On	O
employait	O
aussi	O
l'	O
expression	O
locution	PERSON
vicieuse	PERSON
dans	O
certains	O
dictionnaires	O
,	O
comme	PERSON
le	PERSON
Littré	PERSON
.	O
L'	O
Antiquité	PERSON
ne	PERSON
connaît	PERSON
pas	PERSON
le	PERSON
dictionnaire	PERSON
au	O
sens	O
où	O
nous	O
l'	O
entendons	PERSON
aujourd'hui	PERSON
.	PERSON
La	O
lexicographie	O
n'	O
est	O
donc	O
pas	O
une	O
science	O
délimitée	O
durant	O
l'	O
Antiquité	PERSON
.	PERSON
Seule	PERSON
la	PERSON
Chine	PERSON
,	O
à	O
la	O
même	O
époque	O
,	O
fait	O
exception	O
.	O
Les	PERSON
Grecs	PERSON
ne	PERSON
possédaient	O
pas	O
non	O
plus	O
de	O
véritable	O
dictionnaire	O
.	O
En	O
1694	O
parait	O
la	O
première	O
édition	O
du	O
Dictionnaire	O
de	O
l'	O
Académie	PERSON
française	PERSON
.	PERSON
Citons	PERSON
pour	PERSON
le	PERSON
français	PERSON
ceux	PERSON
de	PERSON
Nodier	PERSON
(	O
1823	O
)	O
,	O
Landais	O
(	O
1834	O
)	O
,	O
Bescherelle	O
(	O
1856	O
)	O
et	O
Littré	O
(	O
de	O
1863	O
à	O
1872	O
)	O
.	O
L'	O
Organisation	O
internationale	O
de	O
normalisation	O
travaille	O
afin	O
de	O
définir	O
un	O
cadre	O
commun	O
normalisé	O
pour	O
l'	O
élaboration	O
des	O
lexiques	O
du	O
traitement	O
automatique	O
des	O
langues	O
.	O
