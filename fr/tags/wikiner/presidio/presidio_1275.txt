Le	O
système	O
russe	O
GLONASS	O
est	O
en	O
cours	O
de	O
restauration	O
opérationnelle	O
.	O
Le	O
systèmes	O
satellitaires	O
ont	O
été	O
précédés	O
par	O
les	O
systèmes	O
terrestres	O
de	O
radionavigation	O
,	O
comme	O
le	O
DECCA	O
,	O
le	O
LORAN	O
et	O
l'	O
Oméga	O
,	O
qui	PERSON
utilisaient	PERSON
des	PERSON
émetteurs	PERSON
terrestres	PERSON
et	O
non	O
des	O
satellites	O
.	O
Le	O
système	O
TRANSIT	O
fut	O
le	O
premier	O
système	O
satellitaire	O
global	O
opérationnel	O
,	O
déployé	O
en	O
1960-70	O
.	O
L'	O
intégrité	LOCATION
est	LOCATION
le	LOCATION
terme	O
officiel	O
de	O
l'	O
OACI	PERSON
pour	O
désigner	O
la	O
fiabilité	O
du	O
point	O
fourni	O
:	O
une	O
position	O
utilisée	O
en	O
navigation	O
au	O
large	O
,	O
par	O
exemple	O
peut	PERSON
être	PERSON
occasionnellement	PERSON
erronée	PERSON
(	O
faible	O
intégrité	O
)	O
sans	O
conséquences	O
graves	O
,	O
si	LOCATION
le	LOCATION
mobile	O
possède	O
des	O
instruments	O
autonomes	O
,	O
alors	O
qu'	O
une	O
position	O
utilisée	O
pour	O
un	O
atterrissage	O
sans	O
visibilité	O
doit	O
au	O
contraire	O
avoir	O
une	O
intégrité	O
absolue	O
.	O
Un	O
système	O
peut	O
avoir	O
une	O
couverture	O
globale	O
ou	O
régionale	O
,	O
il	PERSON
peut	PERSON
être	PERSON
indisponible	PERSON
pendant	O
des	O
périodes	O
plus	O
ou	O
moins	O
longues	O
,	O
avoir	O
des	O
manques	O
de	O
satellites	O
(	O
par	O
exemple	O
GLONASS	O
)	O
.	O
La	LOCATION
Chine	LOCATION
a	O
indiqué	O
son	O
intention	O
d'	O
étendre	O
son	O
système	O
régional	O
Beidou	O
en	O
système	O
global	O
.	O
Le	O
système	O
IRNSS	O
(	O
Indian	O
Regional	O
Navigational	O
Satellite	O
System	O
)	O
est	O
un	O
projet	O
de	O
système	O
autonome	O
de	O
navigation	O
régionale	O
construit	O
et	O
contrôlé	O
par	O
le	O
gouvernement	O
indien	O
.	O
Les	O
systèmes	O
ARGOS	O
et	O
Cospas-Sarsat	O
ne	O
sont	O
pas	O
à	O
proprement	O
parler	O
des	O
systèmes	O
de	O
navigation	O
,	O
mais	O
de	O
positionnement	O
à	O
distance	O
:	O
le	LOCATION
mobile	LOCATION
ne	LOCATION
contient	O
qu'	O
un	O
émetteur	O
,	O
et	O
la	O
position	O
est	O
connue	O
par	O
le	O
centre	O
de	O
calcul	O
du	O
système	O
.	O
Ils	O
fonctionnent	O
,	O
comme	O
le	O
TRANSIT	O
,	O
par	O
mesure	O
d'	O
effet	O
doppler	O
.	O
Les	O
systèmes	O
satellitaires	O
existants	O
(	O
GPS	O
&	O
GLONASS	O
)	O
peuvent	O
être	O
complétés	O
par	O
des	O
systèmes	O
dits	O
d'	O
augmentation	O
ou	O
d'	O
overlay	O
qui	PERSON
délivrent	PERSON
en	O
temps	O
réel	O
des	O
corrections	O
permettant	O
d'	O
accroître	O
la	O
précision	O
ainsi	O
que	O
des	O
informations	O
permettant	O
de	O
garantir	O
l'	O
intégrité	PERSON
de	PERSON
ces	PERSON
corrections	PERSON
.	O
Pour	O
la	O
navigation	O
aérienne	O
,	O
le	O
concept	O
GNSS	O
de	O
l'	O
OACI	PERSON
demande	O
que	O
l'	O
intégrité	O
des	O
systèmes	O
de	O
navigation	O
par	O
satellite	O
soit	O
surveillée	O
,	O
et	O
qu'	O
une	O
alerte	O
soit	O
émise	O
à	O
bord	O
en	O
cas	O
de	O
perte	O
de	O
l'	O
intégrité	O
nécessaire	O
(	O
qui	O
dépend	O
de	O
la	O
phase	O
du	O
vol	O
)	O
.	O
Les	O
systèmes	O
GNSS	O
capable	O
de	O
fournir	O
une	O
précision	O
et	O
une	O
intégrité	PERSON
compatible	PERSON
avec	O
les	O
exigences	O
de	O
la	O
navigation	O
aéronautique	O
civile	O
sont	O
définis	O
ainsi	O
par	O
l'	O
OACI	PERSON
:	O
Pour	O
arriver	O
à	LOCATION
ce	LOCATION
but	O
,	O
les	O
états	O
cherchent	O
une	O
indépendance	O
vis-à-vis	O
du	O
GPS	O
des	O
États-unis	O
,	O
afin	O
de	O
développer	O
des	O
applications	O
civiles	O
ou	O
militaires	O
nationales	O
.	O
