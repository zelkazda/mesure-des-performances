Catherine	PERSON
est	PERSON
la	PERSON
fille	PERSON
cadette	PERSON
du	PERSON
roi	PERSON
Ferdinand	PERSON
II	PERSON
d'	PERSON
Aragon	O
et	O
de	O
la	O
reine	O
Isabelle	O
I	O
re	O
de	O
Castille	O
.	O
Son	PERSON
mariage	PERSON
a	O
pour	O
but	O
de	PERSON
sceller	PERSON
l'	O
alliance	O
diplomatique	O
entre	O
le	O
royaume	O
d'	O
Espagne	O
récemment	O
unifié	O
et	O
celui	O
d'	O
Angleterre	O
,	O
où	O
la	O
maison	O
Tudor	O
vient	O
de	O
s'	O
emparer	O
du	O
trône	O
.	O
Alors	O
que	O
son	O
bâteau	PERSON
était	PERSON
prêt	PERSON
pour	O
partir	O
en	O
Angleterre	O
,	O
une	O
tempête	O
fait	O
rage	O
sur	LOCATION
les	O
côtes	O
espagnoles	O
,	O
repoussant	O
ainsi	O
le	O
départ	O
de	O
5	O
jours	O
.	O
Lors	O
de	O
son	O
arrivée	O
en	O
Angleterre	LOCATION
,	O
elle	O
avait	O
la	O
consigne	O
de	O
garder	O
son	O
visage	O
voilé	PERSON
en	O
public	O
ce	LOCATION
qui	LOCATION
perturba	LOCATION
le	LOCATION
roi	LOCATION
Henri	LOCATION
VII	LOCATION
d'	LOCATION
Angleterre	O
quand	O
vint	O
le	O
moment	O
de	O
la	O
rencontrer	O
:	O
la	O
jeune	PERSON
fille	PERSON
était	PERSON
couchée	PERSON
et	PERSON
ne	PERSON
pouvait	O
recevoir	O
le	O
roi	O
mais	O
celui	O
çi	O
insista	O
pour	O
la	O
voir	O
.	O
On	O
réveilla	PERSON
la	PERSON
jeune	PERSON
Catherine	PERSON
pour	O
qu'	O
elle	O
se	O
montre	O
à	O
son	O
futur	O
beau-père	O
mais	O
elle	O
le	O
fit	O
le	O
visage	O
voilé	O
ce	O
qui	O
contribua	O
à	O
irriter	O
le	O
roi	O
qui	O
eut	O
peur	O
de	O
s'	O
être	O
fait	O
dupé	O
.	O
Lorsque	PERSON
Catherine	PERSON
d'	PERSON
Aragon	PERSON
souleva	PERSON
le	PERSON
voile	PERSON
,	O
Henri	PERSON
VII	PERSON
et	O
son	O
fils	O
Arthur	O
parurent	O
enchanté	O
par	O
la	O
beauté	O
et	O
la	O
grâce	O
de	O
la	O
princesse	O
espagnole	PERSON
.	O
Le	O
mariage	O
est	O
célébré	O
à	O
la	O
Cathédrale	PERSON
Saint-Paul	PERSON
de	PERSON
Londres	PERSON
le	O
14	O
novembre	O
1501	O
.	O
Arthur	PERSON
meurt	PERSON
(	O
peut-être	O
de	O
la	O
suette	O
)	O
le	O
2	O
avril	O
1502	O
,	O
quelques	O
mois	O
seulement	O
après	O
le	O
mariage	O
,	O
tandis	O
que	O
Catherine	PERSON
recouve	PERSON
la	PERSON
santé	PERSON
.	PERSON
Arthur	O
a	O
quinze	O
ans	O
au	O
moment	O
de	O
son	O
décès	O
,	O
il	LOCATION
est	LOCATION
prétendu	O
par	O
la	O
suite	O
que	O
le	O
mariage	O
n'	O
a	O
pas	O
été	O
consommé	O
,	O
ce	O
que	O
corrobore	O
le	O
témoignage	O
de	O
Catherine	O
elle-même	O
.	O
Devenue	PERSON
veuve	PERSON
,	O
Catherine	PERSON
reste	PERSON
en	O
Angleterre	O
.	O
Le	O
motif	O
de	O
cette	O
prolongation	O
de	O
séjour	O
est	O
purement	O
politique	O
et	O
financier	O
:	O
Henri	PERSON
VII	PERSON
n'	PERSON
avait	O
nulle	O
intention	O
de	PERSON
restituer	PERSON
la	PERSON
grosse	O
dot	O
de	O
la	O
jeune	O
femme	O
.	O
Un	O
nouveau	O
mariage	O
est	O
arrangé	PERSON
avec	PERSON
Henri	PERSON
,	PERSON
le	PERSON
frère	PERSON
cadet	PERSON
de	PERSON
son	PERSON
premier	O
mari	PERSON
.	O
Pour	O
d'	O
obscures	O
raisons	O
,	O
on	O
sollicite	LOCATION
du	O
pape	O
Jules	O
II	O
une	O
dispense	O
de	O
ce	O
constat	O
de	O
virginité	O
.	O
Par	O
une	O
bulle	PERSON
de	PERSON
décembre	PERSON
1503	PERSON
,	O
le	O
pape	O
,	O
compréhensif	O
,	O
déclare	O
Henri	O
et	O
Catherine	PERSON
déliés	PERSON
du	PERSON
"	O
lien	O
d'	O
affinité	O
"	O
.	O
Le	O
remariage	O
,	O
désormais	O
possible	O
,	O
est	LOCATION
célébré	LOCATION
le	LOCATION
11	O
juin	O
1509	O
par	O
l'	O
archevêque	O
William	PERSON
Warham	PERSON
,	O
après	O
le	O
décès	O
d'	O
Henri	O
VII	O
(	O
comme	O
il	O
en	O
avait	O
fait	O
la	O
demande	O
)	O
.	O
Catherine	PERSON
d'	PERSON
Aragon	O
est	O
désormais	O
l'	O
épouse	O
du	O
nouveau	O
roi	O
Henri	O
VIII	O
et	O
ipso	O
facto	O
,	O
reine	O
.	O
En	O
1519	O
,	O
il	PERSON
a	O
un	O
fils	O
de	O
sa	O
maîtresse	O
,	O
Elizabeth	PERSON
Blount	PERSON
,	O
à	O
qui	PERSON
il	PERSON
donne	PERSON
le	PERSON
nom	PERSON
révélateur	O
d'	O
Henry	PERSON
FitzRoy	PERSON
.	O
C'	O
est	O
vraisemblablement	O
vers	O
1525	O
ou	O
1526	O
qu'	O
Henri	PERSON
VIII	PERSON
tombe	PERSON
éperdument	PERSON
amoureux	PERSON
d'	PERSON
une	O
des	O
suivantes	O
de	O
la	O
reine	O
,	O
une	O
certaine	O
Anne	PERSON
Boleyn	PERSON
.	O
En	O
1527	O
le	LOCATION
roi	LOCATION
toujours	LOCATION
privé	O
d'	O
héritier	PERSON
mâle	PERSON
légitime	PERSON
,	O
entame	O
l'	O
interminable	O
procédure	O
en	O
vue	O
d'	O
obtenir	O
l'	O
annulation	O
de	O
son	O
mariage	PERSON
avec	PERSON
Catherine	PERSON
d'	PERSON
Aragon	O
.	O
Le	O
roi	O
épouse	O
secrètement	O
sa	PERSON
maîtresse	PERSON
,	O
Anne	PERSON
Boleyn	PERSON
vers	PERSON
le	O
25	O
janvier	O
1533	O
.	O
Catherine	PERSON
est	PERSON
confinée	PERSON
au	O
château	O
de	O
Kimbolton	O
(	O
Cambridgeshire	LOCATION
)	O
,	O
où	O
elle	O
meurt	O
le	O
7	O
janvier	O
1536	O
abandonnée	PERSON
de	PERSON
tous	PERSON
.	O
Henry	PERSON
VIII	PERSON
encourage	O
ses	O
sujets	O
à	O
manifester	O
de	O
la	O
joie	O
à	O
la	O
nouvelle	O
de	O
sa	O
mort	O
.	O
La	O
reine	O
Anne	PERSON
Boleyn	PERSON
,	O
qui	PERSON
l'	O
a	O
supplantée	O
,	O
ne	LOCATION
lui	LOCATION
survit	LOCATION
que	PERSON
de	PERSON
quatre	PERSON
mois	PERSON
.	PERSON
Sa	PERSON
fille	PERSON
est	PERSON
également	PERSON
reine	O
sous	PERSON
le	PERSON
nom	PERSON
d'	PERSON
Élisabeth	PERSON
I	O
re	O
et	O
succède	O
directement	O
à	O
la	O
fille	O
de	O
Catherine	O
,	O
Marie	PERSON
.	O
