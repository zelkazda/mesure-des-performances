Quelques	PERSON
personnalités	O
se	O
présentèrent	O
aussi	O
au	O
forum	O
,	O
dont	O
José	PERSON
Bové	PERSON
,	O
Laurent	PERSON
Fabius	PERSON
,	O
François	PERSON
Hollande	PERSON
.	O
Les	O
groupes	O
politiques	O
les	O
plus	O
visibles	O
furent	O
la	O
Ligue	O
communiste	O
révolutionnaire	O
(	O
LCR	O
)	O
et	O
le	O
Parti	O
communiste	O
français	O
(	O
PCF	O
)	O
.	O
Toujours	PERSON
à	O
Paris	LOCATION
,	O
il	O
y	O
a	O
eu	O
beaucoup	O
de	O
critiques	O
sur	O
la	O
participation	O
du	O
Parti	O
socialiste	O
français	O
,	O
qui	O
,	O
affirment	O
certains	O
altermondialistes	O
,	O
a	O
participé	O
de	O
la	O
"	O
globalisation	O
capitaliste	O
"	O
durant	O
les	O
années	O
précédentes	O
.	O
Pendant	O
la	O
manifestation	O
du	O
15	O
novembre	O
,	O
de	O
violents	O
affrontements	O
ont	O
eu	O
lieu	O
entre	O
les	O
militants	O
du	O
Parti	O
socialiste	O
et	O
les	O
autonomes	O
.	O
Plus	O
de	O
20000	O
personnes	O
de	O
70	O
pays	O
différents	O
assistèrent	O
aux	O
réunions	O
,	O
la	O
plupart	O
organisées	O
à	O
l'	O
Alexandra	PERSON
Palace	PERSON
.	O
