Poitiers	O
est	O
la	O
ville	O
la	O
plus	O
peuplée	O
de	O
la	O
région	O
Poitou-Charentes	LOCATION
,	O
devançant	O
La	PERSON
Rochelle	PERSON
(	O
77	O
196	O
habitants	O
)	O
,	O
Niort	PERSON
(	O
58	O
576	O
habitants	O
)	O
ou	O
Angoulême	O
(	O
42	O
669	O
habitants	O
)	O
.	O
Il	O
s'	O
agit	O
donc	O
d'	O
une	O
voie	O
de	O
passage	O
facile	O
entre	O
le	O
Bassin	O
parisien	O
et	O
le	O
Bassin	O
aquitain	O
,	O
à	O
340	O
km	O
au	O
sud-ouest	O
de	O
Paris	O
,	O
180	O
km	O
de	O
Nantes	O
et	O
à	O
220	O
km	O
de	O
Bordeaux	O
.	O
Poitiers	PERSON
jouit	PERSON
donc	PERSON
d'	O
une	O
position	O
favorable	O
sur	O
une	O
route	O
commerciale	O
et	O
militaire	O
.	O
Poitiers	O
a	O
laissé	O
son	O
nom	PERSON
à	PERSON
trois	PERSON
grandes	PERSON
batailles	PERSON
:	O
(	O
Voir	O
l'	O
article	O
Poitou	PERSON
pour	O
les	O
autres	O
batailles	O
du	O
Seuil	O
du	O
Poitou	O
)	O
.	O
vieil	PERSON
irlandais	PERSON
lem	PERSON
,	O
orme	O
)	O
,	O
même	O
racine	O
indo-européenne	O
que	O
le	O
latin	O
ulmus	O
qui	O
a	O
donné	O
orme	O
;	O
Lemonum	PERSON
signifierait	PERSON
"	O
l'	O
ormeraie	O
"	O
.	O
Il	O
est	O
possible	O
qu'	LOCATION
au	LOCATION
second	O
siècle	O
de	O
notre	O
ère	O
,	O
la	O
ville	O
fut	O
la	O
capitale	O
de	O
la	O
province	O
d'	O
Aquitaine	O
.	O
Les	O
fondations	O
du	O
baptistère	O
Saint-Jean	O
datent	O
de	O
cette	O
époque	O
.	O
À	O
l'	O
époque	O
médiévale	O
,	O
Poitiers	O
tire	O
parti	O
de	O
son	O
site	O
défensif	O
,	O
et	O
de	O
sa	O
situation	O
géographique	O
,	O
loin	O
du	O
centre	O
du	O
pouvoir	O
franc	O
.	O
Une	O
première	O
tentative	O
de	O
création	O
de	O
commune	O
a	O
lieu	O
,	O
de	O
façon	O
autonome	O
par	O
les	O
habitants	O
en	O
1138	O
(	O
peut-être	PERSON
par	PERSON
la	PERSON
confrérie	PERSON
Saint-Hilaire	PERSON
)	O
,	O
qui	PERSON
appellent	PERSON
les	O
bourgs	O
et	O
villes	LOCATION
voisins	O
à	O
former	O
une	O
ligue	O
.	O
La	O
route	O
de	O
Saint-Jacques-de-Compostelle	O
passant	O
par	O
Poitiers	LOCATION
,	O
la	O
ville	O
accueille	PERSON
de	PERSON
nombreux	PERSON
pèlerins	O
,	O
qui	O
y	O
font	O
halte	PERSON
pour	O
vénérer	O
les	O
reliques	O
de	O
sainte	O
Radegonde	O
ou	O
de	O
saint	O
Hilaire	O
.	O
C'	O
est	O
également	O
à	O
Poitiers	O
que	O
Jeanne	O
d'	O
Arc	O
fut	O
examinée	O
en	O
1429	O
avant	O
de	O
recevoir	O
le	O
commandement	O
de	O
l'	O
ost	O
royal	O
.	O
Les	O
poètes	O
Joachim	PERSON
du	PERSON
Bellay	PERSON
et	O
Pierre	PERSON
Ronsard	PERSON
sympathisent	O
à	O
l'	O
Université	O
de	O
Poitiers	O
,	O
avant	O
de	O
monter	O
à	O
Paris	LOCATION
.	O
La	LOCATION
ville	LOCATION
tire	O
sa	O
prospérité	O
essentiellement	O
de	O
ses	O
fonctions	O
administratives	O
:	O
justice	O
royale	O
,	O
évêché	O
,	O
monastères	O
,	O
et	O
l'	O
intendance	PERSON
de	PERSON
la	PERSON
généralité	PERSON
du	PERSON
Poitou	PERSON
.	O
Il	O
fait	O
également	O
abattre	O
la	O
muraille	O
d'	O
Aliénor	O
d'	O
Aquitaine	O
et	O
aménager	O
des	O
boulevards	O
sur	O
leur	O
emplacement	O
.	O
La	O
gare	O
est	O
construite	O
dans	O
les	O
années	O
1850	O
et	O
sera	O
bombardée	O
lors	O
de	O
la	O
Seconde	O
Guerre	O
mondiale	O
,	O
le	O
13	O
juin	O
1944	O
.	O
Le	O
projet	O
du	O
Futuroscope	O
(	O
bâti	O
sur	O
les	O
communes	O
proches	O
de	PERSON
Jaunay-Clan	PERSON
et	O
de	O
Chasseneuil-du-Poitou	O
)	O
,	O
construit	O
en	O
1986	O
--	O
1987	O
sur	O
une	O
idée	O
de	O
René	O
Monory	O
,	O
a	O
permis	O
le	O
développement	O
du	O
secteur	O
touristique	O
de	O
l'	O
agglomération	O
et	O
a	O
ouvert	O
la	O
cité	O
à	O
l'	O
ère	O
technologique	O
et	O
touristique	O
.	O
Aujourd'hui	O
,	O
Poitiers	O
se	O
visite	O
en	O
complément	O
du	O
parc	O
,	O
et	O
bénéficie	O
d'	O
une	O
clientèle	O
de	O
plus	O
en	O
plus	O
européenne	O
,	O
notamment	PERSON
anglaise	PERSON
avec	PERSON
l'	O
ouverture	O
d'	O
une	O
ligne	O
aérienne	O
directe	O
entre	O
l'	O
aéroport	PERSON
de	PERSON
Poitiers-Biard	PERSON
et	PERSON
Londres	PERSON
Stansted	PERSON
.	O
En	O
écho	O
au	O
mouvement	O
national	O
de	O
début	O
de	O
2009	O
,	O
Poitiers	PERSON
voit	PERSON
une	PERSON
manifestation	O
de	O
20	O
000	O
personnes	O
le	O
29	O
janvier	O
rassemble	O
20	O
000	O
personnes	O
,	O
et	O
de	O
30	O
000	O
le	O
19	O
mars	O
.	O
Elle	PERSON
gère	PERSON
l'	PERSON
Aéroport	O
de	O
Poitiers-Biard	O
.	O
La	O
ville	O
est	O
desservie	O
par	O
le	PERSON
réseau	PERSON
de	PERSON
bus	PERSON
de	PERSON
la	PERSON
communauté	PERSON
d'	PERSON
agglomération	O
de	O
Poitiers	O
(	O
CAP	O
)	O
.	O
Poitiers	O
compte	O
deux	O
musées	O
,	O
réunis	LOCATION
en	O
une	O
seule	O
administration	O
.	O
L'	O
Université	O
de	O
Poitiers	O
a	O
été	O
fondée	O
en	O
1431	O
et	O
a	O
formé	O
plusieurs	O
penseurs	O
renommés	O
.	O
Elle	PERSON
accueille	PERSON
également	PERSON
en	O
grand	O
nombre	O
de	O
centres	O
de	O
formation	O
privés	O
,	O
comme	O
l'	O
Isfac	PERSON
.	O
Depuis	O
1991	O
,	O
l'	O
ENSMA	O
et	O
une	O
partie	O
de	O
la	O
faculté	O
des	O
sciences	O
ont	O
été	O
déplacés	O
sur	O
la	O
technopole	O
du	O
Futuroscope	O
.	O
Le	O
Poitiers	O
FC	O
est	O
le	O
club	O
de	O
football	O
de	O
la	O
ville	O
.	O
Son	O
principal	O
fait	O
d'	O
armes	O
est	O
un	O
quart	O
de	O
final	O
de	O
la	O
coupe	O
de	O
la	O
Ligue	O
joué	O
en	O
1997/1998	O
à	O
Bordeaux	O
(	O
perdu	O
4-3	O
)	O
.	O
Le	O
plus	O
célèbre	O
est	O
le	O
parc	O
de	O
Blossac	O
siège	O
d'	O
un	O
petit	O
parc	O
animalier	O
.	O
La	O
communauté	O
d'	O
agglomération	O
de	O
Poitiers	O
mène	O
un	O
effort	O
d'	O
extension	O
des	O
espaces	O
verts	O
,	O
notamment	PERSON
le	PERSON
long	PERSON
des	PERSON
vallées	O
du	O
Clain	O
et	O
de	O
ses	O
affluents	O
.	O
Poitiers	O
est	O
chef-lieu	O
de	O
sept	O
cantons	O
:	O
Poitiers	O
fait	O
partie	O
de	O
la	O
Communauté	O
d'	O
agglomération	O
de	O
Poitiers	O
(	O
CAP	O
)	O
qui	O
regroupe	O
les	O
communes	O
de	PERSON
Biard	PERSON
,	O
Buxerolles	O
,	O
Chasseneuil-du-Poitou	O
,	O
Fontaine-le-Comte	LOCATION
,	O
Mignaloux-Beauvoir	O
,	O
Migné-Auxances	O
,	O
Montamisé	LOCATION
,	O
Poitiers	LOCATION
,	O
Saint-Benoit	O
et	O
Vouneuil-sous-Biard	PERSON
.	O
Au	O
1	O
er	O
janvier	O
2005	O
,	O
les	O
communes	O
de	O
Béruges	O
et	O
Croutelle	O
sont	O
entrées	O
dans	O
l'	O
agglomération	O
,	O
qui	O
,	O
en	O
2006	O
,	O
totalise	O
130	O
710	O
habitants	O
.	O
