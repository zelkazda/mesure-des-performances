En	O
espagnol	O
,	O
cependant	O
,	O
du	O
fait	O
de	O
contraintes	O
liées	O
à	O
la	O
langue	O
,	O
la	O
traduction	O
de	O
La	O
Disparition	O
omet	O
non	LOCATION
pas	LOCATION
le	LOCATION
e	LOCATION
mais	LOCATION
le	LOCATION
a	O
.	O
En	O
russe	O
,	O
la	O
traduction	O
de	O
"	O
La	O
Disparition	O
"	O
omet	O
le	O
"	O
o	O
"	O
,	O
la	O
voyelle	O
la	O
plus	O
fréquente	O
dans	O
cette	O
langue	O
.	O
On	O
en	O
trouve	O
huit	O
au	O
chapitre	O
10	O
de	O
La	O
Disparition	O
de	O
Georges	O
Perec	O
;	O
les	O
rimes	O
féminines	O
(	O
comportant	O
la	O
lettre	O
e	O
)	O
sont	O
exclues	O
par	O
définition	O
,	O
mais	O
les	O
rimes	O
sont	O
parfaites	O
et	O
la	O
rythmique	PERSON
irréprochable	PERSON
.	O
Des	O
auteurs	O
modernes	O
comme	O
Paul	PERSON
Valéry	PERSON
ou	PERSON
Stéphane	PERSON
Mallarmé	PERSON
emploient	O
le	PERSON
lipogramme	PERSON
comme	O
un	O
jeu	O
sur	O
la	O
créativité	O
du	O
poète	O
.	O
Le	O
plus	O
ancien	O
auteur	O
de	O
lipogrammes	O
(	O
ou	O
lipogrammatiste	O
)	O
est	O
Lasos	PERSON
d'	O
Hermione	PERSON
,	O
dont	O
Georges	PERSON
Perec	PERSON
affirme	O
qu'	O
il	PERSON
composa	PERSON
deux	PERSON
poèmes	PERSON
sans	PERSON
utiliser	O
la	O
lettre	O
sigma	O
.	O
Si	PERSON
les	PERSON
créateurs	PERSON
de	PERSON
cette	PERSON
langue	PERSON
choisissent	PERSON
d'	O
écrire	O
cette	O
dernière	O
dans	O
un	O
alphabet	O
préexistant	O
,	O
ils	O
en	O
élimineront	LOCATION
alors	O
certaines	O
lettres	O
.	O
