Visual	O
Basic	O
(	O
VB	O
)	O
est	O
un	O
langage	O
de	O
programmation	O
événementiel	O
de	O
troisième	O
génération	O
ainsi	O
qu'	O
un	O
environnement	O
de	O
développement	O
intégré	O
,	O
créé	LOCATION
par	O
Microsoft	O
pour	O
son	O
modèle	O
de	O
programmation	O
COM	O
.	O
Les	PERSON
langages	PERSON
de	PERSON
script	O
tels	O
que	O
Visual	O
Basic	O
for	O
Applications	O
et	O
VBScript	O
sont	PERSON
syntaxiquement	PERSON
proches	PERSON
de	PERSON
Visual	PERSON
Basic	PERSON
,	PERSON
mais	PERSON
s'	PERSON
utilisent	O
et	O
se	O
comportent	PERSON
de	PERSON
façon	PERSON
sensiblement	PERSON
différente	PERSON
.	PERSON
Un	O
programme	O
en	O
VB	O
peut	O
être	O
développé	O
en	O
utilisant	O
les	O
composants	O
fournis	O
avec	O
Visual	O
Basic	PERSON
lui-même	PERSON
.	O
Les	O
programmes	O
écrits	O
en	O
Visual	O
Basic	O
peuvent	O
aussi	O
utiliser	O
l'	O
API	O
Windows	O
,	O
ceci	O
nécessitant	O
la	O
déclaration	O
dans	O
le	O
programme	O
des	O
fonctions	O
externes	O
.	O
Visual	O
Basic	O
est	O
un	O
des	O
langages	O
les	O
plus	O
utilisés	O
pour	O
l'	O
écriture	O
d'	O
applications	O
commerciales	O
,	O
c'	O
est-à-dire	O
d'	O
applications	O
ne	O
faisant	O
que	O
manipuler	O
des	PERSON
chiffres	PERSON
et	O
des	O
lettres	O
.	O
Ce	O
BASIC	O
est	O
particulièrement	O
adapté	O
à	O
cet	O
usage	O
,	O
pas	O
d'	O
accès	O
système	O
,	O
pas	O
de	O
performances	O
critiques	O
,	O
développement	O
rapide	O
,	O
compétences	O
apparemment	O
faibles	O
.	O
Le	PERSON
défaut	PERSON
étant	O
justement	O
sa	O
facilité	O
de	O
mise	O
en	O
œuvre	O
:	O
un	PERSON
débutant	PERSON
VB	PERSON
pourra	PERSON
rapidement	PERSON
faire	O
un	O
programme	O
"	O
opérationnel	O
"	O
mais	O
souvent	O
tellement	O
"	O
mal	O
"	O
fait	O
(	O
sans	O
analyse	O
,	O
structures	O
ni	PERSON
règles	PERSON
,	O
sans	LOCATION
même	LOCATION
la	LOCATION
moindre	LOCATION
expérience	LOCATION
en	O
programmation	O
parfois	O
...	O
)	O
qu'	O
il	PERSON
sera	PERSON
difficilement	O
maintenable	O
par	O
la	O
suite	O
.	O
Beaucoup	O
de	O
projet	O
VB	O
sont	O
"	O
à	O
refaire	PERSON
entièrement	O
"	O
car	O
ils	O
ont	O
été	O
trop	O
mal	O
faits	O
par	O
des	O
débutants	O
VB	O
.	O
Dans	O
une	O
étude	O
conduite	O
en	O
2005	O
,	O
62	O
pour	O
cent	O
des	O
développeurs	O
déclaraient	O
utiliser	O
l'	O
une	O
ou	O
l'	O
autre	O
forme	O
de	O
Visual	O
Basic	O
.	O
Actuellement	O
,	O
les	LOCATION
langages	LOCATION
les	O
plus	O
utilisés	O
dans	O
le	O
domaine	O
commercial	O
sont	O
Visual	O
Basic	O
,	O
C++	O
,	O
C	O
#	O
,	O
Java	LOCATION
.	O
La	O
dernière	O
mise	O
à	O
jour	O
de	O
Visual	O
Basic	O
est	O
la	O
version	O
6.0	O
,	O
sortie	O
en	O
1998	O
.	O
Le	O
support	O
étendu	O
Microsoft	O
a	O
pris	O
fin	O
en	O
2008	O
.	O
À	O
partir	O
de	O
la	O
version	O
7	O
,	O
le	O
Visual	O
Basic	O
subit	O
des	O
changements	O
substantiels	O
le	O
rapprochant	O
de	O
la	O
plate-forme	O
"	O
dot	O
Net	O
"	O
,	O
et	O
qui	O
amènent	O
Microsoft	O
à	O
le	O
commercialiser	O
sous	O
le	O
nom	O
de	O
Visual	O
Basic	O
.NET	O
.	O
Visual	O
Basic	O
a	O
été	O
conçu	O
pour	O
être	O
facile	O
à	O
apprendre	O
et	O
à	O
utiliser	O
.	O
Ce	O
moteur	O
d'	O
exécution	O
est	O
inclus	O
par	O
défaut	O
dans	O
Windows	O
2000	O
et	O
versions	O
supérieures	O
,	O
sous	O
formes	O
de	O
librairies	O
dynamiques	O
.	O
Pour	O
les	O
versions	O
précédentes	O
de	O
Windows	O
,	O
le	O
moteur	O
d'	O
exécution	O
doit	O
être	O
distribué	O
avec	O
l'	O
exécutable	O
lui-même	PERSON
.	O
À	O
la	O
différence	O
de	O
beaucoup	O
d'	O
autres	O
langages	O
de	O
programmation	O
,	O
Visual	O
Basic	O
n'	O
est	O
en	O
général	O
pas	O
sensible	O
à	O
la	O
casse	O
(	O
l'	O
usage	O
des	PERSON
majuscules	PERSON
ou	PERSON
des	O
minuscules	O
est	O
indifférent	O
)	O
,	O
bien	O
qu'	O
il	O
transforme	O
automatiquement	O
l'	O
écriture	O
des	O
mots-clés	O
selon	O
une	O
convention	O
standard	O
et	O
qu'	O
il	O
force	O
l'	O
écriture	O
des	O
variables	O
dans	O
le	O
code	O
à	O
être	O
identique	O
à	O
l'	O
écriture	O
employée	O
lors	O
de	O
la	O
déclaration	O
.	O
Visual	O
Basic	O
possède	O
quelques	O
caractéristiques	O
inhabituelles	O
:	O
VB	O
1.0	O
a	O
vu	O
le	O
jour	O
en	O
1991	O
.	O
Visual	O
Basic	O
est	O
critiqué	O
pour	O
sa	O
gestion	O
mémoire	O
peu	O
performante	O
et	O
pour	O
la	O
possibilité	O
qu'	LOCATION
il	LOCATION
offre	LOCATION
de	LOCATION
pouvoir	LOCATION
construire	LOCATION
du	O
code	O
utilisant	O
des	O
constructions	O
peu	O
académiques	O
,	O
pouvant	O
donner	PERSON
de	PERSON
mauvaises	O
habitudes	O
de	O
programmation	O
et	O
permettant	O
d'	O
écrire	O
du	O
code	O
peu	O
performant	O
.	O
Voici	O
quelques	O
exemples	O
de	O
code	O
Visual	O
Basic	O
:	O
Visual	O
Basic	O
permet	O
de	O
développer	O
des	O
interfaces	O
utilisateurs	O
graphiques	O
très	O
riches	O
.	O
