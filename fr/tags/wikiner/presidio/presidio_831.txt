La	O
suite	O
du	O
"	O
siècle	O
d'	O
or	O
de	O
la	O
littérature	O
russe	PERSON
"	O
produit	O
de	O
grands	O
romanciers	O
comme	O
Dostoïevski	O
,	O
Gogol	PERSON
,	O
Tolstoï	PERSON
ou	PERSON
Tourguéniev	PERSON
,	O
la	O
fin	O
du	O
siècle	O
étant	O
marquée	PERSON
par	PERSON
la	PERSON
figure	PERSON
du	PERSON
dramaturge	O
Anton	PERSON
Tchekov	PERSON
.	O
Le	O
siècle	O
est	O
cependant	O
riche	O
de	O
poètes	O
comme	O
Sergueï	PERSON
Essénine	PERSON
et	O
Vladimir	PERSON
Maïakovski	PERSON
et	O
de	O
romanciers	O
comme	O
Maxime	PERSON
Gorki	PERSON
,	O
Boris	PERSON
Pasternak	PERSON
,	O
Mikhaïl	PERSON
Cholokhov	PERSON
ou	PERSON
Mikhaïl	PERSON
Boulgakov	PERSON
.	O
La	O
répression	O
stalinienne	O
frappera	O
particulièrement	O
des	O
écrivains	O
comme	O
Vassili	PERSON
Grossman	PERSON
,	O
Varlam	PERSON
Chalamov	PERSON
ou	O
Alexandre	PERSON
Soljenitsyne	PERSON
qui	PERSON
dénoncent	PERSON
le	PERSON
système	PERSON
totalitaire	O
soviétique	O
.	O
La	O
littérature	O
vieux-russe	O
se	O
constitue	O
de	O
rares	O
ouvrages	O
écrits	O
en	O
vieux-russe	O
comme	O
l'	O
anonyme	O
Dit	PERSON
de	PERSON
la	PERSON
campagne	PERSON
d'	O
Igor	O
.	O
Le	O
pouvoir	O
échoit	O
finalement	O
à	O
Boris	PERSON
Godounov	PERSON
.	O
Sous	PERSON
l'	O
impulsion	O
de	O
la	O
République	O
des	O
Deux	O
Nations	O
(	O
Pologne	LOCATION
et	O
Lituanie	O
)	O
,	O
la	O
Russie	O
s'	O
ouvre	O
au	O
monde	O
extérieur	O
.	O
L'	O
incertitude	O
s'	O
achève	O
en	O
1615	O
,	O
après	O
l'	O
élection	O
d'	O
un	PERSON
tsar	PERSON
en	O
1613	O
:	O
Michel	PERSON
III	PERSON
Romanov	PERSON
,	O
premier	O
représentant	O
de	PERSON
la	PERSON
longue	PERSON
dynastie	O
Romanov	PERSON
.	O
Cependant	O
,	O
Pierre	PERSON
le	PERSON
Grand	PERSON
ne	PERSON
nourrit	O
pas	O
un	O
intérêt	O
profond	O
pour	O
la	O
littérature	O
et	O
l'	O
art	O
.	O
Sous	PERSON
Pierre	PERSON
le	PERSON
Grand	PERSON
,	O
les	O
chansons	O
d'	O
amour	O
sont	O
tolérées	LOCATION
,	O
ce	O
qui	O
est	O
un	O
changement	O
essentiel	O
.	O
En	O
1745	O
,	O
le	PERSON
poète	PERSON
Vasily	PERSON
Trediakovski	PERSON
envisage	O
la	O
création	O
d'	O
une	O
langue	O
littéraire	O
,	O
mélange	PERSON
de	PERSON
la	PERSON
langue	PERSON
populaire	O
et	O
du	O
slavon	O
.	O
Et	PERSON
il	PERSON
écrira	PERSON
lui-même	PERSON
dans	PERSON
le	PERSON
parler	PERSON
contemporain	O
de	O
Moscou	O
.	O
Ce	O
fait	O
révèle	O
le	O
besoin	O
d'	O
élaborer	PERSON
une	O
langue	O
écrite	O
adaptée	O
à	O
la	O
réalité	O
de	O
la	O
Russie	O
contemporaine	O
.	O
Le	O
romantisme	O
,	O
au	O
début	O
du	O
XIX	O
e	O
siècle	O
voit	O
l'	O
éclosion	O
d'	O
une	O
génération	O
talentueuse	O
avec	O
Vassili	PERSON
Joukovski	PERSON
mais	O
surtout	PERSON
Alexandre	PERSON
Pouchkine	PERSON
,	O
Mikhaïl	O
Lermontov	O
et	O
Fiodor	PERSON
Tiouttchev	PERSON
.	O
Ce	PERSON
siècle	PERSON
sera	PERSON
le	PERSON
siècle	PERSON
d'	PERSON
or	O
de	O
la	O
littérature	O
russe	O
et	O
plus	O
particulièrement	O
du	O
roman	O
avec	O
Fedor	O
Dostoïevski	O
,	O
Nicolas	PERSON
Gogol	PERSON
,	O
Ivan	PERSON
Gontcharov	PERSON
,	O
Nicolaï	PERSON
Leskov	PERSON
,	O
Mikhaïl	PERSON
Saltykov-Chtchédrine	PERSON
,	O
Léon	PERSON
Tolstoï	PERSON
,	O
Ivan	PERSON
Tourgueniev	PERSON
...	O
Anton	PERSON
Tchekhov	PERSON
développe	PERSON
à	O
la	O
fois	O
une	O
œuvre	PERSON
théâtrale	PERSON
essentielle	O
,	O
mais	PERSON
aussi	PERSON
tout	O
un	O
registre	O
d'	O
histoires	O
très	O
courtes	O
qui	O
en	O
fait	O
un	O
des	O
auteurs	O
russophone	O
les	O
plus	O
marquants	O
.	O
De	PERSON
nombreux	PERSON
poètes	PERSON
participent	O
à	PERSON
ce	PERSON
nouvel	PERSON
âge	PERSON
d'	PERSON
or	O
:	O
Anna	PERSON
Akhmatova	PERSON
,	O
Innocent	PERSON
Annenski	PERSON
,	O
Andreï	PERSON
Biély	PERSON
,	O
Alexandre	PERSON
Blok	PERSON
,	O
Valéry	PERSON
Brioussov	PERSON
,	O
Marina	PERSON
Tsvetaïeva	PERSON
,	O
Sergueï	PERSON
Essénine	PERSON
,	O
Nikolaï	PERSON
Goumilev	PERSON
,	O
Daniil	PERSON
Harms	PERSON
,	O
Vélimir	PERSON
Khlebnikov	PERSON
,	O
Ossip	PERSON
Mandelstam	PERSON
,	O
Vladimir	PERSON
Maïakovski	PERSON
,	O
Boris	PERSON
Pasternak	PERSON
,	O
Fiodor	PERSON
Sologoub	PERSON
ou	PERSON
Maximilien	PERSON
Volochine	PERSON
.	O
Après	PERSON
la	PERSON
révolution	PERSON
d'	O
Octobre	O
,	O
de	O
nombreux	O
écrivains	O
russes	O
s'	O
exilent	O
,	O
notamment	O
à	LOCATION
Berlin	LOCATION
,	O
puis	LOCATION
à	LOCATION
Paris	LOCATION
,	O
où	O
de	O
nombreuses	O
revues	O
littéraires	O
en	O
russe	O
sont	O
éditées	O
.	O
Mais	PERSON
avec	PERSON
le	PERSON
démarrage	PERSON
de	PERSON
la	PERSON
NEP	PERSON
,	O
une	O
relative	O
liberté	O
est	O
accordée	O
aux	O
écrivains	O
,	O
et	O
certains	O
exilés	O
choisissent	O
de	O
revenir	O
en	O
Russie	LOCATION
(	O
Victor	PERSON
Chklovski	PERSON
,	O
Andreï	PERSON
Biély	PERSON
,	O
et	O
plus	O
tard	O
,	O
Maxime	PERSON
Gorki	PERSON
)	O
.	O
L'	O
arrivée	O
au	O
pouvoir	O
suprême	O
de	PERSON
Joseph	PERSON
Staline	PERSON
en	O
1930	O
marque	O
la	O
fin	O
de	O
la	O
relative	O
liberté	LOCATION
accordée	LOCATION
aux	LOCATION
écrivains	LOCATION
russes	O
par	O
le	O
pouvoir	O
bolchévique	O
.	O
Les	O
poètes	O
futuristes	O
Vladimir	PERSON
Maïakovski	PERSON
et	O
Marina	PERSON
Tsvetaïeva	PERSON
choisiront	LOCATION
le	O
suicide	O
.	O
Roman	PERSON
Jakobson	PERSON
s'	PERSON
installe	O
aux	O
États-Unis	O
,	O
Victor	PERSON
Chklovski	PERSON
et	O
Mikhaïl	O
Bakhtine	PERSON
sont	O
réduits	O
au	O
silence	O
.	O
Certains	O
auteurs	O
,	O
pour	O
contourner	O
la	O
censure	O
,	O
s'	O
abritent	O
derrière	O
le	O
genre	O
du	O
conte	O
pour	O
enfants	O
(	O
Daniil	PERSON
Harms	PERSON
)	PERSON
ou	O
de	O
la	O
bibliographie	O
historique	O
(	O
Iouri	O
Tynianov	O
)	O
.	O
Mais	PERSON
la	PERSON
plupart	PERSON
des	PERSON
auteurs	PERSON
(	O
Mikhaïl	PERSON
Boulgakov	PERSON
,	O
Boris	PERSON
Pasternak	PERSON
,	O
Andreï	PERSON
Platonov	PERSON
,	O
Ossip	PERSON
Mandelstam	PERSON
,	O
Iouri	PERSON
Olecha	PERSON
,	O
Isaac	PERSON
Babel	PERSON
ou	PERSON
Vassili	PERSON
Grossman	PERSON
)	O
continuent	O
leur	O
travail	O
littéraire	O
de	O
manière	O
parfois	O
clandestine	O
,	O
en	O
espérant	O
être	O
publiés	O
de	O
manière	O
posthume	O
ou	O
à	O
travers	O
le	O
régime	O
des	O
samizdat	O
(	O
publications	O
artisanales	O
clandestines	O
)	O
.	O
Vénédict	O
Erofeiev	O
continue	O
son	O
travail	O
de	O
publication	O
par	O
samizdat	O
.	O
Les	PERSON
besoins	PERSON
de	PERSON
cette	PERSON
période	PERSON
sont	PERSON
de	PERSON
deux	O
types	O
:	O
former	O
et	O
découvrir	O
de	O
nouveaux	O
talents	O
et	O
créer	O
une	O
économie	O
de	O
l'	O
édition	O
en	O
Russie	LOCATION
.	O
Peu	O
d'	O
écrivains	O
,	O
comme	O
Viktor	PERSON
Pelevine	PERSON
ou	O
Vladimir	PERSON
Sorokine	PERSON
sortent	O
du	O
lot	O
.	O
La	O
littérature	O
plus	O
traditionnelle	O
trouve	PERSON
aussi	PERSON
un	PERSON
nouvel	PERSON
essor	PERSON
avec	PERSON
des	PERSON
auteurs	PERSON
venus	PERSON
de	PERSON
régions	PERSON
éloignées	PERSON
comme	PERSON
Nina	PERSON
Gorlanova	PERSON
de	PERSON
Perm	PERSON
avec	PERSON
ses	PERSON
histoires	PERSON
sur	PERSON
les	PERSON
difficultés	PERSON
quotidiennes	PERSON
et	PERSON
les	PERSON
joies	PERSON
de	PERSON
l'	O
intelligentsia	O
provinciale	O
ou	O
encore	O
Youri	PERSON
Rytkhéou	PERSON
de	PERSON
la	PERSON
Tchoukotka	PERSON
qui	PERSON
raconte	PERSON
les	PERSON
problèmes	O
identitaires	O
des	O
Tchouktches	O
.	O
Des	O
auteurs	O
tels	O
que	O
Dmitri	PERSON
Gloukhovski	PERSON
ou	PERSON
Sergueï	PERSON
Loukianenko	PERSON
connaissent	O
un	O
succès	O
avec	O
leurs	O
romans	O
de	O
science-fiction	O
qui	O
ont	O
même	O
été	O
adaptés	O
en	O
films	O
ou	O
jeux	PERSON
vidéos	PERSON
.	O
