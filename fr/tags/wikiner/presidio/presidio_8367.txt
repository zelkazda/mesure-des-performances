Fernando	PERSON
Arrabal	PERSON
est	PERSON
un	PERSON
écrivain	PERSON
et	O
cinéaste	PERSON
né	PERSON
le	PERSON
11	PERSON
août	O
1932	O
à	O
Melilla	O
(	O
Espagne	O
)	O
.	O
Il	O
vit	O
en	O
France	LOCATION
depuis	O
1955	O
;	O
'	O
desterrado	O
'	O
est	O
sa	O
définition	O
,	O
qu'	O
on	O
pourrait	O
traduire	O
par	O
mi-expatrié	O
,	O
mi-exilé	PERSON
.	O
Lors	O
du	O
dernier	O
demi-siècle	O
quarante	O
personnalités	O
ont	PERSON
reçu	PERSON
cette	PERSON
distinction	O
,	O
parmi	O
lesquelles	O
:	O
Marcel	PERSON
Duchamp	PERSON
,	O
Eugène	PERSON
Ionesco	PERSON
,	O
Man	PERSON
Ray	PERSON
,	O
Boris	PERSON
Vian	PERSON
,	O
Dario	PERSON
Fo	PERSON
,	O
Umberto	PERSON
Eco	PERSON
et	PERSON
Jean	PERSON
Baudrillard	PERSON
.	O
Dans	O
son	O
enfance	O
il	PERSON
a	O
souffert	O
de	O
la	O
mystérieuse	O
disparition	O
de	PERSON
son	PERSON
père	PERSON
,	O
condamné	O
à	O
mort	O
par	O
le	O
régime	O
de	O
Franco	O
,	O
puis	PERSON
évadé	PERSON
en	O
1941	O
.	O
Plus	O
tard	O
la	O
démocratie	O
en	O
Espagne	PERSON
lui	PERSON
permettra	PERSON
d'	PERSON
atteindre	O
une	O
véritable	O
reconnaissance	O
dans	O
son	O
pays	O
natal	O
avec	O
une	O
centaine	O
de	O
distinctions	O
dont	O
deux	O
prix	O
nationaux	O
de	O
théâtre	O
.	O
Le	O
29	O
décembre	O
1941	O
Fernando	PERSON
Arrabal	PERSON
senior	O
s'	O
évade	O
de	O
l'	O
hôpital	O
en	O
pyjama	O
,	O
au-dehors	O
un	PERSON
mètre	PERSON
de	PERSON
neige	PERSON
recouvre	PERSON
les	O
champs	O
.	O
Arrabal	PERSON
a	O
écrit	O
:	O
"	O
Sans	O
vouloir	O
comparer	O
l'	O
incomparable	O
,	O
face	O
à	O
ces	O
choses	O
crépusculaires	O
(	O
et	O
sans	O
lien	PERSON
logique	PERSON
bien	PERSON
souvent	PERSON
)	O
je	O
pense	O
fréquemment	O
à	O
un	O
bouc	O
émissaire	O
:	O
mon	PERSON
père	PERSON
.	O
Le	O
jour	O
où	O
a	O
commencé	O
la	O
guerre	O
incivile	O
,	O
il	O
a	O
été	O
enfermé	O
par	O
'	O
ses	O
compagnons	O
compatissants	PERSON
'	O
dans	O
la	O
salle	O
des	O
drapeaux	O
d'	O
une	O
caserne	O
de	PERSON
Melilla	PERSON
;	O
pour	O
qu'	O
il	O
réfléchisse	O
bien	O
,	O
car	O
il	O
risquait	O
d'	O
être	O
condamné	O
à	O
mort	O
pour	O
rébellion	O
militaire	O
s'	O
il	O
ne	O
se	O
joignait	PERSON
pas	O
au	LOCATION
soulèvement	LOCATION
(	O
alzamiento	LOCATION
)	O
.	O
Au	O
bout	O
d'	O
une	O
heure	O
le	O
lieutenant	O
Fernando	PERSON
Arrabal	PERSON
a	O
appelé	O
ses	O
ex-camarades	O
,	O
déjà	O
!	O
En	O
1941	O
Fernando	PERSON
Arrabal	PERSON
gagne	PERSON
un	PERSON
concours	PERSON
d	PERSON
'	PERSON
"	O
enfants	O
surdoués	O
"	O
.	O
A	O
cette	O
époque	O
Arrabal	O
lit	O
beaucoup	O
et	O
mène	O
des	O
expériences	O
,	O
qui	O
,	O
comme	O
il	LOCATION
le	LOCATION
reconnaît	LOCATION
lui-même	O
,	O
plus	O
tard	PERSON
lui	PERSON
seront	O
utiles	O
.	O
En	O
1954	O
il	O
se	O
rend	O
à	O
Paris	LOCATION
en	O
auto-stop	O
pour	O
voir	O
jouer	O
Mère	O
Courage	O
et	O
ses	O
enfants	O
de	PERSON
Bertold	PERSON
Brecht	PERSON
,	O
car	O
le	O
Berliner	O
Ensemble	O
se	O
produit	O
dans	O
la	O
capitale	O
.	O
Sous	PERSON
le	PERSON
régime	PERSON
franquiste	PERSON
il	PERSON
est	PERSON
jugé	PERSON
et	PERSON
emprisonné	PERSON
(	O
1967	O
)	O
malgré	O
la	O
solidarité	O
de	O
la	O
plupart	O
des	O
écrivains	O
de	O
cette	O
époque	O
,	O
de	O
François	PERSON
Mauriac	PERSON
à	O
Arthur	PERSON
Miller	PERSON
,	O
et	O
la	O
requête	O
du	O
célèbre	O
dramaturge	O
irlandais	O
Samuel	PERSON
Beckett	PERSON
qui	PERSON
déclare	PERSON
:	O
"	O
Si	O
faute	O
il	O
y	O
a	O
qu'	O
elle	O
soit	O
vue	O
à	O
la	O
lumière	O
du	O
grand	O
mérite	O
d'	O
hier	O
et	O
de	O
la	O
grande	O
promesse	O
de	O
demain	O
et	O
par	O
là	PERSON
pardonnée	PERSON
"	O
.	O
La	O
mort	O
du	O
général	O
Franco	O
lui	O
a	O
permis	O
d'	O
obtenir	O
une	O
véritable	O
reconnaissance	O
dans	O
son	O
pays	O
natal	O
.	O
Fernando	PERSON
Arrabal	PERSON
a	O
réalisé	O
sept	O
longs-métrages	O
comme	O
metteur	O
en	O
scène	O
.	O
Alejandro	PERSON
Jodorowsky	PERSON
)	O
,	O
etc	O
.	O
Arrabal	PERSON
a	O
publié	O
aussi	O
huit	O
cents	O
livres	O
de	O
bibliophilie	O
illustrés	O
par	O
,	O
Salvador	PERSON
Dalí	PERSON
,	O
René	PERSON
Magritte	PERSON
,	O
Roland	PERSON
Topor	PERSON
,	O
Enrico	PERSON
Baj	PERSON
,	O
Antonio	PERSON
Saura	PERSON
,	O
Alekos	PERSON
Fassianos	PERSON
,	O
etc	O
.	O
Arrabal	PERSON
qui	PERSON
a	O
obtenu	O
deux	O
prix	O
nationaux	O
de	O
théâtre	O
est	O
actuellement	O
le	O
dramaturge	O
le	O
plus	O
joué	O
.	O
Un	O
livret	O
du	O
corrosif	O
,	O
génial	O
et	O
inclassable	PERSON
Fernando	PERSON
Arrabal	PERSON
...	O
"	O
.	O
