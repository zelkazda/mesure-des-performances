Il	O
commence	O
à	O
enseigner	O
l'	O
allemand	O
en	O
lycée	O
en	O
1973	O
,	O
à	O
Rezé	PERSON
puis	PERSON
à	PERSON
Saint-Herblain	PERSON
.	O
Il	O
adhère	O
au	O
Parti	O
socialiste	O
en	O
1971	O
,	O
à	O
l'	O
occasion	O
du	PERSON
congrès	PERSON
d'	O
Épinay	PERSON
.	O
Il	O
est	O
élu	O
conseiller	O
général	O
à	O
26	O
ans	O
,	O
puis	PERSON
maire	PERSON
de	PERSON
Saint-Herblain	PERSON
(	O
banlieue	O
ouest	O
de	O
Nantes	O
)	O
en	O
1977	O
.	O
Il	O
occupe	O
cette	O
fonction	O
jusqu'	O
en	O
1989	O
,	O
date	O
à	O
laquelle	PERSON
il	PERSON
se	PERSON
porte	PERSON
candidat	O
à	O
la	O
mairie	O
de	O
Nantes	O
et	O
succède	O
au	O
maire	O
sortant	O
,	O
le	PERSON
sénateur	PERSON
RPR	PERSON
Michel	PERSON
Chauty	PERSON
,	O
qui	O
ne	O
se	O
représentait	PERSON
pas	O
,	O
et	O
qui	O
fut	O
également	O
son	O
prédécesseur	PERSON
à	O
la	O
mairie	O
de	O
Saint-Herblain	O
.	O
Il	O
est	O
reconduit	O
le	O
25	O
juin	O
2007	O
,	O
pour	O
la	O
troisième	PERSON
fois	PERSON
,	O
comme	PERSON
président	PERSON
de	PERSON
groupe	PERSON
,	O
devenu	O
le	O
Groupe	O
socialiste	O
,	O
radical	O
,	O
citoyen	O
et	O
apparentés	O
(	O
SRC	O
)	O
.	O
Depuis	PERSON
son	PERSON
élection	PERSON
en	O
1989	O
,	O
il	O
a	O
été	O
réélu	O
à	O
trois	O
reprises	O
dès	LOCATION
le	LOCATION
premier	O
tour	O
,	O
ce	LOCATION
qui	LOCATION
est	LOCATION
,	O
après	PERSON
André	PERSON
Rossinot	PERSON
,	O
à	O
Nancy	PERSON
,	O
un	O
record	O
de	O
longévité	O
pour	O
un	O
maire	O
d'	O
une	O
grande	O
ville	O
encore	O
en	O
place	O
.	O
Le	O
18	O
août	O
2008	O
,	O
Jean-Marc	PERSON
Ayrault	PERSON
a	O
reçu	O
le	O
Dalaï-lama	PERSON
à	O
la	O
mairie	O
de	O
Nantes	O
à	O
l'	O
occasion	O
du	PERSON
séjour	PERSON
du	O
chef	O
spirituel	PERSON
tibétain	PERSON
à	O
Nantes	LOCATION
,	O
du	O
15	O
au	O
20	O
août	O
.	O
