La	LOCATION
ville	LOCATION
se	O
situe	O
à	O
90	O
km	O
de	O
Paris	O
.	O
Il	O
s'	O
agit	O
là	O
d'	O
une	O
tradition	O
de	O
la	O
Rome	O
antique	O
:	O
la	O
couronne	O
de	PERSON
chêne	PERSON
était	PERSON
décernée	PERSON
à	PERSON
tout	PERSON
citoyen	PERSON
ayant	PERSON
,	O
sur	LOCATION
le	O
champ	O
de	PERSON
bataille	PERSON
,	O
sauvegardé	PERSON
l'	O
existence	O
d'	O
un	O
de	O
ses	O
concitoyens	O
.	O
Après	PERSON
l'	O
occupation	O
de	O
la	O
colline	O
,	O
la	O
ville	O
s'	O
est	O
étendue	O
sur	O
une	O
langue	O
de	O
terre	O
constituée	O
par	O
deux	PERSON
bras	PERSON
de	PERSON
l'	PERSON
Eure	O
ainsi	O
que	O
sur	O
une	O
pente	O
douce	O
du	O
côté	O
opposé	O
à	O
la	O
rivière	O
.	O
Une	O
autre	O
source	O
de	O
cette	O
puissance	O
résidait	O
dans	O
la	O
richesse	O
de	O
la	O
Beauce	O
où	O
le	O
chapitre	O
de	O
la	O
cathédrale	O
possédait	O
de	O
grands	O
domaines	O
.	O
À	O
partir	O
de	O
1923	O
,	O
Raoul	PERSON
Brandon	PERSON
érige	PERSON
,	O
sur	LOCATION
le	O
site	O
de	O
l'	O
ancien	O
marché	O
aux	O
chevaux	O
,	O
un	O
imposant	O
bâtiment	O
vite	O
surnommé	O
"	O
Notre-Dame-des-Postes	O
"	O
.	O
