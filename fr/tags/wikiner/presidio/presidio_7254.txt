Cette	O
plausibilité	O
est	O
contestée	O
par	O
Georges	PERSON
Dumézil	PERSON
qui	PERSON
,	O
considérant	O
pour	O
des	O
raisons	O
étymologiques	O
et	O
lexicologiques	O
,	O
que	O
le	O
son	O
/qu/	PERSON
ne	PERSON
peut	PERSON
être	PERSON
d'	PERSON
origine	O
sabine	O
,	O
lui	PERSON
préfère	PERSON
le	PERSON
sens	PERSON
de	PERSON
curia	PERSON
.	O
Pour	O
lui	O
,	O
Quirinus	O
,	O
aux	O
côtés	O
de	O
Jupiter	O
et	O
de	O
Mars	O
,	O
fait	O
partie	O
de	O
la	O
triade	O
précapitoline	O
et	O
y	O
incarne	O
la	O
fonction	O
de	O
production	O
et	O
de	O
reproduction	O
.	O
Finalement	O
,	O
le	PERSON
dieu	PERSON
latin	PERSON
s'	PERSON
est	O
volatilisé	O
pour	O
être	O
identifié	O
avec	O
Romulus	O
divinisé	O
.	O
Tite-Live	O
relate	O
ainsi	O
la	O
légende	O
:	O
Après	PERSON
la	PERSON
mort	PERSON
de	PERSON
Romulus	PERSON
(	O
une	O
mort	PERSON
un	PERSON
peu	PERSON
suspecte	PERSON
,	O
il	PERSON
a	O
disparu	O
dans	O
un	O
orage	O
)	O
,	O
les	O
sénateurs	O
dirent	O
qu'	O
il	LOCATION
avait	LOCATION
été	LOCATION
enlevé	O
au	PERSON
ciel	PERSON
par	O
Mars	LOCATION
son	O
père	O
.	O
Le	O
peuple	O
n'	O
y	O
crut	O
pas	O
et	O
demanda	O
des	O
preuves	O
c'	O
est	O
alors	O
qu'	O
un	O
citoyen	O
digne	O
de	O
foi	O
déclara	O
qu'	O
il	O
avait	O
vu	O
en	O
songe	O
Romulus	PERSON
qui	PERSON
lui	PERSON
avait	PERSON
dit	PERSON
qu'	PERSON
il	PERSON
désirait	PERSON
être	PERSON
adoré	PERSON
sous	PERSON
le	PERSON
nom	PERSON
de	PERSON
Quirinus	PERSON
.	O
Deux	PERSON
autres	PERSON
inscriptions	O
,	O
l'	O
une	O
de	O
l'	O
an	O
-236	O
,	O
l'	O
autre	O
de	O
-204	O
ou	O
de	O
-191	O
,	O
ont	O
été	O
trouvées	O
sur	O
le	O
Quirinal	O
.	O
Sur	O
la	O
première	O
,	O
Mars	LOCATION
est	O
invoqué	O
sans	O
vocable	O
,	O
la	O
seconde	O
lui	PERSON
donne	PERSON
celui	PERSON
de	PERSON
Quirinus	PERSON
:	O
l'	O
identification	O
parait	O
s'	O
être	O
effectuée	O
dans	O
l'	O
intervalle	O
.	O
Alors	O
,	O
le	O
vieux	O
Quirinus	O
ne	O
fut	O
plus	O
qu'	O
un	O
souvenir	O
archéologique	O
.	O
Theodor	PERSON
Mommsen	PERSON
dit	PERSON
que	PERSON
,	O
sauf	O
l'	O
inscription	O
de	O
l'	O
an	O
-204	O
,	O
il	O
n'	O
y	O
a	O
aucun	PERSON
témoignage	PERSON
positif	PERSON
pour	O
affirmer	O
la	O
substitution	O
de	O
Mars	O
lui-même	O
au	O
Quirinus	O
primitif	O
.	O
Quirinus	PERSON
avait	PERSON
,	O
à	O
Rome	LOCATION
,	O
d'	O
autres	O
temples	O
,	O
un	PERSON
notamment	PERSON
auprès	PERSON
de	PERSON
la	PERSON
porte	PERSON
qui	PERSON
lui	PERSON
était	PERSON
redevable	O
de	O
son	O
nom	O
.	O
