Wings	O
3D	O
est	O
un	PERSON
logiciel	PERSON
libre	PERSON
de	PERSON
modélisation	PERSON
polygonale	PERSON
en	O
trois	O
dimensions	O
par	O
surface	O
de	O
subdivision	O
.	O
Wings	O
3D	O
est	O
disponible	O
sous	PERSON
de	PERSON
nombreuses	O
plate-formes	O
,	O
y	O
compris	O
GNU/Linux	O
,	O
Mac	O
OS	O
X	O
et	O
Windows	O
.	O
Il	LOCATION
est	LOCATION
écrit	LOCATION
dans	LOCATION
le	LOCATION
langage	LOCATION
de	LOCATION
programmation	O
Erlang	O
et	O
utilise	O
une	O
console	O
virtuelle	O
.	O
De	O
plus	O
,	O
son	O
système	O
de	O
plugin	O
rend	O
possible	O
l'	O
extension	O
des	O
capacités	O
du	O
logiciel	O
:	O
il	O
est	O
par	O
exemple	O
possible	O
d'	O
utiliser	O
un	PERSON
moteur	PERSON
de	PERSON
rendu	PERSON
par	PERSON
lancer	PERSON
de	PERSON
rayon	PERSON
externe	O
tel	O
que	O
Yafray	O
ou	O
PovRay	O
.	O
Wings	O
3D	O
est	O
disponible	O
en	O
français	O
et	O
dispose	O
de	PERSON
tutoriels	PERSON
et	O
d'	O
une	O
interface	O
très	O
légère	O
qui	O
permettent	O
d'	O
en	O
assimiler	O
les	O
bases	O
rapidement	O
.	O
