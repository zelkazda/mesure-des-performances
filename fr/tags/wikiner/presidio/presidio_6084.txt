Pour	O
définir	PERSON
sa	PERSON
passion	O
,	O
il	PERSON
déclare	PERSON
,	O
"	O
Je	PERSON
suis	PERSON
entré	PERSON
dans	O
l'	O
Égypte	O
par	O
la	O
momie	PERSON
du	PERSON
musée	PERSON
de	PERSON
Boulogne	PERSON
"	O
et	O
"	O
Le	O
canard	O
égyptien	O
est	O
un	O
animal	O
dangereux	O
:	O
un	PERSON
coup	PERSON
de	PERSON
bec	PERSON
,	O
il	O
vous	O
inocule	PERSON
le	PERSON
venin	PERSON
et	PERSON
vous	PERSON
êtes	PERSON
égyptologue	PERSON
pour	PERSON
la	PERSON
vie	PERSON
"	O
.	O
Le	O
voyageur	O
grec	O
affirmait	O
qu'	O
il	O
se	O
trouvait	O
à	O
Memphis	LOCATION
"	O
un	PERSON
temple	PERSON
de	PERSON
Sarapis	PERSON
dans	O
un	O
endroit	O
tellement	O
sablonneux	O
que	O
les	O
vents	O
y	O
amoncellent	O
des	O
amas	O
de	O
sable	O
sous	O
lesquels	O
nous	O
vîmes	O
des	O
sphinx	O
enterrés	O
,	O
les	O
uns	O
à	O
moitié	O
,	O
les	O
autres	O
jusqu'	O
à	O
la	O
tête	O
...	O
"	O
.	O
Le	O
résultat	O
fut	O
immédiat	O
:	O
des	O
sphinx	O
sont	O
ainsi	O
mis	O
au	O
jour	O
,	O
ainsi	O
qu'	O
une	O
statue	O
du	PERSON
dieu	PERSON
Apis	PERSON
de	PERSON
belle	PERSON
facture	O
.	O
En	O
1857	O
,	O
il	PERSON
revient	PERSON
donc	PERSON
en	PERSON
Égypte	PERSON
,	O
y	O
rencontrant	O
Ferdinand	PERSON
de	PERSON
Lesseps	PERSON
,	O
et	O
ce	O
dernier	O
apprécie	O
la	O
tournure	O
d'	O
esprit	O
de	PERSON
Mariette	PERSON
en	O
ce	LOCATION
qui	LOCATION
concernait	LOCATION
la	LOCATION
destination	O
des	PERSON
antiquités	PERSON
.	O
Le	O
directeur	O
général	PERSON
des	PERSON
antiquités	PERSON
intercepte	PERSON
le	PERSON
convoi	PERSON
fluvial	O
et	PERSON
récupère	PERSON
les	PERSON
caisses	PERSON
,	O
se	PERSON
plaignant	PERSON
auprès	PERSON
de	PERSON
Saïd	PERSON
Pacha	PERSON
,	O
qui	PERSON
conserve	O
deux	PERSON
pièces	PERSON
pour	O
son	O
usage	O
personnel	O
.	O
En	O
1860	O
,	O
il	PERSON
découvre	PERSON
puis	PERSON
travaille	PERSON
au	PERSON
temple	PERSON
d'	PERSON
Edfou	O
qu'	O
il	O
fait	O
désensabler	O
.	O
Mariette	PERSON
s'	PERSON
oppose	O
à	O
la	O
volonté	O
impériale	O
,	O
ce	LOCATION
qui	LOCATION
lui	LOCATION
crée	LOCATION
des	LOCATION
soucis	LOCATION
.	O
En	O
1872	O
,	O
Mariette	PERSON
a	O
2780	O
ouvriers	O
travaillant	O
sous	O
sa	O
direction	O
en	O
Égypte	O
,	O
et	O
,	O
en	O
1878	O
,	O
il	LOCATION
est	LOCATION
reçu	LOCATION
à	O
l'	O
Académie	O
des	O
inscriptions	O
et	O
belles-lettres	O
.	O
En	O
1880	O
,	O
il	O
est	O
rejoint	O
par	O
Gaston	PERSON
Maspero	PERSON
,	O
tandis	O
qu'	O
il	O
tombe	O
à	O
nouveau	O
gravement	O
malade	O
du	O
fait	O
de	O
son	O
diabète	PERSON
.	O
