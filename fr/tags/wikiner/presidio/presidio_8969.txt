Abraham	PERSON
Maslow	PERSON
(	O
1	O
er	O
avril	O
1908	O
--	O
8	O
juin	O
1970	O
)	O
est	O
un	PERSON
psychologue	PERSON
célèbre	PERSON
considéré	PERSON
comme	PERSON
le	O
principal	O
meneur	O
de	O
l'	O
approche	O
humaniste	O
,	O
surtout	PERSON
connu	PERSON
pour	O
son	O
explication	O
de	O
la	O
motivation	O
par	O
la	O
hiérarchie	O
des	O
besoins	O
,	O
qui	O
est	O
souvent	O
représentée	O
par	O
une	O
pyramide	O
des	PERSON
besoins	PERSON
.	O
Pour	O
les	O
psychothérapeutes	O
,	O
c'	O
est	O
l'	O
initiateur	O
de	O
la	O
psychologie	O
humaniste	O
,	O
avec	PERSON
Carl	PERSON
Rogers	PERSON
en	O
particulier	O
.	O
Abraham	PERSON
Maslow	PERSON
est	PERSON
tout	O
cela	PERSON
à	PERSON
la	PERSON
fois	PERSON
.	PERSON
Les	O
premières	O
recherches	O
de	PERSON
Maslow	PERSON
ont	PERSON
concerné	PERSON
le	O
comportement	O
des	O
animaux	O
(	O
chiens	PERSON
,	O
singes	O
)	O
et	O
les	O
déterminants	O
du	O
comportement	O
humain	O
en	O
société	O
.	O
La	O
continuité	O
est	O
claire	O
dans	O
cette	O
démarche	O
qui	O
conduit	O
Maslow	PERSON
de	PERSON
l'	PERSON
analyse	O
des	O
états	O
psychologiques	O
les	O
plus	O
pénibles	O
à	O
l'	O
étude	O
de	O
la	O
motivation	O
puis	PERSON
du	PERSON
sentiment	PERSON
de	PERSON
plénitude	PERSON
,	O
ce	LOCATION
qu'	LOCATION
il	LOCATION
a	O
appelé	O
les	O
"	O
expériences	O
paroxystiques	O
"	O
.	O
On	O
doit	O
en	O
particulier	O
à	O
Abraham	PERSON
Maslow	PERSON
l'	PERSON
élaboration	O
d'	O
un	PERSON
lexique	PERSON
précis	PERSON
,	O
pour	O
aborder	O
la	O
mystique	O
et	O
les	O
états	O
de	O
conscience	O
exceptionnels	O
dans	O
des	O
termes	O
scientifiques	O
,	O
tout	O
en	O
respectant	O
la	O
spécificité	PERSON
de	PERSON
ces	PERSON
expériences	PERSON
.	O
Au	O
cours	O
de	O
sa	O
carrière	O
,	O
Maslow	O
s'	O
est	O
intéressé	O
principalement	O
aux	O
motivations	O
"	O
supérieures	O
"	O
de	O
l'	O
homme	O
dans	O
sa	O
hiérarchie	O
(	O
l'	O
accomplissement	PERSON
de	PERSON
soi	PERSON
)	O
et	O
aux	O
états	O
de	PERSON
plénitude	PERSON
(	O
expériences	O
paroxystiques	O
)	O
,	O
ainsi	O
qu'	O
aux	O
fondements	O
de	O
la	O
santé	O
psychique	O
.	O
La	O
pyramide	O
qui	O
a	O
été	O
attribuée	O
à	O
Maslow	PERSON
représente	PERSON
mal	PERSON
la	PERSON
richesse	PERSON
de	O
son	O
analyse	O
,	O
et	O
surtout	O
trahit	O
la	O
vision	O
dynamique	O
qu'	O
il	PERSON
avait	PERSON
des	PERSON
besoins	PERSON
dans	PERSON
la	PERSON
construction	PERSON
de	PERSON
la	PERSON
personnalité	PERSON
.	O
De	PERSON
plus	PERSON
,	O
si	PERSON
Maslow	PERSON
est	PERSON
très	PERSON
connu	PERSON
dans	PERSON
le	PERSON
domaine	PERSON
du	PERSON
management	PERSON
,	O
ses	O
recherches	O
concernaient	O
la	O
psychologie	O
générale	O
,	O
et	O
ce	O
sont	O
ses	O
successeurs	O
qui	O
ont	O
appliqué	O
ses	O
conclusions	O
à	O
la	O
sphère	O
de	O
l'	O
entreprise	O
.	O
Maslow	PERSON
estime	PERSON
que	PERSON
les	PERSON
besoins	PERSON
élémentaires	O
(	O
physiologiques	O
et	O
de	O
sécurité	O
)	O
étant	O
satisfaits	O
,	O
la	O
personne	O
cherche	O
ensuite	O
à	O
satisfaire	O
les	O
autres	O
besoins	O
d'	O
ordre	O
supérieur	O
de	O
façon	O
à	O
alimenter	O
sans	O
cesse	O
les	O
motivations	O
.	O
Maslow	PERSON
tient	O
finalement	O
un	O
discours	O
optimiste	O
dans	O
la	LOCATION
mesure	LOCATION
où	LOCATION
il	LOCATION
considère	O
qu'	O
il	O
est	O
possible	O
que	O
les	O
salariés	O
puissent	O
,	O
dans	LOCATION
leur	LOCATION
travail	O
,	O
s'	O
accomplir	O
,	O
se	PERSON
réaliser	PERSON
,	O
pourvu	O
que	O
le	O
management	O
soit	PERSON
participatif	PERSON
(	O
cf	O
.	O
théorie	PERSON
de	PERSON
Douglas	PERSON
McGregor	PERSON
)	O
.	O
Maslow	PERSON
propose	O
une	O
étude	O
sociologique	O
de	O
la	O
spiritualité	O
dans	O
laquelle	PERSON
il	PERSON
classifie	PERSON
avec	PERSON
beaucoup	PERSON
de	PERSON
finesse	PERSON
les	O
différentes	O
manifestations	O
paroxystiques	O
,	O
telle	O
que	O
la	O
révélation	O
.	O
Cette	O
démarche	O
n'	O
est	O
pas	O
sans	O
rappeler	O
celle	O
de	O
Stanley	O
Milgram	O
.	O
Ce	O
propos	O
stigmatisant	O
la	O
déformation	O
professionnelle	O
est	O
aussi	O
attribué	O
à	O
Paul	PERSON
Watzlawick	PERSON
.	O
