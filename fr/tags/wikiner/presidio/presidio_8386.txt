En	O
effet	O
,	O
entre-temps	O
les	O
économistes	O
ont	O
montré	O
que	O
le	O
marché	O
du	O
travail	O
était	O
le	O
lieu	O
d'	O
un	O
processus	O
de	O
destruction	O
créatrice	O
à	O
la	O
Joseph	O
Schumpeter	O
et	O
que	O
dans	O
ces	O
conditions	O
il	PERSON
convenait	PERSON
d'	PERSON
être	O
prudent	O
sur	LOCATION
les	O
restrictions	O
portées	O
aux	O
licenciements	O
économiques	O
.	O
Mais	O
début	O
2008	O
,	O
le	O
BIT	O
(	O
bureau	O
international	O
du	O
travail	O
)	O
a	O
définitivement	O
enterré	O
le	O
CNE	O
,	O
le	O
jugeant	O
non	O
compatible	O
avec	O
les	O
règles	O
internationales	O
élémentaires	O
.	O
Pour	O
Etienne	PERSON
Wasmer	PERSON
l'	O
accord	O
peut	PERSON
inciter	PERSON
les	O
parties	O
à	O
se	O
mettre	O
d'	O
accord	O
sur	O
des	O
ruptures	O
conventionnelles	PERSON
et	PERSON
donc	PERSON
à	PERSON
s'	PERSON
entendre	O
au	O
détriment	O
des	O
ASSEDIC	O
(	O
aléa	O
moral	O
)	O
.	O
Pour	O
pallier	O
ce	O
problème	O
,	O
Francis	PERSON
Kramarz	PERSON
propose	O
d'	O
instaurer	O
une	O
bonus-malus	O
pour	O
les	O
entreprises	O
dans	O
le	LOCATION
prolongement	LOCATION
du	LOCATION
rapport	LOCATION
d'	O
Olivier	PERSON
Blanchard	PERSON
et	PERSON
de	PERSON
Jean	PERSON
Tirole	PERSON
.	O
