Delphes	O
est	O
le	O
site	O
d'	O
un	O
important	O
"	O
sanctuaire	O
panhellénique	O
"	O
dans	O
la	O
Grèce	O
antique	O
.	O
Delphes	O
se	O
trouve	PERSON
en	O
Phocide	O
.	O
Les	PERSON
traces	O
les	O
plus	O
anciennes	O
d'	O
une	O
occupation	O
humaine	O
dans	O
la	O
région	O
de	O
Delphes	O
remontent	O
au	O
néolithique	O
.	O
Apollon	PERSON
lui-même	PERSON
aurait	PERSON
fondé	PERSON
le	PERSON
sanctuaire	PERSON
de	PERSON
Delphes	PERSON
après	O
avoir	O
construit	O
le	O
temple	O
de	PERSON
Délos	PERSON
.	O
Le	O
sanctuaire	O
était	O
alors	O
gardé	O
par	O
un	PERSON
serpent	PERSON
nommé	PERSON
"	O
Python	O
"	O
,	O
fils	O
de	O
Gaïa	O
et	O
gardien	O
d'	O
un	PERSON
oracle	PERSON
consacré	PERSON
à	O
Thémis	O
.	O
Apollon	O
,	O
désireux	O
d'	O
établir	O
un	O
oracle	O
pour	O
guider	O
les	O
hommes	O
,	O
tua	PERSON
Python	PERSON
avec	O
son	O
arc	O
et	O
s'	O
appropria	PERSON
l'	O
oracle	O
.	O
Elle	PERSON
se	PERSON
fit	O
ensuite	O
le	O
sept	O
de	O
chaque	O
mois	O
durant	O
la	O
période	O
de	O
neuf	O
mois	O
où	O
Apollon	O
était	O
censé	O
occuper	O
le	O
site	O
:	O
ce	O
jour	O
fut	O
nommé	O
polyphthoos	O
(	O
"	O
jour	O
des	O
multiples	O
questions	O
"	O
)	O
.	O
L'	O
explication	O
qui	O
en	O
a	O
longtemps	O
été	O
donnée	O
était	O
l'	O
inhalation	O
par	O
la	O
prophétesse	O
de	PERSON
vapeurs	PERSON
s'	PERSON
échappant	O
des	O
entrailles	O
de	O
la	O
terre	O
(	O
cause	O
physique	O
)	O
;	O
en	O
réalité	O
,	O
une	O
telle	O
explication	O
est	O
fausse	O
,	O
comme	O
l'	O
ont	O
montré	O
les	O
fouilles	O
des	O
soubassements	O
du	O
temple	O
d'	O
Apollon	PERSON
menées	O
par	O
l'	O
École	O
française	O
d'	O
Athènes	O
:	O
aucune	O
fissure	O
n'	O
a	O
été	O
trouvée	O
et	O
la	O
nature	O
du	PERSON
sol	PERSON
de	PERSON
schiste	PERSON
ne	PERSON
laisse	PERSON
aucune	O
chance	O
à	O
des	O
exhalaisons	O
de	O
gaz	O
.	O
Dans	PERSON
un	PERSON
autre	PERSON
sanctuaire	PERSON
d'	PERSON
Apollon	PERSON
,	O
l'	O
oracle	O
se	O
passait	PERSON
mentalement	PERSON
:	O
celui	O
qui	O
venait	O
consulter	O
l'	O
oracle	PERSON
conversait	PERSON
seul	PERSON
avec	PERSON
le	PERSON
dieu	PERSON
et	O
recevait	O
les	O
réponses	O
à	O
ses	O
questions	O
directement	O
dans	O
son	O
esprit	O
,	O
ce	O
qui	O
autorisait	O
une	O
plus	O
libre	O
interprétation	O
)	O
.	O
Delphes	O
était	O
,	O
selon	O
la	O
mythologie	O
grecque	O
,	O
le	O
centre	O
du	O
monde	O
.	O
Dans	O
la	O
mythologie	O
grecque	O
,	O
en	O
effet	O
,	O
Zeus	PERSON
avait	PERSON
fait	PERSON
partir	PERSON
deux	PERSON
aigles	PERSON
,	O
chacun	O
d'	O
un	O
côté	O
du	O
disque	O
terrestre	O
et	O
ces	O
oiseaux	O
de	O
proie	O
s'	O
étaient	O
rencontrés	O
au	O
centre	O
du	O
monde	O
.	O
Il	O
n'	O
y	O
avait	O
pas	O
d'	O
oracle	O
en	O
l'	O
absence	O
d'	O
Apollon	PERSON
,	O
et	O
de	O
nombreux	O
fidèles	O
attendaient	O
son	O
retour	O
.	O
Il	O
était	O
alors	O
remplacé	O
à	O
Delphes	O
par	O
Dionysos	O
.	O
Ce	O
dernier	O
était	O
présent	O
durant	O
trois	O
mois	O
et	O
faisait	O
l'	O
objet	O
d'	O
un	O
culte	O
rendu	O
sur	O
le	O
Parnasse	O
.	O
Dans	O
l'	O
adyton	O
se	O
trouvait	PERSON
la	PERSON
tombe	PERSON
de	PERSON
Dionysos	PERSON
.	PERSON
À	O
Delphes	O
,	O
le	O
temple	O
d'	O
Apollon	PERSON
revêt	O
une	O
importance	O
particulière	O
,	O
puisqu'	LOCATION
il	LOCATION
abrite	O
l'	O
oracle	O
.	O
Le	O
temple	O
d'	O
Apollon	O
à	O
Delphes	O
est	O
situé	LOCATION
sur	LOCATION
les	LOCATION
flancs	LOCATION
du	LOCATION
mont	O
Parnasse	LOCATION
,	O
sommet	O
qui	PERSON
culmine	PERSON
à	PERSON
2459	O
mètres	O
d'	O
altitude	O
et	O
domine	O
la	O
Grèce	PERSON
centrale	O
.	O
Un	PERSON
peu	PERSON
plus	O
bas	O
,	O
un	O
autre	O
temple	O
est	O
dédié	LOCATION
à	O
Athéna	PERSON
Pronaia	PERSON
,	O
divinité	PERSON
qui	PERSON
"	O
protège	O
"	O
ou	O
"	O
précède	O
"	O
le	O
sanctuaire	O
.	O
Pausanias	PERSON
mentionne	PERSON
que	PERSON
six	O
temples	O
dédiés	O
au	O
dieu	O
Apollon	O
se	O
succédèrent	O
au	O
cours	O
du	O
temps	O
:	O
le	O
premier	O
d'	O
entre	O
eux	O
put	O
être	O
une	O
hutte	O
de	O
laurier	O
.	O
L'	O
archéologie	O
en	O
ignore	O
tout	O
,	O
comme	O
des	O
deux	O
suivants	O
construits	O
par	O
des	O
abeilles	O
du	O
Parnasse	O
avec	O
de	O
la	O
cire	O
et	O
des	O
plumes	O
d'	O
oiseau	O
,	O
pour	O
le	O
second	O
et	O
par	O
Héphaïstos	LOCATION
,	O
en	O
bronze	O
pour	O
le	LOCATION
troisième	LOCATION
.	O
Le	O
site	O
du	O
sanctuaire	O
de	O
Delphes	O
comprend	O
nombre	O
d'	O
autres	O
monuments	O
,	O
qui	O
ont	O
fait	O
considérablement	O
évoluer	O
son	O
aspect	O
entre	PERSON
la	PERSON
période	PERSON
archaïque	PERSON
et	PERSON
l'	PERSON
époque	PERSON
hellénistique	PERSON
:	O
la	O
plupart	O
d'	O
entre	O
eux	O
avaient	O
un	PERSON
caractère	PERSON
votif	PERSON
ou	PERSON
commémoratif	PERSON
.	O
Ces	O
éléments	O
fantastiques	O
sont	O
des	O
images	O
orientales	O
venant	O
de	O
Babylone	O
:	O
elles	O
sont	O
reproduites	O
par	O
les	O
artisans	O
grecs	O
,	O
dans	O
une	O
démarche	O
"	O
orientalisante	O
"	O
.	O
Une	O
statue	O
en	O
ivoire	O
d'	O
une	O
divinité	O
masculine	O
(	O
Apollon	PERSON
?	O
)	O
Particulièrement	O
nombreux	O
à	O
Delphes	O
qui	O
en	O
comptait	O
au	O
moins	O
une	O
vingtaine	O
,	O
des	PERSON
trésors	PERSON
existaient	PERSON
dans	PERSON
d'	PERSON
autres	PERSON
grands	O
sanctuaires	PERSON
grecs	PERSON
,	O
notamment	PERSON
à	PERSON
Olympie	PERSON
.	O
Le	PERSON
trésor	PERSON
de	PERSON
l'	PERSON
île	PERSON
de	PERSON
Siphnos	PERSON
a	O
fait	O
l'	O
objet	O
d'	O
une	O
recherche	O
du	O
meilleur	O
emplacement	O
:	O
il	O
se	O
trouve	PERSON
dans	O
un	PERSON
virage	PERSON
de	PERSON
la	PERSON
montée	PERSON
vers	PERSON
le	PERSON
temple	PERSON
d'	PERSON
Apollon	PERSON
,	O
précédé	LOCATION
de	LOCATION
la	LOCATION
base	O
de	O
Marathon	O
qui	O
supportait	O
les	O
statues	O
des	O
héros	O
éponymes	O
d'	O
Athènes	O
.	O
Il	O
mesure	O
6.5	O
m	O
×	O
9.5	O
m	O
et	O
commémore	O
,	O
selon	O
Pausanias	O
,	O
la	O
victoire	O
de	O
Marathon	O
.	O
Le	O
décor	O
est	O
composé	O
de	O
métopes	O
d'	O
ordre	O
dorique	O
représentant	O
,	O
entre	PERSON
autres	PERSON
,	O
les	O
exploits	O
du	PERSON
demi-dieu	PERSON
Héraclès	PERSON
et	PERSON
de	PERSON
Thésée	PERSON
.	O
Sur	O
la	O
droite	O
,	O
une	O
"	O
héracléide	O
"	O
;	O
à	O
l'	O
arrière	O
,	O
enfin	O
,	O
se	O
trouve	O
la	O
"	O
géryonide	O
"	O
(	O
épisode	O
du	O
mythe	O
d'	O
Héraclès	PERSON
dans	O
lequel	O
le	O
héros	O
ramène	PERSON
les	PERSON
bœufs	PERSON
de	PERSON
Géryon	PERSON
à	PERSON
leur	PERSON
propriétaire	PERSON
)	O
.	O
Plutarque	O
,	O
prêtre	O
à	O
Delphes	O
au	O
second	O
siècle	O
de	O
notre	O
ère	O
,	O
déplore	O
cette	O
surenchère	O
à	O
la	O
gloire	O
des	O
princes	O
qui	PERSON
s'	O
entredéchirèrent	O
à	O
l'	O
époque	O
hellénistique	O
.	O
Dans	O
la	O
partie	O
basse	O
du	O
sanctuaire	O
de	O
Delphes	O
,	O
à	O
gauche	O
de	O
l'	O
entrée	O
,	O
était	LOCATION
présente	LOCATION
une	LOCATION
imposante	PERSON
statuaire	PERSON
commémorative	PERSON
aujourd'hui	PERSON
disparue	PERSON
:	O
celle-ci	O
était	O
répartie	O
en	O
plusieurs	O
ensembles	O
,	O
dressés	O
par	O
les	O
cités	O
rivales	O
au	O
gré	O
des	O
événements	O
.	O
Deux	PERSON
monuments	O
symboliques	PERSON
débutaient	PERSON
cette	O
série	LOCATION
:	O
le	O
monument	O
de	O
Miltiade	O
et	O
le	O
monument	O
de	O
Lysandre	O
.	O
Le	O
monument	O
de	O
Lysandre	O
était	O
constitué	O
d'	O
un	PERSON
socle	PERSON
sur	PERSON
lequel	PERSON
reposait	O
un	O
ensemble	O
de	PERSON
statues	PERSON
en	O
bronze	O
:	O
vingt-huit	O
ou	O
vingt-neuf	O
statues	O
à	O
l'	O
arrière	O
représentaient	O
l'	O
ensemble	O
des	O
hommes	O
qui	O
avaient	O
contribué	O
à	O
la	O
bataille	O
et	O
dix	O
statues	O
à	O
l'	O
avant	O
représentaient	O
les	O
Dioscures	O
:	O
ensemble	O
mythologique	O
réunissant	O
Castor	O
et	O
Pollux	O
,	O
Zeus	PERSON
,	O
Apollon	PERSON
,	O
Artémis	O
,	O
et	O
Poséidon	LOCATION
,	O
représenté	PERSON
couronnant	PERSON
Lysandre	PERSON
,	O
un	PERSON
héraut	PERSON
et	PERSON
le	PERSON
pilote	PERSON
du	PERSON
vaisseau	PERSON
amiral	O
.	O
La	O
répartition	O
des	O
statues	O
est	O
éminemment	O
politique	O
et	O
se	O
veut	O
supérieure	O
à	O
celle	O
du	O
monument	O
de	O
Miltiade	O
,	O
dont	O
le	O
propos	O
est	O
pourtant	O
similaire	O
.	O
