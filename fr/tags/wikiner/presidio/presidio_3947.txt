L'	O
obésité	O
humaine	O
a	O
été	O
reconnue	O
comme	O
une	O
maladie	PERSON
en	O
1997	O
par	O
l'	O
OMS	O
.	O
En	O
2005	O
,	O
d'	O
après	O
les	O
estimations	O
mondiales	O
de	O
l'	O
OMS	O
,	O
il	PERSON
y	PERSON
avait	PERSON
environ	PERSON
:	O
L'	O
OMS	O
estime	O
que	O
d'	O
ici	O
2015	O
,	O
environ	O
2,3	O
milliards	O
d'	O
adultes	O
seront	O
en	O
surpoids	O
et	O
plus	O
de	O
700	O
millions	O
seront	O
obèses	O
.	O
La	O
proportion	O
d'	O
obèses	O
tend	O
à	O
se	O
stabiliser	O
depuis	O
la	O
fin	O
des	O
années	O
2000	O
aux	O
États-Unis	O
.	O
En	LOCATION
France	LOCATION
,	O
en	O
1965	O
,	O
seuls	LOCATION
3	O
%	O
des	O
enfants	O
d'	O
âge	O
scolaire	O
étaient	O
obèses	O
selon	O
l'	O
IMC	O
;	O
ils	O
étaient	O
13,3	O
%	O
en	O
2000	O
,	O
26	O
%	O
au	O
Canada	O
et	O
16	O
%	O
aux	O
États-Unis	O
.	O
Le	O
Mexique	O
est	O
le	O
deuxième	O
pays	O
du	O
monde	O
pour	O
la	O
part	O
d'	O
obèses	O
dans	O
la	O
population	O
,	O
juste	PERSON
derrière	PERSON
les	PERSON
États-Unis	PERSON
.	O
L'	O
obésité	O
touche	O
30	O
%	O
des	O
adultes	O
,	O
soit	O
44	O
millions	O
de	PERSON
Mexicains	PERSON
,	O
et	O
40	O
%	O
connaissent	O
un	O
poids	O
excessif	O
.	O
Par	O
exemple	O
,	O
en	O
Mauritanie	O
,	O
les	O
jeunes	O
filles	O
en	O
âge	O
de	O
se	O
marier	O
sont	LOCATION
engraissées	O
afin	O
d'	O
être	O
plus	O
séduisantes	O
et	O
de	PERSON
maximiser	PERSON
leur	PERSON
chance	O
de	O
trouver	O
un	O
conjoint	O
.	O
En	O
1992	O
,	O
l'	O
obésité	O
a	O
été	O
la	O
cause	O
estimée	PERSON
de	PERSON
55000	PERSON
décès	PERSON
en	O
France	LOCATION
,	O
essentiellement	O
par	O
maladies	O
cardio-vasculaires	O
et	O
diabète	LOCATION
.	O
Par	O
ailleurs	O
,	O
du	O
fait	O
des	O
complications	O
du	LOCATION
diabète	LOCATION
,	O
l'	O
obésité	O
est	O
la	O
première	O
cause	O
de	O
cécité	O
avant	O
65	O
ans	O
en	O
France	LOCATION
,	O
et	O
la	O
première	O
cause	O
d'	O
amputation	O
.	O
Ce	O
thème	O
est	O
notamment	O
développé	O
par	O
l'	O
historien	O
britannique	O
Niall	PERSON
Ferguson	PERSON
qui	PERSON
se	PERSON
réfère	O
aux	O
conclusions	O
classiques	O
de	O
l'	O
historien	O
britannique	O
Edward	PERSON
Gibbon	PERSON
sur	O
la	O
décadence	O
physique	O
des	O
citoyens	O
à	O
la	O
fin	O
de	O
l'	O
empire	O
romain	O
.	O
La	O
question	O
de	O
l'	O
obésité	PERSON
comme	PERSON
signe	O
des	PERSON
déclin	PERSON
des	PERSON
États-Unis	PERSON
est	PERSON
également	PERSON
mis	O
en	O
avant	O
par	O
des	O
géopolitologues	O
,	O
tel	O
le	O
français	O
Dominique	PERSON
Moïsi	PERSON
qui	PERSON
cite	O
en	O
2008	O
l'	O
obésité	O
parmi	O
les	O
signes	O
de	PERSON
recul	PERSON
des	PERSON
États-Unis	PERSON
:	O
"	O
L'	O
évolution	O
de	O
leur	O
corps	O
,	O
avec	LOCATION
le	LOCATION
nombre	LOCATION
toujours	LOCATION
plus	O
grand	O
d'	O
obèses	O
,	O
l'	O
approfondissement	O
de	PERSON
leur	PERSON
endettement	PERSON
,	O
le	O
manque	O
d'	O
appétence	O
des	O
soldats	O
américains	PERSON
pour	O
des	O
aventures	O
extérieures	O
sont	O
autant	O
de	PERSON
symboles	PERSON
de	PERSON
ce	PERSON
qui	PERSON
pourrait	PERSON
s'	PERSON
apparenter	O
à	O
un	O
déclin.	O
"	O
.	O
En	O
France	O
,	O
une	O
campagne	O
de	O
sensibilisation	O
lancée	O
en	O
2002	O
incite	O
les	O
gens	O
à	O
manger	O
au	O
moins	O
cinq	O
fruits	O
et	O
légumes	O
par	O
jour	O
et	O
à	O
pratiquer	O
l'	O
équivalent	O
d'	O
une	O
1/2	O
heure	O
de	O
marche	O
par	O
jour	O
.	O
Concernant	O
la	O
suppression	O
de	O
la	O
publicité	O
,	O
celle-ci	O
n'	O
est	O
toujours	O
pas	O
à	O
l'	O
ordre	O
du	O
jour	O
,	O
la	PERSON
loi	PERSON
dite	PERSON
"	PERSON
Hôpital	PERSON
,	O
patients	O
,	O
santé	PERSON
et	O
territoire	O
"	O
,	O
promulguée	O
en	O
2009	O
,	O
en	O
ayant	O
rejeté	PERSON
le	PERSON
principe	PERSON
.	O
