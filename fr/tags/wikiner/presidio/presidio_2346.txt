Sa	PERSON
capitale	PERSON
est	O
Atlanta	LOCATION
.	O
La	LOCATION
culture	O
locale	O
des	O
Mound	O
Builders	O
,	O
décrite	O
par	O
Hernando	PERSON
de	PERSON
Soto	PERSON
en	O
1540	O
,	O
a	O
complètement	O
disparu	O
en	O
1560	O
.	O
Après	PERSON
cette	O
date	O
,	O
l'	O
Espagne	PERSON
ne	PERSON
contrôle	PERSON
plus	O
que	O
St.	O
Augustine	O
et	O
Pensacola	LOCATION
;	O
mais	O
la	O
Floride	O
subit	O
aussi	PERSON
des	PERSON
raids	PERSON
par	O
la	O
suite	O
.	O
La	LOCATION
Géorgie	LOCATION
importa	O
de	O
nombreux	O
esclaves	O
de	O
différentes	O
ethnies	O
africaines	O
pour	O
ses	O
plantations	O
de	O
riz	O
puis	O
de	O
coton	O
.	O
Whitney	O
met	O
au	O
point	O
une	O
machine	O
à	O
trier	O
les	O
fibres	O
de	O
coton	O
des	O
semences	O
,	O
dans	O
le	PERSON
comté	PERSON
de	PERSON
Chatham	PERSON
,	O
le	O
long	O
du	O
fleuve	O
Savannah	O
.	O
En	O
1823	O
,	O
une	O
loterie	O
aboutit	O
à	O
la	O
distribution	O
de	PERSON
terres	PERSON
puis	PERSON
en	PERSON
dix	PERSON
ans	PERSON
à	O
l'	O
installation	O
de	O
3000	O
familles	O
produisant	O
69000	O
balles	O
de	O
coton	O
,	O
dans	O
ce	O
qui	O
est	O
devenu	O
la	O
ville	O
de	O
Macon	O
.	O
Une	O
loterie	O
est	O
ouverte	LOCATION
dès	O
1805	O
dans	O
le	PERSON
comté	PERSON
de	PERSON
Morgan	PERSON
,	O
qui	O
compte	O
en	O
1820	O
un	O
population	O
de	O
13000	O
habitants	O
dont	O
6000	O
noirs	O
.	O
En	O
décembre	O
1864	O
le	O
centre	O
industriel	O
et	O
ferroviaire	O
d'	O
Atlanta	LOCATION
est	O
complètement	O
détruit	O
par	O
l'	O
armée	O
nordiste	O
du	O
général	O
William	PERSON
Tecumseh	PERSON
Sherman	PERSON
.	O
D'	O
une	O
superficie	O
de	O
152577	O
km²	O
,	O
la	O
Géorgie	PERSON
est	PERSON
peuplée	PERSON
de	PERSON
9,5	O
millions	O
d'	O
habitants	O
selon	PERSON
les	PERSON
dernières	PERSON
estimations	PERSON
de	O
2007	O
.	O
De	PERSON
nombreux	PERSON
cours	PERSON
d'	PERSON
eau	O
traversent	O
la	O
Géorgie	O
;	O
on	O
trouve	O
:	O
Selon	O
les	O
dernières	O
estimations	O
du	O
Bureau	O
du	O
Recensement	O
des	O
États-Unis	O
(	O
2008	O
)	O
,	O
les	O
douze	O
communes	O
les	O
plus	O
peuplées	LOCATION
sont	O
:	O
La	PERSON
Géorgie	PERSON
est	PERSON
divisée	PERSON
en	O
159	O
comtés	O
.	O
La	O
croissance	O
démographique	O
de	O
la	O
Géorgie	O
a	O
été	O
très	O
dynamique	O
depuis	O
les	O
années	O
1960	O
,	O
notamment	O
dans	O
l'	O
agglomération	O
d'	O
Atlanta	LOCATION
.	O
Selon	O
les	O
estimations	O
du	O
Bureau	O
de	O
recensement	O
des	O
États-Unis	O
,	O
la	O
population	O
de	PERSON
la	PERSON
Géorgie	PERSON
en	O
2007	O
était	O
de	O
9,5	O
millions	O
d'	O
habitants	O
(	O
par	O
rapport	O
à	O
8,1	O
millions	O
d'	O
hab	PERSON
.	O
Comme	O
beaucoup	O
d'	O
anciens	O
États	O
du	O
sud	O
,	O
la	O
Géorgie	PERSON
a	O
vécu	PERSON
un	PERSON
régime	PERSON
de	PERSON
parti	O
unique	O
pendant	O
une	O
centaine	O
d'	O
années	O
.	O
Les	PERSON
lois	PERSON
sur	PERSON
les	PERSON
droits	PERSON
civiques	PERSON
dans	PERSON
les	O
années	O
1960	O
ont	O
commencé	O
à	O
entamer	O
cette	O
prépondérance	O
démocrate	O
qui	PERSON
aboutit	PERSON
aux	PERSON
premières	PERSON
victoires	O
présidentielles	PERSON
des	O
candidats	O
républicains	O
(	O
Richard	PERSON
Nixon	PERSON
en	O
1972	O
,	O
Ronald	PERSON
Reagan	PERSON
en	O
1984	O
,	O
George	PERSON
W.	PERSON
Bush	PERSON
en	O
2000	O
et	O
2004	O
)	O
puis	O
au	O
triomphe	O
républicain	O
des	O
élections	O
de	O
novembre	O
2002	O
.	O
L'	O
assemblée	O
générale	O
de	O
Géorgie	O
est	O
composée	O
d'	O
un	O
sénat	O
(	O
56	O
membres	O
dont	O
34	O
républicains	O
,	O
élus	O
pour	O
2	O
ans	O
)	O
et	O
d'	O
une	O
chambre	O
des	O
représentants	O
(	O
180	O
membres	O
dont	O
106	O
républicains	O
,	O
élus	O
pour	O
2	O
ans	O
)	O
,	O
lesquels	O
sont	O
dominés	O
depuis	O
2002	O
par	O
les	O
républicains	O
.	O
Au	O
niveau	O
fédéral	O
,	O
les	O
deux	O
sénateurs	O
sont	O
pour	O
la	O
première	O
fois	O
tous	O
deux	O
républicains	O
:	O
Saxby	PERSON
Chambliss	PERSON
et	O
Johnny	PERSON
Isakson	PERSON
(	O
élu	O
en	O
2004	O
,	O
soutenu	O
par	O
le	O
sortant	O
démocrate	O
,	O
Zell	PERSON
Miller	PERSON
)	O
.	O
La	PERSON
Géorgie	PERSON
a	O
vu	O
naître	O
un	O
grand	O
nombre	O
de	O
grands	O
noms	PERSON
du	PERSON
cinéma	PERSON
comme	PERSON
Kim	PERSON
Basinger	PERSON
,	O
Julia	PERSON
Roberts	PERSON
,	O
Laurence	PERSON
Fishburne	PERSON
,	O
Spike	PERSON
Lee	PERSON
,	O
Steven	PERSON
Soderbergh	PERSON
ou	O
encore	O
Hulk	PERSON
Hogan	PERSON
.	O
