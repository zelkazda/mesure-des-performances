Il	O
passa	O
les	O
six	O
premières	O
années	O
de	O
sa	O
vie	O
en	O
compagnie	O
de	O
ses	O
parents	O
à	O
Port	LOCATION
Saïd	LOCATION
,	O
en	O
Égypte	O
.	O
Son	O
roman	O
Last	O
and	O
First	O
Men	O
(	O
Les	O
premiers	O
et	O
les	O
derniers	O
)	O
connut	O
un	O
grand	O
succès	O
,	O
ce	LOCATION
qui	LOCATION
le	LOCATION
convainquit	LOCATION
de	LOCATION
se	LOCATION
consacrer	O
pleinement	O
à	O
l'	O
écriture	O
.	O
Après	O
1945	O
,	O
Stapledon	PERSON
fit	O
de	O
grandes	O
tournées	O
pour	O
présenter	PERSON
son	PERSON
œuvre	PERSON
,	O
visitant	O
les	O
Pays-Bas	O
,	O
la	O
Suède	LOCATION
et	O
la	O
France	LOCATION
.	O
Son	PERSON
œuvre	PERSON
influença	PERSON
directement	PERSON
des	PERSON
auteurs	O
comme	O
Arthur	PERSON
C.	PERSON
Clarke	PERSON
,	O
Brian	PERSON
Aldiss	PERSON
,	O
Stanisław	PERSON
Lem	PERSON
et	O
John	PERSON
Maynard	PERSON
Smith	PERSON
et	O
indirectement	PERSON
un	PERSON
grand	PERSON
nombre	PERSON
d'	PERSON
autres	PERSON
auteurs	PERSON
,	O
fournissant	O
une	O
large	O
contribution	O
d'	O
idées	O
nouvelles	O
au	O
genre	O
de	O
la	O
science-fiction	O
(	O
la	O
plupart	O
d'	O
entre	O
elles	O
étant	O
inspirées	O
de	O
lectures	O
philosophiques	O
)	O
.	O
Son	O
idée	O
d'	O
empires	O
englobant	O
plusieurs	O
galaxies	O
inspira	O
de	O
nombreux	O
auteurs	O
de	O
science-fiction	O
comme	O
Edward	PERSON
Elmer	PERSON
Smith	PERSON
,	O
Alfred	PERSON
Elton	PERSON
van	PERSON
Vogt	PERSON
ou	PERSON
Isaac	PERSON
Asimov	PERSON
.	O
Freeman	PERSON
Dyson	PERSON
déclara	O
que	O
ce	LOCATION
fut	LOCATION
le	LOCATION
roman	O
qui	O
lui	O
en	O
fournit	O
l'	O
idée	O
première	O
.	O
Les	O
derniers	O
et	O
les	O
premiers	O
(	O
Last	O
and	O
First	O
Men	O
)	O
propose	O
également	O
une	O
description	O
d'	O
ingénierie	PERSON
génétique	O
et	O
de	O
terraformation	O
.	O
Les	PERSON
romans	PERSON
Les	PERSON
derniers	PERSON
et	PERSON
les	PERSON
premiers	O
(	O
Last	O
and	O
First	O
Men	O
,	O
une	O
histoire	O
anticipée	PERSON
de	PERSON
l'	PERSON
humanité	O
)	O
et	O
Créateur	O
d'	O
étoiles	O
(	O
Star	O
Maker	O
,	O
une	O
histoire	O
esquissée	O
de	O
l'	O
univers	O
)	O
en	O
particulier	PERSON
furent	PERSON
encensés	PERSON
par	PERSON
des	PERSON
personnages	PERSON
aussi	PERSON
divers	O
que	O
J.	PERSON
B.	PERSON
Priestley	PERSON
,	O
Virginia	PERSON
Woolf	PERSON
et	O
Winston	PERSON
Churchill	PERSON
.	O
En	O
fait	O
,	O
Stapledon	O
était	O
agnostique	O
et	O
hostile	O
aux	O
institutions	O
religieuses	O
,	O
mais	PERSON
non	O
aux	O
aspirations	O
religieuses	O
,	O
ce	PERSON
qui	PERSON
lui	PERSON
valut	PERSON
quelques	PERSON
différends	PERSON
avec	PERSON
H.	PERSON
G.	PERSON
Wells	PERSON
tout	O
au	O
long	O
de	O
leur	O
correspondance	O
.	O
