Quatre	O
communes	O
,	O
dont	O
Seveso	LOCATION
,	O
sont	O
touchées	O
.	O
On	O
sait	O
en	O
revanche	O
que	O
l'	O
une	O
des	O
substances	O
libérée	O
est	O
composante	O
des	O
défoliants	O
utilisés	O
au	O
Viêt	PERSON
Nam	PERSON
par	O
l'	O
armée	O
américaine	O
.	O
Il	O
n'	O
en	O
faut	O
pas	O
plus	O
pour	O
faire	O
basculer	O
Seveso	PERSON
de	PERSON
"	O
catastrophe	O
environnementale	O
"	O
à	O
"	O
la	O
plus	O
grande	O
catastrophe	O
depuis	O
Hiroshima	LOCATION
"	O
.	O
La	PERSON
seule	PERSON
victime	O
indirecte	PERSON
fut	PERSON
le	PERSON
directeur	O
de	O
l'	O
usine	O
qui	O
a	O
été	O
assassiné	O
par	O
les	O
Brigades	O
rouges	O
.	O
En	O
2001	O
,	O
l'	O
usine	O
AZF	O
classée	O
seveso	O
a	O
explosé	O
à	O
Toulouse	LOCATION
.	O
En	O
août	O
1982	O
,	O
les	O
déchets	O
chimiques	O
,	O
contenant	O
de	O
la	O
dioxine	O
,	O
sont	O
enlevés	O
du	O
réacteur	O
,	O
en	O
vue	O
du	O
démantèlement	O
des	O
installations	O
,	O
et	O
transférés	O
dans	O
41	O
fûts	O
pour	O
être	O
envoyés	O
par	O
route	O
à	O
l'	O
usine	O
Ciba	O
de	O
Bâle	O
afin	O
de	O
les	O
incinérer	O
.	O
Mais	PERSON
curieusement	PERSON
leur	PERSON
trace	O
se	PERSON
perd	PERSON
après	PERSON
le	PERSON
passage	PERSON
de	PERSON
la	PERSON
frontière	PERSON
à	PERSON
Vintimille	PERSON
et	PERSON
ils	PERSON
disparaissent	PERSON
quelque	PERSON
part	PERSON
en	O
France	LOCATION
.	O
On	O
les	PERSON
découvrira	PERSON
en	O
mai	O
1983	O
à	O
Anguilcourt-le-Sart	O
(	O
Aisne	O
)	O
dans	O
un	O
abattoir	O
désaffecté	O
,	O
où	O
ils	O
avaient	O
été	O
transportés	O
illégalement	O
.	O
Ils	O
seront	O
finalement	O
incinérés	O
chez	O
Ciba	O
en	O
novembre	O
1985	O
.	O
Une	O
série	O
avec	O
des	O
déchets	O
industriels	O
venant	O
de	O
l'	O
usine	O
Icmesa	O
non-contaminés	O
,	O
et	O
l'	O
autre	O
série	O
avec	O
les	O
déchets	O
toxiques	O
.	O
Et	O
c'	O
est	O
la	O
première	O
série	O
de	O
fûts	O
(	O
non-contaminés	O
)	O
qui	O
auraient	O
été	O
incinérés	O
à	O
Bâle	O
devant	O
la	O
presse	O
.	O
L'	O
autre	O
série	O
de	O
fûts	O
contenant	O
les	O
déchets	O
de	O
dioxine	O
du	O
réacteur	O
auraient	O
eux	O
été	O
envoyés	O
par	O
route	O
en	O
Allemagne	O
de	O
l'	O
Est	O
.	O
