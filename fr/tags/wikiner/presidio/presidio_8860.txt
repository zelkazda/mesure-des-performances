Paul-Michel	PERSON
Foucault	PERSON
est	PERSON
né	PERSON
en	PERSON
1926	O
à	O
Poitiers	O
,	O
dans	O
une	O
famille	O
de	O
notables	O
de	O
province	O
.	O
Il	PERSON
abandonne	PERSON
plus	PERSON
tard	O
le	O
"	O
Paul	O
"	O
de	O
son	O
prénom	O
,	O
pour	O
des	O
raisons	O
qui	O
demeurent	O
toujours	O
inconnues	O
;	O
on	O
a	O
avancé	O
que	O
ce	O
pouvait	O
être	O
pour	O
se	O
démarquer	PERSON
de	PERSON
son	PERSON
père	PERSON
;	O
mais	PERSON
il	PERSON
semble	O
aussi	PERSON
que	PERSON
ce	PERSON
soit	PERSON
par	PERSON
agacement	O
car	O
à	O
l'	O
époque	O
ses	O
initiales	O
étaient	O
celles	O
par	O
lesquelles	O
on	O
désignait	O
Pierre	PERSON
Mendès	PERSON
France	LOCATION
.	O
Il	PERSON
échoue	PERSON
une	PERSON
première	O
fois	O
au	O
concours	O
d'	O
entrée	O
à	O
l'	O
École	O
normale	O
supérieure	O
;	O
il	LOCATION
est	LOCATION
finalement	LOCATION
reçu	LOCATION
en	O
1946	O
.	O
La	O
vie	O
quotidienne	O
de	O
Foucault	O
à	O
l'	O
École	O
normale	O
est	O
difficile	O
et	O
mouvementée	O
;	O
il	PERSON
souffre	PERSON
de	PERSON
dépression	PERSON
grave	PERSON
,	O
marquée	PERSON
par	PERSON
deux	PERSON
tentatives	PERSON
de	PERSON
suicide	PERSON
,	O
en	O
1948	O
et	O
en	O
1950	O
.	O
Il	O
participe	O
alors	O
très	O
vite	O
à	O
la	O
branche	O
clinique	PERSON
de	PERSON
cette	PERSON
discipline	O
où	LOCATION
il	LOCATION
est	LOCATION
amené	O
à	O
côtoyer	O
différentes	O
personnalités	O
,	O
dont	O
Ludwig	PERSON
Binswanger	PERSON
.	O
Comme	O
de	O
nombreux	O
autres	O
normaliens	O
de	O
cette	O
époque	O
,	O
Foucault	O
adhère	O
au	O
Parti	O
communiste	O
français	O
,	O
mais	O
pour	O
une	O
courte	O
période	O
seulement	O
,	O
de	O
1950	O
à	O
1953	O
.	O
Il	O
y	O
adhère	O
suivant	O
ainsi	O
les	O
pas	O
de	O
son	O
mentor	O
de	PERSON
l'	PERSON
époque	PERSON
,	O
Louis	PERSON
Althusser	PERSON
.	O
À	O
l'	O
inverse	O
de	O
la	O
plupart	O
des	O
membres	O
du	O
parti	O
,	O
Foucault	PERSON
ne	PERSON
participa	PERSON
jamais	PERSON
très	PERSON
activement	PERSON
à	PERSON
sa	PERSON
cellule	O
.	O
Tout	LOCATION
en	O
occupant	O
un	O
poste	O
de	O
répétiteur	O
à	O
l'	O
École	O
normale	O
supérieure	O
,	O
il	O
accepte	O
un	O
poste	O
d'	O
assistant	O
à	O
l'	O
Université	O
de	O
Lille	O
,	O
où	O
de	O
1953	O
à	O
1954	O
il	PERSON
enseigne	PERSON
la	PERSON
psychologie	PERSON
.	O
C'	O
est	O
à	O
cette	O
époque	O
qu'	O
il	O
se	O
lie	O
avec	O
le	O
compositeur	O
Jean	PERSON
Barraqué	PERSON
.	O
La	O
même	O
année	O
,	O
il	PERSON
accepte	O
donc	PERSON
un	PERSON
poste	O
à	O
l'	O
Université	O
d'	O
Uppsala	LOCATION
en	O
Suède	LOCATION
en	O
tant	O
que	O
conseiller	O
culturel	O
,	O
position	O
qui	PERSON
fut	PERSON
arrangée	PERSON
pour	PERSON
lui	PERSON
par	O
Georges	PERSON
Dumézil	PERSON
;	O
celui-ci	PERSON
devint	O
par	O
la	O
suite	O
un	PERSON
ami	PERSON
et	O
mentor	O
.	O
C'	O
est	O
fin	O
1958	O
qu'	O
il	O
quitte	O
la	O
Suède	O
pour	O
Varsovie	O
.	O
Il	O
y	O
est	O
chargé	O
de	O
la	O
réouverture	O
du	O
Centre	O
de	O
civilisation	O
française	O
.	O
En	O
1959	O
,	O
il	O
finit	O
par	O
être	O
inquiété	O
par	O
la	O
police	O
de	PERSON
Gomulka	PERSON
qui	PERSON
s'	PERSON
alarme	O
de	O
ses	O
travaux	O
et	O
fréquentations	O
,	O
et	O
qui	O
exige	O
son	O
départ	PERSON
.	O
Foucault	PERSON
retourne	PERSON
en	O
France	LOCATION
en	O
1960	O
pour	O
finir	O
sa	PERSON
thèse	PERSON
et	O
occuper	O
un	O
poste	O
de	O
philosophie	O
à	O
l'	O
Université	O
de	O
Clermont-Ferrand	O
,	O
à	O
l'	O
invitation	O
de	PERSON
Jules	PERSON
Vuillemin	PERSON
,	O
directeur	O
du	PERSON
département	PERSON
de	PERSON
philosophie	PERSON
;	O
les	O
deux	O
hommes	O
se	O
lièrent	O
d'	O
une	O
amitié	O
durable	O
.	O
Il	PERSON
a	O
pour	O
collègue	PERSON
Michel	PERSON
Serres	PERSON
.	PERSON
C'	O
est	O
là	PERSON
aussi	PERSON
que	PERSON
Foucault	PERSON
rencontra	PERSON
Daniel	PERSON
Defert	PERSON
,	O
qui	O
resta	O
son	O
compagnon	O
jusqu'	PERSON
à	O
la	O
fin	O
de	O
ses	O
jours	O
.	O
Au	O
début	O
de	O
cette	O
année	O
il	O
est	O
entré	O
avec	O
Roland	PERSON
Barthes	PERSON
et	O
Michel	PERSON
Deguy	PERSON
au	O
premier	O
"	O
conseil	O
de	O
rédaction	O
"	O
de	O
la	O
revue	O
Critique	O
auprès	O
de	O
Jean	O
Piel	O
qui	O
reprend	O
la	O
direction	O
de	O
la	O
revue	O
après	PERSON
la	PERSON
mort	PERSON
de	PERSON
Georges	PERSON
Bataille	PERSON
.	O
Suite	O
à	O
l'	O
affectation	O
de	PERSON
Defert	PERSON
en	O
Tunisie	LOCATION
pour	O
la	O
durée	O
de	O
son	O
service	O
militaire	O
,	O
Foucault	O
s'	O
y	O
installe	PERSON
lui	PERSON
aussi	PERSON
et	PERSON
prend	PERSON
un	PERSON
poste	O
à	O
l'	O
Université	O
de	O
Tunis	O
en	O
1965	O
.	O
En	O
1966	O
il	O
publie	O
Les	PERSON
Mots	PERSON
et	O
les	O
Choses	O
,	O
qui	O
connaît	O
immédiatement	O
un	O
immense	O
succès	O
.	O
À	O
l'	O
époque	O
,	O
l'	O
engouement	O
pour	O
le	O
structuralisme	O
est	O
à	O
son	O
paroxysme	PERSON
,	O
et	O
Foucault	PERSON
se	PERSON
retrouve	PERSON
très	PERSON
rapidement	PERSON
rattaché	PERSON
à	PERSON
des	PERSON
chercheurs	PERSON
et	O
philosophes	O
tels	O
que	O
Jacques	PERSON
Derrida	PERSON
,	O
Claude	PERSON
Lévi-Strauss	PERSON
et	O
Roland	PERSON
Barthes	PERSON
,	O
alors	PERSON
perçus	PERSON
comme	PERSON
la	PERSON
nouvelle	PERSON
vague	PERSON
de	PERSON
penseurs	PERSON
prêts	PERSON
à	O
renverser	O
l'	O
existentialisme	O
et	O
l'	O
intellectuel	PERSON
total	PERSON
incarné	PERSON
par	O
Jean-Paul	PERSON
Sartre	PERSON
.	O
Nombre	PERSON
des	PERSON
débats	PERSON
,	O
échanges	O
et	O
interviews	O
impliquant	O
Foucault	PERSON
se	PERSON
font	O
alors	O
les	O
échos	O
de	O
l'	O
opposition	O
entre	O
l'	O
humanisme	O
,	O
et	O
de	O
son	O
affranchissement	PERSON
par	PERSON
l'	O
étude	O
des	O
systèmes	O
et	O
de	O
leurs	O
structures	O
.	O
Cependant	PERSON
Foucault	PERSON
se	PERSON
lassa	PERSON
bien	PERSON
vite	PERSON
de	PERSON
cette	PERSON
étiquette	O
de	O
"	O
structuraliste	O
"	O
.	O
Foucault	PERSON
se	PERSON
trouve	PERSON
toujours	PERSON
à	O
Tunis	O
pendant	O
les	O
événements	O
de	O
mai	O
1968	O
,	O
où	LOCATION
il	LOCATION
est	LOCATION
très	LOCATION
profondément	O
ému	O
par	O
la	O
révolte	O
des	O
étudiants	O
tunisiens	LOCATION
,	O
la	LOCATION
même	LOCATION
année	LOCATION
.	LOCATION
À	O
l'	O
automne	O
1968	O
,	O
il	O
revient	O
en	O
France	LOCATION
et	O
publie	O
L'	O
Archéologie	O
du	O
savoir	O
,	O
une	O
réponse	O
à	O
ses	O
critiques	O
,	O
en	O
1969	O
.	O
Dès	PERSON
la	PERSON
fin	PERSON
des	PERSON
événements	PERSON
de	PERSON
1968	PERSON
le	PERSON
gouvernement	PERSON
décide	PERSON
de	PERSON
la	PERSON
création	PERSON
d'	PERSON
une	O
université	O
expérimentale	O
à	O
Vincennes	LOCATION
.	O
Foucault	O
y	O
prend	O
la	O
direction	O
du	PERSON
département	PERSON
de	PERSON
philosophie	PERSON
.	O
Foucault	O
se	O
joint	O
alors	O
aux	O
étudiants	O
qui	O
en	O
représailles	O
occupent	O
les	O
bâtiments	O
administratifs	O
du	O
campus	O
,	O
et	O
affrontent	O
la	O
police	O
.	O
Il	O
participe	O
aussi	O
,	O
de	PERSON
même	PERSON
que	PERSON
Jean-Paul	PERSON
Sartre	PERSON
,	O
aux	O
premières	O
manifestations	O
en	O
soutien	O
des	O
travailleurs	O
immigrés	O
.	O
C'	O
est	O
durant	O
cette	O
période	O
que	O
Foucault	O
se	O
met	O
à	O
l'	O
écriture	O
d'	O
un	O
projet	O
d	O
'	O
Histoire	O
de	O
la	O
sexualité	O
dont	O
il	PERSON
publiera	PERSON
trois	PERSON
volumes	PERSON
,	O
au	LOCATION
lieu	O
des	O
six	O
initialement	O
prévus	O
.	O
Le	O
premier	O
volume	O
de	O
cette	O
étude	O
,	O
La	O
Volonté	O
de	O
savoir	O
,	O
paraît	O
en	O
1976	O
.	O
Les	O
deuxième	O
et	O
troisième	O
volumes	O
,	O
L'	O
Usage	O
des	O
plaisirs	O
et	O
Le	O
Souci	O
de	O
soi	O
ne	O
parurent	O
que	O
huit	O
ans	O
plus	O
tard	O
,	O
et	O
surprennent	O
les	O
lecteurs	O
par	O
leur	O
style	O
relativement	O
traditionnel	O
,	O
leur	O
sujet	O
(	O
les	O
textes	O
classiques	O
latin	O
et	PERSON
grecs	PERSON
)	O
et	O
leur	O
approche	O
,	O
en	O
particulier	O
l'	O
attention	O
que	O
Foucault	O
porte	O
au	O
sujet	O
,	O
concept	O
qu'	O
il	O
avait	O
jusqu'	O
alors	O
négligé	O
.	O
Foucault	O
passe	O
alors	O
de	O
plus	O
en	O
plus	O
de	O
temps	O
aux	O
États-Unis	O
,	O
à	O
SUNY	O
Buffalo	O
(	O
où	O
il	O
avait	O
donné	O
une	O
conférence	O
lors	O
de	O
sa	O
première	O
visite	O
aux	O
États-Unis	O
en	O
1970	O
)	O
,	O
et	O
plus	O
précisément	O
à	O
l'	O
université	O
de	O
Berkeley	O
où	O
les	O
étudiants	O
assistent	O
en	O
très	O
grand	O
nombre	O
à	O
ses	O
conférences	O
.	O
À	O
son	O
retour	PERSON
il	PERSON
rédigea	PERSON
plusieurs	PERSON
articles	O
enthousiastes	O
quant	O
à	O
la	O
Révolution	O
iranienne	O
;	O
une	O
chaude	O
polémique	O
s'	O
ensuivit	O
.	O
Il	O
est	O
hospitalisé	O
à	O
Paris	LOCATION
début	O
juin	O
1984	O
,	O
et	O
meurt	O
le	O
25	O
,	O
d'	O
une	O
maladie	O
opportuniste	O
liée	PERSON
au	O
virus	O
VIH	O
.	O
Ce	O
sont	O
d'	O
ailleurs	O
les	O
mensonges	O
et	O
les	O
malentendus	O
autour	O
de	O
sa	O
mort	O
qui	O
ont	O
poussé	O
Daniel	PERSON
Defert	PERSON
à	O
créer	O
la	O
première	O
association	O
française	O
de	O
lutte	O
contre	O
le	PERSON
sida	PERSON
,	O
Aides	O
.	O
Dans	PERSON
son	PERSON
livre	PERSON
À	PERSON
l'	PERSON
ami	PERSON
qui	PERSON
ne	PERSON
m'	PERSON
a	O
pas	O
sauvé	O
la	O
vie	O
,	O
Hervé	PERSON
Guibert	PERSON
,	O
un	PERSON
des	PERSON
amis	PERSON
de	PERSON
Michel	PERSON
Foucault	PERSON
,	O
y	O
évoquera	O
sa	PERSON
maladie	PERSON
,	O
sa	PERSON
mort	PERSON
et	O
son	O
refus	O
de	O
publications	O
posthumes	O
.	O
Foucault	O
est	O
généralement	O
connu	O
pour	O
ses	O
critiques	O
des	O
institutions	O
sociales	O
,	O
principalement	O
la	PERSON
psychiatrie	PERSON
,	O
la	O
médecine	O
,	O
le	O
système	O
carcéral	O
,	O
et	O
pour	O
ses	O
idées	O
et	O
développements	O
sur	O
l'	O
histoire	O
de	O
la	O
sexualité	O
,	O
ses	O
théories	O
générales	O
concernant	O
le	O
pouvoir	O
et	O
les	O
relations	O
complexes	O
entre	O
pouvoir	O
et	O
connaissance	O
,	O
aussi	PERSON
bien	PERSON
que	PERSON
pour	PERSON
ses	PERSON
études	PERSON
de	PERSON
l'	O
expression	O
du	O
discours	O
en	O
relation	O
avec	O
l'	O
histoire	O
de	O
la	O
pensée	O
occidentale	O
,	O
et	O
qui	O
ont	O
été	O
très	O
largement	O
discutées	O
,	O
à	O
l'	O
image	O
de	O
"	O
la	O
mort	O
de	O
l'	O
homme	O
"	O
annoncée	O
dans	O
Les	PERSON
Mots	PERSON
et	O
les	O
Choses	O
,	O
ou	O
de	O
l'	O
idée	O
de	O
subjectivation	O
,	O
réactivée	O
dans	O
Le	O
Souci	O
de	O
soi	O
d'	O
une	O
manière	O
toujours	O
problématique	O
pour	O
la	O
philosophie	O
classique	O
du	O
sujet	O
.	O
Il	O
semble	O
alors	O
que	O
,	O
plus	O
qu'	O
à	O
une	O
"	O
identité	O
"	O
par	O
définition	O
statique	O
et	O
objectivée	O
,	O
Foucault	O
s'	O
intéresse	O
aux	O
"	O
modes	O
de	O
vie	O
"	O
et	O
aux	O
processus	O
de	O
subjectivation	O
.	O
Sur	O
le	O
thème	O
de	O
la	O
subjectivité	O
,	O
les	O
deux	O
philosophes	O
qui	PERSON
ont	PERSON
le	PERSON
plus	O
influencé	LOCATION
Foucault	PERSON
sur	PERSON
ce	PERSON
thème	PERSON
--	PERSON
et	PERSON
sur	PERSON
d'	PERSON
autres	PERSON
--	O
sont	O
Nietzsche	PERSON
et	PERSON
Heidegger	PERSON
.	O
Si	PERSON
son	PERSON
œuvre	PERSON
est	PERSON
souvent	PERSON
qualifiée	O
de	O
post-moderniste	O
ou	O
post-structuraliste	PERSON
par	PERSON
les	O
commentateurs	O
et	O
critiques	O
contemporains	O
,	O
il	PERSON
fut	PERSON
lui-même	PERSON
plus	O
souvent	O
associé	O
au	LOCATION
mouvement	LOCATION
structuraliste	LOCATION
,	O
surtout	O
dans	O
les	O
années	O
qui	O
suivirent	O
la	O
publication	O
des	PERSON
Mots	PERSON
et	PERSON
les	PERSON
Choses	PERSON
:	O
bien	O
qu'	O
il	O
ait	O
initialement	O
accepté	O
cette	O
affiliation	O
,	O
il	PERSON
marqua	PERSON
par	PERSON
la	PERSON
suite	PERSON
sa	O
distance	O
vis-à-vis	O
de	O
l'	O
approche	O
structuraliste	O
,	O
expliquant	O
qu'	O
à	O
l'	O
inverse	O
de	O
celle-ci	O
,	O
il	O
n'	O
avait	O
pas	O
adopté	O
une	O
approche	O
formaliste	O
.	O
Comme	O
le	O
fait	O
observer	O
Didier	PERSON
Eribon	PERSON
,	O
il	LOCATION
lui	LOCATION
a	O
été	O
objecté	O
qu'	O
écrire	O
une	O
biographie	O
de	O
Michel	O
Foucault	O
était	O
une	O
entreprise	O
ambiguë	O
,	O
Foucault	PERSON
ayant	PERSON
toujours	PERSON
,	O
aux	PERSON
yeux	PERSON
de	PERSON
certains	PERSON
de	PERSON
ses	PERSON
disciples	PERSON
,	O
"	O
résisté	O
à	O
l'	O
expérience	O
biographique	O
"	O
.	O
D'	O
une	O
part	O
,	O
la	O
notion	O
d'	O
auteur	O
,	O
et	O
le	O
mythe	O
qui	O
accompagne	O
cette	O
figure	O
,	O
paraissait	O
suspecte	O
à	PERSON
Foucault	PERSON
,	O
qui	O
préférait	O
l'	O
écriture	O
"	O
anonyme	O
"	O
,	O
et	O
affirmait	O
que	O
l'	O
essentiel	O
de	O
ses	O
ouvrages	O
résidait	O
dans	O
une	O
voix	O
anonyme	O
--	O
la	O
période	O
historique	O
,	O
la	O
société	O
--	O
plus	O
que	O
dans	O
la	O
pensée	O
d'	O
une	O
personne	O
singulière	O
et	O
éminente	O
[	O
réf.	O
nécessaire	O
]	O
.	O
Mais	PERSON
comme	PERSON
l'	PERSON
a	O
fait	O
valoir	O
Eribon	O
,	O
on	O
peut	LOCATION
au	LOCATION
contraire	LOCATION
soutenir	LOCATION
qu'	O
écrire	O
"	O
Pas	O
de	O
publication	O
posthume	O
"	O
,	O
est	O
typiquement	PERSON
le	PERSON
geste	PERSON
d'	O
un	O
auteur	O
qui	O
entend	O
affirmer	O
ses	O
prérogatives	O
d'	O
auteur	O
sur	O
son	O
oeuvre	O
,	O
ce	LOCATION
qui	LOCATION
est	LOCATION
cohérent	PERSON
avec	PERSON
ses	PERSON
analyses	O
réelles	O
sur	O
la	O
notion	O
d'	O
auteur	O
,	O
dans	O
lesquelles	O
il	O
montre	O
comment	O
la	O
fonction-auteur	O
est	O
apparue	O
et	O
s'	O
est	O
imposée	O
comme	O
figure	O
nécessaire	O
.	O
Pour	O
ce	O
qui	O
est	O
de	O
la	O
"	O
biographie	O
"	O
,	O
Foucault	PERSON
insista	PERSON
également	PERSON
,	O
à	O
de	O
nombreuses	O
reprises	O
,	O
sur	O
le	O
fait	O
que	O
tous	O
ses	O
livres	O
étaient	O
liés	O
à	O
ses	O
expériences	O
personnelles	O
,	O
et	O
qu'	O
on	O
pouvait	O
les	O
lire	O
comme	O
autant	O
de	O
"	O
fragments	O
d'	O
autobiographie	O
"	O
.	O
Michel	PERSON
Foucault	PERSON
est	PERSON
connu	PERSON
pour	O
avoir	O
mis	O
en	O
lumière	O
certaines	O
pratiques	O
et	O
techniques	O
de	O
la	O
société	O
par	O
ses	O
institutions	O
à	O
l'	O
égard	O
des	O
individus	O
.	O
Michel	PERSON
Foucault	PERSON
s'	PERSON
est	PERSON
efforcé	O
,	O
dans	O
la	O
grande	O
majorité	O
de	O
ses	O
travaux	O
,	O
de	O
se	O
limiter	O
:	O
Elles	PERSON
conservent	PERSON
ainsi	PERSON
une	PERSON
grande	O
actualité	O
,	O
c'	O
est	O
pourquoi	O
beaucoup	O
d'	O
intellectuels	O
--	O
dans	O
une	O
grande	O
diversité	O
de	O
domaines	O
--	O
peuvent	O
se	O
réclamer	PERSON
de	PERSON
Foucault	PERSON
aujourd'hui	PERSON
.	PERSON
Dès	PERSON
lors	PERSON
,	O
Foucault	PERSON
n'	PERSON
aura	O
de	O
cesse	O
jusqu'	O
en	O
1984	O
d'	O
articuler	O
ensemble	O
,	O
sans	O
les	O
confondre	LOCATION
,	O
ces	O
trois	O
domaines	O
:	O
celui	O
du	O
savoir	O
,	O
celui	O
du	O
pouvoir	O
,	O
celui	O
du	O
sujet	O
.	O
C'	O
est	O
peut-être	PERSON
dans	O
son	O
hommage	O
à	O
Georges	PERSON
Canguilhem	PERSON
(	O
"	O
La	O
vie	O
:	O
l'	O
expérience	O
et	O
la	O
science	O
"	O
,	O
le	PERSON
dernier	PERSON
texte	O
auquel	PERSON
il	PERSON
donna	PERSON
son	PERSON
imprimatur	O
)	O
que	O
l'	O
on	O
perçoit	LOCATION
le	LOCATION
mieux	LOCATION
son	O
intérêt	PERSON
pour	PERSON
ce	O
problème	O
de	O
la	O
vie	O
et	O
son	O
rapport	O
à	O
la	O
vérité	O
:	O
Canguilhem	PERSON
,	O
comme	PERSON
le	PERSON
souligne	PERSON
Foucault	PERSON
,	O
a	O
en	O
effet	O
mis	O
en	O
avant	O
notre	O
humaine	O
capacité	O
(	O
cas	O
d'	O
espèce	O
!	O
dirait	O
encore	O
Nietzsche	PERSON
)	O
à	O
former	O
des	O
concepts	O
,	O
quelles	O
que	O
soient	O
les	O
errances	O
et	O
déviations	O
de	O
la	O
vie	O
,	O
qui	O
sont	O
sa	O
vocation	O
.	O
Malgré	PERSON
la	PERSON
proximité	O
évidente	PERSON
avec	PERSON
Georges	PERSON
Canguilhem	PERSON
,	O
on	O
ne	O
trouve	O
pas	O
cependant	O
,	O
à	O
proprement	O
parler	O
,	O
de	O
"	O
philosophie	O
de	O
la	O
vie	O
"	O
chez	O
Foucault	O
.	O
Ainsi	O
l'	O
utilité	O
chez	PERSON
Foucault	PERSON
,	O
dans	O
son	O
rapport	O
réciproque	PERSON
à	PERSON
la	PERSON
docilité	PERSON
,	O
ouvre	O
un	PERSON
domaine	PERSON
très	PERSON
large	PERSON
de	PERSON
considérations	PERSON
,	O
au-delà	LOCATION
de	O
l'	O
utilitarisme	O
,	O
du	O
côté	O
de	O
l'	O
industrie	O
,	O
du	O
travail	O
,	O
de	O
la	O
productivité	O
,	O
de	O
la	O
créativité	O
,	O
de	O
l'	O
autonomie	O
,	O
du	PERSON
gouvernement	PERSON
de	PERSON
soi	PERSON
.	O
Le	O
problème	O
du	O
désir	O
et	O
le	O
thème	O
de	O
la	O
maîtrise	O
sont	O
au	O
cœur	O
de	O
la	O
question	O
de	O
la	O
subjectivité	PERSON
développée	PERSON
alors	PERSON
par	PERSON
ce	PERSON
que	PERSON
certains	O
s'	O
autorisent	O
à	O
nommer	O
le	O
"	O
second	O
"	O
Foucault	O
,	O
celui	O
du	O
"	O
souci	O
de	O
soi	O
"	O
(	O
1984	O
)	O
,	O
émancipé	PERSON
du	PERSON
régime	PERSON
disciplinaire	PERSON
.	O
Michel	PERSON
Foucault	PERSON
s'	PERSON
est	PERSON
successivement	O
intéressé	O
au	O
savoir	O
,	O
puis	LOCATION
au	LOCATION
pouvoir	LOCATION
,	O
enfin	O
au	O
sujet	O
.	O
Outre	O
que	O
la	O
philosophie	O
foucaldienne	O
influença	O
(	O
tout	O
comme	O
elle	O
fut	O
influencée	O
par	O
)	O
nombre	O
de	O
mouvements	O
contestataires	O
en	O
France	LOCATION
et	O
dans	O
le	O
monde	O
anglo-saxon	PERSON
depuis	PERSON
les	PERSON
années	PERSON
1970	PERSON
(	O
de	O
l'	O
antipsychiatrie	O
au	O
mouvements	O
des	O
prisonniers	O
en	O
passant	O
par	O
les	O
mouvements	O
féministes	O
jusqu'	PERSON
aux	O
mouvements	O
de	O
malades	O
--	O
notamment	O
dans	O
la	O
lutte	O
contre	O
le	PERSON
sida	PERSON
--	PERSON
et	PERSON
des	PERSON
intermittents	PERSON
du	PERSON
spectacle	PERSON
)	O
,	O
la	O
fécondité	O
de	O
nombre	O
de	O
ses	O
propositions	O
essentielles	O
s'	O
éprouve	O
toujours	O
dans	O
le	O
monde	O
académique	O
et	O
au-delà	O
des	O
spécialisations	PERSON
disciplinaires	PERSON
.	O
Et	LOCATION
ce	LOCATION
,	O
malgré	PERSON
un	PERSON
certain	O
désamour	O
de	O
la	O
sociologie	O
,	O
alors	O
que	O
la	O
méthode	O
permet	O
au	O
sociologue	O
qui	O
tente	O
la	O
démarche	O
de	O
Foucault	O
,	O
foncièrement	O
constructiviste	O
,	O
de	PERSON
concevoir	PERSON
que	PERSON
le	O
sens	O
,	O
tout	O
comme	O
l'	O
individu	O
,	O
se	O
crée	O
dans	O
le	O
"	O
social	O
"	O
.	O
La	O
conception	O
que	O
Foucault	PERSON
défendit	PERSON
des	PERSON
intellectuels	O
face	O
aux	O
pouvoirs	O
,	O
avançant	O
la	O
figure	O
de	O
"	O
l'	O
intellectuel	PERSON
spécifique	PERSON
"	PERSON
,	O
et	O
son	O
rapport	O
au	PERSON
marxisme	PERSON
,	O
continuent	O
de	O
nourrir	O
des	O
controverses	O
.	O
