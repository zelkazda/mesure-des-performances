Par	O
son	O
jeu	PERSON
de	PERSON
mime	PERSON
et	PERSON
de	PERSON
clownerie	PERSON
,	O
il	PERSON
a	PERSON
su	PERSON
se	PERSON
faire	O
remarquer	O
,	O
et	O
devenir	O
l'	O
un	O
des	O
plus	O
célèbres	O
acteurs	O
d'	O
Hollywood	LOCATION
.	O
Charlie	PERSON
Chaplin	PERSON
fut	PERSON
l'	PERSON
une	O
des	O
personnes	O
les	O
plus	O
créatives	O
de	O
l'	O
ère	O
du	O
cinéma	O
muet	O
.	O
Réalisateur	LOCATION
,	O
scénariste	LOCATION
,	O
producteur	O
,	O
monteur	O
,	O
et	O
même	O
compositeur	O
de	O
ses	O
films	O
,	O
sa	PERSON
carrière	PERSON
durera	PERSON
plus	O
de	PERSON
soixante-cinq	PERSON
ans	PERSON
,	O
du	O
music-hall	O
en	O
Angleterre	O
,	O
jusqu'	LOCATION
à	O
sa	O
mort	O
,	O
en	O
Suisse	O
.	O
Il	O
fut	O
fortement	O
inspiré	O
par	O
l'	O
acteur	O
burlesque	O
français	O
Max	PERSON
Linder	PERSON
:	O
tous	O
deux	O
choisiront	PERSON
un	PERSON
costume	O
bien	PERSON
à	PERSON
eux	PERSON
.	PERSON
Mais	PERSON
Max	PERSON
Linder	PERSON
,	O
au	O
contraire	O
de	O
Charlie	PERSON
Chaplin	PERSON
,	O
ne	PERSON
se	PERSON
fera	O
pas	O
représenter	O
comme	O
une	O
victime	O
de	O
la	O
société	O
.	O
La	O
vie	O
publique	O
et	O
privée	O
de	O
Charlie	PERSON
Chaplin	PERSON
fera	O
l'	O
objet	O
d'	O
adulation	O
,	O
comme	O
de	O
controverse	O
.	O
Il	O
n'	O
a	O
qu'	O
un	O
an	O
lorsque	O
son	O
père	O
part	O
en	O
tournée	O
aux	O
États-Unis	O
.	O
Deux	O
mois	O
plus	O
tard	O
,	O
la	O
mère	O
de	PERSON
Chaplin	PERSON
obtient	O
son	O
congé	PERSON
de	PERSON
l'	PERSON
hôpital	O
.	O
Pendant	O
ce	O
temps	O
,	O
Charlie	PERSON
vécut	PERSON
avec	O
son	O
père	PERSON
et	PERSON
sa	PERSON
belle-mère	PERSON
alcoolique	PERSON
,	O
dans	O
un	O
environnement	O
intenable	O
pour	O
un	O
enfant	O
,	O
dont	O
les	O
souvenirs	O
inspireront	PERSON
Le	PERSON
Kid	PERSON
.	O
À	O
cinq	O
ans	O
,	O
Chaplin	O
monte	O
sur	O
scène	O
pour	O
remplacer	O
au	O
pied	O
levé	O
sa	O
mère	O
qui	O
ne	O
peut	O
plus	O
chanter	O
,	O
victime	O
d'	O
une	O
extinction	O
de	PERSON
voix	PERSON
.	O
Le	PERSON
frère	PERSON
de	PERSON
Charlie	PERSON
,	O
Sydney	LOCATION
,	O
quitte	O
le	O
foyer	O
parental	O
pour	O
travailler	O
dans	O
la	O
marine	O
.	O
Puis	O
,	O
il	PERSON
obtient	PERSON
à	PERSON
partir	PERSON
de	PERSON
1903	PERSON
une	O
succession	O
de	O
contrats	O
au	O
théâtre	O
,	O
et	O
en	O
1908	O
,	O
il	O
est	O
engagé	O
dans	O
la	O
troupe	O
de	PERSON
Fred	PERSON
Karno	PERSON
,	O
alors	O
le	O
plus	O
important	O
impresario	O
de	PERSON
spectacles	PERSON
avec	PERSON
des	PERSON
sketches	PERSON
.	O
Il	O
y	O
rencontre	O
le	O
futur	O
Stan	PERSON
Laurel	PERSON
.	O
Au	O
cours	O
d'	O
une	O
tournée	O
de	O
la	O
troupe	O
en	O
Amérique	O
,	O
les	O
studios	O
Keystone	O
lui	O
adressent	O
une	O
proposition	O
de	O
contrat	O
qu'	O
il	O
accepte	O
:	O
l'	O
aventure	O
cinématographique	O
commence	O
.	O
Ne	O
supportant	O
pas	O
les	O
pressions	O
dues	O
à	O
ces	O
temps	O
très	O
brefs	O
,	O
Chaplin	PERSON
s'	O
adapte	O
très	PERSON
mal	PERSON
aux	PERSON
conditions	PERSON
de	PERSON
travail	PERSON
de	PERSON
la	PERSON
compagnie	PERSON
,	O
à	O
tel	O
point	O
que	O
les	O
incidents	O
avec	O
les	O
metteurs	O
en	O
scène	O
sont	O
fréquents	O
.	O
Sur	O
les	O
ordres	O
de	O
Mack	O
Sennett	O
qui	O
lui	O
demande	O
de	PERSON
se	PERSON
créer	O
un	O
maquillage	O
au	O
pied	O
levé	O
,	O
il	PERSON
crée	PERSON
en	O
1914	O
le	O
personnage	O
raffiné	PERSON
de	PERSON
Charlot	PERSON
le	PERSON
vagabond	PERSON
,	O
et	O
recentre	O
tout	O
son	O
comique	O
autour	O
du	O
nouveau	O
personnage	O
et	O
de	O
sa	O
silhouette	O
qu'	O
il	PERSON
inaugure	PERSON
dans	O
Charlot	O
est	O
content	O
de	PERSON
lui	PERSON
(	O
1914	O
)	O
.	O
En	O
1916	O
,	O
il	O
signe	O
un	O
contrat	O
de	O
distribution	O
d'	O
un	O
million	O
de	O
dollars	O
avec	O
la	O
First	O
National	O
,	O
qui	PERSON
lui	PERSON
laisse	PERSON
la	PERSON
production	O
et	O
la	O
propriété	O
de	O
huit	O
films	O
prévus	LOCATION
.	LOCATION
Il	O
fait	O
alors	O
immédiatement	O
construire	O
son	O
propre	O
studio	O
dans	O
lequel	PERSON
il	PERSON
réalise	PERSON
neuf	PERSON
films	O
dont	O
Une	PERSON
vie	PERSON
de	PERSON
chien	PERSON
,	O
Le	PERSON
Kid	PERSON
et	O
Charlot	O
soldat	O
.	O
En	O
1919	O
,	O
un	O
vent	O
de	O
révolte	O
souffle	O
sur	O
Hollywood	LOCATION
où	LOCATION
les	LOCATION
acteurs	O
et	O
cinéastes	O
se	O
déclarent	PERSON
exploités	PERSON
;	O
Chaplin	PERSON
s'	O
associe	PERSON
alors	PERSON
à	PERSON
David	PERSON
Wark	PERSON
Griffith	PERSON
,	O
Mary	PERSON
Pickford	PERSON
et	PERSON
Douglas	PERSON
Fairbanks	PERSON
pour	O
fonder	O
la	O
United	O
Artists	O
.	O
Son	O
premier	O
film	O
pour	O
sa	O
nouvelle	O
firme	O
sera	O
L'	O
Opinion	O
publique	O
(	O
1923	O
)	O
.	O
Puis	O
,	O
Chaplin	O
fait	O
peu	O
à	O
peu	O
entrer	O
dans	O
son	O
univers	O
comique	O
celui	O
du	O
mélodrame	O
et	O
de	O
la	O
réalité	O
sociale	O
comme	O
dans	O
La	PERSON
Ruée	PERSON
vers	O
l'	O
or	O
(	O
1925	O
)	O
.	O
Les	PERSON
Lumières	PERSON
de	PERSON
la	PERSON
ville	PERSON
(	O
1931	O
)	O
est	O
le	O
premier	O
film	O
à	O
en	O
bénéficier	O
,	O
mais	O
de	O
manière	O
très	O
ironique	O
.	O
Chaplin	PERSON
souffle	PERSON
pendant	PERSON
des	PERSON
heures	PERSON
dans	PERSON
un	PERSON
vieux	PERSON
saxophone	O
afin	O
de	O
parodier	O
les	O
imperfections	O
du	O
parlant	O
lors	O
de	O
la	O
scène	O
d'	O
ouverture	O
du	O
film	O
.	O
De	O
plus	O
Chaplin	O
ne	O
se	O
détourne	O
pas	O
de	O
son	O
projet	O
initial	O
de	O
film	O
muet	O
.	O
Un	O
film	O
dialogué	O
a	O
une	O
audience	O
un	PERSON
peu	PERSON
plus	O
limitée	O
car	O
il	PERSON
contient	PERSON
la	PERSON
barrière	PERSON
de	PERSON
la	PERSON
langue	PERSON
et	O
Chaplin	O
veut	O
s'	O
adresser	O
à	O
tous	O
.	O
On	O
le	LOCATION
dit	LOCATION
fini	O
,	O
à	O
l'	O
instar	O
de	O
ses	O
amis	O
David	PERSON
Wark	PERSON
Griffith	PERSON
,	O
Mary	PERSON
Pickford	PERSON
et	PERSON
Douglas	PERSON
Fairbanks	O
et	O
de	O
bien	O
d'	O
autres	O
vedettes	O
du	O
muet	O
qui	O
n'	O
ont	O
pas	O
survécu	O
au	O
parlant	O
.	O
Il	O
entreprend	O
un	O
long	O
voyage	O
,	O
qui	LOCATION
va	LOCATION
durer	LOCATION
plus	O
d'	O
un	O
an	O
et	O
demi	O
,	O
à	O
travers	O
le	O
monde	O
,	O
en	O
Europe	LOCATION
notamment	O
,	O
pour	O
présenter	O
son	O
film	O
.	O
Il	O
rencontre	O
la	O
plupart	O
des	O
chefs	O
d'	O
états	O
et	O
de	O
nombreuses	O
personnalités	O
,	O
parmi	O
lesquelles	O
Albert	PERSON
Einstein	PERSON
.	O
Il	PERSON
conjugue	PERSON
tout	O
cela	O
dans	O
Les	O
Temps	O
modernes	O
(	O
1936	O
)	O
,	O
le	LOCATION
dernier	LOCATION
film	O
muet	O
de	O
l'	O
histoire	O
et	O
l'	O
un	O
des	O
plus	O
célèbres	O
,	O
sinon	O
le	O
plus	O
célèbre	O
,	O
de	O
son	O
auteur	O
.	O
Le	O
personnage	O
joué	PERSON
par	PERSON
Paulette	PERSON
Goddard	PERSON
les	O
lui	O
copie	O
sur	O
ses	O
manchettes	O
.	O
Ce	O
film	O
est	O
également	O
l'	O
ultime	O
apparition	O
à	O
l'	O
écran	O
du	O
personnage	O
Charlot	O
.	O
Il	PERSON
parle	PERSON
aussi	PERSON
de	PERSON
la	PERSON
difficulté	PERSON
du	PERSON
travail	PERSON
à	PERSON
la	PERSON
chaîne	PERSON
qui	PERSON
rend	PERSON
fou	PERSON
la	PERSON
plupart	PERSON
des	PERSON
employés	PERSON
,	O
dont	O
le	O
personnage	O
interprété	O
par	O
Chaplin	LOCATION
,	O
ce	O
qui	O
lui	O
vaut	O
un	O
passage	O
à	O
l'	O
hôpital	O
psychiatrique	O
dans	O
le	O
film	O
.	O
Mais	O
le	O
cinéaste	O
reçoit	O
le	O
soutien	O
du	O
président	O
Franklin	PERSON
Roosevelt	PERSON
,	O
lequel	O
l'	O
invitera	O
,	O
quelques	O
semaines	O
après	O
la	O
sortie	O
du	O
film	O
,	O
à	O
la	O
Maison	O
Blanche	O
,	O
pour	O
s'	O
entendre	O
réciter	PERSON
le	PERSON
discours	O
final	O
.	O
Le	O
film	O
est	O
interdit	O
sur	O
tout	O
le	O
continent	O
Européen	O
[	O
réf.	O
nécessaire	O
]	O
,	O
mais	O
une	O
rumeur	O
circule	O
:	O
Hitler	O
l'	O
aurait	O
vu	O
,	O
en	O
projection	O
privée	O
[	O
réf.	O
nécessaire	O
]	O
.	O
En	LOCATION
France	LOCATION
,	O
il	LOCATION
ne	LOCATION
sortira	O
qu'	O
en	O
1945	O
.	O
Cette	PERSON
fois-ci	PERSON
,	O
Chaplin	PERSON
est	O
définitivement	PERSON
entré	PERSON
dans	O
l'	O
ère	O
du	O
cinéma	O
sonore	O
...	O
et	O
signe	O
l'	O
arrêt	O
de	O
mort	O
du	O
petit	O
vagabond	O
.	O
En	O
1946	O
,	O
Chaplin	PERSON
tourne	O
son	O
film	O
le	O
plus	O
dur	O
,	O
Monsieur	O
Verdoux	O
.	O
Orson	PERSON
Welles	PERSON
propose	O
à	O
Chaplin	PERSON
un	PERSON
scénario	PERSON
basé	PERSON
sur	O
l'	O
affaire	O
Landru	PERSON
.	O
Une	PERSON
fois	PERSON
encore	PERSON
,	O
Chaplin	PERSON
livre	O
un	O
message	O
empreint	O
de	O
cynisme	O
mais	O
également	O
d'	O
humanisme	O
.	O
En	O
1950	O
,	O
il	LOCATION
vend	LOCATION
la	LOCATION
quasi-totalité	O
de	O
ses	O
parts	O
à	O
la	O
United	O
Artists	O
et	O
travaille	O
aux	O
Feux	O
de	O
la	O
Rampe	O
où	O
il	O
décrit	O
la	O
triste	O
fin	O
d'	O
un	PERSON
clown	PERSON
dans	PERSON
le	PERSON
Londres	PERSON
de	PERSON
son	O
enfance	O
.	O
Ses	O
propres	O
enfants	O
apparaissent	O
comme	O
figurants	O
et	O
Chaplin	O
tient	O
le	O
premier	O
rôle	LOCATION
.	LOCATION
Le	O
film	O
sort	O
en	O
1952	O
à	O
Londres	O
et	O
vaut	O
un	O
triomphe	O
à	O
son	O
auteur	O
.	O
L'	O
une	O
des	O
plus	O
belles	O
scènes	O
du	O
film	O
se	O
trouve	PERSON
vers	PERSON
la	O
fin	O
:	O
Buster	PERSON
Keaton	PERSON
joue	PERSON
un	PERSON
pianiste	PERSON
et	O
Chaplin	PERSON
un	PERSON
violoniste	PERSON
.	O
Mais	PERSON
rien	PERSON
ne	PERSON
se	O
déroule	PERSON
comme	PERSON
prévu	PERSON
car	O
Keaton	PERSON
a	O
des	O
problèmes	O
avec	O
ses	O
partitions	O
et	O
son	O
piano	O
et	O
Chaplin	O
doit	O
se	O
battre	O
avec	O
les	O
cordes	O
de	O
son	O
violon	O
.	O
Victime	O
du	O
maccarthisme	O
(	O
son	O
nom	O
figure	O
sur	O
la	O
"	O
liste	O
noire	O
"	O
)	O
,	O
il	O
est	O
harcelé	O
par	O
le	O
FBI	O
en	O
raison	O
de	O
ses	O
opinions	O
de	PERSON
gauche	PERSON
(	O
pour	O
sa	O
part	O
,	O
il	PERSON
se	PERSON
présentait	PERSON
comme	PERSON
un	O
"	O
citoyen	O
du	O
monde	O
"	O
)	O
.	O
Pour	O
cette	O
raison	O
,	O
il	PERSON
se	PERSON
voit	PERSON
refuser	PERSON
le	PERSON
visa	PERSON
de	PERSON
retour	PERSON
lors	PERSON
de	PERSON
son	PERSON
séjour	PERSON
en	O
Europe	LOCATION
pour	O
la	O
présentation	O
de	O
son	O
film	O
.	O
Il	PERSON
renonce	PERSON
alors	PERSON
à	PERSON
sa	PERSON
résidence	PERSON
aux	PERSON
États-Unis	PERSON
et	O
installe	O
sa	O
famille	O
en	O
Suisse	O
jusqu'	O
à	O
la	O
fin	O
de	O
ses	O
jours	O
.	O
En	O
1967	O
,	O
il	PERSON
tourne	PERSON
son	PERSON
dernier	PERSON
film	PERSON
,	O
cette	PERSON
fois-ci	PERSON
en	O
couleur	O
,	O
La	O
Comtesse	O
de	O
Hong-Kong	O
,	O
avec	O
Sophia	PERSON
Loren	PERSON
,	O
Marlon	PERSON
Brando	PERSON
et	PERSON
Tippi	PERSON
Hedren	PERSON
,	O
dont	O
l'	O
action	O
se	O
déroule	O
sur	O
un	O
paquebot	O
et	O
où	O
il	O
ne	O
tient	O
qu'	O
un	O
petit	O
rôle	O
:	O
celui	O
d'	O
un	O
steward	O
victime	O
du	PERSON
mal	PERSON
de	PERSON
mer	PERSON
.	PERSON
De	O
nombreuses	O
demandes	O
de	O
rançon	O
plus	O
ou	O
moins	O
farfelues	O
sont	O
adressées	O
à	O
la	O
famille	O
Chaplin	PERSON
.	O
Charlie	PERSON
Chaplin	PERSON
a	O
été	O
marié	O
à	O
4	O
reprises	O
;	O
Mildred	PERSON
Harris	PERSON
,	O
Lita	PERSON
Grey	PERSON
et	O
Paulette	PERSON
Goddard	PERSON
étaient	O
toutes	O
trois	O
ses	O
partenaires	O
à	O
l'	O
écran	O
.	O
L'	O
aventure	O
de	PERSON
Charlie	PERSON
Chaplin	PERSON
avec	PERSON
Lita	PERSON
Grey	PERSON
aurait	PERSON
inspiré	O
Vladimir	PERSON
Nabokov	PERSON
pour	O
son	O
roman	O
Lolita	O
.	O
Ses	O
mariages	O
ont	O
défrayé	O
la	O
chronique	O
américaine	O
,	O
en	O
effet	O
il	O
a	O
29	O
ans	O
quand	O
il	O
se	O
marie	PERSON
avec	PERSON
Mildred	PERSON
Harris	PERSON
,	O
qui	O
en	O
a	O
17	O
;	O
il	O
en	O
a	O
35	O
quand	O
il	O
épouse	O
Lita	PERSON
Grey	PERSON
qui	PERSON
a	O
16	O
ans	O
;	O
il	PERSON
a	O
47	O
ans	O
quand	O
il	O
convole	O
avec	O
Paulette	PERSON
Goddard	PERSON
qui	PERSON
en	O
a	O
25	O
;	O
il	O
a	O
54	O
ans	O
lors	O
de	O
son	O
mariage	PERSON
avec	PERSON
Oona	PERSON
O'Neill	PERSON
qui	PERSON
en	O
a	O
18	O
.	O
Cependant	O
,	O
avec	PERSON
l'	O
arrivée	O
du	O
parlant	O
,	O
Chaplin	PERSON
a	O
dû	O
faire	O
un	O
choix	O
et	O
opérer	O
un	PERSON
passage	PERSON
du	PERSON
muet	PERSON
au	PERSON
sonore	PERSON
,	O
puis	LOCATION
au	LOCATION
parlant	O
.	O
C'	O
est	O
dans	O
Les	PERSON
Lumières	PERSON
de	PERSON
la	PERSON
ville	PERSON
que	O
Chaplin	O
débute	O
ce	O
passage	O
au	O
sonore	O
.	O
Cependant	O
,	O
comme	PERSON
le	PERSON
dit	PERSON
Michel	PERSON
Chion	PERSON
,	O
il	O
s'	O
agit	O
tout	O
de	O
même	O
d'	O
un	O
"	O
véritable	O
manifeste	O
pour	O
la	O
défense	O
du	O
muet	O
"	O
.	O
Or	O
,	O
s'	O
il	O
y	O
a	O
une	O
chose	O
qui	O
n'	O
est	O
pas	O
sonore	O
,	O
c'	O
est	O
bien	LOCATION
le	LOCATION
moment	LOCATION
où	LOCATION
le	LOCATION
bruit	LOCATION
de	LOCATION
la	LOCATION
portière	O
fait	O
croire	O
à	O
la	O
jeune	PERSON
aveugle	PERSON
que	O
Charlie	PERSON
est	PERSON
un	PERSON
millionnaire	PERSON
--	O
gag	O
qui	O
a	O
nécessité	O
plusieurs	O
mois	O
d'	O
élaboration	O
,	O
et	O
plusieurs	O
interruptions	O
de	O
tournage	O
.	O
De	O
plus	O
,	O
lorsqu'	LOCATION
un	O
homme	O
mange	O
le	PERSON
savon	PERSON
de	PERSON
Charlie	PERSON
et	PERSON
que	PERSON
celui-ci	PERSON
se	PERSON
met	O
à	LOCATION
le	LOCATION
disputer	O
,	O
tout	O
ce	O
qui	O
sort	O
de	O
sa	O
bouche	O
est	O
des	O
bulles	O
de	O
savon	O
,	O
comme	O
si	O
toute	O
parole	O
était	O
vaine	O
.	O
Lorsque	O
Chaplin	O
débute	O
le	O
tournage	O
des	O
Temps	O
Modernes	O
(	O
1936	O
)	O
en	O
parlant	O
,	O
il	PERSON
se	PERSON
rend	O
compte	PERSON
bien	PERSON
vite	PERSON
qu'	PERSON
il	PERSON
s'	PERSON
y	PERSON
perd	PERSON
.	PERSON
La	O
seule	O
fois	O
où	O
on	O
entend	O
réellement	O
un	O
personnage	O
parler	O
"	O
en	O
direct	O
"	O
est	O
également	O
la	O
première	O
fois	O
où	O
l'	O
on	O
entend	O
la	O
voix	O
de	O
Chaplin	O
.	O
Cependant	O
,	O
même	PERSON
si	PERSON
celui-ci	PERSON
essaie	PERSON
d'	O
avoir	O
un	O
langage	O
articulé	O
,	O
il	PERSON
baragouine	PERSON
,	O
ayant	O
oublié	O
les	PERSON
paroles	PERSON
de	PERSON
sa	PERSON
chanson	O
:	O
"	O
c'	O
est	O
comme	PERSON
le	PERSON
langage	PERSON
à	O
la	O
naissance	O
"	O
,	O
langage	O
que	O
Chaplin	PERSON
développera	O
dans	O
les	O
prochains	O
films	O
.	O
Dans	O
Le	O
Dictateur	O
,	O
contrairement	O
aux	O
Lumières	O
de	O
la	O
ville	O
,	O
le	O
titre	O
fait	O
appel	O
au	O
monde	O
de	O
la	O
parole	O
.	O
Même	PERSON
si	PERSON
le	PERSON
film	PERSON
est	PERSON
presque	O
entièrement	O
parlant	O
et	O
renonce	O
définitivement	O
aux	O
cartons	O
du	O
muet	O
,	O
Chaplin	PERSON
ne	PERSON
renonce	O
pas	O
encore	O
au	O
langage	O
de	O
la	O
pantomime	O
.	O
Chaplin	PERSON
s'	O
engage	O
politiquement	PERSON
dans	O
certaines	O
de	O
ses	O
œuvres	O
,	O
véritables	O
satires	O
de	O
la	O
société	O
des	O
années	O
1930	O
.	O
Des	O
films	O
comme	O
Les	O
Temps	O
modernes	O
ou	O
Le	O
Dictateur	O
sont	O
respectivement	O
une	O
critique	O
de	O
la	O
société	O
de	O
consommation	O
de	O
masse	O
et	O
du	O
travail	O
à	O
la	O
chaîne	O
,	O
et	O
une	O
critique	O
des	O
régimes	O
politiques	O
dictatoriaux	O
et	O
fascistes	O
qui	O
s'	O
installent	O
en	O
Europe	LOCATION
.	O
On	O
peut	PERSON
donc	PERSON
affirmer	PERSON
l'	PERSON
engagement	O
politique	O
de	PERSON
Charlie	PERSON
Chaplin	PERSON
dans	PERSON
la	O
société	O
de	O
son	O
époque	O
.	O
Accusé	PERSON
de	PERSON
prendre	PERSON
des	PERSON
positions	O
communistes	O
aux	O
États-Unis	O
ce	O
que	O
lui	O
a	O
valu	O
des	O
enquêtes	O
du	O
FBI	O
,	O
il	LOCATION
fut	LOCATION
une	LOCATION
des	LOCATION
victimes	LOCATION
du	O
maccarthisme	O
au	O
début	O
des	O
années	O
1950	O
et	O
inscrit	O
sur	O
la	O
liste	O
noire	O
du	O
cinéma	O
.	O
Ce	O
fut	O
l'	O
une	O
des	O
causes	O
de	O
son	O
exil	O
en	O
Suisse	O
.	O
Il	O
n'	O
existe	O
aucune	O
indication	O
d'	O
une	O
ascendance	O
juive	PERSON
de	PERSON
Chaplin	PERSON
,	O
cependant	O
tout	O
au	LOCATION
long	O
de	O
sa	O
carrière	O
,	O
il	PERSON
y	PERSON
eut	PERSON
des	PERSON
controverses	PERSON
sur	O
ses	O
possibles	O
origines	O
juives	O
.	O
Dans	O
les	O
années	O
1930	O
,	O
la	O
propagande	O
nazie	O
l'	O
a	O
constamment	O
déclaré	O
juif	O
en	O
se	O
fondant	O
sur	O
des	O
articles	O
publiés	O
antérieurement	O
dans	O
la	O
presse	O
américaine	O
;	O
les	O
enquêtes	O
du	O
FBI	O
sur	PERSON
Chaplin	PERSON
à	PERSON
la	PERSON
fin	PERSON
des	PERSON
années	PERSON
1940	PERSON
ont	O
également	O
mis	O
l'	O
accent	O
sur	O
ses	O
origines	O
ethniques	O
.	O
Durant	PERSON
toute	O
son	O
existence	O
,	O
Chaplin	PERSON
a	O
farouchement	O
refusé	O
de	PERSON
contester	PERSON
ou	PERSON
de	PERSON
réfuter	O
les	O
déclarations	O
affirmant	O
qu'	PERSON
il	LOCATION
était	LOCATION
juif	LOCATION
,	O
en	O
disant	O
que	O
ce	O
serait	O
"	O
faire	O
directement	O
le	PERSON
jeu	PERSON
des	PERSON
antisémites	PERSON
"	O
.	O
Comme	O
Orson	PERSON
Welles	PERSON
,	O
Alfred	PERSON
Hitchcock	PERSON
,	O
ou	O
Cary	PERSON
Grant	PERSON
,	O
Charlie	PERSON
Chaplin	PERSON
n'	O
a	O
jamais	O
reçu	O
la	O
célèbre	O
statuette	O
,	O
sinon	O
le	O
prix	O
honorifique	LOCATION
.	LOCATION
Il	O
a	O
toutefois	O
reçu	O
un	O
Oscar	PERSON
de	PERSON
la	PERSON
meilleure	PERSON
musique	PERSON
de	PERSON
film	O
en	O
1952	O
pour	O
Les	PERSON
Feux	PERSON
de	PERSON
la	PERSON
rampe	PERSON
(	O
qui	O
est	O
le	O
seul	O
film	O
réunissant	O
Charlie	PERSON
Chaplin	PERSON
et	O
Buster	PERSON
Keaton	PERSON
)	O
.	O
Avant	O
cette	O
consécration	O
,	O
il	O
avait	O
été	O
nommé	O
comme	O
meilleur	O
acteur	O
,	O
meilleur	PERSON
réalisateur	PERSON
pour	PERSON
le	O
film	O
Le	O
Cirque	O
.	O
