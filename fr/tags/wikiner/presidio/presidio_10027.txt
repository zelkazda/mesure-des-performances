Une	O
céphéide	O
est	O
une	O
étoile	O
géante	O
ou	O
supergéante	O
jaune	O
,	O
de	O
4	O
à	O
15	O
fois	O
plus	O
massive	O
que	O
le	O
Soleil	O
et	O
de	O
100	O
à	O
30	O
000	O
fois	O
plus	O
lumineuse	O
,	O
dont	O
l'	O
éclat	O
varie	O
de	O
0,1	O
à	O
2	O
magnitudes	O
selon	O
une	O
période	O
bien	O
définie	O
,	O
comprise	O
entre	O
1	O
et	O
135	O
jours	O
,	O
d'	O
où	O
elle	O
tire	O
son	O
nom	O
d'	O
étoile	O
variable	O
.	O
Elles	O
ont	O
été	O
nommées	O
d'	O
après	O
le	O
prototype	O
de	O
l'	O
étoile	O
δ	O
de	O
la	O
constellation	O
de	PERSON
Céphée	PERSON
.	O
Cette	O
mesure	O
a	O
été	O
réalisée	O
pour	O
la	O
première	O
fois	O
en	O
1916	O
,	O
à	O
l'	O
université	O
Harvard	O
,	O
par	O
Harlow	PERSON
Shapley	PERSON
qui	PERSON
a	O
complété	O
la	O
découverte	O
d'	O
Henrietta	PERSON
Leavitt	PERSON
.	O
Jeune	O
mais	O
de	O
structure	O
plus	O
évoluée	O
que	O
le	O
Soleil	O
,	O
une	O
céphéide	O
doit	O
son	O
énergie	PERSON
lumineuse	PERSON
aux	PERSON
réactions	O
de	O
fusion	O
nucléaire	O
qui	O
dans	O
sa	O
région	O
centrale	O
transforment	PERSON
de	PERSON
l'	PERSON
hélium	PERSON
en	O
carbone	O
.	O
On	O
doit	O
à	O
Arthur	O
Eddington	O
(	O
1926	O
)	O
une	O
première	O
explication	O
des	O
variations	O
de	O
luminosité	O
.	O
Très	O
brillantes	O
,	O
donc	PERSON
visibles	O
de	PERSON
loin	PERSON
,	O
les	O
céphéides	O
sont	O
détectées	O
à	O
présent	O
dans	O
d'	O
autres	O
galaxies	O
que	O
la	O
nôtre	PERSON
jusqu'	PERSON
à	O
des	O
distances	O
de	O
80	O
millions	O
d'	O
années-lumière	O
environ	O
grâce	O
au	O
télescope	O
spatial	O
Hubble	O
.	O
Le	O
point	O
délicat	PERSON
réside	PERSON
dans	O
l'	O
étalonnage	O
absolu	O
de	O
la	O
relation	O
période-luminosité	O
,	O
qui	O
nécessite	O
de	O
déterminer	O
indépendamment	O
de	O
façon	O
précise	O
la	O
distance	O
d'	O
au	O
moins	O
quelques	O
céphéides	O
situées	O
dans	O
notre	O
Galaxie	O
.	O
