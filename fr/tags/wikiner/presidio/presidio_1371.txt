Sa	PERSON
capitale	PERSON
est	PERSON
Montevideo	LOCATION
,	O
qui	LOCATION
est	LOCATION
également	LOCATION
la	O
plus	O
grande	O
ville	O
du	O
pays	O
avec	PERSON
près	PERSON
de	PERSON
1500000	PERSON
habitants	O
.	O
Le	O
nom	O
Uruguay	O
vient	O
du	O
guarani	O
.	O
Bien	O
que	O
sa	O
signification	O
ne	PERSON
soit	PERSON
pas	PERSON
très	PERSON
claire	PERSON
,	O
Félix	PERSON
de	PERSON
Azara	PERSON
affirma	PERSON
que	PERSON
ce	PERSON
nom	PERSON
désigne	PERSON
un	PERSON
petit	PERSON
oiseau	PERSON
nommé	PERSON
el	PERSON
urú	PERSON
qui	PERSON
vit	O
sur	O
les	O
rives	O
du	O
fleuve	O
Uruguay	O
(	O
qui	O
signifie	O
lui-même-alors	O
"	O
rivière	O
du	O
pays	O
de	O
l'	O
urú	O
"	O
(	O
río	O
del	O
país	O
del	O
urú	O
)	O
.	O
Néanmoins	PERSON
,	O
l'	O
un	O
des	O
accompagnateurs	O
d'	O
Azara	PERSON
donna	PERSON
une	PERSON
autre	PERSON
version	O
en	O
disant	O
que	O
le	O
mot	O
Uruguay	O
se	O
divise	O
en	O
deux	O
parties	O
:	O
uruguá	O
signifiant	O
"	O
escargot	O
"	O
,	O
et	O
le	O
ï	O
signifiant	O
rivière	O
,	O
la	O
traduction	O
serait	O
donc	O
"	O
rivière	O
des	O
escargots	O
"	O
(	O
río	O
de	O
los	O
caracoles	O
)	O
.	O
Enfin	LOCATION
,	O
le	O
poète	PERSON
Juan	PERSON
Zorrilla	PERSON
de	PERSON
San	LOCATION
Martín	LOCATION
a	O
interprété	LOCATION
le	O
mot	O
d'	O
une	O
troisième	PERSON
façon	PERSON
,	O
comme	LOCATION
le	LOCATION
"	O
fleuve	O
des	O
oiseaux	O
peints	O
"	O
(	O
río	O
de	O
los	O
pájaros	O
pintados	O
)	O
.	O
L'	O
histoire	O
de	O
ce	O
pays	O
commence	O
réellement	O
avec	O
celle	O
du	O
peuple	O
Guaraní	O
et	O
des	O
Charrúas	O
.	O
En	O
1516	O
,	O
les	O
Espagnols	O
découvrent	O
le	O
territoire	O
mais	O
le	O
délaissent	O
au	O
départ	O
du	O
fait	O
de	O
la	O
faiblesse	O
de	O
ses	O
ressources	O
naturelles	O
.	O
À	O
la	O
fin	O
du	O
siècle	O
,	O
le	LOCATION
pays	O
participa	O
à	O
la	O
guerre	O
de	O
la	O
Triple	O
Alliance	O
contre	O
le	O
Paraguay	LOCATION
.	O
La	O
gauche	O
met	O
en	O
place	O
un	O
Front	O
large	O
en	O
vue	O
des	O
élections	O
générales	O
de	O
1971	O
,	O
afin	O
de	O
défier	O
les	O
deux	O
partis	O
traditionnels	O
,	O
blancos	O
et	O
colorados	O
.	O
Après	PERSON
le	PERSON
gouvernement	PERSON
libéral	PERSON
de	PERSON
Batlle	PERSON
(	O
2000-2005	O
)	O
,	O
les	O
élections	O
de	O
2004	O
marquèrent	O
,	O
pour	O
la	O
première	O
fois	O
,	O
la	O
victoire	O
de	O
la	O
gauche	O
,	O
le	O
Front	O
large	O
remportant	O
massivement	O
celles-ci	O
,	O
conduisant	O
son	O
candidat	O
présidentiel	O
,	O
le	O
socialiste	O
Tabaré	PERSON
Vázquez	PERSON
,	O
à	O
assumer	O
la	PERSON
présidence	PERSON
(	O
2005-2010	O
)	O
.	O
Son	PERSON
mari	PERSON
,	O
José	PERSON
Mujica	PERSON
,	O
est	O
élu	O
président	O
.	O
Il	O
s'	O
agissait	O
du	O
premier	O
président	O
à	O
n'	O
être	O
ni	O
un	O
Blanco	O
,	O
ni	O
un	O
Colorado	LOCATION
depuis	O
plus	O
de	O
150	O
ans	O
.	O
José	PERSON
Mujica	PERSON
(	O
Front	O
large	O
)	O
a	O
été	O
élu	O
président	O
de	O
l'	O
Uruguay	LOCATION
en	O
novembre	O
2009	O
.	O
Les	O
premiers	O
départements	O
sont	O
formés	O
dès	O
1816	O
et	O
le	O
plus	O
jeune	O
date	O
de	O
1885	O
,	O
c'	O
est	O
celui	PERSON
de	PERSON
Flores	PERSON
.	O
Le	O
point	O
culminant	PERSON
du	PERSON
pays	O
est	O
le	O
Cerro	O
Catedral	O
avec	O
ses	O
514	O
m	O
.	O
Le	O
relief	O
est	O
lié	O
dans	O
la	O
partie	O
sud	PERSON
aux	PERSON
terres	PERSON
de	PERSON
la	PERSON
Pampa	PERSON
et	PERSON
est	PERSON
constitué	O
par	O
de	O
vastes	O
plaines	O
ondulées	O
et	O
sillonnées	O
par	O
des	O
collines	O
de	O
faible	O
élévation	O
appelées	O
cuchillas	PERSON
.	O
Avec	PERSON
ses	PERSON
étés	PERSON
chauds	PERSON
et	PERSON
ses	PERSON
hivers	PERSON
doux	PERSON
,	PERSON
le	PERSON
climat	PERSON
en	O
Uruguay	LOCATION
est	O
tempéré	O
,	O
presque	O
semi-tropical	O
et	O
les	O
précipitations	O
sont	O
assez	O
copieuses	PERSON
et	O
plus	O
ou	O
moins	O
homogènes	O
pendant	O
toute	O
l'	O
année	O
.	O
Le	O
littoral	O
connaît	O
un	O
climat	O
maritime	O
,	O
avec	O
une	O
certaine	O
amplitude	O
,	O
du	O
fait	O
du	O
courant	O
chaud	O
du	O
Brésil	O
,	O
qui	PERSON
augmente	PERSON
la	PERSON
température	O
des	O
côtes	O
de	O
l'	O
Atlantique	O
à	O
partir	O
de	O
janvier	O
jusqu'	O
au	O
début	O
mai	O
;	O
et	O
du	O
courant	O
froid	O
des	PERSON
îles	PERSON
Malouines	PERSON
refroidissant	O
leurs	O
eaux	O
de	O
juin	O
à	O
septembre	O
.	O
La	O
crise	O
a	O
été	O
accentuée	O
par	O
l'	O
effondrement	O
de	O
l'	O
économie	O
argentine	O
dès	O
1999	O
,	O
l'	O
Argentine	O
étant	O
son	O
principal	O
partenaire	O
économique	O
.	O
Bien	PERSON
qu'	PERSON
il	PERSON
n'	PERSON
existe	O
aucune	O
source	O
d'	O
information	O
fiable	O
ou	O
précise	O
concernant	O
le	PERSON
niveau	PERSON
de	PERSON
pauvreté	PERSON
actuel	PERSON
en	PERSON
Uruguay	LOCATION
,	O
celui-ci	LOCATION
pourrait	O
avoir	O
doublé	O
ou	O
même	O
triplé	O
depuis	O
la	O
fin	O
des	O
années	O
1990	O
,	O
ramenant	O
à	O
15	O
ou	O
30	O
%	O
les	O
foyers	O
vivant	O
dans	O
la	PERSON
pauvreté	PERSON
,	O
alors	O
que	O
le	O
chômage	O
touche	O
actuellement	O
12	O
%	O
des	O
actifs	O
.	O
La	O
population	O
est	O
essentiellement	PERSON
urbaine	PERSON
(	O
90,7	O
%	O
)	O
et	O
vit	O
dans	O
les	O
20	O
plus	O
grandes	O
villes	LOCATION
du	LOCATION
pays	O
,	O
principalement	O
à	O
Montevideo	O
(	O
1,4	O
million	O
d'	O
habitants	O
)	O
.	O
Les	O
descendants	O
d'	O
Européens	O
représentent	O
88	O
%	O
de	O
la	O
population	O
.	O
