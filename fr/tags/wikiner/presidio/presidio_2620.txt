Il	PERSON
travaille	PERSON
sur	PERSON
Dragon	PERSON
Ball	PERSON
de	PERSON
1984	PERSON
à	PERSON
1995	O
.	O
Pendant	O
ces	O
onze	O
ans	O
,	O
il	PERSON
dessine	PERSON
42	O
volumes	O
d'	O
environ	O
200	O
pages	O
chacun	O
(	O
ce	O
qui	O
fait	O
de	O
Dragon	O
Ball	O
une	O
œuvre	O
d'	O
environ	O
10.000	O
pages	O
)	O
.	O
Grâce	O
à	O
son	O
sens	PERSON
créatif	PERSON
,	O
Toriyama	PERSON
obtient	PERSON
du	O
travail	O
avec	O
plusieurs	O
projets	O
de	PERSON
jeux	PERSON
vidéo	PERSON
tel	PERSON
Dragon	PERSON
Quest	PERSON
.	O
Il	PERSON
conçoit	PERSON
également	PERSON
les	PERSON
personnages	PERSON
de	PERSON
Chrono	PERSON
Trigger	PERSON
sur	PERSON
Super	PERSON
Nintendo	PERSON
,	O
des	O
jeux	O
de	O
combat	O
Tobal	O
No.	O
1	O
et	O
Tobal	O
2	O
sur	O
PlayStation	O
,	O
et	O
enfin	O
de	O
Blue	O
Dragon	O
sur	O
Xbox	O
360	O
.	O
Après	PERSON
Dragon	PERSON
Ball	PERSON
,	O
Toriyama	LOCATION
produit	O
des	O
one	O
shot	O
ou	O
de	O
courts	O
récits	O
(	O
de	O
100	O
à	O
200	O
pages	O
)	O
,	O
tels	O
que	O
Go	O
!	O
Go	O
!	O
Ackman	O
,	O
Cowa	O
,	O
Kajika	O
,	O
Sand	O
Land	O
ou	O
encore	O
Cross	O
epoch	O
en	O
collaboration	O
avec	PERSON
Eiichirō	PERSON
Oda	PERSON
.	O
Dragon	O
Ball	O
Evolution	O
est	O
malmené	O
par	O
les	O
critiques	O
et	O
par	O
les	O
fans	O
mais	O
l'	O
auteur	O
reste	O
silencieux	O
.	O
Avec	PERSON
Dragon	PERSON
Ball	PERSON
il	PERSON
devient	O
un	O
des	O
plus	O
importants	O
représentants	O
du	O
shōnen	O
manga	O
,	O
et	O
comme	O
Osamu	PERSON
Tezuka	PERSON
,	O
il	PERSON
voit	PERSON
son	PERSON
style	O
graphique	O
souvent	O
copié	O
et	O
intégré	O
par	O
beaucoup	O
de	O
mangaka	O
.	O
Son	O
style	O
s'	O
adapte	O
à	O
l'	O
évolution	O
de	O
Dragon	O
Ball	O
,	O
son	O
trait	O
se	PERSON
durcit	PERSON
pendant	PERSON
cette	PERSON
période	PERSON
(	O
les	O
traits	O
anguleux	LOCATION
deviennent	O
plus	O
systématiques	PERSON
)	O
comparativement	O
à	O
un	O
style	O
plus	O
rond	O
pour	O
ses	O
premières	O
productions	O
,	O
puis	O
pour	O
Dr	O
Slump	PERSON
.	O
Alors	O
que	O
les	O
histoires	O
devenaient	O
presque	O
exclusivement	O
basées	O
sur	O
le	O
combat	O
,	O
il	PERSON
met	O
fin	PERSON
à	PERSON
la	PERSON
série	PERSON
Dragon	PERSON
Ball	PERSON
.	O
Il	O
réalise	O
ensuite	O
des	O
histoires	O
(	O
en	O
one	O
shot	O
)	O
dans	O
le	O
ton	O
de	O
ses	O
premières	O
productions	O
,	O
ou	O
de	O
certains	O
chapitres	O
de	O
Dr	O
Slump	PERSON
,	O
bien	PERSON
qu'	PERSON
ayant	O
évolué	PERSON
graphiquement	PERSON
.	O
Akira	PERSON
Toriyama	PERSON
affirme	O
souvent	O
dans	O
ses	O
messages	O
au	O
lecteur	O
être	O
profondément	O
attaché	O
à	O
sa	O
vie	O
à	O
la	O
campagne	O
japonaise	O
,	O
et	O
redouter	O
la	O
vie	O
urbaine	O
.	O
Cette	O
part	O
de	O
sa	O
personnalité	O
se	PERSON
ressent	PERSON
dans	PERSON
pratiquement	PERSON
toutes	O
les	O
histoires	O
qu'	O
il	O
a	O
créées	PERSON
:	O
le	O
héros	O
vient	O
souvent	O
de	O
la	O
campagne	O
,	O
et	O
le	PERSON
thème	PERSON
de	PERSON
l'	PERSON
enfant	PERSON
qui	PERSON
découvre	PERSON
la	PERSON
ville	LOCATION
sans	O
jamais	O
l'	O
avoir	O
vue	O
,	O
ou	O
inversement	O
de	O
la	O
personne	O
habituée	PERSON
à	PERSON
la	PERSON
ville	PERSON
qui	PERSON
subit	PERSON
les	O
aléas	O
de	O
la	O
campagne	O
est	O
souvent	O
réutilisé	O
(	O
Dr	O
Slump	PERSON
,	O
Sand	O
Land	O
,	O
Dragon	O
Ball	O
,	O
Kajika	PERSON
,	O
etc	O
.	O
Tous	O
ses	O
mangas	O
se	O
passent	O
dans	O
un	O
même	O
monde	O
à	O
différentes	O
époques	O
,	O
The	O
World	O
,	O
une	O
planète	O
jumelle	O
à	O
la	LOCATION
Terre	LOCATION
.	O
Ces	O
séries	O
sont	O
inspirées	O
des	O
œuvres	O
d'	O
Akira	PERSON
Toriyama	PERSON
.	O
Ces	O
films	O
d'	O
animation	O
sont	O
inspirés	O
des	O
œuvres	O
d'	O
Akira	PERSON
Toriyama	PERSON
.	O
Ces	PERSON
jeux	PERSON
vidéo	PERSON
sont	O
inspirés	O
des	O
œuvres	O
d'	O
Akira	PERSON
Toriyama	PERSON
.	O
Ces	O
films	O
d'	O
animation	O
sont	O
inspirés	O
des	O
œuvres	O
d'	O
Akira	PERSON
Toriyama	PERSON
.	O
Ces	O
films	O
sont	O
inspirés	O
des	O
œuvres	O
d'	O
Akira	PERSON
Toriyama	PERSON
.	O
Ces	O
mangas	O
sont	O
adaptés	PERSON
des	PERSON
jeux	PERSON
vidéo	PERSON
créés	PERSON
graphiquement	PERSON
par	O
Akira	PERSON
Toriyama	PERSON
.	O
Ces	O
films	O
d'	O
animation	O
sont	O
inspirés	O
des	O
œuvres	O
d'	O
Akira	PERSON
Toriyama	PERSON
.	O
