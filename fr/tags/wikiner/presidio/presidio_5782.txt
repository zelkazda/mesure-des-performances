Le	PERSON
gallo	PERSON
ou	O
langue	O
gallèse	O
,	O
est	O
une	O
langue	O
d'	O
oïl	O
traditionnelle	O
du	PERSON
nord-ouest	PERSON
de	PERSON
la	PERSON
France	PERSON
,	O
en	O
Haute-Bretagne	O
,	O
dans	O
le	LOCATION
Maine	LOCATION
,	O
l'	O
Anjou	O
ainsi	O
qu'	O
une	O
partie	O
de	O
la	O
Basse-Normandie	O
.	O
Une	LOCATION
des	LOCATION
variantes	LOCATION
,	O
essentiellement	O
basée	LOCATION
sur	O
les	O
parlers	O
et	O
prononciations	O
de	O
Haute	O
Bretagne	O
,	O
a	O
été	O
normalisée	O
pour	O
l'	O
enseignement	O
.	O
Cette	O
version	O
est	O
reconnue	O
depuis	O
2004	O
comme	O
l'	O
une	O
des	O
"	O
langues	O
de	O
la	O
Bretagne	O
"	O
.	O
C'	O
est-à-dire	O
en	O
langue	O
bretonne	O
,	O
ceux	O
qui	O
ne	O
parlent	O
pas	O
breton	O
,	O
ceux	O
qui	O
parlent	O
français	O
;	O
à	O
l'	O
époque	O
le	O
dialecte	O
roman	O
parlé	O
par	O
les	O
habitants	O
vivants	O
dans	O
cette	O
partie	O
occidentale	O
de	O
la	O
Neustrie	O
qui	O
allait	O
devenir	O
l'	O
est	O
de	O
la	O
Haute-Bretagne	O
après	O
le	O
traité	O
d'	O
Angers	O
en	O
851	O
.	O
Henriette	PERSON
Walter	PERSON
avait	PERSON
conduit	PERSON
en	O
enquête	O
en	O
1986	O
qui	LOCATION
montrait	LOCATION
qu'	O
à	O
peine	O
plus	O
de	O
4	O
%	O
l'	O
avaient	O
employé	O
depuis	O
toujours	O
(	O
dans	O
les	O
Côtes-d'Armor	LOCATION
)	O
,	O
et	O
le	O
tiers	O
d'	O
entre-eux	O
le	O
trouvait	O
"	O
chargé	O
d'	O
une	O
signification	O
plutôt	O
péjorative	O
"	O
,	O
et	O
que	O
le	O
terme	O
"	O
patois	O
"	O
était	O
largement	O
majoritaire	O
.	O
Le	O
gallo	O
n'	O
est	O
pas	O
différent	O
des	O
autres	O
langues	O
d'	O
oïl	O
du	O
nord	O
de	O
la	O
France	O
.	O
Cependant	PERSON
ce	PERSON
dialecte	PERSON
prendra	PERSON
dans	PERSON
la	PERSON
partie	PERSON
occidentale	PERSON
de	PERSON
la	PERSON
Neustrie	PERSON
le	PERSON
nom	PERSON
de	PERSON
gallo	PERSON
ou	PERSON
de	PERSON
langue	PERSON
gallèse	O
.	O
D'	O
abord	O
confiné	O
à	O
l'	O
est	O
de	PERSON
la	PERSON
Bretagne	PERSON
,	O
le	O
gallo	O
s'	O
est	O
peu	PERSON
à	PERSON
peu	PERSON
étendu	PERSON
vers	PERSON
l'	O
ouest	O
,	O
remplaçant	PERSON
peu	PERSON
à	PERSON
peu	PERSON
la	PERSON
langue	O
locale	O
.	O
Le	O
gallo	O
de	O
la	O
frontière	O
linguistique	O
vers	O
l'	O
angevin	PERSON
(	O
Anjou	O
)	O
,	O
le	PERSON
tourangeot	PERSON
(	O
Touraine	O
)	O
ou	O
le	O
meuniot	O
(	O
Maine	LOCATION
)	O
est	O
moins	O
différencié	O
.	O
Une	O
certaine	O
proximité	O
entre	O
le	O
gallo	O
et	O
les	O
différentes	O
langues	O
de	O
l'	O
ouest	O
de	PERSON
la	PERSON
France	O
existe	O
mais	O
est	O
de	O
moins	O
en	O
moins	O
évidente	O
au	O
fur	O
et	O
à	O
mesure	O
qu'	O
on	O
s'	O
éloigne	O
des	O
frontières	O
orientales	O
de	O
la	O
Bretagne	O
.	O
En	O
effet	O
,	O
depuis	O
1982	O
,	O
le	LOCATION
gallo	LOCATION
est	LOCATION
proposé	O
en	O
option	O
facultative	O
de	O
langue	O
au	O
baccalauréat	O
dans	O
les	O
quatre	O
départements	O
de	O
la	O
Région	O
Bretagne	O
.	O
Par	O
ailleurs	O
,	O
certaines	O
collectivités	O
territoriales	O
de	O
Haute-Bretagne	O
ont	O
mis	O
en	O
place	O
une	O
signalisation	O
bilingue	O
français-gallo	O
,	O
tandis	O
que	O
d'	O
autres	O
privilégient	O
le	O
bilinguisme	O
français-breton	O
aux	O
dépens	O
du	O
gallo	O
.	O
L'	O
université	O
de	O
Nantes	O
offre	O
également	O
dans	O
son	O
département	O
linguistique	O
,	O
un	O
programme	O
pédagogique	O
d'	O
enseignement	O
du	PERSON
gallo	PERSON
Cette	O
orientation	O
bretonnante	O
est	O
contestée	O
et	O
d'	O
autres	O
codifications	O
sont	O
proposées	O
plus	O
en	O
accord	O
avec	O
les	O
origines	O
du	PERSON
Gallo	PERSON
.	O
Certaines	O
associations	O
font	O
un	O
travail	O
de	O
normalisation	O
du	O
Gallo	O
.	O
On	O
dit	O
tout	O
simplement	O
le	O
"	O
patois	O
"	O
(	O
comme	O
partout	O
en	O
France	LOCATION
)	O
ou	O
"	O
parler	O
nantais	O
"	O
.	O
Le	O
parler	O
nantais	O
se	O
distingue	PERSON
par	PERSON
les	PERSON
apports	PERSON
multiples	PERSON
dû	O
à	O
la	O
situation	O
géographique	O
de	O
Nantes	O
,	O
la	LOCATION
cité	LOCATION
des	LOCATION
ducs	LOCATION
étant	O
située	O
au	PERSON
confins	PERSON
de	PERSON
plusieurs	PERSON
régions	PERSON
historiques	O
(	O
Bretagne	O
,	O
Anjou	O
et	O
Poitou	O
)	O
.	O
La	O
vallée	O
de	O
la	O
Loire	O
fut	O
notamment	O
un	O
lieu	O
d'	O
échange	O
important	O
,	O
autant	O
vers	O
l'	O
océan	O
que	O
vers	O
l'	O
intérieur	O
des	O
terres	O
.	O
