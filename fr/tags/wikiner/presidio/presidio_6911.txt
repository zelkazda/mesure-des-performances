Après	PERSON
sa	PERSON
démission	O
de	O
l'	O
armée	O
avec	O
le	O
grade	O
de	O
sous-lieutenant	O
en	O
1925	O
,	O
il	PERSON
retourna	PERSON
en	O
Sicile	LOCATION
alternant	O
villégiature	O
,	O
voyages	O
(	O
toujours	O
en	O
compagnie	O
de	O
sa	O
mère	O
)	O
et	O
des	O
études	O
sur	O
la	O
littérature	O
.	O
C'	O
est	O
au	O
retour	O
de	O
ce	O
voyage	O
qu'	LOCATION
il	LOCATION
aurait	LOCATION
commencé	LOCATION
à	O
écrire	O
Le	O
Guépard	O
,	O
qu'	LOCATION
il	LOCATION
termina	O
deux	O
ans	O
plus	O
tard	O
,	O
en	O
1956	O
.	O
Le	O
roman	O
fut	O
publié	O
à	O
titre	O
posthume	O
en	O
1958	O
,	O
lorsqu'	O
on	O
envoya	O
le	O
manuscrit	O
à	O
Giorgio	PERSON
Bassani	PERSON
qui	PERSON
le	PERSON
publia	PERSON
,	O
rattrapant	O
ainsi	O
les	O
erreurs	O
de	O
jugement	O
des	O
éditeurs	O
précédents	O
.	O
En	O
1959	O
,	O
le	O
roman	O
remporta	O
le	O
Prix	O
Strega	O
.	O
Dans	O
Le	O
Guépard	O
,	O
il	PERSON
se	PERSON
montre	O
un	O
observateur	O
avisé	O
de	O
la	O
haute	O
société	O
sicilienne	O
,	O
de	O
ses	O
rituels	O
sociaux	O
et	O
de	O
la	O
façon	O
dont	O
ses	PERSON
membres	PERSON
essaient	PERSON
de	PERSON
suivre	PERSON
l'	PERSON
évolution	O
sociale	O
et	O
politique	O
.	O
