StarOffice	O
est	O
une	O
suite	PERSON
bureautique	PERSON
propriétaire	PERSON
(	O
et	O
commerciale	O
)	O
,	O
multiplates-formes	O
(	O
Windows	O
,	O
GNU/Linux	O
,	O
Solaris	O
)	O
éditée	O
par	O
Sun	O
Microsystems	O
et	O
basée	LOCATION
,	O
à	O
partir	O
de	O
la	O
version	O
6	O
,	O
sur	O
le	O
projet	O
OpenOffice.org	O
.	O
La	O
version	O
5.1	O
était	O
utilisable	O
librement	O
pour	O
tout	O
utilisateur	O
privé	O
,	O
particulièrement	O
les	O
étudiants	O
,	O
en	O
obtenant	O
un	PERSON
numéro	PERSON
de	PERSON
série	PERSON
directement	O
délivré	PERSON
par	O
Sun	O
.	O
Puis	O
,	O
avec	O
la	O
version	O
5.2	O
en	O
juin	O
2000	O
,	O
Sun	O
a	O
offert	O
la	O
suite	O
gratuitement	O
à	O
tout	O
le	O
monde	O
,	O
dans	LOCATION
le	LOCATION
but	O
avoué	O
de	O
faire	O
de	O
l'	O
ombre	O
à	O
Microsoft	O
.	O
Cette	O
dernière	O
continue	O
à	O
être	O
librement	O
téléchargeable	O
et	O
utilisable	O
,	O
son	PERSON
développement	PERSON
se	PERSON
faisant	O
indépendamment	O
de	O
la	O
section	O
StarOffice	O
qui	O
,	O
lui	O
,	O
se	PERSON
repose	O
sur	LOCATION
les	O
avancées	O
apportées	O
par	O
OpenOffice.org	O
dont	O
le	O
code	O
est	O
incorporé	LOCATION
à	O
la	O
solution	O
commerciale	O
.	O
StarOffice	O
9	O
a	O
été	O
publié	O
en	O
novembre	O
2008	O
.	O
Cette	O
version	O
est	O
basée	O
sur	O
OpenOffice.org	O
3.0	O
.	O
