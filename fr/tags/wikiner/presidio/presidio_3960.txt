André	PERSON
Breton	PERSON
est	PERSON
un	PERSON
écrivain	PERSON
,	O
poète	PERSON
,	O
essayiste	O
et	O
théoricien	PERSON
du	PERSON
surréalisme	PERSON
,	O
né	O
à	O
Tinchebray	O
dans	O
l'	O
Orne	PERSON
,	O
le	O
19	O
février	O
1896	O
,	O
mort	O
à	O
Paris	LOCATION
le	O
28	O
septembre	O
1966	O
.	O
Il	O
est	O
connu	O
particulièrement	O
pour	O
ses	O
livres	PERSON
Nadja	PERSON
(	O
1928	O
)	O
,	O
L'	O
Amour	O
fou	O
(	O
1937	O
)	O
,	O
et	O
les	O
différents	O
Manifestes	PERSON
du	PERSON
surréalisme	PERSON
.	O
Fils	O
unique	O
d'	O
une	O
famille	O
de	O
la	O
petite	O
bourgeoisie	O
catholique	O
dont	O
la	O
mère	O
impose	O
une	O
éducation	O
rigide	O
,	O
André	PERSON
Breton	PERSON
passe	O
une	O
enfance	O
sans	O
histoire	O
à	O
Pantin	O
,	O
dans	O
la	LOCATION
banlieue	PERSON
nord-est	PERSON
de	PERSON
Paris	PERSON
.	O
Ce	O
dernier	O
les	O
publie	O
et	O
met	O
Breton	O
en	O
relation	O
avec	PERSON
Paul	PERSON
Valéry	PERSON
.	O
À	O
la	O
déclaration	O
de	O
guerre	O
,	O
le	O
3	O
août	O
,	O
il	LOCATION
est	LOCATION
avec	LOCATION
ses	O
parents	O
à	O
Lorient	O
.	O
Il	O
a	O
pour	O
seul	PERSON
livre	PERSON
un	PERSON
recueil	PERSON
de	PERSON
poèmes	PERSON
d'	PERSON
Arthur	PERSON
Rimbaud	PERSON
qu'	O
il	PERSON
connait	PERSON
mal	PERSON
.	O
La	O
lecture	O
d'	O
articles	O
d'	O
intellectuels	O
renommés	O
comme	O
Maurice	PERSON
Barrès	PERSON
ou	PERSON
Henri	PERSON
Bergson	PERSON
,	O
le	PERSON
conforte	PERSON
dans	O
son	O
dégoût	PERSON
du	PERSON
nationalisme	PERSON
ambiant	O
.	O
Il	O
est	O
ensuite	O
affecté	O
à	O
l'	O
hôpital	O
de	O
Nantes	O
(	O
Loire-Atlantique	O
)	O
comme	O
interne	O
en	O
médecine	O
.	O
En	O
février	O
ou	O
mars	O
1916	O
,	O
il	O
rencontre	O
un	O
soldat	O
en	O
convalescence	O
:	O
Jacques	PERSON
Vaché	PERSON
.	O
Le	O
20	O
novembre	O
1916	O
,	O
Breton	O
est	O
envoyé	O
au	O
front	O
comme	O
brancardier	O
.	O
Soupault	O
lui	O
fait	O
découvrir	O
Les	O
Chants	O
de	O
Maldoror	O
de	O
Lautréamont	O
,	O
qui	O
provoquent	O
chez	O
lui	O
une	O
grande	O
émotion	O
.	O
Dans	PERSON
une	PERSON
lettre	PERSON
de	PERSON
juillet	PERSON
1918	PERSON
à	O
Fraenkel	O
,	O
Breton	O
évoque	O
le	O
projet	O
en	O
commun	O
avec	PERSON
Aragon	PERSON
et	PERSON
Soupault	PERSON
,	O
d'	O
un	PERSON
livre	PERSON
sur	PERSON
quelques	PERSON
peintres	PERSON
comme	O
Giorgio	PERSON
De	PERSON
Chirico	PERSON
,	O
André	PERSON
Derain	PERSON
,	O
Juan	PERSON
Gris	PERSON
,	O
Henri	PERSON
Matisse	PERSON
,	O
Picasso	PERSON
,	O
Henri	PERSON
Rousseau	PERSON
...	O
dans	O
lesquels	O
serait	O
"	O
contée	O
à	O
la	O
manière	O
anglaise	O
"	O
la	O
vie	O
de	O
l'	O
artiste	O
,	O
par	O
Soupault	O
,	O
l'	O
analyse	O
des	O
œuvres	O
,	O
par	O
Aragon	O
et	O
quelques	O
réflexions	O
sur	O
l'	O
art	O
,	O
par	O
Breton	PERSON
lui-même	PERSON
.	O
Au	O
mois	O
de	O
janvier	O
1919	O
,	O
profondément	O
affecté	O
par	O
la	O
mort	PERSON
de	PERSON
Jacques	PERSON
Vaché	PERSON
,	O
Breton	PERSON
croit	PERSON
voir	PERSON
en	O
Tristan	PERSON
Tzara	PERSON
la	PERSON
réincarnation	PERSON
de	PERSON
l'	PERSON
esprit	PERSON
de	PERSON
révolte	PERSON
de	PERSON
son	PERSON
ami	PERSON
:	O
"	O
Je	O
ne	O
savais	O
plus	O
de	PERSON
qui	PERSON
attendre	PERSON
le	PERSON
courage	PERSON
que	O
vous	O
montrez	PERSON
.	O
"	O
Les	PERSON
Champs	PERSON
magnétiques	O
"	O
,	O
écrit	PERSON
en	O
mai	O
et	O
juin	O
1919	O
,	O
n'	O
est	O
publié	O
qu'	O
un	O
an	O
plus	O
tard	O
.	O
Le	O
23	O
janvier	O
1920	O
,	O
Tristan	PERSON
Tzara	PERSON
arrive	O
enfin	O
à	O
Paris	LOCATION
.	O
La	O
déception	O
de	O
Breton	O
de	O
voir	O
apparaître	O
un	O
être	O
"	O
si	O
peu	O
charismatique	O
"	O
[	O
réf.	O
nécessaire	O
]	O
est	O
à	O
la	O
hauteur	O
de	O
ce	O
qu'	O
il	O
en	O
attendait	O
.	O
À	O
la	O
fin	O
de	O
l'	O
année	O
,	O
Breton	O
est	O
engagé	O
par	O
le	O
couturier	O
,	O
bibliophile	O
,	O
et	O
amateur	O
d'	O
art	O
moderne	O
Jacques	PERSON
Doucet	PERSON
.	O
Entre	PERSON
autres	PERSON
,	O
Breton	PERSON
lui	PERSON
fera	PERSON
acheter	PERSON
le	O
tableau	O
"	O
Les	O
Demoiselles	O
d'	O
Avignon	O
"	O
de	O
Picasso	O
.	O
L'	O
opposition	O
de	PERSON
Tzara	PERSON
en	O
empêche	O
la	O
tenue	O
.	O
Ces	PERSON
états	PERSON
de	PERSON
sommeil	PERSON
forcé	PERSON
vont	PERSON
révéler	PERSON
les	PERSON
étonnantes	PERSON
facultés	PERSON
d	PERSON
'	PERSON
"	O
improvisation	O
"	O
de	O
Benjamin	PERSON
Péret	PERSON
et	PERSON
de	PERSON
Desnos	PERSON
.	O
À	O
la	O
fin	O
février	O
1923	O
,	O
doutant	O
de	O
la	O
sincérité	O
des	O
uns	O
et	O
craignant	O
pour	O
la	O
santé	O
mentale	O
des	O
autres	O
,	O
Breton	O
décide	O
d'	O
arrêter	PERSON
l'	O
expérience	O
.	O
Breton	O
semble	O
fatigué	O
de	O
tout	O
:	O
il	O
considère	O
les	O
activités	O
de	O
journalisme	O
d'	O
Aragon	O
et	O
Desnos	O
,	O
certes	O
rémunératrices	O
,	O
comme	O
une	O
perte	PERSON
de	PERSON
temps	PERSON
,	O
les	O
écrits	O
de	O
Picabia	O
le	O
déçoivent	O
,	O
il	O
s'	O
emporte	PERSON
contre	O
les	O
projets	O
trop	O
littéraires	O
de	O
ses	O
amis	O
--	O
"	O
toujours	O
des	O
romans	O
!	O
Dans	PERSON
un	PERSON
entretien	PERSON
avec	PERSON
Roger	PERSON
Vitrac	PERSON
,	O
il	PERSON
confie	PERSON
même	PERSON
son	PERSON
intention	O
de	PERSON
ne	PERSON
plus	O
écrire	O
.	O
Cependant	O
,	O
au	PERSON
cours	PERSON
de	PERSON
l'	PERSON
été	O
suivant	O
,	O
il	PERSON
écrit	PERSON
la	PERSON
plupart	O
des	O
poèmes	O
de	O
Clair	O
de	O
terre	O
.	O
Quelques	PERSON
jours	O
après	LOCATION
,	O
le	O
groupe	O
publie	O
le	O
pamphlet	O
"	O
Un	O
cadavre	O
"	O
,	O
écrit	O
en	O
réaction	O
aux	O
funérailles	O
nationales	O
faites	O
à	O
Anatole	O
France	O
:	O
"	O
Loti	PERSON
,	O
Barrès	PERSON
,	O
France	LOCATION
,	O
marquons	O
tout	O
de	O
même	O
d'	O
un	O
beau	O
signe	O
blanc	O
l'	O
année	O
qui	O
coucha	O
ces	O
trois	O
sinistres	O
bonshommes	O
:	O
l'	O
idiot	O
,	O
le	O
traitre	O
et	O
le	O
policier	O
.	O
Avec	LOCATION
France	LOCATION
,	O
c'	O
est	O
un	PERSON
peu	PERSON
de	PERSON
la	PERSON
servilité	O
humaine	PERSON
qui	PERSON
s'	PERSON
en	O
va	O
.	O
Breton	O
radicalise	O
son	O
action	O
et	O
sa	O
position	O
politique	O
.	O
Breton	O
est	O
affecté	O
à	O
une	O
cellule	O
d'	O
employés	O
du	O
gaz	O
.	O
Le	O
4	O
octobre	O
1926	O
,	O
il	O
rencontre	O
Nadja	O
.	O
Elle	O
ordonne	O
à	O
Breton	O
d'	O
écrire	O
"	O
un	O
roman	O
sur	O
moi	O
.	O
Bien	PERSON
qu'	PERSON
elle	PERSON
soit	PERSON
la	PERSON
maîtresse	PERSON
d'	PERSON
Emmanuel	PERSON
Berl	PERSON
,	O
elle	O
partage	O
avec	PERSON
Breton	PERSON
une	O
aventure	O
passionnée	PERSON
et	PERSON
orageuse	PERSON
.	O
Elle	O
demande	O
à	O
Breton	O
de	O
divorcer	O
d'	O
avec	O
Simone	O
,	O
ce	LOCATION
à	LOCATION
quoi	LOCATION
il	LOCATION
consent	O
,	O
mais	PERSON
freinée	PERSON
dans	PERSON
ses	O
désirs	O
d'	O
aventure	O
par	O
son	O
goût	PERSON
du	PERSON
confort	PERSON
et	O
de	O
la	O
sécurité	O
matérielle	O
,	O
elle	PERSON
épouse	PERSON
Berl	PERSON
,	O
sans	O
pour	O
autant	O
rompre	O
définitivement	PERSON
avec	PERSON
Breton	PERSON
.	O
Pour	O
elle	O
,	O
Breton	O
ajoute	O
une	O
troisième	LOCATION
partie	O
à	O
"	O
Nadja	PERSON
"	O
.	O
Breton	O
est	O
alors	O
plongé	O
dans	O
la	O
lecture	O
de	PERSON
Marx	PERSON
,	O
Engels	PERSON
et	PERSON
Hegel	PERSON
;	O
et	O
la	O
question	O
du	PERSON
réel	PERSON
dans	O
sa	O
dimension	O
politique	O
ainsi	O
que	O
celle	O
de	O
l'	O
engagement	O
de	O
l'	O
individu	O
occupent	O
sa	PERSON
réflexion	PERSON
comme	PERSON
le	PERSON
précise	PERSON
l'	PERSON
incipit	PERSON
du	PERSON
livre	PERSON
.	O
Les	O
"	O
exclus	O
"	O
visés	O
par	O
le	O
texte	O
réagissent	LOCATION
en	O
publiant	O
un	O
pamphlet	O
sur	O
le	O
modèle	O
de	O
celui	O
écrit	O
contre	O
Anatole	PERSON
France	PERSON
quelques	PERSON
années	PERSON
plus	O
tôt	O
et	O
en	O
reprennent	O
le	PERSON
même	PERSON
titre	PERSON
,	O
"	O
Un	O
cadavre	O
"	O
.	O
Elle	O
avance	O
que	O
Breton	O
fait	O
allusion	O
à	O
la	O
figure	O
d'	O
Émile	O
Henry	PERSON
qui	PERSON
peu	PERSON
après	PERSON
son	PERSON
arrestation	O
a	O
prétendu	O
s'	O
appeler	O
"	O
Breton	O
"	O
et	O
suggère	O
qu	O
'	O
"	O
une	O
sorte	O
de	O
lent	O
transfert	O
,	O
de	O
nature	O
presque	O
onirique	O
,	O
cheminant	O
dans	O
les	O
zones	O
les	O
plus	O
mystérieuses	O
de	O
la	O
sensibilité	LOCATION
,	O
aurait	O
ainsi	O
préparé	O
en	O
[	O
Breton	O
]	O
la	O
tentation	O
fugitive	O
de	O
s'	O
identifier	O
à	O
l'	O
ange	O
exterminateur	O
de	O
l'	O
anarchie	O
"	O
.	O
Le	PERSON
titre	PERSON
de	PERSON
la	PERSON
revue	PERSON
est	O
d'	O
Aragon	O
.	O
Mais	PERSON
à	PERSON
la	PERSON
suite	PERSON
d'	PERSON
une	O
violente	PERSON
altercation	PERSON
avec	PERSON
Ilya	PERSON
Ehrenbourg	PERSON
,	O
ce	LOCATION
dernier	LOCATION
,	O
délégué	O
de	O
la	O
représentation	O
soviétique	O
,	O
ayant	PERSON
calomnié	PERSON
les	O
surréalistes	LOCATION
,	O
la	O
participation	O
de	PERSON
Breton	PERSON
est	PERSON
annulée	PERSON
.	O
Il	PERSON
fallut	PERSON
le	PERSON
suicide	PERSON
de	PERSON
René	PERSON
Crevel	PERSON
pour	O
que	O
les	O
organisateurs	O
concèdent	O
à	O
Éluard	O
de	O
lire	O
le	O
texte	O
.	O
De	O
cette	O
rencontre	O
et	O
des	O
premiers	O
moments	O
de	O
leur	O
amour	O
,	O
Breton	PERSON
écrit	PERSON
le	PERSON
récit	PERSON
"	O
L'	O
Amour	O
fou	O
"	O
.	O
De	O
leur	O
union	O
naît	PERSON
une	PERSON
fille	PERSON
,	O
Aube	PERSON
.	O
Cette	O
initiative	O
est	O
à	O
l'	O
origine	O
de	O
la	O
rupture	O
avec	PERSON
Éluard	PERSON
.	O
Breton	O
embarque	O
à	O
destination	O
de	O
New	LOCATION
York	LOCATION
le	O
25	O
mars	O
1941	O
avec	O
Wifredo	PERSON
Lam	PERSON
et	O
Claude	PERSON
Lévi-Strauss	PERSON
.	O
À	O
l'	O
escale	O
de	O
Fort-de-France	O
(	O
Martinique	O
)	O
,	O
Breton	O
est	O
interné	O
puis	PERSON
libéré	PERSON
sous	O
caution	O
.	O
Il	O
rencontre	O
Aimé	O
Césaire	O
.	O
Le	O
10	O
décembre	O
1943	O
,	O
Breton	O
rencontre	O
Elisa	O
Claro	O
.	O
Ensemble	O
,	O
ils	O
voyagent	O
jusqu'	PERSON
à	O
la	O
péninsule	O
de	O
la	O
Gaspésie	O
,	O
à	O
l'	O
extrémité	PERSON
sud-est	PERSON
du	PERSON
Québec	PERSON
.	O
Pour	O
régler	O
les	O
questions	O
pratiques	O
de	O
divorce	O
et	O
de	O
remariage	O
,	O
Breton	O
et	O
Élisa	PERSON
se	PERSON
rendent	O
à	O
Reno	LOCATION
dans	O
le	O
Nevada	O
.	O
En	O
décembre	O
1945	O
,	O
à	O
l'	O
invitation	O
de	PERSON
Pierre	PERSON
Mabille	PERSON
,	O
nommé	PERSON
attaché	PERSON
culturel	O
à	O
Pointe-à-Pitre	O
,	O
Breton	O
se	O
rend	O
en	O
Haïti	O
pour	O
y	O
prononcer	O
une	O
série	O
de	O
conférences	O
.	O
Le	O
25	O
mai	O
1946	O
,	O
il	O
est	O
de	O
retour	O
en	O
France	LOCATION
.	O
Dès	PERSON
le	PERSON
mois	PERSON
de	PERSON
juin	PERSON
,	PERSON
il	O
est	O
invité	O
à	O
la	O
soirée	O
d'	O
hommages	O
rendus	O
à	O
Antonin	PERSON
Artaud	PERSON
.	O
En	O
1954	O
,	O
un	O
projet	O
d'	O
action	O
commune	O
avec	O
l'	O
Internationale	O
lettriste	O
contre	O
la	O
célébration	O
du	O
centenaire	O
de	O
Rimbaud	O
échoue	O
lorsque	O
les	O
surréalistes	O
refusent	O
la	O
"	O
phraséologie	PERSON
marxiste	O
"	O
proposée	O
par	O
les	O
lettristes	O
dans	O
le	O
tract	O
commun	O
.	O
Breton	O
est	O
alors	O
pris	O
à	O
partie	O
par	O
Gil	PERSON
Joseph	PERSON
Wolman	PERSON
et	O
Guy	PERSON
Debord	PERSON
qui	PERSON
soulignent	PERSON
dans	O
un	O
texte	O
sur	O
le	O
mode	O
allégorique	PERSON
sa	PERSON
perte	PERSON
de	PERSON
vitesse	PERSON
au	PERSON
sein	O
du	O
mouvement	O
.	O
Toute	O
sa	O
vie	O
,	O
Breton	O
a	O
tenté	O
d'	O
emprunter	O
d'	O
un	O
même	O
front	O
,	O
trois	O
chemins	O
:	O
la	PERSON
poésie	PERSON
,	O
l'	O
amour	O
,	O
la	PERSON
liberté	PERSON
.	O
"	O
Avec	PERSON
Breton	PERSON
,	O
le	O
merveilleux	O
remplace	O
les	O
exhibitions	O
nihilistes	O
et	O
l'	O
irrationnel	O
ouvre	O
les	O
portes	LOCATION
étroites	LOCATION
du	LOCATION
réel	LOCATION
sans	LOCATION
vrai	LOCATION
retour	LOCATION
au	LOCATION
symbolisme	O
"	O
,	O
Hubert	PERSON
Haddad	PERSON
.	O
Pour	O
abolir	O
les	O
conformismes	O
et	O
les	O
préjugés	O
,	O
combattre	O
le	O
rationalisme	O
,	O
Breton	O
usera	O
de	O
la	O
poésie	O
comme	O
d'	O
une	O
arme	O
aux	O
multiples	O
facettes	O
que	O
sont	O
l'	O
imagination	O
,	O
"	O
qui	O
fait	O
à	O
elle	O
seule	O
les	O
choses	O
réelles	O
"	O
,	O
l'	O
émerveillement	O
,	O
les	O
récits	O
de	O
rêves	O
et	O
les	O
surprises	O
du	PERSON
hasard	PERSON
,	O
l'	O
écriture	O
automatique	O
,	O
les	O
raccourcis	O
de	O
la	O
métaphore	O
et	O
l'	O
image	O
.	O
Pour	O
réussir	O
son	O
entreprise	O
de	O
subversion	O
poétique	O
Breton	O
s'	O
est	O
gardé	O
de	O
tout	O
travail	O
quotidien	O
alimentaire	O
,	O
allant	O
jusqu'	O
à	O
défendre	O
à	O
ses	O
amis	O
les	O
plus	O
proches	O
(	O
Aragon	O
,	O
Desnos	O
)	O
de	O
se	O
commettre	O
dans	O
le	O
journalisme	O
.	O
Pour	O
Breton	O
,	O
l'	O
amour	O
,	O
comme	O
le	O
rêve	O
,	O
est	O
une	O
merveille	LOCATION
où	O
l'	O
homme	O
retrouve	O
le	O
contact	O
avec	O
les	O
forces	O
profondes	O
.	O
Breton	PERSON
aimait	PERSON
comme	PERSON
un	PERSON
cœur	PERSON
bat	O
.	O
C'	O
est	O
là	O
son	O
signe	O
"	O
,	O
Marcel	PERSON
Duchamp	PERSON
.	O
Les	O
adversaires	O
de	O
Breton	O
l'	O
ont	O
nommé	O
,	O
par	O
dérision	O
parfois	O
,	O
avec	O
véhémence	O
souvent	O
,	O
le	LOCATION
"	O
pape	O
du	O
surréalisme	O
"	O
.	O
Et	O
Breton	O
ajoute	O
quelques	O
mots	O
d'	O
ordre	O
:	O
Pour	O
souligner	O
son	O
accord	O
avec	O
le	O
matérialisme	O
dialectique	O
,	O
il	PERSON
cite	O
Friedrich	PERSON
Engels	PERSON
:	O
"	O
La	O
causalité	PERSON
ne	PERSON
peut	PERSON
être	PERSON
comprise	O
qu'	O
en	O
liaison	O
avec	PERSON
la	PERSON
catégorie	PERSON
du	PERSON
hasard	PERSON
objectif	O
,	O
forme	O
de	O
manifestation	O
de	O
la	O
nécessité	O
"	O
.	O
"	O
Nadja	PERSON
"	O
semble	O
posséder	O
un	O
pouvoir	O
médiumnique	O
qui	O
lui	O
permet	O
de	O
prédire	O
certains	O
événements	O
.	O
Ainsi	O
annonce-t-elle	O
que	O
telle	O
fenêtre	O
va	O
s'	O
éclairer	O
d'	O
une	O
lumière	O
rouge	O
,	O
ce	LOCATION
qui	LOCATION
se	LOCATION
produit	O
presque	O
immédiatement	O
aux	O
yeux	O
d'	O
un	O
Breton	O
émerveillé	O
.	O
L	O
'	O
"	O
humour	O
noir	O
"	O
,	O
expression	O
dont	O
le	O
sens	O
moderne	O
a	O
été	O
construit	O
par	O
Breton	O
,	O
est	O
un	O
des	O
ressorts	O
essentiels	O
du	PERSON
surréalisme	PERSON
.	O
Il	O
publie	O
en	O
1940	O
une	O
Anthologie	O
de	O
l'	O
humour	O
noir	O
.	O
