Le	O
territoire	O
de	O
Saint-Jean-des-Essartiers	O
a	O
une	O
forme	O
très	O
particulière	O
et	O
complexe	O
.	O
En	O
effet	O
,	O
si	O
la	O
plus	O
grande	O
partie	O
est	O
à	O
l'	O
ouest	O
du	O
territoire	O
de	O
la	O
petite	O
commune	O
des	O
Loges	O
,	O
il	PERSON
contourne	PERSON
cette	O
commune	O
,	O
l'	O
englobant	O
ainsi	O
à	O
l'	O
ouest	O
,	O
au	O
nord	O
et	O
à	O
l'	O
est	O
.	O
C'	O
est	O
à	O
l'	O
extrême	O
sud-est	PERSON
que	PERSON
le	O
découpage	O
est	O
le	O
plus	O
complexe	O
:	O
le	O
territoire	O
de	O
Saint-Jean	O
jouxte	O
la	O
commune	O
de	O
Cahagnes	O
,	O
mais	O
également	O
par	O
un	O
angle	O
Saint-Pierre-du-Fresne	PERSON
,	O
créant	O
ainsi	O
une	O
enclave	O
d'	O
environ	O
150	O
hectares	O
de	O
Cahagnes	O
.	O
Quelques	PERSON
hectomètres	O
plus	O
à	O
l'	O
ouest	O
,	O
Saint-Jean	PERSON
jouxte	PERSON
également	PERSON
un	PERSON
appendice	PERSON
de	PERSON
La	PERSON
Ferrière-au-Doyen	PERSON
,	O
commune	O
associée	O
à	O
Saint-Martin-des-Besaces	O
.	O
La	O
complexité	O
est	O
encore	O
plus	O
en	O
évidence	O
sur	O
l'	O
aire	O
de	O
Cahagnes	O
de	O
l'	O
autoroute	O
A84	O
.	O
À	LOCATION
ce	LOCATION
niveau	LOCATION
,	O
l'	O
enclave	O
de	PERSON
Cahagnes	PERSON
possède	PERSON
également	PERSON
un	PERSON
appendice	PERSON
.	O
De	PERSON
ce	PERSON
fait	PERSON
,	O
l'	O
entrée	O
de	O
l'	O
aire	PERSON
est	O
sur	O
Saint-Jean	O
,	O
l'	O
essentiel	O
des	O
places	O
de	O
parking	O
et	O
l'	O
autoroute	O
à	O
ce	O
niveau	O
est	O
sur	O
Cahagnes	O
,	O
et	O
l'	O
on	O
réempreinte	O
l'	O
autoroute	O
sur	O
Saint-Jean	O
.	O
Saint-Jean-des-Essartiers	O
a	O
compté	O
jusqu'	O
à	O
506	O
habitants	O
en	O
1846	O
.	O
