C'	O
est	O
un	O
affluent	O
du	PERSON
Fier	PERSON
en	O
rive	O
gauche	O
,	O
donc	PERSON
un	PERSON
sous-affluent	PERSON
du	O
Rhône	LOCATION
.	O
Le	O
Chéran	O
prend	O
sa	O
source	O
dans	O
le	O
Massif	O
des	O
Bauges	O
,	O
dans	O
la	O
commune	O
de	O
Verrens-Arvey	O
,	O
en	O
Savoie	LOCATION
.	O
Il	O
se	O
jette	PERSON
dans	PERSON
le	PERSON
Fier	PERSON
,	O
après	PERSON
un	PERSON
parcours	O
de	O
54	O
km	O
.	O
Il	O
arrose	O
notamment	O
Le	O
Châtelard	O
,	O
Alby-sur-Chéran	O
et	O
Rumilly	O
.	O
Le	O
bassin	O
versant	O
du	O
Chéran	O
est	O
en	O
grande	O
partie	O
inclus	O
dans	O
le	O
parc	O
naturel	O
régional	O
du	O
Massif	O
des	O
Bauges	O
.	O
Le	O
débit	O
du	O
Chéran	O
a	O
été	O
observé	O
durant	O
une	O
période	O
de	O
59	O
ans	O
(	O
1950-2008	O
)	O
,	O
à	O
Allèves	O
,	O
localité	O
du	O
département	O
de	O
la	O
Haute-Savoie	O
située	O
malheureusement	O
à	O
une	O
vingtaine	O
de	O
kilomètres	O
de	O
son	O
confluent	O
avec	PERSON
le	PERSON
Fier	PERSON
.	O
Le	O
débit	O
moyen	O
interannuel	O
ou	O
module	O
de	O
la	O
rivière	O
à	O
Allèves	O
est	O
de	O
7,77	O
m³	O
par	O
seconde	O
.	O
Le	O
Chéran	O
présente	O
des	O
fluctuations	O
saisonnières	O
de	PERSON
débit	PERSON
bien	PERSON
marquées	PERSON
,	O
comme	O
généralement	O
en	O
milieu	O
alpestre	O
.	O
Le	PERSON
débit	PERSON
instantané	PERSON
maximal	O
enregistré	O
à	O
Allèves	O
a	O
été	O
de	O
250	O
m³	O
par	O
seconde	O
le	O
1er	O
octobre	O
1960	O
,	O
tandis	O
que	O
la	O
valeur	O
journalière	PERSON
maximale	PERSON
était	PERSON
de	PERSON
148	PERSON
m³	PERSON
par	PERSON
seconde	PERSON
le	PERSON
15	O
février	O
1990	O
.	O
Le	O
Chéran	O
est	O
une	O
rivière	O
très	O
abondante	O
.	O
La	O
lame	O
d'	O
eau	PERSON
écoulée	PERSON
dans	O
son	O
bassin	PERSON
versant	PERSON
est	PERSON
de	PERSON
987	PERSON
millimètres	PERSON
annuellement	PERSON
,	O
ce	LOCATION
qui	LOCATION
est	LOCATION
trois	O
fois	O
supérieur	O
à	O
la	O
moyenne	O
d'	O
ensemble	O
de	PERSON
la	PERSON
France	O
tous	O
bassins	O
confondus	O
,	O
mais	PERSON
tout	PERSON
à	O
fait	O
normal	O
comparé	O
aux	O
divers	O
cours	O
d'	O
eau	O
de	O
la	O
région	O
des	O
préalpes	O
de	O
Savoie	O
,	O
généralement	O
très	O
abondants	O
.	O
C'	O
est	O
de	O
plus	O
nettement	O
supérieur	O
à	O
la	O
moyenne	O
du	O
bassin	O
du	O
Rhône	O
.	O
Le	O
Chéran	O
est	O
renommé	O
pour	O
contenir	O
des	O
paillettes	O
d'	O
or	O
.	O
En	O
1867	O
,	O
un	O
gardien	O
de	O
chèvres	O
trouva	O
une	O
pépite	O
de	O
43,50	O
grammes	O
près	PERSON
du	PERSON
vieux	PERSON
pont	PERSON
d'	O
Alby-sur-Chéran	PERSON
,	O
un	O
endroit	O
dangereux	O
.	O
