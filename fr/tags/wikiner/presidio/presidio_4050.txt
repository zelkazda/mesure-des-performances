Insomnie	O
est	O
un	PERSON
roman	PERSON
de	PERSON
Stephen	PERSON
King	PERSON
publié	O
en	O
1994	O
aux	O
États-Unis	O
sous	O
le	O
titre	O
Insomnia	O
.	O
Il	O
est	O
paru	O
en	O
France	LOCATION
en	O
1995	O
chez	PERSON
Albin	PERSON
Michel	PERSON
.	PERSON
En	O
parallèle	O
,	O
la	O
visite	O
prévue	O
à	O
Derry	O
d'	O
une	O
féministe	O
en	O
faveur	O
de	O
l'	O
avortement	O
et	O
du	O
libre	O
choix	O
des	O
femmes	O
à	O
décider	O
soulève	O
les	O
passions	O
et	O
les	PERSON
haines	PERSON
de	PERSON
la	PERSON
ville	PERSON
,	O
dérapant	O
parfois	O
en	O
violences	O
.	O
Cependant	O
,	O
il	PERSON
y	O
a	O
des	O
différences	O
notables	O
entre	O
le	O
rôle	O
qu'	O
a	O
à	O
jouer	O
Patrick	O
Danville	LOCATION
tel	O
qu'	O
il	LOCATION
est	LOCATION
décrit	O
dans	O
Insomnie	O
et	O
celui	O
qu'	O
il	O
va	O
jouer	O
réellement	O
dans	O
la	O
Tour	O
sombre	O
.	O
