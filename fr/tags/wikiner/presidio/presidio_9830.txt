En	O
Italie	O
,	O
les	O
compositeurs	O
de	O
cantates	O
les	O
plus	O
fameux	O
sont	O
Claudio	PERSON
Monteverdi	PERSON
,	O
Giacomo	PERSON
Carissimi	PERSON
,	O
Alessandro	PERSON
Scarlatti	PERSON
et	O
Antonio	PERSON
Vivaldi	PERSON
.	O
En	O
Angleterre	O
et	O
en	O
Allemagne	O
,	O
il	PERSON
faut	PERSON
citer	PERSON
Georg	PERSON
Friedrich	PERSON
Haendel	PERSON
,	O
Georg	PERSON
Philipp	PERSON
Telemann	PERSON
,	O
Dietrich	PERSON
Buxtehude	PERSON
,	O
Jean-Sébastien	PERSON
Bach	PERSON
,	O
Johann	PERSON
Pachelbel	PERSON
.	O
Les	PERSON
maîtres	PERSON
principaux	PERSON
en	O
sont	PERSON
Jean-Baptiste	PERSON
Morin	PERSON
(	O
le	O
créateur	O
)	O
,	O
Nicolas	PERSON
Bernier	PERSON
,	O
Michel	PERSON
Pignolet	PERSON
de	PERSON
Montéclair	PERSON
et	O
surtout	O
Louis-Nicolas	O
Clérambault	PERSON
.	O
Plus	O
tard	O
,	O
des	O
compositeurs	O
romantiques	LOCATION
comme	O
Robert	PERSON
Schumann	PERSON
ou	O
Felix	PERSON
Mendelssohn-Bartholdy	PERSON
la	O
remettront	O
à	O
l'	O
honneur	O
.	O
