Le	O
golfe	O
du	O
Mexique	O
est	O
un	O
golfe	O
de	O
l'	O
océan	O
Atlantique	O
,	O
située	O
au	O
sud-est	O
de	O
l'	O
Amérique	O
du	O
Nord	O
.	O
Le	O
golfe	O
du	O
Mexique	O
aurait	O
été	O
le	O
lieu	O
d'	O
impact	O
d'	O
un	O
énorme	O
astéroïde	O
,	O
le	PERSON
cratère	PERSON
de	PERSON
Chicxulub	PERSON
,	O
qui	O
aurait	O
mis	O
fin	O
au	O
règne	O
des	O
dinosaures	O
.	O
Les	PERSON
principaux	PERSON
pays	O
qui	PERSON
bordent	PERSON
le	O
golfe	O
du	O
Mexique	O
sont	O
:	O
Le	O
golfe	O
du	O
Mexique	O
communique	O
au	LOCATION
sud-est	LOCATION
avec	LOCATION
la	LOCATION
mer	LOCATION
des	LOCATION
Caraïbes	LOCATION
par	LOCATION
le	O
canal	O
du	O
Yucatán	LOCATION
et	O
à	O
l'	O
est	O
avec	O
l'	O
océan	O
Atlantique	PERSON
par	PERSON
le	O
détroit	O
de	O
Floride	O
.	O
C'	O
est	O
une	O
zone	O
écologiquement	O
très	O
riche	O
,	O
et	O
une	O
zone	O
de	O
pêche	O
importante	O
pour	O
les	O
États-Unis	O
et	O
Cuba	O
.	O
La	O
plate-forme	O
du	O
Yucatan	O
était	O
émergée	O
jusqu'	PERSON
à	O
la	O
moitié	O
du	O
crétacé	O
.	O
Colomb	PERSON
navigua	PERSON
plutôt	O
dans	O
la	O
région	O
des	O
Caraïbes	O
,	O
autour	O
de	O
Cuba	LOCATION
et	O
Hispaniola	O
.	O
La	O
première	O
exploration	O
européenne	O
du	O
golfe	O
est	O
réalisée	O
par	O
Amerigo	PERSON
Vespucci	PERSON
,	O
en	O
1497	O
.	O
En	O
1506	O
,	O
Hernán	PERSON
Cortés	PERSON
prit	O
part	O
à	O
la	O
conquête	O
d'	O
Hispaniola	O
et	O
de	O
Cuba	O
,	O
ce	LOCATION
qui	LOCATION
lui	LOCATION
rapporta	LOCATION
une	LOCATION
grande	LOCATION
propriété	O
ainsi	O
que	O
des	O
esclaves	O
(	O
amérindiens	O
)	O
en	O
récompense	O
.	O
En	O
1510	O
,	O
il	PERSON
accompagna	PERSON
Diego	PERSON
Velázquez	PERSON
de	PERSON
Cuéllar	PERSON
dans	O
son	O
expédition	PERSON
pour	O
la	O
conquête	O
de	O
Cuba	O
.	O
En	O
1517	O
,	O
Francisco	PERSON
Hernández	PERSON
de	PERSON
Córdoba	PERSON
découvrit	O
la	PERSON
péninsule	PERSON
du	O
Yucatán	LOCATION
.	O
Tout	O
ceci	O
motiva	O
le	O
lancement	O
de	O
deux	O
expéditions	O
supplémentaires	O
,	O
en	O
1518	O
sous	O
le	O
commandement	O
de	PERSON
Juan	PERSON
de	PERSON
Grijalva	PERSON
et	O
en	O
1519	O
sous	O
celui	O
d'	O
Hernán	PERSON
Cortés	PERSON
.	O
Cette	PERSON
dernière	PERSON
conduisit	PERSON
à	PERSON
l'	O
exploration	O
espagnole	O
,	O
à	O
l'	O
invasion	O
militaire	O
et	O
finalement	O
à	O
l'	O
installation	O
et	O
la	O
colonisation	PERSON
connue	PERSON
aujourd'hui	PERSON
sous	PERSON
le	PERSON
nom	PERSON
de	PERSON
Conquête	PERSON
du	PERSON
Mexique	PERSON
.	O
Hernández	PERSON
ne	PERSON
vécut	PERSON
pas	O
assez	O
longtemps	PERSON
pour	O
voir	O
la	O
poursuite	O
de	O
son	O
travail	O
.	O
En	O
conséquence	O
,	O
l'	O
expédition	O
de	O
Tristán	O
de	O
Luna	O
y	O
Arellano	O
fut	O
envoyée	O
dans	O
la	PERSON
baie	PERSON
de	PERSON
Pensacola	PERSON
,	O
le	LOCATION
15	O
août	O
1559	O
.	O
Le	O
11	O
décembre	O
1526	O
,	O
Charles	PERSON
Quint	PERSON
accorde	O
à	O
Pánfilo	O
de	O
Narváez	O
le	O
droit	O
de	O
revendiquer	O
les	O
côtes	O
du	O
golfe	O
,	O
suite	O
à	O
l	O
'	O
expédition	O
de	O
Narváez	O
.	O
Le	O
contrat	O
lui	O
laisse	O
un	O
an	O
pour	O
rassembler	O
une	O
armée	O
,	O
quitter	O
l'	O
Espagne	O
,	O
fonder	O
au	O
moins	O
deux	O
villes	LOCATION
pouvant	O
accueillir	O
chacune	O
une	O
centaine	O
de	O
personnes	O
et	O
construire	O
deux	O
forteresses	O
de	O
plus	O
le	O
long	O
des	O
côtes	O
.	O
La	O
flotte	O
d'	O
Iberville	LOCATION
appareilla	O
de	LOCATION
Brest	LOCATION
le	O
24	O
octobre	O
1698	O
.	O
Au	PERSON
sud-est	PERSON
se	O
retrouve	O
l'	O
île	O
de	O
Cuba	O
.	O
Les	O
bords	O
du	O
plateau	O
continental	O
du	O
Yucatán	O
et	O
de	O
Floride	O
baignent	O
dans	O
des	O
eaux	O
plus	O
fraîches	O
et	O
plus	O
riches	O
en	O
nutriments	O
grâce	O
au	O
phénomène	O
de	O
remontée	O
d'	O
eau	O
.	O
On	O
trouve	O
également	O
un	O
vortex	O
cyclonique	O
dans	O
la	O
baie	O
de	O
Campeche	O
.	O
De	PERSON
nombreux	PERSON
fleuves	PERSON
s'	PERSON
y	O
jettent	O
,	O
et	O
plus	O
particulièrement	O
le	O
fleuve	O
Mississippi	O
dans	O
le	O
nord	O
du	O
golfe	O
,	O
ainsi	O
que	O
le	PERSON
río	PERSON
Grijalva	PERSON
et	O
le	O
río	O
Usumacinta	O
dans	O
le	O
sud	O
.	O
Le	O
golfe	O
du	O
Mexique	O
est	O
un	O
excellent	O
exemple	O
de	O
marge	O
passive	O
.	O
L'	O
eau	PERSON
chaude	PERSON
du	PERSON
golfe	PERSON
peut	PERSON
alimenter	O
de	O
puissants	O
ouragans	O
prenant	O
naissance	O
dans	O
l'	O
Atlantique	O
et	O
causant	O
de	O
lourdes	O
pertes	O
humaines	O
et	O
matérielles	O
,	O
comme	O
ce	LOCATION
fut	LOCATION
le	LOCATION
cas	LOCATION
avec	LOCATION
l'	O
ouragan	O
Katrina	O
en	O
2005	O
.	O
Dans	O
l'	O
océan	O
Atlantique	O
,	O
un	PERSON
ouragan	PERSON
a	O
tendance	O
à	O
faire	LOCATION
remonter	O
à	O
la	O
surface	O
l'	O
eau	O
froide	O
des	O
profondeurs	O
,	O
ce	LOCATION
qui	LOCATION
diminue	LOCATION
la	LOCATION
probabilité	O
qu'	O
un	O
autre	O
ouragan	O
suive	O
dans	O
son	O
sillage	O
(	O
car	O
une	O
eau	PERSON
chaude	PERSON
est	PERSON
une	PERSON
condition	O
préalable	O
à	O
la	O
formation	O
d'	O
un	PERSON
ouragan	PERSON
)	O
.	O
Celui-ci	PERSON
n'	PERSON
a	O
causé	PERSON
aucun	PERSON
dégât	PERSON
,	O
mais	PERSON
il	PERSON
a	O
été	O
ressenti	O
dans	O
tout	O
le	O
sud	O
des	O
États-Unis	O
.	O
La	O
circulation	O
dans	O
le	O
Golfe	O
du	O
Mexique	O
et	O
les	O
Caraïbes	O
est	O
similaire	O
à	O
un	O
demi-	O
cul-de-sac	O
.	O
Le	O
plateau	O
est	O
surtout	O
exploité	O
pour	O
le	O
pétrole	O
grâce	O
à	O
des	O
plates-formes	O
de	O
forage	O
en	O
mer	O
,	O
dont	O
la	O
plupart	O
sont	O
situées	O
à	O
l'	O
ouest	O
du	O
golfe	O
et	O
dans	O
la	O
baie	O
de	O
Campeche	O
.	O
