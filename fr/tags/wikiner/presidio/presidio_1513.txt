Le	O
Japon	O
est	O
le	O
4	O
e	O
pays	O
pour	O
ses	O
exportations	O
et	O
le	O
5	O
e	O
pour	O
ses	O
importations	O
.	O
Le	O
taux	O
d'	O
emploi	O
(	O
69.3	O
%	O
en	O
2005	O
,	O
69.5	O
%	O
en	O
février	O
2010	O
)	O
,	O
est	O
l'	O
un	O
des	O
plus	O
forts	O
de	O
l'	O
OCDE	O
(	O
le	O
12	O
e	O
)	O
.	O
Ils	O
dirigeaient	O
un	O
royaume	O
quasi-indépendant	O
qui	O
tirait	O
sa	O
richesse	O
du	PERSON
minage	PERSON
de	PERSON
l'	PERSON
or	O
,	O
du	O
commerce	O
de	PERSON
chevaux	PERSON
et	PERSON
de	PERSON
leur	PERSON
situation	O
d'	O
intermédiaires	PERSON
dans	O
le	O
commerce	O
de	O
produits	O
de	O
luxe	O
importés	O
d'	O
Asie	O
continentale	O
et	O
des	O
états	O
des	O
natifs	PERSON
Emishi	PERSON
--	PERSON
Aïnous	PERSON
.	O
Ils	O
sont	O
néanmoins	O
vaincus	O
et	O
la	O
région	O
conquise	O
en	O
1189	O
par	O
le	O
shogun	O
Minamoto	LOCATION
no	O
Yoritomo	O
,	O
fondateur	O
du	O
premier	O
bakufu	O
(	O
ou	O
gouvernement	O
militaire	O
des	O
shoguns	O
)	O
,	O
le	O
"	O
Shogunat	O
de	O
Kamakura	O
"	O
,	O
faisant	O
entrer	O
le	O
Japon	O
dans	O
son	O
"	O
ère	O
féodale	O
"	O
.	O
Le	O
mouvement	O
embrase	O
les	O
provinces	O
voisines	O
à	O
partir	O
de	O
Kanazawa	O
.	O
Au	O
pied	O
des	O
châteaux	O
seigneuriaux	O
se	O
constituent	O
des	O
capitales	O
de	O
provinces	O
(	O
城下町	O
,	O
jōkamachi	PERSON
?	O
)	O
,	O
comme	O
Odawara	LOCATION
,	O
Sunpu	O
,	O
Yamaguchi	LOCATION
,	O
Kanazawa	LOCATION
,	O
etc	O
.	O
Il	PERSON
faut	PERSON
également	PERSON
noter	O
le	O
développement	O
de	O
villes	O
marchandes	O
,	O
qui	O
profitent	O
de	PERSON
circonstances	PERSON
économiques	O
favorables	O
au	O
commerce	O
international	O
,	O
comme	O
Hakata	LOCATION
à	LOCATION
Kyūshū	LOCATION
,	O
Hyōgo	LOCATION
et	O
Sakai	O
(	O
堺	O
,	O
Sakai	O
?	O
)	O
,	O
dans	O
la	O
banlieue	O
d'	O
Ōsaka	LOCATION
.	O
Le	O
grand	O
maître	O
de	O
thé	O
Sen	O
no	O
Rikyū	O
est	O
un	O
riche	O
marchand	O
de	O
Sakai	O
.	O
Peu	PERSON
après	PERSON
ces	PERSON
premiers	O
contacts	O
,	O
les	O
navires	O
portugais	O
commencent	O
à	O
arriver	O
au	LOCATION
Japon	LOCATION
.	O
À	O
cette	O
époque	O
,	O
il	O
existe	O
déjà	O
depuis	O
environ	O
1515	O
des	O
échanges	O
commerciaux	O
entre	O
le	LOCATION
Portugal	LOCATION
et	O
Goa	O
,	O
consistant	O
en	O
3	O
ou	O
4	O
caraques	PERSON
quittant	PERSON
Lisbonne	PERSON
avec	PERSON
de	PERSON
l'	PERSON
argent	O
pour	O
acheter	O
du	O
coton	O
et	O
des	O
épices	O
en	O
Inde	O
;	O
l'	O
une	O
d'	O
entre	O
elles	O
se	O
rendant	O
jusqu'	PERSON
en	O
Chine	LOCATION
pour	O
acheter	O
de	O
la	O
soie	O
,	O
là	O
encore	O
en	O
échange	O
d'	O
argent	PERSON
portugais	PERSON
.	O
Leur	O
pilote	O
est	O
William	PERSON
Adams	PERSON
,	O
le	O
premier	O
Anglais	O
à	O
atteindre	O
le	O
Japon	O
.	O
En	PERSON
théorie	PERSON
,	O
les	O
seules	O
influences	O
étrangères	O
permises	O
étaient	O
celles	O
des	O
Hollandais	O
(	O
par	O
l'	O
intermédiaire	O
de	O
la	O
Compagnie	O
néerlandaise	O
des	O
Indes	O
orientales	O
)	O
sur	O
l'	O
île	O
artificielle	O
de	O
Dejima	O
dans	O
la	O
baie	O
de	O
Nagasaki	O
,	O
mais	LOCATION
le	LOCATION
commerce	O
avec	O
la	O
Chine	O
était	O
aussi	PERSON
géré	PERSON
à	O
Nagasaki	LOCATION
.	O
Le	O
riz	O
perçu	O
est	O
ensuite	O
vendu	O
à	O
Edo	O
aux	O
maisons	O
spécialisées	O
dans	O
le	O
commerce	O
de	O
cette	O
denrée	O
(	O
les	O
fudasashi	O
)	O
.	O
Le	O
Japon	O
se	O
dresse	O
au	O
bout	O
de	O
quelques	O
décennies	O
,	O
avec	O
une	O
croissance	O
supérieure	O
à	O
celle	O
de	O
l'	O
Allemagne	O
,	O
dans	LOCATION
le	LOCATION
rang	O
des	O
grandes	O
puissances	O
industrielles	O
.	O
Matsushita	O
,	O
quoique	O
n'	O
étant	O
pas	O
un	PERSON
conglomérat	PERSON
,	O
fut	O
identifiée	O
puis	O
finalement	O
épargnée	O
par	O
une	O
mobilisation	O
syndicale	O
.	O
Des	LOCATION
années	LOCATION
1950	LOCATION
à	O
la	O
fin	O
des	O
années	O
1980	O
,	O
le	O
Japon	O
connaît	O
des	O
taux	O
de	O
croissance	O
spectaculaires	O
et	O
une	O
reconstruction	O
industrielle	O
rapide	O
.	O
Dès	O
1968	O
,	O
le	O
Japon	O
est	O
la	O
2	O
e	O
économie	O
mondiale	O
,	O
juste	PERSON
derrière	PERSON
les	PERSON
États-Unis	PERSON
.	O
Or	O
,	O
la	O
poursuite	O
du	O
processus	O
de	O
développement	O
dans	O
ces	O
pays	O
émergents	O
fait	O
que	O
tous	O
commencent	O
à	O
rattraper	LOCATION
le	O
niveau	O
de	O
technicité	O
du	O
Japon	O
,	O
entraînant	O
un	O
changement	O
de	O
modèle	O
économique	O
,	O
dit	O
"	O
des	PERSON
sauts	PERSON
de	PERSON
grenouilles	PERSON
"	O
,	O
avec	O
une	O
division	O
internationale	O
du	O
travail	O
plus	O
horizontale	O
et	O
une	O
interdépendance	O
économique	O
accrue	O
entre	O
les	O
pays	O
asiatiques	O
(	O
Japon	O
et	O
NPIA	O
exportant	O
des	O
biens	O
d'	O
équipement	O
et	O
des	O
produits	O
intermédiaires	O
,	O
la	O
Chine	LOCATION
et	O
l'	O
ASEAN	O
des	O
produits	O
finis	O
)	O
.	O
La	O
Banque	O
du	O
Japon	O
pour	O
sa	O
part	O
a	O
maintenu	O
une	O
politique	O
monétaire	PERSON
inflationniste	PERSON
avec	PERSON
de	PERSON
faibles	PERSON
taux	PERSON
d'	PERSON
intérêts	O
,	O
d'	O
ailleurs	O
maintenus	O
entre	O
1999	O
et	O
2005	O
à	O
un	PERSON
niveau	PERSON
virtuellement	PERSON
nul	PERSON
,	O
cela	PERSON
afin	PERSON
de	PERSON
favoriser	O
les	O
exportations	O
.	O
Ensuite	O
les	O
entreprises	O
,	O
avec	O
un	O
système	O
dualiste	O
:	O
d'	O
un	PERSON
côté	PERSON
les	O
firmes	O
multinationales	O
ou	O
keiretsu	O
comme	O
Toyota	O
par	O
exemple	O
,	O
organisées	O
autour	O
de	O
sociétés	O
de	O
commerce	O
,	O
les	O
sōgō	LOCATION
shōsha	LOCATION
,	O
chargées	O
de	O
récolter	O
et	O
de	O
repérer	O
les	O
meilleurs	O
marchés	O
;	O
d'	O
un	O
autre	O
côté	O
,	O
un	O
réseau	O
dense	O
de	O
petites	O
et	O
moyennes	O
entreprises	O
,	O
qui	O
servent	O
de	O
sous-traitant	O
et	O
d'	O
amortisseurs	O
aux	O
premières	O
:	O
en	O
cas	O
de	O
crise	O
,	O
c'	O
est	O
dans	O
ces	O
entreprises	O
que	O
les	O
travailleurs	O
sont	O
majoritairement	O
licenciés	O
.	O
Cela	PERSON
aboutit	O
à	O
un	O
faible	O
nombre	O
de	O
mouvements	O
sociaux	O
(	O
il	O
n'	O
y	O
a	O
presque	O
jamais	O
eu	O
des	O
grèves	O
dans	O
les	O
entreprises	O
et	O
lorsqu'	O
elles	O
eurent	O
lieu	O
,	O
les	O
gens	O
venaient	O
travailler	O
mais	O
avec	O
un	PERSON
brassard	PERSON
pour	O
exprimer	O
leur	O
mécontentement	O
)	O
,	O
tandis	O
que	O
de	O
puissants	O
syndicats	O
d'	O
entreprise	O
(	O
dont	O
le	O
principal	O
reste	O
le	LOCATION
Rengō	LOCATION
)	O
qui	O
négocient	O
avec	O
le	O
patronat	O
lors	O
d'	O
un	O
processus	O
de	O
revendications	O
salariales	O
annuel	LOCATION
,	O
le	PERSON
shuntō	PERSON
,	O
au	O
printemps	O
.	O
Une	O
génération	O
d'	O
hommes	O
politiques	O
apparue	O
au	O
début	O
des	O
années	O
1990	O
et	O
fortement	O
inspirée	PERSON
des	PERSON
politiques	PERSON
menées	PERSON
par	O
Margaret	PERSON
Thatcher	PERSON
au	O
Royaume-Uni	O
et	O
Ronald	PERSON
Reagan	PERSON
,	O
défend	O
la	O
réduction	O
du	O
poids	O
de	O
la	O
bureaucratie	O
,	O
la	O
baisse	O
du	O
poids	O
budgétaire	O
des	O
grands	O
travaux	PERSON
(	O
facteurs	O
selon	PERSON
eux	PERSON
de	PERSON
corruption	PERSON
et	PERSON
de	PERSON
clientélisme	PERSON
)	O
,	O
une	O
forme	O
de	PERSON
déréglementation	PERSON
et	O
de	O
décentralisation	O
ainsi	O
que	O
la	O
mise	O
en	O
place	O
d'	O
une	O
économie	O
moins	PERSON
dépendante	PERSON
des	PERSON
exportations	PERSON
et	O
plus	O
du	O
marché	O
intérieur	O
et	O
donc	O
de	O
la	O
consommation	O
des	O
ménages	O
.	O
Les	O
adversaires	O
de	PERSON
Koizumi	PERSON
(	O
notamment	O
Gavan	O
McCormack	PERSON
qui	PERSON
parle	PERSON
du	PERSON
"	PERSON
Royaume	PERSON
de	PERSON
l'	PERSON
illusion	O
de	O
Koizumi	O
"	O
)	O
accusent	O
ses	O
réformes	O
d'	O
avoir	O
creusé	O
le	O
fossé	O
social	O
,	O
favorisé	O
les	O
personnes	O
les	O
plus	O
fortunées	O
(	O
taux	O
d'	O
imposition	O
sur	O
le	O
revenu	O
de	O
la	O
tranche	O
supérieure	O
a	O
notamment	O
été	O
ramené	O
,	O
entre	O
2001	O
et	O
2006	O
,	O
de	O
70	O
à	O
37	O
%	O
)	O
et	O
inégalement	O
réparti	O
les	O
fruits	O
du	O
retour	O
à	O
la	O
croissance	O
à	O
partir	O
de	O
2003	O
,	O
mais	LOCATION
le	LOCATION
processus	O
est	O
plus	O
ancien	O
.	O
Le	O
Parti	O
démocrate	O
du	O
Japon	O
,	O
au	O
pouvoir	O
depuis	O
2009	O
,	O
partage	O
l'	O
opposition	O
à	O
la	O
bureaucratie	O
et	O
défend	O
pratiquement	O
les	O
mêmes	O
positions	O
économiques	O
et	O
budgétaires	O
(	O
dans	LOCATION
le	LOCATION
premier	O
budget	O
qu'	O
il	PERSON
présente	PERSON
,	O
pour	O
l'	O
année	O
fiscale	O
2010	O
,	O
les	O
sommes	O
prévues	O
pour	O
les	O
travaux	O
publics	O
baissent	O
de	O
18,3	O
%	O
)	O
tout	O
en	O
défendant	O
une	O
hausse	O
des	O
prestations	O
sociales	O
afin	O
d'	O
augmenter	O
le	O
pouvoir	O
d'	O
achat	O
et	O
donc	O
la	O
consommation	O
(	O
dans	O
le	O
même	O
budget	O
,	O
le	LOCATION
total	LOCATION
des	O
aides	O
augmentent	PERSON
de	PERSON
10	PERSON
%	PERSON
,	O
avec	PERSON
surtout	PERSON
la	PERSON
mise	O
en	O
place	O
d'	O
une	O
allocation	O
versée	O
aux	O
familles	O
de	O
13	O
000	O
yens	LOCATION
par	O
mois	O
et	O
par	O
enfants	O
jusqu'	PERSON
à	O
la	O
fin	O
du	O
collège	O
et	O
l'	O
objectif	O
de	O
doubler	O
ce	O
montant	O
dès	O
2011	O
,	O
ou	O
encore	O
la	O
gratuité	O
des	O
inscriptions	O
au	PERSON
lycée	PERSON
,	O
tandis	O
que	O
son	O
programme	O
électoral	O
prévoyait	PERSON
également	PERSON
de	PERSON
rendre	PERSON
les	O
autoroutes	O
gratuits	O
en	O
zone	O
urbaine	O
)	O
,	O
.	O
Depuis	O
1968	O
,	O
le	O
Japon	O
est	O
la	O
deuxième	O
économie	O
mondiale	O
derrière	O
celle	O
des	O
États-Unis	O
.	O
En	O
2006	O
,	O
le	O
PIB	O
japonais	O
s'	O
élève	O
à	O
4	O
340	O
milliards	O
de	O
dollars	O
(	O
Total	O
GDP	O
2006	O
,	O
World	O
Bank	O
)	O
.	O
Début	O
2006	O
,	O
le	O
Japon	O
semble	O
renouer	O
avec	O
une	O
inflation	O
modérée	O
.	O
