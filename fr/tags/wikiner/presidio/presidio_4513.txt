Le	PERSON
nom	PERSON
Jupiter	PERSON
vient	PERSON
de	PERSON
l'	PERSON
évolution	PERSON
d'	O
un	O
nom	O
composé	O
.	O
Il	O
était	O
le	O
fils	O
de	O
Rhéa	O
et	O
de	O
Saturne	O
qui	O
dévorait	O
ses	O
enfants	O
à	O
mesure	O
qu'	O
ils	O
venaient	O
au	O
monde	O
.	O
Celle-ci	O
fut	O
dévorée	O
par	O
Saturne	O
.	O
Cependant	O
,	O
pour	O
tromper	LOCATION
son	O
mari	PERSON
,	O
Rhéa	PERSON
lui	PERSON
fit	O
avaler	O
une	O
pierre	PERSON
emmaillotée	PERSON
.	O
Ce	O
fut	O
par	O
le	O
conseil	O
de	O
Métis	O
qu'	O
il	O
fit	O
prendre	O
à	O
Saturne	O
un	O
breuvage	O
dont	O
l'	O
effet	O
fut	O
de	O
lui	O
faire	O
vomir	O
premièrement	O
la	O
pierre	O
qu'	O
il	O
avait	O
avalée	O
,	O
et	O
ensuite	O
tous	O
les	O
enfants	O
engloutis	O
dans	O
son	O
sein	PERSON
.	O
Avec	O
l'	O
aide	O
de	PERSON
ses	PERSON
frères	PERSON
,	O
Neptune	O
et	O
Pluton	O
,	O
il	PERSON
se	PERSON
proposa	LOCATION
d'	O
abord	O
de	PERSON
détrôner	PERSON
son	O
père	PERSON
et	PERSON
de	PERSON
bannir	PERSON
les	O
Titans	O
,	O
cette	O
branche	O
rivale	O
qui	O
faisait	O
obstacle	O
à	O
sa	O
royauté	O
.	O
Il	O
leur	O
déclara	PERSON
donc	PERSON
la	PERSON
guerre	PERSON
ainsi	PERSON
qu'	PERSON
à	O
Saturne	O
.	O
Avec	O
ces	O
armes	O
,	O
les	O
trois	O
frères	O
vainquirent	O
Saturne	O
,	O
le	LOCATION
chassèrent	LOCATION
du	LOCATION
trône	O
et	O
de	O
la	O
société	O
des	O
dieux	O
,	O
après	PERSON
lui	PERSON
avoir	PERSON
fait	PERSON
subir	PERSON
de	PERSON
cruelles	PERSON
tortures	O
.	O
Les	O
Titans	O
qui	O
avaient	O
aidé	PERSON
Saturne	PERSON
à	PERSON
combattre	O
furent	O
précipités	O
dans	O
les	O
profondeurs	O
du	O
Tartare	O
sous	O
la	O
garde	O
des	O
Hécatonchires	O
.	O
Jupiter	O
était	O
dans	O
une	O
grande	O
inquiétude	O
,	O
parce	O
qu'	O
un	O
ancien	O
oracle	O
annonçait	O
que	O
les	O
Géants	O
seraient	O
invincibles	O
,	O
à	O
moins	O
que	O
les	O
dieux	O
n'	O
appelassent	O
un	PERSON
mortel	PERSON
à	PERSON
leur	PERSON
secours	PERSON
.	O
Après	PERSON
les	PERSON
avoir	PERSON
défaits	PERSON
,	O
Jupiter	LOCATION
les	O
précipita	O
jusqu'	O
au	O
fond	O
du	O
Tartare	O
,	O
ou	O
bien	O
,	O
suivant	O
d'	O
autres	PERSON
poètes	PERSON
,	O
il	PERSON
les	PERSON
enterra	PERSON
vivants	PERSON
,	O
les	O
uns	O
dans	O
un	O
pays	O
,	O
les	O
autres	O
dans	O
un	O
autre	O
.	O
Il	PERSON
existe	PERSON
de	PERSON
nombreux	PERSON
épithètes	PERSON
de	PERSON
Jupiter	PERSON
,	O
ce	O
sont	O
des	O
noms	O
complémentaires	O
qui	O
correspondent	O
à	O
ses	O
pouvoirs	O
,	O
actions	O
...	O
Quand	PERSON
il	PERSON
épousa	PERSON
Junon	PERSON
,	O
Jupiter	LOCATION
invita	O
à	O
ses	O
noces	O
tous	O
les	O
dieux	O
,	O
tous	O
les	O
hommes	O
et	O
tous	O
les	O
animaux	O
.	O
Elle	PERSON
en	PERSON
fut	PERSON
bien	PERSON
punie	PERSON
:	O
Jupiter	LOCATION
ordonna	O
à	O
Mercure	O
de	O
la	O
changer	O
en	O
tortue	O
.	O
Parmi	O
les	O
divinités	O
,	O
Jupiter	LOCATION
tenait	O
toujours	O
le	O
premier	O
rang	O
;	O
et	O
son	O
culte	O
était	LOCATION
le	LOCATION
plus	O
solennel	O
et	O
le	O
plus	O
universellement	PERSON
répandu	PERSON
.	O
L'	O
imagination	O
des	O
artistes	O
modifiait	PERSON
son	O
image	O
ou	O
sa	O
statue	O
,	O
suivant	O
les	O
circonstances	O
et	O
le	O
lieu	O
même	O
où	O
Jupiter	O
était	O
honoré	O
.	O
