Saturne	PERSON
est	PERSON
la	PERSON
sixième	O
planète	O
du	O
système	O
solaire	O
par	O
ordre	O
de	O
distance	O
au	O
Soleil	O
.	O
C'	O
est	O
une	O
géante	PERSON
gazeuse	PERSON
,	O
la	O
seconde	O
en	O
masse	O
et	O
en	O
volume	O
après	O
Jupiter	O
dans	O
le	O
système	O
solaire	O
.	O
D'	O
un	PERSON
diamètre	PERSON
d'	O
environ	O
neuf	O
fois	O
et	O
demi	O
celui	O
de	O
la	O
Terre	O
,	O
elle	LOCATION
est	O
majoritairement	O
composée	O
d'	O
hydrogène	O
.	O
Saturne	O
a	O
la	O
forme	O
d'	O
un	LOCATION
sphéroïde	LOCATION
oblat	LOCATION
:	O
la	O
planète	O
est	O
aplatie	LOCATION
aux	LOCATION
pôles	LOCATION
et	O
renflée	O
à	O
l'	O
équateur	O
.	O
Les	PERSON
autres	PERSON
géantes	PERSON
gazeuses	PERSON
du	PERSON
système	PERSON
solaire	PERSON
(	O
Jupiter	LOCATION
,	O
Uranus	O
et	O
Neptune	O
)	O
sont	O
également	O
aplaties	O
,	O
mais	PERSON
de	PERSON
façon	PERSON
moins	PERSON
marquée	PERSON
.	O
Saturne	PERSON
est	PERSON
la	PERSON
deuxième	PERSON
planète	O
la	O
plus	O
massive	O
du	O
système	O
solaire	O
,	O
3,3	O
fois	O
moins	O
que	O
Jupiter	O
,	O
mais	O
5,5	O
fois	O
plus	O
que	O
Neptune	O
et	O
6,5	O
fois	O
plus	O
qu'	O
Uranus	LOCATION
.	O
Saturne	PERSON
est	PERSON
la	PERSON
seule	PERSON
planète	PERSON
du	PERSON
système	PERSON
solaire	PERSON
dont	O
la	O
masse	O
volumique	O
moyenne	O
est	O
inférieure	O
à	O
celle	O
de	O
l'	O
eau	O
:	O
0,69	O
g/cm³	O
.	O
La	O
haute	O
atmosphère	O
de	O
Saturne	O
est	O
constituée	PERSON
à	O
93,2	O
%	O
d'	O
hydrogène	O
et	O
à	O
6,7	O
%	O
d'	O
hélium	PERSON
en	O
termes	O
de	O
molécules	O
de	O
gaz	O
(	O
96,5	O
%	O
d'	O
hydrogène	O
et	O
3,5	O
%	O
d'	O
hélium	O
en	O
termes	O
d'	O
atomes	O
)	O
.	O
La	O
structure	O
interne	O
de	PERSON
Saturne	PERSON
serait	PERSON
similaire	PERSON
à	O
celle	O
de	O
Jupiter	O
,	O
avec	O
un	PERSON
noyau	PERSON
rocheux	PERSON
de	PERSON
silicates	PERSON
et	PERSON
de	PERSON
fer	PERSON
,	O
entouré	O
d'	O
une	O
couche	O
d'	O
hydrogène	O
métallique	O
,	O
puis	O
d'	O
hydrogène	O
liquide	O
,	O
puis	O
enfin	O
d'	O
hydrogène	PERSON
gazeux	PERSON
.	O
Une	O
explication	O
proposée	O
serait	O
une	O
"	O
pluie	O
"	O
de	O
gouttelettes	O
d'	O
hélium	PERSON
dans	O
les	O
profondeurs	O
de	O
Saturne	O
,	O
dégageant	O
de	O
la	O
chaleur	O
par	O
friction	O
en	O
tombant	O
dans	O
une	O
mer	O
d'	O
hydrogène	O
plus	O
léger	O
[	O
réf.	O
nécessaire	O
]	O
.	O
De	O
manière	O
similaire	O
à	O
Jupiter	O
,	O
l'	O
atmosphère	O
de	PERSON
Saturne	PERSON
est	PERSON
organisée	O
en	O
bandes	O
parallèles	O
,	O
même	LOCATION
si	LOCATION
ces	LOCATION
bandes	O
sont	O
moins	O
visibles	O
et	O
plus	O
larges	O
près	O
de	O
l'	O
équateur	O
.	O
Depuis	O
,	O
les	O
télescopes	O
terrestres	O
ont	O
fait	O
suffisamment	O
de	O
progrès	O
pour	O
pouvoir	O
suivre	O
l'	O
atmosphère	O
saturnienne	O
et	O
les	O
caractéristiques	O
courantes	O
chez	PERSON
Jupiter	PERSON
(	O
comme	LOCATION
les	LOCATION
orages	LOCATION
ovales	LOCATION
à	LOCATION
longue	O
durée	O
de	O
vie	O
)	O
ont	O
été	O
retrouvées	O
chez	O
Saturne	O
.	O
Dans	O
les	O
images	O
transmises	O
par	O
la	O
sonde	O
Cassini	O
,	O
l'	O
atmosphère	O
de	O
l'	O
hémisphère	O
nord	O
apparaît	O
bleue	O
,	O
de	O
façon	O
similaire	O
à	O
celle	O
d'	O
Uranus	LOCATION
.	O
L'	O
imagerie	O
infrarouge	O
a	O
montré	O
que	O
Saturne	O
possède	O
un	O
vortex	O
polaire	PERSON
chaud	PERSON
,	O
le	PERSON
seul	PERSON
phénomène	PERSON
de	PERSON
ce	PERSON
type	O
connu	O
dans	O
le	O
système	O
solaire	O
.	O
Les	O
images	O
prises	O
par	O
le	O
télescope	O
spatial	O
Hubble	O
indiquent	O
la	O
présence	O
au	O
pôle	O
sud	O
d'	O
un	O
courant-jet	O
,	O
mais	O
pas	O
d'	O
un	O
vortex	O
polaire	O
ou	O
d'	O
un	O
système	O
hexagonal	O
analogue	O
.	O
Les	PERSON
orages	PERSON
de	PERSON
Saturne	PERSON
sont	O
particulièrement	O
longs	O
.	O
Les	O
décharges	O
électriques	O
provoquées	PERSON
par	PERSON
les	PERSON
orages	PERSON
de	PERSON
Saturne	PERSON
émettent	PERSON
des	PERSON
ondes	PERSON
radio	PERSON
dix	PERSON
mille	PERSON
fois	PERSON
plus	O
fortes	O
que	O
celles	O
des	O
orages	O
terrestres	O
.	O
Le	O
champ	O
magnétique	O
de	O
Saturne	O
est	O
plus	O
faible	O
que	O
celui	O
de	O
Jupiter	O
et	O
sa	O
magnétosphère	O
est	O
plus	O
petite	O
.	O
L'	O
atmosphère	O
de	O
Saturne	O
subissant	O
une	O
rotation	PERSON
différentielle	PERSON
,	O
plusieurs	O
systèmes	O
ont	O
été	O
définis	O
,	O
avec	O
des	O
périodes	O
de	O
rotation	O
propres	O
(	O
un	O
cas	O
similaire	O
à	O
celui	O
de	O
Jupiter	O
)	O
:	O
Cependant	O
,	O
lors	O
de	O
son	O
approche	O
de	O
Saturne	O
en	O
2004	O
,	O
la	O
sonde	O
Cassini	O
mesura	O
que	O
la	O
période	O
de	O
rotation	O
radio	O
s'	O
était	O
légèrement	O
accrue	O
,	O
atteignant	O
10	O
h	O
45	O
min	O
45	O
s	O
(	O
±	O
36	O
s	O
)	O
.	O
En	O
mars	O
2007	O
,	O
il	O
a	O
été	O
annoncé	O
que	O
la	O
rotation	O
des	O
émissions	O
radio	O
ne	O
rend	O
pas	O
compte	O
de	O
la	O
rotation	O
de	O
la	O
planète	O
,	O
mais	LOCATION
est	O
causée	O
par	O
des	O
mouvements	O
de	O
convection	O
du	O
disque	O
de	O
plasma	O
entourant	O
Saturne	O
,	O
lesquels	O
sont	O
indépendants	O
de	O
la	O
rotation	O
.	O
Les	PERSON
variations	PERSON
de	PERSON
période	PERSON
pourraient	PERSON
être	PERSON
causées	PERSON
par	PERSON
les	PERSON
geysers	O
de	O
la	O
lune	O
Encelade	O
.	O
La	O
vapeur	O
d'	O
eau	O
émise	O
en	O
orbite	O
saturnienne	O
se	PERSON
chargerait	PERSON
électriquement	PERSON
et	PERSON
pèserait	PERSON
sur	PERSON
le	PERSON
champ	O
magnétique	O
de	O
la	O
planète	O
,	O
ralentissant	O
sa	O
rotation	O
par	O
rapport	O
à	O
celle	O
de	O
Saturne	O
.	O
Si	PERSON
ce	PERSON
point	PERSON
est	PERSON
vérifié	PERSON
,	O
on	O
ne	LOCATION
connaît	LOCATION
aucune	LOCATION
méthode	LOCATION
fiable	O
pour	O
déterminer	PERSON
la	PERSON
période	PERSON
de	PERSON
rotation	PERSON
réelle	PERSON
du	PERSON
noyau	PERSON
de	PERSON
Saturne	PERSON
,	O
,	O
,	O
.	O
Les	PERSON
anneaux	PERSON
de	PERSON
Saturne	PERSON
sont	O
un	O
des	O
spectacles	O
les	O
plus	O
remarquables	O
du	O
système	O
solaire	O
et	O
constituent	O
la	PERSON
caractéristique	PERSON
principale	PERSON
de	PERSON
la	PERSON
planète	O
Saturne	PERSON
.	O
Saturne	PERSON
possède	PERSON
un	O
grand	O
nombre	O
de	O
satellites	O
naturels	O
.	O
Titan	LOCATION
,	O
la	O
plus	O
grande	O
d'	O
entre	O
elles	O
,	O
plus	O
grande	O
que	O
Mercure	O
ou	O
Pluton	O
,	O
est	O
le	O
seul	O
satellite	O
du	O
système	O
solaire	O
à	O
posséder	O
une	O
atmosphère	O
dense	O
.	O
Tous	O
les	O
satellites	O
pour	O
lesquels	O
la	O
période	O
de	O
rotation	O
est	O
connue	O
,	O
à	O
l'	O
exception	O
de	O
Phœbé	O
et	O
d'	O
Hypérion	LOCATION
,	O
sont	O
synchrones	O
.	O
Saturne	PERSON
est	PERSON
une	PERSON
des	LOCATION
cinq	LOCATION
planètes	O
visibles	O
à	O
l'	O
œil	O
nu	O
la	O
nuit	O
et	O
elle	O
est	O
connue	O
et	O
observée	O
depuis	O
l'	O
Antiquité	PERSON
.	PERSON
En	O
1610	O
,	O
Galilée	O
,	O
en	O
braquant	O
son	O
télescope	PERSON
vers	O
Saturne	PERSON
,	O
en	O
observe	O
les	O
anneaux	O
mais	O
ne	O
comprend	O
pas	O
ce	O
qu'	O
il	O
en	O
est	O
,	O
décrivant	O
que	O
la	O
planète	PERSON
aurait	PERSON
des	PERSON
"	PERSON
oreilles	O
"	O
.	O
En	O
1613	O
,	O
ils	O
réapparaissent	O
sans	O
que	O
Galilée	O
puisse	O
émettre	O
une	O
hypothèse	O
quant	O
à	O
ce	O
qu'	O
il	O
observe	O
.	O
En	O
1656	O
,	O
Christian	PERSON
Huygens	PERSON
,	O
en	O
utilisant	O
un	O
télescope	O
bien	O
plus	O
puissant	O
,	O
comprend	O
que	O
la	O
planète	O
est	O
en	O
réalité	O
entourée	O
d'	O
un	PERSON
anneau	PERSON
,	O
qu'	LOCATION
il	LOCATION
pense	LOCATION
être	LOCATION
solide	O
.	O
En	O
1675	O
,	O
Jean-Dominique	PERSON
Cassini	PERSON
détermine	O
que	O
l'	O
anneau	PERSON
est	PERSON
composé	O
de	O
plusieurs	O
petits	O
anneaux	LOCATION
,	O
séparés	PERSON
par	PERSON
des	PERSON
divisions	PERSON
;	O
la	O
plus	O
large	O
d'	O
entre	O
elles	O
sera	O
plus	O
tard	O
appelée	O
la	O
division	O
de	PERSON
Cassini	PERSON
.	O
En	O
1859	O
,	O
James	PERSON
Clerk	PERSON
Maxwell	PERSON
démontre	PERSON
que	PERSON
les	O
anneaux	O
ne	O
peuvent	O
pas	O
être	O
solides	O
.	O
Il	PERSON
émet	PERSON
l'	PERSON
hypothèse	O
qu'	O
ils	O
sont	O
constitués	O
d'	O
un	O
grand	O
nombre	O
de	O
petites	O
particules	O
,	O
toutes	O
orbitant	O
autour	O
de	PERSON
Saturne	PERSON
indépendamment	PERSON
.	PERSON
Pioneer	O
11	O
passa	O
à	O
22	O
000	O
km	O
des	O
nuages	O
de	O
Saturne	O
en	O
septembre	O
1979	O
.	O
Elle	PERSON
étudia	PERSON
l'	PERSON
étalement	O
des	PERSON
anneaux	PERSON
,	O
découvrit	O
l'	O
anneau	O
F	O
et	O
le	O
fait	O
que	O
les	O
divisions	O
ne	O
sont	O
pas	O
vides	O
de	PERSON
matériaux	PERSON
.	PERSON
Pioneer	O
11	O
mesura	LOCATION
également	LOCATION
la	O
température	O
de	O
Titan	O
.	O
En	O
novembre	O
1980	O
,	O
Voyager	O
1	O
visita	O
le	O
système	O
saturnien	O
.	O
En	O
août	O
1981	O
,	O
Voyager	O
2	O
continua	O
l'	O
étude	O
de	O
Saturne	O
.	O
La	O
gravité	O
de	PERSON
Saturne	PERSON
fut	PERSON
utilisée	O
pour	O
diriger	O
la	O
sonde	O
vers	O
Uranus	LOCATION
(	O
voir	O
cette	O
planète	O
)	O
qui	O
,	O
à	O
son	O
tour	O
,	O
la	O
dirigea	O
vers	O
Neptune	O
.	O
Elles	PERSON
découvrirent	PERSON
également	PERSON
la	O
division	O
de	PERSON
Maxwell	PERSON
et	O
la	O
division	O
de	PERSON
Keeler	PERSON
.	O
La	O
sonde	O
Cassini-Huygens	O
s'	O
est	O
placée	O
en	O
orbite	O
autour	O
de	PERSON
Saturne	PERSON
le	PERSON
1	O
er	O
juillet	O
2004	O
afin	O
d'	O
étudier	O
le	PERSON
système	PERSON
saturnien	PERSON
,	O
avec	O
une	O
attention	O
particulière	O
pour	O
Titan	LOCATION
.	O
En	O
juin	O
2004	O
,	O
elle	PERSON
effectua	PERSON
un	PERSON
survol	PERSON
de	PERSON
Phœbé	PERSON
.	O
En	O
mars	O
2007	O
,	O
de	O
nouvelles	O
images	O
du	O
pôle	O
mirent	O
en	O
évidence	O
des	O
mers	O
d'	O
hydrocarbures	O
,	O
la	O
plus	O
grande	O
ayant	O
presque	O
la	O
taille	O
de	O
la	O
mer	O
Caspienne	O
.	O
