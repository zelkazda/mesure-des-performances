Grâce	O
à	O
son	O
goût	O
pour	O
les	O
études	O
,	O
son	O
père	O
l'	O
envoie	O
étudier	O
à	O
Moscou	LOCATION
.	O
Il	O
y	O
rencontre	O
le	O
philosophe	O
Nikolaï	PERSON
Fiodorov	PERSON
,	O
qui	LOCATION
le	LOCATION
guidera	O
pendant	O
trois	O
ans	O
en	O
le	O
faisant	O
progresser	O
dans	O
les	O
sciences	O
et	O
lui	O
ouvrira	O
des	O
horizons	O
divers	O
comme	O
l'	O
exploration	O
de	O
l'	O
espace	O
.	O
En	O
1882	O
,	O
à	O
25	O
ans	O
,	O
il	PERSON
est	PERSON
nommé	PERSON
professeur	O
de	PERSON
mathématiques	PERSON
et	PERSON
de	PERSON
physique	PERSON
au	PERSON
collège	O
de	O
Borovsk	O
,	O
dans	O
le	PERSON
gouvernement	PERSON
de	PERSON
Kalouga	PERSON
.	O
En	O
1895	O
,	O
reprenant	O
le	O
modèle	O
de	O
la	O
Tour	O
Eiffel	O
,	O
il	PERSON
imagine	O
une	O
tour	O
de	O
36	O
000	O
km	O
de	O
haut	O
,	O
qui	O
permettrait	O
d'	O
amener	O
par	O
un	O
ascenseur	O
des	O
charges	O
en	O
orbites	O
.	O
En	O
1897	O
,	O
il	O
fabrique	O
et	O
expérimente	O
une	O
petite	O
soufflerie	O
,	O
la	O
première	O
en	O
Russie	LOCATION
.	O
Tsiolkovski	PERSON
décède	PERSON
à	O
Kalouga	PERSON
le	PERSON
19	O
septembre	O
1935	O
,	O
à	O
l'	O
âge	O
de	O
78	O
ans	O
.	O
