Michel	PERSON
Destot	PERSON
est	PERSON
un	O
homme	O
politique	O
français	O
,	O
né	O
le	O
2	O
septembre	O
1946	O
,	O
à	O
Malo-les-Bains	O
(	O
Nord	O
)	O
.	O
Il	O
est	O
diplômé	O
de	O
l'	O
École	O
nationale	O
supérieure	O
d'	O
arts	O
et	O
métiers	O
.	O
Membre	O
du	O
Parti	O
socialiste	O
,	O
il	PERSON
est	O
:	O
En	O
1977	O
,	O
il	PERSON
devient	PERSON
pour	O
la	O
première	O
fois	O
conseiller	O
municipal	O
,	O
élu	O
sur	O
la	O
liste	O
d'	O
Hubert	PERSON
Dubedout	PERSON
,	O
alors	PERSON
député	PERSON
--	PERSON
maire	PERSON
de	PERSON
Grenoble	PERSON
.	O
Il	PERSON
prend	PERSON
la	PERSON
présidence	PERSON
du	PERSON
groupe	PERSON
d'	O
opposition	O
socialiste	O
et	O
devient	O
le	O
principal	O
contradicteur	O
d'	O
Alain	PERSON
Carignon	PERSON
après	PERSON
les	PERSON
municipales	PERSON
de	PERSON
1989	PERSON
.	O
Élu	PERSON
maire	PERSON
de	PERSON
Grenoble	PERSON
en	O
1995	O
face	O
à	O
Richard	PERSON
Cazenave	PERSON
.	O
Michel	PERSON
Destot	PERSON
,	O
a	O
fait	O
face	O
durant	O
son	O
mandat	O
de	PERSON
maire	PERSON
à	PERSON
Grenoble	PERSON
à	PERSON
plusieurs	PERSON
controverses	O
.	O
La	O
création	O
de	O
ce	O
stade	O
nécessitant	O
,	O
en	O
effet	O
,	O
le	O
réaménagement	O
d'	O
une	O
partie	O
importante	O
du	O
seul	O
grand	O
parc	LOCATION
grenoblois	O
du	O
centre	O
ville	LOCATION
(	O
le	O
parc	PERSON
Paul	PERSON
Mistral	PERSON
)	O
.	O
Le	O
projet	O
de	O
rocade	O
Nord	O
,	O
relancé	LOCATION
en	O
2007	O
,	O
a	O
suscité	O
de	O
vives	O
émotions	O
chez	O
les	O
écologistes	O
.	O
En	O
1985	O
,	O
il	LOCATION
est	LOCATION
élu	LOCATION
conseiller	O
général	O
de	O
l'	O
Isère	PERSON
,	O
sur	LOCATION
le	O
canton	O
de	O
Grenoble-3	O
.	O
Il	O
intervient	O
régulièrement	O
au	O
sein	O
du	O
groupe	O
SRC	O
,	O
en	O
commission	O
ou	O
lors	O
des	O
débats	O
publics	O
sur	LOCATION
les	O
questions	O
relatives	O
aux	O
relations	O
internationales	O
,	O
aux	LOCATION
inégalités	LOCATION
nord/sud	LOCATION
,	O
au	O
développement	O
durable	O
,	O
aux	O
transports	O
,	O
à	O
la	O
politique	O
de	O
la	O
ville	O
,	O
aux	O
enjeux	PERSON
énergétiques	PERSON
,	O
...	O
Michel	PERSON
Destot	PERSON
fut	PERSON
en	O
effet	O
un	O
proche	O
de	O
Michel	O
Rocard	O
,	O
au	O
PSU	O
d'	O
abord	O
,	O
au	O
PS	O
ensuite	O
,	O
et	O
a	O
été	O
au	O
sein	O
du	O
PS	O
l'	O
un	O
des	O
animateurs	O
de	O
la	O
sensibilité	O
rocardienne	O
.	O
