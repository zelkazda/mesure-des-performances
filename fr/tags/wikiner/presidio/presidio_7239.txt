Louis	PERSON
Antoine	PERSON
Léon	PERSON
de	PERSON
Saint-Just	PERSON
est	O
un	O
homme	O
politique	O
français	O
,	O
né	O
à	O
Decize	O
(	O
Nièvre	O
)	O
le	O
25	O
août	O
1767	O
et	O
mort	O
à	O
Paris	LOCATION
le	O
28	O
juillet	O
1794	O
,	O
à	O
26	O
ans	O
,	O
qui	O
se	O
distingua	PERSON
pour	O
son	O
intransigeance	O
sous	O
la	O
Terreur	O
.	O
De	O
septembre	O
1786	O
à	O
mars	O
1787	O
,	O
Saint-Just	O
est	O
placé	O
,	O
à	LOCATION
la	LOCATION
demande	O
de	PERSON
sa	PERSON
mère	PERSON
,	O
dans	O
une	O
maison	O
de	O
correction	O
à	O
Paris	LOCATION
,	O
rue	O
de	O
Picpus	O
,	O
à	O
la	O
suite	O
d'	O
une	O
fugue	O
.	O
Il	O
fait	O
la	O
connaissance	O
de	O
Robespierre	O
,	O
à	O
qui	PERSON
il	PERSON
écrit	PERSON
une	PERSON
première	O
lettre	O
en	O
août	O
1790	O
,	O
et	O
dont	O
il	O
devient	O
un	O
des	O
proches	O
.	O
Député	O
en	O
1791	O
à	O
l'	O
Assemblée	O
législative	O
,	O
on	O
lui	O
refuse	O
le	O
droit	O
de	O
siéger	O
en	O
raison	O
de	O
son	O
âge	O
.	O
De	O
retour	O
dès	O
le	O
31	O
mars	O
à	O
Paris	LOCATION
,	O
où	LOCATION
il	LOCATION
intervient	O
aux	PERSON
Jacobins	PERSON
,	O
sa	O
mission	O
prend	O
officiellement	O
fin	O
par	O
décret	O
du	O
30	O
avril	O
.	O
Saint-Just	O
fait	O
prendre	O
Bitche	O
et	O
délivrer	O
Landau	PERSON
.	O
De	O
retour	O
à	O
Paris	LOCATION
,	O
il	O
est	O
l'	O
un	O
des	O
acteurs	O
de	O
la	O
chute	O
des	O
hébertistes	O
,	O
puis	LOCATION
des	LOCATION
dantonistes	LOCATION
.	O
Le	O
Comité	O
l'	O
ayant	O
rappelé	O
par	O
une	O
lettre	PERSON
datée	PERSON
du	PERSON
6	PERSON
prairial	O
(	O
25	O
mai	O
)	O
,	O
il	O
rentre	O
à	O
Paris	LOCATION
le	O
12	O
(	O
31	O
mai	O
)	O
.	O
Lors	O
de	O
la	O
crise	O
de	O
thermidor	O
,	O
il	LOCATION
tente	LOCATION
,	O
avec	PERSON
Barère	PERSON
,	O
de	PERSON
rétablir	PERSON
la	PERSON
concorde	PERSON
au	PERSON
sein	PERSON
des	O
comités	O
,	O
organisant	O
notamment	O
la	O
réunion	O
du	O
5	O
thermidor	O
.	O
Mais	PERSON
le	PERSON
discours	O
de	PERSON
Robespierre	PERSON
devant	O
l'	O
assemblée	O
,	O
le	O
8	O
thermidor	O
accélère	O
le	PERSON
dénouement	PERSON
de	PERSON
la	PERSON
crise	PERSON
.	O
Pris	O
à	O
partie	O
,	O
dans	O
la	O
nuit	O
,	O
par	O
Billaud-Varenne	O
et	O
Collot	O
d'	O
Herbois	O
,	O
il	O
réoriente	O
son	O
discours	O
dans	O
un	O
sens	O
plus	O
critique	O
à	O
l'	O
égard	O
de	O
ces	O
deux	O
hommes	O
,	O
indiquant	O
au	O
cinquième	O
paragraphe	O
:	O
"	O
quelqu'	O
un	O
cette	O
nuit	O
a	O
flétri	O
mon	PERSON
cœur	PERSON
"	O
.	O
Le	O
lendemain	O
,	O
alors	O
qu'	O
il	O
commence	O
son	O
discours	O
,	O
il	LOCATION
est	LOCATION
interrompu	O
par	O
Tallien	O
et	O
,	O
plutôt	O
que	O
de	O
se	O
battre	O
,	O
se	O
mure	PERSON
dans	O
un	O
silence	O
énigmatique	O
;	O
il	O
est	O
décrété	O
d'	O
accusation	O
.	O
