Le	LOCATION
Jutland	LOCATION
et	O
les	O
îles	O
danoises	O
sont	O
peuplées	PERSON
depuis	PERSON
plusieurs	PERSON
milliers	PERSON
d'	PERSON
années	PERSON
.	PERSON
L'	O
unité	O
du	O
Danemark	O
fut	O
réalisée	O
par	O
Harald	PERSON
"	PERSON
à	PERSON
la	PERSON
dent	PERSON
bleue	PERSON
"	PERSON
(	O
Harald	PERSON
Blåtand	PERSON
)	O
vers	O
980	O
.	O
À	O
un	O
moment	O
ou	O
à	O
un	O
autre	O
,	O
le	O
royaume	O
contrôla	O
l'	O
Angleterre	O
,	O
la	O
Suède	LOCATION
,	O
la	LOCATION
Norvège	LOCATION
,	O
la	O
mer	O
Baltique	O
et	O
des	O
territoires	O
en	O
Allemagne	O
.	O
La	O
partie	O
sud	O
de	O
la	O
Suède	O
moderne	O
,	O
appelée	O
Scanie	O
(	O
Skåne	LOCATION
)	O
,	O
était	O
une	O
partie	O
intégrante	PERSON
du	PERSON
Danemark	PERSON
jusqu'	PERSON
au	PERSON
traité	PERSON
de	PERSON
Roskilde	PERSON
en	O
1658	O
.	O
Si	PERSON
la	PERSON
Suède	PERSON
chercha	O
rapidement	O
à	O
recouvrer	O
son	O
indépendance	O
grâce	O
à	O
Gustave	O
Vasa	O
en	O
1523	O
,	O
notamment	O
en	O
mettant	O
à	O
profit	O
le	O
conflit	O
entre	O
la	O
noblesse	O
danoise	PERSON
et	PERSON
le	PERSON
roi	PERSON
Christian	PERSON
II	PERSON
,	O
l'	O
union	O
avec	O
la	O
Norvège	O
n'	O
a	O
été	O
dissoute	O
qu'	O
en	O
1814	O
après	O
la	O
chute	O
de	O
Napoléon	O
avec	O
qui	O
Frédéric	O
VI	O
avait	O
choisi	O
de	O
s'	O
allier	O
.	O
Resté	PERSON
neutre	O
pendant	O
la	O
Première	PERSON
Guerre	PERSON
mondiale	PERSON
,	O
le	PERSON
Danemark	PERSON
a	O
récupéré	LOCATION
en	O
1920	O
une	O
grande	O
partie	O
du	O
Schleswig-Holstein	O
qu'	O
il	LOCATION
avait	LOCATION
dû	LOCATION
abandonner	LOCATION
en	O
1864	O
à	O
la	O
Prusse	O
et	O
l'	O
Autriche	O
suite	O
à	O
la	O
guerre	O
des	O
Duchés	O
.	O
Le	O
9	O
avril	O
1940	O
,	O
l'	O
Allemagne	O
a	O
envahi	O
le	PERSON
Danemark	PERSON
,	O
lui	O
proposant	O
en	O
vain	O
le	PERSON
protectorat	PERSON
,	O
et	O
le	O
pays	O
fut	O
occupé	O
pendant	O
toute	O
la	O
Seconde	O
Guerre	O
mondiale	O
,	O
malgré	O
les	O
efforts	O
de	O
quelques	O
résistants	O
danois	O
.	O
Son	PERSON
entrée	PERSON
au	PERSON
sein	O
de	O
l'	O
Union	O
européenne	O
date	O
de	O
1973	O
.	O
Le	PERSON
Danemark	PERSON
est	PERSON
constitué	PERSON
d'	PERSON
une	O
péninsule	O
,	O
le	LOCATION
Jutland	LOCATION
(	O
Jylland	O
)	O
et	O
de	O
443	O
îles	O
,	O
dont	O
72	O
sont	PERSON
habitées	PERSON
.	O
Les	O
plus	O
importantes	O
sont	O
l'	O
île	O
de	O
Fionie	O
(	O
Fyn	PERSON
)	O
et	O
le	O
Seeland	O
(	O
Sjælland	LOCATION
)	O
.	O
L'	O
île	O
de	PERSON
Bornholm	PERSON
est	PERSON
située	PERSON
à	O
l'	O
est	O
du	O
reste	O
du	O
pays	O
dans	O
la	O
mer	O
Baltique	O
.	O
Beaucoup	O
d'	O
îles	O
sont	O
reliées	O
par	O
des	O
ponts	O
;	O
le	O
pont	O
d'	O
Øresund	LOCATION
relie	O
le	LOCATION
Seeland	LOCATION
avec	LOCATION
la	LOCATION
région	LOCATION
de	LOCATION
Scanie	O
en	O
Suède	LOCATION
.	O
Les	O
plus	O
grandes	O
villes	LOCATION
sont	O
Copenhague	LOCATION
(	O
sur	O
l'	O
île	O
de	O
Seeland	O
)	O
,	O
Aarhus	LOCATION
(	O
dans	O
le	LOCATION
Jutland	LOCATION
)	O
,	O
et	O
Odense	O
sur	O
l'	O
île	O
de	O
Fionie	O
.	O
Le	O
Groenland	O
bénéficie	O
d'	O
une	O
autonomie	O
politique	O
depuis	O
1994	O
,	O
fortement	O
étendue	O
à	O
la	O
suite	O
du	O
vote	O
du	O
25	O
novembre	O
2008	O
.	O
Ses	O
56500	O
habitants	O
ont	O
choisi	O
,	O
au	O
cours	O
d'	O
un	O
référendum	O
en	O
1982	O
(	O
entré	O
en	O
vigueur	O
le	O
1	O
er	O
février	O
1985	O
)	O
,	O
de	O
ne	O
plus	O
faire	O
partie	O
de	O
la	O
Communauté	O
européenne	O
et	O
de	O
la	O
CECA	O
auxquelles	O
leur	O
territoire	O
appartenait	LOCATION
depuis	LOCATION
le	LOCATION
1	LOCATION
er	LOCATION
janvier	LOCATION
1973	O
.	O
Suite	O
au	O
référendum	O
du	O
25	O
novembre	O
2008	O
,	O
le	LOCATION
Groenland	LOCATION
a	O
accédé	O
le	O
21	O
juin	O
2009	O
à	O
une	O
autonomie	O
renforcée	O
.	O
Le	PERSON
Danemark	PERSON
lui	PERSON
cède	PERSON
32	PERSON
domaines	O
de	O
compétences	O
,	O
dont	O
ceux	O
de	O
la	O
police	O
et	O
de	O
la	O
justice	O
.	O
La	O
capitale	O
du	O
Groenland	O
est	O
Nuuk	O
(	O
ou	O
Godthåb	O
en	O
danois	PERSON
)	O
.	O
Le	O
Groenland	O
et	O
les	O
îles	O
Féroé	O
sont	O
deux	O
régions	O
autonomes	O
rattachées	O
au	LOCATION
Danemark	LOCATION
.	O
Le	O
pays	O
est	O
cependant	O
encore	O
grevé	PERSON
par	PERSON
une	PERSON
relative	O
artificialisation	O
du	O
territoire	O
et	O
par	O
la	O
forte	O
dégradation	O
écologique	O
de	O
la	O
mer	O
Baltique	O
(	O
zones	O
mortes	O
,	O
métaux	PERSON
lourds	PERSON
,	O
radioactivité	O
,	O
surpêche	O
,	O
eutrophisation	O
et	O
"	O
zones	O
mortes	O
"	O
dans	O
le	PERSON
Skagerrak	PERSON
.	O
En	O
1848	O
,	O
le	PERSON
Danemark	PERSON
est	PERSON
devenu	PERSON
une	PERSON
monarchie	PERSON
constitutionnelle	O
avec	O
la	O
ratification	O
d'	O
une	O
nouvelle	O
constitution	O
.	O
Le	O
pouvoir	O
législatif	O
est	O
exercé	LOCATION
par	O
le	O
parlement	O
,	O
connu	O
en	O
danois	O
comme	O
le	O
Folketing	O
qui	O
comprend	O
179	O
membres	O
(	O
dont	O
175	O
représentent	O
le	PERSON
Danemark	PERSON
métropolitain	O
)	O
.	O
Les	PERSON
tribunaux	PERSON
du	O
Danemark	O
sont	O
indépendants	O
du	O
pouvoir	O
législatif	O
et	O
exécutif	O
(	O
séparation	O
des	O
pouvoirs	O
du	O
type	O
de	O
Montesquieu	O
)	O
.	O
Tous	O
les	O
partis	O
qui	O
obtiennent	O
au	O
moins	O
2	O
%	O
des	O
voix	O
sont	O
représentés	O
au	O
Folketing	O
.	O
Mais	PERSON
l'	O
invasion	O
du	O
pays	O
par	O
l'	O
Allemagne	O
nazie	O
en	O
1940	O
a	O
montré	O
les	O
limites	O
de	O
la	O
neutralité	O
et	O
le	O
pays	O
a	O
,	O
depuis	O
la	O
fin	O
de	O
la	O
Seconde	O
Guerre	O
mondiale	O
,	O
adopté	O
pour	O
sa	O
politique	O
extérieure	O
une	O
orientation	O
très	PERSON
atlantiste	PERSON
.	O
Le	O
gouvernement	O
et	O
le	O
parlement	O
sont	O
par	O
ailleurs	O
en	O
dialogue	O
permanent	O
avec	O
les	O
autres	O
pays	O
scandinaves	O
dans	O
le	O
cadre	O
du	O
Conseil	O
nordique	O
.	O
Au	O
Danemark	O
,	O
chaque	O
parti	O
est	O
désigné	O
par	O
une	O
lettre	O
de	O
l'	O
alphabet	O
.	O
Il	O
existe	O
une	O
multitude	O
de	O
partis	O
minoritaires	O
non	O
représentés	O
au	O
Folketing	O
.	O
Le	O
mouvement	O
contre	O
l'	O
Union	O
européenne	O
et	O
le	O
mouvement	O
de	O
juin	O
(	O
une	O
scission	O
du	O
précédent	O
)	O
sont	O
représentés	O
au	O
Parlement	O
européen	O
et	O
ne	O
se	O
présentent	O
que	O
lors	O
des	O
élections	O
européennes	O
.	O
Au	LOCATION
Groenland	LOCATION
et	O
aux	O
îles	O
Féroé	O
,	O
des	O
partis	O
locaux	O
sont	O
représentés	O
au	O
Folketing	O
.	O
Le	PERSON
Danemark	PERSON
a	O
une	O
économie	O
de	O
marché	O
moderne	O
.	O
Le	O
différentiel	O
des	O
besoins	O
provient	O
de	O
centrales	O
thermiques	O
et	O
d'	O
importations	O
principalement	O
de	O
Suède	O
.	O
Un	O
nouveau	O
référendum	O
sur	O
l'	O
adhésion	O
du	PERSON
Danemark	PERSON
à	O
la	O
zone	O
euro	O
aurait	O
pu	O
se	O
tenir	O
au	O
deuxième	O
semestre	O
2008	O
,	O
mais	O
l'	O
idée	O
a	O
été	O
repoussée	O
.	O
La	O
majorité	O
de	O
la	O
population	O
est	O
d'	O
origine	O
scandinave	O
et	O
le	O
danois	O
est	O
parlé	O
partout	O
au	PERSON
Danemark	PERSON
.	O
Le	O
Christianisme	O
a	O
été	O
introduit	O
au	O
Danemark	O
il	O
y	O
a	O
plus	O
de	O
1000	O
ans	O
.	O
Le	O
luthéranisme	O
est	O
maintenant	O
la	O
religion	O
dominante	O
au	LOCATION
Danemark	LOCATION
.	O
Le	O
Danemark	O
est	O
divisé	O
en	O
10	O
évêchés	O
(	O
Aalborg	O
,	O
Viborg	LOCATION
,	O
Århus	LOCATION
,	O
Ribe	O
,	O
Haderslev	LOCATION
,	O
Odense	LOCATION
,	O
Maribo	LOCATION
,	O
Roskilde	LOCATION
,	O
Helsingør	O
et	O
Copenhague	O
)	O
.	O
Dans	O
le	O
sud	O
du	O
Jutland	O
,	O
les	O
règles	O
sont	O
différentes	O
.	O
Depuis	O
1947	O
,	O
les	O
femmes	O
peuvent	O
être	O
pasteurs	O
au	O
Danemark	O
.	O
On	O
y	O
chante	O
des	O
psaumes	O
et	O
écoute	O
le	O
prêche	O
du	O
pasteur	O
concernant	O
le	O
texte	O
de	O
la	O
Bible	O
choisi	O
.	O
Il	O
y	O
a	O
aussi	O
des	O
messes	O
particulières	O
à	O
Noël	O
,	O
à	O
Pâques	O
ou	O
à	O
la	O
Pentecôte	O
.	O
Un	O
seul	O
diocèse	O
catholique	O
existe	O
au	O
Danemark	O
,	O
dont	O
l'	O
évêché	O
se	O
trouve	O
à	O
Copenhague	LOCATION
.	O
Hans	PERSON
Christian	PERSON
Andersen	PERSON
est	PERSON
un	PERSON
écrivain	PERSON
surtout	PERSON
célèbre	PERSON
pour	O
ses	O
contes	O
comme	O
Les	PERSON
Habits	PERSON
neufs	O
de	O
l'	O
empereur	O
et	O
Le	O
vilain	O
petit	O
canard	O
.	O
En	O
musique	O
,	O
notons	O
que	O
le	O
batteur	O
du	O
groupe	O
Metallica	O
,	O
Lars	PERSON
Ulrich	PERSON
,	O
est	O
originaire	O
du	PERSON
Danemark	PERSON
.	O
Le	PERSON
Danemark	PERSON
a	O
pour	O
codes	O
:	O
