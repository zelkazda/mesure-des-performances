Noir	O
Désir	O
est	O
un	O
groupe	O
de	O
rock	O
français	O
originaire	O
de	O
Bordeaux	O
,	O
formé	O
dans	O
les	O
années	O
1980	O
.	O
Il	O
se	O
compose	O
de	PERSON
Bertrand	PERSON
Cantat	PERSON
(	O
chant	O
et	O
guitare	O
)	O
,	O
Serge	PERSON
Teyssot-Gay	PERSON
(	O
guitare	O
)	O
,	O
Denis	PERSON
Barthe	PERSON
(	O
batterie	O
)	O
et	O
Jean-Paul	PERSON
Roy	PERSON
(	O
basse	O
)	O
.	O
Le	O
bassiste	O
originel	O
était	O
Frédéric	PERSON
Vidalenc	PERSON
jusqu'	PERSON
en	O
1996	O
.	O
Serge	PERSON
revient	O
en	O
1985	O
et	O
la	O
formation	O
restera	O
stable	O
pendant	O
dix	O
ans	O
.	O
Aux	O
débuts	O
du	O
groupe	O
,	O
Bertrand	PERSON
Cantat	PERSON
affiche	O
clairement	O
sa	O
passion	O
pour	O
Jim	PERSON
Morrison	PERSON
,	O
chanteur	O
du	O
groupe	O
The	O
Doors	O
en	O
arborant	O
des	O
pantalons	O
de	O
cuirs	O
et	O
des	O
colliers	O
indiens	LOCATION
.	O
L'	O
album	O
est	O
intitulé	O
Où	O
veux-tu	O
qu'	O
je	O
r'garde	O
?	O
.	O
Leurs	O
deux	O
albums	O
suivants	O
,	O
Du	PERSON
ciment	PERSON
sous	PERSON
les	O
plaines	O
et	O
Tostaky	O
,	O
cultivent	O
une	O
noirceur	O
qui	O
les	O
rattache	O
plutôt	PERSON
à	PERSON
une	PERSON
mouvance	PERSON
underground	PERSON
,	O
due	O
aux	LOCATION
sonorités	LOCATION
très	LOCATION
épurées	LOCATION
et	O
profondément	O
rock	O
de	O
l'	O
album	O
Tostaky	O
.	O
Le	O
groupe	O
bâtit	O
l'	O
essentiel	O
de	O
son	O
succès	O
sur	O
scène	O
,	O
avec	O
en	O
particulier	O
la	O
tournée	O
qui	O
sera	O
enregistrée	O
sur	O
Dies	O
Irae	O
et	O
après	O
laquelle	PERSON
Bertrand	PERSON
Cantat	PERSON
devra	PERSON
se	PERSON
faire	PERSON
opérer	PERSON
une	PERSON
première	PERSON
fois	PERSON
des	PERSON
cordes	PERSON
vocales	PERSON
suite	PERSON
aux	PERSON
nombreux	PERSON
hurlements	PERSON
qu'	O
il	O
éructe	O
sur	LOCATION
la	O
majeure	O
partie	O
des	O
chansons	O
.	O
Cependant	O
,	O
la	O
maturité	PERSON
aidant	PERSON
,	O
Noir	O
Désir	O
s'	O
ouvre	O
à	O
d'	O
autres	O
horizons	O
,	O
en	O
publiant	O
d'	O
abord	O
un	PERSON
album	PERSON
de	PERSON
remixes	PERSON
,	O
One	O
Trip/One	O
Noise	O
,	O
puis	O
son	O
dernier	PERSON
album	O
studio	O
à	LOCATION
ce	LOCATION
jour	LOCATION
,	O
Des	LOCATION
Visages	LOCATION
,	O
Des	O
Figures	O
.	O
Au-delà	O
de	O
l'	O
évolution	O
de	O
son	O
style	O
musical	O
,	O
Noir	PERSON
Désir	PERSON
reste	O
caractérisé	O
par	O
des	O
textes	O
travaillés	O
,	O
écrits	O
par	O
Bertrand	O
Cantat	O
,	O
où	LOCATION
se	O
mêlent	O
homophonies	O
,	O
de	PERSON
très	PERSON
nombreux	PERSON
calembours	PERSON
et	O
pastiches	O
pour	O
former	O
une	O
prose	O
poétique	O
d'	O
une	O
richesse	O
et	O
d'	O
une	O
force	O
très	O
particulières	O
,	O
souvent	O
grâce	O
au	O
parlé/chanté	O
.	O
Le	O
lundi	O
29	O
mars	O
2004	O
,	O
Bertrand	PERSON
Cantat	PERSON
est	PERSON
condamné	O
par	O
le	O
tribunal	O
de	O
Vilnius	O
à	O
huit	O
ans	O
de	O
prison	O
ferme	O
pour	O
l'	O
homicide	O
de	PERSON
Marie	PERSON
Trintignant	PERSON
survenu	O
à	O
Vilnius	O
la	O
nuit	O
du	O
26	O
au	O
27	O
juillet	O
2003	O
.	O
En	O
octobre	O
2005	O
,	O
les	O
membres	O
du	O
groupe	O
confient	O
dans	O
un	O
magazine	O
musical	O
la	O
possibilité	O
de	O
ressortir	O
un	O
album	O
dans	O
les	O
années	O
à	O
venir	O
,	O
à	O
la	O
sortie	O
de	O
prison	O
de	O
Bertrand	O
Cantat	O
.	O
Ils	O
sont	O
toujours	O
sous	O
contrat	O
avec	O
leur	O
maison	O
de	O
disque	O
,	O
et	O
les	O
deux	O
futurs	O
éventuels	O
albums	O
sortiront	LOCATION
sous	O
le	O
label	O
Barclay	O
(	O
Universal	O
Music	O
)	O
.	O
À	LOCATION
cette	LOCATION
occasion	LOCATION
,	O
le	PERSON
batteur	PERSON
Denis	PERSON
Barthe	PERSON
et	PERSON
le	PERSON
bassiste	O
Jean	PERSON
Paul	PERSON
Roy	PERSON
ont	PERSON
joué	PERSON
avec	PERSON
leur	PERSON
nouveau	PERSON
groupe	O
The	O
Hyènes	O
;	O
c'	O
est	O
la	O
première	O
fois	O
,	O
que	O
les	O
deux	O
membres	O
rejouent	O
ensemble	O
sur	O
scène	O
depuis	O
la	O
séparation	O
de	O
Noir	O
Désir	O
.	O
Bertrand	PERSON
Cantat	PERSON
dépose	O
le	O
22	O
juillet	O
2007	O
une	O
demande	O
de	O
libération	O
conditionnelle	O
,	O
qui	PERSON
lui	PERSON
est	PERSON
accordée	O
le	O
15	O
octobre	O
.	O
Le	O
8	O
mai	O
2008	O
,	O
Serge	PERSON
Teyssot-Gay	PERSON
déclare	PERSON
dans	PERSON
une	O
interview	O
au	O
journal	O
suisse	O
La	O
Gruyère	O
que	O
le	O
groupe	O
"	O
projette	O
d'	O
enregistrer	O
un	O
album	O
durant	O
l'	O
hiver	O
prochain	O
"	O
.	O
Le	O
groupe	O
s'	O
est	O
réuni	O
plusieurs	O
fois	O
dans	O
le	O
studio	O
de	PERSON
Denis	PERSON
Barthe	PERSON
pour	O
travailler	O
,	O
mais	PERSON
aucune	PERSON
date	O
d'	O
enregistrement	O
n'	O
est	O
à	O
ce	O
jour	O
prévue	O
.	O
Finalement	PERSON
le	PERSON
retour	PERSON
de	PERSON
Noir	PERSON
Désir	PERSON
,	O
qui	O
ne	O
fut	O
jamais	O
dissous	O
,	O
eut	O
lieu	O
le	O
12	O
novembre	O
2008	O
,	O
quand	O
deux	O
nouvelles	O
chansons	O
furent	O
mises	O
en	O
ligne	O
sur	O
le	O
site	O
officiel	O
du	O
groupe	O
sans	O
aucune	O
promotion	O
préalable	O
.	O
Le	O
site	O
annonce	O
par	O
la	O
même	O
occasion	O
que	O
"	O
Noir	O
Désir	O
est	O
au	O
travail	O
...	O
"	O
.	O
Le	O
27	O
décembre	O
2008	O
,	O
Serge	PERSON
Teyssot-Gay	PERSON
déclare	PERSON
dans	O
Le	O
Monde	O
que	O
le	O
groupe	O
,	O
actuellement	O
en	O
sessions	O
de	O
composition	O
et	O
d'	O
enregistrement	O
,	O
compte	O
réaliser	O
un	O
album	O
en	O
2009	O
.	O
Ce	O
nouvel	O
album	O
de	O
Noir	O
Désir	O
ne	O
sortirait	O
finalement	O
pas	O
avant	O
fin	O
2010	O
en	O
raison	O
de	O
retard	O
dans	O
l'	O
écriture	O
des	O
textes	O
alors	O
que	O
la	O
musique	O
est	O
déjà	O
composée	O
depuis	PERSON
longtemps	PERSON
.	O
Le	O
29	O
juillet	O
2010	O
,	O
Bertrand	PERSON
Cantat	PERSON
est	PERSON
officiellement	O
libre	O
,	O
sa	PERSON
peine	PERSON
étant	O
purgée	PERSON
et	O
son	O
contrôle	PERSON
judiciaire	PERSON
terminé	PERSON
.	O
Serge	PERSON
Teyssot-Gay	PERSON
est	PERSON
lui	PERSON
très	PERSON
porté	PERSON
sur	PERSON
le	O
noisecore	O
,	O
appréciant	O
particulièrement	O
des	O
groupes	O
comme	O
The	O
Jesus	O
Lizard	O
,	O
Shellac	O
(	O
il	O
a	O
d'	O
ailleurs	O
joué	PERSON
avec	PERSON
son	PERSON
groupe	O
Zone	O
Libre	O
en	O
première	O
partie	O
de	O
la	O
tournée	O
française	O
de	O
Shellac	O
)	O
ou	O
les	O
projets	O
solo	O
du	O
guitariste	O
de	O
Sonic	O
Youth	O
,	O
Lee	PERSON
Ranaldo	PERSON
.	O
Il	O
convient	O
d'	O
y	O
ajouter	O
l'	O
influence	O
de	PERSON
Paul	PERSON
Morand	PERSON
,	O
les	O
chansons	O
"	O
L'	O
Homme	O
Pressé	O
"	O
et	O
"	O
Fin	O
de	O
siècle	O
"	O
étant	O
également	O
le	PERSON
titre	PERSON
de	PERSON
deux	PERSON
ouvrages	PERSON
de	PERSON
l'	O
auteur	O
.	O
D'	O
autres	O
[	O
réf.	O
nécessaire	O
]	O
remarquent	O
des	O
influences	O
de	PERSON
Charles	PERSON
Baudelaire	PERSON
;	O
la	PERSON
poésie	PERSON
de	PERSON
Jim	PERSON
Morrison	PERSON
;	O
Vladimir	PERSON
Nabokov	PERSON
;	O
Paul	PERSON
Éluard	PERSON
;	O
la	PERSON
poésie	PERSON
de	PERSON
Léo	PERSON
Ferré	PERSON
;	O
Arthur	PERSON
Rimbaud	PERSON
(	O
Toujours	PERSON
être	PERSON
ailleurs	PERSON
)	O
;	O
Attila	PERSON
József	PERSON
,	O
etc	O
.	O
Noir	O
Désir	O
est	O
aussi	PERSON
un	PERSON
groupe	PERSON
militant	O
contre	O
la	O
mondialisation	O
capitaliste	O
(	O
The	O
Holy	O
Economic	O
War	O
,	O
L'	O
Homme	LOCATION
pressé	LOCATION
,	O
L'	O
Europe	LOCATION
)	O
et	O
contre	O
le	O
fascisme	O
.	O
"	O
Nous	O
ne	O
sommes	O
pas	O
dupes	O
de	O
ton	O
manège	LOCATION
,	O
et	O
si	O
nous	O
sommes	O
tous	O
embarqués	O
sur	O
la	O
même	O
planète	O
,	O
on	O
n'	O
est	O
décidément	O
pas	O
du	O
même	O
monde	O
"	O
a-t-il	O
déclaré	O
,	O
aux	O
côtés	O
de	O
Jean-Luc	O
Delarue	O
,	O
présentateur	O
soudainement	O
désorienté	O
.	O
Le	O
groupe	O
a	O
également	O
participé	O
à	O
plusieurs	O
concerts	O
caritatifs	PERSON
et	PERSON
aux	PERSON
événements	PERSON
d'	PERSON
avril	O
2002	O
,	O
aux	O
côtés	O
d'	O
artistes	O
divers	O
tels	O
que	O
Gérard	PERSON
Depardieu	PERSON
ou	O
encore	O
Zazie	O
.	O
Doc	PERSON
Gynéco	PERSON
a	O
sorti	O
en	O
2006	O
une	O
version	O
rap	O
de	O
L'	O
Homme	LOCATION
pressé	LOCATION
,	O
tandis	O
que	O
Métal	PERSON
Urbain	PERSON
en	O
proposait	O
une	O
version	O
punk	O
(	O
en	O
bonus	O
sur	O
l'	O
édition	O
limitée	O
de	O
l'	O
album	O
J'	O
irai	O
chier	O
dans	O
ton	PERSON
vomi	PERSON
)	O
.	O
