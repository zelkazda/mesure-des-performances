Au	O
sein	O
des	O
Beatles	O
,	O
avec	PERSON
son	PERSON
partenaire	O
John	PERSON
Lennon	PERSON
,	O
il	PERSON
forme	O
un	O
des	O
tandems	O
d'	O
auteurs-compositeurs	O
les	O
plus	O
influents	O
et	O
prolifiques	O
de	O
l'	O
histoire	O
du	O
rock	O
et	O
donne	O
naissance	O
à	O
plus	O
de	O
200	O
chansons	O
au	O
succès	O
considérable	O
.	O
Après	PERSON
la	PERSON
séparation	PERSON
du	O
groupe	O
en	O
1970	O
,	O
McCartney	O
lance	O
deux	O
albums	O
en	O
solo	O
(	O
McCartney	PERSON
et	O
Ram	O
)	O
,	O
puis	PERSON
forme	PERSON
le	O
groupe	O
Wings	O
avec	O
sa	O
femme	O
Linda	PERSON
McCartney	PERSON
et	O
un	O
ancien	O
membre	O
des	O
Moody	O
Blues	O
,	O
Denny	PERSON
Laine	PERSON
.	O
Par	O
la	O
suite	O
,	O
McCartney	O
poursuit	O
sa	O
carrière	O
en	O
solo	O
,	O
ininterrompue	PERSON
depuis	PERSON
,	O
tournant	O
dans	O
le	O
monde	O
entier	O
avec	O
son	O
groupe	O
en	O
interprétant	O
ses	O
plus	O
grands	O
succès	O
composés	PERSON
ou	O
co-composés	O
depuis	O
près	O
de	O
cinquante	O
ans	O
.	O
Il	O
a	O
écrit	O
un	O
important	O
catalogue	O
de	O
chansons	O
avec	O
les	O
Beatles	O
et	O
les	O
Wings	O
,	O
ainsi	O
qu'	O
en	O
tant	O
qu'	O
artiste	O
solo	O
,	O
et	O
a	O
pris	O
part	O
à	O
plusieurs	O
projets	O
caritatifs	O
.	O
Il	O
rencontre	O
John	O
Lennon	O
le	O
6	O
juillet	O
1957	O
à	O
une	O
fête	PERSON
paroissiale	PERSON
(	O
kermesse	O
)	O
,	O
par	O
l'	O
intermédiaire	O
de	O
son	O
ami	O
Ivan	PERSON
Vaughan	PERSON
.	O
Au	O
sein	O
des	O
Beatles	O
,	O
il	O
passe	O
à	O
la	O
basse	O
lorsque	O
le	O
premier	O
bassiste	O
Stuart	PERSON
Sutcliffe	PERSON
quitte	O
le	O
groupe	O
en	O
1961	O
.	O
Seulement	O
six	O
années	O
s'	O
écoulent	O
entre	O
les	O
enregistrements	O
de	O
leur	O
premier	O
(	O
Please	O
Please	O
Me	O
)	O
et	O
de	O
leur	O
dernier	O
(	O
Abbey	O
Road	O
)	O
disque	O
.	O
C'	O
est	O
Paul	PERSON
McCartney	PERSON
qui	PERSON
annonce	O
la	O
séparation	O
du	O
groupe	O
phare	O
des	O
années	O
1960	O
,	O
le	O
10	O
avril	O
1970	O
,	O
dans	O
un	O
communiqué	O
de	O
presse	O
inséré	O
dans	O
la	O
pochette	O
promotionnelle	PERSON
de	PERSON
son	PERSON
premier	O
disque	O
solo	O
.	O
Cette	O
annonce	O
prématurée	O
mettra	O
John	O
Lennon	O
dans	O
une	PERSON
colère	PERSON
noire	PERSON
,	O
lui	PERSON
qui	PERSON
est	PERSON
justement	O
à	O
l'	O
origine	O
de	O
la	O
séparation	O
du	O
groupe	O
,	O
contrairement	O
à	O
McCartney	O
.	O
Il	LOCATION
lui	LOCATION
arrive	O
de	O
composer	O
pour	O
d'	O
autres	O
artistes	O
et	O
de	O
les	O
produire	O
,	O
comme	O
c'	O
est	O
le	O
cas	O
pour	O
Mary	PERSON
Hopkin	PERSON
,	O
la	O
première	O
artiste	O
,	O
en	O
dehors	O
des	PERSON
Beatles	PERSON
,	O
à	O
signer	O
chez	O
Apple	O
.	O
Une	O
légende	O
urbaine	O
répandue	O
à	O
la	O
fin	O
des	O
années	O
1960	O
déclarait	O
que	O
Paul	PERSON
McCartney	PERSON
était	PERSON
mort	PERSON
en	O
1966	O
dans	O
un	O
accident	O
de	O
voiture	O
et	O
qu'	O
il	PERSON
avait	PERSON
été	PERSON
remplacé	O
par	O
un	O
sosie	O
.	O
Brian	PERSON
Epstein	PERSON
avait	PERSON
fait	PERSON
de	PERSON
son	PERSON
mieux	PERSON
pour	O
cacher	O
que	O
John	PERSON
Lennon	PERSON
était	PERSON
marié	PERSON
(	O
il	O
était	O
meilleur	O
pour	O
leur	O
image	O
auprès	PERSON
des	PERSON
jeunes	PERSON
groupies	PERSON
que	O
celles-ci	O
les	O
croient	O
célibataires	O
)	O
,	O
mais	O
l'	O
information	O
transpira	O
.	O
Après	PERSON
les	PERSON
Beatles	PERSON
,	O
il	O
publie	O
d'	O
abord	O
deux	PERSON
albums	PERSON
sous	O
son	O
propre	PERSON
nom	PERSON
.	O
Le	O
premier	O
,	O
intitulé	O
tout	O
simplement	O
McCartney	O
,	O
est	O
enregistré	O
dans	O
sa	O
ferme	O
en	O
Écosse	O
.	O
Cet	O
album	O
suscitera	O
ce	PERSON
commentaire	PERSON
élogieux	PERSON
:	O
"	O
Les	O
Beatles	O
sont	O
revenus	O
,	O
ils	O
s'	O
appellent	O
aujourd'hui	PERSON
Paul	PERSON
McCartney	PERSON
"	O
[	O
réf.	O
nécessaire	O
]	O
.	O
En	O
1973	O
,	O
Band	O
on	O
the	O
Run	LOCATION
,	O
le	O
troisième	O
album	O
du	O
groupe	O
,	O
lui	O
vaut	O
la	O
consécration	O
des	O
critiques	O
.	O
En	O
octobre	O
1979	O
,	O
le	O
Livre	O
Guinness	O
des	O
records	O
lui	O
remet	O
un	PERSON
disque	PERSON
de	PERSON
rhodium	PERSON
certifiant	PERSON
ses	PERSON
ventes	PERSON
records	O
de	PERSON
disques	PERSON
.	O
1979	O
est	O
,	O
par	O
ailleurs	O
,	O
l'	O
année	O
de	O
la	O
sortie	O
du	O
dernier	O
album	O
des	O
Wings	O
,	O
Back	O
to	O
the	O
Egg	O
,	O
qui	O
connaît	O
un	O
succès	O
moindre	O
que	O
les	O
albums	O
précédents	O
.	O
Le	O
groupe	O
Wings	O
se	O
dissout	O
en	O
mai	O
1981	O
.	O
Cette	O
chanson	O
est	O
reprise	O
,	O
bien	PERSON
des	PERSON
années	PERSON
plus	PERSON
tard	PERSON
,	O
en	O
2003	O
,	O
sur	O
son	O
album	O
live	O
Back	O
in	O
the	O
World	O
.	O
On	O
y	O
trouve	O
également	O
une	O
reprise	O
de	O
la	O
chanson	O
Something	O
de	O
George	O
Harrison	O
,	O
jouée	PERSON
sur	PERSON
un	PERSON
ukulélé	PERSON
,	O
un	O
instrument	O
que	O
George	PERSON
Harrison	PERSON
aimait	PERSON
beaucoup	PERSON
,	O
en	O
forme	O
d'	O
hommage	O
.	O
Il	PERSON
épouse	O
Heather	PERSON
Mills	PERSON
,	O
ancien	O
mannequin	O
qu'	O
il	O
a	O
rencontrée	O
lors	O
de	O
l'	O
organisation	O
d'	O
un	O
gala	O
pour	O
Handicap	O
International	O
,	O
et	O
elle-même	O
handicapée	O
d'	O
une	O
jambe	O
.	O
Paul	PERSON
McCartney	PERSON
consacre	O
par	O
ailleurs	O
une	O
grande	O
partie	O
des	O
années	O
1990	O
à	O
la	O
réalisation	O
du	O
projet	O
The	O
Beatles	O
Anthology	O
,	O
dans	O
lequel	PERSON
ceux-ci	PERSON
se	PERSON
racontent	O
,	O
et	O
qui	O
débouche	O
sur	O
la	O
publication	O
de	O
trois	O
doubles	O
albums	O
,	O
une	O
série	O
télévisée	PERSON
et	O
un	O
livre	O
entre	O
1994	O
et	O
2000	O
.	O
En	O
1997	O
,	O
il	O
a	O
été	O
anobli	O
par	O
la	O
reine	O
Elizabeth	PERSON
II	PERSON
d'	PERSON
Angleterre	PERSON
.	O
Le	O
25	O
mars	O
2003	O
,	O
au	O
début	O
de	O
son	O
concert	O
à	O
Bercy	O
pour	O
la	O
tournée	O
Back	O
in	O
the	O
World	O
,	O
une	O
partie	O
de	O
la	O
foule	O
entonna	O
Give	O
Peace	O
a	O
Chance	O
de	O
John	PERSON
Lennon	PERSON
pour	O
protester	O
contre	O
la	O
seconde	O
guerre	O
d'	O
Irak	PERSON
qui	PERSON
avait	PERSON
commencé	PERSON
trois	O
jours	O
plus	O
tôt	O
.	O
Paul	PERSON
McCartney	PERSON
se	O
contente	O
d'	O
applaudir	LOCATION
,	O
mais	PERSON
ne	PERSON
chante	PERSON
pas	PERSON
la	O
chanson	O
de	PERSON
John	PERSON
avec	PERSON
le	PERSON
public	O
parisien	O
.	O
Quoi	LOCATION
qu'	LOCATION
il	LOCATION
en	LOCATION
soit	LOCATION
,	O
il	O
a	O
déjà	O
chanté	O
cette	O
chanson	O
en	O
hommage	O
à	O
John	PERSON
Lennon	PERSON
lors	PERSON
d'	O
un	O
de	O
ses	O
concerts	O
à	O
Liverpool	LOCATION
.	O
Alors	O
qu'	O
avait	O
été	O
avancé	O
le	O
chiffre	O
de	O
400	O
millions	O
de	O
dollars	O
,	O
soit	LOCATION
le	LOCATION
quart	O
de	O
la	O
fortune	O
de	PERSON
McCartney	PERSON
,	O
son	O
ex-épouse	O
a	O
obtenu	O
56	O
millions	O
de	O
dollars	O
.	O
L'	O
éternel	O
jeune	PERSON
bassiste	PERSON
gaucher	PERSON
de	PERSON
Liverpool	PERSON
a	O
composé	O
un	O
nombre	O
impressionnant	O
de	O
succès	O
pop	O
.	O
Avec	PERSON
et	PERSON
sans	PERSON
les	PERSON
Beatles	PERSON
,	O
il	PERSON
a	O
cherché	O
également	O
à	O
explorer	O
des	O
formes	O
nouvelles	O
,	O
il	O
a	O
composé	O
notamment	O
deux	O
oratorios	O
classiques	O
.	O
En	O
juillet	O
2007	O
,	O
on	O
estimait	O
la	O
fortune	O
de	PERSON
Paul	PERSON
McCartney	PERSON
à	O
environ	O
1,6	O
milliard	O
de	O
dollars	O
.	O
Le	O
22	O
octobre	O
2007	O
,	O
Paul	PERSON
McCartney	PERSON
se	PERSON
produit	O
à	O
l'	O
Olympia	PERSON
pour	O
la	O
troisième	O
fois	O
de	O
sa	O
carrière	O
après	O
ses	O
passages	O
avec	O
les	O
Beatles	O
puis	O
les	O
Wings	O
.	O
Le	O
spectacle	O
se	O
clôture	O
par	O
le	O
classique	O
I	O
Saw	O
Her	O
Standing	O
There	O
.	O
Il	LOCATION
est	LOCATION
précédé	LOCATION
sur	LOCATION
scène	O
par	O
The	O
Zutons	O
et	O
les	O
Kaiser	O
Chiefs	O
.	O
En	O
fin	O
de	O
set	O
c'	O
est	O
à	O
John	PERSON
Lennon	PERSON
qu'	O
il	O
rend	O
hommage	O
en	O
reprenant	O
pour	O
la	O
première	O
fois	O
sur	O
scène	O
le	O
morceau	O
A	O
Day	O
in	O
the	O
Life	O
couplé	O
au	O
fameux	O
morceau	O
de	O
Lennon	O
Give	O
Peace	O
a	O
Chance	O
.	O
La	O
liste	O
de	O
titres	O
soumise	O
au	O
vote	O
couvre	O
l'	O
ensemble	O
de	PERSON
sa	PERSON
carrière	PERSON
,	O
des	PERSON
Beatles	PERSON
à	O
ses	O
titres	O
solo	O
en	O
passant	O
par	O
les	O
Wings	O
;	O
elle	LOCATION
a	O
également	O
la	O
formidable	O
particularité	O
de	O
présenter	O
des	O
titres	O
peu	PERSON
joués	PERSON
,	O
voire	O
jamais	O
joués	O
sur	O
scène	O
jusque	O
là	O
.	O
Suite	O
à	O
cette	O
soirée	O
,	O
Paul	PERSON
McCartney	PERSON
déclara	O
qu'	O
il	O
venait	O
de	O
vivre	O
l'	O
un	O
des	O
week-ends	O
les	O
plus	O
intenses	O
de	O
sa	O
carrière	O
[	O
réf.	O
nécessaire	O
]	O
.	O
Le	O
25	O
septembre	O
Paul	O
McCartney	O
s'	O
est	O
produit	O
à	O
Tel-Aviv	LOCATION
,	O
faisant	O
fi	O
des	O
menaces	O
de	O
mort	O
qui	O
pesaient	O
sur	O
lui	O
.	O
Il	O
a	O
notamment	O
rejoué	O
"	O
I	O
'm	O
Down	O
"	O
,	O
qu'	O
ils	O
avaient	O
joué	PERSON
en	O
1965	O
.	O
Il	O
se	O
produit	O
le	O
10	O
décembre	O
au	O
Palais	O
omnisport	O
de	O
Paris-Bercy	O
devant	O
18000	O
fans	O
.	O
Certains	O
pensent	O
que	O
cette	O
émission	O
est	O
en	O
dessous	O
de	O
la	O
renommée	O
de	O
Paul	O
McCartney	O
et	O
que	O
ce	O
passage	O
médiatique	O
serait	O
motivé	O
par	O
une	O
forte	O
somme	O
d'	O
argent	O
,	O
ou	O
pour	O
faire	O
un	O
coup	O
de	O
pub	O
...	O
Le	O
blason	O
est	O
d'	O
or	O
,	O
flanqué	O
de	O
sable	O
avec	O
un	O
fasce	O
d'	O
or	O
(	O
les	O
deux	O
flancs	O
ressemblant	O
ainsi	O
à	O
des	O
carapaces	O
de	O
scarabées	O
,	O
référence	O
aux	O
Beatles	O
)	O
.	O
