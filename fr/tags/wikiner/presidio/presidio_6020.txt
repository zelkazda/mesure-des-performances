Antoine	PERSON
Lavoisier	PERSON
,	O
né	PERSON
Antoine	PERSON
Laurent	PERSON
de	PERSON
Lavoisier	PERSON
le	PERSON
26	O
août	O
1743	O
à	O
Paris	LOCATION
et	O
guillotiné	O
le	O
8	O
mai	O
1794	O
à	O
Paris	LOCATION
,	O
est	O
un	PERSON
chimiste	PERSON
,	O
philosophe	O
et	O
économiste	O
français	O
.	O
Il	O
est	O
souvent	O
fait	O
référence	O
à	O
Lavoisier	O
en	O
tant	O
que	PERSON
père	PERSON
de	PERSON
la	PERSON
chimie	PERSON
moderne	O
.	O
Né	O
dans	O
une	O
famille	O
aisée	O
à	O
Paris	O
,	O
Antoine	PERSON
Laurent	PERSON
de	PERSON
Lavoisier	PERSON
hérita	PERSON
d'	O
une	O
grande	O
fortune	O
à	O
l'	O
âge	O
de	O
cinq	O
ans	O
après	O
le	O
décès	O
de	O
sa	O
mère	O
.	O
Il	O
fréquente	O
le	O
collège	O
des	O
Quatre-Nations	O
de	O
1754	O
à	O
1761	O
,	O
où	LOCATION
il	LOCATION
étudie	PERSON
la	PERSON
chimie	PERSON
,	O
la	PERSON
botanique	PERSON
,	O
l'	O
astronomie	O
et	O
les	O
mathématiques	O
.	O
En	O
1767	O
,	O
il	PERSON
travaille	PERSON
sur	PERSON
une	PERSON
étude	PERSON
géologique	PERSON
de	PERSON
l'	PERSON
Alsace	LOCATION
et	O
de	O
la	O
Lorraine	O
.	O
En	O
1771	O
,	O
il	PERSON
épouse	PERSON
Marie-Anne	PERSON
Pierrette	PERSON
Paulze	PERSON
,	O
alors	O
âgée	O
de	O
13	O
ans	O
.	O
Elle	O
réalise	O
de	O
nombreux	O
croquis	O
et	O
gravures	O
des	O
instruments	O
de	O
laboratoire	O
utilisés	O
par	O
Lavoisier	O
et	O
ses	O
collègues	O
.	O
Elle	PERSON
écrit	PERSON
et	O
publie	O
également	O
les	O
mémoires	O
de	O
Lavoisier	O
(	O
bien	O
qu'	O
aucune	O
version	O
anglaise	O
qui	O
aurait	O
subsisté	O
ne	O
soit	O
connue	O
à	O
ce	O
jour	O
)	O
et	O
fut	O
l'	O
hôte	O
de	O
soirées	O
pendant	O
lesquelles	O
d'	O
éminents	O
scientifiques	O
discutèrent	O
d'	O
idées	O
et	O
problèmes	O
relatifs	O
à	O
la	PERSON
chimie	PERSON
.	O
Son	PERSON
étude	PERSON
des	PERSON
lois	PERSON
est	PERSON
d'	PERSON
une	O
importance	O
capitale	O
dans	O
la	O
vie	O
de	O
Lavoisier	O
.	O
Dans	PERSON
son	PERSON
travail	O
pour	O
le	PERSON
gouvernement	PERSON
,	O
il	PERSON
a	O
participé	O
au	O
développement	O
du	O
système	O
métrique	O
pour	O
fixer	O
l'	O
uniformité	O
des	O
poids	O
et	O
des	O
mesures	O
dans	O
l'	O
ensemble	O
de	PERSON
la	PERSON
France	PERSON
.	O
Étant	O
l'	O
un	PERSON
des	PERSON
vingt-huit	PERSON
fermiers	O
généraux	O
,	O
Lavoisier	O
est	O
stigmatisé	O
comme	O
traître	O
par	O
les	O
révolutionnaires	O
en	O
1794	O
et	O
guillotiné	O
lors	O
de	O
la	O
Terreur	O
à	O
Paris	O
le	O
8	O
mai	O
1794	O
,	O
à	O
l'	O
âge	O
de	O
cinquante	O
ans	O
,	O
en	O
même	PERSON
temps	PERSON
que	PERSON
l'	O
ensemble	O
de	PERSON
ses	PERSON
collègues	PERSON
.	O
L'	O
une	O
des	O
plus	O
importantes	O
recherches	O
de	PERSON
Lavoisier	PERSON
a	O
été	O
de	O
redéterminer	O
la	O
nature	O
du	PERSON
phénomène	PERSON
de	PERSON
combustion	PERSON
.	O
L'	O
explication	O
de	O
Lavoisier	O
sur	O
la	O
combustion	O
remplace	O
la	O
théorie	PERSON
phlogistique	PERSON
,	O
qui	O
postule	O
que	O
les	O
matériaux	O
relâchent	O
une	O
substance	O
appelée	O
phlogiston	O
lorsqu'	O
ils	O
brûlent	O
.	O
Il	PERSON
découvre	PERSON
aussi	PERSON
que	PERSON
l'	O
air	O
inflammable	O
de	PERSON
Henry	PERSON
Cavendish	PERSON
,	O
qu'	LOCATION
il	PERSON
baptise	PERSON
hydrogène	PERSON
(	O
du	O
grec	O
"	O
formeur	O
d'	O
eau	O
"	O
)	O
,	O
réagit	O
avec	O
l'	O
oxygène	O
pour	O
former	O
une	O
rosée	O
,	O
qui	O
est	O
de	O
l'	O
eau	O
,	O
comme	O
l'	O
a	O
remarqué	O
Joseph	PERSON
Priestley	PERSON
.	O
Le	O
travail	O
de	O
Lavoisier	O
est	O
en	O
partie	O
basé	O
sur	O
celui	O
de	O
Priestley	O
.	O
Les	PERSON
expériences	PERSON
de	PERSON
Lavoisier	PERSON
sont	PERSON
parmi	PERSON
les	PERSON
premières	O
expériences	O
chimiques	O
véritablement	O
quantitatives	O
jamais	O
exécutées	O
.	O
Lavoisier	O
a	O
aussi	O
étudié	O
la	O
composition	O
de	O
l'	O
eau	O
,	O
et	O
il	O
appelle	O
ses	O
composants	O
"	O
oxygène	O
"	O
et	O
"	O
hydrogène	O
"	O
.	O
Avec	PERSON
le	PERSON
chimiste	PERSON
Claude	PERSON
Louis	PERSON
Berthollet	PERSON
et	PERSON
d'	PERSON
autres	PERSON
,	O
Lavoisier	PERSON
conçoit	PERSON
une	PERSON
nomenclature	O
chimique	O
ou	O
un	O
système	O
des	O
noms	O
qui	O
sert	O
de	O
base	O
au	O
système	O
moderne	O
.	O
En	O
outre	O
,	O
Lavoisier	O
clarifie	O
le	O
concept	O
d'	O
un	O
élément	O
comme	O
substance	O
simple	O
qui	PERSON
ne	PERSON
peut	PERSON
être	PERSON
décomposée	PERSON
par	PERSON
aucune	PERSON
méthode	PERSON
connue	PERSON
d'	O
analyse	O
chimique	O
,	O
et	O
conçoit	O
une	O
théorie	O
de	O
la	O
formation	O
des	PERSON
composés	PERSON
chimiques	PERSON
des	O
éléments	O
.	O
Les	O
contributions	O
fondamentales	O
de	O
Lavoisier	O
à	O
la	O
chimie	O
sont	O
le	O
résultat	O
d'	O
un	O
effort	O
conscient	O
d'	O
adapter	O
toutes	O
les	O
expériences	O
dans	O
le	O
cadre	O
d'	O
une	O
théorie	O
simple	O
.	O
Les	PERSON
trois	PERSON
ou	PERSON
quatre	PERSON
éléments	PERSON
de	PERSON
la	PERSON
chimie	PERSON
classique	PERSON
ont	PERSON
conduit	PERSON
au	PERSON
développement	O
du	O
système	O
moderne	O
,	O
et	O
Lavoisier	O
a	O
traduit	O
des	O
réactions	O
dans	O
les	O
équations	O
chimiques	O
qui	O
respectaient	O
la	O
loi	O
de	O
conservation	O
des	O
masses	O
.	O
La	O
maxime	O
"	O
Rien	PERSON
ne	PERSON
se	PERSON
perd	PERSON
,	O
rien	PERSON
ne	PERSON
se	PERSON
crée	PERSON
,	O
tout	O
se	O
transforme	O
"	O
attribuée	O
à	O
Lavoisier	O
,	O
est	O
simplement	O
la	O
reformulation	O
d'	O
une	O
phrase	O
d'	O
Anaxagore	PERSON
de	PERSON
Clazomènes	PERSON
:	O
"	O
Rien	O
ne	O
naît	PERSON
ni	PERSON
ne	PERSON
périt	LOCATION
,	O
mais	LOCATION
des	LOCATION
choses	O
déjà	O
existantes	O
se	PERSON
combinent	PERSON
,	O
puis	PERSON
se	PERSON
séparent	PERSON
de	PERSON
nouveau	PERSON
"	O
.	O
Au	O
bout	O
de	O
dix	O
ans	O
,	O
Lavoisier	PERSON
rédige	PERSON
un	PERSON
compte-rendu	PERSON
de	PERSON
ses	PERSON
recherches	PERSON
à	O
la	O
Société	O
royale	O
d'	O
agriculture	O
et	O
déclare	O
qu'	O
il	PERSON
lui	PERSON
faudra	PERSON
encore	PERSON
une	PERSON
décennie	PERSON
pour	O
confirmer	O
ses	O
résultats	O
.	O
Vers	O
la	O
fin	O
de	O
sa	O
vie	O
il	O
est	O
confronté	O
à	O
la	O
théorie	O
de	O
l'	O
humus	O
soutenue	O
par	O
Jean	PERSON
Henri	PERSON
Hassenfratz	PERSON
.	O
Cette	PERSON
théorie	PERSON
,	O
qui	O
postule	O
que	O
seul	O
l'	O
humus	PERSON
est	O
capable	O
de	O
nourrir	O
les	O
végétaux	O
,	O
est	O
fausse	O
et	O
prévalut	O
jusqu'	O
aux	O
travaux	O
de	O
Justus	O
von	PERSON
Liebig	PERSON
en	O
1840	O
.	O
Dans	LOCATION
ce	LOCATION
programme	O
,	O
Lavoisier	PERSON
décrit	PERSON
le	PERSON
cycle	PERSON
des	PERSON
composants	PERSON
de	PERSON
la	PERSON
matière	PERSON
à	PERSON
la	PERSON
surface	O
de	PERSON
la	PERSON
terre	PERSON
(	O
le	O
cycle	O
réduction	O
--	O
oxydation	O
)	O
et	O
oppose	O
la	O
"	O
végétalisation	O
"	O
(	O
la	PERSON
photosynthèse	PERSON
)	O
à	O
la	O
combustion	O
et	O
aux	O
fermentations	O
.	O
Les	PERSON
contemporains	PERSON
de	PERSON
Lavoisier	PERSON
étaient	PERSON
convaincus	PERSON
que	PERSON
la	PERSON
matière	PERSON
était	PERSON
composée	O
des	O
quatre	O
éléments	O
fondamentaux	O
:	O
la	O
terre	O
,	O
l'	O
air	O
,	O
l'	O
eau	O
et	O
le	O
feu	O
et	O
la	O
théorie	O
du	O
phlogistique	O
était	O
aussi	O
très	O
vraisemblable	O
à	O
cette	O
époque	O
.	O
Avec	O
de	O
réelles	O
qualités	O
scientifiques	O
,	O
des	O
instruments	O
et	O
une	O
méthode	O
de	O
travail	O
précise	O
,	O
Lavoisier	PERSON
installa	PERSON
la	PERSON
chimie	PERSON
sur	O
une	O
base	O
nouvelle	O
qui	O
conduira	O
à	O
la	O
chimie	O
moderne	O
.	O
