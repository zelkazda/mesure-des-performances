Il	O
s'	O
agit	O
d'	O
OXO	O
,	O
un	O
morpion	O
dont	O
l'	O
un	O
des	O
joueurs	O
est	O
contrôlé	O
par	O
l'	O
ordinateur	O
.	O
En	O
1961	O
,	O
un	O
groupe	O
d'	O
étudiants	O
du	O
MIT	O
mené	PERSON
par	PERSON
Steve	PERSON
Russell	PERSON
programme	O
un	O
jeu	O
nommé	O
Spacewar	O
sur	O
un	O
PDP-1	O
,	O
le	O
premier	O
mini-ordinateur	O
de	O
la	O
société	O
DEC	O
.	O
Le	O
but	O
n'	O
était	O
pas	O
de	O
créer	O
un	O
jeu	O
en	O
lui-même	O
,	O
mais	O
d'	O
expérimenter	O
les	O
possibilités	O
du	O
nouvel	O
ordinateur.	O
[	O
réf.	O
nécessaire	O
]	O
Spacewar	O
met	O
en	O
scène	O
deux	O
vaisseaux	O
spatiaux	O
,	O
chacun	O
contrôlé	O
par	O
un	PERSON
joueur	PERSON
à	PERSON
l'	O
aide	O
de	PERSON
manettes	PERSON
créées	PERSON
pour	O
l'	O
occasion	O
,	O
capables	O
de	O
tirer	O
des	O
missiles	O
;	O
au	O
centre	O
de	O
l'	O
écran	O
,	O
une	O
étoile	O
crée	O
un	O
champ	O
gravitationnel	O
et	O
un	O
danger	O
supplémentaire	O
.	O
Ce	PERSON
jeu	PERSON
,	O
à	O
la	O
différence	O
des	O
précédentes	O
tentatives	O
,	O
fut	O
rapidement	O
distribué	O
avec	O
les	O
nouveaux	O
ordinateurs	O
de	O
DEC	O
et	O
s'	O
échangea	O
à	O
travers	O
les	O
prémices	O
d'	O
Internet	O
.	O
Bien	PERSON
que	PERSON
le	PERSON
jeu	PERSON
ne	PERSON
fût	O
jamais	PERSON
commercialisé	PERSON
(	O
il	O
coûtait	O
d'	O
ailleurs	O
75	O
$	O
par	O
utilisation	O
)	O
et	O
qu'	O
AT	O
&	O
T	O
en	O
ait	O
finalement	O
stoppé	O
le	O
financement	O
en	O
1969	O
,	O
il	LOCATION
est	LOCATION
à	LOCATION
l'	O
origine	O
de	O
la	O
création	O
d'	O
UNIX	O
.	O
En	O
1972	O
,	O
Nolan	PERSON
Bushnell	PERSON
crée	O
l'	O
entreprise	O
Atari	O
et	O
développe	O
la	O
même	O
année	O
le	O
premier	O
jeu	O
vidéo	O
à	O
grand	O
succès	O
:	O
Pong	O
.	O
Atari	O
vend	O
19	O
000	O
machines	O
,	O
débutant	O
ainsi	O
l'	O
engouement	O
du	O
public	O
pour	O
les	O
bornes	O
d'	O
arcade	O
,	O
et	O
est	O
bientôt	PERSON
imité	PERSON
par	PERSON
plusieurs	O
concurrents	O
.	O
C'	O
est	O
également	O
en	O
1972	O
qu'	O
est	O
commercialisée	O
la	O
première	O
console	O
de	O
jeu	O
pour	O
particuliers	O
:	O
la	O
Magnavox	PERSON
Odyssey	PERSON
,	O
basée	LOCATION
sur	LOCATION
les	O
travaux	O
de	O
Ralph	O
Baer	O
de	O
1968	O
.	O
En	O
1979	O
,	O
la	O
société	O
Activision	O
est	O
créée	O
par	O
un	O
groupe	O
de	O
programmeurs	O
d'	O
Atari	O
mécontents	O
.	O
C'	O
est	O
aussi	O
l'	O
année	LOCATION
où	LOCATION
arrive	O
Space	O
Invaders	O
en	O
Europe	LOCATION
.	O
Au	O
Japon	O
,	O
la	O
première	O
polémique	O
autour	O
de	O
l'	O
abrutissement	O
provoqué	O
par	O
les	O
jeux	O
vidéo	O
prend	O
place	O
autour	O
de	LOCATION
ce	LOCATION
jeu	LOCATION
.	LOCATION
C'	O
est	O
également	O
en	O
1978	O
qu'	LOCATION
apparaît	O
aux	O
États-Unis	O
la	O
Magnavox	O
Odyssey²	O
qui	O
succède	O
à	O
l'	O
Odyssey	O
et	O
qui	O
sort	O
également	O
en	O
Europe	LOCATION
sous	O
la	O
marque	O
Philips	O
,	O
sous	O
un	PERSON
autre	PERSON
nom	PERSON
:	O
Videopac	PERSON
G7000	PERSON
.	O
L'	O
industrie	O
du	O
jeu	O
d'	O
arcade	O
entre	O
dans	O
un	O
âge	O
d'	O
or	O
en	O
1978	O
avec	O
la	O
sortie	O
de	O
Space	O
Invaders	O
de	O
Taito	O
.	O
En	O
1979	O
,	O
Atari	O
publie	O
Asteroids	O
,	O
son	O
plus	O
grand	O
succès	O
commercial	O
.	O
Richard	PERSON
Garriott	PERSON
,	O
par	O
exemple	O
,	O
distribua	PERSON
de	PERSON
cette	PERSON
façon	PERSON
plusieurs	PERSON
exemplaires	PERSON
d	PERSON
'	PERSON
Akalabeth	O
,	O
l'	O
ancêtre	O
de	O
la	O
série	O
Ultima	O
,	O
avant	O
que	O
le	O
jeu	O
ne	O
soit	O
publié	O
.	O
À	O
partir	O
du	O
moment	O
où	O
le	O
marché	O
des	O
consoles	O
de	O
jeu	O
redémarrera	O
,	O
grâce	O
à	O
Nintendo	O
en	O
1985	O
,	O
une	O
distinction	O
sera	O
systématiquement	O
effectuée	O
entre	O
jeux	O
sur	O
console	O
et	O
jeux	O
sur	O
ordinateur	O
.	O
Ils	O
enchaîneront	O
les	O
séries	O
à	O
succès	O
comme	O
King	O
's	O
Quest	O
,	O
Space	O
Quest	O
,	O
Leisure	O
Suit	O
Larry	PERSON
,	O
etc	O
.	O
Les	PERSON
jeux	PERSON
vidéo	PERSON
s'	PERSON
adaptent	O
à	O
ce	O
nouveau	O
support	O
(	O
Apple	O
II	O
,	O
ZX80	O
,	O
Commodore	O
64	O
,	O
MSX	O
,	O
Amstrad	O
CPC	O
,	O
etc.	O
)	O
.	O
À	O
partir	O
de	O
1985	O
,	O
l'	O
Amiga	PERSON
et	O
l'	O
Atari	O
ST	O
,	O
techniquement	O
plus	O
évolués	O
,	O
renouvelleront	PERSON
le	PERSON
jeu	PERSON
vidéo	PERSON
sur	PERSON
ordinateur	O
.	O
En	O
1983	O
,	O
Nintendo	O
,	O
société	O
japonaise	O
qui	O
s'	O
est	O
fait	O
un	PERSON
nom	PERSON
dans	PERSON
les	O
cartes	O
à	O
jouer	O
et	O
le	O
domaine	O
des	PERSON
jeux	PERSON
,	O
sort	O
la	O
Famicom	O
,	O
console	O
de	O
jeux	O
individuelle	O
.	O
En	O
effet	O
,	O
la	O
Famicom	O
,	O
devenue	O
NES	O
aux	O
États-Unis	O
et	O
en	O
Europe	LOCATION
est	O
pratiquement	O
seule	O
à	O
chaque	O
lancement	O
et	O
bénéficie	O
de	O
l'	O
appui	O
massif	O
des	O
éditeurs	O
tiers	O
.	O
les	O
icônes	O
Mario	O
,	O
Donkey	O
Kong	O
,	O
Metroid	O
,	O
The	O
Legend	O
of	O
Zelda	O
tirent	O
la	O
machine	O
vers	O
le	PERSON
haut	PERSON
.	PERSON
La	O
NES	O
se	O
vendra	O
à	O
plus	O
de	O
60	O
millions	O
d'	O
exemplaire	O
jusqu'	O
en	O
1990	O
.	O
Début	O
1990	O
,	O
le	O
marché	O
consoles	O
est	O
encore	O
dominé	O
par	O
Nintendo	O
et	O
sa	O
Famicom	O
,	O
opposée	O
à	O
la	O
Master	O
System	O
de	O
Sega	O
.	O
Mais	PERSON
Nintendo	PERSON
possède	O
encore	O
le	O
soutien	O
des	O
éditeurs	O
.	O
En	O
1990	O
,	O
les	O
ventes	O
de	O
Nintendo	O
s'	O
envolent	O
grâce	O
à	O
Super	O
Mario	O
Bros.	O
3	O
.	O
Cet	PERSON
emblème	PERSON
de	PERSON
la	PERSON
remontée	PERSON
des	PERSON
ventes	PERSON
de	PERSON
jeux	PERSON
vidéo	PERSON
encourage	O
SNK	O
à	O
sortir	O
les	O
deux	O
versions	O
de	O
la	O
Neo-Geo	O
(	O
Neo-Geo	O
AES	O
,	O
Neo-Geo	O
MVS	O
)	O
,	O
console	O
et	O
arcade	O
.	O
Pour	O
contrer	O
Nintendo	O
,	O
Sega	O
développe	O
la	O
Mega	LOCATION
Drive	LOCATION
,	O
une	O
console	O
très	PERSON
puissante	PERSON
pour	O
l'	O
époque	O
,	O
et	O
crée	O
son	O
icône	PERSON
,	O
Sonic	LOCATION
.	LOCATION
En	O
1991	O
,	O
Nintendo	O
sort	O
la	O
Super-NES	O
.	O
Sa	PERSON
sortie	PERSON
tardive	PERSON
est	PERSON
compensée	O
par	O
la	O
tactique	O
de	O
Nintendo	O
:	O
seuls	PERSON
les	PERSON
jeux	PERSON
font	O
vendre	O
la	O
console	O
.	O
Super	O
Mario	O
Kart	O
,	O
Final	O
Fantasy	O
VI	O
,	O
Super	O
Metroid	O
,	O
Super	O
Mario	O
World	O
vont	O
pousser	O
les	O
ventes	O
de	O
la	O
console	O
grâce	O
à	O
des	O
graphismes	O
supérieures	O
à	O
la	O
Genesis	O
.	O
Ceux-ci	PERSON
ne	PERSON
feront	O
qu'	O
augmenter	O
durant	O
la	O
vie	O
de	O
la	O
console	O
pour	O
atteindre	O
des	O
sommets	O
en	O
1994	O
avec	O
Donkey	O
Kong	O
Country	O
de	O
Rare	O
,	O
filiale	O
de	O
Nintendo	O
.	O
En	O
1992	O
,	O
id	O
Software	O
sort	O
Wolfenstein	PERSON
3D	O
,	O
un	O
jeu	O
d'	O
action	O
vu	O
entièrement	O
par	O
les	O
yeux	O
du	O
héros	O
que	O
l'	O
on	O
incarne	O
.	O
Cette	PERSON
immersivité	PERSON
sera	PERSON
réellement	PERSON
exploitée	PERSON
un	PERSON
an	O
plus	O
tard	O
,	O
dans	O
Doom	O
(	O
toujours	O
d'	O
id	O
Software	O
)	O
,	O
où	O
l'	O
aspect	O
des	O
monstres	O
,	O
l'	O
ambiance	O
sonore	O
et	O
le	PERSON
jeu	PERSON
des	PERSON
lumières	PERSON
davantage	O
travaillés	O
,	O
contribueront	LOCATION
à	O
créer	O
une	O
atmosphère	O
particulièrement	O
angoissante	O
.	O
Doom	PERSON
sera	PERSON
suivi	PERSON
d'	PERSON
une	O
foultitude	PERSON
de	PERSON
jeux	PERSON
de	PERSON
tir	PERSON
subjectif	PERSON
se	PERSON
rapprochant	O
du	PERSON
concept	PERSON
avec	PERSON
plus	PERSON
ou	PERSON
moins	PERSON
de	PERSON
succès	PERSON
,	O
dont	O
les	O
plus	O
célèbres	O
et	O
réussis	O
restent	O
entre	O
autres	O
:	O
la	O
série	O
des	O
Duke	O
Nukem	O
,	O
Blood	O
,	O
Quake	O
,	O
Unreal	O
,	O
Half-Life	O
(	O
dont	O
l'	O
une	O
des	O
modifications	O
,	O
Counter-Strike	O
,	O
est	O
le	O
jeu	O
réseau	O
le	O
plus	O
populaire	O
[	O
réf.	O
nécessaire	O
]	O
)	O
.	O
En	O
1993	O
,	O
Westwood	O
Studios	O
sort	O
Dune	O
II	O
,	O
considéré	O
comme	LOCATION
le	LOCATION
premier	O
jeu	O
de	O
stratégie	O
en	O
temps	O
réel	O
moderne	O
,	O
car	O
introduisant	O
des	O
concepts	O
originaux	O
encore	O
d'	O
actualité	O
.	O
Le	LOCATION
but	O
du	PERSON
jeu	PERSON
est	PERSON
de	PERSON
devenir	PERSON
le	PERSON
maître	PERSON
d'	PERSON
Arrakis	PERSON
,	O
la	O
seule	O
planète	PERSON
produisant	PERSON
de	PERSON
l'	PERSON
épice	LOCATION
dans	O
la	PERSON
galaxie	PERSON
;	O
l'	O
ambiance	O
et	O
l'	O
histoire	O
sont	O
tirées	O
du	O
livre	O
Dune	O
,	O
roman	O
de	O
science	O
fiction	O
écrit	O
par	O
Frank	PERSON
Herbert	PERSON
.	O
Les	PERSON
mêmes	PERSON
personnes	PERSON
récidiveront	PERSON
plus	O
tard	O
avec	O
la	O
série	O
des	O
jeux	O
Command	O
&	O
Conquer	O
.	O
Dans	LOCATION
le	LOCATION
même	LOCATION
genre	O
,	O
en	O
1995	O
,	O
un	O
nouveau	O
venu	O
,	O
Blizzard	O
Entertainment	O
,	O
sort	O
Warcraft	O
:	O
Orcs	O
&	O
Humans	O
,	O
plongeant	LOCATION
le	LOCATION
joueur	LOCATION
dans	LOCATION
un	O
monde	O
fantastique	O
où	O
s'	O
affrontent	O
les	O
orcs	O
et	O
les	O
humains	O
.	O
Deux	PERSON
suites	O
ainsi	O
qu'	O
une	O
autre	O
série	O
de	O
stratégie	O
,	O
StarCraft	O
,	O
suivront	LOCATION
,	O
toujours	O
avec	O
énormément	O
de	O
succès	O
.	O
Sony	O
,	O
ne	PERSON
voulant	PERSON
pas	PERSON
d'	PERSON
un	O
investissement	O
pour	O
rien	O
,	O
décide	O
de	O
continuer	O
le	O
travail	O
seul	O
,	O
pour	O
développer	O
sa	O
propre	O
console	O
.	O
Elle	PERSON
deviendra	PERSON
simplement	O
la	O
PlayStation	O
qui	O
sort	O
en	O
1994	O
.	O
Toutefois	PERSON
des	PERSON
jeux	PERSON
comme	O
Super	O
Mario	O
64	O
,	O
GoldenEye	O
007	O
ou	O
The	O
Legend	O
of	O
Zelda	O
:	O
Ocarina	O
of	O
time	O
vont	O
marquer	O
les	O
joueurs	O
.	O
Sorti	PERSON
en	O
décembre	O
au	O
Japon	O
,	O
le	PERSON
jeu	PERSON
de	PERSON
course	O
automobile	O
Gran	O
Turismo	O
impose	O
de	O
nouveaux	O
standards	O
en	O
matière	O
de	O
rendu	O
visuel	O
,	O
de	PERSON
physique	PERSON
et	PERSON
de	PERSON
contenu	O
.	O
Il	LOCATION
reste	LOCATION
le	LOCATION
jeu	LOCATION
le	LOCATION
plus	O
vendu	O
sur	O
PlayStation	O
.	O
La	O
série	O
de	O
Polyphony	O
Digital	O
devient	O
la	O
nouvelle	O
référence	O
du	O
genre	O
sur	O
console	O
.	O
En	O
1998	O
,	O
LucasArts	O
renouvelle	O
le	O
genre	O
en	O
produisant	O
Grim	O
Fandango	O
,	O
un	O
jeu	O
d'	O
aventure	O
où	O
l'	O
affichage	O
des	O
lieux	O
et	O
des	O
personnages	O
est	O
semblable	O
à	O
Alone	O
in	O
the	O
Dark	O
.	O
Cette	PERSON
même	PERSON
année	PERSON
,	O
on	O
retrouve	O
Blizzard	O
Entertainment	O
qui	O
produit	O
coup	O
sur	LOCATION
coup	O
deux	O
jeux	O
qui	O
vont	O
rester	O
dans	O
l'	O
histoire	O
:	O
Diablo	O
et	O
StarCraft	O
.	O
Diablo	O
est	O
un	PERSON
jeu	PERSON
de	PERSON
type	O
hack'n'slash	O
,	O
mélange	O
de	O
jeu	O
de	O
rôle	O
,	O
d'	O
aventure	O
et	O
d	O
'	O
heroic	O
fantasy	O
où	O
le	O
joueur	O
contrôle	O
un	O
personnage	O
dont	O
il	PERSON
peut	PERSON
faire	PERSON
évoluer	PERSON
les	O
compétences	O
au	O
combat	O
ou	O
en	O
sorcellerie	PERSON
ainsi	PERSON
que	PERSON
son	PERSON
équipement	PERSON
au	PERSON
fur	O
et	O
à	O
mesure	O
de	O
combats	O
remportés	PERSON
et	PERSON
des	PERSON
objets	O
amassés	O
.	O
Metal	O
Gear	O
Solid	O
invente	O
le	O
jeu	O
d'	O
infiltration	O
.	O
Le	PERSON
titre	PERSON
de	PERSON
Konami	PERSON
allie	O
un	O
système	O
de	O
jeu	O
novateur	O
à	O
une	O
histoire	O
riche	O
en	O
rebondissements	O
distillée	O
à	O
travers	O
de	O
longues	O
cinématiques	LOCATION
.	O
Son	PERSON
créateur	PERSON
,	O
Hideo	PERSON
Kojima	PERSON
,	O
devient	O
une	O
figure	O
populaire	O
chez	O
la	O
communauté	O
de	O
joueurs	O
.	O
Cette	PERSON
même	PERSON
année	PERSON
,	O
la	O
Dreamcast	O
de	O
Sega	O
est	O
commercialisée	O
au	O
Japon	O
.	O
Première	O
"	O
128	O
bits	O
"	O
à	O
voir	O
le	O
jour	O
,	O
c'	O
est	O
aussi	PERSON
la	PERSON
dernière	PERSON
console	PERSON
produite	O
par	O
Sega	O
.	O
Elle	PERSON
proposera	PERSON
quelques	PERSON
titres	PERSON
mémorables	PERSON
comme	PERSON
Soul	O
Calibur	O
,	O
Ikaruga	O
,	O
ou	O
Shenmue	PERSON
avant	O
de	O
disparaître	O
en	O
2002	O
.	O
L'	O
année	O
1998	O
est	O
aussi	PERSON
la	PERSON
date	O
de	PERSON
sortie	PERSON
de	PERSON
Banjo-Kazooie	PERSON
sur	O
Nintendo	O
64	O
:	O
un	PERSON
jeu	PERSON
de	PERSON
plate-forme	PERSON
humoristique	O
développé	O
par	O
Rare	O
.	O
Ce	O
dernier	O
va	O
s'	O
affirmer	O
comme	O
étant	O
le	PERSON
seul	PERSON
jeu	PERSON
capable	O
de	PERSON
rivaliser	PERSON
avec	PERSON
Super	PERSON
Mario	O
64	O
.	O
Sa	PERSON
suite	PERSON
sortira	PERSON
en	O
2001	O
sous	O
le	O
nom	O
de	O
Banjo-Tooie	O
.	O
En	O
2000	O
,	O
Sony	O
lance	O
la	O
PlayStation	O
2	O
.	O
En	O
octobre	O
2001	O
,	O
la	O
sortie	PERSON
de	PERSON
GTA3	PERSON
fait	O
sensation	O
.	O
