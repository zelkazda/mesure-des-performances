En	O
effet	O
,	O
le	LOCATION
village	LOCATION
se	O
trouve	O
à	O
l'	O
ouest	O
de	PERSON
Thèbes	PERSON
,	O
sur	LOCATION
la	LOCATION
rive	LOCATION
opposée	O
du	O
Nil	O
.	O
On	O
leur	O
doit	O
également	O
le	O
temple	O
monumental	O
d'	O
Hatchepsout	O
sur	O
le	O
site	O
de	O
Deir	O
el-Bahari	O
.	O
Le	O
village	O
fut	O
abandonné	O
,	O
puis	PERSON
pillé	PERSON
,	O
durant	O
la	O
Troisième	PERSON
période	PERSON
intermédiaire	PERSON
qui	O
débuta	O
à	O
la	O
fin	O
du	O
règne	O
de	O
Ramsès	O
XI	O
.	O
Un	O
temple	O
de	O
construction	O
ptolémaïque	O
y	O
fut	O
édifié	O
par	O
Ptolémée	O
IV	O
pour	O
les	O
déesses	O
Hathor	O
et	O
Maât	O
.	O
La	LOCATION
cité	LOCATION
se	LOCATION
développa	O
jusqu'	PERSON
à	O
compter	O
sous	O
Ramsès	PERSON
IV	PERSON
quelque	PERSON
1200	PERSON
artisans	O
nourris	O
par	O
une	O
noria	O
de	O
pêcheurs	O
,	O
cultivateurs	O
et	O
porteurs	O
d'	O
eau	O
.	O
D'	O
époque	O
ptolémaïque	O
,	O
le	O
petit	O
temple	O
de	LOCATION
Deir	LOCATION
el-Médineh	LOCATION
(	O
neuf	O
mètres	O
de	O
large	O
sur	O
vingt-deux	O
mètres	O
de	O
long	O
)	O
comporte	O
trois	PERSON
sanctuaires	PERSON
juxtaposés	PERSON
précédés	PERSON
d'	O
un	O
vestibule	O
soutenu	O
par	O
deux	O
colonnes	O
à	O
chapiteau	PERSON
hathorique	PERSON
.	PERSON
Outre	O
le	O
temple	O
de	O
Deir	O
el-Médineh	O
,	O
le	O
site	O
est	O
parsemé	O
de	O
fondations	O
d'	O
autres	O
temples	O
plus	O
anciens	O
,	O
notamment	O
le	O
petit	O
temple	O
d'	O
Amenhotep	O
I	O
er	O
et	O
la	O
chapelle	O
d'	O
Hathor	O
construite	O
par	O
Séthi	O
I	O
er	O
alors	O
que	O
d'	O
autres	O
éléments	O
remontent	O
à	O
Ramsès	O
II	O
.	O
À	O
partir	O
de	O
1810	O
,	O
les	O
voleurs	O
pillent	O
Deir	LOCATION
el-Médineh	LOCATION
,	O
dont	O
les	O
nombreuses	O
tombes	O
et	O
les	O
maisons	O
étaient	O
encore	O
en	O
excellent	O
état	LOCATION
.	O
Heureusement	O
,	O
Auguste	PERSON
Mariette	PERSON
met	O
fin	O
à	O
ce	O
pillage	O
sauvage	O
dans	O
les	O
années	O
1850	O
.	O
Mais	PERSON
le	PERSON
véritable	O
explorateur	O
du	O
site	O
est	O
sans	O
conteste	O
Bernard	PERSON
Bruyère	PERSON
qui	PERSON
y	PERSON
consacra	PERSON
près	PERSON
de	PERSON
vingt-cinq	PERSON
ans	PERSON
de	PERSON
sa	PERSON
vie	PERSON
.	O
Il	O
y	O
entreprend	O
l'	O
exploration	O
systématique	O
et	O
méthodique	O
entre	O
1917	O
et	O
1947	O
ainsi	O
que	O
l	O
'	O
égyptologue	PERSON
tchèque	PERSON
Jaroslav	PERSON
Černý	PERSON
.	O
En	O
1934/1935	O
,	O
Bernard	PERSON
Bruyère	PERSON
y	PERSON
découvre	PERSON
la	PERSON
tombe	PERSON
de	PERSON
la	PERSON
dame	O
Madja	PERSON
et	O
de	O
son	O
époux	O
,	O
un	LOCATION
ouvrier	LOCATION
du	LOCATION
village	O
des	O
artisans	O
.	O
