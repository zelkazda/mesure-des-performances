La	O
Tchécoslovaquie	O
fut	O
un	O
pays	O
d'	O
Europe	LOCATION
centrale	O
du	O
28	O
octobre	O
1918	O
au	O
31	O
décembre	O
1992	O
(	O
à	O
l'	O
exception	O
de	PERSON
la	PERSON
période	PERSON
1939-1945	O
)	O
.	O
Une	O
timide	O
libéralisation	O
en	O
1968	O
,	O
appelée	O
Printemps	O
de	O
Prague	O
,	O
entraînera	O
une	O
intervention	O
des	O
forces	O
du	O
Pacte	O
de	O
Varsovie	O
qui	O
refermera	O
le	PERSON
pays	O
pour	O
20	O
ans	O
.	O
Profitant	PERSON
de	PERSON
la	PERSON
politique	PERSON
de	PERSON
tolérance	PERSON
de	PERSON
l'	PERSON
URSS	O
mise	O
en	O
place	O
par	O
Gorbatchev	LOCATION
,	O
le	LOCATION
pays	O
retrouve	PERSON
sa	PERSON
liberté	PERSON
en	O
1989	O
grâce	O
à	O
la	O
Révolution	O
de	O
velours	O
et	O
porte	O
à	O
sa	O
tête	O
le	O
dramaturge	O
et	O
dissident	O
Václav	PERSON
Havel	PERSON
.	O
Celui-ci	PERSON
ne	PERSON
pourra	PERSON
empêcher	PERSON
les	PERSON
susceptibilités	PERSON
nationales	O
encouragées	LOCATION
par	O
des	O
dirigeants	O
politiques	O
populistes	O
de	PERSON
causer	PERSON
la	PERSON
séparation	PERSON
à	O
l'	O
amiable	O
du	PERSON
pays	O
en	O
1993	O
pour	O
la	O
République	O
tchèque	O
et	O
la	O
Slovaquie	O
,	O
surnommée	PERSON
la	O
"	O
partition	O
de	O
velours	O
"	O
.	O
Dans	O
sa	O
courte	O
existence	O
(	O
moins	O
d'	O
un	O
siècle	O
)	O
,	O
la	O
Tchécoslovaquie	PERSON
connait	PERSON
de	PERSON
nombreux	PERSON
régimes	O
politiques	O
et	O
réformes	O
institutionnelles	O
.	O
L'	O
indépendance	O
de	O
la	O
Tchécoslovaquie	O
est	O
proclamée	O
le	O
28	O
octobre	O
1918	O
dans	O
la	O
petite	O
ville	O
de	O
Darney	O
(	O
Vosges	LOCATION
)	O
et	O
entérinée	O
par	O
le	O
traité	O
de	O
Saint-Germain-en-Laye	O
moins	O
d'	O
un	O
an	O
plus	O
tard	O
.	O
Elle	PERSON
se	PERSON
termine	O
avec	O
les	O
Accords	O
de	O
Munich	O
(	O
septembre	O
1938	O
)	O
et	O
le	O
départ	O
en	O
exil	O
de	O
son	O
président	O
,	O
Edvard	PERSON
Beneš	PERSON
(	O
5	O
octobre	O
1938	O
)	O
.	O
Une	O
deuxième	O
République	O
tchécoslovaque	O
lui	O
succède	O
alors	O
.	O
En	O
1945	O
,	O
Edvard	PERSON
Beneš	PERSON
revient	O
sur	LOCATION
le	O
sol	O
de	O
la	O
Tchécoslovaquie	O
libérée	O
et	O
décrète	O
la	O
formation	O
d'	O
un	PERSON
gouvernement	PERSON
de	PERSON
coalition	PERSON
.	O
Certains	O
historiens	O
,	O
tchèques	LOCATION
en	O
particulier	O
,	O
considèrent	O
comme	O
frappés	O
de	O
nullité	O
les	O
gouvernements	O
suivant	O
la	O
démission	O
d'	O
Edvard	PERSON
Beneš	PERSON
et	PERSON
qu'	PERSON
il	PERSON
y	O
a	O
ainsi	O
continuité	O
de	O
la	O
première	O
République	O
tchécoslovaque	O
jusqu'	O
en	O
1948	O
.	O
La	O
période	O
1945-1948	O
est	O
cependant	O
également	O
désignée	PERSON
sous	PERSON
le	PERSON
nom	PERSON
de	PERSON
troisième	PERSON
République	PERSON
tchécoslovaque	PERSON
.	O
Elle	PERSON
prend	PERSON
fin	PERSON
avec	PERSON
la	PERSON
Révolution	PERSON
de	PERSON
velours	PERSON
,	O
le	O
28	O
novembre	O
1989	O
quand	O
le	O
parti	O
communiste	O
annonce	O
qu'	O
il	O
abandonne	O
sa	PERSON
mainmise	PERSON
sur	O
le	O
pouvoir	O
politique	O
.	O
Les	O
questions	O
nationales	O
travaillent	O
la	O
nouvelle	O
entité	O
et	O
aboutissent	O
à	O
la	O
dissolution	O
de	O
la	O
Tchécoslovaquie	O
le	O
31	O
décembre	O
1992	O
.	O
La	LOCATION
Tchécoslovaquie	LOCATION
a	O
pour	O
codes	O
:	O
