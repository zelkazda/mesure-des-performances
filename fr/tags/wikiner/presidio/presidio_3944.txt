Issu	PERSON
d'	O
une	O
famille	O
piémontaise	O
,	O
il	LOCATION
est	LOCATION
né	LOCATION
au	LOCATION
n°	LOCATION
3	O
de	O
le	O
rue	O
Carreterie	O
à	O
Avignon	O
,	O
en	O
1888	O
,	O
où	O
son	O
père	PERSON
était	PERSON
tailleur	PERSON
de	PERSON
pierre	PERSON
,	O
luthier	O
et	O
chanteur	O
d'	O
opéra	O
.	O
Il	O
est	O
professeur	O
à	LOCATION
Avignon	LOCATION
,	O
à	O
Bourg-en-Bresse	O
et	O
à	O
Philippeville	LOCATION
.	O
Il	O
fait	O
campagne	O
aux	O
Dardanelles	O
,	O
en	O
Macédoine	O
,	O
en	O
Serbie	LOCATION
,	O
en	O
Albanie	LOCATION
,	O
en	O
Hongrie	PERSON
et	O
en	O
Grèce	PERSON
.	O
Au	O
cours	O
de	O
son	O
séjour	O
à	O
Naples	LOCATION
,	O
il	O
rencontre	O
Max	PERSON
Jacob	PERSON
et	PERSON
le	PERSON
philosophe	O
Jean	PERSON
Grenier	PERSON
qui	PERSON
devient	O
son	O
ami	PERSON
.	O
Plus	O
tard	O
,	O
en	O
1931	O
,	O
il	O
arrive	O
au	O
Maroc	O
où	O
il	O
va	O
passer	O
une	O
autre	O
longue	O
partie	O
de	O
sa	O
vie	O
en	O
tant	O
que	O
professeur	O
au	PERSON
lycée	PERSON
de	PERSON
Rabat	PERSON
.	O
C'	O
est	O
à	O
la	O
fin	O
de	O
ce	O
séjour	O
,	O
en	O
1945	O
,	O
qu'	LOCATION
il	LOCATION
obtient	LOCATION
le	LOCATION
Prix	PERSON
Renaudot	PERSON
pour	O
le	PERSON
Mas	PERSON
Théotime	PERSON
.	O
Cette	O
récompense	O
prestigieuse	O
avait	O
été	O
précédée	O
,	O
deux	LOCATION
ans	LOCATION
auparavant	O
,	O
par	O
un	PERSON
hommage	PERSON
rendu	PERSON
à	PERSON
Avignon	PERSON
.	O
Arrivé	O
à	O
l'	O
âge	O
de	O
la	O
retraite	O
,	O
il	PERSON
partage	PERSON
sa	O
vie	O
entre	O
Nice	O
et	O
Lourmarin	O
où	O
il	O
découvre	O
un	O
Luberon	O
,	O
terre	O
de	O
paysans	O
et	O
de	O
vignerons	O
,	O
qu'	LOCATION
il	LOCATION
va	LOCATION
chanter	LOCATION
avec	O
des	O
accents	O
homériques	O
.	O
Le	O
chantre	O
du	O
Luberon	O
désira	O
reposer	O
dans	O
le	O
cimetière	O
de	O
Lourmarin	O
.	O
