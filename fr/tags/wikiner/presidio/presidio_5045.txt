Joseph-Louis	PERSON
Lagrange	PERSON
(	O
1736	O
1813	O
)	O
met	O
en	O
évidence	O
la	O
relation	O
entre	O
les	O
propriétés	O
des	O
permutations	O
des	O
racines	O
et	O
la	O
possibilité	O
de	O
résolution	O
d'	O
une	O
équation	O
cubique	O
ou	O
quartique	O
.	O
Paolo	PERSON
Ruffini	PERSON
(	O
1765	O
1822	O
)	O
est	O
le	O
premier	O
à	O
comprendre	O
que	O
l'	O
équation	O
générale	O
et	O
particulièrement	O
l'	O
équation	O
quintique	O
n'	O
admet	O
pas	O
de	O
solution	O
.	O
Les	PERSON
démonstrations	PERSON
de	PERSON
Niels	PERSON
Henrik	PERSON
Abel	PERSON
(	O
1802	O
1829	O
)	O
dans	O
deux	O
articles	O
écrits	O
en	O
1824	O
et	O
1826	O
passent	O
,	O
après	O
des	O
années	O
d'	O
incompréhension	O
,	O
à	O
la	O
postérité	O
.	O
Évariste	PERSON
Galois	PERSON
(	O
1811	O
1832	O
)	O
résout	O
définitivement	O
la	O
problématique	O
en	O
proposant	O
une	O
condition	O
nécessaire	O
et	O
suffisante	O
juste	O
pour	O
la	O
résolvabilité	O
de	O
l'	O
équation	O
par	O
radicaux	PERSON
.	O
Ses	O
premiers	O
écrits	LOCATION
,	O
présentés	PERSON
à	PERSON
l'	PERSON
Académie	O
des	O
sciences	O
dès	O
1829	O
,	O
sont	O
définitivement	O
perdus	O
.	O
Puis	PERSON
Arthur	PERSON
Cayley	PERSON
(	O
1821	O
1895	O
)	O
donne	O
une	O
première	O
définition	O
abstraite	O
de	O
la	O
structure	O
de	O
groupe	O
,	O
indépendante	O
de	O
la	O
notion	O
de	O
permutation	O
.	O
