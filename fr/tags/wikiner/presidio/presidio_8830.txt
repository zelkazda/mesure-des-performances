Le	LOCATION
western	LOCATION
,	O
littéralement	O
"	O
[	O
film	O
]	O
de	O
l'	O
ouest	O
"	O
,	O
trouve	O
ses	O
origines	O
au	O
plus	O
profond	O
de	O
l'	O
Histoire	O
des	O
États-Unis	O
.	O
D'	O
après	O
l'	O
historien	O
Frederick	PERSON
Jackson	PERSON
Turner	PERSON
,	O
l'	O
épopée	O
des	O
pionniers	O
a	O
forgé	O
l'	O
identité	O
même	PERSON
du	PERSON
peuple	PERSON
américain	PERSON
.	PERSON
Les	O
auteurs	O
étaient	O
américains	O
mais	O
aussi	PERSON
européens	PERSON
avec	PERSON
Karl	PERSON
May	PERSON
en	O
Allemagne	O
ou	O
Gustave	PERSON
Aimard	PERSON
en	O
France	LOCATION
.	O
Le	O
statut	O
de	O
premier	O
western	O
est	O
accordé	O
au	O
Vol	O
du	O
grand	O
rapide	O
tourné	O
en	O
1903	O
,	O
.	O
Ce	O
genre	O
de	O
production	O
est	O
quasiment	O
authentique	O
puisqu'	O
il	O
s'	O
inspire	O
directement	O
de	O
faits	O
récents	O
comme	O
les	O
exploits	O
du	O
Wild	O
Bunch	O
dans	O
les	O
années	O
1890	O
.	O
Si	PERSON
le	PERSON
western	PERSON
est	PERSON
aujourd'hui	PERSON
connu	PERSON
pour	O
être	O
un	O
genre	O
cinématographique	PERSON
essentiellement	PERSON
américain	PERSON
,	O
la	O
France	LOCATION
produira	LOCATION
pourtant	O
dès	O
1907	O
ses	O
propres	O
western	O
,	O
qui	PERSON
furent	PERSON
alors	PERSON
considérés	O
comme	O
plus	O
réalistes	O
que	O
les	O
productions	O
américaines	O
.	O
Le	O
western	O
a	O
construit	O
une	O
légende	O
autour	O
de	O
figures	O
emblématiques	O
telles	O
que	O
Billy	PERSON
the	PERSON
Kid	PERSON
et	O
Jesse	O
James	O
.	O
Il	O
s'	O
est	O
inspiré	O
de	O
faits	O
comme	O
la	O
fusillade	O
d'	O
OK	O
Corral	O
qui	O
fut	O
mise	O
en	O
scène	O
dans	O
de	O
nombreux	O
films	O
.	O
La	O
première	O
star	O
du	O
western	O
est	O
Broncho	PERSON
Billy	PERSON
Anderson	PERSON
,	O
qui	PERSON
fut	PERSON
très	PERSON
actif	PERSON
dès	PERSON
1908	PERSON
.	O
Les	O
deux	O
grandes	O
vedettes	O
du	O
western	O
muet	O
arrivèrent	O
dans	O
les	O
années	O
1910	O
,	O
il	O
s'	O
agit	O
de	O
William	O
S.	O
Hart	O
et	O
Tom	PERSON
Mix	PERSON
.	O
Les	O
artisans	O
du	O
genre	O
sont	O
parfois	O
de	O
véritables	O
acrobates	O
comme	O
Ken	PERSON
Maynard	PERSON
ou	PERSON
Yakima	PERSON
Canutt	PERSON
.	O
Le	O
premier	O
western	O
parlant	O
,	O
In	O
Old	LOCATION
Arizona	LOCATION
,	O
tourné	O
dans	O
le	O
parc	O
national	O
de	O
Zion	O
en	O
1928	O
,	O
montre	O
qu'	O
il	O
est	O
possible	O
de	PERSON
surmonter	PERSON
les	PERSON
difficultés	O
liées	LOCATION
à	O
la	O
prise	O
de	O
son	O
en	O
extérieur	O
.	O
En	O
1930	O
sort	O
La	O
Piste	O
des	O
géants	O
,	O
présenté	O
à	O
l'	O
époque	O
comme	O
"	O
le	O
film	O
le	O
plus	O
important	O
jamais	O
produit	O
"	O
.	O
Dès	O
1932	O
,	O
la	O
plupart	O
des	O
stars	O
du	O
western	O
muet	O
s'	O
y	O
sont	O
reconverties	O
avec	O
succès	O
:	O
Hoot	PERSON
Gibson	PERSON
,	O
Ken	PERSON
Maynard	PERSON
,	O
Buck	PERSON
Jones	PERSON
,	O
Tim	PERSON
McCoy	PERSON
…	O
En	O
1935	O
,	O
la	O
mode	O
du	PERSON
cowboy	PERSON
chantant	O
est	O
lancée	PERSON
par	PERSON
Gene	PERSON
Autry	PERSON
,	O
qui	O
passe	O
de	O
la	O
radio	O
au	O
grand	O
écran	O
.	O
Il	O
est	O
suivi	O
par	O
Tex	PERSON
Ritter	PERSON
et	O
surtout	O
Roy	O
Rogers	O
.	O
Autry	O
et	O
Rogers	O
atteignent	O
une	O
célébrité	O
extraordinaire	O
.	O
En	O
tête	O
de	O
tous	O
les	O
classements	O
,	O
ils	O
font	O
partie	O
des	O
acteurs	O
les	O
mieux	O
payés	O
d'	O
Hollywood	LOCATION
dans	O
les	O
années	O
1940	O
.	O
Plusieurs	O
acteurs	O
ont	PERSON
connu	PERSON
la	PERSON
gloire	PERSON
ou	PERSON
tout	O
simplement	O
lancé	O
leur	O
carrière	O
grâce	O
à	O
lui	O
:	O
Gary	PERSON
Cooper	PERSON
ou	PERSON
John	PERSON
Wayne	PERSON
.	O
Certains	O
,	O
comme	O
Karl	PERSON
Malden	PERSON
ou	PERSON
Lee	PERSON
Marvin	PERSON
y	PERSON
incarnèrent	PERSON
avec	PERSON
succès	PERSON
de	PERSON
sordides	O
crapules	O
.	O
L'	O
armée	O
américaine	O
est	O
quant	O
à	O
elle	O
valeureuse	O
et	O
bienfaisante	O
(	O
La	O
Charge	O
héroïque	O
,	O
1949	O
)	O
.	O
Les	O
femmes	O
sont	O
toujours	O
des	O
êtres	O
distingués	O
et	O
protégés	O
(	O
La	O
Poursuite	O
infernale	O
,	O
1946	O
)	O
.	O
Ce	O
manichéisme	O
apparent	O
est	O
souvent	O
l'	O
articulation	O
de	O
l'	O
action	O
:	O
le	O
bon	O
shérif	O
contre	O
les	O
bandits	O
(	O
Règlements	O
de	O
comptes	O
à	O
OK	O
Corral	O
,	O
1957	O
)	O
,	O
les	O
cultivateurs	O
contre	O
les	O
éleveurs	O
(	O
L'	O
Homme	O
des	O
vallées	O
perdues	O
,	O
1953	O
)	O
,	O
les	O
gens	O
de	O
la	O
ville	O
contre	O
ceux	O
du	O
cru	O
,	O
l'	O
homme	O
de	O
loi	O
contre	O
le	O
shérif	O
véreux	O
,	O
etc	O
.	O
Dans	O
les	O
années	O
1960	O
,	O
le	LOCATION
genre	LOCATION
perd	O
de	O
la	O
vitesse	O
aux	O
États-Unis	O
.	O
Le	O
renouveau	O
du	O
western	O
vient	O
alors	O
paradoxalement	O
d'	O
Europe	LOCATION
,	O
et	O
en	O
particulier	O
des	O
réalisateurs	O
italiens	O
qui	PERSON
lui	PERSON
insufflent	PERSON
une	PERSON
seconde	O
jeunesse	O
,	O
avec	O
ce	O
qui	O
sera	O
nommé	O
le	O
western	O
spaghetti	O
.	O
Faisant	O
la	O
synthèse	O
d'	O
influences	O
multiples	O
,	O
Sergio	PERSON
Leone	PERSON
établit	LOCATION
les	O
codes	O
et	O
usages	O
de	O
cette	O
sous-catégorie	O
en	O
réalisant	O
quelques	O
uns	O
des	O
meilleurs	O
films	O
du	O
genre	O
.	O
La	O
violence	O
fait	O
son	O
apparition	O
via	O
des	O
scènes	O
récurrentes	O
de	O
torture	O
(	O
Django	O
,	O
1966	O
)	O
,	O
de	O
viol	O
(	O
La	O
mort	O
était	O
au	O
rendez-vous	O
,	O
1967	O
)	O
ou	O
de	O
massacre	O
(	O
Le	O
Grand	O
Silence	O
,	O
1968	O
)	O
,	O
préfigurant	O
le	O
western	O
crépusculaire	O
.	O
L'	O
italien	O
Ennio	PERSON
Morricone	PERSON
en	O
a	O
composé	O
les	O
plus	O
grandes	O
réussites	O
.	O
Aux	O
États-Unis	O
,	O
depuis	O
les	O
années	O
1970	O
,	O
des	O
réalisateurs	O
comme	O
Clint	PERSON
Eastwood	PERSON
ou	O
Sam	PERSON
Peckinpah	PERSON
ont	PERSON
réalisé	PERSON
des	PERSON
westerns	O
dits	O
"	O
crépusculaires	O
"	O
.	O
Tout	LOCATION
comme	O
dans	LOCATION
le	LOCATION
western	LOCATION
italien	LOCATION
,	O
l'	O
héroïsme	O
manichéen	O
des	O
cow-boys	O
classiques	O
a	O
cédé	O
la	O
place	O
à	O
des	O
personnages	O
ambivalents	O
,	O
qui	O
s'	O
affranchissent	O
sans	O
difficulté	O
de	O
la	O
frontière	O
ténue	O
entre	O
le	O
bien	O
et	O
le	O
mal	O
(	O
L'	O
Homme	O
des	O
Hautes	O
Plaines	O
,	O
1973	O
)	O
.	O
Les	O
personnages	O
féminins	O
sont	O
essentiellement	O
des	O
prostituées	O
,	O
elles	O
fument	O
et	O
boivent	O
,	O
comme	O
dans	O
Pendez-les	PERSON
haut	PERSON
et	PERSON
court	PERSON
(	O
1968	O
)	O
.	O
Le	O
meilleur	O
exemple	O
est	O
La	O
Horde	O
sauvage	O
(	O
1969	O
)	O
de	PERSON
Sam	PERSON
Peckinpah	PERSON
,	O
où	LOCATION
le	LOCATION
sang	O
est	O
omniprésent	O
,	O
les	O
blessures	O
mises	O
en	O
valeur	O
,	O
et	O
où	O
la	O
fusillade	O
finale	O
est	O
un	O
gigantesque	O
massacre	O
.	O
De	PERSON
même	PERSON
,	O
on	O
assiste	O
à	O
des	O
scènes	O
cruelles	O
comme	O
le	O
viol	O
dans	O
Josey	PERSON
Wales	PERSON
hors-la-loi	O
(	O
1976	O
)	O
.	O
Les	O
dernières	O
grandes	O
réussites	O
du	O
genre	O
,	O
telles	O
qu	O
'	O
Impitoyable	O
(	O
1992	O
)	O
de	PERSON
Clint	PERSON
Eastwood	PERSON
,	O
dressent	O
paradoxalement	O
un	O
constat	O
d'	O
échec	O
et	O
d'	O
impasse	O
du	O
western	O
.	O
Le	O
lieu	O
le	O
plus	O
symbolique	O
du	O
western	O
est	O
Monument	LOCATION
Valley	LOCATION
.	O
Sa	O
notoriété	O
est	O
due	O
à	O
la	O
réputation	O
de	PERSON
John	PERSON
Ford	PERSON
,	O
qui	O
y	O
tourna	O
une	O
dizaine	O
d'	O
opus	O
majeurs	O
comme	O
La	O
Prisonnière	O
du	O
désert	O
(	O
1956	O
)	O
.	O
De	PERSON
même	PERSON
,	PERSON
de	PERSON
par	PERSON
la	PERSON
popularité	PERSON
de	PERSON
Sergio	PERSON
Leone	PERSON
,	O
la	O
présence	O
de	O
Monument	O
Valley	O
dans	O
Il	O
était	O
une	O
fois	O
dans	O
l'	O
Ouest	O
(	O
1968	O
)	O
semble	O
consacrer	O
le	O
lieu	O
au	O
rang	O
d'	O
emblème	O
ultime	O
du	O
western	O
,	O
faisant	O
le	O
pont	O
improbable	O
entre	O
classique	O
et	O
spaghetti	O
.	O
En	O
termes	O
quantitatifs	O
,	O
le	O
lieu	O
le	O
plus	O
important	O
du	O
western	O
est	O
sans	O
conteste	O
Alabama	LOCATION
Hills	LOCATION
,	O
où	O
des	O
centaines	O
de	O
productions	O
se	O
sont	O
succédé	O
.	O
Tous	O
les	O
grands	O
noms	PERSON
s'	O
y	O
sont	PERSON
illustrés	PERSON
;	O
certains	O
tellement	O
de	O
fois	O
qu'	O
ils	O
sont	O
aujourd'hui	O
indissociables	O
de	O
l'	O
endroit	O
:	O
Gene	PERSON
Autry	PERSON
,	O
Roy	PERSON
Rogers	PERSON
,	O
Hopalong	PERSON
Cassidy	PERSON
ou	PERSON
Tom	PERSON
Mix	PERSON
.	O
Ils	O
sont	O
rejoints	O
sur	O
ce	O
plan	O
par	O
les	O
plaines	O
désertiques	O
d'	O
Arizona	LOCATION
ou	O
du	O
Nouveau-Mexique	O
,	O
dont	O
le	O
paroxysme	O
est	O
la	O
Vallée	PERSON
de	PERSON
la	PERSON
mort	PERSON
.	PERSON
Entre	PERSON
autres	PERSON
,	O
Anthony	PERSON
Mann	PERSON
est	PERSON
réputé	PERSON
pour	O
sa	O
faculté	O
à	O
exploiter	O
les	O
paysages	O
naturels	O
.	O
En	O
ce	O
qui	O
concerne	O
le	LOCATION
western	LOCATION
spaghetti	O
,	O
il	O
fut	O
principalement	O
filmé	O
en	O
Espagne	O
dans	O
la	O
Province	O
d'	O
Almería	LOCATION
.	O
Le	PERSON
désert	PERSON
de	PERSON
Tabernas	PERSON
compte	O
parmi	O
les	O
lieux	O
les	O
plus	O
secs	O
d'	O
Europe	LOCATION
,	O
ce	LOCATION
qui	LOCATION
en	LOCATION
fait	O
le	O
substitut	O
idéal	O
des	O
déserts	O
arizoniens	LOCATION
.	O
Le	O
western	O
spaghetti	O
de	O
montagne	O
existe	O
aussi	PERSON
:	O
Le	O
Grand	O
Silence	O
fut	O
tourné	O
dans	O
la	O
neige	O
des	O
Pyrénées	O
et	O
des	O
Dolomites	O
.	O
Pour	O
se	O
rendre	O
sur	O
les	O
lieux	O
sauvages	O
du	O
Nevada	O
ou	O
d'	O
Arizona	LOCATION
,	O
les	O
équipes	O
de	O
tournage	O
basées	PERSON
à	O
Hollywood	LOCATION
devaient	O
voyager	O
plusieurs	O
centaines	O
de	O
kilomètres	O
.	O
Mais	PERSON
plusieurs	PERSON
grands	PERSON
studios	O
possédaient	O
leur	O
propre	O
ranch	O
:	O
la	O
Paramount	O
,	O
RKO	O
,	O
etc	O
.	O
Les	O
immenses	O
plaines	O
occidentales	O
ne	O
sont	O
pas	O
sous	O
la	O
maitrise	PERSON
des	O
États-Unis	O
.	O
Ainsi	O
,	O
ce	PERSON
pays	O
convient	O
tout	O
aussi	PERSON
bien	PERSON
à	PERSON
l'	PERSON
action	O
d'	O
un	O
western	O
,	O
comme	LOCATION
le	LOCATION
montrent	LOCATION
Mr	O
Quigley	PERSON
l'	O
Australien	O
(	O
1990	O
)	O
,	O
Ned	PERSON
Kelly	PERSON
(	O
2003	O
)	O
ou	O
Australia	LOCATION
(	O
2008	O
)	O
.	O
Ces	O
films	O
portent	O
aussi	O
la	O
marque	O
d'	O
un	O
panthéisme	O
parfois	O
naïf	O
,	O
mais	O
souvent	O
lyrique	O
et	O
inspiré	O
(	O
Jeremiah	PERSON
Johnson	PERSON
de	PERSON
Sydney	O
Pollack	O
,	O
1972	O
)	O
,	O
qui	O
est	O
aussi	PERSON
un	PERSON
des	PERSON
éléments	PERSON
fondateurs	PERSON
du	O
mythe	O
,	O
celui	O
de	O
la	O
difficile	O
osmose	O
entre	O
l'	O
homme	O
et	O
la	O
nature	O
(	O
La	O
Captive	O
aux	O
yeux	O
clairs	O
,	O
d'	O
Howard	O
Hawks	O
,	O
1952	O
)	O
.	O
Ces	PERSON
idées	PERSON
de	PERSON
loi	PERSON
absente	PERSON
,	O
constitutives	O
du	O
western	O
,	O
ont	O
souvent	O
été	O
réutilisées	O
par	O
ailleurs	O
au	O
cinéma	O
,	O
souvent	O
dans	O
le	O
film	O
policier	O
(	O
Assaut	O
de	O
John	PERSON
Carpenter	PERSON
est	PERSON
le	PERSON
remake	O
de	O
Rio	O
Bravo	O
d'	O
Howard	PERSON
Hawks	PERSON
)	O
,	O
ou	O
de	O
science-fiction	O
,	O
soit	PERSON
carrément	PERSON
à	O
l'	O
échelle	PERSON
d'	O
une	O
série	O
telle	O
que	O
Star	O
Trek	O
.	O
L'	O
influence	O
a	O
aussi	O
été	O
politique	O
,	O
durant	O
la	O
Seconde	O
Guerre	O
mondiale	O
par	O
exemple	O
,	O
où	LOCATION
le	PERSON
caractère	PERSON
généreux	O
protecteur	O
de	O
l'	O
armée	O
est	O
d'	O
autant	O
plus	O
mis	O
en	O
exergue	O
.	O
Beaucoup	O
de	O
westerns	O
après	O
les	O
années	O
50	O
furent	PERSON
influencés	PERSON
par	PERSON
les	O
films	O
de	PERSON
samouraïs	PERSON
d'	PERSON
Akira	PERSON
Kurosawa	PERSON
.	O
Les	O
Sept	O
Mercenaires	O
(	O
1960	O
)	O
est	O
un	O
remake	O
des	O
Sept	O
Samouraïs	O
(	O
1954	O
)	O
et	O
Et	O
pour	O
quelques	O
dollars	O
de	O
plus	O
(	O
1965	O
)	O
un	O
remake	O
du	O
Garde	O
du	O
corps	O
(	O
1961	O
)	O
.	O
Réciproquement	PERSON
,	O
Kurosawa	O
était	O
lui	O
même	O
influencé	O
par	O
le	O
genre	O
western	O
,	O
en	O
particulier	O
par	O
John	PERSON
Ford	PERSON
.	O
Par	O
exemple	O
The	O
Postman	O
(	O
1997	O
)	O
,	O
la	O
série	O
Mad	O
Max	O
(	O
1979	O
,	O
1981	O
,	O
1985	O
)	O
ou	O
la	O
série	O
de	O
jeux	O
vidéos	O
Fallout	O
.	O
Outland	O
...	O
loin	PERSON
de	PERSON
la	PERSON
terre	PERSON
(	O
1981	O
)	O
transfère	O
le	O
scénario	O
du	O
Train	O
sifflera	O
trois	PERSON
fois	PERSON
(	O
1952	O
)	O
à	O
l'	O
espace	O
interstellaire	O
.	O
De	PERSON
même	PERSON
,	O
Gene	PERSON
Roddenberry	PERSON
,	O
créateur	O
de	O
Star	O
Trek	O
,	O
a	O
décrit	O
sa	O
vision	O
de	PERSON
la	PERSON
série	PERSON
comme	O
"	O
a	O
wagon	O
train	O
to	O
the	O
stars	O
"	O
(	O
un	O
convoi	O
vers	O
les	O
étoiles	O
)	O
.	O
Plus	O
récemment	O
,	O
la	O
série	O
de	O
space	O
opera	O
Firefly	O
(	O
2002	O
)	O
utilise	O
un	O
point	O
de	PERSON
vue	PERSON
explicitement	O
western	O
pour	O
représenter	O
la	O
frontière	O
des	O
mondes	O
.	O
Par	O
exemple	O
,	O
De	O
l'	O
or	O
pour	O
les	O
braves	O
(	O
1970	O
)	O
est	O
un	O
film	O
de	O
guerre	O
dans	O
lequel	O
l'	O
action	O
et	O
les	O
personnages	O
sont	O
semblables	O
à	O
ceux	O
du	O
western	O
.	O
Le	O
film	O
britannique	O
Zoulou	O
(	O
1964	O
)	O
qui	O
se	O
déroule	PERSON
durant	PERSON
la	O
Guerre	O
anglo-zouloue	O
a	O
parfois	O
été	O
comparé	O
à	O
un	O
western	O
,	O
bien	O
qu'	O
il	O
se	O
passe	O
en	O
Afrique	O
du	O
Sud	O
.	O
Le	O
personnage	O
joué	PERSON
par	PERSON
Humphrey	PERSON
Bogart	PERSON
dans	O
les	O
film	O
noirs	O
comme	O
Casablanca	LOCATION
(	O
1942	O
)	O
,	O
Le	O
Port	O
de	O
l'	O
angoisse	O
(	O
1944	O
)	O
ou	O
Le	PERSON
Trésor	PERSON
de	PERSON
la	PERSON
Sierra	O
Madre	O
(	O
1948	O
)	O
--	O
un	O
individu	O
mu	O
uniquement	O
par	O
son	O
sens	O
de	O
l'	O
honneur	O
--	O
a	O
beaucoup	O
en	O
commun	O
avec	O
le	O
héros	O
de	O
western	O
classique	O
.	O
La	O
Tour	O
sombre	O
de	O
Stephen	O
King	O
est	O
une	O
série	O
de	O
romans	O
qui	O
mélange	O
des	O
thèmes	O
western	O
,	O
high	O
fantasy	O
,	O
science-fiction	O
et	O
d'	O
horreur	O
.	O
Les	O
films	O
de	O
la	O
saga	O
La	O
Guerre	O
des	O
étoiles	O
utilisent	O
beaucoup	O
d'	O
éléments	O
des	O
westerns	O
,	O
et	O
George	PERSON
Lucas	PERSON
dit	PERSON
qu'	PERSON
il	PERSON
voulait	PERSON
revitaliser	PERSON
la	PERSON
mythologie	PERSON
cinématographique	PERSON
,	O
que	O
le	O
western	O
détenait	O
jadis	O
.	O
Mais	PERSON
certains	O
peintres	O
comme	O
George	PERSON
Catlin	PERSON
ou	PERSON
Alfred	PERSON
Jacob	PERSON
Miller	PERSON
,	O
conscients	O
des	O
effets	O
que	O
pourrait	O
avoir	O
l'	O
avancée	O
des	O
pionniers	O
sur	O
les	O
cultures	O
et	O
les	O
mythologies	PERSON
amérindiennes	PERSON
,	O
par	O
lesquelles	O
ils	O
sont	O
fascinés	O
,	O
entreprennent	O
un	O
véritable	O
travail	O
ethnographique	O
et	O
reproduisent	O
fidèlement	O
coiffes	O
,	O
costumes	O
,	O
chapeaux	O
,	O
maquillages	PERSON
et	PERSON
autres	PERSON
scènes	PERSON
de	PERSON
la	PERSON
vie	PERSON
quotidienne	PERSON
.	O
C'	O
est	O
Frederic	PERSON
Remington	PERSON
qui	PERSON
le	PERSON
premier	O
annonce	LOCATION
véritablement	LOCATION
le	LOCATION
cinéma	LOCATION
:	O
ses	PERSON
toiles	PERSON
sont	PERSON
de	PERSON
véritables	PERSON
scènes	O
d'	O
action	O
marquées	O
par	O
un	PERSON
dynamisme	PERSON
et	PERSON
une	PERSON
énergie	PERSON
qui	PERSON
ont	PERSON
inspiré	PERSON
bon	PERSON
nombre	PERSON
de	PERSON
cinéastes	PERSON
,	O
dont	O
John	PERSON
Ford	PERSON
qui	PERSON
avoua	PERSON
avoir	PERSON
tenté	PERSON
,	O
en	O
réalisant	O
La	O
Charge	O
héroïque	O
,	O
de	O
reproduire	O
"	O
sa	O
couleur	O
et	O
son	O
mouvement	O
"	O
.	O
Giacomo	PERSON
Puccini	PERSON
a	O
composé	O
un	PERSON
opéra-western	PERSON
:	O
La	LOCATION
Fanciulla	LOCATION
del	LOCATION
West	LOCATION
.	O
Bonanza	O
,	O
diffusée	O
de	O
1959	O
à	O
1973	O
,	O
mais	PERSON
surtout	PERSON
Les	PERSON
Mystères	PERSON
de	PERSON
l'	PERSON
Ouest	PERSON
,	O
malgré	O
seulement	O
104	O
épisodes	O
en	O
quatre	O
ans	O
d'	O
existence	O
,	O
achèvent	PERSON
alors	PERSON
de	PERSON
populariser	PERSON
la	PERSON
série	PERSON
télévisée	PERSON
western	PERSON
et	O
connaîtront	O
même	O
par	O
la	O
suite	O
une	O
sortie	O
en	O
dvd	O
,	O
gage	O
de	O
leur	O
passage	O
à	O
la	O
postériorité	O
.	O
Dès	O
1985	O
,	O
Gun.Smoke	O
sort	O
sur	LOCATION
bornes	O
d'	O
arcade	O
puis	O
NES	O
et	O
se	O
présente	O
comme	O
un	PERSON
jeu	PERSON
de	PERSON
type	O
run	O
and	O
gun	O
qui	O
recense	O
la	O
majorité	O
des	O
codes	O
du	O
western	O
américain	O
classique	O
et	O
crépusculaire	O
.	O
Sunset	O
Riders	O
,	O
en	O
1991	O
,	O
sort	O
également	O
sur	O
bornes	O
d'	O
arcade	O
et	O
reprend	O
l'	O
atmosphère	O
du	O
western	O
mais	O
y	O
inclut	O
une	O
dimension	O
parodique	O
et	O
humoristique	O
.	O
Ce	O
premier	O
rapprochement	O
du	PERSON
jeu	PERSON
vidéo	PERSON
avec	PERSON
le	PERSON
western	PERSON
spaghetti	PERSON
entraîna	PERSON
la	O
création	O
de	PERSON
jeux	PERSON
vidéo	PERSON
western	O
qui	O
s'	O
éloignent	O
de	O
plus	O
en	O
plus	O
du	O
western	O
américain	O
classique	O
et	O
incluent	O
de	O
l'	O
humour	O
,	O
mais	PERSON
aussi	PERSON
parfois	PERSON
de	PERSON
la	PERSON
violence	PERSON
explicite	PERSON
,	O
comme	O
Outlaws	O
.	O
La	O
suite	O
de	O
ce	O
dernier	O
,	O
intitulée	O
Red	O
Dead	O
Redemption	O
et	O
sortie	O
en	O
2010	O
,	O
est	O
basée	O
sur	O
le	O
modèle	O
des	O
GTA	O
et	O
présente	O
un	O
grand	O
monde	O
ouvert	O
qui	O
permet	O
de	O
visiter	O
plusieurs	O
états	O
américains	PERSON
ainsi	PERSON
qu'	PERSON
une	O
province	O
frontalière	PERSON
mexicaine	PERSON
,	O
pendant	O
la	O
Révolution	O
de	O
1910	O
.	O
