Ces	PERSON
réglementations	PERSON
sont	O
généralement	O
basées	O
sur	O
les	O
normes	O
de	O
la	O
Fédération	O
internationale	O
des	O
mouvements	O
d'	O
agriculture	O
biologique	O
(	O
IFOAM	O
)	O
,	O
association	O
internationale	O
coordonnant	O
les	O
organisations	O
actives	O
dans	O
le	O
secteur	O
bio	O
.	O
Un	O
nombre	O
croissant	O
d'	O
agriculteurs	O
au	O
Québec	O
se	O
tournent	O
vers	O
l'	O
agriculture	O
biologique	O
pour	O
répondre	O
à	O
la	O
demande	O
des	PERSON
consommateurs	PERSON
.	O
Ce	O
label	O
est	O
réputé	O
pour	O
être	O
un	O
des	O
plus	O
stricts	O
d'	O
Europe	LOCATION
.	O
Au	O
sein	O
de	O
l'	O
Union	O
européenne	O
,	O
le	O
premier	O
règlement	O
sur	O
l'	O
agriculture	O
biologique	O
est	O
entré	O
en	O
vigueur	O
en	O
1992	O
,	O
suivi	O
en	O
août	O
1999	O
,	O
de	O
règles	O
relatives	O
à	O
la	O
production	O
,	O
l'	O
étiquetage	O
et	O
l'	O
inspection	O
en	O
matière	O
d'	O
élevage	O
.	O
La	O
réalisation	O
la	O
plus	O
visible	O
de	O
ce	O
plan	O
fut	O
la	O
proposition	O
de	O
la	O
Commission	O
européenne	O
pour	O
un	O
nouveau	O
règlement	O
de	O
l'	O
agriculture	O
biologique	O
en	O
2005	O
.	O
La	O
Commission	O
européenne	O
a	O
fixé	O
les	O
règles	O
d'	O
application	O
détaillées	O
par	O
le	O
règlement	O
n	O
o	O
889/2008	O
.	O
Par	O
exemple	O
,	O
les	O
aides	O
à	O
la	O
conversion	O
en	O
Autriche	O
sont	O
de	O
l'	O
ordre	O
de	O
450	O
euros	O
par	O
hectare	O
.	O
En	O
France	O
,	O
l'	O
aide	O
est	O
accordée	O
sur	O
une	O
période	O
de	O
5	O
ans	O
et	O
varie	O
selon	O
les	O
cultures	O
:	O
Ensuite	O
,	O
en	O
France	LOCATION
,	O
les	O
agriculteurs	O
bio	O
peuvent	O
bénéficier	O
d'	O
une	O
aide	O
au	PERSON
maintien	PERSON
.	O
Surface	O
par	O
pays	O
:	O
la	O
plus	O
grande	O
était	O
en	O
2005	O
en	O
Italie	O
,	O
devant	O
l'	O
Allemagne	O
et	O
l'	O
Espagne	O
(	O
0,8	O
million	O
d'	O
hectares	O
chacun	O
,	O
soit	LOCATION
13	O
%	O
)	O
.	O
Les	PERSON
taux	PERSON
les	PERSON
plus	PERSON
faibles	O
étaient	O
mesurés	O
à	O
Malte	O
(	O
0,1	O
%	O
)	O
,	O
Pologne	LOCATION
(	O
0,6	O
%	O
)	O
et	O
Irlande	O
(	O
0,8	O
%	O
)	O
.	O
Les	O
fermes	O
bio	O
couvrant	O
les	O
plus	O
grandes	O
surfaces	O
étaient	O
en	O
Slovaquie	O
(	O
463	O
ha	O
par	O
exploitation	O
)	O
,	O
en	O
République	PERSON
tchèque	PERSON
(	O
305	O
ha	O
)	O
,	O
au	LOCATION
Portugal	LOCATION
(	O
148	O
ha	O
)	O
et	O
au	O
Royaume-Uni	O
(	O
142	O
ha	O
)	O
.	O
La	O
part	O
de	O
la	O
surface	O
en	O
cours	O
de	O
conversion	O
dans	O
le	O
total	O
des	O
surfaces	O
cultivées	O
en	O
bio	O
,	O
varie	O
fortement	O
,	O
de	PERSON
moins	PERSON
de	PERSON
10	PERSON
%	PERSON
au	O
Danemark	O
(	O
1	O
%	O
)	O
,	O
aux	LOCATION
Pays-Bas	LOCATION
(	O
4	O
%	O
)	O
,	O
en	O
Finlande	PERSON
(	O
8	O
%	O
)	O
et	O
en	O
Suède	LOCATION
(	O
9	O
%	O
)	O
à	O
plus	O
de	O
80	O
%	O
à	O
Malte	O
(	O
100	O
%	O
)	O
,	O
Chypre	O
(	O
87	O
%	O
)	O
ou	O
en	O
Lettonie	PERSON
(	O
83	O
%	O
)	O
,	O
pays	O
où	O
le	O
développement	O
de	O
la	O
certification	O
bio	O
est	O
plus	O
récent	O
.	O
L'	O
Allemagne	O
est	O
un	O
pays	O
leader	O
dans	O
le	PERSON
domaine	PERSON
de	PERSON
l'	PERSON
agriculture	O
biologique	O
,	O
à	O
la	O
fin	O
de	O
l'	O
année	O
2007	O
,	O
5,1	O
%	O
des	O
surfaces	O
cultivés	O
étaient	O
consacrés	O
à	O
ce	O
type	O
de	O
culture	O
,	O
et	O
le	O
chiffre	O
d'	O
affaires	O
des	O
produits	O
issus	O
de	O
l'	O
agriculture	O
biologique	O
s'	O
élevait	O
a	O
presque	O
4	O
milliards	O
d'	O
euros	O
.	O
Le	O
terme	O
agriculture	O
biologique	O
est	O
légalement	PERSON
protégé	PERSON
en	O
France	LOCATION
depuis	O
la	O
loi	O
d'	O
orientation	O
agricole	O
du	O
4	O
juillet	O
1980	O
et	O
le	O
décret	O
du	O
10	O
mars	O
1981	O
,	O
lesquels	O
l'	O
ont	PERSON
définie	PERSON
,	O
et	O
ont	O
fixé	O
les	O
conditions	O
d'	O
homologation	O
des	O
cahiers	O
des	O
charges	O
et	O
précisé	O
les	O
substances	O
pouvant	O
être	O
utilisées	O
dans	O
la	O
production	O
,	O
la	O
conservation	O
et	O
la	O
transformation	O
des	O
produits	O
agricoles	O
dits	O
biologiques	O
.	O
Par	O
exemple	O
en	O
France	LOCATION
un	O
sondage	O
de	O
2009	O
montre	O
que	O
90	O
%	O
de	O
la	O
population	O
pensent	O
(	O
opinion	O
)	O
que	O
les	O
produits	O
bilogiques	O
sont	O
"	O
plus	O
naturels	O
car	O
cultivés	O
sans	O
produits	O
chimiques	O
"	O
,	O
81	O
%	O
pensent	O
(	O
preuve	O
empirique	O
requise	O
opinion	O
s'	O
abstenir	O
)	O
qu'	O
ils	O
sont	O
"	O
meilleurs	O
pour	O
la	O
santé	O
"	O
,	O
et	O
74	O
%	O
pensent	O
(	O
non	O
scientifiquement	O
prouvé	O
)	O
que	O
les	O
"	O
qualités	O
nutritionnelles	O
des	O
aliments	O
(	O
sont	O
)	O
mieux	O
préservées	O
"	O
.	O
