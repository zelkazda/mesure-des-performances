C'	O
est	O
au	O
Palais	O
des	O
Festivals	O
et	O
des	O
Congrès	O
,	O
situé	LOCATION
sur	LOCATION
le	O
boulevard	O
de	O
la	O
Croisette	O
,	O
que	O
les	O
principales	O
projections	O
ont	O
lieu	O
.	O
Parmi	O
elles	O
,	O
on	O
retrouve	PERSON
la	PERSON
Quinzaine	PERSON
,	O
la	O
Cinéfondation	O
,	O
la	O
Semaine	PERSON
de	PERSON
la	PERSON
critique	PERSON
,	O
Un	O
Certain	O
Regard	O
,	O
et	O
surtout	O
le	O
Marché	O
du	O
film	O
de	O
Cannes	O
,	O
le	LOCATION
premier	O
au	LOCATION
monde	LOCATION
,	O
en	O
importance	O
.	O
Jean	PERSON
Zay	PERSON
est	PERSON
fortement	PERSON
intéressé	O
par	O
la	O
proposition	O
,	O
les	PERSON
Américains	PERSON
et	O
les	O
Britanniques	O
l'	O
encouragent	O
dans	O
ce	O
sens	O
.	O
Plusieurs	O
villes	O
sont	O
alors	O
candidates	O
,	O
notamment	PERSON
Vichy	PERSON
,	O
Biarritz	O
et	O
Alger	O
mais	O
c'	O
est	O
Cannes	PERSON
qui	PERSON
remporte	PERSON
les	O
suffrages	O
.	O
La	O
sélection	O
française	O
est	O
arrêtée	O
et	O
comprend	O
L'	O
Enfer	O
des	O
anges	O
de	O
Christian-Jaque	O
,	O
La	O
Charrette	O
fantôme	O
de	O
Julien	O
Duvivier	O
,	O
La	LOCATION
Piste	LOCATION
du	LOCATION
nord	LOCATION
de	LOCATION
Jacques	LOCATION
Feyder	LOCATION
et	LOCATION
L'	LOCATION
Homme	O
du	O
Niger	O
de	O
Jacques	O
de	O
Baroncelli	O
.	O
Parmi	O
les	O
films	O
étrangers	O
,	O
on	O
retrouve	O
Le	O
Magicien	O
d'	O
Oz	O
de	O
Victor	O
Fleming	O
,	O
Au	O
revoir	O
Mr.	O
Chips	PERSON
(	O
Goodbye	O
Mr	O
Chips	O
)	O
de	PERSON
Sam	PERSON
Wood	PERSON
et	O
Les	O
Quatre	O
Plumes	O
blanches	O
de	O
Zoltan	PERSON
Korda	PERSON
.	O
Dès	O
le	O
mois	O
d'	O
août	O
,	O
les	O
vedettes	O
commencent	O
à	PERSON
affluer	PERSON
,	O
la	O
Metro-Goldwyn-Mayer	O
affrète	O
un	PERSON
paquebot	PERSON
transatlantique	O
pour	O
amener	O
les	O
stars	O
d'	O
Hollywood	LOCATION
:	O
Tyrone	O
Power	O
,	O
Gary	PERSON
Cooper	PERSON
,	O
Annabella	PERSON
,	O
Norma	PERSON
Shearer	PERSON
ou	O
encore	O
George	PERSON
Raft	PERSON
.	O
Ainsi	O
,	O
en	O
1947	O
,	O
a	O
lieu	O
l'	O
inauguration	O
du	O
Palais	O
des	O
Festivals	O
et	O
des	O
Congrès	O
qui	O
sera	O
remplacé	O
par	O
un	O
nouveau	O
palais	O
en	O
1983	O
.	O
Le	O
principe	O
était	O
simple	O
:	O
le	O
Centre	O
national	O
de	O
la	O
cinématographie	O
devait	O
donner	O
à	O
la	O
commission	O
de	PERSON
sélection	PERSON
les	O
dates	O
et	O
règlements	O
des	O
autres	O
festivals	O
internationaux	O
en	O
précisant	O
les	O
délais	O
de	O
l'	O
envoi	O
des	O
films	O
.	O
Quatre	O
ans	O
plus	O
tard	O
,	O
en	O
1955	O
,	O
est	O
créée	O
la	O
Palme	O
d'	O
or	O
,	O
à	O
l'	O
initiative	O
de	PERSON
Robert	PERSON
Favre	PERSON
Le	PERSON
Bret	PERSON
.	O
Jusque	PERSON
là	PERSON
,	O
c'	O
était	O
le	O
Grand	O
prix	O
qui	O
était	O
remis	O
.	O
Les	O
hésitations	O
des	O
premières	O
années	O
se	O
résolvent	PERSON
donc	PERSON
à	PERSON
partir	PERSON
des	PERSON
années	PERSON
1950	O
et	O
Cannes	O
s'	O
impose	O
comme	O
la	O
grand-messe	O
du	O
cinéma	O
mondial	O
.	O
Le	O
désir	O
du	O
critique	O
André	PERSON
Bazin	PERSON
,	O
qui	PERSON
voulait	PERSON
que	PERSON
le	PERSON
festival	PERSON
s'	PERSON
occupe	O
un	PERSON
peu	PERSON
moins	PERSON
de	PERSON
mondanités	PERSON
,	O
de	PERSON
patriotisme	PERSON
ou	PERSON
de	PERSON
diplomatie	PERSON
et	O
un	PERSON
peu	PERSON
plus	O
de	O
cinéma	O
,	O
devient	O
réalité	O
.	O
Les	O
plus	O
grands	O
cinéastes	O
y	O
présentent	O
certaines	O
de	O
leurs	O
œuvres	O
majeures	O
:	O
Roberto	PERSON
Rossellini	PERSON
,	O
Federico	PERSON
Fellini	PERSON
,	O
Ingmar	PERSON
Bergman	PERSON
,	O
Elia	PERSON
Kazan	PERSON
,	O
Joseph	PERSON
Mankiewicz	PERSON
,	O
Robert	PERSON
Wise	PERSON
,	O
William	PERSON
Wyler	PERSON
,	O
Michelangelo	PERSON
Antonioni	PERSON
,	O
Vittorio	PERSON
de	PERSON
Sica	PERSON
,	O
Andrzej	PERSON
Wajda	PERSON
,	O
Satyajit	PERSON
Ray	PERSON
,	O
Luis	PERSON
Buñuel	PERSON
ou	PERSON
encore	O
Akira	PERSON
Kurosawa	PERSON
.	O
C'	O
est	O
d'	O
ailleurs	O
cette	PERSON
même	PERSON
année	PERSON
qu'	PERSON
est	PERSON
fondé	O
le	O
Marché	O
du	O
film	O
.	O
Depuis	PERSON
bientôt	PERSON
cinquante	PERSON
ans	PERSON
d'	PERSON
activité	O
,	O
le	O
Marché	O
du	O
film	O
est	O
devenu	O
la	O
première	O
plate-forme	O
mondiale	O
pour	O
le	O
commerce	O
international	O
du	O
film	O
.	O
Dans	O
les	O
années	O
1960	O
,	O
la	O
notion	O
de	O
"	O
film	O
de	O
festival	O
"	O
fait	O
débat	O
mais	O
on	O
découvre	O
de	O
nouveaux	O
réalisateurs	O
dont	O
le	O
talent	O
est	O
immédiatement	O
reconnu	O
et	O
le	O
travail	O
largement	O
récompensé	O
:	O
Andreï	PERSON
Tarkovski	PERSON
,	O
Miklós	PERSON
Jancsó	PERSON
,	O
István	PERSON
Szabó	PERSON
ou	PERSON
encore	PERSON
Glauber	PERSON
Rocha	PERSON
.	O
On	O
remarquera	PERSON
en	O
1962	O
la	O
première	O
Semaine	O
Internationale	O
de	O
la	O
Critique	O
,	O
alors	O
créée	O
dans	O
le	O
but	O
"	O
de	O
mettre	O
à	O
l'	O
honneur	O
les	O
premières	O
et	O
deuxièmes	O
œuvres	O
des	O
cinéastes	O
du	O
monde	O
entier	O
"	O
.	O
Depuis	PERSON
un	PERSON
certain	O
temps	O
,	O
on	O
parle	O
,	O
dans	O
les	PERSON
rues	PERSON
de	PERSON
Cannes	PERSON
,	O
de	O
projections	O
privées	O
,	O
à	O
propos	O
de	O
la	O
Semaine	O
Internationale	O
de	O
la	O
Critique	O
.	O
D'	O
autre	O
part	O
,	O
la	O
Semaine	LOCATION
internationale	O
de	O
la	O
critique	O
accueille	LOCATION
sept	O
courts	O
et	O
sept	O
longs	O
métrages	O
en	O
compétition	O
mais	O
visionne	O
plusieurs	O
autres	O
films	O
hors	O
compétition	O
.	O
Ainsi	O
François	PERSON
Ozon	PERSON
,	O
Alejandro	PERSON
González	PERSON
Iñárritu	PERSON
,	O
Julie	PERSON
Bertucelli	PERSON
ou	PERSON
encore	O
Éléonore	PERSON
Faucher	PERSON
y	PERSON
ont	PERSON
été	PERSON
découverts	O
.	O
L'	O
année	O
d'	O
après	O
,	O
le	LOCATION
président	LOCATION
du	LOCATION
jury	LOCATION
est	O
une	O
femme	O
,	O
c'	O
est	O
Olivia	PERSON
de	PERSON
Havilland	PERSON
;	O
c'	O
était	O
la	O
première	O
fois	O
qu'	O
une	O
femme	O
occupait	O
ce	O
poste	O
.	O
Mais	PERSON
,	O
malgré	O
ce	O
développement	O
considérable	O
,	O
le	O
Festival	O
de	O
Cannes	O
1968	O
sera	O
interrompu	O
le	O
19	O
mai	O
.	O
Le	O
18	O
mai	O
,	O
François	PERSON
Truffaut	PERSON
,	O
Jean-Luc	PERSON
Godard	PERSON
,	O
Claude	PERSON
Lelouch	PERSON
,	O
Claude	PERSON
Berri	PERSON
,	O
Roman	PERSON
Polanski	PERSON
,	O
Louis	PERSON
Malle	PERSON
et	O
Jean-Pierre	PERSON
Léaud	PERSON
se	PERSON
mêlent	PERSON
au	O
mouvement	O
étudiant	O
qui	PERSON
agite	PERSON
Cannes	PERSON
.	O
Pour	O
aider	O
ces	PERSON
célébrités	PERSON
,	O
Alain	PERSON
Resnais	PERSON
,	O
Carlos	PERSON
Saura	PERSON
et	O
Miloš	PERSON
Forman	PERSON
retirent	O
leur	O
film	O
en	O
compétition	O
de	O
la	O
sélection	O
cannoise	O
.	O
En	O
1972	O
,	O
le	O
Festival	O
de	O
Cannes	O
changera	O
de	O
figure	O
:	O
Robert	PERSON
Favre	PERSON
Le	PERSON
Bret	PERSON
est	PERSON
nommé	PERSON
président	PERSON
et	O
Maurice	PERSON
Bessy	PERSON
est	PERSON
élu	PERSON
délégué	PERSON
général	PERSON
.	PERSON
Désormais	LOCATION
,	O
le	O
nouveau	O
délégué	O
général	O
instaura	O
les	O
deux	O
comités	O
de	O
sélection	O
,	O
un	O
pour	O
la	O
France	LOCATION
,	O
et	O
un	O
autre	O
pour	O
le	O
cinéma	O
international	O
.	O
Ce	O
renouveau	O
amènera	O
quelques	O
problèmes	O
pour	O
la	O
sélection	O
du	O
Festival	O
de	O
Cannes	O
1972	O
.	O
Le	O
Palais	O
des	O
Festivals	O
et	O
des	O
Congrès	O
de	O
Cannes	O
devait	O
être	O
agrandi	O
.	O
C'	O
est	O
chaque	PERSON
année	PERSON
une	PERSON
dizaine	PERSON
de	PERSON
réalisateurs	PERSON
ayant	PERSON
réalisé	PERSON
un	O
ou	O
deux	O
courts	O
métrages	PERSON
de	PERSON
fiction	PERSON
qui	PERSON
est	PERSON
accueillie	O
à	O
Cannes	LOCATION
.	O
La	O
Cinéfondation	O
met	O
à	O
disposition	O
des	O
réalisateurs	O
une	O
résidence	O
à	O
Paris	LOCATION
et	O
une	O
aide	O
à	O
l'	O
écriture	O
d'	O
un	PERSON
scénario	PERSON
.	O
En	O
2002	O
,	O
le	O
Festival	O
international	O
du	O
film	O
prend	O
officiellement	O
le	O
nom	O
de	O
Festival	O
de	O
Cannes	O
,	O
qui	LOCATION
le	LOCATION
désignait	LOCATION
couramment	O
.	O
Précédemment	O
détenu	O
par	O
Parsifal	O
(	O
4	O
heures	O
et	O
40	O
minutes	O
)	O
,	O
et	O
Nos	O
meilleures	O
années	O
(	O
6	O
heures	O
)	O
,	O
c'	O
est	O
le	O
film	O
de	O
Ken	PERSON
Burns	PERSON
nommé	PERSON
The	O
War	O
,	O
un	O
documentaire	O
sur	O
la	O
Seconde	O
Guerre	O
mondiale	O
durant	O
14	O
heures	O
,	O
qui	O
établit	O
un	O
nouveau	O
record	O
.	O
Depuis	PERSON
soixante	PERSON
ans	PERSON
,	O
le	O
Festival	O
de	O
Cannes	O
a	O
toujours	O
innové	O
et	O
mis	O
en	O
lumière	O
des	O
réalisateurs	O
ou	O
des	O
cinématographies	O
.	O
De	O
La	O
Bataille	O
du	O
rail	O
à	O
Indigènes	O
,	O
de	PERSON
Michèle	PERSON
Morgan	PERSON
à	O
Penélope	PERSON
Cruz	PERSON
,	O
l'	O
histoire	O
du	O
cinéma	O
a	O
été	O
écrite	O
sur	O
un	O
tapis	O
rouge	O
.	O
Le	O
Festival	O
de	O
Cannes	O
comprend	O
deux	O
grandes	O
sections	O
qui	PERSON
englobent	PERSON
plusieurs	PERSON
sous-parties	PERSON
,	O
dont	O
la	O
plus	O
ancienne	O
est	O
La	O
Semaine	O
de	O
la	O
critique	O
,	O
créée	O
en	O
1962	O
.	O
Les	O
bobines	LOCATION
sont	O
cachées	O
dans	O
des	O
boîtes	O
sur	O
lesquelles	O
est	O
inscrit	O
un	O
faux	O
titre	O
,	O
ce	O
qui	O
permet	O
au	O
film	O
de	O
passer	O
la	O
frontière	O
et	O
de	O
concourir	O
au	O
Festival	O
de	O
Cannes	O
.	O
L'	O
Homme	O
de	O
fer	O
,	O
encore	O
de	PERSON
Andrzej	PERSON
Wajda	PERSON
,	O
a	O
même	O
été	O
sélectionné	O
alors	O
que	O
la	O
compétition	PERSON
avait	PERSON
déjà	PERSON
commencé	PERSON
.	O
De	O
l'	O
aveu	O
de	PERSON
Gilles	PERSON
Jacob	PERSON
dans	PERSON
une	PERSON
interview	O
au	O
magazine	O
Studio	O
,	O
"	O
c'	O
est	O
le	O
seul	O
cas	O
de	O
figure	O
où	O
un	PERSON
autre	PERSON
candidat	PERSON
aurait	PERSON
pu	PERSON
protester	O
"	O
.	O
L'	O
un	O
,	O
créé	PERSON
par	PERSON
Gilles	PERSON
Jacob	PERSON
,	O
et	PERSON
dirigé	PERSON
aujourd'hui	PERSON
par	PERSON
Thierry	PERSON
Frémaux	PERSON
,	O
qui	PERSON
choisit	PERSON
les	PERSON
films	O
étrangers	O
.	O
Ce	O
comité	O
est	O
composé	O
de	O
quatre	O
membres	O
:	O
un	O
journaliste	O
,	O
un	PERSON
réalisateur	PERSON
,	O
un	O
cinéphile	O
et	O
Laurent	PERSON
Jacob	PERSON
,	O
le	O
fils	O
de	O
Gilles	O
Jacob	O
.	O
Dans	O
les	O
années	O
1960	O
,	O
le	O
Prix	O
Spécial	O
du	O
Jury	O
fait	O
son	O
apparition	O
en	O
complément	O
du	O
Prix	O
du	O
Jury	O
et	O
change	O
régulièrement	O
d'	O
intitulé	O
,	O
entre	O
"	O
Grand	O
Prix	O
Spécial	O
du	O
Jury	O
"	O
et	O
"	O
Grand	O
Prix	O
du	O
Jury	O
"	O
avant	O
d'	O
être	O
finalement	O
"	O
Grand	O
Prix	O
"	O
.	O
Mais	PERSON
cette	PERSON
définition	PERSON
ne	PERSON
fut	PERSON
jamais	PERSON
comprise	PERSON
,	O
ni	O
vraiment	O
appliquée	O
par	O
les	O
jurys	O
successifs	O
et	O
cette	O
distinction	O
est	O
aujourd'hui	O
définitivement	O
perçue	O
comme	O
inférieure	O
à	O
la	O
Palme	O
d'	O
or	O
même	PERSON
si	PERSON
elle	PERSON
reste	PERSON
l'	O
honneur	O
le	O
plus	O
important	O
après	PERSON
celle-ci	PERSON
.	O
Le	O
deuxième	O
prix	O
le	O
plus	O
prestigieux	LOCATION
est	O
donc	O
le	O
Grand	O
Prix	O
.	O
Le	O
président	O
du	O
jury	O
Patrice	PERSON
Chéreau	PERSON
avait	PERSON
en	PERSON
effet	PERSON
,	O
à	LOCATION
ce	LOCATION
moment-là	LOCATION
,	O
réclamé	O
une	O
"	O
violation	O
exceptionnelle	O
du	O
règlement	O
"	O
qu'	O
il	O
s'	O
était	O
vu	O
accorder	O
et	O
trois	O
films	O
étaient	PERSON
repartis	PERSON
avec	PERSON
deux	PERSON
trophées	PERSON
chacun	PERSON
dont	O
Elephant	O
de	PERSON
Gus	PERSON
van	PERSON
Sant	PERSON
,	O
auréolé	O
de	O
la	O
Palme	O
d'	O
or	O
et	O
du	O
Prix	O
de	O
la	O
mise	O
en	O
scène	O
.	O
Depuis	O
,	O
pour	O
assurer	O
un	O
certain	O
équilibre	O
du	PERSON
palmarès	PERSON
,	O
le	O
règlement	O
s'	O
est	O
durci	O
:	O
le	O
jury	O
n'	O
a	O
droit	O
qu'	O
à	O
une	O
seule	O
mention	O
ex	O
æquo	PERSON
pour	O
un	O
prix	O
et	O
cette	O
disposition	O
ne	PERSON
peut	PERSON
plus	PERSON
s'	PERSON
appliquer	O
à	O
la	O
Palme	O
d'	O
or	O
.	O
De	O
plus	O
,	O
une	O
œuvre	O
ne	O
peut	O
obtenir	O
qu'	O
une	O
seule	O
récompense	O
au	O
palmarès	PERSON
même	PERSON
si	PERSON
le	PERSON
film	O
lauréat	O
du	O
Prix	O
du	O
jury	O
ou	O
du	O
Prix	O
du	O
scénario	O
peut	O
éventuellement	O
,	O
et	O
uniquement	O
sur	O
autorisation	O
du	LOCATION
président	LOCATION
du	LOCATION
festival	LOCATION
,	O
recevoir	O
l'	O
un	O
des	O
deux	O
prix	O
d'	O
interprétation	O
en	O
complément	O
.	O
Le	O
Festival	O
de	O
Cannes	O
est	O
dirigé	O
par	O
plusieurs	O
personnes	O
,	O
aux	O
postes	O
très	O
différents	O
.	O
Bien	PERSON
qu'	O
organisé	O
en	O
France	LOCATION
,	O
le	O
Festival	O
de	O
Cannes	O
ne	O
privilégie	O
pas	O
pour	O
autant	O
le	O
cinéma	O
français	O
.	O
D'	O
ailleurs	O
la	O
France	LOCATION
est	O
le	O
quatrième	O
pays	O
dans	O
le	O
classement	O
du	O
nombre	O
de	O
lauréats	O
de	O
la	O
Palme	O
.	O
Pour	O
éviter	O
une	O
surcharge	O
de	O
travail	O
,	O
Gilles	PERSON
Jacob	PERSON
créera	PERSON
la	PERSON
même	PERSON
année	PERSON
un	PERSON
comité	PERSON
de	PERSON
sélection	PERSON
dédié	PERSON
aux	PERSON
films	PERSON
français	O
dont	O
il	PERSON
choisit	PERSON
lui-même	PERSON
les	O
conseillers	O
et	O
dont	O
le	O
nombre	O
n'	O
est	O
pas	O
prédéfini	O
.	O
On	O
remarque	O
notamment	O
Nanni	PERSON
Moretti	PERSON
,	O
Oliver	PERSON
Stone	PERSON
,	O
Stephen	PERSON
Frears	PERSON
,	O
Francesco	PERSON
Rosi	PERSON
,	O
Wong	PERSON
Kar-wai	PERSON
,	O
Martin	PERSON
Scorsese	PERSON
et	O
Sydney	PERSON
Pollack	PERSON
.	O
Des	PERSON
leçons	PERSON
de	PERSON
cinéma	PERSON
de	PERSON
cette	PERSON
envergure	PERSON
n'	PERSON
ont	O
jamais	O
été	O
présentées	O
dans	O
des	O
festivals	O
internationaux	LOCATION
auparavant	LOCATION
,	O
comme	O
à	O
la	O
Mostra	O
de	O
Venise	O
ou	O
durant	O
la	O
Berlinale	LOCATION
,	O
il	PERSON
aura	O
fallu	O
attendre	O
l'	O
idée	O
de	O
Gilles	O
Jacob	O
pour	O
pouvoir	O
y	O
participer	O
.	O
Se	PERSON
sont	PERSON
succédé	PERSON
Nicola	PERSON
Piovani	PERSON
,	O
Howard	PERSON
Shore	PERSON
et	O
Alexandre	PERSON
Desplat	PERSON
par	O
exemple	O
.	O
On	O
a	O
déjà	O
retrouvé	O
Catherine	PERSON
Deneuve	PERSON
,	O
Max	PERSON
Von	PERSON
Sydow	PERSON
et	O
Gena	PERSON
Rowlands	PERSON
.	O
C'	O
est	O
sur	O
le	O
ton	O
de	O
la	O
confession	O
intime	O
qu'	O
un	O
échange	O
unique	O
dans	O
le	O
monde	O
a	O
été	O
mis	O
en	O
place	O
par	O
Gilles	PERSON
Jacob	PERSON
.	O
La	O
Croisette	O
est	O
envahie	O
par	O
plus	O
de	O
4500	O
journalistes	O
.	O
D'	O
ailleurs	O
,	O
Marina	PERSON
Vlady	PERSON
avouera	O
dans	O
le	PERSON
documentaire	PERSON
Cannes	PERSON
,	O
60	O
ans	O
d'	O
histoire	O
qu'	O
avant	O
les	O
starlettes	O
venaient	O
à	O
Cannes	LOCATION
pour	O
un	O
rendez-vous	O
d'	O
amour	O
et	O
d'	O
amitié	LOCATION
,	O
que	O
les	O
célébrités	O
pouvaient	O
parler	O
aux	O
passants	O
dans	O
la	O
rue	O
.	O
L'	O
actrice	O
dira	PERSON
que	PERSON
le	O
Festival	O
de	O
Cannes	O
vient	O
de	O
perdre	O
un	O
rapport	O
social	O
.	O
Les	O
hommes	O
sont	O
tenus	O
traditionnellement	O
au	O
smoking	O
et	O
les	O
femmes	O
à	O
une	O
robe	O
de	O
soirée	O
,	O
souvent	O
signée	O
par	O
des	O
couturiers	O
de	PERSON
renommée	PERSON
mondiale	PERSON
,	O
ce	O
qui	O
n'	O
a	O
pas	O
empêché	O
Pablo	PERSON
Picasso	PERSON
de	PERSON
monter	O
les	O
marches	O
avec	O
une	O
veste	O
en	O
peau	PERSON
de	PERSON
mouton	PERSON
lors	PERSON
du	PERSON
Festival	PERSON
1953	O
.	O
Cet	O
évènement	O
est	O
organisé	O
en	O
collaboration	O
entre	O
la	O
mairie	O
de	O
Cannes	O
et	O
le	O
Festival	O
de	O
Cannes	O
.	O
Parmi	O
les	O
célébrités	O
exposées	O
,	O
on	O
retrouve	PERSON
Samuel	PERSON
L.	PERSON
Jackson	PERSON
,	O
Elijah	PERSON
Wood	PERSON
,	O
Rossy	O
de	O
Palma	O
,	O
Kevin	PERSON
Bacon	PERSON
ou	PERSON
encore	O
Maïwenn	PERSON
Le	PERSON
Besco	PERSON
.	O
Il	O
se	O
déroule	O
sur	O
douze	O
jours	O
pendant	O
le	O
Festival	O
de	O
Cannes	O
.	O
D'	O
ailleurs	O
,	O
Penélope	PERSON
Cruz	PERSON
qui	PERSON
avait	PERSON
remporté	PERSON
le	PERSON
Prix	PERSON
d'	O
interprétation	O
féminine	O
pour	O
Volver	O
avait	O
été	O
critiquée	O
pour	O
son	PERSON
parrainage	PERSON
avec	PERSON
ce	PERSON
groupe	PERSON
industriel	PERSON
.	PERSON
De	O
multiples	O
journaux	O
de	O
presse	O
écrite	O
sont	PERSON
aussi	PERSON
présent	PERSON
sur	PERSON
la	PERSON
Croisette	PERSON
,	O
comme	O
Paris	O
Match	O
,	O
ou	O
Le	O
Monde	O
.	O
On	O
retrouve	O
de	O
même	O
des	O
magazines	O
de	O
cinéma	O
avec	O
Première	O
,	O
ou	O
Ciné	O
Live	O
.	O
Le	O
transport	O
des	O
célébrités	O
dans	O
Cannes	O
est	O
très	O
prisé	O
.	O
Cette	PERSON
robe	PERSON
avait	PERSON
été	PERSON
spécialement	PERSON
créée	PERSON
pour	O
elle	O
par	O
Karl	PERSON
Lagerfeld	PERSON
.	O
Voici	O
les	O
propos	O
du	O
groupe	O
Maxell	O
:	O
"	O
Ce	O
sponsoring	O
est	O
une	O
excellente	O
opportunité	O
pour	O
Maxell	O
,	O
le	O
Festival	O
de	O
Cannes	O
est	O
un	O
évènement	O
planétaire	O
,	O
diffusé	O
dans	O
le	O
monde	O
entier	O
,	O
et	O
qui	O
jouit	O
d'	O
une	O
reconnaissance	O
importante	O
"	O
.	O
Le	O
Festival	O
de	O
Cannes	O
est	O
devenu	O
au	O
long	O
des	O
années	O
l'	O
une	O
des	O
plus	O
importantes	O
cérémonies	O
de	O
cinéma	O
au	O
monde	O
.	O
La	LOCATION
ville	LOCATION
est	LOCATION
entièrement	PERSON
rénovée	PERSON
pour	O
le	O
Festival	O
international	O
du	O
film	O
qui	O
rend	O
Cannes	O
rayonnante	O
.	O
Parmi	O
tous	O
les	O
cinéastes	O
en	O
compétition	O
à	O
Cannes	O
,	O
quelques-uns	O
d'	O
entre	O
eux	O
ont	O
été	O
privilégiés	O
pour	O
leur	O
art	O
,	O
leur	O
style	O
,	O
ou	O
leur	O
genre	O
.	O
On	O
retrouve	PERSON
Souleymane	PERSON
Cissé	PERSON
,	O
Penélope	PERSON
Cruz	PERSON
,	O
Wong	PERSON
Kar-Wai	PERSON
,	O
Juliette	PERSON
Binoche	PERSON
,	O
Jane	PERSON
Campion	PERSON
,	O
Gérard	PERSON
Depardieu	PERSON
,	O
Bruce	PERSON
Willis	PERSON
,	O
Samuel	PERSON
L.	PERSON
Jackson	PERSON
,	O
et	O
Pedro	PERSON
Almodóvar	PERSON
.	O
Certains	O
réalisateurs	O
ont	O
obtenu	O
deux	O
Palmes	O
d'	O
or	O
:	O
Francis	PERSON
Ford	PERSON
Coppola	PERSON
(	O
avec	O
Conversation	O
secrète	O
et	O
Apocalypse	O
Now	O
--	O
partagé	O
avec	O
un	O
autre	O
film	O
dans	O
le	O
deuxième	O
cas	O
)	O
;	O
Bille	O
August	O
(	O
avec	PERSON
Pelle	PERSON
le	PERSON
conquérant	O
et	O
Les	O
Meilleures	O
Intentions	O
)	O
;	O
Shōhei	PERSON
Imamura	PERSON
(	O
avec	O
La	O
Ballade	O
de	O
Narayama	O
et	O
L'	O
Anguille	O
--	O
partagé	O
avec	O
un	O
autre	O
film	O
dans	O
le	O
deuxième	O
cas	O
)	O
;	O
Emir	PERSON
Kusturica	PERSON
avec	PERSON
(	O
Papa	O
est	O
en	O
voyage	O
d'	O
affaires	O
et	O
Underground	O
)	O
;	O
Luc	PERSON
et	PERSON
Jean-Pierre	PERSON
Dardenne	PERSON
avec	PERSON
(	O
Rosetta	O
et	O
L'	O
Enfant	O
)	O
.	O
Le	PERSON
cinéaste	PERSON
suédois	O
Alf	PERSON
Sjöberg	PERSON
avait	O
aussi	PERSON
reçu	PERSON
la	PERSON
récompense	O
suprême	PERSON
du	PERSON
festival	PERSON
à	O
deux	O
reprises	O
mais	PERSON
il	PERSON
ne	PERSON
s'	PERSON
agissait	O
pas	O
encore	O
de	O
la	O
Palme	O
d'	O
or	O
à	O
l'	O
époque	O
.	O
En	O
outre	O
,	O
plusieurs	O
cinéastes	O
sont	O
des	O
habitués	O
du	O
palmarès	O
même	O
s'	O
ils	O
n'	O
ont	O
jamais	O
gagné	O
la	O
Palme	O
d'	O
or	O
comme	O
Robert	PERSON
Bresson	PERSON
ou	PERSON
encore	O
Andreï	O
Tarkovski	O
qui	O
a	O
obtenu	O
trois	O
fois	O
le	O
Grand	O
Prix	O
,	O
à	O
savoir	O
pour	O
Solaris	O
,	O
Nostalghia	LOCATION
et	O
Le	O
Sacrifice	O
.	O
Bruno	PERSON
Dumont	PERSON
,	O
lui	O
,	O
l'	O
a	O
remporté	O
deux	O
fois	O
(	O
pour	O
L'	O
humanité	O
et	O
Flandres	O
)	O
et	O
a	O
également	O
reçu	O
une	O
mention	O
spéciale	O
Caméra	O
d'	O
or	O
pour	O
la	O
Vie	O
de	O
Jésus	O
,	O
son	O
premier	O
long	O
métrage	O
.	O
Peu	O
d'	O
acteurs	O
ont	O
obtenu	O
deux	O
Prix	O
d'	O
interprétation	O
masculine	O
:	O
Marcello	PERSON
Mastroianni	PERSON
(	O
pour	O
son	O
rôle	O
dans	O
Drame	O
de	O
la	O
jalousie	O
et	O
Les	PERSON
Yeux	PERSON
noirs	O
)	O
;	O
Dean	PERSON
Stockwell	PERSON
(	O
pour	O
son	O
rôle	PERSON
dans	PERSON
Le	PERSON
Génie	PERSON
du	PERSON
mal	PERSON
et	O
Long	O
voyage	O
vers	O
la	O
nuit	O
--	O
à	O
chaque	O
fois	O
partagé	O
avec	O
deux	O
autres	O
acteurs	O
)	O
;	O
Jack	PERSON
Lemmon	PERSON
(	O
pour	O
son	O
rôle	PERSON
dans	PERSON
Missing	O
et	O
Le	O
Syndrome	O
chinois	O
)	O
.	O
Des	O
actrices	O
ont	O
également	O
obtenu	O
deux	O
Prix	O
d'	O
interprétation	O
féminine	O
:	O
Isabelle	PERSON
Huppert	PERSON
(	O
pour	O
son	O
rôle	PERSON
dans	O
Violette	PERSON
Nozière	PERSON
et	PERSON
La	PERSON
Pianiste	PERSON
--	O
partagé	O
avec	O
une	O
autre	O
actrice	O
dans	O
le	O
premier	O
cas	O
)	O
;	O
Helen	PERSON
Mirren	PERSON
(	O
pour	O
son	O
rôle	O
dans	O
Cal	PERSON
et	PERSON
La	PERSON
Folie	PERSON
du	PERSON
roi	PERSON
George	PERSON
)	O
;	O
Barbara	PERSON
Hershey	PERSON
(	O
pour	O
son	O
rôle	O
dans	O
Le	O
Bayou	O
et	O
Un	O
monde	O
à	O
part	O
--	O
partagé	O
avec	O
deux	O
autres	O
actrices	O
dans	O
le	O
deuxième	O
cas	O
)	O
;	O
Vanessa	PERSON
Redgrave	PERSON
(	O
pour	O
son	O
rôle	PERSON
dans	PERSON
Morgan	O
et	O
Isadora	O
)	O
.	O
À	O
noter	O
qu'	O
Isabelle	PERSON
Huppert	PERSON
détient	O
le	O
record	O
de	O
films	O
en	O
sélection	O
officielle	O
pour	O
une	O
comédienne	O
(	O
17	O
au	O
total	O
)	O
.	O
Certains	O
cinéastes	O
ont	O
souvent	O
vu	O
leurs	O
films	O
sélectionnés	O
par	O
le	O
Festival	O
de	O
Cannes	O
.	O
On	O
peut	O
citer	O
Federico	PERSON
Fellini	PERSON
et	O
Carlos	PERSON
Saura	PERSON
(	O
avec	O
11	O
films	O
sélectionnés	PERSON
)	O
;	O
Ingmar	PERSON
Bergman	PERSON
et	O
André	PERSON
Téchiné	PERSON
(	O
avec	O
10	O
films	O
sélectionnés	O
)	O
;	O
Wim	PERSON
Wenders	PERSON
,	O
Luis	PERSON
Buñuel	PERSON
,	O
Michael	PERSON
Cacoyannis	PERSON
,	O
Ettore	PERSON
Scola	PERSON
et	O
Andrzej	PERSON
Wajda	PERSON
(	O
avec	O
9	O
films	O
sélectionnés	O
)	O
et	O
enfin	O
Claude	PERSON
Lelouch	PERSON
(	O
avec	O
7	O
films	O
sélectionnés	O
)	O
.	O
"	O
Cannes	O
"	O
évoque	O
les	O
célébrités	O
du	O
monde	O
du	O
cinéma	O
et	O
la	O
montée	O
des	O
marches	O
,	O
représentant	O
simplement	O
la	PERSON
cérémonie	PERSON
.	O
Le	O
Festival	O
de	O
Venise	O
ou	O
le	O
Festival	O
de	O
Berlin	O
interpellent	O
le	O
public	O
pour	O
d'	O
autres	O
éléments	O
que	O
les	O
films	O
en	O
compétition	O
.	O
Ce	O
qui	O
comptait	O
vraiment	O
c'	O
était	O
les	O
réceptions	O
et	O
les	O
fêtes	O
dans	O
les	O
villas	O
de	O
la	O
Côte	O
d'	O
Azur	O
.	O
Les	O
soirées	O
organisées	O
depuis	O
le	O
début	O
des	O
années	O
2000	O
dans	O
les	O
franchises	O
éphémères	PERSON
des	PERSON
plus	O
grandes	O
discothèques	O
du	O
monde	O
ont	O
néanmoins	O
donné	O
un	O
sérieux	O
coup	O
de	O
frein	O
à	O
l'	O
esprit	O
des	O
fêtes	O
organisées	O
dans	O
des	LOCATION
villas	LOCATION
sur	LOCATION
les	O
collines	O
environnantes	O
de	PERSON
Cannes	PERSON
.	O
Le	O
Festival	O
de	O
Cannes	O
a	O
souvent	O
été	O
attaqué	O
par	O
la	O
presse	O
.	O
Certains	O
grands	O
réalisateurs	O
n'	O
ont	O
jamais	O
remporté	O
la	O
Palme	O
d'	O
or	O
,	O
malgré	LOCATION
le	LOCATION
talent	O
dont	O
ils	O
ont	O
pu	O
faire	O
preuve	O
et	O
la	O
place	O
majeure	O
qu'	O
ils	O
occupent	O
par	O
ailleurs	O
dans	O
l'	O
histoire	O
du	O
7	O
e	O
art	O
.	O
Woody	PERSON
Allen	PERSON
,	O
qui	O
rejette	O
catégoriquement	O
la	O
mise	O
en	O
concurrence	O
des	O
artistes	O
a	O
,	O
lui	O
,	O
toujours	PERSON
refusé	O
les	O
égards	O
de	O
la	O
compétition	PERSON
cannoise	PERSON
.	O
On	O
peut	PERSON
aussi	PERSON
citer	PERSON
,	O
dans	O
la	O
liste	O
des	O
non	O
palmés	O
,	O
Alfred	PERSON
Hitchcock	PERSON
,	O
Stanley	PERSON
Kubrick	PERSON
,	O
Andreï	PERSON
Tarkovski	PERSON
,	O
Michael	PERSON
Powell	PERSON
,	O
Clint	PERSON
Eastwood	PERSON
,	O
Steven	PERSON
Spielberg	PERSON
,	O
Satyajit	PERSON
Ray	PERSON
,	O
Sergio	PERSON
Leone	PERSON
,	O
John	PERSON
Cassavetes	PERSON
,	O
Douglas	PERSON
Sirk	PERSON
,	O
Claude	PERSON
Sautet	PERSON
,	O
Bertrand	PERSON
Tavernier	PERSON
,	O
André	PERSON
Téchiné	PERSON
,	O
François	PERSON
Truffaut	PERSON
,	O
Alain	PERSON
Resnais	PERSON
ou	O
encore	O
Pedro	PERSON
Almodovar	PERSON
qui	PERSON
,	O
malgré	O
plusieurs	O
sélections	O
et	O
un	O
statut	O
de	O
favori	O
,	O
n'	O
est	O
reparti	O
qu'	O
avec	O
des	O
prix	O
subsidiaires	LOCATION
.	O
La	O
Palme	O
d'	O
or	O
est	O
très	PERSON
majoritairement	PERSON
allée	PERSON
à	O
des	O
films	O
occidentaux	O
.	O
Sur	O
62	O
éditions	O
,	O
seules	O
neuf	O
d'	O
entre	O
elles	O
ont	O
eu	O
cette	O
chance	O
:	O
Olivia	PERSON
de	PERSON
Havilland	PERSON
,	O
Sofia	PERSON
Loren	PERSON
,	O
Michèle	PERSON
Morgan	PERSON
,	O
Ingrid	PERSON
Bergman	PERSON
,	O
Jeanne	PERSON
Moreau	PERSON
(	O
qui	O
a	O
été	O
deux	PERSON
fois	PERSON
présidente	PERSON
)	O
,	O
Françoise	PERSON
Sagan	PERSON
,	O
Isabelle	PERSON
Adjani	PERSON
,	O
Liv	PERSON
Ullman	PERSON
et	O
Isabelle	PERSON
Huppert	PERSON
.	O
Certains	O
notent	O
aussi	PERSON
que	PERSON
les	PERSON
films	PERSON
de	PERSON
genre	PERSON
sont	PERSON
très	PERSON
peu	PERSON
représentés	PERSON
à	PERSON
Cannes	O
:	O
peu	O
ou	O
pas	O
de	O
films	O
d'	O
horreur	O
,	O
de	PERSON
kung-fu	PERSON
,	O
etc	O
.	O
Un	O
journal	O
écrira	PERSON
même	PERSON
:	O
"	O
Où	O
va	O
le	O
Festival	O
de	O
Cannes	O
?	O
"	O
à	O
propos	O
de	O
ce	O
nouveau	O
style	O
.	O
Avec	O
l'	O
émergence	O
du	O
cinéma	O
d'	O
animation	O
,	O
ou	O
du	O
film	O
documentaire	O
,	O
le	O
Festival	O
de	O
Cannes	O
devait	O
se	O
mettre	PERSON
à	PERSON
jour	O
.	O
2009	O
semble	O
avoir	O
marqué	O
un	O
renouveau	O
puisque	O
la	O
sélection	O
a	O
fait	O
la	O
part	O
belle	O
aux	O
films	O
de	O
genre	O
revisités	O
.	O
Cannes	O
donne	O
avant	O
tout	O
aux	O
auteurs	O
une	O
crédibilité	O
artistique	O
.	O
À	O
l'	O
étranger	O
,	O
Cannes	LOCATION
est	O
idéalisée	O
.	O
Ainsi	O
,	O
beaucoup	O
de	O
réalisateurs	O
encore	O
méconnus	O
ont	O
pu	O
bénéficier	O
d'	O
une	O
audience	O
internationale	O
grâce	O
à	O
Cannes	O
et	O
jouir	O
d'	O
une	O
reconnaissance	O
de	O
leur	O
travail	O
.	O
Le	O
festival	O
joue	O
le	O
rôle	O
de	O
tremplin	O
et	O
a	O
lancé	O
plusieurs	O
carrières	O
dont	O
celle	O
de	PERSON
Quentin	PERSON
Tarantino	PERSON
avec	PERSON
la	PERSON
sélection	PERSON
en	O
1992	O
de	O
son	O
premier	O
long-métrage	O
Reservoir	LOCATION
dogs	O
en	O
séance	PERSON
spéciale	PERSON
puis	PERSON
avec	PERSON
la	PERSON
Palme	PERSON
d'	PERSON
or	O
décernée	O
à	O
Pulp	O
fiction	O
deux	O
ans	O
plus	O
tard	O
.	O
Très	O
reconnaissant	O
,	O
le	O
réalisateur	O
déclare	O
d'	O
ailleurs	O
:	O
"	O
Cannes	O
m'	O
a	O
fait	O
gagner	O
dix	O
ans.	O
"	O
.	O
La	O
presse	O
est	O
un	O
pilier	O
central	O
du	O
Festival	O
de	O
Cannes	O
et	O
elle	O
s'	O
attend	O
à	O
voir	O
les	O
chefs	O
d'	O
œuvre	O
qui	O
font	O
vibrer	O
le	O
monde	O
.	O
Si	PERSON
Cannes	PERSON
la	PERSON
déçoit	O
,	O
elle	PERSON
attaque	PERSON
.	O
Depuis	O
les	O
années	O
2000	O
,	O
certains	O
reprochent	O
au	O
Festival	O
de	O
Cannes	O
un	O
mélange	O
des	O
genres	O
contre-productif	O
,	O
à	O
savoir	O
de	O
mettre	O
sur	O
un	O
même	O
plan	O
les	O
paillettes	O
et	O
le	O
cinéma	O
d'	O
auteur	O
puis	O
d'	O
autres	O
déplorent	O
le	O
fait	O
qu'	O
il	O
n'	O
invite	O
que	O
des	O
personnalités	O
internationales	O
dont	O
la	O
renommée	PERSON
est	PERSON
acquise	PERSON
et	PERSON
qui	PERSON
n'	PERSON
ont	O
fondamentalement	O
pas	O
besoin	O
des	O
lumières	O
de	O
la	O
sélection	O
officielle	O
.	O
Certains	O
lui	O
accorderont	O
le	O
rang	O
de	O
publicitaire	O
aux	O
blockbusters	O
,	O
par	O
exemple	O
avec	O
la	O
fin	O
de	O
la	O
saga	O
Star	O
Wars	O
:	O
épisode	O
III	O
--	O
La	O
Revanche	O
des	O
Sith	O
.	O
L'	O
intérêt	O
pour	O
les	O
super-productions	O
des	O
journalistes	O
est	O
aussi	PERSON
déçu	PERSON
par	O
le	O
nombre	O
d'	O
entrées	O
des	O
films	O
ayant	O
reçu	O
la	O
Palme	O
d'	O
or	O
:	O
depuis	PERSON
vingt	PERSON
ans	PERSON
,	O
seuls	PERSON
cinq	PERSON
lauréats	PERSON
de	PERSON
la	PERSON
palme	PERSON
ont	O
dépassé	O
le	O
million	O
d'	O
entrées	O
en	O
France	LOCATION
.	O
Cannes	O
est	O
devenu	O
depuis	O
quelques	O
années	O
un	O
festival	O
pour	O
les	O
grands	O
auteurs	O
.	O
Cannes	LOCATION
,	O
en	O
ce	O
sens	O
,	O
sous	PERSON
couvert	PERSON
de	PERSON
défendre	PERSON
une	PERSON
forme	O
cinématographique	O
originale	O
et	O
avant-gardiste	O
,	O
deviendrait	O
le	O
temple	O
des	O
nouveaux	O
académismes	O
.	O
Le	O
Festival	O
de	O
Cannes	O
a	O
souvent	O
été	O
animé	O
par	O
des	O
scandales	O
et	O
des	O
controverses	O
,	O
impliquant	O
indifféremment	O
des	O
journalistes	O
,	O
des	O
célébrités	O
ou	O
le	O
monde	O
politique	O
.	O
D'	O
autres	O
festivals	O
internationaux	O
comme	O
la	O
Mostra	O
de	PERSON
Venise	PERSON
ou	PERSON
la	PERSON
Berlinale	O
semblent	O
moins	O
exposés	O
à	O
ce	PERSON
phénomène	PERSON
,	O
peut-être	O
en	O
partie	O
parce	O
qu'	O
ils	O
sont	O
moins	O
médiatisés	O
.	O
En	O
effet	O
,	O
si	O
certains	O
professionnels	O
du	O
cinéma	O
essaient	O
ostensiblement	O
d'	O
éviter	O
les	O
photographes	O
,	O
il	O
n'	O
est	O
pas	O
impensable	O
que	O
d'	O
autres	PERSON
cherchent	PERSON
à	PERSON
faire	O
évènement	O
,	O
sinon	LOCATION
scandale	LOCATION
,	O
pour	O
tirer	O
profit	O
de	O
la	O
grande	O
concentration	O
de	PERSON
médias	PERSON
durant	PERSON
le	PERSON
Festival	PERSON
de	PERSON
Cannes	PERSON
.	O
Ce	O
film	O
fut	O
hué	O
par	O
le	O
public	O
lors	O
de	O
sa	O
projection	O
car	O
l'	O
absence	O
d'	O
éclaircissement	O
sur	O
la	O
disparition	O
d'	O
Anna	PERSON
avait	PERSON
été	PERSON
mal	PERSON
comprise	O
.	O
Maurice	PERSON
Pialat	PERSON
,	O
qui	O
avait	O
été	O
encouragé	O
par	O
Souleymane	PERSON
Cissé	PERSON
lorsque	PERSON
le	PERSON
public	O
huait	O
son	O
film	O
,	O
s'	O
interposa	O
.	O
Ce	O
fut	O
la	O
première	O
fois	O
que	O
deux	O
réalisateurs	O
présents	O
au	O
Festival	O
de	O
Cannes	O
s'	O
unissaient	O
contre	O
un	O
membre	O
du	O
public	O
.	O
L'	O
écrivain	O
Françoise	PERSON
Sagan	PERSON
fut	O
l'	O
actrice	O
d'	O
un	PERSON
tel	PERSON
scandale	O
.	O
Françoise	PERSON
Sagan	PERSON
ne	PERSON
tint	PERSON
donc	PERSON
pas	O
promesse	O
.	O
Finalement	PERSON
,	O
les	O
deux	O
films	O
partagèrent	O
la	O
Palme	O
d'	O
or	O
ex	O
æquo	O
.	O
Ce	O
dernier	O
répondit	O
à	O
ces	O
provocations	O
en	O
rendant	O
publiques	O
les	O
notes	O
de	O
frais	O
faramineuses	O
laissées	O
par	O
Sagan	O
.	O
Lors	O
du	O
Festival	O
de	O
Cannes	O
1987	O
,	O
le	LOCATION
long	LOCATION
métrage	O
Les	PERSON
Yeux	PERSON
noirs	O
de	PERSON
Nikita	PERSON
Mikhalkov	PERSON
était	PERSON
favori	O
.	O
Le	O
jury	O
aurait	O
alors	PERSON
cédé	PERSON
à	PERSON
cette	O
exigence	O
impérieuse	O
,	O
et	PERSON
décerna	PERSON
la	PERSON
Palme	PERSON
d'	PERSON
or	O
à	O
Maurice	PERSON
Pialat	PERSON
,	O
le	LOCATION
réalisateur	LOCATION
de	LOCATION
Sous	LOCATION
le	O
soleil	O
de	O
Satan	O
.	O
En	O
1991	O
,	O
Roman	PERSON
Polanski	PERSON
n'	PERSON
aimait	O
aucun	O
film	O
et	O
privait	O
ses	PERSON
jurés	PERSON
de	PERSON
toute	PERSON
parole	PERSON
jusqu'	PERSON
à	O
ce	O
qu'	O
arrive	O
Barton	PERSON
Fink	PERSON
des	PERSON
frères	PERSON
Coen	PERSON
qu'	O
il	PERSON
adorait	PERSON
.	PERSON
La	O
veille	O
des	O
délibérations	O
,	O
il	O
fit	O
voter	O
la	O
Palme	O
d'	O
or	O
à	O
ses	O
collègues	O
après	O
les	O
avoir	O
fait	O
boire	O
et	O
refusa	O
toute	O
remise	O
en	O
cause	O
du	O
prix	O
le	O
lendemain	O
.	O
Barton	PERSON
Fink	PERSON
devint	O
ainsi	O
le	O
film	O
le	O
plus	O
primé	O
de	O
l'	O
histoire	O
du	O
festival	O
.	O
Gilles	PERSON
Jacob	PERSON
dut	PERSON
prendre	PERSON
des	PERSON
mesures	PERSON
pour	PERSON
éviter	O
qu'	O
un	PERSON
seul	PERSON
film	PERSON
ne	PERSON
gagne	PERSON
trop	PERSON
de	PERSON
récompenses	PERSON
.	O
Plus	O
récemment	O
,	O
les	O
critiques	O
ont	PERSON
accueilli	PERSON
avec	PERSON
circonspection	PERSON
la	PERSON
décision	PERSON
des	PERSON
présidents	PERSON
Quentin	PERSON
Tarantino	PERSON
(	O
en	O
2004	O
)	O
et	O
Isabelle	PERSON
Huppert	PERSON
(	O
en	O
2009	O
)	O
,	O
soupçonnés	O
de	O
partialité	O
dans	O
l'	O
attribution	O
de	O
la	O
Palme	O
d'	O
or	O
,	O
.	O
Le	O
premier	O
récompensa	O
Michael	PERSON
Moore	PERSON
pour	O
Fahrenheit	PERSON
9/11	O
(	O
les	O
deux	O
cinéastes	O
étaient	O
produits	O
par	O
Bob	PERSON
et	PERSON
Harvey	PERSON
Weinstein	PERSON
et	O
Moore	O
dénonçait	O
violemment	O
la	O
politique	O
de	PERSON
George	PERSON
W	PERSON
Bush	PERSON
avant	O
les	O
élections	O
présidentielles	O
de	O
2004	O
)	O
.	O
Kristin	PERSON
Scott-Thomas	PERSON
,	O
maîtresse	O
de	O
cérémonie	O
,	O
dut	PERSON
intervenir	O
.	O
Le	O
discours	O
erratique	O
de	O
Sophie	O
Marceau	O
pourrait	O
être	O
transcrit	O
ainsi	O
:	O
"	O
Plutôt	O
que	O
de	O
faire	O
la	O
guerre	O
,	O
on	O
fait	O
du	O
cinéma	O
et	O
je	O
vous	O
dis	O
que	O
ça	O
fait	O
rêver	O
les	O
gens	O
,	O
et	O
ça	O
leur	O
donne	O
un	O
…	O
un	O
but	O
,	O
un	O
projet	O
,	O
euh	O
…	O
à	O
court	O
terme	O
et	O
quelque	O
chose	O
qui	O
reste	O
pour	O
toujours	O
,	O
euh	O
…	O
"	O
.	O
L'	O
un	O
d'	O
entre	O
eux	O
,	O
Roman	PERSON
Polanski	PERSON
,	O
critiqua	O
les	O
journalistes	O
lors	O
de	O
la	O
conférence	O
de	O
presse	O
qui	O
les	O
réunissaient	O
après	O
la	O
projection	O
officielle	O
,	O
jugeant	O
que	O
les	O
questions	O
posées	O
n'	O
étaient	O
pas	O
à	O
la	O
hauteur	O
.	O
On	O
remarque	O
notamment	O
Jiang	PERSON
Wen	PERSON
qui	PERSON
,	O
pour	O
Les	PERSON
Démons	PERSON
à	O
ma	O
porte	O
sélectionné	O
en	O
compétition	O
et	O
récompensé	O
par	O
le	O
Grand	O
prix	O
du	O
jury	O
en	O
2000	O
,	O
a	O
été	O
interdit	O
de	O
tournage	O
durant	O
cinq	O
ans	O
en	O
Chine	LOCATION
.	O
En	O
revanche	O
,	O
les	O
Jésuites	O
défendirent	O
le	O
film	O
.	O
Le	O
Ministère	O
de	O
la	O
Culture	O
censurera	O
des	O
parties	O
du	O
film	O
.	O
Ce	O
long	O
métrage	O
ouvrit	O
le	O
Festival	O
de	O
Cannes	O
2006	O
,	O
bien	PERSON
qu'	PERSON
il	PERSON
fut	PERSON
critiqué	O
dans	O
le	O
monde	O
religieux	O
.	O
Des	O
associations	O
catholiques	O
menèrent	O
plusieurs	O
campagnes	O
contre	O
ce	O
film	O
en	O
détériorant	PERSON
par	PERSON
exemple	PERSON
les	PERSON
affiches	O
publicitaires	O
,	O
même	LOCATION
si	LOCATION
le	LOCATION
Vatican	O
condamnait	LOCATION
tout	O
boycott	O
et	O
action	O
contre	O
ce	O
long	O
métrage	O
:	O
il	O
disait	O
qu	O
'	O
"	O
il	O
y	O
avait	O
plus	O
important	O
à	O
faire	O
dans	O
le	O
monde	O
,	O
que	O
les	O
faits	O
du	O
film	O
étaient	O
faux	O
,	O
et	O
qu'	O
il	O
ne	O
servait	O
donc	PERSON
à	PERSON
rien	PERSON
de	PERSON
se	PERSON
défendre	PERSON
"	PERSON
.	O
Le	O
Festival	O
de	O
Cannes	O
a	O
souvent	O
eu	O
des	O
imprévus	O
,	O
des	O
controverses	O
.	O
Cette	PERSON
même	PERSON
année	PERSON
,	O
lors	O
de	O
la	O
première	O
projection	O
,	O
celle	O
du	O
film	O
d'	O
Alfred	PERSON
Hitchcock	PERSON
Les	PERSON
Enchaînés	PERSON
,	O
les	O
techniciens	O
mélangèrent	PERSON
les	O
bobines	O
de	O
pellicules	O
et	O
la	O
projection	O
fut	O
une	O
vraie	O
catastrophe	O
.	O
Lors	O
du	O
Festival	O
de	O
Cannes	O
1954	O
,	O
alors	O
que	O
le	O
festival	O
n'	O
en	O
était	O
qu'	O
à	O
sa	O
8	O
e	O
édition	O
,	O
Simone	PERSON
Sylva	PERSON
posait	O
avec	O
Robert	PERSON
Mitchum	PERSON
pour	O
des	O
photographes	O
.	O
Simone	PERSON
Sylva	PERSON
sera	PERSON
contrainte	PERSON
de	O
quitter	O
le	O
festival	O
.	O
Sans	LOCATION
le	LOCATION
vouloir	LOCATION
,	O
elle	PERSON
deviendra	PERSON
l'	PERSON
évènement	O
du	O
Festival	O
de	O
Cannes	O
2005	O
.	O
Le	O
Festival	O
de	O
Cannes	O
a	O
souvent	O
été	O
critiqué	O
.	O
On	O
verra	O
aussi	O
le	O
réalisateur	O
,	O
Marco	PERSON
Ferreri	PERSON
,	O
du	O
haut	O
d'	O
un	O
balcon	O
,	O
envoyer	O
des	O
baisers	O
aux	O
gens	O
qui	LOCATION
le	LOCATION
huent	LOCATION
,	O
avec	PERSON
la	PERSON
braguette	PERSON
ouverte	PERSON
.	O
Cette	PERSON
même	PERSON
année	PERSON
,	O
La	LOCATION
Maman	LOCATION
et	O
la	O
Putain	O
de	O
Jean	O
Eustache	O
provoque	O
également	O
une	O
forte	O
polémique	O
du	O
fait	O
de	O
ses	O
dialogues	O
crus	O
et	O
la	O
réception	O
par	O
le	O
réalisateur	O
du	O
Prix	O
spécial	O
du	O
jury	O
,	O
lors	O
de	O
la	O
cérémonie	O
de	O
clôture	O
,	O
sera	O
accompagnée	O
de	O
sifflets	O
.	O
Malgré	PERSON
de	PERSON
nombreux	PERSON
scandales	PERSON
qui	PERSON
ont	PERSON
fait	PERSON
le	PERSON
tour	PERSON
du	PERSON
monde	PERSON
,	O
le	O
Festival	O
de	O
Cannes	O
est	O
aussi	O
reconnu	O
pour	O
ses	O
moments	O
émouvants	O
,	O
voire	O
inoubliables	O
.	O
C'	O
est	O
à	O
ce	O
moment	O
qu'	O
il	O
rencontrera	O
la	O
jeune	PERSON
Grace	PERSON
Kelly	PERSON
.	O
Peu	PERSON
de	PERSON
temps	PERSON
après	PERSON
la	PERSON
mort	PERSON
de	PERSON
François	PERSON
Truffaut	PERSON
,	O
lors	O
du	O
Festival	O
de	O
Cannes	O
1985	O
,	O
ses	O
comédiens	O
principaux	O
se	O
réunissent	O
sur	O
scène	O
pour	O
un	O
dernier	O
hommage	O
et	O
une	O
photo	O
de	O
famille	O
.	O
Quelques	O
années	O
plus	O
tard	O
,	O
en	O
1989	O
,	O
les	O
enfants	O
et	O
petits	O
enfants	O
de	PERSON
Charlie	PERSON
Chaplin	PERSON
montent	PERSON
sur	O
scène	O
et	O
célèbrent	O
ainsi	O
le	O
centenaire	O
de	O
sa	O
naissance	O
.	O
Autre	PERSON
beau	PERSON
moment	PERSON
du	O
Festival	O
de	O
Cannes	O
:	O
la	O
remise	O
de	O
la	O
Palme	O
d'	O
or	O
,	O
avec	O
des	O
larmes	O
,	O
et	O
un	O
discours	O
raffiné	PERSON
du	PERSON
gagnant	PERSON
.	PERSON
Mais	PERSON
,	O
des	O
mains	O
du	O
président	O
de	PERSON
cérémonie	PERSON
,	O
Martin	PERSON
Scorsese	PERSON
,	O
il	PERSON
se	PERSON
voit	PERSON
remettre	PERSON
le	PERSON
Grand	O
Prix	O
du	O
jury	O
.	O
Le	O
Festival	O
de	O
Cannes	O
a	O
été	O
le	O
décor	O
,	O
voire	O
le	O
sujet	O
,	O
de	O
tout	O
ou	O
partie	O
des	O
films	O
suivants	O
:	O
