Le	PERSON
philosophe	PERSON
al-Farabi	PERSON
(	O
mort	O
en	O
950	O
)	O
,	O
le	O
second	O
maître	O
(	O
en	O
référence	O
au	O
premier	O
maître	O
,	O
Aristote	PERSON
)	O
,	O
tient	O
une	O
place	O
prépondérante	PERSON
dans	O
cette	O
dynamique	O
.	O
Avicenne	PERSON
se	PERSON
serait	PERSON
plus	O
tard	O
converti	O
au	O
chiisme	O
duodécimain	O
.	O
Il	PERSON
retient	PERSON
de	PERSON
mémoire	PERSON
l'	PERSON
intégralité	O
du	PERSON
Coran	PERSON
.	O
Il	PERSON
étudia	PERSON
à	PERSON
Boukhara	PERSON
,	O
s'	O
intéressant	O
à	O
toutes	O
les	O
sciences	O
,	O
et	O
surtout	O
à	O
la	O
médecine	O
.	O
Tout	O
alors	O
s'	O
enchaîne	O
:	O
ayant	PERSON
guéri	PERSON
le	PERSON
prince	PERSON
samanide	PERSON
de	PERSON
Boukhara	PERSON
,	O
Nuh	PERSON
ibn	PERSON
Mansûr	PERSON
,	O
d'	O
une	O
grave	O
maladie	PERSON
,	O
il	O
est	O
autorisé	O
à	O
consulter	O
la	O
vaste	O
bibliothèque	O
du	O
palais	O
.	O
Il	O
voyage	O
d'	O
abord	PERSON
dans	O
le	PERSON
Khârezm	PERSON
,	O
principauté	PERSON
qui	PERSON
fut	PERSON
indépendante	PERSON
(	PERSON
de	PERSON
994	PERSON
à	O
1231	O
)	O
au	O
sud	O
de	O
la	O
mer	O
d'	O
Aral	O
,	O
sur	O
les	O
deux	O
rives	O
du	O
Djihoun	O
(	O
Amou-daria	O
)	O
,	O
entre	O
Boukhara	PERSON
et	PERSON
la	PERSON
mer	PERSON
Caspienne	PERSON
.	O
Il	LOCATION
jouissait	LOCATION
d'	LOCATION
une	O
telle	O
réputation	O
que	O
plusieurs	O
princes	O
de	O
l'	O
Asie	O
l'	O
appelèrent	O
à	O
leur	O
cour	O
:	O
le	PERSON
roi	PERSON
de	PERSON
Perse	PERSON
l'	O
employa	PERSON
à	O
la	O
fois	O
comme	O
vizir	O
et	O
comme	O
médecin	O
.	O
Il	PERSON
cultiva	PERSON
aussi	PERSON
avec	PERSON
succès	PERSON
la	PERSON
philosophie	PERSON
,	O
et	O
fut	O
l'	O
un	O
des	O
premiers	O
à	O
étudier	O
et	O
à	O
faire	O
connaître	O
Aristote	O
.	O
Avicenne	PERSON
tenta	PERSON
de	PERSON
se	PERSON
soigner	O
de	PERSON
lui-même	PERSON
,	O
mais	O
son	O
remède	PERSON
lui	PERSON
fut	PERSON
fatal	O
.	O
La	O
religion	O
de	PERSON
la	PERSON
mère	PERSON
d'	PERSON
Avicenne	O
n'	O
est	O
connue	O
que	O
par	O
des	O
sources	O
secondaires	O
.	O
Si	O
l'	O
on	O
peut	PERSON
supposer	PERSON
en	O
première	O
approche	O
qu'	O
elle	O
est	O
musulmane	O
,	O
certaines	O
sources	O
indiquent	O
qu'	O
elle	O
était	O
juive	O
:	O
c'	O
est	O
le	O
cas	O
notamment	O
du	O
roman	O
Avicenne	O
de	O
Gilbert	O
Sinoué	O
.	O
Cependant	PERSON
une	PERSON
autre	PERSON
source	O
indiquerait	O
que	O
le	O
sultan	O
Mahmûd	O
de	O
Ghaznî	O
aurait	O
répandu	O
cette	O
information	O
afin	O
de	O
"	O
calomnier	O
"	O
le	O
philosophe	O
.	O
Cette	PERSON
œuvre	PERSON
disparut	PERSON
lors	PERSON
du	PERSON
sac	PERSON
d'	PERSON
Ispahan	PERSON
(	O
1034	O
)	O
,	O
et	O
il	O
n'	O
en	O
subsiste	PERSON
que	O
quelques	O
fragments	O
.	O
Avicenne	O
,	O
fin	LOCATION
lettré	LOCATION
,	O
fut	LOCATION
le	O
traducteur	O
des	O
œuvres	O
d'	O
Hippocrate	O
et	O
de	O
Galien	O
,	O
et	O
porta	O
un	O
soin	O
particulier	O
à	O
l'	O
étude	O
d'	O
Aristote	O
.	O
Avicenne	O
était	O
proche	O
du	O
chiisme	O
ismaélien	O
,	O
le	O
courant	O
auquel	PERSON
appartenaient	PERSON
son	PERSON
père	PERSON
et	PERSON
son	PERSON
frère	PERSON
;	O
ainsi	O
son	O
autobiographie	PERSON
rapporte-t-elle	PERSON
leurs	O
efforts	O
pour	O
entraîner	O
son	O
adhésion	O
à	O
la	O
dawat	PERSON
ismaélienne	PERSON
.	O
Toutefois	O
,	O
Avicenne	PERSON
appartenait	PERSON
au	PERSON
chiisme	PERSON
duodécimain	O
.	O
C'	O
est	O
le	O
développement	O
de	O
la	O
science	O
européenne	PERSON
qui	PERSON
provoquera	O
son	O
obsolescence	O
,	O
par	O
exemple	O
la	O
description	O
de	O
la	O
circulation	O
sanguine	O
par	O
William	PERSON
Harvey	PERSON
en	O
1628	O
.	O
Néanmoins	PERSON
cet	PERSON
ouvrage	PERSON
marqua	PERSON
longuement	PERSON
l'	PERSON
étude	PERSON
de	PERSON
la	PERSON
médecine	PERSON
,	O
et	O
même	O
en	O
1909	O
,	O
un	O
cours	O
de	O
la	O
médecine	O
d'	O
Avicenne	PERSON
fut	PERSON
donné	PERSON
à	PERSON
Bruxelles	PERSON
.	O
Avicenne	PERSON
se	PERSON
démarque	PERSON
dans	O
les	O
domaines	O
de	O
l'	O
ophtalmologie	O
,	O
de	O
la	O
gynéco-obstétrique	O
et	O
de	O
la	O
psychologie	O
.	O
Mais	PERSON
avant	O
tout	O
,	O
Avicenne	PERSON
s'	PERSON
intéresse	O
aux	O
moyens	O
de	O
conserver	O
la	O
santé	O
.	O
Sa	PERSON
doctrine	PERSON
philosophique	PERSON
,	O
en	O
particulier	O
sa	PERSON
métaphysique	PERSON
,	O
se	O
base	O
sur	O
celle	O
d'	O
Aristote	O
et	O
sur	O
les	O
travaux	O
d'	O
Al-Farabi	PERSON
.	O
La	O
philosophie	O
islamique	O
,	O
imprégnée	PERSON
de	PERSON
théologie	PERSON
,	O
concevait	O
plus	O
clairement	O
qu'	O
Aristote	O
la	O
distinction	O
entre	O
essence	O
et	O
existence	O
:	O
alors	O
que	O
l'	O
existence	O
est	O
le	O
domaine	O
du	O
contingent	O
,	O
de	O
l'	O
accidentel	PERSON
,	O
l'	O
essence	O
est	O
,	O
par	O
définition	O
,	O
ce	LOCATION
qui	LOCATION
perdure	LOCATION
dans	O
l'	O
être	O
au	O
travers	O
de	O
ses	O
accidents	O
.	O
L'	O
essence	O
,	O
pour	O
Avicenne	LOCATION
,	O
est	O
non-contingente	O
.	O
Pour	O
Avicenne	O
,	O
l'	O
intellect	O
humain	O
n'	O
est	O
pas	O
forgé	O
pour	O
l'	O
abstraction	O
des	O
formes	O
et	O
des	O
idées	O
.	O
Pour	O
les	O
néo-platoniciens	O
,	O
dont	O
Avicenne	O
fait	O
partie	O
,	O
l'	O
immortalité	O
de	O
l'	O
âme	O
est	O
une	O
conséquence	O
de	PERSON
sa	PERSON
nature	O
,	O
et	O
pas	O
une	O
finalité	O
.	O
Henry	PERSON
Corbin	PERSON
pense	PERSON
que	PERSON
ces	PERSON
œuvres	PERSON
sont	PERSON
le	PERSON
point	O
de	O
départ	O
du	O
projet	O
de	O
philosophie	O
orientale	O
que	O
Sohrawardi	PERSON
mène	PERSON
plus	O
tard	O
à	O
terme	O
.	O
Cette	O
conception	O
est	O
déjà	PERSON
explicite	PERSON
chez	PERSON
Avicenne	PERSON
,	O
et	O
le	O
sera	O
d'	O
autant	O
plus	O
chez	O
ses	O
commentateurs	O
et	O
critiques	O
,	O
comme	O
Sohrawardi	O
.	O
L'	O
influence	O
d'	O
Avicenne	O
est	O
double	O
:	O
Pour	O
Avicenne	O
"	O
l'	O
intellect	O
humain	O
n'	O
a	O
ni	O
le	O
rôle	O
ni	O
le	O
pouvoir	O
d'	O
abstraire	O
l'	O
intelligible	O
du	O
sensible	O
.	O
Pour	O
sa	O
part	O
,	O
Averroès	LOCATION
va	LOCATION
dégager	LOCATION
l'	O
aristotélisme	O
des	O
ajouts	O
platoniciens	O
qui	O
s'	O
étaient	O
greffés	O
sur	O
lui	O
:	O
point	O
d'	O
émanatisme	O
chez	O
lui	O
.	O
Avicenne	PERSON
y	PERSON
explique	PERSON
que	PERSON
les	O
métaux	O
"	O
résultent	O
de	O
l'	O
union	O
du	PERSON
mercure	PERSON
avec	PERSON
une	PERSON
terre	O
sulfureuse	O
"	O
.	O
Avicenne	PERSON
nie	PERSON
la	PERSON
possibilité	O
d'	O
une	O
transmutation	O
chimique	O
des	O
métaux	O
:	O
"	O
Quant	O
à	O
ce	O
que	O
prétendent	O
les	O
alchimistes	O
,	O
il	PERSON
faut	PERSON
savoir	PERSON
qu'	O
il	O
n'	O
est	O
pas	O
en	O
leur	O
pouvoir	O
de	O
transformer	O
véritablement	O
les	O
espèces	O
les	O
unes	O
en	O
les	O
autres	O
(	O
sciant	O
artifices	O
alchemiae	O
species	O
metallorum	O
transmutari	O
non	O
posse	O
)	O
;	O
mais	PERSON
il	PERSON
est	PERSON
en	PERSON
leur	O
pouvoir	O
de	O
faire	O
de	O
belles	O
imitations	O
,	O
jusqu'	PERSON
à	O
teindre	O
le	O
rouge	O
en	O
un	O
blanc	O
qui	O
le	O
rende	O
tout	O
à	O
fait	O
semblable	O
à	O
l'	O
argent	O
ou	O
en	O
un	O
jaune	O
qui	O
le	O
rende	O
tout	O
à	O
fait	O
semblable	O
à	O
l'	O
or	O
"	O
.	O
Pour	O
Avicenne	O
,	O
les	O
métaux	O
"	O
résultent	O
de	O
l'	O
union	O
du	PERSON
mercure	PERSON
avec	PERSON
une	PERSON
terre	O
sulfureuse	O
"	O
:	O
c'	O
est	O
la	O
théorie	PERSON
du	PERSON
mercure/soufre	PERSON
.	O
