Peu	PERSON
après	PERSON
et	PERSON
indépendamment	PERSON
,	O
en	O
1928	O
,	O
Ackermann	PERSON
a	O
publié	O
son	O
propre	O
exemple	O
de	O
fonction	O
récursive	O
mais	O
non	O
récursive	O
primitive	O
.	O
Sur	O
l'	O
infini	O
était	O
l'	O
article	O
le	O
plus	O
important	O
de	PERSON
Hilbert	PERSON
sur	PERSON
les	O
bases	O
des	O
mathématiques	O
,	O
servant	O
de	PERSON
cœur	PERSON
au	PERSON
programme	O
de	PERSON
Hilbert	PERSON
pour	O
fixer	O
la	O
base	O
des	O
nombres	O
transfinis	O
.	O
Ackermann	O
a	O
lui-même	O
initialement	O
donné	O
cette	O
définition	O
:	O
en	O
Caml	PERSON
:	O
Cette	O
fonction	O
prend	O
toute	O
son	O
utilité	O
dans	O
le	O
langage	O
de	O
programmation	O
Haskell	PERSON
,	O
langage	O
très	O
efficace	O
dans	O
le	O
traitement	O
des	O
fonctions	O
récursives	O
.	O
