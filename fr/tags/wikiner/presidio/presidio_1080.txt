L'	O
histoire	O
se	PERSON
déroule	PERSON
dans	PERSON
le	O
Japon	O
médiéval	O
.	O
Ashitaka	PERSON
,	O
le	LOCATION
prince	LOCATION
de	LOCATION
la	LOCATION
tribu	O
des	LOCATION
Emishis	LOCATION
,	O
est	O
frappé	O
d'	O
une	O
malédiction	O
après	O
avoir	O
tué	O
un	O
dieu	O
sanglier	O
devenu	O
démon	O
.	O
Hayao	PERSON
Miyazaki	PERSON
apporte	O
à	O
l'	O
animation	O
et	O
aux	O
décors	O
un	PERSON
soin	PERSON
extrême	PERSON
.	O
On	O
retrouve	PERSON
là	PERSON
un	PERSON
thème	PERSON
cher	PERSON
à	O
Hayao	PERSON
Miyazaki	PERSON
:	O
la	O
destruction	O
de	O
la	O
nature	O
,	O
les	O
risques	O
d'	O
opposition	O
entre	O
écologie	O
et	O
progrès	O
technique	O
,	O
tout	O
en	O
soulevant	O
la	O
complexité	O
des	O
rapports	O
entre	O
humains	O
et	O
entre	O
les	O
hommes	O
et	O
leur	O
environnement	O
.	O
Un	O
autre	O
point	O
récurrent	O
dans	O
les	O
films	O
de	PERSON
Miyazaki	PERSON
,	O
ici	O
aussi	PERSON
soulevé	PERSON
,	O
est	O
la	O
dénonciation	PERSON
de	PERSON
la	PERSON
guerre	PERSON
:	O
le	O
prince	O
Ashitaka	PERSON
,	O
pourtant	O
affligé	O
d'	O
une	O
malédiction	PERSON
mortelle	PERSON
,	O
cherche	O
à	O
concilier	O
les	O
deux	O
clans	O
et	O
répugne	PERSON
à	PERSON
l'	PERSON
usage	O
de	O
la	O
violence	O
.	O
Miyazaki	PERSON
pointe	PERSON
ici	PERSON
(	O
encore	O
une	O
fois	O
)	O
l'	O
importance	O
de	O
la	O
compassion	O
,	O
de	O
la	O
tolérance	O
et	O
d'	O
un	O
profond	O
respect	O
de	PERSON
la	PERSON
vie	PERSON
,	O
humaine	O
ou	O
animale	O
(	O
voire	O
végétale	O
dans	O
le	O
cas	O
de	O
la	O
forêt	O
)	O
.	O
Parmi	O
les	O
œuvres	O
de	O
Hayao	O
Miyazaki	O
,	O
Princesse	O
Mononoké	O
se	O
distingue	PERSON
notamment	PERSON
par	PERSON
:	O
Cependant	O
,	O
de	PERSON
nombreux	PERSON
points	O
montrent	O
qu'	O
on	O
a	O
bien	O
affaire	O
à	O
un	PERSON
anime	PERSON
de	PERSON
Miyazaki	PERSON
:	O
Ne	PERSON
serait-ce	PERSON
que	PERSON
le	PERSON
fait	PERSON
que	PERSON
le	O
film	O
est	O
construit	O
autour	O
d'	O
un	O
couple	O
d'	O
enfants	O
(	O
ici	O
jeunes	O
adolescents	O
)	O
qui	O
naviguent	O
entre	O
des	O
forces	O
qu'	O
ils	O
cherchent	O
à	O
concilier	O
.	O
La	O
musique	O
du	O
film	O
est	O
signée	O
Joe	PERSON
Hisaishi	PERSON
.	O
