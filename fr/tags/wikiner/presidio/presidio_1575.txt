Il	O
est	O
à	O
l'	O
origine	O
de	O
la	O
première	O
réunion	O
internationale	O
inter-religieuse	O
d'	O
Assise	O
en	O
1984	O
,	O
réunissant	O
plus	O
de	O
194	O
chefs	O
de	O
religions	O
.	O
Il	PERSON
parcourut	PERSON
plus	O
de	O
129	O
pays	O
pendant	O
son	O
pontificat	PERSON
,	O
plus	O
de	O
cinq	O
cents	O
millions	O
de	O
personnes	O
ayant	O
pu	PERSON
le	PERSON
voir	PERSON
durant	PERSON
cette	PERSON
période	PERSON
,	O
et	O
institua	O
de	O
grands	O
rassemblements	O
comme	O
les	O
Journée	O
mondiale	O
de	O
la	O
jeunesse	O
.	O
Il	O
a	O
défendu	O
les	O
réformes	O
du	O
Concile	O
Vatican	O
II	O
,	O
auquel	O
il	O
participa	O
en	O
tant	O
qu'	O
évêque	O
.	O
Très	PERSON
tôt	PERSON
,	O
il	PERSON
perd	PERSON
sa	PERSON
mère	PERSON
(	O
1929	O
)	O
puis	O
son	O
frère	PERSON
aîné	PERSON
,	O
Edmund	PERSON
(	O
1906	O
--	O
1932	O
)	O
,	O
médecin	PERSON
.	O
Adolescent	O
,	O
Karol	PERSON
Wojtyła	PERSON
est	PERSON
passionné	O
de	O
littérature	O
et	O
de	O
théâtre	O
.	O
Une	O
communauté	O
juive	O
importante	O
réside	O
à	O
Wadowice	O
,	O
que	O
Karol	PERSON
Wojtyla	PERSON
côtoie	O
quotidiennement	O
.	O
Il	O
rencontre	O
Mieczysław	O
Kotlarczyk	O
,	O
professeur	O
d'	O
histoire	O
au	O
lycée	O
des	O
filles	O
de	O
Wadowice	O
et	O
passionné	O
de	O
théâtre	O
.	O
Karol	PERSON
Wojtyla	PERSON
a	O
alors	O
la	O
volonté	O
de	O
devenir	O
acteur	O
,	O
et	PERSON
souhaite	PERSON
se	PERSON
consacrer	O
au	O
théâtre	O
.	O
Il	O
continuera	O
à	O
écrire	O
à	O
Mieczysław	O
Kotlarczyk	O
quand	O
il	O
quittera	O
Wadowice	O
.	O
Le	O
6	O
mai	O
1938	O
,	O
Karol	PERSON
Wojtyla	PERSON
reçoit	O
le	O
sacrement	O
de	O
confirmation	O
.	O
La	O
défaite	O
de	O
la	O
Pologne	O
contre	O
l'	O
Allemagne	O
nazie	O
conduit	O
à	O
l'	O
occupation	O
allemande	O
.	O
Karol	PERSON
Wojtyla	PERSON
continue	O
à	O
être	O
acteur	O
dans	O
des	O
pièces	O
de	O
théâtre	O
.	O
Dans	PERSON
ces	PERSON
pièces	PERSON
on	O
peut	O
voir	O
des	O
parallèles	O
entre	O
le	O
destin	O
de	O
la	O
Pologne	O
et	O
d'	O
Israël	LOCATION
.	O
Le	O
théâtre	O
est	O
conçu	LOCATION
par	O
Karol	PERSON
Wojtyla	PERSON
comme	O
un	O
moyen	O
de	O
résistance	O
et	O
de	O
défense	O
de	O
la	O
patrie	O
polonaise	O
contre	O
l'	O
occupant	O
nazi	O
.	O
En	O
juillet	O
1941	O
,	O
Mieczysław	PERSON
Kotlarczyk	PERSON
rejoint	O
Cracovie	O
avec	O
son	O
épouse	O
.	O
Ils	O
sont	O
hébergés	O
dans	O
l'	O
appartement	O
de	PERSON
Karol	PERSON
Wojtyla	PERSON
.	O
Ce	O
travail	O
sur	O
la	O
puissance	O
,	O
en	O
soi	LOCATION
,	O
de	O
la	O
parole	O
,	O
influencera	PERSON
profondément	O
Karol	PERSON
Wojtyla	PERSON
dans	O
son	O
apostolat	PERSON
de	PERSON
prêtre	PERSON
,	PERSON
puis	PERSON
d'	PERSON
évêque	O
et	O
de	O
pape	O
.	O
Mais	PERSON
Karol	PERSON
Wojtyla	PERSON
refuse	O
d'	O
entrer	O
dans	O
la	O
résistance	O
armée	O
,	O
préférant	O
des	O
moyens	O
plus	O
pacifiques	O
,	O
comme	LOCATION
le	LOCATION
combat	O
culturel	O
et	O
la	O
prière	O
,	O
.	O
Au	O
cours	O
de	O
l'	O
automne	O
1942	O
,	O
après	PERSON
un	PERSON
long	PERSON
temps	PERSON
de	PERSON
réflexion	PERSON
,	O
il	PERSON
décide	PERSON
de	PERSON
devenir	PERSON
prêtre	PERSON
,	O
et	O
entre	O
au	O
séminaire	O
clandestin	O
de	O
Cracovie	O
.	O
Karol	PERSON
Wojtyła	PERSON
est	PERSON
accepté	O
au	PERSON
séminaire	PERSON
clandestin	PERSON
qu'	O
Adam	PERSON
Stefan	PERSON
Sapieha	PERSON
,	O
archevêque	PERSON
de	PERSON
Cracovie	PERSON
,	O
a	O
organisé	O
malgré	O
l'	O
interdiction	O
allemande	O
de	O
former	O
de	O
nouveaux	O
prêtres	O
,	O
en	O
octobre	O
1942	O
.	O
Karol	PERSON
travaille	PERSON
comme	PERSON
ouvrier	PERSON
la	PERSON
journée	PERSON
et	O
étudie	PERSON
le	PERSON
soir	PERSON
.	PERSON
Karol	PERSON
Wojtyla	PERSON
échappe	PERSON
à	O
une	O
rafle	O
qui	O
a	O
lieu	O
dans	O
son	O
immeuble	O
,	O
restant	O
silencieusement	LOCATION
en	O
prière	O
dans	O
son	O
appartement	PERSON
situé	PERSON
en	O
sous-sol	O
,	O
.	O
Il	PERSON
ne	PERSON
retrouve	PERSON
sa	PERSON
liberté	PERSON
de	PERSON
mouvement	PERSON
que	PERSON
le	O
17	O
janvier	O
1945	O
,	O
suite	O
à	O
la	O
libération	O
de	O
Cracovie	O
par	O
l'	O
armée	O
rouge	O
.	O
Karol	PERSON
Wojtyla	PERSON
étudie	PERSON
particulièrement	O
la	O
théologie	O
de	O
Jean	O
de	O
La	O
Croix	O
,	O
de	O
Thérèse	O
d'	O
Ávila	PERSON
et	O
de	O
Thérèse	O
de	O
Lisieux	O
.	O
Il	O
poursuit	O
ensuite	O
sa	O
formation	O
à	O
l'	O
Angelicum	O
de	O
Rome	O
,	O
université	O
alors	O
dirigée	O
par	O
les	O
dominicains	O
.	O
Il	O
y	O
reste	O
deux	O
ans	O
,	O
pour	O
préparer	PERSON
sa	PERSON
thèse	PERSON
de	PERSON
doctorat	PERSON
en	O
théologie	O
sur	O
"	O
La	O
foi	O
dans	O
la	O
pensée	O
de	O
saint	O
Jean	O
de	O
la	O
Croix	O
"	O
,	O
.	O
Il	PERSON
loge	PERSON
dans	PERSON
le	PERSON
collège	PERSON
Belge	PERSON
,	O
où	LOCATION
il	LOCATION
apprend	O
le	O
français	O
.	O
Pour	O
les	O
besoins	O
de	O
sa	O
thèse	O
sur	O
Jean	O
de	O
La	O
Croix	O
,	O
il	O
apprend	O
aussi	PERSON
l'	O
espagnol	O
.	O
Il	O
voyage	O
alors	O
en	O
France	LOCATION
et	O
en	O
Belgique	O
.	O
Pendant	O
ce	PERSON
séjour	PERSON
il	PERSON
découvre	PERSON
la	PERSON
réalité	PERSON
du	PERSON
début	PERSON
de	PERSON
la	PERSON
déchristianisation	PERSON
de	O
la	O
France	O
mais	O
aussi	O
les	O
nouvelles	O
méthodes	O
pastorales	O
.	O
Il	LOCATION
rencontre	LOCATION
le	LOCATION
théologien	O
Henri	O
de	O
Lubac	O
et	O
observe	O
l'	O
expérience	O
des	O
prêtres-ouvriers	O
.	O
En	O
Belgique	O
,	O
il	O
rencontre	O
l'	O
abbé	O
Joseph	PERSON
Cardjin	PERSON
,	O
fondateur	O
de	O
la	O
Jeunesse	O
ouvrière	O
chrétienne	O
.	O
Lorsqu'	PERSON
il	O
rentre	O
en	O
Pologne	LOCATION
,	O
il	O
publie	O
dans	O
la	O
revue	O
catholique	O
de	O
Cracovie	O
son	O
impression	O
positive	O
sur	O
les	O
nouvelles	O
formes	O
d'	O
évangélisation	O
en	O
France	LOCATION
.	O
Il	O
y	O
découvre	O
le	O
développement	O
du	O
stalinisme	O
en	O
Pologne	LOCATION
.	O
Il	O
célèbre	O
la	O
messe	O
sur	O
un	O
canoë	O
,	O
chose	O
assez	O
rare	O
avant	O
le	O
Concile	O
Vatican	O
II	O
,	O
s'	O
habille	O
en	O
civil	O
,	O
afin	O
de	O
ne	O
pas	O
se	O
faire	O
repérer	O
par	O
le	O
régime	O
communiste	O
.	O
Il	O
est	O
nommé	O
à	O
l'	O
université	O
par	O
le	O
cardinal	O
Sapieha	PERSON
,	O
contre	O
sa	O
volonté	O
.	O
Il	O
apprend	O
alors	O
l'	O
allemand	O
afin	O
de	O
pouvoir	O
mieux	O
comprendre	O
Max	PERSON
Scheler	PERSON
.	O
Des	O
personnalités	O
catholiques	O
comme	O
le	O
cardinal	O
Stefan	PERSON
Wyszyński	PERSON
sont	O
emprisonnées	LOCATION
en	O
septembre	O
1953	O
.	O
Des	O
manifestations	O
en	O
faveur	O
de	O
la	O
liberté	O
religieuse	O
apparaissent	O
et	O
le	O
cardinal	O
Stefan	PERSON
Wyszyński	PERSON
est	PERSON
libéré	LOCATION
en	O
1956	O
.	O
En	O
1954	O
,	O
il	O
est	O
nommé	O
professeur	O
d'	O
éthique	O
à	O
l'	O
Université	O
catholique	O
de	O
Lublin	O
.	O
Karol	PERSON
Wojtyla	PERSON
critique	O
alors	O
le	O
communisme	O
,	O
en	O
considérant	O
que	O
l'	O
éthique	O
marxiste	O
ne	O
permettait	O
pas	O
d'	O
appréhender	O
la	O
réalité	O
de	O
l'	O
homme	O
en	O
tant	O
que	PERSON
tel	PERSON
.	PERSON
Karol	PERSON
Wojtyla	PERSON
considère	O
que	O
l'	O
approche	O
chrétienne	O
de	O
la	O
vie	O
et	O
de	O
la	O
société	O
était	O
extrêmement	PERSON
réaliste	PERSON
,	O
alors	O
que	O
l'	O
approche	O
marxiste	O
finissait	O
par	O
"	O
être	O
purement	O
idéaliste	O
,	O
faute	O
d'	O
être	O
concrète	O
"	O
.	O
Face	O
à	O
cette	O
opposition	O
,	O
Karol	PERSON
Wojtyla	PERSON
ne	PERSON
cherche	PERSON
jamais	PERSON
à	PERSON
développer	O
une	O
confrontation	O
armée	O
ou	O
violente	O
avec	O
les	O
communistes	O
.	O
Le	O
28	O
septembre	O
1958	O
,	O
le	O
pape	O
Pie	PERSON
XII	PERSON
le	PERSON
nomme	PERSON
évêque	PERSON
auxiliaire	PERSON
de	PERSON
Cracovie	PERSON
.	O
À	O
38	O
ans	O
,	O
Karol	PERSON
Wojtyła	PERSON
est	PERSON
le	PERSON
plus	O
jeune	O
évêque	O
de	O
la	O
République	O
populaire	O
de	O
Pologne	O
.	O
Cette	O
nomination	O
est	O
validée	PERSON
par	PERSON
le	PERSON
régime	O
communiste	O
,	O
car	O
Karol	PERSON
Wojtyła	PERSON
est	PERSON
considéré	O
comme	O
une	O
personne	O
qui	PERSON
ne	PERSON
s'	PERSON
intéresse	O
pas	O
aux	O
débats	O
politiques	O
,	O
contrairement	O
au	O
cardinal	O
Stefan	PERSON
Wyszyński	PERSON
.	O
C'	O
est	O
à	O
cette	O
époque	O
qu'	O
il	O
choisit	PERSON
sa	PERSON
devise	O
"	O
Totus	O
tuus	O
"	O
(	O
"	O
tout	O
à	O
toi	O
"	O
)	O
,	O
inspirée	PERSON
de	PERSON
la	PERSON
spiritualité	PERSON
de	PERSON
Louis-Marie	PERSON
Grignion	PERSON
de	PERSON
Montfort	PERSON
et	PERSON
illustration	PERSON
de	PERSON
sa	PERSON
dévotion	PERSON
à	O
la	O
Vierge	PERSON
Marie	PERSON
.	O
Il	O
tente	O
de	O
concilier	O
dans	O
sa	PERSON
réflexion	PERSON
,	O
mais	PERSON
aussi	PERSON
dans	PERSON
les	PERSON
articles	PERSON
qu'	O
il	O
publie	O
,	O
la	PERSON
philosophie	PERSON
de	PERSON
saint	PERSON
Thomas	PERSON
avec	PERSON
la	PERSON
phénoménologie	PERSON
.	O
Karol	PERSON
Wojtyla	PERSON
est	PERSON
alors	PERSON
nommé	PERSON
pour	PERSON
le	O
remplacer	O
le	O
13	O
janvier	O
1964	O
,	O
devenant	O
ainsi	O
le	O
plus	O
jeune	PERSON
administrateur	PERSON
de	PERSON
diocèse	PERSON
en	O
Pologne	LOCATION
.	O
Pendant	O
plus	O
de	O
20	O
ans	O
,	O
Karol	PERSON
Wojtyla	PERSON
défend	PERSON
les	PERSON
paroissiens	PERSON
de	PERSON
la	PERSON
nouvelle	PERSON
ville	PERSON
Nowa	PERSON
Huta	PERSON
,	O
une	O
ville	LOCATION
communiste	O
modèle	O
,	O
privée	O
de	O
lieu	O
de	O
culte	O
.	O
Peu	PERSON
de	PERSON
temps	PERSON
après	PERSON
sa	PERSON
nomination	O
comme	PERSON
évêque	PERSON
,	PERSON
le	PERSON
nouveau	PERSON
pape	O
Jean	PERSON
XXIII	PERSON
décide	PERSON
d'	PERSON
ouvrir	O
le	O
IIe	O
concile	PERSON
œcuménique	PERSON
du	O
Vatican	O
.	O
L'	O
évêque	O
Karol	PERSON
Wojtyla	PERSON
est	PERSON
alors	PERSON
invité	O
à	O
participer	O
au	O
concile	O
.	O
Dans	O
la	O
réponse	O
au	O
questionnaire	O
pour	O
le	O
Concile	O
Vatican	O
II	O
,	O
Karol	PERSON
Wojtyla	PERSON
demande	O
que	O
le	O
concile	O
se	O
prononce	O
clairement	O
sur	O
"	O
l'	O
importance	O
de	PERSON
la	PERSON
transcendance	PERSON
de	PERSON
la	PERSON
personne	O
humaine	PERSON
face	PERSON
au	O
matérialisme	O
croissant	O
de	O
l'	O
époque	O
moderne	O
"	O
.	O
Au	O
cours	O
de	O
ce	O
concile	O
,	O
Karol	PERSON
Wojtyla	PERSON
,	O
parlant	O
le	LOCATION
français	LOCATION
,	O
l'	O
anglais	O
,	O
l'	O
allemand	O
,	O
le	LOCATION
polonais	LOCATION
,	O
le	LOCATION
russe	LOCATION
,	O
l'	O
espagnol	O
,	O
l'	O
italien	O
et	O
le	O
latin	O
,	O
devient	O
progressivement	O
le	LOCATION
porte	LOCATION
parole	LOCATION
de	O
la	O
délégation	O
polonaise	O
.	O
La	O
nomination	O
de	PERSON
Karol	PERSON
Wojtyla	PERSON
comme	O
archevêque	O
en	O
1964	O
,	O
lui	O
permet	O
d'	O
avoir	O
une	O
plus	O
grande	O
stature	O
au	O
sein	O
de	O
la	O
délégation	O
.	O
Lors	O
du	O
Concile	O
Vatican	O
II	O
,	O
deux	O
tendances	O
s'	O
affrontent	O
sur	O
la	O
conception	O
de	O
l'	O
athéisme	O
,	O
souvent	O
liée	O
à	O
la	O
représentation	O
existante	O
du	O
marxisme	O
.	O
Karol	PERSON
Wojtyla	PERSON
demande	O
alors	O
de	O
considérer	O
l'	O
athéisme	O
,	O
non	O
dans	O
sa	O
composante	O
sociologique	O
ou	O
politique	O
,	O
mais	O
avant	O
tout	O
dans	O
son	O
état	PERSON
intérieur	PERSON
de	PERSON
la	PERSON
personne	O
humaine	PERSON
.	PERSON
Le	O
30	O
novembre	O
1964	O
,	O
Paul	PERSON
VI	PERSON
reçoit	PERSON
pour	PERSON
la	PERSON
première	O
fois	O
Karol	O
Wojtyla	O
lors	O
d'	O
une	O
audience	O
privée	O
.	O
Il	O
est	O
nommé	O
archevêque	O
au	O
côté	O
du	O
Cardinal	O
Wyszyński	O
,	O
primat	O
de	O
Pologne	O
,	O
et	O
figure	O
de	PERSON
proue	PERSON
de	PERSON
l'	PERSON
épiscopat	O
polonais	O
dans	O
la	O
résistance	O
au	PERSON
communisme	PERSON
.	O
Paul	PERSON
VI	PERSON
le	PERSON
nomme	PERSON
archevêque	PERSON
de	PERSON
Cracovie	PERSON
le	O
30	O
décembre	O
1963	O
.	O
Cette	O
nomination	O
continue	O
à	O
être	O
soutenue	O
par	O
le	O
régime	O
communiste	O
,	O
qui	PERSON
considère	PERSON
toujours	PERSON
Karol	PERSON
Wojtyla	PERSON
,	O
du	O
fait	O
de	O
son	O
absence	O
d'	O
implication	O
dans	O
les	PERSON
débats	PERSON
politiques	PERSON
,	O
comme	O
un	O
allié	O
face	O
au	O
cardinal	O
Wyszynski	PERSON
,	O
.	O
Cette	O
nomination	O
intervient	O
alors	PERSON
même	PERSON
que	PERSON
le	O
cardinal	O
Wyszynski	PERSON
voulait	PERSON
promouvoir	O
d'	O
autres	PERSON
personnes	PERSON
à	PERSON
ce	PERSON
poste	PERSON
.	O
Ce	PERSON
titre	PERSON
posa	PERSON
des	PERSON
problèmes	PERSON
à	O
Karol	PERSON
Wojtyla	PERSON
qui	PERSON
craignait	PERSON
que	PERSON
le	O
pouvoir	O
communiste	O
utilise	O
et	O
développe	O
une	O
concurrence	O
entre	O
les	O
deux	O
archevêque	O
de	O
Pologne	O
.	O
Karol	PERSON
Wojtyla	PERSON
choisit	PERSON
alors	PERSON
de	PERSON
soutenir	PERSON
inconditionnellement	O
le	O
Cardinal	O
Wyszyński	O
,	O
.	O
Il	PERSON
décide	PERSON
alors	PERSON
en	O
juillet	O
1965	O
,	O
sans	O
l'	O
en	O
avertir	O
,	O
de	O
reprendre	O
et	O
de	O
défendre	O
les	O
conceptions	O
du	O
cardinal	O
Wyszynski	PERSON
sans	O
montrer	PERSON
la	PERSON
moindre	PERSON
divergence	O
avec	PERSON
lui	PERSON
.	O
Karol	PERSON
Wojtyla	PERSON
est	PERSON
alors	PERSON
mis	PERSON
sous	PERSON
écoute	PERSON
et	O
espionné	O
par	O
le	O
pouvoir	O
en	O
place	O
;	O
il	O
est	O
parfois	O
suivi	O
lors	O
de	O
ses	O
déplacements	O
.	O
Il	O
préside	O
plus	O
de	O
cinquante	O
messes	O
d'	O
anniversaire	O
,	O
dont	O
une	O
messe	O
pontificale	O
au	O
nom	O
du	O
pape	O
Paul	PERSON
VI	PERSON
,	O
qui	O
n'	O
est	O
pas	O
autorisé	O
à	O
entrer	O
en	O
Pologne	LOCATION
,	O
au	O
sanctuaire	O
Jasna	O
Góra	O
de	O
Częstochowa	O
,	O
haut	LOCATION
lieu	LOCATION
du	LOCATION
catholicisme	LOCATION
polonais	LOCATION
.	LOCATION
L'	O
objectif	O
de	O
la	O
célébration	O
du	O
millénaire	O
de	O
la	O
Pologne	O
est	O
aussi	O
de	O
mettre	O
en	O
avant	O
l'	O
héritage	O
profondément	O
chrétien	O
du	O
pays	O
alors	O
même	O
que	O
le	O
gouvernement	O
communiste	O
promeut	O
l'	O
athéisme	O
.	O
Suite	O
à	O
sa	O
nomination	O
comme	PERSON
cardinal	PERSON
,	O
Karol	PERSON
Wojtyla	PERSON
passe	O
deux	O
mois	O
par	O
an	O
au	LOCATION
Vatican	LOCATION
.	O
Celui-ci	PERSON
accuse	O
les	O
Juifs	PERSON
d'	O
être	O
responsables	O
de	O
la	O
révolte	O
.	O
Karol	PERSON
Wojtyla	PERSON
prend	PERSON
alors	PERSON
publiquement	PERSON
la	PERSON
défense	PERSON
des	PERSON
étudiants	PERSON
et	O
invite	O
,	O
à	O
une	O
conférence	O
organisée	O
à	O
l'	O
archidiocèse	O
de	PERSON
Cracovie	PERSON
,	O
le	O
philosophe	O
juif	PERSON
Roman	PERSON
Ingarden	PERSON
,	O
montrant	O
ainsi	O
son	O
soutien	PERSON
à	PERSON
la	PERSON
communauté	PERSON
juive	PERSON
.	PERSON
Il	O
ordonne	O
alors	O
secrètement	O
des	O
prêtres	O
à	O
Cracovie	O
.	O
Lors	O
de	O
la	O
mort	O
en	O
prison	O
de	O
l'	O
évêque	PERSON
Štěpán	PERSON
Trochta	PERSON
,	O
en	O
1974	O
,	O
le	O
pouvoir	O
interdit	O
à	O
Karol	PERSON
Wojtyla	PERSON
de	PERSON
venir	PERSON
célébrer	PERSON
les	PERSON
obsèques	PERSON
.	O
Les	O
ouvriers	O
de	O
Pologne	O
se	O
révoltent	O
en	O
1970	O
face	O
à	O
l'	O
augmentation	O
des	O
prix	O
.	O
Karol	PERSON
Wojtyla	PERSON
,	O
tout	O
en	O
se	PERSON
défendant	PERSON
de	PERSON
vouloir	PERSON
agir	PERSON
politiquement	O
,	O
prend	O
la	O
défense	O
des	O
ouvriers	O
.	O
Karol	PERSON
Wojtyla	PERSON
prend	PERSON
la	PERSON
défense	PERSON
des	PERSON
droits	PERSON
de	PERSON
l'	PERSON
homme	PERSON
,	O
affirmant	O
,	O
lors	O
de	O
l'	O
homélie	O
de	O
la	O
veille	O
du	O
jour	O
de	O
l'	O
an	O
,	O
qu'	O
il	O
défendait	O
"	O
le	O
droit	O
de	O
manger	O
à	O
sa	PERSON
faim	PERSON
,	O
le	PERSON
droit	PERSON
à	PERSON
la	PERSON
liberté	PERSON
…	O
une	O
atmosphère	O
d'	O
authentique	O
liberté	O
sans	O
contraintes	O
…	O
que	PERSON
rien	PERSON
ne	PERSON
menace	O
.	O
Un	O
prêtre	O
du	O
diocèse	O
de	O
Cracovie	O
affirma	O
que	O
près	O
de	O
soixante	O
pour	O
cent	O
de	O
l'	O
encyclique	O
provenait	O
du	O
rapport	O
de	PERSON
Wojtyla	PERSON
.	O
Synode	PERSON
des	PERSON
évêques	PERSON
Karol	PERSON
Wojtyla	PERSON
participe	O
aux	O
synodes	O
des	O
évêques	O
de	O
1969	O
sur	O
la	O
collaboration	O
des	O
épiscopats	O
nationaux	O
avec	O
le	O
siège	O
apostolique	O
,	O
puis	O
à	O
celui	O
de	O
1971	O
sur	O
le	O
sacerdoce	O
et	O
la	O
justice	O
dans	O
le	O
monde	O
.	O
Paul	PERSON
VI	PERSON
reçoit	PERSON
souvent	PERSON
le	O
cardinal	O
Wojtyla	PERSON
,	O
dont	O
plus	O
de	O
onze	O
fois	O
pendant	O
la	O
période	O
1973	O
à	O
1976	O
.	O
Cette	O
retraite	O
prêchée	O
au	O
Vatican	LOCATION
fait	O
connaître	O
Karol	PERSON
Wojtyla	PERSON
auprès	PERSON
de	PERSON
la	PERSON
curie	PERSON
,	O
le	O
rendant	O
pour	O
la	O
première	O
fois	O
papabile	O
,	O
.	O
Certains	O
ont	O
cru	O
voir	O
dans	O
ce	O
livre	O
une	O
étape	O
importante	O
dans	O
la	O
maturation	O
philosophique	O
du	O
cardinal	O
Wojtyla	PERSON
.	O
Cette	O
conception	O
centrée	PERSON
sur	PERSON
la	PERSON
personne	O
constitue	O
le	O
fondement	O
pour	O
le	O
cardinal	O
Wojtyla	PERSON
du	PERSON
rôle	PERSON
des	PERSON
systèmes	PERSON
politiques	PERSON
,	O
qui	O
ont	O
pour	O
vocation	O
d'	O
aider	O
les	PERSON
individus	PERSON
à	O
se	O
déterminer	O
eux-mêmes	PERSON
.	O
Jean-Paul	PERSON
I	O
meurt	O
trente-trois	O
jours	O
plus	O
tard	O
.	O
Au	O
cours	O
de	O
ce	O
conclave	O
,	O
Karol	PERSON
Wojtyla	PERSON
avait	PERSON
déjà	PERSON
reçu	PERSON
neuf	PERSON
voix	PERSON
de	PERSON
cardinaux	PERSON
.	O
D'	O
après	O
l'	O
opinion	O
qui	PERSON
s'	PERSON
imposa	O
par	O
la	O
suite	O
,	O
le	O
conclave	O
aurait	O
été	O
divisé	O
entre	O
deux	O
favoris	O
:	O
Giuseppe	PERSON
Siri	PERSON
,	O
archevêque	PERSON
de	PERSON
Gênes	PERSON
,	O
et	O
Giovanni	PERSON
Benelli	PERSON
,	O
archevêque	PERSON
de	PERSON
Florence	PERSON
proche	PERSON
de	PERSON
Jean-Paul	PERSON
I	O
er	O
et	O
grand	O
électeur	O
du	O
conclave	O
précédent	O
,	O
.	O
D'	O
après	O
George	PERSON
Weigel	PERSON
,	O
plusieurs	O
facteurs	O
peuvent	O
expliquer	O
son	O
élection	O
.	O
Cardinal	O
depuis	O
onze	O
années	O
,	O
Karol	PERSON
Wojtyla	PERSON
était	PERSON
bien	PERSON
connu	PERSON
des	PERSON
autres	PERSON
électeurs	PERSON
.	O
Ses	O
interventions	O
lors	O
du	O
concile	O
Vatican	O
II	O
et	O
sa	O
prédication	O
pendant	O
la	O
retraite	O
papale	O
en	O
1976	O
avaient	O
été	PERSON
remarquées	PERSON
.	PERSON
Mais	PERSON
avant	O
tout	O
,	O
selon	PERSON
Weigel	PERSON
,	O
il	PERSON
avait	PERSON
marqué	PERSON
les	PERSON
esprits	PERSON
dans	O
sa	O
mission	O
d'	O
évêque	O
diocésain	O
,	O
montrant	O
qu'	O
une	O
direction	O
ferme	O
pouvait	O
être	O
possible	O
au	O
milieu	O
des	O
tensions	O
post-conciliaires	O
.	O
De	PERSON
même	PERSON
,	O
pour	O
Bernard	PERSON
Lecomte	PERSON
,	O
le	O
souhait	O
général	O
des	O
cardinaux	O
était	O
"	O
d'	O
élire	O
un	O
pasteur	O
,	O
un	PERSON
homme	PERSON
ayant	PERSON
l'	PERSON
expérience	O
du	O
terrain	O
"	O
.	O
Jean-Paul	PERSON
II	PERSON
se	PERSON
démarque	O
dans	O
la	O
succession	O
des	O
papes	O
par	O
sa	O
nationalité	O
,	O
sa	O
relative	O
jeunesse	O
et	O
sa	O
condition	O
d'	O
ancien	O
athlète	O
.	O
Sur	O
ses	O
263	O
prédécesseurs	O
,	O
seul	O
Pie	O
IX	O
(	O
1846	O
--	O
1878	O
)	O
a	O
régné	O
plus	O
longtemps	O
que	O
lui	O
(	O
31	O
ans	O
7	O
mois	O
et	O
17	O
jours	O
)	O
,	O
mais	O
saint	PERSON
Pierre	PERSON
,	O
le	O
premier	O
des	PERSON
évêques	PERSON
de	PERSON
Rome	PERSON
,	O
aurait	O
régné	O
encore	O
plus	O
longtemps	O
(	O
34	O
ans	O
ou	O
37	O
ans	O
dont	O
25	O
à	O
Rome	LOCATION
)	O
.	O
Les	O
premiers	O
jours	O
de	O
son	O
pontificat	PERSON
sont	O
marqués	O
par	O
des	O
changements	O
de	O
forme	O
du	O
fait	O
de	O
Jean-Paul	O
II	O
.	O
Jean-Paul	PERSON
II	PERSON
décida	O
d'	O
aller	O
au	O
Mexique	O
en	O
1978	O
.	O
Il	O
visite	O
le	O
sanctuaire	O
marial	O
de	O
Notre-Dame	O
de	O
Guadalupe	O
.	O
Dès	O
l'	O
année	O
suivante	O
il	O
visite	O
la	O
Pologne	LOCATION
,	O
l'	O
Irlande	LOCATION
,	O
les	O
États-Unis	O
et	O
la	O
Turquie	O
.	O
En	O
1980	O
il	O
se	O
rend	O
en	O
Afrique	O
,	O
en	O
France	LOCATION
et	O
au	O
Brésil	O
.	O
Le	PERSON
mercredi	PERSON
13	PERSON
mai	O
1981	O
,	O
jour	O
de	O
l'	O
audience	O
générale	O
hebdomadaire	O
qui	O
se	O
tient	O
place	O
Saint-Pierre	PERSON
à	PERSON
Rome	LOCATION
,	O
Jean-Paul	PERSON
II	PERSON
est	PERSON
victime	PERSON
d'	PERSON
un	O
attentat	O
:	O
Mehmet	PERSON
Ali	PERSON
Ağca	PERSON
tire	O
sur	O
lui	O
devant	O
une	O
foule	O
de	O
20000	O
fidèles	O
.	O
Plus	PERSON
tard	PERSON
,	O
le	O
pape	O
se	O
rendra	PERSON
dans	O
la	O
cellule	O
de	PERSON
Mehmet	PERSON
Ali	PERSON
Ağca	PERSON
pour	O
lui	O
accorder	O
son	O
pardon	O
.	O
Selon	O
certaines	O
sources	O
,	O
cet	O
attentat	O
pourrait	O
être	O
l'	O
œuvre	O
du	O
GRU	O
,	O
les	O
services	O
de	O
renseignements	O
de	O
l'	O
armée	O
soviétique	O
.	O
Enfin	O
certains	O
n'	O
y	O
ont	O
vu	O
que	O
la	O
volonté	O
propre	O
de	O
Mehmet	O
Ali	O
Ağca	O
,	O
considérant	O
qu'	O
il	PERSON
souffrait	PERSON
de	PERSON
troubles	O
psychiatriques	O
.	O
Walesa	LOCATION
a	O
la	O
permission	O
de	PERSON
rencontrer	PERSON
le	PERSON
pape	PERSON
en	O
1981	O
.	O
Par	O
cette	O
encyclique	O
,	O
il	LOCATION
montre	LOCATION
son	O
soutien	PERSON
à	PERSON
la	O
cause	O
polonaise	O
de	O
Solidarność	O
.	O
Il	O
pousse	O
les	O
évêques	O
polonais	O
à	O
défendre	O
les	O
accords	O
qui	O
ont	O
lieu	O
en	O
Pologne	LOCATION
.	O
Ronald	PERSON
Reagan	PERSON
soutient	O
aussi	O
la	O
position	O
du	PERSON
pape	PERSON
sur	O
les	O
questions	O
liées	PERSON
à	O
l'	O
avortement	O
.	O
Le	O
12	O
décembre	O
1981	O
,	O
face	O
à	O
l'	O
augmentation	O
des	O
protestations	O
en	O
Pologne	LOCATION
,	O
le	O
général	O
Wojciech	PERSON
Jaruzelski	PERSON
déclare	PERSON
la	PERSON
loi	PERSON
martiale	PERSON
.	O
Jean-Paul	PERSON
II	PERSON
cherche	PERSON
alors	PERSON
à	PERSON
apaiser	PERSON
les	PERSON
revendications	PERSON
,	O
craignant	O
un	PERSON
bain	PERSON
de	PERSON
sang	O
,	O
et	O
affirmant	O
qu'	PERSON
il	PERSON
faut	PERSON
promouvoir	PERSON
la	PERSON
paix	PERSON
.	O
Lors	O
de	O
sa	O
visite	O
en	O
Pologne	LOCATION
en	O
1983	O
,	O
il	LOCATION
soutient	LOCATION
les	LOCATION
opposants	O
au	O
régime	O
.	O
Au	O
cours	O
de	O
cette	O
visite	O
,	O
il	PERSON
reçoit	PERSON
le	PERSON
titre	PERSON
de	PERSON
docteur	PERSON
honoris	O
causa	PERSON
de	PERSON
l'	PERSON
Université	O
Jagellon	O
de	PERSON
Cracovie	PERSON
,	O
en	O
1983	O
.	O
En	O
1986	O
,	O
il	PERSON
lance	PERSON
les	PERSON
premières	PERSON
Journée	O
mondiale	O
de	O
la	O
jeunesse	O
.	O
La	O
démarche	O
de	O
Jean-Paul	O
II	O
n'	O
était	O
pas	O
du	O
syncrétisme	O
:	O
toutes	O
les	O
religions	O
étaient	O
ensemble	O
pour	O
prier	O
,	O
mais	PERSON
ne	PERSON
priaient	PERSON
pas	PERSON
d'	PERSON
une	O
seule	O
voix	O
.	O
En	O
1987	O
il	O
visite	O
le	O
Chili	O
et	O
est	O
accueilli	O
par	O
Augusto	PERSON
Pinochet	PERSON
.	O
Au	O
cours	O
de	O
cette	O
visite	O
,	O
il	O
demanda	O
en	O
privé	O
à	O
Augusto	PERSON
Pinochet	PERSON
de	PERSON
démissionner	PERSON
et	PERSON
de	PERSON
rendre	PERSON
le	PERSON
pouvoir	O
à	O
la	O
société	O
civile	O
.	O
En	O
1988	O
,	O
il	O
publie	O
l'	O
encyclique	O
Sollicitudo	PERSON
Rei	PERSON
Socialis	PERSON
.	O
Le	O
succès	O
de	O
ses	O
voyages	O
en	O
Pologne	O
notamment	O
,	O
avaient	O
contribué	O
à	O
déstabiliser	PERSON
le	PERSON
régime	PERSON
.	O
Jean-Paul	PERSON
II	PERSON
critique	O
alors	O
avec	O
plus	O
de	O
force	O
les	O
dérives	O
du	O
capitalisme	O
.	O
Au	O
Mexique	O
il	O
dénonce	O
les	O
inégalités	O
criantes	O
de	O
richesses	O
dans	O
le	O
monde	O
,	O
du	O
fait	O
d'	O
un	O
capitalisme	O
qui	O
se	O
développe	O
sans	O
souci	O
du	O
bien	O
commun	O
.	O
Il	O
s'	O
oppose	O
aussi	O
au	O
déclenchement	O
de	O
la	O
Guerre	O
en	O
Irak	O
,	O
.	O
Lors	O
de	O
sa	O
visite	O
en	O
Pologne	LOCATION
en	O
1991	O
,	O
il	O
dénonce	O
avec	O
force	O
la	O
société	O
de	O
consommation	O
.	O
Jean-Paul	PERSON
II	PERSON
avait	PERSON
réclamé	PERSON
dès	PERSON
l'	PERSON
ouverture	O
de	O
son	O
pontificat	PERSON
que	O
"	O
les	O
malades	O
soient	O
placés	O
au	O
premier	O
rang	O
"	O
.	O
Jean-Paul	PERSON
II	PERSON
refuse	O
alors	O
l'	O
hospitalisation	O
.	O
D'	O
après	O
le	O
certificat	O
du	O
décès	O
publié	O
le	O
3	O
avril	O
par	O
le	LOCATION
Vatican	LOCATION
,	O
sa	PERSON
mort	PERSON
est	PERSON
due	O
à	O
un	O
choc	O
septique	O
et	O
une	O
insuffisance	O
cardiaque	O
.	O
Il	O
est	O
enterré	O
au	O
Vatican	O
le	O
8	O
avril	O
.	O
Plus	O
de	O
3	O
millions	O
de	PERSON
personnes	PERSON
viennent	PERSON
à	PERSON
Rome	LOCATION
,	O
du	O
2	O
au	O
8	O
avril	O
2005	O
.	O
Celles	O
qui	O
vont	O
en	O
la	O
basilique	O
vaticane	O
,	O
saluer	PERSON
la	PERSON
dépouille	PERSON
du	PERSON
pape	PERSON
,	O
défilent	O
au	O
rythme	O
de	O
21000	O
à	O
l'	O
heure	O
,	O
soit	O
350	O
personnes	O
à	O
la	O
minute	O
.	O
On	O
estime	O
à	O
deux	O
milliards	O
le	O
nombre	O
de	O
personnes	O
qui	O
ont	O
vu	O
la	O
cérémonie	O
d'	O
enterrement	O
de	PERSON
Jean-Paul	PERSON
II	PERSON
à	O
travers	O
le	O
monde	O
.	O
De	PERSON
nombreux	PERSON
pays	O
décrètent	PERSON
une	PERSON
ou	PERSON
plusieurs	PERSON
journées	PERSON
de	PERSON
deuil	PERSON
à	PERSON
la	PERSON
suite	PERSON
du	O
décès	O
de	O
Jean-Paul	O
II	O
.	O
Dans	O
d'	O
autres	O
pays	O
,	O
dont	O
la	O
France	LOCATION
,	O
la	O
Suisse	O
et	O
la	O
Turquie	O
,	O
les	O
drapeaux	O
sont	O
mis	O
en	O
berne	O
sur	O
les	O
bâtiments	O
publics	O
.	O
Ces	PERSON
chiffres	PERSON
ne	PERSON
comprennent	O
pas	O
les	O
diverses	O
rencontres	O
qui	O
ont	O
lieu	O
en	O
clôture	O
de	O
cérémonies	O
liturgiques	O
,	O
tant	O
au	O
Vatican	O
que	O
de	O
par	O
le	O
monde	O
.	O
Il	O
a	O
également	O
fondé	O
l'	O
Académie	PERSON
pontificale	PERSON
pour	O
la	O
vie	O
et	O
l'	O
Académie	PERSON
pontificale	PERSON
des	PERSON
sciences	PERSON
sociales	O
.	O
Il	O
a	O
au	O
cours	O
des	O
9	O
consistoires	PERSON
créé	O
232	O
cardinaux	PERSON
et	PERSON
cherché	PERSON
à	PERSON
universaliser	PERSON
la	PERSON
Curie	PERSON
.	O
Jean-Paul	PERSON
II	PERSON
a	O
voulu	O
rendre	O
l'	O
administration	O
du	O
Vatican	O
universelle	O
.	O
Il	O
nomma	O
aux	O
postes	O
importants	O
de	PERSON
la	PERSON
Curie	PERSON
des	PERSON
cardinaux	PERSON
venant	PERSON
du	PERSON
monde	PERSON
entier	O
comme	O
Francis	PERSON
Arinze	PERSON
ou	PERSON
François	PERSON
Xavier	PERSON
Nguyen	PERSON
Van	PERSON
Thuan	PERSON
,	O
alors	O
que	O
l'	O
administration	O
était	O
principalement	O
italienne	O
avant	O
son	O
pontificat	PERSON
.	O
Il	O
n'	O
a	O
pas	O
fait	O
évoluer	O
la	O
pratique	O
des	O
synodes	O
des	O
évêques	O
,	O
et	O
convoqua	O
15	O
synodes	O
:	O
:	O
6	O
assemblées	O
générales	O
ordinaires	O
(	O
sur	O
la	O
famille	O
en	O
1980	O
,	O
la	O
réconciliation	O
en	O
1983	O
,	O
les	O
laïcs	O
en	O
1987	O
,	O
la	O
formation	O
des	O
prêtres	O
en	O
1990	O
,	O
la	O
vie	O
consacrée	O
en	O
1994	O
et	O
en	O
2001	O
sur	O
le	O
ministère	O
épiscopal	O
)	O
,	O
1	O
assemblée	O
générale	O
extraordinaire	O
(	O
sur	O
le	O
concile	O
Vatican	O
II	O
en	O
1985	O
)	O
,	O
7	O
assemblées	O
spéciales	O
et	O
un	O
synode	O
particulier	O
(	O
pour	O
les	O
Pays-Bas	O
en	O
1980	O
)	O
.	O
Il	O
a	O
consacré	O
environ	O
10000	O
audiences	O
aux	O
évêques	O
venus	O
à	O
Rome	LOCATION
.	O
Il	O
les	O
avait	O
parfois	O
rencontrées	O
pendant	O
ces	O
voyages	O
durant	O
le	O
concile	O
Vatican	O
II	O
.	O
Il	O
les	O
appuya	O
durant	O
son	O
pontificat	PERSON
malgré	PERSON
certaines	O
réticences	O
parmi	O
des	O
membres	O
de	O
la	O
Curie	O
.	O
Il	O
a	O
rendu	O
visite	O
à	O
317	O
des	O
333	O
paroisses	O
de	O
Rome	O
.	O
Alors	O
que	O
certains	O
de	O
ses	O
voyages	O
(	O
comme	O
aux	O
États-Unis	O
ou	O
à	O
Jérusalem	LOCATION
)	O
le	O
mènent	O
sur	O
les	O
traces	O
de	O
Paul	O
VI	O
,	O
beaucoup	O
d'	O
autres	O
pays	O
n'	O
avaient	O
jamais	O
été	O
visités	O
par	O
un	O
pape	O
.	O
Lui	PERSON
et	PERSON
l'	PERSON
archevêque	O
anglican	O
de	PERSON
Cantorbéry	PERSON
s'	PERSON
embrassent	O
devant	O
les	O
médias	O
dans	O
la	O
cathédrale	O
de	O
Cantorbéry	O
.	O
Plus	O
de	O
160	O
millions	O
de	O
personnes	O
sont	O
venues	O
à	O
Rome	LOCATION
pour	O
le	O
voir	O
.	O
Durant	PERSON
ses	O
voyages	O
,	O
il	LOCATION
montre	LOCATION
une	LOCATION
dévotion	O
particulière	O
envers	O
la	O
Vierge	PERSON
Marie	PERSON
,	O
visitant	O
de	O
nombreux	O
lieux	O
lui	O
étant	O
consacrés	PERSON
,	O
dont	O
Lourdes	PERSON
(	O
France	LOCATION
)	O
par	O
deux	O
fois	O
,	O
Fátima	PERSON
(	O
Portugal	LOCATION
)	O
,	O
Guadalupe	O
(	O
Mexique	LOCATION
)	O
.	O
Lors	O
de	O
manifestations	O
,	O
comme	O
les	O
Journées	O
mondiales	O
de	O
la	O
jeunesse	O
,	O
on	O
a	O
souvent	O
dépassé	O
le	O
nombre	O
du	O
million	O
de	O
personnes	O
présentes	O
.	O
Le	PERSON
pontificat	PERSON
de	PERSON
Jean-Paul	PERSON
II	PERSON
a	O
été	O
marqué	O
par	O
un	O
profond	O
engagement	O
social	O
.	O
Le	O
système	O
soviétique	O
anticlérical	O
fut	O
l'	O
objet	O
des	O
critiques	O
du	O
pape	O
dès	O
le	O
début	O
de	O
son	O
pontificat	PERSON
,	O
même	LOCATION
si	LOCATION
le	LOCATION
communisme	LOCATION
avait	LOCATION
déjà	LOCATION
été	LOCATION
condamné	O
par	O
Pie	O
IX	O
en	O
1937	O
.	O
Il	O
favorisa	O
en	O
Pologne	LOCATION
une	O
résistance	O
intransigeante	O
contre	O
le	O
communisme	O
.	O
Jean-Paul	PERSON
II	PERSON
s'	PERSON
est	PERSON
également	O
opposé	O
aux	LOCATION
inégalités	LOCATION
criantes	LOCATION
dans	LOCATION
le	LOCATION
monde	LOCATION
.	LOCATION
L'	O
attitude	O
de	PERSON
Jean-Paul	PERSON
II	PERSON
à	O
l'	O
égard	O
des	O
courants	O
proche	O
du	PERSON
marxisme	PERSON
,	O
et	O
notamment	O
la	O
théologie	O
de	O
la	O
libération	O
,	O
ainsi	O
que	O
sa	O
dénonciation	O
de	O
certains	O
régimes	O
dictatoriaux	O
,	O
tant	O
en	O
Amérique	LOCATION
,	O
qu'	O
en	O
Asie	LOCATION
,	O
ont	O
favorisé	O
,	O
selon	O
certains	O
,	O
la	O
transition	O
démocratique	O
en	O
Amérique	O
du	O
Sud	O
et	O
en	O
Asie	O
,	O
.	O
Jean	PERSON
Paul	PERSON
II	PERSON
répondit	O
:	O
"	O
Non	O
,	O
le	O
peuple	O
a	O
le	O
droit	O
de	O
jouir	O
de	O
ses	O
libertés	O
fondamentales	O
,	O
même	O
s'	O
il	O
commet	O
des	O
erreurs	O
dans	O
l'	O
exercice	O
de	O
celles-ci	O
.	O
"	O
Au	O
cours	O
de	O
cette	O
même	O
visite	O
le	O
pape	O
demanda	O
à	O
Augusto	PERSON
Pinochet	PERSON
,	O
lors	O
d'	O
un	O
entretien	O
en	O
privé	O
avec	O
lui	O
,	O
de	PERSON
démissionner	PERSON
et	PERSON
de	PERSON
rendre	PERSON
le	PERSON
pouvoir	O
à	O
la	O
société	O
civile	O
.	O
Le	PERSON
pontificat	PERSON
de	PERSON
Jean-Paul	PERSON
II	PERSON
s'	PERSON
est	O
caractérisé	PERSON
par	PERSON
une	PERSON
intensification	PERSON
des	PERSON
échanges	PERSON
avec	PERSON
les	PERSON
autres	PERSON
religions	O
.	O
Jean-Paul	PERSON
II	PERSON
a	O
grandi	O
dans	O
un	O
contexte	O
de	O
culture	O
juive	PERSON
florissante	PERSON
,	PERSON
son	O
intérêt	PERSON
pour	PERSON
elle	PERSON
datant	O
de	O
son	O
enfance	O
,	O
.	O
Son	O
premier	O
voyage	O
,	O
qui	PERSON
est	PERSON
aussi	PERSON
le	PERSON
premier	O
d'	O
un	O
pape	O
en	O
ce	O
lieu	O
,	O
est	O
à	O
Auschwitz	O
.	O
Il	LOCATION
est	LOCATION
le	LOCATION
premier	O
pape	O
à	O
visiter	O
une	O
synagogue	O
,	O
à	O
la	O
Grande	O
synagogue	O
de	O
Rome	O
en	O
avril	O
1986	O
,	O
.	O
Lors	O
d'	O
un	O
colloque	O
en	O
1997	O
,	O
Jean-Paul	PERSON
II	PERSON
affirme	O
qu'	O
un	O
"	O
examen	O
lucide	O
du	O
passé	O
(	O
...	O
)	O
peut	PERSON
démontrer	PERSON
clairement	PERSON
que	PERSON
l'	O
antisémitisme	O
est	O
sans	O
justification	O
aucune	O
et	O
est	O
absolument	O
répréhensible.	O
"	O
.	O
La	O
rédaction	O
par	O
une	O
partie	O
des	O
théologiens	O
juifs	O
du	O
document	O
Dabru	PERSON
Emet	PERSON
en	O
2000	O
,	O
qui	O
affirme	O
qu	O
'	O
"	O
un	O
nouveau	O
dialogue	O
religieux	O
avec	O
les	O
chrétiens	O
n'	O
affaiblirait	O
pas	O
la	O
pratique	O
juive	O
et	O
n'	O
accélérerait	O
pas	O
l'	O
assimilation	O
des	PERSON
juifs	PERSON
"	O
et	O
affirme	O
la	PERSON
volonté	PERSON
de	PERSON
dialogue	O
théologique	O
avec	O
les	O
chrétiens	O
montre	O
,	O
pour	O
certains	O
,	O
l'	O
impact	O
du	PERSON
pontificat	PERSON
de	PERSON
Jean-Paul	PERSON
II	PERSON
qui	PERSON
a	O
permis	O
de	O
favoriser	O
l'	O
émergence	O
de	PERSON
ce	PERSON
courant	O
juif	PERSON
dans	O
le	O
développement	O
du	O
dialogue	O
inter-religieux	O
.	O
Des	LOCATION
polémiques	LOCATION
émaillèrent	LOCATION
le	PERSON
pontificat	PERSON
de	PERSON
Jean-Paul	PERSON
II	PERSON
.	O
Un	O
Carmel	O
s'	O
était	O
établi	O
à	O
Auschwitz	O
.	O
Jean-Paul	PERSON
II	PERSON
finit	O
,	O
après	O
plusieurs	O
années	O
,	O
par	O
ordonner	O
aux	O
religieuses	O
de	O
déménager	O
,	O
afin	O
de	O
pacifier	O
les	O
relations	O
,	O
.	O
Jean	PERSON
Paul	PERSON
II	PERSON
devint	O
le	O
deuxième	O
pape	O
à	O
avoir	O
visité	O
la	O
Turquie	O
en	O
se	O
rendant	O
dans	LOCATION
ce	LOCATION
pays	O
en	O
novembre	O
1979	O
.	O
Le	O
pape	O
effectue	O
une	O
visite	O
en	O
1985	O
à	O
Casablanca	O
au	O
Maroc	O
.	O
Le	O
pape	O
a	O
effectué	O
une	O
visite	O
d'	O
une	O
journée	O
à	O
Tunis	LOCATION
le	O
14	O
avril	O
1996	O
.	O
Il	O
encourage	O
la	O
construction	O
d'	O
une	O
mosquée	O
à	O
Rome	LOCATION
,	O
tout	O
en	O
demandant	O
plus	O
de	O
réciprocité	O
dans	O
la	O
liberté	O
de	O
culte	O
des	O
pays	O
musulmans	O
.	O
En	O
mai	O
2001	O
,	O
Jean-Paul	PERSON
II	PERSON
est	PERSON
le	PERSON
premier	O
pape	O
à	O
se	O
rendre	O
dans	O
une	O
mosquée	PERSON
.	O
En	O
1999	O
,	O
Jean-Paul	PERSON
II	PERSON
visite	O
la	O
Roumanie	PERSON
avec	PERSON
les	PERSON
personnalités	PERSON
locales	PERSON
de	O
l'	O
Église	O
orthodoxe	O
.	O
Le	O
pape	O
fut	O
critiqué	O
du	O
fait	O
du	O
prosélytisme	O
des	O
catholiques	O
en	O
Russie	LOCATION
,	O
conduisant	O
au	O
refus	O
de	O
l'	O
épiscopat	O
russe	PERSON
de	PERSON
le	PERSON
recevoir	O
.	O
Ils	PERSON
parviennent	PERSON
ainsi	PERSON
à	PERSON
un	O
accord	O
sur	O
l'	O
un	O
des	O
points	O
principaux	O
de	O
divergences	O
issus	O
de	O
la	O
réforme	O
de	PERSON
Luther	PERSON
.	O
Jean	PERSON
Paul	PERSON
II	PERSON
développa	O
une	O
véritable	O
théologie	O
du	O
corps	O
au	O
cours	O
de	O
129	O
conférences	O
de	O
1979	O
à	O
1984	O
.	O
Cette	O
tendance	O
utilitariste	LOCATION
est	O
selon	PERSON
Jean-Paul	PERSON
II	PERSON
une	O
conséquence	O
du	O
péché	O
originel	O
.	O
La	O
sexualité	O
,	O
le	O
don	O
des	O
corps	O
selon	O
Jean-Paul	O
II	O
,	O
dans	O
l'	O
acte	O
conjugal	O
vient	O
donc	O
exprimer	O
et	O
réaliser	O
le	O
don	O
mutuel	O
que	O
les	O
époux	O
font	O
d'	O
eux-mêmes	O
et	O
de	O
toute	O
leurs	O
vies	O
.	O
Selon	PERSON
Jean-Paul	PERSON
II	PERSON
l'	O
homme	O
n'	O
est	O
pas	O
et	O
ne	PERSON
doit	PERSON
pas	PERSON
être	PERSON
maître	PERSON
de	PERSON
la	PERSON
vie	PERSON
,	O
mais	PERSON
dépositaire	PERSON
de	PERSON
la	PERSON
vie	PERSON
.	O
À	O
plusieurs	O
reprises	O
,	O
il	O
a	O
rappelé	O
l'	O
enseignement	O
de	O
l'	O
Église	O
concernant	O
l'	O
exigence	O
de	PERSON
fidélité	PERSON
conjugale	PERSON
et	O
la	O
recommandation	O
d'	O
éviter	O
les	O
méthodes	O
artificielles	O
de	O
contraception	O
.	O
Ainsi	O
quand	O
on	O
l'	O
interrogea	O
sur	O
la	O
possibilité	O
d'	O
utiliser	O
la	O
contraception	O
pour	O
éviter	PERSON
des	PERSON
avortements	PERSON
,	O
Jean-Paul	PERSON
II	PERSON
affirma	O
que	O
la	O
contraception	O
et	O
l'	O
avortement	PERSON
étaient	PERSON
les	PERSON
deux	PERSON
fruits	O
d'	O
une	O
même	PERSON
plante	PERSON
,	O
qui	O
conduit	O
à	O
nier	O
toute	O
la	O
vocation	O
à	O
l'	O
amour	O
présente	O
dans	O
le	O
mariage	O
.	O
Les	O
accusations	O
en	O
1998	O
contre	O
le	O
père	O
Marcial	PERSON
Maciel	PERSON
Degollado	PERSON
,	O
fondateur	O
des	O
Légionnaires	O
du	O
christ	O
,	O
n'	O
ont	O
pas	O
été	O
traitées	O
avec	O
suffisamment	O
de	O
moyens	O
et	O
de	O
rapidité	O
,	O
,	O
,	O
.	O
Cette	O
confiance	O
excessive	O
dans	O
la	O
personne	O
du	PERSON
père	PERSON
Marcial	PERSON
Maciel	PERSON
constitue	O
,	O
d'	O
après	O
George	PERSON
Weigel	PERSON
,	O
une	O
erreur	PERSON
de	PERSON
gouvernement	PERSON
du	PERSON
pape	PERSON
.	O
Les	O
allégations	O
d'	O
abus	O
sexuels	O
contre	O
le	O
cardinal	O
Hans	PERSON
Hermann	PERSON
Groër	PERSON
,	O
n'	O
ont	O
pas	O
non	O
plus	O
donné	O
lieu	O
à	O
une	O
enquête	O
immédiate	O
,	O
,	O
.	O
En	O
avril	O
2002	O
,	O
alors	O
que	O
le	O
scandale	O
des	O
abus	O
sexuels	O
de	O
prêtres	O
américains	O
sur	O
des	O
enfants	O
vient	O
d'	O
éclater	O
,	O
Jean-Paul	PERSON
II	PERSON
convoque	O
onze	O
cardinaux	LOCATION
,	O
tous	O
venus	O
des	O
États-Unis	O
.	O
Jean-Paul	PERSON
II	PERSON
voulait	PERSON
donc	PERSON
revivifier	O
la	PERSON
dévotion	PERSON
aux	PERSON
saints	PERSON
qui	PERSON
avait	PERSON
été	PERSON
un	PERSON
peu	PERSON
oublié	PERSON
après	PERSON
le	PERSON
Concile	PERSON
Vatican	O
II	O
,	O
la	O
vie	O
des	O
saints	O
étant	O
souvent	O
considéré	O
comme	O
exceptionnelle	O
et	O
éloignée	O
de	O
la	O
réalité	O
quotidienne	O
.	O
En	O
octobre	O
1986	O
,	O
il	PERSON
décide	PERSON
de	PERSON
constituer	PERSON
une	O
commission	O
de	PERSON
cardinaux	PERSON
et	PERSON
d'	PERSON
évêques	O
pour	O
préparer	O
un	O
projet	O
de	O
catéchisme	O
universel	O
romain	O
et	O
en	O
confie	O
la	O
présidence	O
au	PERSON
cardinal	PERSON
Ratzinger	PERSON
.	PERSON
Le	O
cardinal	O
autrichien	PERSON
Christoph	PERSON
Schönborn	O
en	O
sera	O
l'	O
un	O
des	O
principaux	O
rédacteurs	O
.	O
Le	PERSON
Catéchisme	PERSON
de	PERSON
l'	PERSON
Église	O
catholique	O
est	O
approuvé	O
officiellement	O
,	O
le	LOCATION
11	O
octobre	O
1992	O
,	O
par	O
le	O
pape	O
qui	O
le	O
considère	O
comme	O
un	O
acte	O
majeur	O
de	O
son	O
pontificat	PERSON
.	O
La	O
publication	O
du	PERSON
catéchisme	PERSON
de	PERSON
l'	PERSON
Église	O
catholique	O
avait	O
pour	O
objectif	O
de	O
montrer	O
que	O
le	O
catholicisme	O
pouvait	O
rendre	O
compte	O
de	O
la	O
foi	O
,	O
de	O
l'	O
amour	O
qui	O
sont	O
à	O
la	O
base	O
de	O
la	O
vie	O
chrétienne	O
.	O
Le	O
31	O
octobre	O
1992	O
il	PERSON
reconnaît	PERSON
les	PERSON
erreurs	PERSON
de	PERSON
la	PERSON
plupart	PERSON
des	PERSON
théologiens	PERSON
dans	O
la	O
condamnation	O
de	O
Galilée	O
en	O
1633	O
.	O
Le	O
13	O
mai	O
2005	O
,	O
seulement	O
41	O
jours	O
après	O
sa	O
mort	O
,	O
le	PERSON
jour	PERSON
du	PERSON
24	O
e	O
anniversaire	O
de	O
l'	O
attentat	O
accompli	O
contre	O
lui	O
place	O
Saint-Pierre	O
(	O
le	O
13	O
mai	O
1981	O
)	O
le	O
pape	O
Benoît	PERSON
XVI	PERSON
,	O
élu	O
le	O
19	O
avril	O
,	O
dispense	O
la	O
cause	O
en	O
béatification	O
de	PERSON
Jean-Paul	PERSON
II	PERSON
du	PERSON
délai	PERSON
de	PERSON
cinq	PERSON
ans	PERSON
avant	O
son	O
ouverture	O
.	O
Jean-Paul	PERSON
II	PERSON
avait	PERSON
lui-même	PERSON
ramené	PERSON
de	PERSON
trente	PERSON
ans	PERSON
(	O
code	O
de	PERSON
droit	PERSON
canonique	PERSON
de	PERSON
1917	PERSON
)	O
à	O
cinq	O
ans	O
après	O
la	O
mort	O
du	O
candidat	O
le	PERSON
délai	PERSON
requis	O
pour	O
l'	O
ouverture	O
d'	O
une	O
cause	O
.	O
Mais	PERSON
il	PERSON
avait	PERSON
aussi	PERSON
fait	PERSON
une	PERSON
exception	O
à	O
cette	PERSON
règle	PERSON
en	O
autorisant	O
,	O
en	O
1999	O
,	O
l'	O
ouverture	O
du	O
procès	O
diocésain	O
de	O
Mère	O
Teresa	O
,	O
deux	O
ans	O
seulement	O
après	O
sa	O
mort	O
.	O
Antoine	PERSON
de	PERSON
Padoue	PERSON
a	O
été	O
canonisé	O
un	O
an	O
après	O
sa	O
mort	O
,	O
mais	O
depuis	O
que	O
le	O
pape	O
Sixte	PERSON
Quint	PERSON
a	O
instauré	O
,	O
en	O
1588	O
,	O
la	O
procédure	O
moderne	O
de	O
canonisation	O
,	O
jamais	O
aucune	O
cause	O
n'	O
a	O
été	O
ouverte	O
aussi	O
vite	O
.	O
On	O
relève	O
également	O
des	O
critiques	O
concernant	O
la	O
couverture	O
des	O
affaires	O
de	PERSON
pédophilie	PERSON
de	PERSON
prêtres	PERSON
catholiques	PERSON
,	O
les	O
négociations	O
financières	O
opaques	O
avec	O
la	O
banque	O
Ambrosiano	O
et	O
les	O
sanctions	O
à	O
l'	O
encontre	O
d'	O
une	O
centaine	O
de	O
théologiens	O
catholiques	O
.	O
Avant	O
son	O
enterrement	PERSON
la	PERSON
crypte	PERSON
du	PERSON
Vatican	PERSON
recevait	O
1000	O
visites	O
par	O
jour	O
.	O
La	O
place	O
du	PERSON
parvis	PERSON
de	PERSON
Notre	PERSON
Dame	PERSON
de	PERSON
Paris	PERSON
s'	PERSON
appelle	O
désormais	PERSON
aussi	PERSON
"	O
Place	O
Jean	PERSON
Paul	PERSON
II	PERSON
"	O
.	O
Une	O
statue	O
en	O
bronze	O
de	O
9	O
mètres	O
de	O
haut	O
,	O
du	O
pape	O
Jean	PERSON
Paul	PERSON
II	PERSON
,	O
a	O
été	O
offerte	O
à	O
la	O
ville	O
de	O
Ploërmel	O
,	O
Morbihan	LOCATION
,	O
par	O
l'	O
artiste	O
russe	PERSON
Zurab	PERSON
Tsereteli	PERSON
,	O
nommé	O
citoyen	O
d'	O
honneur	O
de	O
la	O
ville	O
.	O
Cette	O
œuvre	O
d'	O
art	O
,	O
installée	O
au	O
centre	O
ville	O
,	O
place	O
Jean	PERSON
Paul	PERSON
II	PERSON
,	O
a	O
été	O
inaugurée	LOCATION
le	PERSON
dimanche	PERSON
après-midi	PERSON
10	O
décembre	O
2006	O
en	O
présence	O
de	O
2000	O
personnes	O
.	O
Les	O
seuls	O
écrits	O
officiels	O
de	O
Jean-Paul	O
II	O
représentent	O
55	O
volumes	O
auxquels	O
il	PERSON
faut	PERSON
ajouter	PERSON
des	O
publications	O
à	O
titre	O
personnel	O
et	O
sans	O
doute	O
des	O
milliers	O
de	O
lettres	O
et	O
documents	O
privés	O
divers	O
.	O
Jean-Paul	PERSON
II	PERSON
a	O
écrit	O
14	O
encycliques	O
:	O
Jean-Paul	PERSON
II	PERSON
a	O
écrit	O
:	O
La	O
prestation	O
de	PERSON
Piotr	PERSON
Adamczyk	PERSON
dans	PERSON
le	O
rôle	O
de	O
Jean-Paul	O
II	O
est	O
assez	O
étonnante	O
,	O
notamment	O
par	O
les	O
transformations	O
physiques	O
majeures	O
de	O
l'	O
acteur	O
pendant	O
le	O
déroulement	O
chronologique	O
du	O
film	O
(	O
vieillissement	O
du	O
visage	O
et	O
du	O
corps	O
)	O
.	O
