L'	O
Ultime	O
Secret	O
est	O
un	O
roman	O
de	O
Bernard	O
Werber	O
,	O
paru	O
en	O
2001	O
.	O
L'	O
intrigue	O
de	O
l'	O
histoire	O
nous	O
emmène	O
à	O
la	O
découverte	O
des	O
sources	O
de	O
la	O
motivation	O
et	O
de	O
l'	O
expérience	O
de	O
James	O
Olds	O
,	O
au	O
terme	O
d'	O
une	O
épopée	O
où	O
se	O
mèlent	O
LIS	PERSON
,	O
intelligence	O
artificielle	O
et	O
chirurgie	O
cérébrale	O
...	O
Cet	PERSON
homme	PERSON
aime	PERSON
à	PERSON
se	PERSON
faire	O
nommer	O
Ulysse	O
.	O
Ils	O
découvrent	O
également	O
l'	O
existence	O
de	O
l'	O
Ultime	O
Secret	O
,	O
qui	O
dépasserait	O
de	PERSON
bien	PERSON
loin	PERSON
toutes	O
les	O
raisons	O
du	O
monde	O
…	O
Cette	O
histoire	O
se	O
base	O
sur	LOCATION
une	O
expérience	O
réelle	O
,	O
celle	O
du	O
professeur	O
James	PERSON
Olds	PERSON
,	O
qui	O
découvrit	O
la	O
zone	O
du	O
cerveau	PERSON
qui	PERSON
provoque	PERSON
le	PERSON
plaisir	PERSON
dans	PERSON
les	O
années	O
1940	O
,	O
et	O
décida	O
avec	O
toute	PERSON
son	PERSON
équipe	PERSON
de	PERSON
sceller	PERSON
le	PERSON
secret	PERSON
autour	PERSON
de	O
ce	O
qui	O
aurait	O
pu	O
devenir	O
la	O
pire	O
drogue	O
du	O
monde	O
.	O
Dans	O
ce	O
livre	O
,	O
Bernard	PERSON
Werber	PERSON
répond	O
à	O
la	O
question	O
:	O
"	O
qu'	O
est	O
ce	O
qui	O
nous	O
motive	O
"	O
en	O
dressant	O
une	O
liste	O
des	O
motivations	O
humaines	O
,	O
de	O
la	O
plus	O
primaire	O
à	O
la	O
plus	O
évoluée	O
.	O
Sous	PERSON
couvert	PERSON
d'	PERSON
une	O
histoire	O
divertissante	O
,	O
Werber	PERSON
apporte	O
un	O
positionnement	O
sur	O
le	O
sexe	O
et	O
la	O
relation	O
amoureuse	O
que	O
l'	O
on	O
pourrait	O
croire	O
à	O
première	O
vue	O
nouveau	O
mais	O
fait	O
tout	O
simplement	O
référence	O
à	O
l'	O
amour	O
sacré	O
--	O
par	O
opposition	O
à	O
l'	O
amour	O
profane	O
--	O
.	O
En	O
ce	O
sens	O
Werber	PERSON
relate	O
simplement	O
un	O
éros	O
spiritualisé	O
entre	O
les	O
protagonistes	O
.	O
L'	O
originalité	O
du	O
livre	O
de	O
Werber	O
tient	O
plus	O
à	O
sa	O
façon	O
quasi	O
naïve	O
de	O
présenter	O
des	O
concepts	O
avancés	O
que	O
dans	O
la	O
trame	O
du	O
récit	O
.	O
