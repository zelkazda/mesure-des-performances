Les	O
années	O
1880	O
ont	O
marqué	O
la	O
naissance	O
du	PERSON
syndicalisme	PERSON
en	O
Europe	LOCATION
.	O
Lors	O
du	O
Régime	O
de	O
Vichy	O
la	O
conception	O
corporatiste	O
de	O
l'	O
organisation	O
du	O
travail	O
est	O
mise	O
en	O
avant	O
et	PERSON
divise	PERSON
le	PERSON
monde	PERSON
syndical	O
.	O
La	O
promulgation	O
en	O
1941	O
de	O
la	O
Charte	O
du	O
Travail	O
organise	O
la	O
dissolution	O
des	O
organisations	O
syndicales	O
existantes	O
et	O
la	O
création	O
de	O
syndicats	O
uniques	O
par	O
corporation	O
.	O
En	O
1948	O
,	O
un	O
courant	O
sécessionniste	O
de	O
la	O
CGT	O
,	O
réformiste	LOCATION
et	O
opposé	O
à	O
la	O
domination	O
du	O
Parti	O
communiste	O
français	O
sur	O
la	O
CGT	O
,	O
crée	O
la	O
CGT-FO	O
.	O
En	O
1992	O
,	O
le	O
mouvement	O
"	O
autonome	O
"	O
,	O
comportant	O
principalement	O
des	O
syndicats	O
qui	O
,	O
en	O
1947	O
,	O
avaient	O
refusé	O
de	O
choisir	O
entre	O
la	O
CGT	O
et	O
FO	O
,	O
s'	O
organise	O
dans	O
l'	O
UNSA	O
.	O
Il	PERSON
ne	PERSON
faut	PERSON
donc	PERSON
pas	O
confondre	O
syndicat	O
et	O
organisation	O
syndicale	O
(	O
par	O
exemple	O
la	O
CGT	O
,	O
ou	O
la	O
CFDT	O
)	O
.	O
Les	PERSON
résultats	PERSON
obtenus	PERSON
aux	O
élections	O
professionnelles	O
(	O
63,8	O
%	O
en	O
moyenne	O
en	O
France	LOCATION
lorsqu'	LOCATION
elles	O
sont	O
organisées	O
)	O
sont	O
un	PERSON
baromètre	PERSON
de	PERSON
représentativité	PERSON
institué	PERSON
par	PERSON
la	PERSON
loi	PERSON
du	PERSON
20	PERSON
août	O
2008	O
.	O
Alors	O
que	O
le	O
taux	O
de	O
syndicalisation	O
dans	O
le	O
secteur	O
privé	O
est	O
à	O
peine	O
supérieur	O
à	O
5	O
%	O
(	O
alors	O
que	O
les	O
autres	O
pays	O
européens	O
sont	O
aux	O
alentours	O
de	O
30	O
,	O
voire	O
50	O
%	O
)	O
,	O
la	O
France	LOCATION
a	O
un	O
paysage	O
syndical	O
divisé	O
,	O
constitué	O
de	O
cinq	O
confédérations	O
qui	O
bénéficiaient	O
d'	O
une	O
présomption	O
irréfragable	O
de	O
représentativité	O
jusqu'	O
à	O
la	O
loi	O
du	O
20	O
août	O
2008	O
ainsi	O
que	O
les	O
trois	O
autres	O
principales	O
organisations	O
non	O
représentatives	O
de	O
droit	O
.	O
En	O
France	O
,	O
toutes	O
les	O
avancées	O
sociales	O
bénéficient	O
à	O
tous	O
.	O
Selon	O
un	O
sondage	O
TNS	O
Sofres	O
de	O
décembre	O
2005	O
,	O
les	O
causes	O
de	O
non-syndicalisation	O
sont	O
:	O
On	O
parle	O
au	O
Royaume-Uni	O
de	O
"	O
trade	O
unions	O
"	O
ou	O
"	O
labour	O
unions	O
"	O
pour	O
désigner	O
les	O
syndicats	O
.	O
