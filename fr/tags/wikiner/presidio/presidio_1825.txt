C'	O
est	O
en	O
Serbie	LOCATION
,	O
vers	O
1725	O
que	O
le	O
mot	O
"	O
vampire	O
"	O
fait	O
son	O
apparition	O
.	O
À	O
la	O
même	O
époque	O
,	O
le	O
bénédictin	O
lorrain	O
Augustin	PERSON
Calmet	PERSON
,	O
décrit	O
le	O
vampire	O
comme	O
un	O
"	O
revenant	O
en	O
corps	O
"	O
,	O
se	PERSON
distinguant	PERSON
ainsi	PERSON
des	PERSON
revenants	PERSON
immatériels	PERSON
(	O
fantômes	O
ou	O
esprits	O
)	O
.	O
Le	O
livre	O
remporta	O
un	O
grand	O
succès	O
mais	O
c'	O
est	O
l'	O
ouvrage	O
que	O
Bram	PERSON
Stoker	PERSON
écrivit	O
en	O
1897	O
,	O
Dracula	PERSON
,	O
qui	O
reste	O
la	O
quintessence	O
du	O
genre	O
,	O
établissant	O
une	O
image	O
du	PERSON
vampire	PERSON
toujours	PERSON
populaire	PERSON
de	PERSON
nos	PERSON
jours	PERSON
dans	O
les	O
ouvrages	O
de	O
fiction	O
,	O
même	O
s'	O
il	O
est	O
assez	O
éloigné	PERSON
de	PERSON
ses	PERSON
ancêtres	PERSON
folkloriques	PERSON
dont	O
il	LOCATION
ne	LOCATION
conserve	O
que	O
peu	O
des	O
spécificités	O
originelles	O
.	O
Dans	O
la	O
Grèce	O
antique	O
,	O
les	O
ombres	O
du	O
royaume	O
d'	O
Hadès	O
sont	O
friandes	O
du	O
sang	O
des	O
victimes	O
.	O
Aristée	O
,	O
Platon	O
et	O
Démocrite	O
soutenaient	O
que	O
l'	O
âme	O
peut	PERSON
demeurer	PERSON
auprès	PERSON
des	PERSON
morts	PERSON
privés	PERSON
de	PERSON
sépulture	PERSON
.	O
On	O
peut	O
se	O
référer	O
à	O
Porphyre	PERSON
de	PERSON
Tyr	PERSON
(	O
Des	O
sacrifices	O
,	O
ch	O
.	O
En	PERSON
Crète	PERSON
,	O
selon	PERSON
Pausanias	PERSON
,	O
on	O
enfonçait	O
dans	O
la	O
tête	O
de	O
certains	O
morts	O
un	PERSON
clou	PERSON
.	O
Ovide	PERSON
aussi	PERSON
parlera	PERSON
des	PERSON
vampires	PERSON
.	O
Théocrite	PERSON
évoque	PERSON
aussi	PERSON
les	PERSON
empuses	PERSON
(	O
spectres	O
multiformes	PERSON
de	PERSON
la	PERSON
nuit	PERSON
pouvant	O
se	O
muer	O
en	O
monstres	O
innommables	O
ou	O
en	O
créatures	O
de	O
rêve	O
,	O
aussi	O
appelées	O
démons	O
de	O
midi	O
)	O
.	O
De	PERSON
Lamia	PERSON
viennent	PERSON
les	PERSON
lamies	PERSON
,	O
plus	O
nécrophages	O
que	O
vampires	O
:	O
lascives	O
,	O
ondoyantes	O
,	O
serpentines	O
,	O
avides	O
de	O
stupre	O
et	O
de	O
mort	O
,	O
aux	PERSON
pieds	PERSON
de	PERSON
cheval	PERSON
et	PERSON
aux	PERSON
yeux	PERSON
de	PERSON
dragon	PERSON
.	PERSON
La	PERSON
comtesse	PERSON
Élisabeth	PERSON
Báthory	PERSON
a	O
grandement	O
inspiré	O
les	O
légendes	O
de	O
vampires	O
.	O
Des	PERSON
luttes	PERSON
de	PERSON
pouvoir	PERSON
de	PERSON
l'	PERSON
époque	PERSON
il	PERSON
nous	PERSON
reste	PERSON
ces	PERSON
écrits	PERSON
plus	O
ou	O
moins	O
diffamatoires	O
qui	O
ont	O
fait	O
entrer	O
Vlad	PERSON
III	PERSON
Basarab	PERSON
dans	PERSON
l'	PERSON
histoire	PERSON
.	O
Il	O
reste	O
cependant	PERSON
connu	PERSON
pour	O
l'	O
imaginaire	O
collectif	O
sous	PERSON
le	PERSON
nom	PERSON
de	PERSON
Vlad	PERSON
l'	O
empaleur	O
.	O
Les	O
nombreuses	O
reprises	O
littéraires	O
et	PERSON
cinématographiques	PERSON
ont	PERSON
fini	PERSON
par	PERSON
faire	O
de	O
Dracula	O
un	O
personnage	O
de	O
la	O
culture	O
populaire	O
mondiale	O
.	O
Mais	PERSON
le	PERSON
premier	O
personnage	O
qui	O
attira	O
l'	O
attention	O
fut	O
Lord	PERSON
Ruthven	PERSON
,	O
créé	PERSON
par	PERSON
John	PERSON
William	PERSON
Polidori	PERSON
en	O
1819	O
dans	O
une	O
longue	O
nouvelle	O
intitulée	O
Le	O
Vampire	O
.	O
Un	O
défi	O
fut	O
lancé	O
par	O
Lord	O
Byron	PERSON
lors	PERSON
d'	PERSON
une	O
journée	PERSON
pluvieuse	PERSON
à	PERSON
,	O
entre	PERSON
autres	PERSON
,	O
Percy	PERSON
(	O
qui	O
refusa	O
)	O
et	O
son	O
épouse	O
Mary	PERSON
Shelley	PERSON
,	O
avec	LOCATION
le	LOCATION
but	O
d'	O
écrire	O
une	O
nouvelle	O
mettant	O
en	O
scène	O
un	PERSON
mort-vivant	PERSON
.	O
Mary	PERSON
Shelley	PERSON
engendra	PERSON
d'	PERSON
ailleurs	PERSON
Frankenstein	PERSON
.	O
De	PERSON
fait	PERSON
,	O
la	O
paternité	LOCATION
de	LOCATION
ce	LOCATION
récit	LOCATION
fut	LOCATION
âprement	PERSON
disputée	PERSON
entre	PERSON
les	PERSON
deux	PERSON
écrivains	PERSON
et	PERSON
fut	PERSON
finalement	O
attribuée	O
à	O
Lord	O
Byron	PERSON
.	O
Avec	O
sa	O
publication	O
,	O
le	LOCATION
thème	LOCATION
du	LOCATION
vampirisme	LOCATION
devient	LOCATION
alors	LOCATION
incontournable	O
et	O
de	O
nombreux	O
auteurs	O
britanniques	O
,	O
allemands	O
,	O
français	O
s'	O
y	O
essaient	O
:	O
Théophile	PERSON
Gautier	PERSON
,	O
Hoffman	PERSON
,	O
Tolstoï	LOCATION
,	O
etc	O
.	O
Le	O
virage	O
suivant	O
est	O
pris	O
par	O
Sheridan	PERSON
Le	PERSON
Fanu	PERSON
avec	PERSON
Carmilla	O
en	O
1872	O
.	O
Il	LOCATION
présente	LOCATION
le	LOCATION
vampire	PERSON
comme	PERSON
une	PERSON
victime	O
de	O
son	O
propre	O
état	PERSON
et	O
s'	O
oppose	O
du	O
même	O
coup	O
au	O
bien-pensant	O
de	O
la	O
Grande-Bretagne	O
en	O
abordant	O
le	O
lesbianisme	O
du	O
personnage	O
,	O
sachant	O
que	O
l'	O
homosexualité	O
était	O
fortement	O
condamnée	O
.	O
En	O
1897	O
,	O
Bram	PERSON
Stoker	PERSON
crée	O
Dracula	PERSON
qui	PERSON
sacre	PERSON
le	O
vampire	O
personnage	O
de	O
fiction	O
à	O
part	O
entière	O
.	O
Dans	O
cette	O
série	O
,	O
Anne	PERSON
Rice	PERSON
donne	PERSON
une	PERSON
interprétation	O
originale	O
des	O
origines	O
des	O
vampires	O
,	O
et	O
axe	O
une	O
bonne	O
partie	O
de	O
l'	O
œuvre	O
autour	O
des	O
interrogations	O
métaphysiques	O
et	O
morales	O
qui	O
peuvent	O
tenailler	O
les	O
vampires	O
.	O
Dans	PERSON
Je	PERSON
suis	PERSON
une	PERSON
légende	PERSON
,	O
son	O
premier	O
roman	PERSON
,	O
Richard	PERSON
Matheson	PERSON
met	O
en	O
scène	O
le	O
dernier	O
humain	O
vivant	O
dans	O
un	PERSON
monde	PERSON
peuplé	PERSON
de	PERSON
vampires	PERSON
,	O
tout	O
en	O
prétendant	O
apporter	O
une	O
explication	O
scientifique	O
à	O
l'	O
existence	O
de	O
ces	O
derniers	O
.	O
Plus	O
récemment	O
,	O
dans	O
la	O
Saga	O
du	O
désir	O
interdit	O
(	O
Twilight	O
en	O
anglais	O
)	O
débutée	O
en	O
2005	O
,	O
Stephenie	O
Meyer	O
,	O
certaines	O
créatures	O
nommées	O
des	O
vampires	O
bien	O
que	O
très	O
différentes	O
du	O
vampire	O
traditionnel	O
vivent	O
parmi	O
les	O
humains	O
et	O
mènent	O
une	O
vie	O
normale	O
en	O
se	O
nourrissant	O
uniquement	O
de	O
sang	O
animal	O
,	O
quand	O
d'	O
autres	O
suivent	O
le	O
régime	O
alimentaire	O
traditionnel	O
.	O
Après	PERSON
des	PERSON
représentations	O
du	O
Dracula	O
de	O
Bram	O
Stoker	O
au	O
théâtre	O
,	O
le	O
mythe	O
fut	LOCATION
porté	LOCATION
à	O
l'	O
écran	O
.	O
Le	O
premier	O
film	O
fut	O
Nosferatu	O
le	O
Vampire	O
par	O
Friedrich	PERSON
Murnau	PERSON
en	O
1922	O
.	O
Ce	O
film	O
lui	PERSON
valut	PERSON
des	O
poursuites	O
judiciaires	O
de	O
la	O
part	O
de	O
la	O
veuve	O
de	O
Stoker	O
qui	O
estimait	O
que	O
le	O
film	O
était	O
une	O
adaptation	O
du	O
livre	O
et	O
que	O
Murnau	PERSON
aurait	PERSON
dû	PERSON
en	PERSON
acheter	O
les	O
droits	O
pour	O
le	LOCATION
porter	LOCATION
à	O
l'	O
écran	O
.	O
Vampyr	PERSON
,	O
ou	O
l'	O
Étrange	O
Aventure	O
de	PERSON
David	PERSON
Gray	PERSON
est	PERSON
un	O
film	O
danois	O
de	O
Carl	O
Theodor	O
Dreyer	O
sorti	O
en	O
1932	O
.	O
En	O
1931	O
,	O
Bela	PERSON
Lugosi	PERSON
joue	PERSON
pour	PERSON
la	PERSON
première	O
fois	O
Dracula	O
dans	O
un	O
film	O
de	PERSON
Tod	PERSON
Browning	PERSON
.	O
Pour	O
l'	O
anecdote	O
,	O
Bela	PERSON
Lugosi	PERSON
fut	PERSON
enterré	PERSON
avec	O
la	O
cape	O
de	O
Dracula	O
.	O
Lee	PERSON
joua	PERSON
ce	PERSON
rôle	PERSON
dans	PERSON
une	O
dizaine	O
de	O
films	O
.	O
à	O
la	O
fin	O
des	O
années	O
1960	O
et	O
au	O
début	O
des	O
années	O
1970	O
,	O
le	O
cinéaste	O
français	O
Jean	PERSON
Rollin	PERSON
contribua	PERSON
à	O
érotiser	O
très	O
fortement	O
le	O
mythe	O
dans	O
des	O
réalisations	O
d'	O
un	PERSON
esthétisme	PERSON
très	PERSON
personnel	O
.	O
Au	O
début	O
des	O
années	O
1990	O
,	O
le	LOCATION
thème	LOCATION
des	LOCATION
vampires	LOCATION
reviendra	O
en	O
force	O
sur	O
les	O
écrans	O
avec	O
Dracula	O
de	O
Francis	O
Ford	O
Coppola	O
,	O
en	O
1992	O
,	O
et	O
Entretien	PERSON
avec	PERSON
un	PERSON
vampire	PERSON
de	PERSON
Neil	PERSON
Jordan	PERSON
en	O
1994	O
.	O
Les	O
vampires	O
les	O
plus	O
connus	O
à	O
la	O
télévision	O
sont	O
issus	O
du	O
monde	O
créé	O
par	O
Joss	PERSON
Whedon	PERSON
dans	O
les	O
séries	O
Buffy	O
contre	O
les	O
vampires	O
et	O
Angel	O
.	O
Dans	O
la	O
série	O
Kindred	O
:	O
le	O
Clan	O
des	O
maudits	O
,	O
inspirée	PERSON
de	PERSON
l'	PERSON
univers	O
du	PERSON
jeu	PERSON
de	PERSON
rôle	PERSON
Vampire	PERSON
:	O
la	O
Mascarade	O
,	O
des	O
clans	O
de	PERSON
vampires	PERSON
s'	PERSON
affrontent	O
dans	O
la	O
ville	O
de	O
San	LOCATION
Francisco	LOCATION
.	O
Par	O
ailleurs	O
,	O
Murnau	PERSON
comme	PERSON
les	PERSON
autres	PERSON
cinéastes	PERSON
ne	PERSON
détaillent	PERSON
pas	PERSON
autant	PERSON
les	PERSON
facultés	O
des	O
vampires	O
--	O
par	O
souci	O
d'	O
alléger	PERSON
l'	O
intrigue	O
,	O
très	O
certainement	O
.	O
Mais	PERSON
ils	PERSON
leur	PERSON
en	PERSON
prêtent	O
d'	O
autres	O
;	O
ainsi	O
,	O
les	O
films	O
dans	O
lesquels	O
a	PERSON
joué	PERSON
Bela	PERSON
Lugosi	PERSON
ont	PERSON
développé	PERSON
l'	PERSON
idée	PERSON
que	PERSON
les	PERSON
vampires	PERSON
possédaient	PERSON
un	PERSON
pouvoir	PERSON
hypnotique	PERSON
leur	PERSON
permettant	O
,	O
notamment	O
,	O
de	O
séduire	O
efficacement	O
les	O
femmes	O
.	O
