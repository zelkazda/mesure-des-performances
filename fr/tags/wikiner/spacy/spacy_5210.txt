Le	O
terme	O
"	O
fractale	O
"	O
est	O
un	O
néologisme	O
créé	O
par	O
Benoît	PERSON
Mandelbrot	PERSON
en	O
1974	O
à	O
partir	O
de	O
la	O
racine	O
latine	O
fractus	O
,	O
qui	O
signifie	O
brisé	O
,	O
irrégulier	O
(	O
fractales	O
n.f	O
)	O
.	O
Dans	O
la	O
"	O
théorie	O
de	O
la	O
rugosité	O
"	O
développée	O
par	O
Mandelbrot	PERSON
,	O
une	O
fractale	O
désigne	O
des	O
objets	O
dont	O
la	O
structure	O
est	O
liée	O
à	O
l'	O
échelle	O
.	O
