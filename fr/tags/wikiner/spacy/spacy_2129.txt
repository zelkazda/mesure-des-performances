Ses	O
dates	O
de	O
naissance	O
et	O
de	O
mort	O
ont	O
été	O
fournies	O
par	O
Vasari	PERSON
dans	O
ses	O
Vite	MISC
.	O
On	O
dit	O
que	O
le	O
surnom	O
de	O
Giorgione	PERSON
lui	O
fut	O
donné	O
par	O
Vasari	PERSON
"	O
pour	O
son	O
allure	O
et	O
sa	O
grandeur	O
d'	O
âme	O
"	O
.	O
Giorgione	PERSON
est	O
connu	O
pour	O
la	O
qualité	O
romantique	O
de	O
son	O
travail	O
,	O
et	O
pour	O
le	O
fait	O
que	O
très	O
peu	O
de	O
peintures	O
(	O
autour	O
de	O
six	O
)	O
soient	O
reconnues	O
comme	O
étant	O
de	O
sa	O
main	O
.	O
L'	O
incertitude	O
résultant	O
de	O
la	O
difficulté	O
à	O
identifier	O
ses	O
œuvres	O
et	O
de	O
la	O
signification	O
de	O
son	O
art	O
a	O
fait	O
de	O
Giorgione	PERSON
la	O
figure	O
la	O
plus	O
mystérieuse	O
dans	O
la	O
peinture	O
occidentale	O
.	O
Ceci	O
est	O
peint	O
avec	O
de	O
minuscules	O
taches	O
de	O
couleur	O
technique	O
que	O
Giorgione	PERSON
a	O
introduit	O
dans	O
la	O
peinture	O
à	O
l'	O
huile	O
,	O
dérivées	O
des	O
techniques	O
d'	O
illumination	O
de	O
manuscrit	O
.	O
Celles-ci	O
ont	O
donné	O
aux	O
œuvres	O
de	O
Giorgione	PERSON
la	O
lueur	O
"	O
magique	O
"	O
de	O
la	O
lumière	O
pour	O
laquelle	O
elles	O
étaient	O
célèbres	O
.	O
est	O
une	O
toile	O
appartenant	O
aux	O
poesie	O
,	O
genre	O
créé	O
par	O
Giorgione	PERSON
.	O
Par	O
cette	O
maturation	O
de	O
l'	O
idée	O
sur	O
la	O
toile	O
Giorgione	PERSON
confirme	O
son	O
opposition	O
vénitienne	O
aux	O
pratiques	O
florentines	O
,	O
Florence	LOCATION
étant	O
adepte	O
du	O
dessin	O
préparatoire	O
à	O
l'	O
œuvre	O
.	O
Giorgione	PERSON
a	O
peint	O
sur	O
la	O
droite	O
une	O
femme	O
qui	O
allaite	O
un	O
enfant	O
et	O
,	O
sur	O
la	O
gauche	O
,	O
se	O
trouve	O
un	O
homme	O
debout	O
qui	O
les	O
regarde	O
.	O
Plusieurs	O
interprétations	O
ont	O
été	O
apportées	O
pour	O
comprendre	O
ce	O
que	O
Giorgione	PERSON
a	O
voulu	O
représenter	O
;	O
voici	O
par	O
exemple	O
trois	O
lectures	O
possibles	O
:	O
Sont	O
également	O
présentées	O
dans	O
cette	O
exposition	O
des	O
œuvres	O
de	O
peintres	O
que	O
Giorgione	PERSON
a	O
connu	O
:	O
Giovanni	PERSON
Bellini	PERSON
,	O
Dürer	PERSON
,	O
Titien	PERSON
,	O
Raphaël	PERSON
,	O
Lorenzo	PERSON
Costa	PERSON
,	O
Cima	PERSON
da	PERSON
Conegliano	PERSON
,	O
Palma	PERSON
le	PERSON
Vieux	PERSON
,	O
Le	PERSON
Pérugin	PERSON
…	O
