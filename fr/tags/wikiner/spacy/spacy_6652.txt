De	O
père	O
anglais	O
et	O
de	O
mère	O
flamande	O
,	O
il	O
est	O
né	O
dans	O
une	O
famille	O
de	O
la	O
petite-bourgeoisie	O
d'	O
Ostende	LOCATION
,	O
Ensor	PERSON
quitte	O
peu	O
sa	O
ville	O
natale	O
il	O
meurt	O
dans	O
sa	O
ville	O
natale	O
...	O
Dans	O
la	O
maison	O
familiale	O
où	O
,	O
célibataire	O
convaincu	O
,	O
il	O
vivra	O
jusqu'	O
en	O
1917	O
,	O
Ensor	PERSON
s'	O
installe	O
un	O
cabinet	O
dans	O
les	O
combles	O
et	O
commence	O
à	O
peindre	O
des	O
portraits	O
réalistes	O
ou	O
des	O
paysages	O
inspirés	O
par	O
l'	O
impressionnisme	O
.	O
En	O
1883	O
,	O
Octave	PERSON
Maus	PERSON
fonde	O
le	O
cercle	O
artistique	O
d'	O
avant-garde	O
"	O
Les	MISC
XX	MISC
"	O
et	O
Ensor	PERSON
peint	O
son	O
premier	O
tableau	O
de	O
masques	O
,	O
et	O
un	O
autoportrait	O
auquel	O
il	O
ajoutera	O
plus	O
tard	O
le	O
"	O
chapeau	O
fleuri	O
"	O
.	O
À	O
33	O
ans	O
,	O
Ensor	PERSON
est	O
déjà	O
un	O
homme	O
du	O
passé	O
.	O
Les	O
premières	O
demeures	O
de	O
Victor	PERSON
Horta	PERSON
symbolisent	O
un	O
nouvel	O
art	O
de	O
vivre	O
.	O
Ensor	PERSON
doit	O
attendre	O
le	O
début	O
du	O
siècle	O
suivant	O
,	O
alors	O
qu'	O
il	O
a	O
donné	O
le	O
meilleur	O
,	O
pour	O
assister	O
à	O
la	O
reconnaissance	O
de	O
son	O
œuvre	O
:	O
expositions	O
internationales	O
,	O
visite	O
royale	O
,	O
anoblissement	O
--	O
il	O
est	O
fait	O
baron	O
--	O
,	O
Légion	ORGANIZATION
d'	ORGANIZATION
honneur	ORGANIZATION
.	O
Si	O
la	O
vie	O
privée	O
d'	O
Ensor	PERSON
reste	O
mal	O
connue	O
,	O
c'	O
est	O
parce	O
que	O
l'	O
artiste	O
l'	O
a	O
désiré	O
ainsi	O
.	O
Avec	O
son	O
retour	O
chez	O
sa	O
mère	O
,	O
Ensor	PERSON
est	O
fasciné	O
par	O
la	O
lumière	O
de	O
la	O
cité	O
balnéaire	O
qui	O
lui	O
inspire	O
des	O
pâleurs	O
secrètes	O
.	O
Ensor	PERSON
sculpte	O
la	O
lumière	O
et	O
est	O
fasciné	O
par	O
le	O
pouvoir	O
de	O
recréer	O
les	O
choses	O
ou	O
de	O
les	O
vider	O
de	O
leur	O
contenu	O
familier	O
:	O
"	O
La	O
lumière	O
déforme	O
le	O
contour	O
.	O
Comme	O
chez	O
Pieter	PERSON
Bruegel	PERSON
l'	PERSON
Ancien	PERSON
ou	O
Jérôme	PERSON
Bosch	PERSON
,	O
l'	O
inanimé	O
respire	O
et	O
crie	O
.	O
Dans	O
un	O
but	O
purement	O
alimentaire	O
,	O
il	O
édite	O
des	O
eaux-fortes	O
,	O
les	O
fameux	O
"	O
biftecks	O
d'	O
Ensor	PERSON
"	O
,	O
œuvres	O
purement	O
commerciales	O
mais	O
qui	O
ont	O
fait	O
alors	O
la	O
fierté	O
des	O
marchands	O
de	O
souvenirs	O
.	O
Il	O
réalise	O
aussi	O
des	O
caricatures	O
,	O
laissant	O
libre	O
cours	O
à	O
sa	O
verve	O
gouailleuse	O
,	O
avec	O
un	O
trait	O
racé	O
,	O
canaille	O
et	O
pourfendeur	O
à	O
la	O
manière	O
de	O
Bruegel	PERSON
et	O
de	O
Bosch	PERSON
.	O
Par	O
sa	O
prédilection	O
pour	O
les	O
personnages	O
masqués	O
,	O
les	O
squelettes	O
,	O
qui	O
,	O
dans	O
ses	O
tableaux	O
,	O
grouillent	O
dans	O
une	O
atmosphère	O
de	O
carnaval	O
,	O
Ensor	PERSON
est	O
le	O
père	O
d'	O
un	O
monde	O
imaginaire	O
et	O
fantastique	O
qui	O
annonce	O
le	O
surréalisme	O
.	O
Ensor	PERSON
eut	O
une	O
grande	O
influence	O
sur	O
Michel	PERSON
de	PERSON
Ghelderode	PERSON
qui	O
fut	O
inspiré	O
pour	O
ses	O
œuvres	O
théâtrales	O
par	O
les	O
masques	O
et	O
les	O
figures	O
d'	O
Ensor	PERSON
.	O
La	O
bande	O
dessinée	O
de	O
Jan	PERSON
Bucquoy	PERSON
Le	MISC
Bal	MISC
du	MISC
rat	MISC
mort	MISC
et	O
le	O
film	O
Camping	MISC
Cosmos	MISC
sont	O
directement	O
inspirés	O
par	O
les	O
dessins	O
d'	O
Ensor	PERSON
(	O
carnaval	O
,	O
bal	O
masqué	O
)	O
.	O
