Il	O
est	O
le	O
fils	O
de	O
l'	O
écrivain	O
Andrée	PERSON
Chedid	PERSON
et	O
le	O
père	O
du	O
chanteur	O
Matthieu	PERSON
Chedid	PERSON
(	O
plus	O
connu	O
sous	O
le	O
nom	O
de	O
-M-	O
)	O
.	O
Né	O
à	O
Ismaïlia	LOCATION
,	O
Louis	PERSON
Chedid	PERSON
,	O
fils	O
d'	O
un	O
scientifique	O
et	O
de	O
la	O
célèbre	O
écrivain	O
franco-libanaise	O
Andrée	PERSON
Chedid	PERSON
,	O
s'	O
installe	O
avec	O
sa	O
famille	O
à	O
Paris	LOCATION
six	O
mois	O
après	O
sa	O
naissance	O
.	O
De	O
retour	O
à	O
Paris	LOCATION
,	O
il	O
devient	O
stagiaire-monteur	O
et	O
assistant	O
en	O
1970	O
.	O
Toujours	O
,	O
en	O
1970	O
,	O
sa	O
fille	O
Émilie	PERSON
naît	O
.	O
Son	O
fils	O
Matthieu	PERSON
naîtra	O
un	O
an	O
plus	O
tard	O
.	O
Dès	O
lors	O
,	O
il	O
se	O
lance	O
dans	O
la	O
musique	O
(	O
on	O
le	O
sait	O
guitariste	O
,	O
amateur	O
de	O
Django	PERSON
Reinhardt	PERSON
)	O
.	O
L'	O
année	O
suivante	O
,	O
il	O
signe	O
chez	O
CBS	ORGANIZATION
et	O
signe	O
trois	O
albums	O
par	O
an	O
.	O
En	O
1981	O
,	O
Ainsi	MISC
soit-il	MISC
est	MISC
aux	MISC
premières	MISC
places	MISC
du	MISC
hit-parade	MISC
et	O
fait	O
véritablement	O
ses	O
premières	O
scènes	O
.	O
En	O
1984	O
,	O
il	O
signe	O
la	O
bande	O
originale	O
de	O
Pinot	MISC
simple	MISC
flic	MISC
de	O
Gérard	PERSON
Jugnot	PERSON
.	O
Il	O
est	O
également	O
le	O
compositeur	O
du	O
conte	O
musical	O
Le	MISC
Soldat	MISC
Rose	MISC
,	O
dont	O
les	O
chansons	O
sont	O
interprétées	O
entre	O
autres	O
par	O
-M-	O
,	O
Sanseverino	PERSON
,	O
Vanessa	PERSON
Paradis	PERSON
,	O
Jeanne	PERSON
Cherhal	PERSON
,	O
Francis	PERSON
Cabrel	PERSON
,	O
Alain	PERSON
Souchon	PERSON
et	O
Bénabar	PERSON
.	O
