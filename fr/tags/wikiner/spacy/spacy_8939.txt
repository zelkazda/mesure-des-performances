Alyssa	PERSON
Milano	PERSON
est	O
une	O
actrice	O
,	O
productrice	O
,	O
et	O
ancienne	O
chanteuse	O
née	O
le	O
19	O
décembre	O
1972	O
à	O
New-York	LOCATION
(	O
États-Unis	LOCATION
)	O
et	O
connue	O
avant	O
tout	O
pour	O
ses	O
rôles	O
dans	O
les	O
séries	O
télévisées	O
Madame	MISC
est	MISC
servie	MISC
,	O
Melrose	MISC
Place	MISC
et	O
Charmed	MISC
.	O
Alyssa	PERSON
Jayne	PERSON
Milano	PERSON
est	O
née	O
le	O
19	O
décembre	O
1972	O
dans	O
le	O
quartier	O
de	O
Brooklyn	LOCATION
à	O
New	LOCATION
York	LOCATION
.	O
Alyssa	PERSON
a	O
commencé	O
sa	O
carrière	O
artistique	O
à	O
l'	O
âge	O
de	O
huit	O
dans	O
la	O
comédie	O
musicale	O
Annie	MISC
.	O
Elle	O
joue	O
aussi	O
dans	O
la	O
première	O
adaptation	O
musicale	O
de	O
Jane	MISC
Eyre	MISC
.	O
Elle	O
donne	O
la	O
réplique	O
à	O
Tony	PERSON
Danza	PERSON
,	O
qui	O
incarne	O
son	O
père	O
à	O
l'	O
écran	O
,	O
ainsi	O
qu'	O
à	O
Judith	PERSON
Light	PERSON
,	O
Katherine	PERSON
Helmond	PERSON
et	O
Danny	PERSON
Pintauro	PERSON
.	O
Sa	O
famille	O
s'	O
installe	O
à	O
Hollywood	LOCATION
où	O
la	O
série	O
est	O
tournée	O
.	O
Elle	O
fait	O
ses	O
débuts	O
au	O
cinéma	O
dans	O
Tendres	MISC
années	MISC
(	O
Old	MISC
Enough	MISC
)	O
avec	O
Danny	PERSON
Aiello	PERSON
,	O
puis	O
elle	O
incarne	O
la	O
fille	O
d'	O
Arnold	PERSON
Schwarzenegger	PERSON
dans	O
Commando	MISC
en	O
1985	O
.	O
Au	O
début	O
des	O
années	O
1990	O
,	O
elle	O
enregistre	O
cinq	O
albums	O
:	O
Look	MISC
in	MISC
My	MISC
Heart	MISC
,	O
Alyssa	MISC
,	O
The	MISC
Best	MISC
in	MISC
the	MISC
World	MISC
,	O
Locked	MISC
Inside	MISC
a	O
Dream	ORGANIZATION
et	O
Do	MISC
You	MISC
See	MISC
Me	MISC
?	O
et	O
sort	O
un	O
album	O
best	MISC
of	MISC
en	O
1995	O
intitulé	O
The	MISC
Very	MISC
Best	MISC
of	MISC
Alyssa	MISC
Milano	MISC
.	O
Alyssa	PERSON
fait	O
un	O
carton	O
en	O
Asie	LOCATION
et	O
tout	O
particulièrement	O
au	O
Japon	LOCATION
où	O
elle	O
bat	O
les	O
records	O
de	O
ventes	O
de	O
Madonna	PERSON
ou	O
encore	O
des	O
Rolling	ORGANIZATION
Stones	ORGANIZATION
.	O
Ses	O
albums	O
sont	O
tous	O
disques	O
de	O
platine	O
au	O
Japon	LOCATION
.	O
En	O
1992	O
,	O
Madame	PERSON
est	O
servie	O
s'	O
arrête	O
après	O
huit	O
saisons	O
.	O
Alyssa	PERSON
apparaît	O
dans	O
des	O
téléfilms	O
comme	O
L'	MISC
Affaire	MISC
Amy	MISC
Fisher	MISC
:	O
désignée	O
coupable	O
en	O
1993	O
et	O
Confessions	MISC
d'	MISC
une	MISC
rebelle	MISC
en	O
1994	O
.	O
Elle	O
se	O
fiance	O
en	O
1993	O
à	O
Scott	PERSON
Wolf	PERSON
,	O
le	O
héros	O
de	O
la	O
série	O
La	MISC
vie	MISC
à	MISC
cinq	MISC
,	O
avec	O
qui	O
elle	O
partage	O
l'	O
affiche	O
du	O
film	O
Double	O
Dragon	O
,	O
mais	O
leur	O
relation	O
ne	O
dure	O
pas	O
.	O
En	O
1997	O
,	O
elle	O
fait	O
son	O
retour	O
à	O
la	O
télévision	O
en	O
intégrant	O
le	O
casting	O
de	O
Melrose	LOCATION
Place	LOCATION
,	O
série	O
produite	O
par	O
Aaron	PERSON
Spelling	PERSON
et	O
diffusée	O
sur	O
le	O
réseau	O
américain	O
Fox	ORGANIZATION
aux	O
côtés	O
de	O
David	PERSON
Charvet	PERSON
.	O
En	O
1998	O
,	O
Aaron	PERSON
Spelling	PERSON
lui	O
propose	O
le	O
rôle	O
de	O
Phoebe	PERSON
Halliwell	PERSON
,	O
l'	O
un	O
des	O
trois	O
personnages	O
principaux	O
de	O
sa	O
nouvelle	O
série	O
appelée	O
Charmed	MISC
.	O
Elle	O
y	O
joue	O
aux	O
côtés	O
de	O
Shannen	PERSON
Doherty	PERSON
et	O
d'	O
Holly	PERSON
Marie	PERSON
Combs	PERSON
.	O
Ses	O
deux	O
partenaires	O
de	O
la	O
série	O
Charmed	MISC
,	O
Shannen	PERSON
Doherty	PERSON
et	O
Holly	PERSON
Marie	PERSON
Combs	PERSON
,	O
sont	O
ses	O
demoiselles	O
d'	O
honneur	O
.	O
En	O
2001	O
,	O
la	O
presse	O
s'	O
empare	O
de	O
son	O
conflit	O
avec	O
Shannen	PERSON
Doherty	PERSON
,	O
sa	O
partenaire	O
de	O
Charmed	MISC
.	O
The	MISC
WB	MISC
,	O
le	O
réseau	O
qui	O
diffuse	O
Charmed	MISC
aux	O
États-Unis	LOCATION
,	O
s'	O
inquiète	O
tout	O
comme	O
les	O
fans	O
de	O
la	O
série	O
.	O
Les	O
producteurs	O
se	O
séparent	O
de	O
Shannen	PERSON
Doherty	PERSON
qui	O
est	O
remplacée	O
par	O
l'	O
actrice	O
Rose	PERSON
McGowan	PERSON
.	O
En	O
2003	O
,	O
l'	O
actrice	O
apporte	O
son	O
soutien	O
à	O
l'	O
association	O
PETA	ORGANIZATION
et	O
devient	O
végétarienne	O
.	O
La	O
même	O
année	O
,	O
elle	O
est	O
nommée	O
ambassadrice	O
de	O
l'	O
UNICEF	ORGANIZATION
et	O
se	O
rend	O
en	O
Angola	LOCATION
en	O
2004	O
puis	O
en	O
Inde	LOCATION
en	O
2005	O
où	O
elle	O
porte	O
secours	O
aux	O
enfants	O
défavorisés	O
.	O
Sur	O
le	O
plan	O
professionnel	O
,	O
elle	O
devient	O
productrice	O
de	O
Charmed	MISC
tout	O
comme	O
Holly	PERSON
Marie	PERSON
Combs	PERSON
.	O
En	O
2006	O
,	O
Charmed	MISC
s'	O
arrête	O
après	O
huit	O
saisons	O
.	O
À	O
la	O
rentrée	O
2007	O
,	O
elle	O
est	O
contactée	O
par	O
la	O
chaîne	O
américaine	O
ABC	ORGANIZATION
qui	O
lui	O
propose	O
un	O
rôle	O
dans	O
une	O
comédie	O
qu'	O
elle	O
souhaite	O
lancer	O
.	O
Elle	O
revient	O
finalement	O
à	O
la	O
télévision	O
dans	O
la	O
troisième	O
saison	O
inédite	O
de	O
Earl	PERSON
(	O
My	MISC
Name	MISC
Is	MISC
Earl	MISC
)	O
.	O
Au	O
total	O
elle	O
joue	O
dans	O
dix	O
épisodes	O
qui	O
sont	O
diffusés	O
entre	O
2007	O
et	O
2008	O
sur	O
le	O
réseau	O
américain	O
NBC	ORGANIZATION
.	O
Très	O
grande	O
fan	O
de	O
baseball	O
,	O
l'	O
actrice	O
possède	O
un	O
blog	O
où	O
elle	O
fait	O
part	O
de	O
sa	O
passion	O
pour	O
ce	O
sport	O
et	O
pour	O
les	O
Dodgers	ORGANIZATION
de	O
Los	LOCATION
Angeles	LOCATION
,	O
l'	O
équipe	O
qu'	O
elle	O
soutient	O
.	O
Elle	O
a	O
été	O
l'	O
héroïne	O
d'	O
une	O
toute	O
nouvelle	O
série	O
d'	O
ABC	ORGANIZATION
intitulée	O
Romantically	PERSON
Challenged	PERSON
mais	O
celle-ci	O
a	O
été	O
annulée	O
après	O
la	O
diffusion	O
de	O
seulement	O
quatre	O
épisodes	O
.	O
