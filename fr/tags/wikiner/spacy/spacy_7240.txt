On	O
connaît	O
très	O
peu	O
de	O
choses	O
sur	O
la	O
vie	O
de	O
Tacite	PERSON
.	O
L'	O
historien	O
romain	O
est	O
né	O
entre	O
55	O
et	O
57	O
en	O
Gaule	LOCATION
narbonnaise	LOCATION
(	O
à	O
Vaison-la-Romaine	LOCATION
?	O
Fort	O
de	O
son	O
éducation	O
sévère	O
et	O
disciplinée	O
,	O
il	O
fréquente	O
le	O
grammaticus	O
,	O
le	O
rhetor	O
et	O
devient	O
même	O
,	O
sans	O
doute	O
,	O
l'	O
élève	O
de	O
Quintilien	PERSON
.	O
En	O
77	O
,	O
il	O
épouse	O
la	O
fille	O
du	O
consul	O
Julius	PERSON
Agricola	PERSON
.	O
En	O
81	O
,	O
sous	O
Titus	PERSON
,	O
il	O
devient	O
questeur	O
.	O
En	O
88	O
,	O
sous	O
Domitien	PERSON
,	O
il	O
devient	O
préteur	O
puis	O
tribun	O
de	O
la	O
plèbe	O
.	O
De	O
89	O
à	O
93	O
,	O
il	O
devient	O
légat	O
de	O
province	O
en	O
Gaule	LOCATION
belgique	LOCATION
.	O
Il	O
n'	O
accepte	O
le	O
consulat	O
qu'	O
en	O
97	O
,	O
devenant	O
consul	O
suffect	O
,	O
sous	O
l'	O
empereur	O
Nerva	PERSON
.	O
En	O
98	O
,	O
lorsque	O
Trajan	PERSON
accède	O
au	O
pouvoir	O
,	O
Tacite	PERSON
devient	O
l'	O
un	O
des	O
familiers	O
de	O
l'	O
empereur	O
et	O
se	O
retire	O
de	O
la	O
politique	O
sous	O
Trajan	PERSON
pour	O
se	O
consacrer	O
à	O
l'	O
histoire	O
et	O
son	O
écriture	O
.	O
Cette	O
biographie	O
parait	O
en	O
98	O
,	O
cinq	O
ans	O
après	O
la	O
mort	O
de	O
Agricola	PERSON
,	O
le	O
beau-père	O
de	O
Tacite	PERSON
.	O
Ainsi	O
l'	O
œuvre	O
se	O
présente	O
à	O
la	O
fois	O
comme	O
un	O
éloge	O
funèbre	O
et	O
un	O
essai	O
historique	O
sur	O
la	LOCATION
Bretagne	LOCATION
,	O
sur	O
ses	O
habitants	O
et	O
sa	O
conquête	O
.	O
C'	O
est	O
aussi	O
un	O
manifeste	O
contre	O
la	O
tyrannie	O
de	O
Domitien	PERSON
,	O
assassiné	O
en	O
96	O
.	O
Ce	O
qui	O
est	O
frappant	O
dans	O
cette	O
œuvre	O
,	O
c'	O
est	O
l'	O
approche	O
originale	O
que	O
Tacite	PERSON
fait	O
du	O
phénomène	O
de	O
la	O
conquête	O
impérialiste	O
.	O
Tacite	PERSON
fait	O
preuve	O
de	O
beaucoup	O
de	O
lucidité	O
en	O
soulignant	O
qu'	O
Agricola	PERSON
pratique	O
une	O
politique	O
d'	O
assimilation	O
culturelle	O
.	O
En	O
98	O
paraît	O
également	O
La	LOCATION
Germanie	LOCATION
,	O
petit	O
ouvrage	O
d'	O
actualité	O
--	O
Trajan	PERSON
fortifiait	O
la	O
frontière	O
du	O
Rhin	LOCATION
--	O
,	O
mais	O
dont	O
le	O
caractère	O
est	O
plus	O
nettement	O
historique	O
et	O
ethnographique	O
.	O
C'	O
est	O
une	O
description	O
des	O
différentes	O
tribus	O
vivant	O
au	O
nord	O
du	O
Rhin	LOCATION
et	O
du	O
Danube	LOCATION
.	O
Tacite	PERSON
s'	O
inspira	O
nettement	O
d'	O
auteurs	O
antérieurs	O
comme	O
Tite-Live	PERSON
ou	O
Pline	PERSON
l'	PERSON
Ancien	PERSON
.	O
L'	O
ouvrage	O
fut	O
sans	O
doute	O
composé	O
en	O
80	O
ou	O
81	O
,	O
au	O
moment	O
où	O
Tacite	PERSON
était	O
encore	O
entièrement	O
tourné	O
vers	O
l'	O
éloquence	O
et	O
publié	O
vraisemblablement	O
en	O
107	O
.	O
Le	O
contenu	O
de	O
l'	O
œuvre	O
originelle	O
couvre	O
les	O
règnes	O
suivants	O
:	O
Galba	PERSON
,	O
Othon	PERSON
,	O
Vitellius	PERSON
,	O
Vespasien	PERSON
,	O
Titus	PERSON
et	O
Domitien	PERSON
.	O
L'	O
œuvre	O
qui	O
nous	O
est	O
parvenue	O
s'	O
achève	O
au	O
règne	O
de	O
Titus	PERSON
:	O
Tacite	PERSON
avait	O
une	O
vision	O
pessimiste	O
de	O
l'	O
histoire	O
et	O
tenait	O
des	O
propos	O
déconcertants	O
.	O
Et	O
c'	O
est	O
par	O
une	O
formule	O
sans	O
appel	O
que	O
Tacite	PERSON
tire	O
la	O
leçon	O
de	O
ces	O
temps	O
abominables	O
:	O
"	O
Les	O
dieux	O
,	O
indifférents	O
à	O
notre	O
sauvegarde	O
,	O
n'	O
ont	O
souci	O
que	O
de	O
notre	O
châtiment.	O
"	O
Une	O
des	O
passions	O
de	O
Tacite	PERSON
est	O
la	O
fascination	O
des	O
spectacles	O
de	O
mort	O
.	O
Et	O
la	O
description	O
que	O
donne	O
Tacite	PERSON
de	O
cette	O
scène	O
extraordinaire	O
est	O
elle-même	O
imprégnée	O
de	O
cette	O
fascination	O
de	O
l'	O
horreur	O
qui	O
caractérise	O
sa	O
peinture	O
des	O
évènements	O
.	O
Le	O
pittoresque	O
de	O
Tacite	PERSON
,	O
c'	O
est	O
l'	O
art	O
de	O
faire	O
percevoir	O
la	O
monstruosité	O
des	O
êtres	O
,	O
des	O
situations	O
,	O
des	O
spectacles	O
.	O
Celle-ci	O
devait	O
comporter	O
18	O
livres	O
dont	O
le	O
contenu	O
s'	O
étend	O
du	O
début	O
du	O
règne	O
de	O
Tibère	PERSON
(	O
14	O
ap	O
.	O
à	O
la	O
fin	O
du	O
règne	O
de	O
Néron	PERSON
(	O
68	O
ap	O
.	O
Tacite	PERSON
puisa	O
ses	O
sources	O
dans	O
les	O
ouvrages	O
d'	O
autres	O
historiens	O
,	O
dans	O
les	O
registres	O
publics	O
et	O
parfois	O
dans	O
sa	O
propre	O
expérience	O
.	O
Tacite	PERSON
savait	O
que	O
l'	O
institution	O
impériale	O
était	O
destinée	O
à	O
durer	O
,	O
mais	O
il	O
partageait	O
l'	O
antique	O
conception	O
romaine	O
selon	O
laquelle	O
ce	O
sont	O
des	O
individus	O
qui	O
font	O
l'	O
histoire	O
.	O
De	O
cette	O
œuvre	O
,	O
Racine	PERSON
tirera	O
le	O
sujet	O
d'	O
une	O
de	O
ses	O
tragédies	O
:	O
Britannicus	PERSON
.	O
Tacite	PERSON
tourne	O
essentiellement	O
ses	O
regards	O
vers	O
la	O
politique	O
intérieure	O
et	O
l'	O
équilibre	O
traditionnel	O
entre	O
"	O
ce	O
qui	O
se	O
passe	O
à	O
Rome	LOCATION
"	O
et	O
"	O
ce	O
qui	O
se	O
passe	O
à	O
l'	O
extérieur	O
"	O
n'	O
est	O
pas	O
respecté	O
.	O
Sa	O
valeur	O
d'	O
historien	O
est	O
très	O
contestée	O
:	O
Tacite	PERSON
n'	O
aurait	O
pas	O
été	O
objectif	O
dans	O
ce	O
qu'	O
il	O
écrivait	O
et	O
on	O
conteste	O
la	O
rigueur	O
de	O
son	O
information	O
.	O
Il	O
savait	O
cependant	O
nuancer	O
son	O
portrait	O
laudatif	O
par	O
l'	O
appréciation	O
des	O
erreurs	O
de	O
ses	O
héros	O
(	O
sa	O
haine	O
pour	O
Tibère	PERSON
et	O
Agrippine	PERSON
ne	O
l'	O
empêche	O
pas	O
de	O
leur	O
donner	O
une	O
dimension	O
exceptionnelle	O
dans	O
son	O
œuvre	O
)	O
.	O
Tacite	PERSON
,	O
lorsqu'	O
il	O
écrivait	O
ses	O
œuvres	O
,	O
combinait	O
plusieurs	O
sources	O
,	O
les	O
interprétait	O
et	O
les	O
repensait	O
d'	O
une	O
manière	O
originale	O
.	O
Il	O
combinait	O
la	O
pensée	O
des	O
trois	O
grands	O
historiens	O
qui	O
l'	O
ont	O
précédé	O
:	O
Tite-Live	PERSON
,	O
Salluste	PERSON
et	O
Cicéron	PERSON
.	O
Il	O
était	O
certainement	O
un	O
ami	O
de	O
l'	O
empire	O
,	O
et	O
sans	O
aucun	O
doute	O
un	O
ami	O
de	O
Rome	LOCATION
.	O
Son	O
but	O
premier	O
n'	O
était	O
en	O
fait	O
pas	O
de	O
servir	O
les	O
empereurs	O
,	O
mais	O
Rome	LOCATION
.	O
Il	O
aurait	O
été	O
très	O
proche	O
d'	O
Hadrien	PERSON
et	O
de	O
Trajan	PERSON
.	O
Tacite	PERSON
accorde	O
dans	O
son	O
œuvre	O
une	O
grande	O
place	O
à	O
la	O
philosophie	O
,	O
qu'	O
il	O
connaît	O
sans	O
doute	O
grâce	O
aux	O
maîtres	O
grecs	O
qui	O
se	O
trouvaient	O
à	O
Rome	LOCATION
à	O
son	O
époque	O
.	O
Les	O
grands	O
thèmes	O
de	O
l'	O
œuvre	O
de	O
Tacite	PERSON
sont	O
la	O
glorification	O
des	O
grands	O
administrateurs	O
,	O
la	O
défense	O
libérale	O
de	O
la	O
domination	O
romaine	O
,	O
la	O
critique	O
de	O
la	O
tyrannie	O
,	O
et	O
l'	O
éloge	O
de	O
la	O
sagesse	O
philosophique	O
tempéré	O
par	O
la	O
confiance	O
à	O
l'	O
égard	O
du	O
fanatisme	O
et	O
du	O
dogmatisme	O
.	O
Tacite	PERSON
savait	O
faire	O
des	O
portraits	O
grandioses	O
et	O
sobres	O
de	O
ses	O
personnages	O
.	O
Sa	O
production	O
littéraire	O
,	O
s'	O
inscrivant	O
dans	O
le	O
cadre	O
de	O
son	O
amitié	O
pour	O
Trajan	PERSON
et	O
Pline	PERSON
le	PERSON
Jeune	PERSON
,	O
était	O
appréciée	O
par	O
le	O
milieu	O
impérial	O
.	O
Tacite	PERSON
fut	O
l'	O
historien	O
officieux	O
du	O
régime	O
,	O
ce	O
qui	O
ne	O
l'	O
empêcha	O
pas	O
d'	O
être	O
aussi	O
un	O
historien	O
critique	O
.	O
