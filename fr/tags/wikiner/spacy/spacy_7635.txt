La	O
vodka	O
est	O
une	O
boisson	O
alcoolisée	O
incolore	O
dont	O
l'	O
origine	O
se	O
situe	O
,	O
selon	O
les	O
sources	O
,	O
en	O
Russie	LOCATION
ou	O
en	O
Pologne	LOCATION
.	O
Il	O
s'	O
appuie	O
sur	O
les	O
travaux	O
du	O
chimiste	O
Dimitri	PERSON
Mendeleïev	PERSON
qui	O
avait	O
précédemment	O
démontré	O
que	O
la	O
meilleure	O
vodka	O
titrait	O
à	O
38°	O
,	O
mais	O
les	O
taxes	O
de	O
l'	O
époque	O
étant	O
calculées	O
sur	O
le	O
taux	O
d'	O
alcool	O
,	O
c'	O
est	O
le	O
titre	O
de	O
40°	O
qui	O
fut	O
conservé	O
pour	O
faciliter	O
la	O
tâche	O
des	O
services	O
fiscaux	O
.	O
En	O
raison	O
de	O
la	O
grande	O
consommation	O
de	O
cette	O
boisson	O
en	O
Russie	LOCATION
(	O
elle	O
représente	O
91	O
%	O
de	O
l'	O
alcool	O
consommé	O
,	O
pour	O
en	O
1998	O
14.5	O
litres	O
d'	O
alcool	O
pur	O
consommé	O
par	O
russe	O
)	O
,	O
ce	O
secteur	O
de	O
production	O
est	O
largement	O
infiltré	O
par	O
la	O
mafia	O
(	O
auquel	O
il	O
rapporte	O
1,5	O
milliard	O
de	O
dollars	O
en	O
1997	O
)	O
et	O
autres	O
producteurs	O
clandestins	O
(	O
ces	O
derniers	O
représentent	O
50	O
%	O
de	O
la	O
production	O
)	O
.	O
Elle	O
titre	O
entre	O
37,5	O
(	O
le	O
minimum	O
légal	O
dans	O
l'	O
U.E.	ORGANIZATION
)	O
et	O
97	O
degrés	O
d'	O
alcool	O
,	O
mais	O
plus	O
classiquement	O
40	O
.	O
Aujourd'hui	O
,	O
on	O
produit	O
de	O
la	O
vodka	O
dans	O
de	O
nombreux	O
pays	O
,	O
notamment	O
en	O
Russie	LOCATION
,	O
en	O
Ukraine	LOCATION
,	O
en	O
Pologne	LOCATION
,	O
en	O
Bulgarie	LOCATION
,	O
en	O
Angleterre	LOCATION
,	O
en	O
Suède	LOCATION
,	O
en	O
Finlande	LOCATION
,	O
aux	O
États-Unis	LOCATION
et	O
au	O
Canada	LOCATION
.	O
On	O
en	O
produit	O
même	O
en	O
France	LOCATION
,	O
à	O
Tahiti	LOCATION
et	O
en	O
Suisse	LOCATION
.	O
Il	O
y	O
a	O
plusieurs	O
autres	O
arômes	O
traditionnels	O
:	O
piment	O
(	O
souvent	O
avec	O
du	O
miel	O
,	O
surtout	O
en	O
Ukraine	LOCATION
)	O
,	O
bouleau	O
,	O
airelles	O
,	O
baies	O
de	O
sorbier	O
,	O
noix	O
de	O
cèdre	O
,	O
orties	O
,	O
poivre	O
.	O
En	O
Russie	LOCATION
,	O
depuis	O
quelques	O
années	O
,	O
de	O
nombreux	O
kiosques	O
vendent	O
des	O
produits	O
bon	O
marché	O
(	O
1,5	O
euro	O
)	O
sur	O
lesquels	O
sont	O
apposés	O
des	O
étiquettes	O
"	O
vodka	O
"	O
.	O
Il	O
s'	O
agit	O
en	O
fait	O
de	O
produits	O
souvent	O
toxiques	O
,	O
qui	O
ont	O
causé	O
durant	O
les	O
10	O
premiers	O
mois	O
de	O
l'	O
année	O
2006	O
environ	O
17000	O
décès	O
selon	O
le	O
président	O
de	O
la	O
Douma	ORGANIZATION
Boris	PERSON
Gryzlov	PERSON
.	O
La	O
production	O
de	O
vodka	O
frelatée	O
existe	O
en	O
Russie	LOCATION
depuis	O
longtemps	O
,	O
mais	O
elle	O
n'	O
avait	O
jamais	O
causé	O
autant	O
de	O
morts	O
auparavant	O
.	O
