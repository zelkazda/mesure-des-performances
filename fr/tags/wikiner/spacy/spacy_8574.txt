Jean	PERSON
Froissart	PERSON
ou	O
Jehan	PERSON
Froissart	PERSON
(	O
vers	O
1337	O
,	O
Valenciennes	LOCATION
--	O
après	O
1404	O
)	O
est	O
l'	O
un	O
des	O
plus	O
importants	O
chroniqueurs	O
de	O
l'	O
époque	O
médiévale	O
.	O
Il	O
s'	O
agit	O
également	O
d'	O
une	O
des	O
sources	O
les	O
plus	O
importantes	O
sur	O
la	O
première	O
moitié	O
de	O
la	O
guerre	MISC
de	MISC
Cent	MISC
Ans	MISC
.	O
On	O
sait	O
très	O
peu	O
de	O
choses	O
de	O
la	O
vie	O
de	O
Froissart	PERSON
et	O
le	O
peu	O
qui	O
est	O
connu	O
vient	O
principalement	O
de	O
ses	O
propres	O
chroniques	O
et	O
de	O
ses	O
poésies	O
.	O
Froissart	PERSON
est	O
né	O
à	O
Valenciennes	LOCATION
,	O
dans	O
ce	O
qui	O
était	O
alors	O
le	O
comté	LOCATION
de	LOCATION
Hainaut	LOCATION
.	O
Froissart	PERSON
commence	O
à	O
travailler	O
en	O
tant	O
que	O
négociant	O
mais	O
abandonne	O
bientôt	O
pour	O
se	O
destiner	O
à	O
la	O
prêtrise	O
.	O
Vers	O
l'	O
âge	O
de	O
24	O
ans	O
,	O
il	O
devient	O
poète	O
et	O
ses	O
activités	O
le	O
désignent	O
comme	O
historien	O
officiel	O
à	O
la	O
cour	O
de	O
Philippa	PERSON
de	PERSON
Hainaut	PERSON
,	O
l'	O
épouse	O
d'	O
Édouard	PERSON
III	PERSON
d'	PERSON
Angleterre	PERSON
.	O
À	O
ce	O
mariage	O
étaient	O
aussi	O
présents	O
deux	O
autres	O
auteurs	O
qui	O
marquèrent	O
cette	O
époque	O
,	O
Chaucer	PERSON
et	O
Pétrarque	PERSON
.	O
Il	O
reçoit	O
en	O
récompense	O
le	O
bénéfice	O
ecclésiastique	O
d'	O
Estinnes	LOCATION
,	O
un	O
village	O
près	O
de	O
Binche	LOCATION
et	O
devient	O
ensuite	O
chanoine	O
de	O
Chimay	LOCATION
,	O
ce	O
qui	O
le	O
libère	O
des	O
soucis	O
financiers	O
.	O
