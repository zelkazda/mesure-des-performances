L'	O
INSEE	ORGANIZATION
et	O
la	ORGANIZATION
Poste	ORGANIZATION
lui	O
attribuent	O
le	O
code	O
72	O
.	O
Le	O
département	LOCATION
de	LOCATION
la	LOCATION
Sarthe	LOCATION
est	O
à	O
dominante	O
rurale	O
;	O
il	O
crée	O
une	O
véritable	O
antinomie	O
avec	O
son	O
chef-lieu	O
:	O
la	O
ville	LOCATION
du	LOCATION
Mans	LOCATION
.	O
Ce	O
département	O
est	O
vaste	O
et	O
relativement	O
peu	O
peuplé	O
puisque	O
l'	O
aire	LOCATION
urbaine	LOCATION
du	LOCATION
Mans	LOCATION
comprend	O
presque	O
la	O
moitié	O
de	O
la	O
population	O
totale	O
du	O
département	O
.	O
Le	O
département	O
possède	O
beaucoup	O
de	O
forêts	O
notamment	O
celles	O
de	O
Perseigne	LOCATION
,	O
de	O
Bercé	LOCATION
ou	O
le	O
Bois	LOCATION
de	LOCATION
Changé	LOCATION
.	O
On	O
peut	O
considérer	O
le	O
département	O
sous	O
deux	O
faces	O
:	O
l'	O
une	O
urbaine	O
ou	O
extra-urbaine	O
avec	O
l'	O
agglomération	O
de	O
Le	LOCATION
Mans	LOCATION
Métropole	LOCATION
ainsi	O
que	O
son	O
aire	O
urbaine	O
s'	O
étendant	O
sur	O
90	O
communes	O
et	O
regroupant	O
quelques	O
300000	O
habitants	O
.	O
Peu	O
à	O
peu	O
ces	O
prélats	O
acquirent	O
un	O
pouvoir	O
incontesté	O
,	O
devant	O
lequel	O
durent	O
souvent	O
s'	O
incliner	O
les	O
comtes	O
,	O
révocables	O
et	O
viagers	O
,	O
nommés	O
par	O
le	O
roi	O
,	O
qui	O
gouvernèrent	O
depuis	O
l'	O
époque	O
de	O
la	O
conquête	O
jusqu'	O
à	O
l'	O
avènement	O
d'	O
Hugues	PERSON
Capet	PERSON
.	O
Mais	O
à	O
la	O
mort	O
de	O
Louis	PERSON
le	PERSON
Débonnaire	PERSON
,	O
Lothaire	PERSON
ayant	O
envahi	O
le	O
Maine	LOCATION
qui	O
était	O
échu	O
à	O
Charles	PERSON
le	PERSON
Chauve	PERSON
,	O
et	O
s'	O
en	O
étant	O
emparé	O
,	O
sa	O
décadence	O
fut	O
aussi	O
rapide	O
qu'	O
elle	O
devait	O
être	O
longue	O
[	O
évasif	O
]	O
.	O
Ces	O
pirates	O
surprennent	O
le	LOCATION
Mans	LOCATION
,	O
qu'	O
ils	O
pillent	O
et	O
dont	O
ils	O
massacrent	O
les	O
habitants	O
;	O
ils	O
entrent	O
dans	O
Sablé	LOCATION
,	O
d'	O
où	O
le	O
pape	O
Urbain	PERSON
II	PERSON
,	O
en	O
1096	O
,	O
devait	O
venir	O
prêcher	O
la	O
première	MISC
croisade	MISC
.	O
Édouard	PERSON
débarqua	O
en	O
France	LOCATION
à	O
la	O
tête	O
d'	O
une	O
grande	O
armée	O
.	O
Vainqueur	O
sur	O
mer	O
à	O
la	O
bataille	MISC
de	MISC
l'	MISC
Écluse	MISC
(	O
1340	O
)	O
,	O
et	O
sur	O
terre	O
,	O
à	O
la	O
bataille	MISC
de	MISC
Crécy	MISC
(	O
1346	O
)	O
,	O
il	O
porta	O
le	O
théâtre	O
de	O
la	O
guerre	O
dans	O
le	O
Maine	LOCATION
.	O
Mais	O
vint	O
la	O
folie	O
du	O
roi	O
Charles	PERSON
VI	PERSON
,	O
dans	O
la	O
forêt	LOCATION
de	LOCATION
Bercé	LOCATION
,	O
alors	O
qu'	O
il	O
marchait	O
contre	O
le	O
duc	PERSON
de	PERSON
Bretagne	PERSON
.	O
La	LOCATION
Ferté-Bernard	LOCATION
,	O
qui	O
soutint	O
un	O
siège	O
de	O
quatre	O
mois	O
,	O
fut	O
prise	O
par	O
Salisbury	PERSON
qui	O
,	O
après	O
la	O
bataille	MISC
de	MISC
Verneuil	MISC
(	O
1424	O
)	O
,	O
assiégea	O
le	LOCATION
Mans	LOCATION
et	O
s'	O
en	O
empara	O
.	O
Enfin	O
,	O
Dunois	PERSON
rentre	O
dans	O
le	LOCATION
Mans	LOCATION
(	O
1447	O
)	O
et	O
les	O
troupes	O
anglaises	O
quittent	O
le	O
Maine	LOCATION
pour	O
ne	O
plus	O
y	O
revenir	O
.	O
La	O
province	O
ne	O
fit	O
son	O
retour	O
à	O
la	O
couronne	O
en	O
1481	O
,	O
sous	O
Louis	PERSON
XI	PERSON
.	O
Le	O
département	O
a	O
été	O
créé	O
à	O
la	O
Révolution	MISC
française	MISC
,	O
le	O
4	O
mars	O
1790	O
en	O
application	O
de	O
la	O
loi	O
du	O
22	O
décembre	O
1789	O
,	O
à	O
partir	O
d'	O
une	O
partie	O
de	O
la	O
province	LOCATION
du	LOCATION
Maine	LOCATION
,	O
connu	O
sous	O
le	O
nom	O
de	O
haut	O
Maine	LOCATION
ou	O
Maine	LOCATION
blanc	O
.	O
On	O
lui	O
adjoignit	O
au	O
sud	O
une	O
bande	O
de	O
territoire	O
,	O
appelée	O
le	O
Maine	LOCATION
angevin	LOCATION
,	O
pris	O
sur	O
la	O
province	LOCATION
d'	LOCATION
Anjou	LOCATION
autour	O
des	O
villes	O
de	O
La	LOCATION
Flèche	LOCATION
,	O
du	O
Lude	LOCATION
et	O
de	O
Château-du-Loir	LOCATION
.	O
Pour	O
l'	O
Anjou	LOCATION
,	O
ce	O
sont	O
29	O
paroisses	O
qui	O
ont	O
été	O
implantées	O
,	O
afin	O
de	O
former	O
27	O
communes	O
.	O
Sa	O
préfecture	O
,	O
Le	LOCATION
Mans	LOCATION
,	O
était	O
le	O
chef-lieu	O
de	O
l'	O
ancienne	O
province	O
du	O
Maine	O
.	O
L'	O
autre	O
partie	O
de	O
cette	O
province	O
du	O
Maine	O
,	O
se	O
partage	O
le	O
département	O
de	O
la	O
Mayenne	LOCATION
avec	O
l'	O
ancienne	O
province	O
d'	O
Anjou	LOCATION
.	O
Les	O
anciennes	O
parties	O
de	O
la	O
province	LOCATION
du	LOCATION
Maine	LOCATION
ont	O
été	O
séparées	O
.	O
Elle	O
est	O
limitrophe	O
des	O
départements	O
de	O
l'	O
Orne	LOCATION
,	O
d'	O
Eure-et-Loir	LOCATION
,	O
de	O
Loir-et-Cher	LOCATION
,	O
d'	O
Indre-et-Loire	LOCATION
,	O
de	O
Maine-et-Loire	LOCATION
et	O
de	O
la	O
Mayenne	LOCATION
.	O
Sur	O
une	O
trentaine	O
de	O
kilomètres	O
,	O
une	O
partie	O
nord	O
du	O
département	O
est	O
appelée	O
Maine	LOCATION
normand	LOCATION
.	O
Au	O
sud-est	O
,	O
le	O
climat	O
est	O
approchant	O
de	O
celui	O
du	O
centre	O
et	O
tout	O
particulièrement	O
de	O
la	O
Touraine	LOCATION
.	O
Tout	O
l'	O
ouest	O
du	O
département	O
va	O
de	O
pair	O
avec	O
les	O
espaces	O
de	O
la	O
Mayenne	LOCATION
et	O
de	O
l'	O
ancienne	O
province	O
de	O
l'	O
Anjou	LOCATION
,	O
avec	O
des	O
terres	O
agricoles	O
et	O
des	O
exploitations	O
fermières	O
de	O
taille	O
moyenne	O
.	O
Le	O
patois	O
sarthois	O
est	O
un	O
dialecte	O
(	O
patois	O
)	O
du	O
département	LOCATION
de	LOCATION
la	LOCATION
Sarthe	LOCATION
.	O
Cette	O
ville	O
est	O
une	O
sous-préfecture	O
,	O
elle	O
est	O
située	O
à	O
la	O
limite	O
avec	O
le	O
département	LOCATION
de	LOCATION
l'	LOCATION
Orne	LOCATION
.	O
Au	O
nord-est	O
se	O
trouve	O
la	O
ville	O
de	O
La	LOCATION
Ferté-Bernard	LOCATION
,	O
environ	O
9000	O
habitants	O
.	O
Au	O
sud-est	O
se	O
trouve	O
la	O
ville	O
de	O
Château-du-Loir	LOCATION
,	O
environ	O
5000	O
habitants	O
.	O
Elle	O
est	O
à	O
la	O
limite	O
avec	O
le	O
département	LOCATION
d'	LOCATION
Indre-et-Loire	LOCATION
.	O
Au	O
sud	O
se	O
trouve	O
la	O
ville	O
de	O
La	LOCATION
Flèche	LOCATION
,	O
environ	O
17	O
000	O
habitants	O
.	O
Au	O
sud-ouest	O
se	O
trouve	O
Sablé-sur-Sarthe	LOCATION
,	O
environ	O
13	O
500	O
habitants	O
.	O
Elle	O
est	O
à	O
la	O
limite	O
avec	O
les	O
départements	O
de	O
Maine-et-Loire	LOCATION
et	O
de	O
la	O
Mayenne	LOCATION
.	O
Au	O
sud-est	O
se	O
trouve	O
Saint-Calais	LOCATION
,	O
environ	O
4000	O
habitants	O
.	O
Elle	O
est	O
à	O
la	O
limite	O
du	O
département	LOCATION
de	LOCATION
Loir-et-Cher	O
.	O
Le	O
climat	O
est	O
celui	O
d'	O
un	O
territoire	O
du	O
centre-ouest	O
de	O
la	LOCATION
France	LOCATION
.	O
Elle	O
est	O
également	O
plus	O
faible	O
que	O
la	O
moyenne	O
régionale	O
de	O
106	O
habitants/km²	O
des	O
Pays	LOCATION
de	LOCATION
la	LOCATION
Loire	LOCATION
et	O
s'	O
accroît	O
moins	O
vite	O
(	O
+0,93	O
%	O
depuis	O
2000	O
)	O
.	O
Le	MISC
Château	MISC
du	MISC
Lude	MISC
.	O
Le	O
festival	MISC
d'	MISC
art	MISC
de	MISC
rue	MISC
,	O
"	O
Les	MISC
Affranchis	MISC
"	O
,	O
de	O
La	LOCATION
Flèche	LOCATION
,	O
3	O
e	O
festival	O
du	O
genre	O
en	O
France	LOCATION
,	O
chaque	O
année	O
début	O
juillet	O
.	O
Le	O
département	LOCATION
de	LOCATION
la	LOCATION
Sarthe	LOCATION
,	O
de	O
par	O
sa	O
position	O
géographique	O
,	O
est	O
très	O
bien	O
pourvu	O
par	O
les	O
infrastructures	O
routières	O
et	O
autoroutières	O
.	O
Il	O
s'	O
agit	O
bien	O
souvent	O
d'	O
événements	O
sportifs	O
automobiles	O
:	O
les	O
24	MISC
heures	MISC
du	MISC
Mans	MISC
auto	O
,	O
moto	O
,	O
camion	O
,	O
le	O
Grand	MISC
Prix	MISC
de	MISC
France	MISC
de	MISC
Vitesse	MISC
Moto	MISC
.	O
Le	O
Festival	MISC
Artec	MISC
se	O
déroule	O
à	O
la	LOCATION
Ferté-Bernard	LOCATION
,	O
fin	O
mai	O
.	O
Il	O
réunit	O
des	O
challengers	O
de	O
toute	O
la	LOCATION
France	LOCATION
pour	O
organiser	O
des	O
tournois	O
de	O
créations	O
robotiques	O
.	O
Le	MISC
Château	MISC
du	MISC
Lude	MISC
est	O
un	O
lieu	O
intéressant	O
pour	O
son	O
architecture	O
.	O
Les	O
faïenceries	O
de	O
Malicorne-sur-Sarthe	LOCATION
sont	O
parmi	O
les	O
plus	O
belles	O
de	O
la	O
région	O
.	O
Le	O
zoo	O
de	O
La	LOCATION
Flèche	LOCATION
est	O
reconnu	O
pour	O
posséder	O
quelques	O
races	O
d'	O
animaux	O
rares	O
.	O
Le	O
zoo	O
de	O
La	LOCATION
Flèche	LOCATION
est	O
l'	O
un	O
des	O
5	O
plus	O
grands	O
parcs	O
zoologiques	O
français	O
.	O
Ce	O
tableau	O
indique	O
les	O
principales	O
communes	O
du	O
département	LOCATION
de	LOCATION
la	LOCATION
Sarthe	LOCATION
dont	O
les	O
résidences	O
secondaires	O
et	O
occasionnelles	O
dépassent	O
10	O
%	O
des	O
logements	O
totaux	O
.	O
