La	O
première	O
mesure	O
de	O
la	O
parallaxe	O
d'	O
une	O
étoile	O
a	O
été	O
publiée	O
en	O
1838	O
par	O
l'	O
allemand	O
Friedrich	PERSON
Wilhelm	PERSON
Bessel	PERSON
.	O
Par	O
exemple	O
,	O
la	O
parallaxe	O
horizontale	O
équatoriale	O
du	O
Soleil	LOCATION
vaut	O
8,794″	O
.	O
Le	O
rapport	O
de	O
la	O
parallaxe	O
horizontale	O
équatoriale	O
moyenne	O
du	O
Soleil	LOCATION
et	O
de	O
la	O
parallaxe	O
horizontale	O
d'	O
un	O
astre	O
fournit	O
une	O
valeur	O
approchée	O
de	O
la	O
distance	O
d'	O
un	O
astre	O
du	O
système	O
solaire	O
,	O
en	O
unités	O
astronomiques	O
.	O
Friedrich	PERSON
Wilhelm	PERSON
Bessel	PERSON
utilisa	O
cette	O
méthode	O
pour	O
la	O
première	O
fois	O
en	O
1838	O
pour	O
la	O
binaire	O
61	O
du	O
Cygne	LOCATION
.	O
Grâce	O
au	O
satellite	O
d'	O
astrométrie	O
européen	O
Hipparcos	MISC
,	O
les	O
parallaxes	O
annuelles	O
d'	O
environ	O
100000	O
étoiles	O
sont	O
maintenant	O
connues	O
avec	O
une	O
précision	O
de	O
0,001″	O
.	O
Galilée	PERSON
répondit	O
que	O
les	O
étoiles	O
étaient	O
trop	O
lointaines	O
pour	O
que	O
la	O
parallaxe	O
puisse	O
être	O
vue	O
et	O
mesurée	O
avec	O
les	O
instruments	O
d'	O
alors	O
.	O
Il	O
avait	O
fait	O
sur	O
l'	O
éloignement	O
des	O
plus	O
proches	O
étoiles	O
une	O
hypothèse	O
très	O
en	O
dessous	O
de	O
la	O
réalité	O
,	O
qui	O
en	O
fait	O
confirme	O
l'	O
argument	O
de	O
Galilée	PERSON
.	O
