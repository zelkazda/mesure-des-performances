Sa	O
capitale	O
est	O
Bissau	LOCATION
.	O
Le	O
pays	O
fait	O
partie	O
de	O
la	O
CEDEAO	ORGANIZATION
.	O
Les	O
premiers	O
contacts	O
européens	O
sont	O
établis	O
,	O
en	O
1446	O
,	O
par	O
le	O
navigateur	O
portugais	O
Nuno	PERSON
Tristão	PERSON
,	O
tué	O
en	O
prenant	O
pied	O
dans	O
la	O
zone	O
.	O
Le	O
Portugal	LOCATION
établit	O
quelques	O
comptoirs	O
sur	O
la	O
côte	O
.	O
Les	O
Portugais	LOCATION
quittent	O
le	O
pays	O
après	O
la	O
Révolution	MISC
des	MISC
œillets	MISC
en	O
1974	O
.	O
Le	O
Parti	ORGANIZATION
africain	ORGANIZATION
pour	O
l'	O
indépendance	O
de	O
la	O
Guinée	LOCATION
et	O
du	O
Cap-Vert	LOCATION
qui	O
avait	O
mené	O
la	O
lutte	O
politique	O
puis	O
militaire	O
pour	O
l'	O
indépendance	O
remporte	O
les	O
élections	O
.	O
Le	O
1	O
er	O
mars	O
2009	O
,	O
le	O
chef	O
d'	O
état-major	O
des	O
forces	O
armées	O
,	O
le	O
général	O
Tagmé	PERSON
Na	PERSON
Waié	PERSON
,	O
est	O
tué	O
dans	O
un	O
attentat	O
à	O
la	O
bombe	O
.	O
Le	O
président	O
Joao	PERSON
Bernardo	PERSON
Vieira	PERSON
,	O
que	O
certains	O
militaires	O
tiennent	O
pour	O
responsable	O
de	O
cet	O
attentat	O
,	O
est	O
assassiné	O
à	O
son	O
tour	O
,	O
le	O
2	O
mars	O
2009	O
,	O
par	O
des	O
hommes	O
en	O
armes	O
.	O
La	O
Guinée-Bissau	O
est	O
divisée	O
en	O
9	O
régions	O
,	O
elles-mêmes	O
partagées	O
en	O
37	O
secteurs	O
.	O
La	O
Guinée-Bissau	O
doit	O
son	O
nom	O
à	O
sa	O
capitale	O
,	O
Bissau	LOCATION
,	O
et	O
s'	O
étend	O
sur	O
36120	O
km²	O
,	O
28000	O
km²	O
de	O
terre	O
et	O
8120	O
km²	O
de	O
mer	O
(	O
ce	O
qui	O
est	O
à	O
peine	O
plus	O
étendu	O
que	O
la	O
Belgique	LOCATION
)	O
,	O
y	O
compris	O
une	O
soixantaine	O
d'	O
îles	O
dans	O
l'	O
Atlantique	LOCATION
,	O
dont	O
l'	O
archipel	LOCATION
des	LOCATION
Bijagos	LOCATION
appelé	O
aussi	O
archipel	O
des	O
Bissagos	LOCATION
.	O
Son	O
littoral	O
,	O
très	O
riche	O
en	O
poissons	O
,	O
attire	O
les	O
pêcheurs	O
de	O
l'	O
Union	ORGANIZATION
européenne	ORGANIZATION
qui	O
viennent	O
pêcher	O
chaque	O
année	O
500	O
mille	O
de	O
tonnes	O
de	O
poisson	O
,	O
versant	O
en	O
échange	O
à	O
la	O
Guinée-Bissau	LOCATION
environ	O
7500000	O
€	O
.	O
Malgré	O
ses	O
nombreux	O
atouts	O
,	O
la	O
Guinée-Bissau	LOCATION
est	O
le	O
troisième	O
pays	O
le	O
plus	O
pauvre	O
du	O
monde	O
,	O
parmi	O
les	O
pays	O
les	O
moins	O
avancés	O
.	O
La	O
drogue	O
sud-américaine	O
est	O
donc	O
stockée	O
en	O
Guinée-Bissau	LOCATION
,	O
où	O
elle	O
est	O
ensuite	O
introduite	O
par	O
petites	O
quantités	O
dans	O
les	O
produits	O
de	O
marché	O
acheminés	O
vers	O
l'	O
Europe	LOCATION
,	O
ou	O
ingérée	O
par	O
des	O
mules	O
qui	O
risquent	O
leur	O
vie	O
et	O
leur	O
liberté	O
pour	O
5000	O
€	O
(	O
leur	O
salaire	O
pour	O
acheminer	O
500	O
grammes	O
à	O
1	O
kilogramme	O
de	O
cocaïne	O
en	O
capsules	O
)	O
.	O
La	O
Guinée-Bissau	O
,	O
loin	O
d'	O
être	O
consommatrice	O
de	O
ces	O
drogues	O
de	O
"	O
luxe	O
"	O
que	O
ses	O
habitants	O
n'	O
ont	O
pas	O
les	O
moyens	O
de	O
s'	O
offrir	O
,	O
est	O
devenue	O
en	O
quelques	O
années	O
la	O
plaque	O
tournante	O
du	O
trafic	O
de	O
cocaïne	O
,	O
moyen	O
pervers	O
d'	O
enrichir	O
quelques	O
particuliers	O
.	O
La	O
Guinée-Bissau	O
a	O
pour	O
codes	O
:	O
