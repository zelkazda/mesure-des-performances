Commune	O
située	O
à	O
moins	O
de	O
8	O
km	O
de	O
Caen	LOCATION
et	O
à	O
35	O
km	O
au	O
sud	O
est	O
de	O
Bayeux	LOCATION
.	O
En	O
1911	O
,	O
la	O
commune	O
a	O
été	O
rebaptisée	O
Saint-André-sur-Orne	LOCATION
.	O
Mais	O
le	O
village	O
fut	O
peu	O
de	O
temps	O
après	O
libéré	O
par	O
les	O
armées	O
britanniques	O
et	O
canadiennes	O
dans	O
le	O
cadre	O
de	O
l'	O
Opération	MISC
Spring	MISC
.	O
Le	O
20	O
novembre	O
1992	O
,	O
l'	O
école	O
primaire	O
de	O
Saint-André-sur-Orne	LOCATION
a	O
été	O
baptisée	O
du	O
nom	O
du	O
scénariste	O
de	O
bandes	O
dessinées	O
René	PERSON
Goscinny	PERSON
,	O
créateur	O
d'	O
Astérix	MISC
,	O
du	O
Petit	MISC
Nicolas	MISC
et	O
d'	O
Iznogoud	MISC
.	O
Lors	O
de	O
l'	O
inauguration	O
,	O
de	O
nombreuses	O
personnalités	O
étaient	O
présentes	O
:	O
Albert	PERSON
Uderzo	PERSON
,	O
Anne	PERSON
Goscinny	PERSON
,	O
Enki	PERSON
Bilal	PERSON
,	O
Frank	PERSON
Margerin	PERSON
,	O
Philippe	PERSON
Druillet	PERSON
,	O
Pierre	PERSON
Tchernia	PERSON
et	O
Guy	PERSON
Vidal	PERSON
.	O
