Alors	O
que	O
la	O
plupart	O
des	O
étoiles	O
sont	O
de	O
luminosité	O
presque	O
constante	O
,	O
comme	O
le	LOCATION
Soleil	LOCATION
qui	O
ne	O
possède	O
pratiquement	O
pas	O
de	O
variation	O
mesurable	O
(	O
environ	O
0,1	O
%	O
sur	O
un	O
cycle	O
de	O
11	O
ans	O
)	O
,	O
la	O
luminosité	O
de	O
certaines	O
étoiles	O
varie	O
de	O
façon	O
perceptible	O
pendant	O
des	O
périodes	O
de	O
temps	O
beaucoup	O
plus	O
courtes	O
.	O
Pour	O
cette	O
raison	O
,	O
le	LOCATION
Soleil	LOCATION
est	O
une	O
étoile	O
très	O
faiblement	O
variable	O
à	O
cause	O
des	O
taches	O
solaires	O
et	O
il	O
est	O
fort	O
probable	O
que	O
la	O
plupart	O
des	O
étoiles	O
possèdent	O
des	O
taches	O
similaires	O
.	O
