Elle	O
a	O
été	O
ainsi	O
nommée	O
pour	O
sa	O
proximité	O
avec	O
le	O
Grand	LOCATION
Lac	LOCATION
Salé	LOCATION
(	O
en	O
anglais	O
,	O
Great	LOCATION
Salt	LOCATION
Lake	LOCATION
)	O
.	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
abrite	O
178097	O
habitants	O
dans	O
la	O
ville	O
.	O
La	O
zone	O
métropolitaine	O
de	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
s'	O
étend	O
sur	O
les	O
comtés	O
de	O
Salt	LOCATION
Lake	LOCATION
,	O
de	O
Summit	LOCATION
et	O
de	O
Tooele	LOCATION
pour	O
une	O
population	O
de	O
1018826	O
habitants	O
.	O
Selon	O
l'	O
estimation	O
de	O
2005	O
,	O
l'	O
agglomération	O
métropolitaine	O
de	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
--	O
Ogden	LOCATION
compte	O
1586740	O
habitants	O
.	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
fait	O
partie	O
de	O
la	O
région	O
urbaine	O
sise	O
aux	O
pieds	O
des	O
montagnes	LOCATION
Wasatch	LOCATION
et	O
appelé	O
Wasatch	LOCATION
front	O
.	O
En	O
2002	O
,	O
la	O
ville	O
a	O
accueilli	O
les	O
XIX	MISC
e	MISC
Jeux	MISC
olympiques	MISC
d'	MISC
hiver	MISC
.	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
est	O
le	O
siège	O
de	O
plusieurs	O
universités	O
,	O
dont	O
l'	O
université	ORGANIZATION
de	ORGANIZATION
l'	ORGANIZATION
Utah	ORGANIZATION
.	O
Une	O
tornade	O
a	O
été	O
observée	O
à	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
le	O
11	O
août	O
1999	O
.	O
En	O
2006	O
,	O
2007	O
et	O
2008	O
,	O
ont	O
eu	O
lieu	O
les	O
tournages	O
respectifs	O
de	O
High	MISC
School	MISC
Musical	O
,	O
High	MISC
School	MISC
Musical	O
2	O
et	O
High	O
School	O
Musical	O
3	O
:	O
Nos	O
années	O
lycée	O
au	O
lycée	O
East	ORGANIZATION
High	ORGANIZATION
School	ORGANIZATION
,	O
bien	O
que	O
l'	O
histoire	O
du	O
film	O
se	O
déroule	O
à	O
Albuquerque	LOCATION
.	O
L'	O
Utah	LOCATION
est	O
alors	O
considéré	O
par	O
les	O
mormons	O
comme	O
leur	O
patrie	O
appelée	O
"	O
Deseret	MISC
"	O
(	O
l	O
'	O
"	O
abeille	O
à	O
miel	O
"	O
,	O
laquelle	O
est	O
vénérée	O
pour	O
sa	O
diligence	O
dans	O
le	O
Livre	MISC
de	MISC
Mormon	MISC
)	O
.	O
Lors	O
de	O
la	O
Seconde	MISC
Guerre	MISC
mondiale	MISC
,	O
la	O
demande	O
en	O
métaux	O
crée	O
un	O
essor	O
de	O
l'	O
industrie	O
minière	O
et	O
plus	O
généralement	O
de	O
l'	O
économie	O
locale	O
,	O
propice	O
à	O
un	O
développement	O
de	O
l'	O
agglomération	O
de	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
.	O
Les	O
élections	O
municipales	O
sont	O
organisées	O
à	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
sur	O
une	O
base	O
non	O
partisane	O
.	O
Rocky	PERSON
Anderson	PERSON
,	O
maire	O
controversé	O
de	O
la	O
ville	O
de	O
2000	O
à	O
2008	O
,	O
fut	O
un	O
opposant	O
farouche	O
au	O
président	O
George	PERSON
Bush	PERSON
.	O
Alors	O
qu'	O
en	O
novembre	O
2004	O
,	O
66	O
%	O
des	O
citoyens	O
de	O
l'	O
état	LOCATION
(	O
82	O
%	O
hors	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
)	O
rejetait	O
par	O
référendum	O
toute	O
possibilité	O
de	O
mariage	O
homosexuel	O
ou	O
d'	O
union	ORGANIZATION
civile	ORGANIZATION
,	O
54	O
%	O
des	O
habitants	O
de	O
la	O
ville	O
l'	O
approuvait	O
.	O
La	O
majorité	O
des	O
rues	O
de	O
la	O
ville	O
sont	O
dénommées	O
en	O
fonction	O
de	O
leur	O
localisation	O
par	O
rapport	O
au	O
Temple	LOCATION
mormon	O
de	O
Temple	LOCATION
Square	LOCATION
.	O
Le	O
second	O
est	O
nominatif	O
(	O
Rosa	PERSON
Parks	PERSON
,	O
Cesar	PERSON
Chavez	PERSON
...	O
)	O
et	O
purement	O
honorifique	O
.	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
est	O
le	O
siège	O
d'	O
un	O
évêché	O
catholique	O
.	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
est	O
le	O
principal	O
pôle	O
commercial	O
et	O
industriel	O
de	O
l'	O
Utah	LOCATION
.	O
La	O
région	O
possède	O
son	O
aéroport	O
:	O
l'	O
Aéroport	LOCATION
international	LOCATION
de	LOCATION
Salt	LOCATION
Lake	LOCATION
City	LOCATION
.	O
Il	O
est	O
l'	O
un	O
des	O
hubs	O
de	O
la	O
compagnie	O
aérienne	O
Delta	ORGANIZATION
Air	ORGANIZATION
Lines	ORGANIZATION
.	O
La	O
ville	O
est	O
le	O
siège	O
de	O
l'	O
université	ORGANIZATION
de	ORGANIZATION
l'	ORGANIZATION
Utah	ORGANIZATION
.	O
Salt	LOCATION
Lake	LOCATION
City	LOCATION
a	O
accueilli	O
les	O
Jeux	MISC
olympiques	MISC
d'	MISC
hiver	MISC
de	MISC
2002	MISC
.	O
