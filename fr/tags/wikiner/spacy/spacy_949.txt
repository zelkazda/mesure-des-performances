Les	O
origines	O
de	O
la	O
musique	O
occidentale	O
remontent	O
à	O
l'	O
Antiquité	MISC
:	O
les	O
échelles	O
de	O
la	O
Grèce	MISC
antique	MISC
influenceront	O
,	O
directement	O
ou	O
indirectement	O
,	O
les	O
musiciens	O
et	O
théoriciens	O
médiévaux	O
.	O
Par	O
le	O
jeu	O
des	O
échanges	O
,	O
elles	O
ont	O
influencé	O
des	O
auteurs	O
de	O
musique	O
classique	O
--	O
par	O
exemple	O
,	O
Dvořák	PERSON
ou	O
Moussorgski	PERSON
--	O
mais	O
subissent	O
en	O
retour	O
l'	O
influence	O
de	O
la	O
musique	O
savante	O
.	O
