Félix	PERSON
Grandet	PERSON
a	O
été	O
tonnelier	O
.	O
Les	O
habitants	O
de	O
Saumur	LOCATION
,	O
où	O
chacun	O
estime	O
la	O
fortune	O
du	O
père	O
Grandet	PERSON
,	O
voient	O
en	O
Eugénie	PERSON
Grandet	PERSON
le	O
plus	O
beau	O
parti	O
de	O
la	O
ville	O
,	O
et	O
deux	O
notables	O
la	O
courtisent	O
ardemment	O
.	O
Celle-ci	O
,	O
d'	O
une	O
innocence	O
réelle	O
et	O
d'	O
une	O
naïveté	O
prononcée	O
,	O
ne	O
se	O
doute	O
de	O
rien	O
,	O
jusqu'	O
au	O
jour	O
où	O
arrive	O
son	O
cousin	O
Charles	PERSON
Grandet	PERSON
,	O
fils	O
du	O
frère	O
du	O
père	O
Grandet	PERSON
.	O
C'	O
est	O
Grandet	PERSON
qui	O
annonce	O
son	O
malheur	O
au	O
jeune	O
homme	O
.	O
Grandet	PERSON
,	O
lui	O
,	O
s'	O
arrange	O
pour	O
éloigner	O
son	O
neveu	O
le	O
plus	O
vite	O
possible	O
,	O
et	O
rembourser	O
la	O
faillite	O
de	O
son	O
frère	O
en	O
dépensant	O
le	O
moins	O
possible	O
.	O
Le	O
roman	O
évoque	O
les	O
mentalités	O
sous	O
la	O
Restauration	MISC
française	MISC
et	O
mène	O
également	O
une	O
étude	O
de	O
l'	O
évolution	O
de	O
caractères	O
différents	O
au	O
cours	O
du	O
temps	O
,	O
de	O
l'	O
inflexibilité	O
du	O
père	O
Grandet	PERSON
,	O
de	O
la	O
perte	O
des	O
illusions	O
de	O
sa	O
fille	O
,	O
et	O
de	O
la	O
transformation	O
de	O
son	O
neveu	O
,	O
dandy	O
devenu	O
"	O
gentleman	O
"	O
pour	O
finir	O
en	O
personnage	O
froid	O
,	O
intéressé	O
,	O
et	O
calculateur	O
.	O
