Amenhotep	PERSON
IV	PERSON
est	O
le	O
neuvième	O
pharaon	O
de	O
la	O
XVIII	PERSON
e	PERSON
dynastie	PERSON
.	O
Il	O
est	O
le	O
fils	O
de	O
la	O
reine	O
Tiyi	PERSON
et	O
du	O
roi	O
Amenhotep	PERSON
III	PERSON
.	O
Avec	O
le	O
pharaon	O
hérétique	O
,	O
la	O
XVIII	PERSON
e	PERSON
dynastie	PERSON
touche	O
bientôt	O
à	O
sa	O
fin	O
.	O
La	O
possibilité	O
d'	O
une	O
corégence	O
du	O
jeune	O
Amenhotep	PERSON
IV	PERSON
avec	O
son	O
père	O
reste	O
incertaine	O
.	O
Durant	O
les	O
trois	O
premières	O
années	O
de	O
son	O
règne	O
,	O
Amenhotep	PERSON
IV	PERSON
s'	O
inscrit	O
en	O
continuateur	O
,	O
bien	O
que	O
modéré	O
et	O
déjà	O
novateur	O
,	O
de	O
l'	O
œuvre	O
de	O
ses	O
pères	O
.	O
Ses	O
constructions	O
à	O
Karnak	LOCATION
attestent	O
de	O
cette	O
tendance	O
double	O
.	O
Il	O
adjoint	O
au	O
troisième	O
pylone	O
de	O
Karnak	LOCATION
un	O
"	O
vestibule	O
"	O
,	O
sur	O
la	O
paroi	O
duquel	O
apparaît	O
une	O
scène	O
de	O
l'	O
imagerie	O
traditionnelle	O
.	O
Le	O
jeune	O
souverain	O
va	O
progressivement	O
d'	O
abord	O
,	O
puis	O
plus	O
brutalement	O
ensuite	O
,	O
imposer	O
la	O
première	O
religion	O
hénothéiste	O
connue	O
de	O
l'	O
histoire	O
,	O
privilégiant	O
le	O
culte	O
du	O
disque	O
solaire	O
Aton	PERSON
.	O
Pour	O
des	O
raisons	O
encore	O
mal	O
connues	O
,	O
mais	O
vraisemblablement	O
en	O
butte	O
au	O
conservatisme	O
et	O
à	O
l'	O
hostilité	O
du	O
clergé	O
thébain	O
,	O
Akhénaton	PERSON
décide	O
d'	O
abandonner	O
le	O
culte	O
du	O
dieu	O
dynastique	O
Amon	PERSON
,	O
le	O
"	O
dieu	O
caché	O
"	O
.	O
Il	O
entame	O
des	O
travaux	O
qui	O
draineront	O
une	O
grande	O
partie	O
des	O
revenus	O
affectés	O
à	O
Thèbes	LOCATION
.	O
On	O
attribue	O
souvent	O
cette	O
révolution	O
culturelle	O
et	O
religieuse	O
au	O
seul	O
Akhénaton	PERSON
,	O
mais	O
il	O
semble	O
qu'	O
il	O
n'	O
ait	O
fait	O
qu'	O
imposer	O
une	O
tendance	O
née	O
durant	O
le	O
règne	O
de	O
son	O
père	O
,	O
Amenhotep	PERSON
III	PERSON
.	O
Le	PERSON
roi	PERSON
est	O
l'	O
image	O
terrestre	O
d'	O
Aton	PERSON
,	O
son	O
"	O
enfant	O
parfait	O
"	O
;	O
avec	O
la	O
grande	O
épouse	O
royale	O
,	O
Néfertiti	PERSON
,	O
il	O
est	O
le	O
seul	O
intermédiaire	O
entre	O
la	O
divinité	O
et	O
les	O
humains	O
.	O
Ainsi	O
,	O
les	O
statues	O
colossales	O
découvertes	O
dans	O
le	O
temple	LOCATION
d'	LOCATION
Aton	LOCATION
à	O
Karnak	LOCATION
sont	O
à	O
l'	O
opposé	O
de	O
l'	O
art	O
classique	O
idéalisant	O
:	O
elles	O
montrent	O
le	O
roi	O
avec	O
un	O
corps	O
d'	O
androgyne	O
aux	O
hanches	O
exagérément	O
larges	O
,	O
le	O
ventre	O
proéminent	O
,	O
la	O
tête	O
allongée	O
et	O
les	O
lèvres	O
charnues	O
.	O
Sur	O
un	O
bas-relief	O
,	O
aujourd'hui	O
conservé	O
à	O
l'	O
Ägyptisches	LOCATION
Museum	LOCATION
,	O
Néfertiti	PERSON
et	O
les	O
petites	O
princesses	O
sont	O
représentées	O
avec	O
le	O
même	O
visage	O
étiré	O
en	O
longueur	O
,	O
en	O
tout	O
point	O
identique	O
à	O
celui	O
d'	O
Akhénaton	PERSON
qui	O
leur	O
fait	O
face	O
.	O
Il	O
se	O
peut	O
toutefois	O
qu'	O
Akhénaton	PERSON
ait	O
eu	O
un	O
physique	O
très	O
ingrat	O
,	O
voire	O
un	O
handicap	O
.	O
Akhénaton	PERSON
perpétue	O
la	O
tradition	O
de	O
rois	O
bâtisseurs	O
de	O
ses	O
prédécesseurs	O
.	O
Cette	O
dernière	O
,	O
dont	O
l'	O
importance	O
en	O
matière	O
de	O
politique	O
,	O
intérieure	O
comme	O
internationale	O
,	O
fut	O
avérée	O
à	O
Thèbes	LOCATION
sous	O
le	O
règne	O
précédent	O
,	O
fit	O
,	O
selon	O
certaines	O
représentations	O
,	O
plusieurs	O
séjours	O
dans	O
la	O
nouvelle	O
capitale	O
,	O
et	O
y	O
résida	O
peut-être	O
.	O
Loin	O
de	O
l'	O
image	O
idyllique	O
d'	O
un	O
pharaon	O
poète	O
et	O
rêveur	O
mystique	O
,	O
image	O
peut-être	O
exagérée	O
par	O
l'	O
imaginaire	O
collectif	O
,	O
le	O
règne	O
d'	O
Akhénaton	PERSON
est	O
aussi	O
considéré	O
par	O
beaucoup	O
d'	O
égyptologues	O
comme	O
une	O
période	O
sombre	O
dans	O
l'	O
histoire	O
de	O
l'	O
Égypte	LOCATION
antique	LOCATION
.	O
La	O
réforme	O
religieuse	O
d'	O
Akhénaton	PERSON
entraîna	O
une	O
perte	O
d'	O
influence	O
importante	O
des	O
dieux	O
du	O
panthéon	O
traditionnel	O
:	O
suppression	O
de	O
certains	O
cultes	O
,	O
fermeture	O
de	O
temples	O
,	O
perte	O
de	O
biens	O
du	O
clergé	O
,	O
dégradation	O
des	O
effigies	O
divines	O
,	O
ce	O
qui	O
vaudra	O
au	O
roi	O
d'	O
être	O
surnommé	O
--	O
de	O
manière	O
discutable	O
--	O
le	O
pharaon	O
hérétique	O
.	O
Le	O
Fayoum	LOCATION
semble	O
même	O
avoir	O
presque	O
complétement	O
échappé	O
au	O
martelage	O
.	O
Les	O
noms	O
théophores	O
au	O
sein	O
du	O
peuple	O
restent	O
inchangés	O
,	O
et	O
à	O
Akhetaton	LOCATION
même	O
,	O
la	O
découverte	O
de	O
petites	O
idoles	O
traditionnelles	O
dans	O
certaines	O
habitations	O
plaident	O
pour	O
la	O
continuité	O
des	O
croyances	O
polythéistes	O
habituelles	O
.	O
Les	O
plus	O
grands	O
spécialistes	O
étant	O
encore	O
très	O
partagés	O
sur	O
la	O
question	O
,	O
il	O
convient	O
donc	O
de	O
prendre	O
tous	O
les	O
faits	O
en	O
considération	O
afin	O
de	O
se	O
faire	O
une	O
idée	O
synthétique	O
des	O
bouleversements	O
apportés	O
par	O
Akhénaton	PERSON
.	O
En	O
Syrie	LOCATION
et	O
en	O
pays	LOCATION
de	LOCATION
Canaan	LOCATION
,	O
les	O
Hittites	MISC
et	O
les	O
Amorrites	MISC
grignotent	O
petit	O
à	O
petit	O
les	O
conquêtes	O
de	O
Thoutmôsis	PERSON
III	PERSON
.	O
Akhénaton	PERSON
omet	O
de	O
venir	O
en	O
aide	O
à	O
ses	O
vassaux	O
,	O
malgré	O
leurs	O
appels	O
pressants	O
,	O
de	O
sorte	O
que	O
son	O
inertie	O
cause	O
la	O
perte	O
de	O
Sidon	LOCATION
,	O
de	O
Tyr	LOCATION
et	O
de	O
Byblos	LOCATION
.	O
Pendant	O
ce	O
temps	O
,	O
des	O
bandes	O
de	O
nomades	O
pillards	O
,	O
les	O
Apirou	MISC
,	O
s'	O
emparent	O
de	O
Megiddo	MISC
et	O
de	O
Jérusalem	LOCATION
.	O
Alors	O
qu'	O
une	O
grande	O
partie	O
du	O
prestige	O
moral	O
du	O
royaume	O
,	O
et	O
de	O
son	O
influence	O
à	O
l'	O
extérieur	O
,	O
repose	O
sur	O
sa	O
prodigalité	O
(	O
ce	O
qu'	O
avait	O
parfaitement	O
compris	O
Amenhotep	PERSON
III	PERSON
)	O
,	O
Akhénaton	PERSON
est	O
beaucoup	O
moins	O
généreux	O
que	O
son	O
père	O
,	O
et	O
les	O
envois	O
d'	O
or	O
s'	O
amaigrissent	O
considérablement	O
.	O
La	O
mort	O
d'	O
Akhénaton	PERSON
est	O
entourée	O
de	O
mystère	O
.	O
Smenkhkarê	PERSON
,	O
gendre	O
et	O
successeur	O
d'	O
Akhénaton	PERSON
après	O
une	O
probable	O
corégence	O
,	O
meurt	O
après	O
un	O
règne	O
éphémère	O
.	O
Le	O
pouvoir	O
revient	O
alors	O
à	O
un	O
enfant	O
de	O
neuf	O
ans	O
,	O
Toutânkhaton	MISC
,	O
qui	O
avait	O
épousé	O
la	O
troisième	O
fille	O
d'	O
Akhénaton	PERSON
.	O
Ce	O
qui	O
est	O
sûr	O
en	O
revanche	O
,	O
c'	O
est	O
que	O
le	O
culte	O
d'	O
Aton	PERSON
s'	O
éteint	O
pratiquement	O
avec	O
la	O
disparition	O
d'	O
Akhénaton	PERSON
.	O
Au	O
bout	O
de	O
trois	O
ans	O
,	O
Toutânkhaton	LOCATION
quitte	O
Tell	LOCATION
el-Amarna	LOCATION
;	O
il	O
adopte	O
le	O
nom	O
de	O
Toutânkhamon	PERSON
,	O
restaure	O
le	O
culte	O
des	O
dieux	O
traditionnels	O
et	O
rétablit	O
le	O
clergé	O
dans	O
les	O
biens	O
dont	O
l'	O
avait	O
dépouillé	O
le	O
"	O
misérable	O
d'	O
Akhétaton	PERSON
"	O
.	O
La	O
tombe	O
d'	O
Akhénaton	PERSON
a	O
été	O
aménagée	O
dans	O
la	O
nécropole	O
royale	O
d'	O
Amarna	LOCATION
.	O
Le	O
tombeau	O
a	O
ensuite	O
été	O
fouillé	O
de	O
1893	O
à	O
1894	O
par	O
Alexandre	PERSON
Barsanti	PERSON
pour	O
le	O
compte	O
du	O
Service	ORGANIZATION
des	ORGANIZATION
Antiquités	ORGANIZATION
Égyptiennes	ORGANIZATION
,	O
le	O
dégageant	O
des	O
gravats	O
qui	O
l'	O
encombraient	O
,	O
révélant	O
son	O
plan	O
et	O
découvrant	O
les	O
restes	O
du	O
sarcophage	O
externe	O
du	O
roi	O
ainsi	O
que	O
de	O
son	O
coffre	O
à	O
canope	O
et	O
de	O
nombreux	O
fragments	O
d'	O
oushebtis	O
au	O
nom	O
du	O
roi	O
.	O
Ces	O
résultats	O
révélés	O
le	O
17	O
février	O
2010	O
à	O
la	O
presse	O
et	O
au	O
monde	O
scientifique	O
,	O
associés	O
aux	O
objets	O
déjà	O
découverts	O
dans	O
la	O
tombe	O
au	O
nom	O
d'	O
Akhénaton	PERSON
,	O
permettent	O
désormais	O
aux	O
égyptologues	O
de	O
confirmer	O
l'	O
hypothèse	O
jusque	O
là	O
supposée	O
qu'	O
il	O
sagit	O
bien	O
des	O
restes	O
du	O
roi	O
.	O
Akhénaton	PERSON
à	O
sa	O
mort	O
a	O
bien	O
été	O
momifié	O
et	O
reçut	O
une	O
sépulture	O
officielle	O
dans	O
la	O
nécropole	O
royale	O
de	O
sa	O
capitale	O
.	O
Après	O
le	O
règne	O
de	O
ce	O
dernier	O
héritier	O
de	O
la	O
famille	O
royale	O
de	O
la	O
XVIII	PERSON
e	PERSON
dynastie	PERSON
,	O
la	O
tombe	O
a	O
été	O
ouverte	O
et	O
les	O
objets	O
aux	O
noms	O
du	O
roi	O
ont	O
été	O
délibérément	O
saccagés	O
.	O
Agatha	PERSON
Christie	PERSON
en	O
a	O
fait	O
le	O
thème	O
d'	O
une	O
de	O
ses	O
pièces	O
de	O
théâtre	O
,	O
Akhnaton	LOCATION
.	O
Akhénaton	PERSON
est	O
un	O
des	O
principaux	O
personnages	O
de	O
Sinouhé	PERSON
l'	PERSON
Égyptien	PERSON
,	O
le	O
roman	O
de	O
Mika	PERSON
Waltari	PERSON
.	O
Il	O
est	O
aussi	O
le	O
héros	O
d'	O
un	O
roman	O
de	O
Naguib	O
Mahfouz	O
,	O
Akhénaton	PERSON
le	O
renégat	O
,	O
où	O
il	O
est	O
décrit	O
par	O
le	O
biais	O
de	O
confessions	O
imaginaires	O
de	O
membres	O
de	O
sa	O
cour	O
.	O
Edgar	PERSON
P.	PERSON
Jacobs	PERSON
évoque	O
l'	O
histoire	O
et	O
le	O
culte	O
d'	O
Akhénaton	PERSON
dans	O
la	O
bande	O
dessinée	O
Le	MISC
Mystère	MISC
de	MISC
la	MISC
Grande	MISC
Pyramide	MISC
.	O
