La	O
vie	O
politique	O
au	O
Chili	LOCATION
est	O
l'	O
ensemble	O
des	O
éléments	O
qui	O
influencent	O
les	O
activités	O
politiques	O
au	O
Chili	LOCATION
,	O
c'	O
est-à-dire	O
la	O
constitution	O
,	O
le	O
climat	O
politique	O
et	O
les	O
partis	O
politiques	O
.	O
Le	O
système	O
politique	O
du	O
Chili	LOCATION
est	O
la	O
démocratie	O
républicaine	O
.	O
Actuellement	O
,	O
la	O
présidente	O
est	O
Michelle	PERSON
Bachelet	PERSON
,	O
socialiste	O
,	O
chef	O
d'	O
une	O
coalition	O
de	O
partis	O
de	O
centre	O
et	O
de	O
gauche	O
appelée	O
Concertation	ORGANIZATION
des	ORGANIZATION
partis	ORGANIZATION
pour	ORGANIZATION
la	ORGANIZATION
démocratie	ORGANIZATION
,	O
qui	O
regroupe	O
la	O
Démocratie	ORGANIZATION
chrétienne	ORGANIZATION
,	O
le	O
Parti	ORGANIZATION
socialiste	ORGANIZATION
,	O
le	O
Parti	ORGANIZATION
radical	ORGANIZATION
social-démocrate	ORGANIZATION
et	O
le	O
Parti	ORGANIZATION
pour	ORGANIZATION
la	ORGANIZATION
démocratie	ORGANIZATION
.	O
Il	O
existe	O
une	O
opposition	O
de	O
droite	O
constituée	O
par	O
deux	O
partis	O
:	O
Renovación	ORGANIZATION
Nacional	ORGANIZATION
,	O
plus	O
libéral	O
;	O
et	O
l'	O
Unión	ORGANIZATION
Demócrata	ORGANIZATION
Independiente	ORGANIZATION
(	O
UDI	ORGANIZATION
)	O
,	O
plus	O
conservateur	O
.	O
Ces	O
deux	O
partis	O
forment	O
l'	O
Alliance	ORGANIZATION
pour	ORGANIZATION
le	ORGANIZATION
Chili	ORGANIZATION
.	O
Les	O
principaux	O
blocs	O
politiques	O
se	O
sont	O
constitués	O
au	O
milieu	O
des	O
années	O
1980	O
,	O
pour	O
défendre	O
ou	O
lutter	O
contre	O
la	O
dictature	ORGANIZATION
militaire	ORGANIZATION
d'	ORGANIZATION
Augusto	PERSON
Pinochet	PERSON
,	O
et	O
sont	O
devenus	O
officiels	O
par	O
le	O
référendum	O
de	O
1988	O
qui	O
a	O
mis	O
fin	O
à	O
la	O
dictature	O
.	O
Le	O
15	O
janvier	O
suivant	O
suivant	O
,	O
Bachelet	PERSON
remporte	O
le	O
scrutin	O
avec	O
53,5	O
%	O
des	O
suffrages	O
.	O
Le	O
candidat	O
de	O
la	O
Concertación	ORGANIZATION
,	O
l'	O
ancien	O
président	O
Eduardo	PERSON
Frei	PERSON
Ruiz-Tagle	PERSON
n'	O
en	O
n'	O
obtient	O
que	O
29,60	O
%	O
,	O
et	O
le	O
candidat	O
dissident	O
issu	O
des	O
socialistes	O
,	O
Marco	PERSON
Enríquez-Ominami	PERSON
,	O
rassemble	O
20,14	O
%	O
des	O
voix	O
.	O
