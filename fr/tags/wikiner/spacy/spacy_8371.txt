Le	O
rap	O
a	O
débuté	O
aux	O
États-Unis	LOCATION
à	O
la	O
fin	O
des	O
années	O
70	O
.	O
On	O
peut	O
citer	O
le	O
groupe	O
américain	O
Public	ORGANIZATION
Enemy	ORGANIZATION
ou	O
le	O
groupe	O
français	O
Assassin	ORGANIZATION
.	O
La	O
première	O
chanson	O
de	O
ce	O
genre	O
est	O
The	MISC
Message	MISC
écrite	O
et	O
chantée	O
par	O
Grandmaster	MISC
Flash	O
en	O
1979	O
.	O
Dans	O
les	O
années	O
1960	O
et	O
70	O
,	O
James	PERSON
Brown	PERSON
jette	O
les	O
bases	O
sur	O
lesquelles	O
sera	O
fondé	O
le	O
rap	O
:	O
une	O
musique	O
rythmée	O
(	O
ses	O
enregistrements	O
sont	O
encore	O
aujourd'hui	O
une	O
source	O
de	O
samples	O
inépuisable	O
pour	O
les	O
DJ	O
)	O
,	O
un	O
style	O
de	O
chant	O
saccadé	O
,	O
parfois	O
parlé	O
ou	O
crié	O
et	O
des	O
textes	O
véhiculant	O
une	O
forte	O
identité	O
et	O
des	O
revendications	O
sociales	O
ou	O
politiques	O
.	O
Alors	O
que	O
le	O
mixage	O
réalisé	O
par	O
les	O
DJ	O
disco	O
et	O
de	O
clubs	O
avait	O
pour	O
but	O
de	O
produire	O
une	O
musique	O
continuelle	O
avec	O
des	O
transitions	O
discrètes	O
entre	O
les	O
morceaux	O
,	O
celui	O
réalisé	O
par	O
Kool	PERSON
DJ	PERSON
Herc	PERSON
à	O
lui	O
donné	O
naissance	O
à	O
une	O
pratique	O
visant	O
à	O
isoler	O
et	O
à	O
étendre	O
les	O
seuls	O
breaks	O
en	O
les	O
mélangeant	O
entre	O
eux	O
avec	O
deux	O
copies	O
du	O
même	O
morceau	O
.	O
C'	O
est	O
ce	O
qu'	O
Afrika	PERSON
Bambaataa	PERSON
décrivit	O
comme	O
"	O
la	O
partie	O
du	O
disque	O
qu'	O
attend	O
tout	O
le	O
monde	O
…	O
où	O
ils	O
se	O
laissent	O
aller	O
et	O
font	O
les	O
fous	O
"	O
.	O
James	PERSON
Brown	PERSON
,	O
Bob	PERSON
James	PERSON
et	O
Parliament	ORGANIZATION
--	O
parmi	O
d'	O
autres	O
--	O
ont	O
longtemps	O
été	O
des	O
sources	O
populaires	O
pour	O
les	O
breaks	O
.	O
Toutefois	O
,	O
on	O
peut	O
noter	O
ces	O
dernières	O
années	O
une	O
tendance	O
de	O
retour	O
vers	O
les	O
instruments	O
originaux	O
avec	O
des	O
musiciens	O
et	O
producteurs	O
tels	O
que	O
Timbaland	PERSON
,	O
Outkast	ORGANIZATION
,	O
The	ORGANIZATION
Roots	ORGANIZATION
et	O
The	ORGANIZATION
Neptunes	ORGANIZATION
.	O
Timbaland	PERSON
a	O
récemment	O
démontré	O
,	O
que	O
la	O
batterie	O
pouvait	O
convenir	O
au	O
rap	O
,	O
grâce	O
à	O
la	O
grosse	O
caisse	O
et	O
autres	O
.	O
Et	O
ainsi	O
,	O
d'	O
autres	O
rappeurs	O
vont	O
suivre	O
ce	O
mouvement	O
,	O
celle	O
de	O
l'	O
association	O
d'	O
instruments	O
à	O
percussions	O
,	O
qui	O
mettent	O
en	O
valeur	O
leurs	O
origines	O
lointaines	O
,	O
venant	O
d'	O
Afrique	LOCATION
et	O
autres	O
pays	O
colonisés	O
.	O
Le	O
"	O
flow	O
"	O
est	O
un	O
terme	O
inventé	O
par	O
le	O
rappeur	O
Rakim	PERSON
,	O
et	O
qui	O
signifie	O
la	O
manière	O
dont	O
le	O
rappeur	O
chante	O
.	O
Cependant	O
,	O
des	O
artistes	O
,	O
comme	O
Sinik	PERSON
en	O
France	LOCATION
,	O
possèdent	O
un	O
flow	O
strictement	O
linéaire	O
(	O
lorsque	O
le	O
rappeur	O
place	O
systématiquement	O
le	O
même	O
nombre	O
de	O
syllabe	O
,	O
souvent	O
4	O
,	O
par	O
pulsation	O
,	O
ce	O
qui	O
est	O
souvent	O
perçu	O
comme	O
étant	O
répétitif	O
et	O
monotone	O
)	O
.	O
En	O
1979	O
,	O
quelques	O
mois	O
après	O
,	O
le	O
premier	O
tube	O
rap	O
sort	O
en	O
45	O
tours	O
,	O
c'	O
est	O
Rapper	MISC
's	MISC
Delight	MISC
du	O
Sugarhill	ORGANIZATION
Gang	ORGANIZATION
.	O
En	O
1982	O
,	O
The	MISC
Message	MISC
de	O
Grandmaster	O
Flash	O
fut	O
la	O
révolution	O
annoncée	O
.	O
Il	O
est	O
d'	O
ailleurs	O
curieux	O
que	O
,	O
malgré	O
le	O
fait	O
que	O
ce	O
soit	O
le	O
rappeur	O
Melle	PERSON
Mel	PERSON
qu'	O
on	O
entend	O
sur	O
l'	O
enregistrement	O
,	O
le	O
titre	O
est	O
crédité	O
du	O
nom	O
de	O
Grand	MISC
Master	MISC
Flash	MISC
(	O
le	O
DJ	O
--	O
concepteur	O
sonore	O
)	O
.	O
Les	O
rappeurs	O
américains	O
tel	O
que	O
Run	ORGANIZATION
DMC	ORGANIZATION
critiquent	O
le	O
racisme	O
des	O
blancs	O
dans	O
leurs	O
chansons	O
,	O
la	O
majorité	O
des	O
auditeurs	O
sont	O
alors	O
des	O
noirs	O
.	O
Les	O
Beastie	ORGANIZATION
Boys	ORGANIZATION
commencer	O
eux	O
aussi	O
a	O
se	O
faire	O
connaitre	O
,	O
prouvant	O
et	O
montrant	O
ainsi	O
que	O
la	O
culture	O
hip	O
hop	O
etait	O
bien	O
un	O
melange	O
de	O
culture	O
et	O
d'	O
influence	O
noir	O
et	O
blanche	O
.	O
Les	O
années	O
1980	O
furent	O
celles	O
de	O
l'	O
explosion	O
du	O
rap	O
avec	O
des	O
groupes	O
politiques	O
comme	O
Public	ORGANIZATION
Enemy	ORGANIZATION
ou	O
entertainment	O
comme	O
Run-DMC	ORGANIZATION
.	O
Il	O
s'	O
agit	O
d'	O
une	O
véritable	O
musique	O
populaire	O
de	O
rue	O
qui	O
développait	O
ses	O
propres	O
thèmes	O
:	O
d'	O
une	O
part	O
sous	O
l'	O
influence	O
de	O
la	O
Universal	ORGANIZATION
Zulu	ORGANIZATION
Nation	ORGANIZATION
(	O
ou	O
plus	O
communément	O
appelée	O
Zulu	ORGANIZATION
Nation	ORGANIZATION
)	O
d'	O
Afrika	PERSON
Bambaataa	PERSON
qui	O
voyait	O
dans	O
le	O
hip	O
hop	O
le	O
moyen	O
d'	O
éloigner	O
les	O
jeunes	O
des	O
drogues	O
et	O
des	O
gangs	O
et	O
d'	O
émuler	O
leur	O
créativité	O
,	O
d'	O
autre	O
part	O
en	O
tant	O
que	O
témoignage	O
d'	O
une	O
vie	O
difficile	O
(	O
rap	O
"	O
hardcore	O
"	O
)	O
.	O
À	O
New	LOCATION
York	LOCATION
,	O
la	O
guerre	O
des	O
crews	O
(	O
"	O
équipes	O
"	O
)	O
se	O
termine	O
.	O
En	O
effet	O
alors	O
que	O
le	O
rap	O
new-yorkais	O
produit	O
un	O
rap	O
funky	O
[	O
réf.	O
nécessaire	O
]	O
ou	O
conscient	O
,	O
les	O
NWA	ORGANIZATION
créent	O
le	O
rap	O
gangsta	O
.	O
C'	O
est	O
Puff	PERSON
Daddy	PERSON
qui	O
révolutionne	O
une	O
nouvelle	O
fois	O
le	O
rap	O
new-yorkais	O
en	O
mettant	O
un	O
peu	O
de	O
fête	O
et	O
en	O
samplant	O
de	O
la	O
funk	O
qui	O
permet	O
à	O
Notorious	PERSON
B.I.G.	PERSON
d'	O
avoir	O
une	O
énorme	O
couverture	O
médiatique	O
et	O
de	O
rivaliser	O
avec	O
les	O
rappeurs	O
de	O
Los	LOCATION
Angeles	LOCATION
.	O
Le	O
rap	O
est	O
apparu	O
aux	O
États-Unis	LOCATION
et	O
s'	O
est	O
répandu	O
à	O
travers	O
le	O
monde	O
dans	O
les	O
années	O
1980	O
.	O
Cependant	O
Roi	PERSON
Heenok	PERSON
possède	O
une	O
légitimité	O
envers	O
les	O
gangsters	O
.	O
Vico	PERSON
C	PERSON
est	O
le	O
premier	O
rappeur	O
latino	O
qui	O
sera	O
à	O
l'	O
origine	O
du	O
reggaeton	O
.	O
Le	O
rap	O
sud-américain	O
est	O
surtout	O
connu	O
en	O
France	LOCATION
par	O
le	O
succès	O
du	O
groupe	O
nord-américain	O
Cypress	ORGANIZATION
Hill	ORGANIZATION
ou	O
de	O
kid	O
frost	O
.	O
Certains	O
pays	O
ont	O
une	O
influence	O
francophone	O
et	O
d'	O
autres	O
une	O
influence	O
anglophone	O
(	O
Nigeria	LOCATION
,	O
Ghana	LOCATION
)	O
,	O
ces	O
influences	O
n'	O
empêchant	O
pas	O
une	O
identité	O
africaine	O
propre	O
dans	O
les	O
rythmes	O
et	O
les	O
flows	O
au	O
slang	O
particulier	O
.	O
Mais	O
l'	O
on	O
observe	O
partout	O
en	O
Afrique	LOCATION
un	O
développement	O
de	O
la	O
scene	O
rap	O
comme	O
en	O
Côte	LOCATION
d'	LOCATION
Ivoire	LOCATION
ou	O
le	O
Bénin	LOCATION
.	O
