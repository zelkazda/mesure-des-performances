Le	O
symbolisme	O
est	O
un	O
mouvement	O
littéraire	O
et	O
artistique	O
apparu	O
en	O
France	LOCATION
et	O
en	O
Belgique	LOCATION
vers	O
1870	O
,	O
en	O
réaction	O
au	O
naturalisme	O
et	O
au	O
mouvement	O
parnassien	O
.	O
L'	O
influence	O
de	O
Stéphane	PERSON
Mallarmé	PERSON
est	O
ici	O
considérable	O
,	O
ce	O
qui	O
entraîne	O
la	O
poésie	O
vers	O
l'	O
hermétisme	O
.	O
En	O
littérature	O
,	O
le	O
mouvement	O
du	O
symbolisme	O
trouve	O
ses	O
origines	O
dans	O
Les	MISC
Fleurs	MISC
du	MISC
mal	MISC
(	O
1857	O
)	O
de	O
Charles	PERSON
Baudelaire	PERSON
.	O
L'	O
esthétique	O
symboliste	O
fut	O
développée	O
par	O
Stéphane	PERSON
Mallarmé	PERSON
et	O
Paul	PERSON
Verlaine	PERSON
durant	O
les	O
années	O
1860	O
et	O
1870	O
.	O
La	O
traduction	O
en	O
français	O
par	O
Baudelaire	PERSON
de	O
l'	O
œuvre	O
d'	O
Edgar	PERSON
Allan	PERSON
Poe	PERSON
,	O
d'	O
une	O
influence	O
considérable	O
,	O
fut	O
à	O
l'	O
origine	O
de	O
plusieurs	O
tropes	O
et	O
images	O
du	O
symbolisme	O
.	O
Le	O
roman	O
À	MISC
rebours	MISC
(	O
1884	O
)	O
de	O
Joris-Karl	PERSON
Huysmans	PERSON
contient	O
plusieurs	O
thèmes	O
qui	O
furent	O
par	O
la	O
suite	O
associés	O
à	O
l'	O
esthétique	O
symboliste	O
.	O
Le	O
roman	O
fut	O
imité	O
par	O
Oscar	PERSON
Wilde	PERSON
dans	O
plusieurs	O
passages	O
du	O
Portrait	MISC
de	MISC
Dorian	MISC
Gray	MISC
.	O
Paul	PERSON
Adam	PERSON
était	O
le	O
plus	O
prolifique	O
et	O
représentatif	O
romancier	O
symboliste	O
.	O
Une	O
autre	O
fiction	O
étant	O
parfois	O
considérée	O
comme	O
symboliste	O
sont	O
les	O
contes	O
misanthropiques	O
(	O
et	O
surtout	O
,	O
misogynes	O
)	O
de	O
Jules	PERSON
Barbey	PERSON
d'	PERSON
Aurevilly	PERSON
.	O
Le	O
premier	O
roman	O
de	O
Gabriele	PERSON
D'	PERSON
Annunzio	PERSON
fut	O
aussi	O
écrit	O
dans	O
un	O
esprit	O
symboliste	O
.	O
Plusieurs	O
écrivains	O
et	O
critiques	O
symbolistes	O
étaient	O
positifs	O
à	O
l'	O
égard	O
de	O
la	O
musique	O
de	O
Richard	PERSON
Wagner	PERSON
.	O
L'	O
esthétique	O
symboliste	O
eu	O
une	O
influence	O
importante	O
sur	O
le	O
travail	O
de	O
Claude	PERSON
Debussy	PERSON
.	O
Son	O
œuvre	O
clé	O
,	O
la	O
Prélude	O
à	O
l'	O
après-midi	O
d'	O
un	O
faune	O
,	O
était	O
inspirée	O
par	O
un	O
poème	O
de	O
Stéphane	PERSON
Mallarmé	PERSON
,	O
L'	MISC
Après-Midi	MISC
d'	MISC
un	MISC
faune	MISC
.	O
