Ses	O
frères	O
sont	O
Gilles	PERSON
Boileau	PERSON
et	O
Jacques	PERSON
Boileau	PERSON
.	O
La	O
scolastique	O
n'	O
eut	O
pas	O
plus	O
d'	O
attraits	O
pour	O
ce	O
disciple	O
d'	O
Horace	MISC
,	O
et	O
il	O
se	O
livra	O
dès	O
lors	O
tout	O
entier	O
aux	O
lettres	O
.	O
Sa	O
première	O
satire	O
parut	O
dans	O
un	O
temps	O
où	O
,	O
malgré	O
les	O
succès	O
de	O
Pierre	PERSON
Corneille	PERSON
et	O
de	O
Molière	PERSON
,	O
Jean	PERSON
Chapelain	PERSON
était	O
encore	O
l'	O
oracle	O
de	O
la	O
littérature	O
.	O
Au	O
contraire	O
,	O
il	O
admire	O
Molière	PERSON
et	O
,	O
plus	O
tard	O
,	O
La	PERSON
Fontaine	PERSON
et	O
Jean	PERSON
Racine	PERSON
.	O
Boileau	PERSON
était	O
humble	O
mais	O
on	O
le	O
sentait	O
supérieur	O
dans	O
ses	O
paroles	O
.	O
Marie-Nicolas	PERSON
Bouillet	PERSON
et	O
Alexis	PERSON
Chassang	PERSON
(	O
dir	O
.	O
