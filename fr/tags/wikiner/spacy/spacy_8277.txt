Il	O
s'	O
agit	O
principalement	O
d'	O
une	O
compilation	O
et	O
d'	O
une	O
interprétation	O
du	O
Code	MISC
de	MISC
Théodose	MISC
(	O
438	O
)	O
,	O
destinée	O
aux	O
sujets	O
gallo-romains	O
et	O
romano-hispaniques	O
des	O
Wisigoths	MISC
.	O
Il	O
est	O
ensuite	O
approuvé	O
par	O
les	O
notables	O
gallo-romains	O
,	O
ecclésiastiques	O
et	O
laïques	O
,	O
avant	O
d'	O
être	O
promulgué	O
par	O
le	O
roi	O
Alaric	PERSON
II	PERSON
.	O
Certains	O
historiens	O
pensent	O
aujourd'hui	O
que	O
le	O
Bréviaire	MISC
d'	MISC
Alaric	MISC
serait	O
une	O
pure	O
opération	O
de	O
propagande	O
destinée	O
à	O
obtenir	O
l'	O
adhésion	O
des	O
population	O
gallo-romaines	O
,	O
alors	O
qu'	O
Alaric	PERSON
II	PERSON
ne	O
pouvait	O
pas	O
se	O
targuer	O
,	O
comme	O
Clovis	PERSON
,	O
de	O
s'	O
être	O
converti	O
au	O
catholicisme	O
,	O
mais	O
restait	O
arien	O
.	O
