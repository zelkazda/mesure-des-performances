Le	O
Nicaragua	LOCATION
est	O
un	O
pays	O
d'	O
Amérique	LOCATION
centrale	LOCATION
.	O
Il	O
est	O
limitrophe	O
du	O
Costa	LOCATION
Rica	LOCATION
au	O
sud	O
et	O
du	O
Honduras	LOCATION
au	O
nord	O
.	O
Il	O
est	O
aussi	O
entouré	O
par	O
l'	O
océan	LOCATION
Pacifique	LOCATION
et	O
la	O
mer	LOCATION
des	LOCATION
Caraïbes	LOCATION
.	O
Entre	O
1927	O
et	O
1933	O
,	O
le	O
général	O
Augusto	PERSON
Sandino	PERSON
d'	O
obédience	O
libérale	O
mena	O
une	O
guérilla	O
,	O
d'	O
abord	O
contre	O
le	O
gouvernement	O
conservateur	O
,	O
puis	O
contre	O
les	O
forces	O
américaines	O
.	O
La	O
guérilla	O
fut	O
finalement	O
repoussée	O
par	O
l'	O
United	ORGANIZATION
States	ORGANIZATION
Marine	ORGANIZATION
Corps	O
(	O
USMC	ORGANIZATION
)	O
qui	O
compensa	O
son	O
infériorité	O
numérique	O
par	O
l'	O
appui	O
de	O
l'	O
aviation	O
et	O
de	O
l'	O
artillerie	O
.	O
À	O
la	O
fin	O
de	O
l'	O
intervention	O
américaine	O
,	O
les	O
rebelles	O
avaient	O
été	O
repoussés	O
loin	O
de	O
toute	O
agglomération	O
et	O
réduits	O
à	O
la	O
famine	O
et	O
la	O
désertion	O
,	O
le	O
gouvernement	O
nicaraguayen	O
se	O
trouvant	O
en	O
position	O
de	O
force	O
pour	O
les	O
négociations	O
avec	O
le	O
mouvement	O
de	O
Sandino	PERSON
qui	O
aboutit	O
à	O
la	O
paix	O
en	O
1933	O
.	O
Anastasio	PERSON
Somoza	PERSON
García	PERSON
fut	O
le	O
premier	O
dirigeant	O
de	O
la	O
garde	O
nationale	O
.	O
De	O
retour	O
en	O
1948	O
,	O
il	O
succéda	O
à	O
son	O
père	O
comme	O
éditeur	O
de	O
La	LOCATION
Prensa	LOCATION
où	O
ses	O
positions	O
contre	O
la	O
dictature	O
lui	O
valurent	O
d'	O
être	O
arrêté	O
,	O
torturé	O
et	O
emprisonné	O
en	O
1954	O
puis	O
en	O
résidence	O
surveillée	O
.	O
Mais	O
,	O
resté	O
chef	O
de	O
la	O
Garde	ORGANIZATION
nationale	ORGANIZATION
,	O
il	O
profita	O
de	O
la	O
situation	O
catastrophique	O
créée	O
par	O
un	O
tremblement	O
de	O
terre	O
en	O
décembre	O
1972	O
pour	O
promulguer	O
la	O
loi	O
martiale	O
et	O
prendre	O
ainsi	O
le	O
contrôle	O
du	O
pays	O
.	O
Mais	O
le	O
10	O
janvier	O
1978	O
,	O
Pedro	PERSON
Joaquín	PERSON
Chamorro	PERSON
Cardenal	PERSON
fut	O
assassiné	O
.	O
Ses	O
funérailles	O
font	O
déplacer	O
des	O
foules	O
énormes	O
--	O
30	O
000	O
personnes	O
à	O
Managua	LOCATION
--	O
et	O
des	O
émeutes	O
éclatent	O
dans	O
le	O
pays	O
.	O
Peu	O
à	O
peu	O
,	O
les	O
trois	O
tendances	O
du	O
FSLN	ORGANIZATION
se	O
rapprochent	O
.	O
La	O
réunification	O
du	O
FSLN	ORGANIZATION
est	O
signée	O
en	O
1979	O
cependant	O
que	O
l'	O
opposition	O
conservatrice	O
se	O
renforçait	O
.	O
L'	O
extrême	O
disparité	O
de	O
cette	O
coalition	O
entraîna	O
des	O
conflits	O
continuels	O
et	O
les	O
quatre	O
membres	O
non	O
sandinistes	O
de	O
la	O
coalition	O
dénoncèrent	O
la	O
mainmise	O
progressive	O
de	O
Daniel	PERSON
Ortega	PERSON
sur	O
les	O
organes	O
du	O
pouvoir	O
malgré	O
les	O
accords	O
passés	O
entre	O
les	O
différents	O
acteurs	O
du	O
renversement	O
de	O
la	O
dictature	O
somoziste	O
.	O
Face	O
à	O
de	O
petits	O
adversaires	O
,	O
Daniel	PERSON
Ortega	PERSON
remporta	O
les	O
élections	O
en	O
1984	O
.	O
La	O
rébellion	O
s'	O
étendit	O
mais	O
sans	O
chef	O
unique	O
,	O
elle	O
restait	O
très	O
disparate	O
;	O
elle	O
regroupait	O
tous	O
ceux	O
qui	O
étaient	O
contre	O
le	O
gouvernement	O
sandiniste	O
(	O
de	O
même	O
que	O
se	O
donnaient	O
le	O
nom	O
de	O
sandinistes	O
toutes	O
sortes	O
de	O
courants	O
anti-somozistes	O
...	O
)	O
et	O
reçut	O
pour	O
cela	O
le	O
nom	O
de	O
Contras	ORGANIZATION
.	O
Les	O
États-Unis	LOCATION
,	O
alors	O
dirigés	O
par	O
le	O
président	O
Ronald	PERSON
Reagan	PERSON
décrétèrent	O
un	O
embargo	O
et	O
vinrent	O
en	O
aide	O
aux	O
Contras	ORGANIZATION
en	O
les	O
entraînant	O
,	O
les	O
armant	O
,	O
les	O
finançant	O
et	O
les	O
approvisionnant	O
à	O
partir	O
de	O
1982	O
.	O
L'	O
aide	O
aux	O
Contras	ORGANIZATION
continua	O
jusqu'	O
en	O
1989	O
au	O
moment	O
où	O
éclata	O
le	O
scandale	O
de	O
l'	O
Irangate	ORGANIZATION
.	O
Les	O
élections	O
de	O
1990	O
virent	O
la	O
victoire	O
de	O
Violeta	PERSON
Chamorro	PERSON
(	O
54,2	O
%	O
des	O
voix	O
)	O
sur	O
Daniel	PERSON
Ortega	PERSON
qui	O
,	O
prennant	O
acte	O
de	O
sa	O
défaite	O
,	O
déclara	O
qu'	O
il	O
continuerait	O
à	O
"	O
gouverner	O
d'	O
en	O
bas	O
"	O
,	O
déclaration	O
qui	O
détourna	O
de	O
lui	O
les	O
populations	O
car	O
elle	O
rappelait	O
son	O
attitude	O
au	O
sein	O
de	O
la	O
coalition	O
de	O
1979	O
.	O
La	O
stabilité	O
économique	O
du	O
Nicaragua	LOCATION
fut	O
fortement	O
ébranlée	O
en	O
1998	O
,	O
lorsque	O
l'	O
ouragan	MISC
Mitch	MISC
dévasta	O
une	O
bonne	O
partie	O
du	O
pays	O
.	O
Il	O
prit	O
ses	O
fonctions	O
le	O
10	O
janvier	O
2007	O
et	O
choisit	O
comme	O
vice-président	O
un	O
ancien	O
Contra	ORGANIZATION
.	O
Le	O
Nicaragua	LOCATION
est	O
entouré	O
par	O
la	O
mer	LOCATION
des	LOCATION
Caraïbes	LOCATION
à	O
l'	O
est	O
,	O
l'	O
Océan	LOCATION
Pacifique	LOCATION
à	O
l'	O
ouest	O
,	O
le	O
Costa	LOCATION
Rica	LOCATION
au	O
sud	O
,	O
et	O
le	O
Honduras	LOCATION
au	O
nord	O
.	O
Le	O
Nicaragua	LOCATION
est	O
un	O
pays	O
relativement	O
montagneux	O
.	O
Les	O
plus	O
hauts	O
sommets	O
se	O
situent	O
au	O
nord	O
,	O
près	O
de	O
la	O
frontière	O
du	O
Honduras	LOCATION
.	O
Le	O
sud	O
du	O
pays	O
,	O
à	O
la	O
frontière	O
du	O
Costa	LOCATION
Rica	LOCATION
,	O
est	O
marécageux	O
.	O
Le	O
lac	LOCATION
Nicaragua	LOCATION
compte	O
plusieurs	O
îles	O
,	O
dont	O
l'	O
île	O
volcanique	O
d'	O
Ometepe	LOCATION
et	O
l'	O
archipel	O
des	O
Îles	O
Solentiname	O
.	O
La	O
partie	O
est	O
du	O
pays	O
,	O
exposée	O
aux	O
fortes	O
précipitations	O
,	O
ouragans	O
et	O
cyclones	O
qui	O
traversent	O
régulièrement	O
la	O
mer	LOCATION
des	LOCATION
Caraïbes	LOCATION
,	O
est	O
très	O
peu	O
peuplée	O
,	O
sauf	O
sur	O
quelques	O
ports	O
de	O
la	O
côte	O
atlantique	O
et	O
sur	O
les	O
îles	O
de	O
la	O
mer	LOCATION
des	LOCATION
Caraïbes	LOCATION
.	O
Le	O
Nicaragua	LOCATION
compte	O
plus	O
de	O
1400	O
espèces	O
animales	O
répertoriées	O
(	O
coyote	O
,	O
cerf	O
,	O
tatou	O
,	O
fourmilier	O
,	O
pécari	O
,	O
alligator	O
,	O
tortues	O
,	O
serpents	O
,	O
lézards	O
,	O
iguanes	O
,	O
etc	O
.	O
Le	O
lac	LOCATION
Nicaragua	LOCATION
abritait	O
un	O
requin	O
d'	O
eau	O
douce	O
.	O
Le	O
Nicaragua	LOCATION
compte	O
environ	O
17000	O
espèces	O
de	O
végétaux	O
,	O
dont	O
5000	O
espèces	O
non	O
encore	O
répertoriées	O
.	O
On	O
dénombre	O
au	O
Nicaragua	LOCATION
699	O
espèces	O
d'	O
oiseaux	O
,	O
dont	O
une	O
endémique	O
,	O
deux	O
introduites	O
par	O
l'	O
homme	O
,	O
et	O
14	O
rarement	O
présentes	O
.	O
Le	O
Nicaragua	LOCATION
compte	O
une	O
multitude	O
d'	O
espèces	O
aquatiques	O
et	O
regroupe	O
plusieurs	O
familles	O
dont	O
les	O
cichlidae	O
comme	O
notamment	O
:	O
La	O
capitale	O
nicaraguayenne	O
est	O
Managua	LOCATION
(	O
plus	O
de	O
1,5	O
million	O
d'	O
habitants	O
)	O
.	O
Le	O
Nicaragua	LOCATION
est	O
une	O
république	O
présidentielle	O
.	O
Les	O
États-Unis	LOCATION
d'	LOCATION
Amérique	LOCATION
ont	O
réagi	O
prudemment	O
à	O
la	O
victoire	O
de	O
l'	O
ancien	O
marxiste	O
.	O
Les	O
dirigeants	O
vénézuélien	O
Hugo	PERSON
Chávez	PERSON
et	O
cubain	O
Fidel	PERSON
Castro	PERSON
,	O
se	O
sont	O
,	O
quant	O
à	O
eux	O
,	O
félicités	O
de	O
cette	O
"	O
victoire	O
grandiose	O
"	O
.	O
Environ	O
35	O
%	O
de	O
la	O
production	O
nationale	O
est	O
exportée	O
vers	O
les	O
États-Unis	LOCATION
et	O
13	O
%	O
en	O
Allemagne	LOCATION
(	O
1998	O
)	O
.	O
Le	O
Nicaragua	LOCATION
est	O
un	O
des	O
trois	O
pays	O
américains	O
à	O
avoir	O
bénéficié	O
de	O
l	O
'	O
"	O
initiative	O
pays	O
pauvres	O
très	O
endettés	O
"	O
.	O
Le	O
Nicaragua	LOCATION
a	O
pour	O
codes	O
:	O
