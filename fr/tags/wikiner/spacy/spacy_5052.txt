Ce	O
néologisme	O
a	O
été	O
inventé	O
afin	O
de	O
distinguer	O
les	O
libertariens	O
des	O
libéraux	O
des	O
États-Unis	LOCATION
,	O
à	O
gauche	O
de	O
l'	O
échiquier	O
politique	O
,	O
le	O
libertarianisme	O
se	O
faisant	O
le	O
promoteur	O
d'	O
un	O
marché	O
sans	O
entrave	O
au	O
nom	O
de	O
la	O
liberté	O
individuelle	O
.	O
L'	O
évolution	O
se	O
poursuit	O
dans	O
les	O
années	O
1920	O
,	O
au	O
cours	O
desquelles	O
l'	O
économiste	O
Keynes	PERSON
redéfinit	O
un	O
nouveau	O
libéralisme	O
.	O
Dans	O
les	O
années	O
1970	O
,	O
Henri	PERSON
Lepage	PERSON
,	O
en	O
traduisant	O
le	O
terme	O
libertarian	O
,	O
et	O
en	O
l'	O
absence	O
de	O
littérature	O
libertarian	O
francophone	O
,	O
n'	O
a	O
pas	O
voulu	O
risquer	O
l'	O
amalgame	O
avec	O
les	O
libertaires	O
,	O
et	O
a	O
donc	O
préféré	O
utiliser	O
"	O
libertarien	O
"	O
.	O
Les	O
libertarian	O
francophones	O
du	O
Québec	LOCATION
ont	O
repris	O
le	O
terme	O
"	O
libertarien	O
"	O
,	O
phonétiquement	O
proche	O
de	O
l'	O
américain	O
libertarian	O
.	O
Certains	O
libéraux/libertariens	O
considèrent	O
l'	O
usage	O
du	O
terme	O
comme	O
un	O
anglicisme	O
et	O
une	O
erreur	O
,	O
puisqu'	O
en	O
France	LOCATION
,	O
le	O
terme	O
"	O
libéral	O
"	O
ne	O
prête	O
pas	O
à	O
confusion	O
(	O
même	O
s'	O
il	O
a	O
pris	O
un	O
sens	O
plus	O
large	O
)	O
puisque	O
ceux	O
qui	O
s'	O
en	O
réclament	O
défendent	O
bien	O
le	O
libre-échange	O
et	O
ceux	O
qui	O
s'	O
y	O
opposent	O
le	O
font	O
sur	O
cette	O
base	O
.	O
Ils	O
relèvent	O
notamment	O
la	O
tradition	O
de	O
libéraux	O
comme	O
Frédéric	PERSON
Bastiat	PERSON
dont	O
ils	O
se	O
réclament	O
.	O
Pour	O
Noam	PERSON
Chomsky	PERSON
,	O
"	O
la	O
version	O
américaine	O
du	O
"	O
libertarisme	O
"	O
est	O
une	O
aberration	O
--	O
personne	O
ne	O
la	O
prend	O
vraiment	O
au	O
sérieux	O
.	O
En	O
France	LOCATION
,	O
l'	O
association	O
politique	O
Liberté	ORGANIZATION
Chérie	ORGANIZATION
et	O
le	O
parti	O
politique	O
Alternative	ORGANIZATION
libérale	ORGANIZATION
diffusent	O
des	O
analyses	O
libertariennes	O
ou	O
proches	O
du	O
libertarianisme	O
.	O
