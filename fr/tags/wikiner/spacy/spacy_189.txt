On	O
en	O
doit	O
l'	O
invention	O
à	O
John	PERSON
Tukey	PERSON
et	O
la	O
popularisation	O
à	O
Claude	PERSON
Shannon	PERSON
.	O
L'	O
Intel	MISC
4004	MISC
est	O
le	O
premier	O
microprocesseur	O
.	O
Ainsi	O
,	O
les	O
compatibles	MISC
PC	MISC
ont	O
été	O
initialement	O
basé	O
sur	O
une	O
architecture	O
16	O
bits	O
(	O
architecture	O
x86	MISC
,	O
1978	O
)	O
qui	O
ne	O
pouvait	O
pas	O
adresser	O
plus	O
de	O
64	O
kibioctets	O
de	O
mémoire	O
sans	O
passer	O
par	O
des	O
complications	O
.	O
Les	O
capacités	O
de	O
traitement	O
32	O
bits	O
furent	O
introduites	O
dans	O
la	O
famille	O
x86	MISC
avec	O
l'	O
Intel	MISC
80386	MISC
(	O
1986	O
)	O
,	O
mais	O
ce	O
n'	O
est	O
qu'	O
avec	O
un	O
système	O
d'	O
exploitation	O
conçu	O
pour	O
tirer	O
parti	O
des	O
capacités	O
32	O
bits	O
(	O
Linux	MISC
,	O
Windows	MISC
NT	MISC
,	O
Windows	MISC
95	MISC
)	O
que	O
les	O
4	O
gibioctets	O
d'	O
adressage	O
peuvent	O
être	O
pleinement	O
exploités	O
.	O
