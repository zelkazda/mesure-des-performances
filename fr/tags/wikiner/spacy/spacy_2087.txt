Créée	O
le	O
11	O
février	O
1961	O
après	O
une	O
rencontre	O
à	O
Madrid	LOCATION
entre	O
Jean-Jacques	PERSON
Susini	PERSON
et	O
Pierre	PERSON
Lagaillarde	PERSON
,	O
elle	O
regroupait	O
les	O
partisans	O
du	O
maintien	O
de	O
l	O
'	O
"	O
Algérie	MISC
française	MISC
"	O
par	O
la	O
lutte	O
armée	O
.	O
Ce	O
sont	O
ces	O
gendarmes	O
qui	O
arrêtent	O
le	O
général	O
Raoul	PERSON
Salan	PERSON
,	O
le	O
20	O
avril	O
1962	O
,	O
suite	O
aux	O
renseignements	O
fournis	O
par	O
la	O
Police	ORGANIZATION
judiciare	O
parisienne	O
.	O
Les	O
barbouzes	O
sont	O
chargées	O
de	O
faire	O
du	O
contre-terrorisme	O
,	O
c'	O
est-à-dire	O
des	O
plasticages	O
,	O
de	O
réaliser	O
des	O
interrogatoires	O
(	O
au	O
cours	O
desquels	O
la	O
torture	O
est	O
utilisée	O
,	O
selon	O
les	O
membres	O
de	O
l'	O
OAS	ORGANIZATION
qui	O
les	O
ont	O
subis	O
)	O
,	O
et	O
de	O
transmettre	O
les	O
renseignements	O
recueillis	O
par	O
le	O
FLN	ORGANIZATION
sur	O
l'	O
OAS	ORGANIZATION
,	O
car	O
les	O
services	O
officiels	O
ne	O
pouvaient	O
,	O
avant	O
le	O
cessez-le-feu	O
,	O
entretenir	O
de	O
relations	O
avec	O
un	O
mouvement	O
interdit	O
.	O
Le	O
Service	ORGANIZATION
d'	ORGANIZATION
action	ORGANIZATION
civique	ORGANIZATION
a	O
participé	O
à	O
la	O
répression	O
de	O
l'	O
OAS	ORGANIZATION
[	O
réf.	O
nécessaire	O
]	O
.	O
L'	O
historien	O
français	O
Rémi	PERSON
Kauffer	PERSON
estime	O
que	O
l'	O
OAS	ORGANIZATION
a	O
assassiné	O
entre	O
1	O
700	O
et	O
2	O
000	O
personnes	O
.	O
L'	O
historien	O
Olivier	PERSON
Dard	PERSON
estime	O
ces	O
bilans	O
très	O
exagérés	O
.	O
En	O
1962	O
,	O
635	O
membres	O
de	O
l'	O
OAS	ORGANIZATION
sont	O
arrêtés	O
.	O
224	O
sont	O
ensuite	O
jugés	O
,	O
dont	O
117	O
acquittés	O
,	O
cinquante-trois	O
condamnés	O
à	O
une	O
peine	O
de	O
prison	O
avec	O
sursis	O
,	O
trente-huit	O
à	O
une	O
peine	O
de	O
prison	O
ferme	O
,	O
trois	O
sont	O
condamnés	O
à	O
mort	O
et	O
exécutés	O
(	O
Roger	PERSON
Degueldre	PERSON
,	O
Claude	PERSON
Piegts	PERSON
et	O
Albert	PERSON
Dovecar	PERSON
)	O
;	O
le	O
lieutenant-colonel	O
Bastien-Thiry	PERSON
est	O
également	O
passé	O
par	O
les	O
armes	O
mais	O
n'	O
était	O
pas	O
membre	O
de	O
l'	O
OAS	ORGANIZATION
.	O
Plusieurs	O
membres	O
de	O
l'	O
OAS	ORGANIZATION
se	O
sont	O
réfugiés	O
à	O
l'	O
étranger	O
,	O
notamment	O
en	O
Espagne	LOCATION
,	O
au	O
Portugal	LOCATION
et	O
en	O
Amérique	LOCATION
du	LOCATION
Sud	LOCATION
.	O
Le	O
général	O
Jouhaud	PERSON
,	O
condamné	O
à	O
perpétuité	O
,	O
est	O
libéré	O
en	O
décembre	O
1967	O
.	O
En	O
1968	O
,	O
des	O
anciens	O
de	O
l'	O
OAS	ORGANIZATION
rencontrent	O
Jacques	PERSON
Foccart	PERSON
pour	O
lui	O
proposer	O
leur	O
ralliement	O
au	O
régime	O
gaulliste	O
contre	O
la	O
"	O
chienlit	O
"	O
et	O
demander	O
l'	O
amnistie	O
des	O
membres	O
de	O
l'	O
organisation	O
encore	O
incarcérés	O
,	O
ce	O
qu'	O
ils	O
obtiendront	O
.	O
L'	O
OAS	ORGANIZATION
avait	O
lancé	O
un	O
appel	O
aux	O
français-musulmans	O
pour	O
combattre	O
auprès	O
d'	O
eux	O
,	O
contre	O
le	O
FLN	ORGANIZATION
.	O
En	O
1962	O
,	O
on	O
dénombre	O
au	O
moins	O
deux	O
musulmans	O
parmi	O
les	O
jeunes	O
dirigeants	O
de	O
l'	O
OAS	ORGANIZATION
.	O
