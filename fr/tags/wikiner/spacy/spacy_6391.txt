Elle	O
fait	O
partie	O
de	O
la	O
communauté	LOCATION
d'	LOCATION
agglomération	LOCATION
de	LOCATION
Lens	LOCATION
--	O
Liévin	LOCATION
(	O
Communaupole	LOCATION
)	O
qui	O
regroupe	O
36	O
communes	O
,	O
soit	O
250000	O
habitants	O
.	O
La	LOCATION
Souchez	LOCATION
est	O
un	O
ruisseau	O
constituant	O
le	O
cours	O
amont	O
de	O
la	O
Deûle	LOCATION
et	O
traversant	O
le	O
bourg	O
.	O
La	O
ville	O
se	O
situe	O
à	O
4	O
km	O
de	O
Liévin	LOCATION
et	O
à	O
13	O
km	O
de	O
la	O
préfecture	O
,	O
Arras	LOCATION
.	O
Le	O
passé	O
de	O
Souchez	LOCATION
remonte	O
à	O
l'	O
époque	O
gallo-romaine	O
avec	O
la	O
découverte	O
de	O
vestiges	O
d'	O
habitations	O
.	O
Il	O
faut	O
donc	O
avancer	O
un	O
peu	O
dans	O
le	O
temps	O
pour	O
retrouver	O
des	O
traces	O
de	O
l'	O
histoire	O
de	O
la	O
commune	O
:	O
vers	O
1213-1214	O
le	O
village	O
fut	O
entièrement	O
détruit	O
par	O
les	O
troupes	O
du	O
conte	O
Ferrand	PERSON
de	PERSON
Flandre	PERSON
,	O
alors	O
en	O
guerre	O
contre	O
Philippe	PERSON
Auguste	PERSON
.	O
À	O
la	O
suite	O
de	O
la	O
guerre	O
,	O
Souchez	LOCATION
fut	O
parrainée	O
par	O
le	O
quartier	O
londonien	O
de	O
Kensington	LOCATION
qui	O
soutint	O
la	O
reconstruction	O
par	O
de	O
nombreux	O
dons	O
.	O
On	O
peut	O
voir	O
à	O
Souchez	LOCATION
différents	O
monuments	O
:	O
L'	O
évolution	O
du	O
nombre	O
d'	O
habitants	O
depuis	O
1793	O
est	O
connue	O
à	O
travers	O
les	O
recensements	O
de	O
la	O
population	O
effectués	O
à	O
Souchez	LOCATION
depuis	O
cette	O
date	O
:	O
