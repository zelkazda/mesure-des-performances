La	O
mythologie	O
romaine	O
est	O
l'	O
ensemble	O
des	O
légendes	O
et	O
des	O
mythes	O
de	O
la	O
Rome	LOCATION
antique	LOCATION
.	O
D'	O
origine	O
indo-européenne	O
,	O
la	O
mythologie	O
romaine	O
a	O
emprunté	O
au	O
fil	O
des	O
siècles	O
des	O
conceptions	O
religieuses	O
et	O
culturelles	O
aux	O
pays	O
qui	O
ont	O
été	O
peu	O
à	O
peu	O
intégrés	O
dans	O
la	O
sphère	O
de	O
Rome	LOCATION
:	O
la	O
Grèce	LOCATION
,	O
l'	O
Égypte	LOCATION
,	O
la	O
Syrie	LOCATION
,	O
etc	O
.	O
La	O
majorité	O
des	O
divinités	O
du	O
panthéon	O
romain	O
vient	O
de	O
la	O
Grèce	MISC
antique	MISC
et	O
a	O
supplanté	O
les	O
divinités	O
locales	O
(	O
ou	O
"	O
indigètes	O
"	O
)	O
,	O
à	O
quelques	O
rares	O
exceptions	O
.	O
Si	O
on	O
considère	O
à	O
tort	O
la	O
mythologie	O
romaine	O
comme	O
négligeable	O
par	O
rapport	O
à	O
la	O
mythologie	O
grecque	O
,	O
c'	O
est	O
parce	O
que	O
les	O
mythes	O
romains	O
portent	O
principalement	O
sur	O
l'	O
histoire	MISC
de	MISC
Rome	MISC
,	O
tandis	O
que	O
les	O
mythes	O
grecs	O
sont	O
axés	O
sur	O
les	O
dieux	O
et	O
les	O
héros	O
.	O
Saturne	LOCATION
,	O
plus	O
tard	O
assimilé	O
à	O
Cronos	PERSON
(	O
à	O
ne	O
pas	O
confondre	O
avec	O
Chronos	PERSON
dieu	O
du	O
temps	O
)	O
,	O
est	O
également	O
honoré	O
durant	O
les	O
Saturnales	MISC
.	O
Il	O
sera	O
plus	O
tard	O
assimilé	O
à	O
Romulus	PERSON
divinisé	O
.	O
À	O
Troie	LOCATION
,	O
ils	O
avaient	O
,	O
semble-t-il	O
,	O
le	O
même	O
rôle	O
que	O
celui	O
qui	O
leur	O
fut	O
dévolu	O
à	O
Rome	LOCATION
.	O
Rome	LOCATION
possède	O
ses	O
propres	O
mythes	O
,	O
souvent	O
liés	O
à	O
sa	O
fondation	O
et	O
à	O
son	O
histoire	O
.	O
Le	O
mythe	O
d'	O
Énée	PERSON
fait	O
partie	O
des	O
légendes	O
de	O
la	O
fondation	LOCATION
de	LOCATION
Rome	LOCATION
.	O
Il	O
décrit	O
le	O
voyage	O
d'	O
Énée	PERSON
depuis	O
sa	O
fuite	O
de	O
Troie	LOCATION
jusqu'	O
à	O
son	O
arrivée	O
dans	O
le	O
Latium	LOCATION
.	O
Cette	O
perspective	O
de	O
propagande	O
laisse	O
penser	O
que	O
Virgile	PERSON
a	O
remodelé	O
la	O
légende	O
afin	O
de	O
satisfaire	O
les	O
demandes	O
d'	O
Auguste	PERSON
,	O
mais	O
l'	O
épopée	O
s'	O
appuie	O
d'	O
abord	O
sur	O
la	O
tradition	O
qui	O
donnait	O
pour	O
ancêtres	O
au	O
peuple	O
romain	O
Énée	PERSON
et	O
les	O
derniers	O
Troyens	LOCATION
.	O
Cette	O
légende	O
,	O
probablement	O
la	O
plus	O
célèbre	O
de	O
la	O
mythologie	O
romaine	O
et	O
narrée	O
de	O
nombreuses	O
fois	O
par	O
les	O
auteurs	O
latins	O
,	O
est	O
à	O
l'	O
origine	O
des	O
institutions	O
romaines	O
:	O
le	O
meurtre	O
de	O
Rémus	PERSON
par	O
Romulus	PERSON
montre	O
la	O
prédominance	O
de	O
la	O
patrie	O
sur	O
les	O
liens	O
du	O
sang	O
,	O
l'	O
enceinte	O
(	O
pomoerium	O
)	O
de	O
Rome	LOCATION
tracée	O
par	O
Romulus	PERSON
,	O
demeurera	O
sacrée	O
(	O
sauf	O
pour	O
les	O
triomphes	O
)	O
.	O
La	O
légende	O
donne	O
également	O
une	O
origine	O
divine	O
à	O
Rome	LOCATION
,	O
Mars	LOCATION
étant	O
le	O
père	O
des	O
jumeaux	O
.	O
Les	O
nombreuses	O
légendes	O
qui	O
entourent	O
l'	O
histoire	MISC
de	MISC
Rome	MISC
consolident	O
de	O
même	O
les	O
institutions	O
romaines	O
.	O
Certaines	O
vantent	O
la	O
virta	O
latine	O
(	O
vertu	O
et	O
courage	O
)	O
,	O
ce	O
sont	O
les	O
exempla	O
et	O
d'	O
autres	O
expliquent	O
la	O
fondation	LOCATION
de	LOCATION
Rome	LOCATION
,	O
ce	O
sont	O
les	O
mythes	O
fondateurs	O
.	O
