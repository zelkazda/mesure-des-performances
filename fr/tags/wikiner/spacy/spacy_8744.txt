Il	O
s'	O
établit	O
à	O
Paris	LOCATION
vers	O
1656	O
,	O
et	O
est	O
nommé	O
organiste	O
titulaire	O
de	O
l'	O
église	LOCATION
Saint-Merry	LOCATION
en	O
1664	O
,	O
poste	O
qu'	O
il	O
occupe	O
jusqu'	O
à	O
sa	O
mort	O
.	O
Sa	O
renommée	O
est	O
grande	O
,	O
en	O
tant	O
qu'	O
interprète	O
,	O
compositeur	O
et	O
professeur	O
(	O
il	O
a	O
pour	O
élèves	O
notamment	O
Geoffroy	LOCATION
,	O
Grigny	LOCATION
,	O
d'	O
Agincourt	LOCATION
,	O
probablement	O
Jullien	PERSON
et	O
d'	O
autres	O
)	O
.	O
Il	O
inaugure	O
ainsi	O
,	O
avec	O
Nivers	PERSON
,	O
le	O
genre	O
de	O
la	O
suite	O
pour	O
orgue	O
.	O
