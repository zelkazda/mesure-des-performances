Premier	O
ministre	O
du	O
6	O
mai	O
2002	O
au	O
31	O
mai	O
2005	O
,	O
il	O
est	O
aujourd'hui	O
sénateur	O
de	O
la	O
Vienne	O
.	O
Il	O
est	O
ensuite	O
diplômé	O
de	O
l'	O
ESCP	ORGANIZATION
Europe	ORGANIZATION
,	O
promotion	O
1972	O
(	O
la	O
même	O
que	O
Michel	PERSON
Barnier	PERSON
)	O
.	O
Conseiller	O
technique	O
de	O
Lionel	PERSON
Stoléru	PERSON
de	O
1976	O
à	O
1981	O
,	O
il	O
est	O
aussi	O
maître	O
de	O
conférences	O
à	O
l'	O
Institut	ORGANIZATION
d'	ORGANIZATION
études	ORGANIZATION
politiques	ORGANIZATION
de	ORGANIZATION
Paris	ORGANIZATION
de	O
1979	O
à	O
1988	O
.	O
Il	O
est	O
ensuite	O
secrétaire	O
général	O
adjoint	O
et	O
porte-parole	O
(	O
1993	O
--	O
1995	O
)	O
,	O
puis	O
secrétaire	O
général	O
de	O
l'	O
Union	ORGANIZATION
pour	ORGANIZATION
la	ORGANIZATION
démocratie	ORGANIZATION
française	ORGANIZATION
(	O
UDF	ORGANIZATION
)	O
.	O
Membre	O
du	O
bureau	O
politique	O
de	O
l'	O
UDF	ORGANIZATION
(	O
1996	O
)	O
,	O
il	O
prend	O
part	O
à	O
la	O
création	O
de	O
Démocratie	ORGANIZATION
libérale	ORGANIZATION
,	O
dont	O
il	O
est	O
membre	O
du	O
bureau	O
politique	O
(	O
1997	O
)	O
,	O
puis	O
vice-président	O
jusqu'	O
à	O
la	O
fusion	O
avec	O
l'	O
UMP	ORGANIZATION
en	O
2002	O
.	O
Au	O
niveau	O
local	O
,	O
il	O
est	O
conseiller	O
municipal	O
d'	O
opposition	O
de	O
Poitiers	LOCATION
de	O
1977	O
à	O
1995	O
,	O
conseiller	O
régional	O
de	O
la	O
région	O
Poitou-Charentes	LOCATION
en	O
1986	O
,	O
puis	O
président	O
du	O
conseil	O
régional	O
de	O
1988	O
à	O
2002	O
,	O
où	O
il	O
succède	O
à	O
Louis	PERSON
Fruchard	PERSON
,	O
son	O
mentor	O
.	O
En	O
1995	O
,	O
il	O
quitte	O
le	O
conseil	O
municipal	O
de	O
Poitiers	LOCATION
pour	O
devenir	O
,	O
jusqu'	O
en	O
2001	O
,	O
adjoint	O
au	O
maire	O
de	O
Chasseneuil-du-Poitou	LOCATION
.	O
Il	O
est	O
député	O
européen	O
de	O
1989	O
à	O
1995	O
,	O
élu	O
sur	O
une	O
liste	O
UDF	ORGANIZATION
--	O
RPR	ORGANIZATION
.	O
En	O
septembre	O
de	O
la	O
même	O
année	O
,	O
il	O
se	O
fait	O
élire	O
sénateur	O
de	O
la	O
Vienne	O
,	O
mais	O
n'	O
exerce	O
pas	O
ce	O
mandat	O
pour	O
rester	O
au	O
gouvernement	O
.	O
Jean-Pierre	PERSON
Raffarin	PERSON
se	O
fait	O
surtout	O
connaître	O
à	O
cette	O
époque	O
pour	O
son	O
action	O
en	O
faveur	O
de	O
la	O
protection	O
des	O
artisans	O
boulangers	O
.	O
Jean-Pierre	PERSON
Raffarin	PERSON
est	O
alors	O
écartelé	O
entre	O
la	O
politique	O
de	O
ce	O
dernier	O
(	O
en	O
particulier	O
sa	O
politique	O
de	O
construction	O
de	O
HLM	O
et	O
de	O
développement	O
des	O
emplois	O
aidés	O
)	O
soutenue	O
par	O
Jacques	PERSON
Chirac	PERSON
et	O
la	O
volonté	O
de	O
Nicolas	PERSON
Sarkozy	PERSON
de	O
gérer	O
les	O
finances	O
"	O
en	O
bon	O
père	O
de	O
famille	O
"	O
,	O
avant	O
que	O
ce	O
dernier	O
ne	O
quitte	O
le	O
gouvernement	O
pour	O
prendre	O
la	O
présidence	O
de	O
l'	O
UMP	ORGANIZATION
.	O
La	O
rumeur	O
dit	O
que	O
Henri	PERSON
Emmanuelli	PERSON
,	O
tenor	PERSON
du	O
"	O
non	O
"	O
,	O
l'	O
a	O
appelé	O
,	O
le	O
soir	O
même	O
du	O
29	O
mai	O
2005	O
,	O
pour	O
le	O
remercier	O
:	O
"	O
sans	O
vous	O
,	O
rien	O
n'	O
eût	O
été	O
possible.	O
"	O
Il	O
est	O
aussitôt	O
remplacé	O
par	O
Dominique	PERSON
de	PERSON
Villepin	PERSON
.	O
Une	O
élection	O
partielle	O
provoquée	O
après	O
son	O
départ	O
du	O
gouvernement	O
par	O
la	O
démission	O
de	O
son	O
suppléant	O
lui	O
permet	O
d'	O
être	O
réélu	O
,	O
le	O
18	O
septembre	O
2005	O
,	O
sénateur	O
(	O
UMP	ORGANIZATION
)	O
de	O
la	O
Vienne	LOCATION
,	O
obtenant	O
au	O
premier	O
tour	O
56,98	O
%	O
des	O
voix	O
des	O
1046	O
grands	O
électeurs	O
.	O
Fin	O
2009	O
,	O
il	O
s'	O
oppose	O
à	O
la	O
suppression	O
de	O
la	O
taxe	O
professionnelle	O
,	O
notamment	O
contre	O
l'	O
avis	O
du	O
président	O
Nicolas	PERSON
Sarkozy	PERSON
,	O
et	O
persiste	O
dans	O
son	O
refus	O
de	O
la	O
voter	O
en	O
l'	O
état	LOCATION
.	O
