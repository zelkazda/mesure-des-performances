Joseph	PERSON
Vendryes	PERSON
(	O
1875	O
--	O
1960	O
)	O
linguiste	O
et	O
celtologue	O
français	O
.	O
Élève	O
d'	O
Antoine	PERSON
Meillet	PERSON
,	O
il	O
a	O
enseigné	O
à	O
l'	O
École	ORGANIZATION
pratique	ORGANIZATION
des	ORGANIZATION
hautes	ORGANIZATION
études	ORGANIZATION
,	O
où	O
il	O
a	O
occupé	O
la	O
chaire	O
de	O
langues	O
et	O
littératures	O
celtiques	O
.	O
Il	O
a	O
également	O
enseigné	O
la	O
linguistique	O
à	O
la	O
faculté	O
des	O
lettres	O
de	O
l'	O
université	ORGANIZATION
de	ORGANIZATION
Paris	ORGANIZATION
à	O
partir	O
de	O
1907	O
ainsi	O
qu'	O
à	O
l'	O
Ecole	ORGANIZATION
normale	ORGANIZATION
supérieure	ORGANIZATION
(	O
1920-1936	O
)	O
Vendryes	PERSON
a	O
notamment	O
développé	O
les	O
notions	O
de	O
linguistique	O
idiosynchronique	O
et	O
d'	O
idiolecte	O
.	O
Il	O
fut	O
membre	O
de	O
l'	O
Académie	ORGANIZATION
des	ORGANIZATION
inscriptions	ORGANIZATION
et	ORGANIZATION
belles-lettres	ORGANIZATION
.	O
Une	O
partie	O
de	O
sa	O
bibliothèque	O
,	O
léguée	O
à	O
la	O
bibliothèque	O
de	O
linguistique	O
de	O
l'	O
université	ORGANIZATION
de	ORGANIZATION
Paris	ORGANIZATION
,	O
se	O
trouve	O
désormais	O
à	O
la	O
Bibliothèque	ORGANIZATION
de	ORGANIZATION
sciences	ORGANIZATION
humaines	ORGANIZATION
et	ORGANIZATION
sociales	ORGANIZATION
Paris	ORGANIZATION
Descartes-CNRS	ORGANIZATION
.	O
