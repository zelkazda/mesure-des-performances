Les	O
îles	O
les	O
plus	O
proches	O
sont	O
l'	O
île	O
Saint-Vincent-et-les	LOCATION
Grenadines	LOCATION
et	O
Sainte-Lucie	LOCATION
,	O
à	O
l'	O
ouest	O
.	O
La	O
superficie	O
totale	O
de	O
la	LOCATION
Barbade	LOCATION
est	O
d'	O
environ	O
430	O
kilomètres	O
carrés	O
(	O
166	O
miles	O
carrés	O
)	O
,	O
et	O
est	O
principalement	O
de	O
faible	O
altitude	O
,	O
avec	O
les	O
pics	O
les	O
plus	O
élevés	O
à	O
l'	O
intérieur	O
du	O
pays	O
.	O
Le	O
point	O
le	O
plus	O
élevé	O
de	O
la	LOCATION
Barbade	LOCATION
est	O
le	O
mont	LOCATION
Hillaby	LOCATION
dans	O
la	O
paroisse	O
de	O
Saint	LOCATION
Andrew	LOCATION
.	O
L'	O
île	O
a	O
un	O
climat	O
tropical	O
,	O
avec	O
des	O
alizés	O
de	O
l'	O
océan	LOCATION
Atlantique	LOCATION
maintenant	O
des	O
températures	O
douces	O
.	O
En	O
2006	O
,	O
l'	O
indice	O
de	O
développement	O
humain	O
de	O
la	LOCATION
Barbade	LOCATION
était	O
le	O
37	O
e	O
plus	O
élevé	O
au	O
monde	O
(	O
0,889	O
)	O
.	O
La	LOCATION
Barbade	LOCATION
est	O
divisée	O
en	O
11	O
paroisses	O
:	O
La	LOCATION
Barbade	LOCATION
est	O
une	O
île	O
relativement	O
plate	O
,	O
se	O
relevant	O
doucement	O
dans	O
la	O
région	O
centrale	O
montagneuse	O
,	O
le	O
point	O
le	O
plus	O
élevé	O
est	O
le	O
mont	LOCATION
Hillaby	LOCATION
à	O
336	O
m	O
.	O
L'	O
île	O
est	O
située	O
sur	O
une	O
position	O
légèrement	O
excentrée	O
dans	O
l'	O
océan	LOCATION
Atlantique	LOCATION
comparée	O
aux	O
autres	O
îles	LOCATION
des	LOCATION
Caraïbes	LOCATION
.	O
La	O
capitale	O
est	O
Bridgetown	LOCATION
.	O
La	LOCATION
Barbade	LOCATION
a	O
pour	O
codes	O
:	O
