Le	O
Grand	LOCATION
Lyon	LOCATION
(	O
anciennement	O
COURLY	MISC
)	O
,	O
est	O
une	O
communauté	O
urbaine	O
française	O
,	O
structure	O
intercommunale	O
regroupant	O
57	O
communes	O
de	O
l'	O
agglomération	O
de	O
Lyon	LOCATION
situées	O
dans	O
le	O
département	LOCATION
du	LOCATION
Rhône	LOCATION
.	O
Elle	O
a	O
été	O
initiée	O
en	O
1966	O
par	O
l'	O
annexion	O
au	O
département	LOCATION
du	LOCATION
Rhône	LOCATION
de	O
certaines	O
communes	O
des	O
départements	O
de	O
l'	O
Ain	LOCATION
et	O
de	O
l'	O
Isère	LOCATION
,	O
et	O
créée	O
le	O
1	O
er	O
janvier	O
1969	O
.	O
Le	O
Grand	LOCATION
Lyon	LOCATION
englobe	O
la	O
plupart	O
des	O
communes	O
de	O
l'	O
agglomération	O
lyonnaise	O
.	O
Cependant	O
,	O
les	O
communes	O
plus	O
éloignées	O
du	O
centre	O
ont	O
formé	O
leur	O
propre	O
structure	O
intercommunale	O
,	O
comme	O
la	O
communauté	LOCATION
de	LOCATION
communes	LOCATION
de	LOCATION
l'	LOCATION
Est	LOCATION
Lyonnais	LOCATION
(	O
29	O
464	O
habitants	O
)	O
et	O
la	O
communauté	LOCATION
de	LOCATION
communes	LOCATION
de	LOCATION
la	LOCATION
Vallée	LOCATION
du	LOCATION
Garon	LOCATION
(	O
28	O
459	O
habitants	O
)	O
.	O
En	O
novembre	O
2005	O
,	O
les	O
communes	O
de	O
Givors	LOCATION
et	O
de	O
Grigny	LOCATION
ont	O
soumis	O
leur	O
candidature	O
à	O
l'	O
adhésion	O
au	O
Grand	LOCATION
Lyon	LOCATION
,	O
après	O
consultation	O
de	O
la	O
population	O
.	O
Il	O
s'	O
agit	O
de	O
la	O
première	O
extension	O
du	O
Grand	LOCATION
Lyon	LOCATION
depuis	O
1969	O
.	O
Les	O
deux	O
communes	O
bénéficient	O
des	O
avantages	O
de	O
l'	O
agglomération	O
(	O
rattachement	O
au	O
syndicat	O
des	O
transports	O
TCL	O
,	O
voirie	O
,	O
ordures	O
ménagères	O
...	O
)	O
tandis	O
que	O
le	O
Grand	LOCATION
Lyon	LOCATION
s'	O
agrandit	O
d'	O
une	O
vingtaine	O
de	O
km²	O
.	O
Cependant	O
,	O
il	O
n'	O
y	O
a	O
pas	O
de	O
continuité	O
territoriale	O
,	O
la	O
commune	O
de	O
Millery	LOCATION
séparant	O
les	O
communes	O
de	O
Vernaison	LOCATION
et	O
Grigny	LOCATION
.	O
Depuis	O
1990	O
,	O
et	O
l'	O
élection	O
de	O
Michel	PERSON
Noir	PERSON
à	O
la	O
présidence	O
,	O
cet	O
acronyme	O
a	O
été	O
abandonné	O
officiellement	O
au	O
profit	O
de	O
"	O
Grand	LOCATION
Lyon	LOCATION
"	O
,	O
même	O
si	O
le	O
terme	O
"	O
COURLY	MISC
"	O
reste	O
d'	O
un	O
usage	O
courant	O
.	O
Le	O
budget	O
du	O
Grand	LOCATION
Lyon	LOCATION
s'	O
élève	O
à	O
1,6	O
milliards	O
d'	O
euros	O
en	O
2009	O
et	O
provient	O
de	O
(	O
s	O
)	O
:	O
Depuis	O
le	O
1	O
er	O
janvier	O
1969	O
,	O
date	O
de	O
la	O
création	O
effective	O
de	O
la	O
communauté	LOCATION
urbaine	LOCATION
de	LOCATION
Lyon	LOCATION
,	O
celle-ci	O
exerce	O
ses	O
différentes	O
compétences	O
avec	O
la	O
volonté	O
de	O
développer	O
la	O
solidarité	O
entre	O
les	O
communes	O
et	O
de	O
mettre	O
en	O
commun	O
moyens	O
et	O
compétences	O
.	O
Il	O
est	O
composé	O
de	O
155	O
membres	O
,	O
désignés	O
pour	O
6	O
ans	O
,	O
au	O
sein	O
des	O
57	O
conseils	O
municipaux	O
des	O
communes	O
composant	O
le	O
Grand	LOCATION
Lyon	LOCATION
.	O
La	O
communauté	LOCATION
urbaine	LOCATION
de	LOCATION
Lyon	LOCATION
regroupe	O
75	O
%	O
des	O
habitants	O
du	O
département	LOCATION
du	LOCATION
Rhône	LOCATION
.	O
Dans	O
le	O
cadre	O
de	O
ses	O
activités	O
extérieures	O
,	O
le	O
Grand	LOCATION
Lyon	LOCATION
s'	O
associe	O
au	O
protocole	O
de	O
coopération	O
existant	O
entre	O
Lyon	LOCATION
et	O
Ouagadougou	LOCATION
au	O
Burkina	LOCATION
Faso	LOCATION
.	O
