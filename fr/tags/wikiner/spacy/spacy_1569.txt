Serdar	PERSON
Argic	PERSON
est	O
le	O
pseudonyme	O
d'	O
une	O
personne	O
qui	O
fut	O
à	O
l'	O
origine	O
des	O
premiers	O
grands	O
incidents	O
de	O
pourriel	O
sur	O
Usenet	MISC
,	O
en	O
inondant	O
les	O
forums	O
Usenet	MISC
de	O
réponses	O
aux	O
messages	O
qui	O
parlaient	O
de	O
la	O
Turquie	LOCATION
.	O
Malgré	O
de	O
forts	O
soupçons	O
,	O
l'	O
identité	O
réelle	O
derrière	O
Serdar	PERSON
Argic	PERSON
ne	O
fut	O
jamais	O
révélée	O
.	O
Pendant	O
une	O
période	O
de	O
deux	O
mois	O
,	O
au	O
début	O
de	O
1994	O
,	O
ce	O
"	O
Serdar	PERSON
Argic	PERSON
"	O
postait	O
des	O
messages	O
dans	O
tout	O
groupe	O
de	O
discussion	O
contenant	O
une	O
conversation	O
impliquant	O
la	O
Turquie	LOCATION
.	O
Néanmoins	O
,	O
le	O
compte	O
Serdar	PERSON
Argic	PERSON
disparut	O
à	O
la	O
mi-	O
1994	O
après	O
qu'	O
un	O
newsgroup	O
fut	O
créé	O
,	O
servant	O
de	O
filtre	O
et	O
générant	O
des	O
suppressions	O
de	O
messages	O
sur	O
les	O
interventions	O
venant	O
de	O
Turquie	LOCATION
.	O
C'	O
était	O
un	O
étudiant	O
turc	O
qui	O
avait	O
enregistré	O
le	O
domaine	O
anatolia.org	O
,	O
origine	O
des	O
messages	O
Usenet	MISC
.	O
Les	O
messages	O
de	O
Serdar	PERSON
Argic	PERSON
ont	O
été	O
globalement	O
jugés	O
comme	O
une	O
tentative	O
assez	O
grossière	O
de	O
faire	O
du	O
négationnisme	O
.	O
2000	O
)	O
,	O
rapporte	O
que	O
"	O
Serdar	PERSON
Argic	PERSON
"	O
fut	O
considéré	O
comme	O
"	O
la	O
sixième	O
'	O
personne	O
'	O
la	O
plus	O
malveillante	O
d'	O
Usenet	MISC
"	O
.	O
