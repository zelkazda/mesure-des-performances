Elle	O
est	O
située	O
à	O
environ	O
31	O
kilomètres	O
au	O
sud-est	O
de	O
l'	O
Inde	LOCATION
et	O
compte	O
une	O
population	O
d'	O
environ	O
vingt	O
millions	O
de	O
personnes	O
d'	O
origines	O
,	O
religions	O
,	O
langues	O
et	O
coutumes	O
différentes	O
.	O
Elle	O
joua	O
un	O
rôle	O
important	O
dans	O
les	O
échanges	O
commerciaux	O
maritimes	O
pendant	O
l'	O
Antiquité	MISC
.	O
Une	O
civilisation	O
prospère	O
se	O
développa	O
dans	O
des	O
villes	O
comme	O
Anurâdhapura	LOCATION
et	O
Polonnâruvâ	LOCATION
(	O
1070	O
--	O
1200	O
)	O
.	O
Par	O
la	O
convention	O
de	O
Kandy	LOCATION
,	O
les	O
Anglais	LOCATION
prirent	O
le	O
contrôle	O
de	O
l'	O
île	O
en	O
1815	O
.	O
L'	O
administration	O
anglaise	O
introduisit	O
la	O
culture	O
du	O
thé	O
à	O
Ceylan	LOCATION
,	O
ainsi	O
qu'	O
un	O
réseau	O
ferroviaire	O
.	O
Les	O
Tamouls	MISC
,	O
surtout	O
le	O
parti	O
souverainiste	O
tamoul	O
,	O
utilisèrent	O
le	O
sentiment	O
nationaliste	O
de	O
leur	O
communauté	O
,	O
pour	O
organiser	O
des	O
manifestations	O
contre	O
l'	O
usage	O
d'	O
une	O
seule	O
langue	O
officielle	O
.	O
Une	O
guerre	O
civile	O
opposant	O
le	O
gouvernement	O
central	O
et	O
l'	O
organisation	O
des	O
Tamouls	MISC
Tigres	ORGANIZATION
de	ORGANIZATION
libération	ORGANIZATION
de	ORGANIZATION
l'	ORGANIZATION
Eelam	ORGANIZATION
tamoul	ORGANIZATION
prit	O
de	O
l'	O
ampleur	O
à	O
compter	O
de	O
1983	O
.	O
En	O
effet	O
,	O
la	O
mort	O
le	O
18	O
mai	O
2009	O
de	O
Velupillai	PERSON
Prabhakaran	PERSON
,	O
a	O
marqué	O
la	O
fin	O
de	O
la	O
guerre	O
que	O
Colombo	LOCATION
menait	O
contre	O
les	O
Tigres	ORGANIZATION
tamouls	ORGANIZATION
depuis	O
près	O
de	O
trente	O
ans	O
.	O
Le	O
Sri	LOCATION
Lanka	LOCATION
est	O
une	O
île	O
située	O
dans	O
l'	O
océan	LOCATION
Indien	LOCATION
,	O
à	O
50	O
km	O
de	O
l'	O
Inde	LOCATION
,	O
les	O
deux	O
pays	O
étant	O
séparés	O
par	O
le	O
détroit	LOCATION
de	LOCATION
Palk	LOCATION
,	O
mais	O
quasiment	O
reliés	O
par	O
le	O
pont	LOCATION
d'	LOCATION
Adam	LOCATION
.	O
Les	O
principales	O
villes	O
sont	O
Colombo	LOCATION
(	O
capitale	O
économique	O
,	O
690000	O
habitants	O
en	O
2003	O
)	O
,	O
Kandy	LOCATION
et	O
Galle	LOCATION
.	O
La	O
ville	O
la	O
plus	O
septentrionale	O
du	O
pays	O
,	O
Jaffna	LOCATION
(	O
129000	O
hab.	O
)	O
est	O
aujourd'hui	O
dans	O
la	O
zone	O
contestée	O
entre	O
les	O
militants	O
tamouls	O
et	O
le	O
gouvernement	O
.	O
La	O
capitale	O
politique	O
est	O
Sri	LOCATION
Jayawardenapura	LOCATION
(	O
Kotte	LOCATION
)	O
,	O
située	O
à	O
15	O
km	O
au	O
sud-est	O
de	O
Colombo	LOCATION
.	O
Selon	O
les	O
considérations	O
politiques	O
actuelles	O
,	O
le	O
Sri	LOCATION
Lanka	LOCATION
peut	O
être	O
considéré	O
comme	O
ayant	O
huit	O
ou	O
neuf	O
provinces	O
,	O
elle-mêmes	O
divisées	O
en	O
25	O
districts	O
.	O
En	O
2003	O
,	O
la	O
population	O
du	O
Sri	LOCATION
Lanka	LOCATION
était	O
de	O
20,4	O
millions	O
.	O
Le	O
Sri	LOCATION
Lanka	LOCATION
a	O
été	O
,	O
pendant	O
des	O
décennies	O
,	O
un	O
des	O
pays	O
avec	O
le	O
plus	O
haut	O
taux	O
de	O
suicides	O
au	O
monde	O
.	O
Le	O
Sri	LOCATION
Lanka	LOCATION
a	O
pour	O
codes	O
:	O
