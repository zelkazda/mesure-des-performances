Membre	O
du	O
Parlement	ORGANIZATION
norvégien	ORGANIZATION
(	O
le	O
Storting	MISC
)	O
dès	O
1935	O
,	O
il	O
dirigea	O
divers	O
ministères	O
de	O
1936	O
jusqu'	O
à	O
l'	O
invasion	O
de	O
la	O
Norvège	LOCATION
par	O
les	O
Allemands	LOCATION
.	O
Le	O
1	O
er	O
novembre	O
1950	O
,	O
l'	O
Assemblée	ORGANIZATION
générale	ORGANIZATION
décida	O
de	O
prolonger	O
de	O
3	O
ans	O
le	O
mandat	O
de	O
Trygve	PERSON
Lie	PERSON
.	O
C'	O
est	O
le	O
10	O
avril	O
1953	O
que	O
le	O
diplomate	O
suédois	O
Dag	PERSON
Hammarskjöld	PERSON
prit	O
sa	O
succession	O
.	O
Rentré	MISC
en	MISC
Norvège	MISC
,	O
Trygve	PERSON
Lie	PERSON
y	O
occupa	O
différents	O
postes	O
ministériels	O
et	O
mena	O
plusieurs	O
missions	O
diplomatiques	O
.	O
