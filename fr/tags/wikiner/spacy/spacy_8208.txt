Avec	O
ses	O
quatre	O
épouses	O
,	O
il	O
a	O
onze	O
enfants	O
,	O
dont	O
Hétep-Hérès	PERSON
II	PERSON
qui	O
épouse	O
ses	O
frères	O
Kaouab	PERSON
I	PERSON
er	PERSON
et	O
Djédefrê	PERSON
(	O
le	O
roi	O
suivant	O
)	O
.	O
En	O
dehors	O
des	O
successeurs	O
de	O
Khoufou	PERSON
sur	O
le	O
trône	O
d'	O
Horus	PERSON
qui	O
établirent	O
leur	O
propre	O
monument	O
funéraire	O
,	O
presque	O
tous	O
les	O
membres	O
de	O
cette	O
famille	O
royale	O
se	O
firent	O
construire	O
leur	O
tombeau	O
au	O
voisinage	O
immédiat	O
de	O
la	O
grande	O
pyramide	LOCATION
de	LOCATION
Khéops	LOCATION
à	O
Gizeh	LOCATION
.	O
Il	O
est	O
le	O
fils	O
du	O
roi	O
Snéfrou	PERSON
et	O
de	O
la	O
reine	O
Hétep-Hérès	PERSON
I	PERSON
re	PERSON
,	O
et	O
est	O
considéré	O
par	O
certains	O
comme	O
l'	O
un	O
des	O
plus	O
grands	O
de	O
l'	O
histoire	O
de	O
l'	O
Égypte	LOCATION
antique	LOCATION
.	O
Selon	O
Hérodote	PERSON
et	O
les	O
contes	O
du	O
papyrus	MISC
Westcar	MISC
,	O
à	O
l'	O
inverse	O
de	O
son	O
père	O
,	O
le	O
pharaon	O
Snéfrou	PERSON
,	O
Khéops	PERSON
était	O
considéré	O
comme	O
un	O
pharaon	O
cruel	O
et	O
injuste	O
envers	O
son	O
peuple	O
.	O
Or	O
,	O
de	O
récentes	O
découvertes	O
,	O
suite	O
aux	O
fouilles	O
menées	O
par	O
Mark	PERSON
Lehner	PERSON
ont	O
révélé	O
une	O
ville	O
des	O
artisans	O
et	O
ouvriers	O
à	O
Gizeh	LOCATION
.	O
Khoufou	PERSON
construisit	O
également	O
des	O
temples	O
,	O
en	O
particulier	O
il	O
entama	O
la	O
construction	O
du	O
temple	O
d'	O
Hathor	PERSON
à	O
Dendérah	LOCATION
et	O
on	O
a	O
retrouvé	O
dans	O
les	O
fondations	O
du	O
temple	O
de	O
Bastet	PERSON
à	O
Bubastis	LOCATION
des	O
éléments	O
d'	O
un	O
monument	O
à	O
son	O
nom	O
.	O
