Vârânasî	LOCATION
(	O
prononcer	O
"	O
varanassi	O
"	O
.	O
Elle	O
est	O
située	O
tout	O
entière	O
sur	O
la	O
rive	O
gauche	O
du	O
Gange	LOCATION
,	O
face	O
au	O
soleil	LOCATION
levant	O
,	O
l'	O
autre	O
rive	O
étant	O
dénuée	O
de	O
toute	O
construction	O
.	O
La	O
ville	O
,	O
dédiée	O
à	O
Shiva	PERSON
,	O
est	O
très	O
fréquentée	O
par	O
les	O
sâdhus	O
(	O
ou	O
ermites	O
)	O
et	O
les	O
pèlerins	O
de	O
tout	O
le	O
pays	O
.	O
La	O
ville	O
est	O
citée	O
dans	O
les	O
épopées	O
hindoues	O
du	O
Mahâbhârata	MISC
et	O
du	O
Râmâyana	MISC
.	O
Le	O
poète	O
et	O
réformateur	O
religieux	O
Kabîr	PERSON
et	O
le	O
poète	O
Tulsî	PERSON
Dâs	O
y	O
passèrent	O
la	O
majeure	O
partie	O
de	O
leur	O
vie	O
.	O
La	O
ville	O
de	O
Vârânasî	LOCATION
est	O
surtout	O
célèbre	O
pour	O
ses	O
ghâts	O
,	O
berges	O
recouvertes	O
de	O
marches	O
de	O
pierres	O
qui	O
permettent	O
aux	O
dévots	O
hindous	O
de	O
descendre	O
au	O
fleuve	O
pour	O
y	O
pratiquer	O
ablutions	O
et	O
pûjâs	ORGANIZATION
.	O
Le	O
bain	O
dans	O
le	O
Gange	LOCATION
est	O
censé	O
laver	O
de	O
tous	O
les	O
péchés	O
.	O
