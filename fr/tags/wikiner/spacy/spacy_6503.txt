Le	O
philosophe	O
al-Farabi	PERSON
(	O
mort	O
en	O
950	O
)	O
,	O
le	O
second	O
maître	O
(	O
en	O
référence	O
au	O
premier	O
maître	O
,	O
Aristote	PERSON
)	O
,	O
tient	O
une	O
place	O
prépondérante	O
dans	O
cette	O
dynamique	O
.	O
Avicenne	PERSON
se	O
serait	O
plus	O
tard	O
converti	O
au	O
chiisme	O
duodécimain	O
.	O
Il	O
retient	O
de	O
mémoire	O
l'	O
intégralité	O
du	O
Coran	MISC
.	O
Il	O
étudia	O
à	O
Boukhara	LOCATION
,	O
s'	O
intéressant	O
à	O
toutes	O
les	O
sciences	O
,	O
et	O
surtout	O
à	O
la	O
médecine	O
.	O
Tout	O
alors	O
s'	O
enchaîne	O
:	O
ayant	O
guéri	O
le	O
prince	O
samanide	O
de	O
Boukhara	LOCATION
,	O
Nuh	PERSON
ibn	PERSON
Mansûr	PERSON
,	O
d'	O
une	O
grave	O
maladie	O
,	O
il	O
est	O
autorisé	O
à	O
consulter	O
la	O
vaste	O
bibliothèque	O
du	O
palais	O
.	O
Il	O
voyage	O
d'	O
abord	O
dans	O
le	O
Khârezm	MISC
,	O
principauté	O
qui	O
fut	O
indépendante	O
(	O
de	O
994	O
à	O
1231	O
)	O
au	O
sud	O
de	O
la	O
mer	LOCATION
d'	LOCATION
Aral	LOCATION
,	O
sur	O
les	O
deux	O
rives	O
du	O
Djihoun	LOCATION
(	O
Amou-daria	LOCATION
)	O
,	O
entre	O
Boukhara	LOCATION
et	O
la	O
mer	LOCATION
Caspienne	LOCATION
.	O
Il	O
jouissait	O
d'	O
une	O
telle	O
réputation	O
que	O
plusieurs	O
princes	O
de	O
l'	O
Asie	O
l'	O
appelèrent	O
à	O
leur	O
cour	O
:	O
le	O
roi	O
de	O
Perse	LOCATION
l'	O
employa	O
à	O
la	O
fois	O
comme	O
vizir	O
et	O
comme	O
médecin	O
.	O
Il	O
cultiva	O
aussi	O
avec	O
succès	O
la	O
philosophie	O
,	O
et	O
fut	O
l'	O
un	O
des	O
premiers	O
à	O
étudier	O
et	O
à	O
faire	O
connaître	O
Aristote	PERSON
.	O
Avicenne	PERSON
tenta	O
de	O
se	O
soigner	O
de	O
lui-même	O
,	O
mais	O
son	O
remède	O
lui	O
fut	O
fatal	O
.	O
La	O
religion	O
de	O
la	O
mère	O
d'	O
Avicenne	PERSON
n'	O
est	O
connue	O
que	O
par	O
des	O
sources	O
secondaires	O
.	O
Si	MISC
l'	O
on	O
peut	O
supposer	O
en	O
première	O
approche	O
qu'	O
elle	O
est	O
musulmane	O
,	O
certaines	O
sources	O
indiquent	O
qu'	O
elle	O
était	O
juive	O
:	O
c'	O
est	O
le	O
cas	O
notamment	O
du	O
roman	O
Avicenne	PERSON
de	O
Gilbert	PERSON
Sinoué	PERSON
.	O
Cependant	O
une	O
autre	O
source	O
indiquerait	O
que	O
le	O
sultan	O
Mahmûd	PERSON
de	PERSON
Ghaznî	PERSON
aurait	O
répandu	O
cette	O
information	O
afin	O
de	O
"	O
calomnier	O
"	O
le	O
philosophe	O
.	O
Cette	O
œuvre	O
disparut	O
lors	O
du	O
sac	MISC
d'	MISC
Ispahan	MISC
(	O
1034	O
)	O
,	O
et	O
il	O
n'	O
en	O
subsiste	O
que	O
quelques	O
fragments	O
.	O
Avicenne	PERSON
,	O
fin	O
lettré	O
,	O
fut	O
le	O
traducteur	O
des	O
œuvres	O
d'	O
Hippocrate	PERSON
et	O
de	O
Galien	PERSON
,	O
et	O
porta	O
un	O
soin	O
particulier	O
à	O
l'	O
étude	O
d'	O
Aristote	PERSON
.	O
Avicenne	PERSON
était	O
proche	O
du	O
chiisme	O
ismaélien	O
,	O
le	O
courant	O
auquel	O
appartenaient	O
son	O
père	O
et	O
son	O
frère	O
;	O
ainsi	O
son	O
autobiographie	O
rapporte-t-elle	O
leurs	O
efforts	O
pour	O
entraîner	O
son	O
adhésion	O
à	O
la	O
dawat	O
ismaélienne	O
.	O
Toutefois	O
,	O
Avicenne	PERSON
appartenait	O
au	O
chiisme	O
duodécimain	O
.	O
C'	O
est	O
le	O
développement	O
de	O
la	O
science	O
européenne	O
qui	O
provoquera	O
son	O
obsolescence	O
,	O
par	O
exemple	O
la	O
description	O
de	O
la	O
circulation	O
sanguine	O
par	O
William	PERSON
Harvey	PERSON
en	O
1628	O
.	O
Néanmoins	O
cet	O
ouvrage	O
marqua	O
longuement	O
l'	O
étude	O
de	O
la	O
médecine	O
,	O
et	O
même	O
en	O
1909	O
,	O
un	O
cours	O
de	O
la	O
médecine	O
d'	O
Avicenne	PERSON
fut	O
donné	O
à	O
Bruxelles	LOCATION
.	O
Avicenne	PERSON
se	O
démarque	O
dans	O
les	O
domaines	O
de	O
l'	O
ophtalmologie	O
,	O
de	O
la	O
gynéco-obstétrique	O
et	O
de	O
la	O
psychologie	O
.	O
Mais	O
avant	O
tout	O
,	O
Avicenne	PERSON
s'	O
intéresse	O
aux	O
moyens	O
de	O
conserver	O
la	O
santé	O
.	O
Sa	O
doctrine	O
philosophique	O
,	O
en	O
particulier	O
sa	O
métaphysique	O
,	O
se	O
base	O
sur	O
celle	O
d'	O
Aristote	PERSON
et	O
sur	O
les	O
travaux	O
d'	O
Al-Farabi	LOCATION
.	O
La	O
philosophie	O
islamique	O
,	O
imprégnée	O
de	O
théologie	O
,	O
concevait	O
plus	O
clairement	O
qu'	O
Aristote	PERSON
la	O
distinction	O
entre	O
essence	O
et	O
existence	O
:	O
alors	O
que	O
l'	O
existence	O
est	O
le	O
domaine	O
du	O
contingent	O
,	O
de	O
l'	O
accidentel	O
,	O
l'	O
essence	O
est	O
,	O
par	O
définition	O
,	O
ce	O
qui	O
perdure	O
dans	O
l'	O
être	O
au	O
travers	O
de	O
ses	O
accidents	O
.	O
L'	O
essence	O
,	O
pour	O
Avicenne	PERSON
,	O
est	O
non-contingente	O
.	O
Pour	O
Avicenne	PERSON
,	O
l'	O
intellect	O
humain	O
n'	O
est	O
pas	O
forgé	O
pour	O
l'	O
abstraction	O
des	O
formes	O
et	O
des	O
idées	O
.	O
Pour	O
les	O
néo-platoniciens	O
,	O
dont	O
Avicenne	PERSON
fait	O
partie	O
,	O
l'	O
immortalité	O
de	O
l'	O
âme	O
est	O
une	O
conséquence	O
de	O
sa	O
nature	O
,	O
et	O
pas	O
une	O
finalité	O
.	O
Henry	PERSON
Corbin	PERSON
pense	O
que	O
ces	O
œuvres	O
sont	O
le	O
point	O
de	O
départ	O
du	O
projet	O
de	O
philosophie	O
orientale	O
que	O
Sohrawardi	PERSON
mène	O
plus	O
tard	O
à	O
terme	O
.	O
Cette	O
conception	O
est	O
déjà	O
explicite	O
chez	O
Avicenne	PERSON
,	O
et	O
le	O
sera	O
d'	O
autant	O
plus	O
chez	O
ses	O
commentateurs	O
et	O
critiques	O
,	O
comme	O
Sohrawardi	PERSON
.	O
L'	O
influence	O
d'	O
Avicenne	PERSON
est	O
double	O
:	O
Pour	O
Avicenne	PERSON
"	O
l'	O
intellect	O
humain	O
n'	O
a	O
ni	O
le	O
rôle	O
ni	O
le	O
pouvoir	O
d'	O
abstraire	O
l'	O
intelligible	O
du	O
sensible	O
.	O
Pour	O
sa	O
part	O
,	O
Averroès	PERSON
va	O
dégager	O
l'	O
aristotélisme	O
des	O
ajouts	O
platoniciens	O
qui	O
s'	O
étaient	O
greffés	O
sur	O
lui	O
:	O
point	O
d'	O
émanatisme	O
chez	O
lui	O
.	O
Avicenne	PERSON
y	O
explique	O
que	O
les	O
métaux	O
"	O
résultent	O
de	O
l'	O
union	ORGANIZATION
du	ORGANIZATION
mercure	ORGANIZATION
avec	O
une	O
terre	O
sulfureuse	O
"	O
.	O
Avicenne	PERSON
nie	O
la	O
possibilité	O
d'	O
une	O
transmutation	O
chimique	O
des	O
métaux	O
:	O
"	O
Quant	O
à	O
ce	O
que	O
prétendent	O
les	O
alchimistes	O
,	O
il	O
faut	O
savoir	O
qu'	O
il	O
n'	O
est	O
pas	O
en	O
leur	O
pouvoir	O
de	O
transformer	O
véritablement	O
les	O
espèces	O
les	O
unes	O
en	O
les	O
autres	O
(	O
sciant	O
artifices	O
alchemiae	O
species	O
metallorum	O
transmutari	O
non	O
posse	O
)	O
;	O
mais	O
il	O
est	O
en	O
leur	O
pouvoir	O
de	O
faire	O
de	O
belles	O
imitations	O
,	O
jusqu'	O
à	O
teindre	O
le	O
rouge	O
en	O
un	O
blanc	O
qui	O
le	O
rende	O
tout	O
à	O
fait	O
semblable	O
à	O
l'	O
argent	O
ou	O
en	O
un	O
jaune	O
qui	O
le	O
rende	O
tout	O
à	O
fait	O
semblable	O
à	O
l'	O
or	O
"	O
.	O
Pour	O
Avicenne	PERSON
,	O
les	O
métaux	O
"	O
résultent	O
de	O
l'	O
union	ORGANIZATION
du	ORGANIZATION
mercure	ORGANIZATION
avec	O
une	O
terre	O
sulfureuse	O
"	O
:	O
c'	O
est	O
la	O
théorie	O
du	O
mercure/soufre	O
.	O
