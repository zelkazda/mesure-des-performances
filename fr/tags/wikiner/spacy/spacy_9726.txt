Après	O
avoir	O
soumis	O
un	O
certain	O
nombre	O
d'	O
entre	O
eux	O
,	O
il	O
vainc	O
Pûru	MISC
et	O
son	O
armée	O
d'	O
éléphants	O
de	O
guerre	O
à	O
la	O
terrible	O
bataille	O
de	O
l'	O
Hydaspe	LOCATION
(	O
326	O
)	O
.	O
Il	O
laisse	O
néanmoins	O
Pûru	LOCATION
en	O
place	O
,	O
conquis	O
par	O
le	O
courage	O
et	O
la	O
noblesse	O
de	O
celui-ci	O
,	O
avec	O
un	O
territoire	O
plus	O
vaste	O
qu'	O
il	O
possédait	O
auparavant	O
,	O
continuant	O
ainsi	O
sa	O
politique	O
d'	O
intégration	O
des	O
chefs	O
locaux	O
.	O
Alexandre	PERSON
comprend	O
également	O
qu'	O
il	O
doit	O
s'	O
appuyer	O
sur	O
des	O
alliés	O
fidèles	O
dans	O
une	O
région	O
aux	O
tribus	O
turbulentes	O
.	O
Pûru	LOCATION
a	O
un	O
cousin	O
homonyme	O
qu'	O
Alexandre	PERSON
a	O
soumis	O
à	O
sa	O
suite	O
.	O
