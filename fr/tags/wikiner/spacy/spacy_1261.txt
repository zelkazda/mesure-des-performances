Le	O
scribe	O
(	O
du	O
latin	O
scriba	O
,	O
de	O
scribere	O
,	O
écrire	O
)	O
désigne	O
dans	O
l'	O
Égypte	LOCATION
antique	LOCATION
un	O
fonctionnaire	O
lettré	O
,	O
éduqué	O
dans	O
l'	O
art	O
de	O
l'	O
écriture	O
et	O
de	O
l'	O
arithmétique	O
.	O
celle	O
qui	O
est	O
un	O
scribe	O
)	O
,	O
la	O
profession	O
de	O
scribe	O
passa	O
sous	O
la	O
protection	O
du	O
dieu	O
Thot	PERSON
,	O
au	O
cours	O
des	O
dernières	O
dynasties	O
.	O
Le	O
scribe	O
maîtrise	O
les	O
différentes	O
formes	O
de	O
caractères	O
écrits	O
:	O
écriture	O
hiéroglyphique	O
,	O
à	O
base	O
de	O
symboles	O
,	O
écriture	O
hiératique	O
,	O
à	O
forme	O
cursive	O
et	O
logographique	O
,	O
écriture	O
démotique	O
,	O
de	O
type	O
logo-syllabique	O
et	O
ancêtre	O
du	O
copte	O
(	O
le	O
hiéroglyphique	O
,	O
le	O
démotique	O
et	O
le	O
grec	O
ancien	O
sont	O
les	O
trois	O
langues	O
de	O
la	O
fameuse	O
pierre	PERSON
de	PERSON
Rosette	PERSON
)	O
.	O
D'	O
essence	O
divine	O
,	O
le	O
souverain	O
ne	O
fait	O
que	O
se	O
conformer	O
au	O
dieu	O
Thot	PERSON
,	O
créateur	O
des	O
langues	O
et	O
de	O
l'	O
écriture	O
,	O
scribe	O
et	O
vizir	O
des	O
dieux	O
.	O
Selon	O
la	O
légende	O
,	O
celui	O
capable	O
de	O
déchiffrer	O
les	O
formules	O
magiques	O
du	O
Livre	MISC
de	MISC
Thot	MISC
peut	O
espérer	O
surpasser	O
les	O
dieux	O
.	O
Des	O
chefs	O
d'	O
œuvre	O
de	O
l'	O
art	O
égyptien	O
que	O
l'	O
on	O
retrouve	O
au	O
musée	LOCATION
du	LOCATION
Caire	LOCATION
,	O
au	O
musée	LOCATION
du	LOCATION
Louvre	LOCATION
ou	O
au	O
Neues	LOCATION
Museum	LOCATION
,	O
nous	O
ont	O
été	O
légués	O
,	O
montrant	O
l'	O
importance	O
historique	O
et	O
symbolique	O
des	O
scribes	O
dans	O
l'	O
Égypte	LOCATION
pharaonique	LOCATION
.	O
Le	O
scribe	O
accroupi	O
fut	O
découvert	O
à	O
Saqqarah	LOCATION
en	O
1850	O
.	O
