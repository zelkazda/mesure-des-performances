Miami	LOCATION
est	O
une	O
ville	O
des	O
États-Unis	LOCATION
,	O
centre	O
financier	O
et	O
culturel	O
de	O
niveau	O
international	O
,	O
située	O
au	O
sud-est	O
de	O
la	LOCATION
Floride	LOCATION
.	O
La	O
population	O
du	O
chef-lieu	O
du	O
comté	LOCATION
de	LOCATION
Miami-Dade	LOCATION
est	O
estimée	O
à	O
404048	O
habitants	O
en	O
2006	O
,	O
ce	O
qui	O
fait	O
de	O
Miami	LOCATION
la	O
seconde	O
municipalité	O
de	O
Floride	LOCATION
derrière	O
Jacksonville	LOCATION
et	O
la	O
45	O
e	O
ville	O
des	O
États-Unis	LOCATION
.	O
Ville	LOCATION
de	O
loisirs	O
et	O
de	O
distractions	O
,	O
pour	O
ne	O
pas	O
dire	O
de	O
plaisirs	O
,	O
ultime	O
station	O
balnéaire	O
renommée	O
d'	O
un	O
chapelet	O
de	O
stations	O
prestigieuses	O
depuis	O
Key	LOCATION
West	LOCATION
,	O
Miami	LOCATION
est	O
le	O
premier	O
pôle	O
urbain	O
d'	O
une	O
vaste	O
agglomération	O
de	O
5422200	O
habitants	O
.	O
Le	O
grand	O
Miami	LOCATION
englobe	O
27	O
municipalités	O
qui	O
lui	O
prêtent	O
une	O
allure	O
à	O
la	O
fois	O
urbaine	O
et	O
rurale	O
.	O
La	O
ville	O
même	O
de	O
Miami	LOCATION
,	O
par	O
opposition	O
à	O
son	O
agglomération	O
,	O
est	O
non	O
seulement	O
formée	O
du	O
centre	O
ville	O
(	O
3,8	O
km²	O
)	O
,	O
à	O
l'	O
embouchure	O
de	O
la	O
rivière	O
,	O
mais	O
aussi	O
de	O
plusieurs	O
communautés	O
comme	O
Coconut	LOCATION
Grove	LOCATION
et	O
Coral	LOCATION
Gables	LOCATION
qui	O
,	O
malgré	O
leur	O
annexion	O
,	O
ont	O
su	O
conserver	O
leur	O
individualité	O
.	O
Le	O
centre-ville	O
de	O
Miami	LOCATION
vit	O
depuis	O
quelques	O
années	O
un	O
véritable	O
renouvellement	O
urbain	O
.	O
Le	O
nom	O
de	O
"	O
Miami	LOCATION
"	O
vient	O
d'	O
un	O
mot	O
indien	O
qui	O
signifie	O
"	O
eau	O
douce	O
"	O
.	O
Ils	O
visitent	O
le	O
village	O
des	O
Tequesta	LOCATION
en	O
1556	O
.	O
C'	O
est	O
au	O
début	O
des	O
années	O
1800	O
que	O
les	O
premiers	O
colons	O
européens	O
,	O
qui	O
viennent	O
pour	O
la	O
plupart	O
des	O
Bahamas	LOCATION
,	O
s'	O
installent	O
dans	O
la	O
zone	O
.	O
La	O
région	O
est	O
touchée	O
par	O
la	O
Seconde	MISC
Guerre	MISC
séminole	MISC
,	O
un	O
conflit	O
qui	O
fait	O
rage	O
de	O
1835	O
à	O
1842	O
.	O
La	O
population	O
civile	O
quitte	O
la	O
région	O
et	O
l'	O
armée	O
installe	O
Fort	LOCATION
Dallas	LOCATION
pour	O
protéger	O
la	O
région	O
.	O
En	O
1844	O
Miami	LOCATION
devient	O
le	O
chef-lieu	O
du	O
comté	O
.	O
Miami	LOCATION
connut	O
son	O
apogée	O
dans	O
les	O
années	O
qui	O
suivirent	O
la	O
Première	MISC
Guerre	MISC
mondiale	MISC
.	O
L'	O
ouragan	MISC
Andrew	MISC
cause	O
plus	O
de	O
45	O
milliards	O
de	O
dollars	O
de	O
dégâts	O
dans	O
la	O
région	O
en	O
1992	O
:	O
c'	O
est	O
l'	O
une	O
des	O
catastrophes	O
naturelles	O
les	O
plus	O
destructrices	O
que	O
le	O
pays	O
a	O
subies	O
.	O
Malgré	O
plusieurs	O
épisodes	O
de	O
crise	O
économique	O
et	O
de	O
tension	O
raciale	O
,	O
ainsi	O
que	O
les	O
problèmes	O
de	O
la	O
corruption	O
et	O
du	O
trafic	O
de	O
stupéfiants	O
,	O
Miami	LOCATION
est	O
actuellement	O
en	O
plein	O
développement	O
et	O
attire	O
toujours	O
de	O
nouvelles	O
populations	O
.	O
Miami	LOCATION
fut	O
durement	O
éprouvée	O
par	O
la	O
récession	O
économique	O
qui	O
s'	O
empara	O
du	O
pays	O
dans	O
les	O
années	O
1980	O
.	O
Parmi	O
les	O
métropoles	O
des	O
États-Unis	LOCATION
,	O
elle	O
est	O
la	O
plus	O
petite	O
en	O
étendue	O
(	O
San	LOCATION
Francisco	LOCATION
et	O
Boston	LOCATION
sont	O
légèrement	O
plus	O
grandes	O
)	O
.	O
La	O
municipalité	O
et	O
ses	O
banlieues	O
occupent	O
une	O
large	O
plaine	O
littorale	O
sur	O
l'	O
estuaire	O
de	O
la	O
Miami	LOCATION
River	LOCATION
.	O
Elle	O
est	O
située	O
entre	O
deux	O
parcs	O
nationaux	O
,	O
celui	O
des	O
Everglades	LOCATION
à	O
l'	O
ouest	O
et	O
celui	O
de	O
la	O
Biscayne	LOCATION
à	O
l'	O
est	O
.	O
Le	O
centre-ville	O
est	O
situé	O
sur	O
la	O
côte	O
de	O
la	O
baie	LOCATION
de	LOCATION
Biscayne	LOCATION
qui	O
est	O
parsemée	O
de	O
nombreuses	O
îles	O
,	O
naturelles	O
ou	O
artificielles	O
,	O
dont	O
la	O
plus	O
étendue	O
abrite	O
Miami	LOCATION
Beach	LOCATION
et	O
South	LOCATION
Beach	LOCATION
.	O
La	O
région	O
métropolitaine	O
de	O
Miami	LOCATION
englobe	O
le	O
comté	LOCATION
de	LOCATION
Miami-Dade	LOCATION
ainsi	O
qu'	O
un	O
grand	O
nombre	O
d'	O
îles	O
,	O
parmi	O
lesquelles	O
Miami	MISC
Beach	MISC
(	O
reliée	O
à	O
la	O
côte	O
par	O
sept	O
ponts	O
routiers	O
)	O
,	O
Virginia	LOCATION
Key	LOCATION
et	O
Key	LOCATION
Biscayne	LOCATION
.	O
Le	O
substrat	O
rocheux	O
de	O
la	O
ville	O
est	O
appelé	O
oolithe	O
de	O
Miami	LOCATION
ou	O
calcaire	O
de	O
Miami	LOCATION
.	O
Le	O
niveau	O
des	O
océans	O
commença	O
à	O
baisser	O
avec	O
le	O
début	O
de	O
la	O
glaciation	LOCATION
du	LOCATION
Wisconsin	LOCATION
,	O
il	O
y	O
a	O
environ	O
100000	O
ans	O
et	O
le	O
lagon	O
commença	O
à	O
émerger	O
.	O
Il	O
sert	O
de	O
source	O
d'	O
eau	O
pour	O
l'	O
agglomération	O
de	O
Miami	LOCATION
.	O
Étant	O
placée	O
entre	O
deux	O
vastes	O
étendues	O
d'	O
eau	O
connues	O
pour	O
leur	O
activité	O
cyclonique	O
,	O
Miami	LOCATION
est	O
la	O
ville	O
où	O
le	O
risque	O
cyclonique	O
est	O
le	O
plus	O
important	O
avec	O
Nassau	LOCATION
(	O
Bahamas	LOCATION
)	O
et	O
La	LOCATION
Havane	LOCATION
à	O
Cuba	LOCATION
.	O
Miami	LOCATION
est	O
l'	O
une	O
des	O
villes	O
américaines	O
les	O
plus	O
arrosées	O
(	O
1488	O
mm	O
par	O
an	O
)	O
,	O
130	O
jours	O
de	O
pluie	O
par	O
an	O
;	O
elles	O
évoluent	O
entre	O
1227	O
mm	O
et	O
1621	O
mm	O
dans	O
le	O
reste	O
de	O
l'	O
agglomération	O
(	O
Miami	LOCATION
Beach	LOCATION
et	O
Fort	LOCATION
Lauderdale	LOCATION
)	O
.	O
Miami	LOCATION
est	O
sur	O
la	O
trajectoire	O
des	O
ouragans	O
capverdiens	O
qui	O
se	O
forment	O
à	O
proximité	O
des	O
îles	LOCATION
du	LOCATION
Cap-Vert	LOCATION
(	O
1000	O
km	O
à	O
la	O
ronde	O
)	O
,	O
près	O
de	O
l'	LOCATION
Afrique	LOCATION
de	LOCATION
l'	LOCATION
ouest	LOCATION
entre	O
la	O
mi-août	O
et	O
la	O
fin	O
de	O
septembre	O
.	O
La	O
ville	O
de	O
Miami	LOCATION
est	O
divisée	O
en	O
cinq	O
districts	O
électoraux	O
,	O
chacun	O
étant	O
géré	O
par	O
un	O
commissionnaire	O
de	O
district	O
.	O
Ces	O
bureaux	O
sont	O
gérés	O
par	O
le	O
NET	MISC
,	O
une	O
structure	O
mise	O
en	O
place	O
par	O
la	O
mairie	O
de	O
Miami	LOCATION
en	O
1992	O
.	O
Downtown	LOCATION
Miami	LOCATION
est	O
le	O
quartier	O
d'	O
affaires	O
et	O
le	O
centre-ville	O
;	O
il	O
se	O
trouve	O
à	O
l'	O
extrémité	O
de	O
la	O
Miami	LOCATION
River	LOCATION
qui	O
se	O
jette	O
dans	O
la	O
baie	LOCATION
de	LOCATION
Biscayne	LOCATION
.	O
Il	O
est	O
le	O
troisième	O
plus	O
important	O
CBD	O
des	O
États-Unis	LOCATION
derrière	O
ceux	O
de	O
New	LOCATION
York	LOCATION
et	O
Chicago	LOCATION
,	O
avec	O
une	O
grande	O
concentration	O
de	O
gratte-ciel	O
.	O
Downtown	LOCATION
Miami	LOCATION
était	O
le	O
centre	O
commercial	O
de	O
Miami	LOCATION
,	O
avec	O
ses	O
commerces	O
,	O
consulats	O
,	O
banques	O
et	O
cabinets	O
d'	O
avocats	O
.	O
Allapattah	LOCATION
est	O
un	O
quartier	O
multiethnique	O
.	O
Actuellement	O
,	O
le	O
plus	O
grand	O
gratte-ciel	O
de	O
Miami	LOCATION
est	O
le	O
Four	LOCATION
Seasons	LOCATION
Hotel	O
Miami	O
avec	O
242	O
m	O
de	O
haut	O
et	O
72	O
étages	O
,	O
achevé	O
en	O
2003	O
.	O
Il	O
est	O
suivi	O
par	O
l'	O
immeuble	O
de	O
bureaux	O
Wachovia	ORGANIZATION
Financial	ORGANIZATION
Center	ORGANIZATION
de	O
233	O
m	O
,	O
inauguré	O
en	O
1984	O
.	O
Si	O
tous	O
les	O
projets	O
et	O
constructions	O
actuels	O
arrivent	O
à	O
terme	O
,	O
Miami	LOCATION
pourrait	O
devenir	O
en	O
2010	O
le	O
troisième	O
panorama	O
urbain	O
du	O
pays	O
,	O
après	O
New	LOCATION
York	LOCATION
City	LOCATION
et	O
Chicago	LOCATION
,	O
par	O
le	O
nombre	O
de	O
gratte-ciel	O
.	O
En	O
2006	O
,	O
le	O
bureau	ORGANIZATION
du	ORGANIZATION
recensement	ORGANIZATION
des	ORGANIZATION
États-Unis	ORGANIZATION
fait	O
état	O
d'	O
une	O
population	O
de	O
404048	O
habitants	O
dans	O
la	O
ville	O
de	O
Miami	LOCATION
:	O
Miami	LOCATION
est	O
la	O
deuxième	O
commune	O
la	O
plus	O
peuplée	O
de	O
Floride	LOCATION
derrière	O
Jacksonville	LOCATION
.	O
Miami	LOCATION
compte	O
134	O
198	O
foyers	O
,	O
dont	O
83	O
336	O
familles	O
.	O
57,7	O
%	O
des	O
habitants	O
de	O
Miami	LOCATION
sont	O
nés	O
à	O
l'	O
étranger	O
,	O
76,2	O
%	O
parlent	O
une	O
autre	O
langue	O
que	O
l'	O
anglais	O
à	O
la	O
maison	O
et	O
61,1	O
%	O
appartiennent	O
au	O
groupe	O
hispanique	O
.	O
Miami	LOCATION
accueille	O
des	O
immigrés	O
venant	O
de	O
toute	O
l'	O
Amérique	LOCATION
latine	O
,	O
principalement	O
de	O
Cuba	LOCATION
,	O
de	O
Haïti	LOCATION
,	O
du	O
Mexique	LOCATION
,	O
de	O
Colombie	LOCATION
,	O
et	O
aussi	O
d'	O
Argentine	LOCATION
depuis	O
la	O
crise	O
économique	O
de	O
2002	O
.	O
L'	O
aire	O
urbaine	O
de	O
Miami	LOCATION
s'	O
étend	O
sur	O
les	O
comtés	O
de	O
Miami-Dade	LOCATION
,	O
Broward	LOCATION
et	O
Palm	LOCATION
Beach	LOCATION
,	O
ce	O
qui	O
représente	O
une	O
population	O
de	O
plus	O
de	O
5,4	O
millions	O
de	O
personnes	O
et	O
en	O
fait	O
d'	O
une	O
part	O
la	O
cinquième	O
agglomération	O
du	O
pays	O
et	O
d'	O
autre	O
part	O
le	O
plus	O
grand	O
ensemble	O
urbain	O
du	O
sud-est	O
des	O
États-Unis	LOCATION
devant	O
Atlanta	LOCATION
et	O
sa	O
périphérie	O
.	O
26,9	O
%	O
de	O
la	O
population	O
et	O
22,8	O
%	O
des	O
familles	O
vivent	O
sous	O
le	O
seuil	O
de	O
pauvreté	O
(	O
contre	O
respectivement	O
13,3	O
%	O
et	O
9,8	O
%	O
pour	O
les	O
États-Unis	LOCATION
)	O
.	O
La	O
communauté	LOCATION
cubaine	LOCATION
de	LOCATION
l'	LOCATION
agglomération	LOCATION
de	LOCATION
Miami	LOCATION
compte	O
environ	O
650000	O
personnes	O
en	O
2006	O
.	O
À	O
sa	O
tête	O
le	O
maire	O
(	O
Tomás	PERSON
Regalado	PERSON
depuis	O
novembre	O
2009	O
)	O
est	O
entouré	O
de	O
cinq	O
commissaires	O
--	O
c'	O
est-à-dire	O
des	O
"	O
conseillers	O
municipaux	O
"	O
--	O
qui	O
chacun	O
supervisent	O
les	O
cinq	O
districts	O
de	O
la	O
ville	O
.	O
Le	O
tourisme	O
est	O
une	O
source	O
majeure	O
de	O
revenus	O
pour	O
la	O
ville	O
qui	O
vit	O
de	O
son	O
image	O
de	O
paradis	O
tropical	O
,	O
notamment	O
avec	O
la	O
ville	O
voisine	O
de	O
Miami	LOCATION
Beach	LOCATION
.	O
Le	O
port	O
de	O
Miami	LOCATION
est	O
le	O
port	O
le	O
plus	O
important	O
du	O
globe	O
pour	O
le	O
transport	O
des	O
passagers	O
de	O
croisière	O
.	O
64	O
consulats	O
sont	O
installés	O
à	O
Miami	LOCATION
.	O
Depuis	O
2005	O
,	O
la	O
région	O
de	O
Miami	LOCATION
vit	O
son	O
plus	O
important	O
boom	O
immobilier	O
depuis	O
les	O
années	O
1920	O
.	O
La	O
ville	O
attire	O
de	O
nombreuses	O
entreprises	O
,	O
dont	O
plusieurs	O
grandes	O
sociétés	O
,	O
qui	O
ont	O
installé	O
leur	O
siège	O
social	O
dans	O
Miami	LOCATION
ou	O
à	O
proximité	O
.	O
Le	O
centre	O
culturel	O
de	O
Miami	LOCATION
abrite	O
la	O
bibliothèque	O
municipale	O
.	O
En	O
raison	O
de	O
son	O
importante	O
communauté	O
cubaine	O
,	O
les	O
musiques	O
latino-américaines	O
ont	O
acquis	O
une	O
large	O
audience	O
à	O
Miami	LOCATION
.	O
Les	O
musiques	O
des	O
Caraïbes	LOCATION
et	O
des	O
Antilles	LOCATION
(	O
reggae	O
,	O
soca	O
,	O
kompa	O
,	O
zouk	O
,	O
calypso	O
,	O
steel-drum	O
)	O
sont	O
également	O
présentes	O
à	O
Miami	LOCATION
.	O
Puis	O
Gloria	PERSON
Estefan	PERSON
et	O
Miami	ORGANIZATION
Sound	ORGANIZATION
Machine	ORGANIZATION
marquèrent	O
les	O
années	O
1980	O
.	O
Les	O
labels	O
indépendants	O
Cat	MISC
Power	MISC
et	O
Iron	ORGANIZATION
&	ORGANIZATION
Wine	ORGANIZATION
sont	O
implantés	O
à	O
Miami	LOCATION
.	O
Miami	LOCATION
est	O
enfin	O
une	O
importante	O
scène	O
de	O
musique	O
techno	O
:	O
la	O
Winter	ORGANIZATION
Music	ORGANIZATION
Conference	ORGANIZATION
est	O
la	O
plus	O
grande	O
manifestation	O
de	O
dance	O
music	O
du	O
monde	O
.	O
Miami	LOCATION
est	O
le	O
douzième	O
marché	O
de	O
la	O
radio	O
et	O
le	O
dix-septième	O
marché	O
de	O
la	O
télévision	O
aux	O
États-Unis	LOCATION
.	O
Les	O
deux	O
universités	O
publiques	O
situées	O
sur	O
le	O
territoire	O
municipal	O
sont	O
la	O
Florida	ORGANIZATION
International	ORGANIZATION
University	ORGANIZATION
et	O
le	O
Miami	LOCATION
Dade	LOCATION
College	LOCATION
.	O
Les	O
jeux	O
vidéo	O
Grand	O
Theft	O
Auto	O
:	O
Vice	LOCATION
City	LOCATION
et	O
Grand	MISC
Theft	MISC
Auto	O
:	O
Vice	O
City	O
Stories	O
se	O
déroulent	O
dans	O
une	O
ville	O
imaginaire	O
fortement	O
inspirée	O
par	O
Miami	LOCATION
au	O
niveau	O
de	O
sa	O
géographie	O
et	O
de	O
l'	O
architecture	O
.	O
Si	O
Miami	LOCATION
fascine	O
par	O
son	O
exotisme	O
,	O
c'	O
est	O
aussi	O
la	O
ville	O
où	O
le	O
désenchantement	O
est	O
le	O
plus	O
grand	O
,	O
où	O
la	O
désillusion	O
atteint	O
son	O
paroxysme	O
.	O
Les	O
immigrants	O
latino-américains	O
ont	O
apporté	O
à	O
Miami	LOCATION
le	O
conga	O
et	O
le	O
rumba	O
.	O
Miami	LOCATION
est	O
l'	O
hôte	O
de	O
la	O
Winter	ORGANIZATION
Music	ORGANIZATION
Conference	ORGANIZATION
.	O
L'	O
aéroport	LOCATION
international	LOCATION
de	LOCATION
Miami	LOCATION
fait	O
partie	O
des	O
trente	O
principaux	O
aéroports	O
du	O
monde	O
:	O
35	O
millions	O
de	O
passagers	O
y	O
transitent	O
par	O
an	O
.	O
C'	O
est	O
la	O
quatrième	O
porte	O
d'	O
entrée	O
du	O
pays	O
pour	O
les	O
voyageurs	O
aériens	O
étrangers	O
après	O
l'	O
aéroport	LOCATION
international	LOCATION
O'Hare	LOCATION
de	LOCATION
Chicago	LOCATION
,	O
l'	O
aéroport	LOCATION
international	LOCATION
John-F.-Kennedy	LOCATION
et	O
l'	O
aéroport	LOCATION
international	LOCATION
de	LOCATION
Los	LOCATION
Angeles	LOCATION
.	O
Le	O
Port	LOCATION
de	LOCATION
Miami	LOCATION
,	O
en	O
plus	O
d'	O
être	O
le	O
plus	O
grand	O
port	O
accueillant	O
des	O
bateaux	O
de	O
croisière	O
au	O
monde	O
,	O
est	O
l'	O
un	O
des	O
plus	O
importants	O
ports	O
de	O
transport	O
de	O
marchandises	O
du	O
pays	O
.	O
Miami	LOCATION
est	O
le	O
terminus	O
sud	O
de	O
services	O
de	O
la	O
côte	LOCATION
Atlantique	LOCATION
d'	O
Amtrak	ORGANIZATION
,	O
dont	O
la	O
gare	O
terminus	O
est	O
située	O
dans	O
une	O
ville	O
de	O
la	O
périphérie	O
,	O
Hialeah	LOCATION
.	O
Elle	O
dirige	O
le	O
plus	O
grand	O
réseau	O
de	O
transport	O
de	O
Floride	LOCATION
,	O
et	O
le	O
14	O
e	O
en	O
importance	O
des	O
États-Unis	LOCATION
.	O
