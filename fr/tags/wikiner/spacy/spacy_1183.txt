On	O
lui	O
doit	O
en	O
particulier	O
le	O
scénario	O
du	O
Petit	MISC
Monde	MISC
de	O
Don	PERSON
Camillo	PERSON
.	O
Il	O
naît	O
en	O
1911	O
à	O
Nyons	LOCATION
,	O
dans	O
la	O
Drôme	LOCATION
paysanne	O
.	O
Barjavel	PERSON
disait	O
n'	O
avoir	O
gardé	O
aucune	O
mélancolie	O
de	O
son	O
enfance	O
,	O
c'	O
est	O
parce	O
qu'	O
il	O
n'	O
en	O
a	O
pas	O
tout	O
perdu	O
,	O
du	O
bonheur	O
incessant	O
de	O
vivre	O
jusqu'	O
aux	O
images	O
fortes	O
des	O
choses	O
les	O
plus	O
simples	O
,	O
qui	O
sont	O
miraculeuses	O
,	O
et	O
éternelles	O
"	O
Elle	O
guettait	O
de	O
nouveau	O
,	O
dragon	O
immobile	O
,	O
au	O
centre	O
de	O
sa	O
rosace	O
,	O
au-dessus	O
de	O
l'	O
eau	O
noire	O
.	O
Il	O
rencontre	O
l'	O
éditeur	O
Robert	PERSON
Denoël	PERSON
au	O
cours	O
d'	O
une	O
interview	O
et	O
celui-ci	O
l'	O
embauche	O
.	O
Ce	O
ne	O
sera	O
pas	O
le	O
cas	O
de	O
Robert	PERSON
Denoël	PERSON
:	O
lorsque	O
le	O
comité	O
d'	O
épuration	O
démet	O
ce	O
dernier	O
de	O
ses	O
fonctions	O
,	O
Barjavel	PERSON
dirigera	O
de	O
fait	O
la	O
maison	O
d'	O
édition	O
jusqu'	O
à	O
l'	O
assassinat	O
de	O
l'	O
éditeur	O
le	O
2	O
décembre	O
1945	O
.	O
La	O
tuberculose	O
et	O
ses	O
lacunes	O
financières	O
l'	O
empêchent	O
de	O
réaliser	O
Barabbas	PERSON
.	O
Il	O
lui	O
expliqua	O
ensuite	O
la	O
conduite	O
à	O
tenir	O
pour	O
ne	O
plus	O
être	O
confronté	O
à	O
de	O
tels	O
soucis	O
de	O
santé	O
,	O
ce	O
que	O
René	PERSON
Barjavel	PERSON
mit	O
en	O
œuvre	O
avec	O
succès	O
.	O
À	O
l'	O
époque	O
(	O
1942	O
)	O
où	O
il	O
publie	O
ses	O
deux	O
premiers	O
romans	O
fantastiques	O
,	O
Barjavel	PERSON
fait	O
figure	O
de	O
précurseur	O
dans	O
le	O
désert	O
qu'	O
est	O
alors	O
la	O
science-fiction	O
française	O
.	O
La	O
science-fiction	O
américaine	O
ne	O
débarquera	O
en	O
effet	O
massivement	O
qu'	O
après	O
1945	O
,	O
et	O
encore	O
faudra-t-il	O
de	O
longues	O
années	O
avant	O
que	O
des	O
noms	O
comme	O
Isaac	PERSON
Asimov	PERSON
,	O
Clifford	PERSON
D.	PERSON
Simak	PERSON
ou	O
même	O
l'	O
ancêtre	O
H.	PERSON
P.	PERSON
Lovecraft	PERSON
soient	O
connus	O
de	O
plus	O
que	O
quelques	O
amateurs	O
.	O
Barjavel	PERSON
,	O
bien	O
que	O
se	O
démarquant	O
de	O
la	O
littérature	O
de	O
l'	O
époque	O
par	O
ses	O
thèmes	O
fantastiques	O
,	O
est	O
aussi	O
un	O
écrivain	O
de	O
son	O
temps	O
.	O
Barjavel	PERSON
se	O
verra	O
à	O
cet	O
égard	O
reprocher	O
sa	O
signature	O
dans	O
différents	O
journaux	O
de	O
la	O
collaboration	O
tels	O
Je	MISC
suis	MISC
partout	MISC
et	O
Gringoire	MISC
.	O
Il	O
abandonnera	O
néanmoins	O
rapidement	O
cette	O
veine	O
collaborationniste	O
suite	O
au	O
succès	O
de	O
Ravage	MISC
.	O
Si	O
Barjavel	PERSON
semble	O
nettement	O
se	O
méfier	O
du	O
progrès	O
,	O
ces	O
inquiétudes	O
étaient	O
très	O
présentes	O
à	O
l'	O
époque	O
(	O
cf	O
.	O
On	O
peut	O
aussi	O
y	O
voir	O
les	O
regrets	O
d'	O
un	O
homme	O
de	O
la	O
terre	O
devant	O
l'	O
exode	O
rural	O
qui	O
allait	O
s'	O
intensifier	O
jusque	O
dans	O
les	O
années	O
1970	O
et	O
transformer	O
la	O
société	O
française	O
de	O
manière	O
irréversible	O
:	O
Ravage	MISC
n'	O
est-il	O
pas	O
dédié	O
par	O
l'	O
auteur	O
"	O
à	O
mes	O
grands-pères	O
paysans	O
"	O
?	O
Barjavel	PERSON
ne	O
manque	O
pas	O
,	O
à	O
travers	O
l'	O
absurde	O
robotisation	O
du	O
"	O
civilisé	O
inconnu	O
"	O
ou	O
les	O
dérapages	O
de	O
l'	O
agriculture	O
industrielle	O
(	O
la	O
poule	O
géante	O
dévorant	O
un	O
stade	O
de	O
football	O
)	O
,	O
de	O
se	O
moquer	O
avec	O
cruauté	O
des	O
dérives	O
de	O
la	O
manipulation	O
du	O
vivant	O
.	O
