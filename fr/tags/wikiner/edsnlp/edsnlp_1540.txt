Hideki	PERSON
Tōjō	PERSON
est	O
né	O
à	O
Tōkyō	LOCATION
(	O
Japon	O
)	O
,	O
en	O
1884	O
.	O
En	O
1919	O
,	O
il	O
entreprit	O
de	O
voyager	O
en	O
Europe	O
(	O
Suisse	O
,	O
Allemagne	O
)	O
.	O
De	O
décembre	O
1938	O
à	O
1940	O
,	O
il	O
fut	O
inspecteur	O
général	O
du	O
Service	O
aérien	O
de	O
l'	O
armée	O
impériale	O
japonaise	O
.	O
Cependant	O
,	O
après	O
une	O
série	O
de	O
défaites	O
,	O
culminant	O
avec	O
la	O
chute	O
de	O
Saipan	O
,	O
il	O
fut	O
abandonné	O
par	O
ses	O
partisans	O
et	O
remercié	O
par	O
Hirohito	O
le	O
18	O
juillet	O
1944	O
.	O
Condamné	O
pour	O
crimes	O
de	O
guerre	O
par	O
le	O
tribunal	O
de	O
Tōkyō	O
en	O
1948	O
,	O
il	O
fut	O
pendu	O
le	O
22	O
décembre	O
1948	O
.	O
Son	O
âme	O
est	O
honorée	O
au	O
sanctuaire	O
de	O
Yasukuni	O
.	O
