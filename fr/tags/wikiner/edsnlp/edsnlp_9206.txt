La	O
Bataille	O
du	O
Tessin	O
est	O
une	O
bataille	O
de	O
la	O
Deuxième	O
Guerre	O
punique	O
.	O
Publius	PERSON
Cornelius	PERSON
Scipio	O
,	O
sorti	O
en	O
reconnaissance	O
avec	O
sa	O
cavalerie	O
et	O
ses	O
jaculatores	O
(	O
lanceurs	O
de	O
javelot	O
)	O
,	O
tombe	O
sur	O
le	O
général	O
carthaginois	O
également	O
en	O
exploration	O
avec	O
sa	O
cavalerie	O
,	O
et	O
une	O
escarmouche	O
éclate	O
.	O
Hannibal	O
fait	O
encercler	O
les	O
Romains	O
par	O
ses	O
ailes	O
constituées	O
de	O
Numides	O
.	O
De	O
justesse	O
,	O
Publius	PERSON
Cornelius	PERSON
Scipio	O
blessé	O
parvient	O
à	O
s'	O
échapper	O
.	O
Tite-Live	PERSON
aime	O
à	O
croire	O
qu'	O
il	O
fut	O
sauvé	O
par	O
son	O
propre	O
fils	O
âgé	O
de	O
17	O
ans	O
,	O
nommé	O
lui	O
aussi	O
Publius	PERSON
Cornelius	PERSON
Scipio	O
,	O
(	O
le	O
futur	O
Scipion	O
l'	O
Africain	O
)	O
et	O
futur	O
vainqueur	O
de	O
la	O
bataille	O
de	O
Zama	LOCATION
.	O
