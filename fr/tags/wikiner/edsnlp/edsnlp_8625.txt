À	O
partir	O
des	O
années	O
1970	O
,	O
le	O
terme	O
eutrophisation	O
a	O
été	O
employé	O
pour	O
qualifier	O
la	O
dégradation	O
des	O
grands	O
lacs	O
comme	O
le	O
lac	O
d'	O
Annecy	O
,	O
le	O
lac	O
du	O
Bourget	O
ou	O
le	O
lac	O
Léman	O
par	O
excès	O
de	O
nutriments	O
.	O
Dans	O
ce	O
pays	O
,	O
90	O
%	O
des	O
rivières	O
dépassaient	O
pour	O
leur	O
taux	O
d'	O
azote	O
et	O
de	O
phosphore	O
les	O
seuils	O
de	O
l'	O
EPA	O
en	O
2007	O
;	O
et	O
toutes	O
les	O
éco-régions	O
étaient	O
touchées	O
.	O
Dans	O
les	O
années	O
1950	O
à	O
1970	O
,	O
les	O
Grands	O
Lacs	O
d'	O
Amérique	O
du	O
Nord	O
étaient	O
devenus	O
les	O
déversoirs	O
naturels	O
d'	O
égouts	O
des	O
villes	O
environnantes	O
.	O
