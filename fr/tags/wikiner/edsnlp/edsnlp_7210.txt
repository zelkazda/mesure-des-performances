Cette	O
bataille	O
fut	O
une	O
défaite	O
cuisante	O
contre	O
le	O
carthaginois	O
Hannibal	PERSON
Barca	PERSON
.	O
A	O
Rome	O
,	O
personne	O
ne	O
se	O
propose	O
pour	O
remplacer	O
les	O
deux	O
Scipion	O
tués	O
par	O
les	O
Carthaginois	O
.	O
Il	O
libère	O
les	O
otages	O
des	O
diverses	O
tribus	O
espagnoles	O
gardées	O
par	O
Hasdrubal	O
pour	O
s'	O
assurer	O
de	O
leur	O
fidélité	O
.	O
Dans	O
le	O
même	O
temps	O
il	O
traite	O
avec	O
le	O
roi	O
numide	O
Syphax	PERSON
.	O
Il	O
passe	O
ensuite	O
le	O
détroit	O
et	O
s'	O
empare	O
de	O
la	O
ville	O
de	O
Locres	LOCATION
,	O
qui	O
s'	O
était	O
ralliée	O
aux	O
Carthaginois	O
.	O
Massinissa	PERSON
,	O
qui	O
a	O
été	O
chassé	O
de	O
son	O
trône	O
,	O
par	O
Syphax	O
,	O
le	O
rejoint	O
avec	O
une	O
troupe	O
Numide	O
.	O
Les	O
débuts	O
de	O
la	O
guerre	O
sont	O
favorables	O
aux	O
Romains	O
mais	O
devant	O
Utique	O
,	O
Scipion	O
échoue	O
.	O
Le	O
sénat	O
de	O
Carthage	LOCATION
craint	O
que	O
Scipion	O
ne	O
mette	O
le	O
siège	O
à	O
la	O
cité	O
.	O
Les	O
Carthaginois	O
sont	O
obligés	O
d'	O
accepter	O
les	O
conditions	O
de	O
la	O
paix	O
imposée	O
par	O
les	O
Romains	O
.	O
Il	O
prend	O
part	O
à	O
la	O
guerre	O
avec	O
son	O
frère	O
Scipion	PERSON
l'	O
Asiatique	O
contre	O
Antiochos	O
III	O
de	O
Syrie	O
.	O
)	O
,	O
partie	O
appelée	O
"	O
Le	O
songe	O
de	O
Scipion	O
"	O
,	O
lui	O
attribue	O
une	O
pensée	O
pythagoricienne	O
.	O
