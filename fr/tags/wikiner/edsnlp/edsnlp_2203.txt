Il	O
a	O
terminé	O
sa	O
vie	O
en	O
France	O
,	O
à	O
Cotignac	LOCATION
,	O
où	O
il	O
est	O
inhumé	O
.	O
Timothy	PERSON
Findley	PERSON
est	O
né	O
à	O
Toronto	LOCATION
,	O
en	O
Ontario	O
,	O
le	O
30	O
octobre	O
1930	O
où	O
il	O
fait	O
ses	O
études	O
et	O
veut	O
être	O
danseur	O
avant	O
de	O
devenir	O
de	O
1953	O
à	O
1957	O
acteur	O
.	O
Timothy	PERSON
Findley	PERSON
s'	O
est	O
imposé	O
comme	O
un	O
des	O
écrivains	O
marquant	O
de	O
la	O
littérature	O
de	O
langue	O
anglaise	O
au	O
Canada	O
avec	O
ses	O
pièces	O
de	O
théâtre	O
et	O
ses	O
romans	O
qui	O
sont	O
traduits	O
dans	O
de	O
nombreuses	O
langues	O
et	O
principalement	O
le	O
français	O
.	O
Timothy	PERSON
Findley	PERSON
écrit	O
avec	O
un	O
style	O
maîtrisé	O
et	O
élégant	O
en	O
variant	O
les	O
tonalités	O
et	O
en	O
explorant	O
quelques	O
grand	O
thèmes	O
récurrents	O
comme	O
la	O
destruction	O
des	O
liens	O
affectifs	O
,	O
la	O
solitude	O
et	O
la	O
folie	O
et	O
souvent	O
le	O
regard	O
d'	O
un	O
enfant	O
désemparé	O
devant	O
la	O
noirceur	O
du	O
monde	O
et	O
des	O
âmes	O
.	O
