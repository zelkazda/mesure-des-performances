Frans	PERSON
Eemil	PERSON
Sillanpää	PERSON
(	O
né	O
le	O
16	O
septembre	O
1888	O
à	O
Hämeenkyrö	LOCATION
,	O
Finlande	O
--	O
mort	O
le	O
3	O
juin	O
1964	O
à	O
Helsinki	LOCATION
)	O
est	O
un	O
écrivain	O
finlandais	O
,	O
romancier	O
et	O
nouvelliste	O
,	O
adepte	O
du	O
néo-réalisme	O
psychologique	O
.	O
Ses	O
personnages	O
sont	O
issus	O
du	O
petit	O
peuple	O
de	O
la	O
Finlande	O
rurale	O
:	O
servantes	O
,	O
métayers	O
ou	O
petits	O
propriétaires	O
.	O
Il	O
a	O
reçu	O
le	O
prix	O
Nobel	O
de	O
littérature	O
en	O
1939	O
.	O
