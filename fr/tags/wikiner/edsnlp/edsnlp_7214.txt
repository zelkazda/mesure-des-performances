Il	O
est	O
principalement	O
connu	O
pour	O
sa	O
Vie	O
des	O
douze	O
Césars	O
,	O
qui	O
comprend	O
les	O
biographies	O
de	O
Jules	O
César	O
à	O
Domitien	O
.	O
Peu	O
de	O
choses	O
sont	O
connues	O
de	O
la	O
vie	O
de	O
Suétone	PERSON
.	O
Suétone	O
naquit	O
probablement	O
à	O
Rome	LOCATION
vers	O
69	O
--	O
70	O
ap	O
.	O
Une	O
des	O
lettres	O
de	O
son	O
ami	O
et	O
protecteur	O
Pline	O
le	O
Jeune	O
nous	O
présente	O
Suétone	PERSON
,	O
alors	O
âgé	O
d'	O
environ	O
28	O
ans	O
,	O
se	O
disposant	O
à	O
plaider	O
comme	O
avocat	O
;	O
une	O
autre	O
,	O
écrite	O
vers	O
101	O
,	O
nous	O
montre	O
Suétone	O
briguant	O
un	O
poste	O
de	O
tribun	O
militaire	O
,	O
condition	O
nécessaire	O
pour	O
pouvoir	O
prétendre	O
à	O
la	O
carrière	O
équestre	O
.	O
Il	O
semble	O
cependant	O
que	O
Suétone	PERSON
réussit	O
à	O
se	O
faire	O
dispenser	O
du	O
service	O
militaire	O
.	O
Par	O
ailleurs	O
,	O
son	O
ami	O
Pline	O
le	O
Jeune	O
lui	O
fit	O
obtenir	O
de	O
l'	O
empereur	O
Trajan	O
en	O
112	O
le	O
privilège	O
accordé	O
aux	O
pères	O
de	O
trois	O
enfants	O
,	O
bien	O
qu'	O
il	O
n'	O
en	O
eût	O
aucun	O
.	O
Cette	O
charge	O
permit	O
notamment	O
à	O
Suétone	PERSON
d'	O
avoir	O
accès	O
aux	O
archives	O
impériales	O
.	O
Entre	O
119	O
et	O
122	O
,	O
parait	O
la	O
Vie	O
des	O
douze	O
Césars	O
,	O
point	O
culminant	O
de	O
sa	O
carrière	O
.	O
Toujours	O
est-il	O
que	O
nous	O
ne	O
savons	O
plus	O
rien	O
de	O
Suétone	O
après	O
cette	O
date	O
;	O
sans	O
doute	O
a-t-il	O
vécu	O
dès	O
lors	O
dans	O
la	O
retraite	O
,	O
en	O
se	O
consacrant	O
tout	O
entier	O
à	O
ses	O
travaux	O
de	O
grammaire	O
,	O
de	O
littérature	O
et	O
d'	O
histoire	O
.	O
Suétone	O
fut	O
un	O
auteur	O
très	O
fécond	O
si	O
l'	O
on	O
en	O
croit	O
la	O
longue	O
liste	O
d'	O
ouvrages	O
que	O
la	O
Souda	O
et	O
certains	O
auteurs	O
lui	O
attribuent	O
.	O
C'	O
était	O
un	O
érudit	O
qui	O
écrivit	O
sur	O
les	O
sujets	O
les	O
plus	O
divers	O
,	O
un	O
polygraphe	O
animé	O
d'	O
une	O
incroyable	O
curiosité	O
,	O
qui	O
possédait	O
un	O
savoir	O
encyclopédique	O
à	O
la	O
manière	O
de	O
Varron	O
.	O
Le	O
style	O
de	O
Suétone	O
est	O
froid	O
et	O
sans	O
grand	O
ornement	O
.	O
La	O
prose	O
de	O
Suétone	O
est	O
celle	O
d'	O
un	O
compilateur	O
,	O
qui	O
ne	O
manifeste	O
de	O
l'	O
émotion	O
qu'	O
avec	O
circonspection	O
.	O
Mais	O
la	O
critique	O
reconnaît	O
généralement	O
la	O
vivacité	O
des	O
portraits	O
de	O
Suétone	O
,	O
rédigés	O
dans	O
une	O
prose	O
simple	O
et	O
précise	O
,	O
visant	O
avant	O
tout	O
à	O
l'	O
efficacité	O
,	O
et	O
dépourvue	O
de	O
la	O
phraséologie	O
archaïque	O
et	O
précieuse	O
qui	O
encombre	O
la	O
littérature	O
contemporaine	O
.	O
Suétone	O
suit	O
un	O
plan	O
immuable	O
:	O
il	O
évoque	O
d'	O
abord	O
la	O
famille	O
de	O
l'	O
empereur	O
,	O
ses	O
jeunes	O
années	O
,	O
puis	O
sa	O
carrière	O
publique	O
,	O
son	O
physique	O
et	O
sa	O
vie	O
privée	O
.	O
