La	O
pollution	O
d'	O
origine	O
humaine	O
peut	O
avoir	O
un	O
impact	O
très	O
important	O
sur	O
la	O
santé	O
et	O
dans	O
la	O
biosphère	O
comme	O
en	O
témoigne	O
l'	O
exposition	O
aux	O
polluants	O
et	O
le	O
réchauffement	O
climatique	O
qui	O
transforme	O
le	O
climat	O
de	O
la	O
Terre	O
et	O
son	O
écosystème	O
,	O
en	O
entraînant	O
l'	O
apparition	O
de	O
maladies	O
inconnues	O
jusqu'	O
alors	O
dans	O
certaines	O
zones	O
géographiques	O
,	O
des	O
migrations	O
de	O
certaines	O
espèces	O
,	O
voire	O
leur	O
extinction	O
si	O
elles	O
ne	O
peuvent	O
s'	O
adapter	O
à	O
leur	O
nouvel	O
environnement	O
biophysique	O
.	O
C'	O
est	O
après	O
la	O
Seconde	O
Guerre	O
mondiale	O
qu'	O
une	O
prise	O
de	O
conscience	O
des	O
répercussions	O
des	O
activités	O
humaines	O
sur	O
l'	O
environnement	O
voit	O
le	O
jour	O
via	O
la	O
naissance	O
de	O
l'	O
écologisme	O
.	O
L'	O
Europe	O
dispose	O
d'	O
un	O
registre	O
européen	O
des	O
émissions	O
polluantes	O
couvrant	O
cinquante	O
polluants	O
(	O
eau	O
et	O
air	O
uniquement	O
)	O
,	O
émis	O
par	O
les	O
principales	O
(	O
grandes	O
et	O
moyennes	O
)	O
installations	O
industrielles	O
.	O
L'	O
une	O
des	O
unités	O
retenues	O
en	O
France	O
est	O
le	O
métox	O
,	O
mais	O
uniquement	O
pour	O
huit	O
polluants	O
de	O
type	O
métaux	O
et	O
métalloïdes	O
(	O
arsenic	O
,	O
cadmium	O
,	O
chrome	O
,	O
cuivre	O
,	O
mercure	O
,	O
nickel	O
,	O
plomb	O
et	O
zinc	O
)	O
.	O
La	O
Commission	O
européenne	O
a	O
présenté	O
le	O
9	O
février	O
2007	O
un	O
projet	O
de	O
directive	O
visant	O
à	O
condamner	O
de	O
manière	O
uniforme	O
au	O
sein	O
de	O
l'	O
Union	O
européenne	O
les	O
crimes	O
environnementaux	O
.	O
À	O
Paris	LOCATION
,	O
ville	O
très	O
peu	O
industrialisée	O
,	O
la	O
pollution	O
est	O
due	O
principalement	O
aux	O
transports	O
automobiles	O
et	O
pour	O
une	O
petite	O
partie	O
aux	O
activités	O
fixes	O
(	O
usines	O
,	O
chauffages	O
,	O
incinérateurs	O
)	O
.	O
Afin	O
de	O
permettre	O
une	O
gestion	O
équilibrée	O
de	O
l'	O
eau	O
,	O
la	O
France	O
a	O
été	O
découpée	O
en	O
six	O
bassins	O
versants	O
hydrogéographiques	O
principaux	O
.	O
