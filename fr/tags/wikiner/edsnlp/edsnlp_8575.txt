Bhopal	O
est	O
une	O
ville	O
et	O
une	O
ancienne	O
principauté	O
de	O
l'	O
Inde	O
.	O
Bhopal	O
est	O
la	O
capitale	O
du	O
Madhya	O
Pradesh	O
avec	O
environ	O
1,5	O
millions	O
d'	O
habitants	O
.	O
La	O
ville	O
est	O
célèbre	O
pour	O
avoir	O
été	O
le	O
théâtre	O
de	O
la	O
plus	O
importante	O
catastrophe	O
industrielle	O
au	O
monde	O
:	O
la	O
catastrophe	O
de	O
Bhopal	O
a	O
causé	O
la	O
mort	O
de	O
plusieurs	O
milliers	O
de	O
personnes	O
la	O
nuit	O
du	O
2	O
au	O
3	O
décembre	O
1984	O
.	O
Bhopal	O
est	O
la	O
capitale	O
de	O
l'	O
état	O
du	O
Madhya	O
Pradesh	O
.	O
Bhopal	O
possède	O
un	O
aéroport	O
.	O
Bien	O
que	O
gouvernée	O
par	O
des	O
dirigeants	O
musulmans	O
,	O
la	O
population	O
de	O
Bhopal	LOCATION
a	O
toujours	O
été	O
majoritairement	O
hindoue	O
.	O
La	O
catastrophe	O
de	O
Bhopal	O
est	O
la	O
plus	O
importante	O
catastrophe	O
industrielle	O
connue	O
à	O
ce	O
jour	O
.	O
