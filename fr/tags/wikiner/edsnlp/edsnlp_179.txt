Le	O
mot	O
"	O
bible	O
"	O
désigne	O
l'	O
ensemble	O
du	O
corpus	O
des	O
textes	O
religieux	O
juifs	O
(	O
Bible	O
hébraïque	O
)	O
ou	O
judéo-chrétiens	O
(	O
Bible	O
chrétienne	O
)	O
.	O
Les	O
versions	O
que	O
nous	O
en	O
connaissons	O
de	O
nos	O
jours	O
,	O
comme	O
le	O
Codex	O
Sinaiticus	O
pour	O
le	O
Nouveau	O
Testament	O
,	O
sont	O
notablement	O
plus	O
tardives	O
que	O
la	O
période	O
supposée	O
de	O
rédaction	O
.	O
Un	O
article	O
spécifique	O
a	O
pour	O
objet	O
l'	O
étude	O
du	O
Tanakh	O
.	O
Les	O
chrétiens	O
nomment	O
Ancien	O
Testament	O
la	O
partie	O
de	O
la	O
Bible	O
qui	O
reprend	O
les	O
textes	O
canoniques	O
du	O
Tanakh	O
et	O
d'	O
autres	O
textes	O
antiques	O
non	O
repris	O
par	O
la	O
tradition	O
judaïque	O
.	O
La	O
Bible	O
chrétienne	O
contient	O
en	O
outre	O
un	O
Nouveau	O
Testament	O
qui	O
regroupe	O
les	O
écrits	O
relatifs	O
à	O
l'	O
avènement	O
de	O
Jésus-Christ	O
.	O
Le	O
mot	O
"	O
Testament	O
"	O
traduit	O
du	O
latin	O
testamentum	O
,	O
correspond	O
au	O
mot	O
grec	O
διαθήκη	O
qui	O
signifie	O
"	O
convention	O
"	O
ou	O
"	O
disposition	O
écrite	O
"	O
(	O
d'	O
où	O
"	O
testament	O
"	O
)	O
qui	O
devint	O
dans	O
le	O
contexte	O
biblique	O
"	O
pacte	O
"	O
ou	O
"	O
alliance	O
"	O
.	O
Comme	O
les	O
papyrus	O
égyptiens	O
étaient	O
particulièrement	O
bien	O
préparés	O
à	O
la	O
ville	O
du	O
bord	O
de	O
mer	O
de	O
Byblos	LOCATION
,	O
les	O
Grecs	O
empruntèrent	O
le	O
terme	O
de	O
"	O
biblios	O
"	O
pour	O
désigner	O
le	O
"	O
livre	O
"	O
et	O
le	O
mot	O
s'	O
est	O
ainsi	O
conservé	O
jusqu'	O
à	O
nos	O
jours	O
.	O
Le	O
corpus	O
biblique	O
réunit	O
plusieurs	O
livres	O
d'	O
origines	O
diverses	O
,	O
d'	O
où	O
l'	O
étymologie	O
du	O
mot	O
Bible	O
.	O
La	O
Biblia	O
Hebraica	O
Stuttgartensia	O
en	O
est	O
la	O
principale	O
édition	O
critique	O
publiée	O
pour	O
la	O
première	O
fois	O
en	O
1936	O
.	O
En	O
1555	O
fut	O
publiée	O
l'	O
édition	O
de	O
la	O
Vulgate	O
latine	O
par	O
Robert	PERSON
Estienne	PERSON
;	O
c'	O
était	O
la	O
première	O
Bible	O
complète	O
avec	O
la	O
numérotation	O
actuelle	O
des	O
chapitres	O
et	O
des	O
versets	O
.	O
La	O
Bible	O
hébraïque	O
connaît	O
un	O
autre	O
type	O
de	O
division	O
,	O
celui	O
des	O
parashiot	O
(	O
singulier	O
:	O
parasha	O
)	O
(	O
marquées	O
par	O
un	O
phé	O
dans	O
le	O
texte	O
)	O
qui	O
représente	O
la	O
répartition	O
des	O
lectures	O
hebdomadaires	O
de	O
la	O
Torah	O
.	O
C'	O
est	O
ce	O
texte-ci	O
qui	O
sera	O
retenu	O
en	O
1530	O
comme	O
Ancien	O
Testament	O
par	O
les	O
protestants	O
,	O
qui	O
l'	O
éditeront	O
pourtant	O
dans	O
l'	O
ordre	O
des	O
livres	O
de	O
la	O
Bible	O
grecque	O
.	O
La	O
traduction	O
s'	O
adresse	O
aux	O
Juifs	O
parlant	O
le	O
grec	O
.	O
Dans	O
le	O
monde	O
chrétien	O
,	O
en	O
revanche	O
,	O
la	O
Septante	O
continue	O
d'	O
être	O
la	O
référence	O
et	O
connaît	O
plusieurs	O
traductions	O
en	O
latin	O
.	O
Lors	O
de	O
sa	O
traduction	O
latine	O
,	O
la	O
Vulgate	O
,	O
Jérôme	PERSON
choisit	O
la	O
version	O
hébraïque	O
lorsqu'	O
elle	O
existe	O
,	O
et	O
met	O
en	O
annexe	O
les	O
livres	O
pour	O
lesquelles	O
elle	O
n'	O
existe	O
pas	O
ou	O
plus	O
.	O
Leur	O
religion	O
est	O
basée	O
sur	O
une	O
version	O
particulière	O
du	O
Pentateuque	O
,	O
la	O
Bible	O
Samaritaine	O
.	O
Bien	O
qu'	O
ils	O
soient	O
apparus	O
avant	O
le	O
développement	O
du	O
judaïsme	O
rabbinique	O
et	O
que	O
cette	O
différence	O
ne	O
soit	O
donc	O
pas	O
à	O
l'	O
origine	O
de	O
leur	O
divergence	O
,	O
ils	O
n'	O
ont	O
pas	O
de	O
rabbins	O
et	O
n'	O
acceptent	O
pas	O
le	O
Talmud	O
du	O
judaïsme	O
orthodoxe	O
.	O
Les	O
Samaritains	O
refusent	O
également	O
les	O
livres	O
de	O
la	O
Bible	O
hébraïque	O
postérieurs	O
au	O
Pentateuque	O
.	O
À	O
l'	O
inverse	O
,	O
les	O
Juifs	O
orthodoxes	O
les	O
considèrent	O
comme	O
des	O
descendants	O
de	O
populations	O
étrangères	O
ayant	O
adopté	O
une	O
version	O
illégitime	O
de	O
la	O
religion	O
hébraïque	O
.	O
Les	O
plus	O
importantes	O
portent	O
sur	O
la	O
situation	O
du	O
Mont	O
Garizim	O
comme	O
principal	O
lieu	O
saint	O
en	O
lieu	O
et	O
place	O
de	O
Jérusalem	O
.	O
Les	O
deux	O
versions	O
des	O
dix	O
commandements	O
existants	O
dans	O
le	O
Tanakh	O
juif	O
(	O
celle	O
du	O
livre	O
de	O
l'	O
Exode	O
et	O
celle	O
du	O
Deutéronome	O
)	O
ont	O
été	O
également	O
uniformisées	O
.	O
Pour	O
les	O
Samaritains	O
,	O
"	O
les	O
sages	O
juifs	O
ont	O
fait	O
de	O
la	O
présentation	O
un	O
commandement	O
pour	O
maintenir	O
le	O
nombre	O
de	O
ceux-ci	O
à	O
dix	O
(	O
le	O
nombre	O
de	O
commandements	O
est	O
mentionné	O
dans	O
l'	O
Exode	O
,	O
34.28	O
)	O
,	O
après	O
qu'	O
ils	O
ont	O
corrigé	O
leur	O
version	O
en	O
en	O
retirant	O
le	O
dixième	O
"	O
relatif	O
au	O
mont	O
Garizim	O
.	O
Au-delà	O
de	O
ces	O
différences	O
fondamentales	O
,	O
il	O
existe	O
d'	O
assez	O
nombreuses	O
différences	O
portant	O
sur	O
des	O
détails	O
de	O
rédaction	O
entre	O
la	O
Torah	O
samaritaine	O
et	O
la	O
Torah	O
Juive	O
.	O
Plus	O
intéressant	O
,	O
certaines	O
traductions	O
grecque	O
de	O
la	O
Septante	O
correspondent	O
étroitement	O
à	O
des	O
textes	O
hébreux	O
des	O
manuscrits	O
de	O
la	O
mer	O
Morte	O
.	O
En	O
toute	O
hypothèse	O
,	O
si	O
les	O
divergences	O
concernant	O
la	O
place	O
du	O
mont	O
Garizim	O
et	O
de	O
Jérusalem	O
s'	O
expliquent	O
aisément	O
,	O
tant	O
elles	O
sont	O
fondatrices	O
pour	O
l'	O
existence	O
même	O
des	O
Juifs	O
et	O
des	O
Samaritains	O
,	O
les	O
divergences	O
ou	O
les	O
ressemblances	O
entre	O
la	O
Bible	O
samaritaine	O
et	O
les	O
différentes	O
versions	O
juives	O
connues	O
ont	O
des	O
origines	O
plus	O
obscures	O
.	O
Luther	O
les	O
considérait	O
néanmoins	O
comme	O
utiles	O
.	O
Les	O
protestants	O
les	O
nomment	O
apocryphes	O
(	O
du	O
grec	O
αποκρυφος	O
,	O
caché	O
)	O
;	O
les	O
catholiques	O
les	O
nomment	O
deutérocanoniques	O
,	O
c'	O
est-à-dire	O
entrés	O
secondairement	O
dans	O
le	O
canon	O
(	O
du	O
grec	O
δευτερος	O
,	O
deuxième	O
)	O
,	O
ce	O
qui	O
a	O
été	O
définitivement	O
confirmé	O
au	O
concile	O
de	O
Trente	O
en	O
1546	O
.	O
Certains	O
des	O
livres	O
de	O
la	O
Septante	O
n'	O
ont	O
pas	O
été	O
reçus	O
même	O
comme	O
deutérocanoniques	O
.	O
Comme	O
pour	O
l'	O
Ancien	O
Testament	O
,	O
la	O
canonicité	O
de	O
plusieurs	O
livres	O
du	O
Nouveau	O
Testament	O
a	O
longtemps	O
été	O
débattue	O
.	O
Les	O
Actes	O
sont	O
incontestablement	O
la	O
suite	O
de	O
Luc	O
.	O
Les	O
lectures	O
de	O
la	O
Bible	O
peuvent	O
être	O
différentes	O
entre	O
le	O
judaïsme	O
et	O
le	O
christianisme	O
,	O
et	O
entre	O
les	O
différentes	O
branches	O
du	O
christianisme	O
.	O
Pour	O
le	O
judaïsme	O
,	O
la	O
question	O
de	O
la	O
composition	O
du	O
Tanakh	O
ne	O
se	O
pose	O
pas	O
.	O
Maïmonide	O
,	O
pourtant	O
suspect	O
de	O
rationalisme	O
,	O
pose	O
en	O
article	O
de	O
foi	O
que	O
la	O
Torah	O
a	O
été	O
donnée	O
à	O
Moïse	O
,	O
comme	O
il	O
est	O
décrit	O
dans	O
l'	O
Exode	O
.	O
Le	O
concile	O
de	O
Trente	O
insiste	O
sur	O
cette	O
unique	O
source	O
de	O
la	O
foi	O
.	O
La	O
Bible	O
a	O
toujours	O
été	O
lue	O
et	O
étudiée	O
par	O
les	O
religieux	O
et	O
les	O
intellectuels	O
dans	O
le	O
monde	O
catholique	O
,	O
mais	O
,	O
jusqu'	O
au	O
Concile	O
Vatican	O
II	O
,	O
la	O
grande	O
masse	O
des	O
fidèles	O
la	O
connaissaient	O
surtout	O
à	O
travers	O
le	O
lectionnaire	O
dominical	O
.	O
La	O
connaissance	O
de	O
la	O
Bible	O
s'	O
est	O
accrue	O
chez	O
les	O
fidèles	O
par	O
la	O
diffusion	O
de	O
la	O
traduction	O
,	O
menée	O
par	O
l'	O
École	O
biblique	O
et	O
archéologique	O
française	O
de	O
Jérusalem	O
,	O
appelée	O
la	O
Bible	O
de	O
Jérusalem	O
(	O
première	O
édition	O
en	O
un	O
volume	O
en	O
1956	O
)	O
.	O
En	O
outre	O
certains	O
diocèses	O
proposent	O
une	O
formation	O
aux	O
langues	O
de	O
la	O
Bible	O
(	O
grec	O
de	O
la	O
koinè	O
,	O
hébreu	O
biblique	O
,	O
occasionnellement	O
araméen	O
)	O
.	O
La	O
lecture	O
et	O
le	O
commentaire	O
de	O
la	O
Bible	O
,	O
qui	O
sont	O
le	O
cœur	O
du	O
culte	O
protestant	O
,	O
font	O
aussi	O
partie	O
de	O
la	O
piété	O
familiale	O
et	O
personnelle	O
dans	O
le	O
protestantisme	O
historique	O
.	O
La	O
Bible	O
est	O
donc	O
aussi	O
l'	O
autorité	O
dernière	O
pour	O
la	O
foi	O
comme	O
pour	O
la	O
vie	O
,	O
étant	O
entendu	O
que	O
personne	O
ne	O
détient	O
de	O
magistère	O
pour	O
imposer	O
une	O
interprétation	O
plutôt	O
qu'	O
une	O
autre	O
.	O
Un	O
rapport	O
aussi	O
direct	O
et	O
fondamental	O
au	O
texte	O
biblique	O
suppose	O
et	O
entraîne	O
des	O
études	O
bibliques	O
poussées	O
pour	O
les	O
futurs	O
pasteurs	O
,	O
des	O
études	O
bibliques	O
en	O
paroisse	O
,	O
une	O
catéchèse	O
d'	O
enfants	O
elle	O
aussi	O
centrée	O
sur	O
la	O
Bible	O
,	O
le	O
recours	O
aux	O
langues	O
d'	O
origine	O
,	O
l'	O
utilisation	O
d'	O
une	O
multiplicité	O
de	O
traductions	O
,	O
des	O
listes	O
quotidiennes	O
de	O
lectures	O
commentées	O
,	O
etc	O
.	O
Comme	O
le	O
dit	O
la	O
Bible	O
,	O
la	O
véritable	O
conversion	O
sera	O
suivi	O
de	O
fruits	O
manifestes	O
.	O
Ce	O
dernier	O
utilise	O
la	O
Société	O
Watchtower	O
pour	O
éditer	O
des	O
publications	O
que	O
les	O
fidèles	O
doivent	O
utiliser	O
,	O
celles-ci	O
étant	O
jugées	O
importantes	O
pour	O
comprendre	O
la	O
Bible	O
.	O
Chaque	O
Témoin	O
est	O
encouragé	O
à	O
prendre	O
du	O
temps	O
quotidiennement	O
pour	O
lire	O
la	O
Bible	O
.	O
D'	O
après	O
des	O
théories	O
récentes	O
,	O
aussi	O
bien	O
linguistiques	O
qu'	O
archéologiques	O
,	O
la	O
structure	O
globale	O
des	O
textes	O
de	O
la	O
Bible	O
hébraïque	O
aurait	O
été	O
compilée	O
au	O
temps	O
du	O
roi	O
Josias	O
,	O
bien	O
que	O
la	O
matière	O
première	O
soit	O
issue	O
d'	O
écrits	O
plus	O
anciens	O
.	O
Pour	O
ce	O
qui	O
concerne	O
l'	O
Exode	O
et	O
le	O
séjour	O
au	O
désert	O
pendant	O
quarante	O
ans	O
,	O
les	O
fouilles	O
des	O
lieux	O
qui	O
sont	O
cités	O
dans	O
la	O
Bible	O
ne	O
corroborrent	O
pas	O
les	O
descriptions	O
bibliques	O
et	O
pousent	O
remettre	O
fondamentalement	O
en	O
question	O
la	O
chronologie	O
jusque	O
là	O
proposée	O
.	O
Il	O
les	O
regroupe	O
en	O
deux	O
sujets	O
majeurs	O
qui	O
ont	O
marqué	O
les	O
consciences	O
,	O
puis	O
rapportés	O
jusqu'	O
à	O
nos	O
jours	O
oralement	O
puis	O
par	O
l'	O
écriture	O
dont	O
la	O
Bible	O
.	O
C'	O
est	O
aussi	O
le	O
cas	O
d'	O
un	O
Sinaï	O
volcanique	O
du	O
récit	O
très	O
différent	O
d'	O
aujourd'hui	O
.	O
Le	O
premier	O
livre	O
qui	O
soit	O
sorti	O
des	O
presses	O
de	O
Gutenberg	O
a	O
été	O
la	O
Bible	O
dans	O
la	O
version	O
latine	O
de	O
saint	O
Jérôme	O
,	O
la	O
Vulgate	O
.	O
Au	O
31	O
décembre	O
2007	O
,	O
la	O
Bible	O
,	O
en	O
totalité	O
ou	O
en	O
partie	O
,	O
avait	O
été	O
traduite	O
en	O
2	O
454	O
langues	O
.	O
95	O
%	O
des	O
êtres	O
humains	O
ont	O
désormais	O
accès	O
à	O
la	O
Bible	O
dans	O
une	O
langue	O
qu'	O
ils	O
comprennent	O
.	O
À	O
ce	O
jour	O
,	O
on	O
estime	O
à	O
40	O
millions	O
le	O
nombre	O
de	O
bibles	O
distribuées	O
chaque	O
année	O
,	O
dont	O
280000	O
en	O
France	O
.	O
Des	O
chiffres	O
auxquels	O
il	O
faut	O
ajouter	O
le	O
nombre	O
impressionnant	O
d'	O
exemplaires	O
du	O
Nouveau	O
Testament	O
diffusés	O
(	O
sans	O
doute	O
cinq	O
fois	O
plus	O
que	O
les	O
bibles	O
complètes	O
)	O
.	O
