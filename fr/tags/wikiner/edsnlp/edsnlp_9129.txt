Construit	O
par	O
les	O
chantiers	O
Blohm	PERSON
&	PERSON
Voss	PERSON
de	O
Hambourg	LOCATION
,	O
le	O
Cap	O
Arcona	O
fut	O
lancé	O
le	O
14	O
mai	O
1927	O
.	O
Son	O
nom	O
,	O
Cap	O
Arcona	O
,	O
provient	O
du	O
Kap	O
Arkona	O
sur	O
l'	O
île	O
allemande	O
de	O
Rügen	LOCATION
(	LOCATION
Mecklembourg-Poméranie	LOCATION
occidentale	LOCATION
)	O
.	O
Ce	O
vapeur	O
servit	O
aussi	O
bien	O
à	O
des	O
croisières	O
de	O
luxe	O
qu'	O
à	O
l'	O
émigration	O
,	O
principalement	O
vers	O
l'	O
Amérique	O
du	O
Sud	O
.	O
Le	O
navire	O
fut	O
réquisitionné	O
par	O
la	O
Kriegsmarine	O
le	O
25	O
août	O
1939	O
.	O
Il	O
resta	O
à	O
quai	O
dans	O
le	O
port	O
de	O
Gotenhafen	LOCATION
,	LOCATION
baie	LOCATION
de	LOCATION
Danzig	LOCATION
(	LOCATION
mer	LOCATION
Baltique	LOCATION
)	LOCATION
,	LOCATION
Prusse-Orientale	LOCATION
,	O
servant	O
de	O
logement	O
flottant	O
aux	O
troupes	O
de	O
la	O
Kriegsmarine	O
.	O
En	O
1942	O
--	O
1943	O
,	O
un	O
film	O
de	O
propagande	O
nazie	O
y	O
fut	O
tourné	O
,	O
Titanic	O
.	O
En	O
1944	O
,	O
il	O
reçut	O
l'	O
ordre	O
de	O
transporter	O
des	O
civils	O
et	O
des	O
soldats	O
entre	O
Gotenhafen	LOCATION
et	LOCATION
Copenhague	LOCATION
,	O
mais	O
ses	O
turbines	O
tombant	O
en	O
panne	O
,	O
il	O
fut	O
remorqué	O
et	O
réparé	O
dans	O
un	O
chantier	O
naval	O
scandinave	O
.	O
Dès	O
le	O
20	O
commence	O
l'	O
embarquement	O
sur	O
les	O
navires	O
de	O
11000	O
déportés	O
arrivés	O
à	O
Lübeck	LOCATION
entre	O
le	O
19	O
et	O
le	O
26	O
avril	O
,	O
malgré	O
les	O
protestations	O
de	O
la	O
Croix-Rouge	O
suédoise	O
.	O
En	O
plusieurs	O
voyages	O
jusqu'	O
au	O
30	O
avril	O
,	O
6500	O
déportés	O
et	O
600	O
gardes	O
sont	O
amenés	O
sur	O
le	O
Cap	O
Arcona	O
,	O
où	O
ils	O
survivent	O
dans	O
des	O
conditions	O
atroces	O
.	O
Mais	O
arrivés	O
du	O
camp	O
de	O
concentration	O
de	O
Stutthof	LOCATION
,	O
500	O
nouveaux	O
déportés	O
arrivent	O
à	O
Lübeck	LOCATION
.	O
Environ	O
7000	O
à	O
8000	O
déportés	O
périrent	O
noyés	O
,	O
les	O
survivants	O
nagèrent	O
dans	O
la	O
mer	O
Baltique	O
glaciale	O
puis	O
furent	O
mitraillés	O
par	O
les	O
SS	O
sur	O
la	O
plage	O
.	O
À	O
bord	O
des	O
deux	O
premiers	O
bateaux	O
,	O
plus	O
de	O
7500	O
déportés	O
des	O
camps	O
de	O
concentration	O
de	O
Neuengamme	O
près	O
de	O
Hambourg	LOCATION
et	O
de	O
Stutthof	O
près	O
de	O
Danzig	LOCATION
,	O
dont	O
une	O
moitié	O
étaient	O
des	O
prisonniers	O
de	O
guerre	O
russes	O
et	O
polonais	O
et	O
d'	O
autres	O
français	O
(	O
résistants	O
,	O
réfractaires	O
au	O
STO	O
,	O
anciens	O
STO	O
,	O
etc	O
.	O
Ces	O
bateaux	O
devaient	O
être	O
sortis	O
en	O
pleine	O
mer	O
puis	O
sabordés	O
,	O
noyant	O
tous	O
ceux	O
qui	O
étaient	O
à	O
bord	O
selon	O
l'	O
ordre	O
d'	O
Himmler	O
indiquant	O
à	O
tous	O
les	O
commandants	O
de	O
camps	O
de	O
concentration	O
qu'	O
aucun	O
déporté	O
ne	O
devait	O
tomber	O
vivant	O
entre	O
les	O
mains	O
de	O
l'	O
ennemi	O
.	O
À	O
une	O
courte	O
distance	O
de	O
là	O
,	O
le	O
liner	O
Deutschland	O
était	O
ancré	O
et	O
était	O
en	O
train	O
d'	O
être	O
converti	O
en	O
navire-hôpital	O
.	O
Les	O
capitaines	O
du	O
Cap	O
Arcona	O
et	O
du	O
Thielbek	O
firent	O
déployer	O
un	O
drapeau	O
blanc	O
.	O
Le	O
Deutschland	O
,	O
s'	O
embrasa	O
rapidement	O
,	O
la	O
quille	O
à	O
l'	O
air	O
et	O
coula	O
quatre	O
heures	O
plus	O
tard	O
.	O
Le	O
Cap	O
Arcona	O
,	O
avec	O
4650	O
déportés	O
piégés	O
en	O
dessous	O
dans	O
les	O
cales	O
profondes	O
suffoquèrent	O
dans	O
la	O
fumée	O
et	O
les	O
flammes	O
,	O
le	O
navire	O
s'	O
inclina	O
sur	O
un	O
côté	O
,	O
fut	O
en	O
partie	O
submergé	O
et	O
s'	O
embrasa	O
.	O
Quelques-uns	O
des	O
déportés	O
réussirent	O
à	O
s'	O
en	O
extraire	O
et	O
à	O
se	O
cramponner	O
à	O
la	O
coque	O
du	O
navire	O
,	O
d'	O
autres	O
sautèrent	O
dans	O
la	O
mer	O
Baltique	O
glaciale	O
.	O
Le	O
Thielbek	O
se	O
transforma	O
en	O
épave	O
où	O
l'	O
incendie	O
couvait	O
et	O
coula	O
quarante-cinq	O
minutes	O
plus	O
tard	O
.	O
Du	O
Thielbek	O
,	O
sur	O
2800	O
déportés	O
seulement	O
50	O
furent	O
sauvés	O
.	O
Beaucoup	O
de	O
survivants	O
,	O
essayant	O
de	O
nager	O
jusqu'	O
à	O
la	O
plage	O
,	O
furent	O
abattus	O
dans	O
l'	O
eau	O
par	O
les	O
mitrailleuses	O
des	O
Typhoons	O
volant	O
en	O
rase-mottes	O
et	O
tournant	O
autour	O
des	O
bateaux	O
,	O
d'	O
autres	O
par	O
les	O
unités	O
de	O
la	O
SS	O
,	O
des	O
Jeunesses	O
hitlériennes	O
,	O
de	O
l'	O
infanterie	O
de	O
marine	O
stationnées	O
sur	O
la	O
plage	O
.	O
Les	O
SS	O
en	O
barques	O
épargnèrent	O
uniquement	O
ceux	O
en	O
uniforme	O
SS	O
,	O
au	O
moins	O
400	O
.	O
Le	O
4	O
mai	O
1945	O
,	O
des	O
photos	O
des	O
épaves	O
échouées	O
(	O
Cap	O
Arcona	O
et	O
Thielbek	O
)	O
furent	O
prises	O
par	O
un	O
avion	O
de	O
reconnaissance	O
anglais.	O
.	O
En	O
tout	O
,	O
environ	O
8000	O
personnes	O
moururent	O
dans	O
cette	O
tragédie	O
(	O
7300	O
déportés	O
et	O
600	O
SS	O
)	O
,	O
314	O
déportés	O
et	O
2	O
membres	O
d'	O
équipage	O
furent	O
sauvés	O
.	O
Les	O
survivants	O
ont	O
fait	O
construire	O
un	O
cénotaphe	O
sur	O
lequel	O
est	O
inscrit	O
:	O
"	O
À	O
la	O
mémoire	O
éternelle	O
des	O
déportés	O
du	O
camp	O
de	O
concentration	O
de	O
Neuengamme	LOCATION
.	O
Ils	O
périrent	O
lors	O
du	O
naufrage	O
du	O
Cap	O
Arcona	O
le	O
3	O
mai	O
1945	O
"	O
.	O
Les	O
pilotes	O
de	O
la	O
RAF	O
ne	O
savaient	O
pas	O
qu'	O
il	O
y	O
avait	O
des	O
déportés	O
à	O
bord	O
et	O
ce	O
n'	O
est	O
que	O
beaucoup	O
d'	O
années	O
plus	O
tard	O
,	O
en	O
fait	O
en	O
1975	O
,	O
que	O
certains	O
apprirent	O
qu'	O
ils	O
avaient	O
massacré	O
des	O
déportés	O
des	O
camps	O
de	O
concentration	O
.	O
Pendant	O
des	O
semaines	O
après	O
le	O
naufrage	O
,	O
les	O
corps	O
des	O
victimes	O
furent	O
ramenés	O
par	O
le	O
courant	O
sur	O
la	O
plage	O
,	O
ils	O
furent	O
rassemblés	O
et	O
enterrés	O
dans	O
de	O
simples	O
fosses	O
communes	O
à	O
Neustadt	LOCATION
en	LOCATION
Holstein	LOCATION
.	O
L'	O
histoire	O
de	O
cette	O
tragédie	O
est	O
dépeinte	O
dans	O
le	O
musée	O
du	O
Cap	O
Arcona	O
de	O
la	O
ville	O
de	O
Neustadt	LOCATION
en	LOCATION
Holstein	LOCATION
ouvert	O
en	O
1990	O
.	O
Le	O
4	O
,	O
la	O
presse	O
britannique	O
titrait	O
sur	O
la	O
"	O
brillante	O
attaque	O
"	O
de	O
la	O
RAF	O
.	O
Ce	O
naufrage	O
fait	O
partie	O
,	O
avec	O
ceux	O
du	O
Wilhelm	O
Gustloff	O
et	O
du	O
Goya	O
,	O
qui	O
eurent	O
également	O
lieu	O
en	O
1945	O
dans	O
la	O
mer	O
Baltique	O
,	O
des	O
trois	O
plus	O
grandes	O
pertes	O
en	O
vies	O
humaines	O
de	O
l'	O
histoire	O
de	O
la	O
mer	O
.	O
Les	O
officiers	O
SS	O
de	O
Neuengamme	LOCATION
ont	O
été	O
jugés	O
par	O
des	O
tribunaux	O
militaires	O
britanniques	O
,	O
mais	O
aucun	O
des	O
responsables	O
du	O
transfert	O
vers	O
le	O
Cap	O
Arcona	O
ne	O
le	O
fut	O
.	O
Quant	O
au	O
navire	O
Cap	O
Arcona	O
,	O
son	O
épave	O
resta	O
échouée	O
dans	O
la	O
baie	O
jusqu'	O
en	O
1949	O
,	O
puis	O
fut	O
démantelée	O
pour	O
être	O
réduite	O
en	O
ferrailles	O
.	O
