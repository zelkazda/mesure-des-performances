Georg	PERSON
Simmel	PERSON
,	O
né	O
le	O
1	O
er	O
mars	O
1858	O
à	O
Berlin	LOCATION
en	O
Allemagne	O
et	O
mort	O
le	O
28	O
septembre	O
1918	O
à	O
Strasbourg	LOCATION
,	O
est	O
un	O
philosophe	O
et	O
sociologue	O
allemand	O
.	O
Simmel	PERSON
étudie	O
la	O
philosophie	O
et	O
l'	O
histoire	O
à	O
l'	O
Université	O
Friedrich-Wilhelm	O
de	O
Berlin	LOCATION
de	O
1876	O
à	O
1881	O
.	O
Ses	O
ouvrages	O
ne	O
lui	O
attirèrent	O
pas	O
non	O
plus	O
les	O
faveurs	O
de	O
ses	O
collègues	O
de	O
l'	O
université	O
de	O
Berlin	LOCATION
,	O
mais	O
suscitèrent	O
l'	O
intérêt	O
de	O
l'	O
élite	O
intellectuelle	O
berlinoise	O
.	O
La	O
sociologie	O
de	O
Georg	PERSON
Simmel	PERSON
se	O
caractérise	O
tout	O
d'	O
abord	O
par	O
l'	O
angle	O
d'	O
approche	O
particulier	O
qu'	O
elle	O
préconise	O
pour	O
étudier	O
le	O
vivre	O
ensemble	O
.	O
Pour	O
étudier	O
la	O
société	O
,	O
Simmel	O
nous	O
dit	O
qu'	O
il	O
faut	O
la	O
prendre	O
dans	O
son	O
acception	O
la	O
plus	O
large	O
,	O
c'	O
est-à-dire	O
,	O
"	O
là	O
où	O
il	O
y	O
a	O
action	O
réciproque	O
de	O
plusieurs	O
individus	O
"	O
,	O
le	O
terme	O
important	O
de	O
cette	O
définition	O
étant	O
réciproque	O
.	O
Ce	O
que	O
la	O
sociologie	O
doit	O
observer	O
,	O
ce	O
sont	O
les	O
liens	O
qui	O
existent	O
entre	O
les	O
individus	O
,	O
ce	O
qu'	O
il	O
appelle	O
la	O
socialisation	O
(	O
traduction	O
du	O
terme	O
allemand	O
employé	O
par	O
Simmel	PERSON
qui	O
ne	O
renvoie	O
pas	O
aux	O
théories	O
habituelles	O
de	O
la	O
socialisation	O
comme	O
transmission	O
sociale	O
.	O
Simmel	O
définit	O
le	O
contenu	O
de	O
socialisation	O
comme	O
Ainsi	O
,	O
Simmel	PERSON
dira	O
que	O
le	O
contenu	O
est	O
la	O
matière	O
de	O
la	O
socialisation	O
qui	O
est	O
elle-même	O
la	O
forme	O
que	O
prend	O
l'	O
action	O
réciproque	O
à	O
laquelle	O
le	O
contenu	O
donne	O
lieu	O
.	O
Simmel	O
nous	O
dit	O
que	O
pour	O
réussir	O
à	O
percer	O
les	O
mystères	O
de	O
l'	O
être	O
social	O
,	O
il	O
faut	O
partir	O
de	O
l'	O
étude	O
de	O
l'	O
atome	O
le	O
plus	O
petit	O
de	O
cette	O
réalité	O
:	O
l'	O
individu	O
.	O
L'	O
auteur	O
de	O
cette	O
introduction	O
,	O
reprenant	O
la	O
conception	O
de	O
Raymond	PERSON
Boudon	PERSON
,	O
nous	O
dit	O
que	O
le	O
concept	O
de	O
forme	O
est	O
un	O
synonyme	O
de	O
celui	O
de	O
modèle	O
,	O
fonctionnant	O
sur	O
la	O
même	O
logique	O
que	O
l'	O
idéal	O
type	O
weberien	O
.	O
Si	O
Simmel	PERSON
reconnaît	O
en	O
effet	O
que	O
la	O
sociologie	O
,	O
lorsqu'	O
elle	O
s'	O
exprime	O
sur	O
la	O
forme	O
de	O
certaines	O
interactions	O
ne	O
peut	O
que	O
"	O
poser	O
des	O
concepts	O
et	O
des	O
ensembles	O
de	O
concepts	O
dans	O
une	O
pureté	O
et	O
une	O
abstraction	O
totale	O
qui	O
n'	O
apparaît	O
jamais	O
dans	O
les	O
réalisations	O
historiques	O
de	O
ces	O
contenus	O
"	O
(	O
ibid.	O
,	O
p.	O
176	O
)	O
,	O
la	O
forme	O
d'	O
une	O
interaction	O
est	O
cependant	O
pour	O
lui	O
une	O
dimension	O
qui	O
avec	O
le	O
contenu	O
,	O
forme	O
la	O
totalité	O
de	O
l'	O
être	O
du	O
fait	O
social	O
.	O
Pour	O
illustrer	O
cette	O
seconde	O
interprétation	O
du	O
concept	O
de	O
forme	O
,	O
partons	O
de	O
l'	O
introduction	O
,	O
rédigée	O
par	O
Simmel	PERSON
à	O
son	O
livre	O
Philosophie	O
de	O
l'	O
argent	O
,	O
où	O
il	O
explique	O
ce	O
qu'	O
est	O
pour	O
lui	O
la	O
philosophie	O
.	O
Ce	O
texte	O
montre	O
en	O
effet	O
comment	O
Simmel	O
propose	O
d'	O
élaborer	O
une	O
ontologie	O
des	O
phénomènes	O
sociaux	O
Pour	O
Simmel	PERSON
,	O
la	O
philosophie	O
(	O
comme	O
toutes	O
sciences	O
ou	O
tout	O
art	O
)	O
doit	O
être	O
"	O
entendue	O
comme	O
interprétation	O
,	O
coloration	O
,	O
accentuation	O
sélective	O
du	O
réel	O
par	O
l'	O
individu	O
"	O
(	O
ibid.	O
,	O
p.	O
14	O
)	O
.	O
On	O
voit	O
dans	O
cette	O
phrase	O
en	O
quoi	O
la	O
philosophie	O
de	O
même	O
que	O
la	O
sociologie	O
de	O
Simmel	PERSON
peut	O
être	O
traitée	O
de	O
relativiste	O
.	O
Simmel	O
cherche	O
à	O
"	O
déployer	O
la	O
structure	O
et	O
l'	O
idée	O
[	O
du	O
phénomène	O
historique	O
de	O
l'	O
argent	O
]	O
en	O
partant	O
des	O
sentiments	O
de	O
valeur	O
,	O
de	O
la	O
praxis	O
envers	O
les	O
choses	O
,	O
et	O
des	O
relations	O
interhumaines	O
de	O
réciprocité	O
vues	O
comme	O
leurs	O
présupposés	O
"	O
(	O
ibid.	O
,	O
p.	O
14	O
)	O
.	O
Pour	O
résumer	O
,	O
Simmel	O
nous	O
dit	O
qu'	O
une	O
philosophie	O
de	O
l'	O
argent	O
doit	O
comporter	O
une	O
phase	O
dite	O
analytique	O
,	O
loin	O
devant	O
le	O
champ	O
de	O
la	O
science	O
économique	O
de	O
l'	O
argent	O
,	O
qui	O
doit	O
:	O
"	O
éclairer	O
l'	O
essence	O
de	O
l'	O
argent	O
à	O
partir	O
des	O
conditions	O
et	O
relations	O
de	O
la	O
vie	O
générale	O
"	O
(	O
p.	O
15	O
)	O
;	O
et	O
une	O
phase	O
dite	O
synthétique	O
,	O
loin	O
derrière	O
le	O
champ	O
de	O
la	O
science	O
économique	O
,	O
qui	O
doit	O
"	O
[	O
éclairer	O
]	O
,	O
inversement	O
,	O
l'	O
essence	O
de	O
la	O
vie	O
générale	O
et	O
son	O
modelage	O
à	O
partir	O
de	O
l'	O
influence	O
de	O
l'	O
argent	O
"	O
(	O
p.	O
15	O
)	O
.	O
Au	O
final	O
donc	O
,	O
l'	O
argent	O
,	O
pour	O
Simmel	PERSON
,	O
n'	O
est	O
que	O
"	O
le	O
moyen	O
,	O
le	O
matériau	O
ou	O
l'	O
exemple	O
nécessaires	O
pour	O
présenter	O
les	O
rapports	O
qui	O
existent	O
entre	O
d'	O
une	O
part	O
les	O
phénomènes	O
les	O
plus	O
extérieurs	O
,	O
les	O
plus	O
réalistes	O
,	O
les	O
plus	O
accidentels	O
,	O
et	O
d'	O
autre	O
part	O
les	O
potentialités	O
les	O
plus	O
idéelles	O
de	O
l'	O
existence	O
,	O
les	O
courants	O
les	O
plus	O
profonds	O
de	O
la	O
vie	O
individuelle	O
et	O
de	O
l'	O
histoire	O
.	O
Il	O
s'	O
agit	O
pour	O
Simmel	O
de	O
"	O
déceler	O
dans	O
chaque	O
détail	O
de	O
la	O
vie	O
le	O
sens	O
global	O
de	O
celle-ci	O
"	O
.	O
Simmel	O
créé	O
une	O
nouvelle	O
vision	O
des	O
choses	O
matérielles	O
:	O
L'	O
opposition	O
entre	O
une	O
philosophie	O
réaliste	O
ou	O
idéaliste	O
ne	O
tient	O
pas	O
la	O
route	O
pour	O
Simmel	PERSON
.	O
C'	O
est	O
à	O
partir	O
de	O
cette	O
idée	O
que	O
Simmel	O
va	O
construire	O
son	O
ouvrage	O
sur	O
l'	O
argent	O
,	O
en	O
le	O
coupant	O
en	O
deux	O
parties	O
.	O
Il	O
faut	O
cependant	O
concéder	O
que	O
le	O
concept	O
de	O
forme	O
de	O
Simmel	O
est	O
loin	O
d'	O
être	O
des	O
plus	O
clairs	O
.	O
Cette	O
distinction	O
entre	O
en	O
interaction	O
avec	O
le	O
concept	O
de	O
forme	O
parce	O
que	O
selon	O
Simmel	PERSON
,	O
certaines	O
formes	O
,	O
qui	O
sont	O
parfois	O
appelées	O
,	O
pour	O
les	O
différencier	O
des	O
formes	O
plus	O
fugaces	O
,	O
formes	O
sociales	O
,	O
se	O
retrouvent	O
dans	O
la	O
culture	O
objective	O
.	O
Ce	O
qui	O
mène	O
à	O
l'	O
existence	O
de	O
ce	O
phénomène	O
infini	O
de	O
réciprocité	O
entre	O
le	O
monde	O
idéel	O
et	O
le	O
monde	O
matériel	O
que	O
décrit	O
Simmel	PERSON
quand	O
il	O
parle	O
de	O
l'	O
argent	O
.	O
Dans	O
ce	O
premier	O
extrait	O
issue	O
du	O
chapitre	O
6	O
de	O
Philosophie	O
de	O
l'	O
argent	O
,	O
Simmel	O
nous	O
parle	O
de	O
trois	O
formes	O
sociales	O
qui	O
selon	O
lui	O
se	O
sont	O
fortement	O
autonomisées	O
avec	O
la	O
modernité	O
(	O
on	O
pourrait	O
même	O
dire	O
que	O
selon	O
notre	O
auteur	O
,	O
l'	O
autonomisation	O
de	O
ces	O
trois	O
formes	O
est	O
l'	O
élément	O
constitutif	O
de	O
la	O
modernité	O
)	O
.	O
Simmel	O
va	O
nous	O
dire	O
que	O
ces	O
trois	O
formes	O
en	O
s'	O
autonomisant	O
des	O
individus	O
pour	O
devenir	O
un	O
élément	O
de	O
la	O
culture	O
objective	O
vont	O
obtenir	O
le	O
pouvoir	O
de	O
déterminer	O
des	O
formes	O
d'	O
interaction	O
.	O
