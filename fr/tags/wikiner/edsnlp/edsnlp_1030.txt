Le	O
domaine	O
des	O
radiocommunications	O
est	O
réglementé	O
par	O
l'	O
Union	O
internationale	O
des	O
télécommunications	O
(	O
UIT	O
)	O
qui	O
a	O
établi	O
un	O
règlement	O
des	O
radiocommunications	O
dans	O
lequel	O
on	O
peut	O
lire	O
la	O
définition	O
suivante	O
:	O
Selon	O
la	O
définition	O
de	O
l'	O
UIT	O
,	O
le	O
terme	O
"	O
hertzien	O
"	O
ne	O
couvre	O
que	O
les	O
signaux	O
transmis	O
par	O
rayonnement	O
--	O
il	O
s'	O
agit	O
là	O
du	O
rayonnement	O
électromagnétique	O
--	O
c'	O
est-à-dire	O
sans	O
support	O
matériel	O
,	O
par	O
exemple	O
aussi	O
bien	O
la	O
télévision	O
terrestre	O
que	O
par	O
satellite	O
et	O
tous	O
les	O
autres	O
modes	O
de	O
transmission	O
sans	O
fil	O
dans	O
le	O
spectre	O
de	O
fréquence	O
de	O
ces	O
ondes	O
,	O
.	O
