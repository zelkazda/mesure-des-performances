Par	O
son	O
jeu	O
de	O
mime	O
et	O
de	O
clownerie	O
,	O
il	O
a	O
su	O
se	O
faire	O
remarquer	O
,	O
et	O
devenir	O
l'	O
un	O
des	O
plus	O
célèbres	O
acteurs	O
d'	O
Hollywood	O
.	O
Charlie	PERSON
Chaplin	PERSON
fut	O
l'	O
une	O
des	O
personnes	O
les	O
plus	O
créatives	O
de	O
l'	O
ère	O
du	O
cinéma	O
muet	O
.	O
Réalisateur	O
,	O
scénariste	O
,	O
producteur	O
,	O
monteur	O
,	O
et	O
même	O
compositeur	O
de	O
ses	O
films	O
,	O
sa	O
carrière	O
durera	O
plus	O
de	O
soixante-cinq	O
ans	O
,	O
du	O
music-hall	O
en	O
Angleterre	O
,	O
jusqu'	O
à	O
sa	O
mort	O
,	O
en	O
Suisse	O
.	O
Il	O
fut	O
fortement	O
inspiré	O
par	O
l'	O
acteur	O
burlesque	O
français	O
Max	PERSON
Linder	PERSON
:	O
tous	O
deux	O
choisiront	O
un	O
costume	O
bien	O
à	O
eux	O
.	O
Mais	O
Max	PERSON
Linder	PERSON
,	O
au	O
contraire	O
de	O
Charlie	PERSON
Chaplin	PERSON
,	O
ne	O
se	O
fera	O
pas	O
représenter	O
comme	O
une	O
victime	O
de	O
la	O
société	O
.	O
La	O
vie	O
publique	O
et	O
privée	O
de	O
Charlie	PERSON
Chaplin	PERSON
fera	O
l'	O
objet	O
d'	O
adulation	O
,	O
comme	O
de	O
controverse	O
.	O
Il	O
n'	O
a	O
qu'	O
un	O
an	O
lorsque	O
son	O
père	O
part	O
en	O
tournée	O
aux	O
États-Unis	O
.	O
Deux	O
mois	O
plus	O
tard	O
,	O
la	O
mère	O
de	O
Chaplin	PERSON
obtient	O
son	O
congé	O
de	O
l'	O
hôpital	O
.	O
Pendant	O
ce	O
temps	O
,	O
Charlie	PERSON
vécut	O
avec	O
son	O
père	O
et	O
sa	O
belle-mère	O
alcoolique	O
,	O
dans	O
un	O
environnement	O
intenable	O
pour	O
un	O
enfant	O
,	O
dont	O
les	O
souvenirs	O
inspireront	O
Le	O
Kid	O
.	O
À	O
cinq	O
ans	O
,	O
Chaplin	PERSON
monte	O
sur	O
scène	O
pour	O
remplacer	O
au	O
pied	O
levé	O
sa	O
mère	O
qui	O
ne	O
peut	O
plus	O
chanter	O
,	O
victime	O
d'	O
une	O
extinction	O
de	O
voix	O
.	O
Le	O
frère	O
de	O
Charlie	PERSON
,	O
Sydney	PERSON
,	O
quitte	O
le	O
foyer	O
parental	O
pour	O
travailler	O
dans	O
la	O
marine	O
.	O
Puis	O
,	O
il	O
obtient	O
à	O
partir	O
de	O
1903	O
une	O
succession	O
de	O
contrats	O
au	O
théâtre	O
,	O
et	O
en	O
1908	O
,	O
il	O
est	O
engagé	O
dans	O
la	O
troupe	O
de	O
Fred	PERSON
Karno	PERSON
,	O
alors	O
le	O
plus	O
important	O
impresario	O
de	O
spectacles	O
avec	O
des	O
sketches	O
.	O
Il	O
y	O
rencontre	O
le	O
futur	O
Stan	PERSON
Laurel	PERSON
.	O
Au	O
cours	O
d'	O
une	O
tournée	O
de	O
la	O
troupe	O
en	O
Amérique	O
,	O
les	O
studios	O
Keystone	O
lui	O
adressent	O
une	O
proposition	O
de	O
contrat	O
qu'	O
il	O
accepte	O
:	O
l'	O
aventure	O
cinématographique	O
commence	O
.	O
Ne	O
supportant	O
pas	O
les	O
pressions	O
dues	O
à	O
ces	O
temps	O
très	O
brefs	O
,	O
Chaplin	PERSON
s'	O
adapte	O
très	O
mal	O
aux	O
conditions	O
de	O
travail	O
de	O
la	O
compagnie	O
,	O
à	O
tel	O
point	O
que	O
les	O
incidents	O
avec	O
les	O
metteurs	O
en	O
scène	O
sont	O
fréquents	O
.	O
Sur	O
les	O
ordres	O
de	O
Mack	PERSON
Sennett	PERSON
qui	O
lui	O
demande	O
de	O
se	O
créer	O
un	O
maquillage	O
au	O
pied	O
levé	O
,	O
il	O
crée	O
en	O
1914	O
le	O
personnage	O
raffiné	O
de	O
Charlot	O
le	O
vagabond	O
,	O
et	O
recentre	O
tout	O
son	O
comique	O
autour	O
du	O
nouveau	O
personnage	O
et	O
de	O
sa	O
silhouette	O
qu'	O
il	O
inaugure	O
dans	O
Charlot	O
est	O
content	O
de	O
lui	O
(	O
1914	O
)	O
.	O
En	O
1916	O
,	O
il	O
signe	O
un	O
contrat	O
de	O
distribution	O
d'	O
un	O
million	O
de	O
dollars	O
avec	O
la	O
First	O
National	O
,	O
qui	O
lui	O
laisse	O
la	O
production	O
et	O
la	O
propriété	O
de	O
huit	O
films	O
prévus	O
.	O
Il	O
fait	O
alors	O
immédiatement	O
construire	O
son	O
propre	O
studio	O
dans	O
lequel	O
il	O
réalise	O
neuf	O
films	O
dont	O
Une	O
vie	O
de	O
chien	O
,	O
Le	O
Kid	O
et	O
Charlot	O
soldat	O
.	O
En	O
1919	O
,	O
un	O
vent	O
de	O
révolte	O
souffle	O
sur	O
Hollywood	O
où	O
les	O
acteurs	O
et	O
cinéastes	O
se	O
déclarent	O
exploités	O
;	O
Chaplin	PERSON
s'	O
associe	O
alors	O
à	O
David	PERSON
Wark	PERSON
Griffith	PERSON
,	O
Mary	PERSON
Pickford	PERSON
et	O
Douglas	PERSON
Fairbanks	PERSON
pour	O
fonder	O
la	O
United	O
Artists	O
.	O
Son	O
premier	O
film	O
pour	O
sa	O
nouvelle	O
firme	O
sera	O
L'	O
Opinion	O
publique	O
(	O
1923	O
)	O
.	O
Puis	O
,	O
Chaplin	PERSON
fait	O
peu	O
à	O
peu	O
entrer	O
dans	O
son	O
univers	O
comique	O
celui	O
du	O
mélodrame	O
et	O
de	O
la	O
réalité	O
sociale	O
comme	O
dans	O
La	O
Ruée	O
vers	O
l'	O
or	O
(	O
1925	O
)	O
.	O
Les	O
Lumières	O
de	O
la	O
ville	O
(	O
1931	O
)	O
est	O
le	O
premier	O
film	O
à	O
en	O
bénéficier	O
,	O
mais	O
de	O
manière	O
très	O
ironique	O
.	O
Chaplin	PERSON
souffle	O
pendant	O
des	O
heures	O
dans	O
un	O
vieux	O
saxophone	O
afin	O
de	O
parodier	O
les	O
imperfections	O
du	O
parlant	O
lors	O
de	O
la	O
scène	O
d'	O
ouverture	O
du	O
film	O
.	O
De	O
plus	O
Chaplin	PERSON
ne	O
se	O
détourne	O
pas	O
de	O
son	O
projet	O
initial	O
de	O
film	O
muet	O
.	O
Un	O
film	O
dialogué	O
a	O
une	O
audience	O
un	O
peu	O
plus	O
limitée	O
car	O
il	O
contient	O
la	O
barrière	O
de	O
la	O
langue	O
et	O
Chaplin	PERSON
veut	O
s'	O
adresser	O
à	O
tous	O
.	O
On	O
le	O
dit	O
fini	O
,	O
à	O
l'	O
instar	O
de	O
ses	O
amis	O
David	PERSON
Wark	O
Griffith	PERSON
,	O
Mary	PERSON
Pickford	PERSON
et	O
Douglas	PERSON
Fairbanks	PERSON
et	O
de	O
bien	O
d'	O
autres	O
vedettes	O
du	O
muet	O
qui	O
n'	O
ont	O
pas	O
survécu	O
au	O
parlant	O
.	O
Il	O
entreprend	O
un	O
long	O
voyage	O
,	O
qui	O
va	O
durer	O
plus	O
d'	O
un	O
an	O
et	O
demi	O
,	O
à	O
travers	O
le	O
monde	O
,	O
en	O
Europe	O
notamment	O
,	O
pour	O
présenter	O
son	O
film	O
.	O
Il	O
rencontre	O
la	O
plupart	O
des	O
chefs	O
d'	O
états	O
et	O
de	O
nombreuses	O
personnalités	O
,	O
parmi	O
lesquelles	O
Albert	O
Einstein	O
.	O
Il	O
conjugue	O
tout	O
cela	O
dans	O
Les	O
Temps	O
modernes	O
(	O
1936	O
)	O
,	O
le	O
dernier	O
film	O
muet	O
de	O
l'	O
histoire	O
et	O
l'	O
un	O
des	O
plus	O
célèbres	O
,	O
sinon	O
le	O
plus	O
célèbre	O
,	O
de	O
son	O
auteur	O
.	O
Le	O
personnage	O
joué	O
par	O
Paulette	PERSON
Goddard	PERSON
les	O
lui	O
copie	O
sur	O
ses	O
manchettes	O
.	O
Ce	O
film	O
est	O
également	O
l'	O
ultime	O
apparition	O
à	O
l'	O
écran	O
du	O
personnage	O
Charlot	PERSON
.	O
Il	O
parle	O
aussi	O
de	O
la	O
difficulté	O
du	O
travail	O
à	O
la	O
chaîne	O
qui	O
rend	O
fou	O
la	O
plupart	O
des	O
employés	O
,	O
dont	O
le	O
personnage	O
interprété	O
par	O
Chaplin	PERSON
,	O
ce	O
qui	O
lui	O
vaut	O
un	O
passage	O
à	O
l'	O
hôpital	O
psychiatrique	O
dans	O
le	O
film	O
.	O
Mais	O
le	O
cinéaste	O
reçoit	O
le	O
soutien	O
du	O
président	O
Franklin	O
Roosevelt	O
,	O
lequel	O
l'	O
invitera	O
,	O
quelques	O
semaines	O
après	O
la	O
sortie	O
du	O
film	O
,	O
à	O
la	O
Maison	O
Blanche	O
,	O
pour	O
s'	O
entendre	O
réciter	O
le	O
discours	O
final	O
.	O
Le	O
film	O
est	O
interdit	O
sur	O
tout	O
le	O
continent	O
Européen	O
[	O
réf.	O
nécessaire	O
]	O
,	O
mais	O
une	O
rumeur	O
circule	O
:	O
Hitler	O
l'	O
aurait	O
vu	O
,	O
en	O
projection	O
privée	O
[	O
réf.	O
nécessaire	O
]	O
.	O
En	O
France	O
,	O
il	O
ne	O
sortira	O
qu'	O
en	O
1945	O
.	O
Cette	O
fois-ci	O
,	O
Chaplin	O
est	O
définitivement	O
entré	O
dans	O
l'	O
ère	O
du	O
cinéma	O
sonore	O
...	O
et	O
signe	O
l'	O
arrêt	O
de	O
mort	O
du	O
petit	O
vagabond	O
.	O
En	O
1946	O
,	O
Chaplin	O
tourne	O
son	O
film	O
le	O
plus	O
dur	O
,	O
Monsieur	O
Verdoux	O
.	O
Orson	PERSON
Welles	PERSON
propose	O
à	O
Chaplin	PERSON
un	O
scénario	O
basé	O
sur	O
l'	O
affaire	O
Landru	PERSON
.	O
Une	O
fois	O
encore	O
,	O
Chaplin	O
livre	O
un	O
message	O
empreint	O
de	O
cynisme	O
mais	O
également	O
d'	O
humanisme	O
.	O
En	O
1950	O
,	O
il	O
vend	O
la	O
quasi-totalité	O
de	O
ses	O
parts	O
à	O
la	O
United	O
Artists	O
et	O
travaille	O
aux	O
Feux	O
de	O
la	O
Rampe	O
où	O
il	O
décrit	O
la	O
triste	O
fin	O
d'	O
un	O
clown	O
dans	O
le	O
Londres	O
de	O
son	O
enfance	O
.	O
Ses	O
propres	O
enfants	O
apparaissent	O
comme	O
figurants	O
et	O
Chaplin	PERSON
tient	O
le	O
premier	O
rôle	O
.	O
Le	O
film	O
sort	O
en	O
1952	O
à	O
Londres	LOCATION
et	O
vaut	O
un	O
triomphe	O
à	O
son	O
auteur	O
.	O
L'	O
une	O
des	O
plus	O
belles	O
scènes	O
du	O
film	O
se	O
trouve	O
vers	O
la	O
fin	O
:	O
Buster	O
Keaton	O
joue	O
un	O
pianiste	O
et	O
Chaplin	O
un	O
violoniste	O
.	O
Mais	O
rien	O
ne	O
se	O
déroule	O
comme	O
prévu	O
car	O
Keaton	O
a	O
des	O
problèmes	O
avec	O
ses	O
partitions	O
et	O
son	O
piano	O
et	O
Chaplin	O
doit	O
se	O
battre	O
avec	O
les	O
cordes	O
de	O
son	O
violon	O
.	O
Victime	O
du	O
maccarthisme	O
(	O
son	O
nom	O
figure	O
sur	O
la	O
"	O
liste	O
noire	O
"	O
)	O
,	O
il	O
est	O
harcelé	O
par	O
le	O
FBI	O
en	O
raison	O
de	O
ses	O
opinions	O
de	O
gauche	O
(	O
pour	O
sa	O
part	O
,	O
il	O
se	O
présentait	O
comme	O
un	O
"	O
citoyen	O
du	O
monde	O
"	O
)	O
.	O
Pour	O
cette	O
raison	O
,	O
il	O
se	O
voit	O
refuser	O
le	O
visa	O
de	O
retour	O
lors	O
de	O
son	O
séjour	O
en	O
Europe	O
pour	O
la	O
présentation	O
de	O
son	O
film	O
.	O
Il	O
renonce	O
alors	O
à	O
sa	O
résidence	O
aux	O
États-Unis	O
et	O
installe	O
sa	O
famille	O
en	O
Suisse	O
jusqu'	O
à	O
la	O
fin	O
de	O
ses	O
jours	O
.	O
En	O
1967	O
,	O
il	O
tourne	O
son	O
dernier	O
film	O
,	O
cette	O
fois-ci	O
en	O
couleur	O
,	O
La	O
Comtesse	O
de	O
Hong-Kong	O
,	O
avec	O
Sophia	PERSON
Loren	PERSON
,	O
Marlon	PERSON
Brando	PERSON
et	O
Tippi	PERSON
Hedren	PERSON
,	O
dont	O
l'	O
action	O
se	O
déroule	O
sur	O
un	O
paquebot	O
et	O
où	O
il	O
ne	O
tient	O
qu'	O
un	O
petit	O
rôle	O
:	O
celui	O
d'	O
un	O
steward	O
victime	O
du	O
mal	O
de	O
mer	O
.	O
De	O
nombreuses	O
demandes	O
de	O
rançon	O
plus	O
ou	O
moins	O
farfelues	O
sont	O
adressées	O
à	O
la	O
famille	O
Chaplin	PERSON
.	O
Charlie	PERSON
Chaplin	PERSON
a	O
été	O
marié	O
à	O
4	O
reprises	O
;	O
Mildred	PERSON
Harris	PERSON
,	O
Lita	PERSON
Grey	PERSON
et	O
Paulette	PERSON
Goddard	PERSON
étaient	O
toutes	O
trois	O
ses	O
partenaires	O
à	O
l'	O
écran	O
.	O
L'	O
aventure	O
de	O
Charlie	PERSON
Chaplin	PERSON
avec	O
Lita	PERSON
Grey	PERSON
aurait	O
inspiré	O
Vladimir	PERSON
Nabokov	PERSON
pour	O
son	O
roman	O
Lolita	O
.	O
Ses	O
mariages	O
ont	O
défrayé	O
la	O
chronique	O
américaine	O
,	O
en	O
effet	O
il	O
a	O
29	O
ans	O
quand	O
il	O
se	O
marie	O
avec	O
Mildred	PERSON
Harris	PERSON
,	O
qui	O
en	O
a	O
17	O
;	O
il	O
en	O
a	O
35	O
quand	O
il	O
épouse	O
Lita	PERSON
Grey	PERSON
qui	O
a	O
16	O
ans	O
;	O
il	O
a	O
47	O
ans	O
quand	O
il	O
convole	O
avec	O
Paulette	PERSON
Goddard	PERSON
qui	O
en	O
a	O
25	O
;	O
il	O
a	O
54	O
ans	O
lors	O
de	O
son	O
mariage	O
avec	O
Oona	PERSON
O'Neill	PERSON
qui	O
en	O
a	O
18	O
.	O
Cependant	O
,	O
avec	O
l'	O
arrivée	O
du	O
parlant	O
,	O
Chaplin	O
a	O
dû	O
faire	O
un	O
choix	O
et	O
opérer	O
un	O
passage	O
du	O
muet	O
au	O
sonore	O
,	O
puis	O
au	O
parlant	O
.	O
C'	O
est	O
dans	O
Les	O
Lumières	O
de	O
la	O
ville	O
que	O
Chaplin	O
débute	O
ce	O
passage	O
au	O
sonore	O
.	O
Cependant	O
,	O
comme	O
le	O
dit	O
Michel	PERSON
Chion	PERSON
,	O
il	O
s'	O
agit	O
tout	O
de	O
même	O
d'	O
un	O
"	O
véritable	O
manifeste	O
pour	O
la	O
défense	O
du	O
muet	O
"	O
.	O
Or	O
,	O
s'	O
il	O
y	O
a	O
une	O
chose	O
qui	O
n'	O
est	O
pas	O
sonore	O
,	O
c'	O
est	O
bien	O
le	O
moment	O
où	O
le	O
bruit	O
de	O
la	O
portière	O
fait	O
croire	O
à	O
la	O
jeune	O
aveugle	O
que	O
Charlie	PERSON
est	O
un	O
millionnaire	O
--	O
gag	O
qui	O
a	O
nécessité	O
plusieurs	O
mois	O
d'	O
élaboration	O
,	O
et	O
plusieurs	O
interruptions	O
de	O
tournage	O
.	O
De	O
plus	O
,	O
lorsqu'	O
un	O
homme	O
mange	O
le	O
savon	O
de	O
Charlie	PERSON
et	O
que	O
celui-ci	O
se	O
met	O
à	O
le	O
disputer	O
,	O
tout	O
ce	O
qui	O
sort	O
de	O
sa	O
bouche	O
est	O
des	O
bulles	O
de	O
savon	O
,	O
comme	O
si	O
toute	O
parole	O
était	O
vaine	O
.	O
Lorsque	O
Chaplin	O
débute	O
le	O
tournage	O
des	O
Temps	O
Modernes	O
(	O
1936	O
)	O
en	O
parlant	O
,	O
il	O
se	O
rend	O
compte	O
bien	O
vite	O
qu'	O
il	O
s'	O
y	O
perd	O
.	O
La	O
seule	O
fois	O
où	O
on	O
entend	O
réellement	O
un	O
personnage	O
parler	O
"	O
en	O
direct	O
"	O
est	O
également	O
la	O
première	O
fois	O
où	O
l'	O
on	O
entend	O
la	O
voix	O
de	O
Chaplin	PERSON
.	O
Cependant	O
,	O
même	O
si	O
celui-ci	O
essaie	O
d'	O
avoir	O
un	O
langage	O
articulé	O
,	O
il	O
baragouine	O
,	O
ayant	O
oublié	O
les	O
paroles	O
de	O
sa	O
chanson	O
:	O
"	O
c'	O
est	O
comme	O
le	O
langage	O
à	O
la	O
naissance	O
"	O
,	O
langage	O
que	O
Chaplin	O
développera	O
dans	O
les	O
prochains	O
films	O
.	O
Dans	O
Le	O
Dictateur	O
,	O
contrairement	O
aux	O
Lumières	O
de	O
la	O
ville	O
,	O
le	O
titre	O
fait	O
appel	O
au	O
monde	O
de	O
la	O
parole	O
.	O
Même	O
si	O
le	O
film	O
est	O
presque	O
entièrement	O
parlant	O
et	O
renonce	O
définitivement	O
aux	O
cartons	O
du	O
muet	O
,	O
Chaplin	O
ne	O
renonce	O
pas	O
encore	O
au	O
langage	O
de	O
la	O
pantomime	O
.	O
Chaplin	O
s'	O
engage	O
politiquement	O
dans	O
certaines	O
de	O
ses	O
œuvres	O
,	O
véritables	O
satires	O
de	O
la	O
société	O
des	O
années	O
1930	O
.	O
Des	O
films	O
comme	O
Les	O
Temps	O
modernes	O
ou	O
Le	O
Dictateur	O
sont	O
respectivement	O
une	O
critique	O
de	O
la	O
société	O
de	O
consommation	O
de	O
masse	O
et	O
du	O
travail	O
à	O
la	O
chaîne	O
,	O
et	O
une	O
critique	O
des	O
régimes	O
politiques	O
dictatoriaux	O
et	O
fascistes	O
qui	O
s'	O
installent	O
en	O
Europe	O
.	O
On	O
peut	O
donc	O
affirmer	O
l'	O
engagement	O
politique	O
de	O
Charlie	PERSON
Chaplin	PERSON
dans	O
la	O
société	O
de	O
son	O
époque	O
.	O
Accusé	O
de	O
prendre	O
des	O
positions	O
communistes	O
aux	O
États-Unis	O
ce	O
que	O
lui	O
a	O
valu	O
des	O
enquêtes	O
du	O
FBI	O
,	O
il	O
fut	O
une	O
des	O
victimes	O
du	O
maccarthisme	O
au	O
début	O
des	O
années	O
1950	O
et	O
inscrit	O
sur	O
la	O
liste	O
noire	O
du	O
cinéma	O
.	O
Ce	O
fut	O
l'	O
une	O
des	O
causes	O
de	O
son	O
exil	O
en	O
Suisse	O
.	O
Il	O
n'	O
existe	O
aucune	O
indication	O
d'	O
une	O
ascendance	O
juive	O
de	O
Chaplin	PERSON
,	O
cependant	O
tout	O
au	O
long	O
de	O
sa	O
carrière	O
,	O
il	O
y	O
eut	O
des	O
controverses	O
sur	O
ses	O
possibles	O
origines	O
juives	O
.	O
Dans	O
les	O
années	O
1930	O
,	O
la	O
propagande	O
nazie	O
l'	O
a	O
constamment	O
déclaré	O
juif	O
en	O
se	O
fondant	O
sur	O
des	O
articles	O
publiés	O
antérieurement	O
dans	O
la	O
presse	O
américaine	O
;	O
les	O
enquêtes	O
du	O
FBI	O
sur	O
Chaplin	PERSON
à	O
la	O
fin	O
des	O
années	O
1940	O
ont	O
également	O
mis	O
l'	O
accent	O
sur	O
ses	O
origines	O
ethniques	O
.	O
Durant	O
toute	O
son	O
existence	O
,	O
Chaplin	PERSON
a	O
farouchement	O
refusé	O
de	O
contester	O
ou	O
de	O
réfuter	O
les	O
déclarations	O
affirmant	O
qu'	O
il	O
était	O
juif	O
,	O
en	O
disant	O
que	O
ce	O
serait	O
"	O
faire	O
directement	O
le	O
jeu	O
des	O
antisémites	O
"	O
.	O
Comme	O
Orson	PERSON
Welles	O
,	O
Alfred	PERSON
Hitchcock	PERSON
,	O
ou	O
Cary	PERSON
Grant	PERSON
,	O
Charlie	PERSON
Chaplin	PERSON
n'	O
a	O
jamais	O
reçu	O
la	O
célèbre	O
statuette	O
,	O
sinon	O
le	O
prix	O
honorifique	O
.	O
Il	O
a	O
toutefois	O
reçu	O
un	O
Oscar	O
de	O
la	O
meilleure	O
musique	O
de	O
film	O
en	O
1952	O
pour	O
Les	O
Feux	O
de	O
la	O
rampe	O
(	O
qui	O
est	O
le	O
seul	O
film	O
réunissant	O
Charlie	PERSON
Chaplin	PERSON
et	O
Buster	PERSON
Keaton	PERSON
)	O
.	O
Avant	O
cette	O
consécration	O
,	O
il	O
avait	O
été	O
nommé	O
comme	O
meilleur	O
acteur	O
,	O
meilleur	O
réalisateur	O
pour	O
le	O
film	O
Le	O
Cirque	O
.	O
