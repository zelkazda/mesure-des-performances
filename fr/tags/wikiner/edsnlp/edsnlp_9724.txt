Les	O
villes	O
les	O
plus	O
proches	O
sont	O
Bonn	LOCATION
,	O
à	O
près	O
de	O
60	O
km	O
en	O
aval	O
du	O
Rhin	O
,	O
Mayence	LOCATION
,	O
à	O
90	O
km	O
en	O
amont	O
du	O
Rhin	O
,	O
et	O
Ludwigshafen	LOCATION
.	O
Coblence	LOCATION
est	O
la	O
troisième	O
ville	O
de	O
la	O
Rhénanie-Palatinat	O
et	O
le	O
siège	O
de	O
la	O
conurbation	O
Mayen-Coblence	O
.	O
Coblence	LOCATION
se	O
trouve	O
dans	O
une	O
région	O
appelée	O
Deutsches	O
Eck	O
,	O
littéralement	O
le	O
coin	O
allemand	O
,	O
au	O
confluent	O
de	O
la	O
Moselle	O
et	O
du	O
Rhin	O
.	O
Les	O
quartiers	O
de	O
la	O
ville	O
situés	O
sur	O
la	O
rive	O
droite	O
du	O
Rhin	O
se	O
trouvent	O
à	O
la	O
bordure	O
de	O
la	O
forêt	O
du	O
Westerwald	O
,	O
qui	O
plonge	O
en	O
partie	O
jusqu'	O
au	O
fleuve	O
.	O
Les	O
quartiers	O
situés	O
entre	O
le	O
Rhin	O
et	O
la	O
Moselle	O
sont	O
en	O
partie	O
plantés	O
d'	O
une	O
forêt	O
mixte	O
qui	O
constitue	O
le	O
poumon	O
vert	O
de	O
la	O
ville	O
et	O
de	O
sa	O
banlieue	O
proche	O
.	O
Coblence	LOCATION
se	O
trouve	O
dans	O
une	O
zone	O
dite	O
tempérée	O
,	O
avec	O
un	O
climat	O
froid	O
et	O
un	O
vent	O
d'	O
ouest	O
dominant	O
.	O
Ces	O
vents	O
venus	O
de	O
l'	O
Atlantique	O
et	O
de	O
la	O
mer	O
du	O
Nord	O
apportent	O
chaque	O
année	O
près	O
de	O
700	O
mm	O
de	O
précipitations	O
dans	O
le	O
bas	O
pays	O
de	O
l'	O
Allemagne	O
occidentale	O
ainsi	O
que	O
dans	O
la	O
vallée	O
rhénane	O
.	O
En	O
raison	O
de	O
son	O
appartenance	O
à	O
l'	O
électorat	O
de	O
Trèves	LOCATION
,	O
la	O
ville	O
a	O
été	O
administrée	O
pendant	O
des	O
siècles	O
par	O
les	O
archevêques	O
catholiques	O
qui	O
en	O
firent	O
leur	O
lieu	O
de	O
résidence	O
.	O
Pendant	O
la	O
Réforme	O
,	O
il	O
n'	O
y	O
avait	O
que	O
quelques	O
paroissiens	O
protestants	O
dans	O
les	O
communes	O
environnantes	O
.	O
En	O
1784	O
,	O
sous	O
le	O
prince	O
électeur	O
Clément	PERSON
Wenceslas	PERSON
de	O
Saxe	O
,	O
au	O
travers	O
d'	O
un	O
édit	O
de	O
tolérance	O
,	O
les	O
riches	O
protestants	O
sont	O
officiellement	O
acceptés	O
et	O
leur	O
installation	O
officialisée	O
.	O
Aujourd'hui	O
,	O
les	O
pratiquants	O
évangéliques	O
qui	O
ne	O
sont	O
pas	O
membres	O
d'	O
une	O
église	O
libre	O
appartiennent	O
au	O
diocèse	O
de	O
Coblence	LOCATION
de	O
l'	O
église	O
évangélique	O
du	O
Rhin	O
.	O
Les	O
paroissiens	O
catholiques	O
font	O
partie	O
du	O
doyenné	O
de	O
Coblence	LOCATION
,	O
au	O
sein	O
de	O
la	O
région	O
de	O
Coblence	LOCATION
du	O
diocèse	O
de	O
Trèves	LOCATION
.	O
L'	O
emplacement	O
fertile	O
et	O
propice	O
aux	O
échanges	O
situé	O
au	O
confluent	O
de	O
la	O
Moselle	O
et	O
du	O
Rhin	O
fut	O
habité	O
de	O
manière	O
continue	O
depuis	O
l'	O
âge	O
de	O
la	O
pierre	O
(	O
vers	O
9000	O
avant	O
J.C.	O
)	O
.	O
Coblence	O
fait	O
ainsi	O
partie	O
des	O
plus	O
anciennes	O
cités	O
allemandes	O
.	O
Les	O
Romains	O
appellent	O
ce	O
camp	O
ad	O
confluentes	O
,	O
ce	O
qui	O
signifie	O
"	O
au	O
confluent	O
"	O
.	O
À	O
cette	O
même	O
époque	O
s'	O
installent	O
les	O
futurs	O
habitants	O
de	O
Trèves	LOCATION
,	O
qui	O
contrôlèrent	O
toute	O
la	O
région	O
de	O
la	O
Moselle	O
.	O
Des	O
vestiges	O
de	O
murs	O
et	O
de	O
routes	O
témoignent	O
encore	O
aujourd'hui	O
dans	O
le	O
vieux	O
Coblence	LOCATION
des	O
fortifications	O
du	O
temps	O
de	O
la	O
colonie	O
romaine	O
.	O
Coblence	O
est	O
donc	O
,	O
à	O
la	O
mort	O
de	O
Charlemagne	O
,	O
en	O
814	O
,	O
partagée	O
entre	O
les	O
fils	O
de	O
Louis	O
le	O
Pieux	O
.	O
En	O
830	O
,	O
suite	O
au	O
conflit	O
opposant	O
les	O
fils	O
de	O
Louis	O
le	O
Pieux	O
(	O
Lothaire	PERSON
Ier	O
,	O
Charles	PERSON
le	O
Chauve	O
et	O
Louis	PERSON
le	O
Germanique	O
)	O
entre	O
eux	O
et	O
contre	O
leur	O
père	O
,	O
l'	O
empire	O
est	O
à	O
nouveau	O
éclaté	O
et	O
Charles	O
le	O
Chauve	O
hérite	O
de	O
Coblence	LOCATION
en	O
837	O
.	O
Coblence	O
échoue	O
alors	O
à	O
Lothaire	PERSON
II	O
.	O
Le	O
territoire	O
sous	O
l'	O
autorité	O
séculière	O
de	O
l'	O
archevêque	O
de	O
Trèves	LOCATION
appartient	O
au	O
canton	O
rhénan	O
et	O
s'	O
étend	O
sur	O
la	O
rive	O
gauche	O
et	O
droite	O
des	O
cours	O
inférieurs	O
de	O
la	LOCATION
Moselle	LOCATION
et	LOCATION
du	LOCATION
Lahn	LOCATION
.	O
Il	O
devient	O
leur	O
tête	O
de	O
pont	O
sur	O
la	O
rive	O
droite	O
du	O
Rhin	O
et	O
gagne	O
la	O
réputation	O
d'	O
être	O
leur	O
château	O
le	O
plus	O
sûr	O
.	O
En	O
particulier	O
,	O
la	O
tête	O
de	O
Saint	O
Matthieu	O
,	O
le	O
patron	O
du	O
diocèse	O
,	O
y	O
reste	O
de	O
1380	O
à	O
1422	O
et	O
la	O
Sainte	O
Tunique	O
de	O
1657	O
à	O
1794	O
.	O
En	O
1338	O
,	O
le	O
roi	O
Louis	O
de	O
Bavière	O
tient	O
sa	O
cour	O
pour	O
une	O
journée	O
à	O
Coblence	LOCATION
,	O
en	O
présence	O
du	O
roi	O
Edouard	O
III	O
d'	O
Angleterre	O
.	O
Lors	O
de	O
la	O
guerre	O
de	O
Trente	O
Ans	O
,	O
le	O
prince	O
électeur	O
de	O
Trèves	LOCATION
Philippe	PERSON
Christophe	PERSON
de	O
Sötern	O
prend	O
le	O
parti	O
de	O
la	O
France	O
.	O
Des	O
troupes	O
françaises	O
occupent	O
la	O
fortification	O
d'	O
Ehrenbreitstein	O
le	O
5	O
juin	O
1632	O
.	O
Elles	O
libèrent	O
également	O
Coblence	O
en	O
mai	O
1636	O
.	O
Il	O
faut	O
ensuite	O
plus	O
d'	O
un	O
an	O
de	O
siège	O
pour	O
reprendre	O
possession	O
de	O
la	O
fortification	O
d'	O
Ehrenbreitstein	O
en	O
1637	O
.	O
Sous	O
son	O
règne	O
,	O
en	O
1786	O
,	O
la	O
première	O
conduite	O
d'	O
eau	O
pour	O
la	O
population	O
de	O
Coblence	LOCATION
est	O
construite	O
.	O
La	O
forteresse	O
d'	O
Ehrenbreitstein	O
parvient	O
à	O
tenir	O
jusqu'	O
au	O
27	O
janvier	O
1799	O
avant	O
de	O
capituler	O
à	O
son	O
tour	O
.	O
Cet	O
événement	O
marque	O
la	O
fin	O
du	O
règne	O
des	O
princes	O
électeurs	O
de	O
Trèves	LOCATION
.	O
Avec	O
le	O
traité	O
de	O
Lunéville	O
signé	O
le	O
9	O
février	O
1801	O
,	O
Coblence	LOCATION
est	O
incorporé	O
à	O
la	O
République	O
française	O
et	O
devient	O
chef-lieu	O
du	O
département	O
français	O
de	O
Rhin-et-Moselle	O
.	O
La	O
ville	O
reçoit	O
la	O
visite	O
de	O
Napoléon	O
Bonaparte	PERSON
et	O
son	O
épouse	O
Joséphine	PERSON
de	PERSON
Beauharnais	PERSON
du	O
17	O
au	O
19	O
septembre	O
1804	O
.	O
Il	O
y	O
porte	O
,	O
avec	O
quelques	O
fautes	O
d'	O
orthographe	O
,	O
une	O
inscription	O
en	O
français	O
pour	O
célébrer	O
le	O
succès	O
de	O
l'	O
expédition	O
de	O
Napoléon	O
vers	O
la	O
Russie	O
.	O
Le	O
11	O
mars	O
1815	O
,	O
le	O
roi	O
de	O
Prusse	O
Frédéric-Guillaume	PERSON
III	O
donne	O
l'	O
ordre	O
de	O
construire	O
une	O
nouvelle	O
fortification	O
pour	O
la	O
ville	O
de	O
Coblence	LOCATION
et	O
la	O
forteresse	O
d'	O
Ehrenbreitstein	O
.	O
Dans	O
les	O
années	O
suivantes	O
,	O
une	O
nouvelle	O
fortification	O
est	O
élevée	O
autour	O
de	O
Coblence	LOCATION
.	O
Elle	O
représente	O
le	O
bastion	O
militaire	O
le	O
plus	O
important	O
et	O
l'	O
un	O
des	O
plus	O
solides	O
du	O
Rhin	O
.	O
Un	O
nouveau	O
pont	O
flottant	O
est	O
donc	O
construit	O
en	O
1819	O
entre	O
Coblence	LOCATION
et	LOCATION
Ehrenbreitstein	LOCATION
.	O
De	O
1841	O
jusqu'	O
à	O
sa	O
destruction	O
en	O
1945	O
pendant	O
la	O
Seconde	O
Guerre	O
mondiale	O
,	O
un	O
pont	O
flottant	O
a	O
continué	O
à	O
enjamber	O
le	O
Rhin	O
.	O
Le	O
14	O
septembre	O
1842	O
,	O
Frédéric	PERSON
Guillaume	PERSON
IV	O
y	O
emménage	O
enfin	O
.	O
En	O
1858	O
,	O
le	O
premier	O
train	O
traversa	O
le	O
nouveau	O
pont	O
de	O
Coblence	O
et	O
le	O
réseau	O
de	O
voies	O
ferrées	O
fut	O
systématiquement	O
aménagé	O
à	O
partir	O
de	O
1864	O
.	O
À	O
la	O
suite	O
du	O
développement	O
de	O
l'	O
armement	O
et	O
des	O
techniques	O
militaires	O
,	O
on	O
renonça	O
en	O
1890	O
à	O
augmenter	O
les	O
fortifications	O
de	O
Coblence	LOCATION
.	O
Plusieurs	O
semaines	O
après	O
la	O
mort	O
de	O
l'	O
empereur	O
Guillaume	O
Ier	O
(	O
1888	O
)	O
,	O
l'	O
administration	O
entama	O
un	O
nouveau	O
projet	O
:	O
l'	O
édification	O
d'	O
un	O
mémorial	O
en	O
l'	O
honneur	O
du	O
défunt	O
roi	O
et	O
empereur	O
.	O
À	O
la	O
fin	O
de	O
la	O
Première	O
Guerre	O
mondiale	O
,	O
en	O
novembre	O
1918	O
,	O
un	O
conseil	O
des	O
ouvriers	O
et	O
des	O
soldats	O
se	O
forma	O
à	O
Coblence	LOCATION
,	O
mais	O
un	O
mois	O
plus	O
tard	O
les	O
troupes	O
américaines	O
conquirent	O
la	O
ville	O
et	O
mirent	O
fin	O
au	O
gouvernement	O
des	O
soviets	O
.	O
En	O
1919	O
,	O
à	O
la	O
suite	O
des	O
élections	O
,	O
le	O
conseil	O
municipal	O
de	O
Coblence	LOCATION
accueillit	O
,	O
pour	O
la	O
première	O
fois	O
,	O
des	O
femmes	O
en	O
son	O
sein	O
.	O
Lors	O
de	O
la	O
remilitarisation	O
de	O
la	O
rive	O
gauche	O
du	O
Rhin	O
,	O
des	O
troupes	O
allemandes	O
furent	O
de	O
nouveau	O
stationnées	O
à	O
Coblence	LOCATION
en	O
1936	O
.	O
Le	O
centre	O
historique	O
de	O
Coblence	LOCATION
fut	O
détruit	O
par	O
un	O
bombardement	O
de	O
la	O
Royal	O
Air	O
Force	O
en	O
1944	O
,	O
et	O
la	O
ville	O
de	O
nouveau	O
occupée	O
par	O
des	O
troupes	O
américaines	O
en	O
1945	O
.	O
En	O
1950	O
,	O
le	O
parlement	O
régional	O
se	O
prononça	O
en	O
faveur	O
d'	O
un	O
transfert	O
du	O
siège	O
du	O
gouvernement	O
à	O
Mayence	LOCATION
.	O
En	O
1953	O
,	O
le	O
président	O
Theodor	PERSON
Heuss	PERSON
déclara	O
le	O
site	O
du	O
Deutsches	O
Eck	O
,	O
mémorial	O
de	O
l'	O
unification	O
allemande	O
.	O
À	O
la	O
place	O
de	O
la	O
statue	O
de	O
l'	O
empereur	O
Guillaume	O
Ier	O
,	O
détruite	O
par	O
des	O
feux	O
d'	O
artillerie	O
en	O
1945	O
,	O
on	O
installa	O
le	O
nouveau	O
drapeau	O
allemand	O
.	O
À	O
la	O
suite	O
de	O
la	O
nouvelle	O
fondation	O
d'	O
une	O
armée	O
allemande,les	O
casernes	O
de	O
Coblence	LOCATION
furent	O
de	O
nouveau	O
peuplées	O
et	O
la	O
ville	O
devint	O
le	O
siège	O
de	O
garnison	O
le	O
plus	O
important	O
de	O
l'	O
Allemagne	O
de	O
l'	O
époque	O
de	O
la	O
guerre	O
froide	O
.	O
De	O
nos	O
jours	O
,	O
Coblence	LOCATION
est	O
toujours	O
la	O
ville	O
la	O
plus	O
importante	O
en	O
termes	O
de	O
stationnement	O
des	O
forces	O
armées	O
allemandes	O
.	O
Deux	O
ans	O
plus	O
tard	O
,	O
la	O
ville	O
célébra	O
l'	O
anniversaire	O
de	O
ses	O
2000	O
ans	O
d'	O
existence	O
,	O
et	O
après	O
de	O
longues	O
controverses	O
la	O
statue	O
restituée	O
de	O
l'	O
empereur	O
Guillaume	O
Ier	O
fut	O
inaugurée	O
en	O
1993	O
.	O
Avec	O
l'	O
occupation	O
de	O
la	O
ville	O
par	O
les	O
troupes	O
de	O
Napoléon	O
,	O
le	O
carnaval	O
prend	O
également	O
une	O
nature	O
plus	O
bourgeoise	O
.	O
La	O
réforme	O
implique	O
une	O
organisation	O
des	O
manifestations	O
à	O
Coblence	LOCATION
et	O
entraîne	O
la	O
fondation	O
de	O
nombreuses	O
associations	O
.	O
La	O
ville	O
de	O
Coblence	LOCATION
entretient	O
des	O
liens	O
de	O
jumelage	O
avec	O
sept	O
municipalités	O
réparties	O
sur	O
trois	O
continents	O
.	O
La	O
première	O
relation	O
de	O
ce	O
genre	O
date	O
de	O
1963	O
avec	O
la	O
ville	O
de	O
Nevers	LOCATION
(	O
France	O
)	O
.	O
Un	O
lien	O
de	O
jumelage	O
fut	O
également	O
établi	O
en	O
1981	O
avec	O
la	O
ville	O
de	O
Maastricht	LOCATION
(	O
Pays-Bas	O
)	O
.	O
Grâce	O
à	O
son	O
orientation	O
stratégique	O
au	O
confluent	O
de	O
Rhin	O
et	O
Moselle	O
,	O
Coblence	LOCATION
est	O
facilement	O
accessible	O
par	O
voie	O
fluviale	O
.	O
La	O
ville	O
fait	O
partie	O
du	O
réseau	O
de	O
transport	O
public	O
Rhin	O
et	O
Moselle	O
.	O
