Il	O
passe	O
son	O
enfance	O
à	O
Bois-Guillaume	LOCATION
,	O
ville	O
résidentielle	O
sur	O
les	O
hauteurs	O
de	O
Rouen	LOCATION
,	O
où	O
il	O
est	O
élève	O
au	O
Pensionnat	ORGANIZATION
Jean-Baptiste-de-La-Salle	ORGANIZATION
de	O
Rouen	LOCATION
.	O
A	O
la	O
fin	O
des	O
années	O
1970	O
,	O
il	O
rencontre	O
Ségolène	PERSON
Royal	PERSON
lors	O
d'	O
une	O
soirée	O
de	O
l'	O
École	O
nationale	O
d'	O
administration	O
.	O
Ségolène	PERSON
Royal	PERSON
a	O
démenti	O
la	O
rumeur	O
selon	O
laquelle	O
ils	O
avaient	O
conclu	O
un	O
pacte	O
civil	O
de	O
solidarité	O
.	O
Sa	O
compagne	O
actuelle	O
est	O
la	O
journaliste	O
Valérie	PERSON
Trierweiler	PERSON
.	O
François	PERSON
Hollande	PERSON
présida	O
la	O
section	O
de	O
l'	O
UNEF	O
à	O
l'	O
Institut	O
d'	O
études	O
politiques	O
de	O
Paris	O
.	O
En	O
1974	O
,	O
il	O
entre	O
à	O
HEC	O
Paris	O
et	O
y	O
préside	O
le	O
comité	O
de	O
soutien	O
à	O
François	PERSON
Mitterrand	PERSON
.	O
Il	O
était	O
entré	O
un	O
an	O
auparavant	O
au	O
Parti	O
socialiste	O
,	O
et	O
devient	O
,	O
grâce	O
à	O
Jacques	PERSON
Attali	PERSON
,	O
conseiller	O
de	O
François	O
Mitterrand	O
pour	O
les	O
questions	O
économiques	O
.	O
À	O
l'	O
élection	O
de	O
François	O
Mitterrand	O
,	O
en	O
1981	O
,	O
il	O
devient	O
chargé	O
de	O
mission	O
(	O
toujours	O
à	O
propos	O
d'	O
économie	O
)	O
pour	O
l'	O
Élysée	O
,	O
à	O
l'	O
époque	O
où	O
le	O
nouveau	O
pouvoir	O
entame	O
sa	O
politique	O
de	O
relance	O
par	O
la	O
demande	O
et	O
des	O
nationalisations	O
.	O
En	O
1983	O
,	O
il	O
est	O
directeur	O
de	O
cabinet	O
de	O
deux	O
porte-paroles	O
successifs	O
du	O
gouvernement	O
de	O
Pierre	O
Mauroy	PERSON
:	O
Max	PERSON
Gallo	PERSON
et	O
Roland	PERSON
Dumas	PERSON
.	O
La	O
même	O
année	O
,	O
il	O
échoue	O
aux	O
élections	O
municipales	O
,	O
mais	O
devient	O
conseiller	O
municipal	O
d'	O
Ussel	LOCATION
(	O
Corrèze	O
)	O
,	O
la	O
circonscription	O
de	O
Jacques	PERSON
Chirac	PERSON
.	O
En	O
1984	O
,	O
peu	O
tenté	O
par	O
les	O
querelles	O
de	O
tendances	O
,	O
il	O
fonde	O
les	O
transcourants	O
avec	O
quelques	O
amis	O
,	O
notamment	O
Jean-Yves	PERSON
Le	PERSON
Drian	PERSON
,	O
Jean-Pierre	PERSON
Mignard	PERSON
et	O
Jean-Michel	PERSON
Gaillard	PERSON
.	O
En	O
1988	O
,	O
après	O
la	O
réélection	O
de	O
François	PERSON
Mitterrand	PERSON
,	O
il	O
est	O
élu	O
député	O
de	O
Tulle	LOCATION
(	O
1	O
re	O
circonscription	O
de	O
la	O
Corrèze	O
)	O
,	O
avec	O
près	O
de	O
53	O
%	O
des	O
suffrages	O
exprimés	O
.	O
Il	O
est	O
nommé	O
professeur	O
d'	O
économie	O
en	O
3	O
e	O
année	O
de	O
l'	O
Institut	O
d'	O
études	O
politiques	O
de	O
Paris	O
de	O
1988	O
à	O
1991	O
.	O
En	O
novembre	O
1994	O
,	O
il	O
devient	O
secrétaire	O
national	O
du	O
Parti	O
socialiste	O
,	O
chargé	O
des	O
questions	O
économiques	O
.	O
Il	O
se	O
rapproche	O
de	O
Lionel	PERSON
Jospin	PERSON
après	O
le	O
renoncement	O
de	O
Jacques	PERSON
Delors	PERSON
(	O
qu'	O
il	O
soutenait	O
dans	O
son	O
éventuelle	O
candidature	O
)	O
.	O
Lionel	PERSON
Jospin	PERSON
fait	O
de	O
lui	O
un	O
des	O
porte-paroles	O
de	O
sa	O
campagne	O
présidentielle	O
,	O
puis	O
celui	O
du	O
parti	O
en	O
octobre	O
1995	O
.	O
Il	O
intègre	O
le	O
bureau	O
national	O
du	O
PS	O
en	O
novembre	O
1997	O
.	O
En	O
1999	O
,	O
François	PERSON
Hollande	PERSON
est	O
élu	O
député	O
européen	O
(	O
mandat	O
qu'	O
il	O
ne	O
conservera	O
pas	O
,	O
préférant	O
garder	O
celui	O
de	O
député	O
français	O
,	O
pour	O
cause	O
de	O
non-cumul	O
)	O
et	O
vice-président	O
de	O
l'	O
Internationale	O
socialiste	O
.	O
En	O
2001	O
,	O
il	O
est	O
élu	O
,	O
après	O
deux	O
tentatives	O
,	O
maire	O
de	O
Tulle	LOCATION
,	O
avec	O
53	O
%	O
des	O
voix	O
au	O
premier	O
tour	O
.	O
Après	O
le	O
retrait	O
de	O
Lionel	PERSON
Jospin	O
,	O
il	O
devient	O
véritablement	O
le	O
patron	O
du	O
Parti	O
socialiste	O
.	O
Fin	O
2003	O
,	O
il	O
est	O
un	O
des	O
seuls	O
dirigeants	O
à	O
estimer	O
une	O
large	O
victoire	O
possible	O
et	O
confie	O
la	O
direction	O
de	O
la	O
campagne	O
à	O
François	PERSON
Rebsamen	PERSON
.	O
En	O
mars	O
2004	O
,	O
le	O
PS	O
remporte	O
20	O
des	O
22	O
régions	O
de	O
métropole	O
et	O
la	O
Guadeloupe	O
,	O
les	O
deux	O
tiers	O
des	O
cantons	O
renouvelables	O
élisent	O
un	O
conseiller	O
de	O
gauche	O
,	O
et	O
51	O
des	O
100	O
départements	O
ont	O
un	O
président	O
de	O
gauche	O
(	O
pour	O
la	O
première	O
fois	O
depuis	O
1946	O
)	O
,	O
le	O
PS	O
progresse	O
au	O
détriment	O
de	O
la	O
droite	O
;	O
en	O
juin	O
,	O
le	O
PS	O
obtient	O
presque	O
29	O
%	O
des	O
suffrages	O
exprimés	O
aux	O
élections	O
européennes	O
,	O
son	O
record	O
pour	O
ce	O
scrutin	O
.	O
À	O
la	O
fin	O
du	O
printemps	O
,	O
François	O
Hollande	O
met	O
au	O
point	O
le	O
calendrier	O
d'	O
élaboration	O
du	O
programme	O
du	O
Parti	O
socialiste	O
pour	O
les	O
prochaines	O
élections	O
nationales	O
de	O
2007	O
.	O
Le	O
texte	O
a	O
été	O
soumis	O
aux	O
membres	O
du	O
PS	O
et	O
approuvé	O
le	O
22	O
juin	O
2006	O
.	O
Toujours	O
en	O
2004	O
,	O
il	O
prend	O
position	O
pour	O
le	O
"	O
oui	O
"	O
à	O
la	O
Constitution	O
européenne	O
et	O
s'	O
oppose	O
au	O
numéro	O
deux	O
du	O
parti	O
,	O
Laurent	PERSON
Fabius	O
,	O
qui	O
,	O
dès	O
2003	O
,	O
s'	O
était	O
positionné	O
"	O
contre	O
"	O
.	O
Il	O
organise	O
un	O
référendum	O
interne	O
au	O
PS	O
sur	O
la	O
question	O
.	O
Le	O
1	O
er	O
décembre	O
,	O
les	O
militants	O
votent	O
"	O
oui	O
"	O
à	O
59	O
%	O
:	O
François	PERSON
Hollande	O
en	O
sort	O
renforcé	O
et	O
obtient	O
un	O
statut	O
de	O
présidentiable	O
pour	O
l'	O
élection	O
présidentielle	O
de	O
2007	O
.	O
Il	O
sort	O
partiellement	O
renforcé	O
du	O
congrès	O
du	O
Mans	O
,	O
où	O
sa	O
motion	O
obtient	O
54	O
%	O
des	O
suffrages	O
exprimés	O
lors	O
du	O
vote	O
des	O
militants	O
,	O
une	O
majorité	O
moins	O
confortable	O
que	O
celles	O
obtenues	O
précédemment	O
(	O
84	O
%	O
en	O
1997	O
,	O
73	O
%	O
en	O
2000	O
,	O
61	O
%	O
en	O
2003	O
)	O
mais	O
beaucoup	O
plus	O
confortable	O
que	O
celles	O
de	O
François	PERSON
Mitterrand	O
aux	O
congrès	O
d'	O
Épinay	O
(	O
1971	O
)	O
et	O
de	O
Metz	LOCATION
(	O
1979	O
)	O
,	O
où	O
la	O
motion	O
de	O
la	O
direction	O
était	O
en	O
dessous	O
des	O
50	O
%	O
.	O
Voulant	O
mettre	O
fin	O
à	O
un	O
an	O
de	O
vives	O
discussions	O
et	O
de	O
querelles	O
dangereuses	O
,	O
François	O
Hollande	O
choisit	O
de	O
proposer	O
une	O
synthèse	O
aux	O
courants	O
minoritaires	O
,	O
qui	O
acceptent	O
(	O
mais	O
Arnaud	PERSON
Montebourg	PERSON
se	O
retire	O
du	O
courant	O
NPS	O
)	O
.	O
Ses	O
détracteurs	O
lui	O
ont	O
reproché	O
une	O
absence	O
de	O
charisme	O
et	O
l'	O
accusent	O
de	O
devoir	O
son	O
statut	O
actuel	O
au	O
seul	O
soutien	O
de	O
Lionel	PERSON
Jospin	PERSON
(	O
critique	O
qui	O
n'	O
est	O
plus	O
justifiée	O
depuis	O
le	O
congrès	O
de	O
Dijon	O
)	O
.	O
François	PERSON
Hollande	PERSON
est	O
le	O
premier	O
signataire	O
de	O
la	O
motion	O
1	O
présentée	O
au	O
congrès	O
du	O
Mans	O
en	O
novembre	O
2005	O
.	O
Ses	O
soutiens	O
principaux	O
sont	O
Martine	PERSON
Aubry	PERSON
,	O
Jack	PERSON
Lang	PERSON
,	O
DSK	O
,	O
Ségolène	PERSON
Royal	PERSON
,	O
Julien	PERSON
Dray	PERSON
et	O
Bertrand	PERSON
Delanoë	PERSON
.	O
Le	O
17	O
juin	O
2007	O
,	O
François	PERSON
Hollande	PERSON
est	O
réélu	O
député	O
au	O
deuxième	O
tour	O
,	O
avec	O
60	O
%	O
des	O
suffrages	O
exprimés	O
.	O
Quelques	O
jours	O
plus	O
tôt	O
conformément	O
à	O
ses	O
déclarations	O
antérieures	O
,	O
il	O
déclare	O
qu'	O
il	O
ne	O
sera	O
pas	O
candidat	O
à	O
sa	O
propre	O
succession	O
à	O
la	O
tête	O
du	O
PS	O
en	O
novembre	O
2008	O
.	O
Au	O
même	O
moment	O
,	O
une	O
dépêche	O
de	O
l'	O
AFP	O
officialise	O
sa	O
séparation	O
d'	O
avec	O
Ségolène	PERSON
Royal	PERSON
.	O
En	O
application	O
de	O
la	O
loi	O
du	O
non	O
cumul	O
des	O
mandats	O
,	O
il	O
démissionne	O
de	O
son	O
mandat	O
de	O
conseiller	O
municipal	O
de	O
Tulle	LOCATION
,	O
l'	O
un	O
de	O
ses	O
proches	O
,	O
Bernard	PERSON
Combes	PERSON
,	O
lui	O
succédant	O
en	O
tant	O
que	O
maire	O
.	O
