Le	O
requin	O
blanc	O
de	O
7.13	O
m	O
,	O
capturé	O
en	O
1987	O
à	O
Malte	LOCATION
,	O
ne	O
devait	O
mesurer	O
d'	O
après	O
les	O
experts	O
que	O
5.50	O
m	O
.	O
Il	O
est	O
particulièrement	O
présent	O
en	O
Australie	O
,	O
en	O
Afrique	O
du	O
Sud	O
,	O
et	O
en	O
Californie	O
ainsi	O
que	O
dans	O
les	O
Caraïbes	O
.	O
Il	O
a	O
même	O
été	O
observé	O
au	O
large	O
des	O
côtes	O
d'	O
Alaska	O
.	O
En	O
2005	O
,	O
un	O
grand	O
requin	O
blanc	O
femelle	O
,	O
qui	O
a	O
été	O
doté	O
d'	O
un	O
capteur	O
de	O
localisation	O
,	O
a	O
traversé	O
,	O
aller-retour	O
,	O
l'	O
océan	O
Indien	O
,	O
du	O
Cap	O
(	O
Afrique	O
du	O
Sud	O
)	O
jusqu'	O
aux	O
côtes	O
méridionales	O
d'	O
Australie	O
.	O
Une	O
autre	O
a	O
effectué	O
la	O
traversée	O
de	O
l'	O
île	O
du	O
sud	O
de	O
la	O
Nouvelle-Zélande	O
à	O
la	O
Grande	O
barrière	O
de	O
corail	O
.	O
La	O
couleur	O
du	O
dos	O
de	O
l'	O
animal	O
varie	O
du	O
gris-noir	O
(	O
Afrique	O
du	O
Sud	O
,	O
Australie	O
,	O
Californie	O
)	O
au	O
marron	O
clair	O
pour	O
la	O
Méditerranée	O
,	O
où	O
l'	O
on	O
a	O
observé	O
un	O
comportement	O
alimentaire	O
différent	O
,	O
peut-être	O
une	O
adaptation	O
alimentaire	O
au	O
milieu	O
méditerranéen	O
:	O
des	O
chasses	O
de	O
thons	O
,	O
de	O
marlins	O
,	O
un	O
comportement	O
plus	O
opportuniste	O
et	O
tourné	O
vers	O
les	O
grands	O
poissons	O
plutôt	O
que	O
les	O
mammifères	O
marins	O
devenus	O
rares	O
dans	O
cette	O
région	O
.	O
Les	O
États-Unis	O
et	O
plus	O
particulièrement	O
la	O
Floride	O
,	O
sont	O
l'	O
un	O
des	O
lieux	O
où	O
,	O
statistiquement	O
,	O
il	O
y	O
a	O
le	O
plus	O
d'	O
attaques	O
de	O
requins	O
.	O
Il	O
a	O
été	O
popularisé	O
au	O
cinéma	O
par	O
la	O
tétralogie	O
Les	O
Dents	O
de	O
la	O
mer	O
,	O
dont	O
le	O
premier	O
volet	O
est	O
sorti	O
en	O
salles	O
en	O
1975	O
.	O
Peter	PERSON
Benchley	PERSON
,	O
l'	O
auteur	O
du	O
best-seller	O
Les	O
Dents	O
de	O
la	O
mer	O
adapté	O
pour	O
le	O
célèbre	O
film	O
de	O
Steven	O
Spielberg	O
,	O
a	O
aussi	O
défendu	O
la	O
cause	O
du	O
grand	O
requin	O
blanc	O
dans	O
les	O
dernières	O
années	O
de	O
sa	O
vie	O
.	O
Puis	O
d'	O
autres	O
l'	O
ont	O
imité	O
,	O
dont	O
Jean-Michel	PERSON
Cousteau	PERSON
.	O
Sa	O
pêche	O
est	O
désormais	O
interdite	O
dans	O
de	O
nombreux	O
pays	O
comme	O
l'	O
Australie	O
ou	O
l'	O
Afrique	O
du	O
Sud	O
.	O
