Avec	O
son	O
physique	O
impressionnant	O
et	O
son	O
visage	O
dur	O
,	O
Charles	PERSON
Bronson	PERSON
fut	O
souvent	O
habitué	O
à	O
des	O
rôles	O
virils	O
,	O
dont	O
quelques-uns	O
sont	O
entrés	O
dans	O
la	O
légende	O
.	O
Il	O
reste	O
pour	O
des	O
générations	O
de	O
cinéphiles	O
l'	O
inoubliable	O
homme	O
à	O
l'	O
harmonica	O
de	O
Il	O
était	O
une	O
fois	O
dans	O
l'	O
Ouest	O
,	O
le	O
roi	O
du	O
tunnel	O
de	O
La	O
Grande	O
Évasion	O
,	O
un	O
des	O
Douze	O
Salopards	O
,	O
un	O
des	O
Sept	O
Mercenaires	O
et	O
le	O
Justicier	O
dans	O
la	O
ville	O
.	O
Charles	PERSON
Bronson	PERSON
a	O
tourné	O
plus	O
de	O
150	O
films	O
pour	O
la	O
télévision	O
,	O
entre	O
1952	O
et	O
1998	O
.	O
Il	O
a	O
également	O
interprété	O
un	O
rôle	O
de	O
soldat	O
pacifiste	O
dans	O
la	O
série	O
La	O
Quatrième	O
Dimension	O
,	O
et	O
de	O
gangster	O
gitan	O
dans	O
Les	O
Incorruptibles	O
.	O
Il	O
est	O
surtout	O
connu	O
pour	O
ses	O
rôles	O
dans	O
Les	O
Sept	O
Mercenaires	O
de	O
John	PERSON
Sturges	PERSON
,	O
Il	O
était	O
une	O
fois	O
dans	O
l'	O
Ouest	O
ou	O
encore	O
Soleil	O
Rouge	O
et	O
des	O
films	O
de	O
guerre	O
(	O
La	O
Grande	O
Évasion	O
et	O
Les	O
Douze	O
Salopards	O
)	O
.	O
Il	O
tourne	O
quatre	O
suites	O
à	O
ce	O
film	O
de	O
Michael	PERSON
Winner	PERSON
,	O
au	O
fil	O
des	O
années	O
.	O
Le	O
5	O
octobre	O
1968	O
il	O
avait	O
épousé	O
l'	O
actrice	O
anglaise	O
Jill	PERSON
Ireland	PERSON
avec	O
laquelle	O
il	O
vécut	O
jusqu'	O
au	O
décès	O
de	O
celle-ci	O
,	O
victime	O
d'	O
un	O
cancer	O
du	O
sein	O
le	O
18	O
mai	O
1990	O
.	O
Son	O
interprétation	O
grandiloquente	O
sera	O
détournée	O
dans	O
La	O
Classe	O
américaine	O
de	O
Michel	PERSON
Hazanavicius	PERSON
.	O
Il	O
sera	O
,	O
ensuite	O
,	O
un	O
chef	O
impérial	O
dans	O
le	O
très	O
réussi	O
Le	O
Jugement	O
des	O
flèches	O
de	O
Samuel	PERSON
Fuller	PERSON
.	O
Le	O
héros	O
de	O
la	O
bande	O
dessinée	O
a	O
été	O
volontairement	O
dessiné	O
sous	O
les	O
traits	O
de	O
l'	O
acteur	O
américain	O
Charles	PERSON
Bronson	PERSON
qui	O
joue	O
dans	O
ses	O
films	O
un	O
rôle	O
similaire	O
de	O
justicier	O
.	O
