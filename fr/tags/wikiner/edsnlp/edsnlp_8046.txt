Roberto	PERSON
Calvi	PERSON
était	O
membre	O
de	O
la	O
loge	O
P2	O
,	O
dirigée	O
par	O
Licio	PERSON
Gelli	PERSON
.	O
La	O
gestion	O
de	O
Roberto	PERSON
Calvi	PERSON
laissa	O
un	O
déficit	O
de	O
1,4	O
milliard	O
de	O
dollars	O
dans	O
les	O
caisses	O
de	O
Banco	O
Ambrosiano	O
.	O
Le	O
18	O
juin	O
1982	O
,	O
Roberto	PERSON
Calvi	PERSON
est	O
retrouvé	O
pendu	O
sous	O
un	O
pont	O
de	O
Londres	LOCATION
(	O
Blackfriars	O
Bridge	O
)	O
.	O
En	O
1992	O
,	O
une	O
enquête	O
est	O
rouverte	O
en	O
Italie	O
.	O
Licio	PERSON
Gelli	PERSON
,	O
grand-maître	O
de	O
la	O
loge	O
P2	O
,	O
est	O
aussi	O
interrogé	O
en	O
tant	O
qu'	O
accusé	O
par	O
les	O
magistrats	O
,	O
ainsi	O
que	O
,	O
en	O
tant	O
que	O
témoin	O
,	O
Ernest	PERSON
Backes	PERSON
.	O
Clearstream	O
aurait	O
transféré	O
des	O
fonds	O
pour	O
le	O
bénéfice	O
de	O
la	O
banque	O
Ambrosiano	O
.	O
Selon	O
les	O
magistrats	O
romains	O
,	O
Roberto	PERSON
Calvi	PERSON
fut	O
éliminé	O
pour	O
trois	O
raisons	O
:	O
