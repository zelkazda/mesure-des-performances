Le	O
GATT	O
puis	O
l'	O
OMC	O
ont	O
été	O
créés	O
pour	O
abaisser	O
les	O
barrières	O
protectionnistes	O
et	O
en	O
limiter	O
autant	O
que	O
possible	O
l'	O
usage	O
.	O
Dans	O
son	O
ouvrage	O
maître	O
,	O
Adam	PERSON
Smith	PERSON
justifie	O
le	O
libre-échange	O
,	O
en	O
développant	O
l'	O
idée	O
que	O
,	O
contrairement	O
à	O
ce	O
qu'	O
affirmaient	O
les	O
mercantilistes	O
,	O
le	O
commerce	O
est	O
synonyme	O
de	O
paix	O
et	O
d'	O
enrichissement	O
mutuel	O
.	O
Toutefois	O
,	O
Smith	O
n'	O
est	O
pas	O
contre	O
l'	O
idée	O
d'	O
instaurer	O
des	O
droits	O
de	O
douane	O
,	O
pour	O
deux	O
cas	O
bien	O
spécifiques	O
:	O
en	O
cas	O
de	O
présence	O
d'	O
industries	O
stratégiques	O
pour	O
la	O
défense	O
nationale	O
et	O
en	O
réaction	O
à	O
des	O
taxations	O
opérées	O
par	O
des	O
pays	O
sur	O
les	O
exportations	O
nationales	O
.	O
Le	O
protectionnisme	O
est	O
donc	O
selon	O
Smith	O
une	O
mesure	O
exceptionnelle	O
,	O
mais	O
qui	O
,	O
en	O
règle	O
général	O
,	O
nuit	O
au	O
bon	O
fonctionnement	O
de	O
l'	O
économie	O
.	O
Ces	O
derniers	O
devaient	O
être	O
dédouanés	O
à	O
Poitiers	O
.	O
Lorsqu'	O
il	O
fut	O
supprimé	O
en	O
2005	O
,	O
on	O
a	O
assisté	O
à	O
un	O
forte	O
hausse	O
des	O
importations	O
de	O
textile	O
provenant	O
de	O
Chine	O
,	O
au	O
détriment	O
de	O
celles	O
en	O
provenance	O
de	O
la	O
Tunisie	O
et	O
du	O
Maroc	O
.	O
Les	O
États-Unis	O
ont	O
aussi	O
laissé	O
le	O
cours	O
de	O
leur	O
monnaie	O
baisser	O
afin	O
de	O
favoriser	O
le	O
dollar	O
au	O
détriment	O
de	O
l'	O
euro	O
.	O
On	O
parle	O
de	O
dumping	O
social	O
lorsqu'	O
un	O
gouvernement	O
réduit	O
(	O
ou	O
supprime	O
)	O
les	O
cotisations	O
sociales	O
,	O
ou	O
bien	O
que	O
les	O
autorités	O
d'	O
un	O
pays	O
conservent	O
des	O
normes	O
sociales	O
très	O
basses	O
(	O
par	O
exemple	O
,	O
en	O
Chine	O
,	O
la	O
règlementation	O
du	O
travail	O
est	O
moins	O
contraignante	O
pour	O
les	O
employeurs	O
que	O
des	O
règlementations	O
en	O
vigueur	O
ailleurs	O
)	O
.	O
Depuis	O
les	O
années	O
1930	O
,	O
les	O
États-Unis	O
adoptent	O
une	O
politique	O
systématique	O
consistant	O
à	O
interdire	O
dans	O
les	O
marchés	O
publics	O
les	O
produits	O
qui	O
ne	O
sont	O
pas	O
fabriqués	O
aux	O
États-Unis	O
.	O
Les	O
subventions	O
sont	O
aujourd'hui	O
interdites	O
par	O
l'	O
Union	O
européenne	O
.	O
Les	O
États-Unis	O
se	O
sont	O
opposés	O
au	O
développement	O
du	O
supersonique	O
Concorde	O
en	O
appliquant	O
des	O
normes	O
sur	O
le	O
bruit	O
(	O
il	O
est	O
vrai	O
que	O
le	O
Concorde	O
était	O
un	O
avion	O
très	O
bruyant	O
)	O
.	O
Depuis	O
la	O
fin	O
des	O
années	O
1980	O
,	O
les	O
États-Unis	O
ont	O
élargi	O
cette	O
politique	O
à	O
des	O
actions	O
plus	O
offensives	O
de	O
soutien	O
cohérent	O
des	O
entreprises	O
américaines	O
à	O
l'	O
exportation	O
.	O
Dans	O
la	O
vision	O
des	O
stratèges	O
américains	O
,	O
le	O
monde	O
se	O
répartit	O
en	O
trois	O
zones	O
:	O
les	O
États-Unis	O
conçoivent	O
,	O
l'	O
Asie	O
produit	O
,	O
et	O
l'	O
Europe	O
consomme	O
.	O
Aujourd'hui	O
les	O
États-Unis	O
cherchent	O
à	O
imposer	O
des	O
normes	O
internationales	O
dans	O
le	O
domaine	O
des	O
technologies	O
de	O
l'	O
information	O
.	O
Par	O
exemple	O
,	O
la	O
spécification	O
technique	O
ebXML	O
tend	O
à	O
s'	O
imposer	O
dans	O
le	O
monde	O
comme	O
un	O
standard	O
de	O
commerce	O
électronique	O
.	O
En	O
2000	O
,	O
le	O
président	O
George	O
W.	O
Bush	O
a	O
mis	O
en	O
place	O
des	O
mesures	O
protectionnistes	O
sur	O
les	O
importations	O
d'	O
acier	O
pour	O
satisfaire	O
les	O
demandes	O
des	O
grandes	O
entreprises	O
du	O
secteur	O
dont	O
la	O
productivité	O
était	O
insuffisante	O
.	O
L'	O
un	O
des	O
tarifs	O
douaniers	O
les	O
plus	O
élevés	O
du	O
monde	O
est	O
celui	O
que	O
pratique	O
le	O
Japon	O
sur	O
le	O
riz	O
étranger	O
,	O
taxé	O
à	O
800	O
%	O
.	O
En	O
France	O
,	O
on	O
invoque	O
quelquefois	O
l'	O
exception	O
culturelle	O
.	O
Pour	O
Jean-Baptiste	PERSON
Colbert	PERSON
,	O
"	O
les	O
compagnies	O
de	O
commerce	O
sont	O
les	O
armées	O
du	O
roi	O
,	O
et	O
les	O
manufactures	O
sont	O
ses	O
réserves	O
"	O
.	O
D'	O
autres	O
tel	O
Friedrich	PERSON
List	PERSON
,	O
considèrent	O
le	O
protectionnisme	O
comme	O
nécessaire	O
à	O
court	O
terme	O
pour	O
amorcer	O
le	O
développement	O
d'	O
une	O
économie	O
.	O
Dans	O
les	O
années	O
1980	O
,	O
début	O
1990	O
,	O
des	O
économistes	O
tels	O
que	O
Jagdish	PERSON
Bhagwati	PERSON
ont	O
insisté	O
sur	O
les	O
activités	O
recherches	O
de	O
rente	O
qu'	O
induisaient	O
les	O
politiques	O
protectionnistes	O
.	O
D'	O
une	O
manière	O
générale	O
le	O
protectionnisme	O
est	O
vu	O
,	O
depuis	O
Adam	O
Smith	O
comme	O
favorisant	O
les	O
offreurs	O
au	O
détriment	O
des	O
consommateurs	O
.	O
Les	O
États-Unis	O
sont	O
paradoxalement	O
l'	O
un	O
des	O
pays	O
le	O
plus	O
protecteur	O
du	O
monde	O
.	O
Le	O
protectionnisme	O
libéral	O
de	O
Friedrich	O
List	PERSON
est	O
lui	O
aussi	O
envisageable	O
:	O
le	O
financement	O
des	O
projets	O
de	O
recherche	O
ou	O
la	O
mise	O
en	O
place	O
de	O
branches	O
considérées	O
comme	O
naissantes	O
et	O
donc	O
fragiles	O
pourrait	O
être	O
étudiés	O
.	O
Jacques	PERSON
Sapir	PERSON
réfute	O
ces	O
hypothèses	O
en	O
expliquant	O
que	O
"	O
la	O
chute	O
du	O
commerce	O
international	O
a	O
d'	O
autres	O
causes	O
que	O
le	O
protectionnisme	O
"	O
.	O
Jacques	PERSON
Sapir	PERSON
relève	O
que	O
"	O
la	O
contraction	O
des	O
crédits	O
est	O
une	O
cause	O
majeure	O
de	O
la	O
contraction	O
du	O
commerce	O
"	O
.	O
Une	O
étude	O
du	O
National	O
Bureau	O
of	O
Economic	O
Research	O
met	O
en	O
évidence	O
l'	O
influence	O
prédominante	O
de	O
l'	O
instabilité	O
monétaire	O
(	O
qui	O
entraîna	O
la	O
crise	O
des	O
liquidités	O
internationales	O
)	O
et	O
de	O
la	O
hausse	O
soudaine	O
des	O
coûts	O
de	O
transport	O
dans	O
la	O
diminution	O
du	O
commerce	O
durant	O
les	O
années	O
1930	O
.	O
