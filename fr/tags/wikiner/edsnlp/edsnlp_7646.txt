Ce	O
drapeau	O
a	O
été	O
officiellement	O
adopté	O
le	O
30	O
juin	O
1911	O
,	O
mais	O
était	O
déjà	O
utilisé	O
depuis	O
la	O
proclamation	O
de	O
la	O
république	O
portugaise	O
le	O
5	O
octobre	O
1910	O
et	O
la	O
déposition	O
de	O
Manuel	O
II	O
de	O
Bragance	O
.	O
Celle-ci	O
remplace	O
la	O
couronne	O
qui	O
figurait	O
sur	O
l'	O
ancien	O
drapeau	O
monarchique	O
et	O
symbolise	O
l'	O
empire	O
colonial	O
portugais	O
ainsi	O
que	O
les	O
découvertes	O
effectuées	O
par	O
le	O
Portugal	O
.	O
L'	O
histoire	O
dit	O
qu'	O
avant	O
la	O
bataille	O
d'	O
Ourique	O
(	O
26	O
juillet	O
1139	O
)	O
,	O
Afonso	PERSON
Henriques	PERSON
priait	O
pour	O
la	O
protection	O
des	O
Portugais	O
lorsqu'	O
il	O
eut	O
une	O
vision	O
de	O
Jésus	O
sur	O
la	O
croix	O
.	O
Afonso	PERSON
Henriques	PERSON
remporta	O
la	O
bataille	O
et	O
représenta	O
sur	O
le	O
drapeau	O
de	O
son	O
père	O
(	O
jusqu'	O
alors	O
une	O
croix	O
bleue	O
sur	O
fond	O
blanc	O
)	O
la	O
stigmatisation	O
en	O
y	O
apposant	O
les	O
cinq	O
plaies	O
du	O
martyr	O
en	O
signe	O
de	O
gratitude	O
envers	O
le	O
Christ	O
,	O
au	O
nom	O
duquel	O
il	O
avait	O
vaincu	O
ses	O
ennemis	O
.	O
On	O
considère	O
généralement	O
que	O
les	O
sept	O
châteaux	O
représentent	O
les	O
victoires	O
des	O
Portugais	O
sur	O
leurs	O
ennemis	O
et	O
le	O
royaume	O
de	O
l'	O
Algarve	O
.	O
Toutefois	O
les	O
châteaux	O
ont	O
été	O
introduits	O
dans	O
les	O
armoiries	O
du	O
Portugal	O
par	O
la	O
montée	O
au	O
trône	O
d'	O
Alphonse	PERSON
III	O
de	O
Portugal	O
.	O
Les	O
drapeaux	O
dérivaient	O
des	O
armoiries	O
utilisées	O
par	O
les	O
seigneurs	O
féodaux	O
(	O
le	O
premier	O
blason	O
à	O
avoir	O
été	O
étendu	O
en	O
pavillon	O
semble	O
avoir	O
été	O
celui	O
du	O
royaume	O
de	O
Jérusalem	O
,	O
sur	O
concession	O
du	O
pape	O
Urbain	O
II	O
)	O
.	O
Ainsi	O
,	O
Afonso	PERSON
Henriques	PERSON
aurait	O
mis	O
sur	O
ses	O
armoiries	O
les	O
trente	O
pièces	O
d'	O
argent	O
pour	O
lesquelles	O
Jésus	O
fut	O
vendu	O
(	O
ou	O
en	O
seconde	O
interprétation	O
,	O
les	O
cinq	O
blessures	O
de	O
ses	O
stigmates	O
)	O
.	O
La	O
tradition	O
dit	O
que	O
,	O
du	O
bouclier	O
que	O
D.	PERSON
Afonso	PERSON
Henriques	PERSON
a	O
reçu	O
de	O
son	O
père	O
,	O
avec	O
une	O
croix	O
bleue	O
à	O
laquelle	O
il	O
avait	O
superposé	O
les	O
besants	O
,	O
il	O
ne	O
restait	O
plus	O
que	O
les	O
clous	O
qui	O
représentaient	O
la	O
monnaie	O
et	O
des	O
petits	O
morceaux	O
de	O
tissu	O
bleu	O
qui	O
y	O
restaient	O
accrochés	O
,	O
donnant	O
ainsi	O
l'	O
impression	O
de	O
cinq	O
boucliers	O
pointus	O
que	O
le	O
drapeau	O
conserve	O
encore	O
aujourd'hui	O
.	O
Alphonse	PERSON
III	PERSON
n'	O
étant	O
pas	O
le	O
fils	O
premier-né	O
d'	O
Alphonse	PERSON
II	PERSON
,	O
à	O
l'	O
héritage	O
du	O
trône	O
de	O
son	O
frère	O
Sanche	PERSON
II	PERSON
par	O
l'	O
imposition	O
du	O
pape	O
Innocent	O
IV	O
,	O
il	O
ne	O
put	O
pas	O
utiliser	O
le	O
blason	O
de	O
son	O
père	O
sans	O
y	O
introduire	O
des	O
modifications	O
,	O
conformément	O
aux	O
pratiques	O
de	O
l'	O
époque	O
.	O
Néanmoins	O
,	O
la	O
tradition	O
a	O
conservé	O
une	O
autre	O
histoire	O
,	O
corroborée	O
par	O
d'	O
innombrables	O
chroniqueurs	O
tout	O
au	O
long	O
de	O
l'	O
histoire	O
:	O
les	O
châteaux	O
représenteraient	O
les	O
forteresses	O
prises	O
par	O
Alphonse	O
III	O
aux	O
maures	O
en	O
Algarve	O
.	O
Après	O
la	O
mort	O
de	O
Ferdinand	PERSON
en	O
1383	O
,	O
sa	O
fille	O
unique	O
Béatrice	PERSON
est	O
acclamée	O
reine	O
du	O
Portugal	O
dans	O
quelques	O
localités	O
.	O
C'	O
est	O
le	O
drapeau	O
castillan	O
capturé	O
à	O
Aljubarrota	LOCATION
.	O
