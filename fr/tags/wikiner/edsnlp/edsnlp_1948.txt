L'	O
Hydre	O
mâle	O
(	O
à	O
ne	O
pas	O
confondre	O
avec	O
l'	O
immense	O
Hydre	O
,	O
dite	O
"	O
Hydre	O
femelle	O
"	O
)	O
est	O
une	O
petite	O
constellation	O
située	O
près	O
du	O
pôle	O
sud	O
céleste	O
et	O
découpe	O
une	O
partie	O
de	O
l'	O
espace	O
située	O
sous	O
le	O
plan	O
de	O
la	O
Voie	O
lactée	O
.	O
La	O
constellation	O
de	O
l'	O
Hydre	O
mâle	O
est	O
une	O
création	O
moderne	O
.	O
Comme	O
les	O
autres	O
constellations	O
religieuses	O
de	O
Schiller	O
,	O
elle	O
ne	O
tarda	O
pas	O
à	O
tomber	O
en	O
désuétude	O
.	O
Dans	O
cette	O
direction	O
,	O
encore	O
~5°	O
plus	O
loin	O
,	O
on	O
trouve	O
un	O
petit	O
arc	O
de	O
cercle	O
formé	O
par	O
ε	O
,	O
δ	O
(	O
au	O
centre	O
)	O
et	O
η	O
Hyi	O
.	O
En	O
fait	O
,	O
les	O
limites	O
de	O
l'	O
Hydre	O
sont	O
tellement	O
anguleuses	O
que	O
les	O
trois	O
arêtes	O
de	O
son	O
triangle	O
principal	O
,	O
qui	O
relie	O
α	O
et	O
β	O
et	O
γ	O
Hyi	O
,	O
sortent	O
toutes	O
les	O
trois	O
de	O
la	O
constellation	O
.	O
β	O
Hydri	O
,	O
l'	O
étoile	O
la	O
plus	O
brillante	O
de	O
la	O
constellation	O
avec	O
une	O
magnitude	O
apparente	O
de	O
2,82	O
,	O
ressemble	O
beaucoup	O
au	O
Soleil	O
:	O
c'	O
est	O
une	O
étoile	O
de	O
la	O
séquence	O
principale	O
orange	O
,	O
juste	O
un	O
peu	O
plus	O
chaude	O
(	O
et	O
3	O
fois	O
plus	O
brillante	O
)	O
.	O
β	O
Hydri	O
est	O
l'	O
étoile	O
la	O
plus	O
proche	O
du	O
pôle	O
sud	O
céleste	O
qui	O
soit	O
relativement	O
lumineuse	O
;	O
elle	O
en	O
est	O
écartée	O
de	O
12°	O
environ	O
.	O
α	O
Hydri	O
est	O
une	O
étoile	O
de	O
la	O
séquence	O
principale	O
de	O
magnitude	O
apparente	O
2,86	O
.	O
