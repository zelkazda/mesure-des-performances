En	O
1989	O
,	O
il	O
est	O
élu	O
maire	O
de	O
Bouc-Bel-Air	LOCATION
,	O
dans	O
les	O
Bouches-du-Rhône	O
.	O
Il	O
est	O
réélu	O
en	O
1995	O
et	O
2001	O
,	O
année	O
où	O
il	O
devient	O
également	O
conseiller	O
général	O
du	O
canton	O
de	O
Gardanne	LOCATION
.	O
Candidat	O
pour	O
la	O
première	O
fois	O
aux	O
élections	O
législatives	O
en	O
2002	O
,	O
Richard	PERSON
Mallié	PERSON
est	O
élu	O
député	O
dans	O
la	O
10	O
e	O
circonscription	O
des	O
Bouches-du-Rhône	O
,	O
en	O
battant	O
le	O
député	O
communiste	O
.	O
Il	O
fait	O
partie	O
du	O
groupe	O
UMP	O
.	O
Par	O
ailleurs	O
,	O
Richard	PERSON
Mallié	PERSON
est	O
un	O
parlementaire	O
très	O
investi	O
dans	O
la	O
défense	O
de	O
la	O
cause	O
arménienne	O
.	O
Peu	O
d'	O
activités	O
ont	O
tenu	O
dans	O
l'	O
histoire	O
économique	O
et	O
sociale	O
de	O
la	O
France	O
une	O
place	O
aussi	O
importante	O
que	O
l'	O
exploitation	O
minière	O
.	O
