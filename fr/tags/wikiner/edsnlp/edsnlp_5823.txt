À	O
l'	O
intérieur	O
des	O
terres	O
,	O
Bacilly	LOCATION
se	O
trouve	O
tout	O
proche	O
de	O
la	O
Baie	O
du	O
Mont-Saint-Michel	O
.	O
Sous	O
l'	O
ancien	O
régime	O
on	O
trouve	O
plusieurs	O
fiefs	O
à	O
Bacilly	LOCATION
.	O
Un	O
fief	O
appelé	O
Bacilly	O
,	O
appartenant	O
jusqu'	O
au	O
treizième	O
siècle	O
à	O
des	O
seigneurs	O
de	O
ce	O
nom	O
puis	O
acquis	O
par	O
l'	O
abbaye	O
du	ORGANIZATION
Mont-Saint-Michel	ORGANIZATION
.	O
Ce	O
fief	O
était	O
vassal	O
de	O
celui	O
de	O
Saint-Pierre-Langers	LOCATION
.	O
Le	O
droit	O
de	O
présenter	O
à	O
la	O
cure	O
était	O
alternatif	O
entre	O
l'	O
évêque	O
et	O
l'	O
abbaye	O
du	ORGANIZATION
Mont-Saint-Michel	ORGANIZATION
.	O
En	O
1939	O
,	O
la	O
commune	O
a	O
servi	O
quelque	O
temps	O
de	O
base	O
de	O
repli	O
aux	O
éditions	O
Gallimard	O
.	O
Le	O
patron	O
Gaston	PERSON
Gallimard	PERSON
avait	O
mis	O
sa	O
propriété	O
,	O
alors	O
récemment	O
acquise	O
,	O
à	O
la	O
disposition	O
d'	O
une	O
partie	O
des	O
services	O
de	O
sa	O
société	O
.	O
