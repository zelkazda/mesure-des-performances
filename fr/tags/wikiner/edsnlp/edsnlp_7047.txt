Le	O
léopard	O
ou	O
panthère	O
est	O
une	O
espèce	O
de	O
félin	O
qui	O
vit	O
actuellement	O
en	O
Asie	O
et	O
en	O
Afrique	O
.	O
Bien	O
que	O
répandue	O
chez	O
d'	O
autres	O
félins	O
(	O
chats	O
,	O
jaguars	O
…	O
)	O
,	O
cette	O
mutation	O
affecte	O
préférentiellement	O
les	O
spécimens	O
asiatiques	O
comme	O
ceux	O
des	O
forêts	O
tropicales	O
des	O
îles	O
de	O
Java	LOCATION
et	LOCATION
Sumatra	LOCATION
et	O
surtout	O
en	O
Malaisie	O
,	O
où	O
une	O
panthère	O
sur	O
deux	O
est	O
noire	O
.	O
Il	O
existe	O
neuf	O
sous-espèces	O
de	O
léopard	O
selon	O
l'	O
Union	O
internationale	O
pour	O
la	O
conservation	O
de	O
la	O
nature	O
:	O
En	O
Inde	O
,	O
son	O
alimentation	O
comprend	O
des	O
singes	O
entelles	O
,	O
porcs	O
sauvages	O
,	O
cerf	O
axis	O
,	O
paons	O
,	O
jeunes	O
des	O
cerfs	O
sambar	O
et	O
jeunes	O
des	O
nilgauts	O
.	O
Sa	O
chasse	O
est	O
interdite	O
dans	O
la	O
plupart	O
des	O
pays	O
d'	O
Afrique	O
(	O
Angola	O
,	O
République	O
démocratique	O
du	O
Congo	O
,	O
Rwanda	O
,	O
etc	O
.	O
)	O
voire	O
très	O
réglementée	O
(	O
Afrique	O
du	O
Sud	O
,	O
Kenya	O
,	O
Namibie	O
,	O
Tanzanie	O
)	O
.	O
Cette	O
utilisation	O
du	O
léopard	O
semble	O
très	O
ancienne	O
;	O
des	O
légendes	O
persanes	O
en	O
font	O
mention	O
(	O
voir	O
article	O
"	O
Nimrod	O
"	O
)	O
.	O
Ces	O
deux	O
léopards	O
ont	O
été	O
tués	O
par	O
le	O
célèbre	O
chasseur	O
Jim	PERSON
Corbett	PERSON
.	O
Dans	O
certaines	O
tribus	O
bantoues	O
et	O
particulièrement	O
au	O
Zaïre	O
,	O
le	O
léopard	O
était	O
considéré	O
comme	O
un	O
animal	O
rusé	O
,	O
puissant	O
et	O
résistant	O
.	O
C'	O
est	O
la	O
raison	O
pour	O
laquelle	O
le	O
président	O
Mobutu	PERSON
Sese	PERSON
Seko	PERSON
portait	O
la	O
toque	O
et	O
certains	O
attributs	O
de	O
léopard	O
qui	O
le	O
rendaient	O
puissant	O
aux	O
yeux	O
de	O
la	O
population	O
.	O
Il	O
fait	O
maintenant	O
partie	O
des	O
armoiries	O
de	O
la	O
République	O
démocratique	O
du	O
Congo	O
.	O
