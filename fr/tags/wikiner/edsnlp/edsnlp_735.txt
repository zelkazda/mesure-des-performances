Kyōto	O
ou	O
Kyoto	O
(	O
京都	O
,	O
Kyōto	O
ou	O
Kyoto	O
?	O
littéralement	O
"	O
ville	O
capitale	O
"	O
)	O
est	O
une	O
ville	O
japonaise	O
de	O
la	O
région	O
du	O
Kansai	O
,	O
au	O
centre	O
de	O
Honshu	LOCATION
.	O
Elle	O
fut	O
de	O
794	O
à	O
1868	O
la	O
capitale	O
impériale	O
du	O
Japon	O
.	O
La	O
nouvelle	O
ville	O
,	O
Heiankyō	O
(	O
lit	O
.	O
Plus	O
tard	O
,	O
la	O
ville	O
fut	O
rebaptisée	O
Kyōto	O
(	O
"	O
la	O
ville	O
capitale	O
"	O
)	O
.	O
Par	O
la	O
suite	O
,	O
la	O
ville	O
fut	O
véritablement	O
dévastée	O
par	O
les	O
armées	O
lors	O
de	O
la	O
guerre	O
d'	O
Onin	O
,	O
abandonnée	O
en	O
grande	O
partie	O
par	O
ses	O
habitants	O
et	O
livrée	O
au	O
pillage	O
de	O
1467	O
à	O
1477	O
.	O
Aujourd'hui	O
,	O
les	O
principaux	O
quartiers	O
d'	O
affaires	O
sont	O
situés	O
au	O
sud	O
et	O
au	O
centre	O
de	O
la	O
ville	O
,	O
tandis	O
que	O
le	O
nord	O
et	O
Arashiyama	O
à	O
l'	O
ouest	O
,	O
sont	O
des	O
aires	O
à	O
l'	O
atmosphère	O
verdoyante	O
moins	O
peuplées	O
.	O
Ce	O
sont	O
des	O
divisions	O
municipales	O
disposant	O
d'	O
un	O
bureau	O
municipal	O
mais	O
elles	O
ne	O
sont	O
pas	O
,	O
comme	O
c'	O
est	O
le	O
cas	O
à	O
Tōkyō	O
,	O
dirigées	O
par	O
un	O
conseil	O
.	O
Plusieurs	O
temples	O
de	O
Kyōto	LOCATION
sont	O
classés	O
dans	O
le	O
patrimoine	O
mondial	O
de	O
l'	O
UNESCO	O
,	O
sous	O
le	O
nom	O
"	O
Monuments	O
historiques	O
de	O
l'	O
ancienne	O
Kyōto	LOCATION
(	LOCATION
villes	LOCATION
de	LOCATION
Kyōto	LOCATION
,	LOCATION
Uji	LOCATION
et	LOCATION
Ōtsu	LOCATION
)	O
"	O
.	O
Il	O
est	O
possible	O
de	O
parcourir	O
à	O
Kyōto	O
le	O
chemin	O
de	O
la	O
philosophie	O
,	O
chemin	O
qu'	O
empruntait	O
le	O
philosophe	O
Kitarō	O
Nishida	PERSON
tous	O
les	O
jours	O
,	O
afin	O
de	O
réfléchir	O
.	O
Kyōto	O
est	O
également	O
connue	O
pour	O
l'	O
abondance	O
de	O
ses	O
délicieuses	O
denrées	O
alimentaires	O
.	O
La	O
population	O
de	O
Kyōto	O
parle	O
un	O
dialecte	O
appelé	O
le	O
kyōto-ben	O
,	O
une	O
version	O
du	O
kansai-ben	O
.	O
Kyōto	O
est	O
connue	O
comme	O
un	O
des	O
centres	O
universitaires	O
importants	O
du	O
pays	O
,	O
et	O
accueille	O
37	O
établissements	O
d'	O
éducation	O
supérieure	O
.	O
Kyōto	O
a	O
également	O
un	O
réseau	O
important	O
d'	O
éducation	O
supérieure	O
appelé	O
le	O
consortium	O
des	O
universités	O
de	O
Kyōto	O
,	O
qui	O
se	O
compose	O
de	O
trois	O
établissements	O
nationaux	O
,	O
cinq	O
publics	O
(	O
préfectoral	O
et	O
municipal	O
)	O
,	O
et	O
41	O
universités	O
privées	O
.	O
Le	O
tourisme	O
constitue	O
une	O
importante	O
part	O
de	O
l'	O
économie	O
de	O
Kyōto	LOCATION
.	O
L'	O
industrie	O
de	O
Kyōto	O
est	O
principalement	O
composée	O
de	O
petites	O
installations	O
,	O
la	O
plupart	O
desquelles	O
est	O
gérée	O
par	O
des	O
artisans	O
traditionnels	O
japonais	O
.	O
Les	O
kimonos	O
de	O
Kyōto	O
sont	O
particulièrement	O
reconnus	O
et	O
la	O
ville	O
demeure	O
le	O
premier	O
centre	O
de	O
fabrication	O
de	O
kimonos	O
japonais	O
.	O
La	O
seule	O
grande	O
industrie	O
de	O
Kyōto	LOCATION
est	O
celle	O
de	O
l'	O
électronique	O
.	O
Beaucoup	O
de	O
visiteurs	O
arrivent	O
à	O
Kyōto	LOCATION
depuis	O
Tōkyō	O
,	O
par	O
le	O
biais	O
du	O
Shinkansen	O
.	O
La	O
gare	O
de	O
Kyōto	O
est	O
à	O
elle	O
seule	O
un	O
monument	O
qu'	O
il	O
est	O
intéressant	O
de	O
visiter	O
.	O
À	O
Kyōto	O
sont	O
nés	O
ou	O
décédés	O
:	O
