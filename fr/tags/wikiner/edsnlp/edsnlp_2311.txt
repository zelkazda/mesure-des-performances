L'	O
Inde	O
,	O
comme	O
d'	O
autres	O
pays	O
en	O
développement	O
,	O
a	O
vu	O
son	O
économie	O
croître	O
de	O
façon	O
très	O
importante	O
et	O
s'	O
ajuster	O
à	O
celle	O
des	O
pays	O
développés	O
.	O
Le	O
développement	O
de	O
sites	O
web	O
de	O
réseautage	O
social	O
comme	O
Facebook	O
et	O
MySpace	O
a	O
aussi	O
permis	O
aux	O
gens	O
de	O
rester	O
en	O
contact	O
avec	O
parents	O
,	O
amis	O
et	O
collègues	O
où	O
qu'	O
ils	O
soient	O
sur	O
la	O
planète	O
.	O
L'	O
utilisation	O
d'	O
Internet	O
à	O
des	O
fins	O
commerciales	O
s'	O
est	O
aussi	O
popularisée	O
,	O
remplaçant	O
plusieurs	O
façons	O
traditionnelles	O
de	O
se	O
procurer	O
des	O
produits	O
et	O
services	O
.	O
La	O
Guerre	O
d'	O
Afghanistan	O
débute	O
en	O
octobre	O
2001	O
,	O
et	O
entraine	O
un	O
conflit	O
au	O
Nord-Ouest	O
du	O
Pakistan	O
,	O
qui	O
s'	O
intensifie	O
à	O
la	O
fin	O
de	O
la	O
décennie	O
.	O
La	O
Corée	O
du	O
Nord	O
a	O
créé	O
une	O
crise	O
nucléaire	O
en	O
se	O
retirant	O
du	O
Traité	O
sur	O
la	O
non-prolifération	O
des	O
armes	O
nucléaires	O
et	O
en	O
menant	O
à	O
bien	O
des	O
essais	O
nucléaires	O
.	O
Parmi	O
les	O
enjeux	O
sociaux	O
,	O
notons	O
le	O
mariage	O
homosexuel	O
(	O
qui	O
a	O
été	O
légalisé	O
dans	O
plusieurs	O
pays	O
occidentaux	O
,	O
dont	O
le	O
Canada	O
,	O
les	O
Pays-Bas	O
,	O
la	O
Belgique	O
et	O
l'	O
Espagne	O
)	O
,	O
ainsi	O
que	O
l'	O
égalité	O
des	O
sexes	O
,	O
les	O
droits	O
de	O
la	O
personne	O
et	O
les	O
soins	O
de	O
santé	O
.	O
Ils	O
ont	O
déclenché	O
une	O
guerre	O
contre	O
le	O
terrorisme	O
,	O
avec	O
la	O
guerre	O
d'	O
Afghanistan	O
(	O
2001-2009	O
)	O
et	O
la	O
guerre	O
d'	O
Irak	O
(	O
2003-2009	O
)	O
.	O
Cette	O
dernière	O
a	O
causé	O
la	O
chute	O
du	O
régime	O
de	O
Saddam	O
Hussein	O
en	O
2003	O
.	O
Quant	O
à	O
la	O
guerre	O
d'	O
Afghanistan	O
,	O
elle	O
entraine	O
un	O
conflit	O
armé	O
au	O
Nord-Ouest	O
du	O
Pakistan	O
qui	O
après	O
quelques	O
combats	O
qui	O
commence	O
en	O
2004	O
,	O
s'	O
intensifie	O
réellement	O
à	O
partir	O
de	O
2007	O
et	O
2009	O
.	O
La	O
seconde	O
guerre	O
du	O
Congo	O
est	O
terminée	O
en	O
2003	O
.	O
En	O
2008	O
,	O
un	O
conflit	O
armée	O
oppose	O
la	O
Russie	O
et	O
la	O
Géorgie	O
,	O
entrainant	O
la	O
reconnaissance	O
de	O
l'	O
indépendance	O
de	O
Abkhazie	O
et	O
l'	O
Ossétie	O
du	O
Sud	O
par	O
la	O
Russie	O
.	O
La	O
guerre	O
du	O
Kivu	O
en	O
République	O
démocratique	O
du	O
Congooppose	O
depuis	O
2004	O
les	O
forces	O
régulières	O
de	O
l'	O
armée	O
à	O
des	O
rebelles	O
(	O
Congrès	O
national	O
pour	O
la	O
défense	O
du	O
peuple	O
)	O
.	O
A	O
l'	O
ouest	O
du	O
Soudan	O
,	O
la	O
guerre	O
civile	O
au	O
Darfour	O
liée	O
à	O
des	O
tensions	O
ethniques	O
aurait	O
abouti	O
au	O
génocide	O
de	O
300000	O
personnes	O
,	O
et	O
aurait	O
fait	O
des	O
millions	O
de	O
déplacés	O
et	O
réfugiés	O
depuis	O
2003	O
.	O
Les	O
victimes	O
ont	O
fui	O
le	O
Darfour	O
vers	O
d'	O
autres	O
régions	O
du	O
Soudan	O
ou	O
le	O
Tchad	O
.	O
Al-Qaida	O
ou	O
des	O
groupuscules	O
islamistes	O
indépendants	O
revendiquent	O
ou	O
sont	O
soupçonnés	O
dans	O
les	O
attentats	O
de	O
Bali	O
le	O
12	O
octobre	O
2002	O
,	O
les	O
attentats	O
du	O
11	O
mars	O
2004	O
à	O
Madrid	LOCATION
,	O
les	O
attentats	O
du	O
7	O
juillet	O
2005	O
à	O
Londres	LOCATION
,	O
les	O
attentats-suicides	O
d'	O
Istanbul	O
de	O
novembre	O
2003	O
,	O
les	O
attaques	O
de	O
novembre	O
2008	O
à	O
Bombay	LOCATION
et	O
de	O
nombreux	O
autres	O
en	O
Irak	O
,	O
en	O
Algérie	O
,	O
au	O
Pakistan	O
,	O
en	O
Inde	O
,	O
en	O
Palestine	O
et	O
Israël	O
...	O
Deux	O
prises	O
d'	O
otage	O
ont	O
par	O
ailleurs	O
marqué	O
la	O
décennie	O
,	O
la	O
prise	O
d'	O
otages	O
du	O
théâtre	ORGANIZATION
de	ORGANIZATION
Moscou	ORGANIZATION
en	O
octobre	O
2002	O
et	O
la	O
prise	O
d'	O
otages	O
de	O
Beslan	LOCATION
en	O
Ossétie	O
du	O
Nord	O
en	O
septembre	O
2004	O
.	O
Le	O
Hamas	O
prend	O
le	O
contrôle	O
de	O
la	O
Bande	O
de	O
Gaza	O
.	O
Ingrid	PERSON
Bétancourt	PERSON
,	O
journaliste	O
détenue	O
durant	O
7	O
ans	O
,	O
représente	O
en	O
France	O
l'	O
ensemble	O
des	O
hommes	O
et	O
femmes	O
retenues	O
,	O
kidnappées	O
ou	O
tuées	O
lors	O
des	O
conflits	O
.	O
Vladimir	PERSON
Poutine	PERSON
domine	O
la	O
scène	O
politique	O
russe	O
,	O
en	O
tant	O
que	O
président	O
de	O
la	O
fédération	O
de	O
Russie	O
jusqu'	O
en	O
2008	O
puis	O
du	O
gouvernement	O
depuis	O
.	O
On	O
peut	O
également	O
nommer	O
Nicolas	PERSON
Sarkozy	PERSON
,	O
Tony	PERSON
Blair	PERSON
,	O
José	PERSON
María	PERSON
Aznar	O
,	O
Hugo	PERSON
Chávez	O
,	O
Hu	PERSON
Jintao	PERSON
,	O
Silvio	PERSON
Berlusconi	PERSON
,	O
Stephen	PERSON
Harper	PERSON
...	O
Une	O
évolution	O
est	O
perceptible	O
vers	O
plus	O
d'	O
égalité	O
des	O
chances	O
au	O
sein	O
des	O
grands	O
politiciens	O
,	O
avec	O
par	O
exemple	O
l'	O
élection	O
du	O
premier	O
métis	O
aux	O
États-Unis	O
d'	O
Amérique	O
comme	O
président	O
,	O
Barack	PERSON
Obama	PERSON
,	O
le	O
4	O
novembre	O
2008	O
;	O
la	O
campagne	O
électorale	O
américaine	O
a	O
également	O
laissé	O
penser	O
la	O
possibilité	O
d'	O
une	O
première	O
femme	O
présidente	O
,	O
avec	O
la	O
candidature	O
d'	O
Hillary	PERSON
Clinton	PERSON
.	O
Dans	O
la	O
guerre	O
contre	O
le	O
terrorisme	O
,	O
Oussama	O
Ben	O
Laden	O
revendique	O
les	O
attentats	O
du	O
11	O
septembre	O
2001	O
et	O
demeure	O
actuellement	O
recherché	O
.	O
La	O
Constitution	O
européenne	O
est	O
rejetée	O
par	O
la	O
France	O
et	O
par	O
les	O
Pays-Bas	O
après	O
un	O
référendum	O
(	O
2005	O
)	O
.	O
La	O
Belgique	O
vit	O
depuis	O
2007	O
une	O
crise	O
politique	O
,	O
avec	O
la	O
démission	O
de	O
son	O
gouvernement	O
en	O
2008	O
.	O
Au	O
niveau	O
religieux	O
,	O
Jean-Paul	O
II	O
a	O
cédé	O
sa	O
place	O
de	O
pape	O
à	O
l'	O
allemand	O
Benoît	O
XVI	O
à	O
sa	O
mort	O
en	O
2005	O
.	O
La	O
décennie	O
est	O
marquée	O
par	O
la	O
forte	O
croissance	O
économique	O
en	O
Chine	O
et	O
en	O
Inde	O
.	O
Le	O
monde	O
connait	O
durant	O
la	O
décennie	O
sa	O
plus	O
forte	O
crise	O
financière	O
depuis	O
1930	O
.	O
La	O
prise	O
de	O
conscience	O
des	O
dangers	O
liés	O
au	O
réchauffement	O
climatique	O
est	O
internationale	O
,	O
les	O
engagements	O
pour	O
lutter	O
contre	O
ce	O
phénomène	O
se	O
font	O
progressivement	O
,	O
en	O
Europe	O
puis	O
aux	O
États-Unis	O
d'	O
Amérique	O
à	O
l'	O
arrivée	O
de	O
Barack	O
Obama	O
.	O
Les	O
pays	O
d'	O
Asie	O
du	O
Sud-Est	O
,	O
en	O
plein	O
développement	O
,	O
connaissent	O
une	O
poussée	O
d'	O
intérêts	O
.	O
L'	O
usage	O
d'	O
Internet	O
et	O
de	O
la	O
haute	O
vitesse	O
se	O
vulgarisent	O
.	O
Les	O
téléphones	O
intelligents	O
ou	O
smartphones	O
sont	O
introduits	O
en	O
2001	O
,	O
mais	O
leur	O
volume	O
explose	O
véritablement	O
en	O
2007	O
avec	O
l'	O
arrivée	O
de	O
l'	O
I-Phone	O
.	O
La	O
Télévision	O
numérique	O
terrestre	O
(	O
TNT	O
)	O
est	O
lancée	O
.	O
La	O
révolution	O
numérique	O
,	O
considérée	O
comme	O
la	O
quatrième	O
révolution	O
scientifique	O
après	O
celles	O
enclenchées	O
par	O
les	O
découvertes	O
de	O
Copernic	O
,	O
Darwin	O
et	O
Freud	O
,	O
conduit	O
à	O
l'	O
ère	O
de	O
l'	O
information	O
.	O
Le	O
monde	O
devient	O
plus	O
interactif	O
,	O
artificiellement	O
"	O
vivant	O
"	O
ou	O
"	O
intelligent	O
"	O
(	O
inventions	O
de	O
machines	O
,	O
électroménagers	O
connectés	O
)	O
.	O
Des	O
micro-organismes	O
sont	O
également	O
découverts	O
,	O
dans	O
la	O
fosse	O
des	O
Mariannes	O
,	O
en	O
2005	O
.	O
Plusieurs	O
recherches	O
généalogiques	O
(	O
identification	O
de	O
restes	O
humains	O
tels	O
que	O
ceux	O
de	O
Copernic	O
en	O
2008	O
)	O
et	O
archéologiques	O
sont	O
affinées	O
.	O
On	O
découvre	O
la	O
présence	O
d'	O
eau	O
gelée	O
sur	O
la	O
Lune	O
et	O
sur	O
Mars	O
,	O
et	O
on	O
détecte	O
de	O
nouvelles	O
planètes	O
extra-solaires	O
(	O
environ	O
300	O
en	O
2008	O
)	O
.	O
On	O
découvre	O
un	O
volcan	O
sous-marin	O
inactif	O
au	O
sud-ouest	O
de	O
la	O
Sicile	O
en	O
2006	O
.	O
Plusieurs	O
hommes	O
établissent	O
des	O
records	O
historiques	O
et	O
marquent	O
cette	O
décennie	O
au	O
niveau	O
sportif	O
:	O
Roger	PERSON
Federer	O
et	O
Rafael	PERSON
Nadal	O
en	O
tennis	O
,	O
Michael	PERSON
Phelps	PERSON
en	O
natation	O
(	O
notamment	O
aux	O
JO	O
de	O
Pékin	O
)	O
,	O
Zinedine	PERSON
Zidane	PERSON
au	O
début	O
de	O
la	O
décennie	O
en	O
football	O
,	O
Usain	PERSON
Bolt	O
en	O
athlétisme	O
,	O
Lance	O
Armstrong	O
en	O
cyclisme	O
,	O
Michael	PERSON
Schumacher	PERSON
en	O
courses	O
automobiles	O
.	O
Les	O
années	O
2000	O
sont	O
donc	O
marquées	O
par	O
un	O
retour	O
général	O
vers	O
les	O
succès	O
d'	O
antan	O
,	O
propice	O
à	O
plusieurs	O
retours	O
d'	O
artistes	O
tels	O
que	O
Madness	O
,	O
Carlos	PERSON
Santana	PERSON
,	O
,	O
ainsi	O
que	O
par	O
l'	O
estompement	O
progressif	O
des	O
barrières	O
entre	O
les	O
genres	O
:	O
se	O
banalisent	O
les	O
genres	O
hybrides	O
tels	O
que	O
l'	O
electro-pop	O
(	O
Calvin	O
Harris	O
,	O
Empire	O
of	O
the	O
Sun	O
)	O
,	O
le	O
rap	O
rock	O
(	O
Street	O
Sweeper	O
Social	O
Club	O
)	O
ou	O
l'	O
electro	O
rock	O
.	O
Les	O
super-héros	O
ont	O
également	O
la	O
part	O
belle	O
:	O
Batman	O
(	O
Batman	O
Begins	O
en	O
2005	O
et	O
The	O
Dark	O
Knight	O
:	O
Le	O
Chevalier	O
noir	O
en	O
2008	O
)	O
,	O
Iron	O
Man	O
en	O
2008	O
,	O
Spider-Man	O
(	O
1	O
,	O
2	O
et	O
3	O
en	O
2002	O
,	O
2004	O
,	O
2007	O
)	O
.	O
Plusieurs	O
sont	O
des	O
biographies	O
de	O
chanteurs	O
,	O
comme	O
Ray	PERSON
en	O
2005	O
,	O
ou	O
de	O
sportifs	O
,	O
comme	O
Ali	PERSON
en	O
2002	O
.	O
La	O
musique	O
perce	O
également	O
avec	O
le	O
film	O
Les	O
Choristes	O
en	O
2004	O
.	O
En	O
lien	O
avec	O
l'	O
évolution	O
de	O
la	O
société	O
,	O
plusieurs	O
films	O
sont	O
engagés	O
,	O
dont	O
Fahrenheit	O
9/11	O
en	O
2004	O
,	O
Good	O
Night	O
,	O
and	O
Good	O
Luck	O
en	O
2005	O
,	O
Une	O
vérité	O
qui	O
dérange	O
en	O
2006	O
.	O
Différents	O
films	O
sont	O
inspirés	O
de	O
la	O
vie	O
quotidienne	O
,	O
par	O
exemple	O
Tanguy	O
et	O
Le	O
Fabuleux	O
Destin	O
d'	O
Amélie	PERSON
Poulain	O
en	O
2001	O
,	O
L'	O
Auberge	O
espagnole	O
en	O
2002	O
,	O
Volver	O
en	O
2006	O
,	O
Slumdog	O
Millionaire	O
en	O
2008	O
.	O
Clint	PERSON
Eastwood	PERSON
a	O
marqué	O
par	O
deux	O
œuvres	O
récompensées	O
:	O
Mystic	O
river	O
en	O
2003	O
et	O
Million	O
Dollar	O
Baby	O
en	O
2005	O
.	O
On	O
peut	O
citer	O
Donnie	O
Darko	O
,	O
American	O
psycho	O
et	O
Requiem	O
for	O
a	O
dream	O
en	O
2000	O
,	O
Eternal	O
sunshine	O
of	O
the	O
spotless	O
mind	O
en	O
2004	O
,	O
Brick	O
en	O
2006	O
.	O
A	O
noter	O
parmi	O
les	O
nombreux	O
films	O
comiques	O
,	O
la	O
percée	O
inattendue	O
au	O
box	O
office	O
français	O
de	O
Bienvenue	O
chez	O
les	O
Ch'tis	O
en	O
2008	O
,	O
et	O
les	O
adaptations	O
internationales	O
.	O
À	O
noter	O
que	O
la	O
plupart	O
ne	O
font	O
plus	O
l'	O
objet	O
de	O
censures	O
au	O
niveau	O
des	O
dialogues	O
ou	O
des	O
actions	O
,	O
comme	O
c'	O
était	O
le	O
cas	O
auparavant	O
pour	O
les	O
adaptations	O
en	O
France	O
.	O
La	O
Playstation	O
2	O
fait	O
son	O
apparition	O
,	O
et	O
est	O
durant	O
cette	O
décennie	O
la	O
console	O
la	O
plus	O
vendue	O
de	O
tous	O
les	O
temps	O
(	O
140	O
millions	O
d'	O
exemplaires	O
vendus	O
[	O
réf.	O
nécessaire	O
]	O
)	O
.	O
La	O
Wii	O
introduit	O
un	O
nouveau	O
type	O
de	O
jouabilité	O
pour	O
les	O
joueurs	O
occasionnels	O
.	O
Avec	O
l'	O
avènement	O
d'	O
Internet	O
,	O
les	O
jeux	O
de	O
rôle	O
en	O
ligne	O
massivement	O
multijoueurs	O
(	O
MMORPG	O
en	O
anglais	O
)	O
se	O
développent	O
,	O
comme	O
World	O
of	O
Warcraft	O
avec	O
11,5	O
millions	O
de	O
comptes	O
actifs	O
dans	O
différents	O
continents	O
fin	O
2008	O
.	O
