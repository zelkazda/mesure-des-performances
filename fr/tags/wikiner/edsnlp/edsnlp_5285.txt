L'	O
Association	O
des	O
nations	O
de	O
l'	O
Asie	O
du	O
Sud-Est	O
(	O
ANASE	O
ou	O
ASEAN	O
)	O
est	O
une	O
organisation	O
politique	O
,	O
économique	O
et	O
culturelle	O
regroupant	O
dix	O
pays	O
d'	O
Asie	O
du	O
Sud-Est	O
.	O
Son	O
secrétariat	O
général	O
est	O
installé	O
à	O
Jakarta	LOCATION
(	O
Indonésie	O
)	O
.	O
Le	O
sigle	O
ANSEA	O
(	O
Association	O
des	O
nations	O
du	O
Sud-Est	O
asiatique	O
)	O
n'	O
est	O
plus	O
utilisé	O
.	O
Le	O
Brunei	O
les	O
rejoint	O
6	O
jours	O
après	O
son	O
indépendance	O
du	O
Royaume-Uni	O
le	O
8	O
janvier	O
1984	O
.	O
Le	O
Vietnam	O
entre	O
en	O
1995	O
,	O
suivi	O
du	O
Laos	O
et	O
de	O
la	O
Birmanie	O
le	O
23	O
juillet	O
1997	O
et	O
du	O
Cambodge	O
le	O
30	O
avril	O
1999	O
.	O
La	O
Papouasie-Nouvelle-Guinée	O
a	O
le	O
statut	O
d'	O
observateur	O
depuis	O
1976	O
et	O
réfléchit	O
à	O
une	O
possible	O
candidature	O
.	O
Le	O
23	O
juillet	O
2006	O
,	O
le	O
Timor	O
oriental	O
a	O
posé	O
sa	O
candidature	O
et	O
pourrait	O
devenir	O
membre	O
d'	O
ici	O
à	O
2012	O
.	O
L'	O
Australie	O
est	O
aussi	O
intéressée	O
mais	O
certains	O
pays	O
membres	O
s'	O
y	O
opposent	O
[	O
réf.	O
souhaitée	O
]	O
.	O
Deuxièmement	O
la	O
volonté	O
que	O
la	O
région	O
ne	O
soit	O
pas	O
utilisée	O
comme	O
terrain	O
de	O
bataille	O
de	O
la	O
guerre	O
froide	O
,	O
notamment	O
par	O
une	O
extension	O
de	O
la	O
guerre	O
du	O
Vietnam	O
.	O
L'	O
ASEAN	O
n'	O
a	O
jamais	O
été	O
envisagée	O
comme	O
une	O
alliance	O
militaire	O
.	O
L'	O
ASEAN	O
reste	O
minimale	O
durant	O
la	O
première	O
décennie	O
et	O
s'	O
articule	O
principalement	O
autour	O
de	O
la	O
rencontre	O
annuelle	O
des	O
ministres	O
des	O
affaires	O
étrangères	O
.	O
Ces	O
déclarations	O
traduisent	O
le	O
souhait	O
des	O
pays	O
membres	O
de	O
coexister	O
pacifiquement	O
avec	O
leurs	O
voisins	O
communistes	O
:	O
le	O
Cambodge	O
,	O
le	O
Laos	O
et	O
surtout	O
le	O
Viêt	O
Nam	O
unifié	O
l'	O
année	O
précédente	O
.	O
Une	O
seconde	O
rencontre	O
en	O
1977	O
à	O
Kuala	LOCATION
Lumpur	LOCATION
réaffirme	O
la	O
politique	O
de	O
1976	O
.	O
Fin	O
décembre	O
1978	O
,	O
le	O
Viêt	O
Nam	O
envahit	O
le	O
Cambodge	O
des	O
Khmers	O
rouges	O
qu'	O
il	O
occupera	O
pendant	O
une	O
décennie	O
.	O
Les	O
pays	O
membres	O
cherchent	O
à	O
résoudre	O
la	O
crise	O
en	O
passant	O
par	O
l'	O
ASEAN	O
.	O
Un	O
accord	O
émerge	O
entre	O
eux	O
,	O
consistant	O
en	O
l'	O
isolement	O
du	O
Viêt	O
Nam	O
tout	O
en	O
offrant	O
au	O
Viêt	O
Nam	O
la	O
possibilité	O
de	O
négocier	O
pour	O
retirer	O
ses	O
troupes	O
du	O
Cambodge	O
.	O
Ils	O
convainquent	O
les	O
institutions	O
internationales	O
et	O
les	O
pays	O
occidentaux	O
de	O
mettre	O
une	O
pression	O
diplomatique	O
et	O
financière	O
sur	O
le	O
Viêt	O
Nam	O
qui	O
permettra	O
parmi	O
d'	O
autres	O
facteurs	O
de	O
résoudre	O
la	O
crise	O
.	O
L'	O
opposition	O
commune	O
à	O
l'	O
occupation	O
du	O
Viêt	O
Nam	O
permet	O
aux	O
pays	O
de	O
l'	O
ASEAN	O
de	O
consolider	O
leurs	O
liens	O
politiques	O
.	O
L'	O
ASEAN	O
acquiert	O
une	O
stature	O
et	O
une	O
crédibilité	O
dans	O
la	O
communauté	O
internationale	O
qui	O
renforce	O
en	O
retour	O
les	O
pays	O
membres	O
.	O
Le	O
bloc	O
s'	O
agrandit	O
avec	O
l'	O
entrée	O
du	O
Brunei	O
le	O
8	O
janvier	O
1984	O
,	O
une	O
semaine	O
après	O
son	O
indépendance	O
.	O
Les	O
grandes	O
puissances	O
commencent	O
à	O
considérer	O
les	O
pays	O
de	O
l'	O
ASEAN	O
comme	O
des	O
partenaires	O
commerciaux	O
.	O
L'	O
ASEAN	O
réussit	O
à	O
rassembler	O
les	O
grandes	O
puissances	O
et	O
les	O
pays	O
membres	O
sont	O
ainsi	O
garantis	O
d'	O
être	O
au	O
centre	O
et	O
de	O
peser	O
sur	O
les	O
débats	O
sur	O
la	O
sécurité	O
de	O
la	O
région	O
.	O
La	O
fin	O
de	O
la	O
guerre	O
froide	O
en	O
Asie	O
du	O
Sud-Est	O
peut	O
être	O
datée	O
du	O
retrait	O
de	O
l'	O
armée	O
vietnamienne	O
du	O
Cambodge	O
en	O
1989	O
.	O
La	O
crainte	O
du	O
Viêt	O
Nam	O
constituait	O
le	O
ciment	O
de	O
l'	O
ASEAN	O
.	O
Les	O
négociations	O
pour	O
aboutir	O
à	O
l'	O
ASEAN	O
10	O
débutent	O
après	O
le	O
sommet	O
de	O
Singapour	O
et	O
aboutissent	O
à	O
l'	O
entrée	O
du	O
Vietnam	O
(	O
1995	O
)	O
,	O
puis	O
du	O
Laos	O
et	O
de	O
la	O
Birmanie	O
(	O
1997	O
)	O
et	O
enfin	O
du	O
Cambodge	O
(	O
1999	O
)	O
.	O
L'	O
ASEAN	O
élargie	O
connaît	O
cependant	O
des	O
problèmes	O
et	O
des	O
divisions	O
entre	O
les	O
pays	O
fondateurs	O
et	O
les	O
nouveaux	O
arrivants	O
.	O
Les	O
premiers	O
sont	O
en	O
partie	O
des	O
démocraties	O
(	O
Thaïlande	O
,	O
Philippines	O
,	O
Indonésie	O
)	O
,	O
sont	O
plus	O
riches	O
et	O
veulent	O
accélérer	O
l'	O
intégration	O
alors	O
que	O
les	O
seconds	O
sont	O
parfois	O
autoritaires	O
(	O
Birmanie	O
et	O
Vietnam	O
)	O
,	O
plus	O
pauvres	O
et	O
souhaitent	O
le	O
statu	O
quo	O
dans	O
les	O
institutions	O
.	O
Ces	O
divisions	O
affaiblissent	O
l'	O
ASEAN	O
dans	O
les	O
négociations	O
internationales	O
et	O
donnent	O
le	O
sentiment	O
à	O
la	O
fin	O
de	O
la	O
décennie	O
que	O
l'	O
organisation	O
stagne	O
.	O
La	O
crise	O
économique	O
asiatique	O
débute	O
par	O
une	O
crise	O
monétaire	O
,	O
avec	O
la	O
dévaluation	O
du	O
baht	O
thaïlandais	O
en	O
juillet	O
1997	O
et	O
se	O
répand	O
en	O
Indonésie	O
et	O
en	O
Malaisie	O
et	O
dans	O
une	O
moindre	O
mesure	O
aux	O
autres	O
pays	O
de	O
l'	O
ASEAN	O
.	O
La	O
crise	O
montre	O
l'	O
incapacité	O
de	O
l'	O
ASEAN	O
à	O
régler	O
la	O
crise	O
mais	O
convainc	O
les	O
pays	O
d'	O
améliorer	O
la	O
coopération	O
financière	O
et	O
monétaire	O
pour	O
éviter	O
une	O
nouvelle	O
crise	O
.	O
Il	O
prend	O
ses	O
origines	O
dans	O
la	O
préparation	O
de	O
la	O
première	O
Réunion	O
Asie-Europe	O
(	O
ASEM	O
)	O
.	O
En	O
mai	O
2000	O
à	O
Chiang	LOCATION
Mai	LOCATION
,	O
ils	O
s'	O
accordent	O
pour	O
lutter	O
contre	O
une	O
nouvelle	O
crise	O
financière	O
.	O
Un	O
accord	O
cadre	O
est	O
signé	O
en	O
2002	O
en	O
vue	O
d'	O
établir	O
la	O
zone	O
en	O
2010	O
pour	O
l'	O
ASEAN	O
6	O
et	O
en	O
2015	O
pour	O
l'	O
ASEAN	O
au	O
complet	O
.	O
Des	O
initiatives	O
similaires	O
ont	O
été	O
lancées	O
en	O
réponse	O
par	O
le	O
Japon	O
et	O
la	O
Corée	O
.	O
Ce	O
dernier	O
est	O
le	O
plus	O
important	O
des	O
trois	O
et	O
a	O
été	O
signé	O
suite	O
à	O
une	O
pression	O
considérable	O
exercée	O
par	O
les	O
États-Unis	O
.	O
L'	O
attentat	O
de	O
Bali	O
du	O
12	O
octobre	O
2002	O
accroît	O
la	O
pression	O
sur	O
les	O
pays	O
de	O
l'	O
ASEAN	O
.	O
L'	O
ASEAN	O
tient	O
des	O
sommets	O
où	O
les	O
chefs	O
de	O
gouvernement	O
de	O
chaque	O
pays	O
membre	O
discutent	O
des	O
questions	O
régionales	O
.	O
Le	O
premier	O
sommet	O
se	O
tient	O
à	O
Bali	LOCATION
en	O
Indonésie	O
en	O
1976	O
.	O
Au	O
3	O
e	O
sommet	O
de	O
1987	O
de	O
Manille	O
,	O
il	O
est	O
décidé	O
qu'	O
il	O
aura	O
lieu	O
tous	O
les	O
cinq	O
ans	O
.	O
Le	O
4	O
e	O
sommet	O
se	O
tient	O
donc	O
à	O
Singapour	LOCATION
en	O
1992	O
où	O
les	O
dirigeants	O
décident	O
de	O
se	O
réunir	O
tous	O
les	O
trois	O
ans	O
.	O
Elles	O
incluent	O
la	O
rencontre	O
annuelle	O
ministérielle	O
de	O
l'	O
ASEAN	O
ainsi	O
que	O
d'	O
autres	O
rencontres	O
qui	O
se	O
concentrent	O
sur	O
des	O
sujets	O
spécifiques	O
comme	O
la	O
défense	O
ou	O
l'	O
environnement	O
où	O
les	O
ministres	O
discutent	O
et	O
non	O
les	O
chefs	O
de	O
gouvernement	O
.	O
Le	O
premier	O
sommet	O
s'	O
est	O
tenu	O
à	O
Kuala	LOCATION
Lumpur	LOCATION
le	O
14	O
décembre	O
2005	O
.	O
La	O
zone	O
de	O
libre-échange	O
de	O
l'	O
ASEAN	O
est	O
un	O
accord	O
entre	O
les	O
nations	O
de	O
l'	O
ASEAN	O
Les	O
Jeux	O
d'	O
Asie	O
du	O
Sud-Est	O
,	O
appelés	O
SEA	O
Games	O
,	O
sont	O
une	O
compétition	O
multisports	O
qui	O
a	O
lieu	O
tous	O
les	O
deux	O
ans	O
et	O
auxquels	O
participent	O
11	O
pays	O
.	O
Le	O
Championnat	O
de	O
l'	O
ASEAN	O
de	O
football	O
est	O
une	O
compétition	O
sportive	O
internationale	O
opposant	O
les	O
sélections	O
nationales	O
de	O
football	O
de	O
l'	O
association	O
.	O
Elle	O
a	O
débuté	O
en	O
1996	O
sous	O
le	O
nom	O
Tiger	O
Cup	O
.	O
Les	O
ASEAN	O
ParaGames	O
sont	O
une	O
rencontre	O
multisports	O
pour	O
athlètes	O
handicapés	O
auxquels	O
participent	O
11	O
pays	O
d'	O
Asie	O
du	O
Sud-Est	O
qui	O
a	O
lieu	O
tous	O
les	O
deux	O
ans	O
.	O
