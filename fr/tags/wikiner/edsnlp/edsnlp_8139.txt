L'	O
homéopathie	O
ou	O
homœopathie	O
est	O
une	O
médecine	O
non	O
conventionnelle	O
,	O
inventée	O
par	O
Samuel	PERSON
Hahnemann	PERSON
en	O
1796	O
qui	O
consiste	O
à	O
administrer	O
au	O
malade	O
des	O
doses	O
faibles	O
ou	O
infinitésimales	O
obtenues	O
par	O
dilution	O
et	O
agitation	O
(	O
la	O
dynamisation	O
)	O
d'	O
une	O
substance	O
choisie	O
en	O
fonction	O
de	O
l'	O
état	O
physique	O
et	O
psychique	O
du	O
patient	O
.	O
Le	O
taux	O
de	O
recours	O
annuel	O
va	O
de	O
2	O
%	O
au	O
Royaume-Uni	O
à	O
36	O
%	O
en	O
France	O
.	O
Les	O
pharmaciens	O
refusant	O
de	O
produire	O
ces	O
produits	O
[	O
citation	O
nécessaire	O
]	O
,	O
les	O
disciples	O
d'	O
Hahnemann	PERSON
durent	O
les	O
fabriquer	O
eux-mêmes	O
.	O
L'	O
homéopathie	O
s'	O
oppose	O
à	O
l'	O
allopathie	O
,	O
terme	O
également	O
inventé	O
par	O
Hahnemann	PERSON
et	O
qui	O
désigne	O
tout	O
traitement	O
médicamenteux	O
qui	O
ne	O
s'	O
appuie	O
pas	O
sur	O
la	O
similitude	O
lors	O
du	O
choix	O
thérapeutique	O
,	O
mais	O
sur	O
le	O
"	O
principe	O
des	O
contraires	O
"	O
.	O
L'	O
homéopathie	O
repose	O
sur	O
le	O
principe	O
de	O
similitude	O
formalisé	O
par	O
Hahnemann	O
après	O
la	O
seule	O
observation	O
de	O
l'	O
effet	O
de	O
l'	O
écorce	O
de	O
quinquina	O
:	O
le	O
paludisme	O
s'	O
accompagne	O
de	O
fièvre	O
et	O
l'	O
écorce	O
de	O
quinine	O
quinquina	O
à	O
forte	O
dose	O
provoque	O
une	O
intoxication	O
également	O
accompagnée	O
de	O
fièvre	O
,	O
Hahnemann	O
a	O
supposé	O
que	O
celle-ci	O
activait	O
un	O
mécanisme	O
de	O
défense	O
contre	O
la	O
fièvre	O
,	O
quelle	O
qu'	O
en	O
fût	O
la	O
cause	O
.	O
Une	O
pratique	O
ne	O
reposant	O
pas	O
sur	O
cette	O
analyse	O
des	O
symptômes	O
spécifiques	O
du	O
patient	O
n'	O
est	O
pas	O
en	O
droit	O
de	O
se	O
réclamer	O
de	O
l'	O
homéopathie	O
au	O
sens	O
de	O
Hahnemann	O
.	O
Les	O
expérimentations	O
d'	O
Hahnemann	O
lui	O
auraient	O
montré	O
que	O
le	O
fait	O
de	O
secouer	O
la	O
solution	O
après	O
chaque	O
dilution	O
permettrait	O
de	O
conserver	O
une	O
certaine	O
efficacité	O
thérapeutique	O
;	O
cependant	O
,	O
il	O
proscrit	O
l'	O
emploi	O
de	O
sucre	O
pour	O
administrer	O
ses	O
préparations	O
qu'	O
il	O
conseille	O
d'	O
administrer	O
liquide	O
juste	O
après	O
les	O
avoir	O
préparées	O
et	O
sans	O
les	O
laisser	O
reposer	O
.	O
Ce	O
procédé	O
,	O
sans	O
lequel	O
les	O
dilutions	O
seraient	O
peu	O
ou	O
pas	O
actives	O
,	O
a	O
été	O
nommé	O
"	O
dynamisation	O
"	O
par	O
Hahnemann	O
.	O
Hahnemann	O
considéra	O
cette	O
méthode	O
comme	O
aussi	O
efficace	O
que	O
la	O
sienne	O
.	O
Ceci	O
montrerait	O
que	O
la	O
dilution	O
korsakovienne	O
est	O
beaucoup	O
moins	O
poussée	O
que	O
ce	O
que	O
Korsakov	O
lui-même	O
pensait	O
.	O
De	O
ce	O
fait	O
,	O
les	O
laboratoires	O
financèrent	O
des	O
recherches	O
dans	O
ce	O
sens	O
qui	O
aboutirent	O
sur	O
la	O
théorie	O
de	O
la	O
mémoire	O
de	O
l'	O
eau	O
selon	O
laquelle	O
l'	O
eau	O
pouvait	O
garder	O
les	O
propriétés	O
de	O
substances	O
qu'	O
on	O
y	O
a	O
diluées	O
même	O
en	O
l'	O
absence	O
de	O
ces	O
substances	O
(	O
rémanence	O
de	O
l'	O
empreinte	O
électromagnétique	O
de	O
la	O
molécule	O
)	O
,	O
proposée	O
par	O
Jacques	PERSON
Benveniste	PERSON
en	O
1988	O
.	O
Ce	O
dernier	O
point	O
de	O
vue	O
peut	O
paraître	O
cependant	O
contestable	O
quand	O
on	O
considére	O
l'	O
attachement	O
de	O
Benveniste	PERSON
à	O
perfectionner	O
sans	O
cesse	O
ses	O
protocoles	O
expérimentaux	O
afin	O
d'	O
éviter	O
tout	O
risque	O
d'	O
artefact	O
.	O
L'	O
homéopathie	O
moderne	O
admet	O
la	O
nature	O
moléculaire	O
de	O
la	O
matière	O
qui	O
sous-tend	O
le	O
raisonnement	O
indiqué	O
au-dessus	O
(	O
les	O
théories	O
moléculaires	O
vérifiées	O
expérimentalement	O
sont	O
postérieures	O
à	O
la	O
formulation	O
de	O
l'	O
homéopathie	O
par	O
Hahnemann	O
)	O
.	O
On	O
constate	O
que	O
la	O
pratique	O
homéopathique	O
moderne	O
est	O
bien	O
éloignée	O
des	O
préconisations	O
d'	O
Hahnemann	O
.	O
Selon	O
les	O
tenants	O
de	O
l'	O
homéopathie	O
,	O
ce	O
principe	O
déjà	O
cité	O
par	O
Hippocrate	O
Les	O
travaux	O
de	O
Pasteur	O
et	O
le	O
développement	O
de	O
l'	O
immunologie	O
ont	O
clarifié	O
les	O
choses	O
depuis	O
.	O
L'	O
homéopathie	O
fut	O
formulée	O
à	O
une	O
époque	O
où	O
l'	O
on	O
ne	O
comprenait	O
pas	O
pourquoi	O
la	O
vaccination	O
avait	O
un	O
effet	O
,	O
et	O
dans	O
ce	O
cadre	O
,	O
les	O
propositions	O
d'	O
Hahnemann	O
avaient	O
un	O
sens	O
qu'	O
elles	O
ont	O
perdu	O
depuis	O
.	O
La	O
popularité	O
de	O
l'	O
homéopathie	O
est	O
surtout	O
remarquable	O
en	O
France	O
,	O
ce	O
pays	O
assurant	O
80	O
%	O
du	O
marché	O
des	O
médicaments	O
homéopathiques	O
(	O
et	O
en	O
est	O
également	O
le	O
principal	O
producteur	O
)	O
[	O
réf.	O
nécessaire	O
]	O
.	O
L'	O
emploi	O
de	O
cette	O
méthode	O
en	O
tant	O
que	O
médecine	O
est	O
attesté	O
dans	O
quatre-vingts	O
pays	O
,	O
principalement	O
en	O
Europe	O
,	O
en	O
Amérique	O
du	O
Sud	O
,	O
en	O
Inde	O
,	O
au	O
Bangladesh	O
et	O
au	O
Pakistan	O
.	O
En	O
Grande-Bretagne	O
,	O
cinq	O
hôpitaux	O
sont	O
utilisateurs	O
de	O
traitement	O
homéopathique	O
.	O
En	O
Inde	O
exercent	O
près	O
de	O
250	O
000	O
homéopathes	O
traitant	O
près	O
de	O
10	O
%	O
de	O
la	O
population	O
indienne	O
avec	O
ces	O
seuls	O
produits	O
.	O
L'	O
homéopathie	O
est	O
aujourd'hui	O
utilisée	O
dans	O
plus	O
de	O
quatre-vingts	O
pays	O
dans	O
le	O
monde	O
,	O
principalement	O
en	O
Europe	O
.	O
Elle	O
est	O
aussi	O
très	O
présente	O
en	O
Amérique	O
du	O
Sud	O
,	O
en	O
Inde	O
et	O
au	O
Pakistan	O
.	O
L'	O
Espagne	O
et	O
l'	O
Italie	O
considèrent	O
comme	O
la	O
France	O
que	O
l	O
'	O
exercice	O
de	O
l'	O
homéopathie	O
relève	O
de	O
la	O
médecine	O
(	O
bien	O
que	O
son	O
objet	O
soit	O
considéré	O
comme	O
non-scientifique	O
)	O
et	O
exigent	O
donc	O
que	O
les	O
homéopathes	O
possèdent	O
une	O
formation	O
de	O
médecin	O
classique	O
.	O
Au	O
Brésil	O
,	O
l'	O
homéopathie	O
est	O
une	O
spécialité	O
médicale	O
reconnue	O
au	O
même	O
titre	O
que	O
les	O
autres	O
depuis	O
1992	O
.	O
En	O
Allemagne	O
(	O
comme	O
en	O
France	O
)	O
,	O
certains	O
remèdes	O
homéopathiques	O
peuvent	O
être	O
prescrits	O
,	O
comme	O
d'	O
autres	O
médicaments	O
,	O
par	O
des	O
professionnels	O
de	O
santé	O
non-médecin	O
comme	O
les	O
dentistes	O
,	O
les	O
sages-femmes	O
ou	O
les	O
kinésithérapeutes	O
.	O
Dans	O
certains	O
pays	O
,	O
les	O
remèdes	O
homéopathiques	O
sont	O
remboursés	O
par	O
les	O
mécanismes	O
d'	O
assurance	O
maladie	O
,	O
au	O
même	O
titre	O
que	O
les	O
autres	O
médicaments	O
(	O
c'	O
est	O
le	O
cas	O
en	O
France	O
,	O
par	O
exemple	O
)	O
.	O
Dans	O
d'	O
autres	O
pays	O
,	O
comme	O
l'	O
Allemagne	O
(	O
depuis	O
2003	O
)	O
,	O
l'	O
Espagne	O
,	O
la	O
Finlande	O
,	O
l'	O
Irlande	O
,	O
l'	O
Italie	O
,	O
la	O
Norvège	O
et	O
la	O
Suède	O
,	O
l'	O
homéopathie	O
n'	O
est	O
pas	O
prise	O
en	O
charge	O
par	O
les	O
systèmes	O
de	O
santé	O
.	O
En	O
Europe	O
,	O
l'	O
homéopathie	O
est	O
un	O
produit	O
devant	O
obtenir	O
une	O
autorisation	O
de	O
mise	O
sur	O
le	O
marché	O
,	O
bien	O
qu'	O
il	O
puisse	O
être	O
dispensé	O
d'	O
étude	O
clinique	O
préalable	O
.	O
En	O
2005	O
,	O
la	O
revue	O
médicale	O
The	O
Lancet	O
publie	O
une	O
meta-analyse	O
des	O
études	O
médicales	O
sur	O
l'	O
homeopathie	O
:	O
Les	O
résultats	O
de	O
cette	O
étude	O
ont	O
été	O
publiés	O
dans	O
The	O
Lancet	O
.	O
Les	O
résultats	O
,	O
publiés	O
dans	O
The	O
Lancet	O
en	O
1988	O
,	O
n'	O
ont	O
montré	O
aucune	O
différence	O
avec	O
le	O
placebo	O
.	O
Pour	O
mieux	O
comprendre	O
le	O
débat	O
sur	O
l'	O
efficacité	O
de	O
la	O
thérapeutique	O
homéopathique	O
,	O
il	O
faut	O
en	O
fait	O
prendre	O
en	O
compte	O
deux	O
méta-analyses	O
publiées	O
dans	O
le	O
Lancet	O
.	O
Le	O
Lancet	O
publie	O
en	O
novembre	O
2007	O
les	O
résultats	O
de	O
5	O
méta-analyses	O
.	O
Qui	O
plus	O
est	O
,	O
en	O
Grande-Bretagne	O
,	O
en	O
Allemagne	O
,	O
en	O
Grèce	O
,	O
en	O
Finlande	O
,	O
aux	O
Pays-Bas	O
,	O
il	O
n'	O
est	O
pas	O
nécessaire	O
d'	O
être	O
médecin	O
pour	O
exercer	O
l'	O
homéopathie	O
;	O
en	O
Suède	O
,	O
cette	O
activité	O
est	O
formellement	O
interdite	O
aux	O
médecins	O
.	O
Côté	O
allopathie	O
,	O
selon	O
la	O
même	O
source	O
[	O
réf.	O
nécessaire	O
]	O
,	O
Jacques	PERSON
Servier	PERSON
est	O
11	O
e	O
et	O
Famille	PERSON
Mérieux	PERSON
(	O
bioMérieux	O
)	O
29	O
e	O
.	O
