Elle	O
fut	O
la	O
pièce-clef	O
dans	O
le	O
déchiffrement	O
de	O
l'	O
égyptien	O
hiéroglyphique	O
par	O
Jean-François	PERSON
Champollion	PERSON
en	O
1822	O
.	O
Elle	O
fut	O
découverte	O
dans	O
le	O
village	O
de	O
Rachïd	LOCATION
(	LOCATION
Rosette	LOCATION
)	O
le	O
15	O
juillet	O
1799	O
durant	O
la	O
campagne	O
d'	O
Égypte	O
de	O
Bonaparte	O
.	O
Depuis	O
1802	O
,	O
elle	O
est	O
exposée	O
au	O
British	O
Museum	O
.	O
Elle	O
a	O
toutefois	O
été	O
prêtée	O
au	O
Musée	O
du	O
Louvre	O
dans	O
les	O
années	O
1980	O
.	O
C'	O
est	O
un	O
jeune	O
officier	O
du	O
génie	O
,	O
Pierre-François-Xavier	PERSON
Bouchard	PERSON
,	O
qui	O
remarqua	O
cette	O
pierre	O
noire	O
de	O
plus	O
d'	O
un	O
mètre	O
de	O
haut	O
lors	O
de	O
travaux	O
de	O
terrassement	O
dans	O
une	O
ancienne	O
forteresse	O
turque	O
.	O
Lors	O
de	O
la	O
capitulation	O
de	O
1801	O
,	O
les	O
Britanniques	O
victorieux	O
exigèrent	O
la	O
livraison	O
des	O
monuments	O
antiques	O
,	O
dont	O
la	O
pierre	O
de	O
Rosette	O
.	O
Mais	O
dès	O
1800	O
,	O
une	O
reproduction	O
du	O
texte	O
avait	O
été	O
envoyée	O
en	O
France	O
pour	O
y	O
être	O
étudiée	O
.	O
Akerblad	O
et	O
Silvestre	O
de	O
Sacy	O
se	O
lancèrent	O
dans	O
la	O
première	O
tentative	O
de	O
déchiffrage	O
,	O
mais	O
elle	O
demeura	O
vaine	O
.	O
Ce	O
fut	O
ensuite	O
au	O
tour	O
d'	O
un	O
savant	O
britannique	O
Thomas	PERSON
Young	PERSON
de	O
se	O
lancer	O
dans	O
un	O
travail	O
qui	O
sembla	O
promis	O
au	O
succès	O
.	O
Jean-François	PERSON
Champollion	PERSON
,	O
qui	O
n'	O
avait	O
pas	O
encore	O
dix	O
ans	O
au	O
moment	O
de	O
la	O
découverte	O
de	O
la	O
pierre	O
,	O
se	O
lança	O
très	O
jeune	O
dans	O
la	O
bataille	O
du	O
déchiffrage	O
des	O
hiéroglyphes	O
.	O
C'	O
est	O
un	O
décret	O
de	O
Ptolémée	O
V	O
Épiphane	O
,	O
décrivant	O
des	O
impôts	O
qu'	O
il	O
abrogea	O
(	O
dont	O
l'	O
un	O
est	O
mesuré	O
en	O
ardebs	O
(	O
grec	O
artabai	O
)	O
par	O
aroure	O
)	O
et	O
instituant	O
l'	O
ordre	O
d'	O
ériger	O
des	O
statues	O
dans	O
des	O
temples	O
.	O
