Il	O
permet	O
par	O
exemple	O
d'	O
échanger	O
(	O
envoi/réception	O
)	O
des	O
fichiers	O
,	O
des	O
fax	O
,	O
de	O
se	O
connecter	O
à	O
Internet	O
,	O
d'	O
échanger	O
des	O
courriels	O
,	O
de	O
téléphoner	O
ou	O
de	O
recevoir	O
la	O
télévision	O
.	O
Toutes	O
ces	O
catégories	O
de	O
modem	O
servent	O
bien	O
souvent	O
à	O
accéder	O
à	O
Internet	O
(	O
ou	O
à	O
envoyer	O
ou	O
recevoir	O
des	O
télécopies	O
,	O
à	O
se	O
connecter	O
à	O
des	O
services	O
Minitel	O
…	O
)	O
,	O
ou	O
même	O
à	O
faire	O
de	O
la	O
téléphonie	O
numérique	O
.	O
Depuis	O
la	O
fin	O
des	O
années	O
1990	O
,	O
de	O
nombreuses	O
normes	O
de	O
télécommunications	O
sont	O
apparues	O
et	O
,	O
donc	O
autant	O
de	O
nouveaux	O
types	O
de	O
modems	O
:	O
RNIS	O
(	O
ou	O
ISDN	O
)	O
,	O
ADSL	O
,	O
GSM	O
,	O
GPRS	O
,	O
Wi-Fi	O
,	O
Wimax	O
…	O
Le	O
système	O
,	O
connu	O
sous	O
le	O
nom	O
de	O
"	O
Sabre	O
"	O
était	O
un	O
parent	O
éloigné	O
du	O
système	O
moderne	O
Sabre	O
.	O
La	O
France	O
fut	O
par	O
exemple	O
durant	O
près	O
d'	O
une	O
décennie	O
le	O
pays	O
disposant	O
du	O
nombre	O
de	O
modems	O
par	O
habitant	O
le	O
plus	O
important	O
,	O
cela	O
par	O
l'	O
intermédiaire	O
de	O
la	O
quasi-omniprésence	O
du	O
Minitel	O
.	O
Par	O
ailleurs	O
,	O
comme	O
,	O
avec	O
la	O
norme	O
V.90	O
,	O
on	O
arrive	O
près	O
de	O
la	O
vitesse	O
de	O
transfert	O
théorique	O
maximum	O
d'	O
une	O
ligne	O
téléphonique	O
standard	O
,	O
on	O
a	O
mis	O
au	O
point	O
des	O
techniques	O
permettant	O
d'	O
augmenter	O
le	O
débit	O
en	O
procédant	O
,	O
avant	O
l'	O
envoi	O
,	O
à	O
une	O
compression	O
des	O
données	O
.	O
