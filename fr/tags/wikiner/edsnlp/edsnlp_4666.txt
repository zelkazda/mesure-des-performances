L'	O
empereur	O
Jimmu	O
,	O
fondateur	O
mythique	O
du	O
Japon	O
,	O
est	O
considéré	O
comme	O
le	O
descendant	O
direct	O
de	O
la	O
déité	O
shinto	O
Amaterasu	O
.	O
Selon	O
la	O
croyance	O
shinto	O
,	O
Jimmu	PERSON
est	O
considéré	O
comme	O
un	O
arrière-arrière-arrière-petit-fils	O
de	O
la	O
déesse	O
du	O
soleil	O
Amaterasu	O
.	O
Elle	O
envoie	O
son	O
petit-fils	O
dans	O
l'	O
archipel	O
japonais	O
où	O
il	O
épouse	O
la	O
princesse	O
Konohana-Sakuya	PERSON
.	O
Ils	O
finissent	O
par	O
se	O
marier	O
et	O
ont	O
quatre	O
enfants	O
,	O
dont	O
le	O
dernier	O
devient	O
l'	O
empereur	O
Jimmu	PERSON
.	O
Jimmu	PERSON
,	O
considérant	O
qu'	O
ils	O
avaient	O
perdu	O
car	O
ils	O
avaient	O
combattu	O
vers	O
l'	O
est	O
,	O
contre	O
le	O
soleil	O
,	O
décide	O
par	O
conséquent	O
de	O
débarquer	O
à	O
l'	O
est	O
de	O
la	O
péninsule	O
de	O
Kii	O
et	O
de	O
combattre	O
tourné	O
vers	O
l'	O
ouest	O
.	O
Le	O
jour	O
de	O
l'	O
an	O
du	O
calendrier	O
lunisolaire	O
japonais	O
est	O
traditionnellement	O
célébré	O
comme	O
le	O
début	O
du	O
règne	O
de	O
l'	O
empereur	O
Jimmu	O
.	O
