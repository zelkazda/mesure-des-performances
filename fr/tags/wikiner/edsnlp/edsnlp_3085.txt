Les	O
deux	O
plus	O
grandes	O
îles	O
sont	O
Upolu	LOCATION
et	LOCATION
Savai'i	LOCATION
,	LOCATION
à	LOCATION
l'	LOCATION
ouest	LOCATION
.	LOCATION
La	LOCATION
capitale	LOCATION
Apia	LOCATION
et	LOCATION
l'	LOCATION
aéroport	LOCATION
international	LOCATION
de	LOCATION
Faleolo	LOCATION
sont	LOCATION
situés	LOCATION
sur	LOCATION
la	LOCATION
côte	LOCATION
nord	LOCATION
d'	LOCATION
Upolu	LOCATION
.	O
En	O
1899	O
,	O
les	O
Samoa	O
furent	O
divisées	O
en	O
deux	O
parties	O
:	O
les	O
Samoa	O
allemandes	O
et	O
les	O
Samoa	O
orientales	O
(	O
sous	O
contrôle	O
américain	O
)	O
.	O
En	O
1914	O
,	O
la	O
Nouvelle-Zélande	O
prit	O
le	O
contrôle	O
des	O
Samoa	O
occidentales	O
.	O
Elle	O
le	O
garda	O
ensuite	O
sous	O
mandat	O
de	O
la	O
Société	O
des	O
Nations	O
puis	O
sous	O
mandat	O
de	O
l'	O
Organisation	O
des	O
Nations	O
unies	O
jusqu'	O
en	O
1961	O
.	O
Les	O
Samoa	O
occidentales	O
obtinrent	O
leur	O
indépendance	O
le	O
1	O
er	O
janvier	O
1962	O
suite	O
à	O
un	O
référendum	O
.	O
Le	O
pays	O
devint	O
membre	O
du	O
Commonwealth	O
en	O
1970	O
et	O
des	O
Nations	O
unies	O
en	O
1976	O
.	O
Les	O
Samoa	O
forment	O
une	O
monarchie	O
constitutionnelle	O
indépendante	O
.	O
La	O
constitution	O
de	O
1960	O
,	O
qui	O
prit	O
formellement	O
effet	O
à	O
l'	O
indépendance	O
du	O
pays	O
,	O
est	O
basée	O
sur	O
la	O
démocratie	O
parlementaire	O
du	O
Royaume-Uni	O
,	O
modifiée	O
pour	O
prendre	O
en	O
compte	O
les	O
coutumes	O
des	O
Samoa	O
.	O
Les	O
îles	O
situées	O
à	O
l'	O
est	O
du	O
171	O
e	O
degré	O
de	O
longitude	O
ouest	O
forment	O
les	O
Samoa	O
américaines	O
et	O
celles	O
à	O
l'	O
ouest	O
les	O
Samoa	O
occidentales	O
.	O
Les	O
Samoa	O
comprennent	O
les	O
deux	O
grandes	O
îles	O
d'	O
Upolu	O
et	O
de	O
Savai'i	O
ainsi	O
que	O
huit	O
îlots	O
.	O
Presque	O
les	O
trois	O
quarts	O
de	O
la	O
population	O
habitent	O
sur	O
l'	O
île	O
principale	O
d'	O
Upolu	O
,	O
où	O
se	O
trouve	O
également	O
la	O
capitale	O
,	O
Apia	LOCATION
.	O
La	O
faune	O
des	O
Samoa	O
est	O
assez	O
restreinte	O
,	O
on	O
n'	O
y	O
trouve	O
que	O
peu	O
d'	O
animaux	O
:	O
serpents	O
,	O
lézards	O
et	O
oiseaux	O
.	O
La	O
flore	O
des	O
Samoa	O
est	O
en	O
revanche	O
très	O
riche	O
,	O
la	O
forêt	O
recouvrant	O
les	O
îles	O
est	O
luxuriante	O
.	O
Plusieurs	O
plantes	O
et	O
animaux	O
des	O
Samoa	O
sont	O
endémiques	O
(	O
on	O
ne	O
les	O
retrouve	O
nulle	O
part	O
ailleurs	O
sur	O
la	O
planète	O
)	O
.	O
Les	O
cyclones	O
sont	O
fréquents	O
et	O
dévastateurs	O
pour	O
l'	O
agriculture	O
des	O
Samoa	O
.	O
C'	O
est	O
la	O
raison	O
principale	O
pour	O
laquelle	O
les	O
Samoa	O
,	O
dont	O
l'	O
économie	O
dépend	O
majoritairement	O
de	O
l'	O
agriculture	O
,	O
ont	O
encore	O
besoin	O
de	O
l'	O
aide	O
internationale	O
.	O
Les	O
principales	O
exportations	O
des	O
Samoa	O
sont	O
le	O
coprah	O
,	O
le	O
cacao	O
,	O
la	O
banane	O
et	O
le	O
café	O
.	O
L'	O
agriculture	O
emploie	O
les	O
deux	O
tiers	O
de	O
la	O
population	O
des	O
Samoa	O
et	O
fournit	O
90	O
%	O
des	O
revenus	O
d'	O
exportation	O
.	O
Les	O
principaux	O
pays	O
où	O
les	O
produits	O
sont	O
exportés	O
sont	O
l'	O
Australie	O
,	O
les	O
États-Unis	O
et	O
le	O
Japon	O
.	O
Seul	O
pays	O
du	O
Pacifique	O
à	O
avoir	O
été	O
classé	O
par	O
l'	O
ONU	O
dans	O
la	O
catégorie	O
des	O
pays	O
les	O
moins	O
avancés	O
,	O
cette	O
situation	O
lui	O
permet	O
de	O
recevoir	O
de	O
l'	O
aide	O
internationale	O
.	O
Apia	O
,	O
avec	O
une	O
population	O
de	O
33	O
000	O
habitants	O
(	O
en	O
1995	O
)	O
,	O
est	O
la	O
capitale	O
et	O
le	O
centre	O
commercial	O
des	O
Samoa	O
.	O
50	O
%	O
de	O
la	O
population	O
des	O
Samoa	O
habitent	O
ou	O
travaillent	O
à	O
l'	O
étranger	O
.	O
La	O
plupart	O
partent	O
pour	O
la	O
Nouvelle-Zélande	O
,	O
l'	O
Australie	O
ou	O
vers	O
les	O
Samoa	O
américaines	O
,	O
première	O
étape	O
avant	O
une	O
seconde	O
migration	O
vers	O
les	O
États-Unis	O
.	O
Le	O
fa'asamoa	O
se	O
manifeste	O
également	O
dans	O
le	O
système	O
judiciaire	O
des	O
Samoa	O
qui	O
est	O
séparé	O
en	O
deux	O
.	O
En	O
dépit	O
des	O
siècles	O
d'	O
influence	O
européenne	O
,	O
Samoa	O
maintient	O
ses	O
coutumes	O
historiques	O
,	O
ses	O
systèmes	O
sociaux	O
et	O
sa	O
langue	O
dont	O
on	O
pensait	O
qu'	O
elle	O
est	O
la	O
plus	O
ancienne	O
forme	O
de	O
discours	O
polynésien	O
encore	O
en	O
existence	O
(	O
hypothèse	O
contestée	O
par	O
les	O
linguistes	O
modernes	O
)	O
.	O
Autre	O
sport	O
important	O
aux	O
Samoa	O
,	O
le	O
kirikiti	O
est	O
une	O
sorte	O
de	O
dérivé	O
du	O
cricket	O
qui	O
se	O
joue	O
avec	O
une	O
balle	O
en	O
caoutchouc	O
.	O
Samoa	O
a	O
pour	O
codes	O
:	O
