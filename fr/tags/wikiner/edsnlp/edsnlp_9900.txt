Ces	O
tables	O
couvraient	O
des	O
périodes	O
de	O
95	O
ans	O
(	O
ou	O
cinq	O
cycles	O
de	O
19	O
ans	O
du	O
grec	O
Méton	O
)	O
et	O
dataient	O
les	O
années	O
selon	O
le	O
calendrier	O
dit	O
de	O
l'	O
ère	O
de	O
Dioclétien	O
dont	O
la	O
première	O
année	O
est	O
notre	O
année	O
285	O
.	O
Jusque-là	O
,	O
la	O
date	O
de	O
naissance	O
de	O
Jésus	O
reposait	O
sur	O
l'	O
indication	O
de	O
l'	O
évangéliste	O
Luc	O
:	O
Jésus	PERSON
avait	O
30	O
ans	O
en	O
l'	O
an	O
15	O
de	O
Tibère	O
(	O
an	O
29/30	O
du	O
calendrier	O
actuel	O
)	O
.	O
En	O
525	O
,	O
Denys	PERSON
ajouta	O
un	O
cycle	O
de	O
95	O
ans	O
à	O
partir	O
de	O
l'	O
année	O
247	O
de	O
l'	O
ère	O
de	O
Dioclétien	O
là	O
où	O
s'	O
arrétaient	O
les	O
tables	O
alexandrines	O
qu'	O
il	O
avait	O
en	O
sa	O
possession	O
,	O
(	O
c'	O
est-à-dire	O
à	O
partir	O
de	O
285	O
+	O
247	O
=	O
532	O
de	O
notre	O
calendrier	O
actuel	O
)	O
.	O
Mais	O
il	O
décida	O
en	O
même	O
temps	O
de	O
modifier	O
l'	O
année	O
du	O
début	O
du	O
calendrier	O
pour	O
ne	O
plus	O
se	O
référer	O
au	O
calendrier	O
de	O
Dioclétien	O
,	O
empereur	O
qui	O
avait	O
sévérement	O
persécuté	O
les	O
Chrétiens	O
.	O
On	O
ne	O
sait	O
de	O
manière	O
certaine	O
s'	O
il	O
avait	O
noté	O
que	O
l'	O
année	O
532	O
,	O
à	O
partir	O
de	O
laquelle	O
il	O
avait	O
ajouté	O
un	O
cycle	O
de	O
95	O
ans	O
,	O
correspondait	O
au	O
produit	O
de	O
19	O
(	O
cycle	O
de	O
Méton	O
)	O
,	O
par	O
4	O
(	O
pour	O
les	O
années	O
bissextiles	O
)	O
,	O
par	O
7	O
(	O
pour	O
les	O
jours	O
de	O
la	O
semaine	O
)	O
,	O
c'	O
est-à-dire	O
au	O
cycle	O
de	O
532	O
ans	O
du	O
calendrier	O
alexandrin	O
,	O
cycle	O
pour	O
lequel	O
Pâques	O
tombe	O
le	O
même	O
jour	O
du	O
même	O
mois	O
.	O
Aussi	O
Denys	PERSON
le	PERSON
Petit	PERSON
n'	O
hésita	O
pas	O
à	O
sacrifier	O
la	O
rigueur	O
des	O
repères	O
donnés	O
par	O
l'	O
évangéliste	O
Luc	O
,	O
à	O
une	O
coïncidence	O
astrale	O
.	O
Il	O
y	O
avait	O
plus	O
de	O
quatre	O
ans	O
de	O
retard	O
sur	O
les	O
repères	O
de	O
l'	O
évangile	O
de	O
Matthieu	O
,	O
selon	O
qui	O
Jésus	O
serait	O
né	O
au	O
moins	O
deux	O
ans	O
avant	O
la	O
mort	O
Hérode	O
le	O
Grand	O
en	O
-4	O
,	O
si	O
on	O
considère	O
le	O
récit	O
du	O
"	O
massacre	O
des	O
Innocents	O
"	O
.	O
Si	O
le	O
calendrier	O
commence	O
avec	O
la	O
naissance	O
du	O
Christ	O
,	O
pourquoi	O
l'	O
année	O
ne	O
commence-t-elle	O
pas	O
elle	O
aussi	O
le	O
jour	O
de	O
sa	O
naissance	O
?	O
Dans	O
le	O
catholicisme	O
,	O
ce	O
n'	O
est	O
ni	O
Noël	O
,	O
ni	O
le	O
premier	O
jour	O
de	O
janvier	O
qui	O
marque	O
le	O
début	O
de	O
l'	O
année	O
,	O
d'	O
un	O
point	O
de	O
vue	O
liturgique	O
(	O
or	O
la	O
liturgie	O
est	O
le	O
comput	O
temporel	O
catholique	O
)	O
.	O
