Ses	O
travaux	O
lui	O
valurent	O
en	O
1844	O
la	O
Royal	O
Medal	O
de	O
la	O
Royal	O
Society	O
,	O
puis	O
une	O
chaire	O
de	O
mathématiques	O
à	O
l'	O
université	O
de	O
Cork	O
en	O
1849	O
.	O
Issu	O
d'	O
une	O
famille	O
pauvre	O
,	O
George	PERSON
Boole	PERSON
n'	O
a	O
pas	O
les	O
moyens	O
financiers	O
d'	O
aller	O
à	O
l'	O
université	O
.	O
Boole	O
y	O
développe	O
une	O
nouvelle	O
forme	O
de	O
logique	O
,	O
à	O
la	O
fois	O
symbolique	O
et	O
mathématique	O
.	O
Les	O
travaux	O
de	O
Boole	PERSON
,	O
s'	O
ils	O
sont	O
théoriques	O
,	O
n'	O
en	O
trouveront	O
pas	O
moins	O
des	O
applications	O
primordiales	O
dans	O
des	O
domaines	O
aussi	O
divers	O
que	O
les	O
systèmes	O
informatiques	O
,	O
la	O
théorie	O
des	O
probabilités	O
,	O
les	O
circuits	O
électriques	O
et	O
téléphoniques	O
,	O
etc	O
.	O
Et	O
en	O
1857	O
,	O
il	O
est	O
nommé	O
membre	O
de	O
la	O
Royal	O
Society	O
.	O
George	PERSON
Boole	PERSON
meurt	O
d'	O
une	O
pneumonie	O
le	O
8	O
décembre	O
1864	O
.	O
