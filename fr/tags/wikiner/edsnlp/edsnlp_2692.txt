Le	O
Yémen	O
,	O
officiellement	O
la	O
République	O
du	O
Yémen	O
,	O
est	O
un	O
pays	O
arabe	O
situé	O
à	O
la	O
pointe	O
sud-ouest	O
de	O
la	O
péninsule	O
d'	O
Arabie	O
.	O
Sa	O
capitale	O
est	O
Sanaa	LOCATION
.	O
Elle	O
possède	O
des	O
façades	O
maritimes	O
sur	O
le	O
golfe	O
d'	O
Aden	O
et	O
sur	O
la	O
mer	O
Rouge	O
.	O
Le	O
Yémen	O
couvre	O
une	O
superficie	O
totale	O
de	O
527970	O
km²	O
.	O
Dans	O
le	O
monde	O
antique	O
,	O
le	O
Yémen	O
était	O
connu	O
sous	O
le	O
nom	O
d	O
'	O
"	O
Arabie	O
heureuse	O
"	O
.	O
Une	O
autre	O
interprétation	O
se	O
base	O
sur	O
le	O
mot	O
arabe	O
"	O
al-yiumna	O
"	O
,	O
la	O
prospérité	O
:	O
le	O
Yémen	O
est	O
en	O
effet	O
une	O
des	O
zones	O
les	O
plus	O
irriguées	O
de	O
la	O
péninsule	O
arabique	O
,	O
d'	O
où	O
lui	O
vient	O
aussi	O
le	O
surnom	O
d	O
'	O
"	O
Arabie	O
heureuse	O
"	O
.	O
La	O
partie	O
septentrionale	O
du	O
Yémen	O
fut	O
soumise	O
nominalement	O
à	O
l'	O
Empire	O
ottoman	O
jusqu'	O
en	O
1918	O
.	O
La	O
partie	O
méridionale	O
correspond	O
à	O
l'	O
ancien	O
hinterland	O
britannique	O
,	O
formé	O
progressivement	O
à	O
partir	O
de	O
1839	O
autour	O
du	O
port	O
d'	O
Aden	LOCATION
.	O
Trois	O
ans	O
plus	O
tard	O
,	O
celle-ci	O
adopta	O
le	O
nom	O
de	O
République	O
démocratique	O
populaire	O
du	O
Yémen	O
.	O
La	O
même	O
année	O
,	O
le	O
Yémen	O
soutient	O
l'	O
Irak	O
durant	O
la	O
première	O
Guerre	O
du	O
Golfe	O
(	O
1990-1991	O
)	O
,	O
sans	O
pour	O
autant	O
valider	O
son	O
annexion	O
du	O
Koweït	O
,	O
ce	O
qui	O
lui	O
vaut	O
des	O
représailles	O
de	O
la	O
part	O
des	O
États-Unis	O
:	O
la	O
monnaie	O
est	O
attaquée	O
et	O
fortement	O
dévaluée	O
,	O
l'	O
Arabie	O
saoudite	O
suit	O
l'	O
allié	O
américain	O
et	O
expulse	O
du	O
royaume	O
le	O
million	O
de	O
travailleurs	O
yéménites	O
privant	O
des	O
millions	O
de	O
familles	O
de	O
ressources	O
.	O
Du	O
21	O
mai	O
au	O
7	O
juillet	O
1994	O
,	O
le	O
Yémen	O
du	O
Sud	O
a	O
vainement	O
tenté	O
de	O
faire	O
sécession	O
sous	O
le	O
nom	O
de	O
"	O
République	O
démocratique	O
du	O
Yémen	O
"	O
,	O
avant	O
de	O
retomber	O
sous	O
le	O
contrôle	O
du	O
gouvernement	O
de	O
Sanaa	LOCATION
.	O
En	O
2004	O
,	O
les	O
affrontements	O
près	O
de	O
la	O
frontière	O
avec	O
l'	O
Arabie	O
saoudite	O
ont	O
fait	O
environ	O
400	O
morts	O
.	O
Depuis	O
2004	O
,	O
le	O
Yémen	O
est	O
aussi	O
confronté	O
à	O
une	O
rébellion	O
armée	O
de	O
la	O
minorité	O
chiite	O
zaïdite	O
dans	O
la	O
province	O
de	O
Sa'dah	LOCATION
(	LOCATION
nord-ouest	LOCATION
)	O
,	O
qui	O
ne	O
reconnait	O
pas	O
le	O
régime	O
du	O
président	O
Ali	O
Abdullah	O
Saleh	O
,	O
au	O
pouvoir	O
depuis	O
1990	O
.	O
Depuis	O
le	O
4	O
novembre	O
2009	O
,	O
l'	O
Arabie	O
saoudite	O
intervient	O
militairement	O
contre	O
la	O
rébellion	O
.	O
Le	O
Yémen	O
est	O
une	O
république	O
.	O
Ali	PERSON
Abdullah	PERSON
Saleh	PERSON
est	O
le	O
président	O
depuis	O
le	O
22	O
mai	O
1990	O
.	O
Le	O
droit	O
du	O
Yémen	O
est	O
basé	O
sur	O
le	O
droit	O
islamique	O
,	O
le	O
droit	O
britannique	O
et	O
les	O
coutumes	O
locales	O
.	O
La	O
Cour	O
suprême	O
est	O
la	O
plus	O
haute	O
instance	O
du	O
pouvoir	O
judiciaire	O
,	O
mais	O
en	O
pratique	O
,	O
en	O
dehors	O
des	O
grandes	O
villes	O
,	O
la	O
stricte	O
application	O
de	O
la	O
charia	O
est	O
en	O
place	O
.	O
Le	O
Yemen	O
est	O
divisé	O
en	O
19	O
régions	O
ou	O
gouvernorats	O
.	O
Sanaa	O
,	O
la	O
capitale	O
,	O
ayant	O
un	O
statut	O
spécial	O
.	O
Le	O
Yémen	O
possède	O
1906	O
km	O
de	O
côtes	O
.	O
Le	O
Yémen	O
est	O
souvent	O
touché	O
par	O
les	O
tempêtes	O
de	O
sable	O
.	O
Le	O
Yémen	O
est	O
l'	O
un	O
des	O
pays	O
les	O
plus	O
pauvres	O
de	O
la	O
planète	O
et	O
cumule	O
une	O
situation	O
économique	O
fragile	O
avec	O
une	O
démographie	O
en	O
forte	O
expansion	O
.	O
Il	O
a	O
existé	O
longtemps	O
une	O
forte	O
communauté	O
juive	O
au	O
Yémen	O
,	O
mais	O
elle	O
a	O
émigré	O
en	O
masse	O
peu	O
après	O
la	O
création	O
d'	O
Israël	O
,	O
en	O
1948	O
,	O
durant	O
l'	O
Opération	O
Tapis	O
Volant	O
.	O
Le	O
Yemen	O
a	O
pour	O
codes	O
:	O
