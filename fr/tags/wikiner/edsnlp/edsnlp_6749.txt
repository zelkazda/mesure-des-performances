On	O
peut	O
distinguer	O
deux	O
conceptions	O
différentes	O
d'	O
Aphrodite	PERSON
:	O
celle	O
du	O
plaisir	O
de	O
la	O
chair	O
,	O
plus	O
"	O
terrienne	O
"	O
en	O
quelque	O
sorte	O
,	O
et	O
celle	O
de	O
l'	O
amour	O
spirituel	O
,	O
pure	O
et	O
chaste	O
dans	O
sa	O
beauté	O
.	O
Mariée	O
à	O
Héphaïstos	PERSON
,	O
elle	O
a	O
de	O
multiples	O
aventures	O
extra-conjugales	O
.	O
La	O
principale	O
est	O
celle	O
avec	O
Arès	O
,	O
d'	O
où	O
naissent	O
Harmonie	O
,	O
Déimos	O
,	O
Phobos	O
et	O
Éros	O
.	O
Par	O
suite	O
,	O
Aphrodite	PERSON
maudit	O
Hélios	O
et	O
sa	O
descendance	O
,	O
c'	O
est-à-dire	O
Pasiphaé	PERSON
et	O
ses	O
filles	O
Ariane	PERSON
et	O
Phèdre	PERSON
(	O
malédiction	O
qui	O
sera	O
aggravée	O
par	O
celle	O
dont	O
Poséidon	O
affligera	O
Minos	PERSON
,	O
époux	O
de	O
Pasiphaé	PERSON
et	O
père	O
d'	O
Ariane	PERSON
et	O
Phèdre	PERSON
)	O
.	O
Aphrodite	PERSON
a	O
également	O
une	O
liaison	O
avec	O
:	O
Aphrodite	PERSON
passe	O
en	O
outre	O
pour	O
avoir	O
distingué	O
de	O
nombreux	O
héros	O
mortels	O
,	O
parmi	O
lesquels	O
:	O
La	O
vengeance	O
d'	O
Aphrodite	PERSON
est	O
terrible	O
.	O
Pour	O
la	O
vindicte	O
,	O
elle	O
ne	O
le	O
cède	O
en	O
rien	O
à	O
Héra	PERSON
,	O
mais	O
si	O
cette	O
dernière	O
ne	O
poursuit	O
les	O
femmes	O
que	O
par	O
jalousie	O
,	O
Aphrodite	PERSON
ne	O
les	O
frappe	O
que	O
lorsqu'	O
elles	O
la	O
servent	O
mal	O
ou	O
refusent	O
de	O
la	O
servir	O
,	O
et	O
les	O
femmes	O
sont	O
alors	O
tant	O
ses	O
victimes	O
que	O
ses	O
instruments	O
destinés	O
aux	O
hommes	O
,	O
plus	O
rarement	O
par	O
jalousie	O
,	O
leur	O
inspirant	O
parfois	O
des	O
amours	O
difficiles	O
:	O
La	O
légende	O
la	O
plus	O
connue	O
concernant	O
Aphrodite	PERSON
est	O
peut-être	O
celle	O
qui	O
raconte	O
la	O
cause	O
de	O
la	O
guerre	O
de	O
Troie	O
.	O
Éris	PERSON
,	O
la	O
seule	O
déesse	O
à	O
ne	O
pas	O
être	O
invitée	O
au	O
mariage	O
du	O
roi	O
Pélée	O
et	O
de	O
la	O
nymphe	O
de	O
la	O
mer	O
Thétis	PERSON
,	O
jette	O
par	O
dépit	O
une	O
pomme	O
d'	O
or	O
dans	O
la	O
salle	O
du	O
banquet	O
avec	O
l'	O
inscription	O
"	O
À	O
la	O
plus	O
belle	O
"	O
.	O
Elles	O
demandent	O
à	O
Pâris	PERSON
,	O
prince	O
de	O
Troie	O
,	O
d'	O
être	O
le	O
juge	O
.	O
Héra	PERSON
lui	O
promet	O
la	O
puissance	O
royale	O
,	O
Athéna	PERSON
,	O
la	O
gloire	O
militaire	O
,	O
et	O
Aphrodite	PERSON
,	O
la	O
plus	O
belle	O
femme	O
du	O
monde	O
.	O
Pâris	PERSON
choisit	O
Aphrodite	PERSON
et	O
demande	O
en	O
récompense	O
Hélène	PERSON
de	O
Troie	O
,	O
femme	O
du	O
roi	O
grec	O
Ménélas	O
.	O
L'	O
enlèvement	O
d'	O
Hélène	PERSON
par	O
Pâris	O
provoque	O
la	O
guerre	O
de	O
Troie	O
.	O
Au	O
cours	O
de	O
cette	O
guerre	O
,	O
la	O
déesse	O
sera	O
légèrement	O
blessée	O
par	O
le	O
héros	O
grec	O
Diomède	O
en	O
portant	O
secours	O
à	O
son	O
fils	O
Énée	PERSON
.	O
De	O
fait	O
,	O
elle	O
correspond	O
très	O
probablement	O
à	O
la	O
déesse	O
Ishtar	O
--	O
Astarté	O
,	O
avec	O
laquelle	O
elle	O
partage	O
de	O
nombreux	O
traits	O
:	O
ce	O
sont	O
des	O
divinités	O
androgynes	O
;	O
Astarté	O
est	O
la	O
"	O
reine	O
du	O
ciel	O
"	O
alors	O
qu'	O
Aphrodite	O
est	O
dite	O
"	O
la	O
céleste	O
"	O
;	O
leur	O
culte	O
comprend	O
l'	O
offrande	O
d'	O
encens	O
et	O
le	O
sacrifice	O
de	O
colombes	O
.	O
Par	O
ailleurs	O
,	O
le	O
nom	O
d'	O
Aphrodite	PERSON
n'	O
a	O
pas	O
été	O
retrouvé	O
sur	O
les	O
tablettes	O
de	O
linéaire	O
B	O
,	O
témoignages	O
écrits	O
de	O
la	O
civilisation	O
mycénienne	O
.	O
C'	O
est	O
peut-être	O
à	O
cette	O
fête	O
qu'	O
il	O
faut	O
rattacher	O
un	O
rite	O
rapporté	O
par	O
l'	O
apologiste	O
chrétien	O
Clément	O
d'	O
Alexandrie	O
,	O
selon	O
lequel	O
les	O
participants	O
reçoivent	O
un	O
gâteau	O
en	O
forme	O
de	O
phallus	O
et	O
apportent	O
une	O
pièce	O
de	O
monnaie	O
,	O
"	O
comme	O
à	O
une	O
courtisane	O
ses	O
amants	O
"	O
.	O
Il	O
est	O
probable	O
que	O
Clément	O
parle	O
en	O
fait	O
de	O
l'	O
argent	O
destiné	O
aux	O
sacrifices	O
,	O
ou	O
la	O
taxe	O
pour	O
les	O
oracles	O
,	O
mais	O
il	O
est	O
également	O
possible	O
que	O
la	O
prostitution	O
sacrée	O
ait	O
également	O
été	O
pratiquée	O
.	O
Aphrodite	O
est	O
particulièrement	O
vénérée	O
en	O
Asie	O
mineure	O
,	O
notamment	O
en	O
Troade	O
.	O
Nouvelle-Ilion	O
frappe	O
des	O
monnaies	O
à	O
son	O
effigie	O
et	O
la	O
ville	O
d'	O
Aphrodisias	O
porte	O
son	O
nom	O
.	O
Selon	O
Strabon	O
,	O
qui	O
écrit	O
aux	O
débuts	O
de	O
l'	O
ère	O
chrétienne	O
,	O
on	O
y	O
pratique	O
la	O
prostitution	O
sacrée	O
:	O
"	O
le	O
temple	O
d'	O
Aphrodite	O
à	O
Corinthe	LOCATION
était	O
si	O
riche	O
,	O
qu'	O
il	O
possédait	O
à	O
titre	O
de	O
hiérodules	O
ou	O
d'	O
esclaves	O
sacrés	O
plus	O
de	O
mille	O
courtisanes	O
,	O
vouées	O
au	O
culte	O
de	O
la	O
déesse	O
par	O
des	O
donateurs	O
de	O
l'	O
un	O
et	O
de	O
l'	O
autre	O
sexe	O
.	O
"	O
