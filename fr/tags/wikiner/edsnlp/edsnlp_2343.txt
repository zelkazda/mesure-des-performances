Nekhbet	PERSON
était	O
alors	O
représentée	O
sous	O
la	O
forme	O
d'	O
une	O
tête	O
de	O
vautour	O
.	O
Lors	O
qu'	O
elle	O
était	O
représentée	O
sur	O
les	O
parois	O
des	O
temples	O
ou	O
des	O
tombeaux	O
,	O
elle	O
apparaissait	O
sous	O
les	O
traits	O
d'	O
une	O
femme	O
portant	O
la	O
couronne	O
blanche	O
de	O
Haute-Égypte	O
,	O
ou	O
sous	O
la	O
forme	O
d'	O
un	O
vautour	O
étendant	O
ses	O
ailes	O
protectrices	O
.	O
Autre	O
orthographe	O
:	O
Nékhbet	O
