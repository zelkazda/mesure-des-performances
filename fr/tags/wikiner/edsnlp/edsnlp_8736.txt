Édimbourg	O
est	O
une	O
ville	O
de	O
la	O
côte	O
est	O
de	O
l'	O
Écosse	O
au	O
Royaume-Uni	O
,	O
et	O
sa	O
capitale	O
depuis	O
1437	O
.	O
Elle	O
est	O
le	O
siège	O
du	O
Parlement	O
écossais	O
,	O
qui	O
a	O
été	O
rétabli	O
en	O
1999	O
.	O
Depuis	O
1329	O
,	O
Édimbourg	O
possède	O
officiellement	O
le	O
statut	O
de	O
cité	O
.	O
Le	O
palais	O
de	O
Holyrood	O
est	O
la	O
résidence	O
officielle	O
de	O
la	O
reine	O
lorsqu'	O
elle	O
séjourne	O
dans	O
la	O
ville	O
.	O
Les	O
districts	O
de	O
la	O
vieille	O
et	O
de	O
la	O
nouvelle	O
ville	O
sont	O
classés	O
patrimoine	O
mondial	O
par	O
l'	O
UNESCO	O
depuis	O
1995	O
.	O
Édimbourg	O
est	O
célèbre	O
pour	O
son	O
festival	O
,	O
le	O
plus	O
grand	O
du	O
monde	O
,	O
qui	O
dure	O
trois	O
semaines	O
en	O
août	O
et	O
propose	O
de	O
nombreux	O
spectacles	O
de	O
qualité	O
dans	O
toutes	O
les	O
disciplines	O
.	O
Le	O
roi	O
David	O
Ier	O
accorde	O
alors	O
des	O
terres	O
à	O
l'	O
église	O
de	O
Holyrood	O
d'	O
Édimbourg	LOCATION
.	O
Des	O
figures	O
phares	O
telles	O
que	O
David	PERSON
Hume	PERSON
et	O
Adam	PERSON
Smith	PERSON
faisaient	O
rayonner	O
la	O
ville	O
en	O
ce	O
temps-là	O
.	O
Ayant	O
perdu	O
de	O
son	O
importance	O
politique	O
,	O
certains	O
espéraient	O
qu'	O
Édimbourg	O
pourrait	O
un	O
jour	O
rivaliser	O
avec	O
des	O
centres	O
culturels	O
tels	O
que	O
Londres	LOCATION
,	O
comme	O
Athènes	O
ou	O
Rome	O
auparavant	O
.	O
Un	O
autre	O
facteur	O
de	O
ressemblance	O
est	O
la	O
présence	O
d'	O
architecture	O
néoclassique	O
,	O
particulièrement	O
celle	O
des	O
bâtiments	O
de	O
William	PERSON
Henry	PERSON
Playfair	PERSON
.	O
La	O
topographie	O
de	O
la	O
ville	O
offre	O
plusieurs	O
collines	O
permettant	O
d'	O
avoir	O
de	O
larges	O
panoramas	O
sur	O
Édimbourg	LOCATION
et	O
ses	O
environs	O
.	O
Au	O
sud-est	O
du	O
centre-ville	O
,	O
domine	O
Arthur	O
's	O
seat	O
(	O
le	O
siège	O
d'	O
Arthur	O
ou	O
parfois	O
le	O
siège	O
de	O
l'	O
archer	O
)	O
.	O
Il	O
surplombe	O
le	O
palais	O
de	O
Holyrood	O
ainsi	O
que	O
la	O
vieille	O
ville	O
toute	O
proche	O
.	O
Ce	O
crag	O
,	O
est	O
un	O
ensemble	O
de	O
cheminées	O
volcaniques	O
du	O
principal	O
volcan	O
sur	O
lequel	O
Édimbourg	LOCATION
est	O
construite	O
.	O
Il	O
fait	O
partie	O
des	O
sites	O
d'	O
intérêt	O
scientifique	O
en	O
matière	O
de	O
géologie	O
du	O
Royaume-Uni	O
.	O
L'	O
axe	O
majeur	O
de	O
la	O
vieille	O
ville	O
,	O
le	O
Royal	O
Mile	O
,	O
suit	O
l'	O
arrête	O
du	O
crag	O
en	O
descendant	O
lentement	O
vers	O
le	O
palais	LOCATION
de	LOCATION
Holyrood	LOCATION
à	O
l'	O
est	O
.	O
La	O
vieille	O
ville	O
a	O
préservé	O
sa	O
physionomie	O
médiévale	O
ainsi	O
que	O
de	O
nombreux	O
bâtiments	O
datant	O
de	O
la	O
Réforme	O
.	O
Elle	O
est	O
délimitée	O
sur	O
un	O
côté	O
par	O
le	O
château	O
,	O
d'	O
où	O
l'	O
artère	O
principale	O
(	O
le	O
Royal	O
Mile	O
)	O
descend	O
.	O
Des	O
ruelles	O
(	O
appelées	O
closes	O
ou	O
wynds	O
)	O
,	O
et	O
qui	O
partent	O
de	O
Royal	O
Mile	O
descendent	O
de	O
part	O
et	O
d'	O
autre	O
de	O
la	O
colline	O
.	O
Les	O
restrictions	O
imposées	O
par	O
le	O
manque	O
d'	O
espace	O
dû	O
à	O
l'	O
étroitesse	O
de	O
l'	O
arête	O
sur	O
laquelle	O
est	O
construite	O
la	O
vieille	O
ville	O
firent	O
d'	O
Édimbourg	LOCATION
l'	O
une	O
des	O
premières	O
villes	O
à	O
construire	O
de	O
véritables	O
tours	O
d'	O
habitation	O
.	O
En	O
1766	O
,	O
un	O
concours	O
,	O
lancé	O
afin	O
de	O
tracer	O
la	O
nouvelle	O
ville	O
,	O
fut	O
remporté	O
par	O
James	PERSON
Craig	PERSON
,	O
un	O
architecte	O
de	O
22	O
ans	O
.	O
Princes	O
Street	O
est	O
devenue	O
depuis	O
la	O
principale	O
rue	O
commerçante	O
d'	O
Édimbourg	LOCATION
et	O
quelques	O
bâtiments	O
d'	O
architecture	O
géorgienne	O
y	O
subsistent	O
.	O
Cette	O
dernière	O
a	O
été	O
dessiné	O
par	O
Robert	PERSON
Adam	PERSON
et	O
est	O
considérée	O
comme	O
un	O
des	O
plus	O
beaux	O
exemples	O
d'	O
architecture	O
géorgienne	O
de	O
Grande-Bretagne	O
.	O
Certains	O
plans	O
montrent	O
qu'	O
un	O
canal	O
aurait	O
été	O
envisagé	O
,	O
mais	O
on	O
créa	O
à	O
la	O
place	O
les	O
jardins	O
de	O
Princes	O
Street	O
.	O
Leith	O
est	O
le	O
port	O
d'	O
Édimbourg	LOCATION
.	O
Leith	O
est	O
toujours	O
considéré	O
comme	O
une	O
entité	O
séparée	O
d'	O
Édimbourg	LOCATION
,	O
et	O
sa	O
fusion	O
avec	O
Édimbourg	LOCATION
en	O
1920	O
a	O
été	O
à	O
l'	O
origine	O
d'	O
un	O
fort	O
ressentiment	O
.	O
Encore	O
aujourd'hui	O
le	O
siège	O
parlementaire	O
est	O
celui	O
de	O
la	O
circonscription	O
d	O
'	O
"	O
Édimbourg	LOCATION
nord	LOCATION
et	LOCATION
Leith	LOCATION
"	O
.	O
Une	O
augmentation	O
si	O
l'	O
on	O
se	O
réfère	O
au	O
chiffres	O
du	O
recensement	O
de	O
2001	O
qui	O
établissait	O
la	O
population	O
d'	O
Édimbourg	LOCATION
à	O
448	O
624	O
habitants	O
.	O
Alors	O
que	O
la	O
population	O
d'	O
Édimbourg	LOCATION
vieillit	O
,	O
la	O
large	O
proportion	O
de	O
jeunes	O
étudiants	O
dans	O
les	O
universités	O
de	O
la	O
ville	O
permet	O
en	O
quelque	O
sorte	O
de	O
pallier	O
ce	O
problème	O
démographique	O
.	O
Le	O
temps	O
est	O
très	O
imprévisible	O
à	O
Édimbourg	LOCATION
.	O
Édimbourg	O
a	O
toujours	O
été	O
une	O
des	O
villes	O
les	O
plus	O
prospères	O
de	O
Grande-Bretagne	O
.	O
Les	O
taux	O
de	O
chômage	O
sont	O
parmi	O
les	O
plus	O
bas	O
du	O
Royaume-Uni	O
(	O
aux	O
alentours	O
de	O
2,4	O
%	O
)	O
et	O
les	O
taux	O
de	O
création	O
d'	O
emploi	O
parmi	O
les	O
plus	O
hauts	O
.	O
La	O
population	O
d'	O
Édimbourg	LOCATION
croît	O
rapidement	O
,	O
principalement	O
grâce	O
à	O
l'	O
immigration	O
,	O
principalement	O
en	O
provenance	O
du	O
reste	O
du	O
Royaume-Uni	O
.	O
L'	O
économie	O
d'	O
Édimbourg	O
est	O
largement	O
basée	O
sur	O
le	O
secteur	O
des	O
services	O
,	O
principalement	O
autour	O
du	O
tourisme	O
,	O
des	O
services	O
financiers	O
,	O
de	O
l'	O
éducation	O
et	O
de	O
la	O
recherche	O
en	O
haute	O
technologie	O
.	O
La	O
Royal	O
Bank	O
of	O
Scotland	O
fut	O
elle	O
fondée	O
en	O
1747	O
et	O
est	O
désormais	O
la	O
5	O
e	O
banque	O
mondiale	O
par	O
capitalisation	O
boursière	O
.	O
Avec	O
l'	O
ouverture	O
du	O
siège	O
de	O
la	O
Royal	O
Bank	O
of	O
Scotland	O
,	O
ce	O
sont	O
près	O
de	O
20	O
mille	O
personnes	O
qui	O
travaillent	O
dans	O
les	O
faubourgs	O
ouest	O
d'	O
Édimbourg	LOCATION
.	O
Édimbourg	O
est	O
la	O
principale	O
destination	O
des	O
touristes	O
en	O
Écosse	O
et	O
la	O
seconde	O
dans	O
le	O
Royaume-Uni	O
après	O
Londres	LOCATION
,	O
et	O
son	O
importance	O
grandit	O
un	O
peu	O
plus	O
chaque	O
année	O
,	O
supporté	O
par	O
la	O
croissance	O
de	O
l'	O
aéroport	O
d'	O
Édimbourg	LOCATION
et	O
un	O
réseau	O
ferroviaire	O
reliant	O
bien	O
la	O
ville	O
au	O
reste	O
du	O
royaume	O
.	O
Les	O
festivals	O
d'	O
Édimbourg	O
du	O
mois	O
d'	O
août	O
génèrent	O
à	O
eux	O
seuls	O
un	O
revenu	O
de	O
135	O
millions	O
de	O
livres	O
sterling	O
dans	O
l'	O
économie	O
de	O
la	O
ville	O
.	O
Édimbourg	O
centre	O
a	O
été	O
partagé	O
entre	O
les	O
autres	O
circonscriptions	O
.	O
De	O
chaque	O
circonscription	O
est	O
issu	O
un	O
membre	O
du	O
parlement	O
du	O
Royaume-Uni	O
.	O
La	O
ville	O
d'	O
Édimbourg	LOCATION
possède	O
58	O
conseillers	O
qui	O
sont	O
élus	O
pour	O
4	O
ans	O
et	O
qui	O
représentent	O
les	O
habitants	O
d'	O
Édimbourg	LOCATION
.	O
L'	O
histoire	O
de	O
Heriot-Watt	LOCATION
remonte	O
à	O
1821	O
,	O
lorsque	O
fut	O
ouverte	O
une	O
école	O
d'	O
éducation	O
technique	O
pour	O
la	O
classe	O
ouvrière	O
.	O
L'	O
université	O
Napier	O
possède	O
désormais	O
plusieurs	O
campus	O
dans	O
les	O
quartiers	O
sud	O
et	O
ouest	O
de	O
la	O
ville	O
.	O
On	O
trouve	O
d'	O
autres	O
écoles	O
offrant	O
un	O
enseignement	O
supérieur	O
à	O
Édimbourg	LOCATION
.	O
Édimbourg	O
est	O
particulièrement	O
renommée	O
pour	O
son	O
festival	O
international	O
de	O
théâtre	O
,	O
opéra	O
,	O
musique	O
et	O
danse	O
qui	O
a	O
lieu	O
tous	O
les	O
étés	O
au	O
mois	O
d'	O
août	O
et	O
qui	O
rassemble	O
près	O
d'	O
un	O
million	O
de	O
personnes	O
.	O
Édimbourg	O
possède	O
trois	O
musées	O
de	O
grande	O
importance	O
:	O
Un	O
plan	O
de	O
développement	O
pour	O
accompagner	O
la	O
croissance	O
du	O
trafic	O
a	O
été	O
publié	O
en	O
mai	O
2005	O
indiquant	O
que	O
le	O
terminal	O
d'	O
Édimbourg	O
devrait	O
être	O
agrandi	O
.	O
En	O
2006	O
,	O
Édimbourg	LOCATION
ne	O
possède	O
pas	O
de	O
ligne	O
de	O
tramway	O
.	O
En	O
2004	O
,	O
deux	O
projets	O
de	O
loi	O
ont	O
été	O
présentés	O
devant	O
le	O
Parlement	O
écossais	O
.	O
Édimbourg	O
devrait	O
inaugurer	O
son	O
réseau	O
de	O
tramway	O
en	O
2011	O
,	O
composé	O
de	O
3	O
lignes	O
.	O
Édimbourg	LOCATION
possède	O
un	O
large	O
réseau	O
de	O
bus	O
,	O
desservant	O
tous	O
les	O
quartiers	O
de	O
la	O
ville	O
et	O
ses	O
banlieues	O
.	O
Elle	O
dessert	O
également	O
certaines	O
parties	O
de	O
l'	O
East	O
Lothian	O
et	O
du	O
Midlothian	O
.	O
Située	O
dans	O
le	O
centre-ville	O
,	O
tout	O
près	O
des	O
jardins	LOCATION
de	LOCATION
Princes	LOCATION
Street	LOCATION
,	O
elle	O
est	O
utilisée	O
par	O
13	O
millions	O
de	O
voyageurs	O
par	O
an	O
.	O
La	O
First	O
ScotRail	O
assure	O
les	O
liaisons	O
en	O
Écosse	O
,	O
aussi	O
bien	O
que	O
la	O
laison	O
nocturne	O
en	O
voitures-lits	O
Caledonian	O
Sleeper	O
avec	O
Londres	LOCATION
.	O
Édimbourg	O
est	O
dotée	O
d'	O
un	O
réseau	O
ferroviaire	O
urbain	O
assez	O
limité	O
.	O
Servant	O
de	O
boulevard	O
périphérique	O
,	O
elle	O
n'	O
entoure	O
la	O
ville	O
que	O
par	O
le	O
sud	O
,	O
Édimbourg	O
étant	O
située	O
au	O
bord	O
de	O
la	O
mer	O
.	O
Bien	O
qu'	O
Édimbourg	O
soit	O
la	O
capitale	O
,	O
l'	O
équipe	O
d'	O
Écosse	O
de	O
football	O
joue	O
à	O
Hampden	O
Park	O
à	O
Glasgow	LOCATION
.	O
L'	O
équipe	O
nationale	O
de	O
rugby	O
est	O
basée	O
à	O
Murrayfield	LOCATION
.	O
Les	O
matchs	O
internationaux	O
sont	O
joués	O
dans	O
ce	O
stade	O
,	O
propriété	O
du	O
Scottish	O
Rugby	O
Union	O
.	O
Le	O
club	O
joue	O
ses	O
matchs	O
à	O
domicile	O
à	O
la	O
patinoire	O
de	O
Murrayfield	O
.	O
Édimbourg	O
a	O
accueilli	O
différents	O
événements	O
sportifs	O
internationaux	O
,	O
dont	O
les	O
Jeux	O
du	O
Commonwealth	O
britannique	O
en	O
1970	O
et	O
les	O
Jeux	O
du	O
Commonwealth	O
en	O
1986	O
.	O
Édimbourg	O
est	O
jumelée	O
avec	O
:	O
