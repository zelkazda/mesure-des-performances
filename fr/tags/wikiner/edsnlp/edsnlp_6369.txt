Couvrant	O
3210	O
hectares	O
,	O
le	O
territoire	O
de	O
Saint-Clément-Rancoudray	LOCATION
est	O
le	O
plus	O
étendu	O
du	O
canton	O
de	O
Mortain	LOCATION
.	O
En	O
1861	O
,	O
Saint-Clément	LOCATION
(	O
1844	O
habitants	O
en	O
1856	O
)	O
cède	O
la	O
partie	O
sud	O
de	O
son	O
territoire	O
pour	O
la	O
création	O
de	O
la	O
commune	O
de	O
Rancoudray	LOCATION
(	O
soit	O
403	O
habitants	O
)	O
.	O
En	O
1973	O
,	O
les	O
deux	O
communes	O
fusionnent	O
,	O
Rancoudray	LOCATION
gardant	O
le	O
statut	O
de	O
commune	O
associée	O
.	O
Le	O
conseil	O
municipal	O
est	O
composé	O
de	O
15	O
membres	O
,	O
dont	O
4	O
représentant	O
la	O
commune	O
associée	O
de	O
Rancoudray	LOCATION
.	O
Saint-Clément	LOCATION
a	O
compté	O
jusqu'	O
à	O
1444	O
habitants	O
en	O
1856	O
,	O
mais	O
les	O
deux	O
communes	O
,	O
séparées	O
en	O
1861	O
,	O
totalisaient	O
1503	O
habitants	O
en	O
1876	O
(	O
1080	O
pour	O
Saint-Clément	LOCATION
,	O
423	O
pour	O
Rancoudray	LOCATION
)	O
.	O
