L'	O
Équateur	O
est	O
un	O
pays	O
indépendant	O
depuis	O
1830	O
.	O
Puis	O
,	O
de	O
1717	O
à	O
1723	O
,	O
celui-ci	O
fut	O
intégré	O
à	O
la	O
vice-royauté	O
de	O
Nouvelle-Grenade	O
,	O
dont	O
la	O
capitale	O
était	O
Bogotá	O
(	O
Colombie	O
)	O
.	O
Il	O
fut	O
par	O
la	O
suite	O
placé	O
sous	O
l'	O
autorité	O
de	O
Lima	O
jusqu'	O
en	O
1739	O
,	O
puis	O
de	O
nouveau	O
à	O
la	O
Nouvelle-Grenade	O
.	O
Il	O
fallut	O
douze	O
ans	O
de	O
combats	O
au	O
général	O
Antonio	PERSON
José	PERSON
de	PERSON
Sucre	PERSON
et	O
à	O
Simón	PERSON
Bolívar	PERSON
pour	O
remporter	O
une	O
victoire	O
décisive	O
le	O
24	O
mai	O
1822	O
lors	O
de	O
la	O
Bataille	O
de	O
Pichincha	O
et	O
ainsi	O
obtenir	O
la	O
capitulation	O
des	O
troupes	O
espagnoles	O
.	O
En	O
fait	O
,	O
de	O
1830	O
à	O
1948	O
,	O
l'	O
Équateur	O
connut	O
plus	O
de	O
soixante-deux	O
gouvernements	O
successifs	O
,	O
de	O
type	O
présidentiel	O
,	O
militaire	O
ou	O
dictatorial	O
.	O
En	O
juillet	O
1998	O
,	O
le	O
chrétien	O
démocrate	O
Jamil	PERSON
Mahuad	PERSON
(	O
qui	O
était	O
l'	O
ancien	O
maire	O
de	O
Quito	LOCATION
)	O
fut	O
élu	O
président	O
.	O
L'	O
annonce	O
de	O
l'	O
abandon	O
de	O
la	O
monnaie	O
nationale	O
(	O
le	O
sucre	O
)	O
au	O
profit	O
du	O
dollar	O
dans	O
les	O
premiers	O
jour	O
de	O
l'	O
année	O
2000	O
provoqua	O
dès	O
le	O
9	O
janvier	O
,	O
un	O
soulèvement	O
populaire	O
à	O
Quito	LOCATION
.	O
Il	O
est	O
destitué	O
en	O
2005	O
,	O
remplacé	O
par	O
son	O
vice-président	O
Alfredo	PERSON
Palacio	PERSON
.	O
