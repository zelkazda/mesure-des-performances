Bouto	O
est	O
une	O
ville	O
du	O
delta	O
du	O
Nil	O
,	O
qui	O
se	O
situe	O
dans	O
le	O
sixième	O
nome	O
de	O
Basse-Égypte	O
,	O
le	O
nome	O
"	O
Le	O
taureau	O
montagnard	O
"	O
(	O
ou	O
taureau	O
du	O
désert	O
)	O
,	O
à	O
environ	O
90	O
km	O
à	O
l'	O
est	O
d'	O
Alexandrie	LOCATION
.	O
Bouto	O
est	O
un	O
site	O
important	O
de	O
la	O
Basse-Égypte	O
dont	O
elle	O
devient	O
la	O
capitale	O
à	O
la	O
période	O
pré	O
et	O
proto-dynastiques	O
.	O
Ouadjet	O
était	O
la	O
déesse	O
tutélaire	O
protectrice	O
de	O
Bouto	LOCATION
et	O
de	O
sa	O
région	O
.	O
Isis	PERSON
s'	O
enfuit	O
à	O
Bouto	LOCATION
afin	O
d'	O
y	O
élever	O
son	O
fils	O
et	O
de	O
le	O
soustraire	O
à	O
la	O
vengeance	O
de	O
son	O
frère	O
qui	O
règne	O
alors	O
sur	O
le	O
monde	O
.	O
Ouadjet	O
est	O
alors	O
l'	O
uraeus	O
divin	O
qui	O
protège	O
le	O
dieu	O
destiné	O
à	O
la	O
royauté	O
et	O
par	O
extension	O
assiste	O
la	O
renaissance	O
chaque	O
matin	O
de	O
la	O
divinité	O
solaire	O
avec	O
laquelle	O
Horus	PERSON
se	O
confond	O
.	O
La	O
déesse	O
prend	O
également	O
la	O
forme	O
d'	O
une	O
lionne	O
ou	O
d'	O
une	O
divinité	O
léontocéphale	O
se	O
confondant	O
avec	O
la	O
déesse	O
Sekhmet	O
,	O
protectrice	O
du	O
dieu	O
Rê	O
.	O
La	O
forme	O
de	O
son	O
sanctuaire	O
primitif	O
symbolisa	O
rapidement	O
le	O
temple	O
type	O
de	O
Basse-Égypte	O
et	O
nous	O
est	O
restée	O
tout	O
comme	O
celui	O
de	O
Haute-Égypte	O
à	O
Hiérakonpolis	LOCATION
.	O
D'	O
un	O
côté	O
du	O
périmètre	O
se	O
trouvaient	O
des	O
chapelles	O
aux	O
toits	O
arqués	O
,	O
adoptant	O
la	O
forme	O
caractéristique	O
du	O
sanctuaire	O
de	O
Basse-Égypte	O
que	O
l'	O
on	O
retrouve	O
dans	O
d'	O
autres	O
sites	O
de	O
la	O
région	O
comme	O
celui	O
de	O
Saïs	LOCATION
par	O
exemple	O
.	O
Elles	O
sont	O
représentées	O
parfois	O
ouvertes	O
abritant	O
une	O
statue	O
divine	O
dont	O
les	O
dieux	O
Amset	O
et	O
Douamoutef	O
qui	O
reçoivent	O
un	O
culte	O
à	O
Bouto	LOCATION
.	O
Cela	O
induit	O
que	O
la	O
technique	O
de	O
production	O
des	O
céramique	O
du	O
sud	O
y	O
a	O
été	O
importée	O
remplaçant	O
définitivement	O
l'	O
antique	O
et	O
archaïque	O
production	O
du	O
royaume	O
de	O
Basse-Égypte	O
.	O
Or	O
des	O
prospections	O
géophysiques	O
du	O
site	O
ont	O
été	O
pratiquées	O
à	O
grande	O
échelle	O
à	O
Bouto	LOCATION
.	O
Bouto	O
garde	O
alors	O
probablement	O
un	O
rôle	O
religieux	O
que	O
ne	O
démentent	O
pas	O
les	O
textes	O
des	O
Pyramides	O
mais	O
n'	O
est	O
plus	O
le	O
centre	O
du	O
pouvoir	O
ni	O
le	O
centre	O
économique	O
principal	O
du	O
royaume	O
.	O
C'	O
est	O
de	O
Bouto	O
que	O
provient	O
une	O
grande	O
stèle	O
de	O
Thoutmôsis	O
III	O
qui	O
relate	O
les	O
victoires	O
du	O
roi	O
et	O
les	O
richesses	O
qu'	O
il	O
a	O
rapporté	O
de	O
ses	O
conquêtes	O
dont	O
il	O
consacre	O
une	O
partie	O
au	O
sanctuaire	O
de	O
la	O
déesse	O
.	O
Ramsès	O
II	O
laisse	O
dans	O
le	O
temple	O
des	O
statues	O
à	O
son	O
image	O
dont	O
une	O
dyade	O
le	O
représentant	O
coiffé	O
d'	O
un	O
disque	O
solaire	O
assis	O
à	O
côté	O
de	O
la	O
déesse	O
une	O
fois	O
encore	O
léontocéphale	O
également	O
coiffée	O
d'	O
un	O
disque	O
solaire	O
.	O
Hérodote	O
témoigne	O
de	O
la	O
réputation	O
de	O
l'	O
oracle	O
du	O
temple	O
de	O
la	O
déesse	O
et	O
des	O
grandes	O
fêtes	O
qui	O
s'	O
y	O
célébraient	O
.	O
