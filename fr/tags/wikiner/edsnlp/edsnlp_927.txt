Il	O
fut	O
le	O
meneur	O
du	O
projet	O
libre	O
GNOME	O
lancé	O
en	O
1997	O
et	O
destiné	O
à	O
apporter	O
la	O
convivialité	O
au	O
système	O
GNU	O
/	O
Linux	O
(	O
en	O
alternative	O
à	O
KDE	O
,	O
basé	O
sur	O
la	O
bibliothèque	O
graphique	O
Qt	O
,	O
à	O
l'	O
époque	O
propriétaire	O
)	O
.	O
En	O
1999	O
,	O
il	O
reçoit	O
le	O
Prix	O
pour	O
le	O
développement	O
du	O
logiciel	O
libre	O
de	O
la	O
FSF	O
pour	O
sa	O
contribution	O
au	O
logiciel	O
libre	O
à	O
travers	O
le	O
projet	O
GNOME	O
.	O
Par	O
la	O
suite	O
,	O
il	O
devient	O
l'	O
un	O
des	O
cofondateurs	O
de	O
la	O
société	O
Ximian	O
,	O
qui	O
commercialise	O
notamment	O
des	O
versions	O
paquetées	O
de	O
GNOME	O
.	O
Ximian	O
est	O
ensuite	O
rachetée	O
par	O
Novell	O
,	O
où	O
Miguel	PERSON
de	PERSON
Icaza	PERSON
travaille	O
toujours	O
,	O
en	O
qualité	O
de	O
vice-président	O
en	O
charge	O
du	O
développement	O
.	O
Miguel	PERSON
de	PERSON
Icaza	PERSON
est	O
connu	O
pour	O
son	O
franc-parler	O
et	O
son	O
goût	O
pour	O
la	O
polémique	O
.	O
Il	O
a	O
en	O
particulier	O
alimenté	O
la	O
"	O
guerre	O
des	O
bureaux	O
"	O
entre	O
KDE	O
et	O
GNOME	O
et	O
s'	O
est	O
fait	O
remarquer	O
par	O
une	O
provocation	O
sous	O
la	O
forme	O
d'	O
une	O
critique	O
du	O
système	O
UNIX	O
lors	O
d'	O
une	O
conférence	O
[	O
précision	O
nécessaire	O
]	O
.	O
Il	O
a	O
également	O
lancé	O
en	O
2001	O
le	O
projet	O
Mono	O
,	O
une	O
mise	O
en	O
œuvre	O
libre	O
de	O
la	O
plate-forme	O
de	O
développement	O
Microsoft	O
.NET	O
.	O
Il	O
n'	O
a	O
pas	O
participé	O
aux	O
négociations	O
qui	O
ont	O
précédé	O
l'	O
accord	O
du	O
2	O
novembre	O
2006	O
passé	O
entre	O
Novell	O
et	O
Microsoft	O
,	O
lequel	O
concerne	O
en	O
partie	O
le	O
projet	O
Mono	O
et	O
les	O
risques	O
de	O
poursuite	O
pour	O
violation	O
de	O
brevets	O
.	O
En	O
2001	O
,	O
il	O
a	O
fait	O
une	O
apparition	O
dans	O
le	O
film	O
Antitrust	O
.	O
