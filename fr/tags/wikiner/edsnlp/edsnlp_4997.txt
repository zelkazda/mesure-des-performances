C'	O
est	O
un	O
drapeau	O
tricolore	O
composé	O
de	O
trois	O
bandes	O
horizontales	O
égales	O
aux	O
couleurs	O
nationales	O
de	O
l'	O
Allemagne	O
:	O
noir	O
,	O
rouge	O
et	O
or	O
.	O
Avec	O
la	O
naissance	O
de	O
la	O
République	O
de	O
Weimar	O
,	O
après	O
la	O
Première	O
Guerre	O
mondiale	O
,	O
il	O
est	O
adopté	O
comme	O
drapeau	O
national	O
de	O
l'	O
Allemagne	O
.	O
Suite	O
à	O
la	O
Seconde	O
Guerre	O
mondiale	O
,	O
il	O
devient	O
le	O
drapeau	O
des	O
deux	O
Allemagnes	O
.	O
Ce	O
n'	O
est	O
qu'	O
en	O
1959	O
que	O
des	O
symboles	O
socialistes	O
sont	O
ajoutés	O
au	O
drapeau	O
de	O
l'	O
Allemagne	O
de	O
l'	O
Est	O
,	O
afin	O
de	O
le	O
différencier	O
de	O
celui	O
de	O
l'	O
Allemagne	O
de	O
l'	O
Ouest	O
.	O
Les	O
couleurs	O
noir-blanc-rouge	O
sont	O
réintroduites	O
avec	O
la	O
fondation	O
du	O
Troisième	O
Reich	O
en	O
1933	O
.	O
Les	O
couleurs	O
noir-rouge-or	O
et	O
noir-blanc-rouge	O
ont	O
joué	O
un	O
rôle	O
important	O
dans	O
l'	O
histoire	O
de	O
l'	O
Allemagne	O
,	O
et	O
possèdent	O
plusieurs	O
significations	O
.	O
Suivant	O
les	O
spécifications	O
édictées	O
par	O
le	O
gouvernement	O
(	O
ouest-	O
)	O
allemand	O
en	O
1950	O
,	O
le	O
drapeau	O
se	O
compose	O
de	O
trois	O
bandes	O
de	O
même	O
largeur	O
,	O
avec	O
un	O
rapport	O
largeur/longueur	O
de	O
3:5	O
,	O
alors	O
que	O
le	O
tricolore	O
utilisé	O
à	O
l'	O
époque	O
de	O
la	O
République	O
de	O
Weimar	O
avait	O
un	O
rapport	O
de	O
2:3	O
.	O
Leur	O
usage	O
pour	O
marquer	O
d'	O
autres	O
événements	O
,	O
comme	O
l'	O
élection	O
du	O
président	O
fédéral	O
ou	O
la	O
mort	O
d'	O
un	O
homme	O
politique	O
important	O
,	O
peuvent	O
être	O
déclarés	O
à	O
la	O
discrétion	O
du	O
Ministère	O
fédéral	O
de	O
l'	O
Intérieur	O
.	O
Cette	O
confédération	O
n'	O
a	O
pas	O
de	O
drapeau	O
propre	O
:	O
elle	O
utilise	O
le	O
drapeau	O
tricolore	O
français	O
et	O
l'	O
étendard	O
impérial	O
de	O
son	O
"	O
protecteur	O
"	O
Napoléon	O
.	O
Ce	O
Parlement	O
de	O
Francfort	LOCATION
déclare	O
couleurs	O
officielles	O
de	O
l'	O
Allemagne	O
le	O
noir-rouge-or	O
et	O
vote	O
une	O
loi	O
selon	O
laquelle	O
son	O
pavillon	O
civil	O
est	O
le	O
tricolore	O
noir-rouge-or	O
.	O
En	O
1850	O
,	O
le	O
Parlement	O
de	O
Francfort	LOCATION
s'	O
effondre	O
et	O
la	O
Confédération	O
germanique	O
est	O
rétablie	O
,	O
sous	O
présidence	O
autrichienne	O
,	O
et	O
toutes	O
les	O
mesures	O
prises	O
par	O
le	O
Parlement	O
de	O
Francfort	LOCATION
sont	O
annulées	O
,	O
y	O
compris	O
le	O
drapeau	O
tricolore	O
.	O
Le	O
roi	O
Guillaume	O
I	O
er	O
de	O
Prusse	O
est	O
satisfait	O
de	O
ce	O
choix	O
de	O
couleurs	O
:	O
le	O
rouge	O
et	O
le	O
blanc	O
peuvent	O
également	O
représenter	O
le	O
margraviat	O
de	O
Brandebourg	O
,	O
l'	O
électorat	O
impérial	O
ancêtre	O
du	O
royaume	O
de	O
Prusse	O
.	O
Les	O
anciennes	O
couleurs	O
continuent	O
d'	O
être	O
utilisées	O
dans	O
la	O
Reichswehr	O
sous	O
diverses	O
formes	O
.	O
De	O
nombreux	O
partis	O
politiques	O
nationalistes	O
de	O
l'	O
époque	O
,	O
comme	O
le	O
Parti	O
national	O
du	O
peuple	O
allemand	O
ou	O
le	O
Parti	O
national-socialiste	O
des	O
travailleurs	O
allemands	O
,	O
utilisent	O
les	O
couleurs	O
impériales	O
,	O
une	O
pratique	O
poursuivie	O
aujourd'hui	O
par	O
le	O
Parti	O
national-démocrate	O
d'	O
Allemagne	O
.	O
Son	O
but	O
est	O
de	O
protéger	O
la	O
fragile	O
démocratie	O
de	O
la	O
République	O
de	O
Weimar	O
,	O
soumise	O
à	O
des	O
pressions	O
constantes	O
de	O
l'	O
extrême-gauche	O
et	O
de	O
l'	O
extrême-droite	O
.	O
Les	O
conflits	O
de	O
plus	O
en	O
plus	O
violents	O
entre	O
communistes	O
et	O
nazis	O
,	O
la	O
polarisation	O
croissante	O
de	O
la	O
population	O
allemande	O
et	O
d'	O
autres	O
facteurs	O
entraînent	O
la	O
chute	O
de	O
la	O
République	O
de	O
Weimar	O
en	O
1933	O
,	O
avec	O
la	O
prise	O
du	O
pouvoir	O
par	O
les	O
nazis	O
et	O
l'	O
arrivée	O
d'	O
Adolf	PERSON
Hitler	O
au	O
poste	O
de	O
chancelier	O
.	O
Le	O
dessin	O
du	O
drapeau	O
nazi	O
est	O
introduit	O
par	O
Hitler	O
comme	O
drapeau	O
du	O
parti	O
durant	O
l'	O
été	O
1920	O
:	O
sur	O
fond	O
rouge	O
,	O
un	O
disque	O
blanc	O
avec	O
une	O
svastika	O
noire	O
au	O
milieu	O
.	O
Ce	O
choix	O
des	O
couleurs	O
rappelle	O
l'	O
Allemagne	O
impériale	O
,	O
et	O
Hitler	O
y	O
ajoute	O
une	O
autre	O
interprétation	O
dans	O
Mein	O
Kampf	O
:	O
le	O
blanc	O
représente	O
le	O
nationalisme	O
,	O
le	O
rouge	O
le	O
socialisme	O
,	O
et	O
la	O
svastika	O
la	O
race	O
aryenne	O
.	O
Une	O
version	O
du	O
drapeau	O
avec	O
le	O
disque	O
excentré	O
est	O
utilisée	O
comme	O
enseigne	O
civile	O
des	O
navires	O
civils	O
enregistrés	O
en	O
Allemagne	O
et	O
comme	O
pavillon	O
sur	O
les	O
navires	O
de	O
guerre	O
de	O
la	O
Kriegsmarine	O
.	O
De	O
1933	O
à	O
1938	O
au	O
moins	O
,	O
avant	O
qu'	O
un	O
drapeau	O
à	O
la	O
svastika	O
officiel	O
n'	O
entre	O
en	O
usage	O
,	O
il	O
doit	O
prendre	O
place	O
à	O
une	O
cérémonie	O
durant	O
laquelle	O
il	O
entre	O
en	O
contact	O
avec	O
le	O
Blutfahne	O
(	O
drapeau	O
du	O
sang	O
)	O
,	O
le	O
drapeau	O
à	O
la	O
svastika	O
utilisé	O
par	O
les	O
nazis	O
lors	O
du	O
putsch	O
de	O
la	O
Brasserie	O
en	O
1923	O
.	O
Cette	O
longue	O
cérémonie	O
a	O
lieu	O
lors	O
de	O
chaque	O
congrès	O
de	O
Nuremberg	O
.	O
À	O
la	O
fin	O
de	O
la	O
Seconde	O
Guerre	O
mondiale	O
,	O
la	O
première	O
loi	O
promulguée	O
par	O
le	O
Conseil	O
de	O
contrôle	O
allié	O
abolit	O
tous	O
les	O
symboles	O
nazis	O
et	O
révoque	O
toutes	O
les	O
lois	O
concernées	O
.	O
La	O
possession	O
de	O
drapeaux	O
à	O
la	O
svastika	O
est	O
depuis	O
interdite	O
dans	O
de	O
nombreux	O
pays	O
occidentaux	O
,	O
notamment	O
en	O
Allemagne	O
.	O
Par	O
coïncidence	O
,	O
les	O
couleurs	O
du	O
Bade	O
sont	O
le	O
rouge	O
et	O
le	O
jaune	O
,	O
si	O
bien	O
que	O
le	O
choix	O
des	O
couleurs	O
pourrait	O
faire	O
penser	O
(	O
à	O
tort	O
)	O
à	O
une	O
combinaison	O
des	O
deux	O
drapeaux	O
.	O
Pendant	O
ce	O
temps	O
,	O
la	O
zone	O
soviétique	O
devient	O
la	O
"	O
République	O
démocratique	O
allemande	O
"	O
,	O
ou	O
Allemagne	O
de	O
l'	O
Est	O
.	O
Cette	O
décision	O
est	O
essentiellement	O
motivée	O
par	O
la	O
constitution	O
proposée	O
par	O
le	O
parti	O
d'	O
Allemagne	O
de	O
l'	O
Est	O
SED	O
en	O
novembre	O
1946	O
,	O
dans	O
laquelle	O
les	O
couleurs	O
noir-rouge-or	O
sont	O
proposées	O
pour	O
une	O
future	O
république	O
allemande	O
.	O
De	O
1949	O
à	O
1959	O
,	O
les	O
drapeaux	O
des	O
deux	O
Allemagnes	O
sont	O
identiques	O
.	O
En	O
Allemagne	O
de	O
l'	O
Ouest	O
,	O
cette	O
modification	O
est	O
perçue	O
comme	O
une	O
tentative	O
délibérée	O
d'	O
éloigner	O
les	O
deux	O
Allemagnes	O
.	O
Cet	O
acte	O
,	O
largement	O
répandu	O
,	O
fait	O
implicitement	O
du	O
drapeau	O
tricolore	O
noir-rouge-or	O
le	O
symbole	O
d'	O
une	O
Allemagne	O
unie	O
et	O
démocratique	O
.	O
Cette	O
utilisation	O
de	O
l'	O
ancien	O
drapeau	O
est	O
toutefois	O
presque	O
entièrement	O
occultée	O
par	O
l'	O
usage	O
qu'	O
en	O
fait	O
l'	O
extrême-droite	O
:	O
la	O
svastika	O
étant	O
interdite	O
en	O
Allemagne	O
,	O
elle	O
est	O
obligée	O
de	O
renoncer	O
aux	O
drapeaux	O
nazis	O
et	O
de	O
se	O
rabattre	O
sur	O
l'	O
ancien	O
drapeau	O
impérial	O
,	O
que	O
les	O
nazis	O
avaient	O
eux-mêmes	O
interdit	O
en	O
1935	O
.	O
L'	O
usage	O
du	O
drapeau	O
et	O
d'	O
autres	O
symboles	O
nationaux	O
est	O
relativement	O
faible	O
en	O
Allemagne	O
,	O
en	O
réaction	O
à	O
l'	O
usage	O
généralisé	O
des	O
drapeaux	O
par	O
le	O
parti	O
nazi	O
et	O
au	O
bruyant	O
nationalisme	O
des	O
nazis	O
en	O
général	O
.	O
Durant	O
la	O
Coupe	O
du	O
monde	O
de	O
football	O
de	O
2006	O
,	O
organisée	O
en	O
Allemagne	O
,	O
l'	O
usage	O
public	O
du	O
drapeau	O
national	O
connaît	O
une	O
croissance	O
importante	O
.	O
