Elle	O
mourut	O
alors	O
que	O
Dante	PERSON
avait	O
13	O
ans	O
,	O
en	O
1278	O
(	O
son	O
père	O
décèda	O
5	O
ans	O
plus	O
tard	O
en	O
1282	O
)	O
.	O
Peu	O
de	O
choses	O
sont	O
connues	O
sur	O
l'	O
éducation	O
de	O
Dante	PERSON
;	O
on	O
présume	O
qu'	O
il	O
étudiait	O
à	O
domicile	O
.	O
Il	O
rencontre	O
des	O
poètes	O
et	O
noue	O
une	O
solide	O
amitié	O
avec	O
Guido	PERSON
Cavalcanti	PERSON
.	O
Il	O
est	O
quasi	O
certain	O
qu'	O
il	O
étudia	O
la	O
poésie	O
toscane	O
,	O
au	O
moment	O
où	O
l'	O
école	O
poétique	O
sicilienne	O
,	O
un	O
groupe	O
culturel	O
originaire	O
de	O
Sicile	O
,	O
a	O
commencé	O
à	O
être	O
connue	O
en	O
Toscane	O
.	O
On	O
peut	O
supposer	O
que	O
Dante	O
était	O
un	O
intellectuel	O
en	O
phase	O
avec	O
son	O
époque	O
,	O
raffiné	O
et	O
avec	O
des	O
centres	O
d'	O
intérêts	O
pour	O
ainsi	O
dire	O
"	O
internationaux	O
"	O
.	O
C'	O
est	O
en	O
1274	O
que	O
Dante	O
aurait	O
rencontré	O
pour	O
la	O
première	O
fois	O
Béatrice	PERSON
.	O
Il	O
est	O
difficile	O
de	O
savoir	O
si	O
Dante	O
envisageait	O
véritablement	O
un	O
culte	O
de	O
Béatrice	O
qui	O
orienterait	O
ainsi	O
toute	O
son	O
œuvre	O
,	O
mais	O
il	O
est	O
certain	O
que	O
sa	O
conception	O
de	O
la	O
cité	O
est	O
tributaire	O
de	O
la	O
vie	O
et	O
de	O
la	O
mort	O
de	O
Béatrice	PERSON
:	O
en	O
effet	O
,	O
après	O
la	O
mort	O
de	O
la	O
gentilissima	O
(	O
la	O
très	O
noble	O
,	O
la	O
très	O
courtoise	O
)	O
,	O
Florence	PERSON
est	O
veuve	O
et	O
Béatrice	O
devient	O
un	O
nom	O
commun	O
(	O
Florence	O
a	O
perdu	O
sa	O
Béatrice	O
écrit	O
le	O
poète	O
)	O
.	O
Dante	PERSON
joue	O
un	O
rôle	O
très	O
actif	O
dans	O
la	O
vie	O
politique	O
de	O
Florence	LOCATION
.	O
Il	O
remplit	O
avec	O
succès	O
un	O
grand	O
nombre	O
de	O
missions	O
politiques	O
et	O
fut	O
nommé	O
prieur	O
de	O
Florence	LOCATION
en	O
1300	O
,	O
c'	O
est-à-dire	O
qu'	O
il	O
devient	O
un	O
des	O
magistrats	O
suprêmes	O
de	O
l'	O
exécutif	O
.	O
En	O
1300	O
,	O
le	O
pape	O
Boniface	O
VIII	O
revendique	O
le	O
vicariat	O
impérial	O
sur	O
les	O
communes	O
toscanes	O
.	O
À	O
partir	O
de	O
ce	O
moment-là	O
,	O
Dante	O
s'	O
engage	O
de	O
plus	O
en	O
plus	O
fermement	O
du	O
côté	O
des	O
guelfes	O
blancs	O
,	O
c'	O
est-à-dire	O
contre	O
la	O
politique	O
d'	O
ingérence	O
du	O
pape	O
.	O
Pendant	O
ce	O
temps	O
,	O
Charles	O
de	O
Valois	O
,	O
représentant	O
du	O
pape	O
,	O
se	O
rend	O
à	O
Florence	LOCATION
et	O
s'	O
empare	O
de	O
la	O
ville	O
avec	O
l'	O
aide	O
des	O
guelfes	O
noirs	O
triomphants	O
.	O
Dante	PERSON
apprend	O
sur	O
le	O
chemin	O
du	O
retour	O
qu'	O
il	O
est	O
condamné	O
pour	O
concussion	O
,	O
gains	O
illicites	O
et	O
insoumission	O
au	O
pape	O
et	O
à	O
Charles	O
de	O
Valois	O
.	O
Tous	O
ses	O
biens	O
sont	O
confisqués	O
,	O
il	O
est	O
exilé	O
avec	O
d'	O
autres	O
guelfes	O
blancs	O
et	O
ne	O
reviendra	O
jamais	O
à	O
Florence	LOCATION
.	O
Le	O
décret	O
de	O
bannissement	O
de	O
Dante	O
de	O
la	O
ville	O
de	O
Florence	LOCATION
ne	O
fut	O
d'	O
ailleurs	O
révoqué	O
qu'	O
en	O
2008	O
.	O
Dans	O
les	O
premiers	O
temps	O
de	O
l'	O
exil	O
,	O
Dante	O
songea	O
à	O
assiéger	O
la	O
ville	O
,	O
aux	O
côtés	O
d'	O
autres	O
exilés	O
guelfes	O
blancs	O
ou	O
gibelins	O
.	O
Les	O
années	O
de	O
l'	O
exil	O
sont	O
pour	O
Dante	O
une	O
période	O
d'	O
intense	O
activité	O
intellectuelle	O
.	O
Dante	O
arrive	O
à	O
la	O
conclusion	O
qu'	O
aucune	O
langue	O
vulgaire	O
n'	O
est	O
supérieure	O
à	O
une	O
autre	O
et	O
donc	O
susceptible	O
de	O
s'	O
imposer	O
.	O
Dans	O
le	O
deuxième	O
livre	O
,	O
Dante	O
montre	O
qu'	O
une	O
langue	O
vulgaire	O
mais	O
soignée	O
peut	O
être	O
utilisée	O
pour	O
les	O
plus	O
nobles	O
sujets	O
,	O
et	O
peut	O
même	O
s'	O
appliquer	O
au	O
style	O
tragique	O
.	O
Il	O
apparaît	O
qu'	O
en	O
1305	O
Dante	O
cesse	O
la	O
rédaction	O
du	O
De	O
vulgari	O
eloquentia	O
sans	O
l'	O
avoir	O
achevé	O
,	O
puisqu'	O
il	O
n'	O
a	O
écrit	O
que	O
deux	O
livres	O
sur	O
les	O
quatre	O
initialement	O
prévus	O
.	O
Il	O
semble	O
que	O
Dante	O
se	O
donne	O
pour	O
mission	O
d'	O
ouvrir	O
les	O
portes	O
de	O
la	O
culture	O
et	O
de	O
la	O
science	O
antique	O
et	O
contemporaine	O
au	O
plus	O
grand	O
nombre	O
.	O
Il	O
dit	O
son	O
amertume	O
d'	O
avoir	O
été	O
rejeté	O
par	O
Florence	LOCATION
,	O
sa	O
ville	O
natale	O
qui	O
l'	O
a	O
élevé	O
en	O
son	O
sein	O
avant	O
de	O
le	O
rejeter	O
.	O
C'	O
est	O
peut-être	O
le	O
décès	O
d'	O
Henri	O
VII	O
en	O
1313	O
qui	O
lui	O
donnera	O
l'	O
idée	O
de	O
ce	O
nouveau	O
traité	O
.	O
En	O
effet	O
,	O
avec	O
la	O
mort	O
du	O
monarque	O
disparaissent	O
tous	O
les	O
espoirs	O
de	O
Dante	O
de	O
voir	O
un	O
jour	O
l'	O
autorité	O
impériale	O
restaurée	O
sur	O
la	O
péninsule	O
,	O
au	O
détriment	O
de	O
celle	O
du	O
pape	O
.	O
Dans	O
le	O
premier	O
livre	O
du	O
traité	O
,	O
Dante	O
fait	O
l'	O
éloge	O
de	O
la	O
monarchie	O
universelle	O
comme	O
système	O
politique	O
idéal	O
pour	O
garantir	O
la	O
justice	O
et	O
la	O
paix	O
et	O
,	O
par	O
conséquent	O
,	O
le	O
bonheur	O
des	O
hommes	O
.	O
En	O
dehors	O
des	O
traités	O
,	O
il	O
nous	O
est	O
parvenu	O
de	O
lui	O
deux	O
églogues	O
en	O
latin	O
construites	O
à	O
la	O
manière	O
de	O
Virgile	O
dont	O
il	O
est	O
,	O
depuis	O
sa	O
jeunesse	O
,	O
un	O
fervent	O
admirateur	O
.	O
Bien	O
que	O
le	O
rassemblement	O
et	O
l'	O
organisation	O
de	O
ces	O
textes	O
soit	O
postérieur	O
à	O
Dante	O
,	O
il	O
est	O
probable	O
qu'	O
il	O
soit	O
l'	O
auteur	O
de	O
la	O
majeure	O
partie	O
des	O
poésies	O
.	O
Dante	O
commence	O
la	O
rédaction	O
de	O
la	O
Divine	O
Comédie	O
dès	O
1306	O
et	O
la	O
poursuivra	O
vraisemblablement	O
jusqu'	O
à	O
sa	O
mort	O
.	O
Là	O
,	O
il	O
rencontre	O
Virgile	O
qui	O
l'	O
invite	O
à	O
pénétrer	O
dans	O
le	O
monde	O
de	O
l'	O
au-delà	O
.	O
Dante	O
le	O
suit	O
et	O
c'	O
est	O
par	O
la	O
visite	O
de	O
l'	O
enfer	O
que	O
commence	O
son	O
périple	O
,	O
suivra	O
le	O
purgatoire	O
et	O
enfin	O
le	O
paradis	O
.	O
Il	O
faudra	O
à	O
Dante	O
toute	O
la	O
semaine	O
sainte	O
de	O
l'	O
année	O
1300	O
pour	O
effectuer	O
la	O
totalité	O
de	O
ce	O
voyage	O
.	O
Au	O
cours	O
de	O
son	O
périple	O
,	O
Dante	O
va	O
rencontrer	O
une	O
centaine	O
de	O
personnalités	O
,	O
depuis	O
les	O
grandes	O
figures	O
mythiques	O
de	O
l'	O
antiquité	O
comme	O
les	O
philosophes	O
,	O
jusqu'	O
aux	O
personnalités	O
locales	O
contemporaines	O
de	O
Dante	O
.	O
Cette	O
œuvre	O
monumentale	O
offre	O
ainsi	O
de	O
nombreuses	O
lectures	O
différentes	O
,	O
elle	O
est	O
à	O
la	O
fois	O
le	O
récit	O
du	O
parcours	O
personnel	O
de	O
Dante	O
,	O
un	O
manuel	O
théologique	O
chrétien	O
de	O
description	O
de	O
l'	O
au-delà	O
,	O
un	O
roman	O
à	O
valeur	O
éthique	O
et	O
morale	O
ou	O
encore	O
une	O
réflexion	O
sur	O
la	O
recherche	O
du	O
salut	O
éternel	O
.	O
Une	O
partie	O
du	O
génie	O
de	O
Dante	O
réside	O
en	O
ce	O
savant	O
mélange	O
de	O
lieux	O
imaginaires	O
et	O
d'	O
expériences	O
concrètes	O
.	O
Bien	O
que	O
l'	O
action	O
se	O
situe	O
dans	O
un	O
univers	O
totalement	O
métaphysique	O
,	O
Dante	O
sait	O
décrire	O
les	O
lieux	O
avec	O
force	O
détails	O
et	O
leur	O
donne	O
beaucoup	O
de	O
réalisme	O
en	O
les	O
peuplant	O
de	O
toutes	O
ces	O
figures	O
célèbres	O
ou	O
anonymes	O
.	O
C'	O
est	O
le	O
portrait	O
de	O
Dante	O
par	O
Raphaël	O
qui	O
a	O
été	O
retenu	O
(	O
après	O
un	O
vote	O
populaire	O
)	O
pour	O
figurer	O
sur	O
la	O
face	O
nationale	O
italienne	O
de	O
la	O
pièce	O
de	O
deux	O
euros	O
.	O
Il	O
existe	O
aussi	O
un	O
buste	O
de	O
Dante	O
dans	O
un	O
parc	O
de	O
la	LOCATION
petite	LOCATION
Italie	LOCATION
à	O
Montréal	LOCATION
,	O
au	O
Canada	O
.	O
Outre	O
la	O
Divine	O
Comédie	O
,	O
Dante	O
a	O
aussi	O
composé	O
:	O
