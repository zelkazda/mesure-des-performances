On	O
entend	O
également	O
parfois	O
par	O
théorie	O
naïve	O
des	O
ensembles	O
la	O
théorie	O
des	O
ensembles	O
telle	O
que	O
la	O
concevait	O
et	O
développait	O
son	O
créateur	O
,	O
Georg	PERSON
Cantor	PERSON
,	O
qui	O
n'	O
était	O
pas	O
axiomatisée	O
,	O
et	O
que	O
l'	O
on	O
connait	O
par	O
ses	O
articles	O
et	O
sa	O
correspondance	O
.	O
Enfin	O
la	O
théorie	O
naïve	O
désigne	O
parfois	O
une	O
théorie	O
contradictoire	O
à	O
usage	O
pédagogique	O
formée	O
des	O
axiomes	O
d'	O
extensionnalité	O
et	O
de	O
compréhension	O
non	O
restreinte	O
,	O
qui	O
n'	O
a	O
d'	O
autre	O
intérêt	O
que	O
d'	O
introduire	O
les	O
axiomes	O
de	O
la	O
théorie	O
des	O
ensembles	O
,	O
et	O
qui	O
ne	O
doit	O
pas	O
être	O
identifiée	O
à	O
celle	O
de	O
Cantor	PERSON
.	O
Le	O
livre	O
de	O
Paul	PERSON
Halmos	PERSON
,	O
paru	O
en	O
1960	O
,	O
a	O
popularisé	O
cette	O
terminologie	O
d'	O
abord	O
dans	O
les	O
pays	O
de	O
langue	O
anglaise	O
.	O
Cependant	O
les	O
conceptions	O
de	O
Cantor	PERSON
ont	O
forcément	O
évolué	O
au	O
fil	O
du	O
temps	O
.	O
Certaines	O
de	O
ses	O
lettres	O
,	O
par	O
exemple	O
sa	O
lettre	O
à	O
Richard	PERSON
Dedekind	PERSON
de	O
1899	O
,	O
jouent	O
aujourd'hui	O
un	O
rôle	O
important	O
pour	O
comprendre	O
ses	O
conceptions	O
,	O
or	O
sa	O
correspondance	O
n'	O
a	O
été	O
publiée	O
qu'	O
en	O
1932	O
.	O
Bref	O
il	O
n'	O
est	O
pas	O
si	O
simple	O
de	O
caractériser	O
"	O
la	O
"	O
théorie	O
des	O
ensembles	O
de	O
Cantor	PERSON
.	O
Il	O
semble	O
cependant	O
que	O
très	O
tôt	O
Cantor	O
ait	O
pensé	O
que	O
toute	O
propriété	O
ne	O
pouvait	O
définir	O
un	O
ensemble	O
,	O
il	O
distingue	O
en	O
particulier	O
les	O
ensembles	O
transfinis	O
,	O
et	O
l'	O
infini	O
absolu	O
,	O
qui	O
est	O
indépassable	O
,	O
comme	O
celui	O
de	O
la	O
collection	O
de	O
tous	O
les	O
ordinaux	O
qui	O
ne	O
peut	O
constituer	O
un	O
ensemble	O
.	O
Russell	O
a	O
d'	O
ailleurs	O
montré	O
que	O
son	O
paradoxe	O
rendait	O
contradictoire	O
la	O
théorie	O
de	O
Frege	O
,	O
qui	O
est	O
une	O
théorie	O
des	O
fondements	O
dont	O
les	O
aspects	O
logiques	O
(	O
auxquels	O
Cantor	O
s'	O
intéressait	O
peu	O
)	O
ont	O
eu	O
une	O
grande	O
influence	O
,	O
mais	O
qui	O
utilisait	O
une	O
version	O
de	O
la	O
compréhension	O
non	O
restreinte	O
,	O
c'	O
est-à-dire	O
la	O
possibilité	O
de	O
définir	O
un	O
ensemble	O
en	O
compréhension	O
à	O
partir	O
de	O
n'	O
importe	O
quelle	O
propriété	O
du	O
langage	O
étudié	O
.	O
Cependant	O
Cantor	O
n'	O
a	O
jamais	O
affirmé	O
que	O
l'	O
on	O
pouvait	O
définir	O
un	O
ensemble	O
en	O
compréhension	O
à	O
partir	O
de	O
n'	O
importe	O
quelle	O
propriété	O
sans	O
aucune	O
restriction	O
.	O
Il	O
reste	O
que	O
la	O
découverte	O
de	O
ces	O
paradoxes	O
ou	O
antinomies	O
,	O
a	O
joué	O
un	O
rôle	O
important	O
dans	O
le	O
développement	O
de	O
la	O
théorie	O
des	O
ensembles	O
après	O
Cantor	O
,	O
en	O
particulier	O
son	O
axiomatisation	O
a	O
été	O
en	O
partie	O
développée	O
en	O
réponse	O
à	O
ceux-ci	O
,	O
pour	O
déterminer	O
précisément	O
quelles	O
définitions	O
d'	O
ensembles	O
pouvaient	O
être	O
autorisées	O
.	O
Les	O
paradoxes	O
montrent	O
que	O
la	O
théorie	O
des	O
ensembles	O
au	O
sens	O
de	O
Cantor	O
est	O
une	O
théorie	O
contradictoire	O
.	O
Comme	O
ces	O
classes	O
ne	O
sont	O
pas	O
des	O
ensembles	O
,	O
les	O
paradoxes	O
tels	O
que	O
celui	O
de	O
Russell	O
sont	O
évités	O
.	O
