Cette	O
mesure	O
politique	O
est	O
en	O
usage	O
depuis	O
l'	O
Antiquité	O
et	O
donne	O
par	O
exemple	O
naissance	O
au	O
domaine	O
public	O
dans	O
la	O
Rome	O
antique	O
.	O
Il	O
connaît	O
son	O
âge	O
d'	O
or	O
en	O
Europe	O
entre	O
1945	O
et	O
1973	O
,	O
lorsque	O
le	O
consensus	O
politique	O
qui	O
suit	O
la	O
Seconde	O
Guerre	O
mondiale	O
est	O
favorable	O
à	O
la	O
nationalisation	O
des	O
secteurs	O
stratégiques	O
des	O
services	O
et	O
de	O
l'	O
industrie	O
.	O
Elle	O
est	O
généralement	O
présentée	O
comme	O
une	O
sanction	O
soit	O
judiciaire	O
(	O
affaire	O
des	O
Templiers	O
,	O
affaire	O
Ioukos	O
tout	O
récemment	O
)	O
,	O
soit	O
extra	O
judiciaire	O
(	O
nationalisation	O
de	O
Renault	O
alors	O
que	O
la	O
mort	O
de	O
Louis	PERSON
Renault	PERSON
plusieurs	O
mois	O
auparavant	O
a	O
éteint	O
les	O
poursuites	O
engagées	O
contre	O
lui	O
pour	O
collaboration	O
avec	O
l'	O
occupant	O
)	O
.	O
Voulant	O
éviter	O
les	O
désagréments	O
connus	O
en	O
URSS	O
à	O
ses	O
débuts	O
,	O
la	O
République	O
populaire	O
de	O
Chine	O
ne	O
pratique	O
pas	O
de	O
nationalisations	O
massives	O
entre	O
1949	O
et	O
1953	O
.	O
Les	O
pratiques	O
actuelles	O
de	O
Hugo	PERSON
Chavez	PERSON
et	O
Evo	PERSON
Morales	PERSON
renouent	O
avec	O
ces	O
méthodes	O
(	O
sans	O
nationaliser	O
de	O
façon	O
complète	O
,	O
mais	O
en	O
négociant	O
avec	O
les	O
compagnies	O
privées	O
étrangères	O
)	O
.	O
La	O
nationalisation	O
iranienne	O
de	O
l'	O
Anglo-Iranian	O
Oil	O
Company	O
provoque	O
ainsi	O
le	O
renversement	O
du	O
gouvernement	O
Mossadegh	PERSON
par	O
la	O
CIA	O
.	O
La	O
Seconde	O
Guerre	O
mondiale	O
amplifie	O
le	O
phénomène	O
.	O
Margaret	PERSON
Thatcher	O
entreprend	O
un	O
vaste	O
programme	O
de	O
privatisations	O
à	O
partir	O
de	O
1979	O
.	O
Depuis	O
1997	O
,	O
et	O
la	O
victoire	O
du	O
Labour	O
,	O
Tony	PERSON
Blair	O
ne	O
modifie	O
pas	O
cette	O
orientation	O
,	O
poursuivant	O
même	O
le	O
programme	O
de	O
privatisations	O
.	O
Cette	O
nouvelle	O
approche	O
est	O
l'	O
un	O
des	O
fondements	O
du	O
"	O
New	O
Labour	O
"	O
.	O
En	O
France	O
,	O
la	O
première	O
nationalisation	O
à	O
caractère	O
économique	O
a	O
lieu	O
en	O
1907	O
.	O
Le	O
secteur	O
de	O
la	O
construction	O
aérienne	O
est	O
également	O
partiellement	O
nationalisé	O
et	O
décentralisé	O
à	O
Toulouse	LOCATION
(	O
1937	O
)	O
.	O
À	O
noter	O
que	O
le	O
programme	O
de	O
nationalisations	O
du	O
Front	O
Populaire	O
était	O
beaucoup	O
plus	O
important	O
que	O
celui	O
effectivement	O
réalisé	O
.	O
La	O
Banque	O
de	O
France	O
n'	O
est	O
pas	O
nationalisée	O
.	O
Les	O
réformes	O
monétaires	O
et	O
les	O
réformes	O
de	O
structure	O
accaparent	O
les	O
gouvernements	O
du	O
Front	O
Populaire	O
entre	O
1936	O
et	O
1938	O
.	O
Le	O
programme	O
du	O
Conseil	O
national	O
de	O
la	O
Résistance	O
(	O
CNR	O
)	O
réclame	O
dès	O
1944	O
le	O
"	O
retour	O
à	O
la	O
nation	O
de	O
tous	O
les	O
grands	O
moyens	O
de	O
productions	O
monopolisées	O
,	O
fruits	O
du	O
travail	O
commun	O
,	O
des	O
sources	O
d'	O
énergie	O
,	O
des	O
richesses	O
du	O
sous-sol	O
,	O
des	O
compagnies	O
d'	O
assurance	O
et	O
des	O
grandes	O
banques	O
"	O
.	O
Le	O
projet	O
de	O
loi	O
a	O
été	O
déposé	O
le	O
vendredi	O
30	O
novembre	O
au	O
soir	O
,	O
après	O
la	O
fermeture	O
de	O
la	O
bourse	O
,	O
pour	O
être	O
voté	O
le	O
2	O
décembre	O
et	O
publié	O
au	O
Journal	O
Officiel	O
dès	O
le	O
lendemain	O
.	O
Ce	O
carcan	O
se	O
desserre	O
progressivement	O
:	O
1968	O
,	O
introduction	O
de	O
la	O
publicité	O
;	O
1969	O
,	O
suppression	O
du	O
ministère	O
de	O
l'	O
information	O
puis	O
décret	O
libéralisant	O
le	O
statut	O
des	O
personnels	O
;	O
1975	O
,	O
éclatement	O
de	O
l'	O
ORTF	O
en	O
sept	O
sociétés	O
;	O
1984	O
,	O
création	O
de	O
Canal+	O
,	O
première	O
chaîne	O
privée	O
.	O
C'	O
est	O
la	O
première	O
fois	O
en	O
France	O
qu'	O
un	O
gouvernement	O
pratique	O
des	O
"	O
dénationalisations	O
"	O
.	O
La	O
réélection	O
du	O
président	O
Mitterrand	O
en	O
1988	O
donne	O
naissance	O
à	O
la	O
politique	O
du	O
"	O
ni-ni	O
"	O
:	O
ni	O
nationalisation	O
,	O
ni	O
privatisation	O
.	O
Le	O
Troisième	O
Reich	O
ne	O
pratiqua	O
pas	O
la	O
nationalisation	O
,	O
pas	O
plus	O
que	O
la	O
privatisation	O
:	O
c'	O
est	O
par	O
d'	O
autres	O
moyens	O
qu'	O
il	O
gardait	O
néanmoins	O
un	O
contrôle	O
direct	O
sur	O
l'	O
économie	O
.	O
Aujourd'hui	O
,	O
la	O
Société	O
des	O
alcools	O
du	O
Québec	O
est	O
considérée	O
comme	O
le	O
plus	O
grand	O
vendeur	O
de	O
vins	O
au	O
Canada	O
et	O
le	O
plus	O
grand	O
acheteur	O
institutionnel	O
de	O
vins	O
au	O
monde	O
.	O
Le	O
14	O
avril	O
1944	O
le	O
gouvernement	O
québécois	O
nationalise	O
les	O
compagnies	O
électriques	O
avec	O
la	O
loi	O
sur	O
la	O
Commission	O
hydroélectrique	O
du	O
Québec	O
et	O
donne	O
naissance	O
à	O
Hydro-Québec	O
.	O
Son	O
siège	O
social	O
est	O
situé	O
à	O
Montréal	LOCATION
.	O
En	O
télécommunications	O
,	O
le	O
gouvernement	O
québécois	O
crée	O
Radio-Québec	O
le	O
22	O
février	O
1968	O
par	O
la	O
mise	O
en	O
vigueur	O
d'	O
une	O
loi	O
votée	O
en	O
1945	O
.	O
En	O
1996	O
,	O
Radio-Québec	O
devient	O
officiellement	O
Télé-Québec	O
,	O
et	O
passe	O
de	O
l'	O
analogique	O
au	O
numérique	O
.	O
