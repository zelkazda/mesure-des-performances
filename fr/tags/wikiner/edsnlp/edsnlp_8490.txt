Stratford-upon-Avon	LOCATION
est	O
une	O
ville	O
du	O
centre	O
de	O
l'	O
Angleterre	O
où	O
William	PERSON
Shakespeare	PERSON
est	O
né	O
en	O
1564	O
.	O
La	O
troupe	O
d'	O
acteurs	O
"	O
Royal	O
Shakespeare	O
Company	O
"	O
est	O
originaire	O
de	O
la	O
ville	O
,	O
et	O
s'	O
y	O
produit	O
encore	O
régulièrement	O
.	O
