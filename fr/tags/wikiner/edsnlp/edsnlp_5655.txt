Ce	O
principe	O
,	O
selon	O
Léopold	O
II	O
"	O
consistait	O
non	O
seulement	O
à	O
acheter	O
le	O
produit	O
des	O
plantations	O
à	O
un	O
prix	O
fixé	O
arbitrairement	O
,	O
mais	O
aussi	O
à	O
mettre	O
en	O
place	O
des	O
fonctionnaires	O
qui	O
obtenaient	O
des	O
primes	O
en	O
fonction	O
de	O
la	O
production	O
"	O
.	O
Le	O
monarque	O
eut	O
sa	O
propre	O
"	O
colonie	O
privée	O
"	O
,	O
l'	O
État	O
indépendant	O
du	O
Congo	O
,	O
sur	O
lequel	O
il	O
exerça	O
sa	O
souveraineté	O
de	O
1884	O
à	O
1908	O
.	O
Cette	O
colonie	O
permit	O
en	O
tout	O
cas	O
au	O
roi	O
de	O
s'	O
enrichir	O
considérablement	O
,	O
mais	O
cet	O
argent	O
fut	O
consacré	O
à	O
construire	O
de	O
nombreux	O
bâtiments	O
et	O
monuments	O
à	O
Bruxelles	LOCATION
,	O
Ostende	LOCATION
ou	O
dans	O
les	O
Ardennes	O
.	O
À	O
la	O
suite	O
d'	O
une	O
campagne	O
internationale	O
menée	O
par	O
les	O
Britanniques	O
,	O
notamment	O
Edmund	O
Dene	PERSON
Morel	PERSON
,	O
dénonçant	O
le	O
traitement	O
brutal	O
des	O
populations	O
locales	O
par	O
les	O
coloniaux	O
,	O
ajoutée	O
au	O
rapport	O
Casement	O
,	O
la	O
position	O
du	O
roi	O
devint	O
intenable	O
.	O
Le	O
gouvernement	O
belge	O
renomma	O
le	O
territoire	O
Congo	O
belge	O
.	O
Léopold	PERSON
est	O
né	O
à	O
Bruxelles	LOCATION
.	O
À	O
un	O
âge	O
jeune	O
,	O
il	O
s'	O
est	O
engagé	O
dans	O
l'	O
armée	O
belge	O
,	O
et	O
à	O
Bruxelles	LOCATION
,	O
le	O
22	O
août	O
1853	O
,	O
il	O
a	O
épousé	O
Marie-Henriette	PERSON
de	PERSON
Habsbourg-Lorraine	PERSON
,	O
archiduchesse	O
d'	O
Autriche	O
,	O
née	O
à	O
Pest	LOCATION
,	LOCATION
Autriche	LOCATION
(	LOCATION
maintenant	LOCATION
Budapest	LOCATION
,	O
Hongrie	O
)	O
le	O
23	O
août	O
1836	O
,	O
et	O
morte	O
à	O
Spa	LOCATION
,	O
Belgique	O
le	O
19	O
septembre	O
1902	O
.	O
Léopold	O
II	O
devient	O
roi	O
en	O
1865	O
,	O
à	O
la	O
mort	O
de	O
son	O
père	O
Léopold	O
I	O
er	O
,	O
et	O
détient	O
jusqu'	O
à	O
présent	O
le	O
record	O
de	O
longévité	O
de	O
la	O
dynastie	O
belge	O
(	O
44	O
ans	O
de	O
règne	O
)	O
.	O
Il	O
agrandit	O
également	O
le	O
domaine	O
royal	O
de	O
Laeken	LOCATION
.	O
Son	O
objectif	O
était	O
que	O
ses	O
biens	O
immobiliers	O
appartiennent	O
à	O
la	O
Belgique	O
,	O
et	O
ne	O
soient	O
pas	O
divisés	O
entre	O
ses	O
trois	O
filles	O
qui	O
avaient	O
épousé	O
des	O
princes	O
étrangers	O
.	O
C'	O
est	O
sous	O
le	O
règne	O
de	O
Léopold	O
II	O
que	O
sont	O
votées	O
d'	O
importantes	O
lois	O
sociales	O
en	O
Belgique	O
:	O
suppression	O
du	O
livret	O
d'	O
ouvrier	O
,	O
droit	O
de	O
former	O
des	O
syndicats	O
,	O
âge	O
d'	O
admission	O
des	O
enfants	O
dans	O
les	O
usines	O
fixé	O
à	O
12	O
ans	O
,	O
interdiction	O
du	O
travail	O
de	O
nuit	O
aux	O
enfants	O
de	O
moins	O
de	O
16	O
ans	O
et	O
du	O
travail	O
souterrain	O
pour	O
les	O
femmes	O
de	O
moins	O
de	O
21	O
ans	O
,	O
réparations	O
pour	O
les	O
accidents	O
de	O
travail	O
,	O
repos	O
dominical	O
,	O
etc	O
.	O
C'	O
est	O
également	O
sous	O
son	O
règne	O
qu'	O
a	O
lieu	O
la	O
première	O
révision	O
de	O
la	O
Constitution	O
.	O
La	O
Belgique	O
est	O
,	O
à	O
cette	O
époque	O
,	O
la	O
neuvième	O
puissance	O
économique	O
du	O
monde	O
et	O
dispose	O
désormais	O
d'	O
une	O
colonie	O
--	O
léguée	O
par	O
Léopold	O
II	O
--	O
qui	O
va	O
lui	O
apporter	O
d'	O
énormes	O
débouchés	O
pendant	O
plusieurs	O
décennies	O
et	O
accroître	O
son	O
prestige	O
sur	O
le	O
plan	O
international	O
.	O
Grâce	O
à	O
ces	O
contrats	O
,	O
ces	O
territoires	O
seraient	O
proclamés	O
"	O
états	O
libres	O
"	O
par	O
l'	O
AIA	O
,	O
qui	O
aurait	O
alors	O
la	O
souveraineté	O
intégrale	O
des	O
territoires	O
colonisés	O
.	O
Des	O
témoignages	O
établissant	O
l'	O
exploitation	O
indigne	O
et	O
les	O
mauvais	O
traitements	O
dont	O
était	O
victime	O
la	O
population	O
indigène	O
,	O
y	O
compris	O
l'	O
esclavage	O
,	O
la	O
malnutrition	O
,	O
et	O
la	O
mutilation	O
,	O
en	O
particulier	O
dans	O
l'	O
industrie	O
du	O
caoutchouc	O
,	O
menèrent	O
à	O
un	O
mouvement	O
international	O
de	O
protestation	O
mené	O
par	O
les	O
Britanniques	O
au	O
début	O
des	O
années	O
1900	O
.	O
Le	O
journaliste	O
et	O
écrivain	O
britannique	O
Edmund	O
Dene	PERSON
Morel	PERSON
est	O
l'	O
un	O
des	O
premiers	O
à	O
alerter	O
l'	O
opinion	O
internationale	O
sur	O
les	O
exactions	O
commises	O
.	O
Ces	O
accusations	O
sont	O
reprises	O
dans	O
le	O
livre	O
Les	O
Fantômes	O
du	O
roi	O
Léopold	O
.	O
