Thèbes	O
est	O
le	O
nom	O
grec	O
de	O
la	O
ville	O
d'	O
Égypte	O
antique	O
Ouaset	O
,	O
appartenant	O
au	O
quatrième	O
nome	O
de	O
Haute-Égypte	O
.	O
D'	O
abord	O
obscure	O
capitale	O
de	O
province	O
,	O
elle	O
prend	O
une	O
importance	O
nationale	O
à	O
partir	O
de	O
la	O
XI	O
e	O
dynastie	O
.	O
Un	O
autre	O
dieu	O
y	O
avait	O
son	O
temple	O
:	O
Montou	O
,	O
divinité	O
guerrière	O
également	O
adorée	O
non	O
loin	O
de	O
Thèbes	LOCATION
.	O
La	O
nécropole	O
thébaine	O
est	O
alors	O
délaissée	O
et	O
jamais	O
plus	O
un	O
pharaon	O
ne	O
se	O
fit	O
ensevelir	O
à	O
Thèbes	LOCATION
.	O
Mais	O
la	O
ville	O
est	O
saccagée	O
à	O
la	O
fin	O
de	O
cette	O
période	O
par	O
les	O
Assyriens	O
,	O
qui	O
emportent	O
à	O
Ninive	LOCATION
les	O
trésors	O
accumulés	O
depuis	O
tant	O
de	O
siècles	O
par	O
les	O
pharaons	O
et	O
dépouillent	O
les	O
sanctuaires	O
des	O
statues	O
divines	O
,	O
les	O
dépossédant	O
ainsi	O
de	O
leur	O
élément	O
vital	O
.	O
