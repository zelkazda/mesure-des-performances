À	O
ce	O
sujet	O
,	O
Claude	PERSON
Tresmontant	PERSON
précise	O
:	O
Tertullien	PERSON
a	O
aussi	O
employé	O
les	O
mots	O
substantia	O
,	O
équivalent	O
du	O
grec	O
ουσια	O
/	O
ousia	O
(	O
"	O
essence	O
"	O
,	O
"	O
substance	O
"	O
,	O
"	O
être	O
"	O
)	O
et	O
persona	O
qui	O
signifie	O
"	O
masque	O
d'	O
acteur	O
"	O
,	O
"	O
rôle	O
"	O
puis	O
"	O
personnalité	O
"	O
et	O
correspond	O
au	O
grec	O
προσωπον	O
/	O
prosôpon	O
.	O
Le	O
mot	O
υποστασις	O
/	O
upostasis	O
,	O
"	O
hypostase	O
"	O
,	O
signifiant	O
"	O
base	O
"	O
,	O
"	O
fondement	O
"	O
puis	O
"	O
matière	O
"	O
,	O
"	O
substance	O
"	O
a	O
été	O
employé	O
au	O
concile	O
de	O
Nicée	LOCATION
indifféremment	O
avec	O
ousia	O
.	O
À	O
la	O
suite	O
de	O
Basile	PERSON
de	PERSON
Césarée	PERSON
,	O
s'	O
imposera	O
la	O
formule	O
:	O
"	O
une	O
seule	O
ousia	O
en	O
trois	O
hypostases	O
"	O
.	O
C'	O
est	O
pourquoi	O
le	O
saint	O
enfant	O
qui	O
naîtra	O
de	O
toi	O
sera	O
appelé	O
Fils	PERSON
de	PERSON
Dieu	PERSON
"	O
.	O
Elle	O
eut	O
Jean-Baptiste	PERSON
,	O
le	O
précurseur	O
,	O
comme	O
principal	O
témoin	O
.	O
(	O
Mt	MISC
28	O
19	O
)	O
.	O
Mais	O
c'	O
est	O
dans	O
Jean	PERSON
qu'	O
on	O
trouve	O
la	O
doctrine	O
trinitaire	O
la	O
plus	O
élaborée	O
,	O
à	O
tel	O
point	O
qu'	O
on	O
a	O
pu	O
qualifier	O
Jean	PERSON
de	O
"	O
théologien	O
"	O
.	O
"	O
En	O
vérité	O
,	O
en	O
vérité	O
,	O
je	O
vous	O
le	O
dis	O
,	O
avant	O
qu'	O
Abraham	PERSON
fût	O
,	O
je	O
suis	O
.	O
"	O
Ex	MISC
3	O
14	O
)	O
.	O
Le	O
même	O
Paul	PERSON
utilise	O
souvent	O
des	O
formules	O
trinitaires	O
(	O
Cf	O
.	O
De	O
même	O
l'	O
épître	MISC
aux	MISC
Hébreux	MISC
développe	O
une	O
christologie	O
déjà	O
fort	O
avancée	O
.	O
Les	O
principales	O
personnalités	O
engagées	O
dans	O
ce	O
débat	O
étaient	O
présentes	O
,	O
dont	O
Arius	PERSON
,	O
Eusèbe	PERSON
de	PERSON
Nicomédie	PERSON
qui	O
lui	O
était	O
favorable	O
,	O
Eusèbe	PERSON
de	PERSON
Césarée	PERSON
,	O
modéré	O
,	O
Alexandre	PERSON
d'	PERSON
Alexandrie	PERSON
(	O
accompagné	O
d'	O
Athanase	PERSON
d'	PERSON
Alexandrie	PERSON
comme	O
secrétaire	O
)	O
qui	O
s'	O
opposait	O
à	O
lui	O
,	O
de	O
même	O
que	O
,	O
de	O
façon	O
intransigeante	O
,	O
Eustathe	PERSON
d'	PERSON
Antioche	PERSON
et	O
Marcel	PERSON
d'	PERSON
Ancyre	PERSON
.	O
Ce	O
fut	O
seulement	O
au	O
concile	O
de	O
Chalcédoine	LOCATION
,	O
quatrième	O
concile	O
œcuménique	O
,	O
en	O
451	O
,	O
que	O
le	O
vocabulaire	O
théologique	O
acquit	O
sa	O
pleine	O
stabilité	O
,	O
au	O
sujet	O
du	O
mystère	O
trinitaire	O
.	O
Le	O
concile	MISC
de	MISC
Francfort	MISC
en	O
794	O
,	O
jugera	O
qu'	O
il	O
n'	O
y	O
a	O
pas	O
équivalence	O
entre	O
les	O
deux	O
expressions	O
.	O
Après	O
Vatican	MISC
II	MISC
,	O
un	O
intérêt	O
nouveau	O
se	O
manifeste	O
concernant	O
la	O
théologie	O
de	O
la	O
période	O
anténicéenne	O
.	O
Dans	O
l'	O
Église	MISC
orthodoxe	MISC
:	O
(	O
à	O
compléter	O
)	O
.	O
Des	O
auteurs	O
protestants	O
font	O
observer	O
que	O
la	O
trinité	O
n'	O
est	O
pas	O
dans	O
le	O
Nouveau	MISC
Testament	MISC
.	O
Les	O
participants	O
au	O
concile	MISC
de	MISC
Nicée	MISC
ne	O
furent	O
pas	O
sur	O
une	O
position	O
unanime	O
.	O
À	O
partir	O
de	O
la	O
Réforme	MISC
,	O
plusieurs	O
courants	O
de	O
pensée	O
antitrinitaires	O
se	O
formèrent	O
.	O
