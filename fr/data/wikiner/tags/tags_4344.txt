La	O
commune	O
fait	O
aussi	O
partie	O
du	O
Parc	LOCATION
naturel	LOCATION
régional	LOCATION
d'	LOCATION
Armorique	LOCATION
dont	O
elle	O
est	O
la	O
commune	O
située	O
à	O
son	O
extrémité	O
orientale	O
.	O
Il	O
redevint	O
curé	O
de	O
Bolazec	LOCATION
jusqu'	O
en	O
septembre	O
1809	O
,	O
date	O
où	O
il	O
fut	O
nommé	O
à	O
Pouldreuzic	LOCATION
.	O
Bolazec	LOCATION
fut	O
alors	O
sans	O
clergé	O
pendant	O
une	O
vingtaine	O
d'	O
années	O
,	O
redevenant	O
une	O
simple	O
annexe	O
de	O
Scrignac	LOCATION
.	O
Aujourd'hui	O
,	O
malgré	O
cet	O
exode	O
rural	O
,	O
l'	O
activité	O
agricole	O
reste	O
importante	O
à	O
Bolazec	LOCATION
avec	O
environ	O
quatorze	O
fermes	O
en	O
activité	O
sur	O
la	O
commune	O
.	O
La	O
modestie	O
du	O
patrimoine	O
bâti	O
est	O
directement	O
liée	O
à	O
la	O
situation	O
économique	O
de	O
Bolazec	LOCATION
,	O
territoire	O
peu	O
prospère	O
marqué	O
jusqu'	O
à	O
la	O
fin	O
du	O
19e	O
siècle	O
par	O
la	O
pauvreté	O
de	O
son	O
sous-sol	O
.	O
