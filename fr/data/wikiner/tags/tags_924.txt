En	O
1607	O
,	O
Galilée	PERSON
construit	O
un	O
thermoscope	O
,	O
l'	O
ancêtre	O
du	O
thermomètre	O
,	O
bien	O
que	O
la	O
paternité	O
de	O
cette	O
invention	O
soit	O
contestée	O
.	O
Cet	O
instrument	O
change	O
la	O
pensée	O
du	O
temps	O
car	O
il	O
permet	O
de	O
prendre	O
la	O
mesure	O
de	O
ce	O
qu'	O
on	O
pensait	O
un	O
des	O
éléments	O
immuables	O
d'	O
Aristote	PERSON
(	O
feu	O
,	O
eau	O
,	O
air	O
et	O
chaleur	O
)	O
.	O
En	O
1644	O
,	O
Evangelista	PERSON
Torricelli	PERSON
,	O
un	O
contemporain	O
de	O
Galilée	PERSON
,	O
créa	O
le	O
premier	O
vide	O
artificiel	O
et	O
utilisa	O
le	O
concept	O
pour	O
créer	O
le	O
premier	O
baromètre	O
.	O
Le	O
tube	O
de	O
Torricelli	PERSON
est	O
un	O
tube	O
de	O
verre	O
qu'	O
on	O
a	O
plongé	O
dans	O
le	O
mercure	O
pour	O
enlever	O
l'	O
air	O
puis	O
qu'	O
on	O
redresse	O
sans	O
le	O
sortir	O
complètement	O
du	O
liquide	O
.	O
Torricelli	PERSON
découvrit	O
avec	O
son	O
invention	O
que	O
la	O
pression	O
de	O
l'	O
atmosphère	O
varie	O
dans	O
le	O
temps	O
.	O
En	O
1648	O
,	O
Blaise	PERSON
Pascal	PERSON
découvre	O
que	O
la	O
pression	O
diminue	O
également	O
avec	O
l'	O
altitude	O
et	O
en	O
déduit	O
qu'	O
il	O
y	O
a	O
un	O
vide	O
au-delà	O
de	O
l'	O
atmosphère	O
.	O
En	O
1667	O
,	O
le	O
scientifique	O
britannique	O
Robert	PERSON
Hooke	PERSON
construit	O
l'	O
anémomètre	O
pour	O
mesurer	O
la	O
vitesse	O
du	O
vent	O
,	O
un	O
instrument	O
essentiel	O
à	O
la	O
navigation	O
.	O
En	O
1686	O
,	O
Edmund	PERSON
Halley	PERSON
cartographie	O
les	O
alizés	O
et	O
en	O
déduit	O
que	O
les	O
changements	O
atmosphériques	O
sont	O
causés	O
par	O
le	O
réchauffement	O
solaire	O
.	O
Benjamin	PERSON
Franklin	PERSON
observe	O
quotidiennement	O
le	O
temps	O
qu'	O
il	O
fait	O
de	O
1743	O
à	O
1784	O
.	O
En	O
1780	O
,	O
Horace-Bénédict	PERSON
de	PERSON
Saussure	PERSON
construit	O
un	O
hygromètre	O
à	O
cheveu	O
pour	O
mesurer	O
l'	O
humidité	O
de	O
l'	O
air	O
.	O
En	O
1806	O
,	O
Francis	PERSON
Beaufort	PERSON
introduit	O
son	O
échelle	O
descriptive	O
des	O
vents	O
destinée	O
aux	O
marins	O
.	O
En	O
1841	O
,	O
l'	O
américain	O
Elias	PERSON
Loomis	PERSON
est	O
le	O
premier	O
à	O
suggérer	O
la	O
présence	O
de	O
fronts	O
pour	O
expliquer	O
la	O
météo	O
mais	O
ce	O
n'	O
est	O
qu'	O
après	O
la	O
Première	MISC
Guerre	MISC
mondiale	MISC
que	O
l'	O
école	O
norvégienne	O
de	O
météorologie	O
développera	O
ce	O
concept	O
.	O
En	O
1849	O
,	O
le	O
Smithsonian	ORGANIZATION
Institution	ORGANIZATION
,	O
sous	O
la	O
direction	O
de	O
Joseph	PERSON
Henry	PERSON
commence	O
à	O
mettre	O
sur	O
pied	O
un	O
réseau	O
de	O
stations	O
météorologiques	O
d'	O
observation	O
aux	O
États-Unis	LOCATION
d'	LOCATION
Amérique	LOCATION
.	O
Les	O
observations	O
seront	O
disséminées	O
rapidement	O
grâce	O
à	O
l'	O
invention	O
en	O
1837	O
par	O
Samuel	PERSON
Morse	PERSON
du	O
télégraphe	O
.	O
Le	O
14	O
novembre	O
1854	O
,	O
une	O
violente	O
tempête	O
provoque	O
le	O
naufrage	O
de	O
41	O
navires	O
français	O
en	O
mer	LOCATION
Noire	LOCATION
,	O
au	O
cours	O
de	O
la	O
guerre	MISC
de	MISC
Crimée	MISC
.	O
Ce	O
réseau	O
regroupe	O
24	O
stations	O
dont	O
13	O
reliées	O
par	O
télégraphe	O
,	O
puis	O
s'	O
étendra	O
à	O
59	O
observatoires	O
répartis	O
sur	O
l'	O
ensemble	O
de	O
l'	O
Europe	LOCATION
en	O
1865	O
.	O
En	O
1860	O
,	O
le	O
vice-amiral	O
Robert	PERSON
FitzRoy	PERSON
utilise	O
le	O
télégraphe	O
pour	O
colliger	O
les	O
données	O
météorologiques	O
quotidiennes	O
venant	O
de	O
toute	O
l'	O
Angleterre	LOCATION
et	O
tracer	O
les	O
premières	O
cartes	O
synoptiques	O
.	O
En	O
utilisant	O
la	O
variation	O
de	O
ces	O
cartes	O
dans	O
le	O
temps	O
,	O
il	O
fait	O
les	O
premières	O
prévisions	O
qu'	O
il	O
commencera	O
à	O
publier	O
dans	O
le	O
journal	O
The	ORGANIZATION
Times	ORGANIZATION
en	O
1860	O
.	O
Le	O
principal	O
promoteur	O
d'	O
échanges	O
internationaux	O
sera	O
l'	O
américain	O
Matthew	PERSON
Fontaine	PERSON
Maury	PERSON
.	O
En	O
1853	O
,	O
une	O
première	O
conférence	O
des	O
représentants	O
de	O
dix	O
pays	O
se	O
réunit	O
à	O
Bruxelles	LOCATION
pour	O
formaliser	O
une	O
entente	O
et	O
normaliser	O
le	O
codage	O
des	O
données	O
météorologique	O
.	O
En	O
1902	O
,	O
après	O
plus	O
de	O
200	O
lâchers	O
de	O
ballons	O
,	O
souvent	O
effectués	O
de	O
nuit	O
pour	O
éviter	O
l'	O
effet	O
de	O
radiation	O
du	O
soleil	O
,	O
Léon	PERSON
Teisserenc	PERSON
de	PERSON
Bort	PERSON
découvrit	O
la	O
tropopause	O
.	O
Richard	PERSON
Aßmann	PERSON
est	O
considéré	O
également	O
comme	O
co-découvreur	O
de	O
la	O
stratosphère	O
car	O
il	O
publia	O
indépendamment	O
la	O
même	O
année	O
ses	O
résultats	O
sur	O
le	O
sujet	O
.	O
En	O
1919	O
,	O
les	O
météorologistes	O
norvégiens	O
,	O
sous	O
la	O
direction	O
de	O
Vilhelm	PERSON
Bjerknes	PERSON
,	O
développent	O
l'	O
idée	O
des	O
masses	O
d'	O
air	O
se	O
rencontrant	O
le	O
long	O
de	O
zones	O
de	O
discontinuité	O
qu'	O
on	O
nomma	O
les	O
fronts	O
(	O
front	O
chaud	O
,	O
front	O
froid	O
et	O
occlusion	O
)	O
.	O
Le	O
groupe	O
comprenait	O
Carl-Gustaf	PERSON
Rossby	PERSON
qui	O
fut	O
le	O
premier	O
à	O
expliquer	O
la	O
circulation	O
atmosphérique	O
à	O
grande	O
échelle	O
en	O
termes	O
de	O
mécanique	O
des	O
fluides	O
,	O
Tor	PERSON
Bergeron	PERSON
qui	O
détermina	O
le	O
mécanisme	O
de	O
formation	O
de	O
la	O
pluie	O
et	O
Jacob	PERSON
Bjerknes	PERSON
.	O
Le	O
développement	O
des	O
ordinateurs	O
à	O
la	O
fin	O
de	O
la	O
Seconde	MISC
Guerre	MISC
mondiale	MISC
et	O
durant	O
les	O
années	O
1950	O
mènera	O
à	O
la	O
formulation	O
de	O
programmes	O
informatiques	O
pour	O
résoudre	O
les	O
équations	O
météorologiques	O
.	O
En	O
1960	O
,	O
TIROS-1	MISC
est	O
le	O
premier	O
satellite	O
météorologique	O
lancé	O
avec	O
succès	O
.	O
La	O
théorie	O
du	O
chaos	O
va	O
être	O
appliquée	O
à	O
l'	O
atmosphère	O
par	O
Edward	PERSON
Lorenz	PERSON
au	O
cours	O
des	O
années	O
1960	O
.	O
L'	O
air	O
est	O
un	O
fluide	O
compressible	O
,	O
formé	O
de	O
différents	O
gaz	O
et	O
se	O
trouvant	O
dans	O
une	O
mince	O
couche	O
à	O
la	O
surface	O
d'	O
un	O
référentiel	O
en	O
rotation	O
(	O
la	O
Terre	LOCATION
)	O
.	O
Elle	O
s'	O
est	O
cependant	O
affirmée	O
depuis	O
la	O
Deuxième	MISC
Guerre	MISC
mondiale	MISC
avec	O
l'	O
entrée	O
en	O
jeu	O
des	O
moyens	O
techniques	O
comme	O
le	O
radar	O
,	O
les	O
communications	O
modernes	O
et	O
le	O
développement	O
des	O
ordinateurs	O
.	O
Sur	O
Terre	LOCATION
,	O
plusieurs	O
régions	O
ont	O
des	O
vents	O
caractéristiques	O
auxquels	O
les	O
populations	O
locales	O
ont	O
données	O
des	O
noms	O
particuliers	O
.	O
