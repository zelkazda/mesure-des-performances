Un	O
exemple	O
de	O
synergie	O
connue	O
est	O
celle	O
qui	O
existe	O
entre	O
Intel	ORGANIZATION
et	O
Microsoft	ORGANIZATION
:	O
des	O
microprocesseurs	O
de	O
plus	O
en	O
plus	O
puissants	O
permettent	O
d'	O
offrir	O
à	O
l'	O
utilisateur	O
des	O
fonctions	O
plus	O
conviviales	O
,	O
mais	O
ces	O
nouvelles	O
fonctions	O
vont	O
à	O
leur	O
tour	O
,	O
à	O
mesure	O
qu'	O
elles	O
sont	O
de	O
plus	O
en	O
plus	O
utilisées	O
,	O
entraîner	O
une	O
demande	O
de	O
microprocesseurs	O
encore	O
plus	O
puissants	O
.	O
Les	O
arminiens	O
sont	O
des	O
adeptes	O
du	O
théologien	O
calviniste	O
hollandais	O
Jacobus	PERSON
Arminius	PERSON
.	O
Richard	PERSON
Buckminster	PERSON
Fuller	PERSON
a	O
proposé	O
de	O
nommer	O
synergie	O
la	O
conjugaison	O
de	O
plusieurs	O
fonctions	O
assurant	O
l'	O
émergence	O
d'	O
une	O
fonction	O
unique	O
distincte	O
.	O
Le	O
néo-darwinisme	O
,	O
ou	O
théorie	O
synthétique	O
a	O
donné	O
lieu	O
à	O
la	O
théorie	O
synergique	O
de	O
l'	O
évolution	O
,	O
élaborée	O
dans	O
les	O
années	O
1960	O
par	O
le	O
généticien	O
Denis	PERSON
Buican	PERSON
,	O
qui	O
admet	O
une	O
sélection	O
multipolaire	O
permettant	O
de	O
rendre	O
compte	O
de	O
tous	O
les	O
phénomènes	O
sélectifs	O
(	O
des	O
virus	O
jusqu'	O
à	O
l'	O
homme	O
)	O
[	O
réf.	O
souhaitée	O
]	O
.	O
