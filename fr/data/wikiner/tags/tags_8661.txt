Les	O
premiers	O
esclaves	O
furent	O
amenés	O
d'	O
Afrique	LOCATION
en	O
1616	O
et	O
la	O
colonisation	O
anglaise	O
officialisée	O
en	O
1684	O
.	O
L'	O
archipel	O
est	O
devenu	O
britannique	O
en	O
1707	O
avec	O
la	O
création	O
de	O
la	O
Grande-Bretagne	LOCATION
.	O
De	O
plus	O
,	O
les	O
droits	O
de	O
douane	O
érigés	O
en	O
1930	O
par	O
les	O
États-Unis	LOCATION
à	O
l'	O
encontre	O
de	O
ses	O
partenaires	O
commerciaux	O
eurent	O
pour	O
effet	O
de	O
réduire	O
considérablement	O
les	O
exportations	O
agricoles	O
des	O
Bermudes	LOCATION
.	O
Pendant	O
la	O
Seconde	MISC
Guerre	MISC
mondiale	MISC
,	O
les	LOCATION
Bermudes	LOCATION
devinrent	O
une	O
importante	O
base	O
militaire	O
du	O
fait	O
de	O
leur	O
position	O
dans	O
l'	O
océan	LOCATION
Atlantique	LOCATION
.	O
En	O
1941	O
,	O
les	O
États-Unis	LOCATION
passèrent	O
un	O
accord	O
avec	O
le	O
Royaume-Uni	LOCATION
.	O
En	O
échange	O
de	O
la	O
cession	O
de	O
destroyers	O
américains	O
,	O
les	O
Britanniques	LOCATION
concédaient	O
aux	O
Américains	LOCATION
,	O
pour	O
une	O
durée	O
de	O
99	O
ans	O
,	O
un	O
terrain	O
dans	O
le	O
but	O
d'	O
y	O
établir	O
des	O
bases	O
militaires	O
aériennes	O
et	O
navales	O
.	O
Les	LOCATION
Bermudes	LOCATION
se	O
composent	O
de	O
123	O
petites	O
îles	O
de	O
corail	O
avec	O
précipitations	O
suffisantes	O
,	O
mais	O
d'	O
aucun	O
fleuve	O
ou	O
lac	O
d'	O
eau	O
douce	O
.	O
Les	LOCATION
Bermudes	LOCATION
baignent	O
dans	O
un	O
climat	O
doux	O
avec	O
des	O
extrêmes	O
peu	O
marqués	O
.	O
Les	LOCATION
Bermudes	LOCATION
sont	O
sous	O
la	O
dépendance	O
du	O
Royaume-Uni	LOCATION
.	O
Les	LOCATION
Bermudes	LOCATION
sont	O
découpées	O
en	O
9	O
paroisses	O
et	O
2	O
municipalités	O
.	O
Les	LOCATION
Bermudes	LOCATION
sont	O
divisées	O
en	O
9	O
paroisses	O
et	O
2	O
municipalités	O
.	O
Les	LOCATION
Bermudes	LOCATION
sont	O
un	O
pavillon	O
de	O
complaisance	O
.	O
Les	LOCATION
Bermudes	LOCATION
sont	O
un	O
paradis	O
fiscal	O
.	O
De	O
nombreux	O
riches	O
Américains	LOCATION
sont	O
des	O
résidents	O
bermudiens	O
.	O
Les	LOCATION
Bermudes	LOCATION
sont	O
surtout	O
connues	O
dans	O
le	O
monde	O
de	O
la	O
finance	O
pour	O
leur	O
système	O
concernant	O
les	O
sociétés	O
d'	O
assurance	O
et	O
de	O
captives	O
de	O
réassurance	O
.	O
Le	O
mot	O
bermuda	O
est	O
dérivé	O
du	O
nom	O
des	O
Bermudes	LOCATION
où	O
les	O
policiers	O
britanniques	O
portaient	O
ce	O
short	O
qui	O
descendait	O
jusqu'	O
aux	O
genoux	O
.	O
Les	LOCATION
Bermudes	LOCATION
ont	O
pour	O
codes	O
:	O
