L'	MISC
Internationale	MISC
est	O
l'	O
un	O
des	O
chants	O
révolutionnaires	O
les	O
plus	O
célèbres	O
au	O
monde	O
.	O
À	O
l'	O
origine	O
,	O
il	O
s'	O
agit	O
d'	O
un	O
poème	O
écrit	O
par	O
le	O
chansonnier	O
,	O
poète	O
et	O
goguettier	O
Eugène	PERSON
Pottier	PERSON
en	O
juin	O
1871	O
,	O
en	O
pleine	O
répression	O
de	O
la	O
Commune	MISC
de	MISC
Paris	MISC
.	O
Il	O
était	O
destiné	O
à	O
être	O
chanté	O
sur	O
l'	O
air	O
de	O
La	MISC
Marseillaise	MISC
.	O
Au	O
nombre	O
des	O
textes	O
ainsi	O
sauvés	O
se	O
trouve	O
l'	MISC
Internationale	MISC
.	O
La	O
musique	O
de	O
L'	MISC
Internationale	MISC
a	O
été	O
composée	O
ultérieurement	O
par	O
Pierre	PERSON
Degeyter	PERSON
,	O
en	O
1888	O
.	O
À	O
partir	O
de	O
1904	O
,	O
L'	MISC
Internationale	MISC
,	O
après	O
avoir	O
été	O
utilisée	O
pour	O
le	O
congrès	O
d'	O
Amsterdam	LOCATION
de	O
la	O
II	ORGANIZATION
e	ORGANIZATION
Internationale	ORGANIZATION
,	O
devient	O
l'	O
hymne	O
des	O
travailleurs	O
révolutionnaires	O
qui	O
veulent	O
que	O
le	O
monde	O
"	O
change	O
de	O
base	O
"	O
,	O
le	O
chant	O
traditionnel	O
le	O
plus	O
célèbre	O
du	O
mouvement	O
ouvrier	O
.	O
L'	MISC
Internationale	MISC
a	O
été	O
traduit	O
dans	O
de	O
nombreuses	O
langues	O
.	O
L'	MISC
Internationale	MISC
est	O
chantée	O
par	O
les	O
socialistes	O
(	O
dans	O
le	O
sens	O
premier	O
du	O
terme	O
)	O
,	O
anarchistes	O
,	O
communistes	O
,	O
mais	O
aussi	O
des	O
partis	O
dits	O
socialistes	O
ou	O
sociaux-démocrates	O
et	O
bien	O
sûr	O
par	O
les	O
syndicats	O
de	O
gauche	O
,	O
ainsi	O
que	O
dans	O
des	O
manifestations	O
populaires	O
.	O
Ce	O
fut	O
même	O
l'	O
hymne	O
de	O
ralliement	O
de	O
la	O
révolte	O
des	O
étudiants	O
et	O
des	O
travailleurs	O
sur	O
la	O
place	MISC
Tian'anmen	MISC
en	O
1989	O
.	O
Il	O
fut	O
l'	O
hymne	O
national	O
de	O
l'	O
URSS	ORGANIZATION
(	O
dans	O
une	O
version	O
la	O
plupart	O
du	O
temps	O
expurgée	O
du	O
cinquième	O
couplet	O
)	O
jusqu'	O
en	O
1944	O
,	O
et	O
est	O
toujours	O
l'	O
hymne	O
de	O
la	O
majorité	O
des	O
organisations	O
socialistes	O
,	O
anarchistes	O
,	O
marxistes	O
ou	O
communistes	O
.	O
Dans	O
de	O
nombreux	O
pays	O
d'	O
Europe	LOCATION
,	O
ce	O
chant	O
a	O
été	O
illégal	O
durant	O
des	O
années	O
du	O
fait	O
de	O
son	O
image	O
communiste	O
et	O
anarchiste	O
et	O
des	O
idées	O
révolutionnaires	O
dont	O
elle	O
faisait	O
l'	O
apologie	O
.	O
Dans	O
le	O
roman	O
de	O
George	PERSON
Orwell	PERSON
La	MISC
Ferme	MISC
des	MISC
animaux	MISC
,	O
critiquant	O
allégoriquement	O
l'	O
URSS	ORGANIZATION
sous	O
couvert	O
de	O
narrer	O
une	O
révolution	O
d'	O
animaux	O
,	O
L'	MISC
Internationale	MISC
est	O
parodiée	O
sous	O
le	O
nom	O
de	O
Beasts	MISC
of	MISC
England	MISC
et	O
la	O
révolution	O
ouvrière	O
spoliée	O
par	O
les	O
bolchéviques	O
,	O
comme	O
la	O
modification	O
des	O
textes	O
révolutionnaires	O
par	O
celui-ci	O
y	O
est	O
également	O
décriée	O
.	O
Comme	O
cela	O
arrive	O
pour	O
beaucoup	O
de	O
chants	O
,	O
nombre	O
de	O
ceux	O
qui	O
l'	O
interprètent	O
dans	O
le	O
cadre	O
de	O
manifestations	O
militantes	O
ne	O
connaissent	O
aujourd'hui	O
de	O
l'	MISC
Internationale	MISC
que	O
le	O
refrain	O
et	O
le	O
premier	O
couplet	O
.	O
On	O
peut	O
écouter	O
l	O
'	O
Internationale	MISC
interprétée	O
entre	O
autres	O
par	O
:	O
Cependant	O
,	O
en	O
France	LOCATION
l'	O
œuvre	O
musicale	O
est	O
protégée	O
jusqu'	O
en	O
octobre	O
2017	O
.	O
