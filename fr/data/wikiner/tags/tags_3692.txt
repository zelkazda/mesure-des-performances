Dans	O
le	O
monde	O
des	O
logiciels	O
libres	O
,	O
il	O
s'	O
agit	O
généralement	O
de	O
LAMP	O
:	O
Linux	MISC
pour	O
le	O
système	O
d'	O
exploitation	O
,	O
Apache	MISC
pour	O
le	O
serveur	O
de	O
pages	O
,	O
PHP	MISC
pour	O
le	O
langage	O
et	O
MySQL	MISC
pour	O
la	O
base	O
de	O
données	O
,	O
le	O
langage	O
Perl	MISC
étant	O
de	O
moins	O
en	O
moins	O
utilisé	O
.	O
Ils	O
sont	O
couramment	O
utilisés	O
sur	O
Linux	MISC
avec	O
leurs	O
propres	O
serveurs	O
de	O
pages	O
et	O
bases	O
de	O
données	O
,	O
lorsqu'	O
ils	O
ne	O
sont	O
pas	O
interfacés	O
avec	O
un	O
système	O
d'	O
information	O
hétérogène	O
.	O
Le	O
format	O
d'	O
échange	O
le	O
plus	O
répandu	O
car	O
le	O
plus	O
performant	O
est	O
le	O
XML	MISC
.	O
D'	O
autres	O
solutions	O
sont	O
offertes	O
par	O
Macromedia	ORGANIZATION
tel	O
que	O
ColdFusion	MISC
.	O
Il	O
permet	O
de	O
créer	O
des	O
applications	O
internet	O
comme	O
un	O
album	O
photo	O
ou	O
un	O
forum	O
de	O
discussion	O
par	O
exemple	O
,	O
mais	O
aussi	O
toute	O
interaction	O
avec	O
une	O
base	O
de	O
données	O
comme	O
le	O
logiciel	O
Geneweb	MISC
basé	O
sur	O
le	O
langage	O
Ocaml	MISC
,	O
son	O
propre	O
serveur	O
de	O
page	O
,	O
et	O
sa	O
propre	O
base	O
de	O
données	O
.	O
