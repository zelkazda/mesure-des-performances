Il	O
a	O
été	O
créé	O
dans	O
sa	O
version	O
moderne	O
sous	O
Napoléon	PERSON
I	PERSON
er	PERSON
par	O
le	O
décret	O
organique	O
du	O
17	O
mars	O
1808	O
et	O
est	O
considéré	O
comme	O
le	O
premier	O
grade	O
universitaire	O
.	O
Après	O
la	O
Révolution	MISC
française	MISC
qui	O
supprime	O
les	O
universités	O
,	O
le	O
baccalauréat	O
a	O
été	O
réorganisé	O
pour	O
les	O
cinq	O
disciplines	O
d'	O
alors	O
(	O
sciences	O
,	O
lettres	O
,	O
droit	O
,	O
médecine	O
,	O
théologie	O
)	O
par	O
Napoléon	PERSON
I	PERSON
er	PERSON
en	O
1808	O
,	O
avec	O
les	O
deux	O
autres	O
grades	O
,	O
la	O
licence	O
et	O
le	O
doctorat	O
.	O
La	O
première	O
femme	O
à	O
passer	O
le	O
baccalauréat	O
est	O
Julie-Victoire	PERSON
Daubié	PERSON
en	O
1861	O
.	O
Le	O
second	O
palier	O
dans	O
la	O
hausse	O
du	O
nombre	O
de	O
bacheliers	O
intervient	O
à	O
partir	O
des	O
années	O
1930	O
,	O
quand	O
le	O
lycée	O
public	O
devient	O
gratuit	O
(	O
il	O
était	O
payant	O
auparavant	O
,	O
sauf	O
pour	O
quelques	O
rares	O
boursiers	O
comme	O
Marcel	PERSON
Pagnol	PERSON
ou	O
Georges	PERSON
Pompidou	PERSON
,	O
par	O
exemple	O
)	O
.	O
Sous	O
le	O
premier	O
septennat	O
de	O
François	PERSON
Mitterrand	PERSON
,	O
les	O
mentions	O
ont	O
été	O
supprimées	O
un	O
temps	O
,	O
puis	O
rétablies	O
.	O
C'	O
est	O
notamment	O
le	O
cas	O
de	O
Pierre	PERSON
Bérégovoy	PERSON
,	O
André	PERSON
Malraux	PERSON
,	O
Georges	PERSON
Marchais	PERSON
,	O
Antoine	PERSON
Pinay	PERSON
,	O
Marcel	PERSON
Bleustein-Blanchet	PERSON
,	O
Alain	PERSON
Delon	PERSON
,	O
Gérard	PERSON
Depardieu	PERSON
,	O
Diam	PERSON
's	O
,	O
Sacha	PERSON
Guitry	PERSON
,	O
Michel	PERSON
Drucker	PERSON
,	O
Christian	PERSON
Estrosi	PERSON
,	O
Georges	PERSON
Brassens	PERSON
,	O
du	O
général	PERSON
Bigeard	PERSON
et	O
de	O
Jean-Pierre	PERSON
Pernaut	PERSON
,	O
Jacques	PERSON
Weber	PERSON
.	O
Par	O
exemple	O
,	O
Jacques	PERSON
Marseille	PERSON
le	O
désigne	O
comme	O
un	O
"	O
monument	O
d'	O
hypocrisie	O
nationale	O
"	O
et	O
un	O
"	O
instrument	O
d'	O
inégalité	O
sociale	O
"	O
.	O
