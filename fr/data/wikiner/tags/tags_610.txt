Henry	PERSON
James	PERSON
est	O
un	O
écrivain	O
américain	O
né	O
à	O
New	LOCATION
York	LOCATION
le	O
15	O
avril	O
1843	O
et	O
mort	O
à	O
Londres	LOCATION
le	O
28	O
février	O
1916	O
.	O
Ayant	O
longtemps	O
vécu	O
en	O
Angleterre	LOCATION
,	O
il	O
devient	O
un	O
sujet	O
britannique	O
peu	O
avant	O
sa	O
mort	O
.	O
Henry	PERSON
James	PERSON
voulait	O
convaincre	O
les	O
écrivains	O
britanniques	O
et	O
américains	O
de	O
présenter	O
leur	O
vision	O
du	O
monde	O
avec	O
la	O
même	O
liberté	O
que	O
les	O
auteurs	O
français	O
.	O
La	O
fortune	O
acquise	O
par	O
son	O
grand-père	O
,	O
émigré	O
irlandais	O
arrivé	O
aux	O
États-Unis	LOCATION
en	O
1789	O
,	O
avait	O
mis	O
la	O
famille	O
à	O
l'	O
abri	O
des	O
servitudes	O
de	O
la	O
vie	O
quotidienne	O
.	O
Son	O
frère	O
aîné	O
,	O
William	PERSON
James	PERSON
,	O
deviendra	O
professeur	O
à	O
Harvard	ORGANIZATION
et	O
se	O
fera	O
connaître	O
pour	O
sa	O
philosophie	O
pragmatiste	O
.	O
À	O
l'	O
âge	O
de	O
19	O
ans	O
,	O
il	O
est	O
brièvement	O
inscrit	O
à	O
la	O
Faculté	ORGANIZATION
de	ORGANIZATION
droit	ORGANIZATION
de	ORGANIZATION
Harvard	ORGANIZATION
,	O
rapidement	O
abandonnée	O
face	O
au	O
désir	O
d'	O
être	O
"	O
tout	O
simplement	O
littéraire	O
"	O
.	O
Après	O
Washington	MISC
Square	MISC
,	O
Portrait	MISC
de	MISC
femme	MISC
est	O
souvent	O
considéré	O
comme	O
une	O
conclusion	O
magistrale	O
de	O
la	O
première	O
manière	O
de	O
l'	O
écrivain	O
.	O
Sa	O
mère	O
décède	O
en	O
janvier	O
1882	O
,	O
alors	O
que	O
Henry	PERSON
James	PERSON
séjourne	O
à	O
Washington	LOCATION
.	O
Il	O
revient	O
à	O
Londres	LOCATION
en	O
mai	O
et	O
effectue	O
un	O
voyage	O
en	O
France	LOCATION
.	O
Il	O
rentre	O
de	O
façon	O
précipitée	O
aux	O
États-Unis	LOCATION
où	O
son	O
père	O
meurt	O
le	O
18	O
décembre	O
,	O
avant	O
son	O
arrivée	O
.	O
Il	O
revient	O
à	O
Londres	LOCATION
au	O
printemps	O
1883	O
.	O
En	O
1886	O
,	O
il	O
publie	O
deux	O
romans	O
,	O
Les	MISC
Bostoniennes	MISC
et	O
La	MISC
Princesse	MISC
Casamassima	MISC
,	O
qui	O
associent	O
à	O
des	O
thèmes	O
politiques	O
et	O
sociaux	O
(	O
féminisme	O
et	O
anarchisme	O
)	O
la	O
recherche	O
d'	O
une	O
identité	O
personnelle	O
.	O
En	O
1891	O
,	O
une	O
version	O
dramatique	O
de	O
L'	MISC
Américain	MISC
rencontre	O
un	O
petit	O
succès	O
en	O
province	O
,	O
mais	O
reçoit	O
un	O
accueil	O
plus	O
mitigé	O
à	O
Londres	LOCATION
.	O
Puis	O
,	O
entre	O
1902	O
et	O
1904	O
,	O
viennent	O
les	O
derniers	O
grands	O
romans	O
:	O
Les	MISC
Ailes	MISC
de	MISC
la	MISC
colombe	MISC
,	O
Les	MISC
Ambassadeurs	MISC
et	O
La	MISC
Coupe	MISC
d'	MISC
or	MISC
.	O
En	O
1903	O
,	O
James	PERSON
a	O
soixante	O
ans	O
et	O
un	O
"	O
mal	O
du	O
pays	O
passionné	O
"	O
l'	O
envahit	O
.	O
Le	O
30	O
août	O
1904	O
,	O
il	O
débarque	O
à	O
New	LOCATION
York	LOCATION
,	O
pour	O
la	O
première	O
fois	O
depuis	O
vingt	O
ans	O
.	O
Il	O
quitte	O
les	O
États-Unis	LOCATION
le	O
5	O
juillet	O
1905	O
,	O
après	O
avoir	O
donné	O
de	O
nombreuses	O
conférences	O
à	O
travers	O
tout	O
le	O
pays	O
.	O
Avant	O
son	O
retour	O
en	O
Angleterre	LOCATION
,	O
il	O
met	O
au	O
point	O
,	O
avec	O
les	O
Éditions	ORGANIZATION
Scribner	ORGANIZATION
,	O
le	O
projet	O
d'	O
une	O
édition	O
définitive	O
de	O
ses	O
écrits	O
,	O
The	MISC
Novels	MISC
and	MISC
Tales	MISC
of	MISC
Henry	MISC
James	MISC
,	MISC
New	MISC
York	MISC
Edition	MISC
,	O
qui	O
comportera	O
,	O
à	O
terme	O
,	O
vingt-six	O
volumes	O
.	O
En	O
1915	O
,	O
déçu	O
par	O
l'	O
attitude	O
des	O
États-Unis	LOCATION
face	O
à	O
la	O
guerre	O
qui	O
fait	O
rage	O
sur	O
le	O
continent	O
,	O
il	O
demande	O
et	O
obtient	O
la	O
nationalité	O
britannique	O
.	O
Henry	PERSON
James	PERSON
nourrit	O
très	O
tôt	O
l'	O
ambition	O
d'	O
une	O
carrière	O
d'	O
homme	O
de	O
lettres	O
.	O
Ses	O
biographes	O
et	O
les	O
critiques	O
littéraires	O
permettent	O
de	O
citer	O
Henrik	PERSON
Ibsen	PERSON
,	O
Nathaniel	PERSON
Hawthorne	PERSON
,	O
Honoré	PERSON
de	PERSON
Balzac	PERSON
,	O
et	O
Ivan	PERSON
Tourgueniev	PERSON
comme	O
ses	O
influences	O
majeures	O
.	O
Il	O
révisa	O
ses	O
grands	O
romans	O
et	O
de	O
nombreux	O
contes	O
et	O
nouvelles	O
et	O
contes	O
pour	O
l'	O
édition	O
d'	O
anthologie	O
de	O
son	O
œuvre	O
de	O
fiction	O
dont	O
les	O
vingt-trois	O
volumes	O
constitue	O
son	O
autobiographie	O
artistique	O
qu'	O
il	O
nomma	O
"	O
The	O
New	MISC
York	MISC
Edition	MISC
"	O
pour	O
réaffirmer	O
les	O
liens	O
qui	O
l'	O
ont	O
toujours	O
uni	O
à	O
sa	O
ville	O
natale	O
.	O
À	O
différents	O
moments	O
de	O
sa	O
carrière	O
,	O
Henry	PERSON
James	PERSON
écrivit	O
des	O
pièces	O
de	O
théâtre	O
,	O
en	O
commençant	O
par	O
des	O
pièces	O
en	O
un	O
acte	O
pour	O
des	O
magazines	O
,	O
entre	O
1869	O
et	O
1871	O
,	O
et	O
l'	O
adaptation	O
dramatique	O
de	O
sa	O
fameuse	O
nouvelle	O
Daisy	MISC
Miller	MISC
en	O
1882	O
.	O
De	O
1890	O
à	O
1892	O
,	O
il	O
se	O
consacre	O
à	O
réussir	O
sur	O
la	O
scène	O
londonienne	O
,	O
écrivant	O
six	O
pièces	O
dont	O
seule	O
l'	O
adaptation	O
de	O
son	O
roman	O
L'	MISC
Américain	MISC
sera	O
produite	O
.	O
Celle-ci	O
fut	O
représentée	O
plusieurs	O
années	O
de	O
suite	O
par	O
une	O
compagnie	O
de	O
répertoire	O
et	O
avec	O
succès	O
à	O
Londres	LOCATION
,	O
sans	O
toutefois	O
s'	O
avérer	O
très	O
lucrative	O
pour	O
son	O
auteur	O
.	O
Henry	PERSON
James	PERSON
ne	O
voulait	O
plus	O
écrire	O
pour	O
le	O
théâtre	O
.	O
Deux	O
d'	O
entre	O
elles	O
étaient	O
en	O
production	O
au	O
moment	O
de	O
la	O
mort	O
d'	O
Édouard	PERSON
VII	PERSON
le	O
6	O
mai	O
1910	O
qui	O
plongea	O
Londres	LOCATION
dans	O
le	O
deuil	O
,	O
entraînant	O
la	O
fermeture	O
des	O
théâtres	O
.	O
Henry	PERSON
James	PERSON
ne	O
s'	O
est	O
jamais	O
marié	O
.	O
Installé	O
à	O
Londres	LOCATION
,	O
il	O
se	O
présentait	O
comme	O
un	O
célibataire	O
endurci	O
et	O
rejetait	O
régulièrement	O
toute	O
suggestion	O
de	O
mariage	O
.	O
À	O
mesure	O
de	O
la	O
mise	O
à	O
jour	O
des	O
archives	O
,	O
dont	O
les	O
journaux	O
intimes	O
de	O
contemporains	O
et	O
des	O
centaines	O
de	O
lettres	O
sentimentales	O
et	O
parfois	O
érotiques	O
écrites	O
par	O
Henry	PERSON
James	PERSON
à	O
des	O
hommes	O
plus	O
jeunes	O
que	O
lui	O
,	O
la	O
figure	O
du	O
célibataire	O
névrosé	O
laisse	O
la	O
place	O
à	O
celle	O
de	O
l'	O
homosexuel	O
honteux	O
.	O
Henry	PERSON
James	PERSON
rencontra	O
le	O
jeune	O
artiste	O
de	O
27	O
ans	O
à	O
Rome	LOCATION
en	O
1899	O
,	O
alors	O
qu'	O
il	O
avait	O
56	O
ans	O
,	O
et	O
lui	O
écrivit	O
des	O
lettres	O
particulièrement	O
enflammées	O
:	O
"	O
Je	O
te	O
tiens	O
,	O
très	O
cher	O
garçon	O
,	O
dans	O
mon	O
amour	O
le	O
plus	O
profond	O
et	O
en	O
espère	O
autant	O
pour	O
moi	O
;	O
dans	O
chaque	O
battement	O
de	O
ton	O
âme	O
"	O
.	O
Au	O
lieu	O
de	O
quoi	O
je	O
ne	O
peux	O
qu'	O
essayer	O
de	O
vivre	O
sans	O
toi,	O
"	O
et	O
ce	O
n'	O
est	O
que	O
dans	O
les	O
lettres	O
à	O
de	O
jeunes	O
hommes	O
que	O
Henry	PERSON
James	PERSON
se	O
déclare	O
leur	O
"	O
amant	O
"	O
.	O
Dans	O
sa	O
correspondance	O
avec	O
Hugh	PERSON
Walpole	PERSON
,	O
il	O
joue	O
sur	O
les	O
mots	O
à	O
propos	O
de	O
leur	O
relation	O
,	O
se	O
voyant	O
lui-même	O
comme	O
un	O
"	O
éléphant	O
"	O
qui	O
"	O
te	O
tripote	O
,	O
de	O
tellement	O
bonne	O
grâce	O
"	O
et	O
enchaîne	O
à	O
propos	O
de	O
"	O
la	O
vieille	O
trompe	O
expressive	O
"	O
de	O
son	O
ami	O
.	O
Ses	O
lettres	O
,	O
discrètement	O
reproduites	O
,	O
à	O
Walter	PERSON
Berry	PERSON
ont	O
longtemps	O
été	O
appréciées	O
pour	O
leur	O
érotisme	O
légèrement	O
voilé	O
.	O
Henry	PERSON
James	PERSON
est	O
l'	O
une	O
des	O
figures	O
majeures	O
de	O
la	O
littérature	O
transatlantique	O
.	O
Henry	PERSON
James	PERSON
explore	O
ces	O
conflits	O
de	O
cultures	O
et	O
de	O
personnalités	O
,	O
dans	O
des	O
récits	O
où	O
les	O
relations	O
personnelles	O
sont	O
entravées	O
par	O
un	O
pouvoir	O
plus	O
ou	O
moins	O
bien	O
exercé	O
.	O
Bien	O
que	O
toute	O
sélection	O
des	O
romans	O
de	O
Henry	PERSON
James	PERSON
repose	O
inévitablement	O
sur	O
une	O
certaine	O
subjectivité	O
,	O
les	O
livres	O
suivants	O
ont	O
fait	O
l'	O
objet	O
d'	O
une	O
attention	O
particulière	O
dans	O
de	O
nombreuses	O
critiques	O
et	O
études	O
.	O
C'	O
est	O
même	O
le	O
principal	O
sujet	O
de	O
L'	MISC
Américain	MISC
(	O
1877	O
)	O
.	O
Henry	PERSON
James	PERSON
écrit	O
ensuite	O
Washington	MISC
Square	MISC
(	O
1880	O
)	O
,	O
une	O
tragicomédie	O
relativement	O
simple	O
qui	O
rend	O
compte	O
du	O
conflit	O
entre	O
une	O
fille	O
,	O
douce	O
,	O
soumise	O
et	O
maladroite	O
,	O
et	O
son	O
père	O
,	O
un	O
brillant	O
manipulateur	O
.	O
Le	O
roman	O
est	O
souvent	O
comparé	O
à	O
l'	O
œuvre	O
de	O
Jane	PERSON
Austen	PERSON
pour	O
la	O
grâce	O
et	O
la	O
limpidité	O
de	O
sa	O
prose	O
,	O
et	O
la	O
description	O
centrée	O
sur	O
les	O
relations	O
familiales	O
.	O
Comme	O
Henry	PERSON
James	PERSON
n'	O
était	O
pas	O
particulièrement	O
enthousiaste	O
au	O
sujet	O
de	O
Jane	PERSON
Austen	PERSON
,	O
il	O
n'	O
a	O
sans	O
doute	O
pas	O
trouvé	O
la	O
comparaison	O
flatteuse	O
.	O
En	O
fait	O
,	O
il	O
n'	O
était	O
pas	O
non	O
plus	O
très	O
satisfait	O
de	O
Washington	MISC
Square	MISC
.	O
En	O
tentant	O
de	O
le	O
relire	O
pour	O
l'	O
inclure	O
dans	O
la	O
New	MISC
York	MISC
Edition	MISC
de	O
sa	O
fiction	O
(	O
1907	O
--	O
09	O
)	O
,	O
il	O
s'	O
aperçut	O
qu'	O
il	O
ne	O
pouvait	O
pas	O
.	O
Avec	O
Portrait	MISC
de	MISC
femme	MISC
(	O
1881	O
)	O
Henry	PERSON
James	PERSON
achève	O
la	O
première	O
phase	O
de	O
sa	O
carrière	O
par	O
une	O
œuvre	O
qui	O
demeure	O
son	O
roman	O
le	O
plus	O
connu	O
.	O
Ce	O
roman	O
est	O
assez	O
unique	O
dans	O
l'	O
œuvre	O
jamesienne	O
,	O
par	O
le	O
sujet	O
traité	O
;	O
mais	O
il	O
est	O
souvent	O
associé	O
aux	O
Bostoniennes	MISC
,	O
qui	O
évoque	O
aussi	O
le	O
milieu	O
politique	O
.	O
Ce	O
livre	O
reflète	O
l'	O
intérêt	O
dévorant	O
de	O
Henry	PERSON
James	PERSON
pour	O
le	O
théâtre	O
,	O
et	O
est	O
souvent	O
considéré	O
comme	O
le	O
dernier	O
récit	O
de	O
la	O
deuxième	O
phase	O
de	O
sa	O
carrière	O
romanesque	O
.	O
Henry	PERSON
James	PERSON
poursuit	O
son	O
approche	O
plus	O
impliquée	O
et	O
plus	O
psychologique	O
de	O
sa	O
fiction	O
avec	O
Ce	MISC
que	MISC
savait	MISC
Maisie	MISC
(	O
1897	O
)	O
,	O
l'	O
histoire	O
de	O
la	O
fille	O
raisonnable	O
de	O
parents	O
divorcés	O
irresponsables	O
.	O
Dans	O
la	O
préface	O
à	O
sa	O
parution	O
dans	O
New	MISC
York	MISC
Edition	MISC
,	O
Henry	PERSON
James	PERSON
place	O
ce	O
livre	O
au	O
sommet	O
de	O
ses	O
réussites	O
,	O
ce	O
qui	O
provoqua	O
quelques	O
remarques	O
désapprobatrices	O
.	O
La	MISC
Coupe	MISC
d'	MISC
or	MISC
(	O
1904	O
)	O
est	O
une	O
étude	O
complexe	O
et	O
intense	O
du	O
mariage	O
et	O
de	O
l'	O
adultère	O
qui	O
termine	O
cette	O
"	O
phase	O
majeure	O
"	O
et	O
essentielle	O
de	O
l'	O
œuvre	O
romanesque	O
de	O
James	PERSON
.	O
Henry	PERSON
James	PERSON
était	O
particulièrement	O
intéressé	O
par	O
ce	O
qu'	O
il	O
appela	O
la	O
"	O
belle	O
et	O
bénie	O
nouvelle	O
"	O
,	O
ou	O
les	O
récits	O
de	O
taille	O
intermédiaire	O
.	O
Les	O
récits	O
suivants	O
sont	O
représentatifs	O
du	O
talent	O
de	O
Henry	PERSON
James	PERSON
dans	O
ce	O
court	O
format	O
de	O
la	O
fiction	O
.	O
Au-delà	O
de	O
son	O
œuvre	O
de	O
fiction	O
,	O
Henry	PERSON
James	PERSON
fut	O
l'	O
un	O
des	O
plus	O
importants	O
critiques	O
littéraires	O
dans	O
l'	O
histoire	O
du	O
roman	O
.	O
