Le	O
Viêt	LOCATION
Nam	LOCATION
est	O
le	O
troisième	O
exportateur	O
mondial	O
de	O
riz	O
.	O
Le	O
Viêt	LOCATION
Nam	LOCATION
est	O
membre	O
de	O
l'	O
Association	ORGANIZATION
des	ORGANIZATION
nations	ORGANIZATION
de	ORGANIZATION
l'	ORGANIZATION
Asie	ORGANIZATION
du	ORGANIZATION
Sud-Est	ORGANIZATION
(	O
ASEAN	ORGANIZATION
)	O
depuis	O
1995	O
,	O
de	O
la	O
Coopération	ORGANIZATION
Économique	ORGANIZATION
Asie	ORGANIZATION
Pacifique	ORGANIZATION
(	O
APEC	ORGANIZATION
)	O
depuis	O
1998	O
,	O
ainsi	O
que	O
de	O
l'	O
Organisation	ORGANIZATION
mondiale	ORGANIZATION
du	ORGANIZATION
commerce	ORGANIZATION
(	O
OMC	ORGANIZATION
)	O
depuis	O
le	O
11	O
janvier	O
2007	O
.	O
Malgré	O
sa	O
particularité	O
de	O
zone	O
frontalière	O
,	O
An	LOCATION
Giang	LOCATION
peut	O
être	O
le	O
modèle	O
de	O
ce	O
schéma	O
de	O
développement	O
économique	O
.	O
La	O
houille	O
et	O
le	O
pétrole	O
soutiennent	O
le	O
développement	O
industriel	O
,	O
à	O
la	O
manière	O
de	O
Taïwan	LOCATION
qui	O
en	O
est	O
dépourvu	O
,	O
comme	O
Hong	LOCATION
Kong	LOCATION
et	O
Singapour	LOCATION
.	O
Entre	O
Marx	PERSON
et	O
Confucius	PERSON
,	O
le	O
Viêt	LOCATION
Nam	LOCATION
a	O
choisi	O
Confucius	PERSON
dans	O
ses	O
guerres	O
d'	O
indépendance	O
et	O
de	O
réunification	O
et	O
dans	O
l'	O
économie	O
politique	O
asiatique	O
.	O
