Les	O
compagnies	O
de	O
mercenaires	O
recrutées	O
durant	O
la	O
guerre	MISC
de	MISC
Cent	MISC
Ans	MISC
,	O
privées	O
d'	O
employeurs	O
pendant	O
les	O
périodes	O
de	O
paix	O
,	O
se	O
regroupaient	O
en	O
bandes	O
appelées	O
grandes	O
compagnies	O
,	O
et	O
vivaient	O
au	O
détriment	O
des	O
populations	O
.	O
Cette	O
compagnie	O
s'	O
est	O
formée	O
après	O
la	O
paix	MISC
de	MISC
Brétigny	MISC
du	O
8	O
mai	O
1360	O
sous	O
les	O
ordres	O
de	O
John	PERSON
Hawkwood	PERSON
.	O
La	O
lutte	O
contre	O
les	O
compagnies	O
est	O
l'	O
un	O
des	O
enjeux	O
majeurs	O
du	O
retour	O
de	O
Jean	PERSON
le	PERSON
Bon	PERSON
.	O
Le	O
routier	O
mercenaire	O
reçoit	O
de	O
fortes	O
rétributions	O
de	O
Philippe	PERSON
le	PERSON
Hardi	PERSON
et	O
de	O
Jean	PERSON
le	PERSON
Bon	PERSON
pour	O
éviter	O
les	O
pillages	O
.	O
Philippe	PERSON
le	PERSON
Hardi	PERSON
doit	O
employer	O
toute	O
sa	O
science	O
de	O
la	O
diplomatie	O
pour	O
calmer	O
la	O
colère	O
du	O
roi	O
Charles	PERSON
V	PERSON
,	O
et	O
il	O
soutiendra	O
d'	O
ailleurs	O
Arnaud	PERSON
de	PERSON
Cervole	PERSON
jusqu'	O
en	O
1366	O
date	O
au	O
cours	O
de	O
laquelle	O
le	O
routier	O
est	O
assassiné	O
par	O
un	O
de	O
ses	O
propres	O
hommes	O
.	O
Dès	O
1364	O
,	O
et	O
l'	O
avènement	O
de	O
Charles	PERSON
V	PERSON
le	O
ton	O
change	O
.	O
Charles	PERSON
V	PERSON
doit	O
faire	O
comprendre	O
que	O
le	O
royaume	O
n'	O
est	O
plus	O
un	O
havre	O
pour	O
les	O
pillards	O
.	O
Ainsi	O
Philippe	PERSON
le	PERSON
Hardi	PERSON
,	O
à	O
la	O
tête	O
de	O
l'	O
une	O
de	O
ces	O
armées	O
,	O
mène	O
campagne	O
contres	O
elles	O
en	O
Normandie	LOCATION
et	O
dans	O
la	O
Beauce	LOCATION
.	O
Cette	O
lutte	O
permet	O
de	O
roder	O
de	O
petites	O
armées	O
formées	O
de	O
volontaires	O
aguerris	O
sous	O
commandement	O
de	O
chefs	O
expérimentés	O
et	O
fidèles	O
(	O
comme	O
Bertrand	PERSON
du	PERSON
Guesclin	PERSON
)	O
.	O
En	O
1365	O
,	O
la	O
fin	O
de	O
la	O
guerre	MISC
de	MISC
succession	MISC
de	MISC
Bretagne	MISC
démobilise	O
de	O
nombreux	O
guerriers	O
bretons	O
.	O
Fin	O
1365	O
,	O
Charles	PERSON
V	PERSON
réussit	O
enfin	O
à	O
se	O
débarrasser	O
d'	O
une	O
bonne	O
partie	O
des	O
grandes	O
compagnies	O
qui	O
ruinent	O
le	O
pays	O
.	O
