En	O
1929	O
,	O
il	O
découvre	O
le	O
magazine	O
américain	O
Amazing	MISC
Stories	MISC
qui	O
lui	O
donne	O
envie	O
d'	O
écrire	O
dans	O
le	O
genre	O
de	O
la	O
science-fiction	O
et	O
de	O
proposer	O
ses	O
textes	O
à	O
différents	O
périodiques	O
.	O
Entre	O
1940	O
et	O
1943	O
,	O
John	PERSON
Wyndham	PERSON
travaille	O
officiellement	O
pour	O
le	O
gouvernement	O
britannique	O
sur	O
le	O
système	O
de	O
censure	O
en	O
temps	O
de	O
guerre	O
.	O
Inspiré	O
par	O
le	O
succès	O
de	O
son	O
frère	O
(	O
qui	O
publia	O
quatre	O
nouvelles	O
avant	O
même	O
que	O
John	PERSON
Wyndham	PERSON
ne	O
se	O
soit	O
fait	O
un	O
nom	O
)	O
il	O
modifia	O
son	O
style	O
pour	O
son	O
roman	O
intitulé	O
Le	MISC
Jour	MISC
des	MISC
Triffides	MISC
.	O
Le	O
roman	O
fut	O
un	O
énorme	O
succès	O
et	O
contribua	O
à	O
établir	O
la	O
réputation	O
de	O
John	PERSON
Wyndham	PERSON
comme	O
auteur	O
de	O
science-fiction	O
.	O
