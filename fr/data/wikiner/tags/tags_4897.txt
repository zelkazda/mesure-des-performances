D'	O
abord	O
étudiés	O
par	O
Enrico	PERSON
Fermi	PERSON
et	O
ses	O
collègues	O
en	O
1934	O
,	O
ils	O
ne	O
furent	O
correctement	O
interprétés	O
que	O
plusieurs	O
années	O
plus	O
tard	O
.	O
Le	O
16	O
janvier	O
1939	O
,	O
Niels	PERSON
Bohr	PERSON
arriva	O
aux	O
États-Unis	LOCATION
pour	O
passer	O
plusieurs	O
mois	O
à	O
l'	O
Université	ORGANIZATION
de	ORGANIZATION
Princeton	ORGANIZATION
,	O
où	O
il	O
avait	O
hâte	O
de	O
discuter	O
de	O
certains	O
problèmes	O
théoriques	O
avec	O
Albert	PERSON
Einstein	PERSON
.	O
Le	O
26	O
janvier	O
1939	O
,	O
se	O
tint	O
une	O
conférence	O
de	O
physique	O
théorique	O
à	O
Washington	LOCATION
DC	LOCATION
,	O
organisée	O
conjointement	O
par	O
l'	O
Université	ORGANIZATION
George	ORGANIZATION
Washington	ORGANIZATION
et	O
la	O
Carnegie	ORGANIZATION
Institution	ORGANIZATION
de	O
Washington	LOCATION
.	O
Avant	O
la	O
fin	O
de	O
la	O
conférence	O
à	O
Washington	LOCATION
,	O
plusieurs	O
autres	O
expériences	O
étaient	O
lancées	O
pour	O
confirmer	O
la	O
thèse	O
de	O
la	O
fission	O
du	O
noyau	O
.	O
