En	O
1890	O
,	O
Édouard	PERSON
Branly	PERSON
,	O
professeur	O
à	O
l'	O
Institut	ORGANIZATION
catholique	ORGANIZATION
de	ORGANIZATION
Paris	ORGANIZATION
,	O
s'	O
intéresse	O
à	O
l'	O
effet	O
des	O
ondes	O
électromagnétiques	O
de	O
Hertz	PERSON
sur	O
les	O
conducteurs	O
divisés	O
.	O
En	O
découvrant	O
ce	O
qu'	O
il	O
appelle	O
la	O
radioconduction	O
,	O
il	O
ouvre	O
la	O
voie	O
au	O
développement	O
de	O
détecteurs	O
d'	O
ondes	O
beaucoup	O
plus	O
sensibles	O
que	O
les	O
boucles	O
de	O
Hertz	PERSON
.	O
D'	O
autres	O
auteurs	O
donneront	O
au	O
tube	O
de	O
limaille	O
de	O
Branly	PERSON
le	O
nom	O
de	O
cohéreur	O
,	O
dispositif	O
que	O
l'	O
on	O
peut	O
considérer	O
comme	O
un	O
interrupteur	O
(	O
imparfait	O
)	O
fonctionnant	O
en	O
tout	O
ou	O
rien	O
sous	O
l'	O
effet	O
d'	O
ondes	O
électromagnétiques	O
transitoires	O
.	O
Le	O
tube	O
à	O
limaille	O
en	O
tant	O
que	O
résistance	O
électrique	O
variable	O
avait	O
déjà	O
été	O
étudié	O
par	O
le	O
physicien	O
italien	O
Calzecchi	PERSON
Onesti	PERSON
vers	O
le	O
milieu	O
des	O
années	O
1880	O
.	O
Mais	O
la	O
grande	O
découverte	O
de	O
Branly	PERSON
a	O
été	O
de	O
constater	O
que	O
la	O
conductibilité	O
du	O
tube	O
à	O
limaille	O
augmentait	O
brusquement	O
lorsqu'	O
une	O
étincelle	O
éclatait	O
à	O
quelques	O
dizaines	O
de	O
centimètres	O
de	O
lui	O
:	O
on	O
disait	O
alors	O
que	O
la	O
limaille	O
était	O
cohérée	O
.	O
Ce	O
détecteur	O
d'	O
ondes	O
hertziennes	O
a	O
permis	O
à	O
Guglielmo	PERSON
Marconi	PERSON
de	O
réaliser	O
des	O
liaisons	O
à	O
grande	O
distance	O
en	O
radiotélégraphie	O
.	O
