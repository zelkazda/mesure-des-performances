En	O
tant	O
qu'	O
espèces	O
,	O
les	O
passériformes	O
sont	O
ceux	O
qui	O
ont	O
le	O
plus	O
de	O
succès	O
parmi	O
les	O
vertébrés	O
:	O
il	O
y	O
en	O
a	O
6193	O
espèces	O
dans	O
la	O
classification	O
de	O
référence	O
(	O
version	O
2.2	O
,	O
2009	O
)	O
du	O
Congrès	MISC
ornithologique	MISC
international	MISC
,	O
soit	O
à	O
peu	O
près	O
le	O
double	O
du	O
nombre	O
d'	O
espèces	O
de	O
l'	O
ordre	O
le	O
plus	O
vaste	O
parmi	O
les	O
mammifères	O
,	O
les	O
rongeurs	O
.	O
Dans	O
le	O
sud	O
de	O
la	LOCATION
France	LOCATION
,	O
des	O
termes	O
issus	O
de	O
cette	O
racine	O
ont	O
longtemps	O
été	O
utilisés	O
en	O
se	O
déformant	O
en	O
passerat	O
ou	O
passeret	O
par	O
exemple	O
.	O
Cependant	O
,	O
ces	O
noms	O
vernaculaires	O
ont	O
finalement	O
été	O
supplantés	O
par	O
les	O
termes	O
originaires	O
du	O
nord	O
de	O
la	LOCATION
France	LOCATION
,	O
termes	O
qui	O
donneront	O
moineau	O
.	O
André	PERSON
Marie	PERSON
Constant	PERSON
Duméril	PERSON
,	O
en	O
1806	O
décomposait	O
l'	O
ordre	O
des	O
passereaux	O
,	O
en	O
sept	O
familles	O
en	O
fonction	O
de	O
la	O
forme	O
du	O
bec	O
.	O
Les	O
oiseaux	O
de	O
cette	O
famille	O
portaient	O
pour	O
nom	O
vernaculaire	O
les	O
termes	O
de	O
moineaux	O
,	O
bruants	O
,	O
étourneaux	O
,	O
loriots	O
,	O
et	O
tous	O
les	O
passereaux	O
de	O
France	LOCATION
.	O
