Avec	O
plus	O
de	O
100	O
millions	O
de	O
locuteurs	O
,	O
l'	O
allemand	O
est	O
la	O
langue	O
la	O
plus	O
parlée	O
au	O
sein	O
de	O
l'	O
Union	ORGANIZATION
européenne	ORGANIZATION
.	O
En	O
1521	O
,	O
Martin	PERSON
Luther	PERSON
traduisit	O
le	O
Nouveau	MISC
Testament	MISC
dans	O
cet	O
allemand	O
standard	O
en	O
développement	O
et	O
en	O
1534	O
,	O
l'	O
Ancien	MISC
Testament	MISC
.	O
Johann	PERSON
Christoph	PERSON
Adelung	PERSON
publia	O
en	O
1781	O
le	O
premier	O
dictionnaire	O
allemand	O
exhaustif	O
,	O
initiative	O
suivie	O
par	O
Jacob	PERSON
et	PERSON
Wilhelm	PERSON
Grimm	PERSON
en	O
1852	O
.	O
La	O
Suisse	LOCATION
n'	O
utilise	O
plus	O
le	O
ß	O
depuis	O
les	O
années	O
1930	O
.	O
En	O
Alsace	LOCATION
,	O
on	O
remplace	O
habituellement	O
les	O
umlauts	O
:	O
K	LOCATION
oe	LOCATION
nigshoffen	LOCATION
,	O
le	O
Haut-K	LOCATION
oe	LOCATION
nigsbourg	LOCATION
(	O
un	O
château	O
fort	O
)	O
…	O
