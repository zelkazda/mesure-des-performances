En	O
Australie	LOCATION
,	O
la	O
loi	O
contre	O
la	O
discrimination	O
raciale	O
1975	O
interdit	O
les	O
discours	O
de	O
haine	O
:	O
"	O
Il	O
est	O
illégal	O
pour	O
une	O
personne	O
d'	O
avoir	O
une	O
action	O
,	O
autrement	O
qu'	O
en	O
privé	O
,	O
qui	O
serait	O
susceptible	O
dans	O
certaines	O
circonstances	O
d'	O
insulter	O
,	O
d'	O
humilier	O
,	O
de	O
blesser	O
ou	O
d'	O
intimider	O
une	O
autre	O
personne	O
ou	O
un	O
groupe	O
de	O
personnes	O
,	O
quand	O
cette	O
action	O
est	O
faite	O
sur	O
la	O
base	O
de	O
la	O
race	O
,	O
de	O
la	O
couleur	O
de	O
peau	O
ou	O
l'	O
origine	O
ethnique	O
d'	O
une	O
autre	O
personne	O
ou	O
d'	O
un	O
groupe	O
de	O
personnes	O
"	O
.	O
Au	O
Canada	LOCATION
,	O
inciter	O
au	O
génocide	O
ou	O
à	O
la	O
haine	O
contre	O
des	O
"	O
groupes	O
identifiables	O
"	O
est	O
un	O
délit	O
dans	O
le	O
code	O
criminel	O
avec	O
emprisonnement	O
de	O
deux	O
à	O
quatorze	O
ans	O
.	O
La	O
Saskatchewan	LOCATION
avait	O
la	O
première	O
législation	O
,	O
en	O
1947	O
,	O
interdisant	O
l'	O
agression	O
sur	O
la	O
base	O
de	O
la	O
race	O
,	O
de	O
la	O
religion	O
,	O
de	O
la	O
couleur	O
de	O
peau	O
,	O
du	O
sexe	O
,	O
de	O
la	O
nationalité	O
,	O
de	O
l'	O
ascendance	O
et	O
du	O
lieu	O
d'	O
origine	O
.	O
Depuis	O
1982	O
,	O
toutes	O
les	O
autres	O
lois	O
du	O
Québec	LOCATION
doivent	O
respecter	O
cette	O
charte	O
qui	O
prévoit	O
par	O
ailleurs	O
,	O
comme	O
son	O
équivalente	O
canadienne	O
,	O
des	O
mesures	O
visant	O
à	O
réduire	O
les	O
discriminations	O
existantes	O
par	O
des	O
programmes	O
d'	O
accès	O
à	O
l'	O
égalité	O
.	O
La	O
France	LOCATION
interdit	O
la	O
publication	O
de	O
propos	O
diffamatoires	O
ou	O
insultants	O
,	O
qui	O
inciterait	O
à	O
la	O
discrimination	O
,	O
à	O
la	O
haine	O
,	O
ou	O
à	O
la	O
violence	O
contre	O
une	O
personne	O
ou	O
un	O
groupe	O
de	O
personnes	O
en	O
raison	O
de	O
leur	O
lieu	O
d'	O
origine	O
,	O
de	O
leur	O
ethnie	O
ou	O
absence	O
d'	O
ethnie	O
,	O
de	O
la	O
nationalité	O
,	O
de	O
la	O
race	O
ou	O
d'	O
une	O
religion	O
spécifique	O
,	O
et	O
ce	O
depuis	O
1881	O
.	O
La	O
loi	O
interdit	O
les	O
déclarations	O
qui	O
justifient	O
ou	O
relativisent	O
les	O
crimes	O
contre	O
l'	O
humanité	O
(	O
comme	O
la	O
négation	O
de	O
la	O
Shoah	MISC
)	O
.	O
En	O
France	LOCATION
,	O
le	O
droit	O
pénal	O
réprime	O
les	O
actes	O
qui	O
sont	O
une	O
manifestation	O
de	O
racisme	O
.	O
Dès	O
1881	O
,	O
la	O
loi	O
sur	O
la	O
liberté	O
de	O
la	O
presse	O
punit	O
la	O
diffamation	O
raciste	O
"	O
d'	O
un	O
emprisonnement	O
de	O
un	O
mois	O
à	O
un	O
an	O
et	O
d'	O
une	O
amende	O
de	O
1.000	O
F	LOCATION
à	O
1.000.000	O
de	O
francs	O
"	O
.	O
L'	O
auteur	O
de	O
paroles	O
,	O
écrits	O
ou	O
images	O
à	O
caractère	O
raciste	O
est	O
punissable	O
lorsque	O
les	O
attaques	O
incriminées	O
s'	O
adressent	O
par	O
tout	O
moyen	O
de	O
communication	O
au	O
public	O
,	O
y	O
compris	O
par	O
internet	O
,	O
quand	O
bien	O
même	O
le	O
site	O
serait	O
basé	O
à	O
l'	O
étranger	O
,	O
à	O
condition	O
que	O
le	O
propos	O
litigieux	O
soit	O
diffusé	O
en	O
France	LOCATION
.	O
Le	O
procureur	LOCATION
de	LOCATION
la	LOCATION
République	LOCATION
peut	O
prendre	O
d'	O
office	O
l'	O
initiative	O
de	O
poursuivre	O
l'	O
auteur	O
de	O
l'	O
infraction	O
raciste	O
,	O
sans	O
intervention	O
préalable	O
de	O
la	O
personne	O
ou	O
du	O
groupe	O
de	O
personnes	O
qui	O
en	O
ont	O
été	O
victimes	O
.	O
En	O
2002	O
,	O
l'	O
écrivain	O
Michel	PERSON
Houellebecq	PERSON
fut	O
jugé	O
non	O
coupable	O
d'	O
incitation	O
au	O
racisme	O
après	O
avoir	O
déclaré	O
,	O
lors	O
d'	O
une	O
interview	O
,	O
que	O
l'	O
islam	O
était	O
la	O
"	O
religion	O
la	O
plus	O
con	O
"	O
.	O
En	O
2007	O
,	O
le	O
tribunal	O
correctionnel	O
de	O
Lyon	LOCATION
condamna	O
Bruno	PERSON
Gollnisch	PERSON
à	O
trois	O
mois	O
de	O
prison	O
avec	O
sursis	O
et	O
à	O
une	O
amende	O
de	O
55	O
000	O
€	O
pour	O
avoir	O
contesté	O
l'	O
existence	O
de	O
crimes	O
contre	O
l'	O
humanité	O
dans	O
une	O
remarque	O
sur	O
la	O
Shoah	MISC
.	O
En	O
2008	O
,	O
Brigitte	PERSON
Bardot	PERSON
est	O
condamnée	O
pour	O
la	O
cinquième	O
fois	O
pour	O
"	O
incitation	O
à	O
la	O
haine	O
raciale	O
"	O
.	O
La	O
Norvège	LOCATION
interdit	O
les	O
discours	O
de	O
haine	O
et	O
les	O
définis	O
comme	O
des	O
"	O
déclarations	O
publiques	O
qui	O
menacent	O
ou	O
ridiculisent	O
quelqu'	O
un	O
ou	O
incitent	O
à	O
la	O
haine	O
,	O
la	O
persécution	O
ou	O
le	O
mépris	O
à	O
raison	O
de	O
la	O
couleur	O
de	O
peau	O
,	O
de	O
l'	O
origine	O
ethnique	O
,	O
de	O
l'	O
homosexualité	O
,	O
des	O
styles	O
de	O
vie	O
ou	O
de	O
l'	O
orientation	O
religieuse	O
ou	O
philosophie	O
"	O
.	O
Au	O
Royaume-Uni	LOCATION
,	O
le	O
"	O
public	O
order	O
act	O
"	O
de	O
1986	O
,	O
dans	O
sa	O
partie	O
3	O
,	O
interdit	O
les	O
expressions	O
de	O
haine	O
raciale	O
.	O
En	O
Serbie	LOCATION
,	O
la	O
constitution	O
garantit	O
la	O
liberté	O
de	O
parole	O
mais	O
elle	O
déclare	O
qu'	O
elle	O
peut	O
être	O
réduite	O
par	O
la	O
loi	O
afin	O
de	O
protéger	O
les	O
droits	O
et	O
la	O
respectabilité	O
d'	O
autrui	O
.	O
Singapour	LOCATION
a	O
passé	O
plusieurs	O
lois	O
interdisant	O
les	O
discours	O
qui	O
font	O
la	O
promotion	O
du	O
conflit	O
dans	O
les	O
groupes	O
religieux	O
.	O
La	O
Suède	LOCATION
interdit	O
les	O
discours	O
de	O
haine	O
(	O
hets	O
mot	O
folkgrupp	O
)	O
et	O
les	O
définis	O
comme	O
des	O
déclarations	O
publiques	O
qui	O
menacent	O
ou	O
expriment	O
un	O
manque	O
de	O
respect	O
pour	O
un	O
groupe	O
ethnique	O
ou	O
un	O
groupe	O
similaire	O
à	O
cause	O
de	O
leur	O
race	O
,	O
couleur	O
de	O
peau	O
,	O
origine	O
ethnique	O
ou	O
nationale	O
,	O
leur	O
foi	O
ou	O
leur	O
orientation	O
sexuelle	O
.	O
En	O
Suisse	LOCATION
,	O
la	O
discrimination	O
ou	O
l'	O
évocation	O
d'	O
une	O
"	O
rancœur	O
"	O
contre	O
des	O
personnes	O
ou	O
des	O
groupes	O
de	O
personnes	O
en	O
raison	O
de	O
leur	O
race	O
,	O
ethnie	O
,	O
sont	O
punies	O
par	O
un	O
emprisonnement	O
d'	O
un	O
maximum	O
de	O
trois	O
ans	O
ou	O
une	O
amende	O
.	O
