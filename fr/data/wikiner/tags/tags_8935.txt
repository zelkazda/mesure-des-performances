Le	O
21	O
avril	O
1526	O
,	O
il	O
participe	O
,	O
aux	O
côtés	O
de	O
son	O
père	O
,	O
à	O
la	O
bataille	MISC
de	MISC
Pânipat	MISC
qui	O
marque	O
le	O
début	O
de	O
l'	O
Empire	LOCATION
moghol	LOCATION
.	O
En	O
1528	O
,	O
il	O
est	O
nommé	O
gouverneur	O
du	O
Badakhshan	LOCATION
.	O
De	O
plus	O
,	O
il	O
est	O
pris	O
en	O
tenaille	O
par	O
deux	O
chefs	O
en	O
pleine	O
ascension	O
,	O
Bahadur	PERSON
Shah	PERSON
au	O
Goujerat	LOCATION
et	O
Sher	PERSON
Shâh	PERSON
Sûrî	PERSON
dans	O
le	O
Bihar	LOCATION
.	O
Il	O
se	O
met	O
en	O
route	O
pour	O
mater	O
les	O
Afghans	LOCATION
du	O
Bihar	LOCATION
qu'	O
il	O
défait	O
en	O
1532	O
,	O
puis	O
,	O
en	O
décembre	O
de	O
cette	O
même	O
année	O
,	O
soumet	O
les	O
troupes	O
de	O
Sher	PERSON
Shah	PERSON
Suri	PERSON
.	O
Entre-temps	O
,	O
Bahadur	PERSON
Shah	PERSON
annexe	O
le	O
Mâlvâ	LOCATION
en	O
1531	O
et	O
s'	O
empare	O
de	O
la	O
forteresse	O
de	O
Chittor	LOCATION
en	O
1535	O
.	O
Profitant	O
des	O
campagnes	O
d'	O
Humâyûn	PERSON
à	O
l'	O
ouest	O
,	O
Sher	PERSON
Shah	PERSON
Suri	PERSON
a	O
renforcé	O
sa	O
position	O
au	O
Bihar	LOCATION
et	O
se	O
lance	O
à	O
la	O
conquête	O
du	O
Bengale	LOCATION
.	O
Humâyûn	PERSON
est	O
obligé	O
de	O
s'	O
enfuir	O
pour	O
Âgrâ	LOCATION
seulement	O
accompagné	O
de	O
quelques	O
fidèles	O
,	O
abandonnant	O
son	O
harem	O
au	O
vainqueur	O
.	O
Humâyûn	PERSON
la	O
reprend	O
l'	O
année	O
suivante	O
.	O
Mais	O
Humâyûn	PERSON
est	O
maintenant	O
prêt	O
à	O
revenir	O
en	O
Inde	LOCATION
pour	O
récupérer	O
son	O
trône	O
.	O
En	O
juillet	O
,	O
Humâyûn	PERSON
entre	O
finalement	O
Delhi	LOCATION
,	O
il	O
a	O
retrouvé	O
son	O
trône	O
après	O
15	O
ans	O
d'	O
exil	O
.	O
