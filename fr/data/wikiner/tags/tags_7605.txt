C'	O
est	O
un	O
affluent	O
du	O
Fier	LOCATION
en	O
rive	O
gauche	O
,	O
donc	O
un	O
sous-affluent	O
du	O
Rhône	LOCATION
.	O
Le	O
Chéran	LOCATION
prend	O
sa	O
source	O
dans	O
le	O
Massif	LOCATION
des	LOCATION
Bauges	LOCATION
,	O
dans	O
la	O
commune	O
de	O
Verrens-Arvey	LOCATION
,	O
en	O
Savoie	LOCATION
.	O
Il	O
se	O
jette	O
dans	O
le	O
Fier	LOCATION
,	O
après	O
un	O
parcours	O
de	O
54	O
km	O
.	O
Il	O
arrose	O
notamment	O
Le	LOCATION
Châtelard	LOCATION
,	O
Alby-sur-Chéran	LOCATION
et	O
Rumilly	LOCATION
.	O
Le	O
bassin	O
versant	O
du	O
Chéran	LOCATION
est	O
en	O
grande	O
partie	O
inclus	O
dans	O
le	O
parc	LOCATION
naturel	LOCATION
régional	LOCATION
du	LOCATION
Massif	LOCATION
des	LOCATION
Bauges	LOCATION
.	O
Le	O
débit	O
du	O
Chéran	LOCATION
a	O
été	O
observé	O
durant	O
une	O
période	O
de	O
59	O
ans	O
(	O
1950-2008	O
)	O
,	O
à	O
Allèves	LOCATION
,	O
localité	O
du	O
département	O
de	O
la	O
Haute-Savoie	LOCATION
située	O
malheureusement	O
à	O
une	O
vingtaine	O
de	O
kilomètres	O
de	O
son	O
confluent	O
avec	O
le	O
Fier	LOCATION
.	O
Le	O
débit	O
moyen	O
interannuel	O
ou	O
module	O
de	O
la	O
rivière	O
à	O
Allèves	LOCATION
est	O
de	O
7,77	O
m³	O
par	O
seconde	O
.	O
Le	O
Chéran	LOCATION
présente	O
des	O
fluctuations	O
saisonnières	O
de	O
débit	O
bien	O
marquées	O
,	O
comme	O
généralement	O
en	O
milieu	O
alpestre	O
.	O
Le	O
débit	O
instantané	O
maximal	O
enregistré	O
à	O
Allèves	LOCATION
a	O
été	O
de	O
250	O
m³	O
par	O
seconde	O
le	O
1er	O
octobre	O
1960	O
,	O
tandis	O
que	O
la	O
valeur	O
journalière	O
maximale	O
était	O
de	O
148	O
m³	O
par	O
seconde	O
le	O
15	O
février	O
1990	O
.	O
Le	O
Chéran	LOCATION
est	O
une	O
rivière	O
très	O
abondante	O
.	O
La	O
lame	O
d'	O
eau	O
écoulée	O
dans	O
son	O
bassin	O
versant	O
est	O
de	O
987	O
millimètres	O
annuellement	O
,	O
ce	O
qui	O
est	O
trois	O
fois	O
supérieur	O
à	O
la	O
moyenne	O
d'	O
ensemble	O
de	O
la	LOCATION
France	LOCATION
tous	O
bassins	O
confondus	O
,	O
mais	O
tout	O
à	O
fait	O
normal	O
comparé	O
aux	O
divers	O
cours	O
d'	O
eau	O
de	O
la	O
région	O
des	O
préalpes	O
de	O
Savoie	LOCATION
,	O
généralement	O
très	O
abondants	O
.	O
C'	O
est	O
de	O
plus	O
nettement	O
supérieur	O
à	O
la	O
moyenne	O
du	O
bassin	O
du	O
Rhône	LOCATION
.	O
Le	O
Chéran	LOCATION
est	O
renommé	O
pour	O
contenir	O
des	O
paillettes	O
d'	O
or	O
.	O
En	O
1867	O
,	O
un	O
gardien	O
de	O
chèvres	O
trouva	O
une	O
pépite	O
de	O
43,50	O
grammes	O
près	O
du	O
vieux	O
pont	O
d'	O
Alby-sur-Chéran	LOCATION
,	O
un	O
endroit	O
dangereux	O
.	O
