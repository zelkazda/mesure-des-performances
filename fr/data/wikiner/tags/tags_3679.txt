Le	O
consortium	O
CODASYL	ORGANIZATION
a	O
été	O
formé	O
dans	O
les	O
années	O
1960	O
en	O
vue	O
de	O
produire	O
des	O
normes	O
et	O
standards	O
en	O
rapport	O
avec	O
les	O
SGBD	O
.	O
Le	O
consortium	O
CODASYL	ORGANIZATION
est	O
à	O
l'	O
origine	O
de	O
diverses	O
normes	O
en	O
rapport	O
avec	O
le	O
langage	O
de	O
programmation	O
COBOL	MISC
,	O
un	O
langage	O
créé	O
en	O
1965	O
,	O
axé	O
sur	O
la	O
manipulation	O
des	O
bases	O
de	O
données	O
.	O
Les	O
premiers	O
SGBD	O
qui	O
manipulent	O
des	O
bases	O
de	O
données	O
relationnelles	O
--	O
IBM	MISC
System	MISC
R	MISC
et	O
Oracle	MISC
V2	MISC
--	O
sont	O
apparus	O
en	O
1978	O
.	O
Un	O
SGBD	O
est	O
un	O
ensemble	O
de	O
logiciels	O
parmi	O
lesquels	O
il	O
y	O
a	O
un	O
moteur	O
de	O
base	O
de	O
données	O
,	O
un	O
interprète	O
du	O
langage	O
SQL	MISC
,	O
une	O
interface	O
de	O
programmation	O
,	O
et	O
diverses	O
interfaces	O
utilisateur	O
.	O
SQL	MISC
est	O
un	O
langage	O
informatique	O
qui	O
sert	O
à	O
exprimer	O
des	O
requêtes	O
d'	O
opérations	O
sur	O
les	O
bases	O
de	O
données	O
.	O
L'	O
interprète	O
SQL	MISC
décode	O
les	O
requêtes	O
,	O
et	O
les	O
transforme	O
en	O
un	O
plan	O
d'	O
exécution	O
détaillé	O
,	O
qui	O
est	O
alors	O
transmis	O
au	O
moteur	O
de	O
base	O
de	O
données	O
.	O
Le	O
détail	O
des	O
demandes	O
est	O
souvent	O
formulé	O
en	O
langage	MISC
SQL	MISC
.	O
ODBC	MISC
est	O
un	O
logiciel	O
médiateur	O
qui	O
permet	O
à	O
des	O
logiciels	O
,	O
par	O
l'	O
intermédiaire	O
d'	O
une	O
interface	O
de	O
programmation	O
unique	O
de	O
communiquer	O
avec	O
différents	O
SGBD	O
ayant	O
chacun	O
une	O
interface	O
de	O
programmation	O
différente	O
.	O
Les	O
trois	O
tenors	O
du	O
marché	O
que	O
sont	O
IBM	ORGANIZATION
DB2	MISC
,	O
Oracle	MISC
et	O
Microsoft	ORGANIZATION
SQL	MISC
Server	MISC
occupent	O
85	O
%	O
du	O
marché	O
.	O
le	O
SGBD	O
MySQL	MISC
a	O
été	O
téléchargé	O
plus	O
de	O
6	O
millions	O
de	O
fois	O
et	O
PostgreSQL	MISC
plus	O
de	O
1	O
million	O
.	O
