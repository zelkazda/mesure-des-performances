Elle	O
correspond	O
pour	O
sa	O
plus	O
grande	O
part	O
à	O
l'	O
Irak	LOCATION
actuel	O
.	O
Elle	O
comprend	O
au	O
nord	O
(	O
nord-est	O
de	O
la	O
Syrie	LOCATION
et	O
le	O
nord	O
de	O
l'	O
Irak	LOCATION
actuels	O
)	O
une	O
région	O
de	O
plateaux	O
,	O
qui	O
est	O
une	O
zone	O
de	O
cultures	O
pluviales	O
,	O
et	O
au	O
sud	O
,	O
une	O
région	O
de	O
plaines	O
où	O
l'	O
on	O
pratique	O
une	O
agriculture	O
qui	O
repose	O
exclusivement	O
sur	O
l'	O
irrigation	O
.	O
Le	O
sens	O
du	O
mot	O
Mésopotamie	LOCATION
a	O
évolué	O
au	O
fil	O
du	O
temps	O
.	O
Chez	O
Arrien	PERSON
,	O
qui	O
écrit	O
une	O
Anabase	MISC
d'	O
Alexandre	PERSON
le	PERSON
Grand	PERSON
,	O
on	O
trouve	O
pour	O
la	O
première	O
fois	O
le	O
terme	O
de	O
Mésopotamie	LOCATION
.	O
Actuellement	O
,	O
le	O
terme	O
"	O
Mésopotamie	LOCATION
"	O
est	O
généralement	O
utilisé	O
en	O
référence	O
à	O
l'	O
histoire	O
antique	O
de	O
cette	O
région	O
,	O
pour	O
la	O
civilisation	O
ayant	O
occupé	O
cet	O
espace	O
jusqu'	O
aux	O
derniers	O
siècles	O
avant	O
l'	O
ère	O
chrétienne	O
ou	O
au	O
sixième	O
siècle	O
avant	O
l'	O
ère	O
musulmane	O
.	O
La	O
notion	O
essentielle	O
est	O
celle	O
de	O
Croissant	LOCATION
fertile	LOCATION
.	O
On	O
y	O
inclut	O
la	O
région	O
qui	O
se	O
situe	O
au	O
sud	O
,	O
entre	O
les	O
fleuves	O
du	O
Tigre	LOCATION
et	O
de	O
l'	O
Euphrate	LOCATION
(	O
en	O
Irak	LOCATION
actuel	O
)	O
.	O
Le	O
terme	O
d	O
'	O
Assyrie	LOCATION
est	O
très	O
couramment	O
employé	O
pour	O
désigner	O
le	O
nord	O
de	O
la	O
Mésopotamie	LOCATION
.	O
Parallèlement	O
,	O
le	O
terme	O
de	O
Babylonie	MISC
désigne	O
le	O
sud	O
de	O
la	O
Mésopotamie	LOCATION
,	O
c'	O
est-à-dire	O
la	O
plaine	O
mésopotamienne	O
.	O
Le	O
nord	O
de	O
la	O
Mésopotamie	LOCATION
est	O
un	O
vaste	O
plateau	O
désertique	O
,	O
tandis	O
que	O
le	O
sud	O
est	O
une	O
immense	O
plaine	O
alluviale	O
très	O
fertile	O
où	O
,	O
de	O
plus	O
,	O
la	O
présence	O
de	O
nombreux	O
bras	O
de	O
fleuve	O
et	O
de	O
marécages	O
permettait	O
l'	O
irrigation	O
.	O
La	O
période	O
historique	O
commence	O
en	O
Mésopotamie	LOCATION
vers	O
3400	O
,	O
quand	O
l'	O
écriture	O
est	O
mise	O
au	O
point	O
.	O
A	O
noter	O
:	O
un	O
intermède	O
romain	O
avec	O
les	O
conquêtes	O
de	O
Trajan	PERSON
(	O
116	O
ap	O
.	O
Son	O
successeur	O
,	O
Hadrien	PERSON
,	O
abandonne	O
ces	O
territoires	O
dès	O
son	O
avènement	O
(	O
117	O
)	O
.	O
Ils	O
s'	O
établissent	O
dans	O
toute	O
la	O
Mésopotamie	LOCATION
,	O
et	O
finissent	O
par	O
en	O
devenir	O
une	O
composante	O
majeure	O
.	O
Plus	O
tard	O
c'	O
est	O
un	O
autre	O
peuple	O
iranien	O
,	O
les	O
Parthes	LOCATION
,	O
qui	O
s'	O
établissent	O
en	O
Mésopotamie	LOCATION
.	O
La	O
Mésopotamie	LOCATION
a	O
vu	O
l'	O
élaboration	O
de	O
ce	O
qui	O
est	O
actuellement	O
considéré	O
comme	O
le	O
plus	O
ancien	O
système	O
d'	O
écriture	O
au	O
monde	O
.	O
On	O
écrit	O
alors	O
essentiellement	O
sur	O
des	O
tablettes	O
faites	O
en	O
argile	O
,	O
matériau	O
abondant	O
en	O
Mésopotamie	LOCATION
.	O
Ce	O
support	O
survit	O
très	O
bien	O
à	O
l'	O
épreuve	O
du	O
temps	O
(	O
et	O
encore	O
plus	O
quand	O
il	O
est	O
cuit	O
à	O
la	O
suite	O
d'	O
un	O
incendie	O
)	O
,	O
et	O
c'	O
est	O
ce	O
qui	O
nous	O
permet	O
d'	O
avoir	O
une	O
quantité	O
de	O
documentation	O
écrite	O
considérable	O
sur	O
la	O
Mésopotamie	LOCATION
ancienne	O
.	O
On	O
pouvait	O
faire	O
des	O
classements	O
d'	O
archives	O
administratives	O
,	O
mais	O
aussi	O
de	O
production	O
littéraire	O
savante	O
,	O
comme	O
dans	O
le	O
cas	O
de	O
la	O
prétendue	O
Bibliothèque	MISC
d'	MISC
Assurbanipal	MISC
,	O
trouvée	O
à	O
Ninive	LOCATION
.	O
Ils	O
connaissaient	O
cinq	O
planètes	O
:	O
Mercure	LOCATION
,	O
Vénus	LOCATION
,	O
Mars	LOCATION
,	O
Jupiter	LOCATION
et	O
Saturne	LOCATION
.	O
Celui-ci	O
,	O
avec	O
les	O
autres	O
textes	O
provenant	O
de	O
Mésopotamie	LOCATION
et	O
qui	O
lui	O
sont	O
apparentés	O
,	O
ne	O
représentent	O
qu'	O
une	O
petite	O
partie	O
des	O
sources	O
nous	O
informant	O
sur	O
le	O
droit	O
dans	O
cette	O
région	O
.	O
Parmi	O
les	O
principaux	O
domaines	O
artistiques	O
attestés	O
en	O
Mésopotamie	LOCATION
,	O
on	O
peut	O
relever	O
:	O
La	O
matière	O
de	O
base	O
utilisée	O
pour	O
réaliser	O
des	O
bâtiments	O
en	O
Mésopotamie	LOCATION
est	O
l'	O
argile	O
.	O
L'	O
archéologie	O
de	O
la	O
Mésopotamie	LOCATION
a	O
porté	O
uniquement	O
sur	O
des	O
centres	O
urbains	O
,	O
et	O
jamais	O
sur	O
des	O
sites	O
ruraux	O
(	O
en	O
dehors	O
de	O
la	O
période	O
préurbaine	O
)	O
.	O
L'	O
agriculture	O
est	O
la	O
base	O
des	O
économies	O
de	O
type	O
pré-industriel	O
,	O
et	O
la	O
Mésopotamie	LOCATION
antique	O
ne	O
déroge	O
pas	O
à	O
la	O
règle	O
.	O
La	O
céréaliculture	O
dominait	O
en	O
Mésopotamie	LOCATION
.	O
La	O
productivité	O
céréalière	O
de	O
la	O
Mésopotamie	LOCATION
a	O
pu	O
atteindre	O
des	O
rendements	O
impressionnants	O
,	O
surtout	O
quand	O
une	O
longue	O
période	O
de	O
stabilité	O
politique	O
a	O
permis	O
une	O
bonne	O
mise	O
en	O
valeur	O
des	O
terres	O
.	O
La	O
plupart	O
des	O
domaines	O
artisanaux	O
sont	O
représentés	O
en	O
Mésopotamie	LOCATION
:	O
textile	O
,	O
menuiserie	O
,	O
métallurgie	O
,	O
orfèvrerie	O
,	O
vannerie	O
,	O
etc	O
.	O
Parce	O
qu'	O
elle	O
a	O
été	O
la	O
première	O
région	O
du	O
Proche-Orient	LOCATION
ancien	LOCATION
à	O
être	O
bien	O
fouillée	O
,	O
la	O
Mésopotamie	LOCATION
a	O
longtemps	O
été	O
considérée	O
comme	O
le	O
"	O
centre	O
"	O
de	O
celui-ci	O
,	O
le	O
reste	O
étant	O
relégué	O
au	O
rang	O
de	O
"	O
périphérie	O
"	O
.	O
Mais	O
on	O
a	O
depuis	O
mis	O
au	O
jour	O
de	O
nouveaux	O
centres	O
qui	O
ont	O
montré	O
que	O
les	O
régions	O
considérées	O
comme	O
marginales	O
étaient	O
très	O
avancées	O
dès	O
une	O
époque	O
reculée	O
(	O
notamment	O
grâce	O
aux	O
archives	O
d'	O
Ebla	LOCATION
et	O
de	O
Mari	LOCATION
en	O
Syrie	LOCATION
,	O
et	O
aujourd'hui	O
de	O
Jiroft	LOCATION
en	O
Iran	LOCATION
)	O
,	O
et	O
n'	O
avaient	O
pas	O
grand-chose	O
à	O
envier	O
à	O
la	O
Mésopotamie	LOCATION
contemporaines	O
.	O
La	O
ressemblance	O
entre	O
la	O
civilisation	O
mésopotamienne	O
et	O
ses	O
voisines	O
peut	O
s'	O
expliquer	O
par	O
le	O
fait	O
qu'	O
elles	O
constituent	O
un	O
territoire	O
ayant	O
partagé	O
une	O
destinée	O
commune	O
depuis	O
la	O
période	O
néolithique	O
,	O
phase	O
que	O
la	O
Mésopotamie	LOCATION
est	O
la	O
dernière	O
à	O
avoir	O
expérimenté	O
.	O
Cela	O
débute	O
avec	O
la	O
période	LOCATION
d'	LOCATION
Uruk	LOCATION
,	O
qui	O
voit	O
une	O
expansion	O
des	O
habitants	O
du	O
futur	O
pays	O
de	O
Sumer	LOCATION
dans	O
les	O
régions	O
voisines	O
.	O
La	O
culture	O
élaborée	O
par	O
les	O
Sumériens	LOCATION
,	O
puis	O
les	O
Akkadiens	LOCATION
a	O
un	O
rayonnement	O
considérable	O
.	O
