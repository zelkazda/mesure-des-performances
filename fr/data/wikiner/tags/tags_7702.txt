:	O
la	O
ligne	LOCATION
Bourg	LOCATION
Saint	LOCATION
Maurice	LOCATION
--	LOCATION
Albertville	LOCATION
)	O
,	O
la	O
vitesse	O
sur	O
les	O
portions	O
de	O
ligne	O
équipée	O
de	O
telle	O
caténaire	O
est	O
inférieure	O
à	O
100	O
km/h	O
.	O
Si	O
le	O
TGV	MISC
atteint	O
cette	O
vitesse	O
,	O
le	O
pantographe	O
rattrape	O
l'	O
onde	O
.	O
C'	O
est	O
l'	O
une	O
des	O
causes	O
à	O
la	O
limitation	O
de	O
vitesse	O
des	O
trains	O
de	O
type	O
TGV	MISC
.	O
Dans	O
les	O
sols	O
très	O
rocailleux	O
(	O
comme	O
dans	O
le	O
massif	LOCATION
des	LOCATION
Maures	LOCATION
par	O
exemple	O
)	O
les	O
prises	O
de	O
terre	O
sont	O
très	O
profondes	O
(	O
4	O
mètres	O
minimum	O
)	O
.	O
Par	O
exemple	O
le	O
chemin	LOCATION
de	LOCATION
fer	LOCATION
de	LOCATION
la	LOCATION
Jungfrau	LOCATION
utilise	O
des	O
fils	O
de	O
contact	O
aériens	O
pour	O
deux	O
phases	O
et	O
les	O
rails	O
pour	O
la	O
troisième	O
phase	O
.	O
Ce	O
système	O
,	O
principalement	O
destiné	O
aux	O
tunnels	O
,	O
peut	O
être	O
observé	O
sur	O
la	O
ligne	LOCATION
C	LOCATION
du	LOCATION
RER	LOCATION
entre	O
les	O
stations	O
d'	O
Austerlitz	LOCATION
et	O
de	O
Champ	LOCATION
de	LOCATION
Mars	LOCATION
--	LOCATION
Tour	LOCATION
Eiffel	LOCATION
,	O
où	O
il	O
est	O
en	O
service	O
depuis	O
1997	O
.	O
