Il	O
est	O
le	O
deuxième	O
dieu	O
de	O
la	O
trimourti	O
(	O
également	O
appelée	O
la	O
"	O
trinité	O
hindoue	O
"	O
)	O
,	O
avec	O
Brahma	PERSON
et	O
Shiva	PERSON
.	O
Vishnou	PERSON
est	O
souvent	O
dépeint	O
comme	O
étant	O
assis	O
ou	O
se	O
reposant	O
sur	O
un	O
lotus	O
.	O
Vishnu	PERSON
la	O
sauva	O
en	O
effet	O
des	O
eaux	O
sous	O
son	O
avatar	O
de	O
Varâha	PERSON
.	O
La	O
mitre	O
dont	O
il	O
est	O
coiffé	O
confirme	O
cette	O
fonction	O
royale	O
et	O
démontre	O
que	O
dans	O
l'	O
hindouisme	O
post-védique	O
,	O
même	O
si	O
Indra	PERSON
possède	O
toujours	O
le	O
titre	O
de	O
roi	O
des	O
dieux	O
,	O
c'	O
est	O
de	O
fait	O
Vishnu	PERSON
qui	O
opère	O
vraiment	O
cette	O
fonction	O
.	O
Cette	O
prééminence	O
est	O
d'	O
ailleurs	O
confirmée	O
par	O
de	O
nombreux	O
mythes	O
où	O
Vishnu	PERSON
ou	O
un	O
de	O
ses	O
avatars	O
humilie	O
Indra	PERSON
.	O
Vishnou	PERSON
adopte	O
dix	O
formes	O
différentes	O
pour	O
sauver	O
des	O
personnes	O
de	O
la	O
mort	O
(	O
dans	O
l'	O
eau	O
par	O
exemple	O
,	O
il	O
choisit	O
kurma	O
la	O
tortue	O
)	O
Chaitanya	PERSON
est	O
également	O
considéré	O
comme	O
avatar	O
de	O
Vishnu	PERSON
en	O
tant	O
qu'	O
incarnation	O
de	O
Krishna	PERSON
.	O
On	O
ne	O
connaît	O
pas	O
clairement	O
quand	O
et	O
comment	O
le	O
culte	O
de	O
Vishnu	PERSON
a	O
commencé	O
.	O
Dans	O
les	O
Vedas	MISC
,	O
Vishnu	PERSON
est	O
énuméré	O
en	O
tant	O
que	O
dieu	O
mineur	O
,	O
fortement	O
associé	O
à	O
Indra	PERSON
.	O
Vishnu	PERSON
est	O
le	O
dieu	O
principal	O
du	O
vaishnava	O
.	O
