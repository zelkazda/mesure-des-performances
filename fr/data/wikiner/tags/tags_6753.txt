Oxford	LOCATION
(	O
prononciation	O
:	O
ɒksfɚd	O
)	O
est	O
une	O
ville	O
d'	O
Angleterre	LOCATION
et	O
le	O
centre	O
administratif	O
du	O
comté	O
de	O
l'	O
Oxfordshire	LOCATION
dans	O
l'	O
Angleterre	LOCATION
du	LOCATION
Sud-Est	LOCATION
.	O
L'	O
Université	ORGANIZATION
d'	ORGANIZATION
Oxford	ORGANIZATION
est	O
la	O
plus	O
ancienne	O
université	O
du	O
monde	O
anglo-saxon	O
.	O
La	O
rivière	O
Cherwell	LOCATION
et	O
la	O
Tamise	LOCATION
passent	O
par	O
Oxford	LOCATION
,	O
au	O
sud	O
du	O
centre	O
ville	O
.	O
Oxford	LOCATION
est	O
connu	O
comme	O
"	O
city	O
of	O
dreaming	O
spires	O
"	O
(	O
la	O
ville	O
à	O
la	O
silhouette	O
de	O
rêve	O
)	O
,	O
terme	O
inventé	O
par	O
le	O
poète	O
Matthew	PERSON
Arnold	PERSON
en	O
référence	O
à	O
l'	O
architecture	O
harmonieuse	O
des	O
bâtiments	O
de	O
l'	O
université	ORGANIZATION
d'	ORGANIZATION
Oxford	ORGANIZATION
.	O
En	O
1191	O
,	O
une	O
charte	O
de	O
la	O
ville	O
est	O
rédigée	O
en	O
Latin	MISC
.	O
Les	O
religieux	O
de	O
différents	O
ordres	O
ont	O
des	O
maisons	O
à	O
Oxford	LOCATION
,	O
d'	O
importance	O
variable	O
.	O
Les	O
Provisions	MISC
d'	MISC
Oxford	MISC
y	O
sont	O
installées	O
par	O
un	O
groupe	O
de	O
barons	O
guidés	O
par	O
Simon	PERSON
de	PERSON
Montfort	PERSON
,	O
leurs	O
documents	O
sont	O
souvent	O
considérés	O
comme	O
la	O
première	O
constitution	O
écrite	O
de	O
l'	O
Angleterre	LOCATION
.	O
L'	O
Université	ORGANIZATION
d'	ORGANIZATION
Oxford	ORGANIZATION
est	O
mentionné	O
pour	O
la	O
première	O
fois	O
dans	O
les	O
archives	O
du	O
12ème	O
siècle	O
.	O
Ces	O
collèges	O
sont	O
créés	O
à	O
un	O
moment	O
où	O
les	O
Européens	LOCATION
commencent	O
à	O
traduire	O
les	O
écrits	O
des	O
philosophes	O
grecs	O
.	O
L'	O
épidémie	O
de	O
suette	O
en	O
1517	O
est	O
particulièrement	O
dévastatrice	O
à	O
Oxford	LOCATION
et	O
à	O
Cambridge	LOCATION
,	O
où	O
elle	O
tue	O
la	O
moitié	O
des	O
populations	O
des	O
deux	O
villes	O
,	O
dont	O
de	O
nombreux	O
étudiants	O
et	O
professeurs	O
La	O
cathédrale	LOCATION
Christ	LOCATION
Church	LOCATION
d'	LOCATION
Oxford	LOCATION
est	O
un	O
exemple	O
unique	O
de	O
chapelle	O
d'	O
un	O
collège	O
et	O
de	O
cathédrale	O
.	O
Elle	O
est	O
inaugurée	O
par	O
le	O
futur	O
roi	O
Édouard	PERSON
VII	PERSON
le	O
12	O
mai	O
1897	O
.	O
Au	O
début	O
du	O
20ème	O
siècle	O
,	O
Oxford	LOCATION
connait	O
une	O
industrialisation	O
et	O
une	O
croissance	O
démographique	O
rapides	O
.	O
1194	O
)	O
par	O
l'	O
impératrice	O
Mathilde	PERSON
.	O
Oxford	LOCATION
est	O
située	O
dans	O
le	O
comté	O
de	O
l'	O
Oxfordshire	LOCATION
dont	O
elle	O
est	O
le	O
centre	O
administratif	O
.	O
La	O
ville	O
est	O
à	O
80	O
km	O
au	O
nord-ouest	O
de	O
Londres	LOCATION
.	O
La	O
rivière	O
Cherwell	LOCATION
et	O
la	O
Tamise	LOCATION
traversent	O
Oxford	LOCATION
et	O
se	O
rejoignent	O
au	O
sud	O
de	O
la	O
ville	O
.	O
Oxford	LOCATION
a	O
un	O
climat	O
maritime	O
tempéré	O
.	O
Les	O
précipitations	O
sont	O
uniformément	O
réparties	O
dans	O
l'	O
année	O
et	O
sont	O
fournies	O
principalement	O
par	O
les	O
systèmes	O
météorologiques	O
qui	O
arrivent	O
de	O
l'	O
Océan	LOCATION
Atlantique	LOCATION
.	O
Centre	LOCATION
de	O
services	O
régional	O
,	O
Oxford	LOCATION
possède	O
d'	O
importantes	O
fonctions	O
commerciales	O
et	O
touristiques	O
.	O
En	O
dépit	O
de	O
son	O
potentiel	O
scientifique	O
,	O
Oxford	LOCATION
n'	O
a	O
pas	O
aussi	O
bien	O
réussi	O
que	O
Cambridge	LOCATION
à	O
développer	O
une	O
technopole	O
.	O
Elle	O
est	O
devenue	O
depuis	O
une	O
chaine	O
nationale	O
de	O
distribution	O
de	O
livres	O
comptant	O
60	O
librairies	O
au	O
Royaume	LOCATION
Uni	LOCATION
et	O
devenant	O
la	O
première	O
librairie	O
sur	O
internet	O
en	O
1995	O
.	O
La	O
société	O
appartient	O
toujours	O
à	O
la	O
famille	O
Blackwell	ORGANIZATION
.	O
Désormais	O
,	O
le	O
site	O
d'	O
oxford	O
produit	O
3	O
500	O
Mini	MISC
par	O
semaine	O
pour	O
BMW	ORGANIZATION
et	O
emploie	O
4	O
500	O
personnes	O
.	O
Oxford	LOCATION
a	O
un	O
statut	O
de	O
cité	O
depuis	O
1542	O
.	O
Le	O
projet	O
,	O
intitulé	O
"	O
transformer	O
Oxford	LOCATION
"	O
,	O
est	O
seulement	O
,	O
à	O
ce	O
stade	O
,	O
un	O
plan	O
directeur	O
pour	O
la	O
consultation	O
publique	O
,	O
mais	O
les	O
responsables	O
de	O
conseil	O
sont	O
convaincus	O
de	O
son	O
avenir	O
.	O
Les	O
deux	O
compagnies	O
exploitent	O
également	O
des	O
services	O
réguliers	O
à	O
destination	O
de	O
Londres	LOCATION
.	O
Oxford	LOCATION
comporte	O
5	O
espace	O
de	O
stationnement	O
pour	O
automobiles	O
,	O
situé	O
en	O
périphérie	O
de	O
la	O
ville	O
;	O
Deux	O
universités	O
cohabitent	O
aujourd'hui	O
à	O
Oxford	LOCATION
,	O
l'	O
université	ORGANIZATION
d'	ORGANIZATION
Oxford	ORGANIZATION
et	O
l'	O
Oxford	ORGANIZATION
Brookes	ORGANIZATION
University	ORGANIZATION
.	O
L'	O
université	ORGANIZATION
d'	ORGANIZATION
Oxford	ORGANIZATION
est	O
la	O
plus	O
ancienne	O
du	O
pays	O
et	O
l'	O
une	O
des	O
plus	O
prestigieuses	O
d'	O
Europe	LOCATION
.	O
Aujourd'hui	O
,	O
les	O
enseignements	O
à	O
l'	O
université	ORGANIZATION
d'	ORGANIZATION
Oxford	ORGANIZATION
sont	O
divisés	O
en	O
quatre	O
branches	O
principales	O
:	O
Elle	O
comptait	O
18000	O
étudiants	O
en	O
2006-2007	O
,	O
et	O
a	O
été	O
à	O
plusieurs	O
reprises	O
promue	O
"	O
meilleure	O
nouvelle	O
université	O
"	O
anglaise	O
par	O
le	O
journal	O
The	ORGANIZATION
Times	ORGANIZATION
.	O
Plusieurs	O
films	O
y	O
ont	O
été	O
tournés	O
,	O
comme	O
ceux	O
d	O
'	O
Harry	MISC
Potter	MISC
ou	O
Les	MISC
Royaumes	MISC
du	MISC
Nord	MISC
.	O
