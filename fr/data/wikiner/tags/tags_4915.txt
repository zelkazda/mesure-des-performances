Parmi	O
ses	O
ouvrages	O
,	O
on	O
peut	O
citer	O
la	O
série	O
de	O
quatre	O
livres	O
sur	O
Los	LOCATION
Angeles	LOCATION
dont	O
font	O
partie	O
Le	MISC
Dahlia	MISC
noir	MISC
et	O
L.A.	MISC
Confidential	MISC
,	O
sa	O
trilogie	O
Underworld	MISC
USA	MISC
qui	O
retrace	O
son	O
histoire	O
des	O
États-Unis	LOCATION
de	O
1958	O
à	O
1973	O
,	O
ainsi	O
que	O
son	O
récit	O
autobiographique	O
Ma	MISC
part	MISC
d'	MISC
ombre	MISC
sur	O
le	O
meurtre	O
de	O
sa	O
mère	O
en	O
1958	O
.	O
James	PERSON
Ellroy	PERSON
se	O
fait	O
renvoyer	O
du	O
collège	O
à	O
17	O
ans	O
,	O
sans	O
diplôme	O
.	O
Alors	O
que	O
la	O
santé	O
de	O
son	O
père	O
se	O
dégrade	O
,	O
Ellroy	PERSON
s'	O
engage	O
dans	O
l'	O
armée	O
en	O
1965	O
et	O
fait	O
ses	O
armes	O
en	O
Louisiane	LOCATION
.	O
Ellroy	PERSON
vit	O
plus	O
de	O
dix	O
ans	O
sans	O
domicile	O
,	O
parfois	O
dans	O
de	O
petites	O
chambres	O
d'	O
hôtel	O
miteuses	O
,	O
de	O
boulots	O
sporadiques	O
,	O
de	O
larcins	O
,	O
dormant	O
dans	O
les	O
parcs	O
,	O
s'	O
introduisant	O
chez	O
les	O
gens	O
,	O
moins	O
pour	O
cambrioler	O
(	O
il	O
vole	O
des	O
sous-vêtements	O
,	O
de	O
l'	O
alcool	O
,	O
de	O
l'	O
herbe	O
,	O
des	O
cartes	O
de	O
crédit	O
)	O
,	O
que	O
pour	O
ressentir	O
le	O
grand	O
frisson	O
,	O
déclarera-t-il	O
plus	O
tard	O
.	O
Il	O
devient	O
caddie	O
de	O
golf	O
à	O
Los	LOCATION
Angeles	LOCATION
et	O
commence	O
une	O
vie	O
plus	O
rangée	O
.	O
En	O
réalité	O
,	O
les	O
motivations	O
de	O
l'	O
écrivain	O
sont	O
ailleurs	O
,	O
écrire	O
un	O
livre	O
sur	O
le	PERSON
Dahlia	PERSON
Noir	PERSON
,	O
avant	O
que	O
quelqu'	O
un	O
d'	O
autre	O
ne	O
s'	O
en	O
empare	O
.	O
James	PERSON
Ellroy	PERSON
semble	O
avoir	O
utilisé	O
ce	O
fait-divers	O
,	O
pour	O
commencer	O
à	O
exorciser	O
le	O
souvenir	O
du	O
meurtre	O
de	O
sa	O
propre	O
mère	O
qui	O
a	O
eu	O
lieu	O
environ	O
11	O
ans	O
et	O
5	O
mois	O
après	O
celui	O
du	O
dahlia	O
,	O
Elizabeth	PERSON
Short	PERSON
ayant	O
été	O
assassinée	O
en	O
janvier	O
1947	O
.	O
Il	O
écrira	O
à	O
la	O
suite	O
trois	O
autres	O
romans	O
ayant	O
pour	O
cadre	O
la	O
ville	O
de	O
Los	LOCATION
Angeles	LOCATION
dans	O
les	O
années	O
1940	O
--	O
1950	O
et	O
pour	O
thème	O
le	O
crime	O
et	O
la	O
corruption	O
.	O
Toujours	O
obsédé	O
par	O
l'	O
histoire	O
de	O
sa	O
mère	O
il	O
va	O
tenter	O
de	O
résoudre	O
,	O
près	O
de	O
40	O
ans	O
après	O
les	O
faits	O
,	O
le	O
meurtre	O
de	O
sa	O
mère	O
avec	O
l'	O
aide	O
d'	O
un	O
policier	O
de	O
L.A.	LOCATION
à	O
la	O
retraite	O
.	O
Il	O
en	O
écrira	O
le	O
récit	O
dans	O
un	O
livre	O
autobiographique	O
:	O
Ma	MISC
part	MISC
d'	MISC
ombre	MISC
.	O
Après	O
avoir	O
fui	O
son	O
Los	LOCATION
Angeles	LOCATION
natal	O
et	O
vécu	O
à	O
New	LOCATION
York	LOCATION
,	O
Kansas	LOCATION
City	LOCATION
,	O
il	O
revient	O
vivre	O
à	O
Los	LOCATION
Angeles	LOCATION
à	O
partir	O
de	O
2006	O
.	O
James	PERSON
Ellroy	PERSON
est	O
à	O
présent	O
l'	O
un	O
des	O
auteurs	O
de	O
roman	O
noir	O
américains	O
les	O
plus	O
populaires	O
,	O
bien	O
que	O
peu	O
apprécié	O
et	O
peu	O
lu	O
dans	O
son	O
pays	O
.	O
En	O
2008	O
,	O
James	PERSON
Ellroy	PERSON
est	O
au	O
scénario	O
de	O
Au	MISC
bout	MISC
de	MISC
la	MISC
nuit	MISC
,	O
film	O
sorti	O
aux	O
États-Unis	LOCATION
en	O
avril	O
2008	O
,	O
avec	O
notamment	O
Keanu	PERSON
Reeves	PERSON
dans	O
le	O
rôle	O
principal	O
.	O
Il	O
avait	O
déjà	O
,	O
en	O
2002	O
,	O
écrit	O
le	O
scénario	O
de	O
Dark	MISC
Blue	MISC
(	O
de	O
Ron	PERSON
Shelton	PERSON
,	O
avec	O
Kurt	PERSON
Russell	PERSON
)	O
.	O
Les	O
fictions	O
suivantes	O
,	O
toujours	O
d'	O
après	O
les	O
interviews	O
données	O
début	O
2010	O
,	O
seraient	O
une	O
tétralogie	O
concernant	O
Los	LOCATION
Angeles	LOCATION
dans	O
les	O
années	O
40	O
.	O
Le	O
style	O
d'	O
Ellroy	PERSON
s'	O
affirme	O
par	O
une	O
inventivité	O
verbale	O
crue	O
et	O
acide	O
,	O
dépeignant	O
avec	O
rudesse	O
les	O
recoins	O
sombres	O
de	O
la	O
société	O
américaine	O
.	O
Ellroy	PERSON
ne	O
déroge	O
pas	O
à	O
la	O
règle	O
.	O
A	O
l'	O
instar	O
d'	O
un	O
Melville	PERSON
retranscrivant	O
le	O
vocabulaire	O
des	O
marins	O
de	O
son	O
époque	O
et	O
l'	O
utilisant	O
dans	O
son	O
Moby	MISC
Dick	MISC
,	O
Ellroy	PERSON
reprend	O
avec	O
une	O
précision	O
étonnante	O
l'	O
argot	O
(	O
"	O
slang	O
"	O
en	O
anglais	O
)	O
et	O
les	O
expressions	O
des	O
policiers	O
et	O
de	O
la	O
pègre	O
des	O
années	O
1950	O
et	O
1960	O
.	O
Ellroy	PERSON
mêle	O
personnages	O
réels	O
et	O
personnages	O
fictifs	O
,	O
sur	O
une	O
trame	O
de	O
faits	O
historiques	O
avérés	O
,	O
interprétés	O
à	O
sa	O
façon	O
.	O
