Apollo	MISC
10	MISC
est	O
le	O
nom	O
d'	O
une	O
des	O
missions	O
du	O
programme	MISC
Apollo	MISC
mené	O
par	O
les	O
États-Unis	LOCATION
dans	O
les	O
années	O
1960	O
.	O
Apollo	MISC
10	MISC
fut	O
le	O
deuxième	O
vol	O
humain	O
à	O
approcher	O
la	O
Lune	LOCATION
(	O
après	O
Apollo	MISC
8	MISC
)	O
et	O
le	O
deuxième	O
vol	O
à	O
tester	O
le	O
LEM	MISC
(	O
après	O
Apollo	MISC
9	MISC
)	O
.	O
D'	O
après	O
le	O
livre	MISC
Guinness	MISC
des	MISC
records	MISC
2001	O
,	O
il	O
s'	O
agissait	O
également	O
du	O
véhicule	O
habité	O
le	O
plus	O
rapide	O
jamais	O
construit	O
par	O
l'	O
homme	O
,	O
culminant	O
à	O
39897	O
km/h	O
le	O
26	O
mai	O
1969	O
.	O
Trois	O
membres	O
d'	O
équipage	O
embarquèrent	O
à	O
bord	O
du	O
vaisseau	O
Apollo	MISC
10	MISC
:	O
Les	O
moteurs	O
du	O
module	O
de	O
commande	O
propulsèrent	O
le	O
vaisseau	O
jusqu'	O
à	O
la	O
Lune	LOCATION
.	O
Ils	O
vérifièrent	O
notamment	O
son	O
radar	O
et	O
son	O
moteur	O
d'	O
ascension	O
,	O
ainsi	O
que	O
le	O
site	O
d'	O
alunissage	O
final	O
dans	O
la	O
Mer	LOCATION
de	LOCATION
la	LOCATION
Tranquillité	LOCATION
.	O
Apollo	MISC
10	MISC
réalisa	O
également	O
la	O
toute	O
première	O
diffusion	O
télévisée	O
en	O
couleurs	O
et	O
en	O
direct	O
depuis	O
l'	O
espace	O
.	O
Le	O
module	O
de	O
commande	O
est	O
exposé	O
au	O
Science	LOCATION
Museum	LOCATION
de	O
Londres	LOCATION
.	O
L'	O
emblème	O
de	O
la	O
mission	O
,	O
en	O
forme	O
de	O
bouclier	O
,	O
représente	O
le	O
chiffre	O
romain	O
"	O
X	O
"	O
en	O
perspective	O
,	O
posé	O
sur	O
la	O
Lune	LOCATION
.	O
Un	O
module	O
de	O
commande	O
ceinture	O
la	O
Lune	LOCATION
et	O
passe	O
entre	O
les	O
bras	O
du	O
"	O
X	O
"	O
,	O
alors	O
qu'	O
un	O
module	O
lunaire	O
remonte	O
depuis	O
la	O
surface	O
.	O
La	O
Terre	LOCATION
est	O
visible	O
en	O
arrière-plan	O
.	O
