Une	O
étoile	O
génère	O
donc	O
un	O
rayonnement	O
dans	O
le	O
spectre	O
visible	O
,	O
au	O
contraire	O
de	O
la	O
plupart	O
des	O
planètes	O
(	O
comme	O
la	O
Terre	LOCATION
)	O
qui	O
reçoivent	O
principalement	O
l'	O
énergie	O
de	O
l'	O
étoile	O
ou	O
des	O
étoiles	O
autour	O
desquelles	O
elles	O
gravitent	O
.	O
Le	O
Soleil	LOCATION
est	O
lui-même	O
une	O
étoile	O
assez	O
typique	O
dont	O
la	O
masse	O
,	O
de	O
l'	O
ordre	O
de	O
2×10	O
30	O
kg	O
,	O
est	O
représentative	O
de	O
celle	O
des	O
autres	O
étoiles	O
.	O
Le	O
Soleil	LOCATION
est	O
l'	O
étoile	O
la	O
plus	O
proche	O
de	O
la	O
Terre	LOCATION
,	O
l'	O
énergie	O
qu'	O
il	O
rayonne	O
y	O
permet	O
le	O
développement	O
de	O
la	O
vie	O
.	O
Sauf	O
en	O
cas	O
exceptionnel	O
,	O
les	O
autres	O
étoiles	O
ne	O
sont	O
visibles	O
que	O
la	O
nuit	O
,	O
sous	O
la	O
forme	O
de	O
points	O
lumineux	O
,	O
lorsque	O
leur	O
éclat	O
n'	O
est	O
pas	O
noyé	O
par	O
celui	O
du	O
Soleil	LOCATION
.	O
Une	O
galaxie	O
typique	O
,	O
comme	O
la	O
nôtre	O
,	O
la	O
Voie	LOCATION
lactée	LOCATION
,	O
contient	O
plusieurs	O
centaines	O
de	O
milliards	O
d'	O
étoiles	O
.	O
Une	O
étoile	O
comme	O
le	LOCATION
Soleil	LOCATION
a	O
une	O
durée	O
de	O
vie	O
de	O
l'	O
ordre	O
de	O
10	O
milliards	O
d'	O
années	O
.	O
Le	O
jour	O
,	O
le	O
Soleil	LOCATION
domine	O
et	O
sa	O
lumière	O
,	O
diffusée	O
par	O
la	O
couche	O
atmosphérique	O
,	O
occulte	O
celle	O
des	O
étoiles	O
.	O
Mais	O
l'	O
astre	O
le	O
plus	O
brillant	O
visible	O
depuis	O
la	O
Terre	LOCATION
est	O
bien	O
lui-même	O
une	O
étoile	O
.	O
Hormis	O
le	LOCATION
Soleil	LOCATION
et	O
Sirius	LOCATION
--	O
et	O
encore	O
,	O
uniquement	O
dans	O
d'	O
excellentes	O
conditions	O
d'	O
observation	O
--	O
les	O
étoiles	O
sont	O
trop	O
peu	O
brillantes	O
pour	O
être	O
observables	O
en	O
plein	O
jour	O
(	O
sauf	O
lors	O
des	O
éclipses	O
totales	O
de	O
Soleil	LOCATION
et	O
lors	O
de	O
phénomènes	O
temporaires	O
comme	O
les	O
novae	O
ou	O
les	O
supernovae	O
)	O
.	O
Pour	O
des	O
raisons	O
historiques	O
,	O
la	O
magnitude	O
est	O
d'	O
autant	O
plus	O
petite	O
que	O
l'	O
astre	O
est	O
brillant	O
:	O
l'	O
astronome	O
de	O
la	O
Grèce	MISC
antique	MISC
Hipparque	PERSON
avait	O
classifié	O
les	O
étoiles	O
en	O
astres	O
de	O
première	O
grandeur	O
pour	O
les	O
plus	O
brillants	O
,	O
seconde	O
grandeur	O
pour	O
les	O
suivants	O
,	O
et	O
ainsi	O
de	O
suite	O
jusqu'	O
à	O
cinquième	O
grandeur	O
.	O
La	O
définition	O
mathématique	O
précise	O
de	O
la	O
magnitude	O
apparente	O
reprend	O
essentiellement	O
cette	O
classification	O
,	O
avec	O
les	O
étoiles	O
les	O
plus	O
brillantes	O
dotées	O
d'	O
une	O
magnitude	O
proche	O
de	O
0	O
(	O
à	O
l'	O
exception	O
de	O
Sirius	LOCATION
,	O
de	O
magnitude	O
-1.5	O
et	O
de	O
Canopus	LOCATION
,	O
de	O
magnitude	O
-0.7	O
)	O
et	O
les	O
plus	O
faibles	O
d'	O
une	O
magnitude	O
supérieure	O
à	O
6	O
.	O
Depuis	O
Galilée	PERSON
,	O
de	O
multiples	O
instruments	O
ont	O
permis	O
de	O
révéler	O
des	O
caractéristiques	O
variées	O
des	O
étoiles	O
,	O
qui	O
sont	O
détaillées	O
ci-après	O
.	O
Pour	O
le	LOCATION
Soleil	LOCATION
,	O
ce	O
sont	O
principalement	O
du	O
carbone	O
,	O
de	O
l'	O
oxygène	O
,	O
de	O
l'	O
azote	O
et	O
du	O
fer	O
.	O
La	O
rotation	O
du	O
Soleil	LOCATION
a	O
été	O
mise	O
en	O
évidence	O
grâce	O
au	O
déplacement	O
des	O
taches	O
solaires	O
.	O
Comme	O
le	O
Soleil	LOCATION
,	O
les	O
étoiles	O
sont	O
souvent	O
dotées	O
de	O
champs	O
magnétiques	O
.	O
Le	O
champ	O
magnétique	O
du	O
Soleil	LOCATION
,	O
par	O
exemple	O
,	O
possède	O
ces	O
deux	O
aspects	O
;	O
sa	O
composante	O
à	O
grande	O
échelle	O
structure	O
la	O
couronne	O
solaire	O
et	O
est	O
visible	O
lors	O
des	O
éclipses	O
,	O
tandis	O
que	O
sa	O
composante	O
à	O
plus	O
petite	O
échelle	O
est	O
liée	O
aux	O
taches	O
sombres	O
qui	O
maculent	O
sa	O
surface	O
et	O
dans	O
lesquelles	O
les	O
arches	O
magnétiques	O
sont	O
ancrées	O
.	O
Ces	O
étoiles	O
sont	O
"	O
actives	O
"	O
,	O
c'	O
est-à-dire	O
qu'	O
elles	O
sont	O
le	O
siège	O
d'	O
un	O
certain	O
nombre	O
de	O
phénomènes	O
énergétiques	O
liés	O
au	O
champ	O
magnétique	O
,	O
comme	O
par	O
exemple	O
la	O
production	O
d'	O
une	O
couronne	O
,	O
d'	O
un	O
vent	O
(	O
dit	O
vent	O
solaire	O
dans	O
le	O
cas	O
du	O
Soleil	LOCATION
)	O
ou	O
d'	O
éruptions	O
.	O
Le	O
Soleil	LOCATION
,	O
qui	O
effectue	O
un	O
tour	O
complet	O
sur	O
lui-même	O
en	O
25	O
jours	O
environ	O
,	O
est	O
une	O
étoile	O
ayant	O
une	O
faible	O
activité	O
cylique	O
.	O
Le	O
noyau	O
est	O
la	O
zone	O
la	O
plus	O
dense	O
et	O
la	O
plus	O
chaude	O
,	O
et	O
,	O
dans	O
le	O
cas	O
du	O
Soleil	LOCATION
,	O
atteint	O
la	O
température	O
de	O
15.7	O
millions	O
de	O
kelvins	O
.	O
Dans	O
le	LOCATION
Soleil	LOCATION
,	O
le	O
rayonnement	O
produit	O
dans	O
la	O
partie	O
centrale	O
met	O
près	O
d'	O
un	O
million	O
d'	O
années	O
à	O
traverser	O
la	O
zone	O
radiative	O
.	O
Cette	O
zone	O
convective	O
est	O
plus	O
ou	O
moins	O
grande	O
:	O
pour	O
une	O
étoile	O
sur	O
la	O
séquence	O
principale	O
,	O
elle	O
dépend	O
de	O
la	O
masse	O
et	O
de	O
la	O
composition	O
chimique	O
;	O
pour	O
une	O
géante	O
,	O
elle	O
est	O
très	O
développée	O
et	O
occupe	O
un	O
pourcentage	O
important	O
du	O
volume	O
de	O
l'	O
étoile	O
;	O
pour	O
une	O
supergéante	O
,	O
cette	O
zone	O
peut	O
atteindre	O
les	O
trois	O
quarts	O
du	O
volume	O
de	O
l'	O
étoile	O
,	O
comme	O
pour	O
Bételgeuse	LOCATION
.	O
C'	O
est	O
dans	O
la	O
zone	O
convective	O
externe	O
que	O
sont	O
produits	O
les	O
champs	O
magnétiques	O
de	O
type	O
dynamo	O
des	O
étoiles	O
froides	O
comme	O
le	LOCATION
Soleil	LOCATION
et	O
les	O
naines	O
rouges	O
.	O
Pour	O
le	LOCATION
Soleil	LOCATION
,	O
la	O
photosphère	O
a	O
une	O
épaisseur	O
d'	O
environ	O
400	O
kilomètres	O
.	O
La	O
couronne	O
est	O
la	O
zone	O
externe	O
,	O
ténue	O
et	O
extrêmement	O
chaude	O
du	O
Soleil	LOCATION
.	O
Elle	O
est	O
due	O
à	O
la	O
présence	O
d'	O
un	O
champ	O
magnétique	O
,	O
produit	O
dans	O
la	O
zone	O
convective	O
;	O
on	O
peut	O
l'	O
observer	O
lors	O
des	O
éclipses	O
de	O
Soleil	LOCATION
.	O
Elles	O
sont	O
les	O
plus	O
abondantes	O
:	O
au	O
moins	O
80	O
%	O
des	O
étoiles	O
de	O
notre	LOCATION
Galaxie	LOCATION
sont	O
des	O
naines	O
rouges	O
.	O
La	O
plus	O
proche	O
voisine	O
du	O
Soleil	LOCATION
,	O
Proxima	LOCATION
du	LOCATION
Centaure	LOCATION
,	O
en	O
est	O
une	O
.	O
Il	O
en	O
est	O
de	O
même	O
du	O
second	O
système	O
stellaire	O
,	O
le	O
plus	O
proche	O
système	O
solaire	O
,	O
l'	O
étoile	LOCATION
de	LOCATION
Barnard	LOCATION
est	O
aussi	O
une	O
naine	O
rouge	O
.	O
Le	O
Soleil	LOCATION
est	O
une	O
naine	O
jaune	O
typique	O
.	O
Ces	O
étoiles	O
très	O
massives	O
,	O
au	O
moins	O
dix	O
fois	O
plus	O
grosses	O
que	O
le	LOCATION
Soleil	LOCATION
,	O
consomment	O
rapidement	O
leur	O
hydrogène	O
.	O
Le	O
Soleil	LOCATION
ayant	O
(	O
par	O
définition	O
)	O
une	O
masse	O
d'	O
une	O
masse	O
solaire	O
,	O
il	O
finira	O
aussi	O
en	O
naine	O
blanche	O
.	O
La	O
densité	O
moyenne	O
d'	O
une	O
naine	O
blanche	O
est	O
telle	O
qu'	O
une	O
cuillère	O
à	O
thé	O
de	O
matière	O
d'	O
une	O
telle	O
étoile	O
aurait	O
,	O
sur	O
Terre	LOCATION
,	O
le	O
poids	O
d'	O
un	O
éléphant	O
soit	O
environ	O
1	O
kg⋅mm	O
-3	O
.	O
Après	O
sa	O
mort	O
,	O
le	LOCATION
Soleil	LOCATION
deviendra	O
une	O
naine	O
blanche	O
puis	O
une	O
naine	O
noire	O
.	O
Elles	O
concentrent	O
la	O
masse	O
d'	O
une	O
fois	O
et	O
demi	O
celle	O
du	O
Soleil	LOCATION
dans	O
un	O
rayon	O
d'	O
environ	O
10	O
kilomètres	O
.	O
Si	O
par	O
chance	O
un	O
observateur	O
sur	O
Terre	LOCATION
regarde	O
dans	O
la	O
direction	O
d'	O
une	O
étoile	O
à	O
neutrons	O
et	O
que	O
la	O
ligne	O
de	O
visée	O
est	O
perpendiculaire	O
à	O
l'	O
axe	O
de	O
rotation	O
de	O
l'	O
étoile	O
,	O
celui-ci	O
verra	O
alors	O
le	O
rayonnement	O
synchrotron	O
des	O
particules	O
chargées	O
se	O
déplaçant	O
sur	O
les	O
lignes	O
de	O
champ	O
magnétique	O
.	O
On	O
trouve	O
des	O
pulsars	O
dans	O
des	O
restes	O
de	O
supernovas	O
,	O
le	O
plus	O
célèbre	O
étant	O
le	O
pulsar	O
de	O
la	O
nébuleuse	LOCATION
du	LOCATION
Crabe	LOCATION
,	O
né	O
de	O
l'	O
explosion	O
d'	O
une	O
étoile	O
massive	O
.	O
Alors	O
que	O
la	O
plupart	O
des	O
étoiles	O
sont	O
de	O
luminosité	O
presque	O
constante	O
,	O
comme	O
le	LOCATION
Soleil	LOCATION
qui	O
ne	O
possède	O
pratiquement	O
pas	O
de	O
variation	O
mesurable	O
(	O
environ	O
0.1	O
%	O
sur	O
un	O
cycle	O
de	O
11	O
ans	O
)	O
,	O
la	O
luminosité	O
de	O
certaines	O
étoiles	O
varie	O
de	O
façon	O
perceptible	O
sur	O
des	O
périodes	O
de	O
temps	O
beaucoup	O
plus	O
courtes	O
,	O
parfois	O
de	O
façon	O
spectaculaire	O
.	O
La	O
fonction	O
la	O
plus	O
adoptée	O
actuellement	O
a	O
été	O
proposée	O
par	O
Edwin	PERSON
Salpeter	PERSON
et	O
semble	O
donner	O
des	O
résultats	O
satisfaisants	O
pour	O
l'	O
étude	O
des	O
amas	O
de	O
la	O
Galaxie	LOCATION
.	O
On	O
peut	O
s'	O
en	O
servir	O
pour	O
déterminer	O
l'	O
âge	O
des	O
plus	O
vieilles	O
populations	O
d'	O
étoiles	O
de	O
notre	LOCATION
Galaxie	LOCATION
.	O
Leurs	O
étoiles	O
sont	O
pauvres	O
en	O
métaux	O
et	O
ils	O
comptent	O
parmi	O
les	O
objets	O
les	O
plus	O
vieux	O
de	O
la	LOCATION
Galaxie	LOCATION
.	O
Ils	O
se	O
répartissent	O
dans	O
le	O
sphéroïde	O
de	O
la	LOCATION
Galaxie	LOCATION
qu'	O
on	O
appelle	O
le	O
halo	O
.	O
Il	O
est	O
considéré	O
comme	O
pouvant	O
être	O
le	O
résidu	O
d'	O
une	O
galaxie	O
naine	O
ayant	O
été	O
capturée	O
par	O
la	O
Voie	LOCATION
Lactée	LOCATION
.	O
On	O
les	O
rencontre	O
dans	O
notre	LOCATION
Galaxie	LOCATION
principalement	O
dans	O
les	O
bras	O
.	O
Les	O
étoiles	O
d'	O
une	O
constellation	O
n'	O
ont	O
a	O
priori	O
rien	O
en	O
commun	O
,	O
si	O
ce	O
n'	O
est	O
d'	O
occuper	O
,	O
vues	O
de	O
la	O
Terre	LOCATION
,	O
une	O
position	O
voisine	O
dans	O
le	O
ciel	O
.	O
Toutefois	O
,	O
l'	O
Union	ORGANIZATION
astronomique	ORGANIZATION
internationale	ORGANIZATION
a	O
défini	O
une	O
liste	O
normalisée	O
des	O
constellations	O
,	O
attribuant	O
à	O
chacune	O
une	O
région	O
du	O
ciel	O
,	O
afin	O
de	O
faciliter	O
la	O
localisation	O
des	O
objets	O
célestes	O
.	O
Ainsi	O
,	O
le	O
système	O
solaire	O
est	O
composé	O
d'	O
une	O
étoile	O
centrale	O
,	O
le	O
Soleil	LOCATION
,	O
accompagné	O
de	O
planètes	O
,	O
comètes	O
,	O
astéroïdes	O
.	O
Depuis	O
1995	O
,	O
340	O
planètes	O
ont	O
été	O
découvertes	O
autour	O
d'	O
autres	O
étoiles	O
que	O
le	LOCATION
Soleil	LOCATION
,	O
faisant	O
perdre	O
au	O
système	O
solaire	O
son	O
caractère	O
supposé	O
unique	O
.	O
La	O
première	O
étoile	O
autour	O
de	O
laquelle	O
des	O
planètes	O
ont	O
été	O
révélées	O
par	O
des	O
mesures	O
vélocimétriques	O
est	O
51	LOCATION
Peg	LOCATION
.	O
En	O
raison	O
des	O
limitations	O
actuelles	O
de	O
détection	O
,	O
ils	O
présentent	O
des	O
caractéristiques	O
semblables	O
,	O
avec	O
des	O
planètes	O
géantes	O
sur	O
des	O
orbites	O
très	O
excentriques	O
:	O
on	O
les	O
nomme	O
des	O
"	O
Jupiter	LOCATION
chauds	O
"	O
.	O
La	O
majorité	O
de	O
ces	O
étoiles	O
sont	O
plus	O
riches	O
en	O
métaux	O
que	O
le	LOCATION
Soleil	LOCATION
.	O
Depuis	O
l'	O
espace	O
,	O
la	O
traque	O
des	O
systèmes	O
planétaires	O
par	O
photométrie	O
a	O
commencé	O
avec	O
le	O
satellite	O
CoRoT	LOCATION
.	O
Celui-ci	O
sera	O
relayé	O
en	O
2009	O
par	O
le	O
satellite	O
américain	O
Kepler	MISC
.	O
