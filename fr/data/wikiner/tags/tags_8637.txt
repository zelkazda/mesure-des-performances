Avec	O
plus	O
de	O
60	O
000	O
habitants	O
,	O
Les	LOCATION
Abymes	LOCATION
est	O
la	O
commune	O
la	O
plus	O
peuplée	O
de	O
Guadeloupe	LOCATION
.	O
La	O
commune	O
Les	LOCATION
Abymes	LOCATION
est	O
la	O
plus	O
peuplée	O
des	O
32	O
communes	O
du	O
département	O
de	O
la	O
Guadeloupe	LOCATION
,	O
à	O
l'	O
ouest	O
de	O
la	O
Grande-Terre	LOCATION
.	O
Elle	O
est	O
intégrée	O
à	O
l'	O
agglomération	O
de	O
Pointe-à-Pitre	LOCATION
.	O
Les	O
communes	O
limitrophes	O
avec	O
les	LOCATION
Abymes	LOCATION
sont	O
Pointe-à-Pitre	LOCATION
,	O
Le	LOCATION
Gosier	LOCATION
,	O
Sainte-Anne	LOCATION
,	O
Le	LOCATION
Moule	LOCATION
,	O
Morne-à-l'Eau	LOCATION
et	O
Baie-Mahault	LOCATION
.	O
Le	O
nom	O
de	O
la	O
commune	O
proviendrait	O
selon	O
le	O
père	PERSON
Labat	PERSON
,	O
de	O
la	O
brume	O
formée	O
par	O
l'	O
évaporation	O
au	O
niveau	O
du	O
sol	O
marécageux	O
,	O
que	O
l'	O
on	O
appelait	O
"	O
drap	O
mortuaire	O
des	O
savanes	O
"	O
.	O
Les	LOCATION
Abymes	LOCATION
sont	O
érigées	O
en	O
paroisse	O
en	O
1726	O
.	O
Les	LOCATION
Abymes	LOCATION
sont	O
divisées	O
en	O
cinq	O
cantons	O
:	O
L	O
'	O
Aéroport	LOCATION
de	LOCATION
Guadeloupe	LOCATION
--	LOCATION
Pôle	LOCATION
Caraïbes	LOCATION
se	O
trouve	O
sur	O
le	O
territoire	O
de	O
la	O
commune	O
.	O
L'	O
aéroport	LOCATION
international	LOCATION
Pôle	LOCATION
Caraïbes	LOCATION
est	O
l'	O
ancien	O
aéroport	LOCATION
du	LOCATION
Raizet	LOCATION
.	O
Le	O
siège	O
d'	O
Air	ORGANIZATION
Caraïbes	ORGANIZATION
est	O
situé	O
aux	O
Abymes	LOCATION
.	O
