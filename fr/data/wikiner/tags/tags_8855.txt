La	O
PC-FX	MISC
est	O
une	O
console	O
de	O
jeux	O
vidéo	O
32-bit	O
,	O
développée	O
par	O
NEC	ORGANIZATION
et	O
commercialisé	O
à	O
partir	O
de	O
décembre	O
1994	O
,	O
uniquement	O
au	O
Japon	LOCATION
.	O
Elle	O
succède	O
à	O
la	O
SuperGrafX	MISC
.	O
Une	O
des	O
choses	O
les	O
plus	O
originales	O
concernant	O
la	O
PC-FX	MISC
est	O
son	O
apparence	O
externe	O
.	O
La	O
manette	O
de	O
jeu	O
de	O
ce	O
système	O
ressemble	O
beaucoup	O
à	O
la	O
manette	O
6	O
boutons	O
de	O
la	O
Megadrive	MISC
.	O
La	O
PC-FX	MISC
ne	O
disposait	O
pas	O
d'	O
un	O
,	O
ni	O
deux	O
,	O
mais	O
trois	O
connecteurs	O
d'	O
extensions	O
.	O
Cette	O
console	O
n'	O
eut	O
jamais	O
une	O
place	O
importante	O
dans	O
le	O
cœur	O
des	O
joueurs	O
,	O
elle	O
fut	O
disponible	O
uniquement	O
au	O
Japon	LOCATION
.	O
