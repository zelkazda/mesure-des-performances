Alfred	PERSON
Marie-Jeanne	PERSON
est	O
issu	O
d'	O
une	O
famille	O
modeste	O
.	O
Il	O
effectue	O
ses	O
études	O
primaires	O
à	O
Rivière-Pilote	LOCATION
.	O
Dans	O
les	O
années	O
70	O
,	O
il	O
obtient	O
à	O
l'	O
Université	LOCATION
Antilles-Guyane	LOCATION
une	O
maîtrise	O
en	O
sciences	O
économiques	O
.	O
Alfred	PERSON
Marie-Jeanne	PERSON
s'	O
engage	O
en	O
politique	O
en	O
1971	O
en	O
se	O
présentant	O
pour	O
la	O
première	O
fois	O
aux	O
élections	O
municipales	O
à	O
Rivière-Pilote	LOCATION
,	O
il	O
n'	O
appartient	O
à	O
ce	O
moment-là	O
à	O
aucun	O
parti	O
politique	O
mais	O
est	O
proche	O
des	O
idées	O
de	O
gauche	O
.	O
Puis	O
,	O
aux	O
élections	O
cantonales	O
de	O
1973	O
,	O
il	O
est	O
élu	O
conseiller	O
général	O
du	O
canton	O
de	O
Rivière-Pilote	LOCATION
avec	O
2470	O
voix	O
soit	O
61.08	O
%	O
.	O
Aux	O
législatives	O
de	O
1973	O
,	O
Alfred	PERSON
Marie-Jeanne	PERSON
conclu	O
une	O
alliance	O
politique	O
avec	O
le	O
Parti	ORGANIZATION
progressiste	ORGANIZATION
martiniquais	ORGANIZATION
.	O
La	O
base	O
de	O
cette	O
alliance	O
politique	O
a	O
pour	O
objectif	O
:	O
la	O
lutte	O
pour	O
la	O
reconnaissance	O
par	O
les	O
autorités	O
françaises	O
,	O
du	O
droit	O
à	O
l'	O
autodétermination	O
de	O
la	LOCATION
Martinique	LOCATION
.	O
Alfred	PERSON
Marie-Jeanne	PERSON
grâce	O
à	O
son	O
charisme	O
et	O
son	O
franc-parler	O
devient	O
incontestablement	O
le	O
leader	O
de	O
la	O
mouvance	O
nationaliste	O
en	O
Martinique	LOCATION
.	O
Au	O
début	O
des	O
années	O
90	O
,	O
Alfred	PERSON
Marie-Jeanne	PERSON
change	O
de	O
stratégie	O
et	O
assouplit	O
sa	O
position	O
.	O
Aux	O
élections	O
régionales	O
de	O
1998	O
,	O
la	O
liste	O
conduite	O
par	O
Alfred	PERSON
Marie-Jeanne	PERSON
obtient	O
24.6	O
%	O
des	O
voix	O
et	O
gagne	O
13	O
sièges	O
au	O
conseil	O
régional	O
.	O
Le	O
19	O
décembre	O
1999	O
,	O
il	O
signe	O
avec	O
Antoine	PERSON
Karam	PERSON
,	O
président	O
du	O
conseil	LOCATION
régional	LOCATION
de	LOCATION
la	LOCATION
Guyane	LOCATION
et	O
Lucette	PERSON
Michaux-Chevry	PERSON
,	O
présidente	O
du	O
conseil	LOCATION
régional	LOCATION
de	LOCATION
la	LOCATION
Guadeloupe	LOCATION
,	O
"	O
La	O
déclaration	O
de	O
Basse-Terre	LOCATION
"	O
.	O
Alfred	PERSON
Marie-Jeanne	PERSON
a	O
annoncé	O
plusieurs	O
chantiers	O
prioritaires	O
comme	O
la	O
mise	O
en	O
place	O
du	O
haut	O
débit	O
avec	O
l'	O
ADSL	O
,	O
la	O
dépollution	O
des	O
sols	O
agricoles	O
infectés	O
par	O
les	O
pesticides	O
et	O
la	O
construction	O
d'	O
un	O
institut	O
des	O
métiers	O
du	O
sport	O
.	O
Le	O
rejet	O
du	O
statut	O
d'	O
autonomie	O
par	O
le	O
peuple	O
est	O
un	O
revers	O
pour	O
Alfred	PERSON
Marie-Jeanne	PERSON
puisqu'	O
il	O
avait	O
appelé	O
à	O
voter	O
"	O
oui	O
"	O
.	O
Les	O
électeurs	O
Martiniquais	LOCATION
ont	O
approuvé	O
ce	O
choix	O
à	O
68,4	O
%	O
.	O
Aux	O
élections	O
régionales	O
des	O
14	O
et	O
21	O
mars	O
2010	O
,	O
la	O
liste	O
"	O
Les	O
patriotes	O
martiniquais	O
et	O
sympathisants	O
"	O
conduite	O
par	O
Alfred	PERSON
Marie-Jeanne	PERSON
arrive	O
au	O
second	O
tour	O
en	O
deuxième	O
position	O
avec	O
66309	O
voix	O
et	O
obtient	O
12	O
sièges	O
.	O
