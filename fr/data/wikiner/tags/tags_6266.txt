L	O
'	O
École	ORGANIZATION
des	ORGANIZATION
Mines	ORGANIZATION
d'	ORGANIZATION
Alès	ORGANIZATION
ou	O
EMA	ORGANIZATION
est	O
une	O
grande	O
école	O
d'	O
ingénieurs	O
fondée	O
en	O
1843	O
.	O
Historiquement	O
implantée	O
à	O
Alès	LOCATION
,	O
elle	O
dispose	O
aujourd'hui	O
d'	O
extensions	O
à	O
Nîmes	LOCATION
et	O
Pau	LOCATION
.	O
Les	O
écoles	O
disposent	O
d'	O
un	O
organisme	O
de	O
recherche	O
commun	O
qui	O
est	O
un	O
acteur	O
important	O
de	O
l'	O
innovation	O
technologique	O
et	O
du	O
développement	O
industriel	O
en	O
France	LOCATION
.	O
Le	O
recrutement	O
à	O
l'	O
EMA	ORGANIZATION
se	O
fait	O
sous	O
trois	O
cas	O
de	O
figure	O
:	O
Dans	O
le	O
domaine	O
des	O
matériaux	O
de	O
grande	O
diffusion	O
,	O
le	O
Centre	LOCATION
a	O
trouvé	O
important	O
de	O
s'	O
intéresser	O
aux	O
multimatériaux	O
dans	O
lesquels	O
les	O
composantes	O
"	O
matières	O
premières	O
"	O
,	O
notamment	O
les	O
minéraux	O
industriels	O
co-produits	O
,	O
sous-produits	O
et	O
les	O
matrices	O
de	O
grande	O
diffusion	O
,	O
jouent	O
un	O
rôle	O
important	O
.	O
Dans	O
ce	O
contexte	O
,	O
le	O
Centre	LOCATION
LGEI	ORGANIZATION
propose	O
notamment	O
de	O
nouvelles	O
approches	O
de	O
la	O
gestion	O
des	O
risques	O
pour	O
que	O
de	O
nouveaux	O
procédés	O
et	O
filières	O
de	O
traitement	O
et	O
de	O
recyclage	O
,	O
adaptés	O
pour	O
certains	O
types	O
d'	O
effluents	O
industriels	O
,	O
gazeux	O
(	O
odeurs	O
...	O
)	O
ou	O
liquides	O
.	O
Fondé	O
en	O
1984	O
,	O
l'	O
incubateur	O
technologique	O
de	O
l'	O
École	ORGANIZATION
des	ORGANIZATION
Mines	ORGANIZATION
d'	ORGANIZATION
Alès	ORGANIZATION
a	O
pour	O
vocation	O
d'	O
accompagner	O
les	O
porteurs	O
d'	O
idées	O
et	O
projets	O
intégrants	O
des	O
technologies	O
innovantes	O
jusqu'	O
à	O
la	O
création	O
d'	O
entreprise	O
.	O
En	O
1999	O
,	O
l'	O
EMA	ORGANIZATION
met	O
en	O
place	O
une	O
pédagogie	O
novatrice	O
et	O
inédite	O
,	O
appelée	O
pédagogie-action	O
,	O
et	O
devient	O
une	O
école	O
d'	O
ingénieurs	O
entrepreneurs	O
.	O
En	O
2004	O
,	O
avec	O
au	O
cœur	O
de	O
la	O
formation	O
,	O
son	O
dispositif	O
entrepreneurial	O
,	O
l'	O
École	ORGANIZATION
des	ORGANIZATION
Mines	ORGANIZATION
d'	ORGANIZATION
Alès	ORGANIZATION
contribue	O
davantage	O
au	O
rayonnement	O
économique	O
et	O
technologique	O
de	O
sa	O
région	O
.	O
L'	O
École	ORGANIZATION
des	ORGANIZATION
Mines	ORGANIZATION
d'	ORGANIZATION
Alès	ORGANIZATION
(	O
EMA	ORGANIZATION
)	O
a	O
été	O
créée	O
en	O
1841	O
.	O
--	O
Incubateur	ORGANIZATION
technologique	ORGANIZATION
