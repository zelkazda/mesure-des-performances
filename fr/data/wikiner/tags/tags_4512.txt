Jupiter	LOCATION
est	O
une	O
planète	O
géante	O
gazeuse	O
,	O
la	O
plus	O
grosse	O
planète	O
du	O
système	O
solaire	O
et	O
la	O
cinquième	O
en	O
partant	O
du	O
Soleil	LOCATION
(	O
après	O
Mercure	LOCATION
,	O
Vénus	LOCATION
,	O
la	O
Terre	LOCATION
et	O
Mars	LOCATION
)	O
.	O
Elle	O
doit	O
son	O
nom	O
au	O
dieu	O
romain	O
Jupiter	PERSON
.	O
Le	O
symbole	O
astronomique	O
de	O
la	O
planète	O
est	O
la	O
représentation	O
du	O
foudre	O
de	O
Jupiter	LOCATION
.	O
Visible	O
à	O
l'	O
œil	O
nu	O
dans	O
le	O
ciel	O
nocturne	O
,	O
Jupiter	LOCATION
est	O
habituellement	O
le	O
quatrième	O
objet	O
le	O
plus	O
brillant	O
(	O
après	O
le	O
Soleil	LOCATION
,	O
la	O
Lune	LOCATION
et	O
Vénus	LOCATION
;	O
parfois	O
Mars	LOCATION
apparaît	O
plus	O
lumineuse	O
que	O
Jupiter	LOCATION
,	O
et	O
de	O
temps	O
en	O
temps	O
Jupiter	LOCATION
apparaît	O
plus	O
lumineuse	O
que	O
Vénus	LOCATION
)	O
.	O
La	O
haute	O
atmosphère	O
de	O
Jupiter	LOCATION
est	O
composée	O
à	O
93	O
%	O
d'	O
hydrogène	O
et	O
7	O
%	O
d'	O
hélium	O
en	O
nombre	O
d'	O
atomes	O
,	O
ou	O
à	O
86	O
%	O
de	O
dihydrogène	O
et	O
13	O
%	O
d'	O
hélium	O
en	O
nombre	O
de	O
molécules	O
.	O
L'	O
intérieur	O
de	O
Jupiter	LOCATION
contient	O
des	O
matériaux	O
plus	O
denses	O
et	O
la	O
distribution	O
par	O
masse	O
est	O
de	O
71	O
%	O
d'	O
hydrogène	O
,	O
24	O
%	O
d'	O
hélium	O
et	O
5	O
%	O
d'	O
autres	O
éléments	O
.	O
Néanmoins	O
,	O
le	O
néon	O
n'	O
y	O
est	O
détecté	O
qu'	O
à	O
hauteur	O
de	O
vingt	O
parties	O
par	O
million	O
en	O
termes	O
de	O
masse	O
,	O
un	O
dixième	O
de	O
ce	O
qu'	O
on	O
trouve	O
dans	O
le	O
Soleil	LOCATION
.	O
Les	O
gaz	O
inertes	O
lourds	O
sont	O
2	O
à	O
3	O
fois	O
plus	O
abondants	O
dans	O
l'	O
atmosphère	O
de	O
Jupiter	LOCATION
que	O
dans	O
le	LOCATION
Soleil	LOCATION
.	O
Par	O
spectroscopie	O
,	O
on	O
pense	O
que	O
Saturne	LOCATION
possède	O
une	O
composition	O
similaire	O
,	O
mais	O
qu'	O
Uranus	LOCATION
et	O
Neptune	LOCATION
sont	O
constituées	O
de	O
beaucoup	O
moins	O
d'	O
hydrogène	O
et	O
d'	O
hélium	O
.	O
Jupiter	LOCATION
est	O
2,5	O
fois	O
plus	O
massive	O
que	O
toutes	O
les	O
autres	O
planètes	O
du	O
système	O
solaire	O
réunies	O
,	O
tellement	O
massive	O
que	O
son	O
barycentre	O
avec	O
le	O
Soleil	LOCATION
est	O
situé	O
à	O
l'	O
extérieur	O
de	O
ce	O
dernier	O
,	O
à	O
environ	O
1,068	O
rayon	O
solaire	O
du	O
centre	O
du	O
Soleil	LOCATION
.	O
Si	O
Jupiter	LOCATION
était	O
plus	O
massive	O
,	O
on	O
pense	O
que	O
son	O
diamètre	O
serait	O
plus	O
petit	O
.	O
Par	O
conséquent	O
,	O
Jupiter	LOCATION
posséderait	O
le	O
diamètre	O
maximal	O
d'	O
une	O
planète	O
de	O
sa	O
composition	O
et	O
de	O
son	O
histoire	O
.	O
La	O
plus	O
petite	O
naine	O
rouge	O
connue	O
ne	O
possède	O
un	O
diamètre	O
que	O
de	O
30	O
%	O
plus	O
grand	O
que	O
celui	O
de	O
Jupiter	LOCATION
.	O
Des	O
exoplanètes	O
beaucoup	O
plus	O
massives	O
que	O
Jupiter	LOCATION
ont	O
été	O
découvertes	O
.	O
Jupiter	LOCATION
rayonne	O
plus	O
d'	O
énergie	O
qu'	O
elle	O
n'	O
en	O
reçoit	O
du	O
Soleil	LOCATION
.	O
La	O
quantité	O
de	O
chaleur	O
produite	O
à	O
l'	O
intérieur	O
de	O
la	O
planète	O
est	O
presque	O
égale	O
à	O
celle	O
reçue	O
du	O
Soleil	LOCATION
.	O
Lorsque	O
Jupiter	LOCATION
s'	O
est	O
formée	O
,	O
elle	O
était	O
nettement	O
plus	O
chaude	O
et	O
son	O
diamètre	O
était	O
double	O
.	O
Dans	O
l'	O
état	O
actuel	O
des	O
choses	O
,	O
les	O
connaissances	O
sur	O
la	O
composition	O
planétaire	O
de	O
Jupiter	LOCATION
sont	O
relativement	O
spéculatives	O
et	O
ne	O
reposent	O
que	O
sur	O
des	O
mesures	O
indirectes	O
.	O
Selon	O
l'	O
un	O
des	O
modèles	O
proposés	O
,	O
Jupiter	LOCATION
ne	O
posséderait	O
aucune	O
surface	O
solide	O
,	O
la	O
densité	O
augmentant	O
progressivement	O
vers	O
le	O
centre	O
de	O
la	O
planète	O
.	O
Les	O
énormes	O
pressions	O
générées	O
par	O
Jupiter	LOCATION
provoquèrent	O
les	O
températures	O
élevées	O
à	O
l'	O
intérieur	O
de	O
la	O
planète	O
,	O
par	O
un	O
mécanisme	O
de	O
compression	O
gravitationnelle	O
;	O
qui	O
se	O
poursuit	O
encore	O
de	O
nos	O
jours	O
,	O
par	O
une	O
contraction	O
résiduelle	O
de	O
la	O
planète	O
.	O
Si	O
Jupiter	LOCATION
avait	O
été	O
75	O
fois	O
plus	O
massive	O
,	O
la	O
température	O
au	O
centre	O
du	O
noyau	O
aurait	O
été	O
suffisante	O
pour	O
qu'	O
il	O
y	O
ait	O
la	O
fusion	O
de	O
l'	O
hydrogène	O
,	O
et	O
Jupiter	LOCATION
serait	O
devenue	O
une	O
étoile	O
;	O
d'	O
ailleurs	O
,	O
la	O
plus	O
petite	O
naine	O
rouge	O
connue	O
est	O
seulement	O
30	O
%	O
plus	O
volumineuse	O
que	O
Jupiter	LOCATION
,	O
.	O
La	O
faible	O
inclinaison	O
de	O
l'	O
axe	O
de	O
Jupiter	LOCATION
fait	O
que	O
ses	O
pôles	O
reçoivent	O
bien	O
moins	O
d'	O
énergie	O
du	O
Soleil	LOCATION
que	O
sa	O
région	O
équatoriale	O
.	O
On	O
pense	O
également	O
que	O
l'	O
atmosphère	O
de	O
Jupiter	LOCATION
comporte	O
trois	O
couches	O
de	O
nuages	O
distinctes	O
:	O
Ces	O
chiffres	O
proviennent	O
des	O
données	O
sur	O
la	O
condensation	O
de	O
ces	O
composés	O
en	O
fonction	O
de	O
la	O
température	O
,	O
mais	O
l'	O
évolution	O
de	O
la	O
température	O
à	O
l'	O
intérieur	O
de	O
l'	O
atmosphère	O
de	O
Jupiter	LOCATION
n'	O
est	O
pas	O
connue	O
avec	O
précision	O
.	O
L'	O
atmosphère	O
externe	O
de	O
Jupiter	LOCATION
subit	O
une	O
rotation	O
différentielle	O
,	O
remarquée	O
pour	O
la	O
première	O
fois	O
par	O
Jean-Dominique	PERSON
Cassini	PERSON
en	O
1690	O
,	O
qui	O
a	O
aussi	O
estimé	O
sa	O
période	O
de	O
rotation	O
.	O
La	O
rotation	O
de	O
l'	O
atmosphère	O
polaire	O
de	O
Jupiter	LOCATION
est	O
d'	O
environ	O
5	O
minutes	O
plus	O
longue	O
que	O
celle	O
de	O
l'	O
atmosphère	O
à	O
la	O
ligne	O
équatoriale	O
.	O
Les	O
interactions	O
entre	O
ces	O
systèmes	O
circulatoires	O
créent	O
des	O
orages	O
et	O
des	O
turbulences	O
locales	O
,	O
telles	O
la	O
grande	LOCATION
Tache	LOCATION
Rouge	LOCATION
,	O
un	O
large	O
ovale	O
de	O
près	O
de	O
12000	O
km	O
sur	O
25000	O
km	O
d'	O
une	O
grande	O
stabilité	O
,	O
puisque	O
déjà	O
observé	O
avec	O
certitude	O
depuis	O
au	O
moins	O
1831	O
et	O
possiblement	O
depuis	O
1665	O
.	O
La	O
couche	O
la	O
plus	O
externe	O
de	O
l'	O
atmosphère	O
de	O
Jupiter	LOCATION
contient	O
des	O
cristaux	O
de	O
glace	O
d'	O
ammoniac	O
.	O
Jupiter	LOCATION
possède	O
également	O
des	O
ovales	O
blancs	O
et	O
bruns	O
de	O
plus	O
petite	O
taille	O
.	O
Située	O
à	O
la	O
même	O
distance	O
de	O
l'	O
équateur	O
,	O
elle	O
possède	O
une	O
période	O
de	O
rotation	O
propre	O
,	O
légèrement	O
différente	O
du	O
reste	O
de	O
l'	O
atmosphère	O
avoisinante	O
,	O
parfois	O
plus	O
lente	O
,	O
d'	O
autres	O
fois	O
plus	O
rapide	O
:	O
depuis	O
l'	O
époque	O
où	O
elle	O
est	O
connue	O
,	O
elle	O
a	O
fait	O
plusieurs	O
fois	O
le	O
tour	O
de	O
Jupiter	LOCATION
par	O
rapport	O
à	O
son	O
environnement	O
proche	O
.	O
Jupiter	LOCATION
possède	O
plusieurs	O
anneaux	O
planétaires	O
,	O
très	O
fins	O
,	O
composés	O
de	O
particules	O
de	O
poussières	O
continuellement	O
arrachées	O
aux	O
quatre	O
lunes	O
les	O
plus	O
proches	O
de	O
la	O
planète	O
lors	O
de	O
micro-impacts	O
météoriques	O
du	O
fait	O
de	O
l'	O
intense	O
champ	O
gravitationnel	O
de	O
la	O
planète	O
.	O
Ces	O
anneaux	O
sont	O
en	O
fait	O
tellement	O
fins	O
et	O
sombres	O
qu'	O
ils	O
ne	O
furent	O
découverts	O
que	O
lorsque	O
la	O
sonde	O
Voyager	MISC
1	MISC
s'	O
approcha	O
de	O
la	O
planète	O
en	O
1979	O
.	O
Il	O
existe	O
également	O
un	O
anneau	O
externe	O
extrêmement	O
ténu	O
et	O
distant	O
qui	O
tourne	O
autour	O
de	O
Jupiter	LOCATION
en	O
sens	O
rétrograde	O
.	O
Il	O
proviendrait	O
des	O
mouvements	O
de	O
la	O
couche	O
d'	O
hydrogène	O
métallique	O
qui	O
,	O
par	O
sa	O
rotation	O
rapide	O
(	O
Jupiter	LOCATION
fait	O
un	O
tour	O
sur	O
lui-même	O
en	O
moins	O
de	O
dix	O
heures	O
)	O
,	O
agit	O
comme	O
une	O
immense	O
dynamo	O
.	O
La	O
magnétosphère	O
de	O
la	O
planète	O
correspond	O
à	O
la	O
région	O
où	O
le	O
champ	O
magnétique	O
de	O
Jupiter	LOCATION
est	O
prépondérant	O
.	O
Le	O
vent	O
solaire	O
interagit	O
avec	O
ces	O
régions	O
,	O
allongeant	O
la	O
magnétosphère	O
en	O
direction	O
opposée	O
au	O
Soleil	LOCATION
sur	O
26	O
millions	O
de	O
km	O
,	O
jusqu'	O
à	O
l'	O
orbite	O
de	O
Saturne	LOCATION
.	O
Les	O
quatre	O
lunes	O
principales	O
de	O
Jupiter	LOCATION
sont	O
à	O
l'	O
intérieur	O
de	O
la	O
magnétosphère	O
et	O
donc	O
protégées	O
des	O
vents	O
solaires	O
.	O
Les	O
électrons	O
de	O
ce	O
plasma	O
ionisent	O
le	O
tore	O
de	O
particules	O
neutres	O
provenant	O
de	O
la	O
lune	O
Io	LOCATION
(	O
ainsi	O
que	O
d'	O
Europe	LOCATION
,	O
dans	O
une	O
moindre	O
mesure	O
)	O
.	O
La	O
situation	O
d'	O
Io	LOCATION
,	O
à	O
l'	O
intérieur	O
d'	O
une	O
des	O
plus	O
intenses	O
ceinture	O
de	O
rayonnement	O
de	O
Jupiter	LOCATION
,	O
a	O
interdit	O
un	O
survol	O
prolongé	O
du	O
satellite	O
par	O
la	O
sonde	MISC
Galileo	MISC
qui	O
a	O
dû	O
se	O
contenter	O
de	O
6	O
survols	O
rapides	O
de	O
la	O
lune	O
galiléenne	O
entre	O
1999	O
et	O
2002	O
,	O
en	O
se	O
gardant	O
de	O
pénétrer	O
au	O
sein	O
du	O
tore	O
de	O
particules	O
englobant	O
l'	O
orbite	O
du	O
satellite	O
,	O
particules	O
qui	O
auraient	O
été	O
fatales	O
au	O
fonctionnement	O
de	O
la	O
sonde	O
.	O
Les	O
lignes	O
de	O
champ	O
magnétique	O
entrainent	O
des	O
particules	O
à	O
très	O
haute	O
énergie	O
vers	O
les	O
régions	O
polaires	O
de	O
Jupiter	LOCATION
.	O
La	O
distance	O
moyenne	O
entre	O
Jupiter	LOCATION
et	O
le	O
Soleil	LOCATION
est	O
de	O
778000	O
000	O
km	O
et	O
la	O
planète	O
boucle	O
une	O
orbite	O
en	O
11,86	O
ans	O
.	O
Du	O
fait	O
d'	O
une	O
excentricité	O
de	O
0,048	O
,	O
la	O
distance	O
entre	O
Jupiter	LOCATION
et	O
le	LOCATION
Soleil	LOCATION
varie	O
de	O
75000	O
000	O
km	O
entre	O
le	O
périhélie	O
et	O
l'	O
aphélie	O
,	O
.	O
L'	O
inclinaison	O
de	O
l'	O
axe	O
de	O
Jupiter	LOCATION
est	O
relativement	O
faible	O
:	O
seulement	O
3,13°	O
.	O
Jupiter	LOCATION
n'	O
étant	O
pas	O
un	O
corps	O
solide	O
,	O
sa	O
haute	O
atmosphère	O
subit	O
un	O
processus	O
de	O
rotation	O
différentielle	O
.	O
En	O
juin	O
2010	O
,	O
on	O
connaissait	O
62	O
satellites	O
naturels	O
de	O
Jupiter	LOCATION
.	O
Quatre	O
sont	O
de	O
grands	O
satellites	O
,	O
connus	O
depuis	O
plusieurs	O
siècles	O
et	O
regroupés	O
sous	O
la	O
dénomination	O
de	O
"	O
lunes	O
galiléennes	O
"	O
:	O
Io	LOCATION
,	O
Europe	LOCATION
,	O
Ganymède	LOCATION
et	O
Callisto	LOCATION
.	O
Les	O
16	O
satellites	O
principaux	O
ont	O
été	O
nommés	O
d'	O
après	O
les	O
conquêtes	O
amoureuses	O
de	O
Zeus	PERSON
,	O
l'	O
équivalent	O
grec	O
du	O
dieu	O
romain	O
Jupiter	PERSON
.	O
En	O
1610	O
,	O
Galilée	PERSON
découvrit	O
les	O
quatre	O
plus	O
importants	O
satellites	LOCATION
de	LOCATION
Jupiter	LOCATION
,	O
les	O
lunes	O
galiléennes	O
.	O
C'	O
était	O
la	O
première	O
observation	O
de	O
lunes	O
autres	O
que	O
celle	O
de	O
la	O
Terre	LOCATION
.	O
Ganymède	LOCATION
,	O
avec	O
ses	O
5262	O
km	O
de	O
diamètre	O
,	O
est	O
le	O
plus	O
gros	O
satellite	O
du	O
système	O
solaire	O
.	O
Callisto	LOCATION
,	O
4821	O
km	O
de	O
diamètre	O
,	O
est	O
à	O
peu	O
de	O
choses	O
près	O
aussi	O
grand	O
que	O
Mercure	LOCATION
.	O
Io	LOCATION
et	O
Europe	LOCATION
ont	O
une	O
taille	O
similaire	O
à	O
celle	O
de	O
la	O
Lune	LOCATION
.	O
Trois	O
de	O
ces	O
quatre	O
satellites	O
galiléens	O
sont	O
très	O
rapprochés	O
de	O
Jupiter	LOCATION
:	O
Io	LOCATION
,	O
Europe	LOCATION
et	O
Ganymède	LOCATION
.	O
Les	O
orbites	O
d'	O
Io	LOCATION
,	O
Europe	LOCATION
et	O
Ganymède	LOCATION
sont	O
en	O
résonance	O
orbitale	O
.	O
Quand	O
Ganymède	LOCATION
tourne	O
une	O
fois	O
autour	O
de	O
Jupiter	LOCATION
,	O
Europe	LOCATION
tourne	O
exactement	O
deux	O
fois	O
et	O
Io	LOCATION
quatre	O
fois	O
.	O
En	O
revanche	O
,	O
les	O
forces	O
de	O
marées	O
de	O
Jupiter	LOCATION
tendent	O
à	O
rendre	O
leurs	O
orbites	O
circulaires	O
.	O
En	O
particulier	O
,	O
Io	LOCATION
présente	O
une	O
activité	O
volcanique	O
intense	O
et	O
Europe	LOCATION
un	O
remodelage	O
constant	O
de	O
sa	O
surface	O
.	O
Depuis	O
au	O
moins	O
mi-2007	O
,	O
on	O
pense	O
que	O
les	O
satellites	LOCATION
de	LOCATION
Jupiter	LOCATION
peuvent	O
être	O
regroupés	O
en	O
plusieurs	O
groupes	O
principaux	O
,	O
sur	O
la	O
base	O
de	O
leurs	O
éléments	O
orbitaux	O
,	O
mais	O
certains	O
groupes	O
sont	O
plus	O
frappants	O
que	O
d'	O
autres	O
.	O
Une	O
subdivision	O
de	O
base	O
consiste	O
à	O
regrouper	O
les	O
huit	O
satellites	O
intérieurs	O
,	O
de	O
tailles	O
très	O
diverses	O
mais	O
possédant	O
des	O
orbites	O
circulaires	O
très	O
faiblement	O
inclinées	O
par	O
rapport	O
à	O
l'	O
équateur	O
de	O
Jupiter	LOCATION
et	O
dont	O
on	O
pense	O
qu'	O
ils	O
se	O
sont	O
formés	O
en	O
même	O
temps	O
que	O
la	O
géante	O
gazeuse	O
.	O
Avec	O
celle	O
du	O
Soleil	LOCATION
,	O
l'	O
influence	O
gravitationnelle	O
de	O
Jupiter	LOCATION
a	O
modelé	O
le	O
système	O
solaire	O
.	O
Les	O
orbites	O
de	O
la	O
plupart	O
des	O
planètes	O
sont	O
plus	O
proches	O
du	O
plan	O
orbital	O
de	O
Jupiter	LOCATION
que	O
du	O
plan	O
équatorial	O
du	O
Soleil	LOCATION
(	O
Mercure	LOCATION
est	O
la	O
seule	O
qui	O
fasse	O
exception	O
)	O
.	O
La	O
majorité	O
des	O
comètes	O
de	O
courte	O
période	O
possèdent	O
un	O
demi-grand	O
axe	O
plus	O
petit	O
que	O
celui	O
de	O
Jupiter	LOCATION
.	O
Lors	O
d'	O
approches	O
de	O
Jupiter	LOCATION
,	O
leur	O
orbite	O
aurait	O
été	O
perturbée	O
vers	O
une	O
période	O
plus	O
courte	O
,	O
puis	O
rendue	O
circulaire	O
par	O
interaction	O
gravitationnelle	O
régulière	O
du	O
Soleil	LOCATION
et	O
de	O
Jupiter	LOCATION
.	O
Par	O
ailleurs	O
,	O
Jupiter	LOCATION
est	O
la	O
planète	O
qui	O
reçoit	O
le	O
plus	O
fréquemment	O
des	O
impacts	O
cométaires	O
.	O
Il	O
s'	O
agit	O
de	O
petits	O
corps	O
célestes	O
qui	O
ont	O
la	O
même	O
orbite	O
mais	O
sont	O
situés	O
à	O
60°	O
en	O
avance	O
ou	O
en	O
retard	O
par	O
rapport	O
à	O
Jupiter	LOCATION
.	O
Jupiter	LOCATION
est	O
visible	O
à	O
l'	O
œil	O
nu	O
la	O
nuit	O
et	O
est	O
connue	O
depuis	O
l'	O
Antiquité	MISC
.	O
Pour	O
les	O
Babyloniens	LOCATION
,	O
elle	O
représentait	O
le	O
dieu	O
Marduk	PERSON
;	O
ils	O
utilisèrent	O
les	O
douze	O
années	O
de	O
l'	O
orbite	O
jovienne	O
le	O
long	O
de	O
l'	O
écliptique	O
pour	O
définir	O
le	O
zodiaque	O
.	O
Le	O
symbole	O
astronomique	O
de	O
Jupiter	LOCATION
est	O
une	O
représentation	O
stylisée	O
d'	O
un	O
éclair	O
du	O
dieu	O
.	O
Le	O
nom	O
"	O
jeudi	O
"	O
est	O
étymologiquement	O
le	O
"	O
jour	O
de	O
Jupiter	LOCATION
"	O
.	O
En	O
japonais	O
,	O
ceci	O
se	O
retrouve	O
également	O
:	O
le	O
jeudi	O
se	O
dit	O
mokuyōbi	O
(	O
木曜日	O
,	O
mokuyōbi	O
?	O
)	O
en	O
référence	O
à	O
l'	O
étoile	O
Jupiter	LOCATION
,	O
mokusei	O
(	O
木星	O
,	O
mokusei	O
?	O
)	O
.	O
En	O
janvier	O
1610	O
,	O
Galilée	PERSON
découvre	O
les	O
quatre	O
satellites	O
qui	O
portent	O
son	O
nom	O
en	O
braquant	O
sa	O
lunette	O
vers	O
la	O
planète	O
.	O
Heinrich	PERSON
Schwabe	PERSON
en	O
produit	O
le	O
premier	O
dessin	O
détaillé	O
connu	O
en	O
1831	O
.	O
Ole	PERSON
Christensen	PERSON
Rømer	PERSON
en	O
déduit	O
que	O
l'	O
observation	O
n'	O
était	O
pas	O
instantanée	O
et	O
effectua	O
en	O
1676	O
une	O
première	O
estimation	O
de	O
la	O
vitesse	O
de	O
la	O
lumière	O
.	O
Entre	O
le	O
16	O
juillet	O
et	O
le	O
22	O
juillet	O
1994	O
,	O
l'	O
impact	O
de	O
la	O
comète	O
Shoemaker-Levy	LOCATION
9	LOCATION
sur	O
Jupiter	LOCATION
permet	O
de	O
recueillir	O
de	O
nombreuses	O
nouvelles	O
données	O
sur	O
la	O
composition	O
atmosphérique	O
de	O
la	O
planète	O
.	O
Plus	O
de	O
20	O
fragments	O
de	O
la	O
comète	O
sont	O
entrés	O
en	O
collision	O
avec	O
l'	O
hémisphère	O
sud	O
de	O
Jupiter	LOCATION
,	O
fournissant	O
la	O
première	O
observation	O
directe	O
d'	O
une	O
collision	O
entre	O
deux	O
objets	O
du	O
système	O
solaire	O
.	O
À	O
partir	O
de	O
1973	O
,	O
plusieurs	O
sondes	O
spatiales	O
ont	O
effectué	O
des	O
manœuvres	O
de	O
survol	O
qui	O
les	O
ont	O
placées	O
à	O
portée	O
d'	O
observation	O
de	O
Jupiter	LOCATION
.	O
Les	O
missions	O
Pioneer	MISC
10	MISC
et	O
Pioneer	MISC
11	MISC
obtinrent	O
les	O
premières	O
images	O
rapprochées	O
de	O
l'	O
atmosphère	O
de	O
Jupiter	LOCATION
et	O
de	O
plusieurs	O
de	O
ses	O
lunes	O
.	O
Un	O
tore	O
d'	O
atomes	O
ionisés	O
fut	O
découvert	O
le	O
long	O
de	O
l'	O
orbite	O
de	O
Io	LOCATION
et	O
des	O
volcans	O
furent	O
observés	O
à	O
sa	O
surface	O
.	O
En	O
décembre	O
2000	O
,	O
la	O
sonde	O
Cassini	MISC
,	O
en	O
route	O
pour	O
Saturne	LOCATION
,	O
survola	O
Jupiter	LOCATION
et	O
prit	O
des	O
images	O
en	O
haute	O
résolution	O
de	O
la	O
planète	O
.	O
Le	O
19	O
décembre	O
2000	O
,	O
elle	O
prit	O
une	O
image	O
de	O
faible	O
résolution	O
d'	O
Himalia	LOCATION
,	O
alors	O
trop	O
lointaine	O
pour	O
observer	O
des	O
détails	O
de	O
la	O
surface	O
.	O
La	O
sonde	O
New	MISC
Horizons	MISC
,	O
en	O
route	O
pour	O
Pluton	LOCATION
,	O
survola	O
Jupiter	LOCATION
pour	O
une	O
manœuvre	O
d'	O
assistance	O
gravitationnelle	O
.	O
Le	O
système	O
jovien	O
fut	O
imagé	O
à	O
partir	O
du	O
4	O
septembre	O
2006	O
;	O
les	O
instruments	O
de	O
la	O
sonde	O
affinèrent	O
les	O
éléments	O
orbitaux	O
des	O
lunes	O
internes	O
de	O
Jupiter	LOCATION
,	O
particulièrement	O
Amalthée	LOCATION
.	O
Les	O
caméras	O
de	O
New	MISC
Horizons	MISC
photographièrent	O
des	O
dégagements	O
de	O
plasma	O
par	O
les	O
volcans	O
de	O
Io	LOCATION
et	O
plus	O
généralement	O
des	O
détails	O
des	O
lunes	O
galiléennes	O
,	O
.	O
Galileo	MISC
entra	O
en	O
orbite	O
autour	O
de	O
la	O
planète	O
le	O
7	O
décembre	O
1995	O
,	O
pour	O
une	O
mission	O
d'	O
exploration	O
de	O
près	O
de	O
8	O
années	O
.	O
Elle	O
survola	O
à	O
de	O
nombreuses	O
reprises	O
les	O
satellites	O
galiléens	O
et	O
Amalthée	LOCATION
,	O
apportant	O
des	O
preuves	O
à	O
l'	O
hypothèse	O
d'	O
océans	O
liquides	O
sous	O
la	O
surface	O
d'	O
Europe	LOCATION
et	O
confirmant	O
le	O
volcanisme	O
d'	O
Io	LOCATION
.	O
La	O
sonde	O
fut	O
également	O
témoin	O
de	O
l'	O
impact	O
de	O
la	O
comète	LOCATION
Shoemaker-Levy	LOCATION
9	LOCATION
en	O
1994	O
lors	O
de	O
son	O
approche	O
de	O
Jupiter	LOCATION
.	O
Cependant	O
,	O
bien	O
que	O
les	O
informations	O
récupérées	O
par	O
Galileo	MISC
furent	O
nombreuses	O
,	O
l'	O
échec	O
du	O
déploiement	O
de	O
son	O
antenne	O
radio	O
à	O
grand	O
gain	O
limita	O
les	O
capacités	O
initialement	O
prévues	O
.	O
Galileo	MISC
lâcha	O
une	O
petite	O
sonde	O
à	O
l'	O
intérieur	O
de	O
l'	O
atmosphère	O
jovienne	O
pour	O
en	O
étudier	O
la	O
composition	O
en	O
juillet	O
1995	O
.	O
Un	O
destin	O
que	O
Galileo	MISC
expérimenta	O
de	O
façon	O
plus	O
rapide	O
le	O
21	O
septembre	O
2003	O
,	O
lorsqu'	O
elle	O
fut	O
délibérément	O
projetée	O
dans	O
l'	O
atmosphère	O
jovienne	O
à	O
plus	O
de	O
50	O
km	O
/s	O
,	O
afin	O
d'	O
éviter	O
toute	O
possibilité	O
d'	O
écrasement	O
ultérieur	O
sur	O
Europe	LOCATION
.	O
À	O
cause	O
de	O
la	O
possibilité	O
d'	O
un	O
océan	O
liquide	O
sur	O
Europe	LOCATION
,	O
les	O
lunes	O
glacées	O
de	O
Jupiter	LOCATION
ont	O
éveillé	O
un	O
grand	O
intérêt	O
.	O
Le	O
JIMO	MISC
(	O
Jupiter	MISC
Icy	MISC
Moons	MISC
Orbiter	MISC
)	O
devait	O
être	O
lancé	O
en	O
2015	O
,	O
mais	O
la	O
mission	O
fut	O
estimée	O
trop	O
ambitieuse	O
et	O
son	O
financement	O
fut	O
annulé	O
.	O
À	O
l'	O
œil	O
nu	O
,	O
Jupiter	LOCATION
a	O
l'	O
aspect	O
d'	O
un	O
astre	O
blanc	O
très	O
brillant	O
,	O
puisque	O
de	O
par	O
son	O
albédo	O
élevé	O
,	O
son	O
éclat	O
de	O
magnitude	O
atteint	O
les	O
-2,7	O
en	O
moyenne	O
à	O
l'	O
opposition	O
.	O
Jupiter	LOCATION
est	O
plus	O
brillant	O
que	O
toutes	O
les	O
étoiles	O
et	O
a	O
un	O
aspect	O
similaire	O
à	O
celui	O
de	O
Vénus	LOCATION
,	O
cependant	O
celle-ci	O
ne	O
se	O
voit	O
que	O
quelque	O
temps	O
avant	O
le	O
lever	O
du	O
Soleil	LOCATION
ou	O
quelque	O
temps	O
après	O
son	O
coucher	O
et	O
est	O
l'	O
astre	O
le	O
plus	O
éclatant	O
du	O
ciel	O
.	O
Comme	O
l'	O
a	O
fait	O
Galilée	PERSON
en	O
1610	O
,	O
on	O
peut	O
découvrir	O
quatre	O
petits	O
points	O
blancs	O
qui	O
sont	O
les	O
satellites	O
galiléens	O
.	O
Du	O
fait	O
qu'	O
ils	O
tournent	O
tous	O
assez	O
vite	O
autour	O
de	O
la	O
planète	O
,	O
il	O
est	O
aisé	O
de	O
suivre	O
leurs	O
révolutions	O
:	O
on	O
constate	O
que	O
,	O
d'	O
une	O
nuit	O
à	O
l'	O
autre	O
,	O
Io	LOCATION
fait	O
presque	O
un	O
tour	O
complet	O
.	O
C'	O
est	O
en	O
observant	O
ce	O
mouvement	O
que	O
Roëmer	PERSON
a	O
montré	O
que	O
la	O
lumière	O
voyageait	O
à	O
une	O
vitesse	O
finie	O
.	O
Le	O
meilleur	O
moment	O
pour	O
observer	O
Jupiter	LOCATION
est	O
quand	O
elle	O
est	O
à	O
l'	O
opposition	O
.	O
Jupiter	LOCATION
a	O
atteint	O
le	O
périhélie	O
en	O
mars	O
2001	O
;	O
l'	O
opposition	O
de	O
septembre	O
2010	O
sera	O
donc	O
favorable	O
à	O
son	O
observation	O
.	O
Grâce	O
à	O
sa	O
rapide	O
rotation	O
,	O
toute	O
la	O
surface	O
de	O
Jupiter	LOCATION
est	O
observable	O
en	O
5h	O
.	O
La	O
radioastronomie	O
poussée	O
de	O
Jupiter	LOCATION
est	O
réalisée	O
avec	O
du	O
matériel	O
radioastronomique	O
de	O
réception	O
dans	O
les	O
bandes	O
radios	O
dédiées	O
à	O
la	O
radioastronomie	O
.	O
