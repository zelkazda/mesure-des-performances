Joanne	PERSON
Rowling	PERSON
,	O
OBE	O
,	O
LL.D.	O
(	O
hon	O
.	O
Elle	O
doit	O
sa	O
notoriété	O
mondiale	O
à	O
la	O
série	O
Harry	MISC
Potter	MISC
,	O
dont	O
les	O
tomes	O
traduits	O
en	O
au	O
moins	O
65	O
langues	O
ont	O
été	O
vendus	O
à	O
plus	O
de	O
400	O
millions	O
d'	O
exemplaires	O
.	O
Elle	O
est	O
également	O
l'	O
auteur	O
de	O
Les	MISC
Contes	MISC
de	MISC
Beedle	MISC
le	MISC
barde	MISC
,	O
Le	MISC
Quidditch	MISC
à	MISC
travers	MISC
les	MISC
âges	MISC
et	O
Les	MISC
Animaux	MISC
fantastiques	MISC
,	O
trois	O
livres	O
relatifs	O
à	O
l'	O
univers	MISC
de	MISC
Harry	MISC
Potter	MISC
.	O
Depuis	O
,	O
J.	PERSON
K.	PERSON
Rowling	PERSON
a	O
écrit	O
les	O
six	O
autres	O
livres	O
sur	O
la	O
suite	O
des	O
aventures	O
du	O
jeune	O
sorcier	O
,	O
ce	O
qui	O
lui	O
a	O
permis	O
d'	O
apporter	O
sa	O
contribution	O
aux	O
institutions	O
qui	O
aident	O
à	O
combattre	O
la	O
maladie	O
,	O
l'	O
injustice	O
et	O
la	O
pauvreté	O
.	O
Suite	O
au	O
succès	O
planétaire	O
de	O
l'	O
heptalogie	O
potterienne	O
,	O
Rowling	PERSON
se	O
trouve	O
à	O
la	O
tête	O
d'	O
une	O
fortune	O
estimée	O
en	O
2008	O
par	O
le	O
Sunday	ORGANIZATION
Times	ORGANIZATION
à	O
560	O
millions	O
de	O
livres	O
(	O
environ	O
590	O
millions	O
d'	O
euros	O
ou	O
825	O
millions	O
de	O
USD	O
)	O
.	O
Elle	O
devient	O
une	O
philanthrope	O
reconnue	O
en	O
œuvrant	O
pour	O
de	O
nombreuses	O
œuvres	O
de	O
charité	O
et	O
en	O
co-fondant	O
notamment	O
le	O
Children	MISC
's	MISC
High	MISC
Level	MISC
Group	MISC
.	O
Elles	O
jouent	O
également	O
avec	O
les	O
autres	O
enfants	O
de	O
leur	O
nouvelle	O
rue	O
,	O
et	O
notamment	O
avec	O
un	O
frère	O
et	O
une	O
soeur	O
dont	O
le	O
nom	O
de	O
famille	O
était	O
Potter	PERSON
.	O
Elle	O
lui	O
dédiera	O
plus	O
tard	O
Harry	MISC
Potter	MISC
et	MISC
la	MISC
Chambre	MISC
des	MISC
Secrets	MISC
,	O
contenant	O
justement	O
cette	O
fameuse	O
Ford	MISC
Anglia	MISC
,	O
son	O
premier	O
souvenir	O
significatif	O
de	O
liberté	O
.	O
L'	O
état	O
d'	O
Anne	MISC
Rowling	PERSON
se	O
dégrade	O
alors	O
lentement	O
mais	O
de	O
façon	O
régulière	O
.	O
En	O
1983	O
,	O
ses	O
études	O
scolaires	O
étant	O
terminées	O
,	O
Rowling	PERSON
fait	O
une	O
demande	O
d'	O
inscription	O
à	O
l'	O
université	ORGANIZATION
d'	ORGANIZATION
Oxford	ORGANIZATION
,	O
mais	O
son	O
dossier	O
est	O
refusé	O
car	O
l'	O
école	O
d'	O
où	O
elle	O
vient	O
ne	O
jouit	O
pas	O
d'	O
une	O
bonne	O
réputation	O
.	O
Elle	O
joue	O
de	O
la	O
guitare	O
et	O
découvre	O
les	O
Smiths	ORGANIZATION
en	O
1984	O
,	O
qui	O
deviendront	O
son	O
groupe	O
préféré	O
.	O
Elle	O
participe	O
à	O
la	O
préparation	O
d'	O
une	O
pièce	O
d'	O
Obaldia	PERSON
avec	O
un	O
professeur	O
et	O
s'	O
occupe	O
des	O
costumes	O
.	O
C'	O
est	O
à	O
cette	O
période	O
qu'	O
elle	O
se	O
consacre	O
à	O
la	O
lecture	O
du	O
Seigneur	MISC
des	MISC
Anneaux	MISC
et	O
ses	O
amis	O
la	O
voit	O
pendant	O
des	O
mois	O
avec	O
l'	O
oeuvre	O
de	O
Tolkien	PERSON
sous	O
le	O
bras	O
.	O
Joanne	PERSON
Rowling	PERSON
passe	O
l'	O
année	O
1985	O
à	O
Paris	LOCATION
pour	O
y	O
améliorer	O
sa	O
maîtrise	O
de	O
la	O
langue	O
française	O
.	O
C'	O
est	O
en	O
1990	O
,	O
lors	O
d'	O
un	O
voyage	O
en	O
train	O
de	O
Manchester	LOCATION
à	O
Londres	LOCATION
qu'	O
elle	O
concocte	O
dans	O
sa	O
tête	O
l'	O
histoire	O
de	O
son	O
jeune	O
héros	O
attendant	O
son	O
train	O
qui	O
le	O
conduira	O
jusqu'	O
à	O
une	O
école	O
de	O
sorcellerie	O
.	O
À	O
son	O
arrivée	O
à	O
la	O
gare	LOCATION
de	LOCATION
King	LOCATION
's	LOCATION
Cross	LOCATION
,	O
beaucoup	O
d'	O
idées	O
avaient	O
déjà	O
pris	O
forme	O
et	O
elle	O
stocke	O
des	O
notes	O
dans	O
des	O
boîtes	O
à	O
chaussures	O
.	O
Les	O
éditions	O
Gallimard	ORGANIZATION
sont	O
les	O
premières	O
à	O
acheter	O
les	O
droits	O
pour	O
une	O
traduction	O
et	O
à	O
publier	O
Harry	MISC
Potter	MISC
en	O
dehors	O
des	O
frontières	O
du	O
Royaume-Uni	LOCATION
.	O
En	O
dépit	O
de	O
son	O
succès	O
,	O
elle	O
mène	O
une	O
vie	O
tranquille	O
dans	O
le	O
Perthshire	LOCATION
et	O
ne	O
donne	O
quasiment	O
pas	O
d'	O
interviews	O
.	O
Rowling	PERSON
se	O
trouve	O
à	O
la	O
tête	O
d'	O
une	O
fortune	O
estimée	O
en	O
2008	O
par	O
le	O
Sunday	ORGANIZATION
Times	ORGANIZATION
à	O
560	O
millions	O
de	O
livres	O
(	O
environ	O
590	O
millions	O
d'	O
euros	O
ou	O
825	O
millions	O
de	O
USD	O
)	O
.	O
Harry	MISC
Potter	MISC
et	MISC
les	MISC
Reliques	MISC
de	MISC
la	MISC
Mort	MISC
,	O
quant	O
à	O
lui	O
,	O
sera	O
adapté	O
en	O
2	O
parties	O
:	O
l'	O
une	O
sortira	O
en	O
novembre	O
2010	O
et	O
l'	O
autre	O
en	O
juillet	O
2011	O
,	O
retardant	O
de	O
ce	O
fait	O
la	O
clôture	O
de	O
la	O
saga	O
qui	O
a	O
été	O
l'	O
évènement	O
de	O
toute	O
une	O
génération	O
.	O
Joanne	PERSON
Rowling	PERSON
a	O
déclaré	O
:	O
"	O
Cela	O
signifie	O
tellement	O
pour	O
les	O
enfants	O
en	O
situation	O
de	O
besoin	O
.	O
En	O
janvier	O
2006	O
,	O
Rowling	PERSON
s'	O
est	O
rendue	O
à	O
Bucarest	LOCATION
pour	O
dénoncer	O
l'	O
utilisation	O
des	O
lits-cages	O
dans	O
les	O
institutions	O
psychiatriques	O
pour	O
enfants	O
.	O
Pour	O
son	O
travail	O
artistique	O
et	O
sa	O
bienfaisance	O
,	O
J.	PERSON
K.	PERSON
Rowling	PERSON
a	O
remporté	O
plusieurs	O
honneurs	O
et	O
distinctions	O
.	O
Le	O
15	O
mai	O
2004	O
,	O
J.	PERSON
K.	PERSON
Rowling	PERSON
dévoile	O
son	O
site	O
officiel	O
.	O
Si	MISC
auparavant	O
le	O
site	O
se	O
contentait	O
de	O
proposer	O
des	O
liens	O
vers	O
les	O
éditeurs	O
,	O
il	O
a	O
depuis	O
nettement	O
évolué	O
,	O
proposant	O
une	O
grande	O
interactivité	O
.	O
L'	O
ingéniosité	O
du	O
visiteur	O
est	O
quelques	O
fois	O
récompensée	O
par	O
un	O
document	O
inédit	O
,	O
par	O
exemple	O
une	O
page	O
d	O
'	O
Harry	MISC
Potter	MISC
à	MISC
l'	MISC
école	MISC
des	MISC
sorciers	MISC
tapée	O
à	O
la	O
machine	O
à	O
écrire	O
de	O
l'	O
époque	O
,	O
pleine	O
de	O
ratures	O
et	O
d'	O
annotations	O
écrites	O
à	O
la	O
main	O
de	O
l'	O
auteur	O
.	O
Dans	O
la	O
section	O
des	O
rumeurs	O
,	O
J.	PERSON
K.	PERSON
Rowling	PERSON
distingue	O
les	O
faits	O
véridiques	O
des	O
mensonges	O
concernant	O
son	O
travail	O
.	O
En	O
effet	O
,	O
quelques	O
similitudes	O
de	O
tempérament	O
sont	O
affichées	O
entre	O
les	O
personnages-animaux	O
de	O
l'	O
oeuvre	O
de	O
Grahame	PERSON
et	O
les	O
personnages	O
humains	O
de	O
Rowling	PERSON
,	O
et	O
il	O
s'	O
agit	O
de	O
son	O
livre	O
pour	O
enfants	O
préféré	O
,	O
qui	O
lui	O
était	O
lu	O
par	O
son	O
père	O
quand	O
elle	O
était	O
enfant	O
.	O
L'	O
influence	O
qui	O
vient	O
de	O
J.	PERSON
R.	PERSON
R.	PERSON
Tolkien	PERSON
et	O
de	O
son	O
ami	O
C.	PERSON
S.	PERSON
Lewis	PERSON
existe	O
,	O
mais	O
est	O
discutée	O
.	O
Dans	O
sa	O
biographie	O
,	O
l'	O
auteur	O
précise	O
qu'	O
elle	O
aimait	O
Le	MISC
Seigneur	MISC
des	MISC
Anneaux	MISC
et	O
Les	MISC
Chroniques	MISC
de	MISC
Narnia	MISC
,	O
bien	O
qu'	O
elle	O
ne	O
les	O
a	O
jamais	O
terminés	O
ni	O
l'	O
un	O
ni	O
l'	O
autre	O
.	O
Selon	O
elle	O
,	O
faire	O
des	O
liens	O
entre	O
l'	O
oeuvre	O
de	O
Tolkien	PERSON
et	O
son	O
travail	O
s'	O
avère	O
un	O
peu	O
rapide	O
et	O
facile	O
.	O
Par	O
ailleurs	O
,	O
J.	PERSON
K.	PERSON
Rowling	PERSON
fait	O
référence	O
à	O
la	O
chrétienté	O
dans	O
ses	O
oeuvres	O
,	O
notamment	O
dans	O
le	O
tome	O
7	O
.	O
Celle	O
des	O
parents	O
de	O
Harry	PERSON
comprend	O
l'	O
inscription	O
"	O
Le	O
dernier	O
ennemi	O
qui	O
sera	O
détruit	O
,	O
c'	O
est	O
la	O
mort.	O
"	O
