En	O
s'	O
installant	O
en	O
Inde	LOCATION
,	O
ils	O
abandonnèrent	O
leur	O
style	O
de	O
vie	O
nomade	O
et	O
se	O
mêlèrent	O
aux	O
populations	O
autochtones	O
du	O
nord	O
de	O
l'	O
Inde	LOCATION
.	O
Un	O
adversaire	O
de	O
la	O
théorie	O
de	O
l'	O
invasion	O
aryenne	O
,	O
Koenraad	PERSON
Elst	PERSON
,	O
répond	O
à	O
cela	O
que	O
considérer	O
une	O
opinion	O
de	O
fait	O
comme	O
fausse	O
en	O
raison	O
de	O
mobiles	O
qu'	O
on	O
prête	O
à	O
ceux	O
qui	O
la	O
soutiennent	O
relève	O
du	O
sophisme	O
génétique	O
;	O
faisant	O
allusion	O
à	O
la	O
faveur	O
dont	O
la	O
théorie	O
de	O
l'	O
invasion	O
aryenne	O
jouissait	O
chez	O
les	O
nazis	O
et	O
dont	O
elle	O
jouit	O
maintenant	O
,	O
en	O
Inde	LOCATION
,	O
chez	O
certains	O
membres	O
des	O
castes	O
supérieures	O
(	O
auxquels	O
elle	O
fournit	O
une	O
justification	O
raciste	O
de	O
leur	O
position	O
dominante	O
)	O
ou	O
chez	O
les	O
marxistes	O
,	O
il	O
ajoute	O
:	O
"	O
si	O
une	O
théorie	O
peut	O
être	O
considérée	O
comme	O
fausse	O
simplement	O
parce	O
qu'	O
elle	O
est	O
utilisée	O
à	O
des	O
fins	O
politiques	O
,	O
il	O
est	O
clair	O
que	O
la	O
théorie	O
de	O
l'	O
invasion	O
aryenne	O
elle-même	O
doit	O
être	O
la	O
théorie	O
la	O
plus	O
fausse	O
du	O
monde	O
:	O
on	O
chercherait	O
en	O
vain	O
une	O
hypothèse	O
historique	O
davantage	O
compromise	O
par	O
diverses	O
utilisations	O
politiques	O
,	O
y	O
compris	O
les	O
plus	O
meurtrières.	O
"	O
Il	O
y	O
a	O
,	O
cependant	O
,	O
la	O
description	O
d'	O
un	O
fleuve	O
,	O
la	O
Sarasvatî	LOCATION
d'	O
une	O
importance	O
considérable	O
.	O
Quelques	O
historiens	O
croient	O
que	O
ce	O
fleuve	O
,	O
comparable	O
à	O
l'	O
Amazone	LOCATION
,	O
qui	O
pouvait	O
avoir	O
jusqu'	O
à	O
huit	O
kilomètres	O
de	O
large	O
,	O
est	O
la	O
Sarasvatî	LOCATION
décrite	O
dans	O
les	O
Vedas	MISC
.	O
Ils	O
mettent	O
également	O
en	O
avant	O
qu'	O
il	O
n'	O
y	O
a	O
aucune	O
mention	O
dans	O
les	O
Vedas	MISC
d'	O
une	O
quelconque	O
migration	O
vers	O
l'	O
Inde	LOCATION
depuis	O
une	O
terre	O
originelle	O
.	O
On	O
trouve	O
un	O
grand	O
nombre	O
de	O
sites	O
archéologiques	O
de	O
la	O
civilisation	LOCATION
de	LOCATION
la	LOCATION
vallée	LOCATION
de	LOCATION
l'	LOCATION
Indus	LOCATION
le	O
long	O
de	O
ce	O
lit	O
asséché	O
,	O
en	O
fait	O
en	O
plus	O
grand	O
nombre	O
que	O
le	O
long	O
de	O
l'	O
Indus	LOCATION
,	O
suggérant	O
que	O
la	O
civilisation	LOCATION
de	LOCATION
la	LOCATION
vallée	LOCATION
de	LOCATION
l'	LOCATION
Indus	LOCATION
aurait	O
pu	O
s'	O
épanouir	O
entre	O
ces	O
deux	O
fleuves	O
.	O
C'	O
est	O
cet	O
assèchement	O
,	O
et	O
non	O
une	O
invasion	O
d'	O
un	O
peuple	O
étranger	O
,	O
qui	O
expliquerait	O
le	O
déclin	O
de	O
la	O
civilisation	LOCATION
de	LOCATION
l'	LOCATION
Indus	LOCATION
.	O
La	O
civilisation	O
de	O
vallée	O
d'	O
Indus	LOCATION
est	O
ainsi	O
identifiée	O
à	O
la	O
civilisation	O
védique	O
.	O
De	O
plus	O
des	O
indices	O
archéologiques	O
,	O
des	O
éléments	O
de	O
la	O
culture	O
des	O
Vedas	MISC
semblent	O
en	O
contradiction	O
avec	O
un	O
style	O
de	O
vie	O
nomade	O
,	O
comme	O
,	O
par	O
exemple	O
,	O
l'	O
utilisation	O
de	O
la	O
métallurgie	O
.	O
Peu	O
d'	O
éléments	O
d'	O
une	O
civilisation	O
aussi	O
manifestement	O
urbaine	O
(	O
par	O
exemple	O
,	O
les	O
structures	O
des	O
temples	O
,	O
système	O
de	O
collecte	O
des	O
eaux	O
usées	O
)	O
sont	O
décrits	O
dans	O
les	O
Vedas	MISC
.	O
Elle	O
ignorait	O
totalement	O
le	O
cheval	O
,	O
alors	O
que	O
cet	O
animal	O
est	O
présent	O
dans	O
les	O
Vedas	MISC
.	O
Une	O
divinité	O
de	O
premier	O
plan	O
des	O
Vedas	MISC
est	O
Indra	PERSON
,	O
et	O
c'	O
est	O
un	O
dieu	O
guerrier	O
,	O
or	O
les	O
hommes	O
de	O
l'	O
Indus	LOCATION
semblent	O
avoir	O
été	O
plutôt	O
pacifiques	O
.	O
Il	O
y	O
a	O
très	O
peu	O
de	O
différence	O
entre	O
le	O
védique	O
(	O
langue	O
des	O
Vedas	MISC
,	O
qui	O
est	O
un	O
sanskrit	O
archaïque	O
)	O
et	O
la	O
langue	O
de	O
l'	O
Avesta	MISC
,	O
le	O
texte	O
iranien	O
le	O
plus	O
ancien	O
.	O
Au	O
contraire	O
,	O
l'	O
Inde	LOCATION
a	O
souvent	O
été	O
envahie	O
par	O
des	O
peuples	O
venant	O
de	O
l'	O
Asie	LOCATION
centrale	LOCATION
,	O
qui	O
étaient	O
attirés	O
par	O
la	O
richesse	O
de	O
sa	O
terre	O
.	O
Il	O
est	O
vaincu	O
par	O
un	O
dieu	O
armé	O
de	O
la	O
foudre	O
,	O
Indra	PERSON
en	O
Inde	LOCATION
ou	O
Perun	PERSON
en	O
Russie	LOCATION
.	O
Des	O
similitudes	O
ont	O
été	O
observées	O
entre	O
un	O
grand	O
texte	O
épique	O
indien	O
,	O
le	O
Mahabharata	MISC
,	O
et	O
l'	O
Iliade	MISC
(	O
cf	O
.	O
Les	O
trois	O
premières	O
castes	O
de	O
l'	O
Inde	LOCATION
,	O
celles	O
des	O
brahmanes	O
,	O
des	O
kshatriya	O
et	O
des	O
vaisya	O
,	O
dont	O
l'	O
existence	O
est	O
attestée	O
dans	O
les	O
Vedas	MISC
(	O
mais	O
qui	O
n'	O
étaient	O
pas	O
alors	O
aussi	O
rigides	O
que	O
maintenant	O
)	O
,	O
correspondent	O
aux	O
divisions	O
de	O
la	O
société	O
observées	O
chez	O
les	O
Celtes	LOCATION
.	O
Ce	O
type	O
de	O
rapprochement	O
à	O
conduit	O
Georges	PERSON
Dumézil	PERSON
à	O
élaborer	O
sa	O
théorie	O
des	O
trois	O
fonctions	O
,	O
système	O
de	O
pensée	O
propre	O
aux	O
peuples	O
indo-européens	O
.	O
En	O
le	O
tuant	O
,	O
Indra	PERSON
a	O
libéré	O
les	O
eaux	O
et	O
a	O
permis	O
au	O
soleil	O
de	O
monter	O
au	O
ciel	O
.	O
Il	O
convient	O
de	O
noter	O
que	O
si	O
la	O
linguistique	O
permet	O
de	O
prouver	O
l'	O
origine	O
extérieure	O
du	O
sanskrit	O
,	O
elle	O
ne	O
permet	O
pas	O
de	O
dire	O
comment	O
les	O
locuteurs	O
du	O
sanskrit	O
sont	O
arrivés	O
en	O
Inde	LOCATION
:	O
de	O
manière	O
progressive	O
ou	O
violente	O
.	O
Mais	O
les	O
fouilles	O
des	O
cités	O
de	O
l'	O
Indus	LOCATION
n'	O
ont	O
révélé	O
aucune	O
trace	O
de	O
destruction	O
.	O
À	O
cette	O
époque	O
,	O
la	O
civilisation	LOCATION
de	LOCATION
la	LOCATION
vallée	LOCATION
de	LOCATION
l'	LOCATION
Indus	LOCATION
sombre	O
dans	O
la	O
décadence	O
,	O
probablement	O
à	O
cause	O
d'	O
inondations	O
,	O
de	O
tremblements	O
de	O
terre	O
,	O
de	O
maladies	O
,	O
du	O
changement	O
du	O
cours	O
du	O
fleuve	O
Sarasvatî	LOCATION
,	O
ou	O
d'	O
une	O
combinaison	O
de	O
ces	O
raisons	O
.	O
Tout	O
laisse	O
à	O
penser	O
que	O
la	O
migration	O
aryenne	O
vers	O
l'	O
Inde	LOCATION
a	O
été	O
lente	O
et	O
progressive	O
,	O
étalée	O
sur	O
de	O
nombreux	O
siècles	O
,	O
un	O
point	O
de	O
vue	O
développé	O
par	O
l'	O
indianiste	O
Max	PERSON
Müller	PERSON
.	O
Elle	O
constitue	O
également	O
dans	O
cette	O
perspective	O
un	O
pan	O
de	O
la	O
démonstration	O
de	O
Gobineau	PERSON
sur	O
la	O
nécessité	O
historique	O
de	O
la	O
ségrégation	O
.	O
