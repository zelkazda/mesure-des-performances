Catherine	PERSON
est	O
la	O
fille	O
cadette	O
du	O
roi	O
Ferdinand	PERSON
II	PERSON
d'	PERSON
Aragon	PERSON
et	O
de	O
la	O
reine	O
Isabelle	PERSON
I	PERSON
re	PERSON
de	PERSON
Castille	PERSON
.	O
Son	O
mariage	O
a	O
pour	O
but	O
de	O
sceller	O
l'	O
alliance	O
diplomatique	O
entre	O
le	O
royaume	LOCATION
d'	LOCATION
Espagne	LOCATION
récemment	O
unifié	O
et	O
celui	O
d'	O
Angleterre	LOCATION
,	O
où	O
la	O
maison	PERSON
Tudor	PERSON
vient	O
de	O
s'	O
emparer	O
du	O
trône	O
.	O
Alors	O
que	O
son	O
bâteau	O
était	O
prêt	O
pour	O
partir	O
en	O
Angleterre	LOCATION
,	O
une	O
tempête	O
fait	O
rage	O
sur	O
les	O
côtes	O
espagnoles	O
,	O
repoussant	O
ainsi	O
le	O
départ	O
de	O
5	O
jours	O
.	O
Lors	O
de	O
son	O
arrivée	O
en	O
Angleterre	LOCATION
,	O
elle	O
avait	O
la	O
consigne	O
de	O
garder	O
son	O
visage	O
voilé	O
en	O
public	O
ce	O
qui	O
perturba	O
le	O
roi	O
Henri	PERSON
VII	PERSON
d'	PERSON
Angleterre	PERSON
quand	O
vint	O
le	O
moment	O
de	O
la	O
rencontrer	O
:	O
la	O
jeune	O
fille	O
était	O
couchée	O
et	O
ne	O
pouvait	O
recevoir	O
le	O
roi	O
mais	O
celui	O
çi	O
insista	O
pour	O
la	O
voir	O
.	O
On	O
réveilla	O
la	O
jeune	O
Catherine	PERSON
pour	O
qu'	O
elle	O
se	O
montre	O
à	O
son	O
futur	O
beau-père	O
mais	O
elle	O
le	O
fit	O
le	O
visage	O
voilé	O
ce	O
qui	O
contribua	O
à	O
irriter	O
le	O
roi	O
qui	O
eut	O
peur	O
de	O
s'	O
être	O
fait	O
dupé	O
.	O
Lorsque	O
Catherine	PERSON
d'	PERSON
Aragon	PERSON
souleva	O
le	O
voile	O
,	O
Henri	PERSON
VII	PERSON
et	O
son	O
fils	O
Arthur	PERSON
parurent	O
enchanté	O
par	O
la	O
beauté	O
et	O
la	O
grâce	O
de	O
la	O
princesse	O
espagnole	O
.	O
Le	O
mariage	O
est	O
célébré	O
à	O
la	O
Cathédrale	LOCATION
Saint-Paul	LOCATION
de	LOCATION
Londres	LOCATION
le	O
14	O
novembre	O
1501	O
.	O
Arthur	PERSON
meurt	O
(	O
peut-être	O
de	O
la	O
suette	O
)	O
le	O
2	O
avril	O
1502	O
,	O
quelques	O
mois	O
seulement	O
après	O
le	O
mariage	O
,	O
tandis	O
que	O
Catherine	PERSON
recouve	O
la	O
santé	O
.	O
Arthur	PERSON
a	O
quinze	O
ans	O
au	O
moment	O
de	O
son	O
décès	O
,	O
il	O
est	O
prétendu	O
par	O
la	O
suite	O
que	O
le	O
mariage	O
n'	O
a	O
pas	O
été	O
consommé	O
,	O
ce	O
que	O
corrobore	O
le	O
témoignage	O
de	O
Catherine	PERSON
elle-même	O
.	O
Devenue	O
veuve	O
,	O
Catherine	PERSON
reste	O
en	O
Angleterre	LOCATION
.	O
Le	O
motif	O
de	O
cette	O
prolongation	O
de	O
séjour	O
est	O
purement	O
politique	O
et	O
financier	O
:	O
Henri	PERSON
VII	PERSON
n'	O
avait	O
nulle	O
intention	O
de	O
restituer	O
la	O
grosse	O
dot	O
de	O
la	O
jeune	O
femme	O
.	O
Un	O
nouveau	O
mariage	O
est	O
arrangé	O
avec	O
Henri	PERSON
,	O
le	O
frère	O
cadet	O
de	O
son	O
premier	O
mari	O
.	O
Pour	O
d'	O
obscures	O
raisons	O
,	O
on	O
sollicite	O
du	O
pape	O
Jules	PERSON
II	PERSON
une	O
dispense	O
de	O
ce	O
constat	O
de	O
virginité	O
.	O
Par	O
une	O
bulle	O
de	O
décembre	O
1503	O
,	O
le	O
pape	O
,	O
compréhensif	O
,	O
déclare	O
Henri	PERSON
et	O
Catherine	PERSON
déliés	O
du	O
"	O
lien	O
d'	O
affinité	O
"	O
.	O
Le	O
remariage	O
,	O
désormais	O
possible	O
,	O
est	O
célébré	O
le	O
11	O
juin	O
1509	O
par	O
l'	O
archevêque	O
William	PERSON
Warham	PERSON
,	O
après	O
le	O
décès	O
d'	O
Henri	PERSON
VII	PERSON
(	O
comme	O
il	O
en	O
avait	O
fait	O
la	O
demande	O
)	O
.	O
Catherine	PERSON
d'	PERSON
Aragon	PERSON
est	O
désormais	O
l'	O
épouse	O
du	O
nouveau	O
roi	O
Henri	PERSON
VIII	PERSON
et	O
ipso	O
facto	O
,	O
reine	O
.	O
En	O
1519	O
,	O
il	O
a	O
un	O
fils	O
de	O
sa	O
maîtresse	O
,	O
Elizabeth	PERSON
Blount	PERSON
,	O
à	O
qui	O
il	O
donne	O
le	O
nom	O
révélateur	O
d'	O
Henry	PERSON
FitzRoy	PERSON
.	O
C'	O
est	O
vraisemblablement	O
vers	O
1525	O
ou	O
1526	O
qu'	O
Henri	PERSON
VIII	PERSON
tombe	O
éperdument	O
amoureux	O
d'	O
une	O
des	O
suivantes	O
de	O
la	O
reine	O
,	O
une	O
certaine	O
Anne	PERSON
Boleyn	PERSON
.	O
En	O
1527	O
le	O
roi	O
toujours	O
privé	O
d'	O
héritier	O
mâle	O
légitime	O
,	O
entame	O
l'	O
interminable	O
procédure	O
en	O
vue	O
d'	O
obtenir	O
l'	O
annulation	O
de	O
son	O
mariage	O
avec	O
Catherine	PERSON
d'	PERSON
Aragon	PERSON
.	O
Le	O
roi	O
épouse	O
secrètement	O
sa	O
maîtresse	O
,	O
Anne	PERSON
Boleyn	PERSON
vers	O
le	O
25	O
janvier	O
1533	O
.	O
Catherine	PERSON
est	O
confinée	O
au	O
château	O
de	O
Kimbolton	LOCATION
(	O
Cambridgeshire	LOCATION
)	O
,	O
où	O
elle	O
meurt	O
le	O
7	O
janvier	O
1536	O
abandonnée	O
de	O
tous	O
.	O
Henry	PERSON
VIII	PERSON
encourage	O
ses	O
sujets	O
à	O
manifester	O
de	O
la	O
joie	O
à	O
la	O
nouvelle	O
de	O
sa	O
mort	O
.	O
La	O
reine	O
Anne	PERSON
Boleyn	PERSON
,	O
qui	O
l'	O
a	O
supplantée	O
,	O
ne	O
lui	O
survit	O
que	O
de	O
quatre	O
mois	O
.	O
Sa	O
fille	O
est	O
également	O
reine	O
sous	O
le	O
nom	O
d'	O
Élisabeth	PERSON
I	PERSON
re	PERSON
et	O
succède	O
directement	O
à	O
la	O
fille	O
de	O
Catherine	PERSON
,	O
Marie	PERSON
.	O
