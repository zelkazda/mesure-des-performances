La	O
superficie	O
moyenne	O
est	O
de	O
698	O
130	O
km²	O
,	O
et	O
771	O
705	O
km²	O
si	O
l'	O
on	O
compte	O
l'	O
Antarctique	LOCATION
.	O
Les	LOCATION
superficies	O
pour	O
les	O
pays	O
et	O
les	O
territoires	O
proviennent	O
du	O
CIA	MISC
World	MISC
Factbook	MISC
,	O
mis	O
à	O
jour	O
le	O
1	O
er	O
novembre	O
2005	O
.	O
La	O
plupart	O
des	O
données	O
suivantes	O
proviennent	O
du	O
World	MISC
Factbook	MISC
de	O
la	O
CIA	ORGANIZATION
.	O
Elle	O
contient	O
également	O
le	O
Kosovo	LOCATION
et	O
Taïwan	LOCATION
,	O
régions	O
fonctionnellement	O
indépendantes	O
mais	O
dont	O
l'	O
indépendance	O
ne	O
fait	O
pas	O
consensus	O
,	O
ainsi	O
que	O
la	O
Palestine	MISC
et	O
le	O
Sahara	LOCATION
occidental	LOCATION
.	O
Les	LOCATION
éventuels	O
désaccords	O
de	O
territoire	O
entre	O
pays	O
sont	O
également	O
indiqués	O
.	O
Les	LOCATION
territoires	O
à	O
statut	O
spécial	O
de	O
certains	O
pays	O
sont	O
mentionnés	O
(	O
territoires	O
d'	O
outre-mer	O
,	O
par	O
exemple	O
)	O
.	O
Les	LOCATION
pays	O
suivants	O
ont	O
revendiqué	O
une	O
partie	O
de	O
ce	O
continent	O
:	O
l'	O
Argentine	LOCATION
(	O
969000	O
km	O
2	O
)	O
,	O
l'	O
Australie	LOCATION
(	O
6120000	O
km	O
2	O
)	O
,	O
le	O
Chili	LOCATION
(	O
1250000	O
km	O
2	O
)	O
,	O
la	O
France	LOCATION
(	O
432000	O
km	O
2	O
)	O
,	O
la	O
Norvège	LOCATION
(	O
2500000	O
km	O
2	O
)	O
,	O
la	O
Nouvelle-Zélande	LOCATION
(	O
450000	O
km	O
2	O
)	O
et	O
le	O
Royaume-Uni	LOCATION
(	O
1395000	O
km	O
2	O
)	O
.	O
