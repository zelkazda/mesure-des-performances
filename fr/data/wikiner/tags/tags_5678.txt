Une	O
fois	O
ramené	O
à	O
l'	O
hectare	O
cultivé	O
(	O
5kg/ha/an	O
)	O
hors	O
surface	O
en	O
herbe	O
,	O
la	LOCATION
France	LOCATION
serait	O
au	O
quatrième	O
rang	O
européen	O
.	O
En	O
France	LOCATION
,	O
depuis	O
plusieurs	O
années	O
,	O
de	O
nombreux	O
produits	O
phytosanitaires	O
jusqu'	O
alors	O
autorisés	O
(	O
donc	O
considérés	O
comme	O
efficaces	O
et	O
ne	O
présentant	O
pas	O
de	O
risque	O
inacceptable	O
)	O
ont	O
été	O
interdits	O
à	O
la	O
mise	O
sur	O
le	O
marché	O
et	O
à	O
l'	O
utilisation	O
.	O
Un	O
exemple	O
typique	O
de	O
changement	O
de	O
classification	O
est	O
celui	O
de	O
l'	O
atrazine	O
,	O
utilisé	O
massivement	O
en	O
France	LOCATION
et	O
dans	O
de	O
nombreux	O
autres	O
pays	O
comme	O
un	O
herbicide	O
d'	O
une	O
grande	O
efficacité	O
pour	O
le	O
désherbage	O
du	O
maïs	O
.	O
L'	O
atrazine	O
(	O
comme	O
toute	O
la	O
famille	O
des	O
triazines	O
)	O
est	O
à	O
présent	O
reconnue	O
comme	O
à	O
l'	O
origine	O
de	O
pollutions	O
majeures	O
des	O
nappes	O
souterraines	O
et	O
des	O
eaux	O
de	O
surface	O
qui	O
sont	O
polluées	O
à	O
50	O
%	O
en	O
France	LOCATION
(	O
par	O
rapport	O
aux	O
normes	O
édictées	O
pour	O
les	O
triazines	O
)	O
.	O
