L'	O
idée	O
de	O
motivation	O
est	O
déjà	O
présente	O
dans	O
la	O
division	O
tripartite	O
de	O
l'	O
âme	O
chez	O
Platon	PERSON
.	O
Kant	PERSON
exprime	O
deux	O
origines	O
de	O
la	O
motivation	O
.	O
Dans	O
son	O
journal	O
,	O
Maine	PERSON
de	PERSON
Biran	PERSON
semble	O
considérer	O
la	O
liberté	O
intérieure	O
comme	O
la	O
caractéristique	O
d'	O
une	O
motivation	O
fondamentale	O
;	O
motivation	O
sans	O
objet	O
particulier	O
mais	O
avec	O
laquelle	O
toutes	O
les	O
autres	O
devraient	O
entretenir	O
des	O
rapports	O
de	O
dépendance	O
ou	O
de	O
conciliation	O
.	O
Dans	O
sa	O
conception	O
de	O
rivalité	O
des	O
motifs	O
d'	O
action	O
,	O
Arthur	PERSON
Schopenhauer	PERSON
qualifie	O
le	O
motif	O
vainqueur	O
comme	O
celui	O
qui	O
répond	O
le	O
mieux	O
au	O
vouloir	O
vivre	O
de	O
la	O
personne	O
.	O
Pour	O
Taylor	PERSON
,	O
la	O
motivation	O
est	O
la	O
conséquence	O
du	O
salaire	O
,	O
et	O
il	O
ne	O
tient	O
pas	O
compte	O
des	O
motivations	O
intrinsèques	O
du	O
salarié	O
,	O
ce	O
qui	O
déshumanise	O
le	O
travail	O
.	O
David	PERSON
McClelland	PERSON
fait	O
ressortir	O
trois	O
types	O
de	O
besoins	O
faisant	O
motivation	O
au	O
travail	O
:	O
Mais	O
nous	O
savons	O
avec	O
Sartre	PERSON
que	O
"	O
l'	O
individu	O
est	O
une	O
abstraction	O
"	O
,	O
et	O
que	O
la	O
culture	O
est	O
présente	O
au	O
cœur	O
de	O
l'	O
homme	O
,	O
via	O
la	O
communication	O
,	O
l'	O
éducation	O
,	O
les	O
associations	O
.	O
Skinner	PERSON
a	O
illustré	O
cela	O
avec	O
sa	O
boîte	O
,	O
où	O
des	O
rats	O
étaient	O
directement	O
stimulés	O
au	O
niveau	O
de	O
ces	O
centres	O
nerveux	O
.	O
"	O
La	O
perspective	O
psychodynamique	O
cherche	O
à	O
découvrir	O
les	O
motifs	O
et	O
les	O
influences	O
inconscientes	O
qui	O
s'	O
organisent	O
autour	O
des	O
pulsions	O
sexuelles	O
et	O
agressives	O
pour	O
orienter	O
le	O
comportement	O
(	O
Freud	PERSON
1915	O
)	O
"	O
.	O
L'	O
une	O
des	O
études	O
les	O
plus	O
connues	O
est	O
celle	O
d'	O
Abraham	PERSON
Maslow	PERSON
avec	O
sa	O
célèbre	O
pyramide	O
des	O
besoins	O
.	O
L'	O
autocontrôle	O
est	O
souvent	O
en	O
contraste	O
avec	O
le	O
processus	O
automatique	O
de	O
stimulus-réponse	O
,	O
comme	O
dans	O
le	O
paradigme	O
du	O
comportement	O
de	O
B.F.	PERSON
Skinner	PERSON
.	O
