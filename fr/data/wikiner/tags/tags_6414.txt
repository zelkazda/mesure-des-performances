La	O
province	LOCATION
de	LOCATION
Hainaut	LOCATION
,	O
couramment	O
appelée	O
le	O
Hainaut	LOCATION
,	O
est	O
une	O
province	O
de	O
Belgique	LOCATION
située	O
en	O
Région	LOCATION
wallonne	LOCATION
.	O
Elle	O
est	O
située	O
à	O
l'	O
ouest	O
de	O
la	O
Belgique	LOCATION
et	O
est	O
placée	O
sous	O
la	O
tutelle	O
de	O
la	O
Région	LOCATION
wallonne	LOCATION
.	O
Ce	O
département	O
doit	O
son	O
nom	O
à	O
la	O
victoire	O
des	O
révolutionnaires	O
contre	O
les	O
troupes	O
impériales	O
autrichiennes	O
à	O
la	O
Bataille	MISC
de	MISC
Jemappes	MISC
en	O
1792	O
.	O
Ses	O
limites	O
ne	O
furent	O
définitivement	O
établies	O
qu'	O
avec	O
le	O
rattachement	O
de	O
l'	O
arrondissement	LOCATION
de	LOCATION
Mouscron	LOCATION
lors	O
de	O
la	O
fixation	O
de	O
la	O
frontière	O
linguistique	O
en	O
1963	O
.	O
La	O
province	O
a	O
pour	O
chef-lieu	O
Mons	LOCATION
.	O
Sa	O
superficie	O
est	O
de	O
3	O
786	O
km²	O
,	O
pour	O
1	O
290	O
079	O
habitants	O
ce	O
qui	O
fait	O
d'	O
elle	O
la	O
province	O
la	O
plus	O
peuplée	O
de	O
la	O
Région	LOCATION
Wallonne	LOCATION
.	O
Outre	O
Mons	LOCATION
,	O
ses	O
principales	O
villes	O
sont	O
Charleroi	LOCATION
,	O
La	LOCATION
Louvière	LOCATION
,	O
Mouscron	LOCATION
et	O
Tournai	LOCATION
.	O
L'	O
altitude	O
de	O
la	O
province	O
est	O
comprise	O
entre	O
10	O
mètres	O
(	O
Celles	LOCATION
)	O
et	O
385	O
mètres	O
(	O
L'	LOCATION
Escaillère	LOCATION
)	O
.	O
Population	O
de	O
droit	O
au	O
premier	O
juillet	O
de	O
chaque	O
année	O
(	O
source	O
:	O
INS	LOCATION
)	O
:	O
