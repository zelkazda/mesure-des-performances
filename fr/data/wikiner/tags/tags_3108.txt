Il	O
est	O
secrétaire	O
fédéral	O
adjoint	O
du	O
Parti	ORGANIZATION
socialiste	ORGANIZATION
unifié	ORGANIZATION
de	O
Loire-Atlantique	LOCATION
de	O
1973	O
à	O
1974	O
,	O
puis	O
intègre	O
le	O
Parti	ORGANIZATION
socialiste	ORGANIZATION
où	O
il	O
occupe	O
les	O
fonctions	O
de	O
secrétaire	O
de	O
section	O
à	O
Saint-Nazaire	LOCATION
(	O
1975	O
--	O
1977	O
)	O
,	O
membre	O
du	O
comité	O
fédéral	O
de	O
la	O
Loire-Atlantique	LOCATION
(	O
1975-2005	O
)	O
,	O
et	O
du	O
bureau	O
national	O
(	O
2000	O
-2005	O
)	O
.	O
La	O
même	O
année	O
,	O
il	O
entre	O
au	O
Conseil	LOCATION
régional	LOCATION
des	LOCATION
Pays	LOCATION
de	LOCATION
la	LOCATION
Loire	LOCATION
.	O
Il	O
est	O
maître	O
de	O
conférences	O
associé	O
à	O
l'	O
université	ORGANIZATION
Paris	ORGANIZATION
VIII	ORGANIZATION
entre	O
1993	O
et	O
1997	O
,	O
et	O
professeur	O
associé	O
à	O
l'	O
université	LOCATION
de	LOCATION
Nantes	LOCATION
depuis	O
2007	O
.	O
Titulaire	O
d'	O
un	O
DEA	O
de	O
droit	O
et	O
d'	O
un	O
doctorat	O
en	O
droit	O
public	O
,	O
il	O
entre	O
comme	O
avocat	O
associé	O
au	O
cabinet	O
parisien	O
Jacques	PERSON
Barthélémy	PERSON
à	O
Paris	LOCATION
en	O
2009	O
.	O
