Jacques	PERSON
Masdeu-Arus	PERSON
cosigne	O
en	O
2004	O
une	O
proposition	O
de	O
loi	O
tendant	O
à	O
rétablir	O
la	O
peine	O
de	O
mort	O
pour	O
les	O
auteurs	O
d'	O
actes	O
de	O
terrorisme	O
mais	O
a	O
voté	O
en	O
2007	O
le	O
projet	O
de	O
loi	O
constitutionnelle	O
relatif	O
à	O
l'	O
interdiction	O
de	O
la	O
peine	O
de	O
mort	O
.	O
Son	O
ancien	O
1	O
er	O
adjoint	O
,	O
Gilles	PERSON
Forray	PERSON
a	O
été	O
condamné	O
à	O
3	O
ans	O
de	O
prison	O
avec	O
sursis	O
.	O
La	O
Cour	LOCATION
d'	LOCATION
appel	LOCATION
de	LOCATION
Paris	LOCATION
,	O
après	O
l'	O
audience	O
du	O
2	O
au	O
4	O
avril	O
2008	O
,	O
a	O
confirmé	O
leurs	O
peines	O
à	O
l'	O
exception	O
des	O
amendes	O
,	O
divisées	O
par	O
deux	O
de	O
150	O
000	O
à	O
75	O
000	O
euros	O
.	O
Si	O
Gilles	PERSON
Forray	PERSON
a	O
alors	O
démissionné	O
du	O
mandat	O
de	O
conseiller	O
général	O
,	O
Jacques	PERSON
Masdeu-Arus	PERSON
est	O
resté	O
député	O
des	O
Yvelines	LOCATION
.	O
Des	O
élections	O
législatives	O
partielles	O
ont	O
donc	O
été	O
organisées	O
les	O
11	O
et	O
18	O
octobre	O
2009	O
et	O
Jacques	PERSON
Masdeu-Arus	PERSON
s'	O
est	O
retiré	O
de	O
la	O
vie	O
politique	O
.	O
