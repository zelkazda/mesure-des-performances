Il a obtenu le [MASK] [MASK] [MASK] [MASK] en 1927 .
[MASK] [MASK] donne l' impression d' avoir vécu la vie calme et sans surprises d' un professeur de philosophie .
Sa participation à la création de la [MASK] [MASK] [MASK] [MASK] [MASK] , ancêtre de l' [MASK] , en 1921 peut illustrer l' importance qu' il accordait à l' éducation pour favoriser la paix internationale .
Mais sa participation à la création de la [MASK] [MASK] [MASK] date du moment où il a été le délégué de [MASK] [MASK] pour négocier avec les [MASK] et leur président [MASK] [MASK] pour que ceux-ci s' interposent contre la triplice lors de la [MASK] [MASK] [MASK] .
En 2008 , on a pu déterminer toute l' influence que [MASK] aura eue sur les 14 résolutions proposées par [MASK] afin de créer une instance gouvernementale internationale pour prévenir les conflits armés .
Sa famille vécut à [MASK] quelques années après sa naissance , et il se familiarisa très tôt à l' anglais avec sa mère .
Après quelques hésitations à propos de sa carrière , balançant entre les sciences et les humanités , il opta finalement pour ces dernières , et entra à l' [MASK] [MASK] [MASK] l' année de ses dix-neuf ans dans la promotion d' [MASK] [MASK] , de [MASK] [MASK] et de son ami [MASK] [MASK] .
Deux ans plus tard , il fut muté au lycée [MASK] [MASK] de [MASK] .
Il publia en 1884 des morceaux choisis de [MASK] , accompagnés d' une étude critique du texte et de la philosophie du poète , ouvrage plusieurs fois réédité .
En parallèle à son enseignement , [MASK] trouva le temps de mener des travaux personnels .
Il eut en 1891-1892 [MASK] [MASK] pour élève .
[MASK] a consacré des années de recherches pour la préparation de chacun de ses ouvrages principaux .
En 1898 , [MASK] devint maître de conférence à l' [MASK] [MASK] [MASK] , et obtint ensuite le titre de professeur la même année .
En 1900 , il fut nommé professeur au [MASK] [MASK] [MASK] , où il accepta la chaire de philosophie grecque , en remplacement de [MASK] [MASK] .
Au premier congrès international de philosophie , qui se tint à [MASK] les cinq premiers jours d' août 1900 , [MASK] fit une courte mais importante conférence : Sur les origines psychologiques de notre croyance à la loi de causalité .
En 1901 , [MASK] [MASK] publia [MASK] [MASK] , une des productions " mineures " de [MASK] .
Son étude est essentielle pour comprendre la vision de [MASK] sur la vie , et ses passages traitant de la place de l' art dans la vie sont remarquables .
Après la parution de ce livre , la popularité de [MASK] augmenta considérablement , non seulement dans les cercles académiques , mais aussi dans le grand public .
[MASK] se rendit à [MASK] en 1908 et rendit visite à [MASK] [MASK] , philosophe américain de [MASK] plus vieux que [MASK] de 17 ans , et qui fut l' un des premiers à attirer l' attention du public anglo-américain sur ses travaux .
Ce fut une entrevue intéressante et nous retrouvons les impressions de [MASK] dans une de ses lettres du 4 octobre 1908 : " C' est un homme si modeste , mais quel génie intellectuellement !
Il y exprime sa sympathie pour l' originalité du travail de [MASK] et sa " grandeur d' âme " , mais apporte d' importantes réserves .
Dans ce texte , on appréciera le don de [MASK] pour exposer ses idées de façon claire et brève , et ces deux leçons peuvent servir d' introduction à ses ouvrages plus importants .
En 1913 , il visita les [MASK] à l' invitation de la [MASK] [MASK] de [MASK] [MASK] et donna des conférences dans plusieurs villes américaines , où il était reçu par un très large public .
Malgré cet engagement public en faveur de la métapsychique , [MASK] n' a cessé d' avancer de façon discrète dans un domaine dont il était à la fois un des observateurs avisés et le théoricien caché selon [MASK] [MASK] .
Des mouvements religieux libéraux qualifiés de modernistes ou néo-catholiques tentèrent de s' approprier les thèses de [MASK] .
La seconde fut annulée à cause de la [MASK] [MASK] [MASK] .
[MASK] ne resta pas silencieux pendant le conflit .
[MASK] fit un grand nombre de voyages et de conférences aux [MASK] pendant la guerre .
Cet ouvrage est fort utile pour présenter le concept de force mentale de [MASK] .
Pour lui permettre de se consacrer à ses travaux sur l' éthique , la religion et la sociologie , [MASK] fut dispensé d' assurer les cours liés à la chaire de philosophie moderne au [MASK] [MASK] [MASK] .
En 1921 , il devient le premier président de la nouvelle [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] , la future [MASK] dès 1946 ) qui a pour fonction de promouvoir les conditions favorables à la paix internationale .
La [MASK] rassemble en son sein plusieurs intellectuels du monde entier .
À demi paralysé , il ne put se rendre à [MASK] pour recevoir son prix .
Il fut accueilli avec respect par le public et la communauté philosophique , mais tous à cette époque réalisaient que la grande période de [MASK] était finie .
Sur un pilier du [MASK] [MASK] [MASK] , une inscription honore le philosophe .
La pensée de [MASK] est grandement influencée par [MASK] et par [MASK] , ce dernier se trouvant être la plupart du temps son " adversaire " .
On y trouve aussi l' influence de penseurs qui lui étaient contemporains : [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , et de nombreuses autres sources scientifiques , artistiques , philosophiques ou mystiques , notamment celle exprimée par [MASK] .
Sans remettre en cause la légitimité de l' opération scientifique d' abstraction du concept du temps , [MASK] nous avertit que nous substituons inconsciemment la conception d' une durée vécue , par une conception entachée de cette notion de temps scientifique : nous avons en quelque sorte " spatialisé le temps " .
La spatialisation du temps est la source de faux problèmes qui s' introduisent dans le débat philosophique , et dont l' exemple type est la question de [MASK] : " pourquoi y a-t-il quelque chose plutôt que rien ?
[MASK] démontre qu' en rendant au temps sa nature continue , ces faux problèmes se résolvent d' eux-mêmes comme autant de mirages .
[MASK] distingue l' intelligence de l' intuition .
[MASK] ouvre ainsi la voie à une métaphysique nouvelle , en affirmant que le réel , dans son origine , est connaissable .
Pour [MASK] , " la conscience est coextensive à la vie " .
[MASK] n' aura de cesse de combattre le parallélisme .
Aujourd'hui , la théorie de la localisation considère certaines fonctions dont la mémoire , les souvenirs ( que [MASK] attribue à l' esprit ) comme des attributs du corps , localisés en des zones du cerveau .
C' est la théorie actuelle de [MASK] [MASK] , qui cherche confirmation à travers des analyses effectuées au moyen de la caméra à positons .
Pour [MASK] , il y a effectivement localisation de zone de traitement de certaines informations , mais en aucun cas elle serait compréhensible par le discours : ce serait comme observer un théâtre de mimes et dire que l' on y comprend tout .
Il faut aussi signaler que pour [MASK] , le concept , produit de l' intelligence , étant toujours suspect , le recours à des images métaphoriques permet de dépasser dans une certaine mesure cette suspicion .