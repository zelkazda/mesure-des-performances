[MASK] [MASK] est le fils du professeur [MASK] [MASK] ( 1882-1978 ) , considéré comme le fondateur de la pédiatrie moderne en [MASK] .
[MASK] [MASK] étudie à [MASK] , au [MASK] [MASK] , puis au [MASK] [MASK] .
Il est reçu , à 22 ans , au concours de l' auditorat au [MASK] [MASK] [MASK] .
Mobilisé en 1939 comme officier de cavalerie , il est fait prisonnier à [MASK] en juin 1940 , mais parvient à s' évader en septembre suivant .
L' année suivante , [MASK] [MASK] le charge auprès du [MASK] [MASK] d' une mission de la réforme de la fonction publique , dans le cadre de laquelle il crée et rédige les statuts l' [MASK] [MASK] [MASK] [MASK] , dont l' idée avait été formulée par [MASK] [MASK] avant-guerre .
Sous la [MASK] [MASK] [MASK] , [MASK] [MASK] adhère tout d' abord à l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , puis au [MASK] [MASK] sur les conseils du [MASK] [MASK] [MASK] .
Battu aux élections législatives de 1946 en [MASK] , il rejoint ensuite le [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
Il s' oppose également à la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , accusant le gouvernement de trahison .
En novembre , à l' occasion des élections législatives qui ont suivi la dissolution de l' [MASK] [MASK] , il tente de se faire élire député en [MASK] .
Ainsi , [MASK] [MASK] prend acte de la fondation par [MASK] [MASK] quelques années auparavant du [MASK] [MASK] [MASK] , un mouvement qui réclame l' autonomie de l' île et la suppression du statut de DOM et qui a organisé quelques jours auparavant de grandes manifestations dans l' île .
Cet état de fait ne sera contesté par [MASK] [MASK] que durant la décennie suivante , mais [MASK] [MASK] est néanmoins élu au [MASK] [MASK] [MASK] [MASK] [MASK] pendant cette période .
Pour justifier la départementalisation de l' île survenue en 1946 et préserver ses habitants de la tentation indépendantiste , il met en œuvre une politique de développement axée sur la gestion de l' urgence démographique et de la misère qu' elle engendre dans laquelle les observateurs ont reconnu l' attention accordée par son père [MASK] aux questions sociales .
Il lutte personnellement pour obtenir de [MASK] la création d' un second lycée dans le sud de l' île , au [MASK] : il n' y en alors qu' un seul à [MASK] pour plusieurs centaines de milliers d' habitants , le [MASK] [MASK] .
Son engagement réunionnais n' empêche pas [MASK] [MASK] de demeurer actif et de se voir confier de nouveaux postes .
Face au président du [MASK] [MASK] [MASK] et à [MASK] [MASK] , qui se réclament tous les deux du gaullisme , il ne recueille que 1,66 % des voix et appelle à voter en faveur de [MASK] [MASK] [MASK] [MASK] le 5 mai 1981 .
Le 24 mars 1988 , il est élu au premier fauteuil de l' [MASK] [MASK] , succédant au prince [MASK] [MASK] [MASK] mort le 19 mars 1987 .
Il est enterré au cimetière d' [MASK] , commune dont il a été maire de 1966 à 1989 .
Il se réjouit également de la prise de distance de [MASK] [MASK] avec la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) au profit de la bombe nucléaire française .
Il s' opposera encore clairement , encore qu' avec un moindre écho et sans succès , à l' élection au suffrage universel du [MASK] [MASK] , au motif qu' il ne reçoit et ne devrait recevoir aucune délégation de souveraineté .
Toute la carrière politique de [MASK] [MASK] est placée sous le signe de la fidélité au général [MASK] [MASK] , qu' il rejoint à [MASK] en 1943 ; il est véritablement un " compagnon " , pour reprendre le terme utilisé jusqu' aux années 1990 pour désigner les membres du parti gaulliste .
Bien que réputé jacobin , [MASK] [MASK] s' est toujours déclaré , avec insistance , " libéral " .
La nationalisation de l' [MASK] [MASK] [MASK] [MASK] [MASK] et la création des instituts d' études politiques en 1945 est censée corriger cette insuffisance .