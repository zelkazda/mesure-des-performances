En 1755 , il entre chez un apothicaire de [MASK] , y apprend la pharmacie , et passe , l' année suivante , dans la maison d' un de ses parents , qui exerce à [MASK] la même profession .
[MASK] parvient au rang de pharmacien en second , sous les auspices des deux hommes .
Dans une épidémie qui ravage l' armée , et dans tout le cours de la [MASK] [MASK] [MASK] [MASK] , il donne des preuves de ses capacités .
En 1763 , de retour à [MASK] , il suit les cours de [MASK] [MASK] [MASK] , physicien ( 1700-1770 ) , de [MASK] , chimiste et apothicaire ( 1703-1770 ) et de [MASK] , botaniste ( 1699-1777 ) .
En 1772 , les membres de la [MASK] [MASK] [MASK] [MASK] [MASK] planchent pendant de longues semaines sur le sujet et finissent par déclarer que la consommation de la pomme de terre ne présente pas de danger .
Ne baissant pas les bras pour autant , [MASK] , va promouvoir la pomme de terre en organisant des dîners où seront conviés des hôtes prestigieux tels que [MASK] [MASK] ou [MASK] .
[MASK] se tient d' abord à l' écart de l' administration , puis il est chargé de surveiller les salaisons destinées à la [MASK] , en s' occupant parallèlement de la préparation du biscuit de mer .
En 1796 , il est porté sur la liste de l' [MASK] , formé par le nouveau [MASK] .
Il entre à l' [MASK] [MASK] [MASK] en 1795 dans la section d' économie rurale .
Il est inhumé au [MASK] [MASK] [MASK] à [MASK] dans le caveau familial .
Il travaille également sur l' amélioration de la technique des conserves alimentaires par ébullition découverte par [MASK] [MASK] , en 1795 et publiée en 1810 .
C' est ainsi , que grâce à lui la première raffinerie de sucre de betterave mise en service par [MASK] voit le jour en 1801 .