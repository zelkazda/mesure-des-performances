Il est président du [MASK] [MASK] [MASK] [MASK] [MASK] depuis 2004 et député de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] depuis 2002 .
Longtemps élu d' opposition au conseil municipal de [MASK] à partir de 1989 , puis au conseil régional à partir de 1992 , il entre au conseil général en 1994 et il en devient vice-président en 1998 .
En 2003 , il est l' un des très rares qui s' est opposé au projet de nouveau statut pour la [MASK] .
À la tête d' une liste de large union de la gauche , il remporte plus de 100000 voix et devance de 30000 suffrages [MASK] [MASK] , président sortante , qui était en poste depuis 1992 .
En février 2005 , il démissionne de son poste de maire de [MASK] pour se mettre en conformité avec les dispositions législatives en matière de non-cumul de mandats .
Le 1 er juillet 2006 , [MASK] [MASK] annonce sa démission de ce secrétariat national pour protester contre le non-respect par la direction du parti socialiste de son engagement à mieux représenter la " diversité " à travers les investitures pour les législatives de 2007 et en particulier les originaires de l' outre-mer .
[MASK] [MASK] est finalement investie comme candidate .
En août 2006 , il annonce officiellement qu' il soutient [MASK] [MASK] dans la course à l' investiture pour la candidature à l' élection présidentielle de 2007 .
Dès lors , il est nommé à son conseil politique et il occupe les fonctions de responsable et de porte-parole de la campagne de [MASK] [MASK] dans l' outre-mer .
[MASK] [MASK] quitte ses fonctions de secrétaire national .
Il est réelu président du [MASK] [MASK] [MASK] [MASK] le 19 mars 2010 .
Il fait partie du groupe [MASK] , radical , citoyen et divers gauche dont il est l' un des vice-présidents .
[MASK] [MASK] est passionné par l' économie , le droit et la philosophie .
Sous son impulsion , plus d' un tiers du budget de la [MASK] [MASK] est désormais consacré à l' éducation et à la formation .
Parmi les grandes réalisations de son mandat ( 2004-2010 ) à la tête de la [MASK] [MASK] :