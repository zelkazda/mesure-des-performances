Le [MASK] est le plus long cours d' eau d' [MASK] ( après la [MASK] , mais cette dernière prend sa source en [MASK] ) , et le plus long parcourant l' [MASK] [MASK] .
Il prend sa source dans la [MASK] en [MASK] lorsque deux cours d' eau , la [MASK] et le [MASK] , se rencontrent à [MASK] ; c' est à partir de ce point que le cours d' eau prend son nom de [MASK] .
La source précise du [MASK] est celle du [MASK] et donc la [MASK] est le premier affluent important du [MASK] , nommé [MASK] dès leur confluence .
Le [MASK] mesure 2970 km environ , à partir de [MASK] et mesure en fait 3019 km à partir de sa source officielle .
Il se jette dans la [MASK] [MASK] par le [MASK] [MASK] [MASK] situé en [MASK] et en [MASK] , delta qui figure dans la liste du patrimoine mondial de l' [MASK] .
Le [MASK] est depuis des siècles une importante voie fluviale .
Connu dans l' histoire comme une des frontières de l' [MASK] [MASK] , le cours d' eau s' écoule le long des frontières de dix pays : l' [MASK] ( 7,5 % ) , l' [MASK] ( 10,3 % ) , la [MASK] ( 5,8 % ) , la [MASK] ( 11,7 % ) , la [MASK] ( 4,5 % ) , la [MASK] , la [MASK] ( 5,2 % ) , la [MASK] ( 28,9 % ) , la [MASK] ( 1,7 % ) et l' [MASK] ( 3,8 % ) .
Son bassin versant comporte une partie de neuf autres pays : l' [MASK] ( 0,15 % ) , la [MASK] ( 0,09 % ) , la [MASK] ( 0,32 % ) , la [MASK] [MASK] ( 2,6 % ) , la [MASK] ( 2,2 % ) , la [MASK] ( 4,8 % ) , le [MASK] , la [MASK] [MASK] [MASK] et l' [MASK] ( 0,03 % ) .
Cet étymon est un cognat du nom farsi dānu , signifiant " rivière " ou " courant " , désignant aussi le [MASK] .
[MASK] [MASK] [MASK]
Le [MASK] est le seul grand fleuve européen qui s' écoule d' ouest en est .
Il atteint , après un parcours de 2852 kilomètres ( longueur abrégée ) , la [MASK] [MASK] dans la région du [MASK] [MASK] [MASK] ( 4300 km 2 ) , en [MASK] et en [MASK] .
Contrairement aux autres fleuves , les kilomètres du [MASK] sont comptabilisés depuis l' embouchure jusqu' à la source , le point " zéro " officiel étant matérialisé par le phare de [MASK] en bordure de la [MASK] [MASK] .
Ne sont donc pas pris en compte le parcours de la [MASK] , le cours initial du [MASK] , et le parcours principal dans son delta .
Le bassin versant du [MASK] a une superficie de 802266 km 2 .
Une étymologie commune a donné le même nom à deux d' entre eux : le [MASK] .
Les divers affluents du [MASK] présentent une grande hétérogénéité dans leur régime : régime pluvial océanique en [MASK] occidentale , nivo-pluvial de montagne en [MASK] , pluvio-nival de plaine en [MASK] , nival de plaine en [MASK] -- [MASK] .
Le régime pluvio-nival complexe du [MASK] rend compte de ces diverses influences .
Le fleuve est alors sensible à la rétention hivernale et la fusion nivale lui donne à [MASK] un débit minimum en décembre et un maximum en mai ou juin ( pour une moyenne de 1710 m 3 ⋅s -1 ) .
Ces précipitations sont responsables d' inondations catastrophiques , le [MASK] roulant jusqu' à 5 fois son débit habituel : 8000 m 3 ⋅s -1 en juin 1965 et 1970 , 9000 m 3 ⋅s -1 en juillet 1899 .
À [MASK] et à [MASK] , la fonte hivernale de la plaine maintient le maximum d' abondance en mai-juin .
L' apport des eaux de la [MASK] et de la [MASK] rendent les hautes eaux plus précoces , désormais au printemps ( avril-mai ) , et creusent les basses eaux de juin à septembre ( c' est le cas à [MASK] où le débit atteint 5900 m 3 ⋅s -1 ) .
À partir des [MASK] [MASK] [MASK] , le [MASK] devient sensible au régime climatique annonçant celui de la steppe russe et donnant des débits estivaux très bas .
Les hivers rudes liés au climat continental font que le [MASK] charrie des glaces presque tous les ans et se trouve pris à un quelconque point du cours ( les défilés , surtout ) un an sur deux ou trois .
Les principaux dégâts sont enregistrés en [MASK] dont la plaine est régulièrement envahie par les eaux , ce qui a nécessité une politique d' aménagement avec construction de digues et rectification du cours .
Le [MASK] est le seul fleuve alpin qui s' écoule vers le nord en direction de la [MASK] [MASK] [MASK] .
Les gorges de l' actuel [MASK] [MASK] , aujourd'hui dénuées de cours d' eau , sont des restes du lit de cet ancien fleuve qui était nettement plus important que le [MASK] que nous connaissons .
Comme ces grandes quantités d' eau érodent de plus en plus cette roche calcaire , on suppose que le [MASK] supérieur disparaîtra un jour complètement au profit du [MASK] .
On appelle ce phénomène la " disparition du [MASK] " .
Comme ces périodes de sécheresse ont fortement augmenté ces dernières années , une partie de l' eau du [MASK] a été dérivée de cette zone à travers une galerie souterraine .
Le [MASK] est formé de deux ruisseaux descendant de la [MASK] , la [MASK] et la [MASK] .
La [MASK] prend sa source près de [MASK] , à 1078 mètres d' altitude .
Ayant un parcours plus long , sa source , qui ne se situe qu' à cent mètres de la ligne de partage des eaux du bassin du [MASK] , est considérée comme la source géographique du [MASK] .
La [MASK] [MASK] [MASK] a obtenu en 1990 un accès de quelques 300 mètres à la rive gauche du fleuve à [MASK] ( entre [MASK] et [MASK] ) .
Il est classé au patrimoine mondial par l' [MASK] depuis 1991 .
La [MASK] , qui a inauguré en 1984 un canal de 64 km à partir de [MASK] directement vers la [MASK] [MASK] comme raccourci de 400 km , s' inquiète des répercussions sur l' environnement de l' aménagement du [MASK] [MASK] [MASK] par l' [MASK] .
La contribution des différents pays riverains au débit du [MASK] est la suivante : [MASK] ( 22,1 % ) , [MASK] ( 17,6 % ) , [MASK] ( 14,5 % ) , [MASK] ( 11,3 % ) , [MASK] ( 8,8 % ) , [MASK] ( 6,4 % ) , [MASK] ( 4,3 % ) , [MASK] ( 4,3 % ) , [MASK] ( 3,7 % ) , [MASK] ( 3,1 % ) , [MASK] ( 1,9 % ) , [MASK] [MASK] ( 1,2 % ) , [MASK] ( 0,7 % ) .
Dix pays se trouvent en bordure du [MASK] .
Quatre pays ne se situent que sur un seul rivage ( la [MASK] , la [MASK] , la [MASK] et l' [MASK] ) .
La [MASK] manque dans le tableau ci-dessus : environ 550km rive gauche et 580 rive droite .
Les plus grandes villes situées en bordure du fleuve sont [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] ( [MASK] ) , [MASK] et [MASK] .
A [MASK] , c' est d' abord l' [MASK] qui s' écoule par la gauche dans le [MASK] et juste après c' est l' [MASK] par la droite .
L' eau de l' [MASK] qui provient des [MASK] est verte , l' eau du [MASK] est bleue et l' eau de l' [MASK] , qui provient d' une région marécageuse , est noire .
La prédominance de l' eau verte de l' [MASK] une fois les trois cours d' eau réunis est due d' une part à la grande quantité d' eau charriée par l' [MASK] lors de la fonte des neiges ainsi qu' à la grande différence de profondeur de l' [MASK] et du [MASK] ( 1,90 mètre pour le premier contre 6,80 mètres pour le second ) .
En fait l' eau de l' [MASK] " surnage " au-dessus du [MASK] .
L' [MASK] n' a aujourd'hui plus que 350 kilomètres du fleuve sur son territoire , ce qui place ce pays à la sixième place des pays riverains .
Par contre les cours d' eau de presque tout le pays alimentent le [MASK] et donc la [MASK] [MASK] .
Un peu plus de 70 kilomètres après la frontière , le [MASK] traverse [MASK] , la troisième plus grande ville d' [MASK] .
Ensuite , le fleuve passe sur près de 36 kilomètres au milieu d' un des paysages les plus pittoresques de la vallée du [MASK] , la [MASK] , qui s' étend de [MASK] jusqu' à [MASK] .
La ville fut durant des siècles la ville danubienne la plus grande et la plus importante mais de nos jours elle doit partager ce statut avec [MASK] et [MASK] .
La ville tient son nom d' un affluent , la [MASK] , qui rejoint le [MASK] à cet endroit .
Lors de son entrée en [MASK] , le [MASK] marque d' abord la [MASK] [MASK] [MASK] [MASK] puis à 45 kilomètres seulement de [MASK] , il traverse [MASK] , la capitale slovaque , où il est rejoint par la rivière [MASK] .
Finalement , il matérialise encore la frontière entre la [MASK] et la [MASK] .
Les villes situées le long du fleuve en [MASK] sont , en dehors de [MASK] déjà citée , essentiellement [MASK] , un centre peuplé par la minorité hongroise en [MASK] , où le [MASK] , la plus grande rivière slovaque , conflue avec le [MASK] .
En suivant le [MASK] qui forme alors la frontière entre la [MASK] et la [MASK] , la première ville importante rencontrée est [MASK] , au confluent du [MASK] et de la [MASK] .
Près du confluent avec l' [MASK] , près de [MASK] , le [MASK] passe entièrement la frontière et est désormais hongrois sur ses deux rives .
Vient ensuite la " boucle du [MASK] " , près de [MASK] , où le fleuve pivote de 90° vers le sud .
Après avoir parcouru environ 40 kilomètres , le [MASK] traverse la plus grande ville de son périple , [MASK] ( 1,8 million d' habitants ) , la capitale de la [MASK] .
Avec seulement 137 kilomètres , la [MASK] a , après la [MASK] , la plus petite part du [MASK] sur son territoire .
Le fleuve arrive en [MASK] à [MASK] , un port danubien situé au point de rencontre de la [MASK] , de la [MASK] et de la [MASK] .
Ensuite , il sert de frontière naturelle [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
La ville croate la plus importante située au bord du [MASK] est [MASK] , qui a subi des dégâts importants lors de la [MASK] [MASK] [MASK] [MASK] .
Une autre grande ville croate , [MASK] , se situe également à proximité du fleuve , à environ vingt kilomètres du confluent du [MASK] et de la [MASK] , son second plus long affluent .
Au début , la [MASK] ( rive droite ) se partage le [MASK] avec la [MASK] ( rive gauche ) .
Près de [MASK] [MASK] , le [MASK] forme une boucle et traverse alors la [MASK] vers le sud-est en s' éloignant de la frontière croate et se rapprochant de la frontière roumaine .
En aval de cette ville , le fleuve passe [MASK] [MASK] dont les ponts ont été gravement endommagés en 1999 lors de la [MASK] [MASK] [MASK] .
Ceci perturba la navigation sur le [MASK] car le pont ne fut ouvert que trois fois par semaine .
Après avoir parcouru 70 kilomètres supplémentaires , le [MASK] atteint [MASK] , la troisième plus grande ville riveraine du fleuve avec 1,6 million d' habitants .
Le site est habité depuis 7000 ans , ce qui en fait une des plus vieilles cités habitées en permanence sur les berges du [MASK] .
Elle est construite autour du confluent avec la [MASK] et son centre est dominé par l' imposante forteresse [MASK] .
En continuant son parcours à travers la [MASK] , le [MASK] passe par les villes industrielles de [MASK] , où le [MASK] conflue avec le [MASK] , et [MASK] , où la [MASK] se jette dans le [MASK] .
Sur la rive serbe se trouve le [MASK] [MASK] [MASK] [MASK] contenant la [MASK] [MASK] [MASK] .
Au niveau de la [MASK] , le [MASK] marque la frontière entre le nord de ce pays et la [MASK] : c' est la rive droite qui est bulgare .
Le long de cette frontière de 500 kilomètres , il n' existe qu' un seul pont qui relie depuis 1954 la plus grande ville danubienne bulgare , [MASK] , et la ville roumaine de [MASK] .
Pour la [MASK] , le fleuve a , malgré sa longueur en bordure de son territoire , une importance moindre que pour les autres pays riverains .
Comme il s' agit de la seule voie navigable du pays et que celle-ci se trouve de surcroît dans l' extrême nord peu peuplé de son territoire , le [MASK] n' a qu' une importance régionale pour la petite flotte marchande .
Dans la ville de [MASK] le [MASK] atteint son point le plus méridional .
À partir de là , il remonte vers le nord en territoire roumain , et quitte le territoire bulgare juste après [MASK] .
Sur 1075 kilomètres soit environ un tiers de sa longueur totale , le [MASK] est roumain .
Ainsi la [MASK] possède de loin la plus grande part du fleuve .
Au début le fleuve forme la frontière avec la [MASK] et la [MASK] puis , dans la région située entre le [MASK] et la [MASK] , il effectue un virage vers le nord avant de se jeter dans la [MASK] [MASK] après avoir marqué la [MASK] [MASK] [MASK] [MASK] .
Après avoir atteint [MASK] , il traverse la célèbre " percée du [MASK] " et passe à [MASK] [MASK] [MASK] .
Ensuite le [MASK] poursuit son chemin vers l' est où il forme sur 400 kilomètres la frontière avec la [MASK] .
Ce faisant il passe par les villes de [MASK] , [MASK] , [MASK] [MASK] , [MASK] , [MASK] ( située juste en face de la ville bulgare de [MASK] ) , [MASK] , où la rivière [MASK] se jette dans le [MASK] , et [MASK] .
Sur une centaine de kilomètres après [MASK] , le cours du [MASK] sert de frontière entre la [MASK] , la [MASK] ( sur 570 mètres ) et l' [MASK] .
La [MASK] a le plus petit tronçon du [MASK] sur son territoire .
Juste après avoir été rejointe par la rivière [MASK] en aval de [MASK] , la rive gauche du [MASK] devient moldave et le fleuve marque la frontière entre la [MASK] et la [MASK] sur 570 mètres de long , près de [MASK] .
La [MASK] n' avait à l' origine que 340 mètres de rivage , mais en 1999 , l' [MASK] lui céda lors d' un échange de territoires 230 mètres supplémentaires .
Bien qu' elle puisse accéder aux ports roumain de [MASK] et ukrainiens de [MASK] , d' [MASK] et de [MASK] , la [MASK] envisage d' utiliser son accès au [MASK] pour la construction d' un port .
Après la frontière moldave , la rive gauche du [MASK] devient ukrainienne et le fleuve marque la frontière entre la [MASK] et l' [MASK] sur 47 kilomètres .
Après [MASK] , le bras de [MASK] passe entièrement en [MASK] et se déverse quelques kilomètres plus loin dans la [MASK] [MASK] .
Quelques-unes des plus anciennes civilisations européennes se sont implantées dans le bassin du [MASK] .
Parmi les civilisations du néolithique danubien , on trouve notamment les civilisations rubanées du milieu du bassin du [MASK] .
Tant que le fleuve ne gelait pas , cette flotte suffisait à empêcher les [MASK] , les [MASK] et les [MASK] de traverser , car ils n' avaient pas de technologie pour la contrer .
Les [MASK] dominent le fleuve jusqu' à [MASK] [MASK] [MASK] ( 364 -- 375 ) exception faite de quelques années très froides ( 256 à 259 , lorsque les bases et de nombreux bateaux sont pris par surprise ) .
Cette victoire de l' empereur [MASK] sur les [MASK] sous les ordres de [MASK] a permis la création de la province de [MASK] qui fut à nouveau perdue en 271 .
Le fleuve permettait aux [MASK] d' avancer rapidement et dès 1440 ils livraient les premières batailles pour [MASK] située à 2000 kilomètres de l' embouchure du fleuve .
Comme le roi [MASK] [MASK] [MASK] [MASK] fut tué pendant la bataille , la [MASK] fut intégrée à l' [MASK] des [MASK] .
Ainsi fut stoppée l' expansion des [MASK] le long du [MASK] et à partir de la [MASK] [MASK] [MASK] de 1687 , ils perdent peu à peu du terrain et de la puissance .
Le [MASK] joua alors non seulement le rôle d' artère militaire et commerciale mais également de lien politique , culturel et religieux entre l' [MASK] et l' [MASK] .
Après la [MASK] [MASK] [MASK] , une nouvelle réglementation du trafic fluvial qui devait remplacer les accords de [MASK] de 1921 fut élaborée en 1946 .
Tous les pays limitrophes du fleuve ont participé à la conférence de [MASK] de 1948 sauf les pays vaincus , l' [MASK] et l' [MASK] .
Lors de la signature du traité , il fut également signé un avenant qui accepta l' [MASK] au sein de la [MASK] [MASK] [MASK] .
Sur les centaines de kilomètres de son parcours , le [MASK] traverse une multitude de zones climatiques et de paysages différents ce qui explique que la faune et la flore en bordure du fleuve est tout aussi variée .
Malgré de nombreuses interventions humaines parfois très importantes , de nombreuses sections du [MASK] présentent toujours une grande variété biologique , en partie grâce à la mise en place d' espaces protégés dans les zones les plus sensibles .
Au total , ce sont plus de 300 espèces d' oiseaux qui vivent en bordure du [MASK] .
Pour le monde ornithologique , la région la plus importante est le [MASK] [MASK] [MASK] , un carrefour central des routes migratoires en [MASK] et en même temps un point de rencontre entre la faune européenne et la faune asiatique .
On rencontre plus de 150 espèces de poissons dans le [MASK] [MASK] [MASK] comme par exemple l' esturgeon , le béluga européen , la carpe , le silure , la sandre , le brochet et la perche .
Dans les prairies alluviales à bois durs , on peut noter la présence du frêne à feuilles étroites que l' on rencontre en aval de [MASK] ainsi que l' orme et le chêne pédonculé .
Dans le [MASK] même , on trouve des plantes aquatiques rares comme le piège à loup et l' utriculaire .
Comme beaucoup d' autres fleuves , le [MASK] a subi de nombreuses atteintes importantes à son milieu naturel depuis le début de l' ère industrielle .
À côté de la progression de la pollution liée à l' industrie , à l' agriculture , au tourisme et aux eaux usées ainsi qu' à la régularisation par des digues , des barrages , des écluses et des canaux , se sont surtout les grands projets qui perturbent fortement l' écosystème du [MASK] .
Une protection internationale efficace de celui-ci s' avère difficile car ce ne sont pas moins de dix pays , dont certains des plus pauvres d' [MASK] , qui veulent profiter économiquement de leur situation au bord du fleuve .
Pour l' environnement également , la construction du barrage n' est pas restée sans suite , ainsi les esturgeons ne peuvent plus remonter le [MASK] pour frayer .
Dans les accords du 16 septembre 1977 conclus entre la [MASK] et la [MASK] , la construction d' un énorme ensemble de barrages et d' écluses destinés à la production d' énergie a été décidée .
Aucun compromis n' a encore été trouvé et cette situation envenime les relations entre la [MASK] et la [MASK] jusqu' à aujourd'hui .
Comme le [MASK] [MASK] [MASK] est très proche , des produits souillés , surtout en cas d' avarie , pourraient y arriver très rapidement et sans êtres dilués ce qui menace fortement cet écosystème protégé .
Le 27 août 2004 , le chantier du [MASK] [MASK] [MASK] fut rouvert dans la petite ville ukrainienne de [MASK] .
Des organisations de défense de la nature , le gouvernement roumain et le commissaire de l' environnement de l' [MASK] [MASK] ont donc protesté contre ce canal .
L' [MASK] n' a donc pas tenu compte de ces protestations , et le canal a été inauguré le 14 mai 2007 .
Le [MASK] est une importante source d' eau potable pour dix millions de personnes qui vivent le long du cours d' eau .
D' autres villes comme [MASK] ou [MASK] utilisent également encore pour une grande part de l' eau du [MASK] comme eau potable .
En [MASK] en revanche , 99 % de l' eau potable est puisée dans les nappes phréatiques et des sources et seulement très rarement , par exemple pendant des périodes de canicule , dans l' eau du [MASK] .
La même chose vaut pour la [MASK] qui utilise de l' eau des nappes phréatiques à 91 % .
Seules des communes situées sur son rivage en [MASK] , où le fleuve est à nouveau plus propre , utilisent à nouveau de l' eau du [MASK] ( [MASK] [MASK] , [MASK] [MASK] [MASK] ) .
Néanmoins , le [MASK] n' atteignit jamais l' importance qu' il occupe plus en aval en [MASK] car le débit du fleuve est encore faible et donc pauvre en énergie .
En [MASK] , la situation est complètement différente même si la construction de la première centrale hydroélectrique ne remonte qu' à 1953 .
Aujourd'hui , l' [MASK] est le pays en [MASK] , juste après l' [MASK] et la [MASK] , dans lequel l' énergie hydraulique représente la plus grande part de la production énergétique et 20 % de la production totale d' énergie est assurée par les centrales du [MASK] .
Mais cette évolution n' a pas que des côtés positifs : la monoculture de l' énergie hydraulique , qui en [MASK] est essentiellement concentrée le long du [MASK] et ce de la frontière allemande jusqu' à [MASK] ( à l' exclusion de la [MASK] ) , modifie le tracé et la vitesse du débit du cours d' eau et empêche l' inondation périodique des forêts alluviales écologiquement de grande importance .
En [MASK] , l' énergie hydraulique est , avec 16 % de la production totale , la deuxième source énergie la plus importante après le charbon .
L' eau du [MASK] est aussi utilisée pour le refroidissement de deux centrales nucléaires :
Le [MASK] n' est navigable qu' à partir de [MASK] , à presque 500 kilomètres de sa source , sur une distance totale de 2415 km jusqu' à l' embouchure .
Le canal du [MASK] au [MASK] , qui conflue avec celui-ci près de [MASK] , permet également d' effectuer la liaison fluviale entre la [MASK] [MASK] [MASK] , en passant par le [MASK] et le [MASK] et la [MASK] [MASK] .
Le [MASK] figure parmi les plus anciennes et les plus importantes routes commerciales européennes .
De ce fait les bateaux destinés à la navigation sur le [MASK] étaient de construction plus simple et moins nécessiteuse en bois que les radeaux .
Des embarcations plus grandes , avec des longueurs de trente mètres et deux tonnes de charge utile , nommées bateaux ordinaires de [MASK] ou d' [MASK] , étaient néanmoins parfois chargées de marchandises plus onéreuses , comme le vin ou le sel alimentaire , et tirées à contre-courant .
Avec l' apparition des bateaux à vapeur et plus tard des locomotives , la navigation historique sur la [MASK] entama son déclin et c' est vers 1900 que les derniers convois furent tirés à contre-courant le long du fleuve .
En 1812 le premier bateau à vapeur entra en service à [MASK] .
On peut visiter l' un des derniers exemplaires de ce type de bateau à [MASK] .
De telles chaînes ont d' abord été posées sur la ligne [MASK] -- [MASK] mais aussi près de [MASK] et [MASK] .
Tous les pays héritiers de l' [MASK] ont eu une flotte de navires à vapeur et à aubes après 1918 , pour la plupart de construction allemande et autrichienne , quelques-uns de construction tchécoslovaque ou roumaine .
À l' origine , le [MASK] était un fleuve marchand ouvert , utilisable par tout le monde .
120 ans plus tard , le 18 août 1948 , ce droit fut à nouveau entériné lors de la conférence de [MASK] .
La navigation sur le [MASK] est permise aux bateaux de toutes les nationalités à l' exclusion des navires de guerre étrangers .
Le respect de ces règles et la conservation de la navigabilité sont surveillés par la commission internationale du [MASK] .
En plus d' une centaine de bateaux-hôtels qui circulent principalement entre [MASK] , [MASK] et la [MASK] [MASK] , il existe de nombreux bateaux qui font des excursions journalières , en particulier en [MASK] dans la région de [MASK] et en [MASK] dans la région de la [MASK] , ainsi que d' innombrables petites embarcations de plaisance .
En [MASK] , la pêche est encore un peu pratiquée autour des villes de [MASK] et [MASK] mais dans le [MASK] [MASK] [MASK] , elle est encore pratiquée de manière plus intensive .
Le [MASK] est également une région viticole , dans deux pays essentiellement .
En [MASK] , la vigne est cultivée sur presque tout le long du [MASK] entre [MASK] et la frontière sud du pays .
À côté des nombreux centres d' intérêts célèbres situés le long du [MASK] , de nombreux paysages et parc nationaux ( déjà décrits ci-dessus ) sont également importants pour le tourisme .
Il existe aussi de nombreux endroits , en particulier sur le [MASK] supérieur non navigable , où l' on peut pratiquer le canoë , la barque et le pédalo .
Le fleuve est par ailleurs bordé de belles pistes cyclables , en [MASK] et en [MASK] surtout ( [MASK] [MASK] ) .
La navigation de croisière fluviale est aussi très active sur le [MASK] où , en dehors du tronçon très fréquenté de [MASK] à [MASK] , certains bateaux naviguent également de [MASK] jusqu' au delta .