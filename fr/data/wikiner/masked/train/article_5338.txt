[MASK] , qui revenait de sa victoire sur la [MASK] [MASK] , trouva [MASK] , tua le monstre et libéra la jeune fille .
Persée se maria avec [MASK] bien qu' elle fût promise à [MASK] , le frère de [MASK] .
Lors du mariage , une querelle eut lieu entre les deux prétendants et [MASK] fut à son tour changé en pierre grâce à la tête de la [MASK] .
Après sa mort , [MASK] fut placée par [MASK] parmi les constellations , près de [MASK] et de [MASK] .
[MASK] et [MASK] , et dans des temps plus modernes , [MASK] , écrivirent le récit de sa tragédie , et sa mésaventure est le sujet de nombreuses œuvres d' art anciennes .