Le [MASK] [MASK] [MASK] est une région administrative du [MASK] , située sur la rive nord du [MASK] .
Celles-ci stimuleront la croissance démographique et économique de la région qui deviendra un pôle industriel important du [MASK] .
Situé au sud-est du [MASK] et au nord du [MASK] [MASK] , entre le 48° et le 53° de latitude nord et entre le 70° et le 75° de longitude ouest , la région du [MASK] représente la troisième division territoriale en superficie de la province avec ses 95893 km² ( 1.04 % du [MASK] et 6.74 % du [MASK] ) .
Elle couvre un superficie équivalente à la [MASK] [MASK] [MASK] , l' [MASK] ou de plus de trois fois la [MASK] .
Sur une carte , les limites du territoire prennent la forme d' un cerf-volant inversé ( 550 kilomètres du nord au sud et 330 kilomètres d' est en ouest ) et correspondent pratiquement au bassin hydrographique de la [MASK] [MASK] .
Au nord-est on retrouve la [MASK] ; au nord-ouest ; le [MASK] au sud-ouest ; la [MASK] et au sud-est ; la région de la [MASK] .
Cette composition solide , érodé par le temps , a donnée naissance à un relief arrondi et peu abrupt dans la plupart des plateaux qui entourent la vallée encaissée entre deux failles ( le massif des [MASK] [MASK] au nord et l' abrupte d' [MASK] au sud ) dans laquelle on trouve la majeure partie de la population de la région .
Les événements de [MASK] sont une preuve éloquente de l' instabilité des sol de la région .
Le 4 mai 1971 , une partie de ce village situé sur la rive nord de la [MASK] [MASK] , près de [MASK] , s' est effondré dans la rivière suite à un glissement de terrain laissant un cratère de 32 hectares et causant 31 morts .
Les falaises escarpés surplombant la [MASK] [MASK] aurait été formés par une succession d' événements géologiques s' échelonnant sur 900 millions d' années .
La phase finale se serait produite il y a 180 millions d' années créant un fossé d' effondrement appelé graben du [MASK] .
Cette réalisation est le fruit d' une concertation des deux paliers gouvernementaux , fédérale et provinciale , ce qui constitut un précédant au [MASK] en matière de protection de territoire .
L' eau douce recouvre plus de 7.4 % ( 7929 km² ) de la superficie du [MASK] .
Le territoire englobé par la région correspond de très près au bassin hydrographique des affluents de la [MASK] [MASK] .
Avec des ramifications sur 88000 kilomètres carrés , il est le deuxième plus grand bassin affluent du [MASK] [MASK] après la [MASK] [MASK] [MASK] .
La [MASK] [MASK] possède un débit de 1750 m ³/s et peut atteindre une profondeur de 278 mètres dans son fjord .
Des marées sont présentes jusqu' à [MASK] .
Se jetant dans la [MASK] [MASK] par la [MASK] et la [MASK] [MASK] , le [MASK] [MASK] collecte les eaux de 90 % du bassin et avec ses 1041 km2 est le cinquième plus grand lac du [MASK] .
Le [MASK] est l' une des régions habitées les plus au nord de l' écoumène québécois .
La température dans la vallée qui ceinture le [MASK] et le [MASK] [MASK] est toutefois plus clémente que sur les massifs dans lesquels elle est encaissée d' où sont surnom d ' " oasis tempérée en milieu nordique " .
À leur arrivée au [MASK] , les premiers européens explorent une contrée pratiquement à l' état vierge .
Par sa situation entre le [MASK] [MASK] et la [MASK] [MASK] [MASK] et sa faune abondante , le [MASK] est également un point de rencontre important pour la majorité des nations amérindiennes de l' est du [MASK] .
Dès 1526 , les premiers morutiers et baleiniers européens naviguent dans le [MASK] [MASK] [MASK] et jettent l' ancre aux alentours de [MASK] bien avant le premier établissement permanent érigé en 1550 .
Les limites de ce royaume sont décrites à l' époque comme partant du site actuel [MASK] jusqu' à l' [MASK] [MASK] [MASK] et englobant tout l' arrière pays jusqu' au [MASK] [MASK] .
Les deux chemins d' accès vers ces terres de l' intérieur du continent " d' où l' eau sort " sont la [MASK] [MASK] et la [MASK] [MASK] [MASK] .
La force du courant , qui l' empêche de se rendre bien loin , lui laisse croire que la rivière pourrait être un bras de mer vers l' [MASK] [MASK] .
Le monopole d' exploitation de [MASK] et ses alentours est accordé à [MASK] [MASK] [MASK] en 1603 qui charge [MASK] [MASK] [MASK] d' explorer le territoire et de lui rapporter le plus d' informations possibles .
Il retournera en [MASK] le 16 août après avoir exploré le [MASK] [MASK] .
[MASK] [MASK] [MASK] décède la même année et le monopole est accordé à [MASK] [MASK] [MASK] [MASK] jusqu' en 1607 , année où le monopole est levé jusqu' en 1614 .
L' un d' entre eux , [MASK] [MASK] , se rend à [MASK] pour y brûler toutes les barques du port et capturer le plus gros navire .
La colonisation sera perturbée jusqu' en 1632 , année de la reprise du territoire par la [MASK] .
[MASK] sera de plus en plus délaissé au profit de [MASK] après la reprise de la colonisation .
Selon leurs récits , plusieurs sépultures amérindiennes jonchent alors les rives du [MASK] du fait des ravages importants de l' épidémie .
Les missionnaires empruntent cette route jusqu' en 1671 pour venir en aide aux tribus victimes de l' épidémie et de la guerre contre les [MASK] .
La première mention du nom [MASK] remonterait à cette époque .
Malgré l' acquisition des droits par ces industriels , aucun projet de barrage hydroélectrique ne sera concrétisé avant l' arrivée du géant du tabac [MASK] [MASK] [MASK] .
Ce dernier acquiert les droits de la rivière suite à une visite en 1912 du [MASK] en aval de [MASK] jusqu' à [MASK] .
La [MASK] [MASK] [MASK] éclate en 1914 et retarde ses projets de barrages hydroélectriques .
La construction du barrage de l' [MASK] s' échelonne de 1923 à 1925 .
[MASK] devient propriétaire des droits d' exploitation de la [MASK] [MASK] et entreprend , dès le 24 juillet 1925 , la construction de la ville industrielle d' [MASK] .
En 1926 , la compagnie se porte acquéreur de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et des installations portuaires et ferroviaires de [MASK] suite à la liquidation de la [MASK] [MASK] [MASK] [MASK] [MASK] et ses infrastructures .
Le 26 juillet de la même année , les cuves de l' usine d' [MASK] débutent la production d' aluminium .
Alors que le [MASK] bénéficie de l' essor économique apporté par l' implantation d' une nouvelle industrie , le [MASK] [MASK] subit la montée des eaux entrainée par l' inauguration officielle et la fermeture des vannes du barrage de l' [MASK] le 24 juin 1926 .
De son côté , l' aluminerie d' [MASK] réduit sa main-d'œuvre de 60 % et est considérée au bord du gouffre 6 ans après sa construction .
La population du [MASK] est presque totalement concentrée dans l' espace municipalisé autour de la [MASK] [MASK] et du [MASK] [MASK] qui représente 11 % des 95 892.8 km de la région .
En 2008 , on dénombrait 274 919 saguenéens et jeannois majoritairement répartis dans 5 principaux centres urbains , c' est-à-dire [MASK] ( 53 % de la population ) , [MASK] ( 11 % ) , [MASK] ( 5 % ) , [MASK] ( 4 % ) et [MASK] ( 4 % ) .
La région compte pour 3.8 % de la population du [MASK] .
Le premier missionnaire du [MASK] est un récollet du nom de [MASK] [MASK] , il est de passage à [MASK] à l' automne 1615 alors qu' il entreprend de suivre des coureurs des bois innus .
Les couleurs du [MASK] [MASK] [MASK] [MASK] [MASK] représentent des éléments plus ou moins significatifs de la région .
Le [MASK] est la seule région du [MASK] à avoir un drapeau .