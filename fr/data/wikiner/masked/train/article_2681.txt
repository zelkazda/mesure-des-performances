Sa capitale est [MASK] .
Le pays est ainsi parmi les premiers qui gagnent son indépendance dans la zone de l' [MASK] [MASK] et de [MASK] [MASK] .
La distance entre la façade ouest de [MASK] et les côtes du [MASK] est estimée à plus de 700 km .
Elle a pour voisines l' [MASK] [MASK] [MASK] ( 300 km au nord-ouest ) , [MASK] [MASK] ( 800 km à l' est ) , puis l' [MASK] [MASK] ( à l' est ) et les [MASK] ( au nord ) .
La moitié ouest , la plus large et la plus étalée , est occupée par des plaines alluvionnaires à faible déclivité , depuis les hautes terres du centre jusqu' au [MASK] [MASK] [MASK] , tandis qu' à l' est une étroite bande de falaises s' aplanit brusquement en une mince plaine côtière bordée par l' [MASK] [MASK] .
L' originalité de [MASK] , qui a pour emblème l' arbre du voyageur ( ravinala ) , réside dans son extrême diversité : la variété du relief et du climat a favorisé la biodiversité d' une flore et d' une faune caractérisées par un important taux d' endémisme .
[MASK] est découpée en cinq zones climatiques :
Cette origine permet d' expliquer l' existence d' une faune et d' une flore communes à [MASK] et au sud des continents africain et américain , ainsi que des profils géologiques très proches .
Néanmoins , l' isolement de [MASK] au cours des temps géologiques a fait évoluer la faune et la flore de façon unique .
Du fait de son relief , [MASK] réunit une véritable mosaïque de paysages .
L' [MASK] [MASK] [MASK] est généralement considérée comme faisant partie de l' [MASK] , mais a déjà été décrite comme " le huitième continent " .
Très étirée entre l' équateur et le [MASK] [MASK] [MASK] , [MASK] présente une palette de paysages d' une diversité prodigieuse .
En 2003 , [MASK] [MASK] a annoncé qu' il triplerait la superficie des aires protégées de l' île pour atteindre six millions d' hectares .
[MASK] était déjà séparée du continent africain il y a 65 millions d' années , lors de la grande extinction de la fin du secondaire , et la vie y reprit donc de façon locale .
L' isolement biogéographique de [MASK] et la variété des climats et reliefs y ont favorisé le développement d' une faune et d' une flore uniques au monde , en partie endémique ( dont par exemple l' hapalémur gris du [MASK] [MASK] , unique primate au monde à vivre dans des roseaux ) .
Des études récentes affirment même actuellement que le peuple malgache est originaire de l' [MASK] et d' [MASK] .
Les populations de [MASK] ne sont pas traditionnellement des marins , alors que les [MASK] et les [MASK] le sont .
L' évangélisation est assurée tout d' abord par les [MASK] protestants , premiers occupants , puis par les [MASK] , catholiques .
Mais , au fil des incidents diplomatiques , [MASK] [MASK] mène une politique de plus en plus agressive , puis entreprend la conquête de l' île .
La conquête de l' île par [MASK] [MASK] se déroule sans difficultés et presque sans combat .
Finalement , au premier coup de canon sur la capitale [MASK] , la [MASK] [MASK] [MASK] fait hisser le drapeau blanc .
Le maréchal [MASK] [MASK] , à l' époque gouverneur général de [MASK] ( 1896-1905 ) , contribue à pacifier l' île .
La [MASK] [MASK] de [MASK] reste très étroitement liée à [MASK] [MASK] par les accords de coopération .
Le président [MASK] , critiqué par la population pour son soutien aux intérêts français , fait face à une contestation grandissante ( en particulier la grève des étudiants menée de la capitale vers les provinces ) et quitte le pouvoir en 1972 .
Il donne les pleins pouvoirs au général [MASK] [MASK] qui décide d' organiser un référendum afin d' officialiser son pouvoir pour une période transitoire .
Le référendum l' ayant plébiscité , il crée un gouvernement d' union nationale , qu' il dirige jusqu' en 1975 , avant de passer le flambeau au populaire colonel de gendarmerie [MASK] [MASK] .
L' opposition à [MASK] [MASK] s' amplifie .
[MASK] connaît une période de stabilité économique jusqu' en 2001 avec 4,3 % de croissance annuelle moyenne .
En mai 2002 , [MASK] [MASK] est déclaré vainqueur dès le premier tour avec plus de 51 % des voix .
Il décide de faire appel aux réservistes de l' armée pour lancer des expéditions contre les troupes fidèles à [MASK] [MASK] et pour " libérer " les provinces des barrages .
En juillet 2002 , [MASK] , la dernière province où le camp [MASK] s' est retranché est tombé entre les mains de [MASK] .
[MASK] [MASK] prend la fuite avec ses fidèles à bord d' un avion à destination de [MASK] [MASK] .
Cependant , l' [MASK] [MASK] , [MASK] [MASK] et les bailleurs de fonds ne reconnaissent le régime de [MASK] [MASK] qu' en janvier 2003 , suite aux élections législatives remportées par son parti .
[MASK] [MASK] est également pointé du doigt par l' opposition pour avoir " éliminé " les entrepreneurs malgaches performants , accaparé leurs affaires pour se placer lui-même dans tous les secteurs économiques bénéficiaires .
L' information est dévoilée en novembre suite à la publication dans le [MASK] [MASK] relayé par d' autres médias du monde et sème la panique du peuple dans la capitale ainsi que la colère et la peur de l' envahisseur contribuant à porter au pouvoir [MASK] [MASK] qui dénonce l' accord comme anticonstitutionnel en mars 2009 .
Le 26 janvier 2009 , [MASK] [MASK] amène ses partisans dans les rues de la capitale .
Le 31 janvier 2009 , [MASK] [MASK] s' autoproclame " en charge " de la [MASK] [MASK] [MASK] .
Ces changements sont considérés par l' ensemble de la communauté internationale comme un putsch que [MASK] [MASK] est la première à condamner .
En retour , le 23 mars 2009 , une poignée de personnes , se rassemblent dans le centre de la capitale malgache pour dénoncer la prise de pouvoir illégale d' [MASK] [MASK] et réclamer le retour du président [MASK] [MASK] en l' appelant affectueusement " dadanay " .
Cette manifestation est réprimée par l' armée de M. [MASK] [MASK] .
Auparavant , le premier ministre par intérim du régime de transition dirigé par [MASK] [MASK] , le général [MASK] [MASK] , transfère son pouvoir à [MASK] [MASK] .
L' [MASK] et les officiels étrangers sont ballottés .
[MASK] ( [MASK] [MASK] ) achève la liaison de [MASK] au monde par la fibre optique .
La mouvance [MASK] [MASK] a tout fait pour que ces discussions n' aboutissent à rien de concret en changeant d' avis toutes les 24 heures .
C' est la fin des mouvances à [MASK] .
Depuis avril 2007 , [MASK] est officiellement découpée en 22 régions .
Depuis 1972 , l' enseignement national à [MASK] se dissocie du programme de [MASK] [MASK] .
Cette manne fait vivre un peu plus de 30 % de la population mais la valeur de la monnaie nationale s' en retrouve lourdement affectée auprès des organisations de valorisation économique , comme le [MASK] .
La population malgache est la source des peuples des îles alentours comme l' [MASK] [MASK] [MASK] ( [MASK] [MASK] , [MASK] , [MASK] , [MASK] ) et [MASK] [MASK] .
[MASK] possède plusieurs universités .
[MASK] a signé de multiples conventions de protection de l' enfance .
Cette coutume n' est propre qu' à certaines tribus de [MASK] et est aussi une occasion de rassembler la grande famille et une occasion de voir qui sont les personnes qui préservent leur relation avec cette grande famille ( ayant répondu à l' invitation et apporté une contribution habituellement financière ) .
Ses plus proches parentes sont les langues de la branche orientale du sous-groupe dit du [MASK] , comprenant notamment le ma'anyan , le samihim , le dusun deyah , etc .
Ce qui permet de penser que son habitat d' origine est l' actuelle région de [MASK] d' où elle disparut il y a à peine quelques siècles , remplacée par le malais .
En 1984 , le gouvernement de [MASK] a décidé d' élargir l' ouverture du pays au tourisme .
Tout indique que [MASK] peut devenir une destination touristique importante , à condition que les autorités de l' île consacrent à cette activité les efforts d' équipement nécessaires .
[MASK] abonde en gemmes semi-précieuses très variées .
On les trouve facilement au zoma ( marché ) d' [MASK] , polies en " œufs " ou en " boules " .
Les poissons d' eau douce ( brochets , carpes , truites ) et de mer ( requins , thons , espadons ) tiennent également une bonne place dans la cuisine mais c' est la langouste qui reste un des aliments privilégiés de [MASK] .
[MASK] a pour codes :