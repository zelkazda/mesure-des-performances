Il est député du [MASK] de 1997 à 2002 , puis de la [MASK] [MASK] [MASK] [MASK] [MASK] depuis juin 2002 , ayant été réélu en juin 2007 .
Après des études de mathématiques , il devient enseignant-chercheur à l' [MASK] [MASK] [MASK] ( [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ) en 1969 .
En 1973 , il entre aux [MASK] [MASK] [MASK] [MASK] , dont il fonde le groupe rennais en 1977 .
Il devient vice-président de [MASK] [MASK] [MASK] .
Considérant que sa circonscription du [MASK] rendait sa réélection aléatoire [ réf. nécessaire ] , il se parachute dans la 11 e circonscription de [MASK] ( une partie du 14 e arrondissement ) .
Candidat à sa réélection de député en juin 2007 , il l' emporte facilement au second tour face à l' [MASK] [MASK] [MASK] , avec plus de 57 % des suffrages exprimés .
[MASK] [MASK] a déclaré être pour la " grève du troisième ventre " , c' est-à-dire pour la diminution des aides financières au troisième enfant .