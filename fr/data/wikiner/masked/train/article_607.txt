[MASK] était représenté sous les traits d' un personnage aux formes androgynes .
Son ventre est proéminent et gras , et il porte sur la tête un panache de plantes du [MASK] .
L' un portant sur la tête des tiges de papyrus ( symbole de la [MASK] ) et l' autre portant un nénuphar ( symbole de la [MASK] ) .
Selon la légende , [MASK] vivait dans deux lieux cachés .
Le premier se situait sous la première [MASK] [MASK] [MASK] , près d' [MASK] .
De là , il versait le contenu de deux jarres pour faire monter les eaux de [MASK] .