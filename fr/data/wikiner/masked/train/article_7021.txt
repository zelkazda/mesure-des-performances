C' est en 1967 , suite au [MASK] [MASK] [MASK] , que [MASK] [MASK] a supprimé la référence à un nombre de jours ou d' années déterminé .
Les indulgences se calquent alors sur les pénitenciels , ces manuels venus d' [MASK] qui fixent pour chaque type de faute tant de jours de mortification .
Les indulgences sont dénoncées d' abord par [MASK] [MASK] ( 1320-1384 ) et [MASK] [MASK] ( 1369-1415 ) , qui remettent en cause les abus .
Parmi ceux-ci , on peut citer l' indulgence accordée en 1506 pour quiconque aiderait à la construction de la nouvelle [MASK] [MASK] .
Dans le deuxième cas , [MASK] souligne que la repentance seule vaut rémission des peines , sans nul besoin de lettres d' indulgence .
Cependant , la critique pèse surtout sur les abus qui entachent la pratique , et se teinte de gallicanisme : [MASK] accuse cette tarification de ne pas avoir été approuvée par un concile .
Ainsi , [MASK] [MASK] , dans sa condamnation de [MASK] , rappelle la distinction entre rémission de la peine temporelle et rémission du péché à proprement parler .
La principale indulgence est accordée à l' occasion du jubilé , dont elle est l ' " un des éléments constitutifs " selon [MASK] [MASK] .
En 1999 , le [MASK] a signé avec la [MASK] [MASK] [MASK] l' accord luthéro-catholique sur la justification par la foi .