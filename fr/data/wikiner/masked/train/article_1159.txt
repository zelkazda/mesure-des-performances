[MASK] est un des personnages centraux de l' univers de fiction de la série des [MASK] [MASK] [MASK] , de [MASK] [MASK] .
[MASK] [MASK] , le premier illustrateur des couvertures des [MASK] [MASK] [MASK] , le dessina cependant comme un mage âgé aux cheveux blancs et longs .
L' un des huit sorts du livre s' est alors abrité dans sa tête , et [MASK] en a conclu que ce puissant sortilège faisait fuir tous les autres .
Il prononce ce sort dans [MASK] [MASK] [MASK] , et s' en voit enfin débarrassé .
Il sauve ainsi plusieurs fois le [MASK] , la ville d' [MASK] , etc .
[MASK] [MASK] écrit que [MASK] , fataliste , en a fini par conclure qu' à chaque fois que les choses semblent s' améliorer pour lui , une pirouette du destin le précipite dans une nouvelle situation où il risque sa vie .
[MASK] est le personnage principal de nombreux romans du disque .