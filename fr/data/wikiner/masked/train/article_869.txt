Son siège social se situe à [MASK] , près de [MASK] , et ses meilleures ventes sont le système d' exploitation [MASK] et la suite bureautique [MASK] [MASK] .
Si bien qu' un observateur note même que la mission originale de [MASK] d' avoir " un ordinateur sur chaque bureau et dans chaque maison , tournant sur [MASK] " est aujourd'hui pratiquement accomplie .
[MASK] participe aussi dans d' autres secteurs d' activité , comme la chaîne câblée américaine [MASK] , le portail web [MASK] , les périphériques informatiques ( claviers , souris ) , et les produits de divertissement domestique comme la [MASK] et le [MASK] .
L' introduction en bourse de la société , et l' envolée du prix des actions qui s' ensuivit , a fait quatre milliardaires et environ 12000 millionnaires parmi les employés de [MASK] .
Cette entreprise est surtout connue pour ses logiciels , comme les systèmes d' exploitation [MASK] et [MASK] , la suite bureautique [MASK] [MASK] , ses outils de développement , ses jeux vidéo , également pour divers produits matériels ( périphériques pour [MASK] , consoles de jeux [MASK] , baladeur numérique [MASK] ) , et pour ses services [MASK] ( regroupés sous le nom [MASK] [MASK] ) .
[MASK] domine depuis plusieurs années le marché des systèmes d' exploitation grand-public .
Son système d' exploitation [MASK] , régulièrement réédité , s' est imposé comme un standard dans le domaine informatique .
Depuis le 27 mai 2010 , [MASK] est la seconde capitalisations boursières du [MASK] , derrière [MASK] .
La marque [MASK] ( en fait , originalement , [MASK] : le trait d' union disparaîtra plus tard ) fut déposée le 26 novembre 1976 .
À titre indicatif le prix de vente de l' [MASK] [MASK] étant de 397 dollars , la licence de [MASK] en représentait donc 8,8 % .
Avant la sortie de [MASK] en 1981 , [MASK] poursuivit son développement en produisant divers compilateurs de langages de programmation comme [MASK] ou [MASK] .
En 1980 , [MASK] s' apprêtant à lancer l' [MASK] [MASK] , a demandé son [MASK] à [MASK] .
[MASK] a , par ailleurs , demandé à la société [MASK] [MASK] , dirigée par [MASK] [MASK] de lui fournir une version de son système d' exploitation [MASK] .
[MASK] se tourna alors vers [MASK] , et voulut sous-traiter [MASK] pour l' [MASK] [MASK] .
Le contrat avec [MASK] ne le permettant pas , celui-ci dépensa 25000 $ en décembre 1980 pour une licence non exclusive pour un système d' exploitation , disponible à un stade expérimental , clone de [MASK] , le [MASK] .
En mai 1981 , [MASK] engagea [MASK] [MASK] pour porter [MASK] sur l' [MASK] [MASK] .
[MASK] vit ainsi sauvé son projet d' [MASK] [MASK] , mais au prix , qu' elle ignora , de la perte de sa position dominante : cet accord va permettre de réaliser des clones , et surtout , à [MASK] d' empocher des redevances sur le [MASK] pour les correctifs qu' elle y a apportés ( débogage ) .
[MASK] avait détenu jusqu' à 66 % du marché des mainframes propriétaires ; sa part du marché des [MASK] ne dépassa jamais un maximum de 21 % , atteint vers 1983 , puis a décliné pour placer ce constructeur derrière [MASK] et [MASK] ( aujourd'hui intégrée par [MASK] ) , situation devenue marginale , inimaginable en 1981 .
[MASK] a acheté pour 50000 dollars le logiciel qui va ériger son empire , même si elle a dû en compléter le développement pour répondre au cahier des charges d' [MASK] .
Celui-ci fut édité sous le nom d' [MASK] [MASK] [MASK] lors de l' introduction des [MASK] [MASK] sur les marchés anglophones , le 12 août 1981 .
Étant plus léger , moins cher et rendu plus disponible que ses deux concurrents , il devint rapidement le système d' exploitation installé d' office sur les [MASK] [MASK] , puis plus tard des [MASK] [MASK] .
Comme pour le [MASK] , [MASK] s' est réservé le droit de vendre des licences à d' autres constructeurs sous le nom de [MASK] .
Avec l' essor des [MASK] [MASK] dès le milieu des années 1980 ( de [MASK] [MASK] , [MASK] , [MASK] [MASK] , [MASK] , [MASK] … ) , [MASK] s' imposa rapidement et devient de facto la plate-forme de référence professionnelle , et un monopole , selon les points de vue .
En 1987 , des milliers de constructeurs de [MASK] [MASK] existaient dans le monde , et tous sans exception avaient un point de passage obligé qui était le système d' exploitation de [MASK] , le plus performant de tous , dans un souci , crucial pour le monde professionnel , d' unité , de standardisation , et de portabilité de tous les ordinateurs [MASK] [MASK] .
C' est ensuite grâce à [MASK] , que [MASK] s' imposa comme le principal acteur du secteur de la micro informatique .
Cependant , [MASK] [MASK] a souvent été accusé d' avoir " volé " le concept de [MASK] à [MASK] , qui avait produit peu avant le premier [MASK] , possédant lui aussi une interface graphique similaire à celle de [MASK] .
Mais cette accusation est infondée puisque l' interface graphique du [MASK] est elle même inspirée par le [MASK] [MASK] qui est le premier ordinateur personnel à avoir été doté d' une interface graphique .
D' abord simple interface graphique pour [MASK] , [MASK] est devenu beaucoup plus tard un système d' exploitation à part entière , après quelques versions intermédiaires .
Quelques coups de stratégie de marketing ne sont pas étrangers à ce succès , comme l' ajout de trois touches " [MASK] " sur les claviers afin de marquer celui-ci dans l' esprit du consommateur comme " étant fait pour [MASK] " et marginaliser ainsi le concurrent potentiel [MASK] développé par [MASK] , et co-développé initialement par [MASK] et [MASK] , jusqu' au divorce officiel entre les deux sociétés en septembre 1991 .
Selon [MASK] , un soin particulier a également été apporté aux questions d' ergonomie , et en particulier à la question des polices de caractères typographiques , dès les versions 3.0 ( [MASK] [MASK] [MASK] ) et 3.1 ( [MASK] ) de [MASK] .
Bien des années plus tard , [MASK] affirmera considérer son avance sur le plan de l' ergonomie comme l' atout qui permettra à [MASK] de survivre face à la concurrence libre de [MASK] et de [MASK] / [MASK] .
De fait , [MASK] consacre une part très importante de son budget aux questions d' ergonomie : un service observe toutes les hésitations d' utilisateurs novices , pour rendre les menus plus clairs , démarche fastidieuse et rarement réalisée sur des logiciels gratuits réalisés par des bénévoles .
[MASK] est alors devenu le standard micro-informatique de facto solidement soutenu par l' effet réseau indirect de milliers de logiciels et de périphériques matériels spécifiques à [MASK] qui ont nécessité des milliards de journées/hommes de développement .
L' élaboration d' un produit capable de rivaliser avec [MASK] impliquerait de disposer , comme [MASK] , de revenus réguliers pendant les années nécessaires au développement d' un tel système .
Or , le temps que celui-ci soit développé , [MASK] aurait déjà pris de l' avance , et éventuellement modifié les standards .
La mise à mort d' [MASK] par [MASK] ( contre toute attente ) , avec le consentement d' [MASK] puisque [MASK] [MASK] contient des parties d' [MASK] donc génère des redevances pour [MASK] , constitue un avertissement qui décourage toute velléité de tenter de concurrencer [MASK] .
La société [MASK] propose tout de même [MASK] , orienté d' emblée dans la gestion de la vidéo , et tout aussi ergonomique que [MASK] : ce système d' exploitation ne décollera jamais vraiment hors d' un cercle de passionnés .
Et [MASK] intentera d' ailleurs un procès antitrust contre [MASK] pour abus de position dominante , qui s' achèvera par un accord financier à l' amiable entre les deux sociétés .
[MASK] n' était pas la première à proposer une interface graphique pour les [MASK] .
Avant eux , la société [MASK] [MASK] avait développé un produit très comparable à l' interface du [MASK] , le GEM ( [MASK] [MASK] [MASK] ) .
Un procès intenté par [MASK] à l' encontre de [MASK] [MASK] s' était traduit par une condamnation de cette société à qui le jugement avait imposé d' enlever toutes les caractéristiques ressemblant au [MASK] [MASK] d' [MASK] dans son interface graphique , la rendant ainsi économiquement peu plaisante .
La version 1.0 de [MASK] , rudimentaire -- les fenêtres ne peuvent même pas se recouvrir -- , n' inquiète pas sérieusement [MASK] , qui ne réagit pas .
La version 2.0 est une concurrence plus sérieuse , et [MASK] intente un nouveau procès , cette fois-ci contre [MASK] .
[MASK] perdit définitivement son procès contre [MASK] en appel en 1994 .
Cet accord comprenait une prise de participation temporaire de [MASK] dans le capital d' [MASK] ( à hauteur de 150 millions de dollars soit 6 % du capital de la pomme ) , et l' obligation pour [MASK] de développer [MASK] [MASK] et [MASK] [MASK] pour [MASK] [MASK] au moins jusqu' en 2002 .
En échange , [MASK] abandonnait ses poursuites .
Un facteur important de l' adoption généralisée de [MASK] a été son rôle d' interface non seulement graphique , mais également de pilotes .
Sous [MASK] , chaque éditeur de logiciel devait développer individuellement la gestion de tout le panel des milliers de périphériques [MASK] [MASK] existants et à venir .
Tâche colossale que les éditeurs de logiciels n' ont plus à gérer sous [MASK] dans la mesure où celui-ci se charge de gérer lui-même en standard tous les pilotes de périphériques de l' univers [MASK] [MASK] .
L' histoire de [MASK] ne se résume cependant pas à celle de [MASK] .
D' autres pans importants de l' activité de [MASK] ont permis sa croissance :
[MASK] a rapidement dominé tout le secteur de l' informatique personnelle et s' est imposé comme un acteur incontournable de ce secteur .
Cette situation lui vaut de nombreuses critiques sur sa position dominante , il apparaît donc que [MASK] ait actuellement les mêmes types de problèmes de position dominante que ceux qu' avait [MASK] avant 1980 :
[MASK] est l' un des plus importants éditeurs de logiciels au monde et , dans le même temps , la société est présente depuis ses débuts dans le matériel ; la branche matériel ne semble jamais avoir dépassé 10 % du chiffre d' affaires de [MASK] .
Cette famille de systèmes d' exploitation est le principal produit de la firme et a été le second agent de son phénoménal succès de [MASK] jusqu' à la version 3.0 de ce logiciel , qui atteint alors son seuil de rentabilité ) .
[MASK] est installé sur presque 90 % des ordinateurs personnels vendus dans le monde , et dégage actuellement 87 % de marge bénéficiaire .
Néanmoins , [MASK] perd petit à petit des parts de marché au profit de [MASK] [MASK] [MASK] d' [MASK] .
Mais [MASK] reste aujourd'hui le produit le plus rentable de l' éditeur , suivi de près par la suite [MASK] [MASK] .
À l' origine , c' était la suite bureautique de l' éditeur , composée de nombreux logiciels dont le traitement de texte [MASK] , le tableur [MASK] , le logiciel de présentation [MASK] , l' outil de communication et agenda [MASK] et la base de données [MASK] .
[MASK] est un des logiciels les plus rentables de l' éditeur .
Une version d' [MASK] [MASK] est disponible gratuitement pour les systèmes [MASK] [MASK] d' [MASK] mais le développement de cette version a toutefois été arrêtée en 2003 .
Néanmoins , le navigateur perd des parts de marché depuis 2004 , avec l' arrivée d' autres navigateurs comme [MASK] [MASK] , [MASK] [MASK] et [MASK] .
Le [MASK] [MASK] [MASK] est aussi disponible pour les systèmes [MASK] [MASK] d' [MASK] .
Un nouveau service communautaire a été lancé en 2006 , " [MASK] [MASK] [MASK] " .
Cette gamme accueille plusieurs jeux comme [MASK] [MASK] [MASK] ou [MASK] [MASK] .
Il s' agit d' un serveur web fourni gratuitement par [MASK] depuis la version 4 de [MASK] [MASK] .
Sa fourniture gratuite a sonné le glas de nombreux produits concurrents payants ( le principe de la concurrence , comme [MASK] , était en effet de vendre le serveur et de fournir gratuitement le seul client , comme le fait [MASK] pour son logiciel [MASK] ) .
[MASK] est un logiciel antivirus fourni gratuitement par [MASK] depuis le 29 septembre 2009 .
Il est disponible en français et compatible avec [MASK] [MASK] , [MASK] ( 32 et 64 bits ) et [MASK] [MASK] .
[MASK] [MASK] est la suite de développement de la firme , incluant divers éditeurs et compilateurs , essentiellement pour une version améliorée de [MASK] nommée [MASK] [MASK] , ainsi que pour des implémentations de [MASK] et de [MASK] [MASK] , qui constitue la réponse de [MASK] au langage [MASK] .
Il permet aussi de tirer parti des fonctionnalités du [MASK] [MASK] .
[MASK] [MASK] est le système de gestion de base de données phare de [MASK] , co-développé avec [MASK] jusqu' en 1994 .
Le coeur de [MASK] [MASK] est utilisé dans d' autres produits de la marque tels que [MASK] ou [MASK] [MASK] .
[MASK] s' est lancé en 2001 dans ce secteur hautement concurrentiel , en sortant sa propre console de jeux vidéo , la [MASK] .
Elle est remplacée au bout de 4 ans par la [MASK] [MASK] .
Elles proposent toutes deux des centaines de jeux et un mode de jeu en ligne communautaire , le [MASK] [MASK] .
La [MASK] [MASK] permet également de se connecter à [MASK] et d' utiliser d' autres services de [MASK] .
La [MASK] [MASK] possède environ 30 % des parts de marché sur le marché des consoles de jeux vidéo en février 2009 .
Le [MASK] est un baladeur numérique destiné principalement à écouter de la musique .
Il a été lancé en novembre 2006 pour concurrencer la suprématie de l' [MASK] aux [MASK] .
[MASK] fabrique également une ligne de périphériques divers pour [MASK] ( souris , claviers , périphériques de jeu , volants … ) .
Actuellement l' activité internet de [MASK] est constitué en trois principaux produits :
[MASK] propose également un programme de formation et certification .
Aux [MASK] , [MASK] est une chaîne d' information en continu .
Il propose également [MASK] , une API multimédia ( vidéo , son , réseau , etc . )
pour le développement d' application [MASK] ( principalement des jeux vidéo ) et aussi [MASK] qui permet de visionner des animations vectorielles ( faisaint ainsi concurrence à [MASK] [MASK] [MASK] ) , mais surtout des contenus multimédia intégrant de l' audio et de la vidéo .
D' après les comptes annuels , le chiffre d' affaires de [MASK] s' élevait en 2005 à 39,788 millions de dollars .
L' action valait 26.37 dollars le 19 juin 2010 pour une capitalisation boursière de 231.7 milliards de dollars , ce qui fait d' elle la seconde valeur technologique mondiale ( juste derrière [MASK] ) , à comparer par exemple aux 28.3 milliards de dollars de [MASK] .
La somme versée en dividendes aux actionnaires de [MASK] en 2004 approche donc les 28 milliards USD .
Cependant , cette décision prise au niveau fédéral ne deviendra exécutoire que si [MASK] décide de ne pas faire appel .
Conscient du problème , le [MASK] ( le consortium qui définit les standards technologiques du web ) est sorti de sa réserve , le 29 octobre 2003 , en demandant à l' [MASK] américain des brevets un ré-examen de la validité du brevet 906 .
À l' appui de sa requête , [MASK] a adressé des " preuves d' antériorité " mettant en évidence , selon cette organisation , l' invalidité du brevet .
Le 24 mars 2004 , à la suite de quatre années d' enquête du commissaire européen à la concurrence [MASK] [MASK] , la [MASK] [MASK] rend sa décision dans le procès anti-trust qui l' oppose à la firme .
La décision rendue condamne la position monopolistique de [MASK] et ses pratiques illégales .
La commission inflige par ailleurs à [MASK] une sanction historique de 497.5 millions d' euros .
[MASK] devra notamment vendre une version de [MASK] sans le [MASK] [MASK] [MASK] .
Un recours engagé par la firme ( 7 juin 2004 ) devant le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] n' étant a priori non suspensif la somme de l' amende a été réglée le 1 er juillet 2004 .
Le 12 juillet 2006 , la commission prononce une nouvelle amende ( 280 millions d' euros ) contre [MASK] pour sanctionner le retard pris par le groupe à publier ses spécifications .
Le 17 septembre 2007 , le [MASK] confirme la première amende de 497.5 millions d' euros .
Le litige portait sur l' utilisation par [MASK] , dans son [MASK] [MASK] [MASK] [MASK] , d' une invention destinée à numériser la voix et protégée par un brevet dont [MASK] [MASK] [MASK] était titulaire .
Comme [MASK] envoie directement aux sociétés fabricantes d' ordinateurs situées en dehors des [MASK] , l' intégralité des données composant le code de son système d' exploitation afin qu' elles puissent les copier sur leurs machines avant de les vendre , [MASK] [MASK] [MASK] réclamait d' être dédommagée pour tous les logiciels , comprenant leur invention brevetée , qui avaient été exportés hors du territoire américain .
Les avocats de [MASK] ont reconnu la contrefaçon des brevets [MASK] [MASK] [MASK] sur le territoire américain et rappelé que ce litige avait fait l' objet d' un accord amiable entre les parties en 2004 .
Par contre , ils se sont fermement opposés à l' idée selon laquelle [MASK] pourrait se voir reprocher une contrefaçon au niveau international , estimant qu' un code informatique ne constitue pas un composant en tant que tel tant qu' il n' est pas installé sur un ordinateur .
On ne peut selon eux reprocher à [MASK] d' exporter hors du territoire des [MASK] des composants informatiques protégés par brevet .