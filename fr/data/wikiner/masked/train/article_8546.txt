[MASK] était le service d' information enregistrant l' ensemble des noms de domaines d' [MASK] .
Cet organisme a été instauré en 1992 , afin de pouvoir faire face à l' ouverture d' [MASK] au public et vit son rôle disparaître en 1998 , lors de l' ouverture à la concurrence ; pour être remplacé par l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
Ces noms sont gérés par le [MASK] .
Lors de l' ouverture d' [MASK] , tout dépôt de nom de domaine se fait auprès d ' [MASK] .
Les coûts résultants de ces bases de données de noms sont subventionnés par l' [MASK] .
[MASK] est donc la base de donnée de tous les noms de domaines de l' [MASK] .
Durant l' année 1995 , face à la croissance des demandes de noms de domaine , l' [MASK] ne peut plus faire face aux frais et l' enregistrement devient payant .