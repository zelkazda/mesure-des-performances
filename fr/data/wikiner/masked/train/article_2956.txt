Dans le fond et dans l' essence et tel qu' il est décrit dans les anciens écrits comme le [MASK] , le système de caste basé sur la naissance , qui existe en [MASK] moderne , n' existait pas dans l' hindouisme védique antique .
Un hymne célèbre du [MASK] indique : " Je suis un poète , mon père est un médecin , le travail de ma mère est de moudre le blé … " .
C' est le seul hymne dans le [MASK] , qui appartient à ce livre ( X e ) que beaucoup d' historiens prétendent être une addition peu postérieure aux neuf livres existants , qui énumère les quatre varnas pour la première et la dernière fois ( sans définir n' importe quoi ) .
Une hymne de [MASK] dit :
Les varna sont présentés dans la [MASK] de la façon suivante :
Notons toute fois que dans l' [MASK] védique , aucun individu était lié de naissance à une caste .
Chacune de ces parties du corps social est censée provenir d' une partie du corps de [MASK] , le brahmane est la bouche .
Le système des castes a été fortement combattu par plusieurs réformateurs indiens , le plus connu d' entre eux est [MASK] [MASK] [MASK] , rédacteur de la constitution de l' [MASK] et lui-même intouchable mahar avant de se convertir au bouddhisme .
Ainsi , parmi les hommes les plus prospères de [MASK] , on trouve des intouchables qui ont pris en charge le commerce lié à la mort , la fourniture du bois nécessaire aux crémations par exemple , un commerce refusé par les varna comme impur , puisqu' il touche à la mort .
Enfin , à l' image de ce qui s' était fait au [MASK] , les familles furent regroupés au sein de " réserves confessionnelles " .
Suivant la formule de [MASK] , " d' intouchables qu' ils avaient été , ils étaient devenus étrangers " sans espace reconnu dans la société indienne .
L' [MASK] est le seul pays au monde , en dehors de l' [MASK] , où il existe encore une tradition qui se réfère à l' hindouisme .
On la trouve dans quelques régions de [MASK] ( flanc ouest du mont [MASK] , massif du [MASK] , régions de [MASK] et [MASK] ) .
Mais c' est surtout [MASK] qui est connue dans le monde pour cette tradition .
La distinction entre " intérieur " et " extérieur " se retrouve ailleurs en [MASK] .
Ainsi à [MASK] , le mot dalem , " intérieur " , désigne la demeure d' un aristocrate .
Dans les langues d' [MASK] , ce mot a simplement pris le sens d ' " essence " , " identité " .
En fait , [MASK] n' a fait qu' adopter des termes indiens pour les placer dans un contexte fondamentalement indonésien et sans doute , austronésien .