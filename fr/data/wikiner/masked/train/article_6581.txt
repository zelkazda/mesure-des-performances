Il est né le 21 juillet 1948 à [MASK] .
Il étudia l' économie à l' université de [MASK] , où il obtint une licence en 1972 puis partit poursuivre ces études en [MASK] , [MASK] afin d' obtenir un doctorat .
Il travailla entre 1977 et 1984 à la [MASK] [MASK] [MASK] [MASK] en tant qu' économiste .