Les dispositions de [MASK] [MASK] [MASK] , dixième enfant d' une famille de gantiers de [MASK] , pour la mécanique se révélèrent de très bonne heure et d' une manière significative .
Il est élève au [MASK] [MASK] [MASK] de 1717 à 1721 , et souhaite suivre sa vocation religieuse .
Il suit alors à [MASK] , de 1728 à 1731 , des études de mécanique , physique , anatomie et musique .
Ce point est soupçonné d' être une exagération de la part de [MASK] , et [MASK] [MASK] , entre autres , le dénonce comme une mystification .
Il reste possible que cette mystification n' ait eu lieu que pour les répliques du canard de [MASK] , réalisées plus tard .
Il semble qu' un des écueils ait été la fabrication de l' appareil circulatoire en caoutchouc , matière qui devait alors provenir de [MASK] .
Peu de temps après , le [MASK] [MASK] [MASK] l' en récompensa en l' attachant à l' administration et en lui confiant le poste d' inspecteur général des manufactures de soie en 1741 , le roi souhaitant réorganiser cette industrie , ce qui entraînera l' arrêt de ses travaux sur les automates .
[MASK] [MASK] raconte que ses perfectionnements de machines entraînant une simplification de travail firent à [MASK] des ennemis parmi les ouvriers lyonnais de la soie , qui se croyaient seuls capables d' exécuter certaines étoffes dont le dessin était alors à la mode .
Et il construisit une machine , avec laquelle un âne exécutait une étoffe à fleurs , qu' on voit encore aujourd'hui au [MASK] [MASK] [MASK] [MASK] [MASK] , telle qu' elle fut construite , avec une partie du dessin exécuté .
En 1746 , [MASK] [MASK] [MASK] entre à l' [MASK] [MASK] [MASK] française .
Il meurt le 21 novembre 1782 , à [MASK] , en léguant ses machines au roi , legs qui sera une des bases de la collection du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Son but était de faciliter l' activité humaine , ce qui le conduisit à siéger à l' [MASK] [MASK] [MASK] et à participer à l ' [MASK] de [MASK] et [MASK] [MASK] .