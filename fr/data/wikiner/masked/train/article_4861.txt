On doit ce nom à [MASK] [MASK] , inventeur italien de la pile électrique en 1800 .
En 1800 , [MASK] [MASK] développe ce qu' on appelle la pile voltaïque , un précurseur de la batterie , qui produit un courant électrique stable .
[MASK] a déterminé que la meilleure paire de différents métaux pour produire de l' électricité est une paire de zinc et d' argent .