Il vécut à [MASK] et mourut à 84 ans .
[MASK] a aussi écrit un traité sur les nombres polygonaux , le plus ancien de cette science ; il ne nous en reste que 10 livres sur 13 .
[MASK] s' intéresse notamment aux problèmes suivants :
Ce problème partage la vie de [MASK] en parties inégales représentées par des fractions et permet de calculer la durée de sa vie , soit 84 ans .
Voici le problème en abrégé : L' enfance de [MASK] occupa un sixième de toute sa vie .
[MASK] mourut quatre ans après la mort de son fils .
On trouve x = 84 ans , âge auquel [MASK] mourut .