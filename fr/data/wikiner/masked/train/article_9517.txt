Son frère [MASK] [MASK] est également pilote de rallye .
Il participera aux [MASK] [MASK] [MASK] et au [MASK] [MASK] [MASK] 1998 .
[MASK] sera une année plus faste en résultats .
[MASK] sera l' année de la consécration .
La dernière épreuve qui peut titrer quatre pilotes , le [MASK] [MASK] [MASK] , tournera vite au duel entre lui et [MASK] [MASK] suite au forfait de [MASK] [MASK] et à la sortie de piste de [MASK] [MASK] .
Malgré trois victoires de rang en fin de saison , il perdit son titre au profit de [MASK] à deux épreuves de la fin .
[MASK] ne commença pas très bien avec une 4 e place comme meilleur résultat après les trois premiers rallyes de la saison .
Malgré une solide course en [MASK] à la mi-saison , il n' y eut pas d' autres podiums jusqu' à la fin de saison .
Il finit 6 e du championnat , mais derrière son coéquipier [MASK] [MASK] cette fois , et avec un seul podium .
Le retrait imprévu fin 2008 de [MASK] du championnat du monde le laissa provisoirement sans volant .
Faisant l' impasse sur la première course de la saison , il commencera sa saison au [MASK] [MASK] [MASK] au volant d' une [MASK] [MASK] [MASK] [MASK] , où il termina 6 e .
Malgré une machine vieillissante , plusieurs résultat positifs vont suivre : 3 e au [MASK] [MASK] [MASK] , 4 e au [MASK] , puis une nouvelle 3 e place en [MASK] , à la suite d' une pénalité de [MASK] [MASK] .