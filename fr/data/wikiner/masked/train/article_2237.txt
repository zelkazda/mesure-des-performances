L' étude épistémologique de l' algèbre a été introduite par [MASK] [MASK] .
[MASK] , au III e siècle de l' ère chrétienne , fut le premier à pratiquer l' algèbre en introduisant le concept d' inconnue en tant que nombre , et à ce titre peut être considéré comme " le père " de l' algèbre .
[MASK] , élève de [MASK] , résout l' équation du 4 e degré ( ou équation quartique ) , et la méthode est perfectionnée par [MASK] .
C' est à [MASK] [MASK] ( 1540-1603 ) que l' on doit l' idée de noter les inconnues à l' aide de lettres .
Cette " extension " des nombres réels ( qui prendra le nom de nombres complexes ) amène [MASK] [MASK] ( en 1746 ) et [MASK] ( en 1799 ) à énoncer et démontrer le théorème fondamental de l' algèbre :
Les échecs concernant les équations de degré 5 amènent le mathématicien [MASK] ( après [MASK] , [MASK] et [MASK] ) à approfondir les transformations sur l' ensemble des racines d' une équation .
[MASK] [MASK] ( 1811 -- 1832 ) , dans un mémoire fulgurant , introduit pour la première fois la notion de groupe ( en étudiant le groupe des permutations des racines d' une équation polynomiale ) et aboutit à l' impossibilité de la résolution par radicaux pour les équations de degré supérieur ou égal à 5 .
Celle-ci permettra à [MASK] d' énoncer sa célèbre formule .
Dès lors , l' algèbre moderne entame un parcours fécond : [MASK] crée l' algèbre qui porte son nom , [MASK] invente les quaternions , et les mathématiciens anglais [MASK] , [MASK] et [MASK] étudient les structures de matrices .
Parallèlement , [MASK] généralise les structures galoisiennes et étudie les structures de corps et d' anneau .
[MASK] définit les idéaux ( déjà entrevus par [MASK] ) qui permettront de généraliser et reformuler les grands théorèmes d' arithmétique .
[MASK] axiomatise l' arithmétique , puis les espaces vectoriels .
La structure d' espace vectoriel et la structure d' algèbre sont approfondies par [MASK] en 1925 , avec des corps de base autres que ou et des opérateurs toujours plus abstraits .
On doit aussi à [MASK] , considéré comme le père de l' algèbre contemporaine , des résultats fondamentaux sur les corps de nombres algébriques .