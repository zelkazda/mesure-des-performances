Il tire son nom de la rivière [MASK] .
L' [MASK] et [MASK] [MASK] lui attribuent le code 23 .
Elle est limitrophe des départements de la [MASK] , de la [MASK] , de l' [MASK] , du [MASK] , du [MASK] et de l' [MASK] .
Le département est situé à l' extrémité nord-ouest du [MASK] [MASK] .
Le [MASK] [MASK] [MASK] occupe le sud-est .
En effet ce rôle est tenu par la [MASK] , aussi bien par la densité de population que par la population totale .
De plus ce département , contrairement à certains de ses voisins ( [MASK] ... ) , a un solde migratoire positif .
Cette situation est aggravée par le fait que les jeunes , déjà peu nombreux , s' en vont poursuivre leurs études hors du département ( parfois dès le lycée ) dans les métropoles voisines ( [MASK] , [MASK] , [MASK] , [MASK] ) et ne reviennent que rarement au pays .
Le département possède 1 aire urbaine : [MASK] .
Ce tableau indique les principales [MASK] [MASK] [MASK] [MASK] dont les résidences secondaires et occasionnelles dépassent 10 % des logements totaux .