Sa capitale est [MASK] .
Ses principales villes sont [MASK] , [MASK] , [MASK] , [MASK] ( [MASK] ) , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
Le bavarois est aussi parlé en [MASK] : la moitié de ses locuteurs s' y trouvent d' ailleurs .
Au temps de [MASK] [MASK] , cette contrée paraît avoir encore été désertée , mais sous [MASK] on la voit déjà figurer au nombre des provinces romaines ; elle était comprise dans la [MASK] et le [MASK] .
Les ducs agilofides continuent à régir la [MASK] au nom des rois francs jusqu' à [MASK] [MASK] [MASK] qui en 743 prend le titre de roi .
Dès 739 , [MASK] fixe les diocèses de [MASK] , [MASK] , [MASK] et [MASK] .
[MASK] essaye mais en vain de se soustraire à la suzeraineté de [MASK] [MASK] .
En 757 , [MASK] prête serment de fidélité à [MASK] [MASK] [MASK] , au plaid de [MASK] .
Il réunit des conciles et le pape [MASK] baptise son fils en 772 .
C' est pourquoi [MASK] exige un renouvellement de son serment en 787 : [MASK] reçoit alors l' investiture solennelle de son [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] l' érige en royaume franc ( 814 ) , et le donne à son fils aîné , [MASK] , qui en 817 le cède à [MASK] [MASK] [MASK] .
Le [MASK] [MASK] [MASK] comprend alors , outre la [MASK] propre , la [MASK] , la [MASK] , l' [MASK] , le [MASK] , l' ancienne [MASK] , la [MASK] et la [MASK] .
Sous les successeurs de ce prince , le [MASK] [MASK] [MASK] , qui avait été considérablement réduit , reprend de nouveaux accroissements .
[MASK] [MASK] agrandit considérablement ses domaines ; lorsqu' il meurt ( 1347 ) il possède , outre la [MASK] , le [MASK] , la [MASK] , la [MASK] , le [MASK] , etc .
En récompense l' empereur [MASK] [MASK] élève le duc [MASK] à la dignité d' électeur ( 1623 ) et rend ce titre héréditaire dans sa famille .
Cette dignité lui est confirmée en 1648 par le [MASK] [MASK] [MASK] .
[MASK] , qui lui succède , prétend à la succession de l' empereur [MASK] [MASK] , conquiert la [MASK] et l' [MASK] , et se fait même couronner à [MASK] en 1742 sous le nom de [MASK] [MASK] ; mais vaincu par [MASK] [MASK] [MASK] , à la tête des troupes autrichiennes , il se voit forcé non seulement de renoncer à l' empire , mais d' abandonner la [MASK] elle-même à [MASK] [MASK] [MASK] ; il meurt avant la fin de la guerre .
[MASK] [MASK] , électeur palatin , allié à cette famille , parvient cependant à régner en [MASK] malgré l' [MASK] ; et après sa mort ( 1799 ) , son neveu [MASK] [MASK] lui succède .
Par la [MASK] [MASK] [MASK] , elle doit céder ses possessions sur la rive gauche du [MASK] , mais elle reçoit d' amples compensations .
Longtemps fidèle alliée de [MASK] [MASK] , elle est obligée de lui fournir de nombreux contingents .
En 1809 , [MASK] bat les [MASK] à [MASK] .
Pour prix de cette conduite , il reçoit au [MASK] [MASK] [MASK] , la confirmation de sa royauté et de ses possessions .
Il abdique en 1848 en faveur de son fils [MASK] [MASK] qui , pour maintenir l' importance de la [MASK] , s' est constamment opposé à toute tentative de centralisation de l' [MASK] .
Mais son successeur [MASK] [MASK] , a dû subir la suprématie de la [MASK] , après la guerre contre [MASK] [MASK] ( 1870 -- 1871 ) .
Ce roi fut certainement le monarque le plus connu , notamment grâce à la construction de châteaux fantasmagoriques ( comme [MASK] ) et par son décès mystérieux .
La [MASK] est divisée en sept districts appelés également circonscriptions ou régions administratives , eux-mêmes subdivisés en soixante-et-onze arrondissements et vingt-cinq municipalités non intégrées à un arrondissement , constituant donc un arrondissement à elles-seules , appelées villes-arrondissements .
Il faut attendre le 25 octobre pour qu' une coalition majoritaire , réunissant les conservateurs et le [MASK] [MASK] , soit formée .
Le centre économique de la [MASK] est [MASK] , ville du siège social de nombreuses sociétés .
Le taux de chômage s' élève en septembre 2008 à 3,9 % contre 7,4 % pour l' ensemble de l' [MASK] .
La [MASK] est principalement catholique ( 56,4 % de la population ) , mais l ' [MASK] [MASK] [MASK] a une forte présence dans de grandes parties de [MASK] ( 21 % ) .
Le pape actuel , [MASK] [MASK] , est né à [MASK] [MASK] [MASK] en [MASK] et fut archevêque de [MASK] [MASK] [MASK] .
D' une manière générale en [MASK] , le culte est bien suivi et les traditions sont très marquées .
La fête de [MASK] s' étend sur une semaine durant laquelle la vie s' arrête complètement .
En [MASK] , le carême est véritablement un temps de pénitence , ce qui explique la liesse manifestée au cours de la semaine de carnavals qui précède .
En [MASK] , les lieux publics tels que hôpitaux , jardins d' enfants ou cantines , ne servent que du poisson le vendredi .
L' incroyable densité d' abbayes , couvents et lieux de pèlerinage où les fidèles se rassemblent lors des chemins de croix et autres processions font de la [MASK] un véritable noyau de la spiritualité catholique .
[MASK] [MASK] et [MASK] [MASK] ( dir .