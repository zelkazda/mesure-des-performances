Le nom de [MASK] vient du nom d' une tribu algonquienne , les [MASK] .
Les îles de [MASK] [MASK] [MASK] et [MASK] se trouvent au large de la côte sud-est .
La majorité de la population habite à la côte autour de [MASK] et des villes industrielles .
Au centre et à l' ouest du [MASK] , de nombreuses fermes produisent fruits , tomates et maïs .
La colonie fut ainsi nommée en souvenir d' une tribu indienne du pays , les [MASK] , dont le nom signifiait " un endroit d' une grande colline " .
Le [MASK] était une des treize colonies qui se révoltèrent contre les [MASK] lors de la [MASK] [MASK] .
[MASK] [MASK] , pasteur presbytérien au [MASK] entreprend d' évangéliser les [MASK] à partir de 1641 .
Le libéralisme du [MASK] est aussi ancien que son puritanisme .
Depuis 1960 , trois élus du [MASK] ont postulé aux élections présidentielles : [MASK] [MASK] [MASK] élu en 1960 , [MASK] [MASK] , candidat en 1988 et [MASK] [MASK] , candidat en 2004 .
Le 2 novembre 2004 , [MASK] [MASK] a obtenu 61,94 % des voix contre 36,78 % à [MASK] [MASK] [MASK] , réélu au plan national .
Cependant , tous ces gouverneurs républicains que furent [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] représentaient l' aile la plus modérée et la plus progressiste du parti .
Celui-ci est alors devenu le second gouverneur noir des [MASK] .
Si 51 % des habitants du [MASK] se déclarent indépendant en 2009 , ils sont 13 % à se revendiquer républicains .
Ce dernier a succédé à titre provisoire au titulaire du siège , [MASK] [MASK] au décès de celui-ci , le temps qu' une élection partielle soit organisée pour désigner celui qui terminera le mandat .
Par le référendum du 4 novembre 2008 , le [MASK] dépénalise la marijuana .
Aujourd'hui , il est le seul avec le [MASK] , le [MASK] , le [MASK] [MASK] , et l' [MASK] à l' autoriser .
Le revenu personnel moyen par personne en 2004 était de 42102 dollars par an , soit le deuxième plus élevé après celui du [MASK] .
D' autres secteurs qui sont d' importance vitale à l' économie de [MASK] sont les études supérieures , les soins de santé , les services financiers et le tourisme .
En outre , les événements de la [MASK] [MASK] [MASK] continuent à affecter la culture du [MASK] .
Les feux d' artifice et un concert se produisent sur le rivage du [MASK] [MASK] pendant ce festival .
Il y a un grand nombre d' universités dans les villes de la zone métropolitaine de [MASK] , dont l' [MASK] [MASK] et le [MASK] [MASK] [MASK] [MASK] [MASK] .
D' autres universités du [MASK] :