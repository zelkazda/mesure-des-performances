[MASK] [MASK] [MASK] est un manga de [MASK] .
Il a été adapté en anime de 26 épisodes par le [MASK] [MASK] en 1997 .
[MASK] [MASK] [MASK] est un manga qui raconte l' histoire de trois jeunes garçons de l' école [MASK] .
Dès le départ , on voit que cette histoire est une immense farce , et [MASK] prend un malin plaisir à caricaturer ses personnages , leur donnant des responsabilités et une importance démesurée puisqu' ils ne sont encore qu' à l' école primaire .
Mais [MASK] profite en quelque sorte de ce manga pour planter le décor de l' univers de plusieurs autres de leurs œuvres .
De plus les personnages principaux apparaissent dans d' autres séries , non seulement dans [MASK] , mais aussi dans [MASK] [MASK] [MASK] [MASK] [MASK] ou encore [MASK] .