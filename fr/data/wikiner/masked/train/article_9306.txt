Le camp de concentration de [MASK] a été établi le 13 décembre 1938 , au sud-est de [MASK] sur le fleuve [MASK] , d' abord comme camp extérieur du camp de [MASK] puis transformé en 1940 en camp de travail indépendant ( 213 000 m³ ) avec plus de 90 camps extérieurs annexes .
Lorsque les troupes britanniques découvrent le camp de [MASK] , le 4 mai 1945 , il est " vidé " de sa population et des traces des exactions nazies .
On estime à environ 13 500 le nombre de femmes immatriculées à [MASK] , dont plus de 700 [MASK] .
Ces déportées étaient sous la surveillance de gardiennes [MASK] .
En février 1942 , à l' initiative du [MASK] [MASK] [MASK] , il est décidé d' interner des personnalité politiques françaises .
En novembre 1942 , les autorités nazies auraient envisagé de transférer en [MASK] certaines de ces personnes déjà internées ou en résidence surveillée en [MASK] .
L' [MASK] inaugure alors une nouvelle forme de répression : l' arrestation et l' internement en [MASK] de " personnalités otages " .
326 de ces " personalités otages " seront internées à [MASK] .
Ce monument fut érigé le 29 août 1998 en souvenir des victimes innocentes de la razzia de [MASK] .
Les 1 er et 11 août 1944 61 habitants du petit village belge de [MASK] furent déportés à [MASK] , 8 d' entre eux seulement revinrent chez eux .
Le 2 octobre 1944 , ils furent amenés dans le camp d' [MASK] ( [MASK] ) et de là à [MASK] .
Des 600 , 49 seulement retournèrent chez eux , les autres périrent dans le camp de [MASK] ou dans d' autres camps de concentration .
Le chiffre total des détenus polonais du camp [MASK] et de ses camps extérieurs s' élève à environ 17 000 femmes , hommes et enfants , dont beaucoup de juives et de juifs .
La plus grande partie des victimes soviétiques du camp [MASK] reposant dans le cimetière de [MASK] , c' est pourquoi l' initiative a été prise d' y installer le mémorial ( plus grand que nature ) .