[MASK] [MASK] ou cancoyotte est un fromage français fabriqué principalement en [MASK] -- mais également en [MASK] et au [MASK] ( où elle est appelée également kachkéis ) -- obtenu à partir de lait écrémé caillé .
[MASK] [MASK] est un fromage semi-liquide à l' état de repos à base de lait de vache , à pâte fondue ( le metton ) , conditionné en pots de 200 à 500 grammes .
[MASK] [MASK] se déguste toute l' année et peut se manger chaude ou froide .
[MASK] [MASK] nature et à l' ail ont chacune leurs partisans .