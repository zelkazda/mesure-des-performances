Son nom provient du fleuve de la [MASK] .
L' [MASK] et [MASK] [MASK] lui attribuent le code 80 .
Le département a été créé à la [MASK] [MASK] , le 4 mars 1790 en application de la loi du 22 décembre 1789 , à partir d' une partie de la province de [MASK] ( l' [MASK] , le [MASK] , le [MASK] , le [MASK] et le [MASK] ) .
La [MASK] [MASK] [MASK] [MASK] s' est déroulée en 1916 .
Elle est limitrophe des départements du [MASK] , du [MASK] , de l' [MASK] , de l' [MASK] et de la [MASK] , et est bordée par la [MASK] autour de la [MASK] [MASK] [MASK] .
Il raconte 2000 ans d' histoire de la [MASK] durant une heure et demie de jeux de lumière , de feux d' artifices , de jets d' eau .
A [MASK] , chaque année au début du mois de septembre a lieu une fête médiévale importante et populaire dans le parc de 4ha du château de [MASK] .
A [MASK] , les grottes sont un des plus beaux exemples de muches picardes .
En [MASK] [MASK] [MASK] , chaque année en avril , a lieu le festival de l' oiseau et de la nature .