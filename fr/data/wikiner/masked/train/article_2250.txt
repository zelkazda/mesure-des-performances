En [MASK] on le surnomme " le roi des jeux " ou encore " le noble jeu " .
Le premier tournoi de l' ère moderne a lieu à [MASK] en marge de l' [MASK] [MASK] [MASK] [MASK] .
La compétition est régie par la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) et le jeu est reconnu comme sport olympique depuis 1999 .
Parallèlement , l' [MASK] [MASK] [MASK] [MASK] défend les interêts des joueurs professionnels .
Cette discipline est également sous l' égide de la [MASK] , qui organise des concours spécifiques pour les compositeurs de problème et les solutionnistes .
En 1997 , [MASK] [MASK] devient le premier ordinateur à battre un champion du monde en titre dans un match qui l' oppose à [MASK] [MASK] .
Depuis leur introduction en [MASK] , les échecs jouissent d' un prestige et d' une aura particuliers .
De nos jours on utilise mondialement la notation algébrique abrégée qui est le système officiel de la [MASK] .
Cette notation , non obligatoire dans le règlement officiel de la [MASK] , facilite cependant la lecture .
Le format [MASK] vise à standardiser le format utilisé pour décrire une partie d' échecs à destination des programmes informatiques .
Ces en-têtes sont suivis par les coups joués , décrits en format [MASK] ( [MASK] [MASK] [MASK] ) .
Le format [MASK] , qui fait partie de la spécification [MASK] , est très similaire à la notation algébrique abrégée en langue anglaise mais en diffère cependant quelque peu .
Le diagramme ci-contre , tiré d' une partie [MASK] [MASK] -- [MASK] [MASK] de 1922 , montre la difficulté qu' il peut y avoir à évaluer certaines positions .
Un exemple classique est le sacrifice double de la partie [MASK] [MASK] [MASK] [MASK] .
Cette partie a opposé [MASK] [MASK] à [MASK] [MASK] à [MASK] , en 1851 .
C' est maintenant qu' [MASK] le surprend .
Il fit remarquer que la position finale est un mat modèle , ce à quoi fut certainement sensible [MASK] qui était également un compositeur de problèmes d' échecs .
Elle est toujours utilisée dans les compétitions homologuées par la [MASK] .
La polyvalence des pendules électroniques leur permet aussi d' être utilisées dans d' autres jeux , comme le shōgi , le jeu de go ou le [MASK] .
Elle organise également les [MASK] [MASK] [MASK] et le championnat du monde d' échecs .
Les membres de la [MASK] sont les fédérations nationales , telles la [MASK] [MASK] [MASK] [MASK] .
Le site de la [MASK] [MASK] [MASK] [MASK] propose une rubrique sur l' arbitrage .
Après sa victoire sur [MASK] [MASK] en 1886 , [MASK] [MASK] fut le premier champion du monde officiel .
En 1975 , [MASK] [MASK] refusa de jouer le championnat du monde contre [MASK] [MASK] .
En 1993 , [MASK] [MASK] provoqua une scission avec la [MASK] et créa sa propre fédération , la [MASK] ( [MASK] [MASK] [MASK] ) .
Champions du monde " [MASK] " :
À partir de 1999 , contrairement à la tradition , les championnats du monde " [MASK] " furent des tournois à élimination directe .
[MASK] [MASK] battit [MASK] [MASK] .
En 1946 , le psychologue néerlandais ( et joueur d' échecs ) [MASK] [MASK] [MASK] publie une importante étude des mécanismes du choix des coups .
Une autre légende place l' invention du jeu durant la [MASK] [MASK] [MASK] .
Au-delà de cette époque , certains supposent que le jeu a évolué à partir de jeux de parcours indiens , d' autres lui prêtent un ancêtre extérieur en [MASK] ou en [MASK] [MASK] .
Le jeu se propage jusqu' en [MASK] aux alentours de l' an 600 où il devient le chatrang , qui se joue désormais à deux joueurs sans dés .
L' arrivée des échecs en [MASK] se fait sans doute aux alentours de l' an mille par l' [MASK] [MASK] ou l' [MASK] du sud .
Dans certaines régions d' [MASK] , le double pas initial du pion est pratiqué .
Si les premiers livres traitant des échecs remontent à l' époque arabe , la stabilisation des règles en [MASK] donne naissance à une littérature théorique très riche et on observe notamment l' élaboration des premiers systèmes d' ouverture .
Plus tard , les tensions entre conservateurs russes et partisans de la perestroïka se cristalliseront autour de l' affrontement entre [MASK] [MASK] et [MASK] [MASK] .
Depuis le début de l' année 2008 , l' entrée de ce sport aux [MASK] [MASK] est discutée .
Notons toutefois que les grands maîtres anglais [MASK] [MASK] et [MASK] [MASK] ont remporté le [MASK] [MASK] [MASK] [MASK] [MASK] , et que [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] sont des compositeurs d' étude réputés .
Il fut remporté par le programme soviétique [MASK] .
En 1995 , [MASK] n' hésite pas à investir dans le projet [MASK] [MASK] , dont la seconde mouture , en 1997 , sera la première machine à battre un champion du monde dans des conditions normales de jeu ( à cette époque , les ordinateurs étaient déjà redoutables en partie rapide ) .
[MASK] contestera néanmoins la valeur de cette victoire en soulignant que , contrairement aux conditions d' un match de championnat du monde contre un humain , il n' avait pas eu accès aux parties disputées par l' ordinateur auparavant pour sa préparation ( la réciproque étant fausse ) .
[MASK] exigea une revanche qui lui fut refusée par [MASK] .
On peut remarquer à ce sujet que , contrairement à [MASK] [MASK] , les logiciels opposés aux humains sont des programmes commerciaux tournant sur des micro-ordinateurs standard ( alors que [MASK] [MASK] fonctionnait sur une machine plus puissante ) .
Depuis la victoire de [MASK] [MASK] , le statut des échecs en tant que défi informatique s' est amoindri , et l' attention des programmeurs s' est reportée sur le go .
Pourtant l' exception [MASK] a refait parler des superordinateurs dédiés au jeu d' échecs en juin 2005 , en battant le grand maître international et 7 e mondial [MASK] [MASK] , sur un score sans appel de 5,5 points contre 0,5 .
En décembre 2006 , le champion du monde [MASK] s' est fait battre par le nouveau logiciel [MASK] [MASK] [MASK] 4 à 2 ( 2 défaites , 4 nulles ) .
Très rapidement après leur arrivée en [MASK] , les échecs acquièrent un statut particulier .
C' est cette perception du jeu d' échecs comme expression de l' intelligence humaine qui dramatise les affrontements entre [MASK] [MASK] et la machine [MASK] [MASK] .
Cette dimension encourage l' [MASK] [MASK] à se doter d' une école d' échecs qui forme pendant un demi-siècle tous les champions du monde .
Parmi eux , deux se distinguent en mettant le jeu au centre de l' intrigue : [MASK] [MASK] [MASK] [MASK] , de [MASK] [MASK] , et [MASK] [MASK] [MASK] , de [MASK] [MASK] .
[MASK] [MASK] [MASK] [MASK] , nouvelle de [MASK] [MASK] , a pour sujet l' affrontement d' un joueur particulièrement doué , qui a appris seul à jouer aux échecs , seule façon pour lui de garder son esprit alerte alors qu' il était emprisonné en isolement total sous le régime nazi , et du champion du monde fictif de l' époque , homme particulièrement vulgaire et inculte .
Ainsi , l' intrigue du [MASK] [MASK] [MASK] [MASK] , d' [MASK] [MASK] , s' explique par une analyse rétrograde , et celle de [MASK] [MASK] [MASK] [MASK] [MASK] par la liste des coups d' une partie [MASK] -- [MASK] .
Dans [MASK] [MASK] [MASK] [MASK] , de [MASK] [MASK] , les personnages capables de " dominer " d' autres personnages les utilisent pour jouer une partie d' échecs vivante .
Dans [MASK] [MASK] [MASK] [MASK] , de [MASK] [MASK] , une modeste femme de ménage grecque découvre la puissance du jeu d' échecs .
[MASK] , dans [MASK] [MASK] [MASK] [MASK] , décrit l' habileté aux échecs comme une qualité louable chez un gentilhomme .
Le premier film réalisé autour de la thématique du jeu d' échecs est [MASK] [MASK] [MASK] [MASK] , de [MASK] [MASK] , tourné pendant le [MASK] [MASK] [MASK] [MASK] [MASK] .
Il existe aussi des films d' animation mettant en scène les échecs , comme [MASK] [MASK] [MASK] , court-métrage d' animation produit et réalisé par les studios [MASK] .
Dans [MASK] , d' [MASK] [MASK] , les deux personnages principaux sont liés par leur goût des échecs .
Dans [MASK] [MASK] [MASK] [MASK] , de [MASK] [MASK] , le suspect et celle qui le traque s' affrontent et se séduisent au cours d' une partie .
Le personnage joué par [MASK] [MASK] fait perdre ses moyens au personnage joué par [MASK] [MASK] en le provoquant par différents gestes et poses langoureux .
Citons enfin [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] ; [MASK] , de [MASK] [MASK] , jeux d' échecs et arnaques ( 2005 ) ; [MASK] [MASK] de [MASK] [MASK] , où le personnage principal , un intellectuel surdoué et misanthrope , abandonne son emploi de professeur de physique pour enseigner les échecs .
Le ballet [MASK] ( échec et mat ) a été écrit par le compositeur britannique [MASK] [MASK] en 1937 et met en scène les pièces échiquéennes jusqu' à l' assaut final du roi noir .
La comédie musicale [MASK] ( 1986 ) , sur une musique de [MASK] [MASK] et [MASK] [MASK] ( anciens membres d' [MASK] ) et des paroles de [MASK] [MASK] , met en scène un triangle amoureux entre deux participants à un championnat du monde d' échecs et une femme qui tente de séduire l' un et tombe amoureuse de l' autre .
La ville de [MASK] , [MASK] , organise une partie d' échecs sur la place publique , avec des personnages vivants costumés qui tiennent lieu de pièces .