[MASK] [MASK] est né le 4 septembre 1892 , à [MASK] .
Parmi les membres de sa famille , on compte également [MASK] [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .
[MASK] [MASK] est l' unique fils d' un négociant en amandes et d' une mère née à [MASK] .
[MASK] montre des dons précoces , tout d' abord pour le violon et la composition .
A 17 ans , en 1909 , il va à [MASK] pour étudier au [MASK] [MASK] [MASK] , jusqu' en 1915 .
Ces années sont l' occasion de multiples rencontres sur le plan musical et littéraire : il se lie d' amitié avec les musiciens [MASK] [MASK] et [MASK] [MASK] , et avec le poète [MASK] [MASK] , tué en 1915 lors de la [MASK] [MASK] [MASK] .
Il fait également la connaissance de [MASK] [MASK] et de [MASK] [MASK] en 1912 , auteurs dont il mettra les textes en musique .
La rencontre avec [MASK] [MASK] exerce aussi une influence importante .
Atteint de rhumatismes , [MASK] [MASK] est réformé de l' armée , et échappe donc aux atrocités de la guerre .
Cette amitié entre les deux artistes évolue dans le sens d' une collaboration : [MASK] , nommé ministre plénipotentiaire à [MASK] [MASK] [MASK] , propose à [MASK] de devenir son secrétaire .
[MASK] accepte .
Le mentor de toute cette équipe est [MASK] [MASK] .
[MASK] conserve cette place durant toute la guerre , et au-delà , jusqu' à 1971 .
Après la guerre , il retourne en [MASK] , en 1947 , et se voit offrir un poste de professeur de composition au [MASK] [MASK] [MASK] .
Selon ses souhaits , il est enterré à [MASK] .
Elle est décédée le 17 janvier 2008 , dans sa 106 e année , et est enterrée aux côtés de son mari , à [MASK] .
[MASK] explore toutes les possibilités de l' écriture : à la fois fin contrapuntiste , il utilise fréquemment la polyrythmie et la polytonalité , qui rendent son œuvre extrêmement riche et diverse .
Quant au [MASK] [MASK] [MASK] , il s' agit davantage d' un canular de journaliste que d' un vrai courant musical .
Cette pseudo-école , parrainée par [MASK] [MASK] , prône un retour à la musique légère , comique , et simple .
Musique symphonique [MASK] attend 1939 pour entamer l' écriture de symphonies .
Musique vocale [MASK] a grandement contribué à élargir le répertoire vocal , autant pour voix solo que pour chœur .
C' est en effet dans la musique vocale que la religion prend une place importante chez [MASK] .
Orgue [MASK] [MASK] n' a -- apparemment -- jamais joué d' orgue , mais trouvait l' instrument en soi intéressant , pas seulement pour la multiplicité de ses plans sonores mais surtout pour la grande variété de ses timbres/sonorités .