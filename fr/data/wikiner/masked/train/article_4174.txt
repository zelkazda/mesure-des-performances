Le romanche ( rumantsch en romanche ) est reconnu comme l' une des quatre langues nationales de la [MASK] depuis le 20 février 1938 , et il est considéré avec certaines restrictions comme langue officielle à l' échelle fédérale depuis la votation populaire du 10 mars 1996 .
Le romanche est " langue officielle pour les rapports que la [MASK] entretient avec les personnes de langue romanche " .
Il est parfois utilisé dans la signalisation routière du [MASK] [MASK] [MASK] .
[MASK] ton chant est aussi beau que ton apparence , alors tu es le plus beau de tous les oiseaux " .