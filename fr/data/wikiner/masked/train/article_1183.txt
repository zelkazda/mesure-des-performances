On lui doit en particulier le scénario du [MASK] [MASK] [MASK] [MASK] [MASK] .
Il naît en 1911 à [MASK] , dans la [MASK] paysanne .
[MASK] disait n' avoir gardé aucune mélancolie de son enfance , c' est parce qu' il n' en a pas tout perdu , du bonheur incessant de vivre jusqu' aux images fortes des choses les plus simples , qui sont miraculeuses , et éternelles " Elle guettait de nouveau , dragon immobile , au centre de sa rosace , au-dessus de l' eau noire .
Il rencontre l' éditeur [MASK] [MASK] au cours d' une interview et celui-ci l' embauche .
Ce ne sera pas le cas de [MASK] [MASK] : lorsque le comité d' épuration démet ce dernier de ses fonctions , [MASK] dirigera de fait la maison d' édition jusqu' à l' assassinat de l' éditeur le 2 décembre 1945 .
La tuberculose et ses lacunes financières l' empêchent de réaliser [MASK] .
Il lui expliqua ensuite la conduite à tenir pour ne plus être confronté à de tels soucis de santé , ce que [MASK] [MASK] mit en œuvre avec succès .
À l' époque ( 1942 ) où il publie ses deux premiers romans fantastiques , [MASK] fait figure de précurseur dans le désert qu' est alors la science-fiction française .
La science-fiction américaine ne débarquera en effet massivement qu' après 1945 , et encore faudra-t-il de longues années avant que des noms comme [MASK] [MASK] , [MASK] [MASK] [MASK] ou même l' ancêtre [MASK] [MASK] [MASK] soient connus de plus que quelques amateurs .
[MASK] , bien que se démarquant de la littérature de l' époque par ses thèmes fantastiques , est aussi un écrivain de son temps .
[MASK] se verra à cet égard reprocher sa signature dans différents journaux de la collaboration tels [MASK] [MASK] [MASK] et [MASK] .
Il abandonnera néanmoins rapidement cette veine collaborationniste suite au succès de [MASK] .
Si [MASK] semble nettement se méfier du progrès , ces inquiétudes étaient très présentes à l' époque ( cf .
On peut aussi y voir les regrets d' un homme de la terre devant l' exode rural qui allait s' intensifier jusque dans les années 1970 et transformer la société française de manière irréversible : [MASK] n' est-il pas dédié par l' auteur " à mes grands-pères paysans " ?
[MASK] ne manque pas , à travers l' absurde robotisation du " civilisé inconnu " ou les dérapages de l' agriculture industrielle ( la poule géante dévorant un stade de football ) , de se moquer avec cruauté des dérives de la manipulation du vivant .