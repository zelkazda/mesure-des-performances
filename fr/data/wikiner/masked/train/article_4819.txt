La [MASK] [MASK] [MASK] [MASK] [MASK] , plus connue sous son abréviation [MASK] , est l' agence gouvernementale qui a en charge la majeure partie du programme spatial civil des [MASK] .
La recherche aéronautique relève également du domaine de la [MASK] .
Depuis sa création au début des années 1960 la [MASK] joue un rôle de leader mondial dans le domaine du vol spatial habité , de l' exploration du système solaire et de la recherche spatiale .
Les missions marquantes en cours sont l' achèvement et l' exploitation de la station spatiale internationale , l' utilisation et la réalisation de plusieurs télescopes spatiaux dont le [MASK] [MASK] [MASK] [MASK] , les sondes spatiales [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] déjà lancées ou sur le point d' être lancées .
La [MASK] joue également un rôle fondamental dans les recherches en cours sur le changement climatique .
Le programme spatial habité de la [MASK] est depuis 2009 en cours de restructuration à la suite du retrait de de la navette spatiale américaine programmé pour la fin 2010 et de la remise en cause du [MASK] [MASK] confronté à des problèmes de conception et de financement .
L' [MASK] [MASK] , suivant les recommandations de la [MASK] [MASK] , prévoit d' abandonner le projet de retour d' astronautes sur le sol lunaire à l' horizon 2020 au profit d' une démarche d' exploration plus progressive qui doit être précédée par des recherches poussées notamment dans le domaine de la propulsion .
Pour pallier l' absence actuelle de moyens de lancement , la [MASK] prévoit de s' appuyer au cours de la décennie 2010 sur le secteur privé qui doit prendre en charge la desserte en orbite basse de la station spatiale internationale .
Aux [MASK] , le développement du satellite et de son lanceur est pris en charge par le [MASK] [MASK] , confié à une équipe de l' [MASK] [MASK] , mais le projet lancé tardivement et trop ambitieux , enchaîne les échecs .
Les projets militaires et leurs équipes , dont les ingénieurs commandés par [MASK] [MASK] [MASK] , sont rapidement transférés à la [MASK] .
Le premier projet de vol habité développé par la [MASK] est le [MASK] [MASK] , démarré en 1958 avant même la création de l' agence , qui doit permettre le lancement du premier américain dans l' espace .
Le 5 mai 1961 , [MASK] [MASK] effectue un premier vol de quinze minutes dans le capsule [MASK] [MASK] : mais ce n' est qu' un simple vol suborbital car la [MASK] ne dispose pas à l' époque d' une fusée suffisamment puissante .
La [MASK] mandatée par le président , doit [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] avant la fin de la décennie .
À l' issue du [MASK] [MASK] , des aspects importants du vol spatial , qui devaient être mis en application pour les vols lunaires , ne sont toujours pas maîtrisés .
Les dirigeants de la [MASK] lancent le [MASK] [MASK] destiné à acquérir ces techniques sans attendre la mise au point du vaisseau très sophistiqué de la mission lunaire .
Ce programme doit remplir trois objectifs : Le vaisseau spatial [MASK] , qui devait initialement être une simple version améliorée de la capsule [MASK] , devient un vaisseau sophistiqué de 3.5 tonnes ( contre 1 tonne environ pour le vaisseau [MASK] ) , capable de voler avec deux astronautes durant deux semaines .
Le premier vol habité [MASK] [MASK] emporte les astronautes [MASK] [MASK] et [MASK] [MASK] le 23 mars 1965 .
Au cours de la mission suivante , l' astronaute [MASK] [MASK] réalise la première sortie dans l' espace américaine .
Huit autres missions , émaillées d' incidents sans conséquence , s' échelonnent jusqu' en novembre 1966 : elles permettent de mettre au point les techniques de rendez-vous spatial et d' amarrage , de réaliser des vols de longue durée ( [MASK] [MASK] reste près de 14 jours en orbite ) et d' effectuer de nombreuses autres expériences .
A l' issue du [MASK] [MASK] , les [MASK] ont rattrapé leur retard sur l' [MASK] .
Dans le domaine des lanceurs , la [MASK] développe pour le [MASK] [MASK] la famille de lanceurs lourds [MASK] .
Le modèle le plus puissant , [MASK] [MASK] , permet de placer 118 tonnes en orbite basse , un record jamais égalé depuis .
Il est conçu pour lancer les deux vaisseaux de l' expédition lunaire : le [MASK] [MASK] et le [MASK] [MASK] [MASK] chargé de transporter les astronautes à la surface de la [MASK] .
Deux accidents graves surviennent au cours du [MASK] [MASK] : l' incendie au sol du vaisseau spatial [MASK] [MASK] dont l' équipage périt brûlé et qui entraîna un report de près de deux ans du calendrier et l' explosion d' un réservoir à oxygène du vaisseau spatial [MASK] [MASK] dont l' équipage survécut en utilisant le module lunaire comme vaisseau de secours .
La fusée géante [MASK] [MASK] de 3000 tonnes , est développée pour lancer les véhicules de l' expédition lunaire .
Le 21 juillet 1969 , l' objectif est atteint par deux des trois membres d' équipage de la mission [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .
Parallèlement au [MASK] [MASK] , la [MASK] lance plusieurs programmes pour affiner sa connaissance du milieu spatial et du terrain lunaire .
Ces informations sont nécessaires pour la conception des engins spatiaux et préparer les atterrissages sur la [MASK] .
En 1965 , trois satellites [MASK] sont placés en orbite par une fusée [MASK] [MASK] afin d' évaluer le danger représenté par les micrométéorites ; les résultats seront utilisés pour dimensionner la protection des vaisseaux [MASK] .
Les sondes spatiales [MASK] ( 1961 -- 1965 ) , après une longue série d' échecs , ramènent à compter de fin 1964 , une série de photos de bonne qualité de la surface lunaire qui permettent d' identifier des sites propices à l' atterrissage .
Le [MASK] [MASK] [MASK] , composé de cinq sondes qui sont placées en orbite autour de la [MASK] en 1966 -- 1967 , complète ce travail : une couverture photographique de 99 % du sol lunaire est réalisée , la fréquence des micrométéorites dans la banlieue lunaire est déterminée et l' intensité du rayonnement cosmique est mesurée .
Le phénomène , sous-estimé par la suite , réduira à 10 km l' altitude de l' orbite du [MASK] d' [MASK] [MASK] dont l' équipage était endormi , alors que la limite de sécurité avait été fixée à 15 km pour disposer d' une marge suffisante par rapport aux reliefs .
Le 2 juin 1966 de la même année , la sonde [MASK] [MASK] effectue le premier atterrissage en douceur sur la [MASK] fournissant des informations précieuses et rassurantes sur la consistance du sol lunaire ( le sol est relativement ferme ) ce qui permet de dimensionner le train d' atterrissage du module lunaire .
Malgré la priorité accordée au [MASK] [MASK] et à l' [MASK] [MASK] [MASK] [MASK] , la [MASK] lance également à cette époque également plusieurs missions vers les autres planètes du système solaire .
En 1962 la mission [MASK] [MASK] devient la première sonde spatiale à effectuer un survol d' une autre planète ( [MASK] ) .
[MASK] [MASK] réussit le premier survol de la [MASK] [MASK] en 1964 .
Trois autres sondes [MASK] réussissent un survol de [MASK] en 1967 et 1969 .
Dans le domaine du vol habité la période de compétition acharnée avec l' [MASK] prend fin au début des années 1970 avec la dernière [MASK] [MASK] et l' abandon par les soviétiques de son programme lunaire habité .
Un réchauffement des relations avec l' [MASK] est scellé symboliquement par le vol soviético-américain du projet [MASK] en 1975 .
La station spatiale [MASK] , un projet de station spatiale conçu à moindre frais en recyclant des composants du [MASK] [MASK] , est lancée .
Trois équipages vont l' occuper successivement en 1973-1974 en ayant recours pour leur lancement au stock restant de lanceurs [MASK] [MASK] et de vaisseaux [MASK] .
Le feu vert est arraché aux décideurs en 1972 en intégrant dans le cahier des charges de la navette les besoins du [MASK] [MASK] [MASK] [MASK] et en révisant à la baisse les ambitions initiales du programme .
[MASK] , première des quatre navettes spatiales , effectue son premier vol le 12 avril 1981 .
La catastrophe de [MASK] le 28 janvier 1986 remet en cause le dogme du tout navette et les lanceurs classiques , qui avaient été abandonné , doivent être remis en fonction .
Alors que les relations avec l' [MASK] [MASK] se dégradent à nouveau , le président [MASK] [MASK] demande en avril 1983 à la [MASK] de lancer un projet de station spatiale dédiée à la recherche scientifique et qui soit occupée en permanence .
L' [MASK] [MASK] réussit avec la sonde [MASK] [MASK] ( 1970 ) le premier atterrissage sur une autre planète du système solaire .
La [MASK] de son côté choisit de privilégier pour son programme d' exploration la planète [MASK] qui , contrairement à [MASK] , abrite peut-être la vie et qui pourrait faire l' objet dans le futur d' une mission habitée .
Alors que l' [MASK] consacre tout un programme à [MASK] , la [MASK] ne lance au cours de la décennie qu' une seule mission double vers cette planète : le projet [MASK] [MASK] , à l' étude depuis 1965 , subit plusieurs reports dus aux réductions budgétaires avant de recevoir le feu vert en 1975 et d' être lancé en 1978 .
A la place sont développées les sondes spatiales [MASK] [MASK] et [MASK] [MASK] qui sont lancées en 1971 .
La fusée de [MASK] 8 a une défaillance mais [MASK] [MASK] atteint [MASK] en 1972 et devient la première sonde spatiale à se placer en orbite autour d' une autre planète .
Mais pour répondre à la question de la vie sur [MASK] il faut faire parvenir une sonde jusqu' au sol martien pour que celle-ci puisse effectuer des mesures directes .
Le deux sondes [MASK] [MASK] lancées vers [MASK] : le programme comporte deux atterrisseurs et deux orbiteurs et constitue le premier projet d' exploration planétaire .
Dans le cadre du plan d' exploration à long terme de [MASK] , le projet [MASK] devait être suivi d' un orbiteur chargé d' étudier le climat de [MASK] et d' un rover mobile ( astromobile ) .
La seule planète interne à ne pas avoir été explorée au début des années 1970 est [MASK] .
La [MASK] décide de développer [MASK] [MASK] dans ce but .
[MASK] [MASK] est la première sonde spatiale à utiliser la technique de l' assistance gravitationnelle .
A la fin des années 1960 la [MASK] envisage également de lancer des sondes vers les planètes externes .
A l' époque les astronomes ignorent si une sonde peut se glisser à travers la ceinture d' astéroïdes située entre [MASK] et [MASK] et si le champ magnétique de [MASK] , particulièrement puissant , présente un risque pour le fonctionnement d' un engin spatial .
Pour répondre à ces interrogations le projet des sondes [MASK] [MASK] et [MASK] [MASK] est mis sur pied dès 1968 .
[MASK] [MASK] est lancée en 1972 et est la première sonde spatiale à survoler [MASK] en décembre 1973 .
La reconnaissance effectuée par les [MASK] [MASK] a préparé la voie pour les sondes [MASK] [MASK] et [MASK] [MASK] toutes les deux lancées en 1977 .
[MASK] [MASK] atteint [MASK] en 1979 , [MASK] en 1980 et collecte énormément de données inédites .
A la fin des années 1970 , la situation de la [MASK] se dégrade fortement .
Après l' achèvement du [MASK] [MASK] de nombreux salariés doivent quitter l' agence et les moyens financiers qui subsistaient sont en grande partie absorbés par le projet de la navette spatiale .
La sonde doit être lancée en 1982 par la navette spatiale mais le retard pris dans la mise au point de la navette repousse son lancement jusqu' en 1986 ; le gouvernement [MASK] envisage à un moment d' annuler le programme alors que l' engin est achevé à 90 % et il faudra des pressions officielles très importantes pour le sauver .
L' accident de [MASK] repousse son lancement jusqu' en 1989 et la sonde atteint le système de [MASK] en 1995 où elle démarre sa mission qui s' achèvera en 2003 .
Pour les remplacer des expériences scientifiques américaines sont placées sur la sonde européenne jumelle [MASK] .
En 1979 la sonde de la [MASK] qui devait être lancée vers la [MASK] [MASK] [MASK] en même temps que la sonde européenne [MASK] est également annulée .
En 1983 une nouvelle stratégie reposant sur la réalisation de sondes à coûts modérés est mise en place par la [MASK] .
La nouvelle sonde qui a été renommée [MASK] doit être lancée en 1988 mais ne le sera finalement qu' en 1989 à la suite de l' accident de [MASK] .
[MASK] remplit avec succès sa mission en effectuant une cartographie à haute résolution du sol de [MASK] entre 1990 et 1992 .
Le lancement programmé pour 1990 est repoussé à 1992 à cause de l' accident de [MASK] .
Malheureusement le contact avec la sonde est perdu au moment ou celle-ci va s' insérer en orbite autour de [MASK] .
À cette date , c' est l' erreur la plus coûteuse du programme des sondes spatiales de la [MASK] et c' est la première sonde qui subit un échec depuis 1967 .
Sa mission est en grande partie reprise par les sondes [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] lancées à la fin des années 1990 et au début des années 2000 .
Une troisième sonde [MASK] [MASK] [MASK] , qui devait compléter la couverture des deux engins précédents est un échec .
La sonde réalise avec succès sa collecte de données dans le système de [MASK] qu' elle atteint en 2004 .
Une autre mission marquante de cette époque est le [MASK] [MASK] [MASK] qui avait été construit dès 1977 et devait initialement être lancé en 1986 .
Le changement politique en [MASK] permet de mettre en place un accord de coopération spatial entre les [MASK] et la [MASK] ratifié fin 1992 par les présidents [MASK] [MASK] et [MASK] [MASK] : des astronautes américains pourront effectuer des séjours de longue durée dans la station [MASK] .
La [MASK] , qui met en application l' accord comme une répétition des vols vers la future station spatiale , règle 400 millions de dollars de coût de séjour à l' agence spatiale russe .
Plusieurs missions se succèdent entre 1995 et 1998 au cours desquelles onze astronautes américains passent 975 jours à bord de la [MASK] [MASK] vieillissante .
À neuf reprises , les navettes spatiales américaines ravitaillent la [MASK] [MASK] et assurent la relève des équipages .
Fin 1993 , la [MASK] devient également un acteur majeur du programme de la [MASK] [MASK] [MASK] qui jusqu' à présent n' a pu démarrer faute de consensus sur son financement .
La nouvelle mouture de la station spatiale doit comporter deux sous-ensembles : la partie américaine héritée du [MASK] [MASK] et la partie russe basée sur " [MASK] 2 " successeur prévu de [MASK] .
La [MASK] avec [MASK] [MASK] développe un prototype à l' échelle ½ de navette .
Le [MASK] est un engin mono-étage , entièrement réutilisable .
Au début des années 1990 deux sondes spatiales de la [MASK] très coûteuses essuient des échecs .
La mission [MASK] [MASK] échoue complètement , tandis qu' un problème d' antenne limite fortement le volume des données transmises par la sonde [MASK] .
Dans les sphères politiques , les projets d' exploration solaire qui nécessitent de longs développements et comportent une part de risque non négligeable ne sont pas bien vus et il est demandé à la [MASK] de réduire le budget consacré à chaque mission .
Les deux premières sondes sont lancées en 1996 : [MASK] doit approcher une comète et [MASK] [MASK] est un démonstrateur technologique .
Au cours de la même décennie seront également lancées la sonde lunaire [MASK] [MASK] en 1998 et [MASK] en 1999 .
A la suite de l' échec de [MASK] [MASK] , il a été décidé d' envoyer de nouvelles sondes vers [MASK] .
[MASK] [MASK] [MASK] qui reprend une grande partie des instruments de [MASK] [MASK] est la première à être lancée en 1996 : la mission est un succès et la sonde fournira des données jusqu' en 2006 .
Mais les missions suivantes [MASK] [MASK] [MASK] ( 1998 ) et [MASK] [MASK] [MASK] ( 1999 ) sont toutes deux des échecs .
La mission suivante [MASK] [MASK] [MASK] ( 2001 ) sera un succès mais désormais les sondes spatiales seront mieux financées .
A la fin des années 1980 la [MASK] tente de lancer à côté de la [MASK] [MASK] [MASK] d' autres projets importants bloqués depuis longtemps .
Son noyau est constitué par le [MASK] [MASK] [MASK] ; celui-ci doit débuter par le lancement de deux gros satellites sophistiqués .
Le satellite [MASK] est lancé en 1999 , [MASK] en 2002 et [MASK] en 2004 .
Les sondes [MASK] avaient cartographié en 1976 pratiquement l' ensemble de la [MASK] [MASK] pour identifier des sites propices à l' atterrissage .
Pour explorer l' univers proche et lointain , la [MASK] lance un certain nombre de satellites scientifiques et de télescopes spatiaux dont [MASK] ( 1972-81 ) , [MASK] ( 1977-79 ) , [MASK] ( 1983 ) , [MASK] ( 1999-2007 ) et [MASK] ( depuis 2006 ) .
L' étude du fond diffus cosmologique est au cœur des missions lancées vers 1989 avec [MASK] ( 1989-93 ) et [MASK] ( depuis 2001 ) .
Le [MASK] [MASK] [MASK] lancé en 1990 couvre la lumière visible , l' ultraviolet et le rayonnement infrarouge .
Ces derniers sont en cours de remplacement par des télescopes encore plus puissants : le [MASK] [MASK] [MASK] [MASK] ( 2008 ) et le [MASK] [MASK] [MASK] [MASK] ( vers 2013 ) .
L' aventure spatiale la plus significative de ces dernières années a probablement été la mission [MASK] [MASK] de 1997 .
Il y a aussi eu la sonde [MASK] [MASK] [MASK] , qui a réalisé des expériences de 1997 jusqu' à 2005 .
Depuis [MASK] [MASK] [MASK] cherche des preuves de l' existence passée ou présente d' eau liquide et de volcans sur [MASK] .
La [MASK] a poursuivi l' exploration de la planète rouge avec des vaisseaux comme [MASK] [MASK] [MASK] qui est arrivé dans les parages de [MASK] en 2006 .
La [MASK] [MASK] [MASK] se désintègre le 1 er février 2003 entrainant le décès de son équipage et une interruption de 29 mois des missions des navettes spatiales .
La [MASK] a décidé parallèlement au [MASK] [MASK] de faire appel au privé pour le ravitaillement et la relève des équipages de la station spatiale internationale en attendant la disponibilité des composants du [MASK] [MASK] : deux sociétés sont sélectionnées en 2006 et 2008 dans le cadre du programme [MASK] .
La viabilité du [MASK] [MASK] et les choix techniques effectués sont de plus en plus contestés .
Le président [MASK] [MASK] nouvellement élu en 2009 demande à la [MASK] [MASK] , créée pour la circonstance , d' évaluer le programme spatial habité américain .
Celle-ci souligne le manque d' ambition du [MASK] [MASK] , dont les objectifs sont proches du [MASK] [MASK] .
Le lanceur [MASK] [MASK] , disponible trop tardivement , est jugé de peu d' intérêt .
Le comité estime que la [MASK] doit s' appuyer de manière plus importante sur les opérateurs privés pour tout ce qui relève de l' orbite basse -- lanceur , vaisseau cargo et capsule habitée -- et se concentrer sur les objectifs situés au dela de l' orbite basse .
Prenant le contrepied du plan lancé par le [MASK] [MASK] , le comité recommande la prolongation jusqu' à 2020 de la durée de vie de la station spatiale internationale pour rentabiliser l' investissement effectué .
Enfin le comité fait un certain nombre de constats sur l' organisation de la [MASK] , suggérant des améliorations dans ce domaine .
La [MASK] consacre environ un quart de ses ressources financières aux activités purement scientifiques .
Parmi celles-ci la [MASK] ne dispose que du deuxième budget par ordre d' importance :
Le programme spatial habité de la [MASK] est début 2010 en pleine restructuration après l' annulation du [MASK] [MASK] et l' arrêt confirmé des navettes spatiales fin 2010 .
La [MASK] va devoir durant quelques années s' appuyer lourdement sur ses partenaires pour poursuivre le programme de la station spatiale internationale et en particulier sur l' agence spatiale russe .
Le programme [MASK] n' a pas encore débouché et ne sera manifestement pas prêt à temps pour ravitailler la station spatiale internationale fin 2010 .
Pour 2011 la [MASK] projette de consacrer 1486 millions de dollars soit 8 % de son budget aux missions d' exploration du système solaire .
En 2010 , 11 sondes sont en opérations et 5 sondes spatiales doivent être lancées d' ici 2013 ( un peu plus d' une sonde par an ) : le rover martien [MASK] , l' orbiteur [MASK] ( pour [MASK] ) et l' orbiteur lunaire [MASK] en 2011 , l' orbiteur martien [MASK] et l' orbiteur lunaire [MASK] en 2013 .
Le programme des planètes extérieures se limite en 2010 à la mission [MASK] lancée en 1997 qui étudie [MASK] et ses lunes depuis 2004 .
Cette mission très ambitieuse ( 3,3 milliards de dollars dont 2,6 pris en charge par la [MASK] ) menée en coopération avec l' [MASK] [MASK] [MASK] devrait être prolongée jusqu' en 2017 .
Une autre mission extrêmement sophistiquée , [MASK] , menée en coopération avec les agences spatiales européenne , japonaise et russe est aujourd'hui en phase de pré-étude et son financement estimé à 4,5 milliards de dollars n' est pas encore bouclé .
Son objectif est l' étude de [MASK] et de ses lunes en particulier d' [MASK] .
La planète [MASK] fait l' objet d' un programme dédié .
[MASK] [MASK] est un orbiteur qui étudie depuis 2002 la géologie de [MASK] et recherche en particulier la présence de traces d' eau .
[MASK] est un orbiteur lourd ( plus de 2 tonnes ) , embarquant une caméra particulièrement puissante , qui est entrée en service en 2006 et dont la mission principale est d' établir une cartographie détaillée de [MASK] .
Les deux rovers [MASK] , [MASK] et [MASK] poursuivent leur mission d' exploration au sol entamée en 2004 qui a été prolongée de nombreuses fois .
[MASK] , qui s' est enlisé en 2009 , fonctionne désormais comme une station fixe .
[MASK] [MASK] [MASK] ( rebaptisé [MASK] ) , rover de 775 kg qui doit arpenter la [MASK] [MASK] en emportant 70 kg d' instruments scientifiques , est le projet le plus complexe et le plus coûteux ( 1,7 milliards de dollars ) des dix dernières années .
Il doit aider les scientifiques à déterminer si la vie a pu exister sur [MASK] et à affiner l' étude du climat et de la géologie de la planète .
[MASK] est un orbiteur qui doit être lancé vers [MASK] en 2013 pour étudier son atmosphère .
Enfin la [MASK] prévoit de lancer des missions à l' horizon 2016-2018 en coopération avec l' [MASK] [MASK] [MASK] notamment dans le cadre du projet [MASK] .
Le [MASK] [MASK] [MASK] regroupe des missions ambitieuses dont le coût est néanmoins inférieur à 700 millions USD .
La première mission de ce programme , [MASK] [MASK] , a été lancée en 2006 et doit étudier [MASK] et les confins du système solaire qu' elle devrait atteindre en 2014 .
[MASK] , dont le lancement est planifié pour 2011 , doit se placer sur une orbite polaire autour de [MASK] pour étudier le champ magnétique de la planète .
A côté de missions complexes , coûteuses et longues à mettre au point mais de ce fait rares , la [MASK] développe dans le cadre du [MASK] [MASK] des missions dont le coût doit être inférieur à 425 millions de dollars et dont le délai de développement ne doit pas excéder 36 mois .
La sonde [MASK] , dont le lancement est prévu en 2011 , doit étudier précisément le champ gravitationnel et la structure interne de la [MASK] .
Plusieurs petites sondes spatiales ont été lancées ou vont être lancées vers la [MASK] .
Elles devaient , entre autres , accompagner le [MASK] [MASK] .
Ce sont les sondes [MASK] qui a entamé ses observations depuis l' orbite lunaire en 2009 , [MASK] , un orbiteur qui doit étudier l' atmosphère ténue de la [MASK] et doit être lancé en 2013 et enfin la participation au projet international ILN un projet de réseau de stations lunaires au sol chargées de recueillir des données géophysiques aujourd'hui en phase d' étude .
Le [MASK] [MASK] [MASK] est le plus connu des télescopes spatiaux de la [MASK] : bien que lancé en 1990 il doit rester en activité encore plusieurs années grâce à la dernière opération de maintenance effectuée à l' aide de la navette spatiale en 2009 .
Pour les études portant sur l' histoire de l' univers il est assisté par le télescope infrarouge [MASK] lancé en 2003 qui doit être rejoint en 2014 par le [MASK] : ce télescope infrarouge doté d' un miroir primaire de 6,5 mètres de diamètre est un projet international lourd de 4,5 milliards de $ dont 3,5 pris en charge par la [MASK] .
Celle-ci a également une participation majeure dans le télescope européen [MASK] lancé en 2009 .
Le deuxième projet en cours , [MASK] , est un télescope infrarouge aéroporté développé avec l' agence spatiale allemande et installé à bord d' un [MASK] [MASK] .
La [MASK] est un des participants de l' observatoire européen [MASK] lancé en 2009 qui étudie le fond diffus cosmologique dans le domaine des micro-ondes .
Le télescope [MASK] , lancé en 2009 est dédié à la recherche d' exoplanètes .
La [MASK] utilise également pour cette recherche le télescope terrestre [MASK] dont elle est l' un des propriétaires .
Deux autres missions sont à l' étude : [MASK] observatoire spatial utilisant les techniques d' interférométrie et un instrument dédié qui équipe le télescope terrestre [MASK] [MASK] [MASK] .
Plusieurs télescopes toujours actifs ont contribué à la mise au point de nouvelles technologies : [MASK] est un observatoire en ondes gamma lancé en 2004 .
[MASK] étudie depuis 2001 le fond diffus cosmologique dans le domaine des micro-ondes .
[MASK] est un télescope ultraviolet lancé en 2003 .
[MASK] , lancé en décembre 2009 pour une mission de 6 mois , effectue une cartographie des sources infrarouges à la recherche des galaxies les moins lumineuses , des étoiles froides situées dans la banlieue terrestre et des astéroïdes qui se trouvent dans le système solaire .
[MASK] mission conjointe avec l' [MASK] lancée en 1995 est le principal observatoire utilisé pour la météorologie spatiale et doit rester en activité jusqu' en 2013 .
[MASK] n' est plus opérationnel mais ses données sont en cours d' analyse .
L' observatoire solaire [MASK] a été lancé début 2010 .
Les deux satellites jumeaux [MASK] en activité depuis 2007 étudient notamment les éjections de masse coronale .
[MASK] , lancé en 2008 , étudie l' interaction entre le vent solaire et les vents solaires des autres étoiles .
Les cinq petits satellites [MASK] lancés en 2007 ont permis de mieux comprendre les mécanismes à l' œuvre dans les tempêtes de la magnétosphère .
[MASK] lancé en 2007 étudie la formation des nuages de haute altitude dans les régions polaires .
D' autre part le satellite [MASK] perdu lors de son lancement en 2009 doit être reconstruit et lancé en 2013 .
Pour ses activités la [MASK] dispose d' un super-ordinateur acquis en 2008 et comportant 40000 processeurs qui se classe au sixième rang par sa puissance .
[MASK] qui doit être lancé en 2011 mesure la distribution et les caractéristiques des aérosols d' origine naturelle ou artificielle .
[MASK] , projet conjoint avec la [MASK] et le [MASK] lancé en 2011 , doit permettre de valider les instruments qui seront utilisés par les futurs satellites météorologiques .
[MASK] doit prendre le relais en 2015 de [MASK] pour la mesure des calottes de glace polaires .
Des campagnes de mesures aéroportées seront assurées par la [MASK] pour assurer la continuité entre la fin de vie de [MASK] et le lancement du nouveau satellite .
[MASK] , qui effectue des mesures d' humidité du sol de la surface terrestre et de l' état des sols ( gel-dégel ) , doit être lancé en 2014 .
Deux satellites sont opérationnels : Le satellite [MASK] perdu lors de son lancement en 2009 doit être reconstruit et lancé en 2013 .
Pour ses missions d' exploration du système solaire la [MASK] a plusieurs projets consacrés à la propulsion spatiale dont le financement est rattaché à celui des sondes spatiales .
La [MASK] est le principal centre de recherche aérospatiale américain .
Le directeur en poste depuis mai 2009 est [MASK] [MASK] [MASK] ancien astronaute .
Le siège de la [MASK] se trouve à [MASK] ( [MASK] [MASK] [MASK] ) .
La [MASK] comporte 10 centres spatiaux qui emploient directement 23000 personnes ( dont 5000 au [MASK] [MASK] [MASK] ) ainsi qu' un grand nombre de sous-traitants sur site :
Parmi les installations présentes sur le site , on trouve le centre de contrôle des missions habitées ( station spatiale internationale , navette spatiale ) , les simulateurs de vol et des équipements destinés à simuler les conditions spatiales et utilisés pour tester les composants livrés par les fournisseurs de la [MASK] .
Le centre est responsable de l' [MASK] [MASK] [MASK] où est assemblé le réservoir externe de la navette spatiale .
Cette ancienne installation de l' [MASK] [MASK] [MASK] autrefois dirigée par [MASK] [MASK] [MASK] a mis au point la famille de lanceurs [MASK] .
Créé dans les années 1930 pour étudier la propulsion des fusées , à l' origine de son appellation , c' est une coentreprise entre la [MASK] et le [MASK] .
Initialement connu pour ses souffleries utilisées notamment pour mettre au point la forme de la [MASK] [MASK] , l' établissement est aujourd'hui spécialisé dans l' informatique embarquée sur les vaisseaux et sondes , les supercalculateurs , la gestion du trafic aérien ainsi que l' exobiologie .
Le centre est responsable de quelques programmes spatiaux comme les sondes lunaires [MASK] , [MASK] , le télescope spatial [MASK] et le télescope aéroporté [MASK] .
L' établissement gère également le [MASK] [MASK] [MASK] [MASK] consacré au lancement de ballons , de fusées-sondes ou de petits satellites scientifiques ( à l' aide notamment de fusées [MASK] ) .
Le [MASK] [MASK] [MASK] jouxte la [MASK] [MASK] [MASK] [MASK] [MASK] d' où sont lancés les sondes spatiales de la [MASK] .