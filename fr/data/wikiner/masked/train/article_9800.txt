En 2009 , les [MASK] [MASK] scolarisent 3167 élèves de la maternelle jusqu' au baccalauréat .
convainc le conseil municipal et le maire de [MASK] [MASK] [MASK] de lui louer les locaux publics d' une ancienne école , fermée depuis cinq ans , à [MASK] .
En 2008 , à l' occasion de l' anniversaire des 30 ans d' existence de [MASK] , divers événements festifs et culturels ( concerts , expositions , festoù-noz ) ont été organisés .
Le premier concours national de skrabell en breton a eu lieu , puis [MASK] [MASK] , course de relais de 600 km , à pied à travers [MASK] [MASK] , nuit et jour , de [MASK] à [MASK] et , de là , à [MASK] .
Cette première école est suivie d' autres écoles maternelles , d' une première école primaire en 1980 , du premier collège en 1988 et du premier lycée créé en septembre 1994 au [MASK] ( [MASK] ) avant d' être déplacé à [MASK] .
En septembre 1995 , un second collège est ouvert à [MASK] ( [MASK] ) .
Deux autres suivent , à [MASK] ( [MASK] ) et à [MASK] ( [MASK] ) , ce dernier s' installant ensuite à [MASK] ( [MASK] ) .
À la rentrée 2002 , [MASK] compte 33 écoles , 4 collèges et 1 lycée , employant quelque 250 personnes , dont 130 à plein temps .
Une nouvelle école est ouverte à [MASK] en septembre 2006 , et une autre à [MASK] [MASK] .
À la même époque , l' annexe du collège du [MASK] déménage de [MASK] pour s' installer à [MASK] .
Les collèges se trouvent au [MASK] , à [MASK] , à [MASK] , [MASK] , [MASK] et [MASK] .
Le lycée est situé à [MASK] .
En septembre 2008 [MASK] compte 37 écoles , 6 collèges et 1 lycée .
En septembre 2009 sont ouvertes deux autres écoles à [MASK] et à [MASK] .
En 2001 , dans les cinq départements de la [MASK] historique , 7365 élèves suivent un enseignement bilingue , dont 2609 dans les [MASK] [MASK] contre 2628 dans l' enseignement public et 2128 dans l' enseignement privé catholique .
Sur les 2609 élèves inscrits dans une [MASK] [MASK] , 1502 étudient dans le [MASK] , 427 dans les [MASK] , 377 dans le [MASK] , 206 en [MASK] et 97 en [MASK] .
À la rentrée 2002 , les [MASK] [MASK] accueillent 2780 élèves , soit une hausse de 6,5 % par rapport à 2000 .
En 2004 -- 2005 , dans le [MASK] [MASK] [MASK] , l' enseignement bilingue est présent , dans 18 communes sur 89 ; sur un total de 2142 élèves ( soit 5 % des effectifs [ réf. nécessaire ] ) scolarisés dans l' enseignement bilingue , 777 le sont dans un établissement de [MASK] , contre 728 dans une école catholique et 637 dans le public .
À la même période , les effectifs du collège [MASK] du [MASK] ont augmenté de 10 % , passant à 252 élèves .
Devant cet afflux , une annexe a été créée à [MASK] , où étudient 76 élèves .
À la rentrée scolaire 2006 , les effectifs affichés par l' ensemble des [MASK] [MASK] , tous niveaux confondus , sont de 2 943 élèves .
En septembre 2007 , ils sont de 2 990 élèves , dont 40 élèves de plus au lycée de [MASK] , par rapport à l' année précédente .
En septembre 2008 , le cap des 3 000 élèves est franchi ( avec 3 076 inscrits à la rentrée , hausse de près de 3 % , l' école de [MASK] a dû refuser des inscriptions faute de places ) .
Les [MASK] [MASK] sont associatives , libres et gratuites .
Cet accord est dénoncé par des syndicats d' enseignants qui demandent au [MASK] [MASK] [MASK] de statuer .
Cet arrêt interdit l' intégration de [MASK] dans l' enseignement public sur le fondement d' un bilinguisme reposant sur la méthode de l' immersion .
Devant les difficultés budgétaires de l' association , il considère que le système de financement de [MASK] -- " pour un tiers des collectivités territoriales , pour un tiers des dons , pour un tiers des animations organisées par les parents d' élèves de [MASK] " -- s' essouffle et qu' une des solutions est " que [MASK] sorte du cadre purement associatif , pour entrer dans un cadre semi-public " , à condition que les financeurs , intégrés dans le conseil d' administration de l' association , adhèrent à sa charte pédagogique .
[MASK] est une fédération d' écoles associatives .
Les écoles récentes de [MASK] ne bénéficient cependant pas du statut d' établissements privés sous contrat , la loi imposant à un établissement de fonctionner sans aides pendant cinq années avant de pouvoir prétendre à ce statut .
C' est notamment le cas de l' école [MASK] de [MASK] , créée à la rentrée 2004 .
En visite dans l' école en février 2007 , le [MASK] [MASK] [MASK] a promis de trouver de nouveaux locaux dans le [MASK] [MASK] dès septembre 2008 .
La plus grande partie du budget des [MASK] [MASK] provient donc des dons , de particuliers , soit directement aux écoles , soit reversés par la fédération [MASK] , qui collecte des fonds et les redistribue aux différentes écoles .
En 2003 , le [MASK] [MASK] [MASK] [MASK] a accordé à [MASK] une avance de 100 000 euros afin de leur empêcher de se retrouver en situation de liquidation judiciaire sous condition de négociations " avec le ministre de l' éducation nationale en vue d' une intégration au service public sur la base du bilinguisme à parité horaire " .
Les [MASK] [MASK] utilisent en maternelle et en première année de primaire la méthode dite " par immersion " , l' immersion linguistique consistant à baigner l' élève dans un environnement ne pratiquant que cette langue afin qu' elle devienne langue de vie et que l' apprentissage en soit facilité .
Les techniques d' immersion sont employées par des établissements dépendants du réseau de l' association [MASK] [MASK] .
Devant cette situation , les défenseurs de [MASK] estiment que l' annulation , par le [MASK] [MASK] [MASK] , des arrêtés du 31 juillet 2001 et du 19 avril 2002 , destinés à assurer l' intégration au sein du service public des [MASK] [MASK] dispensant un enseignement en langue bretonne , est une jurisprudence " de circonstance " .
[MASK] revendique depuis le début la sauvegarde des couleurs dialectales du breton .
De plus , les [MASK] [MASK] font régulièrement intervenir les bretonnants locaux , de langue maternelle , inconnus ou célèbres ( écrivains , chanteurs , conteurs ) .
Quand l' association [MASK] a été créée , aucun éditeur n' avait conçu d' ouvrage scolaire en langue bretonne .
Aussi les parents et enseignants de [MASK] avaient-ils élaboré des commissions pour traduire ou rédiger les manuels scolaires nécessaires à l' enseignement en breton .
Le taux de réussite était de 94 % en 2003 , de 93 % en 2004 et de 96 % en 2005 et 2006 , de 99 % en 2007 , de 90 % en 2008 et , selon le bureau de [MASK] , de plus de 98 % en 2009 .
Après la création des [MASK] [MASK] , l' enseignement public ( 1979 ) et l' enseignement privé ( 1990 ) ont développé leur propre réseau d' enseignement du breton :
En effet , en 1982 , la circulaire [MASK] autorise la mise en place d' une filière de classes bilingues .
Plusieurs classes fonctionnant à mi-temps en breton et en français sont créées dans l' enseignement public et privé , notamment à [MASK] , [MASK] , [MASK] , [MASK] ou [MASK] .
Au contraire de [MASK] , ces écoles pratiquent un enseignement dit " à parité horaire " .
Les opposants à [MASK] sont généralement des opposants à l' enseignement du breton et de façon plus générale au bilinguisme , soit parce qu' ils lui supposent des liens avec un nationalisme breton , soit parce qu' ils se posent en défenseurs du principe " Une langue , un peuple , une nation , un état " et du système éducatif français , de préférence public , et basé sur une langue unique , le français .
Ainsi , une polémique est née du fait que deux des écoles de [MASK] portaient le nom d' écrivains bretons , anciens collaborateurs :
Par ailleurs , [MASK] a été mise en cause à propos de l' un des ouvrages scolaires en breton qu' elle avait traduit .
Toutefois , l' article de [MASK] [MASK] , qui est à l' origine de cette dernière polémique , ne précise en rien si , dans le contenu du manuel , il a trouvé des éléments critiquables .
Sa seule remarque est : " Sur l' ouvrage un curieux avertissement : le livre n' ayant pas été " corrigé " , il ne doit pas être diffusé en dehors de [MASK] .
En fait le manuel en question contient un total de 210 pages , dont 24 ( soit 11,4 % ) traitent spécifiquement de la [MASK] pendant la période au programme .
La charte de [MASK] ( 1977 ) , qui revendique " son hostilité à toute uniformisation linguistique " , affirme que " le breton enseigné dans les écoles maternelles [MASK] est celui utilisé dans leur environnement géographique et humain " , et l' intervention dans les classes [MASK] de bretonnants de naissance est mise en avant .
Cependant , certains comme [MASK] [MASK] , qui a brièvement côtoyé les [MASK] [MASK] à leur création ( fin des années 1970 ) , ou comme [MASK] [MASK] , qui n' a pas fait d' études de linguistique bretonne , considèrent que le breton enseigné ( le même que dans les écoles publiques et les écoles privées catholiques bilingues ) est du breton unifié , et donc ne correspondrait pas aux multiples formes parlées " d' origine " .
Les [MASK] [MASK] , du fait de leur impact médiatique et de leur réussite pédagogique , sont au cœur de cette polémique .
Reprenant les propos de [MASK] [MASK] sur les écoles par immersion , il explique que , " successivement , le ministre nie leur laïcité , parle de " terrorisme intellectuel " , invoque sournoisement des " stratégies groupusculaires " , un pseudo-passé nazi ( " racines historiques sulfureuses de ces fondamentalistes " ) et , sans crainte de la contradiction , " des vieilleries soixante-huitardes " .
Les prises de positions de [MASK] [MASK] ont également été critiquées , au niveau régional , parmi les socialistes bretons .
En tant qu' écoles privées , les [MASK] [MASK] peuvent bénéficier de subventions des communes où elles sont implantées .
Elles sont limitées à 10 % de leur budget par la [MASK] [MASK] .
Un certain nombre de communes ne remettent pas le forfait scolaire aux écoles [MASK] sous prétexte qu' elles ne sont pas implantées sur le territoire de la commune d' habitation des enfants , ce qui prive les écoles [MASK] d' une part importante du financement public prévu par la loi .
Deux auteurs ont exprimé l' importance de [MASK] à leurs yeux par des chansons .
Diverses écoles ont produit leur propres albums , comme celle de [MASK] en 2009 .