L' histoire de la [MASK] est traditionnellement divisée en deux grandes parties .
Des vestiges de l' époque néolithique existent à [MASK] où se trouvait une mine préhistorique de silex .
Les écrits de [MASK] [MASK] [MASK] [MASK] [MASK] forment le début de l' histoire écrite .
Ainsi parlait [MASK] [MASK] des tribus qui ont donné tant de fil à retordre à ses légions .
[MASK] [MASK] justifiait ainsi les cinq années dont il avait eu besoin pour faire plier ces féroces guerriers .
Elle recouvre tout le nord-est de la [MASK] actuelle , de la [MASK] à la [MASK] , ainsi que tout l' ouest de la [MASK] .
La sécurité des frontières face aux [MASK] est assurée par les légions et les premières voies de communication sont créées d' une part entre [MASK] et [MASK] , d' autre part entre [MASK] et [MASK] .
Plusieurs bourgs sont créés à l' intersection de ces axes , tels par exemple [MASK] et [MASK] , ou encore [MASK] et [MASK] .
Sous l' impulsion de [MASK] , la vallée de la [MASK] devient le centre politique et économique de l' empire carolingien .
Son règne fut marqué à partir de 829 par les querelles entre le souverain et ses fils , [MASK] , [MASK] , [MASK] et [MASK] .
En août 843 est enfin conclu le [MASK] [MASK] [MASK] , qui divise l' empire entre les trois petits-fils de [MASK] .
Les terres à l' ouest de l' [MASK] ( le futur [MASK] [MASK] [MASK] ) revenaient à [MASK] [MASK] [MASK] .
Ce fut [MASK] [MASK] qui obtint la partie septentrionale , le territoire entre la [MASK] [MASK] [MASK] et le [MASK] , auquel s' attacha spécialement le nom de [MASK] .
[MASK] [MASK] [MASK] , fils de [MASK] [MASK] [MASK] se mit en marche par [MASK] et [MASK] et , dans l' impossibilité où ils étaient de lui opposer une résistance efficace , les grands demeurés fidèles aux princes préférèrent abandonner à l' agresseur la partie occidentale de la [MASK] ( été de 879 ) .
Seul le [MASK] [MASK] [MASK] demeurait aux fils de [MASK] [MASK] [MASK] .
[MASK] reçut en 895 d' [MASK] [MASK] [MASK] le gouvernement de la [MASK] avec titre de roi .
Les souverains du royaume occidental essaient à plusieurs reprises de s' emparer de la [MASK] .
[MASK] [MASK] [MASK] envahit ainsi le pays en 911 et ce n' est qu' en 923 que [MASK] [MASK] [MASK] parvint à réunir la [MASK] à l' [MASK] .
Au nord , elle est bordée par la [MASK] .
La région devient l' un des cœurs de l' économie européenne , avec l' [MASK] .
Les principales villes sont alors , à l' ouest , [MASK] , [MASK] , [MASK] et [MASK] , et en pays mosan , [MASK] , [MASK] , [MASK] et [MASK] .
À cette époque , les affluents de l' [MASK] sont navigables et le trafic commercial entre la [MASK] et le [MASK] augmente .
La garnison française à [MASK] est massacrée lors de la révolte des [MASK] [MASK] [MASK] le 18 mai 1302 , et l' ost royal est écrasé par les milices communales à la [MASK] [MASK] [MASK] , dite des éperons d' or le 11 juillet de la même année .
Cette bataille est aujourd'hui considérée comme la naissance de la nation flamande , bien que la [MASK] actuelle et le [MASK] [MASK] [MASK] de l' époque ne se recouvrent que partiellement .
[MASK] [MASK] [MASK] obtiendra sa revanche à la [MASK] [MASK] [MASK] le 18 août 1304 .
[MASK] devient alors une des capitales économique et financière du nord-ouest de l' [MASK] .
Ces troubles privent [MASK] de sa prépondérance économique .
C' est un peu plus tard qu' [MASK] prendra sa place .
Il faut toutefois rappeler que ce mot " belge " fait référence aux [MASK] [MASK] [MASK] , à l' exclusion donc de la [MASK] [MASK] [MASK] .
Les [MASK] ne durèrent qu' une année , néanmoins le qualificatif belgique s' imposa définitivement pour désigner les [MASK] [MASK] pendant la période française .
La période française que connurent les territoires qui forment actuellement la [MASK] est une époque charnière dans l' histoire de ce pays :
À la tête de ce [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] .
Cette opposition est surtout marquée en [MASK] , et menée par l' évêque de [MASK] .
Le second antagonisme est économique : les libéraux n' ont plus accès au marché français , tandis qu' ils doivent faire face au libre-échange avec l' [MASK] , et son industrie fort avancée .
De plus , le gouvernement favorisait les investissements dans le nord et le roi ira même jusqu' à freiner le développement du port d' [MASK] pour favoriser les ports du nord .
Dans les faits , l' arrêté royal du 15 septembre 1819 établissait le néerlandais comme langue officielle ( dans les provinces néerlandophones ) pour la justice et l' administration , et les langues populaires n' étaient pas protégées ( ainsi en ira-t-il de l' allemand au [MASK] ) .
L' établissement d' une politique linguistique pro-néerlandais a été si graduelle , qu' il existait d' ailleurs très peu de liens culturels entre la [MASK] et [MASK] [MASK] proprement dit .
Le 25 août 1830 , peu après la [MASK] [MASK] [MASK] en [MASK] , [MASK] se soulève .
Il recherche un roi , adresse une demande au [MASK] [MASK] [MASK] , fils de [MASK] , qui refuse par prudence .
Le 4 octobre 1830 , l' indépendance est proclamée par le gouvernement provisoire formé le 26 septembre 1830 , et le 3 novembre un [MASK] [MASK] est élu par 30000 électeurs et s' ouvre le 10 novembre 1830 .
Le 18 novembre , le congrès national belge confirmera en quelque sorte le décret du 4 octobre au sujet de l' [MASK] [MASK] [MASK] [MASK] .
En [MASK] comme en [MASK] et à [MASK] , le peuple use de langues régionales .
Entre-temps , de février 1831 au 21 juillet 1831 , la régence sera assurée par le baron [MASK] [MASK] [MASK] [MASK] .
L' essentiel de la puissance industrielle du pays se trouvait toutefois en [MASK] , où existait déjà une tradition séculaire du travail du fer et de l' extraction du charbon .
L' actuelle [MASK] [MASK] [MASK] [MASK] devient son domaine personnel .
Le nombre de victimes indigènes ne peut faire que l' objet d' estimations ( voir [MASK] [MASK] [MASK] [MASK] [MASK] , voir aussi l' avis de [MASK] [MASK] ) .
Cette vision " paternaliste " fut très répandue en [MASK] , jusque dans les années 1930 .
La violation de la neutralité belge déclenche l' entrée en guerre du [MASK] .
Or la condition de réussite du [MASK] [MASK] , c' était la rapidité .
Le pays sera finalement entièrement occupé pendant toute la guerre sauf derrière la ligne de front de l' [MASK] .
Le gouvernement est contraint de se réfugier à [MASK] , dans la banlieue du [MASK] .
Le gouvernement belge et le souverain réclamaient le paiement d' indemnité de guerre et la suppression du statut de neutralité imposé par le [MASK] [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] , qui était opposé à une trop grande humiliation de l' [MASK] , intervint lui-même à la conférence , à la demande de [MASK] [MASK] .
Il réclama des indemnités de guerre et la révision du [MASK] [MASK] [MASK] [MASK] concernant le statut de l' [MASK] .
Il ne semble pas avoir réclamé le [MASK] [MASK] et le [MASK] [MASK] [MASK] , ni même [MASK] et [MASK] .
Les revendications autonomistes flamandes , excitées durant l' occupation allemande de 1914-1918 , prirent donc de plus en plus d' importance , ce qui força le roi à autoriser la néerlandisation de l' université de [MASK] en 1930 .
À la veille de l' invasion allemande , le roi divisa ses troupes en envoyant la moitié d' entre elles face à la [MASK] .
Fin 1941 , [MASK] [MASK] , qui avait gardé une certaine popularité en restant au milieu de son peuple , choque profondément l' opinion en épousant [MASK] [MASK] ( sans respecter les règles légales ) -- une décision mal perçue , alors que ses soldats wallons resteront en captivité coupés de leur familles jusqu' à la fin de la guerre .
À [MASK] , la municipalité accepte de distribuer les étoiles jaunes et prête sa police à l' organisation des rafles , au contraire de la mairie de [MASK] .
Les résistants arrêtés sont souvent torturés , notamment au siège de la [MASK] bruxelloise [MASK] [MASK] , ou encore au sinistre [MASK] [MASK] [MASK] .
Il est vrai que , en [MASK] , certains idéalistes tablent sur la collaboration pour faire avancer les objectifs du mouvement flamand : 62 des condamnés pour collaboration seront flamands .
La décision de [MASK] [MASK] de rentrer malgré tout provoque des troubles insurrectionnels en [MASK] .
L' abdication de [MASK] [MASK] en faveur de son fils [MASK] permet de sauver l' unité belge et le retour au calme , mais ce qu' on appela la question royale marquera définitivement une rupture dans l' unitarisme .
Le 30 juin 1960 , la colonie du [MASK] [MASK] accède à l' indépendance après seulement six mois de préparation hâtive .
[MASK] [MASK] s' emparera du pouvoir , et enverra [MASK] en prison au [MASK] , où il sera assassiné avec la complicité des services secrets belges .
À partir de 1980 la corruption et la mauvaise gestion du régime de [MASK] va plonger le [MASK] dans la misère et la guerre civile .
La [MASK] est devenue graduellement la région la plus productive et puis la plus riche du pays .
Dans cette période , l' anglais a tendance à remplacer le français comme première langue étrangère en [MASK] .
Dix ans plus tard , la [MASK] marqua son souhait de , déjà , modifier cette constitution en vue de l' établissement d' un état confédéral .
Tout le pays en subit un choc profond : on estime à 500000 le nombre de personnes qui ont fait la file durant de nombreuses heures pour s' incliner devant son cercueil au palais de [MASK] .