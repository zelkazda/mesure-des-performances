L' hymne national mexicain ( en espagnol : [MASK] [MASK] [MASK] ) fut adopté en 1854 .
En 1854 , [MASK] [MASK] composa la musique qui accompagne aujourd'hui le poème de [MASK] .
Le 12 novembre 1853 , le président [MASK] [MASK] [MASK] [MASK] [MASK] annonça un concours pour écrire un hymne national pour le [MASK] .
[MASK] [MASK] [MASK] , poète talentueux , n' était pas intéressé par cette compétition .
Dans la chambre où il était momentanément séquestré , il y avait des images dépeignant divers événements de l' [MASK] [MASK] qui l' inspirèrent à se mettre au travail .
Le lauréat fut [MASK] [MASK] , mais sa musique ne plut pas à la population .
C' est la raison pour laquelle une seconde compétition d' accompagnements au texte de [MASK] [MASK] [MASK] fut organisée .
Il avait été invité à les diriger par le président [MASK] [MASK] , qu' il avait rencontré à [MASK] .
L' hymne fut officiellement adopté le jour du [MASK] [MASK] [MASK] , le 16 septembre 1854 .