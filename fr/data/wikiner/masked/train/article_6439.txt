Une fête ou un party ( au [MASK] , selon le contexte ) est un événement organisé pour célébrer quelque chose ou quelqu' un .
Elle est aussi ce qui peut conférer une raison d' être à la quotidienneté , d' où la tentation de multiplier les occasions de fêtes , au point , note [MASK] [MASK] , que " certaines nations , certaines cultures se sont englouties dans la fête " .
Elle est plutôt de l' ordre de ce que [MASK] appelait l ' " adhérence " .
Néanmoins , selon [MASK] [MASK] , c' est parce que sous nos climats l' ivresse et le masque ne vont guère de pair que nos fêtes ne prennent pas un tour plus violent .
À lire [MASK] , la jouissance est déjà engloutissement du temps et de la signification , étourdissement .
Selon [MASK] [MASK] , il s' agit de " s' engloutir dans le présent " , ce qui impose de renoncer à " la durée où s' accumulent le savoir et les actions concertées humaines " .
Elle ne convoque tout ce qui conteste l' ordre social que pour mieux l' intégrer , et mettre en scène l' éternel retour de l' ordre immuable ( [MASK] [MASK] ) .
Au-delà des fêtes religieuses , il existe des fêtes familiales fixes ( " des partys de famille " au [MASK] ) , certaines propres à chaque famille , d' autres de caractère universel : anniversaires , anniversaires de mariage , ou fêtes des saints dans les familles chrétiennes .