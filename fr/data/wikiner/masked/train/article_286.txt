L' [MASK] comprend 419 communes .
Le tableau suivant en donne la liste , en précisant leur code [MASK] , leur code postal principal , leur appartenance aux principales structures intercommunales en 2008 , leur superficie , leur population et leur densité .
Suivant la classification de l' [MASK] , la typologie des [MASK] [MASK] [MASK] [MASK] se répartit ainsi :
Note : les données présentées ici ne concernent que les communes appartenant à l' [MASK] .
Il est possible qu' une aire urbaine s' étende sur plusieurs départements ( c' est le cas de celle de [MASK] de celle de [MASK] ) .