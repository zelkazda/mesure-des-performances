La première voiture produite par [MASK] [MASK] est terminée en 1913 , elle était destinée à concurrencer [MASK] .
En 1932 , une [MASK] [MASK] gagne la coupe biannuelle du [MASK] .
En 1948 la marque remporte les [MASK] [MASK] [MASK] [MASK] .
Ces voitures gagneront trois fois successivement les [MASK] [MASK] [MASK] [MASK] et seront victorieuses dans de très nombreuses courses internationales ( notamment les [MASK] [MASK] [MASK] [MASK] en 1959 ) .
En 1987 , le groupe [MASK] devient actionnaire majoritaire de la firme ( puis acquiert la totalité des actions en 1993 ) .
et le préparateur automobile [MASK] [MASK] créent l' écurie de course automobile [MASK] [MASK] [MASK] .
Une nouvelle page s' est tournée le 12 mars 2007 pour [MASK] [MASK] .
Les voitures [MASK] [MASK] sont récurrentes dans les films de [MASK] [MASK] .