Il est diplômé de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , et titulaire d' un diplôme d' études supérieures de droit public ( [MASK] ) , d' un diplôme d' études approfondies de propriété intellectuelle ( [MASK] [MASK] [MASK] ) et ancien élève ( doctorat ) de l' [MASK] [MASK] [MASK] [MASK] [MASK] .
En 1983 , [MASK] [MASK] est élu conseiller municipal de [MASK] , puis maire en 1989 .
Il a fait procéder au classement du parc de [MASK] et de nombreux bâtiments au titre du patrimoine [ évasif ] .
En 1994 , il a mobilisé les professionnels et des parlementaires afin de sauver [ non neutre ] l' [MASK] [MASK] [MASK] menacé [ non neutre ] de fermeture par la société mère des courses [MASK] [MASK] .
Il a ainsi triplé le parc de logements sociaux à [MASK] pour arriver à un total de 7 % du nombre de logements , en évitant les programmes d' envergure [ réf. nécessaire ] .
[MASK] [MASK] est élu député de la [MASK] [MASK] [MASK] [MASK] [MASK] en 1993 .
Il siège au sein du groupe [MASK] de 1993 à 2002 .
En 2003 , il défend la position française sur l' [MASK] [ réf. nécessaire ] .
Il est sollicité régulièrement par les médias étrangers pour commenter l' actualité politique française , sur la [MASK] , et [MASK] notamment [ réf. nécessaire ] .
Il participe notamment aux travaux préparatoires au projet de loi sur les jeux en ligne qui devra revoir le monopole du [MASK] par rapport aux règles de concurrence européenne .
Le porte-parole de l' [MASK] lui reprochera par la suite de n' avoir " pas compris internet "
En 2009 toujours , il fait partie de la commission d' enquête parlementaire sur le port du voile intégral en [MASK] , dans laquelle il se sent " victime " de ne pas regarder le visage des femmes .