Elle se développa au cours de l' [MASK] avec l' avènement des civilisations méditerranéennes : sumérienne , égyptienne , grecque et romaine .
On attribue généralement l' invention du pain aux [MASK] , qui en auraient fait la découverte par hasard .
De la pâte à pain sans levain ( eau , lait et farine d' orge et de millet ) aurait été oubliée , se serait " gâtée " , mais aurait tout de même été cuite , menant ainsi à la découverte du pain avec levain que les [MASK] enrichirent parfois de graisse , d' œufs ou de miel .
Les [MASK] développent le métier de boulanger et confectionnent plus de 70 variétés de pain , en utilisant pour faire lever la pâte des levures issues du vin et conservées en petites amphores .
Pour les [MASK] aussi , le pain est , avec la bouillie , l' aliment de base .
Ils créent à [MASK] , sous [MASK] , un collège de meuniers-boulangers et à divers endroits de grandes meuneries-boulangeries .
Le pain , à certaines époques , est distribué gratuitement à la population pauvre de [MASK] pour éviter les émeutes .
Les pains entiers retrouvés à [MASK] sont entaillés en rayons selon l' habitude grecque , ce qui permet un partage aisé .
La panification recule pendant les invasions [MASK] , notamment à cause du non-entretien ou de la destruction des moulins à eau gallo-romains .
En [MASK] [MASK] [MASK] nait l' habitude de donner au nécessiteux des méreaux , ou jetons alimentaires , donnant généralement droit à du pain .
Les émeutes sont nombreuses et aboutissent en [MASK] à la [MASK] [MASK] après la marche des [MASK] les 5 et du 6 octobre 1789 emmenés par [MASK] [MASK] [MASK] [MASK] chez le [MASK] , la [MASK] et le [MASK] [MASK] .
En 1793 , la banalité des moulins et des fours est abolie en [MASK] .
En 1860 , [MASK] [MASK] identifie la levure comme le micro-organisme responsable de la fermentation alcoolique , et montre qu' elle peut vivre aussi bien en présence qu' en l' absence d' oxygène .
Lors de la [MASK] [MASK] [MASK] , les " pains noirs " et pains de guerre ( longue conservation ) sont utilisés par les armées et sur le front .
L' [MASK] [MASK] [MASK] recense une trentaine de noms de pains .
Pour l' [MASK] on compte entre 300 et 600 sortes de pain , qui se distinguent par la forme , la farine ou la méthode de production .
Le pain contient en moyenne 19 g /kg de sel , une baguette ( 250 g ) 4.7 g soit presque la quantité maximale recommandée par l' [MASK] ( 5 g/jour/personne ) .
Les pains traditionnels [MASK] sont le lavash , ce qui signifie " regarde bien " , et le matnakash , qui signifie " tiré des doigts " .
Le pain est porté en procession dans des cérémonies religieuses par les [MASK] qui en offrent aux défunts .
Les prophètes [MASK] , [MASK] ont fait le miracle de la multiplication des pains .
En hébreu , [MASK] signifie " la maison du pain " et les chrétiens voient dans le fait que [MASK] soit né dans une ville de ce nom , la signification de son sacrifice via l' eucharistie .