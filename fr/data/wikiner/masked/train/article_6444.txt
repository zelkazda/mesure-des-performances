Le flamand est , au sens large et dans le domaine linguistique , un dénominateur de tout dialecte parlé quelque part sur l' ancien territoire du [MASK] [MASK] [MASK] .
Cela s' étend du sud des [MASK] ( [MASK] ) au nord de la [MASK] .
Quelques tentatives marginales ont été entreprises pour établir celui-ci en tant que langue " catholique " à part entière sur base de la littérature de [MASK] [MASK] , mais celles-ci n' ont jamais abouti .
Entre le néerlandais parlé aux [MASK] et celui parlé en [MASK] , certains mots et usages de mot sont différents :