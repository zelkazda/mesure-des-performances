Son origine n' est pas claire : il semble être natif de [MASK] , ou du moins du diocèse de [MASK] .
Il applique ainsi les préceptes du pape [MASK] [MASK] à plusieurs niveaux :
Sous [MASK] [MASK] ( 1088-1099 ) , il retrouve ses pouvoirs de légat , traitant en particulier de la question de l' adultère royal de [MASK] [MASK] [MASK] qui a enlevé [MASK] [MASK] [MASK] .
Il meurt le 6 octobre 1106 à [MASK] , sur la route du concile de [MASK] .