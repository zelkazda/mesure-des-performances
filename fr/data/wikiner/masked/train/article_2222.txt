Le genre naît véritablement aux [MASK] dans les années 1920 , avec pour ambition de rendre compte de la réalité sociétale du pays : crime organisé et terreau mafieux : société clanique , anomie sociale , corruption politique et policière , violences et insécurités urbaines ... mais qui a véritablement connu son essor après la [MASK] [MASK] [MASK] .
En août 1944 , quelques jours avant la [MASK] [MASK] [MASK] , [MASK] [MASK] découvre trois livres ; deux sont de [MASK] [MASK] et le troisième de [MASK] [MASK] [MASK] ( tous deux auteurs britanniques ) .
Il en confie la traduction à [MASK] , avec l' idée d' une collection et [MASK] [MASK] en trouve le titre : ce sera la [MASK] [MASK] .
Côté français , [MASK] [MASK] y publie une vingtaine de romans .
En plus les romans de [MASK] [MASK] sont à mentionner .
Le commentateur français par excellence restera toutefois [MASK] [MASK] .
Lui-même auteur des quelques romans , c' est toutefois son travail d' éclaireur pour le genre en tant que critique dans les pages de [MASK] qui le signalera comme le véritable exégète du roman noir .
Moins médiatique que [MASK] , [MASK] a lui aussi su créer un style très personnel qui sera reconnu de tous , au-delà des considérations idéologiques .