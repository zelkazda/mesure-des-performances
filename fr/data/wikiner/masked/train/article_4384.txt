Elle est située dans le [MASK] et constitue , avec quelques îlots avoisinants , une commune dont le nom s' écrit alors [MASK] .
L' [MASK] [MASK] [MASK] est située à environ 5 milles marins ( 8 km ) de la [MASK] [MASK] [MASK] .
Elle fait partie d' une arête granitique dont la partie immergée se prolonge sur 25 kilomètres vers le large et forme la barrière de récifs appelée la [MASK] [MASK] [MASK] .
Cette chaussée s' interrompt un peu au-delà du [MASK] [MASK] [MASK] .
L' [MASK] [MASK] [MASK] s' étend sur environ 2 kilomètres .
Elle se situe au milieu d' une zone de récifs très étendue et particulièrement dangereuse , la [MASK] [MASK] [MASK] parcourue par des courants souvent violents qui dépassent 6 nœuds en vives eaux .
Au XIX e siècle , plusieurs phares ont été construits sur son pourtour , dont le plus célèbre est le [MASK] [MASK] [MASK] .
L' île est desservie quotidiennement , sauf conditions météorologiques défavorables , par une vedette de la compagnie maritime [MASK] qui assure la liaison aller et retour avec le port d' [MASK] qui se situe une vingtaine de kilomètres plus à l' est sur le continent .
L' [MASK] [MASK] [MASK] a eu son heure de gloire , lorsque les 128 pêcheurs de l' île l' ont quittée sur six bateaux pour répondre à l' appel du [MASK] [MASK] [MASK] .
Ce qui valut un éloge de la part du [MASK] [MASK] [MASK] : " l' [MASK] [MASK] [MASK] est un quart de [MASK] [MASK] " .
L' [MASK] [MASK] [MASK] est l' une des cinq communes françaises qui ont été faites [MASK] [MASK] [MASK] [MASK] .
Sur un plan administratif , la commune de [MASK] [MASK] fait partie du pays du [MASK] [MASK] en [MASK] et du canton de [MASK] .
L' [MASK] [MASK] [MASK] fait partie du [MASK] [MASK] [MASK] [MASK] [MASK] , du [MASK] [MASK] [MASK] [MASK] [MASK] et de l' association des [MASK] [MASK] [MASK] .
L' évolution du nombre d' habitants depuis 1793 est connue à travers les recensements de la population effectués à [MASK] depuis cette date :
La démographie de l' île est donnée dans le graphique ci-contre , qui correspond à la population de la commune de [MASK] [MASK] .
L' [MASK] [MASK] [MASK] est isolée du réseau électrique continental .
À l' ouest de l' île , non loin du phare , une petite chapelle isolée entourée d' un muret est dédiée à [MASK] [MASK] , l' évêque qui évangélisa la région .
La chapelle [MASK] [MASK] fut bénie le 13 août 1972 .
[MASK] [MASK] [MASK] fit paraître en 1900 un très beau roman qui a pour sujet le phare dit de [MASK] car posé sur le rocher du même nom , l' intrigue se déroule sur le [MASK] [MASK] [MASK] [MASK] où va se jouer toute une intrigue amoureuse qui se terminera de manière tragique .
[MASK] [MASK] a écrit deux romans sur l' [MASK] [MASK] [MASK] ( voir bibliographie plus bas ) .
Un feu s' allume sur la mer raconte la construction du phare d' [MASK] [MASK] et évoque la société de l' [MASK] [MASK] [MASK] de l' époque ( années 1860 ) .