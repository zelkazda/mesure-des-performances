[MASK] [MASK] explique ainsi que les premiers philosophes ont été amenés à faire de la science ( sans que les deux ne soient confondues ) .
[MASK] [MASK] explique ainsi que " ce que nous mettons sous le vocable " science " n' est en rien un objet circonscrit et stable dans le temps qu' il s' agirait de simplement décrire " .
Néanmoins le rapport entre l' opinion d' une part et la science d' autre part n' est pas aussi systématique ; l' historien des sciences [MASK] [MASK] pense en effet que la science s' ancre dans le sens commun , qu' elle doit " sauver les apparences " .
La sociologie des sciences analyse notamment cette articulation entre science et opinion ; leurs rapports sont davantage complexes et ténus que ne le croyait [MASK] [MASK] lorsqu' il expliquait que " L' opinion pense mal , elle ne pense pas " .
L' épistémologue [MASK] [MASK] écrit ainsi qu' il est " utopique de vouloir donner une définition a priori de la science " .
L' épistémologue [MASK] [MASK] [MASK] parle ainsi , bien plutôt , des " paradigmes de la science " comme des renversements de représentations , tout au long de l' histoire des sciences .
[MASK] énumère ainsi un nombre de " révolutions scientifiques " .
[MASK] [MASK] distingue ainsi entre l' histoire des connaissances scientifiques et celle de la pensée scientifique .
Cependant , pour de nombreux paléontologues et préhistoriens comme [MASK] [MASK] , l' art pariétal montre que l' homme d' alors possédait les mêmes facultés cognitives que l' homme moderne .
Les travaux du français [MASK] [MASK] , spécialiste de la technique , explorent les évolutions à la fois biopsychiques et techniques de l' homme préhistorique .
Les sciences étaient alors le fait des scribes , qui , note [MASK] [MASK] , se livraient à de nombreux " jeux numériques " qui permettaient de lister les problèmes .
La [MASK] crée ainsi les premiers instruments de mesure , du temps et de l' espace ( comme les gnomon , clepsydre , et polos ) .
L' [MASK] [MASK] va développer l' héritage pré-scientifique mésopotamien .
En astronomie , ils identifient la [MASK] [MASK] [MASK] et comprennent la périodicité des éclipses .
Durant la période des [MASK] [MASK] , apparaît l' arbalète .
En 132 , [MASK] [MASK] invente le premier sismographe pour la mesure des tremblements de terre et est la première personne en [MASK] à construire un globe céleste rotatif .
En mathématique , [MASK] [MASK] et [MASK] [MASK] étudient les systèmes linéaires et les congruences ( leurs apports sont généralement considérés comme majeurs ) .
La civilisation dite de la vallée de l' [MASK] ( -3300 à -1500 ) est surtout connue en histoire des sciences en raison de l' émergence des mathématiques complexes ( ou " ganita " ) .
[MASK] [MASK] [MASK] ( v. 625-547 av .
et [MASK] ( v. 570-480 av .
Ces premières recherches sont marquées par la volonté d' imputer la constitution du monde ( ou " cosmos " ) à un principe naturel unique ( le feu pour [MASK] par exemple ) ou divin ( l ' " Un " pour [MASK] ) .
Constatant que la raison d' une part et les sens d' autre part conduisent à des conclusions contradictoires , [MASK] opte pour la raison et estime qu' elle seule peut mener à la connaissance , alors que nos sens nous trompent .
Cet exemple est illustré par les célèbres paradoxes de son disciple [MASK] .
Si [MASK] est d' un avis opposé concernant le mouvement , il partage l' idée que les sens sont trompeurs .
Il faudra attendre [MASK] pour aplanir l' opposition entre les deux courants de pensée mentionnés plus haut .
Avec [MASK] et [MASK] , qui en rapporte les paroles et les dialogues , la raison : logos , et la connaissance deviennent intimement liés .
[MASK] ouvre ainsi la voie à la " mathématisation " des phénomènes .
C' est surtout avec [MASK] , qui fonde la physique et la zoologie , que la science acquiert une méthode , basée sur la déduction .
Pour [MASK] , la science est subordonnée à la philosophie ( c' est une " philosophie seconde " dit-il ) et elle a pour objet la recherche des premiers principes et des premières causes , ce que le discours scientifique appellera le causalisme et que la philosophie nomme l ' " aristotélisme " .
Néanmoins , [MASK] est à l' origine d' un recul de la pensée , par rapport à certains pré-socratiques quant à la place de la terre dans l' espace .
À la suite d' [MASK] [MASK] [MASK] , il imagine un système géocentrique et considère que le cosmos est fini .
La ville égyptienne d' [MASK] en est le centre intellectuel et les savants d' alors y sont grecs .
Les travaux d' [MASK] ( -292 à -212 ) sur sa poussée correspond à la première loi physique connue alors que ceux d' [MASK] ( -276 à -194 ) sur la circonférence de la terre ou ceux d' [MASK] [MASK] [MASK] ( -310 à -240 ) sur les distances terre-lune et terre-soleil témoignent d' une grande ingéniosité .
[MASK] [MASK] [MASK] modélise les mouvements des planètes à l' aide d' orbites excentriques .
[MASK] [MASK] [MASK] ( -194 à -120 ) perfectionne les instruments d' observation comme le dioptre , le gnomon et l' astrolabe .
[MASK] rédige également un traité en 12 livres sur le calcul des cordes ( nommé aujourd'hui la trigonométrie ) .
à 165 ) prolonge les travaux d' [MASK] et d' [MASK] sur les orbites planétaires et aboutit à un système géocentrique du système solaire , qui fut accepté dans les mondes occidental et arabe pendant plus de mille trois cents ans , jusqu' au modèle de [MASK] [MASK] .
[MASK] fut l' auteur de plusieurs traités scientifiques , dont deux ont exercé par la suite une très grande influence sur les sciences islamique et européenne .
L' un est le traité d' astronomie , qui est aujourd'hui connu sous le nom de l' [MASK] ; l' autre est la [MASK] , qui est une discussion approfondie sur les connaissances géographiques du monde gréco-romain .
[MASK] ( -325 à -265 ) est l' auteur des [MASK] , qui sont considérés comme l' un des textes fondateurs des mathématiques modernes .
Ces postulats , comme celui nommé le " postulat d' [MASK] " , que l' on exprime de nos jours en affirmant que " par un point pris hors d' une droite il passe une et une seule parallèle à cette droite " sont à la base de la géométrie systématisée .
[MASK] [MASK] aurait ainsi été capables de développer des techniques alternatives .
Pour l' agriculture , les [MASK] développent le moulin à eau .
Néanmoins , les savants romains furent peu nombreux et le discours scientifique abstrait progressa peu pendant la [MASK] [MASK] : " les [MASK] , en faisant prévaloir les " humanités " , la réflexion sur l' homme et l' expression écrite et orale , ont sans doute occulté pour l' avenir des " realita " scientifiques et techniques " , mis à part quelques grands penseurs , comme [MASK] ou [MASK] [MASK] [MASK] , souvent d' origine étrangère d' ailleurs .
La science arabo-musulmane est fondée sur la traduction et la lecture critique des ouvrages de l' [MASK] .
[MASK] [MASK] décrit la circulation sanguine pulmonaire , et [MASK] recommande l' usage de l' alcool en médecine .
Pour lui , la [MASK] était même animée d' une ambition de collecter de manière désintéressée le savoir , avant même les universités occidentales .
C' est avec [MASK] ( 598 -- 668 ) et son ouvrage célèbre , le [MASK] , particulièrement complexe et novateur , que les différentes facettes du zéro , chiffre et nombre , sont parfaitement comprises et que la construction du système de numération décimal de position est parachevée .
La période s' achève avec le mathématicien [MASK] [MASK] ( 1114 -- 1185 ) qui écrivit plusieurs traités importants .
À l' instar de [MASK] [MASK] [MASK] ( 1201 -- 1274 ) il développe en effet la dérivation .
[MASK] invente par ailleurs un système de représentation des nombres fondé sur les signes consonantiques de l' alphasyllabaire sanskrit .
Aux côtés de [MASK] [MASK] , la période fut marquée par quatre autres personnalités qui jetèrent , en [MASK] chrétienne , les fondements de la science moderne :
[MASK] [MASK] ( ( 1214 -- 1294 ) est philosophe et moine anglais .
[MASK] [MASK] admet trois voies de connaissance : l' autorité , le raisonnement et l' expérience .
Les œuvres de [MASK] ont pour but l' intuition de la vérité , c' est-à-dire la certitude scientifique , et cette vérité à atteindre est pour lui le salut .
[MASK] [MASK] ( env. 1168 -- 1253 ) étudia [MASK] et posa les prémices des sciences expérimentales , en explicitant le schéma : observations , déductions de la cause et des principes , formation d' hypothèse ( s ) , nouvelles observations réfutant ou vérifiant les hypothèses enfin .
[MASK] [MASK] [MASK] ( 1193-1280 ) fut considéré par les historiens comme un alchimiste et magicien , néanmoins ses études biologiques permirent de jeter les fondations des disciplines des sciences de la vie .
[MASK] [MASK] [MASK] [MASK] , théologien , permit de redécouvrir , par le monde arabe , les textes d' [MASK] et des autres philosophes grecs , qu' il étudia à [MASK] , à l' université dominicaine .
[MASK] [MASK] [MASK] ( v. 1285- v. 1349 ) permit une avancée sur le plan de la méthode .
Selon l' historien anglais [MASK] [MASK] , ce fut à cette époque que le mot [MASK] entra dans le langage courant et fut doté d' un cadre de référence solidement appuyé sur des cartes et d' un ensemble d' images affirmant son identité visuelle et culturelle .
[MASK] [MASK] ( 1561 -- 1626 ) est le père de l' empirisme .
D' après [MASK] , nos théories scientifiques sont construites en fonction de la façon dont nous voyons les objets ; l' être humain est donc biaisé dans sa déclaration d' hypothèses .
Pour [MASK] , " la science véritable est la science des causes " .
En somme , [MASK] préconise un raisonnement et une méthode fondés sur le raisonnement expérimental :
Pour [MASK] , comme plus tard pour les scientifiques , la science améliore la condition humaine .
Il expose ainsi une utopie scientifique , dans [MASK] [MASK] [MASK] ( 1627 ) , qui repose sur une société dirigée par " un collège universel " composé de savants et de praticiens .
La théologie médiévale se fonde quant à elle , d' une part sur le modèle d' [MASK] , d' autre part sur le dogme de la création biblique du monde .
Dans cette période , et avant que [MASK] n' intervienne , la théorie de [MASK] reste confinée à quelques spécialistes , de sorte qu' elle ne rencontre que des oppositions ponctuelles de la part des théologiens , les astronomes restant le plus souvent favorables à la thèse géocentrique .
[MASK] unifiera ces approches en découvrant la gravitation universelle .
Le danois [MASK] [MASK] observera de nombreux phénomènes astronomiques comme une nova et fondera le premier observatoire astronomique , " [MASK] " .
Avec tous ces astronomes , et en l' espace d' un siècle et demi ( jusqu' à [MASK] en 1644 ) , la conception de l' univers passe d' un " monde clos à un monde infini " selon l' expression d' [MASK] [MASK] .
Art ésotérique depuis l' [MASK] , l' alchimie est l' ancêtre de la physique au sens d' observation de la matière .
Selon [MASK] [MASK] , c' est surtout sur la médecine que l' alchimie eut une influence notable , par l' apport de médications minérales et par l' élargissement de la pharmacopée .
De nombreux philosophes et savants sont ainsi soit à l' origine des alchimistes ( [MASK] [MASK] ou [MASK] ) , soit s' y intéressent , tels [MASK] [MASK] et même , plus tard [MASK] [MASK] .
[MASK] [MASK] jette ainsi les bases de l' anatomie moderne alors que le fonctionnement de la circulation sanguine est découverte par [MASK] [MASK] et les premières ligatures des artères sont réalisées par [MASK] [MASK] .
Le philosophe [MASK] [MASK] et le mathématicien [MASK] [MASK] publient en 1751 l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] qui permet de faire le point sur l' état du savoir de l' époque .
[MASK] [MASK] devient ainsi un hymne au progrès scientifique .
Avec [MASK] [MASK] naît également la conception classique que la science doit son apparition à la découverte de la méthode expérimentale .
En énonçant en effet la théorie de la gravitation universelle , [MASK] inaugura l' idée d' une science comme discours tendant à expliquer le monde , considéré comme rationnel car ordonné par des lois reproductibles .
L' avènement du sujet pensant , en tant qu' individu qui peut décider par son raisonnement propre et non plus sous le seul joug des us et coutumes , avec [MASK] [MASK] , permet la naissance des sciences humaines , comme l' économie , la démographie , la géographie ou encore la psychologie .
La chimie naît par ailleurs avec [MASK] [MASK] [MASK] [MASK] qui énonce en 1778 la loi de conservation de la matière , identifie et baptise l' oxygène .
Comme discipline , la médecine progresse également avec la constitution des examens cliniques et les premières classification des maladies par [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
La biologie connaît au XIX e siècle de profonds bouleversements avec la naissance de la génétique , suite aux travaux de [MASK] [MASK] , le développement de la physiologie , l' abandon du vitalisme suite à la synthèse de l' urée qui démontre que les composés organiques obéissent aux mêmes lois physico-chimique que les composés inorganiques .
L' opposition entre science et religion se renforce avec la parution de [MASK] [MASK] [MASK] [MASK] en 1859 de [MASK] [MASK] .
Les instruments scientifiques sont plus nombreux et plus sûrs , tels le microscope ( à l' aide duquel [MASK] [MASK] découvre les microbes ) ou le télescope se perfectionnent .
La physique acquiert ses principales lois , notamment avec [MASK] [MASK] [MASK] qui , énonce les principes de la théorie cinétique des gaz ainsi que l' équation d' onde fondant l' électromagnétisme .
En 1971 la firme [MASK] met au point le premier micro-processeur et en 1976 [MASK] commercialise le premier ordinateur de bureau .
Plus récemment , quelques auteurs , comme [MASK] [MASK] , ont évoqué l' apparition d' une catégorie intermédiaire , celle des sciences de l' artificiel , qui portent sur l' étude de systèmes créés par l' homme -- artificiels -- mais qui présentent un comportement indépendant ou relativement de l' action humaine .
Ainsi , sur cette base , les mathématiques appliquées souvent perçus davantage comme une branche mathématique au service d' autres sciences ( comme le démontrent les travaux du mathématicien [MASK] [MASK] qui explique : " Ce que j' aime dans les mathématiques appliquées , c' est qu' elles ont pour ambition de donner du monde des systèmes une représentation qui permette de comprendre et d' agir " ) seraient bien plutôt sans finalité pratique .
Selon [MASK] [MASK] , il existe une autre sorte d' opposition épistémologique , distinguant d' une part les sciences de la nature , qui ont des objets émanant du monde sensible , mesurables et classables ; d' autre part les sciences de l' homme aussi dites sciences humaines , pour lesquelles l' objet est abstrait .
[MASK] [MASK] récuse par ailleurs de faire de l' étude du phénomène humain une science proprement dite .
Pour le sociologue [MASK] [MASK] , il n' existe pas une scientificité unique et transdisciplinaire .
Il s' appuie ainsi sur la notion d ' " airs de famille " , notion déjà théorisée par le philosophe [MASK] [MASK] selon laquelle il n' existe que des ressemblances formelles entre les sciences , sans pour autant en tirer une règle générale permettant de dire ce qu' est " la science " .
Selon [MASK] [MASK] la logique formelle est " science qui expose dans le détail et prouve de manière stricte , uniquement les règles formelles de toute pensée " .
[MASK] [MASK] développe en effet une " méthode atomique " ( ou atomisme logique ) qui s' efforce de diviser le langage en ses parties élémentaires , ses structures minimales , la phrase simple en somme .
[MASK] projetait en effet d' élaborer un langage formel commun à toutes les sciences permettant d' éviter le recours au langage naturel , et dont le calcul propositionnel représente l' aboutissement .
L' expérience ( au sens de mise en pratique ) est ici centrale , selon l' expression de [MASK] [MASK] : " Un système faisant partie de la science empirique doit pouvoir être réfuté par l' expérience " .
À la conception de l' unité de la science postulée par le positivisme tout un courant de pensée va , à la suite de [MASK] [MASK] ( 1833-1911 ) , affirmer l' existence d' une coupure radicale entre les sciences de la nature et les sciences de l' esprit .
Les sciences sociales doivent être l' objet d' une introspection , ce que [MASK] [MASK] appelle une " démarche herméneutique " , c' est-à-dire une démarche d' interprétation des manifestations concrets de l' esprit humain .
Ainsi , concernant la cohérence interne aux disciplines , l' épistémologue [MASK] [MASK] bat en brèche ce critère de scientificité , en posant que les paradigmes subissent des " révolutions scientifiques " : un modèle n' est valable tant qu' il n' est pas remis en cause .
[MASK] [MASK] comme [MASK] [MASK] lui ont refusé ce statut en raison de son caractère non réfutable par l' expérience .
Néanmoins , l' épistémologue [MASK] [MASK] lui oppose l' abduction ( ou méthode par conjecture et réfutation ) .
La philosophe [MASK] [MASK] résume ainsi ce nouveau statut de l' observation : " Le propre de la théorie quantique est de rendre caduque la situation classique d' un " objet " existant indépendamment de l' observation qui en est faite " .
[MASK] [MASK] proposait de définir l' épistémologie " en première approximation comme l' étude de la constitution des connaissances valables " , dénomination qui , selon [MASK] [MASK] [MASK] , permet de poser les trois grandes questions de la discipline :
Selon les tenants de la science comme moyen d' amélioration de la société , dont [MASK] [MASK] ou [MASK] [MASK] sont parmi les plus représentatifs , le progrès offre :
La science progressant de manière fondamentalement discontinue , les renversements des représentations des savants , ou appelées également " paradigmes scientifiques " selon l' expression de [MASK] [MASK] , sont également au cœur des interrogations épistémologiques .
L' épistémologue [MASK] [MASK] , dans " Contre la méthode " , est l' un des premiers , dans les années soixante-dix , à se révolter contre les idées reçues à l' égard de la science et à relativiser l' idée trop simple de " méthode scientifique " .
Enfin , l' épistémologie s' intéresse à la " science en action " ( expression de [MASK] [MASK] ) , c' est-à-dire à sa mise en œuvre au quotidien et plus seulement à la nature des questions théoriques qu' elle produit .
Des auteurs comme [MASK] [MASK] ( on parle alors du cartésianisme ) , ou [MASK] fondent les bases conceptuelles de ce mouvement qui met en avant le raisonnement en général et plus particulièrement le raisonnement déductif dit aussi analytique .
L' induction consiste , selon [MASK] en la généralisation de données de l' expérience pure , appelée " empirie " ( ensemble des données de l' expérience ) , qui est ainsi l' objet sur lequel porte la méthode .
Les travaux d' [MASK] [MASK] témoignent d' une méthode empirique dans la formalisation de la loi gravitationnelle .
Enfin , l' empirisme aurait percé dans le champ scientifique , selon [MASK] [MASK] [MASK] ( dans [MASK] de théorie et de méthode sociologique , 1965 ) grâce à ses liens étroits avec l' éthique protestante et puritaine .
[MASK] [MASK] distingue trois états historiques : dans l' état théologique , l' esprit de l' homme cherche à expliquer les phénomènes naturels par des agents surnaturels .
[MASK] cite ainsi , comme exemple , la théorie de la chaleur de [MASK] [MASK] , qui la bâtit sans avoir à observer la nature du phénomène .
Inventeur de la mesure de la vitesse de propagation du son , [MASK] [MASK] développa une pensée épistémologique qui influença notamment [MASK] [MASK] .
Mais la critique de [MASK] porte surtout sur la méthode de l' induction , pendant de la déduction .
La démarche de recherche est avant tout mentale conclut [MASK] : " Avant de comprendre la nature , il faut l' appréhender dans l' imagination , pour donner aux concepts un contenu intuitif vivant " .
Néanmoins , [MASK] admet que les énoncés non réfutables peuvent être heuristiques et avoir un sens ( c' est le cas des sciences humaines ) .
La pensée d' [MASK] [MASK] ( 1922 -- 1974 ) est en droite file de celle de [MASK] .
[MASK] , bien qu' étant l' élève de [MASK] [MASK] s' en oppose sur le point de la réfutabilité .
Les travaux de [MASK] [MASK] vont marquer une rupture fondamentale en philosophie , en histoire et en sociologie des sciences .
Ces " épisodes extraordinaires " sont comme des " révolutions scientifiques " ( ainsi celles apportées par [MASK] [MASK] , [MASK] [MASK] , [MASK] , ou encore [MASK] ) : toutes viennent renverser un paradigme dominant .
Toutefois , selon [MASK] [MASK] , c' est [MASK] qui fut le " grand pionnier de la construction " .
Le psychologue et épistémologue [MASK] [MASK] expliquera ainsi que le " fait est ( … ) toujours le produit de la composition , entre une part fournie par les objets , et une autre construite par le sujet " .
[MASK] étendra cependant le cadre constructiviste à ce qu' il nomme l ' " épistémologie génétique " qui étudie les conditions de la connaissance et les lois de son accroissement , en lien avec le développement neurologique de l' intelligence .
Sans parler de ressemblance totale , les mécanismes , de l' individu au groupe de chercheurs et donc , aux disciplines scientifiques , sont communs ( [MASK] cite ainsi l ' " abstraction réfléchissante " ) .
[MASK] [MASK] relègue ainsi la psychanalyse au rang de pseudo-science , au même titre que , par exemple , l' astrologie , la phrénologie ou la divination .
Le critère de [MASK] est cependant contesté pour certaines disciplines ; pour la psychanalyse , parce que la psychanalyse ne prétend pas être une science exacte .
De plus , [MASK] a été assez ambigu sur le statut de la théorie de l' évolution dans son système .
Les sceptiques , comme [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] ou encore [MASK] [MASK] considèrent toute pseudo-science comme dangereuse .
" L' homme a été homo-faber , avant d' être homo-sapiens " , explique le philosophe [MASK] .
Pourtant il faut bien admettre la possibilité d' une technique " a-scientifique " , c' est-à-dire évoluant en dehors de tout corpus scientifique et que résume les paroles de [MASK] [MASK] : " le progrès technique s' est fait par une somme d' échecs que vinrent corriger quelques spectaculaires réussites " .
Ces clichés tiennent soit à l' histoire de la science , résumée et réduite à des découvertes qui jalonnent le développement de la société , soit à des idées comme celles qui met en avant que les lois , et plus généralement les connaissances scientifiques , sont des vérités absolues et dernières , et que les preuves scientifiques sont non moins absolues et définitives alors que , selon les mots de [MASK] [MASK] , elles ne cessent de subir révolutions et renversements .
Les travaux de [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] surtout ont démontré l' incohérence du positivisme .
La [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] met à disposition de tous des expositions sur les découvertes scientifiques alors que le centre de culture scientifique , technique et industrielle a " pour mission de favoriser les échanges entre la communauté scientifique et le public .
Le [MASK] ou [MASK] ou le [MASK] [MASK] [MASK] [MASK] sont d' autres exemples de mise à disposition de tous des savoirs scientifiques .
Les [MASK] possèdent également des institutions telles que l' [MASK] de [MASK] [MASK] , qui se veulent plus près d' une expérience accessible par les sens et où les enfants peuvent expérimenter .
Le [MASK] a développé quant à lui le [MASK] [MASK] [MASK] de [MASK] .
Néanmoins , c' est pendant la [MASK] [MASK] [MASK] que la science est le plus utilisée à des fins militaires .
Le kidnapping de scientifiques allemands à la fin de la guerre , soit par les soviétiques , soit par les américains , fait naître la notion de " guerre des cerveaux " , qui culminera avec la course à l' armement de la [MASK] [MASK] .
Ainsi , selon les sociologues relativistes [MASK] [MASK] et [MASK] [MASK] de l' [MASK] [MASK] [MASK] , les théories sont d' abord acceptées au sein du pouvoir politique .
En d' autres termes , la science serait , sinon une expression élististe , une opinion majoritaire reconnue comme une vérité scientifique et le fait d' un groupe , ce que démontrent les travaux d' [MASK] [MASK] .
En grande partie , cette division est un corollaire du critère de réfutabilité de [MASK] [MASK] : la science propose des énoncés qui peuvent être mis à l' épreuve des faits , et doivent l' être pour être acceptés ou refusés .
Mais différents courants religieux radicaux défendent l' exactitude du récit de la [MASK] .
L' autre cas de violation est celui où on extrapole à partir de données scientifiques une vision du monde tout à fait irréfutable ( au sens de [MASK] ) , empiétant sur le domaine du religieux .
Le [MASK] [MASK] [MASK] ( 1545-1563 ) autorisa les communautés religieuses à mener des recherches scientifiques .
Le procès de [MASK] devint le symbole d' une science devenant indépendante de la religion , voire opposée à elle .
Les théories modernes de la physique ( la théorie des quanta notamment ) et de la biologie ( avec [MASK] [MASK] et l' évolution ) , les découvertes de la psychologie , pour laquelle le sentiment religieux demeure un phénomène intérieur voire neurologique , supplantent les explications mystiques et spirituelles .
Cependant , nombre de religieux tentent , comme [MASK] [MASK] [MASK] [MASK] ou [MASK] [MASK] , d' allier explication scientifique et ontologie religieuse .
Le paléontologue [MASK] [MASK] [MASK] dans " Que [MASK] soit ! "
L' ancêtre du chercheur reste , dans l' [MASK] , le scribe .
Depuis la [MASK] [MASK] [MASK] , ce sont les instituts de recherche et les organismes gouvernementaux qui dominent , à travers la figure du chercheur fonctionnaire .
Les sociologues et anthropologues [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] ou encore [MASK] [MASK] ont étudié l' espace scientifique , les laboratoires et les chercheurs .
La sociologie du " champ scientifique " , concept crée par [MASK] [MASK] , porte ainsi une attention particulière aux institutions scientifiques , au travail concret des chercheurs , à la structuration des communautés scientifiques , aux normes et règles guidant l' activité scientifique surtout .
Le " père " de la sociologie des sciences est [MASK] [MASK] [MASK] qui , le premier , vers 1940 , considère la science comme une " structure sociale normée " formant un ensemble qu' il appelle l ' " èthos de la science " ( les principes moraux dirigeant le savant ) et dont les règles sont censées guider les pratiques des individus et assurer à la communauté son autonomie ( [MASK] la dit égalitaire , libérale et démocratique ) .
Ce que cherche [MASK] , c' est analyser les conditions de production de discours scientifiques , alors que d' autres sociologues , après lui , vont viser à expliquer sociologiquement le contenu de la science .
[MASK] [MASK] s' attacha lui à analyser le champ scientifique du point de vue constructiviste .
À la suite des travaux de [MASK] [MASK] , les sociologues dénoncèrent la distinction portant sur la méthode mise en œuvre et firent porter leurs investigation sur le processus de production des connaissances lui-même .
Si la philosophie des sciences se fonde en grande partie sur le discours et la démonstration scientifique d' une part , sur son historicité d' autre part , pour [MASK] [MASK] , elle doit étudier aussi le style du laboratoire .
Mais c' est surtout le sociologue [MASK] [MASK] qui a su analyser l' économie du champ scientifique .