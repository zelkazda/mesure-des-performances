[MASK] [MASK] ( [MASK] , le 19 janvier 1544 -- [MASK] , le 5 décembre 1560 ) , fut roi de [MASK] de 1559 à sa mort .
Il épouse de [MASK] [MASK] , reine d' [MASK] , et s' est appuyé sur les oncles de cette dernière pour régner .
A l' égard des réformés protestants , le règne de [MASK] [MASK] s' inscrit d' abord dans la continuité répressive d' [MASK] [MASK] puis s' essaye à la conciliation .
Il naît onze ans après le mariage de ses parents , soulageant sa mère [MASK] après une période de stérilité qui aurait pu être la cause d' une répudiation .
Ses parrains et marraines sont le pape [MASK] [MASK] , [MASK] [MASK] er , la [MASK] [MASK] [MASK] et sa grande tante [MASK] [MASK] [MASK] .
Fait chevalier par son grand-père lors de son baptême , il reçoit le gouvernement du [MASK] en 1546 .
Il succède à l' âge de 15 ans à son père [MASK] [MASK] , mort accidentellement le 10 juillet 1559 .
Quant au connétable [MASK] [MASK] [MASK] , " tout-puissant " favori sous le règne précédent , il doit s' effacer devant ses anciens rivaux et quitte la cour .
[MASK] [MASK] [MASK] commence par réduire les effectifs de l' armée le 14 juillet 1559 , afin de renflouer les caisses de l' état , alors en banqueroute sous [MASK] [MASK] .
La ville d' [MASK] et le [MASK] [MASK] [MASK] demeurent les lieux de prédilection du nouveau roi .
Le règne de [MASK] [MASK] est marqué par les troubles religieux .
La mort d' [MASK] [MASK] est une source d' espérance pour ceux qu' on appelle les mal sentants de la foi .
Devant le danger , le conseil royal décide , sous l' influence de [MASK] [MASK] [MASK] de faire des concessions .
C' est ce qu' on va appeler la [MASK] [MASK] [MASK] .
Il demande aux nobles de se réunir à [MASK] afin qu' ils puissent donner leur avis sur ces questions .
Cette assemblée de notables , réunie en août , permet à [MASK] [MASK] [MASK] , futur chef des protestants , de faire passer au roi des pétitions émanant des protestants .
Ils émanent principalement des protestants ( iconoclasme , et coup de force manqué sur [MASK] ) .
Le roi reste convaincu qu' il n' est autre que le capitaine muet de la [MASK] [MASK] [MASK] .
Après seulement 17 mois de règne , [MASK] [MASK] meurt le 5 décembre 1560 de maux insupportables à l' oreille .
La trépanation fut envisagée par [MASK] [MASK] .
Certains soupçonneront ultérieurement sa mère [MASK] [MASK] [MASK] et son épouse [MASK] [MASK] de l' avoir empoisonné , rumeurs dénuées de fondement .
[MASK] [MASK] meurt sans laisser d' enfant , son frère cadet [MASK] , âgé de dix ans , lui succède .
[MASK] [MASK] retourne en [MASK] .
[MASK] [MASK] [MASK] , qui attendait son exécution dans sa cellule , est libéré suite à des négociations avec [MASK] [MASK] [MASK] .
[MASK] [MASK] eut un règne très court , montant sur le trône en pleine adolescence et donc sans expérience , alors que son époque était en proie aux troubles religieux .
Les historiens s' accordent sur le fait que [MASK] [MASK] était fragile tant physiquement que psychologiquement et que sa frêle constitution eut raison de sa santé [ réf. nécessaire ] .
Il subsiste aussi une controverse pour savoir si son mariage avec [MASK] avait été consommé ou non , sujet sur lequel un chroniqueur partisan déclara , en parlant du roi : " Il a les parties génitales constipées " .
Il n' apparaît , aujourd'hui , au regard du septième art , que comme le simple époux de [MASK] [MASK] .
Les films ne le font que rapidement apparaître en prologue de la biographie de la fameuse reine d' [MASK] .