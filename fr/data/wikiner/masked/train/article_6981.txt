Il se faisait appeler [MASK] [MASK] en référence au film de [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Il est célèbre notamment pour avoir accédé illégalement aux bases de données des clients de [MASK] [MASK] , ainsi qu' aux systèmes de [MASK] , [MASK] , [MASK] et [MASK] [MASK] .
Quand un abonné appelait pour obtenir une information , il tombait sur [MASK] ou sur l' un de ses complices qui demandait " La personne que vous recherchez est-elle blanche ou noire ?
La police se charge ensuite de l' enquête qui conduit à l' arrestation de [MASK] et de ses comparses .
[MASK] n' avait que 17 ans à l' époque et le verdict est de trois mois de détention en centre de redressement et une année de mise à l' épreuve .
En 1983 , [MASK] fait une intrusion dans un ordinateur du [MASK] .
Il s' est servi d' une machine de l' [MASK] [MASK] [MASK] [MASK] pour se connecter à l' [MASK] , l' ancêtre d' [MASK] , et a obtenu un accès illégal à une machine du [MASK] .
Il est placé en centre de détention pour jeunes situé à [MASK] en [MASK] durant six mois .
En 1987 , il est de nouveau arrêté par la police pour utilisation illégale de numéros de cartes de crédits téléphoniques , ainsi que le vol d' un logiciel de la société californienne [MASK] [MASK] [MASK] .
Son but est d' obtenir le code source du système d' exploitation [MASK] pour les ordinateurs [MASK] .
Les attaques sont repérées rapidement , mais [MASK] brouille la provenance des appels , ce qui empêche de remonter jusqu' à eux .
Quand ce dernier se présente , il est arrêté par deux agents du [MASK] tapis dans l' ombre .
[MASK] accuse [MASK] d' avoir volé des logiciels valant plusieurs millions de dollars , et d' avoir coûté 200000 dollars de frais pour le rechercher et l' empêcher d' avoir accès à leur réseau .
[MASK] plaide uniquement coupable pour fraude informatique et possession illégale de codes d' accès d' appels longue distance .
Il déménage alors à [MASK] [MASK] et travaille comme programmeur dans une agence publicitaire .
Le [MASK] enquête de nouveau sur [MASK] .
Mais quand des agents du [MASK] viennent l' arrêter à son domicile , [MASK] a disparu .
[MASK] a décidé de fuir .
Le [MASK] mettra deux ans à le retrouver .
Ils découvrent qu' il s' agit d' un magasin de reprographie de [MASK] [MASK] , près de [MASK] [MASK] .
Ils établiront plus tard qu' il s' agissait de [MASK] .
Depuis quelque temps déjà , le [MASK] soupçonne [MASK] d' avoir pris le contrôle du réseau téléphonique de la [MASK] et de mettre sur écoute les agents fédéraux chargés de le traquer car il arrive ainsi à anticiper leur mouvement .
[MASK] a dénié par la suite bon nombre des accusations présentes dans cet article .
Pour être sûr que personne ne sera connecté à la machine cible , il choisit d' effectuer son attaque le 25 décembre 1994 , le jour de [MASK] .
[MASK] exploite une faille dans l' architecture du réseau cible et les points faibles du protocole [MASK] de l' époque .
Mais [MASK] ne s' est pas rendu compte que le pare-feu était configuré de telle sorte qu' il envoyait une copie des fichiers de logging à un autre ordinateur toutes les heures , ce qui a produit une alerte automatique .
Le lendemain , [MASK] s' apprête à partir en vacances , quand il reçoit l' appel d' un de ses collègues du [MASK] [MASK] [MASK] de [MASK] [MASK] .
[MASK] décide donc d' aider le [MASK] à arrêter [MASK] .
Les appels de [MASK] proviennent de trois villes différentes : [MASK] , [MASK] et [MASK] .
Le travail est long , mais ils finissent par se convaincre que l' origine des appels est vraisemblablement [MASK] .
En arrivant à [MASK] , l' équipe découvre que l' origine des appels semble provenir de la compagnie téléphonique [MASK] .
Mais en approfondissant leur recherche , ils trouvent qu' ils émanent de la compagnie [MASK] .
L' équipe s' installe alors dans la pièce du commutateur téléphonique central de [MASK] pour localiser physiquement le téléphone cellulaire de [MASK] .
Mais , [MASK] a modifié les logiciels du réseau et [MASK] croyait que les appels provenaient de [MASK] .
Il finit par localiser l' appartement de [MASK] .
[MASK] a plaidé coupable pour sept chefs d' accusation ( intrusion de systèmes et vols de logiciels protégés , entre autres ) envers les sociétés [MASK] , [MASK] et [MASK] [MASK] .
[MASK] fut condamné à cinq ans de prison .
Le [MASK] disait de lui que le profit n' était pas sa motivation , ce qui le rendait d' autant plus dangereux .