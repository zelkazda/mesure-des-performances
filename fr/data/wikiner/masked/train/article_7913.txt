Cette expression n' a véritablement été popularisée qu' en en 1956 lorsque [MASK] [MASK] fut à deux sets près de réaliser l' exploit .
Cette performance a depuis été réalisée , étalée sur plusieurs années , par [MASK] [MASK] puis par [MASK] [MASK] .
Il n' y a pas de jeu décisif dans le cinquième set , ce qui augmente la difficulté du [MASK] [MASK] [MASK] [MASK] lorsqu' on combine ce paramètre avec celui de la surface .
[MASK] débute près d' un mois après [MASK] ( dernière semaine de juin ) .
[MASK] [MASK] les a gagnés en simple et en double ; [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] en double et en mixte .
Le joueur le plus proche de l' exploit demeure [MASK] [MASK] à qui il n' a manqué qu' une victoire à [MASK] en simple messieurs .