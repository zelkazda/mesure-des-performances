[MASK] [MASK] [MASK] est un sociologue américain né aux [MASK] le 13 août 1863 et décédé le 5 décembre 1947 .
Auteur , avec [MASK] [MASK] , d' une imposante étude sur l' immigration polonaise , il est également connu pour avoir formulé le " [MASK] [MASK] [MASK] " .
À l' instar de l' école pragmatiste de [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] , [MASK] sera amené à donner une importance primordiale à la subjectivité des individus .
[MASK] suivra [MASK] et le rejoindra à [MASK] en 1914 .
Tous deux ont entamé un travail considérable qui étudiera les immigrants polonais aux [MASK] ainsi que les paysans en [MASK] .
Tout comme l' anthropologue [MASK] [MASK] , [MASK] rejette les explications biologiques qui servaient alors à expliquer la réussite sociale .
Pour [MASK] et [MASK] , l' assimilation est donc souhaitable et son opposé , c' est la dissimilitude .
Le [MASK] [MASK] [MASK] rend compte du fait que les comportements des individus s' expliquent par leur perception de la réalité et non par la réalité elle-même .
Il a été formulé à diverses reprises par le sociologue américain [MASK] [MASK] [MASK] .
En 1923 , [MASK] présente dans " the unadjusted girl " , la notion de définition de la situation .
[MASK] considère notamment que les individus tendent à définir la situation de façon hédoniste ( recherchant d' abord le plaisir ) , tandis que la société leur enjoint de la définir de façon utilitaire ( plaçant la sécurité au premier plan ) .
[MASK] en a donné -- parmi d' autres -- l' exemple suivant : " Il est également très important pour nous de prendre conscience que nous ne gouvernons nos vies , nous ne prenons nos décisions , nous n' atteignons nos buts dans la vie quotidienne ni au moyen de calculs statistiques ni par des méthodes scientifiques .
[MASK] n' a pas revendiqué la production d' un théorème .
C' est [MASK] [MASK] [MASK] , dans " éléments de théorie " et " méthode sociologique " , qui a baptisé " [MASK] [MASK] [MASK] " l' idée que les représentations des individus avaient une influence sur la réalité .
[MASK] cite le cas d' une banque dont on raconte qu' elle est en faillite et qui voit alors tous ses créanciers réclamer leur argent .
Au contraire du [MASK] [MASK] [MASK] , les notions de prophéties autoréalisatrices et autodestructrices n' ont donc qu' une valeur descriptive .
Par contre , le [MASK] [MASK] [MASK] peut-être considéré comme une loi scientifique : les comportements individuels ne peuvent être expliqués à partir de la réalité mais de la façon dont elle est définie par les individus .
Sans qu' une filiation soit toujours revendiquée , de nombreuses théories sociologiques, ( l' interactionnisme symbolique de [MASK] , l' approche dramaturgique de [MASK] , le constructivisme de [MASK] et [MASK] ou l' effet pygmalion de [MASK] ) sont en partie inspirées du [MASK] [MASK] [MASK] .