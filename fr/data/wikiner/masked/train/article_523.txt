Les principales îles sont , en partant du nord , [MASK] , [MASK] ( l' île principale ) , [MASK] , [MASK] , prolongée au sud par l' [MASK] [MASK] ( [MASK] [MASK] ) dont fait partie l' île d' [MASK] et environ 3000 îles plus petites , avec un total de 29751 kilomètres de côtes .
Environ 73 % du pays est montagneux , la plus grande montagne du [MASK] est le [MASK] [MASK] ( 3776 mètres ) qui est un volcan .
Près de la moitié de la population , soit 60 millions d' habitants , est concentrée sur les mégalopoles de [MASK] et [MASK] -- [MASK] -- [MASK] .
Le climat est très varié , du fait de l' étirement du [MASK] de nord en sud ; [MASK] , sur l' île du nord ( [MASK] ) , a un été chaud et un hiver long , froid et très neigeux .
[MASK] , [MASK] , [MASK] , [MASK] , et [MASK] , dans la partie est de la plus grande île ( [MASK] ) , ont un climat qualifié de subtropical humide , avec un hiver relativement doux avec peu ou pas de neige , et un été chaud et humide .
[MASK] , sur l' île de [MASK] , a un hiver doux et un été long .
[MASK] [MASK] est ainsi en zone sismique : il y a 265 volcans dont une vingtaine en activité , de fréquents tremblements de terre ( environ 5000 chaque année , la plupart ne provoquant pas ou peu de dégâts aux constructions humaines ) ainsi que -- conséquence agréable de l' activité sismique -- des sources d' eau chaude , les onsen .
Entre 1900 et 2004 , sur 796 tsunamis observés dans l' [MASK] [MASK] , 17 % d' entre eux ont eu lieu près du [MASK] .
Le [MASK] ne possède aucune frontière terrestre .
Les frontières maritimes s' étendent sur 29 751 km , avec la [MASK] , la [MASK] et la [MASK] .