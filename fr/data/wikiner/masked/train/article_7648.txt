Connu principalement pour son expérience démontrant la rotation de la [MASK] autour de son axe , il détermina aussi la vitesse de la lumière et inventa le gyroscope .
Fils d' un éditeur , [MASK] [MASK] fait ses premières années d' études en grande partie à la maison et au [MASK] [MASK] à [MASK] .
Il s' intéresse tout d' abord aux expériences de [MASK] [MASK] sur la photographie .
Pendant trois ans , il assista [MASK] [MASK] pour ses conférences sur l' anatomie microscopique .
Avec [MASK] [MASK] , il mène une série d' expériences sur l' intensité de la lumière du [MASK] , en la comparant à celle du carbone dans la lampe à arc , et à celle de la chaux dans la flamme du chalumeau oxyhydrique .
En 1851 , il établit la rotation quotidienne de la terre en utilisant la rotation libre du plan d' oscillation d' un pendule long de 77 mètres , possédant une boule pesant 28 kilos et mesurant 18 centimètres de diamètre , suspendu au [MASK] [MASK] [MASK] .
[MASK] invente en 1857 le polariseur qui porte son nom et l' année suivante , il conçoit une méthode pour donner aux miroirs des télescopes réfléchissants la forme d' un sphéroïde ou d' un paraboloïde de révolution .
Après avoir doté le miroir de son dispositif de laboratoire de 1850 d' une turbine à air comprimé et multiplié les miroirs de réfraction , il établit en 1862 la vitesse de la lumière à 298000 km/s ±500 km/s , soit 10000 km/s de moins que la première mesure directe effectuées en 1849 par [MASK] [MASK] à l' aide d' un dispositif à roue dentée projetant un faisceau lumineux de [MASK] à [MASK] , et à 0.6 % de marge de la valeur actuellement acceptée ( 299792.45898 km/s ±0.0002 km/s ) .
De 1878 à 1926 , en améliorant le dispositif à miroir tournant , [MASK] [MASK] obtient 299 796 km/s ±4 km/s .
Il est nommé en 1862 membre du [MASK] [MASK] [MASK] et fait officier de la [MASK] [MASK] [MASK] .
En 1864 , il est reçu comme membre étranger à la [MASK] [MASK] de [MASK] et , l' année suivante , il entre dans la section de mécanique de l' [MASK] [MASK] [MASK] .
Il publie en 1865 un article sur le gouvernail de [MASK] [MASK] , dont il a essayé de rendre la période de révolution constante , et sur un nouvel appareil de régulation de la lumière électrique .
À partir de 1845 , il est rédacteur en chef de la section scientifique du [MASK] [MASK] [MASK] .
[MASK] [MASK] est inhumé au [MASK] [MASK] [MASK] .