Le plus gros producteur est la [MASK] , avec à peu près 50 % de la production mondiale .
L' huile d' amande extraite du noyau est , depuis l' [MASK] , très utilisée pour ses propriétés cosmétiques , adoucissantes et hydratantes en cas d' inflammation cutanée ( cicatrisante et anti-inflammatoire en cosmétologie ) .
La [MASK] en est le premier producteur mondial avec 250 000 tonnes , puis l' [MASK] avec 60 000 tonnes , l' [MASK] avec 40 000 tonnes et la [MASK] avec 16 000 tonnes .
Parmi les autres producteurs , on trouve l' [MASK] , le [MASK] , le [MASK] , la [MASK] , [MASK] , la [MASK] .
Aujourd'hui les [MASK] , les [MASK] et la [MASK] réalisent 80 % de la production nationale , soit 3 500 tonnes , mais qui ne représentent que 10 % de nos besoins .
L' amandier a été mentionné par les écrivains de l' [MASK] et on le trouve cité dans la [MASK] comme croissant en pays de [MASK] :
Il est cultivé depuis 5 000 ou 6 000 ans en [MASK] .
Les [MASK] rapportèrent l' amande , qu' ils appelaient " noix grecque " en [MASK] .
Les [MASK] la diffusèrent sur tout le pourtour méditerranéen , au fur et à mesure de leurs conquêtes .
En 812 , [MASK] ordonne d' introduire les amandiers dans les fermes impériales .