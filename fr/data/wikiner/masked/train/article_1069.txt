En informatique , le [MASK] ( [MASK] [MASK] [MASK] littéralement le protocole du bureau de poste ) , est un protocole qui permet de récupérer les courriers électroniques situés sur un serveur de messagerie électronique .
Ce protocole a été défini dans la [MASK] 1939 .
[MASK] ( [MASK] over [MASK] ) utilise [MASK] pour sécuriser la communication avec le serveur , tel que décrit dans la [MASK] 2595 .
[MASK] utilise le port 995 .
[MASK] et [MASK] utilisent tous deux le protocole de transfert [MASK] .
Voici un exemple de connexion du protocole [MASK] sur un interpréteur de commandes :
Puis , il est possible d' utiliser l' une des commandes [MASK] suivantes .