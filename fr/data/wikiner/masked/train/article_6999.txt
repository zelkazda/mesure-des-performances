Il a choisi d' écrire en français parce que la littérature qui s' écrivait dans cette langue , en [MASK] , à l' époque , était la littérature prestigieuse .
De milieu très modeste , orphelin très jeune , [MASK] [MASK] a été élevé dans une famille bourgeoise .
Il reste fidèle à la définition qu' en donne [MASK] [MASK] : " J' appelle bourgeoisie tout ce qui est de bas " et invente le concept de " belgeoisie " .
En 1899 , il publie son roman [MASK] , faisant scandale en tant que premier roman en littérature française belge à traiter ouvertement l' homosexualité .
Son objectif est de présenter aux lecteurs l' œuvre de [MASK] [MASK] .
C' est une curieuse analyse , quasi nouvelle par nouvelle , de ce que les ouvrages de [MASK] [MASK] peuvent contenir d' éléments correspondant à ce qu' on appellerait , aujourd'hui , la culture homosexuelle .
Il est rédigé en français et il est signé par [MASK] [MASK] .