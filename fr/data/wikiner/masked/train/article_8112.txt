Né vers -47 , ou vers -44 , il gouverne l' [MASK] avec sa mère ( -44 / -30 ) .
[MASK] [MASK] apparaît sur un relief de [MASK] en compagnie de celle-ci .
[MASK] est assassiné très jeune ( 15/17 ans ) par [MASK] , premier empereur de [MASK] .
[MASK] était le seul fils de [MASK] et donc un rival politique direct .
Ce personnage est au centre de l' intrigue du [MASK] [MASK] [MASK] , album bande dessinée de [MASK] et [MASK] .
[MASK] est interprété par [MASK] [MASK] dans la série télévisée [MASK] .