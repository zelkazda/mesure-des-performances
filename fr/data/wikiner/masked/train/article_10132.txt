Chaque feuille de [MASK] [MASK] est composée :
Depuis la version 2002 le nombre maximum de feuilles par classeur est limité par la quantité de mémoire disponible mais , auparavant ( depuis [MASK] 5.0 ) , la limite du nombre de feuilles était de 256 .
Avec l' introduction de [MASK] [MASK] en 1990 , les ventes d' [MASK] dépassèrent celles de [MASK] [MASK] et il atteignit la première place dans la liste des ventes ( en nombre de licences ) .
Cet accomplissement permit à [MASK] de détrôner le leader mondial du logiciel en se positionnant comme un véritable concurrent et démontra ainsi ses capacités à développer des logiciels avec interface graphique utilisateur .
Les tableurs ont été les premières applications importantes pour les ordinateurs personnels et lorsque [MASK] fut empaqueté avec [MASK] [MASK] et [MASK] [MASK] dans la suite [MASK] [MASK] ( 4.x ) , l' interface de ces deux derniers programmes a dû être revue pour être conforme avec celle d' [MASK] .
Très tôt , [MASK] a été la cible d' une attaque judiciaire sur le copyright par une autre société qui commercialisait une suite de logiciels portant le nom d' [MASK] dans le domaine de la finance .
[MASK] fut d' autant plus ravie de se référer à son programme sous le nom [MASK] [MASK] qu' elle venait justement de décider en interne de remplacer toute mention de [MASK] par [MASK] [MASK] pour un meilleur positionnement de sa marque .
Toutefois , cette pratique a été peu à peu oubliée pour devenir finalement permanente à partir du moment où [MASK] a racheté la marque déposée de l' autre programme .
Il intégra des fonctionnalités graphiques étendues comme la possibilité d' effectuer du publipostage à partir des données issues de tableaux sous forme de base de données vers [MASK] [MASK] .
Sa fonctionnalité essentielle est la consolidation de tableaux ( cross reference ) , qui resta en 2004 , plus puissante que celle par exemple , du tableur gratuit [MASK] .
En 1993 naquit la première version de [MASK] [MASK] , rassemblant la version 5.0 de [MASK] [MASK] et les applications [MASK] et [MASK] .
[MASK] offre la possibilité de générer du code [MASK] [MASK] par l' intermédiaire de l' enregistreur de macros en reproduisant les opérations effectuées par l' utilisateur et ainsi automatiser les tâches répétitives .
D' autres fonctionnalités de liaisons dynamiques et incorporées par automatisme ( [MASK] [MASK] ) firent de [MASK] [MASK] une cible de choix pour les virus-macro .
[MASK] n' a réagi que tardivement face à ce fléau mais a fini par offrir la possibilités aux applications telles que [MASK] [MASK] d' avertir et de pouvoir désactiver les macros incluses dans les classeurs ou dans certains compléments .
C' est pourquoi il existe depuis quelques temps maintenant , la notion de signatures numériques sur les documents issues des applications [MASK] [MASK] qui sont le seul moyen de garantir que ces derniers sont dignes de confiance .
À partir du la version 2007 , les classeurs [MASK] , les documents [MASK] ou les présentations [MASK] pourvus de macros se voient greffés d' un m dans l' extension du nom de fichier , qu' ils soient modèle ou document :
A partir de la version 10 ( [MASK] ou 2002 ) , [MASK] a pris les mesures qui s' imposaient pour éliminer définitivement de leur logiciels , ces fonctions additionnelles qui , de plus n' étaient pas documentées …
Il est possible de faire calculer beaucoup de choses à [MASK] en saisissant une " formule " dans une cellule .
Note : la parenthèse permet de décomposer clairement à [MASK] le calcul qu' il doit effectuer .
Les formules sont grossièrement les mêmes que pour [MASK] [MASK] .
[MASK] permet de dessiner automatiquement des graphiques de visualisation des données chiffrées .
On peut considérer les produits [MASK] [MASK] sous deux angles de vue .
Et l' angle de l' informaticien , qui pour certains , voient en [MASK] [MASK] une véritable plateforme de développement .
Mais dans un deuxième , la puissance du langage permet , à l' aide de certaines bibliothèques de composants , de créer de véritables applications dont [MASK] ne seraient que les interfaces graphiques .
La protection par mot de passe pour verrouiller l' accès au contenu d' une feuille [MASK] permet de se mettre à l' abri de modifications faites par inadvertance par des utilisateurs ( étourdis ) ou tout simplement par pure confidentialité et de réserver la lecture aux titulaires des mots de passe .
En effet , à cause d' un niveau chiffrement faible , de nombreux logiciels permettent de découvrir instantanément le mot de passe utilisé par [MASK] , quelle que soit sa longueur ou le jeu de caractères utilisé .
[MASK] 2007 reste pour autant totalement compatible avec les formats des versions précédentes bien que [MASK] encourage l' usage de ce nouveau format [MASK] .
[MASK] [MASK] 2007 autant que les autres applications de la suite [MASK] [MASK] 2007 , intègre le nouveau format de fichier faisant partie des spécifications du format [MASK] ouvert .
Les nouveaux formats [MASK] 2007 sont :
[MASK] [MASK] fournit un jeu de fonctions d' applications liées directement à l' interface pour pouvoir exploiter des données issues de feuilles de calcul [MASK] dans des environnements et/ou des applications hétérogènes .
Une caractéristique très intéressante de [MASK] [MASK] est celle de pouvoir écrire du code à travers le langage [MASK] [MASK] pour application .
[MASK] a été critiqué sous différentes formes et en particulier pour les problèmes de précision sur des calculs à virgule flottante face à d' autres outils dédiés notamment aux calculs statistiques .
Les adeptes d' [MASK] répondaient que ces erreurs de précision ne touchaient qu' une minorité de personnes qui connaissaient ce problème et que ces mêmes personnes , le plus souvent , avaient des solutions de contournement pour y parer .
Par ailleurs , [MASK] suppose que l' année de base de départ de l' environnement [MASK] est 1900 et que celle-ci est bissextile .
L' objectif était d' être en mesure de rester compatible avec le bug rencontré dans le tableur [MASK] [MASK] .
Il continue d' être exploité ainsi encore aujourd'hui , même au sein du format de fichier [MASK] ouvert .
Pour contrer certaines failles dans des calculs de date , [MASK] intègre et gère aussi la base de ses calculs à partir de l' année 1904 .
Le 22 septembre 2007 , est apparu que la version 2007 de [MASK] [MASK] affichait des résultats de calculs incorrects dans certaines situations .
En parallèle à cela , si l' un des nombres se voit ajouter la valeur 1 , [MASK] recalcule en affichant le résultat déjà affiché additionné de 1 soit 100001 .
[MASK] a réagi en conséquence et a rapporté que ce problème existe pour un jeu de 12 valeurs à virgule flottante comprises pour 6 d' entre-elles entre 65 534.99999999995 et 65 535 et six autres valeurs entre 65 535.99999999995 et 65 536 tout en n' incluant pas les valeurs entières de ces nombres .
Le 9 octobre 2007 , [MASK] à mis à la disposition des utilisateurs un correctif idoine .
1987 -- [MASK] 2.0 for [MASK]
1990 -- [MASK] 3.0
1992 -- [MASK] 4.0
1993 -- [MASK] 5.0
1995 -- [MASK] pour [MASK] 95 ( version 7.0 ) -- inclus aussi dans [MASK] [MASK] 95
1997 -- [MASK] 97 ( version 8.0 ) -- inclus aussi dans [MASK] [MASK] 97 ( [MASK] et aussi [MASK] [MASK] version )
1999 -- [MASK] 2000 ( version 9.0 ) inclus aussi dans [MASK] [MASK]
2001 -- [MASK] 2002 ( version 10 ) inclus aussi dans [MASK] [MASK]
2003 -- [MASK] 2003 ( version 11 ) inclus aussi dans [MASK] [MASK]
2007 -- [MASK] 2007 ( version 12 ) inclus aussi dans [MASK] 2007
2010 -- [MASK] 2010 ( version 14 ) inclus aussi dans [MASK] 2010 sortie le 12 mai pour les professionnels et au mois de juin pour les particuliers
1985 -- [MASK] 1.0
1988 -- [MASK] 1.5
1989 -- [MASK] 2.2
1990 -- [MASK] 3.0
1992 -- [MASK] 4.0
1993 -- [MASK] 5.0
1998 -- [MASK] 8.0 ( [MASK] ' 98 )
2000 -- [MASK] 9.0 ( [MASK] 2001 )
2001 -- [MASK] 10.0 ( [MASK] v .
2004 -- [MASK] 11.0
2008 -- [MASK] 12.0
1989 -- [MASK] 2.2
1990 -- [MASK] 2.3
1991 -- [MASK] 3.0