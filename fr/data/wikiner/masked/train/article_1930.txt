Les [MASK] sont une constellation du zodiaque traversée par le [MASK] du 12 mars au 18 avril .
Dans l' ordre du zodiaque , elle se situe entre le [MASK] à l' ouest et le [MASK] à l' est .
Bien qu' assez grande , elle découpe un pan du ciel éloigné du plan de la [MASK] [MASK] et ne contient que peu d' étoiles visibles , toutes assez peu lumineuses .
Elle était l' une des 48 constellations identifiées par [MASK] .
La [MASK] [MASK] [MASK] est une des constellations les plus anciennes .
Ces conditions de visibilité sont rarement bonnes , comme la plupart des constellations d' eau de cette région , les [MASK] vivent la plupart du temps cachés sous la surface .
L' étoile la plus brillante de la [MASK] [MASK] [MASK] , [MASK] [MASK] , ne possède pas de nom propre et n' atteint que la magnitude apparente 3,62 .
C' est une géante jaune , 25 fois plus grande que le [MASK] et 4 fois plus massive .