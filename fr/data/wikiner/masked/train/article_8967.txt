À [MASK] , il avait une sœur qui travaillait comme cuisinière , qui le reçut et lui trouva un travail de domestique .
[MASK] [MASK] [MASK] [MASK] [MASK] , de retour au pouvoir , de nombreux libéraux furent exilés , lui-même s' en fut à la [MASK] [MASK] .
Nommé à nouveau gouverneur d' [MASK] , il convoqua des élections et fut réélu .
Il fut libéré le 11 janvier 1858 et devint président de la république à [MASK] .
Ces terres furent achetées par des spéculateurs pour la plupart issus du gouvernement de [MASK] et les propriètaires terriens en profitèrent pour constituer ou agrandir leurs domaines .
En 1860 , il entra dans la ville de [MASK] et fut désigné de nouveau président en 1861 .
Pendant cette période , devant une situation financière grave à cause de la guerre , il décida de suspendre le 17 juillet 1861 le paiement de la dette extérieure , ce qui causa les protestations de la [MASK] , de l' [MASK] et de la [MASK] .
[MASK] est l' auteur de la loi dite " mortuaire " du 25 janvier 1862 contre les traîtres à la patrie .
La perte de l' appui des libéraux , la montée du bandolérisme et les désordres sociaux minèrent les capacités politiques de [MASK] .
De nouveau élu président ( mais seulement par le congrès ) en 1871 , [MASK] se confronta à des soulèvements , dirigés principalement par le général [MASK] [MASK] .
Lui succède alors [MASK] [MASK] [MASK] [MASK] .
[MASK] [MASK] a figuré sur les pièces de 10 centavos frappées de 1966 à 1967 , sur les pièces de 25 pesos de 1972 , sur celles de 50 pesos dans les années 80 .