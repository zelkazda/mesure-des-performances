Le [MASK] [MASK] reste sous l' influence de la colonie du [MASK] avant de devenir une province royale en 1679 -- 1698 .
D' une superficie de 24 033 km² , le [MASK] [MASK] est peuplé de 1 235 786 habitants ( 2000 ) .
La plupart de la population vit dans la région sud de l´état , vers la frontière du [MASK] .
Parmi les états côtiers , la longueur de la côte du [MASK] [MASK] , moins de 29 km , est la plus courte .
La capitale du [MASK] [MASK] est [MASK] .
Néanmoins , les deux villes principales sont [MASK] et [MASK] .
Pour les élections présidentielles , les électeurs du [MASK] [MASK] ont choisi [MASK] [MASK] [MASK] en 2000 par six mille voix d' avance mais en 2004 lui préférèrent [MASK] [MASK] par neuf mille voix d' avance .
M. [MASK] a été réélu en 2006 et en 2008 .
Dans le [MASK] [MASK] [MASK] , plus de 16 % de la population parle le français à la maison , selon le recensement de 2000 .