[MASK] est un environnement de développement intégré libre extensible , universel et polyvalent , permettant de créer des projets de développement mettant en œuvre n' importe quel langage de programmation .
[MASK] IDE est principalement écrit en [MASK] ( à l' aide de la bibliothèque graphique [MASK] , d' [MASK] ) , et ce langage , grâce à des bibliothèques spécifiques , est également utilisé pour écrire des extensions .
La spécificité d' [MASK] IDE vient du fait de son architecture totalement développée autour de la notion de plugin ( en conformité avec la norme [MASK] ) : toutes les fonctionnalités de cet atelier logiciel sont développées en tant que plug-in .
Ces composants de base peuvent être réutilisés pour développer des clients lourds indépendants d' [MASK] grâce au projet [MASK] RCP .
Ces plugins sont architecturés selon les recommandations de [MASK] .
Une liste plus complète et à jour peut être trouvée sur le site [MASK] plug-in central .
Cependant , on ne peut dresser une liste exhaustive de projets ou d' extensions d' [MASK] car l' écosystème autour d' [MASK] est très actif .
Il est possible de traduire [MASK] dans de nombreuses langues via des packs :
[MASK] est un environnement de développement libre .
Dès l' origine du projet , [MASK] a voulu offrir une solution multi-plate-forme , pouvant être exécutée sur les différents systèmes d' exploitation de ses clients .
En novembre 2001 , [MASK] , en tant que logiciel libre , voit le jour , porté par un regroupement de sociétés .
La [MASK] [MASK] est constituée en janvier 2004 afin d' assurer son développement .
Le nom serait un jeu de mots : le créateur de [MASK] est [MASK] , concurrent qu' [MASK] semble vouloir " éclipser " .
Les progrès d' [MASK] peuvent être observés dans une vidéo relativement synthétique .