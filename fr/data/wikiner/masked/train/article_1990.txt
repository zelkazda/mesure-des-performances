Le premier numéro de [MASK] [MASK] paraît le lundi 18 avril 1904 .
Lors de sa naissance en 1904 , [MASK] [MASK] ne représente qu' une partie du mouvement socialiste français .
L' unification des socialistes français au sein de la [MASK] en 1905 ouvre le journal à l' ensemble du mouvement socialiste français ( notamment aux guesdistes ) .
Finalement en octobre 1918 , [MASK] [MASK] est remplacé à la tête de [MASK] [MASK] par [MASK] [MASK] .
L' année 1920 est un nouveau tournant pour [MASK] [MASK] .
Le journal suit la majorité et devient donc l' organe officiel du jeune [MASK] .
Dans les années vingt , le journal mène une intense campagne contre la [MASK] [MASK] [MASK] .
En 1926 , [MASK] [MASK] devient rédacteur en chef .
Sous sa direction dans les années 1930 , le journal dépasse les 300000 exemplaires notamment au début du [MASK] [MASK] .
En tant qu' organe central du [MASK] , [MASK] [MASK] est à la fois un outil de mobilisation des militants et un journal d' information .
[MASK] [MASK] parait clandestinement pendant cinq ans ( 383 numéros diffusés à 200000 exemplaires ) jouant un rôle important dans la [MASK] .
Le journal reparait librement le 21 août 1944 durant l' insurrection de [MASK] .
Le 7 novembre 1956 , alors que la situation internationale est extrêmement tendue ( révélation du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] sur le stalinisme , intervention soviétique en [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] ) le siège de [MASK] [MASK] ( et du comité central du [MASK] ) est attaqué par des manifestants anticommunistes , , , , , qui tentent de l' incendier .
Devant l' inertie complaisante de la police déployée en nombre , le bâtiment est défendu par le personnel du journal et des militants du [MASK] .
Parallèlement , le journal est le seul quotidien français à soutenir partout dans le monde les luttes de libération nationale ( décolonisation ) ce qui lui vaut de nombreuses interdictions de parution notamment durant les guerres d' [MASK] et d' [MASK] .
Les articles de [MASK] [MASK] sur la [MASK] [MASK] [MASK] , qui lui valurent une tentative d' attentat de l' [MASK] , puis sur la [MASK] [MASK] [MASK] [MASK] depuis les maquis [MASK] illustrent bien ses positions .
Sa diffusion décline ensuite ( 150000 exemplaires en 1972 , 107000 exemplaires en 1986 ) parallèlement au déclin de l' influence du [MASK] et à la crise de la presse quotidienne .
Le [MASK] reste selon les statuts " l' éditeur " du journal mais sa direction ne préside plus officiellement à l' élaboration de sa ligne éditoriale .
Les militants du [MASK] restent cependant très impliqués dans la diffusion du journal ( essentiellement à travers la vente militante de [MASK] [MASK] [MASK] ) .
Après avoir baissé à 46000 exemplaires en 2002 , [MASK] [MASK] est parvenu à stabiliser ses ventes aux alentours de 50000 exemplaires .
Pour apurer ses dettes ( estimées à 8 millions d' euros ) , le quotidien tente de vendre son siège de [MASK] , conçu par l' architecte [MASK] [MASK] et dans lequel il s' était installé en 1989 , pour 15 millions d' euros .
La rédaction déménage en mai 2008 dans un immeuble situé dans la même ville , près du [MASK] [MASK] [MASK] .
Un appel à une souscription exceptionnelle a alors été lancé par [MASK] [MASK] et la direction nationale du [MASK] , avec l' objectif de recueillir en moins de trois mois plus de 2 millions d' euros .
[MASK] [MASK] se revendique des valeurs de son fondateur [MASK] [MASK] , qui mettait la lutte pour la paix , la " communion avec le mouvement ouvrier " et l' indépendance face aux " groupes d' intérêt " au cœur de ses priorités .
Jusque dans les années 1990 , l ' [MASK] a soutenu toutes les campagnes menées par le [MASK] [MASK] .
Il a participé activement à la campagne du " non de gauche " en 2005 , lors du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Les symboles de la faucille et du marteau ont disparu de la une , et " [MASK] [MASK] " n' est plus l' organe officiel du [MASK] [MASK] [MASK] .
La mise à disposition gratuite des archives est une volonté politique d' ouverture de la direction du journal , de même que le choix des logiciels libres pour ce site ( [MASK] , [MASK] , etc ) .
Parmi les activités de [MASK] [MASK] , l' organisation de compétitions sportives est un domaine riche d' histoire .
Si le cyclisme , comme vecteur de " propagande " , semble avoir perduré un demi-siècle ( de 1954 à 1974 , [MASK] [MASK] organise aussi le circuit des [MASK] [MASK] [MASK] [MASK] ) , d' autres sports permettent au journal d' affirmer son attitude de proximité avec les sportifs .