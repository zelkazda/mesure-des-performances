En effet , le village se trouve à l' ouest de [MASK] , sur la rive opposée du [MASK] .
On leur doit également le temple monumental d' [MASK] sur le site de [MASK] [MASK] .
Le village fut abandonné , puis pillé , durant la [MASK] [MASK] [MASK] qui débuta à la fin du règne de [MASK] [MASK] .
Un temple de construction ptolémaïque y fut édifié par [MASK] [MASK] pour les déesses [MASK] et [MASK] .
La cité se développa jusqu' à compter sous [MASK] [MASK] quelque 1200 artisans nourris par une noria de pêcheurs , cultivateurs et porteurs d' eau .
D' époque ptolémaïque , le petit temple de [MASK] [MASK] ( neuf mètres de large sur vingt-deux mètres de long ) comporte trois sanctuaires juxtaposés précédés d' un vestibule soutenu par deux colonnes à chapiteau hathorique .
Outre le temple de [MASK] [MASK] , le site est parsemé de fondations d' autres temples plus anciens , notamment le petit temple d' [MASK] [MASK] [MASK] et la chapelle d' [MASK] construite par [MASK] [MASK] [MASK] alors que d' autres éléments remontent à [MASK] [MASK] .
À partir de 1810 , les voleurs pillent [MASK] [MASK] , dont les nombreuses tombes et les maisons étaient encore en excellent état .
Heureusement , [MASK] [MASK] met fin à ce pillage sauvage dans les années 1850 .
Mais le véritable explorateur du site est sans conteste [MASK] [MASK] qui y consacra près de vingt-cinq ans de sa vie .
Il y entreprend l' exploration systématique et méthodique entre 1917 et 1947 ainsi que l ' égyptologue tchèque [MASK] [MASK] .
En 1934/1935 , [MASK] [MASK] y découvre la [MASK] [MASK] [MASK] [MASK] [MASK] et de son époux , un ouvrier du village des artisans .