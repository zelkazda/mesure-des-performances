[MASK] [MASK] , né le 4 octobre 1816 à [MASK] où il est mort le 6 novembre 1887 , est un goguettier , poète et révolutionnaire français , auteur des paroles de [MASK] [MASK] .
Il participe à la [MASK] [MASK] [MASK] .
Sous le [MASK] [MASK] , il crée une maison d' impression sur étoffes et , en 1864 , il est à l' origine de la création de la chambre syndicale des dessinateurs , qui adhère ensuite à la [MASK] [MASK] .
Membre de la garde nationale , il participe aux combats durant le [MASK] [MASK] [MASK] [MASK] [MASK] , puis il prend une part active à la [MASK] [MASK] [MASK] , dont il est élu membre pour le 2 e arrondissement .
En juin 1871 , caché dans [MASK] , il compose son poème [MASK] [MASK] et se réfugie en [MASK] .
Condamné à mort par contumace le 17 mai 1873 , il s' exile aux [MASK] , d' où il organise la solidarité pour les communards déportés .
[MASK] [MASK] fréquente les goguettes .
C' est dans cette goguette qu' il retrouve vers 1883 le chansonnier [MASK] [MASK] qu' il a croisé en 1848 .
Le texte a été écrit en juin 1871 , à [MASK] , en pleine répression de [MASK] [MASK] [MASK] [MASK] .
Ses chansons sont reprises après sa mort , que ce soit par des artistes d' inspiration socialiste , communiste , anarchiste ou libertaire comme [MASK] [MASK] .
[MASK] [MASK] est inhumé au [MASK] [MASK] [MASK] à [MASK] .