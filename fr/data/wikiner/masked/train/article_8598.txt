[MASK] [MASK] écrit ainsi :
D' abord , le [MASK] [MASK] [MASK] [MASK] [MASK] en 1215 évoque la possibilité d' un personnel spécialisé , mais restant dans le cadre diocésain .
En 1227 , des dominicains appuyés par un commissaire pontifical , [MASK] [MASK] [MASK] , parcourent la [MASK] pour soutenir les commissions épiscopales : ils se chargent de dénoncer l' hérésie au cours de la procédure .
La législation en la matière est ébauchée avec les décrets du [MASK] [MASK] [MASK] [MASK] [MASK] ( 1139 ) .
Ainsi , se fondant sur les punitions très dures de la loi carolingienne contre le sacrilège , [MASK] [MASK] choisit en 1224 , dans le statut accordé à la ville de [MASK] , d' appliquer la peine du feu aux hérétiques de [MASK] .
Cette pratique est officialisée en 1261 par [MASK] [MASK] .
Parmi les pressions physiques , on peut citer la réclusion qui , selon [MASK] [MASK] , " ouvre l' esprit " , ainsi que la privation de nourriture et la torture .
[MASK] [MASK] , parlant de l' [MASK] [MASK] , rappelle que la pratique de la torture y est très codifiée .
[MASK] considère pour preuve que la torture fut appliquée avec modération le fait que nombreux sont ceux qui y résistèrent .
En 1561 , l' inquisiteur général [MASK] [MASK] [MASK] [MASK] fit preuve du même scepticisme .
L' [MASK] [MASK] condamne aussi à l' ostracisme par le biais du port du sanbenito ou par l' exposition de celui-ci avec le nom du condamné dans les églises .
[MASK] [MASK] précise que l' inquisiteur passe outre à tout privilège d' exemption et à l' appel .
À [MASK] en 1494 , ce droit à l' appel est dénié à ceux condamnés pour hérésie .
[MASK] [MASK] pointe la grande variabilité en nombre de ces condamnations selon les périodes ( rigoureuses ou plus calme ) .
À [MASK] , en 1331 , un commissaire du roi assimile l' inquisition à une juridiction royale , en 1412 , l' inquisiteur de [MASK] est arrêté sur l' ordre du roi .
Le pape n' aura que très peu d' influence sur l' inquisition espagnole et face à l' intransigeance d' un [MASK] , il ne peut qu' élever une protestation .
Elle sert à lutter contre les ennemis du pouvoir ( parti navarrais , [MASK] [MASK] , répression des émeutes de 1591 , répression de la révolution de 1640 en [MASK] , prise de parti dans la [MASK] [MASK] [MASK] [MASK] [MASK] ) .
Les victimes de cette répression religieuse furent considérées comme des martyrs de [MASK] [MASK] , et la répression elle-même alimenta dans la population néerlandaise le rejet du régime espagnol , qui fut obtenu à la suite de près d' un siècle de troubles ( guerre dite de [MASK] [MASK] , 1566-1648 ) .
De plus , depuis 1533 , elle baigne dans un anti- papisme officiel et le bref règne de [MASK] [MASK] , de 1553 à 1558 , ne fera qu' augmenter le rejet du catholicisme .
[MASK] la prend pour cible constante .
[MASK] la prend pour cible dans son [MASK] .
Dans son roman [MASK] [MASK] [MASK] [MASK] [MASK] ( 1980 ) , [MASK] [MASK] choisit pour personnage principal un ancien inquisiteur , [MASK] [MASK] [MASK] , qui fait office de détective élucidant une série de meurtres .
Un an plus tard , dans son film [MASK] [MASK] [MASK] [MASK] [MASK] ( 1981 ) , [MASK] [MASK] se met lui-même en scène dans un sketch parodique qui représente l' inquisition sous la forme d' une comédie musicale .