Il part en [MASK] pour y enseigner les mathématiques sous le règne de [MASK] [MASK] [MASK] .
Il travaille au musée d' [MASK] et à l' école de mathématiques .
Les [MASK] sont une compilation du savoir géométrique et restèrent le noyau de l' enseignement mathématique pendant près de 2000 ans .
Les [MASK] sont divisés en treize livres .
Les [MASK] sont remarquables par la clarté avec laquelle les théorèmes sont énoncés et démontrés .
Plus d' un millier d' éditions manuscrites des [MASK] ont été publiées avant la première version imprimée en 1482 .
Les dernières recherches entreprises en histoire des mathématiques tendent à prouver qu' [MASK] n' est pas le seul auteur des [MASK] .
La géométrie telle qu' elle est définie par [MASK] dans ce texte fut considérée pendant des siècles comme la géométrie et il fut difficile de lui ôter cette suprématie ; [MASK] [MASK] [MASK] fut le premier à s' y essayer officiellement dès 1826 , suivi de [MASK] [MASK] , mais la légende veut qu' il n' ait pas été pris au sérieux jusqu' à la mort de [MASK] , lorsque l' on découvrit parmi les brouillons de ce dernier qu' il avait lui aussi imaginé des géométries non euclidiennes .
[MASK] a montré qu' un modèle de la géométrie sphérique est la géométrie de la sphère où les droites sont les méridiens ou grands cercles .
[MASK] s' est aussi intéressé à l' arithmétique dans le livre 7 .