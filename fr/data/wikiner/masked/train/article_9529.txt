[MASK] ( 奇子 ) est un manga d' [MASK] [MASK] écrit entre 1972 et 1973 .
La reconstruction du pays se fait au prix de nombreux changements exigés par les [MASK] , sous la direction de [MASK] [MASK] .
Jouant les espions pour les [MASK] , il se trouve mêlé à un assassinat .
Même s' il y a une grande partie inventée , le contexte historique correspond bien à ce qui s' est passé dans l' après-guerre japonais , où la reconstruction est dirigée par les [MASK] et notamment [MASK] [MASK] .