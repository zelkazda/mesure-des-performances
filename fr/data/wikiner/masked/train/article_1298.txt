Le [MASK] [MASK] [MASK] [MASK] , généralement abrégé [MASK] , est un protocole de communication utilisé pour transférer le courrier électronique ( courriel ) vers les serveurs de messagerie électronique .
[MASK] est un protocole assez simple ( comme son nom l' indique ) .
Le [MASK] commence à être largement utilisé au début des années 1980 .
Il est alors un complément à l' [MASK] , celui-ci étant plus adapté pour le transfert de courriers électroniques entre des machines dont l' interconnexion est intermittente .
Le [MASK] , de son côté , fonctionne mieux lorsque les machines qui envoient et reçoivent les messages sont interconnectées en permanence .
Le logiciel [MASK] est l' un des premiers , sinon le premier serveur de messagerie électronique à utiliser [MASK] .
Depuis , la plupart des clients de messagerie peuvent utiliser [MASK] pour envoyer les messages .
Certains nouveaux serveurs sont apparus , comme [MASK] , [MASK] de [MASK] [MASK] [MASK] , [MASK] et [MASK] de [MASK] ( qui accomplit également d' autres fonctions ) .
Pour pallier ce problème , des standards comme [MASK] ont été développés pour permettre le codage des fichiers binaires au travers de [MASK] .
[MASK] utilise [MASK] pour le transfert des données .
[MASK] ne permet pas de récupérer à distance des courriels arrivés dans une boîte aux lettres sur un serveur .
Les standards [MASK] [MASK] [MASK] ( [MASK] ) et [MASK] ont été créés dans ce but .
Une des limitations de [MASK] vient de l' impossibilité d' authentifier l' expéditeur .
Afin de lutter efficacement contre ce phénomène , il existe deux approches : modifier profondément [MASK] ou même le remplacer ou bien lui adjoindre d' autres protocoles pour combler ses lacunes .
Une autre approche consiste à créer des systèmes visant à assister les opérations du protocole [MASK] .