Il est inhumé auprès de sa mère en l' [MASK] [MASK] de [MASK] .
Son frère jumeau , [MASK] , meurt peu après .
Durant son règne , il réforme les monastères et change de politique vis-à-vis de la papauté en s' engageant à respecter les [MASK] [MASK] [MASK] [MASK] et à ne pas intervenir dans les élections pontificales .
Le pape retrouve ainsi , après le contrôle exercé par [MASK] , une certaine indépendance politique .
À sa cour , il s' entoure de prélats et de clercs qui le conseillent tels que [MASK] ( 778-840 ) , [MASK] ( † 834 ) et [MASK] [MASK] [MASK] ( 750-821 ) .
En 822 , il accomplit une pénitence publique à [MASK] .
En somme , la politique religieuse de [MASK] [MASK] [MASK] a pour objectif de renforcer l' unité de l' empire , un empire carolingien fondamentalement chrétien .
En octobre 816 , il est sacré par le pape [MASK] [MASK] à [MASK] .
Avec [MASK] [MASK] [MASK] , il a trois fils : [MASK] , [MASK] et [MASK] .
Après avoir durement châtié [MASK] en lui faisant crever les yeux ( 818 ) , il le gracie , mais [MASK] meurt trois jours après .
La naissance de [MASK] vient bouleverser le partage de 817 : il faut possessionner ce nouveau descendant .
Aussi , dès 829 , [MASK] [MASK] [MASK] modifie sa succession pour y intégrer [MASK] .
[MASK] réunit plusieurs aristocrates , et forme le parti de l' unité de l' empire .
Deux ans plus tard , [MASK] [MASK] [MASK] constitue un vaste royaume pour son dernier fils [MASK] , ce qui entraîne les rancœurs des autres .
Après la mort de [MASK] [MASK] [MASK] en 840 , les hostilités entre les fils reprennent aussitôt .