[MASK] est aussi une commande permettant de créer une session [MASK] sur une machine distante .
Cette commande a d' abord été disponible sur les systèmes [MASK] , puis elle est apparue sur la plupart des systèmes d' exploitation .
[MASK] est un protocole de type client-serveur basé sur [MASK] .
Vue la flexibilité du programme , il est possible d' utiliser la commande telnet pour établir une connexion [MASK] interactive avec d' autres services tels que [MASK] , [MASK] , [MASK] , [MASK] , etc .
[MASK] est indispensable pour le parametrage de certains materiels reseaux anciens , n' ayant pas encore d' interface de gestion web à l' époque : certains switchs et serveur d' impression .
Depuis [MASK] [MASK] , le protocole réseau [MASK] n' est plus activé par défaut .
[MASK] explique son choix de le désactiver pour des raisons de sécurité , mais aussi car de moins en moins d' utilisateur y feraient appel .
Le côté sommaire de [MASK] fait que toute communication est transmise en clair sur le réseau , mots de passe compris .
Des sniffeurs comme [MASK] ou [MASK] permettent d' intercepter les communications de la commande telnet .
Des protocoles chiffrés comme [MASK] ont été développés pour fournir un accès distant remplaçant [MASK] et dont l' interception ne fournit aucune donnée utilisable à un éventuel espion .