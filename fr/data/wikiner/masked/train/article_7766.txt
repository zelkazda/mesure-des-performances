[MASK] était un langage compilé , qui autorisait entre autres l' utilisation de variables nommées , d' expressions complexes , et de sous-programmes .
Après plusieurs révisions du langage , en 1978 et en 1990 , [MASK] est toujours utilisé dans le milieu scientifique pour la qualité de ses bibliothèques numériques et sa grande rapidité , ce qui en fait le langage informatique ayant eu la plus grande longévité .
[MASK] , développé en 1958 par un consortium américano-européen pour concurrencer [MASK] , qui était un langage propriétaire , fut l' ancêtre de nombreux langages de programmation d' aujourd'hui .
[MASK] ( 1960 ) et [MASK] ( 1963 ) étaient deux tentatives pour rendre la syntaxe plus proche de l' anglais , et donc plus accessible .
[MASK] était spécifiquement destiné aux applications de gestion , tandis que [MASK] avait essentiellement un but éducatif .
Après un article de [MASK] dénonçant les ravages de [MASK] , la réputation de [MASK] comme langage pour l' enseignement de la programmation déclina .
Dans les années 1970 , le [MASK] fut développé par [MASK] [MASK] , dans le but d' enseigner la programmation structurée et modulaire .
[MASK] combinait les meilleures constructions des langages [MASK] , [MASK] et [MASK] dans un ensemble élégant , mais simpliste , qui lui assura un succès durable comme langage d' initiation ( en remplacement de [MASK] ) .
Par la suite , [MASK] [MASK] fut à l' origine de [MASK] , [MASK] , et d' [MASK] , les successeurs de [MASK] .
À la même époque , [MASK] [MASK] créa [MASK] aux [MASK] [MASK] , pour le développement du système [MASK] .
La puissance du [MASK] , permettant grâce aux pointeurs de travailler à un niveau proche de la machine , ainsi qu' un accès complet aux primitives du système lui assura un succès qui ne s' est jamais démenti depuis .
En 1974 , le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] cherchait un langage dont le cahier des charges mettait l' accent sur la sûreté d' exécution , pour tous ses besoins futurs .
[MASK] , conçu à l' origine par [MASK] [MASK] en 1969 , fut présenté en 1980 par le [MASK] [MASK] [MASK] [MASK] de la compagnie [MASK] ( [MASK] ) .
Le succès d' [MASK] [MASK] , notamment utilisé pour le développement sur les stations [MASK] et [MASK] [MASK] [MASK] , est resté faible par rapport à [MASK] .
Le langage [MASK] [MASK] fut le premier langage à objets ( CLOS ) standardisé par l' [MASK] , en 1995 .
On peut citer dans ces catégories [MASK] , [MASK] , [MASK] , [MASK] et [MASK] ( [MASK] [MASK] , 1996 ) .
Les langages fonctionnels , tels que [MASK] ou [MASK] , ne sont pas des suites d' instructions et ne s' appuient pas sur l' idée d' état global , mais au contraire tendent à s' extraire de ce modèle pour se placer à un niveau plus conceptuel ( qui a ses fondations dans le lambda-calcul ) .
Les langages de programmation logiques , tels que [MASK] , se concentrent sur ce qui doit être calculé , et non comment le calcul doit être effectué .