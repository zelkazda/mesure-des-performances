Il est élu associé étranger à l' [MASK] [MASK] [MASK] en 2000 .
[MASK] [MASK] est né dans une famille juive à [MASK] , dans le [MASK] .
[MASK] a été essentiellement actif dans la théorie quantique des champs et celle des cordes , et dans les domaines connexes de la topologie et de la géométrie .
[MASK] a obtenu de nombreux prix pour ses contributions en physique et en mathématiques .
Il figura sur la liste des 100 personnes les plus influentes du magazine [MASK] en 2004 .
Il devient membre étranger de la [MASK] [MASK] en 1999 .