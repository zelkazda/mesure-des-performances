En créant la série [MASK] [MASK] [MASK] qui est " à la fois un classique du 9 e art et un chef d' œuvre de la science-fiction " , vendue à plus de 2500000 exemplaires , [MASK] a contribué à créer une des séries de bandes dessinées françaises qui font la renommée de celles-ci .
il a exercé les activités de photographe et a collaboré à la conception de plusieurs films pour la télévision et le cinéma , entre autres pour le film de [MASK] [MASK] , [MASK] [MASK] [MASK] .
il fut , pendant un temps , enseignant à l' [MASK] [MASK] [MASK] [MASK] .
C' est [MASK] [MASK] , rédacteur en chef du [MASK] [MASK] , qui décrit [MASK] [MASK] comme " un artisan passionné , qui pousse l' élégance jusqu' à être dénué du moindre sens de l' esbroufe [ ... ] , c' est aussi un assez rare casse-pieds .
C' est vers l' âge de 10 ans , qu' il a eu une véritable " révélation " quand sa marraine lui a offert sa première BD , un album de [MASK] , [MASK] [MASK] [MASK] .
[MASK] [MASK] fait partie du groupe de jeunes sélectionnés , il sera le dessinateur-illustrateur , le plus âgé , 15 ans 1/2 , fait office de rédacteur en chef et c' est [MASK] [MASK] .
Il publie ensuite sans discontinuer des illustrations et des planches sur ses propres scénarios ou sur ceux de [MASK] [MASK] , en fait [MASK] [MASK] .
Au retour de [MASK] , du [MASK] et des [MASK] , [MASK] , [MASK] et deux autres copains , se lancent dans la réalisation d' un western en dessin animé .
[MASK] fait les décors et les personnages , mais le projet ne dépasse pas les 45 secondes .
À la fin de son service militaire , en 1961 , [MASK] trouve par petites-annonces un travail de maquettiste aux studios [MASK] .
Le travail étant particulièrement bien payé , il recommande [MASK] [MASK] qui se joint alors à l' équipe .
[MASK] embauché , fait toute la partie artistique , rough ( esquisses ) , maquettes , prises de vues , etc .
et quelque fois avec l' aide de [MASK] , .
En 1964 , [MASK] repart nourrir son imaginaire " aux amériques " et cette fois c' est certain [MASK] ira le rejoindre .
Mais arrivé aux [MASK] , [MASK] est rentré en [MASK] , alors direction l' ouest pour aller voir les vrais cow-boys loin des charpentes métalliques .
Par la suite , il comblera les temps libres entre deux histoire de [MASK] [MASK] [MASK] en retournant aux [MASK] pour faire des reportages photos .
C' est pendant l' hiver 1965/66 , qu' il retrouve son ami d' enfance [MASK] [MASK] , qui enseigne à l' université de [MASK] [MASK] [MASK] ( [MASK] ) .
[MASK] laisse le soin à [MASK] , de dessiner les dernières cases .
Celui-ci fait intervenir [MASK] à la tête de la cavalerie américaine , il propose le travail à [MASK] qui le publie dans le n o 335 de [MASK] du 24 mars 1966 .
L' histoire , envoyée par le même canal , est publiée dans le n o 351 de [MASK] du 14 juillet 1966 .
Comme rédacteur en chef , il y fait travailler tous ses amis de [MASK] à commencer par [MASK] [MASK] qui a repris en [MASK] son métier de professeur .
Dans le même temps où [MASK] [MASK] , rédacteur en chef , recherche du sang neuf pour [MASK] , [MASK] , rédacteur en chef de [MASK] mais aussi collaborateur à [MASK] , scénarise et fait paraître , dessiné par [MASK] [MASK] , [MASK] [MASK] en janvier 1967 .
[MASK] [MASK] et [MASK] [MASK] cherchaient justement de leur côté le sujet d' une histoire à suivre dite aujourd'hui série .
[MASK] était attiré , suite à son séjour aux [MASK] comme cowboy , par le genre western .
[MASK] était un lecteur moins assidu de science-fiction mais il a lu tous les grands auteurs classiques du genre comme [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] ou [MASK] [MASK] .
À l' époque de la première publication de [MASK] [MASK] [MASK] , les séries de science-fiction sont encore peu nombreuses réalisées par de rares précurseurs d' avant-guerre : Viennent ensuite à partir de 1945 en [MASK] :
Il a vu [ ... ] ce que [MASK] pourrait apporter " , .
C' est à partir de la 4 e aventure [MASK] [MASK] [MASK] [MASK] [MASK] que la série devient pleinement une série de science-fiction avec le thème du voyage dans l' espace .
Concernant la création de [MASK] , [MASK] [MASK] choisit de s' inspirer de la tête de [MASK] [MASK] , un chanteur très populaire à l' époque .
Les premières aventures dessinées par [MASK] [MASK] étaient dans le style de [MASK] , sa seule référence à la bande dessinée américaine déclare-t-il , , matinées de [MASK] , avec des influences de [MASK] , , [MASK] et [MASK] [MASK] , .
À l' origine [MASK] " faisait de la ligne claire " actuellement son graphisme " a toujours ce vieux fond , mais en le maîtrisant mieux , le côté comique n' est plus un poids mais un avantage " .
En fait [MASK] aime accompagner son lecteur tout au long d' une histoire avec un dessin traduisant une lecture la plus linéaire possible avec une mise en page structurée par les exigences du scénario .
À partir des crayonnés longtemps travaillés , il réalise ses planches en noir et blanc en utilisant la plume et le pinceau , " aujourd'hui une planche de [MASK] me prend une semaine " déclare [MASK] et " plus je noircis mes planches plus mon dessin est réaliste " .
Et [MASK] [MASK] de préciser sur le mode humoristique " on peut soutenir que son style se situe plutôt à l' arrière-garde de cette avant-garde qui a révolutionné la bande dessinée dans les années 1960-1970 : des innovations nombreuses , certes , mais dans le strict respect de la tradition [ ... ] on peut même avancer que ce dessin appartient au domaine de l' évidence incontournable : cadrage précis , refus du détail inutile , dépouillement ornemental volontaire , tout concourt à en faire l' archétype du dessin simple , trop simple peut être au goût de ceux qu' éblouissent toujours les maniérismes passagers .
Sa femme étant américaine , [MASK] [MASK] retourne régulièrement en famille aux [MASK] .
Au début des années 1970 , sous l' impulsion de [MASK] [MASK] , il donne des cours sur la bande dessinée à l' [MASK] [MASK] [MASK] [MASK] .
Il a comme étudiants [MASK] [MASK] , [MASK] [MASK] ou encore [MASK] [MASK] [MASK] .
Avec sa vision graphique et son style de narration graphique , il est naturel que [MASK] [MASK] s' intéresse au cinéma .
Il rencontre à [MASK] [MASK] pour lui demander qu' il lui fasse des esquisses de décors .
Malheureusement faute de crédits le projet s' arrête et il ne reste plus du film que les projets de décors imaginés par [MASK] .
Malheureusement les russes n' avait fait que des photos en plan serré et les murailles de la ville de [MASK] sont inexploitable comme décors naturels du fait d' un nombre importants d' éléments insolites .
De retour d' [MASK] , [MASK] travaille trois mois à [MASK] sur les esquisses de décors et de costumes à partir d' un script réalisé par [MASK] [MASK] .
Le montage financier étant assuré , le tournage commence en 1989 aux studio de [MASK] sur le bord de la [MASK] [MASK] .
L' équipe soviétique qui réalise les décors et les costumes s' inspire d' assez loin du travail de [MASK] qui est peu reconnaissable dans le résultat final .
Le travail de [MASK] sur le film [MASK] [MASK] [MASK] de [MASK] [MASK] représente un magistral clin d' œil à la bande dessinée [MASK] [MASK] [MASK] et la bande dessinée va influer de façon significative sur le film .
Pendant toute l' année 92 , [MASK] met de côté l' histoire sur laquelle il avait commencé à travailler [MASK] [MASK] [MASK] [MASK] pour se consacrer au projet .
Pour une scène qui doit se passer à la bibliothèque publique , les personnages se déplacent en métro aérien , et [MASK] agrémente la scène de ses taxis volants et des " limouzingues " des cercles du pouvoir .
Après le succès de [MASK] , [MASK] reprend son projet et réalise ce qui s' appelle désormais [MASK] [MASK] [MASK] en utilisant dans une large mesure les dessins de [MASK] pour ses décors .
[MASK] [MASK] , dont le style graphique donne la prépondérance à la narration graphique , recherchant toujours la mise en page la plus efficace , ne pouvait qu' être attiré par le média télévisuel .
[MASK] et [MASK] [MASK] ont fait plusieurs recherches et tentatives pour transposer pour ce média la série [MASK] [MASK] [MASK] , mais finalement , ils ont cédé les droits pour une série télévisuelle d' animes japonais au dessin dans le style mangas .
La notoriété de [MASK] [MASK] lui permet de se présenter d' une autre façon au public en réalisant des expositions axées sur [MASK] [MASK] [MASK] ou sur la science-fiction ( liste non exhaustive ) :
[MASK] [MASK] a réalisé des scénarisations dans le cadre de manifestations artistiques :
[MASK] [MASK] et la série [MASK] [MASK] [MASK] ont été plusieurs fois distingués ou récompensés dans des salons ou des manifestations françaises et étrangères :