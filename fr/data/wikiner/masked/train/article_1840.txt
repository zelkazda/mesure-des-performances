[MASK] [MASK] [MASK] [MASK] est un continent ou un sous-continent de l' [MASK] , selon le point de vue .
L' [MASK] [MASK] est a l' ouest , l' [MASK] [MASK] est à l' est , et l' [MASK] [MASK] est au nord .
L' [MASK] [MASK] , la partie la plus au sud , en forme d' isthme , relie le continent nord-américain à l' [MASK] [MASK] [MASK] sur le sud-est .
La [MASK] , une région qui est souvent incluse en [MASK] [MASK] [MASK] , est également au sud-est .
Au nord du continent se trouve [MASK] [MASK] [MASK] [MASK] qui regroupe les pays suivants :
Le premier au nord , est l' ensemble anglo-saxon , composé du [MASK] et des [MASK] .
La partie orientale est dominée par des plaines littorales étroites au nord ( [MASK] et [MASK] et plus larges au sud ( [MASK] ) .
Derrière ces espaces plats se trouvent des chaînes de montagnes peu élevées , de formation ancienne et érodées : les [MASK] ne dépassent guère les 2 000 mètres d' altitude .
En allant vers l' ouest , on rencontre des espaces relativement plats et peu élevés , parsemés de lacs ( [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] et les [MASK] [MASK] ) .
Plus au sud , la vallée du [MASK] représente l' épine dorsale du centre de [MASK] [MASK] [MASK] [MASK] .
Ensuite , la région des [MASK] [MASK] puis le piémont des [MASK] se succèdent à des altitudes de plus en plus hautes .
L' ouest de [MASK] [MASK] [MASK] [MASK] est une succession de chaînes plus ou moins parallèles qui constituent un obstacle à la circulation .
Cet ensemble montagneux , plus large aux [MASK] qu' au [MASK] , est entrecoupé de hauts plateaux et de fossés d' effondrement .
: sud-est des [MASK]