[MASK] [MASK] est l' hymne national de la [MASK] .
Les paroles actuelles de [MASK] [MASK] ne datent pas de la révolution belge mais de 1860 et ne furent pas écrites par [MASK] mais par [MASK] [MASK] .
Après les " journées de septembre " , et quelques semaines avant de mourir la tête arrachée par un boulet hollandais près d' [MASK] , [MASK] modifia ses paroles pour condamner la [MASK] [MASK] [MASK] lancée par l' armée hollandaise contre l' indépendance de la [MASK] .
Les paroles actuelles ne sont nullement l' œuvre de [MASK] , elles ne datent que de 1860 et ont été modifiées sur ordre de [MASK] [MASK] .
D' abord pour atténuer les insultes envers le royaume des [MASK] avec lequel la [MASK] était désormais en paix ; et ensuite pour faire croire qu' avant la révolution de 1830 , la [MASK] existait déjà en germe , mais sous " domination étrangère " .
La raison en est , selon [MASK] [MASK] , que la strophe de " l' invincible unité de la [MASK] " est particulièrement malvenue dans un contexte où l' unité de la [MASK] est elle-même menacée .
Différentes commissions ont été chargées d' examiner le texte et la mélodie de [MASK] [MASK] et d' en établir une version officielle .