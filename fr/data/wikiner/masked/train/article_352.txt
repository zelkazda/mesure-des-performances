Elle était la femme de l' acteur [MASK] [MASK] .
Elle devint célèbre et évolua sans heurt vers le cinéma parlant avec [MASK] [MASK] ( 1929 ) .
En 1930 , elle rejoint la [MASK] [MASK] .
En 1932 , elle tourne [MASK] [MASK] [MASK] qui marquera sa rencontre avec [MASK] [MASK] , son futur second époux , dans leur unique film ensemble .
Sa vivacité et son humour vont s' affirmer pleinement dans des comédies comme [MASK] [MASK] ( 1934 ) réalisé par [MASK] [MASK] , [MASK] [MASK] [MASK] ( 1936 ) réalisé par [MASK] [MASK] [MASK] , et [MASK] [MASK] [MASK] ( 1937 ) réalisé par [MASK] [MASK] [MASK] , elle reçoit les louanges des critiques et est décrite comme l' une des pièces maitresse de la screwball comedy .
Produit par [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] sera son seul film en [MASK] .
En octobre 1930 , elle rencontre [MASK] [MASK] .
Ils divorcent en 1933 , mais restent en bons termes et travaillent ensemble sans amertume , notamment dans [MASK] [MASK] [MASK] .
Ils s' offrent un ranch , ancienne propriété du réalisateur [MASK] [MASK] , dans la [MASK] [MASK] [MASK] [MASK] , [MASK] .
Pour tous ceux qui connurent [MASK] , elle était la femme de sa vie .
Après une escale de ravitaillement à [MASK] [MASK] , le vol reprit dans la nuit claire .
Son dernier film , [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( 1942 ) , réalisé par [MASK] [MASK] , aux côtés de [MASK] [MASK] , une satire sur le nazisme et la [MASK] [MASK] [MASK] , était en post-production au moment de sa mort .
Un incident de montage similaire est survenu à la re-sortie du dessin animé de la [MASK] [MASK] [MASK] [MASK] [MASK] ( 1940 ) .
Le 18 janvier 1942 , [MASK] [MASK] ne fit pas son émission habituelle , à la fois par respect pour l' actrice et chagrin pour sa mort .
Le nom gravé sur la crypte est " [MASK] [MASK] [MASK] " .
Malgré son remariage , [MASK] [MASK] fut enterré à ses côtés à sa mort en 1960 .
La maison de son enfance à [MASK] [MASK] a été classée d' importance historique .