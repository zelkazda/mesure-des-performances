Il est le fils posthume de [MASK] [MASK] [MASK] [MASK] [MASK] et d' [MASK] [MASK] [MASK] .
Trop jeune pour régner , il est exclu du pouvoir après la mort de son demi-frère [MASK] [MASK] .
Après la déposition de [MASK] [MASK] [MASK] [MASK] en 887 , [MASK] , qui n' a que huit ans , est encore trop jeune pour monter sur le trône .
Après une période de lutte des deux rois pour un seul pouvoir , ils concluent un accord qui prévoit la succession de [MASK] à la mort d' [MASK] , ce qui devient effectif le 3 janvier 898 .
Le 7 novembre 921 , par le [MASK] [MASK] [MASK] , les deux souverains se reconnaissent mutuellement .
[MASK] est dans un premier temps incarcéré à [MASK] puis , en 924 transféré dans une tour du château de [MASK] ( [MASK] ) .