Un kamikaze est un militaire de l' [MASK] [MASK] [MASK] qui , durant la [MASK] [MASK] [MASK] , effectuait une mission-suicide pendant les [MASK] [MASK] [MASK] .
Au cérémonial de départ d' une attaque , les militaires vouaient allégeance à [MASK] et récitaient un haïku en référence au devoir de sacrifice .
Après la [MASK] [MASK] [MASK] , le mot fut appliqué à d' autres formes d' attaques suicides pour lesquelles le combattant sacrifie délibérément sa vie ( comme dans le cas d' attaques terroristes ) , ou de manière métaphorique lorsqu' une action présente des risques considérables pour la survie d' un individu .
À l' été 1944 , le quartier général impérial , afin de freiner la poussée ennemie , décide de constituer une unité spéciale d' attaque , chargée par son sacrifice d' invoquer les [MASK] pour réitérer le miracle de 1274 .
La première apparition officielle des kamikazes a lieu pendant la [MASK] [MASK] [MASK] [MASK] [MASK] en octobre 1944 .
Le premier était commandé par le lieutenant [MASK] [MASK] .
Ils plongèrent délibérément avec leurs appareils sur les navires de l' [MASK] [MASK] dans ce qui est reconnu pour être la première attaque officielle réussie d' un escadron suicide .
Les kamikazes utilisaient généralement des [MASK] [MASK] , ou tout autre appareil dépassé .
Il est cependant à noter que , concernant les vedettes rapides et les [MASK] , tous les pilotes n' étaient pas prêts à mourir .
Néanmoins plus d' une centaine de jeunes pilotes de la [MASK] se sont portés volontaires ; seuls six d' entre eux ont survécu .