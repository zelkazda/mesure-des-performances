[MASK] [MASK] [MASK] [MASK]
de [MASK] [MASK] , écrit en 1985 , est une ébauche de manifeste pour une révolution ludique .
[MASK] [MASK] analyse et décrypte le temps travaillé comme un temps de servitude , de résignation qui tue le temps du plaisir et de la connaissance .