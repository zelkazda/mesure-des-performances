Sa capitale était [MASK] ( actuelle [MASK] ) .
En 462 , une révolte de la [MASK] est écrasée .
Elle fut conquise par [MASK] .
Lors de la conquête d' [MASK] [MASK] [MASK] ( 328 ) , elle formait alors une des grandes satrapies de la monarchie persane .
[MASK] , satrape de [MASK] , assassina [MASK] [MASK] , son maître , afin de se rendre indépendant dans sa satrapie ; mais il n' y réussit pas : [MASK] joignit ce pays à ses conquêtes .
Il fut envahi en 130 par les nomades [MASK] venu du nord .
À leur chute , les [MASK] de la [MASK] s' emparèrent de toutes leurs conquêtes à l' ouest ; les [MASK] , 90 av .
J.-C. , prirent possession du reste , et fondèrent un nouveau royaume de [MASK] dont les dimensions varièrent souvent .
La [MASK] de l' âge du bronze ( v. -3000 /v. -1200 ) , contrairement à la [MASK] et à l' [MASK] , n' a , à ce jour , livré aucun document écrit .
[MASK] [MASK] et [MASK] [MASK] ( dir .