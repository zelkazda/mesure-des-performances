[MASK] [MASK] est un système d' exploitation de la société [MASK] .
C' est le successeur de [MASK] [MASK] .
Il fut suivi par [MASK] [MASK] .
Il constitue la seconde version de [MASK] [MASK] .
Les nouveautés sont la prise en charge native du [MASK] et de l' [MASK] .
C' est également le premier système [MASK] à fournir [MASK] [MASK] par défaut .
Ce qui vaudra un rappel à l' ordre de la part de la [MASK] [MASK] .
En plus de la correction de nombreux problèmes mineurs , [MASK] [MASK] 5 était livré à la place de la version 4 .
Au chapitre des améliorations , on peut enfin citer l' ajout de [MASK] dans sa version 3 et l' ajout d' un programme de la lecture des DVD ( qui nécessite toutefois l' installation d' un codec tiers non fourni pour être fonctionnel ) .
Bien que cette version ne soit finalement qu' une mise à jour de [MASK] [MASK] dans sa première édition , elle a connu un grand succès .
Le 14 septembre 2000 , [MASK] [MASK] a été remplacé par [MASK] [MASK] , lui-même remplacé par [MASK] [MASK] [MASK] l' année suivante .
[MASK] souhaitait arrêter son support technique pour [MASK] [MASK] le 16 janvier 2004 , mais , en raison de la popularité encore élevée du système , la date de fin de support technique fut fixée au 11 juillet 2006 .
Le support de [MASK] [MASK] s' est également terminé à cette date .
Sorti pourtant plus tôt , le support de [MASK] [MASK] s' est achevé seulement le 13 juillet 2010 .
Il possède encore un mode [MASK] 16 bits réel .
Tous les utilisateurs et leurs programmes en découlant possèdent ainsi un accès illimité au matériel , ce qui permet au [MASK] [MASK] de détruire des cartes mères .