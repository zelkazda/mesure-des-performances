Une étrange histoire qui court sur la naissance de [MASK] nous raconte que la mère de [MASK] , affectée de rhumatisme qui l' empêchait de se mouvoir , fut guérie par un paysan qui lui promit , alors qu' elle avait 48 ans , la naissance d' un fils .
En effet [MASK] nait un an après ces faits , le 23 décembre 1790 à [MASK] et est baptisé le soir même , .
La révolution fait alors rage à [MASK] et le père de [MASK] est plutôt dans la mouvance jacobine même s' il est douteux qu' il soit secrétaire de police .
Il est élevé principalement par son frère , mais celui-ci part à [MASK] en juillet 1798 , .
Fin mars 1801 , il part de [MASK] pour arriver à [MASK] le 27 mars 1801 quittant sa famille pour rejoindre son frère [MASK] qui dirige son éducation .
[MASK] est son élève de novembre 1802 aux vacances d' été de 1804 .
Ses cours se passent alors pour les lettres auprès de l' abbé et pour le reste à l' école centrale de [MASK] .
Il veut alors s' engager dans l' étude de cette langue , mais il ne peut le faire , [MASK] offrant trop peu de ressources .
Il assiste aussi à ceux de l' [MASK] [MASK] [MASK] [MASK] dans les mêmes matières et fréquente la [MASK] [MASK] .
Au printemps 1809 , il se met à rédiger une grammaire copte et étudie le texte démotique de [MASK] [MASK] [MASK] [MASK] .
Lors de l' été 1809 , il commence une grammaire du " langage thébain-sahidique " , celui de la communauté copte de la région de [MASK] .
Il continue de travailler à essayer de déchiffrer le texte démotique de [MASK] [MASK] [MASK] [MASK] .
Il rentre à [MASK] le 15 octobre 1809 pour prendre possession de son poste .
Pour contrer [MASK] , il publie la préface de son ouvrage le 1 er mars 1811 , mais malgré tout après son concurrent , il remporte néanmoins un grand succès auprès notamment d' [MASK] [MASK] [MASK] , directeur de la [MASK] [MASK] [MASK] [MASK] .
Il continue le déchiffrement et écrit à son ami [MASK] [MASK] , le 15 octobre 1812 :
Il découvre aussi que les vases canopes servent à conserver les viscères en découvrant un morceau momifié dans un des vases canopes de la bibliothèque de [MASK] .
Enfin , il doit avec son frère partir en exil à [MASK] , le 18 mars 1816 .
Arrivés le 2 avril 1816 à [MASK] , les deux frères [MASK] s' établissent dans la maison de leur père .
Il s' amuse dans les salons de [MASK] , écrit des poèmes et des satyres politiques , .
À partir de juin 1816 , ils recherchent le site de l' oppidum d' [MASK] , qu' ils identifient comme étant l' actuelle [MASK] .
L' aîné part à [MASK] en avril et le cadet reste à [MASK] pour régler les dettes de son père .
Il rentre enfin à [MASK] le 21 octobre 1817 .
Mais , chassé de la bibliothèque par le préfet , il préfère quitter [MASK] le 11 juillet 1821 .
En 1816 , il est exilé à [MASK] , en raison de ses opinions bonapartistes et doit interrompre ses recherches .
À partir de 1821 , il déchiffre les premiers cartouches royaux , dont celui de [MASK] [MASK] sur la [MASK] [MASK] [MASK] , puis celui de [MASK] sur la base d' un obélisque et sur un papyrus bilingue .
Il est nommé en 1826 conservateur chargé des collections égyptiennes au [MASK] [MASK] [MASK] .
De 1828 à 1830 , il réalise enfin son rêve : il part pour une mission scientifique en [MASK] , où il part avec son collaborateur et ami [MASK] [MASK] , et y recueille de nombreuses données et objets .
Cependant , il meurt à [MASK] le 4 mars 1832 , à l' âge de 41 ans , et est enterré au [MASK] [MASK] [MASK] , à [MASK] .
Son nom a été donné , entre autres , aux lycées de [MASK] , de [MASK] , de [MASK] , de [MASK] , ainsi qu' à l' université d' [MASK] .
Un musée consacré à [MASK] [MASK] a été créé dans la maison natale du père de l' égyptologie à [MASK] dans le [MASK] .
L' artiste [MASK] [MASK] [MASK] crée un moucharabieh typographique polyglotte , une installation typographique sur l' une des façades qui est entièrement percée de pictogrammes , d' idéogrammes originaires du monde entier .