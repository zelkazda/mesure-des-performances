Les villes principales sont la capitale [MASK] ( [MASK] de 1945 à 1992 ) avec 170000 habitants , [MASK] ( 75000 ) et [MASK] ( 37000 ) .
L' ancienne capitale royale est [MASK] qui détient également le titre de " capitale de trône " ( prestonica ) .
Le [MASK] a une longue histoire de plusieurs siècles en tant que duché semi-indépendant , puis principauté autonome , puis en tant que royaume indépendant en 1910 , avant qu' il ne rejoigne le [MASK] [MASK] [MASK] en 1918 .
Le 15 décembre 2008 , le [MASK] présente sa candidature à l' [MASK] [MASK] .
Le [MASK] mène un processus d' adhésion à l' [MASK] [MASK] .
Le processus d' adhésion impose au [MASK] de régler les problèmes liés à la criminalité , à la contrebande , au nationalisme ( la région doit toujours faire face aux problèmes liés au multiculturalisme ) , à la corruption , à la liberté d' information et à la capture des criminels de guerre .
Le processus de séparation administrative d' avec la [MASK] est , en outre , encore loin du terme .
Toutefois , ces dernières années le [MASK] a progressé dans pratiquement tous les critères d' adhésion [ réf. nécessaire ] .
Le [MASK] est situé dans les [MASK] .
Le territoire monténégrin s' étend depuis les hautes montagnes à la frontière avec la [MASK] et l' [MASK] -- une partie des karsts de l' ouest de la péninsule balkanique -- jusqu' à une étroite plaine côtière de deux à six kilomètres de large .
La région karstique du [MASK] se situe à environ 1000 m d' altitude , certaines parties montant à près de 2000 m , comme le mont [MASK] ( 1894 m ) , point culminant des chaînes calcaires côtières .
La vallée de la [MASK] est la zone la plus basse , avec une altitude d' environ 500 m .
Le [MASK] possède quelques cavités naturelles .
Les montagnes du [MASK] comptent parmi les terrains les plus accidentés d' [MASK] et parmi les parties qui furent les plus érodées dans la péninsule balkanique pendant la dernière ère glaciaire .
Le territoire culmine au [MASK] [MASK] , dans les monts [MASK] , à une altitude de 2534 m .
Du point de vue administratif , le [MASK] est divisé en 21 " municipalités " ( opština en serbe ) , regroupant chacune une ville principale dont elle porte le nom .
La langue parlée au [MASK] est la variante iékavienne du [MASK] .
Le gouvernement a provoqué un mouvement de protestation chez les professeurs partisans de l' union avec la [MASK] en souhaitant que les manuels appellent uniquement " langue maternelle " ( et non plus " serbe " ) la langue parlée au [MASK] .
Même si certains linguistes utilisent encore le terme " serbo-croate " pour définir la langue parlée au [MASK] , en [MASK] , en [MASK] et en [MASK] , officiellement le " serbo-croate " n' existe plus , chaque pays nommant sa langue : " monténégrin " , " serbe " , " bosniaque " ( voire " bosno-serbe " ou " bosnien " en fonction de l' écriture utilisée ) ou " croate " .
C' est pourquoi la linguistes ont adopté , en majorité , un nouveau nom pour celle langue : le [MASK] .
En revanche , il y a des différences partielles de lexique ( certains mots , certaines conjugaisons ou déclinaisons varient ) et surtout une différence d' alphabet : il est cyrillique et latin en [MASK] ( avec une utilisation courante de l' alphabet latin en dehors des situations officielles ) et dans la [MASK] [MASK] [MASK] [MASK] , mais latin en [MASK] et dans la [MASK] [MASK] [MASK] .
La petite minorité croate est localisée pour sa presque totalité dans la [MASK] [MASK] [MASK] .
Les [MASK] du [MASK] représentent 10 % de la population et sont majoritairement de confession musulmane .
Ils se trouvent surtout au sud du [MASK] à la frontière avec l' [MASK] .
Cela interdit le droit de vote à la diaspora monténégrine , qui compte 800.000 rien qu' en [MASK] , alors que le [MASK] est peuplé de un peu moins de 700000 habitants .
Les villes de [MASK] et de [MASK] sont particulièrement riches en musées et galeries d' art .
Elle est très proche de la musique serbe , sa voisine immédiate , avec laquelle elle a été liée par l' histoire , de même qu' avec les autres musiques formant l' ex- [MASK] .
Après l' indépendance , les fédérations sportives nationales monténégrines ont été fondées , dont la [MASK] [MASK] [MASK] [MASK] [MASK] .
L' équipe nationale de football joue à domicile au [MASK] [MASK] [MASK] .
D' une capacité de 24 000 places , il s' agit de l' unique stade monténégrin répondant aux normes de la [MASK] pour la tenue de matchs internationaux .
En water-polo , l' équipe nationale masculine remporte le [MASK] [MASK] [MASK] [MASK] [MASK] et la [MASK] [MASK] de 2009 .
Durant les années 1990 et jusque dans les années 2000 , le [MASK] [MASK] [MASK] remporta de nombreuses victoires aux niveaux yougoslave et européen .