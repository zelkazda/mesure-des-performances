Il fut assassiné le 14 mai 1610 par un fanatique charentais , [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] à [MASK] .
Aussitôt né , [MASK] est donc remis entre ses mains .
[MASK] passe une partie de sa petite enfance dans la campagne de son pays au château de [MASK] .
Fidèle à l' esprit du calvinisme , sa mère [MASK] [MASK] [MASK] prend soin de l' instruire dans cette stricte morale , selon les préceptes de la [MASK] .
[MASK] [MASK] [MASK] obtient de [MASK] [MASK] [MASK] le contrôle de son éducation et sa nomination comme gouverneur de [MASK] ( 1563 ) .
En 1567 , [MASK] [MASK] [MASK] le fait revenir vivre auprès d' elle dans le [MASK] .
Sous la tutelle de l' amiral de [MASK] , il assiste aux batailles de [MASK] , de [MASK] [MASK] [MASK] [MASK] et de [MASK] .
Il combat pour la toute première fois en 1570 , lors de la bataille d' [MASK] .
En 1572 , succédant à sa mère [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] devient roi de [MASK] sous le nom d' [MASK] [MASK] .
Le 18 août 1572 , il est marié à [MASK] à la sœur du roi [MASK] [MASK] , [MASK] [MASK] [MASK] .
Ce mariage auquel s' était opposée [MASK] [MASK] [MASK] dans un premier temps , a été arrangé pour favoriser la réconciliation entre catholiques et protestants .
Comme [MASK] [MASK] [MASK] , étant catholique , ne peut se marier que devant un prêtre , et que [MASK] , ne peut entrer dans une église , leur mariage fut célébré sur le parvis de [MASK] .
Cependant , dans un climat très tendu à [MASK] , et suite à un attentat contre [MASK] [MASK] [MASK] , le mariage est suivi quelques jours plus tard du [MASK] [MASK] [MASK] [MASK] .
À l' avènement d' [MASK] [MASK] , il reçoit à [MASK] un nouveau pardon du roi et participe à la cérémonie de son sacre à [MASK] .
[MASK] est désormais confronté à la méfiance des protestants qui lui reprochent son manque de sincérité religieuse .
Il se tient à l' écart du [MASK] qui est fermement tenu par les calvinistes .
[MASK] est plus encore confronté à l' hostilité des catholiques .
En décembre 1576 , il manque de mourir dans un piège organisé dans la cité d' [MASK] et [MASK] , capitale de son gouvernement refuse de lui ouvrir ses portes .
[MASK] s' installe alors le long de la [MASK] à [MASK] et à [MASK] qui a l' avantage d' être situé non loin de son [MASK] [MASK] [MASK] .
Ses conseillers sont essentiellement protestants , tels [MASK] et [MASK] [MASK] [MASK] .
D' octobre 1578 à mai 1579 , la reine mère [MASK] [MASK] [MASK] lui rend visite pour achever la pacification du royaume .
Sous l' influence de l' idéal platonique imposé par la reine , une atmosphère de galanterie règne sur la cour qui attire également un grand nombre de lettrés ( comme [MASK] et [MASK] [MASK] ) .
[MASK] se laisse aller lui-même aux plaisirs de la séduction .
[MASK] participe ensuite à la septième guerre de religion relancée par ses coreligionnaires .
La prise de [MASK] , en mai 1580 , où il réussit à éviter pillage et massacre malgré cinq jours de combats de rue , lui vaut un grand prestige à la fois pour son courage et son humanité .
Les aventures féminines du roi créent la discorde au sein du couple qui n' a toujours pas d' enfants et provoquent le départ de [MASK] pour [MASK] .
Le coup d' éclat de [MASK] à [MASK] ( 1585 ) consommera leur rupture définitive .
Il lui envoie le [MASK] [MASK] [MASK] pour l' inviter à se convertir et à revenir à la cour .
La rumeur dit qu' en une nuit , la moitié de la moustache du futur [MASK] [MASK] blanchit .
Commence alors un conflit où [MASK] [MASK] [MASK] affronte à plusieurs occasions le [MASK] [MASK] [MASK] .
La mort du prince [MASK] [MASK] [MASK] le place clairement à la tête des protestants .
L' élimination violente du [MASK] [MASK] [MASK] l' amène à se réconcilier avec [MASK] [MASK] .
Les deux rois se retrouvent tous les deux au [MASK] [MASK] [MASK] et signent un traité le 30 avril 1589 .
Les catholiques de la [MASK] refusent de reconnaître la légitimité de cette succession .
Conscient de ses faiblesses , [MASK] [MASK] doit d' abord commencer par conquérir les esprits .
Beaucoup hésitent à le suivre , certains protestants comme [MASK] [MASK] quittent même l' armée , qui passe de 40000 à 20000 hommes .
Il échoue par la suite à reprendre [MASK] , mais prend d' assaut [MASK] .
Encouragé par l' amour de sa vie , [MASK] [MASK] [MASK] , et surtout très conscient de l' épuisement des forces en présence , tant au niveau moral que financier , [MASK] [MASK] , en fin politique , choisit d' abjurer la foi calviniste .
Le 4 avril 1592 , par une déclaration connue sous le nom d ' " expédient " , [MASK] [MASK] annonce son intention d' être instruit dans la religion catholique .
[MASK] [MASK] abjure solennellement le protestantisme , le 25 juillet 1593 en la [MASK] [MASK] .
On lui a prêté , bien à tort , le mot selon lequel " [MASK] vaut bien une messe " ( 1593 ) , même si le fond semble plein de sens .
L' augmentation des impôts consécutive ( multiplication par 2,7 de la taille ) provoque la révolte des croquants dans les provinces les plus fidèles au roi , [MASK] , [MASK] , [MASK] et [MASK] .
[MASK] [MASK] est sacré le 27 février 1594 en la [MASK] [MASK] [MASK] .
Il bat de manière définitive l' armée de la [MASK] à [MASK] .
En 1595 , [MASK] [MASK] déclare officiellement la guerre contre [MASK] [MASK] .
[MASK] [MASK] approche de la cinquantaine et n' a toujours pas d' héritier légitime .
Depuis quelques années , [MASK] [MASK] [MASK] partage sa vie mais , n' appartenant pas à une famille régnante , elle ne peut guère prétendre devenir reine .
[MASK] [MASK] compromet son mariage et sa couronne en poursuivant sa relation extraconjugale , commencée peu de temps après la mort de [MASK] [MASK] [MASK] , avec [MASK] [MASK] [MASK] , jeune femme ambitieuse , qui n' hésite pas à faire du chantage au roi , pour légitimer les enfants qu' elle a eus de lui .
En 1609 , après plusieurs autres passades , [MASK] se prendra de passion pour une jeune fille [MASK] [MASK] [MASK] [MASK] .
Il poursuit ainsi la construction du [MASK] [MASK] commencé sous son prédécesseur .
[MASK] [MASK] se montre fervent catholique -sans être dévot- et pousse sa sœur et son ministre [MASK] à se convertir ( aucun d' eux ne le fera ) .
La [MASK] [MASK] [MASK] est créée , les arts et techniques encouragés .
D' autres projets sont préparés mais ensuite abandonnés à la mort d' [MASK] [MASK] .
Mais dans une querelle avec le [MASK] [MASK] [MASK] , il aurait prononcé son désir que chaque laboureur ait les moyens d' avoir une poule dans son pot .
[MASK] [MASK] estime son armée prête à reprendre le conflit qui s' était arrêté dix ans plus tôt .
Tout en préparant la guerre , on s' apprête au couronnement officiel de la reine à [MASK] qui se déroule le 13 mai 1610 .
Le lendemain , [MASK] [MASK] meurt assassiné par [MASK] [MASK] , un catholique fanatique , dans la [MASK] [MASK] [MASK] [MASK] à [MASK] .
[MASK] [MASK] est enterré à la [MASK] [MASK] le 1 er juillet 1610 , à l' issue de plusieurs semaines de cérémonies funèbres .
[MASK] [MASK] eut six enfants de son mariage avec [MASK] [MASK] [MASK] :
[MASK] [MASK] eut également au moins 12 enfants illégitimes :
En l' honneur d' [MASK] [MASK] , [MASK] écrit en 1728 un poème intitulé [MASK] [MASK] .
Malgré cette image positive , son tombeau de [MASK] n' échappe pas à la profanation en 1793 , due à la haine des symboles monarchiques sous la [MASK] [MASK] .
Le corps d' [MASK] [MASK] est le seul de tous les rois à être trouvé dans un excellent état de conservation .
[MASK] [MASK] ordonnera leur exhumation et leur retour dans la crypte , où elles se trouvent encore aujourd'hui .
Le [MASK] [MASK] [MASK] continue de cultiver la légende du bon roi [MASK] .
Avant d' être aimé du peuple , [MASK] [MASK] fut donc l' un des rois les plus détestés , son effigie brûlée et son nom associé au diable .
Son assassinat par [MASK] est même vécu par certains comme une délivrance , au point qu' une rumeur d' une nouvelle [MASK] se répand durant l' été 1610 .
La popularité croissante du roi peut tenir à son attitude lors des sièges : il veille à ce que les villes prises ne soient pas pillées , et leurs habitants épargnés ( et ce , dès le siège de [MASK] en 1580 ) .
Il se montre magnanime également avec ses anciens ennemis ligueurs , notamment après la reddition de [MASK] .