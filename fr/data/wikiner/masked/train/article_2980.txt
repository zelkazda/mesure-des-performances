[MASK] [MASK] est né dans une famille industrielle haut-marnaise , d' un père amiral et d' une mère professeur de danse .
Diplômé , il intègre le groupe international de cosmétiques [MASK] [MASK] en 1990 .
Il adhère en 1991 au [MASK] [MASK] .
[MASK] [MASK] lui conseille de se construire une expérience professionnelle avant de se lancer en politique .
En 1993 , il devient conseiller municipal de [MASK] , d' où est originaire sa famille , puis conseiller municipal de [MASK] en 1996 .
En 1998 , il devient vice-président du conseil régional de [MASK] , poste qu' il occupera jusqu' en 2004 .
En 2004 , il devient secrétaire national puis porte-parole national de l' [MASK] dirigé par [MASK] [MASK] , conservant ce poste malgré la prise en main du parti par [MASK] [MASK] .
Il fait partie du groupe de ministres que le [MASK] [MASK] consulte directement .
Il intervient dans les dossiers de la crise du secteur automobile et de la téléphonie ( lancement du 33700 , service de lutte contre les [MASK] frauduleux ) .
Il déçoit les associations de consommateur comme l' [MASK] [MASK] [MASK] [MASK] sur la question de l' instauration d' une action de groupe dans la législation française .