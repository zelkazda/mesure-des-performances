Le basque ( euskara ) est une langue non indo-européenne parlée au [MASK] [MASK] ( [MASK] et [MASK] ) et le seul isolat d' [MASK] .
Désormais enseigné partout , sa normalisation par l' [MASK] [MASK] [MASK] [MASK] [MASK] ne date que de 1968 .
En [MASK] , le nombre de locuteurs est de 734100 ( provinces de [MASK] , [MASK] , [MASK] et de [MASK] ) .
Le basque est un isolat en [MASK] ( le seul ) .
C' est aussi l' une des quatre familles linguistiques d' [MASK] , avec les langues finno-ougriennes ( finnois , estonien , lapon etc .
Le préhistorien basque [MASK] [MASK] [MASK] [MASK] [MASK] montra l' origine préhistorique de certains termes du vocabulaire basque ; ainsi la hache se dit aizkora , or la racine aiz signifie " pierre " .
Sur une population totale de 2,975,000 habitants répartis dans les 7 provinces du [MASK] [MASK] , 26,9 % sont bilingues et 15,3 % ont une connaissance approximative du basque , soit 1,255,750 personnes .
Du point de vue de leur rapport avec l' euskara , les habitants du [MASK] [MASK] se répartissent en 4 grandes catégories .
La [MASK] compte 1,141,000 habitants , dont 26,5 % ( 302,000 ) sont bilingues et 24,9 % ( 284,000 ) de bilingues passifs .
Le [MASK] avec 686,000 habitants a le plus grand nombre de locuteurs bascophones , soit 329,000 , ce qui correspond à 48 % de la population et 9,5 % ( 65,000 ) de bilingues passifs .
La [MASK] ( 594,000 ) n' a que 10,5 % ( 85,500 ) de bascophones qui sont regroupés essentiellement dans le nord de la province et 6,8 % de bilingues passifs ( 40,200 ) .
L' [MASK] avec ses 298,000 habitants a 13,4 % ( 40,000 ) de bilingues et 11,1 % ( 33,000 ) de bilingues passifs .
Les radios associatives du [MASK] [MASK] [MASK] se sont regroupées dans une association nommée [MASK] irratiak et diffusent des programmes en commun :
De plus [MASK] [MASK] [MASK] [MASK] , consacre 55 minutes d' actualité en basque .