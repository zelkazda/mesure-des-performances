Sa population de plus de 1420 millions d' habitants ( plus de 1400 millions pour la [MASK] en 2010 , et plus de 20 millions pour la [MASK] [MASK] [MASK] ) est la première au monde et représente plus du cinquième de la population mondiale .
Elle trouve son origine dans la vallée du [MASK] [MASK] puis s' est étendue vers le sud , vers l' ouest ( premières incursions en [MASK] [MASK] sous les [MASK] , extension temporaire jusqu' à la [MASK] [MASK] sous les [MASK] , conquête du [MASK] et du [MASK] sous les [MASK] ) et vers le nord .
La dernière dynastie impériale , les [MASK] ( la dynastie d' origine mandchoue qui régnait sur le pays depuis 1644 ) , a connu une période de déclin durant la phase d' expansion coloniale des pays occidentaux , menant le pays de défaite en défaite à partir des guerres de l' opium .
Elle est aujourd'hui souvent qualifiée d ' " usine du monde " et des études ont montré que le panier d' un ménage français , dans sa partie non alimentaire , contenait pour moitié des produits fabriqués en [MASK] .
D' autres courants de pensée et d' autres personnages ont contribué à former la culture chinoise , ainsi [MASK] et le taoïsme , le bouddhisme , le néo-confucianisme et jusqu' au marxisme plus récemment .
Le plus courant aujourd'hui est en sinogrammes traditionnels 中國 ; en sinogrammes simplifiés 中国 ; en pinyin [MASK] ( prononcé /djongkwo/ ) qui signifie " pays du milieu " ou par abus de langage " empire du milieu " .
Il est aujourd'hui inclus dans les appellations de la [MASK] [MASK] [MASK] et de la [MASK] [MASK] [MASK] [MASK] ( [MASK] [MASK] [MASK] ; 中華人民共和國 / 中华人民共和国 ) .
( [MASK] [MASK] [MASK] [MASK] )
) , il est généralement admis que l' âge du bronze en [MASK] a commencé aux alentours de 2100 av .
J.-C. , durant la dynastie des [MASK] , .
Mais c' est sous la dynastie des [MASK] ( de 1766 à 1122 av .
En près de deux millénaires , plusieurs royaumes furent fondés sur le territoire chinois par des ethnies non- [MASK] ou mixtes , tels que les [MASK] ou les [MASK] .
J.-C. , ainsi que de la [MASK] [MASK] de [MASK] .
Une nouvelle dynastie d' ethnie [MASK] reconquiert finalement le pouvoir en 1368 ; c' est la [MASK] [MASK] [MASK] , qui s' efforcera de retrouver la gloire du passé , sans toujours en retrouver le dynamisme .
En 1851 commença la [MASK] [MASK] [MASK] , alimentée par les croyances des sociétés secrètes de [MASK] méridionale , et prônant un mouvement de réformes radicales .
[MASK] [MASK] , devenu président , proclame le rétablissement de la monarchie en 1915 .
En 1921 , le [MASK] [MASK] [MASK] est créé à [MASK] .
Entre-temps , [MASK] [MASK] a multiplié les contacts et demandes d' assistance auprès de la jeune [MASK] [MASK] .
En avril 1927 , il proclame l' établissement de la capitale à [MASK] , instaurant la période dite de la [MASK] [MASK] [MASK] .
Fin 1931 , [MASK] [MASK] proclame la [MASK] [MASK] [MASK] .
Fin 1935 , il se fixe avec les quelques dizaines de milliers de survivants à [MASK] .
En 1948 , les troupes du [MASK] étaient démoralisées , épuisées par la guerre antijaponaise et la corruption du parti nationaliste .
Dès la [MASK] [MASK] [MASK] [MASK] [MASK] et surtout après son installation à [MASK] , [MASK] [MASK] avait rompu avec les principes marxistes-léninistes traditionnels , fondés sur les révolutions urbaines à base ouvrière .
Le 1 er octobre 1949 , [MASK] [MASK] proclamait la [MASK] [MASK] [MASK] [MASK] à [MASK] .
En décembre , [MASK] [MASK] proclamait [MASK] capitale provisoire de la [MASK] [MASK] [MASK] .
Après 20 années de réformes et d' âpres négociations , les efforts entrepris ont été couronnés par l' adhésion de la [MASK] ( et de [MASK] à cette même date ) à l' [MASK] [MASK] [MASK] [MASK] ( l' [MASK] ) , à compter du 1 er janvier 2002 , lui donnant les outils nécessaires à la croissance économique spectaculaire qu' on lui connait aujourd'hui .
L' appellation " [MASK] [MASK] [MASK] [MASK] " ne doit pas abuser : l' ouvrier américain est bien mieux protégé que son collègue chinois .
Les dynasties [MASK] et particulièrement [MASK] , premières entités géopolitiques à l' origine du futur empire chinois fondé par [MASK] [MASK] [MASK] , étaient situées dans la région du [MASK] [MASK] .
L' importance territoriale de la [MASK] [MASK] de [MASK] a été réduite avec l' accession au pouvoir de la [MASK] [MASK] , qui inclut la [MASK] , située au nord de la muraille , dans son territoire .
Siège d' une , puis deux préfectures provinciales , [MASK] fut cédé au [MASK] après la première guerre sino-japonaise en 1895 .
Plusieurs dynasties ont eu des volontés expansionnistes , s' engageant dans des régions telles que la [MASK] [MASK] , la [MASK] , le [MASK] , et le [MASK] .
La dynastie mandchoue des [MASK] et ses successeurs , la [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] , ont cimenté les incorporations de ces territoires .
Ce problème de délimitation a donné lieu à une série de critiques sur l' intégration de certains territoires en [MASK] , notamment celle du [MASK] et du [MASK] ( 新疆 , pinyin [MASK] , qui signifie " nouvelle frontière " ou " nouveau territoire " en chinois ) .
La plupart des terres arables chinoises se situent autour des deux fleuves principaux , le [MASK] [MASK] et le [MASK] [MASK] , qui sont aussi les foyers principaux des anciennes civilisations chinoises .
À l' est , sur le littoral de la [MASK] [MASK] et de la partie orientale de la mer de [MASK] , se trouvent de vastes plaines alluviales toujours densément peuplées ; le littoral de la partie méridionale de la mer de [MASK] est plus montagneux .
Les formations paléozoïques de [MASK] sont pour la plupart marines ; les dépôts du mésozoïque et du tertiaire proviennent d' estuaires et d' eaux douces , ou de terres .
Les conséquences de l' industrialisation et de la déforestation sont considérées être à l' origine des tempêtes de sable en provenance du [MASK] [MASK] [MASK] qui frappent la capitale , et de l' augmentation des violents typhons qui frappent le sud du pays .
Au mois d' octobre 2009 , la [MASK] est la 3 e puissance économique mondiale derrière les [MASK] et le [MASK] .
[MASK] pour sa part est la 26 e puissance économique mondiale ( mais la 19 e si on raisonne en parité de pouvoir d' achat ) .
L' essentiel de ce commerce se fait avec l' [MASK] [MASK] ( 20,4 % ) , les [MASK] ( 17,7 % ) , [MASK] [MASK] ( 13,4 % ) et le [MASK] ( 8,1 % ) .
Aujourd'hui , 39 % des exportations en provenance de [MASK] sont réalisées par des entreprises dont le capital est à cent pour cent étranger et 20 % sont le fait de partenariat entre les sociétés étrangères et les sociétés chinoises .
Un ouvrier non qualifié en [MASK] coûte environ un dollar américain par heure , ce qui est bien en dessous des minima des pays industrialisés .
La forêt y avait déjà fortement régressé il y a huit mille ans , ce qui a engendré des problèmes de désertification , d' érosion et dégradation des sols , ainsi que d' inondations , qui ont empiré avec l' industrialisation et la croissance démographique notamment sur le littoral et dans le bassin de la [MASK] [MASK] [MASK] .
Le pays possède à peu près la même quantité d' eau que les [MASK] , mais sa population y est cinq fois supérieure .
Le cas le plus emblématique est probablement le panda géant , qui a été choisi comme symbole par le [MASK] [MASK] [MASK] [MASK] [MASK] .
Sa population est la plus grande du monde , dépassant les 1,36 milliard d' individus ( 2008 ) , soit 20 % environ des 6,8 milliards d' individus vivant dans le monde d' après les estimations de l' [MASK] .
En 2005 , la [MASK] a enregistré plus de 16 millions de naissances et environ 8,49 millions de décès .
Quant à [MASK] [MASK] [MASK] ( [MASK] ) , elle compte de son côté plus de 22 millions d' habitants .
L' égalité en devoirs et en droits de toutes ces nationalités est inscrite dans le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , .
D' après la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , les nationalités quelles qu' elles soient bénéficient du droit " de développer leur propre langue parlée et écrite ainsi que de préserver ou réformer leurs propres us et coutumes " , ainsi que d' une priorité de recrutement dans les entreprises ou dans les établissements d' une région autonome .
D' après le gouvernement de [MASK] [MASK] [MASK] [MASK] [MASK] , ce type d' affirmations est le fait de mouvements qu' il qualifie de " séparatistes " .
Il y aurait aujourd'hui plus de 1 500 temples taoïstes en [MASK] .
Naturellement voué aux interprétations des dynasties régnantes , la doctrine originelle de [MASK] n' est toutefois pas nécessairement synonyme de soumission aux institutions , comme certains contemporains l' observent .
[MASK] [MASK] et la [MASK] en particulier ont largement bénéficié de la richesse de transmission des traditions bouddhiques en [MASK] .
L' une de ses formes , le bouddhisme tibétain ( ou lamaïsme ) , répandu surtout au [MASK] et en [MASK] [MASK] , recrute de plus en plus d' adeptes parmi les [MASK] depuis quelques décennies .
On estime qu' il y a environ 13 000 temples bouddhistes en [MASK] .
D' après la légende , le fondateur de cette doctrine serait [MASK] le 28 e patriarche du bouddhisme indien qui , venu de l' [MASK] , s' est rendu entre 520 et 526 à [MASK] en [MASK] pour y créer le monastère du même nom .
D' après la tradition , c' est là qu' il aurait mis au point les bases d' un art martial chinois : le [MASK] [MASK] , art martial reposant sur une fine connaissance des règles initiatiques issues de la rencontre du taoïsme et du bouddhisme .
Le périple de [MASK] est le sujet de nombreuses publications .
Le [MASK] a également été un de ces points d' échanges sino-islamiques : le célèbre navigateur [MASK] [MASK] était un musulman originaire du [MASK] .
L' islam a connu sa plus forte expansion sous la dynastie des [MASK] ( 元 ) ( 1271 -- 1368 ) .
La suppression de la [MASK] [MASK] [MASK] en 1773 par le pape mit fin à la mission .
Une communauté plus récente existe à [MASK] .
Plus récemment , malgré la méfiance du [MASK] vis-à-vis des organisations ou mouvements civils de grande envergure , une nouvelle religion -- ou secte -- syncrétiste , [MASK] [MASK] , a émergé en 1992 .
Traditionnellement , on considère que la boussole , l' imprimerie , le papier et la poudre à canon sont les " Quatre grandes inventions " de la [MASK] .
L' impression de ces premiers textes est généralement considérée comme relevant de l' influence chinoise , très forte en cette époque de pénétration de la culture et du bouddhisme chinois au [MASK] , .
Il a été trouvé en 2006 à [MASK] , dans la province du [MASK] , et a été daté en fonction d' autres documents écrits trouvés au même endroit de la fouille .
D' après une autre tradition chinoise , ce serait [MASK] [MASK] , , ministre de l' agriculture qui , en 105 , aurait codifié pour la première fois l' art de fabriquer du papier et en aurait amélioré la technique afin de le produire en masse .
En [MASK] , elle est souvent combinée avec l' utilisation des médicaments traditionnels .
Pendant 1300 ans , entre les années 605 et 1905 , la haute administration impériale , tant centrale que provinciale , mais également l' administration du [MASK] [MASK] sous occupation chinoise , est tenue par une caste recrutée sur la base de concours extrêmement difficiles : les examens impériaux .
Il existe en [MASK] des restaurants de quartier spécialisés dans les pâtes fraîches , faites à la main sans aucun outil de découpe .
Les pâtes alimentaires sont apparues en [MASK] bien avant d' être introduites en [MASK] , où elles auraient été rapportées par [MASK] [MASK] , lorsqu' il revient de son voyage de 17 ans en [MASK] , en 1291 .
La peinture coréenne a été influencée par l' art bouddhiste venu de [MASK] .