Cet ancêtre choisit ce nom car son lieu de naissance était dans la région allemande d' [MASK] , à [MASK] .
[MASK] [MASK] est né dans un milieu aisé ; son père , officier , a épousé quelques années auparavant une femme d' affaires entreprenante .
[MASK] [MASK] utilise alors la technique de fondation à l' air comprimé lors de l' exécution des piles tubulaires .
Or [MASK] [MASK] est l' auteur d' une étude : Le fonçage par pression hydraulique des piles concernant cette nouvelle technique .
Le succès de l' entreprise , qui doit relier la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] à la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , lui assure une première renommée .
Avant la [MASK] [MASK] , [MASK] [MASK] a contribué à la création de la [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] .
[MASK] , qui bénéficie d' une solide expérience , décide de fonder sa propre société .
Puis il se lance dans la conception de structures métalliques pour des ponts : Le talent de l' ingénieur centralien , sa vivacité à saisir toute nouvelle idée ou projet , mais aussi sa grande capacité à s' entourer de brillants collaborateurs , contribuent au succès de la société [MASK] .
[MASK] [MASK] sera d' ailleurs à l' origine en 1881 de la conception de l' armature de fer de la [MASK] [MASK] [MASK] [MASK] , dessinée par [MASK] et inaugurée à [MASK] [MASK] en 1886 .
Ainsi , en 1885 s' achève difficilement la construction en maçonnerie de l' [MASK] [MASK] [MASK] , haut de 169 mètres , et l' [MASK] [MASK] est encore dans les limbes ...
Parmi les sources d' inspiration de cette idées , il faut rappeler la [MASK] [MASK] [MASK] [MASK] de [MASK] .
Leur ébauche , mise en forme le 6 juin 1884 , s' embellit avec la collaboration de l' architecte [MASK] [MASK] , qui affine et décore l' édifice .
D' abord réticent , [MASK] [MASK] s' approprie l' idée de ses collaborateurs ( [MASK] [MASK] ) en rachetant le brevet déposé le 18 septembre 1884 .
C' est sous le label ci-dessus qu' il la propose d' abord au maire de [MASK] -- où doit bientôt se tenir une autre exposition universelle -- qui refuse , jugeant le projet " peu réaliste et surtout beaucoup trop onéreux " .
L' homme a une réputation excellente , il sait s' entourer d' hommes remarquables , comme [MASK] [MASK] et [MASK] [MASK] .
C' est un bourreau de travail , un homme respecté ( à [MASK] , il a sauvé un ouvrier de la noyade en se jetant dans le fleuve ) .
Tous les éléments sont préparés à l' usine de [MASK] puis transférés sur le site .
Le projet de construction de la [MASK] suscita d' ardentes hostilités .
Le 28 janvier 1887 , les travaux commencent et bientôt , les [MASK] assisteront , mi-hébétés mi-émerveillés , à la majestueuse élévation de l' édifice , au " rythme incroyable " de douze mètres par mois .
Sur le chantier ne s' effectue que l' assemblage des éléments de la [MASK] .
Ceux-ci sont dessinés et fabriqués dans les ateliers [MASK] , à [MASK] près de [MASK] .
[MASK] , qui n' a plus qu' une idée en tête , accepte et octroie des salaires exorbitants ( pour l' époque ) .
[MASK] , qui a respecté les délais impartis , reçoit la [MASK] [MASK] [MASK] ( distinction rare à l' époque ) .
Fort de ce succès , [MASK] s' engage aussitôt dans la construction des écluses du [MASK] [MASK] [MASK] .
En effet , le percement du canal n' avance pas et [MASK] [MASK] [MASK] abandonne l' idée d' un canal au niveau de la mer et se range à l' idée d' [MASK] de constructions de grandes écluses .
[MASK] démissionne de la société qu' il a créée trente ans auparavant .
Ce jugement est cassé par la [MASK] [MASK] [MASK] grâce à la brillante défense de son avocat , [MASK] [MASK] , qui , le mettant hors de cause , lui permet d' être réhabilité .
[MASK] , qui est plus ingénieur que financier , durement atteint par la polémique , se retire " des affaires " pour se consacrer uniquement à la pérennité de " sa [MASK] " .
La [MASK] [MASK] est passée de mode .
Il lui préfère le tout nouveau métropolitain dû à un autre ingénieur [MASK] [MASK] et surtout le trottoir roulant qui passent tous deux à proximité .
[MASK] s' acharnera désormais à en démontrer l' utilité .
Pendant la [MASK] [MASK] [MASK] , [MASK] poursuit ses recherches sur les hélices , la voilure mais aussi sur les projectiles .