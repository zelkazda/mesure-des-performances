[MASK] [MASK] , plus connu sous le pseudonyme [MASK] [MASK] , né le 3 mai 1916 à [MASK] ( [MASK] ) , mort le 24 septembre 1984 à [MASK] , est un poète français .
Après des études de lettres à l' université de [MASK] , il entama une carrière d' enseignant .
Bien que peu actif ce conseil a probablement inspiré la politique d' intervention culturelle que pratiquera le futur ministre [MASK] [MASK] .
[MASK] [MASK] fut élu à l' [MASK] [MASK] , le 25 avril 1968 , au fauteuil 4 , succédant au [MASK] [MASK] .
Toutefois , ses confrères ne prirent pas acte de cette décision et attendirent sa disparition pour procéder à son remplacement , intervenu le 18 avril 1985 avec l' élection du [MASK] [MASK] [MASK] .