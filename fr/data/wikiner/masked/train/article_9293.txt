Fils d' un serrurier aux maigres ressources , il réussit à suivre des études au lycée de [MASK] grâce à une bourse accordée par la ville de [MASK] .
Ses classes terminées , la modicité de sa fortune l' oblige à embrasser la profession de maître d' études à [MASK] , puis à [MASK] .
Ses prises de position à l' encontre des notables locaux lui coûtent son poste de directeur à [MASK] .
Un buste représentant [MASK] [MASK] est visible à [MASK] .