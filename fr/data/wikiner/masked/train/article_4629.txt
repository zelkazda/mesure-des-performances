La commune de [MASK] est située au bord de l' estuaire de la [MASK] et de la [MASK] sur la [MASK] [MASK] .
Elle est l' une des communes les plus peuplées du [MASK] [MASK] [MASK] .
[MASK] est limitrophe de [MASK] et [MASK] .
Elle est située à vol d' oiseau à 14 km de [MASK] , 22 km de [MASK] , 28 km de [MASK] et 29 km du [MASK] .
[MASK] est le centre d' une unité urbaine ( ou agglomération au sens de l' [MASK] ) avec [MASK] et [MASK] de 12498 habitants .
Le territoire de [MASK] s' étend de la cuesta de la [MASK] à l' ouest jusqu' au plateau du [MASK] [MASK] [MASK] à l' est .
L' altitude varie de 0 m au niveau de l' estuaire de la [MASK] à 135 m pour le point culminant au niveau du lieu-dit de [MASK] .
En arrivant à la mer , la [MASK] forme une vallée de déblaiement de formation glaciaire quaternaire .
Ce même déblaiement est la cause de la création d' une cuesta qui sépare la vallée du [MASK] [MASK] [MASK] .
La [MASK] sépare la commune de [MASK] en formant la limite ouest et nord où elle y forme un estuaire qui devient plus ou moins important selon les marées .
En raison de sa proximité directe à la mer , [MASK] bénéficie d' un climat océanique tempéré , des hivers doux et des étés frais grâce au vent marin qui rafraîchit les terres de la commune .
L' amplitude thermique est assez faible et les précipitations sont plutôt fortes car [MASK] est située dans une région bocagère .
Il pleut moins qu' à [MASK] sur la [MASK] [MASK] [MASK] , le climat normand est assez similaire de celui de [MASK] .
[MASK] est desservi par la ligne 20 des [MASK] [MASK] [MASK] [MASK] qui passe environ quinze fois par jour vers [MASK] ou [MASK] , [MASK] et [MASK] [MASK] .
C' est du port de [MASK] que [MASK] [MASK] [MASK] appareilla , avec sa flotte , pour [MASK] [MASK] [MASK] à la [MASK] [MASK] [MASK] , en 1066 .
Une plaque commémorative avec la liste des noms des compagnons de [MASK] [MASK] [MASK] est apposée à l' intérieur de l' église , au-dessus des portes d' entrée principales .
Grâce à la générosité de [MASK] [MASK] [MASK] , un édifice de style roman est construit , dont il reste les quatre piliers du chœur , une arcade et une voûte .
L' [MASK] [MASK] fait l' objet d' un classement au titre des monuments historiques depuis le 4 mai 1888 .
C' est le style typique du [MASK] [MASK] [MASK] .
Le nom de [MASK] [MASK] [MASK] n' était pas donné , malgré tout , au hasard .
À ce moment , le lieu était connu comme relais de la poste aux chevaux de [MASK] à [MASK] .
De cette époque , il reste des vestiges visibles sur quelques parties de bâtiment dans la cour [MASK] [MASK] .
C' est du port de [MASK] qu' en 1066 , [MASK] [MASK] [MASK] , alors dit le bâtard , est parti pour l' [MASK] .
Sa victoire à la [MASK] [MASK] [MASK] lui donnera la couronne d' [MASK] ( qui lui revenait de droit ) .