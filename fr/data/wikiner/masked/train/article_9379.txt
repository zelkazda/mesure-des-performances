[MASK] [MASK] [MASK] [MASK] est un roman d' [MASK] [MASK] , écrit avec la collaboration d' [MASK] [MASK] et achevé en 1844 .
Il a d' abord été publié en feuilleton dans [MASK] [MASK] [MASK] [MASK] du 28 août au 26 novembre 1844 ( 1 re partie ) , puis du 20 juin 1845 au 15 janvier 1846 ( 2 e partie ) .
Après quatorze années , il réussit à s' échapper et s' empare du trésor de l' [MASK] [MASK] [MASK] , dont l' emplacement lui a été révélé par un compagnon de captivité , l' [MASK] [MASK] .
Devenu riche et puissant , il entreprend , sous le nom notamment du " [MASK] [MASK] [MASK] " , de se venger de ceux qui l' ont accusé ou ont bénéficié directement de son incarcération pour s' élever dans la société .
Il aura la chance de faire la connaissance de l' [MASK] [MASK] , un autre prisonnier qui voulant s' évader a creusé un tunnel .
[MASK] [MASK] [MASK] [MASK] est une des principales œuvres issues de la collaboration entre [MASK] [MASK] et [MASK] [MASK] , avec la saga des [MASK] [MASK] .
C' est aussi le cheminement d' un homme sans histoire , [MASK] [MASK] , qui devient dans la littérature française l' archétype du vengeur .
Les deux voyageurs visitèrent ainsi l' [MASK] [MASK] [MASK] , partant de [MASK] dans un petit bateau .
Il existe donc à la fois une similarité entre les destins d' [MASK] [MASK] et de [MASK] [MASK] ( le prisonnier à vie qui s' évade et revient dans le monde comme un être puissant et impénétrable ) et une simultanéité entre la création du roman et l' avènement du [MASK] [MASK] .
[MASK] [MASK] [MASK] [MASK] et le bonapartisme : chronologie
De même il achète à [MASK] un bateau dont le prix est de quarante mille francs .
Des visites de la " cellule d' [MASK] [MASK] " sont encore organisées de nos jours au [MASK] [MASK] [MASK] , au large de [MASK] .
[MASK] [MASK] a tiré trois drames de son roman :