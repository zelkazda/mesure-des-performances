[MASK] naît à [MASK] dans le [MASK] le 20 novembre 1889 .
Il étudie les mathématiques et l' astronomie à l' [MASK] [MASK] [MASK] où il obtient son diplôme en 1910 .
[MASK] [MASK] [MASK] , le fondateur et directeur de l' [MASK] [MASK] [MASK] [MASK] , près de [MASK] en [MASK] , lui propose un poste de chercheur en 1919 .
Les observations faites avec ce télescope par [MASK] en 1923 -- 1924 permettent d' établir que les " nébuleuses " observées précédemment avec des télescopes moins puissants ne font pas partie de [MASK] [MASK] , mais constituent d' autres galaxies éloignées .
Les travaux sur la spectroscopie astronomique étaient menés avant 1918 par [MASK] [MASK] [MASK] ( [MASK] [MASK] [MASK] ) , [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] ( [MASK] [MASK] [MASK] ) .
[MASK] [MASK] a établi un système permettant de classer les galaxies .
Il est lauréat de la [MASK] [MASK] en 1939 pour ses travaux sur les nébuleuses .
Le [MASK] [MASK] [MASK] a été nommé en son honneur , ainsi que l' astéroïde [MASK] [MASK] [MASK] [MASK] .