[MASK] [MASK] est leur fils unique .
Fin 1942 , il est recruté comme maçon par le service d' entretien d' une firme nogentaise , mais presque aussitôt se trouve requis pour le [MASK] ( début 1943 ) .
[MASK] abandonne l' activité de dessinateur pour se consacrer à l' écriture , tout en se formant aux aspects techniques du journalisme ( mise en page ... ) .
En mai 1968 , [MASK] [MASK] est brièvement hospitalisé pour une crise hémorroïdaire .
Durant les années 70 , un épisode important de l' histoire de [MASK] [MASK] a été le départ de [MASK] [MASK] [MASK] .
[MASK] et lui se sont gravement brouillés au début des années 70 .
L' un comme l' autre , ainsi que leur ami [MASK] , ont laissé ce qu' il fallait comme clés pour que l' on puisse comprendre à demi-mot pourquoi : il s' agit clairement d' une affaire non pas politique , ni littéraire , mais personnelle .
Sa prise de position virulente ( en compagnie de [MASK] [MASK] [MASK] et d' autres écrivains ) et argumentée contre une réforme de l' orthographe par l' [MASK] [MASK] fut très remarquée .
Au nom de ces valeurs , [MASK] a sa vie durant mené un combat contre tout ce qu' il considérait irrationnel ou injuste , entre autres l' usage de la souffrance des animaux comme agent de distraction des humains .
Ses amis de [MASK] [MASK] y feront souvent référence de façon mi-admirative , mi-ironique ( [MASK] dans quelques dessins , [MASK] [MASK] [MASK] par quelques allusions mordantes , [MASK] en rêvant de façon poétique sur la question dans quelques-uns de ses articles ) .
Le film accuse la direction du [MASK] [MASK] actuel -- celui relancé en 1992 -- d' avoir délibérément fait l' impasse sur l' héritage du [MASK] [MASK] en cherchant à occulter sa mémoire et sa contribution à la création du journal .
En retour , [MASK] [MASK] ( directeur de la publication et de la rédaction de [MASK] [MASK] ) , [MASK] ( directeur artistique ) et [MASK] [MASK] ( critique cinéma ) critiquent sévèrement le film et dénoncent un parti-pris abusif .
[MASK] , pour sa part , défend un point de vue moins tranché dans cette polémique qui oppose [MASK] [MASK] à la direction du journal actuel .
Il estime que " ceux qui , aujourd'hui , divinisent [MASK] ne le font que pour mieux démolir ce qu' est [MASK] [MASK] aujourd'hui " , mais reconnaît , face à [MASK] , les mérites de [MASK] ( qu' il décrit comme " une intelligence -- non , pas " fulgurante " , mais fort vive -- , un esprit déroutant , alerte , s' adaptant très vite , d' une audace saisissante , d' une agilité souvent imprévisible " ) et rappelle que sans lui , " il n' y aurait pas eu d' aventure [MASK] , ni , conséquemment , de [MASK] [MASK] " .
Il fut considéré par [MASK] [MASK] comme l' un des derniers grands écrivains vivants .
" Seule la virulence de mon hétérosexualité m' a empêché à ce jour de demander [MASK] en mariage " .
[MASK] qui collabora à [MASK] [MASK] ( première mouture ) pendant la dernière année ( 1981-1982 ) admirait le talent de [MASK] qu' il comparait à un [MASK] moderne .
Selon lui [MASK] était un des derniers honnêtes hommes de ce siècle pourri et l' inventeur d' une nouvelle presse .
Dans le film [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( 2001 ) , une militante déclare que selon elle [MASK] et [MASK] [MASK] sont les deux " mythes vivants " de notre temps .