Ensuite il entre à [MASK] [MASK] [MASK] , avant d' être reçu à l' [MASK] .
[MASK] [MASK] a , dès son enfance , été élevé dans l' admiration du [MASK] [MASK] [MASK] , par son père militant gaulliste .
Il adhère ensuite à l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] en 1968 dont il est conseiller national en 1969 .
Il garde ce poste jusqu' en 1977 , date à laquelle le socialiste [MASK] [MASK] est élu à la tête de la ville , et devient alors secrétaire départemental du [MASK] .
Durant cette période il poursuit ses études , notamment à l' [MASK] dont il sort diplômé en 1978 .
Il est élu député pour la première fois , à l' âge de 30 ans puis est élu conseiller général de l' [MASK] l' année suivante dans le canton de [MASK] , canton dont il est toujours le représentant .
Cette même année il est chargé de mission pour le commerce et l' artisanat au [MASK] .
Ensuite , le 2 mars 2006 , la [MASK] [MASK] [MASK] [MASK] [MASK] a annulé son inéligibilité tout en confirmant sa sa condamnation avec sursis et son amende .
Il est battu à la législative de 1981 , mais devient alors secrétaire national du [MASK] à l' animation et devient en 1982 membre de droit du comité central et membre du conseil politique du parti .
En 1994 , après avoir été réélu pour la troisième fois consécutive à la présidence du conseil général , il est nommé secrétaire général adjoint du [MASK] avant de devenir en 1995 le secrétaire général du parti lors de l' élection de [MASK] [MASK] à la présidence .
En 1997 , la gauche remporte la majorité à l' [MASK] [MASK] , [MASK] [MASK] est alors battu , dans le cadre d' une triangulaire avec le [MASK] [MASK] par la socialiste [MASK] [MASK] .
Il démissionne alors du secrétariat général du [MASK] .
En 1998 il fait l' objet d' une exclusion du [MASK] ( par [MASK] [MASK] pour avoir accepté et encouragé des désistements réciproques avec le [MASK] [MASK] lors des élections cantonales et régionales ( à [MASK] en particulier ) .
Il est exclu du [MASK] pour avoir déclaré dans le journal [MASK] [MASK] qu' on ne pouvait continuer à ignorer l' existence des thèmes évoqués par le [MASK] [MASK] .
En 2004 , si il est lui-même réélu conseiller général du [MASK] [MASK] [MASK] , il perd sa présidence de conseil général à la suite d' une défaite de la majorité départementale à une voix près .
Il est membre de la coordination des observateurs franco-africains , présidée par [MASK] [MASK] , qui qualifiait les élections au [MASK] de juillet 2009 de " libres " et affirmait : la " démocratie fonctionne " , en dépit des observations en sens inverse de la [MASK] [MASK] .
Il est réélu conseiller général en 2008 , pour le [MASK] [MASK] [MASK] .