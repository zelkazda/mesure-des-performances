En 1956 , [MASK] [MASK] se lance dans la location de véhicules .
Cette activité débouchera sur un partenariat avec [MASK] en 1958 .
[MASK] [MASK] deviendra même en 1989 l' actionnaire principal d' [MASK] .
L' activité d' importation de deux-roues motorisés commence avec une partie de la production de [MASK] en 1975 .