Il est né le 5 janvier 1941 à [MASK] , au [MASK] .
Toutefois , [MASK] reste modeste et explique le succès de son entreprise par la chance qu' il a eu de pouvoir exploiter pleinement sa créativité .
L' œuvre de [MASK] en sera beaucoup inspirée .
C' est certainement à ce contexte que [MASK] doit cette passion pour les avions et le vol en général , thèmes omniprésents dans son œuvre .
Il est également grand amateur d' [MASK] [MASK] et exerce alors ses talents de dessinateur , d' abord en faisant des croquis d' avions en imitant son héros , mais trouve qu' il ne peut pas dessiner les personnes .
Un jour , se rendant compte qu' il ne faisait que copier le style de [MASK] , il brûla tous les manga qu' il avait dessinés ; il dit avoir réalisé que créer son propre style était très difficile .
[MASK] commence sa carrière en avril 1963 comme intervalliste au [MASK] [MASK] .
Il se fait connaître d' abord avec son travail sur [MASK] [MASK] [MASK] [MASK] ( 1965 ) ; ayant trouvé la fin du film non satisfaisante , il en propose une autre , qui est acceptée et incorporée au film .
Il perçoit un salaire de dix-neuf mille cinq cents yens ( le loyer de son petit appartement dans le quartier [MASK] lui coûte six mille yens ) .
Quand quelques troubles syndicaux éclatent en 1964 au sein du studio , [MASK] prend la tête des manifestants et devient secrétaire en chef du syndicat des travailleurs .
[MASK] [MASK] est alors le vice-président du syndicat .
La même année , [MASK] travaille avec sa femme sur [MASK] [MASK] [MASK] mais est cette fois promu animateur clé , .
En 1969 , il anime quelques plans du film [MASK] [MASK] [MASK] [MASK] , un autre long métrage , toujours en compagnie de sa femme .
La famille déménage à [MASK] dans la [MASK] [MASK] [MASK] en 1970 .
Il visite également de nombreux endroits à [MASK] pour un possible nouveau bâtiment pour le studio .
On peut par exemple citer [MASK] [MASK] [MASK] [MASK] où [MASK] travailla en tant que concepteur scénique et fit un voyage en [MASK] pour s' inspirer des paysages .
En 1975 , [MASK] voyagea également en [MASK] et en [MASK] pour préparer [MASK] .
En 1978 , [MASK] obtient chez [MASK] [MASK] l' opportunité de passer à la réalisation .
Il en résulte une série de 26 épisodes de 26 minutes chacun intitulé [MASK] , le fils du futur .
Il passe le téléphone à [MASK] , qui lui parle de [MASK] et demande pas moins de seize pages dans [MASK] .
Il deviendra plus tard producteur en chef du [MASK] [MASK] et ami inséparable de [MASK] .
La même année , sort son premier film en tant que réalisateur : [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Devenu depuis un classique , ce film représente une étape marquante dans la carrière de [MASK] .
[MASK] l' ignore complètement , refusant même d' être pris en photo .
À la même période , il réalise les épisodes 145 et 155 de la série [MASK] [MASK] et utilise [MASK] , le nom de sa société , comme pseudonyme .
En 1982 , il réalise les six premiers épisodes ( dont il signe également le scénario ) de la série [MASK] [MASK] ( finalement diffusée en 1984 et 1985 ) en coproduction avec la [MASK] italienne .
Cette série raconte les aventures d' un [MASK] [MASK] anthropomorphe .
Celui-ci décide de l' aider à les réaliser , en commençant par [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Le manga est un grand succès et est élu manga préféré des lecteurs d' [MASK] l' année suivante .
En 1983 , le projet de faire un long métrage des premiers volumes de [MASK] est lancé .
La production étant en retard , [MASK] étant très exigeant sur la qualité , on publie une petite annonce dans [MASK] pour trouver plus d' animateurs .
Le jeune [MASK] [MASK] ( mieux connu aujourd'hui pour son travail dans [MASK] [MASK] [MASK] ) répondra à l' appel et , [MASK] étant ébloui par la qualité de son travail , il sera tout de suite embauché et mis au travail sur la scène clé du film : l' arrivée du " soldat géant " .
Le succès de l' adaptation cinématographique de [MASK] lui permet en 1985 de fonder le [MASK] [MASK] ( basé au quartier [MASK] de la capitale japonaise ) , en compagnie d' [MASK] [MASK] ; ils occupaient le bâtiment lui-même depuis avril 1984 .
Le succès du film et la fondation du studio surviennent à une époque difficile pour [MASK] , sa mère étant décédée un an avant la sortie du film , en juillet 1983 , à l' âge de 71 ans .
[MASK] entend dès lors se concentrer sur les longs métrages d' animation alors que le genre est essentiellement représenté au [MASK] par les séries et OAV .
Le premier projet du studio est le long métrage [MASK] [MASK] [MASK] [MASK] [MASK] , qui sortira en août 1986 .
La consécration attendra 1988 avec la sortie de [MASK] [MASK] [MASK] ( [MASK] [MASK] [MASK] ) .
Ce fut de même lors de son second essai , en couplant [MASK] ( réalisé par [MASK] ) avec [MASK] [MASK] [MASK] [MASK] .
Ce furent deux années difficiles pour [MASK] et son équipe , devant plaire à deux maisons d' édition à la fois ( [MASK] étant lui aussi basé sur un livre , celui-ci pour enfants ) et jonglant avec deux équipes d' animateurs .
Peu après la sortie de ces films , [MASK] s' assoit devant une gare toute une journée pour observer le mouvement des jupes des passantes .
Ce qu' il vit sera incorporé dans son prochain film : en 1989 , [MASK] [MASK] [MASK] [MASK] , dont l' héroïne porte toujours une longue robe noire de sorcière , est un véritable succès .
Basé sur le roman d' [MASK] [MASK] , le film est classé premier au box-office japonais pour l' année 1989 , récoltant 2170 millions de yens et voyant 2604619 entrées en salle .
Le studio vit encore des temps difficiles pendant la production de [MASK] .
Quoique le budget pour ce film fût le double de celui de [MASK] , la qualité des images étant supérieure , les animateurs gagnaient beaucoup moins par rapport au travail effectué .
[MASK] pense à un petit plan simple pour le studio : créer un bon environnement de travail puis former et guider les jeunes animateurs ( la plupart des animateurs ont entre 18 et 25 ans ) .
Ce film se démarque de l' univers de [MASK] par plusieurs aspects , notamment de par son héros adulte et l' histoire , située dans un contexte historique et géographique réel .
Le même jour de la sortie de [MASK] [MASK] , sont inaugurés les nouveaux bureaux du [MASK] [MASK] dans la banlieue ouest de [MASK] .
En 1995 , [MASK] [MASK] [MASK] , un clip musical au budget considérable est réalisé pour la chanson du même nom du célèbre groupe j-pop [MASK] [MASK] [MASK] .
Il sera diffusé au [MASK] avec le film [MASK] [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] .
[MASK] avait alors 54 ans .
C' était peut-être sa dernière chance de réaliser un film comme [MASK] . "
[MASK] suit le conseil de son ami , et sort [MASK] [MASK] en 1997 .
Le film est annoncé à tort comme " le dernier long métrage de [MASK] " par la presse après une conférence de presse où [MASK] a dit " Je crois que c' est le dernier film que je ferai de cette manière. "
[MASK] aime beaucoup être présent à toutes les étapes de la création d' un film et vérifiant tous les dessins des animateurs , un par un , ce qu' il fit pour tous ses films .
[MASK] le fatigua pendant deux ans .
Le film est considéré comme un chef-d'œuvre de l' animation et propulse la renommée de [MASK] d' un niveau national au niveau mondial .
Il est distribué dans de nombreux pays dont [MASK] [MASK] ( en 2000 ) par [MASK] ( [MASK] ) qui demandera à l' auteur de le couper pour la diffusion internationale .
[MASK] refusera .
Un énorme succès au box-office nippon le classera premier , dépassant [MASK] [MASK] [MASK] et totalisant plus de treize millions d' entrées ( il fut par la suite dépassé par [MASK] ) .
En [MASK] on enregistrera environ 335000 entrées .
À partir de ce moment , [MASK] déclarera sa " retraite " à la fin de la réalisation de chacun de ses films , mais sans succès : devant le vide laissé par le décès de [MASK] [MASK] , le 16 janvier 1999 , [MASK] revient au [MASK] [MASK] en tant que shochō ( ce titre signifie approximativement " la tête du service " ) .
Pendant une longue période de vacances , il connaît les filles d' un ami ; l' une d' elles devient l' inspiration pour son prochain film , [MASK] [MASK] [MASK] [MASK] .
En 2001 , [MASK] termine sa réalisation et annonce , lors d' une conférence de presse , qu' il s' agit de son dernier long métrage .
Ce film sera le plus gros succès cinématographique de tous les temps au [MASK] ( surpassant [MASK] ) avec 23 millions d' entrées , et bénéficiera d' une importante reconnaissance internationale en remportant de nombreux prix .
En [MASK] , il totalisera plus de 1400000 entrées .
La même année voit l' inauguration du [MASK] [MASK] au quartier de [MASK] dans l' ouest de [MASK] .
En 2003 sort [MASK] [MASK] [MASK] [MASK] , qu' il produira pour [MASK] [MASK] , et fin 2004 , [MASK] [MASK] [MASK] sort au [MASK] .
Il relate l' histoire fantastique d' une jeune fille transformée en vieille femme et est inspiré d' un roman de [MASK] [MASK] [MASK] intitulé [MASK] [MASK] [MASK] [MASK] .
Il est distingué par un [MASK] [MASK] [MASK] pour l' ensemble de sa carrière cinématographique à la [MASK] [MASK] [MASK] de 2005 .
Mais on m' a dit qu' on a donné ce prix à des personnes qui sont encore actives , comme [MASK] , donc je l' ai accepté [ humblement ] .
En 2008 sort son dernier film d' animation : [MASK] [MASK] [MASK] [MASK] , qui raconte les aventures d' un petit garçon de cinq ans et d' une princesse poisson rouge qui voudrait devenir humaine .
[MASK] [MASK] confie que " 70 % à 80 % du film se déroule en mer " .
Le film est sorti en salles en juillet 2008 au [MASK] et est projeté lors de la [MASK] [MASK] [MASK] de 2008 pour le public européen .
Il arrive finalement le 8 avril 2009 en [MASK] .
[MASK] s' implique énormément en créant ses films , servant souvent de scénariste et de réalisateur à la fois .
Il vérifia personnellement tous les dessins de ses premiers films , mais maintenant , il délègue une partie de ce travail à d' autres membres du [MASK] [MASK] , suite à des problèmes de santé provoqués par la surcharge de travail .
[MASK] mes employés peuvent me seconder et si je peux me concentrer sur la réalisation , il y a encore plusieurs films que j' aimerais faire. "
Les films sont parfois tirés de ses mangas , comme ce fut le cas pour [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
[MASK] utilise l' animation traditionnelle ( à la main , avec pinceaux , peinture et encre ) , quoique des effets produits sur ordinateur ( peinture numérique ) sont utilisés depuis [MASK] [MASK] pour donner " une petite touche d' élégance " ( dans [MASK] , ils furent utilisés pour respecter les délais ) .
Dans une autre entrevue , [MASK] déclare : " C' est très important pour moi de retenir le bon ratio entre le travail à la main et le travail sur ordinateur .
[MASK] fait souvent référence à l' écologie , thème exploré dans plusieurs de ses films .
Dans une entrevue avec le [MASK] [MASK] il dit qu' une grande partie de la culture moderne est " légère et superficielle et fausse " , et qu' il attend , " pas complètement en plaisantant " , une ère apocalyptique où les " herbes vertes sauvages " reprendront la [MASK] , .
[MASK] dit se refuser à réaliser ses films en se laissant guider par un schéma préétabli et éprouvé ou un thème identique .
[MASK] explique que lorsqu' il imagine ses scénarios , ses personnages , ce sont spontanément des enfants .
Les films de [MASK] sont destinés à tous : ses personnages permettent à la fois l' identification du jeune public et un développement psychologique important .
Les enfants sont caractérisés par leur naïveté liée à la découverte de leur environnement , leur spontanéité , leur enthousiasme et n' ont souvent pas encore acquis la réserve des adultes ( en particulier au [MASK] ) .
Tous les âges sont représentés dans sa filmographie , allant des petites filles de [MASK] [MASK] [MASK] à l' aïeule de [MASK] .
Ce sont également des femmes qui travaillent à la forge dans [MASK] [MASK] .
Les liens filiaux présentés par [MASK] sont presque exclusivement de type mère-fille et il met souvent en scène la rupture de ce lien , un pas vers l' âge adulte et la transmission d' un patrimoine de la mère à sa fille , comme dans [MASK] [MASK] [MASK] [MASK] .
[MASK] s' inscrit dans la lignée des artistes traumatisés , obsédés par la bombe atomique .
Bien que [MASK] fut très jeune lors de cette guerre ( il avait quatre ans lors des bombes nucléaires de [MASK] et de [MASK] ) , il l' a vécue et ressentie au travers de sa mère et son entourage durant toute son enfance et sa vie d' aujourd'hui .
[MASK] [MASK] [MASK] [MASK] [MASK] , inspiré d' un épisode des [MASK] [MASK] [MASK] , renferme une pierre flottante aux pouvoirs apocalyptiques que l' armée convoite .
Dans [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , on retrouve des guerriers géants , plus grands et plus dévastateurs que tout , puisqu' en " sept jours de feu " , ils ont réduit le monde à néant .
De plus , des engins volants , machines souvent représentées , révèlent le passé de [MASK] , qui a longtemps dessiné des avions avant de s' essayer aux personnages .
Dans [MASK] [MASK] , c' est l' histoire des aviateurs des années 20-30 qui constitue le fil conducteur de la narration , et notamment celle des as du ciel , tels que le personnage principal .
[MASK] dénonce également l' inutilité de la violence et la bêtise humaine .
Certains des premiers films de [MASK] avaient des " méchants " effectivement méchants , comme dans [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] .
D' autres sont remarquables par l' absence totale de personnages " méchants " , comme dans [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] .
Ainsi , même les personnages " méchants " sont respectés : dans [MASK] [MASK] , on s' excuse auprès de la dépouille du sanglier possédé par le démon de l' avoir tué .
Les héros de [MASK] vont parfois jusqu' à sauver leurs ennemis de leurs propres méchancetés .
Dans tous les films de [MASK] , quel que soit le sujet , on trouve toujours un endroit de paix éternelle loin de la civilisation , calme , où seul le bruit du vent , des oiseaux et de l' eau vient troubler le silence .
On voit son ancien intérêt pour le marxisme dans certains de ses premiers films , notamment [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] , où les travailleurs sont décrits en termes idéalisés .
[MASK] dit avoir abandonné ses idées marxistes lors de la création du manga de [MASK] : " J' ai arrêté de voir les choses en ' classes ' parce que c' est un mensonge de dire qu' on a raison seulement parce qu' on est travailleur manuel .
Les mangas de [MASK] sont très influencés par le travail d' [MASK] [MASK] , auquel il est souvent comparé dans son pays natal .
Plus tard , il critique toutefois [MASK] en tant que créateur d' anime et dit ne pas aimer du tout son travail .
On le compare aussi à [MASK] [MASK] , l' appelant " le [MASK] japonais " , ce qu' il n' aime pas , d' autant qu' il n' apprécie guère les productions du studio américain , à l' exception des œuvres les plus anciennes , dont les [MASK] [MASK] .
Dans les premières années -- très difficiles -- de sa carrière en tant qu' animateur , il vit [MASK] [MASK] [MASK] [MASK] , un film d' animation du [MASK] [MASK] [MASK] .
On peut voir son influence dans [MASK] , le prince du soleil .
[MASK] [MASK] [MASK] [MASK] [MASK] ( 1952 , réédité en 1979 sous le titre de [MASK] [MASK] [MASK] [MASK] [MASK] ) , film d' animation classique français de [MASK] [MASK] , convainc quant à lui [MASK] qu' il est possible de faire des films d' animation pour adultes .
Il incorpore des détails de ce film dans [MASK] [MASK] [MASK] [MASK] .
[MASK] est aussi ébloui par le travail du [MASK] [MASK] [MASK] ( [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ) , en particulier son talent pour dessiner les plantes .
[MASK] [MASK] le fit déprimer parce qu' il jugeait son propre travail inférieur .
Il dit être très influencé par plusieurs écrivains occidentaux , dont [MASK] [MASK] , [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] , à qui il avoua qu' il avait ses livres sur sa table de chevet .
Il est influencé par deux écrivains français , [MASK] [MASK] [MASK] et [MASK] [MASK] ( [MASK] ) .
Quant à [MASK] , ils s' influencent réciproquement et sont amis .
La [MASK] [MASK] [MASK] a tenu une exposition sur leur travail , intitulée [MASK] et [MASK] : Deux artistes dont les dessins prennent vie , de décembre 2004 à avril 2005 ; ils étaient d' ailleurs présents lors de la cérémonie d' ouverture de l' exposition .
[MASK] a prénommé sa fille [MASK] en l' honneur de [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .