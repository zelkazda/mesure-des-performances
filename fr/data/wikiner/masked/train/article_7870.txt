Le [MASK] [MASK] [MASK] est un diocèse français créé en 1317 , il est rattaché au concordat de 1801 au [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , avant de constituer de nouveau un diocèse indépendant au concordat de 1817 ( décision qui ne effective qu' en 1821 ) .
Il a alors continué à faire partie de la province ecclésiastique de [MASK] .
Son chef-lieu a été fixé à [MASK] , où se trouve la cathédrale .
Le territoire du [MASK] [MASK] [MASK] correspond aujourd'hui à celui du département de la [MASK] et est divisé administrativement en 59 paroisses .
Le [MASK] [MASK] [MASK] ... ( à compléter )