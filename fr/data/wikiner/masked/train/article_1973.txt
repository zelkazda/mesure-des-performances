La [MASK] [MASK] [MASK] ou [MASK] est une société de prêtres catholiques fondée en [MASK] , dont les buts sont " de former des prêtres et créer des séminaires " .
Elle a son siège à [MASK] , dans le [MASK] [MASK] [MASK] ( [MASK] ) .
Fondée le 1 er novembre 1970 , cette société , dont les statuts avaient été reconnus et approuvés par l' évêque diocésain d' [MASK] en 1970 à titre de " pieuse union " , perd sa reconnaissance officielle le 6 mai 1975 .
[MASK] [MASK] et ses disciples se considèrent comme traditionalistes tandis que pour beaucoup , ainsi que l' explique [MASK] [MASK] , le mouvement est l' incarnation de l ' intégrisme .
Pour sa part , la [MASK] considère que son retrait d' approbation prononcé en 1975 est juridiquement nul car entaché d' irrégularités .
La [MASK] , tout en se réjouissant de ce premier pas , reste très méfiante envers la hiérarchie diocésaine et demande une libéralisation générale des livres de 1962 ainsi qu' un statut juridique directement soumis au pape .
M gr [MASK] critique sévèrement la visite d' une synagogue par [MASK] [MASK] , ou les rencontres inter-religieuses d' [MASK] en 1986 .
Certaines communautés de la mouvance de la [MASK] , comme les [MASK] [MASK] [MASK] , n' acceptent pas la situation de schisme provoquée par les sacres du 30 juin .
Une commission cardinalice -- la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] -- veille aux applications pratiques du motu proprio .
Au sein de la [MASK] [MASK] , ce genre d' accord paraît satisfaisant à certains .
M gr [MASK] [MASK] déclare alors que l' [MASK] [MASK] " ne faisait plus partie de la [MASK] [MASK] [MASK] " .
Ces dispositions sont saluées par [MASK] [MASK] au nom de la [MASK] [MASK] [MASK] [MASK] .
Pour lui , ce " climat favorable instauré " par [MASK] [MASK] , et cette " indéniable avancée liturgique " doivent précéder le retrait de excommunications et l' ouverture de vraies discussions doctrinales .
Cependant , l' autorité romaine , par l' intermédiaire du cardinal [MASK] [MASK] [MASK] ( préfet de la [MASK] [MASK] [MASK] [MASK] ) qui a ainsi rapporté l' analyse du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [ réf. nécessaire ] , a assuré que [ réf. nécessaire ] les quatre évêques n' étaient plus suspendus ( suspens a divinis ) .
Cette affirmation [ réf. nécessaire ] , relevant de l' autorité romaine , a par ailleurs été confirmée une lettre du [MASK] [MASK] [ réf. nécessaire ] .
Ainsi la [MASK] refuse l' aggiornamento et ce qu' elle nomme les mesures libérales du [MASK] [MASK] [MASK] , notamment :
Pour la [MASK] , la modification de la liturgie traditionnelle est emblématique des modifications apportées par le [MASK] [MASK] [MASK] .
Dès les sacres de 1988 , la [MASK] publie plusieurs articles justifiant ses choix .
À la fin des années 1990 , la [MASK] lance une campagne d' information auprès de ses fidèles .
La situation irrégulière de la [MASK] [MASK] [MASK] entraîne une remise en cause de la soumission au pouvoir pontifical .
En lien avec la doctrine catholique , ils sont très opposés au communisme que le pape [MASK] [MASK] a fermement condamné en 1937 , par l' encyclique [MASK] [MASK] .
C' est la raison pour laquelle l' [MASK] [MASK] , membre de la [MASK] jusqu' en 2004 , avait reconnu en 1991 le [MASK] [MASK] comme " le parti le moins éloigné du droit naturel " .
La [MASK] demande alors à l' évêque [MASK] [MASK] [MASK] la mise à disposition d' une église ou d' une chapelle pour célébrer la messe avec ses propres officiants selon la [MASK] [MASK] [MASK] [MASK] [MASK] , c' est-à-dire selon le missel de 1962 , en s' appuyant sur le motu proprio [MASK] [MASK] du pape [MASK] [MASK] de juillet 2007 .
Les membres de la [MASK] n' ont pas souhaité assister à ces célébrations .
Ces déclarations sont survenues au moment même où le [MASK] s' apprétait à rendre publique la levée de l' excommunication prononcée en 1988 contre lui et ses trois collègues .
Dans une lettre adressée au cardinal [MASK] [MASK] [MASK] , prélat en charge au [MASK] des négociations avec la [MASK] , [MASK] [MASK] exprime des " regrets sincères " pour les " souffrances " causées au [MASK] [MASK] [MASK] par ses propos " imprudents " , sans pour autant les rétracter .
Le [MASK] , à nouveau contraint de s' expliquer le 4 février après de vives critiques de la chancelière allemande [MASK] [MASK] , demande à [MASK] [MASK] de renier " publiquement " ses propos sur la [MASK] .
Les craintes portent entre autres sur un éventuel reniement des enseignements du [MASK] [MASK] [MASK] dont l' une des avancées réside précisément dans les nouveaux rapports créés avec le peuple juif .
Le 27 janvier 2009 , le président de la conférence des évêques suisses rappelle également que " les quatre évêques ont maintes fois déclaré , qu' eux-mêmes et la fraternité , n' acceptaient pas la déclaration du [MASK] [MASK] [MASK] [MASK] [MASK] sur les relations avec le judaïsme et les religions non-chrétiennes " .
Il fait aussi de l' acceptation du [MASK] [MASK] [MASK] et en particulier de la déclaration [MASK] [MASK] un prérequis au rétablissement de la communion et à la levée des suspensions .
Une fois les ambiguïtés levées , le voyage officiel du souverain pontife en [MASK] , quelques semaines plus tard , a lieu comme prévu .
Ce dernier est expulsé le 6 février de la branche italienne de la [MASK] " pour des motifs disciplinaires graves " et afin d ' " éviter que l' image de la [MASK] [MASK] [MASK] [MASK] ne subisse des distorsions supplémentaires " .