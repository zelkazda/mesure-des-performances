Il fait partie du groupe [MASK] ,
[MASK] [MASK] est partisan du rétablissement de la peine de mort pour les auteurs d' actes terroristes ( proposition de loi 1521 du 8 avril 2004 ) .
En juin 2007 , il est battu par le socialiste [MASK] [MASK] , qui retrouve son siège de député qu' il avait perdu en 2002 .
[MASK] [MASK] est chevalier de la [MASK] [MASK] [MASK] .