Le professeur [MASK] [MASK] , né le 2 décembre 1923 à [MASK] et mort le 14 octobre 2003 à [MASK] , est un cancérologue qui s' est fait connaître comme le défenseur des sans-abri et des " sans-papiers " .
Il est très rapidement interdit de faculté de médecine en raison des lois raciales de [MASK] .
En 1993 , le [MASK] [MASK] [MASK] annule cette décision .
Il est enterré à [MASK] , au [MASK] [MASK] [MASK] .