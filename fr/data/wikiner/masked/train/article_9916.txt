En 1985 , il est le premier joueur classé en [MASK] , place qu' il occupe toujours aujourd'hui ( 2010 ) .
Ses nombreux exploits font de lui le plus grand pongiste belge de tous les temps mais également l' un des meilleurs sportifs que le [MASK] [MASK] [MASK] ait connu .
Mais [MASK] [MASK] est autant connu pour ses victoires que pour sa sportivité .
Enfin , il participe à six olympiades ( [MASK] en 1988 , [MASK] en 1992 , [MASK] en 1996 , [MASK] en 2000 , [MASK] en 2004 et [MASK] en 2008 ) , portant même le drapeau de la délégation en 1996 et 2004 .