L ' ossau-iraty est un fromage français du [MASK] [MASK] et du [MASK] , produit sur un terroir bien délimité , au sein du département des [MASK] et une petite partie du département des [MASK] .
Le fromage d' ossau-iraty trouve son origine dans les traditions pastorales et fromagères du [MASK] [MASK] et du [MASK] .
L' élevage était l' activité primordiale , puisque ce sont les [MASK] qui apportent dans la région la pratique de l' agriculture : défrichage et culture des cérales .
Plus tard , dans le [MASK] , les éleveurs prennent l' habitude d' assembler des laits de brebis , de vache et de chèvre .
En 1904 , des fabricants de roquefort implantent des unités de production dans les [MASK] .
Les fourmes produites sont ensuite acheminées vers les caves de [MASK] pour l' affinage .
À partir des années 1970 , la demande en roquefort s' effrite et la production se recentre petit à petit sur le territoire [MASK] -- [MASK] .
La filière laitière ovine des [MASK] crée un syndicat de défense et dépose un dossier de demande de reconnaissance en AOC du fromage local , .
Traditionnellement , la brebis basque occupait une zone du [MASK] [MASK] oriental , et la béarnaise le [MASK] .
La race est reconnue par le [MASK] [MASK] [MASK] [MASK] en 1970 .
Certains sont devenus propriété de communauté de communes , de collectivités locales ou du [MASK] [MASK] [MASK] [MASK] .
La fête du fromage d' [MASK] réunit tous les ans depuis 1993 , des producteurs , des vacanciers , des amateurs et producteurs des vallées voisines ...