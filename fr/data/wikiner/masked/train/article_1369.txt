[MASK] ( marque déposée officiellement comme [MASK] , parfois aussi écrit comme [MASK] avec des petites capitales ) est le nom d' un système d' exploitation multitâche et multi-utilisateur créé en 1969 , conceptuellement ouvert et fondé sur une approche par laquelle il offre de nombreux petits outils chacun dotés d' une mission spécifique .
Il a donné naissance à une famille de systèmes , dont les plus populaires à ce jour sont [MASK] , [MASK] et [MASK] [MASK] [MASK] .
On nomme " famille [MASK] " l' ensemble de ces systèmes .
Il existe un ensemble de standards réunis sous la norme [MASK] qui vise à unifier certains aspects de leur fonctionnement .
Il réalisa ce travail sur un mini-ordinateur [MASK] de marque [MASK] animé par [MASK] [ réf. nécessaire ] et rédigea le nouveau logiciel en langage d' assemblage .
Ce nom fut par la suite contracté en [MASK] ( pour au final être déposé sous le nom [MASK] par [MASK] [MASK] [MASK] ) , cependant personne ne se souvient de qui est à l' origine de la modification du " cs " en " x " .
Un décret datant de 1956 interdisait à l' entreprise [MASK] [MASK] [MASK] , dont dépendait [MASK] [MASK] , de commercialiser autre chose que des équipements téléphoniques ou télégraphiques .
C' est la raison pour laquelle la décision fut prise en 1975 de distribuer le système [MASK] complet avec son code source dans les universités à des fins éducatives , moyennant l' acquisition d' une licence au prix très faible .
Cependant [MASK] ne fut jamais réécrit en [MASK] ; le [MASK] ne supportait pas de " types " , toutes les variables étaient de la même taille que les mots ( word ) de l' architecture , l' arithmétique sur les flottants n' était pas implémentée ; de plus , le compilateur [MASK] utilisait la technique du " threaded code " .
Début 1975 [ réf. nécessaire ] , [MASK] [MASK] passe une année comme professeur invité à son alma mater , l' [MASK] .
C' est à ce moment , qu' [MASK] fut diffusé hors des [MASK] [MASK] .
Au début de cette dernière année , [MASK] [MASK] réalise la première [MASK] [MASK] [MASK] ( [MASK] ) .
Plus tard , avec l' arrivée de nouveaux terminaux , il écrit [MASK] ( l' éditeur visuel ) , une surcouche de [MASK] .
Dès la fin de l' année 1977 [ réf. nécessaire ] , des chercheurs de l' [MASK] [MASK] [MASK] apportèrent de nombreuses améliorations au système [MASK] fourni par [MASK] [MASK] [MASK] et le distribuèrent sous le nom de [MASK] [MASK] [MASK] ( ou [MASK] ) .
Ainsi [MASK] fut par exemple le premier système [MASK] à exploiter pleinement le mécanisme de mémoire virtuelle paginée du [MASK] [MASK] .
Ils ont l' intention d' utiliser [MASK] pour leurs projets .
[MASK] [MASK] , qui vient juste de passer sa soutenance de thèse ( doctorat ) , se propose de participer .
[MASK] [MASK] l' intègre au système et ajuste les performances .
Elle est reprise plus tard par [MASK] pour le système d' exploitation [MASK] grâce à une license permissive .
Les utilisateurs ne sont pas des utilisateurs passifs mais participent activement au développement et améliorent progressivement le code original d' [MASK] [MASK] [MASK] .
Les notices de copyright dans les sources doivent être laissées intactes , et la documentation doit mentionner l' origine du code ( l' [MASK] ) .
Leurs objectifs sont alors de faire en sorte que [MASK] fonctionne sous n' importe quel matériel .
Le public cible de [MASK] est des développeurs-administrateurs de haute technicité .
Encore quelques mois plus tard , le groupe [MASK] se forme et décide lui de se focaliser sur l' architecture [MASK] .
Dès 1977 , [MASK] [MASK] [MASK] mit les sources d' [MASK] à la disposition des autres entreprises , si bien qu' un grand nombre de dérivés d' [MASK] furent développés :
En 1983 [ réf. nécessaire ] suivit la version [MASK] [MASK] .
Un projet similaire nommé [MASK] fit aussi son apparition dans les années 1980 sous la direction de [MASK] [MASK] .
En 1991 un étudiant finlandais , [MASK] [MASK] , décida de concevoir , sur le modèle de [MASK] , un système d' exploitation capable de fonctionner sur les architectures à base de processeur [MASK] [MASK] .
Le noyau , qui était alors au stade expérimental , devait être généré sur un système [MASK] .
[MASK] baptisa son système [MASK] et posta le message suivant sur le groupe de discussion comp.os.minix :
Il voulut un temps rebaptiser son système [MASK] , mais il était trop tard , [MASK] s' était déjà imposé auprès des aficionados .
[MASK] ne contient pas de code provenant d' [MASK] , mais c' est un système inspiré d' [MASK] et complètement réécrit .
D' autre part , [MASK] est un logiciel libre .
[MASK] est à l' origine de [MASK] [MASK] [MASK] , l' actuelle version du système d' exploitation d' [MASK] .
[MASK] [MASK] [MASK] est basé sur le même noyau que [MASK] , [MASK] , et [MASK] : un micro-noyau [MASK] .
Toutefois , la couche [MASK] de [MASK] [MASK] [MASK] n' est pas une personnalité du noyau [MASK] , l' intégration est plus subtile puisqu' elle s' apparente plutôt à une greffe de l' un sur l' autre .
L' incompatibilité grandissante entre les nombreuses variantes d' [MASK] proposées par les différents éditeurs pour les différentes machines a fini par porter atteinte à la popularité d' [MASK] .
De nos jours , les [MASK] [MASK] propriétaires , longtemps majoritaires dans l' industrie et l' éducation , sont de moins en moins utilisés .
Seuls quelques grands constructeurs de stations de travail et de serveurs développant des dérivés d' [MASK] subsistent en 2007 [ réf. nécessaire ] :
[MASK] a possédé quelque temps les droits d' une version d' [MASK] qui se nommait [MASK] .
La philosophie des constructeurs de stations et serveurs [MASK] a été au départ de développer un système d' exploitation pour pouvoir vendre leurs machines , en y ajoutant si possible un petit " plus " pour se démarquer de la concurrence .
C' était oublier que les parcs [MASK] sont le plus souvent hétérogènes et que toute différence d' une machine à l' autre , même créée avec la meilleure intention du monde , menace l' interopérabilité donc constitue un risque réel de contre-productivité car contraignent les informaticiens à bricoler afin d' interconnecter les systèmes .
C' est une des raisons pour lesquelles nombre de ces constructeurs proposent désormais le système [MASK] avec leurs serveurs .
Toutefois , si le noyau [MASK] est bien défini , le système [MASK] change sensiblement d' une distribution à l' autre , ce qui conduit à des dissemblances causant parfois des pertes de temps .
Ce problème se posait déjà jadis avec l' opposition entre [MASK] [MASK] [MASK] et [MASK] [MASK] , en particulier sur des gestions sensiblement différentes de l' impression et des signaux .
Le système [MASK] est multi-utilisateur et multitâche , il permet donc à un ordinateur mono ou multi-processeurs d' exécuter apparemment simultanément plusieurs programmes dans des zones protégées appartenant chacune à un utilisateur .
Le noyau d' [MASK] repose sur quatre concepts élémentaires : les fichiers , les processus , les IPC ( communications inter-processus ) , et les droits d' accès :
Le fichier est l' unité élémentaire de gestion de ressources sous [MASK] .
Un fichier sous [MASK] n' est pas typé , ce qui veut dire que le système ne connaît pas le format des données qu' il contient , et peut représenter différentes ressources telles qu' une suite de caractères stockée sur un support physique , un périphérique ( disque dur , imprimante , dérouleur à bandes , mémoire , interface réseau etc .
Le processus est l' unité élémentaire de gestion des traitements sous [MASK] .
[MASK] étant un système multitâche , il permet de partager les ressources de calcul entre les threads .
Au niveau logiciel , la politique de sécurité du système [MASK] est fondée sur le principe que chaque ressource admet un identificateur , un propriétaire et un ensemble de droits d' accès ( en lecture , en écriture , en exécution ) répartis en trois groupes : 1 ) les droits du propriétaire ; 2 ) les droits du groupe auquel appartient le propriétaire ; 3 ) les droits des autres utilisateurs .
La plupart des [MASK] [MASK] actuels proposent un modèle plus fin , celui des listes de contrôle d' accès .
Par souci d' une meilleure sécurité , certains [MASK] [MASK] permettent de mieux nuancer l' acquisition de droits supplémentaires par les utilisateurs .
Ainsi [MASK] propose-t-il les " capacités " ( capabilities ) et [MASK] [MASK] [MASK] permet d' installer des applications et d' intervenir sur la configuration du système au moyen d' un compte administrateur distinct de root ( qui est d' ailleurs désactivé par défaut ) , en ce qu' il ne peut modifier les fichiers fondamentaux du système .
Ce type de contrôle d' accès est possible sur la plupart des processeurs modernes supportant un [MASK] .
Des systèmes d' exploitation ( peu nombreux à l' heure actuelle ) , tels que [MASK] , exploitent cette possibilité .
Certains passages de cet article , ou d' une version antérieure de cet article , sont basés sur l' article [MASK] ou sur une version antérieure de cet article .