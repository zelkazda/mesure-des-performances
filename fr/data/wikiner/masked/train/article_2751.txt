[MASK] est le plus grand fournisseur de connexions de téléphones fixe et mobile au [MASK] .
Il propose également des cartes de téléphone pour des appels longue distance à partir de n' importe quel pays , y compris le [MASK] .
[MASK] est l' un des plus grands employeurs du pays .
le siège social se trouve à [MASK] .
[MASK] a été fondé en 1947 quand un groupe d' investisseurs mexicains acheta la branche mexicaine du suédois [MASK] .
En 1950 , les mêmes investisseurs mexicains achetèrent la filiale mexicaine de [MASK] [MASK] , devenant ainsi la seule compagnie de téléphone du pays .
Depuis 1972 jusqu' à sa privatisation en 1990 , [MASK] investissait peu dans les infrastructures du pays .
En 1990 , le président [MASK] [MASK] [MASK] [MASK] décida de vendre l' entreprise publique , en vue d' améliorer les infrastructures et les services .
[MASK] a été vendu à un groupe d' investisseur composé principalement par [MASK] [MASK] , [MASK] [MASK] , et [MASK] [MASK] [MASK] .
Après la privatisation , [MASK] commença à investir dans de nouvelles infrastructures modernes , en créant un réseau national utilisant la fibre optique et offrant des services dans la majeure partie du pays .
En 1991 , le gouvernement mexicain vendit les dernières actions [MASK] qu' il détenait .
Cela accéléra la formation par [MASK] d' une filiale dans ce secteur .
[MASK] débuta en tant que second loin derrière son concurant dans ce marché du téléphone mobile , mais en 1995 tout changea lors de la crise monétaire mexicaine frappa durement de nombreux mexicains .
En 2000 , [MASK] [MASK] nait de la scission des activités de téléphonie mobile de [MASK] .
De nombreuses personnes continuent à croire qu' [MASK] [MASK] fait partie de [MASK] .
[MASK] [MASK] avait décidé en 2000 d' abandonner sa participation de 7 % dans [MASK] .