L' écriture par paquets permet aux [MASK] d' être écrits partiellement en utilisant des enregistrements de taille fixe ou variable sur plusieurs sessions .
Le formatage en paquets de taille fixe ne peuvent pas être fermés au format [MASK] [MASK] mais peuvent être directement écrits et ré-écrits .
Le formatage en taille fixe réduit la capacité d' environ 20 % relativement au même médium formaté en [MASK] [MASK] ou en paquets de tailles variables .
Les systèmes d' exploitation conventionnels offrent un support pour le seul format [MASK] [MASK] , sauf si des pilotes sont chargés .
L' information au format [MASK] ne peut donc pas être utilisée ou écoutée sans fermer le fichier au format [MASK] [MASK] .
Plus généralement , [MASK] est une spécification de format d' un système de fichiers pour stocker les fichiers sur des media enregistrables , principalement des media aux conditions de ré-écriture limitées , comme :
En pratique , [MASK] est le successeur d' [MASK] [MASK] , supportant de plus grands fichiers , de plus grands disques et plus d' information sur les fichiers et répertoires ( ex .
Différentes normes [MASK] :