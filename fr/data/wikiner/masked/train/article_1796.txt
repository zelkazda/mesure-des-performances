La [MASK] est une ancienne province de [MASK] , située dans les régions [MASK] , [MASK] et [MASK] .
Mais sa situation , près des frontières de [MASK] [MASK] , favorisèrent l' émergence de [MASK] qui devint la capitale .
La situation de la plaine de [MASK] était difficile à garder .
L' habitat en [MASK] est dispersé , de type bocager , soucieux d' individualisme au sein de la communauté organisée autour de la paroisse et de la commune .
Le costume traditionnel de fête de la [MASK] , est conservé par des associations folkloriques .
À [MASK] , limite de l' [MASK] et du [MASK] , est organisée le premier mardi d' août de chaque année une foire bressane à l' ancienne où tout le monde est en costume bressan .
La [MASK] possède son propre style en termes de mobilier .
Ces pièces , autrefois délaissées au profit de meubles " modernes " , ont retrouvé de nos jours leur place , de choix , dans les salons , chambres et cuisines , autour de [MASK] ou de [MASK] .