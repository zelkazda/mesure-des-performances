La longitude est une coordonnée géographique représentée par une valeur angulaire , expression du positionnement est -- ouest d' un point sur [MASK] ( ou sur une autre planète ) .
La longitude de référence sur [MASK] est le [MASK] [MASK] [MASK] .
Tous les points de même longitude appartiennent à une ligne épousant la courbure terrestre , coupant l' équateur à angle droit et reliant le [MASK] [MASK] au [MASK] [MASK] .
Les astronomes britanniques choisirent comme méridien d' origine une ligne nord -- sud passant par l' [MASK] [MASK] [MASK] [MASK] près de [MASK] au [MASK] .
Son concurrent français , le [MASK] [MASK] [MASK] , fut définitivement abandonné en 1884 .
Celle-ci fut réalisée et même plusieurs fois améliorée , par [MASK] [MASK] , horloger autodidacte en 1734 .
Il mit en application des travaux de [MASK] [MASK] et de [MASK] [MASK] sur le ressort spiral et construisit un nouveau type de mécanisme .
Les différents modèles de chronomètre d' [MASK] sont aujourd'hui conservés à l' [MASK] [MASK] [MASK] [MASK] .