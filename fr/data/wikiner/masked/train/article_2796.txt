Ses habitants sont appelés les [MASK] .
[MASK] se situe à une centaine de kilomètres de la mer ( [MASK] [MASK] ) et à une cinquantaine de kilomètres de la montagne ( [MASK] ) .
L' [MASK] -- éloignée de cinquante kilomètres à vol d' oiseau -- est facilement accessible , via [MASK] puis [MASK] , par le [MASK] [MASK] [MASK] ( 1631 mètres ) et , via [MASK] puis [MASK] , par le [MASK] [MASK] [MASK] ( 1794 mètres ) .
[MASK] est située à 190 kilomètres de [MASK] et de [MASK] , 30 kilomètres de [MASK] et [MASK] , 25 kilomètres d ' [MASK] et 40 kilomètres d' [MASK] / [MASK] .
[MASK] est desservie par l' [MASK] [MASK] , le [MASK] [MASK] et l' autoroute [MASK] .
L' autoroute [MASK] , dont l' achèvement est prévu en 2010 , reliera [MASK] à [MASK] .
La pluviométrie est forte , de l' ordre de 1100 mm par an ( à comparer avec [MASK] , 650 mm , [MASK] , 900 mm , [MASK] , 650 mm ) .
Ce climat a permis à [MASK] de devenir , à la fin du XIX e siècle , un lieu de villégiature prisé par la bourgeoisie anglaise , russe et brésilienne .
Il entame de grands travaux pour renforcer les places fortes du [MASK] , notamment le [MASK] [MASK] [MASK] dans laquelle il s' installe finalement .
On remarque la position stratégique de [MASK] , au centre du [MASK]
[MASK] devient alors la capitale du [MASK] , en lieu et place d' [MASK] .
Le pieu ou pal , du latin palum , a aussi la même base très ancienne mais ce n' est pas sous cette signification que s' est formé le nom de [MASK] , on se comparera plutôt au col de [MASK] en [MASK] [MASK] [MASK] qui n' a rien à voir avec la ville .
En 1188 , [MASK] [MASK] [MASK] [MASK] y réunit sa cour majour , ancêtre du conseil souverain .
[MASK] devient ainsi la quatrième capitale historique du [MASK] [ réf. nécessaire ] , après [MASK] , [MASK] et [MASK] .
Les troupes de [MASK] [MASK] prennent la ville , mais [MASK] [MASK] [MASK] la reprend en 1569 .
Elle y massacre les chefs catholiques faits prisonniers à [MASK] .
En 1619 , [MASK] se révolte .
[MASK] compte une nouvelle enceinte en 1649 , puis une université en 1722 .
Le 14 octobre 1790 , elle est déclarée , après [MASK] , nouveau chef-lieu du [MASK] [MASK] [MASK] .
Ce statut lui est enlevé le 11 octobre 1795 au profit d' [MASK] , puis définitivement rendu le 5 mars 1796 .
[MASK] [MASK] [MASK] manifeste son intérêt et contribue à sauver le château , un temps devenu prison .
[MASK] [MASK] ajoute une double tour encadrant une fausse entrée , à l' ouest .
Après la [MASK] [MASK] [MASK] [MASK] devient , entre 1830 et 1914 , une des stations climatiques et sportives les plus réputées d' [MASK] occidentale .
Le succès de son ouvrage est important et [MASK] devient un lieu de villégiature prisé des [MASK] .
Le [MASK] [MASK] [MASK] , le somptueux palais d' hiver -- doté d' un palmarium -- et des hôtels de classe internationale -- le [MASK] et le [MASK] -- offrent un cadre luxueux et majestueux aux concerts et réceptions qui s' y déroulent .
Les premiers vols en ballon ont lieu à [MASK] en 1844 et les premiers vols en avion , à partir de 1909 , année au cours de laquelle les [MASK] [MASK] y ouvrent la première école d' aviation au monde .
[MASK] accueille les seuls sept constructeurs mondiaux d' avions jusqu' en 1914 et devient la capitale mondiale de l' aviation .
A partir de 1947 , durant les quatre mandats du maire [MASK] [MASK] , la ville de [MASK] connait une fort développement , la population de la ville doublant en 20 ans .
[MASK] [MASK] , maire de 1971 à 2006 , travaille dans un premier temps à l' embellissement de la ville .
Deuxième ville d' [MASK] , [MASK] est la préfecture des [MASK] et le chef-lieu de six cantons ( même si seulement cinq d' entre eux portent son nom ) :
[MASK] [MASK] meurt le 16 mai 2006 d' un cancer .
Lui succède [MASK] [MASK] , élu par le conseil municipal le 30 mai .
Entre-temps , l' intérim a été assuré par [MASK] [MASK] , première adjointe et députée de la première circonscription des [MASK] .
La ville de [MASK] fait partie de quatre structures intercommunales :
La ville de [MASK] est jumelée avec les villes suivantes :
La population de [MASK] intra-muros s' élève au recensement de 2006 à 83 903 habitants .
La communauté d' agglomération de [MASK] [MASK] compte environ 141 000 habitants .
Les villes de [MASK] , [MASK] et [MASK] sont les 3 premières villes de l' agglomération après [MASK] ( elles comptent environ 35 000 habitants à elles trois ) .