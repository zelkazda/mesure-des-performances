Le [MASK] possède un indicateur de développement humain élevé par rapport à son niveau de développement économique .
De nombreuses personnes originaires du [MASK] ont émigré à l' étranger , en grande partie dans les pays du golfe .
Les [MASK] faisaient déjà du commerce avec le [MASK] .
Le [MASK] consiste en une étroite bande de terre le long de la côte sud ouest de l' [MASK] dont la largeur varie de 35 à 120 km .
Il est bordé par la [MASK] [MASK] [MASK] à l' ouest et par les [MASK] [MASK] à l' est .
Le district de [MASK] , une partie du territoire de [MASK] , est enclavé dans le [MASK] .
Le [MASK] est divisé en trois zones distinctes :
Le climat du [MASK] est tropical avec un régime de mousson du sud-ouest de juin à septembre .
Les précipitations sont abondantes , 3107 mm/an en moyenne ( [MASK] : 1197 mm/an ) et il pleut entre 120 et 140 jours par an .
L' économie du [MASK] est principalement agricole , secteur qui emploie 17 % de la population active .
Les épices généralement cultivées au [MASK] sont le poivre ( 96 % de la production nationale ) , la cardamome , la vanille , la cannelle et la noix de muscade .
La pêche en mer ou dans les [MASK] , et ses industries de transformation , sont aussi des activités importantes ( crevettes , palourdes , homards et huitres ) .
Le niveau de vie au [MASK] est relativement élevé .
Le [MASK] envoie 19 députés à la [MASK] [MASK] .
Avec 31 millions d' habitants pour 38815 km 2 , le [MASK] a une densité de population de 819 habitants au km ² .
Le taux de natalité , 17.2 ‰ , est un des plus bas de l' [MASK] ( 25.4 ‰ ) .
Ces chiffres ont permis au [MASK] de limiter sa croissance démographique à 9,4 % sur la période 1991-2001 , alors qu' elle est de 21,3 % pour l' ensemble du pays .
L' espérance de vie , 73 ans en moyenne , y est également l' une des plus élevées de l' [MASK] [MASK] ( 62 ans ) .
Cet état a le plus fort taux d' alphabétisation en [MASK] avec 91 % contre 64 % en moyenne pour l' ensemble du pays selon le recensement de 2001 .
Au début des années 2000 , le [MASK] comptait 56,1 % d' hindous ( 60 % en 1987 ) , 19 % de chrétiens et 24 % de musulmans .
Contrairement à ce qui s' est passé en [MASK] [MASK] [MASK] , l' arrivée au [MASK] de populations musulmanes ne s' est pas faite par des conquêtes militaires , mais par l' apport progressif de commerçants .
Cette arrivée progressive , et plus ou moins pacifique , des musulmans et des chrétiens ajoutée à l' activité maritime séculaire qui implique le brassage de population , expliquent sans doute la cohabitation plutôt harmonieuse des différentes communautés religieuses qui prévaut au [MASK] jusqu' à présent .
Celle-ci , qui comptait 2500 membres en 1945 , se réduit désormais à une vingtaine d' individus suite à une émigration massive vers [MASK] .
La culture keralaise , création riche et originale , tire ses principales influences du [MASK] [MASK] et du [MASK] voisins ainsi que d' apports extérieurs ( musulman et occidental ) .
La musique du [MASK] est une musique carnatique .
Le kudiyattam , l' un des théâtres sacrés les plus anciens de l' [MASK] , est né au [MASK] .
Le théâtre d' ombres demeure au [MASK] dans la plus pure tradition .
Seuls le [MASK] et l' [MASK] ont conservé des silhouettes opaques : ailleurs , elles laissent passer la lumière et la colorent comme des vitraux .
Le pooram est une fête hindoue dont la plus renommée est celle de [MASK] .
La variété des paysages , le climat , la richesse du patrimoine et la beauté de certains sites font du [MASK] une destination touristique recherchée .
Des courses de snakes boats y sont organisées lors des fêtes d' [MASK] en août ou septembre : des bateaux en bois de 30 mètres de long dans lesquels une centaine de rameurs prennent place , s' affrontent dans un spectacle impressionnant qui attirent les foules .
Le tourisme a pris son essor au [MASK] dans les années 1980 .
En 2005 , le [MASK] a accueilli près de 7 millions de touristes dont 350000 étrangers , essentiellement de novembre à mars .