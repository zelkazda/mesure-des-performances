[MASK] était une sonde spatiale américaine , conçue par la [MASK] pour l' étude de la planète [MASK] et de ses lunes , lancée le 18 octobre 1989 à partir de la navette spatiale [MASK] .
La sonde est arrivée au voisinage de [MASK] en 1995 après un voyage de 6 ans , en s' aidant de la gravitation assistée ( voir plus loin ) , ce qui permit dès le départ d' optimiser sa masse utile , d' autant plus qu' elle fut lancée à partir d' une navette et non du sol .
La quantité de carburant ainsi emportée lui permit d' orbiter ensuite 35 fois autour de [MASK] sur 8 ans , beaucoup plus que les 11 orbites prévues à l' origine sur 2 ans , parcourant ainsi 4,6 milliards de kilomètres depuis son départ de la [MASK] .
[MASK] a permis de prendre 14 000 images , elle a aussi réussi à révéler la présence d' un océan d' eau sous la surface gelée d' [MASK] , une des lunes galiléennes de [MASK] .
D' autres sondes sont déjà passées par [MASK] , par exemple [MASK] [MASK] et [MASK] [MASK] , mais [MASK] s' y est intéressé de façon beaucoup plus approfondie .
[MASK] est en effet un des endroits les plus prometteurs pour abriter de la vie extraterrestre dans son grand océan salé sous-glacien .
Le lancement de [MASK] , d' un poids au départ de plus de 2,2 tonnes , a été retardé de manière importante suite au gel des vols de navettes après l' accident de [MASK] .
De nouveaux protocoles de sécurité imposèrent à [MASK] d' utiliser un étage de propulsion supérieur de faible puissance , ce qui obligea la sonde à utiliser de plus nombreuses accélérations gravitationnelles pour obtenir une vitesse suffisante afin d' atteindre [MASK] .
Pendant son voyage , [MASK] observa de manière rapprochée les astéroïdes [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] , et découvrit une lune de [MASK] , [MASK] .
En 1995 , [MASK] lâcha un module qui s' enfonça dans l' atmosphère de [MASK] .
Cela a servi à étudier la composition de l' atmosphère de [MASK] et autre .
En 1994 , [MASK] était parfaitement positionnée pour observer la [MASK] [MASK] [MASK] s' écraser sur [MASK] .
La mission principale de [MASK] était une étude de 2 ans du système jovien .
[MASK] utilisait une orbite allongée d' environ deux mois , qui lui permettait en voyageant à une distance variable de la planète d' étudier les différentes parties de la magnétosphère .
Une fois la mission principale de [MASK] terminée et au vu des réserves de propergols encore disponibles à bord , une mission étendue débuta le 7 décembre 1997 .
La sonde fit alors une série de survols rapprochés de [MASK] et de [MASK] , deux satellites de la planète .
La réussite spectaculaire de la mission [MASK] au sein du système jovien ( 1995-2003 ) préfigura celle , tout aussi remarquable , de la mission [MASK] autour de [MASK] ( 2004 -2008 ) qui , elle aussi , ayant largement surpassée les objectifs initiaux , fera l' objet d' une prolongation à partir du 1er juillet 2008 .
Comme tous les équipements spatiaux , la [MASK] [MASK] était dotée de moyens de télécommunications hertziens avec la [MASK] :
Les liaisons de transmissions de données étaient destinées à envoyer vers la [MASK] les résultats des analyses et des photographies réalisées par les instruments de la sonde .
Les liaisons de télémétrie permettaient aux techniciens du [MASK] de modifier l' orientation de la sonde par rapport à la [MASK] pour maintenir les liaisons hertziennes et également pour effectuer des missions de maintenance des équipements électroniques de la sonde .