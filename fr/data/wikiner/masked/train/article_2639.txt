Dans le [MASK] [MASK] [MASK] de l' [MASK] [MASK] un passage est considéré , par les orthodoxes , comme une condamnation relativement explicite de l' homosexualité .
Dans le contexte du [MASK] qui est une sorte de code juridique , le sens du mot " abomination " se précise .
Ce terme , en effet , désigne fréquemment dans ce livre ( ainsi que dans le [MASK] ) :
L' épisode concernant [MASK] à [MASK] ( [MASK] 18 -- 19 ) est souvent cité comme une condamnation de l' homosexualité par la [MASK] .
Tous les hommes de la ville entourent la maison de [MASK] en demandant qu' il leur livre les deux étrangers pour qu' ils les " connaissent " ( [MASK] 19:5 ) .
Leur argumentation porte principalement , primo sur le fait que le péché de [MASK] était très clairement sexuel : d' un côté l' utilisation du verbe " connaître " qui en tout état de cause n' indique pas un manque d' hospitalité , mais plutôt un intérêt éventuellement sexuel , et de l' autre côté la proposition de [MASK] de donner ses filles vierges en échange des étrangers visiteurs , qui est une claire indication que l' intérêt des habitants était sexuel .
De nouveau , le fait que [MASK] aie proposé ses filles en échange joue de toutes les façons en faveur d' une accusation d' homosexualité ( ç' eurent été des femmes , qu' elles étaient de toute façon en mesure de coucher avec des hommes aussi bien qu' avec des femmes ) .
Ils privilégient donc une compréhension très générique ( englobant les rites cultuels et les nourritures et habitudes impures ) de ce qu' est l' abomination dans [MASK] [MASK] , en assimilant l' homosexualité à tous les types d' abomination presque sans nuance ; mais est aussi privilégié le sens commun de la signification du terme " abomination " , ce qui est assez fréquent chez les fondamentalistes protestants .
Si tous les conservateurs , modérés y compris , maintiennent l' accent clairement réprobateur de ces passages du [MASK] , seuls les modérés se rapprochent de l' analyse littéraire historique des libéraux en étant peu favorables sur la pertinence de ce texte sur la question de l' homosexualité , vue de façon plus large .
On trouve dans le [MASK] [MASK] [MASK] un épisode peu connu , très semblable à celui de [MASK] à [MASK] .
C' est le cas pour [MASK] , fils du roi [MASK] et ami intime de [MASK] , alors courtisan , et futur roi illustre dans la [MASK] .
[MASK] fait par ailleurs preuve d' un grand attrait constant pour [MASK] .
La " peine " de [MASK] est en fait liée à la jalousie de [MASK] face à la popularité et la gloire grandissante de [MASK] qui les opposera jusqu' à la mort de [MASK] , auquel [MASK] succèdera .
Dans l' [MASK] [MASK] [MASK] ( 1:26-31 ) , [MASK] fait une première condamnation de l' homosexualité .
Dans la [MASK] [MASK] [MASK] [MASK] , [MASK] met les homosexuels ( dit efféminés ) dans une liste hétérogène d' individus qui n' iront pas au paradis :
La ville de [MASK] était réputée dans l' [MASK] [MASK] pour la démesure de son immoralité [ réf. nécessaire ] .
[MASK] hausse le ton dans cette épître et fait comprendre qu' un chrétien ne doit pas jouer avec les dérèglements mentionnés .
Ce texte de [MASK] le met au même niveau que l' ivrognerie , les adultères , les voleurs , les idolâtres ...
De même , dans la [MASK] [MASK] [MASK] [MASK] , [MASK] stigmatise les homosexuels ( abominables , contre-nature ou qui abusent d' eux-mêmes avec des hommes selon la traduction ) .
[MASK] [MASK] [MASK] [MASK]
Cette aversion du christianisme pour cette pratique est justifiée dans le christianisme en référence aux péchés qu' il impute aux villes de [MASK] [MASK] [MASK] .
En juillet 1992 , le [MASK] envoie une lettre aux évêques américains , signée par le [MASK] [MASK] , pour s' opposer aux changements dans les lois et les droits civils .
Sous l' autorité de [MASK] [MASK] , en novembre 2005 dans un document , le [MASK] recommande de refuser l' ordination aux séminaristes ( personnes voulant devenir prêtres ) qui ont des pratiques homosexuelles , mais aussi à ceux qui présentent " des tendances homosexuelles profondément enracinées " et manifestée en acte , ou qui , simplement , soutiennent " la culture gay " , quand ces actes restent trop récents pour que la crainte de désordres manifestes ne puisse être exclue .
Certaines églises , comme la [MASK] [MASK] , acceptent le mariage homosexuel .
[MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] déclare qu' elle souhaite la bienvenue officiellement à ses membres gays et lesbiens , dans certaines conditions .