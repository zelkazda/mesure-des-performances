[MASK] [MASK] [MASK] ( né le 16 septembre 1888 à [MASK] , [MASK] -- mort le 3 juin 1964 à [MASK] ) est un écrivain finlandais , romancier et nouvelliste , adepte du néo-réalisme psychologique .
Ses personnages sont issus du petit peuple de la [MASK] rurale : servantes , métayers ou petits propriétaires .
Il a reçu le [MASK] [MASK] [MASK] [MASK] en 1939 .