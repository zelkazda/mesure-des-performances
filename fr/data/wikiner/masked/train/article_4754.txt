Dans une ville assiégée par les [MASK] , une pièce de théâtre conte les aventures du fameux [MASK] [MASK] [MASK] .
Alors qu' il se trouve dans le palais du sultan à déguster un vin délicieux , [MASK] déclare à son hôte qu' il connaît un vin meilleur .
Si [MASK] réussit ( c' est-à-dire si le vin est là et qu' il est effectivement meilleur que celui du sultan ) , il pourra prendre dans le trésor tout ce qu' un homme peut porter .
Dans la ville assiégée , [MASK] décide d' aller chercher ses compagnons , dont il a été séparé il y a quelques années , afin de faire lever le siège .
[MASK] séduit [MASK] , et celui-ci l' embrasse .
[MASK] , rasséréné , met le cap , dans la barque qu' ils ont récupéré , sur la ville et ses assiégeants .
Cette fois-ci , elle s' empare de l' âme de [MASK] ; celui-ci est enterré avec les honneurs , et un monument est construit sur sa tombe .
On se retrouve par la suite dans le théâtre car l' histoire présentée n' était qu' un récit de [MASK] aux membres de la troupe de théâtre , ainsi qu' à ceux des spectateurs qui sont restés l' écouter .
Pourtant , sortant du théâtre , [MASK] fait ouvrir en grand les portes de la ville : l' armée ennemie a disparu .
[MASK] repart sur son cheval , laissant ses admirateurs derrière lui .