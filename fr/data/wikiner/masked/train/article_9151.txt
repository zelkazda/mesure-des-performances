Arrivé à 25 ans en [MASK] , [MASK] [MASK] ( 1658 -- 16 octobre 1730 ) change d' identité et devient le sieur [MASK] [MASK] [MASK] .
Après avoir été gouverneur de la [MASK] , il rentre en [MASK] où il est nommé gouverneur de [MASK] .
Son nom est donné à la célèbre marque automobile américaine en 1902 , à la suite des commémorations du bicentenaire de la fondation de [MASK] .
Pour faire valoir ses droits sur la succession de son père en 1718 , il reconnaît avoir changé d' identité en s' installant en [MASK] et il retrouve sa véritable identité d' [MASK] [MASK] ( ce changement d' identité ne lui a jamais été reproché par ses contemporains ) .
En revanche , les raisons de ce changement d' identité et de son départ en [MASK] restent encore ignorées .
[MASK] [MASK] naît le 5 mars 1658 à [MASK] , dans cette partie de la [MASK] au nord de [MASK] qui deviendra le département de [MASK] à la [MASK] [MASK] .
Aucun document ne permet de connaître la jeunesse d' [MASK] [MASK] .
Quoi qu' il en soit , à 25 ans , il semble qu' il se commette dans une histoire assez louche pour être obligé de quitter la [MASK] et de se forger une nouvelle identité .
Quatre hypothèses peuvent expliquer ce départ soudain : Il est certain qu' [MASK] [MASK] effectue la traversée par des voies détournées , aucune liste officielle d' embarquement maritime n' indiquant sa présence sur un navire en partance d' un port français .
Il prend alors le titre d' écuyer qui correspond au rang que peut avoir le cadet de la famille , puis le titre de sieur de [MASK] , conformément à la coutume gasconne qui veut que le cadet prenne la succession de l' aîné à son décès .
Il se forge ainsi une identité et une origine noble , tout en se préservant d' une éventuelle reconnaissance par quelqu' un qui l' aurait connu en [MASK] .
En 1689 , il est envoyé en expédition près de [MASK] .
[MASK] se retrouve en 1690 à [MASK] .
A son retour à [MASK] [MASK] , il apprend que l' amiral anglais [MASK] [MASK] s' est emparé de la ville et a fait prisonnier sa femme , sa fille et son fils .
En 1691 , [MASK] rapatrie sa famille à [MASK] , mais leur navire est attaqué par un corsaire de [MASK] qui s' empare de tous leurs biens .
[MASK] est promu lieutenant en 1692 .
[MASK] donne une procuration à son épouse pour qu' elle puisse signer les contrats d' affaires et les actes notariés pendant son absence .
En 1695 , [MASK] part explorer la région des [MASK] [MASK] et en dresse des cartes .
Il découvre alors le détroit reliant le [MASK] [MASK] et le [MASK] [MASK] et imagine y installer un nouveau fort pour rivaliser avec les anglais .
En 1696 , pour pallier les difficultés du commerce des fourrures , le roi ordonne la fermeture de tous les comptoirs de traite , dont [MASK] .
[MASK] rentre à [MASK] .
Mais les notables canadiens s' opposent fortement à son projet de nouveau poste qui , selon eux , entraînerait la ruine de [MASK] et de [MASK] .
Ce n' est qu' en 1699 qu' il obtient le soutien de [MASK] pour la fondation du nouveau poste que le roi autorise en 1700 , en en confiant le commandement à [MASK] .
En 1702 , [MASK] retourne à [MASK] pour solliciter le monopole du commerce des fourrures et le transferts des tribus amérindiennes vers le détroit .
Un incendie ravage le [MASK] [MASK] en 1703 .
[MASK] est rappelé à [MASK] en 1704 pour répondre aux accusations de trafic d' alcool et de fourrures .
Ce dernier établit un véritable réquisitoire contre [MASK] en 1708 .
En 1709 , les troupes stationnées au détroit reçoivent l' ordre de regagner [MASK] .
En 1710 , le roi nomme [MASK] gouverneur de [MASK] et lui ordonne de rejoindre son poste immédiatement par la vallée du [MASK] .
[MASK] n' obtempère pas .
Il procède à l' inventaire général du détroit puis , en 1711 , s' embarque avec sa famille pour la [MASK] .
À [MASK] , en 1712 , il convainc le financier toulousain [MASK] [MASK] d' investir en [MASK] .
En juin 1713 , la famille [MASK] arrive au fort [MASK] , en [MASK] , après une éprouvante traversée .
En 1714 , [MASK] préconise la construction de postes le long du [MASK] alors que [MASK] désire fortifier l' embouchure du fleuve et développer le commerce avec les colonies espagnoles voisines .
En 1715 , [MASK] et son fils [MASK] prospecte l' [MASK] où ils découvrent une mine de cuivre .
La famille [MASK] rentre en [MASK] et , en 1717 , s' installe à [MASK] [MASK] .
Ils sont libérés en 1718 et [MASK] reçoit la [MASK] [MASK] [MASK] en récompense de ses trente années de loyaux services .
Il effectue également de nombreux voyages à [MASK] pour faire reconnaître ses droits sur la concession du détroit .
Il prolonge ses séjours à [MASK] si bien qu' en 1721 , il donne à nouveau procuration générale à sa femme pour qu' elle puisse signer les actes notariés .
[MASK] [MASK] [MASK] meurt le 16 octobre 1730 à [MASK] , " vers la minuit " , à l' âge de 72 ans .
Il est enseveli dans une chapelle de l' église des [MASK] [MASK] .
Les prévisions d' [MASK] [MASK] [MASK] se concrétisent après son départ de la [MASK] .
Ainsi , [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] fonde [MASK] [MASK] à l' embouchure du [MASK] en 1718 .