La [MASK] [MASK] [MASK] [MASK] qui s' ouvre au commerce international et aux investissements étrangers .
Les années 1990 sont marquées par la démocratisation d' [MASK] et l' engouement pour les " nouvelles technologies " avec le boom des start ups .
Également appelé groove jazz , l' acid jazz est un style musical qui combine des influences de jazz avec des éléments issus de la soul music , du funk , du disco et du hip-hop et ayant pour porte-drapeau des artistes comme [MASK] .
Ce style musical fait son apparition à [MASK] .
On peut citer [MASK] , [MASK] .
Seuls les poids lourds du genre que sont [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] tirent leur épingle du jeu sur la scène internationale .
Quant à [MASK] , il s' oriente vers un son résolument plus rock .
[MASK] et les évolutions de sa [MASK] auront contribué également à ce succès .