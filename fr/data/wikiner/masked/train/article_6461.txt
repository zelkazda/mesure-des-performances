[MASK] [MASK] , né [MASK] [MASK] le 22 juin 1959 à [MASK] , dans les [MASK] ( [MASK] ) et mort le 27 février 1999 à [MASK] , est un guitariste français , connu pour avoir joué entre 1982 et 1999 avec le groupe [MASK] .
Il est le frère jumeau du membre fondateur de celui-ci , [MASK] [MASK] .
C' est en 1982 qu' il rejoint officiellement le groupe de son frère jumeau [MASK] [MASK] après avoir participé à la vie du groupe dans l' ombre ( il s' occupait des instruments et de leur programmation à l' arrière de la scène ) .
Il a également composé la musique du film [MASK] [MASK] en 1987 et avait un projet d' album solo resté à jamais inabouti .
Il est enterré au [MASK] [MASK] [MASK] [MASK] .
Ce qui inspirera l' idée de la tournée acoustique [MASK] [MASK] , clôturée par la sortie de l' album éponyme , en 2001 .