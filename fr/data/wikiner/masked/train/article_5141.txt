Le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , plus connu sous le sigle [MASK] , est le plus grand organisme public français de recherche scientifique .
Classé comme établissement public à caractère scientifique et technologique , il est placé sous la tutelle administrative du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Le [MASK] fut réorganisé après la [MASK] [MASK] [MASK] et s' orienta alors nettement vers la recherche fondamentale .
Le [MASK] exerce son activité dans tous les domaines de la connaissance à travers 1100 unités de recherche et de service labellisés dont la plupart sont gérées avec d' autres structures pour quatre ans sous la forme administrative d ' " unités mixtes de recherche " .
Il s' agit de laboratoires universitaires , soutenus par le [MASK] , grâce à ses moyens humains et financiers .
En 1982 , la loi du 15 juillet , dite loi [MASK] de programmation des moyens de la recherche publique , décrète que les personnels chercheurs , ingénieurs techniciens et administratifs passent sous le régime de la fonction publique : ils deviennent fonctionnaires , avec , pour les chercheurs , un statut semblable à celui des maîtres de conférences et des professeurs des universités .
D' après le décret portant organisation et fonctionnement du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , le [MASK] a pour missions :
Pour l' accomplissement de ces missions , le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] peut notamment :
On peut distinguer trois rôles fondamentaux du [MASK] dans la recherche :
Ce triple rôle contribue à la difficulté de définir la part du [MASK] dans la recherche en [MASK] .
En pratique , un chercheur du [MASK] travaille très souvent dans un laboratoire d' une université , n' importe où en [MASK] : ceci conduit généralement à une complication et un manque de lisibilité des affiliations dans les publications des chercheurs français .
Il faut aussi distinguer la recherche financée par le [MASK] , et celle des chercheurs du [MASK] .
Enfin , du fait en particulier de l' intégration du [MASK] et de la recherche universitaire , les résultats de la recherche seront souvent le fruit d' une collaboration entre chercheurs du [MASK] et d' autres organismes , ou universitaires .
Ces dernières années , la politique suivie a été d' augmenter la part des associations entre le [MASK] et les universités , ce qui a contribué à accroître la confusion des rôles et a entraîné une certaine pression corporatiste de la part des professeurs d' université .
L' habilitation à diriger des recherches , délivrée par les universités , tend à devenir un point de passage obligé dans la promotion des chercheurs du [MASK] .
Le [MASK] comporte dix instituts dont deux nationaux :
Le [MASK] compte également 19 délégations régionales qui assurent des missions de représentation au sein des diverses instances locales impliquées dans la recherche et l' enseignement supérieur , de gestion de proximité des laboratoires et des personnels et d' accompagnement des projets scientifiques locaux .
C' est l' instance du [MASK] chargée de l' évaluation de la recherche scientifique des unités de recherche financées par le [MASK] , ainsi que , individuellement , de chaque chercheur rémunéré par le [MASK] .
Le [MASK] possède 98 laboratoires de recherche , dits unités propres de recherche ou unités de service et de recherche .
Parmi ces unités figure l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , spécialisé dans la conservation et la diffusion de publications scientifiques , y compris via internet .
Certaines structures dépendent aussi de l' un des deux instituts du [MASK] :
Dans le cadre de la réforme du [MASK] , les postes de président et de directeur général sont fusionnés .
De nombreux chercheurs ayant reçu des prix internationaux ont été au cours de leur carrière membres du [MASK] ou bien ont travaillé dans un laboratoire associé au [MASK] .
Peu d' entre eux ont cependant été durablement membres du [MASK] , en effet , avant 1982 , celui-ci n' accordait que des emplois non fonctionnaire , et une évolution de carrière comme professeur des universités était la norme .
Par ailleurs , travailler dans un laboratoire associé au [MASK] ne signifie pas appartenir au [MASK] .
Depuis 1954 , le [MASK] décerne chaque année trois types de médailles à des chercheurs travaillant en [MASK] :
D' après le bilan social 2004 publié par la direction des ressources humaines du [MASK] , les effectifs des personnels du [MASK] en 2004 étaient de :
Les agents du [MASK] sont aussi divisés en corps : Les diplômes indiqués sont ceux exigés lors de l' inscription aux concours externes .
En décembre 2005 , sur un ensemble de 26133 personnes , le [MASK] comptait 11095 femmes et 15038 hommes , soit une proportion de 42,5 % .
En 2004 , 70 000 précaires ont été rémunérées par le [MASK] sur des postes non permanents
Le budget de la politique sociale du [MASK] était de :
Il y a au [MASK] 28 médecins de prévention auxquels on ajoute 25 médecins du travail interentreprises et 33 médecins de prévention de l' université ; cela fait donc un total de 86 médecins pour 26 000 agents permanents .
Bien qu' à la pointe de la recherche mondiale , le [MASK] est régulièrement la cible de critiques émanant notamment de certains milieux économiques et spécialistes de gestion publique .
La loi dite [MASK] de 1982 fonctionnarisant le personnel du [MASK] , eut ses partisans et ses adversaires :
Ainsi , l' auteur notait que le [MASK] emploie onze mille chercheurs environ , mais ne parvient à en licencier qu' un ou deux chaque année et que , souvent , ils sont annulés par le tribunal administratif ( les chercheurs du [MASK] , étant fonctionnaires , ne dépendent pas de la juridiction des prud'hommes ) .
Ces polémiques se sont ensuite poursuivies dans un contexte de fronde de l' ensemble de la recherche publique contre le gouvernement de [MASK] [MASK] , accusé de coupes importantes dans les crédits de la recherche .
Or le syndicat [MASK] s' oppose à la généralisation de la bibliométrie .
Outre le caractère collectif de toute recherche ( voir les règles du [MASK] régissant les personnes ayant droit de signer tel ou tel article , les expériences du [MASK] engageant des centaines d' individus ) , il considère en effet improbable la tentative soi-disante " scientifique " de donner une note aux chercheurs afin d' évaluer leurs compétences de chercheurs sur une échelle numérique ( en fonction , par exemple , du facteur-h qui corrèle nombre de publications dans certaines revues scientifiques et nombre de citations , considéré par ses promoteurs comme mesure légitime de la productivité d' un chercheur ) .
Elle ne prend pas en compte les dimensions officiellement mises en avant par la commission européenne , le ministère français de la recherche et la direction du [MASK] elle-même , à savoir la dissémination , la formation , et la communication de leur savoir par les chercheurs , qui sont plus difficilement quantifiables .
En premier lieu , le contrat d' action pluriannuel n' était pas accompagné par une programmation des moyens financiers du [MASK] .