[MASK] [MASK] est un groupe de rock britannique créé durant l' été 1977 par [MASK] [MASK] ( guitare et chant ) , [MASK] [MASK] ( guitare ) , [MASK] [MASK] ( guitare basse ) et [MASK] [MASK] ( batterie ) .
Considéré par certains comme l' un des plus grands groupes de rock des 30 dernières années , [MASK] [MASK] a vendu plus de 120 millions d' albums depuis ses débuts .
Dans leur petit studio , [MASK] [MASK] va composer en 1977 le titre qui restera comme l' hymne du groupe : [MASK] [MASK] [MASK] , un hommage à tous les musiciens de bar et cabaret .
Le premier album , [MASK] [MASK] , enregistré à [MASK] en février 1978 pour seulement 12500 livres sterling , remporte rapidement un énorme succès dans toute l' [MASK] -- à l' exception notable du [MASK] , qui ne viendra au groupe que plus tard .
[MASK] [MASK] , qui signe tous les titres , s' impose comme l' unique compositeur du groupe .
Influencé par le blues et la country , notamment par [MASK] [MASK] et [MASK] [MASK] , [MASK] [MASK] des [MASK] .
[MASK] ne joue qu' en fingerpicking , en son clair .
Le succès ne se dément pas avec le second album du groupe , [MASK] , enregistré dans la foulée du premier , en décembre 1978 , à [MASK] , [MASK] .
À la fin des années 1970 , à l' époque du punk , du funk et de la disco , le succès d' un groupe qui pratique un rock influencé par [MASK] [MASK] et [MASK] peut surprendre , mais il ne diminuera pas pour autant avec les prochains albums , [MASK] [MASK] en 1980 et [MASK] [MASK] [MASK] en 1982 .
[MASK] [MASK] [MASK] dispose d' une particularité , à l' origine il était prévu pour six morceaux , or il s' avère que dans l' album final , cinq sont disponibles .
Des membres de [MASK] [MASK] participèrent musicalement aux albums de [MASK] [MASK] dans les années 1980 .
C' est aussi la fin des premières années , avec le départ du guitariste [MASK] [MASK] et du batteur [MASK] [MASK] .
Ils sont remplacés par [MASK] [MASK] ( guitare ) et [MASK] [MASK] ( batterie ) , et l' effectif est renforcé par [MASK] [MASK] aux claviers .
Il sera suivi en 1984 par le double live [MASK] qui remporte un énorme succès .
En 1985 , le groupe sort [MASK] [MASK] [MASK] , qui restera comme son plus grand succès .
C' est aussi le premier album dont les claviers ont été séquencés et arrangés sur un ordinateur ( [MASK] ) .
Le morceau [MASK] [MASK] [MASK] sera utilisé dans le film [MASK] de [MASK] [MASK] , dans la série [MASK] [MASK] [MASK] [MASK] ( The [MASK] [MASK] ) lors du dernier épisode de la saison 2 et dans le film [MASK] [MASK] [MASK] , lorsque [MASK] [MASK] , sur un plateau de cinéma , sort d' une caravane .
Il joue également au [MASK] [MASK] au stade de [MASK] en compagnie de [MASK] ( qui joue les choristes sur la version studio de [MASK] [MASK] [MASK] ) , et devient , grâce à cette tournée , le groupe le plus vendeur des années 80 .
[MASK] [MASK] , initialement embauché par [MASK] pour ses projets en dehors de [MASK] [MASK] intègre également le groupe en tant que second clavier .
Le groupe ne jouera qu' une fois , pour l' anniversaire de [MASK] [MASK] en 1988 ( accompagné pour l' occasion d' [MASK] [MASK] à la place de [MASK] [MASK] , papa de deux jumeaux le soir-même et donc à la maternité ) , avant sa reformation l' année suivante .
Début 1990 , [MASK] [MASK] , composé alors de [MASK] , [MASK] [MASK] , et des claviers [MASK] [MASK] et [MASK] [MASK] , et accompagné par divers musiciens de session dont le batteur de [MASK] [MASK] [MASK] , se lance dans l' enregistrement d' un nouvel album .
[MASK] [MASK] [MASK] , sorti en septembre 1991 , et qui , sans remporter le même succès que son prédécesseur , se vend tout de même à plus de 8 millions d' exemplaires dans le monde .
Les tournées remportent un grand succès en [MASK] ( automne/hiver 1991 et de printemps/été 1992 ) .
Le groupe se sépare officiellement en 1995 , avec un [MASK] [MASK] au bout du rouleau qui déclare " [ ne plus vouloir ] entendre parler de [MASK] [MASK] pendant au moins 10 ans ! "
Le quatuor accompagné par leur manager à la batterie se reconstitue une seule fois , le temps de 5 chansons à l' occasion du mariage de [MASK] [MASK] le 19 juin 1999 .
Les influences du groupe sont à chercher du côté de [MASK] [MASK] pour le style de chant , et de [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] ou encore [MASK] [MASK] pour le jeu de guitare .
[MASK] [MASK] joue exclusivement aux doigts , n' utilisant pas de médiator , et emploie l' ancestrale technique blues qui consiste à alterner phrases chantées et phrases de guitare selon un système de " questions -- réponses " .
Son jeu , simple et classique mais d' une grande élégance , est le fondement du son de [MASK] [MASK] .
Les autres membres du groupe sont pour le moins discrets , et [MASK] se réserve en général les solos .
[MASK] [MASK] dira de lui " [MASK] fait n' importe quoi , mais il le fait bien ! "
Comme l' a fait remarquer le journal [MASK] [MASK] , une grande modestie a toujours fait partie de l' image de marque du groupe .
Ce son très soigné a fait de [MASK] [MASK] le groupe idéal pour les débuts du [MASK] , qui leur permettait de faire ressortir les moindres nuances de leur son .
[MASK] [MASK] [MASK] fut d' ailleurs souvent employé pour faire la démonstration des possibilités sonores des nouveaux lecteurs .
Les albums solo de [MASK] [MASK] , pourtant parfois de qualité comparable , sans être boudés par le public , ne sont jamais parvenus à remporter le même succès .