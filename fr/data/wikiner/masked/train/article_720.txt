Il a acquis sa notoriété populaire à la suite de ses violentes critiques envers le [MASK] et la [MASK] [MASK] , émises peu après son départ de la [MASK] [MASK] en 2000 , alors qu' il y était économiste en chef .
[MASK] est né en 1943 à [MASK] , [MASK] ( [MASK] ) , dans une famille juive .
De 1960 à 1963 , il étudia à [MASK] [MASK] .
Sa quatrième année d' université se déroula au [MASK] , où il entreprit ses travaux de recherche .
Ainsi commence alors une carrière exceptionnelle , à l' [MASK] [MASK] ( 1966-1973 ) d' abord où il est promu professeur ordinaire alors qu' il a à peine 27 ans .
Un thème qui fera de lui un des fondateurs de l' économie de l' information et , c' est à ce titre d' ailleurs , qu' il reçoit le [MASK] [MASK] [MASK] [MASK] en 2001 .
Il a par ailleurs été nommé par [MASK] [MASK] à l' [MASK] [MASK] [MASK] [MASK] [MASK] .
[MASK] a également joué de nombreux rôles politiques .
Il a par la suite été vice-président et économiste en chef de la [MASK] [MASK] de 1997 à 2000 .
Il n' en critiquera pas moins fortement cette institution par la suite , ainsi que le [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] compte parmi les membres fondateurs du [MASK] [MASK] [MASK] , politique et scientifique , association qui souhaite apporter des réponses intelligentes et appropriés qu' attendent les peuples du monde face aux nouveaux défis de notre temps .
Les deux organismes à caution publique mandatent [MASK] [MASK] pour répondre à ces attaques .
[MASK] y dénonce également les idées fausses quant au libre marché théorique dans lequel opère le système capitaliste dans sa forme libérale .
En 2002 , [MASK] publia [MASK] [MASK] [MASK] , où il affirme que le [MASK] fait passer l' intérêt de son " principal actionnaire " , les [MASK] , avant ceux des nations les moins favorisées qu' il a pourtant pour objectif de servir .
D' autre part , en prenant comme exemple la crise asiatique et la transition russe , [MASK] soutient que les politiques préconisées par le [MASK] ont souvent aggravé les problèmes dont il avait à s' occuper , entraînant des conséquences sociales dévastatrices et un accroissement de la pauvreté .
L' auteur concentre ensuite sa réflexion sur les dysfonctionnements de la sphère financière en critiquant la déréglementation incontrôlée du secteur financier et ses conséquences telles l' [MASK] [MASK] .
Tout en n' étant pas altermondialiste , [MASK] [MASK] collabore avec les forums sociaux et partage certaines analyses : il est ainsi partisan d' une taxe sur les transactions financières et pour une régulation de la mondialisation .
[MASK] a écrit de nombreux livres et articles .