[MASK] est un logiciel libre peer-to-peer conçu pour permettre à tout utilisateur connecté à [MASK] de diffuser sa propre radio ou vidéo en ligne .
Il se base en effet sur le protocole [MASK] pour créer un réseau " d' émetteurs " et d' auditeurs .
Ainsi , un abonnement standard à l' [MASK] suffit pour émettre un flux de très bonne qualité à destination d' un nombre quelconque d' utilisateurs .
Le logiciel [MASK] est disponible pour les plateformes [MASK] , [MASK] [MASK] et [MASK] , ainsi que sous la forme d' un plugin [MASK] 2 .
Le code source est disponible : http://www.peercast.org/code sous licence [MASK] .