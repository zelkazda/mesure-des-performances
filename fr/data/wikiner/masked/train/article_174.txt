C' est principalement dans les champs de coton de la région du delta du [MASK] que ces formes prennent des tours plus complexes .
La légende raconte que l' un des guitaristes bluesmen , [MASK] [MASK] , aurait signé un pacte avec le diable ce qui lui aurait permis de devenir un virtuose du blues ( blue devils : c' est une musique liée aux forces maléfiques qui était fuie et rejetée par beaucoup de personnes aux [MASK] ) .
[MASK] [MASK] fut l' un des premiers musiciens à reprendre des airs de blues , à les arranger et les faire interpréter par des chanteurs avec orchestres .
Les années 1920 et 1930 virent l' apparition de l' industrie du disque , et donc l' accroissement de la popularité de chanteurs et guitaristes tels que [MASK] [MASK] [MASK] et [MASK] [MASK] qui enregistrèrent chez [MASK] [MASK] , ou [MASK] [MASK] chez [MASK] [MASK] .
Après la [MASK] [MASK] [MASK] , l' urbanisation croissante et l' utilisation des amplificateurs pour la guitare et l' harmonica menèrent à un blues plus électrique , avec des artistes comme [MASK] [MASK] [MASK] et [MASK] [MASK] .
Dans les villes comme [MASK] , [MASK] et [MASK] [MASK] , un nouveau style de blues " électrique " apparut .
[MASK] , qui jouait avec les groupes d' [MASK] [MASK] et [MASK] [MASK] a également utilisé le saxophone , plutôt comme instrument d' accompagnement qu' instrument soliste .
Les harmonicistes comme [MASK] [MASK] et [MASK] [MASK] [MASK] ( [MASK] [MASK] ) étaient bien connus dans les clubs de blues à [MASK] .
[MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] jouaient de la guitare électrique avec un " slide " ou " bottle neck " ; l' exercice consiste à jouer les notes sur le manche en posant un bout de métal ou un goulot de bouteille sur les cordes .
[MASK] [MASK] [MASK] et [MASK] [MASK] n' ont pas utilisé le " slide " .
Les chanteurs [MASK] [MASK] [MASK] et [MASK] [MASK] marquèrent le blues de leurs voix rauques et fortes .
Le contrebassiste , compositeur , chercheur de talents [MASK] [MASK] a eu un grand impact sur l' environnement musical de [MASK] .
Le style de blues urbain des années 1950 a eu un grand impact sur la musique populaire des musiciens comme [MASK] [MASK] et [MASK] [MASK] .
Les blues de [MASK] [MASK] [MASK] étaient plus individuels que le style de blues de [MASK] .
Les musiciens blancs ont popularisé beaucoup de styles des américains noirs aux [MASK] et au [MASK] .
Dans les années 1960 , une nouvelle génération d' enthousiastes du blues apparaît en [MASK] et en particulier en [MASK] .
Des artistes américains comme [MASK] [MASK] , [MASK] [MASK] ou [MASK] [MASK] , tous influencés à la fois par le blues traditionnel et le blues électrique , firent découvrir cette musique au jeune public de l' époque .
À la même époque , [MASK] [MASK] [MASK] a retrouvé sa popularité , grâce à ses collaborations avec [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .
Dans le jazz , à partir des années 40 , des musiciens comme [MASK] [MASK] ont poussé la sophistication harmonique ( et mélodique ) de la forme à un degré élevé , qui contraste avec les enchaînements rudimentaires du blues originel ( " early blues " ) .
On a coutume de dire que les chanteurs classiques essaient d' imiter les instruments , alors que les instruments de blues essaient d' imiter la voix humaine ( ou parfois celle de [MASK] [MASK] , d' un bombardier ou d' une mitraillette ) .
Les voix fortes et graves de chanteurs comme [MASK] [MASK] [MASK] et [MASK] [MASK] jouent également beaucoup sur le timbre .
Pour cette dernière , divers moyens ont été utilisés depuis [MASK] [MASK] [MASK] , surtout les moyens mécaniques qui modifient légèrement la longueur de la corde vibrante .
À l' origine les bluesmen étaient des métayers noirs perdus au fin fond du " delta du [MASK] " , plaine cotonnière qui n' est pas le vrai delta mais se situe plus au nord .
Ils chantaient souvent pendant des événements locaux tels que la crue du [MASK] , la construction des digues , l' incendie d' une ferme de coton .
À la rigueur on parle d' une grande ville pas trop éloignée comme [MASK] [MASK] , [MASK] , [MASK] [MASK] .
Mais il y a fatalement des incursions ou des espoirs de voyages dans d' autres villes des [MASK] , que ce soit pour trouver du travail , faire le service militaire ou participer aux luttes d' émancipation .
Enfin l' horizon ne manquera pas de s' élargir au globe avec la participation de certains appelés à la [MASK] [MASK] [MASK] , au [MASK] [MASK] [MASK] , à la [MASK] [MASK] [MASK] [MASK] .
On retrouve tout ceci dans des blues comme ceux de [MASK] [MASK] .
On peut voir l' influence des blues dans la musique de [MASK] [MASK] , [MASK] [MASK] et dans la musique d' [MASK] [MASK] ( [MASK] [MASK] ) .
Le blues est d' abord l' élément principal du " mariage " avec la musique country qui a donné naissance au rock'n'roll , aux [MASK] , au milieu des années 50 .
Même un groupe progressif comme [MASK] [MASK] ( dont le nom lui-même vient de l' association des prénoms des bluesmen [MASK] [MASK] et [MASK] [MASK] ) , a fait appel à plusieurs reprises à la forme blues , non seulement à ses débuts avec [MASK] [MASK] , grand admirateur de [MASK] [MASK] , mais également par la suite , au milieu de morceaux plus psychédéliques .
Des artistes comme [MASK] [MASK] et [MASK] [MASK] revendiquent l' influence que le blues a sur leur création .
Côté instrumental , l' harmoniciste [MASK] [MASK] est un performer internationalement apprécié et enregistre plusieurs albums qui font référence .
Le blues a également influencé le cinéma , surtout aux [MASK] .
Bande son de [MASK] [MASK] et duel mythique entre le héros du film [MASK] [MASK] et [MASK] [MASK] en personne .
Le pacte de [MASK] [MASK] ( plus tard repris et rendu célèbre par [MASK] [MASK] ) est évoqué dans le film [MASK] , de [MASK] [MASK] .
Les deux films de [MASK] [MASK] , [MASK] [MASK] ( 1980 ) et [MASK] [MASK] [MASK] ( 1998 ) , qui dressent un panorama de différents styles et mettant en scène une pléthore de vedettes , ont eu une importante influence sur l' image du blues .