[MASK] [MASK] est un shōnen manga écrit et dessiné par [MASK] [MASK] .
[MASK] [MASK] a été pré-publié dans l' hebdomadaire [MASK] [MASK] de la [MASK] du 21 octobre 1998 au 31 octobre 2001 et a été compilé en 14 tomes .
En [MASK] , la série a été publiée dans son intégralité chez [MASK] [MASK] .
La série a également été adaptée en série d' animation par [MASK] en 2000 ainsi qu' en OAV .
Malgré son cadre typiquement japonais , la série a connu un franc succès au-delà des frontières du [MASK] , elle a d' ailleurs remporté un prix aux [MASK] : Grâce a un tel succès , [MASK] [MASK] s' est imposé comme une nouvelle référence du pantsu , notamment pour ses personnages hors-norme , ses situations rocambolesques et son humour bien souvent absurde .
La série animée de [MASK] [MASK] a été initialement diffusée en france sur [MASK] [MASK] avant de faire l' objet d' une diffusion nationale hertzienne sur [MASK] [MASK] dans l' émission [MASK] .
[MASK] [MASK] [MASK] suit l' intrigue du manga plutôt que celle amorcée par l' animé en couvrant librement les tomes 11 et 12 .
Ainsi , elle essaie par tous les moyens d' éloigner [MASK] de lui , car elle la considère comme une rivale .
[MASK] [MASK] [MASK] se conclut par un baiser passionné entre les deux tourtereaux .
Cet épisode se termine par le rêve de [MASK] dans le train , un passage similaire que l' on retrouve dans le 14 e tome du manga .
Le manga part d' une idée toute simple , confronter un jeune homme qui rate tout ce qu' il entreprend a des personnages haut en couleurs dans une pension ; en cela il est similaire à [MASK] [MASK] de [MASK] [MASK] , cependant les relations et la façon d' être des personnages les font diverger .
Fort du succès du manga , [MASK] [MASK] a été adapté en une série animée de 25 épisodes .
Au [MASK] , un 26e épisode spécial a été diffusé à la suite de l' enregistrement d' un concert regroupant tous les doubleurs qui ont participé au succès de la série animée .
Une OAV en trois parties intitulée [MASK] [MASK] [MASK] fut également produite .