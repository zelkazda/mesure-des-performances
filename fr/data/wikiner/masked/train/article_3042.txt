[MASK] [MASK] est né à [MASK] en 1939 .
Entré dans la vie publique à 20 ans , il fut l' un des animateurs de l' [MASK] jusqu' en 1963 .
Il sera notamment l' avocat de la [MASK] et des ouvriers de l' usine [MASK] de [MASK] .
Conseiller de [MASK] et du 10 e arrondissement à partir de 1989 , [MASK] [MASK] a été secrétaire d' état de 1988 à 1991 au sein du gouvernement de [MASK] [MASK] .
Devant les militants de la section socialiste , [MASK] [MASK] s' engage , en mai 2006 , à ne pas briguer de nouveau mandat à la tête de la mairie en 2008 et de mettre ainsi un terme à une situation de cumul .