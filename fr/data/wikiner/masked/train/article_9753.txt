Durant cette période , les trois royaumes de [MASK] ( 蜀 ) , [MASK] ( 魏 ) et [MASK] ( 吳 ) s' affrontèrent pour la domination de la [MASK] .
Un général du nord , fils adoptif d' un eunuque , nommé [MASK] [MASK] , saura profiter de ces troubles pour faire une carrière militaire fulgurante .
Les différents potentats locaux qui se partagent le pays vont alors tantôt s' allier et tantôt s' affronter jusqu' à ce qu' il ne reste plus que trois sphères d' influence relativement stables , aux lendemains de la [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] zhi zhan ) , en 208 .
Ces trois royaumes sont : au nord , le [MASK] [MASK] [MASK] , dirigé par [MASK] [MASK] ; au sud-est , le [MASK] [MASK] [MASK] , sous la direction de [MASK] [MASK] ; au sud-ouest , le dernier royaume formé est le [MASK] [MASK] [MASK] de [MASK] [MASK] et de son stratège [MASK] [MASK] .
[MASK] [MASK] ne tarde pas à gêner [MASK] [MASK] ( 袁紹 ) , qui occupe la majeure partie des plaines centrales et accumulait de plus en plus de pouvoir .
Une longue guerre d' usure s' en est suivi , guerre contestée par les stratèges de [MASK] [MASK] , qui ne tint pas compte de leurs avis .
Avant de mourir de frustration , [MASK] [MASK] demanda à son fils cadet d' être son successeur .
[MASK] [MASK] sut profiter astucieusement de la situation en laissant les frères guerroyer et s' entretuer .
Bientôt , il ne resta plus aucun descendant de [MASK] [MASK] qui souhaitait voir la guerre continuer .
[MASK] [MASK] avait ainsi gagné : il occupait toutes les plaines centrales et était à la tête de l' un des plus puissants royaumes , sinon le plus puissant .
[MASK] [MASK] , pendant ce temps , avait gagné la confiance de [MASK] [MASK] .
[MASK] [MASK] , s' appuyant sur l' exemple de la famille [MASK] , incita son cousin à choisir son fils aîné afin d' éviter les conflits , lorsque le moment serait venu .
Cependant , [MASK] [MASK] ( 蔡瑁 ) , oncle du fils cadet , se fixa pour but de poursuivre et tuer [MASK] [MASK] .
Le fils de [MASK] [MASK] ( 孫堅 ) , [MASK] [MASK] parvient avec le stratège [MASK] [MASK] à reconquérir toutes les terres dont s' étaient emparé les brigands au sud-est du [MASK] .
Le royaume est ainsi envahi , mais [MASK] [MASK] refuse de se rendre , et s' enfuit encore plus à l' ouest .
Il obtient , par la suite , les services du stratège légendaire , [MASK] [MASK] , qui fut pour beaucoup dans la subsistance du royaume .
L' armée de [MASK] [MASK] n' avait en effet pas l' expérience des combats navals .
[MASK] [MASK] donne son accord à cette alliance .
Le général [MASK] [MASK] parvient à faire exécuter [MASK] [MASK] par [MASK] [MASK] , qui le prend pour un traître .
[MASK] [MASK] envoie 30 000 soldats se joindre aux troupes de [MASK] [MASK] pour brûler les bateaux de [MASK] [MASK] , comme le lui avait suggéré le général suprême .
Les troupes alliées de [MASK] [MASK] et [MASK] [MASK] avaient enfin vaincu [MASK] [MASK] , malgré la faiblesse de leurs moyens .
Voyant que le feu ardent des bateaux de [MASK] [MASK] avait fait rougir la falaise , on l' appela [MASK] [MASK] .
Durant cette bataille , les troupes de [MASK] [MASK] , dont la stratégie audacieuse avait permis de remporter la victoire , tuent plusieurs milliers de soldats de l' armée de [MASK] [MASK] .
La [MASK] [MASK] [MASK] [MASK] [MASK] fut la plus écrasante défaite de [MASK] [MASK] , qui fut stoppé dans son élan de conquête .
La [MASK] [MASK] [MASK] [MASK] [MASK] est un exemple réussi de retournement stratégique majeur : comment l' emporter sur un adversaire plus fort , c' est-à-dire dans un rapport de force disproportionné ?
Car la [MASK] [MASK] [MASK] [MASK] [MASK] raconte comment les armées de [MASK] , soient 50 à 60 000 hommes environ , ont pu venir à bout d' une armée estimée entre 200 et 500 000 hommes .
Plus étonnant encore , les grands vainqueurs de cette [MASK] [MASK] [MASK] [MASK] [MASK] sont ceux qui y ont pris militairement le moins de risque : [MASK] [MASK] et [MASK] [MASK] .
La stratégie parfaitement réussie de [MASK] [MASK] consiste comme dit un proverbe chinois " à tuer avec un couteau emprunté " , jie dao sha ren , c' est-à-dire à agir et à faire porter le risque d' une entreprise par un autre .
[MASK] [MASK] tente de nombreuses fois de tromper [MASK] [MASK] et de le tuer , mais en vain .
[MASK] [MASK] avait estimé que pour obtenir un équilibre , il fallait que ne subsistent que trois royaumes en [MASK] .
Aussi incite-t-il [MASK] [MASK] à envahir le royaume de son autre cousin , [MASK] [MASK] ( 劉璋 ) .
Le royaume pouvait , en effet , rivaliser avec le [MASK] , mais son chef , peu charismatique et prudent , n' avait entrepris aucune opération militaire .
Au terme de quelques escarmouches entre lui et [MASK] [MASK] , il préféra lui céder ses pouvoirs pour éviter un bain de sang .
À la mort de [MASK] [MASK] , le [MASK] [MASK] [MASK] ne dispose d' aucun stratège compétitif , hormis son disciple [MASK] [MASK] ( 姜維 ) , qui ne parvient pas à envahir le [MASK] contre lequel il luttait .
Le royaume du [MASK] ne trouve aucun successeur convenable à [MASK] [MASK] : tous se révélaient être des tyrans sans vertu .