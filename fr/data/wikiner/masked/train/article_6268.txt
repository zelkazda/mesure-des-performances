[MASK] [MASK] [MASK] [MASK] [MASK] est le plus grand récif corallien du monde , .
Il est situé au large du [MASK] , en [MASK] .
Il s' étend sur 2600 kilomètres , de [MASK] à la pointe du [MASK] [MASK] .
À titre de comparaison , l' [MASK] a une superficie de 357000 km² .
La [MASK] [MASK] [MASK] [MASK] compte plus de 2000 îles et près de 3000 récifs de toutes sortes .
Elle est inscrite sur la liste du patrimoine mondial de l' [MASK] .
Pour les [MASK] , les milliers d' îles , d' îlots et d' atolls composant la [MASK] [MASK] [MASK] [MASK] constituent la 8 e merveille du monde .
De nombreuses villes le long de la côte du [MASK] offrent des départs quotidiens en bateau vers le récif .
Le premier explorateur européen à avoir vu la grande barrière fut le capitaine britannique [MASK] [MASK] , lors de son voyage de 1768 .
La [MASK] [MASK] [MASK] [MASK] est relativement jeune .
La menace la plus significative pour l' avenir de la [MASK] [MASK] [MASK] [MASK] et d' autres écosystèmes tropicaux est le réchauffement climatique .
Depuis le 1 er juillet 2004 , la pêche est interdite dans un tiers de la [MASK] [MASK] [MASK] [MASK] .
Celle-ci fut découvert par les européens quand l' [MASK] , le navire commandé par l' explorateur [MASK] [MASK] , s' y échoua le 11 juin 1770 , subissant de gros dommages .
Le navire fut finalement sauvé après avoir été allégé au maximum pour le remettre à flot pendant une marée montante avant d' être amené à [MASK] pour y être réparé .
Un des plus fameux naufrages fut celui du [MASK] , qui coula le 29 août 1791 , occasionnant la mort de 35 marins .