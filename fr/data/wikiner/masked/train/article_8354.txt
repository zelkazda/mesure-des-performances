Elle est située à la confluence du [MASK] et de l' [MASK] .
Il existe aussi une ville de [MASK] au [MASK] .
La ville se révolta contre [MASK] [MASK] en 1520 au cours de la [MASK] [MASK] [MASK] [MASK] [MASK] .
et qui opposa le dominicain [MASK] [MASK] [MASK] [MASK] et le théologien [MASK] [MASK] [MASK] [MASK] ( leur détracteur ) .
La version la plus tangible est celle en relation avec la présence musulmane en [MASK] .
En effet , les arabes appelaient cette région " balad al walid " soit ville d' al walid ( un prince arabe ) , ( balad al walid = [MASK] ) .
Ces dernières années , la ville de [MASK] a connu une perte de population , en faveur de sa banlieue , où de nouvelles zones résidentielles prolifèrent .
Le climat de [MASK] est méditerranéen continental .