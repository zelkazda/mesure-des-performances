L' [MASK] [MASK] ( [MASK] ) est une association de droit américain à vocation internationale créée en janvier 1992 par les pionniers de l' [MASK] pour promouvoir et coordonner le développement des réseaux informatiques dans le monde .
Elle est en 2005 l' autorité morale et technique la plus influente dans l' univers du [MASK] [MASK] .
En 1992 , l' [MASK] poursuivait sa croissance rapide avec un doublement des différents indicateurs tous les 12 à 15 mois .
Dans l' esprit de ses créateurs , il s' agissait dès lors pour l' [MASK] d' encourager la croissance du réseau qui s' amorçait à l' échelle mondiale en débordant le cadre initial de la recherche .
Ainsi , même si l' [MASK] a une dynamique qui lui est propre , l' [MASK] veille à sa progression et à sa bonne marche .