[MASK] [MASK] , homme politique français , né le 19 juin 1933 à [MASK] ( [MASK] ) .
Il fait partie du groupe [MASK] .
[MASK] [MASK] , 1 e secrétaire du [MASK] et candidat aux élections cantonales en mars 2008 avait déclaré vouloir lui reprendre la tête du conseil .
Le [MASK] a battu la droite dans 3 des 18 cantons mis en jeu ; le conseil général bascule donc à gauche .
[MASK] [MASK] remplace [MASK] [MASK] à la tête de l' assemblée départementale .