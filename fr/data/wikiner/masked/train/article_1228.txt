Par [MASK] grondent les orages , il s' oppose toujours à l' harmonie des choses et des arrangements , il est la force brûlante , capable de détruire toute forme de vie .
C' est l' un des dieux les plus complexes et ambigus ; les mythes relatifs à [MASK] le dépeignent comme un dieu ambitieux , comploteur , manipulateur , quand il ne se résume pas tout simplement à un assassin .
D' autres [MASK] lui vouaient des cultes secrets qui exigeaient des sacrifices humains ; ces sectes furent toujours maudites et poursuivies par les pharaons .
Dans un certain sens , le christianisme a récupéré [MASK] sous le nom du " diable " .
[MASK] est associé à deux grands mythes :
Il l' assassine en le noyant dans le [MASK] .
[MASK] , l' épouse d' [MASK] , retrouve le dieu noyé , l' embaume et lui donne une sépulture dans le [MASK] [MASK] [MASK] .
[MASK] retrouve la sépulture de son frère , et de rage , le dépèce et disperse les morceaux du corps dans toute l' [MASK] .
[MASK] , infatigable veuve , retrouve treize des quatorze parties de son bien-aimé ( la partie manquante étant le sexe d' [MASK] , dénommé à cette occasion son " talisman " ) .
Puis [MASK] le reconstitue , lui insuffle le souffle de la vie éternelle , et par sa magie conçoit avec lui un fils , [MASK] .
Au-delà de l' assassinat d' [MASK] par [MASK] , sans cesse poussé par son insatiable jalousie , [MASK] eut tôt fait de reporter sa haine sur [MASK] .
[MASK] , fils d' [MASK] , en est aussi l' héritier : la couronne d' [MASK] lui revient donc de droit .
Mais [MASK] , jaloux et considérant la primauté de ses propres droits , s' en est emparé par la force et ne lui cède rien .
[MASK] , appuyé de sa mère [MASK] , fait convoquer le tribunal des dieux à toute fin de régler ce contentieux .
[MASK] préside , tandis que [MASK] tient le rôle du greffier .
Le tribunal est même partagé entre les tenants de la royauté légitime ( revenant à [MASK] ) , et [MASK] qui voit en [MASK] son perpétuel défenseur contre [MASK] .
C' est donc à [MASK] , déesse de [MASK] , réputée pour son infinie sagesse , que [MASK] s' adresse .
Sa réponse est sans ambigüité : la couronne revient à [MASK] .
Cependant pour ne pas pénaliser [MASK] , [MASK] propose de lui offrir les déesses [MASK] et [MASK] comme épouses .
Si le tribunal se réjouit de cette solution , [MASK] , lui , reste sceptique .
[MASK] ne serait-il pas un peu jeune pour assumer la direction du royaume ?
[MASK] , excédée par tant de tergiversations , propose de déplacer les débats à [MASK] ( [MASK] ) devant [MASK] et [MASK] .
[MASK] , furieux , s' y oppose et ordonne que les débats se fassent en l' absence d' [MASK] .
Elle se réintroduit dans l' enceinte du tribunal sous les traits d' une belle jeune femme qui ne manque pas d' attirer rapidement l' attention de [MASK] .
[MASK] se dévoile alors .
Le coup de théâtre laisse [MASK] sans voix .
Quant à [MASK] , il a pu juger de l' imprudence de [MASK] , qui se confia à une inconnue sans prendre garde .
Aussi la couronne revient-elle à [MASK] des mains de [MASK] lui-même .
[MASK] , éternel jaloux , ne semble pas décidé à en rester là .
Il propose à [MASK] des jeux sportifs .
Mais [MASK] , qui suit de près les mésaventures de son fils , perturbe la partie et s' attire au final le mécontentement des deux protagonistes .
[MASK] , désespérant d' assister enfin à une réconciliation entre l' oncle et le neveu , les invite à faire la paix autour d' un banquet .
L' attentat , destiné à féminiser [MASK] et à le rendre indigne du pouvoir , finit par se retourner contre [MASK] .
[MASK] , resté silencieux , intervient alors et met directement en cause le tribunal qu' il juge trop laxiste .
En tant que dieu de la végétation , il menace de couper les vivres à l' [MASK] .
Les dieux , bousculés par tant d' autorité , ne tardent pas à rendre un verdict favorable à [MASK] .
Mais [MASK] n' est pas oublié .
Placé aux côtés de [MASK] , il devient " celui qui hurle dans le ciel " pour que soit fait place devant le dieu créateur .
Depuis le premier jour , [MASK] voyage dans le ciel à bord de sa barque qui l' emmène le jour d' est en ouest , puis la nuit d' ouest en est .
Car , noyée dans l' obscurité qu' elle fend à vive allure , la barque est épiée depuis les profondeurs des ténèbres par [MASK] , le serpent du chaos .
Mais c' est compter sans [MASK] placé à la proue de la barque et qui , d' un coup de pique , envoie le serpent monstrueux s' en retourner aux confins du monde .
On comprend dès lors que [MASK] ait pour [MASK] une certaine estime .