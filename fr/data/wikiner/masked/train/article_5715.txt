On compte 150 amas globulaires dans [MASK] [MASK] .
C' est par leur étude que [MASK] [MASK] , en 1918 , a pu déterminer la position du [MASK] au sein de [MASK] [MASK] .
Certains amas globulaires , comme [MASK] [MASK] de [MASK] [MASK] , peuvent avoir une masse de plusieurs millions de masses solaires .
Le premier à obtenir ce niveau de détail fut [MASK] [MASK] quand il observa l' [MASK] [MASK] .
[MASK] [MASK] commença un programme d' observation en 1782 , utilisant un télescope plus grand capable de séparer les étoiles des 33 amas globulaires connus à ce moment .
Le premier à utiliser le terme amas globulaire fut [MASK] dans son catalogue des objets lointains de 1789 .
Au début de l' année 1914 , [MASK] [MASK] débuta une série d' études des amas globulaires , publiées dans une quarantaine d' articles scientifiques .
En 1918 cette distribution très asymétrique a été utilisée par [MASK] [MASK] pour déterminer les dimensions de notre galaxie dans son ensemble .
Cette erreur était due au fait que la poussière galactique diminuait la quantité de lumière atteignant la [MASK] en provenance des amas , les faisant apparaître plus lointains .