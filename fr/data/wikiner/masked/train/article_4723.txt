Celle-ci publie un roman autobiographique ( [MASK] [MASK] [MASK] ) en 1932 , et elle est une source d' inspiration constante pour son mari .
[MASK] [MASK] [MASK] [MASK] naît le 24 septembre 1896 dans une famille de la petite bourgeoisie de [MASK] [MASK] , capitale du [MASK] .
Ses trois prénoms lui sont donnés en hommage à son lointain parent [MASK] [MASK] [MASK] qui est le parolier de l' hymne national américain , [MASK] [MASK] [MASK] .
La famille revient s' installer à [MASK] [MASK] en 1908 sans parvenir à trouver une stabilité financière et sociale .
C' est là qu' il rêve d' entrer dans une des toutes meilleures universités du pays : [MASK] .
Ce n' est que lors de sa deuxième année dans le [MASK] [MASK] que le futur écrivain parvient à se faire des amis , ainsi qu' une place dans les journaux de l' université .
C' est là qu' il tombe amoureux de l' excentrique [MASK] [MASK] , dix-huit ans mais déjà pleine d' esprit .
Les retombées financières permettent à l' écrivain d' épouser [MASK] .
À la mise en vente de [MASK] [MASK] [MASK] , en avril 1925 , malgré les bonnes critiques , les ventes ne décollent pas , même si elles lui rapportent la jolie somme de 28000 dollars .
Cela ne satisfait guère l' écrivain , et [MASK] est donc forcé de continuer à écrire des nouvelles , puisque le [MASK] [MASK] [MASK] et d' autres journaux les lui achètent encore à prix d' or .
Une grande partie des nouvelles ultérieures portera en filigrane ce même message , à un point que mentionneront tous les commentateurs de [MASK] : Les riches sont différents .
Et son succès , quoique très relatif commercialement , relègue [MASK] à un rôle secondaire en totale contradiction avec sa nature .
Entre les visites à sa femme ( [MASK] fait interner [MASK] en [MASK] , puis à [MASK] ; toujours dans les meilleures cliniques ) , son propre alcoolisme , les dépressions psychologiques et les soucis financiers , [MASK] [MASK] [MASK] parvient toutefois -- au bout de neuf ans !
-- à écrire [MASK] [MASK] [MASK] [MASK] , aujourd'hui considéré comme son chef-d'œuvre .
Pourtant , les meilleurs livres de [MASK] sont ceux qui se vendent le moins bien , et celui-ci ne fait pas exception .
C' est dans la misère que [MASK] [MASK] [MASK] meurt à [MASK] en 1940 , alors qu' il exerce la profession détestée de scénariste .
Il laisse le fort prometteur [MASK] [MASK] inachevé .
Sa femme meurt quelques années plus tard dans l' incendie qui ravage le sanatorium d' [MASK] , où elle est internée .
Le temps a fait de [MASK] l' émouvante incarnation du talent gâché et incompris .
La capacité de [MASK] [MASK] à capter l' instant , à définir les atmosphères , dans le style visuel et hautement lyrique qui lui est propre -- en somme : son génie -- est aujourd'hui pleinement reconnue .