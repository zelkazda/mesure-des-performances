[MASK] est une préfecture du [MASK] , située au centre de l' île de [MASK] .
Le nom [MASK] tire son origine d' un mont chinois et a été choisi par [MASK] [MASK] qui y construisit son château .
À l' époque féodale , le territoire actuel de la préfecture correspondait à la [MASK] [MASK] [MASK] ( 美濃 ) pour sa partie sud et celle de [MASK] pour sa partie nord .
La [MASK] [MASK] [MASK] est également connue pour sa production d ' agar-agar ( une gélatine fabriquée à partir d' algues ) .
En 1600 le combat de [MASK] s' y déroula .
Les voies de communications sont étroites et coûteuses à réaliser dans cette région , on ne s' étonnera pas de l' absence d' autoroute ( actuellement ) pour relier [MASK] à [MASK] .
Une autoroute , néanmoins , relie [MASK] à [MASK] , au nord de la préfecture , dans un milieu très montagneux et dans une alternance saisissante de ponts et de tunnels .
L' économie de [MASK] souffre de sa situation , tant et malheureusement que son taux de croissance a été pendant longtemps négatif ( jusqu' en 2003 ) .