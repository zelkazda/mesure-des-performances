Cependant , au pluriel il peut être soit féminin en parlant d' un seul instrument ( de belles orgues , les grandes orgues de [MASK] [MASK] [MASK] ) , soit masculin en parlant de plusieurs instruments .
Mais la préhistoire mythologique de l' orgue commence avec la figure grecque du satyre [MASK] , un joueur d' aulos , le " patron " des futurs organistes , qu' on sait avoir été en lutte avec [MASK] , le " patron " des joueurs de lyre , et donc des clavecinistes modernes et des harpistes .
[MASK] , dans son [MASK] nous apprend qu' au cirque , l' hydraule accompagnait les courses de chars .
Ses successeurs , [MASK] , [MASK] [MASK] , [MASK] , furent de fervents admirateurs de l' orgue [ réf. nécessaire ] .
Plusieurs fragments d' orgue d' époque romaine ont été retrouvés , dont celui d' [MASK] en [MASK] .
Un orgue a été offert par une ambassade de [MASK] [MASK] , empereur de [MASK] , à [MASK] [MASK] [MASK] en 757 .
La facture romantique renoue avec les progrès technologiques , sous l' impulsion notoire d' [MASK] [MASK] : ces progrès concernent au premier chef les modes de transmission et la production du vent , mais aussi l' esthétique musicale .
Les compositeurs à l' utiliser à l' époque seront notamment [MASK] [MASK] et [MASK] [MASK] [MASK] .
En [MASK] , ce mouvement se fait en parallèle avec un vaste chantier de restauration du patrimoine instrumental au titre des monuments historiques .
Par exemple , le grand orgue de l' [MASK] [MASK] [MASK] [MASK] possède deux consoles , l' une en tribune à traction mécanique , l' autre , électrique et mobile , au niveau du sol à l' entrée de la nef ( côté gauche ) .
Le grand orgue de [MASK] [MASK] [MASK] en est un exemple .
Le plus souvent , les tuyaux ont une position verticale ; ils peuvent aussi être disposés horizontalement ( disposition en éventail dite " en chamade " souvent usitée en [MASK] ) .
[MASK] .