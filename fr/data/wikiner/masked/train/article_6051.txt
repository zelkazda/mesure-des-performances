Son père , [MASK] [MASK] ( 鄭芝龍 ) est un pirate , un marin et un marchand originaire d' une famille de pêcheurs du [MASK] .
Il est élevé jusqu' à ses sept ans par sa mère puis son père l' emmène à [MASK] , près de [MASK] au [MASK] pour son éducation .
Il reçoit l' appui de [MASK] [MASK] qui s' arrange pour que [MASK] [MASK] serve à ses côtés .
La fidélité aux [MASK] n' est pas la seule raison qu' ont les pirates tels que [MASK] et [MASK] [MASK] de combattre les [MASK] .
Ceux-ci ont en effet restreint le commerce par différentes mesures défavorables et la situation des pirates marchands devient beaucoup plus délicate que sous les [MASK] .
[MASK] n' est pas de cet avis et continue le combat .
Il pousse ses razzias jusqu' à [MASK] mais est repoussé progressivement jusqu' à se sentir acculé .