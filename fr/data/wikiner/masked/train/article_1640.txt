Le [MASK] est un pays d' [MASK] [MASK] , entouré de l' [MASK] , de l' [MASK] , de la [MASK] [MASK] , du [MASK] et de l' [MASK] .
Autrefois république socialiste soviétique jusqu' en 1991 , il portait le nom de [MASK] ou officiellement [MASK] [MASK] [MASK] [MASK] [MASK] .
L' [MASK] [MASK] [MASK] s' étend sur plusieurs millénaires , puisque des vestiges archéologiques vieux de 5000 ans ont été découverts sur le territoire du pays .
[MASK] [MASK] était le chef de la [MASK] [MASK] [MASK] [MASK] [MASK] entre 1985 et 1991 .
Le [MASK] fait partie de la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
En août 2005 , le [MASK] décide de devenir un simple " membre associé " .
Il est officiellement investi le 14 février 2007 , prêtant serment sur le [MASK] et le [MASK] , livre écrit par son prédécesseur .
[MASK] [MASK] a dressé un tableau très noir de la situation au [MASK] en 2003 , et s' est notamment montré pessimiste sur une éventuelle évolution positive à cause de :
La situation des droits de l' homme au [MASK] est toujours critique en 2006 .
Selon [MASK] [MASK] [MASK] , on peut citer parmi les atteintes aux libertés :
et une ville indépendante , qui est la capitale [MASK] .
Le pays a des frontières avec le [MASK] et l' [MASK] au nord et au nord-est , avec l' [MASK] et l' [MASK] au sud et au sud-est .
La caractéristique géographique la plus significative est le [MASK] [MASK] [MASK] qui couvre 80 % de la superficie du pays .
La plupart des montagnes du [MASK] sont inaccessibles .
Sa superficie est comparable à celle de l' [MASK] .
Le transporteur aérien national , [MASK] [MASK] , lui est moins utilisé .
La route européenne [MASK] traverse , par son prolongement asiatique , le [MASK] .
Le groupe français [MASK] [MASK] a réalisé en 1991 la moitié de son chiffre d' affaires mondial au [MASK] .
Il existe toutefois d' importantes minorités d' [MASK] ou de [MASK] .
Il y a également des minorités moins représentées ( [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] ) .
La musique turkmène est typiquement pastorale comme celle d' [MASK] [MASK] ; les bardes nomades chantent et s' accompagnent au luth dutar .
Ainsi , seuls le sunnisme et l' [MASK] [MASK] [MASK] sont enregistrés comme des organisations religieuses légales au [MASK] .
Le [MASK] a pour codes :