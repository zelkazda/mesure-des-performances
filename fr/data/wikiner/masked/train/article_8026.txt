Il est chargé de la production , de l' analyse et de la diffusion des statistiques officielles en [MASK] : comptabilité nationale annuelle et trimestrielle , évaluation de la démographie nationale , du taux de chômage , etc .
Depuis octobre 2007 , son directeur est [MASK] [MASK] .
Les fonctions principales de l' [MASK] sont de :
L' [MASK] gère également des répertoires :
L' [MASK] gère les codes qui servent à identifier les zones géographiques -- les communes ( voir liste ) , les cantons , les arrondissements , les départements , les régions , les pays et territoires étrangers -- ainsi que certaines nomenclatures , par exemple la nomenclature d' activités françaises , qui sert notamment à coder l' activité principale exercée par une entreprise ou un établissement , ou la nomenclature des catégories socio-professionnelles .
L' [MASK] assure également la diffusion et l' analyse des informations statistiques .
Pour faciliter l' accès à ces statistiques , l' [MASK] a instauré des " intermédiaires [MASK] " dans les différentes régions françaises dont la liste exhaustive est disponible ici .
Les services de recherche économique et de conjoncture de l' [MASK] publient des études économiques , en s' attachant à respecter une stricte neutralité et une rigueur d' analyse .
En amont des données statistiques définitives , l' [MASK] réalise des prévisions à trois ou six mois des principaux agrégats statistiques .
Tous les quatre mois , l' [MASK] publie une note de conjoncture .
L' accès aux informations de l' [MASK] est gratuit dans sa quasi-totalité .
L' [MASK] propose également quelques services payants .
En outre , l' [MASK] est le correspondant français d' [MASK] , l' organe européen de statistique ( qui ne travaille pas directement dans les pays , mais fait toujours appel aux organes nationaux ) , et , plus généralement , représente [MASK] [MASK] dans les instances internationales où il est question de statistique .
L' [MASK] , en collaboration avec [MASK] , a harmonisé ses statistiques avec les standards européens , qu' il a contribué à définir .
[MASK] , dans un rapport en janvier 2007 , reconnaît que " l' indépendance professionnelle est un point fort de la culture de l' [MASK] " et que " l' [MASK] est généralement considéré comme un institut statistique de grande qualité " .
[MASK] [MASK] , a été publiée au [MASK] [MASK] de la [MASK] [MASK] le 27 mai 2009 .
Les travaux de l' [MASK] ne sont pas exempts de critiques .
On peut distinguer deux sources principales , l' une de caractère politique , l' autre de caractère scientifique aux reproches concernant les statistiques et les méthodes de l' [MASK] .
Les propres syndicats maisons de l' [MASK] demandent une rectification aux articles publiés .
En vain , car " l' [MASK] assume complètement l' article " .
C' est la loi de finances du 27 avril 1946 qui crée l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] pour la métropole et [MASK] [MASK] d' outre-mer .
Parmi les projets les plus importants de l' [MASK] figurent la mise en place depuis le 1 er janvier 2004 du nouveau recensement de la population française , devenu annuel avec renouvellement partiel , et le programme de refonte des statistiques annuelles d' entreprises , visant à mettre en place la nouvelle enquête sectorielle annuelle auprès des entreprises françaises dès 2009 , afin de répondre aux exigences statistiques européennes .
Le gouvernement envisage d' imposer un déménagement d' une partie des activités de l' [MASK] à [MASK] , au titre de l' aménagement du territoire .
Le rapport remis au premier ministre en décembre 2008 par [MASK] [MASK] , directeur général de l' [MASK] , parle lui même de projet " à hauts risques. "
Il note notamment " qu' une telle opération comporte un risque élevé de perte d' expérience professionnelle et de capital humain , ainsi que des coûts de relocalisation qu' il faut mettre en regard du gain obtenu en termes d' aménagement du territoire " et que " l' opportunité d' ouvrir une nouvelle implantation territoriale en [MASK] ne va pas nécessairement de soi. "
Les services centraux de l' [MASK] sont répartis sur trois bâtiments : Une bibliothèque centrale , regroupant en fait plusieurs anciennes bibliothèques , est installée dans le centre de [MASK] .
Une direction régionale de l' [MASK] assure le service déconcentré dans chaque région française ( les trois régions de [MASK] , [MASK] et [MASK] étant rattachées à une seule direction interrégionale ) , réalisant la collecte des données au niveau local et produisant des statistiques et des études au niveau régional ( par exemple , analyse de bassins d' emploi ou de marché du travail local ) .