Les furets n' existent pas à l' état sauvage en [MASK] car , malgré de nombreuses évasions , ils n' ont jamais réussi à s' installer et à établir des groupes sauvages , même dans les endroits où les proies étaient abondantes .
On craint en revanche qu' il ne devienne une espèce invasive en [MASK] où ils pourraient causer des ravages sur la faune locale protégée .
En [MASK] la pratique du furetage est interdite .
En [MASK] , elle dépend d' une législation spécifique .
Ils sont issus généralement d' élevages intensifs pratiqués aux [MASK] mais certains proviennent d' élevage européens .
La seule différence en effet avec un furet dit " européen " est son espérance de vie plus courte , en raison de la méthode d' élevage , notamment le stérilisation précoce ( à 3 semaines au lieu de 6 mois minimum ) ou du déglandage ( opération interdite en [MASK] ) ) ou l' élevage sous éclairage artificiel 24h sur 24 pour pouvoir produire des furets toute l' année .
Bien que celle-ci soit quasiment éradiquée en [MASK] , elle apparaît encore ponctuellement .
Cette opération est maintenant interdite en [MASK] , en [MASK] et en [MASK] ( considérée comme une " opération de convenance " ) .
Il lui faut un bon espace à l' intérieur pour pouvoir bouger et jouer ( en [MASK] , depuis septembre 2008 , la loi exige une surface minimum de 4 m² pour un couple de furet ( 0,50 m² par furet supplémentaire ) , ainsi qu' une hauteur d' au moins 0,60 m . )
En [MASK] , comme tous les carnivores domestiques de compagnie , le furet doit posséder un passeport européen pour voyager et pour cela être vacciné , examiné et identifié .
En [MASK] , entre autres , le furet est toujours considéré comme un animal non domestique et sa possession nécessite un certificat délivré par le vétérinaire cantonal après contrôle du bon suivi de la loi .
Les furets plus âgés doivent remplir des conditions strictes pour rentrer en [MASK] , surtout s' ils sont importés d' un pays situé hors de l' [MASK] [MASK] .
En [MASK] , le furet n' est pas obligé de posséder un passeport , si ce n' est le passeport européen pour sortir du territoire .
En [MASK] , dans le [MASK] , pour préserver la faune locale et éviter qu' il ne devienne une espèce invasive .
Les furets sont aussi interdits en [MASK] pour des raisons prophylactiques et pour marquer la volonté politique d' éradiquer le furet féral en tant que " unwanted organism " , aux [MASK] , par exemple à [MASK] [MASK] où la raison invoquée est la tendance de l' animal à passer chez les voisins et à mordre , ainsi que dans d' autres pays du monde .