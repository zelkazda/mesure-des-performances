[MASK] [MASK] est un logiciel libre de modélisation polygonale en trois dimensions par surface de subdivision .
[MASK] [MASK] est disponible sous de nombreuses plate-formes , y compris [MASK] , [MASK] [MASK] [MASK] et [MASK] .
Il est écrit dans le langage de programmation [MASK] et utilise une console virtuelle .
De plus , son système de plugin rend possible l' extension des capacités du logiciel : il est par exemple possible d' utiliser un moteur de rendu par lancer de rayon externe tel que [MASK] ou [MASK] .
[MASK] [MASK] est disponible en français et dispose de tutoriels et d' une interface très légère qui permettent d' en assimiler les bases rapidement .