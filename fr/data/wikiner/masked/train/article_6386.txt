Elle semble avoir été commandée par [MASK] [MASK] [MASK] , le demi-frère de [MASK] [MASK] [MASK] et décrit les faits relatifs à la [MASK] [MASK] [MASK] [MASK] [MASK] en 1066 .
Elle détaille les événements clés de cette conquête , notamment la [MASK] [MASK] [MASK] .
Elle a été confectionnée entre 1066 et 1082 , peut-être en [MASK] , pour être exposée à la [MASK] [MASK] [MASK] pour une population souvent analphabète .
Sa fin est perdue mais elle devait se terminer , d' après tous les historiens , par le couronnement de [MASK] .
[MASK] [MASK] [MASK] est généralement identifié comme étant le commanditaire de [MASK] [MASK] [MASK] [MASK] .
Tout d' abord , sur la tapisserie ne sont nommées , en dehors des figures historiques ( [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] etc . )
Ceux-ci ne sont mentionnées dans aucune autre source contemporaine de la [MASK] [MASK] [MASK] .
Or il apparaît que ces hommes sont tous des tenants d' [MASK] dans le [MASK] , signe qu' ils faisaient partie des hommes qu' [MASK] a amené à la bataille .
[MASK] [MASK] place l' événement à [MASK] , et [MASK] [MASK] [MASK] à [MASK] .
De plus , le rôle d' [MASK] à [MASK] est à peine mentionné dans les sources qui ne sont pas liées à [MASK] .
Les historiens concluent qu' [MASK] est le seul à avoir eu les moyens financiers de commanditer une œuvre aussi gigantesque , et qui mette en avant ses tenants et les reliques de [MASK] .
Si une majorité d' historiens s' accorde à penser que c' est bien [MASK] qui commanda cette broderie pour orner la nef de la nouvelle [MASK] [MASK] [MASK] [MASK] , inaugurée en 1077 , la discorde règne encore sur l' identité de ceux qui la fabriquèrent .
La légende dit que c' est la [MASK] [MASK] , aidée de ses dames de compagnie , qui en sont les auteurs ; pour d' autres , elle fut confectionnée : soit dans le [MASK] ; soit à [MASK] , dans le [MASK] , vingt ou trente ans après les événements qu' elle relate .
Enfin dernière des hypothèses , sa fabrication aurait été effectuée à [MASK] .
La broderie reflète le point de vue normand de l' histoire , notamment en justifiant l' invasion de [MASK] par sa légitimité au trône .
Cela dit , on s' accorde généralement à penser que ce serment eut bien lieu mais qu' il y aurait peut-être eu tromperie , puisque [MASK] aurait affirmé qu' il ne savait pas qu' il y avait de saintes reliques sous le livre sur lequel il jura .
La première moitié de la broderie relate les aventures du comte [MASK] [MASK] , beau-frère du roi [MASK] [MASK] [MASK] , dont le navire débarqua suite à la dérive des courants sur les terres du comte [MASK] [MASK] [MASK] ( dans la [MASK] actuelle ) en 1064 .
Hélas , un espion de [MASK] , visible sur la broderie , était là .
[MASK] adouba [MASK] chevalier à [MASK] .
La broderie contient une représentation d' une comète , la [MASK] [MASK] [MASK] visible en [MASK] à la fin d' avril 1066 .
En effet , chronologiquement la comète figure sur la tapisserie , placée entre la scène du couronnement de [MASK] ( janvier 1066 ) et l' annonce qui lui est faite d' une menace d' invasion par la flotte normande dont le regroupement s' effectue dès le début août 1066 à l' embouchure de la [MASK] et dans les ports environnants .
À ce sujet , on a longtemps cru que [MASK] y était représenté mourant d' une flèche dans l' œil , mais on pense , de nos jours , qu' il y a eu confusion sur la personne , le frère de [MASK] étant mort d' une flèche dans l' œil .
Toutefois , à la fin de la broderie , lorsque la bataille entre [MASK] et [MASK] fait rage , les motifs décoratifs de la frise du bas disparaissent , et la frise se remplit des cadavres des morts et des boucliers et armes tombés à terre , comme si ce " débordement " devait traduire la fureur des combats , impossibles à contenir dans la zone du milieu de la tapisserie .
La plus ancienne mention directe de la tapisserie est un inventaire des biens de la [MASK] [MASK] [MASK] , dressé en 1476 , qui en mentionne l' existence et précise qu' elle est suspendue autour de la nef pendant quelques jours chaque été .
En 1562 , des religieux , avertis de l' arrivée imminente d' une troupe de [MASK] , mirent à l' abri quelques biens .
Au moment du départ du contingent de [MASK] , on s' avisa qu' un des chariots chargés de l' approvisionnement n' avait pas de bâche .
Elle retourna à [MASK] en 1805 .
Elle fut à nouveau cachée pendant la guerre franco-prussienne de 1870 puis durant la [MASK] [MASK] [MASK] .