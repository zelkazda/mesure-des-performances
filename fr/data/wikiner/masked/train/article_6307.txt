Peu après et indépendamment , en 1928 , [MASK] a publié son propre exemple de fonction récursive mais non récursive primitive .
Sur l' infini était l' article le plus important de [MASK] sur les bases des mathématiques , servant de cœur au programme de [MASK] pour fixer la base des nombres transfinis .
[MASK] a lui-même initialement donné cette définition :
en [MASK] :
Cette fonction prend toute son utilité dans le langage de programmation [MASK] , langage très efficace dans le traitement des fonctions récursives .