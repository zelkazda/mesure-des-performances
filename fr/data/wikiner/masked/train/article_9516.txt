[MASK] [MASK] est une danseuse , actrice , réalisatrice et photographe allemande .
[MASK] [MASK] est née à [MASK] le 22 août 1902 .
À partir de 1920 , elle connaît un certain succès dans la danse et participe à diverses tournées en [MASK] , en [MASK] et en [MASK] .
Ayant acquis auprès d' [MASK] [MASK] les bases de la réalisation , du cadrage et du montage , elle se lance finalement elle-même dans la réalisation .
Ce film , qui constitue un appel à la tolérance et au respect d' autrui , des plus faibles en particulier , recevra le [MASK] [MASK] [MASK] à la [MASK] [MASK] [MASK] .
C' est aussi ce film qui attire l' attention d' [MASK] [MASK] sur la réalisatrice .
Elle réalise ainsi [MASK] [MASK] [MASK] [MASK] [MASK] .
Elle place également ses caméras de manière à créer une ambiance quasi mythologique autour des chefs du parti dans un décor conçu par le célèbre architecte [MASK] [MASK] .
1936 offre à [MASK] [MASK] , alors au faîte de sa gloire , l' événement idéal pour réaliser un grand film : les [MASK] [MASK] [MASK] [MASK] .
Pour réaliser [MASK] [MASK] [MASK] [MASK] , elle met en œuvre une technique jusqu' alors inouïe et filme pour la première fois durant les épreuves .
[MASK] [MASK] de son côté se verra décerner en 1939 une médaille d' or de la part du [MASK] [MASK] [MASK] pour ce film .
Les films de [MASK] [MASK] et l' admiration que lui porte [MASK] [MASK] vaut à la réalisatrice l' inimitié du ministre de la propagande , [MASK] [MASK] , qui , malgré son soutien officiel , voit dans l' amitié de la réalisatrice avec [MASK] [MASK] une menace pour sa propre position .
Après la [MASK] [MASK] [MASK] , en butte à la haine de ses collègues , notamment d' [MASK] , elle est placée sous la protection et la curatelle des autorités françaises d' occupation de l' [MASK] , et bénéficie de la sympathie de [MASK] [MASK] pendant sept ans .
Ce n' est qu' en 1954 qu' elle termine son film [MASK] qui sera un échec .
En 1972 , elle obtient une accréditation officielle pour couvrir les [MASK] [MASK] [MASK] [MASK] en tant que photographe .
Toujours prête à relever des défis dans le cadre de son travail , elle apprend à 72 ans à faire de la plongée sous-marine ( brevet passé en 1974 au [MASK] ) pour réaliser un film sur l' univers sous-marin .
Elle y sera interprétée par [MASK] [MASK] , qui a été préférée à [MASK] [MASK] .