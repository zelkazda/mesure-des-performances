[MASK] [MASK] est un homme politique français , né le 2 septembre 1946 , à [MASK] ( [MASK] ) .
Il est diplômé de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Membre du [MASK] [MASK] , il est :
En 1977 , il devient pour la première fois conseiller municipal , élu sur la liste d' [MASK] [MASK] , alors député -- maire de [MASK] .
Il prend la présidence du groupe d' opposition socialiste et devient le principal contradicteur d' [MASK] [MASK] après les municipales de 1989 .
Élu maire de [MASK] en 1995 face à [MASK] [MASK] .
[MASK] [MASK] , a fait face durant son mandat de maire à [MASK] à plusieurs controverses .
La création de ce stade nécessitant , en effet , le réaménagement d' une partie importante du seul grand parc grenoblois du centre ville ( le parc [MASK] [MASK] ) .
Le projet de [MASK] [MASK] , relancé en 2007 , a suscité de vives émotions chez les écologistes .
En 1985 , il est élu conseiller général de l' [MASK] , sur le [MASK] [MASK] [MASK] .
Il intervient régulièrement au sein du [MASK] [MASK] , en commission ou lors des débats publics sur les questions relatives aux relations internationales , aux inégalités nord/sud , au développement durable , aux transports , à la politique de la ville , aux enjeux énergétiques , ...
[MASK] [MASK] fut en effet un proche de [MASK] [MASK] , au [MASK] d' abord , au [MASK] ensuite , et a été au sein du [MASK] l' un des animateurs de la sensibilité rocardienne .