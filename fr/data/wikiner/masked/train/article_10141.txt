[MASK] , est la capitale de l' [MASK] et de la [MASK] [MASK] [MASK] .
La ville s' étend du nord au sud sur 20 km de longueur , à une altitude de 2850 m , sur les flancs du volcan [MASK] [MASK] .
Le centre névralgique de [MASK] se situe au nord , là où se trouve l' aéroport international [MASK] [MASK] .
La ville est située près du volcan [MASK] qui entra en éruption en 1999 .
À l' époque pré-incaïque , le site de [MASK] avait déjà une grande importance , en raison de sa situation stratégique .
Son fils , [MASK] , naquit à [MASK] .
En 1822 le général [MASK] [MASK] [MASK] [MASK] proclama l' indépendance de l' [MASK] .
En raison de sa position au cœur de la [MASK] [MASK] [MASK] , la ville a connu plusieurs tremblements de terre .