[MASK] est un langage procédural propriétaire créé par [MASK] et utilisé dans le cadre de bases de données relationnelles .
Il a été influencé par le langage [MASK] .
Il permet de combiner des requêtes [MASK] et des instructions procédurales ( boucles , conditions ... ) , dans le but de créer des traitements complexes destinés à être stockés sur le serveur de base de données ( objets serveur ) , comme par exemple des procédures stockées ou des déclencheurs .
Les dernières évolutions proposées par [MASK] reposent sur un moteur permettant de créer et gérer des objets contenant des méthodes et des propriétés .
[MASK] propose un langage procédural proche du [MASK] , le [MASK] .
Tout programme [MASK] doit se présenter sous forme de blocs .
[MASK] permet de grouper les instructions dans des procédures et des fonctions , ces termes ont la même signification qu' en [MASK] : une fonction est un bloc de code prenant des paramètres et qui effectue des traitements pour obtenir un résultat retourné , une procédure recouvre la même notion sauf qu' une procédure ne retourne pas de résultat .
D' une manière similaire aux modules de [MASK] , la création d' un paquetage requiert l' écriture d' une spécification exposant le prototype des fonctions et procédures du paquetages , ainsi que des variables et types publics .
Les premiers types de variables à disposition sont les types [MASK] supportés par le serveur [MASK] , mais il est possible de définir des types personnalisés .