La bataille navale de [MASK] opposa le 21 octobre 1805 la flotte franco-espagnole sous les ordres de l' amiral [MASK] [MASK] [MASK] [MASK] [MASK] , à la flotte britannique commandée par le vice-amiral [MASK] [MASK] .
[MASK] y trouvera la mort , mais la tactique qu' il avait mise en œuvre valut aux [MASK] une victoire totale malgré leur infériorité numérique .
Les deux tiers des navires franco-espagnols furent détruits , et [MASK] , faute d' une flotte suffisante , dut renoncer à tout espoir de conquête du [MASK] .
Cette victoire marque également la suprématie britannique sur les mers , qui allait rester incontestée jusqu' à la [MASK] [MASK] [MASK] .
Mais pour permettre à la flottille de transport de traverser la [MASK] , il doit obtenir une supériorité au moins temporaire , contre la [MASK] [MASK] .
Pour la réaliser , il lui faut rassembler ses deux flottes principales , celle de l' [MASK] , basée à [MASK] et celle de la [MASK] , alors basée à [MASK] .
Mais ces deux flottes sont sous la surveillance constante de la [MASK] [MASK] , ce qui rend leur jonction difficile .
La flotte à [MASK] , commandée par le vice-amiral [MASK] [MASK] [MASK] [MASK] , forte de vingt et un vaisseaux de ligne est étroitement surveillée par l' amiral [MASK] [MASK] et son escadre , et ne peut appareiller sans combattre .
Cependant , le vice-amiral [MASK] [MASK] , qui commande la [MASK] [MASK] qui fait face à l' escadre de [MASK] , a décidé d' appliquer un blocus relâché , car il espère inciter l' amiral français [MASK] [MASK] [MASK] à prendre la mer , et qu' il pourra ainsi livrer bataille .
Grâce à des tempêtes qui empêchèrent les navires britanniques de maintenir leurs positions de guet , [MASK] fait voile le 29 mars 1805 , s' échappe du piège de [MASK] , passe le [MASK] [MASK] [MASK] le 8 avril , et arrive aux [MASK] , le 12 mai , avec onze vaisseaux .
Le 7 juin , suite à la capture d' un navire de commerce britannique , il apprend que [MASK] et sa flotte , malgré les vents contraires qui les ont retenus , est enfin arrivé dans les [MASK] .
[MASK] décide alors d' appareiller pour retourner en [MASK] , ce qu' il fait le 11 juin .
La bataille qui suit , la bataille " des quinze-vingt " , le 23 , où [MASK] perd deux navires espagnols , dissuade celui-ci de poursuivre au nord .
Malgré l' avantage du vent , il fait demi-tour et arrive à [MASK] [MASK] le 1 er août .
Les ordres de [MASK] qui l' attendent sont clairs : voguer au nord , vers [MASK] , mais nerveux devant les démonstrations de la [MASK] , [MASK] décide de rejoindre [MASK] .
[MASK] [MASK] , revenu au [MASK] après deux ans en mer , est chargé de commander cette nouvelle flotte .
Il ne place devant [MASK] qu' une flottille de frégates sous les ordres du capitaine [MASK] [MASK] .
L' [MASK] [MASK] , de son côté , semble peu enclin à quitter [MASK] : ses capitaines s' y opposent et il craint [MASK] .
Il a reçu des ordres de l' amiral [MASK] , commandant la flotte française , de revenir en [MASK] , mais seule l' annonce de l' arrivée de son remplaçant , le vice-amiral [MASK] [MASK] , à [MASK] , le 18 octobre , ajoutée au rapport de ses espions signalant six vaisseaux britanniques à [MASK] , le décide .
Le 20 octobre , soudainement partisan du départ , il quitte le port après une rapide préparation de ses navires , et formé en trois colonnes , se dirige sur le [MASK] [MASK] [MASK] .
Le soir même , [MASK] [MASK] signale dix-huit navires britanniques à leur poursuite dans le nord-est .
Durant la nuit , [MASK] décide de former sa flotte sur une ligne et de se préparer au combat .
[MASK] [MASK] élabora un message destiné à galvaniser ses hommes , juste avant la bataille , il fit hisser par pavillons le message " [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] " ( " [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] " ) .
L' ampleur de la victoire de l' amiral [MASK] tient à sa manœuvre , consistant en un renversement de la tactique habituelle de combat en mer .
La manœuvre de [MASK] , risquée , cherchait au contraire à la destruction totale de son ennemi en tronçonnant sa flotte et en poussant à un engagement général à courte portée ( " pèle-mèle " ) .
Ces deux files forment une épée qui transperce la flotte [MASK] -- [MASK] .
Celle menée par [MASK] coupe la ligne adverse à angle droit un peu en avant de son milieu et empêche l' avant-garde de secourir le reste de la flotte franco-espagnole .
Après avoir durement touché l' adversaire en coupant sa ligne , la flotte de [MASK] écrase méthodiquement les vaisseaux désorganisés du centre et de l' arrière des [MASK] -- [MASK] .
Cependant , [MASK] compta sur la lenteur et la médiocre précision de tir des canonniers français et espagnols .
Le capitaine du [MASK] choisi alors d' aborder [MASK] [MASK] après avoir échangé des tirs avec [MASK] [MASK] .
Un combat de mousqueterie s' engage et [MASK] [MASK] prend rapidement le dessus .
En quinze minutes le [MASK] est réduit au silence .
L' [MASK] [MASK] est blessé mortellement durant cet affrontement .
Les deux navires dérivent sous le vent ce qui ouvre le passage à la poupe du [MASK] pour le reste de la colonne anglaise .
Ses caronades ravagent le pont du [MASK] , réduisant à néant les efforts de l' équipage pour s' emparer du [MASK] .
Quant à la marine espagnole , elle perdit à [MASK] l' essentiel de ses moyens .
Politiquement aussi , les résultats de [MASK] ne doivent pas être sous-estimés , constituant bientôt tant en [MASK] continentale qu' au [MASK] un contrepoids moral aux victoires terrestres de la [MASK] [MASK] .
Le [MASK] , le vaisseau amiral de [MASK] , est conservé de nos jours comme une relique .
Il fait cependant toujours officiellement partie de la [MASK] [MASK] .
L' une des places les plus célèbres de [MASK] , [MASK] [MASK] , porte le nom de la bataille .
Elle est ornée d' une statue de l' [MASK] [MASK] .
En 2005 , une série de cérémonies officielles a commémoré le bicentenaire de [MASK] [MASK] [MASK] [MASK] dans le [MASK] .
Une flotte réunissant des bateaux britanniques , espagnols et français a conduit des manœuvres navales le 21 octobre dans la baie de [MASK] , près de [MASK] , en présence de nombreux descendants des combattants de la bataille .
Chaque 21 octobre ou à une date très proche , il est de tradition , dans tous les navires de la [MASK] [MASK] , de porter un toast à la mémoire éternelle de [MASK] et de ceux qui sont morts avec lui .
[MASK] [MASK] [MASK] [MASK] est à l' origine d' une expression française : [MASK] [MASK] [MASK] .