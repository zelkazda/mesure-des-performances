Cette dernière deviendra par la suite la [MASK] [MASK] [MASK] .
Plusieurs réformes de la [MASK] [MASK] [MASK] ont progressivement contribué à démocratiser sa composition et à lui donner un rôle de plus en plus symbolique , car certaines dispositions permettent de se passer de leur consentement , mais cette procédure est peu utilisée .
De manière générale , on peut observer le déclin du bicamérisme tant quantitatif ( institution d' un parlement monocaméral en [MASK] ) que qualitatif ( phénomène du bicamérisme fonctionnel , avec une chambre haute purement réflexive en [MASK] et en [MASK] ) .
Le [MASK] [MASK] [MASK] comptait 250 membres renouvelés par tiers tous les ans .
La [MASK] [MASK] [MASK] 1830 supprima l' hérédité des pairs .
Sous l' empire de la loi constitutionnelle du 10 juillet 1940 le [MASK] [MASK] [MASK] proroge et ajourne les chambres via l' acte constitutionnel n° 3 du 11 juillet 1940 .