[MASK] [MASK] [MASK] ( [MASK] ) est une bibliothèque graphique libre pour [MASK] , initiée par [MASK] .
Cette bibliothèque se compose d' une bibliothèque de composants graphiques ( texte , label , bouton , panel ) , des utilitaires nécessaires pour développer une interface graphique en [MASK] , et d' une implémentation native spécifique à chaque système d' exploitation qui sera utilisée à l' exécution du programme .
La deuxième partie de [MASK] n' est en fait qu' une réencapsulation des composants natifs de système ( [MASK] pour [MASK] , [MASK] ou [MASK] pour [MASK] ) .
Plusieurs projets travaillent aujourd'hui sur une implémentation utilisant les composants de [MASK] .
L' environnement de développement libre [MASK] , sponsorisé lui aussi par [MASK] , repose sur cette architecture .