Le [MASK] [MASK] [MASK] est un volcan en sommeil de la [MASK] [MASK] [MASK] , dans le [MASK] [MASK] .
Il se trouve à une quinzaine de kilomètres de [MASK] et a donné son nom au [MASK] [MASK] [MASK] .
La [MASK] [MASK] [MASK] est devenue un site classé en 2000 .
Le [MASK] [MASK] [MASK] est un volcan monogénique ( une seule éruption ) explosif de type péléen en sommeil depuis environ 12000 ans .
Il repose sur un plateau granitique situé à 1000 mètres d' altitude , comme tous les volcans de la [MASK] [MASK] [MASK] .
La baisse de la pression mesurée sur un baromètre à mercure entre [MASK] et le sommet du puy permit de valider cette hypothèse .
Il existe toujours une station d' étude météorologique dépendant de l' [MASK] [MASK] [MASK] de [MASK] .
Il a été créé par les frères [MASK] et [MASK] [MASK] le 6 mars 1908 et récompensa d' une somme de 100000 francs le premier pilote qui en partant de [MASK] poserait son avion sur le sommet du [MASK] [MASK] [MASK] .
En 1956 , un pylône [MASK] , haut de 73 mètres , a été installé .
En 2006 , une étude a été lancée afin d' étudier la possibilité de réinstaller un funiculaire pour monter au sommet du [MASK] [MASK] [MASK] .
L' ascension du [MASK] [MASK] [MASK] à vélo est réservée aux cyclistes les plus entraînés .
De plus , l' arrivée du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] en 2010 rend l' activité cyclotouriste incertaine .
Entre [MASK] et [MASK] , on trouve un faux-plat d' environ 1 km .
La difficulté est progressive et elle se corse à la sortie de [MASK] avec une pente raide surtout après le dernier rond-point .
La pente reste difficile avec quelques virages mais toutefois moins raide qu' à la sortie de [MASK] .
Le [MASK] [MASK] [MASK] a accueilli plusieurs étapes du [MASK] [MASK] [MASK] .
La première arrivée eut lieu en 1952 avec la victoire indiscutable de [MASK] [MASK] .
En 1959 , c' est [MASK] [MASK] qui s' imposa en contre-la-montre en devançant [MASK] [MASK] de 1 min 26 s .
L' année la plus mémorable restera sans doute 1964 avec le mano a mano opposant [MASK] [MASK] et [MASK] [MASK] .
[MASK] [MASK] remporta l' étape mais le fait marquant fut surtout qu' [MASK] craqua dans le dernier kilomètre et [MASK] revint à 14 secondes au classement général .
En 1967 , [MASK] [MASK] s' imposa lui aussi au sommet .
[MASK] [MASK] y réalisa un doublé en 1971 et 1973 , reprenant même 15 secondes à [MASK] [MASK] en 1971 même s' il dut abandonner suite à sa chute quelques jours plus tard .
[MASK] [MASK] [MASK] gagna à son tour l' étape en 1975 mais l' événement marquant fut le coup de poing au foie donné par un spectateur à [MASK] [MASK] .
Ce fut au tour de [MASK] [MASK] d' effectuer un doublé en 1976 et en 1978 contre-la-montre .
Un nouveau contre-la-montre individuel en 1983 fut remporté par [MASK] [MASK] .
En 1986 , [MASK] [MASK] s' imposa en échappée .
Des coureurs moins connus comme [MASK] [MASK] en 1969 ou [MASK] [MASK] en 1988 ont aussi remporté l' étape du [MASK] [MASK] [MASK] .
Le [MASK] n' est plus arrivé au [MASK] [MASK] [MASK] depuis 1988 .
Le [MASK] [MASK] [MASK] est un des lieux les plus visités d' [MASK] avec près d' un demi-million de visiteurs par an .
En prenant le sentier du [MASK] [MASK] [MASK] , d' une longueur de 2 km , on trouve rapidement des marches et pontons de bois installés qui permettent de rejoindre la route ( à suivre sur 200 mètres jusqu' au sommet ) qui finira de vous amener vers le sommet .
Du haut du [MASK] [MASK] [MASK] , par temps clair , on a une vue générale sur le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] avec toute la [MASK] [MASK] [MASK] , les [MASK] [MASK] , les [MASK] [MASK] [MASK] .
On peut apercevoir à l' est toute l' agglomération de [MASK] et plus loin les [MASK] [MASK] [MASK] .
Une éruption avec coulée de lave fut reconstituée en 2002 ( 140 ans plus tard ) lors du spectacle d' inauguration de [MASK] offert par la région [MASK] .