Certains se demandent aussi si la [MASK] ne dérive pas d' un antique " arbre sacré " , ce qui expliquerait sa forme arrondie dans le [MASK] détruit par [MASK] .
Contrairement à la forme plus fréquente de la [MASK] , la tradition juive ( [MASK] dans son célèbre dessin et [MASK] dans son commentaire à [MASK] 25 , 32 ) suggère plutôt une forme selon laquelle les six branches seraient des diagonales droites .
[MASK] [MASK] écrit : " On lui a donné autant de branches qu' on compte de planètes avec le soleil " .
C' est une " imitation de la sphère céleste archétype " selon [MASK] .
[MASK] [MASK] [MASK] considérait le chandelier à sept branches comme un équivalent de la croix du [MASK] .
Aujourd'hui , la ménorah est aussi l' emblème de l' état d' [MASK] .