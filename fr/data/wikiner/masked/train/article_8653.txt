Le [MASK] [MASK] [MASK] est un circuit de golf professionnel masculin et est le plus important d' [MASK] .
Ses tournois ont principalement lieu en [MASK] , mais depuis quelques années se sont étendus à d' autres endroits du monde .
Le premier tournoi est l ' [MASK] [MASK] en 1860 .
Dans les décennies suivant la création de l' [MASK] [MASK] , le nombre de tournois dotés de prix augmente doucement mais sûrement .
Les premières années , les saisons durent six mois ( d' avril à octobre ) et sont basées uniquement en [MASK] .
Il s' agit d' une dotation en argent distribuée à la fin de la saison aux joueurs les plus méritants de l' année , toutefois seuls les golfeurs ayant joué un grand nombre de tournois du [MASK] [MASK] [MASK] peuvent recevoir une part .
Le vainqueur du [MASK] [MASK] [MASK] se voit lui octroyer cinq années d' exemption .