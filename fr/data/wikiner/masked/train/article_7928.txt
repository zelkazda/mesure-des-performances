C' est un petit village du sud-ouest de [MASK] [MASK] en pierres blanches , dans le [MASK] .
Le nom de [MASK] signifie " Bon lieu " .
[MASK] fait partie du [MASK] depuis 1808 .
En effet , avant cette date là , le [MASK] n' existait pas .
Ce fut [MASK] [MASK] qui créa ce département en détachant une portion de territoire à chacun des départements voisins .
Le 20 mars 1819 , la commune de [MASK] a fusionné avec la commune de [MASK] .
Le 20 août 1971 , une tornade s' est produite sur le village de [MASK] , engendrant de gros dommages aux habitations présentes sur la trajectoire du phénomène .