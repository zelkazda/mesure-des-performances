[MASK] [MASK] , homme politique français , né le 17 février 1961 à [MASK] .
Il est réélu député le 17 juin 2007 , pour la nouvelle législature ( 2007 -- 2012 ) , dans la circonscription de l' [MASK] ( 2e ) .
Il fait partie du groupe [MASK] .