Il est exploité par la société franco-britannique [MASK] .
C' est actuellement le deuxième plus long tunnel du monde , et il le restera jusqu' à l' ouverture du [MASK] [MASK] [MASK] [MASK] [MASK] en [MASK] , long de 57 km .
La traversée des voyageurs sans véhicule est assurée par des trains [MASK] , de type [MASK] et conçus spécialement pour cette ligne .
Des voitures-lit ont été conçues et construites pour ce service mais elles n' ont jamais été utilisées pour cela ( elles ont été revendues à l' opérateur canadien [MASK] [MASK] [MASK] ) .
La [MASK] a été pendant plusieurs siècles un rempart pour l' [MASK] .
Après plusieurs tentatives avortées , dont la dernière en 1972 -- 1975 , l' idée de creuser un [MASK] [MASK] [MASK] [MASK] fut relancée en 1984 avec une demande conjointe des gouvernements français et britannique pour des propositions de tunnels financés par le secteur privé .
Le premier projet de lien fixe entre l' [MASK] et le continent remonte à 1801 .
Cependant si cette solution évitait le problème du relief accidenté du fond de [MASK] [MASK] , des problèmes comme la pression à cette profondeur ont bloqué cette proposition .
Après plusieurs présentations , son projet est accepté en 1867 par [MASK] [MASK] et la [MASK] [MASK] .
[MASK] [MASK] [MASK] [MASK] fit suspendre le projet .
La multiplication des tunnels ferroviaires ( avec en particulier le [MASK] [MASK] [MASK] ) font qu' il semblait possible de construire un [MASK] [MASK] [MASK] [MASK] .
Des puits furent forés en [MASK] ( près de [MASK] ) et en [MASK] .
Cependant [MASK] [MASK] était désormais facilement franchissable avec le développement de l' aviation .
Un autre élément permit la relance du projet : la création de la [MASK] en 1957 .
Ce point est refusé par [MASK] [MASK] .
Le choix est entériné le 20 janvier 1986 par le premier ministre britannique [MASK] [MASK] et le président français [MASK] [MASK] .
Les trains qui utilisent le [MASK] [MASK] [MASK] [MASK] sont tractés par deux motrices électriques , chaque motrice étant suffisamment puissante pour tracter seule l' ensemble de la rame .
Le [MASK] [MASK] [MASK] [MASK] est défendu par des sapeurs-pompiers français et britanniques , équipés de véhicules spéciaux circulant dans la galerie technique .
La région du [MASK] subit régulièrement des séismes .
Le foyer se situait en mer à 37 km au nord-ouest de [MASK] , 17 km au sud de [MASK] et environ 10 km de profondeur , .
Les trains de voyageurs qui utilisent le [MASK] [MASK] [MASK] [MASK] sont exploités par [MASK] , société dépendant de la [MASK] , de [MASK] [MASK] et de la [MASK] .
Un type spécial de [MASK] a été conçu pour cette utilisation : le [MASK] [MASK] .
L' ouverture des terminaux de [MASK] et de [MASK] a permis l' ouverture de grandes zones d' activités .
Le [MASK] [MASK] [MASK] [MASK] figure dans le film [MASK] [MASK] [MASK] de [MASK] [MASK] [MASK] en 1996 où [MASK] [MASK] , grimpait sur un train à grande vitesse et est poursuivi par un hélicoptère dans ce qui est censé être le tunnel en question .
Dans le film , le [MASK] [MASK] [MASK] [MASK] apparaît comme un simple lien de section rectangulaire à double sens et les trains y circulent comme des [MASK] classiques sans caténaires .
On peut apercevoir à plusieurs reprises les bouches d' entrée et de sortie du tunnel dans [MASK] [MASK] [MASK] de [MASK] [MASK] ( 2005 ) .
Une émission du magazine de la science et de la découverte " [MASK] [MASK] [MASK] [MASK] " sur [MASK] 3 a été consacrée au [MASK] [MASK] [MASK] [MASK] .
Cette émission était intitulée " [MASK] " .
La première illustration montre le coq français et le lion britannique se serrant la main au-dessus de la [MASK] .