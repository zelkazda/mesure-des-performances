Elle commence sa carrière d' actrice de cinéma en 1966 à l' âge de quatre ans avec [MASK] [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] au côté de son père [MASK] [MASK] , avant d' enchaîner dans d' autres films de sa mère .
En 1979 , alors qu' elle a 17 ans , sa carrière d' actrice commence véritablement avec [MASK] [MASK] ( 1979 ) d' [MASK] [MASK] qui entre dans les annales du film noir grâce à l' ambiance sombre et désespérée qui en émane .
C' est dans les années 1980 qu' elle prend toute sa dimension grâce à [MASK] [MASK] .
Son timbre de voix grave et son regard profond sont mis en valeur dans [MASK] [MASK] [MASK] [MASK] -- film dans lequel elle incarne une prostituée , amie du personnage principal incarné par [MASK] [MASK] -- et dans [MASK] , dans lequel elle tient le premier rôle , un personnage d' alcoolique en rupture avec sa famille bourgeoise et qui provoque le désordre dans le couple qui la recueille .
Elle se met à la comédie avec des films comme [MASK] [MASK] ou encore [MASK] [MASK] [MASK] [MASK] , deux films de [MASK] [MASK] où elle donne la réplique à [MASK] [MASK] .
En 2000 elle est membre du jury au [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
[MASK] [MASK] a eu quatre garçons , de quatre pères différents :
[MASK] [MASK] et [MASK] [MASK] avaient eu une relation durant 18 mois .
Les secours ne furent prévenus que sept heures après les faits , alors que l' actrice était entourée de son frère et de [MASK] [MASK] .
[MASK] [MASK] a été inhumée au [MASK] [MASK] [MASK] [MASK] ( 45 e division ) le 6 août 2003 , en présence d' une assistance vêtue de clair comme l' avait demandé la famille , ont rapporté les journaux .