Sa souveraineté s' étendait du [MASK] [MASK] ( annexion du [MASK] [MASK] [MASK] en 1536 ) à l' [MASK] ( 1415 ) .
Membre de la [MASK] [MASK] depuis 1353 , [MASK] accède , après la [MASK] ( 1528 ) , au rang de puissance européenne .
Le [MASK] [MASK] [MASK] se situe dans le nord-ouest de la [MASK] , à la frontière entre la partie francophone et germanophone du pays .
Au nord avec le [MASK] [MASK] [MASK] , à l' ouest avec les cantons de [MASK] , [MASK] , et [MASK] , au sud avec le [MASK] et à l' est avec [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
La partie francophone du canton est située au nord et se nomme le [MASK] [MASK] .
Son point le plus haut est le [MASK] à 4274 m et les autres sommets majeurs sont l' [MASK] à 4195 m ainsi que la [MASK] à 4158 m .
Son point le plus bas se situe au niveau de l' [MASK] près de [MASK] ( 401,5 m ) .
Le [MASK] [MASK] [MASK] compte 962 982 habitants en 2007 , soit 12,7 % de la population totale de la [MASK] ; parmi eux , 119 930 ( 12,5 % ) sont étrangers .
Seul le [MASK] [MASK] [MASK] est plus peuplé .
Un statut particulier est accordé aux 3 districts francophones du [MASK] [MASK] ainsi qu' à la population francophone de la région de [MASK] .
Les sièges francophones garantis au [MASK] [MASK] restent inchangés par la nouvelle loi .
Il est composé de 7 membres ( les conseillers d' état ) élus au suffrage majoritaire par le corps électoral , dont un siège est garanti par la constitution cantonale à la région francophone du [MASK] [MASK] .
Le [MASK] [MASK] [MASK] compte quatorze villes de plus de 10 000 habitants au 1 er janvier 2007 :