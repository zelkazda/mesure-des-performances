Le vénérable prélat , sur les instances de la religieuse , se transporta à [MASK] avec tout son clergé et lorsqu' il fut arrivé à l' église , il fit fouiller sous l' autel jusqu' à vingt pieds en terre .
Ces sources qui nous renseignent en premier lieu sur les seigneurs qui ont régnés sur [MASK] pendant plusieurs siècles .
En effet , il avait promit de frapper [MASK] , il s' en acquitta de son gant !
Il y est clairement cité comme seigneur de [MASK] .
La forteresse de [MASK] devait avoir un rôle purement militaire , et de résidence secondaire .
Par cette vente , il démembrait la châtellenie de [MASK] .
Ce qui tend à dire que le château de [MASK] devait être important et jouait un rôle stratégique dans la région .
La mise en œuvre dans la construction du château de [MASK] peut paraître rudimentaire au premier abord .
Le château de [MASK] , malgré son statut de place forte secondaire , dévoile un dispositif d' architecture militaire efficace , en tant que château frontalier et d' observation , capable de supporter une attaque .
[MASK] est d' ailleurs une référence archéologique pour l' étude du mobilier métallique , principalement les armes de traits , comme les flèches .
Malgré sa puissante défense , le château de [MASK] fut détruit lors d' un grand siège en 1356 , en pleine guerre de cent ans .
Après dégagement des décombres et quelques restaurations sommaires , une occupation anglaise a lieu entre 1356 et 1361 dans les ruines du château de [MASK] .
Cet ouvrage expose les faits historiques touchant aux annales de [MASK] ( cartulaires , édits , chartres, … ) , dont beaucoup ont disparus durant les bombardements de la seconde guerre mondiale .
Il restitue des scènes sanglantes desquelles résultent ces charniers , dans un récit rocambolesque du siège de [MASK] en 1356 .
Ce qui explique le grand nombre de ces matériaux ( clés , pointes de flèches , fers , carreaux d' arbalètes , clous , outils de carriers, … ) trouvés sur le château de [MASK] , et servant aujourd'hui de référence aux nouvelles études universitaires .