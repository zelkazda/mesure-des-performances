Il était un partisan de l' utilitarisme , une théorie éthique préalablement exposée par son parrain [MASK] [MASK] , dont [MASK] proposa sa version personnelle .
En économie , il est avec [MASK] [MASK] l' un des derniers représentants de l' école classique .
Fils aîné de [MASK] [MASK] , il est né dans la maison parentale à [MASK] , [MASK] .
Il a été instruit par son père , sur les conseils et avec l' assistance de [MASK] [MASK] et [MASK] [MASK] .
Son père , adepte de [MASK] et défenseur de l' associationnisme , avait pour but avoué d' en faire un génie qui pourrait poursuivre la cause de l' utilitarisme et de ses applications après sa mort et celle de [MASK] .
Il n' avait pas à composer en latin ou en grec et ne fut jamais un pur scolaire ; c' étaient des matières qu' il devait lire , et à dix ans il lisait [MASK] et [MASK] aisément .
Les années suivantes , son père l' introduisit à l' économie politique par l' étude d' [MASK] [MASK] et de [MASK] [MASK] et , finalement , compléta sa vision économique avec l' étude des facteurs de production .
Ce sont les oeuvres du poète [MASK] qui , dans un premier temps , l' aident à développer une " culture des sentiments " , à faire ( re ) surgir en lui la vitalité du cœur , et l' amènent à se rapprocher de la pensée romantique .
Sa charge de travail ne semble pas avoir handicapé [MASK] dans sa vie sentimentale : la famille qu' il forma avec sa femme , [MASK] [MASK] [MASK] , et sa belle-fille [MASK] [MASK] , a été considérée par ses contemporains comme exceptionnellement réussie .
Il fut très affecté par le décès de sa femme à [MASK] , morte d' une congestion pulmonaire , et il resta depuis lors en [MASK] , pour demeurer près d' elle .
Sa défense du droit de vote des femmes , qui donna lieu à un discours notable lors de la campagne pour le [MASK] [MASK] [MASK] [MASK] , ainsi qu' à un ouvrage spécifique , eut moins de succès .
Le jeune [MASK] , enfin , traduit son livre [MASK] [MASK] et le positivisme en échange de la publication de sa thèse de médecine .
Il est enterré au [MASK] [MASK] d' [MASK] .