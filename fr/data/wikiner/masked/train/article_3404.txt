Fils d' instituteur et étudiant boursier , [MASK] [MASK] est diplômé de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et de l' [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
Il commence sa carrière administrative dans le corps préfectoral , avant d' être secrétaire général des [MASK] ( 1967 -- 1968 ) .
En mars 1982 , il rejoint le cabinet du maire de [MASK] [MASK] [MASK] , en tant que directeur général des services administratifs du département de [MASK] .
Il doit également gérer les grandes manifestations lycéennes et étudiantes contre le projet de loi réformant les universités françaises présenté par le ministre [MASK] [MASK] .
Celles-ci sont notamment marquées par la mort de [MASK] [MASK] .
Il fut aussi membre du comité d' honneur du [MASK] [MASK] [MASK] [MASK] et de l' [MASK] [MASK] [MASK] ( [MASK] ) .