L' [MASK] et [MASK] [MASK] lui attribuent le code 11 .
C' est le plus vieux crâne connu en [MASK] .
sur l' oppidum de [MASK] qui devient la capitale de la province et un port marchand très actif .
Les [MASK] envahirent le pays en 435 alors que [MASK] [MASK] , sénateur romain , était occupé à réprimer les bagaudes , des brigands de la [MASK] .
La région faisait alors partie de la [MASK] , ainsi appelée car elle se composait de sept évêchés que les rois wisigoths y avaient établis : [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
[MASK] forma un troisième comté .
En 880 , le [MASK] [MASK] [MASK] est uni par un mariage à celui de [MASK] pour n' en être plus jamais séparé .
Et face à son implantation profonde dans les comtés de [MASK] et de [MASK] , le pape [MASK] [MASK] lance en 1209 la [MASK] [MASK] [MASK] [MASK] .
Les barons du nord s' unissent pour former l' armée des chevaliers croisés sous les ordres de [MASK] [MASK] [MASK] .
La [MASK] [MASK] [MASK] devient le refuge de nombreux cathares .
En 1561 , des troubles religieux apparaissent à [MASK] sous forme de crise protestante .
Le département a été créé à la [MASK] [MASK] , le 4 mars 1790 , en application de la loi du 22 décembre 1789 , à partir d' une partie de l' ancienne province du [MASK] .
Les députés des trois sénéchaussées de [MASK] , [MASK] et [MASK] s' accordaient pour réclamer des changements quel que soit l' ordre auquel ils appartenaient .
Le [MASK] [MASK] [MASK] [MASK] apparut le 29 janvier 1790 .
En 1907 , sous l' impulsion de [MASK] [MASK] et du maire de [MASK] , [MASK] [MASK] , la crise viticole se transforme en révolte des vignerons .
Elle est limitrophe des départements des [MASK] au sud , de l' [MASK] à l' ouest , de la [MASK] au nord-ouest , du [MASK] au nord et de l' [MASK] au nord-est .
À l' est , le département est bordé par la [MASK] ( [MASK] [MASK] [MASK] ) sur 47 km .
Ils se sont formés par l' accumulation des sédiments apportés par l' [MASK] , l' [MASK] et l' [MASK] .
Le [MASK] [MASK] [MASK] est dominé par des hêtraies et des sapinières à l' étage montagnard .
On y trouve des plans d' eau comme le [MASK] [MASK] [MASK] [MASK] .
Enfin , la haute [MASK] [MASK] [MASK] [MASK] ( [MASK] ) est formée d' une ripisylve constituée de hêtres , aulnes , peupliers ou frênes .
On y trouve quelques tourbières assez rares dans le sud de [MASK] [MASK] .
Au sud , se trouvent des roches sédimentaires plissées lors de la formation des [MASK] .
À l' extrême est , près de la [MASK] , les roches sont entaillées de failles d' effondrement ( faille normale ) qui sont dues à l' ouverture du [MASK] [MASK] [MASK] .
La [MASK] [MASK] et le [MASK] au nord sont constitués de schistes et de marbre constituant la limite sud du [MASK] [MASK] .
La [MASK] [MASK] [MASK] est un pli anticlinal en forme de voûte et constitué de calcaire .
Au nord , la montagne noire et au sud le [MASK] [MASK] [MASK] sont des climats à dominante montagnarde avec des températures parfois très basses en hiver .
Au centre , dans la région limouxine , carcassonnaise et du [MASK] , le climat est dit intermédiaire avec des expositions importantes aux vents .
De l' est souffle le marin qui devient l' autan au-delà de [MASK] et en pays toulousain .
Ces vents réguliers ont permis d' installer des parcs d' éoliennes comme à [MASK] .
À [MASK] , le fleuve change de direction vers la [MASK] [MASK] à l' est , où il se jette près de [MASK] .
Le secteur primaire tenait une place importante dans le [MASK] [MASK] [MASK] [MASK] .
Dans le [MASK] , c' est l' agriculture céréalière qui domine tandis que dans la [MASK] [MASK] seul l' élevage de moutons est possible .
[MASK] est le premier port de pêche du département suivi du port de [MASK] .
85 % des embarcations de petits métiers sont destinées à la pêche en étang comme dans l' [MASK] [MASK] [MASK] .
Aujourd'hui encore , l' usine de brique ( groupe [MASK] ) de [MASK] est en pleine expansion .
Elle est surtout présente aujourd'hui dans l' arrondissement de [MASK] , notamment avec les installations portuaires et les dépôts pétroliers de [MASK] .
À partir de 1889 , la haute [MASK] [MASK] [MASK] [MASK] connaît un essor important de l' hydroélectricité .
Elle fut même le premier département dans le transport et la production d' hydroélectricité grâce aux usines d' [MASK] et de [MASK] .
Aujourd'hui , le [MASK] [MASK] [MASK] [MASK] est le premier département en ce qui concerne le nombre d' éoliennes installées .
[MASK] croissance s' explique par le retour des retraités de plus de 60 ans dans leur région d' origine et par l' arrivée d' une population immigrée issue du bassin méditerranéen .
Les deux villes principales , [MASK] et [MASK] , sont des villes moyennes regroupant seulement un tiers des habitants du département .
Deux grands axes routiers traversent le [MASK] [MASK] [MASK] [MASK] .
Du nord au sud , en suivant la côte méditerranéenne , l' autoroute [MASK] permet de rejoindre l' [MASK] vers le sud et [MASK] vers le nord .
Il est constitué d' un réseau à faible vitesse mais un projet de construction d' une ligne à grande vitesse en cours pour rejoindre l' [MASK] dans le cadre du réseau ferroviaire transeuropéen .
Le [MASK] [MASK] [MASK] [MASK] compte 367 établissements dans le premier degré ce qui représente 30055 élèves en 2005 .
Le [MASK] [MASK] [MASK] est la fête du pays audois qui se déroule durant plus de dix semaines .
L' équipe de [MASK] sera leader après la seconde guerre mondiale .
D' autres spécialités existent comme les huîtres de [MASK] et de [MASK] .
Les forteresses sont souvent situées sur des pitons rocheux , comme le [MASK] [MASK] [MASK] ou les [MASK] [MASK] [MASK] , leur donnant une position stratégique .
La [MASK] [MASK] [MASK] était le centre logistique du pays lors des conflits avec le [MASK] [MASK] [MASK] .
De nombreuses abbayes sont dispersées dans le [MASK] [MASK] [MASK] [MASK] .
La [MASK] [MASK] est une cathédrale gothique remarquable et inachevée .
Le [MASK] [MASK] [MASK] est constitué de calcaire , l' une des plus vaste zone des [MASK] , favorables à la formation de cavités .
C' est un cas unique en [MASK] .
[MASK] dernière contient le plus gros bloc d' aragonite découvert .
Ainsi , la [MASK] [MASK] [MASK] est le lieu de nombreux tournages .
En 1908 , les cinéastes délaissent les décors sur toile et [MASK] [MASK] filme devant les tours de la cité pour le retour du croisé , serment de fiançailles ou la guitare enchantée .
En 1924 , des films à gros moyens sont produits comme [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] .
En 1928 , pour le bimillénaire de la [MASK] [MASK] [MASK] , [MASK] [MASK] réalise [MASK] [MASK] [MASK] [MASK] [MASK] .
Plus récemment , le château comtal de la cité sert de décor pour [MASK] [MASK] de [MASK] [MASK] en 1992 tandis que le [MASK] [MASK] [MASK] est utilisé dans [MASK] [MASK] [MASK] de [MASK] [MASK] en 1987 et [MASK] [MASK] [MASK] de [MASK] [MASK] en 1999 .
Comme pour le cinéma , c' est la [MASK] [MASK] [MASK] qui attira les plus grands peintres .
[MASK] [MASK] est celui qui peignit le plus la cité en essayant de reproduire la cité à différentes époques .
Plusieurs de ses œuvres sont visibles au musée [MASK] de [MASK] .