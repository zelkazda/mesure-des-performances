C' est le premier film de la [MASK] [MASK] .
Il tue un à un les membres de l' équipage partis à sa recherche , à l' exception d' [MASK] [MASK] .
Lorsqu' elle sait tous ses coéquipiers morts , celle-ci enclenche le système d' auto-destruction du [MASK] et s' enfuit à bord de la navette de secours avec son chat .
Elle se croit alors en sécurité mais découvre avec horreur que l' [MASK] s' est dissimulé dans la navette .
L' esthétique torturée , obscure , cauchemardesque d' [MASK] ( le monstre , mais aussi la planète du monstre ) est fortement influencée par l' œuvre de l' artiste suisse [MASK] [MASK] .
Puis , lorsque l' alien erre dans le vaisseau des humains ( vaisseau dont le style est beaucoup plus fonctionnel , ordinaire , que celui du pilote extra-terrestre ) , c' est lui l' étranger et on comprend qu' il était déjà [MASK] dans le vaisseau écrasé .
C' est une invention du réalisateur [MASK] [MASK] .
En effet , la version longue du film [MASK] -- le huitième passager montre que la victime de l' alien se transforme petit à petit en cocon , comme l' avait imaginé [MASK] , le tout formant un cycle : l' œuf donne un " facehugger " , qui féconde une victime , qui elle-même donne naissance à un alien , qui lui-même transforme une autre victime en œuf , et ainsi de suite .
Selon l' hôte qui a porté l' alien , celui-ci aura une morphologie différente , mais il conserve 20 % de la morphologie de l' être infecté : humanoïde le plus souvent , mais aussi animal comme dans [MASK] , où le monstre , qui a été implanté dans le corps d' un chien , marche à quatre pattes ( on peut aussi citer celui implanté dans le corps d' un bœuf dans la version longue d ' [MASK] ) .
À l' origine , [MASK] [MASK] voulait travailler avec [MASK] [MASK] pour faire la musique d' [MASK] .