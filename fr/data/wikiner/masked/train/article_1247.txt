La forme juridique de la [MASK] est celle d' un établissement public à caractère industriel et commercial ( EPIC ) , qui par ailleurs détient des participations majoritaires ou totales dans des sociétés de droit privé regroupées dans le [MASK] [MASK] .
L' EPIC , employant près de 160000 personnes en mai 2009 pour un chiffre d' affaires de 17 milliards d' euros ( 2007 ) , exerce une double activité : La [MASK] exploite environ 32000 km de lignes , dont 1850 km de lignes à grande vitesse ( juin 2007 ) et 14800 km de lignes électrifiées .
Par son volume d' activité , c' est la deuxième entreprise ferroviaire de l' [MASK] [MASK] après la [MASK] [MASK] .
Le reste du [MASK] [MASK] , employant près de 68000 personnes pour 6 milliards d' euros de chiffre d' affaires , intervient dans les domaines suivants :
La [MASK] a été créée le 1 er janvier 1938 par le gouvernement de [MASK] [MASK] .
Les agents de la [MASK] ne sont pas des fonctionnaires .
Il garantit aussi l' exploitation exclusive de la [MASK] sur le réseau ferré .
La [MASK] s' engage en contrepartie à rédiger un projet industriel , à se recentrer sur le client et à rééquilibrer ses comptes .
En 1997 , la propriété du réseau est transférée à [MASK] [MASK] [MASK] [MASK] , qui reçoit également la charge de la dette liée à l' infrastructure .
La [MASK] conserve la mission d' exploiter les services de transport et la partie " commerciale " des gares , et doit acquitter à [MASK] une redevance ( péage ) pour l' utilisation des voies et de la partie " ferroviaire " des gares .
La [MASK] contrôle aujourd'hui plus de 650 filiales présentes dans des activités liées au transport de personnes ou de marchandises , à la logistique ou à des activités d' études et d' ingénierie des transports , mais parfois fort éloignées du chemin de fer .
Le [MASK] [MASK] est l' un des tout premiers groupes de transport en [MASK] .
Le site internet de la [MASK] , " [MASK] " , géré par la filiale du même nom , est le premier site marchand français .
Il est aussi le plus gros client pour la publicité sur l' internet en [MASK] .
La [MASK] a lancé en décembre 2004 un nouveau mode d' exploitation et de commercialisation des voyages [MASK] , l' [MASK] .
Confronté à une baisse de l' activité fret , qui doit être restructurée , ainsi qu' à l' ouverture des lignes à la concurrence , la [MASK] a identifié six relais de croissance ( autoroutes ferroviaires , autoroutes de la mer , transport combiné , logistique urbaine innovante , investissements dans les ports et plates-formes multimodales ) , en complément d' un fort investissement dans la rénovation des lignes de la banlieue parisienne .
Le conseil d' administration du [MASK] [MASK] est composé de 18 membres :
Le résultat courant ( y compris amortissements , provisions d' exploitation et résultats financiers ) se dégrade de 14 % par rapport à 2007 et s' élève à 980 millions d' euros pour le [MASK] [MASK] .
L' endettement net du [MASK] [MASK] s' établissait à 5,965 milliards d' euros au 31 décembre 2008 , soit une augmentation de 1,5 milliards d' euros .
Depuis 2002 , l' EPIC comme le [MASK] [MASK] connaissent des résultats positifs .
Or la possibilité pour la [MASK] de répercuter dans les tarifs aux voyageurs ces augmentations des péages reste problématique .
Un nouvel équilibre devra donc être trouvé , d' autant plus que la [MASK] sera confrontée à partir de 2010-2011 à la question du financement du renouvellement de son parc de rames [MASK] , au moment même où la concurrence sur les voyageurs internationaux sera ouverte .
On compte en particulier , à la [MASK] , sur une écotaxation du transport de fret routier pour favoriser le transport ferroviaire .
La [MASK] a bénéficié dans le passé d' un double monopole , sur le transport ferroviaire d' une part et sur le transport de voyageurs entre villes françaises d' autre part .
Le cadre juridique pour l' exercice d' une concurrence intramodale est en place en [MASK] pour ce qui concerne le transport des marchandises ( fret ) en trafic international depuis le 15 mars 2003 et en trafic intérieur depuis le 1 er avril 2006 .
Concrètement , le premier train de marchandises privé a circulé en juin 2005 pour le compte d' une filiale du groupe [MASK] .
De son côté , la [MASK] a obtenu les certificats de sécurité lui permettant de commencer à tracter ses propres trains dans certains pays voisins , notamment la [MASK] et l' [MASK] .
Cet accord a pour but de remplacer la confrontation habituelle dans l' entreprise en recherche de compromis , en mettant en place un système analogue à celui de " l' alarme sociale " qui a fait ses preuves à la [MASK] dont [MASK] [MASK] était alors la présidente .
Pour la présidente , [MASK] [MASK] , il s' agit d' un travail de deuil , en raison de la rupture du contrat social implicite entre la [MASK] et ses agents .
Au total , ces grèves auront coûté environ 300 millions d' euros à la [MASK] selon [MASK] [MASK] , avec un effet particulièrement dommageable pour le fret .
La grève à la [MASK] coûte selon [MASK] [MASK] entre 300 et 400 millions d' euros à l' économie française chaque jour .
Fin 2007 , la réforme du régime spécial de retraites est globalement acquise ; le cadre général en a été fixé par le gouvernement et l' adaptation aux spécificités cheminotes négocié dans l' entreprise par [MASK] [MASK] .
Un plan de recapitalisation , d' un montant de 1,5 milliard d' euros sur trois ans , a été engagé en 2005 avec l' accord de la [MASK] [MASK] .
Pour l' exercice 2006 , la [MASK] a annoncé un déficit courant de 250 millions d' euros , pour un chiffre d' affaires de 1,7 milliard .
Le défi à relever pour les années à venir est , selon l' ancienne présidente [MASK] [MASK] , de retrouver un niveau de compétitivité durable dans l' activité fret , en grave difficulté depuis la double concurrence de la route et des transporteurs ferroviaires privés .
L' ambition clairement affichée est de devenir un modèle et un champion [MASK] de transport multi-modal .
Selon un audit commandé en septembre 2004 par la [MASK] et [MASK] [ réf. souhaitée ] , l' état du réseau ferré français serait alarmant .
Cela représente un besoin de financement important pour le gestionnaire du réseau qui se traduira vraisemblablement par une pression accrue sur la [MASK] tant comme transporteur ( hausse des péages ) que gestionnaire délégué de l' infrastructure ( augmentation de productivité ) .
L' image d' une entreprise comme la [MASK] est un enjeu important , , , , .
L' image de la [MASK] est toutefois régulièrement altérée par les mouvements sociaux des syndicats , en conflits avec la direction , et par ses hausses de tarif , et l' abandon de lignes ou de gares .