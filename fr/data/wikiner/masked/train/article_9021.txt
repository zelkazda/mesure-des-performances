[MASK] est une des provinces du [MASK] .
La capitale est [MASK] ( [MASK] [MASK] [MASK] ) .
La province est constituée de l' île de [MASK] et de la région continentale de [MASK] , à l' extrême est du pays ( le point le plus oriental du [MASK] se trouve à quelques kilomètres de la capitale ) .
La frontière terrestre principale est avec le [MASK] .
Il y a aussi une très petite frontière terrestre avec le [MASK] sur l' [MASK] [MASK] à l' extrême nord de [MASK] .
Depuis 1964 , le gouvernement provincial utilise l' appellation " [MASK] " .
L' appellation n' est devenue courante que dans les dernières années , cependant bon nombre de gens utilisent encore " [MASK] " pour désigner et la province et l' île .
[MASK] a son propre dialecte d' anglais distinct de l' anglais canadien : l' anglais terreneuvien .
[MASK] a une longue histoire .
La première colonie européenne en [MASK] , fondée en l' an 1000 par les [MASK] , se trouvait à [MASK] [MASK] [MASK] [MASK] sur l' île .
Des restes et des artefacts provenant de cette occupation sont présentés à [MASK] [MASK] [MASK] [MASK] , maintenant classé au patrimoine mondial de l' [MASK] .
[MASK] [MASK] fonda la première colonie de l' île en 1583 à [MASK] [MASK] [MASK] .
Le fils de [MASK] [MASK] et sa femme , né le 27 mars 1613 , est le premier enfant anglais dont la naissance est connue en ce qui deviendrait le [MASK] .
Malgré ces efforts , ces plantations commerciales anglaises n' eurent pas beaucoup de succès par cause du climat et les conditions difficiles en [MASK] .
Plus tard il fonderait la colonie anglaise de [MASK] .
En 1655 [MASK] [MASK] installe un gouverneur à [MASK] , commençant une période de colonisation française formelle de [MASK] .
La colonie n' était pas une province canadienne jusqu' en 1949 et est demeurée jusqu' à cette date un dominion de l' [MASK] [MASK] ayant une existence juridique séparée du [MASK] .
[MASK] rejeta une première fois l' union avec la confédération canadienne dans l' élection générale de 1869 .
Elle demeura colonie britannique jusqu' à ce qu' elle reçût le statut de dominion en 1907 en même temps que la [MASK] .
Elle tenta de faire un accord de libre-échange avec les [MASK] mais cela échoua .
[MASK] constitua son propre régiment pour participer à la [MASK] [MASK] [MASK] afin de soutenir le [MASK] .
Le 1 er juillet 1916 , une grande partie de ce régiment fut massacrée au début de la [MASK] [MASK] [MASK] [MASK] .
En 1946 , une élection mit en place une convention nationale pour décider de l' avenir de [MASK] .
Après un débat acharné et un premier référendum à trois options en juin 1948 , un deuxième référendum en juillet 1948 décida par un vote de 52 % contre 48 % de s' unir avec le [MASK] .
La politique de la province fut dominée par le [MASK] [MASK] sous [MASK] [MASK] [MASK] jusqu' en 1972 .
[MASK] [MASK] devint premier ministre en 1979 .
En 1989 , le [MASK] [MASK] de [MASK] [MASK] prit le pouvoir .
Durant les années 1990 , [MASK] fit face à une crise écologique et environnementale sévère .
Le gouvernement fédéral aida [MASK] à hauteur d' un milliard de dollars .
En 1996 l' ancien politicien fédéral [MASK] [MASK] prit le pouvoir .
Son but principal était de faire profiter la province des riches dépôts de nickel récemment découverts au [MASK] .
Le [MASK] [MASK] s' est trouvé dans une condition affaiblie depuis ce temps , situation ressentie par l' actuel premier ministre , [MASK] [MASK] .
Un différend perdure avec la [MASK] au sujet de la frontière maritime au sud .
Les gisements de fer les plus importants au monde ont été découverts au [MASK] .
La commission fit état des éléments de tensions dans la relation entre [MASK] et le [MASK] :