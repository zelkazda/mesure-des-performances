Dans l' [MASK] [MASK] , le procédé de momification dépendait du natron , un minerai contenant des borates ainsi que d' autres sels plus communs .
Ce fut [MASK] [MASK] [MASK] qui en 1824 identifia le bore comme un élément .
Les [MASK] et la [MASK] sont les deux plus grands producteurs de bore .
La [MASK] détient près de 65 % des réserves mondiales et les [MASK] environ 13 % .
Les sources économiquement les plus importantes sont le minerai de rasorite et le minerai de borax que l' on peut trouver dans le [MASK] [MASK] [MASK] en [MASK] .
La [MASK] est un autre pays où l' on trouve de grands dépôts de borax .