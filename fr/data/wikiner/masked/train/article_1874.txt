[MASK] [MASK] , né le 15 juin 1934 à [MASK] est un humoriste , artiste de music-hall , acteur et scénariste français , pied noir .
En 1965 , il débute au music-hall au côté de [MASK] , puis se lance dans une carrière d' humoriste en formant un duo avec [MASK] [MASK] qu' il épouse en 1965 .
Il a aussi joué dans des pièces de théâtre comme [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] .
Il aurait également joué dans des pièces d' [MASK] [MASK] .
Il contribue aussi régulièrement dans l' hebdomadaire satirique [MASK] [MASK] crée par le dessinateur [MASK] dont il a pris la défense lorsque celui-ci fut accusé d' antisémitisme .