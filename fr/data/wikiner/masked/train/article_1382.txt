[MASK] est une géante gazeuse et la 7 e planète du système solaire .
[MASK] est la première planète découverte à l' époque moderne .
[MASK] [MASK] annonce sa découverte le 13 mars 1781 , élargissant les frontières connues du système solaire pour la première fois à l' époque moderne .
[MASK] est la première planète découverte à l' aide d' un télescope .
[MASK] et [MASK] ont des compositions internes et atmosphériques différentes de celles des deux plus grandes géantes gazeuses : [MASK] et [MASK] .
L' atmosphère d' [MASK] , bien que composée principalement d' hydrogène et d' hélium , contient une proportion plus importante de glaces d' eau , d' ammoniac et de méthane , ainsi que les traces habituelles d' hydrocarbures .
À l' instar des autres géantes gazeuses , [MASK] a un système d' anneaux , une magnétosphère et de nombreux satellites naturels .
Le système uranien est unique dans le système solaire car son axe de rotation est pratiquement dans son plan de révolution autour du [MASK] ; les pôles nord et sud sont situés où les autres planètes ont leur équateur .
En 1986 , les images de [MASK] [MASK] ont montré [MASK] comme une planète sans caractéristique particulière en lumière visible , sans couches nuageuses ou tempêtes existant sur les autres planètes gazeuses .
Cependant , les observateurs terrestres ainsi que le [MASK] [MASK] [MASK] , ont depuis constaté des signes de changements saisonniers et une augmentation de l' activité météorologique ces dernières années , [MASK] approchant alors de son équinoxe , qu' il a atteint en décembre 2007 .
Le vent à la surface d' [MASK] peut atteindre une vitesse de 250 m/s .
[MASK] fut observée à de nombreuses occasions avant que son caractère planétaire ne soit formellement identifié : elle est en général prise pour une étoile .
La plus ancienne mention date de 1690 lorsque [MASK] [MASK] l' observe au moins six fois et la catalogue sous le nom de [MASK] [MASK] .
L' astronome français [MASK] [MASK] [MASK] [MASK] observe [MASK] au moins douze fois entre 1750 et 1769 , notamment durant quatre nuits consécutives .
[MASK] avait entrepris une série de mesures de la parallaxe des étoiles fixes en utilisant un télescope de sa conception .
Il écrit dans son journal : " dans le quartile près de [MASK] [MASK] … soit [ une ] étoile nébuleuse soit peut-être une comète . "
Lorsqu' il présente sa découverte à la [MASK] [MASK] , il continue d' affirmer que c' est une comète mais la compare aussi implicitement à une planète :
[MASK] avertit l' astronome royal , [MASK] [MASK] , de sa découverte et reçut une réponse embarrassée de sa part le 23 avril : " Je ne sais pas comment l' appeler .
Il est aussi probable que ce soit une planète située sur une orbite autour du [MASK] presque circulaire , que ce soit une comète de trajectoire très elliptique .
Tandis qu' [MASK] continue par précaution à appeler ce nouvel objet une comète , d' autres astronomes soupçonnent sa véritable nature .
[MASK] conclut que son orbite presque circulaire ressemble davantage à celle d' une planète que d' une comète .
En 1783 , [MASK] lui-même le reconnait auprès du président de la [MASK] [MASK] [MASK] [MASK] : " Selon les observations des plus éminents astronomes européens , il apparait que la nouvelle étoile que j' ai eu l' honneur de leur faire découvrir en mars 1781 , est une planète principale du système solaire .
La période de révolution d' [MASK] autour du [MASK] est de 84 années terrestres .
Sa distance moyenne au [MASK] est d' environ 3 milliards de kilomètres .
Les paramètres orbitaux d' [MASK] furent calculés pour la première fois par [MASK] [MASK] en 1783 .
En 1841 , [MASK] [MASK] [MASK] émit l' hypothèse qu' une planète inconnue serait la cause des perturbations constatées .
En 1845 , [MASK] [MASK] [MASK] commença indépendamment ses travaux afin d' expliquer l' orbite d' [MASK] .
Le 23 septembre 1846 , [MASK] [MASK] [MASK] identifia une nouvelle planète ( qui sera plus tard nommée [MASK] ) à une position très proche de celle prédite par [MASK] [MASK] .
La période de rotation des couches intérieures d' [MASK] est de 17 heures et 14 minutes .
[MASK] est une planète géante gazeuse , comme [MASK] , [MASK] et [MASK] .
L' atmosphère d' [MASK] est composée principalement de dihydrogène à 83 % , d' hélium à 15 % , de méthane et d' ammoniac .
La couleur bleu-vert d' [MASK] est due à la présence de méthane dans l' atmosphère , qui absorbe principalement le rouge et l' infrarouge .
[MASK] possède au moins 13 anneaux principaux .
Cinq ont été découverts en 1977 grâce aux observations d' occultations d' étoiles par [MASK] .
Six autres furent observés par [MASK] [MASK] entre 1985 et 1986 .
Les deux derniers furent découverts grâce au [MASK] [MASK] [MASK] en décembre 2005 .
Il est encadré par deux satellites " bergers " , [MASK] et [MASK] .
À la différence de toutes les autres planètes du système solaire , [MASK] est très fortement inclinée sur son axe puisque celui-ci est quasiment parallèle à son plan orbital .
Elle roule pour ainsi dire sur son orbite et présente alternativement son pôle nord , puis son pôle sud au [MASK] ( même si la désignation de nord ou de sud est assez délicate dans ce cas précis ) .
Au moment du survol de la planète par [MASK] [MASK] en 1986 , le pôle sud d' [MASK] était orienté presque directement vers [MASK] [MASK] .
On peut dire qu' [MASK] a une inclinaison légèrement supérieure à 90° ou bien que son axe a une inclinaison légèrement inférieure à 90° et qu' elle tourne alors sur elle-même dans le sens rétrograde .
Une des conséquences de cette orientation est que les régions polaires reçoivent plus d' énergie du [MASK] que les régions équatoriales .
Néanmoins , [MASK] reste plus chaude à son équateur qu' à ses pôles .
Il semblerait également que l' importante inclinaison d' [MASK] entraine des variations saisonnières extrêmes dans son climat .
Le champ magnétique d' [MASK] est à peu près de la même intensité que le champ magnétique terrestre .
[MASK] possède au moins 27 satellites naturels .
Les deux premiers furent découverts par [MASK] [MASK] le 13 mars 1787 et nommés [MASK] et [MASK] par son fils , d' après des personnages du " [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] " de [MASK] [MASK] .
La plupart des autres satellites naturels connus d' [MASK] , découverts par la suite , prirent également le nom de personnages créés par le dramaturge .
Deux autres lunes , [MASK] et [MASK] , furent découvertes par [MASK] [MASK] en 1851 .
[MASK] [MASK] découvrit [MASK] en 1948 .
Dix autres lunes furent découvertes lors du passage de [MASK] [MASK] en 1986 et une autre , [MASK] , fut découverte treize ans plus tard parmi les photographies reçues .
[MASK] [MASK] la découvre le 13 mars 1781 lors d' une recherche systématique d' étoiles doubles à l' aide d' un télescope .
À la frontière des constellations des [MASK] et du [MASK] , [MASK] remarque au milieu des points-étoiles une petite tache semblant sortir de derrière la [MASK] [MASK] .
Il décide alors de prévenir la communauté scientifique de sa découverte et envoie un courrier avec les détails de sa comète au directeur de l' observatoire d' [MASK] , [MASK] [MASK] .
Il informe également l' astronome royal [MASK] [MASK] de l' observatoire de [MASK] .
Celui-ci , après avoir observé la comète et constaté qu' elle se comportait différemment des autres , conseille à [MASK] d' écrire à la [MASK] [MASK] .
[MASK] [MASK] remarque alors qu' avec son aspect de disque , elle ressemblait plus à [MASK] qu' aux 18 autres comètes qu' il avait observé .
La sonde [MASK] [MASK] est le seul engin spatial jamais envoyé vers [MASK] .
La magnitude apparente d' [MASK] évolue entre +5,3 et +6,0 .
C' est d' ailleurs en cataloguant des étoiles allant jusqu' à la limite de visibilité à l' œil nu que [MASK] [MASK] l' inventoria plusieurs fois , chaque fois sous des appellations différentes dont la plus connue est [MASK] [MASK] .
Avec un télescope de plus de 30 cm de diamètre , [MASK] apparaît comme un disque bleu pâle dont l' obscurcissement du limbe est visible .
Les plus grands satellites , [MASK] et [MASK] peuvent être perçus .
La majeure partie de cette activité ne peut pas être perçue autrement qu' avec le [MASK] [MASK] [MASK] ou de grands télescopes munis d' optique adaptative .
En 2006 , une tache sombre a été détectée dans les longueurs d' onde visibles par [MASK] .