Pendant la [MASK] [MASK] [MASK] , il participe à l' élaboration du système d' alerte radar qui a fortement contribué au succès de la [MASK] [MASK] [MASK] pendant la [MASK] [MASK] [MASK] .
La célébrité lui vient grâce à son livre [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Ce dernier est basé sur la nouvelle [MASK] [MASK] qu' il a transformée en roman à l' époque où [MASK] [MASK] en tirait un film .
Ces deux livres forment une bonne sélection des œuvres de fiction et autres de [MASK] , sélection intéressante même pour ceux qui connaissent déjà la plupart de ses livres .