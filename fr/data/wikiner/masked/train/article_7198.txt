Les naines rouges sont les étoiles les plus fréquentes au sein de la [MASK] [MASK] , représentant entre 70 et 90 % des étoiles de notre galaxie .
Leur luminosité peut être très variable mais est sensiblement inférieure à celle du [MASK] .
Celle-ci peut aller de moins de 0,001 % celle du [MASK] et jusqu' à 3 ou 4 % au maximum pour les plus volumineuses .
Ces étoiles possèdent donc une durée de vie bien plus longue que le [MASK] , estimée entre quelques dizaines et 1000 milliards d' années .
À la différence du [MASK] et d' autres étoiles plus massives , l' hélium ne s' accumule donc pas au centre de l' étoile , mais circule à l' intérieur de celle-ci .
[MASK] [MASK] [MASK] , l' étoile la plus proche de nous , ou l' [MASK] [MASK] [MASK] ( 2 e système à moins de 6 années-lumière , solitaire comme [MASK] [MASK] ) sont des naines rouges , de même que vingt autres parmi les trente étoiles les plus proches , comme l' étoile [MASK] [MASK] par exemple .