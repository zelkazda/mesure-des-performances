[MASK] est le plus souvent utilisé pour la production de fichiers plats aux spécifications particulières ( échanges entre différents systèmes d' informations hétérogènes ) .
On distingue [MASK] , la commande originale , du new [MASK] ( nawk ) , arrivée un peu plus tard sur le marché .
On trouve en général la commande [MASK] dans /usr/bin sous [MASK] .
Cependant , on peut faire des scripts en [MASK] , et le shebang ( # ! /usr/bin/awk -f ) devient faux , le script est donc inutilisable si le binaire n' est pas là où on l' attend .
Un programme [MASK] est composé de trois blocs distincts utilisables ou non pour le traitement d' un fichier .
[MASK] lit sur l' entrée ligne par ligne , puis sélectionne ( ou non ) les lignes à traiter par des expressions rationnelles ( et éventuellement des numéros de lignes ) .
" [MASK] " est aussi l' extension de nom de fichier utilisée pour les scripts écrits dans ce langage ( rarement utilisée ) .
Il existe divers programmes qui utilisent la syntaxe du [MASK] original , voici les plus connus :