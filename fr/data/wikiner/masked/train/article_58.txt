Aucune autre source documentaire ne permet de reconstituer aussi précisément les courants d' échanges de l' [MASK] ou des périodes plus récentes .
[MASK] [MASK] a vulgarisé la plongée sous-marine et l' accès aux épaves en [MASK] .
Il relate ses fouilles sur la galère de [MASK] en 1948 et au [MASK] à partir de 1952 .
En 1865 , il relie [MASK] [MASK] à [MASK] [MASK] avec une cargaison de pièces d' or estimée à 400000 $ de l' époque .
Plus de 50000 pièces de 50 cents en argent ont été retrouvées , dont certaines étaient frappées à [MASK] [MASK] ( une telle pièce vaut 100000 $ aujourd'hui ) .