C' est la plus grande île de l' [MASK] [MASK] [MASK] .
Elle contient la ville de [MASK] et environ le quart de la population du [MASK] .
L' [MASK] [MASK] [MASK] est séparée de l' [MASK] [MASK] ( [MASK] ) par la [MASK] [MASK] [MASK] .
L' île est en forme de boomerang , et son extrémité ouest sépare le [MASK] [MASK] [MASK] du [MASK] [MASK] .
Près du centre de l' île , juste au nord du centre-ville de [MASK] , se trouvent les trois hauteurs du [MASK] [MASK] .
L' [MASK] [MASK] [MASK] comptait en 2007 près de 1800000 d' habitants , dont environ les trois quarts ( 1 600 000 ) dans la ville de [MASK] .
L' [MASK] [MASK] [MASK] regroupe presque le quart de la population totale du [MASK] .
C' est la région administrative la plus peuplée du [MASK] , tout en étant l' une des deux régions les plus petites en superficie ( la plus petite en superficie étant celle de [MASK] ) .
57 % des habitants de l' [MASK] [MASK] [MASK] sont francophones , 18 % sont anglophones ( ceux-ci étant surtout concentrés dans l' ouest de l' île ) et 25 % sont locuteurs d' autres langues ( allophones ) .
L' [MASK] [MASK] [MASK] a toujours été au cœur des enjeux linguistiques au [MASK] .
En 1535 , l' explorateur [MASK] [MASK] donna à la montagne qui se trouve sur l' île le nom de [MASK] [MASK] .
En 1603 , [MASK] [MASK] [MASK] visita l' île et y séjourna .