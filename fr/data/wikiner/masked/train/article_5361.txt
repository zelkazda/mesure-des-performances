Les villages avoisinants sont : [MASK] , [MASK] , [MASK] , [MASK] , [MASK] .
En effet , ce village est sur la limite entre [MASK] et [MASK] mais très majoritairement sur cette dernière commune .
Autrefois , seulement 48 % du village était sur [MASK] [ réf. nécessaire ] .
La mairie de [MASK] y a fait mettre des pancartes d' informations concernant la commune et [MASK] a fait de même .
La forêt de [MASK] est très visitée , une dizaine de pancartes furent installées dans la forêt pour expliquer et parler de son passé minier .
La visitée est l' occasion de rencontrer : biches , renards , sangliers , plus les animaux peuplant la rivière la [MASK] .
La commune de [MASK] a un passé minier .
L' entrée de la mine et les bureaux se trouvent à [MASK] .
Ils continueront les combats deux kilomètres plus loin à [MASK] .