Selon la théorie d' [MASK] , la résolution maximum qu' il est possible d' obtenir avec un microscope optique dépend de la longueur d' onde des photons et de l' ouverture numérique du système optique .
Les travaux de [MASK] [MASK] sur les tubes cathodiques et ceux de [MASK] [MASK] sur l' optique électronique sont les contributions majeures qui permettront à [MASK] [MASK] et son étudiant [MASK] [MASK] de construire le premier microscope électronique .
Les études de [MASK] , ont montré qu' il est possible de focaliser les électrons dans une région précise de l' espace , à l' aide de champs électromagnétique .
À la suite de discussion avec les entreprises [MASK] [MASK] et [MASK] , cette dernière fût choisie et le développement commença en 1937 .
À partir de 1939 , les premiers microscopes furent fabriqués en série par [MASK] .
D' après l' hypothèse de [MASK] [MASK] [MASK] dans l' hypothèse relativiste , les électrons possèdent une longueur d' onde donnée par :
L' holographie électronique imaginé par [MASK] [MASK] en 1949 , est une technique d' imagerie qui permet d' enregistrer les figures d' interférences ( hologramme ) formées par un objet .