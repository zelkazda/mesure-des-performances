Il est souvent cité en tant que membre des [MASK] [MASK] [MASK] .
Fille d' écoutète ( prévôt ) , elle épousa à 17 ans le conseiller [MASK] , alors âgé de 38 années .
Mais [MASK] [MASK] était rien moins que soumis et la relation qu' il entretint avec ses parents se révéla souvent conflictuelle , du fait notamment de l' extrême sévérité de son père .
La relation qu' il entretenait avec ses parents , et notamment avec son père , était particulièrement conflictuelle , d' autant plus que le jeune [MASK] n' a rien d' une nature joviale .
Dans sa ville natale , [MASK] , il s' éprit de la jeune et belle [MASK] [MASK] .
[MASK] étudie le droit à [MASK] de 1765 à 1768 et à [MASK] de 1770 à 1771 .
Il y rencontre [MASK] [MASK] [MASK] , et a une idylle avec [MASK] [MASK] .
C' est en 1774 qu' il écrit le livre qui le rend immédiatement célèbre , les [MASK] [MASK] [MASK] [MASK] .
Faisant de brèves étapes à [MASK] , [MASK] , [MASK] puis , après un séjour de deux semaines à [MASK] , à [MASK] , [MASK] , [MASK] et [MASK] , il atteint enfin la [MASK] [MASK] le 29 octobre .
Il y fréquente là des artistes allemands tels que [MASK] qui peindra son portrait le plus célèbre en 1787 , ainsi que la peintre [MASK] [MASK] , et italiens , tel que le graveur [MASK] [MASK] .
Après un bref séjour à [MASK] , il se rend à [MASK] et y parvient le 2 avril après un voyage en mer difficile ( il a le mal de mer ) de quatre jours .
Il visitera de nombreux temples et ruines antiques ( [MASK] le 20 avril , [MASK] le 24 avril , le théâtre de [MASK] le 7 mai ) , mais ne portera aucun intérêt aux autres vestiges culturels de la [MASK] ( qu' ils soient byzantins , arabes ou gothiques ) .
En 1791 , il devient directeur du nouveau [MASK] [MASK] [MASK] [MASK] [MASK] , poste qu' il conserve jusqu' en 1817 .
Mais il ne peut faire autrement que d' accompagner le [MASK] [MASK] [MASK] , officier de l' armée prussienne en 1792 , lors la [MASK] [MASK] [MASK] .
En 1794 , il se lie d' amitié avec [MASK] .
Ils se connaissaient déjà depuis 1788 , mais n' avaient jusque-là que fort peu de sympathie l' un pour l' autre : [MASK] se sentant bien éloigné des préoccupations du dramaturge rousseauiste , tandis que [MASK] redoutait que [MASK] ne lui fasse trop d' ombre .
La rencontre décisive de juillet 1794 va se faire par l' intermédiaire d' amis communs , leur entretien ne portera pas sur la littérature mais sur les sciences naturelles et la philosophie , qui comptaient alors parmi les préoccupations principales de [MASK] .
L' année suivante , [MASK] termine [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , le premier grand roman de formation allemand .
[MASK] en sera l' un des tous premiers lecteurs , et donnera à [MASK] un certain nombre de remarque sur l' œuvre mais celui-ci ne tiendra finalement compte que de quelques unes d' entre elles. .
[MASK] va décéder en 1805 , provoquant l' avènement de ce que certains tiennent pour une troisième période dans la vie de [MASK] .
En 1806 , [MASK] prend la décision d' épouser [MASK] [MASK] .
En 1808 , il rencontre à [MASK] l' empereur français [MASK] [MASK] , présent dans le cadre du [MASK] [MASK] [MASK] , qui l' y décore de la [MASK] [MASK] [MASK] .
Bien que le philosophe [MASK] y ait été présent , ils attendront une rencontre en 1813 pour discuter de la théorie des couleurs élaborée par [MASK] .
En 1822 , son épouse étant décédée depuis six ans déjà , il demande en mariage [MASK] [MASK] [MASK] ( 18 ans ) , qui refuse ( il en a 73 ! ) .
, interprétées de manières bien différentes , certains y voyant le désespoir d' un grand homme de n' avoir pu amasser assez de savoir dans sa vie , tandis que d' autres , comme par exemple [MASK] [MASK] [MASK] , ne le veulent comprendre que comme une prière qu' on lui ouvrît la fenêtre , pour lui donner encore l' occasion de contempler la lumière du jour .
Dans son discours sur le centenaire de la mort de [MASK] , [MASK] [MASK] croit expliquer la versatilité sentimentale de [MASK] , et son incapacité à se donner entièrement à une seule femme , par le fait qu' en chaque femme il recherche l' éternel féminin , c' est-à-dire la beauté physique ( [MASK] ) et morale ( [MASK] ) absolue .
En 1821 , il rencontre [MASK] [MASK] , alors agé de 12 ans , et admire les talents du jeune prodige .
[MASK] a longtemps caressé l' idée de se faire librettiste d' opéra .
[MASK] [MASK] de [MASK] constitue selon lui l' idéal insurpassable vers lequel doit tendre tout opéra .
Le problème dans ces trois livrets consiste en ce que " [MASK] , qui n' est pas musicien , reconnaît nécessairement au livret une antériorité dans le temps " .
Or , le 1er mai 1786 est créé les [MASK] de [MASK] , qui va connaître un succès retentissant .
[MASK] qui tenait ce dernier opéra en assez haute estime va lui assurer une certaine popularité en écrivant dans ses annales de 1791 qu' il aurait été mis en musique par [MASK] et [MASK] .
Dans celui-ci , [MASK] tente d' établir une théorie générale sur la morphologie des végétaux en reconnaissant l' analogie de certaines formes comme les cotylédons , la forme des fleurs ou des feuilles .
Les préoccupations de [MASK] dans ce domaine étaient surtout philosophiques .
Cette réification de l' obscurité fait que la théorie de [MASK] est rejetée par la physique moderne .
Parmi les opéras inspirés par les œuvres de [MASK] , il convient notamment de nommer :
-- [MASK] , 1776 .
-- [MASK] , 1788 .
-- [MASK] , 1800 .
-- [MASK] .
-- [MASK] [MASK] , 27 janvier 1804 .
-- [MASK] .
-- [MASK] .
-- [MASK] [MASK] , 1836 .
-- [MASK] .
-- [MASK] .
-- [MASK] .
-- [MASK] .
On cite souvent , sous des formes diverses , une phrase attribuée à [MASK] : " Mieux vaut une injustice qu' un désordre " , en y voyant le comble du cynisme .
[MASK] [MASK] , professeur de philosophie à l' [MASK] [MASK] [MASK] et à l' [MASK] [MASK] [MASK] [MASK] , l' a expliquée en la replaçant dans son contexte .
[MASK] arrêta tout de suite les manifestants et , interrogé par la suite sur la raison pour laquelle il était venu au secours de ce [MASK] , il répondit par la phrase en question .