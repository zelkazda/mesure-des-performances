La [MASK] est une rivière du nord-est de la [MASK] , du [MASK] et de l' ouest de l' [MASK] et un affluent du [MASK] .
Elle prend sa source dans le massif des [MASK] à [MASK] et se jette dans le [MASK] à [MASK] en [MASK] .
Sa longueur totale est de 560 km : 314 km en [MASK] , 39 faisant frontière entre le [MASK] et l' [MASK] , 208 exclusivement en [MASK] .
Autrefois la [MASK] se jetait dans la [MASK] à hauteur de [MASK] .
La " capture " de la [MASK] a lieu au début de l' ère quaternaire .
À hauteur de [MASK] , la [MASK] est détournée de son cours par un affluent de la [MASK] et rejoint depuis ce temps le cours d' eau principal en aval de [MASK] .
Cette capture change le réseau hydrographique , le nouveau cours d' eau devient l' artère principale et la [MASK] , son tributaire .
Ce parcours tortueux , entre [MASK] et [MASK] est souvent connu sous le nom de boucles de la [MASK] .
À [MASK] , la [MASK] reçoit , en rive gauche , un petit affluent , l' [MASK] , qui coule au milieu d' une large vallée qu' il a été bien incapable d' entailler et dont la topographie montre le creusement par une rivière dont le cours était inverse à celui observé actuellement .
Lorsque la [MASK] quitte le territoire français à [MASK] , son bassin versant est alors d' environ 11500 km² .
À l' inverse , de graves étiages estivaux sont également fréquents : en août 1964 , le débit était tombé à 6,85 m³/s à la station d' [MASK] .
La [MASK] , grossie de nombreux affluents issus des hauteurs des [MASK] , se présente comme un cours d' eau franchement abondant .
Son débit a été observé sur une période de 28 ans ( 1981-2008 ) , à [MASK] , localité du département de la [MASK] située en amont de la ville de [MASK] et peu avant les frontières allemande et luxembourgeoise .
Le bassin versant de la rivière y est de 10770 km² ( soit plus de 93 % de la totalité du bassin en [MASK] ( sans la [MASK] ) qui fait 11479 km² ) .
La [MASK] y présente des fluctuations saisonnières de débit bien marquées sans être excessives , à l' inverse de bien des cours d' eau de l' est de [MASK] [MASK] , avec des hautes eaux d' hiver portant le débit mensuel moyen à un niveau situé entre 237 et 271 m³ par seconde , de décembre à mars inclus ( avec deux maxima : le plus important en janvier et le second très léger en mars ) , et des basses eaux d' été , de juillet à septembre inclus , avec une baisse du débit moyen mensuel jusqu' à 46,2 m³ au mois d' août , ce qui est encore bien confortable il est vrai .
La lame d' eau écoulée dans son bassin versant est de 434 millimètres annuellement , ce qui est élevé et largement supérieur à la moyenne d' ensemble de [MASK] [MASK] ( 320 millimètres ) et notamment supérieur d' environ 60 % à la lame d' eau de l' ensemble du bassin de la [MASK] ( plus ou moins 240 millimètres ) .
Depuis 1815 , la [MASK] sert de frontière entre le [MASK] [MASK] [MASK] et la [MASK] ( [MASK] [MASK] en 1871 ) .
À l' amont de l' embouchure de la [MASK] , à partir de [MASK] , le cours d' eau était jugé " faiblement polluée par les matières organiques et oxydables " .
Ce sont la [MASK] , l' [MASK] et la [MASK] .
Pesticides : l' atrazine a été retenu comme indicateur de pollution par pesticides des bassins de la [MASK] et de la [MASK] .
En [MASK] , un arrêté préfectoral recommande aux femmes enceintes et allaitantes et aux jeunes enfants de ne plus consommer les anguilles pêchées dans la [MASK] et ses affluents .
( Au [MASK] et en [MASK] , la consommation d' anguilles est également déconseillée , et pour le poisson blanc , le [MASK] recommande de ne pas dépasser deux portions par mois , .
La [MASK] a été canalisée à grand gabarit en 1964 et est accessible aux chalands de 3000 t jusqu' à [MASK] .
Auparavant , la navigation se faisait sur la rivière et sur des dérivations ( canal latéral à la [MASK] ) .
Le [MASK] [MASK] [MASK] qui n ' a jamais cessé d' être produit et qui possède une appellation d' origine contrôlée .
Un des tronçons les plus pittoresques de la [MASK] se trouve en [MASK] où la rivière s' écoule doucement entre les massifs de l' [MASK] et du [MASK] dans une belle vallée plantée de superbes vignobles pentus .
L' eau de la [MASK] est utilisée pour le refroidissement de la [MASK] [MASK] [MASK] [MASK] .