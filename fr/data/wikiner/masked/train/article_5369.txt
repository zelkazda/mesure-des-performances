La ville est aussi desservie par le [MASK] [MASK] [MASK] [MASK] qui permet de se déplacer n' importe où dans l' agglomération caennaise en un minimum de temps .
Elle peut accéder rapidement à l' autoroute [MASK] par l' intermédiaire du [MASK] [MASK] [MASK] , vaste pont construit au début des années 1970 enjambant une portion de l' espace portuaire de [MASK] .
L' époque moderne voient les malheurs frapper : les temps de la [MASK] voient le site dévasté , en particulier les édifices religieux .
Simple village au début des années 1960 , la commune est donc devenue la plus importante banlieue de [MASK] et la deuxième commune la plus peuplée du [MASK] .
Dans cette configuration , ce fut à [MASK] [MASK] de présider aux destinées de la ville .
[MASK] est restée un village de la [MASK] [MASK] [MASK] sans particularité quelconque jusqu' en 1963 .
Il s' agit notamment de grandes signatures contemporaines telles que [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] [MASK] .
Face à la problématique de la diversification sociale de la ville , [MASK] [MASK] prend le parti du renouvellement urbain à base de démolition-reconstruction , avec l' objectif d' introduire davantage de développement économique au cœur de la cité .
Signe de ce changement , pour l' opération immobilière " château d' eau " , construction d' une cinquantaine de logements sociaux ( appartements et maisons de ville ) , la [MASK] [MASK] [MASK] est utilisée en guise d' habillage afin de rompre avec le béton qui a longtemps caractérisé la ville .