Après des débuts hésitants entre le folk et la variété dans la deuxième moitié des années 60 et un détour par le mime , [MASK] devient une vedette en 1972 par l' intermédiaire de son alter ego décadent [MASK] [MASK] , et impose un glam rock sophistiqué et apocalyptique et des spectacles flamboyants .
À cette époque , il participe aussi aux carrières solo de [MASK] [MASK] et d' [MASK] [MASK] en tant que collaborateur et producteur .
Pendant le reste de la décennie , il s' intéresse aux musiques noires ( soul , funk et disco ) et à la musique électronique émergente , créant des mélanges nouveaux notamment avec la complicité du producteur et musicien [MASK] [MASK] .
Dans les années 80 , il devient une vedette grand public et remplit les stades avec une pop efficace , puis finit la décennie avec un revirement complet , intégrant le groupe de garage rock [MASK] [MASK] .
[MASK] [MASK] est le père du réalisateur de films [MASK] [MASK] .
[MASK] [MASK] [MASK] naît le 8 janvier 1947 à [MASK] dans le sud de [MASK] .
Sa famille s' installe à [MASK] , dans le [MASK] six ans plus tard .
Adolescent , [MASK] [MASK] s' intéresse au jazz et apprend le saxophone .
Il adopte le pseudonyme " [MASK] [MASK] " à cette époque , pour éviter la confusion avec le chanteur des [MASK] , [MASK] [MASK] .
La chanson est utilisée comme générique pour les émissions de la [MASK] consacrées à la mission [MASK] .
Ce morceau peut s' interpréter à deux niveaux , celui d' un astronaute qui largue les amarres ou celui d' un junkie , et trahit l' influence de [MASK] [MASK] .
Leur fils [MASK] [MASK] [MASK] [MASK] nait le 30 mai 1971 .
L' année 1970 voit naître l' amorce d' une collaboration avec [MASK] [MASK] , déjà producteur et bassiste du single [MASK] [MASK] , et [MASK] [MASK] , guitariste , avec lesquels il sort l' album [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] en 1971 .
Pour [MASK] [MASK] , toujours en 1971 , [MASK] [MASK] , ancien ingénieur du son des [MASK] , prend la place de [MASK] [MASK] à la production .
L' album , ponctué d' hommages explicites à [MASK] [MASK] et [MASK] [MASK] , est plus posé , piano et arrangements de cordes l' emportant .
[MASK] a aussi bien compris qu' il est désormais inutile d' attendre que la musique change le monde .
Comme l' a chanté [MASK] [MASK] dès 1970 , " the dream is over " ( " Le rêve est fini " ) .
La presse est prévenue la veille et le concert est enregistré par [MASK] à des fins commerciales .
Il sort un album de reprises de titres des années 1960 [MASK] [MASK] , produit des artistes tels que [MASK] [MASK] ou [MASK] [MASK] [MASK] et tente de mixer le [MASK] [MASK] d' [MASK] [MASK] [MASK] [MASK] , dont l' enregistrement tourne à la catastrophe technique .
Avec [MASK] [MASK] ( 1974 ) , [MASK] [MASK] a du mal à maitriser un projet dans lequel il s' embarque sans [MASK] [MASK] , jusque-là son principal collaborateur .
[MASK] [MASK] , appelé en renfort , arrive à sauver l' enregistrement de la faillite .
[MASK] [MASK] et son ambiance glauque semblent particulièrement appréciés de son auteur : il s' agit du seul album dont il supervisera personnellement la remasterisation pour l' édition CD .
Les enregistrements de la tournée américaine [MASK] [MASK] donnent le double-album [MASK] [MASK] en 1974 .
[MASK] semble dépassé par son succès et incapable de contrôler son image publique .
Dans ces conditions , la parution de [MASK] [MASK] , en 1975 , est une surprise .
[MASK] , nouvellement arrivé dans la ville de [MASK] [MASK] , fait subir à son personnage une métamorphose radicale , qui emprunte esthétiquement au cabaret allemand de l' entre-deux-guerres et musicalement aux musiques noires nord-américaines .
Deux photos tirées du film serviront aux pochettes de [MASK] [MASK] [MASK] et [MASK] .
Sorti en 1976 , [MASK] [MASK] [MASK] semble issu de séances de studios avortées pour la bande originale de [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( finalement composée par [MASK] [MASK] ) , mais la chronologie reste floue : [MASK] lui-même , à la pointe de sa toxicomanie à l' époque , a déclaré qu' il ne se rappelait même plus l' avoir enregistré .
L' album propose une forme de funk froid et roboratif , [MASK] semble de nouveau sur la corde raide .
Durant la même période , la vie personnelle de [MASK] se délite , rongée par ses abus et sombrant dans un délire mystico-totalitaire il abime son image publique avec des déclarations ambigües sur le nazisme , reniées depuis .
Influencé par l' atmosphère propre à la ville de [MASK] [MASK] et la cocaïne , le chanteur semble se perdre dans le miroir que lui renvoie son œuvre et dans la galerie de personnages qu' il incarne alors tour à tour .
Après la tempête médiatique de 1976 vient la rédemption avec la " période berlinoise " ( 1977-1979 ) , et la trilogie [MASK] , [MASK] [MASK] [MASK] et [MASK] avec [MASK] [MASK] , ancien membre de [MASK] [MASK] .
Le compositeur américain [MASK] [MASK] s' inspire de certains morceaux de [MASK] et [MASK] [MASK] [MASK] qu' il réenregistre dans les années 1990 .
Il compose et produit au cours de la même période deux albums d' [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] , avec qui il trouve le temps de jouer en concert , tenant le clavier .
[MASK] [MASK] est très proche de [MASK] et [MASK] [MASK] [MASK] dans sa conception .
Il apparaît également dans son propre rôle dans le film [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] d' [MASK] [MASK] .
[MASK] semble alors intouchable .
En 1980 , il compose , produit , et enregistre le single [MASK] [MASK] avec [MASK] .
[MASK] [MASK] aborde alors une nouvelle phase dans sa carrière avec [MASK] [MASK] [MASK] ( 1983 ) .
L' album est le deuxième " hit " de l' année 1983 derrière [MASK] .
L' album [MASK] [MASK] [MASK] [MASK] , enregistré en 1987 , se veut un retour plus rock mais ne remporte pas le succès critique et commercial escompté .
Après le dernier concert de la tournée , [MASK] fera brûler le décor .
Le ton d' ensemble est marqué par la guitare dissonnante et avant-gardiste de [MASK] [MASK] , qui collaborera avec [MASK] au-delà du groupe jusqu' en 1999 .
1989 voit les débuts de la campagne de réedition des albums de [MASK] [MASK] , indisponibles depuis longtemps .
Le 20 avril 1992 , [MASK] [MASK] participe à [MASK] [MASK] [MASK] [MASK] , le concert géant en hommage au chanteur de [MASK] décédé en novembre 1991 .
Il étonne aussi le public en récitant le [MASK] [MASK] à genoux .
Cette époque est également celle d' un tournant dans sa vie privée , concrétisé par son mariage , le 6 juin 1992 , avec le top model et femme d' affaires [MASK] .
L' album passe quasi inaperçu , sans promo et éclipsé par le [MASK] [MASK] qui sort au même moment .
En 1995 sort [MASK] [MASK] , concocté avec [MASK] [MASK] .
Pour la première fois depuis bien longtemps , [MASK] , inspiré par le rock industriel ( [MASK] [MASK] [MASK] ) et la techno , prend des risques .
[MASK] [MASK] posera d' ailleurs sa voix en 1998 sur le titre [MASK] [MASK] [MASK] [MASK] [MASK] de l' album éponyme de [MASK] .
[MASK] multiplie à cette période les collaborations .
[MASK] [MASK] participe alors au développement du jeu vidéo [MASK] [MASK] [MASK] .
C' est à cette époque , lors de la tournée 1999-2000 , que [MASK] commence à reprendre sur scène quelques-unes de ses plus vieilles chansons , de l' époque où il ne se faisait même pas encore appeler [MASK] [MASK] .
Après la sortie de [MASK] , [MASK] se lance dans sa première grande tournée mondiale depuis 1997 , baptisée [MASK] [MASK] [MASK] .
Le chanteur et son groupe entament alors une tournée des festivals d' été en [MASK] , mais les quinze dernières dates sont annulées lorsque [MASK] subit en urgence une angioplastie .
Il n' y a eu ni nouveau disque studio ni concert de [MASK] [MASK] depuis cette date .
Il enregistre néanmoins quelques duos , notamment sur les disques de jeunes groupes , et fait quelques apparitions sur scène , avec le groupe canadien [MASK] [MASK] , pour des concerts à but caritatif , ou en hommage à [MASK] [MASK] aux côtés de [MASK] [MASK] .
Le mois de juin 2008 voit la parution du [MASK] [MASK] [MASK] [MASK] .
En 2010 paraît [MASK] [MASK] [MASK] , double album retraçant la tournée 2003 de [MASK] .
[MASK] expose : " Ce n' est pas quelque chose avec lequel j' étais confortable du tout . "