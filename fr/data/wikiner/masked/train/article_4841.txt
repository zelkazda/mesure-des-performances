Le [MASK] , est l' acronyme hébreu désignant [MASK] [MASK] hébraïque , formée de trois parties :
On écrit aussi [MASK] ( sans h à la fin ) .
Le [MASK] est aussi appelé [MASK] [ מקרא , approx .
Les livres inclus dans le [MASK] étant pour la plupart écrits en hébreu , on l' appelle également [MASK] [MASK] hébraïque .
Ces vingt-quatre livres sont les mêmes livres que ceux de l' [MASK] [MASK] protestant , mais l' ordre des livres est différent , ainsi que l' énumération , les protestants comptant trente-neuf livres , et non vingt-quatre .
On parlera plutôt de " [MASK] [MASK] " pour marquer la révérence due à la tradition juive .
En tant que tel , une distinction technique peut être tracée entre le [MASK] et le corpus similaire mais non identique que les chrétiens protestants nomment [MASK] [MASK] .
Ils sont appelés [MASK] [MASK] ( lit .
La [MASK] [ également connue sous le nom de [MASK] ] se constitue de :
Les [MASK] ( כתובים ) consistent en :
Elle a néanmoins été ajoutée dans la plupart des éditions modernes du [MASK] , afin de faciliter la localisation et la citation de ceux-ci .
Cependant , dans de nombreuses éditions juives du [MASK] publiées au cours des quarante dernières années , il s' est produit une tendance notable à en minimiser l' impact sur les pages imprimées .
Le judaïsme rabbinique enseigne que la [MASK] fut transmise en parallèle avec une tradition orale qui la complète .
Selon les tenants de la loi orale , de nombreux termes et a définitions utilisés dans la loi écrite ne sont pas définis dans la [MASK] elle-même , ce qui suppose de la part du lecteur une familiarité avec le contexte et le détail , lesquels ne pourraient être connus que via une antique tradition orale .