Ville assise à l ' altitude de 468 m sur les terres du [MASK] .
Elle borde la [MASK] et son confluent avec la [MASK] .
Aux distances de : 830 km de [MASK] , 100 de [MASK] , 14 de [MASK] .
[MASK] 38 km , [MASK] et [MASK] 55 km , [MASK] 70 km , [MASK] 70 km , [MASK] 90 km , [MASK] 200 km , [MASK] 230 km , [MASK] 150 km .
Les communes avoisinantes ( juxtaposées ) sont : [MASK] , [MASK] .
[MASK] se trouve à proximité de [MASK] , de sa [MASK] [MASK] [MASK] [MASK] , et du site de l' ancienne ville romaine de [MASK] [MASK] .
Aéroport : [MASK] [MASK] .
Autobus pour [MASK] , [MASK] , [MASK] et l' [MASK] .