Les [MASK] [MASK] étaient des organisations internationales régionales qui avaient la particularité d' avoir des institutions communes .
Les [MASK] [MASK] formaient l' un des trois piliers de l' [MASK] [MASK] .
Les [MASK] [MASK] étaient notamment membres de l' [MASK] et de l' [MASK] .
La [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) a pour but de combiner les industries du charbon et de l' acier de ses membres pour créer un marché commun autour de ces ressources .
Le [MASK] [MASK] de cette communauté avait une validité de 50 ans , et il expira en 2002 .
Elle fut fondée par les membres de la [MASK] , en 1957 , grâce au [MASK] [MASK] [MASK] .
La [MASK] avait pour but de créer une union douanière et une coopération économique .
Contrairement à la [MASK] , la [MASK] n' avait pas de limite de validité et continue à exister .
Du fait de la sensibilité de l' opinion publique en matière de nucléaire , le traité n' a pas été amendé depuis sa signature et n' aurait pas été changé par la [MASK] [MASK] bien qu' elle tentait d' abroger les autres traités .