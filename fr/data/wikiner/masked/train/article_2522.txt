La culture est , selon le sociologue québécois [MASK] [MASK] , " un ensemble lié de manières de penser , de sentir et d' agir plus ou moins formalisées qui , étant apprises et partagées par une pluralité de personnes , servent , d' une manière à la fois objective et symbolique , à constituer ces personnes en une collectivité particulière et distincte . "
( [MASK] [MASK] , 1969 , 88 ) .
[MASK] fut le premier à appliquer le mot cultura à l' être humain : Un champ si fertile soit-il ne peut être productif sans culture , et c' est la même chose pour l' humain sans enseignement. .
La définition que peuvent en faire les gouvernements lorsqu' ils fixent sa mission au [MASK] [MASK] [MASK] [MASK] diffère de celle que l' on en donne dans les sciences humaines ou de celle qui correspond à la culture générale de chacun d' entre nous .
Mais la culture n' est pas réductible a son acception scientifique , car , comme l' indique la définition de [MASK] [MASK] , elle concerne les valeurs a travers lesquelles nous choisissons aussi notre rapport a la science .
[MASK] [MASK] donne une division légèrement différente , en mentifacts , socifacts et artifacts , pour des sous-systèmes idéologiques , sociologiques , et technologiques respectivement .
La socialisation , du point de [MASK] , dépend du sous-système de croyance .
On notera qu' en [MASK] , la langue française a le statut de langue officielle , et qu' à ce titre , elle est la langue de l' administration et du droit civil .
Aux [MASK] , il existe une tradition normative très importante en matière industrielle et financière .
Les normes comptables en [MASK] sont actuellement assez largement inspirées des normes américaines .
De là est née , en [MASK] et dans la plus grande partie de l' [MASK] , une tradition qui lie la culture avec les institutions publiques .
Aux [MASK] , il n' existe pas une emprise aussi importante de la puissance publique sur la culture proprement dite .
Ainsi , depuis les années 1950 , l' industrie américaine du cinéma , concentrée à [MASK] , domine non seulement économiquement mais aussi symboliquement , la distribution des films à grand succès et la consécration des grandes vedettes .
La [MASK] a été l' une des premières démocraties modernes à se doter d´un [MASK] [MASK] [MASK] [MASK] en 1959 .
Elle fut suivie par de nombreux autres pays en [MASK] mais selon des formules adaptées à leur contextes respectifs .
L´Espagne s´est quant à elle dotée d´un [MASK] [MASK] [MASK] [MASK] en 1978 , dès que la page du franquisme fut tournée .
Le [MASK] [MASK] constitue un exemple des plus intéressants dans la prise en compte de l´action étatique en faveur de la culture .
De ce point de vue , le [MASK] se distingue des [MASK] , les traditions culturelles des deux pays étant assez distinctes .
En [MASK] , on trouve quelquefois aussi des institutions privées ( châteaux privés , [MASK] [MASK] [MASK] , le [MASK] [MASK] [MASK] ) qui sont issus le plus souvent d' initiatives régionales , même si leur rayonnement est souvent national .
Néanmoins , les [MASK] admirent le patrimoine culturel européen , car il s' agit de leurs racines culturelles : on le constate dans les acquisitions des œuvres d' art , dans leur présence dans les lieux artistiques ( [MASK] , [MASK] , [MASK] , [MASK] … ) , dans les mécénats américains pour la restauration de quelques éléments symboliques du patrimoine européen ( [MASK] [MASK] [MASK] … ) , dans les échanges musicaux ( chefs d' orchestre … ) , etc .
Le patrimoine de [MASK] [MASK] noire est aussi redécouvert ( arts premiers ) .
[MASK] [MASK] est probablement [ réf. souhaitée ] l' un des seuls pays au monde où la langue parlée ( et officielle ) est soutenue par un système d' académies , qui en contrôlent le bon usage .
L' [MASK] [MASK] fut fondée dans ce sens par [MASK] en 1635 .
[MASK] [MASK] a notamment développé la thèse selon laquelle la technique s' auto-accroît , imposant ses valeurs d' efficacité et de progrès technique , niant l' homme , ses besoins , et notamment sa culture .
Dans la définition que donne l' [MASK] du patrimoine culturel immatériel , la diversité culturelle apparait comme un élément déterminant :
Dans les dernières décennies , de nombreux philosophes se sont inquiétés des rapports avec la nature ( [MASK] [MASK] , [MASK] [MASK] … ) .
Selon la philosophie moderne , et en particulier dans le sillage de [MASK] [MASK] , on considère que la culture est naturelle à l' homme , en tant que tous les hommes en ont une et qu' un quelconque " état de nature " ( état pré-culturel ) ne serait que pure fiction .
Pour ce thème , voir l' article [MASK] [MASK] .
Par leurs recherches , [MASK] [MASK] et ses collaborateurs ont montré que , dans des cas d' erreur sur la détermination du sexe à la naissance résultant d' une anomalie biologique non apparente , des forces de la nature agissent " sur les attitudes et comportements d' un enfant à travers ses jeux , son habillement , ses choix de pertenaires de jeu , etc. , autrement dit , que l' inné peut influencer l' acquis. "
Depuis la chute du [MASK] [MASK] [MASK] ( 1989 ) , on tend ainsi à voir apparaître un modèle prédominant , le modèle anglo-saxon réputé " libéral " , mais où , en fait , on trouve un engagement très fort de la puissance publique américaine dans l' industrie de l' armement et l' industrie informatique .
L' [MASK] a établi en 1972 une liste du patrimoine mondial , composée de plusieurs centaines de sites dans le monde .
En 1997 , la notion de patrimoine oral et immatériel de l' humanité a été définie par l' [MASK] .
Ce type d' activité est très naturel aux [MASK] , où les relations entre entreprises et ONG s' établissent facilement .
Les ONG culturelles peuvent pourtant favoriser l' éducation dans les pays en développement ( en [MASK] par exemple ) , et renforcer les liens .
Comme l' a montré l' anthropologue britannique [MASK] [MASK] , aucune culture humaine n' est " homogène " : elle résulte toujours d' une différenciation interne entre partisans ( ou adeptes ) de valeurs plus individualistes , de valeurs plus collectives , de solutions organisationnelles hiérarchiques et enfin de formes de résistance passive ou active à toutes les valeurs en vigueur .
Ainsi l' histoire des cultures ( à commencer par celle des mythes étudiés par [MASK] [MASK] ) est-elle celle d' une sorte de " course-poursuite " entre différentes façons de " prendre la vie " .
[MASK] [MASK] pense que la transmission de la culture comporte une forte composante de croyance et de sacré .
) , les voyages de missionnaires et d' explorateurs , le commerce à partir de [MASK] ( villes hanséatiques et relations maritimes avec le sud de l' [MASK] ) , le protectorat français au [MASK] …
C' est par ce type d' échanges que de nombreux traités scientifiques et philosophiques sont parvenus en occident , depuis la [MASK] [MASK] , l' [MASK] , la [MASK] , l' [MASK] , ainsi que des techniques très utiles : boussole , sextant , informations cartographiques , papier , imprimerie , chiffres " arabes " …
Elle comprend aussi une dimension de structuration de l' esprit , vis-à-vis de l' ensemble des connaissances : La culture est ce qui reste lorsque l' on a tout oublié ( attribué en général à [MASK] [MASK] ) .