[MASK] [MASK] , né le 29 mai 1935 à [MASK] ( [MASK] ) , est un homme politique français .
Il est député de la 4 e circonscription du [MASK] depuis 1986 et maire de [MASK] depuis 1989 .
Il est membre de l' [MASK] et de [MASK] [MASK] .
Le 17 septembre 2006 , il se déclare , dans la presse régionale alsacienne , candidat à la [MASK] [MASK] [MASK] ( 2007 -- 2012 ) dans sa circonscription qui lui est favorable .
Il accorde tout de même sa signature au candidat investi par l' [MASK] , [MASK] [MASK] .
Il soutient l' amendement de [MASK] [MASK] ( avec [MASK] [MASK] ) sur l' expression des langues régionales sur [MASK] [MASK] ( [MASK] [MASK] particulièrement ) .