La région s' appelle le [MASK] .
L' agglomération de [MASK] est traversée par la [MASK] , un affluent rive gauche de l' [MASK] , à 40 km au sud de [MASK] , entre [MASK] , [MASK] et [MASK] .
Les communes limitrophes sont : [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] .
[MASK] est la ville la plus méridionale de [MASK] , à la limite avec l' [MASK] .
La ville de [MASK] est une commune entièrement urbanisée .
Depuis la création de la [MASK] [MASK] [MASK] [MASK] [MASK] un certain nombre de décisions sont partagées avec les 35 autres communes de l' agglomération .
Les axes de communication sont nombreux à [MASK] et font d' elle une ville carrefour .
La ville de [MASK] dispose d' une [MASK] [MASK] permettant de relier [MASK] en une heure grâce au [MASK] ( 8 allers-retours origine [MASK] ) mais aussi des liaisons directes vers [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] [MASK] ( uniquement les samedis estivaux ) , [MASK] , [MASK] ( [MASK] ) , [MASK] , [MASK] ( [MASK] ) , et les [MASK] pendant la saison hivernale à savoir la [MASK] ( [MASK] ) , la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) et la [MASK] ( [MASK] ) .
Les douaisiens peuvent également rejoindre [MASK] les samedis d' été .
[MASK] est à une demi-heure en [MASK] .
[MASK] était le nom d' une villa gallo-romaine .
La première mention de [MASK] date de 930 .
Le comte [MASK] [MASK] [MASK] [MASK] [MASK] érige vers 950 le premier lieu de culte : la [MASK] [MASK] .
C' est sur un exemplaire de cette bible dite de [MASK] que [MASK] [MASK] [MASK] a prêté serment lors de son investiture présidentielle .
[MASK] était alors une cité très riche et réputée pour son industrie lainière .
En 1667 , le roi de [MASK] [MASK] [MASK] envahit la [MASK] .
[MASK] fut assiégée et prise en trois jours , du 23 au 26 juillet 1667 par [MASK] , qui assiège simultanément [MASK] .
Le [MASK] [MASK] [MASK] ( 1668 ) annexa la [MASK] à la [MASK] , et , mis à part durant les deux guerres mondiales , [MASK] est restée française depuis .
Entre 1790 et 1794 , [MASK] a absorbé [MASK] aujourd'hui dont une partie est mise en [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Le chef-lieu du nouveau [MASK] [MASK] [MASK] fut établi à [MASK] en 1790 mais déplacé en 1803 à [MASK] .
En 1804 la préfecture du [MASK] [MASK] [MASK] établie à [MASK] est transférée à [MASK] .
En 1878 , l' école des maîtres ouvriers mineurs , future [MASK] [MASK] [MASK] [MASK] [MASK] est implantée à [MASK] .
Il donne lieu à d' importants aménagements urbains dont la création de la [MASK] [MASK] [MASK] sur l' ancienne place du marché aux bêtes .
En 1895 , le canal de la [MASK] est ouvert , et [MASK] devient le second nœud fluvial français après [MASK] .
[MASK] subit d' importants dommages pendant les deux guerres mondiales .
[MASK] a profité du développement de l' industrie liée au charbon , mais subit ses séquelles , et en particulier les affaissements miniers qui nécessitent des pompages continuels afin que la ville ne soit pas noyée .
[MASK] , en 1985 , a inauguré la nouvelle place aux jets d' eau .
[MASK] conserve des vestiges de son passé militaire , par ses fortifications , mais aussi son arsenal , sa fonderie de canons , ses casernes .
La construction du beffroi de [MASK] a été entreprise en 1380 , sur l' emplacement d' une précédente tour en bois , afin de servir de tour de guet .
Le dessin de [MASK] [MASK] est conservé aujourd'hui dans son musée de la [MASK] [MASK] [MASK] à [MASK] .
Elle fut vendue à la [MASK] puis rasée en 1798 .
L' [MASK] [MASK] dont l' existence est attestée dès 1175 a été constituée en paroisse en 1257 .
En 1667 , [MASK] devient ville française .
[MASK] [MASK] décide d' en faire un centre militaire important .
Des bâtiments de la fonderie de [MASK] [MASK] , il ne reste aujourd'hui que le mur circulaire et le portail d' entrée .
De nombreux canons en bronze sont sortis de la fonderie de [MASK] .
Certains sont visibles en [MASK] ( [MASK] [MASK] [MASK] ) , en [MASK] ( [MASK] [MASK] [MASK] ) , en [MASK] , [MASK] , [MASK] , et même aux [MASK] .
Chaque canon est une pièce unique ayant une carte d' identité avec l' inscription de son calibre , son poids , son nom , celui du fondeur , les armes de [MASK] .
Il pèse 2050 kg et porte les armes de [MASK] [MASK] [MASK] et un soleil , emblème du roi .
Le parc porte le nom de [MASK] [MASK] qui fut maire de [MASK] de 1896 à 1919 .
L' ancien port charbonnier des [MASK] a été transformé en parc de 21 hectares dont 5 de plan d' eau .
70 hectares de forêt pour la protection des eaux souterraines de la vallée de l' [MASK] .
Les cinq chapelles latérales sont consacrées à la présentation des objets d' art dont l' orfèvrerie médiévale , une série de bronzes et de terres cuites de [MASK] [MASK] [MASK] , originaire de [MASK] .
Autrefois siège des [MASK] [MASK] [MASK] , [MASK] a dû se reconvertir dans les années 1980 , notamment avec l' implantation d' une usine [MASK] et de l' [MASK] [MASK] .
[MASK] possède une antenne terriotriale de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Elle gère le port fluvial de [MASK] .
[MASK] [MASK] a d' ailleurs visité cette société lors de sa campagne présidentielle en 2007 .
L' évolution du nombre d' habitants depuis 1793 est connue à travers les recensements de la population effectués à [MASK] depuis cette date :
Pour cette occasion , une fête foraine à lieue depuis une centaine d' année sur la [MASK] [MASK] [MASK] .