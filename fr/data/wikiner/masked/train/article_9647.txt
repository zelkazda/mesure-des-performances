[MASK] , ou [MASK] en suédois , est la capitale et la plus grande ville de la [MASK] totalisant 584420 habitants dans la commune et 1322757 dans le [MASK] [MASK] .
Il y a plus de 130 nationalités résidants dans la ville d' [MASK] .
[MASK] est un port marchand ouvert sur le [MASK] [MASK] [MASK] .
La ville s' appelait à l' origine [MASK] ( [ hɛlsiŋ'fɔʂ ] , qui reste aujourd'hui encore le nom officiel en suédois .
Une autre version attribue le nom aux immigrants suédois venus de la province de [MASK] .
[MASK] fut fondée sous le nom d' [MASK] en 1550 par [MASK] [MASK] , roi de [MASK] dans le but de concurrencer [MASK] , alors ville hanséatique florissante .
Après la la défaite de la [MASK] dans sa guerre contre la [MASK] de 1808 -- 1809 , la [MASK] est annexée à l' [MASK] [MASK] .
La ville connaît dès lors un développement considérable , le centre-ville est reconstruit selon les plans de [MASK] [MASK] [MASK] , l' [MASK] [MASK] [MASK] est déplacée vers la nouvelle capitale et deviendra l' [MASK] [MASK] [MASK] .
Pendant la [MASK] [MASK] [MASK] , la forteresse de [MASK] est en partie détruite par une expédition franco-anglaise .
Contrairement à [MASK] , [MASK] a subi relativement peu de dégâts pendant la guerre .
Après la victoire des blancs , de nombreux ex-rouges ont été mis dans des camps de prisonniers , le plus grand camp avait quelques 13 300 prisonniers et était situé sur l' île forteresse de [MASK] .
Des architectes renommés , comme [MASK] [MASK] ont créé des plans pour la ville , mais ils n' ont jamais été pleinement réalisés .
En 1917 , à l' indépendance de la [MASK] , [MASK] devient le nouveau nom officiel de la ville qui est aussi capitale de la nouvelle république .
La commune d' [MASK] s' étale sur des baies , des péninsules et des îles plus ou moins grandes .
Elle est classée comme très peu peuplée en comparaison à d' autres capitales européennes comme [MASK] , [MASK] ou [MASK] .
Une grande partie d' [MASK] se situe en dehors du centre-ville et se compose de quartiers d' après-guerre séparés les uns des autres par des îlots de forêts .
Le maire d' [MASK] , est nommé par le conseil municipal .
Le poste est actuellement occupé par [MASK] [MASK] .
Au 31 mars 2010 , la commune d' [MASK] comptait 584420 habitants ( 10,8 % de la population de la [MASK] ) , la région capitale ( avec [MASK] , [MASK] et [MASK] ) compte 1027635 d' habitants ( 19,2 % de la population finlandaise ) et le [MASK] [MASK] , regroupant treize communes compte 1322757 d' habitants .
La ville a la plus forte population immigrée de la [MASK] en termes tant absolus que relatifs .
Il y a plus de 130 nationalités résidants dans la ville d' [MASK] .
Les communautés les plus importantes sont les [MASK] , les [MASK] , les [MASK] , mais aussi un nombre important de [MASK] , de [MASK] , de [MASK] , d' [MASK] , d' [MASK] et d' [MASK] .
La commune d' [MASK] est découpée en subdivisions de tailles et de natures variées .
Les quartiers d' [MASK] sont la subdivision la plus ancienne de la commune .
Au 1er janvier 2009 , suite à l' annexion , la commune d' [MASK] s' est agrandie et ses quartiers sont désormais au nombre de 59 .
( L' agrandissement de la ville d' [MASK] aprés la fusion du 1er janvier 2009 ne figure pas sur la carte )
Les transports publics font en général l' objet de vifs débats dans la politique locale d' [MASK] .
En 2000 , 210 millions de voyages furent effectués , dans le réseau de transports publics d' [MASK] dont 47,6 % par bus , 27 % par trams , 24,7 % par métro et 0,7 % grâce aux ferries .
Cette ligne de métro sera prolongée dans les années à venir vers l' ouest jusqu' à [MASK] et à l' est dans la direction de [MASK] .
[MASK] est reliée par ferry à [MASK] ( [MASK] ) , [MASK] ( [MASK] ) , [MASK] et [MASK] ( [MASK] ) , totalisant un trafic de 8,5 millions de passagers .
Chaque hiver , le port d' [MASK] est pris dans les glaces .
Les liaisons maritimes quotidiennes avec les grandes villes de la [MASK] sont assurées grâce à l' action d' une flotte de brise-glace .
[MASK] dispose de deux aéroports : l' [MASK] [MASK] [MASK] et l' [MASK] [MASK] [MASK] .
L' [MASK] [MASK] [MASK] est le principal aéroport international de [MASK] .
Il est situé à 5 km du centre de la ville de [MASK] et à 15 km au nord du centre de la capitale [MASK] .
En 2007 il a vu transiter 13090744 passagers soit plus de 75 % du total des voyageurs ayant transité par un aéroport du pays , et près de 16 fois plus que le deuxième aéroport du pays , l' [MASK] [MASK] [MASK] .
Jusque 1952 , c' était le principal terrain d' aviation d' [MASK] .
[MASK] compte 8 grands établissements d' enseignement supérieur , totalisant 50000 étudiants :
[MASK] est le principal centre économique de la [MASK] .
On y trouve la [MASK] [MASK] [MASK] , et le siège de la plupart des banques et compagnies d' assurances finlandaises .
Le port d' [MASK] est le principal port de [MASK] que ce soit pour le transport de marchandises ou les transport de passagers ( 8,5 millions de passagers ) .
[MASK] est le premier centre industriel du pays et possède une industrie diversifiée : chantiers navals ( [MASK] [MASK] ) , industries chimiques et agro-alimentaires , imprimeries , usines textiles , fabriques de liqueurs et de porcelaine .
La zone métropolitaine d' [MASK] a la meilleure disponibilité pour les travailleurs hautement qualifiés en [MASK] , et de bonnes infrastructures et des systèmes de soutien aux entreprises .
L' amélioration de l' économie d' [MASK] et de la coopération entre les municipalités de l' agglomération d' [MASK] sont considérés comme les principaux défis futurs pour le développement économique de la région .
Le plus grand musée historique à [MASK] , est le [MASK] [MASK] [MASK] [MASK] , qui affiche une vaste collection historique de la préhistoire au 21ème siècle .
[MASK] compte 79 musées , 45 cinémas , et 12 théâtres .
Les principaux orchestres de la ville sont l' [MASK] [MASK] [MASK] [MASK] fondé en 1882 et l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] fondé en 1927 .
[MASK] , [MASK] dispose du plus grand parc des expositions .
Sa principale salle est l' [MASK] [MASK] qui a servi a accueillir de nombreux événements comme le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , des concerts et des spectacles à grands budgets et des événements sportifs .
Le tourisme à [MASK] est largement dominé par la culture .
Le classicisme a donné visage au centre ville comme par exemple à la cathédrale et à l' [MASK] [MASK] [MASK] .
Par rapport à son histoire riche , il est nécessaire de visiter les petites îles de [MASK] ( [MASK] en suédois ) , ancienne fortification dans le port de la ville ayant été construite par les [MASK] pour défendre la ville et la [MASK] , notamment contre les [MASK] .
[MASK] compte 44 hôtels totalisant 6 838 chambres , ainsi que 876 restaurants .
[MASK] compte :
[MASK] n' a pas de villes jumelées à proprement parler .