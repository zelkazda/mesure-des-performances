Elle est située au nord-est de la [MASK] [MASK] et a pour capitale [MASK] .
Elle couvre une superficie de 31950 km² ( 6 % de la superficie de [MASK] [MASK] ) et comptait 7,5 millions d' habitants en 2010 ( 17 % de la population espagnole ) .
La [MASK] est entourée par la [MASK] [MASK] au sud , l' [MASK] à l' ouest , la [MASK] au nord , l' [MASK] au nord-ouest , et la [MASK] [MASK] à l' est .
Les trois plus grandes villes sont [MASK] , [MASK] [MASK] [MASK] [MASK] et [MASK] .
La [MASK] est divisée en 41 comarques , administrations locales supra-municipales ( dont la taille moyenne se situe entre celle des cantons français et celle des arrondissements français ) .
Une autre théorie voit dans le mariage de [MASK] [MASK] [MASK] [MASK] et de [MASK] [MASK] [MASK] l' origine de l' écu aux quatre pals .
La théorie principale suggère que [MASK] provient de l' expression " terre de châteaux " , ayant évolué vers le terme castlà , le dirigeant d' un château .
Le climat de la [MASK] est varié .
Les zones peuplées et situées sur la côte de [MASK] , [MASK] et [MASK] sont caractéristiques d' un climat méditerranéen .
L' intérieur ( y compris la [MASK] [MASK] [MASK] et la partie intérieure de la [MASK] [MASK] [MASK] ) abrite principalement un climat mi-méditerranéen mi- continental .
Les pics des [MASK] sont caractéristiques d' un climat montagnard , voire d' un climat alpin sur les plus hauts sommets .
Il neige souvent dans les [MASK] , et il arrive qu' il neige à basse altitude , même près de la côte .
L' intérieur de la [MASK] est très chaud et sec en été .
Le brouillard n' est pas rare dans les vallées et les plaines , il peut être particulièrement résistant et peut être accompagné par des périodes de bruine verglaçante au cours de l' hiver près de la [MASK] et d' autres vallées de rivières .
La [MASK] [MASK] [MASK] [MASK] déclare que [MASK] [MASK] est une nation indissoluble qui reconnaît et garantit le droit à l' autonomie des régions qui la constituent .
La [MASK] , aux côtés du [MASK] [MASK] et de la [MASK] , a été mise à part du reste de [MASK] [MASK] comme une " communauté historique " .
Dans un processus débuté par [MASK] [MASK] et achevé en 1985 , les 14 autres communautés autonomes ont obtenu leurs propres statuts d' autonomie .
Après 2003 , il y a eu une série d' amendements concernant les divers statuts d' autonomie ( notamment , aux côtés de la [MASK] , ceux de l' [MASK] , la [MASK] [MASK] , les [MASK] [MASK] et les [MASK] [MASK] ) .
Celle-ci va étendre son influence sur le sud de l' [MASK] , [MASK] [MASK] et en [MASK] .
Les almogavres , mercenaires catalans , vont créer un éphémère duché en [MASK] .
Ceci explique l' usage de la langue catalane de nos jours au [MASK] [MASK] , aux [MASK] et dans un bourg de [MASK] , l' [MASK] .
La [MASK] va amorcer son déclin à la disparition du roi [MASK] [MASK] [MASK] [MASK] [MASK] , dernier souverain de la dynastie de [MASK] , mort sans héritier en 1410 .
Cette défaite est à l' origine de la fête " nationale " en [MASK] .
La [MASK] sort brisée et soumise de cette épreuve .
Il faut attendre plus d' un siècle pour voir la [MASK] renaître .
Son promoteur est [MASK] [MASK] .
Ce statut sera suspendu en 1939 , la [MASK] avait été l' avant dernière région à résister jusqu' au 14 février 1939 , durant la [MASK] [MASK] [MASK] .
L' exode rural en provenance d' autres zones de [MASK] [MASK] a également réduit l' usage social de la langue dans les zones urbaines .
Dans une tentative visant à inverser cette tendance , le rétablissement de l' autonomie des institutions de la [MASK] a entrepris une politique linguistique à long terme visant à accroître l' utilisation du catalan et a , depuis 1983 , promulgué des lois qui visent à protéger et à étendre l' usage du catalan .
Selon l' enquête linguistique réalisée en 2008 par le gouvernement de la [MASK] , qui diffère sensiblement de celle de 2003 , une majorité revendique l' espagnol comme la langue à laquelle elle s' identifie ( 46,5 % pour l' espagnol contre 37,2 % pour le catalan ; en 2003 les chiffres étaient de 47,5 % pour l' espagnol et 44,3 % pour le catalan ; entre-temps la part de ceux qui s' identifient autant à l' une qu' à l' autre langue a progressé , passant de 5,0 % à 8,8 % ) .
Puis , le 9 août 2006 , lorsque le nouveau statut est entré en vigueur , l' occitan est devenu officiel dans toute la [MASK] .
En 2007 , le PIB régional de la [MASK] était de 202509 millions d' euros et le PIB par habitant était de 24445 € .
Dans le contexte de la crise financière de 2008 , la [MASK] devrait subir une récession de près de 2 % de son PIB régional en 2009 .
Les principales destinations touristiques de la [MASK] sont la ville de [MASK] , les plages de la [MASK] [MASK] à [MASK] et la [MASK] [MASK] de [MASK] .
Dans les [MASK] , il existe plusieurs stations de ski .
Les touristes viennent essentiellement d' [MASK] et du [MASK] , et dans une moindre mesure du [MASK] et de la [MASK] .
Les caisses d' épargne ont une grande implantation en [MASK] .
[MASK] [MASK] est la première caisse d' épargne de l' [MASK] .
La première banque privée d' origine catalane est [MASK] [MASK] , qui occupe le quatrième rang des banques privées en [MASK] .
[MASK] [MASK] est depuis 2006 le 128 e président du gouvernement de la [MASK] , la [MASK] [MASK] [MASK] , à la tête d' une coalition tripartite de gauche " catalaniste et progressiste " , après les élections du 1 er novembre 2006 .
De 1980 à 2003 , la vie politique catalane a longtemps été dominée par [MASK] [MASK] de [MASK] [MASK] [MASK] , une coalition de deux partis nationalistes de centre-droite .
La [MASK] dispose de sa propre autonomie dans certains domaines .
La [MASK] [MASK] [MASK] [MASK] couvre une superficie de 32114 km² avec une population de 7354411 habitants ( 2008 ) à partir de laquelle on estime que les immigrants représentent 12,3 % , .
La région urbaine de [MASK] comprend 3327872 personnes et couvre une superficie de 2268 km² et environ 4700000 personnes vivent dans un rayon de 15 km de [MASK] .
L' aire métropolitaine de la région urbaine comprend des villes comme [MASK] [MASK] [MASK] [MASK] , [MASK] , [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] .
En dehors de [MASK] , il y a d' autres villes importantes , comme [MASK] , [MASK] , [MASK] , [MASK] , [MASK] .
La région métropolitaine de [MASK] comprend 675000 personnes et est la deuxième région métropolitaine de [MASK] .
En 1900 , la population de la [MASK] était de 1984115 personnes et en 1970 , on comptait déjà 5107606 habitants .
Cette augmentation est due à l' expansion démographique en [MASK] au cours des années 1960 et au début des années 1970 et aussi à l' exode rural en [MASK] .
Il y a 12000 km de routes dans toute la [MASK] .
Elle suit la côte , de la frontière française à [MASK] , située au sud de [MASK] .
Les routes principales sont généralement reliées à [MASK] .
L' [MASK] et l' [MASK] relient l' intérieur de la [MASK] à [MASK] .
La [MASK] a vu la première construction de chemins de fer dans la [MASK] [MASK] en 1848 , avec la liaison entre [MASK] et [MASK] .
Compte tenu de la topographie , la plupart des lignes de chemins de fer en [MASK] sont reliées à [MASK] .
Les compagnies de chemin de fer en [MASK] sont les [MASK] et la [MASK] .
Les trains à haute vitesse d' [MASK] peuvent actuellement atteindre [MASK] , [MASK] et [MASK] .
L' ouverture officielle de la ligne entre [MASK] et [MASK] a eu lieu le 20 février 2008 .
Le trajet entre [MASK] et [MASK] dure environ 2 heures 30 .
La [MASK] a ses propre symboles et signes distinctifs , tels que :
La sardane est la danse populaire la plus caractéristique de la [MASK] .
La [MASK] [MASK] [MASK] est une fête traditionnelle et populaire ayant lieu dans la ville de [MASK] ( située au nord de [MASK] ) .
Les plus hauts bâtiments de [MASK] sont :