En mars 2008 , lors des élections municipales , elle est élue avec 23342 voix , soit 50,15 % des suffrages , face au maire sortant [MASK] [MASK] .
Seize mois après l' élection d' [MASK] [MASK] , les électeurs saint-paulois devront retourner aux urnes .
A l' époque , c' est [MASK] [MASK] qui avait saisi la juridiction administrative .
Une semaine après [MASK] , le scrutin municipal de [MASK] est à son tour annulé .
De même pour les membres du cabinet d' [MASK] [MASK] .