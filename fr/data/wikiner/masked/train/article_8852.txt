[MASK] [MASK] [MASK] [MASK] est un jeu vidéo développé et édité par [MASK] et réalisé par [MASK] [MASK] .
Il est le premier épisode de la série [MASK] [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] [MASK] est l' un des premiers jeux à représenter les personnages jouables en polygones dans un environnement pré-calculé .
L' histoire est inspirée par les nouvelles de [MASK] [MASK] [MASK] .
Elle se déroule dans un vieux manoir en [MASK] , réputé hanté , dont le propriétaire s' est récemment donné la mort .
Il remporte plusieurs prix en [MASK] et au [MASK] , dont le [MASK] d' or décerné par le magazine du même nom qui le consacra meilleur jeu d' aventure de l' année .
Il est le premier survival horror de l' histoire du jeux vidéo , genre popularisé avec la série [MASK] [MASK] ou [MASK] [MASK] .
Le moteur original du jeu a été créé par [MASK] [MASK] comme petit projet indépendant chez [MASK] .
Un concours interne à [MASK] a permis de peindre les différentes vues et textures utilisées .
[MASK] [MASK] fut logiquement proposé pour diriger le projet .
Devant le succès de [MASK] [MASK] [MASK] [MASK] , le développement d' une suite est mis en chantier .
Un désaccord important entre l' équipe et [MASK] [MASK] ( président d' [MASK] ) eut lieu concernant la direction que la suite devrait prendre .
La majorité de l' équipe quitta [MASK] pour fonder un nouveau studio appelé [MASK] [MASK] [MASK] et n' a donc pas participé à [MASK] [MASK] [MASK] [MASK] [MASK] .