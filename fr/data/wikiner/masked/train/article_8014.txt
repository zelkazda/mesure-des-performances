Parmi les apports d' [MASK] à la connaissance de la protohistoire provençale , il faut ainsi citer une statuaire relativement riche , comprenant notamment des " guerriers assis " , et en laquelle on a pu voir l' objet d' un culte d' ancêtres héroïsés .
Des éléments de portiques attestent l' existence d' un culte des têtes ( coupées ) , qui recoupe les témoignages écrits de [MASK] [MASK] [MASK] .
Auparavant , les [MASK] étaient les seuls habitants de la [MASK] et descendants de peuples protohistoriques .
Pour expliquer cette évolution , plusieurs hypothèses ont été formulées , parmi lesquelles il faut citer celle d' un effet des tensions causées par la pression de [MASK] .
En tous cas , à partir de -181 , [MASK] commença à faire appel à [MASK] pour l' aider à mettre fin aux pillages des indigènes et à défendre ses colonies .
La fédération disparut , en effet , sous les coups du consul [MASK] [MASK] [MASK] , entre -125 et -123 , devenant ainsi la " première victime " de la conquête romaine .
Le site d' [MASK] fut encore abandonné au profit de la plaine à la suite d' une nouvelle destruction violente , survenue entre -110 et -90 ; celle-ci marque le terminus postquem du site .