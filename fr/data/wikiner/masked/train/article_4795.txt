[MASK] est un environnement de développement intégré sous [MASK] supportant le langage [MASK] [MASK] et [MASK] .
Il s' agit d' une tentative de portage par [MASK] de son RAD phare [MASK] et de son " petit frère " [MASK] [MASK] .
La première version est sortie début 2001 , un peu jeune ; une deuxième a suivi très vite ; la troisième et dernière version ( [MASK] 3 ) est sortie en juillet 2002 .
[MASK] est un produit très proche de [MASK] : même principe , même interface .
Il fonctionne sous [MASK] et permet de créer des programmes pour ce système .
Le même code source peut en théorie être compilé sous [MASK] et [MASK] ( respectivement avec [MASK] et [MASK] ) grâce à l' utilisation de la bibliothèque objet [MASK] qui s' appuie sur la bibliothèque graphique [MASK] .
Il existait une version en téléchargement gratuit qui permet d' écrire des programmes sous licence [MASK] .
[MASK] est maintenant totalement abandonné par [MASK] , du fait officiellement des faibles retombées économiques du produit .