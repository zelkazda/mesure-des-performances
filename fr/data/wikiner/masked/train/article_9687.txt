[MASK] est une ville de [MASK] , en [MASK] .
Elle se trouve à 82 km au sud de [MASK] , à 55 minutes par la route .
Elle possède une université de renommée internationale qui attire un grand nombre d' étudiants en provenance de l' étranger , en particulier d' [MASK] .
On peut également y trouver deux cathédrales régionales , et de nombreux lieux de culte de différentes religions , dont le célèbre temple bouddhiste de [MASK] [MASK] , qui est le plus grand temple bouddhiste de l' hémisphère sud .
La ville de [MASK] a une situation géographique assez particulière : sur un plateau coincé entre l' [MASK] [MASK] à l' est et un escarpement particulièrement raide à l' ouest .
Un peu au sud du centre-ville de [MASK] , près de [MASK] [MASK] , se trouvent cinq îles qu' on appelle the five islands ( les cinq îles ) .
Une autre fierté de la ville est le lac de l' [MASK] , situé un peu au sud du centre-ville .
[MASK] possède un aéroport .
Les premiers explorateurs européens ayant visité la région furent les navigateurs britanniques [MASK] [MASK] et [MASK] [MASK] , qui débarquèrent près du lac de l' [MASK] en 1796 .
En 1856 , la population de [MASK] était de 864 habitants .
Le navigateur anglais [MASK] [MASK] fut le premier à décrire l' existence de ressources en charbon dans la région en 1797 .
L' aciérie de [MASK] [MASK] est aujourd'hui un leader australien de la production d' acier avec environ 5 millions de tonnes produits chaque année .
Cela n' a pas empêché la ville de [MASK] d' avoir l' un des taux de chômage les plus élevés dans tout le pays , ainsi qu' un des plus grands nombres de toxicomanes .
En 2001 la population de la ville de [MASK] était de 181 612 habitants .
Encore plus récemment , [MASK] est devenue une destination phare pour de nouveaux habitants issus de la banlieue de [MASK] , qui cherchent à se loger moins cher , à avoir un trafic routier plus réduit et un milieu moins urbanisé .
L' existence de transports en commun liant [MASK] à [MASK] a fait que bien des jeunes familles viennent s' installer à [MASK] tout en travaillant à [MASK] .
Il existe à [MASK] une seule université , l' [MASK] [MASK] [MASK] , qui a fait partie de l' [MASK] [MASK] [MASK] , ainsi qu' un institut technique .
La ville dispose de trois chaînes de télévision commerciales , deux chaînes gouvernementales , l' [MASK] [MASK] [MASK] ( [MASK] ) et le [MASK] [MASK] [MASK] ( [MASK] ) , six chaînes de radio propres à [MASK] .
Les [MASK] [MASK] participent à la compétition nationale de basket-ball et ont même remporté le championnat de 2001 .