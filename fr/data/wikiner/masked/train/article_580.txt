Sa préfecture est la ville de [MASK] .
L' [MASK] et [MASK] [MASK] lui attribuent le code 65 .
Le conventionnel vicquois [MASK] [MASK] s' est particulièrement battu en ce sens : " Si ce pays , le [MASK] , est trop petit pour former un département , il convient de l' agrandir .
Mais il serait très inique de n' en faire que des districts dépendant d' une ville étrangère ; ce serait un meurtre politique que de faire de [MASK] le misérable chef-lieu d' un district. "
Il est alors baptisé " [MASK] " .
On notera l' étonnante géographie de ce département qui possède deux petites enclaves dans le département voisin des [MASK] .
Le [MASK] [MASK] [MASK] fait partie de la région [MASK] .
Il est limitrophe des départements des [MASK] , du [MASK] , de la [MASK] , et de la [MASK] [MASK] [MASK] ( [MASK] ) .
La montagne , c' est-à-dire les [MASK] , recouvre , au sud , la moitié du territoire du département .
Elle forme une barrière naturelle entre la [MASK] et l' [MASK] , ne permettant qu' un seul accès routier vers l' [MASK] par le [MASK] [MASK] .
35 de ses pics dépassent 3000 m et le [MASK] [MASK] [MASK] , à 3298 m , en est le point culminant .
L' altitude des [MASK] s' estompe progressivement en remontant vers le nord , et les montagnes cèdent la place au piémont , limité à l' est par le vaste [MASK] [MASK] [MASK] ( à 600 m environ ) , véritable château d' eau du département où naissent de nombreuses rivières des bassins de la [MASK] et de l' [MASK] ( [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] ) .
À l' ouest , le bassin du [MASK] [MASK] [MASK] ou [MASK] forme une large vallée de [MASK] à [MASK] .
À l' est , la vallée de la [MASK] se déroule jusqu' à [MASK] où se rejoignent les vallées d' [MASK] et du [MASK] .
Les vallées sont reliées entre elles par des cols d' altitude parfois élevée tel le [MASK] , le [MASK] , l' [MASK] ou encore le [MASK] .
Les [MASK] , territoire de culture du maïs et d' élevage , sont largement tournées vers la production alimentaire .
Elles comptent de nombreuses productions fameuses tel l' oignon de [MASK] , le haricot tarbais ...
L' activité principale se situe dans la plaine de [MASK] , autour de [MASK] et de [MASK] .
[MASK] produit ainsi à [MASK] et [MASK] .
À [MASK] , la [MASK] , filiale d' [MASK] , est présente afin de fabriquer des avions d' affaires et de tourisme .
[MASK] possède un site à [MASK] .
Le thermalisme , à [MASK] , [MASK] ou encore [MASK] , a une importance économique considérable .
Il s' est développé autour de [MASK] , centre de pèlerinage , des stations de ski , telle la station du [MASK] , ou encore des sites tels le cirque glaciaire de [MASK] , de réputation mondiale , ou l' observatoire du [MASK] [MASK] [MASK] [MASK] [MASK] .
La ville principale et chef-lieu est [MASK] qui a réunit autour d' elle une agglomération , le [MASK] [MASK] , comprenant des communes secondaires telles qu' [MASK] , [MASK] , [MASK] ou [MASK] .
La ville mariale , [MASK] , est la seconde plus grande ville .
[MASK] , [MASK] sont des villes thermales de moindre importance .
Les communes de [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] ou [MASK] pourraient encore être citées .