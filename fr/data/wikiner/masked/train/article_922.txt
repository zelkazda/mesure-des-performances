À son sommet a été construit un observatoire météorologique et malgré les conditions climatiques rigoureuses , la montagne est une destination extrêmement populaire en particulier pour les [MASK] , qu' ils soient shintoïstes ou bouddhistes , en raison de sa forme caractéristique et du symbolisme religieux traditionnel qu' il représente .
Pourtant , cette fréquentation fragilise l' environnement et des actions sont menées en vue d' une reconnaissance par l' [MASK] .
Les kanji pour le [MASK] [MASK] , 富 et 士 , signifient respectivement " richesse " ou " abondance " et " un homme avec un certain statut " mais ces caractères sont probablement un ateji , c' est-à-dire qu' ils ont probablement été sélectionnés en raison de la similitude de leur prononciation avec les syllabes du nom mais sans pour autant posséder un sens particulier .
L' origine du nom [MASK] reste incertaine .
Un texte du [MASK] [MASK] dit que le nom vient d ' " immortel " ( 不死 , fushi ) et de l' image d' abondants ( 富 , fu ) soldats ( 士 , shi ) grimpant les versants de la montagne .
Le [MASK] [MASK] est situé dans le centre du [MASK] et de l' île principale de [MASK] , encadré par les [MASK] [MASK] au nord-ouest et l' [MASK] [MASK] au sud-est .
Administrativement , il est situé à cheval sur les préfectures de [MASK] au sud et de [MASK] au nord .
Il est bordé au nord par les cinq lacs [MASK] : le [MASK] [MASK] , le [MASK] [MASK] , le [MASK] [MASK] , le [MASK] [MASK] et le [MASK] [MASK] .
La topographie du [MASK] [MASK] est dictée par le volcanisme dont il est né : de la forme d' un cône quasi-symétrique de trente kilomètres à sa base , ses pentes prononcées et régulières s' élèvent jusqu' à 3776 mètres d' altitude , conférant un volume de 870 km 3 à ce stratovolcan .
Du fait de l' altitude élevée du sommet du [MASK] [MASK] , plusieurs climats s' étagent le long de ses pentes .
Le 25 septembre 1966 , la station météorologique a également mesuré la vitesse record de vent pour [MASK] [MASK] avec 91 m/s , soit environ 330 km/h , au moment du passage d' un typhon .
Ainsi , le 5 mars 1966 , le [MASK] [MASK] du vol 911 de la [MASK] [MASK] [MASK] [MASK] , pris dans ce type de turbulences , se disloque en plein vol et s' écrase près du [MASK] [MASK] , peu de temps après son décollage de l' [MASK] [MASK] [MASK] [MASK] , en ne laissant aucun rescapé ( 113 passagers et 11 membres d' équipage étaient présents dans l' appareil ) .
Ces plaques forment respectivement les parties occidentale et orientale du [MASK] ainsi que la [MASK] [MASK] [MASK] .
Le [MASK] [MASK] constitue le volcan le plus septentrional de l' arc volcanique formé par l' [MASK] [MASK] [MASK] .
Outre le sommet principal couronné par un cratère sommital , les flancs et les pieds du [MASK] [MASK] comportent une cinquantaine de dômes , de cônes et de petites bouches éruptives .
Les scientifiques ont identifié quatre phases d' activité volcanique distinctes dans la formation du [MASK] [MASK] .
Le [MASK] [MASK] est actuellement classé actif avec un faible risque éruptif .
Les éruptions du [MASK] [MASK] présentent des coulées de lave , des émissions de magma , de scories et de cendre volcanique , des effondrements et des éruptions latérales , d' où le qualificatif de " grand magasin des éruptions " .
Cet incident est appelé aujourd'hui coulée de lave de [MASK] .
Cet évènement est connu sous le nom d ' [MASK] lava ( 青木ヶ原溶岩 , [MASK] lava ? ) et le lieu est actuellement couvert par la dense forêt d' [MASK] .
La première ascension connue du [MASK] [MASK] est datée de 663 et a été réalisée par un moine bouddhiste anonyme .
Depuis , il constitue une destination touristique populaire et nombreux sont les [MASK] qui le gravissent au moins une fois par an .
Des samouraïs pratiquaient jadis leur entraînement au pied du [MASK] [MASK] , près de la ville actuelle de [MASK] .
L' ascension du [MASK] [MASK] est relativement aisée bien que pouvant se révéler éprouvante du fait de la grande distance horizontale à parcourir entre le lieu de départ pédestre et le sommet .
La période la plus fréquentée pour gravir le [MASK] [MASK] dure du 1er juillet au 27 août en raison de l' ouverture estivale des refuges et autres commodités touristiques ainsi que de la circulation des bus jusqu' à la cinquième station , la dernière accessible par la route et la plus proche du sommet .
Chaque année , le nombre de visiteurs gravissant le [MASK] [MASK] est estimé entre 100000 et 200000 personnes , dont 30 % d' étrangers .
Même si elle n' est que la deuxième plus haute station parmi les cinq , [MASK] est la plus fréquentée en raison de son vaste parking , .
Les grimpeurs ayant fait l' ascension de nuit , outre le fait d' avoir évité l' éprouvante randonnée sous le soleil , ont le privilège d' assister au lever du soleil depuis le sommet , évènement particulièrement apprécié des [MASK] , spécialement dans la nuit du 31 décembre au 1 er janvier et cela malgré les conditions climatiques difficiles .
Le [MASK] [MASK] fait partie du [MASK] [MASK] [MASK] [MASK] au même titre que les [MASK] [MASK] [MASK] [MASK] , [MASK] , la [MASK] [MASK] [MASK] et l' [MASK] [MASK] [MASK] .
En 1932 , une station météorologique temporaire est installée au sommet du [MASK] [MASK] .
Les eaux souterraines du [MASK] [MASK] et des alentours sont utilisées à des fins pharmaceutiques , pour les industries papetières et comme eau minérale grâce à leur richesse en vanadium .
Le nom de " [MASK] " sert de franchise a un certain nombre d' entreprises au [MASK] , l' une des plus connues étant [MASK] , la marque de film photo et le fabricant d' appareils photos et caméras numériques .
Il existe un circuit automobile au pied du [MASK] [MASK] : le [MASK] [MASK] .
Le tracé de 4.563 km a été créé en 1965 et a accueilli le [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] en 1976 et 1977 .
Cette année-là , un grave accident impliquant la [MASK] de [MASK] [MASK] entraîne la mort d' un spectateur et d' un commissaire de piste .
En 2000 , le circuit du [MASK] [MASK] devient la propriété de [MASK] et finalement , en 2007 , la compétition y fait son retour avec la victoire de [MASK] [MASK] .
Dans la même période , [MASK] [MASK] ( 1798 -- 1861 ) a peint quelques représentations du [MASK] [MASK] .
De nombreux synonymes japonais du [MASK] [MASK] rendent eux aussi compte de son caractère religieux .
Afin de vénérer les nombreuses divinités des différentes religions , plusieurs sanctuaires ont été bâtis sur ou aux pieds du [MASK] [MASK] et de nombreux torii jalonnent le parcours afin de marquer les limites de l' enceinte sacrée .
Dans le roman [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] raconte sa découverte du [MASK] [MASK] .