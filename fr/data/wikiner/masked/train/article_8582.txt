L ' [MASK] est une vaste péninsule au sud-ouest de l' [MASK] , à la jonction entre ce continent avec l' [MASK] .
Aussi les précipitations autour de [MASK] peuvent atteindre 400 à 600 mm/an , et jusqu' à 1000 en altitude .
L' [MASK] est bordée par la [MASK] [MASK] et le [MASK] [MASK] [MASK] à l' ouest , par le [MASK] [MASK] [MASK] au sud-ouest , par l' [MASK] [MASK] au sud , par la [MASK] [MASK] [MASK] au sud-est et par le [MASK] [MASK] [MASK] et le [MASK] [MASK] à l' est .
Selon les estimations , la population de l' [MASK] était de 77 983 936 habitants en 2008 .
Le forage et le raffinage du pétrole sont les activités industrielles les plus importantes dans la [MASK] [MASK] .