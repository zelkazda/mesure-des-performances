[MASK] [MASK] naît le 22 juillet 1804 à [MASK] dans une famille bourgeoise originaire de [MASK] en [MASK] .
Il fait de courtes études au [MASK] [MASK] , côtoyant les milieux littéraires et artistiques parisiens , faisant connaissance avec [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .
Son père l' envoie au [MASK] , aux [MASK] et à [MASK] en 1828 -- 1830 en tant que représentant commercial de l' entreprise familiale .
Lorsqu' il est à [MASK] , il y est révolté par l' esclavage .
De retour en [MASK] , il devient journaliste et critique artistique , publiant des articles , des ouvrages , multipliant ses déplacements d' information .
Le discours abolitionniste de [MASK] évolue au cours de sa vie .
Lors du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , il fut un des députés présents aux côtés de [MASK] [MASK] sur la barricade où celui-ci sera tué .
Il s' exile en [MASK] où il rencontre fréquemment son ami [MASK] [MASK] ; il y devient un spécialiste de l' œuvre du compositeur [MASK] [MASK] [MASK] , rassemble une collection très importante de ses manuscrits et partitions et rédige une de ses premières biographies , mais celle-ci n' est éditée que dans sa traduction anglaise .
En 1870 , il revient en [MASK] suite à la [MASK] [MASK] [MASK] .
En 1877 , [MASK] [MASK] dépose une proposition de loi pour interdire la bastonnade dans les bagnes .
En 1884 et 1885 il tente de s' opposer , sans succès , à l' institution de la relégation des forçats récidivistes en [MASK] .
En 1952 , un billet de 5000 francs à l' effigie de [MASK] [MASK] est mis en circulation en [MASK] .
La commune de [MASK] a fait de la maison de la famille [MASK] un musée qui porte son nom .