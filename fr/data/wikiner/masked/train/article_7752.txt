Le terme est probablement apparu aux [MASK] dans les années 1930 .
L' heroic fantasy est à l' origine le nom donné à la fantasy , dans le sens d' oeuvres centrées sur des aventures héroïques dans des mondes imaginaires au contexte fortement médiéval ou proto-médiéval ( [MASK] ) .
Le véritable créateur de l' heroic fantasy , contrairement à ce qu' on pense , n' est pas [MASK] [MASK] [MASK] mais [MASK] [MASK] [MASK] ( bien que celui qui ait perfectionné l' art soit [MASK] , avec [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] ainsi que [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ) .
Son [MASK] ( créé en 1912 ) fait d' abord office de personnage précurseur .
Mais au fur et à mesure , [MASK] sort du cadre purement aventureux pour rentrer dans la fantasy .
Le deuxième grand auteur d' heroic fantasy est évidemment [MASK] [MASK] [MASK] .
Dans [MASK] , l' intrigue bien que centrée sur [MASK] et [MASK] , s' articule autour d' une dizaine de personnages secondaires tous intéressants à leur manière .
Dans [MASK] , le héros central est en fait l' épée elle même .
Chez [MASK] , les créatures sont très proches de l' humanoïde et souvent peu développées .
Seule [MASK] [MASK] finit par introduire les dragons .
La situation s' inverse , alors que c' est d' habitude [MASK] qui séduit de nombreuses femmes sans défense , ici , ce sont parfois les femmes qui défendent les hommes et qui choisissent leurs nombreux amants ou amantes .