[MASK] est un langage ( non [MASK] ) pour localiser une portion d' un document [MASK] .
Une expression [MASK] est un chemin de localisation , constitué de pas de localisation .
L' axe indique la direction dans laquelle se déplacer dans l' arbre [MASK] , relativement au nœud courant ou depuis la racine .
Dans [MASK] , quand l' axe n' est pas précisé , il s' agit implicitement de l' axe des enfants ( child:: ) .
[MASK] offre ainsi une recherche séquentielle par nœuds .
Le résultat de l' évaluation d' une expression [MASK] est une séquence contenant des nœuds et des valeurs atomiques ( textes , booléens ... ) .
alors les expressions [MASK] suivantes
Toutes ces expressions [MASK] sont absolues , c' est-à-dire qu' elles donnent le même résultat quel que soit le contexte .
[MASK] est le langage de requête élémentaire dans [MASK] .
Il détermine si une règle template s' applique ( via son attribut match ) , et peut aussi servir à extraire des contenus du document [MASK] transformé par le programme [MASK] .
Autre exemple , avec xfind pour chercher des fichiers ( [MASK] servant à modéliser les attributs du fichier ) :
La syntaxe [MASK] a évolué pour devenir plus complète .