Il existe plusieurs versions de ces listes car ces noms ont été extraits de différents passages du [MASK] et des hadiths .
Un autre hadith , rapporté par [MASK] , cite 99 attributs , et il s' agit de la liste la plus communément rencontrée .
Toutefois , les oulémas affirment [ réf. nécessaire ] que cette liste n' est pas attribuée au prophète [MASK] , mais à certains rapporteurs du hadith .
Il est généralement admis , chez ces oulémas , qu' [MASK] possède plus de quatre-vingt-dix-neuf attributs ou noms dont certains ne sont pas connus .
Un hadith parle ainsi de noms dont [MASK] s' est réservé la connaissance .
Dans le sens où affirmer qu' [MASK] possède ces 99 attributs ou noms ne doit pas impliquer qu' il n' en possède pas d' autres .