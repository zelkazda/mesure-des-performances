Elle est le chef-lieu du [MASK] [MASK] [MASK] .
[MASK] se trouve sur la route de [MASK] au [MASK] [MASK] [MASK] , à l' entrée de la vallée de la [MASK] au pied des [MASK] .
[MASK] est à 13 kilomètres de [MASK] .
À l' origine , le village dépendait de la paroisse de [MASK] .
Elle fut rattachée dès lors au diocèse de [MASK] .
Par la suite , l' installation d' une machine à vapeur permettra le fonctionnement de métiers à tisser mécaniques en compensant l' irrégularité de la fourniture d' énergie hydraulique de la rivière la [MASK] .
Le fort [MASK] , construit entre 1875 et 1889 suivant les plans du général [MASK] [MASK] [MASK] , complète au nord la ceinture de fortification construite autour de [MASK] .
La voie de chemin de fer qui relie en 1883 [MASK] à la ligne [MASK] -- [MASK] permet non seulement d' alimenter en charbon les machines à vapeur de l' industrie textile et d' en exporter les produits , mais aussi de transporter plus de 40000 voyageurs par an quatre ans plus tard .
À la suite de la guerre de 1870 , [MASK] a changé de département ( [MASK] [MASK] [MASK] ) mais l' administration postale a conservé le cachet à date initial ( mention 66 pour [MASK] ) .
En 1928 , il a bénéficié d' une [MASK] [MASK] qui demandait de présenter la ville par un slogan sur quatre lignes d' une dizaine de caractères :