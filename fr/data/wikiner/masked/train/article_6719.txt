Il en est ainsi de la nappe phréatique de l' [MASK] ou de celle , gigantesque , de la [MASK] qui couvre 9 000 km² .
La craie a ensuite été exploitée pour produire des pierres de taille , pour alimenter les fours à chaux ou fournir les moellons qui garnissaient l' intérieur des murailles de fortifications ( [MASK] [MASK] [MASK] par exemple ) .
C' est l' origine des catiches du [MASK] et de [MASK] .