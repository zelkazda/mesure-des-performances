Dès 999 , l' [MASK] [MASK] [MASK] devint comte du [MASK] .
En 1968 , la commune et le village de [MASK] ont fusionné avec la municipalité de [MASK] .
[MASK] tenta à trois reprises d' obtenir les [MASK] [MASK] [MASK] [MASK] , mais fut trois fois le dauphin malheureux du vainqueur ( [MASK] qui finalement refusa en 1976 au profit d' [MASK] , [MASK] [MASK] [MASK] en 2002 et [MASK] en 2006 ) .
La [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( ancienne résidence des chanoines du chapitre ) et le [MASK] [MASK] [MASK] surplombent la ville .
On compte 97 jours par an de gel à [MASK] .
Le secteur primaire , bien que marginalisé , n' est pas négligeable : [MASK] est la troisième commune viticole de [MASK] , la culture maraîchère y est également notable .
De plus , divers festivals de musique y ont lieu , dont les renommés festival [MASK] [MASK] et son concours international de violon ou encore le festival de l' orgue ancien , dédié au plus vieil orgue jouable du monde ( c .
1390 -- 1430 ) qui se trouve dans la [MASK] [MASK] [MASK] .
Depuis 2003 , la vieille ville de [MASK] est également dotée d' un marché traditionnel .
La société d' escrime de [MASK] existe depuis 1945 .
La ville de [MASK] possède deux écoles de maturité pour la formation des étudiants :
L' [MASK] [MASK] [MASK] [MASK] [MASK] est également implantée à [MASK] ( ainsi qu´à [MASK] ) .