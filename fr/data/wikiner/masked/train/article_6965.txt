La [MASK] est composée de 18 chapitres .
L' histoire se déroule au début de la grande guerre entre les [MASK] , fils du roi [MASK] , et les [MASK] .
[MASK] doit souffler dans une conque pour annoncer le début des combats mais , voyant des amis et des parents dans le camp opposé , il est désolé à la pensée que la bataille fera beaucoup de morts parmi ceux qu' il aime .
Il se tourne alors vers [MASK] pour exprimer son dilemme ( faire son devoir en conduisant son armée et , ce faisant , tuer des membres de sa famille ) et demander conseil .
La [MASK] , conte l' histoire de [MASK] , 8 e avatar de [MASK] , et d' [MASK] , un prince guerrier en proie au doute devant la bataille qui risque d' entraîner la mort des membres de sa famille , les [MASK] , qui se trouvent dans l' armée opposée .
Le récit est constitué du dialogue entre [MASK] et [MASK] .
[MASK] instruit [MASK] sur un grand éventail de domaines , à commencer par celui qui résout le dilemme d' [MASK] , la réincarnation , signifiant par là que les vies perdues dans la bataille ne le sont pas véritablement .
[MASK] continue d' exposer un grand nombre de sujets spirituels , parmi lesquels plusieurs yogas -- ou chemins de dévotion -- différents .
Dans le onzième chapitre , [MASK] dévoile à [MASK] qu' il est , en fait , une incarnation du dieu [MASK] .
Selon [MASK] , la racine de toutes les douleurs et de tous les troubles est l' agitation de l' esprit provoquée par le désir .
La seule manière d' éteindre la flamme du désir , indique [MASK] , c' est de calmer l' esprit par la discipline des sens et de l' esprit .
Selon la [MASK] , le but de la vie est de libérer l' esprit et l' intellect de leurs complexités et de les concentrer sur la gloire de l' âme .
La première traduction anglaise , par [MASK] [MASK] , date de 1785 .
[MASK] ne s' est pas contenté de traduire , il a proposé un commentaire , qui insistait sur la signification morale du poème , que [MASK] rapprochait de l' éthique stoïcienne du détachement et du devoir .
[MASK] , dans son compte rendu critique du commentaire de [MASK] , a voulu au contraire montrer que ce rapprochement n' était que superficiel , que la pensée indienne ne pouvait se hisser jusqu' au concept de la subjectivité libre .