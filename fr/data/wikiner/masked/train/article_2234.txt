Sa première utilisation remonte à l' [MASK] [MASK] qui dans les années 1780 évoque un " traité du socialisme " devant parler " du but que se propose l' homme en société et des moyens qu' il a d' y parvenir " .
Selon certains , ce serait [MASK] qui aurait utilisé pour la première fois le mot " socialisme " en 1827 .
Même s' il n' est pas possible de dater l' origine du concept de socialisme aux philosophes de l' antiquité comme [MASK] qui globalement acceptait les castes et l' esclavage , son approche du concept de justice chercha à montrer que celle-ci était au profit des plus faibles , se rapprochant ainsi d' utopies sociales , de certains mouvements religieux ou laïcs antérieurs à 1789 .
Le penseur principal de cette seconde phase est [MASK] [MASK] .
Les courants politiques se revendiquant aujourd'hui du socialisme sont : le socialisme marxiste , la social-démocratie ( soit le mot " socialisme " tel qu' employé couramment en [MASK] ) , et les socialismes autogestionnaire ou libertaire .
Le socialisme est né aussi dans les années 1820 -- 1830 avec des penseurs -- les précurseurs -- comme [MASK] ( ne pas le confondre avec le [MASK] [MASK] [MASK] , mémorialiste ) , qui s' inscrivit dans la lignée de l' école des idéologues .
D' autres penseurs comme [MASK] [MASK] , [MASK] , et [MASK] [MASK] en [MASK] ou encore [MASK] [MASK] au [MASK] , considéré comme le premier à mettre en pratique ses idées avec la création d' une communauté de travail .
Ces premiers socialistes , ainsi que [MASK] furent qualifiés par la suite d' utopiques par [MASK] [MASK] .
L' héritage de [MASK] sera multiple .
Ses écrits ont été repris après sa mort en 1825 par [MASK] [MASK] [MASK] ( polytechnicien ) , pour engendrer le courant du saint-simonisme .
Ce courant a subi assez rapidement un " schisme " entre les partisans d' [MASK] , plutôt libéraux , et les partisans d' [MASK] [MASK] ( polytechnicien également ) , qui ont initié un socialisme scientifique .
[MASK] [MASK] peut être considéré comme l' un des précurseurs du marxisme [ réf. nécessaire ] .
Il s' est cependant poursuivi à travers le mouvement coopératif et de nombreuses expériences communautaires auxquelles on doit rattacher les " milieux libres " libertaires , plus ou moins durables , plus ou moins organisées autour du travail , de l' épanouissement personnel , de valeurs morales ( Les [MASK] [MASK] [MASK] [MASK] , etc .
Les nombreuses mais souvent éphémères communautés hippies ( 1967 aux [MASK] ) et héritées du mouvement de mai 1968 constituent la forme récente de l' ancien socialisme utopique .
Malgré l' édification théorique de sociétés idéales fondées sur des systèmes économiques et sociaux aboutis ( le phalanstère de [MASK] , le communisme colonial de [MASK] [MASK] ) , ils considèrent de façon pragmatique comme prioritaire la lutte contre les conséquences les plus dures de l' économie capitaliste .
Pour [MASK] [MASK] , le travail permet à l' homme de transformer profondément la nature .
La force de travail est vendue au prix de sa valeur d' échange , mais ce qu' en gagne le capitaliste est sa valeur d' usage ( le travail lui-même ) : comme les deux valeurs différent , il réalise un surplus , que [MASK] appelle plus-value .
Enfin pour [MASK] , le système capitaliste s' engorge irrémédiablement et génère de plus en plus de biens qu' il ne peut plus écouler .
[MASK] parle également de " l' idéologie dominante " .
Les penseurs principaux de cette tendance seront [MASK] [MASK] [MASK] et [MASK] [MASK] , d' autres penseurs de cette tendance suivront par la suite .
Les travaux de [MASK] sont aujourd'hui une référence importante du socialisme libertaire .
Les réformistes défendaient l' appui sur les groupes parlementaires organisés et sur la démocratie représentative ( [MASK] [MASK] ) .
L' avènement de la social-démocratie en tant qu' idéologie identifiée s' est fait réellement avec la première guerre mondiale et la scission avec les adversaires de la guerre , désormais dénommés " communistes " , qui se sont regroupés pour la majorité au sein de la [MASK] [MASK] de [MASK] à partir de 1919 .
L' emploi du terme " social-démocrate " doit être cependant manipulé avec précaution , d' autant plus qu' il désigne toujours au [MASK] des forces politiques de centre-droit .
Sans oublier la célèbre enquête sociale d' [MASK] [MASK] [MASK] .
Ce courant influencera notamment des écrivains tels que [MASK] [MASK] ou [MASK] .
Ils ne sont cependant pas totalement absents , à l' image de [MASK] [MASK] .
Actuellement , en [MASK] , le socialisme chrétien s' exprime avant tout à travers des chrétiens socialistes ( des individus engagés au sein du mouvement socialiste ) , tels [MASK] [MASK] et [MASK] [MASK] , pour qui la religion a joué un grand rôle dans son engagement .
De fait , le socialisme de la chaire influencera les réformes sociales du chancelier allemand [MASK] .
Ses principaux représentant sont [MASK] [MASK] ( 1838-1917 ) , [MASK] [MASK] ( 1835-1917 ) , [MASK] [MASK] ( 1844-1931 , qui insiste plus sur le rôle des syndicats ) , [MASK] [MASK] ( 1863-1941 ) .
Dans le monde arabe , s' est développé le socialisme arabe , notamment sous la forme du [MASK] .