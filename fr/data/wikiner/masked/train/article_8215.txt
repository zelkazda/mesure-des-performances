Il est né , ainsi que sa sœur jumelle et épouse [MASK] ( ou [MASK] ) , de la semence d' [MASK] le créateur .
Tout deux n' ont qu' un unique parent , le dieu [MASK] .
[MASK] symbolisant l' air et [MASK] l' humidité , ils représentent avec leurs deux enfants , [MASK] ( la terre ) et [MASK] ( le ciel ) , les quatre éléments primordiaux .
C' est [MASK] qui , sur ordre de son père , sépara [MASK] et [MASK] ( l' air entre la terre et le ciel ) .
On le représentait sous les traits d' un homme barbu se tenant debout ou à genou près de [MASK] et soutenant [MASK] les bras tendus .
[MASK] et [MASK] étaient honorés dans la ville de [MASK] dans le [MASK] [MASK] [MASK] .
Plus tard , [MASK] remplacera [MASK] dans le rôle du père de [MASK] et de [MASK] .