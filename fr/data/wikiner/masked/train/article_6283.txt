Les contours de l' [MASK] ont varié au cours des siècles , en fonction des aléas de l' histoire et des rattachements ou séparations d' avec les comtés voisins , [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] , etc .
[MASK] [MASK] donna l' [MASK] en 1237 , avec titre de comté , à [MASK] , son frère puîné .
Il comporte plusieurs sites : [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] .
Le siège de l' école interne est à [MASK] .
Pendant plus d' un siècle , la vie au sud de l' [MASK] a été tournée vers l' exploitation du charbon .