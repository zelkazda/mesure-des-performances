Situé au nord du [MASK] [MASK] [MASK] , quasiment enclavé dans l' [MASK] , il a une petite frontière commune avec la [MASK] .
Les frontières de la région qui constitue aujourd'hui le [MASK] furent établies en 1947 pendant la [MASK] [MASK] [MASK] , quand le pays devint la partie orientale du [MASK] .
Le [MASK] voit le jour après une guerre d' indépendance avec l' appui de l' [MASK] et de l' [MASK] .
Géographiquement , l' essentiel du [MASK] est occupé par le delta du [MASK] et du [MASK] .
Il est membre du [MASK] depuis 1972 , de l' [MASK] , du [MASK] , de l' [MASK] , et du [MASK] .
Le [MASK] est situé dans le delta plat et bas formé par la confluence du [MASK] et du [MASK] .
Ce dernier est appelé [MASK] dès son entrée en territoire bangladais , et le premier devient la [MASK] dès qu' il rencontre la [MASK] peu avant [MASK] .
La [MASK] , quant à elle , rejoint la [MASK] en aval de la capitale du pays .
Le [MASK] a 58 cours d' eau de part et d' autre de ses frontières internationales , ce qui cause des problèmes politiques liés à l' eau particulièrement difficiles à résoudre ; il partage également des zones ripariennes avec l' [MASK] .
La plus grande partie du [MASK] est à moins de 12 mètres au-dessus du niveau de la mer et environ 10 % du territoire est situé en dessous du niveau de la mer .
[MASK] [MASK] [MASK] , au sud de la ville de [MASK] dans l' extrême sud-est du pays , possède une plage ininterrompue de 120 km de long , la plus longue du monde , .
Situé de part et d' autre du [MASK] [MASK] [MASK] , le [MASK] a un climat de type tropical avec un hiver doux d' octobre à mars , un été chaud et humide de mars à juin , et des moussons de juin à octobre .
Le phénomène d' inondation est accentué par la déforestation des pentes de l' [MASK] , par la forme en entonnoir du [MASK] [MASK] [MASK] , par le relief de plaine du pays , par l' hydrographie du pays ( plus de 90 % du pays est occupé par un delta ) et par le réchauffement climatique .
En 1998 le [MASK] a vu les pires inondations de l' histoire moderne .
En 1970 , le [MASK] [MASK] [MASK] fait 500 000 morts .
Le 15 novembre 2007 , le [MASK] [MASK] a provoqué la mort de 3300 personnes et 1,5 milliard de dollars de dégâts .
Il existe des vestiges d' une civilisation datant d' il y a quatre mille ans dans la région du [MASK] , , alors peuplée de [MASK] , [MASK] -- [MASK] et [MASK] .
Entre 1905 et 1911 il y eut une tentative avortée de diviser la province du [MASK] en deux zones , avec [MASK] capitale de la zone orientale .
Quand l' [MASK] est divisée en 1947 , le [MASK] est également divisé pour des raisons religieuses ; la partie occidentale est donnée à l' [MASK] et la partie orientale devient une province du [MASK] appelée [MASK] [MASK] , avec sa capitale à [MASK] .
Le mouvement de la langue bengalî de 1952 est le premier signe de tension entre les deux parties du [MASK] .
L' insatisfaction à l' égard du gouvernement sur les problèmes économiques et culturels augmente dans la décennie qui suit , pendant laquelle la [MASK] [MASK] émerge comme voix politique de la population bengalophone .
En 1966 son président , [MASK] [MASK] , est emprisonné ; il est libéré en 1969 après une insurrection populaire .
Parmi les cibles les plus importantes on trouve des intellectuels et des hindous ; environ dix millions de réfugiés s' enfuient en [MASK] .
Depuis lors , le [MASK] est à nouveau une démocratie parlementaire .
Cette situation transitoire est une innovation du [MASK] , introduite lors des élections de 1991 puis institutionnalisée en 1996 par le treizième amendement à la constitution .
Les deux principaux partis politiques sont le [MASK] [MASK] [MASK] [MASK] , la [MASK] [MASK] .
Le premier cas d' attentat suicide au [MASK] eut lieu en novembre 2005 .
Le gouvernement intérimaire de [MASK] [MASK] veut réviser la liste des votants et agir contre la corruption .
Les deux candidates principales , [MASK] [MASK] et [MASK] [MASK] [MASK] , sont inculpées de crimes concernant la corruption .
Les forces militaires du [MASK] manifestent également l' intention d' exercer une action politique dans le pays , essayant de changer la constitution pour permettre une participation des militaires à la vie politique .
Le [MASK] est membre du [MASK] depuis son indépendance .
Il a été admis aux [MASK] [MASK] en 1974 .
Le [MASK] est aussi membre de l' [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
Le [MASK] suit une politique modérée de relations internationales mettant l' accent sur la diplomatie multinationale , particulièrement au sein des [MASK] [MASK] .
Dans les années 1980 le [MASK] a tenu un rôle important dans la fondation de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , pour développer ses liens avec d' autres pays du sous-continent indien .
Ses relations internationales les plus importantes et complexes sont celles avec l' [MASK] et le [MASK] .
Il commence également à développer ses liens avec la [MASK] [MASK] [MASK] [MASK] , économiquement et militairement .
Ses relations avec l' [MASK] commencèrent positivement du fait de l' aide apportée par ce pays dans la guerre d' indépendance et pendant la reconstruction .
L' [MASK] a exprimé son inquiétude pour les séparatistes hostiles à l' [MASK] et les militants extrémistes islamistes qui se cacheraient le long de la frontière indo-bangladaise de 4000 km , ainsi que les immigrants clandestins ; l' [MASK] est en train de construire une barrière tout le long de presque toute la frontière .
Toutefois , lors de la réunion annuelle de 2007 de l' [MASK] les deux pays s' engagèrent à coopérer sur des problèmes de sécurité , d' économie et ceux liés à leur frontière commune .
Le [MASK] n' est pas en guerre mais a contribué à la coalition combattant dans la [MASK] [MASK] [MASK] [MASK] en apportant 2300 hommes , et est l' un des premiers pays participant aux forces de maintien de paix de [MASK] [MASK] partout dans le monde .
En mai 2007 , le [MASK] avait des forces déployées en [MASK] [MASK] [MASK] [MASK] , au [MASK] , au [MASK] , à [MASK] et en [MASK] [MASK] [MASK] .
Le [MASK] est organisé en divisions ( bibhags , বিভাগ ) , districts ( zila ou jela , জেলা ) , upazila ou thana ( les gouvernements successifs renomment les unités par l' un ou l' autre terme ) , parishad et villages .
Sa part dans l' exportation du produit vit son apogée lors de la [MASK] [MASK] [MASK] et la fin des années 1940 , oscillant autour de 80 % du marché ; encore dans les années 1970 le jute comptait pour environ 70 % des exportations du pays .
Selon la [MASK] [MASK] , " parmi les obstacles les plus importants à la croissance on trouve la mauvaise gouvernance et la faiblesse des institutions publiques " .
Le [MASK] a connu une croissance en investissement direct à l' étranger .
Plusieurs multinationales , dont [MASK] [MASK] et [MASK] , y ont beaucoup investi , dans le secteur du gaz naturel en priorité .
Un contributeur significatif au développement de l' économie est la propagation massive du microcrédit de [MASK] [MASK] ( qui se vit décerner le [MASK] [MASK] [MASK] [MASK] [MASK] en 2006 pour cette idée ) , à travers le [MASK] [MASK] .
Le [MASK] possède le plus grand centre commercial de l' [MASK] [MASK] [MASK] , " [MASK] [MASK] " , qui se trouve à [MASK] .
Le [MASK] possédait une population estimée à 144 millions d' habitants en 2006 .
En 1961 , le [MASK] comptait un peu plus de 50 millions d' habitants , et en 1981 , un peu moins de 90 millions .
Les minorités sont des peuples à majorité musulmane non bengalis venus d' [MASK] ( principalement du [MASK] ) .
Le trafic d' êtres humains est un problème récurrent au [MASK] , et l' immigration clandestine reste une cause de tension entre le pays et la [MASK] et l' [MASK] .
Parmi ses maîtres on trouve les poètes [MASK] [MASK] et [MASK] [MASK] [MASK] .
Le [MASK] produit environ 80 films par an .
On publie environ 200 journaux quotidiens au [MASK] , ainsi que 1800 périodiques .
Le [MASK] a la quatrième plus peuplée majorité musulmane du monde soit , selon les estimations officielles , 125 millions de personnes , 89,7 % de la population nationale .
Le [MASK] compte onze jours fériés répartis sur les calendriers grégorien , musulman et bengali .
Le [MASK] étant un pays à majorité musulmane , les autres fêtes de cette religion sont également très importantes .
Parmi les fêtes hindoues principales on trouve le [MASK] [MASK] et la [MASK] puja .
Le [MASK] a pour codes :