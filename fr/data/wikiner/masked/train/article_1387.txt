[MASK] est une norme informatique , développée par le [MASK] [MASK] , qui vise à donner à tout caractère de n' importe quel système d' écriture un nom et un identifiant numérique , et ce de manière unifiée , quelle que soit la plate-forme informatique ou le logiciel .
En pratique , la norme [MASK] inclut intégralement la norme [MASK] [MASK] en tant que sous-ensemble , puisque cette dernière ne normalise que les caractères individuels en leur assignant un nom et un numéro normatif et une description informative très limitée , mais aucun traitement ni aucune spécification ou recommandation pour leur emploi dans l' écriture de langues réelles , ce que seule la norme [MASK] définit précisément .
Toutefois , la norme [MASK] [MASK] confère à [MASK] le statut de norme internationale approuvée pour le codage des textes ; [MASK] est également une norme de facto pour le traitement de ces textes , et sert en 2009 de base à de nombreuses autres normes .
[MASK] , dont la première publication remonte à 1991 , a été développée dans le but de remplacer l' utilisation de pages de code nationales .
Le travail sur [MASK] est parallèle et synchronisé avec celui sur la norme [MASK] / [MASK] 10646 dont les buts sont les mêmes .
L' [MASK] [MASK] , une norme internationale publiée en français et en anglais , ne précise cependant ni les règles de composition de caractères , ni les propriétés sémantiques des caractères .
[MASK] aborde cependant la problématique de la casse , du classement alphabétique , et de la combinaison d' accents et de caractères .
Depuis la version 1.1 d' [MASK] et dans toutes les versions suivantes , les caractères ont les mêmes identifiants que ceux de la norme [MASK] [MASK] : les répertoires sont maintenus parallèlement , à l' identique lors de leur normalisation définitive , les deux normes étant mises à jour simultanément .
Les deux normes [MASK] ( depuis la version 1.1 ) et [MASK] [MASK] assurent une compatibilité ascendante totale : tout texte conforme à une version antérieure doit rester conforme dans les versions ultérieures .
La version 3.2 d' [MASK] classait 95,221 caractères , symboles et directives .
La version 4.1 d' [MASK] , mise à jour en novembre 2005 , contient : soit un total de près de 245,000 points de codes assignés dans un espace pouvant contenir 1,114,112 codes différents .
Quelques problèmes semblent cependant exister , pour le codage des caractères chinois , à cause de l' unification des jeux idéographiques utilisés dans différentes langues , avec une calligraphie légèrement différente et parfois signifiante , mais ils sont en cours de résolution par [MASK] qui a défini des sélecteurs de variantes et ouvert un registre de séquences normalisées qui les utilise .
[MASK] est défini suivant un modèle en couches ( Note technique [MASK] # 17 ) .
Par exemple , [MASK] a un jeu de 256 caractères et [MASK] normalise actuellement plus de 120,000 caractères .
En outre , [MASK] leur donne des noms .
Cette définition est totalement identique à celle de l' [MASK] [MASK] , qui approuve toute extension du répertoire .
[MASK] ne reprend dans le texte de sa norme que les noms normatifs en anglais , mais la norme [MASK] [MASK] est publiée en deux langues également normatives .
On notera également qu' [MASK] ( ou [MASK] [MASK] ) a assigné de nombreux points de code à des caractères valides mais dont la sémantique est inconnue car d' usage privé .
Là encore , la normalisation du codage , c' est-à-dire l' assignation des points de codes aux caractères du répertoire commun est une décision conjointe partagée entre les normes [MASK] et [MASK] [MASK] .
Tous les caractères du répertoire disposent d' un point de code unique ( même si pour certaines langues ou pour [MASK] certains caractères sont considérés comme équivalents ) .
Ces caractères abstraits ( de même que les caractères à usage privé ) complètent le jeu de caractères codés du répertoire normalisé pour former un jeu unique dit " jeu de caractères codés universel " qui contient tous les jeux de caractères codés des répertoires de chacune des versions passées , présentes et futures de l' [MASK] [MASK] et d' [MASK] ( depuis la version 1.1 uniquement ) .
Dans le monde [MASK] , on l' utilise rarement , en préférant un marquage explicite .
Contrairement aux normes précédentes , [MASK] sépare la définition du jeu de caractères ( la liste des caractères , leur nom et leur index , le point de code ) de celle du codage .
Ainsi , on ne peut donc pas parler de la taille d' un caractère [MASK] , car elle dépend du codage choisi .
Là où l' ASCII utilisait jadis 7 bits et [MASK] [MASK] 8 bits ( comme la plupart des pages de codes nationales ) , [MASK] , qui rassemble les caractères de chaque page de code , avait besoin d' utiliser plus que les 8 bits d' un octet .
La limite fut dans un premier temps fixée à 16 bits pour les premières versions d' [MASK] , et à 32 bits pour les premières versions de la norme [MASK] [MASK] .
[MASK] et [MASK] [MASK] acceptent plusieurs formes de transformation universelle pour représenter un point de code valide .
Ces transformations ont été initialement créées pour la représentation interne et les schémas de codage des points de code de la norme [MASK] [MASK] , qui au départ pouvait définir des points de code sur 31 bits .
[MASK] a normalisé également de façon très stricte ces trois formes de transformation de tous les points de code valides et uniquement eux , que ce soit pour représenter du texte sous forme de suites de points de codes , ou des points de code assignés aux caractères valides , ou réservés , ou assignés à des non-caractères .
L' [MASK] assure aussi , et c' est son principal avantage , une compatibilité avec les manipulations simples de chaînes en ASCII dans les langages de programmation .
Ainsi , les programmes écrits en [MASK] peuvent souvent fonctionner sans modification .
Aussi les séquences les plus longues en [MASK] nécessitent au maximum 4 octets , au lieu de 6 précédemment .
D' autre part , l' [MASK] est totalement compatible pour la transmission de textes par des protocoles basés sur le jeu de caractères ASCII , ou peut être rendu compatible avec les protocoles d' échange supportant les jeux de caractères codés sur 8 bits .
Son principal défaut est le codage de longueur très variable , même si l' auto-synchronisation propre à l' encodage [MASK] permet de déterminer le début d' une séquence à partir d' une position aléatoire ( en effectuant au plus 3 lectures supplémentaires des codets qui précèdent ) .
L' [MASK] [MASK] nomme ces entités de 16 bits des seizets .
Cette forme est plus économique et plus facile à traiter rapidement que l' [MASK] pour la représentation de textes contenant peu de caractères ASCII .
Historiquement c' était un jeu de caractères codé , qui a été étendu pour supporter l' intégralité du répertoire [MASK] par une transformation algorithmique complétant une large table de mappage d' un code à l' autre .
[MASK] ne code en revanche pas les représentations graphiques des caractères , les glyphes .
Ceci est autorisé et même hautement recommandé car les différentes formes de codage sont classées par [MASK] comme " canoniquement équivalentes " , ce qui signifie que deux formes de codage équivalentes devraient être traitées de façon identique .
La sélection du glyphe correct à utiliser nécessite un traitement permettant de déterminer la forme contextuelle à sélectionner dans la police , alors même que toutes les formes contextuelles sont codées de façon identique en [MASK] .
Pour ces raisons , la police [MASK] doit être utilisée très prudemment .
Au contraire , une police qui ne représente que certains caractères mais qui sait comment les afficher mérite mieux le terme de " police [MASK] " .
Une police de caractères [MASK] est donc seulement une police permettant d' afficher directement un texte codé selon toutes les formes autorisées par [MASK] , et permettant de supporter un sous-ensemble cohérent adapté à une ou plusieurs langues pour supporter une ou plusieurs écritures .
Aucune police de caractère [MASK] ne peut " fonctionner " seule , et le support complet de l' écriture nécessite un support de celles-ci dans un moteur de rendu , capable de détecter les formes de codage équivalentes , rechercher les formes contextuelles dans le texte et sélectionner les différents glyphes d' une police codée avec [MASK] , en s' aidant au besoin de tables de correspondances incluses dans la police elle-même .
La bibliothèque multi plate-forme [MASK] permet de manipuler des données unicodées .
Un support d' [MASK] spécifique à certaines plates-formes ( non compatible quant au code-source ) est également fourni par les systèmes modernes .
Les types à utiliser pour stocker des variables [MASK] , sont les suivants :
[MASK] souffre toutefois encore d' un faible support des expressions rationnelles par certains logiciels , même si des bibliothèques comme [MASK] et [MASK] peuvent les supporter .
Le partitionnement à jour peut être trouvé sur le site officiel d' [MASK] .
Cependant , vu le rôle important d' [MASK] , ( [MASK] [MASK] ) on décrira ici les principaux blocs de caractères .
Les noms français sont les noms officiels de l' [MASK] [MASK] , la norme internationale bilingue qui reprend les mêmes caractères qu' [MASK] .
Toutefois ces points de codes à usage privé sont valides et peuvent être utilisés dans tout traitement automatisé conforme aux normes [MASK] et [MASK] [MASK] , y compris entre systèmes différents s' il existe un accord mutuel privé concernant leur usage .
Leur usage dans le codage de textes transmis entre systèmes ( même si identiques ) est interdit , car il est impossible de les rendre compatibles avec les formes de transformation universelles normalisées les schémas de codage correspondants , et les autres codages normalisés compatibles avec [MASK] et [MASK] [MASK] .
Les autres zones libres ( non assignées à un bloc nommé normalisé , ou les points de code laissés libres et réservés dans les blocs nommés existants ) sont réservés pour un usage ultérieur dans des versions futures d' [MASK] et [MASK] [MASK] , mais sont valides .
[MASK] définit des propriétés par défaut pour les hypothétiques caractères correspondants , afin de préserver la compatibilité des systèmes ( conformes à la norme [MASK] ) avec les futurs textes conformes qui les contiendraient .