La [MASK] fait partie des sept régions ultrapériphériques ( RUP ) de l' [MASK] .
C' est en outre l' un des seuls territoires de l' [MASK] en [MASK] [MASK] [MASK] avec les [MASK] [MASK] .
Son nom officiel est " [MASK] " .
L' ajout de l' adjectif " française " dans les dénominations courantes est une commodité de langage issue de la période coloniale , alors qu' existaient trois [MASK] : la [MASK] , le [MASK] ( [MASK] néerlandaise ) et la [MASK] française .
Le terme " [MASK] " est d' origine indigène .
Dans le dialecte guanao , c' est-à-dire celui de la population amérindienne du delta de l' [MASK] , guai signifierait " nom " , " dénomination " , yana est une négation .
" [MASK] " voudrait donc dire " sans nom " , " ce qu' on ne peut nommer " .
La [MASK] serait donc la terre " qu' on n' ose nommer " , " la terre sacrée " , " la maison de l' être suprême " .
À cette divinité se rattacherait la légende de l' [MASK] ( le doré ) qui n' est une invention européenne , mais une superstition indigène .
Paradoxalement , le terme de [MASK] serait donc une appellation digne de cet [MASK] , longtemps mystérieux , paradis terrestre rêvé par les uns , enfer vert subi par les autres .
La côte de [MASK] fut reconnue par [MASK] [MASK] en 1498 .
En 1503 commencent les premières implantations françaises dans la zone de [MASK] .
La [MASK] restera alors une colonie française jusqu' au 19 mars 1946 , où elle obtient le statut de département d' outre-mer .
[MASK] [MASK] attendait des détenus qu' ils fussent aussi des colons .
Certaines de ces communes , comme [MASK] et [MASK] , ont des superficies supérieures aux départements métropolitains .
Elle est aussi l' une des neuf régions ultrapériphériques de l' [MASK] .
Enfin , la troisième tendance politique , très minoritaire , est l' extrême-gauche indépendantiste , chantre de la guyanité , représentée par le mouvement de décolonisation et d' émancipation [MASK] .
Le 22 janvier 2010 , au lendemain de la défaite des partis indépendantistes lors de la consultation populaire du 10 janvier 2010 sur l' autonomie de la [MASK] , les élus présents en assemblée au conseil général de [MASK] votent à l' unanimité pour la reconnaissance du drapeau de la [MASK] .
La [MASK] est surtout connue pour accueillir , dans la ville de [MASK] , le [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , le [MASK] [MASK] [MASK] ( [MASK] ) .
Le bagne a été aboli mais il subsiste des bâtiments aux [MASK] [MASK] [MASK] , à [MASK] , etc .
La [MASK] est frontalière du [MASK] ( sur 730 km ) , ce qui fait du [MASK] le pays ayant la plus grande frontière terrestre avec [MASK] [MASK] , devant le [MASK] ( sur 510 km ) .
Son chef-lieu est [MASK] .
L' environnement de la frange littorale est celui qui , le long de la [MASK] , a historiquement connu le plus de modifications , mais une forte artificialisation est localement constatée le long de la [MASK] et là où les orpailleurs opèrent à l' ouest de la [MASK] .
La forêt humide de [MASK] s' est paradoxalement épanouie sur un des sols les plus pauvres du monde , en azote , en potassium , en phosphore et en matières organiques .
Les micro-organismes seraient bien plus nombreux encore , notamment dans le nord qui rivalise avec l' [MASK] brésilienne , [MASK] et [MASK] .
Ce seul département français abrite au moins 98 % de la faune vertébrée et 96 % des plantes vasculaires de [MASK] [MASK] .
Les menaces qui pèsent sur l' écosystème sont la fragmentation par les routes , qui reste très limitée comparativement aux autres forêts d' [MASK] [MASK] [MASK] , les impacts immédiats et différés du [MASK] [MASK] [MASK] d' [MASK] , de l' orpaillage , d' une chasse chaotique et du braconnage facilités par la création de nombreuses pistes et l' apparition des quads .
Une ordonnance du 28 juillet 2005 a étendu le code forestier français à la [MASK] , mais avec des adaptations et dérogations importantes .
La moitié de la biodiversité française est en [MASK] : 29 % des plantes , 55 % des vertébrés supérieurs ( mammifères , oiseaux , poissons ... ) et jusqu' à 92 % des insectes .
Il existe peu de lignes aériennes directes à destination des autres pays de l' [MASK] [MASK] [MASK] , mis à part le [MASK] .
En 2006 , la population de la [MASK] était de 206000 habitants .
La [MASK] est le département français où le taux de natalité est le plus élevé et le taux de mortalité est le plus faible .
La population de la [MASK] est en constante augmentation .
Elle devrait passer à 425000 en 2030 ( 600000 selon une hypothèse haute ) , en raison d' un fort taux de croissance naturelle ( excédent des naissances sur les décès ) et sous l' effet d' une immigration importante ( souvent clandestine ) venant des pays limitrophes ( [MASK] , [MASK] , [MASK] , [MASK] ... ) .
La population est essentiellement groupée dans quelques communes sur le littoral , le long de la [MASK] ( bande littorale ) et au bord des grands fleuves et de leurs estuaires .
La communauté asiatique de [MASK] participe également aux défilés en apportant sa touche caractéristique , avec des dragons .
La [MASK] française a pour codes :