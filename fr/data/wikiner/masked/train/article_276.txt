La [MASK] [MASK] de [MASK] , en [MASK] , est le monument emblématique de la préfecture du département d' [MASK] , située à 80 kilomètres au sud-ouest de [MASK] .
Grand lieu de pèlerinage , cette cathédrale et ses tours dominent la ville de [MASK] et la plaine de la [MASK] alentour .
La cathédrale a été parmi les premiers monuments classés au patrimoine mondial par l' [MASK] en 1979 .
Elle est appelée " cathédrale d' [MASK] " , du nom du premier évêque de la ville .
Le 12 juin 858 , cette deuxième cathédrale fut détruite par les pirates [MASK] .
L' évêque [MASK] releva l' église de ses ruines , en style roman .
[MASK] [MASK] [MASK] [MASK] a été classée par [MASK] [MASK] aux trois motifs suivants :
On reconnaît facilement la [MASK] [MASK] [MASK] [MASK] du fait de la grande différence entre ses deux tours : la tour nord a une base de type gothique primitif ( avec contrefort épais et ouverture réduite ) , surmontée d' une flèche flamboyante plus tardive ; en revanche , la tour sud , dotée d' une base plus typiquement gothique , est surmontée d' une flèche très simple .
Cette flèche a fait l' objet de très nombreux commentaires d' artistes et écrivains ( ' unique au monde ' disait d' elle [MASK] [MASK] ) tellement l' impression de ' jaillissement ' est frappante .
Le portail nord est aussi appelé " portail de l' [MASK] " .
Elles représentent des scènes de l' [MASK] [MASK] et de la vie de la [MASK] [MASK] .
Les voussures de la baie centrale évoquent les épisodes de la [MASK] .
Pour la plupart , ils représentent des saints et saintes ou des personnages de la [MASK] : mais aussi de la [MASK] [MASK] de [MASK] [MASK] [MASK] .
Dans les écoinçons , on peut voir les armes de [MASK] [MASK] [MASK] ( château castillan ) et de [MASK] [MASK] ( fleur de lys ) .
Lors de l' incendie de l' ancienne église , en 1194 , on crut que la relique était perdue mais on la retrouva intacte : cela fut interprété comme le fait que la [MASK] [MASK] désirait une plus grande église pour sa relique , et explique peut-être l' enthousiasme et la rapidité avec laquelle la nouvelle cathédrale fut bâtie .
Une expertise du tissu , réalisée en 1927 par le musée des soieries de [MASK] propose une datation ancienne ( premiers siècles ) .
Cependant , il est en soie de grande valeur , ce qui est étonnant au vu du statut social de [MASK] .
La [MASK] [MASK] [MASK] [MASK] est , depuis son édification , un haut lieu de pèlerinage pour les catholiques français ( et avant tout un pèlerinage marial -- ce qui explique notamment l' ampleur du déambulatoire , permettant la circulation des fidèles autour du chœur .
L' un des tableaux les plus connus est celui de [MASK] [MASK] , peint en 1830 .
[MASK] [MASK] a repris le même thème en 1933 .
[MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] , [MASK] , [MASK] , 1961 , 499 p. .