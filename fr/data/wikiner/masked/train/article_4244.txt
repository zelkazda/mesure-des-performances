Elle constitue le centre économique et culturel de la [MASK] .
La ville , dirigée par le maire [MASK] [MASK] , comptait 589141 habitants en l' an 2000 , alors que la zone métropolitaine concentrait environ 5.8 millions d' habitants .
[MASK] est connue pour son excellence culturelle mise en œuvre par ses universités , ses bibliothèques et ses festivals .
Fondée en 1630 , la cité reprend le nom d' une ville anglaise du [MASK] dont sont originaires ses fondateurs anglais .
Aux XVII e siècle et XVIII e siècle , elle se développe et s' enrichit grâce à son port , par les relations commerciales maritimes avec la [MASK] et les [MASK] .
[MASK] devient le chef-lieu de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Elle s' impose aussi comme la capitale intellectuelle de la [MASK] , notamment avec l' ouverture d' [MASK] en 1636 et la naissance de plusieurs journaux .
Mais la cité acquiert également une réputation d' intolérance religieuse lorsqu' elle condamne [MASK] [MASK] , une quaker en 1660 .
Le port exporte du bois , de la farine , de l' huile de baleine , de la viande et du poisson ; les marchands bostoniens reviennent des [MASK] avec du sucre , du rhum , des mélasses et du tafia .
[MASK] joue un rôle central avant et pendant la [MASK] [MASK] contre la [MASK] .
En 1770 , le [MASK] [MASK] [MASK] alimente la rancœur des habitants .
Le 17 juin 1775 s' engage la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) qui se solde par la défaite des insurgés américains .
En 1776 , [MASK] [MASK] conquiert [MASK] , tenue jusqu' ici par les troupes du général britannique [MASK] [MASK] .
Après la [MASK] [MASK] [MASK] , la ville continue à se développer en même temps que le port de commerce international , exportant du rhum , du poisson , du sel et du tabac .
Une charte lui octroie son autonomie municipale en 1822 , et au cours des années 1850 [MASK] devient l' un des plus grands centres manufacturiers des [MASK] , célèbre pour la confection , l' industrie du cuir , la construction navale et la fabrication de machines .
La [MASK] [MASK] [MASK] stimule la production industrielle destinée au ravitaillement des troupes .
La ville reste longtemps dominée par de riches familles dont plusieurs sont toujours présentes à [MASK] .
Leur généalogie remonte aux premiers colons et certaines sont surnommées les " brahmanes de [MASK] " , en allusion au système de castes indien .
Malgré la concurrence de [MASK] [MASK] , [MASK] reste un foyer intellectuel et culturel de premier ordre au XIX e siècle .
La ville accueille de nombreux écrivains américains ( [MASK] [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , etc .
Le 23 août 1927 , les anarchistes italiens [MASK] [MASK] et [MASK] [MASK] sont exécutés .
Pendant la [MASK] [MASK] [MASK] , [MASK] reconvertit son économie pour les besoins de l' industrie de guerre .
Les quelques atouts de [MASK] , d' excellentes banques , ses hôpitaux , ses universités , son savoir-faire technique , comptent alors peu à l' échelle de l' économie des [MASK] .
Dans les années 1960 , 13 femmes sont assassinées par le tueur en série [MASK] [MASK] [MASK] .
[MASK] connaît un renouveau économique depuis les années 1970 .
À ce moment , l' importance des institutions financières dans l' économie américaine s' accroît , beaucoup de particuliers plaçant leur épargne en bourse et [MASK] se développe dans le secteur financier .
Alors que le poids des dépenses de santé augmente aux [MASK] , de nombreux hôpitaux de la ville dégagent des bénéfices .
L' agglomération devient le deuxième pôle américain pour les hautes technologies ( informatique , biotechnologies ) , derrière la [MASK] [MASK] californienne .
La construction de nouveaux gratte-ciel dans le quartier des affaires témoigne du réveil économique de [MASK] .
Elle est entourée par les villes de [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , et [MASK] .
Plusieurs autres villes en périphérie constituent le [MASK] [MASK] .
La baie du [MASK] offrait un abri en eaux profondes pour les navires et son site péninsulaire lui donnait une défense naturelle .
À l' ouest s' étendaient des marais envahis par la marée : de nos jours , cette partie correspond au quartier de [MASK] [MASK] .
Le centre de la [MASK] coloniale se trouvait autour de l' [MASK] [MASK] [MASK] .
Tout comme [MASK] [MASK] , [MASK] est aujourd'hui en grande partie implantée sur des terre-plein artificiels qui ont fait disparaître son caractère péninsulaire .
Avec le percement du [MASK] [MASK] au début du XIX e siècle , [MASK] perd son avantage au profit de [MASK] [MASK] .
La croissance du trafic dans le bassin du [MASK] et des [MASK] [MASK] éclipse également l' influence de [MASK] .
Aujourd'hui , la [MASK] [MASK] sépare [MASK] de [MASK] et [MASK] .
À l' est de la ville se trouvent le port de [MASK] et ses îles .
Plus du quart du territoire de la ville est sous le niveau de la mer , qu' il s' agisse de la [MASK] [MASK] ou du quartier du port .
Le climat de [MASK] est à l' image de celui de la [MASK] : il s' agit d' un climat tempéré de façade orientale , qui se caractérise par une amplitude thermique relativement importante .
La position de [MASK] expose la ville aux flux méridiens froids en hiver et chauds en été , qui apportent des perturbations .
Tout au long de son histoire , l' urbanisme de [MASK] a connu d' importants bouleversements , liés à la croissance démographique et économique de la cité .
La capitale du [MASK] a su préserver son patrimoine historique et s' adapter aux besoins de la modernité .
Sa prospérité économique au XIX e siècle permet aux élites enrichies de se faire construire de belles demeures victoriennes à [MASK] [MASK] .
L' urbanisme des premières décennies du XIX e siècle est marqué par les réalisations de l' architecte [MASK] [MASK] : celui-ci transforme la ville coloniale en une cité américaine moderne .
Il dessine plusieurs maisons en briques , notamment à [MASK] [MASK] , ainsi que la [MASK] [MASK] [MASK] , en style néoclassique .
À la fin du XIX e siècle , le quartier de [MASK] [MASK] est complètement poldérisé ; la hauteur des maisons est limitée par une législation stricte .
Entre 1630 et 1890 , la superficie de [MASK] est multipliée par trois .
L' architecte-paysager [MASK] [MASK] [MASK] ( 1822 -- 1903 ) a fait plusieurs parcs , appelés le " collier d' émeraude " .
En 1910 , l' achèvement d' un barrage crée le bassin de la [MASK] [MASK] , au nord-ouest .
Jusque là , [MASK] n' avait aucun édifice très élevé , à part les bâtiments administratifs et les clochers des églises .
Le premier est la [MASK] [MASK] ( 1964 , 229 mètres ) .
D' autres buildings sortent de terre dans les années 1970 : le [MASK] [MASK] [MASK] ( 183 m .
) , la [MASK] [MASK] [MASK] ( 241 m . )
ou encore le [MASK] [MASK] [MASK] [MASK] ( 196 m . ) .
La municipalité tente de pallier ces difficultés de circulation en faisant la promotion des moyens de transports en commun , mais aussi en perçant des tunnels comme le [MASK] [MASK] .
Au début du XVIII e siècle , [MASK] était l' une des villes les plus peuplées des treize colonies britanniques .
Elle est notamment dépassée par [MASK] [MASK] dans la deuxième moitié du XVIII e siècle .
En 1690 , [MASK] compte 7000 habitants , en 1742 , environ 16000 .
Le retournement de situation reste fragile car , entre 2000 et 2005 , la ville de [MASK] a perdu 30107 résidents .
La densité y est relativement élevée ( 4696 hab./km² contre 3127 hab./km² à [MASK] , [MASK] par exemple ) , ce qui la rapproche des villes européennes .
[MASK] est devenue une ville cosmopolite au XIX e siècle .
Elle est alors avec [MASK] [MASK] l' une des portes d' entrée aux [MASK] pour de nombreux [MASK] .
Le revenu moyen des ménages bostoniens s' élève à 55183 $ en 2004 : il est inférieur à celui de [MASK] [MASK] mais plus élevé que celui de [MASK] [MASK] .
[MASK] souffre des mêmes maux que les autres villes-centres américaines : certains quartiers connaissent une grande pauvreté , en particulier dans la communauté afro-américaine de [MASK] .
Chaque année plusieurs festivals de cinéma se tiennent à [MASK] : le plus important est le festival du film de [MASK] qui dure une semaine , en général au début du mois de septembre .
Dans le domaine de la musique , [MASK] organise un festival de musique baroque depuis 1980 qui attire les spécialistes du monde entier .
[MASK] a inspiré de nombreux réalisateurs de cinéma : plusieurs films reprennent les caractères originaux de la ville .
Ainsi , les personnages principaux de [MASK] [MASK] , réalisé par [MASK] [MASK] ( 2003 ) , ont été élevés dans la communauté irlandaise .
[MASK] [MASK] ( [MASK] [MASK] , 2006 ) évoque le quartier irlandais de [MASK] [MASK] .
Dans [MASK] [MASK] [MASK] des [MASK] [MASK] ( 2005 ) , l' héroïne tombe amoureuse d' un fan des [MASK] [MASK] [MASK] [MASK] .
Récemment , dans [MASK] [MASK] [MASK] , réalisé par [MASK] [MASK] avec son frère cadet [MASK] dans le rôle principal , on retrouve les mêmes quartiers " durs " de [MASK] que dans [MASK] [MASK] : pas de hasard , l' auteur est le même [MASK] [MASK] .
Par ailleurs , [MASK] a été rendue célèbre dans le monde grâce à la télévision .
Trois séries judiciaires créées par [MASK] [MASK] [MASK] ont pour cadre [MASK] : [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .
La série [MASK] raconte le quotidien d' un bar de [MASK] .
[MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] a pour décor un palace de la ville .
Les intrigues de deux séries policières , [MASK] [MASK] [MASK] [MASK] et [MASK] se déroulent également dans les rues de [MASK] .
La série [MASK] [MASK] [MASK] [MASK] [MASK] joue sur la proximité de [MASK] .
Plus récemment , la série [MASK] se déroule également à [MASK] .
L' évolution de la littérature à [MASK] est le reflet de l' histoire de la ville .
Les premières formes de littérature se manifestent dans les écrits des pasteurs de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
[MASK] [MASK] ( 1663-1728 ) est représentatif de ces premiers auteurs : il laissa une œuvre écrite de plus de 450 livres et pamphlets .
Le courant abolitionniste se développe parmi les auteurs résidant à [MASK] : [MASK] [MASK] ( 1652 -- 1730 ) ou [MASK] [MASK] [MASK] ( 1780 -- 1842 ) ont écrit contre l' esclavage .
[MASK] [MASK] , ( 1753-1784 ) a vécu à [MASK] ; elle est considérée comme la première poétesse afro-américaine des [MASK] .
En 1770 , elle écrivit un hommage poétique au calviniste [MASK] [MASK] , qui eut une large audience à [MASK] .
Au XIX e siècle , de nombreux écrivains américains ( [MASK] [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , etc . )
habitent dans l' agglomération de [MASK] .
Le poète et romancier [MASK] [MASK] [MASK] ( 1809-1849 ) est né à [MASK] .
[MASK] [MASK] , un roman d' [MASK] [MASK] ( 1843-1916 ) , aborde le thème du rôle des femmes dans la société de la fin du XIX e siècle .
Une partie du roman [MASK] [MASK] [MASK] [MASK] [MASK] ( 1929 ) de [MASK] [MASK] ( 1897-1962 ) , se déroule à [MASK] .
[MASK] [MASK] choisit la capitale du [MASK] pour faire évoluer les personnages de son roman [MASK] ( 2006 ) .
Enfin , l' ensemble des romans policiers de [MASK] [MASK] , dont [MASK] [MASK] adapté au cinéma par [MASK] [MASK] , et [MASK] [MASK] [MASK] réalisé par [MASK] [MASK] avec son frère cadet [MASK] dans le rôle principal , se déroulent à [MASK] .
L' académie de musique de [MASK] est la compagnie d' opéra la plus célèbre de la ville .
Le conservatoire de musique de [MASK] est un lieu important de la vie culturelle .
L' [MASK] [MASK] [MASK] [MASK] ( The [MASK] [MASK] [MASK] ) est une autre institution dynamique de l' agglomération .
En été des orchestres de rue se produisent à [MASK] [MASK] , [MASK] [MASK] ou sur l' esplanade de la [MASK] [MASK] .
[MASK] , [MASK] et [MASK] [MASK] popularisés au courant des années 1980 par leur mélodies électro-pop accrocheuses .
Le groupe [MASK] [MASK] [MASK] est originaire de [MASK] .
Enfin , le groupe précurseur du grunge les [MASK] vient également de la capitale du [MASK] .
En 1895 , l' architecte [MASK] [MASK] [MASK] , se voit confier le projet de la bibliothèque sur le [MASK] [MASK] .
En 1972 , le site est encore agrandi grâce aux travaux de [MASK] [MASK] .
Le [MASK] [MASK] sert de lieu d' exposition et de bibliothèque : elle conserve plus de 500000 documents , parmi lesquels de vieux livres , des manuscrits et des photographies .
Fondé en 1870 et ouvert au public en 1876 , le [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] [MASK] [MASK] [MASK] [MASK] ) est le principal musée de la ville .
Ses collections , riches de plus de 2500 œuvres et objets , illustrent différentes époques de l' art , de l' [MASK] au XIX e siècle .
L' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] propose des expositions temporaires dans une ancienne station de police et de pompier .
Le [MASK] [MASK] [MASK] [MASK] propose des activités ludiques et pédagogiques .
D' autres musées attendent les visiteurs sur le [MASK] [MASK] .
Parmi elles , les 65 colleges et universités font de [MASK] une ville étudiante .
Cependant , le [MASK] [MASK] [MASK] [MASK] ( [MASK] ) et [MASK] ne se trouvent pas dans les limites de la ville , mais sont installés à [MASK] , sur l' autre rive de la [MASK] [MASK] .
L' [MASK] [MASK] [MASK] , fondée en 1869 , est aujourd'hui la quatrième plus grande université du pays avec environ 30000 étudiants et le second employeur de la ville .
L' [MASK] [MASK] [MASK] est un établissement d' enseignement supérieur public situé dans le quartier de [MASK] .
Les publications médicales de [MASK] sont deux fois plus nombreuses que celles de [MASK] [MASK] entière .
La ville dispose d' une importante concentration de laboratoires et de lieu de formation en relation avec la santé : les écoles de médecine de [MASK] et de l' [MASK] [MASK] [MASK] ne sont pas à [MASK] mais bien à [MASK] .
C' est à [MASK] qu' eut lieu , en 1954 , la première greffe de rein .
L' histoire de l' immigration à [MASK] permet d' expliquer la diversité des confessions présentes dans la ville .
[MASK] est le siège d' un archevêché catholique .
En 2002 , la ville est secouée par le scandale des abus sexuels dans le clergé catholique , qui a mené à la destitution de l' archevêque [MASK] [MASK] .
Les institutions municipales de [MASK] sont réglementées par une charte .
Dans le passé , [MASK] a montré à la fois son attachement aux valeurs conservatrices des puritains , mais aussi sa capacité d' ouverture et de non-conformisme : pendant la guerre de [MASK] , [MASK] fut le foyer le plus actif de la lutte antiesclavagiste .
La ville a souvent été le lieu de la contestation pacifique , pendant la [MASK] [MASK] [MASK] [MASK] ou la [MASK] [MASK] [MASK] .
Depuis 1929 , tous les maires de [MASK] sont de cette tendance politique .
En 2004 a eu lieu la convention démocrate pour l' élection présidentielle du candidat [MASK] [MASK] .
Ce dernier possède une maison dans le quartier de [MASK] [MASK] .
[MASK] est un centre de décision : elle est le chef-lieu du [MASK] [MASK] [MASK] , le centre de l' administration américaine pour la [MASK] et le siège du premier district de la [MASK] [MASK] [MASK] [MASK] .
Enfin , [MASK] est le quartier général du premier district naval .
L' économie de [MASK] a d' abord reposé sur le commerce transatlantique et le cabotage .
Le trafic des marchandises avec les [MASK] est important : au XVIII e siècle , [MASK] exporte du bois , de la farine , de l' huile de baleine , de la viande et du poisson ; ses marchands reviennent avec du sucre , du rhum , des mélasses et du tafia ) .
En 1906 , [MASK] [MASK] voit le jour à [MASK] .
Le secteur secondaire a connu une crise importante après la [MASK] [MASK] [MASK] et a vu fondre ses effectifs .
Elles ont permis la reconversion industrielle de l' agglomération de [MASK] dans les années 1980 et ont fourni de nombreux emplois .
La ville reste également l' un des principaux foyers d' édition et d' imprimerie d' [MASK] [MASK] [MASK] ( voir le paragraphe sur les médias ci-dessous ) .
Les services bancaires et financiers se concentrent dans le quartier des affaires avec [MASK] [MASK] ou le siège régional de [MASK] [MASK] [MASK] .
La ville de [MASK] dispose également d' un des plus grands marchés de gestions d' actifs au monde .
Le nombre de journaux se multiplie après la [MASK] [MASK] [MASK] : le [MASK] [MASK] est fondé en 1872 par des hommes d' affaires .
[MASK] [MASK] [MASK] , diffusé à 435000 exemplaires , représente le grand quotidien de la [MASK] .
En 1993 , il a été racheté par le [MASK] [MASK] [MASK] .
Si [MASK] bénéficie surtout d' une réputation de ville intellectuelle , cela ne l' empêche pas d' avoir aussi de grandes équipes sportives .
En base-ball , la rivalité est exacerbée entre les [MASK] [MASK] et les [MASK] [MASK] [MASK] [MASK] .
Les [MASK] ont le record historique de titres [MASK] ( 17 ) dont celui de 2008 en finale , une fois de plus contre les [MASK] [MASK] [MASK] [MASK] .
La ville compte de nombreux équipements sportifs : le [MASK] [MASK] ( ou [MASK] [MASK] [MASK] ) reçoit les deux grandes équipes des [MASK] [MASK] [MASK] et des [MASK] [MASK] [MASK] .
Le [MASK] [MASK] , construit en 1912 dans le [MASK] [MASK] , est l' un des plus vieux stades en activité du pays .
Il est le domicile des [MASK] [MASK] [MASK] [MASK] et peut accueillir un peu plus de 38000 spectateurs .
Le [MASK] [MASK] , inauguré en 2002 , ne se trouve pas à [MASK] même , mais à [MASK] .
Dès le XVII e siècle , [MASK] est rattachée au reste du monde par des liaisons transatlantiques et un système de ferrys constitue les premiers transports en commun .
Dans les années 1960 , le [MASK] [MASK] [MASK] [MASK] ( [MASK] ) est fondé pour s' occuper du réseau de transport en commun .
L ' [MASK] [MASK] [MASK] connecte [MASK] au reste du pays .
Il se trouve à quelques kilomètres dans le quartier d' [MASK] [MASK] .
Le réseau est géré par la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) et il comprend 13 lignes sur près de 750 km de voies ; il est utilisé chaque jour par 140000 personnes en moyenne .
Il existe deux grandes gares ferroviaires : [MASK] [MASK] est l' une des plus importantes de la ville , avec ses 13 quais et ses liaisons avec le bus et le métro .
La [MASK] [MASK] représente un tronçon de l' [MASK] , qui ceinture la ville à l' ouest .
La route 1 et l' [MASK] parcourent l' agglomération du nord au sud .
Face aux problèmes d' embouteillages , l' artère centrale qui traverse le centre-ville a été remplacée par un tunnel appelé [MASK] [MASK] .
Les personnages historiques les plus célèbres nés à [MASK] sont l' écrivain , physicien et diplomate [MASK] [MASK] ( 1706-1790 ) , le poète [MASK] [MASK] [MASK] ( 1803-1882 ) et enfin l' écrivain [MASK] [MASK] [MASK] ( 1809-1849 ) .
[MASK] est également le berceau de nombreux hommes politiques tels que [MASK] [MASK] ( 1722-1803 ) ou [MASK] [MASK] [MASK] ( 1925-1968 ) .
D' autres politiciens sont originaires de l' agglomération ou apparentés aux grandes familles bostoniennes : le président américain [MASK] [MASK] [MASK] descend de la famille [MASK] par sa mère ; [MASK] [MASK] [MASK] est né dans la banlieue sud de [MASK] .
Les peintres [MASK] [MASK] [MASK] ( 1738-1815 ) , [MASK] [MASK] ( 1836-1910 ) et [MASK] [MASK] ( 1859-1935 ) , l' architecte [MASK] [MASK] ( 1856-1924 ) sont également natifs de la cité .
Liste des villes jumelées à [MASK] :