L ' [MASK] est une constellation située à peu près sur l' équateur céleste .
La [MASK] [MASK] [MASK] [MASK] fait partie du triangle d' été .
Elle se reconnaît directement par l' alignement de trois étoiles dont [MASK] [MASK] ( [MASK] ) est le centre , et β et [MASK] [MASK] les extrémités .
Le triangle dont les sommets sont [MASK] [MASK] ( sommet de la tête ) , [MASK] [MASK] ( bout de l' aile ) et [MASK] [MASK] ( centre du corps ) est sensiblement équilatéral , et marqué par des petits alignements intermédiaires d' étoiles assez brillantes .
De là , on remonte l' aile vers l' ouest par un petit alignement de trois étoiles qui passe par [MASK] [MASK] ( variable ) et tombe sur [MASK] [MASK] , au centre du corps de l' [MASK] .
Dans l' alignement du " corps " , on peut voir [MASK] [MASK] entre [MASK] [MASK] et [MASK] [MASK] .
Dans le prolongement du " corps " , on tombe sur [MASK] [MASK] , qui marque le bout de la queue .
À côté d' elle [MASK] [MASK] , l' extrémité de l' aile .
( Ces deux étoiles s' appellent [MASK] [MASK] [MASK] , ce qui signifie en fait " la queue du faucon " ) .
Le bout de l' aile [MASK] [MASK] -- [MASK] [MASK] pointe vers [MASK] [MASK] , sur le cou , et de là vers [MASK] [MASK] .
L' autre aile pointe après 5° sur le bout du pied d' [MASK] .
En face de la tête de l' aigle , au nord-est , on peut voir la forme très caractéristique du [MASK] , petit groupe d' étoiles assez serré , formant un quadrilatère .
Au nord de la tête , on reconnaît la [MASK] .
Dans le prolongement des deux étoiles rapprochées qui forment la queue de l' [MASK] , et à l' ouest de cette queue , on trouve l' [MASK] [MASK] [MASK] , une petite constellation peu lumineuse .
Cet alignement passe par le cœur du [MASK] et par sa tête , pour venir toucher [MASK] de la [MASK] , puis [MASK] .
L' alignement [MASK] -- [MASK] permet de repérer 20° plus au sud α du [MASK] et les deux " pieds " du capricorne 15° plus loin ; et pour les observateurs situés suffisamment au sud , cet alignement se prolonge jusqu' à [MASK] [MASK] ( α [MASK] ) , à une soixantaine de degrés d' [MASK] .
Dans l' axe du " corps " de l' aigle , on tombe sur la queue du [MASK] , à une quarantaine de degrés vers le sud .
Deux novae majeures ont été observées dans l' [MASK] ; la première en 389 av .
Située juste sur la [MASK] [MASK] , l' [MASK] recèle de nombreuses étoiles brillantes .
Les deux autres étoiles de cette ligne sont [MASK] et [MASK] ( β et [MASK] [MASK] ) .
[MASK] , une étoile de la séquence principale , tourne rapidement sur elle-même en 6,5 heures .
Sa vitesse de rotation est si grande ( elle atteint à l' équateur 210 km/s , soit plus de 200 fois celle du [MASK] ) que sa forme est celle d' un ellipsoïde nettement aplati .
[MASK] est une étoile double .
Comme [MASK] , il s' agit d' un système double .
[MASK] [MASK] est une étoile dont le nom tradionnel usuel ne vient pas de l' arabe .
[MASK] [MASK] est une étoile géante , 2960 fois plus brillante que [MASK] [MASK] et 110 fois plus large .
[MASK] [MASK] est l' une des plus brillantes céphéides du ciel , une supergéante qui peut atteindre la brillance de sa voisine [MASK] [MASK] .
Antinoüs fut intégrée définitivement à l' [MASK] par [MASK] [MASK] .