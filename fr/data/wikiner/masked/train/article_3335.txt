L' émancipation des femmes et le tiers-mondisme conduisent [MASK] [MASK] à s' engager en politique .
Après avoir été militante trotskyste et membre de la [MASK] , elle rejoint le [MASK] [MASK] en 1975 , suite à la campagne présidentielle de [MASK] [MASK] .
Elle milite et s' implique progressivement dans ce parti aux côtés de [MASK] [MASK] .
En 1977 , à 25 ans elle est élue conseillère municipale sur la liste d' [MASK] [MASK] , qui en fera une de ses adjointes six ans plus tard .
En 1995 , [MASK] [MASK] la choisit comme première adjointe à la mairie , poste qu' elle occupera jusqu' au décès de celui-ci en mai 2006 .
Par reconnaissance de son travail accompli lors de ces deux derniers mandats de parlementaire elle a été nommée par ses pairs à la fin du mois de juin 2007 , vice-présidente du groupe socialiste à l' [MASK] [MASK] , en charge des transports .
[MASK] [MASK] s' est constamment impliquée dans la vie de sa formation politique .
En 1979 , à l' âge de 27 ans , elle est élue première secrétaire fédérale des [MASK] et devient ainsi la première femme à accéder à cette fonction dans ce département et ce , pendant quatre ans .
[MASK] [MASK] a occupé le poste de secrétaire nationale du [MASK] [MASK] en charge de la vie associative et acteurs sociaux .
Elle était la seule candidate en lice , le maire sortant [MASK] [MASK] ayant décidé de mener une liste dissidente soutenue par l' [MASK] , ce qui entraîna de fait son exclusion du [MASK] [MASK] .
Le chanteur [MASK] et l' écrivain [MASK] [MASK] soutiennent la candidate .
[MASK] [MASK] est élue à la mairie de [MASK] en obtenant 39,76 % des suffrages exprimés au second tour le 16 mars 2008 devançant le président du [MASK] [MASK] [MASK] ( 38,81 % ) de 342 voix sur un total de plus de 36000 .
Le candidat soutenu par l' [MASK] [MASK] [MASK] , maire sortant qui s' est maintenu au second tour , obtient quant à lui 21,42 % des suffrages .
L' émancipation des femmes dans les années 1970 est l' une des raisons qui a conduit [MASK] [MASK] à s' engager en politique .
Le 30 mai 2001 a été votée la loi relative à l' interruption volontaire de grossesse et à la contraception , dont [MASK] [MASK] a été rapporteur au nom de la commission des affaires culturelles .
Ses années de travail comme secrétaire nationale du [MASK] [MASK] en charge de la vie associative et des acteurs sociaux l' ont convaincue de l' importance de cette nouvelle forme d' engagement citoyen [ réf. souhaitée ] .
Ces soirées , une centaine depuis 1997 , sont l' occasion pour les habitants d' engager un débat avec [MASK] [MASK] mais aussi avec l' intervenant invité ( conseiller auprès d' un ministre , professionnel , philosophe , député … ) .
[MASK] [MASK] a fait partie de la mission parlementaire sur l' effet de serre qui a rendu ses conclusions en avril 2006 , après avoir auditionné près de 200 personnes ( scientifiques , associations , administrations … ) et fait des recommandations .
Durant le 1er semestre 2006 , pour comprendre les enjeux du développement durable , parallèlement aux travaux de la mission parlementaire , [MASK] [MASK] a organisé dans sa circonscription , un cycle de débats : " Quel devenir pour notre planète ?
[ citation nécessaire ] " Dans la continuité de la ligne à grande vitesse qui arrivera de [MASK] , nous devons avoir une voie ferrée performante vers l' [MASK] , traversant les [MASK] .
[MASK] [MASK] s' inscrit dans la tradition europhile du [MASK] .
Pour elle , coopération transfrontalière et développement local vont de pair et empêcher [MASK] et le piémont pyrénéen de " demeurer au centre d' un mouvement centrifuge entre [MASK] et [MASK] " .
Elle était présidente de son comité de soutien dans les [MASK] .
Elle adhérait au discours novateur [ réf. nécessaire ] de [MASK] [MASK] sur les institutions , les pratiques politiques et la nécessité d' associer les citoyens aux processus de décision .