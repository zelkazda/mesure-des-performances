Né dans une riche famille de [MASK] , il fait d' abord des études de rhétorique qui l' amènent à s' installer à [MASK] seulement à l' âge adulte .
Malgré ses convictions républicaines , il est très ami avec [MASK] qu' il va d' ailleurs aider dans son entreprise de réhabilitation de la grandeur de [MASK] .
Il devint par la suite le précepteur de l' empereur [MASK] dont il encouragea les penchants pour l' histoire .
Sur certains texte , [MASK] est marquée en bas du texte
Ils nous donnent une idée du plan suivi par [MASK] et de la manière dont il avait réparti la matière qu' il a traitée .
Dans la préface , il est dit " Quant aux récits relatifs à la [MASK] [MASK] [MASK] ou antérieurs à sa fondation , je ne cherche ni à les donner pour vrais ni à les démentir : leur agrément doit plus à l' imagination des poètes qu' au sérieux de l' information " .
Il se montre critique vis-à-vis de ce qu' il juge comme une décadence de [MASK] ( qui n' a pas encore atteint son apogée ) et exalte les valeurs qui ont fait la [MASK] éternelle .
[MASK] , bien que républicain , est aidé dans son travail par la famille impériale. [MASK] a effectivement compris que cette évocation des hauts faits de [MASK] pouvait servir son pouvoir [ réf. nécessaire ] .
Il s' agit non seulement d' élever " un monument à la gloire de [MASK] " mais surtout de rappeler l' ancienneté et la continuité sans faille de l' histoire de la ville .
Enfin , [MASK] ne possède pas la méthode scientifique de [MASK] ou de [MASK] .