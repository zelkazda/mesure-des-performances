Le principe de la fibre optique a été développé dans les années 1970 dans les laboratoires de l' entreprise américaine [MASK] [MASK] [MASK] .
Dans les réseaux informatiques du type [MASK] , pour la relier à d' autres équipements , on peut utiliser un émetteur-récepteur .
À l' époque des [MASK] [MASK] , le phénomène du transport de la lumière dans des cylindres de verre était déjà connu .
On doit la première tentative de communication optique à [MASK] [MASK] [MASK] , connu pour l' invention du téléphone .
Le premier système de communication téléphonique optique fut installé au centre-ville de [MASK] en 1977 .