Il prononce un discours célèbre le 28 août 1963 devant le [MASK] [MASK] à [MASK] durant la marche pour l' emploi et la liberté : " [MASK] [MASK] [MASK] [MASK] " ( Je fais un rêve ) .
Il est soutenu par [MASK] [MASK] [MASK] dans la lutte contre la discrimination raciale ; la plupart de ces droits seront promus par le " [MASK] [MASK] [MASK] " et le " [MASK] [MASK] [MASK] " sous la présidence de [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] devient le plus jeune lauréat du [MASK] [MASK] [MASK] [MASK] [MASK] en 1964 pour sa lutte non violente contre la ségrégation raciale et pour la paix .
Il commence alors une campagne contre la [MASK] [MASK] [MASK] [MASK] et la pauvreté , qui prend fin en 1968 avec son assassinat officiellement attribué à [MASK] [MASK] [MASK] , dont la culpabilité et la participation à un complot sont toujours débattues .
En 1939 , il chante avec le chœur de son église à [MASK] pour la première du film [MASK] [MASK] [MASK] [MASK] [MASK] .
Il entre à l' âge de 15 ans à [MASK] [MASK] , une université réservée aux garçons noirs , après avoir sauté deux années de lycée et sans avoir officiellement obtenu son certificat de graduation .
Des accusations de plagiat contre sa thèse de doctorat à l' [MASK] [MASK] [MASK] aboutissent en 1991 à une enquête officielle des responsables de cette université .
Ceux-ci concluent qu' un tiers environ de la thèse est plagié d' un article écrit par un étudiant diplômé antérieurement , mais il est décidé de ne pas retirer son titre à [MASK] [MASK] , car la thèse constitue tout de même " une contribution intelligente au savoir " .
En 1953 , [MASK] [MASK] [MASK] devient le pasteur de l' église baptiste de l' avenue [MASK] à [MASK] ( [MASK] ) .
[MASK] [MASK] est arrêté durant cette campagne qui dure 382 jours et qui devient extrêmement tendue à cause de ségrégationnistes blancs qui ont recours au terrorisme : la maison de [MASK] [MASK] [MASK] est attaquée à la bombe incendiaire le matin du 30 janvier 1956 , ainsi celle de [MASK] [MASK] et quatre églises .
En 1957 , il joue un rôle capital dans la fondation de la [MASK] [MASK] [MASK] [MASK] , dont il est élu président et le reste jusqu' à sa mort .
[MASK] [MASK] [MASK] échappe de peu à la mort , la lame du coupe-papier utilisé ayant frôlé l' aorte .
[MASK] [MASK] pardonne à son agresseur et dans une déclaration à la presse souligne la violence de la société américaine :
Le [MASK] commence à mettre [MASK] [MASK] [MASK] sur écoute en 1961 , craignant que des communistes essayent d' infiltrer le mouvement des droits civiques .
La plupart de ces droits ont été votés comme lois avec le [MASK] [MASK] [MASK] [MASK] [MASK] et le [MASK] [MASK] [MASK] de 1965 .
[MASK] [MASK] et le [MASK] appliquent avec succès les principes de manifestation non-violente en choisissant stratégiquement les lieux et la méthode de protestation qui aboutissent à des confrontations spectaculaires avec les autorités ségrégationnistes .
[MASK] [MASK] [MASK] revient en juillet 1962 , et est condamné à 45 jours de prison ou 178 $ d' amende .
Lors d' une manifestation , des jeunes noirs jettent des pierres sur la police : [MASK] [MASK] [MASK] demande une halte de toutes les protestations et un " jour de pénitence " pour promouvoir la non-violence et maintenir le moral .
En 1960 , la population de [MASK] est de 350000 personnes , à 65 % blanches et 35 % noires .
C' est alors une des villes qui maintient et assure par la loi locale la plus grande ségrégation raciale des [MASK] dans tous les aspects de la vie , aussi bien dans les établissement publics que privés .
[MASK] n' a ni officier de police noir , ni pompier , ni vendeur en magasin , ni conducteur ou employé de banque , l' emploi pour la population noire est limité aux seuls travaux manuels aux aciéries .
Les églises noires où les droits civiques sont discutés sont des cibles privilégiées et la ville est particulièrement violente contre les [MASK] [MASK] .
Un responsable des droits civiques local , le [MASK] [MASK] essaye bien de lutter en gagnant en justice la déségrégation des parcs de la ville , mais la ville réagit en les fermant .
Alors que la campagne n' a plus assez de volontaires , les organisateurs , malgré les hésitations de [MASK] [MASK] [MASK] , recrutent des étudiants et des enfants dans ce qui est appelé par les média " la croisade des enfants " .
En réaction et malgré les instructions du [MASK] , des parents et des passants commencent à jeter des projectiles sur la police mais sont raisonnés par les organisateurs .
La décision d' utiliser des enfants même dans une manifestation non-violente est très critiquée , entre autres par le ministre de la justice [MASK] [MASK] [MASK] et l' activiste [MASK] [MASK] qui déclare que " les vrais hommes ne mettent pas leurs enfants dans la ligne de mire " .
Il est l' un de ceux qui acceptent le souhait du président [MASK] [MASK] [MASK] de changer le message de la marche .
Le président , qui avait déjà soutenu publiquement [MASK] [MASK] [MASK] et était déjà intervenu plusieurs fois pour le faire sortir de prison , s' était initialement opposé au principe de la marche car il craignait un impact négatif sur le vote de la loi sur les droits civiques .
Certains activistes des droits civiques pensent alors que la marche ne présente plus qu' une vision inexacte et édulcorée de la situation des noirs ; [MASK] [MASK] l' appelle alors " La farce sur [MASK] " , et les membres de l' organisation [MASK] [MASK] [MASK] qui participent à la marche seront suspendus temporairement .
Plus de 250000 personnes de toutes les ethnies se réunissent le 28 août 1963 face au [MASK] , dans ce qui est la plus grande manifestation ayant eu lieu jusque là dans l' histoire de la capitale américaine .
Cette déclaration est considérée comme un des meilleurs discours de l' histoire américaine avec le [MASK] [MASK] d' [MASK] [MASK] .
En mai et juin 1964 , une action directe est menée par [MASK] [MASK] [MASK] et d' autres dirigeants des droits civiques .
La photographie d' un policier plongeant pour arrêter un manifestant et celle du propriétaire du motel versant de l' acide chlorhydrique dans la piscine pour faire sortir les activistes firent le tour du monde et servirent même aux états communistes pour discréditer le discours de liberté des [MASK] .
Les manifestants endurent les violences physiques et verbales sans riposter , ce qui entraîne un mouvement de sympathie nationale et aide au vote du [MASK] [MASK] [MASK] le 2 juillet 1964 .
Le 14 octobre 1964 , [MASK] [MASK] [MASK] devient le plus jeune lauréat du [MASK] [MASK] [MASK] [MASK] [MASK] pour avoir mené une résistance non violente dans le but d' éliminer les préjudices raciaux aux [MASK] .
[MASK] est alors un lieu important pour la défense du droit de vote des afro-américains .
Le dimanche 7 mars 1965 , 600 défenseurs des droits civiques quittent [MASK] pour tenter de rejoindre [MASK] , la capitale de l' état , pour présenter leurs doléances au moyen d' une marche pacifique .
Ce jour sera connu sous le nom de " [MASK] [MASK] " et marqua un tournant dans la lutte pour les droits civiques .
Les reportages montrant les violences policières permettent au mouvement de gagner le soutien de l' opinion publique et soulignent le succès de la stratégie non-violente de [MASK] [MASK] [MASK] , qui n' est pas présent lors de cette première marche , tentant de la retarder après sa rencontre avec le président [MASK] [MASK] [MASK] .
Deux jours après , [MASK] [MASK] mène une marche symbolique jusqu' au pont , une action qu' il semblait avoir négociée avec les autorités locales et qui provoqua l' incompréhension des activistes de [MASK] .
3200 marcheurs partent finalement de [MASK] le dimanche 21 mars 1965 , parcourant 20 km par jour et dormant dans les champs .
Au moment où ils atteignent le capitole de [MASK] , le jeudi 25 mars , les marcheurs sont 25000 .
Le jour même , la militante blanche des droits civiques [MASK] [MASK] est assassinée par le [MASK] [MASK] [MASK] alors qu' elle ramène des marcheurs dans sa voiture .
En 1966 , après les succès du sud , [MASK] [MASK] [MASK] et d' autres organisations de défense des droits civiques essayent d' étendre le mouvement vers le nord , [MASK] devenant l' objectif principal .
[MASK] [MASK] et [MASK] [MASK] , tous les deux de classe moyenne , déménagent vers les bidonvilles de [MASK] dans le cadre d' une expérience éducative et pour montrer leur soutien et empathie avec les pauvres .
Ils sont reçus par une foule haineuse et des lancers de bouteilles , et [MASK] [MASK] et lui commencent à vraiment craindre qu' une émeute se déclenche .
Les croyances de [MASK] [MASK] [MASK] se heurtent à sa responsabilité d' emmener les siens vers un événement violent .
Si [MASK] [MASK] a la conviction qu' une marche pacifique sera dispersée dans la violence , il préfére l' annuler pour la sécurité de tous , comme ce fut le cas lors du " bloody sunday " .
La violence à [MASK] est si intense qu' elle secoua les deux amis .
Quand [MASK] [MASK] et ses alliés retournent chez eux , ils laissent [MASK] [MASK] , un jeune séminariste qui avait déjà participé aux actions dans le sud , qui organise les premiers boycotts réussis pour le droit à l' accès aux mêmes emplois , ce qui sera un succès tel qu' il débouchera sur le programme d' opportunités égales dans les années 1970 .
À partir de 1965 , [MASK] [MASK] [MASK] commence à exprimer ses doutes sur le rôle des [MASK] dans la [MASK] [MASK] [MASK] [MASK] .
Il y dénonce l' attitude des [MASK] au [MASK] [MASK] et insiste sur le fait " qu' ils occupent le pays comme une colonie américaine " et appelle le gouvernement américain " le plus grand fournisseur de violence dans le monde aujourd'hui " .
[MASK] [MASK] [MASK] était déjà haï par de nombreux blancs racistes des états du sud , mais ce discours retourne de nombreux médias importants contre lui .
[MASK] [MASK] déclare souvent que le [MASK] [MASK] [MASK] [MASK] " n' avait pas commencé à envoyer un grand nombre de provisions ou d' hommes tant que les forces américaines n' étaient pas arrivées par dizaines de milliers " .
Il accuse aussi les [MASK] d' avoir tué un million de vietnamiens , " surtout des enfants " .
Il propose dans une lettre le moine bouddhiste et pacifiste vietnamien [MASK] [MASK] [MASK] , qui lutte pour l' arrêt du conflit , au [MASK] [MASK] [MASK] [MASK] [MASK] de l' année 1967 .
[MASK] [MASK] [MASK] dit aussi dans son discours que " la vraie compassion , c' est plus que jeter une pièce à un mendiant ; elle permet de voir qu' un édifice qui produit des mendiants à besoin d' une restructuration .
[MASK] [MASK] commence à parler d' un besoin de changements fondamentaux dans la vie politique et économique de la nation .
Cependant , la campagne n' est pas soutenue par tous les dirigeants du mouvement des droits civiques , y compris [MASK] [MASK] .
[MASK] [MASK] [MASK] traverse le pays de long en large pour rassembler une " armée multiraciale des pauvres " qui marcherait sur [MASK] et engagerait une désobéissance civile pacifique au capitole , si besoin est jusqu' à ce que le congrès signe une déclaration des droits de l' homme du pauvre .
Le [MASK] [MASK] [MASK] parlera d' une " insurrection " .
[MASK] [MASK] [MASK] voit un besoin urgent de se confronter au congrès qui avait démontré son " hostilité aux pauvres " en " distribuant les fonds militaires avec générosité " mais donnant " des fonds aux pauvres avec avarice " .
Fin mars 1968 , [MASK] [MASK] [MASK] se déplace à [MASK] ( [MASK] ) pour soutenir les éboueurs noirs locaux qui sont en grève depuis le 12 mars afin d' obtenir un meilleur salaire et un meilleur traitement .
Ses amis à l' intérieur de la chambre du motel entendent des coups de feu et courent sur le balcon pour trouver [MASK] [MASK] [MASK] abattu d' une balle dans la gorge .
300000 personnes assistent à ses funérailles le même jour , ainsi que le vice-président [MASK] [MASK] ( [MASK] était à une réunion sur le [MASK] [MASK] à [MASK] [MASK] et il y avait des craintes que la présence du président provoque des manifestations des pacifistes ) .
Dans ce sermon , il demande qu' à ses funérailles aucune mention de ses honneurs ne soit faite , mais qu' il soit dit qu' il avait essayé de " nourrir les affamés " , " habiller les nus " , " être droit sur la question du [MASK] [MASK] " et " aimer et servir l' humanité " .
La ville de [MASK] négocie la fin de la grève d' une manière favorable aux éboueurs après l' assassinat , .
Entre 1957 et 1968 , [MASK] avait voyagé sur plus de 9,6 millions de kilomètres , parlé en public plus de 2500 fois , été arrêté par la police plus de vingt fois et agressé physiquement au moins quatre fois .
[MASK] est très vite extradé au [MASK] et accusé du meurtre de [MASK] [MASK] [MASK] , ayant avoué l' assassinat le 10 mars 1969 , avant de se rétracter trois jours après .
Il raconte de plus qu ' " il n' avait pas tiré personnellement sur [MASK] " mais qu' il pouvait " être partiellement responsable sans le savoir " , indiquant une piste de conspiration .
À l' issue de celui-ci , la famille de [MASK] [MASK] [MASK] ne croit pas que [MASK] ait quelque chose à voir avec l' assassinat .
Certains spéculent que [MASK] n' était qu' un pion , de la même façon que l' assassin présumé de [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] était supposé l' avoir été ( voir [MASK] [MASK] [MASK] [MASK] [MASK] ) .
Il dit que ses motifs n' étaient pas racistes mais politiques , pensant que [MASK] était communiste .
En 2004 , [MASK] [MASK] , qui était avec [MASK] au moment de son assassinat , nota :
[MASK] [MASK] répond alors que sans des actions directes et puissantes comme celles qu' il entreprenait , les droits civiques ne seraient jamais obtenus .
La lettre inclut la célèbre citation " Une injustice où qu' elle soit est une menace pour la justice partout " mais aussi les paroles de [MASK] [MASK] qu' il répète : " Une justice trop longtemps retardée est une justice refusée " .
Pour lui une guérilla comme celle de [MASK] [MASK] est une " illusion romantique " .
[MASK] [MASK] [MASK] reconnaît la difficulté de la tâche mais demande à ne pas être intimidé par ceux qui se moquent de la non-violence .
Il note la similitude de leur lutte avec celle de [MASK] :
Pour [MASK] [MASK] , la non-violence est non seulement juste mais indispensable , car aussi juste que soit la cause d' origine , la violence signifie l' échec et le cycle de vengeance de la loi du talion , alors qu' il défend l' éthique de réciprocité :
Il affirme également que la fin ne peut justifier les moyens contrairement à la formule de [MASK] :
Au-delà de son combat pour l' égalité raciale , du discours [MASK] [MASK] [MASK] [MASK] où il imagine que ses " quatre jeunes enfants vivront un jour dans une nation où ils ne seront pas jugés par la couleur de leur peau , mais par le contenu de leur personne " et de la victoire politique avec les votes des [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] a identifié que l' égalité raciale ne vient pas seulement des lois qui défendent la personne mais surtout de la façon dont cette personne se perçoit elle-même :
[MASK] [MASK] [MASK] souligne que la non-violence n' est pas seulement une méthode juste , mais aussi un principe qui doit être appliqué à tous les êtres humains , où qu' ils soient dans le monde , et compare la campagne non-violence acclamée aux [MASK] à la violence de la [MASK] [MASK] [MASK] [MASK] soutenue par une partie de l' opinion américaine :
[MASK] [MASK] [MASK] invoque souvent la responsabilité personnelle pour développer la paix mondiale .
De par sa vocation de pasteur , [MASK] [MASK] [MASK] place la [MASK] au cœur de son message , considérant que l' humanité a été depuis trop longtemps " dans la montagne de la violence " , qu' elle devait aller vers " la terre promise de justice et de fraternité " .
L' amour n' est donc plus pour [MASK] [MASK] seulement une fin mais aussi un moyen d' arriver à la paix et la justice mondiale , et il réfute la notion de faiblesse de l' amour qu' ont émise certains philosophes dont [MASK] :
[MASK] [MASK] [MASK] considère que le pouvoir dans ce contexte n' est pas quelque chose de mauvais en soit à partir du moment où il est compris et utilisé correctement , c' est-à-dire quand il n' est pas considéré comme l' opposé exact de l' amour .
Pour lui , la mauvaise interprétation que l' amour est l' abandon du pouvoir et le pouvoir un déni d' amour est la raison pour laquelle [MASK] a rejeté le concept chrétien d' amour et les théologiens chrétiens le concept nietzschéen de la volonté de puissance .
Pour [MASK] [MASK] , si la violence et la guerre deviennent si destructrices , c' est aussi parce que la vitesse du progrès scientifique a dépassé celle du développement de l' éthique et la morale , qui n' ont pu toujours restreindre ses applications négatives .
" , [MASK] [MASK] [MASK] ne rend pas la science responsable de tous les maux pour autant et met en avant sa complémentarité avec la religion et l' éthique dans le développement humain :
Plusieurs fois , [MASK] [MASK] [MASK] exprime l' opinion que les afro-américains , ainsi que les autres américains désavantagés , devraient être dédommagés pour les torts qui leur ont été fait historiquement .
[MASK] lui fait prendre conscience qu' une lutte active mais non-violente contre le mal était aussi juste et nécessaire qu' aider le bien , et que les moyens et formes de cette lutte étaient innombrables :
Le dirigeant des droits civiques , théologien et éducateur [MASK] [MASK] a très tôt une influence sur lui .
L' activiste des droits civiques [MASK] [MASK] , qui a eu le [MASK] [MASK] comme professeur , conseille à [MASK] [MASK] [MASK] de suivre les principes de la non-violence dès 1956 .
Il lui sert de conseiller et de mentor à ses débuts et sera l' organisateur principal de la marche vers [MASK] .
Le [MASK] et son directeur [MASK] [MASK] [MASK] ont des rapports antagonistes avec [MASK] [MASK] [MASK] .
Sur ordre écrit du ministre de la justice [MASK] [MASK] [MASK] , le [MASK] commence à enquêter sur lui et le [MASK] [MASK] [MASK] [MASK] en 1961 .
Cette tentative de prouver que [MASK] [MASK] [MASK] est communiste doit beaucoup à ce que beaucoup de ségrégationnistes croient que les noirs du sud étaient jusqu' ici heureux de leur sort mais qu' ils sont manipulés par des " communistes " et des " agitateurs étrangers " .
Comme rien n' avait pu être trouvé politiquement contre lui , les objectifs et les enquêtes du [MASK] changèrent en des tentatives de le discréditer au travers de sa vie privée .
Le [MASK] distribue des rapports sur ces supposés écarts de vie privée à des journalistes amis , des alliés ou sources de financement possibles de la [MASK] , et même à la famille de [MASK] [MASK] .
L' agence envoie également des lettres anonymes à [MASK] [MASK] [MASK] , le menaçant de révéler plus d' informations s' il ne cesse pas son militantisme pour les droits civiques .
Cette lettre a souvent été interprétée comme une demande à [MASK] [MASK] de se suicider , .
Mais après qu' une manifestation pacifique à [MASK] en mars 1968 a été débordée par des éléments violents du black power , [MASK] , qui possédait un agent infiltré dans la hiérarchie du [MASK] , lance une nouvelle campagne de discrédit sur [MASK] [MASK] [MASK] .
Le dernier contact du [MASK] avec [MASK] [MASK] [MASK] est le moment de son assassinat .
Dès que [MASK] [MASK] [MASK] est abattu , ils sont les premiers à arriver sur les lieux pour lui administrer les premiers soins .
Pour les partisans d' une théorie de la conspiration , leur présence si proche des lieux du crime est une confirmation que le [MASK] est impliqué dans l' assassinat .
ordonne que tous les enregistrements et transcriptions manuelles connues et existantes sur l' espionnage de [MASK] [MASK] [MASK] de 1963 à 1968 soient conservés au [MASK] [MASK] [MASK] [MASK] [MASK] et interdits d' accès public jusqu' en 2027 .
Il reçoit en 1965 le médaillon des libertés américaines du [MASK] [MASK] [MASK] " pour son exceptionnel avancement sur les principes des libertés humaines " .
En 1980 , le quartier où [MASK] [MASK] [MASK] passa sa jeunesse est déclaré monument historique .
[MASK] [MASK] [MASK] sera la premier afro-américain et le deuxième non-président à être honoré par un monument dans le [MASK] [MASK] de [MASK] , [MASK] .
[MASK] [MASK] [MASK] est considéré comme l' auteur des plus grands discours historique des [MASK] , au côté d' [MASK] [MASK] ou [MASK] [MASK] [MASK] .
Plus de 730 villes des [MASK] ont une rue [MASK] [MASK] [MASK] en 2006 et de nombreuses autres ont été baptisés à son nom dans le monde entier .
Comme il avait été inspiré par [MASK] , de nombreuses personnalités sur la scène internationale dont [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] l' ont pris comme exemple pour sa lutte en faveur des droits de l' homme et sa méthode de désobéissance civile au travers de la non-violence pour y parvenir .
Il a influencé les mouvements des droits de l' homme en [MASK] [MASK] [MASK] et a été cité comme inspiration par un autre [MASK] [MASK] [MASK] [MASK] [MASK] qui a combattu pour l' égalité dans ce pays , [MASK] [MASK] .
La femme de [MASK] [MASK] , [MASK] [MASK] [MASK] , a suivi les traces de son mari et était très active sur les problèmes de justice sociale et les droits civiques jusqu' à sa mort en 2006 .
En 2008 , lors de l' élection présidentielle américaine [MASK] [MASK] remplit sa campagne de références à [MASK] [MASK] [MASK] et lui rend hommage .
[MASK] [MASK] , compagnon de lutte de [MASK] [MASK] [MASK] , déclare qu' il aurait beaucoup aimé que [MASK] soit témoin de sa victoire qui en a fait le premier métis noir président des [MASK] de l' histoire .
Ainsi , [MASK] [MASK] est en désaccord avec la volonté d' intégration de [MASK] [MASK] [MASK] , qu' il considère comme un moyen d' arriver à ses fins et non comme un principe .
[MASK] [MASK] voyait donc le combat de [MASK] [MASK] [MASK] comme une insulte à la culture afro-américaine .
De nombreux artistes ont été inspirés par le message de [MASK] [MASK] [MASK] .