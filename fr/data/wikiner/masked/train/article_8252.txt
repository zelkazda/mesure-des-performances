[MASK] est la capitale de la [MASK] [MASK] [MASK] , en [MASK] .
Elle est située sur la [MASK] , à 601 km à l' est de [MASK] .
Le barrage hydroélectrique sur la [MASK] fut achevé en 1982 à quelques kilomètres en aval de [MASK] , formant le [MASK] [MASK] [MASK] .
[MASK] est reliée à [MASK] par une ligne de chemin de fer directe et abrite un bon nombre d' industries et d' universités .
Comme de nombreuses villes russes , [MASK] offre des opportunités culturelles variées .
[MASK] possède aussi de très belles promenades et plages le long de la [MASK] , où les gens se baignent en été .
On trouve également de nombreuses piscines chauffées , des clubs de sport et des musées , dont un musée de la bière unique en [MASK] .