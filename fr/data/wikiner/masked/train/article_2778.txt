Souvent , on l' appelle simplement " [MASK] " , ce qui crée une confusion avec la notion géographique d' [MASK] -- ou péninsule indochinoise -- qui désigne les pays situés entre l' [MASK] et la [MASK] , soit les pays sus-cités ainsi que la [MASK] , la [MASK] et une partie de la [MASK] .
En 1939 , le [MASK] était l' allié du [MASK] dont le chemin de fer de [MASK] ( du [MASK] à l' [MASK] ) était prévu pour la logistique des troupes japonaises dans leur marche vers l' [MASK] pendant la [MASK] [MASK] [MASK] .
La création de l' [MASK] [MASK] , plus précisément [MASK] [MASK] , remonte à 1887 ( première expédition de missionnaires en 1663 ) .
À l' intérieur de cette entité , le [MASK] ( régime mixte ) et le [MASK] ( protectorat , monarchie sous tutelle française ) gardent leur nom , mais le [MASK] [MASK] n' existait pas en tant que tel .
Pendant la période coloniale , les missions catholiques , très actives , ont obtenu de nombreuses conversions surtout au [MASK] .
Contrairement à l' [MASK] [MASK] , l' [MASK] [MASK] ne fut une colonie de peuplement qu' au début de la colonisation française , mais elle se transformera vite en une colonie d' exploitation économique en raison de la présence de plusieurs ressources naturelles qui allaient enrichir la [MASK] durant les années de son contrôle sur le [MASK] , le [MASK] , le [MASK] , l' [MASK] et la [MASK] .
L' [MASK] était une colonie d' exploitation .
Du côté financier , la [MASK] [MASK] [MASK] [MASK] , fondée en 1875 , banque privée française , contrôle l' ensemble de l' économie vietnamienne .
L' [MASK] est , derrière l' [MASK] , la colonie qui reçoit le plus d' investissements français ( 6.7 milliards de francs-or en 1940 ) .
Au cours des années 1930 , les [MASK] exploitaient différentes ressources naturelles dans les pays formant l' [MASK] [MASK] .
Par exemple , on trouvait au [MASK] du riz et du poivre .
Le [MASK] ( qui était formé de la [MASK] , de l' [MASK] et du [MASK] ) , quant à lui , permettait aux [MASK] de mettre la main sur du thé , du riz , du charbon , du café , du poivre , de l' hévéa , du zinc et de l' étain .
Le [MASK] était le seul pays de l' [MASK] à n' avoir aucun potentiel économique aux yeux de [MASK] [MASK] .
Dans l' histoire de la présence française en [MASK] , l' exploitation de l' hévéa , le " bois qui pleure " , occupe une place particulière .
En effet , le caoutchouc fait figure de symbole de la réussite coloniale , puisqu' il garantira une source de revenu pour la [MASK] .
Toutefois , une société de l' envergure de [MASK] n' investit qu' en 1925 en [MASK] .
Dans le même temps , on assiste à l' arrivée massive de capitaux métropolitains en [MASK] , attirés par des taux de profit élevés .
Cependant , le marché mondial connaît des fluctuations brutales avec les débuts de la crise et l' effondrement de la demande des plus gros consommateurs mondiaux , les [MASK] en particulier .
À partir de 1930 , le cours du caoutchouc descend en dessous de celui de 1922 , et le gouvernement général de l' [MASK] est appelé à l' aide .
La [MASK] [MASK] [MASK] intervient , elle , en accordant des avances et en se rendant parfois maîtresse des plantations .
En 1939 , l' [MASK] exporte huit fois plus de caoutchouc qu' une quinzaine d' années auparavant ; cela représente plus du quart de la valeur des exportations totales de la péninsule , contre moins de 5 % en 1924 .
Ces procédés sont employés par les [MASK] lors du conflit mondial puis des guerres d' indépendance en [MASK] .
Durant la colonisation de l' [MASK] , les [MASK] s' y rendaient surtout pour l' exploitation de l' hévéa qui permettait de produire des caoutchoucs .
De toute évidence , les colonisateurs français faisaient toujours appel à des travailleurs vietnamiens , puisque l' hévéa se trouvait au [MASK] , ou plutôt en [MASK] si les divisions coloniales sont prises en considération .
Les lettrés de la cour de [MASK] ( ou celles antérieures ) qui se présentaient au concours triennal pour être mandarins , étaient des érudits et écrivaient en caractères chinois .
Il est bordé , à l' ouest , par le [MASK] , morcelé en principautés , et , au sud-ouest , par le royaume khmer , hindouisé , qui était entré dans une période de décadence .
Les premières interventions françaises remontent à 1858 , à l' époque du [MASK] [MASK] ( 1852-1871 ) avec comme prétexte la protection des missionnaires , mais il ne faut point oublier que dans la troupe d' invasion se trouvaient un très grand nombre de soldats espagnols .
Dans les milieux d' affaires , liés aux républicains opportunistes , de [MASK] [MASK] à [MASK] [MASK] , pour qui la colonisation devait permettre de résoudre la crise des débouchés industriels , le [MASK] semblait être un tremplin vers l' immense marché chinois .
Cette union comprenait cinq pays , en partie des créations coloniales : le [MASK] , le [MASK] ( à partir de 1893 ) , la [MASK] , l' [MASK] et le [MASK] .
Alors qu' elle fut une colonie de peuplement dès le début , la colonisation de l' [MASK] par la [MASK] devint très rapidement une colonie d' exploitation de nature économique .
En raison de certaines difficultés , les troupes françaises procédèrent à une évacuation en mars 1885 à [MASK] [MASK] .
En se rendant sur-le-champ au [MASK] [MASK] [MASK] , le lieu de résidence de [MASK] [MASK] , il pénétra dans les appartements de celui-ci et lui délivra le contenu de la dépêche .
Le 29 mars au matin , tout [MASK] bruit du " désastre de [MASK] [MASK] " .
On évoqua des hordes chinoises déferlant sur le [MASK] .
Pour beaucoup , c' était [MASK] outre-mer .
L' humiliation qu' aurait constituée une telle défaite était à la mesure de l' enjeu que représentait le [MASK] et de la façon dont le cabinet [MASK] en avait géré la conquête .
De plus , les richesses agricoles et minières du [MASK] tentaient les milieux d' affaires .
L' accès au marché chinois , que les [MASK] voulaient atteindre en reliant la [MASK] au [MASK] , devint une priorité .
Évidemment , pour les missionnaires catholiques , la conquête du [MASK] représentait pour eux un moyen pour protéger les minorités chrétiennes du [MASK] [MASK] qui étaient persécutées .
[MASK] avait fait du [MASK] son affaire .
Par conséquent , cela obligea [MASK] [MASK] à poursuivre la guerre , la négociation secrète , envoyant toujours plus de soldats , consommant toujours plus d' argent .
[MASK] décida de conduire sa politique en solitaire .
Quand la nouvelle du " désastre de [MASK] [MASK] " fit le tour de [MASK] , [MASK] joua sa survie politique .
[MASK] [MASK] dut intervenir devant les députés à 15 heures .
Des nouvelles rassurantes arrivèrent de [MASK] : la situation militaire au [MASK] était loin d' être aussi mauvaise qu' on l' avait cru .
À bas [MASK] , le lâche ! "
[MASK] gravit les marches qui le menaient à la tribune de l' hémicycle .
Lorsque le discours de [MASK] [MASK] toucha à sa fin , les huées redoublèrent .
La majorité qui avait soutenu [MASK] [MASK] depuis plus de deux ans l' avait abandonné .
[MASK] prit brièvement la parole , annonça qu' il allait remettre sa démission .
À la nouvelle de la démission de [MASK] [MASK] , les [MASK] s' enflammèrent de joie .
Par exemple , la foule hurla : " À bas [MASK] !
À mort [MASK] ! "
Lorsque [MASK] [MASK] sortit du [MASK] , il fut longuement acclamé .
[MASK] avait été victime de sa diplomatie secrète ( réussie , mais un jour trop tard ) , et de l' incurie des chefs militaires qui avaient mal manœuvré sur le terrain , et gonflé le récit de l' attaque chinoise pour expliquer leur retrait .
Le 6 avril 1885 s' installe un nouveau ministère républicain dirigé par [MASK] [MASK] ( avril-décembre 1885 ) , député radical depuis 1876 .
En peu de jours , le calme est revenu à [MASK] .
Le protectorat français sur toute l' [MASK] est acquis , les préliminaires de la paix avec l' [MASK] [MASK] [MASK] [MASK] ayant été conclus le 4 avril de cette année .
Le 9 juin , le nouveau traité de [MASK] est signé , la [MASK] finit par mettre la main sur le [MASK] .
Malgré le mauvais souvenir de ces ides de mars qui ont causé sa mort politique , [MASK] [MASK] , qui a voulu cette conquête , en a toujours revendiqué la paternité .
Deux ans plus tard , l' [MASK] est devenue un sujet de consensus .
L' [MASK] finira par devenir un des fleurons de l' [MASK] [MASK] et une large et constante majorité politique soutiendra les ambitions coloniales de la [MASK] [MASK] .
Après des années d' hésitation entre l' administration directe ( le statut de colonie ) ou indirecte ( le régime du protectorat ) , les [MASK] choisirent le " protectorat " pour le [MASK] et l' [MASK] .
En même temps , il permettait d' alléger les dépenses de la [MASK] , mais le protectorat se mua rapidement en administration directe .
Seule la [MASK] fut à proprement à parler une colonie .
Le [MASK] et le [MASK] eurent un régime mixte .
Un traité conclu en 1896 par la [MASK] et la [MASK] , prévoyait l' extension territoriale de leurs colonies et protectorats respectifs .
Ce traité prévoyait la réunion au profit de la [MASK] du nord-ouest du [MASK] , et au profit de la [MASK] britannique , des provinces du sud récemment annexées par le [MASK] .
[MASK] [MASK] de son côté , devait étendre son influence à tout le nord-est du [MASK] .
Le [MASK] aurait donc constitué selon les modalitées de ce traité , un état-tampon entre les possession britanniques et française en [MASK] .
À cette fin , [MASK] organisa un système de prélèvement fiscal lourd et impopulaire .
L' [MASK] était une colonie d' exploitation et non de peuplement : la mise en valeur des ressources du pays débuta dès la fin de la conquête .
La [MASK] [MASK] [MASK] [MASK] fondée en 1875 devint l' interlocutrice obligée du gouvernement général ; elle contrôla de fait l' économie indochinoise .
Bénéficiant d' un statut unique parmi les banques privées françaises , car dotée du privilège d' émission de la piastre indochinoise , la [MASK] [MASK] [MASK] [MASK] était à la fois une banque d' affaires et une société financière .
Son domaine d' action s' étendait à [MASK] [MASK] entière et au-delà .
De toutes les colonies , mise à part l' [MASK] , ce fut elle qui reçut le plus d' investissements , évalués à 6.7 milliards de francs-or en 1940 .
Les transports et les voies de communication , essentiels à l' expansion de l' économie coloniale , bénéficièrent d' une attention particulière de la part de la [MASK] .
En 1939 , [MASK] n' était plus qu' à trente jours de bateau de [MASK] , et à cinq jours d' avion de [MASK] .
Mais en [MASK] , sans parler des éloignements de race , nous trouvons des âmes et des esprits pétris par la plus vieille civilisation du globe. "
Le nombre d' élèves de l' enseignement public au [MASK] dans le primaire s' éleva de cent vingt-six mille en 1920 à plus de sept cents mille en 1943 -- 1944 .
Au [MASK] , quinze mille sept cents enfants étaient scolarisés dans le primaire public en 1930 ; ils étaient trente-deux mille en 1945 .
Toutefois , sauf en [MASK] , les campagnes ne furent pas dotées d' un réseau scolaire serré et , en 1942 , sept cent-trente-et-un mille enfants étaient scolarisés sur une population totale de 24,6 millions .
Bien que minoritaires , ils formaient ce " tiers-état " auquel le gouverneur général [MASK] [MASK] recommandait que l' on fît une place , considérant que , si des droits ne leur étaient pas octroyés , ils les réclameraient eux-mêmes .
Mais les enfants de l' œuvre civilisatrice française , de formation équivalente ou à diplômes identiques , se voyaient refuser l' égalité de statut et de traitement avec les [MASK] , car dans cette société coloniale , la minorité européenne occupait le sommet de la hiérarchie .
Plus que la fortune , l' appartenance raciale était un indicateur du statut social d' une personne vivant en [MASK] .
Par exemple , la liaison amoureuse que [MASK] [MASK] met en scène dans le roman [MASK] [MASK] est une transgression du code de la société coloniale .
Un [MASK] se trouvant en [MASK] vivait le plus souvent en célibataire .
À l' époque de la colonisation de l' [MASK] , les [MASK] , qui prenaient la décision de s' établir dans cette région asiatique , formaient une minorité : leur nombre ne dépassa jamais le seuil de 34000 personnes ( en 1940 ) sur une population totale de 22655000 habitants .
De toute évidence , ces [MASK] fut en majorité des cadres de la fonction publique ou du secteur privé , et plus rarement des colons .
Parallèlement , l' économie coloniale ruina les structures traditionnelles d' organisation et d' entraide de la communauté paysanne : l' institution multiséculaire des rizières communales connut une forte érosion au point d' avoir pratiquement disparu en [MASK] à la fin de la [MASK] [MASK] [MASK] .
La paysannerie manifesta ses frustrations de façon bruyante , voire violente , dès 1930 -- 1931 en [MASK] et dans le nord de l' [MASK] , puis à nouveau entre 1936 et 1938 .
Faute de s' attaquer à la racine des maux dont souffraient les ruraux , l' administration coloniale était prisonnière de contradictions apparemment insolubles , qu' exprimait le gouverneur général [MASK] [MASK] à propos de l' occupation de concessions par des paysans sans terre en 1938 : " Lorsque nous protégeons les droits des uns , nous commettons une injustice et portons atteinte à l' équité à l' égard des autres .
Dans les années 1930 , celui-ci connut deux grandes poussées sous l' effet de la crise économique mondiale causée par le krach boursier en 1929 , de l' extension des activités de la [MASK] [MASK] [MASK] et de la stratégie " classe contre classe " du parti communiste français ( [MASK] ) .
De même que la grande dépression économique qui toucha l' [MASK] entre 1930 et 1934 , et illustra la crise du régime capitaliste tout en démontrant la puissance de la [MASK] [MASK] [MASK] à laquelle le gouvernement français confia l' assainissement de l' économie et qui fut la principale bénéficiaire des faillites , des expropriations et de la concentration des entreprises .
La [MASK] [MASK] [MASK] sera déterminante pour l' avenir de l' [MASK] [MASK] .
Il entend occuper la frontière nord de l' [MASK] et couper la voie ferrée de [MASK] au [MASK] , une des voies de ravitaillement de [MASK] [MASK] .
Le gouverneur [MASK] accepte , faute de moyens pour s' y opposer .
Cet accord autorise la présence de 6000 soldats japonais au [MASK] ainsi que l' utilisation d' aérodromes .
Cet accord ne peut toutefois empêcher la violente occupation de [MASK] [MASK] et le bombardement de [MASK] .
L' [MASK] reste sous l' autorité nominale de [MASK] jusqu' en 1945 .
[MASK] [MASK] [MASK] , nationaliste vietnamien fortement recherché par les autorités coloniales françaises à l' époque , crée en 1941 le [MASK] par la fusion du [MASK] [MASK] [MASK] ( fondé en 1930 ) et d' éléments nationalistes .
Son mouvement se développera surtout à partir du début de 1945 , grâce à l' aide matérielle des [MASK] .
Le but était de récupérer les territoires situés au [MASK] et au [MASK] qui lui furent ravis par [MASK] [MASK] en 1893 , 1902 , 1904 et 1907 et de reconstituer le [MASK] [MASK] [MASK] .
Sur les 40000 [MASK] métropolitains dans la région dont 18000 militaires , plus de 3000 ont été tués en moins de 48 heures , parfois exécutés par décapitation à coups de sabre .
[MASK] [MASK] encourage la formation de régimes nominalement indépendants , dans le cadre de sa [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , avec en particulier le soutien de l' empereur d' [MASK] [MASK] [MASK] .
[MASK] , [MASK] [MASK] et [MASK] [MASK] se réunissent à la [MASK] [MASK] [MASK] en 1945 .
Les [MASK] , sous l' impulsion de [MASK] [MASK] , décident de reprendre le contrôle de l' [MASK] après la [MASK] [MASK] mondiale à une époque où d' autres puissances coloniales reprennent pied dans leurs colonies asiatiques ( [MASK] et [MASK] pour le [MASK] , [MASK] pour les [MASK] ) .
Au lendemain de l' évacuation japonaise , le [MASK] et le [MASK] parviennent à faire reconnaître leur souveraineté en douceur .
Il n' en va pas de même au [MASK] [MASK] , enjeu stratégique et économique d' une tout autre importance .
Le [MASK] et d' autres groupes indépendantistes cherchent à établir leur autorité sur le pays .
Des négociations sont ouvertes et aboutissent aux accords du 6 mars 1946 aux termes desquels le [MASK] est libre .
Ces accords , signés par [MASK] [MASK] [MASK] et [MASK] [MASK] , ne durent pas .
[MASK] reconquiert la [MASK] , mais estime qu' à terme , l' indépendance vietnamienne deviendra inévitable .
Sa mort laisse le [MASK] sans interlocuteur .
Celui-ci tente d' interdire aux [MASK] le port de [MASK] , dont le bombardement par la flotte française rend le conflit inévitable .
[MASK] [MASK] [MASK] , son chef politique , et le jeune général [MASK] [MASK] [MASK] , chef de la branche militaire , peuvent compter sur un encadrement discipliné et efficace .
Du côté français , les chefs du gouvernement provisoire ( le général [MASK] [MASK] , puis [MASK] [MASK] ) , puis les chefs des gouvernements successifs de la [MASK] [MASK] , délèguent des chefs militaires de valeur : le général [MASK] , remplacé à sa mort par l' amiral [MASK] [MASK] , puis les généraux [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] .
Les [MASK] , d' abord réticents , finiront par financer en grande partie l' effort de guerre français au nom de la lutte anticommuniste .
A partir de 1949 , la victoire de [MASK] [MASK] en [MASK] permet au [MASK] de recevoir un équipement important venu de [MASK] , de l' [MASK] [MASK] et des autres pays du bloc communiste .
Il reçoit aussi un soutien moral considérable des mouvements communistes et anticolonialistes du monde entier , y compris de [MASK] , où le [MASK] [MASK] [MASK] ( plus de 20 % de l' électorat à l' époque ) fait campagne contre la " sale guerre " .
Au contraire , le [MASK] arrive à marginaliser et éliminer les forces indépendantistes rivales et à élargir son emprise sur la population , qu' il peut mobiliser dans le renseignement , le ravitaillement et le soutien logistique des combattants .
C' est la clé de sa victoire contre les [MASK] , et plus tard contre les [MASK] .
La [MASK] , considérée par les [MASK] et l' [MASK] comme un pays vaincu , ne colonisait pas uniquement pour regagner du prestige sur la scène politique internationale , encore que cette dimension ait eu son importance , mais aussi pour des raisons économiques .
La question de rétablir la grandeur de la [MASK] -- par la guerre s' il le fallait -- faisait l' unanimité dans les sphères dirigeantes , y compris chez les partis de gauche .
Ces remarques sur la situation économique des investissements français portent à penser que la [MASK] fut très déterminée à garder ses colonies en [MASK] , car le contrôle des leviers politiques , économiques et culturels de l' [MASK] qu' exerçait la [MASK] lui permit de mettre la main sur les ressources naturelles de la région .
En 1939 , les exportations de caoutchouc d' [MASK] étaient huit fois plus élevées qu' elles ne l' étaient une quinzaine d' années auparavant .
On peut aussi y lire que le [MASK] , en raison de sa population , fournissait aux [MASK] un très bon marché pour la vente des produits manufacturiers de la métropole .
Le chemin de fer du [MASK] était la voie privilégiée de cette pénétration .
Cette inquiétude face au communisme est très bien exprimée par [MASK] [MASK] , représentant de [MASK] [MASK] à l' [MASK] [MASK] [MASK] [MASK] .
D' ailleurs , pendant la même période , des insurrections communistes sont étouffées par les [MASK] aux [MASK] et par les [MASK] en [MASK] .
Mais , dans ces deux pays , la guérilla communiste n' avait qu' une base populaire insuffisante et ne disposait pas , comme le [MASK] par la frontière chinoise , d' une voie de ravitaillement régulier .
Ainsi conçu , la [MASK] [MASK] [MASK] a donné l' occasion tant aux [MASK] qu' au [MASK] de déployer une panoplie de stratégies et de tactiques qui reflétaient leur désir de gagner .
Tout d' abord , commençons par exposer les stratégies et les tactiques déployées par le [MASK] .
Étant techniquement désavantagé , le [MASK] ne pouvait point s' offrir le luxe d' affronter les [MASK] d' une manière traditionnelle en tout temps .
C' est pourquoi , le [MASK] jouait la stratégie de l' espace -- en s' étendant pour disperser l' effort de l' adversaire -- , mais plus encore celle du temps .
Afin de bien s' organiser , d' un point de vue militaire , bien entendu , les têtes dirigeantes du [MASK] , tels que [MASK] [MASK] [MASK] et [MASK] [MASK] , se sont assurés qu' une structure militaire était rapidement mise en place afin d' assurer le bon déroulement des activités de guérilla menées par ce groupe qui revendiquait l' indépendance totale du [MASK] .
Les structures militaires du [MASK] ont été rapidement mises en place .
À tous les niveaux , un commissaire politique veille au respect de la ligne définie par le [MASK] .
Pour le [MASK] , la montagne couverte de forêts tropicales offrait un cadre particulièrement adapté à la guérilla .
Toutefois , contrairement à ce que nous pouvons penser sur le [MASK] ( et sur [MASK] [MASK] en général ) , ce pays n' est aucunement un pays homogène .
Pour obtenir leur collaboration , les membres du [MASK] pouvaient parfois utiliser la persuasion , et même parfois la terreur , pour contrôler les villages habités par les minorités ethniques du [MASK] .
Vers la fin du conflit indochinois , le [MASK] tenait dans ses mains plus de 5000 villages sur 7000 .
De plus , même dans les villes qui avaient l' air d' être bien contrôlées par les [MASK] , la présence du [MASK] lui donnait des occasions en or pour prélever l' impôt révolutionnaire .
Bref , pour résumer la stratégie et les tactiques de combat du [MASK] , nous pouvons dire que leur but consistait à être partout et nulle part , se fondre dans la population , disparaître subitement dans la nature , voire dans le sol .
De toute évidence , pour s' assurer que les membres du [MASK] ne se trouvaient point près d' eux , les [MASK] lèvent des supplétifs , installent des postes ici ou là pour encadrer la population et l' empêcher de tomber sous l' emprise vietminh .
Malgré cela , la présence française est plutôt molle et durant la nuit , les membres du [MASK] font sentir leur présence .
Les membres du [MASK] sont au courant des mouvements des troupes françaises même avant que celles-ci décident de passer à l' action .
Ils pouvaient prévoir sans aucune difficulté les mouvements du corps expéditionnaire , tributaire de sa lourde logistique , car il était espionné par toute une population ( celle du [MASK] ) .
Bref , pour connaître les conditions générales de la progression du conflit qui opposait les [MASK] au [MASK] , il suffisait de lire les journaux .
Cela poussera un dirigeant du [MASK] à dire que le corps expéditionnaire français est " aveugle " ( il ne voit pas l' adversaire ) , " sourd " ( il n' est pas renseigné ) et " boiteux " ( ses communications sont difficiles ) .
Dans ses mémoires sur la [MASK] [MASK] [MASK] , le général [MASK] [MASK] [MASK] décrit ce duel entre les [MASK] et le [MASK] comme étant " une guerre sans front ni objectif fixes , avec des vagues d' attaque imprévues , venant de directions jugées sans risque par l' ennemi " et il ajoute aussi que " la guérilla de mouvement dans notre résistance différait de beaucoup de la guerre de mouvement d' envergure des temps modernes. "
Les [MASK] ont tenté par tous les moyens de mater le mouvement nationaliste qui revendiquait l' indépendance totale du [MASK] [MASK] en créant dès 1949 un [MASK] [MASK] [MASK] [MASK] dirigé par le dernier empereur de la [MASK] [MASK] [MASK] [MASK] , et en exerçant un effort de contre-guérilla qui sera un échec .
Cela avantageait donc considérablement le [MASK] en termes de forces mobiles , car il pouvait saisir l' occasion " de se concentrer au moment et à l' endroit où il le voulait. "
Une autre chose importante à mentionner : la difficulté de la guerre , pour les [MASK] , était accrue par l' impossibilité de distinguer un [MASK] ordinaire d' un guérillero .
Finalement , la [MASK] [MASK] [MASK] [MASK] [MASK] , à la limite occidentale du [MASK] , dans les montagnes aux confins du [MASK] , a violemment mis un terme à la domination française sur l' [MASK] .
En fait , le [MASK] [MASK] déjoue la surveillance aérienne en faisant passer hommes et matériels par des pistes invisibles sous les arbres , sur des véhicules bricolés avec des carcasses de vélos .
Les premières vagues d' assaut ( 50000 hommes du général [MASK] [MASK] [MASK] contre 11000 soldats français ) mettent l' ensemble du camp à la portée de la puissante artillerie ( d' origine chinoise ) amenée à pied d' oeuvre par le [MASK] [MASK] .
Le 7 mai 1954 , après deux mois de résistance acharnée , la base de [MASK] [MASK] [MASK] tombe .
À [MASK] , une nouvelle crise parlementaire fait tomber [MASK] [MASK] et [MASK] [MASK] , partisans de la poursuite des opérations , et amène au pouvoir [MASK] [MASK] [MASK] avec un programme de négociation .
La [MASK] [MASK] [MASK] consacrée au règlement de la question indochinoise s' ouvre le lendemain de la chute de [MASK] [MASK] [MASK] .
Par la suite , l' indépendance du [MASK] [MASK] ( divisé en deux parties ) , du [MASK] et du [MASK] est reconnue .
Selon les [MASK] [MASK] [MASK] , des élections devront être tenues dans les deux ans afin d' unifier le [MASK] .
Mais le non-respect de cette échéance entraîne une reprise de la guérilla communiste ( [MASK] ) dans le sud .
À partir de 1964 , les [MASK] soutiendront massivement , d' un point de vue militaire , le gouvernement sud-vietnamien et cela plongera le [MASK] [MASK] dans une autre guerre .