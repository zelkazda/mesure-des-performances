La croix gammée ou svastika est un symbole ancien présent dans de nombreuses cultures , utilisé comme symbole par [MASK] [MASK] et le [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) en raison de son association avec les peuples " aryens " dont ils se réclamaient .
La croix gammée fut adoptée par le [MASK] alors qu' il n' était encore que le [MASK] [MASK] [MASK] allemands , et devint dès 1920 son emblème officiel .
Cette idée fut répandue par de nombreux auteurs , en particulier [MASK] [MASK] [MASK] , poète nationaliste allemand .
Dans [MASK] [MASK] , [MASK] [MASK] présente le débat qui a entouré le développement de l' insigne nazi , pour lequel un véritable concours d' idées fut lancé .
[MASK] accordait non seulement de l' importance au symbole , mais surtout au choix des couleurs ( rouge , blanc , noir , couleurs de l' [MASK] [MASK] , trahi par les criminels de novembre ) .
Le 14 mars , peu après l' accession de [MASK] au poste de chancelier , le drapeau nazi fut hissé en même temps que le drapeau national , devenant co-drapeau national .
À l' occasion du congrès de [MASK] , il deviendra le seul drapeau national le 15 septembre 1935 .
À [MASK] , la valeur raciale de l' emblème nazi et même la [MASK] étaient jusqu' au milieu des années 1980 ignorées de la plupart des gens .
La croix gammée évoquait uniquement la puissance militaire allemande , et pouvait occasionnellement se voir sur des accessoires de moto ou des emballages de maquettes d' engins de la [MASK] [MASK] [MASK] .
Son usage fut presque totalement abandonné après que la représentation allemande ainsi qu' israélienne eurent protesté contre une campagne publicitaire sur le flanc des autobus de [MASK] .
Le groupe [MASK] dut se fendre d' un communiqué dans lequel il expliquait que ce symbole n' était pas vraiment le bienvenu aux concerts ni aux séances de dédicace .
Lors de son procès , [MASK] [MASK] arborait également un tatouage en croix gammée sur le front