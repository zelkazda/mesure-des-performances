Il est parlé par environ 12,5 millions de personnes , dont les trois quarts vivent en [MASK] .
Il existe aussi des communautés magyarophones dans tous les pays voisins de la [MASK] ( [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] ) , ainsi que d' importantes communautés apparues par émigration aux [MASK] [MASK] [MASK] , au [MASK] , en [MASK] , etc .
Sur les 12,5 millions de locuteurs du hongrois , 9,5 millions vivent en [MASK] , où le hongrois est langue officielle .
Quant à l' importance du nombre de locuteurs , le pays suivant est la [MASK] , avec 1450000 personnes .
En [MASK] ( 286000 locuteurs ) , dans la province de [MASK] , le hongrois est dit " d' usage officiel " dans les localités où la population magyarophone atteint 15 % de la population totale .
Cette langue a le même statut dans trois localités de [MASK] ( 6000 locuteurs ) .
En [MASK] ( 573000 locuteurs ) , en [MASK] , dans la région de [MASK] ( 150000 locuteurs ) , en [MASK] , dans le [MASK] ( 40000 locuteurs ) et en [MASK] ( 12000 locuteurs ) , le hongrois a un statut de langue minoritaire ou régionale .
En dehors des pays voisins de la [MASK] , où les [MASK] ont un statut de minorité nationale , il y a beaucoup de locuteurs de hongrois dans la diaspora .
Les communautés les plus nombreuses se trouvent aux [MASK] ( 118000 ) , au [MASK] ( 90000 ) et en [MASK] ( 70000 ) .
L' histoire du hongrois commence il y a quelque 3000 ans , à l' est de l' [MASK] , dans la région du fleuve [MASK] .