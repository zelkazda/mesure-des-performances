Les sociologues classiques que [MASK] [MASK] regroupe au sein d' une " trinité " de la sociologie -- [MASK] , [MASK] et [MASK] -- ont produit des interprétations riches de la constitution et de l' évolution des formes sociales .
Ainsi , [MASK] considère que la division du travail provient essentiellement de l' accroissement de la population et de la " densité sociale " .
Ce qu' explique [MASK] recouvre deux aspects étroitement liés entre eux : le passage d' une société moderne à une société traditionnelle à travers le dépassement de l' équilibre population-subsistance .
[MASK] en est le témoin dans les pays occidentaux au dix-neuvième siècle .
La contribution de [MASK] [MASK] à la compréhension du développement s' inscrit dans ses nombreuses analyses d' un processus qu' il considère être à l' œuvre dans toute société : le processus de rationalisation .
Si la religion est au centre du processus décrit par [MASK] , c' est aussi et surtout , pour l' auteur , un moyen explicatif en terme épistémologique .
Ce qu' explique [MASK] , c' est qu' à une certaine forme d' ordre social correspondent certaines valeurs , une certaine culture qui , en conférant un sens aux actions humaines , les oriente .
[MASK] [MASK] offre une interprétation du développement comme étant indissociable d' un système de rapports sociaux qu' il résume avec le terme " capitalisme " .
Ce groupe social avait déjà été identifié par [MASK] [MASK] , par exemple , qui se méfiait de ses possibles abus de pouvoir ( ententes sur les prix ... ) .
[MASK] considère que la position des capitalistes comme propriétaires des moyens de production se traduit par leur domination de l' espace social .
Ce qu' explique [MASK] , c' est que le développement capitaliste est motivé par la recherche de profits qui sont eux-mêmes indissociables de la domination du prolétariat .
Dans la francophonie , la sociologie du développement est surtout le fait de [MASK] [MASK] ( perspective historique du fait social de la relation développeur-développé ) , [MASK] [MASK] , [MASK] [MASK] et de [MASK] [MASK] ( grand chantre de la théorie de la dépendance ) et dans une certaine mesure , [MASK] [MASK] .
De manière plus contemporaine , on soulignera le travail du paradigme dit de l ' après-développement ou [MASK] [MASK] [MASK] en particulier [MASK] [MASK] et [MASK] [MASK] .