Les fables de [MASK] empruntent souvent leurs sujets à celles d' [MASK] et de [MASK] [MASK] .
Il devint ensuite substitut à la magistrature de [MASK] , où il mourut , laissant une veuve avec deux jeunes enfants .
Le jeune [MASK] fut très tôt attiré par la bibliothèque paternelle et apprit le français .
Sa mère tenta avec lui de recouvrer une pension d' orphelin en 1782 à [MASK] , mais finalement il se fit accepter comme fonctionnaire de la cour des comptes , poste dont il démissionna en 1788 , à la mort de sa mère .
Par la suite , il se lance dans l' écriture de livrets d' opéras-bouffe , inspirés d' auteurs français , à l' époque de la mort de sa mère qui lui laissait un petit frère , [MASK] , dont il se considéra toute sa vie , comme un véritable père .
En 1790 , il écrit et imprime une ode à la paix avec la [MASK] .
Il parvint à attirer 170 abonnements , ainsi que l' attention de [MASK] avec qui il polémiqua .
[MASK] était autodidacte , il parlait en plus du français , l' italien et savait jouer du violon .
De 1812 à 1841 , il obtient un poste à la bibliothèque impériale de [MASK] , ce qui lui permet d' assurer son quotidien .
En 1838 , on organise pour lui une grande réception jubilaire et l' empereur [MASK] [MASK] lui octroie une pension à vie .