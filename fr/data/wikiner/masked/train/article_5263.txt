Il est considéré comme saint par l' [MASK] [MASK] , de même que sa mère [MASK] .
Quelques mois plus tard , [MASK] , fils de [MASK] , est proclamé princeps par les prétoriens et le peuple de [MASK] mécontent de l' impôt de capitation .
[MASK] , envoyé les combattre , est tué en 307 .
[MASK] fait alors appel à [MASK] qui accepte le consulat et une conférence a lieu à [MASK] qui réunit [MASK] , [MASK] et [MASK] dans le but de rétablir la tétrachie mais elle se termine par un échec :
À partir de 320 , [MASK] entre de nouveau en conflit avec [MASK] .
En 324 , [MASK] est vaincu à [MASK] , puis à [MASK] et fait sa soumission à [MASK] .
Depuis la tétrarchie , [MASK] n' est plus dans [MASK] même .
À partir de 324 , [MASK] transforme la cité grecque de [MASK] en une " Nouvelle [MASK] " , à laquelle il donne son nom , [MASK] .
Dès [MASK] , la ville compte 100000 habitants .
[MASK] institue une nouvelle monnaie d' or , le solidus dont la stabilité et l' abondance est assurée grâce aux confiscations qu' il fait des importants stocks d' or des temples païens .
La nuit même , [MASK] lui serait apparu en rêve et lui aurait montré un chrisme flamboyant dans le ciel en lui disant : " Par ce signe , tu vaincras " ( In hoc signo uinces ) .
En 313 , [MASK] rencontre [MASK] à [MASK] et conclut avec lui un accord de partage de l' [MASK] .
La progressive conversion de [MASK] au christianisme s' accompagne d' une politique impériale favorable aux chrétiens mais le paganisme n' est jamais persécuté .
[MASK] finit par céder et promulgue en 321 un édit de tolérance laissant aux donatistes les églises qu' ils possèdent tout en maintenant sa condamnation de principe .
En fait , la christianisation du pouvoir impérial est lente car [MASK] est obligé de tenir compte du poids des traditions , surtout chez les élites :
Trois fronts retiennent tour à tour l' attention de [MASK] .
Les opérations sont momentanément interrompues au moment de l' affrontement avec [MASK] .
Depuis la paix de 297 conclue sous la tétrarchie , la [MASK] est demeurée relativement tranquille .
L' aboutissement logique de cette évolution est , dès le règne de [MASK] [MASK] ( 337-361 ) , l' accession de barbares aux plus hauts postes de l' état-major .
En 326 , [MASK] fait périr son fils aîné [MASK] , puis son épouse [MASK] .
On ignore les raisons de ces exécutions , qui ne sont peut-être pas liées entre elles , mais on a évoqué un adultère ou une dénonciation calomnieuse de la part de [MASK] .
En 337 , il vient de déclencher un conflit avec la [MASK] [MASK] de [MASK] [MASK] et s' apprête à mener une expédition contre cet empire , quand il meurt subitement près de [MASK] .
Il est enterré dans l' [MASK] [MASK] [MASK] qu' il a fait construire à [MASK] .
Quand [MASK] meurt , il n' a pas réglé sa succession .
D' après [MASK] [MASK] [MASK] , [MASK] est mort le dimanche de [MASK] 22 mai 337 .
Il est inscrit dans la plupart des calendriers byzantins le 21 mai avec sa mère [MASK] , parfois le 22 .