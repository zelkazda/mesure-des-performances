La doctrine du sikhisme se base sur les enseignements spirituels des [MASK] [MASK] , recueillis dans le [MASK] [MASK] [MASK] [MASK] .
Dès son enfance , [MASK] est fasciné par la spiritualité et montre des dispositions peu ordinaires pour l' apprentissage .
C' est sans doute durant cette période qu' il découvre l' enseignement de [MASK] , un saint révéré aussi bien par les hindous que par les musulmans .
Une sentence bien connue de [MASK] est : " Il n' y a ni hindou et ni musulman. "
C' est ainsi que le mot [MASK] ( disciple ) , se répand .
[MASK] [MASK] est opposé au système des castes .
Le dixième et dernier gurû , [MASK] [MASK] [MASK] ( 1666 -- 1708 ) introduit la cérémonie de baptême sikh en 1699 donnant par là une identité caractéristique aux [MASK] .
Le livre saint des [MASK] est compilé et édité par le cinquième gurû , [MASK] [MASK] en 1604 .
Durant la guerre d' indépendance de l' [MASK] , de nombreux [MASK] furent pendus , durent faire face à toutes sortes de brutalités , se battre contre l' occupant , subir de longues périodes d' emprisonnement afin de libérer le pays .
Bien que les [MASK] ne représentent que 1,8 % de la population de l' [MASK] , ils se sont néanmoins forgé une solide réputation dans pratiquement tous les domaines , tels que l' armée , l' agriculture , les sports , l' industrie , l' éducation , la médecine , l' ingénierie etc .
La religion [MASK] est strictement monothéiste .
Ainsi [MASK] dit :
Non seulement toute la philosophie [MASK] , mais aussi toute l' histoire et le tempérament des [MASK] découlent de cette manière de voir .
Les [MASK] ne reconnaissent pas le système de castes ; de même , ils ne croient pas en l' adoration des idoles , dans les rituels ou les superstitions .
[MASK] donne à une humanité opprimée un nouvel espoir de rejoindre sa fraternité en tant qu' égal .
Par contre , le dicton [MASK] est le suivant :
Le [MASK] n' accepte pas le pessimisme .
Le précepte : " ne réponds pas à l' offense , mais à celui qui t'a frappé sur la joue droite , tend la joue gauche " ne trouve pas sa place dans le mode de vie [MASK] .
La position doctrinale de [MASK] est assez simple , en dépit de son origine .
Selon [MASK] , discuter quels composants de sa croyance proviennent de l' hindouisme , quels sont musulmans , c' est discuter comme un idiot qui cherche quelle religion possède le droit de professer des concepts universels tels que la bonté , la charité , l' honnêteté , la vénération du nom de dieu , le respect des autres .
[MASK] [MASK] souscrit également à la croyance en la mâyâ , l' illusion du monde physique .
Un [MASK] ne peut avoir foi en aucun autre prophète vivant ou non vivant .
Le [MASK] est basé sur la théorie du karma et de la réincarnation ; on évite les réincarnations en renonçant aux vices ( alcool , tabac , jeux de hasard ) , en surmontant son propre égoïsme ( haumou ) , en menant une vie intègre et honnête , car le but suprême de l' existence est la libération ( mukti ) .
Le pèlerinage vers des lieux saints ne trouve pas sa place dans le [MASK] .
Le [MASK] n' est pas une religion fataliste .
On trouve des communautés [MASK] dans de nombreux pays .
En [MASK] , on estime la communauté [MASK] à quelque 20 millions de personnes , soit environ 2 % de la population indienne .
Les [MASK] sont installés principalement au [MASK] , mais aussi dans la région de [MASK] .
Ailleurs dans le monde , on trouve aussi d' importantes communautés [MASK] au [MASK] , [MASK] et dans les anciennes colonies britanniques -- [MASK] , [MASK] , [MASK] , [MASK] , etc .