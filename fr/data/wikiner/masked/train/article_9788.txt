La [MASK] [MASK] a vu le jour en 1987 .
La [MASK] [MASK] a obtenu 19,6 % des voix lors des élections aux chambres d' agriculture qui se sont déroulées en janvier 2007 .
La [MASK] [MASK] est présente dans la quasi totalité des départements français , régions ultra-marines comprises .
Les actions de [MASK] [MASK] sur les scènes nationale et internationale ont fait connaître le syndicat bien au-delà de ses sympathisants et des frontières françaises .
La [MASK] [MASK] milite pour une agriculture paysanne , respectueuse de l' environnement , de l' emploi agricole et de la qualité des produits .
Avec le réseau [MASK] [MASK] , elle se bat pour une reconnaissance du droit à la souveraineté alimentaire .