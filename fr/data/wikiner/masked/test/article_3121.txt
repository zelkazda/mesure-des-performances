[MASK] [MASK] , homme politique français , né le 17 juin 1938 à [MASK] ( [MASK] ) .
[MASK] [MASK] a également exercé des mandats locaux : maire de [MASK] de 1986 à 1989 , puis membre du conseil municipal de [MASK] de 1989 à 2002 ( adjoint au maire jusqu' en 2001 ) .
Il est actuellement vice-président du conseil régional de [MASK] dont il est membre depuis 1986 .