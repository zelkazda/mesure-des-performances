Comme indique le nom latin de l' espèce , l' abricotier vient d' [MASK] .
La fête de l' abricot est célébrée à [MASK] [MASK] chaque 19 juin .
La production mondiale est quant à elle dominée par la [MASK] , et plus particulièrement la région de [MASK] , avec environs 390 000 tonnes d' abricots produits en 2005 , suivie de l' [MASK] avec 285 000 tonnes .
Dans certains pays , comme le [MASK] , on consomme également l' amande située dans le noyau de l' abricot .