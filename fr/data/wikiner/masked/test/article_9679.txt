Le quartier de [MASK] [MASK] , principal quartier d' affaires de [MASK] , s' étend en partie sur [MASK] ( le reste étant partagé entre les communes de [MASK] et [MASK] ) .
[MASK] est une ville située en proche banlieue ouest de [MASK] , à deux kilomètres des limites de la capitale .
Elle s' étend sur la rive gauche de la [MASK] , du quartier de [MASK] [MASK] au sud , qui occupe une partie de son territoire , à [MASK] , au nord .
Elle est également limitrophe des communes de [MASK] [MASK] et [MASK] au nord , [MASK] à l' est , [MASK] au sud-est , [MASK] au sud-ouest et [MASK] à l' ouest .
Au départ , [MASK] était un petit hameau de pêcheurs et de vignerons .
Par la suite , il dépendra de la paroisse de [MASK] jusqu' en 1787 .
En 1606 , alors que le roi [MASK] [MASK] et la reine [MASK] [MASK] [MASK] revenaient de [MASK] , ils empruntèrent le bac permettant de traverser [MASK] [MASK] pour regagner [MASK] .
À la suite de ce " naufrage " , [MASK] [MASK] chargea [MASK] de construire un pont à l' emplacement de l' actuel pont de [MASK] .
En 1814 , le gouvernement provisoire fit établir dans la caserne de [MASK] un hôpital militaire destiné aux blessés des puissances alliées .
À la fin des années 1730 , [MASK] devient une ville de garnison .
Après la guerre , avec l' aménagement du quartier de [MASK] [MASK] , l' activité économique de [MASK] connaît un accroissement notable .
Peu à peu , [MASK] est passée du statut de ville industrielle à celle de ville à vocation tertiaire .
Au pont courbe à trois arches d' or maçonné de sable sur une rivière d' argent ombrée d' azur qui est du pont de [MASK] .
Les deux branches nouées aux couleurs tricolores de [MASK] [MASK] .
L' évolution du nombre d' habitants est connue à travers les recensements de la population effectués à [MASK] depuis 1793 .
[MASK] est divisée en deux cantons :
Pour les échéances électorales de 2007 , [MASK] fait partie des 82 communes , de plus de 3500 habitants ayant utilisé les machines à voter électroniques .
Au 1 er janvier 2010 , [MASK] est jumelée avec :
Autrefois , la [MASK] [MASK] [MASK] [MASK] permettait de desservir le cynodrome .
À en juger par son nom , la station de métro [MASK] [MASK] [MASK] [MASK] [MASK] , située de l' autre côté de la [MASK] , sur la commune de [MASK] , a aussi pour vocation de desservir le quartier de [MASK] de [MASK] .
Enfin , [MASK] bénéficie d' un accès direct aux importants réseaux de transport qui desservent le quartier d' affaires de [MASK] [MASK] :
[MASK] a servi de décors à de nombreux films .