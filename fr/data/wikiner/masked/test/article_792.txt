Communiste , internationaliste , [MASK] [MASK] défend les idées du trotskisme et met l' accent sur le rôle central de la classe ouvrière .
[MASK] [MASK] revendique environ 8000 adhérents .
Autour de lui le groupe s' élargit sensiblement lorsqu' avec d' autres il entre sur l' indication de [MASK] au [MASK] de [MASK] [MASK] ( c' est l ' " entrisme " ) .
Il déclenche et participe activement à la grève chez [MASK] .
[MASK] [MASK] , démissionnaire à l' été 1948 reparaît dès 1950 et en compagnie de [MASK] [MASK] , toujours actif chez [MASK] , tente de recoller les morceaux .
[MASK] [MASK] hebdomadaire commence à être diffusé à partir de 1963 d' abord sur 4 pages , puis sur 8 .
Le mouvement se reforme immédiatement sous le nom de son journal rebaptisé [MASK] [MASK] puis se renforce en nombre et multiplie les bulletins d' entreprise .
[MASK] [MASK] sera ensuite le premier groupe politique à présenter une femme à l' élection présidentielle , en 1974 , en choisissant pour porte-parole et candidate nationale [MASK] [MASK] .
[MASK] [MASK] connaît par la suite un certain succès médiatique et politique .
En 2002 , [MASK] [MASK] obtient 5,72 % des voix à l' élection présidentielle .
[MASK] [MASK] présente des candidats dans toutes les grandes élections depuis 1973 .
[MASK] [MASK] , porte-parole de l' organisation depuis 1973 est candidate aux élections présidentielle de 1974 , 1981 , 1988 , 1995 , 2002 et 2007 .
Aux élections municipales de 1995 , [MASK] présente 49 listes , obtenant 2,81 % des voix , soit 39 879 .
En 2001 , [MASK] présente 128 listes dans 109 villes différentes , qui recueillent 4,37 % des suffrages , soit 120 347 voix .
[MASK] obtient ainsi 33 élu ( e ) s dont 11 femmes , dans 22 villes différentes , sans fusionner ses listes au second tour avec la gauche plurielle .
Lors des élections régionales de 1998 , [MASK] présente des listes dans 68 départements , obtenant 4,5 % et 20 élus .
[MASK] appelle également à voter pour [MASK] [MASK] lors de l' élection présidentielle de 1969 .
Selon [MASK] , cette alliance est inspirée par l' hypothèse qu' il est probable qu' un fort courant d' électeurs préférerait voter [MASK] pour donner une gifle à la droite .
La réforme du mode de scrutin fait que les trotskystes ( [MASK] et [MASK] ) perdent tous leurs élus dans les conseils régionaux , malgré un score supérieur à 1998 .
À la différence des élections municipales de 2001 , où [MASK] ne présente que des listes autonomes , [MASK] [MASK] propose en 2008 des alliances locales aux autres partis de gauche .
À ces élections [MASK] présente donc près de 5000 candidats sur 188 listes différentes ( contre 128 en 2001 ) .
Dans 70 villes les militants de [MASK] [MASK] participent à des listes d' union , principalement conduites par le [MASK] [MASK] ( 38 listes ) ou le [MASK] [MASK] ( 26 listes ) , sur des listes qui comportent parfois d' autres partis comme la [MASK] , les [MASK] , le [MASK] , le [MASK] ou le [MASK] .
[MASK] présente par ailleurs des listes indépendantes dans 118 villes , là où aucun accord n' a pu être conclu .
Au premier tour , [MASK] obtient 36 élus ( dont 14 sur des listes indépendantes et 22 sur des listes d' union ) , au deuxième tour , 43 candidats sont élus sur des listes d' union .
Les listes [MASK] [MASK] sont alors conduites par :
[MASK] [MASK] se présente comme une libre association de militants qui œuvrent à la création d' un parti ouvrier , communiste , révolutionnaire capable de défendre les intérêts des travailleurs , à court terme ( licenciements , chômage , salaires , conditions de travail et de logement … ) et à long terme ( inégalités entre hommes et femmes , pollution , guerres , famines … ) .
Selon [MASK] , tous ces problèmes découlent de l' organisation capitaliste de la société et de la course au profit , et ne concernent pas que les travailleurs , mais l' ensemble de la population mondiale qui en subit les conséquences .
[MASK] [MASK] justifie son existence séparée au sein du mouvement trotskyste par sa volonté d' orienter son action en priorité vers la classe ouvrière .
[MASK] [MASK] estime que l' écart croissant entre les moyens techniques de plus en plus perfectionnés dont dispose l' humanité et la persistance , voire dans certaines régions l' aggravation , de la misère et des inégalités , rend possible et nécessaire le remplacement de la société capitaliste par une société communiste , c' est-à-dire par la mise en commun et le contrôle démocratique par l' ensemble de la population des capacités de production et d' échange dont dispose l' humanité pour assurer tous ses besoins .
[MASK] [MASK] voit par ailleurs dans la classe ouvrière la seule force sociale qui ait à la fois la force et l' intérêt de prendre en main une telle réorganisation des mécanismes de production et de redistribution des richesses .
[MASK] estime qu' une telle transformation de la société ne pourra que résulter d' une confrontation entre " ceux qui n' ont que leur travail à vendre " et ceux qui détiennent les grands moyens de production .
C' est pourquoi [MASK] se dit révolutionnaire .
Dans la continuité de ces militants , [MASK] [MASK] estime qu' il est nécessaire au prolétariat de disposer d' un parti qui défende ses intérêts .
C' est ce parti , ouvrier , communiste , révolutionnaire , que [MASK] cherche à construire .
[MASK] [MASK] fait également sienne la formule de [MASK] " les prolétaires n' ont pas de patrie " .
Pour [MASK] [MASK] , l' internationalisme découle de l' organisation même de la société capitaliste , qui est déjà mondiale et met en relations tous les peuples de la planète .
[MASK] [MASK] se revendique du trotskisme , qui est à ses yeux le seul courant communiste révolutionnaire à avoir toujours combattu le stalinisme , et à exister à l' échelle internationale .
Cependant , contrairement à la plupart des autres groupes trotskystes , [MASK] [MASK] estime qu' aucune des organisations internationales qui se nomment elles-mêmes [MASK] [MASK] ne constitue une véritable internationale .
De même que [MASK] [MASK] milite en [MASK] pour la création d' un parti ouvrier communiste révolutionnaire , elle milite pour la construction d' une véritable internationale .
Dans les élections , [MASK] défend les revendications dont elle estime qu' elles pourraient mettre un coup d' arrêt à la dégradation des conditions de vie de toute une partie pauvre de la population .
Pour l' élection présidentielle de 2007 , [MASK] [MASK] met l' accent sur la nécessité que la population puisse contrôler les choix économiques des grandes entreprises , insistant sur le fait que " la gestion capitaliste des entreprises , menée dans le secret des conseils d' administration en fonction de la seule rentabilité financière , est non seulement catastrophique pour les travailleurs mais aussi pour toutes les autres couches populaires , et même pour toute la société " ( [MASK] [MASK] , [MASK] , 3 juin 2006 ) .
[MASK] défend depuis plusieurs années un " plan d' urgence " :
Concernant toutes ces propositions , [MASK] affirme à chaque élection , dans les professions de foi , ses tracts , son journal , qu' elles ne constituent pas un programme électoral au sens où il suffirait d' élire un représentant pour qu' il soit appliqué , mais que par contre de telles revendications devraient être mises en avant lors d' une mobilisation des travailleurs .
Pour [MASK] , être révolutionnaire ne signifie pas attendre passivement le " grand soir " , mais militer dès aujourd'hui pour contribuer à modifier l' état d' esprit de la classe ouvrière , par la propagande générale pour ses idées , et par les participations à des luttes ( grèves , manifestations … ) .
[MASK] avance en effet l' idée que c' est dans les luttes que les travailleurs peuvent apprendre à s' auto organiser et prendre conscience de leur poids dans la société et de leur force .
[MASK] définit son orientation comme principalement tournée vers les travailleurs , y compris les travailleurs du rang .
[MASK] [MASK] entend également être présente " là où habitent les travailleurs , dans leurs quartiers , dans leurs communes " , en diffusant sa presse et discutant avec eux .
Dans la tradition du mouvement communiste , [MASK] [MASK] participe aux élections dans la mesure de ses forces .
Quand [MASK] [MASK] dispose d' élus ( conseillers municipaux , conseillers régionaux , députés européens ) , son principe affiché est de soutenir les mesures qui lui semblent aller dans le sens des intérêts des travailleurs , et de voter contre celles qui dégradent leurs conditions de vie .
Par ailleurs , [MASK] [MASK] organise régulièrement des conférences sur des sujets de politique générale ( sujets d' actualités ou historiques ) .
Ainsi , à [MASK] , le [MASK] [MASK] [MASK] , qui rassemble environ un millier de personnes , sur des sujets comme :
[MASK] organise enfin des fêtes politiques , dont la fête annuelle de [MASK] , qui rassemble un large public le week-end de la pentecôte .
A la fête de [MASK] , le visiteur peut ainsi trouver :
Durant les années 1970 , [MASK] [MASK] connaît des ruptures d' importances variables .
Cette brochure influence d' autres militants , comme ceux qui publient peu de temps après le texte " Pour que les travailleurs ne votent plus à gauche et ne se fassent plus massacrer , six militants quittent [MASK] [MASK] " .
et aussi les mêmes idées -- à part sur la question de l' [MASK] .
A sa décharge on peut dire que le groupe tenta notamment une critique plus radicale de la fonction des syndicats et du stalinisme que [MASK] ; qu' il essaya de donner une formation politique beaucoup moins dogmatique ; et qu' il essaya aussi dans sa presse et ses bulletins d' entreprise ( du moins les premières années ) de réintroduire la propagande socialiste de façon plus systématique et vivante .
[MASK] [MASK] comprend de 1996 à 2008 une petite fraction , qui obtient environ 3 % des voix lors du congrès annuel de l' organisation .
Suite à un désaccord initial sur l' évolution de l' [MASK] depuis 1989 , ces militants ont demandé à fonctionner séparément , c' est-à-dire en fraction .
Ensuite , s' ajoute aux griefs sa participation à la construction d' un [MASK] [MASK] [MASK] ( " [MASK] " dans lequel la [MASK] se dissout par la suite ) .
[MASK] [MASK] , comme les autres organisations d' extrême-gauche , est parfois accusé de sectarisme , c' est-à-dire de ne pas se tourner suffisamment vers les autres courants aux idées proches et de fonctionner replié sur lui-même , par d' autres courants politiques .
Cette partie vise à présenter ce qu' en disent différents protagonistes : d' autres courants ou partis politiques , certains militants ou anciens militants de [MASK] , et enfin [MASK] [MASK] elle-même .
Parmi les critiques externes ayant accusé [MASK] [MASK] , on peut citer :
Le principal dirigeant de [MASK] [MASK] , [MASK] [MASK] , alias [MASK] , a publié un livre d' entretiens : La véritable histoire de [MASK] [MASK] , qui contient des réponses à ces accusations .