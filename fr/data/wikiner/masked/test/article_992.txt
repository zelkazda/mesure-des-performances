Par la suite il se rend en [MASK] où il étudie le droit canonique et la médecine à l' [MASK] [MASK] [MASK] où il était inscrit à la nation universitaire allemande , puis l' astronomie dans les cours de [MASK] [MASK] [MASK] .
Mais c' est à [MASK] , le 31 mai 1503 , qu' il obtient le titre de docteur en droit canon .
L' intérêt de [MASK] pour la géographie et l' astronomie est encouragé par son professeur .
Les deux hommes observent ensemble de nombreuses occultations , éclipses de lune , ainsi que l' occultation de l' étoile [MASK] le 9 mars 1497 à [MASK] .
Il retourne finalement en [MASK] pour finir ses études à la faculté de droit et de médecine de l' [MASK] [MASK] [MASK] ( l' université où [MASK] enseignera plus tard ) .
Cette œuvre magistrale ne sera publiée , par un imprimeur luthérien de [MASK] , que le 24 mai 1543 , peu de temps avant la mort de [MASK] .
Elle n' aurait sans doute jamais été publiée sans l' intervention enthousiaste d' un jeune professeur de mathématiques , [MASK] [MASK] [MASK] .
Avant [MASK] , la façon de voir le cosmos reposait sur la thèse aristotélicienne que la [MASK] est au centre de l' univers et que tout tourne autour d' elle : " l' univers géocentrique " .
La description des mouvements des astres reposait sur le système dit " de [MASK] " et la théorie des épicycles .
Ce qui est intéressant , c' est qu' entre l' astronomie arabe et l' astronomie de [MASK] , il y a continuité mathématique , quand bien même y a-t-il discontinuité au niveau cosmologique puisque qu' il met le soleil , et non plus la terre au centre de l' univers .
On ne peut rien comprendre à ce que l' on appelle à présent la révolution copernicienne si l' on passe sans transition de [MASK] à [MASK] , en sautant par-dessus quatorze siècles où il ne se serait rien passé .
Il tire probablement cette idée de la lecture des anciens , en particulier des traces laissées par [MASK] [MASK] [MASK] .
Cette théorie est en partie motivée par des considérations philosophiques et théologiques , ce qui provoque un fort rejet du travail de [MASK] .
[MASK] avait tenu compte de cette distinction introduite par son système .
[MASK] conjecture aussi que l' axe de la terre oscille comme celui d' une toupie , ce qui explique la précession .
La théorie de [MASK] contredit la théorie de [MASK] : [MASK] conserve toutefois certains éléments de l' ancien système : ainsi l' idée aristotélicenne ( pourtant abandonnée par [MASK] et même probablement déjà par [MASK] ) des sphères solides , ou la sphère des fixes physique , est-elle conservée par [MASK] .
Le nouveau système proposé par [MASK] explique le mouvement journalier du [MASK] et des étoiles par la rotation terrestre .
Il explique , en outre , certains phénomènes qui n' étaient expliqués qu' à grand peine et de manière imparfaite , notamment le mouvement rétrograde des planètes externes , ( [MASK] , [MASK] , [MASK] ) .
[MASK] avance aussi une théorie sur l' ordre des planètes , leurs distances et , par conséquent , leur période de révolution .
En effet , [MASK] contredit [MASK] en affirmant que plus l' orbite d' une planète est grande , plus il lui faudra de temps pour faire une révolution complète autour du [MASK] .
[MASK] [MASK] obtint plus tard de ses observations des relations mathématiques précises allant dans ce sens , et [MASK] [MASK] les démontra par sa théorie de la gravité .
Toutefois en 1588 , bien après la mort de [MASK] , on arrive à un certain compromis .
Près de cent ans après la parution du livre [MASK] [MASK] [MASK] [MASK] [MASK] , réticences et hésitations existent toujours .
Si certains philosophes jésuites sont profondément convaincus , certains sont même disciples de [MASK] , d' autres acceptent plutôt le système de [MASK] [MASK] .
Vers 1750 et en plusieurs étapes , le pape [MASK] [MASK] abandonne de fait le système géocentrique .
[MASK] a retardé de plusieurs années la parution de l' œuvre de sa vie .
Ses croyances et la peur de la réaction du [MASK] et de [MASK] en sont les principales raisons .
[MASK] sut libérer les scientifiques et chercheurs de leurs préjugés ( le système cosmologique d' [MASK] et de [MASK] était longtemps resté la référence ) .
L' astéroïde [MASK] [MASK] [MASK] [MASK] a été nommé en son honneur .
Il est né sur le territoire de [MASK] [MASK] , dans une ville hanséatique avec une population majoritairement allemande .
Il a longtemps été débattu pour savoir si [MASK] [MASK] était vraiment allemand ou polonais .
Cette discussion est devenue une féroce querelle savante aux époques nationalistes ( de la deuxième moitié du XIX e siècle jusqu' à la [MASK] [MASK] [MASK] ) " .
Le lieu exact d' inhumation de [MASK] demeura longtemps inconnu avant que des ossements soient retrouvés en 2005 dans la cathédrale de [MASK] ( [MASK] ) , près de l' autel dont il avait la charge .
Le 22 mai 2010 , l' astronome dont les restes ont été identifiés , a été enterré à nouveau à la cathédrale de [MASK] dans le nord de [MASK] [MASK] , au lendemain du 467 e anniversaire de sa mort .