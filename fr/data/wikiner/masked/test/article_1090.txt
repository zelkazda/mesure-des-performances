Le terme pharaon désigne a posteriori les souverains d' [MASK] durant l' [MASK] [MASK] .
Le pharaon était à la fois l' administrateur principal , le chef des armées , le premier magistrat et le prêtre suprême de l' [MASK] [MASK] .
Il est le fils de [MASK] .
D' après l' historiographie égyptienne , la monarchie fut créée par le démiurge qui la transmit aux dieux ses successeurs , puis à des créatures divines , les suivants d' [MASK] qui , dans les listes royales , précèdent immédiatement les rois historiques .
Le mot français " pharaon " dérive du grec pharaô , mot introduit dans cette langue par la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Sur le [MASK] [MASK] ( 5,2 ) , on trouve
On en trouve également deux dans l' [MASK] : celui de la naissance et du mariage de [MASK] -- qui meurt au verset 2 , 23 -- puis celui de la [MASK] [MASK] [MASK] emmenée par le même [MASK] .
Il est identifié avec [MASK] [MASK] .
[MASK] [MASK] écrit à ce sujet :
-- [MASK] [MASK] .
Pharaon est absent également dans l' œuvre monumentale des savants de [MASK] , la [MASK] [MASK] [MASK] [MASK] parue en 1821 .
[MASK] [MASK] fut le premier à se servir du mot en dehors du contexte biblique .
[MASK] ne donna jamais d' explication pour l' emploi de ce barbarisme .
En 1856 , [MASK] [MASK] [MASK] proposa une réponse satisfaisante où pharaon vient du mot égyptien pour désigner le palais gouvernemental ( pr-ˁȝ ) .
Nous retrouvons là les propositions d' [MASK] [MASK] et d' [MASK] [MASK] [MASK] pour l' origine de pharaon .
Elle serait ensuite employée pour désigner [MASK] seul .
Pour d' autres égyptologues , cette attestation remonterait à l' époque de [MASK] [MASK] ou de [MASK] [MASK] [ réf. nécessaire ] .
Pendant la [MASK] [MASK] [MASK] [MASK] et la [MASK] [MASK] , les rois sont étrangers ou vassaux et certains ne parlent pas l' égyptien .
Le premier sera [MASK] , suivit de [MASK] [MASK] [MASK] à titre posthume .
[MASK] [MASK] voulait que ses tribunaux connaissent les lois régissant les différents groupes ethniques de son royaume , pour les juger selon leurs coutumes .
Entre les deux publications , [MASK] avait trouvé la clé de l' écriture égyptienne et sa méprise entrait dans la tradition moderne .
Titulature de [MASK] [MASK] [MASK] :
La tradition égyptienne faisait de [MASK] l' unificateur du pays ( alors divisé en deux royaumes ) et le premier des pharaons humains après le règne des suivants de [MASK] .
Il en subsiste des abrégés fournissant une liste de rois classés en trente et une dynasties , regroupées de la période thinite à la [MASK] [MASK] .
[MASK] , le soleil de l' univers et des hommes sur terre , s' est retiré vers le ciel en laissant aux dieux la direction du monde , puis à des rois semi-divins et enfin à des monarques humains , les pharaons , qui sont ses fils et représentants sur terre .
La mythologie fournit d' ailleurs des exemples d' inceste , avec [MASK] et [MASK] , ou encore [MASK] et [MASK] et dans le même ordre d' idées , certains mariages consanguins entre pharaon et sa fille ou ses filles .
De telles unions sont attestées notamment pour [MASK] et [MASK] [MASK] .
Faute d' héritier mâle , ou quand le nouveau roi est encore un tout jeune enfant ( [MASK] [MASK] ) , la fonction peut échoir à une femme de sang divin ( [MASK] , [MASK] , [MASK] ) plutôt qu' à un homme qui ne ll'est pas ; elle en est donc dépositaire jusqu' à la transmission à son époux , ce qui ne signifie pas que la légitimité monarchique repose uniquement sur le mariage avec une fille de sang .
Le dieu marquait son choix par un signe , une naissance prodigieuse , un rêve de l' heureux élu ( [MASK] [MASK] ) au pied du [MASK] [MASK] , ou un oracle ( [MASK] , [MASK] [MASK] [MASK] ) .
Puis [MASK] prend les traits de l' actuel roi :
Puis [MASK] donne à [MASK] , le potier divin , l' ordre de modeler l' enfant et son ka .
L' enfant est présentée à [MASK] qui lui promet la royauté terrestre ; il en confie l' allaitement à [MASK] , la nourrice divine .