Par exemple [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] a été le vingt-septième astéroïde découvert dans la seconde quinzaine d' août 1992 .
Par exemple voici un extrait de la notice de [MASK] [MASK] [MASK] [MASK] :
Ainsi , [MASK] [MASK] est le premier objet découvert dans la deuxième moitié de juillet 1904 .
Hormis [MASK] [MASK] [MASK] [MASK] , chacun possédait son propre symbole .
Il a utilisé des numéros entourés au lieu des symboles , bien que sa numérotation ne commençait qu' avec [MASK] qui portait alors le numéro ( 1 ) , les quatre premiers astéroïdes continuant à être désignés par leurs symboles traditionnels .
Les objets de [MASK] à [MASK] ne seront représentés par un nombre cerclé qu' à partir de l' édition de 1867 .
Quelques autres astéroïdes ( [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] ) seront également représentés par des symboles en parallèle du système de numérotation .
En 1893 , les 25 lettres furent insuffisantes et on commença les désignations à double lettre : [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , etc .
On décida de continuer l' année suivante , et ainsi [MASK] [MASK] fut suivi de [MASK] [MASK] .