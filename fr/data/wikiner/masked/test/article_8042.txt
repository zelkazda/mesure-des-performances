Son invention va en tout cas révolutionner les méthodes traditionnelles de production des livres en [MASK] .
Il sera spolié de son matériel par l' un de ses associés , [MASK] [MASK] , et ne sera sauvé de la misère que grâce à [MASK] [MASK] [MASK] [MASK] qui lui accorda une pension à vie et le titre de gentilhomme de sa cour .
Les lieux de séjour , et les activités de [MASK] , ne sont pas connus entre 1400 et 1420 .
Entre 1434 et 1444 , la famille [MASK] s' installe à [MASK] .
Jean [MASK] fait son apprentissage pour devenir orfèvre .
[MASK] , le plus ancien livre imprimé avec caractères mobiles en métal encore en état date de 1377 .
En 1450 , [MASK] [MASK] persuade le riche banquier [MASK] [MASK] de l' aider à financer son projet .
En garantie d' hypothèque , [MASK] devra engager sa presse et les outils et réglera 6 % d' intérêt l' an .
À l' époque , le seul livre capable d' un succès immédiat est la [MASK] dans sa version en latin de [MASK] [MASK] , la [MASK] [MASK] .
L' idée première de [MASK] pour imposer son invention sera d' imiter parfaitement les livres manuscrits ( codex ) .
À ce jour , on n' a pas trouvé le modèle précis de [MASK] utilisé par [MASK] .
C' est à cette époque que [MASK] perfectionne simultanément ce qui constitue la globalité de son " invention " :
Les lettres d' indulgence à 31 lignes ( dont la plus vieille , datée du 22 octobre 1454 , est le premier spécimen d' une œuvre d' imprimerie venant de [MASK] ) et les petits ouvrages connus
ont semble-t-il été produits par un apprenti de [MASK] .
Composée à partir de la [MASK] de [MASK] [MASK] , la [MASK] [MASK] [MASK] est considérée comme l' œuvre la plus technique et la plus esthétique de l' imprimerie de [MASK] .
Entre 1452 et 1455 , la " [MASK] à 42 lignes " a été imprimée à environ 180 exemplaires .
Malheureusement pour [MASK] , l' impression des livres connaît un succès mitigé .
[MASK] refusant de payer -- ou ne le pouvant pas -- les intérêts et le capital qu' il lui avait prêtés , il décide de porter l' affaire en justice .
Ils s' installent pour vendre des livres à [MASK] en 1463 , à une date où l' imprimerie n' existe pas encore en [MASK] .
En janvier 1465 , [MASK] fut nommé gentilhomme auprès de l' archevêque de [MASK] [MASK] [MASK] [MASK] [MASK] .
Il mourut en 1468 , largement méconnu par ses contemporains , et fut enterré à [MASK] dans un cimetière qui sera détruit plus tard .
Associé à [MASK] [MASK] et à [MASK] [MASK] , [MASK] [MASK] est l' inventeur de l' imprimerie à caractères mobiles en [MASK] .
Contrairement à une idée reçue , [MASK] n' a pas inventé l' imprimerie , déjà connue en [MASK] bien avant sa naissance , et il n' est pas davantage le pionnier de l' imprimerie à caractères mobiles , qu' il a toutefois probablement " réinventée " en [MASK] , en améliorant ou perfectionnant les procédés d' impression et de reproduction , déjà connus de l' époque , comme la presse xylographie , actionnée à la main , pour imprimer , par exemple , les cartes à jouer .
En effet , la technique d' impression avec des caractères mobiles est attestée , avant lui , en [MASK] ( céramique puis xylographie ) et en [MASK] ( métallographie ) .
Pour parvenir à ses fins , [MASK] est à l' origine de nombreuses innovations :
Depuis longtemps , l' histoire conteste à [MASK] [MASK] l' invention de l' imprimerie typographique et celui-ci n' a jamais rien fait pour s' assurer la paternité de son invention .
Le premier colophon apparait avec les impressions de [MASK] [MASK] et [MASK] [MASK] .
Le 3 février 1468 , [MASK] lègue son invention à l' humanité .
Selon la légende , c' est en voyant fonctionner un pressoir à vin à [MASK] , que [MASK] eut l' idée d' inventer un nouveau procédé d' impression qui permit de produire 180 [MASK] en l' espace de trois ans , alors qu' un moine recopiait une [MASK] dans le même temps .
En imaginant la mobilité des caractères et en améliorant leur longévité grâce à leur consistance métallique , [MASK] rendait les caractères réutilisables et interchangeables .
Cette révolution s' étend à toute l' [MASK] , principalement en [MASK] et aux [MASK] .
Une grande quantité des témoignages sur [MASK] provient des archives judiciaires , l' inventeur étant manifestement assez procédurier .