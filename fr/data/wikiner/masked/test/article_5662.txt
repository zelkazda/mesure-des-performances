Il était le fils d' [MASK] [MASK] [MASK] et d' [MASK] [MASK] [MASK] .
Il a abdiqué au terme de la longue polémique et de la crise nationale suscitée par son comportement controversé lors de la [MASK] [MASK] [MASK] .
En 1935 , un accident de voiture à [MASK] ( [MASK] ) causa la mort de la [MASK] [MASK] et blessa le roi , qui était au volant .
En 1941 il épousa [MASK] [MASK] dont il eut trois enfants :
Durant les années 1930 [MASK] avait fermement soutenu la politique gouvernementale des " mains libres " , c' est-à-dire de la stricte neutralité de la [MASK] , ce qui eut pour effet d' interdire toute coopération préalable entre états-majors français et belge .
Fidèle à son engagement vis-a-vis de l' armée mais contre leur volonté , [MASK] se constitua prisonnier de guerre .
Le souverain fut notamment radié de l' ordre de la [MASK] [MASK] [MASK] .
Le 19 novembre 1940 , il rendit une visite aimable à [MASK] [MASK] pour discuter du sort de la population civile , mais sans obtenir de résultats .
Il s' abstint de tout acte politique et refusa d' administrer la [MASK] sous occupation allemande .
En décidant dès juillet 1940 que le roi serait interdit d' activités politiques , [MASK] lui épargna sans doute d' avoir à choisir entre la réserve et la prise du pouvoir .
Il se remaria secrètement en septembre 1941 avec une jeune roturière [MASK] [MASK] qui , renonçant au titre de reine , fut élevée au rang de princesse et titrée " [MASK] [MASK] [MASK] " .
Enfin , les sentiments pro-nazis que l' on prêtait à la [MASK] [MASK] [MASK] parurent accrédités auprès de la population lorsque celle-ci apprit qu' [MASK] avait envoyé des fleurs et un mot de félicitations lors du mariage .
Le gouvernement , ainsi qu' une partie de la population belge , étant opposés à son retour sur le trône , le [MASK] [MASK] , son frère , réputé plus favorable à [MASK] [MASK] , assuma la régence .
Par ailleurs , le texte ne disait mot de la [MASK] , et parlait de la future présence alliée en [MASK] libérée comme d' une " occupation " .
Il s' établit en [MASK] .
En 1946 , une commission d' enquête exonéra le roi [MASK] de toute accusation de trahison .
Une consultation populaire eut lieu en 1950 qui autorisa à 57 % le roi à rentrer en [MASK] .
[MASK] [MASK] influença le règne de son fils jusqu' au mariage de ce dernier .
En 1959 , le gouvernement lui demanda de cesser de vivre sous le même toit que son fils et de quitter le palais de [MASK] .