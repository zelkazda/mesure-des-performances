Il est bordé par le [MASK] [MASK] au sud .
Le [MASK] [MASK] [MASK] a sur son territoire 700000 habitants appelés les vaudois et les vaudoises , vivant principalement dans l' agglomération lausannoise , de la [MASK] [MASK] , d' [MASK] et de [MASK] [MASK] [MASK] .
Il a gagné son indépendance le 24 janvier 1798 par l' œuvre de [MASK] et a adhéré à la [MASK] [MASK] le 14 avril 1803 .
Ses frontières s' étendent des bords du [MASK] [MASK] à ceux du [MASK] [MASK] [MASK] et du [MASK] [MASK] [MASK] .
Le [MASK] [MASK] [MASK] possède une enclave dans le [MASK] [MASK] [MASK] : [MASK] .
Le [MASK] [MASK] [MASK] possède deux enclaves dans le [MASK] [MASK] [MASK] : [MASK] .
Le [MASK] [MASK] [MASK] possède trois enclaves dans le [MASK] [MASK] [MASK] : [MASK] , [MASK] , [MASK] .
Le 28 juin 2007 , les vignobles de [MASK] ont été inscrits au patrimoine mondial de l' humanité , [MASK] .
Il était le troisième canton le plus peuplé derrière le [MASK] [MASK] [MASK] et le [MASK] [MASK] [MASK] .
Depuis le début de la [MASK] [MASK] , les pasteurs devaient tenir annuellement un tableau des naissances et des décès .
Sous l' [MASK] [MASK] [MASK] , ce travail fut transmis aux administrations municipales .
Les statistiques du 31 décembre 2008 comptabilisent 684922 habitants dans le [MASK] [MASK] [MASK] .
La solde migratoire de la population suisse dans le [MASK] [MASK] [MASK] est pour la majorité des années négative , baissant de 990 en 1998 et de 554 en 2008 ; elle augmenta cependant en 1999 et 2001 de respectivement 266 et 133 .
La population résidente du [MASK] [MASK] [MASK] est composée , selon les statistiques du 31 décembre 2008 , de 70,4 % de [MASK] et 29,6 % d' étranger , totalisant respectivement 202605 et 582317 personnes .
En septembre 2009 , le taux de chômage de la population atteignait 5,4 % dans le [MASK] [MASK] [MASK] , soit 17980 personnes , contre 3,9 % en [MASK] .
De 5,3 % en moyenne , le chômage dans le [MASK] [MASK] [MASK] atteint 7,2 % dans le [MASK] [MASK] [MASK] contre 3,4 % dans celui du [MASK] .
Pour plus de détails sur la politique d' une commune vaudoise voir la liste des [MASK] [MASK] [MASK] [MASK] [MASK] .
La plus petite commune du canton est [MASK] ( 32 hectares ) , la plus grande est [MASK] ( 11376 hectares ) , suivie de la commune du [MASK] ( 9922 hectares ) .
La commune la plus peuplée est le chef-lieu [MASK] avec 131130 habitants , suivie d' [MASK] avec 25252 de [MASK] avec 23513 et de [MASK] avec 17,983 habitants .
La commune la moins peuplée est [MASK] avec 31 habitants .
La première autoroute a été inaugurée en 1964 , reliant [MASK] à [MASK] .
Quatre autoroutes traversent le canton , [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] , et [MASK] [MASK] [MASK] .
Deux ans plus tard , en 1858 , [MASK] est reliée à [MASK] .
L' année 1860 a vu le réseau s' étendre , de [MASK] à [MASK] , et qui atteindra [MASK] dans le [MASK] [MASK] [MASK] en 1878 .
En 1862 , la ligne , [MASK] , [MASK] , [MASK] , [MASK] est inaugurée , non sans peine .
Plusieurs de ces entreprises de transport font partie de la communauté tarifiaire [MASK] [MASK] , permettant ainsi aux usagers d' emprunter différents moyens de transport avec le même ticket ou abonnement pour se rendre d' un point à un autre .
Le premier plan d' étude du [MASK] [MASK] [MASK] parut le 19 février 1868 .
[MASK] [MASK] [MASK] fut l' un des pionnier dans le canton de la pédagogie moderne .
En [MASK] , la scolarisation des élèves est confiée aux cantons , de l' école enfantine à l' université .
Le canton applique les règles d' [MASK] , qui vise à harmoniser la scolarité obligatoire entre les différents cantons .