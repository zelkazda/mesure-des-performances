Ayant réalisé une grande partie de sa filmographie aux [MASK] , il est célèbre pour avoir tourné des films provocateurs , assez violents ( [MASK] , [MASK] [MASK] ) et/ou érotiques ( [MASK] [MASK] , [MASK] ) .
Il fait ses études à l' [MASK] [MASK] [MASK] à la fois en mathématique et en physique .
Il dirige des films à gros budget et violents , tel [MASK] ( 1987 ) et [MASK] [MASK] ( 1990 ) qui ont la particularité de mélanger pur divertissement et critique acerbe des dérives de la société contemporaine , celle des [MASK] en particulier .
Ainsi [MASK] est-il un tableau au vitriol du capitalisme et de l' ultra libéralisme où l' aspect humain est écrasé par les intérêts politiques et financiers , alors que [MASK] [MASK] montre les dérives de la société de consommation et des plaisirs .
Il continue ses succès avec [MASK] [MASK] ( 1992 ) puis le " flop " de [MASK] ( 1995 ) .
Après ce film il revient à la violence qui a marqué ses premiers films et réalise [MASK] [MASK] ( 1997 ) et [MASK] [MASK] ( 2000 ) .
[MASK] [MASK] lui permet de renouer avec son style provocateur et iconoclaste du cinéma hollywoodien : cette fois-ci , il s' attaque au culte du militarisme et aux mécanismes de propagande des masses .
Il participe au [MASK] [MASK] [MASK] [MASK] de [MASK] en 2001 .
En 2005 , il revient aux [MASK] pour tourner son premier film néerlandais depuis près de trente ans , [MASK] [MASK] .
Très grosse production à l' échelle néerlandaise , le film connaît un net succès , battant en brèche les idées reçues sur la [MASK] [MASK] [MASK] .