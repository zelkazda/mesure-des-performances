[MASK] est une ville-arrondissement d' [MASK] , capitale de la [MASK] .
Elle se situe dans le [MASK] [MASK] [MASK] , entre les parties supérieures et médianes de l' [MASK] et la [MASK] [MASK] [MASK] [MASK] [MASK] .
[MASK] n' est citée qu' à partir de 1206 , et devint alors une résidence princière et royale .
[MASK] forme le noyau de l' agglomération du même nom .
La région se positionne économiquement comme l' une des plus dynamiques d' [MASK] .
En 1745 , un traité de paix , qui assurait la [MASK] à la [MASK] , y fut conclu avec l' [MASK] et la [MASK] .
[MASK] accueille aussi d' exceptionnelles collections d' œuvres d' art .
À la fin de [MASK] [MASK] [MASK] , un tiers de la ville fut détruit du 13 au 15 février 1945 par la [MASK] [MASK] [MASK] , avec l' appui de l' aviation américaine .
À la difficile situation de l' après-guerre suivit le régime communiste de la [MASK] , lorsque de nombreux efforts de reconstruction de la vieille ville baroque sont entrepris .
La reconstruction de la [MASK] a de particulier que , non seulement elle a été reconstruite à l' identique , mais en réutilisant tous les matériaux d' origine récupérables .
Dans un premier temps , il n' était pas prévu de reconstruire l' église , mais de laisser les ruines dans leur état et d' en faire un monument commémoratif ; c' est ce qui explique , avec le manque de fonds et le peu d' importance attaché à la religion par le régime communiste , la reconstruction tardive de la [MASK] .
Les efforts de reconstruction se sont accélérés avec la [MASK] [MASK] [MASK] [MASK] et culminent en 2006 avec les fêtes des 800 ans de la ville .
Quoi qu' il en soit , [MASK] , à nouveau presque aussi belle qu' autrefois , est redevenue , avec [MASK] , [MASK] ou [MASK] , l' un des haut-lieux du tourisme en [MASK] .
[MASK] joue aussi un rôle important dans la vie musicale avec l' orchestre de la [MASK] , une des plus anciennes formations du monde puisque créée en 1548 , et celui de l' opéra .
Le 13 février 2005 , à l' occasion des commémorations du [MASK] [MASK] [MASK] en 1945 , entre 3000 et 5000 manifestants d' extrême-droite ont manifesté devant le parlement de [MASK] .
La ville compte une usine automobile du groupe allemand [MASK] [MASK] qui y produit la [MASK] .
Les technologies de micro-electronique sont présentes avec notamment l' américain [MASK] et l' allemand [MASK] [MASK] .