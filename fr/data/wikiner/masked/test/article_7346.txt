En 1700 , le [MASK] est envahi par le [MASK] avec l' aide de la demi-sœur du chogyal , qui avait été écartée du trône .
En 1791 , la [MASK] envoie des troupes soutenir le [MASK] et défendre le [MASK] contre les [MASK] .
À la suite de la défaite du [MASK] , la [MASK] [MASK] prend le contrôle du [MASK] .
À la suite de l' arrivée du [MASK] [MASK] dans l' [MASK] voisine , le [MASK] s' allie avec son ennemi , le [MASK] .
Les [MASK] attaquent le royaume , envahissant la région , y compris le [MASK] .
Les traités signés entre le [MASK] et le [MASK] conduisent ce dernier à restituer en 1817 les territoires précédemment annexés .
En 1890 , le [MASK] devient un protectorat britannique .
Ce traité reconnaissait notamment les frontières entre le [MASK] et le [MASK] .
En 1947 , un référendum rejette l' intégration du [MASK] dans une [MASK] [MASK] nouvellement indépendante ; le premier ministre indien , [MASK] [MASK] , accorde un statut de protectorat au royaume .
En 1973 , à la suite d' émeutes devant le palais , le pays demande officiellement la protection de l' [MASK] .
En avril , l' armée indienne prend le contrôle de [MASK] et désarme les gardes du palais .
Un référendum ( auquel 59 % des électeurs participent ) approuve l' union avec l' [MASK] à 97,5 % .
Le sommet du [MASK] ( 8 598 m ) , situé à cheval sur le [MASK] et le [MASK] , est le troisième plus haut sommet du monde et le point culminant de l' [MASK] .
Le [MASK] est entouré par les chaînes de l' [MASK] sur ses frontières nord , est et ouest .
Huit cols le relient au [MASK] , au [MASK] et au [MASK] .
De nombreux ruisseaux alimentés par la fonte des neiges ont creusé des vallées fluviales , à l' ouest et au sud du [MASK] .
Les sources chaudes du [MASK] sont connues pour leurs propriétés médicinales et thérapeutiques .
Les collines du [MASK] sont principalement constituées de gneiss et de roches à moitié schisteuses , recouvrant leur sol d' une terre argileuse brune généralement pauvre et peu profonde .
Une grande partie du territoire du [MASK] est couverte par des roches précambriennes et est nettement plus jeune que les collines .
Le climat du [MASK] varie de subtropical dans le sud à celui de toundra dans le nord .
Les sommets du nord-ouest du [MASK] sont toujours gelés .
Le [MASK] connait cinq saisons : l' hiver , été , le printemps et l' automne , ainsi qu' une saison de mousson entre juin et septembre .
De toutes les teintes , de toutes les tailles , les orchidées sont la parure du [MASK] .
Le [MASK] est subdivisé en quatre districts , chacun dirigé par un gouverneur nommé par le gouvernement central , le collecteur de district , qui est en charge de l' administration des zones civiles .
En 2004 , le produit intérieur brut du [MASK] était estimé à 478 millions de dollars .
Le [MASK] est le premier producteur de cardamome en [MASK] .
Au cours des dernières années , le gouvernement du [MASK] a largement favorisé le tourisme .
Le 6 juillet 2006 , le col de [MASK] [MASK] a été ouvert , reliant [MASK] , au [MASK] , à l' [MASK] .
En raison de son terrain accidenté , le [MASK] ne possède pas d' aéroport ou de ligne ferroviaire .
L' aéroport est situé à environ 124 km de [MASK] .
La compagnie de transport nationale du [MASK] gère des services de bus et de camion .
Avec 50 000 habitants , [MASK] est la seule ville importante ; 11,06 % de la population du [MASK] est urbaine .
Le [MASK] est représenté au niveau fédéral par un siège dans chacune des deux chambres , le [MASK] [MASK] et le [MASK] [MASK] .