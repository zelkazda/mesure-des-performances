Au cours de certaines de ces réunions , le plus puissant des feudataires est parfois nommé hégémon ( 霸 ) , prenant ainsi la tête des armées vassales des [MASK] .
Au sud , les princes de [MASK] et de [MASK] se proclament rois , affirmant ainsi leur indépendance vis-à-vis des [MASK] .
Vers le milieu du cinquième siècle , le système féodal mis en place par les [MASK] n' est plus réellement appliqué en pratique .
On entre dans la période des [MASK] [MASK] .