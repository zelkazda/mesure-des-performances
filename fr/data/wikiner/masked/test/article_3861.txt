Elle est le siège de la préfecture et la troisième ville la plus peuplée du département après [MASK] et [MASK] .
[MASK] est situé à 41 km au sud-est de [MASK] , dans un méandre de la [MASK] , entre [MASK] et [MASK] .
[MASK] élève [MASK] au rang de duché .
Quand [MASK] est chassé de [MASK] en 1138 , c' est à [MASK] qu' il vient poursuivre son enseignement .
[MASK] devient sa préfecture .
Deux autres ponts et une pénétrante ont ensuite été réalisés dans les années 1970 pour permettre une meilleure circulation dans la ville [MASK] [MASK] .