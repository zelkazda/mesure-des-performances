Il s' agit du volet gauche d' un diptyque représentant la crucifixion et conservé au [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] .
À la mort de celui-ci , en janvier 1425 , il rejoint en [MASK] son frère , [MASK] [MASK] [MASK] , également peintre .
Il entre alors au service du duc de [MASK] , [MASK] [MASK] [MASK] , le 19 mai 1425 , comme peintre et " valet de chambre " , avec un salaire annuel fixe qui lui fut régulièrement attribué une rente annuelle jusqu' à sa mort .
Il est fixé à [MASK] , centre administratif du duché , avant le 2 août 1425 .
Ces données attestent que pendant les années 1426 et 1427 , [MASK] [MASK] a fait , au moins , deux voyages lointains dont un pèlerinage .
Il est possible que certains de ces paiements concernent aussi sa participation à une ambassade à [MASK] , en 1426 .
Un rapport de l' époque confirme que [MASK] [MASK] en faisait partie .
En 1434-1435 , le magistrat de la ville de [MASK] rétribue [MASK] [MASK] pour la polychromie de six statues et leur baldaquin à la façade de l' hôtel de ville .
En septembre 1434 , il se produisit un incident administratif : la chambre des comptes de [MASK] refusa de payer [MASK] [MASK] ; le 13 mars 1435 , le duc intervint personnellement en faveur de son peintre .
Il s' agit vraisemblablement d' une mission en terre non chrétienne , liée aux projets de croisade de [MASK] [MASK] [MASK] .
Il n' est pas à exclure que [MASK] [MASK] ait eu à faire des reconnaissances de voies et de territoires et à les porter sur une carte .
Rien pratiquement ne subsiste des commandes que [MASK] [MASK] exécuta pour [MASK] [MASK] [MASK] .
Il a travaillé à la décoration des résidences de [MASK] ( 1431 ou 1432 ) , de [MASK] ( 1433 ) , et de [MASK] ( 1434 ) .
Il n' est pas exclu que le duc ait commandé à [MASK] [MASK] des œuvres décoratives à caractère héraldique .
L' œuvre de [MASK] [MASK] [MASK] , en dehors de ce chef d' œuvre exceptionnel , est composée surtout de représentations de la [MASK] [MASK] et de portraits .
[MASK] [MASK] a ainsi été considéré comme le fondateur du portrait occidental .
Le miroir convexe au centre du tableau reflète deux personnages , peut-être les deux témoins , dont l' un serait [MASK] [MASK] lui-même .
On peut alors voir dans cette œuvre un véritable certificat de mariage , d' ailleurs signé par [MASK] [MASK] lui-même .
L' apport technique de [MASK] [MASK] à la peinture occidentale est capital .
Le liant utilisé par [MASK] [MASK] était à base d' huile siccative et d' un autre élément qui rendait le liant consistant , ce qui était l' une des difficultés rencontrés par les utilisateurs de la peinture à l' huile auparavant .