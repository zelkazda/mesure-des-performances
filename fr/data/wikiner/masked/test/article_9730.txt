La commune de [MASK] compte 130 201 habitants en septembre 2009 .
Elle est traversée par la rivière [MASK] et se situe à environ 30 km au nord des [MASK] .
Elle est inscrite au patrimoine culturel mondial de l' [MASK] , grâce à son patrimoine médiéval urbain qui a réussi à traverser les siècles .
Le paysage autour de [MASK] a été formé par des glaciers au cours de la dernière glaciation .
Un certain nombre de ponts ont été construit pour permettre à la ville de se développer au-delà de l' [MASK] .
[MASK] est construite sur un sol très inégal .
Il existe plusieurs dizaines de mètres de hauteur de différence entre les quartiers près de l' [MASK] et les plus élevés .
[MASK] a une superficie de 51.6 km ² .
Le duc [MASK] [MASK] [MASK] [MASK] a fondé la ville au bord de l' [MASK] en 1191 et l' aurait nommé d' après le nom de l' ours qu' il avait tué .
Elle a été occupée par les troupes françaises en 1798 au cours des [MASK] [MASK] [MASK] [MASK] [MASK] , lorsqu' elle fut dépouillée de la plupart de ses territoires .
En 1831 , la ville devint la capitale du [MASK] [MASK] [MASK] et en 1848 , elle est devenue la capitale suisse ( ou plus précisément " la ville fédérale " ) .
Entre 1885 et 1886 , [MASK] fut le lieu d' une conférence qui avait pour objectif de dresser un accord international sur les droits d' auteur .
Un certain nombre de congrès socialistes de la [MASK] [MASK] et de la [MASK] [MASK] ont eu lieu à [MASK] , en particulier durant la [MASK] [MASK] [MASK] , lorsque la [MASK] était neutre .
Les frontières de la ville se sont élargies à l' ouest jusqu' à la presqu'île formée par la rivière [MASK] .
[MASK] a une population de 122658 d' habitants ( en 2007 ) , dont 22,5 % sont de nationalité étrangère .
Durant l' élection de 2007 , le parti le plus populaire a été le [MASK] , qui a reçu 29,1 % des voix .
Les trois autres partis les plus populaires ont été le [MASK] [MASK] [MASK] ( 24,9 % ) , l' [MASK] ( 16,7 % ) et le [MASK] ( 15,7 % ) .
En 2005 , les représentants du [MASK] [MASK] [MASK] et ceux des partis [MASK] détiennent la majorité dans les deux conseils .
Les autres grands partis politiques de [MASK] sont le [MASK] [MASK] et l' [MASK] [MASK] [MASK] [MASK] .
[MASK] se trouve sur les lignes ferroviaires :