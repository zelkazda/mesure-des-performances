Professeur de philosophie morale à l' université de [MASK] , il consacre dix années de sa vie à ce texte qui inspire les grands économistes suivants , ceux que [MASK] [MASK] appellera les " classiques " et qui poseront les grands principes du libéralisme économique .
[MASK] [MASK] est né le 5 juin 1723 à [MASK] , petite ville de 1500 habitants en [MASK] .
Dès sa naissance , [MASK] [MASK] est orphelin de père .
À l' âge de quatre ans , [MASK] [MASK] est enlevé par des bohémiens , qui , prenant peur en voyant l' oncle du jeune garçon les poursuivre , l' abandonnent sur la route où il sera retrouvé , .
Élève particulièrement doué dès son enfance , bien que distrait , [MASK] [MASK] part étudier à [MASK] à l' âge de quatorze ans et y reste de 1737 à 1740 .
Il y reçoit , entre autres , l' enseignement de [MASK] [MASK] , le prédécesseur d' [MASK] [MASK] à la chaire de philosophie morale .
[MASK] sera très influencé par [MASK] , .
Il part ensuite étudier à l' [MASK] [MASK] [MASK] .
Cette sélection personnelle lui vaut d' ailleurs d' être menacé d' expulsion de l' université lorsqu' on découvre dans sa chambre le [MASK] [MASK] [MASK] [MASK] [MASK] du philosophe [MASK] [MASK] , lecture jugée inconvenante à l' époque .
Cette institution est bien plus sérieuse que celle d' [MASK] et le corps enseignant apprécie peu ce nouveau venu qui sourit pendant les services religieux et qui est de plus un ami déclaré de [MASK] [MASK] .
[MASK] affirme que l' individu partage les sentiments d' autrui par un mécanisme de sympathie .
[MASK] étend ce point de vue en évoquant un hypothétique spectateur impartial avec lequel nous serions en permanence en situation de sympathie .
On discute vite des thèses de ce livre un peu partout , et plus particulièrement en [MASK] .
[MASK] [MASK] , alors qu' il était professeur de logique , a écrit d' autres ouvrages qui ne seront publiés qu' après sa mort .
L' histoire de l' astronomie à proprement parler ne représente qu' une petite partie de l' ouvrage , et s' arrête à [MASK] , car en fait [MASK] s' intéresse davantage aux origines de la philosophie .
Selon [MASK] , l' esprit prend plaisir à découvrir les ressemblances entre les objets et les observations , et c' est par ce procédé qu' il parvient à combiner des idées et à les classifier .
[MASK] et son élève quittent la [MASK] pour la [MASK] en 1764 .
Ils restent dix-huit mois à [MASK] , ville dont la société lui semble ennuyeuse .
Après être passés par [MASK] , [MASK] et son élève arrivent à [MASK] .
C' est là qu' il rencontre l' économiste le plus important de l' époque , le médecin de [MASK] [MASK] [MASK] , [MASK] [MASK] .
[MASK] avait fondé une école de pensée économique , la physiocratie , en rupture avec les idées mercantilistes du temps .
[MASK] [MASK] est intéressé par les idées libérales des physiocrates , mais ne comprend pas le culte qu' ils vouent à l' agriculture .
Ayant vécu à [MASK] , il a conscience de l' importance économique de l' industrie .
En 1766 , le voyage de [MASK] et de son protégé s' achève , le frère de ce dernier ayant été assassiné dans les rues de [MASK] .
[MASK] rentre à [MASK] , puis à [MASK] où il se consacre à son traité d' économie politique .
Il ne se rend que rarement à [MASK] pour participer aux débats de son temps .
Il y rencontre [MASK] [MASK] dont l' influence lui fera dire que les colonies américaines sont une nation qui " deviendra très probablement la plus grande et la plus formidable qui soit jamais au monde " .
En 1778 , [MASK] devient commissaire aux douanes à [MASK] , ce qui lui assure une retraite confortable .
Le premier ministre [MASK] [MASK] [MASK] lui déclare même un jour : " Nous sommes tous vos élèves. "
Paradoxalement , [MASK] [MASK] n' a apporté presque aucune idée nouvelle à la philosophie et à l' économie dans son ouvrage .
La plupart de ces idées ont déjà été approchées par des philosophes et des économistes comme [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] ( avec qui il entretenait des relations amicales ) , [MASK] ou encore [MASK] [MASK] .
Ce qui donne toute sa valeur à l' œuvre de [MASK] n' est donc pas son originalité , mais la synthèse de la plupart des idées économiques pertinentes de son temps .
[MASK] corrige les erreurs a posteriori évidentes des auteurs qui l' ont précédé , il approfondit leurs idées et les liens entre elles pour tisser une compilation cohérente .
Son mode de pensée repose souvent sur le principe suivant : pour [MASK] ce qui est sage pour le chef de famille ne peut pas être une folie dans la gestion d' un empire .
[MASK] s' interroge sur l' origine de la capacité qu' ont les individus de porter des jugements moraux sur les autres mais aussi sur leur propre attitude .
[MASK] commence par affirmer , contre les théories de l' égoïsme et de l' intérêt , le caractère désintéressé de certains de nos jugements .
Pour [MASK] , l' homme dans ses actions doit tenir compte du point de vue des spectateurs réels ou du spectateur impartial , dans le cadre d' un double processus de sympathie .
Avant [MASK] , les économistes avaient proposé deux grandes définitions de la richesse .
Pour [MASK] , la richesse de la nation , c' est l' ensemble des produits qui agrémentent la vie de la nation tout entière , c' est-à-dire de toutes les classes et de toutes leurs consommations .
[MASK] [MASK] rejoint donc la vision de la monnaie proposée par [MASK] dans l' [MASK] .
Il pose ainsi les bases de la doctrine de la valeur travail , qui sera pleinement théorisée au siècle suivant par [MASK] [MASK] .
En tentant de répondre à cette question , [MASK] propose une analyse de la croissance économique .
[MASK] le marché n' est pas assez grand , le surplus de production permis par une division du travail toujours accrue ne trouvera pas acheteur .
[MASK] note qu' elle peut avoir des effets désastreux sur l' intellect des ouvriers qui sont abrutis par la répétition de gestes d' une simplicité toujours plus grande .
Les analystes de [MASK] ont longtemps débattu sur une éventuelle opposition entre les thèses exposés dans ces deux ouvrages .
Dans cette version , la " main invisible " serait une métaphore par laquelle [MASK] signifierait que les marchés sont autorégulateurs et conduiraient à l' harmonie sociale .
Selon [MASK] , les " lois " du marché , associées au caractère égoïste des agents économiques , conduiraient à un résultat inattendu : l' harmonie sociale .
[MASK] un producteur tente d' abuser de sa position et fait monter les prix , des dizaines de concurrents tout aussi avides de profit en profiteront pour conquérir le marché en vendant moins cher .
[MASK] [MASK] avancerait donc l' idée d' un marché " autorégulateur " que n' auraient pas eu les physiocrates .
[MASK] [MASK] [MASK] l' avait déjà fait remarquer dans sa [MASK] [MASK] [MASK] , où il expliquait comment les vices privés , c' est-à-dire la consommation de richesses , se révélaient être des vertus collectives , susceptibles de stimuler l' activité économique .
La conséquence en est que l' ordre physique , c' est-à-dire l' ordre de la richesse matérielle ne se confond pas avec l' ordre moral entendu chez [MASK] comme harmonie ou bonheur intérieur .
Toutefois , [MASK] s' inscrit dans la tradition de [MASK] où le monde reste au-delà de ce que la raison peut concevoir .
[MASK] donc chez [MASK] la notion de main invisible traduit bien l' existence de conséquences inattendues , celles-ci ne sont pas forcément favorables .
Grâce aux lois du marché , [MASK] décrit ensuite une dynamique économique qui doit conduire la société vers l' opulence .
Faisant la louange de l' épargne , qui n' est que la manifestation de la frugalité et du renoncement au bien-être immédiat afin que survive et prospère l' industrie , [MASK] voit dans l' accumulation du capital , c' est-à-dire l' investissement en machines , l' occasion de décupler la productivité et d' accroître la division du travail .
Pour [MASK] [MASK] , l' accumulation des machines implique une augmentation des besoins en main-d'œuvre , et donc une montée des salaires .
[MASK] parle ainsi d' un " salaire de subsistance " qui permet d' assurer la satisfaction des besoins physiologiques de l' être humain , ainsi que ceux de sa descendance , laquelle est nécessaire pour fournir la main-d'œuvre future .
La thèse de [MASK] sur le commerce international se fonde sur une évidence a priori : il est prudent " de ne jamais essayer de faire chez soi la chose qui coûtera moins à acheter qu' à faire . "
[MASK] reprend en fait une critique du mercantilisme entamée par [MASK] [MASK] en 1752 .
[MASK] pensait que les excédents commerciaux , en accroissant la quantité de monnaie sur le territoire , provoquaient une hausse des prix et donc une baisse de la productivité induisant un déficit commercial , de sorte que les balances commerciales s' ajustaient naturellement , et qu' il était inutile de poursuivre l' excédent .
La démonstration formelle des avantages du libre-échange est différente chez [MASK] .
[MASK] une première nation est meilleure dans la production d' un premier bien , tandis qu' une seconde est meilleure dans la production d' un second bien , alors chacune d' entre elles a intérêt à se spécialiser dans sa production de prédilection et à échanger les fruits de son travail .
L' analyse du droit public de [MASK] s' inscrit dans la logique de [MASK] , [MASK] et [MASK] , mais [MASK] [MASK] opère dans ses cours à [MASK] ( 1762-1763 ) une rupture nette dans sa définition des fonctions de la " police " , c' est-à-dire la protection et la régulation de l' ordre intérieur .
La police impliquerait donc l' intervention économique , ce à quoi s' oppose [MASK] dans ses cours à [MASK] en expliquant que l' intervention économique est contre-productive vu qu' elle nuit à l' opulence des denrées .
[MASK] [MASK] définit donc les devoirs régaliens dans leur sens moderne : la protection des libertés individuelles fondamentales contre les agressions du dedans et du dehors .
Selon [MASK] , le marché ne peut pas prendre en charge toutes les activités économiques , car certaines ne sont rentables pour aucune entreprise , et pourtant elles profitent largement à la société dans son ensemble .
Dans un article publié en 1927 , [MASK] [MASK] , professeur d' économie à l' [MASK] [MASK] [MASK] , écrivait ainsi qu ' " [MASK] [MASK] n' était pas un avocat doctrinaire du laisser-faire " , laissant beaucoup de place à l' intervention gouvernementale , tenant compte des circonstances pour décider si une politique libérale est bonne ou mauvaise .
" L' expérience de tous les temps et de toutes les nations , écrit [MASK] [MASK] , s' accorde , je crois , pour démontrer que l' ouvrage fait par des esclaves , quoiqu'il paraisse ne coûter que les frais de leur subsistance , est au bout du compte le plus cher de tous . "
[MASK] [MASK] n' épargne pas non plus l' aristocratie terrienne .
[MASK] [MASK] n' a pas fait naître le libéralisme économique .
Puis le physiocrate [MASK] [MASK] [MASK] avait demandé aux gouvernants de " laisser faire les hommes " et de " laisser passer les marchandises " , mais il ne s' agissait alors que de dénoncer le système des corporations et d' encourager la libre circulation des grains dans les provinces d' un unique royaume .
On considère néanmoins que c' est [MASK] [MASK] qui , en faisant de l' initiative privée et égoïste le moteur de l' économie et le ciment de la société , achève d' énoncer le dogme libéral .
Parmi eux se réclament de [MASK] des auteurs dont la célébrité deviendra presque aussi grande comme [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] en [MASK] , [MASK] [MASK] en [MASK] .
[MASK] [MASK] , lui-même admirateur d' [MASK] [MASK] , les qualifie de " classiques " , bien que ses propres travaux , fondés sur la méthode " scientifique " et rigoureuse des classiques , l' amènent à prôner une doctrine , le communisme , opposée au libéralisme .
En 1776 , [MASK] écrivait déjà :
À la fin du XIX e siècle , le sociologue américain [MASK] [MASK] critique les postulats économiques sur le comportement du consommateur .
[MASK] nombre d' économistes admirent [MASK] , c' est peut-être parce que nombre de courants peuvent y voir le père de leurs idées .
Bien qu' à l' opposé des idées politiques de [MASK] , [MASK] [MASK] lui-même s' en inspire en développant toute une doctrine fondée sur la théorie classique de la valeur .
Pour autant , l' œuvre de [MASK] n' est pas exempte d' imperfections et la science économique a su se placer en rupture avec certains de ses postulats .
La théorie de l' avantage absolu s' est révélé être un argument relativement faible en faveur du libre-échange , inférieur aux analyses de [MASK] [MASK] sur la balance des paiements qui l' avaient précédée , mais surtout à la théorie de l' avantage comparatif avancée par [MASK] [MASK] en 1817 dans [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Dans le monde de [MASK] , deux pays n' avaient avantage à échanger que lorsque chacun d' entre eux disposait d' un avantage sur l' autre dans une production donnée .
Ce sera donc la démonstration de [MASK] selon laquelle même le pays le moins compétitif du monde trouve intérêt au commerce international qui sera retenu comme argument principal du courant libre-échangiste .
De même la théorie de la " valeur travail " développée par [MASK] et adoptée par la plupart des classiques anglo-saxons et les marxistes , en opposition avec la conception subjective de [MASK] , des scolastiques et des classiques français , a été abandonnée par la science économique néoclassique à partir de la fin du XIX e siècle .
Or [MASK] avait écarté l' utilité comme facteur de valeur des produits au profit du travail nécessaire à leur production .
[MASK] [MASK] , économiste de l' école autrichienne d' économie , voit de façon plus dure dans l' importance supposée de [MASK] un " mythe " .
Il fait plutôt remonter l' origine de l' économie moderne à [MASK] [MASK] .
Dans la sphère politique et industrielle , les admirateurs de [MASK] sont nombreux .
En [MASK] , l' idée selon laquelle la recherche du profit individuel se fait au profit de la nation tout entière devient le dogme de la bourgeoisie capitaliste qui y trouve une justification .
De cette façon , les idées de [MASK] ont été profondément détournées .
Le concept de la main invisible qui devint si chère aux défenseurs de l' entrepreneuriat capitaliste ne s' appliquait qu' à l' économie essentiellement artisanale de l' époque d' [MASK] [MASK] , qui se méfiait lui-même des industriels et de leurs manigances visant à établir des ententes et des monopoles afin de s' affranchir des contraintes du marché et d' imposer leurs prix .
Quelle étendue peut-on prêter à l' influence d' [MASK] [MASK] sur le monde ?
-- [MASK] on en croit [MASK] , il ne semble donc pas exagéré de prétendre qu' [MASK] [MASK] et ses idées ont modelé le monde qui les a suivis .