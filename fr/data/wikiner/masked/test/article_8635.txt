[MASK] ou [MASK] [MASK] est une île du nord-est des [MASK] située à 250 kilomètres au nord de l' archipel de la [MASK] et 240 kilomètres à l' est de [MASK] [MASK] .
[MASK] [MASK] , lors de son retour vers [MASK] ( dit le deuxième voyage ) en passant au large avec sa flotte de 17 navires ( entre le 11 et le 13 novembre 1493 ) , jour de la fête de [MASK] [MASK] [MASK] [MASK] , l' aurait désignée sous ce nom .
Quelques familles françaises issues de la proche colonie françaises de l' île [MASK] cultivent du tabac sur la partie orientale de [MASK] .
Ils y laissèrent une petite garnison en liaison avec [MASK] [MASK] .
En 1644 ce fort résista à une attaque du célèbre capitaine néerlandais [MASK] [MASK] .
Par la suite des pirates y ont fait plusieurs raids destructeurs et les forces militaires britanniques occupèrent l' île de nombreuses fois en fonction des conflits et des alliances en [MASK] .
Les îles voisines les plus proches sont [MASK] et [MASK] , qui émergent du même plateau sous-marin situé à -40m en moyenne .
En revanche [MASK] , [MASK] , [MASK] et [MASK] sont plus éloignées bien que généralement visibles .
Le littoral de [MASK] est découpé en de nombreuses baies bordées d' une trentaine de plages de sable blanc mais certaines baies sont bordées uniquement de galets .
Il faut ajouter l' île [MASK] située à trois kilomètres de la côte
Alors qu' à [MASK] , les cordons de mangrove sont réduits à des reliques étroites , à [MASK] , ils bordent largement les rives des lagunes littorales et de quelques baies .
Donc il y a 12 000 ans et à plusieurs reprises auparavant l' île a été réunie en un seul bloc avec les îles sœurs [MASK] et [MASK] situées sur le même banc sous-marin actuellement à une profondeur moyenne de -40m à -60m .
On trouve des cristaux de grenats dans la ravine du hameau [MASK] .