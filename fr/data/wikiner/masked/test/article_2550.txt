, un éditeur de solutions de test et d' assurance qualité globale basé dans le [MASK] .
en août 1981 pour développer des produits pour le système d' exploitation [MASK] en utilisant une structure juridique " prête à l' emploi " .
Les fondateurs rencontrèrent alors [MASK] [MASK] , un jeune mathématicien français venant de s' établir dans le [MASK] [MASK] -- où il était un des principaux développeurs du [MASK] .
Le prospectus du premier appel public à l' épargne ( IPO ) de [MASK] décrit en détail les entreprises antérieures et leurs relations avec [MASK] .
Les trois danois avaient connu un succès initial estimable dans le marketing logiciel depuis l' [MASK] ( un paradis fiscal ) avant d' être confrontés à de nouveaux challenges au moment où ils rencontrèrent [MASK] [MASK] .
[MASK] développa une série d' outils de développement très bien accueillis par le marché comme son premier produit , [MASK] [MASK] , basé sur le compilateur développé par [MASK] [MASK] .
[MASK] [MASK] fut lancé le 18 mai 1987 et vendu à plus de 100000 copies le mois même de son lancement .
Ce litige conduit [MASK] à adopter une approche basée sur des standards ouverts -- contrairement à [MASK] qui opta pour des solutions propriétaires .
Sous la direction de [MASK] [MASK] , [MASK] prit une position de principe en annonçant son intention de se défendre contre [MASK] et de " se battre pour les droits des programmeurs " [ réf. nécessaire ] .
Dans le même temps , [MASK] était reconnue pour son approche pratique et créative du piratage logiciel et de la propriété intellectuelle , notamment à travers son concept de contrat de licence " exempt de non-sens " permettant aux développeurs/utilisateurs de se servir des produits [MASK] comme de livres et de faire de multiples copies du programme -- tant qu' une seule était en service à un moment donné .
En 1995 , sortie de [MASK] 1 , un outil de développement rapide d' applications .
L' architecte en chef en est [MASK] [MASK] , le créateur de [MASK] [MASK] .
Ce dernier sera plus tard débauché par [MASK] pour créer le langage [MASK] [MASK] et le framework [MASK] .
Il reste néanmoins au conseil d' administration de [MASK] .
La même année , l' environnement de développement intégré [MASK] [MASK] est lancé .
Le nom [MASK] est repris en janvier 2001 .
L' objectif est de sortir des produits pour [MASK] .
Elle sera abandonnée en juillet de la même année après la chute de l' action [MASK] .
En 2001 , sortie de [MASK] , version de [MASK] pour [MASK] .
En 2003 , sortie de [MASK] [MASK] [MASK] , environnement de développement en langage [MASK] [MASK] pour le framework [MASK] [MASK] , et de [MASK] , environnement de développement multiplateformes en [MASK] [MASK] destiné à remplacer [MASK] [MASK] , avec une bibliothèque d' interfaces utilisateur basée sur [MASK] .
En 2004 , sortie de [MASK] 8 pour [MASK] [MASK] .
En 2006 , en février , [MASK] annonce son intention de se séparer de l' activité des IDE et met donc en vente ses logiciels [MASK] , [MASK] [MASK] et [MASK] .
[MASK] est également connu pour son approche originale et pragmatique face au piratage informatique et à la propriété intellectuelle , lançant sa " [MASK] no-nonsense license agreement . "
[MASK] offre aussi l' intégralité du code source de plusieurs de ses produits , dont les éditeurs , tableurs , jeu d' échecs et moteurs de bases de données .