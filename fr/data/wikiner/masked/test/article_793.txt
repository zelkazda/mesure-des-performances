En pratique [MASK] [MASK] n' ont quasiment jamais conclu d' alliances qu' avec la gauche .
[MASK] [MASK] obtiennent leur meilleur résultat avec 10.6 % des voix .
Sur la profession de foi des candidats , la photo regroupe : [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] ...
Contrairement à ce qui est fréquemment affirmé , [MASK] [MASK] n' ont pas décidé de leur ancrage à gauche au sens où ils s' inscriraient dans cette famille politique , mais au sens où ils considéraient leur idéologie comme incompatible avec celle de la droite [ réf. souhaitée ] .
En raison de sa mise en minorité et des conflits internes assez durs qui suivent , [MASK] [MASK] quitte [MASK] [MASK] pour fonder le [MASK] [MASK] [MASK] en 1994 .
À [MASK] , le nouveau maire [MASK] [MASK] doit donner à ceux-ci , après de dures négociations dans l' entre deux tours , un nombre d' adjoints proportionnel à leurs résultats dans les urnes .
Il s' agit là du score le plus important réalisé par un candidat [MASK] au premier tour d' une élection présidentielle .
La victoire de la droite ainsi que le mode de scrutin ne leur laissent que 3 élus à l' [MASK] [MASK] ( contre 6 pour le précédent mandat ) .
Ils estiment qu' ils auraient dû sortir du gouvernement dès 2000 quand le contrat qu' ils avaient passé avec le [MASK] [MASK] avait cessé d' être respecté , et réaffirmer leur radicalité politique .
Le courant incarné par [MASK] [MASK] , qui avait en juin précédent évoqué la possibilité d' un grand parti unique de la gauche dans lequel se fondrait [MASK] [MASK] , est mis en minorité .
En janvier 2003 , [MASK] [MASK] devient secrétaire national , succédant à [MASK] [MASK] .
[MASK] [MASK] doivent donc régler eux-mêmes la plupart des factures , grevant ainsi lourdement leur budget .
[MASK] [MASK] font leur entrée dans la majorité d' une vingtaine de régions .
Le 16 janvier 2005 , le secrétaire national [MASK] [MASK] est remplacé par l' ancien porte-parole [MASK] [MASK] .
Au soir du 29 mai d' après les sondages la majorité des électeurs [MASK] ( 60 % ) s' est prononcée pour le " Non " .
Les deux candidats arrivés en tête étant donc : [MASK] [MASK] et [MASK] [MASK] ( tous les deux clairement partisans du " Oui " au référendum de 2005 ) .
Le second tour donne une quasi-égalité de voix entre eux , avec seulement deux voix d' écart ( en faveur de [MASK] [MASK] ) sur un total de 5181 bulletins .
Elle n' obtient cependant que 1,57 % des suffrages ( soit 576 666 voix ) , ce qui est le plus mauvais résultat de écologistes depuis la candidature de [MASK] [MASK] en 1974 .
Ainsi , dans un sondage paru dans [MASK] [MASK] [MASK] [MASK] , 48 % des sondés estimaient que la présidentielle 2007 avait signé l' arrêt de mort du parti .
Aux cantonales , [MASK] [MASK] obtiennent 11,54 % des voix au premier tour .
Aux municipales , [MASK] [MASK] déposent des listes autonomes dans un tiers des trente-neuf villes de plus de 100000 habitants et dans trente-deux villes de plus de 20000 habitants .
Dès le premier tour , [MASK] [MASK] est réélu à [MASK] ( [MASK] ) , tout comme [MASK] [MASK] à [MASK] avec 82 % des voix .
Au second tour à [MASK] ( [MASK] ) , [MASK] [MASK] l' emporte face au maire sortant apparenté [MASK] , [MASK] [MASK] .
Les têtes de listes d' [MASK] [MASK] sont annoncées en janvier 2009 .
Le 7 juin au soir , les listes [MASK] [MASK] obtiennent 16,28 % des voix au niveau national et 14 députés au parlement européen , autant que le [MASK] .
Le 8 juillet 2009 , la députée de [MASK] [MASK] [MASK] , ne se reconnaissant plus dans cette " évolution au centre que subissent [MASK] [MASK] avec [MASK] [MASK] " , annonce qu' elle quitte [MASK] [MASK] pour rejoindre le [MASK] [MASK] [MASK] .
[MASK] [MASK] défendent les idées de l' écologie politique , ce qui est souvent confondu avec la défense de l' environnement .
Cela va en fait bien au-delà : [MASK] [MASK] militent pour que la société s' achemine progressivement vers un mode de vie durable en résolvant par l' action politique ses déséquilibres sociaux et environnementaux .
[MASK] [MASK] considèrent qu' une transition vers un mode de vie soutenable , loin de ne reposer que sur un changement des comportements individuels , doit demander des efforts à tout le monde , et doit donc faire appel à une action politique forte et à des modifications législatives .
[MASK] [MASK] considèrent qu' un monde dans lequel subsistent de profondes inégalités sociales n' est pas durable .
[MASK] [MASK] pensent que l' économie non-marchande
D' un point de vue plus général , [MASK] [MASK] considèrent que les revenus d' une personne devraient représenter ce qu' elle apporte à la société en général et non uniquement ce qu' elle apporte à l' économie marchande comme c' est le cas actuellement .
[MASK] [MASK] sont considérés comme progressistes sur les questions de société .
[MASK] [MASK] critiquent en particulier :
D' un point de vue général , [MASK] [MASK] militent pour anticiper la transition ( qu' ils pensent de toute façon inéluctable ) de notre modèle de production énergétique vers le tout renouvelable .
Considérant que la production de l' énergie est presque toujours destructrice pour l' environnement , [MASK] [MASK] militent pour une société énergétiquement plus sobre .
[MASK] [MASK] sont opposés à une trop grande personnalisation de la politique , et sont dans l' ensemble favorable à un régime beaucoup plus parlementaire .
Peu représentés à l' [MASK] [MASK] notamment en raison du mode de scrutin , ils militent pour l' introduction de la proportionnelle pour les élections législatives .
En 2010 , [MASK] [MASK] sont représentés dans les différentes chambres parlementaires de la manière suivante :