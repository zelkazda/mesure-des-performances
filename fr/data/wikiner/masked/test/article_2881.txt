[MASK] [MASK] ( 聖闘士星矢 , [MASK] [MASK] ? ; en français [MASK] [MASK] [MASK] [MASK] ) est un manga de [MASK] [MASK] publié pour la première fois en 1986 .
Il compte 28 volumes et est publié en français aux éditions [MASK] .
La deuxième partie du chapitre [MASK] a été diffusée en deux volets jusqu' en 2007 et la dernière partie de ce chapitre fut diffusée en mars 2008 .
Plusieurs œuvres dérivées du manga original ont été publiées , notamment [MASK] [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
[MASK] présente la petite fille comme étant la réincarnation de la déesse [MASK] .
[MASK] a 88 chevaliers , chacun associé à une constellation stellaire .
Pourtant de force moindre à leurs adversaires , les interventions d' [MASK] leur permettent de remporter les victoires .
Mais [MASK] [MASK] succombe à une maladie .
Beaucoup d' entre eux sont corrompus par [MASK] et se retournent contre [MASK] , pensant qu' elle n' est pas la déesse [MASK] .
Fin avril 2006 , l' auteur de la série originelle , [MASK] [MASK] , a annoncé sur son blog le début de la prépublication d' une nouvelle série nommée [MASK] [MASK] [MASK] [MASK] [MASK] .
En [MASK] , ces épisodes ont été diffusés à la télévision , au [MASK] [MASK] sur [MASK] , entre la fin des années 1980 et le milieu des années 1990 .
Il a fallu attendre 13 ans avant que la dernière partie du manga , le chapitre [MASK] , ne soit adaptée en animation .
La diffusion a été assurée sur la chaine de télévision pay-per-view japonaise [MASK] .
La deuxième partie de la saga [MASK] , se déroulant dans le monde des ténèbres , est constituée de 6 OAV .
Ils ont été projetés au cinéma au [MASK] .
Liste de jeux spécialement conçus pour l' univers de [MASK] [MASK] .
Liste des jeux dans lesquels les personnages du manga [MASK] [MASK] font leur apparition .