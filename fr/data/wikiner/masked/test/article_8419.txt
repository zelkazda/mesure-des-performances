Pour certains [MASK] fait aussi partie de l' [MASK] .
[MASK] est à la merci des seigneurs de la région .
[MASK] séculiers
Liste des pasteurs réformés de [MASK]
Liste des diacres réformés de [MASK]
L' annexe de [MASK] dépend de la paroisse luthérienne de [MASK] ( consistoire et inspection de [MASK] ) .
En 1900 , la communauté luthérienne de [MASK] et celle de [MASK] se constitue en vicariat jusqu' en 1919 .
[MASK] redevient annexe de [MASK] jusqu' en 2006 .
Liste des pasteurs luthériens déservant [MASK]
Les juifs de [MASK] fréquentent la synagogue de [MASK] , érigée en 1869 et détruite en 1945 , rattachée au rabbinat de [MASK] jusqu' en 1910 puis au rabbinat de [MASK] .
Suite à une crûe du [MASK] en 1307 et à la destruction de la 2 e abbaye , le pape autorisa la construction de la 3e abbaye .
La chapelle funéraire des [MASK] date de la 1 re moitié du 16 e siècle .
En 1682 , [MASK] [MASK] donna [MASK] aux [MASK] .