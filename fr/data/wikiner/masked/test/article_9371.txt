[MASK] [MASK] est un musicien , théoricien et érudit allemand né et mort à [MASK] ( * 28 septembre 1681 -- † 17 avril 1764 ) .
Un de ses professeurs est l' organiste [MASK] [MASK] [MASK] .
En 1703 , il fait la connaissance de [MASK] , et commence une amitié qui durera toute leur vie .
Ensemble , ils font le voyage à [MASK] pour y entendre et rencontrer le célèbre organiste [MASK] [MASK] , espérant peut-être obtenir sa succession .
Il épousa une anglaise et c' est probablement par son entremise que [MASK] noua des contacts décisifs dans ce pays .
En 1715 il devient directeur de la musique de la cathédrale de [MASK] , poste qu' il assure jusqu' en 1728 où il est atteint de surdité .
Un grand nombre de ses écrits semblaient avoir été perdus durant la [MASK] [MASK] [MASK] .
En 1998 , ils ont été retrouvés en [MASK] .