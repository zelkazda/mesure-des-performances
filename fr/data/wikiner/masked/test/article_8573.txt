Celui-ci retrouve une certaine autonomie à partir du règne de [MASK] [MASK] en tant qu' apanage .
Le [MASK] [MASK] [MASK] est alors centré sur le pagus d' [MASK] .
Le domaine englobe alors [MASK] et dépasse le [MASK] , avec [MASK] .
Sous le règne de [MASK] [MASK] ( 987-1040 ) , le comté ne tarde pas à assujettir les régions voisines .
Il mena également quelques expéditions contre [MASK] , [MASK] , et [MASK] entre autres .
Au printemps 992 , il assiégea [MASK] qu' il prit en trois semaines , apparemment par trahison .
Les deux comtes décident de livrer bataille sur la lande de [MASK] , sur laquelle s' était déjà affronté les deux armées en 981 ( voir [MASK] [MASK] [MASK] [MASK] ) .
En mauvaise posture , il pense alors à se rendre quand intervient l' armée royale mené par [MASK] [MASK] .
Le mois suivant , [MASK] décède .
Cependant , la mort d' [MASK] [MASK] en octobre 996 renverse les alliances .
Les partisans d' [MASK] [MASK] [MASK] [MASK] profitent de cette absence , et attaquent le [MASK] [MASK] [MASK] .
[MASK] , en désespoir de cause , tenta alors de reprendre [MASK] .
La saison des vendanges approchant , [MASK] dût se retirer , et licencier son armée .
[MASK] tenta de prendre sa revanche en 1027 en attaquant [MASK] par surprise , mais échoua et dû se replier sur [MASK] .
Ce qui provoque une lutte d' influence avec les [MASK] [MASK] [MASK] .
Cet essor est arrêté par la mort du comte [MASK] [MASK] [MASK] .
Le comté est partagé entre ses deux neveux : [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] .
En 1067 , [MASK] [MASK] [MASK] parvient finalement s' imposer comme comte mais son pouvoir s' étend sur un territoire restreint par rapport à son oncle [MASK] [MASK] [MASK] .
Devenu comte , [MASK] conquiert le [MASK] [MASK] [MASK] pour le compte de sa femme .
En 1204 , le [MASK] [MASK] [MASK] entre donc dans le domaine royal français ainsi que le [MASK] [MASK] [MASK] et le [MASK] [MASK] [MASK] .
Par le [MASK] [MASK] [MASK] ( 1259 ) , son fils [MASK] [MASK] renonce à ses prétentions sur la [MASK] , l' [MASK] , la [MASK] et le [MASK] .
Le [MASK] [MASK] [MASK] est érigé en comté-pairie en 1297 , puis en duché en 1360 .