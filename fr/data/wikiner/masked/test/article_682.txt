Dans cette partie du [MASK] [MASK] [MASK] , d' après la situation géographique des [MASK] [MASK] , il n' y a qu' une vingtaine de kilomètres tout au plus entre deux terres .
Ces chasseurs n' ont jamais atteint la côte sud de l' [MASK] et les [MASK] [MASK] .
Ils se sont plutôt répandus rapidement dans le [MASK] arctique et le [MASK] à la poursuite de bœufs musqués ou mammifères marins .
En plus , de minuscules lames triangulaires servant de pointes de projectile constituaient très probablement le premier indice de l' usage de l' arc et de la flèche en [MASK] [MASK] [MASK] .
De plus , les langues inuites ont d' importantes affinités avec celle des [MASK] , ce qui laisse croire qu' elles ont possiblement une même origine .
Nous savons que ces derniers étaient des chasseurs des forêts nordiques de la [MASK] qui se sont adaptés aux régions de toundra et de banquise .
C' était la première phase d' extension territoriale d' une bonne partie de l' [MASK] [MASK] et du [MASK] , encore inhabité à cette époque .
En revanche , sur l' [MASK] [MASK] , on a plutôt trouvé de bonnes quantités d' os de phoque , de morse et d' ours polaire .
Dans la région d' [MASK] , un site daté au radiocarbone nous indique qu' il est vieux de 3900 ans .
Le territoire s' étend du district de [MASK] au nord jusqu' au district de [MASK] au sud .
Il semblerait qu' un nombre assez important d' individus ont occupé cette riche région côtière du [MASK] .
Ils passaient l' été sur les côtes de la [MASK] [MASK] [MASK] et durant les autres saisons , à l' intérieur des terres à la recherche de caribou et de poissons anadromes .
La technologie microlithique a sûrement pris racine dans la tradition paléolithique de l' [MASK] et plus sûrement dans la culture paléosibérienne .
Le terme " paléoesquimau moyen " est utilisé comme expression générique regroupant plusieurs cultures régionales s' étendant de l' [MASK] [MASK] à [MASK] et du delta du [MASK] [MASK] jusqu' au [MASK] .
Sur la [MASK] [MASK] [MASK] , le bœuf musqué était le principal mammifère terrestre disponible , le caribou en était totalement absent .
Nous avons trouvé aussi des sites d' occupations autour de la [MASK] [MASK] [MASK] , au [MASK] et à [MASK] .
De son côté , les [MASK] [MASK] ont connu un développement graduel qui a débouché sur la culture des [MASK] d' aujourd'hui .
La côte pacifique de l' [MASK] , quant à elle , a connu une évolution technologique basée sur l' ardoise polie qui a pu être à l' origine des cultures esquimaudes de cette région .
Dès cette époque , de nouveaux villages permanents voient le jour le long des côtes de la [MASK] [MASK] [MASK] .
Dès ce moment , il y eut un important accroissement démographique dans la partie septentrionale de l' [MASK] .
Cette culture porte ce nom parce que c' est sur la côte nord-ouest du [MASK] , près de la communauté de [MASK] que l' on a identifié pour la première fois de vieilles maisons de type thuléenne .
Comme nous l' avons énoncé précédemment , la baleine boréale de l' [MASK] ( ouest ) et celle du [MASK] ( est ) étaient la ressource principale de ces populations .
Dans la région d' [MASK] , ils firent aussi la découverte de nombreux troupeaux de morses .
De plus , ces gens construisaient un autre type d' habitations hivernales complètement inconnu en [MASK] : l' igloo .
Des harnais pour chiens apparaissent dans les sites thuléens du [MASK] à cette époque .
Pour ce qui est de la [MASK] [MASK] [MASK] et du [MASK] [MASK] , les gens de l' endroit avaient un mode de vie semblable aux gens du nord de l' [MASK] .
Les groupes de la région ont dû mourir de faim ou sont partis rejoindre leurs parents sur la côte nord-ouest du [MASK] .
Une analyse isotopique du sang prélevé dans le cordon ombilical des bébés inuits , au moment de la naissance , montre que le plomb qui les contaminent ( 8 fois plus de bébés inuits étaient atteints de saturnisme à la naissance qu' au sud du [MASK] ) provient des cartouches de chasse , et non de retombées atmosphérique ou des poissons ou phoques , autres hypothèses avancées .
[MASK] refuse sur le champ , il a aussi des problèmes financiers .
Le 2 avril 1935 , le drame se déplace alors en [MASK] [MASK] .
Tout à coup , le [MASK] du [MASK] et ses habitants devenaient intéressants pour le gouvernement provincial .
Les élus provinciaux de l' époque découvraient les énormes potentiels de développement hydroélectrique à la [MASK] [MASK] et dans le nord québécois .
Certains résidents de la [MASK] [MASK] [MASK] ont même menacé de déménager sur les [MASK] [MASK] .
Mais le [MASK] ne voulait plus que ces services soient assurés par des agents fédéraux .
Elle a conclu que les programmes gouvernementaux de réinstallation au [MASK] avaient été considérés par le gouvernement comme une fin en soi et non comme un élément d' un processus de développement .
On peut lire dans un rapport de 1943 , que les réinstallés entretenaient toujours la folle idée de retourner à [MASK] [MASK] .
Pour cet auteur anonyme , la solution serait de les déplacer tous , dans deux ou trois villes du sud du [MASK] .
On pensa en effet , à l' implantation d' un village inuit à [MASK] ( [MASK] ) , un autre à [MASK] ( [MASK] ) et un dernier près d' [MASK] ( [MASK] ) .
Encore pour des raisons non avouées de souveraineté sur l' archipel arctique , le gouvernement du [MASK] préparait l' une des plus tragiques histoires des régions nordiques .
Trois jours plus tard , 3 familles ( 16 personnes ) de [MASK] [MASK] les rejoignent sur le pont d' acier du navire gouvernemental .
Elle est transcrite en caractères latins pour les [MASK] [MASK] [MASK] et du [MASK] .
Pour le [MASK] et le [MASK] la transcription se fait en caractères syllabiques .
Au [MASK] , l' inuktitut n' est pas reconnu en tant que langue officielle mais est toutefois reconnu dans l' administration .
Le 18 décembre 1971 , les autochtones de l' [MASK] saluaient le début d' une nouvelle ère .
La population inuite du [MASK] est composée d' environ 40 000 personnes .