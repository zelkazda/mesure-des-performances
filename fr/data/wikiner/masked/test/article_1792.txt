Grâce au soutien français , le conclave qui s' ouvre à la mort de [MASK] [MASK] ( 1774 ) l' élit à la dignité pontificale .
Il choisit le nom de [MASK] [MASK] en hommage à [MASK] [MASK] , pape de l' application du [MASK] [MASK] [MASK] et de la [MASK] [MASK] [MASK] .
Quelques temps plus tard , [MASK] [MASK] doit affronter les événements de la [MASK] [MASK] :
La réaction de [MASK] [MASK] par rapport à la constitution civile du clergé n' est , aujourd'hui encore , pas élucidée .
Cette question fait l' objet de discussions de la part des historiens des religions ; il existe des archives sur ce sujet en [MASK] .
[MASK] [MASK] annexe [MASK] et le [MASK] [MASK] .
A la nouvelle de l' assassinat du général [MASK] , le [MASK] ordonne le 11 janvier 1798 l' occupation de [MASK] .
[MASK] [MASK] part le 6 février pour [MASK] .
Le pape [MASK] [MASK] est contraint par la république française de renoncer à son pouvoir temporel et de se contenter de son pouvoir spirituel .
On l' oblige à quitter [MASK] sous deux jours .
Il est successivement emmené à [MASK] , [MASK] , [MASK] , puis [MASK] , [MASK] et enfin [MASK] ( [MASK] ) .
Malgré les bouleversements que connaissait alors [MASK] [MASK] , le pape octogénaire reçut de nombreuses et touchantes marques de respect , de compassion et de communion dans la foi de la part du peuple , tout au long de sa route , entre [MASK] et [MASK] .
Le poète [MASK] [MASK] le surnommera le père commun des fidèles .
C' est à [MASK] qu' il fut incarcéré par la [MASK] [MASK] , et qu' il mourut , épuisé , le 29 août 1799 à l' âge de 82 ans .
Son successeur fut [MASK] [MASK] .
[MASK] [MASK] , d' abord enseveli civilement à [MASK] , est enterré dans la basilique [MASK] [MASK] [MASK] , son corps y ayant été ramené en triomphe le 17 février 1802 .
Le [MASK] [MASK] [MASK] les rétablit dès 1814 .