Elle est située à mi-distance entre [MASK] et la [MASK] [MASK] [MASK] .
[MASK] fait partie de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] avec 9 autres communes .
On dit qu' elle fait partie du [MASK] [MASK] [MASK] .
Dans cet enclos également se trouvent les monuments aux morts des deux guerres et deux canons du vaisseau [MASK] [MASK] [MASK] [MASK] [MASK] .
En fait dans le [MASK] , seules deux églises paroissiales et une douzaine de chapelles furent placées sur ce vocable .
Le choix de [MASK] s' est fait notamment pour sa position reculée et pour le fort taux de luxation congénitale de la hanche dans le [MASK] sud , dont la cause supposée à l' époque est la consanguinité .
À la suite de tous ces travaux et enquêtes se sont déroulées diverses manifestations ; par exemple [MASK] a reçu une délégation en mars 2005 de chercheurs japonais , puis en septembre 2005 l' université de [MASK] a organisé un colloque consacré à [MASK] .