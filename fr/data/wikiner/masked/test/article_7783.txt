Le règne de [MASK] fut troublé par des guerres incessantes sur toutes les frontières ; malgré ses très bonnes dispositions à gouverner , il ne put que provisoirement prévenir la désintégration du grand empire de [MASK] .
Juste après son accession au trône le 14 août 582 , il réussit à interférer dans les guerres de succession perses et occupa l' [MASK] .
Il aida à placer sur le trône perse le plus grand de ses rois , [MASK] [MASK] , qui allait être le farouche adversaire du futur basileus [MASK] .
Mais au même moment , ses provinces des [MASK] étaient ravagées par les [MASK] : elles ne purent jamais vraiment s' en remettre .
Il entretenait des rapports difficiles avec le pape [MASK] [MASK] [MASK] , qui lui reprochait son césaropapisme .
Les [MASK] pénétrèrent jusqu' au [MASK] et de nombreuses campagnes brillantes , quoique coûteuses , durent être dirigées contre eux .
[MASK] succéda à [MASK] [MASK] [MASK] qui en quatre années de règne avait diminué les impôts de 25 % et par prodigalité avait mis l' empire dans une situation économique difficile .
[MASK] se montra , lui , parcimonieux à l' excès .
Pour des raisons économiques , il ordonna à ses armées de camper en territoire hostile par delà le [MASK] .
Elles se choisirent comme représentant [MASK] , un centurion , qui aspirait au pouvoir suprême ( 23 novembre 602 ) .
Les corps furent jetés dans le [MASK] .
[MASK] laissait un empire incomparablement renforcé .
Sa mort et l' extinction de sa dynastie provoqua l' invasion perse et la mutinerie d' une partie de l' armée contre le tyrannique [MASK] .