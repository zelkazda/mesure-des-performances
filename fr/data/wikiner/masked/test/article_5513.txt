Commune située à moins de 8 km de [MASK] et à 35 km au sud est de [MASK] .
En 1911 , la commune a été rebaptisée [MASK] .
Mais le village fut peu de temps après libéré par les armées britanniques et canadiennes dans le cadre de l' [MASK] [MASK] .
Le 20 novembre 1992 , l' école primaire de [MASK] a été baptisée du nom du scénariste de bandes dessinées [MASK] [MASK] , créateur d' [MASK] , du [MASK] [MASK] et d' [MASK] .
Lors de l' inauguration , de nombreuses personnalités étaient présentes : [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .