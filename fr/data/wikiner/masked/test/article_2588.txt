[MASK] est un manga en 4 volumes de [MASK] , publié intégralement en français aux éditions [MASK] .
[MASK] a édité les 4 volumes de la série vers la fin des années 90 , et hormis le deuxième tome en 2005 , les autres tomes n' ont pas bénéficié d' une réédition .
De plus , le quatrième et dernier tome est assez rare , du fait qu' il ait été tiré à un nombre moindre d' exemplaires , et il est assez recherché par les fans du groupe [MASK] .
Actuellement , la série est en réédition chez [MASK] .