Le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ou [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] -- ce qui signifie aussi " forme " en anglais ) est le centre de commandement militaire des forces de l' [MASK] en [MASK] .
Situé au 13 [MASK] [MASK] à [MASK] puis à [MASK] à partir de 1952 , le siège politique de l' [MASK] [MASK] occupe initialement des locaux temporaires au [MASK] [MASK] [MASK] .
Il quitte la capitale française pour s' installer [MASK] [MASK] [MASK] à [MASK] en décembre 1966 .
Le 2 avril 1951 , le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] commence à fonctionner .
À la suite du retrait de la [MASK] du commandement militaire de l' [MASK] , il a été déplacé en [MASK] , sur le territoire des anciennes communes de [MASK] , [MASK] et de [MASK] où le nouveau [MASK] a été inauguré le 31 mars 1967 .
Depuis la fusion des communes , tout le territoire du [MASK] fait partie de l' entité de [MASK] située dans la [MASK] [MASK] [MASK] en [MASK] .
Il y a été décidé la construction d' un nouveau siège pour le quartier général qui devrait être inauguré en 2014 sur le site de l' ancien aéroport d' [MASK] pour un budget total estimé à 650 millions d' euros .
Il possède des prérogatives propres de " coordinateur stratégique " avec l' [MASK] [MASK] .