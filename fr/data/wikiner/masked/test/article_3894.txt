L' iode a été découvert en 1811 par le chimiste et fabricant de salpètre [MASK] [MASK] dans des cendres d' algues marines .
Il a été nommé ainsi par [MASK] [MASK] du grec iodes ( violet ) , en raison de sa couleur .
L' iode 129 était présent sur [MASK] à sa formation ( dû à sa demi-vie relativement longue , de 15,7 millions d' années ) mais est aujourd'hui éteint .
Bien qu' il ait une faible durée de demi-vie , l' iode 131 émis lors des accidents nucléaires ( dont celui de [MASK] ) pose problème en raison du fait que la thyroïde fixe une grande partie de l' iode absorbé via l' alimentation , l' eau ou l' inhalation .
Dans les [MASK] , la population isolée des vallées était beaucoup plus souvent atteinte de désordres liés à la carence en iode .
L' expression " crétin des [MASK] " est usuelle .