[MASK] est un système d' exploitation libre de type [MASK] , dérivé de [MASK] .
Créé en 1994 par [MASK] [MASK] [MASK] , il est issu de la séparation avec [MASK] , le plus ancien des trois autres principaux systèmes d' exploitation de [MASK] [MASK] [MASK] [MASK] aujourd'hui en activité .
Le projet [MASK] est réputé pour son intransigeance sur la liberté du logiciel et du code source , la qualité de sa documentation , et l' importance accordée à la sécurité et la cryptographie intégrée .
[MASK] inclut un certain nombre de mesures de sécurité absentes ou optionnelles dans d' autres systèmes d' exploitation .
Le projet suit des politiques strictes sur les licences et préfère la licence open source [MASK] et ses variantes : dans le passé , ceci a conduit à un audit exhaustif des licences et des remplacements , voire des suppressions de codes sous licences considérées comme moins acceptables .
À l' instar de la plupart des systèmes d' exploitation basés sur [MASK] , le noyau d' [MASK] et ses programmes utilisateurs , tels que le shell et les outils habituels comme cat et ps , sont développés dans un seul et même dépôt [MASK] .
Le projet est coordonné par [MASK] [MASK] [MASK] de sa maison à [MASK] , [MASK] , [MASK] , et la mascotte du projet est [MASK] , un diodon .
[MASK] [MASK] [MASK] était le cofondateur de [MASK] , et membre de l' équipe de développement .
En 1994 , l' équipe lui demanda de démissionner et son accès au [MASK] fut révoqué .
Il lui était notamment reproché d' être désagréable avec les utilisateurs en employant un ton agressif sur les listes de discussion de [MASK] .
Bien que sa personnalité laisse rarement indifférent , la plupart des commentateurs s' accordent à reconnaître en [MASK] [MASK] [MASK] un programmeur talentueux et un gourou de la sécurité .
Sa spontanéité , que certains apprécient , s' est illustrée dans ce conflit avec l' équipe de [MASK] , dont la plupart des membres gardent encore aujourd'hui le silence sur cet épisode .
À l' inverse , [MASK] [MASK] [MASK] répondit à toutes les questions qui lui furent posées , et mit à disposition les courriels échangés et les logs des salons de discussion avec l' équipe de [MASK] .
[MASK] [MASK] [MASK] ne fut pas exclu d' emblée du projet [MASK] .
Le dépôt [MASK] de ce projet nécessite différents niveaux de droits en lecture et en écriture .
L' équipe de [MASK] lui retira alors la possibilité d' envoyer directement des modifications dans le dépôt , et de rendre ces changements permanents .
Après la création d' [MASK] , chaque projet essaya d' attirer à lui des développeurs .
Bien qu' il refusa formellement d' incriminer des membres de l' équipe de [MASK] , [MASK] [MASK] [MASK] décida d' examiner la sécurité de [MASK] , qu' il jugea déficiente .
Cette coopération permit également au projet de se concentrer sur un point précis : les développeurs [MASK] devraient essayer de faire ce qui est correct , propre et sécurisé , même au détriment de la facilité d' utilisation , de la vitesse ou des fonctionnalités .
Les failles d' [MASK] devenant plus difficilement détectables et exploitables , l' entreprise de sécurité statua que l' audit de code était devenu trop difficile et peu rentable .
Ce fut la première vulnérabilité découverte dans l' installation par défaut d' [MASK] qui permet à un attaquant d' accéder à distance au compte superutilisateur .
L' usage répandu d' [MASK] à ce moment était à l' origine de la gravité de la faille , qui affectait un nombre considérable d' autres systèmes d' exploitation .
Ce problème nécessita l' ajustement du slogan du site web d' [MASK] :
Cette affirmation a été critiquée du fait du peu de logiciels activés dans l' installation par défaut d' [MASK] , et du fait également que des failles distantes avaient été découvertes après la publication d' une version .
Une des idées fondamentales sous-jacentes à [MASK] est de concevoir un système simple , propre et sécurisé par défaut .
[MASK] inclut un grand nombre de fonctionnalités spécifiques destinées à améliorer la sécurité , notamment :
La séparation des privilèges est une technique , pionnière sur [MASK] et inspirée du principe du moindre privilège , dans lequel un programme est divisé en deux ou plusieurs parties , dont l' une effectue les opérations privilégiées , et l' autre -- presque toujours le reste du code -- fonctionne sans privilège .
Les développeurs ont appliqué ces fonctionnalités aux versions [MASK] des applications communes , notamment [MASK] et le serveur web [MASK] , lequel n' est qu' une version 1.3 lourdement modifiée en raison de problèmes de licences avec la série [MASK] 2 .
À l' instar de [MASK] [MASK] , [MASK] est l' un des deux systèmes d' exploitation libres dont la politique est de rechercher du code [MASK] au format classique pre- [MASK] , et de le convertir en son équivalent moderne [MASK] .
Le nom [MASK] vient de l' aventure [MASK] de [MASK] [MASK] [MASK] .
En effet le [MASK] de [MASK] n' était pas accessible aux non-développeurs officiels ; seules les releases étaient diffusées .
Pour son fork , [MASK] [MASK] [MASK] a mis en place un serveur [MASK] public : tout le monde peut accéder aux dernières sources du projet .
Comme les autres [MASK] ( [MASK] et [MASK] ) , [MASK] a pour mascotte le [MASK] [MASK] ( alias [MASK] ) .
Il a aussi comme mascotte propre le [MASK] , alias [MASK] .
Chaque version d' [MASK] est accompagnée d' un morceau de musique ainsi que d' une planche de bande dessinée mettant généralement en scène [MASK] .
[MASK] est actuellement en version 4.7 ( depuis le 19 mai 2010 ) .
D' après un message déposé sur undeadly.org , le projet [MASK] aurait subi des pertes financières de l' ordre de 20 000 USD par an en 2004 et en 2005 .
L' équipe d' [MASK] travaille également sur d' autres projets qui deviennent des pièces à part entière de l' OS et qui sont également portés ( ou portables ) sur d' autres systèmes d' exploitation .