Apparu tardivement , [MASK] est un dieu guerrier qui lutte aux côtés de [MASK] contre le serpent [MASK] .
Fils de [MASK] , la déesse chatte , on le représente sous les traits d' un lion sauvage .
Dieu de [MASK] , on élevait dans son temple un lion vivant , symbole du dieu , qui était enterré dans une nécropole toute proche .
[MASK] est parfois assimilé au dieu [MASK] .