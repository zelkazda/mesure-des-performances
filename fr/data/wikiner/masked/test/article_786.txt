Les contacts oraux ont en effet été très limités durant la période qui s' étend de la parution du premier manuel de la [MASK] [MASK] , le 26 juillet 1887 , jusqu' au premier congrès universel d' espéranto qui se tint en 1905 à [MASK] en présence de 688 participants de vingt pays .
C' est justement à cette occasion que la grammaire fondamentale , ou [MASK] [MASK] [MASK] , fut adoptée .
À cela se sont ajoutées des anthologies de la littérature polonaise , slovaque , bulgare , estonienne , danoise , belge , suisse , chinoise , australienne , française ( 3 volumes ) , ainsi que des textes philosophiques , religieux ( [MASK] , [MASK] ) , ou politiques ( Le petit livre rouge de [MASK] [MASK] ) , etc .
Le travail accompli par [MASK] [MASK] ( essais , grammaire complète , lexicographie , traductions ) est prodigieux .
Elle parut de 1906 jusqu' à la [MASK] [MASK] [MASK] .
[MASK] fut d' ailleurs le premier poète espérantiste , puisqu' il s' exerça à cette forme d' expression à l' aide d' une langue encore embryonnaire et rudimentaire , qu' il lui fallait éprouver avant de la proposer à l' humanité .
Preuve que la poésie a été le genre majeur en espéranto , c' est [MASK] [MASK] , poète écossais qui fut proposé de 1999 à 2006 comme candidat au [MASK] [MASK] [MASK] [MASK] par l' association des écrivains espérantophones .
Depuis 2007 , c' est un autre poète , islandais cette fois , qui est proposé : [MASK] [MASK] .
Mais le roman en espéranto le plus important de l' entre-deux guerres est sans doute celui de [MASK] [MASK] , un auteur déjà connu pour ses poésies .
Hormis quelques magazines mensuels tels que [MASK] , on trouve aussi :
Depuis 2007 [MASK] a recréé sa coopérative d' édition ( [MASK] eldona fako kooperativa ) et tente une diversification de ses éditions .