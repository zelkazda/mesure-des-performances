[MASK] est la fille cadette du roi [MASK] [MASK] [MASK] [MASK] et de la reine [MASK] [MASK] [MASK] [MASK] [MASK] .
Son mariage a pour but de sceller l' alliance diplomatique entre le [MASK] [MASK] [MASK] récemment unifié et celui d' [MASK] , où la [MASK] [MASK] vient de s' emparer du trône .
Alors que son bâteau était prêt pour partir en [MASK] , une tempête fait rage sur les côtes espagnoles , repoussant ainsi le départ de 5 jours .
Lors de son arrivée en [MASK] , elle avait la consigne de garder son visage voilé en public ce qui perturba le roi [MASK] [MASK] [MASK] [MASK] quand vint le moment de la rencontrer : la jeune fille était couchée et ne pouvait recevoir le roi mais celui çi insista pour la voir .
On réveilla la jeune [MASK] pour qu' elle se montre à son futur beau-père mais elle le fit le visage voilé ce qui contribua à irriter le roi qui eut peur de s' être fait dupé .
Lorsque [MASK] [MASK] [MASK] souleva le voile , [MASK] [MASK] et son fils [MASK] parurent enchanté par la beauté et la grâce de la princesse espagnole .
Le mariage est célébré à la [MASK] [MASK] [MASK] [MASK] le 14 novembre 1501 .
[MASK] meurt ( peut-être de la suette ) le 2 avril 1502 , quelques mois seulement après le mariage , tandis que [MASK] recouve la santé .
[MASK] a quinze ans au moment de son décès , il est prétendu par la suite que le mariage n' a pas été consommé , ce que corrobore le témoignage de [MASK] elle-même .
Devenue veuve , [MASK] reste en [MASK] .
Le motif de cette prolongation de séjour est purement politique et financier : [MASK] [MASK] n' avait nulle intention de restituer la grosse dot de la jeune femme .
Un nouveau mariage est arrangé avec [MASK] , le frère cadet de son premier mari .
Pour d' obscures raisons , on sollicite du pape [MASK] [MASK] une dispense de ce constat de virginité .
Par une bulle de décembre 1503 , le pape , compréhensif , déclare [MASK] et [MASK] déliés du " lien d' affinité " .
Le remariage , désormais possible , est célébré le 11 juin 1509 par l' archevêque [MASK] [MASK] , après le décès d' [MASK] [MASK] ( comme il en avait fait la demande ) .
[MASK] [MASK] [MASK] est désormais l' épouse du nouveau roi [MASK] [MASK] et ipso facto , reine .
En 1519 , il a un fils de sa maîtresse , [MASK] [MASK] , à qui il donne le nom révélateur d' [MASK] [MASK] .
C' est vraisemblablement vers 1525 ou 1526 qu' [MASK] [MASK] tombe éperdument amoureux d' une des suivantes de la reine , une certaine [MASK] [MASK] .
En 1527 le roi toujours privé d' héritier mâle légitime , entame l' interminable procédure en vue d' obtenir l' annulation de son mariage avec [MASK] [MASK] [MASK] .
Le roi épouse secrètement sa maîtresse , [MASK] [MASK] vers le 25 janvier 1533 .
[MASK] est confinée au château de [MASK] ( [MASK] ) , où elle meurt le 7 janvier 1536 abandonnée de tous .
[MASK] [MASK] encourage ses sujets à manifester de la joie à la nouvelle de sa mort .
La reine [MASK] [MASK] , qui l' a supplantée , ne lui survit que de quatre mois .
Sa fille est également reine sous le nom d' [MASK] [MASK] [MASK] et succède directement à la fille de [MASK] , [MASK] .