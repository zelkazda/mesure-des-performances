Il nait à [MASK] , à une cinquantaine de km au nord-est de [MASK] , dernier d' une famille nombreuse et pauvre .
À 17 ans , il part pour [MASK] où il va exercer divers petits boulots .
Aux élections municipales de 1959 , il est élu conseiller puis devient en 1965 adjoint au maire du [MASK] [MASK] [MASK] [MASK] .
En 1968 , il est élu sénateur et siège à la [MASK] [MASK] [MASK] [MASK] qui vient d' être créée .
En 1971 , il est nommé adjoint aux affaires économiques à la mairie de [MASK] .
Il est battu aux élections municipales françaises de 1989 par [MASK] [MASK] .