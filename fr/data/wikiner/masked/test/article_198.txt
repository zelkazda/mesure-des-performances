Le [MASK] est une monarchie , dirigée par le roi [MASK] [MASK] [MASK] [MASK] .
En 2002 , les femmes de [MASK] purent pour la première fois exercer leur droit de vote .
L' économie de [MASK] est fortement tributaire du pétrole qui représente 60 % des exportations du pays , 70 % des revenus du gouvernement et 30 % du PIB .
La santé économique du pays fluctue avec la variation du prix de l' or noir : ainsi , pendant et après la crise du [MASK] [MASK] de 1990 à 1991 .
Avec des équipements de communication et de transport fortement développés , [MASK] accueille de nombreuses multinationales ayant des activités dans la région .
Grâce à une politique fiscale intéressante et à ses infrastructures , [MASK] , comme [MASK] aux [MASK] [MASK] [MASK] , attire de nombreuses personnalités politiques , sportives ou artistiques .
On peut citer comme exemple le chanteur [MASK] [MASK] qui avait fait du royaume sa nouvelle résidence principale en 2005 .
[MASK] est l' un des pays les plus actifs dans le domaine de l' art contemporain .
[MASK] a pour codes :