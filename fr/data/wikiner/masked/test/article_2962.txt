[MASK] [MASK] , homme politique français , né le 2 septembre 1960 à [MASK] ( [MASK] ) .
Il est membre du [MASK] [MASK] , dans la section du [MASK] [MASK] [MASK] [MASK] .
Diplômé de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , proche de [MASK] [MASK] , il est élu député dans la [MASK] [MASK] [MASK] [MASK] en 1997 , et a rejoint [MASK] [MASK] en 2001 .
Il se démarque ainsi de [MASK] [MASK] , fidèle supporter de [MASK] [MASK] .
Candidat à sa succession aux élections législatives des 10 et 17 juin 2007 , il obtient 37,7 % des voix contre 29,6 % à [MASK] [MASK] au premier tour .
Il est finalement réélu le 17 juin 2007 devant [MASK] [MASK] avec 63,29 % des voix .
Elle propose de " consacrer les quatre milliards d' euros que coûterait chaque [MASK] [MASK] [MASK] aux économies d' énergie et aux énergies renouvelables " , ainsi qu' une modification de la politique de transports ainsi que d' aménagement du territoire afin de réduire les temps et les coûts énergétiques des transports .