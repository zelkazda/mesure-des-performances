[MASK] [MASK] est un écrivain , dramaturge et cinéaste français , né le 28 février 1895 à [MASK] ( [MASK] ) , mort le 18 avril 1974 à [MASK] .
[MASK] [MASK] devient célèbre avec [MASK] , pièce représentée au théâtre en mars 1929 .
Il fonde à [MASK] en 1934 sa propre société de production et ses studios de cinéma , et réalise de nombreux films avec les grands acteurs du moment ( en particulier [MASK] , [MASK] , [MASK] [MASK] ou [MASK] [MASK] ) : [MASK] ( 1934 ) , [MASK] ( 1937 ) , [MASK] [MASK] [MASK] [MASK] ( 1938 ) ...
Élu à l' [MASK] [MASK] en 1946 , il abandonne le cinéma et le théâtre , et commence la rédaction de ses [MASK] [MASK] [MASK] avec notamment [MASK] [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] .
[MASK] et célèbre , [MASK] [MASK] meurt à [MASK] en 1974 , à l' âge de 79 ans .
En 1897 , le jeune ménage s' établit dans le logement de fonction de l' école de [MASK] .
Il a pour condisciple [MASK] [MASK] avec lequel il se lie d' amitié .
Elle sera inhumée au cimetière marseillais de [MASK] , puis à [MASK] [MASK] .
En 1913 , à 18 ans , il obtient son baccalauréat de philosophie avec mention assez bien , et commence ses études de lettres à l' université d' [MASK] .
Puis , la [MASK] [MASK] [MASK] éclatant , il est mobilisé au 163 e régiment d' infanterie de [MASK] en même temps que son ami [MASK] [MASK] [MASK] , puis réformé en janvier 1915 pour faiblesse de constitution .
En novembre de la même année , il obtient sa licence des lettres et littératures vivantes ( [MASK] ) .
Et c' est ainsi que le 9 mars 1929 , [MASK] , pièce en quatre actes et six tableaux , ouvre au [MASK] [MASK] [MASK] avec [MASK] dans le rôle de [MASK] .
De leur union naîtra [MASK] [MASK] en 1930 , qui deviendra son assistant après la guerre , puis caméraman pour [MASK] [MASK] [MASK] .
[MASK] est né en 1895 , l' année où , à quelques kilomètres de là , [MASK] [MASK] [MASK] [MASK] tournent [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , premier film du cinématographe projeté publiquement .
Il contacte aussitôt les studios [MASK] [MASK] pour faire adapter sa pièce [MASK] .
Les producteurs décident d' en confier la réalisation à un metteur en scène anglais confirmé : [MASK] [MASK] .
Sorti le 10 octobre 1931 , [MASK] est l' un des premiers films à succès du cinéma parlant français .
[MASK] [MASK] [MASK] , réalisée par [MASK] [MASK] , sort le 2 novembre 1932 .
Le 28 juillet 1932 , son frère [MASK] , " le dernier chevrier des collines d' [MASK] " , qu' il allait souvent visiter dans les collines de leur enfance , meurt à l' âge de 34 ans .
La même année , [MASK] [MASK] fonde à [MASK] sa propre société de production et ses studios de cinéma .
Son premier film en tant que réalisateur est [MASK] ( 1933 ) , suivi d' [MASK] en 1934 , [MASK] et [MASK] en 1935 , [MASK] en 1936 , [MASK] en 1937 , [MASK] [MASK] [MASK] [MASK] en 1938 , etc .
Il fait jouer les plus grands acteurs français de l' époque [MASK] [MASK] , [MASK] , [MASK] [MASK] , [MASK] , amis avec lesquels il joue à la pétanque entre deux scènes .
C' est en visitant son domaine huit jours plus tard , qu' il reconnait " l' affreux château , celui de la peur de ma mère " ( [MASK] [MASK] [MASK] [MASK] [MASK] ) .
Mais la [MASK] [MASK] [MASK] fait rage ; [MASK] doit interrompre ses tournages et vendre ses studios à la [MASK] , tout en restant directeur de production .
Son dernier film tourné pendant la guerre [MASK] [MASK] [MASK] [MASK] reste inachevé et , pour garder la maitrise de son œuvre , [MASK] détruit la pellicule du film .
En 1944 , [MASK] est élu président de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , ainsi que du comité d' épuration , où siégeait une soixantaine d' intellectuels comme [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , etc .
En 1945 , il épouse l' actrice [MASK] [MASK] , rencontrée en août 1938 , qui sera jusqu' à sa mort son " brin de poésie et de tendresse " .
Il y remplace [MASK] [MASK] au 25 e fauteuil .
Il est reçu le 27 mars 1947 par [MASK] [MASK] à ce fauteuil qu' occupa jadis [MASK] [MASK] .
En 1955 , à 60 ans , [MASK] [MASK] préside le 8 e [MASK] [MASK] [MASK] [MASK] [MASK] .
L' éclairage nouveau , voire d' avant-garde , du personnage , tant il se rapproche de l ' [MASK] [MASK] [MASK] , est mal perçu par l' ensemble des confessions .
À partir de 1957 , il commence la rédaction romancée de ses [MASK] [MASK] [MASK] avec [MASK] [MASK] [MASK] [MASK] [MASK] , qui connaîtra un immense succès , dû entre autres à la façon dont [MASK] décrit les personnes qui lui sont chères dans le petit monde provençal qui l' entoure , et à la vivacité de ses souvenirs , embellis par le temps et l' imagination .
La suite en sera donnée la même année dans [MASK] [MASK] [MASK] [MASK] [MASK] , puis dans [MASK] [MASK] [MASK] [MASK] en 1960 et [MASK] [MASK] [MASK] [MASK] qui , inachevé , ne sera publié qu' en 1977 , trois ans après sa mort .
[MASK] a dit : " Si j' avais été peintre , je n' aurais fait que des portraits " .
[MASK] [MASK] meurt le 18 avril 1974 , à l' âge de 79 ans , au square de l' [MASK] [MASK] [MASK] à [MASK] .
[MASK] [MASK] est le metteur en scène des films suivants :
[MASK] [MASK] est l' auteur des scénarios et dialogues des films suivants :