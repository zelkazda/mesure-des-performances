Le livre est composé de nouvelles parues dans des magazines entre 1945 et 1950 ainsi que de nouveaux textes écrits spécialement pour le recueil , dont l' action se déroule sur la planète [MASK] .
La première édition du livre est dédiée à la femme de l' auteur , [MASK] .
Depuis lors , le livre est édité chez [MASK] dans la collection " [MASK] SF " .
[MASK] [MASK] conte l' histoire des premiers colons terriens vers la [MASK] [MASK] .
À cause d' une guerre qui se déclare sur leur planète d' origine , les hommes repartent et abandonnent [MASK] , excepté une poignée d' entre eux .
Le décollage d' une fusée à destination de [MASK] provoque une éphémère canicule au milieu de l' hiver .
Une nuit d' été sur [MASK] .
Des [MASK] se mettent à chanter des chansons ou jouer des airs qui leur sont inconnus mais qui leur viennent spontanément à l' esprit .
Derrière les grillages qui protègent la piste de lancement de la prochaine fusée à destination de [MASK] , un homme interpelle les gardes et insiste pour faire partie de l' expédition .
La troisième expédition , composée de dix-sept hommes , se pose sur [MASK] .
La quatrième expédition a établi un campement sur [MASK] .
Les scientifiques ont découvert que tous les [MASK] ont succombé à une épidémie de varicelle ( apportée par les précédentes expéditions ) .
Des prêtres arrivent sur [MASK] .
Il apprend que parmi les rares [MASK] survivants , il en existe se présentant sous une forme désincarnée : des sphères de lumière bleue , qui auraient sauvé la vie d' un colon en difficulté dans les collines ...
Première publication dans [MASK] [MASK] , juillet 1947 .
Dans les ruines , ils trouvent des ossements de [MASK] avec lesquels ils s' amusent à produire de la " musique " .
Première publication dans [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , novembre 1952 .
Le lendemain , elles doivent prendre une fusée pour [MASK] , afin d' y rejoindre leurs hommes ...
Les colons nomment les villes , les montagnes , les vallées , les cours d' eau de [MASK] en référence à leur culture , effaçant celle des anciens [MASK] .
On peut clairement relever dans cette nouvelle des idées qui seront développées dans le roman [MASK] [MASK] .
Il reçoit la visite d' un des tout derniers [MASK] encore en vie .
Le couple voyant approcher d' autres [MASK] , craignant pour sa sécurité , décide de prendre la fuite ...
Première publication dans [MASK] [MASK] , mai 1945 .
Les villes de [MASK] sont à l' abandon .
Il a promis de leur montrer des [MASK] ...
Il n ' y a pas de personnage principal dans les [MASK] [MASK] .
Ce livre a fait l' objet , en 1980 , d' une adaptation télévisuelle du même nom sous le forme d' un feuilleton en trois parties , réalisé par [MASK] [MASK] sur un scénario de [MASK] [MASK] , avec notamment [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] .
[MASK] [MASK] a lui-même adapté son récit pour le théâtre .
En [MASK] , [MASK] [MASK] signe une adaptation théatrale avec [MASK] [MASK] en 1966 .
Le journal américain [MASK] [MASK] [MASK] a publié dans ses pages une bande dessinée inspirée des [MASK] [MASK] en 1975 , .