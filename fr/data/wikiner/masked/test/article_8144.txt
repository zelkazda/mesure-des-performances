Ils jouèrent un rôle considérable dans les événements de la fin de l' [MASK] [MASK] .
La signification du nom [MASK] est sujette à controverses .
Le texte de [MASK] a été perdu .
Selon [MASK] , la terminologie se réfère à la région d' habitat des [MASK] , les [MASK] étant les [MASK] du " pays de l' ouest " et les [MASK] étant les " [MASK] de l' est " .
Or , on sait par ailleurs que d' autres noms existaient auparavant pour désigner une division géographique des [MASK] : les tervingi , c' est-à-dire les " gens de la forêt " , et les greutingi , c' est-à-dire les " gens de la grève " .
Aussi , l' explication géographique de l' origine des noms [MASK] et [MASK] donnée par [MASK] fut mise en question et l' on chercha une explication d' ordre étymologique , plus à même d' expliquer une différence antérieure au III e siècle .
Dans ce cas , [MASK] signifierait " [MASK] brillants " ( racine germanique ostr -- ) et [MASK] , " [MASK] sages " ( racine wise -- ) .
Le choix des historiens se porte alors sur l' une ou sur l' autre de ces hypothèses selon la confiance qu' ils accordent au témoignage de [MASK] .
Les deux tribus , en tous cas , partageaient de nombreux traits culturels et ne formaient , toujours selon [MASK] , qu' une seule " nation " .
Entre autres , les [MASK] reconnaissaient une divinité tutélaire commune que les [MASK] associaient à [MASK] .
Les [MASK] , un autre peuple germanique , devinrent leurs vassaux et leurs rivaux .
Les [MASK] soumirent les [MASK] vers 370 , et leur arrivée incita probablement les [MASK] à s' installer au-delà du [MASK] .
Selon [MASK] , la défaite face aux [MASK] provoqua également le suicide du roi ostrogoth [MASK] en 378 .
Au cours des décennies suivantes , les [MASK] demeurèrent dans les [MASK] sous la domination des [MASK] , devenant un de leurs nombreux peuples vassaux .
Les [MASK] se soulevèrent plusieurs fois contre les [MASK] .
Ces soulèvements furent matés , mais l' apport de la " culture à dos de cheval " des [MASK] constitua par la suite un avantage majeur pour les [MASK] .
Leur histoire écrite commence avec leur indépendance de l' empire des [MASK] après la mort d' [MASK] .
Le plus grand de tous les souverains ostrogothiques , [MASK] [MASK] [MASK] , naquit vers 455 , peu après la [MASK] [MASK] [MASK] .
Il passa son enfance en tant qu' otage à [MASK] , où il reçut une instruction soignée .
C' est dans ces deux rôles à la fois qu' en 488 il conquit l' [MASK] à la demande de l' empereur byzantin [MASK] , trop heureux de se débarrasser d' un chef de guerre entreprenant dont les troupes venaient de ravager les faubourgs de [MASK] quelque temps auparavant .
En 493 , [MASK] fut prise et [MASK] fut tué des mains même de [MASK] .
Lors de cette reconquête , [MASK] et [MASK] commencèrent également à se réunir , du moins si l' on en croit le témoignage d' un auteur qui écrit que [MASK] était aidé par des auxiliaires wisigoths .
Alors que la puissance de [MASK] s' étendait en pratique sur une grande partie de la [MASK] , elle s' installa sur presque la totalité de l' [MASK] : en effet , les événements contraignirent [MASK] à devenir le régent du royaume wisigoth de [MASK] .
Le roi [MASK] assuma à cette occasion son rôle de tuteur à l' égard de son petit-fils [MASK] et se réserva la totalité du domaine hispanique ainsi qu' un fragment de la [MASK] .
[MASK] passa aux [MASK] mais les [MASK] gardèrent la cité de [MASK] et la [MASK] : cette dernière région était la dernière partie de la [MASK] qui fut tenue par les [MASK] et elle garda le nom de [MASK] [MASK] [MASK] pendant encore longtemps .
Tant que [MASK] vécut , le [MASK] [MASK] demeura pratiquement indissociable de ses propres possessions .
Son avènement déclenche une guerre importante contre l' [MASK] [MASK] en [MASK] qui tente sous [MASK] de restaurer l' ordre impérial perdu .