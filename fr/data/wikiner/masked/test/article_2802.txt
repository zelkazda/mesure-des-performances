[MASK] était le quatrième fils de [MASK] et le troisième des quatre que ce dernier eut avec [MASK] .
À la mort de son père , en 511 , le royaume est divisé en quatre : l' aîné , [MASK] en a la moitié et les trois fils de [MASK] se partagent la moitié restante .
[MASK] devient roi de [MASK] .
Son royaume comprenait les cités de la future [MASK] avec le [MASK] et le [MASK] au sud de la [MASK] .
Il se rendit alors en [MASK] [MASK] , où le roi wisigoth tenta de s' enfuir par bateaux mais s' étant rendu compte qu' il avait oublié des trésors , il partit les chercher .
[MASK] récupéra les trésors d' [MASK] et ramena sa sœur mais elle mourut en cours de route .
Elle fut ensevelie auprès de [MASK] [MASK] er .
Mais [MASK] lui fit remarquer qu' il était à l' initiative de l' entreprise .
Le dernier , [MASK] resta en vie parce que ce dernier parvint à s' enfuir .
Mieux connu sous le nom de [MASK] [MASK] , ce dernier fonda un monastère à [MASK] ( aujourd'hui [MASK] ) et en devint le premier abbé , ayant préféré renoncer à sa chevelure , symbole de la royauté franque , plutôt qu' à la vie .
En accord avec ses frères , il continue la guerre contre le nouveau roi de [MASK] , [MASK] [MASK] , roi des [MASK] , frère de [MASK] , tué le 1 er mai 524 , sur ordre de [MASK] .
En 532 , il assiège [MASK] , la prend , enferme à jamais [MASK] [MASK] , qui s' était porté successeur de [MASK] , avec l' aide de son parent et allié , le roi des [MASK] [MASK] [MASK] [MASK] .
Il profita de la mort de [MASK] pour tenter de récupérer , avec [MASK] , son royaume .
Puis au cours de l' hiver 536 -- 537 , ils récupèrent ( ou plus précisément achètent ) la [MASK] .
[MASK] pria [MASK] qui fit se déchainer un orage sur les agresseurs , qui furent obliger d' abandonner leur projet .
Mais il rapporte de cette expédition l' étole de [MASK] [MASK] , en l' honneur de qui il fit bâtir une église , intégrée par la suite dans l' [MASK] [MASK] [MASK] .
L' évêque [MASK] officiait au maître-autel entouré de six autres évêques et la dépouille de [MASK] fut inhumée dans le caveau qui l' attendait et qu' il avait lui-même désigné .