Enfin , en plus de l' image proprement dite , ces fichiers transportent des informations sur les conditions de prises de vue ( en-tête [MASK] ) , qui peuvent être lues totalement ou partiellement par de nombreux logiciels .
Le transfert des données peut être fait par un câble ( [MASK] en général ) , par extraction et lecture de la carte mémoire sur un ordinateur .
Certains appareils peuvent transférer les images par un système sans fil ( [MASK] ou [MASK] ) .
Parallèlement , le marché professionnel utilise aussi des dos numériques de 11 ( pour [MASK] ) à 16 voire 50 millions de pixels ; tous ces capteurs sont conçus pour se placer au dos de l' appareil photo ( d' où leur nom de dos numérique ) .
L' informatique à domicile , le [MASK] [MASK] [MASK] , le courrier électronique et la facilité de partager ses photos avec ses proches ont probablement beaucoup contribué à cet engouement .
Un intérêt essentiel de la photographie numérique réside dans l' existence de logiciels de retouche tels que [MASK] ou [MASK] [MASK] qui permettent d' améliorer une photographie jugée imparfaite beaucoup plus facilement qu' à l' agrandisseur .
Par exemple , une image codée sur 8 bits ( c' est le cas d' une image compressée en [MASK] ) produit 256 niveaux de gris ( 2 8 ) .
Le format [MASK] , en plus d' offrir des possibilités d' étalonnage très sophistiquées , permet de conserver la résolution du capteur .
Cette résolution est couramment de 12 ou 14 bits par composante pour les reflex actuels ( 14 bits pour un [MASK] [MASK] [MASK] et [MASK] ) .