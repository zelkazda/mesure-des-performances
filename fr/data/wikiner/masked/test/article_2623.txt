[MASK] [MASK] ne portait que peu d' intérêt aux mangas durant son enfance .
[MASK] [MASK] poussait souvent ses élèves à créer des personnages intéressants et bien pensés , et son influence aura un fort impact sur le travail de [MASK] [MASK] tout au long de sa carrière .
La carrière professionnelle de [MASK] [MASK] commença en 1978 .
Plus tard la même année , elle commença sa première série , [MASK] [MASK] .
En 1980 , [MASK] [MASK] a trouvé son créneau et commence à publier régulièrement .
Elle commence alors sa deuxième série-phare , [MASK] [MASK] , dans le magazine [MASK] [MASK] [MASK] .
Elle réussit à poursuivre les deux séries simultanément , et les termina en 1987 , [MASK] [MASK] se concluant au volume 34 , et [MASK] [MASK] au volume 15 .
Pendant les années 1980 , [MASK] [MASK] devint prolifique en mangas d' histoires courtes .
[MASK] [MASK] est une autre série publiée de façon erratique puis laissée en plan .
En 2006 , [MASK] [MASK] a dessiné cinq nouveaux chapitres pour conclure cette série .
En 1987 , [MASK] [MASK] commença sa troisième série d' envergure , [MASK] [MASK] , suivant la mode de la fin des années 1980 / début des années 1990 des mangas shōnen d' arts martiaux .
[MASK] [MASK] est l' un des mangas de [MASK] [MASK] les plus populaires en occident .
Durant la deuxième moitié des années 1990 , [MASK] [MASK] a continué ses histoires courtes et a commencé sa quatrième œuvre majeure , [MASK] .
[MASK] est maintenant terminé après 558 chapitres et 56 volumes , ce qui en fait son plus long manga à ce jour .