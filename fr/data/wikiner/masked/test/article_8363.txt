En 2010 , il est classé par le [MASK] comme le dirigeant le plus influent au monde .
Son surnom officialisé sert à le désigner de façon abrégée , le patronyme ( da ) [MASK] étant extrêmement courant .
C' est traditionnellement ce dernier qui se transmet en tant que nom de famille , mais les enfants du président brésilien portent désormais le patronyme [MASK] [MASK] [MASK] , en dehors de sa première fille , née d' une relation hors mariage .
Il est né dans le village de [MASK] dans l' [MASK] [MASK] [MASK] .
Tous les enfants sont mis à contribution , et le petit [MASK] quitte l' école à 10 ans pour des petits boulots dans la rue ( cireur de chaussures , vendeur de cacahuètes ) .
Dans les années 1960 , le [MASK] connaît un boom économique , mais qui ne profite pas à la classe ouvrière .
[MASK] se syndicalise et se radicalise ; ses talents de tribun et de négociateur sont remarqués .
En 1980 , il décide de passer du syndicalisme à la politique et fonde le [MASK] [MASK] [MASK] ( [MASK] [MASK] [MASK] ) , d' inspiration trotskiste , à une époque où le général [MASK] [MASK] préparait lentement le pays au retour de la démocratie .
Le vice-président [MASK] [MASK] le remplace à la présidence et il impose un programme d' austérité économique et introduit une nouvelle monnaie pour tenter de contenir une inflation galopante qui se montera à 1000 % en 1989 .
En 1992 , se déroule une procédure de destitution du président [MASK] [MASK] [MASK] [MASK] , accusé de corruption financière .
En 1994 , le [MASK] met en œuvre un nouveau plan d' austérité économique pour pallier les conséquences de la récession mondiale .
[MASK] se présente une deuxième fois à la présidence .
En 1998 , [MASK] se présente une troisième fois à la présidence , mais il est battu dès le premier tour , à cause de la popularité du gouvernement sortant , de la bonne économie du [MASK] et peut être de son discours politique , ressenti comme trop radical .
Premier président brésilien de gauche , chantre de la démocratie participative expérimentée à [MASK] [MASK] , il ne remet pas en question la rigueur budgétaire des années précédentes , mise en œuvre par [MASK] [MASK] [MASK] et accepte le code de conduite du [MASK] .
Bénéficiant d' une large popularité , [MASK] a d' abord agi pour continuer la politique exigée par le [MASK] , tout en militant avec l' [MASK] , pour son assouplissement .
Il a choisi comme ministre de l' économie , [MASK] [MASK] , ancien trotskiste converti à l' économie du marché , pour renforcer la crédibilité du [MASK] et attirer les investissements étrangers qui avaient baissé de 22 milliards de dollars [ réf. nécessaire ] par peur de [MASK] .
Beaucoup estimaient fin 2003 que [MASK] devait commencer à assumer ses promesses électorales .
La reprise économique qui se confirme au [MASK] à la fin du premier semestre 2004 , avec l' augmentation de la production industrielle , la baisse du chômage et la prévision de croissance de 3,5 % pour 2004 , la popularité de [MASK] et de son gouvernement augmente ( 58,1 % d' opinion favorable pour [MASK] [ réf. nécessaire ] ) .
En outre , le [MASK] est affecté par la fièvre aphteuse bovine , conduisant à l' interdiction dans plusieurs pays de l' importation de viande brésilienne .
Selon les sondages , l' action du président [MASK] aurait été appréciée pour son combat contre la faim et la pauvreté mais n' aurait pas brillée dans la prévention de la violence et de la corruption .
L' affaire impliquait également le ministre de la justice , [MASK] [MASK] [MASK] .