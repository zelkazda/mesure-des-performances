Le [MASK] [MASK] [MASK] est une organisation créée le 18 décembre 1986 .
Le [MASK] [MASK] [MASK] compte 21 pays membres .
Le groupe est une instance régionale informelle , son siège est à [MASK] au [MASK] .
Ces deux groupes vont finalement fusionnés en 1986 pour donner naissance au [MASK] [MASK] [MASK] .
Au fil des années le [MASK] [MASK] [MASK] s' est élargit .
En 1990 [MASK] [MASK] , le [MASK] , l' [MASK] et le [MASK] intègrent le groupe .
Puis en 2000 c' est au tour du [MASK] [MASK] , du [MASK] , le [MASK] , l' [MASK] , le [MASK] et la [MASK] [MASK] .
En 2005 le [MASK] intègre à son tour le [MASK] [MASK] [MASK] suivit de près par la [MASK] et [MASK] en mars 2008 et enfin [MASK] en novembre 2008 .
Une des missions majeure du [MASK] [MASK] [MASK] est d' améliorer les relations entre ses pays membres .
Le [MASK] [MASK] [MASK] joue aussi un rôle au niveau international .
Les relations sont pérennes depuis la création du [MASK] [MASK] [MASK] .
On peut mettre en avant deux grandes limites au fonctionnement du [MASK] [MASK] [MASK] .
L' autre grande limite est le paradoxe qu' il existe entre les résolutions prises par le [MASK] [MASK] [MASK] et le comportement des différents pays membres .
Néanmoins , le [MASK] [MASK] [MASK] a de nombreux points négatifs .