Le mot tsar désigne un souverain de [MASK] ( de 1547 à 1917 ) , de [MASK] ( de 893 à 1422 ) , et de [MASK] ( de 1346 à 1371 ) .
Le terme de " tsar " était en effet officieusement utilisé depuis que le [MASK] [MASK] [MASK] [MASK] [MASK] avait épousé la princesse byzantine [MASK] [MASK] le 12 novembre 1472 en la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Tous ses successeurs adoptent ce nouveau titre , mais le terme de " tsar " , correct mais non-officiel , demeure cependant le plus usité , tant en [MASK] que dans le reste du monde .
il donna lieu à plusieurs prétendants au trône dont le " [MASK] [MASK] " .
Les moines du [MASK] [MASK] ont accepté son couronnement pour cette raison .