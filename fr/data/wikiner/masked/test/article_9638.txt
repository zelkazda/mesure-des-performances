L' [MASK] [MASK] [MASK] est une grande et célèbre avenue de [MASK] .
Elle s' étend sur 1910 mètres , d' est en ouest , sur terrain plat dans la moitié vers la [MASK] [MASK] [MASK] [MASK] , et en montée dans celle vers la [MASK] [MASK] .
Dans sa partie inférieure , l' avenue forme les jardins des [MASK] , longs de 700 mètres et larges de 300 à 400 mètres .
À l' exception du dernier , chacun de ces carrés comporte , depuis les aménagements effectués sous la direction de l' architecte [MASK] [MASK] en 1840 -- 1847 , une fontaine .
L' avenue a inspiré la création du [MASK] [MASK] [MASK] [MASK] à [MASK] ( [MASK] ) en 1860 , de la [MASK] [MASK] [MASK] [MASK] à [MASK] [MASK] et de la [MASK] [MASK] [MASK] à [MASK] ( [MASK] ) en 1917 .
En 1616 , [MASK] [MASK] [MASK] décide d' y faire aménager , le long de la [MASK] , une longue allée bordée d' arbres : le [MASK] [MASK] [MASK] .
Ce pont permet de prolonger l' avenue jusqu' à ce que l' on appelait alors l' étoile de [MASK] -- correspondant à l' ensemble du tracé actuel .
En 1765 , il permit la construction de bâtiments de part et d' autre de l' [MASK] [MASK] [MASK] .
Les promeneurs préfèrent diriger leurs pas le long du [MASK] [MASK] [MASK] , qui suit le tracé de la [MASK] et où l' on peut jouer aux quilles , à la paume ou aux barres .
La popularité des [MASK] , qui prennent alors leur dénomination définitive d' [MASK] [MASK] [MASK] ( 1789 ) , ne décolle véritablement que sous la [MASK] [MASK] .
Sous la [MASK] , la [MASK] [MASK] [MASK] [MASK] est le théâtre des exécutions capitales .
Au bas de l' avenue , [MASK] fait placer , sur des socles dessinés par le peintre [MASK] , les groupes de chevaux en marbre exécutés par [MASK] [MASK] pour l' abreuvoir du [MASK] [MASK] [MASK] .
Le [MASK] fait élargir l' avenue centrale , fermer quelques bouges et combler les caves et souterrains où se réfugiaient les malfaiteurs pour échapper à la police .
Le quartier reste cependant peu sûr : en 1804 , le soir du sacre de [MASK] [MASK] [MASK] , [MASK] [MASK] , inventeur du gaz d' éclairage , est assassiné dans un fourré des jardins des [MASK] .
[MASK] [MASK] le fait remettre en état et ouvrir l' [MASK] [MASK] .
À partir de 1834 , l' architecte [MASK] [MASK] est chargé de réaménager les jardins des [MASK] , parallèlement à son intervention sur la [MASK] [MASK] [MASK] [MASK] .
Elle est l' œuvre du sculpteur [MASK] .
L' ingénieur [MASK] [MASK] , sous [MASK] [MASK] , est à son tour chargé de l' aménagement des jardins .
Sa disparition permet de relier l' [MASK] [MASK] [MASK] au [MASK] [MASK] [MASK] [MASK] par le [MASK] [MASK] .
L' [MASK] [MASK] [MASK] est la première au monde à recevoir un revêtement en bitume , en 1938 .
L' arrivée du [MASK] [MASK] modifie la donne : de nombreux parisiens et franciliens de toutes conditions pouvant accéder facilement aux [MASK] , les boutiques d' enseignes plus populaires vont alors se multiplier .
Le trottoir nord -- côté pair -- est le côté ensoleillé mais aussi celui qui connaît la plus forte fréquentation en partie du fait qu' il se situe dans le prolongement de la sortie [MASK] .
Pour beaucoup d' enseignes , une installation sur les " [MASK] " , même si elle est très coûteuse , présente un double intérêt : la publicité par l' emplacement , mais aussi de fortes ventes de par la fréquentation touristique .
et l' activité commerciale ( horaires d' ouverture des magasins , qui par dérogation sont beaucoup plus étendus qu' ailleurs à [MASK] et en [MASK] ) .
Le président qui a redynamisé le comité et l' avenue a été [MASK] [MASK] [MASK] [MASK] qui a collaboré à la modernisation de l' avenue voulue par [MASK] [MASK] .
Mais à ce jour , seules des mesures politiques incitées par le militantisme associatif semble pouvoir , à court terme , sauver la diversité unique des [MASK] qui est l' un de ses attraits majeurs .
Chaque année , de fin novembre à fin décembre , le comité [MASK] offre les illuminations de l' avenue , tradition mise en place sous la présidence de [MASK] [MASK] [MASK] [MASK] .
Le coup d' envoi étant donné par une célébrité , tels [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , ou [MASK] [MASK] , la dernière en date étant [MASK] [MASK] .
La notoriété nationale et internationale de l' avenue , son accessibilité ( métro et [MASK] ) et sa dimension en font un lieu pour certaines grandes manifestations au caractère exceptionnel :