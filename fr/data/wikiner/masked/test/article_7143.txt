La région fut une partie des colonies portugaises d' [MASK] , formant avec [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] , l' [MASK] [MASK] [MASK] [MASK] [MASK] .
Après 450 ans de présence portugaise , [MASK] a été repris par les troupes indiennes sous [MASK] [MASK] le 19 décembre 1961 .
Il est tropical : il fait chaud toute l' année à [MASK] , il n' y a pas de saison froide .
Le tourisme est la principale source de revenus de [MASK] .
Le riz est la principale plante cultivée à [MASK] , viennent ensuite la noix de cajou et la noix de coco .
De nombreuses excursions touristiques sont possibles à [MASK] :