Le [MASK] est un projet de dictionnaire multilingue de [MASK] [MASK] .
Il est fondé sur un système de wiki et son contenu est librement réutilisable ( sous [MASK] ) .
Les projets [MASK] ont pour vocation de devenir ensemble un dictionnaire descriptif doublement multilingue ( les entrées et les définitions sont multilingues ) .
Selon le service de statistiques de web [MASK] , environ 5000000 de pages sont vues chaque jour sur l' ensemble des sites wiktionary.org .
Ce qui en fait approximativement le millième site le plus fréquenté d' [MASK] , et l' un des principaux sites en matière de lexicographie .
Le 6 avril 2005 , le [MASK] comptait environ 4000 articles .
Aux premiers jours de 2006 , il a dépassé les 100000 articles , et devint temporairement le [MASK] le plus important , toutes langues confondues .
En nombre d' articles , le [MASK] est le deuxième projet [MASK] le plus avancé ( la branche anglophone l' ayant dépassé le 16 juillet 2010 ) .