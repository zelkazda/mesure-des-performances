Cette bataille s' est soldée par une victoire décisive d' [MASK] [MASK] , qui lui permit de rattacher la [MASK] à l' [MASK] , ce qui n' était plus le cas depuis la mort de leur père [MASK] [MASK] [MASK] en 1087 .
[MASK] [MASK] était en conflit avec son frère aîné , [MASK] [MASK] , depuis la mort de leur frère [MASK] [MASK] [MASK] en 1100 .
Les conflits persistants entre les deux frères poussèrent [MASK] à envahir la [MASK] en 1105 , prenant notamment les villes de [MASK] et de [MASK] .
Après l' échec des tentatives de négociations , le duc refusant les offres du roi , la bataille s' avère inévitable et [MASK] donne l' ordre du combat .
[MASK] [MASK] a avec lui le comte [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] .
C' est alors que [MASK] [MASK] [MASK] [MASK] , en attaquant les forces ducales à revers , vient briser la mêlée et mettre définitivement la victoire du côté royal .
La plupart des prisonniers seront libérés à l' exception de [MASK] [MASK] qui passera le restant de ses jours en captivité .