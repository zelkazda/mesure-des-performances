Le [MASK] [MASK] [MASK] , plus connu sous l' abréviation [MASK] , littéralement le " protocole de transfert hypertexte " , est un protocole de communication client-serveur développé pour le [MASK] [MASK] [MASK] .
[MASK] est un protocole de la couche application .
Il peut fonctionner sur n' importe quelle connexion fiable , dans les faits on utilise le protocole [MASK] comme couche de transport .
À cette époque , le [MASK] [MASK] [MASK] ( [MASK] ) était déjà disponible pour transférer des fichiers , mais il ne supportait pas la notion de format de données telle qu' introduite par [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
La première version de [MASK] était très élémentaire , mais prévoyait déjà le support d' en-têtes [MASK] pour décrire les données transmises .
Certains serveurs autorisent d' autres méthodes de gestion des ressources du serveur ( par exemple [MASK] ) .
[MASK] permet l' identification du visiteur par transmission d' un nom et d' un mot de passe .
L' identification est cependant souvent effectuée par une couche applicative supérieure à [MASK] .
Au début du [MASK] [MASK] [MASK] , il était prévu d' ajouter au [MASK] [MASK] des capacités de négociation de contenu , en s' inspirant notamment de [MASK] .
En attendant , le [MASK] [MASK] 0.9 était extrêmement simple .
Le serveur reconnaît qu' il a affaire à une requête [MASK] 0.9 au fait que la version n' est pas précisée suite à l' URI .
Il ne doit jamais se comporter ainsi pour les requêtes [MASK] de version supérieure .
Il a fallu lui en attribuer un quand [MASK] 1.0 est arrivé .
Le [MASK] [MASK] 1.0 , décrit dans le [MASK] 1945 , prévoit l' utilisation d' en-têtes inspirés de [MASK] .
La gestion de la connexion reste identique à [MASK] 0.9 : le client établit la connexion , envoie une requête , le serveur répond et ferme immédiatement la connexion .
Les réponses [MASK] présentent le format suivant :
La version du [MASK] [MASK] est précisée suite à l' URI .
La première ligne donne le [MASK] [MASK] [MASK] [MASK] ( 200 dans ce cas ) .
Le [MASK] [MASK] 1.1 est décrit par le [MASK] 2616 qui rend le [MASK] 2068 obsolète .
La différence avec [MASK] 1.0 est une meilleure gestion du cache .
Les soucis majeurs des deux premières versions du [MASK] [MASK] sont d' une part le nombre important de connexions lors du chargement d' une page complexe ( contenant beaucoup d' images ou d' animations ) et d' autre part le temps d' ouverture d' une connexion entre client et serveur .
Des expérimentations de connexions persistantes ont cependant été effectuées avec [MASK] 1.0 , mais cela n' a été définitivement mis au point qu' avec [MASK] 1.1 .
Par défaut , [MASK] 1.1 utilise des connexions persistantes , autrement dit la connexion n' est pas immédiatement fermée après une requête , mais reste disponible pour une nouvelle requête .
[MASK] 1.1 supporte la négociation de contenu .