[MASK] [MASK] a été très tôt attiré par la scène théâtrale .
Il est le père de la comédienne [MASK] [MASK] .
Il présida également le festival de théâtre de [MASK] de 1963 à 1977 .
Ancien élève de [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , il est docteur et agrégé de droit public et sciences politiques .
Étudiant en droit à [MASK] en parallèle de [MASK] [MASK] [MASK] , et passionné de théâtre , [MASK] [MASK] crée en 1963 le festival de théâtre universitaire de [MASK] , qu' il préside jusqu' en 1977 .
La première année , il interprète le rôle-titre de [MASK] .
Diplômé de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] en 1961 , il obtient le doctorat en droit en 1967 à l' [MASK] [MASK] [MASK] .
En 1971 , il est nommé maître de conférences à l' [MASK] [MASK] [MASK] par le concours d' agrégation de droit public et sciences politiques , puis professeur de droit international en 1976 et doyen de l' unité d' enseignement et de recherche de sciences juridiques et économiques de l' [MASK] [MASK] [MASK] en 1977 .
En 1972 , il est appelé par le président [MASK] [MASK] à la direction du [MASK] [MASK] [MASK] .
Il entre au [MASK] [MASK] en 1977 , et devient secrétaire national à la culture en 1979 .
Élu en 1977 conseiller municipal du [MASK] [MASK] [MASK] [MASK] [MASK] , il milite contre le réaménagement des [MASK] [MASK] [MASK] .
Il est révélé au grand public en 1981 quand il est nommé [MASK] [MASK] [MASK] [MASK] , poste qu' il occupera pendant dix ans sous tous les gouvernements socialistes des deux septennats de [MASK] [MASK] .
Cette fête populaire , qui est l' occasion de concerts de rue gratuits et de manifestations culturelles , connut rapidement un grand succès en [MASK] au point que de nombreux pays en reprirent l' idée .
De même , en 1984 avec les [MASK] [MASK] [MASK] [MASK] .
Pendant les années d' alternance ( 1986-1988 et 1993-1995 ) , il retrouve son poste de professeur de droit à l' [MASK] [MASK] [MASK] [MASK] [MASK] .
Candidat lors des primaires du [MASK] [MASK] , il doit finalement se retirer sous les critiques de certains socialistes , le rocardien [MASK] [MASK] et une vingtaine de premiers secrétaires fédéraux ayant mené une campagne interne contre lui sous le thème de " [MASK] ?
[MASK] [MASK] renchérit en le traitant de " déshonneur de la gauche " .
[MASK] qualifie alors [MASK] [MASK] de " loser " , lequel s' oppose à l' entrée de [MASK] [MASK] au gouvernement en 1997 , après la victoire de la gauche aux législatives .
D' après [MASK] [MASK] , [MASK] a toujours été exaspéré par [MASK] , sa futilité , ses approximations , sa grandiloquence , son côté " paillettes " , son amour des " [MASK] [MASK] " et des grands travaux , sa soif de gloire , d' honneurs et d' argent .
Arrivé en pleine période de contestation étudiante et lycéenne , il commence pour " épurer l' atmosphère " par suspendre le projet de réforme universitaire de son prédécesseur , [MASK] [MASK] .
En 2004 , il est nommé porte-parole national de la campagne du [MASK] pour les régionales et les cantonales .
À la fin de l' année , il réintègre la direction du [MASK] [MASK] , en étant chargé , avec [MASK] [MASK] et [MASK] [MASK] , du projet socialiste pour 2007 .
Selon la [MASK] , entre 1987 et 2005 , sa cote de popularité oscille autour de 45 % d' opinions favorables , avec un taux , au plus bas , de 35 % en 2000 .
Il s' est également rendu chez le président cubain [MASK] [MASK] , ainsi que le vénézuélien [MASK] [MASK] .
Son amitié avec [MASK] [MASK] lui a été reprochée .
Cette primaire l' aurait opposé à [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .
Dans ce livre , [MASK] [MASK] déclare que [MASK] [MASK] n' a : " aucune expérience ni à l' international , ni dans un grand ministère " . " On ne peut pas jouer uniquement de son charme , ne rien dire , et espérer devenir présidente " " .
Selon lui , le premier secrétaire [MASK] [MASK] aurait : " manipulé tout le monde , en [ le ] faisant travailler sur le projet socialiste alors que [MASK] n' était jamais là " .
À la suite de son ralliement , [MASK] [MASK] a renoncé à la publication de son livre d' entretiens devenu obsolète , arguant qu ' " on ne peut pas [ l ] 'obliger à l' assumer alors qu ' [ il ] le réfute et ne [ s ] 'y reconnaît plus " .
Le 25 février 2009 , il se rend à [MASK] comme " émissaire spécial " de [MASK] [MASK] pour relancer le dialogue franco-cubain , cette fois sans critique de la direction de son parti .
Au printemps 2009 , il apporte son soutien au projet de [MASK] [MASK] , en désaccord avec les députés de son groupe présents lors des débats .
Malgré cette prise de position , il ne prit à aucun moment part aux débats dans [MASK] [MASK] [MASK] .
Le 11 mars , son collègue socialiste [MASK] [MASK] affirme par ailleurs dans l' hémicycle que [MASK] [MASK] n' aurait pas lu le texte .
[MASK] [MASK] est désigné secrétaire national du [MASK] à l' action culturelle en 1973 .
Il devient [MASK] [MASK] [MASK] [MASK] en 1981 .
Le [MASK] [MASK] , suivi par les autres partis politiques , désire accentuer la mise en application de démocratisation de la culture , initiée par [MASK] [MASK] sans qu' il n' ait jamais prononcé ce terme .
Ainsi , au niveau local , les budgets liés à la culture éclatent et deviennent un réel enjeu politique , au même titre que l' économie du pays au profit du [MASK] [MASK] .
Les politiques culturelles , soutenues par le président , [MASK] [MASK] , leur donne un caractère légitime .
Les objectifs principaux de la politique de [MASK] [MASK] sont à la fois de diminuer la hiérarchisation traditionnelle qui sépare les " arts majeurs " des " arts mineurs " et d' intégrer aux derniers des activités alors non considérées comme faisant parties du domaine culturel .
Ainsi , [MASK] [MASK] permet la mise en avant :
[MASK] [MASK] est également à l' origine du concept de [MASK] , salle sécurisée de grande capacité destinée à l' organisation de concerts de " rock " ( terme employé à l' époque pour désigner tout ce qui n' est pas du classique ou du jazz ) .