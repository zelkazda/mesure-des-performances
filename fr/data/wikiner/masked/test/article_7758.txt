[MASK] [MASK] est né à [MASK] .
En 1809 il entra à l' université de [MASK] [MASK] [MASK] [MASK] et en 1812 il proposa son premier article de mathématiques à la [MASK] [MASK] .
Entre 1834 et 1838 , à partir de l' observatoire du [MASK] [MASK] [MASK] , il cartographia le ciel austral .
[MASK] était aussi un chimiste accompli et il s' intéressa beaucoup à la photographie , alors naissante : il donna des conférences sur le sujet et exhibait ses propres photographies .
En 1839 , indépendamment de [MASK] [MASK] , il inventa un procédé photographique utilisant du papier sensibilisé .
On découvrit alors les comptes-rendus de ses expériences , antérieures et similaires à celle de [MASK] [MASK] .