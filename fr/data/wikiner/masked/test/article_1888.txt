Il est surnommé " le roi des animaux " car sa crinière lui donne un aspect semblable au [MASK] , qui apparaît comme " le roi des astres " .
Le lion est le deuxième plus grand félidé , après le tigre , et ainsi le plus grand carnivore d' [MASK] .
Les plus grands lions vivent au sud de l' [MASK] , les plus petits en [MASK] .
Les lions du [MASK] [MASK] [MASK] [MASK] en [MASK] ont tendance à être plus petits que la moyenne .
Les lions du parc national du [MASK] , sont quant à eux dépourvus de crinières .
Chez les lions d' [MASK] , ainsi que certains spécimens d' [MASK] [MASK] [MASK] [MASK] , la crinière est clairement moins prononcée que chez leurs cousins d' [MASK] , les poils ont la particularité d' être également plus fins .
En 2005 , deux lionceaux au pelage blanc et aux yeux bleus sont nés dans un parc zoologique à proximité d' [MASK] et quatre au [MASK] [MASK] [MASK] [MASK] , près de [MASK] , le 20 mai 2007 , de deux parents blancs également .
Le [MASK] [MASK] [MASK] dans le [MASK] fut le premier parc français à présenter un couple de lions blancs au public .
Le plus étonnant chez les lions est leur queue se terminant par un pinceau de poils noirs ; non seulement cette dernière est indispensable contre les mouches , mais à l' extrémité se trouve une vertèbre non développée , découverte par [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] , en 1829 , décrit cette partie comme une sorte d' ongle ou de production cornée ayant la forme d' un cône un peu recourbé vers la pointe , adhérant par sa base à la peau seulement , et non à la dernière vertèbre caudale , dont il est séparé de 4 à 6 mm .
Aujourd'hui , sa diffusion est largement limitée à l' [MASK] [MASK] .
Les populations significatives de lions africains sont localisées dans les parcs nationaux du [MASK] , de [MASK] et d' [MASK] [MASK] [MASK] et se font rares en dehors des zones protégées .
Classé comme " vulnérable " par l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , le lion est exposé à un risque d' extinction .
Dans le [MASK] [MASK] [MASK] [MASK] en [MASK] , la densité des lions peut atteindre un individu par kilomètre carré .
Dans l' [MASK] [MASK] [MASK] [MASK] , le nombre maximum d' individus est de 1,6 à 2,4 au km² .
[MASK] les vieux mâles du groupe perdent la lutte , ils sont chassés et mènent ensuite une vie de solitaires .
[MASK] les nouveaux venus gagnent , ils en viennent fréquemment à l' infanticide , c' est-à-dire qu' ils tuent les petits de leurs prédécesseurs .
C' est la pénétration qui déclenche la ponte des ovules qui seront fécondés par les spermatozoïdes . , [MASK] une lionne accepte de se reproduire , ils s' accoupleront toutes les quinze minutes et ce , jusqu' à cinquante fois par jour , auquel cas chaque rapport dure environ trente secondes , jusqu' à ce que l' œstrus de la femelle , qui ne dure que quatre jours , soit terminé .
[MASK] cette dernière est assez éloignée du groupe , la mère ira seule à la chasse .
Au [MASK] [MASK] [MASK] , la population des hyènes dépasse de beaucoup celle des lions résidents , aussi ces derniers obtiennent une grande partie de leur nourriture en volant les proies des hyènes .
En [MASK] [MASK] [MASK] , près de 300 éleveurs élèvent environ 5 000 lions pour la chasse ; 480 lions , dont 444 élevés en captivité , ont été chassés dans le pays , pour un prix variant de 6 000 à 8 000 dollars la femelle et de 20 000 à 30 000 dollars le mâle .
En [MASK] , le lion a pratiquement disparu depuis le milieu du XIX e siècle à l' état sauvage , autant par la chasse que par la réduction de son habitat .
Les maladies représentent un autre problème , surtout dans le [MASK] [MASK] [MASK] en [MASK] [MASK] [MASK] .
Environ 60 à 70 % des lions du parc [MASK] sont contaminés par le virus de l' immunodéficience féline , qui " paralyse " le système immunitaire de l' animal et ouvre ainsi la voie à la tuberculose .
Il arrive de nos jours que quelques lions attaquent les humains en [MASK] ; invariablement , les populations mènent des représailles .
Entre 1990 et 2005 , 563 villageois ont été attaqués par des lions en [MASK] , ce qui correspond à une augmentation considérable .
L' [MASK] est partie en 2004 du principe que le nombre de lions a diminué dans le monde entier au cours des vingt dernières années de 30 à 50 % .
À travers [MASK] [MASK] , le lion a disparu sur plus de 80 % de son ancien territoire .
Le lion africain est considéré comme " vulnérable " sur la liste rouge des espèces menacées de l' [MASK] , en raison de la baisse constante de l' effectif de cette espèce .
En [MASK] [MASK] [MASK] [MASK] , le nombre des lions est inférieur à 1500 .
Il n' y a plus que 200 à 300 exemplaires en [MASK] , gravement menacés par la perte de leur patrimoine génétique .
Les lions vivent en captivité depuis l' [MASK] , sur des périodes ponctuelles .
Les [MASK] les utilisaient dans leurs [MASK] par exemple .
En [MASK] , le premier lion fut d' ailleurs exhibé à [MASK] en 1716 .
Plus tard , les [MASK] organisateurs des jeux en conservaient .
Ainsi des [MASK] célèbres comme [MASK] , [MASK] , [MASK] [MASK] , ont ordonné la capture de centaines de lions à la fois .
[MASK] [MASK] rapporte que les princes indiens continuaient à en apprivoiser et que [MASK] [MASK] gardait même des lions à l' intérieur de ses habitations .
[MASK] [MASK] [MASK] rapporte lui que des lions ont été conservés en [MASK] , à [MASK] par la volonté d' [MASK] [MASK] [MASK] , le lion étant présent sur les héraldiques anglaises .
En 1982 , des procédures ont été mises en place en [MASK] du [MASK] pour préserver le patrimoine génétique du lion asiatique .
Ceux-ci descendent tous d' animaux ayant appartenu au roi du [MASK] .
Des combats entre lions et chiens en général ont été organisés à [MASK] en [MASK] à partir de 1800 et en [MASK] à partir de 1825 , .
Les pionniers du domptage sont [MASK] [MASK] , un français , et [MASK] [MASK] [MASK] , un américain .
[MASK] [MASK] [MASK] fit une tournée en [MASK] , devant la reine [MASK] .
[MASK] [MASK] est un autre dompteur de lions célèbre .
Cette tradition est toujours vivace ; certains dompteurs actuels , comme le duo de magiciens [MASK] [MASK] [MASK] et leurs lions blancs , sont toujours célèbres .
En [MASK] , régulièrement , des actions de saisie ont été menées par l' administration même si certaines associations les trouvent peu virulentes .
L' [MASK] interdit même la possession de lion depuis 1998 .
Ceux-ci sont considérés comme les plus grands lions d' [MASK] et ont chassé pendant l' interglaciaire cromérien , il y a plus de 500000 ans , près de [MASK] en [MASK] et près de [MASK] dans le [MASK] .
La peau d' un lion de ce genre est gardée encore aujourd'hui au [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
En [MASK] , les pharaons furent représentés par des sphinx , lions à la tête humaine .
La plus célèbre de ces représentations est le [MASK] [MASK] [MASK] [MASK] .
[MASK] fut vénérée en tant que déesse au corps humain et à tête de lionne , envoyée par [MASK] contre les [MASK] qui complotaient contre lui .
Dans l' histoire d' [MASK] , une des fables d' [MASK] , le héros , un esclave échappé , retire une épine de la patte d' un lion ; quand plus tard , pour le punir de son évasion , il fut jeté par son maître au lion pour être dévoré , l' animal le reconnut et refusa de tuer l' homme .
À [MASK] par exemple , la voie processionnelle est décorée de bas-reliefs en carreaux de céramique en forme de lion du temps de [MASK] [MASK] .
Dans l' art grec , le motif des scènes de chasse du [MASK] [MASK] [MASK] dont la peau est l' attribut d' [MASK] est très présent .
Chez les [MASK] , il est également très représenté comme animal du cirque , combattant contre des gladiateurs .
Dans l' art chrétien , le lion accompagne parfois [MASK] [MASK] , ou la force , c' est le symbole de [MASK] [MASK] [MASK] , de la royauté .
Le lion n' est présent en [MASK] que dans la péninsule indienne , il est pourtant très présent dans l' art statuaire de l' ensemble des pays asiatiques .
Des lions , représentés avec une crinière bouclée , montent la garde devant les pagodes , comme celle de [MASK] ou dans les temples bouddhistes .
Une symbolique basée sur la figure du lion a pu être créée ; par exemple , un lion d' argent sur champ de sinople symboliserait la tempérance et selon [MASK] [MASK] , les divers lions héraldiques sont issus de lointaines croyances préhistoriques .
Le lion est le symbole national de l' [MASK] , et figure sur ces armoiries sous la forme des lions de l' empereur indien [MASK] .
Par exemple , la marque automobile [MASK] utilise comme symbole les armoiries de [MASK] depuis 1847 .
Le groupe bancaire [MASK] utilise également un logo qui contient un lion , cette fois-ci , orange .
[MASK] [MASK] [MASK] [MASK] , imitant [MASK] dans plusieurs de ses fables , fait du lion un des personnages principaux .
[MASK] [MASK] dans sa saga du [MASK] [MASK] [MASK] utilise le symbole du lion , " roi des animaux " , à travers [MASK] , dieu vivant combattant le mal , se sacrifiant pour le salut de son peuple et ressuscitant peu après .
Dans le cinéma avec , entre autres , le film d' animation à succès de [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Le lion est un personnage récurrent de nombreux films , de [MASK] au [MASK] [MASK] [MASK] , et de séries télévisées avec par exemple [MASK] .
Une consultation partielle du grand dictionnaire arabe-français de [MASK] confirme ce nombre .
A contrario , pour [MASK] [MASK] [MASK] [MASK] [MASK] ce ne sont ni sa force , ni son courage , mais ses sauts d' humeur qui lui valurent , en [MASK] , d' être surnommé " [MASK] [MASK] [MASK] " , en référence à l' imprévisibilité de l' animal .
Dans le firmament de l' hémisphère nord , il existe deux constellations nommées pour cet animal : le [MASK] et le [MASK] [MASK] , située juste au-dessus de la première .
Ainsi , elle fut mentionnée par [MASK] dans son [MASK] et correspondrait au [MASK] [MASK] [MASK] tué par [MASK] .