En [MASK] , le microfilmage a souvent été réalisé par les [MASK] qui se donnent pour mission de reconstituer l' arbre généalogique de l' humanité .
On citera [MASK] .
Certaines municipalités permettent de consulter sur internet une partie de leurs archives , comme par exemple la ville de [MASK] .
Plusieurs dizaines de milliers d' arbres en ligne sont consultables grâce au moteur [MASK] .
D' autres formes sont également présentes telles que des groupes de généalogie sur [MASK] [MASK] qui regroupent fonction de critères géographiques ou thématiques les internautes .