Le [MASK] , au [MASK] [MASK] ( [MASK] [MASK] ) et au [MASK] ) , est une province du centre-ouest de la [MASK] , dont la capitale est [MASK] .
Par la suite , se développent le [MASK] [MASK] [MASK] ( 巴 ) dans la région de [MASK] et le [MASK] [MASK] [MASK] ( 蜀 ) dans la région de [MASK] .
L' existence de [MASK] était inconnue jusqu' aux découvertes archéologiques en 1986 dans un petit village appelé [MASK] , dans le district de [MASK] .
Le [MASK] est une région très montagneuse , et l' accès à cette province a toujours été difficile .
Ses sommets hauts de 6000 , voire 7000 mètres , plus anciens que ceux de l' [MASK] tout proche , émergent d' une couche brumeuse et humide qui favorise le développement d' une végétation particulièrement épaisse .
La mosaïque de vieilles montagnes granitiques et de bassins qui caractérise le [MASK] chinois s' est formée il y a 140 à 65 millions d' années quand la poussée des plaques tectoniques a façonné l' [MASK] [MASK] [MASK] .
Le [MASK] dispose aussi du plus large rendement de porc parmi toutes les provinces et le deuxième plus grand rendement de cocons de vers à soie en [MASK] durant l' année 1999 .
Le [MASK] est riche en ressources avec plus de 132 types de minéraux tels que le vanadium , le titane et le lithium qui sont les plus répandus en [MASK] .
C' est aussi au [MASK] qu' on parle le plus le guiqiong et le horpa .