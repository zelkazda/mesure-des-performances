Il fut l' élève du caricaturiste [MASK] [MASK] .
Il créa le plus ancien dessin animé sur pellicule de cinéma connu à ce jour , [MASK] , qui fut projeté pour la première fois le 17 août 1908 , au [MASK] [MASK] [MASK] à [MASK] , pour la société [MASK] .
Ce type d' animation connut un essor considérable dans le monde au cours des décennies qui suivirent , se diffusant notamment à l' échelle planétaire dans les années 1930 via les studios [MASK] .
En créant ainsi le dessin animé tel que nous le connaissons de nos jours , [MASK] [MASK] associa l' art du cinéma , développé par les [MASK] [MASK] à partir de 1895 , et celui des pantomimes lumineuses d' [MASK] [MASK] , projetées sur le théâtre optique , un système proche du projecteur de cinéma , à partir du 28 octobre 1892 .
De 1908 à 1923 , [MASK] [MASK] réalisa trois cents films , pour la plupart des films précurseurs en matière de cinéma d' animation , puisqu' il maniait avec autant de bonheur le dessin que les allumettes , le papier découpé ou encore les marionnettes .
À partir de 1886 , son épouse eut une liaison avec [MASK] [MASK] dit [MASK] ( qui sera plus tard le mari de [MASK] ) ; les deux amants auront d' ailleurs un fils ensemble .
Cette épisode provoqua le second duel à l' épée de la vie de [MASK] , le 25 octobre 1886 ( le premier duel l' ayant opposé à [MASK] [MASK] en 1880 ) .
Il fréquenta de nombreux écrivains , comme [MASK] [MASK] ou [MASK] [MASK] .
Il rencontra également des cinéastes comme [MASK] [MASK] ( qui mourut le même jour que lui à quelques heures d' intervalle ) ou [MASK] [MASK] .