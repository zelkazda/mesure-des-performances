Le néologisme " négationnisme " a été créé par l' historien [MASK] [MASK] en 1987 .
Son utilité est de désigner correctement la démarche de falsification historique comme celle de [MASK] [MASK] ou d' [MASK] [MASK] , qui se qualifient eux-mêmes indûment de " révisionnistes " .
La négation d' un génocide ( [MASK] , [MASK] [MASK] , [MASK] , etc . )
La négation de la [MASK] , à l' origine du terme même de " négationnisme " , est un fait complexe actuellement développé dans un article consacré .
Entre avril 1915 et juillet 1916 ont été méthodiquement massacrés les deux tiers de la population arménienne de l' [MASK] [MASK] par le gouvernement [MASK] .
[MASK] [MASK] a fait près de 1 500 000 morts .
La [MASK] , la [MASK] [MASK] [MASK] et les [MASK] dénoncent régulièrement les tentatives de la droite nationaliste japonaise de nier les crimes de guerre perpétrés sur le continent asiatique par l' armée impériale japonaise au cours de l' expansion de l' empire japonais .
Le [MASK] [MASK] [MASK] , considéré par le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et la [MASK] [MASK] [MASK] [MASK] comme " génocide " , est contesté dans cette dimension par certains .
Certains pourront ainsi prétendre , sans aucune démonstration à l' appui de leur affirmation très gratuite , que les chambres à gaz ne sont que des constructions postérieures à la guerre érigées pour accréditer la thèse de la [MASK] et diaboliser l' [MASK] [MASK] .
Les négationnistes , selon [MASK] [MASK] , exploitent l' ignorance des journalistes .
[MASK] cite la description que fait [MASK] [MASK] de cette manipulation :
Il est banal de constater que l' histoire est écrite par les survivants et par les vainqueurs , ainsi si l' [MASK] écrasée et occupée en 1945 n' a pu que reconnaître les crimes nazis , les alliés ont pu écrire leur histoire officielle et minimiser leurs propres crimes et notamment les bombardements massifs sur les populations civiles des villes japonaises et allemandes .
La négation du génocide perpétré par l' [MASK] [MASK] sur [MASK] [MASK] est réprimée pénalement dans les pays suivants :