Poète du souvenir d' une première rencontre amoureuse , de la nostalgie d' une autre existence ( " Je suis un pâle enfant du vieux [MASK] " ) ou de la beauté du crépuscule ( " Le crépuscule est triste et doux " ) , il rencontra un grand succès populaire avant de tomber dans l' oubli .
Il naquit à [MASK] au 2 , [MASK] [MASK] [MASK] [MASK] .
Il mourut à [MASK] au 12 , [MASK] [MASK] et fut inhumé au [MASK] [MASK] [MASK] .
Les " poètes maudits " de son temps ( [MASK] , [MASK] , [MASK] [MASK] ) , aimaient à pasticher ses dizains .