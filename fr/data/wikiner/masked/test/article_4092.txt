Le [MASK] [MASK] [MASK] est un canton de [MASK] .
Sur les rivages du [MASK] [MASK] [MASK] et de celui du [MASK] [MASK] [MASK] , des vestiges préhistoriques ont été découverts .
Le [MASK] [MASK] [MASK] a rejoint la [MASK] [MASK] en 1481 .
En 2008 , le [MASK] [MASK] [MASK] comptait 222 ' 471 habitants dont 20.7 % d' étrangers .
Pays de la fondue , le [MASK] [MASK] [MASK] est aussi une région de ponts et de transition entre la partie romande et la partie alémanique de la [MASK] , entre les lacs ( [MASK] et [MASK] ) et les montagnes des [MASK] .
Le [MASK] [MASK] [MASK] est situé sur les axes autoroutiers [MASK] ( [MASK] -- [MASK] -- [MASK] -- [MASK] -- [MASK] [MASK] ) , [MASK] ( [MASK] -- [MASK] -- [MASK] ) et des transports ferroviaires qui vont de [MASK] / [MASK] à [MASK] / [MASK] .
Le [MASK] [MASK] [MASK] est décomposé en 7 districts :
Dans le [MASK] [MASK] [MASK] , seules [MASK] , [MASK] et [MASK] sont considérées statistiquement comme des villes .
voir [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ainsi que la liste des fusions de communes .
Au fil du temps , cet emblème est devenu celui , non plus de la ville , mais du [MASK] [MASK] [MASK] .