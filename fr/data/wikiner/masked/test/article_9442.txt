La création poétique , partie de la cour , se répand par la suite dans les communes toscanes et à [MASK] , en restant cependant le privilège d' une riche , bien que toujours plus vaste , bourgeoisie communale .
C' est donc principalement à [MASK] et dans toute la [MASK] que la littérature en langue vulgaire poursuit son développement .
C' est principalement sous les plumes de [MASK] [MASK] , [MASK] [MASK] [MASK] et bien sûr [MASK] que va se construire ce nouveau courant qui connaitra un succès suivi durant tout le XIV e siècle .
Trois immenses figures dominent ce siècle : [MASK] , [MASK] et [MASK] .
[MASK] [MASK] ( 1266 -- 1321 ) est généralement considéré comme le véritable fondateur de la poésie et de la littérature italiennes .
C' est la circulation et les imitations des œuvres de [MASK] qui donneront au toscan ses lettres de noblesse , et accéléreront sa diffusion dans toute la péninsule au détriment des autres dialectes .
De son côté , [MASK] ( 1313 -- 1375 ) écrit tantôt en toscan tantôt en latin .
Dans son [MASK] ( 1348 -- 1353 ) il fait l' éloge du mode de vie de la bourgeoisie commerçante .
Dans ce domaine , l' œuvre de [MASK] [MASK] [MASK] est particulièrement marquante .
Ainsi , la majeure partie de l' activité littéraire du XV e siècle consiste à compiler et à traduire des textes grecs et latins des auteurs de l' [MASK] .
À l' exception de la [MASK] , où demeure encore l' héritage des " trois couronnes " , partout ailleurs dans la péninsule , les langues vulgaires reculent .
À [MASK] , sous le mécénat et la protection des [MASK] , les auteurs latins prospèrent .
Dans la deuxième moitié du siècle , [MASK] [MASK] représentera le volet politique et militaire de ce nouvel humanisme comme il le montrera à travers son traité : [MASK] [MASK] , tandis que [MASK] [MASK] donnera lui sa vision d' historien .
À [MASK] , où depuis [MASK] le prestige du toscan est resté grand , la nouvelle sous toutes ses formes continue à recevoir les faveurs du public .
À [MASK] , [MASK] [MASK] ( 1378 -- 1455 ) rédige son commentari dans lequel il mêle l' histoire de l' art au genre autobiographique .
Le génial [MASK] [MASK] [MASK] ( 1452 -- 1519 ) laisse également une œuvre fragmentaire mais riche , à travers plus de cinq mille feuillets qui composent ses carnets .
Les premières tentatives de cette intégration , de celle " littératurisation " , des vers populaires sont à rechercher dans les poésies de [MASK] [MASK] ( v. 1335 -- v. 1400 ) .
Cette évolution doit beaucoup à l' influence de [MASK] [MASK] [MASK] .
Le recueil , auquel contribuent [MASK] [MASK] ( 1424 -- 1498 ) et [MASK] [MASK] ( 1454 -- 1494 ) va participer au renforcement du prestige du toscan sur les autres langues vulgaires de la péninsule .
À la fin du siècle on voit s' affirmer le renouveau de la poésie chevaleresque , d' abord à [MASK] avec [MASK] [MASK] puis à [MASK] avec [MASK] [MASK] [MASK] .
L' œuvre , qui reprend le mythe d' [MASK] , ouvrira une nouvelle voie pour le théâtre italien .
La [MASK] et l' [MASK] luttent pour prendre le contrôle de la péninsule et c' est finalement [MASK] [MASK] qui l' emporte après plusieurs décennies d' affrontement .
Les [MASK] [MASK] [MASK] signés en 1559 officialisent l' hégémonie espagnole sur tout le sud du pays et lui assurent le contrôle du nord par l' intermédiaire de la papauté .
Le raffinement recherché dans les cours des princes contribue au développement de la littérature courtisane et chevaleresque dont l' [MASK] est un des plus illustres représentants .
En 1958 , l' [MASK] [MASK] [MASK] est fondée à [MASK] .
[MASK] [MASK] , à [MASK] , use du nouveau moyen de l' imprimerie pour diffuser ses nouvelles .
Condamné à l' errance et à la persécution , il est finalement brûlé vif à [MASK] .