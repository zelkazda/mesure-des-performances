Ceci était particulièrement novateur , s' agissant d' ensembles éventuellement infinis ( ce sont ces derniers qui intéressaient [MASK] ) .
Ce sont les propriétés de cette relation que [MASK] , puis d' autres , ont axiomatisé en théorie des ensembles .
Mais ce n' était pas l' intention de [MASK] , et il n' avait pas non plus axiomatisé sa théorie .
Le symbole " ∈ " , dérive de la lettre grecque ε ( epsilon ) introduite par [MASK] [MASK] dès 1889 .
Le ε renvoie à l' initiale du mot " est " ( en latin , langue de l' article de [MASK] de 1889 !
En mathématiques -- et pas seulement en mathématiques d' ailleurs -- , on considère que deux objets sont égaux quand ils ont les mêmes propriétés , que l' on ne peut donc les distinguer l' un de l' autre -- c' est la définition de l' égalité de [MASK] .
La restriction de la compréhension à un ensemble connu protège contre ce genre de paradoxes , elle correspond directement au schéma d' axiomes de compréhension de la théorie de [MASK] .
Cette restriction ne peut se lever que dans des cas particuliers précis , qui correspondent à d' autres axiomes de la théorie de [MASK] ( axiome de la paire , axiome de la réunion , axiome de l' ensemble des parties ) .