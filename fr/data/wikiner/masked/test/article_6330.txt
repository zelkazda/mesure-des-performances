Cette hypothèse est justifiée par le fait que le système de la langue est " relatif " et " oppositif " ( [MASK] ) .
[MASK] disait : " dans la langue , il n' y a que des différences " .
Les principaux auteurs et penseurs post-structuralistes sont : [MASK] [MASK] , un philosophe qui renouvelle l' épistémologie , le philosophe [MASK] [MASK] et le sociologue [MASK] [MASK] .
Cette théorie s' appuie sur la linguistique , [MASK] [MASK] [MASK] ayant montré que toute langue constitue un système au sein duquel les signes se combinent et évoluent d' une façon qui s' impose à ceux qui la manient .
-- [MASK] [MASK]
Pour [MASK] [MASK] , le structuralisme " est bien une méthode et non pas une doctrine " et " le danger permanent qui menace le structuralisme [ ... ] est le réalisme de la structure sur lequel on débouche sitôt que l' on oublie ses attaches avec les opérations dont elle est issue " .