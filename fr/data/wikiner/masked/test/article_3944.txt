Issu d' une famille piémontaise , il est né au n° 3 de le [MASK] [MASK] à [MASK] , en 1888 , où son père était tailleur de pierre , luthier et chanteur d' opéra .
Il est professeur à [MASK] , à [MASK] et à [MASK] .
Il fait campagne aux [MASK] , en [MASK] , en [MASK] , en [MASK] , en [MASK] et en [MASK] .
Au cours de son séjour à [MASK] , il rencontre [MASK] [MASK] et le philosophe [MASK] [MASK] qui devient son ami .
Plus tard , en 1931 , il arrive au [MASK] où il va passer une autre longue partie de sa vie en tant que professeur au lycée de [MASK] .
C' est à la fin de ce séjour , en 1945 , qu' il obtient le [MASK] [MASK] pour [MASK] [MASK] [MASK] .
Cette récompense prestigieuse avait été précédée , deux ans auparavant , par un hommage rendu à [MASK] .
Arrivé à l' âge de la retraite , il partage sa vie entre [MASK] et [MASK] où il découvre un [MASK] , terre de paysans et de vignerons , qu' il va chanter avec des accents homériques .
Le chantre du [MASK] désira reposer dans le cimetière de [MASK] .