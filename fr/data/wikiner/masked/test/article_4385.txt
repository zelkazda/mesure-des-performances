[MASK] est la troisième île française par la taille ( après la [MASK] et [MASK] ) , si l' on omet l' île d' [MASK] , l' [MASK] [MASK] [MASK] et l' [MASK] [MASK] [MASK] , qui sont désormais reliées au continent par un pont .
L' île est séparée de l' archipel de [MASK] par le [MASK] [MASK] [MASK] , un froid et puissant courant marin ( 8 à 10 nœuds ) résultant d' une faille locale de 60 m de profondeur .
[MASK] est entourée de plusieurs îlots , dont le plus gros , au nord , est considéré comme une île : l' [MASK] [MASK] [MASK] ( 0,28 km 2 ) .
[MASK] marque traditionnellement l' entrée sud de la [MASK] ( l' entrée nord étant balisée par les [MASK] appelées plus couramment [MASK] [MASK] , de leur nom anglais ) .
Par sa situation , [MASK] est épargnée des nombreuses sources de pollution de l' air , ce qui en fait une zone d' étape privilégiée pour certaines espèces d' oiseaux migrateurs ou perdus en mer .
Les arbres sont assez peu nombreux à [MASK] .
L' [MASK] [MASK] [MASK] est une terre peu cultivée .
À noter que lorsque le brouillard s' installe sur [MASK] , il peut durer quelques heures comme il peut s' installer pendant plusieurs jours .
Ainsi , les prévisions de température faites par [MASK] sont généralement plus basses que la réalité .
En effet , [MASK] est balayée quasi-continuellement par les vents , ce qui régule significativement la température de l' air .
[MASK] est une commune insulaire et ne possède pas de contact terrestre avec une autre commune .
Cependant , l' [MASK] est située à 9 km au sud-est d' [MASK] .