[MASK] [MASK] est un personnage cinématographique de fiction , apparaissant dans cinq films écrits et réalisés par [MASK] [MASK] et tous interprétés par [MASK] [MASK] :
Le temps , dans le cycle d' [MASK] [MASK] , n' est pas linéaire .
[MASK] joue de sa ressemblance avec l' acteur , utilise les lieux de son enfance et va même jusqu' à projeter d' épouser [MASK] [MASK] .
[MASK] a ainsi l' occasion de peindre trois états , trois âges , de la femme : convoitée et vouvoyée ( [MASK] [MASK] ) , mariée et trompée ( [MASK] [MASK] ) , divorcée mais amie ( [MASK] [MASK] [MASK] [MASK] ) .
Elle est sage et naïve dans [MASK] [MASK] , douce et amère dans " [MASK] [MASK] " , indépendante et déterminée dans [MASK] [MASK] [MASK] [MASK] ; son personnage change et évolue au fur et à mesure que les épisodes passent , avec une justesse troublante , vers plus de gravité et d' amertume .
Il songe à se marier avec [MASK] [MASK] , et revient brutalement sur sa décision au dernier moment .
[MASK] [MASK] a quatorze ans lorsque [MASK] [MASK] le choisit pour jouer le personnage d' [MASK] [MASK] dans [MASK] [MASK] [MASK] [MASK] .
[MASK] était pour [MASK] et [MASK] un ami paternel .
En 1983 , j' ai reçu une lettre déchirante dans laquelle il m' annonçait sa maladie : " J' ai bien failli passer de l' autre côté du miroir mais pas au sens d' [MASK] [MASK] [MASK] [MASK] [MASK] , plutôt au sens d' [MASK] dans les films de [MASK] [MASK] " .
Le cycle [MASK] [MASK] met donc en scène des personnages ordinaires ( issus de la vie de [MASK] et de [MASK] [MASK] ) présenté de façon extraordinaire .
Dans les films de [MASK] comme [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] ( 1936 ) le réalisme et l' intimité sont suggérés par l' utilisation de cadres ajoutés comme des portes ou des fenêtres et par l' exploration d' une cour intérieure d' immeuble comme lieu central .
Cette capacité à communiquer qui progresse au cours du film , marque une évolution dans le personnage d' [MASK] [MASK] , jusque là plutôt solitaire .