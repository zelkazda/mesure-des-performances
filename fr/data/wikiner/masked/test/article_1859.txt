Le calendrier républicain , ou calendrier révolutionnaire français , fut créé pendant la [MASK] [MASK] et fut utilisé de 1792 à 1806 , ainsi que brièvement durant la [MASK] [MASK] [MASK] ( 1871 ) .
D' autre part , le philosophe grec [MASK] [MASK] s' en inspira pour son calendrier théosébiste dans les années 1830 .
L' organisation du nouveau calendrier a été créée par une commission formée de [MASK] [MASK] et de [MASK] [MASK] [MASK] , qui demandèrent que [MASK] [MASK] leur soit adjoint .
Ils associèrent [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] à leurs travaux .
[MASK] [MASK] fut le rapporteur de la commission , et c' est à ce titre que la création du calendrier républicain lui est généralement attribuée .
Les noms des mois et des jours furent conçus par le poète [MASK] [MASK] [MASK] avec l' aide d' [MASK] [MASK] , jardinier du [MASK] [MASK] [MASK] du [MASK] [MASK] [MASK] [MASK] [MASK] .