Il ne fera que les deux premiers tomes de la série , et passera le relais au dessinateur [MASK] [MASK] .
En 1979 , il démarre [MASK] [MASK] [MASK] [MASK] dans la revue [MASK] , série dont le tome 1 paraît en janvier 1980 .
En 1993 , il entame dans [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] , un récit futuriste réalisé en collaboration avec [MASK] [MASK] .
Tant par son souci d' authenticité ( il aime par exemple reconstituer ses différents décors sous la forme de véritables maquettes ) que par son sens de la narration , du découpage , des dialogues et de la mise en couleurs , [MASK] [MASK] est déjà une figure emblématique du monde du 9 e art .
( En collaboration avec [MASK] [MASK] )