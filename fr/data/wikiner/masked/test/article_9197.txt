Il passe à [MASK] , à [MASK] .
Il revient à nouveau en 1649 , mais reprend très vite la route , souvent de façon aventureuse et risquée , cette fois vers les [MASK] espagnols , [MASK] , [MASK] , [MASK] .
On le retrouve à [MASK] en 1653 .
C' est à cette même époque qu' au cours d' un passage à [MASK] , il participe à une joute musicale avec [MASK] [MASK] , qui restera son ami et avec qui il entretiendra une correspondance suivie .
La mort de son protecteur et ami [MASK] [MASK] survient en 1657 : cette mort consterne [MASK] qui compose à cette occasion pour le clavecin une remarquable " lamentation " à la mémoire du défunt .
Il s' installe au château d' [MASK] ( dépendance à cette époque du [MASK] ) , fait d' autres voyages ( il parvient à [MASK] en 1662 dans un état de pauvreté total , s' étant fait voler pendant le voyage ... .et actionne des soufflets d' orgue pour gagner un peu d' argent ) ; il rencontre [MASK] en 1665 à [MASK] .
Elle comprend de nombreuses pièces de forme italienne dans un style proche de [MASK] et plusieurs dizaines de suites de danses dont certaines sont vraisemblablement perdues .
Il participe activement à la mise en forme de la suite de danses et à sa diffusion en [MASK] .
[MASK] est également un artiste sensible , qui " invente " la musique à programme : de nombreuses pièces initiales de ses suites évoquent , dans leur titre et dans leur écriture , ses aventures personnelles , ses états d' âme .
[MASK] [MASK] , entre autres compositeurs , avait pour lui une grande estime .