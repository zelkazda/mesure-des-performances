Le [MASK] , ou [MASK] [MASK] [MASK] [MASK] [MASK] , est un pays de l' [MASK] , enclavé , bordé au nord par la [MASK] ( [MASK] [MASK] [MASK] [MASK] ) , au sud , à l' ouest et à l' est par l' [MASK] .
Bien que petit , le [MASK] possède une très grande variété de paysages , s' étendant du tropical humide du [MASK] , au sud , jusqu' aux plus hautes montagnes du monde , au nord .
Le [MASK] possède huit montagnes parmi les dix plus hautes du monde , dont l' [MASK] ( [MASK] en népalais ) qui marque la frontière avec la [MASK] .
Le [MASK] a été rendu célèbre pour les possibilités qu' il offre pour le tourisme , le trekking , l' alpinisme , le VTT , les safaris , le rafting et ses nombreux temples et lieux de cultes .
[MASK] est la capitale ( politique et religieuse ) du [MASK] , dont elle est largement la plus grande ville .
Les [MASK] , considérés comme les premiers habitants de la [MASK] [MASK] [MASK] comptent pour 5,4 % de la population népalaise .
Après une histoire riche en rebondissements , où les régions qui le constituent ont connu une diversité de régimes totalitaires qui se sont successivement rassemblés ou séparés , le [MASK] devint une monarchie constitutionnelle en 1990 .
L' instabilité politique , déjà latente , prit alors de l' ampleur à partir de 1996 , où une insurrection menée par le [MASK] [MASK] [MASK] [MASK] ( maoïste ) : la " [MASK] [MASK] [MASK] [MASK] " , apparut notamment dans les campagnes .
L' arrivée sur le trône de [MASK] , personnage déjà très impopulaire , aggrava la situation lorsque celui-ci chercha à exercer un pouvoir personnel en suspendant les libertés fondamentales et le parlement .
[MASK] est élue le 10 avril 2008 et voit la victoire relative des maoïstes qui remportent plus du tiers des 601 sièges .
Le [MASK] est subdivisé en cinq régions de développement ( विकास क्षेत्र , vikās kṣetra ) , en 14 zones administratives ( अञ्चल , añcal , transcrit par " anchal " , au singulier et au pluriel ) et en 75 districts ( जिल्ला , jillā , transcrit par " jilla " ) .
Le [MASK] a approximativement la forme d' un trapèze .
Il est enclavé entre l' [MASK] et la [MASK] avec lesquelles il partagent 2 810 km de frontières terrestres .
Le [MASK] est un pays pauvre avec un revenu moyen par personne de 340 dollars par an .
La population du [MASK] était estimée en 2004 à 27 millions d' habitants .