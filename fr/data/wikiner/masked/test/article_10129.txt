Comme son nom l' indique , [MASK] est très lié à [MASK] [MASK] ( les syntaxes et concepts des deux langages se ressemblent ) , mais ne peut normalement qu' exécuter du code provenant d' une application hôte ( et non pas d' une application autonome ) .
Il peut cependant être utilisé pour contrôler une application à partir d' une autre ( par exemple , créer automatiquement un document [MASK] à partir de données [MASK] ) .
[MASK] est fonctionnellement riche et extrêmement flexible , mais il possède d' importantes limitations , comme son support limité des fonctions de rappel ( callbacks ) , ainsi qu' une gestion des erreurs archaïque , utilisation de handler d' erreurs en lieu et place d' un mécanisme d' exceptions .
Toutefois , en raison de la dépendance de certaines entreprises à [MASK] , [MASK] sera encore disponible dans [MASK] 2008 .
L' enregistreur de macro sous [MASK] [MASK] permet de générer facilement du code [MASK] dans une procédure .
Toute la séquence d' action effectuée entre le début et la fin de l' enregistrement est enregistré dans une procédure [MASK] , qui pourra être réexécutée à l' identique .
Il est possible de modifier ce code ou de programmer directement dans la [MASK] .
[MASK] [MASK] permet de créer des fonctions personnalisées programmées en code [MASK] et placées dans un module .
Le [MASK] est grandement utilisé pour [MASK] [MASK] , à partir d' excel afin de générer une page word .