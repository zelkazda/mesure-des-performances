La commune se situe sur le littoral de la [MASK] [MASK] [MASK] [MASK] , la côte est hérissée de falaises de soixante-cinq mètres de hauteur .
[MASK] se trouve dans le [MASK] , à 6,5 kilomètres de [MASK] .
Couvrant 1229 hectares , son territoire est le plus étendu du [MASK] [MASK] [MASK] .
La commune prend le nom de [MASK] 1924 .
Au nord du village , à 500 mètres du littoral , les [MASK] avaient construit dès 1943 , une batterie côtière .
Celle-ci abritait quatre canons de marine bien protégés d' une portée de près de 20 km qui outre les navires au large , pouvaient aussi atteindre les plages du débarquement d' [MASK] [MASK] à l' ouest et de [MASK] [MASK] à l' est .
Outre des bombardements aériens la semaine précédente , la batterie subit un pilonnage depuis deux croiseurs anglais et du croiseur français [MASK] [MASK] empêchant les canons de longue portée allemands d' exercer une réelle menace .