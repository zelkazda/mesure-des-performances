C' est l' arbre typique de l' [MASK] [MASK] sèche et l' emblème du [MASK] .
Il est absent des forêts ombrophiles d' [MASK] [MASK] .
Depuis juillet 2008 , le fruit est autorisé à la vente par la [MASK] [MASK] .
Au [MASK] , le " lalo " est une poudre de feuilles de baobab séchées que l' on incorpore aux céréales ou aux sauces , notamment lors de la préparation du couscous de mil .