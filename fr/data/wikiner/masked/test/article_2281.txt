[MASK] est une série de jeu vidéo débutée par [MASK] [MASK] en 1989 pour le compte de la société [MASK] .
Alors que [MASK] [MASK] développe un jeu d' hélicoptère , il prend tellement de plaisir à imaginer les villes en arrière fond , qu' il a l' idée de développer [MASK] , ce jeu révolutionnaire à l' époque .
Le jeu a inspiré des développeurs qui ont programmé leur propre logiciel libre , notamment [MASK] et [MASK] .
[MASK] [MASK] est quant à lui propriétaire .