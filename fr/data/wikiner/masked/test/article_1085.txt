La [MASK] comptait 10666866 habitants au premier janvier 2008 , et 10584534 au premier janvier 2007 en augmentation de 73152 par rapport à la même date en 2006 et 82322 en 2007 .
Au 1 er janvier 2003 , 10355844 personnes précisément vivaient en [MASK] .
Le nombre de femmes dépasse celui des hommes en [MASK] , respectivement 5288959 et 5066885 .
La plus petite commune du pays reste [MASK] avec seulement 87 habitants .
Les communes qui comptent le moins d' habitants sont [MASK] ( 97 personnes ) , [MASK] ( 1336 personnes ) , [MASK] ( 1456 ) , [MASK] ( 1539 ) et [MASK] ( 1904 ) .
Ces chiffres montrent une dénatalité plus importante en [MASK] qu' ailleurs .
En effet , en 25 ans la [MASK] francophone a connu une baisse de 1179 naissances , contre 8585 en [MASK] .
Quant à [MASK] ( francophone à 94 % [ 1 ] ) , elle affiche un bond de non moins de 2972 naissances , soit une augmentation de près de 24 % .
Un moyen assez simple de connaître la répartition culturelle à [MASK] est de compter les cartes d' identité , lesquelles sont obligatoires sauf pour les enfants et libellées soit en français , soit en néerlandais .
La petite [MASK] [MASK] [MASK] [MASK] bénéficie de son autonomie au sein de la [MASK] [MASK] .
Ce flux d' immigrants est particulièrement important au niveau de la [MASK] [MASK] [MASK] où ce mouvement tend à s' accentuer depuis le début des années 2000 .