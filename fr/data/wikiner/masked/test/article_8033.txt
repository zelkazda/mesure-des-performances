[MASK] [MASK] [MASK] ( v. 1224 -- 24 décembre 1317 ) est un noble champenois et biographe de [MASK] [MASK] .
Lors de la croisade , [MASK] se mit au service du roi et devint son conseiller et son confident .
En 1250 , quand le roi et ses troupes furent capturés par les mamelouks à [MASK] , [MASK] , parmi les captifs , participa aux négociations et à la collecte de la rançon .
[MASK] se rapprocha probablement encore du roi dans les moments difficiles qui suivirent l' échec de la croisade .
Pendant les quatre années suivantes , passées en [MASK] [MASK] , [MASK] fut le conseiller très écouté du roi .
Celui-ci s' amusait des emportements , de la naïveté et des faiblesses de [MASK] , et il le reprenait parfois , mais il savait qu' il pouvait compter sur son absolu dévouement et sur sa franchise .
[MASK] refusa de le suivre , conscient de l' inefficacité de l' entreprise et convaincu que le devoir du roi était de ne pas quitter un royaume qui avait besoin de lui .
De fait , l' expédition fut un désastre et le roi mourut devant [MASK] le 25 août 1270 .
À partir de 1271 , la papauté mena une longue enquête au sujet de [MASK] [MASK] , qui aboutit à sa canonisation , prononcée en 1297 par [MASK] [MASK] .
Comme [MASK] avait été l' intime du roi , son conseiller et son confident , son témoignage en 1282 fut très précieux pour les enquêteurs ecclésiastiques .
C' est donc soit la date de l' achèvement de l' œuvre par [MASK] , soit la date de rédaction d' un manuscrit ayant servi de modèle à celui dont nous disposons .
Par divers recoupements , on peut également affirmer qu' un passage situé à l' extrême fin du livre , relatant un songe de [MASK] , n' a pu être écrit avant 1308 .
[MASK] a donc terminé son œuvre peu de temps avant de la remettre à [MASK] [MASK] [MASK] .
On n' a pas conservé le manuscrit qui fut offert à [MASK] [MASK] [MASK] .
Il est repris dans l' inventaire de 1373 de la bibliothèque de [MASK] [MASK] .
Cette copie resta dans la bibliothèque royale puis passa entre les mains de [MASK] [MASK] [MASK] , duc de [MASK] , avant d' atterrir à [MASK] , où on l' oublia .
Il ne fut redécouvert qu' en 1746 , à la prise de [MASK] par les troupes françaises .
Ce manuscrit , dit " de [MASK] " , est conservé à la [MASK] [MASK] [MASK] [MASK] .
La première page est décorée d' or et d' enluminures , et d' une peinture représentant l' écrivain présentant son livre à [MASK] [MASK] [MASK] .
Ce sont des transcriptions modernisées ( rajeunissement systématique de la langue ) d' un manuscrit antérieur au manuscrit de [MASK] .
[MASK] est un chevalier .
Mais [MASK] parle presque autant de lui-même que du roi , le sujet de son livre , mais il le fait d' une manière si naturelle qu' il ne donne jamais l' impression de vouloir se mettre en avant .
La première partie de l' ouvrage de [MASK] est consacrée aux saintes paroles du roi .
[MASK] rapporte les propos édifiants du roi et ses vertus chrétiennes .
Il existait une intimité entre le roi et ses proches ( familiers , confidents , conseillers , parmi lesquels figurent [MASK] et [MASK] [MASK] [MASK] ) qui s' exprimait particulièrement dans la conversation : le roi invite ses interlocuteurs à répondre à ses questions , souvent en vue de les instruire sur les plans moral et religieux .
Cette importance de la parole royale est particulièrement bien rendue par [MASK] , qui fait très souvent parler ses personnages .
Et [MASK] ne fait jamais tenir de longs discours monologués à ses personnages : les enseignements découlent toujours du dialogue .
Pour [MASK] , [MASK] [MASK] incarnait l' idéal du prud'homme , à la fois pieux , courageux , bon , intelligent et sage , un homme qui défend la foi chrétienne par son courage .
Sous certains aspects , [MASK] n' est parfois pas très loin de l' hagiographie .
[MASK] , comme son roi , était manifestement très attaché à la religion chrétienne , à ses doctrines , à sa morale et à ses pratiques .
Le christianisme de [MASK] est plus terre à terre , plus proche de celui du commun des mortels .
Si [MASK] ne fait pas œuvre d' historien , il est cependant tout à fait sincère .
Mais le recul du temps et , surtout , le désintéressement de [MASK] et sa naïve rudesse donnent à ses souvenirs une exceptionnelle valeur .