[MASK] [MASK] est issue d' une famille modeste -- sa mère , aide-soignante élevait seule six enfants .
Elle crée le parti [MASK] ( " éventail " en amérindien ) en 1993 et en devient la présidente .
La même année , elle est élue députée " non inscrite " de [MASK] et est réélue en 1997 .
L' année suivante , elle est quatrième de la liste [MASK] [MASK] menée par [MASK] [MASK] .
À sa réélection en juin 1997 , elle rallie le groupe socialiste , et se voit confier par [MASK] [MASK] un rapport sur la recherche de l' or en [MASK] .
Selon certains socialistes , cette candidature aurait contribué à l' éparpillement des voix " de gauche " et aurait été une des causes de l' échec de [MASK] [MASK] à accéder au second tour de l' élection présidentielle .
Mais d' après d' autres observateurs , comme [MASK] [MASK] , [MASK] [MASK] avait proposé une alliance à [MASK] [MASK] , qui n' aurait pas donné suite à cette demande .
Le 20 janvier 2007 , [MASK] [MASK] rallie l' équipe de [MASK] [MASK] où elle est nommée " déléguée à l' expression républicaine " .
Par la suite , lors de la campagne des législatives de juin 2007 , elle déclare avoir été " approchée " par l' entourage de [MASK] [MASK] " avant la fin de la présidentielle " pour faire partie du gouvernement , mais " avoir alors décliné l' offre " .
Elle est apparentée au groupe [MASK] , radical , citoyen et divers gauche .
Arrivée en tête des quatre listes de gauche en présence mais largement derrière le candidat de la majorité présidentielle , [MASK] [MASK] , elle conduit une liste d' union de la gauche au second tour .
Le 21 mars 2010 , elle est battue par la liste du maire de [MASK] , [MASK] [MASK] , soutenue par l' [MASK] , qui réalise 56,11 % de suffrages contre 43,89 pour la liste de [MASK] [MASK] .
[MASK] [MASK] a donné son nom à la loi française , votée le 10 mai 2001 , qui reconnaît comme crimes contre l' humanité la traite négrière transatlantique et l' esclavage qui en a résulté .
[MASK] [MASK] n' a pas parlé de la collaboration des ethnies africaines à la traite atlantique .
En 2005 , elle prend position pour le " Non " lors du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , contrairement au [MASK] , dont elle est à l' époque encore vice-présidente .