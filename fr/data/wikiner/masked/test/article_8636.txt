Selon la légende , [MASK] est considéré comme le fondateur de la dynastie des rois troyens .
La ville elle-même est fondée par son fils [MASK] .
[MASK] , le fils d' [MASK] , lui succède sur le trône .
[MASK] et [MASK] , punis par [MASK] , ont bâti pour ce roi cruel les murs de [MASK] , mais n' ont finalement pas reçu le salaire promis et , offensés par le roi , qui les menace de leur couper les oreilles , ils se vengent .
[MASK] envoie une épidémie de peste , et [MASK] ordonne à un monstre marin de dévorer les habitants et de dévaster les champs en vomissant de l' eau de mer .
[MASK] brise ses chaînes et offre de tuer le monstre marin en échange de deux chevaux blancs immortels , que [MASK] avait offert à [MASK] pour le prix de l' enlèvement de [MASK] .
Les [MASK] construisent alors un haut mur à quelque distance du rivage .
Lorsque le monstre atteignit le mur , il ouvrit ses énormes mâchoires , et [MASK] s' engagea armé dans la gorge du monstre .
[MASK] aurait alors trompé [MASK] en substituant deux chevaux ordinaires aux chevaux immortels promis .
[MASK] s' embarque très en colère après avoir menacé de mener la guerre contre [MASK] .
[MASK] débarque près de [MASK] , en confiant la garde des navires à [MASK] .
[MASK] envoya le peuple équipé d' épées et de torches brûler les navires d' [MASK] , mais [MASK] résista jusqu' à son dernier souffle et permit à ceux-ci de reprendre la mer .
[MASK] ordonna l' assaut immédiat de la ville , et ce fut [MASK] qui réussit à créer une brèche dans la muraille et à pénétrer dans la ville .
L' origine de la [MASK] [MASK] [MASK] est l' enlèvement par [MASK] , prince troyen , d' [MASK] , épouse de [MASK] , roi de [MASK] .
Pour punir les [MASK] , les rois grecs se coalisent et mettent le siège devant la cité .
Selon [MASK] [MASK] , [MASK] était située sur les deux sources du [MASK] , l' une dégageant des vapeurs chaudes , et l' autre glacée .
De plus , le géographe romain [MASK] déclara que le site véritable se trouvait à 5,6 km de là , au " village des [MASK] " .
Le site d' [MASK] est aujourd'hui reconnu sous le nom de " [MASK] [MASK] [MASK] [MASK] " par l' [MASK] , qui l' a inscrit sur la liste de son patrimoine mondial en 1998 .
Selon la mythologie comparée , consistant à comparer des récits et des mythes , notamment à l' intérieur d' une même sphère socio-linguistique -- suivant [MASK] [MASK] , la [MASK] [MASK] [MASK] n' a pas eu lieu .