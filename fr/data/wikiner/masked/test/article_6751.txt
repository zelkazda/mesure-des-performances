[MASK] [MASK] ( 3 avril ( ? )
[MASK] naît au château de [MASK] ( dans le comté de [MASK] ) .
Leur fils [MASK] , futur [MASK] [MASK] [MASK] [MASK] , naît à [MASK] en 1387 .
Il effectue un pèlerinage à [MASK] en 1393 .
En 1399 , il débarque secrètement à [MASK] dans le [MASK] et vainc puis capture le roi [MASK] [MASK] , contraint d' abdiquer .
Le parlement reconnaît aussitôt son avènement , sous le nom de [MASK] [MASK] .
Son fils , le futur [MASK] [MASK] , se révolte en 1411 contre lui ( révolte manquée ) .
Il meurt ( peut-être de la lèpre ) à l' [MASK] [MASK] [MASK] , mais il est enterré à la [MASK] [MASK] [MASK] .