Il est limitrophe du [MASK] [MASK] et de la [MASK] , dont il faisait autrefois partie .
Un groupe d' indépendantistes discrètement soutenus par les [MASK] déclara l' indépendance de [MASK] de la [MASK] le 3 novembre 1903 et la souveraineté des [MASK] sur le [MASK] [MASK] [MASK] ( dont la construction fut commencée en 1881 sous l' impulsion de [MASK] [MASK] [MASK] et inauguré en 1914 ) , par un accord à perpétuité signé la même année .
À sa mort en 1981 , le général [MASK] [MASK] , commandant la garde nationale ( qui fut à une période rémunéré par la [MASK] ) , s' est mis , dans les faits , à contrôler le pouvoir en accentuant le caractère populiste , nationaliste et très anti-américain de sa politique .
En juillet 1987 , les [MASK] ont tenté d' obtenir l' extradition de [MASK] , pour trafic de drogue , puis ont soumis le pays à des sanctions économiques .
[MASK] , en dépit d' une opposition civile vigoureuse , s' est maintenu au pouvoir avec l' aide du [MASK] intéressé par la construction d' un nouveau canal , jusqu' à l' [MASK] [MASK] [MASK] lancée en décembre 1989 par l' armée américaine pour le capturer .
Le [MASK] [MASK] [MASK] fut rendu au [MASK] le 31 décembre 1999 , suite aux [MASK] [MASK] [MASK] .
Le [MASK] est une république parlementaire .
Le [MASK] est divisé en neuf provinces et cinq comarques ( territoires autonomes indigènes ) .
Les neuf provinces sont : [MASK] [MASK] [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] [MASK] , [MASK] et [MASK] .
Les cinq comarques sont : [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] et [MASK] [MASK] .
Le [MASK] est un pays d' [MASK] [MASK] , entouré par la [MASK] [MASK] [MASK] , l' [MASK] [MASK] , la [MASK] et le [MASK] [MASK] .
Le projet est alors une initiative française après le succès du [MASK] [MASK] [MASK] .
Les [MASK] exploitèrent ensuite les droits du canal ( sur dix miles ) avec le traité de [MASK] et malgré l' indépendance du [MASK] signé avec la [MASK] en 1903 .
En effet les travaux ont repris grâce à la pression américaine qui a aussi contribué à la révolte du [MASK] envers cette même [MASK] .
En 1978 , le [MASK] a repris le contrôle de la concession sous les interventions multiples du général [MASK] [MASK] au pouvoir .
Le [MASK] possède la plus puissante économie d' [MASK] [MASK] .
Le pays doit principalement son essor économique au [MASK] [MASK] [MASK] .
La [MASK] [MASK] [MASK] est le plus important centre financier d' [MASK] [MASK] .
[MASK] fait partie des pavillons de complaisance .
Cela peut aussi influer sur la région où les investisseurs pourront trouver des conditions avantageuses à leur installation , de trouver des taux d' impositions faibles et pourquoi pas profiter d' une zone franche comme on en trouve à la frontière sud des [MASK] .
Le [MASK] compte de nombreuses danses d' origine africaine .
Le [MASK] a pour codes :