[MASK] est en effet un romancier d' une fécondité exceptionnelle : on lui doit 192 romans , 158 nouvelles , plusieurs œuvres autobiographiques et de nombreux articles et reportages publiés sous son propre nom et 176 romans , des dizaines de nouvelles , contes galants et articles parus sous 27 pseudonymes .
[MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] sont parmi les premiers hommes de lettres à le reconnaître comme un grand écrivain .
[MASK] [MASK] , fasciné par la créativité de [MASK] [MASK] qu' il avait souhaité rencontrer dès son succès policier , le questionna à maintes reprises , échangea une correspondance quasi-hebdomadaire pour poursuivre les méandres créatifs de cet écrivain populaire et prit la surprenante manie d' annoter en marge tous ces romans pour conclure en 1941 : " [MASK] est un romancier de génie et le plus vraiment romancier que nous ayons dans notre littérature d' aujourd'hui " .
[MASK] est né au 2 e étage du " 26 ( aujourd'hui 24 ) [MASK] [MASK] " à [MASK] .
La famille [MASK] est originaire du [MASK] belge , une région de basses terres proches de la [MASK] , carrefour entre la [MASK] , la [MASK] et les [MASK] ( voir aussi [MASK] [MASK] ) .
La famille de sa mère est aussi originaire du [MASK] , mais du côté hollandais ; plat pays de terres humides et de brumes , de canaux et de fermes .
Le [MASK] apparaît aussi .
[MASK] a logé quelques semaines à [MASK] , notamment dans une maison qui lui inspira le roman [MASK] [MASK] [MASK] [MASK] .
Cette période journalistique fut pour le jeune [MASK] , juste âgé de seize ans , une extraordinaire expérience qui lui permet d' explorer les dessous de la vie d' une grande ville , les dessous de la politique , mais aussi de la criminalité , de fréquenter et de pénétrer la vie nocturne réelle , de connaître les dérives dans les bars et les maisons de passe ; elle lui permet aussi d' apprendre à rédiger de façon efficace .
Il écrira plus de 150 articles sous le pseudonyme " [MASK] [MASK] " .
Durant cette période , il s' intéresse particulièrement aux enquêtes policières et assiste aux conférences sur la police scientifique données par le criminaliste français , [MASK] [MASK] .
Las , [MASK] découvre que cette protection ne comporte que des menus services de portefaix et de manutention sommaires mal rétribués , même si son protecteur présomptueux s' est engagé à le présenter à des cercles littéraires .
Disposant des meubles de l' épousée qui a d' ailleurs plus de ressources financières que le mari , le couple emménage à [MASK] .
Avec son épouse , [MASK] [MASK] approfondit sa connaissance des arts .
Il est attiré par la gravure et la sculpture , il poursuit inlassablement sa découverte de la peinture impressionniste commencée à [MASK] .
Alors que le jeune homme intelligent pénètre les arcanes de l' aristocratie française en déclin , tant en campagne qu' à [MASK] , ces premières tentatives littéraires l' amènent à fréquenter le milieu des lettres et des journalistes littéraires .
Et l' écrivain des années cinquante de suggérer tacitement la sévérité et la cruauté de la patronne [MASK] , refusant fermement toute chance d' édition au jeune écrivain raté .
C' est mu par ses expériences que [MASK] [MASK] simplifie radicalement son écriture , et observe avec rigueur le fonctionnement de l' écriture commerciale selon les genres : littérature enfantine d' aventures et de combats , écrits de cœur pour midinettes , histoires sensuelles pour dactylos , drames effrayants pour concierges , historiettes de gare pour voyageurs , écrits érotiques ou licences pornographiques ..
Il rencontre avec plus d' attention le petit peuple parisien d' artisans besogneux , de concierges acariâtres et de pauvres types à la double vie d' autant plus que dès l' été 1924 , le fructueux labeur du couple lui donne accès à un bel appartement [MASK] [MASK] [MASK] .
Après une longue croisière en [MASK] , il s' embarque pour un tour du monde en 1934 et 1935 .
[MASK] est touché par la crise économique , le chômage , la fermeture de charbonnages , de verreries et d' industries métallurgiques .
A [MASK] , [MASK] va faire ce qu' il toujours au cours de sa carrière de romancier : mettre en mémoire un décor , dramatique ou paisible , qu' il réutilisera , peut-être des années plus tard , comme cadre spatial d' un roman .
Pour rendre vrai son roman , [MASK] va introduire son personnage dans les décors qu' il a vus , photographiés et mémorisés dans la ville belge .
Dans l' œuvre de [MASK] , trente-quatre romans et nouvelles se situent ou évoquent la ville de [MASK] [MASK] .
Pendant toute la guerre , entre 1940 et 1945 , [MASK] a continué à vivre en [MASK] , mais cette période , assez mal connue , est sujette à de multiples soupçons .
Non seulement son frère fut volontaire auprès de la [MASK] [MASK] , mais de plus , selon certaines personnes , lors de cette période cruciale de sa vie et de son œuvre , l' écrivain aurait été un collaborateur -- comme le dit ambigument [MASK] [MASK] dans sa biographie consacrée à [MASK] .
En 1944 , une dépêche de l' [MASK] , retrouvée à [MASK] , mentionne sa dénonciation pour " intelligence avec l' ennemi " par " certains villageois [MASK] exaspérés par la conduite égoïste de cet écrivain affichant l' opulence de son train de vie , à l' époque des tickets d' alimentation " .
D' autre part , la " [MASK] a soupçonné [MASK] d' être juif , tablant sur une confusion entre [MASK] et [MASK] , patronyme d' origine israélite " .
Lors de cette période , [MASK] , qui n' est plus libre de ses mouvements , écrit énormément , vingt romans dont seulement trois [MASK] .
Parmi eux de nombreux chefs-d'œuvre et paradoxalement , dans l' intrigue de ses romans , la grande présente c' est la [MASK] , décrite comme " une région lumineuse , impressionniste , où la mer rejoint la terre .
Un plat pays " comme une lointaine nostalgie de son [MASK] familial .
La vision ambiguë que [MASK] avait de la région et de la bourgeoisie locale a quelquefois offusqué ses habitants .
[MASK] passe donc la [MASK] [MASK] [MASK] en [MASK] et entretient une correspondance avec [MASK] [MASK] .
Il y écrivit trois romans dont " [MASK] [MASK] [MASK] [MASK] " .
Afin d' assouvir sa curiosité et son appétit de vivre , il visite intensément [MASK] [MASK] , la [MASK] , l' [MASK] , la [MASK] et toute la côte est , des milliers de miles , de motels , de routes et de paysages grandioses .
Dès cette époque , les étudiants en langue française des universités américaines commencent à étudier l' œuvre de [MASK] .
En 1952 , il est reçu à l' [MASK] [MASK] [MASK] [MASK] , et revient définitivement en [MASK] en 1955 .
Après une période mouvementée sur la [MASK] [MASK] [MASK] à côtoyer la jet-set , il finit par s' installer en [MASK] à [MASK] au nord de [MASK] , où il se fait construire une gigantesque maison .
À 86 ans , [MASK] [MASK] s' éteint à son domicile lausannois à l' aube du 4 septembre 1989 ; son corps est incinéré le 6 .
À la différence de beaucoup d' auteurs d' aujourd'hui qui essayent de construire une intrigue la plus complexe possible , comme un jeu d' échecs , [MASK] propose au final une intrigue simple , mais un décor et des personnages forts , un héros attachant d' humanité , obligé d' aller au bout de lui-même , de sa logique .
Le message de [MASK] est complexe et ambigu : ni coupables , ni innocents mais des culpabilités qui s' engendrent et se détruisent dans une chaîne sans fin .
Il avait aussi précisé que [MASK] était meilleur dans la peinture des états que dans celle des actions , définissant son univers comme statique .
Il a fui [MASK] pour des raisons étrangères à cela et c' est [MASK] qui l' a consacré où , au départ , il n' a écrit que ce qui était considéré comme de la paralittérature pour gagner sa vie , ce qui ne le disposait pas à être reconnu immédiatement par la critique belge .
Cette entrée dans la célèbre collection est une consécration de l' œuvre de [MASK] , présentée notamment par l' un de ses meilleurs connaisseurs , le professeur liégeois [MASK] [MASK] .
On a longtemps cru que [MASK] [MASK] était un pseudonyme de [MASK] .
Voici comment [MASK] le décrit :
L' univers de [MASK] est relativement statique , mais cela n' a jamais découragé les réalisateurs de cinéma , pourtant " art du mouvement " , à porter sur grand écran son œuvre .
Il fut le premier romancier contemporain à être adapté dès le début du parlant avec [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] , parus en 1931 et portés à l' écran dès 1932 .
Mais au final , les réussites sont assez rares , car entre la fidélité décevante et la trahison féconde , la ligne de partage est étroite , de nombreux réalisateurs ( et des plus prestigieux : [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] … ) s' y sont essayés avec plus ou moins de succès .
[MASK] [MASK] et [MASK] étaient très amis et l' acteur a tourné un total de dix films adaptés de [MASK] , dans lesquels il a su presque faire oublier son passé cinématographique et ses très nombreux rôles de mauvais garçon .