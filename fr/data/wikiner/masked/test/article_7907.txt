Il poursuit ses études au [MASK] [MASK] à [MASK] et est admis en 1831 à l' [MASK] [MASK] dont il sort deux ans après comme ingénieur des tabacs .
Il travaillera d' abord au laboratoire de chimie de [MASK] .
En 1839 , il présenta à l' [MASK] [MASK] [MASK] son premier mémoire sur les variations séculaires des orbites des planètes .
[MASK] [MASK] [MASK] devient célèbre lorsque la planète , dont il a calculé les caractéristiques comme cause hypothétique des anomalies des mouvements d' [MASK] , est effectivement observée , par l' astronome allemand [MASK] [MASK] à l' [MASK] [MASK] [MASK] , le 23 septembre 1846 .
On baptisera [MASK] cette nouvelle planète , malgré la proposition qui fut faite de la baptiser [MASK] [MASK] .
La planète [MASK] , découverte par [MASK] [MASK] en 1781 , présentait en effet des irrégularités par rapport à l' orbite qu' elle aurait dû avoir suivant la loi de la gravitation universelle d' [MASK] [MASK] .
Encouragé par [MASK] [MASK] , [MASK] [MASK] se lance en 1844 dans le calcul des caractéristiques de cette nouvelle planète ( masse , orbite , position actuelle ) , dont il communiquera les résultats à [MASK] [MASK] [MASK] [MASK] le 31 août 1846 .
Ils seront confirmés ( à peu de choses près ) par [MASK] [MASK] , qui observa le nouvel astre le jour même où il reçut en courrier sa position par [MASK] [MASK] .
[MASK] [MASK] vit le nouvel astre au bout de sa plume " .
Cette découverte sera le sujet de nombreuses polémiques à l' époque , puisque ces calculs ont été menés en même temps par [MASK] [MASK] mais sans qu' aucun d' eux ne connaisse les travaux de l' autre .
Les caractéristiques de la planète avaient été déterminées par [MASK] un an plus tôt mais n' avaient pas été publiées .
[MASK] [MASK] lui succédera en 1878 .
Plus tard , [MASK] [MASK] tenta de répéter le même exploit pour expliquer les perturbations de [MASK] .
Ces prédictions se révéleront inexactes , et ces anomalies seront expliquées un demi-siècle plus tard par [MASK] [MASK] avec la théorie de la relativité générale .
Le 14 novembre 1854 , une terrible tempête , survenant sans la moindre alerte , traverse l' [MASK] d' ouest en est , causant la perte de 41 navires dans la [MASK] [MASK] .
[MASK] [MASK] entreprend alors de mettre en place un réseau d' observatoires météorologiques sur le territoire français , destiné avant tout aux marins afin de les prévenir de l' arrivée des tempêtes .
Ce réseau regroupe 24 stations dont 13 reliées par télégraphe , puis s' étendra à 59 observatoires répartis sur l' ensemble de l' [MASK] en 1865 .
À la tête d' une commission qui porta son nom , il réforma l' enseignement de l' [MASK] [MASK] en introduisant plus de science appliquée .
Élu député de la [MASK] en 1849 , il deviendra sénateur et inspecteur général de l' enseignement supérieur pour les sciences à partir de janvier 1852 .
En 1852 , il est élu conseiller général du [MASK] [MASK] [MASK] .
Il proposa de revoir à la baisse la distance [MASK] -- [MASK] et la vitesse de la lumière .