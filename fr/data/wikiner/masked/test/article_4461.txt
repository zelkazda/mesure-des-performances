Le manoir aurait été celui de [MASK] [MASK] [MASK] [MASK] [MASK] , gouverneur de la [MASK] et sans doute amant de [MASK] [MASK] , mère de [MASK] .
Le registre des naissances d' [MASK] aurait subi le même sort à la même date , celle de la naissance présumée du futur empereur .
Certains auteurs pensent que le prénom " [MASK] " aurait été donné à l' enfant en référence au [MASK] [MASK] [MASK] voire à [MASK] lui-même .
De retour à [MASK] , [MASK] aurait donné une consonance plus française à ce curieux prénom en le transformant en [MASK] .
La commune de [MASK] est située à proximité de le la ville de [MASK] , sous-préfecture du département du [MASK] .
Les commerces y sont rares et un seul bar-tabac-épicerie ( chez [MASK] ) est proposé aux habitants de la commune .
Un club de football est aussi présent dans [MASK] .