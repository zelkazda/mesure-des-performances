Il débute au cinéma en 1967 avec [MASK] [MASK] [MASK] d' [MASK] [MASK] .
Il retrouve [MASK] [MASK] pour tourner [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , ainsi que sa suite , [MASK] [MASK] [MASK] [MASK] [MASK] , tous deux scénarisés par [MASK] [MASK] .
Ce dernier lui confie ensuite le premier rôle de son premier long-métrage en tant que réalisateur , [MASK] [MASK] .
[MASK] et [MASK] entament alors , au début des années 1980 , une collaboration fructueuse qui verra naître [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] , trois comédies à succès où [MASK] partage l' affiche avec [MASK] [MASK] .
Il revient à la réalisation avec [MASK] [MASK] [MASK] [MASK] en 1991 et [MASK] [MASK] [MASK] [MASK] en 1997 .
En 2005 , [MASK] [MASK] est président du jury du [MASK] [MASK] [MASK] [MASK] .
Il revient au cinéma en 2009 avec deux films : [MASK] , où il joue son propre rôle , et [MASK] , dans lequel il a le rôle-titre .
[MASK] [MASK] a joué dans tous les films qu' il a réalisés .