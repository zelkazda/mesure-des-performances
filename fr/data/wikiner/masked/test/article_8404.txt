Ce tabou , considéré par l' ethnologue [MASK] [MASK] comme un universel présent dans toute société , prend cependant différentes formes selon les formations sociales .
Ce texte , présenté par la députée [MASK] [MASK] ( [MASK] ) , prévoit l' inscription de la notion d' inceste dans le code pénal et dispose que les viols et agressions sont qualifiés d' incestueux lorsqu' ils sont commis " au sein de la famille sur la personne d' un mineur par un ascendant , un frère , une soeur ou par toute autre personne , y compris s' il s' agit d' un concubin d' un membre de la famille , ayant sur la victime une autorité de droit ou de fait " .
L' article 213 du code pénal [MASK] ( livre deuxième , titre sixième ) condamne clairement l' inceste en ces termes :
Depuis l' [MASK] [MASK] et encore récemment dans certains pays comme le [MASK] pour la famille des [MASK] , il était fréquent , dans la noblesse , de se marier et d' avoir des enfants avec un membre plus ou moins éloigné de sa famille .
Cette tradition disparaît peu à peu : l' [MASK] [MASK] [MASK] [MASK] est le premier de sa dynastie à être marié à une femme ne faisant pas partie de sa famille .
Dans la [MASK] [MASK] , la violation du serment de chasteté par les vestales était taxé d ' incestus et , considéré comme un crime inexpiable , généralement puni par la mort de la coupable , condamnée à être enterrée vivante .
Ainsi , bien que présenté comme une exigence du peuple de [MASK] , le remariage de l' empereur romain [MASK] avec sa nièce [MASK] [MASK] [MASK] était clairement considéré comme incestueux .
Dans la [MASK] , la prohibition de l' inceste est longuement détaillée au chapitre 18 du [MASK] ( parasha [MASK] ) .
L' inceste est traité dans le [MASK] avec les deux autres interdits : l' idolâtrie et le meurtre .
Avec [MASK] , on inscrit l' interdiction de l' inceste directement dans l' ordre du désir et de la loi .
[MASK] [MASK] y voit l' articulation entre nature et culture , le fondement social .
Malgré l' interdit qui l' accompagne et que [MASK] croyait universel , l' inceste reste un phénomène non marginal .