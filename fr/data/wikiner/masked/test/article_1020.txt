Quand il apprend ses fiançailles avec [MASK] [MASK] , [MASK] lui annonce son intention de quitter définitivement l' [MASK] .
[MASK] s' installe à [MASK] en 1879 : séduisant , raffiné et subtil , il est fêté dans toute l' [MASK] .
Il s' installe quelque temps à [MASK] .
En 1886 , il rencontre [MASK] [MASK] qui devient son amant et sera plus tard son exécuteur testamentaire .
La parution en 1890 du [MASK] [MASK] [MASK] [MASK] marque le début d' une célébrité littéraire .
[MASK] décide alors de lui intenter un procès pour diffamation , qu' il perd .
Le marquis se retourne contre [MASK] .
C' est le premier des procès intentés contre [MASK] .
[MASK] joue tout d' abord de son charme habituel , de son inégalable sens de la repartie , déclenchant l' hilarité du public , transformant par moment le tribunal en salle de théâtre .
Il séjourne dans différentes prisons dont la geôle de [MASK] .
[MASK] était par ailleurs le grand-oncle de [MASK] .
Commence alors une période de déchéance dont il ne sortira pas et , malgré l' aide de ses amis , notamment [MASK] [MASK] , il finit ses jours dans la solitude et la misère .
[MASK] [MASK] meurt d' une méningite , âgé de 46 ans , en exil volontaire à [MASK] , le 30 novembre 1900 .
Le critique , selon [MASK] , ne doit considérer l' œuvre littéraire que comme " un point de départ pour une nouvelle création " , et non pas tenter d' en révéler , par l' analyse , un hypothétique sens caché .
La critique elle-même doit se faire œuvre d' art , et ne peut dès lors se réaliser que dans le subjectif ; à cet égard , dit [MASK] , la critique est la " forme la plus pure de l' expression personnelle " .
La théorie critique de [MASK] a été très influencée par les œuvres de [MASK] [MASK] .
L' intuition de [MASK] , fortement influencée par les écrivains français de son temps qu' il lisait dans le texte , était que dans la forme même,gît le sens et le secret de tout art .