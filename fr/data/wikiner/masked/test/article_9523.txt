Selon la légende , [MASK] fut fondée en 1209 par des universitaires fuyant [MASK] après un différend avec les dirigeants locaux .
Toutefois , le collège le plus ancien encore existant à [MASK] est [MASK] , fondé en 1284 .
Ce fut en 1318 que l' université obtint du pape [MASK] [MASK] le droit de délivrer des diplômes .
Les deux universités sont membres du [MASK] [MASK] [MASK] [MASK] ( un réseau de grandes universités de recherche britanniques ) , du [MASK] [MASK] [MASK] ( une association des plus grandes universités européennes ) et de la [MASK] ( [MASK] [MASK] [MASK] [MASK] [MASK] ) .
Ce système est spécifique à [MASK] et [MASK] ( où il prend le nom de tutorial .
Le collège le plus récent est [MASK] , construit dans les années 1970 .
En 2004 , certains journaux ont annoncé que l' université étudiait la possibilité d' ajouter 3 nouveaux collèges , mais l' information a été démentie par [MASK] .
Ce changement oriente l' université loin de la loi canonique et davantage vers les classiques , la [MASK] et les mathématiques .
Cependant , quelques étudiants célèbres comme [MASK] n' aimaient pas le système , pensant que les étudiants étaient plus intéressés par l' accumulation d' honneurs et de prix plutôt que par le sujet lui-même .
Malgré la diversification de sujets de recherche et d' enseignement , [MASK] garde sa prédominance mathématique .
Les premiers collèges pour femmes sont [MASK] , créé en 1869 , et [MASK] , créé en 1872 .
Les premières étudiantes ont passé leurs examens en 1882 , mais c' est seulement en 1947 , 20 ans après [MASK] , que les femmes ont été considérées comme des membres à part entière de l' université .
L' [MASK] [MASK] [MASK] possède une bibliothèque de plus de 7 millions de volumes , comprenant de nombreuses collections remarquables et documents anciens .
C' est une des cinq bibliothèques de dépôt légal du [MASK] .
À [MASK] , comme dans toutes les autres universités , les étudiants sont en effet classés en 4 classes ( 1st , 2:1 , 2:2 et 3rd ) .
Par exemple , l' aviron ( sport ) est un sport populaire et il existe des compétitions entre collèges ( les bumps races ou courses de bosses ) et contre [MASK] ( la [MASK] [MASK] qui oppose tous les ans sur la [MASK] à [MASK] les meilleurs rameurs des deux universités ) .
Il existe de nombreux mythes associés à l' [MASK] [MASK] [MASK] et son histoire , certains devant être considérés moins sérieusement que d' autres .
L' un des mythes les plus célèbres est lié au [MASK] [MASK] ( pont des mathématiques ) du [MASK] , qui aurait été construit par [MASK] [MASK] avec pour objectif qu' il tienne sans vis ni boulons .
Cette histoire n' est pas véridique , dans la mesure où le pont a été construit 22 ans après la mort de [MASK] .
Elle est maintenant exposée au [MASK] [MASK] [MASK] [MASK] .
L' examen de plus haut niveau est le [MASK] [MASK] [MASK] [MASK] [MASK] .