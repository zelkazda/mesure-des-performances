Au total des nominations , il arrive deuxième derrière [MASK] [MASK] ( 16 nominations et deux victoires ) .
Il arrive à égalité avec [MASK] [MASK] pour le plus grand nombre de prix obtenus par un acteur ( à savoir trois ) , ainsi qu' avec [MASK] [MASK] pour le nombre et le type de trophées remportés ( deux pour le meilleur premier rôle et un en tant que meilleur second rôle ) .
Dans ce classement , il reste néanmoins derrière [MASK] [MASK] au vu du nombre total de ses victoires .
Dans sa vie privée adulte , [MASK] a été connu pour son incapacité à " se fixer " .
Il eut aussi des liaisons avec de nombreuses actrices et top models ( tel que [MASK] [MASK] [MASK] , de trente ans sa cadette ) .
Sa plus longue liaison dura 17 ans avec l' actrice [MASK] [MASK] , la fille du légendaire réalisateur [MASK] [MASK] .
[MASK] a débuté sa carrière en tant qu' acteur , scénariste et producteur , travaillant pour et avec [MASK] [MASK] , parmi d' autres .
Lors des années 1960 , les rôles n' étant pas toujours faciles à trouver , [MASK] se mit à écrire plus souvent .
Ces films n' ont pas eu vraiment de succès , mais le jeune [MASK] travaillait régulièrement .
Alors que sa carrière d' acteur ne semble mener nulle part , [MASK] se résigne à une carrière derrière la caméra en tant que scénariste/réalisateur .
Son premier véritable succès en tant que scénariste vient en 1967 avec [MASK] [MASK] , film sous acide , interprété par [MASK] [MASK] et [MASK] [MASK] .
Une nomination pour le meilleur acteur suit l' année suivante pour son rôle dans [MASK] [MASK] [MASK] ( 1970 ) .
En plus de ces films , citons d' autres de ses premiers rôles les plus notables : [MASK] [MASK] [MASK] de [MASK] [MASK] ( 1973 ) , qui lui valut un [MASK] [MASK] [MASK] à [MASK] et le film noir de [MASK] [MASK] , [MASK] ( 1974 ) .
[MASK] a aussi joué dans le film sur le groupe [MASK] [MASK] et réalisé par [MASK] [MASK] , [MASK] ( 1975 ) , et la même année dans le film de [MASK] [MASK] , [MASK] [MASK] [MASK] , considéré par beaucoup comme l' un de ses rôles les plus mémorables et les moins connus .
[MASK] a été très prolifique durant les années 1980 , jouant dans des films tels que [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( 1981 ) , [MASK] ( 1981 ) , [MASK] [MASK] [MASK] [MASK] ( 1985 ) , [MASK] [MASK] [MASK] [MASK] ( 1987 ) , et [MASK] ( 1987 ) .
Le film [MASK] de 1989 , où [MASK] tient le rôle du [MASK] , fut un succès commercial international et , grâce à un arrangement qui lui permit d' obtenir un pourcentage lucratif , [MASK] gagna environ 60 millions de dollars .
Toutes les performances de [MASK] n' ont pas été bien reçues .
Dans [MASK] [MASK] ( 2002 ) , [MASK] interprète un actuaire d' [MASK] qui s' interroge sur sa vie et sur la mort de sa femme .
Dans la comédie [MASK] [MASK] ( 2003 ) , il joue un thérapeute agressif désigné pour aider le pacifiste convaincu [MASK] [MASK] .
En novembre 2006 , [MASK] a joué dans le nouveau film de [MASK] [MASK] , [MASK] [MASK] [MASK] qui met en scène [MASK] et [MASK] [MASK] dans le rôle de deux mourants qui veulent réaliser leur liste de buts .
Le film , sorti en 2008 , se titre en français [MASK] [MASK] [MASK] .