Après l' échec de [MASK] [MASK] pour reconquérir l' [MASK] suite à la victoire du [MASK] [MASK] à la [MASK] [MASK] [MASK] [MASK] [MASK] le 7 mai 1954 , les [MASK] [MASK] [MASK] divisèrent le pays en deux par une zone démilitarisée au niveau du 17 e parallèle .
Au nord , la [MASK] [MASK] [MASK] [MASK] [MASK] , régime communiste fondé par [MASK] [MASK] [MASK] en septembre 1945 .
[MASK] [MASK] [MASK] et ses alliés s' opposèrent à la tenue des élections de réunification initialement prévues au plus tard à l' été 1956 par les [MASK] [MASK] [MASK] .
Après quinze ans de combats ( entre 1957 et 1972 ) et un lourd bilan humain , l' intervention directe et massive des [MASK] prit fin avec la signature des [MASK] [MASK] [MASK] [MASK] [MASK] en 1973 .
Considérée comme la première défaite militaire de l' histoire des [MASK] , cette guerre impliqua plus de 3,5 millions de jeunes américains envoyés au front entre 1965 et 1972 .
En 1945 , la reconquête de l' [MASK] par [MASK] [MASK] puis , l' échec des gouvernements vietnamien et français à s' accorder sur un modus vivendi , conduisit en décembre1946 à la [MASK] [MASK] [MASK] [MASK] .
La bataille décisive eut lieu au printemps 1954 avec la [MASK] [MASK] [MASK] [MASK] [MASK] .
À cet accord était également signée une déclaration commune par les neuf participants , sauf par les [MASK] et par l' [MASK] [MASK] [MASK] [MASK] .
L' indépendance du [MASK] , du [MASK] et du [MASK] [MASK] était reconnue .
Avec le soutien du président américain [MASK] , son gouvernement refusait les élections générales initialement prévues .
De fait , dès août 1955 , [MASK] [MASK] [MASK] déclarait que son pays ne se considérait lié en aucune façon par les [MASK] [MASK] [MASK] dont il n' avait pas été signataire .
Les [MASK] ajoutaient ne pas reconnaitre la [MASK] , déclarant également ne pas avoir été signataire des accords .
Le 20 janvier 1961 , le président [MASK] débutait son mandat et confirmait l' interventionnisme américain en portant à 15000 hommes l' effectif des conseillers militaires .
Le 4 janvier 1962 étaient signés entre les [MASK] et le gouvernement de [MASK] [MASK] [MASK] , des accords dont les principaux points portaient sur la pacification , la démocratisation et la libéralisation .
En février , un accord était conclu entre la [MASK] et le [MASK] [MASK] au [MASK] pour maintenir la [MASK] [MASK] ouverte .
Malgré cette escalade militaire et son intensité opérationnelle ( 27000 ratissages et 60000 sorties aériennes ) en 1962 , l' activité [MASK] [MASK] ne tarissait pas .
Il était tué le 2 novembre avec son frère [MASK] [MASK] [MASK] et une junte militaire était mise en place avec à sa tête le général [MASK] [MASK] [MASK] .
Le nouveau président américain [MASK] [MASK] , annula le retrait des troupes , augmenta le contingent et demanda l' aide de plusieurs alliés des [MASK] , notamment la [MASK] [MASK] [MASK] et l' [MASK] .
En mai , débutaient des raids américains sur le [MASK] pour tenter de couper la [MASK] [MASK] .
Le 10 février , le [MASK] [MASK] attaquait [MASK] [MASK] et provoquait la mort de 21 [MASK] .
[MASK] [MASK] franchissait une nouvelle étape le 13 du même mois en ordonnant des raids aériens plus étendus sur le [MASK] ( [MASK] [MASK] [MASK] ) .
Le 9 mars , [MASK] autorisait l' usage du napalm .
En mai 1966 , une rébellion militaire pro-bouddhiste éclatait à [MASK] [MASK] .
En février 1968 , débutait l' [MASK] [MASK] [MASK] .
Le 7 juillet , les forces américaines évacuaient la base de [MASK] [MASK] , après l' avoir pourtant renforcée et défendue bec et ongles de janvier à avril contre un véritable siège par trois divisions de l' armée nord-vietnamienne .
Ce siège s' avéra plus tard être une manoeuvre de diversion par les nords-vietnamiens destinée à attirer le plus de forces américaines possible dans cette région montagneuse éloignée des centres de population côtiers , avant de déclencher l' [MASK] [MASK] [MASK] sur les principaux centres urbains du sud-vietnam .
La foi du public américain en la " lumière au bout du tunnel " est balayée en février 1968 quand l' ennemi , supposé être sur le point de s' effondrer , organise l' [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] , en charge des opérations viet-congs , lance la quasi-totalité de ses effectifs dans la bataille ( environ 230000 hommes ) [ réf. nécessaire ] .
Le [MASK] ne revient à son niveau d' effectifs d' avant l' offensive que dans le courant de l' année 1972 avec le renfort d' unités régulières du nord et ne jouera plus de rôle déterminant dans le conflit .
À [MASK] , le [MASK] massacra environ 3000 intellectuels , commerçants et personnes liées au régime sud-vietnamien .
Il y avait déjà un faible mouvement d' opposition à la guerre dans certaines parties des [MASK] , dès 1964 , spécialement sur quelques campus universitaires .
La [MASK] [MASK] [MASK] s' était terminée en 1945 , et la [MASK] [MASK] [MASK] en 1953 .
L' [MASK] [MASK] [MASK] relève de la guerre psychologique .
Son but n' était pas seulement la conquête territoriale , mais d' amplifier l' opposition à la guerre aux [MASK] .
Des dossiers partiellement déclassifiés démontrent que 6359 officiers et généraux de l' [MASK] [MASK] soviétique ont agi comme conseillers militaires et ont pris part aux opérations de combats ( surtout dans la défense anti-aérienne ) .
En 1968 , le président [MASK] [MASK] débute sa campagne de réélection .
[MASK] perd les premières élections primaires dans le [MASK] [MASK] , mais il provoque la surprise en réalisant un score élevé contre le sortant .
Le coup porté à la campagne de [MASK] , combiné à d' autres facteurs , le mène à annoncer qu' il retire sa candidature , lors d' un discours télévisé surprise le 31 mars .
Se saisissant de l' opportunité causée par l' abandon de [MASK] , [MASK] [MASK] brigue alors l' investiture sur une plate-forme anti-guerre .
Le vice-président de [MASK] , [MASK] [MASK] , se porte également candidat , promettant de continuer d' aider le gouvernement de la [MASK] .
[MASK] [MASK] est assassiné durant cet été , et [MASK] est incapable de contrer le support dont [MASK] jouit dans l' élite du parti .
[MASK] gagne l' investiture de son parti , et se présente contre [MASK] [MASK] dans les élections générales .
[MASK] [MASK] est élu président et démarre à compter de janvier 1969 sa politique de lent désengagement de la guerre .
Le but est d' aider progressivement la [MASK] à construire sa propre armée de sorte qu' elle puisse poursuivre la guerre par elle-même .
Cela doit aussi donner , selon le mot de [MASK] [MASK] , " l' intervalle nécessaire " au désengagement des troupes américaines .
Le 18 mars 1970 , [MASK] [MASK] , alors premier ministre , obtient le soutien du parlement pour destituer le prince [MASK] [MASK] , accusé de ne pas lutter contre les [MASK] qui utilisent l' est du [MASK] comme sanctuaires militaires .
Parallèlement , [MASK] ordonne , le 29 avril 1970 , une incursion militaire du [MASK] par des troupes américaines et sud-vietnamiennes , afin de détruire les refuges viêt-cong bordant la [MASK] .
Le 30 avril , il s' adresse aux [MASK] pour justifier l' initiative , destinée essentiellement à protéger le processus de désengagement .
Celle-ci provoqua d' importantes manifestations à [MASK] et à [MASK] [MASK] [MASK] , qui augmentèrent l' opposition de l' opinion publique américaine à la guerre .
Ensuite , il n' y a plus qu' à attendre le 30 juin pour se redéployer , submergeant mécaniquement les forces de la [MASK] [MASK] , peu équipées , peu entraînées , mal gérées , mal organisées ...
Ce crime de guerre fut stoppé lorsque [MASK] [MASK] [MASK] [MASK] [MASK] , chef d ' équipe d' un hélicoptère d' observation remarque le carnage et intervient avec ses coéquipiers pour arrêter le massacre .
L' opinion publique américaine commence à douter majoritairement des options liées à un tel niveau d' engagement , alors que le parti de la génération de la " prise de conscience " ( consciousness generation ) maintient l' activisme par des sit-ins en faveur du règlement du conflit et du désengagement dans les allées publiques de [MASK] : la pression est telle que le pouvoir politique doit répondre instamment à la situation d' enlisement .
1972 est de plus une année terrible pour l' exécutif américain , avec le [MASK] [MASK] [MASK] et la publication des [MASK] [MASK] qui éclaboussent la classe politique .
À [MASK] [MASK] , plusieurs centaines de jeunes détruisent leurs papiers militaires .
D' autres fuient au [MASK] pour échapper à la guerre .
Le 21 octobre 1967 , une marche sur le [MASK] réunit plus de 100 000 personnes .
En avril 1968 , des étudiants occupent le campus de l' [MASK] [MASK] ; ils sont évacués par la police le 30 , ce qui entraîne une grève de protestation jusqu' à l' été 1968 .
D' autres universités prennent position contre la guerre , comme celle de [MASK] en [MASK] .
Cependant , la paix n' est toujours pas garantie et le nouveau général nordiste prépare l' offensive finale qui vaincra la [MASK] .
Nixon se bat alors aussi pour sa propre carrière politique empêtrée dans le [MASK] [MASK] [MASK] .
De plus , les [MASK] retirent unilatéralement leurs dernières forces du [MASK] [MASK] en 1973 .
À la fin de 1974 , en violation des accords , 100000 soldats supplémentaires s' infiltreront au [MASK] et au [MASK] dans des camps frontaliers puis dans les zones " libérées " au sud .
Hue ( l' ancienne cité impériale ) tombe le 25 mars , puis [MASK] [MASK] ( la seconde cité du [MASK] [MASK] ) le 2 avril .
Après avoir pris les plateaux centraux et coupé les forces sudistes en deux , puis écrasé la partie nord du [MASK] [MASK] [MASK] , les troupes de l' [MASK] [MASK] [MASK] se tournèrent ensuite vers le sud , tandis que de nouvelles troupes franchissaient la frontière depuis la [MASK] .
Début avril , la région de [MASK] est encerclée .
À 7h53 , le 30 avril , lorsque le dernier hélicoptère décolle du toit de l' ambassade des [MASK] à [MASK] , des milliers de candidats à l' exil se pressent encore dans les jardins .
Cette scène de panique à [MASK] , le 30 avril 1975 , sur le toit de l' ambassade des [MASK] à [MASK] est bien connue .
De 1975 à 1982 , 65000 personnes furent exécutées au [MASK] [MASK] et plus d' un million furent envoyées en " camps de rééducation " ou dans les " nouvelles zones économiques " .
Au total , trois millions de personnes quittèrent l' [MASK] entre 1975 et 1997 selon le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Une partie des réfugiés de 1975 étaient l' élite du régime de [MASK] partis en avion dans les bagages des derniers personnels américains .
Les réfugiés de 1980 étaient de petites gens fuyant la [MASK] [MASK] [MASK] [MASK] et les difficultés économiques d' un pays dévasté par des guerres depuis 1946 .
Il est ainsi très difficile de s' accorder exactement sur ce qui doit compter comme " victime de [MASK] [MASK] [MASK] [MASK] " ; des gens sont encore aujourd'hui tués par des sous-munitions non explosées et des mines , particulièrement les bombes à sous-munitions .
Les effets sur l' environnement des agents chimiques , tels que l' agent orange qui était un défoliant très utilisé par les [MASK] , ainsi que les problèmes sociaux colossaux causés par la dévastation du pays après tant de morts ont certainement réduit la durée de vie de beaucoup de survivants .
Concernant les pertes aériennes nord vietnamienne , 202 [MASK] furent abattus en combats aériens par 174 avions américains entre avril 1965 et janvier 1973 .
Voici les pertes des aéronefs des forces armées des [MASK] .
Les opérations entre 1961 et 1975 coûteront 533 milliards USD ( valeur 2005 ) aux [MASK] .
La plus grande conséquence sur le développement sud-asiatique est le fait que les plus grands " cerveaux " du [MASK] [MASK] ont soit fuit aux [MASK] avec les [MASK] , soit été décimés/envoyés dans des camps de rééducation par les communistes .
Ils constituent une force économique aux [MASK] et ailleurs .