[MASK] [MASK] [MASK] ( titre original : [MASK] [MASK] [MASK] [MASK] [MASK] ) est un roman de science-fiction , écrit en 1961 par [MASK] [MASK] [MASK] ( [MASK] ) .
Par les thèmes qu' elle aborde , cette œuvre a exercé une influence majeure sur la pensée de la contre-culture des années 1970 aux [MASK] .
[MASK] [MASK] [MASK] est un roman de science-fiction de [MASK] [MASK] [MASK] , paru aux [MASK] en 1961 , puis en [MASK] en 1970 .
Le titre original du roman , [MASK] [MASK] [MASK] [MASK] [MASK] est une citation de [MASK] dans la [MASK] , tirée de l' [MASK] , 2:22 .
L' œuvre est dédiée à [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] , deux écrivains de science-fiction .
[MASK] [MASK] [MASK] mit dix ans à écrire son roman .
Le roman [MASK] [MASK] [MASK] a connu 19 éditions américaines successives et 4 versions audio entre 1961 et 2003 .
Les éditeurs de [MASK] [MASK] [MASK] exigèrent de l' auteur qu' il fasse de larges coupes et qu' il retire une scène au contenu sexuel explicite de son manuscrit .
[MASK] [MASK] [MASK] travailla au remaniement de son manuscrit pendant l' hiver 1960-1961 .
Certains commentateurs américains y ont vu des réminiscences du style réaliste de [MASK] [MASK] .
Les deux amants quittent ensuite la [MASK] .
Sur [MASK] , l' eau est une denrée précieuse qui se retrouve au cœur de rites sacrés des [MASK] .
Les [MASK] naissent dans des nids .
Pendant leur période d' apprentissage , à cause de leur caractère plutôt turbulent , beaucoup de nymphes meurent et celles qui survivent sont ensuite fécondées par les [MASK] adultes .
Toutes les nymphes martiennes sont femelles , tandis que tous les [MASK] adultes sont mâles .
Les [MASK] ne connaissent ni l' argent , ni la propriété .
Jouant de la naïveté de son personnage et de la candeur de ses questionnements , [MASK] [MASK] [MASK] met en perspective -- ou remet en cause -- des aspects de la société américaine ou plus généralement occidentale aussi variés que la peur de la mort , l' amour , les tabous sexuels , la place du désir , la mauvaise conscience , la religion , la propriété , l' argent , la place de la violence légale , etc .
Le roman de [MASK] [MASK] [MASK] développe deux thématiques qui auront une grande importance pour la révolution culturelle des années 1970 :
[MASK] [MASK] [MASK] fait intervenir et interagir le pouvoir exécutif , le pouvoir répressif , le pouvoir judiciaire et la presse d' investigation .
Cette configuration politique anticiperait presque la tristement célèbre affaire du [MASK] qui défraiera la chronique américaine en 1972 .
[MASK] [MASK] [MASK] semble moins dénoncer l' abus de pouvoir en lui-même que ce qui le rend possible , à savoir l' interminable chaîne des intermédiaires politiques qui s' insère entre la population et ses hauts dirigeants .
[MASK] [MASK] [MASK] consacre les trois dernières parties de son roman à la religion .
La légende veut que [MASK] [MASK] , instigateur de l' assassinat de [MASK] [MASK] alors enceinte , ait effectué un certain amalgame entre la société que fréquentait l' épouse de [MASK] [MASK] et les personnes que [MASK] [MASK] [MASK] qualifiait de better dead et qui étaient éliminées par le héros afin de leur permettre de mener une existence posthume plus réussie .