[MASK] est la ville la plus peuplée de [MASK] ( en terme de population urbaine ) et l' une des plus grandes mégapoles du monde avec plus de vingt millions d' habitants .
Dans les années 1920 et 1930 , [MASK] a été le théâtre d' un formidable essor culturel qui a beaucoup contribué à l' aura mythique et fantasmatique qui est associée à la ville depuis cette époque .
Sa croissance à deux chiffres , les 18,9 millions d' habitants de sa région urbaine , sa mutation cosmopolite et son essor culturel l' appellent à devenir une métropole mondiale , aux côtés de [MASK] [MASK] , [MASK] ou [MASK] .
Elle accueille l' [MASK] [MASK] [MASK] [MASK] .
La transcription " [MASK] " est généralement prononcée / ʃɑ̃.gaj / en français , mais en chinois mandarin le nom 上海 se prononce / ʂɑŋ.xaɪ / .
Les deux sinogrammes dans le nom " [MASK] " ( 上 , shàng ; et 海 , hǎi ) signifient littéralement " sur , au dessus de " et " mer " .
À cause du changement du littoral , les historiens chinois ont conclu que , durant la [MASK] [MASK] , [MASK] était littéralement sur la mer , d' où l' origine du nom .
Au temps de la concession française , le nom français de la ville s' écrivait " [MASK] " .
La municipalité de [MASK] est un territoire administratif ayant le statut de province : elle comprend plusieurs villes dont [MASK] et compte environ 20 millions d' habitants .
L' agglomération ( en chinois : chengqu ) de [MASK] comptait 16,7 millions d' habitants en 2000 .
L' avenue [MASK] ( cinq kilomètres ) fut autrefois la grande artère de la concession dite étrangère .
Elle est considérée maintenant comme le vrai centre de [MASK] et elle offre souvent dans sa partie ouest , près du fleuve , le spectacle d' une indescriptible cohue de piétons .
[MASK] est situé dans un vaste delta , formé par l' embouchure du fleuve [MASK] [MASK] qui se jette dans la [MASK] [MASK] [MASK] [MASK] .
Les basses terres qui se trouvent des deux côtés de la rivière sont composés de lœss d' alluvions , qui est formé par les sédiments du [MASK] .
Construit de boue , sillonné de canaux et de barrages le delta est l' une des zones les plus fertiles de [MASK] , et également son principal fournisseur de coton .
La formation du delta a renvoyé [MASK] , une ville portuaire à l' origine construite sur la mer , à 30 km à l' intérieur des terres .
[MASK] bénéficie d' un climat subtropical humide .
La municipalité de [MASK] exerce sa juridiction sur dix-neuf subdivisions : dix-huit districts et un xian .
Un district gouverne le quartier de [MASK] , à l' est du [MASK] :
L' île de [MASK] , située dans l' estuaire du [MASK] [MASK] ( [MASK] [MASK] ) , est gouvernée par un seul xian :
À l' origine port modeste et village de tisserands , [MASK] ne semblait pas promise à pareil essor cosmopolite .
[MASK] ne s' est pas toujours appelée [MASK] .
[MASK] et [MASK] suivront , précédant les [MASK] et les [MASK] .
Les [MASK] vainqueurs y aménagent l' un des cinq ports ouverts qui leur seront alors concédés .
Cette année eut également lieu la première réunion du conseil municipal de [MASK] , afin de gérer les concessions étrangères établies de facto .
Son parrain le plus connu , [MASK] [MASK] , menait ses trafics en collaborant étroitement avec la police de la concession française .
Ils établirent à [MASK] les premières usines de la ville .
C' est aussi à [MASK] que fut créé le [MASK] [MASK] [MASK] en 1921 et qu' ont été organisées les premières grèves ouvrières .
La plupart , coolies et ouvriers , demeurèrent dans la pauvreté et vinrent grossir les rangs du [MASK] [MASK] [MASK] .
Sous le régime de la [MASK] [MASK] [MASK] , [MASK] devint une ville spéciale en 1927 , et une municipalité en mai 1930 .
La marine japonaise bombarda la ville le 28 janvier 1932 , officiellement pour réprimer les protestations étudiantes ayant suivi l' [MASK] [MASK] [MASK] , déclenchant la " [MASK] [MASK] [MASK] " .
Disposant de forces terrestres et navales bien supérieures à l' armée chinoise , les troupes impériales prirent possession de la ville en novembre ( [MASK] [MASK] [MASK] ) , puis se dirigèrent vers [MASK] où elles se livrèrent à un terrible carnage ( [MASK] [MASK] [MASK] ) .
En 1938 , [MASK] fut considérée comme le cinquième port mondial ; les plus grandes firmes occidentales y étaient désormais représentées .
En 1941 , sous pression de leurs alliés nazis , les [MASK] reçurent les réfugiés juifs dans un ghetto , où les maladies pullulaient , .
L' immigration juive fut finalement stoppée par les [MASK] le 21 août 1941 .
Les [MASK] prirent le contrôle total des concessions le 8 décembre 1941 .
En février 1943 , le gouvernement du [MASK] signa avec la [MASK] [MASK] [MASK] un traité acceptant le principe d' une rétrocession .
En juillet de la même année , les [MASK] rétrocédèrent le conseil municipal au gouvernement collaborateur chinois de [MASK] [MASK] .
Les huit années d' occupation , puis la victoire , en 1949 , de [MASK] [MASK] sur les troupes du général [MASK] [MASK] précipitèrent le déclin de la ville .
Après la victoire des communistes , la ville a été considérée comme le symbole du capitalisme étranger , elle sommeillait , et le monde l' avait presque oubliée , avant d' être revalorisée suite au mouvement de réformes de [MASK] [MASK] .
Autrefois tête de pont des puissances coloniales dans une [MASK] agonisante , [MASK] est devenue le premier centre industriel du pays , en même temps que l' une des plus grandes métropoles du monde .
Pendant la [MASK] [MASK] , [MASK] connut des troubles politiques et sociaux : à la fin décembre 1966 , la municipalité fut renversée .
Le bilan de la [MASK] [MASK] fut considérable : 150000 logements furent confisqués rien qu' à [MASK] .
Au début des années 1990 , en une décennie , la " [MASK] [MASK] [MASK] [MASK] " est redevenue un centre économique de première importance , qui compte en 2005 pour 20 % de la production industrielle nationale pour seulement 1,5 % de la population .
En septembre 2006 , [MASK] [MASK] est limogé suite à un scandale de corruption .
Avant cela , le 3 décembre 2002 , la métropole chinoise a été désignée pour organiser l' [MASK] [MASK] [MASK] [MASK] , qui se tient donc , pour la première fois depuis 151 ans , dans un pays en voie de développement .
La population de [MASK] est de 19 213 200 habitants .
D' après la population totale de la municipalité , [MASK] est la troisième plus grande municipalité de la [MASK] [MASK] [MASK] [MASK] , après [MASK] et [MASK] .
En [MASK] , une municipalité ( 直辖市 en pinyin : zhíxiáshì ) est une ville avec un statut équivalent aux provinces chinoises .
Le recensement de 2000 position la population de [MASK] à 16,738 million , dont 3,871 millions de migrants .
En 2008 , la population de résidents à long terme atteint 18,88 millions , incluant la population permanente officielle de 13,71 millions d' individus et 4,79 millions de migrants des autres provinces , notamment des provinces du [MASK] , [MASK] et [MASK] .
On compte également un nombre important d' habitants de [MASK] qui résident à [MASK] pour les affaires ( entre environ 350 000 et 700 000 ) .
En 2009 , les communautés sud-coréennes à [MASK] atteignaient 70 000 ressortissants. .
Le revenu moyen annuel des résidents de [MASK] , basé sur les trois premiers trimestres de 2009 , est de 21871 yuans .
La ville a longtemps été l' un des principaux centres de production textile de la [MASK] [MASK] [MASK] [MASK] .
Avec le début de réformes économiques chinoises au début des années 1980 , [MASK] a d' abord été dépassé par certaines provinces du sud , telles que [MASK] .
[MASK] [MASK] constitue le principal rival de [MASK] dans le titre honorifique de plus grand centre économique en [MASK] .
[MASK] [MASK] possède l' avantage d' une plus grande expérience , notamment dans le secteur bancaire .
[MASK] a des liens plus étroits avec l' arrière-pays chinois et le gouvernement central de [MASK] .
De plus , [MASK] possède plus de terrains pour accueillir les nouveaux investissements , alors qu' à [MASK] [MASK] , l' espace est très limité .
Depuis 1991 , la croissance économique à [MASK] est à deux chiffres .
La ville est donc la seule région de [MASK] dans ce cas sur une telle durée .
La croissance économique annuelle à [MASK] est actuellement d' environ 12 % .
Le PIB par habitant était d' environ 7000 dollars ( la moyenne chinoise se situe à 1800 dollars ) et constitue le troisième plus élevé du pays , derrière [MASK] [MASK] et [MASK] .
En 1984 , à [MASK] , une coentreprise avec le constructeur automobile [MASK] constitue la première usine automobile construite avec une marque occidentale .
[MASK] [MASK] représente une part de marché d' environ 60 % sur les véhicules étrangers en [MASK] , ce qui est en baisse constante en raison d' une concurrence accrue .
Ainsi , après l ' [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] [MASK] [MASK] [MASK] , la conférence de l' [MASK] en 2001 a réduit progressivement les droits à l' importation .
Le 5 août 2002 , le nouveau maire de [MASK] , [MASK] [MASK] a déclaré qu' il voulait " faire de sa ville , dans les trois années à venir , le centre du marché financier intérieur , des circulations des capitaux et de gestion de fonds , et l' un des centres financiers internationaux les plus importants pour une durée de dix à vingt ans. "
Cela dépend directement de la réforme du système financier chinois , encore très archaïque , mené par les autorités centrales de [MASK] .
Actuellement , on compte plus de 50 000 étrangers dans la métropole chinoise , en plus d' environ 300 000 [MASK] .
Ces travailleurs étrangers sont principalement originaires du [MASK] , des [MASK] , de la [MASK] [MASK] [MASK] , de [MASK] , de l' [MASK] , de la [MASK] et du [MASK] .
Elle donne une idée de la valeur de prestige accordée au développement immobilier à [MASK] .
Le [MASK] [MASK] [MASK] , en est l' exemple le plus éclatant , avec ses 492 mètres de hauteur , il est le plus haut bâtiment de [MASK] .
En [MASK] , l' immobilier est une des activités les plus opaques , ce qui explique la fragilité du secteur qui pourrait éclater si la croissance économique montre des signes de ralentissement .
[MASK] est également un centre important de raffineries de pétrole .
La plus grande aciérie de [MASK] , et l' une des plus modernes , se situe à [MASK] , en bord de mer .
Environ quatre millions de tonnes d' eaux usées industrielles et domestiques non filtrée sont versées quotidiennement dans la rivière [MASK] , la principale source d' eau potable de la ville , et dans le canal de [MASK] dont les eaux sont fréquemment noires et nauséabondes .
Un autre problème est le chômage , qui est supérieur à [MASK] par rapport à d' autres grandes villes du pays .
L ' [MASK] [MASK] est l' une des universités de premier plan en [MASK] [MASK] [MASK] [MASK] .
Elle prend son nom actuel en1946 quand elle revient à [MASK] .
L ' [MASK] [MASK] est l' une des plus prestigieuses universités en [MASK] .
En 1923 , elle devient une université et en 1937 elle est déménagé à cause de la guerre , d' abord dans la province de [MASK] .
Lorsque le front approche , elle déménage vers la province de [MASK] , puis [MASK] , et plus tard même pour le [MASK] .
Après la fin de la [MASK] [MASK] [MASK] , elle revient de nouveau à [MASK] , en 1946 .
L ' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] est une institution importante dans le pays .
Depuis 2002 il existe un programme allemand des affaires , qui a été conçu conjointement avec l' [MASK] [MASK] [MASK] .
Voici une liste des autres principaux institut et universités présentes à [MASK] :
La langue officielle de [MASK] , comme dans l' ensemble de la [MASK] est le mandarin .
La variété parlée à [MASK] est le shanghaïen .
Dans le centre de [MASK] , près de l' hôtel de ville et de la [MASK] [MASK] [MASK] se trouvent le [MASK] [MASK] [MASK] et l' [MASK] [MASK] [MASK] .
Le long de la rivière [MASK] se trouve le [MASK] d' où l' on peut voir le quartier d' affaires de [MASK] et ses gratte-ciels dont les plus hauts sont la [MASK] [MASK] [MASK] [MASK] , la [MASK] [MASK] [MASK] et le [MASK] [MASK] [MASK] [MASK] .
En ce qui concerne les religions asiatiques , on trouve trois principaux temples : le [MASK] [MASK] [MASK] , le [MASK] [MASK] [MASK] [MASK] [MASK] et le temple du dieu de la ville , ce dernier se situant près du [MASK] [MASK] .
Enfin , certains ponts sont remarquables , comme le [MASK] [MASK] [MASK] et le [MASK] [MASK] [MASK] , qui se situent parmi les plus longs du monde avec respectivement plus de 400 m et plus de 600 m de portée .
Le [MASK] [MASK] [MASK] , quant à lui , est le deuxième plus long pont en arc du monde , avec 550 m de portée .
La cuisine de [MASK] est en en partie tournée vers les fruits de mer et les poissons d' eau douce , du fait de sa position géographique .
Ainsi , le crabe poilu de [MASK] ( shàng hǎi máo xiè , 上海毛蟹 ) est une célèbre spécialité délicate , prisée pour les qualités aphrodisiaques des ovaires du crabe femelle .
La cuisine de [MASK] est également réputée pour la cuisson " braisée en rouge " ( hóng shāo , 紅燒 ) , qui consiste à faire cuire à feu doux viandes et légumes .
Les habitants de la ville de [MASK] sont réputés pour manger de petites portions .
Par exemple , les bouchées à la vapeur ( xiǎo lóng bāo , 小笼包 ) sont beaucoup plus petites que leurs cousines baozi ( 包子 ) que l' on trouve ailleurs en [MASK] .
Voici une liste de spécialités de la cuisine de [MASK] :
[MASK] , ville de cinéma , a inspiré les cinéastes .
Quelques films où le décor de [MASK] à diverses époques joue un rôle majeur .
[MASK] possède d' importantes infrastructures sportives .
Le [MASK] [MASK] [MASK] peut ainsi accueillir 80000 personnes et constitue le troisième plus grand stade en [MASK] .
Le [MASK] [MASK] [MASK] compte quant à lui 31000 places .
La ville organise également chaque année les [MASK] [MASK] [MASK] , une compétition de tennis masculin , qui fait partie des [MASK] [MASK] de l' [MASK] [MASK] depuis 2009 , au même titre que les [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] ou encore [MASK] [MASK] [MASK] .
Depuis le 1 er janvier 2004 , une [MASK] [MASK] [MASK] , un train à sustentation magnétique , relie la ville au nouvel [MASK] [MASK] [MASK] [MASK] .
La 2 e compagnie aérienne chinoise est basée sur cet aéroport : [MASK] [MASK] [MASK] .
Il est relié à l' [MASK] [MASK] [MASK] [MASK] par la ligne 2 du métro qui le dessert depuis 2010 .
Une bonne partie du trafic s' effectue avec l' intérieur du pays , par les 5000 kilomètres navigables du [MASK] [MASK] : les bateaux peuvent aller de [MASK] jusqu' à [MASK] .
[MASK] est jumelée avec les villes suivantes :
[MASK] a des partenariats avec les villes ou régions suivantes :