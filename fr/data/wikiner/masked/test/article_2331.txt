Durant les années qui suivirent la guerre , l' [MASK] s' affirma comme une puissance économique montante , le moteur économique passant de l' agriculture à l' industrie lourde , à l' extraction de minerai , à l' éducation et aux hautes technologies .
La capitale est [MASK] et la plus grande ville [MASK] .
Le mot [MASK] proviendrait de la langue [MASK] et fut plus tard adopté par la tribu [MASK] comme nom .
Bien que les origines du nom [MASK] soit évidente , le sens du nom de la tribu n' est pas toujours clair .
Il est dorénavant accepté que le mot proviendrait des mots [MASK] alba ( signifiant " plantes " ou " mauvaises herbes " ) et amo ( signifiant " couper " , " équilibrer " , ou " cueillir " ) , , .
L' [MASK] du nord et du centre faisait partie de la [MASK] britannique de 1763 à 1783 et ensuite une partie du territoire du [MASK] américain .
En 1720 , la capitale de la [MASK] [MASK] est transférée de [MASK] à [MASK] et en 1763 , la [MASK] à l' est du fleuve du [MASK] est cédée à la [MASK] .
En 1800 , ce territoire n' a toujours que 5750 habitants , 4500 dans le [MASK] [MASK] et 1250 le long de la [MASK] [MASK] , dans le futur [MASK] , à l' ouest de [MASK] , secteur qui deviendra très vite une partie de l' [MASK] .
Le flot d' immigrants sur la période 1798-1812 fut significatif pour le [MASK] [MASK] , placé sur le [MASK] , mais beaucoup plus faible pour l' [MASK] .
En 1810 , c' est toujours le [MASK] , privilégié par son grand fleuve et son peuplement plus ancien qui croît plus vite : la population y est de 15.826 blancs et 13.924 noirs , soit 29.700 habitants , pour la plupart dans le [MASK] [MASK] et ses terres très fertiles .
L' [MASK] devient la nouvelle frontière durant les années 1820 et 1830 .
La taux de croissance de la population en [MASK] tombe presque de moitié de 1910 à 1920 , reflétant cette émigration .
Dans le même temps , beaucoup de blancs et de noirs , anciennement agriculteurs , s' installent dans la ville de [MASK] pour trouver un emploi dans le secteur industriel .
Le développement industriel dû aux exigences de la [MASK] [MASK] [MASK] a apporté la prospérité .
La population blanche de l' [MASK] s' opposera souvent violemment dans les années 1950 et 1960 à l' émancipation des noirs .
Le mouvement des droits civiques avec [MASK] [MASK] [MASK] ou [MASK] [MASK] a débuté en [MASK] .
Les trois cinquièmes de la superficie terrestre constituent une plaine qui descend en pente douce vers le [MASK] et le [MASK] [MASK] [MASK] .
Le nord de l' [MASK] est plus montagneux , avec le [MASK] créant une large vallée qui forme de nombreux cours d' eau , des lacs et des collines .
Le cratère d' impact d' une météorite de 8 km de long se trouve dans le [MASK] [MASK] [MASK] , au nord de [MASK] .
Généralement , l' [MASK] est sujet à des étés très chauds et des hivers moyens , avec des précipitations tout au long de l' année .
L' [MASK] est aussi enclin aux cyclones tropicaux et aux ouragans .
Le sud de l' [MASK] rencontre plus d' orages que n' importe quelle autre région des [MASK] .
L' [MASK] est l' un des rares endroits au monde qui connait une saison des tornades secondaire ( novembre et décembre ) en plus de la saison estivale propice à ce phénomène .
Les chutes de neige sont rares dans presque tout l' [MASK] .
Les régions au nord de [MASK] peuvent recevoir un peu de neige chaque hiver , avec des chutes un peu plus importantes de temps en temps .
La région de [MASK] se distingue par son fort pourcentage de catholiques , en raison de l' occupation française et espagnole de cette région .
Lors de l' année scolaire 2006-2007 , l' [MASK] utilisa $ 3775163578 au primaire et au secondaire .
Les programmes d' enseignement supérieur de l' [MASK] comprend quatorze universités ( quatre ans d' étude ) , et des lycées ( deux ans d' étude ) , et dix-sept universités privées .
La production agricole de l' [MASK] est composée de volailles et d' œufs , de bétail , de cacahuètes , de coton , de céréales telles que le maïs et le sorgho , de légumes , de lait , de soja et de pêches .
L' activité industrielle de l' [MASK] comprend les productions dérivées du fer et de l' acier ( comme les moules et les tuyaux ) , ainsi que les activités dérivées du bois , de l' extraction à la coupe en passant par la fabrication de papier .
L' [MASK] abrite aussi une industrie minière ( principalement dans l' extraction du charbon ) .
Cette croissance est en grande partie liée à la rapide expansion de l' industrie automobile qui , depuis son arrivée en [MASK] en 1993 , a généré plus de 67800 nouveaux emplois .
En mai 2007 , un site au nord de [MASK] a été sélectionné par le producteur d' acier allemand [MASK] pour l' implantation d' une usine représentant un investissement de 3,7 milliards de dollars .
Le port de [MASK] , le seul d' [MASK] sur le [MASK] [MASK] [MASK] , est le plus actif du golfe .
La structure de l' impôt en [MASK] est l' une des plus régressive aux [MASK] .
En 2003 , l' [MASK] a connu un budget en déficit de 670 millions de dollars .
Un mouvement important existe qui voudrait réécrire et moderniser la constitution de l' [MASK] .
Ce mouvement se concentre sur le fait que la constitution de l' [MASK] concentre tous les pouvoirs sur [MASK] et ne laisse pratiquement aucun pouvoir aux élus locaux .
Le gouvernement de l' [MASK] est divisé en trois branches de pouvoir égal :
L' [MASK] a 67 comtés .
[MASK] [MASK] , le gouverneur d' alors , reste encore aujourd'hui une figure connue et controversée .
Lors d' une cérémonie symbolique , la résolution fut signée au [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , qui fut la première capitale des [MASK] [MASK] [MASK] [MASK] .
En 1948 , 79 % des électeurs avaient cependant choisi [MASK] [MASK] , le candidat démocrate dissident ( [MASK] ) .
Depuis 1980 , les électeurs alabamiens conservateurs votent de plus en plus pour les candidats républicains au niveau fédéral mais localement , les démocrates restent encore majoritaires , tant en termes de fonctions électives détenues qu' en termes de préférences politiques affirmées par les habitants de l' [MASK] .
La liste suivante recense les équipes de sports professionnelles en activité en [MASK] .
La liste suivante recense les équipes de sports professionnelles qui ont été en activité en [MASK] .
Le groupe de rock [MASK] [MASK] a composé la célèbre chanson [MASK] [MASK] [MASK] , chanson controversée qui vante la beauté de la région mais évoque aussi en termes ambigus le gouverneur [MASK] [MASK] , ségrégationniste convaincu .