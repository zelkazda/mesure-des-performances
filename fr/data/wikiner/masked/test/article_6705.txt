Il acquiert aussi ses premières lettres de noblesse grâce à des grands réalisateurs comme [MASK] , [MASK] ou [MASK] qui en fixent les règles de langage .
La photographie , quant à elle , naît en 1839 sous l' impulsion de [MASK] [MASK] .
[MASK] [MASK] a précisément l' idée en 1878 d' aligner vingt-quatre appareils photographiques pour décomposer le mouvement d' un cheval lancé au galop .
[MASK] [MASK] qui travaille également sur le mouvement des animaux crée en 1882 un fusil photographique qu' il dote ensuite d' une pellicule : le chronophotographe .
[MASK] [MASK] donne naissance au théâtre optique .
L' américain [MASK] [MASK] invente la pellicule permettant ainsi d' aligner plusieurs images en négatif sur un film transparent .
[MASK] [MASK] [MASK] [MASK] [MASK] construit et dépose le brevet d' une caméra le 11 janvier 1888 .
Cependant , en 1890 , après avoir amélioré sa caméra , l' inventeur disparaît mystérieusement dans le train express [MASK] -- [MASK] .
En [MASK] l' inventeur [MASK] [MASK] met au point une caméra capable de fixer dix images par seconde à l' aide d' un film perforé .
Pour alimenter ce commerce , la compagnie d' [MASK] construit le premier studio de l' histoire : la [MASK] [MASK] .
Le nom " cinématographe " apparaît lui dans un brevet déposé le 12 février 1892 par [MASK] [MASK] , d' abord sous l' intitulé " cynématographe [MASK] [MASK] " , puis " cinématographe " , dans un autre brevet qu' il dépose le 27 décembre 1893 .
Les [MASK] [MASK] déposent le brevet de leur cinématographe le 13 février 1895 .
Le gérant du [MASK] [MASK] , celui des [MASK] [MASK] et [MASK] [MASK] qui y assistent , surenchérissent pour s' accaparer l' appareil .
En vain puisque [MASK] [MASK] refuse de le leur vendre .
En effet , les [MASK] [MASK] conservent pour eux l' exploitation de leur invention .
Au [MASK] , [MASK] [MASK] [MASK] exploite déjà un plagiat du kinétoscope ; il commercialise bientôt un appareil de projection sous le nom d ' " animatograph " ou " theatrogoraph " .
Les [MASK] [MASK] forment et envoient des opérateurs de par le monde pour faire la promotion de leur cinématographe .
Dans les pays qu' ils traversent , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] tournent et projettent leurs réalisations devant un public médusé .
Au passage , la société des [MASK] [MASK] concède des licences d' exploitation de l' appareil notamment au [MASK] et en [MASK] .
Le [MASK] voit en 1898 son premier cinéaste [MASK] [MASK] filmer les vacances d' été de la famille royale danoise à l' aide d' un appareil de sa propre fabrication .
La première bande allemande est aussi tournée en 1898 montrant une promenade de jeunes gens dans la [MASK] .
L' [MASK] se lance dans la production cette même année avec une course automobile .
La [MASK] , elle , n' entame véritablement son histoire cinématographique qu' en 1907 avec le photographe [MASK] [MASK] qui fonde son propre studio .
[MASK] [MASK] déclenche en 1897 ce qu' on a appelé la " guerre des brevets " .
Un an après , ne restent plus que deux studios américains : la [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] , et l' [MASK] [MASK] [MASK] [MASK] [MASK] , société cofondée par [MASK] [MASK] , qui exploite le mutoscope puis met au point la biograph , une caméra imposante mais de grande qualité .
Cette même année 1897 , en [MASK] , l' incendie du [MASK] [MASK] [MASK] [MASK] marque suffisamment les esprits pour freiner le développement du cinématographe et entacher sa réputation .
Son inventeur , [MASK] [MASK] , songe dès 1894 à coupler son kinétoscope d' un cylindre phonographique , et commercialise le kinétophone .
Dans ce cadre , [MASK] [MASK] invente le travelling à [MASK] au printemps 1896 .
Le film obtient une certaine renommée et est exporté aux [MASK] .
Il a recours au trucage avec l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , arrêtant la prise de vue et la reprenant , substituant au passage une femme à un squelette .
L' homme s' illustre d' abord dans l' actualité reconstituée inspirée des illustrations du [MASK] [MASK] .
Mais c' est encore dans la féerie que [MASK] [MASK] s' épanouit , peignant lui-même d' immenses toiles de fond , utilisant les procédés de surimpression déjà connus de la photographie .
Cependant , [MASK] [MASK] dont l' expérience vient du spectacle vivant s' en tient à une narration théâtrale .
Ce procédé primaire de narration est aussi celui adopté par les concurrents de [MASK] [MASK] que sont [MASK] [MASK] et [MASK] [MASK] .
[MASK] [MASK] recrute [MASK] [MASK] en 1900 .
Quant à [MASK] [MASK] , plus intéressé par la technique , il confie ses productions à sa secrétaire [MASK] [MASK] à qui l' on doit sans doute le premier film narratif original , [MASK] [MASK] [MASK] [MASK] , réalisé dès 1896 avec les moyens du bord .
D' après la définition de la [MASK] [MASK] selon laquelle un long métrage est un film de plus de soixante minutes , ce film d' une durée de 60 à 70 minutes est le premier long métrage de l' histoire du cinéma .
Ce film raconte l' histoire du bushranger [MASK] [MASK] . .
En 1901 , les deux frères [MASK] commencent à fonder leur empire en construisant une usine phonographique à [MASK] , ainsi que deux studios de cinéma , dont les films produits sont signés par le label " [MASK] " .
En 1904 , [MASK] distribue 50 % des films diffusés en [MASK] et en [MASK] .
Dans les années 1910 , le cinéma américain se distingue par ses comiques : [MASK] [MASK] , [MASK] [MASK] , [MASK] , [MASK] [MASK] , [MASK] [MASK] ...
Les premières stars apparaissent , ainsi [MASK] [MASK] reçoit 100.000 $ par an .
[MASK] [MASK] [MASK] ( avec surtout [MASK] [MASK] [MASK] [MASK] et [MASK] ) , [MASK] [MASK] [MASK] ( avec [MASK] ) , [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] font leur début dans le métier aux [MASK] .
En 1919 , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] créent la [MASK] [MASK] [MASK] ( aussi connu sous le nom des [MASK] [MASK] ) .
En [MASK] c' est les débuts des réalisateurs [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] .
Un des premiers succès du cinéma français est un serial : [MASK] , film en 5 épisodes , réalisé par [MASK] [MASK] qui réitèrera l' initiative avec [MASK] [MASK] .
Ce sont les débuts de [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] ( [MASK] [MASK] [MASK] [MASK] [MASK] ) .
En 1917 en [MASK] , [MASK] [MASK] [MASK] de [MASK] [MASK] donne à [MASK] [MASK] une stature de célébrité dans le monde du cinéma .
En 1921 , [MASK] [MASK] rend définitivement célèbre [MASK] [MASK] .
En 1922 , [MASK] [MASK] [MASK] de [MASK] [MASK] permet aux spectateurs de découvrir un monde inconnu , celui du grand nord .
En 1928 , [MASK] [MASK] incarne [MASK] [MASK] [MASK] dans [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] [MASK] .
En août 1932 a lieu la [MASK] [MASK] [MASK] [MASK] .
Le 28 avril 1937 , les studios de [MASK] à [MASK] sont inaugurés , ils devaient faire concurrence à [MASK] dans la pensée de [MASK] [MASK] , le directeur .
Le 1 er septembre 1939 devait avoir lieu le premier [MASK] [MASK] [MASK] présidé par [MASK] [MASK] mais la guerre éclate avec [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , le festival sera reporté après la guerre .
Dans le cinéma comique , les [MASK] [MASK] apparaissent avec les films [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] , leur comique est basé sur la dérision et le non-sens .
Cette société présidée par [MASK] [MASK] produira 30 films français jusqu' en 1944 ( dont [MASK] [MASK] de [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] ) .
Les [MASK] [MASK] de 1946 imposent , en contrepartie d' un effacement partiel de la dette de [MASK] [MASK] envers les [MASK] , la projection de films américains dans les salles françaises , ce qui contribue à l' américanisation du cinéma en [MASK] et en [MASK] .