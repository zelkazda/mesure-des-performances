La nétiquette est une règle informelle , puis une charte qui définit les règles de conduite et de politesse recommandées sur les premiers médias de communication mis à disposition par [MASK] .
) , les standards plus récents ( [MASK] , [MASK] , etc . )
Pour cela , il recourt au langage [MASK] qui est normalement prévu pour la mise en page des sites [MASK] .
Il existe un nombre important de raisons pour lesquelles , il ne faut pas envoyer de courriel au format [MASK] ; l' une d' entre elles étant que tous les clients de courrier électronique ne lisent pas correctement ( ou du tout ) les messages formatés en [MASK] .
Un texte court introduisant le propos du message pourra alors précéder un lien vers une page [MASK] qui offrira un design personnalisé .
La copie cachée vous permet également d' envoyer un courriel à tous ou certains de vos contacts sans diffuser votre carnet d' adresses sur [MASK] ( ce qui pourrait , à juste titre , être considéré comme une faute grave par votre employeur dans le cas d' un fichier clients ) .
Un utilisateur d' [MASK] est invité à ne pas participer à un groupe dès qu' il l' a rejoint .
Il est convenu depuis la création des [MASK] de répondre en dessous et non au-dessus de l' article précédant .
La politesse sur [MASK] veut que l' utilisateur ne trolle pas , ne fasse pas de pub et ne perturbe pas le bon fonctionnement du chat .
De plus les sites qui hébergent des applets chat ( [MASK] ) rappellent les règles de bons sens .
et respecte la liberté de choix de son interlocuteur par l' usage de formats interopérables et standardisés ( [MASK] , [MASK] , [MASK] , etc. ) .