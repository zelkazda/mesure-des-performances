L' algorithme [MASK] , pour [MASK] [MASK] [MASK] , est une fonction de hachage cryptographique qui permet d' obtenir l' empreinte numérique d' un fichier ( on parle souvent de message ) .
Il a été inventé par [MASK] [MASK] .
[MASK] ( [MASK] [MASK] [MASK] ) est une fonction de hachage cryptographique qui calcule , à partir d' un fichier numérique , son empreinte numérique ( en l' occurrence une séquence de 128 bits ou 32 caractères en notation hexadécimale ) avec une probabilité très forte que deux fichiers différents donnent deux empreintes différentes .
En 1991 , [MASK] [MASK] améliore l' architecture de [MASK] pour contrer des attaques potentielles qui seront confirmées plus tard par les travaux de [MASK] [MASK] .
Cinq ans plus tard , en 1996 , une faille qualifiée de grave ( possibilité de créer des collisions à la demande ) est découverte et indique que [MASK] devrait être mis de côté au profit de fonctions plus robustes comme [MASK] .
[MASK] n' est donc plus considéré comme sûr au sens cryptographique .
On suggère maintenant d' utiliser plutôt des algorithmes tels que [MASK] , [MASK] ou [MASK] .
Cependant , la fonction [MASK] reste encore largement utilisée comme outil de vérification lors des téléchargements et l' utilisateur peut valider l' intégrité de la version téléchargée grâce à l' empreinte .
Ceci peut se faire avec un programme comme md5sum pour [MASK] et sha1sum pour [MASK] .
[MASK] peut aussi être utilisé pour calculer l' empreinte d' un mot de passe ; c' est le système employé dans [MASK] avec la présence d' un sel compliquant le décryptage .
Ainsi , plutôt que de stocker les mots de passe dans un fichier , ce sont leurs empreintes [MASK] qui sont enregistrées , de sorte que quelqu' un qui lirait ce fichier ne pourra pas découvrir les mots de passe .
Le programme [MASK] [MASK] [MASK] permet de casser ( trouver une collision pour ) les [MASK] triviaux par force brute .
Très concrètement , la vérification de l' empreinte ou somme de contrôle [MASK] peut-être réalisée de la façon suivante : lors du téléchargement d' un programme , on note la série de caractères indiquée sur la page de téléchargement .
Le [MASK] n' est donc plus considéré comme sûr mais l' algorithme développé par ces trois chercheurs concerne des collisions quelconques et ne permet pas de réaliser une collision sur une empreinte spécifique , c' est-à-dire réaliser un deuxième message , à partir de l' empreinte d' un premier message , qui produirait la même empreinte .
La sécurité du [MASK] n' étant plus garantie selon sa définition cryptographique , les spécialistes recommandent d' utiliser des fonctions de hachage plus récentes comme le [MASK] .
On sait maintenant générer des collisions [MASK] en moins d' une minute lorsque les deux blocs en collisions sont " libres " .
Dès 2006 , il est par exemple possible de créer des pages [MASK] aux contenus très différents et ayant pourtant le même [MASK] .
La présence de métacodes de " bourrage " placés en commentaires , visibles seulement dans la source de la page web , trahit toutefois les pages modifiées pour usurper le [MASK] d' une autre .
[MASK] travaille avec un message de taille variable et produit une empreinte de 128 bits .
[MASK] peut s' écrire sous cette forme en pseudo-code .
le [MASK] 1321 qui détaille l' algorithme :
[MASK] est maintenant intégré d' office au sein des API de plusieurs langages comme [MASK] , [MASK] ou [MASK] .