[MASK] est une constellation de l' hémisphère nord .
Relativement grande , elle possède plusieurs étoiles brillantes , dont l' étoile variable [MASK] ( [MASK] [MASK] ) .
La [MASK] [MASK] [MASK] est ancienne .
La constellation appartient au cycle qui décrit le mythe d' [MASK] .
[MASK] est une constellation très lumineuse , placée dans un environnement très riche .
Bien que formée d' étoiles relativement brillantes et assez rapprochées , [MASK] ne dégage pas de forme très nette .
Ces quatre étoiles sont celles qui forment le " pied " immédiatement au sud d' [MASK] .
[MASK] ( [MASK] [MASK] ) est l' étoile la plus brillante de la constellation .
Elle est parfois nommée " [MASK] " .
[MASK] ( [MASK] [MASK] ) est la deuxième étoile de la constellation par la luminosité , mais la plus connue .
[MASK] passe ainsi de la magnitude 2,12 à la magnitude 3,39 tous les 2,867 jours .
On peut noter également les objets célestes suivant dans la [MASK] [MASK] [MASK] , traversée par la [MASK] [MASK] :