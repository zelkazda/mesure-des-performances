Comme pays , la [MASK] eut des liens historiques prolongées avec l' [MASK] , la [MASK] , la [MASK] et la [MASK] .
Dans le processus pour l' indépendance du début des années 1990 , la [MASK] , tout comme l' [MASK] , proposa des lois pour prévenir l' extinction de la langue .
Le letton est la langue maternelle de 1,4 million de personnes en [MASK] où c' est la langue officielle et d' environ 500000 personnes à l' étranger .
Le phonème noté par ŗ s' étant amuï , la lettre est de nos jours généralement ignorée , du moins dans les publications de [MASK] propre .