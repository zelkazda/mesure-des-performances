C' est la plus grande ville en population de la [MASK] [MASK] [MASK] [MASK] [MASK] qui regroupe 14 communes , soit 125000 habitants , d' où le nom de la communauté d' agglomération .
La commune résulte de la fusion d' [MASK] et de [MASK] en 1971 .
[MASK] se situe dans la région [MASK] plus précisément dans la [MASK] et comme toutes les villes de la [MASK] [MASK] [MASK] [MASK] [MASK] , la ville fait aussi partie du [MASK] [MASK] [MASK] [MASK] .
De plus , [MASK] est à 30 kilomètres , [MASK] à 20 et [MASK] à 10
Les villes limitrophes sont [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] mais il est impossible de passer directement de cette dernière à [MASK] en voiture .
La ville d' [MASK] est desservie par l ' [MASK] , sortie 16.1 , sortie construite en 2003 pour un accès direct aux zones logistiques et commerciales toutes proches .
Néanmoins on peut aussi arriver par avion grâce à l' [MASK] [MASK] [MASK] qui est à 22 km .
La [MASK] [MASK] [MASK] [MASK] traverse aussi la ville , mais aucune gare n' existe pour prendre ou descendre des passagers , il faut aller jusque [MASK] , [MASK] , [MASK] ou encore [MASK] pour prendre un [MASK] .
L' évolution du nombre d' habitants depuis 1793 est connue à travers les recensements de la population effectués à [MASK] depuis cette date :
Le site d' [MASK] a été très tôt occupé : l' existence d' un village gaulois prospère est attestée par les fouilles .
Il est généralement admis que [MASK] viendrait du latin hinniens ( hénissant ) , ce qui expliquerait les armoiries de la ville ( un cheval au galop ) et que la plupart des pièces gauloises retrouvées portent un cheval ( d' aucun proposent la thèse d' un centre d' élevage de chevaux renommé en ces temps anciens ) .
D' autres vont chercher l' origine d' [MASK] dans les origines des populations ( celtes et germano-belges ) .
En 1852 , la découverte de la houille dans le bassin minier du [MASK] fait entrer [MASK] dans la modernité .
Il fit d' [MASK] la seule ville défendue par ses habitants que les prussiens ne prirent pas .
C' est un autre socialiste , [MASK] [MASK] qui lui succède .
Après la fermeture du dernier puits de mine en octobre 1970 , [MASK] [MASK] doit négocier la difficile transition d' [MASK] d' une ville minière à une ville tournée vers le commerce et les services .
Pour une ville de l' ancien [MASK] [MASK] [MASK] [MASK] , les terrils y sont implantés .
Le centre-ville d' [MASK] est un peu démesuré par rapport au centre commercial à [MASK] qui est à cinq minutes environ .
La dégradation de la situation financière de la commune entre 2002 et 2009 évoque d' autres villes célèbres pour leurs difficultés , comme la commune d' [MASK] ou celle de [MASK] .
La ville de [MASK] est confrontée à une situation financière difficile diganostiquée depuis 2003 : endettement élevé , accumulation de déficits , manque de recettes de fonctionnement .
Dans un avis du 7 novembre 2003 , la chambre a proposé à la commune d' [MASK] un plan de rétablissement de son équilibre budgétaire par la résorption sur 3 exercices ( 2004 à 2006 ) du déficit constaté fin 2002 , établi à 12 390 703 € .