[MASK] [MASK] pour les catholiques est fêté le 1 er décembre .
À [MASK] , on commémore le 25 juin la translation d' un de ses bras en la [MASK] [MASK] [MASK] [MASK] en 1212 .
Le divin ouvrier ( summus faber ) aurait multiplié l' or tandis qu' [MASK] fabriquait le trône .
L' honnêteté d' [MASK] paya , [MASK] [MASK] le garda dans son entourage .
La confiance que porta le roi envers lui s' accrut lorsque [MASK] [MASK] voulu qu' [MASK] prête serment , par la pose de ses mains , sur de saintes reliques .
Devant l' insistance du roi , [MASK] pleura pour son offense envers [MASK] [MASK] , et redouta sept fois plus de porter la main sur de saintes reliques .
L' orfèvre [MASK] devint contrôleur des mines et métaux , maître des monnaies , puis grand argentier du royaume de [MASK] [MASK] , puis trésorier de [MASK] [MASK] [MASK] avant d' être élu [MASK] [MASK] [MASK] en 641 .
Il est aussi considéré comme le fondateur de l' [MASK] [MASK] [MASK] [MASK] située à l' ouest d' [MASK] .
[MASK] [MASK] est réputé fondateur de l' église de [MASK] .
Il y aurait , selon la légende , pacifié le géant [MASK] , pour protéger [MASK] des invasions et pillages des [MASK] .
Quoique étant encore laïc , il fut élevé en 640 sur le siège de [MASK] .
[MASK] [MASK] est généralement considéré comme le saint patron des ouvriers qui se servent d' un marteau , et plus précisément des orfèvres , graveurs , forgerons , mécaniciens , chaudronniers , horlogers , mineurs , taillandiers , batteurs d' or , doreurs , tisseurs d' or , monnayeurs , serruriers , cloutiers , fourbisseurs , balanciers , épingliers , aiguilliers , tireurs de fils de fer , ferblantiers , lampistes , loueurs de voiture , voituriers , cochers , vétérinaires , selliers , bourreliers , maréchaux-ferrants , charrons , carrossiers , charretiers , éperonniers , maquignons , fermiers , laboureurs , valets de ferme , pannetiers , vanniers , bouteillers , plonchoyeurs mais également du matériel et des militaires logisticiens .
Au dire de cette légende , [MASK] , simple maréchal-ferrant , s' était installé à son compte et avait accroché à sa porte une enseigne ainsi conçue : " [MASK] .
Le [MASK] s' habilla donc comme un simple et pauvre forgeron et vient demander de l' embauche à l' atelier d' [MASK] .
Du coup , [MASK] capitule .
[MASK] comprend enfin à qui il a affaire et se prosterne .
Cette légende tente d' expliquer pourquoi [MASK] apparaît sur de très anciennes gravures , tenant une jambe de cheval à la main .
[MASK] , à l' époque , n' est rien .
Il s' appelait [MASK] .
Les pêcheurs s' approchèrent ensuite du corps d' [MASK] : ainsi , c' était donc lui , le géant , le monstre qui avait fait périr leurs enfants et pleurer leurs femmes ! ...
[MASK] [MASK] , justement , passait par là .
Voyant les pêcheurs affairés , il se précipita , les écarta , bénit [MASK] et le fit porter dans sa demeure .
Personne n' osait rien dire ou demander , tant était grande l' autorité d' [MASK] .
Le seizième jour enfin , [MASK] [MASK] fit sortir le géant , guéri , et l' amena à l' église , où il le baptisa et le maria à la plus belle fille du pays .
[MASK] s' y installa à demeure , devint le chef des habitants à qui il fit construire des remparts , des tours et des bâtiments .
Ainsi , selon la légende , naquit [MASK] .
Cette église fut dédiée à [MASK] [MASK] .