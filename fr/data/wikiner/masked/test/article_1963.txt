[MASK] et [MASK] étaient à nouveau consuls lorsque , 16 ans plus tard , en -54 , le jour même , dit-on , où disparaissait [MASK] , le jeune homme revêtit la toge virile .
Mais , c' est [MASK] qui deviendra son intime au point de l' appeler animae dimidium meae , " la moitié de mon âme " .
Il semble que le domaine paternel de [MASK] fut confisqué , et ses légitimes propriétaires faillirent même y laisser la vie .
[MASK] mourut en -19 à [MASK] .
On dit qu' il eût voulu que l' on brûlât l ' [MASK] après sa mort .
Les [MASK] sont beaucoup moins un traité d' agriculture ( aussi ne visent-elles pas à l' exhaustivité ) qu' un poème sur l' agriculture ; elles s' adressent au moins autant à l' homme des villes qu' à l' homme des champs .
[MASK] sait agrémenter son sujet d' épisodes variés et de véritables morceaux de bravoure qui sont autant de " respirations " dans le poème .
[MASK] ne cache d' ailleurs nullement son ambition .
Au niveau architectural le plus visible ( car [MASK] [MASK] fait jouer simultanément plusieurs " géométries " ) , le poème se compose d' une [MASK] suivie d' une [MASK] .
Mais l' émulation avec [MASK] se manifeste surtout par le nombre considérable des imitations textuelles , dont les critiques s' employèrent très tôt à dresser la liste , cela quelquefois dans une intention maligne , et pour accuser [MASK] de plagiat .
À quoi celui-ci répliquait qu' il était plus facile de dérober sa massue à [MASK] que d' emprunter un vers à [MASK] .
Et de fait , loin qu' elle soit servile ou arbitraire , l' imitation virgilienne obéit toujours à une intention précise et poursuit un projet qu' il appartient au lecteur de découvrir à travers l' écart , parfois minime , qui la sépare de son modèle , [MASK] ou l' un des nombreux autres écrivains , tant grecs que latins , auxquels [MASK] se mesure tout en leur rendant hommage .
Ce jeu intertextuel presque illimité n' est pas la moindre source de la fascination qu' exerça toujours [MASK] [MASK] sur les lettrés .
Le second défi consistait à filtrer l' actualité de [MASK] à travers le prisme de la légende .
C' est du moins ce qu' [MASK] attendait , ou plutôt ce qu' il exigeait de lui .
Le protagoniste du poème ne serait pas [MASK] [MASK] mais [MASK] . "