Selon le célèbre historien [MASK] , les forces perses engagées dans les guerres médiques à cette période dépassaient les cinq millions d' hommes .
Il estime qu' [MASK] aurait confondu les termes perses " chilliarchie " ( 10000 ) et " myiarchie " ( 1000 ) , évaluant ainsi les forces comme étant dix fois plus importantes qu' elles ne l' étaient .
Les préparatifs perses ne sont évidemment pas passés inaperçus et un congrès des différentes cités grecques se réunit à [MASK] à la fin de l' automne -481 .
Pour une fois , les intérêts immédiats de [MASK] et d' [MASK] se confondent .
[MASK] , en tant que plus puissante des cités , préside le congrès .
Le commandement des troupes est confié à deux [MASK] , le roi [MASK] [MASK] [MASK] pour les fantassins et [MASK] pour la flotte grecque .
Entre les 6000 hommes environ dont dispose [MASK] et la flotte d' [MASK] , les liaisons sont constantes .
Au sortir de la [MASK] , les troupes de [MASK] font mouvement vers le sud .
Les fantassins quittent la cité de [MASK] et arrivent treize jours plus tard dans la plaine trachinienne .
La principale conséquence est que [MASK] , bien que gardant la supériorité numérique , n' est plus en mesure de diviser ses forces navales de manière à convoyer l' armée tout en livrant combat à la flotte grecque .
Surtout , cette nouvelle tempête cause la destruction totale de l' escadre envoyée pour contourner l' [MASK] .
Les 400 combattants de [MASK] avaient aussi reçu l' ordre de participer à cette dernière action , mais ils désertent à la première occasion .
Ils résistent héroïquement autour du roi spartiate [MASK] , qui est tué .
Pour son corps , les [MASK] se battent sans relâche et parviennent , en repoussant avec acharnement les assauts perses , à le récupérer .
Ils se replient avec le peu d' armes qui leur reste sur une butte , mais l' intervention perse leur est fatale et ils sont tous massacrés sur ordre de [MASK] .
Cette bataille devint l' emblème de la résistance grecque à l' envahisseur et de l' esprit de sacrifice des [MASK] .