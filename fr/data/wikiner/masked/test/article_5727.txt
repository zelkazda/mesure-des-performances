Il est le père de [MASK] [MASK] [MASK] .
L' historien [MASK] [MASK] dépeint [MASK] [MASK] [MASK] comme " une personnalité violente et difficile " .
Sa vie a donné lieu à la naissance d' histoires plus ou moins légendaires comme sa rencontre avec [MASK] , qui deviendra sa " frilla " ( épouse à la manière danoise ) , ou son passage à [MASK] .
Sa mauvaise réputation provient en partie des conditions douteuses qui lui permirent d' accéder au trône de [MASK] .
[MASK] [MASK] [MASK] était le second fils du duc [MASK] [MASK] [MASK] [MASK] .
L' armée ducale se présenta alors devant [MASK] où s' était retranché le rebelle .
[MASK] [MASK] [MASK] capitula et se soumit à son frère .
Mais en 1027 [MASK] [MASK] mourut empoisonné .
Il apparaît en effet comme le principal bénéficiaire de la mort de [MASK] [MASK] .
Il montra rapidement qu' il entendait tenir la [MASK] d' une main de fer .
Le duc vint l' assiéger dans [MASK] .
Le rebelle fut contraint à la reddition mais [MASK] [MASK] [MASK] lui imposa une pesante humiliation : il l' obligea à se présenter devant lui avec une selle de cheval sur les épaules .
Le conflit entre le duc et l' [MASK] [MASK] [MASK] est peut-être en lien avec celui entre [MASK] [MASK] [MASK] et son oncle [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] nous explique que [MASK] [MASK] [MASK] se déclara son ennemi , sans que l' on sache les raisons de cette soudaine opposition .
Les historiens ont remarqué en effet qu' au début de son principat , [MASK] [MASK] [MASK] enleva des terres aux abbayes et aux grandes églises pour les distribuer à de jeunes nobles ( tel [MASK] [MASK] [MASK] [MASK] [MASK] ) .
[MASK] [MASK] [MASK] lui a-t-il adressé des remontrances pour ces usurpations ?
Toujours est-il que le duc s' enflamma contre lui et partit faire le siège d' [MASK] en 1027/1028 .
Il choisit l' exil et se rendit auprès du roi [MASK] [MASK] [MASK] .
La sanction ecclésiastique fit son effet : [MASK] [MASK] [MASK] rappela [MASK] [MASK] [MASK] et le rétablit dans ses charges comtales et archiépiscopales .
Ce conflit entre l' archevêque et le duc semble constituer une flexure dans la politique religieuse de [MASK] [MASK] [MASK] .
Tout d' abord , [MASK] [MASK] [MASK] signe des chartes à plusieurs abbayes pour confirmer leurs biens ou pour les restituer .
Les [MASK] [MASK] [MASK] et de [MASK] et la [MASK] [MASK] [MASK] [MASK] figurent parmi les bénéficiaires de ces actes .
En premier lieu , l' [MASK] [MASK] [MASK] .
Ensuite , en 1035 , [MASK] [MASK] [MASK] refonda l' [MASK] [MASK] [MASK] en remplaçant les moines par des moniales .
Avant de partir , le duc conscient de cette difficulté , rassembla les grands du duché à [MASK] .
Il leur demanda de reconnaître comme hériter son jeune fils [MASK] âgé d' environ sept ans .
L' empereur byzantin [MASK] [MASK] l' accueillit ensuite à [MASK] .
Son fils aîné [MASK] [MASK] [MASK] lui succéda mais il se heurta à une révolte de son frère cadet [MASK] , appuyé par sa mère [MASK] [MASK] [MASK] .
Les historiens [MASK] [MASK] , [MASK] [MASK] et plus récemment [MASK] [MASK] doutent de l' affirmation du chroniqueur anglo-normand .
Vers 1030 , ce dernier dut faire face à une rébellion de son fils [MASK] .
[MASK] [MASK] [MASK] entra en [MASK] et brûla le château de [MASK] .
La cour normande accueillait depuis le principat de [MASK] [MASK] [MASK] [MASK] les deux fils du roi anglo-saxon [MASK] [MASK] .
Au contraire de son père , [MASK] [MASK] [MASK] s' engagea clairement pour les deux cousins exilés .
Il envoya à [MASK] une ambassade pour lui demander de rendre le royaume aux enfants d' [MASK] .
Les bateaux , chargés en vivres , en armes et en hommes , se rassemblèrent à [MASK] et prirent la mer mais une tempête déporta les navires vers [MASK] .
Depuis [MASK] , les [MASK] [MASK] [MASK] intervenaient régulièrement en [MASK] .
En 1008 , la mort du duc breton [MASK] [MASK] [MASK] laissait le pouvoir à sa femme [MASK] [MASK] [MASK] , sœur de [MASK] [MASK] .