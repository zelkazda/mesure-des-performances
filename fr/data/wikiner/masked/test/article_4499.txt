Autodidacte et rêvant de poésie , [MASK] [MASK] est introduit vers 1920 dans les milieux littéraires modernistes et rejoint en 1922 l' aventure surréaliste .
Il poursuit ses activités de [MASK] jusqu' à son arrestation le 22 février 1944 .
La dépouille du poète est rapatriée en [MASK] , et [MASK] [MASK] est enterré au [MASK] [MASK] [MASK] à [MASK] .
[MASK] [MASK] naît à [MASK] au 32 , [MASK] [MASK] .
En 1902 , la famille s' installe dans le quartier populaire des [MASK] où son père est mandataire pour la volaille et le gibier , mais également adjoint au maire de l' arrondissement .
Ils habitent 11 , [MASK] [MASK] , dans " ce coin de [MASK] qui sent le soufre " où , jadis , les alchimistes et autres sorciers se livraient à d' étranges métamorphoses .
[MASK] [MASK] [MASK] avait d' ailleurs trouvé là une source à ses voyages imaginaires .
En 1913 , la famille déménage pour le 9 , [MASK] [MASK] [MASK] , un autre univers .
Mais ce [MASK] interlope des artisans et des commerçants marque profondément l' enfant et apparaîtra abondamment dans son œuvre .
À six ou sept ans , [MASK] dessine d' étranges formes sur ses cahiers .
Élevé dans un milieu petit-bourgeois , [MASK] fait sa première communion en 1911 en l' [MASK] [MASK] .
Il préfère lire [MASK] [MASK] de [MASK] et s' embarquer avec les marins de [MASK] .
Il se passionne aussi pour la culture populaire : romans -- [MASK] [MASK] , [MASK] [MASK] ou [MASK] [MASK] [MASK] -- , et bandes dessinées , avec une affection particulière pour l' insaisissable [MASK] dont les exploits sont relatés au cœur d' ouvrages bariolés de couleurs .
De tout cela , [MASK] témoignera dans ses récits et ses critiques de films .
On le trouve , un temps , commis dans une droguerie de la [MASK] [MASK] , mais le plus important est ailleurs : [MASK] , buvant l' eau vive de ce qui s' offre à lui , se forge une solide et vaste culture autodidacte .
Pendant qu' il joue les tirailleurs entre dattiers et palmiers en s' efforçant de tromper son ennui comme il peut , à [MASK] , les dynamiteurs de la pensée officielle comme de l' ordre social ont lancé leurs premières grenades .
[MASK] [MASK] avait parlé de [MASK] à [MASK] avant son départ pour l' armée .
Sans doute est-ce au cours d' une permission que le troufion [MASK] établit enfin le contact avec " ces compteurs d' étoiles " , selon le mot de [MASK] [MASK] .
[MASK] monte dans la nacelle sans se faire prier , car il a déjà expérimenté à sa façon l' écriture automatique , forme d' expression aussi peu contrôlée que possible .
Voir au-delà ou au-dedans ... [MASK] s' impose immédiatement par ses exceptionnelles capacités verbales ( un flot de paroles intarissable où les mots s' appellent par affinités sonores ) et met sa fougue à entrer dans les expériences les plus diverses .
Le rêve , cette porte ouverte sur l' inconnu , [MASK] l' a déjà entrebâillée .
De fait , [MASK] est un voyant : il est ce medium qui , endormi , répond aux questions des assistants , amorce des poèmes ou des dessins .
Lors de ces séances des sommeils , ( la première a lieu chez [MASK] le 25 septembre 1922 ) il est question d' aller retrouver la liberté première de la pensée ayant élu domicile dans cet état de somnolence/rêverie que [MASK] avait nommé supernaturaliste .
[MASK] revient , à la fois magicien et sorcier et pénètre les mots .
Ce voyage expérimental vers le verbe nouveau est une impasse , et [MASK] le sait .
Peu importe , il faut partir sur les routes , selon le mot de [MASK] .
[MASK] incarne cela plus que tout autre .
Dans les années 1924-1929 , [MASK] est rédacteur de [MASK] [MASK] [MASK] .
[MASK] écrira de nombreux scénarios .
Le cinéma du rêve , [MASK] [MASK] ou [MASK] [MASK] , est encore trop pauvre pour le satisfaire , mais il fait affaire avec ce qu' il voit et multiplie les critiques .
[MASK] voue alors une passion à l' émouvante chanteuse de music-hall [MASK] [MASK] .
Si l' on en croit [MASK] [MASK] , l' ami fidèle , cet amour ne fut jamais partagé .
Une occasion pour [MASK] de renouer avec le lyrisme .
Cette mystérieuse , [MASK] lui a donné un visage et une voix .
[MASK] [MASK] est devenu une sorte de [MASK] qui agace certains de ses amis .
[MASK] , auquel ce dernier reproche son narcissisme , est de ces êtres libres qui , jamais , ne plieront devant qui ou quoi que ce soit , fût-ce le rêve du surréalisme .
En plus , [MASK] reproche au poète de faire du journalisme , sorte de tare absolue .
Il y a autre chose : [MASK] veut entraîner les siens vers le communisme et [MASK] ne franchit pas cette ligne .
Dans " [MASK] [MASK] [MASK] " , le groupe des dissidents , passe alors à l' action .
Pour [MASK] , il est temps de constater que [MASK] est dépassé , vieux , abêti et sent , effectivement , le cadavre .
La rupture est douloureuse , [MASK] se retrouve solitaire mais son chemin continue .
[MASK] est un grand amateur de musique .
Le jazz , la salsa découverte lors d' un voyage à [MASK] en 1928 , le tango , le fado et les disques de [MASK] , [MASK] , [MASK] et [MASK] [MASK] , rengaines reflétant bien son [MASK] populaire , meublent sa discothèque .
Mais on y trouve aussi les 78 tours de [MASK] , [MASK] , [MASK] [MASK] et , surtout [MASK] .
Il devient vite assez célèbre et la radio lui offre des ressources que le journalisme de presse écrite ( il a quitté la plupart des quotidiens pour ne plus écrire que dans des hebdomadaires édités par la [MASK] ) ne lui assurent plus .
[MASK] [MASK] qui assure la direction dramatique tient le rôle de [MASK] , tandis qu' [MASK] [MASK] est responsable de la mise en onde sonore .
L' expérience radiophonique transforme la pratique littéraire de [MASK] : de l' écrit celle-ci se déplace vers des formes plus orales ou gestuelles .
L' essentiel pour [MASK] est maintenant de communiquer , et la littérature est un moyen parmi d' autres .
Alors que la conjoncture internationale devient de plus en plus menaçante , [MASK] renonce à ses positions pacifistes : [MASK] [MASK] doit , selon lui , se préparer à la guerre , pour défendre l' indépendance de [MASK] [MASK] , sa culture et son territoire et pour faire obstacle au fascisme .
Mobilisé en 1939 [MASK] fait la drôle de guerre convaincu de la légitimité du combat contre le nazisme .
Après l' arrestation de [MASK] , le quotidien est rapidement soumis à la censure allemande mais [MASK] ruse , surveille ses paroles et réussit à publier , mine de rien , selon son expression , des articles de littérature qui incitent à préparer un avenir libre .
Pour [MASK] , la lutte est désormais clandestine .
Dès juillet 1942 , il fait partie du [MASK] [MASK] , auquel il transmet des informations confidentielles parvenues au journal , tout en fabriquant par ailleurs de faux papiers pour des [MASK] ou des résistants en difficulté .
Ses ennemis essaieront d' ailleurs de le faire passer pour [MASK] , ce qui signifie la mort .
" ce n' est pas la poésie qui doit être libre , c' est le poète " , dit [MASK] .
Interrogé [MASK] [MASK] [MASK] , il finit à la [MASK] [MASK] [MASK] , dans la cellule 355 de la deuxième division .
Le 20 mars , il est transféré au [MASK] [MASK] [MASK] à [MASK] où il trouve la force d' organiser des conférences et des séances de poésie .
Il y arrive le 12 mai et repart deux jours plus tard pour [MASK] : le convoi , cette fois , ne compte qu' un millier d' hommes .
Le 14 avril 1945 sous la pression des armées alliées , le kommando de [MASK] est évacué .
Vers la fin du mois d' avril la colonne est scindée en deux groupes : les plus épuisées -- dont [MASK] -- sont acheminés jusqu' au [MASK] [MASK] [MASK] [MASK] [MASK] , à [MASK] ( [MASK] ) , les autres sont abandonnés à eux-mêmes .
[MASK] est de ceux-là .
Le 3 mai 1945 , les [MASK] prennent la fuite ; le 8 mai , l' [MASK] [MASK] et les partisans tchèques pénètrent dans le camp .
Sur une paillasse , en pyjamas rayés , tremblant de fièvre , [MASK] n' est plus qu' un matricule .
[MASK] a tout juste eu la force de se relever en entendant son nom et de souffler " Oui , oui , [MASK] [MASK] , le poète , c' est moi " .
Le 8 juin 1945 , à cinq heures du matin , [MASK] [MASK] meurt .
La poésie de [MASK] , c' est la poésie du courage .
[MASK] [MASK] est enterré au [MASK] [MASK] [MASK] à [MASK] .
En réalité , ce texte est le résultat d' une traduction approximative à partir du tchèque de la dernière strophe d' un poème de [MASK] écrit en 1926 et dédié à [MASK] [MASK] : J' ai tant rêvé de toi :
On sait que dans l' infirmerie du camp et vu son état moribond , [MASK] n' a eu ni physiquement ni matériellement la possibilité d' écrire quoi que ce soit .
L' article , traduit du tchèque en français ( traduction de traduction ) , paraît le 11 août 1945 dans [MASK] [MASK] [MASK] .
La voix de [MASK] [MASK] résonne désormais dans un poème qui a cessé de lui appartenir pour devenir la voix de tous .