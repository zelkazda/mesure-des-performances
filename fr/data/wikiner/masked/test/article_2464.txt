Comme son arme favorite est la fronde , il se fait désormais appeler " [MASK] [MASK] [MASK] " .
On reconnaîtra dans le sujet de la série une démarque , à peine voilée , de [MASK] [MASK] [MASK] .
Si le paysage de la série est bien un paysage de [MASK] , [MASK] est une commune de la [MASK] .
Le [MASK] [MASK] [MASK] à [MASK] a servi de décor pour la série .
Cette série bénéficia de la présence de [MASK] [MASK] comme acteur principal , très charismatique , et également d' un thème musical approprié jouant de cors , et d' un rythme évoquant le galop d' un cheval .