Le [MASK] [MASK] [MASK] est un canular littéraire antimaçonnique français , sans doute le plus célèbre .
Pour rendre son canular plus crédible , [MASK] mélangeait des éléments réellement tirés de rituels maçonniques avec des déformations de son invention .
[MASK] [MASK] dévoilera finalement son " aimable plaisanterie " le 19 avril 1897 .
[MASK] [MASK] [MASK] [MASK] a défendu l' hypothèse que [MASK] [MASK] avait bel et bien existé mais qu' elle aurait été enlevée au moment de révéler son existence .