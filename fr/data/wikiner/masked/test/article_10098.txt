Il inaugure , avec [MASK] [MASK] [MASK] , la période romantique de la littérature anglaise lors de la publication de [MASK] [MASK] ( 1798 ) .
[MASK] entre au [MASK] [MASK] [MASK] [MASK] de [MASK] en 1787 .
En 1790 , il se rend en [MASK] et soutient les républicains de la [MASK] [MASK] .
Il obtient son diplôme l' année suivante , sans mention , puis part pour un tour d' [MASK] incluant les [MASK] et l' [MASK] .
La même année , le manque d' argent le contraint à retourner , seul , en [MASK] .
Accusé d' être girondin sous la [MASK] [MASK] , il prend ses distances avec le mouvement républicain français ; de plus , la guerre entre [MASK] [MASK] et la [MASK] l' empêche de revoir sa femme et sa fille .
Il rencontre [MASK] [MASK] [MASK] dans le [MASK] cette même année .
Ensemble , ils publient [MASK] [MASK] ( 1798 ) , qui s' avère d' importance capitale pour le mouvement romantique en [MASK] .
Pour un temps , [MASK] et [MASK] se sont éloignés l' un de l' autre en raison de l' addiction de ce dernier à l' opium .
Elle est énoncée par [MASK] dans la préface qu' il a rédigée pour la seconde édition des [MASK] [MASK] ( " Les [MASK] [MASK] " ) parue en 1801 .
Cette simplicité recherchée conduit [MASK] à privilégier les événements de la campagne .
Les thèmes choisis par [MASK] relèvent de ce qu' il appelle animal sensations et moral sentiments , c' est-à-dire la perception de la nature dans ses aspects élémentaires et tous les sentiments et toutes les passions de l' homme .
L' œuvre poétique de [MASK] est immense :
C' est d' ailleurs sur ces mêmes vers que se termine le film , avec le visage et la voix off de [MASK] [MASK] ( qui partage l' affiche du film avec [MASK] [MASK] ) .