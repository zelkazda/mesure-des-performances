En 1912 , un décret du 26 octobre regroupe à [MASK] les quatre sections normales sous le nom d' [MASK] [MASK] de l' enseignement technique .
Cette nouvelle école s' installe dans les locaux de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
En 1956 , l' [MASK] s' installe sur le campus de [MASK] au sud de [MASK] , construit par les architectes [MASK] [MASK] puis [MASK] [MASK] qu' elle ne quitte plus ( construction commencée en 1937 mais achevée uniquement en 1955 ) .
En 1960 , un décret du 21 novembre énonce que l' [MASK] prépare à l' agrégation ; la durée de la scolarité passe à quatre années .
Les sections littéraires sont déplacées à l' [MASK] [MASK] [MASK] en 1976 , alors que sont instaurées des formations en sciences pures ( mathématiques , physique , chimie , biochimie ) , en sciences sociales et de gestion .
En 1985 , l' [MASK] [MASK] [MASK] remplace l' [MASK] et acquiert un statut identique aux autres [MASK] [MASK] [MASK] .
L' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] est créée en 1994 .
Elle est la seule des trois [MASK] [MASK] [MASK] françaises à exercer ses missions à la fois dans les disciplines technologiques , scientifiques et de gestion .
La qualité de ses laboratoires ouvre l' [MASK] [MASK] [MASK] sur l' environnement scientifique et économique .
Le transfert de compétences scientifiques issues de la recherche dans les milieux industriels et économiques est une caractéristique de l' [MASK] [MASK] [MASK] .
Le cursus comprend communément la préparation de la licence , puis , en deux ans , du master et/ou une préparation à l' agrégation pendant les 48 mois rémunérés de scolarité à l' [MASK] [MASK] [MASK] .
Les concours sont généralement organisés dans le cadre de banques d' épreuves communes aux quatre [MASK] [MASK] [MASK] .
Certains étudiants sont recrutés sur dossier dans le cadre d' un magistère ou d' un master selon les départements ; ils ont le statut d' élève de l' [MASK] [MASK] supérieure mais n' acquièrent pas celui de fonctionnaire stagiaire .
L' [MASK] [MASK] [MASK] , à l' instar des autres [MASK] [MASK] [MASK] , tisse de nombreux liens avec la recherche .
Treize laboratoires liés au [MASK] sont présents au sein de l' école et couvrent l' ensemble des disciplines enseignées à l' école .
L' [MASK] [MASK] [MASK] accueille ainsi un nombre important d' enseignants-chercheurs et de doctorants .