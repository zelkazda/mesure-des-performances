[MASK] [MASK] est un créateur de jeu vidéo japonais .
Il est surtout connu pour être le créateur de la célèbre franchise de jeux de rôle [MASK] [MASK] .
Il imagine , produit et supervise de nombreux jeux chez [MASK] .
Très attaché à la narration dans ses jeux , il a travaillé au développement du scénario dans les jeux [MASK] , à tel point que l' entreprise s' est rapidement spécialisé dans le jeu vidéo de rôle à l' intrigue complexe .
Il travaille à mi-temps pour [MASK] et prend part à la réalisation des premiers jeux vidéos de l' entreprise .
Il travaille sur plusieurs jeux comme [MASK] [MASK] ( mars 1987 ) , un jeu d' action ou le joueur incarne un aventurier qui doit détruire les monstres sur plusieurs planètes , inspiré de [MASK] [MASK] de [MASK] sur arcade , ou [MASK] [MASK] ( août 1987 ) , le premier jeu de course de la [MASK] , fortement inspiré d ' [MASK] [MASK] , lui aussi de [MASK] sur arcade .
Mais son projet est peu populaire à [MASK] et il décide d' en faire son dernier jeu vidéo .
Grâce à sa persévérance et au bon accueil du magazine japonais [MASK] , le produit bénéficie d' une importante distribution et se vend à 400.000 exemplaires .
Avec un total de 9,8 millions d' unités écoulées , il devient la deuxième meilleure vente dans l' histoire de la [MASK] .
En 1996-1997 , il travaille sur de nouveau concepts de jeux pour la [MASK] .
Le jeu est réalisé par [MASK] [MASK] et reprend l' apparence et le système de jobs introduit dans les premiers [MASK] [MASK] .
Il imagine et produit le jeu [MASK] [MASK] , inspiré d' un roman japonais .
La réalisation du produit est confié à son ami et collègue de longue date [MASK] [MASK] .
Il tient le rôle de producteur délégué sur plusieurs projets de [MASK] comme [MASK] ( 1998 ) de [MASK] [MASK] ou [MASK] [MASK] ( 1999 ) de [MASK] [MASK] .
À la fin des années 1990 , il ne tient qu' un rôle très secondaire dans la création de [MASK] [MASK] [MASK] ( 1999 ) .
Il est très satisfait de la direction que prend le projet , commentant : " il s' agit de l' épisode le plus proche de la vision que je me fait de ce que devrait être [MASK] [MASK] . "
Il co-réalise donc [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et une fois de plus s' attache à essayer de faire passer l' émotion via des images créés par ordinateur .
En 2002-2003 il travaille seulement en free-lance pour [MASK] .
Il est producteur délégué sur [MASK] [MASK] en 2002 ainsi que [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] en 2003 .
Pourtant fin 2004 , il est avide de se lancer dans de nouveaux projets et fonde [MASK] , un studio principalement formé pour créer l' ensemble de la pré-production d' un jeu .
[MASK] [MASK] crée les personnages de [MASK] [MASK] , tandis que [MASK] [MASK] dirige le graphisme de [MASK] [MASK] .