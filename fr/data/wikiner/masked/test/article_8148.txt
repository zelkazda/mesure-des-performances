L' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ou [MASK] ( que l' on peut prononcer " i trois e " ) est une organisation à but non lucratif .
L' [MASK] compte plus de 325000 membres , et possède différentes branches dans plusieurs parties du monde .
L' [MASK] est constituée d' ingénieurs électriciens , d' informaticiens , de professionnels du domaine des télécommunications , etc .
L' [MASK] a plusieurs activités généralement associées aux organisations professionnelles ou autres , telles que l' édition et la publication de revues scientifiques pour un total de 144 transactions , revues , journaux et magazines .
Beaucoup de personnes reprochent à l' [MASK] une position proche du monopole dans certains domaines scientifiques .
Lors d' une publication dans une revue de l' [MASK] , l' auteur doit abandonner ses droits d' auteur en faveur de l' [MASK] qui ensuite vend l' article dans son journal en ligne sans aucun bénéfice pour l' auteur ou les relecteurs .
Les frais de participation aux rencontres sont très élevés , d' autant plus que la publication dans l' un des journaux [MASK] est presque obligatoire pour obtenir une reconnaissance dans certaines communautés scientifiques .
L' [MASK] joue un rôle très important dans l' établissement de normes .