L' initiateur est [MASK] ( " né de la lune " ) , premier roi de cette époque .
Cette période commençe par la fin du règne [MASK] qui occupait la [MASK] .
Vers -1570 , le roi [MASK] ne contrôle plus que le nord de l' [MASK] depuis [MASK] , sa capitale .
[MASK] , futur roi thébain prend la ville de [MASK] lors d' une seconde attaque et place ses troupes devant la ville d' [MASK] qu' ils prennent .
Il en est fini des [MASK] , les territoires anciennement égyptiens sont remis sous contrôle .
Cependant , les territoires au-delà de la vallée du [MASK] ne sont pas bien contrôlés , notamment les principautés palestiniennes qui , bien que contrôlées par un conseiller égyptien , restent sous la domination des populations autochtones .
Cette fonction est très importante car elle est susceptible d' apporter beaucoup d' influence politiquement ( à cause de l' oracle d' [MASK] ) .
Cette époque voit aussi [MASK] , dieu à l' origine seulement local , élevé au rang de dieu dynastique ( constructions de temples dans tout le pays ) .
À la mort de [MASK] [MASK] , c' est le fils d' une épouse secondaire , [MASK] [MASK] , qui monte sur le trône .
[MASK] s' impose au niveau politique .
L' histoire retiendra une [MASK] pacifique et un [MASK] [MASK] belliqueux et parfois même un peu trop énergique : au cours d' une chasse de cent vingt éléphants dans une plaine de l' [MASK] , le roi avait fait une démonstration personnelle de son audace , mais un officier avait dut intervenir pour le tirer d' une situation périlleuse .
À la mort de la femme pharaon , les troupes de [MASK] [MASK] atteignent [MASK] .
Après un siège de sept mois , la ville de [MASK] se rend , suivi de nombreuses principautés qui reconnaissent la suprématie égyptienne .
Les vingtaines d' années suivantes sont occupées par des campagnes visant à asseoir l' emprise égyptienne sur la [MASK] centrale .
Pour intervenir rapidement contre une rébellion possible à [MASK] , deux bases sont établies , à l' embouchure de l' [MASK] , à [MASK] et peut être aussi à [MASK] .
Dès la fin de l' occupation des [MASK] , on s' empresse de reconquérir les territoires du [MASK] .
Le tribut reçu du [MASK] est administré par le [MASK] [MASK] [MASK] à [MASK] .
On introduit le culte du roi et de ses ancêtres dans les régions du [MASK] en édifiant de nouveaux temples au sud de [MASK] .
[MASK] [MASK] [MASK] [MASK] , érige pour le roi un imposant temple funéraire .
Cette époque est aussi celle de la naissance du culte d' [MASK] , dont le nom est habituellement donné au disque solaire .
[MASK] fait de l' ombre à [MASK] qui est contesté par la cour , et ainsi naît une dispute entre celle-ci et les grandes familles égyptiennes .
Il a sans doute baigné depuis sa plus tendre enfance dans les discussions sur le culte dynastique du roi et de son dieu [MASK] .
Sa femme , [MASK] , et sa fille , [MASK] , exercent la fonction sacerdotale des anciennes épouses du dieu .
Petit à petit , on supprime le nom d' [MASK] dans les lieux de cultes égyptiens , et la propriété d' [MASK] change de culte .
En changeant de capitale , il change son nom pour être plus connu sous le nom d' [MASK] , probablement par provocation aux règles religieuses précédentes ; il centralise le culte solaire au palais royal avec le temple du palais d' [MASK] et un temple funéraire , et sa tombe est volontairement édifiée loin de celles des fonctionnaires de la cour dans le désert oriental .
Souvent on représente [MASK] par des statues relativement laides au niveau de l' expression , ceci étant dû à la modification des canons artistiques , et non forcément à la réalité physique .
Pour pallier ce problème , le pharaon envoie d' abord des troupes nubiennes en [MASK] pour assurer la sécurité de l' administration égyptienne , mais il finit par régler la situation par un mariage diplomatique en épousant une fille du roi kassite de [MASK] .
Le gouvernement revient à [MASK] , quittant [MASK] , et la tombe du successeur d' [MASK] est construite comme à l' habitude à [MASK] .
C' est sous l' influence d' [MASK] que [MASK] , une des filles d' [MASK] et de [MASK] , épouse le jeune [MASK] .
Son règne est bref , mais il reste pourtant un des personnages les plus connus de l' [MASK] [MASK] .
À sa mort , c' est le vieux [MASK] qui monte sur le trône , mais il meurt très peu de temps après .
Le haut commandant [MASK] , chef de l' armée à [MASK] , prend le pouvoir après le règne controversé d' [MASK] , et se fait confirmer cette prise de pouvoir par un oracle d' [MASK] à [MASK] .
[MASK] , puis [MASK] [MASK] [MASK] et son fils [MASK] [MASK] [MASK] effectuent de multiples réformes à l' intérieur du pays .
Séthi réhabilite le nom d' [MASK] et les anciens sanctuaires et fait déclarer [MASK] roi hérétique .
À l' extérieur il entreprend la consolidation des frontières réduisant les foyers de rebellions en [MASK] et aux frontières orientales .
Ces dernières fabriquaient des boucliers hittites pour des troupes auxiliaires grâce au cuivre tiré des mines de [MASK] en [MASK] .
[MASK] est aussi un point militaire stratégique , permettant une action rapide des troupes égyptiennes en cas de révoltes en [MASK] ou en [MASK] par exemple .
C' est ce que fait [MASK] [MASK] , fils et successeur de [MASK] [MASK] [MASK] , en engageant une importante guerre contre les [MASK] .
Lors de la [MASK] [MASK] [MASK] , les troupes égyptiennes sont attirées dans un piège .
En vain , car [MASK] parvient à les repousser à l' issue d' une victoire totale par terre et par mer .
Après le règne de celui-ci , la famille royale est déchirée par une guerre de succession que le trop grand nombre de descendants de [MASK] [MASK] rendait prévisible menaçant le pays d' anarchie .
Grâce à cette victoire et son butin correspondant , [MASK] [MASK] peut entreprendre la construction du temple de [MASK] [MASK] qui , signe des temps , est aussi une forteresse de par ses imposantes murailles et son portail d' accès en forme de migdol ( sorte de porte fortifiée renforcée de deux tours crénelées ) .
Plus tard il formeront de véritables chefferies qui feront les fondations des futurs dynasties de la [MASK] [MASK] [MASK] [MASK] .
De grands procès sanctionnent des scandales qui éclaboussent [MASK] à la fin de la dynastie .
[MASK] [MASK] , à la fin de son règne est victime d' une conspiration du harem qui atteint mortellement le roi et qui sera punie par son fils légitime et successeur [MASK] [MASK] .
Pour couronner le tout , la main-d'œuvre de la [MASK] [MASK] [MASK] travaillant aux tombes royales se met en grève faute d' approvisionnement régulier .
Toujours à [MASK] , de véritables associations de malfaiteurs n' hésitent pas à piller les tombes de leurs ancêtres dénotant certes une transgression morale impensable aux dynasties précédentes mais traduisant surtout une réalité économique plus catastrophique que ne le laissent imaginer les grandes œuvres du règne qui se résument parfois à un hypogée royal commandé par des souverains de plus en plus isolés dans leur fonction régalienne et qui n' avaient plus vraiment les moyens de les achever .
Le prix de cette victoire est la perte définitive de l' emprise égyptienne sur la [MASK] et le [MASK] qui désormais formeront un royaume indépendant .
Le clergé d' [MASK] , devenu une véritable dynastie , prend le pouvoir en [MASK] .