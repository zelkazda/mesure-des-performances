En informatique , [MASK] , abréviation de [MASK] [MASK] [MASK] , est le compilateur créé par le [MASK] [MASK] .
Il s' agit d' une collection de logiciels libres intégrés capables de compiler divers langages de programmation , dont [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
[MASK] est utilisé pour le développement de la plupart des logiciels libres .
Le [MASK] [MASK] dépend notamment étroitement des fonctionnalités de [MASK] .
[MASK] a été conçu pour remplacer le compilateur [MASK] fourni en standard sur le système d' exploitation [MASK] , qui s' appelle CC .
Comme [MASK] est très extensible , le support de nombreux autres langages a été ajouté et le nom officiel a été changé en [MASK] [MASK] [MASK] .
En pratique , l' abréviation [MASK] est utilisée pour nommer trois entités légèrement différentes :
[MASK] dispose également d' un outil de débuggage , [MASK] [MASK] ( gdb ) .
Bien que ne faisant pas partie de [MASK] , [MASK] est cependant préféré pour des tests plus en profondeurs , notamment pour rechercher les fuites de mémoire .
[MASK] a été porté sur un nombre considérable de systèmes d' exploitation ( pratiquement toutes les variantes d' [MASK] , [MASK] , [MASK] ) et de microprocesseurs .
Le développement de [MASK] a été commencé en 1985 par [MASK] [MASK] , fondateur de la [MASK] [MASK] [MASK] , dans un " dialecte " non portable de [MASK] .
En 1992 , la version 2.0 apporte en plus des nombreuses optimisations , le support du [MASK] [MASK] .
[MASK] est aujourd'hui le compilateur le plus utilisé dans la communauté des logiciels libres et est le compilateur de nombre de systèmes d' exploitations , comme [MASK] , les [MASK] , [MASK] [MASK] [MASK] , [MASK] ou encore [MASK] / [MASK] .
C' est , en effet , dans ce langage que s' échangent la plupart des bibliothèques source et sous-programmes scientifiques actuels [ réf. nécessaire ] ; or [MASK] est très présent dans les laboratoires .
[MASK] est souvent utilisé dans les [MASK] par le programme [MASK] .