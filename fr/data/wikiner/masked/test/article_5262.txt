L ' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , , généralement connue par son acronyme [MASK] ( ou [MASK] ) , est une organisation altermondialiste créée en [MASK] en 1998 .
L' association pour la taxation des transactions pour l' aide aux citoyens , [MASK] , est créée le 3 juin 1998 au cours d' une assemblée constitutive .
Depuis , [MASK] est présente dans 38 pays .
En [MASK] , des hommes politiques , des associations , des syndicats et des particuliers sont adhérents à l' association .
L' association s' est dotée en [MASK] dès l' origine , et aussi dans certains pays , d' un " conseil scientifique " qui se veut indépendant de toute structure universitaire ou officielle .
En [MASK] , ce conseil scientifique a été présidé successivement par deux professeurs d' économie , [MASK] [MASK] puis [MASK] [MASK] .
[MASK] a reçu en [MASK] le statut d' association d' éducation populaire par arrêté ministériel , ce qui lui permit notamment de recevoir des subventions .
Dès l' origine , [MASK] était une association très centralisée sur [MASK] .
Elle bénéficiait d' un local situé à proximité du siège du [MASK] [MASK] dont deux salariés consacraient la majeure partie de leur temps au développement d' [MASK] .
Quelques mois après sa création , [MASK] a embauché quatre permanents salariés .
Après 1999 , [MASK] a ainsi fonctionné sur deux modes d' organisation parallèles :
Ce sont les associations dites " fondatrices " qui avaient la réalité du pouvoir dans [MASK] ; du fait des statuts de 1998 rédigés pour se prémunir contre le risque d' entrisme .
Créée en 1998 , [MASK] [MASK] a été la première association [MASK] .
C' est une des deux associations [MASK] , avec [MASK] [MASK] , qui a actuellement le plus d' activité et de militants dans le monde .
Dans la brochure d' [MASK] de 2009 on lit que " près de 15000 personnes ont adhéré en 2007 " .
[MASK] [MASK] dispose en son sein d' un groupe d' études dénommé " comité scientifique " qui lui fournit des études approfondies , notamment sous la forme de livres et de brochures , et des analyses détaillées des questions d' actualité pouvant alimenter ses campagnes .
[MASK] est dirigé par un conseil de trente administrateurs , élus pour trois ans et rééligibles .
Ce dernier est constitué de représentants des organisations syndicales , journaux et associations ayant participé à la création d' [MASK] ainsi que de nombreux membres , personnes physiques et morales , cooptés depuis .
Au début de l' année 2006 , [MASK] était considéré par le journal [MASK] [MASK] comme étant en perte d' influence auprès du mouvement altermondialiste .
L' année 2006 a été une année difficile pour [MASK] [MASK] , avec une baisse du militantisme et de vives polémiques intestines liées à une fraude électorale commise à l' occasion du renouvellement du conseil d' administration .
Depuis la réélection de sa direction en juin 2006 , la branche française de l' association a traversé une crise interne grave , des accusations de fraude électorale ayant été notamment portées contre son président [MASK] [MASK] .
Plusieurs enquêtes internes , dont l' une à laquelle a notamment travaillé l' économiste et premier président de son comité scientifique [MASK] [MASK] , ont prouvé la réalité de cette " manipulation " .
Lors de la création de l' association en 1998 , on trouve parmi les membres fondateurs d' [MASK] [MASK] de nombreuses personnalités .
[MASK] est présente dans 38 pays : [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] [MASK] , [MASK] ( [MASK] ) , [MASK] , [MASK] , [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] .
Depuis , [MASK] a élargi son champ d' intervention et s' intéresse maintenant à tous les aspects qui se rapportent au cours dominant de la mondialisation économique , qu' elle qualifie de néolibérale .
[MASK] combat à ce titre les décisions de l' [MASK] , de l' [MASK] , de la [MASK] [MASK] ou du [MASK] , qu' elle ne voit pas comme des instances de régulation favorables .
Bien qu' [MASK] critique le fonctionnement de l' économie mondiale , qu' elle considère dominée par le " néolibéralisme " , elle ne se dit pas opposée à la mondialisation en général .
Les principaux sujets sur lesquels travaille [MASK] sont :
[MASK] participe aux manifestations altermondialistes dans le monde , dont celles visant à interpeller les grandes puissances lors des réunions internationales ( [MASK] , [MASK] , [MASK] , [MASK] , [MASK] [MASK] [MASK] , etc .
Pour diffuser ses idées , [MASK] cherche à être visible sur la scène publique et médiatique en utilisant divers moyens de communication : conférences , articles , communiqués de presse , manifestations diverses , campagnes de communication , projections de films documentaires , publications .
Deux slogans illustrent les thèses d' [MASK] : " [MASK] [MASK] n' est pas une marchandise " et " D' autres mondes sont possibles " .
[MASK] considère ne pas porter la responsabilité des arrachages de plants d' OGM effectués selon le " principe de précaution " , ni des affrontements avec les forces de l' ordre .
[MASK] publie des livres de vulgarisation présentant ses thèses sur les conséquences d' une économie jugée " néo-libérale " .
Depuis sa création , l' organisation interne d' [MASK] repose beaucoup sur internet ( listes de diffusions , site internet ) .
Diverses controverses existent sur le mode de fonctionnement , les méthodes , les propositions , les ambitions politiques d' [MASK] .
Selon l' écrivain libéral [MASK] [MASK] , cette incapacité à se mettre d' accord montre que ce qui :