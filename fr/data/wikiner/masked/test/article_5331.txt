En acceptant d' être couronné empereur par le pape [MASK] [MASK] en l' an 800 , [MASK] fonde son empire dans la continuité de l' [MASK] [MASK] , on parle de translatio imperii , bien que l' [MASK] [MASK] [MASK] [MASK] dit byzantin , se place également dans une continuité et cela de manière plus ancienne .
Il faut attendre le règne de [MASK] [MASK] pour la voir utilisée dans des documents en langue allemande .
En 1486 , cette titulature est utilisée par [MASK] [MASK] puis reprise officiellement en 1512 dans le préambule des actes de la diète de [MASK] .
[MASK] l' un des enfants meurt sans descendance , sa partie revient à l' un de ses frères .
C' est ce qu' il se passe lorsque [MASK] et [MASK] meurent et que l' héritage de [MASK] revient tout entier à [MASK] [MASK] [MASK] .
[MASK] se fait couronner sur le trône supposé de [MASK] à [MASK] le 7 août 936 .
À cette époque , le pape est menacé par les rois régionaux italiens et espère s' attirer les grâces d' [MASK] en lui faisant cette offre .
Mais le cri à l' aide du pape montre également que les anciens " barbares " deviennent les porteurs de la culture romaine et que le regnum oriental est le successeur légitime de [MASK] .
[MASK] accepte l' offre du pape et se rend à [MASK] .
Toutefois , [MASK] s' en veut le continuateur .
Lorsque le pouvoir central mérovingien décline à la suite des différentes divisions territoriales , les duchés ethniques comme ceux des [MASK] ou des [MASK] gagnent en indépendance .
Les duchés ethniques renaissent cependant vers 900 lorsque le pouvoir carolingien s' affaiblit : [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] .
Le [MASK] [MASK] [MASK] est divisé en 959 en [MASK] -- et [MASK] .
Le [MASK] [MASK] [MASK] naît de la division du [MASK] [MASK] [MASK] en 976 .
Depuis la chute de l' [MASK] [MASK] , gouvernent ceux qui ont la clientèle la plus puissante .
Ils peuvent aussi compter sur les mines d' argent de [MASK] qui leur permettent de battre la monnaie et de dynamiser encore plus le commerce .
Dans chaque diocèse , on peut ainsi trouver un membre de l' entourage royal car [MASK] a pris soin de retirer aux ducs le droit de nommer les évêques , y compris dans les diocèses situés dans leurs propres duchés .
[MASK] sait garder la mainmise sur les péages et développer les marchés nécessaires à l' augmentation de ce trafic .
Cette puissance commerciale lui permet d' étendre son influence à la périphérie de l' empire : les marchands italiens ou anglais ont besoin de son soutien , les [MASK] adoptent le denier d' argent .
Lorsqu' [MASK] [MASK] meurt en décembre 983 , il n' a que 28 ans .
Il avait fait sacrer son fils [MASK] , le futur [MASK] [MASK] , à [MASK] en mai 983 .
Mais en raison du jeune âge de ce dernier ( il n' a que trois ans ) , c' est sa mère [MASK] et sa grand-mère [MASK] [MASK] [MASK] qui exercent la régence .
Il profite pour cela du mouvement de réforme monastique en cours , promu par [MASK] ou des monastères lotharingiens tels [MASK] .
Les exemples les plus probants sont [MASK] qui se voit attribuer une véritable principauté à [MASK] , ou [MASK] [MASK] [MASK] qui reçoit l' archiépiscopat de [MASK] dont dépendent quinze évêchés .
C' est ainsi qu' il nomme pape son cousin [MASK] qui le couronne en 996 .
De plus , le pape nommé à discrétion et étranger ( [MASK] [MASK] est germain et [MASK] [MASK] franc ) n' a que peu de soutien à [MASK] et dépend d' autant plus de l' appui de l' empereur .
Ce processus avait commencé sous [MASK] [MASK] .
[MASK] poursuit certes la politique religieuse de son prédécesseur mais il ne le fait pas avec la même véhémence .
La soumission au nouveau roi n' est possible que si [MASK] [MASK] se révèle être un souverain juste .
À la mort de son père [MASK] [MASK] , son fils monte sur le trône sous le nom d' [MASK] [MASK] .
Étant donné son jeune âge en 1065 -- il a six ans -- sa mère [MASK] [MASK] [MASK] exerce la régence .
À [MASK] , l' avis du futur empereur sur le choix du prochain pape n' intéresse plus personne .
[MASK] [MASK] doit se plier à la volonté des princes et se rend par trois fois en habits de pénitent devant le pape qui lève l' excommunication le 28 janvier 1077 .
C' est la [MASK] [MASK] [MASK] .
En 1046 , [MASK] [MASK] avait commandé à trois papes , désormais un pape commande le roi .
Avec l' aide du pape [MASK] [MASK] , le futur [MASK] [MASK] obtient de son père qu' il abdique en sa faveur en 1105 .
Le nouveau roi n' est cependant reconnu par tous qu' après la mort d' [MASK] [MASK] .
Lorsqu' [MASK] [MASK] est sûr de cette reconnaissance , il se dresse contre le pape et continue la politique dirigée contre ce dernier que son père avait mise en place .
Tout d' abord , il s' applique à poursuivre la [MASK] [MASK] [MASK] contre [MASK] et obtient une conciliation avec le pape [MASK] [MASK] au [MASK] [MASK] [MASK] de 1122 .
Après la mort d' [MASK] [MASK] en 1125 , [MASK] [MASK] est élu roi , choix contre lequel va se dresser une forte résistance .
Les [MASK] qui avaient aidé [MASK] [MASK] espéraient en effet à juste titre accéder au pouvoir royal mais ce sont les [MASK] en la personne de [MASK] [MASK] [MASK] qui y accèdent .
Lothaire est dévoué au pape et lorsqu' il meurt en 1137 , ce sont les [MASK] en la personne de [MASK] [MASK] qui arrivent au pouvoir , les [MASK] en étant écartés .
Il veut récupérer les droits impériaux sur ce territoire et entreprend six campagnes en [MASK] pour retrouver l' honneur impérial .
Les relations diplomatiques se détériorent également avec [MASK] .
Les relations sont si mauvaises que la [MASK] [MASK] se crée , s' affirmant militairement contre les [MASK] .
L' élection du nouveau pape [MASK] [MASK] suscite la controverse , [MASK] se refuse à le reconnaître dans un premier temps .
Ce n' est qu' après avoir constaté qu' une victoire militaire n' était pas à espérer -- l' armée impériale est décimée par une épidémie devant [MASK] en 1167 puis elle est battue en 1176 à la [MASK] [MASK] [MASK] -- qu' est signée la [MASK] [MASK] [MASK] en 1177 entre le pape et l' empereur .
Son deuxième fils lui succède sous le nom d' [MASK] [MASK] .
Étant donné qu' il était marié à une princesse normande , [MASK] [MASK] [MASK] , et que la maison dont descend sa femme s' était éteinte faute de descendant mâle , [MASK] [MASK] peut faire valoir ses revendications sans toutefois pouvoir s' affirmer .
Le fils d' [MASK] [MASK] , [MASK] [MASK] , avait certes déjà été élu roi à l' âge de deux ans en 1196 mais ses droits à la royauté ont été vite balayés .
[MASK] [MASK] [MASK] s' était considérablement imposé mais il meurt assassiné en juin 1208 .
[MASK] [MASK] est couronné empereur en 1209 mais est excommunié par le pape [MASK] [MASK] l' année suivante .
[MASK] [MASK] soutient [MASK] [MASK] auxquels tous se rallient .
Il veut en effet faire élire et reconnaître son fils [MASK] comme son successeur .
À la fin , [MASK] [MASK] semble dominer militairement .
Depuis [MASK] [MASK] , la modernisation du système juridique attire dans la sphère culturelle française de nombreuses régions limitrophes .
Ainsi , la cour du roi [MASK] [MASK] est largement cosmopolite : beaucoup de seigneurs tels le [MASK] [MASK] [MASK] ont des possessions à cheval sur plusieurs royaumes .
Avec le déclin des [MASK] et l' interrègne qui s' ensuit jusqu' au règne de [MASK] [MASK] [MASK] , le pouvoir central s' affaiblit , tandis que le pouvoir des princes-électeurs s' accroît .
Le vieux conflit entre papauté et empire pour la prééminence sur la chrétienté se réanime sous le règne de [MASK] [MASK] .
À la mort de l' empereur [MASK] [MASK] en 1313 , les princes s' étant divisés en deux factions , le pape [MASK] [MASK] , entreprenant et autoritaire , croit pouvoir en profiter : il refuse de choisir entre les deux élus .
Ce conflit soulève une question de principe : le pape prétend être le vicaire de l' empire en [MASK] pendant la vacance du trône impérial .
Or , à ses yeux le trône est vacant puisque la désignation de [MASK] [MASK] [MASK] n' a pas obtenu l' approbation pontificale .
Des débats politico-théoriques sont engagés , par exemple par [MASK] [MASK] [MASK] ou [MASK] [MASK] [MASK] .
Créée en 1241 , elle regroupe un ensemble de plus de 300 villes dont [MASK] , [MASK] , [MASK] ou [MASK] .
La population est décimée à hauteur de moitié et les pogroms contre [MASK] [MASK] se multiplient .
Le fils du souverain , [MASK] , est même déchu par un groupe de princes-électeurs le 20 août 1400 du fait de son incapacité notoire .
Le camp catholique , en particulier l' empereur [MASK] [MASK] , ne veut pas d' une division religieuse durable .
[MASK] exhorte alors les paysans à la paix et prône la soumission à l' autorité .
Il s' attaque tout d' abord à la [MASK] et remporte la [MASK] [MASK] [MASK] en 1526 .
En 1529 , [MASK] est assiégée .
Sa tâche est d' autant plus malaisée que [MASK] [MASK] en la personne de son roi [MASK] [MASK] [MASK] soutient les [MASK] .
Cette rivalité avait été d' autant plus grande que [MASK] [MASK] er avait été le rival de [MASK] [MASK] lors de l' élection impériale .
Trois ans plus tard , [MASK] [MASK] signe une paix avec [MASK] en 1547 .
Cette confrontation de 1546-1547 entrera dans l' histoire sous le nom de [MASK] [MASK] [MASK] .
Cette sortie de guerre vraiment favorable pour les états impériaux protestants est due au fait que [MASK] [MASK] poursuit des projets constitutionnels parallèlement à ses buts politico-religieux .
Ces buts supplémentaires entraînent la résistance des états impériaux catholiques , si bien que [MASK] [MASK] ne trouve aucune solution satisfaisante concernant la question religieuse .
Toutefois , il ne parvient ni à rendre la charge d' empereur héréditaire ni à échanger la couronne impériale entre les lignes autrichienne et espagnole des [MASK] .
Le soulèvement des princes contre [MASK] [MASK] sous la conduite du prince-électeur [MASK] [MASK] [MASK] et la [MASK] [MASK] [MASK] signée en 1552 entre les princes et le futur [MASK] [MASK] [MASK] qui en résulte sont les premiers pas vers une paix religieuse durable étant donné que le traité garantit une liberté de culte aux protestants .
Il en résultera la [MASK] [MASK] [MASK] en 1555 .
La [MASK] [MASK] [MASK] n' est pas seulement importante en tant que paix de religion , elle a également un grand rôle politico-constitutionnel en posant de nombreux jalons en matière de politique constitutionnelle .
Le 9 juillet 1553 a lieu la bataille la plus sanglante de [MASK] [MASK] , la [MASK] [MASK] [MASK] , au cours de laquelle [MASK] [MASK] [MASK] est tué .
Les cercles et les états impériaux locaux obtiennent également parallèlement à leurs devoirs habituels la compétence pour appliquer les jugements du [MASK] ainsi que la nomination des assesseurs qui y siègent .
La politique intérieure et étrangère de [MASK] [MASK] avait définitivement échoué .
En réaction , les princes catholiques fondent la [MASK] [MASK] le 10 juillet 1609 autour de [MASK] [MASK] [MASK] .
La promulgation de l' [MASK] [MASK] [MASK] le 6 mars 1629 est le dernier acte de loi impérial important .
Tout comme la mise au ban de [MASK] [MASK] , il trouve sa source dans la revendication du pouvoir de l' empereur .
Cet édit réclame l' adaptation de la [MASK] [MASK] [MASK] d' un point de vue catholique .
En conséquence , tous les évêchés , les évêchés et archevêchés-princiers qui avaient été sécularisés par les seigneurs protestants depuis la [MASK] [MASK] [MASK] doivent être restitués aux catholiques .
Lors de leur réunion de 1630 , les princes-électeurs , sous la conduite de [MASK] [MASK] [MASK] [MASK] [MASK] , obligent l' empereur à renvoyer le généralissime [MASK] et à accorder une révision de l' édit .
Après plusieurs années où les troupes suédoises se montrent supérieures à celles de l' empereur , ce dernier parvient à les battre et à retrouver l' avantage à la [MASK] [MASK] [MASK] en 1634 .
La [MASK] [MASK] [MASK] signée entre l' empereur et l' [MASK] [MASK] [MASK] en 1635 autorise [MASK] à suspendre l' [MASK] [MASK] [MASK] pour quarante ans .
C' est le [MASK] [MASK] [MASK] qui ouvre la marche en mai 1641 .
La [MASK] y montre d' ailleurs toute sa bienveillance puisqu' elle veut absolument réduire le pouvoir des [MASK] en appuyant fortement la demande de participation des états impériaux aux négociations .
[MASK] l' empereur finit par consentir à la participation des états impériaux aux négociations , il le fait pour ne pas se couper définitivement d' eux .
Les deux villes où ont lieu les négociations ainsi que les chemins qui les relient sont déclarés démilitarisés ( ce qui n' a été complètement appliqué que pour [MASK] ) .
On négocie également une paix entre l' [MASK] et les [MASK] le 30 janvier 1648 .
Dans la période qui suit directement les [MASK] [MASK] [MASK] , la paix est toutefois vue d' une toute autre manière .
En outre , elle condamne ceux de la [MASK] [MASK] [MASK] .
Même si l' on accorde aux états impériaux les droits complets de souveraineté et que l' on réinstaure le droit d' alliance annulé par la [MASK] [MASK] [MASK] , ce n' est pas la souveraineté totale des territoires qui est envisagée puisqu' ils restent soumis à l' empereur .
Selon les spécialistes du droit de l' époque , les [MASK] [MASK] [MASK] sont une sorte de coutume traditionnelle des états impériaux qu' ils ne font que fixer par écrit .
La [MASK] [MASK] [MASK] est confirmée dans son ensemble et déclarée comme intouchable mais les questions litigieuses sont de nouveau réglées .
Depuis 1658 , c' est l' empereur [MASK] [MASK] [MASK] qui est au pouvoir .
Cette situation montre que la politique impériale n' est pas devenue une partie de la politique de grande puissance des [MASK] comme elle va l' être sous le règne de ses successeurs au XVIII e siècle .
D' après les historiens , ceci est une conséquence à long terme de la [MASK] [MASK] [MASK] [MASK] pendant laquelle les titres et les positions juridiques n' ont presque plus joué de rôle , en particulier pour les états impériaux les plus petits .
À [MASK] , on ne se préoccupe pour ainsi dire pas des institutions impériales .
[MASK] [MASK] abandonne .
Cependant , on peut souligner le fait que [MASK] [MASK] agit de manière malheureuse et brusque .
On suggère alors à [MASK] [MASK] qui avait accepté l' héritage contre son gré l' idée d' un futur échange avec les [MASK] [MASK] .
Les troupes prussiennes et saxonnes marchent sur la [MASK] .
Face aux troupes révolutionnaires française , les deux grandes puissances allemandes s' allient dans la [MASK] [MASK] .
La [MASK] veut aussi se dédommager de ses coûts de guerre en annexant des territoires ecclésiastiques .
En 1796 , le [MASK] et le [MASK] font de même .
Les accords ainsi signés stipulent que les possessions situées sur la rive gauche du [MASK] doivent être cédées à [MASK] [MASK] .
Elle cède ainsi différentes possessions comme les [MASK] [MASK] et le [MASK] [MASK] [MASK] .
Mais la [MASK] [MASK] met un terme au marchandage lié aux différents territoires .
Le [MASK] [MASK] [MASK] signé en 1801 met fin à la guerre .
Même l' [MASK] [MASK] [MASK] perd son siège pour être nommé à [MASK] .
Le [MASK] [MASK] [MASK] crée un nombre clair de moyennes puissances à partir d' une multitude de territoires .
Le margrave de [MASK] reçoit par exemple neuf fois plus de sujets par rapport à ceux perdus lors de la cession des territoires sur la rive gauche du [MASK] et sept fois plus de territoires .
Les positions anticléricales de [MASK] [MASK] ont fait le reste , d' autant plus que l' empereur perd ainsi l' un de ses pouvoirs les plus importants .
Ce couronnement qui renforce son pouvoir montre également sa volonté de devenir l' héritier de [MASK] et d' ainsi légitimer son action en l' inscrivant dans la tradition médiévale .
C' est pourquoi ce dernier visite la cathédrale d' [MASK] en septembre 1804 ainsi que la tombe de [MASK] .
[MASK] [MASK] [MASK] écrit d' ailleurs à son ami le [MASK] [MASK] : " [MASK] la couronne impériale allemande reste dans la [MASK] [MASK] [MASK] -- et on trouve déjà une telle masse de non-politique aujourd'hui où il n' y a encore aucun danger imminent clairement visible que l' on craint le contraire ! -- toute dignité impériale est vaine " .
Toutefois [MASK] perd définitivement patience .
Lors de la [MASK] [MASK] , il fait marcher son armée sur [MASK] .
Les troupes de l' armée bavaroise et de l' armée du [MASK] lui viennent en renfort .
C' est ainsi que [MASK] [MASK] se décide à protester le 18 juin , en vain .
Le 12 juillet 1806 , l' [MASK] [MASK] [MASK] , la [MASK] , le [MASK] , le [MASK] , le [MASK] [MASK] [MASK] devenu [MASK] [MASK] [MASK] , le [MASK] [MASK] [MASK] , le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , et d' autres princes fondent la [MASK] [MASK] [MASK] à [MASK] .
À la place , il introduit la constitution suédoise en [MASK] [MASK] .
L' abdication de la couronne impériale est anticipée par un ultimatum présenté le 22 juillet à [MASK] à l' envoyé autrichien .
La conservation de la dignité impériale va inéluctablement conduire à un conflit avec la [MASK] , le renoncement à la couronne est donc inévitable .
Le 1 er août , l' envoyé français [MASK] [MASK] entre dans la chancellerie autrichienne .
C' est ainsi que [MASK] [MASK] et son chancelier [MASK] considèrent la charge impériale comme un fardeau tout en ne voulant pas que le titre d' empereur revienne à la [MASK] ou à tout autre prince puissant .
La [MASK] [MASK] est fondée le 8 juin 1815 et l' [MASK] la dirige jusqu' en 1866 .
La première convention que l' on peut considérer comme étant de droit constitutionnel est celle du [MASK] [MASK] [MASK] de 1122 qui met fin à la [MASK] [MASK] [MASK] .
[MASK] [MASK] reconnaît également aux princes le droit de légiférer .
Les concordats de 1447 entre le pape [MASK] [MASK] et l' empereur [MASK] [MASK] sont également considérés comme étant une loi fondamentale .
Après la [MASK] [MASK] [MASK] [MASK] , les [MASK] [MASK] [MASK] sont déclarés loi fondamentale perpétuelle en 1654 .
[MASK] le droit non écrit peut avoir valeur de loi , le fait de ne pas appliquer une règle peut suffire à la faire abolir .
Ce titre auquel tous renoncent après leur couronnement par le pape excepté [MASK] [MASK] montre que l' empire ne naît pas du couronnement par le pape .
Il préside le collège électoral , c' est-à-dire qu' il convoque les six autres grands électeurs pour le choix du nouveau roi à [MASK] .
On peut citer parmi les états impériaux le [MASK] [MASK] [MASK] , le [MASK] [MASK] [MASK] [MASK] , le [MASK] [MASK] [MASK] ou la [MASK] [MASK] [MASK] .
En 1632 , l' empereur [MASK] [MASK] octroie la charge électorale palatine au [MASK] [MASK] [MASK] .
Ce sont les [MASK] [MASK] [MASK] qui réinvestissent le [MASK] comme huitième électorat ( le [MASK] et la [MASK] sont à nouveau réunis comme électorat unique en 1777 ) .
En 1692 , c' est au duché de [MASK] que revient la neuvième charge électorale qui n' est cependant confirmée par la diète qu' en 1708 .
Le [MASK] [MASK] [MASK] joue alors un rôle particulier puisque depuis les [MASK] [MASK] [MASK] [MASK] , il ne participe plus qu' à l' élection royale sans prendre part aux autres activités du collège des électeurs , situation qui n' est changée qu' en 1708 .
[MASK] un prince temporel ou spirituel règne sur plusieurs territoires , il dispose d' un nombre de voix correspondant .
Après le [MASK] [MASK] [MASK] de 1803 , on n' en compte plus que six : [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
Les [MASK] [MASK] [MASK] stipulent en effet que les questions religieuses doivent être réglées non plus selon le principe de majorité mais selon celui de consensus .
Les cercles impériaux disposent d' une diète où sont discutées les différentes affaires économiques , politiques ou militaires , faisant d' eux des acteurs politiques importants , notamment en ce qui concerne la [MASK] [MASK] de justice .
Après avoir également siégé à [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] , elle siège à [MASK] à partir de 1527 .
En créant la [MASK] [MASK] de justice , l' empereur perd son rôle de juge absolu laissant le champ libre à l' influence des états impériaux , ces derniers sont d' ailleurs chargés de faire appliquer les décisions de justice .
Selon des estimations grossières , on compte environ dix habitants par kilomètre carré sous [MASK] .
La partie occidentale qui avait appartenu à l' [MASK] [MASK] est plus peuplée que la partie orientale .
Cela est d' autant plus correct que la [MASK] est un fief impérial et le roi de [MASK] -- dignité créée seulement sous les [MASK] -- est un prince-électeur .
Même à l' époque moderne , il s' en faut que les droits impériaux en [MASK] soient devenus insignifiants .
La langue allemande est quant à elle introduite à la chancellerie impériale à partir du règne de [MASK] [MASK] .
Après la [MASK] [MASK] [MASK] [MASK] , c' est une politique de migration en partie ciblée qui est mise en place , par exemple en [MASK] , qui a conduit à une migration considérable dans les territoires concernés .
On la retrouve en effet sous [MASK] [MASK] [MASK] ou [MASK] [MASK] .
Ce n' est qu' après cette date que l' aigle devient bicéphale sous le règne de [MASK] [MASK] .
On la retrouve déjà en 1312 sur la bannière impériale et c' est sous [MASK] [MASK] qu' elle s' impose sur la bannière .
C' est sous [MASK] [MASK] [MASK] que l' aigle bicéphale devient le symbole de l' empereur sur les sceaux , les monnaies , le drapeau impérial , etc. , tandis que l' aigle simple devient le symbole du roi .
En 1938 , elles sont transportées sur l' ordre d' [MASK] à [MASK] .