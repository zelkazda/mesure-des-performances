Il est traversé , du sud vers le nord , par la rivière qui lui a donné son nom , l' [MASK] , affluent de la [MASK] , qui la rejoint à [MASK] ( [MASK] ) .
L' [MASK] et [MASK] [MASK] lui attribuent le code 89 .
Cette attractivité lui permet de rester le département le plus dynamique de la [MASK] avec une croissance démographique de 0,41 % par an .
C' est le 3ème département de la [MASK] [MASK] .
Elle est limitrophe des départements de l' [MASK] , de la [MASK] , de la [MASK] , du [MASK] et de [MASK] .
Le [MASK] [MASK] [MASK] [MASK] couvre notamment les pays de [MASK] ( sous-sol argileux , bocage ) et de [MASK] ( sous-sol calcaire , champs ouverts ) .
[MASK] est la première ville du département ( 40 000 habitants ) , puis suivent [MASK] ( 30 000 habitants ) et [MASK] ( 10 000 habitants ) .
De tendance continentale , le [MASK] [MASK] [MASK] [MASK] présente une forte amplitude thermique .
Ainsi , [MASK] a été la ville de métropole avec la température la plus élevée pendant la canicule de 2003
L' essentiel du trafic aérien s' effectue à partir de l' [MASK] [MASK] [MASK] [MASK] [MASK] même si l' activité de celui-ci est fortement limitée par la démographie d' une part , mais aussi par la proximité des aéroports de [MASK] .
Le trafic ferroviaire s' effectue principalement depuis la [MASK] [MASK] [MASK] et depuis la [MASK] [MASK] [MASK] [MASK] [MASK] .
Des trains quotidiens et réguliers relient [MASK] à [MASK] ( [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] ) .
Il y a du champenois , et des parlers d' origine du [MASK] .