La [MASK] participe à toutes les élections depuis 1997 [ réf. nécessaire ] et s' investit dans le mouvement social ; ses membres militent activement dans les organisations syndicales ( en particulier les syndicats [MASK] , [MASK] et [MASK] ) , dans diverses associations du mouvement ouvrier , dans le mouvement altermondialiste et sont actifs dans maintes autres activités considérées comme liées à la lutte des classes .
La [MASK] est issue du mouvement communiste trotskiste , anti- stalinien .
[MASK] est élu .
[MASK] [MASK] soutient alors la " violence révolutionnaire " et critique l ' " électoralisme stalinien " , tandis que [MASK] [MASK] préconise le " travail syndical " et l' entrisme dans les partis ouvriers .
Puis elle se reformera officiellement sous le nom de " [MASK] [MASK] [MASK] " .
Même [MASK] [MASK] , secrétaire général du [MASK] , accepte de se rendre à un meeting de soutien au [MASK] [MASK] [MASK] .
De nombreux débats parcourent la [MASK] après cet évènement , notamment sur la stratégie révolutionnaire , le rôle de la violence ou la notion d ' " avant-garde ouvrière large " , sévèrement critiquée par [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] , qui prône un retour à l' usine .
Après le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] contre [MASK] [MASK] au [MASK] , [MASK] signe un article critiquant le réformisme et prônant l' armement du prolétariat
Mais la même année , sous l' influence du [MASK] [MASK] [MASK] américain , la [MASK] [MASK] abandonne la ligne de soutien aux guérillas , constatant leur échec .
Un groupe du [MASK] , mené par [MASK] [MASK] et [MASK] [MASK] , rejoint la [MASK] en 1973 .
Mais le 7 avril 1974 , un mois avant le premier tour , [MASK] [MASK] convainc le [MASK] de soutenir [MASK] .
[MASK] se présente donc pour le " [MASK] [MASK] [MASK] " , obtenant 0,36 % des suffrages , deux fois moins qu' en 1969 et six fois moins qu' [MASK] [MASK] ( [MASK] ) .
Les années [MASK] commencent .
De 1977 à 1981 , la [MASK] dénonce la " politique de division " du [MASK] qui voit l' union se faire à son détriment .
La [MASK] défend le maintien de " l' unité des organisations ouvrières " .
En novembre 1979 , la direction de la [MASK] est mise en minorité concernant sa volonté d' appuyer l' [MASK] [MASK] [MASK] [MASK] .
Le départ de [MASK] permet à [MASK] et [MASK] de reprendre la majorité , n' ayant plus besoin d' une alliance avec [MASK] .
reviennent à la [MASK] .
Elle appelle à voter pour [MASK] [MASK] ( [MASK] ) ou [MASK] [MASK] ( [MASK] ) au premier tour et pour [MASK] [MASK] au second , tandis que l' [MASK] appelle à voter [MASK] dès le premier tour .
Elle compare cette élection à celle du [MASK] [MASK] de 1936 et considère que la dynamique de l' élection doit déboucher sur un nouveau " juin 1936 " ( grève générale ) .
La [MASK] veut " changer sa composition sociale par effet mécanique d' implantation " .
Mais , selon le bilan fait plus tard par la [MASK] , " le tournant ouvrier fut une mauvaise réponse à un vrai problème : sa faiblesse d' implantation dans les concentrations ouvrières , mais une mauvaise réponse car on ne peut se fabriquer une implantation ouvrière en transformant intellectuels , étudiants et employés en ouvriers " [ réf. souhaitée ] .
décide à nouveau d' abandonner ses études pour se consacrer au militantisme ouvrier , dont [MASK] [MASK] .
La [MASK] juge que le gouvernement adhère aux thèses capitalistes et considère que cela accélère le " déclin du [MASK] " et la transformation " social-libérale " du [MASK] [MASK] .
La [MASK] amorce alors un déclin , ayant surestimé les possibilités révolutionnaires de 1981 .
De plus , le virage brusque de la politique de [MASK] [MASK] ne leur profite pas .
Au contraire , tous les mouvements d' extrême gauche décroissent durant cette période ( beaucoup d' électeurs se tournant vers le [MASK] [MASK] ) .
La [MASK] perd beaucoup de militants et donc de visibilité .
Elle s' est cependant investie dans des conflits sociaux de l' époque , notamment dans les mouvements des ouvriers de l' automobile ( [MASK] , [MASK] ) ou de la sidérurgie .
Selon [MASK] [MASK] , elle n' a au contraire aucune " prise ni le plus petit début d' influence " sur [MASK] [MASK] et les grèves étudiantes de 1986 , qui ont comme porte-parole [MASK] [MASK] , suggéré par [MASK] , passé au [MASK] .
La [MASK] commence à se mettre en cause à partir de 1984 , élaborant un aggiornamento à l' initiative de [MASK] [MASK] .
Sa candidature suscite le soutien du [MASK] [MASK] [MASK] , de la [MASK] et de groupes pablistes .
Des comités de soutien se mettent en place dans toute [MASK] [MASK] , mélangeant des militants communistes , des militants du [MASK] , de la [MASK] .
Le résultat est décevant : [MASK] [MASK] n' obtient que 2,08 % des voix , tandis que [MASK] [MASK] recueillent 3,8 % , [MASK] [MASK] 1,9 % et le [MASK] [MASK] [MASK] 0,4 % .
En 1989 , le [MASK] [MASK] [MASK] s' effondre , puis en 1991 l' [MASK] .
L' hypothèse de [MASK] n' est pas validée , à savoir qu' à l' effondrement du régime de l' [MASK] [MASK] succède une " révolution anti-bureaucratique " vers le socialisme .
La [MASK] s' oriente alors vers la création d' un nouveau parti .
En 1995 , la [MASK] ne présente pas de candidat à l' élection présidentielle , et appelle à voter pour [MASK] [MASK] , [MASK] [MASK] ou [MASK] [MASK] .
La [MASK] se revigore à partir du mouvement de novembre-décembre 1995 contre le plan [MASK] .
La [MASK] considère que ce fut la " première révolte anti-libérale " .
Par la suite , la [MASK] s' immerge dans le mouvement altermondialiste , né suite aux manifestations de [MASK] en 1999 ou [MASK] en 2001 .
En 1999 , lors des élections européennes , la [MASK] se présente derrière une candidature commune avec [MASK] [MASK] .
Pour la première fois lors de cette élection , l' extrême-gauche obtient cinq députés dont deux sont de la [MASK] ( [MASK] [MASK] et [MASK] [MASK] ) .
La [MASK] confirme ce refus car il s' agissait d' établir une commission sur la question dirigée par [MASK] [MASK] .
Lors de l' élection présidentielle de 2002 , après le refus de [MASK] de continuer l' aventure des listes communes de 1999 , la [MASK] décide de présenter un candidat [MASK] pour la première fois depuis 1974 et la candidature d' [MASK] [MASK] .
Suite à cette élection , la [MASK] connaît une inflation du nombre de ses militants ( environ 3000 membres ) et de sa popularité .
Pour les élections régionales et européennes de 2003 et 2004 , [MASK] [MASK] ayant proposé de faire liste commune , la [MASK] accepte pour créer un pôle " à gauche de la gauche plurielle " mais malgré un résultat supérieur à celui des élections régionales de 1998 , elle n' arrive pas à profiter de sa popularité avec l' effet " vote utile " en faveur du [MASK] .
Grâce à cette campagne , la [MASK] et son porte parole , [MASK] [MASK] connaissent un regain de popularité .
[MASK] [MASK] n' a pas souhaité se représenter pour être élu au bureau politique en janvier 2006 .
Lors d' une conférence nationale en juillet 2006 la [MASK] a déclaré la candidature d' [MASK] [MASK] .
La conférence nationale ayant déclaré la possibilité d' un retrait de la candidature d' [MASK] [MASK] en cas d' accord avec les autres composantes et les collectifs .
Le [MASK] [MASK] [MASK] ne semblait pas répondre aux critères d' indépendance vis-à-vis du [MASK] [MASK] .
Toutes les tendances de la [MASK] estimaient indispensable l' affirmation de l' indépendance vis-à-vis du [MASK] .
Le désaccord à l' origine de cette forte polémique dans la [MASK] est de nature stratégique : la minorité pensant qu' il était possible de mener une bataille politique dans les collectifs pour obtenir une amélioration du programme et affirmer l' indépendance face au [MASK] et la majorité pensant qu' il n' était plus possible de gagner des avancées positives dans les collectifs antilibéraux .
La [MASK] se hisse alors à la 5 e position et devient la deuxième force de gauche après le [MASK] .
Au premier tour des élections législatives , la [MASK] décide de présenter pour la première fois 492 candidats sur tout le territoire français .
Avec un total d' un peu moins de 529000 voix , la [MASK] progresse de 209000 suffrages par rapport à 2002 ( elle avait alors totalisé 320000 voix dans 441 circonscriptions ) .
La [MASK] échappe donc au recul qui touche l' ensemble des autres composantes de la gauche en dehors du [MASK] .
La majorité des militants estiment qu' il s' agit d' une " victoire " qui cautionne la ligne politique de critique ferme de la social-démocratie et du social-libéralisme ( [MASK] , [MASK] , [MASK] , etc . )
; d' autres , très minoritaires , pensent que la présence de la [MASK] lors de ce cycle électoral a pu participer à l' échec des autres partis de la gauche du " non " .
Cette voie la conduit à s' auto-dissoudre dans le [MASK] [MASK] [MASK] ( [MASK] ) lors de son congrès de dissolution le 5 février 2009 .
Elle n' enregistre cependant qu' un léger recul par rapport aux élections européennes , où la candidate de l' alliance , [MASK] [MASK] avait totalisé 5,18 % des voix , et améliore même les scores globaux de l' extrême gauche aux élections régionales de 1998 .
Cette démarche s' inscrit dans l' optique d' un " programme de transition " défendu par [MASK] dans son ouvrage du même nom .
La [MASK] est caractérisée par le droit de tendance offrant aux minorités en son sein , en désaccord ou non avec les stratégies de la majorité de l' organisation , d' exprimer leurs idées .
La [MASK] est dotée d' un mouvement de jeunes : les [MASK] [MASK] [MASK] .
Le débat continue au sein de la [MASK] sur la meilleure façon de coordonner les deux organisations .
Il existe aussi des sections jeunes de la [MASK] .
Les deux journaux laissant la place au système de presse du [MASK] [MASK] [MASK]