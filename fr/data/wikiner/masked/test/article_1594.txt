Le [MASK] , officiellement la [MASK] [MASK] [MASK] , est un pays d' [MASK] [MASK] sans accès à la mer , situé au sud de la [MASK] , à l' est du [MASK] et du [MASK] , au nord du [MASK] et de la [MASK] [MASK] et à l' ouest du [MASK] .
Sa capitale est [MASK] [MASK] .
Géographiquement et culturellement , le [MASK] constitue un point de passage entre l' [MASK] [MASK] [MASK] et l' [MASK] [MASK] .
Le [MASK] se divise en trois grands ensembles géographiques : du nord au sud , on trouve successivement une région désertique , un espace semi-aride , puis la savane soudanaise .
Le [MASK] [MASK] , qui donne son nom au pays , est son principal plan d' eau ; le point culminant du pays est l' [MASK] [MASK] , dans le [MASK] [MASK] [MASK] .
Le pays est le théâtre de troubles quasi-permanents , liés à des dissensions internes , et plus récemment à l' extension du [MASK] [MASK] [MASK] .
Considéré comme protectorat français à partir de 1900 , le [MASK] fut érigé en colonie en 1920 dans le cadre de l' [MASK] ( [MASK] [MASK] [MASK] ) .
Sous l' impulsion du gouverneur [MASK] [MASK] , il fut la première colonie française à se rallier à la [MASK] [MASK] en 1940 .
Devenu république autonome en 1958 , le [MASK] accéda à l' indépendance le 11 août 1960 sous la présidence de [MASK] [MASK] .
Celui-ci dut bientôt faire face à la révolte des populations du [MASK] , en majorité musulmanes , ce qui l' amena à solliciter l' aide des troupes françaises en 1968 .
Après l' assassinat de [MASK] en 1975 , le pouvoir échut au général [MASK] [MASK] , qui dut céder la place au nordiste [MASK] [MASK] à la suite de la première bataille de [MASK] en 1979 .
En 1980 , la seconde bataille de [MASK] permit à [MASK] [MASK] d' évincer son rival , [MASK] [MASK] , avec l' aide décisive des troupes libyennes .
Après l' échec d' un projet de fusion entre le [MASK] et la [MASK] en 1981 , les troupes libyennes se retirèrent dans le cadre d' un accord conclu avec le gouvernement français .
En 1982 , [MASK] [MASK] fut renversé à son tour par [MASK] [MASK] , qui dut faire appel l' année suivante aux troupes françaises pour contenir une nouvelle invasion libyenne .
En 1990 , [MASK] [MASK] fut renversé du pouvoir par [MASK] [MASK] [MASK] , qui est en place depuis lors .
Paradoxalement , ce dernier semble bénéficier aujourd'hui du soutien de [MASK] [MASK] et de la [MASK] , face aux divers mouvements de rébellion qui seraient plus ou moins encouragés par le [MASK] voisin , en liaison avec le conflit du [MASK] .
En 1999 , le [MASK] s' engage dans la [MASK] [MASK] [MASK] [MASK] , en soutenant le gouvernement de [MASK] .
En février 2008 , la rébellion tente de renverser le gouvernement d' [MASK] [MASK] en pénétrant dans [MASK] après une traversée du pays depuis le [MASK] voisin .
En mai 2009 , une autre offensive de la rébellion partant du [MASK] a lieu .
Le [MASK] est une république .
Le point le plus important est la possibilité pour le président [MASK] [MASK] de se présenter indéfiniment aux élections présidentielles ( modification de l' article 61 de la constitution du 31 mars 1996 ) .
Peu avant cette date , le 13 avril 2006 , de brefs combats ont lieu dans la périphérie de [MASK] [MASK] , entre une faction de la rébellion , le [MASK] et les troupes gouvernementales .
[MASK] [MASK] accuse le [MASK] de soutenir ses adversaires .
Sans grande surprise en l' absence de concurrent sérieux et avec des élections truquées , [MASK] [MASK] est réélu avec plus de 77 % des voix .
Le 2 février 2008 , les rebelles , apparemment soutenus par le [MASK] [ réf. nécessaire ] ont pris la capitale du pays [MASK] [MASK] , à l' exception du palais présidentiel où le président [MASK] [MASK] semble s' être réfugié .
[MASK] [MASK] évacue une partie de ses ressortissants .
L' armée tchadienne repousse les rebelles avec l' aide logistique de [MASK] [MASK] .
La stabilité régionale au [MASK] est assurée conjointement par la force de l' [MASK] [MASK] [MASK] ( déployée de mars 2008 à mars 2009 , environ 3000 soldats ) et par les forces françaises de l' [MASK] [MASK] .
Le [MASK] est un pays vaste et de faible densité humaine .
C' est dans le [MASK] de cette zone que se trouve la capitale [MASK] ainsi que le [MASK] [MASK] .
Le [MASK] et le [MASK] représentent les deux-tiers de la superficie du pays et comptent environ 30 % de la population totale .
En 2002 , le [MASK] a été divisé en 18 régions administratives .
Le [MASK] est aux trois-quarts rural .
La mise en exploitation des gisements pétroliers depuis 2003 a été très encadrée par la [MASK] [MASK] .
Le [MASK] s' est engagé auprès de la [MASK] [MASK] à dépenser 80 % des redevances et 85 % des dividendes à la lutte contre la pauvreté .
En 2009 , la population tchadienne est estimée par le [MASK] [MASK] [MASK] à environ 10,33 millions d' habitants ; 46,7 % a moins de quinze ans , 50,4 % entre quinze et soixante-quatre ans , et 2,9 % soixante-cinq ans et plus .
Les densités varient considérablement du nord au sud du pays , avec 0,1 habitant au km² dans les régions du [MASK] , de l' [MASK] et du [MASK] , et 52,4 habitants au km² dans le [MASK] [MASK] .
242600 d' entre eux provenaient du [MASK] et le reste de la [MASK] [MASK]
Le [MASK] a quatre codes :