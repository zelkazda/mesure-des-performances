Elle doit d' ailleurs repousser un assaut de la flotte de [MASK] lors d' une bataille fort indécise où plusieurs dizaines de navires sont perdus ( Voir [MASK] [MASK] [MASK] [MASK] ) .
Dans la nuit , subrepticement , la flotte dirigée par [MASK] emprunte le canal de l' [MASK] et navigue vers le sud .
La situation pour les [MASK] n' est pas encourageante , comme le rapporte [MASK] [MASK] [MASK] .
[MASK] [MASK] , le frère de [MASK] et roi des [MASK] , ne songe qu' à protéger le [MASK] par la construction d' un mur vers l' [MASK] [MASK] [MASK] .
Quant à la flotte , elle s' installe à [MASK] à la demande de [MASK] .
Ce plan , tenir l' [MASK] [MASK] [MASK] et le golfe de [MASK] , implique l' abandon total de l' [MASK] , ce qui explique aussi la prise d' [MASK] , abandonnée par ses habitants sur les conseils de [MASK] .
[MASK] a un plan précis qu' il impose contre l' avis d' [MASK] .
Il tient les propos suivants , rapportés par [MASK] :
[MASK] préfère défendre un autre point de vue , plus circonspect .
Maintenant que la flotte grecque a assuré l' évacuation de l' [MASK] , il faut retourner à proximité des forces terrestres afin d' entreprendre des actions combinées .
Ce point de vue est partagé par les [MASK] , deuxième flotte de la coalition .
[MASK] reçoit cependant le soutien d' [MASK] et de [MASK] , il est vrai directement menacées en cas de repli sur l' [MASK] [MASK] [MASK] de la flotte grecque .
Pendant que la flotte perse termine dans la nuit l' encerclement de l' île de [MASK] , les généraux grecs tergiversent toujours .
La ruse de [MASK] vient de réussir .
Les combats terrestres précédents ont montré que la valeur au combat des [MASK] ainsi que l' armement sont supérieurs , ce qui dans le cas d' abordage des navires ennemis est un avantage .
Deux impératifs s' imposent aux [MASK] .
Dès le départ [MASK] [MASK] font une fausse manœuvre décrite ainsi par [MASK] [MASK] [MASK] :
[MASK] [MASK] font une erreur par excès de confiance et sont désorganisés dès le début de la bataille .
Pour certains il est acquis que la flotte grecque est adossée à l' île de [MASK] et que la flotte perse est alignée plus ou moins parallèlement au rivage de l' [MASK] .
L' aile droite grecque , dirigée par [MASK] , et constituée des navires lacédémoniens , corinthiens et éginètes , flanche au départ et recule provisoirement , sous les probables huées des civils massés sur les rivages de l' île de [MASK] .
Face à eux se tiennent leurs vieux adversaires , les [MASK] .
[MASK] raconte ainsi le déclenchement de cette bataille :
Cependant la combativité des [MASK] d' [MASK] , ou des [MASK] face à [MASK] sur l' aile gauche , ne suffit pas à contrebalancer l' erreur initiale qu' avait été le désordre introduit dans leurs lignes dès avant l' attaque .
La bousculade , la panique conduisent bien des navires perses à présenter le flanc au lieu de la proue ce qui dans un combat à l' éperon est rédhibitoire surtout face à des [MASK] qui réussissent à tenir leur alignement .
La bataille est déjà engagée quand une brise marine se lève -- selon [MASK] -- qui ne gène pas les navires grecs dont les superstructures sont peu élevées mais désavantage nettement les bateaux en particulier phéniciens dont la poupe est haute et le tillac surélevé .
Le plus surprenant est qu' elle reçut des éloges de [MASK] pour ce fait d' armes car dans la confusion il apparut qu' elle venait de couler un navire ennemi .
C' est à propos de cet épisode que l' on prête à [MASK] la fameuse phrase :
Cependant [MASK] ne souhaite pas poursuivre la flotte perse en haute mer car malgré le désastre elle conserve probablement sa supériorité numérique .
Il semble que les [MASK] ne comprennent pas tout de suite la portée de leur victoire et qu' ils s' attendent à un nouvel assaut le lendemain .
Le soir venu le silence revient sur le lieu de cette bataille comme l' écrit [MASK] dans [MASK] [MASK] :
Lors de cette bataille , [MASK] [MASK] ont perdu au moins 200 trières , sans compter celles tombées aux mains des vainqueurs , et les [MASK] une quarantaine .
La flotte perse reste , en dépit de ses pertes , supérieure en tonnage et les immenses ressources de l' empire peuvent permettre la construction de nombreux navires alors que pour les [MASK] , la destruction des chantiers de l' [MASK] est une perte irremplaçable .
En effet , laissant le commandement de son armée à [MASK] , son beau-frère , celui qui dirigeait déjà l' expédition de 492 , [MASK] abandonne ses troupes pour retourner vers ses capitales [MASK] et [MASK] .
Il suit en cela le conseil de [MASK] et de la reine [MASK] [MASK] [MASK] d' [MASK] , à savoir laisser en [MASK] une armée importante , [MASK] parle de 300 000 hommes ce qui est sans doute excessif , qui hivernera en [MASK] continentale , puis attaquer le [MASK] au printemps .
Quant à [MASK] sa présence n' est plus utile , puisque son principal objectif est atteint , à savoir la destruction d' [MASK] .
Quand il apparaît que [MASK] [MASK] font retraite , [MASK] dans l' euphorie de la victoire propose de couper la route de l' [MASK] à [MASK] en traversant l' [MASK] .
De plus les [MASK] ont perdu à [MASK] plus de 40 navires et ne peuvent les remplacer aussi rapidement que leurs adversaires .
Enfin , envoyer toute la flotte aussi loin de la [MASK] alors que les réfugiés d' [MASK] sont encore sur l' île de [MASK] et que les côtes grecques sont non protégées est assez hasardeux .
Pour [MASK] une éventuelle défaite d' [MASK] ferait le jeu de [MASK] , d' autant que [MASK] est en train de finir le mur qui barre l' isthme du [MASK] et donc ne ressent plus la menace perse avec la même acuité .