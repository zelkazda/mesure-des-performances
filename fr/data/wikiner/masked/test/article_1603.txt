Elle est bordée par la [MASK] [MASK] [MASK] et la [MASK] au sud , le [MASK] à l' ouest , et la [MASK] au nord .
La [MASK] [MASK] [MASK] est divisée en cent comtés et sa capitale est [MASK] .
La [MASK] [MASK] [MASK] est l' une des treize colonies qui fondèrent [MASK] [MASK] [MASK] [MASK] .
Elle fut le second territoire américain colonisé par l' [MASK] .
Quelques temps après , [MASK] [MASK] [MASK] fut envoyé avec trois vaisseaux pour venger le massacre des [MASK] , mais il n' essaya pas de relever la colonie .
La plupart d' entre eux importent des esclaves après la création en 1672 de la [MASK] [MASK] [MASK] [MASK] , pour leurs vastes plantations de tabac .
En 1670 , [MASK] [MASK] avait donné une constitution à la [MASK] ; mais cette constitution ne put être appliquée .
La [MASK] connut plus tard , dans les années 1730 , une forte immigration de familles modestes écossaises qui s' installèrent au pied des [MASK] .
Grâce à leur implication , la [MASK] sera la treizième des treize colonies qui se sont révoltées contre la domination britannique lors de la [MASK] [MASK] [MASK] .
Elle avait été nommée en honneur du roi [MASK] [MASK] [MASK] [MASK] [MASK] ( ? ?
La [MASK] [MASK] [MASK] a connu trois constitutions :
La [MASK] [MASK] [MASK] compte trois régions urbaines avec plus de 1 million d' habitants :
La [MASK] [MASK] [MASK] a des frontières avec la [MASK] [MASK] [MASK] au sud , la [MASK] au sud-ouest , le [MASK] à l' ouest , la [MASK] au nord , et l' [MASK] [MASK] à l' est .
À l' ouest , les [MASK] constituent une frontière naturelle avec le [MASK] .
D' autres rivières naissent dans les [MASK] pour se jeter ensuite à l' ouest dans les affluents du [MASK] par exemple , la [MASK] , la [MASK] [MASK] et la [MASK] [MASK] .
À partir de 2009 , le gouverneur sera [MASK] [MASK] .
En 2009 , [MASK] laissera son siège à [MASK] [MASK] ( démocrate ) .
Le [MASK] [MASK] [MASK] est un parc de haute technologie et un lieu d' enseignement situé à [MASK] .
[MASK] [MASK] et [MASK] [MASK] ( dir .