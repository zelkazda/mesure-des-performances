La première expédition européenne à atteindre la région fut l' expédition menée par [MASK] [MASK] , qui a ensuite donné son nom au [MASK] [MASK] .
L' [MASK] [MASK] [MASK] , dont l' extrémité nord se trouve à [MASK] , relie la ville aux cités situées le long de l' [MASK] [MASK] .
L' [MASK] [MASK] , parallèle à l' [MASK] [MASK] [MASK] relie [MASK] à [MASK] et [MASK] au nord , et à [MASK] , [MASK] et [MASK] [MASK] au sud .
Une gare ferroviaire du réseau [MASK] relie quotidiennement [MASK] à [MASK] et [MASK] à [MASK] [MASK] .
[MASK] est un centre régional des arts avec de nombreuses compagnies de théâtre , un festival de film , de nombreux labels de musique implantés dans la ville comme [MASK] [MASK] et [MASK] [MASK] [MASK] et des groupes de musique fondés à [MASK] tels [MASK] [MASK] , précurseur du riot grrrl , [MASK] [MASK] [MASK] , [MASK] [MASK] et [MASK] .
La ville a été jumelée à [MASK] ( [MASK] ) et [MASK] ( [MASK] ) mais ces jumelages ne sont plus effectifs .
Il y a également un groupe de pression visant à faire établir un lien avec la ville de [MASK] ( [MASK] ) , lieu de décès de l' activiste [MASK] [MASK] .