Elle se situe dans le nord-est du pays , à 150 miles ( 240 km ) au sud des [MASK] [MASK] et de [MASK] .
Le site de la nouvelle capitale de l' [MASK] [MASK] [MASK] [MASK] est choisi en 1820 pour remplacer [MASK] à partir de 1825 .
Selon le [MASK] [MASK] [MASK] [MASK] [MASK] , 55 % de l' année , il y a du soleil .
[MASK] [MASK] , qui était l' assistant de [MASK] [MASK] [MASK] [MASK] lors de l' élaboration des plans de la ville de [MASK] [MASK] , s' en inspire pour les plans de la nouvelle capitale .
En juillet 2008 , on dénombrait pas moins de 91 grattes-ciel dans le skyline d' [MASK] .
[MASK] compte six grands parcs , sur les 206 parcs présents dans la ville .
Le [MASK] [MASK] [MASK] [MASK] est le plus grand .
Selon le [MASK] [MASK] [MASK] [MASK] [MASK] , on comptait 320107 foyers à [MASK] pour une densité de population de 837 hab./km² .
L' âge médian est de 33,6 ans ( 35,3 ans pour l' ensemble des [MASK] ) et pour 100 femmes , on comptait 102,1 hommes .
Le revenu médian pour un foyer est de 40051 $ ( contre 42100 $ pour l' ensemble des [MASK] ) .
[MASK] est desservie par de nombreux médias , dans la presse , la radio , la télévision et d' internet .
[MASK] est classé 39 e marché pour la radio aux [MASK] avec près de 1388800 auditeurs .
[MASK] possède de nombreuses équipes sportives dont certaines jouent un rôle de premier plan dans deux sports majeurs .
L' équipe de football américain des [MASK] [MASK] [MASK] , franchise qui a déménagé dans les années 1980 de [MASK] pour s' implater à [MASK] , qui appartient à la ligue nationale [MASK] et dispose de nombreux supporters qui se rendent dans l' important stade [MASK] [MASK] [MASK] construit dans les années 2000 .
Son stade , le [MASK] [MASK] , abrite également l' équipe de basketball [MASK] des [MASK] [MASK] [MASK] [MASK] .
Chaque année ont lieu de nombreuses courses automobiles sur le fameux [MASK] [MASK] [MASK] [MASK] [MASK] .
On peut aussi citer l' [MASK] [MASK] [MASK] [MASK] [MASK] , une course de [MASK] ayant lieu chaque année depuis 1994 .
De 2000 à 2007 le circuit a accueilli le [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] , sur un circuit spécialement aménagé , qui reprend une partie seulement de l' anneau de vitesse .
Depuis 2008 , le [MASK] [MASK] [MASK] [MASK] [MASK] , une manche du championnat du monde de [MASK] s' y déroule .
L' aéroport se situe à moins de 20 minutes du centre ville par l' [MASK] .
L' [MASK] [MASK] [MASK] [MASK] sert d' aéroport de complément pour l' aéroport international .
Au total sur la ville d' [MASK] , neuf infrastuctures aéroporturaires publiques sont présentes .
L' [MASK] [MASK] coupe la ville du nord-ouest en provenance [MASK] éventuellement de [MASK] en direction de [MASK] dans le [MASK] .
L' [MASK] [MASK] part du nord-ouest de [MASK] , vers le sud-est à [MASK] .
La ville d' [MASK] est jumelée avec plusieurs villes étrangères .