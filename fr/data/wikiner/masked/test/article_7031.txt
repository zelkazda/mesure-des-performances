Ancienne capitale des [MASK] [MASK] [MASK] , chef-lieu de la [MASK] [MASK] [MASK] , ville principale de l' [MASK] [MASK] [MASK] , elle est le siège d' une des cinq cours d' appel du pays .
Les communes limitrophes de [MASK] sont [MASK] , [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
Le [MASK] , une ancienne région industrielle ( charbonnages notamment ) , est situé à l' ouest et au sud-ouest de la ville .
Le relief de la commune de [MASK] est influencé par la vallée de la [MASK] , rivière qui s' écoule d' est en ouest au nord de la ville avant de rejoindre l' [MASK] en [MASK] .
La [MASK] , affluent de [MASK] [MASK] se jette dans la [MASK] au niveau de [MASK] .
Le nord et le sud de la vallée de [MASK] [MASK] sont constitués de collines et de plateaux dont l' altitude augmente progressivement pour atteindre des hauteurs variant de 50 à 115 mètres ( point culminant situé au nord-est du village de [MASK] ) .
Au niveau de la vallée , l' altitude descend jusque 20 mètres à proximité de la rivière et du [MASK] [MASK] .
Sur le territoire communal , le sous-sol de la vallée de [MASK] [MASK] est composé d' alluvions .
À d' autres endroits on trouve du silex , comme par exemple à [MASK] où l' affleurement date du [MASK] .
Il existe également à [MASK] des projets d' utilisation de sources de chaleur géothermiques qui sont disponibles à [MASK] , [MASK] et [MASK] pour chauffer certains bâtiments publics et privés .
Grâce aux fonds européens , ces projets se concrétiseront peut-être totalement dans un futur proche et un projet va être incessamment déposé à la [MASK] [MASK] .
Seuls deux puits , à [MASK] et à [MASK] , font l' objet d' une exploitation depuis 1985 .
Le climat de la région de [MASK] est un climat tempéré océanique comme pour l' ensemble de la partie occidentale de la [MASK] , cela grâce à la proximité de l' [MASK] [MASK] qui régule le temps grâce à l' inertie calorifique de ses eaux .
En moyenne ( moyenne faite sur une période couvrant les 100 dernières années ) , on observe 208 jours de pluie par an dans la région de [MASK] .
Le site devient un enjeu militaire suite à l' implantation des [MASK] à [MASK] en 876 .
Cette forteresse est prise et réduite en cendres en 956 , sous [MASK] [MASK] .
Dès 959 , le comté fait partie de la [MASK] .
[MASK] [MASK] [MASK] [MASK] en 1290 construit la deuxième fortification qui , à la différence de la première , défend aussi la ville et non plus seulement le château : cette enceinte urbaine ( frumeteit ou fermetei ( t ) en picard montois ) est percée de six portes .
[MASK] souffre également de plusieurs désastres au cours de cette période .
L' épidémie cesse après la procession , organisée par les autorités , des reliques de [MASK] [MASK] .
Par suite du mariage de [MASK] [MASK] [MASK] avec [MASK] [MASK] [MASK] , la ville passe sous tutelle de la maison des [MASK] en 1477 .
À cette époque , [MASK] compte environ 9500 habitants et elle attire un grand nombre d' artisans ( tanneurs , cordonniers , tisserands ) .
Ainsi , des tanneries s' installent le long de la [MASK] qui à cette époque passe encore dans la ville .
Entre 1580 et 1584 , [MASK] [MASK] installe à [MASK] le siège du gouvernement des [MASK] [MASK] .
Néanmoins , les échevins montois s' adressent au chapitre de [MASK] pour obtenir les reliques de [MASK] [MASK] en espérant ainsi faire disparaître l' épidémie .
En remerciement , la ville fait réaliser par un orfèvre montois une châsse en argent pour y transférer les reliques : elle fait toujours partie des trésors de la [MASK] [MASK] [MASK] [MASK] .
En 1655 , la ville est assiégée par l' armée française : les opérations de siège sont dirigées par le chevalier de [MASK] : elles commencent le 15 août et la ville tombe le 18 .
En 1678 , au cours de la [MASK] [MASK] [MASK] , le [MASK] [MASK] [MASK] assiège [MASK] .
Suite à la [MASK] [MASK] [MASK] , le siège finit par être levé .
La ville tombe et [MASK] [MASK] nomme gouverneur [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] .
[MASK] est chargé d' améliorer le système défensif de la ville .
La place-forte est toutefois contrôlée par des troupes des [MASK] .
En 1718 , le pouvoir , représenté par la cour souveraine du [MASK] , quitte le château qui , par faute d' entretien , se dégrade .
En 1747 , [MASK] [MASK] conquiert la ville et la garde jusqu' en 1748 , où elle est restituée à l' impératrice [MASK] par le [MASK] [MASK] [MASK] ( 1748 ) qui met fin à la [MASK] [MASK] [MASK] [MASK] [MASK] .
Le vainqueur de [MASK] , [MASK] , est sensible au désir d' autonomie des populations , mais son vœu de voir procéder à des élections se heurte aux vieilles structures , sauf à [MASK] , [MASK] et [MASK] et aux dirigeants français avides des richesses belges .
Bien que des pillages et exactions soient signalés dès cette première conquête française , les choses s' aggravent avec la seconde , consécutive à la [MASK] [MASK] [MASK] , le 26 juin 1794 .
Ce régime s' adoucit relativement rapidement en raison des protestations parvenues au [MASK] [MASK] [MASK] [MASK] et parce que la [MASK] était exsangue .
Mais les représentants en mission ont quand même frappé [MASK] .
En 1794 comme en 1792 , les révolutionnaires français peuvent compter sur de nombreux sympathisants à [MASK] comme à [MASK] .
La ville devient la préfecture du [MASK] [MASK] [MASK] ( alors orthographié [MASK] ) en 1794 .
En 1800 débutent les travaux du [MASK] [MASK] : il permet d' acheminer le charbon des mines du [MASK] vers le reste de la [MASK] ( les mines boraines produisent plus de charbon que [MASK] [MASK] entière ) .
Cette période voit la construction du [MASK] [MASK] permettant de rejoindre l' [MASK] sans passer par [MASK] [MASK] .
La [MASK] [MASK] , en préparation à [MASK] dès la fin août 1830 , ne laisse pas la population locale indifférente .
Dès 1841 , une liaison de chemin de fer est réalisée entre [MASK] et [MASK] .
Le démantèlement des fortifications a lieu entre 1861 et 1864 , sous le maïorat de [MASK] [MASK] , tandis que son successeur , [MASK] [MASK] , donne à [MASK] son aspect actuel en faisant construire deux ceintures : le boulevard intérieur sur le site de la fortification dite " urbaine " et le grand boulevard sur les fondations du mur néerlandais .
Même sans ces protections devenues inutiles face aux canons , [MASK] reste une ville de garnison jusqu' en 1914 .
et acclamant le député socialiste [MASK] [MASK] .
Bien que les forces allemandes soient supérieures en nombre , les troupes britanniques retardent pourtant leur percée , permettant ainsi à l' armée belge de se réorganiser au-delà de l' [MASK] et à l' armée française de se ressaisir pour préparer la victoire de la [MASK] .
La [MASK] [MASK] [MASK] donne naissance à la légende des [MASK] [MASK] [MASK] .
En 1967 , le [MASK] , quittant [MASK] ( [MASK] ) , s' installe à [MASK] .
À partir des années 1970 , [MASK] est en mutation .
Il y a tout d' abord l' étape importante des fusions de communes de 1972 ( fusion avec [MASK] , [MASK] , [MASK] , [MASK] , [MASK] avec des parties de [MASK] et de [MASK] ) et de 1977 ( fusion avec [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] ainsi que des parties de [MASK] , de [MASK] et de [MASK] à l' emplacement du [MASK] ) .
La commune de [MASK] a une population totale d' un peu plus de 92.000 habitants .
Ces derniers sont environ 3.200 , mais ne sont évidemment pas domiciliés à [MASK] et ne figurent donc pas dans le registre de la population de la ville .
Cela s' explique par la diminution de la natalité qui ne renouvelle pas la population née durant l' après-guerre ( [MASK] [MASK] [MASK] ) .
La majorité ( 41 % ) de la population étrangère de [MASK] en 2003 était de nationalité italienne .
Cette frange de la population est en grande partie constituée des immigrés italiens et de leurs descendants venus travailler dans les mines du [MASK] .
La population d' origine française arrive en seconde position : la proximité de [MASK] avec la [MASK] ( moins de 10 km ) explique facilement cette présence .
En troisième position arrivent les [MASK] : ils représentent 8 % de la population étrangère .
La présence sur le territoire de la commune du centre de commandement militaire des forces de l' [MASK] , le [MASK] , mais également la proximité de la [MASK] [MASK] [MASK] [MASK] , appartenant à l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , sont à l' origine de cette présence américaine .
On constate dans le tableau ci-dessous que la proportion des revenus faibles est plus importante dans la région de [MASK] que sur toute la [MASK] .
[MASK] est une ville universitaire .
Cette dernière a reçu le statut universitaire en 1965 , tout comme les [MASK] [MASK] [MASK] [MASK] [MASK] .
Le 1 er octobre 2009 , la [MASK] [MASK] [MASK] [MASK] et l' [MASK] [MASK] [MASK] ont fusionné pour donner naissance à une seule université publique , appelée simplement [MASK] [MASK] [MASK] .
La bibliothèque de l' [MASK] [MASK] [MASK] , qui a fêté son bicentenaire en 1997 , possède 715000 ouvrages .
[MASK] compte également plusieurs salles de sport privées , une salle d' escalade .
Par contre l' offre au niveau des piscines demeure très faible à [MASK] .
Puisque la ville ne compte que deux piscines communales , assez vétustes , une à [MASK] et l' autre à [MASK] .
Une nouvelle piscine est actuellement en construction aux abords du [MASK] [MASK] .
En 2006 , 40156 faits criminels ont été relevés sur l' arrondissement judiciaire de [MASK] qui compte une population d' environ 420000 habitants .
Le taux de chômage était de 28 % en 2005 ( 12 % pour la [MASK] ) .
Le taux de chômage à [MASK] était , en août 2008 , de 20.75 % .
Le PIB du [MASK] équivaut seulement à 68 % du PIB moyen européen , ce qui vaut à la province des subsides européens pour atténuer son retard économique .
[MASK] a également décidé d' installer un centre de recherche dans ce parc scientifique .
La ville a également affecté une partie de ces fonds à la rénovation de la [MASK] afin de favoriser son développement touristique .
Celles-ci sont attirées par les moyens de transports locaux ( autoroutes , canaux appartenant à la liaison entre le port maritime de [MASK] en [MASK] et la vallée industrielle de la [MASK] en [MASK] ) et par le caractère bon marché des terrains .
La firme [MASK] a par ailleurs décidé d' installer dans ce zoning un tout nouveau centre de traitement des données .
La part des emplois privés est également assurée par les activités commerciales du centre-ville et du complexe des [MASK] [MASK] .
La présence du [MASK] depuis 1967 stimule également l' économie locale , mais au détriment du prix des loyers , alors que les militaires en poste effectuent de nombreux achats importants hors taxes dans le centre commercial interne au [MASK] .
L' origine des touristes à [MASK] montre qu' il s' agit d' un tourisme de proximité .
En résumé , l' économie de [MASK] est axée sur :
[MASK] bénéficie du réseau autoroutier belge très dense .
Se rendre à [MASK] en voiture est assez simple par autoroute , mais circuler dans la ville l' est beaucoup moins .
La [MASK] [MASK] [MASK] ( [MASK] ) est située à environ 10 minutes à pied du centre .
Elle relie [MASK] à la plupart des villes belges dont [MASK] en 1 heure environ et [MASK] ( côte belge ) en environ 2 heures .
La gare accueille aussi le [MASK] [MASK] et rallie [MASK] en environ 3 heures , [MASK] en 3 ou 4 heures selon les cas , [MASK] ainsi que [MASK] en 4 heures et [MASK] en 1 heures 20 .
Avec 10.113 voyageurs par jour , la [MASK] [MASK] [MASK] se classe 5 e de [MASK] et 15 e parmi les 545 gares belges .
Ainsi que des arrêts à [MASK] , [MASK] , [MASK] et [MASK] .
[MASK] est situé entre les aéroports civils internationaux de [MASK] , de [MASK] et de [MASK] .
Depuis le début des années 2000 , la ville a mis au point avec le [MASK] [MASK] ( la société publique wallonne de transport par bus ) un système de navettes qui parcourt la ville selon trois itinéraires .
Il est également possible de se rendre à [MASK] par voie maritime grâce à son port intérieur du [MASK] [MASK] qui est à l' intersection de deux canaux dont le gabarit est de 1350 tonnes .
La vie politique montoise est marquée depuis le milieu des années 1950 par la gouvernance du [MASK] [MASK] .
La puissance de ce parti de gauche ( renforcée par les électeurs des communes du [MASK] fusionnées à partir de 1972 ) s' enracine dans son histoire sociale et économique comme pour la plupart des autres grandes villes wallonnes .
Le parti socialiste , malgré la majorité absolue dont il dispose , partage le pouvoir depuis l' an 2000 avec le [MASK] [MASK] , un parti de droite .
Ce budget , qui est en partie le fruit des recettes fiscales communales , a été en équilibre grâce à un apport financier de la [MASK] [MASK] .
[MASK] abrite , entre autres , six commissariats de proximité de police .
une caserne de pompiers , deux hôpitaux , une prison ( pour les longues peines ) , un palais de justice , une cour d' appel et le siège du gouvernement de la [MASK] [MASK] [MASK] .
Quelques HLM ont ainsi été bâties à [MASK] , [MASK] et [MASK] , dans la proche banlieue de la ville .
16,5 % de la population de la ville habite en appartement ( 17 % pour la [MASK] ) et 82,7 % en maison unifamiliale ( 82,3 % en [MASK] ) .
Sur les 82,7 % de maisons unifamiliales , seulement 26 % ( 37,3 % en [MASK] ) sont des maisons séparées alors que 55,7 % ( 44,4 en [MASK] ) sont des maisons accolées ou mitoyennes .
Il est intéressant de remarquer que les chiffres indiquent très clairement la forte présence de maisons accolées plutôt que de maisons séparées : cela représente bien l' urbanisation du centre-ville , mais également de noyaux urbains tels que [MASK] et [MASK] .
La [MASK] est le centre de la vieille ville .
La [MASK] est également dotée d' une fontaine , dont l' inauguration date du 21 mars 2006 .
Le 23 avril 2006 a été inauguré un groupe statuaire en bronze de [MASK] [MASK] , déjà auteur d' une fresque pour la salle des mariages .
Bien que située au cœur de l' ancien [MASK] [MASK] [MASK] , elle est une des églises les plus caractéristiques et les plus homogènes de l' architecture gothique brabançonne .
Elle renferme de nombreuses œuvres de [MASK] [MASK] [MASK] .
Il est classé patrimoine mondial de l' [MASK] depuis le 1 er décembre 1999 .
Le hall industriel actuellement présent est le seul vestige de la machine qui alimenta [MASK] en eau potable dès 1871 , année où la [MASK] fut détournée .
La machine-à-eau reste le témoignage des préoccupations sanitaires et hygiéniques nées à [MASK] dans les années 1865-1870 ; elle marque le passage du système médiéval d' alimentation en eau par les puits , fontaines et pompes à mains , par fonctionnement de pompes aspirantes et foulantes .
Ces deux changements sont rendus possibles grâce à la démolition des fortifications , qui dégage des terrains , et au détournement de [MASK] [MASK] dont le rôle stratégique d' alimentation des fossés était alors dépassé .
En 2009 cet événement a été transféré sur la [MASK] .
Le style néo-égyptien était alors devenu une référence pour l' architecture maçonnique , et on peut considérer la loge de [MASK] comme un modèle du genre avec ses nombreux motifs " égyptiens " , chapiteaux papyriformes , frise en bouton de lotus , etc .
Elle est l' œuvre de l' architecte [MASK] [MASK] [MASK] ( 1789-1844 ) .
et la télévision régionale [MASK] [MASK] [MASK] ( [MASK] [MASK] ) .
Trois des puits-fontaines qui ornaient autrefois les places de [MASK] ont subsisté jusqu' à aujourd'hui .
Les casemates et la boulangerie militaire sont les restes des fortifications datant du [MASK] [MASK] [MASK] ( 1814-1830 ) .
Cette construction défensive en moellons de grès de [MASK] fut érigée aux environs de 1358 .
Finalement , il ne faudrait pas oublier également le petit [MASK] [MASK] [MASK] situé dans le village de [MASK] .
La culture musicale montoise se perpétue grâce à l' existence de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] qui s' est par exemple produit à [MASK] et le [MASK] [MASK] [MASK] [MASK] .
Ce festival dont la renommée progresse d' année en année depuis 1984 a accueilli quelques célébrités dont [MASK] [MASK] .
Aux XV e et XVI e siècles , la ville fut le lieu d' une vie théâtrale intense , plus intense que dans d' autres villes comme [MASK] , [MASK] , [MASK] ou même [MASK] .
[MASK] dispose d' un riche passé dans l' art de l' orfèvrerie .
[MASK] possède plusieurs musées d' importance .
Les artistes représentés sont originaires de [MASK] et plus généralement du [MASK] , mais aussi d' autres artistes belges et internationaux : [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] entre autres .
Le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] possède plusieurs collections prestigieuses dont une collection de pendules de la période 1795-1815 et tout un panel d' objets décoratifs de qualité ( orfèvrerie , faïencerie , porcelaine … ) .
Le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] propose des collections relatives à la flore et à la faune du monde entier .
Les festivités ont lieu le dimanche de la [MASK] et sont déjà précédées d' activités le vendredi et le samedi soir avec notamment un concert gratuit sur la [MASK] et se prolonge jusqu' au mercredi suivant , avec le feu d' artifice le mardi soir et le dimanche ; jour où se déroule à nouveau le combat mais cette fois ce sont les enfants qui sont acteurs et qui constituent le public .
[MASK] voit également en août un rassemblement d' engins militaires de la seconde guerre mondiale .
Durant l' automne , [MASK] accueille une grande fête foraine ( foire de [MASK] ) .
[MASK] est située également à proximité du parc animalier [MASK] , des remarquables châteaux du [MASK] comme ceux de [MASK] , [MASK] et d' [MASK] , de l' [MASK] [MASK] [MASK] , elle-même considérée comme patrimoine exceptionnel par la [MASK] [MASK] .
[MASK] est jumelée avec :