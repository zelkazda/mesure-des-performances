Il s' agit donc d' un standard dont les spécifications sont implémentées dans la plupart des langages script , comme [MASK] ou [MASK] .
La société [MASK] , à l' origine connue sous le nom de [MASK] [MASK] [MASK] , a développé un langage de script côté client , appelé [MASK] , pour renforcer l' offre commerciale de son serveur web .
[MASK] travailla alors au développement d' une version orientée client de [MASK] .
Quelques jours avant sa sortie , [MASK] changea le nom de [MASK] pour [MASK] .
[MASK] [MASK] et [MASK] étaient partenaires , et la machine virtuelle [MASK] de plus en plus populaire .
Ce changement de nom servait les intérêts des deux sociétés , et [MASK] promit de ne pas poursuivre [MASK] .
En décembre 1995 , [MASK] et [MASK] annoncent ( en ) la sortie de [MASK] .
En mars 1996 , [MASK] implémente le moteur [MASK] dans son navigateur web [MASK] [MASK] 2.0 .
Le succès de ce navigateur contribue à l' adoption rapide de [MASK] dans le développement web orienté client .
[MASK] réagit alors en développant [MASK] , qu' il inclut ensuite dans [MASK] [MASK] 3.0 en août 1996 pour la sortie de son navigateur .
[MASK] soumet alors [MASK] à l' [MASK] pour standardisation .
Les travaux débutent en novembre 1996 , et se terminent en juin 1997 par l' adoption du nouveau standard [MASK] .
[MASK] for [MASK] ( [MASK] ) est une extension [MASK] au langage [MASK] .
Il n' est pas connu si [MASK] restera synchronisé avec de futures modifications des spécifications d' [MASK] .
Une première version du référentiel d' implémentation d' [MASK] 4 est disponible depuis juin 2007 .
Par ailleurs , [MASK] et [MASK] [MASK] travaillent depuis des mois à l' implémentation des spécifications d' [MASK] 4 dans [MASK] 2 et [MASK] 3 .
Pour concilier ces implémentations avec la création de la [MASK] [MASK] [MASK] , [MASK] et [MASK] [MASK] collaborent sur le [MASK] [MASK] .