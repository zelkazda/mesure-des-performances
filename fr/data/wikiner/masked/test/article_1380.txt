L ' utopie ( néologisme de l' écrivain anglais [MASK] [MASK] ) , synthèse des mots grecs οὐ-τοπος ( lieu qui n' est pas ) et εὖ-τοπος ( lieu de bonheur ) est une représentation d' une réalité idéale et sans défaut .
Devant la menace de la censure politique ou religieuse , les auteurs situent l' action dans un monde imaginaire , île inconnue par exemple ( [MASK] [MASK] [MASK] [MASK] , [MASK] , 1725 ) , ou montagne inaccessible ( la découverte de l' [MASK] , dans [MASK] , 1759 ) .
Ces deux aspects du texte de [MASK] [MASK] ont amené à qualifier d' utopie des œuvres très différentes .
C' est ainsi que des œuvres telles que [MASK] [MASK] [MASK] [MASK] ( 1721 ) de [MASK] [MASK] furent qualifiées en leur temps d' utopies .
[MASK] [MASK] inventa le genre littéraire de l' utopie , il avait l' ambition d' élargir le champ du possible et non de l' impossible comme ce mot est synonyme aujourd'hui .
Autrement dit , [MASK] [MASK] considère l' utopie comme une œuvre purement romanesque , nécessairement progressiste , constituée de deux éléments : " le cadre , c' est-à-dire le récit d' aventures fantaisistes ou fantastiques , le roman merveilleux ou géographique ; le contenu , c' est-à-dire la représentation d' une société idéale. "
Pour [MASK] , il va sans dire que ne peuvent être considérées comme de véritables utopies les œuvres où domine le second élément , le contenu , c' est-à-dire la représentation d' une société parfaite ou du moins perfectionnée .
" Tout au plus , dit-il , peut-on ranger , si l' on veut , la [MASK] dans la catégorie des utopies pédagogiques et le mettre à côté de [MASK] , auquel il servit d' ailleurs de modèle. "
Par ailleurs , [MASK] [MASK] observe que les récits utopiques répondent à un besoin social .
[MASK] [MASK] , de son côté , écrit " L' utopie est simplement ce qui n' a pas encore été essayé ! "
[MASK] est le premier grand idéaliste de la pensée occidentale .
On peut en effet rapprocher l' utopie ( au sens moderne que prit ce mot ) du concept d' idée de [MASK] .
[MASK] voulut donc tracer les grandes lignes de ce que devait être une cité organisée de manière idéale par castes .
L' avocat et homme de lettres [MASK] [MASK] s' inscrit , à l' instar de son ami [MASK] , dans le cadre du mouvement humaniste qui redécouvre la littérature antique grecque et latine et s' en inspire .
Le texte de [MASK] est cependant également tributaire de son époque : il emprunte en partie sa forme aux récits de voyage de [MASK] [MASK] [MASK] ou de [MASK] .
[MASK] s' abstient pourtant de présenter son utopie comme un programme politique .
Ainsi , le genre littéraire crée par [MASK] [MASK] repose sur un paradoxe .