Le [MASK] est une constellation du zodiaque qui est traversée par le [MASK] du 19 avril au 14 mai .
Dans l' ordre du zodiaque , la constellation se situe entre les [MASK] à l' ouest et le [MASK] à l' est .
[MASK] était l' une des 48 constellations identifiées par [MASK] .
Il semblerait que les [MASK] , les [MASK] , les [MASK] , et les [MASK] aient tous nommé cette constellation " [MASK] " .
Son étoile principale était [MASK] [MASK] ( ce qui explique l' absence de lettre grecque pour désigner cette étoile ) .
Elle se repère par ses deux étoiles principales , α et β du [MASK] , qui ne pointent sur rien de très évident .
Parmi les autres étoiles du [MASK] , on trouve entre autres [MASK] ( γ ) et [MASK] ( δ ) .
C' est une géante orange , 15 fois plus grande que le [MASK] et 90 fois plus brillante .
[MASK] est la deuxième étoile la plus brillante de la [MASK] [MASK] [MASK] .
C' est une étoile blanche de la séquence principale , seulement deux fois plus massive que [MASK] [MASK] .
[MASK] n' est pas la troisième étoile de la constellation , mais la quatrième .
[MASK] est une étoile double .
Les deux étoiles combinées donnent à [MASK] une magnitude de 3,88 .
Sans être une constellation excessivement étendue , le [MASK] possède un assez grand nombre d' étoiles visibles à l' œil nu sans être particulièrement brillantes .