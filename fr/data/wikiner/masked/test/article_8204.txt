[MASK] est un client de messagerie développé par le projet [MASK] , pour les systèmes d' exploitation [MASK] , [MASK] et autres systèmes apparentés à [MASK] .
Cependant , des portages récents permettent aujourd'hui de l' utiliser sous [MASK] ou [MASK] .
[MASK] est placé sous la licence [MASK] .
[MASK] peut accéder aux comptes de courrier électronique en utilisant les protocoles [MASK] et [MASK] ; il peut gérer plusieurs comptes .
Il est également possible d' envoyer des courriers électroniques en utilisant le protocole [MASK] .
Les courriers électroniques peuvent être envoyés et reçus sous forme de texte brut ou de documents [MASK] , et l' utilisation de jeux de caractères internationaux est possible dans ces deux formats .
Lors de l' envoi d' un courriel , [MASK] repère les expressions faisant référence à une pièce jointe ( " ci-joint " , " is attached " , etc . )
Si aucune pièce jointe n' a été ajoutée , [MASK] avertit le rédacteur de cet oubli et lui propose de joindre son fichier avant d' envoyer .
Le système de filtres de [MASK] peut être utilisé pour soumettre les messages entrants à des tests anti- spam et antivirus , en utilisant des programmes externes .
[MASK] intègre un système de configuration pour les applications de filtrage les plus utilisées , telles que [MASK] ou [MASK] .
Il est possible de chiffrer ou de signer électroniquement ses courriers électroniques grâce à l' intégration de [MASK] avec le logiciel de cryptographie [MASK] [MASK] [MASK] .
Le gestionnaire d' informations personnelles [MASK] utilise cette technologie pour intégrer [MASK] à son interface .
D' autre part , [MASK] peut interagir étroitement avec [MASK] , le logiciel de messagerie instantanée de [MASK] , par exemple en récupérant les adresses électroniques à partir de [MASK] , en affichant l' avatar du contact ou sa présence en ligne .