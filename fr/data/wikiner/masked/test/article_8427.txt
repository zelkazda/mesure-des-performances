[MASK] [MASK] ( * [MASK] , 25 septembre 1683 -- † [MASK] , 12 septembre 1764 ) est un compositeur français et théoricien de la musique .
Son plus jeune frère , [MASK] [MASK] , précocement doué pour la musique finit par exercer lui aussi cette profession -- avec beaucoup moins de succès .
Il avoue d' ailleurs plus tard regretter de n' avoir pas séjourné plus longtemps en [MASK] , où " il aurait pu perfectionner son goût " [ réf. souhaitée ] .
En janvier 1702 , on le trouve organiste intérimaire à la cathédrale d' [MASK] ( dans l' attente du nouveau titulaire , [MASK] [MASK] ) [ réf. nécessaire ] .
Il est vraisemblablement encore à [MASK] en juillet 1708 [ réf. nécessaire ] .
Il fait un court séjour à [MASK] lors de la mort de son père en décembre 1714 , y assiste au mariage de son frère [MASK] en janvier 1715 et retourne à [MASK] .
Toutefois , il retourne à [MASK] dès le mois d' avril , muni d' un nouveau contrat à la cathédrale , pour une durée de vingt-neuf ans .
La couverture de l' ouvrage le désigne comme " organiste de la [MASK] [MASK] [MASK] " .
C' est d' ailleurs à la foire qu' il rencontre [MASK] [MASK] qui en devient le librettiste .
Le parrain est son frère , [MASK] , avec lequel il conserve tout au long de sa vie de très bonnes relations .
C' est [MASK] [MASK] qui lui est préféré .
[MASK] [MASK] [MASK] [MASK] [MASK] aurait pu être ce librettiste .
Cependant , il ne tarde pas à être subjugué par sa musique et , pour saluer son double talent de savant théoricien et de compositeur de haut vol , lui invente le surnom d ' " [MASK] -- [MASK] " .
Il conserve ce poste ( où lui succéderont [MASK] puis [MASK] ) pendant 22 ans .
À cette époque , ses contemporains sensiblement du même âge , [MASK] -- de cinq ans son aîné , qui mourra en 1741 , [MASK] , [MASK] , [MASK] , ont déjà composé l' essentiel d' une œuvre très importante .
Bien sûr , [MASK] [MASK] [MASK] sacrifie aussi aux exigences particulières de la tragédie en musique qui donne une place importante aux chœurs , danses et aux effets de machinerie .
Après les répétitions à l' [MASK] [MASK] [MASK] [MASK] à partir de juillet , la première représentation a lieu le 1 er octobre .
Le vieux compositeur [MASK] [MASK] , qui assiste à la représentation estime d' ailleurs qu' il y a " assez de musique dans cet opéra pour en faire dix " , ajoutant que " cet homme les éclipserait tous " .
L' œuvre est créée à l' [MASK] [MASK] [MASK] [MASK] le 23 août 1735 et connaît un succès croissant .
De l' avis général , le livret narrant les aventures des divins jumeaux amoureux de la même femme est un des meilleurs qu' ait traités le compositeur ( même si le talent de [MASK] ne mérite pas l' appréciation dithyrambique de [MASK] à son égard ) .
[MASK] [MASK] [MASK] [MASK] connaissent un succès immédiat mais l' abbé [MASK] est appelé pour améliorer le livret ( particulièrement la deuxième entrée ) après quelques représentations .
[MASK] est très fier de son invention d' un système chiffré destiné à noter la musique , beaucoup plus simple selon lui que le système traditionnel de la portée .
[MASK] [MASK] [MASK] [MASK] , opéra-ballet , est créé à [MASK] le 12 octobre .
[MASK] [MASK] [MASK] [MASK] est une œuvre de pur divertissement qui doit réutiliser la musique de [MASK] [MASK] [MASK] [MASK] sur un livret minimal écrit par [MASK] .
C' est probablement pendant cette période qu' il entre en contact avec [MASK] [MASK] , intéressé par l' approche scientifique du musicien de son art .
Le 1 er août 1752 , une troupe itinérante italienne s' installe à l' [MASK] [MASK] [MASK] [MASK] pour y donner des représentations d' intermezzos et d' opéras bouffes .
La même œuvre avait déjà été donnée à [MASK] en 1746 , sans attirer quelque attention .
Il y a même un duel entre [MASK] [MASK] [MASK] , librettiste et admirateur du compositeur et le castrat italien [MASK] , qu' il blesse .
Il y rencontre parfois le jeune [MASK] qui écrit plus tard son éloge funèbre et qui y recueille quelques-unes de ses rares confidences désabusées :
[MASK] [MASK] attendront plus de deux siècles leur création triomphale à [MASK] en 1982 .
Le grand musicien est inhumé dès le lendemain à l' [MASK] [MASK] .
C' est [MASK] qui explique que " Toute son âme et son esprit étaient dans son clavecin ; quand il l' avait fermé , il n' y avait plus personne au logis " .
L' aisance financière lui était venue sur le tard , avec le succès de ses œuvres lyriques et l' attribution d' une pension par le roi ( quelques mois avant sa mort , il fut même anobli et fait chevalier dans l' [MASK] [MASK] [MASK] ) .
Un trait de caractère que l' on retrouve d' ailleurs chez d' autres membres de sa famille est une certaine instabilité : il s' est fixé à [MASK] vers l' âge de quarante ans après une phase d' errance et avoir tenu de nombreux postes dans des villes variées : [MASK] , peut-être [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , à nouveau [MASK] puis [MASK] .
Jean-Philippe a un jeune frère , [MASK] également musicien ( beaucoup moins célèbre ) .
Ce dernier a deux fils , musiciens comme lui mais à l' existence de " ratés " : [MASK] [MASK] qui inspire à [MASK] la matière de son livre [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] .
Au début du XVIII e siècle , la cantate est un genre qui remporte un grand succès : la cantate française -- à bien distinguer de la cantate italienne ou germanique -- est " inventée " en 1706 par le poète [MASK] [MASK] et aussitôt adoptée par plusieurs musiciens de renom tels [MASK] , [MASK] , [MASK] , [MASK] …
Celle-ci atteint son apogée pendant la décennie 1700-1710 avec les parutions successives des recueils de [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] .
Reprenant une formule pratiquée avec succès par [MASK] quelques années auparavant , les pièces en concert se démarquent des sonates à trois en ce que le clavecin ne se contente pas d' assurer la basse continue en accompagnement des instruments mélodiques ( violon , flûte , viole ) mais " concerte " à égalité avec eux .
Cette musicalité continue préfigure donc également le drame wagnérien plus que l' opéra " réformé " de [MASK] qui apparaîtra dans la fin du siècle .
Il était très exigeant , de mauvais caractère et ne put entretenir de longues collaborations avec ses différents librettistes , à l' exception de [MASK] [MASK] [MASK] .
) , cependant les conclusions qu' il en tire vont le confirmer dans cette voie , d' autant plus que , avant 1726 , il prend connaissance des travaux de [MASK] [MASK] sur les sons harmoniques qui viennent les corroborer de façon providentielle .
Le musicien n' est pourtant pas oublié : il est choisi pour que sa statue soit l' une des quatre qui ornent le grand vestibule de l' [MASK] [MASK] [MASK] conçu en 1861 par [MASK] [MASK] ; en 1880 , [MASK] lui rend également hommage par l' inauguration d' une statue .
L' [MASK] [MASK] [MASK] suit en 1908 avec [MASK] [MASK] [MASK] : c' est un semi-échec ; l' œuvre n' attirant qu' un public restreint , ne connaît que quelques représentations .
Enfin , la première ( sic ) représentation de sa dernière tragédie lyrique , [MASK] [MASK] , a même eu lieu en 1982 au [MASK] [MASK] [MASK] ( les répétitions avaient été interrompues par la mort du compositeur en 1764 ) .