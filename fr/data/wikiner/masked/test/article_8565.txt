[MASK] [MASK] est né le 19 janvier 1736 à [MASK] , petite ville d' [MASK] .
[MASK] [MASK] est allé à l' école de manière irrégulière et était préférentiellement instruit dans la demeure de ses parents par le soin de sa mère .
[MASK] a adapté le régulateur à boules -- déjà utilisé pour la régulation des moulins à vent et à eau- pour assurer la régulation de la vitesse d' une machine à vapeur .
[MASK] a grandement contribué à la transformation de la machine à vapeur embryonnaire en un moyen de production d' énergie fiable et économique .
Il s' est aperçu que la machine à vapeur de [MASK] gâchait presque trois quarts de l' énergie de la vapeur en chauffant le piston et la chambre .
[MASK] a développé une chambre de condensation séparée ce qui a augmenté significativement l' efficacité .
[MASK] était opposé à l' utilisation de vapeur à haute pression et certains considèrent qu' il a freiné le développement technique de la machine à vapeur par d' autres ingénieurs , jusqu' à ce que ses brevets expirent en 1800 .
En particulier l' interdiction faite à son employé [MASK] [MASK] de travailler avec de la vapeur à haute pression pour ses expérimentations sur la locomotive à vapeur , a retardé le développement et l' application de cette invention .
[MASK] s' est avéré excellent homme d' affaires , et les deux hommes ont fini par faire fortune .
[MASK] a également inventé plusieurs autres choses , un appareil pour copier les lettres n' étant pas la moindre [ réf. nécessaire ] .
Le statut de [MASK] comme véritable inventeur de certains des nombreux principes et inventions pour lesquels il a déposé des brevets est sujet à controverse .
Il a en particulier bénéficié des recherches des chimistes [MASK] [MASK] et [MASK] [MASK] ( 1718-1794 ) .
Comme il le dit dans une lettre adressée à [MASK] le 17 août 1784 :
La machine à vapeur conçue par [MASK] [MASK] a permis de passer d' une machine d' usage limité à une machine efficace aux nombreuses applications .