[MASK] [MASK] [MASK] ( né à [MASK] le 11 décembre 1810 et mort également à [MASK] le 2 mai 1857 ) est un poète et un dramaturge français de la période romantique .
Il reçoit la [MASK] [MASK] [MASK] en 1845 et est élu à l' [MASK] [MASK] en 1852 .
Sa santé se dégrade gravement avec son alcoolisme et [MASK] [MASK] [MASK] meurt à 46 ans , le 2 mai 1857 , à peu près oublié : il est enterré dans la discrétion au [MASK] [MASK] [MASK] .
Son grand-père était poète , et son père était un spécialiste de [MASK] , dont il édita les œuvres .
La figure de [MASK] joua en l' occurrence un rôle essentiel dans l' œuvre du poète .
Il lui rendit hommage à plusieurs reprises , attaquant au contraire violemment [MASK] , l' adversaire de [MASK] .
En octobre 1819 , alors qu' il n' a pas encore neuf ans , il est inscrit en classe de sixième au [MASK] [MASK] -- on y trouve encore une statue du poète -- , où il a pour condisciple et ami un prince du sang , le [MASK] [MASK] [MASK] , fils du [MASK] [MASK] [MASK] , et obtient en 1827 le deuxième prix de dissertation latine au [MASK] [MASK] .
Il sympathise alors avec [MASK] et [MASK] , et se refuse à aduler le " maître " [MASK] [MASK] .
Il moquera notamment les promenades nocturnes du " cénacle " sur les tours de [MASK] .
À l' âge de 22 ans , le 8 avril 1832 , [MASK] est anéanti par la mort de son père , dont il était très proche , victime de l' épidémie de choléra .
Cet évènement va décider de la carrière littéraire que [MASK] choisit alors d' entamer .
[MASK] tente sa chance au théâtre .
[MASK] exprime déjà dans ce recueil la douloureuse tension entre débauche et pureté qui domine son œuvre .
Ce voyage lui inspire [MASK] , considéré comme le chef d' œuvre du drame romantique , qu' il écrira en 1834 .
Puis il rencontre , le 29 mai 1839 , à la sortie du [MASK] , [MASK] , qui l' emmène souper chez elle , et avec laquelle il a une brève liaison en juin .
De même , en 1852 , [MASK] [MASK] qui est la maîtresse de [MASK] , aura quelque temps une liaison avec lui .
Après la [MASK] [MASK] [MASK] [MASK] , ses liens avec la [MASK] [MASK] [MASK] lui ont valu d' être révoqué de ses fonctions par le nouveau ministre [MASK] le 5 mai 1848 .
Nommé [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] le 24 avril 1845 , en même temps que [MASK] , il est élu à l' [MASK] [MASK] 12 février 1852 au siège du baron [MASK] , après deux échecs en 1848 et 1850 .
Sa mère décède à [MASK] le 11 février 1864 , à 83 ans .
En 1999 , la liaison entre [MASK] [MASK] [MASK] et de [MASK] [MASK] a fait l' objet d' une adaptation cinématographique de [MASK] [MASK] , [MASK] [MASK] [MASK] [MASK] .
Les œuvres de [MASK] ont fait l' objet de plusieurs adaptations cinématographiques .
Ainsi [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] a été adapté par [MASK] [MASK] en 2005 .