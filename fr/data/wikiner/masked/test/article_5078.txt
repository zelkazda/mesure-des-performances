Elle est située à 60 km au sud de [MASK] ( [MASK] ) sur la [MASK] [MASK] [MASK] et elle abrite un site archéologique de première importance en [MASK] du sud .
Le site comporte un grand nombre de monuments hindouistes dédiés à [MASK] , à [MASK] , mais aussi à [MASK] et aux héros du [MASK] .
Les sculptures qui couvrent la totalité de la surface de deux énormes rochers , soit 27 mètres de long sur 9 mètres de haut , dépeignent le cours du [MASK] depuis l' [MASK] tel que décrit dans le [MASK] .
Au terme de ces années , son cours avait été ralenti et [MASK] put le laisser couler librement .
La fissure centrale représentant le cours du [MASK] est peuplée de créatures aquatiques tels des nâgas et naginis .
De part et d' autre de cette représentation du fleuve , se trouve l' image de [MASK] .
Au-dessus de celle-ci , on trouve les ruines d' un réservoir , ce qui laisse penser qu' autrefois de l' eau s' écoulait pour matérialiser le [MASK] à l' occasion de rituel et d' offrandes .
De part et d' autre de ce relief sont figurés de grands éléphants dont l' interprétation reste incertaine : ils figurent peut être les piliers du monde , placés dans le monde souterrain : c' est là que les ancêtres de [MASK] avaient été réduits en cendres par [MASK] .
Ces événements avaient pour but de préparer cette [MASK] [MASK] [MASK] .
L' attribution de ce relief à un épisode du [MASK] ou du [MASK] fait encore débat et est très souvent sujet à polémique dans les milieux universitaires .
Le temple , qui a souffert depuis douze siècles de sa situation sur le rivage , est maintenant protégé de l' érosion éolienne par une haie et de celle des vagues par des blocs de rocher mis en place par le gouvernement d' [MASK] [MASK] , blocs qui lui ont permis de résister à la vague du tsunami du 26 décembre 2004 .
Le terme ratha est incorrectement utilisé ici car il signifie " chariot " ( voir [MASK] ) , comme ceux utilisés dans les processions .
Il y a quatre autres ratha ailleurs dans [MASK] .
Enfin , on trouve également dans le village un énorme rocher vaguement sphérique appelé la boule de beurre de [MASK] .
Le site de [MASK] est inscrit au patrimoine mondial de l' [MASK] depuis 1985 .
[MASK] accueille l' un des plus importants festivals de danse indienne à la fin de chaque année .