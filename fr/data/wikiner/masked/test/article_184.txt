Les couvertures de presque tous les volumes de la série utilisent des peintures de [MASK] [MASK] .
[MASK] , le jeu officiel tiré de ces romans , avait à l' époque rencontré un grand succès .
Cependant , [MASK] est plus qu' un simple plagiat car les thèmes qu' il aborde , notamment les intrigues politiques entre les peuples et les guildes d' influence , sont particulièrement bien pensés .
Une autre source d' inspiration est la trilogie [MASK] de [MASK] [MASK] , qui dota le monde de [MASK] de plusieurs lunes qui en fonction de leur phase influencent les passions des humains .
Ce continent , dont la géographie physique est inspirée par celle de l' [MASK] , est partagé entre plusieurs peuples .
Les armes-dieux sont la principale spécificité de [MASK] par rapport à d' autres jeux de rôle mediévaux-fantastiques .
Il est proposé aux personnages-joueurs de [MASK] d' incarner ou bien des porteurs d' arme ou bien les armes elles-mêmes .
A sa sortie en 1991 , la boîte de jeu de [MASK] a été un des plus gros succès du jeu de rôle français .