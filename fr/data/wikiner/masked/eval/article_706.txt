Après quelques années passées au collège de [MASK] [MASK] , [MASK] entre dans le génie militaire , puis dans les chevau-légers .
Ce mémoire lui ouvre la même année les portes de l' [MASK] [MASK] [MASK] .
Il est nommé aide de camp du [MASK] [MASK] [MASK] et il publie plusieurs mémoires sur l' hydraulique et la résistance des fluides .
En 1767 , [MASK] entre au service actif de la marine .
Ce navire partait pour les [MASK] [MASK] , puis les [MASK] , avec pour mission d' essayer de nouveaux modèles de montres et chronomètres marins , au nom de l' [MASK] [MASK] [MASK] .
En 1774 et 1775 , [MASK] participe à une expédition qui longe la côte de l' [MASK] .
Nommé lieutenant de vaisseau , il est ensuite envoyé aux [MASK] [MASK] et chargé d' en déterminer la position avec exactitude ; à cette époque , la plupart des nations d' [MASK] comptaient les longitudes à partir de l' [MASK] [MASK] [MASK] .
En 1781 , [MASK] reçoit le commandement de plusieurs vaisseaux de la flotte militaire française , chargés d' escorter un corps de troupe qu' on envoyait en [MASK] .
Libéré sur parole , il retourne en [MASK] peu de temps après .
Avec [MASK] [MASK] et [MASK] [MASK] , il est chargé par l' [MASK] [MASK] [MASK] de déterminer la longueur d' un arc de méridien , et il s' occupe en particulier de tout ce qui se rattache aux expériences de physique .