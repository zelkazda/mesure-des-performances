À l' exception de son travail comme réalisateur il est connu presque exclusivement sous son pseudonyme de [MASK] [MASK] .
Depuis avril 2005 , il est professeur à l' école universitaire des arts de [MASK] .
Les films de [MASK] semblent laisser des impressions controversées auprès du public .
Par la suite , [MASK] , réalisateur fasciné par la mort violente , a suggéré lors d' une interview que cet accident était une " tentative de suicide inconsciente " .
Après son accident de moto , [MASK] reprend la peinture .
Son style est réminiscent du peintre biélorusse [MASK] [MASK] .
Ses peintures ont été mises en évidence dans son film le plus acclamé par la critique , [MASK] sorti en 1997 .
En 1970 , à sa sortie de l' [MASK] [MASK] où il a fait des études d' ingénieur pendant 4 ans , [MASK] [MASK] trouve un travail de réparateur d' ascenseurs dans une boîte de nuit du quartier d' [MASK] à [MASK] , qui faisait office de théâtre de sketches comiques et de strip-tease .
Petit à petit quand un des membres réguliers du club tombe malade , [MASK] prend sa place sur scène .
[MASK] [MASK] est né .
La raison de leur succès est due aux thèmes de [MASK] , qui sont bien plus risqués que les traditionnels manzai .
Certains éléments autobiographiques remontant à sa carrière manzai se trouvent dans son film de 1996 , [MASK] [MASK] .
En 1983 , dans un tout autre registre , il joue dans son premier grand film : [MASK] [MASK] l' engage dans son film [MASK] .
Après plusieurs autres rôles , pour la plupart des comédies , il est choisi , en 1989 , pour jouer dans [MASK] [MASK] .
Lorsque [MASK] [MASK] le réalisateur initial tombe malade , [MASK] se propose pour la réalisation du film .
Le film est un succès au [MASK] tant commercial qu' auprès de la critique .
Il marque le début de la carrière de réalisateur de [MASK] .
[MASK] , son deuxième film en tant que réalisateur et le premier comme scénariste sort en 1990 .
Avec un ami , il se rend à [MASK] [MASK] pour se procurer des armes et assouvir sa vengeance .
Sur le chemin ils sont aidés par un gangster psychotique joué par [MASK] , qui a sa propre vengeance à assouvir .
Avec une maîtrise complète du script et de la réalisation , [MASK] utilise ce film pour affirmer son style : une violence choquante , un humour noir étrange et des scènes d' images arrêtées stoïques .
Le troisième film de [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] , sort en 1991 .
Le film montre une facette plus romantique et délicate de [MASK] ainsi que son humour pince-sans-rire qui est sa marque de fabrique .
Les spectateurs étrangers , qui dépasseront en nombre son public national dans les années qui suivront , remarquèrent [MASK] après la sortie de [MASK] en 1993 .
Il y joue un yakuza de [MASK] envoyé par son patron à [MASK] pour aider à mettre fin à une guerre de gang .
Les cinéphiles ont reconnu les influences des films dirigés par [MASK] : le cinéma américain ; notamment [MASK] [MASK] , ainsi que le maître du yakuza eiga ( films sur la pègre japonaise moderne ) , [MASK] [MASK] , à qui il rend hommage dans un de ses chefs-d'œuvre , [MASK] , mélodie mortelle ( 1993 ) .
La sortie de [MASK] [MASK] [MASK]
en 1995 montre un [MASK] retournant à ses racines , la comédie .
Une bonne partie du film moque la culture japonaise populaire , comme [MASK] ou [MASK] et même le personnage de [MASK] que [MASK] lui-même interprètera huit ans plus tard .
La même année , [MASK] apparait dans [MASK] [MASK] de [MASK] [MASK] , bien que son temps sur écran ait été énormément réduit pour la sortie américaine du film .
[MASK] fait [MASK] [MASK] en 1996 , juste après son rétablissement .
À cette époque c' est son film le plus réussi au [MASK] .
[MASK] y apparaît comme un escroc bon à rien qui finit par faire équipe avec un jeune garçon cherchant sa mère .
[MASK] joue [MASK] dans [MASK] [MASK] de 2000 , un blockbuster à grand succès japonais controversé situé dans un future dystopique morne où un groupe d' adolescents est aléatoirement choisi chaque année pour se tuer sur une île déserte .
Le film [MASK] [MASK] [MASK] [MASK] de 2001 , tourné à [MASK] [MASK] , montre un [MASK] en yakuza de [MASK] chargé de l' installation d' un empire de drogues à [MASK] [MASK] avec l' aide d' un gangster local joué par [MASK] [MASK] .
Il dirige [MASK] , en 2002 , sans jouer dans le film bien accueilli par le public et la critique .
Il y raconte trois versions différentes de l' amour éternel.. [MASK] travaillait souvent avec [MASK] [MASK] , compositeur de la majorité de ses musiques de film jusqu' en 2002 , après [MASK] , ils se sont séparés .
[MASK] de 2003 , dans lequel [MASK] est acteur et réalisateur nous montre toute la verve du réalisateur .
Un merveilleux film dans lequel il se teint les cheveux en blond , ce qui choque initialement au [MASK] vu que les héros traditionnels de [MASK] ont tous les cheveux noirs , ce film a fait taire beaucoup de ces détracteurs .
[MASK] est une pure invention cinématographique et une réussite évidente confirmant les talents de ce réalisateur atypique extrêmement doué .
En 2007 sort [MASK] [MASK] [MASK] [MASK] [MASK] , suivi en 2008 par [MASK] [MASK] [MASK] [MASK] .
[MASK] a l' habitude de faire appel régulièrement au compositeur [MASK] [MASK] , qui a créé un grand nombre de musiques pour ses films .
Cependant , pendant la réalisation de [MASK] ils ont eu un désaccord , apparemment concernant les airs a inclure dans la bande originale du film .
[MASK] [MASK] a sorti un jeu vidéo du nom de [MASK] [MASK] [MASK] en 1986 sur la console [MASK] de [MASK] .
Le principe est d' accomplir des missions répétitives , inutiles et presque impossibles tel que chanter pendant une heure sans s' arrêter ( la [MASK] possédait un micro ) , appuyer pendant 4 heures sur la touche " select " , ou frapper le boss final 20000 fois .
La carrière de [MASK] [MASK] balance entre ses deux personnages .
Il réserve [MASK] [MASK] à ses rôles d' homme " sérieux " ( comme le réalisateur , mais aussi certains de ses rôles d' acteur , par exemple pour [MASK] et [MASK] [MASK] ) et [MASK] [MASK] à l' acteur , le comique et aussi l' animateur d' émissions télévisées .
C' est d' ailleurs dans ce cadre qu' il a créé le jeu télévisé [MASK] [MASK] [MASK] , diffusé sur [MASK] [MASK] [MASK] de 1986 à 1989 , et ultérieurement dans d' autres pays .
Outre la trame sensiblement identique ( un chef de gang téméraire déchu et trahi par son chef est banni de la métropole et s' exile sur [MASK] [MASK] en compagnie de ses lieutenants ) , et la brutalité des scènes de combat au pistolet ou à main nue ( [MASK] est le berceau du karaté et du nunchaku ) , on décèle une frappante ressemblance entre le chef de gang de chacun des deux films ( costume impeccable et lunettes noires de rigueur , esprit fier et de la vieille école , volontiers frondeur envers les caïds croulants ) , de même qu' entre les deux idylles éphémères mais sincères que vit le héros .
Ce qui par dessus tout ne laisse aucun doute quant à l' inspiration de [MASK] par [MASK] , c' est la chanson en dialecte d' [MASK] , accompagnée au shamisen ( un instrument de musique traditionnel à cordes ) qui se répète tout au long des deux films .
La chanson est interprétée en costumes traditionnels , comme dans le film de [MASK] .
[MASK] [MASK] [MASK] est un jeu télévisé diffusé depuis les années 1980 .
[MASK] [MASK] s' est également essayé à la chanson , il a sorti plusieurs albums dont certains ont eu un petit succès commercial au [MASK] .
Il réalise également des peintures , présentes en grand nombre dans son film [MASK] , ainsi que des sculptures .
[MASK] a écrit plus de cinquante livres de poésie , des critiques de films et plusieurs romans , dont quelques-uns ont aussi été adaptés dans des films par d' autres réalisateurs .