Son existence officielle a été annoncée le 20 juin 2003 , par [MASK] [MASK] , co-fondateur de [MASK] .
Elle est souvent appelée [MASK] [MASK] en français .
est l' organisation qui soutient les projets en ligne [MASK] , [MASK] , [MASK] , [MASK] , [MASK] [MASK] , [MASK] , [MASK] , [MASK] et [MASK] , développés grâce au logiciel [MASK] .
La [MASK] [MASK] possède de nombreux noms de domaines , les droits sur les logos et certains noms de marque .
Le 28 octobre 2006 , [MASK] [MASK] a été élue présidente du conseil d' administration .
Elle est remplacée , le 17 juillet 2008 , par [MASK] [MASK] , avocat américain .
Son mandat a été prorogé d' un an par le conseil d' administration de [MASK] [MASK] .
L' annonce de la création de la [MASK] [MASK] a été faite par [MASK] [MASK] en juin 2003 .
[MASK] [MASK] fonctionne pour l' essentiel par l' intermédiaire des dons faits par les lecteurs .
En avril 2009 , la [MASK] [MASK] signe un partenariat avec [MASK] afin de développer des fonctionnalités de l' encyclopédie [MASK] susceptibles d' élargir le libre accès à la connaissance .
Tous les projets fonctionnent avec le moteur de wiki [MASK] .