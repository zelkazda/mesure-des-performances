[MASK] catégorie administrative est , en [MASK] , le plus haut degré de coopération intercommunale , par laquelle plusieurs communes mettent en commun une partie de leur fonctionnement .
Dans un premier temps , la loi du 31 décembre 1966 crée les communautés urbaines pour plusieurs villes ( [MASK] , [MASK] , [MASK] et [MASK] ) .
La [MASK] [MASK] du 12 juillet 1999 limite la création de nouvelles communautés urbaines à des ensembles d' au moins 500 000 habitants comportant au moins une ville de plus de 50 000 habitants , d' un seul tenant et sans enclave .
L' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] regroupe seize communautés urbaines depuis le 28 janvier 2009 .
Depuis cette date et pour un mandat de trois ans renouvelable , son président est [MASK] [MASK] , président du [MASK] [MASK] .
Les communautés urbaines créées avant la [MASK] [MASK] restent soumises à l' ancien régime , moins étendu .
Il y a aujourd'hui seize communautés urbaines en [MASK] , qui regroupent en tout 7,64 millions d' habitants ( au 1 er janvier 2010 ) soit près de 12 % de la population française .
[MASK] [MASK] , maire de [MASK] et [MASK] [MASK] , maire de [MASK] envisagent de rassembler la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , ainsi que toutes celle qui les alentours formant le sillon mosellan afin de transformer le territoire qui compte une population d' environ 600 000 habitants en communauté urbaine .
Elle représentera la 11ème aire urbaine française et constituera une métropole de poids entre [MASK] et [MASK] .
Une alliance avec [MASK] est parfois évoquée afin de constituer une nouvelle grande métropole méridionale face à [MASK] , [MASK] et [MASK] .
[MASK] [MASK] , dont la structure et le statut juridique demeurent à définir mais devraient différer de celui des communautés urbaines ou communautés d' agglomération , est actuellement dans les cartons et devrait offrir à la capitale et sa région urbaine une structure de coopération pérenne dite également du [MASK] [MASK] ( projet gouvernemental concurrent ) .
Le refus de la [MASK] [MASK] [MASK] [MASK] d' adhérer à ce projet , lors de son conseil communautaire du 16 décembre 2008 , ne permet plus d' attendre le seuil des 500 000 habitants nécessaire à la constitution d' une communauté urbaine .
Malgré cette décision , [MASK] [MASK] , président de l' [MASK] [MASK] [MASK] confirme sa volonté de poursuivre ce projet par étape , au travers , dans un premier temps de la création d' une " grande communauté " d' agglomération .
[MASK] , par arrêté préfectoral daté du 24 décembre 2008 , le [MASK] [MASK] devient officiellement une communauté urbaine .
Deux communautés d' agglomération voisines d' environ 70 000 habitants chacune , la [MASK] [MASK] [MASK] [MASK] [MASK] et la [MASK] semblent pour le moment n' avoir aucun intérêt à rejoindre la communauté urbaine dès lors que cela impliquerait la mise en place d' une taxe additionnelle sur les ménages .
À terme , la future [MASK] [MASK] [MASK] [MASK] peut espérer seulement voir la commune de [MASK] la rejoindre .
Même si elle revêt des réalités institutionnelles différentes , l' appellation " communauté urbaine " possède des équivalents en [MASK] et dans le monde .
Il en est de même pour [MASK] ( [MASK] ) , [MASK] ( [MASK] ) et [MASK] ( [MASK] [MASK] [MASK] ) .