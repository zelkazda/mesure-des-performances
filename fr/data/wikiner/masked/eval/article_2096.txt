Il a opposé les autorités et l' armée françaises à des indépendantistes musulmans , principalement réunis sous la bannière du [MASK] [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] [MASK] , qui est aussi une double guerre civile , entre les communautés d' une part et à l' intérieur des communautés d' autre part , entraîne de graves crises politiques jusqu' en [MASK] métropolitaine , avec pour conséquences le retour au pouvoir de [MASK] [MASK] [MASK] et la chute de la [MASK] [MASK] , remplacée par la [MASK] [MASK] .
Après avoir donné du temps à l' armée pour qu' elle utilise tous les moyens à sa disposition pour écraser définitivement l' insurrection , [MASK] [MASK] penche finalement pour l' indépendance en tant que seule issue possible au conflit , ce qui conduit une fraction de l' armée française à se rebeller et entrer en opposition ouverte avec le pouvoir .
Le terme de " [MASK] [MASK] [MASK] " a été officiellement adopté en [MASK] le 18 octobre 1999 .
Elle oppose l' armée française , faisant cohabiter commandos de troupes d' élite , goums marocains ( jusqu' à leur dissolution en avril 1956 ) , forces de maintien de l' ordre ( gardes mobiles , [MASK] [MASK] [MASK] [MASK] ( [MASK] ) ) , appelés du contingent et supplétifs indigènes ( harkis , moghaznis ) aux groupes armés indépendantistes de l' [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , branche armée du [MASK] [MASK] [MASK] [MASK] ( [MASK] ) d' encadrement politico-administratif .
Le conflit se double d' une guerre civile et idéologique à l' intérieur des deux communautés , donnant lieu à des vagues successives d' attentats , assassinats et massacres sur les deux rives de la [MASK] .
Une loi devrait permettre en [MASK] l' accès aux archives classifiées " secret défense " postérieure à 1948 à l' horizon 2012 .
Cette disposition , vivement critiquée par des historiens car elle aurait accru les délais de communicabilité des archives relatives à [MASK] [MASK] [MASK] [MASK] , a finalement été retirée du texte au cours de l' examen du texte à [MASK] [MASK] [MASK] .
En [MASK] , cette guerre est appelée également " révolution algérienne " ( appellation initiale ) par analogie avec les révolutions américaine , française et russe .
Pour des raisons assez complexes , [MASK] [MASK] n' a reconnu qu' il s' agissait d' une guerre qu' en 1999 , sous la présidence de [MASK] [MASK] .
Toutefois , dans les textes législatifs notamment , l' expression officielle consacrée continue d' être les [MASK] [MASK] [MASK] .
À l' époque même des faits , [MASK] [MASK] considérait qu' il ne s' agissait pas d' une guerre mais de troubles à l' ordre public et plus substantiellement des troubles contre l' ordre établi .
Si [MASK] [MASK] a reconnu la guerre , seuls ont été également reconnus officiellement des actes individuels commis par les militaires sans pour autant les condamner .
Cependant , avant , pendant et bien après [MASK] [MASK] [MASK] [MASK] , la majorité des populations musulmanes , y compris celles qui ont adhéré à la vision du [MASK] , considère les pieds noirs comme des [MASK] à part entière , que les vicissitudes de l' histoire ont lancés sur le chemin de l' exil .
A l' attention des enseignants français , des projections " pour les classes " sont organisées et un " dossier d' accompagnement pédagogique " est proposé sur le site officiel de la fiction de [MASK] [MASK] , cette dernière a même une influence politique .
Les partisans de la commémoration du 19 mars soutiennent la proposition depuis une décennie sans en avoir obtenu l' adoption par les deux chambres , comme en atteste le dossier de 2005 portant sur la " journée nationale du souvenir de [MASK] [MASK] [MASK] [MASK] " .
Cette proposition est redéposée par [MASK] [MASK] ( [MASK] ) , [MASK] [MASK] ( [MASK] [MASK] [MASK] ) , [MASK] [MASK] , [MASK] [MASK] ( [MASK] [MASK] ) et [MASK] [MASK] ( [MASK] ) .
Le texte n°762 dit " petite loi " est adopté par l' [MASK] [MASK] en première lecture le 22 janvier 2002 .
En 2003 , le maire de [MASK] , [MASK] [MASK] ( [MASK] [MASK] ) , commémore le cessez-le feu en baptisant une voie " [MASK] [MASK] [MASK] " .
De même , il existe des " rue du 19 mars 1962 " et " avenue du 19 mars 1962 " en [MASK] .
Cette querelle franco-française liée à la date 19 mars se prolonge , le 29 avril 2010 , avec la proposition de loi de [MASK] [MASK] ( [MASK] ) " visant à établir la reconnaissance par [MASK] [MASK] des souffrances subies par les citoyens [MASK] [MASK] [MASK] , victimes de crimes contre l' humanité du fait de leur appartenance ethnique , religieuse ou politique " .
[MASK] [MASK] de la repentance est une des constantes des relations franco algériennes .
En 1999 , 2004 et en 2007 , le président algérien [MASK] [MASK] a , en différentes occasions , qualifié la colonisation française de génocide culturel et a appelé [MASK] [MASK] à assumer son histoire en présentant des excuses formelles .
Un article du [MASK] du 10 février 2010 résume la situation en ces termes :
Les termes " terroriste " et " terrorisme " sont inscrits à six reprises dans la charte algérienne et servent à désigner les attentats perpétrés par le [MASK] [MASK] [MASK] ( [MASK] ) ( 1991-2002 ) .
Les agents du [MASK] se définissent comme des " résistants " , alors que les autorités politiques françaises les qualifient de " rebelles " .
Dans les manuels d' histoire et la presse algérienne , on se montre indulgents envers l' armée française mais pas à l' égard de l' [MASK] , qualifié d' organisation terroriste derrière l' exil de la composante [MASK] de la population algérienne .
Le 18 avril 1951 , [MASK] [MASK] signe le traité instituant la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
Le 27 mai 1952 , le traité instituant la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) est adopté par le gouvernement français .
À la suite de la [MASK] [MASK] [MASK] , [MASK] [MASK] s' engage résolument dans une politique européenne qui dessine l' avenir de la nation .
Au début de [MASK] [MASK] [MASK] [MASK] , des forces politiques encore puissantes essayent de maintenir ce qui reste de l' [MASK] [MASK] [MASK] , mais les conséquences du choix du [MASK] [MASK] [MASK] pour [MASK] [MASK] sont inexorables .
Le conflit s' inscrit dans le cadre du processus de décolonisation qui se déroule après la fin de la [MASK] [MASK] [MASK] .
Pour la [MASK] , cela concerne entre autres les colonies françaises d' [MASK] ( [MASK] [MASK] [MASK] de 1946-1954 ) , la [MASK] , [MASK] , l' [MASK] [MASK] [MASK] et l' [MASK] [MASK] .
Le [MASK] et la [MASK] ne sont pas des colonies mais des protectorats , celui du [MASK] étant un protectorat franco-espagnol .
L' [MASK] est un cas spécifique n' étant ni une colonie à partir de 1848 ( création des provinces françaises de l' [MASK] , [MASK] et [MASK] qui deviennent des départements jusqu' en 1962 ) , ni un protectorat depuis la [MASK] [MASK] [MASK] par les français en 1830 .
L' [MASK] française qui doit rétablir l' ordre le fait sans ménagement pour la population civile .
Dans son rapport , le [MASK] [MASK] , maître d' œuvre de la répression , se montra prophétique : " je vous donne la paix pour dix ans , à vous de vous en servir pour réconcilier les deux communautés " , .
La principale cause du déclenchement de cette guerre réside dans le blocage de toutes les réformes , dû au fragile équilibre du pouvoir sous la [MASK] [MASK] [MASK] , et à l' opposition obstinée de la masse des [MASK] et de leurs représentants hostiles à toute réforme en faveur des musulmans .
En 1960 , 85000 musulmans servaient dans l' [MASK] régulière plus environ 150000 supplétifs soit au total près de 235000 musulmans combattant aux cotés des soldats français .
Selon [MASK] [MASK] , on comptait ainsi quatre fois plus de combattants musulmans dans le camp français que dans celui du [MASK] .
La plupart des figures du mouvement algérien vont être surveillées de près par les services policiers français , d' autres seront exilées vers d' autres pays comme l ' a été l' émir [MASK] [MASK] [MASK] [MASK] en [MASK] puis en [MASK] .
À [MASK] , après des heurts entre policiers et nationalistes , la manifestation tourne à l' émeute et la colère des manifestants se retourne contre les " [MASK] " : 27 [MASK] sont assassinés ( 103 trouveront la mort dans les jours suivants ) .
La répression de l' [MASK] française est brutale .
Officiellement , elle fait 1500 morts parmi les musulmans , chiffre sous-estimé et probablement plus proche des 20000 à 30000 selon l' historien [MASK] [MASK] .
Le [MASK] [MASK] [MASK] [MASK] ( [MASK] ) estime qu' il y a eu 45000 morts .
De par la radicalisation qu' ils ont engendrée dans les milieux nationalistes algériens , certains historiens considèrent ces massacres comme le véritable début de [MASK] [MASK] [MASK] [MASK] .
L' [MASK] [MASK] apparait et a pour but de rassembler les armes pour le combat .
[MASK] [MASK] fut le premier chef de l' organisation clandestine .
La poste d' [MASK] est attaquée par les membres de [MASK] [MASK] .
[MASK] [MASK] [MASK] prend la place de [MASK] [MASK] [MASK] en 1949 .
[MASK] [MASK] sera libéré de prison en 1958 et sera assigné à résidence surveillée en [MASK] .
Le [MASK] commence ses actions en 1954 , seulement deux ans avant que la [MASK] et le [MASK] obtiennent leur totale indépendance par la négociation ( [MASK] [MASK] et le [MASK] n' étaient pas des colonies mais des protectorats ) .
Cet assassinat marquera profondément les consciences des algériens et [MASK] [MASK] [MASK] .
Ce sera le véritable tournant de ce que l' on appellera [MASK] [MASK] [MASK] [MASK] .
La [MASK] joue un rôle clé dans l' organisation des convois d' armes à destination des maquis algériens .
Le soutien au [MASK] vient d' un autre pays arabe , l' [MASK] , où le lieutenant-colonel [MASK] prône une politique de panarabisme ( nassérisme ) , et , bénéficiant de l' appui soviétique , fournit des armes au [MASK] .
Les services secrets français ( [MASK] ) qui surveillent le raïs , parviennent à démontrer l' aide militaire fournie au [MASK] par [MASK] [MASK] .
Faisant valoir le soutien apporté par [MASK] [MASK] au [MASK] , [MASK] [MASK] planifie avec ses alliés israéliens et britanniques la [MASK] [MASK] [MASK] .
L' appel aux puissances étrangères , c' est enfin le recours aux instances de l' [MASK] , où le [MASK] parvient à faire inscrire " la question algérienne " à l' ordre du jour de la commission politique des [MASK] [MASK] .
Les [MASK] proposent une médiation , ce sont " les bons offices " , qui sont par ailleurs rejetés par [MASK] [MASK] .
La majorité des [MASK] vivaient dans les campagnes .
Les six chefs du [MASK] qui ont fait le déclenchement des hostilités le 1 er novembre 1954 sont [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] .
La [MASK] [MASK] [MASK] [MASK] [MASK] est émise par radio depuis [MASK] .
Dans la nuit du 1 er novembre 1954 , la caserne de la ville de [MASK] est attaquée par les moudjahidines .
Un caïd et deux enseignants français vont être abattus sur la route de [MASK] et [MASK] .
Des attentats sont enregistrés dans les trois districts [MASK] , [MASK] , [MASK] et le reste du pays .
Les opérations sont déclenchées dans les [MASK] .
L' [MASK] [MASK] [MASK] [MASK] ( [MASK] ) ne dispose alors que de 500 hommes qui seront , après quelques mois , plus de 15 000 à défier l' autorité française .
A noter aussi la mort de [MASK] [MASK] [MASK] , de [MASK] [MASK] , etc .
Des intellectuels français vont aider le [MASK] .
[MASK] [MASK] fut torturé et tué par les services français .
[MASK] [MASK] s' engage auprès de la résistance algérienne et a des contacts avec certains officiers de l' [MASK] ( [MASK] [MASK] [MASK] [MASK] ) et avec la direction politique du [MASK] , [MASK] [MASK] et [MASK] [MASK] en particulier .
[MASK] s' élève contre la torture , revendique la liberté pour les peuples de décider de leur sort , analyse la violence comme une gangrène , produit du colonialisme .
En 1960 , lors du procès des réseaux de soutien au [MASK] , il se déclare " porteur de valise " du [MASK] .
Plusieurs partis algériens adhèrent à la cause du [MASK] .
La [MASK] va être le théâtre d' affrontement entre les différents chefs .
Le [MASK] aussi va jouer un rôle important , notamment pour faire transiter les armes , organiser des réunions du [MASK] et héberger des troupes militaires algériennes .
Mais les éléments de l' [MASK] ( [MASK] [MASK] [MASK] [MASK] ) vont déjouer toute la stratégie militaire française .
Les villes ( population algérienne ) seront sous le contrôle de l' [MASK] [MASK] [MASK] algérienne .
La [MASK] [MASK] [MASK] fera la une de la presse internationale et interne .
Le conflit est porté jusqu' à [MASK] [MASK] .
Les protestations ont été organisées par le [MASK] .
Le colonel [MASK] [MASK] [MASK] fera un massacre dans les [MASK] en voulant intervenir pour unifier des zones des [MASK] et faire passer les armes en [MASK] .
L' [MASK] fut le lieu de passage des armes vers l' intérieur du pays .
Il franchira les [MASK] pour rejoindre la [MASK] .
Une vingtaine de chaouis vont être du voyage , mais à la fin , ils abandonneront les troupes du [MASK] [MASK] pour revenir aux [MASK] .
Les bombardements massifs , les tueries , les massacres , la torture , les viols , etc. , tous les actes de crime ( [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ) ont été employés dans cette guerre .
Plusieurs attentats seront organisés par l' [MASK] dans les villes et les villages , dans les zones interdites et dans les zones montagneuses des [MASK] .
Selon [MASK] [MASK] , [MASK] [MASK] s' opposera sévèrement aux militaires .
Plus tard , il sera tué au [MASK] , mais les souces de [MASK] diront qu' il aurait été tué lors d' un accrochage avec l' [MASK] française .
Les colonels [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] sont tués lors d' un accrochage avec les éléments de l' [MASK] [MASK] .
Le [MASK] appelle les éléments de son armée à tenir jusqu' au bout .
En 1959 , [MASK] [MASK] sort de prison , et est assigné à résidence surveillée en [MASK] .
Les [MASK] en [MASK] organisent des attentats et des manifestations en [MASK] en faveur du [MASK] .
1960 , la semaine des barricades à [MASK] fait 22 morts algériens et des centaines de prisonniers .
Les [MASK] sont tenus à se prononcer .
Certains généraux français se rebellent contre l' autorité du général [MASK] [MASK] ( le [MASK] [MASK] [MASK] ( 1958 ) et [MASK] [MASK] [MASK] ) .
Le général [MASK] [MASK] reprend en main le destin de [MASK] [MASK] .
Il annonce la tenue de référendum et invite le [MASK] à faire la paix des braves .
Au même moment , le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] est proclamé .
[MASK] [MASK] décline l' invitation française .
Le colonel [MASK] [MASK] est alors le chef de [MASK] [MASK] [MASK] [MASK] .
En 1960 , [MASK] [MASK] annonce le droit à l' autodétermination du peuple algérien .
Le côté français organise des pourparlers avec le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Le 17 octobre 1961 , lors de la nuit noire à [MASK] ,appelée aussi la bataille de [MASK] ( [MASK] [MASK] [MASK] [MASK] [MASK] ) , plusieurs [MASK] sont tués en métropole lors d' une manifestation du [MASK] .
Il y aura aussi des milliers d' arrestations au sein des [MASK] pendant cette nuit .
Ce fait survient à la suite de l' instauration du couvre-feu à [MASK] pour les [MASK] suite à l' assassinat de 21 policiers français par le [MASK] .
À [MASK] .
L' [MASK] [MASK] [MASK] ( [MASK] ) organise des attentats contre les [MASK] malgré l' accord de cesser le feu et les résultats du référendum pour l' indépendance pour sanctionner les gens qui étaient pour .
La plus grande bibliothèque d' [MASK] a été complètement détruite par l' [MASK] ( [MASK] [MASK] [MASK] .
De plus en plus d' historiens estiment que c' est suite à ces massacres et non pas à ceux de [MASK] ( mai 1945 ) que se radicalise l' insurrection en optant pour la guerre à outrance comme unique moyen de parvenir à se faire écouter des autorités coloniales françaises .
En 1956 , [MASK] [MASK] qui soupçonne le [MASK] [MASK] de soutenir le [MASK] en moyens et en armes s' engage dans la " [MASK] [MASK] [MASK] " ce qui gèle ses relations avec les pays arabes et l' [MASK] .
Les [MASK] font alors pression sur le premier ministre britannique [MASK] [MASK] en le menaçant de dévaluer la monnaie de son pays si ses troupes ne se retirent pas d' [MASK] , ce qu' elles feront ainsi que leurs alliés français ( la flotte du corps expéditionnaire est placée sous haut commandement britannique ) .
Une fois l' avion posé , les troupes françaises donnent l' assaut et interpellent [MASK] [MASK] et ses compagnons .
Cependant l' armée française , par la voix du [MASK] [MASK] , réfute cette version et nie toute implication dans l' affaire qui serait en réalité l' œuvre du cabinet [MASK] .
Les attentats se multiplient dans tout le territoire et la guérilla commence à se signaler dans les montagnes , les légionnaires et les parachutistes doivent intervenir régulièrement dans les [MASK] , en [MASK] et ailleurs .
L' année 1957 voit le déroulement de [MASK] [MASK] [MASK] [MASK] .
Sous les ordres du [MASK] [MASK] , la 10 e division parachutiste fait du maintien de l' ordre dans la capitale .
Le [MASK] perd la bataille et sa structure dans la capitale est détruite .
Dans le même temps , le [MASK] [MASK] organise la contre-guerrilla grâce à des techniques de quadrillage .
Le 7 janvier 1957 , le gouvernement général donne les pleins pouvoirs de police au général [MASK] [MASK] et ses 6000 parachutistes de la 10 e division parachutiste sur le grand [MASK] .
Leur mission est de démanteler les cellules du [MASK] à [MASK] .
Du 12 septembre 1957 au 7 février 1958 , près de vingt incidents de frontière sont signalés dans la région du village tunisien de [MASK] [MASK] [MASK] .
Suite au départ de [MASK] [MASK] qui laisse vacant le poste de chef du gouvernement , une grave crise ministérielle s' installe le 15 avril .
L' armée prend alors le pouvoir le 13 mai 1958 , à [MASK] .
L' image de la [MASK] dans le monde , et plus particulièrement en [MASK] occidentale en est fortement dégradée .
Dans les colonies françaises le référendum vise également à la création de la [MASK] [MASK] .
La proposition est rejetée par [MASK] [MASK] en novembre 1961 .
Pour [MASK] [MASK] , si la solution de la partition , dont on a souvent dressé des " images caricaturales " , a rencontré peu de faveur c' est pour la seule raison qu' elle a été farouchement rejétée par les extrémistes des deux camps .
Bientôt l' aviation de l' aéronavale pilonne les bâtiments occupés par l' [MASK] , tandis que les chars de l' armée française prennent position dans le quartier en état de siège .
le 2 mai 1962 , un attentat terroriste à la voiture piégée commis par des membres de l' [MASK] au port d' [MASK] fait 110 morts et 150 blessés , en majorité des dockers et des demandeurs d' emploi .
Au vaste élan de solidarité déclenché à partir des différents quartiers de la capitale par toute la population , européens et musulmans confondus , répondaient les tirs des ultras de l' [MASK] à partir d ' immeubles avoisinants , lesquels ont pris pour cibles les blessés , les ambulances et les personnes venues nombreuses participer aux opérations de secours , provoquant ainsi un véritable carnage .
Les pieds-noirs du [MASK] [MASK] [MASK] , plus loyalistes que ceux de l' [MASK] vis à vis de la mère patrie , rentreront presque tous en [MASK] où malgré les mauvaises conditions d' accueil , continueront à y vivre .
Entre 15000 et 150000 harkis auraient été massacrés par le [MASK] , et 30000 harkis se réfugient en [MASK] où ils sont parqués dans des camps d' internement sur ordre du gouvernement .
Les organisations communautaires font preuve d' une extrême modération et refusent de prendre politiquement position , car elles considèrent que ce n' est pas de leur ressort , pourtant -- malgré les nombreux attentats -- certains embrassent la cause du [MASK] et d' autres , s' engagent dans l' [MASK] .
Certains intellectuels juifs , comme [MASK] [MASK] , ont pris fait et cause pour les nationalistes algériens du [MASK] ( [MASK] [MASK] [MASK] [MASK] ) .
A l' inverse , des [MASK] sympathisent avec l' [MASK] , à [MASK] et à [MASK] essentiellement ( ils sont particulièrement actifs à [MASK] ) .
Les attentats du [MASK] touchent les synagogues et les rabbins .
Auparavant , le chanteur [MASK] [MASK] était abattu au pistolet en avril 1961 , à [MASK] .
Des [MASK] [MASK] [MASK] furent également victimes de l' [MASK] .
La [MASK] [MASK] ( [MASK] ) , unique organisme audiovisuel français , est composée de cinq chaînes de radio et deux chaînes de télévision .
Près de 14000 [MASK] suspectés d' être membres du [MASK] [MASK] [MASK] [MASK] ( [MASK] ) y furent internés .
De 1958 à 1961 les attentats du [MASK] font 191 victimes dans les rangs de la police ; soit 54 morts et 137 blessés .
Dans la nuit du 6 au 7 janvier , c' est le domicile de [MASK] [MASK] qui est l' objet d' un plasticage .
Le 24 janvier , on compte 21 explosions dans le département de la [MASK] , visant des personnalités ou des organisations supposées hostiles à l' idéologie de l' [MASK] .
Pour empêcher les populations d' aider le [MASK] , l' armée concentre aussi , selon le rapport [MASK] de 1959 , un million de civils ( dont la moitié d' enfants ) des zones rurales dans des " camps de regroupement " .
Après l' indépendance , l' [MASK] française refuse d' intervenir pour assurer la sécurité de ses supplétifs musulmans , comme le 5 juillet 1962 à [MASK] pour protéger les [MASK] , .
Sur ces près de 25000 militaires décédés , on dénombre environ 5000 " [MASK] musulmans " tués dont les deux tiers étaient des appelés .
Cependant , cette thèse , non dénuée d' arrières pensées idéologiques , tendant à faire penser à un ralliement massif de la population musulmane au [MASK] a été maintes fois battue en brêche .
Certaines sources affirment que le nombre de combattants de l' [MASK] n' aurait jamais franchi le chiffre de 15000 combattants tandis que d' autres sources avancent que le [MASK] n' a jamais réussi à mobiliser plus de 10000 combattants dans les maquis .
Le nombre de combattants de l' [MASK] tués demeure inconnu à ce jour .
Du côté algérien , le [MASK] compte en 1964 près de 1500000 victimes de cette guerre .
Les officiels algériens font valoir de leur côté que les données démographiques ne rendent aucunement compte des massacres dans les douars et les hameaux ( 8000 rasés de la carte durant la guerre ) et que ces chiffres correspondent peut être davantage au nombre de morts dans les villes où les [MASK] étaient en effet recensés et inscrits à l' état civil depuis 1881 .
Toutefois selon [MASK] [MASK] : " Il semble qu' un consensus rassemble peu à peu les historiens français à propos de cette question et qu' une évaluation entre 60 à 80.000 victimes soit retenue " .
Ils sont dus à des règlements de compte entre clans rivaux , des vengeances mais aussi au zèle des " marsiens " , ralliés [MASK] de la 25 e heure , voulant montrer leur patriotisme de façade .
Par ailleurs , la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] a été pratiquée aussi bien par l' armée française que par les insurgés algériens , à la différence notable que celle pratiquée par les services spéciaux de l' armée française et les légionnaires relevait de la méthode scientifique et avait clairement un caractère industriel tandis que celle pratiquée par le [MASK] demeurait artisanale et primitive .
Une loi spécifique est votée pour amnistier les responsables de l' [MASK] [MASK] .
Après les doubles lois de 1962 , les seuls actes pouvant être poursuivis sont ceux de torture commis par les forces françaises contre des membres de l' [MASK] .
Elle écarte ainsi la possibilité de poursuites contre le général [MASK] [MASK] .
Des associations de défense des droits de l' homme comme la [MASK] demandent un revirement .
Le général de [MASK] , sanctionné de soixante jours d' arrêts de forteresse pour avoir dénoncé la torture , n' a pas été réhabilité .
Le 2 juillet 1957 , le sénateur [MASK] [MASK] [MASK] prononce un discours à la chambre haute des [MASK] [MASK] [MASK] sur le thème de la [MASK] [MASK] [MASK] dans lequel il souligne que cette guerre atroce a cessé de représenter un problème interne purement français et que les [MASK] sont directement concernés par ce conflit lequel a " dépouillé jusqu' à l' os les forces continentales de l' [MASK] " .
Un projet de résolution est adressé à cet effet à l' administration [MASK] du président [MASK] [MASK] mais n' aboutira pas pour cause de procédures .
[MASK] [MASK] [MASK] [MASK] offre une occasion inespérée à l' [MASK] de reprendre son rôle en [MASK] sur fond d' anciennes rivalités avec [MASK] [MASK] .
Cependant , c' est la gauche italienne qui se montre la plus intransigeante contre ce qu' elle appelle l ' " impérialisme français " en [MASK] du nord .
De nombreux activistes italiens soutiendront les réseaux du [MASK] en [MASK] .
Cette guerre fut l' occasion pour l' armée française d' utiliser à grande échelle des hélicoptères dans le cadre de la lutte anti-insurrectionnelle et des armes dites de troisième dimension dans l' un des engagements militaires les plus intensifs de l' histoire militaire de [MASK] [MASK] .