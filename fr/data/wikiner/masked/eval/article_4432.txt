Il aurait élu domicile plusieurs années à [MASK] et serait décédé en 490 .
La commune de [MASK] est une commune rurale du sud-est du [MASK] assez vaste appartenant à l' arrière-pays de [MASK] .
Les paroissiens de [MASK] étaient soumis à la corvée au grand chemin .
1795 : [MASK] n' échappa pas aux évènements tragiques qui ternissent la période révolutionnaire .
1857 : La commune de [MASK] est amputée de 20 villages au profit de sa voisine [MASK] qui vient nouvellement d' être rattachée au [MASK] .