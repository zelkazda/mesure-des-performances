Il se compose à l' origine de [MASK] [MASK] [MASK] , [MASK] [MASK] ( [MASK] [MASK] ) et [MASK] [MASK] .
Le groupe a également eu une grande collaboration avec le musicien [MASK] [MASK] ( [MASK] ) mais ce dernier quitte le groupe en 1994 , après la sortie de l' album [MASK] .
[MASK] , fin des années 1980 .
En 1994 , l' album [MASK] voit le jour .
En 1998 , paraît [MASK] , leur troisième album .
En 2006 , sort leur best-of [MASK] , et le groupe annonce la sortie d' un nouvel album pour avril 2007 qui sera finalement repoussé au printemps 2009 .
Composé de 10 titres , [MASK] marque le retour du musicien [MASK] [MASK] ( alias [MASK] [MASK] ) qui aurait rejoint [MASK] [MASK] en 2006 .
De nombreux artistes ont gravité autour de [MASK] [MASK] , notamment :
[MASK] [MASK] est considéré comme l' un des précurseurs d' un style de musique nouveau à base de samples , le trip-hop .
Pour l' album [MASK] paru en février 2010 , on trouve des collaborations de :