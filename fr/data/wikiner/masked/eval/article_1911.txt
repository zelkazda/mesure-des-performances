[MASK] [MASK] est né à [MASK] en 1861 .
Son père était un méticuleux qui lui transmit l' amour du travail bien fait ; par ailleurs , le jeune [MASK] a un caractère hors du commun et un esprit rebelle , qui n' aime pas l' autorité .
Or à cette époque [MASK] est le berceau du monde .
Elles vont droit au cœur de [MASK] [MASK] .
Il voulait être à l' architecture ce que [MASK] [MASK] [MASK] avait été à la peinture , l' émancipateur des éternelles règles classiques .
À [MASK] , le futur architecte étudie les bâtiments classiques et leurs matériaux .
Lorsque son père meurt en 1880 , il se hâte de retourner en [MASK] .
[MASK] [MASK] y fait la connaissance de [MASK] [MASK] .
[MASK] [MASK] , élève exceptionnellement brillant , attire l' attention de beaucoup de ses professeurs .
On retiendra parmi eux surtout [MASK] [MASK] , l' architecte du roi .
Ensemble ils dessinent les [MASK] [MASK] [MASK] [MASK] , où sont à nouveau combinés les éléments que [MASK] admire le plus : les jeux de lumière , la verrerie et l' acier .
[MASK] reprendra le cabinet d' [MASK] [MASK] à sa mort , en 1895 .
C' est en 1892 qu' il réalise la [MASK] [MASK] , passionnante œuvre de transition , puis l' [MASK] [MASK] , le premier d' une longue série d' hôtels particuliers aussi inventifs que raffinés .
C' est en 1902 , qu' il fait la connaissance de [MASK] [MASK] , dont il fera par la suite son élève .
Il est concurrencé par [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] , deux autres architectes moteurs du renouveau de l' architecture moderne belge .
Il passe ensuite deux années ( 1916 -- 1918 ) aux [MASK] où il découvre de nouveaux matériaux de construction , tout en se détournant du mouvement art nouveau .
En 1932 , le roi [MASK] [MASK] [MASK] lui confère le titre de baron .
À [MASK] :
A [MASK] , trois réalisations de l' architecte sont désormais des musées :