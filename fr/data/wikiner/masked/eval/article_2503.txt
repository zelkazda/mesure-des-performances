Les premiers découpages climatiques en effet ont été établis selon l' inclinaison des rayons du [MASK] par rapport à l' horizon .
[MASK] en serait la limite orientale en [MASK] .
Certains auteurs parlent de climat hyperocéanique pour la bande de terre où l' influence de l' océan est journalière par la brume de mer ( [MASK] , [MASK] , [MASK] , [MASK] , etc .
L' influence de l' océan ne pouvant se faire sentir vue la direction générale des vents , c' est l' humidité due à l' évapotranspiration des terres ( forêts et marécages ) et des lacs qui fournit les précipitations ( [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] ) .
Les villes côtières des façades orientales subissent surtout ce climat malgré leur proximité des océans ( [MASK] [MASK] , [MASK] , [MASK] , [MASK] , [MASK] ) .
Là aussi certains auteurs parlent de climat hypercontinental pour les régions intérieures des grands continents où seule la terre influence le climat ( [MASK] , [MASK] ( [MASK] ) , [MASK] , [MASK] , [MASK] ( [MASK] ) .
On ne retrouve ce type de climat que dans l' hémisphère nord : partie centrale de tout le [MASK] , majeure partie de la [MASK] et nord-est de la [MASK] .
Les quelques villes connues sous ce climat sont : [MASK] [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] [MASK] , [MASK] [MASK] au [MASK] ( toutes des villes minières ) .
Il est caractéristique des côtes nord de l' [MASK] , de l' [MASK] et de l' [MASK] , ainsi que du [MASK] et de l' [MASK] .