[MASK] [MASK] est un roman de [MASK] [MASK] .
Cette série est considérée par [MASK] [MASK] comme étant son œuvre principale .
[MASK] [MASK] a d' ailleurs pratiqué une complète révision de ce volume , en 2003 , afin d' améliorer la cohérence avec les volumes ultérieurs et de rendre plus humain son personnage principal .
On y trouve quelques vestiges culturels de notre monde , comme la chanson [MASK] [MASK] , une comptine pour enfants , des extraits de la bible , ainsi que des vestiges technologiques .