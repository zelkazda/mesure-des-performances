Il est le fils de [MASK] [MASK] [MASK] et le petit-fils de [MASK] [MASK] auquel il succède en 1774 , et le frère aîné des futurs rois [MASK] [MASK] et [MASK] [MASK] .
[MASK] [MASK] tenta alors de passer outre l' opposition des privilégiés en présentant ses réformes devant une assemblée de notables ( 1787 ) puis devant les états généraux ( 1789 ) .
Les dernières années du règne de [MASK] [MASK] sont marquées par la [MASK] [MASK] qui , tout en reprenant certaines des réformes soutenues par le roi avant 1789 , les dépassent grandement .
Aujourd'hui , les historiens français en général ont une vue nuancée de [MASK] [MASK] , considéré comme un honnête homme mû par de bonnes intentions , mais qui n' était pas à la hauteur de la tâche herculéenne qu' aurait représentée une profonde réforme de la monarchie .
Il est élevé dans une religion stricte et est formé à des principes conservateurs sous la tutelle du duc de [MASK] [MASK] .
Ils estiment qu ' " il est très influencé par [MASK] , qui lui inspire une conception moderne de la monarchie détachée du droit divin .
Selon la tradition remontant à [MASK] [MASK] [MASK] , il est sacré en la [MASK] [MASK] [MASK] le 11 juin 1775 .
Le règne de [MASK] [MASK] est marqué par de nombreuses tentatives de réformes économiques et institutionnelles , dans le sillage de la réforme [MASK] [MASK] [MASK] [MASK] ( 1771 ) initiée sous [MASK] [MASK] .
[MASK] [MASK] rétablit les parlements .
[MASK] est appelé par [MASK] [MASK] comme contrôleur général des finances .
[MASK] s' attelle alors à un projet " révolutionnaire " de mise en place d' une pyramide d' assemblées élus à travers le royaume : municipalités de communes , d' arrondissement puis de province et une municipalité de royaume .
Une vaste coalition d' intérêts se forma contre [MASK] : détenteurs du monopole sur le grain , parlementaires appartenant à la noblesse de robe ( opposés au remplacement de la corvée par un impôt payé par les propriétaires , et qui déclare que " le peuple est taillable et corvéable à volonté et que c' est une partie de la constitution que le roi est dans l' impuissance de changer. " ) , patrons des jurandes , privilégiés , etc .
En mars 1776 , il déclare encore : " Je vois bien qu' il n' y a que [MASK] [MASK] et moi qui aimions le peuple. " .
Mais , après deux années de " résistance " , [MASK] [MASK] et ses ministres réformateurs finissent par céder aux pressions .
Puis [MASK] [MASK] doit se résoudre à renvoyer [MASK] , le 12 mai 1776 , et à revenir peu après sur ses réformes ( rétablissement des corvées et des maîtrises ) .
[MASK] et [MASK] [MASK] remettent sur le métier les réformes les plus essentielles du royaume .
L' administration de [MASK] est ainsi marquée par l' affranchissement des derniers serfs du domaine royal par une ordonnance du 8 août 1779 .
Néanmoins l' ordonnance ne fut guère appliquée , et le servage a persisté localement jusqu' à la [MASK] qui l' abolit avec les privilèges lors de la célèbre nuit du 4 août 1789 .
ajoute l' historien [MASK] [MASK] .
[MASK] [MASK] et [MASK] ne purent tenir longtemps devant l' opposition des privilégiés .
[MASK] remit sa démission au roi , qui l' accepta le 21 mai 1781 .
Comme dans le plan de [MASK] , [MASK] prévoyait une pyramide d' assemblées locales ( assemblées paroissiales et municipales , assemblées de districts ) élues par les contribuables .
[MASK] [MASK] aurait lancé à [MASK] : " C' est du [MASK] tout pur que vous me donnez là ! " , mais le plan serait plus proche de celui de [MASK] .
L' un des principaux rédacteurs du projet est le physiocrate [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , ancien collaborateur de [MASK] .
En janvier 1784 , [MASK] [MASK] abolit le péage personnel qui pesait sur [MASK] [MASK] d' [MASK] .
Devant la protestation du parlement , [MASK] [MASK] l' exile à [MASK] en août 1787 et ne le rappelle que lorsque les parlementaires acceptent d' étendre l' impôt direct à toutes les formes de revenus .
[MASK] accepte leur tenue et quitte le pouvoir le 25 août 1788 .
Une mesure qui finira par se retourner contre les privilégiés , notamment parce que [MASK] se prononce en faveur du doublement de la participation du tiers état .
Face au vide créé par l' interdiction de la [MASK] [MASK] [MASK] ( les [MASK] ) en 1773 , il choisit les [MASK] pour les remplacer dans les missions en territoire ottoman .
[MASK] [MASK] a joué un rôle important dans la modernisation de la [MASK] [MASK] .
[MASK] [MASK] a été longtemps caricaturé comme un roi un peu simplet , manipulé par ses conseillers , peu au fait des questions de pouvoir , avec des marottes comme la serrurerie et une passion envahissante pour la chasse .
[MASK] [MASK] était un prince studieux et érudit .
Sur le plan scientifique , il mandata [MASK] [MASK] [MASK] [MASK] pour effectuer le tour du monde et le cartographier .
[MASK] [MASK] , aurait alors demandé des nouvelles de cette entreprise jusque sur l' échafaud .
Depuis [MASK] [MASK] , la noblesse était en grande partie " domestiquée " par le système de cour .
[MASK] [MASK] hérita de ce système .
Après la [MASK] [MASK] [MASK] [MASK] , le roi se rend de son plein gré à [MASK] , le 17 juillet , où il est accueilli par le maire de la nouvelle municipalité , [MASK] .
Le 14 septembre 1791 , [MASK] [MASK] jure fidélité à ladite constitution .
Une manifestation l' empêche physiquement de se rendre au [MASK] [MASK] [MASK] .
Les révolutionnaires s' opposent en effet à ce qu' il fasse ses [MASK] avec un prêtre réfractaire à la constitution civile du clergé .
D' une part , [MASK] [MASK] y stigmatise les [MASK] et leur emprise croissante sur la société française .
Ce document historique majeur , traditionnellement appelé " le testament politique de [MASK] [MASK] " , a été redécouvert en mai 2009 .
Les [MASK] décident de suivre les [MASK] , ce qui crée une rupture en leur sein .
Une partie de leurs membres créèrent le [MASK] [MASK] [MASK] .
[MASK] [MASK] perd alors tous ses titres , les autorités révolutionnaires le désignent sous le nom de [MASK] [MASK] ( en références à [MASK] [MASK] , dont le surnom est considéré , de manière erronée , comme un nom de famille ) .
Puis , avec une majorité étroite , condamné à mort au manège du château des [MASK] , à la suite de la " séance permanente du mercredi 16 et du jeudi 17 janvier 1793 " et du scrutin rectificatif du 18 .
[MASK] [MASK] fut guillotiné le lundi 21 janvier 1793 à [MASK] , [MASK] [MASK] [MASK] [MASK] ( actuelle [MASK] [MASK] [MASK] [MASK] ) .
Les 18 et 19 janvier 1815 , [MASK] [MASK] fit exhumer ses restes et ceux de [MASK] pour les faire inhumer à la [MASK] [MASK] le 21 janvier .
En outre , il fit édifier en leur mémoire la [MASK] [MASK] à l' emplacement du [MASK] [MASK] [MASK] [MASK] .
Le 3 mai 1826 [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] pose la première pierre du monument à la mémoire de [MASK] [MASK] .
Son socle servira de base à l' [MASK] [MASK] [MASK] dressé en 1836 .
Pour anecdote , quelques jours avant son exécution , il s' intéressait encore au sort de [MASK] [MASK] et de son expédition .
En 1900 , le leader socialiste [MASK] [MASK] , juge [MASK] [MASK] " indécis et pesant " , " incertain et contradictoire " .
[MASK] [MASK] croit dans le double jeu du roi .
Le [MASK] [MASK] [MASK] [MASK] s' appuie principalement sur l' accusation de trahison envers la patrie .
Leur ouvrage de réhabilitation de [MASK] [MASK] affirme que l ' " armoire de fer " contenant la correspondance secrète du roi avec les princes étrangers aurait été fabriquée de toute pièce par le révolutionnaire [MASK] pour accuser le roi .
L' historien [MASK] [MASK] a vivement critiqué les méthodes et conclusions de cet ouvrage , estimant pour sa part que la condamnation de [MASK] [MASK] était inscrite d' office dans son procès , car le souverain déchu était traité comme un " ennemi à abattre " par les révolutionnaires .