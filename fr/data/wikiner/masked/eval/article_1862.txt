Les années 1880 ont marqué la naissance du syndicalisme en [MASK] .
Lors du [MASK] [MASK] [MASK] la conception corporatiste de l' organisation du travail est mise en avant et divise le monde syndical .
La promulgation en 1941 de la [MASK] [MASK] [MASK] organise la dissolution des organisations syndicales existantes et la création de syndicats uniques par corporation .
En 1948 , un courant sécessionniste de la [MASK] , réformiste et opposé à la domination du [MASK] [MASK] [MASK] sur la [MASK] , crée la [MASK] .
En 1992 , le mouvement " autonome " , comportant principalement des syndicats qui , en 1947 , avaient refusé de choisir entre la [MASK] et [MASK] , s' organise dans l' [MASK] .
Il ne faut donc pas confondre syndicat et organisation syndicale ( par exemple la [MASK] , ou la [MASK] ) .
Les résultats obtenus aux élections professionnelles ( 63,8 % en moyenne en [MASK] lorsqu' elles sont organisées ) sont un baromètre de représentativité institué par la loi du 20 août 2008 .
Alors que le taux de syndicalisation dans le secteur privé est à peine supérieur à 5 % ( alors que les autres pays européens sont aux alentours de 30 , voire 50 % ) , [MASK] [MASK] a un paysage syndical divisé , constitué de cinq confédérations qui bénéficiaient d' une présomption irréfragable de représentativité jusqu' à la loi du 20 août 2008 ainsi que les trois autres principales organisations non représentatives de droit .
En [MASK] , toutes les avancées sociales bénéficient à tous .
Selon un sondage [MASK] [MASK] de décembre 2005 , les causes de non-syndicalisation sont :
On parle au [MASK] de " trade unions " ou " labour unions " pour désigner les syndicats .