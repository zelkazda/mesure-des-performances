Sur les communes d' [MASK] et de [MASK] en [MASK] , le site de [MASK] [MASK] n' est plus aujourd'hui qu' une colline couverte de forêt .
Ici se trouvait , avant 1645 , la citadelle de [MASK] [MASK] .
Elle est de nouveau assiégée dès décembre 1642 , jusqu' à la mort de [MASK] [MASK] en mai 1643 .
Après une existence de trois cent quatre-vingt-sept ans , [MASK] [MASK] qui , de 1634 à 1645 , avait résisté à quatre reprises à l' armée française , n' était plus qu' une vaste ruine .
Aussi après le découpage de la [MASK] en départements , on fit faire à la frontière de la [MASK] un petit décrochage pour y inclure le site de [MASK] [MASK] .
Les ruines de [MASK] [MASK] ont été classées monument historique en 1923 et d' importants travaux de restauration ont été effectués .