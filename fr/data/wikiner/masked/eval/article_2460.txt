[MASK] [MASK] fut l' un des premiers à parcourir la région .
Le [MASK] [MASK] [MASK] , société secrète sudiste , naquit dans le [MASK] dès 1865 .
Son centre géographique est situé à quelques kilomètres à l' est de [MASK] et son point le plus bas est situé à 54 m au-dessus du niveau de la mer .
Il est parcouru par le [MASK] .
L' [MASK] [MASK] traverse presque la totalité du territoire sur un axe est-ouest .
Les autres autoroutes importantes est-ouest sont l' [MASK] et l' [MASK] .
Les autoroutes nord-sud sont les [MASK] , [MASK] , [MASK] et [MASK] .
Selon le [MASK] [MASK] [MASK] [MASK] [MASK] , en 2005 , le [MASK] a une population estimée à 5962959 habitants , ce qui représente une augmentation de 1,2 % , soit 69661 personnes , par rapport à l' année précédente ; et de 4,8 % , soit 273697 personnes , par rapport à 2000 .
La population d' origine afro-américaine est surtout concentrée dans les régions de l' ouest et du centre , spécialement dans les villes de [MASK] , [MASK] , [MASK] , [MASK] , et [MASK] .
Lors des élections présidentielles , le [MASK] est souvent très partagé entre républicains et démocrates et opte dans des marges réduites pour l' un ou l' autre candidat .
Le dernier démocrate à avoir emporté le [MASK] est [MASK] [MASK] en 1996 .
Le vice-président [MASK] [MASK] tout comme son père furent sénateur du [MASK] .
Le [MASK] était réputé grâce à [MASK] , la capitale du rock et [MASK] , capitale de la country .
De 1975 à 1982 toutes les plus grandes stars du rock ont enregistré leur album à [MASK] .
Mais le [MASK] a aussi d' énormes ressources minières ( charbon , fer , etc . )