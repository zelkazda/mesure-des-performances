[MASK] [MASK] s' engage dans la [MASK] et rejoint [MASK] en janvier 1943 .
Après la guerre il devient un homme de lettres à succès avec [MASK] [MASK] [MASK] et surtout la saga des [MASK] [MASK] , roman historique en sept tomes publiés entre 1955 et 1977 et que l' adaptation télévisée fera connaître à un très large public .
Il est élu à l' [MASK] [MASK] en 1966 à 48 ans et en devient le secrétaire perpétuel de 1985 à 1999 .
Gaulliste et engagé dans l' action politique , [MASK] [MASK] a été [MASK] [MASK] [MASK] [MASK] entre 1973 et 1974 .
Il fait ses études secondaires au [MASK] [MASK] de [MASK] .
En septembre 1939 , appelé par les obligations militaires , il publie dans [MASK] de [MASK] [MASK] , un article intitulé " J' ai vingt ans et je pars " .
Élève officier de cavalerie à l' [MASK] [MASK] [MASK] en 1940 , il participe lors de la [MASK] [MASK] [MASK] aux combats des [MASK] [MASK] [MASK] sur la [MASK] .
Il s' engage dans la [MASK] .
Il écrit alors avec [MASK] en mai 1943 [MASK] [MASK] [MASK] [MASK] qui , sur une musique composée par [MASK] [MASK] , devient l' hymne aux mouvements de la [MASK] durant la [MASK] [MASK] [MASK] .
Puis il accède à la célébrité avec le succès de sa saga historique littéraire , [MASK] [MASK] [MASK] publiée à partir de 1955 , et adaptée en 1973 à la télévision .
[MASK] [MASK] n' a jamais caché que sa série " [MASK] [MASK] [MASK] " avait été le résultat d' un travail d' atelier .
[MASK] [MASK] est nommé en 1973 [MASK] [MASK] [MASK] [MASK] par [MASK] [MASK] .
Il n' est pas reconduit dans le troisième gouvernement de [MASK] [MASK] en mars 1974 .
Il entre au comité central de la nouvelle formation gaulliste , le [MASK] [MASK] [MASK] [MASK] , et siège à son conseil politique en 1979 et 1980 .
Il est élu député [MASK] de [MASK] de mars 1978 à mai 1981 .
Il continue à prendre la parole sur la politique française , prenant parti pour [MASK] [MASK] à la présidentielle de 2007 , fustigeant le candidat [MASK] [MASK] , prenant parti en faveur de la lecture de la lettre de [MASK] [MASK] dans les écoles , ou encore comme témoin de moralité lors du procès de [MASK] [MASK] en 1998 , considérant que le procès avait été fait en 1945 et qu' il ne faut pas " juger avec nos yeux instruits d' aujourd'hui mais avec yeux aveugles d' hier " .
Grâce aux [MASK] [MASK] qui furent traduits en de nombreuses langues et à la série télévisée vendue à des chaînes étrangères , [MASK] [MASK] acquiert une notoriété internationale importante .
Ce dernier déclare à sa mort qu' il " salue la mémoire d' un ami fidèle de la [MASK] " .
Le président [MASK] [MASK] quant à lui a regretté la disparition " d' un éminent acteur de la culture mondiale. "