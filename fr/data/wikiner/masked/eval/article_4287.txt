La commune longe l' [MASK] vers le sud .
[MASK] possède plus de 40 km de sentiers piétonniers .
L' étendue de la commune permet aux promeneurs de découvrir un patrimoine naturel très varié : rives de l' [MASK] que l' on atteint par de petits sentiers aménagés en sous-bois , faune et flore des bois communaux , paysages de bocages dans la campagne .
La commune a signé la charte [MASK] [MASK] [MASK] [MASK] le 11 décembre 2008 .
Une sage jeune fille , poursuivie par un galant très assidu , préféra se jeter dans l' [MASK] plutôt que de perdre son honneur .