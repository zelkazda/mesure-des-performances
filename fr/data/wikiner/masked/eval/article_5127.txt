Il fut l' un des dirigeants de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] , parti socialiste ) , et président du conseil , c' est-à-dire chef du gouvernement français , à trois reprises : en 1936 -- 1937 , en 1938 et en 1946 .
Il a refusé d' aider militairement les républicains espagnols ( pendant la [MASK] [MASK] [MASK] [MASK] ) , ce qui a entraîné le retrait des communistes du [MASK] [MASK] .
En juin 1937 , [MASK] [MASK] démissionne du poste de président du conseil .
Élève du [MASK] [MASK] , il y rencontre l' écrivain [MASK] [MASK] et publie ses premiers poèmes à l' âge de 17 ans dans un journal créé avec celui-ci .
Il est admis en 1890 à l' [MASK] [MASK] [MASK] , où il est influencé par [MASK] [MASK] .
Il hésite alors entre le droit et la littérature , et entreprend finalement les deux à l' [MASK] [MASK] [MASK] , en visant une carrière de fonctionnaire .
[MASK] [MASK] est reçu à sa deuxième présentation du concours du [MASK] [MASK] [MASK] à l' âge de 23 ans et nommé auditeur au [MASK] [MASK] [MASK] en décembre 1895 .
Pour une grande partie de la population juive , [MASK] était un homme politique comme les autres .
[MASK] souffrit très tôt de l' antisémitisme .
Le metteur en scène et professeur [MASK] [MASK] estime , pour sa part , qu' il est " le critique le plus intelligent de son époque " .
Il côtoie les auteurs de l' époque ( [MASK] [MASK] [MASK] par exemple ) et fait partie de la vie parisienne ( se battant même en duel pour une critique ) .
[MASK] [MASK] se lance réellement en politique durant l' [MASK] [MASK] de 1894 à 1906 , y jouant un rôle actif dans la coordination des " intellectuels " , ce qui provoqua sa rupture avec [MASK] [MASK] , qu' il considérait jusqu' alors comme son maître en littérature et surtout , sa rencontre avec [MASK] [MASK] en 1897 .
Dès lors , son action militante à la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) commence vraiment .
Si [MASK] laisse à [MASK] un grand héritage idéologique , ce n' est pas tant la mort de celui-ci que le début de la [MASK] [MASK] [MASK] qui pousse [MASK] [MASK] à entrer en politique .
Et même si le parti socialiste français était divisé sur la question de la guerre , [MASK] restait convaincu qu' il s' agissait de la bonne option .
Ce n' est qu' un peu avant les élections législatives de 1919 que [MASK] [MASK] accède au cercle dirigeant de [MASK] [MASK] .
Lors du congrès d' avril 1919 , [MASK] tente de préserver les acquis démocratiques et républicains de [MASK] et d' insuffler aux socialistes une approche progressive de la révolution dans les domaines sociaux , économiques et politiques .
[MASK] [MASK] connut des fortunes électorales variables durant les années vingt , mais ne pâtit pas de la division avec le parti communiste .
Au début des années 1930 , la [MASK] entra dans la crise économique .
[MASK] préconisait aussi d' insister sur les intérêts communs des classes moyennes avec les classes ouvrières .
Début juin 1934 , des contacts se nouèrent entre la [MASK] et le [MASK] , les négociations avancèrent vite grâce à des concessions mutuelles ( le [MASK] étant poussé dans certains cas par l' [MASK] ) .
C' est le dirigeant communiste [MASK] [MASK] qui , par des articles dans [MASK] [MASK] , appela à la formation d' un large " [MASK] [MASK] " , après que l' ancien leader communiste et futur collaborationniste [MASK] [MASK] l' eut lui aussi proposé deux années auparavant .
Ce gouvernement de [MASK] [MASK] fut aussi le premier à comprendre des femmes , alors qu' à cette époque elles n' avaient pas encore en [MASK] le droit de voter .
Les raisons de la victoire du [MASK] [MASK] sont multiples : crise économique , montée d' [MASK] [MASK] , scandales financiers , instabilité du gouvernement de la législature de 1932 , existence des ligues d' extrême droite , armées et de plus en plus nombreuses , émeutes du 6 février 1934 .
Les résultats donnant le [MASK] [MASK] vainqueur aux élections donnèrent beaucoup d' espoir au sein du prolétariat , qui déclencha une grève générale spontanée .
Le socialiste [MASK] [MASK] exhorta [MASK] à prendre le pouvoir immédiatement , en se basant sur cette mobilisation populaire et sans attendre la passation de pouvoir officielle .
Mais [MASK] préféra attendre .
Il y eut cependant une aide clandestine , organisée par [MASK] [MASK] et [MASK] [MASK] .
On peut noter que les rapports avec l' [MASK] furent beaucoup plus difficiles qu' avec l' [MASK] .
Les calomnies de l' extrême droite visaient toutes les personnalités du [MASK] [MASK] .
L' arrivée de [MASK] au pouvoir déclencha aussi une vague d' antisémitisme d' une très grande ampleur .
[MASK] [MASK] écrit dans [MASK] [MASK] [MASK] du 15 mai 1936 : " C' est en tant que juif qu' il faut voir , concevoir , entendre , combattre et abattre le [MASK] .
Ce dernier verbe paraîtra un peu fort de café : je me hâte d' ajouter qu' il ne faudra abattre physiquement [MASK] que le jour où sa politique nous aura amené la guerre impie qu' il rêve contre nos compagnons d' armes italiens .
Et [MASK] [MASK] dans [MASK] du 7 avril 1938 : " Il [ [MASK] ] incarne tout ce qui nous révulse le sang et nous donne la chair de poule .
La presse allant dans le sens du [MASK] [MASK] était largement plus faible que la presse d' opposition , et l' une comme l' autre contribuèrent à l' échec du gouvernement de [MASK] [MASK] .
[MASK] remit sa démission en juin 1937 et fut remplacé par un gouvernement radical .
La défaite définitive du [MASK] [MASK] eut lieu en septembre-décembre 1938 avec l' arrivée au pouvoir d' [MASK] [MASK] .
Le bilan du [MASK] [MASK] est mitigé , mais cette brève expérience permit tout de même un nombre important d' avancées dans de nombreux domaines , en particulier dans le domaine social : congés payés [ réf. souhaitée ] , semaine de quarante heures , établissement des conventions collectives , prolongement de la scolarité à 14 ans , etc .
Lors de la signature des [MASK] [MASK] [MASK] ( fin septembre 1938 ) , [MASK] [MASK] n' exerçait aucune fonction gouvernementale .
Néanmoins , peu de temps après , il se ravise et plus rien n' entravera ses positions de fermeté face aux fascismes , et dans la poursuite de son combat pour le réarmement de [MASK] [MASK] , au prix de la division de son propre parti .
[MASK] [MASK] se mit aussi à dos une partie des socialistes en particulier [MASK] [MASK] à cause de ses positions non pacifistes .
Lors du vote des pleins pouvoirs au [MASK] [MASK] ( 10 juillet 1940 ) , [MASK] [MASK] fit partie des quatre-vingts parlementaires de l' [MASK] [MASK] qui votèrent contre , mais il ne prit pas la parole .
À ce moment , une partie des dirigeants de [MASK] [MASK] était déjà en [MASK] .
La [MASK] [MASK] de justice fut instituée par [MASK] en juillet 1940 pour rechercher les responsables politiques de la guerre .
[MASK] s' en indigna .
Ces lieux de détention devinrent le lieu de ralliement des socialistes résistants , comme [MASK] [MASK] , [MASK] [MASK] son avocat ou [MASK] [MASK] .
Il fut traduit devant la [MASK] [MASK] [MASK] mais sa défense , et celle de [MASK] , fut si efficace et si courageuse que le procès fut suspendu sine die .
[MASK] démontra avec brio que le réarmement ne fut jamais aussi intense que sous le [MASK] [MASK] , au contraire des gouvernements l' ayant précédé , dont un avait eu pour ministre de la guerre le [MASK] [MASK] .
Ses conditions de détention étaient plus acceptables que celles du camp , et il put s' y remarier avec [MASK] .
Le 3 avril 1945 , [MASK] [MASK] et sa femme sont emmenés dans un convoi de prisonniers et au bout d' un mois de pérégrinations , ils se retrouvèrent dans un hôtel du [MASK] italien , où , le 4 mai , ils aperçurent les premiers soldats américains .
[MASK] [MASK] dirigea , du 16 décembre 1946 au 16 janvier 1947 le dernier [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] avant l' instauration de la [MASK] [MASK] .
Il se retira ensuite dans sa [MASK] [MASK] [MASK] près de [MASK] où il mourut le 30 mars 1950 d' un infarctus à l' âge de 77 ans .
Il dénonça notamment le danger que constituait selon lui le [MASK] pour le régime parlementaire .