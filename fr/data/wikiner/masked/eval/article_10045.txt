L ' [MASK] [MASK] [MASK] [MASK] ( de 1804 à 1806 ) est la première expédition américaine à traverser les [MASK] à terre jusqu' à la côte [MASK] .
[MASK] [MASK] et [MASK] [MASK] sont les chefs de l' expédition et donneront leur nom à celle-ci .
L' expédition atteint l' [MASK] [MASK] en 1805 .
Le succès de l' expédition est dû en grande partie à la présence de [MASK] , guide et interprète shoshone .
La [MASK] [MASK] [MASK] [MASK] par la [MASK] aux [MASK] en 1803 provoque un intérêt grandissant pour l' expansion du pays vers l' [MASK] [MASK] .
Quelques semaines après , le président en exercice , [MASK] [MASK] , obtient du congrès une subvention de 2500 dollars afin d' envoyer des officiers explorer l' ouest " sauvage " .
La mission assignée à [MASK] par [MASK] consistait à trouver des fleuves navigables en vue de développer le commerce vers l' [MASK] [MASK] .
Des témoignages de trappeurs avaient déjà indiqué que le [MASK] prenait sa source dans les [MASK] [MASK] , et qu' il existait un autre fleuve plus à l' ouest qui se jetait dans le [MASK] .
En 1792 , [MASK] [MASK] , un marchand américain , avait reconnu l' estuaire d' un fleuve qu' il baptisa " [MASK] " .
[MASK] choisit le capitaine [MASK] [MASK] pour mener l' expédition .
[MASK] choisit quant à lui [MASK] [MASK] comme partenaire dans cette aventure .
Bien que [MASK] n' ait été , au regard de l' armée américaine , que lieutenant en second , [MASK] le considéra toujours comme son égal et partagea avec lui le commandement de l' expédition .
Dès 1803 , [MASK] étudia à [MASK] les informations consignées dans les journaux des trappeurs et des marchands qui avaient visité l' ouest .
Trois embarcations partent de [MASK] [MASK] : la principale mesure environ 16 mètres .
L' expédition continua son chemin en passant par le site de la future [MASK] [MASK] ( [MASK] ) et d' [MASK] , traversa les [MASK] [MASK] et descendit la [MASK] [MASK] , la [MASK] , et la [MASK] .
Elle passa par [MASK] [MASK] et l' emplacement de [MASK] ( [MASK] ) .
Elle atteint l' [MASK] [MASK] en décembre 1805 .
[MASK] put observer le [MASK] [MASK] .
Les explorateurs se mirent en route le 23 mars 1806 pour le voyage retour : l' expédition remonta la [MASK] au prix d' efforts importants .
Le 11 août , lors d' une partie de chasse , [MASK] fut blessé à la cuisse .
Les deux groupes se retrouvèrent pour descendre le [MASK] .
Ils arrivèrent le 23 septembre 1806 à [MASK] où ils furent accueillis par des centaines de personnes .