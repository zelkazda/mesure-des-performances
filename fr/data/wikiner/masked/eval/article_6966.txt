Son sobriquet , [MASK] [MASK] [MASK] vient de ce que son père n' a pas de terres à lui donner jusqu' à la mort de ses frères aînés .
Membre de la dynastie angevine des [MASK] , il était le cinquième et dernier fils du roi [MASK] [MASK] [MASK] [MASK] et d' [MASK] [MASK] [MASK] .
Le roi [MASK] et la reine [MASK] n' étaient pas ensemble durant les neuf mois qui précèdent décembre 1167 , mais bien en mars 1166 .
Par ailleurs , [MASK] vit le jour à [MASK] vers [MASK] , mais [MASK] et [MASK] passaient [MASK] en [MASK] , en 1167 .
Le chanoine de [MASK] , écrivant un siècle après , affirma que [MASK] fut nommé en référence à l' apôtre [MASK] , dont la fête est célébrée le 27 décembre .
[MASK] [MASK] [MASK] considère également que [MASK] naquit en 1166 et que c' est la reine [MASK] qui choisit son prénom .
Alors que [MASK] était le fils préféré d' [MASK] , étant le plus jeune , il ne pouvait espérer une part importante de l' héritage .
[MASK] fit emprisonner [MASK] en 1173 , alors que [MASK] était enfant .
Sa mère confia une partie de son éducation à l' [MASK] [MASK] [MASK] .
En 1185 , son père l' envoya gouverner la [MASK] [MASK] [MASK] , dont il fut contraint de partir après seulement huit mois .
Toutefois , il garda la faveur de son père , une faveur qui causa la révolte de son frère aîné [MASK] en 1188 .
Finalement , [MASK] abandonna le roi pour soutenir le révolté .
[MASK] [MASK] meurt peu après cette trahison , en juillet 1189 .
Peu après son accession au trône , en 1199 , il se débarrassa de son épouse en faisant annuler ce mariage par le pape [MASK] [MASK] pour cause de consanguinité ( ils étaient cousins au second degré , tous deux descendants d' [MASK] [MASK] [MASK] [MASK] [MASK] ) .
C' est l' un des événements qui amenèrent plus tard des écrivains à le présenter comme un traître dans leurs récits de la légende d' [MASK] [MASK] [MASK] , devenu [MASK] [MASK] [MASK] , qui se situe à l' origine un siècle avant l' époque de [MASK] .
[MASK] promit à la cité de lui accorder le droit de se gouverner elle-même en contrepartie d' une reconnaissance comme héritier présomptif de [MASK] .
Lors de son retour de la croisade , [MASK] fut capturé par [MASK] [MASK] , duc d' [MASK] et emprisonné par l' empereur [MASK] [MASK] .
À son retour en [MASK] , en 1194 , [MASK] pardonna à [MASK] et le désigna comme son héritier .
Plusieurs historiens avancent que [MASK] n' avait pas tenté de renverser [MASK] , mais , au contraire , fait de son mieux pour améliorer la situation d' un pays ruiné par les taxes excessives levées par [MASK] pour financer la croisade .
Plusieurs regardaient son jeune neveu , [MASK] [MASK] [MASK] [MASK] , fils de son frère [MASK] , comme l' héritier légitime .
Seulement , [MASK] était jeune ( il n' avait que douze ans en 1199 , à la mort de son royal oncle ) et semblait ne pas être à la hauteur aux yeux de [MASK] [MASK] [MASK] [MASK] , de [MASK] et de sa grand-mère [MASK] [MASK] [MASK] .
[MASK] entra donc en lice contre son oncle et revendiqua le trône , avec le soutien du roi [MASK] .
Il fit de [MASK] le nouveau siège de la marine .
Durant le règne de [MASK] , des améliorations sensibles furent réalisées dans la conception des bateaux , notamment l' addition de voiles et les châteaux amovibles à l' avant .
[MASK] est parfois crédité de la fondation de la [MASK] [MASK] moderne .
Dans le cadre de la guerre , [MASK] tenta de s' emparer de sa grand-mère , [MASK] [MASK] [MASK] , qui , n' admettant pas que l' on dispose de ses domaines , avait pris parti pour son fils et venait de quitter [MASK] pour [MASK] .
Nul n' est certain de ce qu' il est advenu à [MASK] , par la suite .
Cependant , [MASK] [MASK] [MASK] , l' officier commandant la forteresse de [MASK] , affirma avoir remis [MASK] , autour de [MASK] 1203 , aux agents du roi , envoyés pour le castrer ; [MASK] serait mort suite à l' opération .
Outre [MASK] , [MASK] captura également sa nièce , [MASK] [MASK] [MASK] .
[MASK] demeura prisonnière le reste de sa vie ( qui s' acheva en 1241 ) ; par ce geste , [MASK] acquit une réputation d' homme impitoyable .
L ' " [MASK] [MASK] " qu' avait créé son père se trouvait très sérieusement amputé .
En échange , les régions de [MASK] , [MASK] et [MASK] s' engagèrent à le soutenir contre les rois capétiens .
Cette mesure donnait aux seuls marchands gascons libre accès au marché du vin en [MASK] dans un premier temps .
Les années suivantes , [MASK] octroya les mêmes privilèges à [MASK] [MASK] et au [MASK] .
Une seconde élection , imposée par [MASK] , aboutit à une autre élection .
Quand les deux candidats comparurent à [MASK] , le pape [MASK] [MASK] désavoua l' un et l' autre et consacra un théologien anglais , [MASK] [MASK] , malgré les objections des ambassadeurs de [MASK] .
[MASK] passait ainsi outre les droits du roi à choisir ses propres vassaux .
Malgré son poids , l' interdit n' aboutit à aucune rébellion contre [MASK] .
En novembre 1209 , [MASK] fut lui-même excommunié et , en février 1213 , [MASK] menaça de prendre des mesures plus sévères si [MASK] ne se soumettait pas .
Par cette charte , qui est l' une des bases de la démocratie britannique , la royauté en [MASK] n' est dorénavant plus absolue .
[MASK] traversa le pays pour s' opposer aux forces rebelles , notamment lors du siège de deux mois contre le château de [MASK] .
Cette perte causa à [MASK] une grande tristesse , qui affecta sa santé et l' état de son esprit .
Atteint de dysenterie et se déplaçant par petites étapes , il séjourna une nuit au château de [MASK] , avant de mourir , dans la nuit du 18 au 19 octobre 1216 , au [MASK] [MASK] [MASK] , dans le [MASK] ( à cette époque , dans le [MASK] , maintenant sur la frontière du [MASK] avec ce comté ) .
Il fut inhumé dans la cathédrale de [MASK] , dans la ville de [MASK] .
Il est le premier roi de la dynastie [MASK] à être enterré en [MASK] .
Il laissait un fils âge de neuf ans , qui lui succéda sous le nom d' [MASK] [MASK] [MASK] [MASK] .
Peu après son accession au trône , en 1199 , il fit annuler ce mariage par le pape [MASK] [MASK] pour cause de consanguinité .
[MASK] l' avait enlevée , alors qu' elle était fiancée à [MASK] [MASK] [MASK] [MASK] .
Les chroniqueurs de son temps accordent à [MASK] un goût très sensuel , ajoutant divers embellissements à leur récit .
[MASK] [MASK] l' accuse d' avoir été envieux de nombre de ses barons et d' avoir séduit leurs filles et sœurs les plus attirantes .
[MASK] eut plusieurs enfants illégitimes :
D' une ou plusieurs maîtresse ( s ) inconnue ( s ) , [MASK] eut également plusieurs autres enfants :
Plusieurs historiens ont affirmé , cependant , que le gouvernement de [MASK] ne fut ni meilleur ni pire que celui des rois [MASK] [MASK] [MASK] ou [MASK] [MASK] , ajoutant qu' il passa ( au contraire de [MASK] ) la majorité de son règne en [MASK] .
Toutefois , sa réputation demeure si entachée qu' aucun monarque anglais n' a plus prénommé [MASK] son héritier présomptif .
Dans l' administration de son royaume , [MASK] était un gouvernant compétent , mais il gagna la désapprobation des barons anglais en les taxant au-delà des règles féodales traditionnelles de la suzeraineté .
[MASK] était un roi impartial et bien informé .
[MASK] [MASK] a résumé le legs du règne de [MASK] : " La nation britannique et le monde anglophone doivent bien plus aux vices de [MASK] qu' aux labeurs de souverains vertueux " .
[MASK] [MASK] [MASK] est représenté dans de nombreux récits .
On le retrouve dans la pièce de [MASK] [MASK] , [MASK] [MASK] [MASK] , dans le roman de sir [MASK] [MASK] , [MASK] , dans le roman de [MASK] [MASK] [MASK] , [MASK] [MASK] , et dans d' autres versions de la légende de [MASK] [MASK] [MASK] où il est généralement représenté à l' époque de sa régence en l' absence de [MASK] , et désigné sous le titre de " [MASK] [MASK] " .
Il apparaît encore dans le cycle intitulé [MASK] [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] [MASK] , où il est l' archétype du despote cruel et calculateur .