[MASK] est la sixième planète du système solaire par ordre de distance au [MASK] .
C' est une géante gazeuse , la seconde en masse et en volume après [MASK] dans le système solaire .
D' un diamètre d' environ neuf fois et demi celui de la [MASK] , elle est majoritairement composée d' hydrogène .
[MASK] a la forme d' un sphéroïde oblat : la planète est aplatie aux pôles et renflée à l' équateur .
Les autres géantes gazeuses du système solaire ( [MASK] , [MASK] et [MASK] ) sont également aplaties , mais de façon moins marquée .
[MASK] est la deuxième planète la plus massive du système solaire , 3,3 fois moins que [MASK] , mais 5,5 fois plus que [MASK] et 6,5 fois plus qu' [MASK] .
[MASK] est la seule planète du système solaire dont la masse volumique moyenne est inférieure à celle de l' eau : 0,69 g/cm³ .
La haute [MASK] [MASK] [MASK] est constituée à 93,2 % d' hydrogène et à 6,7 % d' hélium en termes de molécules de gaz ( 96,5 % d' hydrogène et 3,5 % d' hélium en termes d' atomes ) .
La structure interne de [MASK] serait similaire à celle de [MASK] , avec un noyau rocheux de silicates et de fer , entouré d' une couche d' hydrogène métallique , puis d' hydrogène liquide , puis enfin d' hydrogène gazeux .
Une explication proposée serait une " pluie " de gouttelettes d' hélium dans les profondeurs de [MASK] , dégageant de la chaleur par friction en tombant dans une mer d' hydrogène plus léger [ réf. nécessaire ] .
De manière similaire à [MASK] , l' atmosphère de [MASK] est organisée en bandes parallèles , même si ces bandes sont moins visibles et plus larges près de l' équateur .
Depuis , les télescopes terrestres ont fait suffisamment de progrès pour pouvoir suivre l' atmosphère saturnienne et les caractéristiques courantes chez [MASK] ( comme les orages ovales à longue durée de vie ) ont été retrouvées chez [MASK] .
Dans les images transmises par la sonde [MASK] , l' atmosphère de l' hémisphère nord apparaît bleue , de façon similaire à celle d' [MASK] .
L' imagerie infrarouge a montré que [MASK] possède un vortex polaire chaud , le seul phénomène de ce type connu dans le système solaire .
Les images prises par le [MASK] [MASK] [MASK] indiquent la présence au pôle sud d' un courant-jet , mais pas d' un vortex polaire ou d' un système hexagonal analogue .
Les orages de [MASK] sont particulièrement longs .
Les décharges électriques provoquées par les orages de [MASK] émettent des ondes radio dix mille fois plus fortes que celles des orages terrestres .
Le champ magnétique de [MASK] est plus faible que celui de [MASK] et sa magnétosphère est plus petite .
[MASK] [MASK] de [MASK] subissant une rotation différentielle , plusieurs systèmes ont été définis , avec des périodes de rotation propres ( un cas similaire à celui de [MASK] ) :
Cependant , lors de son approche de [MASK] en 2004 , la sonde [MASK] mesura que la période de rotation radio s' était légèrement accrue , atteignant 10 h 45 min 45 s ( ± 36 s ) .
En mars 2007 , il a été annoncé que la rotation des émissions radio ne rend pas compte de la rotation de la planète , mais est causée par des mouvements de convection du disque de plasma entourant [MASK] , lesquels sont indépendants de la rotation .
Les variations de période pourraient être causées par les geysers de la lune [MASK] .
La vapeur d' eau émise en orbite saturnienne se chargerait électriquement et pèserait sur le champ magnétique de la planète , ralentissant sa rotation par rapport à celle de [MASK] .
Si ce point est vérifié , on ne connaît aucune méthode fiable pour déterminer la période de rotation réelle du noyau de [MASK] , , , .
Les anneaux de [MASK] sont un des spectacles les plus remarquables du système solaire et constituent la caractéristique principale de la [MASK] [MASK] .
[MASK] possède un grand nombre de satellites naturels .
[MASK] , la plus grande d' entre elles , plus grande que [MASK] ou [MASK] , est le seul satellite du système solaire à posséder une atmosphère dense .
Tous les satellites pour lesquels la période de rotation est connue , à l' exception de [MASK] et d' [MASK] , sont synchrones .
[MASK] est une des cinq planètes visibles à l' œil nu la nuit et elle est connue et observée depuis l' [MASK] .
En 1610 , [MASK] , en braquant son télescope vers [MASK] , en observe les anneaux mais ne comprend pas ce qu' il en est , décrivant que la planète aurait des " oreilles " .
En 1613 , ils réapparaissent sans que [MASK] puisse émettre une hypothèse quant à ce qu' il observe .
En 1656 , [MASK] [MASK] , en utilisant un télescope bien plus puissant , comprend que la planète est en réalité entourée d' un anneau , qu' il pense être solide .
En 1675 , [MASK] [MASK] détermine que l' anneau est composé de plusieurs petits anneaux , séparés par des divisions ; la plus large d' entre elles sera plus tard appelée la [MASK] [MASK] [MASK] .
En 1859 , [MASK] [MASK] [MASK] démontre que les anneaux ne peuvent pas être solides .
Il émet l' hypothèse qu' ils sont constitués d' un grand nombre de petites particules , toutes orbitant autour de [MASK] indépendamment .
[MASK] [MASK] passa à 22 000 km des nuages de [MASK] en septembre 1979 .
Elle étudia l' étalement des anneaux , découvrit l' [MASK] [MASK] et le fait que les divisions ne sont pas vides de matériaux .
[MASK] [MASK] mesura également la température de [MASK] .
En novembre 1980 , [MASK] [MASK] visita le système saturnien .
En août 1981 , [MASK] [MASK] continua l' étude de [MASK] .
La gravité de [MASK] fut utilisée pour diriger la sonde vers [MASK] ( voir cette planète ) qui , à son tour , la dirigea vers [MASK] .
Elles découvrirent également la [MASK] [MASK] [MASK] et la [MASK] [MASK] [MASK] .
La [MASK] [MASK] s' est placée en orbite autour de [MASK] le 1 er juillet 2004 afin d' étudier le système saturnien , avec une attention particulière pour [MASK] .
En juin 2004 , elle effectua un survol de [MASK] .
En mars 2007 , de nouvelles images du pôle mirent en évidence des mers d' hydrocarbures , la plus grande ayant presque la taille de la [MASK] [MASK] .