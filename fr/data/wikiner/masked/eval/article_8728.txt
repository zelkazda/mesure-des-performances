L' arrondissement de [MASK] ( Ville ) compte 6 cantons :
Note : la nomenclature officielle utilise la surprenante graphie [MASK] ( Ville ) , sans espace , pour qualifier la commune , le canton et l' arrondissement .
En dehors de ces usages officiels , cette graphie est peu respectée , de même que , dans l' usage courant , les noms sont abrégés en " [MASK] " , " [MASK] [MASK] [MASK] " et " [MASK] [MASK] [MASK] " .
L' arrondissement de [MASK] compte 6 cantons :
L' arrondissement de [MASK] compte 7 cantons :
L' arrondissement de [MASK] compte 13 cantons :