Le samarium a été découvert par spectroscopie en 1853 par le chimiste suisse [MASK] [MASK] [MASK] [MASK] [MASK] , par l' observation de ses fines raies d' absorption dans le didymium .
En 1901 le chimiste français [MASK] [MASK] [MASK] réussit à séparer les deux oxydes .
Son nom provient du minerai , la samarskite , découverte par le colonel [MASK] , dans une mine de l' [MASK] .