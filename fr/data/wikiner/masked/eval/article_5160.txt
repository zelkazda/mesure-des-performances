La [MASK] [MASK] [MASK] désigne une série de guerres civiles qui eurent lieu en [MASK] entre la [MASK] [MASK] [MASK] [MASK] et la [MASK] [MASK] [MASK] [MASK] .
La guerre prit fin en 1485 , quand le dernier des rois [MASK] [MASK] [MASK] [MASK] [MASK] mourut au champ d' honneur , et qu' [MASK] [MASK] devint roi .
L' emblème de la [MASK] [MASK] [MASK] était la rose rouge , tandis que celui des [MASK] était la rose blanche , ce qui est à l' origine du nom donné a posteriori à ce conflit .
Comme il descendait de [MASK] [MASK] [MASK] , le troisième fils d' [MASK] [MASK] , les droits de [MASK] à la couronne étaient discutables .
Malgré tout , [MASK] fut couronné sous le nom d' [MASK] [MASK] ; il fut accepté , car le gouvernement de [MASK] [MASK] avait été extrêmement impopulaire .
[MASK] mourut en 1413 .
Son fils et successeur , [MASK] [MASK] , était un grand stratège et ses succès militaires contre la [MASK] dans la [MASK] [MASK] [MASK] [MASK] lui valurent une énorme popularité , qui lui permit d' assurer le maintien des [MASK] sur le trône .
Les rois [MASK] avaient d' ailleurs été tourmentés par la question de leur légitimité et la [MASK] [MASK] [MASK] pensait avoir des droits sur le trône beaucoup plus forts .
Dans bien des cas , il s' agissait de luttes entre des familles établies depuis longtemps et la petite noblesse d' autrefois dont [MASK] [MASK] avait accru le pouvoir et l' influence à la suite des rébellions organisées contre lui .
Un élément dans ces querelles était apparemment la présence d' un grand nombre de soldats qu' on avait renvoyés des armées anglaises en [MASK] .
[MASK] manifesta bien vite son pouvoir avec une audace jamais égalée ( bien qu' il n' y ait aucune preuve qu' il eût alors aspiré au trône ) .
Elle noua des alliances contre [MASK] et conspira avec d' autres nobles pour réduire son influence .
[MASK] de plus en plus pressé recourut finalement aux armes en 1455 avec la [MASK] [MASK] [MASK] [MASK] .
La [MASK] [MASK] [MASK] [MASK] , relativement limitée , fut le premier conflit ouvert de la guerre civile .
[MASK] et ses alliés recouvrèrent leur position influente , et pendant quelque temps les deux côtés parurent choqués qu' une bataille réelle se fût déroulée , si bien qu' ils firent tout leur possible pour apaiser leurs différends .
La cour du roi fut réinstallée à [MASK] .
Le désordre croissait dans la capitale , ainsi que la piraterie sur la côte sud , mais le roi et la reine ne se préoccupaient que de garantir leurs propres positions , la reine par exemple établit pour la première fois la conscription en [MASK] .
Bientôt , les armées yorkistes réunies affrontèrent l' armée lancastrienne , beaucoup plus nombreuse , à la [MASK] [MASK] [MASK] [MASK] .
Un tel succès militaire poussa [MASK] à revendiquer le trône en se fondant sur l' illégitimité de la lignée lancastrienne .
Le lendemain , [MASK] produisit des généalogies détaillées pour soutenir sa revendication en se fondant sur le fait qu' il descendait de [MASK] [MASK] [MASK] et il rencontra plus de compréhension .
[MASK] accepta ce compromis comme ce qu' on lui proposait de meilleur .
Elle prit son armée à [MASK] , en se trouvant à la tête de plus d' hommes que lorsqu' elle était venue .
Il donna du courage à ses hommes en leur montrant une " vision " de trois soleils à l' aube ( un phénomène connu sous le nom de " parhélie " ) , et en leur disant que c' était là un présage de victoire puisqu' il représentait les trois fils survivants de [MASK] : lui-même , [MASK] et [MASK] .
Dunstanburgh , [MASK] et [MASK] furent parmi les derniers à tomber .
Le dernier à capituler fut en 1468 la puissante forteresse de [MASK] ( [MASK] [MASK] [MASK] ) , après un siège qui dura sept ans .
Cette fois , c' est [MASK] [MASK] qui fut forcé de fuir le pays quand [MASK] [MASK] changea de camp pour soutenir son frère [MASK] .
Édouard ne s' attendait pas à l' arrivée par le nord de la grande armée de [MASK] et il dut ordonner à son armée de se disperser .
Le succès de [MASK] fut pourtant de courte durée .
Édouard débarqua avec une petite armée à [MASK] , sur la côte du [MASK] .
La bataille fut disputée dans un brouillard épais et certains des hommes de [MASK] s' attaquèrent entre eux par erreur .
Immédiatement , tous les hommes crurent qu' ils avaient été trahis et l' armée de [MASK] se débanda .
La restauration d' [MASK] [MASK] en 1471 est quelquefois considérée comme ayant mis fin à la [MASK] [MASK] [MASK] .
Les deux garçons emprisonnés , connus comme " les [MASK] [MASK] [MASK] [MASK] " , disparurent , peut-être assassinés ; l' identité de l' assassin et de ses commanditaires demeure l' un des sujets les plus controversés de l' histoire anglaise .
Comme [MASK] était le meilleur général des [MASK] , beaucoup d' entre eux l' acceptèrent , voyant en lui un chef plus capable de les maintenir au pouvoir qu' un garçon qui aurait dû régner assisté d' un conseil de régents .
Il consolida alors sa position en se mariant avec [MASK] [MASK] [MASK] , la fille d' [MASK] [MASK] et la meilleure prétendante yorkiste survivante .
Il réunit ainsi les deux maisons royales , et combina les symboles rivaux de la rose rouge et de la rose blanche dans le nouvel emblème rouge et blanc des [MASK] .
Pour plus de sûreté il prit soin de faire exécuter au moindre prétexte quiconque pouvait prétendre au trône , politique poursuivie par son fils [MASK] [MASK] .
La plupart des historiens considèrent que l' avènement d' [MASK] [MASK] a marqué la fin de la [MASK] [MASK] [MASK] .
D' autres soutiennent qu' elle ne s' est terminée qu' avec la [MASK] [MASK] [MASK] ( 1487 ) .
À cette occasion le prétendant au trône était un garçon appelé [MASK] [MASK] qui présentait une certaine ressemblance physique avec le jeune [MASK] [MASK] [MASK] , un survivant de la [MASK] [MASK] [MASK] .
Bien que les historiens discutent toujours sur l' importance de l' impact qu' a eu le conflit sur la vie de l' [MASK] médiévale , il ne fait guère de doute que la [MASK] [MASK] [MASK] a entraîné un bouleversement politique considérable et d' énormes changements dans l' équilibre des pouvoirs tel qu' il s' était établi .
Le résultat le plus évident a été l' effondrement de la dynastie des [MASK] et son remplacement par une nouvelle dynastie , les [MASK] , qui devait radicalement changer l' [MASK] au cours des années suivantes .
Les lourdes pertes parmi la noblesse , additionnées aux effets de la peste noire , entraînèrent une période de bouleversement social intense dans l' [MASK] féodale ; le pouvoir des nobles s' effondra , tandis que se renforçaient les classes marchandes et que naissait une monarchie forte et centralisée avec les [MASK] .
On s' est tout de même demandé si le traumatisme des guerres n' avait pas été exagéré par [MASK] [MASK] , qui voulait ainsi se présenter comme celui qui y avait mis fin et avait ramené la paix .
La guerre fut un désastre pour l' influence anglaise en [MASK] , qui déclinait déjà , et vers la fin du conflit , il ne restait rien de ce qui avait été conquis pendant la [MASK] [MASK] [MASK] [MASK] , à part [MASK] qui devait finalement tomber sous le règne de [MASK] [MASK] .
Bien que les derniers dirigeants anglais dussent continuer à faire campagne sur le continent , jamais l' [MASK] ne reprit ces territoires .
L' [MASK] ne devait plus voir une autre armée se constituer jusqu' à la [MASK] [MASK] [MASK] d' [MASK] [MASK] .
Avec le déclin du pouvoir militaire des barons , c' est à la cour des [MASK] que se sont vidées les querelles entre barons sous l' arbitrage du roi .
Au fil des siècles , la rivalité supposée entre le [MASK] et le [MASK] s' est transformée en rivalité sportive .