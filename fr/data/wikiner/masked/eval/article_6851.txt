Issu d' une famille noble mais sans grandes ressources , [MASK] [MASK] reçut une bonne éducation humaniste , mais regretta toujours de ne pas avoir appris le grec .
Entré en 1504 au service du cardinal [MASK] [MASK] [MASK] , il accomplit pour ce prince de nombreuses ambassades , notamment auprès du pape [MASK] [MASK] .
Ayant refusé de suivre le prélat en [MASK] , il passa au service d' [MASK] [MASK] [MASK] , duc de [MASK] et frère du cardinal .
Il s' en tira avec honneur et put enfin se retirer dans sa petite maison de [MASK] ( toujours visible ) entouré par l' affection de sa maîtresse et de son fils .
C' est entre ces diverses charges que [MASK] [MASK] ne cessa de travailler à son chef-d'œuvre , le [MASK] [MASK] ( [MASK] [MASK] ) , subtile parodie du poème chevaleresque , qui se présente comme une suite au [MASK] amoureux de [MASK] [MASK] [MASK] , son prédécesseur .
Ses compatriotes , dans leur admiration , l' ont surnommé le divin [MASK] .