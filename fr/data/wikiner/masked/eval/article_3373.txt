[MASK] [MASK] , homme politique alsacien , né le 26 décembre 1941 à [MASK] ( [MASK] ) .
[MASK] [MASK] est né le 26 décembre 1941 à [MASK] , commune du [MASK] , dans une famille d' agriculteurs .
Il crée avec des amis une société de tir , un judo-club , l' office municipal et culturel de [MASK] .
C' est sur les traces de [MASK] [MASK] , président du conseil général et député , que [MASK] [MASK] commence à s' intéresser à la politique .
C' est en 1969 qu' il se lance , en participant à la campagne de [MASK] [MASK] .