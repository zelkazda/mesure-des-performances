[MASK] [MASK] [MASK] [MASK] ( [MASK] [MASK] ) , son unique roman , est considéré comme un classique de la littérature anglaise .
Un cadeau offert par leur père à [MASK] ( douze soldats de bois ) , en juin 1826 , met en branle leur imagination :
Dès ce moment , le nom de [MASK] " devint synonyme de toutes les interdiction et de toutes les audaces " , comme s' il suscitait par essence la levée des inhibitions .
Dans le domaine artistique , le peintre [MASK] [MASK] exerce également une impression forte sur l' imagination des enfants [MASK] .
Les références à [MASK] [MASK] dans la culture populaire sont nombreuses :