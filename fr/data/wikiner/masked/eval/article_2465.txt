Le terme verts a été créé par les écologistes allemands , les [MASK] , lors de leur première participation à une élection nationale en 1980 .
En 1993 , [MASK] [MASK] devint ministre de l' environnement dans le gouvernement letton .
La [MASK] [MASK] [MASK] rejoignit elle le gouvernement de centre-gauche du social-démocrate [MASK] [MASK] en 1995 .
En 1998 , le camp rouge-vert allemand mit fin à seize ans de domination de l' [MASK] [MASK] et accéda au pouvoir .
[MASK] [MASK] , qui devint alors ministre des affaires étrangères , a probablement été le ministre vert le plus médiatisé jusqu' à ce jour .
Il siégea , avec d' autres écologistes , jusqu' en 2005 dans le gouvernement du social-démocrate [MASK] [MASK] .
Ils restèrent au gouvernement belge durant une législature dans le gouvernement de [MASK] [MASK] , réunissant libéraux , socialistes et écologistes .
En 2004 , les écologistes connurent une défaite retentissante aux élections et durent quitter certains gouvernements mais restèrent présents à la tête de la [MASK] [MASK] .
Ils accédèrent à nouveau au gouvernement régional et communautaire au sud du pays et à [MASK] après les élections de 2009 .
En 2004 , un vert , [MASK] [MASK] , occupa même brièvement le poste de premier-ministre .
En avril , [MASK] [MASK] verte finlandaise fit son retour au gouvernement après cinq ans d' absence , cette fois-ci dans un gouvernement de centre-droite .