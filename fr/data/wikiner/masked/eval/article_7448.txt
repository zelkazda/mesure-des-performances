L' [MASK] et [MASK] [MASK] lui attribuent le code 39 .
Le mot [MASK] vient du bas-latin ( ou latin tardif ) juria et signifie " forêt " ou " forêt sauvage " ou " forêt de montagne " ( du celtique joris ) .
Le département a été créé à la [MASK] [MASK] , le 4 mars 1790 en application de la loi du 22 décembre 1789 , à partir d' une partie de la province de [MASK] .
Le tourisme d' hiver est également important au-dessus de 800 mètres avec le ski de fond ( [MASK] -- [MASK] ... ) ou ski de piste dans la station [MASK] [MASK] .