[MASK] en tombe très vite amoureux .
[MASK] est un petit garçon turbulent , il arrive à sa nouvelle école lors du premier tome .
Marie-Rose , la mère de [MASK] , est mère au foyer .
Elle adore [MASK] , même si parfois il la met dans tous ses états .
Ainsi , quand [MASK] ramène son bulletin , il entre souvent dans une colère folle .
Le grand-père de [MASK] déteste le mari de sa fille et il ne lui cache pas .
Il dit que le père de [MASK] est un vendeur de " carpettes " , ce qui agace le père au plus haut point .
Il partage une très grande complicité avec [MASK] , son petit-fils .
Par exemple , une fois , [MASK] est au courant des difficultés financières de ses parents .
[MASK] fait tout pour lui plaire mais elle ne s' en rend même pas compte .
C' est l' institutrice de [MASK] .
C' est le meilleur ami de [MASK] .
Il a des lunettes et se plaint souvent quand [MASK] les casse .
Il lui rappelle souvent que sa sœur est dingue de son meilleur ami mais [MASK] réplique en disant toujours que sa sœur est moche .
La famille de [MASK] est unie malgré les disputes fréquentes qui jalonnent les albums .
En règle générale , cela part d' une réflexion de [MASK] sur un de ses problèmes quotidiens .
Le passé de la famille de [MASK] est plus ou moins flou .
En effet on apprend dans un album que ses parents se sont rencontrés lors d' un achat de tapis pour la grand mère de [MASK] , aujourd'hui décédée .
[MASK] est un enfant dans la moyenne de son âge .
[MASK] a souvent des traits de caractère et des pensées extrêmement égocentriques ( à rapprocher de ses tendances caractérielles ) et machistes .
Au total , 24 albums ont été publiés aux [MASK] [MASK] et diffusés à plus de 8 millions d' exemplaires .
[MASK] a un lectorat de fidèles .
Depuis sa création , les gags de [MASK] paraissent chaque semaine dans le magazine hebdomadaire belge [MASK] .
Et pendant tout un temps dans " [MASK] [MASK] [MASK] [MASK] " hebdomadaire français pour les jeunes de 7 à 14 ans .
C' est en 2002 que [MASK] y fait sa première apparition dans un livre relié sans images .
[MASK] inaugure sa carrière à la télévision en septembre 2001 sur [MASK] [MASK] et vingt jours après sur [MASK] [MASK] , avec une série de dessins animés , adaptation plus ou moins libre des albums parus .
Tous les épisodes sont racontés à la première personne par [MASK] à la manière d' un journal intime ( qu' il range dans sa table de nuit d' ailleurs ) .
Chaque épisode se conclut par la réplique suivante prononcée par [MASK] : " Quelle vie on mène quand on a huit ans " , prononcée avant qu' il ne s' endorme .