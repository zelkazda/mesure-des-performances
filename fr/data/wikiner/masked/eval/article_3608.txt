Le parti devenant [MASK] [MASK] en juin 1997 , il en prend la présidence dans le [MASK] [MASK] [MASK] .
Il est considéré comme le leader de l' opposition municipale à [MASK] , commune de la banlieue de [MASK] , depuis mars 2001 .
À l' automne 2000 , la suppléante de [MASK] [MASK] démissionne pour lui permettre de récupérer son siège à l' [MASK] [MASK] .
[MASK] [MASK] est le candidat de la droite à la législative partielle ; il est battu ( 16,82 % et 34,02 % aux deux tours ) .
Comptable , [MASK] [MASK] quitte en 2001 la société de sérigraphie qu' il a créé pour se consacrer plus largement à la politique .
En tête dès le premier tour ( 25,6 % contre 21,6 % ) , il gagne avec 1864 voix d' avance ( 53,4 % ) face à [MASK] [MASK] .
Il fait partie du groupe [MASK] et de la commission des affaires culturelles , familiales et sociales de l' [MASK] [MASK] .
Il est président du groupe d' amitié [MASK] -- [MASK] , vice-président du groupe d' études pour les personnes âgées , membre du groupe d' étude sur la chasse , du groupe d' étude sur les contrefaçons , il est aussi membre de la mission d' information sur l' amiante .
[MASK] [MASK] est membre de la liste [MASK] de [MASK] [MASK] pour les régionales , liste battue au second tour par celle de [MASK] [MASK] .
En mars 2004 , [MASK] [MASK] est également candidat aux cantonales sur son canton de [MASK] .
Il rejoint le [MASK] [MASK] [MASK] en 2004 , dont il devient ensuite secrétaire national .