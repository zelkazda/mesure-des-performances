Les concepts d ' enfance et les valeurs y étant afférentes ont beaucoup évolué de l' [MASK] à nos jours , selon les civilisations , les classes sociales et les contextes et la personnalité des parents .
À partir de [MASK] on identifie des stades associés au symbolisme de la sexualité infantile .
On peut citer [MASK] [MASK] qui a mis en évidence le besoin relationnel de l' enfant dès le plus jeune âge , ou encore [MASK] [MASK] qui dans la suite de [MASK] [MASK] sera un des premiers à proposer une théorie du développement psychique depuis la naissance .
On peut citer aussi [MASK] [MASK] qui a entre autres médiatisé l' existence de l' enfant en tant que personne ( ou sujet ) dès sa naissance .
En [MASK] , la loi offre une protection accrue pour les mineurs .