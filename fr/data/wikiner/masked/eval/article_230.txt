L' [MASK] et [MASK] [MASK] lui attribuent le code 14 .
Le département a été créé à la [MASK] [MASK] , le 4 mars 1790 , en application de la loi du 22 décembre 1789 , à partir d' une partie de l' ancienne province de [MASK] .
Il est limitrophe des départements de l' [MASK] à l' est , de l' [MASK] au sud et de la [MASK] à l' ouest , tandis que son flanc nord est constitué par les côtes de la [MASK] .
Le climat plutôt doux ; plus doux qu' au cœur de l' [MASK] par exemple .
Il ne fait pas que pleuvoir dans le [MASK] , les saisons sont relativement bien marquées , et là en ce début de printemps , on peut sentir les effluves des fleurs dites de " printemps " .
Un proverbe dit aussi : " En [MASK] , il peut pleuvoir chez ton voisin et faire soleil dans ton jardin " .
[MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] sont nés à [MASK] .