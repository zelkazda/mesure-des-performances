Les projets de loi sont élaborés au sein d' un ou plusieurs ministères et soumis pour avis au [MASK] [MASK] [MASK] .
Les projets de loi de finances et de loi de financement de la sécurité sociale sont soumis en premier lieu à l' [MASK] [MASK] .
Le [MASK] [MASK] [MASK] a la responsabilité de conseiller le gouvernement français sur les projets de lois , il est juge administratif de la réglementation .
Deux rapports du [MASK] [MASK] [MASK] , en 1991 et 2006 , ont alerté sur des risques d' insécurité juridique provoqués par l' excès de lois .
Une des instances chargées de veiller à la sécurité juridique est le [MASK] [MASK] [MASK] .
Il faut remarquer que tout projet de loi est soumis à l' avis du [MASK] [MASK] [MASK] , ce qui n' est pas le cas d' une proposition de loi .