La nécropole s' étend sur la bordure orientale du plateau dominant la rive gauche de la vallée du [MASK] .
Elle se compose du sud vers le nord d' un premier monument funéraire sur la commune d' [MASK] , qui a été fouillé de 1985 à 1990 .
En 1848 , création de la commune par la réunion de [MASK] et d' [MASK] .