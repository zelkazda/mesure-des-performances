L' [MASK] [MASK] souhaite quant à elle faire de [MASK] ( considérée par l' [MASK] comme un territoire occupé ) la capitale d' un futur [MASK] [MASK] .
[MASK] [MASK] située sur les [MASK] [MASK] [MASK] ( dont le [MASK] [MASK] ) , à 745 m d' altitude moyenne , avec de fortes variations entre monts et vallées ( de 700 à 800 m environ ) .
Le [MASK] [MASK] culmine à 826 m et la [MASK] [MASK] [MASK] descend sous les 600 m .
Le point le plus élevé est le [MASK] [MASK] avec 834 m .
La ville , chef-lieu du [MASK] [MASK] [MASK] , est très hétérogène : s' y mêlent de nombreuses religions , peuples , groupes socio-économiques .
Une étymologie détaillée est donnée par [MASK] et [MASK] .
[MASK] possède un climat méditerranéen , marqué par une forte chaleur et une forte aridité en été .
Depuis 1967 , les gouvernements israéliens successifs , quel que soit le parti au pouvoir , s' évertuent à transformer la physionomie de [MASK] .
[MASK] faisait 38 km 2 en 1967 .
Il faut ajouter à cela la mise en place d' une ceinture de blocs de colonies de peuplement juif autour de la ville au nord , au sud et à l' est ( [MASK] [MASK] ) qui augmente la taille de [MASK] à 440 km 2 .
Ainsi , l' extension de [MASK] a eu pour effet d' édifier une ceinture urbaine disjoignant les quartiers arabes de [MASK] du reste de la [MASK] .
On appelle hiérosolymitain ( e ) s les habitants de [MASK] .
La population arabe a augmenté en proportion depuis 1967 , passant de 20 % en 1967 à 32 % en 2004 pour l' ensemble de [MASK] .
Cependant , depuis 2005 , les indices conjoncturels de fécondité des populations juives et arabes de [MASK] sont au même niveau de 3,9 enfants par femme -- conséquence de la baisse significative de la natalité arabe enregistrée depuis quelques années .
La majorité part habiter dans la proche banlieue de [MASK] où les coûts du logement sont nettement inférieurs .
La politique de permis de construire , que le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] jugent discriminatoire envers les [MASK] , et la destruction de maisons appartenant à des [MASK] affectent également la population arabe de [MASK] , .
La ville de [MASK] [MASK] considérée comme " trois fois sainte " car elle contient les lieux les plus sacrés des religions juive et chrétienne et le troisième lieu saint de l' islam :
[MASK] [MASK] un site privilégié :
Le statut de la ville , intégralement sous administration civile israélienne depuis la [MASK] [MASK] [MASK] [MASK] , est contesté .
Elle avait été convoquée par la commission de conciliation des [MASK] .
Finalement les accords d' armistice n' ont pas été enregistrés par les [MASK] qui ont néanmoins contribué à leur surveillance .
Selon le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , il y a trois entités concurrentes : une partie israélienne , une partie palestinienne , une partie internationale .
Le 26 octobre 1994 le [MASK] [MASK] [MASK] [MASK] est signé .
Mais , le statut des territoires " ... sous contrôle militaire israélien depuis 1967 " , c' est-à-dire notamment [MASK] , est réservé .
Il est néanmoins précisé que " le rôle spécial " du [MASK] [MASK] [MASK] sur les lieux de pélérinage musulmans de [MASK] [MASK] reconnu .
La question de la légitimité de chacune des deux parties sur [MASK] entraîne également des débats d' ordre archéologique .
Les différents pouvoirs israéliens , législatif , exécutif , judiciaire et administratif , sont regroupés à [MASK] .
En 2000 au [MASK] [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] propose de partager la ville [ réf. nécessaire ] mais cela est refusée par [MASK] [MASK] [ réf. nécessaire ] et ce qui a vraisemblablement conduit le premier ministre israélien à la défaite aux élections anticipées qui ont suivi .
En 2005 , la question du statut et de l' éventuel partage de [MASK] reste au cœur du futur processus de paix mais aucune tentative de négociations n' a plus été entamée sur ce point depuis le [MASK] [MASK] [MASK] .
Lors de la [MASK] [MASK] [MASK] [MASK] [MASK] par l' [MASK] en 1988 une première revendication de [MASK] pour capitale est effectuée. , .
L' [MASK] de [MASK] [MASK] s' est souvent positionnée dans le sens de ces revendications refusant d' avoir une autre capitale que [MASK] .
[MASK] [MASK] par ailleurs la ville d' origine de réfugiés palestiniens qui souhaitent revenir y vivre .
Elles concernent [MASK] de manière tacite ( résolution 252 ) ou explicite pour toutes les résolutions ultérieures :
Parmi les principales industries de [MASK] , citons les fabriques de chaussures , le textile , l' industrie pharmaceutique , les produits de métaux , et les articles imprimés .
On trouve à [MASK] un grand nombre de monuments religieux et historiques , comme le [MASK] [MASK] -- appelé aussi [MASK] [MASK] [MASK] -- , la [MASK] [MASK] et le [MASK] [MASK] [MASK] , l' [MASK] [MASK] [MASK] .
De nombreux sites archéologiques existent à [MASK] , notamment les [MASK] [MASK] [MASK] , la [MASK] [MASK] [MASK] .
L' [MASK] [MASK] [MASK] [MASK] a été fondée en 1925 , elle fait depuis partie des 100 meilleures universités du monde .
[MASK] [MASK] ou [MASK] [MASK] ont fait partie du conseil des gouverneurs .
Le travail des étudiants et chercheurs est facilité par la [MASK] [MASK] [MASK] [MASK] , qui accueille prés de 5 millions de livres .
L' [MASK] [MASK] a été fondée en 1984 , elle accueille les universitaires palestiniens et ceux des pays arabes .
L' [MASK] [MASK] est située au sud-est de la ville sur près de 19 hectares .
De nombreuses écoles de l' est de [MASK] sont remplies à leur capacité maximum , au point qu' on enregistre des plaintes contre la surpopulation .
Cependant , la municipalité de [MASK] [MASK] en train de construire plus d' une douzaine de nouvelles écoles dans les quartiers arabes de la ville .
Le [MASK] [MASK] , dans le [MASK] [MASK] [MASK] [MASK] [MASK] , est le plus connu en [MASK] , ayant été vainqueur cinq fois de la [MASK] [MASK] [MASK] [MASK] [MASK] .
Le club a gagné le [MASK] [MASK] [MASK] [MASK] [MASK] trois fois , et la [MASK] [MASK] en 2004 .
Il a été utilisé pour les vols domestiques israéliens jusqu' en octobre 2000 notamment pour les vols vers [MASK] .
Depuis lors , les vols ont été transférés vers l' [MASK] [MASK] [MASK] .
Désormais , l' aéroport est sous le contrôle de [MASK] en raison de la situation géographique de l' aéroport , proche des territoires palestiniens .
[MASK] s' occupe de la construction des rames du tramway et [MASK] [MASK] s' occupe de l' aménagement cependant le groupe pourrait quitter le projet .