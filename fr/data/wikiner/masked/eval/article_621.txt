Le [MASK] est un pays d' [MASK] [MASK] [MASK] , situé au sud des [MASK] et bordé au sud par le [MASK] et le [MASK] .
Les premiers hommes seraient arrivés par le [MASK] , en traversant le [MASK] [MASK] [MASK] .
La présence humaine attestée au [MASK] remonte à environ 50000 ans [ réf. nécessaire ] .
À cette époque , les [MASK] étaient le groupe dominant dans la population locale .
Pour cette raison , les [MASK] n' opposèrent initialement que peu de résistance à l' avance des conquistadors , mais plus tard , ils marquèrent leur opposition lorsqu' ils se rendirent compte qu' ils n' étaient pas les messagers divins d' abord admirés .
Les [MASK] furent défaits en 1521 et leur capitale rasée .
Il s' agit de la période qui s' étend entre la défaite de l' empire aztèque et l' [MASK] [MASK] [MASK] , c' est-à-dire entre 1521 et 1821 .
L' actuel [MASK] faisait partie de la vice-royauté de la [MASK] .
La frontière nordique de la [MASK] n' était d' ailleurs pas définie avec précision mais peut se lire au caractère hispanique des noms géographiques conservé de nos jours .
L' arrivée d' un gouvernement libéral en [MASK] convainquit les créoles qui voulaient s' approprier les privilèges des espagnols de proclamer l' indépendance .
L' [MASK] [MASK] [MASK] entre 1820 et 1848 pourrait presque se résumer à une succession de troubles qui suscitèrent les réactions des puissances européennes , inquiètes des dettes non remboursées .
A la suite de la guerre contre les [MASK] ( lire ci-dessous ) entre 1846 et 1848 , un apaisement se fit sentir .
Face à l' incapacité du gouvernement de [MASK] [MASK] de payer les dettes mexicaines , les gouvernements français , espagnol et britannique envoyèrent une force expéditionnaire occuper le port de [MASK] .
Ce régime dura environ trente ans de 1876 à 1910 , bien que [MASK] eût déclaré avant d' arriver au pouvoir qu' il était opposé aux réélections .
[MASK] encouragea les investissements étrangers ; il s' appuya aussi sur les conseils de positivistes connus sous le nom de cientificos .
[MASK] [MASK] inaugura le monument de l' indépendance et l' hémicycle a [MASK] [MASK] et le 15 septembre 1910 ( centenaire de l' [MASK] [MASK] [MASK] ) tout le centre de [MASK] est illuminé ainsi que la cathédrale .
[MASK] [MASK] , au pouvoir depuis une trentaine d' années , voulait à nouveau se représenter à l' élection présidentielle de 1910 , mais [MASK] [MASK] annonça aussi sa candidature .
Parmi les meneurs des mouvements armées se trouvaient [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .
En 1925 est fondée l' organisation qui deviendra le [MASK] , qui prend le pouvoir en 1929 et le gardera jusqu' en 2000 .
En 1938 , le président [MASK] [MASK] nationalisa la production de pétrole en créant [MASK] .
À la fin de la [MASK] [MASK] [MASK] , le gouvernement mexicain offrit l' asile aux opposants à [MASK] .
L' aviation mexicaine ( escadrille 201 ) participa à la [MASK] [MASK] [MASK] .
La campagne électorale de 1994 fut marquée par l' assassinat de [MASK] [MASK] [MASK] .
D' une part , l' ALENA , accord de libre échange entre les trois pays d' [MASK] [MASK] [MASK] , entra en vigueur le 1 er janvier ; d' autre part , une récession temporaire entraina une dévaluation du peso .
En outre , [MASK] [MASK] succéda à [MASK] [MASK] à la présidence .
[MASK] [MASK] [MASK] , membre du [MASK] ( [MASK] [MASK] [MASK] ) , remporta l' élection présidentielle et devint le premier président n' appartenant pas au [MASK] ( [MASK] [MASK] [MASK] ) depuis plus de 70 ans .
En effet , [MASK] [MASK] avait recueilli 43 % des voix , alors que [MASK] [MASK] ( [MASK] ) obtenait 37 % des suffrages et [MASK] [MASK] 17 % ( [MASK] [MASK] [MASK] [MASK] [MASK] ) ; le [MASK] et le [MASK] sont membres de l' [MASK] [MASK] .
Dans le meme temps , les [MASK] refusent de régulariser la situation des quelques quatre millions de clandestins mexicains se trouvant sur leur territoire .
Ce désintéret soudain des [MASK] , remonte au brusque renforcement de la politique migratoire depuis les attentats du 11 septembre 2001 .
Des anomalies toutes simples comme des erreurs arithmétiques ( par exemple nombre de votants supérieur ou inférieur au nombre de bulletins déposés ) sont mises en avant ( les mêmes " erreurs " sont détectées en faveur du [MASK] ) .