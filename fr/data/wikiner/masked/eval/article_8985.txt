Elle couvre une superficie de 55500 km² où vit une population de quelques 935962 personnes ( [MASK] ) .
Sa capitale est [MASK] .
Les autochtones de la province se nomment les [MASK] .
La partie continentale de la province est une péninsule entourée de l' [MASK] [MASK] , qui a façonné sa côte de plusieurs baies et estuaires .
Aucun endroit de [MASK] ne se trouve à plus de 50 km de la mer .
L' [MASK] [MASK] [MASK] , une grande île au nord-est de la partie continentale , fait aussi partie de la province , de même que l' [MASK] [MASK] [MASK] , une petite île célèbre pour ses naufrages .
La [MASK] est la deuxième plus petite province en superficie après l' [MASK] .
La [MASK] est aussi la province canadienne centrée le plus au sud .
Elle n' est cependant pas celle qui descend le plus au sud , laquelle est l' [MASK] .
Parce qu' une partie de l' [MASK] s' étend loin vers le nord , le centre de l' [MASK] est plus au nord que la [MASK] .
Bien que l' explorateur [MASK] [MASK] l' eût visitée en 1497 pour la couronne d' [MASK] , la [MASK] fut colonisée pour la première fois par la [MASK] .
[MASK] [MASK] [MASK] [MASK] , entouré notamment de [MASK] [MASK] [MASK] et de [MASK] [MASK] , fonda une colonie sur une île à l' embouchure de la [MASK] [MASK] en 1604 .
L' eau potable manqua à cette île pendant l' hiver et , l' année suivante , la colonie fut déplacée à [MASK] [MASK] près d' [MASK] [MASK] , en 1605 .
À cet effet , il fonda le baronetage de [MASK] : ceux qui désiraient acquérir le titre nobiliaire de baronnet devaient payer une taxe qui servirait à l' établissement de la colonie , et y recevraient une dotation en terres .
La colonisation française se poursuivit dans toute la région des provinces maritimes actuelles , en étant centrée sur ce qui constitue aujourd'hui la péninsule de la [MASK] .
Le [MASK] actuel était alors un territoire disputé .
La ville d' [MASK] fut fondée en 1749 par [MASK] [MASK] après la restitution de [MASK] à [MASK] [MASK] pour sauvegarder la colonie britannique de la [MASK] contre la menace perçue de la grande forteresse française .
La présence des [MASK] , francophones et catholiques , sur le territoire de la future colonie britannique posait , aux yeux des autorités britanniques , un problème .
En 1750 , un bon nombre de colons protestants , la plupart des [MASK] , furent attirés en [MASK] pour s' établir sur la côte sud .
Après la [MASK] [MASK] [MASK] , les terres acadiennes furent allouées aux colons américains provenant de la [MASK] .
Environ 8000 de ces planters s' établirent dans la colonie entre 1759 et 1774 , dont l' arrière-grand-père de [MASK] [MASK] [MASK] .
Des milliers de loyalistes qui s' étaient opposés à la révolution américaine , dépouillés de leurs terres et la plupart de leurs biens par le gouvernement de la nouvelle république américaine , s' échappèrent après le [MASK] [MASK] [MASK] en 1783 et vinrent s' établir en [MASK] .
En [MASK] beaucoup de ces loyalistes s' installèrent dans la région au sud d' [MASK] .
En 1848 , la [MASK] devint la première colonie de l' [MASK] [MASK] à avoir un gouvernement responsable , où le gouverneur britannique devait accepter les décisions de l' assemblée législative et des ministres .
Le [MASK] , le voilier qui figure sur la pièce de dix cents canadiens , fut construit à [MASK] , sur la côte sud .
L' économie de la [MASK] est aujourd'hui une économie mixte , de services et d' industrie .
La [MASK] est subdivisée en 18 comtés .