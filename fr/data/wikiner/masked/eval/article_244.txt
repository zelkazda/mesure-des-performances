Sa capitale est [MASK] .
Depuis lors , ce pays a survécu tant bien que mal entre la [MASK] et le [MASK] sans toutefois perdre son identité .
La [MASK] garde encore une culture riche qui a son caractère propre .
À la fin de la [MASK] [MASK] [MASK] en 1945 , la [MASK] a été divisée en deux zones par les puissances mondiales , les [MASK] et l' [MASK] .
La [MASK] [MASK] [MASK] commença en juin 1950 .
L' accord de cessez-le-feu de [MASK] ( signé en 1953 ) , mit fin aux combats .
[MASK] [MASK] est le premier président bénéficiant d' une véritable légitimité démocratique .
Environ 30000 soldats américains sont stationnés en [MASK] [MASK] [MASK] depuis la fin de la [MASK] [MASK] [MASK] .
Le nombre de soldats américains en [MASK] a diminué à 25000 en 2008 dans le cadre d' un redéploiement des forces .
En cas de guerre , les [MASK] exerceraient le commandement militaire en [MASK] [MASK] [MASK] .
Suite à des négociations terminées en 2007 , un accord prévoit que le 17 avril 2012 , le commandement des forces combinées en cas de conflit passe sous la responsabilité de la [MASK] [MASK] [MASK] .
La politique extérieure de la [MASK] [MASK] [MASK] reste dominée par la question des relations intercoréennes et de la [MASK] [MASK] [MASK] [MASK] .
Mais la situation est toujours extrémement tendue entre les deux [MASK] .
Exemple avec la menace d ' " attaque préemptive " proférée par la [MASK] [MASK] [MASK] le 24 décembre 2008 et le torpillage d' une corvette sud-coréenne par un submersible nord-coréen .
Par ailleurs , la [MASK] [MASK] [MASK] est un allié des [MASK] dont environ 30 000 soldats stationnent sur son territoire .
La [MASK] [MASK] [MASK] a apporté le plus important contingent étranger , après celui des [MASK] , lors de la [MASK] [MASK] [MASK] .
Elle a également envoyé des troupes en [MASK] ; le 28 novembre 2006 , le gouvernement sud-coréen a annoncé son intention de diminuer de moitié ( de 2300 à 1200 hommes ) la taille du contingent alors présent en [MASK] .
Les forces sud-coréennes ont quitté ce pays lors de la fin du mandat de la [MASK] [MASK] [MASK] [MASK] en décembre 2008 .
La [MASK] et la [MASK] [MASK] [MASK] ont établi des relations diplomatiques en 1992 .
Les premières relations diplomatiques entre [MASK] [MASK] et la [MASK] ont été établies en 1886 .
Des cérémonies ont été organisées en 2006 en [MASK] et en [MASK] [MASK] [MASK] pour célébrer le 120 e anniversaire des relations diplomatiques entre les deux pays .
Souhaitant diversifier ses relations extérieures , la [MASK] [MASK] [MASK] s' est engagée , en septembre 2006 , à tripler son aide à l' [MASK] .
En particulier , la [MASK] [MASK] [MASK] doit financer en 2007 un projet de lutte contre la méningite en [MASK] [MASK] [MASK] qui concerne un million de personnes .
La [MASK] [MASK] [MASK] est divisée en neuf provinces ( do , 도 , 道 ) , six villes métropolitaines ( gwangyeogsi , 광역시 , 廣域市 ) , et une ville spéciale , la capitale [MASK] , ( teukbyeolsi , 특별시 , 特別市 ) .
[MASK] s' impose comme un nœud de circulation vital , et ce n' est pas un hasard si les deux premières lignes du [MASK] coréen ( le [MASK] [MASK] [MASK] ) inauguré en 2004 passent par cette ville :
La [MASK] [MASK] [MASK] possède un réseau de 88,775 km de routes , dont 1,889 km d' autoroutes , sur lesquelles circulent plus de 14 millions de véhicules immatriculés .
Des mouvements de défense de l' environnement se sont développés en [MASK] [MASK] [MASK] depuis les années 1980 .
Le tigre , qui aurait disparu du sud de la [MASK] en 1922 , a été réintroduit en [MASK] [MASK] [MASK] en 1986 .
Par ailleurs , l' hibiscus syriacus est un des emblèmes de la [MASK] [MASK] [MASK] , cette fleur étant originaire de la [MASK] .
Le plus grand en termes de poids économique des dragons asiatiques ( à ne pas confondre avec les 5 tigres asiatiques ) , la [MASK] [MASK] [MASK] a connu une phase spectaculaire de croissance et d' intégration dans l' économie mondiale moderne .
En 2007 , son PIB par habitant à parité de pouvoir d' achat , à 26523 $ , le place loin devant le [MASK] , au même niveau que la [MASK] , et légèrement inférieur à l' [MASK] , pays membres de l' [MASK] [MASK] .
En 2008 , la [MASK] [MASK] [MASK] est devenue la 13ème puissance économique mondiale avec un PIB de 1024 milliards de dollars américain .
La crise économique asiatique de 1997 a exposé des faiblesses anciennes du modèle de développement de la [MASK] [MASK] [MASK] , y compris des ratios dettes/capitaux propres élevés , la dépendance vis-à-vis de prêts étrangers massifs , le manque de rigueur du secteur financier .
Ayant fait le choix d' un modèle d' économie tournée vers les exportations , la [MASK] [MASK] [MASK] , qui s' est longtemps concentrée sur le marché nord-américain , a récemment diversifié ses partenariats commerciaux .
En 2007 , la [MASK] [MASK] [MASK] est devenu le troisième pays fournisseur de la [MASK] , à hauteur de 10,9 % de l' importation totale , après le [MASK] et l' [MASK] [MASK] .
Le marché de la [MASK] a représenté d' ailleurs plus de 22 % de l' exportation totale de la [MASK] , devant celui de l' [MASK] [MASK] ( 15,1 % ) et les [MASK] ( 12,4 % ) .
La majorité de la population coréenne au [MASK] s' y trouve depuis la période coloniale .
La ville de [MASK] est une des plus grandes zones métropolitaines du monde .
La [MASK] [MASK] [MASK] partage sa culture traditionnelle avec celle de la [MASK] [MASK] [MASK] .
Les deux principaux syndicats sont la [MASK] et la [MASK] .
Les [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ont été organisés à [MASK] .
Le baseball a été introduit en 1905 , et en 1982 l' [MASK] [MASK] [MASK] [MASK] a été formée .
L' équipe nationale a gagné la petite finale du [MASK] [MASK] [MASK] [MASK] en 2004 .
En effet , la [MASK] [MASK] [MASK] a atteint la demi-finale en battant la [MASK] , le [MASK] , l' [MASK] et l' [MASK] , avant d' être éliminée par l' [MASK] .
En 2000 , 2004 et 2006 lors des cérémonies d' ouverture des [MASK] [MASK] les deux [MASK] ont défilé ensemble mais étaient séparées pour les épreuves sportives .
Il est envisagé de constituer une seule équipe nationale commune aux deux [MASK] aux [MASK] [MASK] de 2008 , ainsi qu' aux [MASK] [MASK] .
À cette occasion , " le [MASK] se félicite que le sport et l' idéal olympique puissent amener à la constitution d' une équipe unifiée et souhaiterait remercier les deux délégations de leur engagement et de leurs efforts pour atteindre cet objectif " .
Un circuit de formule 1 est actuellement en construction afin d' accueillir le grand prix de [MASK] [MASK] [MASK] 2010 .
La [MASK] [MASK] [MASK] a pour codes :
Alors que la [MASK] [MASK] [MASK] est un des pays les mieux connectés au monde , le site [MASK] est influent .