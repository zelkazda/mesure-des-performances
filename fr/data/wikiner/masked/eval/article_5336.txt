Expression d' une inaltérable foi en l' homme et d' un optimisme volontaire , affirmant la création musicale comme action d' un artiste libre et indépendant , l' œuvre de [MASK] a fait de lui une des figures les plus marquantes de l' histoire de la musique .
[MASK] [MASK] [MASK] naît à [MASK] en [MASK] le 17 décembre 1770 dans une famille modeste qui perpétue une tradition musicale depuis au moins deux générations .
Il ne faut pas longtemps à [MASK] [MASK] [MASK] pour détecter le don musical de son fils et réaliser le parti exceptionnel qu' il peut en tirer .
Mais là où [MASK] [MASK] avait su faire preuve d' une subtile pédagogie auprès de son fils , [MASK] [MASK] [MASK] ne semble capable que d' autoritarisme et de brutalité et cette expérience demeure infructueuse , à l' exception d' une tournée aux [MASK] en 1781 .
Il emmène [MASK] une première fois à [MASK] en avril 1787 , séjour au cours duquel aurait eu lieu une rencontre furtive avec [MASK] .
À la fin du XVIII e siècle , [MASK] est la capitale de la musique occidentale et représente la meilleure chance de réussir pour un musicien désireux de faire carrière .
Âgé de vingt-deux ans à son arrivée , [MASK] a déjà beaucoup composé , mais pour ainsi dire rien d' important .
Quant à l' enseignement de [MASK] , si prestigieux qu' il soit , il s' avère décevant à bien des égards .
D' un côté , [MASK] se met rapidement en tête que son maître le jalouse ; de l' autre côté , [MASK] ne tarde pas à s' irriter devant l' indiscipline et l' audace musicale de son élève .
Malgré l' influence profonde et durable de [MASK] sur l' œuvre de [MASK] et une estime réciproque plusieurs fois rappelée par ce dernier , le " père de la symphonie " n' a jamais avec [MASK] les rapports de profonde amitié qu' il avait eus avec [MASK] et qui avaient été à l' origine d' une si féconde émulation .
Après le nouveau départ de [MASK] pour [MASK] ( janvier 1794 ) , [MASK] poursuit des études épisodiques jusqu' au début de 1795 avec divers autres professeurs dont le compositeur [MASK] [MASK] et deux autres témoins de l' époque mozartienne : [MASK] [MASK] [MASK] et [MASK] [MASK] .
Son apprentissage terminé , [MASK] se fixe définitivement dans la capitale autrichienne .
[MASK] entreprend une tournée de concerts qui le mène de [MASK] à [MASK] en passant notamment par [MASK] , [MASK] , [MASK] et [MASK] .
Tandis que son activité créatrice s' intensifie , le compositeur participe jusqu' aux environs de 1800 à des joutes musicales dont raffole la société viennoise et qui le consacrent plus grand virtuose de [MASK] au détriment de pianistes réputés comme [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
Bien que l' influence des dernières symphonies de [MASK] y soit apparente , cette dernière est déjà empreinte du caractère beethovénien ( en particulier dans le scherzo du troisième mouvement ) .
Se contraignant à l' isolement par peur de devoir assumer en public cette terrible vérité , [MASK] gagne dès lors une réputation de misanthrope dont il souffrira en silence jusqu' à la fin de sa vie .
Conscient que son infirmité lui interdirait tôt ou tard de se produire comme pianiste et peut-être de composer , il songe un moment au suicide , puis exprime à la fois sa tristesse et sa foi en son art dans une lettre qui nous est restée sous le nom de " [MASK] [MASK] [MASK] " , qui ne fut jamais envoyée et retrouvée seulement après sa mort :
[MASK] , le 6 octobre 1802 .
Ces deux œuvres sont accueillies très favorablement le 5 avril 1803 , mais pour [MASK] une page se tourne .
Au sortir de la crise de 1802 s' annonce l' héroïsme triomphant de la [MASK] [MASK] .
Le compositeur entend initialement dédier cette symphonie au général [MASK] [MASK] , [MASK] [MASK] de la [MASK] [MASK] en qui il voit le sauveur des idéaux de la [MASK] .
Dans l' écriture pianistique aussi , le style évolue : c' est en 1804 la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] dédiée au comte [MASK] dont elle porte le nom , qui frappe ses exécutants par sa grande virtuosité et par les capacités qu' elle exige de la part de l' instrument .
En juillet 1805 , le compositeur fait la rencontre du compositeur [MASK] [MASK] pour qui il ne cache pas son admiration .
À trente-cinq ans , [MASK] s' attaque au genre dans lequel [MASK] s' était le plus illustré : l' opéra .
Mal accueilli au départ ( trois représentations seulement en 1805 ) , [MASK] s' estimant victime d' une cabale , [MASK] ne connaît pas moins de trois versions remaniées ( 1805 , 1806 et 1814 ) et il faut attendre la dernière pour qu' enfin l' opéra reçoive un accueil à sa mesure .
Après 1805 , malgré l' échec retentissant de [MASK] , la situation de [MASK] est redevenue favorable .
L' ouverture [MASK] , avec laquelle elle partage la tonalité d' ut mineur , date de cette même époque .
Le concert donné par [MASK] le 22 décembre 1808 est sans doute une des plus grandes " académies " de l' histoire ( avec celle du 7 mai 1824 , voir plus bas ) .
Après la mort de [MASK] en mai 1809 , bien qu' il lui restât des adversaires déterminés , il ne se trouve plus guère de monde pour contester la place de [MASK] dans le panthéon des musiciens .
[MASK] accepte , voyant son espoir d' être définitivement à l' abri du besoin aboutir , mais la reprise de la guerre entre la [MASK] et l' [MASK] au printemps 1809 remet tout en cause .
La famille impériale est contrainte de quitter [MASK] occupée , la grave crise économique qui s' empare de l' [MASK] après [MASK] et le [MASK] [MASK] [MASK] imposé par [MASK] ruine l' aristocratie et rend caduc le contrat passé par [MASK] .
Le [MASK] [MASK] [MASK] [MASK] et la [MASK] [MASK] sont le point d' orgue de la période héroïque .
La vie sentimentale de [MASK] a suscité d' abondants commentaires de la part de ses biographes .
Le compositeur s' éprit à de nombreuses reprises de jolies femmes , le plus souvent mariées , mais jamais ne connut ce bonheur conjugal qu' il appelait de ses vœux et dont il faisait l' apologie dans [MASK] .
Le mois de juillet 1812 , abondamment commenté par les biographes du musicien , marque un nouveau tournant dans la vie de [MASK] .
Malgré l' accueil très favorable réservé par le public à la [MASK] [MASK] et à la [MASK] [MASK] [MASK] ( décembre 1813 ) , malgré la reprise enfin triomphale de [MASK] dans sa version définitive ( mai 1814 ) , [MASK] perd peu à peu les faveurs de [MASK] toujours nostalgique de [MASK] et acquise à la musique plus légère de [MASK] .
En outre , le durcissement du régime imposé par [MASK] le place dans une situation délicate , la police viennoise étant depuis longtemps au fait des convictions démocratiques et révolutionnaires dont le compositeur se cache de moins en moins .
Tandis que sa situation matérielle devient de plus en plus préoccupante , [MASK] tombe gravement malade entre 1816 et 1817 et semble une nouvelle fois proche du suicide .
Neuf ans avant la création de la [MASK] [MASK] , [MASK] résume en une phrase ce qui va devenir à bien des égards l' œuvre de toute sa vie ( 1815 ) :
Les forces de [MASK] reviennent à la fin de 1817 , époque à laquelle il ébauche une nouvelle sonate qu' il destine au piano-forte le plus récent , et qu' il envisage comme la plus vaste de toutes celles qu' il a composées jusque là .
À l' exception de la [MASK] [MASK] , il en est de même pour l' ensemble des dernières œuvres du maître , dont lui-même a conscience qu' elles sont très en avance sur leur temps .
Mais il lui reste à composer un ultime chef-d'œuvre pianistique : l' éditeur [MASK] [MASK] invite en 1822 l' ensemble des compositeurs de son temps à écrire une variation sur une valse très simple de sa composition .
Après s' être d' abord moqué de cette valse , [MASK] dépasse le but proposé et en tire un recueil de [MASK] [MASK] que [MASK] lui-même estime comparable aux célèbres [MASK] [MASK] de [MASK] , composées quatre-vingts ans plus tôt .
La symphonie est créée devant un public enthousiaste le 7 mai 1824 , [MASK] renouant un temps avec le succès .
C' est en [MASK] et en [MASK] , où la renommée du musicien est depuis longtemps à la mesure de son génie , que la symphonie connait le succès le plus fulgurant .
Plusieurs fois invité à [MASK] comme l' avait été [MASK] [MASK] , [MASK] a été tenté vers la fin de sa vie de voyager en [MASK] , pays qu' il admire pour sa vie culturelle et pour sa démocratie et qu' il oppose systématiquement à la frivolité de la vie viennoise , mais ce projet ne se réalisera pas et [MASK] ne connaitra jamais le pays de son idole [MASK] , dont l' influence est particulièrement sensible dans la période tardive de [MASK] , qui compose dans son style , entre 1822 et 1823 , l' ouverture [MASK] [MASK] [MASK] [MASK] [MASK] .
Par leur caractère visionnaire , renouant avec des formes anciennes ( utilisation du mode lydien dans le [MASK] [MASK] [MASK] [MASK] ) , ils marquent l' aboutissement des recherches de [MASK] dans la musique de chambre .
De retour à [MASK] en décembre 1826 , [MASK] contracte une double pneumonie dont il ne peut se relever : les quatre derniers mois de sa vie sont marqués par des douleurs permanentes et une terrible détérioration physique .
Quelques semaines avant sa mort , il aurait reçu la visite de [MASK] [MASK] , qu' il ne connaissait pas et qu' il regrette d' avoir découvert si tardivement .
Mais le 26 mars 1827 , [MASK] [MASK] [MASK] meurt à l' âge de cinquante-six ans .
Alors que [MASK] ne se souciait plus guère de son sort depuis des mois , ses funérailles , le 29 mars 1827 , réunissent un cortège impressionnant de plusieurs milliers d' anonymes .
[MASK] repose au cimetière de [MASK] .
La particularité de l' influence exercée par [MASK] -- par rapport , notamment , à celle exercée par [MASK] -- tient au fait qu' elle dépasse littéralement le simple domaine esthétique ( auquel elle ne s ' applique que momentanément et superficiellement ) pour imprégner bien davantage le fond même de la conception beethovénienne de la musique .
On distingue ainsi , dans le style de [MASK] , les aspects qui deviendront essentiels de l' esprit beethovénien .
Plus que tout , c' est le sens haydnien du motif qui influence profondément et durablement l' œuvre de [MASK] .
Jamais celle-ci ne connaîtra de principe plus fondateur et plus immuable que celui , hérité de son maître , de bâtir un mouvement entier à partir d' une cellule thématique réduite parfois jusqu' à l' extrême -- et les chefs-d'œuvre les plus célèbres en témoignent , à l' exemple du premier mouvement de la [MASK] [MASK] .
À la réduction quantitative du matériau de départ doit évidemment correspondre une extension du développement ; et si la portée de l' innovation apportée par [MASK] s' est révélée si grande , sur [MASK] et donc indirectement sur toute l' histoire de la musique , c' est justement parce que le motif haydnien a eu vocation à engendrer un développement thématique d' une ampleur jusqu' alors inédite .
Cette influence de [MASK] ne se limite pas toujours au thème ou même au développement de celui-ci , mais s' étend parfois jusqu' à l' organisation interne de tout un mouvement de sonate .
Ainsi en est-il par exemple , comme l' explique [MASK] [MASK] , du premier mouvement de la [MASK] [MASK] [MASK] [MASK] : c' est la tierce descendante du thème principal qui en détermine toute la structure ( on voit par exemple tout au long du morceau les tonalités se succéder dans un ordre de tierces descendantes : si bémol majeur , sol majeur , mi bémol majeur , si majeur … ) .
En dehors de ces aspects essentiels , d' autres caractéristiques moins fondamentales de l' œuvre de [MASK] ont parfois influencé [MASK] .
Même si l' on pourrait citer quelques rares exemples antérieurs , [MASK] est le premier compositeur à avoir véritablement fait usage d' une technique consistant à débuter un morceau dans une fausse tonalité -- c' est-à-dire une tonalité autre que la tonique .
Ce principe illustre bien la propension typiquement haydnienne à susciter la surprise de l' auditeur , tendance que l' on retrouve largement chez [MASK] : le dernier mouvement du [MASK] [MASK] [MASK] [MASK] , par exemple , semble commencer en ut majeur le temps de quelques mesures avant que ne s' établisse clairement la tonique ( sol majeur ) .
[MASK] est également le premier à s' être penché sur la question de l' intégration de la fugue dans la forme sonate , à laquelle il répond principalement en employant la fugue comme développement .
Dans ce domaine , avant de mettre au point de nouvelles méthodes ( qui n' interviendront que dans la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ) [MASK] reprendra plusieurs fois les trouvailles de son maître : le dernier mouvement de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et le premier de la [MASK] [MASK] [MASK] [MASK] en fournissent probablement les meilleurs exemples .
L' aspect formel -- et plus profond -- de l' influence de [MASK] se manifeste plutôt à partir des œuvres dites de la " deuxième période " .
C' est dans le concerto , genre que [MASK] a porté a son plus haut niveau , que le modèle du maître semble être demeuré le plus présent .
Ainsi , dans le premier mouvement du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , l' abandon de la double exposition de sonate ( successivement orchestre et soliste ) au profit d' une exposition unique ( simultanément orchestre et soliste ) reprend en quelque sorte l' idée mozartienne consistant à fondre la présentation statique du thème ( orchestre ) dans sa présentation dynamique ( soliste ) .
Plus généralement , [MASK] , dans sa propension à amplifier les codas jusqu' à les transformer en éléments thématiques à part entière , se pose bien plus en héritier de [MASK] que de [MASK] -- chez qui les codas se distinguent bien moins de la réexposition .
Dans le domaine de la musique pour piano , c' est surtout l' influence de [MASK] [MASK] qui s' exerce rapidement sur [MASK] à partir de 1795 et permet à sa personnalité de s' affirmer et s' épanouir véritablement .
Si elle n' a pas été aussi profonde que celle des œuvres de [MASK] , la portée des sonates pour piano du célèbre éditeur n' en a pas moins été immense dans l' évolution stylistique de [MASK] , qui les jugeait d' ailleurs supérieures à celles de [MASK] .
Certaines d' entre elles , par leur audace , leur puissance émotionnelle et le caractère novateur de leur traitement de l' instrument , inspirent quelques uns des premiers chefs-d'œuvre de [MASK] ; et les éléments qui , les premiers , permettent au style pianistique du compositeur de se distinguer proviennent pour une bonne part de [MASK] .
Ainsi , dès les années 1780 , [MASK] fait un emploi nouveau d' accords peu usités jusqu' alors : les octaves , principalement , mais aussi les sixtes et les tierces parallèles .
Il étoffe ainsi sensiblement l' écriture pianistique , dotant l' instrument d' une puissance sonore inédite , qui impressionne certainement le jeune [MASK] ; lequel va rapidement intégrer , dès ses trois premières sonates , ces procédés dans son propre style .
L' usage des indications dynamiques s' élargit dans les sonates de [MASK] : pianissimo et fortissimo y deviennent fréquents et leur fonction expressive prend une importance considérable .
Là aussi , [MASK] saisit les possibilités ouvertes par ces innovations ; et dès la [MASK] [MASK] [MASK] [MASK] , ces principes se voient définitivement intégrés au style beethovénien .
Un autre point commun entre les premières sonates de [MASK] et celles -- contemporaines ou antérieures -- de [MASK] est leur longueur , relativement importante pour l' époque : les sonates de [MASK] dont s' inspire le jeune [MASK] sont en effet des œuvres d' envergure , souvent constituées de vastes mouvements .
Les sonates pour piano de [MASK] sont connues pour avoir été en quelque sorte son " laboratoire expérimental " , celui duquel il tirait les idées nouvelles qu' il étendait ensuite à d' autres formes -- comme la symphonie .
Par elles , l' influence de [MASK] s' est donc exercée sur l' ensemble de la production beethovénienne .
Ainsi , comme le fait remarquer [MASK] [MASK] , on trouve par exemple des influences importantes des sonates op. 13 n o 6 et op. 34 n o 2 de [MASK] dans la [MASK] [MASK] .
Une fois les influences " héroïques " assimilées , après avoir véritablement pris le " nouveau chemin " sur lequel il souhaitait s' engager , et après avoir définitivement affirmé sa personnalité à travers les réalisations d' une période créatrice allant de la [MASK] [MASK] jusqu' à la [MASK] [MASK] , [MASK] cesse de s' intéresser aux œuvres de ses contemporains , et par conséquent d' être influencé par elles .
Parmi ses contemporains , seuls [MASK] et [MASK] l' enchantent encore ; mais en aucune manière il ne songe à les imiter .
Parmi ces influences , la place de [MASK] est plus que privilégiée : jamais sans doute n' eut-il de plus fervent admirateur que [MASK] ; qui ( désignant ses œuvres complètes , qu' il vient de recevoir ) s' écrie : " Voilà la vérité ! " ; ou encore [MASK] qui , au soir de sa vie , dit vouloir s ' " agenouiller sur sa tombe " .
De l' œuvre de [MASK] , la musique du dernier [MASK] prend souvent l' aspect grandiose et généreux , par l' emploi de rythmes pointés -- comme c' est le cas dans l' introduction de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , dans le premier mouvement de la [MASK] [MASK] ou encore dans la seconde [MASK] [MASK] -- ou même par un certain sens de l' harmonie , ainsi que le montrent les premières mesures du deuxième mouvement de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , entièrement harmonisées dans le plus pur style haendelien .
Enfin , c' est également dans le domaine de la fugue que l' œuvre de [MASK] imprègne [MASK] .
Si les exemples du genre écrits par l' auteur du [MASK] reposent sur une parfaite maîtrise des techniques contrapuntiques , elles se fondent généralement sur des thèmes simples et suivent un cheminement qui ne prétend pas à l' extrême élaboration de fugues de [MASK] .
C' est ce qui a dû satisfaire [MASK] , qui d' une part partage avec [MASK] le souci de construire des œuvres entières à partir d' un matériau aussi simple et réduit que possible , et qui d' autre part ne possède pas les prédispositions pour le contrepoint qui lui permettraient d' y chercher une excessive sophistication .
Dans l' histoire musicale , l' œuvre de [MASK] représente une transition entre l' ère classique ( approximativement 1750 -- 1810 ) et l' ère romantique ( approximativement 1810- 1900 ) .
[MASK] est aussi l' un des tout premiers à se pencher sur l' orchestration avec autant de soin .
Si les œuvres de [MASK] sont aussi appréciées , c' est également grâce à leur force émotionnelle , caractéristique du romantisme .
Le grand public connaît surtout ses œuvres symphoniques , souvent novatrices , en particulier les symphonies " impaires " ( 3 , 5 ,7 et 9 ) et la sixième , dite [MASK] .
[MASK] a composé plus de 100 symphonies et [MASK] plus de 40 .
De ses prédécesseurs , [MASK] n' a pas hérité de la productivité car il n' a composé que neuf symphonies , et en a ébauché une dixième .
Mais chez [MASK] , les neuf symphonies ont toutes une identité propre .
Curieusement , plusieurs compositeurs romantiques ou post-romantiques sont morts après leur neuvième ( achevée ou non ) , d' où une légende de malédiction attachée à ce chiffre : [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , mais aussi [MASK] [MASK] [MASK] .
Les deux premières symphonies de [MASK] sont d' inspiration et de facture classiques .
Cette œuvre monumentale , écrite au départ en hommage à [MASK] avant qu' il ne soit sacré empereur , révèle [MASK] comme un grand architecte musical et est considérée comme le premier exemple avéré de romantisme en musique .
La 6 e symphonie dite " [MASK] " , évoque à merveille la nature que [MASK] aimait tant .
Enfin , la [MASK] [MASK] est la dernière symphonie achevée et le joyau de l' ensemble .
Chacun d' eux est un chef-d'œuvre de composition qui montre que [MASK] s' est totalement affranchi des conventions classiques et fait découvrir de nouvelles perspectives dans le traitement de l' orchestre .
C' est à son dernier mouvement que [MASK] ajoute un chœur et un quatuor vocal qui chantent l' [MASK] [MASK] [MASK] [MASK] , un poème de [MASK] [MASK] [MASK] .
Cette œuvre appelle à l' amour et à la fraternité entre tous les hommes et la partition fait maintenant partie du patrimoine mondial de l' [MASK] .
A l' âge de 14 ans , [MASK] avait déjà écrit un modeste concerto pour piano en mi bémol majeur , resté inédit de son vivant .
Vers 1800 , il composa deux romances pour violon et orchestre ( [MASK] [MASK] et [MASK] [MASK] ) .
Mais [MASK] reste avant tout un compositeur de concertos pour piano , œuvres dont il se réservait l' exécution en concert .
Sauf pour le dernier , où sa surdité l' ayant complètement atteint , il a dû laisser son élève [MASK] le jouer le 28 novembre 1811 à [MASK] .
Contrairement aux concertos de [MASK] , ce sont des œuvres spécifiquement écrites pour le piano alors que [MASK] laissait la possibilité d' utiliser le clavecin .
Enfin [MASK] sera l' auteur d' un unique opéra , [MASK] , œuvre à laquelle il tiendra le plus , et certainement celle qui lui a coûté le plus d' efforts .
Bien que les symphonies soient ses œuvres les plus populaires et celles par qui le nom de [MASK] est connu du grand public , c' est certainement dans sa musique pour le piano ( ainsi que pour le quatuor à cordes ) que se distingue le plus le génie de [MASK] .
Traditionnellement , on dit que [MASK] a écrit 32 sonates pour piano , mais en réalité il existe 35 sonates pour piano totalement achevées .
Pour ce qui est des 32 sonates traditionnelles , œuvres d' importance majeure pour [MASK] puisqu' il a donné un numéro d' opus à chacune d' elle , leur composition s' échelonne sur une vingtaine d' années .
Dans la célèbre [MASK] ( 1819 ) , longueur et difficultés techniques atteignent des proportions telles qu' elles mettent en jeu les possibilités physiques de l' interprète comme celles de l' instrument , et exigent une attention soutenue de la part de l' auditeur .
Ce terme désigne un aboutissement stylistique de [MASK] , dans lequel le compositeur , désormais totalement sourd et possédant toutes les difficultés techniques de la composition , délaisse toutes considérations formelles pour ne s' attacher qu' à l' invention et à la découverte de nouveaux territoires sonores .
La " dernière manière " de [MASK] , associée à la dernière période de la vie du maître , désigne la manifestation la plus aiguë de son génie et n' aura pas de descendance .
Cependant , aucune de ces 5 pièces ne présentait une conclusion satisfaisante pour [MASK] et il composa donc une sixième bagatelle .
Lorsque [MASK] travaillait à ce recueil , il y avait 5 autres bagatelles achevées qui sont aujourd'hui restées seules à côté des trois recueils .
La plus connue , datant de 1810 , est la [MASK] [MASK] [MASK] .
Beaucoup d' autres petites pièces peuvent être considérées comme bagatelle en tant que telle , mais elles n' ont jamais fait partie d' un projet quelconque de [MASK] de les publier dans un recueil .
Mais le thème a bien initialement été composé pour [MASK] [MASK] [MASK] [MASK] ) et enfin le monument du genre , les [MASK] [MASK] opus 120 .
Elles furent composées en 1782 ( [MASK] avait 11 ans ) .
Avant son départ pour [MASK] en 1792 , [MASK] composa 3 autres séries .
Viennent ensuite les années 1795-1800 , au cours desquelles [MASK] composa pas moins de 9 séries .
C' est également à cette époque que [MASK] commença à utiliser des thèmes originaux pour ses séries de variations .
La première innovation se trouve dès le départ où , au lieu d' énoncer son thème , [MASK] en présente uniquement la ligne de basse en octaves , sans accompagnement .
La dernière période va de 1802 à 1809 où [MASK] composa 4 séries .
À partir de 1803 , [MASK] eut tendance à se concentrer sur des œuvres plus vastes .
Contrairement à ce que certains pourraient attendre , voulant voir cette série parmi les plus grandes œuvres de [MASK] , le compositeur la publia sans numéro d' opus et sans dédicataire .
Dix années s' écoulèrent avant que [MASK] n' aborde sa dernière série de variations .
Finalement , en 1822 , l' éditeur et compositeur [MASK] [MASK] eut l' idée de réunir en un recueil des pièces des compositeurs majeurs de son époque autour d' un unique thème musical de sa propre composition .
[MASK] , sollicité , et qui n' avait pas écrit pour le piano depuis longtemps , se prit au jeu , et au lieu de fournir une variation , en écrivit 33 , qui furent publiées dans un fascicule à part .
Les [MASK] [MASK] , de par leur invention , constituent le véritable testament de [MASK] pianiste .
Ce titre figurait sur le manuscrit original mais n' était pas de la main de [MASK] et on ne sait pas si celui-ci avait l' approbation du compositeur .
Autres pièces substantielles , l' [MASK] [MASK] en fa majeur et la fantaisie en sol mineur .
Ils existent deux autres rondos que [MASK] composa à l' âge de 12 ans environ .
[MASK] composa également des danses pour piano .
La première série de variations qui en compte huit est basée sur un thème du mécène [MASK] .
Enfin , [MASK] réalisa la transcription de sa " grande fugue " opus 133 pour duo de pianistes .
Il s' agissait à l' origine du finale du quatuor à cordes op. 130 mais les critiques furent tellement mauvaises que [MASK] se trouva contraint de réécrire un autre finale et l' éditeur eut l' idée de transcrire le finale original pour piano à 4 mains .
[MASK] n' écrivit pratiquement rien pour l' orgue .
Le grand monument de la musique de chambre de [MASK] est formé par les 16 quatuors à cordes .
C' est sans doute pour cette formation que [MASK] a confié ses plus profondes inspirations .
Le quatuor à cordes a été popularisé par [MASK] , [MASK] puis [MASK] , mais c' est [MASK] qui a le premier utilisé au maximum les possibilités de cette formation .
Les six derniers quatuors et la " [MASK] [MASK] " en particulier , constituent le sommet insurpassé du genre .
Depuis [MASK] , le quatuor à cordes n' a cessé d' être un passage obligé des compositeurs et un des plus hauts sommets fut sans doute atteint par [MASK] .
À côté des quatuors , [MASK] a écrit de belles sonates pour violon et piano , les premières étant l' héritage direct de [MASK] , alors que les dernières , notamment la " [MASK] [MASK] [MASK] " s' en éloignent pour être du pur [MASK] , cette dernière sonate étant quasiment un concerto pour piano et violon .
Cependant , après les concertos de [MASK] et l' importance du violoncelle dans la musique de chambre de [MASK] , c' est avec [MASK] que pour la première fois , le violoncelle est traité dans le genre de la sonate classique .
Les deux premières sonates sont composées en 1796 et dédiées au roi [MASK] [MASK] [MASK] [MASK] .
Ce sont des œuvres de jeunesse ( [MASK] avait 26 ans ) , qui présentent pourtant une certaine fantaisie et liberté d' écriture .
Dans le rondo final , une polyphonie distrubant un rôle différent aux deux solistes prend la place à l' imitation et la répartition égale des thèmes entre les deux instruments telle qu' elle était pratiquée à l' époque , notamment dans les sonates pour violon de [MASK] .
[MASK] ne recompose une autre sonate que bien plus tard , en 1807 .
[MASK] achève son parcours dans les sonates pour violoncelle en 1815 , dans les deux sonates opus 102 .
Un adagio conduit à une reprise variée de l' andante puis au finale allegro vivace , lui aussi de forme sonate , dont le développement et la coda font apparaître une écriture fuguée , une première pour [MASK] dans la forme sonate .
L' œuvre se conclut par une fugue à quatre voix dont la dernière partie présente une âpreté harmonique caractéristique des fugues de [MASK] .
De ces sonates se dégage la liberté avec laquelle [MASK] se détache des formules mélodiques et harmoniques traditionnelles .
Ils ont précédé la génération des quatuors et constituent les premières œuvres de [MASK] pour cordes seules .
Après [MASK] , le trio à cordes sera pratiquement délaissé .
Bien qu' ayant écrit des sonates pour piano et violon , piano et violoncelle , des quintettes ou des quatuors à cordes , [MASK] a aussi composé pour des ensembles moins conventionnels .
La majorité de ses œuvres ont été composées pendant ses jeunes années , période où [MASK] était toujours à la recherche de son style propre .
Le piano reste l' instrument de prédilection de [MASK] , et cela se ressent dans sa production de musique de chambre où l' on trouve de manière quasi systématique un piano .
[MASK] a également adapté lui-même une version pour piano et orchestre de son propre concerto pour violon et orchestre en ré majeur , opus 61
La vie de [MASK] a aussi inspiré plusieurs films , entre autres :