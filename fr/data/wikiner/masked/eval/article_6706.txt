Né le 19 décembre 1942 à [MASK] , où la guerre a temporairement conduit ses parents , [MASK] [MASK] passe la majeure partie de son enfance et de son adolescence à [MASK] , dans la banlieue sud de [MASK] .
Destiné par ses parents à une carrière d' enseignant , il abandonne à leur grand désarroi ses études à l' [MASK] [MASK] [MASK] sans terminer aucun diplôme , pour tenter de vivre de sa plume .
Il enseigne un semestre le français en [MASK] , dans un collège pour aveugles de [MASK] , puis revient en [MASK] .
Dans l' espoir d' y parvenir , il se lance dès 1965 dans une série de travaux alimentaires nombreux et variés : scénarios de courts métrages , écriture de synopsis , puis de deux films sexy pour [MASK] [MASK] .
C' est assez logiquement que [MASK] se tourne vers le roman noir , car il est déjà féru de ce genre littéraire et apprécie l' écriture " behavioriste " ou comportementaliste chère au romancier américain [MASK] [MASK] .
Neuf des onze romans de [MASK] seront édités dans la [MASK] [MASK] .
Ces livres marquent le coup d' envoi de ce que [MASK] lui-même baptisera par la suite le " néo-polar " , genre en rupture radicale avec la [MASK] [MASK] française des années 50/60 .
En s' appuyant sur la pensée situationniste , [MASK] utilise la forme du roman policier comme tremplin à la critique sociale : le roman noir renoue ici avec sa fonction originelle .
Le livre , refusé par la [MASK] [MASK] pour manque d' action , paraît hors collection chez [MASK] .
En novembre 2010 , [MASK] publiera une adaptation BD de cet ouvrage .
Dans les années qui suivent , cependant qu' il est régulièrement identifié par la presse comme le père spirituel du courant néo-polar , [MASK] ne publie plus de romans , mais il continue à écrire pour le cinéma ou la télévision , à traduire et à rédiger ses chroniques sur le roman policier .
À partir de 1973 , [MASK] travaille régulièrement pour le cinéma comme adaptateur , scénariste et dialoguiste .
Parmi ses travaux , [MASK] ( [MASK] [MASK] , 1973 ) adapté de son propre roman , [MASK] [MASK] ( [MASK] [MASK] , 1974 ) , [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] [MASK] , 1976 ) , [MASK] [MASK] [MASK] [MASK] ( [MASK] [MASK] , 1979 ) , [MASK] [MASK] ( [MASK] [MASK] , 1982 ) , [MASK] [MASK] ( [MASK] [MASK] , 1983 ) .
Il travaille occasionnellement pour la télévision , par exemple à la collection de fictions TV " [MASK] [MASK] " au début des années 80 , écrivant deux téléfilms de la série et presque tous les textes de présentation des épisodes , dits par le comédien [MASK] [MASK] qui incarne un détective privé .
Parallèlement à ses romans et à ses travaux pour le cinéma , [MASK] a également tenu un abondant journal intime de 1965 à 1995 , qui forme au final un ensemble de plus de cinq mille pages manuscrites .
Ces parutions sont venues confirmer l' importance de [MASK] dans le panorama littéraire français .