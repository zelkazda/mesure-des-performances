C' est le plus grand fabricant de semi-conducteurs du [MASK] .
En 2009 , la société est le troisième fabricants mondial de semiconducteurs , derrière [MASK] et [MASK] , mais devant [MASK] .
[MASK] est fondée à partir de la fusion de deux entreprises en 1939 .
L' incident mit sous tension les relations entre les [MASK] et le [MASK] et eut comme conséquence la poursuite et l' arrestation de deux cadres supérieurs , ainsi que la prise de sanctions sur la compagnie par les deux pays .
En 2001 , [MASK] signa un contrat avec [MASK] [MASK] , un des premiers OEM mondiaux dans le domaine de la fabrication d' équipements électroniques visuels , pour fabriquer et fournir le consommateur en matériel vidéo afin que [MASK] puisse satisfaire la demande croissante du marché [MASK] .
En décembre 2004 , [MASK] a annoncé qu' il arrêterait progressivement la fabrication de télévisions à tubes cathodiques .
La compagnie transféra rapidement à [MASK] la production des téléviseurs cathodiques et plasma de marque [MASK] .
Cependant , pour être présent et assurer sa future compétitivité sur le marché des écrans plats numériques , [MASK] a fait un investissement considérable dans une nouvelle génération de technologie d' affichage appelée [MASK] .
Avant la [MASK] [MASK] [MASK] , [MASK] était un membre du zaibatsu [MASK] .
Aujourd'hui , [MASK] appartient au keiretsu [MASK] ( ensemble d' entreprises , de domaines variés , entretenant entre elles des participations croisées ) , et a toujours bénéficié de rapports privilégiés avec la [MASK] [MASK] et les autres membres du keiretsu .