En 1959 , sa famille déménage à [MASK] ( dans l' [MASK] ) , puis à [MASK] en 1965 .
[MASK] est un enfant difficile , caractériel et indiscipliné .
En 1974 , à 19 ans , il part en voyage aux [MASK] , au titre de son service militaire , accompagné de son ami d' enfance [MASK] .
Ils y mènent finalement pendant cinq mois une vie d' artistes-aventuriers-routards-musiciens et sillonnent les routes en auto-stop avec leurs guitares , leurs répertoires de musique ( [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] , [MASK] [MASK] , etc .
De retour à [MASK] , il chante dans les boums et fêtes de son quartier ( le [MASK] [MASK] [MASK] [MASK] [MASK] ) et dans les universités , les grandes écoles , sur les campus .
[MASK] est impressionné et forme avec ses nouveaux amis le groupe [MASK] en 1975 .
[MASK] [MASK] [MASK] enterre rapidement le disque , aujourd'hui collector , et le groupe , ce qui déçoit profondément les intéressés .
[MASK] tente l' université de musicologie de [MASK] mais passe l' essentiel de son temps à jouer de la guitare dans une cave avec ses copains colocataires .
En novembre 1976 , [MASK] [MASK] loue une salle de concert , mais le groupe avec lequel il est supposé jouer n' est pas disponible .
Il réunit alors quelques amis qu' il apprécie et avec qui il joue depuis longtemps : [MASK] [MASK] , le guitariste [MASK] [MASK] et la bassiste [MASK] [MASK] .
C' est une énorme ovation pour le nouveau groupe qui prend le nom de [MASK] et qui devient rapidement le groupe phare du rock français des 1980 .
[MASK] [MASK] compose la majeure partie des chansons du groupe , joue de la guitare et chante .
[MASK] [MASK] [MASK] réalise à partir de 1989 un travail plus personnel et explore le monde de la musique .
Il servira de base à l' une des plus grosses tournées d' [MASK] , dont sera tirée le live [MASK] [MASK] [MASK] [MASK] en 1994 .
Comme à son habitude , [MASK] met le ton avec des musiques de plus en plus recherchées , textuellement et musicalement .
Un [MASK] sur moi-même , tournée de 4 mois à l' origine , durera finalement pratiquement une année , à la suite des demandes incessantes du public .
[MASK] [MASK] a donc su , durant toutes ces années de carrière , dépasser les modes et accrocher un public toujours présent .
Cette reconnaissance lui permet d' être un pilier , modeste certes , des [MASK] , aux concerts desquels il a participé de 1994 à 2010 .