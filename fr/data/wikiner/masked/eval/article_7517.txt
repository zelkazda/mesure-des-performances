À la [MASK] [MASK] [MASK] [MASK] , du temps de [MASK] [MASK] [MASK] , c' est-à-dire lors de l' achèvement du chœur en 1190 , il n' y avait point de chéneaux et de gargouilles ; plus tard , dans le même édifice , vers 1210 encore , les eaux des chéneaux s' écoulaient sur la saillie des larmiers , au moyen de rigoles ménagées de distance en distance .
Nous voyons apparaître les gargouilles , vers 1220 , sur certaines parties de la cathédrale de [MASK] .
Aussi est-ce à [MASK] , ou dans les contrées où l' on trouve des liais , comme à [MASK] par exemple , que l' on peut encore recueillir les plus beaux exemples de gargouilles .
Les gargouilles sont employées systématiquement à [MASK] vers 1240 ; c' est à [MASK] [MASK] que nous voyons apparaître , sur les corniches supérieures refaites vers 1225 , des gargouilles , courtes encore , robustes , mais taillées déjà par des mains habiles ( cf .
Voici ( figure 5 ) une de ces gargouilles à double fin , provenant des parties supérieures de la nef de la cathédrale d' [MASK] .
figure 6 ) ; ou elles sont appuyées sur la tête même de ces contre-forts , comme autour des chapelles du chœur de la cathédrale de [MASK] ( cf .
figure 10 , reproduction d' une gargouille de [MASK] [MASK] [MASK] [MASK] ) .
Là où les matériaux durs sont peu communs , comme en [MASK] , par exemple , les gargouilles sont courtes , rarement sculptées , ou manquent absolument , les eaux s' égouttant des toits sans chéneaux .
En voici une ( figure 11 ) qui se voit à l' angle d' une maison de [MASK] ; elle date du XV e siècle , et est faite en plomb repoussé .
Source : [MASK] [MASK] .
On les trouve par exemple en grand nombre sur les toits de la [MASK] [MASK] [MASK] [MASK] .
Elles ont été conçues par [MASK] [MASK] et sont le produit de son imagination féconde .