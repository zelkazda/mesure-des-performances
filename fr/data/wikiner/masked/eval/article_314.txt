Ancien protectorat allemand , le territoire a été placé sous la tutelle de la [MASK] [MASK] [MASK] à la fin de la [MASK] [MASK] [MASK] et confié à l' administration de la [MASK] et du [MASK] .
L' ancien territoire sous administration française accéda à l' indépendance sous l' appellation de [MASK] [MASK] [MASK] le 1 er janvier 1960 .
Le [MASK] et ses frontières actuelles résultent de la colonisation européenne , mais l' histoire de ses habitants remonte à bien plus longtemps .
La première allusion historique aux côtes camerounaises se trouverait dans le récit dit périple d' [MASK] , d' après un texte grec très controversé .
En fait , d' après les témoignages archéologiques , les [MASK] ne semblent pas être allés au sud d' [MASK] .
Les marins anglais adoptèrent ce nom en l' anglicisant , d' où le nom actuel de [MASK] .
Après les [MASK] viennent les [MASK] puis les [MASK] .
Chacun de ces deux pays imprimera sa marque à " son " [MASK] , [MASK] [MASK] adoptant la politique de l' assimilation et le [MASK] celle de l' indirect rule .
L' indépendance de la zone française est proclamée le 1 er janvier 1960 et la réunification a lieu l' année suivante avec la partie sud de la zone britannique , la partie nord ayant opté pour l' union avec le [MASK] .
Le [MASK] est une république de type présidentialiste .
Le pouvoir exécutif est concentré autour du président et contrôlé par [MASK] [MASK] depuis 1982 .
Le 11 octobre 2004 , [MASK] [MASK] est réélu à travers des élections très contestées à la présidence du pays au premier tour de scrutin et avec près de 75 % des voix .
Le 8 décembre 2004 , il y a un nouveau gouvernement avec à sa tête le premier ministre [MASK] [MASK] , qui est anglophone ( du sud-ouest ) comme le veut l' usage de l' équilibre [MASK] [MASK] [MASK] .
Le 10 avril 2008 , l' [MASK] [MASK] adopte le projet de loi sur la révision constitutionnelle avec 157 voix pour , 5 contre et 15 non votants .
Ce projet adopté est très critiqué par les partis politiques de l' opposition puisqu' il permet à [MASK] [MASK] de prétendre a un autre mandat à la fin de son mandat en 2011 .
Il n' est pas rare que les fils des dynasties royales , des lamidos ou de sultans exercent des responsabilités ministérielles à [MASK] .
Le [MASK] est un melting pot de plusieurs ethnies ( On en dénombre 280 ) avec quelques grands ensembles ( sémites , hamités , bantous , semi-bantous et soudanais ) et de nombreux métissages .
Le [MASK] est un pays du [MASK] [MASK] [MASK] , sur la façade occidentale de l' [MASK] .
Il possède 590 km de côtes , très découpées , le long de l' [MASK] [MASK] .
Le [MASK] est entouré des pays et étendues d' eau suivants :
Par sa superficie de 475442 km 2 et sa population d' environ 18 467 692 habitants ( 2008 ) , le [MASK] est un pays de taille moyenne en [MASK] .
Le long de ses 590 km de côtes , on compte quelques cités balnéaires : [MASK] , et [MASK] près du [MASK] [MASK] .
Le [MASK] dispose de ressources naturelles agricoles ( bananes , cacao , café , coton , miel ) , forestières , minière et pétrolière .
De 1965 à 1985 , le [MASK] a connu une croissance soutenue ( plus de 15 % par an en moyenne ) , portée par les prix des matières premières , et a longtemps été parmi les pays les plus prospères du continent africain .
Après une décennie de récession caractérisée par une forte baisse du PIB ( -30 % entre 1985 et 1993 ) et une chute de 40 % de la consommation par habitant , le [MASK] a renoué avec la croissance économique depuis 1994 avec une moyenne de 5 % par an .
[MASK] % de la main-d'œuvre urbaine travaillerait dans le secteur informel ( secteur du travail non déclaré et donc en principe à faibles revenus ) et 6 ménages sur 10 tireraient au moins une partie de leurs revenus de ce secteur informel .
Le [MASK] conserve trois atouts : une production agro-alimentaire autosuffisante à 95 % , une industrie du bois et d' hydrocarbures performante et une production d' aluminium assise sur d' importantes réserves de bauxite .
Malgré son potentiel naturel , minéral et humain énorme , le [MASK] souffre encore aujourd'hui de plusieurs maux qui empêchent un véritable décollage économique : la corruption , une production énergétique déficitaire par rapport à la demande , des finances publiques insuffisamment assainies , une attractivité pour des investissements de capitaux privés et étrangers en retrait par rapport à d' autres pays , une lourdeur administrative souvent handicapante .
La population du [MASK] est estimée en 2008 à 18 millions d' habitants .
En 2001 , 6 villes dépassaient le seuil des 200000 habitants : [MASK] ( la capitale économique , 1,5 million d' habitants en 2001 ) , [MASK] ( la capitale politique et siège des institutions , environ 1,25 million d' habitants en 2001 ) , [MASK] ( environ 357000 habitants en 2001 ) , [MASK] ( environ 316000 habitants en 2001 ) , [MASK] ( environ 272000 habitants en 2001 ) et [MASK] ( environ 242000 habitants en 2001 ) .
Le [MASK] compterait au total une vingtaine de villes ayant au moins 50000 habitants .
Selon les résultats du dernier recensement , le [MASK] compte toujours un peu plus de femmes ( 50,6 % ) que d' hommes ( 49,4 % ) .
Au [MASK] l' on retrouve différents groupes socio-culturelle .
Le [MASK] , état laïque , regroupe 3 religions principales :
La santé ainsi que le développement humain au [MASK] sont encore très bas .
En 2008 , on dénombrait plus de 140000 étudiants au [MASK] .
Au [MASK] , l' entrée à l' école maternelle en général se fait à l' âge de trois ans .
Il est à noter qu' au [MASK] , le terme " lycée " désigne un établissement public , tandis que le qualificatif " collège " est attribué à un établissement privé .
La rentrée scolaire au [MASK] a lieu traditionnellement le premier lundi de septembre ( sauf si celui-ci est le 1 er septembre ) .
Ce qui fait que contrairement à la majorité des pays africains , le [MASK] n' a pas de langue régionale dominante ou commune .
En ce qui concerne les langues officielles , le français ( 80 % de la population étant francophone ) et l' anglais ( parlé dans deux subdivisions administratives limitrophes du [MASK] anglophone ) sont les deux langues de l' administration , de l' enseignement et des médias .
Ce bilinguisme est un héritage de la colonisation et permet au [MASK] de faire à la fois partie du monde francophone et anglophone .
Le [MASK] compte une douzaine de journaux quotidiens .
Le [MASK] possède ( officiellement ) 50000 km de routes , dont 6000 bitumées .
Il possède aussi un port fluvial saisonnier à [MASK] .
Le projet de construction d' un port en eau profonde à [MASK] est en cours d' exécution .
Le [MASK] a 3 aéroports internationaux et une quarantaine d' aéroports secondaires ( pas toujours utilisés ) .
Les [MASK] adorent le football .
Le [MASK] possède peu d' infrastructures sportives .
Le [MASK] a aussi obtenu d' excellents résultats en boxe ( les 2 premières médailles olympiques du pays sont venu de ce sport , en 1968 et 1984 ) , en handball ( compétitions nationales comme en club , avec de nombreux trophées ) , en volley-ball ( plusieurs fois champions d' [MASK] ) , en basketball ( vice-champion d' [MASK] en 2007 ) , en haltérophilie .
En athlétisme , la triple sauteuse [MASK] [MASK] [MASK] est double championne olympique de sa discipline ( 2004 et 2008 ) .
Il est prévu la construction sur la période 2008-2018 de plusieurs stades , piscines ou palais omnisports à [MASK] , à [MASK] , à [MASK] , [MASK] ainsi que dans d' autres villes du pays .
Ces travaux sont financés par la [MASK] [MASK] [MASK] [MASK] et construits par des entreprises chinoises .
Le tourisme n' est encore que très peu développé au [MASK] .
Pour cela , le gouvernement a noué des liens de coopération en ouvrant des bureaux touristiques dans les grandes villes européennes telles que [MASK] , [MASK] et [MASK] .
Ces derniers ont pour but de vanter le " produit " [MASK] à l' étranger afin d' inciter des voyageurs à venir .
Le [MASK] possède deux sites naturels classés au patrimoine mondial par l' [MASK] : la [MASK] [MASK] [MASK] [MASK] [MASK] et le [MASK] [MASK] [MASK] [MASK] .
La corruption existe au [MASK] comme dans tous les pays du monde .
Le [MASK] a pour codes :