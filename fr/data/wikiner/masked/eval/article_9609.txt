Toutefois la société [MASK] qui recense les gratte-ciel de la planète utilise la limite inférieure de 100 mètres pour caractériser un gratte-ciel .
Par ailleurs ne sont pas considérés comme gratte-ciel les tours comme la [MASK] [MASK] car il s' agit d' une tour d' observation et non pas d' un immeuble constitué d' une juxtaposition d' étages .
La [MASK] [MASK] [MASK] , dont la hauteur atteignait presque 150 mètres était un tombeau .
C' est ainsi que [MASK] [MASK] [MASK] [MASK] fut amené à élaborer un système de structure interne sur laquelle repose tout l' édifice , le mur extérieur n' ayant plus rien à porter .
Il tira également parti de l' invention de l' ascenseur mécanique et notamment de l' ascenseur de sécurité par [MASK] [MASK] .
C' est une vraie course au plus haut building qui commence : [MASK] [MASK] [MASK] [MASK] ( 1894 , 106 mètres ) , le [MASK] [MASK] [MASK] ( 1899 , 119 mètres ) , puis la [MASK] [MASK] [MASK] franchit la barre des 200 m en 1909 mais est finalement dépassée par le [MASK] [MASK] ( 1913 , 241 mètres ) .
Le mouvement se poursuit après la [MASK] [MASK] [MASK] par le [MASK] [MASK] [MASK] mais surtout par le [MASK] [MASK] puis l' [MASK] [MASK] [MASK] qui atteint 381 mètres , en 1931 .
[MASK] par la crise économique des années 1930 , le mouvement de construction de gratte-ciel reprend dans les années 1960 , à [MASK] [MASK] et à [MASK] et , à un moindre niveau , dans d' autres villes du monde .
C' est en [MASK] , dans des régions à forte croissance que le développement est le plus spectaculaire .
La [MASK] [MASK] , inaugurée en 2004 à [MASK] était , à l' époque de sa construction , le plus haut gratte-ciel achevé du monde .
La plus haute tour terminée en 2008 est la [MASK] [MASK] [MASK] [MASK] de [MASK] , mesurant 492 m .
Dans les zones hautement sismiques , comme [MASK] [MASK] , la construction d' immeubles de grande hauteur pose de redoutables problèmes de sécurité .
Les gratte-ciel de [MASK] [MASK] construits durant les années 1890 existent toujours ( du moins les plus hauts ) .
Certains des gratte-ciel qui seront construits à [MASK] [MASK] en seront l' illustration .
Selon [MASK] [MASK] , architecte américain et l' un des principaux concepteurs de gratte-ciels , la construction d' un bâtiment de très grande hauteur est donc difficilement voire rarement rentable .
En effet , la construction d' une très grande tour donne de la valeur au quartier environnant comme cela s' est produit par exemple avec le secteur de [MASK] à [MASK] ( [MASK] [MASK] [MASK] ) ou à [MASK] [MASK] à [MASK] .
Ce fut le cas de la [MASK] [MASK] à [MASK] ou des [MASK] [MASK] à [MASK] [MASK] .
Lors de la crise financière d' automne 2008 les travaux ont ainsi été interrompus sur la [MASK] [MASK] [MASK] à [MASK] et au [MASK] [MASK] à [MASK] .
L' achèvement des tours du [MASK] [MASK] [MASK] et de la [MASK] [MASK] aux [MASK] en 1973 / 1974 coïncide avec le début de la crise économique des années 1970 .
La construction de la plus haute tour du monde à [MASK] en 2010 à coïncidé a un mois près à une grave crise économique dans la ville-état .
Le gratte-ciel était devenu un symbole des [MASK] , son pays d' origine .
[MASK] et [MASK] [MASK] sont aujourd'hui comme hier les deux villes du continent où la densité de gratte-ciel est la plus élevée mais la plupart des grandes villes possèdent désormais un CBD comprenant plusieurs tours relativement hautes .
Le rythme de constructions nouvelles est actuellement bien inférieur à celui de [MASK] [MASK] , mais assez soutenu tout de même , et ce en dépit du traumatisme qu' a représenté la destruction du [MASK] [MASK] [MASK] .
Les constructions se concentrent aujourd'hui dans les agglomérations de [MASK] , [MASK] , [MASK] [MASK] et en [MASK] .
Depuis le début des années 1990 , l' agglomération de [MASK] ( [MASK] ) connait ainsi un important renouvellement urbain .
Le grand magnat de l' immobilier , [MASK] [MASK] est assez présent dans ces constructions avec des associés locaux .
[MASK] est désormais l' agglomération américaine qui comprend le plus de gratte-ciel après [MASK] [MASK] et [MASK] et devant [MASK] .
Les gratte-ciel de l' agglomération de [MASK] sont souvent très récents , sur les 50 immeubles les plus hauts de l' agglomération , 43 ont été construits depuis l' an 2000 .
De nombreux gratte-ciel sont également en construction ou en projet à [MASK] [MASK] , une ville en plein boom .
Le plus haut immeuble de la ville est [MASK] [MASK] inauguré en 2007 et qui mesure près de 200 mètres .
Certains approchent ou dépassent les 300 mètres de hauteur comme c' est le cas de la [MASK] [MASK] [MASK] ( [MASK] ) .
À un moment [MASK] [MASK] [MASK] , l' autorité fédérale américaine de l' aviation civile , a interdit la construction d' immeubles de plus de 600 mètres .
[MASK] [MASK] l' un des plus grands promoteurs mondiaux de gratte-ciel qui envisageait la construction d' un immeuble de cette hauteur avant les attentats du 11 septembre a abandonné de peur que cette nouvelle tour ne devienne une cible .
Dans l' ensemble beaucoup de gratte-ciel américains se caractérisent par une très grande recherche esthétique comme c' est par exemple le cas de la [MASK] [MASK] ( [MASK] ) , et des représentants du style de toutes les époques .
En décembre 2009 les agglomérations des [MASK] ou il y avait le plus de gratte-ciel d' une hauteur supérieure ou égale à 150 mètres sont ;
Au [MASK] , bien que l' espace ne manque pas , de nombreuses villes ont choisi de se développer verticalement .
Au [MASK] , malgré le risque sismique de nombreux gratte-ciel ont été construits récemment dans la capitale [MASK] ainsi qu' à [MASK] .
Le plus haut gratte-ciel du [MASK] est la [MASK] [MASK] haute de 225 mètres .
[MASK] [MASK] est l' une des villes qui comportent le plus de gratte-ciel au monde .
À [MASK] , il y a une extraordinaire frénésie de construction .
La ville de [MASK] accueille en effet des retraités d' [MASK] [MASK] [MASK] qui veulent profiter du soleil et d' une monnaie indexée sur le dollar .
À noter que son concepteur est [MASK] [MASK] , l' un des plus grands architectes de la planète , qui est d' origine argentine .
La plupart des gratte-ciel se situent dans la capitale [MASK] .
C' est à [MASK] [MASK] et au [MASK] que furent créés les premiers très hauts immeubles de la région .
Au [MASK] , le manque de place a poussé à la construction en hauteur , mais les risques sismiques imposaient d' importantes contraintes techniques .
[MASK] [MASK] est historiquement la ville du gratte-ciel dans le monde chinois , et est la ville comptant le plus d' immeubles de très grande hauteur dans le monde .
Le plus haut gratte-ciel de la ville est le [MASK] [MASK] [MASK] [MASK] de 415 m de hauteur .
Le mouvement de construction continue à [MASK] [MASK] mais il s' est surtout étendu à d' autres villes chinoises , où il est stimulé par forte croissance urbaine et l' expansion économique .
À l' exception de ceux situés à [MASK] aucun d' entre eux n' a plus de 30 ans d' âge .
Certains projets importants sont actuellement à l' étude en [MASK] [MASK] [MASK] où la plupart des gratte-ciel se concentrent à [MASK] une des villes de la planète qui en comporte le plus .
En [MASK] du nord se trouve l' un des bâtiments les plus haut de la planète , l' [MASK] [MASK] haut de 330 mètres sur 105 étages mais qui n' a jamais été achevé .
À [MASK] , les gratte-ciel se concentrent essentiellement dans les deux plus grandes villes du pays ; la capitale [MASK] et [MASK] dans le sud du pays .
À [MASK] se trouve le [MASK] [MASK] qui a son inauguration en 2004 était le plus haut gratte-ciel du monde .
À [MASK] figure un gratte-ciel exceptionnel la [MASK] [MASK] [MASK] de 348 m de hauteur .
Aux [MASK] , la quasi totalité des nombreux gratte-ciel se concentrent dans la capitale [MASK] et notamment le quartier d' affaires de [MASK] .
Au [MASK] , il y a plusieurs dizaines de gratte-ciel tous très récents qui se concentrent essentiellement à [MASK] et à [MASK] .
C' est dans cette ville que se trouve la [MASK] [MASK] [MASK] [MASK] [MASK] actuellement en construction et qui avec ses 336 mètres sera la tour la plus haute du pays .
En [MASK] , la plupart des gratte-ciel se concentrent à [MASK] où ils sont particulièrement nombreux .
La tour la plus haute du pays est la [MASK] [MASK] [MASK] haute de 304 mètres ( hors antenne ) .
La [MASK] a détenu pendant plusieurs années le titre du plus grand gratte-ciel , avec ses tours jumelles , les [MASK] [MASK] [MASK] à [MASK] [MASK] , construites en 1998 .
Les gratte-ciel se concentrent à [MASK] [MASK] , la capitale , et dans l' ile de [MASK] où sont présentes de très nombreuses société étrangères .
À [MASK] , comme à [MASK] , la forte densité de population a imposé la construction d' immeubles de grande hauteur .
En [MASK] , l' écrasante majorité des gratte-ciel se concentrent à [MASK] où ils se comptent par dizaines .
La plus haute tour du pays est la [MASK] [MASK] , haute de 250 mètres .
Ce sont les [MASK] [MASK] [MASK] , et notamment [MASK] , qui ont le plus retenu l' attention .
Fin 2009 [MASK] comptera 15 immeubles de plus de 300 mètres plus 7 en construction .
Parmi ceux-ci se trouve le [MASK] [MASK] qui est le plus haut édifice du monde du haut de ses 828 m .
Beaucoup de gratte-ciel de [MASK] se caractérisent par une très grande recherche esthétique et certains seront très originaux .
[MASK] a servi d' exemple aux autres émirats tels que [MASK] [MASK] , [MASK] , [MASK] , [MASK] [MASK] [MASK] , [MASK] qui eux aussi se sont lancés dans la construction d' immeubles de grandes hauteurs , bien qu' aucun d' entre eux n' atteignent les dimensions spectaculaires des plus hauts bâtiments de [MASK] .
Cependant les constructions à [MASK] ont été beaucoup trop nombreuses ces dernières années .
La densité de gratte-ciel par habitants à [MASK] est aujourd'hui la plus élevée du monde après [MASK] .
D' après la société [MASK] en décembre 2009 , sur les 100 plus haut immeubles en construction dans le monde , 27 le sont à [MASK] ce qui est beaucoup pour une ville de moins de 2 millions d' habitants .
Du fait de la crise financière qu' a connu [MASK] en décembre 2009 la construction de beaucoup de tours s' est interrompue , le taux de vacance des immeubles atteint un niveau très elevé , le prix des appartements a très fortement chuté .
Dans ces conditions il y a peu de chance que soit lancée la construction de beaucoup de nouvelles tours à [MASK] .
[MASK] est la ville du golfe qui a le plus de gratte-ciel après [MASK] .
En [MASK] [MASK] , des gratte-ciel d' une beauté spectaculaire ont été construits ou sont en construction tels que le [MASK] [MASK] à [MASK] ou l' [MASK] [MASK] [MASK] hotel à [MASK] [MASK] .
En [MASK] , la forte densité d' habitants a imposé la construction de plusieurs dizaines d' immeubles de très grande hauteur .
La plupart se situent dans l' agglomération de [MASK] [MASK] où se trouve , à [MASK] [MASK] , la plus haute tour du pays , la [MASK] [MASK] [MASK] [MASK] , haute de 244 mètres .
Au [MASK] , à [MASK] les très nombreuses destructions durant la guerre civile ont imposé la reconstruction de la capitale .
L' [MASK] ne compte que deux des cinquante plus hauts gratte-ciel mondiaux ( tous deux situés a [MASK] ) .
L' ancien détenteur du titre était le [MASK] , également à [MASK] .
On observe actuellement un nouvel intérêt pour les gratte-ciel dans les grandes villes européennes , notamment à [MASK] , [MASK] , [MASK] , [MASK] , [MASK] .
Toujours dans les années 1930 , l' architecte [MASK] [MASK] développa des projets de gratte-ciel dans l' [MASK] [MASK] [MASK] [MASK] à [MASK] .
En 1952 , fut achevée la [MASK] [MASK] à [MASK] .
Au début des années 1970 , dans [MASK] même , de très nombreuses tours sont lancées dans les 13 e , 14 e , 15 e , 17 e , 19 e arrondissements .
Tours d' habitations essentiellement , mais aussi tours de bureaux comme la [MASK] [MASK] achevée en 1972 , qui , avec ses 210 mètres de hauteur , fut à son époque le plus haut bâtiment d' [MASK] .
Les autorités en limitèrent les possibilités de construction dans [MASK] même .
Dans le centre d' affaires de [MASK] [MASK] , un plan de relance de la construction de tours a été lancé en 2006 .
De la sorte [MASK] [MASK] devrait voir son parc de gratte-ciel connaître une grande croissance avec en effet une dizaine de gratte-ciel de plus de 150 mètres mètres de hauteur en projet , dont quatre géants dépassant les 300 mètres .
En province , seule la ville de [MASK] compte un immeuble de plus de 150 mètres de hauteur , la [MASK] [MASK] [MASK] [MASK] ( [MASK] [MASK] ) haute de 165 mètres inaugurée en 1977 .
Une tour de 115 mètres de hauteur la [MASK] [MASK] est en construction ( inauguration en 2010 ) et trois tours de plus de 100 mètres sont en projet dont une de 200 mètres de hauteur : La [MASK] [MASK] .
D' autres tours de grandes envergures sont également en projet dans le cadre du projet [MASK] .
On trouve des tours de plus de 100 mètres également à [MASK] ( [MASK] ) , [MASK] , [MASK] .
Il y a des projets de ce type à [MASK] .
Parmi les gratte-ciel emblématique de la ville , la [MASK] ( tour de la foire ) conçue par l' architecte d' origine allemande [MASK] [MASK] et la [MASK] [MASK] qui , à sa construction était , avec ses 259 m , la plus haute tour de l' [MASK] .
Beaucoup de ces tours abritent des banques , des compagnies d' assurances et des compagnies financières liées à la bourse de [MASK] , l' une des plus importantes d' [MASK] , après [MASK] et [MASK] .
Au [MASK] , les tours se situent essentiellement à [MASK] , ou , depuis le lancement du quartier [MASK] [MASK] , au début des années 1990 , leur construction a été relancée .
Ces dernières années des gratte-ciel ont été également construits dans des villes moyennes comme [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] .
En [MASK] , le boom immobilier de ces dernières années a entrainé la construction de nombreux d' immeubles de grande hauteur , notamment dans les cités balnéaires telles que [MASK] , [MASK] ainsi que dans la capitale [MASK] où quatre tours de plus de 200 mètres ont été inaugurés en 2007 et 2008 dont la plus haute du pays la [MASK] [MASK] [MASK] .
En [MASK] , il y a peu de gratte-ciel .
Ils se concentrent à [MASK] et à [MASK] .
À [MASK] des gratte-ciel ont été construit dans les années 1950 à une époque où on en construisait très peu dans le monde .
[MASK] est désormais la ville d' [MASK] qui compte le plus de gratte-ciel avec en 2009 une quarantaine de tours dépassant les 150 mètres et plus de 200 immeubles de plus de 100 mètres de hauteur la plupart construits depuis l' an 2000 .
Les deux plus haut gratte-ciel d' [MASK] sont d' ailleurs situés à [MASK] .
Ce sera la tour la plus haute d' [MASK] à son achèvement en 2009 .
Dans les autres grandes villes , de nombreux projets sortent de terre , à [MASK] [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] .
D' autres gratte-ciel perpétuent le style stalinien comme c' est le cas du [MASK] construit en 2005 ou de l' [MASK] construit en 2003 .
Dans la partie européenne de la [MASK] à [MASK] ont été construits depuis les années 1990 un très grand nombre de gratte-ciel dont la [MASK] [MASK] [MASK] haute de 181 mètres , le [MASK] [MASK] .
En [MASK] , plusieurs gratte-ciel sont également construits , notamment à [MASK] et au [MASK] , mais aucun ne dépasse les 150 mètres .
À [MASK] en [MASK] [MASK] [MASK] , plusieurs tours de plus de 25 étages ont été construites durant les années 1980 .
[MASK] [MASK] du sud est de loin le pays d' [MASK] qui compte le plus de gratte-ciel .
Il y a de très nombreux gratte-ciel en [MASK] du fait de la concentration de la population dans quelques villes millionnaires .
La tour la plus haute du pays ( hors antenne ) est la [MASK] [MASK] , haute de près de 300 mètres sur 91 étages , située à [MASK] .
[MASK] comprend une centaine d' immeubles de 100 mètres de hauteur et plus .
Il faut noter la présence de nombreux gratte-ciel dans la [MASK] [MASK] [MASK] [MASK] au nord de [MASK] , qui comprend l' une des tours les plus haute du pays , la [MASK] [MASK] , qui comprend 78 étages ( 276 mètres de hauteur hors antenne ) .
En [MASK] , la plupart des gratte-ciel sont concentrés dans la plus importante ville du pays , [MASK] .
La tour la plus haute du pays est le [MASK] [MASK] haute de 167 mètres .
La société [MASK] spécialisé dans le recensement des gratte-ciel , indique les chiffres suivants pour le nombre de gratte-ciel dans le monde le 9 février 2010 , gratte-ciel que cette société défini comme immeuble d' une hauteur supérieure ou égale à 100 mètres :
Pour les villes asiatiques les chiffres d' [MASK] ne sont pas cohérents avec les chiffres de sa base de donnée .
Suivant ce critère , [MASK] était en 2009 la ville où les constructions étaient les plus nombreuses ( et aussi les plus ambitieuses ) , suivie de [MASK] .
Les perspectives de construction sont un peu assombris par la crise de l' immobilier ( août 2008 ) mais elles sont très favorables en [MASK] et surtout en [MASK] ou il y a encore très peu de gratte-ciel vue la taille du pays .
En [MASK] , les pays situés à l' est doivent rapidement s' équiper en immeubles de bureaux modernes et les pays de la moitié ouest comme [MASK] [MASK] développent de nouveaux centres urbains tels que [MASK] [MASK] .
L' exemple-type du gratte-ciel à flèche est le [MASK] [MASK] à [MASK] [MASK] .
Cette distinction , qui peut paraître inutile , a en fait pris toute son importance en 1997 , lorsque les [MASK] [MASK] à [MASK] [MASK] ont ravi le titre de plus haut gratte-ciel du monde à la [MASK] [MASK] de [MASK] .
La taille officielle des premières est de 452 m contre 442 m pour la [MASK] [MASK] .
Mais , pour les [MASK] [MASK] , il s' agit de la hauteur de la flèche structurale , le toit s' élevant en fait à 410 m .
Alors que pour la [MASK] [MASK] , c' est bien le toit qui s' élève à 442 m , les deux immenses antennes ( qui culminent à 527 m ) ne faisant pas partie de la structure .
Ainsi , si l' on mettait ces trois tours côte à côte , un spectateur aurait , au premier coup d' œil , l' impression que la [MASK] [MASK] est bien plus grande .
On pourra aisément le constater , la [MASK] investit massivement dans la construction de nouveaux gratte-ciels , notamment dans le quartier de [MASK] [MASK] , leader européen dans sa catégorie .
[MASK] est ainsi la ville de l' [MASK] [MASK] comptant le plus de gratte-ciels ( voir [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] ) .
Parallèlement , le [MASK] , et notamment la ville de [MASK] cherchent à relancer la construction d' immeubles de grande hauteur , notamment dans [MASK] [MASK] , et bien plus récemment , dans les rues rutilantes de son nouveau quartier d' affaires , [MASK] [MASK] .
[MASK] et [MASK] se sont également lancées dans la construction de tours , sans oublier [MASK] , ville comptant le plus de gratte-ciels d' [MASK] .
Il serait également impossible d' aborder le sujet des gratte-ciels européens sans parler de la démarche environnementale qui accompagne la tour , du développement à l' exploitation sur le long terme , notamment dans le cadre du désormais incontournable label " HQE " , garant de la propreté des quartiers d' affaires du [MASK] [MASK] et d' ailleurs .
L' [MASK] [MASK] [MASK] , pionnière en matière de gratte-ciels , est toujours fidèle à son image de continent novateur , pionnier dans ce domaine .
On ne peut en effet pas parler de gratte-ciels sans penser à des villes comme [MASK] , [MASK] [MASK] , [MASK] [MASK] ou [MASK] .
[MASK] étant aujourd'hui un quartier sans plus aucun espace libre et le bord du [MASK] [MASK] , aujourd'hui très encombré , de nouveaux quartiers d' affaires surgissent , ou alors , de massifs plan de destructions-reconstructions , de rénovations , font prendre de la hauteurs à ces quartiers d' affaires déjà resplendissants .
On peut citer pour l' exemple , que , bien que par la force des choses , la [MASK] [MASK] de [MASK] [MASK] aura bientôt remplacé ( 2012 ) l' emplacement des anciennes [MASK] [MASK] ( [MASK] [MASK] ) du [MASK] [MASK] [MASK] .
En somme , les [MASK] et le [MASK] sont toujours d' actualité en matière de buildings et conserveront probablement leur leadership longtemps encore au vu des projets qui suivent .
En effet , nous n' oublierons pas de mentionner le rutilant quartier d' affaire de [MASK] , qui s' équipe de tours dépassant les 200 mètres .
L' exemple type est [MASK] , qui , non loin du désert , connaît une véritable colonisation , cette fois-ci celle des tours , du tertiaire .
Les pays émergents d' [MASK] tendent à construire des gratte-ciels imposants , dignes des plus grandes nations industrialisées .
Ainsi se développent , en [MASK] ou en [MASK] , pour ne citer que les cas les plus flagrants , de respectables quartiers d' affaires où les tours fleurissent .
En outre , il serait utile d' ajouter que des villes telles [MASK] se tournent aujourd'hui essentiellement sur le tourisme de masse pour réorienter leur économie ( jusque lors portée totalement par les hydrocarbures ) , et ainsi que les hôtels les plus grands du monde s' y trouvent .