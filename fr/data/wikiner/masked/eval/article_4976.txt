Le drapeau de la [MASK] a été officiellement adopté en 1861 .
Il s' agissait à l' origine du drapeau de la [MASK] [MASK] [MASK] , dont faisaient aussi partie de 1806 à 1830 l' [MASK] et le [MASK] .
Il représente l' [MASK] ( jaune ) et l' [MASK] ( rouge ) , séparées par l' [MASK] [MASK] ( bleu ) .