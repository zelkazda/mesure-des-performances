Après des études de violon , de composition et de direction d' orchestre au [MASK] [MASK] [MASK] de 1925 à 1929 où il est élève de [MASK] [MASK] , il est remarqué par le grand chef tchèque [MASK] [MASK] dont il va devenir le disciple .
[MASK] [MASK] en fait son assistant de 1929 à 1931 à [MASK] puis à [MASK] .
Il commence aussi à diriger l' [MASK] [MASK] [MASK] de [MASK] [MASK] .
Il est déporté au [MASK] [MASK] [MASK] [MASK] [MASK] qui après la [MASK] [MASK] [MASK] ( 1942 ) devient un ghetto pour les juifs âgés , ceux qui ont participé à la [MASK] [MASK] [MASK] ou des personnalités juives qui ne peuvent s' exiler facilement .
Après ce tournage , leur vie va devenir plus difficile : beaucoup vont être internés à [MASK] ou dans les camps de l' est .
En cette période de [MASK] [MASK] il faut cependant attendre 1956 pour que l' orchestre puisse jouer à l' étranger .
Il dirige une dernière fois l' [MASK] [MASK] [MASK] au printemps 1969 .
Les plus grands solistes vont jouer avec lui à [MASK] .
Il nous a donné plusieurs interprétations de référence : citons [MASK] [MASK] [MASK] [MASK] de [MASK] en 1963 , une version époustouflante , de très belles réussites chez [MASK] et sa vision foudroyante de [MASK] [MASK] [MASK] de [MASK] .