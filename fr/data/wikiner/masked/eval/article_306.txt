[MASK] [MASK] [MASK] surnomma ainsi le château la " maison des siècles " , évoquant par là les souvenirs historiques dont les lieux sont le témoignage .
Depuis 1981 , le château fait partie avec son parc du patrimoine mondial de l' [MASK] .
Un château-fort est mentionné à cet emplacement pour la première fois en 1137 dans une charte de [MASK] [MASK] [MASK] [MASK] .
La date exacte de la fondation du château reste inconnue , mais le premier édifice a probablement été construit sous le règne du père de [MASK] [MASK] , [MASK] [MASK] , voire sous celui de son grand-père , [MASK] [MASK] [MASK] , lorsqu' il réunit le [MASK] au domaine royal français en 1068 .
À la [MASK] 1191 , [MASK] fête à [MASK] le retour de la troisième croisade .
[MASK] [MASK] [MASK] naît au château en 1268 et fait aménager des appartements en 1286 .
En 1313 , [MASK] [MASK] [MASK] , propriétaire du domaine de [MASK] , épouse [MASK] [MASK] [MASK] , futur [MASK] [MASK] , qui y fera des séjours fréquents .
En janvier 1332 , a lieu à [MASK] la signature du contrat de mariage entre [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] .
[MASK] [MASK] y séjourne à partir de 1388 .
Le château est cependant abandonné en raison des affrontements de la [MASK] [MASK] [MASK] [MASK] , lorsque la cour s' exile au bord de la [MASK] et à [MASK] .
[MASK] [MASK] y revient après la libération de l' [MASK] et de [MASK] en 1436 , privilégiant le lieu pour sa salubrité .
[MASK] [MASK] er vient chasser à [MASK] , accompagné de sa cour et de sa favorite , la [MASK] [MASK] [MASK] , délaissant ainsi plus ou moins le [MASK] [MASK] [MASK] , et annonçant le retour progressif de la cour dans les environs de [MASK] .
Les noms des architectes du château sont , quant à eux , plus hypothétiques : [MASK] [MASK] , pour sa part , se voyait offrir le 27 décembre 1541 l' assurance de 400 livres par an pour " son état de peintre et d' architecteur au fait de ses édifices et bastiments au dit lieu de [MASK] " .
D' autres noms ont été avancés pour identifier l' architecte qui officia sous le règne de [MASK] [MASK] er .
La fin du règne de [MASK] [MASK] er , décédé en 1547 , aurait vu le remplacement de la chapelle .
C' est en 1539 que [MASK] [MASK] er reçoit à [MASK] [MASK] [MASK] et lui fait visiter son palais , entre le 24 et le 30 décembre .
[MASK] se fera l' écho du faste déployé au château par l' écriture de quelques vers :
Il nomme [MASK] [MASK] pour vérifier et visiter le château le 3 avril 1548 , date à laquelle la suite des travaux lui est confiée .
Deux jours après la mort d' [MASK] [MASK] en 1559 , [MASK] [MASK] [MASK] remercie [MASK] [MASK] , protégé de [MASK] [MASK] [MASK] , et confie les travaux au [MASK] qui devient surintendant des maisons royales le 12 juillet 1559 .
C' est à cette époque que [MASK] [MASK] [MASK] décore le château .
À la mort de [MASK] [MASK] en octobre 1578 , le chantier est confié par [MASK] [MASK] à [MASK] [MASK] [MASK] [MASK] .
Pendant le règne des trois fils d' [MASK] [MASK] ( [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] ) , le château de [MASK] est moins habité , les monarques lui préférant le [MASK] , ou encore les demeures du [MASK] [MASK] [MASK] comme [MASK] ou [MASK] .
Le château accueille entre le 14 et le 21 décembre 1599 la visite de [MASK] [MASK] [MASK] .
[MASK] [MASK] , qui hérite en 1610 d' un château encore en chantier , fait achever les travaux sans apporter de modification majeure .
C' est là que le cardinal [MASK] , neveu du pape [MASK] [MASK] , est reçu par [MASK] [MASK] au château pendant l' été 1625 ; que le [MASK] [MASK] [MASK] est arrêté le 4 mai 1626 ; qu' est ratifié le traité de paix ( [MASK] [MASK] [MASK] ) entre [MASK] [MASK] et l' [MASK] le 16 septembre 1629 .
Le 25 septembre 1645 est signé à [MASK] le contrat de mariage entre [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] .
[MASK] [MASK] , bien que préférant les demeures situées à l' ouest de [MASK] et accordant toutes ses attentions au [MASK] [MASK] [MASK] , aime venir à [MASK] .
Le [MASK] [MASK] s' éteint à son tour dans le château le 11 décembre 1686 .
Le 27 octobre 1743 , [MASK] est le théâtre de la signature d' un traité d' alliance secret entre [MASK] [MASK] et [MASK] [MASK] .
Le 3 novembre 1762 y est signé le [MASK] [MASK] [MASK] , traité secret entre [MASK] [MASK] et [MASK] [MASK] au sujet des possessions de la [MASK] .
Le dauphin [MASK] , fils de [MASK] [MASK] , meurt de la tuberculose au château le 20 décembre 1765 .
[MASK] [MASK] [MASK] fait revivre [MASK] à partir de 1804 , il le fait meubler , y tient sa cour pour laquelle il fait aménager 40 appartements de maître .
Le pape quittera [MASK] le 23 janvier 1814 .
Le 23 mai 1808 , le château accueille la visite de [MASK] [MASK] [MASK] [MASK] et de la reine [MASK] .
Le futur [MASK] [MASK] est baptisé au château le 4 novembre 1810 , avec 24 autres enfants de dignitaires et généraux .
À la suite de [MASK] , les derniers monarques français y feront plusieurs séjours : le 15 juin 1816 , [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] , est reçue au château .
[MASK] [MASK] et [MASK] [MASK] y ont dormi .
Sous la [MASK] [MASK] [MASK] , [MASK] entreprend les premiers travaux de restauration ( dirigés par [MASK] [MASK] , [MASK] , et [MASK] [MASK] [MASK] ) et fait redécorer et remeubler l' intérieur , avant que le château ne serve de cadre au mariage de [MASK] [MASK] [MASK] avec [MASK] [MASK] [MASK] le 30 mai 1837 .
C' est en 1848 qu' [MASK] [MASK] devient architecte du château et entreprend de nouvelles restaurations .
À sa mort en 1853 , il est remplacé par [MASK] [MASK] puis [MASK] [MASK] en 1855 .
Sous le [MASK] [MASK] , [MASK] fait partie , avec [MASK] , [MASK] et [MASK] , des lieux de villégiature de la cour .
L' impératrice [MASK] , épouse de [MASK] [MASK] , passe ses soirées dans le petit théâtre construit par son mari .
Elle s' attache au salon chinois , agrémenté par des objets provenant du [MASK] [MASK] [MASK] [MASK] [MASK] et par les cadeaux des ambassadeurs du [MASK] , reçus au château le 27 juin 1861 .
En janvier 1949 , une partie du château est investie par le commandement en chef des [MASK] [MASK] [MASK] ( [MASK] ) et y restera jusqu' en juillet 1966 .
Une restauration générale du château est permise par la loi-programme des années 1964 -- 1968 dont [MASK] [MASK] est l' investigateur .
C' est en 1986 qu' est inauguré dans l' aile [MASK] [MASK] le musée [MASK] [MASK] er .
[MASK] [MASK] avait présidé à la création de deux oratoires : l' un pour [MASK] [MASK] réalisé en 1557 , l' autre pour [MASK] [MASK] [MASK] .
Les dernières peintures décoratives exécutées dans la chapelle sont les tableaux ovales réalisés sous [MASK] [MASK] .
Le principal évènement qui eu lieu dans cette chapelle fut le mariage de [MASK] [MASK] et [MASK] [MASK] en 1725 .
La décoration et l' ameublement furent revus notamment sous le [MASK] [MASK] , mais le décor de boiseries des trois salles les plus importantes a été renouvelé dès 1644 .
Les peintures représentent des récits de la mythologie gréco-romaine et des allégories dont le sens nous échappe aujourd'hui ( [MASK] [MASK] [MASK] , sœur de [MASK] [MASK] er , admettait elle-même la complexité des thèmes et disait " lire en hébreu " sans explication annexe ) , mais qui symbolisent probablement le bon gouvernement du roi et font l' éloge de [MASK] [MASK] er .
À ses pieds figurent trois allégories de l' air , de la terre et de l' eau , ainsi qu' une cigogne qui symboliserait l' amour filial , celle-ci représentant la mère du roi , [MASK] [MASK] [MASK] .
[MASK] [MASK] fit dédoubler l' aile en 1786 en ajoutant des appartements , la privant ainsi de son ouverture sur le jardin de [MASK] , mais faisant réaliser de fausses portes-fenêtres pour garder un aspect symétrique .
[MASK] [MASK] er avait fait aménager , en 1534 , au rez-de-chaussée de l' aile qui porte aujourd'hui son nom , un ensemble composé de trois salles de bains et de quatre petits salons qui furent décorés de stucs , de grotesques et de fresques , dont certaines étaient dues au [MASK] .
Les petits appartements de [MASK] [MASK] er se situent à l' emplacement des anciens bains de [MASK] [MASK] er , transformés sous [MASK] [MASK] en appartements privés réservés au roi , à [MASK] [MASK] [MASK] [MASK] puis à [MASK] [MASK] [MASK] [MASK] .
Ils furent aménagés pour [MASK] [MASK] er de 1808 à 1810 .
En 1565 , [MASK] [MASK] [MASK] fait doubler le corps de bâtiment jouxtant le jardin de [MASK] et multiplie ainsi le nombre d' appartements .
Situé au rez-de-chaussée de l' aile des appartements royaux , les appartements de [MASK] ont été aménagés pour elle en 1808 , à partir d' une suite de pièces aux lambris de style [MASK] [MASK] .
Ils furent occupés par l' impératrice [MASK] à partir de 1810 .
Le mobilier de style [MASK] [MASK] , bien qu' incomplet , restitue assez fidèlement l' aspect de la pièce lors de la réalisation des décors muraux .
La soierie des murs brochée et chenillée a été retissée sur le modèle ancien exécuté à [MASK] à la fin du règne de [MASK] [MASK] .
La pièce est meublée dans son état [MASK] [MASK] , cependant incomplet .
Les garnitures sont en velours blanc lamé or et en gros de [MASK] jaune broché or .
Le portique de [MASK] donne sur la cour ovale .
Le portique a vraisemblablement été édifié en 1531 , il est donc antérieur à l' arrivée de [MASK] à [MASK] .
Il fut déplacé par [MASK] [MASK] et fut reconstruit en 1893 .
Les peintures du [MASK] dont elle est décorée ont été restaurées .
Le tympan est orné de la salamandre de [MASK] [MASK] er .
Celle du premier étage , fermée par un vitrage sous [MASK] [MASK] correspond à l' appartement de [MASK] [MASK] [MASK] .
Sous [MASK] [MASK] , les appartements sont occupés par la [MASK] [MASK] [MASK] , puis par [MASK] [MASK] [MASK] , et enfin par le [MASK] [MASK] [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] .
En 1804 , les appartements deviennent les quartiers privés de [MASK] [MASK] .
En 1837 , ils sont occupés par le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , puis par le [MASK] [MASK] en 1839 , et [MASK] [MASK] en 1845 .
La salle de bal , dite parfois " galerie [MASK] [MASK] " , longue de 30 m et large de 10 m , a une superficie qui dépasse 300 m 2 .
[MASK] [MASK] er puis [MASK] [MASK] décident de la transformer en une grande salle de réception et d' apparat pour y organiser les fêtes royales .
La conception de la salle est confiée à l' architecte [MASK] [MASK] .
La cour était invitée à des bals masqués extravagants : on a pu voir [MASK] [MASK] er déguisé en centaure .
En 1642 , le surintendant des bâtiments du roi [MASK] [MASK] [MASK] fait appel à [MASK] pour savoir comment éviter des dégradations qui ruinent peu à peu le décor peint .
La porte d' entrée en pierre de taille réalisée par [MASK] [MASK] date du règne de [MASK] [MASK] et était autrefois peinte , comme le prouve un paiement fait en 1558 à deux peintres .
Construite en pierre de taille et plus dégagée qu' elle ne l' est aujourd'hui , elle apparaissait comme le pendant du portique de [MASK] avec lequel elle partageait de nombreux traits " français " : arcs en anse-de-panier , chapiteaux de fantaisie , ici avec le cerf bellifontain .
En 1612 , une commande passée à [MASK] [MASK] prévoyait l' exécution de six grandes toiles pour couvrir les fenêtres aveuglées .
Située sous la salle de bal , la salle des colonnes a été amménagée par [MASK] .
Le jeu de paume de [MASK] fut surtout utilisé par le roi [MASK] [MASK] .
Utilisée comme salle des banquets par [MASK] , elle est transformée en bibliothèque sous le [MASK] [MASK] , en 1858 .
Ses principaux conservateurs au XIX e siècle furent entre autres [MASK] [MASK] , [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .
La galerie conserve également les fontes d' origines des copies de statues antiques exécutées par [MASK] [MASK] en 1540 .
Située à l' emplacement d' une ancienne porte aux bossages rustiques en grès construite en 1565 par [MASK] [MASK] et qui constitue aujourd'hui le rez-de-chaussée de l' édifice , la porte triomphale actuelle possède un étage en forme d' arcade surmonté d' un dôme à pans dont le fronton triangulaire est orné de sculptures représentant deux victoires soutenant les armes de [MASK] [MASK] .
Elle fut édifiée par [MASK] [MASK] , soucieux de trouver de nouveaux espaces .
Il a été édifié par [MASK] [MASK] en 1750 , avec un toit d' ardoise mansardé et percé de plusieurs œils-de-bœuf .
Les salons qui composent ce musée , aux décors de style [MASK] [MASK] , furent restaurés en 1991 .
Un escalier d' honneur , édifié en 1768 à l' emplacement d' un ancien escalier du XVI e siècle , est orné sous [MASK] de tableaux d' [MASK] [MASK] et [MASK] [MASK] représentant des scènes de chasses et des natures mortes .
Au cours du XIX e siècle , le château de [MASK] n' a subi que peu de transformations extérieures .
Ces aménagements ont surtout eu lieu sous les règnes de [MASK] [MASK] er , [MASK] [MASK] , et [MASK] .
On peut néanmoins noter la construction en 1834 ( sous [MASK] ) , d' un petit pavillon , dit " pavillon [MASK] " , jouxtant la galerie de [MASK] .
Disposant d' environ 450 places , le théâtre s' inspire des décors de l' opéra royal de [MASK] .
Elle acquit son nom grâce à un moulage en plâtre de la [MASK] [MASK] [MASK] [MASK] [MASK] au [MASK] , installé entre 1560 et 1570 , disparu en 1626 , et dont une petite dalle , dans l' allée centrale , rappelle l' emplacement .
Elle fut considérablement modifiée sous [MASK] [MASK] .
Le parc de [MASK] s' étend sur 115 hectares .
Le jardin de [MASK] , au nord du château , fut élevé par [MASK] [MASK] [MASK] sur un espace déjà aménagé par [MASK] [MASK] er et portait à l' époque le nom de jardin de la [MASK] .
Il fut de nouveau remanié sous [MASK] [MASK] .
Au XIX e siècle , sous [MASK] [MASK] er puis [MASK] , le jardin fut transformé en jardin anglais et l' orangerie détruite .
La statue actuelle , dite [MASK] à la biche , date de 1684 .
Même après la disparition de ces arbres , le nom lui est resté , et [MASK] [MASK] y plante le premier platane , essence rare à l' époque .
Au centre d' un vaste étang peuplé de carpes tricentenaires , dont les premiers spécimens , une soixantaine , furent offerts à [MASK] [MASK] par [MASK] [MASK] [MASK] , s' élève un pavillon d' agrément octogonal à toiture basse , sobrement décoré , édifié sous [MASK] [MASK] , reconstruit sous [MASK] [MASK] en 1662 et restauré par [MASK] [MASK] er .
Les terrasses furent plantées de tilleuls sous [MASK] [MASK] er .
On pouvait s' y promener en bateau et [MASK] [MASK] y fit naviguer une galère .
Il est nécessaire de différencier , dans l' appréciation des chiffres du tourisme sur le site du [MASK] [MASK] [MASK] , le château lui-même , le domaine ( château , jardins et parc ) , et un troisième ensemble plus large englobant le château , ses jardins , son parc , et la [MASK] [MASK] [MASK] environnante .
Le château de [MASK] a reçu 329960 visiteurs en 2001 , 232087 en 2007 et 230816 en 2008 .
Le château et son parc , constituent en 2008 le troisième site le plus visité du département de [MASK] , avec une fréquentation de 384039 visiteurs , en hausse de 8 % par rapport à 2007 .
En tout , château , jardins , et [MASK] [MASK] [MASK] accueillent quelques 13 millions de visiteurs .
Cependant , cette fréquentation exceptionnelle qui touche à la fois le national et l' international , ne produit que peu de retombées économiques sur [MASK] et sa région , du fait d' un manque de services associés ( hébergement , restauration , locations ... ) .
C' est également lors d' un séjour au château que [MASK] [MASK] écrit et dicte sa célèbre dictée en 1857 à la demande de l' [MASK] [MASK] pour distraire la cour de [MASK] [MASK] .
On note la présence des compositeurs [MASK] [MASK] [MASK] et [MASK] [MASK] dans la cour de [MASK] [MASK] er .
Dans la dernière moitié du siècle , et en particulier sous le règne d' [MASK] [MASK] , les œuvres de [MASK] [MASK] [MASK] et [MASK] [MASK] sont jouées au château .
Cette démarche artistique sera poursuivie au siècle suivant , avec la venue de [MASK] [MASK] , [MASK] [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , et [MASK] [MASK] .
Le XIX e siècle est particulièrement marqué par la venue , sur ordre de [MASK] , de l' [MASK] [MASK] [MASK] qui interprète en 1835 [MASK] [MASK] [MASK] de [MASK] .
Le château de [MASK] , grâce à son cadre historique , a été le théâtre de nombreux tournages cinématographiques , parmi lesquels :