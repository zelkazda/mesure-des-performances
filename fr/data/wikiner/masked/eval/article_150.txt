[MASK] [ bʀysɛl ] est la capitale de la [MASK] ( [MASK] ) et l' un des sièges de l' [MASK] [MASK] et de plusieurs de ses institutions .
Elle est le siège des gouvernements et parlements de plusieurs des entités fédérées qui composent la [MASK] -- la [MASK] [MASK] [MASK] , la [MASK] [MASK] [MASK] [MASK] , la [MASK] [MASK] et la [MASK] [MASK] .
Enfin , c' est aussi la ville où siègent certaines organisations internationales , dont l' [MASK] .
Bien que [MASK] fut historiquement d' expression néerlandaise ( brabançon ) , la ville s' est francisée jusqu' au point de devenir majoritairement francophone .
Il existe d' autres exemples tels qu' [MASK] ou [MASK] en [MASK] .
Le terme [MASK] est le plus souvent utilisé pour désigner la ville-région , administrée par la [MASK] [MASK] [MASK] .
La commune centrale , qui prend le nom de " [MASK] [MASK] [MASK] " ( communément appelée " [MASK] " ) est un ensemble hétéroclite comprenant l' hypercentre ( le [MASK] ) et une série d' extensions urbaines , comme le populeux quartier nord de [MASK] et le quartier maritime , la très bourgeoise [MASK] [MASK] au sud , le [MASK] [MASK] [MASK] [MASK] , ou encore le quartier central d' affaires [MASK] , où se concentrent notamment les institutions européennes .
Les 18 autres communes s' agglomèrent autour du [MASK] ( l' hypercentre ) pour former une ville comptant un million d' habitants et une centaine de quartiers distincts .
La ville-région est une des trois [MASK] [MASK] [MASK] [MASK] , les deux autres étant la [MASK] [MASK] et la [MASK] [MASK] .
Une comparaison avec d' autres communes belges est possible dans la liste des communes belges les plus peuplées , où [MASK] n' apparaît pas d' un seul tenant , mais par division communale .
La ville est par ailleurs le siège du gouvernement flamand et le siège du gouvernement de la [MASK] [MASK] [MASK] [MASK] ( le siège du gouvernement wallon étant lui à [MASK] ) .
Pour les aspects institutionnels , référez-vous à la page " [MASK] [MASK] [MASK] " .
[MASK] s' étend sur les 19 communes de la [MASK] [MASK] [MASK] et compte un peu plus d' un million d' habitants .
L' agglomération réelle , en tenant compte de la zone d' emplois ( zone [MASK] ) compte de l' ordre de 2,7 millions d' habitants et s' étend sur une grande partie des deux provinces de [MASK] ( [MASK] [MASK] et [MASK] [MASK] ) .
[MASK] fait également partie d' une large conurbation qui s' étend en triangle entre [MASK] , [MASK] et [MASK] et qui rassemble environ 4,4 millions d' habitants .
Enfin , [MASK] partage avec [MASK] le titre de ville comptant le plus de journalistes accrédités .
[MASK] , qui a fêté son millénaire en 1979 , a une histoire mouvementée liée à celle du continent européen dans la même période .
Étymologiquement , le nom de " [MASK] " est expliqué de diverses manières , pour les uns il serait d' origine flamande signifiant en vieux flamand " habitation " ( sel/zele ) " des marais " ( broek ) , en effet , jusqu' au voûtement de la [MASK] en 1871 , [MASK] était marécageux et sujet à des inondations périodiques .
D' autres encore [ réf. nécessaire ] , pensent que la première partie du mot correspond à un autre mot celte : briga , " hauteur " ; ajouté au terme latin cella , " le temple " , cela signifierait " le temple sur la hauteur " , comme c' est d' ailleurs toujours le cas de la [MASK] [MASK] .
C' est également à [MASK] ( [MASK] ) que se situe le siège de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
La ville accueille 120 institutions internationales , 159 ambassades ( intra muros ) et plus de 2500 diplomates , faisant de [MASK] le deuxième centre de relations diplomatiques au monde ( après [MASK] [MASK] ) .
Bien qu' historiquement [MASK] fut une cité dont les habitants parlèrent des dialectes brabançons -- communément appelé dialecte flamand , -- , la situation linguistique bruxelloise changea radicalement au cours des deux derniers siècles .
À partir des années 1960 , à la suite de la fixation de la frontière linguistique et de l' essor socio-économique de la [MASK] [MASK] , la francisation des néerlandophones a stagné , .
[MASK] [MASK] [MASK] [MASK] constituent ensemble la seule partie officiellement bilingue de la [MASK] .
La création de la [MASK] [MASK] [MASK] a été longtemps retardée du fait des visions différentes sur le [MASK] [MASK] [MASK] .
En 1989 , la [MASK] [MASK] [MASK] a tout de même fini par être créée officiellement .
[MASK] s' impose comme une des principales villes d' affaires d' [MASK] .
Sa position géographique au centre des régions les plus dynamiques d' [MASK] , son statut de grand centre urbain et ses fonctions de multiple capitale font de la ville un réservoir d' emplois très qualifiés , dominés par les activités tertiaires de pointes .
Le [MASK] [MASK] a également son siège à [MASK] pour les sessions extraordinaires et les commissions .
Dans les faits , le travail parlementaire se fait à [MASK] , les députés n' allant à [MASK] que pour voter les lois .
La [MASK] [MASK] [MASK] compte plus d' un million d' habitants et connait une remarquable augmentation de sa population .
[MASK] est la ville qui croît le plus vite en [MASK] .
[MASK] fait aussi partie d' une plus large conurbation en triangle avec [MASK] et [MASK] qui compte environ 4,4 millions d' âmes ( un peu plus de 40 % de la population totale de la [MASK] ) et rassemble l' essentiel de l' activité économique de la [MASK] .
L' [MASK] relie [MASK] à [MASK] ( via [MASK] ) .
[MASK] est desservie par plusieurs gares :
Il existe un projet de [MASK] devant apporter une réponse aux gros problèmes de mobilité rencontrés par la ville .
La [MASK] devrait être terminé pour 2016 .
Les transports urbains sont assurés par un réseau dense de tramways ( en surface et souterrains ) , d' autobus et de métro géré par la [MASK] .
[MASK] est aussi desservi par deux aéroports :
[MASK] est une ville étendue , l' espace disponible par habitant dépasse la moyenne des autres capitales européennes .
) , concentrées dans les quartiers d' affaires de la ville : [MASK] [MASK] , quartier européen , [MASK] [MASK] .
Après la [MASK] [MASK] [MASK] , les destructions ainsi que la forte croissance démographique due à l' afflux de nouveaux habitants venus des autres régions du pays provoquent une crise du logement et une extension rapide des surfaces construites .
À l' instar de nombreuses villes dans les pays anglo-saxon , [MASK] a connu depuis l' après-guerre jusqu' aux années 1990 un très fort mouvement de périurbanisation des classes moyennes et supérieures , cherchant en périphérie un habitat de standing tandis que le centre , abandonné , se taudifiait et devenait la proie de nombreuses spéculations immobilières .
L' [MASK] [MASK] [MASK] [MASK] est en résidence à [MASK] .
Les taux de couverture végétale et d' espaces naturel sont plus importants en périphérie où ils ont limité la périurbanisation de la capitale , mais ils diminuent fortement vers le centre de [MASK] : 10 % du pentagone , 30 % de la première couronne et 71 % de la deuxième couronne sont des espaces verts !
Ce sont aussi les axes de " mobilité douce " , dont sur 14 itinéraires ( radials ) en cours de verdurisation et de ramification en périphérie de [MASK] .
Des perruches prospèrent en se réchauffant l' hiver sur des transformateurs électriques ou des lampadaires , et de nombreuses plantes venues de tous les continents peuvent être trouvées à [MASK] .
En moyenne ( moyenne faite sur une période couvrant les 30 dernières années ) , on observe un peu plus de 130 jours de pluie par an dans la [MASK] [MASK] [MASK] .
Selon la presse , [MASK] attirerait de très nombreux services d' espionnage nationaux et internationaux , en raison de la présence du siège d' institutions comme l' [MASK] , la [MASK] [MASK] ou le [MASK] [MASK] .
[MASK] serait le deuxième nid d' espions dans le monde après le quartier du siège de l' [MASK] à [MASK] [MASK] .
La [MASK] [MASK] [MASK] est actuellement jumelée avec :