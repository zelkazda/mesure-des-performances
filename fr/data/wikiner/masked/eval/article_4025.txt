Toute sa vie , [MASK] [MASK] souffrit de ce sentiment de rejet et de l' image que lui renvoyait son miroir : celle d' un homme que l' on qualifiait de laid .
Les textes de ses chansons jouent souvent sur le double sens et illustrent son goût pour la provocation , en particulier érotique ( [MASK] [MASK] ) ou pornographique ( [MASK] [MASK] [MASK] [MASK] ) .
[MASK] [MASK] aime également jouer avec les références littéraires comme [MASK] [MASK] [ réf. nécessaire ] ou [MASK] ( Je suis venu te dire que je m' en vais ) .
Cependant il considérait la chanson , et en particulier les paroles de chanson , comme un genre mineur , même s' il travaillait parfois la forme poétique comme certaines de ses rimes dans [MASK] [MASK] [MASK] [MASK] [MASK] .
Il devra même se cacher trois jours durant dans une forêt tandis que les [MASK] [MASK] [MASK] [MASK] .
[MASK] [MASK] n' a jamais rien su de cette dénaturalisation .
De retour à [MASK] après la libération , la petite famille s' installe dans le [MASK] [MASK] [MASK] [MASK] [MASK] .
[MASK] est en échec scolaire et abandonne peu avant le bac au [MASK] [MASK] .
L' année 1948 est une année importante pour [MASK] qui fait son service militaire à [MASK] où il sera envoyé régulièrement au trou pour insoumission .
Il a une révélation en voyant [MASK] [MASK] au [MASK] [MASK] [MASK] , qui écrit et interprète des textes provocateurs , drôles , cyniques , loin des vedettes du moment , comme [MASK] [MASK] ou [MASK] [MASK] .
Plus tard , engagé comme pianiste d' ambiance par [MASK] [MASK] , directeur artistique du cabaret , [MASK] [MASK] accompagnera à la guitare la chanteuse [MASK] [MASK] .
Le lendemain , [MASK] [MASK] pousse sur scène [MASK] qui , mort de trac , interprétera son propre répertoire .
[MASK] [MASK] sera d' ailleurs la première interprète de [MASK] ( et plus tard , en 1966 , son fils [MASK] [MASK] ) .
Il commence à déposer ses titres à la [MASK] .
Il entre en studio et commence sa fructueuse collaboration avec [MASK] [MASK] , déja arrangeur de [MASK] [MASK] .
Il est remarqué par [MASK] [MASK] , qui dit que ses chansons " ont la dureté d' un constat " .
Son maître [MASK] [MASK] , avant de mourir en 1959 , le compare à [MASK] [MASK] .
Lorsque l' époque des yéyés arrive , il est alors âgé de 32 ans , il n' est pas très à l' aise : il passe en première partie de [MASK] ou de [MASK] , mais le public le rejette et les critiques , cruelles , se moquent de ses grandes oreilles et de son nez proéminent .
Il fera en 1964 quelques duos avec l' artiste [MASK] [MASK] auquel il ressemble alors de façon troublante .
Il rencontre alors [MASK] [MASK] et [MASK] [MASK] et leur demande de faire un disque avec lui .
Ce sera [MASK] [MASK] empreint d' un jazz archimoderne qui plaisait tant à [MASK] , mais qui , il le sait , ne lui permettra jamais d' atteindre le succès .
Sa décision était prise dès la sortie du studio : " Je vais me lancer dans l' alimentaire et m' acheter une [MASK] " .
Malgré tout , son album suivant , [MASK] [MASK] , inspiré ( parfois directement -- et sans droit d' auteur ! [ réf. nécessaire ] ) des rythmes africains de [MASK] [MASK] et [MASK] [MASK] , reste encore à l' écart de la vague yéyé qui apparaît et fera la fortune de [MASK] .
C' est en écrivant pour [MASK] [MASK] et [MASK] [MASK] qu' il rencontre ses premiers succès , mais c' est avec [MASK] [MASK] ( [MASK] [MASK] [MASK] [MASK] [MASK] )
et surtout [MASK] [MASK] qu' il va réussir à séduire un public jeune .
La chanson lauréate devient le tube international qui passe sur toutes les ondes et que [MASK] [MASK] enregistre même en japonais .
Sur le tournage du film [MASK] , en 1968 , il rencontre [MASK] [MASK] pour laquelle il sera à nouveau auteur-compositeur .
Pas une [MASK] [MASK] [MASK] de 1910 , comme dans une de ses chansons mais une [MASK] [MASK] [MASK] de 1928 .
Mais [MASK] ne pouvait conduire ses voitures , il n' avait pas son permis .
[MASK] [MASK] [MASK] [MASK] est accueilli par la presse comme " le premier vrai poème symphonique de l' âge pop " [ réf. nécessaire ] .
Avec [MASK] [MASK] [MASK] [MASK] il pousse la provocation à son comble : il tourne en dérision , au second degré , l' esthétique et la verroterie nazies .
En mai 1973 , [MASK] [MASK] est victime d' une crise cardiaque , et la transforme en élément promotionnel en annonçant à la presse , depuis son lit d' hôpital , qu' il va réagir " en augmentant sa consommation d' alcool et de cigarettes " .
Son film [MASK] [MASK] [MASK] [MASK] [MASK] obtient très vite une réputation sulfureuse avec un scénario audacieux touchant aux tabous de l' homosexualité et de l' érotisme .
Il réalise trois autres films ( dont [MASK] en 1983 ) qui obtiennent peu de succès , les sujets abordés étant souvent provocateurs , que ce soit l' inceste ( [MASK] [MASK] [MASK] en 1986 ) , l' exhibitionnisme ( [MASK] [MASK] [MASK] ) ou l' homosexualité ...
En 1978 , son nouvel album enregistré à [MASK] devient disque de platine en quelques mois .
[MASK] [MASK] reggae choque le journaliste du [MASK] [MASK] [MASK] qui écrit un article virulent selon lequel , en antisémitisme , " il y a aussi des rabatteurs " .
[MASK] poursuit une tournée triomphale , accompagné de [MASK] [MASK] [MASK] et des choristes de [MASK] [MASK] : les [MASK] [MASK] .
En septembre 1980 , après plus de dix ans de vie commune , [MASK] [MASK] n' en peut plus et le quitte .
Elle admet lors d' une émission télévisée réalisée après sa mort : " J' avais beaucoup aimé [MASK] , mais j' avais peur de [MASK] " .
Il rencontre une nouvelle égérie , [MASK] , pour laquelle , il ne peut , une fois de plus , s' empêcher de composer .
Il se produit de longues semaines en concert au [MASK] [MASK] [MASK] .
[MASK] s' éteint en 1991 à la suite d' une cinquième crise cardiaque , ( un comble pour celui qui suivait affligé les enterrements de ses cardiologues successifs ) .
Il est enterré avec ses parents au [MASK] [MASK] [MASK] ( 1 re section ) à [MASK] où sa tombe est l' une des plus visitées avec celles de [MASK] [MASK] -- [MASK] [MASK] [MASK] et de [MASK] [MASK] qu' il mit en musique ( [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ) [ réf. nécessaire ] .
Lors de son enterrement , le 7 mars 1991 , vinrent notamment parmi la foule , outre sa famille , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] , [MASK] [MASK] , les ministres [MASK] [MASK] et [MASK] [MASK] , et les brigades de cuisiniers et serveurs du restaurant [MASK] [MASK] [MASK] [MASK] où il avait passé ses derniers jours .
En 1951 , [MASK] [MASK] se marie à [MASK] [MASK] , fille d' aristocrates russes émigrés , avec qui il restera jusqu' en 1957 .
Fin 1967 , il vit une passion courte , qui ne dure donc que quelques mois avec [MASK] [MASK] .
En 1968 , il rencontre l' actrice britannique [MASK] [MASK] , sur le tournage du film [MASK] .
Leur fille [MASK] [MASK] naît le 21 juillet 1971 à [MASK] .
En 1981 , il se met avec une jeune mannequin , [MASK] , dont il a un fils , [MASK] dit [MASK] [MASK] , né le 5 janvier 1986 .
Le 7 mars 2003 la première rue [MASK] [MASK] est inaugurée à [MASK] en présence de [MASK] [MASK] .
Il existe également une rue [MASK] [MASK] à [MASK] .
Dans le film [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , l' établissement scolaire , centre de l' intrigue s' appelle " collège [MASK] [MASK] " .
Le scénariste et dessinateur [MASK] [MASK] a réalisé un film biographique sur [MASK] [MASK] , dont le rôle est interprété par [MASK] [MASK] .
[MASK] [MASK] [MASK] [MASK] , il est sorti en salles le 20 janvier 2010 .
[ ... ] il va s' agir d' un film très documenté qui court tout le long de la vie de [MASK] .
[MASK] [MASK] marque fortement la musique française .
Il n' hésite pas à métisser ses compositions avec des influences musicales très variées , contribuant à en populariser certaines en [MASK] :
L' album [MASK] [MASK] [MASK] sorti en mars 2006 , regroupe 14 adaptations anglaises réalisées par [MASK] [MASK] et interprétées notamment par [MASK] [MASK] , [MASK] , [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] , [MASK] , [MASK] ...
[MASK] [MASK] imprime en outre durablement sa marque grâce à ses textes .
Dans un style poétique , il n' hésite pas à produire des rimes complexes ( [MASK] [MASK] [MASK] [MASK] [MASK] ) .
Certaines de ses chansons marquent les mémoires par leur caractère provocateur , ainsi les allusions appuyées à la fellation dans [MASK] [MASK] , qui provoquent l' émoi dans la bouche d' une [MASK] [MASK] d' à peine 18 ans .
[MASK] flirte avec le tabou de l' inceste en compagnie de sa fille , la frêle [MASK] [MASK] : dans les années 1980 , elle accompagne son père dans le duo [MASK] [MASK] , titre évocateur qui suscitera une levée de boucliers .
[MASK] atteindra les sommets de la provocation érotique avec le tube [MASK] [MASK] [MASK] [MASK] : véritable poème pornographique , dit par lui-même d' une voix monocorde et cassée .
[MASK] [MASK] a écrit pour de nombreuses interprètes notamment sa fille [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] , seules ou le temps d' un duo à leur côté .
Il se classe à la 85 e place des ventes de disques cumulées pour [MASK] [MASK] .