Le département a été créé à la [MASK] [MASK] , le 4 mars 1790 en application de la loi du 22 décembre 1789 .
La [MASK] a été habitée dès le paléolithique moyen ( sites de [MASK] , [MASK] , [MASK] , [MASK] ) .
La coupure entre l' [MASK] , à l' est , centré sur [MASK] et la [MASK] , à l' ouest , centrée sur [MASK] et englobant [MASK] , qui existait déjà à l' époque celte a perduré jusqu' à la [MASK] et n' a d' ailleurs pas totalement disparu .
Suite aux grandes invasions et à l' effondrement de l' [MASK] [MASK] , le département entre dans une période de léthargie comme l' ensemble de l' [MASK] à laquelle il appartient .
Durant la [MASK] , peu d' évènements marquants sont à signaler -- si ce n' est la création artificielle du [MASK] [MASK] [MASK] [MASK] -- et la [MASK] y a été très modérée .
À cette baisse de population se sont ajoutées les saignées des guerres et la [MASK] n' a toujours pas retrouvé son niveau de population de 1851 .
Le [MASK] [MASK] [MASK] [MASK] fait administrativement partie de la région [MASK] dont il occupe le quart sud-est .
Le département est limitrophe , au nord , des départements des [MASK] et de la [MASK] , à l' est , de celui de la [MASK] , au sud-est et au sud de celui de la [MASK] et , enfin , à l' ouest de celui de la [MASK] , département avec lequel il partage la plus grande longueur de ses limites administratives .
La [MASK] est limitrophe de deux régions administratives .
A l' est , elle est au contact avec la [MASK] [MASK] et au sud et sud-est avec la [MASK] [MASK] .
Avant sa formation lors des débats de la [MASK] de 1790 , le département , dénommé [MASK] de janvier à mars 1790 , était composé très majoritairement des anciennes provinces de l' [MASK] , au centre , et de la [MASK] , à l' ouest et au sud-ouest , avec respectivement [MASK] et [MASK] comme capitales historiques .
La géographie administrative de la [MASK] a subi beaucoup de modifications depuis sa création en 1790 , à l' exception des limites départementales qui sont demeurées sans changement depuis le premier tracé .
Comme l' indique la carte administrative ci-contre , le [MASK] [MASK] [MASK] [MASK] est subdivisé aujourd'hui en trois arrondissements de taille à peu près comparable depuis la refonte de leurs limites administratives au 1er janvier 2008 .
Le département est également subdivisé en 35 cantons qui ont subi de nombreuses modifications territoriales depuis leurs origines , surtout autour des deux principales villes que sont [MASK] et [MASK] , et il regroupe aujourd'hui 404 communes , ces dernières ayant à leur tour beaucoup varié aussi bien par le nombre que par les remaniements ( fusions , absorptions , annexions ) .
Les divisions administratives actuelles du [MASK] [MASK] [MASK] [MASK] sont les suivantes :
Dans le même temps les deux sénateurs élus sont plus modérés , ce sont [MASK] [MASK] juste de retour de déportation et [MASK] [MASK] qui seront ensuite réélus constamment jusqu' en 1980 .
[MASK] [MASK] sauve son fauteuil mais après sa mort accidentelle en 1970 il est remplacé par un troisième député [MASK] , [MASK] [MASK] .
L' élection présidentielle de 1974 , et le score de 54,01 % de [MASK] [MASK] au second tour , marquent l' essor de la gauche : [MASK] [MASK] , [MASK] , est élu maire d' [MASK] en 1977 et député en 1978 .
La [MASK] est passée de trois députés de droite en 1970 à trois députés de gauche en 1981 .
Mais les électeurs ruraux restent conservateurs et en 1980 ce sont [MASK] [MASK] et [MASK] [MASK] qui sont élus sénateurs , et en 1982 le conseil général reste à droite avec 19 conseillers contre 16 et [MASK] [MASK] , maire de [MASK] en prend la présidence .
Au législatives de 1986 , les résultats s' équilibrent , [MASK] [MASK] et [MASK] [MASK] pour le [MASK] , [MASK] [MASK] pour l' [MASK] et [MASK] [MASK] pour l' [MASK] .
En 1997 , c' est donc le retour de [MASK] [MASK] à [MASK] , l' arrivée à [MASK] de [MASK] [MASK] , à [MASK] de [MASK] [MASK] et la réélection de [MASK] [MASK] avec 65 % au second tour .
En 2002 , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] sont réélus , mais [MASK] [MASK] prend le siège de [MASK] [MASK] qui le récupèrera en 2007 alors que [MASK] [MASK] remplace [MASK] [MASK] ( [MASK] [MASK] était la candidate des militants locaux , candidate dissidente se présentant contre [MASK] [MASK] , candidat du [MASK] imposé par le national ) .
[MASK] [MASK] [MASK] en devient le président .
Les deux grandes villes [MASK] et [MASK] ont basculé dans l' escarcelle du [MASK] aux élections municipales de 2008 , ce qui confirme la poussée à gauche du département ( après conseils régional et général en 2004 et 4 députés sur 4 à l' issue des législatives de 2007 ) .
Aujourd'hui , [MASK] , 3ème ville du département en banlieue d' [MASK] , reste la dernière ville importante dirigée par la droite .
La [MASK] appartient physiquement , géologiquement et climatiquement au [MASK] [MASK] dont elle constitue avec le département voisin de la [MASK] l' extrémité septentrionale .
C' est également un département de contact puisqu' il confine d' une part , au nord , avec le [MASK] [MASK] [MASK] qui sépare le [MASK] [MASK] du [MASK] [MASK] , et , à l' est , avec le [MASK] [MASK] que délimitent les premières marches du plateau du [MASK] .
C' est dans ce département que se trouve le point culminant de la région [MASK] avec le site de [MASK] qui atteint 368 mètres d' altitude .
Les limites du [MASK] [MASK] [MASK] [MASK] coïncident avec celles du bassin supérieur et moyen du fleuve [MASK] , qui prend sa source à quelques kilomètres dans le département voisin de la [MASK] .
De son lieu de source jusqu' à [MASK] , la pente de la [MASK] est forte et les méandres sont de faible développement .
Les affluents de la [MASK] sont de plusieurs types :
A chaque période hivernale , les crues sont habituelles où l' eau recouvre les vallées de la [MASK] et de ses affluents caractérisées par des marais et des prairies inondables , appelées localement " prées " .
Lors des crues exceptionnelles qui ont lieu environ tous les vingt ans , il n' est pas rare de voir les routes coupées , les bas quartiers des villes comme [MASK] , [MASK] et [MASK] , complètement inondés .
Ces crues peuvent parfois revêtir des aspects spectaculaires comme en 1960 , ou encore en 1982 , année considérée comme la " crue du siècle " , où de [MASK] à l' embouchure du fleuve la vallée de la [MASK] avec ses prairies fluviales ne forment plus qu' un immense lac .
D' [MASK] à [MASK] de nombreux ouvrages , écluses , canaux , barrages , rendent le fleuve navigable , maintenant uniquement pour la navigation fluviale de plaisance .
Les terres qui constituent la [MASK] [MASK] sont argileuses et imperméables , appelées aussi terres froides .
Le nord du département , le [MASK] , est occupé par de grandes plaines céréalières qui ne sont pas sans rappeler celles du [MASK] .
La [MASK] et ses affluents de rive gauche traversent en [MASK] des plateaux calcaires fissurés , générateurs de gouffres et de résurgences , et sur le reste de son bassin versant des terrains imperméables mais aussi des sols calcaires qui , une fois gorgés d' eau , se comportent comme des terrains imperméables .
Les terres à l' ouest , de part et d' autre de la vallée de la [MASK] , sont de nature calcaire et sont appelées terres chaudes étant propices à la culture de la vigne , des céréales ou à la polyculture .
Ces sols et ces paysages annoncent ceux du département voisin de la [MASK] où les affinités y sont particulièrement nombreuses .
Enfin , tout au sud du département , de grandes chênaies , mêlées de châtaigniers et de pins maritimes , recouvrent les sommets décalcifiés de la [MASK] [MASK] , ce dernier constitue un grand massif forestier qui se prolonge en [MASK] et en [MASK] .
Le [MASK] [MASK] [MASK] [MASK] a un climat océanique de type aquitain de [MASK] jusqu' à [MASK] qui se modifie en allant vers l' est en climat océanique dégradé .
Dans le [MASK] les précipitations sont plus importantes et , l' hiver , le froid est plus marqué .
Le vent est le plus souvent d' ouest-nord-ouest en particulier lors des tempêtes dont la plus violente a été la tempête [MASK] avec des vents de plus de 140 km/h sur l' ensemble du département .
Autour d' [MASK] , la densité de population franchit aisément les 200 hab/km² , soit pratiquement le quadruple de la moyenne départementale , et autour de [MASK] la densité est supérieure à 150 hab/km² , soit le triple de celle du département .
Il faut aussi noter que la [MASK] héberge de nombreux résidents britanniques , 5083 en 2006 , ce qui la place au 4 e rang des départements français , juste derrière [MASK] , la [MASK] et les [MASK] , .
En 2007 , la [MASK] recense 31 communes de plus de 2000 habitants dont neuf ont de plus de 5000 habitants .
Les deux principales agglomérations de la [MASK] sont [MASK] qui regroupe 106 230 habitants et [MASK] 27 203 habitants au recensement de 2007 .
En [MASK] , elles occupent respectivement le 3e et le 9e rang régional .
La [MASK] est avant tout un département industriel et il se positionne au tout premier rang régional en [MASK] dans ce domaine que ce soit sur le plan des effectifs que sur celui du nombre des activités .
En raison de cet héritage industriel conséquent , le département se caractérise par la présence de deux chambres consulaires que sont les chambres de commerce et d' industrie d' [MASK] et de [MASK] .
Ainsi la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] envisage-t-elle de s' associer à celle de [MASK] plutôt qu' à celle d' [MASK] .
Le secteur industriel de la [MASK] se distingue notamment par la présence d' activités traditionnelles qui occupent encore aujourd'hui un grand nombre de personnes .
La zone d' appellation [MASK] représente 47131 ha de vignes en [MASK] , assurant 45 % des revenus agricoles du département .
Cette production viticole alimente de nombreuses distilleries , soit artisanales , soit industrielles , où les bouilleurs de cru et de profession et une centaine de négociants assurent 40 % des exportations du [MASK] .
L' industrie lourde est représentée par un puissant secteur des activités extractives qui alimente les usines de la région de [MASK] où l' argile est employée pour la fabrication des briques et des tuiles .
La marne grise alimente la grosse cimenterie [MASK] [MASK] de [MASK] [MASK] , au sud d' [MASK] .
Fort anciennement implantée dans la région d' [MASK] , elle est passée par une crise sévère qui a entamé durement et profondément ses activités .
La poudrerie , devenue [MASK] , est un secteur dynamique .
Les industries électriques sont surtout représentées par [MASK] , grand fabricant de moteurs électriques .
Le patrimoine départemental est d' une extrême richesse et se décline en plusieurs thèmes qui vont des vestiges de l' époque gallo-romaine comme le célèbre site de [MASK] aux nombreuses églises romanes comme celle d' [MASK] sans oublier les multiples châteaux et manoirs dont celui de [MASK] [MASK] en est certainement le plus bel édifice .
Ce dernier qui a lieu annuellement à [MASK] dépasse très largement le cadre du département et a atteint une solide notoriété internationale .