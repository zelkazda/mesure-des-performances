Il suit ses études à [MASK] et à [MASK] [MASK] .
Ses nombreuses lectures durant sa longue convalescence et notamment celle du livre de [MASK] [MASK] le décident à partir pour l' [MASK] pour acheter une concession et pouvoir faire des fouilles .
Le 4 novembre 1922 , [MASK] découvre l' entrée d' une tombe inviolée , celle du pharaon [MASK] .
Vingt-six autres personnes ayant un lien avec la découverte de la tombe de [MASK] seraient mortes prématurément sans qu' on comprenne de quelle maladie elles étaient atteintes .
À noter toutefois qu' [MASK] [MASK] , le premier à y entrer , n' est mort qu' en 1939 de cause naturelle , ainsi que des centaines d' ouvriers , de curieux , et parmi eux des souverains étrangers , qui visitèrent la tombe .