D' origine juive , il fut contraint à l' exil en 1933 et mourut sur le chemin de [MASK] en [MASK] .
[MASK] [MASK] est issu de la petite bourgeoisie d' origine juive allemande .
Il a reçu le [MASK] [MASK] [MASK] [MASK] de 1918 pour ses travaux .
Pendant la [MASK] [MASK] [MASK] , il travaille activement à la mise au point d' armes chimiques et l' emploi du chlore comme gaz de combat ( " vagues dérivantes " ) reçoit l' accord de l' état-major allemand .
Mais leurs parents respectifs s' étaient opposés au projet , jugeant [MASK] trop jeune .
Les deux jeunes gens avaient pu se fiancer quand [MASK] s' était mis à travailler pour le compte de son père , mais les fiançailles avaient été rompues à la suite de la malheureuse affaire du chlorure de chaux .
Cette surenchère dans la barbarie , c' est plus que la femme de [MASK] ne peut en supporter .
Elle a tenté en vain de faire comprendre à [MASK] [MASK] à quel point son travail sur les gaz toxiques corrompait et pervertissait l' essence même de la chimie .
Mais [MASK] a refusé de l' écouter au nom des intérêts supérieurs du pays .
Les gaz pouvaient permettre à l' [MASK] de gagner la guerre et lui-même luttait pour une [MASK] triomphante , pilier de justice et d' ordre , soutien de la culture et de la science .
Elle aurait aussi pu effacer la tache que le nom de [MASK] associé aux gaz empoisonnés , lui avait infligée .
Membre du conseil de surveillance du groupe militaro-industriel [MASK] [MASK] dès sa création en 1925 , [MASK] fut aussi actif dans les recherches sur les réactions de combustion , sur la séparation de l' or de l' eau de mer , sur l' effet adsorption et en électrochimie .
[MASK] étant juif selon l' idéologie des nouveaux maîtres de l' [MASK] , les lois nazies l' obligèrent à prendre sa retraite après la promulgation des lois antisémites et à émigrer en 1934 , malgré tous ses bons et loyaux services .