Aujourd'hui , il semble attesté que l' huile sur bois où l' on voit [MASK] volant dans le ciel , soit une copie , bien que des critiques la considèrent d' une meilleure facture .
À l' horizon , le [MASK] forme un disque qui irradie et unit le violet du ciel à l' émeraude de la mer .
[MASK] illustre un passage des [MASK] d' [MASK] .
Si les personnages d' [MASK] sont représentés pour la première fois , l' essentiel est inversé : les gens à l' aube d' une journée de travail n' ont pas de temps à perdre avec l' ambition d' un fou ou d' un rêveur .
À l' avant-plan , l' épée et la bourse , posées près du laboureur , évoquent un de ces proverbes populaires que [MASK] a illustrés dans d' autres tableaux :
[MASK] [MASK] développe une autre théorie qui a le mérite de situer le peintre dans le contexte historique de son pays .
C' est l' ingéniosité de son père , [MASK] , qui lui en fournit le moyen .
[MASK] reste le forgeron , l' artiste et le créateur génial .