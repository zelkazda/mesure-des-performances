[MASK] [MASK] , né le 31 mars 1596 à [MASK] [MASK] [MASK] [MASK] ( localité rebaptisée [MASK] par la suite ) et mort à [MASK] dans le palais royal de [MASK] le 11 février 1650 , est un mathématicien , physicien et philosophe français .
Sa pensée a pu être rapprochée de la peinture de [MASK] [MASK] pour son caractère clair et ordonné .
Elle se caractérise par sa simplicité ( [MASK] la résume en peu de règles , quatre en tout dans le [MASK] [MASK] [MASK] [MASK] ) et prétend rompre avec les interminables raisonnements scolastiques .
[MASK] se rallie au système cosmologique copernicien qui a révolutionné la science , à l' instar de [MASK] ; mais , par peur de la censure , il " avance masqué " ( larvatus prodeo ) , diluant ses idées nouvelles sur l' homme et le monde dans ses pensées métaphysiques , qui révolutionneront à leur tour la philosophie et la théologie .
L' influence de [MASK] sera déterminante sur tout son siècle : les grands philosophes qui lui succèderont développeront leur propre philosophie par rapport à la sienne , soit en opposition ( [MASK] , [MASK] , [MASK] , [MASK] ) , soit en accord ( [MASK] , [MASK] ) .
[MASK] affirme un dualisme radical entre l' âme ( la res cogitans , la pensée ) et le corps ( la res extensa , l' étendue ) .
[MASK] [MASK] est né le 31 mars 1596 à [MASK] [MASK] [MASK] [MASK] ( aujourd'hui rebaptisée " [MASK] " ) , sa mère ayant quitté la ville de [MASK] où s' était déclarée une épidémie de peste .
Il y apprend la physique et la philosophie scolastique et étudie avec intérêt les mathématiques ; il ne cesse de répéter , en particulier dans son [MASK] [MASK] [MASK] [MASK] combien ces études lui paraissent incohérentes et fort impropres à la bonne conduite de la raison .
De cette période , nous ne conservons qu' une lettre d' authenticité douteuse ( peut-être de l' un de ses frères ) , lettre que [MASK] aurait écrite à sa grand-mère .
En 1616 , il obtient son baccalauréat et sa licence de droit à l' université de [MASK] .
Après ses études , il part vivre à [MASK] .
Il fait la même année la connaissance du physicien [MASK] .
Le physicien [MASK] , promu en véritable détecteur de talent , tient un journal de recherches , il y relate les idées sur les mathématiques , la physique , la logique , la chimie etc .
que [MASK] lui communique au gré de leurs discussions à bâton rompu ou de leurs échanges épistolaires ; [MASK] consacre ses heures de loisir , mais aussi de veille imposée au corps de garde , à la lecture et à la réflexion , à l' étude des mathématiques et des phénomènes physiques .
Toujours est-il que dans le contexte qui suivit la condamnation des écrits favorables à l' héliocentrisme ( 1616 ) , en [MASK] et en [MASK] , on parlait beaucoup des idées de cette prétendue fraternité .
C' est pendant ses quartiers d' hiver ( 1619 -- 1620 ) à [MASK] que se révèle à lui une pensée décisive pour sa vie .
[MASK] en a fait le récit , dont voici le début :
Il fait alors vœu d' un pèlerinage à [MASK] [MASK] [MASK] à [MASK] ( accompli finalement en 1623 ) , renonça à la vie militaire et , de 1620 à 1622 , il voyage en [MASK] et en [MASK] , puis revient en [MASK] .
Ce qu' il a écrit pendant cette période se trouve dans un petit registre mentionné dans l' inventaire fait à [MASK] après sa mort , mais il est aujourd'hui perdu .
Il nous est néanmoins connu par [MASK] et par [MASK] qui en avait fait des copies .
En 1622 , [MASK] estime sa fortune suffisante pour ne pas avoir à travailler ; il règle ses affaires de famille et recommence à voyager , visitant l' [MASK] .
De l' été 1625 à l' automne 1627 , [MASK] est de nouveau en [MASK] .
Il rencontre le père [MASK] [MASK] à [MASK] et commence à être connu pour ses inventions en mathématiques .
Mais , à l' automne 1627 , chez le nonce du pape , le cardinal de [MASK] lui fait obligation de conscience d' étudier la philosophie .
Il part alors à la campagne , en [MASK] , pendant l' hiver 1627 -- 1628 .
C' est de cette époque ( 1622 -- 1629 ) que datent divers traités de mathématiques ( sur l' algèbre , l' hyperbole , l' ellipse , la parabole ) connus par le journal de [MASK] , et d' autres petits traités qui sont perdus .
Cherchant la solitude , il décide de s' installer dans les [MASK] ; il y fait d' abord un bref séjour à l' occasion duquel il va voir [MASK] , mais revient probablement à [MASK] pendant l' hiver 1628 , puis s' installe définitivement en [MASK] au printemps 1629 .
Enfin , [MASK] veut expliquer tous les phénomènes de la nature : il étudie les êtres vivants et fait de nombreuses dissections à [MASK] pendant l' hiver 1631 -- 1632 .
Les observations anatomiques de [MASK] nous sont connues par les copies de [MASK] et des fragments .
Dans sa lettre à [MASK] du 4 novembre 1630 , [MASK] dit songer à faire un traité de morale .
La biographie du religieux [MASK] montre qu' il est l' animateur incontournable de la vie scientifique à [MASK] et un des premiers vigoureux partisans de la pensée de [MASK] en [MASK] , alors que ce dernier voyageur n' a publié aucun ouvrage phare .
En novembre 1633 , [MASK] apprend que [MASK] a été condamné .
Il renonce par prudence à publier le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] qui ne paraîtra qu' en 1664 .
Mais l' ouvrage de [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , fut condamné le 22 juin 1633 .
[MASK] reçoit de [MASK] l' année suivante ( 1634 ) le livre de [MASK] qui lui valut cette condamnation .
À la fin de 1633 , [MASK] quitte [MASK] pour [MASK] ; en 1635 , il est à [MASK] .
Ils l' accusent également d' athéisme et n' hésitent pas à pourfendre en chaire le philosophe , à réclamer qu' il subisse le sort réservé à [MASK] en 1619 à [MASK] [MASK] [MASK] .
Il rencontre [MASK] [MASK] [MASK] , fille de l' [MASK] [MASK] détrôné en exil en [MASK] , en 1643 , et commence une abondante correspondance avec la jeune femme , traitant notamment d' éthique .
Pour le philosophe catholique [MASK] [MASK] , cette question " n' a aucun intérêt " .
En 1667 , les restes de [MASK] furent rapatriés en [MASK] .
Depuis 1819 , sa tombe est à l' [MASK] [MASK] , à [MASK] .
Il y a au XVII e siècle une influence des courants philosophiques du stoïcisme , de l' augustinisme et du scepticisme -- plus particulièrement en ce qui a trait à l' influence de [MASK] , qui constitue à cet égard une figure représentative du doute et du scepticisme qui anime l' époque .
Son ami le pape [MASK] [MASK] commue sa peine en assignation à résidence .
Avec [MASK] , les outils mathématiques permettent le développement d' une science nouvelle , la dynamique , issue de l' astronomie et de la physique .
[MASK] , avide de connaissances , s' interrogea sur la place de la science dans la connaissance humaine .
Il approuvait le projet de [MASK] de rendre compte de la nature en langage mathématique , mais il lui reprochait son manque de méthode , d' ordre , et d' unité .
Après le procès de [MASK] , le projet philosophique de [MASK] se présente alors en trois étapes principales correspondant aux trois œuvres suivantes :
[MASK] commença donc par élaborer une méthode qu' il voulait universelle , aspirant à étendre la certitude mathématique à l' ensemble du savoir , et espérant ainsi fonder une mathesis universalis , une mathématique universelle .
C' est l' objet du [MASK] [MASK] [MASK] [MASK] ( 1637 ) .
Toutefois , dans les méditations , [MASK] semble montrer des réticences à s' étendre sur la notion scolastique de substance , qui se trouve pourtant au cœur de la métaphysique .
[MASK] revient sur ce qui est immédiatement évident , à savoir la condition de la connaissance .
Il existe donc pour [MASK] des propositions simples qui , dès qu' elles sont pensées , sont tenues pour vraies : rien ne produit rien , une seule et même chose ne peut à la fois être et ne pas être , etc .
[MASK] passe en revue les moyens d' accès à la connaissance , indiquant dans la huitième règle :
Pour parvenir à la certitude , tout doit être reconstruit ; [MASK] va ainsi s' efforcer de bâtir la science en un fonds qui soit tout à lui .
La méthode sera pour [MASK] le point de départ de toute philosophie , car elle " prépare notre entendement pour juger en perfection de la vérité et nous apprend à régler nos volontés en distinguant les choses bonnes d' avec les mauvaises .
La grande préoccupation de [MASK] est ainsi d' atteindre la certitude .
[MASK] publia des extraits du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et introduisit une préface , intitulée discours de la méthode , pour bien conduire sa raison et chercher la vérité dans les sciences , qui est restée célèbre .
C' est à partir de ses intuitions des principes que [MASK] propose de raisonner , c' est-à-dire de nous avancer dans la connaissance au moyen de la déduction .
La méthode de [MASK] ne prétend pas déduire a priori les phénomènes .
Ainsi [MASK] est-il rationaliste quand il estime que la déduction est par elle-même suffisante pour valider la connaissance , et que ce sont les causes prouvées par l' expérience qui expliquent l' expérience .
Cependant , lorsque l' expérience n' est pas conforme à ses principes , [MASK] préférera privilégier les principes plutôt que de se plier à la réalité des résultats expérimentaux , parfois à l' excès .
Sur ce point , [MASK] s' opposera au cartésianisme , attribuant la plus grande importance à l' adéquation entre les théories scientifiques et les faits expérimentaux , quitte pour cela à ne pas former d' hypothèses ( par exemple sur la nature de la force gravitationnelle ) .
La science est certes pour [MASK] un système hypothético-déductif s' appuyant sur l' expérience , mais il reste que pour lui il devrait être possible de comprendre le monde physique par une théorie explicative complète prenant la forme d' une algèbre universelle .
La certitude que [MASK] se propose de trouver est au contraire absolue , et c' est une certitude analogue à celle des démonstrations mathématiques qui nous font voir avec évidence que la chose ne saurait être autrement que nous la jugeons et qui ne donne pas prise au scepticisme :
Ainsi , par le nom de science , [MASK] n' entend-il rien d' autre qu' une connaissance claire et distincte .
[MASK] [MASK] de départ de la théorie de la connaissance , ce qui sera retenu tout particulièrement par un cartésien comme [MASK] [MASK] , c' est la simplicité et la clarté des premiers éléments .
Il y a ainsi pour [MASK] une unité de la méthode , et il ne peut y avoir qu' une méthode vraie qui exprime l' unité et la simplicité essentielle de l' intelligence : la méthode en est la manifestation ordonnée .
Les règles de la méthode sont ainsi présentées par [MASK] dans [MASK] [MASK] [MASK] [MASK] [MASK] :
Le doute méthodique et le cogito avaient été esquissés dans le [MASK] [MASK] [MASK] [MASK] .
Les contemporains demandèrent à [MASK] de plus amples explications sur sa métaphysique .
Avant la publication , il demanda à son correspondant , [MASK] [MASK] , de recueillir les objections des plus grands esprits de l' époque ( 1640 ) .
Parmi les connaissances que nous avons dans notre esprit , [MASK] distingue celles que nous avons reçues dès le plus jeune âge et celle que l' on apprend dans les livres ou par des maîtres .
Au contraire , chez [MASK] , le doute n' est qu' un moment , fondateur , dans le cheminement de la connaissance .
Il se distingue du doute des sceptiques ou de celui de [MASK] , et ne repose pas sur la mise en question de l' objet lui-même ( de son existence ) mais du rapport du sujet à l' objet .
Chez [MASK] , le doute ne consiste donc pas en la suspension du jugement , mais il consiste au contraire à juger comme faux ce qui apparaît comme seulement probable .
L' argument du rêve permet à [MASK] de rejeter comme faux toutes les perceptions sensibles , puisque , comme l' expérience en atteste , nos sens peuvent parfois se révéler trompeurs .
Par cette remarque d' apparence anodine , [MASK] évacue l' essentialisme de la nature humaine : il est faux d' affirmer que je suis un animal rationale ( un animal raisonnable ) , comme le dit une définition classique de l' homme , car je ne sais ni ce qu' est un animal , ni ce qu' est la raison , ni encore moins comment elle se trouve en l' homme .
[MASK] est donc parvenu à une certitude première , mais il apparaît pour le moins difficile d' en déduire une connaissance quelconque .
[MASK] semble maintenant enfermé dans ce que l' on nomme le " solipsisme " .
La question est alors de savoir si nous pouvons donner un fondement réel , objectif à notre connaissance , ce que [MASK] affirme :
[MASK] analyse alors les idées que nous avons , indépendamment de leur vérité ou de leur fausseté ; il les examine ainsi en tant qu' elles sont dans la pensée , en tant que représentation ( c' est-à-dire en tant qu' elles ont un esse objectivum ) .
[MASK] se place ainsi en deçà du vrai et du faux par une distinction radicale et anti scolastique de l' esse objectivum et de l' esse formale .
Toutes ces idées doivent avoir une cause , car c' est un principe postulé par [MASK] que tout effet doit avoir une cause ( principe de causalité ) ; nous allons voir qu' il utilise également ce principe ontologique suivant lequel un effet ne renferme pas plus de réalité que sa cause .
Nous avons en nous , selon [MASK] , l' idée d' un être infini , somme de toutes perfections et de toutes réalités .
Le raisonnement de [MASK] postule alors certains axiomes , et peut se formuler ainsi :
Pour résoudre cette difficulté , [MASK] distingue entre ce qui a une cause hors de soi ( substance au sens large ) et ce qui a sa cause en soi ( la substance per se ) .
[MASK] opère ainsi la synthèse entre la notion de substance et celle de cause de soi-même .
Pour [MASK] , cette objection ne tient pas compte du fait que toute finitude est une limitation , une négation : un néant d' être .
[MASK] établit une classification des connaissances en comparant la connaissance à un arbre :
Pour [MASK] , la seule pensée ne peut être la cause de mon existence en tant que chose pensante : il faut un acte qui me crée , en tant que substance pensante , et me maintienne dans l' existence .
Une objection fut formulée par [MASK] et par [MASK] [ réf. nécessaire ] : nous ne connaissons que des qualités ( des attributs , des phénomènes ) : nous n' avons aucune perception immédiate de la substance .
[MASK] accorde toutefois que nous ne percevons , en tant que tel , aucune substance ; il soutient que nous pouvons néanmoins la penser , et que nous pouvons la connaître par ses attributs .
Il n' y a pas d' attribut sans substance chez [MASK] : " le néant ne peut avoir aucuns attributs , ni propriétés ou qualités : c' est pourquoi , lorsqu' on en rencontre quelqu' un , on a raison de conclure qu' il est l' attribut de quelque substance , et que cette substance existe " ( ibid. ) : autrement dit , il ne peut y avoir de pensée sans sujet pensant .
[MASK] critiquera précisément ce point-ci , refusant la possibilité d' inférer de la pensée au sujet pensant .
L' âme est pour [MASK] une substance indépendante , et seul l' homme a une âme .
Il y a pour [MASK] une grande différence entre l' âme et le corps : l' âme est une substance pensante ( res cogitans ou " chose qui pense " ) , la matière est une substance étendue ( res extensa ou " chose étendue " ) .
En partant du cogito , [MASK] fait de la conscience de soi un fait primitif .
L' âme et le corps sont donc dans une certaine communauté , et leur indépendance réciproque affirmée par [MASK] rend cette union inintelligible [ réf. nécessaire ] .
[MASK] admet ces difficultés : en effet , dit-il [ réf. nécessaire ] , nous ne pouvons comprendre cette union , mais nous en avons néanmoins l' expérience tout au long de notre vie .
Principalement , la distinction opérée par [MASK] entre les phénomènes purement spirituels et les phénomènes qui résultent de l' influence du corps sur l' âme .
On a alors un mécanisme analysé ainsi par [MASK] :
Seule la pensée est active , en ce sens qu' elle n' a pas besoin de mouvements matériels : selon [MASK] , la pensée est possible sans la perception et sans l' imagination .
[MASK] opère une distinction semblable en ce qui concerne nos actions : l' appétition est un mouvement produit par le corps , alors que la volonté appartient à l' âme seule .
Cette différence de perfection entre la volonté et l' entendement permet à [MASK] de faire une " psychologie de l' erreur " : l' erreur se produit lorsque nous donnons notre assentiment à quelque chose que notre entendement ne conçoit pas clairement et distinctement .
[MASK] n' a pas souhaité écrire de traité de morale :
[MASK] propose donc dans le [MASK] [MASK] [MASK] [MASK] une " morale par provision " , en attendant de trouver mieux .
La métaphysique est pour [MASK] le fondement de toutes les sciences .
[MASK] souligne par là l' importance qu' il accorde à la métaphysique , mais il s' agit d' une métaphysique subjective reposant sur des objets abstraits .
[MASK] ne se destinait pas à une carrière philosophique .
Ce furent la controverse ptoléméo-copernicienne et le procès de [MASK] ( 1633 ) qui orientèrent sa carrière vers la philosophie .
Quelques philosophes aux [MASK] puis en [MASK] ont suivi [MASK] ( voir cartésianisme ) .
Le dualisme de substance développé par [MASK] , a posé des difficultés à ses successeurs .
[MASK] explicita une théorie de la substance , tandis que [MASK] développa une philosophie originale sur le problème corps-esprit , l' occasionalisme , dans lequel intervient la foi .
La philosophie de [MASK] continue d' alimenter les débats .
Dans les années 1960 -- 1996 , on compte 4402 publications sur [MASK] , dont 1745 dans le monde anglo-saxon , et 1334 francophones .
[ ... ] conviennent que le problème des relations entre l' esprit et le corps , est un casse-tête philosophico-scientifique d' une importance énorme , et que les idées émises par [MASK] ont influé d' une façon extraordinaire sur les approches ultérieures de ce problème , pour le meilleur et pour le pire. "
Sur le plan scientifique , [MASK] contribue à une évolution importante en mathématiques , par l' unification entre le domaine géométrique et le domaine numérique , et parachève le formalisme symbolique engagé par [MASK] [MASK] dans son algèbre nouvelle .
À peu près toutes les théories de [MASK] se sont révélées fausses et les cartésiens , se basant sur ces principes , se sont opposés à l' introduction de la mécanique newtonienne en [MASK] .
On retiendra néanmoins la généralisation qu' il donne au principe d' inertie de [MASK] , et la loi de la réfraction , même s' il fait reposer cette dernière sur des principes erronés .
Sa théorie de la conservation de la quantité de mouvement , fausse , doit être rectifiée par ses successeurs , tels que [MASK] .