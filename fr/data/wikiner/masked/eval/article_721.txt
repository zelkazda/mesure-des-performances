[MASK] [MASK] ( 1875 -- 1960 ) linguiste et celtologue français .
Élève d' [MASK] [MASK] , il a enseigné à l' [MASK] [MASK] [MASK] [MASK] [MASK] , où il a occupé la chaire de langues et littératures celtiques .
Il a également enseigné la linguistique à la faculté des lettres de l' [MASK] [MASK] [MASK] à partir de 1907 ainsi qu' à l' [MASK] [MASK] [MASK] ( 1920-1936 )
[MASK] a notamment développé les notions de linguistique idiosynchronique et d' idiolecte .
Il fut membre de l' [MASK] [MASK] [MASK] [MASK] [MASK] .
Une partie de sa bibliothèque , léguée à la bibliothèque de linguistique de l' [MASK] [MASK] [MASK] , se trouve désormais à la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .