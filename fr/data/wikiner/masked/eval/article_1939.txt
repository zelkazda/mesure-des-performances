Les [MASK] sont la deuxième constellation résultante par la taille .
[MASK] l' avait répertoriée dans son [MASK] .
À cause de sa taille ( 1 884 degrés carrés ) et de son étendue ( 70° d' est en ouest ) , [MASK] [MASK] [MASK] la divisa en trois constellations en 1752 afin de pouvoir la manier plus aisément .
La [MASK] [MASK] [MASK] est l' une des parties résultantes .
α et β ayant échu à la [MASK] , [MASK] [MASK] ( ou [MASK] [MASK] [MASK] ) est l' étoile la plus brillante de la [MASK] [MASK] [MASK] .
[MASK] [MASK] est parfois appelée [MASK] , en l' honneur de l' astronaute [MASK] [MASK] qui périt dans l' incendie de la capsule [MASK] [MASK] en 1967 , en écrivant son prénom à l' envers .
[MASK] [MASK] ( [MASK] ) est une supergéante orange , aussi grande que l' orbite de la [MASK] .
La [MASK] [MASK] [MASK] s' étend en grande partie sur les [MASK] ( elle déborde un peu sur la [MASK] ) .
[MASK] ( [MASK] ) , aussi appelé [MASK] [MASK] , résulte également d' une supernova , en l' occurrence celle qui a produit le [MASK] [MASK] [MASK] ( ou [MASK] [MASK] ) , un des premiers pulsars découverts et un des plus étudiés encore à l' heure actuelle .
Un rémanent plus jeune que [MASK] ( [MASK] ) a été découvert , recouvrant partiellement celui-ci .
En 1999 , une nova fut observée dans la [MASK] [MASK] [MASK] .