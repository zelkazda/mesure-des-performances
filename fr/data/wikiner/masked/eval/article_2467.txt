Il décline lentement par la suite , avant de se replier sur son foyer de [MASK] méridionale où il disparaît aux débuts de notre ère .
Le cunéiforme a été un élément marqueur des cultures du [MASK] [MASK] qui ont développé un rapport à l' écrit et des littératures à partir de ce système .
Sa redécouverte à l' époque moderne et sa traduction au XIX e siècle ont donné naissance aux disciplines spécialisées dans l' étude des civilisations du [MASK] [MASK] , à commencer par l' assyriologie , et ainsi de mettre en lumière les accomplissements de ces civilisations jusqu' alors oubliées .
La [MASK] étant une région pauvre en matériau , ses habitants n' avaient pas un vaste choix d' instruments utilisables pour écrire .
On inscrivait également des poteries , briques ( en argile ou quelquefois les pierre des édifices , des objets en métal , des rochers naturels ( comme à [MASK] ) .
Ce dernier point ne concerne pas à proprement parler la graphie cunéiforme , qui apparaît plus tardivement , mais le fait que celle-ci dérive du premier système d' écriture élaboré en basse [MASK] fait qu' elle n' est pas compréhensible sans prendre en compte l' origine de l' écriture dans cette région .
Un syllabaire simplifié en perse , écrit en cunéiforme , fut développé par les scribes des rois [MASK] , mais sans grand succès .
À l' inverse , ceux de [MASK] gardent les traits obliques , et leurs signes restent plus proches de ce qu' ils étaient aux périodes précédentes .
En [MASK] même , l' écriture alphabétique prend de plus en plus de poids dans les empires néo-assyrien et néo-babylonien , et les reliefs et fresques d' [MASK] présentent souvent un scribe écrivant sur papyrus ou parchemin à côté d' un scribe écrivant sur tablette , signe de la cohabitation des deux systèmes .
Les [MASK] choisissent l' araméen comme langue officielle , achevant le processus entamé par leurs prédécesseurs .
Il existe cependant certains mots ( écrits phonétiquement ou idéographiquement ) pour lesquels le déterminatif est toujours utilisé , comme les noms de certaines villes ( déterminatif postposé [MASK] ) .
Les [MASK] auraient donc joué à leur tour le rôle de " passeurs " de l' écriture cunéiforme .
On a déjà vu plus haut le rôle des rois hittites dans la pratique de l' écriture en [MASK] [MASK] .
Le rôle de l' administration des empires peut aussi être important : sous les rois d' [MASK] est pratiquée une homogénéisation de l' écriture employée dans l' administration impériale , tandis que pour les affaire locales la division régionale continue .
La [MASK] n' était alors plus la région d' origine d' un pouvoir politique fort supportant son antique écriture .
Seuls quelques voyageurs visitant la [MASK] et la [MASK] ramenaient parfois des pierres inscrites , dont ils ignoraient presque tout .
[MASK] [MASK] [MASK] , originaire de [MASK] , est le premier à recopier des inscriptions cunéiformes sur le site de [MASK] en 1621 .
En 1771 , le danois [MASK] [MASK] ramène également des copies d' inscriptions cunéiformes de son voyage en [MASK] et en [MASK] .
Les premiers éléments pour la traduction du cunéiforme sont avancés en 1802 par un philologue allemand , [MASK] [MASK] [MASK] .
Utilisant l' intuition de certains de ces prédécesseurs qui avaient émis l' hypothèse que certaines des inscriptions venues de [MASK] dataient de la période des rois [MASK] , il analysa certaines inscriptions de [MASK] en devinant qu' il s' agissait d' inscriptions royales , puis isola le terme le plus courant , qu' il identifia comme signifiant " roi " ; il identifia également les groupes de signes voisins du précédent comme étant le nom des rois , en se basant sur les noms connus par les historiens grecs antiques .
Avec l' [MASK] [MASK] [MASK] , on disposait alors d' un corpus de signes très conséquent .
Une fois le vieux-perse cunéiforme , on allait pouvoir tenter le déchiffrement des deux autres écritures , à l' image de ce qu' avait fait [MASK] [MASK] avec la [MASK] [MASK] [MASK] une vingtaine d' années auparavant .
En utilisant la version perse de l' [MASK] [MASK] [MASK] , il identifia plusieurs signes et confirma qu' on était en présence d' un système à dominante syllabique , avant de découvrir la nature idéographique d' autres signes , ainsi que leur polysémie .
[MASK] établit de son côté le caractère polyphonique des signes et identifia aussi des homophones , ce qui confirma la complexité de ce système d' écriture .
En 1857 , les avancées dans le déchiffrement du cunéiforme font penser que les mystères de ce système d' écriture ont été vaincus , alors que les premiers chantiers de fouilles dans l' ancienne [MASK] ont livré une grande quantité de tablettes cunéiformes .
L' assyriologie en tant que discipline était née , et prenait le nom du peuple dont on déchiffrait alors les textes , puisque ni [MASK] ni les cités de [MASK] n' avaient encore été mises au jour .
Au même moment , on établissait après de longues querelles l' identité des probables inventeurs du système d' écriture mésopotamien d' où dérive le cunéiforme , les [MASK] .
D' autres langues sont ensuite identifiées dans un système cunéiforme similaire , en plus de celle d' [MASK] connue en même temps que l' akkadien grâce à l' [MASK] [MASK] [MASK] , mais encore mal connue de nos jours .
Le dernier système d' écriture cunéiforme à être déchiffré est celui de l' [MASK] [MASK] [MASK] , identifié sur des tablettes et objets provenant de cette cité découverte en 1929 .
Quand un son peut être marqué par plusieurs signes , on emploie un système facilitant leur différenciation des signes en les numérotant , mis au point par [MASK] [MASK] .