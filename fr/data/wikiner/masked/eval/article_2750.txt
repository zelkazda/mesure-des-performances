[MASK] contre-attaqua [MASK] [MASK] et le battit .
En effet , d' après [MASK] [MASK] [MASK] , [MASK] considère que les femmes de ses frères ne sont pas dignes de leur rang , et que seule la fille d' un roi peut prétendre au titre de reine d' [MASK] .
Sous l' influence de [MASK] et des prêtres , [MASK] dut se convertir au catholicisme .
En 567 , [MASK] , la sœur de [MASK] et femme de [MASK] [MASK] , roi de [MASK] son frère , est assassinée ( étranglée dans son lit , probablement sur ordre de [MASK] , la future femme de [MASK] [MASK] .
[MASK] , influencé par [MASK] , est décidé à venger sa belle-sœur : c' est le début d' une guerre entre [MASK] et [MASK] qui durera fort longtemps , survivant même à la mort de [MASK] .
Il fit attaquer par surprise la ville d' [MASK] , propriété de [MASK] , qu' il voulait récupérer lors du partage du royaume de [MASK] [MASK] .
[MASK] prit la fuite sous les sons des cors et trompettes comme lors d' une chasse au cerf .
Un concile fut mené à [MASK] pour statuer de l' affaire , mais n' aboutit guère .
Il menace [MASK] , qui a établi un traité de secours mutuel avec [MASK] [MASK] par peur des barbares , de lui faire la guerre si celui-ci ne le laisse pas passer .
Mais les barbares alliés de [MASK] pillent les environs de [MASK] et [MASK] doit les haranguer pour imposer le calme .
Il retourne à [MASK] pour rejoindre la reine [MASK] et ses enfants .
À [MASK] , l' armée de [MASK] le reconnaît comme roi de [MASK] ; mais alors qu' il part se faire sacrer roi , il est poignardé par des pages de [MASK] à coup de scramasaxe de chaque côté dans le flanc .
[MASK] est mort à l' âge de 40 ans , après quatorze années de règne .
Lors du règne de [MASK] , la charge de maire du palais est pour la première fois mentionnée .