[MASK] est un roman de science-fiction de l' écrivain français [MASK] [MASK] , publié en 1957 .
Parmi ceux-ci , une tribu a élu domicile dans l' ancien [MASK] [MASK] [MASK] , aux abords du continent nord-américain .
Le style de l' auteur , sobre et précis , et la relative simplicité de la narration font de [MASK] un roman accessible à un public très large .
Dans le monde de [MASK] , la planète [MASK] devenue invivable a été abandonnée par sa population humaine au profit de [MASK] .
Accompagné de l' ours qu' il a réussi à apprivoiser en chemin , l' enfant noir rejoint les siens et les guide vers le nord , à la recherche de la ville de [MASK] .
L' enfant noir arrive seul devant les portes de la grande ville de [MASK] .
La ville de [MASK] impressionne beaucoup l' enfant noir par ses très hautes tours , sa gigantesque statue de femme et ses robots .
[MASK] [MASK] propose dans [MASK] deux visions concurrentes de l' évolution possible de l' espèce humaine .
La première concerne l' émigration des populations humaines vers [MASK] qui s' est accompagnée d' une disparition de la reproduction humaine , considérée dès lors comme animale et primitive et remplacée par une forme de clonage donnant des êtres anthropoïdes asexués .
[MASK] de [MASK] [MASK] a connu différentes éditions françaises :