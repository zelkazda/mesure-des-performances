[MASK] [MASK] est né en 1948 à [MASK] , près de [MASK] [MASK] en [MASK] [MASK] [MASK] .
[MASK] [MASK] vit ce déménagement comme un bond en arrière dans le passé .
Un nouveau traumatisme que [MASK] surmonte , une fois encore , par la lecture .
[MASK] a 18 ans , et voit ainsi se matérialiser l' une de ses pires angoisses .
En 1968 , il s' enfuit au [MASK] pour éviter d' être envoyé au [MASK] [MASK] et s' installe en 1972 à [MASK] .
[MASK] , comme les autres , commence à écrire des nouvelles qui attirent l' attention .
Dans les années 80 ses fictions se développent sur le mode du film noir ; des nouvelles publiées dans le magazine [MASK] commencèrent à esquisser les thèmes qu' il développera dans son premier roman , [MASK] .
C' est [MASK] , avec [MASK] , qui décroche le premier un immense succès littéraire .
Les trois romans de cette seconde trilogie sont : [MASK] [MASK] , [MASK] et [MASK] [MASK] [MASK] [MASK] .
Plus récemment , [MASK] [MASK] s' est quelque peu éloigné du genre des dystopies fictionnelles qui le rendirent célèbre pour davantage privilégier un style d' écriture plus réaliste , troquant les sauts narratifs caractéristiques de sa première manière contre un flux d' écriture plus continu .
Comme [MASK] [MASK] l' avait dit dans son blog , la disquette devait se " manger elle-même " après avoir été lue .
[MASK] [MASK] commença à rédiger son blog à partir de 2003 qui resta actif jusqu' en 2005 , avec une seule grosse coupure .
[MASK] écrivit également quelques éléments d' anticipation pour [MASK] dont certains furent intégrés au film du même nom .
Il fit par ailleurs une apparition à l' écran dans la mini-série [MASK] [MASK] , une série largement influencée par l' œuvre de [MASK] et d' autres auteurs cyberpunk .
Son roman l ' [MASK] [MASK] [MASK] est actuellement en cours d' adaptation au cinéma par [MASK] [MASK] et devrait sortir en 2008 .