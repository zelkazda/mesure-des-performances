[MASK] [MASK] est une femme politique française , née le 10 mai 1945 à [MASK] ( [MASK] ) .
D' abord directrice commerciale dans une PME de 500 personnes à [MASK] , elle revient vivre et travailler en [MASK] où elle exerce le métier de professeur de sciences économiques .
En 1982 , elle est nommée chargée de mission départementale aux droits des femmes auprès du préfet des [MASK] .
[MASK] [MASK] s' est engagée aux côtés de [MASK] [MASK] depuis fin 2005 .
Elle demande à [MASK] [MASK] d' entrer dans son comité de campagne national et la nomme directrice adjointe de sa campagne .
Parallèlement elle est co-présidente du comité de soutien départemental de [MASK] [MASK] dans les [MASK] .
Elle est candidate en 2007 comme première des socialistes aux municipales pour la ville de [MASK] .
Elle perd au second tour face au candidat centriste et maire sortant [MASK] [MASK] .
Après la débacle rencontrée par la liste qu' elle présente pour les municipales de 2008 , [MASK] [MASK] décide de se retirer de la vie politique locale et démissionne du conseil municipal .