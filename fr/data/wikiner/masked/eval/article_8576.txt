Une production commerciale à grande échelle s' est par la suite développée à [MASK] et dans les îles des [MASK] .
Actuellement la production se fait pour l' essentiel dans [MASK] [MASK] , [MASK] et [MASK] .
Avec 50 à 70 tonne d' essence , [MASK] [MASK] tiennent le rang de premier producteur mondial .
L' ylang-ylang constitue la principale culture de rente de [MASK] , dont il représente 84 % du total des exportations .
Aux [MASK] , ces mêmes fleurs tressées avec des fleurs de sampiguita servent à confectionner des colliers portés par les femmes ou disposés autour d' images saintes .
Les armoiries de [MASK] , adoptées en 1982 , comprennent deux fleurs d' ylang-ylang sur l' écu central .