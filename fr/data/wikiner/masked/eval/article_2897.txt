Il est député de la [MASK] [MASK] [MASK] [MASK] depuis 2002 et maire de la ville de [MASK] depuis 1995 .
[MASK] [MASK] est consultant en communication et relations publiques .
Il a été élu et réélu maire de la ville de [MASK] depuis 1995 sans discontinuité , sur la liste [MASK] puis de l' [MASK] , après avoir été conseiller municipal puis adjoint au maire [MASK] [MASK] durant de 12 années .
Il fait partie du groupe [MASK] .
Il copréside également avec [MASK] [MASK] le groupe d' études sur les pouvoirs publics et groupes d' intérêt .