Son livre [MASK] à [MASK] , publié suite au procès d' [MASK] en 1961 , a suscité controverses et polémiques .
[MASK] [MASK] est née à [MASK] en 1906 .
Des deux côtés , les grands-parents étaient des [MASK] laïcs .
En 1925 , sa rencontre avec [MASK] sera un évènement majeur de sa vie , tant sur le plan intellectuel que sentimental .
C' est le début d' une relation secrète , passionnée et irraisonnée , qui laissera des traces chez [MASK] toute sa vie .
Par-delà la guerre et l' exil , [MASK] [MASK] se fera l' infatigable promoteur du philosophe , aussi éminent que controversé , aux [MASK] .
En 1929 , elle épouse [MASK] [MASK] ( nommé plus tard [MASK] [MASK] ) , un jeune philosophe allemand rencontré dans le milieu universitaire .
La même année , elle obtient une bourse d' études qui lui permet de travailler jusqu' en 1933 à une biographie de [MASK] [MASK] , une [MASK] allemande de l' époque du romantisme , qui ne paraîtra qu' en 1958 .
En 1933 , elle quitte l' [MASK] pour la [MASK] où elle participe à l' accueil des réfugiés fuyant le nazisme .
Depuis [MASK] , elle milite pour la création d' une entité judéo-arabe en [MASK] .
Elle facilite l' immigration des jeunes [MASK] vers la [MASK] .
En mai 1940 , en raison de l' avancée éclair de la [MASK] en [MASK] , elle est internée au [MASK] [MASK] [MASK] ( [MASK] ) avec d' autres apatrides .
À l' issue d' une traversée éprouvante , elle s' installe à [MASK] [MASK] .
Dans une situation de dénuement total , elle doit absolument gagner sa vie , et trouve un emploi d' aide à domicile dans le [MASK] et envisage de devenir assistante sociale .
Après la [MASK] [MASK] [MASK] , elle retourne en [MASK] , travaillant pour une association d' aide aux rescapés juifs .
Elle reprend contact avec [MASK] , témoignant en faveur du philosophe lors de son procès en dénazification .
Elle renoue également avec le couple [MASK] dont elle devient une amie intime .
Après ces trois livres fondamentaux , elle couvre à [MASK] le procès du responsable nazi [MASK] [MASK] , en qui elle voit l' incarnation de la " banalité du mal " .
En 1966 , elle apporta son soutien à la pièce de théâtre de l' allemand [MASK] [MASK] , [MASK] [MASK] , œuvre qui déclencha une violente controverse en critiquant l' action du pape [MASK] [MASK] face à la [MASK] .
Elle meurt le 4 décembre 1975 à [MASK] [MASK] .
Lors des obsèques , son ami [MASK] [MASK] après avoir prononcé le kaddish lui dira : " Avec ta mort tu as laissé le monde un peu plus glacé qu' il n' était. "
Ses réflexions sur l' action ne l' ont pas empêchée de s' interroger sur le rôle de la pensée , en particulier dans [MASK] [MASK] [MASK] [MASK] [MASK] : il ne s' agit plus d' une vita contemplativa , censée permettre d' accéder à la vérité avant de décider comment agir .
Mais c' est surtout par cette purgation par la pensée , qu' il est possible face à un événement dans le domaine public de faire preuve de discernement , de juger ce qui est beau et ce qui est mal ( et c' est faute d' un tel jugement que peut apparaître la banalité du mal comme dans le cas d' [MASK] ) .
Pour [MASK] [MASK] , la pensée la plus haute n' est pas celle qui se réfugie dans la contemplation privée , mais celle qui , après la pensée purgatrice et la volonté légiférante , s' expose dans la domaine public en jugeant les événements , en faisant preuve de goût dans ses paroles et ses actions .
La réflexion politique d' [MASK] [MASK] , appuyée sur la question de la modernité , c' est-à-dire de la rupture du fil de la tradition ( cf .
[MASK] souhaitait penser son époque , et elle s' est ainsi intéressée au totalitarisme .
Son analyse continue à faire autorité , à côté de celle , différente et plus descriptive , de [MASK] [MASK] .
Dans le livre [MASK] [MASK] [MASK] [MASK] elle met sur le même plan stalinisme et nazisme , contribuant ainsi à systématiser le nouveau concept de " totalitarisme " .
Ainsi , pour [MASK] [MASK] , " [ ... ] elle ne parvient pas à élaborer une théorie claire ou une conception satisfaisante des systèmes totalitaires .
Bien qu' elle ait travaillé de nombreuses années au sein d' une organisation sioniste ( lors de son séjour à [MASK] ) , [MASK] [MASK] évolua progressivement au sujet d' [MASK] , et exprima son opposition constante à tout enfermement nationaliste .
Elle entra en conflit avec la presse israélienne lors de sa couverture du procès d' [MASK] à [MASK] au début des années 1960 .
Il fut , en effet , injustement reproché à [MASK] [MASK] d' avoir non seulement présenté [MASK] [MASK] comme un sioniste mais aussi , et surtout , d' avoir fait le reproche aux populations déportées de ne pas s' être suffisamment révoltées contre le sort terrible que les dirigeants nazis leurs réservaient .
Ces critiques infondées , qui résultent d' une lecture erronée de l' œuvre de [MASK] [MASK] , furent largement rejetées par l' auteur .
[MASK] [MASK] ne reproche , en effet , dans [MASK] à [MASK] à aucun moment aux déportés de s' être montrés passifs .
Quant à la seconde critique faite à [MASK] [MASK] lors de la parution de [MASK] à [MASK] , qui portait sur le " sionisme " d' [MASK] [MASK] -- qu' elle était censée avoir voulu mettre en avant -- il s' avère que , comme la précédente , cette critique était infondée .
[MASK] [MASK] ne fait jamais , dans son compte rendu du procès d' [MASK] [MASK] , un sioniste du " spécialiste de la question juive " .
[MASK] [MASK] fut blessée par les accusations auxquelles elle eut à faire face au lendemain de la parution de [MASK] à [MASK] d' autant qu' à ses yeux lesdites accusations relevaient du procès d' intention .