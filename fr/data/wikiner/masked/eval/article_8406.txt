Il passe son enfance à [MASK] , une ville du nord-ouest de l' [MASK] qui est sous la protection de la couronne de [MASK] et de son roi [MASK] [MASK] , où il reçoit une éducation musicale de la part de son père violoniste et hautboïste .
Libéré de ses obligations militaires , il gagne sa vie comme copiste musical à [MASK] puis directeur de la milice de [MASK] .
En 1758 , il obtient la direction des concerts d' [MASK] avant de devenir organiste à [MASK] en 1766 , puis à [MASK] l' année suivante .
Sa musique , assez largement oubliée aujourd'hui , a été redécouverte avec bonheur en 2003 par les [MASK] [MASK] [MASK] , dans un disque consacré aux symphonies .
Les œuvres " galantes " d' [MASK] ont été composées entre 1759 et 1770 : 24 symphonies , une douzaine de concertos ( violon , alto , hautbois , orgue ) , des sonates pour clavecin et de la musique religieuse .
En 1821 il devient le premier président de la [MASK] [MASK] [MASK] .