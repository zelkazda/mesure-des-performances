Le [MASK] est une constellation du zodiaque traversée par le [MASK] du 18 décembre au 18 janvier .
La constellation se situe entre [MASK] à l' ouest et le [MASK] à l' est .
[MASK] était l' une des 48 constellations identifiées par [MASK] .
On l' appelle aussi l' [MASK] ( c' est-à-dire " le porteur d' arc " ) .
De là il passe le long de la diagonale d' [MASK] , puis par [MASK] , [MASK] , [MASK] et [MASK] , [MASK] ( [MASK] , l' extrémité des [MASK] puis [MASK] et [MASK] [MASK] , [MASK] ( [MASK] [MASK] ) , et enfin reboucle sur le [MASK] .
Quand le [MASK] est visible , le [MASK] se repère facilement , par sa forme .
Il se situe dans l' axe des deux " yeux du chat " de la queue du [MASK] .
L' étoile la plus brillante de la constellation est [MASK] [MASK] ( [MASK] [MASK] ) , à la base sud de l' arc du [MASK] .
L' arc du [MASK] est formé de [MASK] [MASK] ( λ ) , [MASK] [MASK] ( δ ) et [MASK] [MASK] ( ε ) .
La [MASK] [MASK] est la plus dense à l' endroit où elle traverse le [MASK] ; c' est aussi là que se trouve son centre .
Par conséquent , le [MASK] contient un bon nombre d' étoiles brillantes , d' amas stellaires , et de nébuleuses .
[MASK] [MASK] ( [MASK] [MASK] ) , l' étoile la plus brillante de la constellation , atteint la magnitude apparente 1,79 ( 36 e du ciel ) .
[MASK] ( [MASK] [MASK] ) est la deuxième étoile de la constellation .
[MASK] [MASK] , la source radio complexe associée au centre de la galaxie se trouve aussi ici .
Les astronomes pensent que [MASK] [MASK] pourrait contenir un trou noir supermassif .