[MASK] [MASK] était une auteur , une activiste et une philosophe de l' architecture et de l' urbanisme .
Elle est née aux [MASK] et demeurait à [MASK] , [MASK] .
[MASK] [MASK] a passé son existence à étudier l' urbanisme .
Elle a changé le cours de l' urbanisme dans de nombreuses villes nord-américaines , y compris [MASK] .
En 1968 , durant la [MASK] [MASK] [MASK] [MASK] , elle quitte les [MASK] avec ses fils afin de leur éviter le service militaire et trouve refuge au [MASK] .