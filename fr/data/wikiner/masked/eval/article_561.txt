Sa capitale est [MASK] .
Le pays fait partie de la [MASK] .
Les premiers contacts européens sont établis , en 1446 , par le navigateur portugais [MASK] [MASK] , tué en prenant pied dans la zone .
Le [MASK] établit quelques comptoirs sur la côte .
Les [MASK] quittent le pays après la [MASK] [MASK] [MASK] en 1974 .
Le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] qui avait mené la lutte politique puis militaire pour l' indépendance remporte les élections .
Le 1 er mars 2009 , le chef d' état-major des forces armées , le général [MASK] [MASK] [MASK] , est tué dans un attentat à la bombe .
Le président [MASK] [MASK] [MASK] , que certains militaires tiennent pour responsable de cet attentat , est assassiné à son tour , le 2 mars 2009 , par des hommes en armes .
La [MASK] est divisée en 9 régions , elles-mêmes partagées en 37 secteurs .
La [MASK] doit son nom à sa capitale , [MASK] , et s' étend sur 36120 km² , 28000 km² de terre et 8120 km² de mer ( ce qui est à peine plus étendu que la [MASK] ) , y compris une soixantaine d' îles dans l' [MASK] , dont l' [MASK] [MASK] [MASK] appelé aussi [MASK] [MASK] [MASK] .
Son littoral , très riche en poissons , attire les pêcheurs de l' [MASK] [MASK] qui viennent pêcher chaque année 500 mille de tonnes de poisson , versant en échange à la [MASK] environ 7500000 € .
Malgré ses nombreux atouts , la [MASK] est le troisième pays le plus pauvre du monde , parmi les pays les moins avancés .
La drogue sud-américaine est donc stockée en [MASK] , où elle est ensuite introduite par petites quantités dans les produits de marché acheminés vers l' [MASK] , ou ingérée par des mules qui risquent leur vie et leur liberté pour 5000 € ( leur salaire pour acheminer 500 grammes à 1 kilogramme de cocaïne en capsules ) .
La [MASK] , loin d' être consommatrice de ces drogues de " luxe " que ses habitants n' ont pas les moyens de s' offrir , est devenue en quelques années la plaque tournante du trafic de cocaïne , moyen pervers d' enrichir quelques particuliers .
La [MASK] a pour codes :