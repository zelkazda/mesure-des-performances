[MASK] , victorieux , retourne à [MASK] et perd un temps précieux avant de réaliser la menace que représente la campagne de [MASK] [MASK] [MASK] .
Il ne doit la vie qu' à un batelier du [MASK] qui le recueille après qu' il s' est enfui en plongeant dans le fleuve .
[MASK] [MASK] [MASK] réduit aussi le [MASK] et réorganise son empire pour mieux le contrôler en le découpant en districts avec , à leur tête , des hommes sûrs qui ne sont responsables que devant lui .
[MASK] [MASK] [MASK] est souvent considéré comme le véritable responsable de l' implantation de l' islam en [MASK] .
Il réforme la monnaie et les taxes pour favoriser le commerce et la circulation des marchandises et , dans le même but , crée des routes , comme la [MASK] [MASK] [MASK] qui mène du [MASK] oriental aux rives de l' [MASK] , ou en améliore d' autres , ce qui profite aussi aux armées en campagne .
Finalement , [MASK] [MASK] [MASK] a posé les bases solides d' un état , mais n' engendrera pas une dynastie véritable pour en jouir .
Les [MASK] [MASK] seront les grands bénéficiaires de ses réalisations et de son esprit visionnaire .