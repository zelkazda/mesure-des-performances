Le reste est recouvert par le [MASK] [MASK] à l' ouest , le [MASK] [MASK] à l' est et le [MASK] au nord-est .
L' [MASK] a subi depuis très longtemps un phénomène de désertification qui voit le désert gagner du terrain sur les terres arables .
On pense ainsi qu' à l' époque de la construction des grandes pyramides , le plateau de [MASK] était recouvert d' une savane ( il est aujourd'hui complètement désertique ) .
La crue annuelle du [MASK] était l' événement majeur de l' année pour les [MASK] de l' antiquité ( d' ailleurs le jour de l' an commence avec les premiers signes de montée des eaux ) .
Aujourd'hui canalisées par le [MASK] [MASK] [MASK] [MASK] , les eaux du [MASK] recouvraient autrefois une grande partie de ses berges , apportant par là même un précieux limon noir qui rendait ses terres fertiles .
Cette crue apportait la prospérité aux [MASK] , mais ses débordements pouvaient être aussi meurtriers que les famines causées par une trop faible montée des eaux .
La quasi-disparition de cette inondation annuelle du [MASK] a également eu une grande répercussions sur l' écosystème de la [MASK] [MASK] [MASK] .
Les frontières " traditionnelles " de l' [MASK] antique sont assez semblables aux frontières de l' [MASK] moderne .
Ce sont principalement les frontières sud avec la [MASK] et nord-ouest qui ont fluctué au cours des siècles .
La domination égyptienne en syro-palestine sera toujours de courte durée et dépassera rarement la [MASK] .