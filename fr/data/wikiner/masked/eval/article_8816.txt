Elle est reliée au continent par l' [MASK] [MASK] [MASK] .
La région , bien que faisant partie de l' [MASK] , est essentiellement russophone .
Elle a fait partie historiquement de l' [MASK] [MASK] , de l' [MASK] [MASK] et de l' [MASK] [MASK] [MASK] [MASK] [MASK] ( de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , puis de la [MASK] [MASK] [MASK] [MASK] [MASK] en 1954 ) , avant d' être rattachée à l' [MASK] indépendante en 1991 .
C' est l' antique [MASK] [MASK] .
On y trouve notamment les villes de [MASK] , [MASK] et la célèbre ville de [MASK] , où furent signés les [MASK] [MASK] [MASK] en 1945 , entre [MASK] , [MASK] et [MASK] .
La [MASK] borde les frontières de la région de [MASK] au nord .
Le reste de ses frontières est la [MASK] [MASK] au sud et à l' ouest , et la [MASK] [MASK] [MASK] à l' est .
Sa capitale est [MASK] .
La [MASK] est reliée au territoire ukrainien par une bande de terre large de 5 à 7 kilomètres appelée l' [MASK] [MASK] [MASK] .
À l' est se trouve la [MASK] [MASK] [MASK] , qui fait face directement à la [MASK] [MASK] [MASK] [MASK] .
Entre les péninsules de [MASK] et [MASK] se trouve le [MASK] [MASK] [MASK] , large de 3 à 13 kilomètres , liant la [MASK] [MASK] à la [MASK] [MASK] [MASK] .
Les côtes de [MASK] sont irrégulières et forment un grand nombre de ports et de baies .
La [MASK] [MASK] [MASK] se trouve dans le nord de la [MASK] [MASK] [MASK] .
Ces montagnes sont voisines d' une seconde rangée [MASK] plus dans l' arrière pays .
Une grande partie de ces montagnes ont des sommets assez abrupts et impressionnants , avec une différence d' altitude élevée par rapport à la mer si proche ( 650 à 750 mètres ) , commençant à la pointe sud de la péninsule , appelée [MASK] [MASK] .
À l' époque de l' [MASK] [MASK] [MASK] [MASK] [MASK] , cette région offrait de nombreux sites de villégiature pour les élites du parti et les travailleurs émérites .
Près d' [MASK] se situe le plus prestigieux parc d' attraction pour enfants de l' [MASK] , l' [MASK] .
La région de [MASK] abrite de nombreuses vignes et vergers .
) , les [MASK] ( 376 ) , les [MASK] , les [MASK] , les [MASK] [MASK] [MASK] [MASK] , les [MASK] ( 1016 ) , les [MASK] ( 1050 ) , les [MASK] [MASK] [MASK] ( 1171 ) , et les [MASK] et [MASK] ( 1237 ) .
Mais les princes , voïvodes et tsars de ces pays devenaient de plus en plus puissants et en 1475 , le khanat se plaça sous la protection de l' [MASK] [MASK] , allié précieux ( et musulman comme la majorité des [MASK] ) .
En 1569 , le khanat attaqua [MASK] , qui était passé sous le contrôle de la [MASK] .
Le [MASK] [MASK] [MASK] qui fut signé fit sortir le [MASK] [MASK] [MASK] du giron ottoman pour le transférer dans l' [MASK] [MASK] .
Dès l' occupation russe , les [MASK] [MASK] [MASK] furent persécutés , déportés ou carrément massacrés , tandis que dans le même temps , la [MASK] envoyait des colons des autres provinces , afin de rendre les [MASK] minoritaires dans leur propre pays .
La [MASK] [MASK] colonisa les territoires qu' elle venait d' enlever à l' [MASK] [MASK] .
Les [MASK] furent chargés de " pacifier " et d' assimiler les [MASK] .
La [MASK] fut intégrée dans le gouvernement de [MASK] ; des nouvelles villes slaves furent édifiées , des voies ferrées construites , des marais assainis .
Cependant , la [MASK] [MASK] [MASK] , qui eut lieu entre 1854 et 1856 , ruina l' économie et les structures sociales de la [MASK] , pendant quelques années .
Ce fut surtout après 1860 que la [MASK] se releva et devint une véritable riviera russe .
Ce fut le début du maillage des stations balnéaires tout autour de la [MASK] [MASK] , et pas seulement en [MASK] .
Beaucoup se sont dirigés et réinstallés dans d' autres provinces de l' [MASK] [MASK] , notamment en [MASK] et en [MASK] .
En 1921 fut créée la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , partie de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
La [MASK] fut la scène de batailles les plus sanglantes de la [MASK] [MASK] [MASK] .
Les [MASK] subirent de nombreuses pertes pendant leur progression vers la [MASK] , notamment sur l' [MASK] [MASK] [MASK] , étroite bande qui relie l' [MASK] à la [MASK] , durant l' été 1941 .
[MASK] résista au siège d' octobre 1941 jusqu' au 4 juillet 1942 .
En 1967 , les [MASK] [MASK] [MASK] furent réhabilités , mais leur exil ne prit pas fin pour autant avant les premiers retours massifs à partir des années 1990 .
La [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] fut abolie en 1945 et transformée en province de [MASK] , toujours partie de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
En 1954 , [MASK] [MASK] ayant lui-même longtemps vécu en [MASK] , offrit la province à la [MASK] [MASK] [MASK] [MASK] [MASK] pour marquer le 300 e anniversaire de la réunification de la [MASK] et de l' [MASK] .
Avec l' effondrement de l' [MASK] [MASK] , il devient difficile à accepter pour la population , majoritairement d' origine russe ou russophone , que la [MASK] , donnée en 1954 à l' [MASK] [MASK] , soit dorénavant une partie intégrante de l' [MASK] indépendante .
Cette situation provoqua de nombreuses tensions entre la [MASK] et l' [MASK] , exacerbées par la présence de l' ex-flotte soviétique ( devenue russe ) de la [MASK] [MASK] sur la péninsule , laissant planer le risque d' un conflit armé .
La [MASK] proclama ses propres lois le 5 mai 1992 , mais décida plus tard de rester au sein de l' [MASK] en tant que république autonome .
La ville de [MASK] possède aussi un statut spécial en [MASK] .
Les [MASK] [MASK] [MASK] ne représentent que 10 % de la population .
On estime à 6 600 000 de locuteurs du [MASK] [MASK] [MASK] , dont près des 90 % se trouvent en [MASK] .
Le rattachement de la [MASK] à l' [MASK] n' avait été reconnu qu' en 1997 par la [MASK] , pour dix ans seulement et uniquement compte tenu du statut autonome de la [MASK] [MASK] [MASK] .
Il en est résulté un clivage à l' intérieur même de l' [MASK] entre les pro-russes et les pro-occidentaux .
La [MASK] exerce une influence certaine dans les affaires intérieures de l' [MASK] , en particulier , à travers la distribution de passeports russes à la population russophone de [MASK] .
Les tensions autour du statut de la ville de [MASK] , majoritairement pro-russe , et les questions relatives au retrait de la flotte russe de la [MASK] [MASK] y stationnant ravivent de nombreuses inquiétudes quant aux relations entre la [MASK] et l' [MASK] .
Elle a subi , à l' instar de l' [MASK] pendant la décennie 1990 , une grave récession qui a amené les pouvoirs publics à tenter de diversifier ses ressources .
Car les ressources minérales jouent un rôle primordial dans l' économie de la [MASK] .
On dénombre pas moins de 250 gisements de 27 minéraux différents , constituant la base de l' industrie minière et de transformation chimique de l' [MASK] .
L' industrie de la [MASK] ne représente plus aujourd'hui que 2 % des revenus de l' industrie ukrainienne .
La ville de [MASK] reste l' une des principales zones d' activité industrielle , puisqu' elle représente près de 10 % de la valeur industrielle de la [MASK] .
Pour autant , le secteur participe pour plus de 35 % à la production viticole de l' [MASK] , 10 % de la production de fruit et 5 % de celle du blé .
La [MASK] profite pourtant de l' amélioration globale de l' économie ukrainienne et a vu son volume total de production croître de 20 % entre 2000 et 2004 .
L' objectif principal des pouvoirs publics est en effet de tertiariser l' économie criméenne , à l' exemple de ce que tente de réaliser le gouvernement de [MASK] .
Le tourisme en [MASK] peut être considéré comme une activité traditionnelle .
Faisant suite à la révolution soviétique , le pouvoir communiste décide de créer une administration touristique centralisée , faisant de la [MASK] le lieu de repos des travailleurs " méritants " et de l' oligarchie , ceci dans la démarche idéologique , culturelle et éducative propre à l' époque .
Les infrastructures principales , notamment hôtelières , sont construites à cette période et concentrées dans quelques villes littorales ( [MASK] , [MASK] ... ) .
À la suite de l' indépendance ukrainienne et de l' autonomie de la [MASK] , les pouvoirs publics décident rapidement de miser sur le secteur touristique , considérant que celui-ci , grâce à son caractère dynamique , peut permettre , à moyen terme , de diversifier l' économie régionale .
La [MASK] possède de nombreuses infrastructures touristiques .
Elle est l' une des régions de l' ancienne [MASK] qui compte le plus de stations balnéaires et thermales .
Ce développement rapide a été possible après la dislocation de l' [MASK] , l' accès au territoire étant devenu largement plus aisé pour les étrangers .
Le tourisme en [MASK] s' est en effet spécialisé sur la vente de produits thérapeutiques , sur le tourisme de santé ( stations thermales ... ) .
Grâce à sa situation , elle joue également un rôle important de point d' escale des croisières de la [MASK] [MASK] .
Les ports sont essentiellement à vocation internationale et permettent de rejoindre les principales villes portuaires de la [MASK] [MASK] .
La [MASK] compte un aéroport international ( [MASK] ) et deux aéroports à vocation régionale et nationale ( [MASK] et [MASK] ) .
[MASK] , qui se situe à la pointe est de la péninsule , est une ville à l' écart des principaux flux touristiques de la région .
L' un des axes du développement touristique de la [MASK] est la mise en valeur du patrimoine naturel .
Il faut pourtant agir vite car la [MASK] traverse une crise sérieuse .
L' [MASK] adhère , depuis 1997 , aux chartes internationales de protection de l' environnement et adopte ses propres lois-cadres permettant de définir , sur le terrain , les principales zones à sauvegarder .