Actuellement maire de [MASK] et député dans les [MASK] , il a également été [MASK] [MASK] [MASK] [MASK] [MASK] du 7 mars au 19 juin 2007 .
Il fait partie du groupe [MASK] .
Dès 2002 , il est président de la commission des affaires économiques , de l' environnement et du territoire de l' [MASK] [MASK] .
Le 7 mars 2007 , il est élu [MASK] [MASK] [MASK] [MASK] [MASK] ( mandat jusqu' au 19 juin 2007 ) .
[MASK] [MASK] est maire de [MASK] et membre d' honneur du [MASK] [MASK] .