En raison du célèbre roman classique [MASK] [MASK] [MASK] [MASK] [MASK] ( ch .
Il est né à [MASK] dans le [MASK] , cadet de quatre fils d' une famille de lettrés .
Bien que né dans une famille essentiellement confucéenne , [MASK] exprime dès son jeune âge le désir de devenir moine bouddhiste comme l' un de ses grands frères .
[MASK] est pleinement ordonné moine en 622 à l' âge de 20 ans .
Il quitte ensuite son frère et retourne à [MASK] pour apprendre des langues étrangères et continuer son étude du bouddhisme .
Pendant ce temps , [MASK] s' intéresse également à l' école métaphysique yogācāra .
En 629 , [MASK] prétend avoir eu un rêve qui l' a convaincu de partir en [MASK] .
La [MASK] [MASK] et les [MASK] orientaux étant en conflit , l' empereur [MASK] [MASK] avait interdit tout voyage à l' étranger .
Il traverse ainsi le [MASK] [MASK] [MASK] à [MASK] , suivant la chaîne des [MASK] [MASK] vers l' ouest , arrivant enfin à [MASK] en 630 .
Toujours plus à l' ouest , il passe [MASK] avant de prendre vers le nord-ouest pour entrer dans ce qui est maintenant le [MASK] .
[MASK] continue au sud-ouest vers [MASK] , aujourd'hui capitale de l' [MASK] .
À partir de là , il poursuit au travers du désert jusqu' à [MASK] .
[MASK] participe à un débat religieux et fait montre de sa connaissance d' un grand nombre d' écoles bouddhiques .
Il traverse [MASK] puis la [MASK] [MASK] [MASK] vers l' est , atteignant l' ancienne capitale du [MASK] , [MASK] .
[MASK] visite un certain nombre de stūpas autour de [MASK] notamment celui construit par le roi [MASK] au sud-est de la cité .
[MASK] continue maintenant vers le nord puis traverse l' [MASK] à [MASK] .
Il se dirige vers le [MASK] , un royaume bouddhique mahâyâna vassal du [MASK] , où il se rend ensuite .
Durant cette période , [MASK] écrit sur le quatrième concile bouddhique qui eut lieu tout près de là , vers 100 , convoqué à la demande de [MASK] .
[MASK] y compte cent monastères des deux écoles principales et y est impressionné par le patronage que le roi assure aux étudiants et aux bouddhistes .
Il rend compte aussi de l' armée du râja , 500 éléphants , 20000 cavaliers et 50000 fantassins mais les attaques de dacoïts qu' il subit sur ses terres montrent que [MASK] ne contrôle pas parfaitement son territoire .
Accompagné par des moines locaux , il se rend à [MASK] , la grande université antique de l' [MASK] , où il passe les deux années suivantes .
Mais il pense maintenant à une autre destination , l' île de [MASK] , foyer de l' école theravâda .
Il continue sa descente et entre en pays pallava , passe à [MASK] où il découvre peut-être la [MASK] [MASK] [MASK] en travaux .
À [MASK] , des religieux cinghalais en fuite à cause de la guerre civile qui gronde dans l' île lui déconseillent de s' y rendre .
Il renonce donc à contrecœur et visite [MASK] et [MASK] .
Il remonte ensuite le long de la côte occidentale , traverse le pays konkani ( [MASK] ) et le [MASK] qui forment alors l' empire [MASK] et passe peut-être la saison des pluies 641 à [MASK] .
C' est alors que [MASK] , le dernier des grands rois indiens bouddhistes et le suzerain du roi de l' [MASK] , lui fait savoir qu' il souhaite sa présence auprès de lui .
Malgré son attachement au bouddhisme mahâyâna et comme tous les souverains de l' [MASK] , [MASK] n' a pas rompu avec les brahmanes et les sectes hindouistes et il compte organiser une assemblée entre savants religieux de toutes obédiences .
Durant les premiers jours de l' année 643 , il fait un accueil munificent au pèlerin puis l' accompagnent en remontant le [MASK] vers [MASK] .
Cinq cents d' entre eux seront déportés hors des frontières de l' [MASK] , une punition plus sévère que la mort car elle les oblige à vivre dans l' impureté .
Malgré l' insistance d' [MASK] , [MASK] prend la route du retour .
Le roi du [MASK] , ayant appris qu' il ne traverserait pas son pays , se rend au-devant de lui .
[MASK] reprend ainsi la route du [MASK] , refaisant le chemin qui l' avait conduit en [MASK] en sens inverse .
[MASK] est connu pour son énorme travail de traduction des textes bouddhiques indiens en chinois , permettant ainsi la reconstitution de textes indiens perdus grâce à ses traductions .
Son récit de voyage offre une description de l' [MASK] bouddhique au moment où elle entame son déclin .
Il est d' un intérêt majeur pour la connaissance de l' [MASK] de cette époque .