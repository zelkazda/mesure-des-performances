Quatre autres pays ont le statut de pays candidats à l' intégration , tandis que trois ( le [MASK] , l' [MASK] et la [MASK] ) ont entamé une demande pour être reconnus comme candidats .
Les partenaires de [MASK] [MASK] sont en désaccord avec cette position mais ils sont impuissants en raison de l' obligation d' unanimité .
En [MASK] , la procédure du référendum fut utilisée une seule fois , en 1972 , pour confirmer l' acceptation de l' adhésion du [MASK] , de l' [MASK] , de la [MASK] et du [MASK] ( le peuple norvégien refusant ultérieurement cette adhésion ) .
Les autres pays des [MASK] occidentaux sont reconnus comme des candidats potentiels :
La [MASK] a officiellement ouvert des négociations à [MASK] le 3 octobre 2005 .
La [MASK] devait alors ouvrir des négociations formelles le 17 mars 2005 .
Sauf gros contretemps le pays intégrera l' [MASK] [MASK] en 2010 .
La [MASK] est également un pays candidat depuis 2004 : demande d' adhésion déposée officiellement le 22 mars 2004 .
Le statut de candidat a été retenu par la [MASK] [MASK] le 9 novembre 2005 et par le [MASK] [MASK] le 16 décembre 2005 .
La [MASK] a déposé une demande d' adhésion le 22 décembre 2009 .
Par ailleurs , ce critère pose aujourd'hui le problème de [MASK] ou encore des régions ultrapériphériques .
Ces pays à haut niveau de vie et à longue tradition démocratique répondent globalement aux critères de [MASK] , même si des problèmes techniques subsistent quant aux mécaniques de la démocratie directe en [MASK] ( la question du secret bancaire a été réglée courant 2009 après l' acceptation par la [MASK] des critères de l' [MASK] ) ou la réglementation de la pêche en [MASK] ( qui perçoit par ailleurs d' énormes revenus pétroliers ) .
Ainsi la [MASK] a refusé par deux fois d' adhérer par référendum en 1972 , puis en 1994 .
En 2001 , consécutivement à une initiative populaire , la [MASK] rejette l' idée d' une réouverture rapide des négociations .
Il faut attendre fin 2009 pour que la question revienne sur l' avant-scène fédérale sous l' impulsion du ministre [MASK] [MASK] .
Bien qu' elle ait refusé en 1992 d' adhérer à l' [MASK] , de nombreuses conventions bilatérales entre la [MASK] et l' [MASK] assurent à ce pays une intégration économique globalement équivalente .
Ces pays sont également intéressés par certains projets de l' [MASK] , ils sont par exemple engagés par des accords de coopération avec [MASK] .
Par exemple le [MASK] est membre de l' [MASK] [MASK] [MASK] et [MASK] a signé un accord de coopération et d' union douanière avec l' [MASK] en 1991 .
Par ailleurs , pour certains d' entre eux , la qualité démocratique de leurs institutions et la transparence de leur système bancaire sont sujets à caution : [MASK] , le [MASK] et [MASK] font toujours partie au 18 mars 2004 de la " liste noire " des paradis fiscaux non coopératifs publié par l' [MASK] .
Pour la première fois en 2004 , les habitants ont voté lors des élections européennes en tant que citoyens de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Le président géorgien [MASK] [MASK] a déclaré le 7 avril 2004 que l' objectif d' une adhésion était la " priorité numéro un " de la politique étrangère de la [MASK] .
Des pays extra-européens ont également été en pourparlers puisque la demande d' adhésion du [MASK] a été rejetée en octobre 1987 .
Bien que [MASK] [MASK] se soit , par exemple , déclaré favorable à une candidature d' [MASK] , une adhésion de ces pays est extrêmement improbable .
Le 28 novembre 1995 , l' [MASK] a signé un accord de [MASK] [MASK] avec 12 pays du sud-est méditerranéen .
La [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .