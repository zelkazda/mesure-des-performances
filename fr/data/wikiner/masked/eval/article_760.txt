Le [MASK] est membre du [MASK] , de l' [MASK] [MASK] -- dont il est l' un des six pays fondateurs -- et de l' [MASK] .
Les [MASK] , les [MASK] puis les [MASK] peuplent successivement la région .
En 1815 , [MASK] [MASK] [MASK] l' intègre à son [MASK] [MASK] [MASK] puis en reconnaît l' indépendance en 1839 .
Pendant la [MASK] [MASK] [MASK] , le [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] jusqu' en 1918 .
Les négociations du [MASK] [MASK] [MASK] en 1919 confirment l' indépendance du pays tandis qu' un référendum populaire consolide l' indépendance du pays et la monarchie .
Le pays est libéré en septembre 1944 par les troupes américaines mais subit la contre-attaque allemande lors de la [MASK] [MASK] [MASK] en décembre de la même année .
Dès 1944 , l' union du [MASK] est conclue et le pays s' inscrit dans le processus de la construction européenne .
En 1948 , le [MASK] est membre fondateur du [MASK] [MASK] [MASK] et de l' [MASK] .
En 1952 [MASK] devient le siège de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
L' adhésion à la [MASK] [MASK] [MASK] est le point de départ d' une expansion économique et d' une hausse de l' immigration .
Le [MASK] est une démocratie représentative , sous la forme d' une monarchie constitutionnelle .
À la tête du gouvernement , nous retrouvons un premier ministre ( [MASK] [MASK] en 2009 ) ainsi qu' une chambre des députés qui débat et vote les lois .
Le [MASK] , membre de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , a supprimé en 1967 le service militaire obligatoire et entretient une petite armée de 800 volontaires .
Le [MASK] n' a aucune marine , ni force aérienne .
Le [MASK] est divisé en trois subdivisions administratives , ou districts :
Situé au cœur de l' [MASK] occidentale entre la [MASK] , la [MASK] et l' [MASK] , le [MASK] [MASK] [MASK] a une superficie de 2586 km² .
Le [MASK] peut être divisé en deux régions géologiques :
Outre la banque privée , depuis une quinzaine d' années , le [MASK] est devenu la place la plus importante d' [MASK] sur le marché des fonds d' investissement .
Le PIB par habitant du [MASK] est le plus élevé au monde [ réf. nécessaire ] .
Le [MASK] compte plus de 502 066 habitants avec une croissance de près de 100 000 personnes en 30 ans , croissance démographique plutôt exceptionnelle en comparaison avec les pays proches .
En effet , le solde migratoire , en moyenne annuelle , au [MASK] était de plus de 10‰ au cours de la décennie 1990-2000 , alors qu' il était d' environ 2,3‰ dans l' [MASK] des 15 .
Le [MASK] a adopté le modèle fiscal allemand et a été cadastré pour la première fois complètement par l' armée allemande , pendant la première guerre mondiale .
[MASK] [MASK] [MASK] est la principale chaîne de télévision .
Trois champions luxembourgeois ont remporté la grande boucle : [MASK] [MASK] en 1909 et [MASK] [MASK] en 1927 et 1928 , et enfin [MASK] [MASK] en 1958 .
En tennis féminin , [MASK] [MASK] et [MASK] [MASK] se sont illustrées dans ce sport en remportant plusieurs tournois [MASK] et [MASK] , notamment [MASK] [MASK] qui est la première joueuse de tennis luxembourgeoise à atteindre le top 20 ( 18° le 29 juillet 2002 ) .
Le [MASK] a pour codes :