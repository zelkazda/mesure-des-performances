Parmi les nombreuses applications des courbes elliptiques , on peut mentionner le théorème de clôture de [MASK] par rapport à deux coniques .
Cette interprétation permet d' ailleurs de déterminer des paires de coniques supportant des polygones de [MASK] à un nombre voulu de côtés .
À partir du point sur la courbe ( ou ( 12 , 36 ) sur la courbe ) , point qui correspond au triangle de côtés 3 , 4 , 5 , le point correspond au triangle 7/10 , 120/7 , 1201/70 et le point au 3e triangle indiqué par [MASK] .
On suppose que , pour un nombre premier p > 5 , l' équation de [MASK] ait une solution en entiers non nuls .
En comptant le point à l' infini , on a le résultat suivant , dû à [MASK] [MASK] :