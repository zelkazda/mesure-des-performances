[MASK] , ville acropole , est située sur un plateau dominant une boucle de la [MASK] , limitée en amont par la confluence de la [MASK] et en aval celles de l' [MASK] et des [MASK] .
La partie ancienne de la ville est bâtie sur le plateau , éperon rocheux créé par les vallées de l' [MASK] et de la [MASK] , à une altitude de 102 mètres , alors que la berge du fleuve , la partie inondable est à 27 mètres .
[MASK] est caractérisé par la présence de ses remparts sur un à-pic de 80 mètres .
Le vieil [MASK] est la partie ancienne , entre le rempart et le centre ville aux ruelles tortueuses et petites places .
Le centre ville , situé aussi sur le plateau a été dépeint par [MASK] [MASK] [MASK] dans " Les [MASK] [MASK] " : " en haut la noblesse et le pouvoir " .
Toutes ces communes sauf [MASK] font partie de la [MASK] , la communauté d' agglomération .
Le climat est océanique de type aquitain et semblable à celui de la ville de [MASK] où est située la station météorologique départementale .
La ville qui n' était pas sur les grands axes routiers était considérée par le poète [MASK] comme une petite bourgade .
Elle connaît une période prospère à la fin de l' [MASK] [MASK] .
Le réseau de voies romaines est alors réorganisé en sa faveur avec les cités alentours ( [MASK] , [MASK] , [MASK] , [MASK] , [MASK] ) .
Au cours de la bataille , cependant [MASK] aurait été gravement blessé à une jambe , sans doute une fracture .
Le fait est rapporté par la tradition , et sur une tour de la 2 e enceinte figure une jambe sculptée qui est dite " jambe de [MASK] " .
Ce territoire devient par la suite la [MASK] , puis [MASK] [MASK] .
Il y rédige une partie de l' [MASK] [MASK] [MASK] [MASK] [MASK] dont la première édition est publiée en latin à [MASK] en 1536 .
La [MASK] [MASK] [MASK] , qui se rend tragiquement célèbre en 1944 , continue sa " guerre éclair " en rejoignant au plus vite la frontière espagnole pour définir rapidement la ligne de démarcation qui va couper [MASK] [MASK] en deux .
C' est le premier d' une longue liste de 98 résistants ou otages originaires de [MASK] .
Le 8 octobre 1942 , 387 personnes d' origine juive sont arrêtées puis déportées à [MASK] .
À la fin du mois d' août 1944 la [MASK] [MASK] qui réunit des débris de différentes unités allemandes et la légion hindoue traversent la ville sans incident notable en se repliant .
Différentes unités des FFI du département et des renforts venus de [MASK] commencent alors l' encerclement de la ville .
En 1989 , défait aux élection municipales , le député-maire [MASK] , [MASK] [MASK] laisse un trou de 164 millions de francs dans les finances de la ville et une dette de 1,2 milliard de francs .
[MASK] est divisée en trois cantons :
La [MASK] [MASK] a été ouverte en 2007 ce qui a désenclavé plusieurs quartiers .
L' évolution du nombre d' habitants depuis 1793 est connue à travers les recensements de la population effectués à [MASK] depuis cette date :
[MASK] est le siège de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Elle gère l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Parmi eux se trouvent des espèces de marais et zones humides , et à [MASK] , il est fréquent de voir sur la [MASK] des oiseaux plongeurs et nageurs , des cygnes ( cygne tuberculé ) , des grèbes ( grèbe à cou noir , grèbe castagneux , grèbe esclavon , grèbe huppé ) , des oies ( oie cendrée ) , des canards ( canard chipeau , canard pilet , canard siffleur , canard souchet ) , des sarcelles ( sarcelle d' été , sarcelle d' hiver ) , et des fuligules ( fuligule milouin , fuligule morillon ) .
Unités actuellement en garnison à [MASK] :
Unités militaires ayant été en garnison à [MASK] :