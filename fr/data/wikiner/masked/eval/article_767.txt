L' [MASK] et [MASK] [MASK] lui attribuent le code 40 .
Le département a été créé à la [MASK] [MASK] le 4 mars 1790 en application de la loi du 22 décembre 1789 , à partir d' une partie de la province de [MASK] et [MASK] .
Elle couvrait environ 67 % du département avant la [MASK] [MASK] de janvier 2009 , dans sa partie nord , mais s' étendait aussi largement sur la [MASK] et le [MASK] ( arrondissement de [MASK] ) .
26 % de la [MASK] [MASK] [MASK] connaît plus de 40 % de dégâts en superficie , soit plus de 200000 hectares ravagés , .
Les marchés au gras sont légion , les plus importants étant sans doute ceux d' [MASK] , de [MASK] , de [MASK] et de [MASK] .
L' animatrice de télévision [MASK] a par ailleurs contribué à populariser la cuisine gasconne , dans son émission quotidienne " La cuisine des mousquetaires " .
Le tourisme thermal attire de nombreux curistes avec les stations de [MASK] , [MASK] , [MASK] , [MASK] ou encore [MASK] .
Après une baisse de fréquentation en 2003 suite à la catastrophe du [MASK] , l' année 2004 a été une excellente surprise pour le tourisme landais : le département a figuré à la première place des destinations estivales .
D' autre part , les trois députés que le département envoie à l' [MASK] [MASK] sont membres du [MASK] [MASK] .