Ugolin tombe éperdument amoureux de [MASK] lorsqu' il l' aperçoit se baignant dans la rivière .
Il ne sait comment faire pour attirer son attention : il place , par exemple , des animaux dans les pièges tendus par [MASK] pour lui faciliter la vie .
Les villageois en viennent alors à avouer leur complicité dans la mort de [MASK] [MASK] [MASK] .
Ugolin vient déclarer à genoux , une dernière fois , son amour à [MASK] .
Les villageois supplient [MASK] de participer à une procession en l' honneur de [MASK] [MASK] [MASK] , car ils pensent que la sécheresse a été causée par son esprit mécontent .
[MASK] débouche la source avec l' aide de son amant [MASK] et , dès que la procession arrive au village , l' eau jaillit des fontaines .
[MASK] et [MASK] se marient .