Sa capitale est [MASK] .
La culture locale des [MASK] [MASK] , décrite par [MASK] [MASK] [MASK] en 1540 , a complètement disparu en 1560 .
Après cette date , [MASK] [MASK] ne contrôle plus que [MASK] [MASK] et [MASK] ; mais la [MASK] subit aussi des raids par la suite .
La [MASK] importa de nombreux esclaves de différentes ethnies africaines pour ses plantations de riz puis de coton .
[MASK] met au point une machine à trier les fibres de coton des semences , dans le [MASK] [MASK] [MASK] , le long du [MASK] [MASK] .
En 1823 , une loterie aboutit à la distribution de terres puis en dix ans à l' installation de 3000 familles produisant 69000 balles de coton , dans ce qui est devenu la ville de [MASK] .
Une loterie est ouverte dès 1805 dans le [MASK] [MASK] [MASK] , qui compte en 1820 un population de 13000 habitants dont 6000 noirs .
En décembre 1864 le centre industriel et ferroviaire d' [MASK] est complètement détruit par l' armée nordiste du général [MASK] [MASK] [MASK] .
D' une superficie de 152577 km² , la [MASK] est peuplée de 9,5 millions d' habitants selon les dernières estimations de 2007 .
De nombreux cours d' eau traversent la [MASK] ; on trouve :
Selon les dernières estimations du [MASK] [MASK] [MASK] [MASK] [MASK] ( 2008 ) , les douze communes les plus peuplées sont :
La [MASK] est divisée en 159 comtés .
La croissance démographique de la [MASK] a été très dynamique depuis les années 1960 , notamment dans l' agglomération d' [MASK] .
Selon les estimations du [MASK] [MASK] [MASK] [MASK] [MASK] , la population de la [MASK] en 2007 était de 9,5 millions d' habitants ( par rapport à 8,1 millions d' hab .
Comme beaucoup d' anciens [MASK] [MASK] [MASK] , la [MASK] a vécu un régime de parti unique pendant une centaine d' années .
Les lois sur les droits civiques dans les années 1960 ont commencé à entamer cette prépondérance démocrate qui aboutit aux premières victoires présidentielles des candidats républicains ( [MASK] [MASK] en 1972 , [MASK] [MASK] en 1984 , [MASK] [MASK] [MASK] en 2000 et 2004 ) puis au triomphe républicain des élections de novembre 2002 .
L' assemblée générale de [MASK] est composée d' un sénat ( 56 membres dont 34 républicains , élus pour 2 ans ) et d' une chambre des représentants ( 180 membres dont 106 républicains , élus pour 2 ans ) , lesquels sont dominés depuis 2002 par les républicains .
Au niveau fédéral , les deux sénateurs sont pour la première fois tous deux républicains : [MASK] [MASK] et [MASK] [MASK] ( élu en 2004 , soutenu par le sortant démocrate , [MASK] [MASK] ) .
La [MASK] a vu naître un grand nombre de grands noms du cinéma comme [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] ou encore [MASK] [MASK] .