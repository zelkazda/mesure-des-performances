Elle suit la [MASK] [MASK] et précéde [MASK] [MASK] [MASK] .
Cette période correspond à l' âge du bronze en [MASK] et marque une transition entre l' histoire légendaire et les faits archéologiques .
Ces témoignages ne proviennent toutefois que de ses derniers souverains , à partir de [MASK] [MASK] .
On connaît 102 inscriptions sur bronze , mais surtout 200 000 oracles provenant tous de la dernière capitale des [MASK] ) , près de la ville moderne d' [MASK] , au [MASK] .
La méthode de divination utilisée s' est répandue à l' époque de la culture néolithique de [MASK] ( de -3000 à -- 2000 environ ) : la scapulomancie .
Les [MASK] utilisaient aussi des plastrons ( carapaces ventrales ) de tortues .
Le territoire contrôlé par les [MASK] semble avoir atteint sa superficie maximale au milieu de la dynastie .
Il a ensuite rétréci , malgré les efforts de [MASK] [MASK] , qui aurait régné à [MASK] entre -1198 et -1189 .
Son nouveau territoire allait devenir le berceau de la [MASK] [MASK] .
Sous les derniers rois , le territoire des [MASK] devint de plus en plus petit .
Le terme de [MASK] semble plutôt avoir été utilisé par les [MASK] .
Les [MASK] considéraient qu' ils régnaient au centre d' une terre carrée .
Les vassaux des [MASK] envoyaient des tributs sous forme de bétail ou de chevaux ou encore de carapaces de tortues utilisées en pyromancie .
Depuis le début de la dynastie , il était fréquent que le frère cadet d' un roi défunt lui succède , mais après les règnes de [MASK] [MASK] et de [MASK] [MASK] ( rois 24 et 25 ) , la succession s' est faite seulement de père en fils .
On ne compte ainsi que 17 générations depuis le premier roi , [MASK] [MASK] .
[MASK] [MASK] était le vingt-et-unième roi , et deux de ses fils ont régné , [MASK] [MASK] ( roi 22 ) puis [MASK] [MASK] ( roi 23 ) .
Pour compter les jours , les [MASK] utilisaient le cycle sexagésimal .
Ces appellations de " troncs célestes " ( tian gan ) et " branches célestes " ( di zhi ) sont postérieures aux [MASK] .
Chaque nom de temple des rois défunts comprenait un nom de tronc céleste , et les [MASK] avaient de plus en plus tendance à leur offrir un sacrifice durant le jour qui leur correspondait : [MASK] [MASK] était vénéré au jour jia ( le premier jour ) , [MASK] [MASK] était vénéré au jour geng ( le septième jour ) , etc .
[MASK] [MASK] ( roi 23 ) paraît avoir été à l' origine de cette réforme .
Un certain nombre d' officiers étaient répartis sur le territoire des [MASK] , tels que les " officiers des champs " ( tian ) , qui s' occupaient de l' agriculture , les " officiers des chiens " ( quan ) , qui intervenaient lors des chasses , les " pasteurs " ou les " gardes " .
Ceux-ci s' établirent sur la terre de [MASK] et y prospérèrent " .
Cette religion a également repris des collèges de magiciens ( wu ) qui existaient dès l' époque des [MASK] .
Durant l' une d' elles , [MASK] [MASK] captura un tigre , 40 cerfs , 150 biches et 164 renards .
Ils devenaient également les victimes de sacrifices humains , que les [MASK] ont pratiqués à grande échelle .
Sous la [MASK] [MASK] est utilisée l' écriture pictographique , avec le style sigillaire ( voir styles calligraphiques chinois ) .
On trouve sous la [MASK] [MASK] de nombreux objets d' arts :
La [MASK] [MASK] compta de manière certaine au moins 29 rois :
Le roi [MASK] [MASK] fut détrôné par [MASK] [MASK] fondateur de la [MASK] [MASK] .