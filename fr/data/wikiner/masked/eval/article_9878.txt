[MASK] [MASK] est considérée par l' [MASK] comme une maladie réémergente .
13 cas de peste ont été détectés en [MASK] à la mi-juin 2009 , mais l' épidémie a été enrayée immédiatement [ réf. souhaitée ] .
[MASK] [MASK] est actuellement inexistante en [MASK] .
Au [MASK] e siècle , [MASK] [MASK] [MASK] écrit :
[MASK] [MASK] s' exprime sous trois formes cliniques différentes pouvant parfois se succéder dans le temps :
[MASK] [MASK] se déclare d' abord chez les rongeurs qui meurent en grand nombre .
En [MASK] , le traitement s' est longtemps limité à :
[MASK] [MASK] est une maladie à potentiel épidémique qui justifie un diagnostic précoce et exige une déclaration aux autorités sanitaires nationales et internationales .
Les [MASK] ont également subi de telles maladies .
Ils attribuaient traditionnellement la peste à la vengeance d' [MASK] comme cela est décrit dans l' [MASK] .
[MASK] nous en a laissé une description qui laisse souvent penser que la maladie en question était en fait la variole .
En 1347 , des navires infectés abordent en [MASK] et déclenchent une épidémie dont mourra un quart de la population occidentale en quelques années .
1921 : vaccin aqueux de l' [MASK] [MASK]
L' [MASK] la classe comme " maladie réémergente " .
Pays les plus frappés : [MASK] et la [MASK] [MASK] [MASK] [MASK] .
[MASK] [MASK] a été utilisée comme arme par l' armée impériale japonaise lors de l' [MASK] [MASK] [MASK] [MASK] , notamment dans la région de [MASK] .
Les " danses macabres " constituaient des représentations d' épisodes de peste , notamment celle de l' église de [MASK] ( 1460 ) , aujourd'hui disparue .
[MASK] [MASK] généralise ensuite cette idée de peste : de la conception de la lèpre qui excluait les lépreux en masse , le pouvoir préfère à présent , dit [MASK] , quadriller , afin d' appliquer sa puissance normative sur les individus .
Le but , pour [MASK] , n' est plus de purifier la population , mais de produire une population saine .