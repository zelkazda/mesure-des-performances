[MASK] [MASK] est un pionnier de l' industrie du jeu vidéo aux [MASK] [MASK] [MASK] , il est un concepteur de [MASK] et un fondateur d' [MASK] .
En 1971 , [MASK] invente la première machine d' arcade , [MASK] [MASK] , puis commercialise l' un des premiers jeux vidéo , [MASK] en 1972 .
Le succès financier du jeu lui permet de développer la petite entité qu' il a créée et baptisée [MASK] .
[MASK] intervient , par ailleurs , régulièrement dans des conférences et des colloques aux [MASK] .