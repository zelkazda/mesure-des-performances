C' est à ce titre qu' [MASK] soutient , avec l' aide de l' évêque [MASK] , le [MASK] [MASK] [MASK] par les [MASK] au cours de l' hiver 885/886 .
L' étape suivante est pour [MASK] la royauté elle-même .
En effet , [MASK] [MASK] [MASK] [MASK] est déchu par les grands du royaume peu avant sa mort en 888 .
On lui reproche notamment d' avoir trop tardé à envoyer des troupes afin de lutter contre les [MASK] , et de s' être contenté en octobre 886 , malgré la résistance acharnée de la ville , de leur proposer de les payer pour qu' ils cessent leurs agissements , sans succès d' ailleurs ( ils partirent piller la [MASK] ) .
Les [MASK] mettent néanmoins à sac les villes de [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
Malgré tout , sa volonté de lutter contre les invasions normandes demeure intermittente , dans la mesure où , il se contente souvent de leur verser tribut ( [MASK] ) pour détourner leur violence .
Il dispose dans cette lutte d' alliés parmi les grands comtes , princes féodaux , notamment [MASK] [MASK] [MASK] [MASK] .
Ces derniers sont en effet inquiets de la volonté d' [MASK] de réaffirmer la capacité royale à disposer à son gré d ' honores que leurs titulaires considèrent s' être appropriés définitivement .
Finalement , [MASK] reconnaît , juste avant sa mort , [MASK] [MASK] [MASK] [MASK] comme son successeur , en échange d' un nouveau prélèvement de terres appartenant au fisc royal au bénéfice de son frère [MASK] .
Il meurt en 898 à [MASK] [MASK] , et est inhumé à [MASK] .