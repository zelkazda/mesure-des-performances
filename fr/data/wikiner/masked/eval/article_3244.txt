[MASK] [MASK] , rebaptisé [MASK] [MASK] dans le dessin animé français , est un personnage de fiction des mangas [MASK] [MASK] et [MASK] [MASK] .
Il est plus connu dans le milieu clandestin sous le nom " [MASK] [MASK] " .
Une des rares personnes à pouvoir le frapper est [MASK] , qui le punit régulièrement pour ses outrances .
[MASK] ayant repris connaissance sera fou de rage en apprenant qu' il fût sous l' influence de la poussière d' ange , et décide d' aller se venger en défiant son père adoptif .
Mais [MASK] perdit ( c' est le seul combat qu' il perdra ) .
Cependant il est rapidement forcé de retourner au [MASK] pour échapper à une organisation mafieuse .