En 1610 , elle fut emprisonnée dans le château de [MASK] , où elle resta jusqu' à sa mort , au bout de quatre ans .
[MASK] [MASK] est née dans une propriété familiale à [MASK] , en [MASK] , le 7 août 1560 .
Pendant les absences de son mari , [MASK] [MASK] gère leurs affaires .
En 1585 , une fille , [MASK] , naît .
Selon plusieurs sources , [MASK] était une mère affectueuse et dévouée .
[MASK] est une femme cultivée , sachant lire et écrire en quatre langues .
Un procès et une exécution auraient causé un scandale public , et jeté la disgrâce sur une noble famille influente ( qui , à l' époque , règne sur la [MASK] ) ; la fortune d' [MASK] -- considérable -- aurait été saisie par la couronne .
Tandis que [MASK] [MASK] est assignée à résidence ( et va le rester jusqu' à sa mort ) , ses complices sont poursuivis .
[MASK] elle-même ne prend pas part au procès .
[MASK] , jamais poursuivie au tribunal , reste assignée à résidence dans une seule pièce de son château et ce , jusqu' à sa mort .
Le 21 août 1614 , [MASK] [MASK] meurt dans son château .
Elle est enterrée à l' église de [MASK] .
Ses victimes initiales étaient de jeunes paysannes de la région , attirées à [MASK] par des offres de travail bien payé pour être servantes au château .
En plus des accusés , plusieurs personnes furent mentionnées comme ayant fourni des jeunes filles à [MASK] [MASK] .
Puis , [MASK] [MASK] met en scène l' histoire d' [MASK] [MASK] dans un de ses quatre [MASK] [MASK] ( 1974 ) .
Au cours de l' histoire , l' histoire d' [MASK] [MASK] y est également mentionné .
En 2005 , dans le film de [MASK] [MASK] [MASK] [MASK] [MASK] , le personnage de " La reine au miroir " interprétée par [MASK] [MASK] y fait indirectement référence , personnage obsédé par la jeunesse et la beauté éternelles , prisonnière dans la tour de son propre château et qui par l' intermédiaire de l' un de ses sujets dévoués , enlèvent des jeunes filles en les plongeant dans un sommeil proche de la mort dans le but de les sacrifier le moment venu .
Le film [MASK] [MASK] , retraçant la vie d' [MASK] [MASK] , réalisé par [MASK] [MASK] ( qui interprète également le rôle titre ) est sorti le 21 avril 2010 au cinéma .
[MASK] [MASK] est l' un des personnages de la bande-dessinée [MASK] [MASK] [MASK] [MASK] de [MASK] [MASK] et [MASK] [MASK] .
[MASK] [MASK] est le titre d' une bande dessinée de [MASK] [MASK] , parue en 2009