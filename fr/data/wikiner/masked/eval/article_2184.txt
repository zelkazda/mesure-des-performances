Une mesure de la solution courante dans le [MASK] , appelée température , permet de modifier le comportement du système afin de l' adapter à la situation .
Ensuite , un certain nombre d' agents du [MASK] sont choisis et exécutés .
Ces agents peuvent ajouter , supprimer ou modifier des objets du [MASK] .
Une température , mesurant l' état d' avancement de la solution contenue dans le [MASK] , est calculée .