Une grande partie des systèmes de [MASK] [MASK] [MASK] a été construite depuis la création de la [MASK] [MASK] en 1949 et surtout depuis le début des années 1980 .
Cependant , l' équipement du pays dans ces infrastructures varie énormément selon la région , avec d' importantes disparités locales , les régions les moins bien équipées sont ainsi l' ouest du pays , notamment le [MASK] , de par ses reliefs , puis le [MASK] , la [MASK] [MASK] et le [MASK] .
Les régions les plus desservies sont ainsi les régions urbaines de l' ouest , dans la région comprise entre les métropoles de [MASK] , [MASK] , [MASK] et [MASK] .
Le rail est le principal mode de [MASK] [MASK] [MASK] .
Seuls l' [MASK] avait plus de passagers/kilomètres et les [MASK] plus de tonnes par kilomètre .
La [MASK] [MASK] [MASK] de 1080 km a été construit pour désenclaver le [MASK] .
Elle part de [MASK] pour arriver à [MASK] .
La ligne devrait être étendue de 254 km jusqu' à [MASK] en 2010 .
Les villes de [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , possèdent actuellement un métro , alors que des métros sont en construction dans les villes de [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
Le [MASK] [MASK] [MASK] , ouvert en 1969 , dispose actuellement de 9 lignes , 147 stations et 228 km de voies et devrait passer à 420 km d' ici 2012 .
Le [MASK] [MASK] [MASK] , ouvert en 1997 , dispose , en 2009 , de 5 lignes , 88 stations et 150 km , et 133 km supplémentaires sont prévus .
Le [MASK] [MASK] [MASK] , ouvert en 1995 , dispose , en 2009 , de 11 lignes , 268 stations et 420 km , ce qui est l' un des plus grands métros au monde .
De plus , la ville possède une ligne de train à sustentation magnétique , dénommée [MASK] [MASK] [MASK] , qui relie le centre de [MASK] à l' [MASK] [MASK] [MASK] [MASK] , ouvert en 2003 , le projet a coûté 1,2 milliard de dollars .
Le [MASK] [MASK] [MASK] , a ouvert en 1970 , il possède 7 lignes sur 153,9 km .
Le [MASK] [MASK] [MASK] [MASK] , construit en 1979 , comprend 10 lignes sur 175 km de voies , il a fusionné avec le [MASK] [MASK] en 2007 .
Un métro est en construction à [MASK] , il devrait comprendre en 2013 , 2 lignes avec 23 stations et 20 km de voies .
La motorisation est rapide , bien qu' elle soit encore faible par rapport aux autres pays en développement tel que la [MASK] et le [MASK] .
[MASK] est la ville la plus motorisée de [MASK] avec 11 voitures pour 100 habitants soit près de 1,3 millions de voitures en 2004 .
Les principaux cours d' eau navigables sont le [MASK] [MASK] , l' [MASK] , la [MASK] [MASK] , la [MASK] [MASK] [MASK] , le [MASK] , la [MASK] [MASK] , le [MASK] [MASK] et le [MASK] [MASK] .
Le [MASK] [MASK] est navigable par des navires de 10000 tonnes jusqu' à [MASK] .
À partir de là , le [MASK] [MASK] n' est navigable que pour des navires de 1000 tonnes et ce , jusqu' à [MASK] .
Le [MASK] [MASK] est le plus long canal du monde avec 1794 km de voies navigables en desservant 17 villes dont [MASK] et [MASK] .
Il relie cinq grands fleuves : le [MASK] [MASK] , le [MASK] [MASK] , le [MASK] [MASK] , le [MASK] et le [MASK] [MASK] .
Les sept plus grands terminaux portuaires sont ceux de [MASK] , [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
La construction d' un pipeline de 4200 km de long entre le [MASK] et [MASK] a été achevée en 2004 .
Ils sont , respectivement , les hubs d' [MASK] [MASK] , de [MASK] [MASK] [MASK] et de [MASK] [MASK] [MASK] .
Parmi les autres grands aéroports chinois , l' on peut citer l' [MASK] [MASK] [MASK] , l' [MASK] [MASK] [MASK] , l' [MASK] [MASK] [MASK] , l' [MASK] [MASK] [MASK] et l' [MASK] [MASK] [MASK] .
Mais d' autres grands aéroports sont situés également à [MASK] , à [MASK] , à [MASK] , à [MASK] , à [MASK] , à [MASK] , à [MASK] , à [MASK] , à [MASK] et à [MASK] .
En 2002 , le gouvernement a fusionné les neuf plus grandes compagnies aériennes en trois groupes régionaux : [MASK] [MASK] avec à l' époque un chiffre d' affaire de 7 milliard de dollars , 20000 salariés et 15,6 millions de passagers transportés , [MASK] [MASK] [MASK] avec un chiffre d' affaire de 6 milliard de dollars , 19000 salariés et 16,2 millions de passagers transportés et [MASK] [MASK] [MASK] avec un chiffre d' affaire de 4,7 milliard de dollars , 30000 salariés et 24,1 millions de passagers transportés , toujours en 2002 .
Il reste cependant de nombreuses compagnies régionales tel [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et [MASK] [MASK] .