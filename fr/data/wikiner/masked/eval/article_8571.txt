Le mot pirate est utilisé aussi dans différents contextes autres que maritime : le " pirate de la route " , que l' on appelait autrefois " voleur de grand chemin " , le pirate informatique qui est un individu s' adonnant à des détournements de fonds effectués par [MASK] , ou des copies d' œuvres sans respecter le droit d' auteur ou le copyright .
La piraterie existait déjà dans l' [MASK] .
[MASK] [MASK] dut lui-même affronter la piraterie .
Les boucaniers : les pirates qui sévissaient dans la [MASK] [MASK] [MASK] étaient parfois appelés abusivement boucaniers .
Ils se trouvaient au nord-ouest de [MASK] et dans la baie de [MASK] , mais ils avaient souvent leurs comptoirs à la [MASK] .
On note également que les attaques qui avaient jusqu' alors lieu dans le [MASK] [MASK] [MASK] , ont eu lieu en [MASK] [MASK] et jusqu' à 1000 milles marins des côtes de la [MASK] .
Lorsqu' il a été détourné par des pirates somaliens le 15 novembre 2008 , le [MASK] [MASK] est devenu le plus grand navire de l' histoire moderne capturé par des pirates