Au début des années 1970 , il travaille avec [MASK] [MASK] en tant que programmeur dans les [MASK] [MASK] sur le développement de [MASK] .
Pour [MASK] , il s' avère nécessaire d' améliorer le [MASK] [MASK] créé par [MASK] [MASK] et c' est ainsi que [MASK] crée le [MASK] [MASK] .
Par la suite , aidé de [MASK] [MASK] , il promeut le langage et rédige notamment le livre de référence [MASK] [MASK] [MASK] [MASK] .
En 1967 , il commença à travailler aux [MASK] [MASK] .
Son invention du [MASK] [MASK] et sa participation au développement d' [MASK] au côté de [MASK] [MASK] ont fait de lui un pionnier de l' informatique moderne .
Le [MASK] [MASK] reste aujourd'hui un des langages les plus utilisés , tant dans le développement d' applications que de systèmes d' exploitation .
[MASK] a aussi eu une grande influence en établissant des concepts qui sont désormais totalement incorporés dans l' informatique moderne .