Ses pièces mettent en scène des héros , souvent solitaires et même rejetés ( [MASK] , [MASK] , [MASK] , [MASK] ) , et confrontés à des problèmes moraux desquels naît la situation tragique .
Comparé à [MASK] , [MASK] ne met pas ou peu en scène les dieux , qui n' interviennent que par des oracles dont le caractère obscur trompe souvent les hommes , sur le mode de l' ironie tragique .
Les détails de la vie de [MASK] sont connus , bien qu' assez mal , grâce à une compilation anonyme , à la [MASK] et aux mentions d' auteurs comme [MASK] ou [MASK] .
À quatre-vingt-trois ans , il fait également partie des dix conseillers désignés après le désastre de [MASK] .
La carrière de tragédien de [MASK] débute au plus tôt en 468 .
[MASK] est le rival de ce dernier pendant douze ans , avant qu' [MASK] le concurrence à son tour dès 455 .
[MASK] meurt en 406 ou 405 .
[MASK] est l' auteur de cent vingt-trois tragédies , , ainsi que des drames satyriques .
La plupart ont été perdues : il nous reste cent quatorze titres et seulement sept pièces , auxquelles on peut ajouter les fragments importants du drame satyrique [MASK] [MASK] , retrouvés en 1912 .
Mais une étude sur la récurrence des thèmes mythiques chez [MASK] devrait également prendre en compte les pièces perdues .
On lui attribue aussi un péan pour [MASK] , dont [MASK] participa à introduire le culte .
Ses proches constatant sa mort se lamentent , et [MASK] accepte sur les supplications de [MASK] la sépulture d' [MASK] .
[MASK] est datée avec précision de 442 .
Pour avoir enterré son frère rebelle [MASK] , tué dans sa lutte avec son frère [MASK] , [MASK] qui a enfreint le décret de [MASK] doit être punie de mort .
Le tyran refuse de revenir sur sa décision malgré les lamentations du chœur des vieillards de [MASK] et les supplications de son propre fils [MASK] , fiancé d' [MASK] .
Seuls les présages de [MASK] le font changer d' avis , mais il est trop tard : [MASK] et [MASK] se sont entretués , et la femme du prince , [MASK] , se suicide à son tour .
De datation imprécise mais considérées comme l' une des plus anciennes tragédies de [MASK] , [MASK] [MASK] ont pour sujet la mort d' [MASK] .
À [MASK] , sa femme [MASK] , prévenue par son fils [MASK] du retour d' [MASK] , mais inquiète de voir ce dernier devancé par la jeune [MASK] pour laquelle il brûle , elle fait envoyer par [MASK] une tunique trempée dans le sang du centaure [MASK] .
Apprenant l' événement , elle se suicide alors que son époux arrive à [MASK] et , entendant de la bouche d' [MASK] la nouvelle de cette mort , ce dernier comprend qu' il meurt par la ruse d' un mort , [MASK] , comme l' avait prédit un oracle .
À [MASK] ravagé par la peste , [MASK] devenu roi cherche à connaître l' identité du meurtrier de [MASK] , cause de la malédiction .
Des révélations successives viennent pourtant étayer la révélation , et [MASK] doit admettre qu' en tentant de déjouer l' oracle , il n' a fait que l' accomplir .
La pièce s' achève sur le suicide de [MASK] et l' apparition d' [MASK] mutilé après s' être crevé les yeux , le visage ensanglanté , réclamant l' exil .
Reprenant le thème des [MASK] [MASK] d' [MASK] ( et conservant le chœur de jeunes femmes ) , [MASK] décrit le retour à [MASK] d' [MASK] , vengeur de son père [MASK] .
Il y retrouve sa sœur [MASK] , qui attend son retour avec un désespoir grandissant et envisage d' accomplir elle-même la vengeance .
La scène de reconnaissance intervient dans le dernier épisode et le meurtre de [MASK] , puis celui d' [MASK] , sont accomplis en exodos .
Chargé par [MASK] de ramener à [MASK] [MASK] , blessé et abandonné jadis par [MASK] et les [MASK] sur une île déserte , et son arc , le jeune [MASK] , fils d' [MASK] , est confronté à un choix moral délicat .
Puis c' est [MASK] qui vient quémander son soutien pour la guerre des sept qui se prépare .
Des fragments des [MASK] ont été découverts en [MASK] en 1912 .
Les critères stylistiques et métriques font dater [MASK] [MASK] probablement d' avant 440 .
La pièce débute par la plainte d' [MASK] , que le chœur de satyres propose d' aider contre la promesse d' être libérés de l' esclavage .
Les traces du bétail les conduisent à la grotte de la nymphe [MASK] , qui veille sur l' enfant .
La fin du drame devait présenter la réconciliation entre [MASK] et [MASK] grâce à la lyre de ce dernier .
Sur les cent vingt-trois pièces écrites par [MASK] sont connus cent quatorze titres .
L' innovation la plus remarquable de [MASK] , si l' on compare son œuvre à [MASK] , est l' abandon de la tragédie " liée " , puisqu' il n' en a composé à notre connaissance aucune .
L' exemple du titre de la pièce sur le retour d' [MASK] est parlant : alors qu' il met en avant le chœur chez [MASK] ( [MASK] [MASK] ) , la sœur d' [MASK] passe au premier plan chez [MASK] ( et le restera chez [MASK] ) : la tragédie prend le nom de l' héroïne , [MASK] , et des héros comme elle donnent leur nom à toutes les pièces conservées de [MASK] sauf une ( [MASK] [MASK] ) .
Hormis les deux pièces de [MASK] consacrées à [MASK] ( où l' obstination du héros n' a pas de justification morale , et ne concerne pas une opposition de valeurs ) , le premier point commun entre les pièces conservées est la place centrale occupée par les enjeux moraux sous forme de choix .
L' autre grand exemple est [MASK] : le meurtre de [MASK] et d' [MASK] n' intervient qu' à la toute fin de la tragédie , qui développe surtout la psychologie de l' héroïne , et le thème de la vengeance du père à travers le meurtre de la mère .
Alors qu' ils pèsent de tout leur poids sur le théâtre d' [MASK] , les dieux ont un tout autre rôle chez [MASK] .
Mais cette distance a pour conséquence de souligner le contraste entre le monde des hommes , qui évoluent sur la scène , et celui des dieux , comme le souligne le chœur d' [MASK] ) ou celui d' [MASK] [MASK] , .
Seulement celle-ci intervient par des oracles et le théâtre de [MASK] porte non plus sur la " justice divine " comme chez [MASK] , mais sur le sens de ces oracles , qui sont le seul indice dont les hommes disposent sur la décision divine .
Au début des [MASK] , [MASK] annonce l' oracle qui concerne [MASK] : " Ou il trouvera là le terme de sa vie , ou il triomphera et dès lors à jamais passera dans le calme le reste de ses jours " ; dans [MASK] , le présage de [MASK] est rapporté par le messager , la colère d' [MASK] ne doit poursuivre le héros qu ' " un seul jour " : " s' il survit pourtant à cette journée , peut-être le sauverons-nous , avec l' aide de quelque dieu " .
Selon les mots d' [MASK] , [MASK] ne peut être guéri qu' à [MASK] : mais y parviendra-t-il ?
Par cette place laissée à l' erreur font irruption la surprise et la péripétie , mais surtout le spectacle du destin de l' homme en train de se jouer : " toute la dramaturgie de [MASK] repose sur l' idée que l' homme est le jouet de ce que l' on pourrait appeler l' ironie du sort " , une " ironie tragique , dont le sens s' inscrit en clair sous les yeux des spectateurs , alors que les personnages n' en distinguent pas toujours le sens " , et propre à [MASK] .
Cette ironie qui fait de [MASK] l' instrument de la mort d' [MASK] , qui fait intervenir la mort après les chants d' espoir et de joie du chœur dans [MASK] [MASK] , dans [MASK] , dans [MASK] .
Chez [MASK] ou [MASK] , l' ironie tragique peut faire d' un personnage le dupe d' un autre , mais chez [MASK] les hommes ne se dupent pas entre eux , sauf à de rares exceptions : c' est par les dieux qu' ils sont abusés .
Cette distance et cette ironie tragique trouvent leur illustration la plus aboutie dans [MASK] [MASK] , " quête tragique " d' [MASK] qui apprend ce qu' il est , c' est-à-dire meurtrier de son père et époux de sa mère , et qui , ayant tout fait pour fuir l' oracle prononcé contre lui , l' a réalisé dans l' ignorance complète de la portée de ses actes .
La portée religieuse de cette perfection de l' ironie , particulièrement dans [MASK] [MASK] , ne doit cependant pas être considérée comme le fait de dieux cruels ou indifférents ( le destin d' [MASK] devenu protégé des dieux dans [MASK] [MASK] [MASK] l' interdit d' ailleurs ) .
Chez le pieux [MASK] , " les hommes n' ont pas à comprendre , mais à adorer . " [MASK] , [MASK] ou [MASK] paient leur irrespect des devins et des oracles , la tragédie est le fait de l' erreur des hommes .
Or , on peut rappeler que l' homme est au cœur du théâtre de [MASK] , comme en témoignent les évolutions formelles .
Le héros donne généralement son nom à la pièce et se trouve en opposition à d' autres personnages : c' est même là ce qui définit son statut de héros , son " isolement progressif de toute aide et de tout soutien humains . " [MASK] invite d' abord sa sœur à une action conjointe , mais le refus d' [MASK] l' enferme dans le rejet de toute assistance , même lorsqu' il s' agit pour [MASK] de se joindre à elle devant la colère de [MASK] : " Tu n' as pas voulu , toi , me suivre , et je ne t'ai pas , moi , associée à mon acte " .
[MASK] est donc dès lors seule , " sans amis , sans mari " , " abandonnée des [ siens ] " .
On retrouve ces éléments dans [MASK] : rejetée par sa famille et par le chœur , sa solitude culmine lorsqu' elle croit [MASK] mort et que , comme dans [MASK] , sa sœur [MASK] lui refuse son aide .
[MASK] enfin est seul , abandonné , et n' a plus que son arc dont [MASK] vient le priver .
C' est encore une fois en [MASK] que [MASK] trouve la meilleure application de ses choix .
Dans [MASK] [MASK] [MASK] , le héros est un vagabond aveugle , rejeté par ses fils et fui des hommes .
Une solitude de fait ( aggravée au cours de la pièce par [MASK] , qui prive [MASK] de ses filles ) , à laquelle s' ajoute une solitude morale , [MASK] affirmant ainsi à plusieurs reprises avoir subi bien plus que commis ses actes .
[MASK] ne change pas fondamentalement cette solitude : [MASK] meurt seul et sans témoins , il demeure en marge .