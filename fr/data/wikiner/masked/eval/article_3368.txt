Ingénieur agronome , ancien élève de l' [MASK] , ingénieur du génie rural , des eaux et forêts .
Il est le fils d' [MASK] [MASK] ancien député [MASK] d' [MASK] de 1945 à 1968 et ancien maire de [MASK] de 1945 à 1976 .
Candidat dès 1968 aux élections législatives dans la circonscription de [MASK] , l' ancienne circonscription de son père , [MASK] [MASK] est battu dans le contexte de la vague gaulliste qui suit les évènements de mai 1968 .
À nouveau candidat en 1973 , il bat le sortant [MASK] .
Il sera , à compter de cette date réélu député de la circonscription de [MASK] en 1978 , 1981 , 1986 , 1988 , 1993 , 1995 , 1997 , 2002 et 2007 et chaque fois au premier tour .
Ancien président du [MASK] [MASK] [MASK] [MASK] de 1982 à 1994 , il est aujourd'hui vice-président de l' [MASK] après en avoir été le secrétaire général .
Lors des élections cantonales de 1976 , il est élu conseiller général du [MASK] [MASK] [MASK] et succède ainsi à son père .
Suite à son entrée au conseil général , il devient un an plus tard maire de [MASK] lors des élections municipales de 1977 .
Son action politique a permis d' accompagner le développement économique et démographique de la ville , notamment en faisant pression pour bénéficier d' un arrêt quotidien du [MASK] lors de sa mise en service en 1989 .
Il démissionne de ce mandat en 1988 , suite à son retour à l' [MASK] [MASK] .