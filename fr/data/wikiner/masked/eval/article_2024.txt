[MASK] [MASK] ( 1 er juillet 1872 à [MASK] -- 2 août 1936 à [MASK] ) était un constructeur de lanternes d' automobiles , d' avions et de motocyclettes et pilote pionnier de l' aviation français .
Il fut le premier à traverser la [MASK] en avion avec son [MASK] [MASK] le 25 juillet 1909 .
Ingénieur de l' [MASK] [MASK] ( promotion 1895 ) , il a volé pour la première fois en 1907 dans un avion de sa conception .
En 1910 , il deviendra titulaire du premier brevet de pilote délivré en [MASK] .
Sorti de l' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] avec la promotion de 1895 , il devient ingénieur avant de connaître un succès certain dans la fabrication de phares à acétylène pour l' industrie automobile alors en pleine expansion .
Cette année 1901 est également marquée par sa rencontre avec [MASK] [MASK] , auquel il voue une sincère admiration .
[MASK] et [MASK] .
En 1906 , [MASK] se prend de passion pour la question des moteurs .
[MASK] [MASK] a fabriqué de nombreux modèles d' avions dont :
L' hélice du moteur [MASK] de 24 ch est placée à l' arrière de l' appareil , tandis que la gouverne de profondeur est placée devant l' appareil ( configuration canard ) .
Le moteur [MASK] de 50 ch est placé à l' avant , les gouvernails de direction et de profondeur étant placés à l' arrière .
Le [MASK] n o 10 est par contre un biplan de 60 m² de surface portante à moteur [MASK] de 50 ch avec démultiplicateur 30/12 , un gouvernail de direction avant , stabilisateur et gouvernail de profondeur arrière , ailerons derrière les ailes remplaçant le gauchissement des ailes [MASK] , châssis renforcé à roues analogue à celui des monoplans .
En mars 1909 , le moteur de [MASK] ne lui convenant pas , [MASK] se sépare d' [MASK] , tout en y conservant des intérêts .
Il décide également de monter une opération destinée à promouvoir sa marque : la traversée de [MASK] [MASK] .
[MASK] [MASK] se lance dans la conception du modèle suivant en faisant en sorte de prendre un monoplan à empennage très réduit qui devait rencontrer une moindre résistance à l' avancement , et ainsi se contenter d' un moteur de même puissance que l' appareil de [MASK] .
Le [MASK] [MASK] était un appareil frêle , construit en bois et consolidé par des cordes à piano , les ailes recouvertes de papier parcheminé .
Cet appareil , d' une envergure de 7.20 m , après quelques améliorations , allait devenir le célèbre [MASK] [MASK] de la traversée de [MASK] [MASK] .
Le 3 juillet 1909 , [MASK] [MASK] participe au premier meeting aérien mondial au [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] avec son monoplan .
[MASK] [MASK] y participe également sur biplan [MASK] .
[MASK] [MASK] s' installe à [MASK] le 25 juillet 1909 sur 200 hectares , où il implante son aérodrome privé puis , en 1913 , une école de pilotage .
La course pour être le premier à rejoindre la [MASK] par la voie des airs fait rage en ce mois de juillet 1909 .
Cependant , [MASK] laisse la priorité à [MASK] [MASK] dans la mesure où ce dernier s' est engagé dès le 2 juillet .
Le 19 juillet 1909 dans l' après-midi , [MASK] [MASK] s' inscrit dans la course à [MASK] [MASK] avant de partir s' installer à [MASK] le 21 juillet 1909 .
[MASK] [MASK] est le premier à traverser la [MASK] , le 25 juillet 1909 en décollant au lever du soleil , condition exigée par le [MASK] [MASK] ( journal britannique ) qui est à l' origine du défi et lui remettra la somme de 25000 francs-or mise en jeu .
Il s' agit d' un moteur à soupapes passives à trois cylindres en éventail , fabriqué par [MASK] [MASK] .
Le hameau [MASK] [MASK] , faisant partie de la commune de [MASK] , sera plus tard rebaptisé [MASK] en son honneur .
[MASK] crée une école de pilotage à [MASK] , ville qu' il a connue lors de son service militaire à [MASK] et où il s' est marié .
Après l' exploit qui le rendit célèbre dans le monde entier , [MASK] participe au meeting de [MASK] à l' automne 1909 .
La compagnie [MASK] sait très vite tirer partie de la publicité , notamment avec son premier pilote de démonstration , [MASK] [MASK] .
Son pilote d' essai [MASK] [MASK] bat le record du monde avec 7 passagers .
De 1910 à l' automne 1913 , [MASK] retira d' importants bénéfices de sa firme .
Afin de satisfaire la demande , [MASK] fabrique finalement 126 avions de sa marque au lieu des trois initialement prévus .
[MASK] achète des sites industriels hors de [MASK] ; il devient le banquier de ses sociétés et fait dans l' industrie des loisirs des placements considérables , en particulier à [MASK] .
Au total , [MASK] produisit beaucoup d' avions ( environ 10000 ) pendant la [MASK] [MASK] [MASK] essentiellement dotés de [MASK] [MASK] .
L' usine de [MASK] travaille en discontinu et aucune série d' avions ne dépasse les 100 unités .