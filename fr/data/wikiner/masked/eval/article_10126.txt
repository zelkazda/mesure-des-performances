En 1579 , il reçoit le commandement d' une compagnie d' ordonnance du roi puis devient gouverneur du [MASK] [MASK] .
En 1580 , il participe au siège de [MASK] [MASK] .
Le 24 février 1583 , il est nommé gouverneur de [MASK] .
Et , en 1584 , il devient gouverneur du [MASK] .
Le 20 octobre 1587 , il attaqua les troupes protestantes à [MASK] ( [MASK] ) , mais son infanterie et sa cavalerie furent décimées .
[MASK] [MASK] [MASK] s' était constitué prisonnier , lorsqu' il fut reconnu et tué d' un coup de pistolet .