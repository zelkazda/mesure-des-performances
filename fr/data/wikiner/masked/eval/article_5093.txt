[MASK] [MASK] est une collection de bibliothèques destinées à la programmation d' applications multimédia .
Plus particulièrement de jeux ou de programmes faisant intervenir de la vidéo , sur les plates-formes [MASK] ( [MASK] , systèmes d' exploitation [MASK] ) .
[MASK] étant le nom générique faisant référence à l' ensemble de ces technologies .
[MASK] est largement utilisée dans le développement de jeux pour la plate-forme [MASK] [MASK] , pour [MASK] et [MASK] [MASK] .
[MASK] étant peut-être une des technologies [MASK] les plus reconnues , il n' est pas rare de voir le nom [MASK] utilisé en lieu et place de [MASK] .
La multitude des composants [MASK] existe sous deux formes .
Initialement les composants destinés à l' exécution des applications étaient redistribués avec les jeux qui en faisaient usage , aujourd'hui , ils se trouvent aussi préinstallés avec [MASK] .
Ces deux versions font usage de fonctionnalités propres au nouveau modèle de pilote [MASK] [MASK] [MASK] [MASK] apparu avec [MASK] [MASK] .
Les composants constituant [MASK] sont : ( en ) [MASK] : pour manipuler les contenus multimédia , audio et vidéo , streamé ou non .
En 1994 , [MASK] était sur le point de lancer son système d' exploitation , [MASK] 95 .
[MASK] se devait donc de fournir aux développeurs ce qu' ils voulaient , par ailleurs il était nécessaire de le fournir rapidement ; la date de sortie du nouveau système d' exploitation n' étant plus distante que de quelques mois .
Une équipe de développement d' [MASK] [MASK] soumis à l' attention de [MASK] un certain nombre de technologies graphiques spécifiques au monde du jeu .
Pour résumer , il permit à toutes les versions de [MASK] depuis [MASK] 95 de bénéficier de capacités multimédia performantes .
Avant l' existence de [MASK] , [MASK] avait déjà inclus le support d' [MASK] sur [MASK] [MASK] .
À cette époque , [MASK] nécessitait du matériel haut de gamme qui restait réservé aux utilisateurs disposant de moyen financier important comme dans le monde de l' industrie et plus généralement les utilisateurs de système CAO .
John comme une alternative à l' [MASK] de [MASK] ) était destiné à être une technologie compagnon plus légère et spécialisée pour les jeux .
La puissance des cartes graphiques et des ordinateurs évoluant rapidement , [MASK] devint de son côté un standard de fait et une technologie accessible à tous .
[MASK] ) .
En revanche , [MASK] est toujours nécessaire pour compiler les exemples [MASK] .
Ce sera la seule version disponible tant qu' un pilote [MASK] [MASK] [MASK] [MASK] n' aura pas été installé avec [MASK] , par exemple juste après une mise à jour alors que seul un [MASK] graphic driver est présent .
[MASK] 10 : Destiné à fournir l' accès le plus direct et le plus total aux cartes graphiques modernes par le biais du nouveau modèle de pilote [MASK] .
Certains essaient d' adapter [MASK] à leur plate-forme .
Par exemple , un jeu s' appuyant sur [MASK] 9 pouvait fonctionner sur une machine équipé d' une carte n' ayant qu' un driver développé à l' époque de [MASK] 6 .
En décembre 2005 , février 2006 , avril 2006 et août 2006 , [MASK] mis à disposition des développeurs des mises à jour successives de cette technologie pour aboutir à la version 2.0 qui n' a jamais existé que sous la forme d' une version bêta ayant expiré le 5 octobre 2006 .
ne sont pas préinstallés avec aucune version de [MASK] , aussi il est recommandé de les installer en même temps que tout jeu en faisant usage .
[MASK] toutes versions est compatible avec [MASK] 32 bits et 64 bits .
Il existe tout une série de bibliothèques qui couvre souvent des partie de [MASK] .
D' autres projets comme une partie de [MASK] visent à fournir une implémentation alternative du même jeu de fonctionnalités .