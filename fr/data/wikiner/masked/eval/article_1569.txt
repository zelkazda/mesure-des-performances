[MASK] [MASK] est le pseudonyme d' une personne qui fut à l' origine des premiers grands incidents de pourriel sur [MASK] , en inondant les forums [MASK] de réponses aux messages qui parlaient de la [MASK] .
Malgré de forts soupçons , l' identité réelle derrière [MASK] [MASK] ne fut jamais révélée .
Pendant une période de deux mois , au début de 1994 , ce " [MASK] [MASK] " postait des messages dans tout groupe de discussion contenant une conversation impliquant la [MASK] .
Néanmoins , le compte [MASK] [MASK] disparut à la mi- 1994 après qu' un newsgroup fut créé , servant de filtre et générant des suppressions de messages sur les interventions venant de [MASK] .
C' était un étudiant turc qui avait enregistré le domaine anatolia.org , origine des messages [MASK] .
Les messages de [MASK] [MASK] ont été globalement jugés comme une tentative assez grossière de faire du négationnisme .
2000 ) , rapporte que " [MASK] [MASK] " fut considéré comme " la sixième ' personne ' la plus malveillante d' [MASK] " .