Le premier sommet a eu lieu à [MASK] ( [MASK] ) en 1972 , le deuxième à [MASK] ( [MASK] ) en 1982 , la troisième à [MASK] [MASK] [MASK] ( [MASK] ) en 1992 , et le quatrième et dernier en date à [MASK] ( [MASK] [MASK] [MASK] ) en 2002 .
Le prochain sommet devrait se dérouler de nouveau à [MASK] [MASK] [MASK] , en 2012 .
Le sommet de 1972 a donné naissance au [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , tandis que le sommet de 1992 a lancé la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) dont les pays signataires se rencontrent annuellement depuis 1995 .
Ce sommet a donné naissance au [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
À cette époque , les dirigeants mondiaux se sont engagés à se rencontrer tous les dix ans pour faire le point sur l' état de la [MASK] .
Les événements de l' époque ( [MASK] [MASK] ) et le désintérêt du président des [MASK] , [MASK] [MASK] ( qui a nommé sa fille déléguée des [MASK] ) ont fait de ce sommet un échec .
Le [MASK] [MASK] [MASK] a donné le coup d' envoi à un programme ambitieux de lutte mondiale contre les changements climatiques , pour la protection de la diversité biologique , ou biodiversité , et l' élimination des produits toxiques dangereux .
Il a abouti à la signature de la [MASK] [MASK] [MASK] .
Ce sommet s' est tenu du 26 août au 4 septembre 2002 à [MASK] ( [MASK] [MASK] [MASK] ) sous l' égide des [MASK] [MASK] .
Ce sommet constituait une occasion pour le monde entier de faire le bilan et de compléter le programme lancé lors du [MASK] [MASK] [MASK] ; il était axé autour du développement durable , le prochain se déroulera en 2012 .