L' [MASK] , officiellement la [MASK] [MASK] est un pays d' [MASK] [MASK] [MASK] partageant ses frontières avec le [MASK] à l' ouest , la [MASK] au nord-ouest , le [MASK] au nord , le [MASK] et l' [MASK] au nord-est et à l' est , et l' [MASK] [MASK] à l' est et à l' extrême sud .
Le pays proclama son indépendance le 25 mai 1810 lors de la [MASK] [MASK] [MASK] , indépendance définitivement acquise le 9 juillet 1816 à [MASK] [MASK] [MASK] [MASK] .
L' [MASK] est le pays le plus développé du continent latino-américain .
Un grand nombre de tribus indiennes peuplait l' [MASK] avant la conquête espagnole
Dès 1810 avec la [MASK] [MASK] [MASK] ( 25 mai 1810 ) les [MASK] deviennent indépendants de fait .
Au commandement d' une armée d' environ 4000 soldats , [MASK] [MASK] réalise une campagne prodigieuse .
La constitution sera proclamée en 1853 , après la fin de la dictature de [MASK] [MASK] [MASK] [MASK] .
[MASK] parvint au pouvoir après la fin de la [MASK] [MASK] [MASK] .
Après la guerre , de très nombreux nazis fuirent en [MASK] .
Désormais , l' [MASK] entre dans une période d' instabilité à la fois économique et politique .
Jamais alors le climat politique n' avait été aussi propice à la gauche en [MASK] .
À cette occasion , les leçons apprises lors de la [MASK] [MASK] [MASK] sont mises en pratique .
[MASK] [MASK] participe en outre à l' [MASK] [MASK] , et de nombreux réfugiés politiques de pays voisins sont assassinés par le biais des services secrets ou d' escadrons de la mort ( la [MASK] [MASK] ) .
Afin de relancer sa popularité , la junte de [MASK] [MASK] , dirigée depuis décembre 1981 par [MASK] [MASK] , l' un des plus " durs " , décide d' envahir les [MASK] [MASK] en 1982 , provoquant ainsi la [MASK] [MASK] [MASK] contre le [MASK] , alors dirigé par [MASK] [MASK] .
) , [MASK] [MASK] semblait penser pouvoir compter , à tort , sur le soutien de [MASK] [MASK] , nouvellement élu .
La défaite lors de la guerre des [MASK] précipite la chute du régime et une lente transition démocratique .
[MASK] [MASK] ( 1983 -- 1989 ) fut le symbole même du retour à la démocratie en [MASK] [MASK] .
Depuis lors , plusieurs présidents se sont succédé : [MASK] [MASK] ( 1989 -- 1999 ) , [MASK] [MASK] [MASK] [MASK] ( 1999 -- 2001 ) .
Le [MASK] aida beaucoup l' [MASK] à se développer durant cette période .
La consommation a augmenté considérablement , et les [MASK] ont alors pu accéder aux biens matériels qu' ont les [MASK] ; l' [MASK] , la télévision , la téléphonie mobile , l' électro-ménager moderne , etc .
Les laissés-pour-compte du miracle économique représentaient une part non-négligeable dans l' [MASK] des années 1990 : 18 % de chômeurs en 1996 .
Le gouvernement , le [MASK] et la parité entre le peso et le dollar américain sont les thèmes les plus critiqués .
En décembre 1993 , le pays a connu des révoltes , notamment à [MASK] [MASK] [MASK] .
Les protestations de décembre 2001 doivent être analysées en tenant compte des changements que le répertoire de l' action collective a connus ces dernières années en [MASK] .
Il appelle à des élections présidentielles anticipées en avril 2003 où il soutient le candidat péroniste de centre gauche [MASK] [MASK] .
Ce dernier est élu par défaut suite au retrait de [MASK] [MASK] au second tour .
Son épouse , [MASK] [MASK] [MASK] [MASK] , élue au premier tour le 28 octobre 2007 lui succède le 10 décembre 2007 .
L' [MASK] a un régime présidentiel dans une république fédérale .
L' [MASK] est membre permanent du [MASK] avec le [MASK] , le [MASK] , l' [MASK] et le [MASK] ; cinq autres pays y sont associés : la [MASK] , le [MASK] , le [MASK] , la [MASK] et l' [MASK] .
L' [MASK] fut le seul pays d' [MASK] [MASK] [MASK] à avoir pris part à la [MASK] [MASK] [MASK] [MASK] en 1991 , mandatée par l' [MASK] .
Elle fut également le seul pays latin à participer à l' opération démocratique à [MASK] en 1994 -- 95 .
En janvier 1998 , en reconnaissance de ses contributions à la sécurité internationale , le président des [MASK] [MASK] [MASK] désigna l' [MASK] comme l' un des alliés majeurs hors- [MASK] .
En 1993 , l' [MASK] lança l' initiative des casques blancs des [MASK] [MASK] spécialisés dans l' aide humanitaire .
Depuis 2004 , les relations habituellement cordiales entre l' [MASK] et l' [MASK] se sont progressivement dégradées à cause de la construction en [MASK] de deux grandes usines de fabrication de cellulose , sur les rives du [MASK] [MASK] qui marque la frontière entre les deux pays .
L' [MASK] met en avant les dégâts écologiques que subirait le fleuve .
Des blocus routiers en [MASK] ont empêché l' approvisionnement en matériaux de construction depuis le [MASK] , aggravant la situation , .
Douze pays d' [MASK] [MASK] [MASK] ont signé le 8 décembre 2004 la [MASK] [MASK] [MASK] visant à la réunion du [MASK] , de la [MASK] [MASK] et du [MASK] , de la [MASK] et du [MASK] en une seule communauté supranationale , la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) , sur le modèle de l' [MASK] [MASK] .
[MASK] bien que l' année suivante , elle mit sa priorité dans les initiatives régionales telles que le [MASK] ou la [MASK] [MASK] [MASK] après une décennie de partenariat avec les [MASK] .
En contentieux avec le [MASK] , l' [MASK] réclame la souveraineté des [MASK] [MASK] , de la [MASK] [MASK] [MASK] , des [MASK] [MASK] [MASK] [MASK] et des [MASK] [MASK] [MASK] [MASK] et d' environ 1 million km² du continent [MASK] .
Autre sujet de discorde est la frontière avec le [MASK] , en particulier au sujet du tracé de la frontière extrême sud en [MASK] [MASK] [MASK] , un traité fut signé en 1984 entre les deux pays au [MASK] .
Conformément à la constitution de 1853 , révisée en 1994 , l' [MASK] est une république fédérale organisée en 23 provinces et une cité autonome érigée en district fédéral : [MASK] [MASK] , capitale fédérale qui a un statut spécial .
Il fut formé en 1900 et couvrait alors la totalité de la [MASK] du nord-ouest du pays , mais , en raison d' un développement et d' une population très faibles , il fut dissous en 1943 , les territoires étant alors incorporés aux provinces de [MASK] , [MASK] et [MASK] .
La surface totale de l' [MASK] est répartie de la façon suivante ( excepté l' [MASK] ) :
L' [MASK] est longue de 3700 kilomètres du nord au sud et de 1400 kilomètres de l' est à l' ouest .
Le territoire peut être divisé en quatre zones distinctes : les plaines fertiles de la [MASK] au centre du pays , le plat pays de la [MASK] au sud ( s' étendant sur un gros quart sud du pays ( 28 % ) , jusqu' à la [MASK] [MASK] [MASK] ) , les plaines sèches du [MASK] [MASK] au nord et enfin la région très élevée de la [MASK] [MASK] [MASK] à l' ouest le long de la frontière avec le [MASK] dont le mont [MASK] culmine à 6960 mètres .
Parmi les grands fleuves , citons le [MASK] , le [MASK] , le [MASK] [MASK] , le [MASK] [MASK] , l' [MASK] , ainsi que le [MASK] qui est le plus long fleuve d' [MASK] .
Les fleuves [MASK] et [MASK] coulent vers l' [MASK] [MASK] et se rejoignent pour former l' estuaire du [MASK] [MASK] [MASK] [MASK] .
Des grands lacs comme des mers se sont formés au pied des [MASK] , dans des sites encore vierges tels le [MASK] [MASK] , à [MASK] [MASK] [MASK] [MASK] .
Dans les immenses étendues de la [MASK] subsiste encore une faune précolombienne représentée en particulier par le tatou , dit à neuf bandes : les gaúchos pourchassent ce mammifère édenté , car ils redoutent ses terriers , dans lesquels le bétail se casse les pattes .
Le sud du pays est exposé à une augmentation des UV solaires ( cancérigènes , mutagènes ) , induite par le trou de la couche d' ozone , plus grande au dessus de l' [MASK] qu' au dessus de l' arctique .
L' [MASK] fait partie du [MASK] .
Souffrant d' inflation et d' ingérence financière , le pays doit souvent faire appel aux organisations économiques internationales tel que le [MASK] .
L' [MASK] est la seconde puissance économique d' [MASK] [MASK] [MASK] , derrière le [MASK] .
Le pays possède une importante richesse agricole , de nombreuses capacités industrielles et un certain potentiel minier , pourtant l' [MASK] connaît d' importants problèmes économiques .
L' [MASK] est le pays le plus développé du continent latino-américain en 2005 selon les données des [MASK] fournies en 2007 et se rapproche des standards européens de niveau de vie .
L' [MASK] dispose de nombreuses richesses naturelles et d' une main-d'œuvre très qualifiée , d' une agriculture orientée vers l' exportation et d' un tissu industriel diversifié .
Jusque dans les années 1950 , à son apogée économique , l' [MASK] était l' un des pays les plus riches du monde .
Malgré ces atouts , l' [MASK] a accumulé à la fin des années 1980 une lourde dette externe ( dette qu' elle ne compte rembourser qu' en partie , " 10 % " ) , l' inflation atteignait 100 % par mois et la production avait considérablement chuté .
Pour lutter contre cette crise économique , le gouvernement de [MASK] a lancé une politique de libéralisation du commerce , de déréglementation et de privatisation .
Le 1 er février 2006 , l' [MASK] et le [MASK] signent , après près de trois ans de négociations , un accord qui doit permettre de protéger les secteurs de production qui pourraient être trop durement affectés par la compétition du pays voisin .
Depuis 2003 , l' [MASK] semble avoir repris le chemin de la forte croissance économique et de l' augmentation des salaires .
Cependant , l' [MASK] semble souffrir de la crise américaine et de la chute du dollar , en effet , la forte inflation avec un taux " officiel " de 8 à 9 % , pourrait en réalité atteindre 25 % en 2008 .
L' [MASK] compte près de 40 millions d' habitants .
Après les colons espagnols , il y eut plusieurs vagues d' immigration et d' autres [MASK] s' établirent en [MASK] durant les années 1870-1950 .
La population est très inégalement répartie , puisqu' un tiers de la population ( environ 13 millions d' habitants ) est concentré dans la capitale et l' agglomération de [MASK] [MASK] ( appelée aussi [MASK] [MASK] [MASK] ) .
Outre la région de la capitale fédérale , la population est concentrée dans d' autres zones urbaines dont les principales sont : [MASK] ( centre , 1,6 million d' habitants ) , [MASK] ( est , 1,4 million d' habitants ) , [MASK] ( ouest , 1 million d' habitants ) , [MASK] [MASK] [MASK] [MASK] ( nord , près d' un million d' habitants ) .
Traditionnellement , l' [MASK] a joui d' un très haut niveau de vie en comparaison avec d' autres pays de la région , mais la crise économique des années 2001 -- 2002 a diminué cette impression .
Le groupe [MASK] détient la principale chaîne de télévision du pays : [MASK] [MASK] , ainsi que le journal argentin le plus important en nombre de tirages : le quotidien centriste [MASK] .
Le second quotidien en nombre de tirages est [MASK] [MASK] , conservateur , suivi de [MASK] , de gauche , de bien moindre importance cependant .
Le service de communication téléphonique a été privatisé en 1990 par le gouvernement de [MASK] [MASK] , il y a 8,3 millions de lignes téléphoniques installées , soit 23 lignes pour 100 habitants .
Selon une récente étude , 92 % des [MASK] se déclareraient catholiques dont 18,5 % sont pratiquants .
Ainsi en 2008 , des manifestations de soutien à la [MASK] étaient organisées à [MASK] [MASK] , ainsi que d' autres manifestations de soutien à [MASK] .
L' [MASK] a pour codes :
L' éducation en [MASK] est de très grande qualité malgré l' instabilité économique du pays .