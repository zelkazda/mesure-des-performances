A sa mort , les deux époux étaient séparés et [MASK] vivait avec l' auteur [MASK] [MASK] .
[MASK] [MASK] est mort en 1995 , à l' âge de 58 ans , d' une insuffisance rénale faisant suite à un cancer du côlon .
La majorité des héros de [MASK] ne sont pas des humains normaux .
Et certains sont totalement non-humains : [MASK] utilise les points de vue d' entités diverses , d' extra-terrestres , de dieux , de super-ordinateurs et même d' un chien .
[MASK] a également écrit de nombreuses nouvelles se déroulant dans le multivers d' ambre : A l' exception de la première nouvelle , ces dernières forment une suite se déroulant après le dernier roman de la série .