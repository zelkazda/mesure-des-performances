Un prototype d' ordinateur à écran tactile , le [MASK] [MASK] a été présenté mais ne fut jamais commercialisé .
Avec l' apparition des microprocesseurs 32 bits sont apparus l' [MASK] [MASK] et le [MASK] .
Certains d' entre eux s' inspiraient de la conception du [MASK] , d' autres de celle du [MASK] ( avec un DSP ) , en intégrant des évolutions de processeur , et d' autres évolutions , comme l' utilisation d' un [MASK] [MASK] .
La gamme [MASK] [MASK] était en concurrence directe avec l' [MASK] [MASK] du constructeur [MASK] .
D' après certaine sources , il s' est vendu 6 millions d' [MASK] [MASK] dans le monde .
Le système d' exploitation est le [MASK] pour [MASK] [MASK] [MASK] .
L' [MASK] [MASK] est un ordinateur versatile permettant aussi bien de jouer que de travailler .