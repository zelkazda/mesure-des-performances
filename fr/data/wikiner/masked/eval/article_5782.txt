Le gallo ou langue gallèse , est une langue d' oïl traditionnelle du nord-ouest de [MASK] [MASK] , en [MASK] , dans le [MASK] , l' [MASK] ainsi qu' une partie de la [MASK] .
Une des variantes , essentiellement basée sur les parlers et prononciations de [MASK] [MASK] , a été normalisée pour l' enseignement .
Cette version est reconnue depuis 2004 comme l' une des " langues de [MASK] [MASK] " .
C' est-à-dire en langue bretonne , ceux qui ne parlent pas breton , ceux qui parlent français ; à l' époque le dialecte roman parlé par les habitants vivants dans cette partie occidentale de la [MASK] qui allait devenir l' est de la [MASK] après le [MASK] [MASK] [MASK] en 851 .
[MASK] [MASK] avait conduit en enquête en 1986 qui montrait qu' à peine plus de 4 % l' avaient employé depuis toujours ( dans les [MASK] ) , et le tiers d' entre-eux le trouvait " chargé d' une signification plutôt péjorative " , et que le terme " patois " était largement majoritaire .
Le gallo n' est pas différent des autres langues d' oïl du nord de [MASK] [MASK] .
Cependant ce dialecte prendra dans la partie occidentale de la [MASK] le nom de gallo ou de langue gallèse .
D' abord confiné à l' est de [MASK] [MASK] , le gallo s' est peu à peu étendu vers l' ouest , remplaçant peu à peu la langue locale .
Le gallo de la frontière linguistique vers l' angevin ( [MASK] ) , le tourangeot ( [MASK] ) ou le meuniot ( [MASK] ) est moins différencié .
Une certaine proximité entre le gallo et les différentes langues de l' ouest de [MASK] [MASK] existe mais est de moins en moins évidente au fur et à mesure qu' on s' éloigne des frontières orientales de [MASK] [MASK] .
En effet , depuis 1982 , le gallo est proposé en option facultative de langue au baccalauréat dans les quatre départements de la [MASK] [MASK] .
Par ailleurs , certaines collectivités territoriales de [MASK] ont mis en place une signalisation bilingue français-gallo , tandis que d' autres privilégient le bilinguisme français-breton aux dépens du gallo .
L' université de [MASK] offre également dans son département linguistique , un programme pédagogique d' enseignement du gallo
Cette orientation bretonnante est contestée et d' autres codifications sont proposées plus en accord avec les origines du [MASK] .
Certaines associations font un travail de normalisation du [MASK] .
On dit tout simplement le " patois " ( comme partout en [MASK] ) ou " parler nantais " .
Le parler nantais se distingue par les apports multiples dû à la situation géographique de [MASK] , la cité des ducs étant située au confins de plusieurs régions historiques ( [MASK] , [MASK] et [MASK] ) .
La vallée de la [MASK] fut notamment un lieu d' échange important , autant vers l' océan que vers l' intérieur des terres .