Il fut dépossédé de son pouvoir en 68 et se suicida assisté de son scribe [MASK] .
Certains historiens débattent encore aujourd'hui de la folie , réelle ou mise en scène , de [MASK] .
Les sources primaires concernant [MASK] doivent être lues avec précaution .
[MASK] et [MASK] avaient rang de sénateur .
Leurs descriptions des événements du règne de [MASK] sont suspectes dans la mesure où l' on sait que [MASK] persécuta les sénateurs romains à partir des années 65-66 à la suite de la découverte de deux conspirations .
Certains récits exaltés du règne de [MASK] pourraient donc n' être que des exagérations .
[MASK] [MASK] [MASK] est né le 15 décembre 37 .
Son oncle maternel [MASK] venait de commencer à régner le 16 mars de cette année , à 25 ans .
Ses prédécesseurs , [MASK] et [MASK] , avaient vécu respectivement jusqu' à 76 et 79 ans .
Si [MASK] vivait aussi longtemps qu' eux , il pouvait espérer une succession par ses propres descendants .
Un scandale marquant le début du règne de [MASK] fut sa relation particulièrement étroite avec ses trois sœurs [MASK] , [MASK] [MASK] et [MASK] .
Les écrits de [MASK] [MASK] , [MASK] , [MASK] [MASK] rapportent qu' elles avaient des relations incestueuses avec leur frère .
La mort rapide de [MASK] en 38 n' a fait que renforcer ce soupçon .
On disait d' elle qu' elle était la favorite de [MASK] ; elle a d' ailleurs été enterrée avec les honneurs dus à une impératrice .
[MASK] la déifia même , faisant d' elle la première femme de l' histoire romaine à obtenir cet honneur .
[MASK] n' avait toujours pas d' enfant .
Ils étaient les héritiers probables en cas de décès prématuré de [MASK] .
[MASK] ordonna également l' exécution de [MASK] [MASK] [MASK] [MASK] , le populaire légat de [MASK] [MASK] , et son remplacement par [MASK] [MASK] [MASK] .
[MASK] et [MASK] furent reléguées aux [MASK] [MASK] .
La chance lui sourit l' année suivante : le 24 janvier 41 , [MASK] , son épouse [MASK] [MASK] , et leur fille [MASK] [MASK] furent assassinés par une conspiration menée par [MASK] [MASK] .
La garde prétorienne aida [MASK] à obtenir le trône .
[MASK] se remaria rapidement au riche [MASK] [MASK] [MASK] [MASK] .
Son mari mourut entre 44 et 47 , et [MASK] fut suspectée de l' avoir empoisonné pour hériter de son immense fortune .
[MASK] , âgé de 57 ans à cette époque , avait régné plus longtemps , et sans doute plus efficacement que son prédécesseur .
[MASK] s' était déjà marié trois fois .
Il avait épousé [MASK] [MASK] et [MASK] [MASK] quand il était simple citoyen .
Empereur , il s' était marié à [MASK] [MASK] .
Le couple avait deux enfants , [MASK] ( né en 41 ) et [MASK] ( née en 40 ) .
[MASK] n' avait que 25 ans et pouvait lui donner d' autres héritiers .
Pourtant , [MASK] fut exécutée en 48 , accusée de conspiration contre son époux .
L' ambitieuse [MASK] projeta rapidement de remplacer sa tante par alliance .
Le 1 er janvier 49 , elle devint la quatrième femme de [MASK] , [MASK] [MASK] [MASK] [MASK] [MASK] .
La même année , [MASK] fait rompre les fiançailles d' [MASK] et de [MASK] [MASK] [MASK] et la fait fiancer avec [MASK] .
[MASK] était plus âgé que [MASK] , son frère adoptif , et cette adoption fit de lui l' héritier officiel du trône .
[MASK] honora son fils adoptif de plusieurs manières .
[MASK] fut émancipé en 51 , à 14 ans .
En 53 , il épousa sa sœur adoptive , [MASK] .
[MASK] mourut empoisonné le 13 octobre 54 et [MASK] fut rapidement nommé empereur à sa place .
Les historiens s' accordent à considérer que [MASK] a joué le rôle de figure de proue au début de son règne .
Les décisions importantes étaient probablement laissées entre les mains plus capables de sa mère [MASK] [MASK] [MASK] ( qui pourrait avoir empoisonné [MASK] elle-même ) , de son tuteur [MASK] , et du préfet du prétoire [MASK] [MASK] [MASK] .
[MASK] cherche dès le début de son règne à obtenir les faveurs de l' armée et de la plèbe par diverses primes .
Les problèmes devaient pourtant bientôt surgir de la vie personnelle de [MASK] et de la course à l' influence croissante entre [MASK] et les deux conseillers .
Tout le monde savait que [MASK] était déçu de son mariage et trompait [MASK] .
Il prit pour maîtresse [MASK] [MASK] , une ancienne esclave , en 55 .
[MASK] tenta d' intervenir en faveur d' [MASK] et exigea de son fils le renvoi d' [MASK] .
[MASK] résista à l' intervention de sa mère dans ses affaires personnelles .
Son influence sur son fils diminuant , [MASK] se tourna vers un candidat au trône plus jeune .
[MASK] , à treize ans , était toujours légalement mineur et sous la responsabilité de [MASK] , mais il approchait de l' âge de la majorité .
[MASK] était un successeur possible de [MASK] et établir son influence sur lui pouvait renforcer la position d' [MASK] .
[MASK] se révoltait de plus en plus contre l' emprise d' [MASK] , et il commençait à envisager le meurtre de sa propre mère .
[MASK] [MASK] [MASK] était au nombre de ces nouveaux favoris .
À tous points de vue , [MASK] était aussi débauché que [MASK] , mais il devint aussi intime qu' un frère .
[MASK] aurait présenté à [MASK] une femme qui aurait d' abord épousé le favori , puis l' empereur .
[MASK] ( [MASK] [MASK] ) était décrite comme une femme de grande beauté , pleine de charme , et d' intelligence .
On peut trouver dans de nombreuses sources les rumeurs d' un triangle amoureux entre [MASK] , [MASK] , et [MASK] .
En 58 , [MASK] avait assuré sa position de favorite de [MASK] .
L' année suivante ( 59 ) fut un tournant dans le règne de [MASK] .
[MASK] et/ou [MASK] auraient organisé le meurtre d' [MASK] .
[MASK] fut bientôt chassé de l' entourage impérial et envoyé en [MASK] comme gouverneur .
Leur remplaçant aux postes de préfet du prétoire et de conseiller fut [MASK] .
Il avait été banni en 39 par [MASK] , accusé d' adultère avec à la fois [MASK] et [MASK] .
Il avait été rappelé d' exil par [MASK] , puis avait réussi à devenir un proche de [MASK] ( et peut-être son amant ) .
Avec [MASK] , il aurait eu une plus grande influence que [MASK] en eut jamais sur l' empereur .
Quelques mois plus tard , [MASK] épousait [MASK] .
[MASK] , âgé alors de vingt-cinq ans , avait régné huit ans et n' avait pas encore d' héritier .
Mais [MASK] avait déjà acquis la réputation d' être infidèle , alors qu' [MASK] était connue pour être un parangon de vertu .
[MASK] réussit à obtenir le divorce pour cause d' infertilité , ce qui lui permettait d' épouser [MASK] et d' attendre qu' elle donne naissance à un héritier .
Au cours de cette année , [MASK] fit exécuter deux des membres restants de sa famille :
Début 63 , [MASK] donna naissance à une fille : [MASK] [MASK] .
[MASK] célébra l' évènement , mais l' enfant mourut quatre mois plus tard .
[MASK] n' avait toujours pas d' héritier .
Le feu débuta dans les boutiques des environs du [MASK] [MASK] .
Mais [MASK] perdit toute chance de redorer sa réputation en rendant trop vite public ses projets de reconstruction de [MASK] dans un style monumental .
La population désorientée cherchait des boucs émissaires , et bientôt des rumeurs tinrent [MASK] pour responsable .
Il était important pour [MASK] d' offrir un autre objet à ce besoin de trouver un coupable .
[MASK] nous fait le récit de cet épisode :
Bien que les anciennes sources ( et les lettrés ) penchent pour un [MASK] incendiaire , il faut rappeler que les incendies étaient fréquents dans la [MASK] [MASK] .
La célèbre [MASK] [MASK] faisait partie du projet de reconstruction imaginé par [MASK] .
En 65 , [MASK] fut impliqué dans un autre scandale , pris plus au sérieux par le peuple de cette époque qu' il ne le serait de nos jours .
De plus , [MASK] ordonna que [MASK] [MASK] [MASK] , un général populaire et valeureux , se suicidât , pour faire suite à de vagues soupçons de trahison .
Cette décision poussa les commandeurs militaires , à [MASK] et dans les provinces , à envisager l' organisation d' une révolution .
En 65 , [MASK] meurt alors qu' elle était enceinte , d' un coup porté au ventre par [MASK] , si l' on en croit [MASK] et [MASK] , et ce , malgré la passion qu' il semblait lui vouer .
[MASK] va d' abord essayer de se remarier à [MASK] [MASK] , la fille de [MASK] et d' [MASK] [MASK] ( sa demi-sœur par adoption ) .
Comme celle-ci refuse , [MASK] la fait tuer sous prétexte qu' elle fomentait un complot .
[MASK] se tourne alors vers son ancienne maîtresse , [MASK] [MASK] .
Dès le mois de septembre , [MASK] quitte sa jeune épouse pour un voyage de plus d' un an en [MASK] .
L' empereur partit en [MASK] , en 66 , où il distrayait ses hôtes avec des spectacles artistiques ( les écrits de [MASK] rapportent cependant que l' empereur empêchait quiconque de sortir de l' amphithéâtre lorsqu' il déclamait ses écrits , et que certains spectateurs durent se faire passer pour morts pour s' échapper , tant ils s' ennuyaient ) , alors qu' à [MASK] le préfet du prétoire [MASK] [MASK] cherchait à obtenir le soutien des gardes prétoriens et des sénateurs .
[MASK] , son ( autrefois ) fidèle serviteur , gouverneur d' [MASK] ( [MASK] ) , était l' un de ces nobles dangereux .
[MASK] [MASK] corrompit la garde impériale , qui se retourna contre [MASK] avec la promesse d' une récompense financière de [MASK] .
Ils s' appuient sur les textes de [MASK] , fréquemment colporteur de ragots , et de [MASK] , augmentés des attaques des auteurs chrétiens , et couronnés par des œuvres de fiction comme [MASK] [MASK] .
De plus , aucune loi anti-chrétienne ne fut promulguée sous son règne de manière officielle : il y a bien eu persécution , mais uniquement localisée à [MASK] .
Ainsi la grande popularité auprès du peuple de son temps prit , dès sa mort , le mythe du " retour de [MASK] " : caché chez les [MASK] , il devait réapparaître à la tête d' une armée pour vaincre les conspirateurs et rentrer victorieux à [MASK] .
[MASK] est le personnage principal de plusieurs opéras dont :