Celui-ci était plus facile à cultiver en [MASK] .
La conquête d' [MASK] en 642 par les [MASK] marqua le début de ce commerce .
Le poivre était également cultivé à [MASK] .
Le [MASK] [MASK] , qui ne produisait que 25000 tonnes en 1994 , est depuis 2001 le premier pays producteur et exportateur .
Il a le plus fort rendement à l' hectare : 1200 à 1300 kg ( l' [MASK] a un rendement de 314 kg ) .
En 2008 , le [MASK] [MASK] produit 34 % de la production mondiale ( 98500 tonnes ) .
En 2009 , la production mondiale est de 285000 tonnes , celle du [MASK] [MASK] de 105600 tonnes .