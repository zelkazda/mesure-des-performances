[MASK] [MASK] est né le 7 octobre 1951 à [MASK] , en [MASK] [MASK] [MASK] [MASK] [MASK] , d' un père bosniaque et d' une mère slovaque .
Son père était maître-tailleur et s' occupait personnellement de la garde-robe de [MASK] , qu' il avait connu dans la résistance et avec lequel il avait sympathisé .
Son enfance dans la [MASK] de [MASK] et son exil en [MASK] ont nourri l' univers envoûtant de ses albums .
1967 : Les [MASK] sont naturalisés .
1980 : [MASK] série personnelle , dans [MASK] , [MASK] [MASK] [MASK] [MASK] .
La seconde partie , [MASK] [MASK] [MASK] , est éditée en album en 1986 .
1982 : [MASK] [MASK] dessine sur verre une partie des décors de [MASK] [MASK] [MASK] [MASK] [MASK] , film d' [MASK] [MASK] .
1990 : [MASK] dessine les décors et costumes de [MASK] [MASK] [MASK] de [MASK] , sur une chorégraphie de son ami [MASK] [MASK] .
2009 : Dernier album " [MASK] " ( [MASK] ) .