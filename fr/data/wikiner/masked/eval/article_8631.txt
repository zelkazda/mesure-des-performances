[MASK] est située à l' est de la [MASK] et intégrée à l' agglomération de [MASK] .
Sur son territoire se trouve la zone industrielle de [MASK] [MASK] qui est le cœur économique de la [MASK] .
Le [MASK] [MASK] [MASK] y est aussi implanté .
Les communes limitrophes de [MASK] sont [MASK] , [MASK] [MASK] , [MASK] et [MASK] [MASK] .
La commune de [MASK] fait partie du [MASK] [MASK] [MASK] [MASK] .
C' est ainsi que fut longtemps considérée la région de l' actuelle commune de [MASK] , ouverte sur les deux baies de l' île , et dont une grande partie du territoire était , dans les années 1980 , couverte de marécages .
Cependant , la zone industrielle de [MASK] , face à [MASK] , entièrement gagnée sur la mangrove , a fait de [MASK] le poumon économique de la [MASK] .
C' est aussi [MASK] qui accueille le plus grand centre commercial de l' île .
Longtemps restée inhabitée en raison de son insalubrité , [MASK] n' est devenue commune à part entière qu' en 1837 .
1735 -- 1973 : La commune de [MASK] compte plus de deux siècles .
[MASK] a participé avec passion au grand élan de la [MASK] de 1789 et a offert ses baies hospitalières aux flibustiers qui y avaient d' insoupçonnées batteries , ses mornes et vallons de la pointe [MASK] .
En béton armé , cette ancienne centrale électrique de [MASK] est un bâtiment édifié après la [MASK] [MASK] [MASK] afin de renforcer la production d' électricité de la première centrale .
L' électrification de la [MASK] débute vers 1914 par l' installation de deux groupes électrogènes , à [MASK] et à [MASK] .
La première centrale à vapeur de 2750 kilowatts est construite en 1934 dans la commune de [MASK] et inaugurée en 1936 .
La façade , qui mêle volumes pleins et arrondis , rappelle certaines églises fortifiées du sud-ouest de [MASK] [MASK] métropolitaine avec ses tours symétriques de part et d' autre du clocher médian .