La commune est située au sud-est de [MASK] .
Couvrant 1870 hectares , le territoire de [MASK] [MASK] est le plus étendu des cinq communes du [MASK] [MASK] [MASK] .
Seule la route reliant [MASK] à [MASK] traverse cette [MASK] [MASK] [MASK] .
En 1901 , la commune se crée autour du village par démembrement de la commune de [MASK] .