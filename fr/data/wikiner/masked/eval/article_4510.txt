[MASK] est situé sur la [MASK] [MASK] mais est aussi à l' origine de la [MASK] [MASK] .
On rejoint facilement , à partir de la ville , la [MASK] , par l' intermédiaire de la [MASK] , et la [MASK] , via la [MASK] .
Elles sont essentiellement localisées sur les bord de l' [MASK] , où une piste de trial est également accessible .
L' histoire de la ville de [MASK] est étroitement liée à celle des [MASK] [MASK] [MASK] , puisqu' elle devient la capitale du duché et de ses importantes dépendances en 1327 .
Cet état de fait perdurera jusqu' en 1523 , avec la défection du [MASK] [MASK] [MASK] .
Une ville se développera autour : ce sera [MASK] , .
Les habitants de [MASK] pourront à l' avenir administrer eux-mêmes leur cité : quatre consuls , présidés par un fonctionnaire ducal , seront élus tous les ans .
Devenue ville franche , [MASK] attire nombres d' étrangers , l' activité économique naît et prend un essor important .
[MASK] [MASK] [MASK] [MASK] [MASK] , premier [MASK] [MASK] [MASK] , réside peu à [MASK] , de même que son fils et successeur , [MASK] [MASK] [MASK] .
Le duché n' avait pas alors de capitale fixe : la famille , originaire de [MASK] , résidait aléatoirement dans cette même ville , à [MASK] , [MASK] ou [MASK] .
C' est avec [MASK] [MASK] [MASK] [MASK] que [MASK] devient effectivement capitale du duché , et , comme lieu de résidence des ducs , de facto la capitale des territoires sous leur administration :
En 1400 , [MASK] compte 5000 habitants .
Le duc s' éteint en 1410 au [MASK] [MASK] [MASK] .
[MASK] [MASK] [MASK] lui succède .
De 1434 à 1456 , le [MASK] [MASK] [MASK] est [MASK] [MASK] [MASK] .
On peut encore observer des maisons de cette époque dans le vieux [MASK] .
Mort en 1588 , sans héritier légitime , ses successeurs sont ses frères : [MASK] [MASK] , cardinal archevêque-comte de [MASK] , qui abdique rapidement , et [MASK] [MASK] .
L' administration ducale , réorganisée et modernisée , compte près de 1650 fonctionnaires , rien que dans [MASK] .
[MASK] [MASK] ne va pas s' installer à [MASK] , mais , avec la famille royale , reste à [MASK] , qui devient alors la véritable capitale du pays .
Il restitue ses pouvoirs , en 1495 , à [MASK] , au roi .
Pendant le règne de [MASK] [MASK] , [MASK] [MASK] [MASK] conserve une très grande influence sur les affaires du duché .
L' habile choix de ce premier maire , et sa prestation immédiate de fidélité auprès du chancelier ducal conduisirent la duchesse [MASK] , au mois de décembre de la même année , à accorder une charte municipale qui entérine ce droit pour la ville , .
Tous ses domaines , le [MASK] , le [MASK] , la [MASK] , l' [MASK] , et bien d' autres sont mis sous séquestre .
[MASK] est déchue de son statut de capitale .
Cependant , en raison des prétentions territoriales que sa mère avait eu sur le [MASK] , [MASK] [MASK] er s' emploiera à faire de [MASK] le centre administratif d' une province royale .
La population ne cesse de croître , à tel point qu' à partir de 1536 , une seconde enceinte est érigée , pour protéger tous les faubourgs qui s' étalaient au-delà de la première muraille de [MASK] [MASK] .
Cet évènement conduit à une violente réaction dans les campagnes avoisinantes : tous ceux qui auraient soutenus les [MASK] , ou qui en seraient , sont massacrés .
A quel diocèse appartenait [MASK] ?
Enfin , en 1641 meurt à [MASK] [MASK] [MASK] [MASK] , fondatrice de l' [MASK] [MASK] [MASK] [MASK] .
Le 18 novembre 1632 , [MASK] [MASK] [MASK] , l' épouse du duc [MASK] [MASK] [MASK] [MASK] , qui vient d' être exécuté à [MASK] pour crime de lèse-majesté , arrive à [MASK] pour être retenue captive dans l' ancien palais ducal .
En 1653 , naît à [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , qui deviendra en 1733 maréchal général des camps et armées du roi .
Les rives de l' [MASK] sont urbanisées , avec l' expansion de la batellerie .
La coutellerie de luxe et l' industrie armurière de [MASK] connaissent leurs heures de gloire .
La première s' éteindra avec la [MASK] , et donc la fin de la haute noblesse , la seconde ne survivra pas à l' avènement des armes à feu .
D' après le journal de [MASK] [MASK] , médecin attaché à la personne du dauphin puis roi [MASK] [MASK] , on apprend qu' en 1603 la ville de [MASK] offre au dauphin , qui est âgé de deux ans , sa première armure .
Sous le règne d' [MASK] [MASK] , on entreprend d' établir une industrie séricicole à [MASK] , comme en beaucoup d' endroits du royaume .
Le 22 avril 1654 , [MASK] [MASK] modifie les critères d' admission au sein du conseil municipal , " afin de remédier aux brigues , cabales et monopoles pratiqués , depuis quelques années , dans la ville de [MASK] , pour l' élection du maire " : seuls des notables pourront y siéger .
Au niveau de [MASK] , [MASK] [MASK] est relativement étroit , et c' est sans doute une des raisons qui poussèrent initialement des populations à s' installer ici .
En 1499 , le duc [MASK] [MASK] prévoit de faire construire un pont de pierre , mais son projet n' est pas réalisé .
L' an 1532 marque un tournant , puisque l' on construit effectivement le premier pont de pierre de [MASK] .
C' est que le lit de la rivière est particulièrement sablonneux à [MASK] , rendant les fondations de ces édifices très fragiles .
Cependant , il faut ici constater que le mémorialiste affabule , puisque [MASK] meurt en 1708 .
Mais cette tentative n' est qu' éphémère , puisqu' elle prend fin , pour le [MASK] , dans le courant de 1781 .
A partir du 16 mars 1789 , les trois ordres sont réunis à [MASK] pour élire leurs représentants et rédiger les cahiers de doléances .
Le 21 mars , [MASK] connaît une nuit du 4 août anticipée : la noblesse fait savoir aux clergé et tiers état qu' elle renonce à tous ses privilèges : " la noblesse fait , avec plaisir , au tiers-état le sacrifice de tous ses privilèges pécuniaires , ne demandant à se réserver , franc de toutes impositions réelles , que le manoir seigneurial , avec sa cour et son jardin , pourvu que le tout n' excédât pas la valeur de deux arpents royaux , assurant que ladite noblesse se bornait à cette distinction purement honorifique " .
Elle siège , à partir de 1792 , en l' [MASK] [MASK] .
Les élections municipales de décembre 1792 mettent à la tête de la ville des citoyens " plus républicains " , qui commencent à prendre des mesures fortes , avec le soutien de la [MASK] populaire .
C' est bientôt l' avènement de la [MASK] .
Les représentants du peuple qui suivront [MASK] œuvreront tous dans le même sens .
Durant la [MASK] , à [MASK] , suite à la loi du 19 juillet 1792 , une manufacture d' armes est instituée , qui fonctionnera concurremment avec une fonderie de canons .
Voici ci-dessous le partage des sièges au sein du [MASK] municipal de [MASK] :
Liste de l' ensemble des maires qui se sont succédé à la mairie de [MASK] :
[MASK] [MASK] ( [MASK] ) depuis 26 octobre 1990 [MASK] ( [MASK] )