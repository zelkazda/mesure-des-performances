Les premiers exemples de telles unités remontent à l' [MASK] avec les dimaques d' [MASK] [MASK] [MASK] ou les [MASK] .
On situe d' ailleurs sous le règne d' [MASK] [MASK] l' apparition du nom " dragon " qui désignait à l' époque les arquebusiers à cheval , corps créé par le [MASK] [MASK] [MASK] pour servir dans l' armée du [MASK] .
Mais , bien que séduisante , cette hypothèse se heurte au fait qu' aucune arme à feu portative n' ait été désignée par le terme dragon en [MASK] -- un mousquet portait ce nom en [MASK] -- et que les récits des chroniqueurs militaires de l' époque ne font aucune allusion à la présence de dragons sur les étendards de la troupe du [MASK] [MASK] [MASK] .
En effet , en 1524 , les hommes du corps des arquebusiers à cheval de l' armée du [MASK] étaient à deux par cheval : un cavalier qui dirigeait l' animal et un tireur avec une arquebuse .
Quelques-uns considèrent que le comte [MASK] [MASK] [MASK] ( 1580-1626 ) , un des grands généraux de mercenaires de la [MASK] [MASK] [MASK] [MASK] , a inventé les dragons , mais c' est impossible , puisqu' ils existaient déjà longtemps avant lui .
En vérité , [MASK] quelquefois -- selon l' occasion et la nécessité -- a mis un grand nombre de ses troupes à cheval , parfois même avec un deuxième soldat " en croupe " , pour former ce qu' on appelait déjà à son époque une " armée volante " .
Ainsi , sous [MASK] [MASK] , les dragons français sont assimilés à de la cavalerie légère puis lourde et , à de rares exceptions près , ne combattent plus qu' à cheval .
La [MASK] crée de nombreux régiments de dragons .
Ces derniers , sous [MASK] [MASK] , sont envoyés dans les [MASK] et en [MASK] afin de contraindre les protestants à se convertir " pacifiquement " ( les dragons étaient logés chez l' habitant ) , d' où le nom de dragonnades .
Sous [MASK] [MASK] les régiments de dragons servent essentiellement en [MASK] .
Durant la [MASK] [MASK] [MASK] , les deux belligérants utilisent leur cavalerie comme unité de reconnaissance et comme unité d' infanterie montée .
Le 5 décembre 1972 , le [MASK] [MASK] décide d' abolir les formations de cavalerie dans l' armée par 91 voix contre 71 .