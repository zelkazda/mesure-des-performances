On connaît très peu de choses sur la vie de [MASK] .
L' historien romain est né entre 55 et 57 en [MASK] [MASK] ( à [MASK] ?
Fort de son éducation sévère et disciplinée , il fréquente le grammaticus , le rhetor et devient même , sans doute , l' élève de [MASK] .
En 77 , il épouse la fille du consul [MASK] [MASK] .
En 81 , sous [MASK] , il devient questeur .
En 88 , sous [MASK] , il devient préteur puis tribun de la plèbe .
De 89 à 93 , il devient légat de province en [MASK] [MASK] .
Il n' accepte le consulat qu' en 97 , devenant consul suffect , sous l' empereur [MASK] .
En 98 , lorsque [MASK] accède au pouvoir , [MASK] devient l' un des familiers de l' empereur et se retire de la politique sous [MASK] pour se consacrer à l' histoire et son écriture .
Cette biographie parait en 98 , cinq ans après la mort de [MASK] , le beau-père de [MASK] .
Ainsi l' œuvre se présente à la fois comme un éloge funèbre et un essai historique sur la [MASK] , sur ses habitants et sa conquête .
C' est aussi un manifeste contre la tyrannie de [MASK] , assassiné en 96 .
Ce qui est frappant dans cette œuvre , c' est l' approche originale que [MASK] fait du phénomène de la conquête impérialiste .
[MASK] fait preuve de beaucoup de lucidité en soulignant qu' [MASK] pratique une politique d' assimilation culturelle .
En 98 paraît également [MASK] [MASK] , petit ouvrage d' actualité -- [MASK] fortifiait la frontière du [MASK] -- , mais dont le caractère est plus nettement historique et ethnographique .
C' est une description des différentes tribus vivant au nord du [MASK] et du [MASK] .
[MASK] s' inspira nettement d' auteurs antérieurs comme [MASK] ou [MASK] [MASK] [MASK] .
L' ouvrage fut sans doute composé en 80 ou 81 , au moment où [MASK] était encore entièrement tourné vers l' éloquence et publié vraisemblablement en 107 .
Le contenu de l' œuvre originelle couvre les règnes suivants : [MASK] , [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
L' œuvre qui nous est parvenue s' achève au règne de [MASK] :
[MASK] avait une vision pessimiste de l' histoire et tenait des propos déconcertants .
Et c' est par une formule sans appel que [MASK] tire la leçon de ces temps abominables : " Les dieux , indifférents à notre sauvegarde , n' ont souci que de notre châtiment. "
Une des passions de [MASK] est la fascination des spectacles de mort .
Et la description que donne [MASK] de cette scène extraordinaire est elle-même imprégnée de cette fascination de l' horreur qui caractérise sa peinture des évènements .
Le pittoresque de [MASK] , c' est l' art de faire percevoir la monstruosité des êtres , des situations , des spectacles .
Celle-ci devait comporter 18 livres dont le contenu s' étend du début du règne de [MASK] ( 14 ap .
à la fin du règne de [MASK] ( 68 ap .
[MASK] puisa ses sources dans les ouvrages d' autres historiens , dans les registres publics et parfois dans sa propre expérience .
[MASK] savait que l' institution impériale était destinée à durer , mais il partageait l' antique conception romaine selon laquelle ce sont des individus qui font l' histoire .
De cette œuvre , [MASK] tirera le sujet d' une de ses tragédies : [MASK] .
[MASK] tourne essentiellement ses regards vers la politique intérieure et l' équilibre traditionnel entre " ce qui se passe à [MASK] " et " ce qui se passe à l' extérieur " n' est pas respecté .
Sa valeur d' historien est très contestée : [MASK] n' aurait pas été objectif dans ce qu' il écrivait et on conteste la rigueur de son information .
Il savait cependant nuancer son portrait laudatif par l' appréciation des erreurs de ses héros ( sa haine pour [MASK] et [MASK] ne l' empêche pas de leur donner une dimension exceptionnelle dans son œuvre ) .
[MASK] , lorsqu' il écrivait ses œuvres , combinait plusieurs sources , les interprétait et les repensait d' une manière originale .
Il combinait la pensée des trois grands historiens qui l' ont précédé : [MASK] , [MASK] et [MASK] .
Il était certainement un ami de l' empire , et sans aucun doute un ami de [MASK] .
Son but premier n' était en fait pas de servir les empereurs , mais [MASK] .
Il aurait été très proche d' [MASK] et de [MASK] .
[MASK] accorde dans son œuvre une grande place à la philosophie , qu' il connaît sans doute grâce aux maîtres grecs qui se trouvaient à [MASK] à son époque .
Les grands thèmes de l' œuvre de [MASK] sont la glorification des grands administrateurs , la défense libérale de la domination romaine , la critique de la tyrannie , et l' éloge de la sagesse philosophique tempéré par la confiance à l' égard du fanatisme et du dogmatisme .
[MASK] savait faire des portraits grandioses et sobres de ses personnages .
Sa production littéraire , s' inscrivant dans le cadre de son amitié pour [MASK] et [MASK] [MASK] [MASK] , était appréciée par le milieu impérial .
[MASK] fut l' historien officieux du régime , ce qui ne l' empêcha pas d' être aussi un historien critique .