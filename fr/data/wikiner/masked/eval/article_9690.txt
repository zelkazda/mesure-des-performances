[MASK] [MASK] ou [MASK] [MASK] est une ville de l' ouest de l' [MASK] [MASK] non loin de la charnière séparant le [MASK] de l' [MASK] , à 80 km de la [MASK] [MASK] et capitale de la province de [MASK] .
Elle abrite la [MASK] au cœur de la mosquée [MASK] [MASK] , ce qui en fait la ville sainte la plus sacrée de l' islam .
Le port de [MASK] n' est distant que de 65 kilomètres .
La partie centrale a une altitude moyenne de 294 m et la [MASK] est à 300 m .
La température à [MASK] [MASK] atteint un maximum de 48 degrés l' été et un minimum de 18 degrés l' hiver , avec une moyenne comprise entre 29,9 et 31 degrés , ce qui en fait une des régions les plus chaudes du monde .
La ville de [MASK] [MASK] prospère surtout grâce aux millions de pèlerins qui s' y rendent chaque année .
Les précipitations concernent [MASK] [MASK] dans des petites quantités , principalement entre novembre et janvier .
L' accès à [MASK] [MASK] est interdit aux non-musulmans ( al-balad al-harām : c' est-à-dire : " territoire sacré " ) .
Avant l' islam , [MASK] [MASK] est déjà un haut lieu de vénération .
Dieu aurait alors ordonné à [MASK] ( [MASK] ) de la reconstruire avec l' aide de son fils [MASK] .
Cependant , même si la [MASK] mecquoise a été édifiée à une époque indéterminée , peut-être vers la fin de la période romaine , il semblerait qu' elle apparaisse probablement au départ comme un simple enclos de pierres sans toit , édifié à proximité immédiate d' un point d' eau salvateur au fond d' une vallée sèche et non arborée .
Sa construction dans ce lieu insolite signalait manifestement déjà une intention cultuelle et confirmait son caractère d' espace sacré , dans lequel chaque tribus nomades d' [MASK] y déposaient les statues ( asnam ) de ses dieux .
Vers l' an 200 , le [MASK] devient une région qu' empruntent de nombreuses caravanes .
Cette ville voit la naissance de [MASK] en 570 dans une famille de marchands caravaniers très influente .
Ceux-ci le chassent avec ses premiers compagnons , lesquels doivent s' exiler vers l' oasis de [MASK] ( [MASK] ) le 9 septembre 622 .
Après des campagnes militaires victorieuses et un grand nombre de conversions , [MASK] revient en 630 à [MASK] [MASK] .
Étant à la tête d' une armée de plusieurs milliers d' hommes après le début de la huitième année de l' hégire , [MASK] entoure la ville de nuit avec des torches allumées .
Voyant les excès commis durant ces représailles , [MASK] intervint et proclama la paix générale .
C' est en allant attaquer [MASK] [MASK] que [MASK] autorisa pour la première fois la rupture momentanée du jeûne pendant le ramadan .
Contrairement à des rumeurs infondées , [MASK] respectait les animaux .
Après avoir pris la cité , [MASK] la consacre ville sainte .
La [MASK] est débarrassée de ses idoles païennes en janvier 630 .
[MASK] [MASK] , comme [MASK] , seront interdites aux non-musulmans à partir de ce moment-là .
Après la mort de [MASK] en 632 , l' islam commença son expansion géographique .
[MASK] [MASK] attira alors de plus en plus de nouveaux convertis venus en pèlerinage et gardera son caractère de capitale religieuse et de cité commerciale .
En 930 , une secte ismaelienne , les [MASK] se livreront au sac de la ville sainte .
Ceci , jusqu' à ce que [MASK] [MASK] , le vice-roi d' [MASK] , reprenne son contrôle en 1813 .
[MASK] [MASK] est un centre fondamental de la vie religieuse musulmane .
L' un des cinq piliers de l' islam stipule en effet que tout croyant doit faire un [MASK] [MASK] [MASK] [MASK] , s' il en a les moyens .
[MASK] [MASK] est aussi la direction , la qibla , vers laquelle les musulmans qui prient se tournent au cours de leurs prières .
Selon la tradition musulmane , la [MASK] est construite par [MASK] , premier prophète , et fut reconstruite par [MASK] ( [MASK] ) et son fils [MASK] .
Ce sanctuaire est le plus grand au monde et aurait été construit ultérieurement autour de la [MASK] par le prophète [MASK] .
Une pierre noire , creuse , est enclose dans l' un des angles de la [MASK] .
Cet hôtel sera surmonté d' une horloge six fois plus grande que celle de [MASK] [MASK] , à [MASK] .