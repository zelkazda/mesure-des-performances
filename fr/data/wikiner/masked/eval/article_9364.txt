La division en deux cycles , inspirée du modèle anglo-saxon avec son cursus undergraduate et son cursus postgraduate ( ou graduate aux [MASK] ) semble peu à peu laisser place à une division en trois cycles plus proche du système LMD français par exemple .
Le 25 mai 1998 , les quatre ministres chargés de l' enseignement supérieur d' [MASK] , de [MASK] , de [MASK] et d' [MASK] se retrouvent lors d' un colloque à la [MASK] , à l' occasion de la célébration du 800 e anniversaire de l' [MASK] [MASK] [MASK] , pour lancer un appel à la construction d' un espace européen de l' enseignement supérieur .
Lors de la conférence de [MASK] en juin 1999 , 29 pays signent un texte commun .
À [MASK] , les 16 et 17 septembre 2003 , les ministres décident d' accélérer le processus et de l' étendre au cycle doctoral afin d' approfondir les liens entre l' espace européen de l' enseignement supérieur et celui de la recherche .
À [MASK] a été signée un nouveau communiqué en mai 2005 :
La conférence ministérielle de [MASK] a eu lieu en mai 2007 à [MASK] , au [MASK] [MASK] .
La conférence ministérielle organisée par les pays du [MASK] a eu lieu en [MASK] , les mardi 28 et mercredi 29 avril 2009 .
Il s' agit de l' ensemble des pays du continent [MASK] , de la [MASK] et de la [MASK] , à l' exception de la [MASK] , de [MASK] , de [MASK] [MASK] , et du [MASK] .
En [MASK] , le premier grade , le baccalauréat , est obtenu en 3 ans .
Depuis 2002 , en [MASK] , toutes les licences se font en 3 ans ( contre 4 avant 2001 ) .
[MASK] [MASK] distingue deux types de masters , héritage de la séparation entre le diplôme d' études supérieures spécialisées et le diplôme d' études approfondies , anciens diplômes nationaux à bac+5 délivrés par les universités :
Ailleurs en [MASK] cette distinction est moins importante .
Au [MASK] le bachelor s' obtient généralement en 3 années et le master s' obtient en une année ( quelquefois deux ) après le bachelor .
Depuis l' an 2000 l' ancienne organisation didactique , composée d' un seul cycle d' études qui durait 4 à 6 ans , était modifiée pour faciliter la compatibilité des enseignements et les échanges culturels au sein de la [MASK] [MASK] .
[MASK] les étudiants veulent prolonger leurs études d' un an , ils obtiennent un diplôme post-grade , souvent appelé " master of advanced studies " .