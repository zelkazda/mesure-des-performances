L' [MASK] est connu pour sa grande diversité géologique , avec des montagnes enneigées , des vallées aux fortes rivières et des déserts arides aux formes géologiques spectaculaires .
L' [MASK] est un centre pour la technologie de l' information , le transport et les mines .
La ville de [MASK] est nommée ainsi en l' honneur d' [MASK] [MASK] qui voyagea dans la région en 1825 .
Après le [MASK] [MASK] [MASK] [MASK] , l' [MASK] est cédé aux [MASK] en 1848 ( voir : [MASK] [MASK] ) .
L' [MASK] est l' un des [MASK] [MASK] [MASK] .
L' [MASK] est globalement rocheux avec trois régions géologiques distinctes : les [MASK] [MASK] , le [MASK] [MASK] et le [MASK] [MASK] [MASK] .
L' [MASK] est connu pour sa diversité naturelle et abrite des formations géologiques très diverses , des déserts arides avec des dunes de sables aux prospères forêts de pin dans des vallées de montagne .
L' [MASK] est caractérisé par une grande diversité géologique : au centre se trouvent les [MASK] [MASK] , qui s' élèvent à environ 3650 m au-dessus du niveau de la mer .
À l' ouest des [MASK] [MASK] , se trouve le [MASK] [MASK] [MASK] qui appartient au [MASK] [MASK] ( [MASK] [MASK] ) .
Les paysages du sud de l' [MASK] se caractérisent par des formes érodées par le [MASK] et ses affluents .
L' [MASK] s' étend sur une superficie de 219932 km² mais ne compte moins de 2700000 habitants .
Elle est située dans une série de vallées et de bassins et centrée autour de [MASK] [MASK] [MASK] .
Outre [MASK] [MASK] [MASK] , les principales villes de cette zone sont [MASK] , [MASK] , [MASK] [MASK] [MASK] , [MASK] , [MASK] [MASK] , [MASK] et [MASK] .
L' [MASK] est subdivisé en 29 comtés .
L' [MASK] a pour codes ;
Les déserts du sud de l' [MASK] occupent 5 parcs nationaux :
La [MASK] [MASK] et [MASK] [MASK] [MASK] relient les grands aéroports des [MASK] à ceux de [MASK] [MASK] [MASK] et [MASK] [MASK] .
Quand la nature bâtit ses cathédrales : à plus de 2000 m d' altitude , des milliers d' aiguilles et de flèches de calcaire orange , roses , blanches se dressent vers le ciel à [MASK] [MASK] [MASK] [MASK] , l' un des plus célèbres sites de l' [MASK] .
Le grand désert salé beaucoup plus grand que le lac , s' étend sur plus de 400 km , jusqu' au [MASK] .
La lente évaporation de l' eau a formé une couche de sel tellement dure et régulière qu' il serait très difficile de l' obtenir par des moyens techniques , endroits rêvé pour les amateurs pour les records de vitesse ou pour les randonnées à moto , c' est là sur le circuit de [MASK] , qu' une voiture roula en 1965 à près de 1000 km/h .
Les monuments nationaux de l' [MASK] sont :
Enfin , l' [MASK] dispose de plusieurs parcs fédéraux et monuments :
Il dispose en outre d' une industrie relativement importante et de ressources minières de valeur : or , cuivre , plomb et aussi dans la région de [MASK] , de l' uranium .
La constitution de l' [MASK] date de 1895 .
En 2004 , par référendum , les électeurs de l' [MASK] ont approuvé par 66 % des voix ( 82 % hors [MASK] [MASK] [MASK] ) un amendement constitutionnel définissant le mariage comme une union civile entre un homme et une femme , bannissant ainsi toute forme d' union homosexuelle .
Aux élections fédérales , l' [MASK] est incontestablement l' un des bastions républicains les plus marqués du pays .
Aucun candidat démocrate n' a remporté l' [MASK] depuis [MASK] [MASK] en 1964 contre [MASK] [MASK] .
En 1992 , le démocrate [MASK] [MASK] était même arrivé troisième avec 24,65 % des voix derrière le républicain [MASK] [MASK] ( 43,36 % ) et l' indépendant [MASK] [MASK] ( 27,34 % ) .
L' [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , dont ils sont membres , a été organisée aux [MASK] en 1830 par [MASK] [MASK] .
L' [MASK] a accueilli les [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .