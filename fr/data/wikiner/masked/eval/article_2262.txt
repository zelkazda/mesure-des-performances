[MASK] est la capitale et la plus grande ville d' [MASK] .
Après 1945 et jusqu' à la chute du [MASK] [MASK] [MASK] en 1989 , la ville est partagée en quatre secteurs d' occupation .
[MASK] est une ville mondiale culturelle et artistique de premier plan .
En 2008 , [MASK] a accueilli près de 8 millions de visiteurs .
[MASK] se situe dans la plaine germano-polonaise , à 33 m d' altitude , au confluent de la [MASK] et de la [MASK] .
[MASK] est redevenue la capitale de l' [MASK] le 3 octobre 1990 .
Il a cependant fallu un vote tendu et très serré au [MASK] -le 21 juin 1991 -- pour que la décision soit prise de transférer effectivement les institutions de [MASK] à [MASK] .
Le transfert du gouvernement et du chancelier à [MASK] a eu lieu en 1999 .
[MASK] et [MASK] possèdent une organisation similaire .
[MASK] n' est pas une ville grise et triste mais égayée par plusieurs rivières , canaux , parcs et lacs .
Du fait de ce développement décentralisé , [MASK] présente de nombreuses choses à voir , dans son centre comme dans sa périphérie .
Pour diverses raisons , la [MASK] [MASK] [MASK] ( [MASK] [MASK] ) est devenue l' emblème de la ville -- et plus encore , puisqu' elle représente aussi la réunification des deux [MASK] .
Deux tours s' élancent dans le paysage berlinois : la [MASK] ( tour de la télévision ) , sur l' [MASK] dans le quartier [MASK] , et la [MASK] ( tour de la radio ) qui se trouve dans le parc des expositions de [MASK] .
La réputation du clubbing berlinois est reconnue et enviée dans le monde entier grâce à des discothèques légendaires , tel le fameux [MASK] et , plus récemment , le [MASK] , deux institutions mondialement connues pour leur programmation musicale combinée à une certaine liberté sexuelle de leur clientèle .
[MASK] a donc une vie culturelle riche et très diverse .
Aujourd'hui , [MASK] doit faire face à de graves difficultés financières , mais les manifestations culturelles continuent .
De plus , la chaîne musicale [MASK] [MASK] a aussi déménagé son siège de [MASK] pour [MASK] fin avril 2004 .
Enfin , [MASK] est aussi une référence pour le cinéma avec l' accueil chaque année en février de la [MASK] , festival international de cinéma dont la récompense suprême est l' [MASK] [MASK] [MASK] .
L' [MASK] [MASK] [MASK] abrite :
[MASK] accueille chaque année en février la [MASK] .
Quelques films se déroulant à [MASK] :
À [MASK] , il y a deux jardins zoologiques : Le [MASK] [MASK] [MASK] ( [MASK] [MASK] [MASK] ) , fondé déjà en 1844 , et le [MASK] [MASK] [MASK] [MASK] , fondé en 1954 .
Toutefois , en 2007 [MASK] a dégagé pour la première fois de son histoire un excédent budgétaire .
Elle y a toujours son siège , contrairement à beaucoup d' autres sociétés berlinoises qui ont quitté la ville après la construction du [MASK] , par peur d' être coupées de leurs fournisseurs et de leurs marchés .
Dans les années 1990 , [MASK] s' est largement désindustrialisée .
Si le secteur des services occupe une place croissante à [MASK] , la fonction publique reste le premier employeur de la ville .
[MASK] est ainsi la ville la plus touristique d' [MASK] .
[MASK] dispose également de 6 lignes publiques de ferry ( bateau ) .
La ville est traversée d' est en ouest par le [MASK] [MASK] .
Une partie des travaux ( notamment concernant la desserte de la gare par les transports urbains ) est retardée , voire suspendue sine die , faute de financements , [MASK] étant au bord de la faillite .
Il existe une deuxième rocade qui fait le tour de la ville à une plus grande distance qui est le plus grand périphérique d' [MASK] .
Pour le transport ferroviaire , la [MASK] [MASK] fait rouler des trains et des express régionaux ainsi que des [MASK] .
Pour le transport aérien , [MASK] possède deux aéroports : [MASK] et [MASK] .
Il sera suivi par [MASK] , dont la fermeture est prévue en 2011 ou 2012 .
[MASK] a accueilli les [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] et a été une des villes de la [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] dont elle a accueilli la finale à l' [MASK] .
Le [MASK] [MASK] , ensemble des tournois de tennis féminin , comprend l' [MASK] [MASK] [MASK] organisé annuellement dans la ville depuis 1979 .
[MASK] est aussi la ville des [MASK] [MASK] du [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] , une équipe qui a été fondée à l' époque de l' [MASK] [MASK] [MASK] [MASK] .
Devenue 1 er champion de la toute nouvelle [MASK] [MASK] lors de la saison 2008/2009 , l' équipe de football du [MASK] [MASK] [MASK] jouera en [MASK] [MASK] la saison prochaine .
À [MASK] sont nés :
À [MASK] sont décédés :
En 2005 , [MASK] est en deuxième position pour ce qui est du taux de délinquance en [MASK] ( 15002 délits pour 100000 habitants ) .