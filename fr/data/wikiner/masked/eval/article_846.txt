Cet homme passe pour un authentique schizophrène , depuis le jour où il détruisit complètement son studio ( [MASK] [MASK] ) en y mettant lui-même le feu en 1981 .
Aujourd'hui [MASK] [MASK] donne dans le reggae international avec un son new roots , digital , et il n' est désormais plus que chanteur .
" Cet homme est le [MASK] [MASK] jamaïcain "
[MASK] [MASK] est pour les connaisseurs une figure emblématique de la musique expérimentale -- comme [MASK] [MASK] pour le rock -- même s' il n' est pas reconnu ( ni même connu ) parmi le monde de la musique savante .
[MASK] [MASK] a souvent été en avance sur ses contemporains et découvrit des univers musicaux , par le sample et d' autre audaces , qui ont eu du succès .
[MASK] [MASK] joua un rôle initiateur , un rôle de père pour le reggae , le dub et la plupart des musiques dites " actuelles " qui en découlent .
[MASK] [MASK] gagne peu , connaît la misère et décide vers l' âge de 15 ans de partir .
Il s' installa a plusieurs endroits différents , alternant entre travaux manuels , danseur et joueur de dominos ( le domino est un jeu très courant en [MASK] , et on y joue de l' argent ) .
Après maintes exodes , un mariage qui ne dura pas longtemps , il arriva à [MASK] dans la fin des années 1950 , avec l' ambition d' y faire une carrière de chanteur dans un climat favorable à l' émergence d' une nouvelle musique jamaïcaine qui va faire du bruit dans l' île , le ska qui est le début d' une grande onde sismique que sera la musique jamaïcaine du début des années 1960 .
Ce climat d' émergence pour la musique est intimement lié avec celui qui amènera la [MASK] à son indépendance en 1962 .
[MASK] [MASK] traîne dans la capitale [MASK] qui connaît une poussée urbaine incroyable à cette époque , poussée urbaine qui est causée par l' exode rural , et qui est à l' origine de nombreux ghettos , et dont les sound system vont pouvoir profiter pour se créer un public .
Beaucoup de gens comme [MASK] [MASK] rêvaient à l' époque de faire une carrière dans la musique .
[MASK] [MASK] est présenté à [MASK] [MASK] [MASK] [MASK] [MASK] , dans l' espoir de devenir chanteur , cependant ce dernier qui trouvait qu' il n' avait pas de voix refusa de lui enregistrer un disque .
Mais vraisemblablement pris par une certaine amitié pour lui , [MASK] accepta quand même que [MASK] [MASK] travaille pour lui .
Le travail que [MASK] [MASK] faisait consistait à faire tout travail qui puisse aider [MASK] , en rapport avec le studio ou non .
Comme [MASK] [MASK] en témoigna : " [MASK] avait des gens avec qui il aimait bien être , qui l' assistaient et dont il voulait leur présence , j' étais l' un d' eux " .
[MASK] [MASK] était la personne de [MASK] [MASK] qui écrivait ces chansons adressées aux concurrents comme [MASK] [MASK] .
À plusieurs reprises il retente de se faire imposer comme chanteur à [MASK] [MASK] mais [MASK] refuse obstinément , bien que [MASK] [MASK] compose des chansons beaucoup plus osées et intéressantes .
Pendant cette période où il travaille à [MASK] [MASK] , il collaborera avec [MASK] [MASK] avec qui il liera une relation durable .
Il va aussi y faire la connaissance des [MASK] avec qui il composa et chanta .
Il assistera aussi aux débuts de [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] et d' autres ... .
Cette chanson ainsi que quelques autres ont souvent été enregistrées discrètement en fin de session d' autres enregistrements considérés comme plus important par [MASK] qui ne voulait pas entendre [MASK] [MASK] chanter .
[MASK] [MASK] est exploité par [MASK] ( comme la plupart des musiciens qui travaillaient pour lui ) , on lui pille ses chansons car il n' est pas crédité comme l' auteur de celles qu' il écrit pour d' autres , il est aussi mal payé .
C' est pourquoi en 1966 il quitte [MASK] [MASK] .
Il se met alors à travailler avec et pour [MASK] [MASK] chez qui il produira quelques 45 tours .
[MASK] commence petit à petit à voler de ses propres ailes en produisant lui-même ses disques bien qu' il ne possède pas encore son propre studio .
Il va aussi travailler quelque temps en collaboration avec [MASK] [MASK] .
1969 sera une année où [MASK] [MASK] [MASK] [MASK] [MASK] va apparaître sur le devant de la scène .
On commence à entendre le mot reggae à cette époque , et l' origine de ce terme comme celle de ce qu' on appelle le reggae reste disputée et [MASK] [MASK] fait partie de ces quelques protagonistes que l' on soupçonne d' avoir " inventé le reggae " du fait de l ' " avance " qu' il avait prise au niveau musical par rapport à d' autres musiciens de l' île .
A la fin 1969 , les [MASK] , désemparés et cherchant à se faire enregistrer s' adressèrent à [MASK] [MASK] qu' ils connaissaient déjà .
[MASK] les accepta donc .
[MASK] [MASK] sent quand même que [MASK] [MASK] , qui devenait le personnage prédominant du trio , ne faisait pas de son mieux , que les chansons qu' il écrivait n' étaient pas encore captivantes , qu' il n' était pas encore grand ( ses dreadlocks non plus ) et il en conclut qu' il était possédé par un mauvais esprit ( duppy en jamaïcain ) .
Beaucoup de connaisseurs de reggae considèrent cette période comme majeure dans l' histoire du reggae , car non seulement ce fut selon eux celle pendant laquelle les [MASK] firent leurs meilleurs chansons , mais aussi celle qui permit d' élaborer et d' affiner le son " reggae " , annonçant un âge d' or qui n' allait tarder à arriver pour [MASK] [MASK] , [MASK] [MASK] mais aussi pour la musique jamaïcaine dans son ensemble .
[MASK] [MASK] confia quelques années plus tard que [MASK] [MASK] était selon lui un génie .
[MASK] en fit de même pour [MASK] [MASK] en le considérant comme le " meilleur musicien qu' il ait jamais connu " .
Par cette collaboration qui leur permit d' aller très haut plus tard , [MASK] [MASK] et [MASK] [MASK] [MASK] [MASK] [MASK] sont aujourd'hui considérés comme monuments du reggae .
Après avoir enregistré des œuvres majeures que sont les chansons compilées dans les albums [MASK] [MASK] , [MASK] [MASK] , les [MASK] quittent [MASK] [MASK] et " s' emparent " des frères [MASK] qui ne sont désormais plus des membres des [MASK] et deviennent des membres permanents des [MASK] .
Durant cette période [MASK] [MASK] commence à s' affirmer , même s' il ne possède pas encore son propre studio .
Durant cette période , [MASK] [MASK] a enregistré beaucoup de disques qui resteront quasiment inconnus hors de [MASK] jusqu' à temps qu' on les réédite vers la fin des années 1990 .
[MASK] [MASK] , ses chanteurs et ses musiciens accomplirent en effet plusieurs tours de force .
Cette dernière chanson , chantée avec beaucoup de conviction et avec une rare puissance , connaîtra un long destin , [MASK] [MASK] en fit un de ses classiques et la réutilisa une dizaine de fois en dub , instrumental , versions DJ ... .
[MASK] [MASK] reprend quelques uns des morceaux cités ci-dessus , les remixe , et produit ainsi l' album [MASK] [MASK] [MASK] .
De gros doutes sont émis sur la contribution de [MASK] [MASK] sur cet album : une version des faits relate que cet album est une grande et unique rencontre entre [MASK] [MASK] et [MASK] [MASK] , une autre voit en cet album un chef-d'œuvre du génie de [MASK] [MASK] .
Celle qui semble la plus complète est l' édition intitulée [MASK] [MASK] [MASK] [MASK] [MASK] qui contient 14 plages alors que les autres n' en contiennent que 12 et c' est d' ailleurs celle-ci qui est reconnue par [MASK] [MASK] , biographe de [MASK] [MASK] .
Les autres versions sont sorties sous le nom [MASK] [MASK] [MASK] avec sur la pochette soit un lion fumant un joint , soit un tableau noir et l' ordre et le nom des plages ont été changés .
Cet album est au vu des admirateurs de [MASK] [MASK] un des plus importants qu' il n' ait réalisé , elle est aussi une œuvre qui commence à être reconnue par d' autres et qui est en train de se faire une place dans l' histoire de la musique expérimentale .
[MASK] [MASK] fonde avec les chansons qu' il a produites les bases de la musique qu' il produit dans les années qui suivent , il fonde en quelque sorte les bases de son studio qui va bientôt voir le jour .
En 1973 , [MASK] [MASK] commença à souffrir de la pression d' avoir à compter sur des studios commerciaux pour son travail .
Aux commandes de son propre studio , [MASK] porta ses compétences à un autre niveau , faisant de sa table de mixage un véritable instrument .
Même le fils prodigue [MASK] [MASK] retourne au [MASK] [MASK] et enregistre plusieurs morceaux .
Alors que d' autres studios pensaient " productivité " , [MASK] était heureux de prendre tout le temps nécessaire pour arriver à la bonne vibe .
Une session d' enregistrement au [MASK] [MASK] faisait penser à une fête , [MASK] laissant les portes du studio en béton ouvertes , permettant aux curieux de venir y faire un tour , pendant qu' il dansait , tapait des mains , et criait son approbation lorsqu' il était satisfait du résultat .
[MASK] utilisait d' excentriques méthodes , comme nettoyer les têtes des cassettes avec son t-shirt pour ensuite souffler de la fumée de ganja sur les bandes pendant qu' elles tournaient , s' assurant que la musique enregistrée au [MASK] [MASK] ait un son brut , une qualité magique qui ne pourrait être surpassée .
Utilisant un équipement basique , [MASK] était capable d' utiliser un 4 pistes et de donner l' impression d' un 8 pistes ou plus en mixant plusieurs pistes sur une seule et en répétant le processus .
" Il n' y avait que 4 pistes sur la machine, " expliqua [MASK] [MASK] , énigmatique " mais j' en piochait 20 chez les extra-terrestres " .
Entre 1974 et 1979 , beaucoup de pièces maîtresses de l' âge d' or du reggae sortirent du [MASK] [MASK] .
Notamment [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , et [MASK] [MASK] , ainsi que de nombreux singles .
Pendant ce temps , le climat politique en [MASK] commençait à chauffer .
Les deux principaux partis disposaient de " gunmen " pour faire régner l' ordre dans les rues de [MASK] et asseoir leurs positions .
Durant cette période , les productions [MASK] [MASK] reflétèrent assez fidèlement ce climat et cette confusion .
Contre ce contexte vif , le son du [MASK] [MASK] commençait à être internationalement reconnu .
Des journalistes commençaient à voyager en [MASK] pour rencontrer l' [MASK] , dont le studio devint célèbre au-delà de l' île .
[MASK] [MASK] avait considéré certains de ses plus grands enregistrements comme insortables .
Le [MASK] [MASK] est également devenu la cible de bandits locaux qui ont commencé à faire pression pour obtenir de l' argent de protection .
La relation de [MASK] avec sa femme a commencé à tomber en morceaux .
Le [MASK] [MASK] a bientôt atteint le point d' ébullition , et un point de non retour pour [MASK] .
[MASK] se retrouve sur une corde raide entre l' imagination et la réalité , et le départ de sa famille a semblé l' enfoncer un peu plus dans le chaos .
Une nouvelle et inquiétante personne a émergé , et tandis que [MASK] déclarait que c' était une démarche volontaire pour nettoyer la maison de gens qu' il ne voulait plus autour de lui , l' humeur de l' [MASK] était clairement sujet d' inquiétudes .
Les visiteurs et les journalistes sont arrivés chez [MASK] [MASK] pour le trouver vénérant des bananes , vandalisant le studio , et débitant de longues et violentes diatribes .
Ce qui avait été soigneusement reconstruit fut vandalisé , démonté et détruit par [MASK] .
Il semble qu' après deux ans de confusion , [MASK] [MASK] a retrouvé la forme .
" The [MASK] [MASK] was too black and too dread, " [MASK] expliqua plus tard .
Après avoir passé trois jours en prison , suspecté d' incendie , [MASK] n' a nulle part où aller .
Sa relation fragile avec [MASK] [MASK] se brise lorsqu' il traite le chef d' [MASK] [MASK] [MASK] de vampire et dit qu' il est responsable de la mort de [MASK] [MASK] .
L' opus , plein de surprises , sonne comme un témoignage de la situation de [MASK] : après des années de confusion , " the [MASK] was ready to upset again " .
Fort de deux nouveaux albums , [MASK] est remis sur pieds pour de bon .
Loin de l' effervescence de la scène musicale jamaïcaine , [MASK] [MASK] devint un " family man " heureux .
20 ans après l' apogée du [MASK] [MASK] , [MASK] [MASK] connaît une renaissance avec une nouvelle vague de fans de sa musique .
Les compagnies d' enregistrement ne tardent pas à réagir à l' engouement du public , et une grande variété de collections produites par [MASK] [MASK] sont rééditées , ainsi que de nombreux albums .