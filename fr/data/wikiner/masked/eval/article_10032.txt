Il tient son nom du village de [MASK] ( [MASK] ) d' où il est originaire .
Le vieux-lille est une pâte de maroilles , fabriquée en [MASK] ( et non à [MASK] ) que l' on fait macérer deux fois pendant trois mois dans la saumure .
Il bénéficie du label régional [MASK] par arrêté du 5 novembre 1986 .
Le maroilles est évoqué dans le film [MASK] [MASK] [MASK] [MASK] ( sorti en 2008 ) dans lequel il constitue un des éléments caractéristiques de la " culture [MASK] " .