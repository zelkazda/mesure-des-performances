La ville est baignée par la [MASK] [MASK] [MASK] et plusieurs canaux .
La ville et ses alentours ont appartenu au [MASK] [MASK] [MASK] et ils relèvent de la zone linguistique flamande .
L' histoire de la ville est liée à la mer : à l' origine [MASK] était un village de pêcheur , des siècles plus tard la ville abritait des corsaires dont le célèbre [MASK] [MASK] , héros de la [MASK] [MASK] [MASK] .
De par sa position sur la [MASK] [MASK] [MASK] , la ville a depuis souvent suscité les convoitises , elle fut le théâtre de nombreuses opérations militaires .
Au cours de la [MASK] [MASK] [MASK] elle fut le théâtre de l' [MASK] [MASK] .
Sortie anéantie de cette guerre , la ville doit son salut à l' installation dans son port de l' usine sidérurgique [MASK] qui accéléra sa reconstruction et son développement .
Le drapeau de [MASK] est argent fascé d' azur , c' est-à-dire sur un fond blanc se trouvent trois bandes bleues , la bande la plus basse étant bleue .
En 1662 , alors que [MASK] devient définitivement française , le roi [MASK] [MASK] flatte les corsaires dunkerquois en leur offrant un pavillon particulier afin de favoriser la course sur mer .
Durant le siècle suivant , les [MASK] conservent deux pavillons : le premier bleu et blanc , et le second qui est une croix rouge sur fond blanc .
Le 8 décembre 1817 , le drapeau de [MASK] est remplacé par décret officiel par le pavillon de l' [MASK] [MASK] [MASK] , qui est blanc orné de deux bandes bleues .
De plus [MASK] est à moins de 300 km de quatre autres capitales européennes : [MASK] , [MASK] , [MASK] et [MASK] .
La ville est également sur le [MASK] [MASK] [MASK] , et depuis 2000 sur la [MASK] [MASK] .
[MASK] appartient à la région naturelle du [MASK] , caractérisée par un paysage plat et la présence de nombreux canaux contrôlés .
La superficie de la ville est de 37.34 km 2 , ce qui fait de [MASK] la ville de plus de 20000 habitants la plus étendue du [MASK] , devant notamment [MASK] et [MASK] .
Situation de [MASK] par rapport aux communes adjacentes :
[MASK] est situé sur un promontoire de sable et de terre par rapport aux autres villes du [MASK] , par exemple il y a un dénivelé de 1.80 m entre [MASK] et [MASK] .
Ceci est dû au fait que [MASK] est installée en bord de mer , sur les dunes qui empêchent l' eau de mer de rentrer irrémédiablement dans les terres qui sont situées sous le niveau de la pleine mer .
Le sol dunkerquois empêche l' érection de bâtiments de grande taille par des procédés standards , mais il permet la culture de nombreuses plantes , fruits et légumes , comme en témoigne l' activité maraichère de [MASK] -- étymologiquement la " vallée des roses " -- au cours de son histoire .
Hormis les buttes que se trouvent dans le quartier du " [MASK] [MASK] " et dont la plus orientale est le point culminant de [MASK] avec 17 mètres , quelques pentes dans le quartier de [MASK] à proximité de la plage et les dunes , le paysage dunkerquois est nivelé à environ 4 mètres .
Ainsi le point culminant des quartiers habités de [MASK] est situé en plein centre ville à mi-distance entre l' hôtel de ville et la place [MASK] .
Le point le plus bas de [MASK] est situé au niveau de la mer sur la plage .
[MASK] est baignée par la [MASK] [MASK] [MASK] , tout comme la " digue du [MASK] " protégeant le port industriel .
Il reçoit les eaux du [MASK] [MASK] [MASK] [MASK] .
À son arrivée à [MASK] , le canal se divise en deux embranchements .
La construction du [MASK] [MASK] [MASK] a débuté en 1679 et il a été mis en service en 1846 , sa réalisation est due à la volonté de relier l' [MASK] au [MASK] [MASK] [MASK] .
Le lit du canal suit globalement un axe sud-ouest -- nord-est avant de se diviser en deux parties au niveau de la ville de [MASK] au sud-ouest de [MASK] .
La partie ouest du canal , qui prend la direction du nord , est appelée " dérivation du [MASK] [MASK] [MASK] " .
Elle sépare [MASK] sur sa rive gauche de [MASK] et se jette dans le bassin du [MASK] [MASK] [MASK] [MASK] .
La partie est du canal conserve le nom de [MASK] [MASK] [MASK] , elle suit une direction globalement ouest-est avant de bifurquer vers le nord au niveau de [MASK] .
Le [MASK] [MASK] [MASK] se jette à l' extrémité ouest du " canal de jonction " .
Les canaux de [MASK] et de [MASK] accueillant une partie des eaux de l' [MASK] , [MASK] constitue l' embouchure orientale du fleuve .
De plus le [MASK] [MASK] [MASK] et sa dérivation forment une partie de la [MASK] [MASK] .
Il débute au niveau des " 4 écluses " et prend une direction quasiment parallèle à la côte en direction de la [MASK] , où , sous le nom de [MASK] [MASK] , il se jette dans la [MASK] [MASK] [MASK] .
Le [MASK] [MASK] [MASK] accueille une partie des eaux des canaux de [MASK] et du " canal de jonction " .
Il sépare [MASK] sur sa rive gauche de [MASK] puis [MASK] .
Le premier continuait vers le nord afin de se déverser dans la mer du nord via une cunette , le second venant du sud communiquait vers l' est avec le [MASK] [MASK] [MASK] et le troisième venant de l' ouest communiquait également avec le [MASK] [MASK] [MASK] .
Le canal de [MASK] a été mis en service en 1715 , un an après le début des travaux .
Il était destiné à fournir un accès à la mer supplémentaire aux eaux du canal de jonction tout en desservant [MASK] .
Aujourd'hui , il sépare [MASK] sur sa rive gauche de [MASK] , il est recouvert par la voie express tout le long de cette délimitation , puis serpente sous forme de petits cours d' eau avant de se jeter dans le bassin du port autonome .
Les principaux risques qui pèsent sur [MASK] sont principalement de deux types .
Il y a , entre autres , des raffineries , une aciérie , un usine de fabrication d' aluminium ( [MASK] [MASK] ) et des fabricants de polymères .
La ville de [MASK] est également soumise au risque d' incident nucléaire en raison de la [MASK] [MASK] [MASK] [MASK] qui est située à environ 20 km du centre-ville .
La station météorologique de [MASK] est située au bout de la digue du [MASK] , à côté de l' écluse [MASK] [MASK] [MASK] dans le [MASK] [MASK] [MASK] [MASK] .
Elle fut nommée en l' honneur de [MASK] [MASK] [MASK] .
Aujourd'hui s' y trouvent notamment l' université du littoral et le siège de la [MASK] [MASK] [MASK] [MASK] .
La création d' une gare [MASK] déplacée et d' une gare inter-modale est en projet , tout comme la rénovation du quartier .
On y trouve le lycée [MASK] .
C' est la zone comprise entre la gare et [MASK] .
En 1666 , la parcelle de terre comprise entre les canaux de [MASK] et de [MASK] , est à l' extérieur de la ville et appartient à la ville de [MASK] .
Le terrain abrite les stations des barques qui font les liaisons avec [MASK] et [MASK] .
Le 3 février 1850 , le nord de la parcelle est annexé par [MASK] , car le roi des [MASK] [MASK] [MASK] juge nécessaire l' édification d' une nouvelle enceinte , traversant le terrain , la partie à l' intérieur des murailles devient donc dunkerquoise .
Suite à l' érection des murs , le quartier se retrouve coupé de celle-ci et est alors annexé par [MASK] .
[MASK] devient un port de guerre .
En 1846 , le percement du [MASK] [MASK] [MASK] donne ses limites sud et est à la ville de [MASK] .
Au XIX e siècle , [MASK] profite de l' essor de [MASK] pour se développer : au nord-ouest de la ville , on cultive la terre , le long des canaux s' installent des industries ( tissage , raffinerie , scierie , etc. ) .
Comme toute la région dunkerquoise , [MASK] est très sévèrement touchée lors de la [MASK] [MASK] [MASK] .
L' installation notamment d' [MASK] accélère la reconstruction de la ville et son développement .
En 1972 , la ville fusionne avec [MASK] .
Les [MASK] sont 16700 à vivre à [MASK] , répartis sur une superficie de 11.45 km 2 .
À l' origine [MASK] était bordée de longs bancs de dunes .
À la fin des 60 ' [MASK] et [MASK] , soucieuses de mieux accéder aux ressources nécessaires à leur développement , décident de fusionner .
La superficie du quartier est de 3.76 km 2 et , en 1999 , 16182 [MASK] y vivent .
L' histoire de [MASK] commence lorsque des pêcheurs décident de s' installer à l' est de [MASK] dans les dunes .
Le hameau étant en dehors des fortifications dunkerquoises , il subit de plein fouet les différentes campagnes militaires qui s' abattent sur le dunkerquois , c' est le cas lors de la [MASK] [MASK] [MASK] .
Après la reconstruction , [MASK] devient une paroisse , celle-ci s' étend des glacis des fortifications de [MASK] jusqu' à une taverne appelée " tente verte " à l' ouest , et du [MASK] [MASK] [MASK] à la mer .
Le 24 mars 1860 , un décret consacre [MASK] comme commune autonome occupant globalement le territoire de la paroisse .
Une nouvelle fois , [MASK] est meurtrie lors des bombardements de la [MASK] [MASK] [MASK] .
[MASK] est relativement épargnée par les bombardements de la [MASK] [MASK] [MASK] .
Le 8 octobre 1971 , le conseil municipal de [MASK] propose la fusion des deux villes à celui de [MASK] qui répond favorablement , la ville est ainsi intégrée à [MASK] le 1 er janvier 1972 .
Quatre ans plus tard , [MASK] [MASK] , maire de [MASK] , implante à [MASK] le centre hospitalier de [MASK] .
Aujourd'hui , [MASK] compte 18272 habitants , vivant sur une superficie de 3.97 km 2 .
[MASK] est une commune associée à [MASK] .
L' [MASK] a été achevée en 1972 .
Il s' agissait à l' époque de relier [MASK] au [MASK] [MASK] [MASK] alors en plein essor .
[MASK] est également sur le trajet de plusieurs routes nationales : l' ex- [MASK] relie [MASK] à [MASK] via [MASK] , [MASK] , [MASK] et [MASK] , la [MASK] joint l' [MASK] à l' [MASK] et l' ex- [MASK] relie [MASK] à [MASK] .
Les lignes de bus numéros 2 et 3 de [MASK] [MASK] assurent cependant encore le transport de passagers entre [MASK] et la [MASK] .
Le [MASK] [MASK] , qui est à grand gabarit , relie [MASK] à [MASK] et prochainement au futur [MASK] [MASK] [MASK] .
L' [MASK] [MASK] [MASK] dessert également [MASK] , tous deux sont des aérodromes de tourisme .
La ville de [MASK] et son agglomération ( [MASK] [MASK] [MASK] ) de 18 communes , sont desservis par la société [MASK] [MASK] qui exploite neuf lignes régulières et des lignes de service à la demande .
Toutes les lignes hormis le 9 , passent par la [MASK] [MASK] [MASK] .
Une ligne 10 ( navette plage ) existe également pendant les mois de juillet et août qui fait le trajet de la ligne 3 entre la gare et l' arrêt [MASK] plage .
Il y a plus d' un millénaire , le [MASK] est sous les eaux , l' histoire de [MASK] commence lorsque l' amoncellement du sable dû aux courants marins forme des dunes qui gagnent sur la mer .
Afin d' évangéliser la zone , on construit une chapelle pour les pêcheurs et leurs familles , le bourg a un nom : [MASK] .
Au fil des années , les [MASK] apprennent à maitriser les dunes et les eaux intérieures afin d' éviter les inondations de l' arrière pays .
Les moines de [MASK] construisent de grands fossés appelés watergangs afin d' assécher les terres et les rendre cultivables .
Le 27 mai 1067 , [MASK] [MASK] [MASK] [MASK] reconnaissant , leur attribue l' autel de [MASK] ainsi que la dîme .
Le comte octroie à [MASK] le statut de " ville nouvelle " et exonère les [MASK] de tonlieux , en remerciement de la délivrance par les [MASK] de sa fiancée [MASK] [MASK] [MASK] aux mains des [MASK] .
En 1226 , [MASK] dispose d' un sceau , qui représente un poisson vu de sa droite .
Finalement , les armées flamandes sont vaincues le 20 août 1297 , lors de la [MASK] [MASK] [MASK] , [MASK] devient alors française pour éviter d' être pillée .
[MASK] dispose à cette époque d' un " corps échevinal " , ni plus ni moins que l' ancêtre du conseil municipal , composé d' un mayeur ( un maire ) assisté de neuf échevins ( les conseillers municipaux ) .
[MASK] subit une nouvelle fois les représailles .
Durant la guerre accostent à [MASK] des bateaux transportant des vivres pour les troupes , l' importance du port croit jour après jour .
Cependant l' arrière pays est noyé , les récoltes sont perdues et les bêtes sont mortes , [MASK] a souffert plus que toutes les autres villes de la côte .
Dix ans après , l' église [MASK] l' utilise comme clocher .
En 1477 , à la mort de son père [MASK] [MASK] [MASK] , [MASK] [MASK] [MASK] épouse [MASK] [MASK] [MASK] [MASK] , [MASK] [MASK] envahit aussitôt la [MASK] .
Le magistrat de [MASK] prend la décision d' armer des bateaux en course afin de protéger ses bateaux de pêche .
L' année suivante , suite à la prise par les [MASK] de [MASK] , ville anglaise , [MASK] se prépare à la guerre .
En juillet , en dépit de la défense héroïque de [MASK] par ses habitants , le [MASK] [MASK] [MASK] conquiert [MASK] et y met le feu .
[MASK] , fidèle à son roi , s' y oppose et subit donc les attaques des gueux .
[MASK] suscite de plus en plus les convoitises .
Pour protéger son port , on construit , en 1622 , le fort de [MASK] .
En 1638 est construit le [MASK] [MASK] [MASK] qui facilite le commerce entre [MASK] et le reste de la [MASK] .
En 1646 , la ville après 17 jours de siège devient française , grâce à [MASK] .
Le 16 septembre 1652 , [MASK] est à nouveau espagnole .
Le 25 mai 1658 , le maréchal [MASK] fait le siège de la ville .
Le soir même , [MASK] [MASK] la remet à [MASK] [MASK] .
Le 27 septembre 1662 , [MASK] devient définitivement française .
Le 2 décembre , le [MASK] [MASK] fait une entrée triomphale dans [MASK] .
[MASK] entreprend alors de fortifier la ville et développe son port , qui devient le plus grand port de guerre du royaume .
Dès 1670 , [MASK] [MASK] encourage la course à [MASK] .
C' est à cette époque que [MASK] va connaitre le plus célèbre de ses corsaires : [MASK] [MASK] .
En 1700 apparait à [MASK] la chambre de commerce qui fait prospérer les commerçants , la ville est alors une puissante place commerciale .
Cependant ce n' est pas le cas de toute [MASK] [MASK] .
Les échevins installent ainsi des citernes publiques pour récupérer l' eau de pluie et recrutent une entreprise qui récupère les déchets des [MASK] .
Le 18 juillet , arrive la nouvelle de la [MASK] [MASK] [MASK] [MASK] à [MASK] , se forme alors une garde bourgeoise dirigée par le colonel [MASK] .
Lors de la [MASK] [MASK] [MASK] , la garde bourgeoise de [MASK] perd son étendard emporté par un chevalier royaliste , il adopte alors le drapeau de la [MASK] [MASK] , c' est le premier régiment d' infanterie à avoir un drapeau aux couleurs nationales .
La déclaration de guerre avec l' [MASK] provoque la radicalisation des positions .
Le 21 juillet 1793 , avec la mort du roi [MASK] [MASK] , [MASK] redoute l' [MASK] , la ville est en danger .
Le 23 août 1793 , il arrive à [MASK] , [MASK] est en état de guerre , on a construit des batteries flottantes et renforcé les fortifications , le siège de la ville commence .
Le 8 septembre , le général [MASK] [MASK] [MASK] arrive à [MASK] et libère la ville après la victoire lors de la [MASK] [MASK] [MASK] , [MASK] reste française .
La [MASK] à [MASK] est relativement peu sanglante .
La [MASK] est bien accueillie à [MASK] .
Dix ans plus tard , le chemin de fer relie [MASK] à [MASK] .
Entre temps , le 7 septembre 1845 est inaugurée une statue à la gloire du héros de la ville : [MASK] [MASK] , sur la place qui porte désormais son nom .
[MASK] [MASK] visite la ville deux fois , la seconde il proclame : " Que de progrès , depuis ma première visite , c' est remarquable " , soulignant ainsi les avancés de la ville dotée dorénavant d' un port à flot .
Les morutiers partent en [MASK] et aux [MASK] [MASK] en février et reviennent à l' automne .
En 1868 , la station balnéaire de [MASK] se dote d' un casino , d' un kursaal , d' hôtels , d' une digue , de cabines de plage et de villas .
Quelques années avant 1900 , on fait construire l' hôtel de ville actuel , sur son fronton figure [MASK] [MASK] entouré des [MASK] célèbres .
Le 1 er août 1914 à 18h00 , les cloches du beffroi sonnent le tocsin , le gouvernement du président [MASK] [MASK] a décrété la mobilisation générale car suite à l' [MASK] [MASK] [MASK] et aux jeux des alliances , [MASK] [MASK] est en guerre .
En octobre 1914 , l' armée allemande entreprend de marcher sur [MASK] et [MASK] , c' est le début de la [MASK] [MASK] [MASK] [MASK] dont les alliés belges et français ( notamment les troupes de fusiliers marins de l' [MASK] [MASK] ) sortent vainqueurs .
Les [MASK] restés dans la ville participent à la défense nationale , les chantiers navals lancent le plus gros cargo de la flotte française .
À ce moment de la guerre , 20000 soldats de toutes les nations alliées transitent par [MASK] .
Le 15 avril 1922 est inauguré un cénotaphe au pied du beffroi en l' honneur des [MASK] morts pour [MASK] [MASK] .
En se repliant , elles se trouvèrent finalement encerclées à [MASK] .
Beaucoup d' autres [MASK] furent faits prisonniers .
La ville de [MASK] fut sévèrement bombardée par l' aviation allemande pendant cette bataille .
Au lendemain de la guerre , [MASK] est détruite à plus de 70 % .
La reconstruction du tissu urbain est entreprise par [MASK] [MASK] .
L' architecte [MASK] [MASK] aura la responsabilité de la reconstruction de l' habitat ( îlots rouges ) .
En 1957 , le groupe sidérurgique [MASK] décide l' implantation d' une usine de production d' acier à [MASK] .
[MASK] devient une grande agglomération industrielle .
L' [MASK] [MASK] [MASK] regroupe notamment les villes de [MASK] , [MASK] et [MASK] .
[MASK] est une sous-préfecture depuis 1803 , date à laquelle elle a été déplacée de [MASK] .
L' actuel arrondissement est la réunion de l' [MASK] [MASK] [MASK] et de celui de [MASK] , ce dernier fusionne en 1926 avec l' [MASK] [MASK] [MASK] pour donner l' arrondissement que l' on connait aujourd'hui .
Le territoire administré par [MASK] correspond à l' ancien [MASK] [MASK] .
[MASK] est également le centre de la [MASK] [MASK] [MASK] [MASK] ( [MASK] ) .
La [MASK] regroupe 208705 habitants ( 55 % de la population de l' arrondissement ) repartis sur une surface de 255 km 2 .
La [MASK] a connu seulement deux président depuis sa création , [MASK] [MASK] jusqu' en 1995 puis [MASK] [MASK] jusqu' à aujourd'hui .
À noter que la [MASK] ayant été créée en 1968 , elle n' est pas concernée par la [MASK] [MASK] qui fixe le seuil de population minimal pour une communauté urbaine à 500000 habitants .
[MASK] est divisée en quatre cantons , mais n' est le chef-lieu que de deux d' entre eux :
[MASK] [MASK] étant resté à la tête de la ville presque vingt-quatre ans , record que battra sauf incident [MASK] [MASK] le 30 septembre 2012 .
Les résultats de l' élection à [MASK] diffèrent de ceux du pays , les socialistes résistant mieux au raz-de-marée : la liste de [MASK] arrive effectivement en tête avec 23,79 % ( nat:27,87 % ) , la liste est cependant talonnée par le [MASK] avec 22,21 % ( 16,48 % ) , arrive ensuite [MASK] [MASK] avec 13,73 % ( 16,28 % ) , puis le [MASK] ayant recueilli 9,04 % ( 6,34 % ) , enfin le [MASK] obtient 8,58 % ( 8,45 % ) , les autres ayant moins de 6 % .
La ville a suivi la moyenne nationale en donnant 52,30 % ( 53,06 % ) des voix à [MASK] [MASK] contre 47,70 % ( 46,94 % ) pour [MASK] [MASK] , l' abstention étant de 16,73 % ( 16,03 % ) .
Dans le [MASK] , sur les 2133125 foyers fiscaux , 988830 le payaient soit 46,5 % .
On peut donc en conclure que le nombre de [MASK] imposables est dans les mêmes proportions qu' au niveau de la région .
Les taux d' imposition de [MASK] varient peu au cours des années , notamment en raison du fait que cela pourrait créer un déséquilibre au niveau de la communauté urbaine .
La [MASK] décide d' ailleurs du taux de la taxe professionnelle qui est le même pour toutes les villes qui en sont membres .
[MASK] dispose d' un palais de justice , sa construction a débuté en 1858 et il a été inauguré en 1864 .
Le taux de violence pour [MASK] est de 3,88 pour 1000 habitants .
La collecte des déchets à [MASK] est confiée à la [MASK] [MASK] [MASK] [MASK] .
En 2000 , la [MASK] installe des poubelles vertes destinées aux déchets fermentescibles ( déchets de jardins et d' alimentation ) .
L' évolution du nombre d' habitants depuis 1793 est connue à travers les recensements de la population effectués à [MASK] depuis cette date :
Sur la courbe de démographie , on peut constater que [MASK] a payé un lourd tribut lors de la [MASK] [MASK] [MASK] ( 1939-1945 ) divisant le nombre de [MASK] par deux , outre les départs des habitants désireux de se réfugier plus au sud , ce nombre est lié aux nombreux morts lors de l' [MASK] [MASK] où la ville fut bombardée par la [MASK] , puis dans les années qui suivirent par les avions de la [MASK] [MASK] [MASK] .
Le premier creux correspond à la [MASK] , on note cependant que la [MASK] fut relativement peu sanglante .
La [MASK] [MASK] [MASK] fut beaucoup moins mortelle pour la population dunkerquoise , malgré les bombardements de la ville par l' armée allemande .
Durant l' entre-deux-guerres , le nombre de [MASK] continue de baisser , la ville étant en partie détruite , la population a peine à y habiter .
La première grande augmentation de l' après [MASK] [MASK] [MASK] , du sortir de la guerre jusqu' au milieu des années 1960 , est due au phénomène du baby boom .
Il convient d' ajouter également le nombre d' habitants de retour à [MASK] après la guerre .
Le nombre de [MASK] triple presque passant de 10000 à 27000 .
Enfin la seconde augmentation , qui est de loin la plus importante puisque la ville passe de 27500 âmes à 74000 ( soit 2,5 fois plus ) , est due à l' absorption de plusieurs communes voisines : [MASK] en 1970 , puis [MASK] et [MASK] en 1972 ( respectivement 15223 , 12044 et 19591 habitants en 1968 , soit près de 47000 au total ) , villes ayant profité de l' installation d' [MASK] pour se développer .
L' économie de [MASK] profite de la position de la ville .
Elle est en effet située à moins de 300 kilomètres de cinq capitales : [MASK] , [MASK] , [MASK] , [MASK] et [MASK] .
Le [MASK] [MASK] [MASK] présente un des revirements de tendance les plus remarqués du monde portuaire français .
De longue date port industriel , [MASK] cherche aujourd'hui à cultiver sa place dans les réseaux logistiques internationaux et européens .
Actuellement , [MASK] est le :
[MASK] est le siège et , avec [MASK] , [MASK] et [MASK] , l' un des sites de l' [MASK] [MASK] [MASK] [MASK] [MASK] regroupant plus de 11000 étudiants .
Le centre hospitalier de [MASK] a été créé dans les années 1970 sous le mandat de [MASK] [MASK] à [MASK] .
Ces relevés indiquent que la qualité de l' air à [MASK] est globalement bonne , cependant ils ne prennent pas en compte toutes les émissions polluantes ( le nickel par exemple ) .
La ville dépend de l' [MASK] [MASK] [MASK] et comprend plusieurs paroisses pour ce qui est du catholicisme .
Enfin [MASK] compte les églises [MASK] , [MASK] et [MASK] .
[MASK] comporte de nombreux espaces verts et lieux de promenade .
L ' église [MASK] , église-halle ( hallekerke ) à cinq nefs .
Lieu de culte catholique situé au centre ville de [MASK] .
L' église fut ravagée par les bombardements de la [MASK] [MASK] [MASK] , qui laissèrent debout la façade et les murs porteurs .
Comme de nombreuses églises de [MASK] , ce monument ne possède pas de clocher .
À l' origine , l' église [MASK] était accolée au beffroi qui constituait le clocher ou le campanile de l' édifice .
À l' intérieur les ex-voto et autres inscriptions funéraires permettent de reconstituer la nationalité des maîtres de [MASK] .
Il est à l' origine rattaché à l' église [MASK] et lui sert de clocher .
Le vitrail en haut de la première volée de marches intérieure représente [MASK] [MASK] parmi son équipage sur le port au retour de la [MASK] [MASK] [MASK] .
[MASK] [MASK] meurt le 27 avril 1702 à environ 15h30 , à la veille de partir en course .
Un siècle et demi plus tard , les 7 et 8 septembre 1845 , [MASK] rend hommage à son héros en inaugurant une statue à son image créée par [MASK] [MASK] [MASK] , représentant le corsaire épée à la main , défiant l' ennemi .
Enfin , au cours de l' année 1983 , la municipalité fait placé autour de la statue , les armes de la ville de [MASK] et le nom des 16 navires que commanda [MASK] [MASK] .
Aux XVII e et XVIII e siècles , les armateurs offraient à leurs marins un festin , une fête à la veille de leurs départs pour les mers périlleuses d' [MASK] , où ils y péchaient la morue .
Le dunkerquois est un dialecte très différent du ch'ti , qui se parle à [MASK] et dans son agglomération ( y compris à [MASK] ) .
À l' image des autres accents , il passe inaperçu à [MASK] , mais est caractéristique dans d' autres régions .
On raconte qu' un guerrier scandinave nommé [MASK] , voulut conquérir [MASK] .
[MASK] s' installe alors dans la cité , il se marie et ce met au service de la ville .
À sa mort , la ville pleure [MASK] et décide de lui rendre hommage en l' immortalisant au travers d' un géant .
Le géant de [MASK] a été créé en 2006 .
C' est un des navires réquisitionné pendant l' [MASK] [MASK] .
Ce " quartier général " est un centre de convergence de l' art contemporain du [MASK] .
Le [MASK] [MASK] [MASK] [MASK] est destiné à éclairer les visiteurs sur les métiers de la mer et sur l' histoire du [MASK] [MASK] [MASK] .
La ville de [MASK] a donné son nom à une des chansons du groupe [MASK] , dans l' album [MASK] .
Unités militaires ayant été en garnison à [MASK] :
[MASK] [MASK] et [MASK] [MASK] ( dir .