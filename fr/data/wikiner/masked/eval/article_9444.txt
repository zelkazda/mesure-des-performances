Sa musique est , de son propre aveu , profondément ancrée dans la pop , mais [MASK] a su en repousser les limites .
[MASK] grandit en [MASK] aux côtés de sa mère , dans une communauté hippie .
[MASK] enregistre son premier album à l' âge de 11 ans , durant l' été 1977 .
L' adolescence de [MASK] est marquée par son appartenance à un grand nombre de groupes locaux .
En 1983 , [MASK] crée avec quelques amis le groupe punk [MASK] , le premier dont les chansons vont traverser les frontières islandaises .
Bien que le succès soit très limité et cantonné au [MASK] , c' est là un premier pas vers une carrière internationale .
En 1987 , les membres de [MASK] sabordent le groupe et fondent [MASK] [MASK] , qui deviendra vite le groupe islandais le plus célèbre au monde ( à l' époque ) .
Ayant produit trois albums ( sans compter un album de remixes et une anthologie ) , [MASK] [MASK] connaît un succès incroyable ( même si ce succès se centre assez vite sur la personnalité de sa chanteuse , [MASK] ) et part en tournée dans le monde entier .
Parallèlement , [MASK] commence à vivre ses premières expériences de collaboration extérieure , enregistrant deux chansons pour [MASK] [MASK] , un groupe de house britannique .
Elle sort également [MASK] , un album de jazz composé principalement de reprises en islandais de standards du genre .
Elle travaille en collaboration avec [MASK] [MASK] , producteur de [MASK] [MASK] .
[MASK] obtient des prix dès ce premier album , comme le [MASK] [MASK] de la meilleure nouvelle artiste internationale féminine .
[MASK] contient plusieurs titres qu' elle avait écrits durant son adolescence .
Après la parution de l' album , [MASK] entame une nouvelle tournée mondiale , lançant ainsi un rythme de travail qu' elle n' a jamais abandonné jusqu' à ce jour : un album , une tournée .
Tout comme [MASK] , [MASK] est un patchwork de musique nouvelle et de textes écrits longtemps auparavant dans sa carrière .
Si [MASK] montrait une jeune fille chétive et timide , si [MASK] montrait une adolescente excentrique , [MASK] montre une femme mature , maturité évoquée par la droiture de la posture prise par [MASK] pour la couverture de l' album ; mais , dans cette image , il reste quelque chose d' inhumain , le thème de l' hybridation persiste .
[MASK] est alors considéré comme l' album le plus expérimental de la chanteuse .
On y retrouve la dualité sentimentale , entre amour et colère , qui existait déjà dans [MASK] .
[MASK] dit que pour [MASK] , elle n' a gardé que les trois bruits essentiels , les trois bruits qui existent depuis la nuit des temps -- les plus forts : la respiration , le cœur qui bat et les nerfs qui frémissent [ réf. nécessaire ] .
2000 est une année particulière pour [MASK] .
[MASK] [MASK] [MASK] [MASK] , le film de [MASK] [MASK] [MASK] , sort sur les écrans .
Elle y incarne le personnage principal et en compose la musique , que l' on peut retrouver en partie remaniée dans l' album [MASK] .
L' actrice et le film sont récompensés au [MASK] [MASK] [MASK] .
C' est en 2001 que sort l' album [MASK] , album dans lequel [MASK] crée un univers intimiste et introverti fait de beats légers et fugaces .
C' est après l' expérience de [MASK] [MASK] [MASK] [MASK] , que la chanteuse dit avoir très mal supporté , qu' elle effectue un retour en [MASK] , un retour aux origines , qui est un des thèmes de l' album .
L' image médiatique qu' elle s' était créée continue donc sa maturation commencée avec [MASK] , mais maintenant beaucoup plus sombre .
La même année , [MASK] sort un livre , constitué de photos , de dessins et de quelques textes , laissant la place à plusieurs de ses collaborateurs artistiques , notamment les réalisateurs de ses clips .
En 2004 [MASK] enregistre [MASK] , un album entièrement consacré à la voix humaine , pour lequel elle s' entoure de plusieurs chanteurs et collaborateurs venus du monde entier .
En dehors de quelques performances , aucune tournée ne fut organisée pour [MASK] .
[MASK] déclara que c' était pour continuer immédiatement l' écriture de l' album suivant .
En juin 2004 elle déclara dans une interview pour le magazine [MASK] [MASK] : " À chaque album que j' ai fait , au moment où je le terminais , j' étais encore très inspirée , et je pensais pouvoir réaliser encore un album en cinq minutes ; alors , je voulais juste savoir si c' était seulement une fantaisie ou si c' était vrai " .
En juillet-août 2005 , sort [MASK] [MASK] [MASK] , bande originale que [MASK] a composée pour le film éponyme .
Dans ce film , elle joue l' un des rôles principaux , au côté de son mari , qui en est aussi le réalisateur , [MASK] [MASK] .
[MASK] [MASK] [MASK] est un film réalisé à l' occasion du 60 e anniversaire commémorant les [MASK] [MASK] [MASK] [MASK] [MASK] [MASK] .
Le thème du film étant le [MASK] , elle y utilise des instruments traditionnels japonais .
Ces deux éléments , qui jusque là n' étaient utilisés que de manière fugace dans ses compositions , prendront une place importante dans l' album suivant , [MASK] .
Le chœur devait se produire lors de deux tournées , accompagné de [MASK] , lors du millénium en [MASK] et durant l' été à travers toute l' [MASK] .
Malheureusement , [MASK] se ravise pour la seconde partie du projet ; certaines villes auraient toutefois abusé de l' image de la chanteuse pour promouvoir les concerts donnés par le chœur .
Le 17 novembre 2006 , le premier groupe a succès mondial de la chanteuse , les [MASK] , se reforme pour un concert d' un soir , à [MASK] .
Selon les dires de [MASK] dans la presse après ce concert , elle a choisi de ne pas profiter des gains amassés , dans le but d' aider la scène musicale islandaise à émerger .
En 2006 , ses collaborations avec [MASK] et [MASK] [MASK] sont enregistrées entre l' [MASK] et la [MASK] .
Son dernier album , [MASK] , est sorti en mai 2007 .
Le [MASK] ! "
[MASK] est surtout teinté de cuivres ( cor , tuba , trompette , trombone ... ) et de percussions .
La première chose qui frappe en écoutant [MASK] , outre sa voix puissante , c' est cette façon très étrange de scander les syllabes , particularité qui semble provenir directement de la tradition musicale médiévale islandaise , les fameux " rímur " , chorales exclusivement masculines .
Le principe que retient [MASK] est en effet la juxtaposition de références d' origines fortement éloignées .
la ligne mélodique de la voix sur le passage " to enjoy " est caractéristiquement inspirée des lignes de [MASK] [MASK] ou de [MASK] [MASK] .
[MASK] aime inviter des compositeurs et producteurs provenant des tendances les plus diverses sur ses albums ( à l' exception de [MASK] et , dans une moindre mesure , de [MASK] , qui se veulent profondément inspirés par ses racines islandaises ) .
Cette collaboration se poursuit d' ailleurs au-delà des albums : pour presque chacune de ses chansons , [MASK] engage une horde hétéroclite de remixeurs pour les réinterpréter .
Cette philosophie du remixage , à mille lieues des traditionnels " boum boum " prévisibles des remixes pour discothèques , trouve son point culminant dans [MASK] , disque entièrement voué à cette forme musicale .
Enfin , [MASK] considère le clip vidéo comme un prolongement à part entière de son œuvre et s' y implique pleinement .
[MASK] poursuit ses recherches artistiques , en dehors de la musique .
Elle débute sa carrière au cinéma en 1986 dans le rôle principal du film [MASK] [MASK] [MASK] .
En photographie , [MASK] pose pour les photographes les plus en vogue , tels que [MASK] [MASK] .
Enfin , son goût pour la mode d' avant-garde lui vaut parfois les moqueries des commentateurs , mais lui fait rencontrer une foule de stylistes parmi lesquels [MASK] [MASK] , qui a signé la pochette d ' [MASK] .
Pour ses vidéo-clips , elle fait appel à des réalisateurs plus ou moins renommés , comme [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] , [MASK] [MASK] ou [MASK] [MASK] .
( [MASK] , [MASK] , lève ton drapeau ! ) .
En 2005 , elle participe au [MASK] [MASK] , série de concerts organisée pour faire pression sur les dirigeants du [MASK] pour qu' ils effacent la dette publique des pays les plus pauvres .
[MASK] [MASK] [MASK] , [MASK] [MASK] et [MASK] jouaient au même concert .
Entre-temps , elle a multiplié les relations , plus ou moins médiatisées , dont [MASK] et [MASK] [MASK] .