Manéthon l' appelle Ramsès .
Il lutte contre les Hittites et , assurant la domination de l' Égypte sur la Nubie et ses gisements aurifères , il y construit une série de temples dont les plus célèbres sont ceux d' Abou Simbel .
L' ensemble de ces données issues de l' archéologie ou bien de la mémoire collective fait de Ramsès II l' un des pharaons les plus connus à travers le monde .
Il est le fils de Séthi I er et de la reine Mouttouya .
Son règne de 66 ans est exceptionnellement long et marque la dernière grande période de prospérité de l' Égypte antique .
puis six de ses filles : puis une princesse babylonienne , une princesse syrienne et deux princesses hittites , filles de l' empereur Hattousili III ( -1264 / -1234 ) , dont Maâthornéferourê .
Ramsès succède à son père Séthi I er apparemment sans problèmes particuliers .
Face à cette situation Ramsès met sur pied une puissante armée , et établit son camp de base à Pi-Ramsès qu' il transforme en capitale de son empire .
Les hittites de leur côté avaient rassemblé une puissante armée de coalisés et s' étaient rassemblés dans la plaine de Qadesh , y installant leur camp et attendant l' arrivée de l' ennemi .
Ils envoyèrent des éclaireurs qui furent interceptés par les égyptiens et ramenés au camp de Ramsès .
Ils informèrent le roi que les troupes de Mouwatalli se trouvaient au nord et n' osaient s' avancer vers Qadesh par crainte d' une confrontation avec les troupes égyptiennes .
En tête de ses troupes Ramsès avec la division d' Amon traverse l' Oronte et il est le premier à arriver sur le site .
La ruse hittite a fonctionné et l' armée de Ramsès offre dangereusement l' occasion que Mouwatalli et ses généraux attendaient d' anéantir à jamais les velléités de conquête des égyptiens .
Les troupes égyptiennes sont coupées en deux par la charge de l' armée hittite et Ramsès se retrouve seul face au danger .
La division de Rê qui franchissait le fleuve est taillée en pièce par les chars hittites qui se retournent vers la division d' Amon et le camp de Ramsès , à peine installés au pied de la citadelle , déjà attaqués de leur côté par les fantassins de Mouwatalli .
Ramsès et sa garde rapprochée se jette dans la mêlée et il adresse aux divisions de Ptah et de Seth restée en arrière des messages urgents , leur intimant l' ordre d' entrer dans la bataille .
Cependant , au contraire de son père et de son illustre ancêtre Thoutmôsis III , Ramsès , dont les troupes sont affaiblies au lendemain de la bataille , ne s' empare pas de la citadelle et Qadesh reste aux mains des Hittites .
Ramsès ne pousse d' ailleurs pas plus loin cet avantage annoncé , et préfère renforcer ses positions .
Cependant la situation ne semble pas à l' avantage des hittites qui ne cherchent pas à engager un nouveau conflit direct avec Ramsès .
En effet , la bataille de Qadesh avait momentanément porté un sérieux coup à la puissante armée égyptienne et en tout cas au crédit de pharaon sur la région .
Ces troubles permettaient en tout cas d' éloigner les ambitions de Ramsès des terres hittite .
La réaction de Ramsès est aussi rapide que décisive à l' encontre des insurgés .
Une fois assuré de ses arrières et de son ravitaillement Ramsès peut alors reprendre la route de la Syrie afin de reprendre les territoires perdus et abandonnés aux hittites suite à la bataille de Qadesh .
Grâce a cette tactique de sièges successifs et de mise en coupe réglée des terres conquises , Ramsès a repris le contrôle de la situation au plus proche de ses frontières ainsi que sur toute la zone d' influence égyptienne en orient .
Cette fois encore Ramsès cherche à pousser son avantage et à conquérir du terrain .
Il semble que Mouwatalli n' ait pas eu la capacité de contrer cette avancée sur son territoire même si de nombreuses troupes avaient été mises en garnison dans et autour de la citadelle .
Cette prise représente pour Ramsès une revanche sur la semi-défaite de Qadesh .
L' année suivante , pour consolider ses positions il organise une nouvelle campagne qui voit les troupes égyptiennes défiler dans les principales cités de la région prenant au passage Acre .
En échange , pharaon fait parvenir au roi hittite la version égyptienne marquée du sceau de Ramsès .
À ce ballet épistolaire participent non seulement les souverains mais aussi les reines et les ministres , tel le vizir Paser .
C' est alors qu' est évoqué un possible mariage entre Ramsès II et une fille du roi Hattousili III , acte diplomatique venant sceller définitivement la nouvelle alliance des deux anciens ennemis .
Cette pratique est courante et Ramsès a déjà épousé une princesse babylonienne .
Ce problème réglé , des envoyés égyptiens viennent à Hattousa , la capitale hittite pour procéder à l' onction de la princesse , acte qui officialise l' union .
Elle rencontre Ramsès II à Pi-Ramsès et semble-t-il plaît à son mari .
Elle est renommée d' un nom égyptien , Maât-Hor-Néférou-Rê .
Nous ignorons si elle eut la moindre influence sur la politique conduite par son mari ; cependant Ramsès fait construire pour elle un palais à Pi-Ramsès .
Ramsès II épouse une seconde princesse hittite des années plus tard mais nous ignorons pratiquement tout du contexte qui préside à cette nouvelle union .
Ramsès II fut aussi un grand théologien , reprenant à son compte l' initiative solaire amorcée par Akhénaton , mais en se préservant des cultes traditionnels .
En effet , plutôt que d' effacer leur culte comme le fit à son péril Akhénaton , il les affirma dans leur rôle central dans la vie économique et spirituelle du pays , et instaura le sien propre , de son vivant , s' associant ainsi encore davantage que ses ancêtres aux dieux dynastiques et tout particulièrement au dieu Rê .
L' exemple des temples de Nubie est parlant à ce sujet .
En retour , il donna des gages de sa bonne foi aux prêtres de Karnak en effaçant le souvenir de celui qui voulut leur perte , ainsi que de sa descendance .
Il construit son temple funéraire , le Ramesséum , en face de Louxor , qui comprend deux pylônes précédant deux cours à portiques et une grande salle hypostyle .
Il fait également édifier un temple cénotaphe à Abydos non loin de celui de son père qu' il achève de décorer .
C' est pour l' occasion de ces jubilés qu' il fit bâtir un grand parvis à Pi-Ramsès qui comportait au moins six obélisques de grande taille .
Ramsès II fit ériger des colosses à son effigie dans les grands temples construits ou restaurés .
Les plus célèbres sont ceux en façade des temples d' Abou Simbel , ceux qui encadrent l' entrée du pylône du temple de Louxor , le colosse couché à Memphis , ainsi que celui qui trônait depuis quelques décennies en plein centre du Caire , sur la place qui porte son nom devant la gare centrale et qui provient également du grand temple de Ptah .
Ramsès II eut une fin de règne endeuillée par la disparition successive de ses héritiers et de sa grande épouse royale Néfertari .
La dépouille ( momifiée ) de Ramsès II se trouve au musée égyptien du Caire et dut être " soignée " dans les années 1970 car des champignons s' y étaient développés au contact de l' air moderne .
L' étude de cette dépouille au musée de l' Homme à Paris en 1976-77 a révélé que Ramsès était " leucoderme , de type méditerranéen proche de celui des Amazighes africains " .
L' identification à Moïse peut sembler assez évidente .
Cette dernière ville apparaît ensuite comme le point de départ de l' Exode .
Or , Ramsès II est un grand bâtisseur et qu' il entreprend au cours de son règne la construction d' une nouvelle capitale , Pi-Ramsès , non loin d' Avaris , l' ancienne capitale des Hyksôs , les pharaons d' origine sémitique .
Tout d' abord parce que la momie de Ramsès , mort nonagénaire , ne présente aucune trace de noyade et qu' il paraît pour le moins hasardeux de prendre le texte biblique au sens littéral .
Mais ce dernier est cité par Ezéchiel et Jérémie ( ainsi qu' Hérodote ) comme d' une ville située dans le delta du Nil où séjournent de nombreux juifs après la destruction de Jérusalem par Nabuchodonosor II en -587 .
Par conséquent , l' identification de l' adversaire de Moïse à Ramsès II est une hypothèse que rien d' historiquement sérieux ne vient confirmer .
Sans doute faut-il y voir le désir conscient ou non de donner à Moïse un adversaire de sa stature créant de fait un mythe historique .
La tombe de Ramsès II a été ravagée par le temps .
En face de la tombe de Ramsès II , une grande tombe collective a été retrouvée dans la vallée des rois , la KV5 , qui comprend de multiples chapelles et tombeaux des enfants royaux .
En 1974 pour connaître les causes de la mort de Ramsès II et d' autres momies , dont celle Mérenptah , des recherchent furent entreprises sous la direction de Maurice Bucaille avec des collaborateurs égyptiens puis une dizaine d' autres collaborateurs français de disciplines médicales diverses .
Une trouvaille de grande importance grâce à l' utilisation de films radiologiques de très haute sensibilité permis de mettre en évidence l' existence d' une très grave lésion de la mâchoire de Ramsès II , une ostéite étendue du maxillaire inférieure d' origine dentaire .
Dans le film Les Dix Commandements ( 1956 ) , le personnage de Ramsès fut interprété par Yul Brynner .
Ramsès II apparait aussi dans le dessin animé Le Prince d' Égypte ( 1998 ) qui traite également de la vie de Moïse .
