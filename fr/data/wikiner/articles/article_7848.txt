Luiz Eduardo de Oliveira voit le jour du 13 décembre 1944 à Rio de Janeiro au Brésil , est passionné de dessin , ce qui lui fait entrer à l' université et suit des études d' ingénieur .
En 1971 , il fuit le Brésil à cause de la dictature militaire .
Il rejoint finalement le Brésil en 1974 , de manière clandestine .
Dessinateur de publicité à São Paulo , il émigre en France en 1981 pour faire de la bande dessinée .
Il publie toutefois quelques planches dans le magazine L' Écho des savanes en 1982 et Pilote en 1985 .
En 1986 , il entame grâce à Jean-Claude Forest une collaboration fructueuse avec plusieurs publications pour la jeunesse du groupe Bayard Presse .
Il dessine des histoires vraies pour le périodique Okapi .
Pour Astrapi , il réalisera son premier album , Gandhi , le pèlerin de la paix , qui retrace la vie du Mahatma .
En 1988 , le scénariste Rodolphe propose à Léo de dessiner ses histoires , ce qu' il accepte .
Le dessin de Léo consiste en des traits simples .
