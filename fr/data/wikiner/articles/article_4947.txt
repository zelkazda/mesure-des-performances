Anat est une déesse des panthéons ouest-sémitiques de l' âge du bronze récent .
Dans les panthéons ouest-sémitiques , Anat est une déesse de premier plan .
Anat joue un rôle important dans un texte mythologique d' Ougarit , le Cycle de Baal .
Elle domine les animaux sauvages , veille sur les chars de guerre et sur les chevaux durant les batailles , et , dans cette fonction , on en fit l' épouse de Seth .
Elle était considérée comme une fille de Rê ou de Ptah , et s' habillait en soldat mais c' était aussi une déesse vache .
Elle se confondait parfois avec Astarté .
Un mythe nous rapporte que Seth battit un troupeau de soixante-dix-sept âne qu' il égorgea mais , étant gravement blessé et Anat mit ferma sa blessure après avoir remplit sept bassines de son sang qu' elle offrit à Rê pour revivifier son époux .
Elle recevait un culte à Tanis dans le delta , et y possédait même un temple personnel .
Plusieurs haches de ce type , faites d' or , ont été découvertes dans le temple des obélisques de Byblos .
