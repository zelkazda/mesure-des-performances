Le village est situé sur une presqu'ile qui s' étire dans la baie de Lannion .
La vue de la pointe de Locquirec offre une vue splendide sur toute la partie ouest de la Côte de granite rose , de l' Île Milliau à Plestin-les-Grèves , en passant par Trébeurden , Saint-Michel-en-Grève et Tréduder .
La baie de Locquirec est formée par l' embouchure de la rivière le Douron , qui sépare les deux départements .
