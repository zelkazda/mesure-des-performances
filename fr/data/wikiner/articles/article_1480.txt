Les Grecs anciens avaient découvert qu' en frottant l' ambre jaune , il produisait une attirance sur d' autres objets et , parfois des étincelles .
Mais c' est Coulomb qui en énonça les premières lois physiques .
En 1799 , Alessandro Volta inventa la pile électrique , et en 1868 le Belge Zénobe Gramme réalisa la première dynamo .
En 1879 , Thomas Edison présenta sa première ampoule électrique à incandescence .
En 1889 , une ligne de 14 km fut construite dans la Creuse , entre la Cascade des Jarrauds , lieu de production , et la ville de Bourganeuf .
A titre d' exemple , en France , depuis la fin des années 1990 , l' industrie consomme moins d' un tiers de l' électricité finale .
Par ailleurs , la France qui a justifié son programme nucléaire par le soucis de ne plus dépendre du pétrole détenait en 2009 le record de consommation par habitant d' électricité ( un français moyen consomme plus d' électricité qu' un californien moyen rappelle , mais aussi paradoxalement de consommation de pétrole par habitant , avec un fort endettement et une précarité énergétique des plus pauvres .
Il diffuse , notamment en Bretagne ou Provence-Alpes-Côte d' Azur , des incitations à économiser l' électricité .
Il existe en France trois normalisations en électricité :
La normalisation en France est réglementée par la loi du 24 mai 1941 qui a créé l' Association française de normalisation ( AFNOR ) et définit la procédure d' homologation des normes .
