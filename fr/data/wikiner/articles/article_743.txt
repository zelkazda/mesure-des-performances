Il joue là un rôle majeur dans le quotidien des Égyptiens , préservant le peuple de la famine .
Khnoum était particulièrement adoré à Éléphantine et Esna .
Un temple lui était également dédié sur l' île de Philaé .
On le retrouve dans une dizaine de villes d' Égypte sous des formes variées .
Khnoum est un dieu très ancien qui est paradoxalement surtout connu grâce aux textes assez récents gravés sur les parois du temple d' Esna .
