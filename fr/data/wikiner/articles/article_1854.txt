Les calendes désignaient chez les Romains le premier jour du mois qui était le début de la nouvelle lune et les jours étaient comptés à l' envers à partir des calendes , des nones , cinquième ou septième jour du mois , des ides , treizième ou quinzième jour du mois .
Les phases de la Lune étant simples à observer , elles ont fourni un moyen commode de mesure du temps .
Il doit cependant être régulièrement synchronisé sur le cycle de la Lune car l' année et le mois ainsi définis sont tous deux légèrement excédentaires .
L' ère chrétienne démarre l' année supposée de la naissance de Jésus-Christ .
L' ère de Rome commence avec la fondation de Rome , le 21 avril -753 .
À Rome , il représentait l' espace de temps séparant deux recensements .
Ces ajustements , annoncés plusieurs mois à l' avance par l' International Earth Rotation and Reference Systems Service ( IERS ) , ont lieu à des dates prédéfinies en fin de mois , en ajoutant ou retirant une seconde à la dernière minute d' une journée .
