La Liberté éclairant le monde ( Liberty Enlightening the World ) , plus connue sous le nom de statue de la Liberté , est l' un des monuments les plus célèbres de la ville de New York .
Elle est située sur l' île de Liberty Island au sud de Manhattan , à l' embouchure de l' Hudson et à proximité d' Ellis Island ( États-Unis ) .
Elle fut offerte par la France en 1886 , pour célébrer le centenaire de la déclaration d' indépendance américaine et en signe d' amitié entre les deux nations .
Le projet est confié en 1871 , au sculpteur français né à Colmar Frédéric Auguste Bartholdi .
Le choix des cuivres devant être employés à la construction fut confié à l' architecte Eugène Viollet-le-Duc , qui eut l' idée d' employer la technique du repoussé .
En 1879 , à la mort d' Eugène Viollet-le-Duc , Frédéric Auguste Bartholdi fit appel à l' ingénieur Gustave Eiffel pour décider de la structure interne de la statue .
La statue de la Liberté , en plus d' être un monument très important de la ville de New York , est devenue l' un des symboles des États-Unis et représente de manière plus générale la liberté et l' émancipation vis-à-vis de l' oppression .
Au plan de l' architecture , la statue rappelle le Colosse de Rhodes qui était l' une des sept merveilles du monde antique .
Bartholdi aurait confié à ce dernier :
En 1870 , Bartholdi sculpta une première ébauche en terre cuite et en modèle réduit aujourd'hui exposée au musée des Beaux-Arts de Lyon .
L' idée d' offrir une représentation de la liberté à une république sœur située de l' autre côté de l' Atlantique joua alors un rôle important dans la lutte pour le maintien de la république [ réf. nécessaire ] .
Il rencontra le président américain Ulysses S. Grant le 18 juillet 1871 à New York .
Parmi les modèles proposés , on trouve Isabella Eugenie Boyer , veuve du milliardaire du monde de la couture , Isaac Singer .
Le National Geographic Magazine appuie cette hypothèse , en précisant que le sculpteur n' a jamais expliqué ni démenti cette ressemblance avec sa mère .
D' autres modèles ont été avancés sans faire l' unanimité : Bartholdi aurait voulu reproduire le visage d' une jeune fille juchée sur une barricade et tenant une torche , au lendemain du coup d' État de Louis-Napoléon Bonaparte .
Il a peut-être réalisé une synthèse de plusieurs visages féminins , afin de donner une image neutre et impersonnelle de la Liberté .
Lors d' une visite en Égypte , Auguste Bartholdi fut inspiré par le projet du canal de Suez dont la construction allait être entamée sous la direction de l' entrepreneur et diplomate français Ferdinand de Lesseps , qui devint par la suite l' un de ses plus grands amis .
Ces représentations ont également pu être inspirées par la statue de la Liberté .
La position du colosse , les jambes écartées autour de l' entrée , étant différente de celle de la Liberté .
Cependant , des problèmes financiers survinrent des deux côtés de l' Atlantique .
En France , la campagne de promotion pour la statue débuta à l' automne 1875 .
Plusieurs villes françaises , des conseils généraux , des chambres de commerce , le Grand Orient de France mais aussi des milliers de particuliers firent des dons .
Ce n' est qu' en 1880 que la totalité du financement fut assurée en France .
Parallèlement , aux États-Unis , des spectacles de théâtre , des expositions d' art , des ventes aux enchères ainsi que des combats de boxe professionnels furent organisés pour recueillir de l' argent .
Pendant ce temps , en France , Bartholdi avait besoin d' un ingénieur pour se charger de la structure interne d' une telle statue en cuivre .
C' est Gustave Eiffel qui fut engagé pour réaliser le pylône métallique massif qui soutient la statue , ainsi que le squelette secondaire interne qui permet à la " peau " en cuivre de la statue de tenir d' elle-même en position verticale .
Bartholdi avait espéré que la statue serait terminée et assemblée pour le 4 juillet 1876 , date précise du centenaire de l' indépendance , mais un départ différé puis quelques soucis durant la période de construction retardèrent les travaux : le plâtre de la main se brisa en mars 1876 .
Cette dernière fut tout de même exposée en septembre 1876 à la Centennial Exposition ( exposition du centenaire ) de Philadelphie .
Deux années plus tard , en juin 1878 , la tête de la statue fut révélée au public dans les jardins du Champ de Mars à l' occasion de l' exposition universelle de Paris de 1878 : les visiteurs pouvaient pénétrer dans la tête jusqu' au diadème au moyen d' un escalier de 43 mètres .
Il est en outre précisé que la statue est interdite de reproduction " de toute manière connue en art glyphique sous forme de statue ou statuette , ou en haut-relief ou bas-relief , en métal , pierre , terre cuite , plâtre de Paris ou autre composition plastique . "
La statue est située sur l' île de Liberty Island , dans le port de New York .
Dans son esprit , elle y était déjà construite et tournée vers son continent d' origine , l' Europe dont elle accueillait et allait continuer d' accueillir les immigrants .
La collecte des fonds nécessaires à la réalisation de l' ouvrage fut placée sous la responsabilité du procureur général , William M. Evarts .
Mais comme elle avançait très lentement , Joseph Pulitzer accepta de mettre à la disposition des responsables de la construction les premières pages du New York World afin de récolter de l' argent .
Les fonds nécessaires à la construction du socle imaginé par l' architecte américain Richard Morris Hunt et réalisé par l' ingénieur Charles Pomeroy Stone , furent toutefois rassemblés en août 1884 .
La première pierre du piédestal fut posée le 5 août 1884 , et le socle , majoritairement composé de pierre de Kersanton provenant des carrières de Loperhet et de Logonna-Daoulas , fut construit entre le 9 octobre 1883 et le 22 août 1886 .
Au cœur du bloc qui compose le socle , deux séries de poutres rattachent directement la base à la structure interne imaginée par Gustave Eiffel de façon que la statue ne fasse qu' un avec son piédestal .
Les différentes pièces de la statue furent terminées en France dès 1884 .
La statue fut envoyée à Rouen par le train , puis elle descendit la Seine en bateau avant d' arriver au port du Havre .
Elle entra dans le port de New York le 17 juin 1885 , à bord d' une frégate française , l ' Isère , et reçut un accueil triomphal de la part des New-Yorkais .
Afin de rendre la traversée possible à bord d' un tel navire , la statue fut démontée en 350 pièces , réparties dans 214 caisses , en sachant que le bras droit et sa flamme étaient déjà présents sur le sol américain , où ils avaient été exposés une première fois lors de la Centennial Exposition , puis à New York .
Ferdinand de Lesseps et de nombreux francs-maçons étaient également présents .
L' accès de l' île fut interdit les 10 jours suivant l' explosion et , pour réparer le flambeau , le gouvernement engagea le sculpteur Gutzon Borglum , qui plus tard conçut le mont Rushmore .
En effet , en 1983 , le monument fut placé au cœur d' une opération promotionnelle menée par American Express , visant à récolter des fonds pour entretenir et rénover l' édifice .
Il fut convenu que chaque achat fait avec une carte American Express entraînerait un don d' un cent par l' entreprise bancaire .
Le président de Chrysler , Lee Iacocca , fut nommé par le président Ronald Reagan à la tête de la commission responsable de la supervision des œuvres , mais il fut plus tard destitué pour " éviter tout conflit d' intérêts " .
La flamme actuelle reprend le modèle original de Bartholdi alors que depuis l' inauguration elle avait été remplacée par un phare , qui n' a d' ailleurs pas fonctionné longtemps ( 1886-1891 ) .
Une équipe de Rémois a donc remis à neuf la torche rongée par la rouille .
La statue , entourée de son échafaudage , apparaît d' ailleurs dans le film Remo sans arme et dangereux , sorti en 1985 .
Bartholdi avait pourtant anticipé ce phénomène et prévu une combinaison d' amiante et de poix pour séparer les deux métaux , mais l' isolation s' était détériorée plusieurs décennies auparavant .
Lors de la construction de la statue , le membre avait été décalé de 46 centimètres sur la droite , et en avant par rapport à la structure centrale d' Eiffel .
La statue fut déclarée monument national le 15 octobre 1924 et fut confiée au National Park Service le 10 juin 1933 .
Celles-ci commencèrent le 3 juillet par une cérémonie d' ouverture sur Governors Island , et s' achevèrent le 6 juillet dans le Giants Stadium de New York .
La cérémonie d' ouverture , qui se tint le jeudi 3 juillet dans le port de New York et sur Governors Island , attira de nombreuses célébrités , comme Gene Kelly , Gregory Peck et Steven Spielberg .
Après plusieurs chansons interprétées par Debbie Allen , Neil Diamond et Frank Sinatra , le président de l' époque , Ronald Reagan prononça deux discours : le premier au milieu de la cérémonie pour dévoiler les travaux sur la statue , et le second à la fin , au moment d' allumer la torche de la statue , puis de déclencher les feux d' artifice .
Reagan aurait alors dit que le cortège auquel le public allait assister était aussi coloré que des feux d' artifices , et que Lady Liberty elle-même .
Un concert fut donné plus tard dans la soirée , avec notamment la participation du compositeur John Williams .
Le lendemain matin , l' épouse du président , Nancy Reagan prononça un discours marquant la réouverture de la statue au public , et le soir , un opéra fut joué à Central Park .
Le 6 juillet , les cérémonies de clôture eurent lieu dans le Giants Stadium situé dans le New Jersey , mais géographiquement proche de la statue .
Les visiteurs arrivaient par ferry , le plus souvent en provenance de Battery Park , et avaient la possibilité de grimper l' unique escalier en colimaçon au cœur de la structure métallique .
De là , il était possible d' apercevoir le port de New York , mais pas la skyline de Manhattan contrairement à une croyance répandue .
Cela s' explique par le fait que le visage de la statue est orienté en direction de l' océan Atlantique et de la France , vers l' est .
En outre , bien que l' intérieur de la statue soit inaccessible , une baie vitrée située à l' intérieur du socle permet de voir la structure interne réalisée par Gustave Eiffel .
Tous les visiteurs qui désirent se rendre sur Liberty Island sont contrôlés de la même manière que dans les aéroports .
À partir du 4 juillet 2009 , l' accès du public à l' intérieur de la tête de la statue de la Liberté est rétabli pour deux ans avant une nouvelle fermeture devant permettre une rénovation totale .
Le diadème fait aussi penser à celui que portait le dieu du soleil Hélios .
La hauteur de la statue de la Liberté est de 46,5 mètres , hauteur qui est portée à 92,9 mètres entre la base du piédestal et la torche .
Sur sa base , une plaque de bronze porte , gravée , une partie ( la fin ) du poème de la poétesse américaine Emma Lazarus , intitulé " The New Colossus " ( " le nouveau colosse " ) .
En raison de son statut de monument universel , la statue de la Liberté a été copiée et reproduite à différentes échelles et en divers endroits du globe .
La majorité d' entre elles se trouvent aujourd'hui en France ou aux États-Unis , cependant , on en retrouve dans de très nombreux pays , parmi lesquels l' Autriche , l' Allemagne , le Brésil , la Chine , l' Italie , le Japon , ainsi que le Viêt Nam , ancienne colonie française .
Parmi les principales répliques françaises du monument , on trouve celle de l' île des Cygnes à Paris , haute de 11.50 m , qui se dresse à l' extrémité aval de l' île , à la hauteur du pont de Grenelle , près de l' ancien atelier de Bartholdi .
Il y a aussi une réplique de la statue de la liberté à Colmar , érigée en 2004 à l' entrée nord de la ville pour marquer ainsi l' année du centenaire de la mort de Frédéric Auguste Bartholdi .
Il y a également , à Barentin , en Seine-Maritime , une copie en polyester de 13.5 m et d' un poids de 3,5 tonnes , utilisée dans le film Le Cerveau de Gérard Oury , sorti en 1965 .
C' est grâce à l' action de Paul Belmondo ( le père de Jean-Paul Belmondo ) , du maire de Barentin à l' époque André Marie , et de Gérard Oury que cette statue fut conservée .
Il existe une autre réplique , réalisée dans les années 1900 , sur la place du village de Saint-Cyr-sur-Mer dans le Var .
Il en existe également une à Lunel ( Hérault ) .
Une réplique de la flamme de la statue , la Flamme de la Liberté , réalisée par les mêmes deux entreprises françaises ayant fait cette part de restauration à New-York en 1985-86 , " offerte par les États-Unis " grâce à une souscription lancée par l ' International Herald Tribune fut installée à Paris place de l' Alma en 1989 et est devenue depuis 1997 un monument commémoratif " spontané " de l' accident mortel de la princesse Diana , survenu juste au dessous .
Pendant la Guerre froide , la statue était figurée sur les affiches de propagande comme symbole de la liberté ou des États-Unis .
Les dessinateurs américains en ont fait l' incarnation de New York au moment des attentats du 11 septembre 2001 .
La toute première remonte à 1917 , dans le film de Charlie Chaplin L' Émigrant .
En 1942 , elle apparaît dans le film d' Alfred Hitchcock , Cinquième Colonne lors de la scène finale .
À la fin de la première version de La planète des singes , elle se trouve en partie ensevelie sous le sable d' une plage .
Elle est également filmée dans d' autres blockbusters comme Le Cinquième Élément , Le Jour d' après , A.I. Intelligence artificielle , ainsi que dans des cut-scenes de séries télévisées comme Sex and the City ou Les Experts : Manhattan .
Dans Cloverfield , film catastrophe réalisé par Matt Reeves , un monstre sème la destruction dans New York .
On voit une scène où la tête de la statue de la Liberté s' écrase brutalement en pleine rue .
En 1978 , la statue est au cœur d' un canular imaginé à l' université du Wisconsin-Madison .
Le monument figure en outre sur les plaques d' immatriculation de l' État de New York ainsi que sur celles du New Jersey .
Dans le milieu du sport , Lady Liberty sert de logo à l' équipe de la LNH des Rangers de New York , et à l' équipe de basket-ball des Liberty de New York , qui évolue en WNBA .
En 2000 , le monument fait partie des propositions pour désigner les " sept nouvelles merveilles du monde " , projet lancé par le réalisateur suisse Bernard Weber .
Le logo de l' Université de New York reprend la torche de la statue de la Liberté pour montrer qu' elle est au service de la ville de New York .
L' autorisation lui avait été donnée par le capitaine d' armée en charge de Liberty Island .
Selon un article du New York Times , le cascadeur " serait tombé comme un poids d' une hauteur de 23 mètres , alors que le parachute ne montrait aucune intention de s' ouvrir au départ " , avant de descendre " gracieusement " mais d' atterrir durement pour enfin s' éloigner en boitillant .
Le premier suicide enregistré sur la statue de la Liberté remonte au 13 mai 1929 .
