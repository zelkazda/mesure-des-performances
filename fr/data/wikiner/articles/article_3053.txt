Frédéric Dutoit est un homme politique français , né le 26 mai 1956 à Marseille ( Bouches-du-Rhône ) .
En 1995 , Guy Hermier , alors maire du 8 e secteur , le nomme premier adjoint .
À la mort de Guy Hermier en août 2001 , il prend sa succession en tant que maire , puis en tant que député lorsqu' il est élu député de la quatrième circonscription des Bouches-du-Rhône le 16 juin 2002 .
Pour la première fois depuis 80 ans , le PCF n' a plus de député à Marseille .
