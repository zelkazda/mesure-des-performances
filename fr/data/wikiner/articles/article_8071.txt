Louis VIII et Louis IX créent aussi des apanages .
Louis XI assure le retour de la Bourgogne et de l' Anjou au domaine royal .
François I er confisque le Bourbonnais en 1531 , le dernier apanage d' importance , à l' occasion de la trahison du connétable de Bourbon .
Les apanages sont supprimés en 1792 avant la proclamation de la Première République française .
Les apanages sont rétablis par Napoléon Bonaparte et confirmés par le roi Louis XVIII .
A la mort de Robert II d' Artois en 1302 , c' est ainsi sa fille Mahaut d' Artois qui s' empare du comté , au détriment d' ailleurs de la descendance masculine du défunt .
Par la suite , le comté est transmis par les femmes à Philippe de Rouvres en 1347 puis à Louis de Mâle en 1382 .
À partir de Philippe le Bel , les dispositions excluant le droit des filles à hériter des nouveaux apanages se généralisent .
Du point de vue de Louis XI et de François I er , l' extinction de la descendance masculine de Philippe le Hardi présente l' opportunité de réunir au domaine royal la Bourgogne ducale .
Ils se comportent comme si cette province était revenue une première fois au domaine par absence d' héritier mâle sous Jean II Le Bon et que ce dernier en a aussitôt fait un nouvel apanage qui doit suivre la règle de succession par les mâles .
