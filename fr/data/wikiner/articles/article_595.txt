Hiroshi Yamauchi ( né le 7 novembre 1927 à Kyoto ) arrière petit-fils de Fusajiro Yamauchi , est un homme d' affaire .
Il était le troisième président de Nintendo depuis 1949 , et malgré des débuts un peu houleux , il s' est imposé comme le président emblématique et incontesté de la société .
Sa passion pour le baseball l' a poussé à acquérir les Seattle Mariners , et à encourager la création des premiers jeux vidéo consacrés à la discipline .
Il s' est fait remarquer dernièrement par un don de 7 milliards de yens ( soit environ 51,100,000 euros ) en faveur de l' hôpital universitaire de Kyoto , en février 2006 .
Après son départ de Nintendo , ses fonctions ont été occupées par un comité directeur dirigé par Satoru Iwata .
