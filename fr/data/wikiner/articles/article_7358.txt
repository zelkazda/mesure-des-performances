Il fut dépossédé de son pouvoir en 68 et se suicida assisté de son scribe Epaphroditos .
Certains historiens débattent encore aujourd'hui de la folie , réelle ou mise en scène , de Néron .
Les sources primaires concernant Néron doivent être lues avec précaution .
Suétone et Tacite avaient rang de sénateur .
Leurs descriptions des événements du règne de Néron sont suspectes dans la mesure où l' on sait que Néron persécuta les sénateurs romains à partir des années 65-66 à la suite de la découverte de deux conspirations .
Certains récits exaltés du règne de Néron pourraient donc n' être que des exagérations .
Lucius Domitius Ahenobarbus est né le 15 décembre 37 .
Son oncle maternel Caligula venait de commencer à régner le 16 mars de cette année , à 25 ans .
Ses prédécesseurs , Octave et Tibère , avaient vécu respectivement jusqu' à 76 et 79 ans .
Si Caligula vivait aussi longtemps qu' eux , il pouvait espérer une succession par ses propres descendants .
Un scandale marquant le début du règne de Caligula fut sa relation particulièrement étroite avec ses trois sœurs Drusilla , Julia Livilla et Agrippine .
Les écrits de Flavius Josèphe , Suétone , Dion Cassius rapportent qu' elles avaient des relations incestueuses avec leur frère .
La mort rapide de Drusilla en 38 n' a fait que renforcer ce soupçon .
On disait d' elle qu' elle était la favorite de Caligula ; elle a d' ailleurs été enterrée avec les honneurs dus à une impératrice .
Caligula la déifia même , faisant d' elle la première femme de l' histoire romaine à obtenir cet honneur .
Caligula n' avait toujours pas d' enfant .
Ils étaient les héritiers probables en cas de décès prématuré de Caligula .
Caligula ordonna également l' exécution de Gnaeus Cornelius Lentulus Gaetulicus , le populaire légat de Germanie supérieure , et son remplacement par Servius Sulpicius Galba .
Agrippine et Livilla furent reléguées aux Îles Pontines .
La chance lui sourit l' année suivante : le 24 janvier 41 , Caligula , son épouse Cæsonia Milonia , et leur fille Julia Drusilla furent assassinés par une conspiration menée par Cassius Chaerea .
La garde prétorienne aida Claude à obtenir le trône .
Agrippine se remaria rapidement au riche Gaius Sallustius Crispus Passienus .
Son mari mourut entre 44 et 47 , et Agrippine fut suspectée de l' avoir empoisonné pour hériter de son immense fortune .
Claude , âgé de 57 ans à cette époque , avait régné plus longtemps , et sans doute plus efficacement que son prédécesseur .
Claude s' était déjà marié trois fois .
Il avait épousé Plautia Urgulanilla et Aelia Paetina quand il était simple citoyen .
Empereur , il s' était marié à Valeria Messalina .
Le couple avait deux enfants , Britannicus ( né en 41 ) et Octavie ( née en 40 ) .
Messaline n' avait que 25 ans et pouvait lui donner d' autres héritiers .
Pourtant , Messaline fut exécutée en 48 , accusée de conspiration contre son époux .
L' ambitieuse Agrippine projeta rapidement de remplacer sa tante par alliance .
Le 1 er janvier 49 , elle devint la quatrième femme de Claude , Tiberius Claudius Nero Caesar Drusus .
La même année , Agrippine fait rompre les fiançailles d' Octavie et de Lucius Junius Silanus et la fait fiancer avec Néron .
Néron était plus âgé que Britannicus , son frère adoptif , et cette adoption fit de lui l' héritier officiel du trône .
Claude honora son fils adoptif de plusieurs manières .
Néron fut émancipé en 51 , à 14 ans .
En 53 , il épousa sa sœur adoptive , Octavie .
Claude mourut empoisonné le 13 octobre 54 et Néron fut rapidement nommé empereur à sa place .
Les historiens s' accordent à considérer que Sénèque a joué le rôle de figure de proue au début de son règne .
Les décisions importantes étaient probablement laissées entre les mains plus capables de sa mère Agrippine la Jeune ( qui pourrait avoir empoisonné Claude elle-même ) , de son tuteur Sénèque , et du préfet du prétoire Sextus Afranius Burrus .
Néron cherche dès le début de son règne à obtenir les faveurs de l' armée et de la plèbe par diverses primes .
Les problèmes devaient pourtant bientôt surgir de la vie personnelle de Néron et de la course à l' influence croissante entre Agrippine et les deux conseillers .
Tout le monde savait que Néron était déçu de son mariage et trompait Octavie .
Il prit pour maîtresse Claudia Acte , une ancienne esclave , en 55 .
Agrippine tenta d' intervenir en faveur d' Octavie et exigea de son fils le renvoi d' Acte .
Néron résista à l' intervention de sa mère dans ses affaires personnelles .
Son influence sur son fils diminuant , Agrippine se tourna vers un candidat au trône plus jeune .
Britannicus , à treize ans , était toujours légalement mineur et sous la responsabilité de Néron , mais il approchait de l' âge de la majorité .
Britannicus était un successeur possible de Néron et établir son influence sur lui pouvait renforcer la position d' Agrippine .
Néron se révoltait de plus en plus contre l' emprise d' Agrippine , et il commençait à envisager le meurtre de sa propre mère .
Marcus Salvius Otho était au nombre de ces nouveaux favoris .
À tous points de vue , Othon était aussi débauché que Néron , mais il devint aussi intime qu' un frère .
Othon aurait présenté à Néron une femme qui aurait d' abord épousé le favori , puis l' empereur .
Poppée ( Poppaea Sabina ) était décrite comme une femme de grande beauté , pleine de charme , et d' intelligence .
On peut trouver dans de nombreuses sources les rumeurs d' un triangle amoureux entre Néron , Othon , et Poppée .
En 58 , Poppée avait assuré sa position de favorite de Néron .
L' année suivante ( 59 ) fut un tournant dans le règne de Néron .
Néron et/ou Poppée auraient organisé le meurtre d' Agrippine .
Othon fut bientôt chassé de l' entourage impérial et envoyé en Lusitanie comme gouverneur .
Leur remplaçant aux postes de préfet du prétoire et de conseiller fut Tigellin .
Il avait été banni en 39 par Caligula , accusé d' adultère avec à la fois Agrippine et Livilla .
Il avait été rappelé d' exil par Claude , puis avait réussi à devenir un proche de Néron ( et peut-être son amant ) .
Avec Poppée , il aurait eu une plus grande influence que Sénèque en eut jamais sur l' empereur .
Quelques mois plus tard , Tigellin épousait Poppée .
Néron , âgé alors de vingt-cinq ans , avait régné huit ans et n' avait pas encore d' héritier .
Mais Néron avait déjà acquis la réputation d' être infidèle , alors qu' Octavie était connue pour être un parangon de vertu .
Néron réussit à obtenir le divorce pour cause d' infertilité , ce qui lui permettait d' épouser Poppée et d' attendre qu' elle donne naissance à un héritier .
Au cours de cette année , Néron fit exécuter deux des membres restants de sa famille :
Début 63 , Poppée donna naissance à une fille : Claudia Augusta .
Néron célébra l' évènement , mais l' enfant mourut quatre mois plus tard .
Néron n' avait toujours pas d' héritier .
Le feu débuta dans les boutiques des environs du Grand Cirque .
Mais Néron perdit toute chance de redorer sa réputation en rendant trop vite public ses projets de reconstruction de Rome dans un style monumental .
La population désorientée cherchait des boucs émissaires , et bientôt des rumeurs tinrent Néron pour responsable .
Il était important pour Néron d' offrir un autre objet à ce besoin de trouver un coupable .
Tacite nous fait le récit de cet épisode :
Bien que les anciennes sources ( et les lettrés ) penchent pour un Néron incendiaire , il faut rappeler que les incendies étaient fréquents dans la Rome antique .
La célèbre Domus aurea faisait partie du projet de reconstruction imaginé par Néron .
En 65 , Néron fut impliqué dans un autre scandale , pris plus au sérieux par le peuple de cette époque qu' il ne le serait de nos jours .
De plus , Néron ordonna que Gnaeus Domitius Corbulo , un général populaire et valeureux , se suicidât , pour faire suite à de vagues soupçons de trahison .
Cette décision poussa les commandeurs militaires , à Rome et dans les provinces , à envisager l' organisation d' une révolution .
En 65 , Poppée meurt alors qu' elle était enceinte , d' un coup porté au ventre par Néron , si l' on en croit Tacite et Suétone , et ce , malgré la passion qu' il semblait lui vouer .
Néron va d' abord essayer de se remarier à Claudia Antonia , la fille de Claude et d' Aelia Paetina ( sa demi-sœur par adoption ) .
Comme celle-ci refuse , Néron la fait tuer sous prétexte qu' elle fomentait un complot .
Néron se tourne alors vers son ancienne maîtresse , Statilia Messalina .
Dès le mois de septembre , Néron quitte sa jeune épouse pour un voyage de plus d' un an en Grèce .
L' empereur partit en Grèce , en 66 , où il distrayait ses hôtes avec des spectacles artistiques ( les écrits de Tacite rapportent cependant que l' empereur empêchait quiconque de sortir de l' amphithéâtre lorsqu' il déclamait ses écrits , et que certains spectateurs durent se faire passer pour morts pour s' échapper , tant ils s' ennuyaient ) , alors qu' à Rome le préfet du prétoire Nymphidius Sabinus cherchait à obtenir le soutien des gardes prétoriens et des sénateurs .
Galba , son ( autrefois ) fidèle serviteur , gouverneur d' Hispanie ( Espagne ) , était l' un de ces nobles dangereux .
Nymphidius Sabinus corrompit la garde impériale , qui se retourna contre Néron avec la promesse d' une récompense financière de Galba .
Ils s' appuient sur les textes de Suétone , fréquemment colporteur de ragots , et de Tacite , augmentés des attaques des auteurs chrétiens , et couronnés par des œuvres de fiction comme Quo Vadis .
De plus , aucune loi anti-chrétienne ne fut promulguée sous son règne de manière officielle : il y a bien eu persécution , mais uniquement localisée à Rome .
Ainsi la grande popularité auprès du peuple de son temps prit , dès sa mort , le mythe du " retour de Néron " : caché chez les Parthes , il devait réapparaître à la tête d' une armée pour vaincre les conspirateurs et rentrer victorieux à Rome .
Néron est le personnage principal de plusieurs opéras dont :
