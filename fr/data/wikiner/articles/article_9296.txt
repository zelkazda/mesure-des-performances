Guadalinex est un système d' exploitation promu par le gouvernement d' Andalousie .
En avril 2003 , le gouvernement de l' Andalousie avait annoncé qu' il favoriserait l' implantation de logiciels libres dans sa région et qu' il collaborerait avec l' Estrémadure qui avait adopté Linux un an auparavant .
Un accord fut signé avec l' Andalousie et le décret 72/2003 fut émis .
