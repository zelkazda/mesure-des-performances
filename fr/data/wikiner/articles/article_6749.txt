On peut distinguer deux conceptions différentes d' Aphrodite : celle du plaisir de la chair , plus " terrienne " en quelque sorte , et celle de l' amour spirituel , pure et chaste dans sa beauté .
Mariée à Héphaïstos , elle a de multiples aventures extra-conjugales .
La principale est celle avec Arès , d' où naissent Harmonie , Déimos , Phobos et Éros .
Par suite , Aphrodite maudit Hélios et sa descendance , c' est-à-dire Pasiphaé et ses filles Ariane et Phèdre ( malédiction qui sera aggravée par celle dont Poséidon affligera Minos , époux de Pasiphaé et père d' Ariane et Phèdre ) .
Aphrodite a également une liaison avec :
Aphrodite passe en outre pour avoir distingué de nombreux héros mortels , parmi lesquels :
La vengeance d' Aphrodite est terrible .
Pour la vindicte , elle ne le cède en rien à Héra , mais si cette dernière ne poursuit les femmes que par jalousie , Aphrodite ne les frappe que lorsqu' elles la servent mal ou refusent de la servir , et les femmes sont alors tant ses victimes que ses instruments destinés aux hommes , plus rarement par jalousie , leur inspirant parfois des amours difficiles :
La légende la plus connue concernant Aphrodite est peut-être celle qui raconte la cause de la guerre de Troie .
Éris , la seule déesse à ne pas être invitée au mariage du roi Pélée et de la nymphe de la mer Thétis , jette par dépit une pomme d' or dans la salle du banquet avec l' inscription " À la plus belle " .
Elles demandent à Pâris , prince de Troie , d' être le juge .
Héra lui promet la puissance royale , Athéna , la gloire militaire , et Aphrodite , la plus belle femme du monde .
Pâris choisit Aphrodite et demande en récompense Hélène de Troie , femme du roi grec Ménélas .
L' enlèvement d' Hélène par Pâris provoque la guerre de Troie .
Au cours de cette guerre , la déesse sera légèrement blessée par le héros grec Diomède en portant secours à son fils Énée .
De fait , elle correspond très probablement à la déesse Ishtar -- Astarté , avec laquelle elle partage de nombreux traits : ce sont des divinités androgynes ; Astarté est la " reine du ciel " alors qu' Aphrodite est dite " la céleste " ; leur culte comprend l' offrande d' encens et le sacrifice de colombes .
Par ailleurs , le nom d' Aphrodite n' a pas été retrouvé sur les tablettes de linéaire B , témoignages écrits de la civilisation mycénienne .
C' est peut-être à cette fête qu' il faut rattacher un rite rapporté par l' apologiste chrétien Clément d' Alexandrie , selon lequel les participants reçoivent un gâteau en forme de phallus et apportent une pièce de monnaie , " comme à une courtisane ses amants " .
Il est probable que Clément parle en fait de l' argent destiné aux sacrifices , ou la taxe pour les oracles , mais il est également possible que la prostitution sacrée ait également été pratiquée .
Aphrodite est particulièrement vénérée en Asie mineure , notamment en Troade .
Nouvelle-Ilion frappe des monnaies à son effigie et la ville d' Aphrodisias porte son nom .
Selon Strabon , qui écrit aux débuts de l' ère chrétienne , on y pratique la prostitution sacrée : " le temple d' Aphrodite à Corinthe était si riche , qu' il possédait à titre de hiérodules ou d' esclaves sacrés plus de mille courtisanes , vouées au culte de la déesse par des donateurs de l' un et de l' autre sexe . "
