L' Œuvre au noir est un roman de Marguerite Yourcenar , paru le 8 mai 1968 .
La fin du personnage ( refus de rétractation ) n' est pas sans rapport avec celle de Giordano Bruno .
L' Œuvre au Noir peut être vue comme le pendant médiéval des Mémoires d' Hadrien , le roman le plus célèbre de Marguerite Yourcenar .
