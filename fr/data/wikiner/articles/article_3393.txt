Le Pays de Galles est l' une des quatre nations constitutives et une des trois nations celtes du Royaume-Uni .
En 1999 , une Assemblée nationale de Galles a été instituée .
Elle siège à Cardiff .
Les Saxons ont toujours échoué à conquérir le pays de Galles , tant en raison du terrain montagneux , que de la résistance acharnée du peuple gallois .
Sa superficie est d' environ 20.779 km ² ( soit environ le quart de la superficie de l' Écosse ) .
Le Pays de Galles est bordé par l' Angleterre à l' est et par la mer dans les trois autres directions .
Au total , le pays de Galles a plus de 1200 km de littoral .
La plus grande partie du territoire de Galles est montagneuse , en particulier dans le nord et les régions centrales .
Aujourd'hui , le gallois est la première langue celtique parlée au monde , avec plus de 580000 locuteurs recensés au Pays de Galles et 133000 en Angleterre .
Le dragon rouge ( Y ddraig goch en gallois ) , qui symbolise la lutte entre les Saxons et les Celtes .
Une légende raconte que le roi Uther Pendragon ( père du roi Arthur ) voulait construire un château mais la terre tremblait et en détruisait sans arrêt les fondations .
Merlin , appelé Myrddin en gallois , qui avait le don de voyance , comprit que ce tumulte était causé par deux dragons : l' un , le dragon blanc , avait pris la place de l' autre , le dragon rouge , dans sa caverne .
On connaît quelques poèmes , probablement apocryphes , de Merlin .
L' origine du poireau comme symbole remonte à une bataille qui se déroula dans un champ de poireaux , où saint David conseilla aux combattants gallois de s' en munir pour se distinguer de leurs assaillants .
A noter également que l' équipe de l' équipe du pays de Galles de rugby à XV est familièrement appelée par ses supporters le " le XV du poireau " .
De nos jours , le taux d' assistance aux célébrations religieuses au pays de Galles est de 8,6 % , soit le plus bas du Royaume-Uni .
Cette dernière est majoritairement composée de personnes originaires d' autres pays européens , surtout l' Irlande .
Le pays de Galles est réputé pour le nombre et la qualité de ses chorales .
Le Pays de Galles est , selon les paroles de l' hymne officiel , " gwlad beirdd a chantorion " , un " pays de bardes et de chanteurs " .
