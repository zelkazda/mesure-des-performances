Il naît le 13 janvier 1951 à Chamalières en Auvergne dans un milieu modeste .
En 2003 , il projette d' ouvrir un hôtel de caractère et de luxe l' année suivante , dans le sud-ouest , autour de Toulouse .
En 2003 , tout le programme d' embellissement du Relais & Châteaux est terminé .
Devant la sévérité de ce guide qui lui reproche de s' endormir sur ses lauriers , il se suicide sans laisser d' explication le 24 février 2003 , à l' âge de 52 ans , dans sa maison de Saulieu , laissant un grand étonnement et un certain mystère sur les causes de son départ anticipé .
