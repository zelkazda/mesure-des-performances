Reyrieux est un chef-lieu de canton .
Reyrieux était , jusqu' à 1900 , par la superficie , la seconde commune du canton de Trévoux , juste derrière Saint-André-de-Corcy .
Elle a perdu le 16 février 1900 les 474 hectares de Toussieux devenue commune et se retrouve de la sorte au cinquième rang .
Pour la population , elle arrive nettement au troisième rang après Trévoux et Jassans-Riottier en 1975 .
Comme ses voisines Massieux , Parcieux et Trévoux , elle juxtapose la plaine alluviale de la Saône ( en l' occurrence le remblaiement de la rive convexe d' un large méandre ) avec les pentes mollement adoucies de la côtière .
Reyrieux est d' ailleurs un pays très riche en sources , résurgences des eaux du plateau .
On note comme limites , à l' ouest Trévoux , au sud la Saône et la commune de Parcieux , à l' est Civrieux et Rancé , au nord Toussieux , Sainte-Euphémie et Saint-Didier-de-Formans .
La voie ferrée Sathonay-Trévoux aujourd'hui supprimée , passe en dessous du village à une altitude moyenne de 171 mètres .
Un guichard de Reyrieux est mentionné en 1096 comme bienfaiteur d' un prieuré .
En 1750 , l' auberge servit de repaire au célèbre brigand Mandrin .
Toussieux fut un hameau de Reyrieux jusqu' en 1900 .
Cinquante ans plus tard , à la veille de la Grande Guerre , la vie agricole demeurait l' essentiel de l' activité , avec l' apparition du colza , du maïs , le recul du chanvre et le développement de l' élevage comme partout en Dombes .
En 1986 , Reyrieux est devenu le chef-lieu du 43 e canton du département de l' Ain .
