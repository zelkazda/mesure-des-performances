Il défia l' autorité papale en tenant la Bible pour seule source légitime d' autorité religieuse .
Elle eut notamment une large influence sur la traduction anglaise connue sous le nom de Bible du roi Jacques .
Son père , paysan d' origine , devint mineur dans une mine de cuivre de la région de Mansfeld et reçut le statut de bourgeois puis de magistrat .
En 1501 , à l' âge de dix-sept ans , il entra à l' université d' Erfurt , où il obtint un diplôme de bachelier en 1502 et une maîtrise en 1505 .
Selon Martin Luther , il se serait voué à la vie monastique à Erfurt dès les prémices du mois de juillet 1505 .
En même temps , il continue à étudier la théologie et bientôt commence à l' enseigner : ordonné prêtre en 1507 , il est désigné pour enseigner la philosophie au couvent d' Erfurt .
Docteur en théologie en 1512 , il occupe par la suite la chaire d' enseignement biblique à Wittenberg , ville où il sera à partir de 1514 également prédicateur de l' église .
Certains font remonter les idées réformatrices de Luther à un séjour qu' il fit à Rome en 1510 -- 1511 pour les affaires de son ordre .
Plus importants sont ses travaux sur les épîtres de Paul et son obsession du salut divin .
Face à Martin Luther , Rome a choisi l' affrontement , méconnaissant l' adversaire et sa pugnacité , et sans doute aussi la situation politique allemande .
Le procès menant à son excommunication , loin d' affermir le catholicisme , n' a fait qu' accélérer le processus de la Réforme .
En octobre 1518 , Martin Luther est convoqué à Augsbourg , où le cardinal Cajetan , nonce apostolique , est chargé d' obtenir sa rétractation .
Luther aggrave même son cas en juillet de l' année suivante , en mettant en cause l' infaillibilité des conciles .
En juin 1520 , Rome publie la bulle Exsurge Domine le menaçant d' excommunication , tandis que ses livres sont brûlés .
Luther réagit avec la même violence , brûlant le 10 décembre à la fois la bulle papale et le droit canonique .
Mais il dispose cependant , outre un appui populaire assez large , de divers appuis politiques , tels celui du landgrave de Hesse et surtout celui du prince-électeur de Saxe Frédéric III le Sage ( 1463-1525 ) .
Il y demeure jusqu' au 6 mars 1522 sous le pseudonyme de chevalier Georges .
C' est ici que Luther commence sa traduction de la Bible , d' abord celle du Nouveau Testament .
Après moins de deux ans de clandestinité , il revient de son propre chef à Wittenberg , qu' il ne quittera plus guère désormais , et où il ne sera plus vraiment inquiété .
Lors de la diète de Spire ( avril 1529 ) , le souverain tente bien de reprendre les choses en main , mais il se heurte à six princes et quatorze villes qui protestent d' en appeler à un concile si Charles Quint veut revenir à l' édit de Worms .
Les détracteurs de Martin Luther lui ont souvent fait grief de ce soutien des princes , lui reprochant d' avoir mis en place une religion qui n' est pas vraiment celle du peuple .
Ils lui reprochent surtout son comportement pendant la guerre des Paysans ( 1524 -- 1525 ) , révolte provoquée par la misère mais liée aussi à la question religieuse et à des préoccupations proches des siennes ( plusieurs leaders du mouvement étaient anabaptistes ) .
En avril 1525 , en des termes très durs , Luther se prononce pour une répression impitoyable de la révolte -- il y aura en tout plus de 100000 morts .
Luther n' en demandait pas tant : selon lui , il importait d' éviter de heurter les faibles , seule la parole persuasive était de mise .
Bien que désapprouvant les moines qui s' étaient hâtés de quitter son propre couvent de Wittenberg , Luther , au terme d' une réflexion critique sur les vœux monastiques , se marie lui-même en 1525 avec une ancienne religieuse , Catherine de Bora dont il aura six enfants .
Luther , et plus tard Jean Calvin , y apportèrent leur soutien .
Ils se basaient sur les mots de la Bible " tu n' accepteras pas de laisser vivre une sorcière " .
Luther alla jusqu' à en parler dans certains de ses sermons .
D' autre part , après avoir prêché ouvertement une attitude humaine et tolérante envers les Juifs , il se laissera aller vers la fin de son existence à des considérations judéophobes .
Enfin Martin Luther est conscient de s' être trop volontiers et trop souvent abandonné , dans ses écrits polémiques , à un talent inné de pamphlétaire dont les insultes , truculentes , n' étaient pas absentes .
Luther a vécu toutes ses dernières années à Wittenberg .
Mais il s' en est également pris aux Juifs , coupables apparemment de ne pas s' être convertis à la nouvelle religion , et dont il souhaitait voir les synagogues brûler , les maisons détruites et l' argent confisqué .
L' antisémitisme de Luther lui a été longtemps reproché , d' autant que les nazis n' ont pas hésité à le revendiquer pour justifier leurs crimes .
Martin Luther s' est éteint après avoir confirmé sa foi , alors qu' il était à Eisleben , sa ville natale , afin de régler un différend entre les comtes de Mansfeld .
Sous le nom de chorals , ces cantiques deviennent le centre de la liturgie protestante , et leur influence sur le développement de la musique allemande se fait sentir durant de longues années , si l' on pense à la place essentielle qu' ils occupent dans l' œuvre de Jean-Sébastien Bach .
Le plus connu de ses hymnes , Ein ' feste Burg ( " C' est un rempart que notre Dieu " ) , reste populaire parmi les luthériens et d' autres protestants aujourd'hui .
