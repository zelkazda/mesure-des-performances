JavaScript est un langage de programmation de scripts principalement utilisé dans les pages web interactives mais aussi coté serveur .
Le langage a été créé en 1995 par Brendan Eich pour le compte de Netscape Communications Corporation .
Le langage actuellement à la version 1.7 est une implémentation du standard ECMA-262 .
La version 1.8 est en développement et intégrera des éléments du langage Python .
La version 2.0 du langage est prévue pour intégrer la 4 e version du standard ECMA .
Le langage a été créé en 1995 par Brendan Eich pour le compte de la Netscape Communications Corporation , qui s' est inspiré de nombreux langages , notamment de Java mais en simplifiant la syntaxe pour les débutants .
Netscape travaille alors au développement d' une version orientée client de LiveScript .
Quelques jours avant sa sortie , Netscape change le nom de LiveScript pour JavaScript .
Sun Microsystems et Netscape étaient partenaires , et la machine virtuelle Java de plus en plus populaire .
En décembre 1995 , Sun et Netscape annoncent la sortie de JavaScript .
En mars 1996 , Netscape met en œuvre le moteur JavaScript dans son navigateur Web Netscape Navigator 2.0 .
Le succès de ce navigateur contribue à l' adoption rapide de JavaScript dans le développement web orienté client .
Microsoft réagit alors en développant JScript , qu' il inclut ensuite dans Internet Explorer 3.0 en août 1996 pour la sortie de son navigateur .
Netscape soumet alors JavaScript à Ecma International pour standardisation .
Les travaux débutent en novembre 1996 , et se terminent en juin 1997 par l' adoption du nouveau standard ECMAScript .
JavaScript est décrit comme un complément à Java dans un communiqué de presse commun de Netscape et Sun Microsystems , daté du 4 décembre 1995 .
La troisième édition d' ECMAScript , parue en 1999 correspond à la version 1.5 de JavaScript .
Sa mise en œuvre par Microsoft est JScript .
Le propos de JavaScript est de manipuler de façon simple des objets , au sens informatique , fournis par une application hôte .
C' est alors le navigateur Web qui prend en charge l' exécution de ces programmes appelés scripts .
Généralement , JavaScript sert à contrôler les données saisies dans des formulaires HTML , ou à interagir avec le document HTML via l' interface Document Object Model , fournie par le navigateur .
JavaScript n' est pas limité à la manipulation de documents HTML et peut aussi servir à manipuler des documents SVG , XUL et autres dialectes XML .
Netscape et Microsoft ( avec JScript dans Internet Explorer ) ont développé leur propre variante de ce langage qui chacune supporte presque intégralement la norme ECMAScript mais possède des fonctionnalités supplémentaires et incompatibles , rarement utilisées dans le cadre de la programmation de pages web .
Pourtant les scripts JavaScript sont souvent la source de difficultés .
Elles sont plus souvent dues à la prise en charge des différentes versions des modèles d' objets ( DOM ) fournis par les navigateurs , qu' à des problèmes de portabilité du langage ( les différentes mises en œuvre respectant relativement bien la norme ECMAScript ) .
JavaScript est un des composants essentiels de la technique AJAX .
La plupart des applications AJAX utilisent l' objet XMLHTTPRequest ( XHR ) pour envoyer une requête à un script serveur , et parser dynamiquement les résultats de ce dernier via DOM .
JSON ( JavaScript Object Notation ) est un format utilisant la notation des objets JavaScript pour transmettre de l' information structurée , d' une façon plus compacte et plus proche des langages de programmation , que XML .
Malgré l' existence du DOM et l' introduction récente de E4X ( voir ci-dessous ) dans la spécification du langage JavaScript , JSON reste le moyen le plus simple d' accéder à des données , puisque chaque flux JSON n' est rien d' autre qu' un objet JavaScript sérialisé .
De plus , malgré son lien historique ( et technique ) avec JavaScript , JSON reste un format de données structurées , et peut être utilisé facilement par tous les langages de programmation .
Depuis 2009 , les navigateurs commencent à intégrer un support natif du format JSON , ce qui facilite sa manipulation , la sécurité ( contre l' évaluation de scripts malveillants inclus dans une chaine JSON ) , et la rapidité de traitement .
Ainsi les navigateurs Firefox et IE l' intègrent dès la version 3.5 , respectivement 8 .
JScript peut d' ailleurs servir pour scripter une plate-forme Microsoft Windows via Windows Scripting Host ( WSH ) .
On peut encore citer ActionScript , utilisé dans Macromedia Flash qui est aussi une mise en œuvre d' ECMAScript .
JavaScript est enfin utilisé dans la plate-forme de développement Mozilla , sur laquelle sont basés plusieurs logiciels comme des navigateurs Web , pour des tâches relatives à l' interface utilisateur et de communication interne ( exemple : les extensions de Firefox et Thunderbird sont installées à base de fichiers XPI utilisant le JavaScript .
Depuis 2004 , l' objet " js " de l' environnement de programmation graphique Max/MSP , permet d' ouvrir une fenêtre pour programmer en JavaScript , au sein même d' un programme Max/MSP .
Les logiciels ImageJ et CaRMetal sont munis de consoles JavaScript , qui leur permet d' écrire des scripts dans un contexte graphique .
JavaScript est aussi utilisé dans un contenu BIFS pour l' exploitation des événements .
La suite bureautique OpenOffice.org permet d' utiliser JavaScript comme langage de macros .
Un prototype est un objet JavaScript qui est utilisé lors d' un échec de résolution d' un nom sur son objet parent .
En C , chaque instruction se termine par un point-virgule .
JavaScript est plus souple , permettant à une fin de ligne de marquer implicitement la fin d' une instruction .
Les versions récentes de la mise en œuvre du langage JavaScript de SpiderMonkey supportent l' E4X .
Il s' agit d' un support natif de XML ainsi que d' un support natif d' une syntaxe d' accès aux données XML ( sorte de XPath )
