Alain Suguenot , homme politique français , né le 17 septembre 1951 à Troyes ( Aube ) .
Il fait partie du groupe UMP , réélu en 2007 .
Il a été désigné par les militants UMP pour être la tête de liste du parti pour les régionales 2010 en Bourgogne par 1782 voix contre 1317 à Jean-Paul Anciaux .
