Le premier maquis en France est installé dans le massif du Vercors en décembre 1942 .
Durant le Débarquement de Normandie , le maquis et d' autres groupes de résistants jouèrent un rôle non-négligeable , en retardant l' arrivée des renforts allemands .
Les cellules maquisardes prenaient le nom de l' endroit depuis lequel elles opéraient ( ex : le Maquis du Vercors ) , d' un évènement historique ou d' un personnage historique .
Il y eut même , en Lozère , un maquis allemand , dirigé par le communiste Otto Kühne .
Les combats des maquis ne sont qu' une des formes de la Résistance .
