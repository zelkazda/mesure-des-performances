Un temps concurrent d' Epiphany , le navigateur officiel de GNOME , Galeon n' est aujourd'hui plus développé , le projet ayant fini par fusionner avec Epiphany .
Malgré tous ses avantages , Galeon est voué à disparaître .
En effet Epiphany et Galeon étaient en concurrence directe au sein de GNOME car tous les deux basés sur Gecko , et le 22 octobre 2005 ses développeurs ont décidé de s' allier aux développeurs d' Epiphany .
