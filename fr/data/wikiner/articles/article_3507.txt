Charleville-Mézières est une commune française située dans le département des Ardennes et la région Champagne-Ardenne .
La ville est située au nord du département des Ardennes , à 87 km au nord-est de Reims ou 239 km de Paris , à 130 km à l' ouest de Luxembourg , et à 90 km au sud de Charleroi ou 150 km de Bruxelles en Belgique .
Lille , ( puis Arras et Calais ) , est à 2 heures de voiture , ainsi que Metz ( puis Strasbourg ) .
Le TGV Est , avec un raccord à Reims , met Charleville-Mézières à 1 heure 35 ' de Paris depuis juin 2007 .
Une ligne existe vers Givet et la Belgique , mais elle n' est plus utilisée après la frontière : la ligne y est simplement entretenue ( TER puis bus ) .
Mézières quant à elle aurait été fondée en 899 .
Des restaurations furent entreprises ensuite avec notamment des vitraux de René Dürrbach , collaborateur de Pablo Picasso .
En 1521 , Bayard défend la ville de Mézières contre les troupes impériales de Charles Quint .
Il a , en effet , démontré l' importance de la place de Mézières pour la défense du royaume .
La citadelle de Mézières occupe déjà l' accès à la boucle la plus au sud , en rive gauche .
Or , l' activité de Mézières est asphyxiée par le statut de ville de garnison et par la contrainte de commercer avec le royaume .
La nouvelle cité ducale est ainsi destinée à rivaliser avec Sedan , autre capitale princière mais devenue fief protestant .
La cité neuve , élevée selon un plan en damier dit plan hippodamien , construite en ardoises bleues , pierres de taille ocres , briques rouges , devient très vite le nouveau centre économique de l' Ardenne .
Elle deviendra ultérieurement , après la destruction de la citadelle en 1686 en même temps que les fortifications de la cité , le village de Montcy-Saint-Pierre , aujourd'hui intégré à la ville .
En 1870 , elle a été le théâtre proche de la chute du Second Empire à Sedan .
