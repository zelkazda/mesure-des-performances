EverQuest ( EQ ) est un jeu de rôle en ligne massivement multijoueur sorti en mars 1999 .
Il a été développé par Verant Interactive et distribué par Sony Online Entertainment ( SOE ) .
Au-delà de cet aspect de progression mécanique commun à de nombreux jeux de rôle , Everquest , en tant que jeu de rôle en ligne massivement multijoueur , donne accès à un vaste monde virtuel que le joueur peut explorer à sa guise et où il pourra tenir le rôle de son choix , devenir artisan , guerrier ou magicien , jouer en solo ou participer à la vie d' une guilde et se créer un réseau social dense .
EverQuest est lancé le 16 mars 1999 et rencontre au départ quelques difficultés techniques .
Le jeu remporte pourtant un grand succès et dès la fin de l' année dépasse son seul concurrent , Ultima Online , en nombre d' inscriptions .
Ainsi , dans sa structure et ses règles , Everquest est presque un descendant direct sur ordinateur , du célèbre jeu de rôle médiéval-fantastique Donjons et dragons .
L' univers d' Everquest est tellement vaste que peu de joueurs ont réellement visité chacune des 400 zones du jeu .
Même si certaines zones d' Everquest permettent à un joueur solitaire de se débrouiller seul , et même si on peut , à condition de bien choisir sa classe dès le départ , atteindre le niveau maximal sans jamais se grouper avec d' autres joueurs , EQ reste un jeu où il est bien plus intéressant d' interagir avec les autres .
C' est pour cela que le système des " guildes " à une telle importance dans Everquest , en effet , pour explorer les zones les plus ardues , il faut pouvoir réunir de très nombreux joueurs , à la même heure de la journée et pouvoir compter sur eux lors du raid .
Everquest est un jeu qui peut combler différents types de joueurs , les solitaires préférant le solo ou ceux ne jouant qu' en groupe , les débutants et ceux ne pouvant y consacrer que quelques heures par semaines et les acharnés qui à haut niveau , trouveront des challenges dignes de ce nom .
Lorsque l' on débute Everquest , la première décision que l' on doit prendre concerne la création du personnage que l' on incarnera .
L' univers d' EverQuest , est divisé en plus de 200 zones .
Tous les jeux en ligne massivement multijoueurs possèdent certaines expressions qui leur sont propres et Everquest ne déroge pas à la règle .
Pourtant , EQ existant depuis 1999 , les joueurs d ' Everquest ont développé au fur et à mesure des années un langage interne propre .
Pour les non-initiés , et même pour les joueurs habitués aux mmorpg , le langage spécifique pratiqué sur EQ peut être l' équivalent d' une langue étrangère , quasiment incompréhensible .
Le design et le concept même d ' EverQuest est profondément inspiré des ancêtres des MMOG , les multi-user dungeon ou MUD , des jeux de rôle gratuits basés sur des descriptions textuelles qui sont apparus à la fin des années 1970 et qui ont connus leur âge d' or entre le début des années 1980 et le début des années 1990 .
En juin 1997 , EQ est présenté à l' Electronic Entertainment Expo ( E3 ) .
En janvier 1999 , la société Verant Interactive est créée , suite à un désaccord avec les dirigeants de 989 studios .
Le 16 mars 1999 , EverQuest est mis en vente .
Dès la fin de l' année , EverQuest surpasse en nombre de souscriptions , son rival , Ultima Online , sorti un an plus tôt .
EverQuest rencontre alors des problèmes de bande passante , récurrents et entraînant régulièrement des crashs des serveurs .
Ils durent également prendre en compte que grâce à l' Internet , les joueurs forment une véritable communauté , inter-serveur et inter-guilde , et que dès qu' une guilde trouve la façon de résoudre une quête ou la bonne technique pour vaincre un dragon , la solution peut apparaître le jour même sur les forums , accélérant la transmission de l' information et facilitant le travail pour tous les autres joueurs .
Ainsi EverQuest évolue sans cesse , au grès des patchs , particulièrement attendus par toute la communauté , certains espérant des nouveaux sorts ou objets , des nerfs ou des améliorations pour les personnages ou des changements dans certaines zones .
Un autre facteur qui a rendu ces extensions indispensables était le manque d' espace vital qui s' est fait sentir à mesure que de nouveaux joueurs affluaient et que les serveurs devenaient de plus en plus denses en population , suivant le succès d ' EverQuest .
Ces différentes extensions s' intègrent dans le monde d ' EverQuest , sous forme de nouveaux continents et même d' une lune .
EverQuest a une place importante dans la longue histoire des jeux de rôle en ligne massivement multijoueur ( MMORPG ) .
Ainsi en 1984 sort le premier RPG online multijoueur rogue-like payant , en 1988 le premier monde virtuel graphique , en 1991 le premier MMORPG graphique ( Neverwinter Nights ) , en 1992 le premier MMORPG textuel et payant passe sur internet .
En plus de l' évolution technologique , c' est l' arrivée de l' Internet qui va permettre l' apparition de MMORPG au sens moderne du terme ( permettant vraiment l' aspect " massivement multijoueur " , jusque là limité ) .
Internet permet ainsi à des joueurs du monde entier , de fuseaux horaires et de langages différents , de jouer ensemble au même jeu et dans le même monde virtuel .
Avec Ultima Online , les MMORPG entrent dans l' ère moderne .
Ultima Online est bien plus complexe que tous ses prédécesseurs et son succès est sans précédent .
EverQuest , lancé en mars 1999 , différent d ' Ultima Online par sa vue " à la première personne " , en mode subjectif , permettant une immersion dans l' action et dans le monde virtuel sans précédent pour un MMORPG .
L' impression d' être dans le jeu , et non plus de jouer à un jeu peut expliquer en partie le succès d' EverQuest et les problèmes de dépendance qui ont vus le jour par la suite .
EverQuest est le jeu qui apporta réellement le MMORPG dans la zone nord américaine et européenne [ réf. nécessaire ] où il resta pendant 5 années le MMORPG le plus joué [ réf. nécessaire ] , avec de nombreuses extensions , des jeux dérivés et plus de 450000 joueurs .
Fin 1999 , c' est également la mise sur le marché du jeu Asheron 's Call .
La décennie suivante vit l' explosion du genre , avec en 2001 Dark Age of Camelot , MMORPG médiéval orienté " joueurs contre joueurs " et Anarchy Online le premier MMORPG non médiéval fantastique .
En 2003 Eve Online et Star Wars Galaxies situe l' action dans l' espace .
En 2004 , sortent City of Heroes , sur le thème alors inédit des super-héros , et surtout les très attendus World of Warcraft de Blizzard Entertainment et EverQuest II en novembre .
Pourtant , World of Warcraft surpasse rapidement tous ses concurrents et connaît un succès sans précédent .
En janvier 2005 , World of Warcraft en a 750000 , EverQuest II 310000 alors que les chiffres du premier EverQuest restere stable .
En mai 2006 , World of Warcraft est à 6500000 , EverQuest II à 175000 et EverQuest à 200000 .
Actuellement World of Warcraft est le mmorpg le plus joué de la planète , avec quasiment dix millions d' abonnés et plus de 50 % de part de marché .
Les pièces de platine ( pp ) sont la devise que dans le monde d ' EverQuest , les joueurs les utilisent dans le jeu pour effectuer leurs transactions avec les marchands et les autres joueurs .
L' économie d ' EverQuest repose sur deux marchés , le principal étant celui régissant les échanges entre joueurs ( dit a2a , pour avatar to avatar ) , le second entre les joueurs et les marchands non-joueurs ( dit a2b pour avatar to bot ) .
Inversement , l' inflation ( hausse persistante et générale des prix ) peut également menacer l' économie d ' EverQuest .
Avec , initialement , une boîte de jeu aux environs de 46 euros et surtout un abonnement mensuel de 11 euros , EverQuest a rapporté beaucoup d' argent à SOE .
Des estimations non officielles évaluent le volume d' échange de ce marché " underground " de 100 à 800 millions USD par année , les jeux de SOE , EverQuest et EverQuest II se taillant une part de 20 % de ce marché .
Il n' y a pas de dépendance spécifique à Everquest .
Everquest est seulement l' un des premiers jeux du genre à avoir connu un phénomène de cette ampleur .
EverQuest est notamment connu pour être responsable de nombreuses ruptures et divorces .
Sa mère le retrouve alors mort devant son ordinateur , avec EverQuest tournant encore et de nombreuses notes à propos du jeu autour de lui .
Il jouait à EverQuest depuis 2000 et s' y consacrait entièrement , jouant de nombreuses heures par jour malgré son épilepsie .
