En France , les historiens font commencer le XVII e siècle avec l' assassinat du roi Henri IV en 1610 et le font terminer avec la mort de Louis XIV en 1715 .
En 1602 est fondée , sous l' impulsion d' Henri IV , la manufacture nationale des Gobelins .
Louis XIV lance avec Jean-Baptiste Colbert une politique visant à développer le commerce ( mercantilisme ) .
