Il se manie généralement à deux mains , mais certaines techniques , comme la technique à deux sabres de Musashi Miyamoto , ou des techniques impliquant l' utilisation du fourreau , supposent le maniement à une main .
On le retrouve également dans Highlander où Juan Sanchez Villa-Lobos Ramirez ( Sean Connery ) et Connor MacLeod ( Christophe Lambert ) utilisent successivement un même katana au long du film et de ses suites .
On peut également citer Le Dernier Samouraï où Tom Cruise s' initie au combat de sabre à la japonaise et ne tarde pas à mettre , de manière spectaculaire , ses acquis en œuvre .
Il y a également Zatoichi , dans lequel Takeshi Kitano campe un masseur aveugle , redoutable expert du sabre .
Dans Soleil rouge avec Toshirō Mifune , l' art des samouraïs et le maniement du sabre sont mis en valeur .
