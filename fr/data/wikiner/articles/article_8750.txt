Se rendant compte qu' une famine menaçait les mortels , Zeus se décida à envoyer Hermès au royaume d' Hadès pour lui demander de rendre Perséphone à sa mère .
Mais Perséphone avait mangé six pépins de la grenade offerte par Hadès en guise de dernière ruse pour la garder avec lui , et la tradition voulait que quiconque mangeant dans le royaume des morts ne puisse le quitter .
Mais Déméter n' eut pas que Perséphone .
Le héros Iasion s' unit à elle dans un champ labouré trois fois et lui donna un fils qui fut appelé Ploutos et devint la personnification de la richesse .
La légende rapporte qu' ayant conçu Despina durant sa quête de Perséphone , Déméter la fit élever par un Titan du nom d' Anytos .
Déméter enseigna aux humains le travail des semis et du labour .
Pour le remercier de son accueil , elle prit avec elle les fils du roi , Démophon et Triptolème , tenta de rendre le premier immortel et enseigna au second l' art de l' agriculture .
Dans Les Travaux et les Jours , Hésiode revient fréquemment sur Déméter , et il y donne de nombreux détails sur les rites religieux entourant la fertilité et le travail de la terre .
Déméter fut honorée dans les mystères d' Éleusis , un culte célébrant le retour à la vie et le cycle des moissons .
Déméter est souvent représentée assise et portant une gerbe d' épis de blé tressés .
Sa fille Perséphone apparaît assez souvent à ses côtés .
