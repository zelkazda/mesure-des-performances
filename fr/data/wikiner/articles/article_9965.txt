Brahmagupta est l' un des plus importants mathématiciens tant de l' Inde que de son époque .
C' est dans son premier ouvrage le Brahmasphutasiddhanta , qu' il définit le zéro comme résultat de la soustraction d' un nombre par lui-même , qu' il décrit les résultats d' opérations avec ce nouveau nombre , mais se " trompe " en donnant comme résultat zéro à 0/0 , par contre il donne des règles correctes sur les signes lors d' opérations entre entiers relatifs ( profits et pertes ) .
Brahmagupta fut le premier mathématicien à utiliser l' algèbre pour résoudre des problèmes astronomiques .
