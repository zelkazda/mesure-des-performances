Le World Wide Web Consortium , abrégé par le sigle W3C , est un organisme de standardisation à but non-lucratif , fondé en octobre 1994 comme un consortium chargé de promouvoir la compatibilité des technologies du World Wide Web telles que HTML , XHTML , XML , RDF , CSS , PNG , SVG et SOAP .
Le W3C a été fondé par Tim Berners-Lee lorsqu' il a quitté l' Organisation européenne pour la recherche nucléaire ( Cern ) en octobre 1994 .
Le W3C a créé de nombreux bureaux régionaux dans le monde .
En octobre 2007 on compte 16 bureaux du W3C dans les différentes régions du monde qui couvrent l' Australie , le Luxembourg , la Belgique , les Pays-Bas , la Chine , l' Allemagne , la Finlande , la Grèce , la Hongrie , l' Inde , l' Irlande , Israël , l' Italie , le Maroc , la Corée , l' Afrique du Sud , l' Espagne , la Suède , et la Grande-Bretagne .
Le W3C est supervisé par son fondateur Tim Berners-Lee , le principal créateur du protocole URL , de l' HTTP , et de l' HTML .
Le W3C publie aussi des remarques informatives qui ne sont pas destinées à être traitées en tant que norme .
Contrairement à l' Organisation internationale de normalisation ou d' autres corps internationaux de standardisation , le W3C ne possède pas de programme de certification .
Cependant les spécifications techniques du W3C définissent la conformité de façon formelle par l' intermédiaire de la section conformité et de l' emploi de la RFC 2119 .
Le W3C supervise le développement d' un ensemble de standards ( liste incomplète ) :
Le W3C a environ 70 employés comprenant des personnes en charge des groupes de travail , du personnel administratif , des administrateurs systèmes , des responsables communication .
Ils sont garants du bon respect de la charte de fonctionnement du W3C .
