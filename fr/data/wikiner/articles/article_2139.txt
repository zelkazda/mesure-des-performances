Yabasic permet la création d' exécutables mais , lors d' une telle action , le fichier n' est pas compilé mais combiné à l' interpréteur .
Par conséquent , le code source ne peut pas être caché et peut toujours être visualisé à l' aide d' un éditeur de texte adapté ( comme SciTE ou Notepad++ ) .
