Ce nom est souvent abrégé pour des besoins de communication , mais aussi quelques fois dans des actes légaux ou diplomatiques en ordre souverain militaire de Malte , ordre souverain de Malte ou plus couramment ordre de Malte .
Il y a un désaccord sur le point de savoir si l' ordre souverain de Malte actuel " est " ou " n' est pas " l' ordre de Saint-Jean de Jérusalem , s' il en " prend " la suite ou s' il " reprend " sa tradition et son histoire .
Il faut attendre la création en 1864 d' associations nationales ou de prieurés , le rétablissement de la charge de grand maître par le pape Léon XIII et l' élection de Giovanni Battista Ceschi a Santa Croce en 1879 , la reconnaissance de la souveraineté de l' organisation par le tribunal cardinalice en 1953 et l' approbation en 1961 par le pape Jean XXIII de la charte constitutionnelle pour permettre aujourd'hui à l' ordre de Malte d' exister officiellement et canoniquement .
La volonté de l' ordre de Malte n' aura de cesse de " reprendre " la tradition de l' ordre de Saint-Jean de Jérusalem [ précision nécessaire ] malgré l' éclatement de l' ordre historique en plusieurs ordres hospitaliers .
L' ordre de Malte se considère lui-même comme simplement " issu " des Hospitaliers et non comme " étant " les chevaliers de l' ordre de Saint-Jean de Jérusalem .
L' ordre de Saint-Jean de Jérusalem est chassé de Malte par Bonaparte qui récupère la souveraineté de l' archipel .
Le grand maître Ferdinand de Hompesch demande au tsar de Russie Paul I er de devenir le protecteur de l' ordre .
Au décès de Paul I er , en 1801 , son fils Alexandre I er de Russie , conscient de cette irrégularité , décide de rétablir les anciens us et coutumes de l' ordre catholique des Hospitaliers , par un édit du 16 mars 1801 par lequel il laisse les membres profès libres de choisir un nouveau chef .
Depuis 1805 , à la mort de Giovanni Battista Tommasi , aucun grand maître n' est élu à la tête de l' ordre .
C' est sur ces ruines que va se reconstruire l' Ordre de Malte .
En 1879 , le pape Léon XIII rétablit la dignité de " grand maître " qui était vacante avec le nomination de Giovanni Battista Ceschi a Santa Croce .
Et c' est finalement une nouvelle charte constitutionnelle qui est promulguée par les membres de l' ordre de Malte et approuvée le 27 juin 1961 par le pape Jean XXIII ce qui met un terme à dix ans de tumultes .
Il dispose également d' un siège d' observateur permanent auprès des Nations unies , de la Commission européenne et des principales organisations internationales .
On dénombre environ 12500 membres de l' ordre de Malte et 80000 bénévoles réguliers à travers le monde qui font vivre les activités hospitalières .
À cette occasion en France , les membres de l' association française des membres de l' ordre souverain de Malte se réunissent au château de Versailles .
Aujourd'hui , en France , on compte par exemple en France : 3 chevaliers profès ( qui prononcent des vœux évangéliques : obéissance , chasteté et pauvreté ) et un chevalier à vœux simples sur 542 chevaliers laïcs dont 23 chevaliers en obédience ( qui prononcent une promesse d' obédience ) [ réf. nécessaire ] .
L' ordre a aussi toujours officiellement un caractère militaire -- même s' il n' est plus armé -- et catholique mais il a surtout conservé sa mission hospitalière : " secourir et soigner " ; ce qui fait de l' ordre de Malte le plus ancien organisme humanitaire ( environ 900 ans d' âge ) .
En France , l' ordre de Malte n' est pas reconnu comme réellement souverain et ne dispose pas à Paris d' un ambassadeur mais d' un " représentant officiel auprès de la France " .
L' ordre de Malte est présent dans plus d' une centaine de pays en permanence et ses activités diplomatiques l' amènent à intervenir lors de catastrophes naturelles ou lors de conflits armés comme en 1969 au Biafra , au Vietnam en 1974 , en Ouganda en 1980 mais aussi , par exemple , en Yougoslavie dans les années 1995 -- 1999 .
D' autres opérations à caractère médical et d' assistance ont eu lieu au Zimbabwe et en Angola en 2002 , en Iraq en 2002 -- 2003 .
Son siège est situé à Genève en Suisse .
L' ordre de Malte examine chaque année environ 300 demandes d' admission .
De nombreuses critiques sont régulièrement formulées sur l' actuel Ordre de Malte .
Elles sont résumées par Philippe du Puy de Clinchamps : " L' ordre dit de Malte , hors quelques œuvres charitables , ( notamment dans la lutte contre la lèpre ) , dépense surtout ses forces pour des questions de préséances et dans des querelles intestines .
On sait qu' après avoir exigé seize quartiers de noblesse , c' est-à-dire la noblesse des seize trisaïeuls du futur chevalier , l' ordre de Malte ramena cette exigence à huit quartiers , la noblesse des huit bisaïeuls .
Il reste permis de racheter un défaut de noblesse par quelque aumône ; et Malte accueille ainsi dans son sein des chevaliers aux quartiers quasi tous roturiers y compris le pourtant indispensable quartier noble paternel .
Cependant comme aux yeux de la société , assez restreinte il est vrai , qui s' intéresse encore à cette sorte d' association sans être très au fait de leur état réel , ordre dit de Malte sous-entend noblesse , on y entre pour paraître noble sans l' être le moins du monde et ceci , en France tout particulièrement ; certains pays , et notamment ceux d' Europe centrale , sont restés relativement exigeants quant aux preuves de noblesse à fournir .
Néanmoins , en matière postale stricte , les timbres émis par l' ordre ne sont pas reconnus par l' Union postale universelle ( UPU ) .
