Le ragga , abréviation de ragamuffin : ( " va-nu-pieds " , " galopin " ) , est un genre musical issu du mouvement dancehall reggae et apparu en Jamaïque à la fin des années 1980 , caractérisé par une diction répétitive rappelant les toasters .
C' est à partir de cette période que toute une génération de chanteurs représentatifs du style raggamuffin va tenir la tête de l' affiche en Jamaïque .
Le style ouvrira donc la voie au style " dancehall " actuel représenté par des artistes comme Sean Paul , entre autres .
