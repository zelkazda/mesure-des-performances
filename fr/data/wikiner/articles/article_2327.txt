Premier ministre de 1988 à 1991 sous la présidence de François Mitterrand , il est député ( PSE ) au Parlement européen de 1994 à 2009 .
Il est licencié ès lettres , diplômé de l' Institut d' études politiques de Paris et ancien élève de l' École nationale d' administration .
Michel Rocard est issu d' une famille de la bourgeoisie , protestant du côté de sa mère , catholique du côté de son père .
Il est également apparenté à l' actrice Pascale Rocard .
Entré en 1947 à l' Institut d' études politiques de Paris , Michel Rocard adhère en 1949 aux Jeunesses socialistes .
Il entre à l' ENA en 1955 .
Alors que se déclare la guerre d' Algérie , il rejoint les socialistes en rupture avec Guy Mollet à propos de la politique algérienne .
En fait , cette politique fut proclamée et jamais appliquée " , écrivait Michel Rocard dans un rapport rédigé en 1957 .
Michel Rocard prend , à partir de 1953 le pseudonyme de " Georges Servet " ( du nom d' un hérétique protestant , Michel Servet ) et c' est sous ce nom qu' il est connu au PSU avant 1967 .
Il partage également le nom de plume " Jacques Malterre " avec Hubert Prévot .
Après la défaite du général de Gaulle lors du référendum du 27 avril 1969 , Michel Rocard en appelle à " un pouvoir de transition vers le socialisme " .
Il se présente à l' élection présidentielle de 1969 , où il recueille un score de 3,61 % des suffrages exprimés -- le meilleur score obtenu par le PSU depuis sa création -- , le candidat socialiste Gaston Defferre , allié à Pierre Mendès France , n' obtenant que 5 % .
Lors de la campagne présidentielle d' avril-mai 1974 il soutient François Mitterrand , mais en octobre , lorsqu' il propose au PSU de rejoindre le PS , il est mis en minorité ( 40 % ) .
Son entrée au Parti socialiste date de décembre 1974 .
L' antagonisme qui couvait entre les deux hommes depuis au moins 1977 durera jusqu' à la mort de Mitterrand en 1996 .
La fin des années 1970 marque l' avènement du rocardisme , un courant au sein du Parti socialiste qui affiche une tendance décentralisatrice , puis ouvertement hostile aux nationalisations intégrales ( à 100 % ) préparées par les mitterrandistes ( congrès de Metz , 1979 ) .
Michel Rocard , dont la cote a toujours été élevée dans les sondages , devient ainsi une figure incontournable du paysage politique français .
Les rocardiens se réclament souvent de la pensée de Pierre Mendès France lorsqu' ils prônent une politique économique " réaliste " et une culture de gouvernement .
Le 26 juin 1988 , Michel Rocard fait signer les accords de Matignon entérinant les droits de la Nouvelle-Calédonie à l' autodétermination et mettant fin aux violences sur l' île .
Cette action pour la pacification de la Nouvelle-Calédonie est , selon lui , ce qu' il a fait de mieux au gouvernement [ réf. nécessaire ] , mais c' est aussi l' action pour laquelle il dit avoir subi les pires attaques [ réf. nécessaire ] .
Il remanie profondément les institutions dirigeantes du parti , donnant notamment son autonomie au MJS .
Michel Rocard doit alors abandonner l' idée d' une deuxième candidature présidentielle que les médias lui prêtent depuis 1981 .
Il est élu sénateur des Yvelines le 24 septembre 1995 .
Il occupe plusieurs fonctions au Parlement européen en présidant les commissions de la coopération et du développement ( 1997-1999 ) , puis de l' emploi et des affaires sociales ( 1999-2002 ) et enfin de la culture ( 2002-2004 ) .
Il s' en prend également à l' association altermondialiste ATTAC .
Michel Rocard est un fervent défenseur de l' entrée de la Turquie dans l' Union européenne .
Il a d' ailleurs écrit un plaidoyer concernant ce sujet : selon lui , " la Turquie est un enjeu stratégique " et " une vraie chance pour l' Europe " .
Michel Rocard compte également parmi les membres fondateurs du Collegium international éthique , politique et scientifique , association qui réfléchit aux transformations urgentes qu' imposent les désordres actuels dans le monde .
Ce dernier avait joué un rôle déterminant dans la bataille au Parlement européen contre la brevetabilité du logiciel .
Le 13 avril 2007 , neuf jours seulement avant le premier tour , il se prononce dans Le Monde pour un accord Royal -- Bayrou afin de battre la " coalition de Nicolas Sarkozy et Jean-Marie Le Pen " , sans toutefois préciser les modalités de cet éventuel accord .
Il est suivi dans sa démarche par Bernard Kouchner , le lendemain .
Michel Rocard révèle , en juillet 2007 , avoir tenté en vain de convaincre Ségolène Royal de se désister en sa faveur un mois avant le premier tour de l' élection présidentielle , estimant que ce changement de candidat aurait pu éviter à la gauche la défaite à venir .
Le 30 juin 2007 , en voyage en Inde , il est victime d' une hémorragie cérébrale et transféré , dans un état grave , au sein du service de neurochirurgie de l' hôpital de Calcutta .
Sorti de cet hôpital le 10 juillet pour être rapatrié en France , il déclare que les chirurgiens ont " fait des miracles " pour lui permettre de se rétablir .
Il va jusqu' à menacer de quitter le Parti socialiste en cas de victoire de Ségolène Royal .
Finalement , c' est Martine Aubry qui est élue face à cette dernière .
Le 14 janvier 2009 , le porte-parole du groupe socialiste au Parlement européen annonce la démission de Michel Rocard de son mandat de député pour la fin du mois de janvier et son retrait de la vie politique active .
Le lendemain , il est salué par une ovation des députés du Parlement européen .
Avec Alain Juppé , il co-préside la commission chargée de réfléchir à la mise en œuvre d' un grand emprunt national , qui devrait être lancé début 2010 .
