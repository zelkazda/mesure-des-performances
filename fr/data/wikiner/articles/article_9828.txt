Son principe , énoncé par Leonard Adleman en 1994 , " consiste à coder une instance du problème avec des brins d' ADN et à les manipuler par les outils classiques de la biologie moléculaire pour simuler les opérations qui isoleront la solution du problème , si celle-ci existe "
Des premiers résultats ont été obtenus par Leonard Adleman ( NASA , JPL )
