Il inaugure , avec Samuel Taylor Coleridge , la période romantique de la littérature anglaise lors de la publication de Lyrical Ballads ( 1798 ) .
Wordsworth entre au St John 's College de Cambridge en 1787 .
En 1790 , il se rend en France et soutient les républicains de la Révolution française .
Il obtient son diplôme l' année suivante , sans mention , puis part pour un tour d' Europe incluant les Alpes et l' Italie .
La même année , le manque d' argent le contraint à retourner , seul , en Grande-Bretagne .
Accusé d' être girondin sous la la Terreur , il prend ses distances avec le mouvement républicain français ; de plus , la guerre entre la France et la Grande-Bretagne l' empêche de revoir sa femme et sa fille .
Il rencontre Samuel Taylor Coleridge dans le Somerset cette même année .
Ensemble , ils publient Lyrical Ballads ( 1798 ) , qui s' avère d' importance capitale pour le mouvement romantique en Grande-Bretagne .
Pour un temps , Wordsworth et Coleridge se sont éloignés l' un de l' autre en raison de l' addiction de ce dernier à l' opium .
Elle est énoncée par Wordsworth dans la préface qu' il a rédigée pour la seconde édition des Lyrical Ballads ( " Les Ballades lyriques " ) parue en 1801 .
Cette simplicité recherchée conduit Wordsworth à privilégier les événements de la campagne .
Les thèmes choisis par Wordsworth relèvent de ce qu' il appelle animal sensations et moral sentiments , c' est-à-dire la perception de la nature dans ses aspects élémentaires et tous les sentiments et toutes les passions de l' homme .
L' œuvre poétique de Wordsworth est immense :
C' est d' ailleurs sur ces mêmes vers que se termine le film , avec le visage et la voix off de Natalie Wood ( qui partage l' affiche du film avec Warren Beatty ) .
