César était l' un des titres des empereurs romains , les situant dans la continuité de Jules César .
Suétone a écrit la Vie des douze Césars .
Constantin I er réutilisa le titre , mais pour donner un statut impérial à ses fils et les installer dans certaines régions de l' empire afin de l' y représenter .
Puis Manuel Ier Comnène le rétrograde en 4 e place en accordant le titre de despote .
