La ville est située au pied des premières hauteurs du Massif armoricain , sur l' Odon , à quelques kilomètres de sa source .
Elle est ensuite prolongée jusqu' à Vire le 1 er juin 1891 .
Le transport des voyageurs sur la ligne Caen -- Vire est interrompu le 1 er mars 1938 .
Le transport de marchandises est par la suite limité à Jurques , puis définitivement suspendu .
En 1895 , Aunay devient Aunay-sur-Odon .
C' est à sa position de carrefour , cette fois entre Caen et Vire et entre Bayeux et Falaise , que la ville doit les deux bombardements stratégiques alliés entre le 11 et le 15 juin 1944 .
Aunay-sur-Odon dispose d' une école maternelle publique et d' une école élémentaire publique .
L' enseignement secondaire y est transmis au sein du collège Charles Lemaître qui accueille également une section d' enseignement général et professionnel adapté .
Aunay-sur-Odon avait compté jusqu' à 2102 habitants en 1851 puis la population était redescendu à 1145 ( 1946 ) .
