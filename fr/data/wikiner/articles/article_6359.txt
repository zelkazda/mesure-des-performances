Justine Henin passe son premier test officiel en compétition alors qu' elle n' a que six ans et demi .
Elle est la première Belge depuis la création du tournoi junior , en 1947 , à décrocher l' épreuve [ réf. nécessaire ] .
1999 marque les débuts de Justine Henin dans la carrière des joueuses professionnelles .
Après s' être extirpée des qualifications sans perdre un set , elle passe le premier tour , puis tombe face à Lindsay Davenport ( numéro deux mondiale ) , non sans enlever le deuxième set .
Elle réalise sa meilleure performance en septembre , à l' occasion du tournoi de Luxembourg , où elle s' incline en quarts contre sa compatriote Sabine Appelmans .
À l' issue de l' année , Justine Henin pointe au 69 e rang du classement WTA .
2000 voit Justine Henin poursuivre son apprentissage et se forger de l' expérience .
À l' Open d' Australie , elle perd au deuxième tour contre la numéro un mondiale , Martina Hingis .
En août , elle franchit une nouvelle étape : pendant la tournée américaine sur dur , elle se qualifie pour le deuxième tour de l' Open du Canada et de New Haven .
Bien que n' ayant gagné aucune épreuve , Justine Henin termine 48 e mondiale .
Les efforts sur le dur de l' an 2000 payent enfin avec les premiers tournois de l' année gagnés en Australie , Canberra et Gold Coast .
La saison de terre battue débute également mal , Justine n' arrivant même pas en finale sur trois tournois .
Mais à Roland-Garros , un tableau dégagé permet à Justine d' arriver en demi-finale .
À nouveau face à Kim , elle gagne le premier set puis craque mentalement .
Justine enchaîne cependant immédiatement avec le tournoi sur herbe de Bois-le-Duc , qu' elle remporte face à Clijsters .
En finale , elle se retrouve contre Venus Williams , la spécialiste du gazon des années 2000 : elle perd avec les honneurs en trois sets .
Après ces succès , l' US Open la voit échouer à nouveau sur cette surface en huitième , contre la redoutable Serena Williams , qui ira jusqu' en finale .
Au terme de la saison , Justine a gagné des tournois sur toutes les surfaces sauf la terre battue , et elle a atteint la septième place mondiale .
Par ailleurs , avec Kim Clijsters , Laurence Courtois et Els Callens , elle offre également la Fed Cup , l' équivalent féminin de la Coupe Davis de tennis , à la Belgique .
La saison sur dur du début d' année commence par son premier quart de finale à l' Open d' Australie , battue une nouvelle fois par Clijsters .
C' est un désastre : elle perd au premier tour face à Anikó Kapros sur le score de 6-4 , 1-6 , 0-6 .
Mais comme en 2001 , elle est rapidement remise pour le gazon et est double demi-finaliste à Bois-le-Duc et Wimbledon , battue ici encore par une Williams en deux sets , après avoir battu Seles pour la première fois en quart de finale .
À l' US Open , elle déçoit en étant éliminée en huitièmes contre Daniela Hantuchová et ne gagnera qu' un deuxième tournoi en fin d' année , le Tournoi de Leipzig , mais sans tête d' affiche .
En janvier , lors des huitièmes de finale de l' Open d' Australie , elle s' impose face à Lindsay Davenport en trois sets au terme d' un match épique ( 7-5 , 5-7 , 9-7 ) .
C' est la première fois que Justine arrive à prendre l' avantage sur une joueuse qui jusque là avait pu tabler sur son expérience dans les cinq rencontres qui les avaient opposées .
L' accès à la finale lui sera cependant barré par Venus Williams .
Pour arriver à ce stade , elle a été contrainte de livrer en demi-finale un éprouvant combat contre l' Américaine Jennifer Capriati .
Bien que prise de crampes , Justine ne baissa jamais les bras et refusa de s' incliner .
Dans la nuit qui suivit le match , Justine dut être mise sous perfusion .
Le 20 octobre , elle ponctue sa saison en supplantant Kim Clijsters au rang de numéro un mondiale .
Lors du tournoi d' Amelia Island , Justine découvre qu' elle souffre d' hypoglycémie et s' incline en demi-finale face à Amélie Mauresmo après s' être battue pendant 3 sets .
Des examens médicaux réalisés en Belgique montreront qu' elle a contracté un cytomégalovirus , dont les conséquences feront qu' elle sera écartée des courts pendant plusieurs mois .
Justine a confié qu' il lui arrivait pendant cette période de dormir 18 heures par jour et d' être dénuée de toute force physique .
Elle décide ensuite de déclarer forfait pour le tournoi de Wimbledon .
La saison commence mal , puisque blessée au genou ( tendinite ) , Justine Henin est contrainte à déclarer forfait pour le tournoi de Sydney et l' Open d' Australie .
Ce n' est donc que le 25 mars 2005 que Justine retourne sur le circuit lors du NASDAQ-100 Open de Miami .
Malgré plus de 6 mois d' inactivité , elle ne s' incline qu' en quarts de finale face à la numéro deux mondiale , Maria Sharapova .
De fait elle rafle tout sur cette surface , à Varsovie , l' Open d' Allemagne , malgré de nombreux matchs en trois sets ( 8 matchs sur 17 ) .
Mais cette année Justine a eu l' intelligence d' espacer ses tournois : c' en est terminé des années marathons , une philosophie qu' elle gardera sienne pour se préserver de ses trop nombreux pépins [ réf. nécessaire ] .
Face à Svetlana Kuznetsova , sa plus dangereuse rivale sur cette surface , Justine sauve deux balles de match et finit par s' imposer en 3 manches après 3 h 15 .
Mise en confiance , elle bat facilement Nadia Petrova en demi-finale et le 4 juin 2005 , elle ajoute une deuxième fois son nom au palmarès des vainqueurs de Roland-Garros en battant Mary Pierce en finale par un score sans appel de 6-1 , 6-1 .
Elle décide ensuite de déclarer forfait pour le tournoi de San Diego .
Mais elle doit s' incliner face à la future vainqueur de l' US Open , Kim Clijsters ( 5-7 , 1-6 ) .
En septembre , Justine est désignée tête de série n° 7 à l' US Open .
Toujours en septembre , elle est contrainte de déclarer forfait pour le tournoi de Luxembourg , souffrant encore des ischio-jambiers .
Après cette contre-performance , elle jette l' éponge sur les tournois de Zurich et de Linz et met un terme à sa saison 2005 le 31 octobre sans participer ( bien que mathématiquement qualifiée ) aux Masters , épreuve finale ponctuant le calendrier féminin où les 8 meilleures du monde s' affrontent pour le titre .
En janvier 2006 à l' Open d' Australie Justine , tête de série numéro 8 , s' incline en finale face à Amélie Mauresmo : elle abandonne dans le deuxième set alors qu' elle était menée 6-1 , 2-0 , se plaignant de problèmes gastriques , probablement causés par la prise trop importante d' anti-inflammatoires pour lutter contre des douleurs récurrentes à l' épaule droite .
Trois semaines après , elle est à Dubaï et pour la 3 e fois en autant de participations , Justine le remporte face à Maria Sharapova et s' adjuge ainsi le 25 e titre de sa carrière .
Cette nouvelle victoire lui permet de grimper d' une place au classement WTA lors de sa parution le lundi 27 février .
Justine compte désormais 12 victoires consécutives à Dubaï .
Malgré une gêne au genou et des douleurs dorsales , elle participe à l' Open de Miami , mais est battue au deuxième tour par Meghann Shaughnessy .
À Charleston , Justine s' incline en demi-finale du tournoi qu' elle avait remporté en 2003 et 2005 .
Pour son retour en Fed Cup à Liège , Justine s' impose face à Nadia Petrova au cours d' un match de près de trois heures ( 6-7 , 6-4 , 6-3 ) et remporte le tournoi le lendemain , en battant Elena Dementieva ( 6-2 , 6-0 en 1 heure et 11 minutes de jeu ) .
Justine décide ensuite de ne pas aller à Varsovie et reprend la compétition à Berlin où elle échoue en finale face à Nadia Petrova en 3 sets .
C' est en tant que 5 e tête de série que Justine aborde Roland-Garros .
Elle passe sans encombre les 3 premiers tours , se défait d' Anastasia Myskina en 2 sets puis de Anna-Lena Groenefeld .
En demi-finale , elle affronte Kim Clijsters .
Présenté comme une finale avant la lettre , le match déçoit quelque peu et Justine l' emporte 6-3 , 6-2 .
Le 24 juin 2006 , Justine décroche le 27 e titre de sa carrière en battant Anastasia Myskina en finale du tournoi de Eastbourne sur le score de 4-6 , 6-1 et 7-6 ( 5 ) .
Justine déclare ensuite forfait pour les demi-finales de la Fed Cup contre les États-Unis et pour le tournoi de Montréal en raison d' une blessure au genou .
Elle reprend la compétition à New Haven où elle bat Medina , Santangelo , Kuznetsova pour atteindre la finale .
Elle y affronte l' Américaine Lindsay Davenport , contrainte d' abandonner à cause de douleurs persistantes à l' épaule droite .
Grâce à ce 5 e titre de la saison , Justine gagne une place au classement WTA et occupe la deuxième place derrière Amélie Mauresmo .
Elle est battue en finale par Maria Sharapova ( 4-6 , 6-4 , 6-4 ) .
Aux Masters , à Madrid , malgré des soucis physiques , Justine termine néanmoins deuxième de son groupe qualificatif et bat Maria Sharapova en demi-finale ( 6-2 , 7-6 ) .
En finale , elle s' impose face à Amélie Mauresmo ( 6-4 , 6-3 ) et remporte pour la première fois les Masters .
Seul le titre à Wimbledon manque encore à son palmarès .
Lors de cette saison 2006 , Justine a engrangé 4204810 de dollars en prize money .
Justine est la première femme à être ainsi distinguée , elle rejoint des sportifs prestigieux , tels que le judoka David Douillet , le pilote de Formule Un Michael Schumacher ou le perchiste Sergueï Boubka .
Ce forfait lui vaut la perte de la place de n° 1 mondiale au profit de Maria Sharapova .
Justine reprend la compétition à l' Open Gaz de France à Paris où elle atteint les demi-finales où elle est battue par Lucie Šafářová 7-6 , 6-4 .
Déjà victorieuse en 2003 , 2004 et 2006 , Justine remporte ainsi le tournoi de Dubaï pour la quatrième fois où elle y est invaincue en 16 rencontres .
La semaine suivante , Justine enchaîne avec le tournoi de Doha , où elle remporte la finale du tournoi face à Svetlana Kuznetsova .
La Belge qui disputait sa 46 e finale décroche son 2 e titre en 2007 , une semaine après son succès à Dubaï .
Justine reprend la place de n° 1 mondiale à la russe Maria Sharapova à l' issue du tournoi d' Indian Wells .
A Miami , épreuve sur surface dure dotée de 3,45 millions de dollars , elle s' incline en finale et en trois sets ( 0-6 , 7-5 , 6-3 ) face à Serena Williams en 3 sets , alors qu' elle a disposé de deux balles de match en menant 6-0 , 5-4 , 40-15 .
À Berlin par contre , elle est éliminée en demi-finale ( 3 sets 6-4 , 5-7 , 6-4 ) par Svetlana Kuznetsova , joueuse qu' elle a déjà battu 14 fois en 15 confrontations .
Henin devient ainsi la 5 e joueuse depuis 1925 à décrocher 4 victoires ou plus à Roland-Garros , restant toutefois à bonne distance de l' Américaine Chris Evert , qui en compte sept à son actif .
Elle remporte ensuite l' US Open contre Svetlana Kuznetsova ( 6-1 , 6-3 ) sans avoir perdu un seul set , et en ayant battu les sœurs Williams , respectivement en quart ( Serena ) et en demi-finale ( Venus ) .
Elle ne perd aucun de ses matchs de poules , battant notamment Marion Bartoli ( 6-0 , 6-0 ) , 9ème mondiale , qui avait battu Justine , à Wimbledon .
En demi-finales , Justine bat Ana Ivanović en deux sets , et remporte son deuxième Masters consécutif au terme d' un match de 3 heures et 25 minutes face à Maria Sharapova .
Cette finale contre Maria Sharapova est considérée comme l' un des plus beaux matchs de l' histoire du tennis féminin .
La championne belge débute l' année en remportant pour la troisième fois de sa carrière les internationaux de Sydney au terme d' une finale de 2h18 de jeu contre Svetlana Kuznetsova décrochant ainsi le 40 e titre de sa carrière et le sixième d' affilée sur le circuit .
À l' Open d' Australie , elle s' incline en quart de finale devant Maria Sharapova interrompant une série de 32 matches sans défaite .
Elle remporte le 17 février 2008 le Tournoi d' Anvers contre Karin Knapp sans parvenir à développer son meilleur tennis .
A Miami , après un début de tournoi sans problème , Justine subit la loi de Serena Williams en 1/4 de finale sur le score sans appel de 0-6 , 2-6 .
Au tournoi de Berlin , elle s' incline au 3 e tour ( 7-5 , 3-6 , 1-6 ) contre Dinara Safina , 17 e joueuse mondiale .
Le 14 mai 2008 , à la surprise générale , Justine Henin annonce lors d' une conférence de presse qu' elle met fin à sa carrière .
Début décembre , la Belge retrouve le goût de la victoire lors d' une exhibition à Charleroi , en battant sa compatriote Kirsten Flipkens en deux sets ( 6-4 , 6-4 ) , puis Flavia Pennetta , 12 e mondiale , sur le même score .
Une semaine plus tard , elle disputait un autre match exhibition au Caire contre Nadia Petrova , 20 e mondiale à la WTA , qu' elle a battue en deux sets : 7-6 4 , 6-2 .
Elle débute sa saison au tournoi de Brisbane ( Australie ) et se qualifie pour la finale après avoir battu successivement Nadia Petrova , Sesil Karatantcheva , Melinda Czink et Ana Ivanović .
Elle finit par s' incliner contre sa compatriote Kim Clijsters , sur le score serré de 3-6 , 6-4 , 6-7 6 .
Dans la foulée , elle déclare forfait pour les Internationaux de Sydney .
Bénéficiant d' une wild card à l' Open d' Australie , elle élimine au premier tour sa compatriote Kirsten Flipkens , puis la n o 5 mondiale Elena Dementieva ( 7-5 , 7-6 6 ) au terme d' un match de 2 h 50 .
Elle enchaîne par deux victoires en trois sets face à Alisa Kleybanova et Yanina Wickmayer , puis passe l' obstacle Nadia Petrova en deux sets serrés ( 7-6 3 , 7-5 ) en quarts de finale .
Elle s' incline néanmoins en demi-finale face à Kim Clijsters , future vainqueur du tournoi ( 6-2 , 6 3 -7 , 7-6 6 ) .
À Stuttgart , elle s' impose en finale contre l' Australienne Samantha Stosur sur le score de 6-4 , 2-6 , 6-1 mais cette dernière la bat un mois plus tard au troisième tour de Roland-Garros .
À Wimbledon , Justine gagne ses trois premiers tours en deux sets , mais finit par s' incliner , pour la troisième fois de la saison , face à sa compatriote Kim Clijsters en huitièmes de finale ( 2-6 , 6-3 , 6-3 ) .
Le 30 novembre 2007 , en présence de sa famille et de Rudy Demotte , Justine Henin a inauguré à Limelette , en Brabant wallon , son nouveau club de tennis , ouvert à des joueurs de tous niveaux .
Justine Henin a effectué ses débuts d' actrice dans le feuilleton Plus belle la vie le 8 mai 2009 .
L' astéroïde ( 11948 ) Justinehénin porte le nom de la joueuse de tennis Justine Henin .
