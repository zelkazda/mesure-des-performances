Elle est la principale commune de l' agglomération Maubeuge Val de Sambre et de l' arrondissement d' Avesnes-sur-Helpe .
Maubeuge se trouve entre Valenciennes et Lille au nord-ouest , Charleroi et Liège au nord-est , Mons et Bruxelles au nord , Laon et Paris au sud .
La ville , baignée par la Sambre navigable , est une porte du Parc naturel régional de l' Avesnois .
C' est la plus importante commune de l' arrondissement d' Avesnes-sur-Helpe , au cœur d' une agglomération de plus de 120 000 habitants comprenant notamment les communes de Hautmont , Jeumont , Louvroil , Feignies , Rousies , Ferrière-la-Grande et Aulnoye-Aymeries .
Il s' agit d' une agglomération transfrontalière puisque le tissu urbain est continu de part et d' autre de la frontière franco-belge , entre les villes de Jeumont ( France ) et d' Erquelinnes ( Belgique ) .
La grande ville la plus proche est d' ailleurs la ville belge de Mons , chef-lieu de la province de Hainaut , avec laquelle Maubeuge partage une politique culturelle commune .
La Première Guerre mondiale va éprouver à nouveau la cité sambrienne .
En 1918 , elle est délivrée par les Britanniques .
L' installation de ces ateliers donne un second souffle à l' activité industrielle de Maubeuge .
Le 3 août 2008 , en début de nuit , une tornade s' est abattue sur Boussières-sur-Sambre , Hautmont , Neuf-Mesnil et Maubeuge , provoquant d' importants dégâts .
À Maubeuge , 679 habitations , dans un secteur extrêmement diffus , ont été touchées , dont 463 logements privés et 216 logements de bailleurs sociaux , 112 d' entre eux étant désormais inhabitables .
La ville de Maubeuge est divisée en différents quartiers , qui possèdent chacun leur propre conseil .
C' est un quartier historiquement ouvrier , ayant vu la création de forges , de faïenceries et d' usines sidérurgiques , alimentées par voie ferroviaire avec le charbon du Borinage et le minerai de fer de Lorraine .
Certaines maisons anciennes de Maubeuge sont , encore à l' heure actuelle , embellies par ses carrelages .
Un pôle universitaire , antenne de la faculté de Valenciennes , est également présent , comportant un IUT informatique et une école d' ingénieurs .
Sous-le-Bois était à l' origine une forêt d' environ 120 hectares , propriété des chanoinesses de Maubeuge .
Trois ans plus tôt , la Société des transports de Maubeuge avait acquis du département la concession du tramway .
Le lycée André-Lurçat , construit en 1994 , permet d' y maintenir une activité permanente .
Le faubourg Saint-Quentin devrait son nom aux bénédictins portant le nom de chanoines de Saint-Quentin .
La reconstruction fut menée par l' idée de la mise en avant de Maubeuge en tant que première ville française après la frontière belge .
C' est le plus petit quartier de Maubeuge , essentiellement résidentiel et enclavé par la voie ferrée au nord et la route d' Avesnes à l' ouest .
Une zone d' accueil de fournisseurs a vu le jour aux côtés de l' usine , qui réunit quelques entreprises sous-traitantes de Renault .
Le groupe Vallourec a été fondé , entre autres , dans la banlieue maubeugeoise ( le nom " Vallourec " étant un concentré de Valenciennes , Louvroil et Recquignies ) .
La principale surface commerciale de l' agglomération , se situant sur les communes de Louvroil et Hautmont , s' articule autour d' un hypermarché Auchan .
Actuellement , le centre commercial s' étend sur plus de 35 000 mètres carrés , supplantant de loin le centre-ville de Maubeuge , qui connaît par ailleurs des difficultés fréquentes pour les villes moyennes : désaffection des habitants , fermetures de petits commerces de proximité , manque de dynamisme du tissu commercial .
Maubeuge a su préserver et valoriser son patrimoine architectural et naturel riche de 1800 ha de verdure .
Le centre ville est enchâssé dans les remparts construits par Vauban .
La ceinture fortifiée sauvée du démantèlement se développe en couronne boisée d' ouest en est sur la rive gauche de la Sambre .
Au Nord , sur la place Vauban s' adosse contre la muraille la porte de Mons , ouvrage en pierre de taille surmonté de combles à la Mansart .
À l' origine , le plan Vauban comportait trois poudrières disposées et réparties stratégiquement au bord de l' enceinte intérieure .
Quelques îlots furent épargnés ou reconstruits sur leur propre base , pour s' associer et s' intégrer dans le plan rationnel de la ville reconstruite établi par André Lurçat .
La scène transfrontalière du Manège , très réputée dans la région , accueille des spectacles nombreux et divers , en partenariat avec l' intercommunalité et la ville belge de Mons .
Ce film raconte l' histoire d' un chauffeur de taxi qui devient célèbre après avoir composé cette chanson , dont le succès atteint même le Japon .
Le film Avida ( 2006 ) , réalisé par Benoît Delépine et Gustave Kervern , produit par Mathieu Kassovitz , a été tourné au parc zoologique de Maubeuge .
La ville de Maubeuge inspire régulièrement les scénaristes de cinéma :
L' évolution du nombre d' habitants depuis 1793 est connue à travers les recensements de la population effectués à Maubeuge depuis cette date :
Après la Seconde Guerre mondiale , la ville a été marquée par une forte poussée démographique , dont le pic fut atteint entre 1962 et 1968 ( + 18 % ) .
Par ailleurs , Maubeuge compte une part importante de familles de plus de six personnes ( environ 7 % des ménages , à mettre en regard avec les 2,5 % de la moyenne nationale ) .
Malgré le fait qu' elle soit de loin le plus important foyer de population de la région , la ville n' est pas sous-préfecture mais chef-lieu de canton , laissant la fonction à la petite commune d' Avesnes-sur-Helpe .
