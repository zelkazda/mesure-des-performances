Le Limbourg est une des douze provinces des Pays-Bas .
La capitale du Limbourg est Maastricht .
On y trouve le point le plus élevé des Pays-Bas , le Vaalserberg ( 321 m ) .
Le fleuve le plus important , la Meuse , traverse la province du sud au nord .
Dans le passé , on extrayait en Limbourg de la tourbe , du gravier et du charbon .
Du point de vue politique , le Limbourg était traditionnellement très divisé , comme en témoigne aujourd'hui la grande variété de dialectes qui y sont parlés : chaque commune a son propre dialecte , avec parfois de grandes différences à l' intérieur d' une même commune ( par exemple à Venlo ) .
En 1815 , lors de la formation du nouveau Royaume des Pays-Bas ( couvrant également l' actuelle Belgique ) , une des provinces ( constituée des actuelles provinces belge et néerlandaise de Limbourg ) reçut le nom de Limbourg .
Ce nom remonte au vieux duché de Limbourg , qui couvrait une partie du triangle Maastricht -- Aix-la-Chapelle -- Liège et exista jusqu' en 1648 .
Le nom de Limbourg est donné à cette province , bien qu' elle ne comprenne que quelques kilomètres carrés de l' ancien duché de Limbourg , à la demande expresse de Guillaume I er , qui ne voulait pas voir disparaître ce nom prestigieux .
Lors de la séparation des Pays-Bas et de la Belgique en 1830 , l' ensemble de cette province de Limbourg fut rattachée à la Belgique , à l' exception de la forteresse de Maastricht , qui restait aux mains des troupes néerlandaises .
Comme le roi Guillaume refusait de ratifier le traité , l' ensemble de la province resta administré par la Belgique jusqu' en 1839 .
À partir de ce moment et jusqu' en 1866 , le Limbourg néerlandais fit partie de la Confédération germanique sous le nom de duché du Limbourg .
Voir Communes du Limbourg néerlandais et Anciennes communes du Limbourg néerlandais pour une liste historique des fusions .
