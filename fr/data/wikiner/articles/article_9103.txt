Il étend les capacités des SMS , qui sont limités à 160 caractères , et permet notamment de transmettre des photos , des enregistrements audios ainsi que de la vidéo .
Si c' est le cas , le message est placé de façon temporaire sur un serveur HTTP .
Le terminal du récepteur peut enfin accéder au MMS via son client WAP .
Après l' expérience positive du SMS , les départements marketing des entreprises ont développé des nouvelles possibilités avec le MMS et les ont naturellement exploitées .
