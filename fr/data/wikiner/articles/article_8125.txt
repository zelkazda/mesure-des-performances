Daniel Bell est un sociologue et essayiste américain né le 10 mai 1919 à New York ( États-Unis ) .
Professeur émérite à l' Université Harvard , il a aussi enseigné la sociologie à l' Université Columbia .
Daniel Bell est , avec le français Alain Touraine , à l' origine du courant sociologique post-industrialiste .
Manuel Castells définira également Bell et Touraine comme les pères de l' informationalisme , qu' il considère comme le paradigme sociologique dominant depuis le milieu des années 1990 .
On peut ainsi voir dans l' œuvre de Bell une volonté de déconstruction du matérialisme historique .
Compte-tenu du contexte académique de l' après Seconde Guerre mondiale et de la passion inébranlable de Daniel Bell pour la sociologie , on peut considérer cet auteur comme ayant contribué à la confirmation de sa discipline au rang de science .
Bell a toujours prétendu décrire des principes sociétaux et leur fonctionnement sans se préoccuper de conclusions politiques .
Pour autant , son influence déterminante sur des sociologues comme Anthony Giddens , Ulrich Beck ou Manuel Castells est largement reconnue .
