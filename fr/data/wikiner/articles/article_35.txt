Né dans une famille issue de la noblesse française , Antoine de Saint-Exupéry passe une enfance heureuse malgré la mort prématurée de son père .
Aussi Saint-Exupéry se consacre-t-il à l' écriture et au journalisme .
Il entreprend de grands reportages au Vietnam en 1934 , à Moscou en 1935 , en Espagne en 1936 , qui nourriront sa réflexion sur les valeurs humanistes qu' il développe dans Terre des hommes , publié en 1939 .
Le Petit Prince , écrit à New York pendant la guerre , est publié avec ses propres aquarelles en 1943 à New York et en 1945 en France .
À la fin de l' été 1909 , sa famille s' installe au Mans , région d' origine de son père .
En 1912 , il passe les grandes vacances à Saint-Maurice-de-Rémens .
Il est fasciné par le nouvel aérodrome d' Ambérieu-en-Bugey , situé à quelques kilomètres à l' est [ réf. nécessaire ] .
Saint-Exupéry passe ainsi presque toute son enfance dans le château familial , entouré de ses frères et sœurs .
Alors que la Première Guerre mondiale éclate , Marie de Saint-Exupéry est nommée infirmière-chef de l' hôpital militaire d' Ambérieu-en-Bugey dans l' Ain .
À la rentrée scolaire de 1915 , Marie de Saint-Exupéry , toujours en poste à Ambérieu-en-Bugey , estime que ses fils ne se plaisent pas vraiment chez les frères jésuites de Mongré .
En rapport étroit avec le collège Stanislas de Paris , ce collège a développé une méthode d' éducation moderne qui leur permet d' exercer leur créativité .
L' élève Saint-Exupéry est davantage à l' aise dans les matières scientifiques que littéraires .
En 1918 , il avait fait la connaissance de Louise de Vilmorin , qui lui inspire des poèmes romantiques .
En avril 1921 , il est affecté pour son service militaire en tant que mécanicien au 2 e régiment d' aviation de Strasbourg .
Début août , il est affecté au 37 e régiment d' aviation à Casablanca , où il obtient son brevet civil .
En janvier 1922 , il est à Istres comme élève officier de réserve .
En octobre , sous-lieutenant de réserve , il choisit son affectation au 34 e régiment d' aviation , au Bourget .
Au printemps 1923 , il a son premier accident d' avion au Bourget : fracture du crâne .
Mais la famille de Louise de Vilmorin , sa fiancée , s' y oppose .
En septembre , c' est la rupture des fiançailles avec Louise .
À Toulouse , il fait la connaissance de Jean Mermoz et de Henri Guillaumet .
Au bout de deux mois , il est chargé de son premier convoyage de courrier sur Alicante .
En septembre 1929 , il rejoint Mermoz et Guillaumet en Amérique du Sud pour contribuer au développement de l' Aéropostale jusqu' en Patagonie .
En 1931 , il publie son second roman , Vol de nuit , un immense succès , dans lequel il évoque ses années en Argentine et le développement des lignes vers la Patagonie .
À partir de 1932 , alors que la compagnie , minée par la politique , ne survit pas à son intégration dans Air France , il subsiste difficilement , se consacrant à l' écriture et au journalisme .
Saint-Exupéry demeure pilote d' essai et pilote de raid en même temps qu' il devient journaliste d' occasion pour de grands reportages .
Il part pour l' Espagne en 1936 .
Sa réflexion aboutit à l' écriture de Terre des hommes , qui est publié en 1939 .
L' ouvrage est récompensé par le prix de l' Académie française .
Le 23 mai 1940 , il survole Arras alors que les panzers allemands envahissent la ville .
De fait , il a surtout essayé de réconcilier les factions opposées ; lors de son appel radiophonique du 29 novembre 1942 depuis New York , il lançait : " Français , réconcilions-nous pour servir " , mais il fut incompris , car il était trop tard et le temps était celui de l' affrontement général .
Cependant , selon des archives américaines récemment ouvertes il semblerait que les services secrets américains auraient envisagé de le pousser en lieu et place du général de Gaulle .
Alors que son séjour devait durer quelques jours , il passe finalement pres de cinq semaines au Québec à cause de problèmes de visa .
Mais il ne pense qu' à s' engager dans l' action , considérant , comme ce fut le cas avec l' Aéropostale , que seuls ceux qui participent aux événements sont légitimes pour en témoigner .
En avril 1943 , bien que considéré par les alliés comme un pilote médiocre , incapable de piloter un avion de combat moderne , il reprend du service actif dans l' aviation en Tunisie grâce à ses relations et aux pressions du commandement français .
Il séjourne alors en Algérie , au Maroc , puis en Algérie de nouveau , où il obtient au printemps 1944 l' autorisation du commandant en chef des forces aériennes en Méditerranée , le général américain Eaker , de rejoindre le prestigieux groupe 2/33 basé à Alghero , en Sardaigne .
Le 17 juillet 1944 , le 2/33 s' installe à Borgo , non loin de Bastia , en Corse .
" Saint-Ex " est officiellement porté disparu .
Sa mémoire est célébrée solennellement à Strasbourg le 31 juillet 1945 .
Le 12 mars 1950 , au Journal officiel , le commandant Antoine de Saint Exupéry est cité à l' ordre de l' armée aérienne à titre posthume , pour avoir " prouvé , en 1940 comme en 1943 , sa passion de servir et sa foi en le destin de la patrie " , et " trouvé une mort glorieuse , le 31 juillet 1944 , au retour d' une mission de reconnaissance lointaine sur son pays occupé par l' ennemi " .
Dans les années 1990 , un autre témoignage surgit tardivement , à propos d' une habitante de Carqueiranne qui aurait vu , le jour fatidique , le Lightning se faire abattre .
Chaque fois , ces " révélations " relancèrent l' intérêt aussi bien des spécialistes que du grand public , pour le " mystère Saint-Ex " .
Enfin , en 2000 , des morceaux de son appareil , le train d' atterrissage , un morceau d' hélice , des éléments de carlingue et surtout du châssis , sont retrouvés en Méditerranée au large de Marseille .
Le 7 septembre 1998 , un pêcheur avait déjà trouvé sa gourmette dans son chalut , près de l' île de Riou .
Les restes du Lightning sont exposés au Musée de l' air et de l' espace du Bourget , dans un espace consacré à l' écrivain aviateur .
En mission pour retrouver un avion ennemi qui survolait la région d' Annecy , Horst Rippert tourne plusieurs minutes au-dessus de la Méditerranée sans rien repérer .
Horst Rippert tire et touche .
L' avion s' enflamme et tombe à pic dans la Méditerranée .
Saint-Exupéry est porté disparu ce jour-là , donnant lieu au mystère sur sa disparition .
Après la guerre Horst Rippert , frère d' Ivan Rebroff ( décédé en février 2008 , soit peu avant cette révélation ) , se reconvertit dans le journalisme et dirige le service des sports de la ZDF .
Si elle n' est pas tout à fait autobiographique , son œuvre est largement inspirée de sa vie de pilote aéropostal , excepté pour Le Petit Prince ( 1943 ) -- sans doute son succès le plus populaire ( il s' est vendu depuis à plus de 80 millions d' exemplaires dans le monde ) -- qui est plutôt un conte poétique et philosophique .
Sa ville natale de Lyon , en hommage à l' écrivain et en clin d' œil au pionnier de l' aéropostale , a rebaptisé l' aéroport de Satolas en aéroport international Lyon Saint-Exupéry et la gare de Lyon-Saint-Exupéry TGV .
Sur les murs du Panthéon de Paris , une inscription honore sa mémoire en ces termes :
