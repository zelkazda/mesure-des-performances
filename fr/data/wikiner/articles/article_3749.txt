En avril 1940 , sa famille quitte Bruxelles pour aller au Maroc , dans la ville de Casablanca .
En y allant , elle passe par Paris où elle écrit son premier roman qui sera long d' une demi-page .
Elle vivra à Casablanca pendant cinq ans .
Une partie de sa famille est déportée en Allemagne lors de la Seconde Guerre mondiale .
En 1945 , elle retourne à Bruxelles .
Ensuite , elle entreprend des études de médecine à l' ULB ( Université libre de Bruxelles ) .
Elle passe 21 mois sous pneumothorax au sanatorium universitaire d' Eupen .
Elle se marie avec le cinéaste Emile Degelin .
C' est Christian Bourgois qui va reprendre la direction de la maison d' édition .
Jacqueline Harpman subira un blocage dans l' écriture .
En 1967 , elle va commencer des études de psychologie à l' ULB .
Un an aprés , son roman Orlanda est publié et reçoit le prix Médicis en 2006 .
Aujourd'hui , Jacqueline Harpman continue à écrire et à exercer ses activités de psychanalyste .
Le milieu bourgeois de Jacqueline Harpman a fortement déteint sur son style d' écriture : élégant , elliptique , étoffé et envoûtant .
Jacqueline Harpman n' est pas classable , étant donné qu' elle touche à tous les genres littéraires .
