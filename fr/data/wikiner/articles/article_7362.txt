Par son jeu de mime et de clownerie , il a su se faire remarquer , et devenir l' un des plus célèbres acteurs d' Hollywood .
Charlie Chaplin fut l' une des personnes les plus créatives de l' ère du cinéma muet .
Réalisateur , scénariste , producteur , monteur , et même compositeur de ses films , sa carrière durera plus de soixante-cinq ans , du music-hall en Angleterre , jusqu' à sa mort , en Suisse .
Il fut fortement inspiré par l' acteur burlesque français Max Linder : tous deux choisiront un costume bien à eux .
Mais Max Linder , au contraire de Charlie Chaplin , ne se fera pas représenter comme une victime de la société .
La vie publique et privée de Charlie Chaplin fera l' objet d' adulation , comme de controverse .
Il n' a qu' un an lorsque son père part en tournée aux États-Unis .
Deux mois plus tard , la mère de Chaplin obtient son congé de l' hôpital .
Pendant ce temps , Charlie vécut avec son père et sa belle-mère alcoolique , dans un environnement intenable pour un enfant , dont les souvenirs inspireront Le Kid .
À cinq ans , Chaplin monte sur scène pour remplacer au pied levé sa mère qui ne peut plus chanter , victime d' une extinction de voix .
Le frère de Charlie , Sydney , quitte le foyer parental pour travailler dans la marine .
Puis , il obtient à partir de 1903 une succession de contrats au théâtre , et en 1908 , il est engagé dans la troupe de Fred Karno , alors le plus important impresario de spectacles avec des sketches .
Il y rencontre le futur Stan Laurel .
Au cours d' une tournée de la troupe en Amérique , les studios Keystone lui adressent une proposition de contrat qu' il accepte : l' aventure cinématographique commence .
Ne supportant pas les pressions dues à ces temps très brefs , Chaplin s' adapte très mal aux conditions de travail de la compagnie , à tel point que les incidents avec les metteurs en scène sont fréquents .
Sur les ordres de Mack Sennett qui lui demande de se créer un maquillage au pied levé , il crée en 1914 le personnage raffiné de Charlot le vagabond , et recentre tout son comique autour du nouveau personnage et de sa silhouette qu' il inaugure dans Charlot est content de lui ( 1914 ) .
En 1916 , il signe un contrat de distribution d' un million de dollars avec la First National , qui lui laisse la production et la propriété de huit films prévus .
Il fait alors immédiatement construire son propre studio dans lequel il réalise neuf films dont Une vie de chien , Le Kid et Charlot soldat .
En 1919 , un vent de révolte souffle sur Hollywood où les acteurs et cinéastes se déclarent exploités ; Chaplin s' associe alors à David Wark Griffith , Mary Pickford et Douglas Fairbanks pour fonder la United Artists .
Son premier film pour sa nouvelle firme sera L' Opinion publique ( 1923 ) .
Puis , Chaplin fait peu à peu entrer dans son univers comique celui du mélodrame et de la réalité sociale comme dans La Ruée vers l' or ( 1925 ) .
Les Lumières de la ville ( 1931 ) est le premier film à en bénéficier , mais de manière très ironique .
Chaplin souffle pendant des heures dans un vieux saxophone afin de parodier les imperfections du parlant lors de la scène d' ouverture du film .
De plus Chaplin ne se détourne pas de son projet initial de film muet .
Un film dialogué a une audience un peu plus limitée car il contient la barrière de la langue et Chaplin veut s' adresser à tous .
On le dit fini , à l' instar de ses amis David Wark Griffith , Mary Pickford et Douglas Fairbanks et de bien d' autres vedettes du muet qui n' ont pas survécu au parlant .
Il entreprend un long voyage , qui va durer plus d' un an et demi , à travers le monde , en Europe notamment , pour présenter son film .
Il rencontre la plupart des chefs d' états et de nombreuses personnalités , parmi lesquelles Albert Einstein .
Il conjugue tout cela dans Les Temps modernes ( 1936 ) , le dernier film muet de l' histoire et l' un des plus célèbres , sinon le plus célèbre , de son auteur .
Le personnage joué par Paulette Goddard les lui copie sur ses manchettes .
Ce film est également l' ultime apparition à l' écran du personnage Charlot .
Il parle aussi de la difficulté du travail à la chaîne qui rend fou la plupart des employés , dont le personnage interprété par Chaplin , ce qui lui vaut un passage à l' hôpital psychiatrique dans le film .
Mais le cinéaste reçoit le soutien du président Franklin Roosevelt , lequel l' invitera , quelques semaines après la sortie du film , à la Maison Blanche , pour s' entendre réciter le discours final .
Le film est interdit sur tout le continent Européen [ réf. nécessaire ] , mais une rumeur circule : Hitler l' aurait vu , en projection privée [ réf. nécessaire ] .
En France , il ne sortira qu' en 1945 .
Cette fois-ci , Chaplin est définitivement entré dans l' ère du cinéma sonore ... et signe l' arrêt de mort du petit vagabond .
En 1946 , Chaplin tourne son film le plus dur , Monsieur Verdoux .
Orson Welles propose à Chaplin un scénario basé sur l' affaire Landru .
Une fois encore , Chaplin livre un message empreint de cynisme mais également d' humanisme .
En 1950 , il vend la quasi-totalité de ses parts à la United Artists et travaille aux Feux de la Rampe où il décrit la triste fin d' un clown dans le Londres de son enfance .
Ses propres enfants apparaissent comme figurants et Chaplin tient le premier rôle .
Le film sort en 1952 à Londres et vaut un triomphe à son auteur .
L' une des plus belles scènes du film se trouve vers la fin : Buster Keaton joue un pianiste et Chaplin un violoniste .
Mais rien ne se déroule comme prévu car Keaton a des problèmes avec ses partitions et son piano et Chaplin doit se battre avec les cordes de son violon .
Victime du maccarthisme ( son nom figure sur la " liste noire " ) , il est harcelé par le FBI en raison de ses opinions de gauche ( pour sa part , il se présentait comme un " citoyen du monde " ) .
Pour cette raison , il se voit refuser le visa de retour lors de son séjour en Europe pour la présentation de son film .
Il renonce alors à sa résidence aux États-Unis et installe sa famille en Suisse jusqu' à la fin de ses jours .
En 1967 , il tourne son dernier film , cette fois-ci en couleur , La Comtesse de Hong-Kong , avec Sophia Loren , Marlon Brando et Tippi Hedren , dont l' action se déroule sur un paquebot et où il ne tient qu' un petit rôle : celui d' un steward victime du mal de mer .
De nombreuses demandes de rançon plus ou moins farfelues sont adressées à la famille Chaplin .
Charlie Chaplin a été marié à 4 reprises ;
Mildred Harris , Lita Grey et Paulette Goddard étaient toutes trois ses partenaires à l' écran .
L' aventure de Charlie Chaplin avec Lita Grey aurait inspiré Vladimir Nabokov pour son roman Lolita .
Ses mariages ont défrayé la chronique américaine , en effet il a 29 ans quand il se marie avec Mildred Harris , qui en a 17 ; il en a 35 quand il épouse Lita Grey qui a 16 ans ; il a 47 ans quand il convole avec Paulette Goddard qui en a 25 ; il a 54 ans lors de son mariage avec Oona O'Neill qui en a 18 .
Cependant , avec l' arrivée du parlant , Chaplin a dû faire un choix et opérer un passage du muet au sonore , puis au parlant .
C' est dans Les Lumières de la ville que Chaplin débute ce passage au sonore .
Cependant , comme le dit Michel Chion , il s' agit tout de même d' un " véritable manifeste pour la défense du muet " .
Or , s' il y a une chose qui n' est pas sonore , c' est bien le moment où le bruit de la portière fait croire à la jeune aveugle que Charlie est un millionnaire -- gag qui a nécessité plusieurs mois d' élaboration , et plusieurs interruptions de tournage .
De plus , lorsqu' un homme mange le savon de Charlie et que celui-ci se met à le disputer , tout ce qui sort de sa bouche est des bulles de savon , comme si toute parole était vaine .
Lorsque Chaplin débute le tournage des Temps Modernes ( 1936 ) en parlant , il se rend compte bien vite qu' il s' y perd .
La seule fois où on entend réellement un personnage parler " en direct " est également la première fois où l' on entend la voix de Chaplin .
Cependant , même si celui-ci essaie d' avoir un langage articulé , il baragouine , ayant oublié les paroles de sa chanson : " c' est comme le langage à la naissance " , langage que Chaplin développera dans les prochains films .
Dans Le Dictateur , contrairement aux Lumières de la ville , le titre fait appel au monde de la parole .
Même si le film est presque entièrement parlant et renonce définitivement aux cartons du muet , Chaplin ne renonce pas encore au langage de la pantomime .
Chaplin s' engage politiquement dans certaines de ses œuvres , véritables satires de la société des années 1930 .
Des films comme Les Temps modernes ou Le Dictateur sont respectivement une critique de la société de consommation de masse et du travail à la chaîne , et une critique des régimes politiques dictatoriaux et fascistes qui s' installent en Europe .
On peut donc affirmer l' engagement politique de Charlie Chaplin dans la société de son époque .
Accusé de prendre des positions communistes aux États-Unis ce que lui a valu des enquêtes du FBI , il fut une des victimes du maccarthisme au début des années 1950 et inscrit sur la liste noire du cinéma .
Ce fut l' une des causes de son exil en Suisse .
Il n' existe aucune indication d' une ascendance juive de Chaplin , cependant tout au long de sa carrière , il y eut des controverses sur ses possibles origines juives .
Dans les années 1930 , la propagande nazie l' a constamment déclaré juif en se fondant sur des articles publiés antérieurement dans la presse américaine ; les enquêtes du FBI sur Chaplin à la fin des années 1940 ont également mis l' accent sur ses origines ethniques .
Durant toute son existence , Chaplin a farouchement refusé de contester ou de réfuter les déclarations affirmant qu' il était juif , en disant que ce serait " faire directement le jeu des antisémites " .
Comme Orson Welles , Alfred Hitchcock , ou Cary Grant , Charlie Chaplin n' a jamais reçu la célèbre statuette , sinon le prix honorifique .
Il a toutefois reçu un Oscar de la meilleure musique de film en 1952 pour Les Feux de la rampe ( qui est le seul film réunissant Charlie Chaplin et Buster Keaton ) .
Avant cette consécration , il avait été nommé comme meilleur acteur , meilleur réalisateur pour le film Le Cirque .
