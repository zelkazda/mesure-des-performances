Pendant la guerre , il est embauché par la Société parisienne d' édition , et apprend les bases du métier : le lettrage , le calibrage d' un texte , la retouche d' image ...
Dans le cadre de son travail il rencontre Calvo , qui le pousse à persister dans le dessin .
C' est à cette époque qu' il rencontre le scénariste avec lequel il travaille le plus durant sa carrière : René Goscinny .
En 1959 , Uderzo participe au lancement de Pilote en illustrant deux de ses séries-titres : Tanguy et Laverdure , série d' aviation réaliste scénarisée par Charlier et Astérix le Gaulois , série humoristique scénarisée par Goscinny qui , dès cette date , obtient un très grand succès .
Face à celui-ci , Uderzo abandonne progressivement au cours des années 1960 toutes ses autres séries .
En 1967 , Uderzo déménage à Neuilly-sur-Seine .
Avec Goscinny , tout en réalisant environ une histoire par an , il supervise attentivement le développement des produits dérivés .
La mort de Goscinny , en 1977 , bouleverse profondément Uderzo ( il dira plus tard qu' il restera assis 24 heures ou 48 heures après avoir appris la nouvelle ) , qui décide pourtant de poursuivre Astérix .
Dès le départ , Uderzo se heurte à des critiques bien que ses premiers scénarios se rapprochent de ceux de Goscinny , en conservant ce ton et cet humour propre à la série avec un réel talent ; la qualité sera néanmoins plus contestée autour des années 2000 .
À l' instar de Franquin , ses personnages sont très expressifs et dotés d' une gestuelle extrêmement travaillée qui vient en partie de l' intérêt d' Uderzo pour le dessin d' animation .
Il reste une référence incontournable pour les dessinateurs et animateurs actuels comme Juanjo Guarnido .
Il a réalisé les dessins du film L' Avare sorti en 1980 avec pour vedette Louis de Funès .
