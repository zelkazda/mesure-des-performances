Après l' échec de la France pour reconquérir l' Indochine suite à la victoire du Việt Minh à la bataille de Diên Biên Phu le 7 mai 1954 , les accords de Genève divisèrent le pays en deux par une zone démilitarisée au niveau du 17 e parallèle .
Au nord , la République démocratique du Viêt Nam , régime communiste fondé par Hô Chi Minh en septembre 1945 .
Ngô Dinh Diêm et ses alliés s' opposèrent à la tenue des élections de réunification initialement prévues au plus tard à l' été 1956 par les accords de Genève .
Après quinze ans de combats ( entre 1957 et 1972 ) et un lourd bilan humain , l' intervention directe et massive des États-Unis prit fin avec la signature des accords de paix de Paris en 1973 .
Considérée comme la première défaite militaire de l' histoire des États-Unis , cette guerre impliqua plus de 3,5 millions de jeunes américains envoyés au front entre 1965 et 1972 .
En 1945 , la reconquête de l' Indochine par la France puis , l' échec des gouvernements vietnamien et français à s' accorder sur un modus vivendi , conduisit en décembre1946 à la Première Guerre d' Indochine .
La bataille décisive eut lieu au printemps 1954 avec la bataille de Diên Biên Phu .
À cet accord était également signée une déclaration commune par les neuf participants , sauf par les États-Unis et par l' État du Viêt Nam .
L' indépendance du Laos , du Cambodge et du Viêt Nam était reconnue .
Avec le soutien du président américain Eisenhower , son gouvernement refusait les élections générales initialement prévues .
De fait , dès août 1955 , Ngô Đình Diệm déclarait que son pays ne se considérait lié en aucune façon par les Accords de Genève dont il n' avait pas été signataire .
Les États-Unis ajoutaient ne pas reconnaitre la RDVN , déclarant également ne pas avoir été signataire des accords .
Le 20 janvier 1961 , le président Kennedy débutait son mandat et confirmait l' interventionnisme américain en portant à 15000 hommes l' effectif des conseillers militaires .
Le 4 janvier 1962 étaient signés entre les États-Unis et le gouvernement de Ngô Đình Diệm , des accords dont les principaux points portaient sur la pacification , la démocratisation et la libéralisation .
En février , un accord était conclu entre la RDVN et le Pathet Lao au Laos pour maintenir la piste Hô-Chi-Minh ouverte .
Malgré cette escalade militaire et son intensité opérationnelle ( 27000 ratissages et 60000 sorties aériennes ) en 1962 , l' activité Viêt Cong ne tarissait pas .
Il était tué le 2 novembre avec son frère Ngô Đình Nhu et une junte militaire était mise en place avec à sa tête le général Duong Van Minh .
Le nouveau président américain Lyndon Johnson , annula le retrait des troupes , augmenta le contingent et demanda l' aide de plusieurs alliés des États-Unis , notamment la Corée du Sud et l' Australie .
En mai , débutaient des raids américains sur le Laos pour tenter de couper la piste Hô-Chi-Minh .
Le 10 février , le Viêt Cong attaquait Qui Nhon et provoquait la mort de 21 Américains .
Lyndon Johnson franchissait une nouvelle étape le 13 du même mois en ordonnant des raids aériens plus étendus sur le Nord ( Opération Rolling Thunder ) .
Le 9 mars , Johnson autorisait l' usage du napalm .
En mai 1966 , une rébellion militaire pro-bouddhiste éclatait à Da Nang .
En février 1968 , débutait l' offensive du Tết .
Le 7 juillet , les forces américaines évacuaient la base de Khe Sanh , après l' avoir pourtant renforcée et défendue bec et ongles de janvier à avril contre un véritable siège par trois divisions de l' armée nord-vietnamienne .
Ce siège s' avéra plus tard être une manoeuvre de diversion par les nords-vietnamiens destinée à attirer le plus de forces américaines possible dans cette région montagneuse éloignée des centres de population côtiers , avant de déclencher l' offensive du Têt sur les principaux centres urbains du sud-vietnam .
La foi du public américain en la " lumière au bout du tunnel " est balayée en février 1968 quand l' ennemi , supposé être sur le point de s' effondrer , organise l' offensive du Tết .
Võ Nguyên Giáp , en charge des opérations viet-congs , lance la quasi-totalité de ses effectifs dans la bataille ( environ 230000 hommes ) [ réf. nécessaire ] .
Le FNL ne revient à son niveau d' effectifs d' avant l' offensive que dans le courant de l' année 1972 avec le renfort d' unités régulières du nord et ne jouera plus de rôle déterminant dans le conflit .
À Hué , le Viêt-Cong massacra environ 3000 intellectuels , commerçants et personnes liées au régime sud-vietnamien .
Il y avait déjà un faible mouvement d' opposition à la guerre dans certaines parties des États-Unis , dès 1964 , spécialement sur quelques campus universitaires .
La Seconde Guerre mondiale s' était terminée en 1945 , et la Guerre de Corée en 1953 .
L' offensive du Tết relève de la guerre psychologique .
Son but n' était pas seulement la conquête territoriale , mais d' amplifier l' opposition à la guerre aux États-Unis .
Des dossiers partiellement déclassifiés démontrent que 6359 officiers et généraux de l' Armée rouge soviétique ont agi comme conseillers militaires et ont pris part aux opérations de combats ( surtout dans la défense anti-aérienne ) .
En 1968 , le président Lyndon Johnson débute sa campagne de réélection .
McCarthy perd les premières élections primaires dans le New Hampshire , mais il provoque la surprise en réalisant un score élevé contre le sortant .
Le coup porté à la campagne de Johnson , combiné à d' autres facteurs , le mène à annoncer qu' il retire sa candidature , lors d' un discours télévisé surprise le 31 mars .
Se saisissant de l' opportunité causée par l' abandon de Johnson , Robert Kennedy brigue alors l' investiture sur une plate-forme anti-guerre .
Le vice-président de Johnson , Hubert Humphrey , se porte également candidat , promettant de continuer d' aider le gouvernement de la RVN .
Robert Kennedy est assassiné durant cet été , et McCarthy est incapable de contrer le support dont Humphrey jouit dans l' élite du parti .
Humphrey gagne l' investiture de son parti , et se présente contre Richard Nixon dans les élections générales .
Richard Nixon est élu président et démarre à compter de janvier 1969 sa politique de lent désengagement de la guerre .
Le but est d' aider progressivement la RVN à construire sa propre armée de sorte qu' elle puisse poursuivre la guerre par elle-même .
Cela doit aussi donner , selon le mot de Henry Kissinger , " l' intervalle nécessaire " au désengagement des troupes américaines .
Le 18 mars 1970 , Lon Nol , alors premier ministre , obtient le soutien du parlement pour destituer le prince Norodom Sihanouk , accusé de ne pas lutter contre les Viêt-Cong qui utilisent l' est du Cambodge comme sanctuaires militaires .
Parallèlement , Nixon ordonne , le 29 avril 1970 , une incursion militaire du Cambodge par des troupes américaines et sud-vietnamiennes , afin de détruire les refuges viêt-cong bordant la RVN .
Le 30 avril , il s' adresse aux Américains pour justifier l' initiative , destinée essentiellement à protéger le processus de désengagement .
Celle-ci provoqua d' importantes manifestations à Washington et à Kent State University , qui augmentèrent l' opposition de l' opinion publique américaine à la guerre .
Ensuite , il n' y a plus qu' à attendre le 30 juin pour se redéployer , submergeant mécaniquement les forces de la République khmère , peu équipées , peu entraînées , mal gérées , mal organisées ...
Ce crime de guerre fut stoppé lorsque Hugh C. Thompson , Jr. , chef d ' équipe d' un hélicoptère d' observation remarque le carnage et intervient avec ses coéquipiers pour arrêter le massacre .
L' opinion publique américaine commence à douter majoritairement des options liées à un tel niveau d' engagement , alors que le parti de la génération de la " prise de conscience " ( consciousness generation ) maintient l' activisme par des sit-ins en faveur du règlement du conflit et du désengagement dans les allées publiques de Washington : la pression est telle que le pouvoir politique doit répondre instamment à la situation d' enlisement .
1972 est de plus une année terrible pour l' exécutif américain , avec le scandale du Watergate et la publication des Pentagon Papers qui éclaboussent la classe politique .
À Central Park , plusieurs centaines de jeunes détruisent leurs papiers militaires .
D' autres fuient au Canada pour échapper à la guerre .
Le 21 octobre 1967 , une marche sur le Pentagone réunit plus de 100 000 personnes .
En avril 1968 , des étudiants occupent le campus de l' université Columbia ; ils sont évacués par la police le 30 , ce qui entraîne une grève de protestation jusqu' à l' été 1968 .
D' autres universités prennent position contre la guerre , comme celle de Berkeley en Californie .
Cependant , la paix n' est toujours pas garantie et le nouveau général nordiste prépare l' offensive finale qui vaincra la RVN .
Nixon se bat alors aussi pour sa propre carrière politique empêtrée dans le scandale du Watergate .
De plus , les États-Unis retirent unilatéralement leurs dernières forces du Viêt Nam en 1973 .
À la fin de 1974 , en violation des accords , 100000 soldats supplémentaires s' infiltreront au Laos et au Cambodge dans des camps frontaliers puis dans les zones " libérées " au sud .
Hue ( l' ancienne cité impériale ) tombe le 25 mars , puis Đà Nẵng ( la seconde cité du Việt Nam ) le 2 avril .
Après avoir pris les plateaux centraux et coupé les forces sudistes en deux , puis écrasé la partie nord du Vietnam du Sud , les troupes de l' Armée populaire vietnamienne se tournèrent ensuite vers le sud , tandis que de nouvelles troupes franchissaient la frontière depuis la RDVN .
Début avril , la région de Saïgon est encerclée .
À 7h53 , le 30 avril , lorsque le dernier hélicoptère décolle du toit de l' ambassade des États-Unis à Saïgon , des milliers de candidats à l' exil se pressent encore dans les jardins .
Cette scène de panique à Saïgon , le 30 avril 1975 , sur le toit de l' ambassade des États-Unis à Saïgon est bien connue .
De 1975 à 1982 , 65000 personnes furent exécutées au Viêt Nam et plus d' un million furent envoyées en " camps de rééducation " ou dans les " nouvelles zones économiques " .
Au total , trois millions de personnes quittèrent l' Indochine entre 1975 et 1997 selon le Haut Commissariat des Nations unies pour les réfugiés .
Une partie des réfugiés de 1975 étaient l' élite du régime de Saïgon partis en avion dans les bagages des derniers personnels américains .
Les réfugiés de 1980 étaient de petites gens fuyant la Troisième Guerre d' Indochine et les difficultés économiques d' un pays dévasté par des guerres depuis 1946 .
Il est ainsi très difficile de s' accorder exactement sur ce qui doit compter comme " victime de guerre du Viêt Nam " ; des gens sont encore aujourd'hui tués par des sous-munitions non explosées et des mines , particulièrement les bombes à sous-munitions .
Les effets sur l' environnement des agents chimiques , tels que l' agent orange qui était un défoliant très utilisé par les Américains , ainsi que les problèmes sociaux colossaux causés par la dévastation du pays après tant de morts ont certainement réduit la durée de vie de beaucoup de survivants .
Concernant les pertes aériennes nord vietnamienne , 202 MiG furent abattus en combats aériens par 174 avions américains entre avril 1965 et janvier 1973 .
Voici les pertes des aéronefs des forces armées des États-Unis .
Les opérations entre 1961 et 1975 coûteront 533 milliards USD ( valeur 2005 ) aux États-Unis .
La plus grande conséquence sur le développement sud-asiatique est le fait que les plus grands " cerveaux " du Viet Nam ont soit fuit aux États-Unis avec les Américains , soit été décimés/envoyés dans des camps de rééducation par les communistes .
Ils constituent une force économique aux États-Unis et ailleurs .
