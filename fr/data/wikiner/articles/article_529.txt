La Garonne se forme au Val d' Aran dans les Pyrénées espagnoles .
Cette branche est en effet plus longue que le cours officiel et son débit plus important .
Mais la définition d' un fleuve étant déterminée par l' altitude de la source la plus élevée d' un de ses affluents , c' est bien ce modeste ruisseau qui a droit à l' appellation de Garonne .
La longueur de son cours d' eau , en France , est alors de 522,6 km .
Le fleuve traverse Muret , reçoit l' Ariège à Portet sur Garonne pour atteindre Toulouse où il change de direction en se dirigeant au nord-ouest pour se jeter dans l' Atlantique à son embouchure en commun avec la Dordogne où les deux fleuves forment l' estuaire de la Gironde .
Entre Toulouse et Bordeaux le fleuve traverse Agen et reçoit ses principaux affluents sur la rive droite , le Tarn et le Lot issus du système hydrologique du Massif central .
Le fleuve est navigable de l' océan à Langon .
Des Pyrénées à Toulouse le fleuve est aménagé pour l' industrie hydroélectrique .
A Bordeaux le fleuve est très large et sous l' influence des marées .
Dans sa partie supérieure à l' amont de Toulouse son débit dépend de l' enneigement et de la fonte des neiges et dans sa partie inférieure elle a une alimentation pluviale due à ses principaux affluents .
La Garonne alimente aussi plusieurs canaux :
Le débit de la Garonne a été observé sur une période de 76 ans ( 1913-1988 ) , au Mas-d'Agenais , localité du département de Lot-et-Garonne située à une douzaine de kilomètres en amont de la ville de Marmande .
Le débit moyen interannuel ou module du fleuve au Mas-d'Agenais est de 631 m³ par seconde ( plus que la Seine à son embouchure qui fait plus ou moins 540 m³ par seconde ) .
La Garonne présente des fluctuations saisonnières de débit bien marquées , mais pas excessives .
Le débit de la Garonne se maintient ainsi bien mieux que ceux de la Seine ou de la Loire , en période de sècheresse .
Le débit journalier maximal enregistré au Mas-d'Agenais a été de 5700 m³ par seconde le 5 mars 1930 .
Bien qu' une bonne partie des plaines de son bassin soient peu arrosées , au point de nécessiter des ouvrages d' irrigation , la Garonne est un fleuve abondant , puissamment alimenté par les fortes précipitations des hauts sommets des Pyrénées centrales , et d' une bonne partie du Massif central .
La lame d' eau écoulée dans son bassin versant se monte à 384 millimètres annuellement , ce qui est nettement supérieur à la moyenne d' ensemble de la France tous bassins confondus ( 320 millimètres ) .
Afin de limiter les risques pour la Garonne et éviter les conflits entre usage , une réalimenation du fleuve est assurée à partir de réservoirs situés dans les Pyrénées
Cette eau est largement restituée , la consommation nette représente à peine 7 % du prélèvement , mais peut engendrer des variations instantanées de débits préjudiciables à l' écosystème et aux autres activités .
Trois canaux prélèvent de l' eau sur les quatre mois d' étiage : le canal de Garonne et de celui de Saint-Martory ( dont les prélèvements en Garonne , non compensés , pèsent sur la ressource naturelle quand elle vient à manquer ) et le canal de la Neste .
Le soutien d' étiage : Des conventions pluriannuelles de soutien d' étiage mobilisent déjà et ce depuis 1993 , plus de 50 millions de mètres cubes ( hm3 ) de ressources en amont de Toulouse ( de 30 à 70 hm3 mobilisables selon les années ) .
Autrefois important axe de navigation et de transport de marchandises , la Garonne n' est aujourd'hui navigable pour les plus gros bateaux ( cargos , porte-conteneurs ... ) que dans son estuaire , jusqu' au Pont de Pierre à Bordeaux , et pour les grosses péniches jusqu' à Langon , la suite se faisant par le Canal de Garonne qui est voué presque exclusivement au tourisme fluvial .
Le transport de gros gabarit a repris de l' activité jusqu' à Langon grâce à la construction aéronautique et surtout de l' A380 dans les ateliers de Toulouse .
Voir Ville de Bordeaux nom du bateau construit spécialement pour le transport d' une partie du fuselage de l' A380 de Saint-Nazaire à Pauillac ( Gironde ) et de là transbordée sur une des deux barges ou l' une des 27 péniches appartenant à Airbus remontant la Garonne jusqu' à Langon
La Garonne constitue l' axe majeur de migration pour les poissons grands migrateurs , reliant l' Atlantique jusqu' aux Pyrénées .
L' estuaire de la Gironde , véritable milieu de transition , joue un rôle clé dans l' adaptation physiologique des poissons grands migrateurs au passage d' un milieu marin à un milieu fluvial , et vice versa .
Il a fallu attendre les années 1970-1980 pour que les pouvoirs publics étendent le plan de sauvetage du saumon à l' ensemble des espèces migratrices , imposent des dispositifs de franchissement de barrages , prévoient des alevinages , limitent la pêche et redonnent un avenir à des espèces emblématiques en pays de Garonne .
L' esturgeon européen : Il est sur la liste rouge des espèces menacée de l' Union internationale pour la conservation de la nature ( UICN ) .
La lamproie marine : Elle est pêchée dans la partie aval de la Garonne , au filet ou dans des nasses .
