Phénix , l' oiseau de feu est un manga de Osamu Tezuka , publié en français aux éditions Tonkam ( onze volumes ) .
Ce manga a obtenu en 1970 le Prix culturel Kōdansha dans la catégorie des mangas pour enfant .
Il s' agit d' un ensemble d' histoires se déroulant dans le Japon médiéval ou le monde du futur , tournant autour du légendaire phénix , oiseau de feu dont le sang rend immortel .
Malheureusement , cet ordre n' a pas été gardé par la version bunko sur laquelle s' est basé Tonkam pour réaliser la version française .
D' autres chapitres étaient prévus mais la mort d' Osamu Tezuka a empêché leur réalisation .
