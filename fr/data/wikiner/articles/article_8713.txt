La première bataille de Pânipat a lieu en Inde du nord , et marque le début de l' Empire moghol .
Il rassemble ses forces , 12 000 hommes et quelques pièces d' artillerie et marche sur l' Inde le 17 novembre 1525 .
En février 1526 , il prend Lahore .
Bâbur est un stratège inspiré et à la tête d' une armée bien disciplinée .
Le 24 avril , Bâbur entre dans Delhi .
Cette bataille marque le début de l' utilisation à grande échelle de l' artillerie dans la guerre en Inde .
