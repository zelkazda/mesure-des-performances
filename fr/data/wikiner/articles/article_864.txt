L' Insee et la Poste lui attribuent le code 56 .
Le Morbihan est le seul département français à avoir gardé un nom qui n' a pas été modifié par la langue française et est ainsi le seul département portant un nom non-français .
Le département a été créé à la Révolution française , le 4 mars 1790 en application de la loi du 22 décembre 1789 , à partir d' une partie de l' ancienne province de Bretagne , composée de l' évêché de Vannes ( moins deux petites parties au nord , une autre à l' est et une dernière à l' ouest , de l' extrême est de la Cornouaille , du sud de l' évêché de Saint-Brieuc , du sud-ouest de l' évêché de Saint-Malo et du nord-est de l' évêché de Nantes .
Il est créé à partir de la circonscription du présidial de Vannes à laquelle on a retranché la sénéchaussée de Quimperlé et la moitié nord de celle de Ploërmel et ajouté la sénéchaussée de Gourin ( à peu près ) .
Les communes de la Cornouaille morbihannaise ( l' ancienne sénéchaussée de Gourin ) qui n' avaient pas choisi d' être annexées au département du Morbihan ont régulièrement réclamé de rejoindre le Finistère , sans succès [ réf. nécessaire ] .
On songea à nommer ce département " les Côtes du sud " , par opposition aux Côtes-du-Nord , mais la présence de plusieurs golfes appelés mor bihan ( " petite mer " en français ) par les habitants , à Gâvres et au sud de Vannes , lui a fait préférer ce vocable géographique .
Plus tard , le Morbihan fait partie des régions " Bretagne " créées successivement en 1941 , 1944 et 1956 -- 72 -- 88 ( l' actuelle région Bretagne ) et regroupant toutes les départements du Finistère , des Côtes-d'Armor et de l' Ille-et-Vilaine .
Le Morbihan fait partie de la région Bretagne .
Il est limitrophe des départements du Finistère à l' ouest , des Côtes-d'Armor au nord , d' Ille-et-Vilaine à l' est et de la Loire-Atlantique au sud-est , et bordé par l' océan Atlantique au sud .
Il existe des microclimats surprenants tels que ceux de la presqu'île de Quiberon ou de Belle-Île .
La population se concentre majoritairement dans les aires urbaines de Lorient et de Vannes et dans les zones côtières .
Lorient , la ville la plus peuplée du département , compte environ 147000 habitants pour son agglomération tandis que Vannes , la seconde ville en importance , compte environ 133000 habitants .
Ce tableau indique les principales communes du Morbihan dont les résidences secondaires et occasionnelles dépassent 25 % des logements totaux .
