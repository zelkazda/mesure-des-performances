Le territoire est constitué de l' île de Porto Rico proprement dite , ainsi que de plusieurs îles plus petites , dont Vieques , Culebra et Isla Mona .
Porto Rico est l' ancienne dénomination en anglais américain .
En revanche , Porto Rico reste la seule dénomination officielle en langue française .
L' histoire de l' archipel de Porto Rico avant l' arrivée de Christophe Colomb n' est pas bien connue .
Porto Rico devint un bastion et un port important pour l' empire espagnol .
En 1945 , Luis Muñoz Marin gagne les premières élections démocratiques de l' histoire de Porto Rico , et en 1952 , il aide Porto Rico à obtenir une autonomie partielle vis-à-vis des États-Unis .
En 1974 , le radiotélescope d' Arecibo est inauguré .
Ce télescope est le plus grand du monde , avec sa parabole de plus de 300 m de diamètre , encastrée dans le paysage karstique de la région d' Arecibo .
En 2005 , à la suite de l' assassinat d' un leader indépendantiste , la situation se crispe de nouveau , malgré les annonces de George W. Bush .
Le 1 er mai 2006 , les États-Unis interrompent le système de prêts à Porto Rico rendant impossible le paiement des salaires des fonctionnaires portoricains .
La situation juridique et diplomatique de Porto Rico est complexe :
Les conditions économiques à Porto Rico se sont temporairement améliorées depuis la grande dépression due à l' investissement externe dans l' industrie onéreuse telle que des pharmaceutiques et la technologie de produits pétrochimiques .
Après avoir été les bénéficiaires du régime fiscal spécial du gouvernement des États-Unis , des industries locales doivent concurrencer ceux des régions économiquement plus pauvres du monde où les salaires ne sont pas sujets à la législation de salaire minimum des États-Unis .
Porto Rico est soumis aux lois du commerce et à des restrictions des États-Unis .
Bien que l' espagnol soit la seule langue parlée , l' anglais reste officiel du fait du rapprochement avec les États-Unis .
Il permet à un Espagnol ne connaissant que peu l' anglais et à un Anglais hispanophone de communiquer sans gros problèmes de langues .
Il est cependant surtout utilisé par des habitants des États-Unis ou hispano-américains .
La capitale San Juan , fondée en 1521 , possède une riche histoire .
L' autre grande ville de l' île , Ponce , possède une atmosphère totalement différente , beaucoup moins touristique , mais plus coloniale et plus bourgeoise , industrieuse , notamment en raison de la production historique de canne à sucre dans la région pour la fabrication de rhum .
Porto Rico est aussi le lieu d' accueil du radiotélescope d' Arecibo .
Porto Rico possède à l' est une forêt tropicale humide montagneuse préservée par un parc national nommée El Yunque .
Il existe également des lagunes naturellement bioluminescentes en trois endroits de l' île ( Fajardo , Vieques et vers Ponce ) .
Les plages de l' île ( surtout dans la partie nord-est et dans les îles de Culebra et Vieques ) sont très attractives pour différentes activités : plongée , surf ...
Porto Rico a pour codes :
