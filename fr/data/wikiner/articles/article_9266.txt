En 1890 , Édouard Branly , professeur à l' Institut catholique de Paris , s' intéresse à l' effet des ondes électromagnétiques de Hertz sur les conducteurs divisés .
En découvrant ce qu' il appelle la radioconduction , il ouvre la voie au développement de détecteurs d' ondes beaucoup plus sensibles que les boucles de Hertz .
D' autres auteurs donneront au tube de limaille de Branly le nom de cohéreur , dispositif que l' on peut considérer comme un interrupteur ( imparfait ) fonctionnant en tout ou rien sous l' effet d' ondes électromagnétiques transitoires .
Le tube à limaille en tant que résistance électrique variable avait déjà été étudié par le physicien italien Calzecchi Onesti vers le milieu des années 1880 .
Mais la grande découverte de Branly a été de constater que la conductibilité du tube à limaille augmentait brusquement lorsqu' une étincelle éclatait à quelques dizaines de centimètres de lui : on disait alors que la limaille était cohérée .
Ce détecteur d' ondes hertziennes a permis à Guglielmo Marconi de réaliser des liaisons à grande distance en radiotélégraphie .
