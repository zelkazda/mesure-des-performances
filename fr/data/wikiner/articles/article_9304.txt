Charles Racquet est un compositeur et organiste français .
Il est pendant 41 ans organiste titulaire de la cathédrale Notre-Dame de 1618 à 1659 .
Le luthiste Denis Gaultier , qui fut peut-être son élève , lui a dédié un tombeau .
