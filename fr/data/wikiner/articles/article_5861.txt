Située dans la vallée de la Saire , la commune de Brillevast a un relief accidenté et un sous-sol composé de roches dont les dominantes sont des arkoses , phyllades et grauwackes .
Comme beaucoup de communes avoisinantes , Brillevast faisait partie de la forêt de Brix .
Dans le nord du département , vast est une forme qui obéit à un traitement régional , employé isolément dans Le Vast , mais surtout en composition avec des noms de type roman ( Martinvast ) , scandinave ( Sottevast , Tollevast ) ou germanique .
La densité des noms en vast autour de Valognes est remarquable , ils y jalonnent apparemment la progression d´une conquête du sol .
Ce regroupement de ces noms en vast dans le nord Cotentin est l´exemple d´une mode locale favorisée par l´isolement géographique de la presqu´île du Cotentin .
Son second patron est saint Sébastien .
Du côté opposé existe l´ancienne chapelle des cloches , autrefois dédiée à saint Roch et saint Adrien .
C´est un clocher en bâtière typique du Cotentin .
De plus , l´autel est encadré à gauche par la statue de saint Martin , à droite par celle de saint Jean-Baptiste .
La verrière de style ogival à l´arrière , représente trois épisodes de la vie de saint Martin .
Saint Martin coupant son manteau , saint Martin célébrant la messe et saint Martin mourant .
Sur le deuxième , Sébastien est recueilli et soigné , il guerira .
Ce château résidence des anciens seigneurs de Brillevast , a été déplorablement mutilé .
Sur la cheminée , on voit encore une fresque représentant saint Hubert .
Il décéda à Valognes , le 23 août 1702 et fut enterré le lendemain , dans le chœur de l´église de Brillevast .
Les deux généraux , père et fils , reposent dans le cimetière de Brillevast .
Il venait tous les deux jours à Brillevast , paroisse qui n´avait plus de curé .
