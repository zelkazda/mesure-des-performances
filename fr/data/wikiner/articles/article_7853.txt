Le prosélytisme des origines est lié secondairement à la conversion religieuse , c' est dans cette dernière acception qu' il s' est répandu , grâce au grec de la Septante .
En mai 2008 , l' armée américaine en Afghanistan a été contrainte de confisquer et de faire brûler des bibles dans la base de Bagram .
