Christian Philip , universitaire et homme politique français , né le 2 octobre 1948 à Boulogne-Billancourt ( Hauts-de-Seine ) .
Il est aussi le frère de Thierry Philip .
Il ne se représente pas en 2007 , laissant ainsi le champ libre à la candidature de Dominique Perben dans cette circonscription , alors que ce dernier cherche à s' implanter à Lyon en vue des élections municipales de 2008 .
Perben sera effectivement élu député mais sera très largement distancé aux municipales .
