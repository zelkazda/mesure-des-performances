Le site historique de la ville se trouve dans la partie sud-est de ce territoire , sur le rebord d' un plateau calcaire dominant la Seine de 60 mètres environ et d' où la vue s' étend sur une grande partie de l' ouest parisien .
Les communes limitrophes en sont Maisons-Laffitte au nord-est , Le Mesnil-le-Roi à l' est , Le Pecq au sud-est , Mareil-Marly au sud , Fourqueux au sud-sud-ouest , Chambourcy , au sud-ouest , Poissy à l' ouest et Achères en couronne du nord-ouest au nord-nord-est .
Le plateau s' abaisse progressivement vers l' ouest à 50 à 60 mètres en limite de Poissy et vers le nord jusqu' à environ 25 mètres dans la plaine agricole d' Achères .
On compte un seul cours d' eau dans le territoire communal , il s' agit du ru de Buzot , ruisseau affluent de la Seine de 9 km de long dont le cours orienté ouest-est traverse la partie sud de la commune .
Le fond du vallon est emprunté par la déviation de la RN 13 .
Les couches affleurant à Saint-Germain-en-Laye , au niveau de la ville , sont de haut en bas :
Elles sont formées d' alluvions ( sables et graviers ) anciens au sud ( terrasse supérieure ) , sauf vers l' ouest et le nord-ouest en limite de Poissy et Achères où ces alluvions disparaissent pour laisser affleurer les couches de calcaire sous-jacentes , et d' alluvions plus récentes au nord ( terrasse inférieure ) .
L' espace rural correspond en quasi-totalité à l' espace couvert par la forêt de Saint-Germain-en-Laye .
Il comprend les zones d' habitation , concentrées dans la partie sud de la commune , soit 340 ha ( 36 % de l' espace urbain construit ) , les divers équipements , soit 265 ha ( 20 % ) , incluant notamment des surfaces occupée par la station d' épuration " Seine-Aval " du SIAAP , située dans l' extrême nord du territoire communal , les surfaces affectées au transport , 186 ha ( 14 % ) , incluant entre autres les installations ferroviaires de l' ancien triage et du dépôt de locomotives d' Achères , les zones d' activités , 95 ha ( 7 % ) , constituée majoritairement de bureaux .
Il comprend d' une part le noyau historique dense qui s' est progressivement étendu à partir du château jusqu' au bord de la vallée du ru de Buzot .
Les communications avec Paris sont assurées par la ligne de RER A dont la gare de Saint-Germain-en-Laye est le terminus ouest .
La ligne de Paris à Saint-Germain-en-Laye était la plus ancienne ligne de chemin de fer ouverte aux voyageurs en France .
Depuis peu , les gares de Saint-Germain-en-Laye -- Grande-Ceinture et de Saint-Germain-en-Laye -- Bel Air -- Fourqueux ont été rouvertes à l' occasion de la remise en service d' un tronçon de la ligne de Grande ceinture Ouest .
Les autoroutes les plus proches sont respectivement l' A 13 accessible par l' échangeur d' Orgeval situé à huit kilomètres environ à l' ouest de la ville et l' A 14 accessible par l' échangeur de Chambourcy situé à un kilomètre environ à l ' ouest .
Le tracé retenu pour le bouclage de la Francilienne à l' ouest de Paris écorne légèrement le territoire communal dans sa lisière nord mais évite la traversée de la forêt .
Plusieurs routes départementales relient Saint-Germain-en-Laye aux communes voisines .
La RN13 est isolée par des murs antibruit continus dans le vallon du ru de Buzot .
Les liaisons avec Paris sont assurées principalement par la ligne A du RER , inaugurée en 1972 , exploitée par la RATP .
Elle permet de joindre le centre de la capitale ( gare de Châtelet -- Les Halles ) en 30 minutes , depuis la gare de Saint-Germain-en-Laye , dont elle est le terminus , via La Défense ( 17 mn ) , avec une fréquence élevée , étant proche d' une ligne de métro .
Cette ligne succède à la ligne Paris -- Saint-Germain-en-Laye , dont le tronçon Paris -- Le Pecq , inauguré en 1837 , fut l' un des tous premiers chemins de fer en France .
Ultérieurement , fut construit le pont et la ligne reliant Le Pecq à Saint-Germain-en-Laye .
Cette ligne de banlieue à banlieue , sans passer par Paris , sera prolongée au nord , jusqu' à Achères ( liaison avec le réseau Saint-Lazare et le RER A , branches de Cergy -- Le Haut et de Poissy ) et au sud .
L' accès routier à Saint-Germain-en-Laye et la circulation dans la ville sont assez difficiles du fait de l' encombrement , notamment en semaine aux heures de pointe , du réseau constitué de rues au tracé très ancien .
Il sera accompagné de la mise à 2x2 voies de la RN 13 entre Saint-Germain-en-Laye et le nouvel échangeur .
Saint-Germain-en-Laye jouit comme toute l' Île-de-France d' un climat océanique dégradé .
Peu de vestiges archéologiques ont été retrouvés dans le territoire de la commune , longtemps occupé par la vaste forêt d' Yveline .
Néanmoins on peut voir , reconstituées dans les fossés du château , des sépultures néolithiques découvertes dans les environs , dont l' allée couverte venant de la commune limitrophe de Conflans-Sainte-Honorine .
Selon la tradition hagiographique , saint Érembert mit fin , miraculeusement , à un incendie en dressant sa houlette devant les flammes .
Ce monastère est rattaché à l' abbaye bénédictine Notre-Dame de Coulombs ( près de Nogent-le-Roi ) .
En 1040 , Henri II en donna le gouvernement temporel au chapitre de la cathédrale .
Saint Louis agrandit le château et fait construire la Sainte Chapelle achevée en 1238 .
Cet édifice encore visible actuellement est parfois attribuée , sans preuve , à l' architecte Pierre de Montreuil , auteur de la Sainte-Chapelle de Paris .
En 1286 , sous Philippe IV Le Bel , le village devient une prévôté , premier degré de la justice royale .
Vingt ans plus tard , sous Charles V , il sera reconstruit et transformé en forteresse par l' architecte Raymond du Temple .
Avec François I er , qui épouse Claude de France dans la chapelle le 18 mai 1514 , le château de Saint-Germain-en-Laye devient la résidence favorite du roi .
Henri II , né à Saint-Germain-en-Laye , devient roi en 1547 .
les travaux sont commencés en 1559 , mais la construction ne sera terminée que sous le règne d' Henri IV , vers 1600 .
Charles IX , en 1561 , y établit une manufacture de glaces .
En 1599 , Henri IV exempta les habitants de toutes charges , privilège qui dura jusqu' en 1789 .
Le dimanche 5 septembre 1638 , c' est la naissance très attendue de Louis Dieudonné , futur Louis XIV .
De 1661 à 1682 , le roi Louis XIV passe une partie importante de son temps à Saint-Germain-en-Laye .
En 1680 commencent les travaux d' agrandissement du château , menés par Jules Hardouin-Mansart , par la construction de cinq pavillons d' angle qui lui donnent , selon certains historiens , un " aspect bizarre et déplaisant " .
Madame de Montespan y fait construire l ' " hôpital général royal " ainsi que le couvent des Ursulines .
L' église , brûlée en 1346 , rebâtie depuis , réparée par Charles IX , en 1562 , agrandie en 1677 , s' écroula en 1681 .
Louis XIV ordonna d' en rebâtir une nouvelle , qui fut achevée en 1683 .
Saint-Germain-en-Laye connaît alors une phase de déclin prolongé malgré le séjour du roi d' Angleterre Jacques II , cousin germain de Louis XIV , qui vit en exil au château , de 1689 à sa mort en 1701 .
En mars 1787 , un édit de Louis XVI crée les municipalités , dirigées par un syndic .
Versailles , qui offre l' avantage de disposer de vastes bâtiments inoccupés , est choisie comme chef-lieu de la Seine-et-Oise nouvellement créée .
Sous la Révolution , la ville connaît un net déclin démographique , perdant un tiers de sa population , tant du fait d' un solde naturel négatif que du départ de nombreux habitants .
Il accueille l ' " école spéciale militaire de cavalerie " qui est ouverte le 15 octobre 1809 et fusionnée en 1914 avec l' école spéciale militaire de Saint-Cyr .
La ville est occupée en 1814 et 1815 par les troupes alliées ( russes , prussiennes et britanniques ) qui ont vaincu Napoléon .
Le 24 août 1837 , première circulation sur la ligne de chemin de fer Paris -- Saint-Germain , première ligne ouverte au service des voyageurs en France , qui est en fait limitée au débarcadère du Pecq près du pont sur la Seine .
L' implantation de la gare sur la place du château bouleverse le jardin créé par Le Notre .
À cet effet , le château est classé monument historique le 8 avril 1863 et les premiers travaux de rénovation sont engagés par l' architecte Eugène Millet , élève de Viollet-le-Duc .
Les premières salles du musée sont inaugurées par Napoléon III le 18 mai 1867 .
Une statue de Thiers est érigée en 1880 sur la place du château .
Cette ligne de 18,7 kilomètres , à traction à vapeur , relie le château à la place de l' Étoile via Rueil-Malmaison et le pont de Neuilly en 1 h 30 environ .
Pendant la Première Guerre mondiale , Saint-Germain-en-Laye , bien qu' incluse dans le périmètre du camp retranché de Paris , n' est pas affectée directement par les combats .
Quelques bombes , lancées par des Zeppelins , visant le viaduc de la ligne de Grande ceinture tombent sans provoquer de victimes .
Dès 1914 , des trains militaires venant directement du front sont reçus en gare de Saint-Germain-Grande-Ceinture .
En 1917 , la gare de triage d' Achères est agrandie pour recevoir les trains de permissionnaires ou de blessés .
Le monument aux morts , œuvre de l' architecte saint-germanois Jacques Carlu , est inauguré le 24 septembre 1922 .
Lors de la Seconde Guerre mondiale , la ville est à nouveau occupée par l' armée allemande du 14 juin 1940 au 25 août 1944 .
Saint-Germain-en-Laye devient ensuite le siège de l' Ob West , commandement des forces allemandes de la Norvège à Biarritz .
De nombreux bunkers sont construits à partir de 1943 par des ouvriers réquisitionnés pour l' Organisation Todt , en particulier des bunkers enterrés destinés au commandement et situés près de la rue Félicien David .
Le 7 novembre 1962 est créé l' arrondissement de Saint-Germain-en-Laye qui regroupe 45 communes pour 341 km² , soit environ 15 % de la superficie du département et 528 000 habitants ( 1999 ) , soit 42 % de sa population .
Le 25 mars 1965 , Malraux fait visiter les nouvelles salles au général de Gaulle au cours d' une visite privée , mais la rénovation complète ne sera achevée qu' en 1984 .
Ce tracé , qui respecte l' environnement naturel et culturel , est le résultat d' un long combat du maire , Michel Péricard , qui convainc le président de la république , François Mitterrand .
Le 4 août 2007 , un accident de manège à la Fête des Loges , faisant deux morts et deux blessés graves , endeuille la commune .
Louis XVIII , en 1825 la décréta succursale de celle de Saint Denis .
Avec un revenu revenu annuel médian par unité de consommation de 25499 euros en 2004 , elle se place au 124 e rang des communes de plus de 1000 habitants , devant Versailles ( 131 e ) .
Saint-Germain-en-Laye est un pôle d' emploi important avec près de 19 000 emplois en 1999 pour une population de 38 000 habitants , soit un emploi pour deux habitants .
Le taux de chômage était de 7,4 % en 2005 , un chiffre légèrement supérieur à la moyenne des Yvelines ( 7,1 % ) , mais inférieur à la moyenne nationale ( 8,6 % ) .
Saint-Germain-en-Laye est considéré comme le " plus grand centre commercial à ciel ouvert de l' ouest parisien " avec plus de 800 commerces implantés surtout en centre-ville .
L' accès au centre ville est facilité par la présence de la station RER en plein centre , en revanche l' accès routier est rendu plus difficile par l' exiguïté des rues et l' insuffisance des parcs de stationnement .
Ce commerce est en concurrence avec des centres commerciaux extérieurs , notamment la zone commerciale d' Orgeval le long de la route nationale 13 et Parly 2 au Chesnay .
Mise en service en 1940 , c' est la plus grande station d' épuration d' Île-de-France .
Historiquement , Saint-Germain-en-Laye a été une importante ville de garnison et elle compte actuellement plus de 600 militaires .
La ville est depuis 1962 le siège d' une sous-préfecture , d' abord de Seine-et-Oise , puis depuis 1968 des Yvelines .
L' arrondissement de Saint-Germain-en-Laye comprend 45 communes , soit 15 % de la superficie des Yvelines , et représentait , au recensement de 2007 , avec 550 428 habitants , 39,2 % de la population du département .
Pierre Morange , qui a succédé dans cette fonction à Michel Péricard , est par ailleurs maire de Chambourcy depuis 1995 .
Saint-Germain-en-Laye est une ville qui a une longue tradition politique bien ancrée à droite .
Le maire Emmanuel Lamy ( UMP ) a succédé à Michel Péricard ( RPR ) en 1999 , au décès de ce dernier , puis il a été élu pour la première fois comme tête de liste en 2001 .
Au second tour , les électeurs ont voté à 87,9 % pour Jacques Chirac contre 12,1 % pour Jean-Marie Le Pen avec un taux d' abstention de 18,6 % , résultat plus contrasté qu' au niveau national ( respectivement 82,21 % et 17,79 % ; abstention 20,29 % ) .
Ces chiffres amplifient la tendance départementale des Yvelines ( oui à 59,53 % ; non à 40,47 % ) et celle de la région Île-de-France ( oui 53,99 % ; non 46,01 % ) .
Le second tour a vu Nicolas Sarkozy arriver en tête à une très large majorité de 66,95 % contre 33,05 % pour Ségolène Royal ( résultat national : respectivement 53,06 et 46,94 % ) .
La part communale des trois principale taxes locales est relativement modérée ( à peine plus élevée qu' à Versailles ) avec les taux suivants en 2006 : 11,68 % pour la taxe d' habitation , 9,5 % pour la taxe foncière sur les propriétés bâties et 10,35 % pour la taxe professionnelle .
En 1999 , 3,2 % des résidences principales dataient de 1990 ou après contre 9,1 % en Île-de-France , démontrant un fléchissement des constructions depuis 1990 .
Saint-Germain-en-Laye a connu un développement ancien grâce à son statut de ville royale .
Entre 1990 et 1999 , Saint-Germain-en-Laye a vu sa population diminuer de 1503 personnes , soit -3,8 % .
