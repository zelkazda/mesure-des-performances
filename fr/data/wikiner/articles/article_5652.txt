La glasnost est une politique de liberté d' expression et de la publication d' informations , introduite par Mikhaïl Gorbatchev en URSS en 1985 .
Le but de Gorbatchev avec la glasnost fut en partie de mettre la pression sur les conservateurs du parti qui étaient opposés à sa politique de restructuration économique ( la perestroïka ) .
Cependant , la perestroïka va accentuer la crise économique et va précipiter l' URSS et sa population dans des conditions de vie très difficiles , ce qui va provoquer une montée des contestations .
A travers cette politique , Gorbatchev approfondit la déstalinisation , politique mise en place par Khrouchtchev .
Malgré cela , le but premier de Gorbatchev n' aboutit pas et le régime , largement basé sur la contrainte étatique , s' effondra à l' été 1991 .
