Le drapeau de l' Argentine est constitué de deux bandes bleu ciel horizontales séparées par une bande blanche .
D' après la tradition , il a été créé en 1812 par l' intellectuel devenu général Manuel Belgrano quand il regarda le soleil devant les côtes du Paraná , depuis la ville de Rosario .
Ainsi , les drapeaux du Salvador , du Honduras et du Nicaragua ont conservé les trois bandes horizontales bleu-blanc-bleu , seul le motif central et les nuances de bleus variant de l' un à l' autre ; celui du Guatemala reprend les trois bandes mais verticalement .
