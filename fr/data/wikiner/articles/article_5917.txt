Entré au conseil municipal de Lyon en 1904 , il devient adjoint , puis maire le 3 novembre 1905 , en succédant à Jean-Victor Augagneur .
Son successeur est Louis Pradel .
Il s' engage dans l' affaire Dreyfus aux côtés d' Émile Zola et Anatole France , et fonde la section lyonnaise de la Ligue des droits de l' homme .
Le cabinet Briand est immédiatement renversé par une telle offensive , et Herriot est nommé président du conseil .
Son propre gouvernement ne dure pas trois jours et il cède la place à Poincaré , signant la fin du Cartel des gauches .
Herriot est devenu beaucoup plus modéré et l' on a dit que son gouvernement a été le dernier à laisser une impression de calme [ réf. nécessaire ] .
Son gouvernement tombe le 14 décembre 1932 sur la question du remboursement de la dette française à l' égard des États-Unis .
Herriot s' abstient lors du vote sur les pleins pouvoirs au maréchal Pétain le 10 juillet 1940 .
Il est élu membre de l' Académie française le 5 décembre 1946 , le dernier des quatorze nouveaux élus de cette année-là .
Aujourd'hui , de nombreux établissements scolaires portent le nom d' Édouard Herriot .
Mais c' est sans doute François Mauriac qui a le mieux dépeint Herriot .
