Le président actuel est Shimon Peres .
Un vote à la majorité absolue ( 61 voix ) de la Knesset désigne directement le nouveau président .
Tout citoyen israélien qui réside en Israël peut être éligible .
Le terme du mandat présidentiel peut être provoqué par la démission du président ou par une décision prise par les trois quarts des parlementaires de la Knesset pour le destituer pour sa conduite ou pour incapacité à conduire le mandat .
