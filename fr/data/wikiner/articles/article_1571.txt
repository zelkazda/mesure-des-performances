Il s' agit du volet gauche d' un diptyque représentant la crucifixion et conservé au Metropolitan Museum of Art de New York .
À la mort de celui-ci , en janvier 1425 , il rejoint en Flandre son frère , Hubert van Eyck , également peintre .
Il entre alors au service du duc de Bourgogne , Philippe le Bon , le 19 mai 1425 , comme peintre et " valet de chambre " , avec un salaire annuel fixe qui lui fut régulièrement attribué une rente annuelle jusqu' à sa mort .
Il est fixé à Lille , centre administratif du duché , avant le 2 août 1425 .
Ces données attestent que pendant les années 1426 et 1427 , Van Eyck a fait , au moins , deux voyages lointains dont un pèlerinage .
Il est possible que certains de ces paiements concernent aussi sa participation à une ambassade à Valence , en 1426 .
Un rapport de l' époque confirme que Van Eyck en faisait partie .
En 1434-1435 , le magistrat de la ville de Bruges rétribue Van Eyck pour la polychromie de six statues et leur baldaquin à la façade de l' hôtel de ville .
En septembre 1434 , il se produisit un incident administratif : la chambre des comptes de Lille refusa de payer Van Eyck ; le 13 mars 1435 , le duc intervint personnellement en faveur de son peintre .
Il s' agit vraisemblablement d' une mission en terre non chrétienne , liée aux projets de croisade de Philippe le Bon .
Il n' est pas à exclure que Van Eyck ait eu à faire des reconnaissances de voies et de territoires et à les porter sur une carte .
Rien pratiquement ne subsiste des commandes que Van Eyck exécuta pour Philippe le Bon .
Il a travaillé à la décoration des résidences de Hesdin ( 1431 ou 1432 ) , de Bruxelles ( 1433 ) , et de Lille ( 1434 ) .
Il n' est pas exclu que le duc ait commandé à Van Eyck des œuvres décoratives à caractère héraldique .
L' œuvre de Jan van Eyck , en dehors de ce chef d' œuvre exceptionnel , est composée surtout de représentations de la vierge Marie et de portraits .
Van Eyck a ainsi été considéré comme le fondateur du portrait occidental .
Le miroir convexe au centre du tableau reflète deux personnages , peut-être les deux témoins , dont l' un serait Van Eyck lui-même .
On peut alors voir dans cette œuvre un véritable certificat de mariage , d' ailleurs signé par van Eyck lui-même .
L' apport technique de Van Eyck à la peinture occidentale est capital .
Le liant utilisé par Van Eyck était à base d' huile siccative et d' un autre élément qui rendait le liant consistant , ce qui était l' une des difficultés rencontrés par les utilisateurs de la peinture à l' huile auparavant .
