Elle traverse les départements du Bas-Rhin et du Haut-Rhin , depuis Marlenheim au nord jusqu' à Thann au sud .
Dans le département du Bas-Rhin :
Dans le département du Haut-Rhin :
Une couche sédimentaire de craie , de marne ou de grès , recouvre des roches anciennes : granit des Vosges , gneiss ou ardoise .
La viticulture est attestée du temps des Celtes .
Une association de vignerons de Riquewihr décida alors de la date officielle de début des vendanges , et définit les cépages à planter .
La guerre de Trente Ans mit fin à cette période faste , et apporta dans la contrée la guerre , la famine et la peste .
