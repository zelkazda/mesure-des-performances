Une vingtaine d' années plus tard , on trouve une description plus élaborée des passiflores dans l' ouvrage du médecin botaniste espagnol Nicolas Monardes publié en 1569-1574 .
Il fut probablement aussi le premier a employer le terme religieux de flos de passionis " fleur de la passion " pour la désigner car la fleur était selon lui , " précisément faite pour représenter la Passion du Christ " .
