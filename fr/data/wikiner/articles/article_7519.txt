Non , car il n' est point dit que Saint-Aubin est in parochia , mais in villa , ce qui est très différent .
En 1790 , le recteur de Saint-Aubin déclare que son tiers de dîmes est estimé 760 livres , et qu' il jouit en outre du presbytère et de son pourpris , contenant 7 journaux de terre labourable et 3 journaux de prairies , valant 300 livres ; c' est donc un revenu total de 1 060 livres de rente , dont il faut vraisemblablement déduire les charges .
La seigneurie de Saint-Aubin-du-Pavail relevait de la baronnie de Châteaugiron .
