Auguste Comte a été secrétaire particulier , puis disciple du comte de Saint-Simon .
Auguste Comte étudie au lycée de Montpellier .
Il est reçu à 16 ans à l' École polytechnique dans les premiers en 1814 .
À la Restauration , en avril 1816 , toute sa promotion est congédiée pour manque de discipline par le comte de Vaublanc .
Il trouve en 1817 un poste de secrétaire auprès de Saint-Simon ( Claude-Henri de Rouvroy , comte de Saint-Simon , à ne pas confondre avec le duc de Saint-Simon auteur des mémoires de la cour de Louis XIV ) , avec qui il collabore sur différents ouvrages jusqu' à une rupture orageuse en 1824 .
Un mariage civil est d' abord célébré , puis un mariage ecclésiastique suite aux instances de la mère de Comte .
Il rencontre Félicité Robert de Lamennais .
Il assiste à l' enterrement de Saint-Simon et participe vers fin 1825 -- début 1826 aux réflexions sur la nécessité d' une nouvelle doctrine générale .
Il erre pendant dix jours à Montmorency d' où il écrit une note à M. de Blainville .
C' est de cette façon qu' il rencontre en 1844 Clotilde de Vaux , sœur de l' un de ses plus célèbres élèves Maximilien-Marie de Ficquelmont .
Mais , Clotilde meurt l' année suivante , le 5 avril , de la tuberculose .
Au niveau politique , il s' enthousiasme pour la Révolution de 1848 .
Il soutient le coup d' État de 1851 après avoir été très critique vis-à-vis du prince Louis-Napoléon , ce qui provoque le trouble chez ses disciples .
Ainsi , Littré prend ses distances .
Ami personnel de John Stuart Mill , il a vécu de ce que celui-ci lui a versé avant qu' ils ne se brouillent .
Il meurt le 5 septembre 1857 et est inhumé au cimetière du Père-Lachaise .
Auguste Comte a eu un enfant d' une première femme , qu' il n' a pas élevé .
Une statue d' Auguste Comte a été inaugurée en 1902 place de la Sorbonne , sous la présidence du général André , en présence de membres de la société positiviste .
Claude Allègre fit déplacer la statue et la fit pivoter de 90° , de sorte qu' elle " tourne presque le dos à la Sorbonne " .
Dans son livre Auguste Comte et le positivisme , John Stuart Mill résume la doctrine positiviste de Comte d' une manière à la fois claire et synthétique :
Comme Mill l' indique par la suite , " M .
Comte ne revendique aucune originalité pour cette conception du savoir humain .
On peut noter que le terme même de positivisme n' est nullement l' invention de Comte .
Saint-Simon employait déjà le terme de positivisme ; Auguste Comte , qui fut son secrétaire pendant six ans , l' a étendu à la philosophie .
Si les fondements de la philosophie positive ne sont donc nullement une découverte d' Auguste Comte ( ce qu' il n' a jamais nié ) , il a apporté à cette doctrine un nouveau tour en montrant ce qu' elle n' était pas .
John Stuart Mill confirme cette idée :
Comte a pris garde que nous fassions ainsi .
En plus d' affirmer l' existence de ces trois grands modes de pensée , Comte propose une loi concernant l' évolution de chaque grande classe des connaissances humaines : celles-ci passent par trois états , de l' état théologique vers l' état métaphysique , puis vers l' état positif ; l' état métaphysique , s' il n' en est pas moins nécessaire , n' étant qu' une étape de transition entre les deux autres modes .
De cela , Comte déduit que le mode de pensée positif est " destiné à prévaloir finalement par l' effet de la conviction où l' on arrivera universellement que tous les phénomènes , sans exception , sont gouvernés par des lois invariables , avec lesquelles aucune volonté naturelle ou surnaturelle n' entre en lutte . "
Pour certains , la position de Comte révèlent toutefois certaines ambiguités lorsqu' il se réclame de Kant et de Leibniz pour affirmer qu' il existe chez l' homme des " dispositions mentales " spontanées et fait référence à un bon sens ou à une " raison commune spontanée " chez l' homme .
La philosophie d' Auguste Comte peut se décomposer en deux phases qui correspondent aussi à chacune des femmes qu' il a connues .
Dès cette époque , Comte commence à s' intéresser aux principes d' organisation sociale , en créant le terme de " sociologie " en 1839 .
La deuxième phase , qui se déroule de 1846 à 1857 , correspond à ce que l' on appelle quelquefois le positivisme religieux , en raison des applications politiques que Comte tire de sa doctrine : sacerdoce et prêtrise positivistes , culte de la science et de l' humanité , calendrier avec les noms des grands savants , organisation de la société par et pour la science .
On peut noter que de nombreux disciples et/ou admirateurs de la philosophie d' Auguste Comte ont rejeté dans les grands traits la deuxième partie de son travail .
-- Emile Littré , préface d' Auguste Comte et la philosophie positive John Stuart Mill , dans la deuxième partie du livre qu' il lui consacre ( intitulée " dernières spéculations d' Auguste Comte ) , s' étonne du ridicule de certaines propositions et prescriptions , au point de parler d' une " dégénération intellectuelle " chez Comte .
Auguste Comte y expose une théorie dite loi des trois états .
Pour Auguste Comte , le positivisme est lié à l' émergence de l' âge de la science caractéristique de " l' état positif " qui succède , dans la loi des trois états , à " l' état théologique " et à " l' état métaphysique " .
Partant du fait que " les phénomènes observables peuvent être classés dans un très-petit nombre de catégories naturelles " correspondant aux différentes sciences , Auguste Comte en a proposé une classification rationnelle à partir d' une comparaison de ces différents phénomènes .
Dans cette phase , Auguste Comte cherche à concilier les principes de la rationalité scientifique avec l' amour humain , qu' il a découvert par sa rencontre avec Clotilde de Vaux .
Après la mort de Clotilde ( 1846 ) , il lui voue un culte qu' il qualifie de fétichisme .
Comte fut en effet influencé à ce stade de sa pensée par les études de l' ethnologue Charles de Brosses sur le fétichisme des peuples dits primitifs .
On ne peut donc pas considérer que Comte ait été à l' origine du racialisme , puisque ces doctrines furent développées ultérieurement .
Dans cette phase , Comte considère que sa vie privée concerne toute l' humanité .
D' après Raymond Aron reprenant l' analyse d' Auguste Comte : " L' homme a besoin de religion parce qu' il a besoin d' aimer quelque chose qui le dépasse .
Comte est amené à définir une morale , qu' il fonde sur l' ordre , le progrès et l' altruisme .
Comte développe les principes de la sociologie dans son Système de politique positive , publié entre 1851 et 1854 .
Cette réglementation devait , bien entendu , comme c' est le cas , partout et toujours , de toute réglementation , consister principalement en interdictions , et Comte a tracé d' avance le programme de quelques-unes d' entre ces dernières .
John Stuart Mill qui a consacré un livre à Auguste Comte ( Auguste Comte et le positivisme ) pensait que les objections de ce genre étaient " fondées sur une compréhension imparfaite ou plutôt sur un simple premier coup d' oeil " :
C' est par les milieux médicaux de la société positiviste que la pensée d' Auguste Comte s' est tout d' abord développée et a contribué à l' émergence d' une médecine positive .
D' autres ouvrages comme le " catéchisme " positiviste ou la synthèse subjective ont également diffusé cette doctrine , notamment auprès de Charles Maurras .
Dans le monde anglo-saxon , le positivisme s' est manifesté par certaines formes d' altruisme , qui à travers John Stuart Mill rejoignent les théories utilitaristes de Jeremy Bentham .
Herbert Spencer a aussi subi l' influence positiviste .
Les États-Unis ont été influencés à travers le positivisme anglais .
Auguste Comte croit que l' ensemble des phénomènes observables sont soumis à des lois causales immuables dans le temps et l' espace et que le but de la science est de rechercher ces lois ( ce qui correspond au travail d' analyse ) .
On a vu aussi les quatre ordres que distingue André Comte-Sponville .
René Rémond parle de positivisme à travers certaines formes d' esthétique .
Auguste Comte a propagé une représentation du monde héliocentrique .
