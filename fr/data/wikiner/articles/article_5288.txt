Issu du croisement du blues , du ragtime et de la musique européenne , le jazz est considéré comme la deuxième forme musicale spécifique à s' être développée aux États-Unis , après le blues .
Le groupe endossa l' adjectif lors de ses engagements à New York en 1914 et le terme se répandit progressivement jusqu' à Chicago avant de revenir en Nouvelle-Orléans sous la forme d' une lettre de Freddie Keppard à King Oliver qui le popularisera dès 1917 avec son protégé , Louis Armstrong .
La diffusion du mot " jazz " est largement associée à son apparition sur le premier enregistrement du style , en mars 1917 par l' Original Dixieland Jass Band .
Parallèlement , le ragtime apparaît , style de piano incarné par Scott Joplin , musique syncopée influencée par la musique classique occidentale .
Dans les années 1920 , le stride se développe à Harlem .
Le boogie-woogie se développe à la même époque à Chicago .
C' est à la Nouvelle-Orléans que l' on fait en général naître le jazz , avec les formations orchestrales des " brass bands " , mélange de marches militaires revisitées par les noirs américains et les créoles , qui privilégie l' expression collective .
Le premier enregistrement de jazz voit le jour en mars 1917 par l' Original Dixieland Jass Band .
C' est l' ère des big bands de Duke Ellington , Count Basie , Glenn Miller , avec un répertoire marqué par les compositions de George Gershwin , Cole Porter , Richard Rodgers etc .
Les grands solistes de cette époque sont Coleman Hawkins et Lester Young .
Art Blakey , Horace Silver ou Sonny Rollins y participent .
D' autres personnalités inclassables émergent : Bill Evans , Charles Mingus , Oscar Peterson …
À la fin des années 1950 , les structures harmoniques et l' improvisation sont portées à leurs limites par John Coltrane .
Les grandes figures en sont Miles Davis , Frank Zappa ou encore le groupe Weather Report .
Au même moment , la création de la maison de disques ECM à Munich participe à la création et à la diffusion d' un jazz plus " européen " , aux sonorités plus feutrées et subtiles , inspiré par la musique classique , la musique contemporaine et les musiques du monde .
Jan Garbarek , John Surman , Louis Sclavis , Kenny Wheeler en sont quelques représentants .
Parmi les peintres , Sacha Chimkevitch , auteur notamment de plusieurs affiches de festivals a été salué par Franck Ténot et certains musiciens de jazz , qui disent retrouver leur " swing " dans ses toiles [ réf. nécessaire ] .
