Villars-les-Dombes est une commune française , située dans le département de l' Ain et la région Rhône-Alpes .
La ville se situe dans la partie sud du département de l' Ain , à mi-chemin entre Lyon ( 33 km ) et Bourg-en-Bresse ( 29 km ) , au cœur de la région de la Dombes connue pour ses nombreux étangs propices à la pisciculture ainsi que pour son parc ornithologique , le Parc des oiseaux .
Les nombreux étangs qui composent la Dombes ne sont pas étrangers à cette particularité .
Villars-les-Dombes indique que le village est situé près de la ( principauté de ) Dombes , sans en faire partie .
Son ancien nom mettait l' accent sur son appartenance à la Bresse , dépendant de la Savoie , par opposition au Dauphiné .
En 1565 , Villars fut érigé en marquisat dépendant de la maison de Savoie au bénéfice de son fils Honorat II de Savoye .
Sa postérité s' est éteinte , en 1788 , par la mort de son fils Honoré-Armand , 2 e duc de Villars .
