Georgius Agricola décrivait déjà l' existence du fluor en 1530 mais il n' a été isolé qu' en 1886 par Henri Moissan .
Scheele a réuni tous les résultats de ses expériences dans ses travaux et a montré comment identifier cet acide .
Ce nom ne fut accepté qu' en Grèce .
Gay-Lussac et Thénard sont les premiers à essayer d' isoler cet élément .
Humphry Davy a cherché à montrer que l' acide fluorhydrique ne contenait pas d' oxygène .
Edmond Frémy démontrera ensuite que l' action du chlore sur le fluor ne l' isole pas .
Antoine Lavoisier fit lui aussi des expériences avec de l' acide fluorhydrique ( solution de fluorure d' hydrogène HF dans l' eau ) .
La première densité du fluor a été calculée par Henri Moissan à partir d' un expérience où il a recueilli du fluor et de l' air puis a rempli de flacon de platine .
L' Organisation mondiale de la santé , dans un rapport de novembre 2006 , attire l' attention sur les dangers des doses excessives de fluor dans l' alimentation et explique comment fabriquer et utiliser des filtres permettant de neutraliser le fluor dans l' eau de boisson .
