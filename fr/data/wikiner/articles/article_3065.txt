Située à 42 kilomètres au sud de l' équateur , l' île ne mesure que 21.3 km 2 de superficie et est formée d' un plateau central peu élevé culminant à 71 mètres d' altitude au Command Ridge ceinturé par une étroite plaine côtière .
Sur cette plaine se concentrent les logements et les infrastructures industrielle , agricoles , publiques et de transport , l' intérieur des terres étant majoritairement dévolu à l' extraction du minerai de phosphate qui constitue la seule richesse naturelle de Nauru .
L' étymologie de " Nauru " est incertaine .
Le conflit prend fin le 16 avril 1888 lorsque l' Empire allemand annexe Nauru sous prétexte de rétablir la paix .
Le statut de Nauru sera un point d' achoppement entre négociateurs de l' Empire britannique .
Dans les faits , seule l' Australie administre la colonie , , .
L' extraction du phosphate se poursuit tout au long de la Première Guerre mondiale mais c' est durant l' entre-deux-guerres que la production décolle , la demande des agriculteurs australiens et néo-zélandais s' accroissant .
Ils la fortifient et font construire par des travailleurs forcés une piste d' atterrissage qui sera la base de l' actuel aéroport international de Nauru .
Courant 1943 , les Américains bombardent l' île dans le cadre de leur reconquête des îles du Pacifique , mais n' y débarquent pas .
Les habitants et occupants de Nauru , coupés des lignes d' approvisionnement japonaises , commencent alors à manquer de ravitaillement .
Le 13 septembre 1945 , onze jours après la capitulation du Japon , la garnison de Nauru signe sa reddition .
Les derniers déportés des Îles Truk , qui ne sont plus que 737 , sont rapatriés sur Nauru le 31 janvier 1946 , .
Les Nations unies réattribuent en 1947 Nauru à l' Empire britannique et son administration à l' Australie , .
En 1989 , Nauru porte plainte devant la Cour internationale de justice contre l' Australie , réclamant compensation pour la destruction du centre de l' île provoquée par l' extraction de phosphate .
Depuis 2004 , une nouvelle majorité déclare cesser les activités qui font de Nauru un paradis fiscal et lancer des plans de restructuration de l' économie nauruane .
Le point culminant de Nauru est le Command Ridge , avec 71 mètres d' altitude .
L' hydrographie est quasiment inexistante sur l' île à l' exception de la lagune Buada , un lac qui se trouve sur le plateau et qui accueille sur ses rives quelques cultures et une petite partie de la population .
La population de Nauru est concentrée sur la bande côtière de l' île , formant un ruban urbain presque continu avec des densités moindres au nord-est .
L' unique autre foyer de population est centré autour de la lagune Buada , le reste du centre de l' île étant constitué d' un plateau calcaire rendu inculte et extrêmement aride suite à l' exploitation de son phosphate .
Une route circulaire longeant la côte de 12 km fait le tour de l' île et une autre permet de rejoindre la lagune Buada .
Nauru , très éloignée des principaux archipels du pacifique , ne dispose pas de ports en eau profonde .
Ceci a conduit à la construction près du port d' Aiwo de deux structures en porte à faux s' avançant en pleine mer permettant de charger les phosphatiers ancrés plus loin .
Quelques vols hebdomadaires de la compagnie nationale Our Airline sont assurés depuis l' aéroport international de Nauru , le seul de l' île .
La végétation tropicale est relictuelle sur le littoral et autour de la lagune Buada mais relativement absente au centre de l' île à la suite de l' exploitation minière .
Quelques espèces endémiques ou indigènes se rencontrent sur Nauru mais leur survie est compromise par la destruction de leurs milieux ( exploitation minière , pollution ) et par l' introduction d' espèces invasives ( chien , chat , poule , rat polynésien ... ) .
Nauru ne possède pas de division territoriale correspondant aux communes .
Nauru est une démocratie parlementaire .
Le système politique du pays se rapproche du bipartisme libre , les deux principaux partis étant le Parti démocratique et le Parti de Nauru .
Entre 1999 et 2003 une série de votes de défiance et d' élections amenèrent René Harris et Bernard Dowiyogo à diriger le pays en alternance .
Bernard Dowiyogo mourut alors qu' il dirigeait le pays le 10 mars 2003 à Washington à la suite d' une opération cardiaque .
Ludwig Scotty fut élu président le 29 mai 2003 , faisant penser que la période d' instabilité politique allait s' achever .
À nouveau en minorité en 2004 à la suite de l' état de faillite dramatique de sa république , René Harris fut battu une nouvelle fois par Ludwig Scotty , qui déclare l' état d' urgence et dissout le parlement après le rejet de son budget .
Marcus Stephen lui succède le 19 décembre 2007 .
Le 15 décembre 2009 , Nauru reconnaît l' indépendance de l' Abkhazie et de l' Ossétie du Sud moyennant 50 millions de dollars pour financer les besoins essentiels du pays .
Nauru a profité durant 30 ans de la richesse apportée par le phosphate ( richesse nationale ) .
En 2006 elle reprend après des travaux de remise à niveau des infrastructures minières menés par une entreprise minière australienne en partenariat avec la RONPHOS , la nouvelle entreprise publique du phosphate à Nauru .
Cette nouvelle forme d' extraction ainsi que la vente de gravier obtenue après concassage et vendu aux petites nations océaniennes voisines devrait assurer à Nauru des rentrées d' argent pour 30 ans .
Cependant , le dernier recensement effectué sur Nauru en 2002 donne une population totale de 9872 et certaines sources estiment que la population de l' île est située en 2007 dans une fourchette de 7500 à 8000 résidants en raison du rapatriement de la plupart des travailleurs Gilbertins et Tuvaluans courant 2006 .
Nauru possède le plus fort taux mondial de diabète de type 2 ( 40 % de la population est affectée ) .
Il existe un campus de l' Université du Pacifique Sud sur l' île .
Avant que ce dernier ne soit construit , les étudiants devaient se rendre en Australie pour faire des études universitaires .
L' Australie qui balaie en 1914 la colonie allemande et a administré l' île jusqu' en 1968 ( excepté durant la période japonaise de 1942-45 ) occupe un statut d' ancienne métropole pour Nauru .
Nauru a pour codes :
