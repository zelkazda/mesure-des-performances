La construction des consoles de jeu est similaire à celle des ordinateurs , depuis Pong en passant par la Nintendo 64 et toutes les consoles de jeu portables ( PSP , DS , Game Gear , et autres ) .
En effet , la première console à être compatible avec le système d' exploitation Microsoft Windows fut la Dreamcast , lancée en 1998 .
Sur la même architecture de base se sont greffées au fil du temps diverses extensions , comme des processeurs graphiques additionnels ( avec celui de la première PlayStation par exemple ) , des modems ou des disques durs .
La frontière entre ordinateur , centre multimédia et console de jeu n' est plus aussi marquée depuis la Playstation 2 qui propose -- pour la première fois concernant une console -- d' autres fonctionnalités que le jeu vidéo en permettant nativement de lire les DVD vidéo .
Avant cela , Sega et d' autres avaient déjà fait de timides tentatives de consoles/plate-forme multimédia , sans grand écho .
Une étape est également franchie avec l' apparition de la Xbox et de son disque dur intégré en standard .
La Wii , qui est sorti en 2006 , propose un ensemble doté de deux manettes sans-fil ( la Wiimote et le nunchuk ) ayant un gyroscope et un accéléromètre intégrés pouvant détecter les mouvements du joueur .
De plus , depuis la sortie des consoles de septième génération comme Xbox 360 , Wii , ou PlayStation 3 , les consoles de salon incluent des systèmes d' exploitation plus évolués permettant des mises à jour via une connexion à Internet .
L' exemple le plus marquant est sans doute le cas d' Xbox 360 se basant sous un système d' exploitation se rapprochant beaucoup du système Windows de Microsoft .
Ce que cette dernière décennie tend à prouver étant donné que les grands rivaux Sony , Nintendo et Microsoft ont évincé Sega par les lois du marché et que le marché du jeu vidéo se sature et se resserre autour de ces trois acteurs .
Une guerre commerciale et marketing les oppose depuis les lancements de la Wii , la Xbox 360 et la PS3 , et dont les armes principales deviendront inéluctablement la diversification sectorielle .
Si le quasi-monopole de Microsoft dans le domaine informatique peut suffire à assurer des retours sur investissements dans sa branche jeux vidéo et console de jeux , Nintendo dispose d' une clientèle fidèle à son savoir-faire et à son univers virtuel , qui reste le plus ancien parmi ces trois grands .
Mais alors que la PlayStation 2 représentait plus de la moitié des recettes de Sony , sa dernière mouture , la PS3 nécessiterait selon la presse économique au moins 50 millions d' exemplaires vendus pour que Sony puisse obtenir un premier retour sur investissement .
Selon la presse spécialisée et les franchises de vente et reprise de jeux vidéo , Microsoft et Sony vendent chaque console à perte jusqu' à un stade de plusieurs dizaines de millions d' exemplaires vendus .
La première console de jeux vidéo répertoriée est l' Odyssey , apparue sur le marché en 1972 .
Quatre ans plus tard , les cartouches apparaissent sur la Fairchild Channel F .
La Game Boy connaît le succès parmi les consoles portables .
Dans le domaine des consoles de salon , Sony devient le leader du marché avec la PlayStation .
La génération suivante est marquée par l' arrivée sur le marché de Microsoft et la Xbox et par la retraite de Sega suite à l' échec commercial de la Dreamcast .
La PlayStation 2 bat les records de vente de consoles de salon .
La Nintendo DS et la PSP introduisent de nouveaux concepts de jeux parmi les consoles portables .
