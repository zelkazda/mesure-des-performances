Les Gémeaux sont une constellation du zodiaque traversée par le Soleil du 20 juin au 20 juillet .
Dans l' ordre du zodiaque , la constellation se situe entre le Taureau à l' ouest et le Cancer faible à l' est .
Elle est entourée par le Cocher et le Lynx à peine visible au nord et la Licorne et le Petit Chien au sud .
Deux étoiles de la constellation sont nommées d' après les jumeaux de la légende : Castor et Pollux ( β Geminorum ) .
Les Gémeaux étaient l' une des 48 constellations identifiées par Ptolémée .
Le programme d' exploration spatial Gemini , mené par la NASA dans les années 1960 , a été nommé d' après cette constellation .
Castor est une étoile géante blanche .
Avec une magnitude de 1,58 , elle n' est pas la plus brillante de la constellation et sa désignation " α " rend compte de sa position tout au nord des Gémeaux .
L' étoile la plus brillante de la constellation est Pollux ( β Geminorum ) , magnitude 1,16 , la 16 e étoile la plus brillante du ciel .
C' est une géante orange , distante de 34 années-lumière ( assez proche du système solaire , donc ) , environ 10 fois plus large que le Soleil .
