Commune littorale de la baie des Veys à l' extrême ouest du Calvados , Géfosse-Fontenay se trouve à 4,5 kilomètres d' Isigny-sur-Mer et presque à mi-distance entre Bayeux ( 29 km ) et Saint-Lô ( 27 km ) .
La commune se situe dans le pays du Bessin et dans le Parc naturel régional des Marais du Cotentin et du Bessin .
Géfosse-Fontenay a compté jusqu' à 365 habitants en 1866 , mais les deux communes fusionnées en 1861 totalisaient 438 habitants au premier recensement républicain , en 1793 .
