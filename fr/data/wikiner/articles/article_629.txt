Henry Ford ( 30 juillet 1863 à Dearborn , Michigan , États-Unis -- 7 avril 1947 , Dearborn ) est le fondateur de la Ford Motor Company .
Sa conception de l' automobile Modèle T révolutionne le transport et l' industrie américaine .
Ford a une vision globale de son action : il voit dans la consommation la clé de la paix .
L' important engagement d' Henry Ford à réduire les coûts aboutit à de nombreuses innovations techniques et commerciales , notamment un système de franchises qui installe une concession dans toutes les villes en Amérique du Nord et dans les grandes villes , sur les six continents .
La Fondation Ford hérite de la majeure partie de la fortune de Ford , mais l' industriel veille néanmoins à ce que sa famille en conserve le contrôle de façon permanente .
Ils arrivent en Amérique en 1847 et montent une ferme dans le comté de Wayne ( Michigan ) près de Détroit .
Ford fréquente l' école jusqu' à l' âge de 15 ans , date à laquelle il développe une aversion pour la vie agricole et une fascination pour les machines .
Ford écrira en utilisant seulement les phrases les plus simples .
Enfant , Henry Ford s' intéresse beaucoup à la mécanique .
Malgré les besoins de la ferme , ses parents l' autorisent à partir travailler à Détroit dans le Michigan à 17 ans .
Détroit est , à l' époque , l' un des symboles de l' industrie aux États-Unis .
Ford est déprimé lorsque sa mère meurt en 1876 ; son père doit reprendre la ferme familiale .
Ils eurent un fils , Edsel Bryant Ford , né le 6 novembre 1893 .
Ces expériences aboutissent en 1896 avec l' achèvement de son propre véhicule automoteur nommé Ford quadricycle , un véhicule de 4 chevaux à 4 roues refroidi par eau , dont il conduit des essais le 4 juin .
Après divers essais difficiles , Ford réfléchit à un moyen d' améliorer le quadricycle .
Toujours en 1896 , Ford assiste à une réunion des dirigeants Edison .
Edison approuve les expérimentations de Ford sur l' automobile ; Thomas Edison lui dit : " Jeune homme , vous l' avez !
Encouragé par cette approbation , Ford conçoit et construit un second véhicule , dont la fabrication s' achève en 1898 .
Cependant , les automobiles produites sont de moindre qualité et les prix sont supérieurs à ceux espérés par Ford .
En conséquence , Ford quitte la société qui porte son nom en 1902 .
Ford cherche à produire des automobiles à la conception bon marché .
Le 16 juin 1903 , Ford aide à organiser la Ford Motor Company , capitalisée à 150000 $ , dont 28000 de sa bourse personnelle .
Henry Ford possède 25,5 % des parts de la nouvelle organisation en tant que vice-président .
Dans une nouvelle voiture de sa conception , Ford parcourt 1 mille en 39,4 secondes sur la glace du Lac Sainte-Claire , établissant ainsi un nouveau record de vitesse terrestre à 91.3 milles à l' heure ( 147.0 km/h ) .
Ford étonne le monde en 1914 en offrant un salaire journalier de 5 $ , soit plus du double du salaire de la plupart des travailleurs .
Cela s' avère extrêmement rentable : les meilleurs mécaniciens de Détroit affluent chez Ford , augmentant la productivité et abaissant les coûts de formation .
Toujours à la recherche de plus d' efficacité et de réduction des coûts , en 1913 , Ford introduit le déplacement des pièces sur une ligne d' assemblage , ce qui permet une énorme augmentation de la production .
Ford écrit même dans sa biographie " Chaque client peut avoir une voiture de toutes les couleurs tant qu' ils la veulent en noir " .
La conception est vivement encouragée et défendue par Ford , et la production atteint , en 1927 , le total final de 15007034 unités .
Bien que la nation soit en guerre , Ford est un ardent défenseur de la paix et du projet de " Société des Nations " .
Henry Ford s' intéresse à la politique et en tant que puissant dirigeant d' affaire , il est quelquefois impliqué dans la politique .
Après la guerre , Henry Ford n' entre finalement pas au sénat .
Henry Ford cède la présidence de Ford Motor Company à son fils Edsel Ford en décembre 1918 .
Le déclin de la Ford T s' explique par des raisons sociales et commerciales .
La production de la Ford T cesse le 27 mai 1927 .
Le résultat est un succès : la Ford Modèle A , introduite en décembre 1927 sur le marché , connaîtra en 1931 une production totale de plus de quatre millions d' unités .
Henry Ford est un pionnier du welfare capitalism ( le " capitalisme du bien-être " ) , destiné à améliorer le sort de ses travailleurs et en particulier pour réduire le taux de rotation des ouvriers , qui conduit de nombreux départements à devoir engager annuellement 300 personnes pour remplir 100 postes de travail .
Dans les années 1920 , le fordisme et ses corollaires font d' Henry Ford un héros populaire en URSS , et les ventes de véhicules Ford sont décuplées entre 1922 et 1925 .
Le succès d' Henry Ford et de son premier véhicule peut s' expliquer par les quatre principes qu' il a mis en place pour parvenir à ses fins .
La rationalisation des tâches ou organisation scientifique du travail est l' application des méthodes de Taylor au processus de fabrication .
Cette technique a pour origine l' industrie de l' armement , d' où proviennent certains ingénieurs de la Ford Motor Company .
Elle permet en outre l' expansion géographique de la Ford T .
Le " fordisme " , terme par lequel on désigne l' ensemble des conceptions d' Henry Ford sur le développement de son entreprise , parie sur l' augmentation du pouvoir d' achat des ouvriers pour stimuler la demande de biens de consommation .
C' est le 5 janvier 1914 que Ford annonça sa formule du salaire journalier à 5 $ .
Lorsque Ford instaure les 40 heures de travail par semaine et un salaire minimum , il est vivement critiqué par d' autres industriels et Wall Street .
Comme Henry Ford l' explique , c' est " un des meilleurs moyens de réduction des coûts jamais mis en place . "
En effet , Henry Ford agit uniquement dans l' intérêt de son entreprise .
La philosophie du travail de Ford permet d' augmenter rapidement la productivité , mais les salaires demeurent inchangés pendant 30 ans : 6 $ en 1919 et 7 $ en 1927 .
Ford crée une machine de publicité massive à Détroit pour s' assurer que tous les journaux retransmettent les annonces sur les nouveaux produits .
Ford est toujours désireux de vendre aux agriculteurs , qui considèrent l' automobile comme un moyen susceptible d' aider leurs entreprises .
Le réseau de distributeurs Ford introduit ainsi la voiture dans presque toutes les villes en Amérique du Nord .
Quinze millions d' exemplaires de Ford T ont été construits en 19 ans .
" Si vous étudiez l' histoire de la plupart des criminels , vous constaterez qu' ils étaient des fumeurs invétérés " , explique Henry Ford .
L' intrusion excessive de Ford dans la vie privée de ses employés sera longtemps source de controverses .
Il constitue un énorme progrès technologique sur les appareils existants et permet à Ford de devenir le plus grand fabricant mondial d' avions commerciaux .
Les compagnies aériennes abandonnent leurs précédents avions qui ne pouvaient transporter que quelques passagers pour acheter les nouveaux avions Ford , possédant une importante capacité de transport de passagers .
Henry Ford exprime depuis longtemps un intérêt particulier pour la science des matériaux .
Les matières plastiques à base de soja sont utilisées dans les automobiles Ford tout au long des années 1930 dans les pièces en plastique comme les cornes de voiture , dans la peinture , etc .
En 1942 , Ford brevette une automobile composée presque uniquement de plastique issu du soja , raidie par un cadre tubulaire métallique .
Elle pèse 30 % de moins que celles en acier et peut , selon Ford , résister à des efforts dix fois supérieurs à ce que peut supporter l' acier .
Henry Ford et Karl Benz sont parfois crédités de l' invention de l' automobile , mais , comme c' est généralement le cas , le développement et la conception associent de nombreux inventeurs .
Ford le dit lui-même : " ... dans les années 1870 , la notion d' une voiture sans cheval était une idée banale " .
Le leitmotiv de Ford est l' indépendance économique , voire l' autarcie des États-Unis .
Les objectifs de Ford sont de produire un véhicule à partir de zéro sans recours au commerce extérieur .
Il ouvre des usines de montage Ford en Grande-Bretagne et au Canada en 1911 , et devient rapidement le plus grand producteur automobile de ces pays .
En 1912 , Ford coopère avec Agnelli , patron de Fiat , afin de lancer la première chaîne de montage automobile italienne .
Dans les années 1920 , Ford ouvre également des usines en Australie , en Inde , et en France .
Ford expérimente une plantation de caoutchouc dans la jungle amazonienne appelé Fordlândia , mais celle-ci fut un de ses rares échecs .
Fordlândia visait à mettre fin à la dépendance de Ford envers le caoutchouc venant de la Malaisie britannique .
En 1932 , Ford produit le tiers des automobiles construites dans le monde .
Partisans et détracteurs insistent sur le fait que le fordisme américain incarne le développement capitaliste , et que l' industrie automobile est la clé pour comprendre les relations économiques et sociales aux États-Unis .
Ford vienne prêcher son nouvel évangile " .
Ford s' est toujours déclaré catégoriquement opposé à la présence de syndicats dans les entreprises .
La plupart voient la restriction de productivité comme un moyen de favoriser l' emploi , mais Ford ne croit pas en cette tactique , car la productivité est nécessaire pour accroître la prospérité économique .
Ford estime également que les dirigeants syndicaux fomentent de perpétuelles crises socio-économiques de façon à maintenir leur propre pouvoir .
Néanmoins , Ford estime que de bons gestionnaires comme lui sont en mesure de repousser les attaques de personnes malavisées à la fois de gauche et de droite , et qu' ils sont capables de créer un système socio-économique dans lequel ni la mauvaise gestion ni les syndicats ne pourront trouver le soutien leur permettant de se maintenir .
Alors que la dépression frappe , Henry Ford accélère la production à un rythme insupportable .
Henry Ford admet publiquement qu' il règne par la crainte .
La terreur règne dans les usines Ford .
Le maire de Détroit observe qu ' " Henry Ford emploie certains des pires bandits de notre ville " .
Ford s' oppose violemment aux organisations de travailleurs et travaille activement contre United Auto Workers qui essaye de créer des sections syndicales dans ses entreprises , afin de fédérer les travailleurs .
Il avait obtenu l' autorisation de la mairie , mais pas celle d' Henry Ford .
Mais à force de stagnation , les salaires des travailleurs de Ford finissent par devenir très inférieurs à ceux des autres usines concurrentes , alors que les syndicats n' obtiennent toujours aucun droit .
Mais les grévistes sont victorieux : l' UAW contrôle une surface d' usine de 20 kilomètres carrés .
Ford n' a pas d' autre issue que de négocier avec le syndicat .
Lors de la guerre , pour inciter les responsables syndicaux à agir comme recruteurs , Roosevelt n' a accordé de contrats militaires qu' aux sociétés où des syndicats existaient .
Du coup , Ford a bénéficié d' énormes contrats , qui ont remis la société à flot pour les quatre années suivantes .
L' administration du président Franklin Roosevelt envisage une reprise de l' entreprise pour assurer la continuité de la production pendant la guerre , mais le projet ne se concrétise pas .
Les déclarations publiques d' Henry Ford et de son fils Edsel Ford montrent qu' ils sont à l' avant-garde des hommes d' affaires américains qui privilégient la recherche du profit sans que leurs opinions politiques n' aient d' influence sur leur volonté de trouver de nouveaux marchés .
Dans plusieurs autres passages , les Juifs sont présentés comme un " germe " qui doit faire l' objet d' un " nettoyage " .
Adolf Hitler et ses collaborateurs reprendront cette terminologie pour justifier leurs crimes .
Le Juif n' est plus défini par sa religion mais par sa " race " , " une race dont la persistance a vaincu tous les efforts faits en vue de son extermination " .
Ford s' inspire des Protocoles des Sages de Sion , un ouvrage qui serait " trop terriblement vrai pour être une fiction , trop profond dans sa connaissance des rouages secrets de la vie pour être un faux " , cité et commenté abondamment , comme preuve ultime et irréfutable de la conspiration juive pour s' emparer du pouvoir à l' échelle mondiale .
Trois volumes ont pour objet la place des Juifs aux États-Unis .
Les Juifs sont responsables de l' introduction dans les arts de la scène aux États-Unis d' une " sensualité orientale " sale et indécente , " instillant un poison moral insidieux " .
La contribution de Ford à la propagation de l' antisémitisme va au-delà de l' imprimé .
Anti-communiste convaincu , Henry Ford crée , dans les années 1930 , la première usine automobile moderne d' Union des républiques socialistes soviétiques , à Gorki .
Antimarxiste , Ford met en place avec sa Ford Motor Company une idéologie industrielle fondée sur les principes d' ordre et d' autorité .
Dans les années 1950 et 60 , soit après la mort de Ford , les usines Ford fabriquent les camions utilisés par les Nord-Vietnamiens pour acheminer armes et munitions dans le cadre de leur guerre contre les Américains .
Ford s' exprime à propos de cette polémique en clamant que " [ son ] acceptation d' une médaille du peuple allemand ne [ le fait ] pas , comme certains semblent le penser , entraîner aucune sympathie de [ sa ] part avec le nazisme " .
Alors que Ford clame publiquement qu' il n' aime pas les gouvernements militaristes , il tire profit de la Seconde Guerre mondiale , en alimentant l' industrie de guerre des deux camps : il produit , via ses filiales allemandes , des véhicules pour la Wehrmacht , mais aussi pour l' armée américaine .
Un million de dollars est réclamé aux Américains pour les dégâts provoqués dans l' usine de Cologne .
Ford demande aussi des réparations au gouvernement français .
Au cours de cette période , Ford apparait comme " un porte-parole respecté de l' extrémisme de droite " .
Cependant , selon le rapport des témoignages du procès , Ford n' écrit presque rien dans ces articles .
Des amis et des associés d' affaires déclarent qu' ils ont mis Ford en garde sur le contenu du journal mais que Ford ne lit probablement jamais les articles , .
Un procès en diffamation intentée par un avocat de San Francisco et une coopération agricole juive en réponse à des articles antisémites conduisent Ford à fermer le journal en décembre 1927 .
Le Juif international est une figure mythique qu' on critique pour financer la guerre .
Cependant , lors du procès de Nuremberg , Baldur von Schirach , le chef des Jeunesses hitlériennes déclare avoir été influencé par la lecture de Ford .
Après ses excuses en 1927 , Ford ne fait plus de déclarations publiques sur la question juive .
