Issu d' une famille originaire de Neuchâtel et Les Verrières , en Suisse , Breguet ne révèle , de prime abord , aucune disposition particulière .
Lors de sa quinzième année , il est placé en apprentissage d' horlogerie par son beau-père , lui-même horloger , à Les Verrières , dans le Val-de-Travers .
Après avoir longuement étudié dans ce domaine , il fonde en 1775 la maison d' horlogerie Breguet à Paris .
Breguet devient maitre horloger en 1784 .
Breguet se réfugie donc en Suisse .
La maison Breguet est alors à son plus haut niveau grâce à son succès commercial .
Après la mort de Berthoud , Breguet fut choisi pour le remplacer comme horloger de la marine , et le Bureau des longitudes l' admit au nombre de ses membres .
Puis , par ordonnance royale de 1816 , il prit place à l' Académie des sciences , section de mécanique .
Il était également chevalier de la Légion d' honneur .
Lorsque la mort le surprit , Breguet s' occupait d' un grand travail sur l' horlogerie .
Il est inhumé au cimetière du Père-Lachaise .
Marie-Antoinette , Napoléon , Talleyrand , l' impératrice Joséphine , la reine Victoria et Winston Churchill , entre autres , possédèrent des montres Breguet .
Son petit-fils , Louis Breguet , participa avec Antoine Masson à la réalisation d' une bobine d' induction perfectionnée par Heinrich Daniel Ruhmkorff .
En 1795 , il invente un spiral , dit " spiral Breguet " dont la courbe terminale se trouve sur un plan différent afin que ce spiral se développe concentriquement .
Parmi les clients célèbres de Breguet , nous pouvons encore citer le roi Louis XVI .
Breguet trouve une solution à ce problème lors d' un séjour en Suisse .
