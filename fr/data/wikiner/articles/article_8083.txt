Le grand nomenclateur que fut Linné , celui qui consacra sa vie à nommer la plupart des êtres vivants et à les ordonner selon leur rang , eut lui-même maille à partir avec sa propre identité , son nom et même son prénom ayant été remaniés tant de fois au cours de sa vie qu' on ne dénombre pas moins de neuf binômes et autant de synonymes !
Mais Nils , pour répondre aux exigences administratives lors de son inscription à l' université de Lund , doit choisir un patronyme .
Et c' est sous ce nom de Carolus Linnæus , qu' il publie ses premiers travaux en latin .
Dans le monde francophone comme en Suède , il est aujourd'hui communément connu sous le nom de Linné .
En zoologie , où il est d' usage de citer au long le nom de l' auteur des taxons , on emploie " Linnæus " ( ou sa graphie sans ligature latine " Linnaeus " , adoptée en anglais et plus pratique pour les utilisateurs de claviers dits internationaux ) à la suite des taxons qu' il a décrits , et non " Linné " , car c' est sous son nom universitaire que ses principaux travaux de taxinomie ont été publiés .
De plus , à la différence de son prénom , " Linnæus " n' est pas une transcription latine a posteriori , mais son véritable patronyme .
En 1762 , sur la page de couverture de la seconde édition de Species plantarum , le nom est encore imprimé de cette manière .
Nils est un amoureux des plantes qui transmet sa passion à son jeune fils , permettant à celui-ci d' entretenir son propre jardin dès l' âge de 5 ans .
Inscrit sous le nom de " Carolus Linnæus " , il commence ses études à l' université de Lund en 1727 .
Il y reçoit notamment l' enseignement de Kilian Stobæus ( 1690-1742 ) , le futur professeur et recteur de l' université , alors encore seulement docteur en médecine , qui lui offre son amitié et ses encouragements et lui ouvre ses collections et sa bibliothèque .
Fort peu développées à cette époque , les études de médecine n' étaient suivies que par une dizaine d' étudiants sur les cinq cents environ que comptait l' université et il n' était pas prévu que l' on puisse soutenir sa thèse de doctorat en Suède .
Ces études furent sans doute le moyen , voire le prétexte , pour Carolus Linnæus de s' adonner à sa passion pour la botanique .
Arrivé à Uppsala sans un sou vaillant , il lui faut aussi subvenir à sa propre existence .
Alors qu' à peine arrivé en ville , il visite le jardin botanique fondé par Olof Rudbeck ( 1630-1702 ) , il est remarqué et pris en charge par Olof Celsius ( 1670-1756 ) , le doyen de la cathédrale et oncle du savant Anders Celsius ( 1701-1744 ) .
Olof Celsius présente Linné à Olof Rudbeck le Jeune ( 1660-1740 ) , lui-même médecin naturaliste , qui engage le jeune étudiant comme tuteur de ses fils et lui permet d' accéder à sa bibliothèque .
Linné remplace un temps l' assistant de Rudbeck , Nils Rozén ( 1706-1773 ) , alors en voyage à l' étranger .
Linné a justement comme professeur Olof Rudbeck le Jeune , ainsi que Lars Roberg ( 1664-1742 ) .
C' est aussi à Uppsala , que Linné se lie d' amitié avec Peter Artedi ( 1705-1735 ) , son aîné de deux ans , qui également issu d' un milieu d' église , destiné à devenir pasteur et venu étudier la théologie , s' intéresse finalement plus à l' histoire naturelle , particulièrement aux poissons .
Il conduit des missions scientifiques en Laponie et en Dalécarlie , à l' époque régions inconnues .
Bien qu' il donne des conférences de botanique et qu' il soit considéré à Uppsala comme un génie , il n' a pas encore de diplôme de médecine .
Il retourne alors en Suède , où , ne recevant pas de proposition qui le satisfasse , il exerce la médecine à Stockholm en se spécialisant dans le traitement de la syphilis .
Il se marie le 26 juin 1739 avec Sara Elisabeth Moræa ( 1716-1806 ) , originaire de Falun .
Finalement , en 1741 , il obtient la chaire de médecine à l' université d' Uppsala puis celle de botanique , fonction qu' il occupera jusqu' à sa mort .
Il effectue trois expéditions en Suède et inspire une génération d' étudiants .
À la fin de sa vie il est si célèbre que Catherine II de Russie lui envoie des graines de son pays .
Scopoli lui a transmis toutes ses recherches et ses observations pendant des années , sans qu' ils pussent se rencontrer à cause de la distance .
Il meurt le 10 janvier 1778 à Uppsala au cours d' une cérémonie dans la cathédrale , où il est par ailleurs enterré .
C' est avec la dixième édition , de 1758 , que Linné généralise le système de nomenclature binominale .
Pour cela , il utilise les animaux décrits ailleurs ( comme dans les œuvres de Seba , Aldrovandi , Catesby , Jonston ou d' autres auteurs ) .
C' est en 1753 que Linné fait publier Species plantarum ( les espèces des plantes ) où il décrit environ 8 000 végétaux différents pour lesquels il met en application de manière systématique la nomenclature binomiale dont il est le promoteur .
De retour à Uppsala , il tente une expérience , introduit une petite dose de plâtre fin dans des moules perlières et replace celles-ci dans la rivière de la ville , la Fyris .
Ce n' est qu' en 1900 que l' invention de Linné est redécouverte lors de la lecture de ses manuscrits conservés à Londres .
Linné met au point son système de nomenclature binominale , qui permet de désigner avec précision toutes les espèces animales et végétales ( et , plus tard , les minéraux ) grâce à une combinaison de deux noms latins ( le binôme ) , qui comprend :
Linné est un naturaliste " fixiste " .
Mais , par la suite , au fur et à mesure de l' avancée des connaissances , notamment à partir des travaux de Lamarck et de Darwin , la systématique a pris diverses formes ( phénétique , évolutioniste , phylogénétique … ) , pour aboutir de nos jours à une systématique pragmatique ( " au quotidien " ) qui essaie de prendre en compte les diverses données propres à chaque méthode .
Linné a appliqué le concept de " race " à l' homme ( ainsi qu' aux créatures mythologiques ) .
Chaque " race " possédait certaines caractéristiques que Linné considérait comme endémiques pour les individus qui la représentaient .
Linné a eu une immense influence sur les naturalistes de son époque .
Nombreux sont ceux qui s' embarquent pour des contrées lointaines pour y reconnaître la flore , Linné lui-même les nomme ses apôtres .
C' est avec sa collaboration que Philibert Commerson put écrire son traité d' ichtyologie .
Il eut aussi quelques autres correspondants tels que Frédéric-Louis Allamand .
Parmi ses nombreux élèves , citons : Anders Dahl , Daniel Solander , Johan Christian Fabricius , Martin Vahl ou Charles de Géer .
Son caractère égocentrique , conjugué à une extrême ambition , le conduit , comme Buffon , à persécuter ceux qui n' optent pas pour son système .
Mais il est le premier , suivant en cela John Ray , à utiliser un concept clair d' espèce qui n' est en rien diminué par sa conviction de l' immuabilité des espèces .
Contrairement à la plupart des naturalistes européens qui reconnaissent la révolution linnéenne , des naturalistes et des philosophes français comme Julien Offray de La Mettrie , Denis Diderot , Buffon ou Maupertuis critiquent la systématique linnéenne .
L' entreprise de Linné ne fait que partiellement appel à la raison , et peu d' incitation à l' expérimentation .
Finalement , des idées de Linné , seule la nomenclature binominale survivra ...
