Après leur échec chinois , les Huns ont porté leurs conquêtes vers l' ouest , passant par la Bavière et l' Autriche avant d' atteindre l' Alsace en 451 .
La Haut-Rhin ne produit presque plus de chou destiné à la choucroute ; le village de Wickerschwihr en fut le dernier bastion .
En revanche , la production s' est développée dans l' Aube , vers Brienne-le-Château , dans la Sarthe et le Nord-Pas-de-Calais .
