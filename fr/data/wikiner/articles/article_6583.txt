Une Jeune Fille assoupie est un tableau de Johannes Vermeer peint vers 1657 , exposé au Metropolitan Museum de New York ( huile sur toile , 87,6 x 76,5 cm ) .
Le tableau derrière , faiblement éclairé , représente Cupidon avec un masque tragique à ses pieds .
Entre la rêverie amoureuse et l' alcoolisme , impossible de trancher : Johannes Vermeer joue avant tout avec l' ombre et la lumière dans un espace clos pour créer une atmosphère dans laquelle chacun trouve ce qu' il veut .
