Ce sont des objets qui évoluent assez rapidement , on en connaît environ 1,500 dans notre Galaxie .
Un des exemples célèbres de ce type d' objet est la nébuleuse de l' Anneau située dans la constellation de la Lyre d' où son autre appellation : nébuleuse de la Lyre .
Le terme de " planétaire " semble avoir été proposé la première fois par William Herschel , vers 1784 ou 1785 , en raison de la ressemblance visuelle entre ces objets qu' il classait et la planète qu' il venait de découvrir , à savoir Uranus .
William Huggins fut l' un des premiers astronomes à étudier le spectre des objets astronomiques en dispersant leur lumière à l' aide d' un prisme .
D' autre part , lorsqu' il étudia la nébuleuse de l' Œil de Chat , il rencontra un spectre totalement différent : un faible nombre de raies en émission se dégageaient sur un continu quasiment nul .
Parmi les quelques 200 à 400 milliards d' étoiles que compte notre Galaxie , il n' a été détecté qu' environ 1500 nébuleuses planétaires , .
Les nébuleuses planétaires ont d' abord été observées comme des anneaux diffus ( rappelant les planètes , d' où leur nom , dû à William Herschel ) , puis furent considérées comme des coquilles projetées sur le plan du ciel quand leur nature fut explicitée .
