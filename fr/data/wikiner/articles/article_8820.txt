Lara Croft est un personnage de fiction et la protagoniste de la série de jeux vidéos Tomb Raider , produite par Square Enix ( précédemment Eidos Interactive ) .
Créé par Toby Gard durant son emploi dans la branche anglaise de Core Design , le personnages fait sa première apparition dans le jeu Tomb Raider , en 1996 .
Elle fit alors également des apparitions dans les suites du jeu vidéo , dans des adaptations imprimées , dans des séries de courts dessins animés , dans deux longs métrages ( où elle est représentée par Angelina Jolie ) , ainsi que des variantes relatives à la série .
Lara Croft a aussi été utilisée dans des promotions de tierces parties , incluant la télévision et des publicités , ainsi qu' en tant que porte-parole .
Core Design a premièrement mené le développement du personnage et des séries .
De la réception très mitigée pour l' opus Tomb Raider : l' Ange des ténèbres de 2003 résulta un changement de développeur ; la licence fut remise à l' américain Crystal Dynamics .
Crystal Dynamics se concentre sur le fait de rendre le personnage plus crédible , et altéra sa capacité à interagir avec l' environnement du jeu avec ses proportions .
Les critiques considèrent Lara Croft comme un personnage significatif de jeux vidéos dans la culture populaire .
Lara Croft est aussi considérée comme un sex symbol , l' un des premiers dans l' industrie .
Lara Croft est présentée comme une femme athlétique ; elle a les yeux marrons , tout comme ses cheveux , qui sont fréquemment coiffés en une tresse ou en une queue de cheval .
Lara Croft porte un haut court dans les jeux suivants , et des variations sur ce thème , comme un pantalon de camouflage et une brassière noire .
Lara Croft écrit des guides et d' autres travaux publiés basés sur ses exploits de mercenaire .
Alors qu' elle était en train de chercher un abri pour se protéger du froid , Lara Croft est témoin de la disparition de sa mère après qu' elle a touché une épée ancienne .
Lara Croft apparaît en tout premier lieu dans la série de jeux vidéos Tomb Raider produite par SCi Entertainment ( précédemment Eidos Interactive ) .
Tomb Raider III a été publié en 1998 , et se concentre sur des fragments de météorite qui donneraient des pouvoirs surnaturels aux humains .
Dans Tomb Raider : Sur les traces de Lara Croft , publié en 2000 , la quasi-totalité du jeu se concentre sur des aventures narrées via flashbacks .
Eidos remet à zéro la série en 2006 avec Tomb Raider : Legend , qui se concentre sur la quête de Lara Croft pour Excalibur et sa mère .
L' histoire se centre sur la recherche de Lara Croft : des informations sur la disparition de sa mère .
Un nouveau jeu , Lara Croft and the Guardian of Light , est annoncé en 2010 .
Lara Croft a été incarnée dans deux long-métrages par l' actrice Angelina Jolie .
Le premier film , Lara Croft : Tomb Raider , paraît en 2001 .
Paramount Pictures a acquis les droits du film Tomb Raider en 1998 .
Paramount a également reçu des idées de Core Design sur le casting .
Jolie a finalement été choisie pour incarner Lara Croft .
Elle a remarqué que les proportions de Lara Croft dans les jeux vidéos était irréalistes , et a voulu éviter de montrer de telles proportions à de jeunes filles .
Jolie s' est entrainée rigoureusement pour les scènes d' action nécessitées pour le rôle , ayant quelque fois des blessures .
Jolie a également rencontré des difficultés avec l' utilisation des pistolets , le saut à l' élastique et manœuvrer avec sa tresse .
Angelina Jolie a repris son rôle pour une suite , Lara Croft : Tomb Raider , le berceau de la vie , dans lequel Lara Croft recherche la boîte de Pandore , en compétition avec un syndicat du crime chinois .
Malgré la mauvaise réception du second film , Paramount a annoncé vouloir sortir un troisième film .
En 2007 , Jolie a été sélectionnée pour jouer le personnage dans une autre suite .
À partir de 1997 , le personnage apparaît régulièrement dans les comics de Top Cow Productions .
Lara Croft apparaît dans un cross-over dans Witchblade de Sara Pezzini , et plus tard dans sa propre série de comic , en 1999 .
La websérie est une collection de dix courts dessins animés qui présente la version re-imaginée de Lara Croft par des animateurs spécialistes , des artistes de comic , et des écrivains , comme Jim Lee , Warren Ellis et Peter Chung .
L' actrice Minnie Driver a doublé Lara Croft dans la version originale .
Elle a fait l' objet de deux adaptations cinématographiques : Tomb Raider et Le Berceau de la Vie , où elle fut incarnée par Angelina Jolie .
Angelina Jolie aurait l' intention de produire le prochain film Tomb Raider sans y incarner l' héroine .
Plusieurs actrices sont envisagées , telles que la Britannique Rhona Mitra ( la toute première incarnation officielle de Lara en 1997 pour la promotion du jeu vidéo ) , Megan Fox , Charisma Carpenter , Eva Longoria , et Mena Suvari et même Kristen Stewart .
Plusieurs top-models ont incarné Lara Croft pour assurer la promotion des différents jeux de la série .
