Le Luxembourg est membre du Benelux , de l' Union européenne -- dont il est l' un des six pays fondateurs -- et de l' OTAN .
Les Celtes , les Romains puis les Francs peuplent successivement la région .
En 1815 , Guillaume I er l' intègre à son royaume des Pays-Bas puis en reconnaît l' indépendance en 1839 .
Pendant la Première Guerre mondiale , le Luxembourg est occupé par l' Allemagne jusqu' en 1918 .
Les négociations du Traité de Versailles en 1919 confirment l' indépendance du pays tandis qu' un référendum populaire consolide l' indépendance du pays et la monarchie .
Le pays est libéré en septembre 1944 par les troupes américaines mais subit la contre-attaque allemande lors de la bataille des Ardennes en décembre de la même année .
Dès 1944 , l' union du Benelux est conclue et le pays s' inscrit dans le processus de la construction européenne .
En 1948 , le Luxembourg est membre fondateur du Traité de Bruxelles et de l' OTAN .
En 1952 Luxembourg-ville devient le siège de la Communauté européenne du charbon et de l' acier ( CECA ) .
L' adhésion à la Communauté économique européenne est le point de départ d' une expansion économique et d' une hausse de l' immigration .
Le Luxembourg est une démocratie représentative , sous la forme d' une monarchie constitutionnelle .
À la tête du gouvernement , nous retrouvons un premier ministre ( Jean-Claude Juncker en 2009 ) ainsi qu' une chambre des députés qui débat et vote les lois .
Le Luxembourg , membre de l' Organisation du traité de l' Atlantique nord ( OTAN ) , a supprimé en 1967 le service militaire obligatoire et entretient une petite armée de 800 volontaires .
Le Luxembourg n' a aucune marine , ni force aérienne .
Le Luxembourg est divisé en trois subdivisions administratives , ou districts :
Situé au cœur de l' Europe occidentale entre la Belgique , la France et l' Allemagne , le grand-duché de Luxembourg a une superficie de 2586 km² .
Le Luxembourg peut être divisé en deux régions géologiques :
Outre la banque privée , depuis une quinzaine d' années , le Luxembourg est devenu la place la plus importante d' Europe sur le marché des fonds d' investissement .
Le PIB par habitant du Luxembourg est le plus élevé au monde [ réf. nécessaire ] .
Le Luxembourg compte plus de 502 066 habitants avec une croissance de près de 100 000 personnes en 30 ans , croissance démographique plutôt exceptionnelle en comparaison avec les pays proches .
En effet , le solde migratoire , en moyenne annuelle , au Luxembourg était de plus de 10‰ au cours de la décennie 1990-2000 , alors qu' il était d' environ 2,3‰ dans l' Europe des 15 .
Le Luxembourg a adopté le modèle fiscal allemand et a été cadastré pour la première fois complètement par l' armée allemande , pendant la première guerre mondiale .
RTL Télé Lëtzebuerg est la principale chaîne de télévision .
Trois champions luxembourgeois ont remporté la grande boucle : François Faber en 1909 et Nicolas Frantz en 1927 et 1928 , et enfin Charly Gaul en 1958 .
En tennis féminin , Anne Kremer et Claudine Schaul se sont illustrées dans ce sport en remportant plusieurs tournois WTA et ITF , notamment Anne Kremer qui est la première joueuse de tennis luxembourgeoise à atteindre le top 20 ( 18° le 29 juillet 2002 ) .
Le Luxembourg a pour codes :
