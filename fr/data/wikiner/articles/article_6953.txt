Il rencontre Jean-Christophe Menu dans un colloque de Cerisy en 1987 , ce qui lui permet de comprendre qu' il est possible de faire de la bande dessinée sans connaître le dessin académique .
Lewis Trondheim se distingue aussi par le fait qu' il refuse d' accorder des interviews aux journalistes , arguant que tout est dit dans ses œuvres .
Il se consacre depuis à cette dernière et à Donjon .
Fin 2006 , il annonce son départ de L' Association en raison de " divergences éditoriales " avec Jean-Christophe Menu , .
Elle est constituée d' au moins deux saisons , dont 22 épisodes de la première sont sortis , ils sont notamment diffusés sur la chaîne de télévision française France 3 dans une émission pour enfants .
Lewis Trondheim est scénariste et dessinateur , sauf indication contraire .
