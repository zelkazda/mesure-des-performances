Le canton de Schaffhouse est un canton de Suisse .
Les cantons limitrophes sont Zurich et de Thurgovie .
Le canton de Schaffhouse compte 74527 habitants en 2007 , soit 1,0 % de la population totale de la Suisse ; parmi eux , 16 323 ( 21,9 % ) sont étrangers .
