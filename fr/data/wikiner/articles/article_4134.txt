La bataille de Stalingrad , ville du sud de la Russie sur la Volga ( appelée aujourd'hui Volgograd ) , a été marquée par la brutalité et le manque de prise en compte des pertes civiles .
Contrairement au " classique " siège , elle a principalement consisté en combats urbains menés par les Allemands et leurs alliés .
Le 22 juin 1941 , l' Allemagne et ses alliés de l' Axe envahissent l' Union soviétique , avançant rapidement et profondément dans le territoire ennemi .
Après avoir beaucoup souffert pendant l' été et l' automne 1941 , les forces soviétiques contre-attaquent lors de la bataille de Moscou en décembre 1941 .
Des plans pour lancer une autre offensive contre Moscou sont rejetés , car les troupes ont été fortement affaiblies .
La philosophie militaire allemande voulant que dans l' espoir de gains rapides l' attaque se fasse là où cela est le moins prévisible , une attaque sur Moscou aurait été perçue comme trop évidente par certains , et notamment Adolf Hitler .
Le Haut Commandement allemand sait alors que le temps joue contre eux , les États-Unis venant d' entrer en guerre après l' attaque sur Pearl Harbor par les Japonais .
Hitler veut donc finir le combat sur le front de l' est avant que les États-Unis puissent s' impliquer plus avant dans la guerre en Europe .
Une percée au sud sécuriserait le Caucase riche en pétrole , aussi bien que le fleuve Volga , une voie très importante de transport soviétique en Asie centrale .
De plus , l' espion russe au Japon , Richard Sorge , a informé Moscou du fait que le Japon attaquerait l' URSS dès que l' armée allemande aurait pris une quelconque ville sur la Volga , coupant l' approvisionnement en pétrole et carburant en provenance de Bakou , et les munitions et ressources en nourriture envoyées par les alliés depuis le golfe Persique à travers l' Iran , l' Azerbaïdjan soviétique , et le long de la Volga .
Le nom de la ville faisant référence au dirigeant soviétique , elle revêt un intérêt symbolique tout particulier pour les deux camps : sa prise serait , pour la propagande nazie , une victoire que Staline ne peut se permettre d' accepter .
Trois semaines ont ainsi été perdues sans gain notable sur le front du Caucase mais avec des effets négatifs dans la grande boucle du Don .
En novembre 1942 , la Wehrmacht avait déployé , sous le commandement de Friedrich Paulus , la 6 e armée , composée de :
Staline interdit l' évacuation des civils de la ville , pensant que leur présence encouragerait une plus grande résistance des défenseurs .
Un bombardement aérien allemand massif , le 23 août , cause une véritable tempête de feu , tuant 40000 civils et transformant Stalingrad en un vaste paysage de gravats et de ruines en feu .
Vers la fin d' août , les troupes allemandes atteignent la Volga au nord de Stalingrad .
Les combattants soviétiques sont donc encerclés dans la ville , adossés à la Volga , malgré divers moyens mis en œuvre pour circuler sur le fleuve .
L' Ordre n°227 de Staline , connu sous le slogan " Pas un pas en arrière !
Des renforts soviétiques sont embarqués à travers le fleuve Volga de la rive orientale sous le bombardement constant de l' artillerie et des Stukas .
Le contact avec l' arrière est fréquemment coupé , en particulier avec l' état-major , installé de l' autre côté de la Volga .
Sur le kourgane Mamaïev , une colline de 102 mètres de hauteur , les combats sont particulièrement impitoyables .
L' enjeu est crucial pour la Wehrmacht qui veut installer de l' artillerie dans le but de détruire tous les bateaux naviguant sur la Volga .
L' artillerie soviétique sur la berge orientale de la Volga continue à bombarder les positions allemandes .
Pour Staline et Hitler , la bataille de Stalingrad est devenue une question de vie et de mort .
Le commandement soviétique déplace les troupes de réserve stratégiques de l' Armée rouge à Moscou vers la Volga et transfère toute l' aviation disponible du pays entier à Stalingrad .
Les pressions sur les deux commandants militaires sont immenses : Paulus développe un tic incontrôlable à son œil et Tchouïkov éprouve une manifestation d' eczéma qui exige de lui bander complètement les mains .
À l' automne , le général soviétique Gueorgui Konstantinovitch Joukov responsable de la planification stratégique dans la région de Stalingrad , concentre les forces soviétiques dans les steppes au nord et au sud de la ville .
Cette opération , dont le nom de code est Uranus , est lancée le 19 novembre 1942 , en même temps que l' opération Mars qui , elle , est dirigée vers le centre .
Les unités soviétiques attaquent sous le commandement du général Nikolaï Vatoutine .
Le 22 novembre , les deux pinces de la tenaille se rejoignent à Kalatch , parachevant l' encerclement de Stalingrad .
Ceci , cumulé à la pression exercée par l' Armée rouge , rend la situation intenable .
Les divisions blindées , commandées par Von Manstein , que le commandement de la Wehrmacht a envoyées pour briser l' encerclement de Paulus sont arrêtées et repoussées par l' Armée rouge , d' autant que Paulus refuse de désobéir aux ordres d' Hitler et de tenter une sortie .
Hitler octroie cependant à Paulus le titre de maréchal , afin d' inciter ses hommes à le défendre jusqu' au-delà de leur courage , car aucun récipiendaire de cette haute distinction n' a jamais été capturé .
Hitler justifie ce sacrifice en expliquant que ces troupes permettent de fixer sept armées russes , ce qui lui laisse le champ libre pour attaquer un autre secteur que celui de Stalingrad .
Paulus donne personnellement à ses troupes l' ordre de se rendre .
Cette lueur d' espoir au plus fort de la guerre a profondément marqué les populations d' Europe et contribué au prestige soviétique au lendemain de la guerre .
Ce climat de crainte permanente contribue à saper le moral des combattants de l' Axe .
