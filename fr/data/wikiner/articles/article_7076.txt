Marquise fait partie de la communauté de communes de la terre des deux caps qui comprend notamment les villages côtiers d' Ambleteuse , Audresselles , Audinghen et Tardinghen .
Marquise est située sur l' axe autoroutier Boulogne-sur-Mer -- Calais ( autoroute A16 ) qui la traverse du nord ( vers Calais ) au sud ( vers Boulogne-sur-Mer ) .
Lille se trouve sur la ligne à grande vitesse qui relie notamment Paris à Bruxelles .
La commune est à deux pas du site du Tunnel sous la Manche qui permet de se rendre en Grande-Bretagne en 35 minutes grâce à la navette shuttle .
Calais : 20 km .
Boulogne-sur-Mer : 15 km .
Dunkerque : 70 km .
Lille : 130 km .
Bruxelles : 220 km .
Paris : 260 km .
Rouen : 200 km .
Reims : 280 km .
