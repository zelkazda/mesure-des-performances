Une Game Boy est une console de jeux vidéo portable créée par Nintendo en 1989 .
C' est Gunpei Yokoi , le célèbre employé de Nintendo , qui créa cette console à la puissance comparable à celle de la NES ( Famicom au Japon ) mais avec un écran monochrome , ceci afin de pouvoir vendre la console à bas prix .
Malgré une qualité graphique très moyenne , le Game Boy a su s' imposer grâce à de nombreux atouts :
Depuis sa sortie en 1989 , le Game Boy a connu plusieurs successeurs , dont la Game Boy Color en 1998 et la Game Boy Advance en 2001 .
Des consoles de récupération ont notamment été recyclées pour fabriquer des électrocardiographes à destination de Madagascar dans la lutte contre le paludisme : la cartouche de jeu avec cette nouvelle fonctionnalité engendre un coût de fabrication de seulement 30 euros .
Le Game Boy est la seconde console portable la plus vendue au monde avec plus de 118 millions d' exemplaires vendus juste derrière la Nintendo DS ( toutes version confondues ) puisque celle-ci en a 119 millions
En 1995 , alors que Nintendo et Gunpei Yokoi essuient l' échec du Virtual Boy , on apprend qu' une version relookée du Game Boy est en préparation .
En effet , la firme vient non seulement de connaître l' un de ses plus gros échecs mais voit également chuter ses ventes de Game Boy .
Mais Nintendo , croyant beaucoup en son Virtual Boy , n' avait pas préparé la succession de sa petite console .
Cette console diffère très peu de la Game Boy si ce n' est sa taille qui a été réduite de 30 % , l' écran a été très légèrement agrandi en perdant son aspect verdâtre et son autonomie est passée à 10 heures pour 2 piles ( contre 4 pour la GB originale ) .
Cette version a été rapidement éclipsée par la Game Boy Color .
Non pas que ce fut un échec mais Nintendo lui-même voulait contrecarrer les plans d' une concurrence de plus en plus agressive .
Un point intéressant de cette console réside dans sa capacité à lire les anciennes cartouches de jeux Game Boy en sélectionnant les couleurs à utiliser .
En juin 2001 , Nintendo sort une mise à jour de sa console portable .
C' est techniquement une console équivalente à la Super Nintendo , reprenant la même architecture permettant notamment de porter des jeux classiques comme Super Mario Bros. 2 en incluant des améliorations , aux côtés de nouveaux titres comme Mario Kart : Super Circuit , F-Zero : Maximum Velocity et Kuru Kuru Kururin .
La version SP de la Game Boy Advance , lancée en mars 2003 , propose un encombrement réduit avec notamment la possibilité de la replier en deux , un rétro éclairage et une batterie rechargeable , tout le reste demeurant identique .
À la mi- septembre 2005 , Nintendo sort une nouvelle version de la Game Boy Advance SP fournissant un nouvel écran rétro éclairé amélioré .
C' est la dernière Game Boy à permettre la rétro compatibilité avec les cartouches de jeux Game Boy et Game Boy Color .
Le Game Boy Micro est une nouvelle version de la Game Boy Advance .
Elle est sortie en septembre 2005 en Amérique du Nord et au Japon , et le 4 novembre 2005 en Europe .
À la différence de la Game Boy Advance , cette console portable n' est pas compatible avec les jeux Game Boy et Game Boy Color .
La Game Boy Micro a un poids de 80 grammes et une taille de 10 cm de long sur 5 cm de large et moins de 2 cm d' épaisseur .
La console joue aussi sur l' aspect retrogaming avec sa façade Famicom très appréciée au Japon [ réf. souhaitée ] .
Cette console est annoncée par Nintendo comme la dernière Game Boy .
Bien que souvent considérée comme une Game Boy , elle ne fais pas partie de la série et est une suite et constitue un nouveau pilier des consoles de jeux Nintendo avec les Game Boy et les consoles de salon .
Elle accepte néanmoins les cartouches de jeux provenant de la Game Boy Advance dans un deuxième emplacement cartouche prévu à cet effet .
Les jeux Game Boy ou Game Boy Color ne sont pas compatibles .
Son chargeur est le même que celui de la Game Boy Advance SP .
Cette console a une forme plus carrée , n' inclut pas de dragonne , contient un stylet différent et un chargeur non compatible avec la DS de base .
Plus petite la DS lite peut tenir plus facilement dans une poche et est très design .
Notons que cet accessoire est utilisé dans certains jeux comme Pokémon .
Étant donné le manque de lisibilité de l' écran , Nintendo a sorti cette loupe couplée à 2 petites ampoules qui se clipse au-dessus de l' écran .
Cet accessoire se branchant sur le port link de la console utilisait les piles de la Game Boy pour s' alimenter en énergie et permettait d' écouter la radio grâce à des oreillettes fournies avec .
Cartouche vendue seulement au Japon .
Les jeux Game Boy s' inséraient dans le Super Game Boy qui s' insérait dans la Super Nintendo .
100 000 000 [ réf. nécessaire ] : C' est le nombre de Game Boy vendues en 2000 ( sont exclus les GBA , GBA SP et GBM )
400 000 000 [ réf. nécessaire ] : C' est le nombre de jeux Game Boy vendus en 2000 ( sont également exclus GBA , etc . )
Officiellement , on peut lire dans le mode d' emploi que le terme de Game Boy devrait être masculin .
