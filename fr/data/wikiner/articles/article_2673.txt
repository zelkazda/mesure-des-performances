La date de naissance de la France est , en revanche , sujette à controverse .
La date admise la plus reculée étant l' avènement de Clovis , en 481 .
Par ailleurs , on considère parfois le traité de Verdun de 843 comme " l' acte de naissance de la France " .
La plupart des Mérovingiens ne régnèrent que sur une partie du royaume des Francs , avec un ou plusieurs monarques concurrents .
