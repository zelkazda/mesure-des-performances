Né dans un milieu aisé , il suit son père , ingénieur des travaux publics , au Mexique , à Cuba et aux Bahamas .
Après le divorce de ses parents , à l' âge de 15 ans , il rentre à New York pour vivre auprès de sa mère , pianiste , et termine ses études en 1925 à l' université de Columbia .
Il connaît le succès à partir de 1940 , avec La mariée était en noir .
De nombreux metteurs en scène ont porté les œuvres à l' écran de ce maître du suspense , notamment Alfred Hitchcock pour Fenêtre sur cour , d' après une nouvelle , et François Truffaut pour La sirène du Mississippi et La mariée était en noir .
William Irish a également écrit des récits fantastiques .
