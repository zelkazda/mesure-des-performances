CSS ( Cascading Style Sheets : feuilles de style en cascade ) est un langage informatique qui sert à décrire la présentation des documents HTML et XML .
Les standards définissant CSS sont publiés par le World Wide Web Consortium ( W3C ) .
L' un des objectifs majeurs de CSS est de permettre la stylisation hors des documents .
Il est par exemple possible de ne décrire que la structure d' un document en HTML , et de décrire toute la présentation dans une feuille de style CSS séparée .
CSS permet de définir le rendu d' un document en fonction du média de restitution et de ses capacités ( type de moniteur ou de dispositif vocal ) , de celles du navigateur ( texte ou graphique ) , ainsi que des préférences de son utilisateur .
Les caractéristiques applicables aux boîtes CSS sont exprimées sous forme de couples propriété : valeur .
Les valeurs peuvent être selon les cas exprimées à l' aide d' unités normalisées par ailleurs , ou de mots-clés propres à CSS .
Les propriétés CSS ont été établies selon un compromis entre deux contraintes opposées : faciliter la lecture des feuilles de styles par les agents utilisateurs en multipliant les propriétés individuelles , ou faciliter leur écriture par les auteurs en recourant à un nombre plus réduit de propriétés combinées .
Bien que ce code CSS ne décrive pas la totalité de la présentation d' un document , il constitue à lui seul une feuille de style à part entière .
Pour permettre la cascade des styles , des règles de calcul de spécificité permettent au moteur de rendu CSS de déterminer le degré de priorité à appliquer à différents sélecteurs visant concurremment un même élément , et de trier ainsi les règles à lui appliquer .
Les premières implémentations HTML ne comportant pas non plus d' éléments de présentation , une pression croissante s' exerce alors pour que les navigateurs permettent aux auteurs de déterminer eux-mêmes la mise en forme des pages web , dans une démarche issue de la publication imprimée électronique .
L' apparition de CSS répond à une volonté de " proposer une alternative à l' évolution du HTML d' un langage de structuration vers un langage de présentation " .
Les origines des CSS sont donc liées à trois alternatives majeures :
Un autre aspect de CSS s' avère alors déterminant face aux alternatives existantes : CSS est le premier format à inclure l' idée de " cascade " ( feuille de style en cascade ) , c' est-à-dire la possibilité pour le style d' un document d' être hérité à partir de plus d' une " feuille de style " .
Le World Wide Web Consortium ( W3C ) devient opérationnel en 1995 , et la liste de discussion www-style est alors créée .
Contrairement aux logiciels , les spécifications CSS ne sont pas développées par versions successives , qui permettraient à un navigateur de se référer à une version en particulier .
Ceci explique en partie la lenteur de l' avancement normatif de CSS .
Le choix éventuel d' exprimer CSS dans une syntaxe XML ou SGML , régulièrement évoquée pour éviter l' implémentation d' un nouveau mode d' analyse syntaxique , est donc définitivement écartée .
CSS garantit ainsi sa compatibilité ascendante .
Il n' est donc pas encore question de " mise en page " : la propriété float , qui sera par la suite massivement utilisée pour la mise en page CSS globale des designs web , n' est alors conçue que comme un moyen de placer localement , côte à côte , une portion réduite du contenu , tel qu' une image , et le reste du texte .
En 1996 , Internet Explorer 3.0 est le premier navigateur commercial à implémenter partiellement CSS alors que celle-ci est encore en cours de formulation .
Cette implémentation anticipée , menée par Chris Wilson ne correspond pas à la spécification finale .
La présence de ces suites de test se révèle un atout majeur à la fois pour les implémentations dans les navigateurs et pour l' appropriation des techniques CSS par les auteurs ( en fournissant des exemples détaillés des propriétés et de leurs valeurs ) .
Pour assurer la compatibilité avec les précédentes pratiques de codage propre à chaque implémentation ( la " soupe de balises " ) , et permettre un affichage correct des documents web qu' ils soient respectueux ou non des standards CSS et HTML , il est également le premier à mettre en œuvre la technique du doctype switching .
Durant cette période de la fin des années 1990 , la conception web est avant tout dépendante de l' utilisation d' HTML en tant que format de présentation .
Il est également l' inspirateur d' une démarche de conception " hybride " , tirant profit des techniques CSS tout en demeurant temporairement dans le cadre d' une mise en forme des documents basée sur les tableaux de présentation .
En 2007 , ce groupe comporte notamment des représentants d' Apple , Google , IBM , Microsoft , Adobe , de la Fondation Mozilla et d' Opera .
Publié comme une recommandation en mai 1998 , le second niveau de CSS étend considérablement les possibilités théoriques des feuilles de styles en cascade , avec en particulier environ 70 propriétés supplémentaires .
Après avoir connu 8 versions successives , CSS 2.1 est en juillet 2007 une recommandation candidate , c' est-à-dire le standard que doivent suivre les implémentations .
L' utilisation professionnelle de CSS reste donc limitée à un sous-ensemble arbitraire de celle-ci , déterminé par les implémentations communes .
Le développement du troisième niveau des feuilles de styles en cascade commence dès 1999 , parallèlement à celui de CSS 2.1 .
En 2008 , aucun module n' est considéré comme terminé par le W3C , et l' implémentation dans les navigateurs est donc marginale , souvent à titre de test , comme les fonctions -moz-xxx du moteur Gecko , dont le préfixe limite l' utilisation aux seuls navigateurs basés sur Gecko .
CSS ambitionnait initialement l' indépendance entre structure et présentation d' un document .
Cependant , la plupart de ces designs reposent en tout ou partie sur le remplacement du contenu textuel de la page par des images CSS qui le reproduisent en enrichissant son aspect : la liberté graphique repose toujours partiellement sur la transformation du texte en image .
D' autre part , le CSS Zen Garden reste un exercice de style limité à un document unique , à la structure doublée d' éléments et attributs sémantiquement neutres , qui ne servent qu' à donner appui à des sélecteurs CSS .
C' est davantage à travers la création de design patterns HTML CSS que s' exploite cette indépendance potentielle des styles envers la structure spécifique des documents .
En séparant structure et présentation , CSS favorise également l' écriture de documents structurés de manière sémantique , potentiellement plus exploitables par les aides techniques : la liberté de présentation des éléments de titrage permet par exemple de respecter strictement l' ordre hiérarchique formel de ceux-ci , ce qui permet en retour aux aides techniques d' en établir une table des matières navigable .
Enfin , en donnant aux auteurs les moyens d' enrichir la mise en forme du texte , CSS permet de limiter le recours aux textes mis en images .
Cependant , certaines utilisations de CSS peuvent également compromettre l' accessibilité du contenu :
CSS répond à une volonté de disposer d' un format de présentation simple , tant dans ses fonctionnalités que dans sa syntaxe , afin d' en favoriser la manipulation directe par les auteurs et les utilisateurs .
