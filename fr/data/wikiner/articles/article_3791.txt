R. U. R. est une pièce de théâtre de science-fiction , écrite en 1920 par Karel Čapek ( Tchécoslovaquie ) , mise en scène à Prague en 1921 et jouée à New York en 1922 .
C' est dans cette pièce que l' auteur utilisa le mot robot pour la première fois , bien que ce soit son frère Josef qui l' ait inventé .
