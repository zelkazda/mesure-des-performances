Peu après la publication des travaux de Charles Darwin , notamment de L' Origine des espèces en 1859 , le biologiste et philosophe allemand Ernst Haeckel proposa un arbre généalogique théorique de l' homme dans lequel il fait apparaître un " chaînon manquant " , un être intermédiaire entre le singe et l' homme .
Pour cela , il s' engagea comme médecin militaire dans l' armée des Indes orientales néerlandaises .
Nommé à Sumatra en Indonésie en 1887 , il s' y rendit convaincu qu' il trouverait sous les tropiques les traces d' un être intermédiaire entre l' homme et les grands singes .
La publication d' Eugène Dubois fut accueillie avec scepticisme .
De nombreux spécialistes doutaient du caractère humain de la calotte de Java et surtout de son association avec le fémur .
Pierre Teilhard de Chardin , considéré comme l' un des plus grands paléontologues français de l' époque , prend la direction des fouilles à Zhoukoudian .
La Seconde Guerre mondiale approche et la fouille s' arrête , notamment pour préserver la sécurité des chercheurs dans le contexte troublé de l' époque .
Les fossiles sont placés dans deux grandes caisses et partent par voie de mer en direction des États-Unis .
