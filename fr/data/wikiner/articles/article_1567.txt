Un pont de diodes ou pont de Graetz est un assemblage de quatre diodes montées en pont , qui redresse le courant alternatif en courant continu , c' est-à-dire ne circulant que dans un seul sens .
