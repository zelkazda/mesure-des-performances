Le bulgare est la principale langue de la Bulgarie .
Les officiels des deux côtés ont généralement de plus en plus tendance à passer outre ce différend ; néanmoins , le débat reste vif au sein du public , notamment en République de Macédoine , où certains média utilisent la thématique pour dénoncer un " impérialisme bulgare " .
On retrouve le même type de controverses entre la Serbie et le Monténégro , ou entre la Roumanie et la Moldavie : la construction d' une identité locale légitime la souveraineté des nouveaux états issus de la désagrégation de l' URSS ou de la Yougoslavie .
