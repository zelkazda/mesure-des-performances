Dans l' Union européenne , de nombreuses spécialités légumières sont protégées soit par des appellations d' origine protégées ( AOP ) , soit par des indications géographiques protégées ( IGP ) , soit en tant que spécialités traditionnelles garanties .
On peut citer l' exemple des hortillonnages d' Amiens .
