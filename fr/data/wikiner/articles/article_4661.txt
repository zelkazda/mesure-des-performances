Elle opère des vols domestiques et internationaux sur quatre continents , depuis ses nombreux hubs à Dallas-Fort Worth ( DFW ) , New York/JFK ( JFK ) , Miami et Chicago O'Hare ( ORD ) .
En 2010 , c' est la 3 e plus grande compagnie aérienne au monde ( derrière Delta Air Lines et Southwest ) en termes de nombre de passagers transportés et la 2 e au niveau de la taille de la flotte d' avions passager .
Elle est contrôlée par AMR Corporation et son siège social se trouve à Fort Worth , au Texas .
En 2010 , elle est au 120 ème rang du Fortune 500 .
American Airlines est membre fondateur de Oneworld .
Ce vol est considéré comme le tout premier vol régulier de ce qui allait devenir plus tard American Airlines .
Le 25 juin 1936 , American Airlines est la première compagnie aérienne à introduire , le Douglas DC-3 .
Le 10 juin 1939 , American Airlines entre à la bourse de New York .
Durant la Seconde Guerre mondiale , la moitié de la flotte de Douglas DC-3 d' American Airlines est affrété par la U.S. Army .
En 1941 , American Airlines inaugure son vol New York -- Toronto .
Septembre de la même année , American Airlines inaugure ses vols El Paso -- Monterrey et Dallas -- Mexico .
En 1946 , American Airlines établit sa base d' entretien d' aéronefs à Tulsa en Oklahoma .
En 1947 , American Airlines met en service son premier Douglas DC-6 suivi du Convair 240 en 1948 .
En janvier 1959 , American Airlines introduit dans sa flotte le Lockheed L-188 Electra .
En 1960 , American Airlines et IBM conçoivent le système de réservation SABRE , .
En 1962 , elle introduit dans sa flotte le Convair 990 et en 1964 le Boeing 727 .
En décembre 1966 , elle retire de sa flotte le Douglas DC-6 .
En 1968 , elle est la première compagnie aérienne à commander le McDonnell Douglas DC-10 .
En mars 1970 , American Airlines introduit dans sa flotte son premier Boeing 747 , .
Elle utilisera aussi des Boeing 747 pour le cargo pendant quelques années , .
Le 5 août 1971 , American Airlines effectue le premier vol commercial avec le McDonnell Douglas DC-10 .
En 1977 , elle commence à desservir Montréal , Montego Bay , Kingston , Pointe-à-Pitre et Fort-de-France .
En 1979 , le siège social déménage de New York à Fort Worth .
La même année , le 11 juin , American Airlines établit son hub à Dallas-Fort Worth .
En 1982 , elle introduit dans sa flotte le Boeing 767 et établit son hub à Chicago O'Hare .
En mai 1982 , les actionnaires approvent une réorganisation d' American Airlines et la création d' un holding , AMR Corporation , devient ainsi la maison mère de la compagnie aérienne .
Toujours en mai 1982 , American Airlines inaugure sa liaison Dallas/Forth-Worth -- Londres-Gatwick .
En 1983 , elle introduit dans sa flotte le McDonnell Douglas MD-80 , .
En 1984 , American Airlines introduit American Eagle Airlines , un réseau de compagnies aériennes régionales , desservant les plus petites villes aux plus grosses .
En 1985 , 10000 agences de voyages utilisent le système SABRE pour leurs réservations .
Toujours la même année , elle inaugure ses vols vers Genève , Tōkyō et Zurich .
En 1989 , American Airlines introduit dans sa flotte le Boeing 757 .
Le 13 septembre 1989 , American Airlines inaugure son 7 ème hub à Miami .
En 1991 , American Airlines reçoit son milliardième passager .
La même année , elle introduit dans sa flotte le McDonnell Douglas MD-11 .
En 1995 , American Airlines lance son premier site internet .
Le 21 septembre 1998 , American Airlines et quatre autres compagnies aériennes annoncent la création d' une future alliance appelée Oneworld qui entrera en fonction 1 er février 1999 .
En 1999 , American Airlines introduit le Boeing 777 et le Boeing 737-800 à sa flotte .
Avril 2001 , la Trans World Airlines est rachetée par American Airlines .
Le vol 11 s' écrasa sur la tour nord du World Trade Center et le vol 77 sur le Pentagone .
En avril 2002 , American Airlines inaugure une nouvelle route aérienne entre New York et Tōkyō , faisant ainsi sa quatrième entrée au Japon .
Le 14 janvier 2004 , American Airlines célèbre le 30 ème anniversaire du premier vol commercial à l' aéroport international de Dallas-Fort Worth .
En 2005 , American Airlines inaugure sa plus longue route aérienne entre Chicago et Delhi .
Le 2 avril 2006 , American Airlines inaugure son premier vol vers la Chine avec un Boeing 777 en lançant son vol quotidien Chicago -- Shanghai .
En 2008 , American Airlines devient le premier transporteur américain à proposer une connexion haut débit WiFi sur ses vols intérieurs .
Le 15 octobre 2008 , elle annonce qu' elle planifie la commande de 42 Boeing 787-9 Dreamliner et 58 en option .
Elle devrait recevoir ses premiers Boeing 787 début septembre 2012 et ce jusqu' en 2018 .
En décembre 2008 , American Airlines annonce une nouvelle route aérienne entre Dallas et Madrid qui entrera en service le 1 er mai 2009 .
En avril 2009 , American Airlines reçoit ses deux premiers nouveaux Boeing 737-800 d' une commande 76 qui remplaceront à long terme , ses vieux MD-80 .
Le 11 juin 2009 , American Airlines effectue depuis l' aéroport Paris-Charles-de-Gaulle vers l' aéroport international de Miami , son premier vol vert avec un Boeing 767 , .
Le 22 décembre 2009 , le vol 331 d' American Airlines Miami -- Kingston , dérape et sort de piste à l' atterrissage en Jamaïque , les 148 passagers et 6 membres d' équipage survivent mais l' on dénombre 90 blessés .
Le 7 mai 2010 , American Airlines annonce qu' elle a reçu son accord du Département des Transports des États-Unis pour voler vers Tōkyō-Haneda depuis New York/JFK à partir du 1 er octobre 2010 .
Le 21 juillet 2010 , American Airlines annonce au Salon aéronautique de Farnborough , la commande de 35 Boeing 737-800 pour livraison en 2011 et 2012 pour ainsi renouveler sa flotte pour remplacer ses vieux MD80 .
American Airlines dessert 157 destinations au Canada , aux États-Unis , au Mexique , dans les Caraïbes , en Amérique centrale , en Amérique du Sud , en Asie et en Europe .
En décembre 2009 , American Airlines exploitait 612 avions .
American Airlines est la première compagnie aérienne à avoir introduit les salons lounges dans les aéroports .
Certaines options peuvent différer selon les vols qu' ils soient domestiques aux États-Unis ou à l' international .
La classe économique d' American Airlines , propose à ses clients des sièges confortables et des écrans projecteurs en cabine ou écrans individuels dépendant de la destination et du type d' avion , avec une sélections de films et émissions de télévision , .
Sur les vols domestiques aux États-Unis et internationaux à destination du Canada , d' Amérique centrale et dans les Caraïbes , American Airlines offre un programme de repas légers payants comme des sandwichs , des chips et amandes / noix en classe économique .
American Airlines accepte uniquement les cartes de crédit depuis 2009 pour payer les différents achats à bord .
Les clients voyageant en première classe et classe affaires sur ses vols domestiques aux États-Unis et internationaux , American Airlines propose des repas et vins gratuitement , , , .
American Airlines propose en première classe à ses clients un écran individuel avec programmes sur demande , une prise de raccordement pour ordinateur portable , une liseuse , un téléphone par satellite et des écouteurs individuel Bose .
Pour ses clients en classe affaires , American Airlines propose un écran tactile avec une sélection de films , documentaires , jeux et vidéos clips .
Pour la classe économique , American Airlines propose des écrans projecteurs en cabine avec une sélections de films .
Les couvertures et les oreillers deviendront payants à compter du 1 er mai 2010 au coût de huit dollars pour tous les vols en Amérique du Nord , en Amérique centrale et aux Caraïbes .
En 2007 , American Airlines représentait 18 % du marché domestique américain des compagnies aériennes .
En 2007 , American Airlines a transporté 98,1 millions de passagers .
Une journée moyenne chez American Airlines était comme suit :
Au niveau des vols domestiques et/ou vols internationaux , voici les principaux concurrents d' American Airlines :
American Airlines est la première compagnie aérienne américaine à s' être jointe à l' agence de protection de l' environnement des États-Unis et de plus s' être engagée à réduire ses gaz à effet de serre de 30 % d' ici 2025 .
De plus , elle a commencé à remplacer sa flotte de MD-80 par de nouveaux Boeing 737-800 .
American Airlines est impliquée dans plusieurs causes sociales .
Elle est partenaire de l' Unicef depuis 1994 .
Le siège social d' American Airlines est situé à Fort Worth au Texas , adjacent à l' aéroport international de Dallas-Fort Worth .
Anciennement situé à New York , elle déménage en 1979 , son siège social à Fort Worth .
Elle est classée 3 étoiles Skytrax .
