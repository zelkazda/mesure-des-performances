Des successions de canicules ou d' étés chauds et secs suivant des hivers anormalement doux semblent avoir favorisé -- dans tout l' hémisphère Nord -- des pullulations de défoliateurs et de scolytes , que les monocultures équiennes de résineux semblent aussi favoriser .
Des programmes d' incendies contrôlés sont encouragés au Canada ( exemple ) et localement obligatoires en Suède .
