Dès les premiers vols des frères Montgolfier , l' aéronautique s' est orientée dans deux directions : les plus légers et les plus lourds que l' air .
Cette dernière voie fut explorée avec beaucoup de rigueur par Otto Lilienthal qui reste pour la plupart des vélivoles du monde le premier d' entre eux .
Il effectua entre 1891 et 1896 deux mille vols planés attestés depuis une colline artificielle à proximité de Berlin .
Le principe du planeur pendulaire d' Otto Lilienthal a retrouvé une nouvelle vie au cours des années 70 avec la pratique du deltaplane .
