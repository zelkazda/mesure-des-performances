Il couvre d' immenses étendues de territoires et s' étend sur 10 pays : le Maroc , l' Algérie , la Tunisie , la Libye , l' Égypte , le Soudan , le Tchad , le Niger , le Mali , la Mauritanie , ainsi que sur le territoire contesté du Sahara occidental .
C' est donc un pléonasme que de dire le " désert du Sahara " .
Le Sahara est le plus vaste et le seul vrai désert au sens géographique du terme car il comporte des zones hyperarides ( moins de 50 mm de précipitations annuelles et pas tous les ans ) , arides ( moins de 150 mm de précipitations annuelles et présence de végétation concentrée dans les oueds ) , semi-arides et sub-humide sèches .
Ce sont les alizés qui sont responsables de l' aridité du Sahara en repoussant les dépressions de l' atlantique nord .
Quelques centimètres de précipitations font reverdir le sable dans le Sahara .
Quand , après une averse aussi rare que violente , le soleil brille sur le Sahara , le désert de pierres se couvre , en très peu de temps , d' un tapis vert de petites plantes .
Depuis 2009 , on constate pourtant selon de nombreuses sources que le Sahara reverdit .
Des analyses de pollen montrent que des herbages et des zones forestières ont existé périodiquement au Sahara .
Dans le Ténéré les minimums ( à 13h ) sont inférieurs à 20 % et les maximums ( au petit matin ) sont inférieurs à 43 % sauf en août ( 29 % et 56 % ) .
D' innombrables canyons atteignant soixante mètres de profondeur ont entaillé le tassili n' Ajjer selon la direction des alizés : du nord-est au sud-est .
Il existe bien sûr d' autres animaux dans le désert ( des oiseaux notamment ) , mais les animaux cités plus haut sont les plus représentatifs du Sahara .
Le Sahara a 20 % de surfaces sableuses et 80 % de surfaces rocheuses où dominent des roches sédimentaires .
Les ergs sont les grands massifs de dunes , ils occupent environ 20 % de la surface du Sahara .
Les regs sont des étendues plates , caillouteuses et constituent le paysage le plus fréquent du Sahara .
On les rencontre surtout au nord du Sahara .
La plus grande , le Chott el-Jérid , couvre 5000 km² .
Lorsqu' ils sont recouverts de grès , ils sont nommés tassilis ( par exemple : Tassili des Ajjer en Algérie ) .
Les oasis sahariennes , milieu naturel et anthropique , n' occupent qu' un millième de la surface du Sahara .
On les rencontre dans les situations protégées d' une trop grande exposition au soleil dans les massifs montagneux , dans celui de l' Ennedi ou dans l' Adrar des Ifoghas au Mali .
C' est le long des oueds qu' on observe les seules formations arborées un peu denses dans le Sahara .
Le Ténéré , ou " désert des déserts " est la partie centrale du Sahara qui s' étend au Niger .
Le désert blanc , aussi appelé désert Libyque s' étend à l' ouest du Nil , sur une largeur d' environ 800 km .
Plus de cinq millions d' habitants vivent dans le Sahara , un habitant sur deux vivant dans des villes , un habitant sur huit dans le Sahara maghrébin ( estimation en 1990 ) .
Parmi les populations actuelles du Sahara , on peut noter :
On peut trouver au Sahara de nombreuses traces d' une activité humaine préhistorique ( outils , poteries , et peintures rupestres ) .
Le climat du Sahara a subi des changements climatiques durant la préhistoire :
La région actuelle du Sahara comprenait des lacs , des sources où nagaient des poissons , des fossiles d' animaux marins et des troupeaux de boeufs peints sur les parois des grottes .
Depuis 1900 , le Sahara a progressé vers le sud de 250 kilomètres et ce sur un front qui en fait plus de 6000 .
La taille , le degré d' ensoleillement et la faible population sédentaire du Sahara en font potentiellement un gigantesque " gisement " d' énergie solaire renouvelable , tant photovoltaïque que thermique .
