Les 66 habitants de La Boulouze sont rattachés au canton de Ducey , alors que le reste de la commune fait partie de celui d' Avranches .
En 1973 , elle a fusionné avec La Boulouze et Le Mesnil-Ozenne , qui gardaient le statut de communes associées .
En 1985 , Le Mesnil-Ozenne a repris son indépendance .
Le 1 er janvier 1973 , Saint-Osvin devient Saint-Ovin .
