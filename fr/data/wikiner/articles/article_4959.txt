Trente est l' un des conciles les plus importants de l' histoire du catholicisme ; il est le plus abondamment cité par le concile de Vatican II .
Le premier appel au concile émane de Luther lui-même , d' abord le 28 septembre 1518 , puis le 11 octobre 1520 : il demande l' arbitrage d' un concile dans son conflit avec la papauté .
La papauté connaît bien ce risque et Clément VII , pape de 1523 à 1534 , hésite .
Cependant , la guerre avec François I er de France rend impossible la tenue d' une assemblée universelle .
En 1534 , le pape Clément VII meurt .
Charles Quint pense qu' un concile pourra rétablir l' unité chrétienne .
Peu de temps après , la guerre reprend entre Charles Quint et François I er .
Parallèlement , le lieu de la tenue du concile pose problème : le duc de Mantoue formule des exigences démesurées tandis que la République de Venise refuse à demi-mot d' héberger l' assemblée , qui est donc ajournée .
La première séance se tient dans la cathédrale de Trente , le 13 décembre 1545 .
Calvin plaisantera à ce sujet : " si c' estoit seulement un synode provincial , ilz devroyent avoir honte de se trouver si peu. "
Contre Charles Quint qui entendait les limiter aux abus ecclésiastiques , ils se saisissent également des questions dogmatiques .
Furieux , Charles Quint interdit à ses prélats de quitter Trente , et déclare invalide le transfert .
Dès lors , les sessions tenues à Bologne sont purement formelles .
Parallèlement , Charles Quint ne fait pas poursuivre les travaux des prélats allemands restés à Trente .
Peu de temps après , Paul III meurt .
Le secrétaire du concile est élu pape et prend le nom de Jules III .
Henri II de France , furieux , récuse le concile et interdit à ses prélats de s' y rendre .
Seuls les ducs de Saxe et de Wurtemberg , l' électeur de Brandebourg et la ville de Strasbourg s' y plient .
Jules III prend acte de l' échec du concile en matière d' unité chrétienne .
Son successeur, Paul IV , élu en 1555 , se consacre à la réforme des institutions romaines .
En 1556 , Charles Quint abdique et partage ses possessions entre son fils Philippe II d' Espagne et son frère Ferdinand Ier du Saint-Empire .
Ce sera la tentative du colloque de Poissy .
Pie IV , élu en décembre 1559 doit faire face à des pressions divergentes .
En novembre , la délégation française arrive , présidée par le cardinal de Lorraine .
Les ambassadeurs français quittent Trente après moult protestations .
Les éditions de la Bible sont désormais soumises à la censure des autorités ecclésiastiques .
À la différence de versions plus récentes de la Bible , la Vulgate est reconnue fiable du fait qu' on y a jamais décelé d ' hérésie .
La lecture d' autres versions que la Vulgate n' est pas interdite .
À la suite de ce concile est rédigé , dès 1566 , le Catéchisme du Concile de Trente .
Ce concile est le dernier concile dogmatique ayant reçu une conclusion : le concile de Vatican I n' a pas été conclu ; le concile œcuménique Vatican II est un concile pastoral .
Ils n' hésitent pas à citer leur propre exemple pour dénoncer les abus ecclésiastiques : ainsi du cardinal de Lorraine , archevêque à 14 ans .
Cette petite plaisanterie pouvait coûter cher car , si l' on était pris sur le fait à Cologne , le tarif habituel était le bannissement pour le potier , sa famille et son personnel .
