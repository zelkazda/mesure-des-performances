Il a obtenu en 2005 le Prix Nobel de la paix avec l' Agence internationale de l' énergie atomique .
Né d' un père avocat , il suit des études de droit à l' Université du Caire , où il obtient son diplôme en 1962 .
Il commence en 1964 une carrière au sein du service diplomatique d' Égypte .
C' est durant cette période qu' il est en charge du contrôle des armes au sein de l' ONU .
En 1980 , il quitte le service diplomatique d' Égypte pour intégrer l' Institut des Nations unies pour la formation et la recherche ( UNITAR ) , où il est en charge du programme de droit international .
En 1984 à 1993 , il intègre l' AIEA en tant que membre du secrétariat .
Il a été directeur général de l' AIEA à partir du 1 er décembre 1997 .
Il déplore que la politique américaine vis-à-vis de l' Iran et de la Corée du Nord ne permette pas de résoudre la question des armes nucléaires dans ces deux pays .
