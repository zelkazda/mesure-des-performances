Le nom trivial , acide acétique , est le plus utilisé et est officiellement préféré par l' IUPAC .
Les bactéries qui produisent l' acide acétique , dont la première a été découverte par Louis Pasteur , sont présentes partout dans le monde , et toute culture pratiquant le brassage de la bière ou du vin a inévitablement découvert le vinaigre , résultat naturel de l' évolution de ces boissons alcoolisées laissées à l' air libre .
Le sapa était riche en acétate de plomb , une substance sucrée appelée sucre de plomb ou sucre de Saturne , et qui provoqua de nombreux empoisonnements au plomb dans l' aristocratie romaine .
L' alchimiste perse Jabir Ibn Hayyan ( Geber ) concentra l' acide acétique à partir du vinaigre par distillation .
C' est le chimiste français Pierre Auguste Adet qui prouva qu' ils étaient le même composé chimique .
En 1847 , le chimiste allemand Hermann Kolbe synthétisa l' acide acétique à partir de matières inorganiques pour la première fois .
Le premier processus commercialisé de carbonylation du méthanol , qui utilise du cobalt comme catalyseur , a été développé par l' entreprise chimique allemande BASF en 1963 .
La première usine utilisant ce catalyseur a été bâtie par l' entreprise américaine Monsanto en 1970 , et la carbonylation du méthanol catalysée au rhodium est alors devenue la méthode dominante de production d' acide acétique .
Ce processus est plus écologique et efficace que le précédent , et a largement supplanté le processus Monsanto , souvent dans les mêmes usines .
Avant la commercialisation du processus Monsanto , la majeure partie de l' acide acétique était produit par oxydation de l' acétaldéhyde .
