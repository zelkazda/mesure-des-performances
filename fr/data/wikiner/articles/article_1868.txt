Premier ministre du 6 mai 2002 au 31 mai 2005 , il est aujourd'hui sénateur de la Vienne .
Il est ensuite diplômé de l' ESCP Europe , promotion 1972 ( la même que Michel Barnier ) .
Conseiller technique de Lionel Stoléru de 1976 à 1981 , il est aussi maître de conférences à l' Institut d' études politiques de Paris de 1979 à 1988 .
Il est ensuite secrétaire général adjoint et porte-parole ( 1993 -- 1995 ) , puis secrétaire général de l' Union pour la démocratie française ( UDF ) .
Membre du bureau politique de l' UDF ( 1996 ) , il prend part à la création de Démocratie libérale , dont il est membre du bureau politique ( 1997 ) , puis vice-président jusqu' à la fusion avec l' UMP en 2002 .
Au niveau local , il est conseiller municipal d' opposition de Poitiers de 1977 à 1995 , conseiller régional de la région Poitou-Charentes en 1986 , puis président du conseil régional de 1988 à 2002 , où il succède à Louis Fruchard , son mentor .
En 1995 , il quitte le conseil municipal de Poitiers pour devenir , jusqu' en 2001 , adjoint au maire de Chasseneuil-du-Poitou .
Il est député européen de 1989 à 1995 , élu sur une liste UDF -- RPR .
En septembre de la même année , il se fait élire sénateur de la Vienne , mais n' exerce pas ce mandat pour rester au gouvernement .
Jean-Pierre Raffarin se fait surtout connaître à cette époque pour son action en faveur de la protection des artisans boulangers .
Jean-Pierre Raffarin est alors écartelé entre la politique de ce dernier ( en particulier sa politique de construction de HLM et de développement des emplois aidés ) soutenue par Jacques Chirac et la volonté de Nicolas Sarkozy de gérer les finances " en bon père de famille " , avant que ce dernier ne quitte le gouvernement pour prendre la présidence de l' UMP .
La rumeur dit que Henri Emmanuelli , tenor du " non " , l' a appelé , le soir même du 29 mai 2005 , pour le remercier : " sans vous , rien n' eût été possible. "
Il est aussitôt remplacé par Dominique de Villepin .
Une élection partielle provoquée après son départ du gouvernement par la démission de son suppléant lui permet d' être réélu , le 18 septembre 2005 , sénateur ( UMP ) de la Vienne , obtenant au premier tour 56,98 % des voix des 1046 grands électeurs .
Fin 2009 , il s' oppose à la suppression de la taxe professionnelle , notamment contre l' avis du président Nicolas Sarkozy , et persiste dans son refus de la voter en l' état .
