Il est par ailleurs président d' honneur du Pôle de renaissance communiste en France depuis 2006 .
Il a été le " doyen d' âge " de l' Assemblée nationale française .
La période 2002-2007 a été son dernier mandat , son ancien suppléant Jean-Jacques Candelier lui succèdant .
