La Chevelure de Bérénice est une constellation assez peu lumineuse qui se trouve juste à l' ouest du Lion .
Elle est associée à une légende , et est l' une des seules ( avec l' Écu de Sobieski ) qui doive son nom à un personnage historique , en l' occurrence la reine Bérénice II d' Égypte , femme de Ptolémée III Evergetes , le roi qui a fait d' Alexandrie un important centre culturel .
Devant les périls de cette expédition , et craignant pour la vie de son mari , la reine Bérénice se rendit au temple d' Aphrodite pour lui faire la promesse solennelle de sacrifier ses longs cheveux , dont elle était très fière , si le roi son mari rentrait sain et sauf de la guerre .
Quand Ptolémée revint vivant quelques semaines plus tard , Bérénice se coupa les cheveux et les déposa en offrande au temple de la déesse , selon son engagement .
Ptolémée entra dans une rage folle , fit fermer les portes de la ville pour la faire fouiller de fond en comble , mais sans résultat aucun .
Pour apaiser le roi et la reine outragés ( et pour sauver la vie des prêtres du temple ) , l' astronome de la cour , Conon de Samos , annonça que l' offrande avait tellement plu à la déesse qu' elle l' avait placée dans les cieux .
La constellation est située au sud du manche de la " grande casserole " que forme la Grande Ourse .
Partant du " manche " on trouve ~15° au sud les deux étoiles brillantes qui forment les Chiens de chasse , et encore ~15° dans la même direction l' amas stellaire .
Quand on a déjà repéré dans cette zone la forme de la Grande Ourse et Arcturus du Bouvier , le repérage de la zone est assez facile : le gros de l' amas stellaire est à mi-distance entre Arcturus et les deux étoiles qui marquent la patte avant de la Grande Ourse .
Les trois étoiles principales de la Chevelure de Bérénice sont peu lumineuses .
Cette dernière est un petit peu plus brillante que le Soleil , ce qui nous donne une idée de la faible luminosité que celui-ci aurait , même vu d' une si petite distance ( 27 années-lumière ) .
On y trouve également une partie de l' amas de galaxies dit de la Vierge .
Le pôle nord galactique ( défini par la perpendiculaire au plan de la Voie lactée ) est également situé dans cette constellation .
