Il était le fils d' une concubine issue d' une famille Jurchen ( ethnie à laquelle appartiennent les Mandchous ) .
La mère de Kangxi était donc certainement en partie une Han .
Kangxi pensait déjà pouvoir régner à l' âge de 16 ans .
Cependant , son père lui avait choisi comme régent un ministre du nom de Oboi 鳌拜 qui ne lui a pas laissé le pouvoir comme convenu .
Il dut encore faire face en 1673 à la grande rébellion des " trois feudataires " , les provinces de Yunnan , Sichuan et Guangdong ayant fait sécession simultanément .
Au Tibet , le régent Sangyé Gyatso cacha la mort du cinquième dalaï-lama Lobsang Gyatso pendant douze ans pour terminer la construction du palais du Potala .
Lhazang Khan prit Lhassa et tua le régent Sangyé Gyatso .
Le protectorat Qing sur le Tibet ne fut établi qu' en 1720 .
Kangxi ordonna la réparation du Grand Canal construit par les Sui , plus de mille ans auparavant .
En 1675 naquit son successeur Yinzhen ( futur Yongzheng ) ; à cette occasion six eunuques furent élevés à des dignités de fonctionnaires civils .
On l' appela le Dictionnaire Kangxi .
Il introduisit les 214 clefs de classement des caractères chinois qui sont toujours largement utilisées ( 214 clefs de Kangxi ) .
Kangxi avait eu de nombreux fils et choisit son héritier avec beaucoup de soin .
Le choix finit par tourner autour de deux candidats nés de la même mère , dont les noms personnels étaient homonymes ( Yinzhen ) , la graphie ne différant que par la clé du second caractère : 胤禛 et 胤禎 .
Kangxi avait tenu à garder secrètes ses dernières volontés concernant sa succession , qu' il avait placées dans une boîte .
L' un des héritiers présomptifs , quatorzième fils de l' empereur , était en campagne dans le Xinjiang au moment de la mort de son père .
