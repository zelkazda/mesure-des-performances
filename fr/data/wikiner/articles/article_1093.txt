Le territoire de la Phénicie correspond au Liban actuel auquel il faudrait ajouter certaines portions de la Syrie et d' Israël .
Les Phéniciens étaient un peuple antique d' habiles navigateurs et commerçants .
L' invasion des Peuples de la Mer va ravager les cités phéniciennes , de même que Mycènes et les autres territoires qu' ils traversent , mais c' est ce qui va permettre aux Phéniciens de trouver leur indépendance vis-à-vis des puissances voisines qui les avaient assujettis puisque celles-ci seront elles aussi détruites par ces invasions .
La chute de Mycènes en particulier va leur permettre de dominer les mers .
Selon Pline , " le peuple phénicien a l' insigne honneur d' avoir inventé les lettres de l' alphabet " .
Leur pays est prospère , très boisé , fertile , mais étroit entre la chaîne montagneuse du Liban et la mer .
Bientôt Tyr va devenir la capitale de leur empire maritime .
Ils sauront , durant des siècles , s' assurer le quasi monopole du commerce en Méditerranée .
" Des Phéniciens apportaient une foule de breloques dans leur vaisseau noir . "
Leurs navires sillonnent la Méditerranée et transportent tout ce qui peut s' échanger ou se vendre : denrées alimentaires telles que le vin , l' huile ou les grains ( blé , orge ) dans leurs amphores de forme caractéristiques , rondes et ventrues , minerais de cuivre , d' argent et surtout d' étain servant à la production du bronze .
Car c' est sur les rivages qu' avait lieu le troc avec les indigènes tandis que les transactions avec les marchands des différentes civilisations avec lesquelles ils commerçaient avaient lieu dans les emporions qui représentaient autant d' étapes des routes des Phéniciens , généralement près des temples prévus également à cet effet .
De nombreux mythes grecs font intervenir les Phéniciens , leur pays ou des dieux de leur rivage .
Leur fondation est souvent légendaire , et ce qu' on sait des constructions ( en dehors de ce que nous livre l' archéologie ) est surtout relatif au roi Hiram de Tyr .
Le choix des lieux suit les mêmes critères qu' en Phénicie , quelques fois au fond de baies selon la géographie des côtes qui se présentent .
Les fortifications , excepté en Sardaigne , sont pratiquement inexistantes : la mer est le meilleur rempart .
Il est probable que l' archipel maltais était un relais important dans le commerce avec les actuelles îles Britanniques et du Cap vert avec des dépôts de marchandises et déjà des chantiers de réparation navale .
