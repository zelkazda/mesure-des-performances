Ce descendant indirect du chevalier de Charette est aujourd'hui maire de Saint-Florent-le-Vieil , haut-lieu de la Vendée militaire .
Il est inscrit au barreau de Paris depuis 2001 .
En décembre 2009 , Hervé de Charette quitte l' UMP ( dont il est l' un des membres fondateurs ) , qu' il juge trop à droite , .
Hervé Morin annonce son ralliement au Nouveau Centre .
Néanmoins , selon le sénateur Jean Arthuis , il ne peut pas prendre cette décision .
