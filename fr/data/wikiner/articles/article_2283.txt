Will Wright est né en 1960 à Atlanta , il est le co-fondateur ( en 1987 ) de la société de création de jeu vidéo américaine Maxis .
Il est , entre autres , le concepteur de la série des SimCity et des Sims .
Ce sont Les Sims 2 , qui sortent en septembre 2004 , ainsi que des extensions un peu plus tard
Son nouveau projet , Spore , un simulateur de vie , est sorti le 4 septembre 2008 .
Will Wright naît à Atlanta , en Géorgie en 1960 , où il vit jusqu' à l' âge de neuf ans , à la mort de son père .
La famille déménage pour s' installer dans la ville natale de la mère à Bâton-Rouge , en Louisiane .
Dans une interview de 2003 , Will Wright affirma que les jeux vidéo lui prenaient une grande part de son temps , et il en conclut que la meilleure chose à faire était d' en concevoir lui-même .
Son premier jeu est Raid on Bungeling Bay , un jeu d' hélicoptère sorti d' abord sur Commodore 64 en 1984 , puis sur NES en 1985 .
Will Wright s' étonne lui-même de trouver plus amusant de créer des villes durant la période de développement du jeu , que d' y jouer en sa version finale .
De ce constat naît l' idée de SimCity , jeu de gestion dont le but est de créer et gérer sa propre ville .
Le contenu de SimCity est en partie influencé par les travaux de deux grands théoriciens de l' urbanisme , Christopher Alexander et Jay Forrester .
SimCity ( 1989 ) est un succès et considéré comme un des jeux les plus innovant jamais faits .
Sur le sentier du succès de SimCity , Will Wright crée SimEarth en 1990 et SimAnt en 1991 , qui sont en fait des déclinaisons du concept .
Le prochain jeu de Will Wright est SimCopter , en 1996 .
Bien qu' aucun de ces jeux n' ait eu le succès de SimCity , ils ont forgé la réputation de Wright en tant que concepteur de " logiciels jouets " , terme inventé par Maxis elle-même pour promouvoir ses jeux .
En 1995 , Maxis s' affiche avec un chiffre d' affaires de 38 millions de dollars .
L' action atteint un maximum de 50 $ pour ensuite dégringoler à l' annonce de pertes par Maxis .
En juin 1997 , Maxis est racheté par le géant américain de l' édition Electronic Arts .
Wright avait pensé à faire une sorte de maison de poupée virtuelle depuis le début des 1990 , semblable à SimCity mais concentré sur différentes personnes .
C' était un concept difficile à vendre à EA , parce que 40 % des employés de Maxis avait déjà été congédiés .
Les négociations aboutissent et EA publie Les Sims en février 2000 , qui devient le plus gros succès international de Will Wright .
Il surpasse Myst en tant que jeu sur ordinateur le plus vendu de tous les temps , et engendre de nombreux paquets d' extension et autres jeux .
Il conçoit une version massivement multijoueur du jeu appelé The Sims Online , mais qui n' est pas aussi populaire que l' original .
En 2005 , Will Wright est la seule personne au monde à avoir été honorée de cette façon par ces deux organismes de l' industrie du jeu vidéo .
Il est nommé l' une des personnes les plus importantes de l' industrie des divertissements électroniques par des périodiques comme le Time Magazine ou GameSpy .
À la Game Developers Conference du 11 mars 2005 , il a annoncé son futur jeu , Spore .
Le 9 avril 2009 , Will Wright décide de quitter l' entreprise Maxis , qu' il a lui même fondé .
Jeux auxquels Will Wright a participé , mais peut ne pas avoir exclusivement conçus .
Wright , présent dans la version Super Nintendo de SimCity , et depuis récurrent dans les jeux vidéo Nintendo , est bien évidemment un clin d' œil à Will Wright .
