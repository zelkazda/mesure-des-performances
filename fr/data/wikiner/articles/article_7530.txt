La Gaule belgique ou Gallia Belgica était une province romaine correspondant aux territoires actuels du sud des Pays-Bas , de la Belgique , du Luxembourg , du nord-est de la France et de l' ouest de l' Allemagne .
Certains de ces peuples se sont installés dans l' île de Bretagne , ils sont notamment mentionnés dans les Commentaires sur la Guerre des Gaules de Jules César .
La Belgique entre dans l' histoire lors de sa conquête par Jules César .
En préambule à ses Commentaires sur la Guerre des Gaules , le futur dictateur décrit un territoire ethniquement varié .
Les manuels d' histoire privilégient un ensemble disparate de gaulois et de germain , particulièrement entre la Seine et la Somme .
Ailleurs au cours du récit , Jules César nous laisse entrevoir une réalité linguistique et ethnique complexe pour le territoire de la Belgique .
Au départ , la capitale de la province fut Reims puis , à une date indéterminée , la capitale fut transférée à Trèves .
Sur le terrain , les frontières de la province belge , tant avec la Gaule lyonnaise qu' avec la Germanie , sont floues , au début de la période impériale .
Le premier événement qui vient quelque peu clarifier ces limites , même s' il les change par la même occasion , est la création par l' empereur Domitien d' abord de deux districts , puis de deux provinces de Germanie : la Germanie inférieure et la Germanie supérieure .
En revanche , un bon nombre d' habitants de cette province connurent une romanisation plus lente qu' en Lyonnaise et en Narbonnaise , cela pouvant être dû au contact des Germains qui voulurent s' y créer un domaine .
La Belgique première comprenait :
La Belgique seconde comprenait :
