Bizanos est située aux portes sud de Pau .
Paul Raymond note qu' en 1385 , Bizanos comptait treize feux et dépendait du bailliage de Pau .
Après la Révolution française , la construction de la route reliant Pau à Nay contribue au développement démographique et économique de la ville .
Bizanos fait partie de neuf structures intercommunales :
