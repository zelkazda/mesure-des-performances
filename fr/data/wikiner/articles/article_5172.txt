Indira Gandhi , née le 19 novembre 1917 et morte le 31 octobre 1984 .
L' accession de Jawaharlal Nehru à la tête de l' Inde indépendante en 1947 fait d' Indira Gandhi la plus proche collaboratrice de son père .
Le premier défi auquel est confrontée Indira Gandhi est celui de la famine qui menace l' Inde .
Cependant , si elle se montre critique à l' égard de la guerre du Vietnam , elle se voit poussée à mettre en œuvre une importante et impopulaire dévaluation de la roupie indienne en juin 1966 .
Malgré la perte de nombreux sièges , les quatrième élections générales permettent au Parti du Congrès de conserver une courte majorité en 1967 au Lok Sabha .
Comme elle occupait la position la plus élevée du gouvernement dans une société très patriarcale , on aurait pu s' attendre à ce qu' Indira Gandhi soit un chef passif , mais ses actions n' ont cessé de prouver le contraire .
Lorsqu' Indira Gandhi est exclue du parti par ses dirigeants , la scission est consommée .
Figure charismatique , parfois accusée de populisme , Indira Gandhi entretient la ferveur des masses populaires autour de sa personne pour mieux assurer son autorité et sa légitimité .
Son triomphe électoral et militaire face au Pakistan en 1971 confirme un pouvoir de plus en plus autoritaire .
En tant que premier ministre , Indira Gandhi a soigneusement utilisé tous les leviers à sa disposition pour consolider son pouvoir et son autorité .
Elle nomma ainsi des chefs de gouvernement notoirement incompétents et flagorneurs , dont Fakhruddin Ali Ahmed , un président faible qu' Indira Gandhi savait incapable d' exercer ses prérogatives et de miner son autorité .
Faisant appel à l' article 352 de la Constitution , Indira Gandhi s' octroie des pouvoirs dictatoriaux et procède à une réduction massive des libertés civiles , déjà initiée par la loi sur la prévention des activités illégales de décembre 1967 , et au muselage de l' opposition politique et de la presse .
Aussi importantes que furent ces réformes , Indira Gandhi jugea cependant que son pouvoir restait insuffisant .
En 1977 , méjugeant considérablement sa popularité , Indira Gandhi organise de nouvelles élections qui entraînent sa chute .
Elle exerce alors un mandat beaucoup moins autoritaire mais Sanjay Gandhi continue à jouer auprès d' elle un rôle prépondérant et fait figure de " dauphin " désigné .
À ce jour , le bilan d' Indira Gandhi en tant que premier ministre reste mitigé .
Bien qu' elle ait eu une personnalité forte , et que sa gouvernance ait été populaire auprès de certaines catégories de la population de l' Inde , en particulier chez les jeunes et les pauvres , sa décision de déclarer l' état d' urgence dans le but d' échapper aux poursuites reste controversée .
Les accords de Simla en 1972 avec Zulfikar Alî Bhutto consacrent alors la prédominance de l' Inde dans la région tandis qu' Indira Gandhi fait de son pays la première puissance nucléaire du tiers-monde en 1974 .
Pendant les dernières années de sa vie , elle subit la popularité grandissante d' un chef et missionnaire sikh , Jarnail Singh Bhindranwale , et de son message prônant une communauté sikh souveraine et autonome .
Par suite , en juin 1984 , l' Opération Bluestar , organisée de longue date , fut déclenchée .
Indira Gandhi mit en avant le caractère guerrier traditionnel des sikhs et leur aspiration sécessionniste afin de faire accepter par l' opinion publique le fait que l' opération était inévitable .
Le deuil officiel dura 12 jours et le retour au calme fut progressif mais émaillé d' incidents , notamment à l' occasion du 515ème anniversaire de la naissance du maître fondateur de la religion sikhe , Gurû Nanak , le 8 novembre .
Les deux fils d' Indira Gandhi , Sanjay et Rajiv menaient aussi une carrière politique .
Sanjay Gandhi mourut en 1980 dans un accident d' avion , en ratant une acrobatie aérienne .
Rajiv Gandhi est nommé premier ministre à la mort de sa mère .
Il démissionne de son poste en 1989 et sera assassiné par une femme kamikaze du LTTE près de Madras .
