Les îles Tonga ont une superficie de 748 km² et la capitale est Nuku'alofa ( " la patrie de l' amour " en tongien ) sur l' île de Tongatapu .
Ils se répandirent rapidement dans l' ensemble des îles Tonga .
À cette époque , l' empire de Tonga avait une population d' environ de 40 000 personnes .
Le 21 janvier 1643 , Abel Tasman découvrit Tongatapu et visita une partie des îles .
Entre 1773 et 1777 , James Cook prit contact avec les insulaires de Tongatapu .
L' archipel acquit son indépendance en 1970 et devint aussi un membre du Commonwealth .
Le Tonga ne constitue plus une monarchie traditionnelle .
Le roi Taufa'ahau Tupou IV , décédé le 10 septembre 2006 à Auckland , pouvait remonter son arbre généalogique sur quatre générations de monarques .
Siaosi Tupou V lui a succédé le 1 er août 2008 et vient de renoncer à tout pouvoir politique sur son royaume , et annonce des élections parlementaires pour 2010 .
Le Tonga est une monarchie constitutionnelle , où le roi détient le pouvoir exécutif .
La plus haute instance du pouvoir judiciaire est la Cour suprême , dont les juges sont nommés par le roi .
Les îles Tonga constituent un archipel d' environ 170 îles disséminées dans l' Océan Pacifique , et répartis en trois grands groupes :
La moitié des habitants sont concentrés sur l' île principale de Tongatapu , d' une superficie de 260 km² .
La densité de population moyenne des îles Tonga atteint 159 habitants au km² .
Un seul représentant du Tonga est monté sur un podium olympique .
Tonga a pour codes :
