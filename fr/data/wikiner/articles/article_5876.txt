Ramanujan travailla principalement en théorie analytique des nombres et devint célèbre pour ses formules sommatoires impliquant des constantes telles que π et e , des nombres premiers et la fonction partage d' un entier obtenue avec Godfrey Harold Hardy .
Ramanujan avait un raisonnement très rapide , ce qui faisait dire à certains de ses contemporains qu' il était un mathématicien " naturel " , voire un génie .
Une lettre de 1913 à Hardy contenait une longue liste de théorèmes sans démonstration .
Hardy considéra tout d' abord cet envoi inhabituel comme une supercherie , puis -- interpellé par l' étrangeté de certains théorèmes -- en discuta longuement avec John Littlewood pour aboutir à la conviction que son auteur était certainement un " homme de génie " .
Hardy aimait classer les mathématiciens sur une échelle de 1 à 100 .
Il s' attribuait 25 , donnait 30 à Littlewood , 80 à David Hilbert et 100 à Ramanujan .
Ramanujan a donné la formule suivante :
