La région de Château-Thierry ( l' arrondissement plus exactement ) est appelée le pays de l' Omois .
Château-Thierry se situe dans un vallon de la vallée de la Marne .
Sa situation dans la vallée de la Marne comme son réseau de transports font de Château-Thierry une ville de l' est de la France , dans la grande banlieue parisienne .
La gare de Château-Thierry est le terminus d' une ligne de Transilien -- le Transilien Paris Est , mais est aussi une des gares de la ligne inter-régionale de la ligne TER Vallée de la Marne .
Ceci est dû à la proximité de Paris , ce qui engendre d' importants flux journaliers entre le sud de l' Aisne et la capitale .
La ville de Château-Thierry regroupe 15239 habitants en 2008 .
L' ensemble de ces communes forme le périmètre de la Communauté de communes de la Région de Château-Thierry , ou CCRCT , quatrième agglomération du département après celles de Saint-Quentin , Soissons , et Laon .
Après cette période , Château-Thierry reprit son nom .
En 721 , il imagina de faire bâtir un château fort dans la dépendance immédiate du sien , sur une importante position qui dominait une chaussée romaine réparée par la reine Brunehilde .
Il alla chercher dans l' abbaye de Chelles un jeune prince , fils de Dagobert III , qui portait le nom de Thierry .
Thierry IV mourut en 737 , à l' âge de 23 ans .
Ayant succédé à son père Charles Martel , mort en 741 à Quierzy-sur-Oise , Pépin le Bref fut oint en 754 par le pape Étienne II , dans la basilique de Saint-Denis .
En 1544 , la ville est prise et pillée par Charles Quint .
Le peintre officiel de armées françaises François Flameng y a réalisé de nombreux croquis et dessins sur ces douloureux événements qui seront publiés dans la revue L' Illustration .
Le pont principal de Château-Thierry a été défendu par les hommes de l' Aspirant de Rougé .
