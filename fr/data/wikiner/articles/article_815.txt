Officiellement , le hindî devrait servir de lingua franca en Inde , ce qui , dans les faits , n' est pas le cas puisque environ 40 % de la population seulement déclare l' utiliser [ réf. nécessaire ] .
En Inde même , le hindi est parlé dans une majeure partie du nord de l' Inde , entre le Bengale à l' est et le Panjab et le Gujarat à l' ouest .
