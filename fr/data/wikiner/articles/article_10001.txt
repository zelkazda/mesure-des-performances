Neuf années de guerre s' ensuivent qui s' achèvent par la capture de Sambhaji et sa mise à mort .
Au décès d' Aurangzeb , en 1707 , le combat marathe s' éteint après 30 ans de lutte sans discontinuer contre le pouvoir moghol .
