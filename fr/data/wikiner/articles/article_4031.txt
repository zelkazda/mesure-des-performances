Ses incursions dans les autres genres ( dont Le Professionnel en 1981 ou La Maison assassinée en 1988 ) connurent également un succès auprès du public .
Cet artisan prolifique n' en demeure pas moins une figure incontournable de la comédie française de l' après-guerre ( avec Gérard Oury ) .
Il est le fils de la comédienne Renée Saint-Cyr ( qui apparaît dans une dizaine de ses films ) .
En 1933 , il monte à Paris car sa mère va débuter sa carrière cinématographique cette même année et va connaître un succès avec Les Deux orphelines .
Durant la Seconde Guerre mondiale , il est scolarisé au lycée Janson-de-Sailly , à Paris .
Malgré cette période difficile , il essaie de préserver une jeunesse fêtarde , puis se sentant concerné par ce qui se passe en France , il n' hésite pas à venir observer de plus près les évènements dans la capitale , ce qui ne manque pas de développer son sens critique .
En 1947 , il est contraint de cesser ses petits boulots pour aller faire son service militaire en Autriche et va faire un stage de projectionniste 16 mm .
Sorti de l' armée , son expérience en matière de pellicule lui vaut de devenir en 1949 le second assistant-réalisateur de Sacha Guitry pour Le Trésor de Cantenac .
Le film amorti l' échec du précédent , lui permettant de réaliser avec son équipe Arrêtez les tambours , avec Bernard Blier .
Ce film marque le début de sa collaboration avec le chef-opérateur Maurice Fellous .
Mais c' est en 1961 qu' il va se faire connaître du grand public avec Le Monocle noir .
Sa façon de tourner -- usage du champ/contre-champ qui permet de jouer avec la profondeur et d' orchestrer ainsi une composition visuelle particulière devient une de ses marques de fabrique et les gros plans de manière à mettre les comédiens et le dialogue en valeur -- lui vaut d' être recommandé par Bernard Blier et Michel Audiard à Alain Poiré , directeur de production chez Gaumont .
En 1963 , Poiré lui offre la réalisation des Tontons flingueurs .
Avec Lino Ventura , Bernard Blier et Francis Blanche au casting et Michel Audiard aux dialogues , le film , sommet de la parodie de film policier , est un succès et devient un classique du cinéma français .
Il rencontre à la même époque Mireille Darc et la fait tourner dans une dizaine de films .
En 1968 , il réalise le film policier Le Pacha , avec Jean Gabin , qui devait tourner dans Les tontons flingueurs , et dialogué par Audiard .
Après l' échec de son long-métrage La Route de Salina , il tourne la comédie Laisse aller , c' est une valse , avec Jean Yanne .
Ce film marque les débuts au cinéma de Coluche .
Après une collaboration avec Delon ( Les Seins de glace et Mort d' un pourri ) , Georges Lautner fait tourner Jean-Paul Belmondo à partir de 1979 dans Flic ou voyou .
Il tourne ce qui est son dernier film pour le cinéma , L' Inconnu dans la maison , qui ne connaît pas le succès escompté , puis signe deux téléfilms et un court-métrage de 1996 à 2000 .
