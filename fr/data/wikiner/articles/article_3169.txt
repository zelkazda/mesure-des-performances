Jean-Claude Guibal est un homme politique français né le 13 janvier 1941 à Ajaccio ( Corse-du-Sud ) .
Il est marié à la sénatrice Colette Giudicelli .
Ancien dirigeant d' organisations professionnelles , Jean-Claude Guibal est élu maire de Menton ( 29266 hab. ) en 1989 .
Après avoir été membre de l' ancienne UDF , proche de Raymond Barre , puis affilié au RPR enfin à l' UMP .
Il est député de la quatrième circonscription des Alpes-Maritimes depuis 1997 ( réélu en 2002 , puis en 2007 ) .
Il a pratiquement changé d' étiquette politique à chaque élection , sauf à la toute dernière élection parlementaire ( mais le sigle UMP a changé de sens entre 2002 et 2007 ) .
Le 10 juin 2007 , il est réélu député de la quatrième circonscription des Alpes-Maritimes dès le premier tour en obtenant 59,50 % des suffrages .
Le 16 mars 2008 , il est réélu maire de Menton avec 58,78 % des suffrages au second tour .
