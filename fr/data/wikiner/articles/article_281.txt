La liste suivante donne pour chacune des communes son code postal , son code INSEE , l' aire urbaine à laquelle elle se rattache ainsi que son appartenance éventuelle à une structure intercommunale .
Note : les données présentées ici ne concernent que les communes appartenant à la Gironde .
