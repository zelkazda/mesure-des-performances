Bruxelles [ bʀysɛl ] est la capitale de la Belgique ( Bruxelles-ville ) et l' un des sièges de l' Union européenne et de plusieurs de ses institutions .
Elle est le siège des gouvernements et parlements de plusieurs des entités fédérées qui composent la Belgique -- la Région de Bruxelles-Capitale , la Communauté française de Belgique , la Communauté flamande et la Région flamande .
Enfin , c' est aussi la ville où siègent certaines organisations internationales , dont l' OTAN .
Bien que Bruxelles fut historiquement d' expression néerlandaise ( brabançon ) , la ville s' est francisée jusqu' au point de devenir majoritairement francophone .
Il existe d' autres exemples tels qu' Auxerre ou Auxonne en Bourgogne .
Le terme Bruxelles est le plus souvent utilisé pour désigner la ville-région , administrée par la Région de Bruxelles-Capitale .
La commune centrale , qui prend le nom de " Ville de Bruxelles " ( communément appelée " Bruxelles-Ville " ) est un ensemble hétéroclite comprenant l' hypercentre ( le Pentagone ) et une série d' extensions urbaines , comme le populeux quartier nord de Laeken et le quartier maritime , la très bourgeoise avenue Louise au sud , le Bois de la Cambre , ou encore le quartier central d' affaires Léopold , où se concentrent notamment les institutions européennes .
Les 18 autres communes s' agglomèrent autour du Pentagone ( l' hypercentre ) pour former une ville comptant un million d' habitants et une centaine de quartiers distincts .
La ville-région est une des trois régions fédérées de Belgique , les deux autres étant la Région wallonne et la Région flamande .
Une comparaison avec d' autres communes belges est possible dans la liste des communes belges les plus peuplées , où Bruxelles n' apparaît pas d' un seul tenant , mais par division communale .
La ville est par ailleurs le siège du gouvernement flamand et le siège du gouvernement de la Communauté française de Belgique ( le siège du gouvernement wallon étant lui à Namur ) .
Pour les aspects institutionnels , référez-vous à la page " Région de Bruxelles-Capitale " .
Bruxelles s' étend sur les 19 communes de la Région de Bruxelles-Capitale et compte un peu plus d' un million d' habitants .
L' agglomération réelle , en tenant compte de la zone d' emplois ( zone RER ) compte de l' ordre de 2,7 millions d' habitants et s' étend sur une grande partie des deux provinces de Brabant ( Brabant flamand et Brabant wallon ) .
Bruxelles fait également partie d' une large conurbation qui s' étend en triangle entre Bruxelles , Anvers et Gand et qui rassemble environ 4,4 millions d' habitants .
Enfin , Bruxelles partage avec Washington le titre de ville comptant le plus de journalistes accrédités .
Bruxelles , qui a fêté son millénaire en 1979 , a une histoire mouvementée liée à celle du continent européen dans la même période .
Étymologiquement , le nom de " Bruxelles " est expliqué de diverses manières , pour les uns il serait d' origine flamande signifiant en vieux flamand " habitation " ( sel/zele ) " des marais " ( broek ) , en effet , jusqu' au voûtement de la Senne en 1871 , Bruxelles était marécageux et sujet à des inondations périodiques .
D' autres encore [ réf. nécessaire ] , pensent que la première partie du mot correspond à un autre mot celte : briga , " hauteur " ; ajouté au terme latin cella , " le temple " , cela signifierait " le temple sur la hauteur " , comme c' est d' ailleurs toujours le cas de la cathédrale Sainte-Gudule .
C' est également à Bruxelles ( Evere ) que se situe le siège de l' Organisation du traité de l' Atlantique Nord .
La ville accueille 120 institutions internationales , 159 ambassades ( intra muros ) et plus de 2500 diplomates , faisant de Bruxelles le deuxième centre de relations diplomatiques au monde ( après New York ) .
Bien qu' historiquement Bruxelles fut une cité dont les habitants parlèrent des dialectes brabançons -- communément appelé dialecte flamand , -- , la situation linguistique bruxelloise changea radicalement au cours des deux derniers siècles .
À partir des années 1960 , à la suite de la fixation de la frontière linguistique et de l' essor socio-économique de la Région flamande , la francisation des néerlandophones a stagné , .
Les 19 communes bruxelloises constituent ensemble la seule partie officiellement bilingue de la Belgique .
La création de la Région de Bruxelles-Capitale a été longtemps retardée du fait des visions différentes sur le fédéralisme en Belgique .
En 1989 , la Région de Bruxelles-Capitale a tout de même fini par être créée officiellement .
Bruxelles s' impose comme une des principales villes d' affaires d' Europe .
Sa position géographique au centre des régions les plus dynamiques d' Europe , son statut de grand centre urbain et ses fonctions de multiple capitale font de la ville un réservoir d' emplois très qualifiés , dominés par les activités tertiaires de pointes .
Le Parlement européen a également son siège à Bruxelles pour les sessions extraordinaires et les commissions .
Dans les faits , le travail parlementaire se fait à Bruxelles , les députés n' allant à Strasbourg que pour voter les lois .
La région de Bruxelles-Capitale compte plus d' un million d' habitants et connait une remarquable augmentation de sa population .
Bruxelles est la ville qui croît le plus vite en Belgique .
Bruxelles fait aussi partie d' une plus large conurbation en triangle avec Gand et Anvers qui compte environ 4,4 millions d' âmes ( un peu plus de 40 % de la population totale de la Belgique ) et rassemble l' essentiel de l' activité économique de la Belgique .
L' Eurostar relie Bruxelles à Londres ( via Lille ) .
Bruxelles est desservie par plusieurs gares :
Il existe un projet de RER devant apporter une réponse aux gros problèmes de mobilité rencontrés par la ville .
La RER devrait être terminé pour 2016 .
Les transports urbains sont assurés par un réseau dense de tramways ( en surface et souterrains ) , d' autobus et de métro géré par la STIB .
Bruxelles est aussi desservi par deux aéroports :
Bruxelles est une ville étendue , l' espace disponible par habitant dépasse la moyenne des autres capitales européennes .
) , concentrées dans les quartiers d' affaires de la ville : Quartier Nord , quartier européen , avenue Louise .
Après la Première Guerre mondiale , les destructions ainsi que la forte croissance démographique due à l' afflux de nouveaux habitants venus des autres régions du pays provoquent une crise du logement et une extension rapide des surfaces construites .
À l' instar de nombreuses villes dans les pays anglo-saxon , Bruxelles a connu depuis l' après-guerre jusqu' aux années 1990 un très fort mouvement de périurbanisation des classes moyennes et supérieures , cherchant en périphérie un habitat de standing tandis que le centre , abandonné , se taudifiait et devenait la proie de nombreuses spéculations immobilières .
L' Orchestre national de Belgique est en résidence à Bruxelles .
Les taux de couverture végétale et d' espaces naturel sont plus importants en périphérie où ils ont limité la périurbanisation de la capitale , mais ils diminuent fortement vers le centre de Bruxelles : 10 % du pentagone , 30 % de la première couronne et 71 % de la deuxième couronne sont des espaces verts !
Ce sont aussi les axes de " mobilité douce " , dont sur 14 itinéraires ( radials ) en cours de verdurisation et de ramification en périphérie de Bruxelles .
Des perruches prospèrent en se réchauffant l' hiver sur des transformateurs électriques ou des lampadaires , et de nombreuses plantes venues de tous les continents peuvent être trouvées à Bruxelles .
En moyenne ( moyenne faite sur une période couvrant les 30 dernières années ) , on observe un peu plus de 130 jours de pluie par an dans la région de Bruxelles .
Selon la presse , Bruxelles attirerait de très nombreux services d' espionnage nationaux et internationaux , en raison de la présence du siège d' institutions comme l' OTAN , la Commission européenne ou le Parlement européen .
Bruxelles serait le deuxième nid d' espions dans le monde après le quartier du siège de l' O.N.U à New York .
La ville de Bruxelles est actuellement jumelée avec :
