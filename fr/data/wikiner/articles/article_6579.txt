La Creuse est située dans la région Limousin .
D' une superficie de 5565 km 2 , la Creuse est subdivisée en 260 communes accueillant une population de 123861 habitants ( recensement de 2007 ) , soit une densité de 22 habitants au kilomètre carré .
Le tableau suivant donne la liste des 260 communes par ordre alphabétique , avec leur code INSEE , leur code postal principal , leur population , leur superficie en kilomètre carré ( 1 km²= 100 hectares ) , leur densité de population est exprimée en habitants au kilomètre carré , leur appartenance aux principales structures intercommunales et cantonales .
La Creuse compte les structures intercommunales suivantes ( les sièges sont indiqués entre parenthèses ) :
