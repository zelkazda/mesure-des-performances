L' entreprise s' est adaptée avec le temps et a produit par la suite une gamme d' ordinateurs personnels bon marché tournant sous MS-DOS , dont le premier était le Amstrad PC-1512 .
En 1984 , sans doute inspiré par les succès de Sir Clive Sinclair , il se lance sur le marché de la micro-informatique domestique en commercialisant une machine qui réunit tout le savoir-faire anglo-saxon en la matière ( une architecture à base de Z80 , sur un marché alors déjà ancien ) .
Il se démarque de la concurrence par un design harmonieux et coloré qui doit beaucoup à l' Oric Atmos .
En matière de programmation , le langage BASIC des Amstrad conçu par Locomotive Software est des plus rapides , souple et puissant à la fois , avec un éditeur intégré , malheureusement " ligne à ligne " .
Reprenant les principes qui ont fait le succès de sa gamme à usage domestique , Amstrad sort dans la deuxième moitié des années 1980 une gamme PCW , destinée à conquérir le marché à usage professionnel .
Là aussi , le postulat est d' intégrer à un prix abordable un système d' exploitation éprouvé le CP/M et les outils de base ( tableur , traitement de texte , imprimante , écran , clavier ) à un prix forfaitaire abordable pour l' époque .
C' est cette dernière activité qui a motivé l' OPA de BSkyB en juillet 2007 , à qui Amstrad écoulait deux tiers de ses ventes .
Alan Sugar restera à la direction de l' entreprise et gardera 28 % de son capital .
