Son usage par les sages-femmes pour accélérer la délivrance semble ancestral même s' il n' est mentionné dans un recueil de plantes médicinales qu' en 1582 par le docteur allemand Adam Lonitzer .
Ce n' est qu' en 1918 qu' Arthur Stoll isole enfin un alcaloïde , l' ergotamine ce qui ouvre la voie à l' usage thérapeutique .
Saint Antoine est le saint-patron des ergotiques .
Pendant l' été 1951 , une série d' intoxications alimentaires frappe la France , dont la plus sérieuse à partir du 17 août à Pont-Saint-Esprit , où elle fait sept morts , 50 internés dans des hôpitaux psychiatriques et 250 personnes affligées de symptômes plus ou moins graves ou durables .
En définitive , l' affaire du pain maudit de Pont-Saint-Esprit conserve , à ce jour , tout son mystère .
