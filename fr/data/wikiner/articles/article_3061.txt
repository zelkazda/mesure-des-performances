Son nom officiel est République arabe syrienne .
Cette région fut un temps regroupée comprenant la Syrie actuelle , le Liban actuel , la Jordanie actuelle , la Palestine .
Durant l' Antiquité , ces pays étaient distinctement la Phénicie , la Palestine , l' Assyrie et une partie de la Mésopotamie occidentale .
L' origine du nom " Syrie " est incertaine .
Hérodote y voit plutôt une forme abrégée d' Assyrie .
En 1865 , il devient le nom officiel d' une province , celle du vilayet de Damas .
Dans la ville excavée d' Ebla , dans le nord-est de la Syrie , les archéologues ont découvert en 1975 les vestiges d' un grand empire sémite , qui va du nord de la mer Rouge à la Turquie et jusqu' en Mésopotamie dans sa partie orientale .
La Syrie compte d' autres grands sites archéologiques comme Mari , Ougarit et Doura Europos .
; c' est une des villes les plus anciennes du monde et elle a été habitée sans interruption dans le monde ( comme Vârânasî et Jéricho ) .
Cet empire s' étendait de l' Espagne à l' Asie centrale ( 661 à 750 apr .
Les Ottomans ont régné sur le pays pendant plus de 400 ans jusqu' en 1918 , excepté la brève période où l' Égyptien Ibrahim Pacha occupa le pays de 1832 à 1840 .
Le pays se libéra de l' occupation ottomane après la Révolte arabe , les forces arabes entrèrent à Damas en 1918 .
Un royaume arabe syrien , indépendant fut alors créé , Fayçal , issu de la famille hachémite , frère d' Abd Allah ibn Hussein , en sera le premier et dernier roi .
Mais les Syriens continuèrent d' exiger le départ des Français , avec l' appui des Britanniques .
Leur vœu fut exaucé en 1946 , avec l' indépendance de la Syrie .
Les premiers pourparlers entre Français et Syriens en vue de la rédaction d' un traité d' indépendance débutèrent en septembre 1936 .
Mohammad Al-Abid fut le premier président syrien du mandat français , il incarna alors dans le pays la volonté de la France .
Mais la France refusa de continuer la négociation du traité et maintint sa présence dans le pays jusqu' en 1946 .
Les États-Unis et le Royaume-Uni portent un intérêt considérable à Chichakli ; les Britanniques espèrent même l' amener à adhérer au Pacte de Bagdad .
Les Américains , dans l' espoir qu' il signe un traité de paix avec Israël , lui offrent par ailleurs une aide étrangère considérable .
Devant l' agitation , Chichakli refuse l' accord avec les États-Unis .
Les civils reprennent le pouvoir en 1954 , après le renversement du président Chichakli .
Mais une grande instabilité politique régnait alors dans le pays , le parallélisme des politiques syriennes et égyptiennes et l' appel à l' union du président égyptien Gamal Abdel Nasser à la suite de la crise du canal de Suez en 1956 a créé des conditions favorables à l' union entre l' Égypte et la Syrie .
Le 1 er février 1958 , l' Égypte et la Syrie s' unissent , créant la République arabe unie , ce qui entraîne , de facto , l' interdiction des partis politiques syriens .
Le gouvernement syrien réfléchit , à nouveau , à l' éventualité d' une union entre l' Égypte , l' Irak et la Syrie .
Un accord fut signé au Caire le 17 avril 1963 , pour mettre en œuvre un référendum sur l' union qui devait se tenir en septembre 1963 .
Le 23 février 1966 , un groupe de militaires , toujours issus du Baas , avec à leur tête Salah Jedid , réussit à renverser le gouvernement d' Amin al-Hafez .
Mais la guerre des Six Jours , perdue par les Égyptiens et les Syriens , affaiblit du même coup le gouvernement de Salah Jedid .
Officiellement , la Syrie est une république parlementaire .
Les Syriens sont régulièrement appelés aux urnes , mais ils n' ont pas le droit de voter contre le gouvernement .
Le président actuel est Bachar el-Assad , qui a succédé à son père le 17 juillet 2000 .
Un autre facteur de son maintien au pouvoir est le nationalisme , dont entre autres les conflits qui l' opposait et qui oppose la Syrie aux États-Unis , à Israël et à l' Irak de Saddam Hussein .
8 partis politiques ont été légalisés dans le pays , ils font tous partie du Front national progressiste .
C' est la même chose pour le parlement , le Conseil du peuple .
Dans la courte période de 6 mois , le printemps de Damas a vu des débats politiques et sociaux intenses , d' une part , et d' autre part il a conservé un écho qui sonne encore dans les débats politiques , culturels et intellectuels jusqu' à aujourd'hui en Syrie .
La Syrie est divisée en quatorze gouvernorats , ou muhafazat ( singulier : muhafazah ) , portant chacun le nom de leur chef-lieu .
Le Golan est un des principaux sujets de discorde entre Israël et la Syrie .
Ce dernier le considère comme territoire syrien occupé , alors qu' Israël le considère comme annexé .
L' essentiel du territoire syrien est constitué par un vaste plateau calcaire ( hamada ) surmonté de quelques anciens reliefs volcaniques ( djebel druze ) , et traversé au nord-est par le fleuve Euphrate .
La Syrie est un pays majoritairement aride , en particulier à l' intérieur et dans la partie orientale .
La Syrie reçoit de plus son eau des pays voisins : 50 % des réserves proviennent de Turquie , 20 % du Liban .
La Syrie exploite ainsi aujourd'hui plus de 50 % des ressources renouvelables , alors que le seuil maximum communément admis est de 30 % .
Le nord-est du pays ( " Djézireh " ) et le sud sont des zones agricoles importantes .
La Syrie connaît un climat tempéré composé de quatre saisons .
La plupart des Syriens vivent non loin de l' Euphrate et le long de la côte , une bande de terre fertile entre les montagnes côtières et le désert .
L' alphabétisation chez les Syriens est de 89 % chez les garçons et de 64 % chez les filles .
Un plus petit nombre de Syriens sont d' origines non sémitiques , issus de peuples ayant occupé la région .
Leur nombre était estimé à 40000 , mais la plupart ont émigré vers Israël dans les années 1990 .
Il reste aujourd'hui une petite minorité juive à Damas et à Alep .
Les juifs israéliens originaires de Syrie conservent pour la plupart des liens avec leur pays d' origine .
Les Kurdes , qui sont linguistiquement un peuple indo-iranien , représentent la plus grosse minorité ethnique du pays , avec 10 % de la population .
La plupart des Kurdes vivent dans le nord-est de la Syrie , et beaucoup parlent encore la langue kurde .
Beaucoup de Kurdes vivent aussi dans les grandes villes syriennes .
Les premières traces d' agriculture ou d' élevage furent trouvées en Syrie .
Le premier alphabet du monde fut inventé en Syrie , à Ougarit .
Les réalisations artistiques et culturelles de la Syrie antique sont nombreuses .
Les archéologues ont découvert que la culture syrienne rivalisait avec celles de la Mésopotamie et de l' Égypte , surtout autour d' Ebla .
Cicéron était un élève d' Antiochos d' Ascalon à Athènes .
Et les livres de Poseidonios ont beaucoup influencé Tite-Live et Plutarque .
Les Syriens ont aussi contribué à la littérature et à la musique arabe et ont une grande tradition de la poésie orale et écrite .
La série télévisée syrienne de Bab el Hara , très connue dans le monde arabe , a eu un énorme succès .
Damas , la capitale de la Syrie , a été élue capitale culturelle du monde arabe en 2008 .
La Syrie a pour codes :
