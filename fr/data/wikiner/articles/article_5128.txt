Saturne est une ancienne divinité romaine , agraire à l' origine ( on lui attribuait notamment la protection des semailles ) , qui a été peu à peu assimilée au titan grec Cronos .
Titan toutefois y mit une condition , c' est que Saturne ferait périr toute sa postérité , afin que la succession au trône fût réservée aux propres fils de Titan .
Sachant qu' un jour il serait lui aussi renversé du trône par un de ses fils et pour tenir la promesse faite à Titan , il exigeait de son épouse qu' elle lui livrât les nouveau-nés mâles .
Ayant découvert le subterfuge , Titan emprisonna Saturne et Ops , après quoi Jupiter , devenu adulte , déclara la guerre à Titan et à ses fils , les vainquit , libéra ses parents et rétablit son père sur le trône .
Mais ayant découvert que Saturne , peu reconnaissant , complotait contre lui , Jupiter finit par traiter ce dernier comme Uranus avait été autrefois traité par ses propres fils , après quoi il le chassa du ciel .
Ainsi la dynastie de Saturne se continua au détriment de celle de Titan .
Selon une autre tradition , pour éviter que ne s' accomplisse la prédiction selon laquelle il serait détrôné par l' un de ses enfants , Saturne dévora chacun d' eux à leur naissance .
Mais un jour , son épouse Cybèle et sa mère Tellus réussirent à sauver Jupiter en faisant avaler à Saturne une pierre enveloppée dans des langes à la place de son fils .
C' était pour rappeler la mémoire de cet âge heureux qu' on célébrait à Rome les Saturnales .
Elles commençaient le 16 décembre de chaque année : d' abord elles ne durèrent qu' un jour , mais l' empereur Auguste ordonna qu' elles se célèbreraient pendant trois jours auxquels plus tard Caligula en ajouta un quatrième .
De plus tous les habitants de la ville cessaient leurs travaux : la population se portait en masse vers le mont Aventin , comme pour y prendre l' air de la campagne .
À Rome , le temple que ce dieu avait sur le penchant du Capitole fut dépositaire du trésor public , par la raison que , du temps de Saturne , c' est-à-dire durant l' âge d' or , il ne se commettait aucun vol .
Sa statue était attachée avec des chaînes qu' on ne lui ôtait qu' au mois de décembre , époque des Saturnales .
Saturne était communément représenté comme un vieillard courbé sous le poids des années , tenant une faux à la main pour marquer qu' il préside au temps .
Il est l' époux de Rhéa / Cybèle / Ops .
