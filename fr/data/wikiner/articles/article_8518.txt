Il confortera Arnold Schwarzenegger dans son statut de star de cinéma d' action , acquis en 1982 avec Conan le Barbare .
En 2008 , le film a été inclus aux États-Unis dans le National Film Registry pour conservation à la Bibliothèque du Congrès .
Terminator a donné lieu à trois autres films , Terminator 2 : Le Jugement dernier , Terminator 3 : Le Soulèvement des machines et Terminator 4 : Renaissance , seuls les deux premiers ayant été réalisés par James Cameron .
Los Angeles , an 2029 .
Los Angeles , 1984 .
Il s' agit d' un Terminator , un robot cyborg tueur ( un endosquelette de métal recouvert de tissus charnels humains ) .
Envoyé du futur pour une mission d' infiltration meurtrière , son objectif est de tuer une certaine Sarah Connor , dont la vie aura une grande importance dans les années à venir .
En effet , celle-ci doit donner naissance au futur chef de la résistance humaine , John Connor .
Mais la résistance humaine a également envoyé un soldat , Kyle Reese pour contrer le Terminator .
The Sarah Connor Chronicles est diffusée depuis le 13 janvier 2008 sur le réseau américain FOX .
La série est située après Terminator 2 .
Cette série fait l' impasse sur Terminator 3 , les producteurs expliquant que c' est leur propre vision de la franchise .
La série a été officiellement annulée par la chaîne américaine FOX en juin 2009 mettant en cause une chute des audiences tout au long de la diffusion de la seconde saison , .
