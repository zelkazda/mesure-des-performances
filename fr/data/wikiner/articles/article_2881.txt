Saint Seiya ( 聖闘士星矢 , Saint Seiya ? ; en français Les Chevaliers du Zodiaque ) est un manga de Masami Kurumada publié pour la première fois en 1986 .
Il compte 28 volumes et est publié en français aux éditions Kana .
La deuxième partie du chapitre Hadès a été diffusée en deux volets jusqu' en 2007 et la dernière partie de ce chapitre fut diffusée en mars 2008 .
Plusieurs œuvres dérivées du manga original ont été publiées , notamment Saint Seiya , épisode G , Saint Seiya : Next Dimension et Saint Seiya : The Lost Canvas .
Aiolos présente la petite fille comme étant la réincarnation de la déesse Athéna .
Athéna a 88 chevaliers , chacun associé à une constellation stellaire .
Pourtant de force moindre à leurs adversaires , les interventions d' Athéna leur permettent de remporter les victoires .
Mais Mitsumasa Kido succombe à une maladie .
Beaucoup d' entre eux sont corrompus par Saga et se retournent contre Saori , pensant qu' elle n' est pas la déesse Athéna .
Fin avril 2006 , l' auteur de la série originelle , Masami Kurumada , a annoncé sur son blog le début de la prépublication d' une nouvelle série nommée Saint Seiya : Next Dimension .
En France , ces épisodes ont été diffusés à la télévision , au Club Dorothée sur TF1 , entre la fin des années 1980 et le milieu des années 1990 .
Il a fallu attendre 13 ans avant que la dernière partie du manga , le chapitre Hadès , ne soit adaptée en animation .
La diffusion a été assurée sur la chaine de télévision pay-per-view japonaise Animax .
La deuxième partie de la saga Hadès , se déroulant dans le monde des ténèbres , est constituée de 6 OAV .
Ils ont été projetés au cinéma au Japon .
Liste de jeux spécialement conçus pour l' univers de Saint Seiya .
Liste des jeux dans lesquels les personnages du manga Saint Seiya font leur apparition .
