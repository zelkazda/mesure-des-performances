Également conseiller général , il dirige pendant 7 ans la fédération socialiste de son département d' origine , et est élu en mars 2001 président du conseil général de l' Isère .
Il est devenu en juin 2007 porte-parole du groupe socialiste à l' Assemblée nationale .
Secrétaire national du Parti socialiste en charge des questions de justice , il démissionne le 12 janvier 2009 pour " raisons personnelles " .
