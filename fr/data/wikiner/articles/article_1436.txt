Warhammer est un univers imaginaire médiéval fantastique développé pour servir de cadre au jeu de figurines Warhammer Fantasy Battle ( 1983 ) ainsi que pour d' autres jeux , notamment le jeu de rôle Warhammer Fantasy Roleplay ( 1986 ) .
Les deux fondateurs de la société Games Workshop , les britanniques Steve Jackson et Ian Livingstone , ont débuté dans le domaine du jeu dans les années 1980 .
Ils possédaient une boutique et ont importé Donjons et Dragons ; ils ont fondé la première entreprise d' édition de jeux de rôle en Europe .
Ils ont lancé un fanzine , devenu un véritable magazine , White Dwarf , sur D & D et son univers .
Mais trouvant l' univers trop " naïf " et voulant jouer à une échelle plus importante , ils ont inventé un " jeu de guerre " ( wargame ) qui a connu un succès grandissant : Warhammer Fantasy Battle .
Au mélange des épopées tolkienites et du chaos de Moorcock s' est ajouté une inspiration historique très forte .
Kislev enfin est un royaume nordique aux forts accents polonais .
Les nombreuses chaînes de montagnes abritent des forteresses naines dans le plus pur style " Tolkien " : en lutte perpétuelle contre les créatures souterraines comme les gobelins ou les skavens ( des hommes-rats )
Les romans sont traduits en français et édités par la Bibliothèque Interdite .
