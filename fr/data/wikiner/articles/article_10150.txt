Dilbert est un comic strip satirique américain qui met en scène le monde de l' entreprise au travers de son personnage principal , Dilbert , un ingénieur informaticien .
Cette bande dessinée , créée par Scott Adams , paraît en syndication dans les journaux depuis 1989 .
Tout employé d' une grande société retrouve avec un amusement mêlé de consternation de nombreux aspects de sa vie de bureau dans la série Dilbert .
L' environnement de Dilbert est l' étroit cadre d' un bureau paysagé à l' américaine , agrémenté de notes de services , de crâne d' oeufs stressés ayant perdu tout contact avec la réalité , de lubies de chefs , de projets dérapant totalement , voire pouvant aller jusqu' à menacer sérieusement l' intégrité physique des clients finaux .
L' observation et l' analyse est si fine qu' on pourrait aller jusqu' à utiliser les dessins de Dilbert pour illustrer de véritables cours de management .
Le style de Scott Adams est un humour à froid , elliptique , caractérisé par une grande économie de moyens au niveau graphique et des mots .
