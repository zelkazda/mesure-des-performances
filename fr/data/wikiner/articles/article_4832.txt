Le GNU Debugger également appelé gdb est le débogueur standard du projet GNU .
Il fut écrit par Richard Stallman en 1988. gdb est un logiciel libre , distribué sous la licence GNU GPL .
Notons également que gdb est souvent invoqué en arrière-plan par les environnements de développement intégré comme Eclipse .
