Une association pour le maintien d' une agriculture paysanne ( AMAP ) est , en France , un partenariat de proximité entre un groupe de consommateurs et une ferme locale , débouchant sur un partage de récolte régulier ( le plus souvent hebdomadaire ) composée des produits de la ferme .
Un des exemples le plus ancien du concept a émergé dans les années 1960 au Japon .
À l' époque , des mères de familles japonaises s' inquiètent de voir l' agriculture s' industrialiser avec un recours massif aux produits chimiques ( en 1957 , les premières victimes de Minamata , empoisonnées au mercure , sont déclarées ) .
Au Japon , un foyer sur quatre participe à un teikei ( 16 millions de personnes en 1993 ) .
À la même époque en Suisse , des fermes communautaires nommées food guilds ( ou association alimentaire ) développent leur propre partenariat avec les consommateurs locaux en leur fournissant chaque semaine des produits frais ( légumes , lait , œufs , et fromages ) .
Le concept se répand ensuite rapidement par bouche-à-oreille dans tous les États-Unis , puis gagne le Canada .
Depuis fin 2007 environ 750 AMAP approvisionnent en France environ 30000 familles , soit 90000 personnes .
Les colloques suivants ont eu lieu à Aubagne ( fin janvier 2008 ) et à Kobé ( février 2010 ) .
