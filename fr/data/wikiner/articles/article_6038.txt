La Communauté économique européenne ( CEE ) était une organisation internationale créée en 1957 pour mener une intégration économique ( dont le marché unique entre l' Allemagne , la Belgique , la France , l' Italie , le Luxembourg , et les Pays-Bas ) .
À partir de 1967 , ses institutions dirigeaient la Communauté européenne du charbon et de l' acier ( CECA ) et l' Euratom désignés alors " Communautés européennes " .
En 1951 , la signature du Traité de Paris créa la Communauté européenne du charbon et de l' acier ( CECA ) .
Après la Conférence de Messine en 1955 , Paul Henri Spaak eut la tâche de préparer un rapport sur l' idée d' union douanière .
Les communautés naissant suite à la signature du Traité de Rome étaient la Communauté économique européenne ( CEE ) et la Communauté européenne de l' énergie atomique ( CEEA ) , plus connue sous le nom d' Euratom .
La première réunion formelle de la Commission Hallstein eut lieu le 16 janvier 1958 au Château de Val Duchesse .
La CEE permettrait la création d' une union douanière tandis que l' Euratom devait promouvoir la coopération dans l' énergie atomique .
La CEE devint rapidement la plus importante et étendit ses activités .
Un des premiers accomplissements les plus importants de la CEE fut l' établissement de niveaux commun des prix sur les produits agricoles en 1962 .
Ensemble , elles étaient appelées Communautés européennes .
Elles augmentait ainsi l' intégration politique en une Europe unie et pacifiée que Mikhaïl Gorbatchev décrivait comme une maison commune européenne .
Toutefois , le président Charles de Gaulle vit l' entrée du Royaume-Uni comme un cheval de Troie américain et utilisa son droit de veto , et les demandes des quatre pays furent rejetées .
Ces quatre pays firent une nouvelle demande d' adhésion le 11 mai 1967 et , avec Georges Pompidou ayant pris la suite de Charles de Gaulle à la présidence , le veto fut levé .
Les négociations commencèrent en 1970 sous le gouvernement anglais pro-européen d' Edward Heath , qui devait subir des désaccords concernant la politique agricole commune et les relations entre le Royaume-Uni et le Commonwealth of Nations .
Néanmoins , deux ans après que les traités d' adhésion furent signés tous , sauf la Norvège qui avait rejeté l' adhésion par référendum , entrèrent dans la communauté .
Le Conseil européen donna son accord et adopta la Communauté adopta ses symboles en 1984 .
La Grèce déposa sa demande d' adhésion le 12 juin 1975 , après la restauration de la démocratie , et entra dans la communauté le 1 er janvier 1981 .
Suivant la Grèce , et après la restauration de la démocratie , l' Espagne et le Portugal firent leur demande d' adhésion aux communautés en 1977 et y entrèrent ensemble le 1 er janvier 1986 .
En 1987 la Turquie devint officiellement candidate à l' adhésion ce qui marqua le début du plus long processus d' adhésion d' un pays ( en 2009 , la Turquie est encore en négociation avec l' UE ) .
Avec les projets de futurs élargissements , et le désire d' augmenter les domaines de coopération , l' Acte unique européen fut signé par les ministres des affaires étrangères les 17 et 28 février 1986 respectivement à Luxembourg et à La Haye .
Les domaines de compétences de la CEE devinrent le pilier dénommé Communauté européenne , continuant à suivre la structure supranationale de la CEE .
Les institutions de la CEE devinrent celle de l' UE , certaines changeant leur nom en conséquence .
L' entrée en vigueur du Traité de Lisbonne le 1 er décembre 2009 a mis fin à cette structure en pilier .
Le but principal de la CEE , comme le présente son préambule , était " d' établir les fondements d' une union sans cesse plus étroite entre les peuples européens " et " d' assurer par une action commune le progrès économique et social en éliminant les barrières qui divisent l' Europe " .
La Grèce , l' Espagne et le Portugal la rejoignirent durant les années 1980 .
Avant 2004 , les membres les plus étendus ( Allemagne , France , Italie et Royaume-Uni ) avaient deux commissaires .
La plupart des autres institutions , dont la Cour de justice des Communautés européennes , possèdent une certaine forme de division nationale de ses membres .
La CEE disposait de ses propres institutions :
La CEE a hérité de certaines institutions de la CECA parmi lesquelles Assemblée commune et la Cour de justice de la CECA ( dont les autorités avaient été étendues à la CEE et à l' Euratom ) .
Toutefois la CEE , et l' Euratom , possédait des corps exécutifs différent de ceux de la CECA .
Dès lors , le terme de Communautés européennes a été utilisé pour les institutions , , .
La Commission des Communautés européennes était l' organe exécutif de la communauté , rédigeant les lois communautaires , traitant du fonctionnement quotidien de la communauté et soutenant les traités .
Il a été conçu pour être indépendant et pour représenter l' intérêt Communautaire .
Elle est devenue une institution puissante puisque la loi Communautaire prévaut sur les lois nationales .
La cinquième institution est la Cour des comptes européenne .
