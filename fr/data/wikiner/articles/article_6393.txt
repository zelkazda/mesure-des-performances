Fils d' un pasteur de Brandebourg-Prusse , Christian Goldbach fit des études de droit ainsi que de mathématiques à l' université de Königsberg .
En 1725 , il entre à la toute nouvelle Académie des sciences de Russie à Saint-Pétersbourg .
Goldbach voyagea à travers l' Europe , ce qui lui permit de rencontrer des mathématiciens célèbres , comme par exemple Gottfried Leibniz , Leonhard Euler ou Nicolas Bernoulli , avec lesquels il correspondit .
] , ein aggregatum trium numerorum primorum sey " , soit : " Il semble au moins que tout nombre entier plus grand que 2 est somme de trois nombres premiers " ( ce sont les derniers mots écrits verticalement dans la marge ; Goldbach admettait 1 comme nombre premier : " die unitatem mit dazu gerechnet " ) ) .
Outre ses travaux en théorie des nombres , Goldbach s' intéressa également aux séries , aux équations différentielles , à la théorie des courbes et à la fonction gamma .
