Cet article traite de la Flandre belge .
Pour la Flandre française et la Flandre zélandaise , voir les articles dédiés .
Dans le contexte belge , le terme Flandre peut désigner plusieurs choses :
En 1581 , les Provinces-Unies proclament leur indépendance et seuls les Pays-Bas du Sud restent sous domination espagnole .
En 1792 , Pays-Bas autrichiens et principauté de Liège sont envahies par la France , puis reconquis en 1793 par l' Autriche .
La France les reprend en 1794 et les annexe en 1795 .
En 1815 , le territoire actuel de la Flandre est rattaché au royaume des Pays-Bas .
La majorité d' entre eux vivent dans les cinq provinces flamandes : la province d' Anvers ( 1 ) , le Limbourg ( 2 ) , la Flandre-Orientale ( 3 ) , le Brabant-Flamand ( 4 ) et la Flandre-Occidentale ( 5 ) , et une toute petite partie à Bruxelles , où ils sont minoritaires ( environ 10 à 15 % de la population bruxelloise ) .
La Flandre a son propre parlement , son gouvernement et son administration .
Ces institutions exercent à la fois les compétences de la Communauté flamande et de la Région flamande , suite à la fusion de ces deux entités ( juridiquement , c' est la communauté qui a repris les compétences de la région , qui a cessé d' exister sur le terrain ) .
Toutes ces institutions ont leur siège à Bruxelles .
À Bruxelles , la Communauté flamande est compétente dans certaines matières ( tout comme la Communauté française de Belgique ) : enseignement , culture , etc .
Certains pensent que ce terme devrait être utilisé uniquement pour désigner un habitant d' une des deux provinces belges : Flandre-Orientale ou Flandre-Occidentale .
Au total , il y aurait des résidents d' environ 170 nationalités en Flandre .
Toutes ces institutions sont installées à Bruxelles .
La Flandre est renommée pour
Les écrivains flamands sont couramment lus aux Pays-Bas , et vice-versa .
Il existe également un courant poétique très important représenté par Willem Elsschot et Guido Gezelle entre autres .
Une partie de ce que l' on peut qualifier de littérature flamande se trouve être écrite en français , Michel de Ghelderode en est un exemple .
La Flandre a plusieurs chaînes de radio et de télévision , les principaux étant la VRT , chaîne officielle et dotée d' une mission qui comprend de l' information grand-public et objective , non-partisane et de qualité , et VTM , société privée qui regroupe deux chaînes .
