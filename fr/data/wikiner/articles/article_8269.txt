La Haute-Navarre est le nom historique donné à la partie du royaume de Navarre qui fut rattaché au royaume d' Aragon en 1512 puis à la couronne d' Espagne en 1516 .
Elle était constituée des merindades de Pampelune , Tudela , Olite , Sangüesa et Estella .
