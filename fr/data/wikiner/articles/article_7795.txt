Saturn V reste , encore en 2010 , le lanceur spatial le plus imposant qui ait été utilisé en opération , que ce soit du point de vue de la hauteur , de la masse au décollage ou de la masse de la charge utile injectée en orbite .
Seule la fusée russe Energia , qui ne vola que pour deux missions de test , la dépassa légèrement au niveau de la poussée au décollage .
Saturn V , qui a été conçue pour lancer le vaisseau spatial habité Apollo permettant les premiers pas de l' homme sur la Lune , a continué son service en envoyant en orbite la station spatiale Skylab .
En tout , la NASA lança treize fusées Saturn V , sans avoir à déplorer la moindre perte de charge utile .
Les trois étages qui composaient Saturn V ont été développés par de nombreuses entreprises sous-traitantes sous pilotage de la NASA .
Ces sociétés , suite à de multiples fusions et rachats , font aujourd'hui toutes parties du groupe Boeing .
Le 25 mai 1961 , le président John Fitzgerald Kennedy annonça que les États-Unis se donnaient comme objectif d' envoyer un homme sur la Lune avant la fin de la décennie .
Aucune fusée d' un seul étage au monde n' aurait pu envoyer une capsule habitée sur la Lune .
La fusée Saturn I était en développement , mais n' avait encore jamais décollé , et avec sa petite taille , il aurait fallu plusieurs lancements pour placer en orbite tous les composants d' un module lunaire .
Au milieu de l' année 1962 , la NASA décida d' utiliser un plan d' essai " tout en un " , avec les trois étages testés en même temps lors du tout premier vol , ce qui devait raccourcir drastiquement le planning d' essai et de développement , et réduire le nombre de fusées nécessaires pour le programme complet de vingt-cinq à quinze .
Le premier lancement de Saturn V eut lieu le 9 novembre 1967 avec à son bord le vaisseau spatial inhabité Apollo 4 .
Le premier lancement habité eut lieu en décembre 1968 , pour la mission Apollo 8 circumlunaire .
Saturn V est , sans aucun doute , une des machines les plus impressionnantes de l' histoire de l' humanité .
À titre de comparaison , Saturn V a à peu près la même hauteur que la grande Arche de la Défense à Paris .
Les moteurs utilisés par ce lanceur étaient notamment les nouveaux et puissants moteurs F-1 et moteurs J-2 .
L' ensemble des stations sismographiques des États-Unis étaient capables de percevoir les vibrations lors du décollage d' une Saturn V .
Les concepteurs décidèrent très tôt d' utiliser pour Saturn V le maximum des technologies déjà éprouvées pour le programme Saturn 1 .
De la même façon , les instruments de bord qui contrôlaient Saturn V partageaient certaines caractéristiques avec ceux de Saturn 1 .
Les cinq moteurs F-1 étaient disposés en croix .
La case à équipement , fabriquée par la société IBM , était positionnée en haut du troisième étage .
L' équivalent soviétique de Saturn V fut la fusée N1 .
Saturn V était légèrement plus haute , plus lourde , et bien que moins puissante elle avait une plus grande capacité d' emport que la fusée soviétique grâce à l' utilisation d' hydrogène , plus efficace que le kérosène , dans ses étages supérieurs .
La N1 dépassait aussi la fusée américaine pour le diamètre du premier étage .
Quatre tirs d' essai de la N1 furent réalisés .
Le premier étage de la Saturn V utilisait cinq moteurs très puissants tandis que la N1 était équipée d' un assemblage complexe de 30 moteurs plus petits , architecture imposée par le fait que Sergueï Korolev ( son concepteur ) ne disposait pas à cette époque de moteurs de forte puissance et qu' il se refusait à utiliser ceux que lui proposait son adversaire Valentin Glouchko , plus puissants mais utilisant des ergols hypergoliques toxiques .
Au cours des vols Apollo 6 et Apollo 13 , Saturn V fut capable de corriger sa trajectoire de vol malgré des incidents de perte de fonctionnement moteur .
Au contraire , même si la N1 disposait également d' un système informatique conçu pour corriger les défauts de fonctionnement des moteurs , ce dernier manquait de fiabilité et ne parvint jamais à sauver un lancement de l' échec , étant même à une occasion à l' origine de l' échec en éteignant de manière impromptue tous les moteurs du premier étage , détruisant le lanceur et le pas de tir par la même occasion .
Aucun autre lanceur spatial en opération n' a surpassé Saturn V en hauteur , en poids , ou en charge utile .
Hormis la fusée N1 dont les quatre lancements furent des échecs , il n' y a que pour la poussée au décollage que Saturn V a été égalée par une autre fusée , si on compte les deux vols d' essais d' Energia comme opérationnels .
La NASA envisageait aussi de nouveaux membres encore plus performants à la famille Saturn , y compris le lanceur Nova , mais ils ne furent jamais produits .
Autre comparaison , Ariane 5 ECA peut envoyer environ 10 tonnes en orbite GTO , et 20 tonnes en LEO .
La fusée américaine Delta 4 Heavy envoie 13.1 tonnes en orbite de transfert géosynchrone .
Après qu' un étage ait été fini , il était transporté par bateau jusqu' au centre spatial Kennedy .
La NASA construisit aussi de larges structures cylindriques qui pouvaient être mises à la place des étages si l' un d' entre eux était retardé .
Tous les lancements eurent lieu depuis le complexe de lancement 39 au centre Spatial John F. Kennedy .
Bien qu' Apollo 6 et Apollo 13 connurent des pannes moteurs , les ordinateurs de bord furent capables de compenser en laissant fonctionner les moteurs restants plus longtemps , et aucun des lancements Apollo ne se termina par une perte de la charge utile .
Du lancement jusqu' à la seconde 38 après l' allumage du second étage , Saturn V utilisait un programme préenregistré pour la consigne de l' angle d' assiette .
Saturn V accélérait rapidement , atteignant la vitesse de 500 m/s à 2 km d' altitude .
Il n' y avait pas de moyens plus simples d' arriver à ce résultat , étant donné que la poussée du moteur F-1 n' était pas contrôlable .
Puis le premier étage retombait dans l' océan Atlantique à environ 560 km du pas de tir .
Le second étage suivait une procédure d' allumage en deux temps , qui a varié pendant les différents lancements de Saturn V .
Pour les quatre derniers lancements de Saturn , les quatre moteurs inutilisés furent même retirés .
Environ 38 secondes après l' allumage du second étage , le système de guidage de Saturn V passait d' une consigne préenregistrée pour l' assiette de vol à un système de guidage en boucle , contrôlé par les instruments de la case à équipement , tels qu' accéléromètres et instrument de mesure de l' altitude .
Un système d' élimination de l' effet pogo fut mis en place à partir d' Apollo 14 , mais le moteur central était toujours éteint en avance .
Une fois que deux d' entre eux étaient découverts , les systèmes de contrôle de Saturn V initiaient la séquence de changement d' étage .
10 minutes et 30 secondes après le décollage , Saturn V était à 164 km d' altitude et à 1700 km de distance au sol du site de lancement .
Pour les deux missions de mise en orbite terrestre , Apollo 9 et Skylab , l' orbite d' injection était plus élevée .
À partir d' Apollo 13 , les contrôleurs dirigeaient le troisième étage vers la Lune .
Des sismographes déposés sur la Lune par de précédentes missions détectaient les impacts , et les données enregistrées ont contribué à étudier la composition intérieure de la Lune .
Avant Apollo 13 ( sauf Apollo 9 et Apollo 12 ) , les troisièmes étages étaient placés sur une trajectoire passant à proximité de la Lune qui les renvoyaient vers une orbite solaire .
Apollo 9 quant à lui fut dirigé directement vers une orbite solaire .
Le 3 septembre 2002 , Bill Yeung découvrit un astéroïde suspect à qui il donna le nom provisoire de J002E3 .
En 1968 , le programme d' application Apollo fut créé afin d' étudier les missions scientifiques qui pouvaient être réalisés avec le surplus d' équipements du programme Apollo .
La plus grande partie des réflexions tournait autour de l' idée d' une station spatiale , qui donna finalement naissance au programme Skylab .
Un système de rechange ( parfois nommé Skylab B ) , qui fut construit à partir d' un troisième étage de Saturn V , est aujourd'hui exposé au National Air and Space Museum .
Trois équipages ont occupé Skylab du 25 mai 1973 jusqu' au 8 février 1974 .
Skylab resta en orbite jusqu' en mai 1979 .
La navette aurait pu remonter l' orbite de Skylab , et lui permettre d' être utilisée comme une base pour de futures stations spatiales .
Cependant , la navette ne vola pas avant 1981 et , rétrospectivement , on a pris conscience que Skylab n' aurait de toute façon pas été d' une grande utilité , n' étant pas conçue pour être réapprovisionnée ou ravitaillée .
La navette devait s' occuper de la logistique de la station spatiale , tandis que Saturn V devait s' occuper du lancement des différents composants .
L' absence de la deuxième série de production de Saturn V ruina ce plan et laissa les États-Unis sans lanceur super-lourd .
Certains au sein de la communauté spatiale américaine ont fortement regretté cette situation , sachant que la poursuite de la production aurait permis la réalisation de la Station spatiale internationale , en configuration Skylab ou Mir avec les ports d' ancrages russes et américains , avec une poignée seulement de lancements .
Certains considèrent également que le concept de " navette Saturn " aurait permis d' éviter les conditions qui ont amené au désastre de Challenger en 1986 .
Aux États-Unis , les propositions pour une fusée plus grande que Saturn V étudiées de la fin des années 1950 jusqu' au début des années 1980 ont toutes porté le nom général de Nova .
Wernher von Braun et d' autres avaient aussi des plans pour une fusée qui aurait eu huit moteur F-1 sur son premier étage lui permettant d' envoyer un vaisseau spatial habité directement vers la Lune .
Ces améliorations auraient augmenté sa capacité à envoyer de grands vaisseaux inhabités explorer les autres planètes ou des vaisseaux habités vers Mars .
Ce nouveau lanceur a été baptisé en l' honneur de Saturn V .
Il est destiné à être un véhicule inhabité , à forte capacité de lancement , prévu pour les futures missions habitées vers la Lune et éventuellement plus tard vers Mars .
De 1964 à 1973 , un total de 6,5 milliards de dollars a été dépensé pour Saturn V .
Une des principales raisons à l' arrêt du programme Apollo a été son coût .
La même année , le Département de la Défense des États-Unis recevait 63,5 milliards de dollars .
En 2009 , trois Saturn V sont exposées aux États-Unis , toutes à l' horizontale :
Sur ces trois Saturn V , seule celle du centre spatial Johnson est composée entièrement d' étages prévus pour un lancement réel .
Le centre américain de l' espace et des fusées à Huntsville dispose également en exposition d' une réplique à l' échelle de Saturn V érigée à la verticale .
Une rumeur répandue depuis 1996 , prétend que la NASA a perdu ou détruit les plans de Saturn V .
