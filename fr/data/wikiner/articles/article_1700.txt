Au niveau français , elle est exécutée principalement via deux offices agricoles sous tutelle du Ministère de l' Agriculture .
La politique agricole commune ( PAC ) est , avec la CECA instituée en 1951 , une des plus anciennes et jusqu' à peu la plus importante des politiques communes de l' UE en matière budgétaire , mais actuellement en baisse .
Créée par le traité de Rome en 1957 , elle a été mise en place en 1962 .
Le traité de Rome , signé le 25 mars 1957 par les membres fondateurs de la Communauté économique européenne ( CEE ) , a donné à la PAC une orientation résolument productiviste , car il fallait augmenter la production agricole , et protectionniste , car la construction d' une union douanière nécessitait une protection aux frontières .
Il s' agissait alors de rendre la Communauté auto-suffisante , plus solidaire et de moderniser un secteur agricole encore très disparate selon les pays .
Or le traité de Rome instituait le principe de libre circulation des marchandises , qui était par définition incompatible avec une politique au niveau national , car cela aurait faussé le jeu normal de la concurrence .
Or l' agriculture européenne avait besoin de développement car à l' exception de la France , les pays fondateurs n' assuraient pas , à des degrés divers , leur autosuffisance ( assurée à 80 % seulement en moyenne ) pour la plupart des grands produits alimentaires , et dépendaient donc du marché mondial .
La Conférence de Stresa qui s' est tenue du 3 au 11 juillet 1958 ( à ne pas confondre avec la conférence de Stresa du 14 avril 1935 ) , a défini les grands principes de la PAC :
Bref , le cadre légal dans lequel des denrées agricoles peuvent être produites , introduites , ou mises en vente en Europe .
Certains produits agricoles de l' Union Européenne , comme la pomme de terre , ne font pas l' objet d' une organisation commune des marchés .
La PAC compte pour environ 43 % du budget total de l' UE ( 129,1 milliards d' euros ) , soit 55,5 milliards .
En 2008 , La France contribue pour 16,95 % du total de 129,1 milliards d' euros ( budget européen ) , soit 21,9 milliards .
La France reçoit plus , en proportion , de paiements relatifs à la PAC ( environ 20,3 % du budget de la PAC ) , soit pour 11,2 milliards ( 9,8 milliards d' aides du premier pilier et 1,4 milliard d' aides du deuxième pilier ) , mais moins dans les autres postes budgétaires .
La France est donc bénéficiaire net de la PAC , le plus gros contributeur étant l' Allemagne .
Cependant , en France , cette subvention est plus élevée dans les zones à haut potentiel de production ( environ 420 euros par hectare ) , et plus basse dans les zones à faible potentiel ( environ 260 euros ) , pour des raisons de références historiques .
La France utilise cette option qui ne modifie pas le budget de la PAC , par des dispositions complexes qui transfèrent environ 1 milliard des céréaliers vers les éleveurs .
On peut aussi comparer le budget du premier pilier ( 9,7 milliards pour la France ) à la valeur ajoutée de l' agriculture ( 35,0 milliards ) soit 27,7 % de subventionnement en 2008 .
En effet elles sont là pour compenser la baisse partielle des prix garantis de l' UE en 1992 .
Depuis avril 2009 , on peut connaître les montants reçus au titre du premier pilier pour tous les bénéficiaires de la PAC en France .
Cette dénonciation est toutefois en baisse depuis le découplage des aides de 2003 ( appliqué en 2006 en France ) .
Certains pays de l' UE ont de plus grands secteurs agricoles que les autres , notamment la France , l' Espagne , et le Portugal , et reçoivent par conséquent plus d' argent de la PAC .
Les plus importants bénéficiaires par tête sont la Grèce et l' Irlande .
La rabais britannique a été négocié en 1984 par Margaret Thatcher .
Le subventionnement à l' export est considéré comme la plus grande source de distorsions du commerce international , et l' UE s' est engagée unilatéralement à supprimer les restitutions .
Un accord à l' OMC , s' il était conclu , comprendrait certainement des limitations des boîtes bleue et ambre , ainsi que des réductions ciblées de droits de douanes sur certains produits agricoles .
Il faut noter que les études prospectives n' indiquent pas forcément que l' agriculture de l' UE serait perdante si un accord était conclu .
En France , il semble que les syndicats agricoles soutiennent le modèle actuel de la PAC avec quelques variantes .
En novembre 2009 , un groupe d' économistes de toute l' Europe a publié une déclaration sur l' avenir de la PAC .
Ils estiment qu' une stimulation de la production afin d´assurer la sécurité alimentaire n' est pas nécessaire étant donné que le potentiel productif de l' UE est suffisant .
De plus , ils constatent que le soutien des revenus agricoles ne constitue pas une politique sociale ciblée ni une tâche adéquate pour l' UE .
Les agriculteurs ne devraient , selon eux , recevoir des subsides de l' UE que quand ils protègent le climat , préservent la biodiversité ou rendent un service similaire à la société .
