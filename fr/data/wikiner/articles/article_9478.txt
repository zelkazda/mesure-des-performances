L' Apocalypse est le dernier livre de la Bible chrétienne .
Mais aussi des passages entiers du Nouveau Testament , en dehors de l' Apocalypse canonique elle-même , peuvent être dits du genre apocalyptique .
Plusieurs autres textes de la Bible parlent de la fin des temps :
L' admission de l' Apocalypse dans le canon des livres reçus fut assez difficile .
L' évêque Denis d' Alexandrie contestait son authenticité johannique pour des raisons stylistiques .
Ce qui n' empêchait pas saint Athanase , évêque d' Alexandrie , de la reconnaître pleinement dans sa 39 e lettre pascale ( 367 ) .
La théologie orthodoxe a pris parti dans ce débat lorsque les moines orthodoxes de Patmos ont solennellement fêté le dix-neuvième centenaire de la rédaction de l' Apocalypse en 1995 .
5 , 6-14 ) leur soient communs " , il est bien difficile , pour les critiques contemporains , d' admettre la possibilité que l' auteur soit Jean l' évangéliste .
Ce plan a été proposé par Alfred Läpple dans son ouvrage L' Apocalypse de Jean , d' après les " mots-charnières " qui jalonnent le texte .
L' Apocalypse fut écrite à l' apogée du principat de Néron ( cf .
L' Apocalypse n' évoque pas la fin du monde seulement , mais dans un langage symbolique toute l' histoire humaine : le présent : 1,9 à 3,22 ; le passé ( éloigné ou immédiat ) : 4,1 à 13,18 ; le futur ( proche ou lointain ) : 14,1 à 22,5 .
Cette approche suppose donc que l' Apocalypse comportait un message à destination de ses contemporains , tout en revêtant un caractère prophétique concernant les " temps de la fin " .
La critique interne de l' Apocalypse conduirait très certainement à une datation de l' Apocalypse du temps de Néron , et plus précisément du temps de la persécution de Néron ( vers 66-67 ) .
Cette manière de voir éclaire l' Apocalypse .
L' Apocalypse ne prétend pas décrire seulement la fin du monde , et les catastrophes qui la précèderont , mais bien toute l' histoire humaine avec ses péripéties : le présent , cf .
La parousie , et la Jérusalem d' en haut , seront seulement présentées en finale : cf .
En aucun cas l' Apocalypse ne fut composée pendant la persécution de Domitien , dont on n' est même pas sûr qu' elle ait existé .
La théorie documentaire qui fait de l' Apocalypse un patchwork composé de morceaux rapportés , de différentes époques ou de différents auteurs , est vigoureusement réfutée , d' une part par l' unité de style de ce petit ouvrage , et d' autre part par la rigueur du plan septénaire qu' on peut y découvrir et qui l' organise comme un tout .
Elle n' aurait pas de sens si elle n' eût été expédiée , dans son intégralité , avant la chute et le suicide de Néron ( chute et suicide auxquels elle ne fait aucune allusion ) .
L' Apocalypse et le quatrième évangile , bien que du même auteur , furent rédigés dans des circonstances et à des époques très différentes .
L' Apocalypse est plus ancienne que l' évangile de Jean , et son style nettement plus fruste , et d' autre part truffé de réminiscences vétérotestamentaires .
Le roi Salomon y reçoit en une année 666 talents d' or .
Le kérygme caché de l' Apocalypse se laissait finalement facilement déchiffrer , selon d' ailleurs que Jean nous y invitait lui-même à plusieurs reprises .
1° ) par cette annonce que l' auteur , Jean , devait encore , avant que ne survînt la fin , proclamer l' évangile par le monde entier , et même rédiger un petit livre , en sept chapitres , contenant l' évangile .
Elle évoquait , pour Jean , le moment présent , celui même où il notait sa prophétie .
Désormais Jean allait évoquer " ce qui doit arriver plus tard " .
Dans l' épilogue , Jean recommandait à son lecteur de conserver fidèlement , et même scrupuleusement , les paroles de la prophétie , dans leur lettre comme dans leur esprit .
