Aristote pose le principe de contradiction comme une nécessité absolue .
Héraclite ( vers 500 av .
Le principe de non-contradiction se trouve de façon implicite chez Parménide ( vers 450 av .
La première formulation nette se rencontre chez Platon :
Aristote reprend la pensée de Platon en amplifiant son importance : Aristote donne à la démonstration deux principes : le principe de non-contradiction , le principe du tiers exclu .
Épicure , pour sauver l' indétermination des propositions se rapportant aux événements futurs , niait le principe de non-contradiction .
Leibniz :
Dès le départ , Nietzsche pense " la contradiction logée au coeur du monde " .
Louis de Broglie , après la découverte de la diffraction des électrons par les cristaux , a montré qu' il faut associer l' aspect corpusculaire à l' aspect ondulatoire , tant pour la matière que pour le rayonnement .
Très audacieux , Stéphane Lupasco tire des conséquences :
