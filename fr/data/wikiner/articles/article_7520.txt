Frère du roi Conrad le Pacifique et de l' impératrice Adélaïde de Bourgogne , épouse de l' empereur Otton Ier du Saint-Empire .
Archevêque de Lyon ( 948 -- 963 ) , il restaura le monastère de Savigny et lui donna une règle .
