En Europe , la maîtrise en théologie ( bac+4 ) est exigée partout .
Dans l' Église réformée de France , le pasteur doit , après son master pro , faire encore deux ans de proposanat ( stage ) en paroisse pour enfin être agréé ( ou pas ) par le commission des ministères et reconnu comme " ministre " .
Le luthéranisme maintient une ordination ; l' Église réformée de France célèbre une reconnaissance de ministère .
