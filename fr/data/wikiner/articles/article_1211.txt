L' Insee et la Poste lui attribuent le code 93 .
Il hérite du code postal " 93 " , autrefois dévolu au département de Constantine .
Sa préfecture est Bobigny .
La Seine-Saint-Denis était , quelque part , comme la concession tacite d' un territoire donné à la principale force politique d' opposition de l' époque .
Par voie de conséquence , elle permettait d' envisager que les deux autres départements constitués dans la proche banlieue ( Hauts-de-Seine et Val-de-Marne ) échapperaient à l' influence de cette même force d' opposition , tout en bénéficiant des programmes les plus déterminants d' aménagement urbain .
Cependant , comme pour les deux autres départements de la petite couronne , le subtil découpage territorial était organisé de telle sorte que ce qui avait constitué pour l' essentiel la ceinture rouge depuis l' époque du Front populaire ( les anciennes communes du département de la Seine ) soit " tempéré " par adjonction de communes issues de la Seine-et-Oise , réputées a priori moins portées à choisir des élus de gauche , et surtout communistes , pour les représenter .
La Seine-Saint-Denis est située au nord-est de Paris .
La Seine-Saint-Denis forme avec les deux autres petits départements limitrophes de Paris ( les Hauts-de-Seine et le Val-de-Marne ) la petite couronne de la région parisienne .
Elle est située dans le Bassin parisien et comprend une part importante du Pays de France .
Ces derniers sont séparés par les dépressions de Gagny et de Rosny-sous-Bois , qui correspondent à un ancien lit de la Marne .
Le département est limité au nord-ouest par la Seine , à la hauteur de la boucle de Gennevilliers , et , au sud-est , par son affluent , la Marne .
La géologie du département est celle , sédimentaire , du Bassin parisien .
Une succession d' horizons argilo-marneux s' intercalent dans des formations marno-calcaires ou calcaires , souvent gypseuses , qui ont longtemps été exploitées dans des carrières de pierre à plâtre à ciel ouvert ou en galerie ( à Romainville , Rosny-sous-Bois ou Gagny , par exemple ) .
Le département comme toute l' Île-de-France est soumis à un climat océanique dégradé .
Le département est également desservi par les anciennes routes nationales RN1 , RN2 et RN3 .
Le département de la Seine-Saint-Denis est traversé par d' importants faisceaux ferroviaires , avec notamment les lignes Paris -- Lille , La Plaine -- Hirson , Paris -- Strasbourg et Paris -- Mulhouse , ainsi que par la Ligne de Grande Ceinture .
Sauf cette dernière , dévolue au trafic de marchandises , les autres lignes accueillent des services TGV , grandes lignes , Transilien et RER .
Le département accueille la première ligne de tramway moderne en Île-de-France , le , ouvert en 1992 , et plusieurs projets sont actuellement en cours d' étude .
Le département est traversé par cinq voies d' eau : le canal de l' Ourcq , le canal Saint-Denis , le canal de Chelles , la Seine et la Marne .
Il n' existe toutefois pas de port important en Seine-Saint-Denis , le port de Gennevilliers étant situé dans les Hauts-de-Seine , sur la rive opposée d' Épinay sur Seine .
Le département dispose du système aéroportuaire parisien géré par aéroports de Paris , et accueille l' aéroport du Bourget , réservé à l' aviation d' affaire , ainsi qu' une partie de l' aéroport Paris-Charles-de-Gaulle .
Parmi les 40 communes , les plus peuplées sont Montreuil ( 102 097 habitants ) , Saint-Denis ( 100 800 habitants ) , Aulnay-sous-Bois ( 82 513 habitants ) , Aubervilliers ( 73 699 habitants ) , Drancy ( 65 843 habitants ) et Noisy-le-Grand ( 62 529 habitants ) .
La Seine-Saint-Denis est l' un des départements français comptant le plus d' immigrés , ou de personnes issues de l' immigration .
Pour comparaison en Ile de France elle est de 4,2 .
En effet , dès le développement industriel de la seconde moitié du XIX e siècle , les communes de l' actuelle Seine-Saint-Denis accueillent des populations de travailleurs issus de l' immigration .
Les données du recensement général de la population 1999 étaient les suivantes : le département comptait un peu moins de 1383000 habitants , dont près de 260000 étaient de nationalité étrangère ( dont plus de 40000 nés en France ) et plus de 126000 des Français par acquisition , c' est-à-dire ayant été naturalisés .
Les données du recensement de 2007 indiquent un accroissement du nombre et de la proportion d' étrangers ( 317000 personnes , soit 21,10 % de la population du département ) et du nombre de Français par acquisition ( 181200 ) .
Les immigrés sont au nombre de 402000 et représentent plus du quart de la population de la Seine-Saint-Denis ( 26,75 % exactement en 2007 ) , proportion la plus élevée de tous les départements métropolitains .
Globalement , c' est au nord-ouest de la Seine-Saint-Denis que les plus grandes concentrations sont observées , alors que les proportions d' étrangers sont plus faibles dans l' est .
Les communes où , selon les chiffres du recensement de 2007 , la part des immigrés dans la population est la plus élevée ( supérieure au tiers ) sont Aubervilliers ( 40,20 % ) , Clichy-sous-Bois ( 37,55 % ) , La Courneuve ( 37,25 % ) , Saint-Denis ( 36,81 % ) , Saint-Ouen ( 34,15 % ) , Villetaneuse ( 33,63 % ) et Bobigny ( 33,35 % ) .
Les communes où la proportion d' immigrés est la plus faible sont Coubron ( 5,49 % ) , Gournay-sur-Marne ( 9,28 % ) , Le Raincy ( 10,43 % ) et Vaujours ( 11,96 % ) , la moyenne étant de 8,33 % en France métropolitaine en 2007 .
Le revenu moyen des ménages s' élève selon l' INSEE en 2004 à 15 175 € /an , alors que la moyenne nationale s' élève à 15 027 € /an .
Cependant il existe des disparités criantes de niveaux de vie en Seine-Saint-Denis selon les villes habitées .
Ainsi l' arrondissement de Saint-Denis est le plus défavorisé , la quasi totalité des villes s' y trouvant cumulant des obstacles en matière sociale ( chômage , taux de logements sociaux très élevés , revenus des ménages faibles ... ) .
Ainsi , en 2006 , alors que le taux de chômage était de 11,1 % en moyenne pour les communes françaises , l' arrondissement de Saint-Denis affichait 20,3 % , l' arrondissement de Bobigny 16,7 % et celui du Raincy 13,7 % .
De fait , la ville de Clichy-sous-Bois , où le revenu moyen net imposable des ménages était de 15 090 € en 2007 , est limitrophe de la ville du Raincy alors que ces communes sont particulièrement opposées ; les ménages du Raincy avaient , avec 38 156 € en moyenne en 2007 , des revenus compris entre ceux de Levallois-Perret et de Saint-Maur-des-Fossés .
Ce parc est principalement regroupé dans les communes limitrophes de Paris .
Les principaux sont l' OPH départemental ( 20000 logements ) et l' OPH Plaine commune habitat
D' autre part , le logement privé ancien est parfois fort dégradé : 140 arrêtés d' insalubrité ont pris en 2007 ; pour les seules six villes de Romainville , Saint-Denis , Aubervilliers , Saint-Ouen , Pantin et Montreuil , 1 255 logements sont considérés comme à démolir et 2 921 à réhabiliter .
En 1999 , la seule Plaine commune recensait 6700 logements indécents dont 2500 insalubres .
Plaine Commune a ainsi signé un contrat territorial de renouvellement urbain qui concerne le tiers de ses logements , qu' il s' agisse d' HLM dégradées , de copropriétés en difficulté où d ' " habitat indigne " [ réf. nécessaire ] .
Suite à l' appel de l' Abbé Pierre en 1954 et la relance du logement , ceux-ci ont été globalement éradiqués vers 1974 .
Le fil directeur de ce document stratégique d' aménagement est de faire de l' Ile de France une éco région à l' horizon 2030 .
En 2008 , 683 740 tonnes de déchets ménagers et assimilés ont été collectées en Seine-Saint-Denis , soit 481 kg/hab .
La Seine-Saint-Denis fait partie , avec le Val-de-Marne et la Seine-et-Marne de l' Académie de Créteil .
La population de la Seine-Saint-Denis est de plus en plus formée , prolongeant de plus en plus ses études , ce qui constitue l' un des potentiels de développement futur du département , et du pays en général .
D' un point de vue sanitaire , neuf centres hospitaliers publics sont implantés en Seine-Saint-Denis :
Le département accueille depuis 1998 le Stade de France à la Plaine Saint-Denis à Saint-Denis .
Elle abrite également plusieurs clubs de football au niveau amateur comme le Red Star , la Jeanne d' Arc de Drancy Noisy-le-Sec et Villemonble .
Le club de Villepinte évolue en deuxième division jeunes .
Le quotidien Le Parisien -- Aujourd'hui en France , dont le siège est d' ailleurs situé à Saint-Ouen , a parmi ses éditions départementales une édition de 8 pages consacrée à la Seine-Saint-Denis qui est le principal média consacré à la vie du département .
En l' absence d' un média comparable à la presse de province , les 8 pages de l' édition départementale du Parisien ne permettent pas de donner écho à l' information de proximité .
Depuis mars 2006 , la direction a été confiée à une équipe locale et le blog constitue désormais un média très lu , représentant la diversité ethnique et sociologique de la Seine-Saint-Denis , couvrant l' actualité du département .
Le quotidien national L' Humanité a son siège à Saint-Denis .
La Seine-Saint-Denis dispose d' un important réseau de salles de spectacles accueillant notamment des représentations théâtrales .
Un projet est en cours à Aulnay-sous-Bois .
La Seine-Saint-Denis est l' un des départements constituant la Ceinture Rouge historique de Paris .
Après y avoir disposé de la majorité absolue de 1967 à 2001 , le PCF a détenu la présidence du Conseil général de la Seine-Saint-Denis jusqu' en mars 2008 , avec des personnalités comme Georges Valbon .
La Seine-Saint-Denis vote très majoritairement à gauche depuis fort longtemps , avec , cependant , quelques différences de comportements électoraux entre les communes issues de l' ancien département de la Seine et celles issues de l' ancienne Seine-et-Oise .
La droite dispose de quelques bastions anciens dans l' est du département comme Le Raincy ou Montfermeil .
Le PS a longtemps dû se contenter de quelques bastions comme Bondy , Le Pré-Saint-Gervais et Épinay-sur-Seine , ville qui vit François Mitterrand prendre la tête du parti en 1971 .
En 1968 , année où le parti gaulliste UNR avait obtenu la majorité absolue à l' Assemblée Nationale , il n' avait pu obtenir que 2 des 9 sièges de député du département , les 7 autres restant acquis au PCF .
En 1977 , la stratégie d' union de la gauche lui permet de conquérir six municipalités , tandis que Sevran passait du PS au PCF .
La situation au sein de la gauche a également évolué , avec un renforcement de l' influence du Parti Socialiste , prenant notamment appui sur les divisions et divergences d' approche des élus et organisations communistes dans le département , comme sur son évolution sociologique et l' aggravation de la situation sociale de nombreuses cités .
Un grand nombre d' hommes politiques d' envergure nationale sont des élus de Seine Saint-Denis .
Notons toutefois qu' Alain Calmat , comme Élisabeth Guigou et Dominique Voynet ont exercé des responsabilités ministérielles avant d' être élus en Seine-Saint-Denis .
La préfecture de la Seine-Saint-Denis se situe à Bobigny , chef-lieu du département .
Le département comprend deux sous-préfectures , l' une à Saint-Denis , l' autre au Raincy .
Le tableau suivant donne la liste des communes de la Seine-Saint-Denis , en précisant leur code INSEE , leur code postal principal , leur appartenance aux principales structures intercommunales en 2009 , leur superficie et leur population .
Le département de la Seine-Saint-Denis a un taux de criminalité certes élevé , 95,67 crimes et délits pour 1000 , mais inférieure à celui de Paris qui est de 124,7 .
Cependant les taux pour les départements voisins tels que les Hauts-de-Seine , le Val-de-Marne et le Val-d'Oise sont respectivement de 70,9‰ , de 78,7‰ et de 88‰ .
En passant par celle de Bobigny et celle d' Aulnay-sous-Bois ayant un taux d' environ 105 pour 1000 , puis celles de Drancy et de Livry-Gargan avec un taux moyen de 84,5‰ .
À Paris , les cas de violences sexuelles sont au nombre de 1413 en 2008 soit 0,6‰ , la ville se place en 2nde position du plus fort taux .
En Seine-Saint-Denis , les cas sont de 730 soit 0,5‰ et se place en 16ème position .
De la même manière , en 2008 , pour les atteintes aux biens ( vols sans violence , dégradations et destructions ) la Seine-Saint-Denis est le troisième département le plus touché , après les Bouches-du-Rhône et Paris .
Cependant Paris étant l' une si ce n' est la ville la plus touristique du monde , ses taux sont à nuancer .
On peut en dire de même de Saint-Ouen qui voit sa population multiplier par 4 lors du marché aux puces .
Entre 2007 et 2008 , on distingue en Seine-Saint-Denis une diminution des cas de violences physiques et des menaces , alors qu' à Paris sur la même période on aperçoit une hausse des cas .
