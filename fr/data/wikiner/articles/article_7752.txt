Le terme est probablement apparu aux États-Unis dans les années 1930 .
L' heroic fantasy est à l' origine le nom donné à la fantasy , dans le sens d' oeuvres centrées sur des aventures héroïques dans des mondes imaginaires au contexte fortement médiéval ou proto-médiéval ( Thorgal ) .
Le véritable créateur de l' heroic fantasy , contrairement à ce qu' on pense , n' est pas Robert E. Howard mais Edgar Rice Burroughs ( bien que celui qui ait perfectionné l' art soit Tolkien , avec Le Seigneur des anneaux , Le Silmarillion ainsi que Les contes et légendes du 1 er , 2 e et 3 e Age ) .
Son Tarzan ( créé en 1912 ) fait d' abord office de personnage précurseur .
Mais au fur et à mesure , Tarzan sort du cadre purement aventureux pour rentrer dans la fantasy .
Le deuxième grand auteur d' heroic fantasy est évidemment Robert E. Howard .
Dans Légende , l' intrigue bien que centrée sur Druss et Regnak , s' articule autour d' une dizaine de personnages secondaires tous intéressants à leur manière .
Dans Excalibur , le héros central est en fait l' épée elle même .
Chez Gemmell , les créatures sont très proches de l' humanoïde et souvent peu développées .
Seule Megan Lindholm finit par introduire les dragons .
La situation s' inverse , alors que c' est d' habitude Conan qui séduit de nombreuses femmes sans défense , ici , ce sont parfois les femmes qui défendent les hommes et qui choisissent leurs nombreux amants ou amantes .
