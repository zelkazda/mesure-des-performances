Le philosophe Vladimir Jankélévitch déterminait ainsi , à la suite de Bergson , les fondamentaux d' une spiritualité humaine .
La spiritualité constitue , selon Daniel S. Larangé , un discours social composé de représentations partagées par une communauté d' esprit .
Dans les pays occidentaux , lors de l' apparition des premières psychothérapies ( Freud , Jung ) , quelques psychanalystes en vinrent à conclure que certaines pathologies pouvaient ne pas trouver de résolution par l' analyse seule .
Certains psychanalystes , dont Jung , se tournèrent vers l' étude de pratiques issues de certaines religions traditionnelles afin de " guérir l' âme " .
