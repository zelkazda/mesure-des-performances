Les cantons de Seine-et-Marne ont une population moyenne de 27 762 habitants en 1999 .
Il n' y a pas d' homonymie pour l' arrondissement de Torcy et le canton de Torcy , mais il y a une homonymie pour la commune chef-lieu .
Il n' y a pas d' homonymie pour les cantons de Chelles , de Coulommiers et de Perthes , mais chacune des communes chefs-lieux a un ou plusieurs homonymes exacts ou partiels .
