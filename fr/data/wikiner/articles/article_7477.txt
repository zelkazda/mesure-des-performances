Depuis 1927 , le nom officiel de la Grande-Bretagne est " Royaume-Uni de Grande-Bretagne et d' Irlande du Nord " .
La conquête de l' Irlande commence en 1169 , sous Henri II .
Par la suite , les Normands prennent pied en Irlande , et s' emparent de Dublin en 1170 .
Il parvient à contrôler Dublin et ses alentours .
La reconquête de l' Irlande par les Tudors voit se dérouler des épisodes particulièrement violents , comme les deux révoltes des comtes de Desmond ( 1569 -- 1573 et 1579 -- 1583 ) et la guerre de Neuf Ans .
L' Écosse , jusqu' en 1707 , reste un royaume indépendant et résiste à l' expansion de la domination anglaise .
À cause du climat , de la géographie physique et de la densité de la population , le royaume d' Écosse a , d' un point de vue économique et militaire , tendance à être considéré comme inférieur à son voisin au sud , le royaume d' Angleterre .
La Réforme écossaise provoque un conflit entre la " vieille " religion ( le catholicisme romain ) et la nouvelle ( l' Église écossaise ou presbytérienne . )
Les Stuart sont maintenant la famille royale de " Grande-Bretagne " , mais les deux royaumes conservent des parlements séparés .
Le nouveau régime est impopulaire , et la mort de Cromwell en 1658 laisse la place à un vide politique que le gouvernement de son fils Richard ( 1658- 1659 ) ne parvient pas à combler .
Néanmoins , pendant la période des Guerres des Trois Royaumes , les rois anglais 1639 -- 1651 ) ont consacré beaucoup de temps à établir la primauté de l' Angleterre sur les deux autres royaumes de la monarchie Stuart .
Une intégration politique plus profonde est le projet de la reine Anne de Grande-Bretagne ( 1702 -- 1714 ) qui succède au trône en 1702 comme dernière monarque Stuart de la Grande-Bretagne et d' Irlande .
Les circonstances de l' acceptation de l' Écosse sont discutées .
En 1707 les actes d' union reçoivent la sanction royale , les parlements d' Angleterre et d' Écosse sont abolis pour créer le royaume de Grande-Bretagne avec un parlement unique .
Le plus grand bénéfice de cette union est pour l' Écosse le fait de profiter du libre-échange avec l' Angleterre et son empire colonial d' outre-mer .
Pour l' Angleterre , un allié possible des états européens hostiles à l' Angleterre est neutralisé , ainsi que la succession protestante au trône sécurisée .
Certaines institutions anglaises et écossaises ne sont pas fusionnées dans le système britannique : l' Écosse conserve son système judiciaire de même que son système banquier .
Le siècle est marqué par une rivalité avec la France , qui se prolonge à leurs colonies outre-mer en Amérique du Nord , aux Antilles , et en Inde .
Dans la guerre de Sept Ans ( 1756 -- 1763 ) , la Grande-Bretagne triomphe décisivement sur ces trois fronts , conquérant la Nouvelle-France ( le Canada ) ainsi que des iles dans les Antilles mais aussi et surtout , gagne de manière décisive le contrôle du sous continent indien .
Cependant , le triomphe britannique en Amérique du Nord est de courte durée .
Avec l' assistance de la France motivée par l' envie de revanche , les États-Unis gagnent leur indépendance en 1783 .
Sur le continent européen , la Grande-Bretagne soutient uniformément le statu quo .
Le commerce extérieur de la Grande-Bretagne connaît au XVIII e siècle une croissance exceptionnelle .
La valeur des exportations de biens produits en Grande-Bretagne passe de 4,5 millions de livres à 18,3 millions .
Une autre cause de la croissance phénoménale du commerce extérieur britannique est le développement d' un marché du crédit particulièrement performant , soutenu par les principaux grossistes et marchands internationaux , et par des institutions comme la Bank of England , et qui permet de pallier la pénurie constante de numéraire .
La croissance du coton décolle dès 1771 et accélère encore en 1787 , au fil des progrès technologiques : sur seulement 37 ans , entre 1771 et 1808 , les importations de coton brut de l' Angleterre sont multipliées par douze .
L' invasion de l' Irlande par les Anglo-Normands en 1170 est suivie par des siècles de lutte .
Après sa défaite , l' Irlande est soumise à l' autorité et aux lois de l' Angleterre et les terres du nord du pays sont confisquées et attribuées à des colons venus d' Écosse et d' Angleterre .
La guerre de l' Indépendance américaine ( 1775 -- 1783 ) trouve un puissant écho au sein du peuple irlandais .
En effet , des associations de volontaires militaires irlandais usent de leur influence pour parler en faveur d' une plus grande indépendance du Parlement irlandais .
Les paysans se soulèvent à Wexford et , quoique insuffisamment armés , combattent avec bravoure .
Le pays est alors nommé " Royaume-Uni de Grande-Bretagne et d' Irlande " .
Une république d' Irlande s' autoproclame à Dublin en 1916 et est approuvée en 1919 par Dáil Éireann , le parlement lui aussi auto-déclaré .
Ceci provoque une augmentation du support pour l' insurrection en Irlande et la déclaration d' indépendance est ratifiée par le Dáil Éireann , le parlement de la république auto-déclarée en 1919 .
Aujourd'hui , l' état est connu sous le nom de République d' Irlande .
Des désaccords entre les manifestants et le Royal Ulster Constabulary conduisent à des différends au niveau communal .
Ces changements sont suivis par de nouveaux chefs à Dublin ( Albert Reynolds ) , à Londres ( John Major ) et à la tête des unionistes ( David Trimble ) .
Le contrôle de l' Empire britannique sur ses colonies diminue au cours de l' entre-deux-guerres .
Entre 1867 et 1910 , l' Australie , le Canada et la Nouvelle-Zélande reçoivent le statut de dominion .
Ils deviennent des membres fondateurs du Commonwealth britannique ( connu sous le nom de Commonwealth of Nations depuis 1949 ) , une organisation informelle qui succède à l' Empire britannique .
Aujourd'hui , la plupart des anciennes colonies britanniques font partie du Commonwealth , pratiquement toutes en tant que membre indépendant .
La Première Guerre mondiale change radicalement la société britannique .
En 1918 , l' armée et la Royal Air Force , formée du Royal Naval Air Service ( RNAS ) et du Royal Flying Corps , en comptent cinq millions .
La Royal Air Force comptait environ deux cent cinquante mille unités combattantes en 1918 .
Le pays comptera à la fin de la guerre 900000 morts [ réf. nécessaire ] et plusieurs centaines de milliers de blessés ( la génération perdue du poète écrivain T.S. Eliot ) .
Pour reconstruire sa flotte le Royaume uni doit faire appel au crédit américain .
Les États-Unis prennent la place de première puissance maritime .
De retour des tranchées , beaucoup éprouvent de la rancœur face à l' indifférence des civils et , compte tenu de leur propre sacrifice ( en France , un phénomène semblable a lieu ) , la conscription brasse des populations d' origines et rangs divers et ce nivellement de masse accéléra le changement social d' après-guerre .
Les réformes sociales du siècle dernier donnent naissance en 1900 au Parti travailliste , qui n' accède cependant au pouvoir qu' en 1922 .
La grande dépression de 1929 frappe durement le nord de l' Angleterre et le Pays de Galles ( 70 % de chômeurs dans certaines régions ) .
Le roman Le Quai de Wigan de George Orwell décrit bien les conditions d' existence de la classe ouvrière du nord de l' Angleterre durant les années 1930 .
Le début de la Seconde Guerre mondiale est l' occasion d' une relance de l' emploi ( défense , armements … ) .
D' ailleurs les sévères bombardements aériens par la Luftwaffe sur les grandes villes forgent l' esprit britannique de résistance à l' ennemi .
Même la princesse et future reine Elizabeth II contribue à l' effort de guerre en conduisant des camions .
Après la Seconde Guerre mondiale , Clement Attlee est porté au pouvoir par le raz-de-marée électoral de 1945 .
Pour Winston Churchill ( 1951 -- 1955 ) , c' est la troisième expérience de gouvernement après la coalition du temps de guerre et le bref gouvernement d' après-guerre .
Il s' attache à maintenir et entretenir la " relation spéciale " avec les États-Unis .
Il doit affronter les soubresauts des crises internationales , souvent liées au réveil des nationalismes dans les ex-colonies , et le déclin du prestige et de la puissance de l' Empire britannique .
La Cour internationale de justice propose un partage des profits , que Churchill rejette .
Anthony Eden arrive au pouvoir le 7 avril 1955 .
En 1956 , le président égyptien Gamal Abdel Nasser veut nationaliser le canal de Suez .
Après des mois d' intenses négociations et de tentatives de médiation , le Royaume-Uni , la France et Israël réagissent en attaquant et occupent la zone du canal .
Les États-Unis et l' URSS , partisans de la décolonisation , s' y opposent chacun de leur côté ; et l' opinion internationale est majoritairement défavorable à l' intervention .
Face à ce tollé , la France et la Grande-Bretagne cèdent et retirent leurs troupes .
Harold Macmillan ( janvier 1957 -- 1963 ) , spécialiste des affaires économiques et financières , recherche le plein emploi contre l' avis de ses collaborateurs , qui doivent finalement démissionner .
Il supervise de près la politique étrangère du pays , recherche un rapprochement avec l' Europe et explore les pistes d' une entrée dans la CEE .
La technologie nucléaire ( militaire ) n' étant pas encore au point , il se rapproche des États-Unis pour l' améliorer .
La carrière politique d' Alec Douglas-Home ( 1963 -- 1964 ) est originale et médiocre à la fois .
Harold Wilson ( 1964 -- 1970 ) , chef du Parti travailliste , remporte les élections mais avec une faible majorité de cinq sièges .
Plusieurs anciennes colonies connaissent des crises ( Rhodésie , futur Zimbabwe , et Afrique du Sud ) .
Le gouvernement soutient les États-Unis dans sa guerre du Viêt Nam mais refuse d' y envoyer des troupes .
Les marins font grève durant six semaines sitôt Wilson réélu en 1966 , ce qui accroît ce sentiment .
Edward Heath ( 1970 -- 1974 ) doit affronter la question de l' Irlande du Nord .
( Bloody Sunday , 1972 : quatorze hommes sont tués lors d' une manifestation catholique non autorisée à Londonderry ) .
En 1973 cependant , il réussit à faire entrer le pays dans la Communauté économique européenne .
L' inflation est galopante , une grave crise énergétique doublée du choc pétrolier suite à la guerre du Kippour ( 1973 ) oblige le pays à ne travailler que trois jours par semaine .
Aux élections de février 1974 , les parlementaires unionistes d' Irlande du Nord refusent de soutenir le gouvernement .
Les négociations échouent aussi avec les libéraux pour former un gouvernement et Heath démissionne .
Harold Wilson ( 1974 -- 1976 ) arrive de nouveau au pouvoir en 1974 .
Les travaillistes veulent renégocier les termes de l' accord d' entrée dans la CEE .
Mais la crise économique perdure et Wilson démissionne en mars 1976 .
Les syndicats refusent cette limitation de hausse et entament des grèves dures à l' hiver 1978 : l' Hiver du mécontentement .
La voie est libre pour les conservateurs et pour Margaret Thatcher .
Elle entend mettre un terme aux politiques socialistes menées depuis trente ans , selon les mots de son mentor Keith Joseph .
Elle est sur de nombreux thèmes ( à l' exception notable des déficits publics ) au diapason avec les idées de Ronald Reagan , élu un an après elle .
Ses grands inspirateurs économiques sont Friedrich von Hayek et Milton Friedman .
Le 2 avril 1982 , la guerre des Malouines ( la guerre des îles Falkland pour les Britanniques ) éclate .
Thatcher fait intervenir l' armée et inflige une défaite dont la dictature argentine ne se remettra pas .
Pendant ce temps , le Parti travailliste se radicalise de plus en plus , s' éloignant aussi de sa base [ réf. souhaitée ] .
Margaret Thatcher est résolue à réduire la puissance des syndicats .
Elle doit aussi affronter l' Armée républicaine irlandaise provisoire et ses attentats .
Margaret Thatcher échappe à l' attentat du grand hôtel de Brighton , pendant le congrès du Parti conservateur le 12 octobre 1984 .
Elle est d' accord avec Ronald Reagan pour sa politique de défense envers l' Union soviétique , au grand dam de l' Union européenne qui recherche la détente et de bonnes relations .
Mais Mikhaïl Gorbatchev arrivant au pouvoir en 1985 , elle révise sa position hostile .
Elle appuie le raid aérien contre la Libye au départ de bases en Grande-Bretagne alors que les autres membres de l' Otan s' y opposent .
Elle est populaire dans la plupart des quotidiens sauf le Daily Mirror et le Guardian .
Elle fait reposer cet échec sur le chancelier Nigel Lawson qui aurait mené contre ses consignes une politique permettant une intégration économique européenne .
C' en est trop pour ses partisans en Écosse .
Premier ministre durant la guerre du Golfe , il doit affronter la récession mondiale .
Le nouveau leader travailliste , Tony Blair , arrive au pouvoir .
Tony Blair était le leader charismatique du " New Labour " ( " nouveau parti travailliste " ) , converti à l' économie libérale .
Cependant , des difficultés naissent avec Gordon Brown en matières fiscale et économique .
Brown avait conclu un accord avec Blair qui devrait lui laisser le poste de premier ministre après deux mandats .
Il préfère aligner ses troupes avec celles des États-Unis dans la guerre d' Irak malgré la position de la France et de l' Allemagne et une opinion publique britannique plutôt défavorable .
Sous la pression de fractions travaillistes et en raison de l' accord passé avec Gordon Brown , il démissionne en 2007 .
Gordon Brown est le seul candidat aux élections pour le remplacer .
