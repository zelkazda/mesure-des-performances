Le premier exemple d' outil plus complexe est l' abaque , qui connut diverses formes , jusqu' au boulier toujours utilisé en Chine et en Russie .
Parmi les algorithmes les plus anciens , on compte des tables datant de l' époque d' Hammurabi ( env. -1750 ) .
Vers 1617 , John Napier invente une sorte d' abaque perfectionné .
Son invention des logarithmes permit en 1625 à William Oughtred de développer la règle à calcul qui fut utilisée jusqu' à l' apparition des calculatrices de poche par de nombreux ingénieurs .
Ainsi , par exemple , une grande partie des calculs nécessaires au programme Apollo furent effectués avec des règles à calcul .
Deux manuscrits de Léonard de Vinci , écrits vers 1500 , semblaient décrire un tel mécanisme .
Wilhelm Schickard en 1623 , dans une lettre écrite à Johannes Kepler , propose l' utilisation d' une machine décrite comme une horloge à calcul capable de " calculer immédiatement les nombres donnés automatiquement ; additionner , soustraire , multiplier et diviser , etc .
En 1673 , Gottfried Leibniz en perfectionne le principe pour la rendre capable d' effectuer des multiplications , des divisions et même des racines carrées , le tout par une série d' additions sous la dépendance d' un compteur .
Leibniz inventa aussi le système binaire , système de numération qui sera approprié aux futurs ordinateurs .
Jacques de Vaucanson reprend l' idée en remplaçant ruban et cartes par un cylindre métallique perforé .
En 1833 , Charles Babbage décrivit sa machine analytique .
Ada Lovelace créa une série de programmes ( suite de cartes perforées ) pour cette machine , ses efforts firent d' elle la première programmeuse du monde .
L' ère des ordinateurs modernes commença avec les grands développements de la Seconde Guerre mondiale .
Konrad Zuse mit au point cette année-là le Z1 , qui ne fonctionna jamais vraiment correctement faute de crédits de développement .
Berry achevèrent l' ABC .
Colossus était la première machine totalement électronique , elle utilisait uniquement des tubes à vide et non des relais .
Colossus implémentait les branchements conditionnels .
Il a été dit que Winston Churchill a personnellement donné l' ordre de leur destruction en pièces de moins de vingt centimètres pour conserver le secret .
La première machine utilisant cette architecture était le Small-Scale Experimental Machine construit à l' université de Manchester en 1948 .
Parallèlement , l' université de Cambridge développa l' EDSAC , inspiré des plans de l' EDVAC , le successeur de l' ENIAC .
Contrairement à l' ENIAC qui utilisait le calcul en parallèle , l' EDVAC et l' EDSAC possédaient une seule unité de calcul .
Il utilisait un type de mémoire différent du Manchester Mark I , constitué de lignes à retard de mercure .
En février 1951 , le premier modèle de Ferranti Mark I , version commerciale du Manchester Mark I et premier ordinateur commercial de l' histoire , est vendu .
En avril 1952 , IBM produit son premier ordinateur , l' IBM 701 , pour la défense américaine .
L' IBM 701 utilisait une mémoire à tubes cathodiques de 2 048 mots de 36 bits .
La même année , IBM est contacté pour mettre en chantier la production des ordinateurs du réseau SAGE .
En juillet 1953 , IBM lance l' IBM 650 , ordinateur scientifique comme tous ceux des séries 600 ( son successeur sera le 1620 ) .
En avril 1955 , IBM lance l' IBM 704 , premier ordinateur commercial capable aussi de calculer sur des nombres à virgule flottante .
D' après IBM , le 704 pouvait exécuter 40 000 instructions par seconde .
Par exemple , l' IBM 650 de 1954 composé de tubes à vide pesait 900 kg et son alimentation environ 1 350 kg , chacun enfermé dans un module de près de 2,5 m³ .
En 1955 , Maurice Wilkes inventa la microprogrammation , désormais universellement utilisée dans la conception des processeurs .
En 1956 , IBM sortit le premier système à base de disque dur , le Ramac 305 .
L' IBM 350 utilisait 50 disques de 24 pouces en métal , avec 100 pistes par face .
Le premier langage de programmation universel de haut niveau à être implémenté , le Fortran , fut aussi développé par IBM à cette période .
En 1959 , IBM lança l' IBM 1401 ( commercial ) , qui utilisait des cartes perforées .
En 1960 , IBM lança l' IBM 1620 ( scientifique ) .
Un exemplaire opérationnel fut longtemps présent au palais de la Découverte .
En 1960 , l' IBM 7000 est le premier ordinateur à base de transistors .
Le travail de l' utilisateur est facilité par le langage Programmation Automatique des Formules ( PAF ) , qui traduit les fonctions explicites en langage machine .
Plus de 14 000 ordinateurs IBM 360 furent vendus jusqu' en 1970 , date où on les remplaça par la série 370 beaucoup moins chère à puissance égale ( mémoires bipolaires à la place des ferrites ) .
Celui-ci supportait de nombreux langages , dont l' Algol et le Fortran , comme les " grands " .
Le BASIC y sera adjoint plus tard .
Dans le même temps , grâce à une politique de mise en commun gratuite de logiciels particulièrement novatrice , l' IBM 1130 se tailla la part du lion dans les écoles d' ingénieurs du monde entier .
Le circuit intégré a été inventé par Jack St. Clair Kilby en 1958 .
Clive Sinclair se basera plus tard sur cette approche pour construire son Sinclair ZX80 .
Dans les années 1970 , IBM a sorti une série de mini-ordinateurs .
Une troisième série a succédé à la série 30 : les AS/400 .
Une définition non universellement acceptée associe le terme de quatrième génération à l' invention du microprocesseur par Marcian Hoff .
Les contrôleurs 3745 ( IBM ) utilisaient intensivement cette technologie .
Dans le même temps , aux États-Unis , la compagnie AT & T se rendit compte qu' avec tous ses standards téléphoniques interconnectés , elle se trouvait sans l' avoir cherché disposer du plus grand réseau d' ordinateurs des États-Unis ( un standard téléphonique , depuis l' invention des microprocesseurs , tient beaucoup plus de l' ordinateur que du dispositif câblé , et nombre d' entre eux se commandent en UNIX ) .
Il comprenait une unité centrale équipée d' un Intel 8008 ( 4 ko extensible à 16 ko ) , d' un lecteur enregistreur de mini-cassette magnétique et d' une imprimante à boule IBM .
Il utilise lui aussi le microprocesseur Intel 8008 .
La machine ne survécut pas au rachat de R2E par Bull .
En janvier 1975 , sort l' Altair 8800 .
Il était le premier ordinateur à utiliser un processeur Intel 8080 .
C' est l' Altair qui inspira le développement de logiciels à Bill Gates et Paul Allen , qui développèrent un interpréteur BASIC pour cette machine .
En 1975 sortira aussi l' IBM 5100 , machine totalement intégrée avec son clavier et son écran , qui se contente d' une prise de courant pour fonctionner .
Ces passionnés se rencontraient lors de réunions au Homebrew Computer Club , où ils montraient leurs réalisations , comparaient leurs systèmes et échangeaient des plans ou des logiciels .
Il vendit avec Steve Jobs environ 200 machines à 666 $ l' unité .
L' Apple II sortit en 1977 .
Malgré son prix élevé ( environ 1 000 $ ) , il prit rapidement l' avantage sur les deux autres machines lancées la même année , le TRS-80 et le Commodore PET , pour devenir le symbole du phénomène de l' ordinateur personnel .
D' une très grande qualité , l' Apple II avait de gros avantages techniques sur ses concurrents : il avait une architecture ouverte , un lecteur de disquettes , et utilisait des graphismes en couleur .
Grâce à l' Apple II , Apple domina l' industrie de l' ordinateur personnel entre 1977 et 1983 .
Plus de deux millions d' Apple II furent vendus au total .
En août 1981 sortit l' IBM PC .
Cela se révèlera une erreur monumentale pour IBM .
L' ordinateur le plus vendu de tous les temps [ réf. nécessaire ] fut sans doute le Commodore 64 , dévoilé par Commodore International en septembre 1982 .
Après le 64 , Commodore sortit l' Amiga .
C' est à cette époque que le PC devint l' architecture dominante sur le marché des ordinateurs personnels .
Seul le Macintosh d' Apple continua à défier l' IBM PC et ses clones , qui devinrent rapidement le standard .
Son interface graphique s' inspirait de celle du Xerox Star .
Le 22 janvier 1984 , Apple lance le Macintosh , le premier micro-ordinateur à succès utilisant une souris et une interface graphique .
Il était fourni avec plusieurs applications utilisant la souris , comme MacPaint et MacWrite .
Malgré ses nombreuses innovations dans le domaine , Apple perdit peu à peu des parts de marché pour se stabiliser à environ 4 % des ventes d' ordinateurs dans les années 2000 .
Et ce , malgré le succès de l' iMac , premier ordinateur conçu par des designers , qui s' écoula à plus de six millions d' exemplaires , en en faisant le modèle d' ordinateur personnel le plus vendu au monde .
Cela a permis de remplacer les logiciels spécifiques affectés par le problème , par des logiciels ou des progiciels le plus souvent sous UNIX avec des ordinateurs de taille réduite .
Cette décennie a aussi été marquée bien sûr par le développement de l' Internet et l' apparition de la Toile .
Avec Internet s' ouvre une nouvelle page de l' histoire de l' informatique .
