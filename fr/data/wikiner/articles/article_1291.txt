Super Mario Sunshine est un jeu vidéo de plates-formes développé par Nintendo EAD pour l' éditeur japonais Nintendo .
Cet épisode retrace les aventures du héros Mario sur l' île Delfino .
Comme Super Mario 64 six ans auparavant , Super Mario Sunshine constitue l' épisode GameCube de la série principale Super Mario commencée en 1985 .
Super Mario Sunshine prend place dans un monde imaginaire exotique .
L' histoire se déroule sur l' île Delfino , une île tropicale en forme de dauphin .
Cette île ne fait pas partie du Royaume Champignon .
Le joueur contrôle Mario ( doublage anglais : Charles Martinet ) .
Lorsque le jeu débute , l' avion de Mario subit un atterrissage mouvementé .
peut être considéré comme un outil par ses fonctions ou alors comme un personnage du fait qu' il parle et le joueur a la possibilité de le contrôler par le biais de Mario .
Mario est arrêté car un criminel a , sous son apparence , saccagé l' île en peignant des graffiti un peu partout .
Mario est jugé et reconnu coupable .
Là , Mario doit faire face à un gigantesque robot en forme de Bowser .
Bowser Jr .
Mario aura de nouveau affaire à Bowser Jr .
Le gameplay de Super Mario Sunshine ressemble beaucoup à celui de l' opus précédent , Super Mario 64 , mais Mario peut utiliser de nouvelles fonctions comme l' attaque en vrille , qui lui permet de sauter et d' attaquer tout en tournant sur lui-même .
Comme dans Super Mario 64 , le joueur peut découvrir l' environnement dans toutes les directions sans limite de temps .
Toutefois , l' environnement de Super Mario Sunshine est plus réaliste que dans son prédécesseur comme le montre certains éléments du décor ( habitations , parc d' attractions , fruits , insectes ... ) et le fait que Mario peut avoir une insolation .
Les niveaux regorgent d' ennemis qui attaquent Mario mais aussi de personnages qui aident Mario ou demandent un service .
Super Mario Sunshine est un jeu qui se joue en solo .
Dans ce jeu , Mario dispose de plusieurs mouvements distincts .
Ces soleils sont équivalents aux étoiles de puissance de Super Mario 64 et le but du jeu est de tous les obtenir .
Une suite de Super Mario 64 était déjà en projet depuis plusieurs années .
Super Mario 64 2 et Super Mario 128 , dont les sorties ont été annulées , présentaient déjà quelques idées pour une suite de Super Mario 64 .
Certains éléments de Super Mario 128 ont été utilisées pour la conception de Super Mario Galaxy .
Certaines avaient déjà été évoquées dans The Legend of Zelda : Majora 's Mask .
Takashi Tezuka dit : " le concept a repris des éléments de Super Mario 64 en ajoutant leurs idées " .
Certains types de buses ont dû être retirés car ils ont fait polémique aux États-Unis .
Les développeurs ont aussi crée un univers quatre fois plus grand que dans Super Mario 64 et l' ont rendu plus réel .
Pour les voix dans Super Mario Sunshine , ce sont les acteurs habituels qui prêtent leur voix aux personnages .
Super Mario Sunshine est développé par le studio Nintendo EAD pour Nintendo .
Super Mario Sunshine a été un succès commercial .
Le jeu était en 2002 , le dixième meilleur jeu vendu aux États-Unis selon le NPD Group .
Super Mario Sunshine a reçu les éloges de la presse spécialisée .
et GameSpy a ajouté une critique positive sur la conception du décor et des niveaux aux hauteurs .
Le jeu a reçu une très bonne note de la part de Nintendo Power qui commente la qualité des graphismes , de la musique , de la mise en scène , des cinématiques et des énigmes .
GamePro a également donné une note parfaite au jeu toujours pour la qualité des niveaux .
Computer and Video Games affirme que le jeu est mieux que Super Mario 64 .
L ' Official Nintendo Magazine place le jeu à la 46 e place parmi les 100 meilleurs jeux Nintendo de tous les temps .
GameSpot a critiqué le système de caméra et que le jeu peut être parfois ennuyeux car il oblige à refaire les niveaux plusieurs fois pour finir le jeu .
Super Mario Sunshine présente de nouveaux personnages dans la série .
Il est devenu un personnage récurrent de la série principale en tant qu' adversaire ( New Super Mario Bros où il est l' antagoniste principal , New Super Mario Bros Wii , Super Mario Galaxy et Super Mario Galaxy 2 ) et dans les spin-off sportifs en tant que personnage jouable ( dans la série des Mario Kart depuis Mario Kart : Double Dash ! ! , Mario Golf : Toadstool Tour , Mario Power Tennis , Mario Strikers Charged Football , Mario Super Sluggers ou encore Mario & Sonic aux Jeux Olympiques d' Hiver .
Une réédition de Super Mario Sunshine intitulée Super Mario Sunshine est sortie en 2003 .
Sur la console de la génération suivante , la Wii , Nintendo publie en 2007 une suite , Super Mario Galaxy , et en annonce une autre , Super Mario Galaxy 2 , prévue pour le 11 juin 2010 .
Super Mario Galaxy ne reprend aucune nouveauté de Super Mario Sunshine .
