Au départ , la musique de Ange est un mélange de rock progressif et de musique médiévale .
Leur rock , au départ fortement influencé par Genesis et King Crimson , se veut théâtral et poétique .
Christian Décamps , le leader et chanteur du groupe , manie les mots et les tournures de phrases , donnant ainsi au groupe une originalité qui le démarque des autres formations de l' époque .
