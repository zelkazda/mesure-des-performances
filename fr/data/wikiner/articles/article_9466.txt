Le professeur Léon Schwartzenberg , né le 2 décembre 1923 à Paris et mort le 14 octobre 2003 à Villejuif , est un cancérologue qui s' est fait connaître comme le défenseur des sans-abri et des " sans-papiers " .
Il est très rapidement interdit de faculté de médecine en raison des lois raciales de Vichy .
En 1993 , le Conseil d' État annule cette décision .
Il est enterré à Paris , au cimetière du Montparnasse .
