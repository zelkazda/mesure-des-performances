Paul VI fut pape du 21 juin 1963 à sa mort , le 6 août 1978 .
Il est décédé le 6 août 1978 à Castel Gandolfo en Italie .
Ayant perdu ses parents très jeune , elle est placée sous l' autorité d' un tuteur et envoyée dans un pensionnat religieux à Milan .
Les Montini ont une origine montagnarde .
Giovanni Battista Montini naît le 26 septembre 1897 à Concesio .
Comme le veut la coutume pour les familles bourgeoises de Brescia , il est confié à une nourrice .
L' année suivante ( en 1905 ) , Montini reprend l' école .
Dès le collège , il rejoint l' association Manzoni , du nom de l' auteur italien Alessandro Manzoni , qui rassemblait des élèves et des étudiants catholiques .
Montini n' ayant laissé aucun journal intime ni mémoires , on ne peut déterminer avec exactitude comment naquit sa vocation .
Pourtant , il ne suivra pas la même formation que ses confrères séminaristes : son état de santé demeurant fragile , le supérieur du séminaire et l' évêque de Brescia acceptent d' emblée que le jeune homme ne soit pas soumis à la vie d' internat .
Il prend tout d' abord la présidence de l' association Manzoni en 1917 , grâce à laquelle il lance une " bibliothèque du soldat " destinée à envoyer aux soldats du front de bons livres leur permettant de se distraire et d' entretenir leur foi .
En juin 1918 , Montini s' attelle à un autre grand projet : défendre la liberté de l' enseignement .
Enfin , il prend position en faveur du PPI dont son père sera député à trois reprises .
Après une retraite spirituelle qu' il doit interrompre à cause de la chaleur , Montini est ordonné prêtre le 29 mai 1920 .
Il célèbre sa première messe le lendemain dans le sanctuaire de Santa Maria delle Grazie , la nappe d' autel ayant été taillée dans la robe de sa mère .
Montini arrive à Rome le 10 novembre 1920 .
Il y étudie dans deux universités : à la Grégorienne ( chez les jésuites ) et à la Sapienza .
Pour la première fois , 35 fascistes ( dont Mussolini ) y sont élus .
Il assiste en outre au couronnement de Pie XI le 2 février , toujours dans la basilique .
Le nouveau pape reçoit les élèves de l' académie le 6 mars , dont Montini .
Après avoir voyagé en Allemagne et en Autriche durant l' été 1922 , le jeune prêtre passe son doctorat en droit canon le 9 décembre suivant .
En mai 1923 , Montini apprend qu' il est affecté à la nonciature de Varsovie en tant qu ' attaché à la nonciature .
Durant l' été 1924 , Montini fait un séjour d' un mois en France : il prend des cours de français à l' Alliance française de Paris dispensés par René Doumic , et visite notamment le musée du Louvre et la ville de Lisieux où repose sainte Thérèse .
Montini continuera en parallèle son apostolat auprès des jeunes , travaillant au Vatican le matin puis au Cercle romain de la FUCI l' après-midi .
Pour donner plus d' autorité à l' aumônier , Pie XI le nomme camérier secret , titre ne correspondant plus à aucune fonction précise .
La direction spirituelle de la FUCI n' est pas de tout repos , notamment à cause des multiples incidents qui naissent entre les étudiants catholiques et les étudiants fascistes .
La tentative d' assassinat de Mussolini le 31 octobre 1926 envenima ces incidents .
Montini adopte alors une nouvelle stratégie pour évangéliser le milieu étudiant sans risquer de heurts : le combat culturel , visant à former de l' intérieur le milieu étudiant en donnant un nouvel élan à la culture catholique .
Ses écrits témoignent de l' influence qu' on eue sur lui l' abbé Maurice Zundel et le philosophe Jacques Maritain .
Néanmoins , le mouvement essuie bientôt de grandes difficultés , et Montini se verra contraint d' en démissionner :
D' autre part , Montini lui-même doit faire face à plusieurs accusations :
Il poursuit en effet son enseignement d' histoire de la diplomatie pontificale à l' Université du Latran et assure un cours d' introduction au dogme catholique dans la même université .
L' été 1934 est pour lui l' occasion de voyager en France , en Grande-Bretagne et en Irlande .
Il s' éloigne de Rome pendant toute l' année 1935 pour des raisons de santé , et se repose près de sa région natale de Brescia .
À son retour , il retourne à son travail à la secrétairerie d' État , sans entrain et avec lassitude .
Cependant , la survenance de la Seconde Guerre mondiale va bousculer cette organisation .
Le 10 février 1939 , le pape Pie XI meurt ; son successeur , le cardinal Pacelli , est élu le 2 mars suivant et prend le nom de Pie XII .
Pendant le temps du conclave , Montini veille à l' organisation matérielle des lieux où se réunissent les cardinaux .
Montini et le pape se verront tous les jours avant la guerre et pendant celle-ci , multipliant les audiences et les productions de documents .
En janvier 1940 , Pie XII demande à Montini de diffuser des messages via Radio Vatican pour dénoncer le sort réservé par les nazis au clergé et aux civils polonais .
À partir de septembre 1942 , Montini va se trouver au cœur d' un complot visant à renverser Mussolini .
Le lendemain , le roi demande au maréchal Badoglio de former un ministère et ce dernier fait arrêter Mussolini .
Le 13 août 1943 , un nouveau bombardement allié survient sur Rome : Montini accompagne à nouveau le pape sur les lieux touchés afin de réconforter la population .
Le lendemain , le gouvernement Badoglio proclame Rome " ville ouverte " .
Pie XII avait en effet estimé que le peuple allemand n' était pas collectivement coupable de la Seconde Guerre mondiale , ce à quoi le philosophe Jacques Maritain répondait que le peuple allemand était responsable comme peuple .
L' ambassadeur français insiste aussi auprès de Montini pour que Pie XII renouvelle son soutien au peuple juif en faisant une déclaration solennelle de compassion en faveur des victimes de la Shoah .
Face à la Démocratie chrétienne dirigée par Alcide De Gasperi , d' autres partis dits chrétiens apparaissent , notamment à gauche .
Partant , il rédige ou signe pour le pape un grand nombre de discours , messages ou allocutions à des organisations , personnalités ou pèlerins de passage au Vatican .
Ce dernier est réputé pour être ouvert d' esprit , et les théologiens condamnés par le Saint Office ou en passe de l' être viennent d' abord se référer à Montini avant d' aller voir le pape .
Montini relativise la portée du texte en confiant à son ami Jean Guitton que le pape ne dénonce pas des erreurs mais seulement des opinions pouvant aboutir à des erreurs .
Les protestants s' insurgent contre cette proclamation car elle attribue un privilège supplémentaire à la Vierge Marie qui n' est pas attesté historiquement et , aussi , elle engage l' infaillibilité du pape , notion que les protestants refusent également .
Montini reçoit beaucoup de prélats et de diplomates au Vatican .
Parmi ceux-ci , dom Hélder Câmara avec qui il évoque la création d' une conférence épiscopale pour le Brésil .
Enfin , le substitut effectue un voyage au Canada et aux États-Unis en 1951 , où il rencontre notamment Mgr Spellman , archevêque de New York .
Selon certains , Pie XII lui aurait " suggéré " de renoncer à cette promotion , probablement parce qu' il ne voulait pas de lui comme successeur .
Quelques auteurs , dont Jean Guitton , l' ont en effet affirmé .
Pourtant , ils l' accepteront tous les deux dès le premier consistoire de Jean XXIII le 15 décembre 1958 .
Le cardinal Schuster , archevêque de Milan depuis le 26 juin 1929 , meurt le 30 août 1954 .
Peu de temps après , Pie XII annonce à Montini qu' il songe à le nommer à cette fonction .
Bien que le siège archiépiscopal de Milan fût considéré comme illustre , Montini ressent cette nomination comme une sanction .
Il souffre en effet qu' on l' éloigne ainsi de Rome .
Quoi qu' il en soit , le futur évêque se prépare à sa nouvelle charge , et reçoit dès le mois de novembre 1954 l' évêque auxiliaire et le vicaire général de l' Archidiocèse de Milan .
La consécration épiscopale de Montini est célébrée le 12 décembre 1954 en la Basilique Saint-Pierre .
Pie XII , malade , ne peut procéder lui-même au sacre .
Il prend le train jusqu' à la ville de Lodi , où il est reçu par l' évêque du lieu et le vicaire général de Milan .
Puis , se rendant à Milan en voiture , le nouvel archevêque descend du véhicule et embrasse le sol de son nouveau diocèse .
Arrivé au Dôme de Milan , il prononce un discours mêlant esprit de tradition ( " Notre catholicisme doit être intègre et fidèle " ) et esprit d' ouverture ( il faut œuvrer à la " pacification de la tradition catholique italienne avec le bon humanisme de la vie moderne " ) .
Le diocèse de Milan , le plus important d' Italie avec plus de trois millions d' habitants , est en proie à la déchristianisation et à la libéralisation des mœurs .
M gr Montini se constitue progressivement un cercle restreint de clercs qui seront aussi ses conseillers , notamment le supérieur du séminaire de Milan qu' il recevra tous les mercredis .
L' événement est considérable : deux cardinaux ( Giuseppe Siri et Giacomo Lercaro ) , vingt-quatre archevêques et évêques , plus d' un millier de prêtres et religieux sont mobilisés pour prêcher dans les lieux de la ville .
M gr Montini fait deux retraites le mois d' août suivant , dans l' Abbaye d' Einsiedeln puis dans celle d' Engelberg en Suisse .
Le pape Pie XII meurt le 9 octobre 1958 à Castel Gandolfo , après trois jours d' agonie .
Depuis son ordination épiscopale , Montini ne l' avait vu qu' à quelques audiences publiques mais jamais personnellement .
Bien que Montini ne soit pas cardinal , certains envisagent quand même son élection au trône de saint Pierre , ce qui est canoniquement possible mais ne s' était pas produit depuis l' élection de Grégoire XVI en 1831 .
Certains cardinaux , dont Giuseppe Siri , s' y opposent néanmoins farouchement .
Le Conclave de 1958 s' ouvre donc le 26 octobre et , après deux jours et dix scrutins infructueux , le cardinal Roncalli est élu le 28 et prend le nom de Jean XXIII .
M gr Montini est finalement nommé cardinal lors du consistoire du 15 décembre 1958 .
Le 25 janvier 1959 , Jean XXIII annonce officiellement son intention de procéder à un concile œcuménique , afin de prolonger les travaux du Concile Vatican I , interrompu en 1870 .
Parmi toutes les réponses recueillies , reviennent régulièrement une proclamation dogmatique de la médiation de la Vierge Marie , la condamnation du communisme et l' instauration de la langue vernaculaire dans la liturgie .
À la fin de l' année 1961 , Jean XXIII nomme le cardinal Montini membre de la commission centrale .
Le concile Vatican II s' ouvre à Rome le 11 octobre 1962 ; plus de 2000 évêques et supérieurs du monde entier , ainsi qu' une trentaine d' observateurs non catholique , se rassemblent pour l' occasion dans la Basilique Saint-Pierre .
M gr Montini y est présent , et il a fait inviter son ami Jean Guitton parmi les observateurs .
Montini , que Jean XXIII a pris soin de loger dans une maison attenante à la basilique , est resté très discret durant cette première session .
Les 5 et 6 décembre , la voix de Montini est écoutée : Jean XXIII proclame la création d' une commission de coordination ayant pour but de relier les autres commissions entre elles ; elle est composée de cinq cardinaux : Léon-Joseph Suenens , Paul-Émile Léger , Giacomo Lercaro , Julius Döpfner et Montini .
Jean XXIII meurt le 3 juin 1963 .
Le conclave qui va élire le successeur de Jean XXIII s' ouvre dans l' après-midi du 19 juin 1958 , dans la chapelle Sixtine .
Il était pressenti favori par tous à tel point que le journal La Croix publia son édition spéciale sur sa nomination quelques minutes à peine après l' annonce officielle .
Vers midi , le cardinal Ottaviani annonce l' élection du nouveau pape à la foule massée place Saint-Pierre .
Après son apparition place Saint-Pierre , le nouveau pape retourne parmi les cardinaux et partage un banquet avec eux , en prenant la même place que pendant le conclave .
Le lendemain , il prend possession des appartements pontificaux , aux deuxième et troisième étages du Palais du Vatican .
Le premier soir où il loge dans les appartements pontificaux , il se plaint d' être gêné par le bruissement des fontaines de la place Saint-Pierre .
Le 22 juin , lendemain de son élection , le pape s' adresse aux cardinaux réunis dans la chapelle Sixtine dans un message retransmis par Radio Vatican .
Il affirme les principaux objectifs de son pontificat : reprendre le Concile Vatican II ( " La partie la plus importante de notre pontificat sera occupée par la continuation du deuxième concile œcuménique du Vatican , vers lequel sont tournés les yeux de tous les hommes de bonne volonté. " ) , œuvrer à la paix entre les peuples et à l' unité des chrétiens .
Le dimanche 30 juin 1963 a lieu le couronnement de Paul VI .
Au cours d' une longue cérémonie , l' épître et l' évangile sont chantés en latin puis en grec ( en signe d' unité ) , puis Paul VI fait une allocution au cours de laquelle il parle en neuf langues [ réf. nécessaire ] .
Après cette allocution , le cardinal Ottaviani , protodiacre , pose sur la tête du souverain pontife la tiare qui a été dessinée selon les indications du nouveau pape : simple et fuselée .
Lors de son premier message au monde le 22 juin , lendemain de son élection , Paul VI prévoit de mener à bien trois grands projets :
Pour préparer cette reprise , Paul VI réunit à deux reprises la commission de coordination , les 3 juillet et le 31 août .
Il approuve l' idée d' organiser les sessions selon un plan précis , confirme le cardinal Suenens en tant que légat au sein du concile , et émet le souhait d' unifier les tendances traditionnelles et progressistes .
Enfin , le 21 septembre , il s' adresse à la Curie romaine et lui annonce deux projets de réforme : création d' un conseil d' évêques du monde entier en qualité de membres dans les congrégations de la Curie romaine et réforme générale de la Curie romaine .
Parmi ces derniers , outre Jean Guitton déjà présent lors de la première session , assistent désormais douze autres laïcs du monde entier ( dirigeants d' organisations catholiques internationales ) .
Le dialogue est un des grands traits du pontificat de Paul VI .
Paul VI resta fidèle aux traditions italiennes qui fait du pape un acteur important de la vie politique et un leader , de fait , de Démocratie chrétienne .
Le PCI ne s' y trompa pas et tenta d' amadouer le pape , sans succès .
C' est la première fois depuis Pie VII qu' un pape quitte l' Italie pour participer à un voyage durant son pontificat .
Âgé de 80 ans et souffrant d' arthrose , Paul VI vit ses derniers jours presque toujours allongé .
Il est victime d' une crise cardiaque en fin d' après-midi le 6 août 1978 dans sa résidence d' été de Castel Gandolfo et meurt quatre heures plus tard , à 21h00 , le jour de la Transfiguration du Christ .
Il est inhumé le 12 août 1978 et enterré selon ses souhaits dans les grottes du Vatican après une cérémonie d' un extrême dépouillement [ réf. nécessaire ] qui a lieu sur le parvis de la basilique Saint-Pierre .
La première audience générale de Paul VI a lieu le 13 juillet 1963 .
Certaines de ces audiences ont lieu dans la Salle Paul VI , inaugurée en 1971 et pouvant accueillir jusqu' à 12000 personnes debout .
