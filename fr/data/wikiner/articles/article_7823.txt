Ce mot désignait autrefois un combattant ( le plus souvent à pied ) dont l' arc était l' arme principale ; la célèbre bataille de Crécy ( 26 août 1346 ) illustra le rôle des archers anglais ( ou yeomen ) équipés de l' arc long ( longbow ) .
L' arc et l' arbalète étaient des armes si redoutables par leur force et si dangereuses par la facilité à s' en servir , que le IIe concile du Latran , tenu en 1139 , les anathématisa .
Il s' est rendu célèbre pour l' avantage qu' il a pu donner aux troupes anglaises de la guerre de Cent Ans .
En créant ainsi à plusieurs un nuage de flèches , les Anglais cherchaient à créer la panique .
Il a été utilisé jusqu' à la Première révolution anglaise puis remplacé par les mousquets à platine à mèche .
Le modèle le plus courant est celui utilisé aux Jeux olympiques , qui comporte des éléments améliorant la stabilité et la précision du tir .
