Le premier être humain à y avoir marché est l' astronaute Neil Armstrong le 21 juillet 1969 à 2 h 56 UTC , lors de la mission Apollo 11 .
Parmi les grands bassins d' impact , le bassin South Pole Aitken , avec ses 2500 km de diamètre , est le plus grand cratère d' impact connu à ce jour dans le système solaire .
Les données de télémétrie laser accumulées depuis les missions Luna et Apollo permettent toutefois aux scientifiques de penser qu' un petit noyau de 300- 400 km de rayon est bien présent .
Pourtant , les données recueillies par les sondes Clementine et Lunar Prospector à la fin des années 1990 montrent la présence de grandes zones riches en hydrogène , aux pôles sud et nord .
À la fin de sa mission , la sonde Lunar Prospector a même été précipitée dans le fond d' un cratère censé contenir de la glace d' eau .
En 2006 , les relevés réalisés par le radiotélescope d' Arecibo braqués sur les cratères polaires constamment dans l' ombre montrent que la présence de glace d' eau est encore plus rare qu' escomptée .
La mission LCROSS a pour objectif de confirmer ou infirmer les informations faisant état de présence d' hydrogène et de glace dans ces lieux difficiles à explorer et encore largement méconnus .
Une analyse du panache de poussières ( provoqué par l' impact de la sonde LCROSS ) révèle , en juin 2010 , la présence de molécules d' eau qui n' avaient pas vu la lumière du soleil depuis des milliards d' années .
À l' exception de Mercure et Vénus , toutes les planètes du système solaire possèdent des satellites naturels qualifiés de lunes .
Le 17 novembre 1970 , Lunokhod 1 fut le premier véhicule robotisé à explorer sa surface .
Les derniers hommes à marcher sur le sol lunaire furent le scientifique Harrison Schmitt et finalement l' astronaute Eugene Cernan , lors de la mission Apollo 17 en décembre 1972 .
Le lundi 31 mars 2008 , la NASA dévoile par l' intermédiaire de Neil Armstrong -- le premier homme à avoir marché sur la lune en 1969 -- son nouveau programme d' exploration de notre satellite .
En 2024 , cette station aurait pu être habitée en permanence par des équipes qui se relaieraient tous les six mois , indique la NASA .
Peu après , cette base lunaire devait être utilisée pour les décollages vers Mars et même plus loin encore .
En effet ceux-ci seraient plus faciles du fait de la très faible gravitation ( 6 fois moins élevée que sur Terre ) .
Elles ont lieu uniquement quand un nœud coïncide avec la nouvelle Lune .
Celle-ci couvre alors le Soleil , en tout ou en partie .
peut-être même dans le Robert , plus vraisemblable en 1680 ) .
Ce nom vient du nom de la déesse grecque Séléné , qui était associée à cet astre .
