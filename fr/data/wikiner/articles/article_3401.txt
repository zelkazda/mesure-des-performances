Actuellement maire de Rueil-Malmaison et député dans les Hauts-de-Seine , il a également été président de l' Assemblée nationale du 7 mars au 19 juin 2007 .
Il fait partie du groupe UMP .
Dès 2002 , il est président de la commission des affaires économiques , de l' environnement et du territoire de l' Assemblée nationale .
Le 7 mars 2007 , il est élu président de l' Assemblée nationale ( mandat jusqu' au 19 juin 2007 ) .
Patrick Ollier est maire de Rueil-Malmaison et membre d' honneur du Rotary Club .
