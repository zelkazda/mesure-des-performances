En 1916 , le chimiste Gilbert Lewis développa l' idée de la liaison par paire d' électrons .
Walter Heitler et Fritz London sont les auteurs de la première explication par la mécanique quantique de la liaison chimique , spécialement celle de l' hydrogène moléculaire , en 1927 , utilisant la théorie de liaisons de valence .
En 1930 , la première description mathématique quantique de la liaison chimique simple fut développée dans la thèse de doctorat de Edward Teller .
