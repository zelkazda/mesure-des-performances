Audiogalaxy était un système de partage de fichier centralisé sur Internet très populaire .
Le site met aujourd'hui à disposition de la musique en ligne via la plateforme Rhapsody .
Les fichiers présents sur les disques durs des utilisateurs étaient alors listés sur le site web d' Audiogalaxy .
Audiogalaxy a très tôt mis en place une version payante en plus de la version gratuite : les abonnés payants avaient accès à :
