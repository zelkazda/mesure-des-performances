Le système d' éducation québécois est un système éducatif appliqué dans la province de Québec , au Canada .
Peu importe le réseau , les barèmes de l' éducation québécoise relèvent du MELS .
Le ministère de l' éducation du Québec fut fondé en 1964 .
De ce nombre , 10 universités sont publiques et rattachées au réseau de l' Université du Québec .
Le MELS impose aux élèves de sixième année un examen de passage du primaire au secondaire .
Autrement , le déroulement des études universitaires ressemble généralement à ce qui est la norme ailleurs en Amérique du Nord .
Elles sont cependant beaucoup plus coûteuses si l' on compare avec certains pays d' Europe , tels la France ou les pays scandinaves .
Avec ses quatre universités , Montréal est la ville d' importance qui possède le plus haut pourcentage d' étudiants en Amérique du Nord .
Au Québec , il existe un réseau scolaire francophone et un réseau scolaire anglophone .
Les frais de scolarité du système d' éducation québécois sont parmi les plus faibles en Amérique du Nord .
