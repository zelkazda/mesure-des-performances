En s' installant en Inde , ils abandonnèrent leur style de vie nomade et se mêlèrent aux populations autochtones du nord de l' Inde .
Un adversaire de la théorie de l' invasion aryenne , Koenraad Elst , répond à cela que considérer une opinion de fait comme fausse en raison de mobiles qu' on prête à ceux qui la soutiennent relève du sophisme génétique ; faisant allusion à la faveur dont la théorie de l' invasion aryenne jouissait chez les nazis et dont elle jouit maintenant , en Inde , chez certains membres des castes supérieures ( auxquels elle fournit une justification raciste de leur position dominante ) ou chez les marxistes , il ajoute : " si une théorie peut être considérée comme fausse simplement parce qu' elle est utilisée à des fins politiques , il est clair que la théorie de l' invasion aryenne elle-même doit être la théorie la plus fausse du monde : on chercherait en vain une hypothèse historique davantage compromise par diverses utilisations politiques , y compris les plus meurtrières. "
Il y a , cependant , la description d' un fleuve , la Sarasvatî d' une importance considérable .
Quelques historiens croient que ce fleuve , comparable à l' Amazone , qui pouvait avoir jusqu' à huit kilomètres de large , est la Sarasvatî décrite dans les Vedas .
Ils mettent également en avant qu' il n' y a aucune mention dans les Vedas d' une quelconque migration vers l' Inde depuis une terre originelle .
On trouve un grand nombre de sites archéologiques de la civilisation de la vallée de l' Indus le long de ce lit asséché , en fait en plus grand nombre que le long de l' Indus , suggérant que la civilisation de la vallée de l' Indus aurait pu s' épanouir entre ces deux fleuves .
C' est cet assèchement , et non une invasion d' un peuple étranger , qui expliquerait le déclin de la civilisation de l' Indus .
La civilisation de vallée d' Indus est ainsi identifiée à la civilisation védique .
De plus des indices archéologiques , des éléments de la culture des Vedas semblent en contradiction avec un style de vie nomade , comme , par exemple , l' utilisation de la métallurgie .
Peu d' éléments d' une civilisation aussi manifestement urbaine ( par exemple , les structures des temples , système de collecte des eaux usées ) sont décrits dans les Vedas .
Elle ignorait totalement le cheval , alors que cet animal est présent dans les Vedas .
Une divinité de premier plan des Vedas est Indra , et c' est un dieu guerrier , or les hommes de l' Indus semblent avoir été plutôt pacifiques .
Il y a très peu de différence entre le védique ( langue des Vedas , qui est un sanskrit archaïque ) et la langue de l' Avesta , le texte iranien le plus ancien .
Au contraire , l' Inde a souvent été envahie par des peuples venant de l' Asie centrale , qui étaient attirés par la richesse de sa terre .
Il est vaincu par un dieu armé de la foudre , Indra en Inde ou Perun en Russie .
Des similitudes ont été observées entre un grand texte épique indien , le Mahabharata , et l' Iliade ( cf .
Les trois premières castes de l' Inde , celles des brahmanes , des kshatriya et des vaisya , dont l' existence est attestée dans les Vedas ( mais qui n' étaient pas alors aussi rigides que maintenant ) , correspondent aux divisions de la société observées chez les Celtes .
Ce type de rapprochement à conduit Georges Dumézil à élaborer sa théorie des trois fonctions , système de pensée propre aux peuples indo-européens .
En le tuant , Indra a libéré les eaux et a permis au soleil de monter au ciel .
Il convient de noter que si la linguistique permet de prouver l' origine extérieure du sanskrit , elle ne permet pas de dire comment les locuteurs du sanskrit sont arrivés en Inde : de manière progressive ou violente .
Mais les fouilles des cités de l' Indus n' ont révélé aucune trace de destruction .
À cette époque , la civilisation de la vallée de l' Indus sombre dans la décadence , probablement à cause d' inondations , de tremblements de terre , de maladies , du changement du cours du fleuve Sarasvatî , ou d' une combinaison de ces raisons .
Tout laisse à penser que la migration aryenne vers l' Inde a été lente et progressive , étalée sur de nombreux siècles , un point de vue développé par l' indianiste Max Müller .
Elle constitue également dans cette perspective un pan de la démonstration de Gobineau sur la nécessité historique de la ségrégation .
