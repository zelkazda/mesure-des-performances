Il ne fera que les deux premiers tomes de la série , et passera le relais au dessinateur Didier Convard .
En 1979 , il démarre Les Passagers du vent dans la revue Circus , série dont le tome 1 paraît en janvier 1980 .
En 1993 , il entame dans A suivre , La Source et la sonde , un récit futuriste réalisé en collaboration avec Claude Lacroix .
Tant par son souci d' authenticité ( il aime par exemple reconstituer ses différents décors sous la forme de véritables maquettes ) que par son sens de la narration , du découpage , des dialogues et de la mise en couleurs , François Bourgeon est déjà une figure emblématique du monde du 9 e art .
( En collaboration avec Claude Lacroix )
