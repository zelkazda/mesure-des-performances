Béziers est une commune française située dans le département de l' Hérault et la région Languedoc-Roussillon .
Par sa population , Béziers est la seconde ville du département de l' Hérault et la quatrième de la région Languedoc-Roussillon .
La ville est située dans la plaine héraultaise , dominant l' Orb et le canal du Midi dans un site pittoresque au sein du plus grand vignoble du monde .
Bénéficiant d' une superficie communale importante ( 9548 hectares ) , Béziers est la commune la plus étendue du département de l' Hérault .
Au nord , elles débouchent sur le théâtre , édifice inauguré en 1844 , typique du style des théâtres dits " à l' italienne " ; au sud , sur le parc dit Plateau des Poètes .
Béziers possède en outre sur son territoire , l' œuvre maitresse du Canal du Midi voulu par Pierre-Paul Riquet , son inventeur , à savoir les neuf écluses de Fonserannes , site classé patrimoine mondial de l ' humanité par l' UNESCO , tout comme le canal et le Pont-canal de l' Orb .
Cette problématique , excessivement passionnante et passionnée devrait alimenter encore pour de nombreuses années , au gré des opportunités de fouilles , les débats scientifiques dont les implications dépassent largement le seul cadre régional .
Voir article Arnaud Amaury . )
La cathédrale de Béziers est reconstruite , Béziers intégrée au domaine royal en 1247 .
Béziers n' est pas inquiétée pendant la guerre de Cent Ans .
Elle sert de base arrière pendant toutes les guerres de l' époque moderne : surtout contre les Habsbourg .
Au cours du XVIII e siècle , Béziers prospère , notamment grâce à la culture de la vigne qui lui permet d' être un important centre de négoce d' alcool .
Au moment de la Révolution , la ville ne prend pas part au mouvement des fédéralistes .
De 1790 à 1800 , Béziers est le chef-lieu du district de Béziers .
En 1851 , Béziers est une des seules villes à se révolter contre le coup d' État du prince-président Louis-Napoléon Bonaparte .
En 1907 , Béziers est l' épicentre de la révolte des vignerons , et des soldats mutins s' y installent avant d' être envoyés à Gafsa , en Tunisie .
Après la Seconde Guerre mondiale sonne l' heure du déclin pour Béziers .
Pour autant , Béziers a bénéficié depuis les années 1960 de l' essor touristique du golfe du Lion .
Béziers est chef-lieu de quatre cantons :
Béziers est le siège d' un tribunal d' instance et d' un tribunal de grande instance .
