Il aimera à dire plus tard qu' il est un vrai parisien comme ses grand-parents , tous quatre nés à Paris .
La famille , grands-parents paternels compris , s' installe au Havre en Normandie vers 1845 , l' année de ses cinq ans .
De manière précoce , il développe un goût pour le dessin et il suit avec intérêt le cours d' Ochard , un ancien élève de David .
Ses premiers dessins sont des caricatures de personnages ( professeurs , hommes politiques ) dont Monet " enguirlande la marge de ses livres ... en déformant le plus possible la face ou le profil de ses maîtres " selon ses propres termes .
C' est là qu' il va faire sa connaissance , déterminante pour sa carrière artistique , et il dira plus tard : " Si je suis devenu un peintre , c' est à Eugène Boudin que je le dois. "
Il conseille aussi à son jeune comparse à quitter Le Havre pour Paris dans le but d' y prendre des cours et d' y rencontrer d' autres artistes .
En 1861-62 , Monet sert dans l' armée en Algérie .
En 1862 , il se lie d' amitié avec Johan Barthold Jongkind , lors de son séjour à Sainte-Adresse et à Honfleur .
La même année , il commence à étudier l' art avec Charles Gleyre à Paris , où il rencontre Pierre-Auguste Renoir avec qui il fonde un mouvement artistique qui s' appellera plus tard impressionniste .
Il se lie également d' amitié avec le peintre Frédéric Bazille avec qui il entretient une importante correspondance et qui mourra au champ d' honneur en 1870 .
Un de ses modèles , Camille Doncieux , deviendra quelques années plus tard son épouse .
Ils emménagent dans une maison à Argenteuil , près de la Seine , après la naissance de leur premier enfant .
Trois ans plus tard , Monet loue le pressoir et son clos normand à Giverny près de Vernon ( Eure ) et s' y installe alors définitivement .
En 1884 , commence sa longue amitié avec l' écrivain Octave Mirbeau , qui est désormais son chantre attitré et contribua à sa reconnaissance .
En 1892 , Monet épouse Alice Hoschedé qui était sa maîtresse sans doute depuis 1875 ou 1876 , et avec qui il vivait depuis l' été 1878 .
Cette grande famille nécessite l' aménagement d' une grande maison , comme ce fut le cas avec Giverny .
Entre 1892 et 1894 , Monet peint une série de peintures de la cathédrale de Rouen , à partir de 3 emplacements distincts en face de l' édifice et à différentes heures du jour .
Vingt vues de la cathédrale sont exposées à la galerie de Durand-Ruel en 1895 .
Il avait rencontré , à cette occasion , le peintre américain Whistler ( 1834-1903 ) , également influencé par Turner , avec lequel il s' était lié d' amitié .
Ce qu' il avait vu à Londres l' incita à y revenir plusieurs fois .
Un de ceux-ci a été vendu 15,8 millions € ( 21,1 millions de dollars canadiens ) ( frais compris ) en novembre 2004 chez Christie 's à New York .
Monet aimait particulièrement peindre la nature contrôlée : son propre jardin , ses nymphéas , son étang et son pont , que le passionné des plantes qu' il était avait patiemment aménagés au fil des années .
Il a également peint les berges de la Seine .
En 1914 , Monet commence une nouvelle grande série de peintures de nymphéas , sur la suggestion de son ami Georges Clemenceau .
À la fin de sa vie , Monet souffrait d' une cataracte qui altéra notablement sa vue .
Claude Monet est décédé le 5 décembre 1926 et est enterré dans le cimetière de l' église de Giverny .
1894 : Cathédrale de Rouen .
Les tableaux de Claude Monet sont très disputés aux enchères .
