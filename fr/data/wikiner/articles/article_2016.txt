L ' Écu de Sobieski est une petite constellation qui se trouve juste à l' est de la Queue du Serpent .
Il s' agit de la 5 e plus petite constellation , mais sa position sur la Voie lactée lui permet de posséder un certain nombre d' objets célestes intéressants .
Cette constellation est l' une des seules ( avec la Chevelure de Bérénice ) qui doive son nom à un personnage historique , en l' occurrence le roi Jean III Sobieski de Pologne .
Il conduisit la défense de la Pologne et de Vienne contre les armées de l' Empire ottoman et gagna une bataille importante le 12 septembre 1683 .
Sept ans plus tard , l' astronome polonais Johannes Hevelius nomma cette petite partie du ciel coincée entre l' Aigle et le Sagittaire en son honneur .
La constellation se repère assez facilement à partir de la queue de l' Aigle .
En prolongeant cette direction , on tombe après 6 ou 7 degrés sur ν du Serpentaire/Ophiuchus , qui tient la queue du Serpent .
20 fois plus grande que le Soleil , 130 fois plus lumineuse , elle est légèrement variable , mais de manière aléatoire .
δ Scuti est le prototype d' un type d' étoiles variables qui sont soumises à de petites pulsations sur plusieurs périodes de quelques heures qui se superposent , causant une variation minime de leur luminosité .
β Cassiopeiae ( Caph ) est la plus brillante de ces étoiles .
δ Scuti est une étoile géante qui passe de la magnitude 4,60 à la magnitude 4,70 suivant deux pulsations principales de 4,65 et 4,48 heures , sur lesquelles s' ajoutent de plus petites pulsations de 2,79 , 2,28 , 2,89 et 20,11 heures .
Riche en métaux , elle est 2,2 à 2,4 fois plus massive que le Soleil et tourne sur elle-même 15 fois plus vite que celui-ci .
δ Scuti est une étoile multiple et possède deux compagnons , situés à 870 et 3 000 ua .
