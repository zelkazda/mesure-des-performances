Ethernet est un protocole de réseau local à commutation de paquets .
Depuis les années 1990 , on utilise très fréquemment Ethernet sur paires torsadées pour la connexion des postes clients , et des versions sur fibre optique pour le cœur du réseau .
Cette configuration a largement supplanté d' autres standards comme le Token Ring , FDDI et ARCNET .
Depuis quelques années , les variantes sans-fil d' Ethernet ont connu un fort succès , aussi bien sur les installations personnelles que professionnelles .
Dans un réseau Ethernet , le câble diffuse les données à toutes les machines connectées , de la même façon que les ondes radiofréquences parviennent à tous les récepteurs .
L' Ethernet a originellement été développé comme l' un des projets pionniers du Xerox PARC .
Une histoire commune veut qu' il ait été inventé en 1973 , date à laquelle Bob Metcalfe écrivit un mémo à ses patrons à propos du potentiel d' Ethernet .
Metcalfe affirme qu' Ethernet a en fait été inventé sur une période de plusieurs années .
Metcalfe a quitté Xerox en 1979 pour promouvoir l' utilisation des ordinateurs personnels et des réseaux locaux , et a fondé l' entreprise 3Com .
Il réussit à convaincre DEC , Intel et Xerox de travailler ensemble pour promouvoir Ethernet en tant que standard .
Ethernet était à l' époque en compétition avec deux systèmes propriétaires , Token Ring et ARCnet , mais ces deux systèmes ont rapidement diminué en popularité face à l' Ethernet .
Pendant ce temps , 3Com est devenue une compagnie majeure du domaine des réseaux informatiques .
L' Ethernet est basé sur le principe de membres ( pairs ) sur le réseau , envoyant des messages dans ce qui était essentiellement un système radio , captif à l' intérieur d' un fil ou d' un canal commun , parfois appelé l' éther .
Au départ développée durant les années 1960 pour ALOHAnet à Hawaii en utilisant la radio , la technologie est relativement simple comparée à Token Ring ou aux réseaux contrôlés par un maître .
Les ordinateurs connectés sur l' Ethernet doivent donc filtrer ce qui leur est destiné ou non .
Ce type de communication " quelqu' un parle , tous les autres entendent " d' Ethernet est une de ses faiblesses , car , pendant que l' un des nœuds émet , toutes les machines du réseau reçoivent et doivent , de leur côté , observer le silence .
Il y a quatre types de trame Ethernet :
La version 1 originale de Xerox possède un champ de 16 bits identifiant la taille de trame , même si la longueur maximale d' une trame était de 1500 octets .
Toutes les trames 802.3 ont un champ LLC .
Attention il existe d' autres types de trames Ethernet qui possèdent d' autres particularités .
La section ci-dessous donne un bref résumé de tous les types de média d' Ethernet .
10 Gigabit Ethernet supporte seulement le mode full duplex , beaucoup de liens sont en mode point à point bien que du routage à ce débit commence à apparaître .
Nous noterons l' efficacité de cet encodeur ( 3 % ) par rapport à celui du mode Gigabit Ethernet qui apporte quant à lui un sur débit de 25 % .
