Marié puis séparé de sa femme et occupant diverses fonctions , il se lance alors dans l' écriture et après le roman pastoral La Galatea en 1585 , c' est en 1605 qu' il publie la première partie de ce qui sera son chef-d'œuvre : L' ingénieux hidalgo Don Quichotte de la Manche dont la deuxième partie ne paraît qu' en 1615 .
Sa parodie grandiose des romans de chevalerie et la création des personnages mythiques de Don Quichotte , Sancho Panza ou Dulcinée ont fait de Cervantes la plus grande figure de la littérature espagnole et l' un des plus grands écrivains de tous les temps .
On suppose que Miguel de Cervantes est né à Alcalá de Henares .
Au contraire , Jean Canavaggio insiste sur le fait que cette ascendance " n' est pas prouvée " et le compare à Mateo Alemán pour qui les origines sont démontrées par des documents .
En 1556 il se rend à Cordoue pour recevoir l' héritage de Juan de Cervantes , grand-père de l' écrivain , et fuir ses créanciers .
Il n' existe pas de données précises sur le début des études de Miguel de Cervantes , qui sans doute , ne sont jamais arrivées au niveau universitaire .
On pense qu' il aurait pu étudier à Valladolid , Cordoue ou Séville .
En 1566 , il s' installe à Madrid .
C' est à cette époque que Cervantes prend goût au théâtre en assistant aux représentations de Lope de Rueda et , comme il le déclare dans la seconde partie de Don Quichotte , par la bouche du personnage principal , " se le iban los ojos tras la farándula " ( il adorait le monde du théâtre ) .
Si cela concernait réellement Cervantes , ce pourrait être le motif qui le fit fuir en Italie .
Il est arrivé à Rome en décembre de la même année .
Il l' a suivi à Palerme , Milan , Florence , Venise , Parme et Ferrare .
Le 7 octobre 1571 il participe à la bataille de Lépante , du côté de l' armée chrétienne dirigée par Don Juan d' Autriche , " fils du foudre de guerre Charles Quint , d' heureuse mémoire " et demi-frère du roi .
Ces blessures n' ont pas été trop graves , après six mois de séjour dans un hôpital de Messine , Cervantes renoue avec sa vie militaire en 1572 .
Plus tard , il a parcouru les villes principales de Sicile et Sardaigne , de Gênes et de la Lombardie .
Il resta finalement deux ans à Naples , jusqu' en 1575 .
Cervantes s' est ensuite toujours montré très fier d' avoir participé à la bataille de Lépante , qui fut pour lui comme il l' a écrit dans le prologue de Don Quichotte , " le plus grand évènement que virent les siècles passés , présents , et que ceux qui viennent ne peuvent espérer " .
Pendant ses cinq ans d' emprisonnement , Cervantes , en homme à l' esprit fort et motivé , essaya de s' échapper à quatre occasions .
La première tentative de fuite fut un échec , car le complice maure qui devait conduire Cervantes et ses compagnons à Oran les a abandonnés dès le premier jour .
Les prisonniers durent retourner à Alger , où ils furent enfermés et mieux gardés qu' avant .
Pourtant , la mère de Cervantes avait réussi à réunir une certaine quantité de ducats , avec l' espoir de pouvoir sauver ses deux fils .
Celui-ci rentra alors en Espagne .
Cervantes se déclare alors comme le seul responsable de l' organisation de l' évasion et d' avoir convaincu ses compagnons de le suivre .
La troisième tentative , conçue par Cervantes dans le but d' arriver par la terre jusqu' à Oran .
Les lettres dénonçaient Miguel de Cervantes et montraient qu' il avait tout monté .
La dernière tentative de fuite s' est produite grâce à une importante somme d' argent que lui donna un marchand valencien qui était à Alger .
Cervantes acheta une frégate capable de transporter soixante captifs chrétiens .
Ensuite , il décida de l' emmener à Constantinople , d' où la fuite deviendrait une entreprise quasi impossible à réaliser .
Une fois encore , Cervantes assuma toute la responsabilité .
Frère Antonio partit dans une expédition de sauvetage .
Grâce aux cinq cents écus si durement réunis , Cervantes est libéré le 19 septembre 1580 .
Le 24 octobre il revient enfin en Espagne avec d' autres captifs sauvés également .
Il arrive alors à Dénia , d' où il partit pour Valence .
En novembre ou décembre , il retrouve sa famille à Madrid .
Elle est connue comme la grotte de Cervantes . .
Après deux ans de mariage , Cervantes entreprend de grands voyages à travers l' Andalousie .
Il est probable que La Galatea fut écrite entre 1581 et 1583 , c' est sa première œuvre littéraire remarquable .
Elle fut publiée à Alcalá de Henares en 1585 .
La Galatea est divisée en six livres , mais seule la " première partie " fut écrite .
Cervantes a promis de donner une suite à l' œuvre ; pourtant , elle ne fut jamais imprimée .
Non sans autodérision , Cervantes place dans la bouche de l' un des personnages de Don Quichotte ce commentaire de la Galatée : " Il y a bien des années , reprit le curé , que ce Cervantes est de mes amis , et je sais qu' il est plus versé dans la connaissance des infortunes que dans celle de la poésie .
Dans le prologue de la Galatée , l' œuvre est qualifiée d ' " églogue " et l' auteur insiste sur l' affection qu' il a toujours eue pour la poésie .
On peut encore y deviner les lectures qu' il a pu avoir quand il était soldat en Italie .
En 1587 , il voyage à travers l' Andalousie en tant qu' intendant de l' Invincible Armada .
Il parcourt à nouveau le chemin entre Madrid et l' Andalousie , qui passe par la Castille et la Manche .
Il s' établit alors à Séville .
C' est là qu' il aurait , selon le prologue de l' œuvre , imaginé le personnage de Don Quichotte .
En 1605 , il publie la première partie de ce qui sera son chef-d'œuvre : L' ingénieux hidalgo Don Quichotte de la Manche .
La seconde partie ne paraît pas avant 1615 : L' ingénieux chevalier don Quichotte de la Manche .
Les deux œuvres lui donnent un statut dans l' histoire de la littérature universelle , aux côtés de Dante Alighieri , William Shakespeare , François Rabelais et Goethe comme un auteur incontournable de la littérature occidentale .
Et plus précisément dans Illusions perdues où il qualifie Don Quichotte de sublime
Entre les deux parties du Don Quichotte , paraissent en 1613 les Nouvelles exemplaires .
Elle apparaît dans la Galatea et dans Don Quichotte .
Ce roman grec , qui prétend concurrencer le modèle classique grec d' Héliodore , connut quelques éditions supplémentaires à son époque ; mais elle fut oubliée et effacée par le triomphe indiscutable du Don Quichotte .
Cervantes utilise un groupe de personnages comme fil conducteur de l' œuvre , au lieu de deux .
D' autre part , une autre œuvre importante de Cervantes , les Nouvelles exemplaires , démontre la largeur d' esprit et son désir d' expérimenter les structures narratives .
On a donné à Madrid en 1805 une collection de ses œuvres , 16 vol .
Le Don Quichotte a été souvent imprimé :
Le Don Quichotte a été plusieurs fois traduit en français :
Jean-Pierre Claris de Florian ( 1755-1794 ) a écrit une traduction très libre et personnelle de Don Quichotte et Galatée .
Marie-Nicolas Bouillet et Alexis Chassang ( dir .
