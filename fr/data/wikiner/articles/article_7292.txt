Le mot pédérastie désigne à l' origine une institution morale et éducative de la Grèce antique , bâtie autour de la relation particulière entre un homme mûr et un jeune garçon .
Une minorité de lettrés , dont Voltaire , continua à utiliser le mot dans son sens étymologique pour désigner l' amour des garçons .
Comme lors des siècles précédents , seuls quelques auteurs cultivés continuaient à utiliser pédérastie dans son sens étymologique , osant parfois prendre la défense de cet amour , tel Proudhon ( 1809 -- 1865 ) , qui fut aussi un virulent pourfendeur de la " pédérastie " .
Il aura fallu cette lente pénétration des concepts d' homosexualité et d' homosexuel dans le langage courant pour que les termes pédérastie et pédéraste puissent opérer un retour progressif qui dépasse la sphère de quelques lettrés ( auteurs souvent eux-mêmes pédérastes comme André Gide , Henry de Montherlant ou encore Roger Peyrefitte ) .
Les uns pensent qu' il conviendrait en toute rigueur de réserver l' emploi du mot pédérastie à l' institution de la Grèce antique , et préfèrent parler d' homosexualité pédérastique ou d' homosexualité de type pédérastique pour désigner l' attirance amoureuse et/ou sexuelle que certains hommes éprouvent pour les adolescents .
Ces termes , sinon forgés , du moins réactivés , par le groupe des poètes uraniens , doivent cette popularité inattendue au développement du World Wide Web , lieu de rassemblement de toutes sortes de minorités .
Or le Web fut d' abord anglophone , et beaucoup de termes anglais ont passé tels quels dans les autres langues , sans faire l' objet de traductions .
Il semblerait en outre que sous l' étiquette boylove la pédophilie soit plus représentée sur le Web que la pédérastie .
Ainsi , sur Internet , un nombre croissant d' individus francophones se détournent-ils de l' anglicisme boylover pour lui préférer celui de pédéraste .
L' exemple le plus connu de pédérastie codifiée et institutionnelle est celui de la Grèce antique .
Les Grecs anciens semblent avoir été les premiers à s' être exprimés au sujet de la pédérastie , à l' avoir étudiée et à l' avoir organisée et érigée en institution dans certaines cités .
Divers indices permettent néanmoins de supposer que le modèle pédérastique de la Grèce antique a évolué à partir de rites initiatiques des sociétés de chasseurs-cueilleurs du paléolithique supérieur .
Ce couple tenait sa légitimité de nombreux équivalents symboliques ou mythologiques en la personne des dieux ou des héros ( Zeus et Ganymède , Apollon et Hyacinthe , Apollon et Cyparisse , Héraclès et Iolaos , Thésée et Pirithoos , Achille et Patrocle ) .
À Sparte , il était directement institué par la loi .
Les termes désignant l' homme et le garçon pouvaient varier d' une cité à l' autre : par exemple , erastes ( amant ) et eromenos ( aimé ) à Athènes , eispnelas ( inspirateur ) et aites ( auditeur ) à Sparte .
Sparte requérait de tous ses citoyens de nouer une relation pédérastique .
Mais l' homme devait au préalable gagner l' affection du garçon , à la différence du cas de la Crète ou même de celui d' Athènes et de nombreuses autres cités grecques , où cette affection , bien que souhaitée , n' était pas requise .
À Leuctres , ils écrasèrent Sparte .
Il fallut attendre Philippe II de Macédoine , père d' Alexandre le Grand , pour en venir à bout .
Après les avoir enfin vaincus à Chéronée , il leur rendit les honneurs militaires .
Platon fut de ceux qui s' élevèrent contre le dévoiement de la pédérastie institutionnelle .
Par exemple Athenaeus , le rhétoricien et grammairien grec , répétant des affirmations faites par Diodorus , a écrit que : " Les Celtes , bien qu' ils aient des femmes très belles , apprécient de jeunes garçons davantage , de sorte que certains d' entre eux avaient souvent deux amoureux à dormir avec eux sur leurs lits en peau de bête " , , .
Selon Aristote encore , " les Celtes sont ouverts et approuvent les jeux amoureux masculins " .
Les concepts d' homosexualité ou d' hétérosexualité s' appliquent mal à la sexualité telle qu' elle était vécue et pratiquée dans le Japon féodal .
Aucune source ne nous renseigne sur l' existence éventuelle de pratiques homosexuelles dans le Japon de l' Antiquité .
Le bonze Kûkai ( 774-835 ) , fondateur d' une communauté monastique , passe pour avoir introduit l' homosexualité au Japon , à son retour de Chine en 806 -- réputation que certains considèrent comme induite ou amplifiée par les missionnaires de François Xavier .
À la fin du XVI e siècle , le Japon en compte environ quatre-vingt-dix mille .
À Rome , à l' époque historique , les relations pédérastiques ont cessé de relever de l' homosexualité initiatique , si tant est qu' elles y aient jamais eu ce caractère .
Elles furent de moins en moins acceptées au fil des siècles , atteignant leur zénith sous le règne de l' empereur Hadrien , qui après la mort prématurée de son jeune amant Antinoüs fit ériger des statues à son effigie dans tout l' empire , allant jusqu' à l' élever au rang de dieu .
À l' époque , Florence en particulier était renommée pour la fréquence des relations pédérastiques qui s' y nouaient .
La réputation de Florence était telle que les Allemands créèrent le verbe florenzen pour désigner le fait d' avoir des relations sexuelles avec un jeune garçon .
La Nuit des Rois , pièce de William Shakespeare , se délecte de cette ambiguïté sexuelle caractérisant le théâtre élisabéthain : une jeune femme déguisée en page s' éprend de son seigneur , tandis qu' une dame tombe amoureuse dudit page , qui après avoir recouvré sa véritable identité épousera pour finir son ancien seigneur -- autant de situations impliquant une jeune travestie , incarnée à la scène par un garçon .
Même si l' on fait abstraction de certains propos attribués à Christopher Marlowe dans le cadre de son procès pour athéisme ( " Ceux qui n' aiment ni le tabac ni les garçons sont des idiots " ) , plusieurs passages des pièces et poèmes de l' auteur laissent supposer chez lui des inclinations pédérastiques .
Les deux garçons sur lesquels respectivement Orphée et Zeus jettent leur dévolu sont ici des nourrissons : cette assimilation de la pédérastie à une représentation extrême et prédative de la pédophilie , qui permet de mieux les condamner , demeure d' ailleurs aujourd'hui encore des plus courantes .
Dans la version de ce tableau conservée à Rome , le satyre/démon à la peau brune , à terre aux côtés de l' amour vaincu , apparaît sous les traits du Caravage lui-même , jetant vers le spectateur des regards effrayés .
Le procès d' Oscar Wilde donne une publicité nouvelle au désir homophile , en le faisant sortir de l' ombre et des manuels médicaux .
Si l' homosexualité que défend alors Wilde n' est déjà plus tout à fait la pédérastie ( l' écrivain parle bien de relations entre hommes ) , " l' amour qui n' ose dire son nom " reste néanmoins structuré par la différence d' âge et se réclame de modèles pédérastiques comme Platon , Michel-Ange , Shakespeare .
La popularité d' Oscar Wilde n' y résistera pas .
Si plusieurs garçons se succéderont à la place du Boy Wonder , les deux derniers Robin furent des filles .
Par ses choix esthétiques et quelques répliques placées dans la bouche des personnages , le réalisateur Joel Schumacher réintroduit au cinéma , de façon sous-jacente , le thème de l' homosexualité de l' homme chauve-souris : son Robin , âgé d' une vingtaine d' années , symbolise de manière éloquente le passage d' une homosexualité pédérastique pluriséculaire à l' homosexualité androphile contemporaine .
Dans le cas d' une erreur d' appréciation de sa part , celle-ci pourrait peut-être s' expliquer par la ( très relative ) légitimité qu' apportaient aux amoureux des garçons les modèles d' une Antiquité prestigieuse , leur permettant ainsi une certaine visibilité ; les hommes aimant d' autres hommes ne pouvaient quant à eux s' appuyer sur une tradition aussi riche .
Le symétrique du couple pédérastique ( une femme et une jeune fille ) semble avoir historiquement existé dans l' Antiquité , en la personne de Sappho et de ses élèves , sans toutefois qu' il y ait eu d' équivalent féminin de l' institution pédérastique .
Ainsi , jusqu' en 2002 , l' Autriche sanctionnait les relations homosexuelles masculines impliquant des garçons de quatorze à dix-huit ans , mais ni les relations hétérosexuelles ni les relations homosexuelles féminines impliquant des adolescent ( e ) s de la même tranche d' âge .
En France , jusqu' en mars 2005 , il était de 15 ans pour les filles et de 18 ans pour les garçons non émancipés -- pour lutter contre les mariages arrangés des jeunes filles , cet âge a été porté à 18 ans pour les deux sexes .
L' âge peut être différent si l' adulte est un ascendant légal du jeune homme ou dans une position d' autorité vis-à-vis de lui ( parent , professeur , éducateur ... ) : en France par exemple , l' âge est élevé à 18 ans dans ce cas .
D' autres artistes ont été pédérastes , sans toutefois que leurs attirances semblent transparaître dans leurs œuvres : ainsi Johann Rosenmüller , Jean-Baptiste Lully , Piotr Ilitch Tchaïkovski ou Charles Trenet .
