Dès 1983 , il connaît le succès avec les treize volumes de Wingman , alliance d' humour et de science-fiction avec une touche d' érotisme .
Sa seconde série à succès , Video Girl Ai , qui reprend les ingrédients qui avaient valu le succès de Wingman , est l' un des premiers mangas traduits en français .
Masakazu Katsura était l' invité d' honneur de Japan Expo 6 en juillet 2004 .
