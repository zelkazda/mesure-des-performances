La Baleine est une énorme constellation située dans le ciel méridional parmi d' autres constellations " aquatiques " .
Bien que la Baleine soit la quatrième plus grande constellation du ciel , elle ne possède que des étoiles au plus de 2 e magnitude .
Il est possible que le monstre ait été par la suite assimilé à la baleine ayant avalé Jonas dans la Bible .
Elle était l' une des 48 constellations identifiées par Ptolémée .
Initialement , le " monstre marin " qu' est Cetus était représenté comme une sorte de grand encornet , avec une bouche armée d' un bec et de grands tentacules ( β Cet ) .
Cet anneau était traditionnellement la bouche du monstre ( d' où les noms arabes de α ) , et figure bien à présent les nageoires caudales .
Côté β Cet , la constellation s' organise autour de deux quadrilatères .
Mira se repère sur l' axe α-β Cet .
Si les Poissons sont visibles , elle est située dans l' axe des " cordes " des poissons .
Mira ( ο Cet ) est certainement l' étoile la plus intéressante de la constellation .
Située à environ 400 années-lumière , Mira ( la " merveilleuse " ) est la première étoile variable qui ait été découverte ( en 1596 ) .
Mira est une étoile variable géante rouge à longue période .
Mira est un système multiple .
Néanmoins , en raison de sa taille , elle est à son maximum 15 000 fois plus brillante que le Soleil .
Mira est une étoile en fin de vie , ayant épuisé ses réserves d' hélium , fusionnées en carbone et en oxygène .
Mira est une étoile instable , dont les changement de luminosité sont causés par des modifications périodiques de la taille de l' étoile .
Cette matière a été libérée par Mira au cours des 30000 dernières années .
Rappelons que Mira se déplace à une vitesse de 130 km/s dans la Voie lactée , soit 4 fois plus vite que le Soleil .
Deneb Kaitos est une étoile géante en fin de vie , 17 fois plus grande que le Soleil .
Sa désignation comme α , alors qu' elle est moins lumineuse que Deneb Kaitos ( ou que Mira par intermittence ) , tient probablement à sa place dans la constellation , à la tête du monstre .
Menkar est une géante rouge de température peu élevée .
À terme , elle devrait vraisemblablement connaître la même évolution que Mira .
Dans cette grande constellation , plusieurs autres étoiles portent un nom : Baten Kaitos ( ζ Cet ) ( le " ventre du monstre " ) , une géante orange , Kaffaljidhm ( γ Cet ) , Deneb Algenubi ( η Cet ) et Deneb Kaitos Shemali ( ι Cet ) .
Puisque la Baleine se trouve loin du plan de la Voie lactée , les galaxies qui y sont présentes ne sont pas obscurcies par la poussière de la nôtre .
L' astéroïde ( 4 ) Vesta fut d' ailleurs découvert dans cette constellation en 1807 .
