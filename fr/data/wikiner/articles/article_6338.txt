Les fondements de la mécanique , en tant que science et au sens moderne du terme , sont posés par Galilée .
Les contributions d' Archimède à la construction d' une science mécanique , alors encore en devenir , sont absolument fondamentales .
Ce sont notamment à partir de ses réflexions sur l' équilibre des corps que Galilée posera les bases de la mécanique moderne .
C' est Aristote qui posera le premier les bases d' une véritable théorie mécanique .
La théorie du mouvement d' Aristote comporte de nombreuses difficultés dont le Stagirite était bien conscient .
Les idées d' Aristote domineront cependant largement la manière de concevoir le mouvement jusqu' au début du second millénaire , en l' absence de théorie alternative véritablement plus crédible .
La notion d' impetus vise en particulier à répondre aux apories de la théorie d' Aristote .
Il faut attendre Galilée pour dépasser ces conceptions erronées du mouvement .
