Le symbolisme est un mouvement littéraire et artistique apparu en France et en Belgique vers 1870 , en réaction au naturalisme et au mouvement parnassien .
L' influence de Stéphane Mallarmé est ici considérable , ce qui entraîne la poésie vers l' hermétisme .
En littérature , le mouvement du symbolisme trouve ses origines dans Les Fleurs du mal ( 1857 ) de Charles Baudelaire .
L' esthétique symboliste fut développée par Stéphane Mallarmé et Paul Verlaine durant les années 1860 et 1870 .
La traduction en français par Baudelaire de l' œuvre d' Edgar Allan Poe , d' une influence considérable , fut à l' origine de plusieurs tropes et images du symbolisme .
Le roman À rebours ( 1884 ) de Joris-Karl Huysmans contient plusieurs thèmes qui furent par la suite associés à l' esthétique symboliste .
Le roman fut imité par Oscar Wilde dans plusieurs passages du Portrait de Dorian Gray .
Paul Adam était le plus prolifique et représentatif romancier symboliste .
Une autre fiction étant parfois considérée comme symboliste sont les contes misanthropiques ( et surtout , misogynes ) de Jules Barbey d' Aurevilly .
Le premier roman de Gabriele D' Annunzio fut aussi écrit dans un esprit symboliste .
Plusieurs écrivains et critiques symbolistes étaient positifs à l' égard de la musique de Richard Wagner .
L' esthétique symboliste eu une influence importante sur le travail de Claude Debussy .
Son œuvre clé , la Prélude à l' après-midi d' un faune , était inspirée par un poème de Stéphane Mallarmé , L' Après-Midi d' un faune .
