La capitale est Wiesbaden , la plus grande ville est Francfort-sur-le-Main .
Les rivières les plus importantes sont la Fulda , la Lahn , le Main et le Rhin .
La population se concentre dans la partie sud de la Hesse dans la Région Rhin-Main .
La chaîne de montagnes entre le Main et le Neckar est appelé l' Odenwald .
La région Rhin-Main , grâce à la Ruhr , a la plus grande densité d' industries en Allemagne .
L' industrie du cuir à Offenbach et l' aéroport de Francfort .
En mai 2008 , la Hesse comptait 204.421 chômeurs .
La CDU a perdu en janvier 2008 la majorité absolue qu' elle détenait depuis 2003 .
Grande perte pour le SPD , le scrutin permet à Roland Koch de se réaffirmer en Hesse , après avoir failli perdre son poste de ministre-président en janvier 2008 .
Le ministre-président en Hesse est le chef
Le Land de Hesse , en tant que tel , ne disposait pas d' armoiries qui lui étaient propres .
Il s' est contenté de s' inspirer , pour ses armes , des armes qui étaient celles du grand-duché de Hesse avant 1918 , de les transformer au passage , en escamotant l' épée d' or tenue en patte avant droite et en " couronnant " l' écu d' une manière qui n' est pas très conforme aux règles de l' héraldique .
Hesse est subdivisé en 3 districts de Cassel , Gießen et Darmstadt .
En Hesse il y a 21 arrondissements ruraux comprenant 426 communes dont 189 villes , et 5 arrondissements-villes .
