Basse-sur-le-Rupt est une commune du nord-est de la France , dans le département des Vosges et la région Lorraine .
Située en moyenne montagne , la commune de Basse-sur-le-Rupt s' allonge sur une dizaine de kilomètres , le long de la départementale 34 qui relie Vagney à La Bresse , passant de 409 m à 1060 m d' altitude .
Basse-sur-le-Rupt fut marquée durant la Seconde Guerre mondiale .
