GNU Privacy Guard ( GPG ou GnuPG ) est un logiciel qui permet à ses utilisateurs de transmettre des messages signés et/ou chiffrés .
GPG est un remplacement libre de la suite PGP de logiciels cryptographiques ( plus précisément , de cryptographie asymétrique ) .
Il est disponible selon les termes de la GNU GPL .
GPG est un logiciel très stable , apte à la production .
Ainsi , il est généralement inclus dans les systèmes d' exploitation libres , comme les BSD ou GNU/Linux .
Le risque principal de GPG , comme pour tous les systèmes à clé publique , est que la clé privée doit être enregistrée quelque part .
Depuis sa version 2.0 , GPG peut être installé sur une carte à puce .
