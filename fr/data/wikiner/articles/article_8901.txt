Common Lisp est un langage fonctionnel impur de la famille Lisp .
Common Lisp est un langage de programmation multi-paradigmes qui :
Common Lisp a une pléthore de types de données , plus qu' aucun autre langage .
Common Lisp utilise des grands nombres pour représenter des valeurs numériques de taille et de précision arbitraires .
Common Lisp convertit automatiquement les valeurs numériques entre ces types de façon appropriée .
Voici la tour numérique , c' est-à-dire la hiérarchie des types numériques de Common Lisp :
Certaines implémentations modernes supportent les caractères Unicode .
Les listes de Common Lisp ne sont pas un type de donnée mais sont composées à partir de conses ( pluriel ) , parfois appelés cellules cons ou paires .
Common Lisp supporte les tableaux de dimensions arbitraires , et peut aussi redimensionner dynamiquement les tableaux .
Common Lisp a été le premier langage à objets standardisé ( en 1995 , par l' ANSI ) .
Le système de conditions de Common Lisp utilise CLOS pour définir les types des conditions pouvant survenir à l' exécution .
En Common Lisp , les fonctions sont un type de donnée .
Common Lisp a un espace de nom respectivement pour les fonctions et pour les variables .
Les autres types de données de Common Lisp comprennent :
Par exemple , la macro suivante fournit la forme de boucle until ( boucler ... jusqu' à ) , qui est familière dans un langage comme Perl .
Comme les fonctions , les macros peuvent utiliser l' ensemble du langage Common Lisp ( et bibliothèques tierces ) pour effectuer leur travail de transformation ( on les appelle pour cela des macros procédurales ) , contrairement aux macros du langage C qui ne permettent que des substitutions de chaînes de caractères au niveau du source , sans accès à l' ensemble du langage C lui-même .
Un exemple classique est l' inclusion dans le langage d' une extension permettant de faire de la programmation logique à la manière de Prolog ou encore de la programmation par contrainte ( ajout d' un nouveau paradigme de programmation ) .
Common Lisp offre la quasiquotation , représentée par le caractère ' ( dit backquote ) .
On voit par là que les macros sont déjà massivement utilisées dans l' implémentation d' un langage riche et complexe comme Common Lisp .
Les macros Common Lisp sont capables de capture de variable , une situation où des symboles situés dans le corps de la macro-expansion coincident avec des symboles du contexte appelant .
Pour cette raison elles sont parfois appelées macro " non hygiéniques " , par comparaison avec le système de " macro hygiéniques " de Scheme , qui garantit la séparation entre ces ensembles de symboles .
Common Lisp est défini par une spécification ( comme Ada et C ) plutôt que par une seule implémentation ( comme Perl ou Python ) .
Selon une erreur répandue , les implémentations de Common Lisp sont toutes des interpréteurs .
La plupart des implémentations de Common Lisp compilent les fonctions vers du code machine .
Il existe toute une communauté de programmeurs utilisateurs de Common Lisp .
La communauté qui s' est créée autour du langage Common Lisp n' y fait pas exception , en témoignent les citations suivantes :
" Common Lisp , la force d' une boule de glaise , est le patriarche bien vivant de la famille Lisp .
Common Lisp est utilisé avec succès dans de nombreuses applications commerciales :
Il existe également des applications open-source écrites en Common Lisp , telles que :
Des exemples d' utilisation par la NASA incluent :
