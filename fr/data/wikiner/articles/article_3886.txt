Le nom dérive de Niobé , la fille de Tantale .
Les États-Unis ont longtemps utilisé le nom " colombium " , du district de Columbia où le minéral a été découvert .
On trouve aussi du niobium dans le Nord-Kivu en République démocratique du Congo et au Gabon .
Madagascar devient producteur en 2008 .
Par ailleurs , il existe de grandes quantités de niobium en Afghanistan mais la faiblesse des infrastructures dans ce pays rend sa récolte très difficile .
