Shanghai est la ville la plus peuplée de Chine ( en terme de population urbaine ) et l' une des plus grandes mégapoles du monde avec plus de vingt millions d' habitants .
Dans les années 1920 et 1930 , Shanghai a été le théâtre d' un formidable essor culturel qui a beaucoup contribué à l' aura mythique et fantasmatique qui est associée à la ville depuis cette époque .
Sa croissance à deux chiffres , les 18,9 millions d' habitants de sa région urbaine , sa mutation cosmopolite et son essor culturel l' appellent à devenir une métropole mondiale , aux côtés de New York , Londres ou Paris .
Elle accueille l' Exposition universelle de 2010 .
La transcription " Shanghai " est généralement prononcée / ʃɑ̃.gaj / en français , mais en chinois mandarin le nom 上海 se prononce / ʂɑŋ.xaɪ / .
Les deux sinogrammes dans le nom " Shanghai " ( 上 , shàng ; et 海 , hǎi ) signifient littéralement " sur , au dessus de " et " mer " .
À cause du changement du littoral , les historiens chinois ont conclu que , durant la dynastie Tang , Shanghai était littéralement sur la mer , d' où l' origine du nom .
Au temps de la concession française , le nom français de la ville s' écrivait " Changhaï " .
La municipalité de Shanghai est un territoire administratif ayant le statut de province : elle comprend plusieurs villes dont Shanghai et compte environ 20 millions d' habitants .
L' agglomération ( en chinois : chengqu ) de Shanghai comptait 16,7 millions d' habitants en 2000 .
L' avenue Nanjing ( cinq kilomètres ) fut autrefois la grande artère de la concession dite étrangère .
Elle est considérée maintenant comme le vrai centre de Shanghai et elle offre souvent dans sa partie ouest , près du fleuve , le spectacle d' une indescriptible cohue de piétons .
Shanghai est situé dans un vaste delta , formé par l' embouchure du fleuve Yangzi Jiang qui se jette dans la Mer de Chine orientale .
Les basses terres qui se trouvent des deux côtés de la rivière sont composés de lœss d' alluvions , qui est formé par les sédiments du Yangzi .
Construit de boue , sillonné de canaux et de barrages le delta est l' une des zones les plus fertiles de Chine , et également son principal fournisseur de coton .
La formation du delta a renvoyé Shanghai , une ville portuaire à l' origine construite sur la mer , à 30 km à l' intérieur des terres .
Shanghai bénéficie d' un climat subtropical humide .
La municipalité de Shanghai exerce sa juridiction sur dix-neuf subdivisions : dix-huit districts et un xian .
Un district gouverne le quartier de Pudong , à l' est du Huangpu :
L' île de Chongming , située dans l' estuaire du Yangzi Jiang ( Chang Jiang ) , est gouvernée par un seul xian :
À l' origine port modeste et village de tisserands , Shanghai ne semblait pas promise à pareil essor cosmopolite .
Shanghai ne s' est pas toujours appelée Shanghai .
Américains et Français suivront , précédant les Russes et les Japonais .
Les Britanniques vainqueurs y aménagent l' un des cinq ports ouverts qui leur seront alors concédés .
Cette année eut également lieu la première réunion du conseil municipal de Shanghai , afin de gérer les concessions étrangères établies de facto .
Son parrain le plus connu , Du Yuesheng , menait ses trafics en collaborant étroitement avec la police de la concession française .
Ils établirent à Shanghai les premières usines de la ville .
C' est aussi à Shanghai que fut créé le Parti communiste chinois en 1921 et qu' ont été organisées les premières grèves ouvrières .
La plupart , coolies et ouvriers , demeurèrent dans la pauvreté et vinrent grossir les rangs du Parti communiste chinois .
Sous le régime de la République de Chine , Shanghai devint une ville spéciale en 1927 , et une municipalité en mai 1930 .
La marine japonaise bombarda la ville le 28 janvier 1932 , officiellement pour réprimer les protestations étudiantes ayant suivi l' incident de Mandchourie , déclenchant la " guerre de Shanghai " .
Disposant de forces terrestres et navales bien supérieures à l' armée chinoise , les troupes impériales prirent possession de la ville en novembre ( bataille de Shanghai ) , puis se dirigèrent vers Nankin où elles se livrèrent à un terrible carnage ( massacre de Nankin ) .
En 1938 , Shanghai fut considérée comme le cinquième port mondial ; les plus grandes firmes occidentales y étaient désormais représentées .
En 1941 , sous pression de leurs alliés nazis , les Japonais reçurent les réfugiés juifs dans un ghetto , où les maladies pullulaient , .
L' immigration juive fut finalement stoppée par les Japonais le 21 août 1941 .
Les Japonais prirent le contrôle total des concessions le 8 décembre 1941 .
En février 1943 , le gouvernement du Royaume-Uni signa avec la République de Chine un traité acceptant le principe d' une rétrocession .
En juillet de la même année , les Japonais rétrocédèrent le conseil municipal au gouvernement collaborateur chinois de Wang Jingwei .
Les huit années d' occupation , puis la victoire , en 1949 , de Mao Zedong sur les troupes du général Tchang Kaï-chek précipitèrent le déclin de la ville .
Après la victoire des communistes , la ville a été considérée comme le symbole du capitalisme étranger , elle sommeillait , et le monde l' avait presque oubliée , avant d' être revalorisée suite au mouvement de réformes de Deng Xiaoping .
Autrefois tête de pont des puissances coloniales dans une Chine agonisante , Shanghai est devenue le premier centre industriel du pays , en même temps que l' une des plus grandes métropoles du monde .
Pendant la Révolution culturelle , Shanghai connut des troubles politiques et sociaux : à la fin décembre 1966 , la municipalité fut renversée .
Le bilan de la Révolution culturelle fut considérable : 150000 logements furent confisqués rien qu' à Shanghai .
Au début des années 1990 , en une décennie , la " perle de l' Orient " est redevenue un centre économique de première importance , qui compte en 2005 pour 20 % de la production industrielle nationale pour seulement 1,5 % de la population .
En septembre 2006 , Chen Liangyu est limogé suite à un scandale de corruption .
Avant cela , le 3 décembre 2002 , la métropole chinoise a été désignée pour organiser l' Exposition universelle de 2010 , qui se tient donc , pour la première fois depuis 151 ans , dans un pays en voie de développement .
La population de Shanghai est de 19 213 200 habitants .
D' après la population totale de la municipalité , Shanghai est la troisième plus grande municipalité de la République populaire de Chine , après Chongqing et Pékin .
En RPC , une municipalité ( 直辖市 en pinyin : zhíxiáshì ) est une ville avec un statut équivalent aux provinces chinoises .
Le recensement de 2000 position la population de Shanghai à 16,738 million , dont 3,871 millions de migrants .
En 2008 , la population de résidents à long terme atteint 18,88 millions , incluant la population permanente officielle de 13,71 millions d' individus et 4,79 millions de migrants des autres provinces , notamment des provinces du Anhui , Jiangsu et Zhejiang .
On compte également un nombre important d' habitants de Taïwan qui résident à Shanghai pour les affaires ( entre environ 350 000 et 700 000 ) .
En 2009 , les communautés sud-coréennes à Shanghai atteignaient 70 000 ressortissants. .
Le revenu moyen annuel des résidents de Shanghai , basé sur les trois premiers trimestres de 2009 , est de 21871 yuans .
La ville a longtemps été l' un des principaux centres de production textile de la République populaire de Chine .
Avec le début de réformes économiques chinoises au début des années 1980 , Shanghai a d' abord été dépassé par certaines provinces du sud , telles que Guangdong .
Hong Kong constitue le principal rival de Shanghai dans le titre honorifique de plus grand centre économique en Chine .
Hong Kong possède l' avantage d' une plus grande expérience , notamment dans le secteur bancaire .
Shanghai a des liens plus étroits avec l' arrière-pays chinois et le gouvernement central de Pékin .
De plus , Shanghai possède plus de terrains pour accueillir les nouveaux investissements , alors qu' à Hong Kong , l' espace est très limité .
Depuis 1991 , la croissance économique à Shanghai est à deux chiffres .
La ville est donc la seule région de Chine dans ce cas sur une telle durée .
La croissance économique annuelle à Shanghai est actuellement d' environ 12 % .
Le PIB par habitant était d' environ 7000 dollars ( la moyenne chinoise se situe à 1800 dollars ) et constitue le troisième plus élevé du pays , derrière Hong Kong et Macao .
En 1984 , à Anhui , une coentreprise avec le constructeur automobile Volkswagen constitue la première usine automobile construite avec une marque occidentale .
Volkswagen Shanghai représente une part de marché d' environ 60 % sur les véhicules étrangers en Chine , ce qui est en baisse constante en raison d' une concurrence accrue .
Ainsi , après l ' adhésion à l' OMC de la République populaire de Chine , la conférence de l' APEC en 2001 a réduit progressivement les droits à l' importation .
Le 5 août 2002 , le nouveau maire de Shanghai , Chen Liangyu a déclaré qu' il voulait " faire de sa ville , dans les trois années à venir , le centre du marché financier intérieur , des circulations des capitaux et de gestion de fonds , et l' un des centres financiers internationaux les plus importants pour une durée de dix à vingt ans. "
Cela dépend directement de la réforme du système financier chinois , encore très archaïque , mené par les autorités centrales de Pékin .
Actuellement , on compte plus de 50 000 étrangers dans la métropole chinoise , en plus d' environ 300 000 Taiwanais .
Ces travailleurs étrangers sont principalement originaires du Japon , des États-Unis , de la Corée du Sud , de Singapour , de l' Allemagne , de la France et du Canada .
Elle donne une idée de la valeur de prestige accordée au développement immobilier à Shanghai .
Le World Financial Center , en est l' exemple le plus éclatant , avec ses 492 mètres de hauteur , il est le plus haut bâtiment de Chine .
En Chine , l' immobilier est une des activités les plus opaques , ce qui explique la fragilité du secteur qui pourrait éclater si la croissance économique montre des signes de ralentissement .
Shanghai est également un centre important de raffineries de pétrole .
La plus grande aciérie de Chine , et l' une des plus modernes , se situe à Baoshan , en bord de mer .
Environ quatre millions de tonnes d' eaux usées industrielles et domestiques non filtrée sont versées quotidiennement dans la rivière Huangpu , la principale source d' eau potable de la ville , et dans le canal de Suzhou dont les eaux sont fréquemment noires et nauséabondes .
Un autre problème est le chômage , qui est supérieur à Shanghai par rapport à d' autres grandes villes du pays .
L ' Université Fudan est l' une des universités de premier plan en République populaire de Chine .
Elle prend son nom actuel en1946 quand elle revient à Shanghai .
L ' Université Tongji est l' une des plus prestigieuses universités en Chine .
En 1923 , elle devient une université et en 1937 elle est déménagé à cause de la guerre , d' abord dans la province de Zhejiang .
Lorsque le front approche , elle déménage vers la province de Jiangxi , puis Yunnan , et plus tard même pour le Sichuan .
Après la fin de la Seconde guerre mondiale , elle revient de nouveau à Shanghai , en 1946 .
L ' Université des études internationales de Shanghai est une institution importante dans le pays .
Depuis 2002 il existe un programme allemand des affaires , qui a été conçu conjointement avec l' Université de Bayreuth .
Voici une liste des autres principaux institut et universités présentes à Shanghai :
La langue officielle de Shanghai , comme dans l' ensemble de la Chine est le mandarin .
La variété parlée à Shanghai est le shanghaïen .
Dans le centre de Shanghai , près de l' hôtel de ville et de la rue de Nankin se trouvent le Musée de Shanghai et l' opéra de Shanghai .
Le long de la rivière Huangpu se trouve le Bund d' où l' on peut voir le quartier d' affaires de Pudong et ses gratte-ciels dont les plus hauts sont la Perle de l' Orient , la Jin Mao Tower et le Shanghai World Financial Centre .
En ce qui concerne les religions asiatiques , on trouve trois principaux temples : le temple de Jing'an , le temple du Bouddha de jade et le temple du dieu de la ville , ce dernier se situant près du jardin Yuyuan .
Enfin , certains ponts sont remarquables , comme le pont de Nanpu et le pont de Yangpu , qui se situent parmi les plus longs du monde avec respectivement plus de 400 m et plus de 600 m de portée .
Le pont de Lupu , quant à lui , est le deuxième plus long pont en arc du monde , avec 550 m de portée .
La cuisine de Shanghai est en en partie tournée vers les fruits de mer et les poissons d' eau douce , du fait de sa position géographique .
Ainsi , le crabe poilu de Shanghai ( shàng hǎi máo xiè , 上海毛蟹 ) est une célèbre spécialité délicate , prisée pour les qualités aphrodisiaques des ovaires du crabe femelle .
La cuisine de Shanghai est également réputée pour la cuisson " braisée en rouge " ( hóng shāo , 紅燒 ) , qui consiste à faire cuire à feu doux viandes et légumes .
Les habitants de la ville de Shanghai sont réputés pour manger de petites portions .
Par exemple , les bouchées à la vapeur ( xiǎo lóng bāo , 小笼包 ) sont beaucoup plus petites que leurs cousines baozi ( 包子 ) que l' on trouve ailleurs en Chine .
Voici une liste de spécialités de la cuisine de Shanghai :
Shanghai , ville de cinéma , a inspiré les cinéastes .
Quelques films où le décor de Shanghai à diverses époques joue un rôle majeur .
Shanghai possède d' importantes infrastructures sportives .
Le Stade de Shanghai peut ainsi accueillir 80000 personnes et constitue le troisième plus grand stade en Chine .
Le stade de Hongkou compte quant à lui 31000 places .
La ville organise également chaque année les Masters de Shanghai , une compétition de tennis masculin , qui fait partie des Masters 1000 de l' ATP Tour depuis 2009 , au même titre que les Masters de Madrid , Masters de Monte-Carlo ou encore Masters de Paris-Bercy .
Depuis le 1 er janvier 2004 , une ligne de Transrapid , un train à sustentation magnétique , relie la ville au nouvel aéroport international de Pudong .
La 2 e compagnie aérienne chinoise est basée sur cet aéroport : China Eastern Airlines .
Il est relié à l' aéroport international de Pudong par la ligne 2 du métro qui le dessert depuis 2010 .
Une bonne partie du trafic s' effectue avec l' intérieur du pays , par les 5000 kilomètres navigables du Yangzi Jiang : les bateaux peuvent aller de Shanghai jusqu' à Chongqing .
Shanghai est jumelée avec les villes suivantes :
Shanghai a des partenariats avec les villes ou régions suivantes :
