La République centrafricaine , aussi appelée Centrafrique , est un pays sans accès à la mer d' Afrique centrale , entouré par le Cameroun à l' ouest , le Tchad au nord , le Soudan à l' est , la République démocratique du Congo et le Congo au sud .
L' essentiel de la frontière sud du pays est marqué par le fleuve Oubangui et le Mbomou en amont .
La partie nord du pays constitue le haut bassin du fleuve Chari .
Le pays devient la République centrafricaine le 1 er décembre 1958 et proclame son indépendance le 13 août 1960 .
David Dacko lui succède encore brièvement .
Il sera chassé du pouvoir en 1982 par le général André Kolingba , qui établit un régime militaire .
Une élection présidentielle a eu lieu , après plusieurs reports , le 13 mars 2005 dans laquelle se présentaient , entre autres , François Bozizé , l' ancien président André Kolingba , et l' ancien vice-président Abel Goumba .
Suite à ces accords , seule la candidature de l' ancien président Ange-Félix Patassé a été définitivement rejetée par la commission électorale .
La République centrafricaine est composée de 14 préfectures , 2 préfectures économiques .
Bangui a le statut de commune .
Le Centrafrique est un pays enclavé .
Le Mont Ngaoui avec ses 1420 m est le point culminant .
Le pays souffre d' inondations en raison du manque d' entretien des fleuves et des débits impressionnants qu' engendre la saison des pluies en Afrique Centrale .
Le Centrafrique comporte deux grands bassins séparés .
L' activité minière ( or et diamants ) constitue l' autre ressource importante de la République centrafricaine en termes de recettes d' exportation : la production officielle -- principalement artisanale -- de diamants alluvionnaires de très bonne qualité ( diamants de joaillerie ) s' établit à environ 500000 carats par an .
Le tissu industriel , qui n' a jamais été très développé par rapport aux pays voisins comme le Cameroun par exemple , a souffert des troubles militaires et politiques successifs , et est aujourd'hui quasiment inexistant .
Le système éducatif en Centrafrique est calqué sur le modèle de celui de la France .
