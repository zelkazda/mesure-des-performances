Sony Corporation est une entreprise japonaise mondialement représentée , notamment dans les domaines de la musique , de l' électronique , de la téléphonie , de l' informatique , du cinéma et de l' audiovisuel .
La compagnie ou ses entreprises sont communément appelés Sony .
Voir les entreprises de Sony Corporation .
Ainsi , les premiers transistors japonais sortent des usines de Sony cette année-là , 6 ans après leur invention aux États-Unis .
L' année suivante , Sony commercialise le premier récepteur radio entièrement à base de transistors .
Depuis , peu de sociétés ont réalisé un parcours semblable à celui de Sony en termes d' invention et d' innovation .
Sony est une société internationale -- Akio Morita a reconnu dès le départ que sa société devait considérer le monde entier comme marché et non se limiter au Japon .
Il insista pour que le nom Sony apparaisse clairement sur tous les produits de la société .
Sony fabrique aussi des semiconducteurs mais uniquement pour ses propres filiales .
En 2005 , Sony pointe ainsi à la 13 e place des vingt plus grands fabricants de semiconducteurs .
Le siège social se trouve à Tōkyō au Japon .
Le chiffre d' affaires pour l' ensemble des sociétés et filiales de Sony Corporation à travers le monde en 2004 s' élève à près de 69 milliards d' euros .
La part de Sony dans le marché mondial de l' électronique grand public a été estimée en 2004 à plus de 14 % ( devant Matsushita , Hitachi et Philips ) .
En mai 2008 , Sony annonça un profit net record de près de 2,4 milliards d' euros suite à une année 2007 marquée par les belles performances de ses produits phares
Le 26 janvier 2006 , Sony annonce en même temps que ses résultats financiers , l' abandon de tout développement concernant ses robots Aibo et Qrio pour se recentrer sur des segments plus rentables .
