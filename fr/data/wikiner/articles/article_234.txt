L' Insee et la Poste lui attribuent le code 18 .
Le département a été créé à la Révolution française , le 4 mars 1790 en application de la loi du 22 décembre 1789 , à partir d' une partie de la province du Berry , du Bourbonnais et du Nivernais ( vallée de l' Aubois ) .
Le Cher formait autrefois avec le département de l' Indre la province du Berry .
Il fait aujourd'hui partie de la Région Centre et est limitrophe des départements de l' Indre , de Loir-et-Cher , du Loiret , de la Nièvre , de l' Allier et de la Creuse .
Le Cher en sortant du département est à l' altitude la plus faible : 89 m .
De nombreuses rivières traversent son territoire , dont les noms se retrouvent mêlés aux toponymes communaux : l' Auron , la Sauldre , la Yèvre , la Vauvise , le Cher .
