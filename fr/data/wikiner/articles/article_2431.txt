Anatoli Karpov , né en 1951 , apprit à jouer à l' âge de 4 ans et à 11 ans il devenait candidat à la maîtrise .
À 12 ans , il fut admis dans la prestigieuse école d' échecs de Mikhaïl Botvinnik à Moscou .
La raison principale de ce déménagement était de se rapprocher de son entraîneur Semion Fourman .
Son classement ( 4 e -- 6 e avec +8 -2 =7 ) au tournoi international de Caracas lui fit obtenir le titre de grand maître international en 1970 .
Il partagea la 1 re place ( +5 =12 ) ex-aequo avec Leonid Stein en devançant Vassily Smyslov , Tigran Petrossian , Boris Spassky et Mikhaïl Tal , tous quatre ex-champions du monde .
Il ne participa qu' au tournoi de San Antonio ( Texas , États-Unis ) où il termina 1 er ex æquo avec Tigran Pétrossian et Lajos Portisch ( +7 -1 =7 ) .
Durant sa prime jeunesse , Karpov avait pour livre de chevet un recueil de parties de José Raúl Capablanca .
Karpov a toujours dit jouer " aux vrais échecs " et ne pas laisser place au " hasard " comme le faisait , par exemple , Mikhaïl Tal avec ses attaques virevoltantes et ses sacrifices invraisemblables , quoique parfois douteux , qui ébranlaient psychologiquement ses adversaires .
La particularité de Karpov était d' obtenir un petit , voire minuscule avantage dans l' ouverture , puis de l' accroître progressivement par la pression sur une faiblesse créée dans le camp de l' adversaire jusqu' à ce que la position de ce dernier s' écroulât .
À la fin de l' année , Karpov remporta le tournoi de Madrid ( +7 =8 ) .
Lors des matches des candidats , en 1974 , il battit successivement Lev Polougaïevsky ( +3 =5 ) , Boris Spassky ( +4 -1 =6 ) , puis Viktor Kortchnoï ( +3 -2 =19 ) et devint champion du monde d' échecs en 1975 à la suite du forfait du tenant du titre , Bobby Fischer .
Karpov fut le premier champion du monde depuis 1948 à conquérir son titre sans avoir pu livrer de match contre son prédécesseur .
De 1972 à 1985 , rares furent les tournois où la victoire lui échappa , mais dans la même période ( de 1976 à 1984 ) , il refusa , comme les autres joueurs soviétiques , de participer aux tournois où jouait le dissident et numéro deux mondial Viktor Kortchnoï .
A partir de 1987 , Karpov dut souvent laisser la première place dans les tournois à Garry Kasparov .
A partir de 1991 , la nouvelle génération de joueurs ( Vassili Ivantchouk , Boris Gelfand , Nigel Short , Viswanathan Anand , Gata Kamsky , Vladimir Kramnik , Michael Adams ... ) priva Karpov de nombreux premiers prix dans les tournois
En 1978 , son ami et entraîneur depuis 1968 Semion Fourman décéda et , la même année , à Baguio ( Philippines ) , Karpov défendit son titre contre Viktor Kortchnoï qui avait fui l' URSS en 1976 , était devenu apatride et avait perdu ses soutiens et secondants habituels .
Il protesta aussi , au début du match , contre l' apport , durant les parties , de yoghourts à Karpov où ses secondants auraient pu dissimuler des suggestions ( suivant la couleur du yoghourt ) sur la meilleure tactique à adopter .
Cette confrontation , qui se jouait en six parties gagnantes , fut remportée à la 32 e partie par Karpov , sur le score de 6 victoires à 5 ( +6 -5 =21 ) .
Au cycle suivant , en 1981 à Merano ( Italie ) , Karpov affronta le même adversaire dans un match plus court et conserva le titre " à la régulière " en 18 parties sur le score de 6 victoires à 2 ( +6 -2 =10 ) .
En octobre 1984 -- février 1985 à Moscou , il rencontra Garry Kasparov dans un match marathon .
Après 9 parties , Karpov menait 4 -- 0 , puis s' ensuivit une série de 17 parties nulles .
Karpov gagna la 27 e partie ( 5 -- 0 ) , puis Kasparov gagna la 31 e partie ( 5 -- 1 ) .
Mais , après une nouvelle série de 15 parties nulles , quand Kasparov remonta à 5 -- 3 , le match fut interrompu après la 48 e partie , sans qu' un vainqueur fût désigné .
On accusa le président de la Fédération internationale des échecs , Florencio Campomanes de protéger Karpov .
En septembre -- novembre 1985 , le match fut rejoué en 24 parties , Kasparov l' emporta : 13 à 11 ( +5 -3 =16 ) .
Par la suite , Karpov tenta trois fois de récupérer la couronne :
En 1986 eut lieu le match revanche , disputé à Londres et Léningrad , qui vit la victoire de Kasparov sur le score de 12,5 à 11,5 ( +5 -4 =15 ) .
En 1987 , après qu' il eut écarté Andreï Sokolov de la course ( +4 =7 ) , sa confrontation de Séville contre Kasparov se disputa en 24 parties et s' acheva sur une égalité 12 à 12 ( +4 -4 =16 ) qui favorisait le tenant du titre ( Kasparov égalisa dans la dernière partie , conservant son titre de justesse ) .
Durant le cycle des candidats suivant , en 1988-1989 , il élimina successivement Johann Hjartarson ( +2 =3 ) , Arthur Youssoupov ( +2 -1 =5 ) et Jan Timman ( +4 =5 ) et se qualifia à nouveau pour la finale de 1990 , disputée à New York et Lyon , qu' il perdit 11½ -- 12½ ( +3 -4 =17 ) .
Au cours de leurs cinq matchs , Karpov disputa un total de 144 parties contre Kasparov avec un résultat de +19 -21 =104 .
En 1991 -- 1993 , après avoir battu Viswanathan Anand ( +2 -1 =5 ) , il perdit , en 1992 , contre toute attente son match en demi-finale des candidats contre Nigel Short ( +2 -4 =4 ) , mais put récupérer le titre laissé vacant par Kasparov , en battant Jan Timman ( +6 -2 =13 ) , en 1993 , car Garry Kasparov , qui s' était brouillé avec la FIDE , avait quitté la FIDE et créé un championnat du monde " parallèle " ( PCA , Professional Chess Association ) avec Nigel Short .
Lors de deux finales , il réussit à défendre son titre : en 1996 , après avoir écarté Boris Gelfand ( +4 -1 =4 ) , il battit en finale Gata Kamsky ( +6 -3 =9 ) ; et , en février 1998 , en disposant de Viswanathan Anand , visiblement épuisé par les matchs de sélection , après des parties de départage ( +4 -2 =2 ) .
Karpov conserva son titre jusqu' au tournoi de Las Vegas en 1999 qui vit l' instauration d' un nouveau système à élimination directe pour décerner le titre et non plus un match entre le champion et un candidat .
Karpov poursuivit la FIDE devant le tribunal arbitral du sport de Lausanne car son titre lui était acquis pour deux ans , mais un accord à l' amiable fut finalement trouvé .
Il refusa cependant de participer à cette nouvelle formule et perdit son titre au profit du méconnu Aleksandr Khalifman .
La finale fut disputée en janvier 2002 et remportée par Ruslan Ponomariov .
En effet , lors de ce tournoi auquel participaient entre autres Garry Kasparov , Vladimir Kramnik et Viswanathan Anand , il ne perdit aucune partie et marqua 11 points sur 13 possibles ( +9 -0 =4 ) .
Ce fut , avec le tournoi de Moscou en 1981 , le seul tournoi où il devança Garry Kasparov .
Il a battu Kasparov lors d' un match rapide en quatre parties disputé à New York en 2002 .
En 2009 , il a disputé un nouveau match rapide et blitz contre Kasparov à Valence où Kasparov a obtenu sa revanche de sa défaite en 2002 .
Le 1 er mars 2010 , Karpov a annoncé sa candidature à la présidence de la Fédération internationale des échecs .
En mai 2010 , la fédération russe des échecs le nomme candidat à la présidence de la FIDE , aux dépens de Kirsan Ilioumjinov , .
Depuis la disparition de l' URSS , il conserve toujours cette fonction dans la structure russe analogue .
En 2004 , il se retira de la finale du championnat de Russie .
Après la disparition de l' URSS , la Russie fut à nouveau opposée ( à cadence rapide ) aux meilleurs joueurs non russes à Moscou en 2002 .
Karpov ne marqua que 5 points sur 9 ( +3 -2 =4 ) contre neuf adversaires différents .
À chaque fois , l' URSS remporta la médaille d' or .
En 1972 , à Skopje , en 1 er remplaçant , il marqua 13 points sur 15 ( +12 -1 =2 ) et obtint la médaille d' or individuelle pour les cinquième échiquiers .
A Nice , en 1974 , au 1 er échiquier , il obtint 12 points sur 14 ( +10 =4 ) et la médaille d' or individuelle .
En 1978 , à Buenos Aires , Karpov ne joua pas pour son équipe , car il avait été épuisé par son match de championnat du monde qui venait de s' achever .
Son absence eut pour conséquence que , pour la première fois depuis 1952 , l' URSS dut se contenter de la 2 e place , devancée par la Hongrie .
Karpov fut de retour au 1 er échiquier , en 1980 , à La Valette et marqua 9 points sur 12 ( +6 =6 ) .
A Lucerne , en 1982 , toujours au 1 er échiquier , il marqua 6½ points sur 8 ( +5 =3 ) .
En 1986 à Dubaï , au 2 e échiquier , il inscrivit 6 points sur 9 ( +4 -1 =4 ) .
Au total , Karpov disputa 68 parties dans le cadre des Olympiades , pour un résultat de +43 -2 =23 .
( Anatoli Karpov -- Veselin Topalov , Tournoi de Linares , 1994 )
Cette partie , jouée durant le meilleur tournoi de la carrière de Karpov , voit ce dernier offrir sa tour à trois reprises .
