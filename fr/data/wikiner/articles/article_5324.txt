Le reste est recouvert par le désert Libyque à l' ouest , le désert Arabique à l' est et le Sinaï au nord-est .
L' Égypte a subi depuis très longtemps un phénomène de désertification qui voit le désert gagner du terrain sur les terres arables .
On pense ainsi qu' à l' époque de la construction des grandes pyramides , le plateau de Gizeh était recouvert d' une savane ( il est aujourd'hui complètement désertique ) .
La crue annuelle du Nil était l' événement majeur de l' année pour les Égyptiens de l' antiquité ( d' ailleurs le jour de l' an commence avec les premiers signes de montée des eaux ) .
Aujourd'hui canalisées par le Haut barrage d' Assouan , les eaux du Nil recouvraient autrefois une grande partie de ses berges , apportant par là même un précieux limon noir qui rendait ses terres fertiles .
Cette crue apportait la prospérité aux Égyptiens , mais ses débordements pouvaient être aussi meurtriers que les famines causées par une trop faible montée des eaux .
La quasi-disparition de cette inondation annuelle du Nil a également eu une grande répercussions sur l' écosystème de la vallée du Nil .
Les frontières " traditionnelles " de l' Égypte antique sont assez semblables aux frontières de l' Égypte moderne .
Ce sont principalement les frontières sud avec la Nubie et nord-ouest qui ont fluctué au cours des siècles .
La domination égyptienne en syro-palestine sera toujours de courte durée et dépassera rarement la Palestine .
