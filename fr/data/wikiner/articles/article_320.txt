Il est le fils de Marcus Junius Brutus , partisan de Marius , et de Servilia Caepionis , demi-sœur de Caton d' Utique .
Il revint à Rome enrichi et commença son cursus honorum .
Il obtint en -53 la questure en Cilicie où il s' enrichit encore plus .
Sa conduite est ensuite dénoncée par Cicéron .
César lui fit gravir les échelons du cursus honorum traditionnel .
Il fut nommé gouverneur de Gaule Cisalpine pour -46/-45 , puis préteur urbain pour l' année -44 , préféré alors à son concurrent , Caius Cassius Longinus , le futur assassin de César , qui fut nommé préteur pérégrin .
-- Appien , 4 , 17
ou César mort , et tous vivre libres ?
César m' aimait et je le pleure .
Jules César ( Julius Caesar ) est un film américain réalisé par Joseph Leo Mankiewicz , d' après la pièce de William Shakespeare , sorti en 1953 .
Marie-Nicolas Bouillet et Alexis Chassang ( dir .
