En 1812 , Saint-Jean-de-Savigny ( 546 habitants en 1806 ) absorbe Clouay ( 119 habitants ) au nord-ouest de son territoire .
Saint-Jean-de-Savigny a compté jusqu' à 656 habitants en 1821 , mais les deux communes de Saint-Jean-de-Savigny et de Clouay en totalisaient 665 en 1806 ( respectivement 546 et 119 ) .
