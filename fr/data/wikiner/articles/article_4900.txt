Orphelin de père à 6 ans il est suivi par son professeur de philosophie morale à l' Université de Leipzig .
En 1663 , il obtient son baccalauréat en philosophie ancienne , puis entre à l' université de droit de Leipzig .
En 1666 , il devient docteur en droit à Nuremberg et refuse peu après un poste de professeur .
Il est envoyé en 1672 à Paris , en mission diplomatique dit-on , pour convaincre Louis XIV de porter ses conquêtes vers l' Égypte plutôt que l' Allemagne .
Il y reste jusqu' en 1676 et y rencontre les grands savants de l' époque : Huygens et Malebranche , entre autres .
Il se consacre aux mathématiques et laisse à Paris son manuscrit sur la quadrature arithmétique du cercle .
Il passe également par La Haye où il rencontre Baruch Spinoza .
En 1676 , à la mort de son protecteur , le baron von Boyneburg , le duc de Brunswick le nomme bibliothécaire du Hanovre .
Il reste à ce poste au service des ducs de Hanovre pendant près de 40 ans .
En 1684 , il publie dans les Acta Eruditorum son article sur les différentielles et en 1686 celui sur les intégrales .
En 1700 , il fonde à Berlin une académie qui ne sera inaugurée qu' en 1711 .
Pour Leibniz , la physique a sa raison dans la métaphysique .
Leibniz définit la force comme " ce qu' il y a dans l' état présent , qui porte avec soi un changement pour l' avenir. "
À cette notion de loi se rattache également l' idée d' individualité : l' individualité est pour Leibniz une série de changements , série qui se présente comme une formule :
Ce qui existe est donc pour Leibniz l' individuel ; il n' existe que des unités .
Leibniz nomme monade cette réalité .
Ajoutons que le concept de monade a été influencé par la philosophie de Pierre Gassendi , lequel reprend la tradition atomiste incarnée par Démocrite , Épicure et Lucrèce .
La différence majeure avec la monade étant que celle-ci est d' essence spirituelle alors que l' atome est d' essence matérielle , et donc l' âme , qui est une monade chez Leibniz , est composée d' atomes chez Lucrèce .
Leibniz explique cette concordance par une harmonie universelle entre tous les êtres , et par un créateur commun de cette harmonie :
Cette transposition pose des problèmes qui ne sont pas vraiment résolus par Leibniz :
Leibniz décrit ainsi la représentation du corps ( c.-à-d. du multiple ) par l' âme :
La réponse de Leibniz au conflit entre nécessité et liberté est originale .
Le principe de raison suffisante , parfois nommé principe de " la raison déterminante " ou le " grand principe du pourquoi " , est le principe fondamental qui a guidé Leibniz dans ses recherches : rien n' est sans une raison qui explique pourquoi il est plutôt qu' il n' est pas , et pourquoi il est ainsi plutôt qu' autrement .
Leibniz ne nie pas que le mal existe .
En 1759 , dans le conte philosophique Candide , Voltaire fait de son personnage Pangloss le porte-parole du providentialisme de Leibniz .
Toutefois le texte de Voltaire ne s' oppose pas à Leibniz sur un plan théologique ni métaphysique : le conte de Candide trouve son origine dans l' opposition entre Voltaire et Rousseau , et son contenu cherche à montrer que ce ne sont pas les raisonnements des métaphysiciens qui mettront fin à nos maux , faisant l' apologie d' une philosophie volontariste invitant les hommes à organiser eux-mêmes la vie terrestre et où le travail est présenté comme source de progrès matériels et moraux qui rendront les hommes plus heureux .
Les Nouveaux essais sur l' entendement humain , rédigés en français , sont la réponse de Leibniz à l' Essai sur l' entendement humain de John Locke .
Mais la mort de Locke convainc Leibniz de reporter à plus tard leur publication .
L' algorithme différentio -- intégral achève une recherche débutée avec la codification de l' algèbre par Viète et l' algébrisation de la géométrie par Descartes .
Comme Newton , Leibniz domine tôt les indéterminations dans le calcul des dérivées .
Leibniz développe une symbolique mathématique qu' il tente d' intégrer dans une notion plus générale qu' il appelle sa caractéristique universelle qu' il voulait pouvoir appliquer à tous les domaines .
Il est à l' origine du terme de " fonction " ( 1692 , de functio : exécution ) , de celui de " coordonnées " , de la notation du produit de a par b sous la forme a . b ou ab , d' une définition logique de l' égalité , du terme de " différentielle " ( qu' Isaac Newton appelle " fluxion " ) , de la notation différentielle , du symbole pour l' intégrale .
Dans l' histoire du calcul infinitésimal , le procès de Newton contre Leibniz est resté célèbre .
Newton et Leibniz avaient trouvé l' art de lever les indéterminations dans le calcul des tangentes ou dérivées .
Mais Newton a publié tard ( son procès intervient en 1713 , presque 30 ans après les publications de Leibniz : 1684 et 1686 ) et , surtout , Newton n' a ni l' algorithme différentio-intégral fondé sur l' idée que les choses sont constituées de petits éléments , ni l' approche arithmétique nécessaire à des différentielles conçues comme " petites différences finies " .
Leibniz s' intéresse aux systèmes d' équations et pressent l' usage des déterminants .
Il a étudié le traité des coniques de Pascal et écrit sur le sujet .
Il conçoit une machine arithmétique inspirée de la Pascaline .
Leibniz était aussi physicien comme de nombreux mathématiciens de son temps .
Il a très tôt été mécaniste et l' est resté toute sa vie , mais une différence profonde le sépare d' Isaac Newton : si Newton considère que " la physique se garde de la métaphysique " et cherche à prévoir les phénomènes par sa physique , Leibniz cherche à découvrir l' essence cachée des choses et du monde , sans réussir ( ni vouloir ? )
Les deux grandes caractéristiques de la logique de Leibniz consistent d' une part dans le fait qu' il a voulu constituer un langage universel ( la lingua caracteristica universalis ) prenant en compte non seulement les connaissances mathématiques , mais aussi la jurisprudence ( il établit les correspondances à la base de la déontique ) , l' ontologie ( Leibniz critiqua la définition que René Descartes donnait de la substance ) voire la musique .
A côté de cette langue universelle , Leibniz a rêvé d' une logique qui serait calcul algorithmique et donc mécaniquement décidable ( calculus ratiocinator ) .
Leibniz annonce ainsi la langue artificielle et purement formelle développée par Frege .
L' œuvre de Leibniz a été écrite pour moitié en latin et pour un tiers en français .
