Jean Arp ou Hans Arp , né à Strasbourg le 16 septembre 1886 et mort à Bâle en Suisse le 7 juin 1966 était un peintre , un sculpteur et un poète allemand puis français .
Il réalisa de nombreuses œuvres plastiques en étroite collaboration avec sa femme Sophie Taeuber .
Le 20 octobre 1922 , il épouse Sophie Taeuber-Arp qu' il a connue à Zurich .
En 1925 , il s' installe à Meudon , dans une maison-atelier dont Sophie Taeuber a dressé elle-même les plans .
Il participe aux activités des surréalistes et fréquente les peintres abstraits de Cercle et Carré .
Arp est à l' origine d' un vocabulaire de signes aux allusions figuratives et ironiques .
Arp est devenu un artiste internationalement reconnu .
Un grand nombre de ses œuvres sont aujourd'hui exposées au musée d' art moderne et contemporain de Strasbourg , qui lui consacre un espace central .
Son nom a également été donné à la place servant de parvis à ce musée , ainsi qu' au bâtiment de l' École nationale d' administration .
Les poèmes allemands de Jean Arp n' ont fait l' objet jusqu' à ce jour que de rares traductions :
