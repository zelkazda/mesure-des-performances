En 2008 , la population comptait 4428368 habitants ( les Britanno-Colombiens ) .
La Colombie-Britannique est la province la plus occidentale du Canada , elle longe la côte Pacifique du Canada .
La province est majoritairement dominée par des chaînes montagneuses globalement parallèles à l' axe nord-sud principal de la province , notamment les Rocheuses canadiennes , la chaîne Côtière , la chaîne des Cassiars et les chaîne Columbia .
Dans la partie sud située entre les chaînes montagneuses de l' ouest et celles de l' est , se trouve un vaste plateau , le plateau Intérieur , tandis que le coin nord-est de la province est occupée par des plaines légérement ondulées dans le prolongement des grandes plaines de l' Alberta et de la Saskatchewan .
La Colombie-Britannique est généralement divisée en trois grandes régions :
À l' intérieur des terres les températures estivales peuvent être plutôt chaudes , puisqu' il existe plusieurs régions semi-arides dans la province , par exemple près des villes d' Osoyoos et de Lillooet .
Un endroit unique en Colombie-Britannique s' appelle Sunshine Coast ( " La côte ensoleillée " ) .
La capitale de la Colombie-Britannique est Victoria , à l' extrémité sud-est de l' île de Vancouver .
La principale métropole de la province est Vancouver , au coin sud-ouest du Canada continental .
C' est après la fin du dernier réchauffement climatique que les premiers habitants amérindiens s' établirent le long des côtes et dans les vallées de la Colombie-Britannique .
Le plus ancien site archéologique connu en Colombie-Britannique est situé au nord-est ( région presque arctique ) de la Colombie-Britannique .
L' année suivante , l' espagnol Juan Francisco de la Bodega y Quadra sera le premier à explorer la région .
Avant l' établissement en 1846 de la frontière entre les parties américaine et britannique des territoires situés à l' ouest des Rocheuses , la plus grande partie de l' actuelle Colombie-Britannique faisait partie de ce qui était appelé Oregon Country à l' époque , par les americains. .
C' est la reine Victoria qui prit la décision finale sur le nom de la colonie .
Les deux colonies fusionnèrent en 1866 sous le nom de colonie de Colombie-Britannique .
La région du Cariboo fut l' objet d' une ruée vers l' or en 1862-1865 .
Les raisons de la décision des Britanno-Colombiens de se joindre au dominion du Canada en 1871 furent nombreuses .
Il y avait la peur d' annexion aux États-Unis , la dette écrasante créée par la croissance rapide de la population et le besoin de services gouvernementaux pour les supporter , et la fin de la ruée vers l' or et la dépression légère qui l' avait accompagnée .
Le Canadien Pacifique à travers les montagnes Rocheuses fut difficilement construite entre 1875 -- 1885 .
En 1898 -- 1903 la province rétrécit considérablement après que la Dispute de la frontière de l' Alaska attribua le nord-ouest , en particulier la côte , à son voisin septentrional .
Un autre ordre religieux français , les Oblats , fondèrent en 1859 une mission à ce qui est aujourd'hui la ville de Kelowna .
En 1910 la communauté francophone de Maillardville fut fondée par des colons québécois dans la région de Coquitlam .
L' économie de la Colombie-Britannique est historiquement liée aux ressources naturelles ( exploitation forestière , extraction de minéraux , pêche ) .
Le PIB de la Colombie-Britannique s' est élevé à 192528 millions de dollars canadiens en 2007 ( +5,4 % sur un an ) et son économie est désormais essentiellement liée aux activités tertiaires .
L' exploitation des forêts est une activité économique très importante en Colombie-Britannique .
L' exploitation minière est une des principales activités économiques de la Colombie-Britannique .
Par son climat , sa beauté et son esprit pionnier très ouvert , la Colombie-Britannique convient à ceux qui cherchent une dernière frontière .
La vallée de l' Okanagan devient une importante zone de vignobles .
Whistler offre un des plus grands domaines hors-piste du monde durant l' hiver ainsi que de nombreux kilomètres de pistes de VTT durant l' été .
Elle abrite également des lieux de mémoires du Canada :
