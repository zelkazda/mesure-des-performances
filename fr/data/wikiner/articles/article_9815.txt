Final Fantasy est une série de jeux vidéo de rôle ( RPG ) produite par Square Enix ( originellement Square ) initiée par Hironobu Sakaguchi en 1987 au Japon sur la console NES avec le jeu Final Fantasy .
Depuis , la licence évolue en parallèle avec chaque génération de console de salon , quelquefois adaptée sur d' autres plates-formes comme l' ordinateur ou les téléphones portables dont tout récemment sur Iphone ( 2010 ) et se diversifie dans les domaines du film en images de synthèse et de l' anime .
La franchise s' est vendue à plus de 96 millions d' unités à travers le monde et est depuis Final Fantasy VII une série majeure et mondialement connue par les pratiquants du jeu vidéo , puis par le grand public avec la sortie au cinéma de Final Fantasy : les Créatures de l' esprit et celle en DVD de Final Fantasy VII : Advent Children .
Convaincu que ce projet serait le dernier avant de mettre la clé sous la porte , Square l' a nommé Final Fantasy .
Ceux qui furent derrière Final Fantasy comptent aujourd'hui parmi les figures les plus connues du jeu vidéo : Yoshitaka Amano , responsable du design , aujourd'hui artiste reconnu au-délà du domaine du jeu vidéo , Nobuo Uematsu , un compositeur , et Hironobu Sakaguchi , chef de projet .
Chaque Final Fantasy apporte un nouveau monde et un nouveau système de jeu .
Plusieurs éléments et thèmes se répètent à travers la série , mais il n' y aura pas de suite directe avant la sortie de Final Fantasy X-2 en 2003 .
Depuis , d' autres Final Fantasy ont eu droit à une suite directe .
Final Fantasy VII est celui qui en a eu le plus , parfois plusieurs années après sa commercialisation et dans des genres qui s' éloignent du RPG comme avec Before Crisis , Crisis Core ou encore Dirge of Cerberus .
Plusieurs éléments originalement introduits dans la série ont fait leur chemin dans d' autres titres de Square , en particulier dans deux autres franchises majeures que sont SaGa ou Seiken Densetsu ( Mana en Europe ) .
Même si chaque histoire de Final Fantasy est indépendante , plusieurs thèmes et éléments du gameplay sont récurrents dans la série .
Parmi les objets clés et concepts qui sont apparus dans plus d' un Final Fantasy , sont les plus récurrents :
Le design artistique , incluant l' aspect des personnages et des monstres , a été conçu par l' artiste japonais de renom , Yoshitaka Amano , de Final Fantasy à Final Fantasy VI .
Il a partiellement ou entièrement écrit les histoires de Final Fantasy VII , Final Fantasy VIII , Final Fantasy IX , Final Fantasy X et Final Fantasy X-2 , ainsi que le scénario de Final Fantasy VII : Advent Children , le dernier long métrage en date des studios Square Enix .
Mais les véritables artistes de la saga sont Yoshitaka Amano et Tetsuya Nomura .
Tetsuya Nomura est l' une des figures les plus célèbres et imposantes de Square Enix dans ses dernières productions .
Son admiration pour Yoshitaka Amano l' amène alors à postuler chez Square .
Il rejoint la compagnie le 16 avril 1991 et prend part directement au développement de Final Fantasy V , pour lequel il réalise les designs des monstres .
Nobuo Uematsu est le compositeur des pièces musicales de Final Fantasy jusqu' à Final Fantasy XI ( en ce qui concerne Final Fantasy XII , il n' en a écrit que le thème principal ) .
Il est le compositeur en chef de la musique des Final Fantasy jusqu' à sa démission en novembre 2004 .
Uematsu est aussi impliqué dans un groupe rock nommé " The Black Mages " , qui a sorti trois albums des musiques de Final Fantasy en versions ré-arrangées .
Plus récemment , Yoko Shimomura compose les musiques de Final Fantasy Versus XIII et des jeux de la série Kingdom Hearts .
Sur l' écran principal de jeu , une petite représentation en sprite du chef de file était le maximum qui pouvait être affiché pour les personnages à cause des limitations graphiques de la NES alors que , durant les séquences de combat , des versions complètes de tous les personnages étaient affichées en perspective de côté .
À cause du faible engouement des joueurs américains pour le premier opus , ces deux épisodes ne furent pas traduits et restèrent au Japon , où ils eurent également un gros succès .
À la conquête du marché mondial Square amorce le développement du quatrième épisode de sa série fétiche sur Famicom , mais Nintendo ayant sorti une nouvelle console bien plus performante , la Super Nintendo , le développement se poursuivit sur cette nouvelle console .
Ce quatrième épisode , dont la difficulté et la complexité ont été revues à la baisse pour les américains , déclenche des passions chez eux , mais les ventes ne sont toujours pas un réel succès face aux raz de marée déclenchés par les sorties des Final Fantasy et Dragon Quest au Japon .
Final Fantasy IV est le dernier de la série à utiliser le kana pour les versions japonaises comme il était l' habitude depuis le premier Final Fantasy .
Finalement , dans Final Fantasy V , les jeux commencent à utiliser le kanji .
Cette tendance se poursuivra dans Final Fantasy VI et continuera par la suite à rendre les jeux de la série plus érudibles .
Final Fantasy IV se démarque également de ces prédécesseurs par l' apport d' un scénario beaucoup plus travaillé , une personalités des protagonistes plus approfondie , et de nombreux rebondissements qui font prendre au jeu une toute autre dimension .
Cet épisode , même s' il ne fut pas le plus vendu de tous , est réellement le premier qui contribuera a la renommée et au succès international de la série , et Final Fantasy IV sera réédité de nombreuses fois sur des consoles ultérieures ( Playstation , GameBoy Advance et DS )
À la place , les américains auront droit à un Final Fantasy édulcoré baptisé " Mystic Quest Legend " qui reprend le moteur de jeu de Final Fantasy IV mais reste un jeu simple avec un scénario peu développé .
Ce jeu verra également le jour en Europe , mais son succès très mitigé ne marquera pas .
Final Fantasy VI , sorti en 1994 sur Super Nintendo , se voit traduit en anglais et apparaît sur le marché américain sous le nom de Final Fantasy III avec quelques changements mineurs par rapport à la version japonaise .
C' est maintenant au tour des Européens de signer des pétitions pour une importation de cet épisode , mais les traductions seront longtemps repoussées , et finalement Final Fantasy VI ne sera jamais importé en Europe avant 2002 , où il sort sur la Playstation de Sony avec une démo de Final Fantasy X .
Final Fantasy VI sera le premier vrai épisode remportant un franc succès à l' étranger .
Après avoir tenté une démo d' un hypothétique Final Fantasy sur Nintendo 64 , Square décide de tourner le dos à Nintendo au profit de Sony et de sa PlayStation .
Cependant , les vidéos de Final Fantasy VII manquent souvent de consistance , avec les personnages apparaissant très petits et peu voyants dans une scène et extrêmement détaillés dans la suivante .
Malgré cela , de très nombreuses passions sont nées de cet épisode et il est à ce jour l' épisode possédant le plus de fans à travers le monde et régulièrement classé comme l' un des meilleurs jeux de tous les temps ; les attentes des fans pour les prochains épisodes sont donc désormais différentes , et Square ne pourra plus contenter tout le monde ...
Tetsuya Nomura confirmera ses talents de designer sur cet épisode .
Le film est une véritable prouesse technique , utilisant des technologies inédites pour l' époque , mais ne rencontre pas le succès escompté par les dirigeants de Square .
Le coût des acteurs en images de synthèse est trop élevé et les fans de la série ne ressentent pas la présence de l' univers de Final Fantasy , se sentant en quelque sorte trahis .
L' épisode rencontre un gros succès et marque un tournant de plus dans la série puisqu' il voit l' apparition du doublage pour les scènes de dialogue , assuré par des acteurs , chose qui n' avait pas encore été vue jusque là dans un Final Fantasy .
Cet épisode voit également un bouleversement au niveau musical , puisque Nobuo Uematsu partage l' affiche en ce qui concerne les compositions avec Junya Nakano et Masashi Hamauzu qui ont déjà contribué aux compositions musicales d' autres jeux .
Un changement de concept Final Fantasy XI voit le jour en 2002 .
Ce genre de jeu , à l' époque surtout populaire sur compatibles PC , constitue un changement radical dans la série .
C' est ainsi que dans cet épisode sorti en 2003 , le joueur retrouve l' univers propre à Final Fantasy X et une histoire venant directement suivre cet épisode .
Certains y ont vu une perte de créativité et d' imagination de la part de Square .
Les deux géants du jeu vidéo sont donc parvenus à un accord , de cet accord est né Final Fantasy Crystal Chronicles sur GameCube et Final Fantasy Tactics Advance sur Game Boy Advance .
Cloud l' ancien héros est devenu quant à lui un solitaire vivant avec les troubles de son passé .
Basé sur le même univers , Final Fantasy XII Revenant Wings est sorti l' année suivante sur DS .
La PlayStation 3 et la Xbox 360 doivent accueillir le jeu de rôle Final Fantasy XIII et la PlayStation 3 accueillera le jeu d' action Final Fantasy Versus XIII .
Final Fantasy Agito XIII est un projet de jeu multijoueur destiné à la PSP .
Un nouveau Final Fantasy Tactics Advance a également vu le jour sur DS , sous le nom de Final Fantasy Tactics A2 , Grimoire of the Rift .
Puis dans un futur pas si lointain , fin 2010 , selon Square Enix , le jeu Final Fantasy XIV sortira simultanément dans tous les pays .
Ce jeu sera un mmorpg , la seule facon , selon Square , de renouveler une nouvelle fois la série .
Comme précisé précédemment , la série des Final Fantasy puise tout d' abord sa source dans les jeux de rôle ( ou JDR ) " américains " tels que Donjons et Dragons .
Les JDR console tels que Final Fantasy n' ont donc plus cet aspect de jeux d' acteur et se concentrent essentiellement sur la partie statistique et incarnation d' un personnage dans un univers de science fiction .
ainsi , dans Final Fantasy , il était possible d' incarner au choix un guerrier , un mage noir , un mage blanc , un mage rouge , un moine ou un voleur .
Le point fort de Final Fantasy était de proposer l' incarnation de 4 personnages à la fois , l' intérêt du jeu était donc de savoir gérer vos choix de départ quant à leur métier et d' ensuite gérer avec les forces et les faiblesses de chaque protagoniste .
Pendant la période Super Nintendo / Playstation la série était connue pour son mécanisme particulier à chaque jeu .
Final Fantasy emprunte plusieurs éléments de gameplay à son rival , la franchise Dragon Quest .
Ainsi , Final Fantasy utilise un système de combat à tours de rôle actionné à l' aide d' un menu .
La plupart des jeux dans la série utilisent un système de niveau d' expérience pour l' avancement des personnages ( bien que Final Fantasy II et Final Fantasy X n' étaient pas ainsi ) un système à base de points pour lancer les magies ( bien que Final Fantasy , Final Fantasy III et Final Fantasy VIII contenaient tous des approches différentes ) .
Souvent , ces attaques spéciales sont intégrées dans le " système de métier " qui est apparu dans plusieurs jeux de la série ( Final Fantasy III , Final Fantasy V , Final Fantasy Tactics , Final Fantasy X-2 ) .
Final Fantasy à Final Fantasy III comportaient tous un système de combat à tours de rôle .
Ce système fut aussi utilisé dans Final Fantasy Tactics , les tours à venir était visibles via le menu .
Final Fantasy XI contenait un système de combat en temps réel similaire à EverQuest : quand confronté à un ennemi , un personnage attaquait automatiquement avec des attaques physiques de base à moins que le joueur détermine autrement .
Il est indiqué que Final Fantasy XII adoptera un système semblable .
Certains citent un manque d' interactivité ( trop de scènes cinématiques ) , une structure scénaristique trop rigide ( en particulier pour Final Fantasy X ) et souvent linéaire , ainsi qu' un manque d' originalité .
Les admirateurs de ces jeux diront que la nostalgie est un facteur important pour les critiques négatives aux volets post- Final Fantasy VII .
Final Fantasy XI et Final Fantasy X-2 ont particulièrement été ciblés par certains fans pour ne pas respecter certaines règles traditionnelles : d' une part Final Fantasy XI pour avoir changé de format , passant du jeu de rôle solo au jeu en ligne multijoueur , et d' autre part Final Fantasy X-2 pour être la première véritable suite de la série ( le jeu reprend l' univers de Final Fantasy X et met en scène exclusivement des personnages féminins ) , ce qui a pu être perçue comme une démarche commerciale , et parfois assimilée à du fan service .
À partir de Final Fantasy VII le système est revenu à la normale et tous les jeux suivants ont gardé leur numéro japonais , menant à un apparent saut de 3 jeux .
En Amérique du Nord , Final Fantasy IV est sorti dans Final Fantasy Chronicles , alors que Final Fantasy V et Final Fantasy VI ont été regroupés dans la compilation Final Fantasy Anthology .
En Europe , Final Fantasy IV et Final Fantasy V ont été compilés dans Final Fantasy Anthology Edition Européenne , alors que Final Fantasy VI est sorti séparément de façon individuelle .
Séries dérivées et confusions Square a créé plusieurs séries de jeux de rôle plus ou moins apparentées à l' univers de Final Fantasy mais qu' ils faut néanmoins savoir distinguer , chose malaisée à cause du manque d' uniformité des titres des différentes traduction , entraînant très souvent des confusions .
