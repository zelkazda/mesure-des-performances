Le concept a d' abord été exploité dans le film scénarisé par Joss Whedon , et sorti le 31 juillet 1992 , Buffy , tueuse de vampires , avec Kristy Swanson dans le rôle titre .
La réalisatrice , Fran Rubel Kuzui , décrit le film comme " une comédie de culture populaire sur ce que les gens pensent des vampires " .
Ainsi la métaphore est devenue le concept central de Buffy et c' est comme ça que je l' ai vendu. "
Joss Whedon était crédité en tant que producteur délégué tout au long de la série .
Leurs droits et les royalties qu' ils touchent sont dus à leur investissement , ainsi que leur travail de production et de réalisation , sur le film Buffy .
Jane Espenson a expliqué le processus créatif en oeuvre lors de l' écriture d' une saison de Buffy .
Joss Whedon écrit d' abord l' arc narratif , puis les scénaristes écrivent les épisodes individuels .
Pour écrire un épisode particulier , les scénaristes partent des situations émotionnelles auxquelles Buffy devra faire face et comment cela interagira avec sa lutte contre les forces surnaturelles .
Elle auditionna à l' origine pour le personnage de Cordelia Chase mais décida ensuite de tenter sa chance pour le rôle de Buffy et le décrocha après plusieurs auditions .
Julie Benz , Elizabeth Anne Allen , Julia Lee et Mercedes McNab auditionnèrent également pour ce rôle mais en obtinrent finalement d' autres .
Anthony Stewart Head avait déjà eu une carrière prolifique , à la fois en tant qu' acteur et chanteur mais était surtout connu aux États-Unis pour une série de publicités pour la marque Nescafé avant d' accepter le rôle de Rupert Giles .
Contrairement aux autres acteurs réguliers de Buffy , Nicholas Brendon n' était pas un acteur expérimenté lorsqu' il a commencé la série .
Il obtint le rôle d ' Alexander Harris après quatre jours d' auditions .
La musique du générique est jouée par le groupe Nerf Herder .
Deux épisodes ont eu un générique spécial : Que le spectacle commence , dont le générique respecte les codes des comédies musicales , et Superstar où de nombreuses séquences avec le personnage Jonathan Levinson sont intégrées , comme s' il était le personnage principal de la série .
Christophe Beck a affirmé que l' équipe utilisait des ordinateurs et des synthétiseurs et ne pouvaient enregistrer qu' un ou deux " vrais " morceaux .
Au fur et à mesure de la progression de la série , de plus en plus d' épisodes contiennent de la musique rock indépendante , généralement jouée quand les personnages se rendent au Bronze .
John King , le responsable de la musique , explique qu' ils " aimaient inviter des groupes peu connus , dont la venue dans ce lieu soit crédible " .
Bien que rarement mis en avant , des artistes célèbres sont aussi apparus dans la série , tels que Sarah McLachlan , , Blink-182 , Third Eye Blind , Aimee Mann , , The Dandy Warhols , Cibo Matto , Michelle Branch et K 's Choice .
L' histoire de Buffy est racontée sous forme de feuilleton télévisé ; chaque épisode raconte une histoire originale tout en contribuant à l' avancement d' un arc narratif plus grand .
La narration est centrée sur Buffy et son groupe d' amis , surnommés collectivement le Scooby-gang , qui tentent de concilier leur lutte contre les forces du mal avec leurs vies sociales compliquées , .
Cela fait de Buffy une épopée , qui se démarque cependant du genre par un grand souci d' authenticité et de réalisme psychologique .
Lors des premières saisons , les principaux adversaires de Buffy et de ses amis sont des vampires mais , au fur et à mesure de l' avancement de la série , les antagonistes sont de plus en plus diversifiés [ réf. nécessaire ] .
Durant la première saison de la série , le créateur Joss Whedon l' a décrit comme étant " une rencontre entre Angela , 15 ans et X-Files " , soit un mélange des angoisses de l' adolescence et de surnaturel .
Il a également cité le film La Nuit de la comète comme une influence majeure et reconnu que le personnage de Kitty Pryde avait influence celui de Buffy .
Buffy est guidée , entraînée et assistée par son Observateur , Rupert Giles ( Anthony Stewart Head ) .
Son rôle est aussi de rechercher la nature des créatures surnaturelles que Buffy affronte et ainsi lui fournir des moyens de les vaincre .
Buffy est aussi aidée par les amis qu' elle a rencontrés au lycée de Sunnydale : Willow Rosenberg ( Alyson Hannigan ) et Alexander Harris ( Nicholas Brendon ) .
Willow est au début une intellectuelle réservée ; elle contraste avec la personnalité avenante de Buffy .
À mesure que la série progresse , Willow gagne en assurance , devenant une sorcière puissante .
Il aurait par exemple souhaité inclure l' acteur Eric Balfour dans le générique , afin de choquer les spectateurs lors de la mort de son personnage .
De nombreux personnages ont des rôles récurrents tout au long de la série , que ce soit en tant qu' alliés ou qu' antagonistes de Buffy .
Ainsi , Joyce Summers ( Kristine Sutherland ) , est la mère de Buffy , qui ancre les vies des personnages dans la normalité .
Jenny Calendar ( Robia LaMorte ) , professeur d' informatique au lycée de Sunnydale , Faith Lehane ( Eliza Dushku ) , une autre tueuse de vampires , Wesley Wyndam-Pryce ( Alexis Denisof ) , un observateur maniéré et timoré , le proviseur du lycée Robin Wood ( D. B. Woodside ) et la tueuse potentielle Kennedy ( Iyari Limon ) font également partie de l' entourage de Buffy durant des périodes diverses .
Chaque saison de la série comporte un ou plusieurs grands méchants opposés à Buffy pour diverses raisons .
La série comporte donc au total 144 épisodes d' une durée d' environ 40 minutes ( excepté l' épisode Que le spectacle commence qui dure presque 50 minutes ) .
Buffy contre les vampires peut être lu comme la métaphore du passage de l' adolescence à l' âge adulte et des conflits qu' il suscite .
Le romancier et essayiste Martin Winckler , auteur de nombreuses analyses sur les séries télévisées , présente Buffy comme l ' " épopée d' un groupe d' adolescents face aux démons de la vie " .
Joss Whedon a expliqué qu' un des thèmes majeurs de sa série était l' incompréhension des adultes à l' égard des adolescent-es .
Il s' appuie notamment sur un passage de l' épisode Bienvenue à Sunnydale 2/2 , où Joyce , la mère de Buffy interdit à sa fille de sortir le soir , pensant que l' adolescente veut s' amuser .
L' adulte , persuadée d' incarner la raison et la responsabilité , prétend savoir ce que pense Buffy : " Si tu ne sors pas , ce sera la fin du monde " .
En revanche , dans Buffy , ce thème exploite plutôt les remords et la culpabilité des personnages face aux mauvaises actions qu' ils ont commises dans leur passé et les moyens qu' ils mettent en oeuvre pour se faire pardonner .
Les trois principaux exemples de rédemption à travers la série sont celles respectivement d' Angel , Spike et Willow Rosenberg .
Angel est en effet un vampire doté d' une âme à la suite d' une malédiction , afin d' expier par le remords les crimes qu' il a commis en tant que " méchant " vampire .
Dans Buffy , Angel apparaît pour la première fois après avoir effectué sa rédemption .
Après sa disparition , la Force du mal le fera revenir , mais le vampire récupérera son âme et décidera de quitter Buffy pour que .
Ce thème est par la suite encore plus exploité dans la série dérivée Angel dont il est le héros .
Spike sera présenté au cours de la saison 2 comme un vampire aux idées maléfiques pour qui la rédemption n' a aucun intérêt .
Dès lors , sa rédemption commencera et se déroulera sur les trois dernières saisons de la série , puis également dans Angel , la série dérivée , lors de sa dernière saison .
C' est son amour pour Buffy qui lui permettra de se repentir une première fois .
Mais les deux amants rompront et , désespéré , il quittera Sunnydale pour récupérer son âme ( saison 6 ) .
Sur l' ensemble de la saison 7 , son repentir apparaîtra comme complet , après être passé notamment par la folie et s' être confronté à Robin Wood , le fils d' une des deux tueuses qu' il a tuées .
Dans le dernier épisode de Buffy , Spike se sacrifiera pour sauver le monde , avant d' être ressuscité dans Angel .
L' évolution de Willow sera bien plus complète et se concluera par une rédemption moins complexe que celles des deux vampires .
Sa guérison débutera après que Tara aura rompu avec elle et que son attitude irresponsable aura failli tuer Dawn .
Elle sera longue et délicate et Willow devra dire adieu à son amie Amy .
Durant ce laps de temps , elle tuera Warren et tentera de provoquer la fin du monde .
Alex la ramènera à la raison juste à temps , mais Willow aura du mal à se remettre du meurtre qu' elle a commis .
Lors de son premier baiser avec Kennedy , la culpabilité de trahir la mémoire de Tara reprendra néanmoins le dessus .
Mais Willow restera à jamais fragile à cause de la magie .
Buffy a donné naissance à une large gamme de produits , officiels ou non , et notamment une série dérivée , des romans , des comics et des jeux vidéo .
La série Angel , créée par Joss Whedon et David Greenwalt , raconte leur histoire .
Lors de la cinquième et dernière saison de cette série , les personnages de Spike et Harmony rejoignent également le générique et plusieurs des personnages principaux de la série Buffy font de brèves apparitions dans Angel , notamment Buffy elle-même , Willow , Faith , Oz et Andrew .
De son côté , le personnage d' Angel continue lui-aussi à apparaître occasionnellement dans Buffy .
Parmi les comics de Buffy contre les vampires , certains sont des réécritures des épisodes de la série ; d' autres approfondissent l' histoire de personnages ; enfin , Buffy contre les vampires , Saison huit est dans la continuité de la série télévisée , tout comme Angel : After the Fall est dans la continuité d' Angel .
Parmi les romans et nouvelles de Buffy contre les vampires , on peut noter des novellisations d' épisodes ainsi que des histoires écrites par des personnes non membres de l' équipe de création de la série .
Cinq jeux vidéo reprennent l' univers de Buffy contre les vampires .
C' est à cette occasion que Joss Whedon a révélé le nom de famille de Faith , Lehane .
Le jeu de carte à jouer Buffy contre les vampires est sorti en 2001 .
Parmi les projets qui n' ont pas été menés à terme , on peut compter des épisodes réécrits par la suite , des séries centrées sur Giles et Faith , un dessin animé et un film sur Spike .
Buffy a attiré l' intérêt de spécialistes universitaires de la culture populaire dans le cadre de leurs études sur ce sujet et la série a été inclue comme sujet d' étude littéraire et d' analyse .
La popularité de Buffy a conduit à la création de nombreux sites web de fans , de forums de discussions sur la série , de nombreuses fanfictions et même de conventions organisées par des fans et dans lesquelles sont invités des membres du casting et de l' équipe .
Buffy a marqué la télévision par sa mythologie compliquée et le véritable travail d' écriture fourni par l' équipe de scénaristes ( notamment lors des crossovers avec la série Angel ) .
Les créateurs des séries Dead Like Me et Doctor Who ont , entre autres , reconnu l' influence de Buffy sur leur travail .
D' autre part , de nombreux scénaristes de Buffy ont par la suite travaillé sur d' autres séries télévisées .
