Outre la bande dessinée , Pilote publiait également des nouvelles , romans , reportages et blagues en tous genres , et un pilotorama , une double page centrale didactique .
Son slogan devient alors " Pilote , le journal qui s' amuse à revenir " .
L' idée de Pilote naît vers la fin 1958 quand six hommes de presse et de bandes dessinées ont l' idée de créer un journal périodique pour jeunes .
Ces hommes , ont pour idée de créer une sorte de " Paris-Match pour jeune " où , même si la bande-dessinée jouera un rôle important , elle ne représentera pas l' intégralité du journal où elle côtoiera les grands sujets d' actualité .
Le nom de Pilote est finalement décidé après réflexion .
De nombreuses séries de bande dessinée franco-belge devenues connues ont commencé leur carrière dans Pilote .
