À la suite de la Première Guerre mondiale ( 1914 -- 1918 ) , certains pays connaissent une crise de régime immédiate , d' autres non .
De ce décalage et de ces difficultés naît une instabilité internationale qui met à mal les plans de cohésion avancés par les nouveaux organismes de paix et de coordination , telle la récente Société des Nations .
Pour se protéger de ce revirement et de la montée rapide de l' extrême gauche , les pays géographiquement proches de l' Union soviétique envisagent le recours ou l' affirmation de l' autoritarisme , si bien que la victoire des démocraties en 1918 ne sera que de courte durée .
Le problème des réparations financières et énergétiques mine les relations internationales dès la sortie de la guerre , jusqu' à 1929 ; le plan Young de 1930 , puis la conférence de Lausanne de 1932 , qui fixent " définitivement " les clauses de remboursement par l' Allemagne , dérivent d' une longue suite d' échecs depuis 1921 .
La Société des Nations traverse l' ensemble de ces circonvolutions historiques sans y apposer sa marque et sa dynamique .
À l' échelle nationale , tous les belligérants en dehors des États-Unis sont endettés .
L' exemple le plus probant est l' effondrement monétaire en Allemagne et dans ses satellites .
En augmentant à outrance la masse de monnaie en circulation , l' Allemagne s' adapte en pratiquant une " politique du pire " qui vise à rendre impossible tout remboursement envers les vainqueurs : en janvier 1919 , 1 mark " papier " valait 1,3 mark en or ; en décembre 1921 , ce rapport était d' un pour 46 ; en août 1923 , d' un pour un trillion .
Ce mécanisme durera jusqu' à la crise de 1929 et permettra le retour à une certaine stabilité des monnaies européennes , notamment du Mark , l' Allemagne voyant ses exportations facilités au-delà de toute espérance .
Les États-Unis sont une nouvelle fois les artisans du redressement allemand .
En collaboration avec Hjalmar Schacht , le " magicien de la finance " , les Américains concèdent des prêts importants à l' Allemagne .
Pour les assurer , Schacht gage sa nouvelle monnaie , le Rentenmark , sur l' ensemble des bénéfices du chemin de fer du pays , en octobre 1923 .
À la fin des années 1920 , Raymond Poincaré fait en sorte de redresser et d' assainir la monnaie nationale , en faisant jouer la confiance , les mécanismes d' épargne et la dévaluation .
Comme en Allemagne , la dévaluation change de statut auprès des classes dirigeantes : d' échec politique , il devient un outil -- voire une arme -- économique .
