Le canton de Zurich est un canton de Suisse , dont le chef-lieu est Zurich .
Elle devint une ville libre en 1218 , entra dans la Confédération suisse en 1351 et y prit une grande importance .
Huldrych Zwingli introduisit la Réforme dans le canton et la ville de Zurich à partir de 1519 .
Aujourd'hui , le canton de Zurich est le premier canton de la Confédération par l' importance de sa population , plus que deux fois plus nombreuse que celle du Canton de Genève .
Le canton de Zurich se trouve vers 47,5 degré de latitude nord et 8,6 degré de longitude est .
Il est situé au nord-est de la Suisse .
Il a comme voisin l' Allemagne , le Canton d' Argovie , le Canton de Saint-Gall , le Canton de Schaffhouse , le Canton de Schwytz , le Canton de Thurgovie et le Canton de Zoug .
La frontière avec l' Allemagne est sur le Rhin , elle mesure 41.8 km , celle avec le canton de Argovie en fait 62,4 , celle avec celui de Saint-Gall 34,2 , celle avec Schaffhouse 23,1 , celle avec Schwytz 34,3 , celle avec Thurgovie 82,5 et celle avec Zoug 31,7 .
La commune la plus au sud est Hütten , celle la plus au nord est Feuerthalen , celle la plus à l' est est Fischenthal et celle la plus à l' ouest est Niederweningen .
Le canton de Zurich fait 59.6 km par un axe nord-sud et 47,5 par un axe ouest-est .
La Sihl part depuis le canton de Schwytz et arrive dans celui de Zurich part le sud-ouest , elle passe dans tout l' ouest-sud-ouest du canton , elle se jette dans la Limmat dans la ville de Zurich .
Les régions du nord , ont un relief des plus plats du canton , les plus hauts reliefs dépassent rarement 600 m , et le sol en général est aux alentours de 450 m , les vallées de cette région donnent sur le Rhin .
La région de l' aire urbaine zurichoise , basée sur la ville de Zurich , a déjà un reliefs plus mouvementé , autour de la ville il y a au sud-ouest une grande colline appelée Uetliberg , qui monte jusqu' à 869 m , à l' est il y a une autre colline , Zürichberg , qui monte à 679 m , dans la région la plupart des collines sont gardée pour des espaces naturels ; la principale vallée se déverse vers l' ouest sur celle de l' Aar .
Son chef-lieu est la ville de Zurich et les principales villes sont Bülach , Winterthour , Dietikon et Uster .
La région du lac de Zurich est essentiellement utilisée comme zone de résidence et de loisirs , jouissant en outre d' un fort potentiel touristique .
Les activités agricoles sont néanmoins préservées dans le nord du canton ( vignes ) et dans les Préalpes ( fruits , céréales , vignes , élevage ) .
À noter que ce n' est pas l' allemand d' Allemagne qui est parlé mais l' alémanique zurichois , lequel diffère quelque peu des autres parlers alémaniques helvétiques .
