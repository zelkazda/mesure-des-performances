Victor Schoelcher naît le 22 juillet 1804 à Paris dans une famille bourgeoise originaire de Fessenheim en Alsace .
Il fait de courtes études au lycée Condorcet , côtoyant les milieux littéraires et artistiques parisiens , faisant connaissance avec George Sand , Hector Berlioz et Franz Liszt .
Son père l' envoie au Mexique , aux États-Unis et à Cuba en 1828 -- 1830 en tant que représentant commercial de l' entreprise familiale .
Lorsqu' il est à Cuba , il y est révolté par l' esclavage .
De retour en France , il devient journaliste et critique artistique , publiant des articles , des ouvrages , multipliant ses déplacements d' information .
Le discours abolitionniste de Schoelcher évolue au cours de sa vie .
Lors du coup d' État du 2 décembre 1851 , il fut un des députés présents aux côtés de Jean-Baptiste Baudin sur la barricade où celui-ci sera tué .
Il s' exile en Angleterre où il rencontre fréquemment son ami Victor Hugo ; il y devient un spécialiste de l' œuvre du compositeur Georg Friedrich Haendel , rassemble une collection très importante de ses manuscrits et partitions et rédige une de ses premières biographies , mais celle-ci n' est éditée que dans sa traduction anglaise .
En 1870 , il revient en France suite à la défaite de Sedan .
En 1877 , Victor Schoelcher dépose une proposition de loi pour interdire la bastonnade dans les bagnes .
En 1884 et 1885 il tente de s' opposer , sans succès , à l' institution de la relégation des forçats récidivistes en Guyane .
En 1952 , un billet de 5000 francs à l' effigie de Victor Schoelcher est mis en circulation en Martinique .
La commune de Fessenheim a fait de la maison de la famille Schoelcher un musée qui porte son nom .
