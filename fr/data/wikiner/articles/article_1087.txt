Le Portable Document Format ( communément abrégé PDF ) est un langage de description de pages d' impression créé par Adobe Systems .
La spécificité du PDF est de préserver la mise en forme ( polices d' écritures , images , objets graphiques … ) telle qu' elle a été définie par son auteur , et ce quelles que soient l' application et la plate-forme utilisées pour imprimer ou visualiser ledit fichier .
Le Portable Document Format , généralement abrégé PDF , est un format de fichier informatique créé par Adobe Systems , comme évolution du format PostScript .
L' avantage du format PDF est qu' il préserve les polices , les images , les objets graphiques et la mise en forme de tout document source , quelles que soient l' application et la plate-forme utilisées pour le lire .
Le format PDF peut aussi être interactif .
C' est pourquoi ce format est utilisé dans un ensemble large et varié de logiciels , de l' exportation dans les suites bureautiques grand public , aux manipulations par des programmes spécialisés de l' industrie artistique , en passant par la génération de factures électroniques via Internet .
Plus techniquement , les fichiers PDF peuvent être créés avec des options personnalisées , tant aux niveaux de la compression des images et des textes , de la qualité d' impression du fichier , que du verrouillage ( interdiction d' impression , de modification … ) .
Le PDF s' est imposé comme format d' échange ( consultation écran , impression , etc ... ) et d' archivage de documents électroniques , il est devenu un " standard international " .
L' adoption du PDF au début de l' existence du format fut lente .
Aussi dans les premières années , les fichiers PDF étaient surtout utilisés dans les processus de publication assistée par ordinateur .
Adobe commença alors à distribuer gratuitement son programme Acrobat Reader ( aujourd'hui Adobe Reader ) , et maintint la compatibilité avec le PDF original , qui devint progressivement le standard de facto pour les documents imprimables sur Internet ( un document web standard ) .
Le format de fichier PDF a changé plusieurs fois , et continue d' évoluer , parallèlement à la sortie de nouvelles versions d' Adobe Acrobat .
Il y a eu neuf versions de PDF et la version correspondante du logiciel :
Le format PDF préserve la mise en forme du document source .
En effet , il intègre dans le fichier PDF les polices utilisées pour la création du document .
Pour garantir la portabilité , quelques précautions sont donc à prendre lors de la génération du fichier PDF .
Il s' agit d' utiliser ( et d' intégrer dans le PDF ) des polices redistribuables .
Pour ceci , la spécification PDF indique qu' un ensemble minimal de 14 polices est fourni en standard .
Le PDF est consultable sur de très nombreux appareils communicants ( ordinateurs , assistants personnels numériques / PDA , nouveaux téléphones hybrides … ) .
De nombreux autres lecteurs dont certains sont des logiciels libres , existent également ( Sumatra PDF , Xpdf , gv ... ) .
Il y a des logiciels gratuits comme STDU Viewer qui reconnaissent le format PDF .
De nouveaux procédés , comme le papier électronique , sortent actuellement des laboratoires et rentrent en production , donnant un nouveau souffle à ces débouchés du PDF .
La création du format PDF date de 1993 .
Deux sous-ensembles du format PDF ont également été normalisés par l' ISO , il s' agit des formats PDF/A-1 et PDF/X .
PDF est un format ouvert , c' est-à-dire que ses spécifications sont connues et que son créateur Adobe Systems autorise des programmes tiers à réutiliser son format .
