Le Parti Révolutionnaire Institutionnel ou PRI ( ( es ) Partido Revolucionario Institucional ) est une des principales forces politiques du Mexique .
Il est né en 1929 sous l' impulsion du Général Calles .
Le président Cárdenas qui milita dans l' aile gauche du PRI reçut en 1955 le prix Lénine pour la paix .
Son président pour la période 1939 -- 1940 , le général Heriberto Jara Corona reçut le prix Staline pour la paix en 1950 .
L' article 7 de la déclaration de principes du PRI le définit comme un parti social démocrate .
Après 70 ans à la tête du pays , le PRI perd l' élection présidentielle du 2 juillet 2000 au profit du candidat du PAN ( Partido Acción Nacional ) Vicente Fox Quesada .
