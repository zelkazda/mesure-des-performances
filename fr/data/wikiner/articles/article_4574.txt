La commune est située au nord du Bocage virois .
Le territoire et son bourg sont traversés du sud au nord par la route départementale 674 de Vire à Carentan .
Campeaux est bordé au sud par la Vire où le fleuve emprunte une vallée étroite ( les gorges des la Vire ) à partir de sa confluence avec la Souleuvre .
La sortie 40 de l' autoroute A84 est à 4 km au nord du bourg .
Campeaux a compté jusqu' à 921 habitants en 1841 .
