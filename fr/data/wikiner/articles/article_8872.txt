BeOS est un système d' exploitation développé par la société américaine Be Inc. , fondée par le français Jean-Louis Gassée , un ex-dirigeant d' Apple à partir de 1991 .
Initialement conçu pour un ordinateur spécifique , la BeBox , il a d' abord été adapté au Macintosh , puis au PC en 1998 .
BeOS est un système présentant des caractéristiques innovantes pour l' époque :
Il n' y aura plus de version éditée par Be Inc. , qui a été rachetée par Palm en 2001 .
Toutefois , plusieurs projets libres et/ou gratuits contribuent au développement de différents successeurs à BeOS 5 .
Cette version était basée sur la version 5.1 de BeOS qui était encore en version bêta dans les bureaux de Be Inc .
lors de son rachat par Palm .
Il sera ensuite à la tête du groupe Newton PDA , qui pour rappel était l' un des premiers PDA .
Jean-Louis comprit " Be " -- être en anglais -- et trouva que cela sonnait bien .
Ils se sont alors rendu compte du quiproquo , en rirent et décidèrent de garder " Be " comme nom de société .
Leur rêve était de concevoir un système nouveau , dédié aux applications multimédia , dans la lignée des Amiga .
Le 3 octobre 1995 , la première BeBox fait son apparition publique et génère l' effervescence des utilisateurs et des développeurs .
Le système d' exploitation de la BeBox trouvera enfin son nom en février 1996 : BeOS .
La société interrompt la production de ses BeBox en janvier 1997 , elle concentre alors ses efforts sur le système d' exploitation .
BeOS attire déjà l' attention de nombreux développeurs .
Apple , qui recherche alors de nouvelles technologies pour son futur système d' exploitation ( qui deviendra Mac OS X ) , voit BeOS à la fois comme un concurrent et comme un associé potentiel .
L' entreprise demandant un prix trop élevé selon Apple , 200 millions de dollars , ce dernier préférera investir le double dans la plate-forme NeXTSTEP de Steve Jobs , ce qui n' est pas surprenant étant donné la position que celui-ci avait occupé chez Apple , auparavant .
Il est disponible pour PC et un port pour PowerPC apparaît logiquement un mois après .
Avec une cinquantaine d' ingénieurs seulement , BeOS 4 arrive en novembre 1998 : le système continue de séduire toute une communauté d' utilisateurs , BeOS est rapide , simple , intuitif et stable .
L' entreprise est trop petite pour prétendre à concurrencer Microsoft et c' est d' ailleurs dans l' esprit de Be Inc .
Be ne parvient pas à signer de contrat avec les fabricants , cela prive ainsi l' entreprise du principal vecteur de distribution .
BeOS fonctionne toujours sur les BeBox , les PowerPC et les processeurs x86 .
Outre les petites phrases Haiku , il permet de consulter le développement interne .
L' entreprise est cotée au Nasdaq le 20 juillet , elle propose 6 millions d' actions .
En janvier de l' an 2000 , l' entreprise n' arrive toujours pas à atteindre la rentabilité sur un marché largement dominé par Microsoft .
Be Inc .
Les finances s' amenuisent et Be Inc .
Depuis , tous ces projets sont à l' abandon , seul Haiku ( anciennement OpenBeOS ) est toujours actif .
Pourtant ce projet est le plus ambitieux , car ayant pour objectif de redévelopper de zéro BeOS , tout en gardant la compatibilité au niveau de l' exécutable ( là où les autres projets gardaient une compatibilité avec les sources uniquement ) .
Aujourd'hui , Haïku démarre avec l' interface graphique et plusieurs logiciels fonctionnent , dont un des plus spectaculaire est Quake 3 .
BeOS a inspiré un autre projet de système d' exploitation libre , Syllable , qui lui est toujours actif et déjà bien avancé .
Elle apporte maintes nouveautés , telles des icônes vectorielles , une réécriture complète de la couche réseau , un support plus complet de l' USB .
D' autres technologies sont annoncées telles que Java ou encore OpenGL .
