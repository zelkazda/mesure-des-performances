Le Lot est une rivière française du Massif central , affluent en rive droite de la Garonne .
Le Lot se distingue notamment par ses longs méandres et ses boucles qui commencent à partir de Cajarc dans le Lot jusqu' à Fumel dans le Lot-et-Garonne .
Le débit moyen interannuel ou module de la rivière à Villeneuve-sur-Lot est de 151 m³ par seconde .
Le Lot présente des fluctuations saisonnières de débit très importantes , avec des crues d' hiver-printemps , portant le débit mensuel moyen situé entre 193 et 289 m³ par seconde , de fin novembre à début mai inclus , avec un maximum en février , et des basses eaux d' été , de juillet à septembre , avec une baisse du débit moyen mensuel atteignant le plancher de 32,5 m³ par seconde au mois d' août , ce qui reste malgré tout assez confortable .
Le débit spécifique se monte à 14,1 litres par seconde et par kilomètre carré de bassin , un peu moins que son voisin le Tarn ( 15,1 ) .
Avant l' étabissement des chemins de fer , le Lot était une très importante voie navigable .
Ce parcours surnommé " La ligne " permit également échanger des richesses entre le Massif central et l' Aquitaine .
Depuis 1991 grâce à une politique de réaménagement , le Lot est à nouveau navigable pour les plaisanciers sur deux sections , de la Garonne à Lustrac pour le département de Lot-et-Garonne et de Luzech à Larnagol pour le département du Lot .
Des travaux d' aménagements sont prévus pour rouvrir le Lot entre Lustrac et Luzech .
Neuf écluses seront reconstruites d' ici à 2012 entre Soturac et Castelfranc pour un coût de 13,5 millions de francs .
Cette continuité permettra d' assurer la navigation sur le Lot à travers la plaine d' Aquitaine jusqu' au relief verdoyant du Massif central avec leurs paysages variés et leurs sites touristiques .
Le Lot fut la plus longue rivière canalisée du bassin de la Garonne traversant la Guyenne le Quercy et le Rouergue ( actuelles régions Aquitaine et Midi-Pyrénées ) avec 297 km classés navigables .
