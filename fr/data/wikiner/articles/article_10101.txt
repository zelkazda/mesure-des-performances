Son père , parti combattre en Europe peu après sa naissance , est ensuite retourné au Canada .
Il sera plus tard définitivement abandonné par sa mère , partie à son tour au Canada au bras d' un autre soldat .
L' enfance de Clapton ne se passe pas sans accrocs -- il a plus tard confessé avoir été un " sale gosse " .
Encore adolescent , Eric Clapton puise son inspiration musicale -- il est finalement parvenu à jouer un peu de guitare -- dans le blues américain : Big Bill Broonzy , Muddy Waters , Elmore James , Howlin ' Wolf et surtout Robert Johnson , le légendaire bluesman du Mississippi .
Dans ce même club jouent les Blues Incorporated d' Alexis Corner , dont les batteurs et bassistes sont , de temps à autres ( le personnel varie beaucoup à cette époque ) , Ginger Baker et Jack Bruce , ses futurs acolytes de Cream .
Mais cette nouvelle association ne dure qu' un mois : Clapton , qui a déjà acquis une certaine réputation en tant que guitariste , est embauché par les Yardbirds , qui seront son premier groupe véritablement professionnel .
Avec l' arrivée de Clapton , les Yardbirds commencent à véritablement décoller .
Clapton , de son côté , crée peu à peu son style personnel : façon de jouer bien sûr , synthétisant de manière révolutionnaire les influences de Buddy Guy , Freddie King et B.B. King , mais aussi façon de s' habiller .
En effet , à cette époque , Clapton est encore un fanatique de blues authentique , qui considère comme une trahison de jouer autre chose que des reprises de grands bluesmen .
Une épopée qui s' achève rapidement en Grèce , d' où ils reviennent sans un sou après avoir eu un accident de la route et s' être presque fait kidnapper à Athènes .
Clapton reprend donc dès octobre 1965 sa place au sein des Bluesbreakers …
En mars 1966 , Clapton , Mayall et les Bluesbreakers enregistrent l' album Blues Breakers -- John Mayall with Eric Clapton .
Quand l' album sort , Clapton a déjà quitté le groupe .
Il vient en effet de former avec Jack Bruce et Ginger Baker un " supergroupe " qui deviendra bientôt Cream .
Formé par trois musiciens déjà très célèbres -- Eric Clapton , Ginger Baker et Jack Bruce -- Cream est le premier " supergroupe " , et l' un des premiers " power-trios " célèbres .
Il est aussi pour Clapton l' occasion de développer sa technique de chant et ses talents de guitariste et d' auteur de chansons .
Un terrain fertile , à partir duquel Clapton crée un style de guitare plus expérimental que jamais : les concerts du groupe sont l' occasion de très longues improvisations à un volume délirant pour l' époque , où Clapton , qui doit assurer à la fois la rythmique et les solos , est forcé de se surpasser .
D' autant que , malgré les déclarations enthousiastes de la presse et de leurs proches , l' ambiance n' est pas toujours au beau fixe entre les membres de Cream : leur association se fonde plus sur une rivalité parfois brutale que sur une réelle émulation .
En 1967 , la popularité de Clapton est quelque peu entamée par l' arrivée à Londres de Jimi Hendrix , dont le style flamboyant concurrence le sien .
Mais Clapton continue à être désigné , par tous les sondages des magazines , comme le " meilleur guitariste du monde " , et le succès d' Hendrix n' empêche pas Cream de vendre environ 5 millions de disques en 40 ans ( y compris best of et singles ) , rien qu' aux États-Unis
Après trois albums , le groupe est cependant victime de l' inimitié qui règne entre ses trois membres , mais aussi des hésitations de Clapton .
Le dernier album de Cream , Goodbye , disque en partie live , paraît donc en 1969 après la dissolution du groupe .
Il contient , entre autres , la chanson Badge , première collaboration de Clapton avec son ami George Harrison , guitariste des Beatles .
Après la séparation de Cream , Clapton fonde un nouveau supergroupe , Blind Faith , avec l' ancien organiste et chanteur de Traffic Steve Winwood , à qui s' ajoute le batteur Ginger Baker de Cream .
La frénésie que déclenche chez les fans la création de ce nouveau groupe dépasse encore celle suscitée par la formation de Cream .
Blind Faith apparaît pour la première fois en public devant une foule de 100 000 personnes à Hyde Park , le 7 juin 1969 .
Mais les musiciens , en particulier Clapton , semblent tendus , et beaucoup parmi le public sont déçus .
Pendant l' expérience Blind Faith , Eric Clapton a fait la connaissance de Delaney et Bonnie , un couple de musiciens " simples , naturels , libres et pas du tout vaniteux " , qui prennent simplement plaisir à jouer : tout le contraire des supergroupes , dont il est si fatigué .
Delaney et Bonnie ne l' acceptent pas [ réf. nécessaire ] , et la séparation intervient en 1970 .
Elle fut utilisée pour l' enregistrement de nombreux disques , dont Layla and Other Assorted Love Songs .
Fin 1970 , Clapton débauche la section rythmique de Delaney & Bonnie ( le claviériste Bobby Whitlock , le bassiste Carl Radle et le batteur Jim Gordon ) et forme un nouveau groupe , Derek and the Dominos .
Le groupe entre rapidement en studio pour enregistrer son premier album , aujourd'hui considéré par beaucoup comme le chef-d'œuvre de Clapton .
La suite de la carrière du groupe est cependant nettement moins brillante : ravagé par la nouvelle de la mort de Jimi Hendrix , Clapton commence à augmenter sérieusement sa consommation de drogues et d' alcool .
Pire encore , Duane Allman meurt brutalement d' un accident de moto le 29 octobre 1971 , juste avant le début de la tournée américaine de Derek and the Dominos .
Au début des années 70 , la vie de Clapton devient pour le moins chaotique : la fin tragique de Derek and the Dominos , groupe qui avait pourtant commencé sous les meilleurs auspices , et son amour malheureux pour Pattie Boyd plongent le musicien dans la déprime .
Il cesse d' enregistrer et d' apparaître publiquement , et se retire dans sa résidence du Surrey .
Commence une terrible période de dépendance à cette drogue qui le marquera à vie : durant trois ans , Clapton n' enregistre pas , et ne sort de sa retraite qu' à quelques rares occasions comme le concert pour le Bangladesh organisé par George Harrison en août 1971 .
Le public peut alors avoir un aperçu de l' état de délabrement de sa santé : Clapton s' évanouit sur scène , et doit être réanimé avant de continuer à jouer .
Le concert produit l' effet escompté : Clapton suit ensuite une cure de désintoxication , et parvient à surmonter sa dépendance à l' héroïne .
Inspiré par son voyage en Jamaïque l' année précédente , au cours duquel il avait rencontré le jeune et encore inconnu Bob Marley , le disque comprend peu de solos de guitare .
Les années suivantes voient Clapton continuer à sortir des albums , qui se situent musicalement dans la lignée de 461 ... plutôt que de Derek and the Dominos : peu de solos , et des chansons mieux écrites .
Clapton veut dépasser sa réputation de " plus grand guitariste du monde " ( le titre original et ironique de l' album There 's one in every crowd de 1975 ) pour devenir un auteur de chansons reconnu .
Mais les ennuis de Clapton ne sont pas terminés pour autant : le musicien continue à boire bien plus que de raison , et en 1976 , déclenche une violente polémique lors d' un concert à Birmingham .
Ces propos , qui font écho à ceux d' artistes comme David Bowie ou Siouxsie Sioux à la même époque , provoquent un tollé général , et sont sans doute pour beaucoup dans la création du mouvement anglais Rock Against Racism .
Refusant à l' époque de revenir sur ses déclarations , et affirmant ne pas voir de contradiction entre elles et son amour pour la musique noire , Clapton finira par les attribuer à son état passablement alcoolisé au moment des faits .
D' autre part , de nombreux faits vont contre la thèse d' un Eric Clapton chanteur du racisme : outre les inspirations noires prééminentes dans sa musique , il a partagé la scène à de très nombreuses reprises avec des artistes noirs , et a eu une liaison avec le top model noir Naomi Campbell .
Les disques que Clapton réalise dans les années 80 se plient à la mode des synthétiseurs et des boîtes à rythme .
August , sorti en 1986 et produit par Phil Collins , est l' un de ses plus grands succès .
Il part ensuite pour une tournée de deux ans aux côtés de Collins et remporte des récompenses pour son travail .
L' album Journeyman , enregistré en 1989 avec des collaborations de George Harrison des Beatles , Robert Cray , Daryl Hall , Chaka Khan , Mick Jones du groupe Foreigner et Phil Collins de Genesis , confirme aux yeux du public la renaissance artistique de Clapton .
Puis , au début des années 1990 , deux tragédies majeures touchent Clapton .
Il y aura tout d' abord la mort , le 27 août 1990 , du guitariste Stevie Ray Vaughan , alors en tournée avec Clapton .
Vaughan se trouve avec deux membres de son équipe dans un hélicoptère qui s' écrase lors d' un trajet entre deux concerts .
Clapton devait initialement faire partie du vol , avant de laisser sa place à Stevie Ray Vaughan .
Les deux camarades commencent par une tournée au Japon , où ils reprennent les vieux standards de ce dernier .
Les années suivantes , Clapton partage son temps entre des collaborations avec Carlos Santana et B. B. King , qui remportent un immense succès , et des albums de musique électronique , qui déchaînent beaucoup moins d' enthousiasme .
L' album Reptile ( 2001 ) , sans rencontrer un très grand succès , sera toutefois bien accueilli : Clapton y chante et joue avec beaucoup de son ancienne conviction .
Le grand absent de ce festival , Gregg Allman , qui avait subi avec succès une greffe du foie le 23 juin , est remplacé par Derek Trucks et Susan Tedeschi .
En 2005 Eric Clapton reforme Cream avec Jack Bruce et Ginger Baker pour une série de concerts qui se tiennent les 2 , 3 , 5 et 6 mai au Royal Albert Hall de Londres , ainsi qu' au Madison Square Garden de New York quelques mois plus tard .
Les concerts sont enregistrés et font l' objet d' une publication sous forme de DVD et CD , à la fin de la même année .
En février 2009 , Clapton et Steve Winwood donnent un concert au Madison Square Garden , à New York .
Le concert a été enregistré , et un CD et un DVD sont sortis en mai 2009 .
Eric Clapton reste lié à l' image de guitares mythiques , et en particulier certaines , qui l' accompagnent depuis des années :
