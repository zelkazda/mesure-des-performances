La Central Intelligence Agency ou CIA , fondée en 1947 par le National Security Act , est l' une des agences de renseignement les plus connues des États-Unis .
Le quartier général de la CIA est actuellement et depuis 1961 sur le site de Langley , dans la ville de McLean en Virginie , aux États-Unis , à environ 40 km de Washington .
On estime qu' elle emploierait environ 16000 personnes rien qu' à son quartier général de Langley , et environ 23500 au total dans le monde entier .
Le chiffre de 28 milliards est souvent cité car révélé en 1987 mais il s' agit du budget de toute l ' Intelligence Community , dont la part de la CIA à l' époque n' était que d' un milliard .
La CIA s' organise en quatre directions principales :
La CIA est chargée de deux rôles : d' une part fournir et analyser des informations sur les gouvernements , les entreprises et les individus de tous les pays du monde pour le compte du gouvernement américain , d' autre part conduire des opérations clandestines à l' étranger .
Actuellement la CIA est sérieusement réglementée et surveillée par les pouvoirs exécutifs et législatifs américains , bien que ce n' ait pas été toujours le cas par le passé .
De la création de la CIA au milieu des années 1970 , aucun contrôle parlementaire n' a été établi sur " l' agence " ( ni sur les autres services de renseignements américains ) .
12 036 ) et Ronald Reagan ( E.O .
L' agence est la descendante de l' OSS , dissous en octobre 1945 ; William Donovan , son créateur , propose alors à Harry Truman la création d' une nouvelle agence directement sous l' autorité du président .
En 1947 , il est transformé en CIA .
La NSA sera créée peu de temps après en 1952 .
En 1949 , la CIA obtient l' autorisation d' utiliser des procédures fiscales et administratives confidentielles et devient exemptée des limitations habituelles dans l' utilisation du budget fédéral .
La CIA a été créée suite au début de la Guerre froide , ce qui explique qu' à l' origine toute l' action de l' agence ( aussi bien le renseignement que les opérations clandestines ) est initialement dirigée contre l' Union soviétique et le bloc communiste , considérés comme le principal adversaire des États-Unis .
La CIA est donc le principal élément de la politique de l' endiguement du communisme édictée par Harry Truman agissant au-delà du rideau de fer .
Les actions de la CIA reprennent souvent les tactiques de l' OSS pendant la Seconde Guerre mondiale , comme la propagande et des liens avec des groupes de résistants .
Après les premières années de la Guerre froide , les États-Unis et l' Union soviétique comprennent que du fait de la dissuasion nucléaire la guerre a peu de chance d' éclater .
Mais en URSS , bon nombre de tentatives de renseignement , en particulier des défections , sont bloquées par James Jesus Angleton , le chef du contre-espionnage de la CIA qui est persuadé de l' infiltration de ses services par le KGB .
Il faudra attendre 1953 pour que le premier officier de la CIA arrive en poste a Moscou .
Les États-Unis et l' URSS vont rapidement se lancer dans une nouvelle rivalité : installer des gouvernements alliés dans un maximum de pays .
C' est là que la CIA va mener la plupart de ses actions dans les décennies suivantes , en renversant ou en aidant à renverser des pouvoirs considérés comme hostiles .
Également pour contrer l' influence communiste , la CIA parvient à se procurer une copie du rapport secret de Nikita Khrouchtchev dénonçant les crimes de Staline au XX e congrès du PCUS , qui est publiée dans le New York Times le 16 mars 1956 ( le discours de Khrouchtchev a eu lieu le 25 février ) .
La CIA va échouer sur ses tentatives de renversement de Castro à Cuba , notamment avec le retentissant échec du débarquement de la Baie des Cochons le 16 avril 1961 , puis plusieurs tentatives d' assassinat du dirigeant cubain .
Depuis au minimum les années 1960 , la CIA dispose de stations également sur le territoire des États-Unis .
Revenu en Afghanistan , il a été retourné par les services secrets afghans et soviétiques .
Mikhaïl Gorbatchev avait déclaré peu après la chute de l' URSS : " J' ai fait la pire chose qui pouvait arriver aux États-Unis : je leur ai enlevé leur meilleur ennemi " .
Cette remarque s' applique particulièrement à la CIA , dont la structure avait été créée pour lutter contre le communisme et l' URSS .
À partir de la fin des années 1980 et de la direction de Robert Gates , la CIA cherche à s' adapter à la nouvelle situation mondiale et aux nouveaux problèmes qui menacent les États-Unis telle la guerre économique .
Onze de ses agents sont officiellement morts durant la guerre d' Afghanistan entre 2001 et 2009 dont sept le 30 décembre 2009 suite à un attentat-suicide .
Au tout début de ce conflit , une centaine d' agents de la CIA accompagné les 300 hommes des United States Special Operations Command sur place .
En juillet 2010 , on fait état de 22 employés de la CIA dont 8 membres de sociétés privés tués dans la guerre contre le terrorisme .
Le journal The Washington Post révèle l' existence d' un réseau mondial de centres de détention clandestins ( black sites ) géré par la CIA .
Avec l' arrivée de l' administration Obama , Michael Hayden , directeur sortant , a écrit une liste de préoccupations par ordre d' importance pour la CIA en 2009 :
Elle a créé ou soutenu plusieurs mouvements insurrectionnels , qu' ils soient armés ou non armés ( Solidarność , en Pologne [ réf. nécessaire ] ) .
Le DCI dirige la CIA et toute l' Intelligence Community .
Forte d' environ 23 500 employés , la CIA dispose d' un budget annuel estimé à 3,1 milliards de dollars .
En 2003 , la plus importante promotion de nouveaux agents de la CIA depuis 50 ans est arrivée .
Seuls les citoyens américains peuvent postuler à la CIA .
Il s' agit d' un terrain aménagé par les forces spéciales de l' United States Navy , plus connu pour abriter le site des entraînements de la CIA .
En 1972 , la " gazette de Virginie " a prétendu que la CIA y formait des tueurs , envoyés ensuite à l' étranger pour éliminer les cibles gênantes .
Le CIA World Factbook est une source documentaire sur les pays du monde éditée par la CIA , libre de droits .
