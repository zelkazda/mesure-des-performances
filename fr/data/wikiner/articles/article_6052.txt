Ses travaux lui valurent en 1844 la Royal Medal de la Royal Society , puis une chaire de mathématiques à l' université de Cork en 1849 .
Issu d' une famille pauvre , George Boole n' a pas les moyens financiers d' aller à l' université .
Boole y développe une nouvelle forme de logique , à la fois symbolique et mathématique .
Les travaux de Boole , s' ils sont théoriques , n' en trouveront pas moins des applications primordiales dans des domaines aussi divers que les systèmes informatiques , la théorie des probabilités , les circuits électriques et téléphoniques , etc .
Et en 1857 , il est nommé membre de la Royal Society .
George Boole meurt d' une pneumonie le 8 décembre 1864 .
