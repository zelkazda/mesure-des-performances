Yalta est une ville de la république autonome de Crimée , en Ukraine .
Elle est située au bord de la mer Noire , entourée de montagnes boisées à 51 km au sud de Simferopol .
Elle comptait 80140 habitants en 2005 ; son agglomération , le " Grand Yalta " , en compte environ 150000 .
La ville a connu beaucoup de difficultés économiques après la fin de l' Union soviétique .
L' idée que la conférence de Yalta aurait impliqué un véritable partage du monde est un point de vue largement répandu .
C' est même plutôt le véritable acte fondateur des Nations unies .
