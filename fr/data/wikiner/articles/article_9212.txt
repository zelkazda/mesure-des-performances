Le Gouvernement révolutionnaire de Zanzibar est une entité administrative autonome de la Tanzanie qui réunit cinq régions de la Tanzanie couvrant deux des trois îles principales de l' archipel de Zanzibar : Unguja et Pemba .
Elle couvre le territoire insulaire correspondant à l' ancien sultanat de Zanzibar , la partie continentale de ce sultanat ayant été rattaché au protectorat de Tanganyika le 10 décembre 1963 .
Le Gouvernement révolutionnaire de Zanzibar est composé des îles principales d' Unguja et de Pemba et des petites îles situées à proximité .
Pemba , la plus au nord des deux îles principales , est divisée en deux régions , Pemba nord et Pemba sud , dont quatre districts .
L' autre île principale de l' archipel de Zanzibar , Mafia , ne fait pas partie du Gouvernement révolutionnaire de Zanzibar et est gérée par la région de Pwani .
Bien que rattaché à la Tanzanie , Zanzibar bénéficie d' une certaine autonomie politique mais dépend totalement du pouvoir tanzanien à propos des questions économiques , financières , juridiques , de politique étrangère et de maintien de la paix à l' étranger et dans l' archipel .
Le pouvoir est aux mains du président qui est ministre de Tanzanie .
Freddie Mercury , leader du groupe britannique Queen , est né à Zanzibar le 5 septembre 1946 et l' a quitté à l' âge de 5 ans .
