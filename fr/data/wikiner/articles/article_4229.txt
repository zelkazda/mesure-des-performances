Située dans le centre-ouest des États-Unis , dans la vallée de la rivière South Platte sur le flanc ouest des Grandes Plaines , la ville est à moins d' une trentaine de kilomètres à l' est du Front Range , la cordillère orientale des montagnes Rocheuses .
Denver dispose de deux centres-villes situés côte à côte .
Néanmoins , l' artère principale de ce centre ( 16th Street ) est piétonne .
Ce quartier dénommé Lower Downtown ou LoDo , constitué d' immeubles de trois ou quatre étages , a gardé son cachet d' époque .
Ce fut l' une des premières colonies à s' implanter à l' emplacement de la ville actuelle de Denver .
Le 22 novembre 1858 , le général William Larimer , un spéculateur du Kansas , installa des cabanons en bois sur la colline surplombant les rivières South Platte et Cherry Creek à proximité d ' Auraria .
Denver était , à l' époque , une ville frontière dont l' économie reposait sur l' extraction minière et sur les services à destination de la clientèle des mineurs comme les casinos , les saloons , les commerces de marchandises et de vivres .
La cité fut le siège du comté d' Arapahoe jusqu' à sa consolidation en 1902 .
Entre 1880 et 1895 , la cité fut le théâtre d' une forte corruption et les élus étaient souvent liés à des chefs mafieux comme Soapy Smith .
En 1890 , Denver était la deuxième plus grande ville située à l' ouest d' Omaha et était toujours troisième en 1900 après San Francisco et Los Angeles .
Suivant une tendance plus générale de découpage des comtés , l' assemblée générale du Colorado décida , en 1901 , de découper le comté d' Arapahoe en trois parties : la cité-comté de Denver , le comté d' Adams et le comté de South Arapahoe .
La création de la cité-comté de Denver ne fut effective finalement que le 15 novembre 1902 .
Avant la Seconde Guerre mondiale , de nombreux organismes fédéraux se sont installés dans la ville , dont l' United States Geological Survey , l' United States Mint , le Bureau of Land Management et une base de l' US Air force .
Le Comité international olympique dut trouver en catastrophe une autre ville pour la remplacer et Innsbruck fut choisie .
Denver est entourée par trois autres comtés : le comté d' Adams au nord et à l' est , le comté d' Arapahoe à l' est et au sud , et le comté de Jefferson à l' ouest .
Denver s' est construite au centre du Front Range Urban Corridor , un corridor en plaine compris entre les montagnes Rocheuses à l' ouest et les Grandes Plaines à l' est .
Selon le Bureau du recensement des États-Unis , la ville a une superficie de 401.3 km² , dont 4.1 km² d' étendues d' eau .
De par son climat semi-aride , Denver possède une flore assez variée .
Denver possède un climat semi-aride , .
Alors que Denver est située dans les Grandes Plaines , le climat de la région est fortement influencé par les montagnes Rocheuses situées juste à l' ouest .
Le climat est la plupart du temps plus doux que ceux des montagnes Rocheuses ou des hautes plaines plus à l' est , même s' il varie fortement lui aussi .
Selon le National Oceanic and Atmospheric Administration , le soleil est présent 69 % du temps en journée .
Les hivers de Denver peuvent connaître des périodes douces ou froides .
Bien que les montagnes à l' ouest soient le théâtre de chutes de neige importantes suite au phénomène d' onde orographique , la zone de Denver est privée d' une grande partie de cette neige suite à l' assèchement des flux d' air après la traversée de la cordillère du Front Range .
Les premiers orages surviennent également au printemps lorsque des masses d' air chaud et humide atteignent Denver en provenance du golfe du Mexique .
La cité de Denver et son comté sont officiellement découpés en 79 quartiers .
Les quartiers en dehors du centre-ville sont apparus surtout après la Seconde Guerre mondiale et sont constitués de maisons construites avec des matériaux différents dans un style architectural plus moderne .
Denver possède de nombreux gratte-ciel dans son quartier des affaires .
Les efforts pour développer la verdure dans Denver datent de 1867 , pour embellir la ville .
Le plus grand se nomme City Park et s' étend sur 1.3 km² .
Denver possède également 29 centres récréatifs largement fréquentés par la population locale .
Ces derniers étaient alimentés par la rivière South Platte .
Denver a ainsi acheté et entretenu environ 56 km² de parcs de montagnes , dont le parc Red Rocks , qui abrite l' amphithéâtre de Red Rocks , .
Denver possède également la colline où est localisé le complexe hôtelier de sports d' hiver du Winter Park Resort dans le comté de Grand à environ 100 km à l' ouest de Denver .
Le bureau du recensement des États-Unis estimait , en 2006 , que la population de la ville et du comté était composée de 566974 habitants , la classant ainsi 26 e ville la plus peuplée des États-Unis .
La ville la plus proche dépassant Denver en termes de population est El Paso au Texas qui se situe à plus de 900 km .
Selon le recensement des États-Unis de 2000 , on comptait 239235 foyers à Denver pour une densité de population de 1428 hab./km² .
L' âge médian était de 33 ans ( 35,3 ans pour l' ensemble des États-Unis ) et pour 100 femmes , on comptait 102,1 hommes .
Le revenu médian pour un foyer monoparental dans la ville était de 39500 $ alors que le revenu médian pour un foyer était de 48195 $ ( contre 42100 $ pour l' ensemble des États-Unis ) .
Denver est une ville- comté dirigée par un maire , élu lors d' un vote apolotique , par 13 conseillers de la ville élus pour 4 ans et par un auditeur , sorte de trésorier ou de secrétaire communal qui vérifie toutes les dépenses et peut en refuser si des raisons financières le justifient .
Les conseillers de la cité de Denver sont élus dans 11 districts ainsi que deux autres membres .
Ils peuvent également lancer une enquête en cas de suspicion de délits de la part d' officiels du département de Denver .
Lors d' élections fédérales , les citoyens de Denver ont tendance à voter en faveur du candidat démocrate .
Actuellement le maire , un démocrate , se nomme John Hickenlooper .
Benjamin F. Stapleton fut maire de la ville de 1923 à 1931 et de 1935 à 1947 .
Il décida la construction de l' aéroport municipal de Denver qui débuta en 1929 .
Cet aéroport fut ensuite renommé en son hommage aéroport international de Stapelton .
Aujourd'hui , il a disparu mais a été remplacé par l' aéroport international de Denver .
Après son départ du poste , on découvrit que celui-ci était lié au Ku Klux Klan et profitait de son influence aux élections .
Entre le 20 juin 1997 et le 22 juin 1997 , la ville a accueilli une conférence du G7 .
Récemment , Denver a renforcé la protection des sans-abri notamment sous la législature de John Hickenlooper .
En 2005 , Denver devint la première grande cité à autoriser la consommation privée de cannabis ( plafonnée à environ 25 grammes ) à condition d' êtres âgé de plus de 21 ans .
Un tel vote au niveau de l' ensemble du Colorado avait échoué en novembre 2006 .
John Hickenlooper est membre de la coalition des maires contre les armes illégales .
Cet organisme compte également des maires importants comme ceux de New York ( Michael Bloomberg ) , Boston ( Thomas Menino ) ou de Seattle ( Greg Nickels ) .
L' économie de Denver dépend partiellement de sa position géographique qui en fait un carrefour de nombreuses voies de communication .
Étant la ville la plus importante dans un rayon de près de 950 km , elle est devenue naturellement le centre logistique , financier et commercial de la région des montagnes Rocheuses .
Ces activités s' étendent jusqu' à la ville proche de Boulder qui constitue un technopole spécialisé dans le domaine énergétique .
Dans une région possédant la quasi-totalité des réserves américaines d' uranium , la moitié du charbon , ainsi que d' importantes réserves de gaz naturel et de pétrole , Denver est aujourd'hui une des capitales américaines de l' énergie .
La zone de Denver accueille de nombreuses institutions gouvernementales .
La position de la cité à proximité des montagnes Rocheuses favorise entre autres l' activité économique dans les domaines de l' exploitation minière et de l' énergie .
Cela s' est par exemple traduit dans la fiction par la série télévisée Dynastie .
Plus de 15000 travailleurs de l' industrie du pétrole perdirent leur emploi , dont le maire John Hickenlooper , qui était géologue à l' époque .
Alors que le dôme du capitole du Colorado est recouvert d' une fine couche d' or , la cité accueille également les bâtiments de l' United States Mint qui abrite environ 33 % des réserves en or du pays .
Mais d' autres grandes sociétés possèdent également leur siège dans des villes faisant partie de la zone métropolitaine de Denver .
Dans les années 1880 , Horace Tabor construisit le premier opéra de la cité .
Denver accueille de nombreux musées reconnus nationalement , comme le récent musée des Arts de Denver réalisé par l' architecte Daniel Libeskind et qui abrite plus de 30000 objets d' arts .
Le quartier très animé de LoDo accueille de nombreuses galeries d' art , des restaurants , des bars , des cinémas et des discothèques .
Grâce à toutes ces animations , Denver a été classée trois années consécutives comme la meilleure ville pour les célibataires .
Le zoo de Denver accueille plus de 4000 animaux et fait partie des dix meilleurs zoos du pays .
Bien que Denver ne soit pas reconnue comme le berceau d' un type de musique contrairement à d' autres grandes villes américaines , la cité dispose d' une scène musicale active dans les musiques pop , jazz , jam , folk et musique classique .
La ville fut dans les années 1960 et 1970 reconnue dans le domaine de la musique folk en accueillant des chanteurs de renom comme Bob Dylan , Judy Collins et John Denver qui y vivaient tout en jouant dans les clubs locaux .
Au mois de mars a lieu la seconde plus grande parade de la saint-Patrick aux États-Unis .
Côté gastronomie , la cité peut offrir des spécialités mexicaines épicées , du bœuf ou du bison des plaines ou des truites en provenance des proches montagnes Rocheuses .
Denver est également desservie par plus de 40 stations de radio diffusant en FM et AM couvrant une large variété de styles musicaux .
Denver est classé 11 e marché pour la radio aux États-Unis .
Le Denver Public Schools est le système scolaire public de Denver .
Pour l' enseignement supérieur , Denver accueille de nombreuses universités organisées par âges et programmes d' étude .
Au total , la zone métropolitaine de Denver accueille 15 universités et collèges , ce qui représente au total environ 125000 étudiants .
L' université privée de Denver possède une école de commerce bien classée également .
Le département de police de Denver et plus généralement les forces de police à Denver emploient 1405 personnes .
Denver possède de nombreuses équipes sportives dont certaines jouent un rôle de premier plan dans quatre sports majeurs .
L' équipe de football américain des Broncos de Denver appartient à la ligue nationale NFL et dispose de nombreux supporters qui se rendent dans l' important stade Invesco Field at Mile High .
Dans les années 1980 et 1990 , les autorités locales ont misé sur l' implantation d' une équipe de baseball notamment en favorisant la construction du stade de Coors Field .
L' équipe des Rockies du Colorado fut lancée en 1993 .
La cité accueille également l' équipe de hockey sur glace , Colorado Avalanche .
Il fut alors forcé de revendre sa franchise à des investisseurs américains qui décidèrent d' implanter l' équipe à Denver .
L' équipe a remporté la coupe Stanley en 1996 et 2001 .
Ils évoluent dans l' Invesco Field en Major League Lacrosse .
Les rues de Denver sont organisées selon un plan quadrillé orienté vers les points cardinaux .
Colfax Avenue , la principale artère est-ouest à travers Denver , est constituée de 15 îlots ( 1500 ) au nord de la médiane .
La plupart des rues du centre-ville et dans le LoDo ont une direction nord-ouest / sud-est .
Il peut y avoir des confusions entre les deux plans d' organisation , compte tenu surtout des rues à sens unique dans le centre-ville de Denver .
Les montagnes à l' ouest de Denver permettent un bon point de repère pour se déplacer dans Denver .
De nombreuses rues dans Denver possèdent une piste cyclable et il existe également un itinéraire indépendant pour les vélos passant dans les parcs ou au bord des cours d' eau , comme le Cherry Creek et la South Platte .
Ceci permet à une part importante de la population de Denver de pouvoir utiliser ce mode de transport .
La ville de Denver a établi trois types de dénomination pour les routes .
La ville de Denver possède un taux de motorisation important , il est de 1.07 voiture par personne pour l' année la plus récente .
Denver est principalement desservie par les autoroutes interétatiques I-25 et I-70 .
Denver possède également une rocade quasi-complète appelée le 470 .
Le projet prévoit une voie longue de 31 km qui sera parallèle à l' I-225 .
Des liaisons journalières d' Amtrak relient également Denver à la ville d' Albuquerque au Nouveau-Mexique .
Dans les premières années après la fondation de Denver , la ville est devenue un point clef dans la desserte ferroviaire de l' ouest , ce qui est encore visible de nos jours .
Il est le dixième aéroport le plus fréquenté au monde et figure au quatrième rang aux États-Unis , avec 47324844 passagers en 2006 .
Les emprises aéroportuaires couvrent plus de 137 km 2 , ce qui en fait le plus grand aéroport en superficie des États-Unis , plus étendu que l' île de Manhattan .
L' aéroport sert de plaque tournante pour United Airlines et est également le siège social de la compagnie Frontier Airlines .
Son concept architectural vise à rappeler les sommets enneigés des proches montagnes Rocheuses .
Trois autres aéroports déservent la région de Denver .
Dans le passé , Denver a été le siège d' installations aéroportuaires qui ne sont plus opérationnelles .
De nombreuses célébrités sont nées dans la ville de Denver :
La ville de Denver est jumelée avec plusieurs villes étrangères :
