Elle est hébergée par une fondation américaine , la Wikimedia Foundation .
Wikipédia est issue de Nupédia , un projet d' encyclopédie libre en ligne fondé en mars 2000 par Jimmy Wales et soutenue par la société Bomis fondée en 1996 et dont il est l' actionnaire majoritaire .
Larry Sanger fut engagé au titre de rédacteur en chef .
Nupédia fonctionnant avec un comité scientifique , sa progression se révéla très lente .
À cause de la frustration occasionnée par la lenteur de progression de Nupédia , Larry Sanger proposa à Jimmy Wales la création d' un wiki afin d' accroître la vitesse de développement des articles , ce qui donna lieu à la création de Wikipédia en janvier 2001 .
Ce nouveau projet devait servir à fournir du contenu textuel selon une méthode plus souple , permettant ensuite éventuellement d' alimenter Nupédia , après un passage par le filtre d' un comité d' experts .
Jimmy Wales intervint fin 2005 sur l' article de Wikipédia en anglais consacré à Wikipédia , pour retirer l' information selon laquelle Larry Sanger en était cofondateur , puisque Sanger a toujours été un salarié .
La Wikipédia en français fut officiellement fondée le 23 mars 2001 .
Elle fut la première version de Wikipédia dans une langue autre que l' anglais , suivie par les versions en allemand et en catalan , puis par d' autres en environ 250 langues dont environ 200 sont actives en 2009 .
À partir de ce moment , Larry Sanger travailla parallèlement à Nupedia et à Wikipédia pour laquelle il participa à l' élaboration de la plupart des règles de fonctionnement .
En février 2002 , la rétribution de son travail pour Nupedia et Wikipédia fut supprimé du budget alloué par Bomis ; en conséquence , il démissionna officiellement le 1 er mars 2002 de ses fonctions sur les deux projets .
En 2003 , la progression de Nupedia stagna , alors que Wikipédia se développait très rapidement .
Selon Larry Sanger , Nupedia échoua à cause d' une chaîne éditoriale trop lourde et de la difficulté à trouver des rédacteurs bénévoles .
Dès lors , le projet Wikipédia opéra par consensus , utilisant les règles et recommandations adaptées au cours du temps par les contributeurs .
Le 20 juin 2003 , la Wikimedia Foundation fut créée pour financer le soutien technique aux différents projets de Wikipédia .
La recherche de moyens techniques et économiques permettant de rendre accessible les informations de Wikipédia par d' autres voies que le Web , est liée au projet d' une diffusion la plus large possible des connaissances .
Le projet de distribution sur papier est destiné en particulier aux personnes n' ayant pas les moyens de se raccorder à Internet .
Des dossiers sur support papier appelés les " wikireaders " , rassemblent des articles de la Wikipédia en allemand relatifs à un thème donné .
L' appareil électronique WikiReader permet également la consultation hors-ligne de Wikipédia .
Wikipédia a pour slogan : " Le projet d' encyclopédie librement distribuable que chacun peut améliorer " .
Ce projet est décrit par son cofondateur Jimmy Wales comme " un effort pour créer et distribuer une encyclopédie libre de la meilleure qualité possible à chaque personne sur la terre dans sa langue maternelle " .
Ainsi , Jimmy Wales proposa comme objectif que Wikipédia puisse atteindre un niveau de qualité au moins équivalent à celui de l' Encyclopædia Britannica .
En revanche , Wikipédia n' a pas pour but de présenter des informations inédites , elle ne vise donc qu' à exposer des connaissances déjà établies et reconnues .
Les francophones utilisent un " é " aussi bien dans le nom dactylographié que dans le logo , la plupart des autres communautés s' en tenant à l' écriture " Wikipedia " .
On trouve rarement l' écriture " WikipédiA " , correspondant plus au logo .
La version en ligne est réalisée collaborativement sur Internet , grâce au système des wikis .
Wikipédia fut la première encyclopédie généraliste à ouvrir , grâce à ce système , l' édition de ses articles à tous les internautes .
Toute personne modifiant le contenu de Wikipédia est censée présenter avec impartialité les principales opinions sur le sujet d' un article , en assurant la vérifiabilité des informations par l' ajout d' une référence permettant d' identifier l' auteur initial du point de vue présenté .
Le contenu étant modifiable , aucun article n' est considéré comme achevé , et Wikipédia se présente comme un projet en constant développement .
Wikipédia se construit sur la base de cinq principes fondateurs , dont les trois premiers sont : Wikipédia est une encyclopédie , proposant un contenu librement réutilisable publié sous une licence libre , avec une présentation " neutre " des faits et des diverses opinions .
Wikipédia étant un projet collaboratif , le quatrième principe fondateur régit les relations entre les contributeurs , exigeant un dialogue respectueux des règles de savoir vivre .
Le dernier des principes fondateurs est qu' en dehors de ceux-ci , il n' y a aucune règle définitive : le fonctionnement de Wikipédia est adaptable , modifiable par ses utilisateurs .
Le projet Wikipédia vise à être encyclopédique , à refléter de manière aussi exhaustive que possible l' ensemble du savoir humain .
Ainsi , toutes langues confondues , des centaines de sites web reprennent l' ensemble ou une partie du contenu de Wikipédia .
Cette notion de contenu libre découle de celle de logiciel libre , formulée avant Wikipédia par la Free Software Foundation .
En revanche , chaque site qui héberge une copie de Wikipédia a sa propre politique éditoriale ; dans wikipedia.org en particulier , l' édition est soumise à de nombreuses règles .
À partir de 2009 , il est principalement publié sous licence Creative Commons paternité-partage des conditions initiales à l' identique 3.0 , la GFDL devenant une licence secondaire disponible sous certaines conditions .
Les modifications apportés par les utilisateurs sont publiées sous les deux licences , et l' import de contenu uniquement sous licence Creative Commons by-sa 3.0 est autorisé , mais il entraine l' impossibilité de réutiliser globalement les pages concernées sous licence GFDL .
Au niveau du contenu , l' encyclopédie se veut respectueuse de la " neutralité de point de vue " , définie par Jimmy Wales comme le fait de " décrire le débat plutôt que d' y participer " .
Dans la mesure du possible , toute contribution à Wikipédia doit se garder de prendre parti dans une discussion argumentée .
Sur Wikipédia , les règles d' écriture visent à convenir aux personnes rationnelles , même si celles-ci ne sont pas toujours du même avis .
La politique de neutralité de Wikipédia stipule que les articles doivent évoquer toutes les facettes d' une question controversée , et ne pas déclarer ni insinuer que l' un ou l' autre des points de vue est a priori le bon .
Wikipédia accorde plus de place aux opinions les plus répandues , notamment chez les spécialistes , et les mieux étayées , qu' à celles de groupes minoritaires .
Les différentes communautés linguistiques et la Wikimedia Foundation s' accordent pour publier Wikipédia et ses projets frères sans recourir à un financement publicitaire .
Par ses objectifs et son fonctionnement , le projet Wikipédia s' inscrit dans une série de filiations culturelles :
En revanche , Wikipédia est assez éloignée de l ' Encyclopédie ou Dictionnaire raisonné des sciences , des arts et des métiers par sa volonté de présenter des informations neutres , alors que l' ouvrage conçu par Denis Diderot et Jean le Rond D' Alembert se caractérisait au contraire par son fort engagement contre l' obscurantisme .
L' historien du livre Roger Chartier souligne cependant que Wikipédia " repose sur les contributions multiples d' une sorte de société de gens de lettres invisibles " même si " Diderot n' aurait sûrement pas accepté la simple juxtaposition des articles , sans arbre des connaissances ni ordre raisonné , qui [ la ] caractérise " .
Le succès de Wikipédia a poussé la Wikimedia Foundation à développer d' autres sites en reprenant ses mécanismes de fonctionnement : le Wiktionary , un dictionnaire et thésaurus créé le 12 décembre 2002 ; Wikiquote , un recueil de citations ( 27 juin 2003 ) ; Wikibooks , un annuaire des livres électroniques destinés aux étudiants ; Wikisource , un recueil de textes dans le domaine public ; Wikinews , un site d' informations ( décembre 2004 ) ; Wikispecies , un répertoire du vivant ( 2004 ) ; et la Wikiversity , une communauté pédagogique créée en 2006 .
Cette banque de données regroupe les schémas , photos , vidéos et sons libres qui servent à illustrer les articles de Wikipédia dans ses différentes versions linguistiques .
Créée le 7 septembre 2004 , Wikimedia Commons dispose de plus de 7069689 fichiers libres à la date du 28 juillet 2010 .
Wikimedia Commons collabore aussi avec d' autres médiathèques afin de diffuser plus largement leurs fonds d' images libres , à travers Wikipédia notamment .
En décembre 2008 , les archives fédérales du Bundestag ont ainsi téléchargé 80000 images vers ce site , suivit en avril par la librairie du Länder de Saxe avec un don de 250000 images , et en novembre 2009 par le musée ethnographique d' Amsterdam Tropenmuseum qui téléchargea 35000 images concernant l' Indonésie .
Ces images , dont beaucoup ont une valeur historique , servent ensuite d' illustrations à des articles de Wikipédia , des sites web et des journaux en ligne .
Wikipédia est organisée afin de regrouper les articles rédigés dans la même langue , qui forment la version de Wikipédia dans cette langue .
Un clic de souris sur les illustrations de Wikipédia conduisent à une page de description du fichier multimédia indiquant notamment le nom de l' auteur et la licence sous laquelle il est publié .
Des hyperliens externes permettent aux lecteurs de consulter des sources d' information en lignes lorsqu' une référence soutenant une information dans un article est présente sur le Web .
Les différentes communautés linguistiques de rédacteurs de Wikipédia élaborent des règles , des conventions et des principes guidant la rédaction des articles qui leur sont propres .
Il existe 267 éditions de Wikipédia localisées par langue au 1 er janvier 2010 , et parmi elles une centaine sont actives .
Le nombre total d' articles de l' ensemble des éditions de Wikipédia a dépassé les 15 millions le 15 février 2010 .
Le projet Wikipédia ne se limite pas aux langues vivantes comptant un très grand nombre de locuteurs , officielles ou attachées à un pays .
Huit versions linguistiques de l' encyclopédie recourent à l' orthographe et à la typographie " Wikipédia " ( avec l' accent aigu ) pour désigner l' encyclopédie :
Tout lecteur de Wikipédia est un rédacteur potentiel .
Environ 130000 des lecteurs et contributeurs de Wikipédia y ont répondu , principalement en langue anglaise , allemande et espagnole .
Ces contributeurs passent en moyenne 4,3 heures par semaine sur Wikipédia , et leurs motivations principales sont de partager le savoir et de corriger les erreurs .
Ils se répartissent généralement par communauté linguistique concentrée sur la rédaction de la version de Wikipédia correspondante , mais interviennent aussi souvent ponctuellement sur les versions de Wikipédia en d' autres langues , ou les projets frères de la Wikimedia Foundation .
Depuis 2008 , les comptes enregistrés peuvent être unifiés : un seul compte sert ainsi à identifier l' utilisateur sur tous les projets de la Wikimedia Foundation .
Ces associations sont reconnues comme associations locales par la Wikimedia Foundation , mais n' y sont pas juridiquement ou financièrement liées .
Au sein de Wikipédia , les comptes utilisateurs disposent de différents statuts techniques gérés par le logiciel MediaWiki et contrôlant les actions qui leurs sont permises , .
Nombre de gens avec au moins 100 modifications en 1 mois pour l' ensemble des Wikipédia :
Nombre de gens avec au moins 5 modifications en 1 mois pour l' ensemble des Wikipédia :
Le nombre de pages vues par mois pour l' ensemble des Wikipédia :
Wikipédia permet à tout internaute de modifier les articles , mais ces modifications font l' objet de plusieurs niveaux de surveillance a posteriori , qui permettent de corriger les erreurs les plus évidentes .
Son cofondateur , Jimmy Wales , affirme ainsi qu ' " en général , la correction d' une erreur ou d' une information fallacieuse a lieu en quelques heures , voire en quelques minutes " .
Cet examen systématique facilité par les fonctionnalités du logiciel MediaWiki , permet souvent de rattraper quelques modifications qui avaient initialement échappé aux deux premiers niveaux de contrôle .
Le 6 décembre 2009 , la Wikipédia en français proposait ainsi 792 bons articles et 614 articles de qualité .
Le statut de Wikipédia en tant que source de référence est un sujet de controverses , en particulier à cause de son système de rédaction ouvert au public .
L' audience grandissante de Wikipédia a conduit un grand nombre de personnes à formuler des avis critiques sur la fiabilité des informations présentées dans cette encyclopédie .
Ces critiques étant récurrentes , une page spéciale de Wikipédia est consacrée aux réponses de participants à Wikipédia aux objections les plus fréquentes .
Les critiques de Wikipédia l' accusent d' incohérences , de partialité systémique et d' une forme d' anti-élitisme , et d' avoir une politique favorisant trop le consensus dans son processus éditorial .
La fiabilité et la précision de Wikipédia sont aussi des questions débattues .
Ainsi , en juin 2009 , le philosophe français Bernard Stiegler estime que Wikipédia , " passage obligé pour tout utilisateur d' Internet " , est un " exemple frappant d' économie de la contribution " et que l' encyclopédie " a conçu un système d' intelligence collective en réseau " .
Des études ont été menées sur la qualité du contenu proposée par Wikipédia , et des comparaisons effectuées avec d' autres encyclopédies .
Ces évaluations fournissent généralement des conclusions positives pour Wikipédia , mais ces résultats font aussi l' objet de critiques .
Wikipédia et ses projets parallèles sont des wikis libres .
Sur Wikipédia , par exemple , la syntaxe utilisée pour modifier une page est beaucoup plus simple que celle du HTML , et elle est censée permettre un apprentissage rapide .
A partir de l' été 2002 , tous les sites sont progressivement migrés vers MediaWiki .
Le succès croissant de Wikipédia nécessite l' emploi d' un grand nombre de serveurs informatique qui fonctionnent tous avec un système d' exploitation GNU/Linux .
Une description précise de l' architecture des serveurs est difficile , car elle change très fréquemment en raison des améliorations régulièrement apportées pour répondre au très fort trafic engendré par la consultation de Wikipédia par le public .
Cet aspect de Wikipedia est géré par le personnel technique de la Wikimedia Foundation .
D' après le site Alexa , Wikipédia fait partie en 2008 des dix sites les plus visités du World Wide Web .
La même année , le nombre d' articles a dépassé les 11 millions , dont plus de 2400000 dans Wikipédia en anglais , plus de 700000 dans Wikipédia en allemand et Wikipédia en français .
De nombreux projets d' encyclopédie existent ( ou ont déjà existé ) sur Internet .
La forte fréquentation de Wikipédia , combinée aux critiques sur son principe de fonctionnement , ont également poussé au développement de projets concurrents .
Citizendium est par exemple une encyclopédie en ligne en anglais dirigée par Larry Sanger et publiée sous licence libre .
Le 5 décembre 2009 , Citizendium propose 12790 articles , dont 121 ont été approuvés par son système de sélection .
À titre de comparaison et à la même date , Wikipédia en anglais propose 3116306 articles , dont 2710 sont présentés comme articles de qualité ( featured articles ) .
Conservapedia est une encyclopédie collaborative en ligne en anglais , conservatrice et créationniste , construite en réaction à la neutralité de point de vue de Wikipédia en anglais , jugée trop " gauchiste " et " liberal " ( au sens américain du terme ) .
En Chine , le moteur de recherche Baidu a ouvert l' encyclopédie en ligne Baidu Baike le 20 avril 2006 .
Des systèmes de contrôle assurent sur ces deux sites que des informations jugées inappropriées par le gouvernement de la République populaire de Chine ne sont pas publiées .
À titre de comparaison , la Wikipédia en chinois contient 270000 articles en septembre 2009 , et le site a été fréquemment bloqué en chine populaire , notamment parce qu' il présentait des articles sur des sujets sensibles comme les manifestations de la place Tian'anmen , le Falun Gong , ou le dalaï-lama .
Wikipédia a remporté deux prix importants en mai 2004 .
Le 26 janvier 2007 , Wikipédia a aussi été nommée quatrième meilleure marque par les lecteurs de brandchannel.com , recevant 15 % des voix en réponse à la question " Quelle marque a le plus d' impact sur nos vies en 2006 ?
En plus de la très forte croissance du nombre de ses articles , Wikipédia a acquis un statut de site de référence depuis sa création en 2001 .
Sur les dix premiers , Wikipédia est le seul site à but non lucratif .
Le contenu de Wikipédia est utilisé sur des sites webs , dans des devoirs scolaires , dans des études universitaires , des livres , des conférences et des affaires judiciaires , .
De nombreux sites internet , comme les blogs , les sites officiels ou journalistiques , proposent souvent des liens complémentaires vers des articles de Wikipédia pour approfondir un sujet .
Le contenu figurant sur Wikipédia a également été cité comme une source de référence dans certaines rapports de l' Intelligence Community .
Wikipédia est aussi utilisée comme source pour des articles de presse , provoquant des polémiques lorsqu' une information erronée et non supportée par une note de référence indiquant sa source , est repris sans vérification par les journalistes , .
Plusieurs journalistes ont été licenciés pour plagiat de Wikipédia , , .
Wikipédia est devenu un sujet d' actualité , de débat , et de satire dans de nombreux pays .
Certaines sources médiatiques font sa satire en insistant sur le manque de fiabilité de Wikipédia , comme par exemple le journal satirique The Onion .
Des émissions radiophoniques ou télévisées , comme le The Colbert Report par exemple , ont plusieurs fois incités les téléspectateurs à modifier les pages de Wikipédia , parfois pour y inclure des informations volontairement erronées ou fantaisistes .
Il a affirmé que l' absence de cette liberté forçait Wikipédia , " le septième site le plus consulté " , à interdire toutes les images de bâtiments modernes et d' art moderne italien , et a déclaré que c' était très handicapant pour les recettes touristiques .
Un article d' octobre 2007 de l' agence Reuters indiquait qu' avoir un article sur Wikipédia commençait à prouver la notoriété d' une personne .
