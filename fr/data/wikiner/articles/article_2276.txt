La vie énigmatique de Pythagore permet difficilement d' éclaircir l' histoire de ce réformateur religieux , mathématicien , philosophe et thaumaturge .
Le néopythagorisme est néanmoins empreint d' une mystique des nombres , déjà présente dans la pensée de Pythagore .
Il conserve encore aujourd'hui un grand prestige à tel point que Hegel disait qu' il était " le premier maître universel " .
D' après un écho marquant d' Héraclide du Pont , Pythagore serait le premier penseur grec à s' être qualifié lui-même de " philosophe " .
Cicéron évoque l' anecdote célèbre sur la création du mot φιλόσοφος : " amoureux de la sagesse " , par Pythagore :
