Par exemple , Franquin a travaillé l' amélioration de la transcription sonore par des fantaisies typographiques expressives .
Carali et Édika ont mis en place des onomatopées imaginaires , plus éloignées du bruit à reproduire mais chargées d' un comique en accord avec la bande ( " balouza , kwika , woga chtonga , azlok " : poing dans la figure ) .
Charlie Schlingo a parfois utilisé des onomatopées reprenant littéralement l' action : le bruit que fait l' action de couper du jambon est tout simplement " coupdujambon " .
Le dessinateur américain Don Martin s' était également spécialisé dans les onomatopées incongrues , qui constituaient parfois les seuls " mots " de ses histoires .
Alors que les théories de l' onomatopée affirmant à la suite de Leibniz que les onomatopées sont à l' origine du langage ont été réfutées depuis par Max Müller , Otto Jespersen ou Chomsky , l' onomatopée , comme son étymologie l' indique , reste un moyen de formation de mots important dans les différentes langues : de nombreux mots des lexiques des différents idiomes sont des dérivés d' onomatopées .
Si le français utilise les onomatopées essentiellement comme phononymes , d' autres langues , comme le japonais , utilisent des images sonores comme phénonymes ( mots mimétiques représentant des phénomènes non-verbaux : ex .
On pense que l' influence sur André Franquin de ses lectures de jeunesse permet de faire le lien historique .
Hugo Pratt , l' auteur du célèbre Corto Maltese , a été influencé dans sa jeunesse par la lecture de bandes dessinées américaines .
