Selon le modèle alternatif avec antimatière soutenu notamment par Gabriel Chardin et Stephen Hawking , la quantité de matière présente dans l' univers est 15 fois plus abondante que dans le modèle conventionnel .
Les objets galactiques auront une fin : le Soleil , par exemple , s' éteindra dans 5 ( à 7 ) milliards d' années , lorsqu' il aura consumé tout son combustible .
