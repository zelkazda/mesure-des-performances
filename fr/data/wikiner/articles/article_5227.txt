Steve Ballmer , né le 24 mars 1956 à Détroit aux États-Unis , est un chef d' entreprise américain .
Il fait la connaissance de Bill Gates à l' Université Harvard et entre chez Microsoft en 1980 .
Steve Ballmer est né le 24 mars 1956 , d' un père suisse et d' une mère dont la famille juive est originaire de Pinsk .
Il grandit à Farmington Hills dans le Michigan et étudie les mathématiques et l' économie à l' Université Harvard , où il fait la connaissance de Bill Gates .
Il est employé par Procter & Gamble en tant qu' assistant du responsable produits ( " assistant product manager " ) pendant deux ans , avant d' entrer à la Stanford Graduate School of Business .
En 1980 , Gates le persuade d' abandonner ses études et de le rejoindre chez Microsoft , la société de logiciels de micro-informatique qu' il a fondé en 1975 avec Paul Allen .
En juillet 1998 , il devient président de la société et en janvier 2000 CEO , poste où il remplace le co-fondateur Bill Gates .
Depuis 1998 , il est notamment à l' origine du développement de la Xbox .
En 2001 , il a décrit Linux , système d' exploitation libre , comme un cancer qui contamine la propriété intellectuelle dès qu' il la touche .
Il avait auparavant , en 2000 , déclaré que Linux présentait les caractéristiques du communisme .
Steve Ballmer est réputé pour exprimer son enthousiasme sans retenue .
Plusieurs vidéos le montrant en train de bondir sur la scène d' une conférence au rythme d' une chanson de Gloria Estefan ou d' haranguer les développeurs informatiques sont devenues virales et ont été parodiées par les internautes .
