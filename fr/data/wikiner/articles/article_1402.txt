Il est président des États unis mexicains du 1 er décembre 2000 au 1 er décembre 2006 .
Il rejoint en 1980 le Partido Acción Nacional .
En 1995 , il devient gouverneur de l' État de Guanajuato .
Il succède ainsi à Ernesto Zedillo et met fin à une longue période de 71 ans pendant laquelle le PRI occupa sans interruption la présidence .
Ainsi , des syndicalistes et des patrons de Pemex ont été incarcérés .
Ce puissant groupe pétrolier d' état était impliqué dans le financement occulte du PRI [ réf. nécessaire ] .
Le 26 novembre 2006 , le président mexicain s' effondre lors d' une cérémonie d' adieu célébrée dans son ranch de Guanajuato , à cinq jours de la fin de son mandat .
