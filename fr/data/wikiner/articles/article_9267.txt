De nos jours son nom est commémoré avec l' institut Razi près de Téhéran et son anniversaire est célébré tous les 27 août en Iran lors de la journée de la pharmacie .
Razi est né dans la ville de Ray ( en langage perse Razi signifie " de la ville de Ray " ) , une ville située au sud de Téhéran , Iran , dans le province du Khorassan et a effectué une grande partie de ses recherches dans celle-ci .
Avicenne a vécu aussi un moment dans cette cité .
Selon certains de ses biographes , Razi aurait souffert d' une maladie des yeux provoquée par les émanations résultant de ses expériences d' alchimie qui lui aurait fait abandonné ce domaine pour s' intéresser à la médecine mais Razi aurait dit , lui-même , que sa vue avait été affectée par les lectures prolongées .
Vers l' âge de trente ans , il débute donc une formation de médecin à Ray .
A la mort du souverain Al-Muktafi , en 907 , Razi retourne à Ray .
Devenu aveugle à la fin de sa vie , il décède à Ray le 27 octobre 925 ( ou 932 suivant les sources ) , en l' an 313 du calendrier musulman .
En tant que médecin chef de l' hôpital progressiste et humaniste , Razi introduisit des pratiques radicalement nouvelles dans le soin des patients et la formation des médecins .
Razi est reconnu pour ses talents d' observations alliés à une grande rigueur scientifique .
Ce faisant , il se fit un critique sévère mais admiratif de l' œuvre de Galien qu' il jugeait manquer d' observations empiriques .
Ibn al-Nadim identifie cinq domaines dans lesquels Razi s' est distingué :
Razi pratiquait de nombreuses spécialités médicales : chirurgie , gynécologie , obstétrique , ophtalmologie …
Razi a écrit 184 livres et articles dans plusieurs domaines scientifiques , dont 61 relevant de la médecine , tous en langue arabe .
Razi est l' auteur d' un des tout premiers traités de psychologie et de psychiatrie .
L' hôpital qu' il dirigea à Bagdad fut le premier à posséder un service pour les malades mentaux .
Razi s' intéressa aussi à la neurologie : il décrivit le rôle moteur et sensitif des nerfs en identifiant 7 des nerfs craniens et 31 des nerfs spinaux par un nombre référant à leur position anatomique depuis le nerf optique jusqu' au nerf hypoglosse .
Razi est également le premier dans le monde méditerranéen à différencier clairement la petite vérole de la varicelle .
Razi a découvert l' asthme allergique , et aurait été la première personne à avoir écrit un traité sur l' allergie et l' immunologie .
Razi aurait été le premier à comprendre que la fièvre était un mécanisme naturel de défense du corps humain .
Rhazes a contribué à la pratique précoce de la pharmacie grâce à des textes , mais aussi par d' autres manières .
En effet par son attachement à l' empirique et sa méfiance vis-à-vis du théorique , Razi ne cherchent pas à organiser les maladies en grandes familles de symptômes .
Au niveau professionnel , Razi a introduit beaucoup d' idées médicales et psychologiques utiles et progressives .
Néanmoins , pour être plus efficaces dans leurs soins , Razi a exhorté les praticiens à garder des connaissances à jour en étudiant continuellement des livres médicaux et à faire connaître toute nouvelle information .
Dans son ouvrage de critique à propos de Galien , Razi propose quatre raisons permettant d' expliquer pourquoi les grands hommes peuvent commettre des erreurs par :
