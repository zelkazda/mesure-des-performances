Cet article présente une liste des communes du canton du Tessin .
Le canton comprend deux communanza , des territoires qui appartiennent à deux communes : la C' za Capriasca/Valcolla ( sous la souveraineté commune de Capriasca et Valcolla ) et la C' za Medeglia/Cadenazzo ( Cadenazzo et Medeglia ) .
Le canton s' étend également sur une partie des lacs de Lugano et Majeur , sans que ces deux zones ne fassent partie d' aucune commune ; elles sont comprises dans la liste à fins de comparaison .
