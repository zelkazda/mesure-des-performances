C' est une province autonome du Danemark .
En 1984 ce pays a signé un traité modificatif avec la Communauté européenne pour préciser la situation du Groenland .
Il remplace la communauté économique européenne par l' Union européenne .
Mais le Danemark bénéficie de certaines exemptions .
Suite à la loi sur l' autonomie du Groenland votée par le parlement danois le 19 mai 2009 , le Groenland a accédé le 21 juin 2009 à une autonomie renforcée .
Le Danemark lui cède 32 domaines de compétences , dont ceux de la police et de la justice .
La monnaie , la défense et la politique étrangère relative à ces aspects restent sous le contrôle du Danemark .
Cet acte fait suite à un référendum consultatif qui a eu lieu au Groenland le 25 novembre 2008 .
La capitale du Groenland est Nuuk ( ou Godthåb en danois ) .
Lors de l' arrivée des Vikings qui y subsistèrent pendant plus de quatre siècles , il était en revanche très probablement inhabité .
En revanche , de nombreux liens se créèrent avec les États-Unis et le Canada .
Après la guerre , le Danemark reprit le contrôle du Groenland , mais dut transformer le statut de l' île en 1953 : de colonie , il passa à celui de comté d' outre-mer , avant d' acquérir l' autonomie interne en 1979 .
Enfin , en 1985 les habitants décidèrent de quitter la CEE à laquelle le Danemark avait adhéré en 1973 .
En tant que territoire autonome , le Groenland est membre du Conseil nordique .
Cependant le Danemark le représente auprès du Conseil arctique .
Il s' agissait de ne pas être soumis à certaines contraintes de la CEE , en particulier pour protéger son industrie de pêche .
Ce souhait a donné lieu à une demande du Danemark présentée à la communauté Européenne .
Le Groenland a été retiré des accords sur le charbon et l' acier , ainsi que des accords sur l' énergie atomique .
Il a été placé sur la liste des territoires d' outre-mer associés à la communauté Européenne ( devenue Union Européenne ) .
Le 25 novembre 2008 a été organisé un référendum consultatif portant sur l' autonomie de l' île , où les habitants ont très majoritairement voté en faveur d' un plan d' autonomie vis-à-vis du Danemark .
Il est entré en vigueur le 21 juin 2009 , jour de la fête nationale du Groenland .
Cet accord ne limite pas les pouvoirs constitutionnels du Danemark .
Il est réaffirmé que les affaires internationales , la défense et la politique de sécurité sont affaires du Royaume de Danemark .
En hiver , cette bande côtière est cernée par la banquise à l' exception du sud-ouest de l' île ( environ jusqu' à la capitale Nuuk ) .
Le point culminant est le mont Gunnbjørn , haut de 3 733 m .
Le plus connu est le mont Forel ( 3 360 m ) .
Il porte le nom du professeur suisse François-Alphonse Forel qui , en 1912 , organisa une souscription pour financer une expédition suisse au Groenland .
On signalera qu' un autre mont proche porte le nom de Paul-Émile Victor , explorateur et ethnologue français .
C' est le cas à Ilulissat où les plus gros icebergs de l' hémisphère Nord sont produits .
En 1912 , c' est l' un d' eux que le Titanic heurta .
La toundra , une végétation basse et pauvre composée de mousses et d' herbes poussant dans les zones polaires occupe une grande partie du Groenland .
Au Groenland , la grande végétation ne peut pas pousser car le sol est trop gelé en profondeur .
L' école publique du Groenland est , comme au Danemark , sous la juridiction des municipales : ce sont donc des écoles municipales .
Au Groenland , l' enseignement secondaire correspond généralement à une formation professionnelle et un enseignement technique .
Un enseignement supérieur est offert au Groenland dans les domaines suivants : la formation d' aides-soignants et d' infirmiers ( règlement no 9 du 13 mai 1990 ) , la formation des journalistes , la formation des enseignants de l' école primaire et secondaire du premier cycle ( règlement no 1 du 16 mai 1989 ) , la formation des travailleurs sociaux ( règlement no 1 du 16 mai 1989 ) , la formation des éducateurs sociaux ( règlement no 1 du 16 mai 1989 ) et la " formation universitaire " ( règlement no 3 du 9 mai 1989 ) .
Les élèves groenlandais peuvent poursuivre leur scolarité au Danemark , s' ils le désirent et en ont les moyens financiers .
Des bourses d' études sont accordées aux élèves groenlandais admis dans les établissements d' enseignement du Danemark .
La durée totale des séjours effectués hors du Groenland ne peut pas être supérieure à trois années .
Il existe un accord de partenariat en matière de pêche entre la Communauté européenne , d' une part , et le gouvernement du Danemark et le gouvernement local du Groenland .
Le Groenland présente un fort potentiel minier et pétrolier .
Ses eaux côtières recèleraient des réserves de pétrole équivalentes à la moitié de celles de la mer du Nord .
Ce projet suscite d' ors et déjà un conflit avec le Danemark .
Une importante base militaire américaine intégrée à l' OTAN se situe à Thulé ( aujourd'hui Qaanaaq ) .
C' est à cette époque qu' est construit un radar du Ballistic Missile Early Warning System , c' est-à-dire un élément stratégique de la défense antimissiles des États-Unis .
Un vol de bombardier armé d' armes nucléaires , non déclaré au Danemark , s' est écrasé au Groenland en 1967 ( Accident de Thulé ) .
En 2004 le gouvernement danois a signé un accord avec les États-Unis autorisant le renforcement de la base pour la modernisation du système antimissiles .
Il existe aux États-Unis une conscience aiguë de l' importance du Groenland .
Miller a déclaré " C' est une honte qu' un pays aussi insignifiant que le Danemark puisse tenir une telle place à propos d' un aspect aussi essentiel pour la sécurité des États-Unis "
À la suite de dérogations au Traité de Nice , le Danemark ne fait pas partie de la Politique européenne de sécurité et de défense .
Le Groenland n' est pas non plus partie à cette politique .
Le nouveau secrétaire général de cette organisation est Anders Fogh Rasmussen ancien premier ministre du Danemark de 2001 à 2009 .
La capitale , Nuuk , rassemble un cinquième de la population autochtone .
Le Groenland a pour codes :
