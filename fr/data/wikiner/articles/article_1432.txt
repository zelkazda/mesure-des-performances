Au lendemain de la Seconde Guerre mondiale les États-Unis connurent une période de croissance économique rapide , ainsi qu' un développement du commerce international .
Son nombre d' usagers était passé de 113 millions en 1927 à 26 millions en 1958 , suite à l' ouverture de nouveaux tunnels et ponts au trafic automobile sur l' Hudson .
Le projet du WTC impliquait la destruction de ces bâtiments et la relocalisation de leurs occupants , ce qui engendra des protestations .
Les tours jumelles furent les seconds gratte-ciel à utiliser ce système , après le John Hancock Center de Chicago .
Le système fut inspiré par le réseau du métro de New York , dont les lignes incluent des stations locales où s' arrêtent les rames locales , et des stations express où tous les trains s' arrêtent .
Le concept reçut plusieurs critiques au niveau de l' esthétique de la part de l' American Institute of Architects et d' autres groupes , .
Les radiodiffuseurs et les chaines de télévision s' inquiétèrent d' une éventuelle interférence dans la réception de la télévision à New York qui serait causée par le nouveau complexe .
Otis Elevator Company fut la société qui installa les ascenseurs .
La construction fut achevée en 1970 pour le One World Trade Center qui accueillit ses premiers occupants en décembre 1970 .
Cette situation était due à la crise financière qui secoua New York dans les années 1970 .
Ce ne fut que dans les années 1980 que des sociétés privées s' installèrent dans le complexe , dont de nombreuses firmes financières liées à Wall Street .
En parallèle , les tours accédèrent rapidement au statut de symboles de New York .
Ces évènements , qui reçurent une importante couverture médiatique , contribuèrent à la renommée internationale du World Trade Center .
Selon l' architecte du World Trade Center , la tour se serait effondrée si le camion avait été placé plus près des fondations .
Six extrémistes islamistes , dont Ramzi Yousef , furent condamnés à perpétuité pour leur rôle dans l' attentat .
Fin juillet 2001 , le bail du World Trade Center fut acheté pour 99 ans par Larry Silverstein , déjà propriétaire de l' immeuble de 47 étages situé juste au nord du complexe , nommé par extension WTC7 .
La 7 World Trade Center du complexe s' effondre plus tard le soir à 17 h 20 .
Les 1,6 million de tonnes de débris du World Trade Center fumèrent pendant 99 jours et plus de 8 mois furent nécessaires pour assurer le nettoyage du site , opération conduite par environ 40000 personnes et qui s' est terminée le 30 mai 2002 , à 10 h 29 précises .
Le jour même des attaques Giuliani déclare : " Nous reconstruirons : nous allons en sortir plus forts qu' auparavant , plus forts au niveau politique , plus forts au niveau économique .
Larry Silverstein le détenteur des droits à construire du site , répond immédiatement que " ce serait la tragédie des tragédies de ne pas reconstruire cette partie de New York .
Le projet fut confié pour son ensemble , ainsi que la tour la plus haute , la Freedom Tower , à l' américain Daniel Libeskind .
Autre point remarquable et hautement symbolique du projet : tous les ans , le 11 septembre , le site sera éclairé par le Soleil sans aucune ombre de 8 h 46 ( premier crash ) à 10 h 28 .
Le 7 World Trade Center fut le premier édifice à être reconstruit à proximité du site : les travaux débutèrent en 2002 et s' achevèrent en 2006 .
Des critiques ont été formulées concernant la durée des travaux , notamment dans les médias , par exemple par les journalistes Keith Olbermann et Rush Limbaugh .
Les deux tours emblématiques du World Trade Center avaient toutes deux 110 étages .
1 WTC devint en 1972 le plus haut gratte-ciel au monde , dépassant l' Empire State Building qui détenait ce titre depuis 40 ans .
Cependant les deux tours ne conservèrent ce statut qu' un court moment , puisque la Sears Tower de Chicago , achevée en 1974 , culminait à 442 mètres .
La construction du World Trade Center avait commencé courant 1966 pour s' achever en 1971 ( l' inauguration eut lieu en 1973 ) .
Lors de sa construction , les planchers du World Trade Center étaient construits selon une structure en treillis , c' est-à-dire des poutrelles en forme de grues pylône à l' horizontale dans lesquelles était coulé une dalle de compression en béton armé .
Les tours jumelles avaient aussi été réalisées avec des coefficients de sécurité tels qu' ils permettaient , selon les concepteurs , de résister à l' impact d' un Boeing 707 ou un Douglas DC-8 en pleine charge , lancé à 950 km/h : " … une telle collision résulterait seulement en dégâts locaux ne pouvant causer l' effondrement ou des dommages conséquents à l' immeuble et ne mettrait en danger ni la vie ni la sécurité des occupants hors de la proximité immédiate de l' impact " .
Trois immeubles ( 4 WTC , 5 WTC et 6 WTC ) s' élevaient également autour de la place .
5 WTC , au coin nord-est , la station de PATH et 4 WTC au coin sud-est complétaient le site , auquel il faut ajouter le gratte-ciel WTC7 .
L' immeuble WTC7 possédait plusieurs réservoirs de carburant :
Le principal occupant était l' entreprise Salomon Smith Barney , filiale de la Citigroup , avec 64 % de la superficie des bureaux .
D' autres sociétés comme American Express , Standard Chartered occupaient des locaux .
Il abritait aussi des services officiels avec un bureau de la CIA , du Department of Defense et l' Internal Revenue Service qui occupaient tout le 25 e étage , un bureau des USSS , la Securities and Exchange Commission ( SEC ) , où se trouvaient les archives d' affaires financières tels que les dossiers concernant les fraudes de Enron et de WorldCom , ainsi que le poste de commandement des situations d' urgence de la mairie de New York ( tout un étage renforcé contre les attaques terroristes ) .
Le World Trade Center apparaît dans de nombreux films , émissions de télévision , bandes dessinées et jeux vidéo .
Dans Les Trois Jours du condor , les locaux de la CIA sont basés dans le WTC .
Le complexe apparaît également dans Working Girl .
American Pie 2 fut la dernière grande production montrant les deux tours , bien qu' elles aient été ajoutées numériquement par synthèse d' image à une scène tournée à Los Angeles .
Le trailer de Spider-Man incluait une scène dans laquelle le super-héros dressait une grande toile entre les deux tours .
Le projet collectif 11 ' 09 " 01 -- September 11 présente des évènements liés aux attentats contre les tours .
