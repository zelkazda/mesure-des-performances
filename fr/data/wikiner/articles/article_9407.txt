Les modèles produits dans les années 1980 par Oric sont :
Le noyau du BASIC ( c' est-à-dire sans les extensions graphiques et sonores ) est similaire à celui du Commodore 64 .
Il présente dès 1986 une nouveauté révolutionnaire pour un langage BASIC sur micro-ordinateur familial : il est compilé ( voir : compilation ) et affiche des performances très supérieures au BASIC interprété des autres ordinateurs ( voir : interpréteur ) .
Le mode texte dispose de 3 variantes ( par le BASIC ) .
