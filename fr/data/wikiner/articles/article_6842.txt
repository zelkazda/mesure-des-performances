Saint-Trivier-sur-Moignans -- autrefois " Saint-Trivier-en-Dombes " , son ancien nom -- est une commune française , chef-lieu de canton de l' Ain .
Le canton , qui fait partie de l' arrondissement de Bourg-en-Bresse , comportait , en 2006 , 13 communes et 13477 habitants .
