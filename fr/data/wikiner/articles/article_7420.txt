Cette commune est située sur la rive droite de la Seine , limitrophe du Havre , dans le canton de Gonfreville-l'Orcher .
L' estuaire de la Seine était alors encadré par les ports d' Harfleur sur la rive droite et de Honfleur sur la rive gauche .
L' envasement progressif de la Seine et la fondation du Havre en 1517 condamnèrent définitivement l' activité portuaire .
