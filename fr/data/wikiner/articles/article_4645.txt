Étouvy est une commune du Bocage virois , sur la rive gauche de la Vire .
Elle est située à 7 km au nord de Vire , sur la route départementale 674 de Vire à Saint-Lô .
Le bourg d' Étouvy et celui de La Graverie sont directement reliés par un pont sur la Vire .
Étouvy se situe sur l' ancienne voie romaine menant de Vieux à Avranches où cet axe franchissait la Vire .
Étouvy avait compté jusqu' à 207 habitants en 1851 puis la population était redescendu à 100 ( 1954 ) .
Une minoterie est présente sur les bords de la Vire .
