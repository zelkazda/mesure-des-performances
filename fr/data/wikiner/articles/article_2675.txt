Il épouse en secondes noces -- contre l' avis des autorités ecclésiastiques -- Adélaïde de Paris qui lui donne un fils , Charles , qui naît après sa mort .
Comme l' indique son surnom , Louis II bégaie , ce qui l' empêche de s' exprimer en public et nuit à son autorité .
Son accession au trône est contestée par plusieurs seigneurs et même par l' impératrice Richilde , seconde épouse de son père .
Le 8 décembre 877 , il est couronné et sacré par l' archevêque Hincmar de Reims dans la chapelle palatine de l' abbaye Saint-Corneille de Compiègne .
