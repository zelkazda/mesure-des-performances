Il est protégé depuis 1992 au Royaume-Uni , où il fait aussi l' objet d' un élevage conservatoire et de réintroduction .
En Belgique où le blaireau est protégé , des passages à blaireaux ( écoducs spécialisés , en réalité de simples tuyaux de béton , type canalisations d' égouts ) passent sous les routes pour aider les blaireaux à se déplacer sans se faire écraser ou blesser par les véhicules .
Lors du voyage , il seconde Renard , son ami de longue date .
