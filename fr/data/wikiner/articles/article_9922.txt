Keno Don Hugo Rosa ( né le 29 juin 1951 à Louisville , dans le Kentucky ) est un auteur de comics ( bandes dessinées américaines ) .
Sa première histoire Disney fut le Fils du soleil ( The son of the sun ) , mettant en scène Picsou , qu' il réalisa pour la maison d' édition Gladstone et qui fut publiée pour la première fois en juillet 1987 .
Devant le succès rencontré par cette histoire , Don Rosa continua d' inventer d' autres histoires de Donald Duck et Picsou pour Gladstone .
Mais en 1989 , suite à la décision de Disney de ne plus rendre les planches originales , Don Rosa quitte Gladstone et décide de ne plus travailler pour Disney .
En 1990 , Don Rosa se met à travailler pour la société Egmont qui publie les histoires Disney au Danemark , société avec laquelle il collabore toujours actuellement ; les histoires dessinées par Don Rosa sont également populaires dans les autres pays scandinaves .
Par exemple : des souris au comportement très humain ou le personnage de Mickey Mouse caché ou malmené .
Au grand dam de son éditeur américain , Don Rosa parvient grâce à son dessin à introduire l' hommage à Carl Barks décrit ci-dessous .
L' autre force de Don Rosa réside dans sa capacité à ancrer ses histoires dans un contexte historique réel et à pouvoir jouer sur les émotions du lecteur .
Ainsi croise-t-on dans ses histoires des personnages historiques comme Theodore Roosevelt , Geronimo , les frères Dalton et bien d' autres encore .
À travers ses histoires , Don Rosa fait passer des messages tels que le respect de la nature .
Du fait de son talent et de sa connaissance de l' univers de Donald Duck , Don Rosa apparaît comme le digne successeur de Carl Barks , le " père " de Balthazar Picsou .
Don Rosa crée ainsi des histoires qui font référence aux histoires de Barks .
Don Rosa peint ainsi un milliardaire avare en argent , mais pas en sentiments ; et un oncle en costume de marin soucieux de ses neveux , plus encore que dans les histoires de Barks .
Cependant , Don Rosa se distingue de Barks en cherchant à raconter les origines des personnages , de Donaldville et des lieux comme le plan du coffre-fort de Picsou , là où Barks préférait sous-entendre et suggérer .
Les personnages sont aussi plus complexes chez Don Rosa : Picsou doute de lui-même , s' emporte facilement , Donald est parfois courageux car il déborde d' un amour paternel pour ses neveux .
Don Rosa parvient même à raconter des histoires tristes dans des bandes dessinées destinées à la jeunesse : l' annonce de la mort de sa mère déclenche le final de " l' Empereur du Klondike " et Picsou est montré plusieurs fois en train de se recueillir sur la tombe de sa mère et de son père qui est dessiné mort à la fin du " Milliardaire des landes perdues " .
Cet index est initialement tiré de la base Inducks ( voir liens externes ) .
Pour les douze épisodes initiaux et les histoires évoquant le passé de Picsou , voir l' article La Jeunesse de Picsou .
