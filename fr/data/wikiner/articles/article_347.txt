Choisy-le-Roi est une ville de France , dans la banlieue sud de Paris .
À 12 km en amont de Paris , Choisy-le-Roi est avec la capitale la seule commune de la petite couronne à s' étendre de part et d' autre de la Seine .
Une piste cyclable longeant la Seine relie Choisy-le-Roi à Paris .
Communes limitrophes : Alfortville au nord-est , Créteil à l' est , Valenton et Villeneuve-Saint-Georges au sud-est , Orly au sud-ouest , Thiais à l' ouest , Vitry-sur-Seine au nord-ouest .
J.-C. , conduite par le commandant Labienus , aurait livré bataille sur le territoire actuel de la commune ( voir guerre des Gaules ) .
Choisy n' est connu que depuis 1176 , comme faisant partie de la seigneurie de Thiais , laquelle appartient à l' abbaye de Saint-Germain-des-Prés .
Sous Louis XI en 1482 , les seigneurs de Choisy , avaient droit de haute et basse justice .
Le bourg se situe alors uniquement en bord de Seine .
Le château appartient alors à la princesse douairière de Conti , fille légitimée de Louis XIV et de la duchesse de La Vallière .
En 1739 , à la mort de la princesse , Louis XV fait l' acquisition du château pour disposer d' une résidence à proximité de la forêt de Sénart où il va chasser et décide que le village sera nommé désormais Choisy-le-Roi .
Madame de Pompadour y est installée en 1746 : les fêtes s' y multiplient .
Une partie des menus de ces repas gastronomiques a été conservée par la Bibliothèque nationale de France , et étudiée par des historiens de l' alimentation .
De 1775 à 1780 , Marie-Antoinette y organise des amusements de toutes sortes .
Pour remplacer le vieux village partiellement englobé dans ces transformations , et pour faire de Choisy une véritable résidence royale , un nouveau village est projeté dès 1746 .
Les terrains , de grandeur raisonnable , et les moellons de meulière sont donnés aux habitants par Louis XV , en priorité aux habitants du vieux bourg ainsi qu' à ceux liés au domaine royal par leur fonction .
Son clocher est moins élevé que le comble , à cause de l' aversion que Louis XV avait pour le son des cloches .
De 1748 à 1757 est percée la route royale de Versailles qui permet en faisant des fouilles d' y trouver des tombeaux antiques..
En 1750 , la route de Choisy à Paris est pavée le pont sur la Seine est construit .
Le port joue un rôle de relais entre le sud du bassin parisien et Versailles .
La Révolution a la même intensité à Choisy qu' à Paris , le maire de la ville entretenant des liens étroits avec Robespierre .
Danton séjourne à Choisy tout comme Rouget de Lisle , l' auteur de La Marseillaise .
À partir de 1809 , le pont de Navier remplace enfin le bac .
Un service d' omnibus en 1829 et une ligne de tramways en 1892 relient Choisy à Paris .
La ville subit de gros dégâts lors des combats franco-prussiens dans une tentative de libération de Paris le 30 septembre 1870 .
En 1912 , Choisy-le-Roi revient sous les feux de l' actualité avec la fin tragique de Jules Bonnot .
De 1968 à 1973 , Choisy-le-Roi accueille la délégation vietnamienne pour les négociations de Paris , originellement prévues pour quatre mois , dans le bâtiment qui fut , à l' époque , le siège de l' école centrale du Parti communiste français .
