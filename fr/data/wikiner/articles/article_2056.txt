Il se trouve à l' est du Chili ( 5150 km de frontières ) , au sud de la Bolivie ( 832 km ) , du Paraguay ( 1880 km ) , et du Brésil ( 1224 km ) ainsi qu' à l' ouest de l' Uruguay ( 579 km ) .
L' Argentine possède 4989 km de côtes .
Le point culminant est le mont Aconcagua , 6960 m , qui se situe dans la Cordillère des Andes .
L' Argentine héberge sur son territoire la plupart des plus hauts sommets du continent .
Ce sont surtout des volcans , quoique l' Aconcagua et le Mercedario soient d' origine orogénique par plissement .
Les deux seuls manquant à la liste sont le Huascarán péruvien avec ses 6768 mètres , et le Yerupajá de 6617 mètres , lui aussi péruvien .
L' Argentine , comme le Chili voisin , possède dans la région occidentale andine ou préandine une grande quantités de volcans .
Rien que dans le département d' Antofagasta de la Sierra , petite partie de la province de Catamarca , on en compte près de 200 !
Le fleuve principal de l' Argentine est incontestablement le Paraná , qui associé à l' Uruguay constitue le grand estuaire du Río de la Plata .
Parmi les autres grands cours d' eau , citons le Paraguay , le Bermejo , le Río Negro , le Río Colorado , le Río Salado del Norte , le Río Santa Cruz , le Río Desaguadero , le Río Neuquén et le Río Limay .
L' Argentine possède , en plus , de nombreux lacs , dont beaucoup sont situés en Patagonie .
Les principaux sont le lac Argentino , le lac Nahuel Huapi , le lac Futalaufquen , le lac Buenos Aires , le lac Viedma , le lac San Martín , le lac Lácar , le lac Huechulafquen , la laguna del Diamante et le lac Menéndez .
Parmi les lacs et lagunes salées , citons la Mar Chiquita , lac salé tellement imposant qu' il a reçu le titre de " mer " , la laguna Urre Lauquén , la lagunilla Llancanelo , le lac Colhue Huapi et le lac Cardiel .
L' Argentine connait un boom des exportations .
Les chiffres suivants sont exprimés en millions de dollars US $ :
