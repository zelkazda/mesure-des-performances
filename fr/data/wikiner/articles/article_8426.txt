La mer de Barents est la mer de l' océan Arctique qui est située au nord de la Norvège et de la Russie occidentale .
Elle est constituée d' un plateau assez peu profond ( avec une profondeur de 230 m en moyenne ) , délimitée par la mer de Norvège à l' ouest , l' île norvégienne de Svalbard au nord-ouest , et les archipels François-Joseph et Nouvelle-Zemble au nord-est et est .
Au sud de la mer de Barents se trouvent les ports de Mourmansk ( Russie ) et de Vardø ( Norvège ) , qui restent libre de glace durant toute l' année grâce à la dérive nord atlantique relativement chaude , ce qui en a fait des emplacements stratégiques pour les marines nationales .
Le territoire de la Finlande s' étendait à la côte de la mer jusqu' à la Guerre d' Hiver ; le port de Petsamo était alors le seul port finlandais libre de glace pendant l' hiver .
Il y a trois types principaux de masses d' eau dans la mer de Barents : de l' eau chaude et saline de l' océan Atlantique de la dérive nord atlantique , de l' eau froid arctique ( temp .
Dans l' ouest de la mer ( près de l' Île aux Ours ) , ce front est déterminé par la topographie du fond et est par conséquent assez stable d' année en année , tandis qu' à l' est ( près de la Nouvelle-Zemble , il peut être assez diffus et change de position toutes les années .
La pêcherie de la mer de Barents , en particulier celle de morue et du cabillaud , est très importante pour la Norvège et la Russie .
Au cours de la Seconde Guerre mondiale , la mer de Barents a été le théâtre de nombreuses opérations navales .
Parmi les différents affrontements s' y étant déroulés , on trouve la bataille de la mer de Barents à la fin de l' année 1942 qui met aux prises la Royal Navy à la Kriegsmarine .
La contamination de la mer de Barents par des déchets nucléaires provenant des réacteurs nucléaires de la marine russe est un problème écologique préoccupant .
Le premier à entrer en production fut celui de Snøhvit en territoire norvégien .
