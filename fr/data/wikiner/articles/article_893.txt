Dans l' urgence , le nouveau parti de Bruno Mégret modifia l' intitulé de sa liste pour les élections européennes en l' appelant Mouvement national .
La création formelle du nouveau parti interviendra à partir du 2 octobre 1999 , à La Baule , lorsque le comité national vote le changement de nom du parti en Mouvement national républicain , à la suite de quoi les formalités légales sont remplies .
Après l' échec de Jean Marie Le Pen en 2007 , Mégret s' en prend vivement à Marine Le Pen .
Le MNR échoue à atteindre le seuil de 50 candidats faisant plus de 1 % des exprimés , tous les candidats MNR ont été éliminés .
Il devrait toutefois partir sous ses propres couleurs dans des villes comme Marseille , Roubaix , Le Havre , Romainville , Poissy , Asnières , Noisy-le-Grand ou Sartrouville .
Les élections sont finalement un désastre pour le MNR , qui perd un grand nombre de conseillers municipaux .
Il obtient toutefois un élu à Sartrouville .
Finalement dans cette dernière Nicolas Bay , unique élu du MNR dans une ville de plus de 3500 habitants , est exclu du parti fondé par Bruno Mégret .
Le 23 mai 2008 , Bruno Mégret annonce se mettre " en réserve de la politique " et partir " à l' étranger exercer des responsabilités au sein d' une grande entreprise française " .
