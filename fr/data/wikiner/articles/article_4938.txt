Toronto est la capitale de la province de l' Ontario au Canada .
Avec plus de 2,5 millions d' habitants , Toronto est la plus grande ville du Canada , devant Montréal et la cinquième plus grande en Amérique du Nord .
La ville actuelle est issue de la fusion des municipalités du Toronto Métropolitain , en 1998 , alors que l' ancienne ville comptait une population de 653 734 résidents , deux ans avant la fusion .
En tant que capitale économique du Canada , Toronto est une ville de classe internationale , ainsi qu' une des villes financières les plus importantes dans le monde .
La majorité des entreprises canadiennes ont leur siège social dans la ville , ainsi que la Bourse de Toronto , la septième plus grande au monde sur le plan de la capitalisation boursière , .
Toronto est une ville multiculturelle et moderne dont 49 % de la population est née à l' extérieur du Canada .
50 % des habitants de Toronto ont une langue maternelle autre que l' anglais , et 32 % parlent à la maison une langue autre que l' anglais .
La colonie du Haut-Canada fut ainsi établie sous le gouverneur John Graves Simcoe ( 1752-1806 ) .
York a été le nom de la ville de Toronto de 1793 à 1834 .
Fort York fut construit à l' entrée du port naturel de la ville , abrité par un long banc de sable en forme de péninsule .
La reddition de la ville fut négociée par John Strachan .
Au cours des cinq jours d' occupation , les soldats américains détruisirent une grande partie de Fort York et mirent le feu aux bâtiments abritant le parlement .
Le sac d' York fut la motivation première de l' incendie de Washington par les troupes britanniques en 1814 .
Le 6 mars 1834 , l' agglomération devient Toronto , l' année de son incorporation comme ville .
L' esclavage fut aboli dans tout le Haut-Canada en 1834 .
Le politicien réformiste William Lyon Mackenzie devint le premier maire de la ville de Toronto .
Le nom Toronto était autrefois celui d' un lac d' assez bonnes dimensions se trouvant à environ 120 kilomètres au nord de l' agglomération et qui se nomme aujourd'hui lac Simcoe ( du nom du premier gouverneur du Haut-Canada qui fit de York / Toronto sa capitale ) .
Le mot Toronto signifie " l' endroit où les racines des arbres trempent dans l' eau " dans un dialecte mohawk de l' est du Canada .
Au cours de son histoire , Toronto a été choisie à deux reprises comme capitale de la Province du Canada : une première fois entre 1849 et 1852 à causes de troubles à Montréal puis une deuxième fois entre 1856 et 1858 .
La ville de Toronto accueillie également du fait de son statut de capitale provinciale la résidence du lieutenant-gouverneur , représentant de la couronne .
La compagnie de chemin de fer du Grand Tronc du Canada et la Northern Railway of Canada se réunirent dans la construction de la première Gare Union au centre-ville .
En 1904 , le grand incendie de Toronto détruisit une partie importante du centre de Toronto .
Malgré sa croissance importante , Toronto reste dans les années 20 la seconde ville du Canada en terme économique et de population derrière la ville plus ancienne de Montréal .
Néanmoins en 1934 , la bourse de Toronto devint la plus importante du pays .
En 1951 , la population de Toronto dépassa le million d' habitants avec le commencement d' une grande suburbanisation .
En 1954 , la ville fut frappée par l' ouragan Hazel .
81 personnes furent tuées dans la région de Toronto , près de 1900 familles se retrouvèrent sans logement et l' ensemble des dégâts fut estimé à plus de 25 millions de dollars .
Dès les années 1960 , de grands projets immobiliers sont entrepris comme la construction de la First Canadian Place , haute tour ( 72 étages ) blanche du centre ville qui sera le premier grand projet du futur milliardaire Paul Reichmann .
En 1971 , Toronto comptait plus de deux millions d' habitants et dans les années 80 , elle devint la ville la plus peuplée et le principal centre économique du Canada dépassant Montréal .
En 1998 , la municipalité régionale disparaît au profit d' une seule ville , Toronto , le nouveau maire étant Mel Lastman , l' ancien maire de North York ( devenu un quartier du nord du nouveau Toronto ) .
Le maire actuel de Toronto est David Miller .
La ville de Toronto a une superficie de 630 km² avec une distance nord-sud maximale de 21 km et une distance est-ouest maximale de 43 km .
La ville de Toronto possède une côte de 46 km de long sur la partie nord-ouest du lac Ontario .
Les Toronto Islands et la partie portuaire de la ville , qui s' étendent vers l' intérieur du lac , offrent une protection à la partie de la côte qui se situe directement au sud de la partie centrale de la ville .
La côte est formée de sédiments qui ont été apportés naturellement par les courants du lac qui ont également créé les Toronto Islands .
Des viaducs , comme celui du prince Edouard , ont également été construits pour franchir ces obstacles .
Au cours de la glaciation de Würm , la partie sud de Toronto était sous le lac glaciaire Iroquois .
Le château Casa Loma surplombe ces escarpements .
Les Toronto Islands étaient à l' origine un cordon littoral naturel jusqu' à ce qu' une tempête en 1858 les coupe du littoral formant ainsi un chenal qui fut utilisé plus tard pour permettre l' accès aux docks .
Malgré la présence de nombreux ravins , Toronto n' est pas une ville spécialement vallonnée .
En regard de l' ensemble du territoire canadien , le climat de Toronto peut être considéré comme modéré .
En raison de l' urbanisation et de sa proximité avec le lac Ontario , Toronto présente une amplitude thermique entre le jour et la nuit relativement faible en particulier à cause de nuits plus chaudes .
C' est surtout la croissance de l' industrie automobile dans la banlieue de ( Oshawa ) qui a donné l' impulsion industrielle initiale à cet ancien centre d' abattoirs porcins .
Toronto est aujourd'hui le cœur bancaire , financier , commercial et culturel du Canada .
Les immeubles relativement hauts du centre-ville bénéficient depuis 2004 d' un mode de réfrigération innovant par le biais de pompes à chaleur alimentées par l' eau froide des profondeurs du lac Ontario .
De plus , Toronto est un centre très important pour la recherche médicale et scientifique avec de nombreux grands hôpitaux qui abritent des centres de recherche .
Toronto est au cœur de la vie intellectuelle et culturelle du Canada anglophone .
La plupart des maisons d' édition se trouvent à Toronto .
Toronto est également un centre de productions pour le film et le théâtre .
La région de Toronto compte aujourd'hui plus de 100 théâtres [ réf. nécessaire ] , où on peut voir tous les genres de spectacles , ainsi que plusieurs cinémas Imax , dont la technologie a été inventée à Toronto .
La célèbre auteure canadienne , Margaret Atwood , y vit depuis des années [ réf. nécessaire ] , ainsi de nombreux autres personnages très connus dans le monde de la culture canadienne [ réf. nécessaire ] .
La ville de Toronto regroupe de nombreuses institutions d' enseignement supérieur .
La plus ancienne , l' université de Toronto , fondée en 1827 , est la doyenne des universités de l' Ontario et une des principale institution de recherche du Canada .
Les deux autres universités d' importance sont l' université York , la troisième du Canada par son nombre d' étudiants , et l' université Ryerson .
Toronto possède également une grande offre de formation dans les domaines artistiques .
Le Conservatoire royal de musique est une école de musique réputée située dans le centre-ville .
La bibliothèque publique de Toronto possède le réseau le plus important du Canada avec 99 établissements et plus de onze millions d' ouvrages répertoriés dans ses collections .
Son réseau de bibliothèques est le quatrième plus important de l' Amérique du nord , juste après ceux des universités d' Harvard , de Yale et de Berkeley .
De plus , la bibliothèque centrale de Mississauga est la troisième en importance dans la région métropolitaine avec 5 étages .
Il n' existe pas de style architectural prédominant dans la ville de Toronto .
La tour CN est certainement le symbole de la ville de Toronto et la signature de son panorama urbain .
La majorité de ces gratte-ciel sont des immeubles résidentiels ; les tours à vocation commercial se regroupent principalement dans le pôle financier de la ville , et dans les banlieues de Mississauga , North York et Scarborough .
Le quartier historique , nommé Distillery District et situé dans le coin sud-est du centre-ville , est l' exemple de zone industrielle d' architecture victorienne le plus important et le mieux conservée d' Amérique du Nord .
La ville actuelle de Toronto est composée de six anciennes municipalités qui ont chacune développée une identité propre au cours de l' histoire .
Il s' agit du cœur historique de Toronto et reste la partie de la ville possédant la plus forte densité de population .
A partir du district financier , la skyline de Toronto s' étend vers le nord le long de la Rue Yonge .
Les nombreuses communautés résidentielles de Toronto possèdent des caractères architecturaux propres et se distinguent nettement des immeubles du centre financier et commercial .
Les banlieues de la couronne extérieure comprennent les anciennes municipalités de Etobicoke à l' ouest , Scarborough à l' est et North York au nord et sont très largement organisées selon un plan hippodamien .
Parmi toutes les villes canadiennes Toronto a la plus grande quantité d' équipes professionnelles , dont la plus populaire localement est son équipe de hockey , les Maple Leafs de Toronto .
Toronto est une ville cosmopolite .
Toronto est d' ailleurs une ville très communautaire et inclusive .
Ainsi , les francophones de la ville bénéficient d' une législation qui " garantit au public le droit de recevoir des services en français de la part des ministères et organismes du gouvernement de l' Ontario " .
L' aéroport le plus utilisé du Canada , l' aéroport international Toronto Paerson , déssert la partie ouest de la ville et la banlieue de la ville de Mississauga .
L' arme de service est le Glock 22 .
Ses véhicules sont de marque Ford ou GMC de fabrication états-unienne ou canadienne .
Il sert de cadre aux séries télévisées Détective Murdoch et Blue Murder .
