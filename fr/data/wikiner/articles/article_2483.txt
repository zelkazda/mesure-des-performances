L' usage du terme " pare-feu " en informatique est donc métaphorique : une porte empêchant les flammes de l' Internet de rentrer chez soi et/ou de " contaminer " un réseau informatique .
Le pare-feu était jusqu' à ces dernières années considéré comme une des pierres angulaires de la sécurité d' un réseau informatique ( il perd en importance au fur et à mesure que les communications basculent vers le HTTP sur SSL , court-circuitant tout filtrage ) .
Généralement , les zones de confiance incluent Internet ( une zone dont la confiance est nulle ) et au moins un réseau interne ( une zone dont la confiance est plus importante ) .
Enfin , le pare-feu est également souvent extrémité de tunnel IPsec ou SSL .
Certains protocoles dits " à états " comme TCP introduisent une notion de connexion .
Ceci est fondamental pour le bon fonctionnement de tous les protocoles fondés sur l' UDP , comme DNS par exemple .
Cependant dans le cas d' UDP , cette caractéristique peut être utilisée pour établir des connexions directes entre deux machines ( comme le fait Skype par exemple ) .
Par exemple , ce type de pare-feu permet de vérifier que seul du HTTP passe par le port TCP 80 .
Il est justifié par le fait que de plus en plus de protocoles réseaux utilisent un tunnel TCP pour contourner le filtrage par ports .
Ces protocoles sont dits " à contenu sale " ou " passant difficilement les pare-feu " car ils échangent au niveau applicatif ( FTP ) des informations du niveau IP ( échange d' adresses ) ou du niveau TCP ( échange de ports ) .
Une autre méthode est l' identification connexion par connexion , réalisée par exemple par la suite NuFW , qui permet d' identifier également sur des machines multi-utilisateurs .
C' est par exemple le cas des points d' accès Wi-Fi qui sont souvent protégés par ce genre de solution .
