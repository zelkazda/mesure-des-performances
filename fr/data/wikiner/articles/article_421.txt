Il est le moyen de communication d' une communauté estimée de 0.1 à 10 millions de locuteurs , présents dans la majorité des pays du monde ( 115 selon Ethnologue.com ) .
L' UNESCO a adopté plusieurs recommandations en faveur de l' espéranto .
L' espéranto fut composé entre la fin des années 1870 et le début des années 1880 par Ludwik Lejzer Zamenhof , un ophtalmologue polonais issu d' une famille juive de Białystok ( alors en Russie , maintenant en Pologne ) , ville alors peuplée de quatre communautés : juive , polonaise , allemande et biélorusse .
En Chine , les premiers cours furent donnés à Shanghai dès 1906 et à Canton dès 1908 .
Zamenhof n' était pas le premier à essayer de réunir les gens grâce à la création d' une langue commune mais cela reste l' essai le plus réussi .
En 1905 , le premier congrès mondial d' espéranto eut lieu en France à Boulogne-sur-Mer ; les caractéristiques de l' espéranto furent fixées et ses objectifs définis : cette langue devait être universellement comprise et parlée par l' humanité entière , sans aucune priorité .
En 2005 , le centenaire de l' espéranto fut célébré , de nouveau à Boulogne-sur-Mer .
Lors de la guerre d' Espagne ( 1936 -- 1939 ) , la mouvance anarchiste représentait l' essentiel des défenseurs de l' espéranto ; il était aussi utilisé par des socialistes , des communistes allergiques au stalinisme , dont George Orwell , à qui l' espéranto inspira la novlangue , qu' il utilise pourtant de manière péjorative dans ses romans , et même une partie de la droite catholique .
Il existe des universités espérantophones en Roumanie ( Sibiu ) , en Bulgarie , en Slovaquie à ( Komárno ) .
En France , il n' est pas reconnu dans l' enseignement .
En Hongrie , où cette reconnaissance a eu lieu , l' espéranto fait partie des cinq premières langues étrangères .
Deux millions est le chiffre le plus couramment repris , comme , par exemple , dans Ethnologue.com .
Il existe cependant un petit nombre de locuteurs dont il est la langue maternelle , le plus connu étant l' homme d' affaire George Soros .
Ce groupe linguistique a constitué le répertoire de base à partir duquel Ludwik Lejzer Zamenhof a " composé " sa langue internationale .
Le Fundamento de Esperanto préconise dans ce cas de remplacer les lettres diacritées par des digrammes composés de la lettre de base suivie d' un h , les éventuelles ambiguïtés étant levées par l' ajout d' un tiret entre les monèmes .
Les trois systèmes ( ŝ , sh , sx ) coexistent sur Internet .
Le conditionnel est le mode du fictif , de l' irréel ; il s' emploie aussi bien en proposition principale qu' en proposition subordonnée : Mi povus , se mi volus. " Je pourrais si je voulais " .
" , Mi proponas ke ni laboru kune. " Je propose que nous travaillions ensemble " ) .
À l' inverse du français , mais à l' instar des langues slaves , l' espéranto ne pratique pas la concordance des temps : Mi ne sciis ke li venos. " Je ne savais pas qu' il viendrait " .
Zamenhof a suivi diverses méthodes pour adapter ses sources lexicales à l' espéranto .
Lorsque plusieurs de ses sources comportaient des mots proches par la forme et le sens , Zamenhof a souvent créé un moyen terme ( ex .
français chef / anglais chief ; forgesi " oublier " , cf .
allemand vergessen / anglais to forget ; gliti " glisser " , cf .
français glisser / allemand gleiten / anglais to glide ; lavango " avalanche " , cf .
Le vocabulaire de l' espéranto comprenait quelques centaines de radicaux dans le Fundamento de Esperanto de 1905 .
En 2002 , après un siècle d' usage , le plus grand dictionnaire monolingue espérantiste ( Plena Ilustrita Vortaro de Esperanto ) , en comprend 16.780 correspondant à 46.890 éléments lexicaux .
Ainsi , du mot reto ( " réseau , filet " ) , on a extrait le radical ret- pour former tout un ensemble de mots liés à Internet : retadreso ( " adresse de courriel " ) , retpirato ( " pirate informatique " ) , etc .
Les partisans de l' espéranto y opposent le fait que la langue ait été employée sur une longue durée et dans de nombreux pays ( 115 selon Ethnologue.com ) , et argumentent que le nombre de locuteurs ne présume pas de ses qualités intrinsèques , mais relève plutôt de l' absence de soutien politique ; son adoption officielle comme langue de travail ( par exemple comme langue pivot pour les traductions au sein de l' Union européenne ) aurait pour effet d' augmenter rapidement le nombre de locuteurs .
L' espéranto donne lieu à un mouvement militant qui s' est notamment traduit par l' apparition en France de la liste Europe Démocratie Espéranto aux élections européennes de juin 2004 .
L' UNESCO a fait plusieurs recommandations en faveur de l' espéranto .
Zamenhof lui-même proposa des modifications et des réformes , mais celles-ci furent refusées par le comité Espéranto .
La volonté de corriger certains défauts de l' espéranto a poussé certaines personnes à créer des variantes telles que l' Ido ou à proposer des réformes importantes telles que le riisme .
Ce discours est typique du style de Zamenhof , empreint de naïveté pour ses détracteurs , d' un profond humanisme pour ses sympathisants .
Des livres ( comme la Bible , Le Petit Prince ou le Manifeste du parti communiste ) ont été traduits en espéranto ; les œuvres originales en espéranto sont également bien représentées .
Dans le film de Charlie Chaplin , Le Dictateur , les plaques des magasins du ghetto juif sont en espéranto , décrite comme " langue juive internationale " par Hitler dans Mein Kampf .
Il est aussi possible d' entendre de l' espéranto dans la version originale du film Bienvenue à Gattaca .
Dans son album HIStory , Michael Jackson exploite la sonorité à la fois internationale et exotique de l' espéranto , en prononçant quelques phrases dans cette langue .
La pochette de l' album OK Computer de Radiohead présente de nombreuses phrases , dont certaines en espéranto .
Dans le domaine de l' informatique , Java fut qualifié d' espéranto des langages de programmation , en particulier à cause de sa simplicité et de son universalité ( indépendance par rapport au système d' exploitation ) , métaphore reprise pour XML , qualifié à son tour d' espéranto du système d' information .
