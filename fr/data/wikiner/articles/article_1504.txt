C' est sans doute vers l' époque de Charlemagne que les gens s' aperçoivent de cette évolution : ils ne parlent plus le latin mais l ' " ancêtre " du français .
Mais il faudra attendre François I er pour que cette langue supplante le latin comme langue écrite et bien plus longtemps encore pour qu' elle soit comprise et parlée dans toutes les régions .
