Henry James est un écrivain américain né à New York le 15 avril 1843 et mort à Londres le 28 février 1916 .
Ayant longtemps vécu en Angleterre , il devient un sujet britannique peu avant sa mort .
Henry James voulait convaincre les écrivains britanniques et américains de présenter leur vision du monde avec la même liberté que les auteurs français .
La fortune acquise par son grand-père , émigré irlandais arrivé aux États-Unis en 1789 , avait mis la famille à l' abri des servitudes de la vie quotidienne .
Son frère aîné , William James , deviendra professeur à Harvard et se fera connaître pour sa philosophie pragmatiste .
À l' âge de 19 ans , il est brièvement inscrit à la Faculté de droit de Harvard , rapidement abandonnée face au désir d' être " tout simplement littéraire " .
Après Washington Square , Portrait de femme est souvent considéré comme une conclusion magistrale de la première manière de l' écrivain .
Sa mère décède en janvier 1882 , alors que Henry James séjourne à Washington .
Il revient à Londres en mai et effectue un voyage en France .
Il rentre de façon précipitée aux États-Unis où son père meurt le 18 décembre , avant son arrivée .
Il revient à Londres au printemps 1883 .
En 1886 , il publie deux romans , Les Bostoniennes et La Princesse Casamassima , qui associent à des thèmes politiques et sociaux ( féminisme et anarchisme ) la recherche d' une identité personnelle .
En 1891 , une version dramatique de L' Américain rencontre un petit succès en province , mais reçoit un accueil plus mitigé à Londres .
Puis , entre 1902 et 1904 , viennent les derniers grands romans : Les Ailes de la colombe , Les Ambassadeurs et La Coupe d' or .
En 1903 , James a soixante ans et un " mal du pays passionné " l' envahit .
Le 30 août 1904 , il débarque à New York , pour la première fois depuis vingt ans .
Il quitte les États-Unis le 5 juillet 1905 , après avoir donné de nombreuses conférences à travers tout le pays .
Avant son retour en Angleterre , il met au point , avec les Éditions Scribner , le projet d' une édition définitive de ses écrits , The Novels and Tales of Henry James , New York Edition , qui comportera , à terme , vingt-six volumes .
En 1915 , déçu par l' attitude des États-Unis face à la guerre qui fait rage sur le continent , il demande et obtient la nationalité britannique .
Henry James nourrit très tôt l' ambition d' une carrière d' homme de lettres .
Ses biographes et les critiques littéraires permettent de citer Henrik Ibsen , Nathaniel Hawthorne , Honoré de Balzac , et Ivan Tourgueniev comme ses influences majeures .
Il révisa ses grands romans et de nombreux contes et nouvelles et contes pour l' édition d' anthologie de son œuvre de fiction dont les vingt-trois volumes constitue son autobiographie artistique qu' il nomma " The New York Edition " pour réaffirmer les liens qui l' ont toujours uni à sa ville natale .
À différents moments de sa carrière , Henry James écrivit des pièces de théâtre , en commençant par des pièces en un acte pour des magazines , entre 1869 et 1871 , et l' adaptation dramatique de sa fameuse nouvelle Daisy Miller en 1882 .
De 1890 à 1892 , il se consacre à réussir sur la scène londonienne , écrivant six pièces dont seule l' adaptation de son roman L' Américain sera produite .
Celle-ci fut représentée plusieurs années de suite par une compagnie de répertoire et avec succès à Londres , sans toutefois s' avérer très lucrative pour son auteur .
Henry James ne voulait plus écrire pour le théâtre .
Deux d' entre elles étaient en production au moment de la mort d' Édouard VII le 6 mai 1910 qui plongea Londres dans le deuil , entraînant la fermeture des théâtres .
Henry James ne s' est jamais marié .
Installé à Londres , il se présentait comme un célibataire endurci et rejetait régulièrement toute suggestion de mariage .
À mesure de la mise à jour des archives , dont les journaux intimes de contemporains et des centaines de lettres sentimentales et parfois érotiques écrites par Henry James à des hommes plus jeunes que lui , la figure du célibataire névrosé laisse la place à celle de l' homosexuel honteux .
Henry James rencontra le jeune artiste de 27 ans à Rome en 1899 , alors qu' il avait 56 ans , et lui écrivit des lettres particulièrement enflammées : " Je te tiens , très cher garçon , dans mon amour le plus profond et en espère autant pour moi ; dans chaque battement de ton âme " .
Au lieu de quoi je ne peux qu' essayer de vivre sans toi, " et ce n' est que dans les lettres à de jeunes hommes que Henry James se déclare leur " amant " .
Dans sa correspondance avec Hugh Walpole , il joue sur les mots à propos de leur relation , se voyant lui-même comme un " éléphant " qui " te tripote , de tellement bonne grâce " et enchaîne à propos de " la vieille trompe expressive " de son ami .
Ses lettres , discrètement reproduites , à Walter Berry ont longtemps été appréciées pour leur érotisme légèrement voilé .
Henry James est l' une des figures majeures de la littérature transatlantique .
Henry James explore ces conflits de cultures et de personnalités , dans des récits où les relations personnelles sont entravées par un pouvoir plus ou moins bien exercé .
Bien que toute sélection des romans de Henry James repose inévitablement sur une certaine subjectivité , les livres suivants ont fait l' objet d' une attention particulière dans de nombreuses critiques et études .
C' est même le principal sujet de L' Américain ( 1877 ) .
Henry James écrit ensuite Washington Square ( 1880 ) , une tragicomédie relativement simple qui rend compte du conflit entre une fille , douce , soumise et maladroite , et son père , un brillant manipulateur .
Le roman est souvent comparé à l' œuvre de Jane Austen pour la grâce et la limpidité de sa prose , et la description centrée sur les relations familiales .
Comme Henry James n' était pas particulièrement enthousiaste au sujet de Jane Austen , il n' a sans doute pas trouvé la comparaison flatteuse .
En fait , il n' était pas non plus très satisfait de Washington Square .
En tentant de le relire pour l' inclure dans la New York Edition de sa fiction ( 1907 -- 09 ) , il s' aperçut qu' il ne pouvait pas .
Avec Portrait de femme ( 1881 ) Henry James achève la première phase de sa carrière par une œuvre qui demeure son roman le plus connu .
Ce roman est assez unique dans l' œuvre jamesienne , par le sujet traité ; mais il est souvent associé aux Bostoniennes , qui évoque aussi le milieu politique .
Ce livre reflète l' intérêt dévorant de Henry James pour le théâtre , et est souvent considéré comme le dernier récit de la deuxième phase de sa carrière romanesque .
Henry James poursuit son approche plus impliquée et plus psychologique de sa fiction avec Ce que savait Maisie ( 1897 ) , l' histoire de la fille raisonnable de parents divorcés irresponsables .
Dans la préface à sa parution dans New York Edition , Henry James place ce livre au sommet de ses réussites , ce qui provoqua quelques remarques désapprobatrices .
La Coupe d' or ( 1904 ) est une étude complexe et intense du mariage et de l' adultère qui termine cette " phase majeure " et essentielle de l' œuvre romanesque de James .
Henry James était particulièrement intéressé par ce qu' il appela la " belle et bénie nouvelle " , ou les récits de taille intermédiaire .
Les récits suivants sont représentatifs du talent de Henry James dans ce court format de la fiction .
Au-delà de son œuvre de fiction , Henry James fut l' un des plus importants critiques littéraires dans l' histoire du roman .
