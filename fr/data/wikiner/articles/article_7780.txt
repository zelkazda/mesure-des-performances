Adonis est un personnage du manga et de l' anime Berserk .
Guts est forcé de le tuer après avoir assassiné son père , car le garçon l' a surpris et a vu son visage .
Selon la légende , Adonis , amant de la déesse Aphrodite , fut tué par un sanglier envoyé par Arès jaloux ; selon certains , c' est Arès lui-même qui s' était changé en sanglier .
D' après James George Frazer ( Le Rameau d' or , t .
