Cosqueville est située en bord de mer .
En 1973 , elle a fusionné avec Angoville-en-Saire et Vrasville , qui ont gardé le statut de communes associées .
Le conseil municipal est composé de 11 membres , dont 2 représentant la commune associée de Vrasville et 1 celle d' Angoville-en-Saire .
