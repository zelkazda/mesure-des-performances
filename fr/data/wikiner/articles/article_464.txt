Ce grand nombre de variétés a inspiré au général de Gaulle un mot resté célèbre : " Comment voulez-vous gouverner un pays où il existe 246 variétés de fromage ? " .
Le slogan " l' autre pays du fromage " pour désigner la " Hollande " est une référence à la France comme premier pays du fromage .
