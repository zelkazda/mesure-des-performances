Michel Françaix , homme politique français , né le 28 mai 1943 à Paris .
Après un début de carrière auprès de François Mitterrand , dont il a été un des conseillers , Michel Françaix s' investit dans l' appareil départemental du Parti Socialiste , dont il devient le premier secrétaire fédéral .
Ami de longue date de Ségolène Royal , il est un de ses soutiens pour sa candidature à l' investiture pour les élections présidentielles de 2007 .
