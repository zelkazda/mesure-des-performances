Le Feu de Wotan est le quatorzième album de la série Yoko Tsuno , écrite et dessinée par Roger Leloup , sorti en 1984 .
Elle demande à Yoko Tsuno , électronicienne de venir examiner cet appareil .
Il a été prépublié dans le Journal de Spirou numéros 2388 à 2391 du 19 janvier au 9 février 1984 .
