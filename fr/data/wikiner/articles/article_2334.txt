L' Arizona a été exploré par Marcos de Niza en 1539 .
L' Espagne y exerçait sa puissance coloniale jusqu' à l' indépendance du Mexique en 1821 .
Les États-Unis ont pris l' Arizona au Mexique après la Guerre américano-mexicaine en 1848 .
D' une surface de 295 260 km² , l' Arizona est peuplé de 5 130 632 habitants ( 2000 ) .
Les principaux cours d' eau sont le Colorado , fleuve qui a creusé le Grand Canyon , la Gila et la Salt River .
La capitale de l' Arizona est Phoenix .
Les autres villes importantes sont Tucson , Mesa , et Flagstaff .
Classement des villes les plus peuplées de l' Arizona :
Selon le bureau du recensement des États-Unis , l' Arizona avait une population estimée à 6166318 habitants en 2006 , ce qui représente une augmentation de 213311 habitants , soit 3,6 % , par rapport à l' année précédente ; et une augmentation de 1035686 personnes , soit 20,2 % , depuis l' an 2000 .
L' immigration interne aux États-Unis représente 541283 personnes , les immigrés venant d' autres pays représentent 204661 personnes .
Le gouverneur de l' Arizona est élu pour quatre ans .
Depuis 1952 , lors des élections présidentielles , l' Arizona a constamment voté pour les candidats républicains .
En 2004 , l' Arizona a voté à 54,83 % pour George W. Bush contre 44,37 % à John Kerry .
Le président mexicain a même pris parti contre cette loi en demandant à ses ressortissants de ne pas voyager en Arizona .
Les touristes fréquentent les nombreux parcs naturels et historiques de l' Arizona .
Le Grand Canyon est le plus célèbre , le plus fréquenté ( 4,3 millions de visiteurs en 2004 ) et le plus étendu de tous .
Il y a deux grandes universités en Arizona : l' université d' Arizona à Tucson et l' université d' État de l' Arizona à Tempe .
