Wall Street est le nom d' une rue située dans le sud du borough de Manhattan à New York ( États-Unis ) .
Avec le temps , Wall Street a fini par désigner l' ensemble du quartier ainsi que la plus importante bourse du monde , le New York Stock Exchange ( NYSE ) .
La ville s' appelait alors " La Nouvelle Amsterdam " ( principalement établie sur l' île de Manhattan ) , et la colonie était baptisée " Nouvelle-Néerlande " .
Bien que le nom " Wall street " ( signifiant " rue du mur " en anglais ) tienne de l' existence d' un seul et même mur , à la place de la rue actuelle , les plans de la Nouvelle Amsterdam montrent deux noms différents pour cette rue .
En effet , vers 1630 , la population totale de Nouvelle-Néerlande était de 300 personnes , dont une grande majorité de Wallons .
Durant le XVII e siècle , Wall Street forma la frontière nord de la Nouvelle Amsterdam .
Plus tard , sur ordre de la Compagnie néerlandaise des Indes occidentales , avec l' aide d' esclaves venus d' Afrique noire , les Néerlandais bâtirent une palissade plus solide .
À cette époque , la deuxième Guerre anglo-néerlandaise faisait rage , un mur de quatre mètres de haut fut créé en 1653 .
La Nouvelle Amsterdam devint " New York " ( " la nouvelle York " ) .
Malgré le fait que le nom original faisait référence aux Wallons qui avaient contribué à la création de la ville de New York , " Wall Street " fut repris en mémoire du mur érigé à cet endroit autrefois ( " wall " signifiant " mur " en anglais ) .
En 1792 , les traders formalisèrent leur union avec l ' " accord de Buttonwood " ( " accord du platane " ) , en souvenir de cet arbre sous lequel ils avaient commencé leur échanges .
Ce média de la presse écrite , publié dans la ville de New York , existe toujours et possède une grande influence dans le monde les quotidiens économiques .
Il fut pendant longtemps le journal le plus lu des États-Unis d' Amérique , bien que désormais second national après USA Today .
Il appartient au Dow Jones and Company .
La rue de Wall Street présente plusieurs édifices intéressants , dont plusieurs sont classés au patrimoine historique de la ville de New York .
Une statue de George Washington devant l' entrée rappelle que c' est là que le premier président américain prononça son serment d' investiture en 1789 .
Un attentat à Wall Street le 16 septembre 1920 fit 38 morts et 200 blessés , les coupables ne furent jamais arrêtés .
