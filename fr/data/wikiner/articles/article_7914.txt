Louis Pasteur est né le 27 décembre 1822 à Dole .
En 1825 la famille quitte Dole pour Marnoz , pour enfin s' installer à Arbois en 1830 .
Le jeune Pasteur suit à Arbois les cours d' enseignement mutuel puis entre au collège de la ville .
Pasteur retourne de nouveau à Paris et est finalement admis à l' École normale en 1843 .
À l' École normale , Pasteur étudie la chimie et la physique , ainsi que la cristallographie .
Il est professeur à Dijon puis à Strasbourg de 1848 à 1853 .
Il est ensuite nommé professeur de chimie et doyen de la faculté des sciences de Lille nouvellement créée en 1854 .
Pasteur , qui s' intéressait à la fermentation depuis 1849 ( voir plus loin ) , est stimulé dans ces travaux par les demandes des brasseurs lillois concernant la conservation de la bière .
Après Frédéric Kuhlmann et Charles Delezenne , Pasteur est ainsi un des premiers en France à établir des relations fructueuses entre l' enseignement supérieur et l' industrie chimique .
En 1857 , il est nommé administrateur chargé de la direction des études à l' École normale supérieure .
En 1861 , Pasteur publie ses travaux réfutant la théorie de la génération spontanée .
En 1862 , il est élu à l' Académie des sciences en remplacement de Henri Hureau de Senarmont .
En 1865 , Dumas le nomme membre , avec Claude Bernard et Sainte-Claire Deville , d' une commission chargée d' étudier l' étiologie du choléra .
Les trois savants , qui cherchent le principe de la contagion dans l' air ( alors que Snow , dans un travail publié en 1855 , avait montré qu' il était dans l' eau ) , ne trouvent pas le microbe , que Pacini avait pourtant fait connaître en 1854 .
À l' École normale supérieure , Pasteur est jugé autoritaire aussi bien par ses collègues que par les élèves et se heurte à de nombreuses contestations , ce qui le pousse à démissionner , en 1867 , de ses fonctions d' administrateur .
Il reçoit une chaire en Sorbonne et on crée , à l' École normale même , un laboratoire de chimie physiologique dont la direction lui est confiée .
La défaite de 1870 et la chute de Napoléon III sont un coup terrible pour Pasteur , grand patriote et très attaché à la dynastie impériale .
L' Assemblée nationale lui vote une récompense pour le remercier de ses travaux dont les conséquences économiques sont considérables .
En 1876 , Pasteur se présente aux élections sénatoriales , mais c' est un échec .
En 1881 , l' équipe de Pasteur met au point le vaccin contre le charbon des moutons .
Sa famille décida de l' enterrer dans une crypte de l' Institut Pasteur .
Dans les travaux que Pasteur a réalisés au début de sa carrière scientifique en tant que chimiste , il résolut en 1848 un problème qui allait par la suite se révéler d' importance capitale dans le développement de la chimie contemporaine : la séparation des deux formes de l' acide tartrique .
En 1844 , Mitscherlich avait affirmé que , parmi les couples tartrate / paratartrate , il y en avait un , à savoir le couple " tartrate double de soude et d' ammoniaque " / " paratartrate double de soude et d' ammoniaque " , où le tartrate et le paratartrate n' étaient discernables que par la propriété rotatoire , présente dans le tartrate et absente dans le paratartrate .
En particulier , ce tartrate et ce paratartrate avaient , selon Mitscherlich , la même forme cristalline .
Pasteur eut peine à croire " que deux substances fussent aussi semblables sans être tout à fait identiques " .
Il refit les observations de Mitscherlich et s' avisa d' un détail que Mitscherlich n' avait pas remarqué : dans le tartrate en question , les cristaux présentent une dissymétrie ( " hémiédrie " ) , toujours orientée de la même façon ; en revanche , dans le paratartrate correspondant , il coexiste deux formes de cristaux , images spéculaires non superposables l' une de l' autre , et dont l' une est identique à celle du tartrate .
La déviation du plan de polarisation par les solutions étant considérée , depuis les travaux de Biot , comme liée à la structure de la molécule , Pasteur conjectura que la dissymétrie de la forme cristalline correspondait à une dissymétrie interne de la molécule , et que la molécule en question pouvait exister en deux formes dissymétriques inverses l' une de l' autre .
Depuis les travaux de Pasteur , l' acide racémique ou paratartrique est considéré comme composé d' un acide tartrique droit ( l' acide tartrique connu antérieurement ) et d' un acide tartrique gauche .
Pasteur avait correctement démontré ( par l' examen des cristaux puis par l' épreuve polarimétrique ) que l' acide paratartrique est composé de deux formes distinctes d' acide tartrique .
En revanche , la relation qu' il crut pouvoir en déduire entre la forme cristalline et la constitution de la molécule était inexacte , le cas spectaculaire de l' acide paratartrique étant loin d' être l' illustration d' une loi générale , comme Pasteur s' en apercevra lui-même .
François Dagognet dit à ce sujet : " la stéréochimie n' a rien conservé des vues de Pasteur , même s' il demeure vrai que les molécules biologiques sont conformées hélicoïdalement " .
Gerald L. Geison a noté chez Pasteur une tendance à atténuer sa dette envers Auguste Laurent pour ce qui est de la connaissance des tartrates .
En 1849 , Biot signale à Pasteur que l' alcool amylique dévie le plan de polarisation de la lumière et possède donc la propriété de dissymétrie moléculaire .
Pasteur estime peu vraisemblable que l' alcool amylique hérite cette propriété du sucre dont il est issu ( par fermentation ) , car , d' une part , la constitution moléculaire des sucres lui paraît très différente de celle de l' alcool amylique et , de plus , il a toujours vu les dérivés perdre la propriété rotatoire des corps de départ .
Cagniard de Latour et Theodor Schwann avaient apporté des faits supplémentaires à l' appui de la nature vivante de la levure .
Dans le même ordre d' idées , Jean-Baptiste Dumas , en 1843 ( époque où le jeune Pasteur allait écouter ses leçons à la Sorbonne ) , décrivait le ferment comme un être organisé et comparait son activité à l' activité de nutrition des animaux .
Berzélius , lui , avait eu une conception purement catalytique de la fermentation , qui excluait le rôle d' organismes vivants .
Liebig , de façon plus nuancée , avait des idées analogues : il voulait bien envisager que la levure fût un être vivant , mais il affirmait que si elle provoquait la fermentation , ce n' était pas par ses activités vitales mais parce qu' en se décomposant , elle était à l' origine de la propagation d' un état de mouvement ( vibratoire ) .
Pasteur " dispose d' une première orientation donnée par Cagniard de Latour ; il la développe et montre que c' est en tant qu' être vivant que la levure agit , et non en tant que matière organique en décomposition. "
Même si Liebig resta sur ses positions , les travaux de Pasteur furent généralement accueillis comme prouvant définitivement le rôle des organismes vivants dans la fermentation .
En 1878 , Berthelot publia un travail posthume de Claude Bernard qui , contredisant Pasteur , mettait l' accent sur le rôle des " ferments solubles " dans la fermentation alcoolique .
Il en résulta entre Pasteur et Berthelot une des controverses célèbres de l' histoire des sciences .
Pasteur ne rejetait pas absolument le rôle des " ferments solubles " .
Dans le cas particulier de la fermentation ammoniacale de l' urine , il considérait comme établi , à la suite d' une publication de Musculus , que la cause proche de la fermentation était un " ferment soluble " ( dans ce cas , l' enzyme qu' on appellera " uréase " ) produit par le ferment microbien qu' il avait découvert lui-même .
On s' accorde donc à penser que Pasteur fut incapable de comprendre l' importance des " ferments solubles " ( consacrée depuis par les travaux d ' Eduard Buchner ) et souligna le rôle des micro-organismes dans les " fermentations proprement dites " avec une insistance excessive , qui n' allait pas dans le sens du progrès de l' enzymologie .
On met cette répugnance de Pasteur à relativiser le rôle des organismes vivants sur le compte de son vitalisme , qui l' empêcha aussi de comprendre le rôle des toxines et d' admettre en 1881 , lors de sa rivalité avec Toussaint dans la course au vaccin contre le charbon , qu' un vaccin " tué " pût être efficace .
Les travaux de Pasteur sur la fermentation ont fait l' objet d' un débat dans les années 1970 et 1980 , la question étant de savoir si , en parlant de " fermentations proprement dites " , Pasteur avait commis une tautologie qui lui permettait de prouver à peu de frais la cause biologique des fermentations .
À partir de 1859 , Pasteur mène une lutte contre les partisans de la " génération spontanée " , en particulier contre Félix Archimède Pouchet et un jeune journaliste , Georges Clemenceau ; ce dernier , médecin , met en cause les compétences de Pasteur , qui ne l' est pas , et attribue son refus de la génération spontanée à un parti-pris idéologique ( Pasteur est chrétien ) .
En 1837 , déjà , Schwann a fait une expérience que les adversaires de la génération spontanée considèrent comme probante en faveur de leur thèse : il a montré que si l' air est chauffé ( puis refroidi ) avant de pouvoir exercer son influence , la vie n' apparaît pas .
Pasteur fait des expériences en divers lieux , temps et altitudes et montre que ( si on laisse pénétrer l' air ambiant sans le débarrasser de ses germes ) la proportion des bocaux contaminés est d' autant plus faible que l' air est plus pur .
Ainsi , sur la Mer de Glace , une seule des vingt préparations s' altère .
foin contient d' ordinaire , comme Cohn l' a montré depuis , un bacille très ténu ( ... ) .
( Émile Duclaux ajoute que Pasteur revint de son erreur . )
On considère que c' est John Tyndall qui , en suivant les idées de Cohn , mettra la dernière main à la réfutation de la génération spontanée .
Pasteur estimait d' ailleurs que la génération spontanée n' était pas réfutée de façon absolue , mais seulement dans les expériences par lesquelles on avait prétendu la démontrer .
Il y a une autre circonstance où , dans ses travaux sur la génération spontanée , Pasteur peut sembler tendancieux , puisqu' il admet avoir passé sous silence des constatations qui n' allaient pas dans le sens de sa thèse .
( Pasteur , qui était spiritualiste , voyait un lien entre matérialisme et adhésion à la génération spontanée , mais se défendait de s' être lui-même laissé influencer par cette sorte de considérations dans ses travaux scientifiques . )
En fait , ils exonèrent Pasteur et blâment plutôt une conception aseptisée de la méthode scientifique : " Pasteur savait ce qui devait être considéré comme un résultat et ce qui devait l' être comme une ' erreur ' .
Pasteur était un grand savant , mais la manière dont il a agi ne s' approche guère de l' idéal de la méthode scientifique proposé de nos jours .
Napoléon III demande à Pasteur , spécialiste de la fermentation et de la putréfaction , de chercher un remède .
La pasteurisation du lait , en revanche , à laquelle Pasteur n' avait pas pensé ( c' est le chimiste allemand Franz von Soxhlet qui , en 1886 , proposa d' appliquer la pasteurisation au lait ) , s' implanta durablement .
( Ici encore , d' ailleurs , on marchait sur les traces d' Appert . )
La théorie de l' origine microbienne des maladies contagieuses existait depuis longtemps à l' état d' hypothèse quand , vers 1835 , quelques savants , dont on a surtout retenu Agostino Bassi , la prouvèrent pour la première fois dans le cas de la muscardine , une des maladies du ver à soie .
Ainsi , la découverte du bacille du choléra était restée quasiment lettre morte quand Pacini l' avait publiée en 1854 , alors qu' elle devait trouver immédiatement une vaste audience quand Koch la refit en 1883 .
À l' époque des débuts de Pasteur , donc , la théorie microbienne existe , même si elle est encore dans l' enfance .
Il n' est donc pas étonnant , dans ce contexte , que les travaux de Pasteur sur la fermentation aient stimulé le développement de la théorie microbienne des maladies contagieuses .
En 1860 , après avoir réaffirmé le rôle des organismes vivants dans la putréfaction et la fermentation , Pasteur lui-même ajoutait : " Je n' ai pas fini cependant avec toutes ces études .
On verra toutefois que Pasteur , quand il aura à s' occuper des maladies des vers à soie , en 1865 , commencera par nier le caractère microbien de la pébrine , compris par d' autres avant lui .
Dans une lettre de 1874 , il remercie Pasteur " pour m' avoir , par vos brillantes recherches , démontré la vérité de la théorie des germes de putréfaction , et m' avoir ainsi donné le seul principe qui ait pu mener à bonne fin le système antiseptique. "
Asepsie Pasteur " est de ceux qui cherchent à dépasser l' antisepsie par l' asepsie . "
Certes , ces recommandations n' étaient pas d' une nouveauté absolue : Semmelweis et d' autres avant lui ( par exemple Claude Pouteau et Jacques Mathieu Delpech ) avaient déjà compris que les auteurs des actes médicaux pouvaient eux-mêmes transmettre l' infection , et ils avaient fait des recommandations en conséquence , mais les progrès de la théorie microbienne avaient tellement changé les données que les conseils de Pasteur reçurent beaucoup plus d' audience que ceux de ses prédécesseurs .
Pasteur accepte et fera quatre longs séjours à Alès , entre le 7 juin 1865 et 1869 .
Arrivé à Alès , Pasteur se familiarise avec la pébrine et aussi avec une autre maladie du ver à soie , connue plus anciennement que la pébrine : la flacherie ou maladie des morts-flats .
Contrairement , par exemple , à Quatrefages , qui avait forgé le mot nouveau pébrine , Pasteur commet l' erreur de croire que les deux maladies n' en font qu' une et même que la plupart des maladies des vers à soie connues jusque-là sont identiques entre elles et à la pébrine .
Il commet une autre erreur : il commence par nier le caractère " parasitaire " ( microbien ) de la pébrine , que plusieurs savants ( notamment Antoine Béchamp ) considéraient comme bien établi .
Même une note publiée le 27 août 1866 par Balbiani , que Pasteur semble d' abord accueillir favorablement , reste sans effet , du moins immédiat .
" Pasteur se trompe .
Alors que Pasteur n' a pas encore compris la cause de la maladie , il propage un procédé efficace pour enrayer les infections : on choisit un échantillonnage de chrysalides , on les broie et on recherche les corpuscules dans le broyat ; si la proportion de chrysalides corpusculeuses dans l' échantillonnage est très faible , on considère que la chambrée est bonne pour la reproduction .
À partir de 1876 , Pasteur travaille successivement sur le filtre et l' autoclave , tous deux mis au point par Charles Chamberland ( 1851-1908 ) , et aussi sur le flambage des vases .
Mais il devient bientôt un des partisans les plus actifs et les plus en vue de la théorie microbienne des maladies contagieuses , domaine où son plus grand rival est Robert Koch .
Quand Pasteur commence ses recherches sur les vaccins , on fait des inoculations préventives contre une maladie humaine , la variole ( la méthode de Jenner est célèbre ) , et contre deux maladies du bétail : la clavelée , maladie du mouton , et la péripneumonie bovine .
Durant l' été 1879 , Pasteur et ses collaborateurs , Émile Roux et Émile Duclaux , découvrent que les poules auxquelles on a inoculé des cultures vieillies du microbe du choléra des poules non seulement ne meurent pas mais résistent à de nouvelles infections -- c' est la découverte d' un vaccin d' un nouveau type : contrairement à ce qui était le cas dans la vaccination contre la variole , on ne se sert pas , comme vaccin , d' un virus bénin fourni par la nature ( sous forme d' une maladie bénigne qui immunise contre la maladie grave ) mais on provoque artificiellement l' atténuation d' une souche initialement très virulente et c' est le résultat de cette atténuation qui est utilisé comme vaccin .
Certains voient là un demi-aveu de l' irrégularité du vaccin , irrégularité que la suite confirma : " Cette voie , que le génie de Pasteur avait ouverte et qui fut ensuite si féconde , se révéla bientôt fermée en ce qui concerne la vaccination anti-pasteurellique de la poule .
La théorie de Pasteur , selon laquelle la virulence du vaccin était atténuée par l' action de l' oxygène , n' a pas été retenue .
Bien que l' oxygène puisse jouer un rôle en accélérant les processus d' autolyse , il n' a probablement pas une action aussi directe que Pasteur le pensait . "
Certains auteurs reprochent à Pasteur d' avoir induit le public scientifique en erreur sur la nature exacte du vaccin utilisé .
La même année 1879 , Galtier montre qu' on peut utiliser le lapin , beaucoup moins dangereux que le chien , comme animal d' expérimentation .
( Galtier ne précise pas que le moyen préventif auquel il pense doive être un vaccin . )
Les choses en sont là quand Pasteur , en 1881 , commence ses publications sur la rage .
Dans cette note de 1881 , Galtier n' est nommé qu' une fois , et c' est pour être contredit ( avec raison ) .
En décembre 1882 , nouvelle note de Pasteur et de ses collaborateurs , établissant que le système nerveux central est le siège principal du virus , où on le trouve à l' état plus pur que dans la salive , et signalant des cas d' immunisation d' animaux par inoculation du virus , autrement dit des cas de vaccination .
En 1885 , Pasteur se dit capable d' obtenir une forme du virus atténuée à volonté en exposant de la moelle de lapin rabique au contact de l' air gardé sec .
Le 6 juillet 1885 , on amène à Pasteur un petit berger alsacien de Steige âgé de neuf ans , Joseph Meister , mordu l' avant-veille par un chien qui avait ensuite mordu son propriétaire .
Cette incertitude du diagnostic rend le cas plus délicat que les précédents et Roux , l' assistant de Pasteur dans les recherches sur la rage , refuse formellement de participer à l' injection .
Joseph Meister reçoit treize inoculations réparties sur dix jours .
À propos de la seconde de ces trois phrases , André Pichot , dans son anthologie d' écrits de Pasteur , met une note : " Cette phrase est un peu déplacée , dans la mesure où il s' agissait ici de soigner un être humain ( et non de faire une expérience sur un animal ) . "
L' efficacité du vaccin de Pasteur remise en cause. Pasteur ayant publié ses premiers succès , son vaccin antirabique devient vite célèbre et les candidats affluent .
Déçu par quelques cas où le vaccin a été inefficace , Pasteur croit pouvoir passer à un " traitement intensif " , qu' il présente à l' Académie des Sciences le 2 novembre 1886 .
En 1877 , Pasteur veut tester l' hypothèse selon laquelle le bacille du charbon ne causerait l' état morbide que de façon indirecte , en produisant un " ferment diastasique soluble " qui serait l' agent pathogène immédiat .
L' animal récepteur ne développe pas la maladie et Pasteur estime que cette expérience " écarte complètement l' hypothèse du ferment soluble " .
Dans une publication ultérieure , toujours en 1877 , Pasteur note toutefois que le sang filtré , s' il ne cause pas la maladie , rend les globules agglutinatifs , autant et même plus que dans la maladie , et envisage que ce soit l' effet d' une " diastase " formée par les bacilles .
En fait , les pasteuriens Roux et Yersin prouveront en 1888 ( dans le cas de la diphtérie ) que les microbes sécrètent bel et bien une substance ( la toxine ) qui est la cause directe et isolable de la maladie .
Il faut dire , à la décharge de Pasteur , que l' existence d' une toxine du charbon ne sera démontrée qu' en 1955 .
En 1880 , d' ailleurs , Pasteur accepte d' envisager , à titre d' hypothèse , le rôle d' une substance toxique .
En 1880 , le vétérinaire Henry Toussaint estime , à tort ou à raison , avoir immunisé des moutons contre le charbon par deux méthodes : en inoculant du sang charbonneux dont les microbes ont été éloignés par filtration , et en inoculant du sang charbonneux où les microbes ont été laissés , mais tués par chauffage .
Pasteur , qui voit ainsi Toussaint , " à son insu , peut-être , car il n' y fait aucune allusion " , battre en brèche les opinions publiées antérieurement par Pasteur , rejette l' idée d' un vaccin qui ne contiendrait pas d' agents infectieux vivants .
Ici encore , André Pichot voit un effet de la tendance de Pasteur à cloisonner rigoureusement les domaines du vivant et de l' inanimé .
Pasteur , toutefois , finira par admettre la possibilité des " vaccins chimiques " .
Pour expliquer l' immunisation , Pasteur adopta tour à tour deux idées différentes .
En 1950 , René Dubos faisait gloire à Pasteur " d' audacieuses divinations " .
Il ajoute que la science de Pasteur " consiste moins à découvrir qu' à enchaîner " .
Ils comportent assez peu d' éléments originaux ( En note : Cela peut surprendre , mais les études sur la dissymétrie moléculaire étaient déjà bien avancées quand Pasteur s' y intéressa , celles sur les fermentations également ; les expériences sur la génération spontanée sont l' affinement de travaux dont le principe était vieux de plus d' un siècle ; la présence de germes dans les maladies infectieuses étudiées par Pasteur a souvent été mise en évidence par d' autres que lui ; quant à la vaccination , elle avait été inventée par Jenner à la fin du XVIII e siècle , et l' idée d' une prévention utilisant le principe de non-récidive de certaines maladies avait été proposée bien avant que Pasteur ne la réalisât .
) ; mais , le plus souvent , ils partent d' une situation très confuse , et le génie de Pasteur a toujours été de trouver , dans cette confusion initiale , un fil conducteur qu' il a suivi avec constance , patience et application. "
Pasteur n' était en rien un chercheur isolé dans sa tour d' ivoire .
" C' est sans doute à cela que Pasteur doit sa grande popularité .
En 1939 , Pasteur Vallery-Radot , petit-fils de Louis Pasteur , fit cette mise au point : " Mon père a toujours eu soin , et ma mère également d' ailleurs , de dire que Pasteur n' était pas pratiquant .
En 1994-1995 , Maurice Vallery-Radot , arrière-petit-neveu de Pasteur et catholique militant , ne se contente pas du spiritualisme , du théisme de Pasteur , il tient que Pasteur resta au fond catholique , même s' il n' allait pas régulièrement à la messe .
En 2004 , Pasteur sert de caution morale à une cause d' une nature différente : son précédent est évoqué à l' assemblée nationale en faveur de l' euthanasie compassionnelle .
Il existe 2 020 rues " Pasteur " en France .
Lors des grands mouvements de décolonisation , qui entraînèrent des renommages de rue , les rues Pasteur gardèrent souvent leur nom .
