Ce tabou , considéré par l' ethnologue Claude Lévi-Strauss comme un universel présent dans toute société , prend cependant différentes formes selon les formations sociales .
Ce texte , présenté par la députée Marie-Louise Fort ( UMP ) , prévoit l' inscription de la notion d' inceste dans le code pénal et dispose que les viols et agressions sont qualifiés d' incestueux lorsqu' ils sont commis " au sein de la famille sur la personne d' un mineur par un ascendant , un frère , une soeur ou par toute autre personne , y compris s' il s' agit d' un concubin d' un membre de la famille , ayant sur la victime une autorité de droit ou de fait " .
L' article 213 du code pénal Suisse ( livre deuxième , titre sixième ) condamne clairement l' inceste en ces termes :
Depuis l' Égypte pharaonique et encore récemment dans certains pays comme le Pérou pour la famille des Incas , il était fréquent , dans la noblesse , de se marier et d' avoir des enfants avec un membre plus ou moins éloigné de sa famille .
Cette tradition disparaît peu à peu : l' empereur actuel du Japon est le premier de sa dynastie à être marié à une femme ne faisant pas partie de sa famille .
Dans la Rome antique , la violation du serment de chasteté par les vestales était taxé d ' incestus et , considéré comme un crime inexpiable , généralement puni par la mort de la coupable , condamnée à être enterrée vivante .
Ainsi , bien que présenté comme une exigence du peuple de Rome , le remariage de l' empereur romain Claude avec sa nièce Agrippine la Jeune était clairement considéré comme incestueux .
Dans la Torah , la prohibition de l' inceste est longuement détaillée au chapitre 18 du Lévitique ( parasha A'harei ) .
L' inceste est traité dans le Talmud avec les deux autres interdits : l' idolâtrie et le meurtre .
Avec Freud , on inscrit l' interdiction de l' inceste directement dans l' ordre du désir et de la loi .
Claude Lévi-Strauss y voit l' articulation entre nature et culture , le fondement social .
Malgré l' interdit qui l' accompagne et que Freud croyait universel , l' inceste reste un phénomène non marginal .
