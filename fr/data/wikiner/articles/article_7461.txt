Les fullerènes ont été découverts en 1985 par Harold Kroto , Robert Curl et Richard Smalley , ce qui leur valut le prix Nobel de chimie en 1996 .
Pour cette raison , il est appelé " buckminsterfullerène " , en l' honneur de Buckminster Fuller qui a conçu le dôme géodésique , ou " footballène " .
C 60 fut nommé buckminsterfullerène en l' honneur de Richard Buckminster Fuller , un architecte renommé qui popularisa le dôme géodésique .
Le buckminsterfullerène ( code IUPAC : [ 5,6 ] fullerène ) est la plus petite molécule de fullerène dans laquelle deux pentagones ne partagent pas un côté ( ce qui peut être déstabilisateur ) .
Des chercheurs de l' Université Rice ont avancé la possibilité de l' existence d' un type de buckyballe formée d' atomes de bore au lieu du carbone habituel .
