À la suite de Heidegger , Derrida cherche à dépasser la métaphysique traditionnelle et ses résonances dans les autres disciplines ( motif de la déconstruction ) .
De 1935 à 1941 , il va à l' école maternelle et primaire d' El-Biar .
Les enfants sont obligés de manifester leur attachement au Maréchal de multiples manières .
Derrida connaît ainsi , durant sa jeunesse , une scolarité mouvementée .
Mais c' est aussi à cette époque qu' il découvre et lit des philosophes et écrivains comme Jean-Jacques Rousseau , Friedrich Nietzsche , André Gide et Albert Camus .
En 1949 , il vient en France pour étudier en classe de première supérieure au lycée Louis-le-Grand à Paris , où il se lie d' amitié avec Pierre Bourdieu , Michel Deguy ou Louis Marin .
Il entre -- après deux échecs -- à l' École normale supérieure en 1952 .
Il y fait la rencontre de Louis Althusser , qui exerce comme " caïman " .
Derrida milite dans des groupes d' extrême gauche non communiste .
Il suit les cours de Michel Foucault .
Reçu au concours d' agrégation de philosophie de 1956 , après un échec en 1955 , il part à l' université Harvard comme special auditor .
Il se marie en juin 1957 avec Marguerite Aucouturier , une psychanalyste qu' il a rencontrée en 1953 par l' intermédiaire de son frère qui étudiait avec lui à l' École normale .
Il effectue son service militaire de 1957 à 1959 ( en pleine guerre d' Algérie ) comme enseignant dans une école d' enfants de troupe près d' Alger .
Il rencontre souvent Pierre Bourdieu à Alger .
En 1959 , Derrida est affecté au lycée Montesquieu du Mans en classe de lettres supérieures et est invité à la première décade de Cerisy-la-Salle ( cycle de conférences auquel il sera invité quatre fois ) .
Il fait son premier voyage à Prague pour rendre visite à la famille de son épouse .
L' année suivante il devient assistant à la faculté des lettres de l' université de Paris .
Il enseigne à la Sorbonne jusqu' en 1964 ( " philosophie générale et logique " ) .
Il fréquente également Robert Antelme , Pierre Boulez , Jean Genet , Pierre Klossowski , Francis Ponge et Nathalie Sarraute .
En 1964 il est nommé maître-assistant d' histoire de la philosophie à l' Ecole normale supérieure sur recommandation d' Althusser et Jean Hyppolite .
Sa participation au colloque de Baltimore à l' Université Johns Hopkins marque le début de ses fréquents voyages aux États-Unis et de l' introduction de la nouvelle pensée française sur le continent américain .
Derrida rencontre à cette occasion Jacques Lacan et Paul de Man .
Il côtoie régulièrement Edmond Jabès , Gabriel Bounoure ou Maurice Blanchot et s' associe progressivement à Jean-Luc Nancy , Philippe Lacoue-Labarthe et Sarah Kofman .
Il est accueilli avec une grande hospitalité aux États-Unis , il enseigne dans des dizaines d' universités tandis que son travail se heurte en France à une opposition massive .
En 1971 , il revient en Algérie après neuf ans d' absence .
Il sera arrêté et brièvement emprisonné à Prague ( des agents des services tchèques ont dissimulé de la drogue dans ses bagages ) à la suite d' un séminaire clandestin .
C' est François Mitterrand qui le fera libérer .
L' une des traces les plus visibles dans son travail de ce que certains ont considéré comme sa " politisation " aura été la publication en 1993 de Spectres de Marx .
En 1984 , alors toujours maître-assistant , il devient directeur d' études à l' École des hautes études en sciences sociales .
En 1984 , un enfant naît de sa relation hors mariage avec Sylviane Agacinski .
En 1995 , Jacques Derrida est membre du comité de soutien à Lionel Jospin .
À partir de 2003 , Jacques Derrida souffre d' un cancer du pancréas et réduit considérablement ses conférences et ses déplacements .
Derrida a la réputation d' être un écrivain difficile , exigeant pour son lecteur , même pour des philosophes .
Sa remise en cause d' Husserl et plus largement de la philosophie occidentale le conduit à déconstruire l' approche phénoménologique : pour lui , l' écrit a longtemps été négligé au profit de la parole .
Derrida éprouve un cœur d' opacité au cœur du rationnel , identifié comme défaut nécessaire et originaire de présence , comme écart originaire .
Derrida déclarait avant sa mort au journal L' Humanité : " Je n' ai jamais fait de longs séjours aux États-Unis , le plus clair de mon temps ne se passe pas là-bas .
Cela dit , la réception de mon travail y a été effectivement plus généreuse , plus attentive , j' y ai rencontré moins de censure , de barrages , de conflits qu' en France .
Derrida bénéficie d' une reconnaissance qui va au-delà du monde universitaire .
Par exemple , le film de Woody Allen Deconstructing Harry ( en 1997 , traduit en français par Harry dans tous ses états ) est une référence directe aux travaux de cet auteur -- " référence " que Derrida jugera d' ailleurs pauvre et décevante au regard de la complexité de ce " concept " .
Derrida est un philosophe rejeté par la très grande partie de la tradition analytique .
Nombreux sont les philosophes qui se sont élevés contre le doctorat honoris causa que lui a décerné l' Université de Cambridge en 1992 , reprochant aux travaux de Derrida " leur inadéquation aux standards de clarté et de rigueur " .
Jacques Derrida est l' auteur de plus de 80 ouvrages .
Jacques Derrida a fait des apparitions dans deux films :
