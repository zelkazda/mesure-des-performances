Multiple Virtual Storage ( MVS ) est le système d' exploitation des gros ordinateurs ( " mainframes " ) d' IBM .
MVS a été lancé en 1974 par IBM .
Il est destiné aux grosses machines du constructeur IBM .
Comme son nom veut le faire comprendre ( " Multiple Virtual Storage " ) , il applique le principe de la mémoire virtuelle pour traiter différents travaux simultanément sur une machine comprenant un ou plusieurs processeurs ( jusqu' à 54 , sans doute davantage dans les années qui viennent ) .
MVS se veut universel , et gère des sous-systèmes aptes à répondre à des requêtes en mode interactif ( appelé aussi " dialogué " ) , qu' il s' agisse aussi bien de " temps partagé " ( TSO : l' ordinateur sert plusieurs utilisateurs -- informaticiens le plus souvent -- en même temps ) que de " transactionnel " , où l' utilisateur final , non forcément informaticien , dialogue avec des applications en rapport avec sa fonction dans l' entreprise ( moniteurs transactionnels CICS , IMS ) .
MVS reste cependant bien adapté au traitement par lots ( batch ) , pris en charge par le langage JCL .
Les caractéristiques du système MVS sont les suivantes :
