Charles Babbage a été le premier à énoncer le principe de l' ordinateur .
Il a étudié au Trinity College en 1810 et au Peterhouse .
Il obtint son diplôme à Cambridge en 1814 .
Son épouse meurt en 1827 , Babbage a alors 36 ans .
Babbage ne se limita pas seulement aux problèmes techniques , ses inventions vont du compteur de vitesse au pare-buffle que l' on place devant les locomotives pour écarter les animaux , il fut aussi le premier à comprendre que dans un tronc d' arbre la largueur d' un anneau dépend du temps qu' il a fait dans l' année .
Babbage fut aussi l' inventeur du prix unique du timbre poste indépendant de la destination de chaque lettre .
Babbage s' aperçoit que les tables de calculs mathématiques comportent beaucoup d' erreurs , responsables , entre autres , de beaucoup d' accidents de navigation .
Trois facteurs semblent avoir contribué à sa décision de concevoir un tel appareil : son aversion pour le désordre , sa connaissance des tables de logarithmes , et le travail déjà commencé dans ce domaine par Blaise Pascal ( avec la " Pascaline " ) et Gottfried Leibniz ( multiplicatrice ) .
Il s' adjoint l' aide d' une jeune femme , Ada Lovelace , brillante mathématicienne qui l' aide à concevoir les " diagrammes " pour faire fonctionner la machine .
Il présente un modèle de sa machine à différences à la Société Royale d' Astronomie en 1821 .
Deuxièmement , Babbage modifiait également la conception de son projet de façon constante .
Un roman de science-fiction ( steampunk ) de William Gibson et Bruce Sterling , La Machine à différences , est construit autour de l' uchronie : " Et si Charles Babbage avait réussi à construire ses machines à différences " .
Une avancée fondamentale en matière d' automatisation des calculs fut réalisée par Charles Babbage entre 1834 et 1836 .
La machine analytique de Babbage n' est toutefois pas le véritable ancêtre de l' ordinateur actuel , en ce sens qu' elle n' intègre pas la notion fondamentale de programme enregistré .
Babbage n' avait pas non plus compris l' intérêt de l' algèbre booléenne pour ses travaux , même si son inventeur George Boole lui était contemporain .
Par ailleurs , Babbage fut dans l' incapacité , malgré ses efforts , de réaliser sa machine car les techniques de l' époque ( roues dentées , leviers , tambours ) étaient insuffisantes .
Pendant son travail sur la machine analytique , Babbage se rendit compte qu' il pouvait simplifier les plans de sa machine à différences .
Babbage n' essaya jamais de la construire .
