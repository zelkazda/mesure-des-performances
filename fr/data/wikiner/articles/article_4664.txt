En mathématiques , la fonction de Riemann est une fonction analytique complexe qui est apparue essentiellement dans la théorie des nombres premiers .
Euler a calculé la valeur de la fonction pour les entiers positifs pairs en utilisant l' expression de sous forme de produit infini ; il en a déduit la formule :
Ramanujan a beaucoup travaillé sur ces séries et Apéry a démontré en 1979 que , qui vaut environ est irrationnel .
Le lien entre la fonction et les nombres premiers avait déjà été établi par Leonhard Euler avec la formule , valable pour :
Charles-Jean de La Vallée Poussin démontra que pour , on a
Dans sa thèse soutenue en 1914 , Georges Valiron a montré qu' il existait une infinité de valeurs de dans tout intervalle pour lesquelles on avait la minoration
Cette hypothèse , formulée dès 1859 par Bernhard Riemann , a de très grandes conséquences dans le comportement asymptotique de nombreuses fonctions arithmétiques qui se trouvent liées à .
