Il est également langue officielle à Singapour et au Sri Lanka .
Le tamoul appartient à la famille des langues dravidiennes .
Étant donné le faible taux d' alphabétisation qui régna en Inde durant plusieurs siècles , il existe une grande divergence entre la langue tamoule écrite ( centamil ) et la langue parlée ( koduntamil ) .
Le tamoul populaire est extrêmement variable d' un pays à l' autre , d' une région à l' autre , voire d' un village à l' autre : même un locuteur maîtrisant le tamoul littéraire , c' est-à-dire officiel , peut ne rien comprendre à cette langue , qui utilise en abondance des noms étrangers ( lugéj : bagage , de l' anglais luggage , zanti : gentil , du français … ) souvent adaptés à la prononciation de la langue .
Le tamil parlé en Inde est un tamil populaire , le tamil parlé au Sri Lanka étant plus littéraire .
Le tamoul , à l' origine dérivé du grantha , utilisait un système de numération décimal propre :
Le tamoul est une langue post-positionelle principalement SOV , et possède une syntaxe très stricte : Le tamoul compte trois temps : le passé , le présent et le futur .
Le tamoul possède dans son paradigme verbal deux flexions distinctes , positive et négative .
