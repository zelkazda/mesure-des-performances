L' endettement de ces pays préoccupe certains gouvernements qui doivent faire des efforts pour bénéficier des prêts du FMI ou de la Banque mondiale .
L' Argentine a d' ailleurs connu une grave crise et a suspendu le paiement de la dette .
Le Venezuela , le Mexique ,l' Argentine , la Colombie , et l' Équateur exportent davantage de pétrole qu' ils n' en importent .
L' exportation des minérais occupe une part non négligeable dans les économies de la Bolivie , du Chili , du Pérou et du Mexique .
