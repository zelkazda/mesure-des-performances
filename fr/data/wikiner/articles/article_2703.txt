Louis XI signe en 1482 le traité d' Arras avec Maximilien I er de Habsbourg par lequel lui sont cédés le duché de Bourgogne et la Picardie .
Fils de Charles VII et de Marie d' Anjou .
Le 24 juin 1436 , il épousa Marguerite d' Écosse , fille de Jacques Ier d' Écosse .
Il entra à Lyon et Vienne pour recevoir les serments de fidélité de leurs habitants .
En février-mai 1437 , il visita le Languedoc et mena seul la reconquête des places-fortes anglaises dans le Velay .
Accompagné de son père , il fit une entrée royale dans Paris , récemment conquise par le connétable de Richemont .
En mai 1439 , son père le nomma lieutenant général en Languedoc .
En décembre de la même année , il fut transféré en Poitou , cette fois sans vrai pouvoir de décision .
En février 1440 , après une entrevue avec Jean II d' Alençon , il rejoignit la Praguerie , révolte de grands seigneurs mécontents , comprenant également Dunois , le maréchal de La Fayette ou encore Georges de la Trémoille .
Cette rébellion du dauphin , menée depuis Niort , s' expliquait par l' absence de responsabilité où le maintenait son père -- celui-ci avait constaté les effets désastreux des apanages sur l' unité du domaine royal .
Charles VII lui accorda le gouvernement , mais refusa le reste .
Il mena l' armée royale lors de la bataille qui se déroula du 5 juin au 19 septembre devant Pontoise .
En 1443 , il fit campagne contre Jean IV d' Armagnac , grand vassal insoumis .
Il les conduisit en Suisse .
Le 26 août 1444 , il remporta la victoire de Pratteln , puis se dirigea contre Bâle où se tenait un concile où l' antipape Félix V avait été élu .
En récompense , il fut nommé protecteur du Comtat Venaissin le 26 mai 1445 .
Il était frustré de n' avoir retiré que le Dauphiné de la Praguerie .
Charles VII ne s' en laissa pas conter , et envoya Antoine de Chabannes à la tête d' une armée pour lui arracher le Dauphiné .
Il y fut bien reçu , et en octobre , Philippe le Bon lui rendit hommage .
La même année , Charles VII tomba malade .
En avril 1461 naquit de nouveau une fille , Anne , qui épousa Pierre de Beaujeu .
Le 22 juillet 1461 , Charles VII mourut à Mehun-sur-Yèvre .
Louis XI affecta l' indifférence , et il fut absent lors des funérailles royales à Saint-Denis .
Il se fit sacrer à Reims trois semaines après la mort du feu roi , avant d' entrer dans Paris le 30 août 1461 .
Philippe le Bon fut remarqué avec son escorte comptant pour la moitié du cortège , et comprenant une troupe en armes .
Le nouveau roi ne demeura pas longtemps à Paris .
Dès le 25 septembre , il s' installa à Tours , ville gagnée à sa cause .
Sa première action de monarque fut de profiter de la crise de succession en Aragon .
En effet , Alphonse le Magnanime était mort en 1458 .
Jean II , frère du défunt , disputait la couronne à son fils Charles de Viane .
Celui-ci fut retrouvé mort en septembre 1461 , ce qui déclencha une guerre civile entre Jean II et les villes , en particulier Barcelone .
Louis XI en prit tout bonnement possession .
Un mois après la naissance de sa fille Jeanne en 1464 il apprend que l' enfant est boiteuse ( elle fut d' une laideur proverbiale , petite , contrefaite , malingre ) et décide sur le champ de la marier à son lointain cousin Louis d' Orléans , fils du poète Charles d' Orléans , dans le but avoué que le mariage reste stérile et que s' éteigne cette branche capétienne rivale de la sienne mais celui-ci , lorsqu' il deviendra roi ( sous le nom de Louis XII ) , obtiendra l' annulation de son mariage .
À l' intérieur se forma , en mars 1465 , la ligue du Bien public .
Très comparable à la Praguerie , elle avait à sa tête Charles de Charolais ( Charles le téméraire ) , fils de Philippe le Bon , qui réclamait plus de pouvoir .
Cette cession , décidée au traité d' Arras de 1435 devait compenser l' assassinat de Jean sans Peur à Montereau , le 10 septembre 1419 .
Se joignirent à eux Jean II de Bourbon et Jean V d' Armagnac .
Il livra une grande bataille à Montlhéry , le 16 juillet 1465 , pleine de confusion et de sang et sans réel vainqueur , mais le siège de Paris fut brisé .
Il lâcha cependant le gouvernement de Normandie à son frère .
Louis XI s' y rendit en personne .
Au cours des pourparlers , Liège se rebella contre la tutelle bourguignonne .
Furieux , le Téméraire se retourna contre Louis XI .
Il dut également promettre de donner la Champagne en apanage à son frère .
Il fit emprisonner son conseiller , le cardinal La Balue , en 1469 et dénonça le traité en 1470 .
En 1472 , le Téméraire envahit de nouveau la Picardie .
En 1474 , Louis XI manœuvre contre René d' Anjou , dont il désire annexer le domaine angevin .
Louis XI se rend à Angers avec son armée , sous couvert d' une visite de courtoisie .
Louis XI installe aussitôt une garnison dans le château d' Angers et en confie le commandement à Guillaume de Cerisay .
Il lui cède l' Anjou sans combat et se tourna vers la Provence dont il était le souverain et qu' il rejoignit aussitôt .
René d' Anjou arriva à récolter 50000 écus pour la libération de sa fille .
En 1482 , il parvint cependant à récupérer la Picardie et la Bourgogne , par le traité d' Arras .
Par le jeu d' héritages , dont celui de René I er d' Anjou , il entra en possession du Maine et de la Provence .
Sa vie durant , Louis XI est un perpétuel malade : " brûlures d' estomac , crises de foie , goutte , congestion hémorroïdaire qui l' empêche de marcher , eczéma purulent " , selon Ivan Gobry qui -- pour son physique -- cite Basin : " Avec ses cuisses et ses jambes maigrichonnes , il n' avait , dès le premier abord , rien de beau ni d' agréable .
Thomas Basin entreprit de régler ses comptes , en 1473 , dans une biographie censée révéler " ses ruses , ses malices , ses perfidies , ses sottises , ses méfaits et ses cruautés " sous couvert d' objectivité .
Louis XI se fait inhumer dans la basilique Notre-Dame de Cléry , qu' il avait fait édifier vers 1467 .
