La pratique du vol sans moteur remonte , de fait , à l' entre deux-guerres et résulte des contraintes imposées à l' Allemagne de ne pas réarmer et de ne l' autoriser qu' à construire des machines sans moteur et de subir de sévères restrictions dans la fabrication d' avions monoplace .
Depuis , ce sport s' est répandu dans de nombreux pays , mais l' Allemagne est restée le leader mondial de la conception et la fabrication des meilleurs planeurs de performance .
En vol de thermique , le pilote recherche des colonnes d' air ascendantes qui résultent de l' échauffement du sol par le Soleil .
Toutefois , dans les Alpes du Sud , il est possible de trouver des thermiques toute l' année .
Dans le sud des États-Unis , lorsque le temps est ensoleillé en hiver , il y a des ascendances thermiques parfaitement exploitables en milieu de journée .
Celle-ci l' élèvera jusqu' à ce qu' il rencontre la base des nuages ( ou 500 pieds au-dessous aux USA ) , dans lesquels il ne pourra entrer pour des raisons légales ou bien à proximité de la couche d' inversion où la température cesse de décroître suivant l' adiabatique sèche .
Ce type de décollage a été illustré par le film " La grande vadrouille " .
L' origine de cette expression date du débarquement de l' opération Overlord et plus particulièrement dans l' opération Tonga , le 5 juin 1944 .
