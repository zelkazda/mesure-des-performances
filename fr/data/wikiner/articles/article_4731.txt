Il passa une majeure partie de sa vie à voyager entre la France et la Roumanie .
Eugène Ionesco est le fils d' un juriste roumain travaillant dans l' administration royale ; sa mère est la fille d' un ingénieur français ( chemins de fer ) qui a grandi en Roumanie .
En 1913 , la jeune famille émigre à Paris où le père veut passer un doctorat .
Quand , en 1916 , la Roumanie déclare la guerre à l' Allemagne et à l' Autriche , le père revient au pays , coupant rapidement tous les liens avec sa famille ; il demande le divorce et se remarie .
Eugène est placé dans un foyer d' enfants auquel il ne peut s' habituer .
Aussi , de 1917 à 1919 , sa sœur et lui sont confiés à une famille de paysans de La Chapelle-Anthenaise , un village proche de Laval ( Mayenne ) .
En 1926 , Ionesco se fâche avec son père , apparemment très autoritaire , et qui du reste n' a que du mépris pour l' intérêt évident que son fils porte à la littérature : il aurait voulu en faire un ingénieur .
Ionesco entretiendra une relation exécrable avec ce père opportuniste et tyrannique .
Ionesco n' acceptera jamais le manque d' amour et le rejet infligés par son père .
De retour à Paris en 1945 il exerce divers métiers avant de travailler comme correcteur dans une maison d' édition administrative .
En 1938 , Ionesco reçoit de l' institut de français à Bucarest une bourse pour se perfectionner en France , ce qui lui permet d' échapper à l' atmosphère étouffante d' une Roumanie nationaliste , qu' en tant qu' intellectuel plutôt à gauche , il supporte mal .
De Paris , il fournit des informations aux revues roumaines sur les évènements littéraires de la capitale .
Considéré comme roumain Ionesco doit passer le conseil de révision , mais n' est pas incorporé dans l' armée .
Le couple connaît alors une période de grande gêne financière ; Ionesco entre comme correcteur au service d' une maison parisienne d' édition juridique et y reste jusqu' en 1955 .
En 1952 il a l' idée de Victimes du devoir .
La même année voit la reprise de La Cantatrice chauve et de La Leçon .
1953 est l' année de la reconnaissance : Victimes du devoir est représentée pour la première fois , accompagnée d' une série de sept sketches , et reçoit un accueil favorable .
Ionesco est alors reconnu comme un auteur jouant spirituellement avec l' absurde et parvient presque à vivre de ses pièces .
La Cantatrice chauve et La Leçon reçoivent une nouvelle mise en scène au petit Théâtre de la Huchette à Paris ; elles figurent depuis lors sans interruption au programme de cette salle .
En automne 1957 paraît Rhinocéros , nouvelle pièce dans laquelle Ionesco manifeste son effroi devant l' éclatement contagieux du patriotisme chauvin et du racisme qui saisissait la France à l' occasion de la " Bataille d' Alger " ( hiver 1956 / 1957 ) où l' armée française voulait voir le tournant décisif de la guerre d' Algérie ( 1954 -- 1962 ) [ réf. nécessaire ] .
À l' automne 1958 , la pièce Rhinocéros reprend , avec de légères modifications , l' action et les personnages de la nouvelle et montre à nouveau l' inquiétude de l' auteur [ réf. nécessaire ] devant " la confiscation du pouvoir " par le général de Gaulle dont beaucoup de partisans espéraient qu' il établirait un régime autoritaire de droite .
La pièce est adaptée par Jean-Louis Barrault : pour Ionesco , c' est la consécration .
Comme la pièce touche en France des sujets trop délicats , c' est à Düsseldorf qu' elle est représentée pour la première fois en 1959 , et le public allemand y voit pour sa part une critique du nazisme -- interprétation qu' on se hâte de reprendre en France quand Rhinocéros est mis en scène en 1960 , à Paris , qui a retrouvé son calme .
Pour la première fois dans la même année , une de ses pièces , Rhinocéros est mise en scène dans son pays natal , la Roumanie .
Un peu malgré lui , Ionesco entrait maintenant dans le personnage de l' écrivain établi , invité à des conférences , comblé des prix et d' honneurs et accédait en 1970 à l' Académie française .
Après quoi Ionesco campe sur sa position d' auteur de théâtre reconnu , jouissant d' une gloire incontestée , et se tourne davantage vers d' autres genres , en particulier l' autobiographie .
Dans les années 1980 et 1990 , Ionesco , dont la santé est de plus en plus mauvaise , sombre dans la dépression .
" En disant que Beckett est le promoteur du théâtre de l' absurde , en cachant que c' était moi , les journalistes et les historiens littéraires amateurs commettent une désinformation dont je suis victime et qui est calculée. "
Dans son expression la plus simple , Ionesco est réduit à " l' auteur de La Cantatrice chauve " .
Rien de plus réducteur : le roman , les contes , les nouvelles , les journaux intimes , les pamphlets , les essais politiques et esthétiques de Ionesco ont été trop souvent mésestimés , voire occultés , peut-être à cause de la difficulté à les relier directement à la dramaturgie avant-gardiste de leur auteur .
La particularité de celui auquel Jacques Mauclair a décerné le titre d ' " enfant terrible de la littérature et de la vie parisienne " est certainement de résister farouchement à tout essai de démystification .
Ionesco devient auteur , ou plutôt un " anti-auteur " , présentant au public des " anti-pièces " qui s' écartent de l' horizon d' attente de celui-ci .
Ionesco est alors un personnage iconoclaste et avant-gardiste .
Ionesco est un de ces rares auteurs à avoir été reconnu de son vivant comme un " classique " .
Ses pièces ont en outre connu un succès populaire jamais démenti , qui les a conduites des petites salles du Quartier latin où il a fait ses débuts , aux grandes scènes parisiennes .
À Saint-Gall , en Suisse , Ionesco abandonne ainsi les mots pour une peinture naïve et chargée de symboles .
Le dernier visage de Ionesco est celui du mystique épris de philosophie orientale , passionné par la Kabbale , dans le sillage de son ami Mircea Eliade .
Ionesco reste parfaitement inégal à lui-même .
Eugène Ionesco est considéré , avec l' Irlandais Samuel Beckett , comme le père du théâtre de l' absurde , pour lequel il faut " sur un texte burlesque un jeu dramatique ; sur un texte dramatique , un jeu burlesque " .
Au-delà du ridicule des situations les plus banales , le théâtre de Ionesco représente de façon palpable la solitude de l' homme et l' insignifiance de son existence .
