En 2001 , le gouvernement norvégien a annoncé qu' à l' occasion du bicentenaire de la naissance du mathématicien norvégien Niels Henrik Abel ( 1802 ) serait créé un nouveau prix pour les mathématiciens .
Le prix est décerné par l' Académie norvégienne des sciences et des lettres .
Un classement national des lauréats n' a donc pas grand sens , si ce n' est pour remarquer que certains pays ( la France et les États-Unis ) réussissent à " récupérer " des mathématiciens de valeur , qui ont souvent fait toute leur éducation et le début de leur carrière à l' étranger .
