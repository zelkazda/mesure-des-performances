Le regroupement des loges en obédiences maçonniques , apparu pour la première fois en Angleterre en juin 1717 , marqua un tournant des débuts de la franc-maçonnerie dite " spéculative " .
Elle s' est longtemps heurtée , notamment en France , au fait qu' elle était un enjeu de pouvoir entre les adversaires et les partisans de la franc-maçonnerie .
Dans un siècle où les travaux de la paléontologie n' existaient pas encore , il fut tout naturel pour eux de placer cette origine à l' époque d' Adam ( le premier homme , selon la conception de l' époque ) , à celle de Noé ou , beaucoup plus fréquemment , à celle de la construction du temple de Salomon par l' architecte Hiram Abif .
Comme elle , la plupart des toutes premières loges maçonniques distinctes des corporations sont écossaises et créées sous le régime des statuts Schaw .
À la même époque en Angleterre , il n' existait déjà plus aucune organisation de maçons opératifs .
Des loges furent notamment fondées en Russie ( 1717 ) , en Belgique ( 1721 ) , en Espagne ( 1728 ) , en Italie ( 1733 ) , en Allemagne ( 1736 ) .
Elle s' appuiera sur les constitutions de Laurence Dermott ( Ahiman Rezon -- 1751 ) et inspirera à son tour un certain nombre de loges en dehors du Royaume-Uni , ainsi que dans les colonies d' Amérique du Nord .
Dans le même temps , l' empereur Napoléon I er imposait en France la réorganisation de la franc-maçonnerie autour du Grand Orient de France et d' une orientation plus proche de celle des " modernes " .
Cette organisation en loges et ordres fut largement copiée par la suite par un grand nombre d' organisations non-maçonniques , principalement au Royaume-Uni et aux États-Unis , comme les nombreuses sociétés amicales ou le B'nai B'rith .
Cette baisse d' effectifs a touché principalement la maçonnerie anglo-américaine dont les effectifs avaient presque doublé dans les dix années qui suivirent la Seconde Guerre mondiale avant de diminuer progressivement de plus de 60 % au cours des cinquante années suivantes .
Elle est ainsi très présente en Europe ( elle constitue l' essentiel de la maçonnerie européenne ) et en Amérique latine .
Au Canada , elle est assez marginale et elle est quasi-inexistante aux États-Unis , où les rares loges libérales sont principalement fréquentées par des européens résidents ou de passage .
Enfin , certaines tenues sont consacrées à des événements particuliers : ouverture de la loge en début d' année , initiation de profane , passage au grade de Compagnon , élévation au grade de Maître , banquet rituel -- parfois nommé banquet d' ordre -- aux solstices d' hivers et d' été , élections de fin d' année , etc .
Pour n' en présenter que dix parmi les plus connues et les plus souvent citées , on peut mentionner Benjamin Franklin , Voltaire , Frédéric II , Goethe , Mozart , George Washington , Jules Ferry , Théodore Roosevelt , Simon Bolivar ou le duc de Kent .
Certaines , comme le Grand Orient de France , reconnaissent les loges féminines et acceptent la présence de femmes dans leurs loges , mais ne les initient pas .
Bien que l' Amérique du Nord suive généralement l' Angleterre sur de nombreux points , c' est sur ce continent que se concentre aujourd'hui principalement la résistance à la reconnaissance des femmes francs-maçonnes .
Mozart , lui-même franc-maçon , dans son opéra : La Flûte enchantée , fait usage du symbolisme de la franc-maçonnerie .
C' est dans le même contexte qu' est apparu le célèbre Canular de Taxil .
Sous le pape Jean XXIII une tentative de compréhension du phénomène maçonnique est entreprise .
Dans les années 1970 , particulièrement en France , des tentatives de réconciliation entre l' église catholique et la franc-maçonnerie voient le jour .
Toutefois , le 26 novembre 1983 , une déclaration de la Congrégation pour la doctrine de la foi alors dirigée par Joseph Ratzinger ( devenu depuis le pape Benoît XVI ) réaffirme l' interdiction faite aux catholiques de rejoindre la maçonnerie sous toutes ses formes ou tendances .
Le 2 mars 2007 le Vatican redit son opposition aux francs-maçons .
Toutefois , de nombreux pays de traditions musulmanes comme le Maroc ou la Turquie n' ont pas intégré cette fatwa dans le cadre de leurs législations respectives .
Bien que quelques communistes célèbres aient pu être francs-maçons à une période de leur vie , notamment en France , les partis communistes ont à partir des années 1920 majoritairement condamné la franc-maçonnerie , considérée comme étant d' origine bourgeoise .
Pour Léon Trotski , l' appartenance à une loge maçonnique suppose la collaboration de classe , nécessairement contre-révolutionnaire , et donc incompatible avec le militantisme révolutionnaire : " La franc-maçonnerie est une plaie sur le corps du communisme français , qu' il faut brûler au fer rouge " .
Trotski demande à la direction du Parti communiste français de donner l' ordre à ses adhérents maçons de quitter leurs loges : " La dissimulation par quiconque de son appartenance à la franc-maçonnerie sera considérée comme une pénétration dans le parti d' un agent de l' ennemi et flétrira l' individu en cause d' une tache d' ignominie devant le prolétariat " .
Les archives du Reichssicherheitshauptamt ( RSHA , bureau du haut commandement des services de sécurité ) , démontrent que des persécutions de francs-maçons furent organisées .
C' est par exemple le cas en Belgique et en France au sujet des lois relatives à la laïcité ou à la contraception .
