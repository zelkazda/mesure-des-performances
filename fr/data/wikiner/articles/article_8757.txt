Toute planète dotée d' un champ magnétique ( la Terre , Jupiter , Saturne , Uranus et Neptune ) possède sa propre magnétosphère .
Mercure et Ganymède , une lune de Jupiter , en possèdent également une , mais ces magnétosphères sont trop faibles pour capturer le vent solaire ionisé .
Sur Mars , on a observé des anomalies magnétiques locales dans l' écorce planétaire , restes présumés d' un champ magnétique ancestral de nos jours disparu .
Les aurores , boréales dans l' hémisphère nord , australes dans l' Antarctique , se forment dans les zones aurorales nord et sud : la ionosphère .
