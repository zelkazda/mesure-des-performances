Roosevelt fut l' un des principaux acteurs de la Seconde Guerre mondiale et rompit avec l' isolationnisme traditionnel de son pays .
Franklin Delano Roosevelt est né le 30 janvier 1882 à Hyde Park , une localité de la vallée de l' Hudson située à environ 160 km au nord de New York .
Ses parents appartenaient à deux vieilles familles patriciennes de New York .
Franklin Roosevelt était fils unique ; il grandit sous l' influence d' une mère possessive et eut une enfance heureuse et solitaire .
Il passait souvent ses vacances dans la maison familiale de l' île de Campobello située au Canada .
Grâce à de nombreux voyages en Europe , Roosevelt se familiarisa avec les langues allemande et française .
Il entra dans la fraternité Alpha Delta Phi et participa au journal étudiant The Harvard Crimson .
C' était le début de l' ère progressiste qui remodelait profondément le paysage politique américain , et c' est au sein du Parti démocrate qu' il entra en politique .
En 1902 , Franklin Roosevelt entra à l' école de droit de l' université Columbia mais abandonna son cursus en 1907 sans diplôme .
Franklin Roosevelt épousa Eleanor le 17 mars 1905 à New York , malgré l' opposition de sa mère .
Franklin Roosevelt eut plusieurs aventures amoureuses pendant son mariage : il entretint dès 1914 une liaison avec la secrétaire de son épouse , Lucy Page Mercer Rutherfurd .
Les quatre fils ont participé à la Seconde Guerre mondiale comme officiers et ont été décorés pour leur bravoure au combat .
Roosevelt n' aimait pas particulièrement sa carrière juridique et ne termina pas ses études de droit commencées à l' Université de Columbia .
Il prit rapidement la tête d' un groupe parlementaire de réformistes qui s' opposait au clientélisme du Tammany Hall , la " machine " politique du Parti démocrate à New York .
Pendant la Première Guerre mondiale , Roosevelt fit preuve d' une attention particulière pour la marine et milita pour le développement des sous-marins .
Afin de parer aux attaques sous-marines allemandes contre les navires alliés , il proposa d' installer un barrage de mines en mer du Nord , entre la Norvège et l' Écosse .
En 1918 , il inspecta les équipements navals américains en Grande-Bretagne et se rendit sur le front en France .
Pendant sa visite , il rencontra Winston Churchill pour la première fois .
Après cet échec , il se retira de la politique et travailla à New York : il fut vice-président d' une société de vente par actions et directeur d' un cabinet d' avocats d' affaires .
En août 1921 , pendant ses vacances à l' île Campobello , Roosevelt contracta une maladie que l' on pensait être à l' époque la poliomyélite .
Roosevelt cacha la dégradation de son état de santé pour pouvoir être réélu .
Roosevelt prit bien soin de rester en relation avec le Parti démocrate et s' allia avec Alfred E. Smith , ancien gouverneur du New Jersey .
C' est à cette époque que Roosevelt commença à réunir une équipe de conseillers parmi lesquels Frances Perkins et Harry Hopkins , en prévision de son élection au poste de président .
Le principal point faible de son mandat fut la corruption de Tammany Hall à New York .
Roosevelt remplaça le catholique Alfred E. Smith à la tête du Parti démocrate de New York dès 1928 .
Ses adversaires à l' investiture , Albert Ritchie , le gouverneur du Maryland et W. H. Murray , celui de l' Oklahoma , étaient des personnalités locales et moins crédibles .
Roosevelt resta confronté à l' hostilité affichée du président du parti , John Raskob , mais reçut le soutien financier de William Randolph Hearst , de Joseph P. Kennedy , de William G. McAdoo et d' Henry Morgenthau .
En 1932 , Roosevelt avait récupéré physiquement de sa maladie , si ce n' est l' usage de ses jambes , et il n' hésita pas à se lancer dans une épuisante campagne électorale .
Dans ses nombreux discours électoraux , Roosevelt s' attaqua aux échecs du président sortant Herbert Hoover et dénonça son incapacité à sortir le pays de la crise .
Le programme de Roosevelt n' obéissait à aucune idéologie , bien qu' il fut d' inspiration social-démocrate et keynésienne , et n' était pas précis quant aux moyens qui devraient être mis en œuvre pour aider les Américains les plus pauvres , , , .
La campagne de Roosevelt fut un succès pour plusieurs raisons .
Tout d' abord le candidat fit preuve de pédagogie et sut convaincre les Américains par ses talents d' orateur .
Il ne faut pas négliger non plus le rôle des conseillers du gouverneur qu' il fut , tels Raymond Moley , Rexford Tugwell , Adolf Berle , tous les trois chercheurs et universitaires , généralement de Columbia , pressentis par Samuel Rosenman le rédacteur des discours de Roosevelt .
Le 15 février 1933 , Roosevelt échappa à un attentat alors qu' il prononçait un discours impromptu depuis l' arrière de sa voiture décapotable à Bayfront Park à Miami en Floride .
L' auteur des coups de feu était Joseph Zangara , un anarchiste d' origine italienne dont les motivations étaient d' ordre personnel .
Il fut condamné à 80 ans de réclusion , puis à la peine de mort , car le maire de Chicago Anton Cermak mourut des blessures reçues pendant l' attentat .
Lors de son discours inaugural , Roosevelt dénonça la responsabilité des banquiers et des financiers dans la crise ; il présenta son programme directement aux Américains par une série de discussions radiophoniques connues sous le nom de fireside chats ( " causeries au coin du feu " ) .
Le premier cabinet de l' administration Roosevelt comprenait une femme pour la première fois de l' histoire politique américaine : il s' agissait de Frances Perkins , qui occupa le poste de Secrétaire au travail jusqu' en juin 1945 .
Au début de son mandat , Roosevelt prit de nombreuses mesures pour rassurer la population et redresser l' économie .
Pour expliquer ces succès politiques , les historiens invoquent également la capacité de séduction de Roosevelt et son habileté à utiliser les médias .
Comme son prédécesseur Herbert Hoover , Roosevelt considérait que la crise économique résultait d' un manque de confiance qui se traduisait par une baisse de la consommation et de l' investissement .
Au moment des faillites bancaires du 4 mars 1933 , son discours d' investiture , entendu à la radio par quelque deux millions d' Américains , comportait cette déclaration restée célèbre : " The only thing we have to fear is fear itself " ( " la seule chose que nous ayons à craindre , c' est la crainte elle-même " ) , , .
Il reprit également la Reconstruction Finance Corporation pour en faire une source majeure de financement des chemins de fer et de l' industrie .
Parmi les nouvelles agences les plus appréciées de Roosevelt figurait le " corps de préservation civile " , qui embaucha 250000 jeunes chômeurs dans différents projets locaux .
En outre , le 19 avril , les États-Unis abandonnaient l' étalon-or , ce qui eut pour effet de relancer l' économie .
Les réformes économiques furent entreprises grâce au National Industrial Recovery Act ( NIRA ) de 1933 .
Le NIRA établissait une planification économique , un salaire minimum et une baisse du temps de travail ramené à 36 heures hebdomadaires .
Le NIRA instaurait également plus de liberté pour les syndicats .
Roosevelt injecta d' énormes fonds publics dans l' économie : le NIRA dépensa ainsi 3,3 milliards de dollars par l' intermédiaire de la Public Works Administration sous la direction de Harold Ickes .
Roosevelt essaya de tenir ses engagements de campagne sur la réduction des dépenses publiques : mais il souleva l' opposition des vétérans de la Première Guerre mondiale en diminuant leurs pensions .
À la suite des rigueurs de l' hiver 1933-1934 , la Civil Works Administration fut fondée et employa jusqu' à 4,5 millions de personnes ; l' agence engagea des travailleurs pour des activités très diverses telles que des fouilles archéologiques ou la réalisation de peintures murales .
Le 28 mai 1934 , Roosevelt rencontra l' économiste anglais Keynes , entrevue qui se passa mal , ce dernier estimant que le président américain ne comprenait rien à l' économie .
Roosevelt nomma Jospeh P. Kennedy , le père de John F. Kennedy , comme premier président de la SEC .
Le Social Security Act prévoyait pour la première fois à l' échelon fédéral la mise en place d' une sécurité sociale pour les retraités , les pauvres et les malades .
Mais il suscita également l' opposition des démocrates les plus conservateurs emmenés par Al Smith .
Avec l' American Liberty League , ce dernier critiqua Roosevelt et le compara à Karl Marx et Lénine .
Elle décréta unanimement que le National Recovery Act ( NRA ) n' était pas constitutionnel car il donnait un pouvoir législatif au président .
Le monde des affaires se montra également hostile au " type de la Maison-Blanche " .
Enfin , Roosevelt était critiqué pour avoir creusé le déficit du budget fédéral , qui passa de 2,6 milliards de dollars en 1933 à 4,4 milliards de dollars en 1936 .
En 1937 , 7,7 millions d' Américains étaient au chômage soit 14 % de la population active , .
Cet ensemble électoral multi-ethnique , multi-religieux essentiellement urbain devint ensuite le réservoir de voix du Parti démocrate .
Roosevelt fut réélu pour un deuxième mandat .
Les propositions de Roosevelt furent ainsi rejetées .
Le deuxième mandat de Roosevelt a été marqué par la montée des oppositions .
Roosevelt ne réussit qu' à déstabiliser le démocrate conservateur de la ville de New York .
La politique menée par le président Franklin Roosevelt a changé le pays par des réformes et non par la révolution .
Roosevelt avait su instaurer un lien direct avec le peuple , par les nombreuses conférences de presse qu' il avait tenues , mais aussi par l' utilisation de la radio ( " causeries au coin du feu " ) et ses nombreux déplacements .
La politique étrangère de Roosevelt fit l' objet de nombreuses controverses .
Le 16 novembre 1933 , le gouvernement américain reconnut officiellement l' Union soviétique et établit des relations diplomatiques avec ce pays .
En 1936 , le droit d' intervention au Panama fut aboli , mettant au fin au protectorat américain sur ce pays .
Elle fut appliquée à la guerre entre l' Italie et l' Éthiopie , puis à la guerre civile en Espagne .
Le 5 octobre 1937 à Chicago , Roosevelt prononça un discours en faveur de la mise en quarantaine de tous les pays agresseurs qui seraient traités comme une menace pour la santé publique .
En décembre 1937 , au moment du massacre de Nankin en Chine , les avions japonais coulèrent la canonnière américaine Panay sur le Yang-tseu-Kiang .
Le président américain fit publiquement part de son indignation face aux persécutions antisémites en Allemagne .
À partir de 1938 , l' opinion américaine se rendit progressivement compte que la guerre était inévitable et que les États-Unis devraient y participer .
Roosevelt prépara dès lors le pays à la guerre , sans entrer directement dans le conflit .
Ainsi , il lança en secret la construction de sous-marins à long rayon d' action qui auraient pu bloquer l' expansionnisme du Japon .
Lorsque la Seconde Guerre mondiale se déclencha en septembre 1939 , Roosevelt rejeta la proposition de neutralité du pays et chercha des moyens pour aider les pays alliés d' Europe .
Le 4 novembre 1939 , Roosevelt obtint l' abrogation de l' embargo automatique sur les armes et les munitions .
Il commença aussi une correspondance secrète avec Winston Churchill pour déterminer le soutien américain au Royaume-Uni .
Roosevelt se tourna vers Harry Hopkins qui devint son conseiller en chef en temps de guerre .
Ils trouvèrent des solutions innovantes pour aider le Royaume-Uni comme par exemple l' envoi de moyens financiers à la fin de 1940 .
Contrairement à la Première Guerre mondiale , ces aides ne devaient pas être remboursées après la guerre .
Au mois de mai 1940 , l' Allemagne nazie envahit le Danemark , les Pays-Bas , la Belgique , le Luxembourg , et la France en laissant seul le Royaume-Uni face au danger d' une invasion allemande .
Très vite , on se mit d' accord pour agrandir l' enveloppe des dépenses pour l' aide aux pays attaqués en sachant que le pays risquait d' entrer en guerre contre l' Allemagne à cause de cette aide .
Roosevelt mit au pouvoir deux chefs républicains Henry L. Stimson et Frank Knox comme secrétaire de guerre et secrétaire de la Navy .
La chute de Paris choqua l' opinion américaine et le sentiment d' isolationnisme tomba .
Roosevelt usa de son charisme pour que le public fût favorable à une intervention militaire du pays .
Cet acte fut précurseur des aides massives qui suivirent en mars 1941 envers le Royaume-Uni , la Chine et la Russie .
Le 29 décembre 1940 , Roosevelt évoqua dans un discours radiodiffusé la conversion de l' économie américaine pour l' effort de guerre : le pays devait devenir " l' arsenal de la démocratie " .
Le 7 juillet 1941 , Washington envoya quelque 7000 marines en Islande pour empêcher une invasion allemande .
Le 11 septembre 1941 , Roosevelt ordonna à son aviation d' attaquer les navires de l' Axe surpris dans les eaux territoriales américaines .
Le 27 octobre 1941 , après le torpillage de deux navires de guerre américains par des sous-marins allemands , Roosevelt déclara que les États-Unis avaient été attaqués .
Les relations avec le Japon commençaient à se détériorer .
En mai 1941 , Washington accorda son soutien à la Chine par l' octroi d' un prêt-bail .
Le 11 décembre , l' Allemagne et l' Italie déclaraient la guerre aux États-Unis .
Avec la loi sur la conscription du 20 décembre 1941 , la mobilisation s' élargit à tous les Américains entre 20 et 40 ans .
La Déclaration des Nations unies du 1 er janvier 1942 prévoyait la création de l' ONU .
L' entrée en guerre des États-Unis marquait un tournant dans la mondialisation du conflit .
Cette théorie fut d' abord avancée par les officiers déchus par les commissions d' enquête : Husband Kimmel se dit victime d' un complot visant à cacher la responsabilité du gouvernement et de l' état-major .
Cette thèse fut ensuite reprise par les adversaires de Roosevelt et de sa politique extérieure .
Il est cependant difficile d' imaginer que Roosevelt ait laissé détruire autant de bâtiments de la marine juste pour engager son pays dans la guerre .
Par conséquent , rien ne permet d' affirmer que Roosevelt était au courant de l' attaque sur Pearl Harbor , , même s' il fait peu de doute qu' il a accumulé les actes contraires à la neutralité durant les années 1930 .
Si Roosevelt et son entourage étaient conscients des risques de guerre provoqués par la politique de soutien au Royaume-Uni , à l' URSS et à la Chine , il n' y a pas d' indication qu' il ait souhaité l' attaque de Pearl Harbor .
La tradition d' une limite maximale de deux mandats présidentiels était une règle non écrite mais bien ancrée depuis que George Washington déclina son troisième mandat en 1796 .
C' est ainsi que Ulysses S. Grant et Theodore Roosevelt furent attaqués pour avoir essayé d' obtenir un troisième mandat ( non consécutif ) de président .
Roosevelt se déplaça dans une convention de Chicago où il reçut un fervent support de son parti .
L' opposition à FDR était mal organisée malgré les efforts de James Farley .
Lors du meeting , Roosevelt expliqua qu' il ne se présenterait plus aux élections sauf s' il était plébiscité par les délégués du parti qui étaient libres de voter pour qui ils souhaitaient .
Le candidat républicain , Wendell Willkie , était un ancien membre du Parti démocrate qui avait auparavant soutenu Roosevelt .
Dans sa campagne électorale , Roosevelt mit en avant son expérience au pouvoir et son intention de tout faire pour que les États-Unis restent à l' écart de la guerre .
Si dans les institutions américaines , le président est le chef des armées , Roosevelt ne se passionnait pas pour les affaires strictement militaires .
Une agence unique de renseignements fut mise en place en 1942 qui fut remplacée par la CIA en 1947 .
Il autorisa le FBI à utiliser les écoutes téléphoniques pour démasquer les espions .
Enfin , Roosevelt s' intéressa au projet Manhattan pour fabriquer la bombe atomique .
En 1939 , il fut averti par une lettre d' Albert Einstein que l' Allemagne nazie travaillait sur un projet équivalent .
En août 1943 , à Québec , fut signé un accord anglo-américain de coopération atomique .
Mais ce fut son successeur Harry Truman qui prit l' initiative des bombardements nucléaires d' Hiroshima et Nagasaki , plusieurs mois après la mort de Roosevelt .
Pourtant , il considérait également qu' il ne pouvait intervenir directement dans les affaires internes de l' Allemagne .
Malgré la pression des Juifs américains , de sa femme et de l' opinion publique américaine , le président ne dévia pas de cette direction .
Il ne fut pas mis au courant des projets de bombardements d' Auschwitz ou des voies ferrées .
Roosevelt fut l' un des principaux acteurs des conférences inter-alliées et tenta d' y défendre les intérêts des États-Unis tout en faisant des compromis .
En 1942 , il donna la priorité au front européen tout en contenant l' avancée japonaise dans le Pacifique .
Son évaluation à sa juste mesure de l' énormité du danger hitlérien et de la nécessité d' empêcher l' URSS de sombrer justifiait certes ce choix .
Mais il dut néanmoins pour l' imposer surmonter les préférences post-isolationnistes de la majorité des Américains pour lesquels l' ennemi principal était le Japon .
À la Conférence d' Anfa ( Casablanca , janvier 1943 ) , Roosevelt obtint d' exiger la reddition sans condition des puissances de l' Axe .
Les 11 -- 24 août 1943 , Roosevelt et Churchill se rencontrèrent au Canada pour préparer le débarquement en France prévu au printemps 1944 .
Entre le 1 er et le 22 juillet 1944 , les représentants de 44 nations se réunirent à Bretton Woods et créèrent la Banque mondiale et le FMI ( Fonds monétaire international ) .
À la conférence de Dumbarton Oaks ( août-octobre 1944 ) , Roosevelt réussit à imposer un projet auquel il tenait beaucoup : les Nations Unies .
Ce fut à l' initiative de Roosevelt que se tint de la conférence de Yalta en février 1945 .
Au début de 1942 , il s' opposait à ce que la France libre participât aux Nations unies avant les élections en France .
La presse américaine était également favorable à la France libre .
Roosevelt ne reconnut le GPRF qu' en octobre 1944 .
La France ne fut pas invitée à la Conférence de Yalta .
Churchill insistait pour que la France fût en charge d' une zone d' occupation de l' Allemagne .
Sur le plan économique , Roosevelt prit des mesures contre l' inflation et pour l' effort de guerre .
La conversion de l' économie se fit rapidement : entre décembre 1941 et juin 1944 , les États-Unis produisirent 171257 avions et 1200 navires de guerre , ce qui entraîna la croissance du complexe militaro-industriel .
Cependant , les produits de consommation courante et d' alimentation furent insuffisants , sans que la situation fût aussi difficile qu' en Europe .
Après l' attaque de Pearl Harbor , le sentiment anti-japonais prit de l' ampleur .
Le 14 janvier 1942 , Roosevelt signa un décret de fichage des Américains d' origine italienne , allemande et japonaise soupçonnés d' intelligence avec l' Axe .
Il fut de nouveau opposé à un candidat républicain , Thomas Dewey , dont le programme n' était pas en contradiction totale avec la politique de Roosevelt .
Ce dernier , malgré son âge et sa fatigue , mena campagne en demandant aux Américains de ne pas changer de pilote au milieu du gué .
Roosevelt fut réélu pour un quatrième mandat avec une courte majorité de 53 % ( 25602505 voix ) mais plus de 80 % du vote du collège électoral ( 432 mandats ) .
Le 12 avril 1945 , il s' écroula se plaignant d' un terrible mal de tête alors qu' Elizabeth Shoumatoff était en train de peindre son portrait .
Le cercueil fut déposé à la Maison Blanche puis dans la maison familiale de Hyde Park .
Le président fut enterré au Franklin D. Roosevelt National Historic Site le 15 avril 1945 .
La mort de Roosevelt souleva une grande émotion dans le pays et à l' étranger .
Son état de santé avait été caché par son entourage et par les médecins de la Maison Blanche .
Roosevelt était président depuis plus de 12 ans , une longévité jamais égalée par aucun président américain .
En URSS , le drapeau soviétique fut bordé de noir et les dignitaires assistèrent à la cérémonie à l' ambassade .
Truman dédia la cérémonie du 8 mai 1945 à la mémoire de Roosevelt .
Les traits principaux du caractère de Roosevelt apparaissent dès l' époque de sa première campagne présidentielle : son optimisme , notamment face à la gravité de sa maladie puisqu' il avait la volonté de s' en remettre ; également son exigence vis-à-vis de lui-même comme de ses collaborateurs .
L' un de ses films préférés était Gabriel over the White House de Gregory La Cava ( 1933 ) qu' il se faisait projeter à la Maison Blanche .
Roosevelt était quelqu' un d' intuitif , de chaleureux et même charmeur , , , toujours souriant , et sachant désarmer les critiques par l' humour .
Roosevelt était doué pour la communication et même capable d' éloquence , moins en meeting qu' en petits comités d' où l' incontestable succès de ses causeries " au coin du feu " ( fireside chat ) dans lesquelles il s' adressait de façon simple et directe aux Américains , .
En 1939 , Roosevelt devint le premier président à apparaître à la télévision .
Il se souciait réellement des Américains les plus défavorisés et était sensible aux injustices et à l' oppression sous toutes ses formes .
Mais Roosevelt pouvait être également un politique hésitant , un tacticien manipulateur , capable de ne pas s' embarrasser de sentiments pour parvenir à ses fins , souvent secret , égoïste et attaché à son indépendance .
Franklin Roosevelt avait le souci de l' opinion publique : il s' intéressa d' ailleurs aux sondages de l' Institut Gallup .
L' héritage de Roosevelt a été considérable sur la vie politique américaine : il consacra la fin de l' isolationnisme , la défense des libertés et le statut de superpuissance des États-Unis .
Eleanor Roosevelt continua d' exercer son influence dans la politique américaine et dans les affaires mondiales : elle participa à la conférence de San Francisco et défendit ardemment les droits civiques .
De nombreux membres de l' administration Roosevelt poursuivirent une carrière politique auprès de Truman , Kennedy et Johnson .
Truman essaya de marcher dans les pas de son prédécesseur en lançant le Fair Deal .
La maison natale de Roosevelt est classée site national historique et abrite la bibliothèque présidentielle .
La villa de vacances de Campobello Island est administré par le Canada et les États-Unis ( Roosevelt Campobello International Park ) .
Elle est accessible par le Franklin Delano Roosevelt Bridge .
Le Roosevelt Memorial se trouve à Washington D.C. , juste à côté du Jefferson Memorial .
Les plans furent dessinés par l' architecte Lawrence Halprin .
Les sculptures en bronze représentent les grands moments de la présidence , accompagnées de plusieurs extraits des discours de Roosevelt .
Le réservoir situé derrière le barrage Grand Coulee dans l' État de Washington est appelé lac Franklin D. Roosevelt , qui présida à l' achèvement de l' ouvrage .
À Paris , il existe une station de métro ( Franklin D. Roosevelt ) à son nom .
Roosevelt est l' un des présidents les plus représentés dans les œuvres de fiction américaine .
Dans Le Maître du Haut Château ( 1962 ) , Philip K. Dick imagine que Roosevelt meurt dans l' attentat de Miami en 1933 , événement qui constitue le point de divergence de son uchronie .
Le portrait de Franklin Roosevelt apparaît sur la pièce de 10 cents .
Monaco a émis plusieurs timbres d' hommage pendant la seconde moitié des années 1940 .
L' un d' eux représente Roosevelt devant sa collection de timbres-poste .
Roosevelt est un des dirigeants de la civilisation américaine dans le jeu Civilization IV , avec George Washington .
