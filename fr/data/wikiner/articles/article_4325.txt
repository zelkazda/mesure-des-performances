Cairns ( 98349 habitants ) est une ville ( city ) côtière du Queensland en Australie .
Mais Cairns est surtout une ville touristique de la côte est qui possède de nombreux atouts :
Le futur site de la ville fut aperçu pour la première fois par un européen par le capitaine Cook en 1770 et dans le siècle qui suivit plusieurs expéditions visitèrent la région et comprirent la possibilité de création d' un port à cet endroit .
Pendant la Seconde Guerre mondiale Cairns servit de point de départ pour des opérations militaires dans l' océan Pacifique .
Après la guerre , Cairns devint progressivement un centre touristique et l' ouverture en 1984 de l' aéroport international de Cairns ainsi que d' un centre des affaires fit connaitre la ville internationalement et en fit un lieu réputé de vacances ou de réunions d' affaires .
Cairns possède un climat tropical chaud en été avec des pluies de mousson survenant entre novembre et mars .
Cairns est également sujet à l' activité possible de cyclones pendant la saison des pluies entre novembre et mars .
En termes de mouvements internationaux et domestiques de passager , l' aéroport international de Cairns , est le sixième plus actif aéroport d' Australie .
Après Sydney et Brisbane , Cairns est la destination touristique la plus populaire en Australie .
L' aéroport international de Cairns est directement connecté à la Chine , à Hong Kong , au Japon , à la Papouasie-Nouvelle-Guinée , à la Nouvelle-Zélande , à Singapour et aux États-Unis .
