Les graffiti existent depuis des époques reculées , dont certains exemples remontent à la Grèce antique ainsi qu' à l' Empire romain et peut aller de simple marques de griffures à des peintures de murs élaborées .
On peut encore lire des graffiti âgés de deux millénaires à Pompeï car c' est l' un des rares sites qui soit suffisamment bien conservé .
Dans la cité d' Éphèse , on trouvait des graffiti publicitaires pour les prostituées , indiquant de manière graphique à combien de pas et pour combien d' argent on pouvait trouver des professionnelles de l' amour .
L' église de Moings en est un exemple .
Un musée du graffiti ancien existe à Marsilly .
Le graffiti urbain se développe souvent dans un contexte de tensions politiques : pendant les révolutions , sous l' occupation , ( le reichstag à Berlin couvert de graffiti par les troupes russes ) , pendant la guerre d' Algérie , en mai 1968 , sur le Mur de Berlin ou dans les régions où se posent des problèmes d' autonomie ( Bretagne des années 1970 , Irlande du Nord , etc. ) .
La simple affirmation d' une identité ( je me surnomme Taki , j' habite la 183 e rue " , mon nom parcourt la ville tous les jours , j' existe ) s' est doublé d' ambitions plastiques , qui se sont révélées être un autre moyen de se faire remarquer : ce n' est plus seulement le graffeur le plus actif ou celui qui prend le plus de risques qui obtient une forme de reconnaissance , mais aussi celui qui produit les œuvres les plus belles .
Des groupes ( appelés " posses " , " crews " , " squads " ou " gangs " ) , comme la ville de New York en a toujours connus , se forment et permettent aux graffeurs de s' unir pour exécuter des actions spectaculaires ( peindre plusieurs rames d' un train par exemple ) , pour ajouter un nom collectif à leur nom individuel mais aussi pour s' affronter entre groupes , de manière pacifique ou non .
En 1973 , le New York Magazine lance le concours du plus beau graffiti du métro .
Les deux pionniers les plus célèbres d' une conjonction entre break dance , rap , dee-jaying et graffiti sont Phase 2 et Fab Five Freddy .
À la fin des années 1970 , le graffiti a été sévèrement réprimé dans le métro de New York et a commencé à se diffuser sur les murs des boroughs défavorisés de la ville avant d' essaimer dans d' autres grandes villes américaines ( Los Angeles , Chicago , Philadelphie , Houston ) et dans diverses grandes villes européennes : Paris , Londres , Berlin , Amsterdam et Barcelone surtout .
Picasso y participe .
Les sérigraphies urbaines d' Ernest Pignon-Ernest interpellent le passant et lui demandent quelle est la place de l' homme ou de la poésie dans la cité moderne .
Outre la rue , les catacombes de Paris seront aussi à l' époque un lieu important du graffiti .
Paris attire de nombreux graffiteurs européens mais aussi américains .
En 1961 , le Mur de Berlin est construit .
La ville de Barcelone accueille pourtant une quantité extraordinaire de graffiti atypiques et créatifs qui mixent revendications sociales et politiques , graphisme underground et , dans une certaine mesure , culture hip-hop .
) , le marqueur et le stylo , la craie , la peinture au rouleau ou au pinceau , l' acide ( pour vitre ou pour métal ) auxquels on peut adjoindre , dans une définition élargie du graffiti , l' affiche ( voir : les sérigraphies de Antonio Gallego ) , les stickers , les moulages ( en résine ou en plâtre collés sur les murs ) et la mosaïque ( voir : Space Invader ) .
Certains artistes ont même peint le Concorde exposé au Musée Delta d' Athis-Mons , à l' aéroport d' Orly .
On recense depuis l' antiquité de nombreux exemples d' hommages à des défunts , sur leurs sépultures ( voir par exemples les tombes de certains artistes ou poètes au cimetière du Père-Lachaise à Paris ) ou dans d' autres lieux : le mur de la maison de Serge Gainsbourg , rue de Verneuil à Paris , était couvert de graffiti-hommages après le décès du chanteur .
Le graffiti " hip-hop " , ou " tag " , qui représente 90 % des graffiti aux États-Unis et sans doute autant dans la plupart des pays , est un cas complexe .
