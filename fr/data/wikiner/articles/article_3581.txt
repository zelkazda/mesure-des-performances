Entre temps , il se présente aux municipales de 1989 sur la liste de Gilles de Robien , qui emporte la mairie d' Amiens .
En 1992 , il s' implante politiquement en Essonne à la demande du président RPR du conseil général , Xavier Dugoin .
Élu le 28 mars 1993 député de la 9 e circonscription de l' Essonne , il est réélu le 1 er juin 1997 , puis le 17 juin 2002 avec 55,78 % des voix au second tour .
Secrétaire général adjoint chargé des élections de 2002 à 2005 à l' UMP , il est alors considéré dans son parti comme " villepiniste " .
Il préside la Communauté d' agglomération Sénart Val de Seine depuis 2003 .
Au sein du gouvernement , Georges Tron est chargé de préparer la réforme des retraites dans la fonction publique .
