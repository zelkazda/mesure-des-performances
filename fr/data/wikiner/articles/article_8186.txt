Il pose également des problèmes de MTU : PPPoE occupe 8 octets dans les trames Ethernet , abaissant de fait la taille maximum des paquets IP de 1500 octets à 1492 octets .
Une alternative à PPPoE réside dans PPTP , conçu par Microsoft , plus puissant mais plus lourd .
