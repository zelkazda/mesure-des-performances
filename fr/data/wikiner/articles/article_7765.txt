Chavannes-près-Renens
Chavannes-près-Renens partage aussi ses frontières avec Lausanne et Renens .
C' est également vers cette époque que plusieurs maraîchers , poussés par le développement de la capitale , mais aussi attirés par un microclimat bien établi , s' installent à Chavannes-près-Renens qui devient la deuxième région du légume de Suisse .
Chavannes-près-Renens est une commune qui bouge : proportionnellement à sa population , la commune possède le plus fort potentiel de développement de l' ensemble de l' agglomération .
En sa qualité de commune résolument tournée vers l' avenir , Chavannes-près-Renens s' est engagée depuis plusieurs années déjà à mettre en place une politique sociale et familiale active , notamment dans le développement de places d' accueil familial et extrafamilial pour les enfants .
Forte de multiples réalisations privées et publiques et de plusieurs projets à venir , Chavannes-près-Renens vise prioritairement à répondre aux besoins d' une société moderne et offrir ainsi à ses concitoyens la possibilité de concilier vie professionnelle et vie familiale dans un environnement de qualité , avec des structures accessibles à tous .
Les renseignements se trouvant sur cette page , ont été pris auprés de l' administration communale de Chavannes-près-Renens .
