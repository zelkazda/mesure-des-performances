Max Roustan , homme politique français , né le 29 septembre 1944 à Saint-Julien-les-Rosiers ( Gard ) , député-maire d' Alès .
Il est d' abord enseignant dans un établissement privé d' Alès , et apiculteur .
En 1989 , après diverses candidatures infructueuses , il sera élu troisième adjoint au maire d' Alès , dans le cadre d' une union avec la gauche non communiste , l' UDF , le RPR et quelques personnalités de la société civile .
Mis au banc de la majorité municipale de l' époque , il prend son indépendance avec une partie du groupe majoritaire et annonce son intention de briguer en juin 1995 le fauteuil de maire d' Alès avec une liste apolitique décidée à en finir avec le marasme de l' après mine .
Le 18 juin 1995 , contre toute attente , il devient maire d' Alès au prix d' une triangulaire avec 37 % des voix et à peine 177 d' avance sur la liste communiste , ce qui fut un vrai séisme dans cette ville à fort passé ouvrier et minier gérée par la gauche et les communistes depuis des décennies .
Il fait partie du groupe UMP .
