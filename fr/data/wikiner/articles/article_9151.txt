Arrivé à 25 ans en Amérique , Antoine Laumet ( 1658 -- 16 octobre 1730 ) change d' identité et devient le sieur Antoine de Lamothe-Cadillac .
Après avoir été gouverneur de la Louisiane , il rentre en France où il est nommé gouverneur de Castelsarrasin .
Son nom est donné à la célèbre marque automobile américaine en 1902 , à la suite des commémorations du bicentenaire de la fondation de Détroit .
Pour faire valoir ses droits sur la succession de son père en 1718 , il reconnaît avoir changé d' identité en s' installant en Amérique et il retrouve sa véritable identité d' Antoine Laumet ( ce changement d' identité ne lui a jamais été reproché par ses contemporains ) .
En revanche , les raisons de ce changement d' identité et de son départ en Amérique restent encore ignorées .
Antoine Laumet naît le 5 mars 1658 à Saint-Nicolas-de-la-Grave , dans cette partie de la Gascogne au nord de Toulouse qui deviendra le département de Tarn-et-Garonne à la Révolution française .
Aucun document ne permet de connaître la jeunesse d' Antoine Laumet .
Quoi qu' il en soit , à 25 ans , il semble qu' il se commette dans une histoire assez louche pour être obligé de quitter la France et de se forger une nouvelle identité .
Quatre hypothèses peuvent expliquer ce départ soudain : Il est certain qu' Antoine Laumet effectue la traversée par des voies détournées , aucune liste officielle d' embarquement maritime n' indiquant sa présence sur un navire en partance d' un port français .
Il prend alors le titre d' écuyer qui correspond au rang que peut avoir le cadet de la famille , puis le titre de sieur de Cadillac , conformément à la coutume gasconne qui veut que le cadet prenne la succession de l' aîné à son décès .
Il se forge ainsi une identité et une origine noble , tout en se préservant d' une éventuelle reconnaissance par quelqu' un qui l' aurait connu en France .
En 1689 , il est envoyé en expédition près de Boston .
Cadillac se retrouve en 1690 à Paris .
A son retour à Port Royal , il apprend que l' amiral anglais William Phips s' est emparé de la ville et a fait prisonnier sa femme , sa fille et son fils .
En 1691 , Cadillac rapatrie sa famille à Québec , mais leur navire est attaqué par un corsaire de Boston qui s' empare de tous leurs biens .
Cadillac est promu lieutenant en 1692 .
Cadillac donne une procuration à son épouse pour qu' elle puisse signer les contrats d' affaires et les actes notariés pendant son absence .
En 1695 , Cadillac part explorer la région des Grands Lacs et en dresse des cartes .
Il découvre alors le détroit reliant le lac Huron et le lac Érié et imagine y installer un nouveau fort pour rivaliser avec les anglais .
En 1696 , pour pallier les difficultés du commerce des fourrures , le roi ordonne la fermeture de tous les comptoirs de traite , dont Michillimakinac .
Cadillac rentre à Montréal .
Mais les notables canadiens s' opposent fortement à son projet de nouveau poste qui , selon eux , entraînerait la ruine de Québec et de Montréal .
Ce n' est qu' en 1699 qu' il obtient le soutien de Pontchartrain pour la fondation du nouveau poste que le roi autorise en 1700 , en en confiant le commandement à Cadillac .
En 1702 , Cadillac retourne à Québec pour solliciter le monopole du commerce des fourrures et le transferts des tribus amérindiennes vers le détroit .
Un incendie ravage le fort Pontchartrain en 1703 .
Cadillac est rappelé à Québec en 1704 pour répondre aux accusations de trafic d' alcool et de fourrures .
Ce dernier établit un véritable réquisitoire contre Cadillac en 1708 .
En 1709 , les troupes stationnées au détroit reçoivent l' ordre de regagner Montréal .
En 1710 , le roi nomme Cadillac gouverneur de Louisiane et lui ordonne de rejoindre son poste immédiatement par la vallée du Mississippi .
Cadillac n' obtempère pas .
Il procède à l' inventaire général du détroit puis , en 1711 , s' embarque avec sa famille pour la France .
À Paris , en 1712 , il convainc le financier toulousain Antoine Crozat d' investir en Louisiane .
En juin 1713 , la famille Cadillac arrive au fort Louis , en Louisiane , après une éprouvante traversée .
En 1714 , Crozat préconise la construction de postes le long du Mississippi alors que Cadillac désire fortifier l' embouchure du fleuve et développer le commerce avec les colonies espagnoles voisines .
En 1715 , Cadillac et son fils Joseph prospecte l' Illinois où ils découvrent une mine de cuivre .
La famille Cadillac rentre en France et , en 1717 , s' installe à La Rochelle .
Ils sont libérés en 1718 et Cadillac reçoit la croix de Saint-Louis en récompense de ses trente années de loyaux services .
Il effectue également de nombreux voyages à Paris pour faire reconnaître ses droits sur la concession du détroit .
Il prolonge ses séjours à Paris si bien qu' en 1721 , il donne à nouveau procuration générale à sa femme pour qu' elle puisse signer les actes notariés .
Antoine de Lamothe-Cadillac meurt le 16 octobre 1730 à Castelsarrasin , " vers la minuit " , à l' âge de 72 ans .
Il est enseveli dans une chapelle de l' église des Pères Carmes .
Les prévisions d' Antoine de Lamothe-Cadillac se concrétisent après son départ de la Nouvelle-France .
Ainsi , Jean Baptiste Le Moyne de Bienville fonde La Nouvelle-Orléans à l' embouchure du Mississippi en 1718 .
