L' Insee et la Poste lui attribuent le code 67 .
Exemples : les Basses-Pyrénées devenues en 1969 les Pyrénées-Atlantiques ou les Basses-Alpes , devenues en 1970 le département des Alpes-de-Haute-Provence .
Le département a été créé à la Révolution française , le 4 mars 1790 en application de la loi du 22 décembre 1789 , à partir de la moitié nord de la province d' Alsace ( Basse-Alsace ) .
Les limites du Bas-Rhin furent modifiées à de nombreuses reprises :
Le département du Bas-Rhin fait partie de la région Alsace .
Il est limitrophe des départements du Haut-Rhin au sud , des Vosges et de Meurthe-et-Moselle au sud-ouest , de la Moselle à l' ouest , ainsi que de l' Allemagne , à l' est le long du Rhin et au nord .
Ce tableau indique les principales communes du département du Bas-Rhin dont les résidences secondaires et occasionnelles dépassent 10 % des logements totaux .
Le siège du conseil général est situé à Strasbourg , dans un bâtiment construit par l' architecte Claude Vasconi .
L' Alsace tout comme la Moselle relèvent , dans certains domaines , d' un droit local particulier , principalement issu du droit allemand .
En effet , l' Alsace , la plus grande partie de la Moselle et une partie de la Meurthe sont restées des annexes de l' Empire allemand du 1871 à 1919 .
Que ce soit par ses établissements d' enseignements secondaires ou supérieurs , l' Alsace est une région d' étudiants très importante et très tournée vers l' international .
Strasbourg accueille à elle seule 75 % d' étudiants au sein de son université .
La Cathédrale de Strasbourg : la cathédrale de Strasbourg est un chef d' œuvre de l' art gothique .
Le Mont Sainte-Odile : le Mont Sainte-Odile est un haut lieu spirituel toujours vivant .
Le Musée alsacien : le musée Alsacien est un musée d' art et traditions populaires .
La cigogne est l' oiseau emblématique de l' Alsace .
Même s' il reste composé le plus souvent d' une coiffe noire et d' une jupe rouge , symboles de l' Alsace , il existe une multitude d' autres tenues qui varient selon les villages mais aussi selon le statut social de la personne .
