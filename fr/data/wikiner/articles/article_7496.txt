Il se borna à écrire que l' église de Lyon avait joui de ce privilège per annorum longa curriccula .
Au Concile de Clermont du 1 er décembre 1095 , le pape Urbain II confirma de nouveau les privilèges de Lyon et déclara que l' archevêque de Sens devait soumission et obéissance au primat .
