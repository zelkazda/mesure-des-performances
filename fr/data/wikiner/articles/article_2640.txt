C' est donc cette passion qui l' amènera à vouloir intégrer l' US Army , mais à son grand regret , il ne pourra participer à la guerre du Viêt Nam , sa vue étant jugée trop mauvaise .
Ce premier livre lui vaudra les honneurs du président américain Ronald Reagan qui dira de son œuvre " c' est le roman parfait ! "
Aujourd'hui , Tom Clancy est un auteur richissime , qui fait de chacun de ses livres un best-seller .
Il fut également co-fondateur de la société de jeux vidéo Red Storm Entertainment ( créateur de Splinter Cell , Rainbow Six ... ) revendue depuis à la société française Ubisoft .
Il est aussi propriétaire d' une équipe de baseball ( Baltimore Orioles ) et a tenté de racheté une équipe de football américain ( Minnesota Vikings ) avant de se raviser à cause de son divorce .
Dans Sans aucun remords , Jack fait une apparition très courte et sans importance pour l' intrigue , alors que son père joue un rôle important .
Dans Rainbow Six , Jack est mentionné , mais sans que son nom soit employé .
Le nom de Tom Clancy figure parmi les auteurs de cinq séries à ce jour , mais il ne participe pas à leur écriture .
Tom Clancy est également connu dans le monde du jeu vidéo de par sa participation à l' élaboration de divers scénarios pour des jeux comme Hawx , SSN , Splinter Cell , EndWar , Ghost Recon ou la série des Rainbow Six réalisé par le studio Red Storm Entertainment et commercialisés par Ubisoft qui a acquis Red Storm depuis .
