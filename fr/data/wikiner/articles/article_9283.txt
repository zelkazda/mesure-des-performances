Athalie est une tragédie de Jean Racine en cinq actes ( comportant respectivement 4 , 9 , 8 , 6 et 8 scènes ) et en alexandrins .
Elle a abandonné la religion juive en faveur du culte de Baal .
En fait , son petit-fils Joas a été sauvé par la femme du grand prêtre .
Contrairement à Esther , Athalie est une vraie tragédie en cinq actes .
Racine atteint avec Athalie la grandeur des tragédies grecques qu' il connaît très bien .
Athalie fut victime de l' opposition des moralistes lors de sa création .
