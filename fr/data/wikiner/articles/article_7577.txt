Ainsi une légende raconte que vers 220-240 , saint Trophime aurait été le premier prélat de la cité .
À la fin du moyen-âge , à la mort de l' archevêque d' Arles Philippe de Lévis ( 1475 ) , le pape Sixte IV réduit une seconde fois l' Archevêché d' Arles : il détache l' évêché d' Avignon de la province d' Arles .
