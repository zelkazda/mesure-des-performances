Harry Potter et la Coupe de feu est le quatrième tome de la série littéraire centrée sur le personnage d' Harry Potter créé par J. K. Rowling .
Le lecteur suit Harry à Poudlard , l' école de sorcellerie où il poursuit son apprentissage .
Il y retrouve Ron et Hermione , tous ses amis , ses professeurs mais aussi ses rivaux .
Poudlard doit élire son candidat ; Harry et ses amis , qui n' ont pas l' âge requis pour s' inscrire , suivent l' événement avec intérêt .
Les épreuves étant toutes aussi dangereuses les unes que les autres , Harry devra affronter bien plus que de simples sortilèges .
