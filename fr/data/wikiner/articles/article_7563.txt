Le chapitre comprenait deux archidiacres ( pour la Cornouaille et le Poher ) , un chantre , un trésorier , un théologal et douze chanoines .
Il était le moins riche de toute la Bretagne .
Le diocèse actuel fut établi , lors du concordat de 1801 , dans les limites du département du Finistère .
