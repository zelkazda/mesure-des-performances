Les fables de Krylov empruntent souvent leurs sujets à celles d' Ésope et de La Fontaine .
Il devint ensuite substitut à la magistrature de Perm , où il mourut , laissant une veuve avec deux jeunes enfants .
Le jeune Krylov fut très tôt attiré par la bibliothèque paternelle et apprit le français .
Sa mère tenta avec lui de recouvrer une pension d' orphelin en 1782 à Saint-Pétersbourg , mais finalement il se fit accepter comme fonctionnaire de la cour des comptes , poste dont il démissionna en 1788 , à la mort de sa mère .
Par la suite , il se lance dans l' écriture de livrets d' opéras-bouffe , inspirés d' auteurs français , à l' époque de la mort de sa mère qui lui laissait un petit frère , Léon , dont il se considéra toute sa vie , comme un véritable père .
En 1790 , il écrit et imprime une ode à la paix avec la Suisse .
Il parvint à attirer 170 abonnements , ainsi que l' attention de Karamzine avec qui il polémiqua .
Krylov était autodidacte , il parlait en plus du français , l' italien et savait jouer du violon .
De 1812 à 1841 , il obtient un poste à la bibliothèque impériale de Saint-Pétersbourg , ce qui lui permet d' assurer son quotidien .
En 1838 , on organise pour lui une grande réception jubilaire et l' empereur Nicolas Ier lui octroie une pension à vie .
