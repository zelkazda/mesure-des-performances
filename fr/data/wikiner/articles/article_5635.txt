On estime aujourd'hui le nombre de locuteurs réels du français à environ 200 millions , dans l' ensemble des pays membres de l' Organisation internationale de la Francophonie .
Pour d' autres , le français est la langue administrative , ou une deuxième ou troisième langue , comme en Afrique subsaharienne , dont la République démocratique du Congo , premier pays francophone du monde , au Grand-Duché de Luxembourg , au Maghreb et plus particulièrement en Algérie , qui se trouve être le pays où l' on parle le plus français après la France ( voir tableau en bas de page ) et cela malgré sa non-adhésion à l' Organisation internationale de la Francophonie .
Il existe d' autres pays , comme le Liban , où la langue française a un statut officiel à caractère administratif .
C' est le cas de la Suisse , du Luxembourg , de Monaco .
C' est d' ailleurs parfois cette Organisation internationale de la Francophonie qui se voit reprocher , à tort ou à raison , des pratiques " néo-coloniales " .
Le Québec se revendique de la francophonie , sans du tout rompre le lien de cousinage d' une culture québécoise .
Senghor a parlé aussi de négritude dans le contexte de la francophonie .
Il y a des citoyens américains qui parlent français en Louisiane .
À l' origine , le terme de francophonie a été utilisé de façon purement descriptive par des géographes dès 1880 , le mot ayant été " inventé " par Onésime Reclus ( 1837 -- 1916 ) .
C' est après la Seconde Guerre mondiale , à partir d' un numéro spécial de la revue Esprit ( 1962 ) , qu' une " conscience francophone " s' est développée .
Le terme a été particulièrement popularisé par Léopold Sédar Senghor .
L' ALENA , l' APEC sont des regroupements régionaux économiques comme l' Union européenne .
C' est une tentative de regroupement parmi d' autres , comme l' OPEP pour le pétrole .
Certains défenseurs de l' idée francophone comme Stelio Farandjis ont aussi vu dans la francophonie le creuset d' un dialogue des cultures allant jusqu' à créer une terminologie spécifique ( arabofrancophonie ) .
L' Unesco a établi un rapport mondial en 2005 .
L' Agence intergouvernementale de la francophonie a également établi un dossier en 2005 .
L' Organisation internationale de la Francophonie ( OIF ) est une institution fondée sur le partage d' une langue et de valeurs communes .
