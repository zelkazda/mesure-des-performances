Il poursuit ses études au Collège Louis-le-Grand à Paris et est admis en 1831 à l' École polytechnique dont il sort deux ans après comme ingénieur des tabacs .
Il travaillera d' abord au laboratoire de chimie de Gay-Lussac .
En 1839 , il présenta à l' Académie des sciences son premier mémoire sur les variations séculaires des orbites des planètes .
Urbain Le Verrier devient célèbre lorsque la planète , dont il a calculé les caractéristiques comme cause hypothétique des anomalies des mouvements d' Uranus , est effectivement observée , par l' astronome allemand Johann Galle à l' observatoire de Berlin , le 23 septembre 1846 .
On baptisera Neptune cette nouvelle planète , malgré la proposition qui fut faite de la baptiser Le Verrier .
La planète Uranus , découverte par William Herschel en 1781 , présentait en effet des irrégularités par rapport à l' orbite qu' elle aurait dû avoir suivant la loi de la gravitation universelle d' Isaac Newton .
Encouragé par François Arago , Le Verrier se lance en 1844 dans le calcul des caractéristiques de cette nouvelle planète ( masse , orbite , position actuelle ) , dont il communiquera les résultats à l' Académie des Sciences le 31 août 1846 .
Ils seront confirmés ( à peu de choses près ) par Johann Galle , qui observa le nouvel astre le jour même où il reçut en courrier sa position par Le Verrier .
Le Verrier vit le nouvel astre au bout de sa plume " .
Cette découverte sera le sujet de nombreuses polémiques à l' époque , puisque ces calculs ont été menés en même temps par John Adams mais sans qu' aucun d' eux ne connaisse les travaux de l' autre .
Les caractéristiques de la planète avaient été déterminées par Adams un an plus tôt mais n' avaient pas été publiées .
Pierre-Ossian Bonnet lui succédera en 1878 .
Plus tard , Le Verrier tenta de répéter le même exploit pour expliquer les perturbations de Mercure .
Ces prédictions se révéleront inexactes , et ces anomalies seront expliquées un demi-siècle plus tard par Albert Einstein avec la théorie de la relativité générale .
Le 14 novembre 1854 , une terrible tempête , survenant sans la moindre alerte , traverse l' Europe d' ouest en est , causant la perte de 41 navires dans la Mer Noire .
Le Verrier entreprend alors de mettre en place un réseau d' observatoires météorologiques sur le territoire français , destiné avant tout aux marins afin de les prévenir de l' arrivée des tempêtes .
Ce réseau regroupe 24 stations dont 13 reliées par télégraphe , puis s' étendra à 59 observatoires répartis sur l' ensemble de l' Europe en 1865 .
À la tête d' une commission qui porta son nom , il réforma l' enseignement de l' École polytechnique en introduisant plus de science appliquée .
Élu député de la Manche en 1849 , il deviendra sénateur et inspecteur général de l' enseignement supérieur pour les sciences à partir de janvier 1852 .
En 1852 , il est élu conseiller général du canton de Saint-Malo-de-la-Lande .
Il proposa de revoir à la baisse la distance Terre -- Soleil et la vitesse de la lumière .
