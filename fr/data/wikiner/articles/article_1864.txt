On peut se voir en utilisant le reflet à la surface de l' eau ( comme Narcisse ) ou dans une vitre ; dans ce cas la réflexion est partielle tandis qu' avec un miroir parfait la réflexion est totale .
De même dans les expériences d' optique ( interférences avec les miroirs de Fresnel , par exemple ) , on utilise des miroirs dits " face avant " , dans lesquels le verre est utilisé seulement comme support mécanique stable .
Cette propriété de focalisation est utilisée de nos jours dans les télescopes ainsi que pour le four solaire d' Odeillo .
Le psychanalyste Jacques Lacan définit le " stade du miroir " , qui est le moment où l' enfant a conscience que c' est lui-même qu' il voit dans un miroir .
C' est aussi le symbole d' une porte , d' une limite vers un autre monde , particulièrement mis en valeur dans Alice au pays des merveilles .
