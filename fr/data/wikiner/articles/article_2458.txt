Dans les années 1960 , cette technique est utilisée par des joueurs américains pour le jeu Diplomatie .
Le créateur de jeu doit alors maîtriser les techniques de programmation , le plus souvent le langage PHP , pour construire ce qui est désormais un véritable jeu en ligne .
Les termes employés sur les sites de ces jeux sont parfois jeu PHP , jeu par navigateur , ou bien simplement jeu en ligne .
