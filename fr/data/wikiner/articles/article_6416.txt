Second enfant et premier fils de Léopold III et de la reine Astrid .
Son enfance est marquée par la mort accidentelle de sa mère , alors qu' il n' a que cinq ans , puis par la Seconde Guerre mondiale , vécue à Bruxelles .
Juste après le Débarquement , la famille royale est emmenée par les nazis en Allemagne au château de Hirschstein , puis en Autriche , où elle fut libérée le 7 mai 1945 par les troupes américaines .
Commence alors la " question royale " qui voit la famille royale s' exiler en Suisse jusqu' en juillet 1950 .
Si Baudouin a toujours défendu l' unité de la Belgique , il ne put empêcher les querelles linguistiques et la création d' une frontière linguistique , de 3 régions et de 3 communautés .
Si la Belgique est une monarchie parlementaire où le roi ne peut exprimer publiquement d' opinion qu' avec l' accord du gouvernement , le roi Baudouin a eu une influence certaine sur les gouvernements qui se sont succédé pendant ses quarante-deux années de règne .
Le 30 juin 1960 , le monarque assiste à la transmission des pouvoirs à Léopoldville .
C' est seul que le roi Baudouin commence son règne .
En Belgique , ces années sont marquées par la question scolaire qui oppose les partisans de l' école catholique et ceux de l' enseignement public .
Au cours de son règne , le roi Baudouin a dénoncé le racisme et la xénophobie dans ses discours , et n' a jamais reçu en audience aucun représentant de l' extrême-droite .
Le 15 décembre 1960 , il épouse doña Fabiola de Mora y Aragón .
Le mariage est célébré en la cathédrale Saints-Michel-et-Gudule à Bruxelles et est retransmis à la télévision , une première pour un mariage royal en Belgique .
En 1976 , lors des célébrations des 25 ans de son règne , le roi Baudouin exprime le souhait de voir une fondation contribuant à l' amélioration des conditions de vie de la population : la Fondation Roi-Baudouin est donc créée à l' aide des fonds récoltés .
La reine Fabiola , très pieuse , souhaitant une messe de gloire et d' espérance , était habillée de blanc , couleur de la résurrection ( également couleur de deuil des reines catholiques ) .
Comme ses prédécesseurs , le roi Baudouin repose dans la crypte royale de l' église Notre-Dame de Laeken .
