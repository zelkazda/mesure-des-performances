Le protocole WAP ( en anglais : Wireless Application Protocol ou WAP ) est un protocole de communication qui permet d' accéder à Internet à partir d' un appareil de transmission sans fil , comme par exemple un téléphone portable ou un assistant personnel .
Le protocole est maintenu par le Open Mobile Alliance .
Afin d' atteindre cet objectif , une passerelle ( en anglais gateway ) est connectée au réseau mobile , routant les connexions WAP vers Internet ( la passerelle effectue également une compression des données envoyée vers le téléphone portable , afin de faciliter la transmission ) .
Grâce à cette passerelle , le client , c' est-à-dire dans ce cas le téléphone portable , se connecte à un serveur WAP , capable de lui envoyer des données au format WML , qui est le format spécifique du WAP , dérivé de HTML .
La version 2.0 du WAP ( destinée par exemple à l' UMTS ) marque l' abandon de WML au profit de XHTML .
Si on se réfère au modèle OSI , le WAP prend en charge 4 couches :
