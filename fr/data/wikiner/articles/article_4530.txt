Baron-sur-Odon fait partie du canton d' Évrecy .
Situé à 81 mètres d' altitude et voisin des communes d' Esquay-Notre-Dame et de Tourville-sur-Odon , 847 habitants ( population totale 2006 ) résident dans la commune de Baron-sur-Odon sur une superficie de 643 hectares ( soit 131,7 hab/km² ) .
La rivière l' Odon traverse Baron-sur-Odon .
L' aéroport le plus proche est Carpiquet à 5.4 km .
Quant au fer à cheval retourné , il symbolise la fuite de Guillaume le Conquérant avec son cheval qui aurait été ferré à l' envers afin de tromper ses poursuivants ( fuite de Valognes à Falaise en 1047 ) .
Les vaguelettes représente la rivière Odon .
Le village de Baron-sur-Odon est jumelée avec les villages de :
