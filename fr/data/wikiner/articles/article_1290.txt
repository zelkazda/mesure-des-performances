Le surréalisme est un mouvement artistique qu' André Breton définit dans le premier Manifeste du Surréalisme comme " automatisme psychique pur , par lequel on se propose d' exprimer , soit verbalement , soit par écrit , soit de toute autre manière , le fonctionnement réel de la pensée .
Plus sûrement , les œuvres littéraires d' Alfred Jarry , d' Arthur Rimbaud et de Lautréamont , et picturales de Gustave Moreau et Odilon Redon sont les sources séminales dans lesquelles puiseront les premiers surréalistes ( Louis Aragon , Breton , Paul Éluard , Philippe Soupault , Pierre Reverdy ) .
Dans l' esprit de Breton , l' analogie entre le rêveur et le poète , présente chez Baudelaire , est dépassée .
C' est dans une lettre de Guillaume Apollinaire à Paul Dermée , de mars 1917 , qu' apparaît pour la première fois le substantif " surréalisme " : " Tout bien examiné , je crois en effet qu' il vaut mieux adopter surréalisme que surnaturalisme que j' avais d' abord employé .
L' écrivain et collagiste E. L. T. Mesens fut l' ami de René Magritte , les poètes Paul Colinet , Louis Scutenaire et André Souris et plus tard Marcel Mariën appartiennent également au courant .
Ramón Gómez de la Serna définit ses rapprochements insolites , " greguerias " , comme " humour + métaphore " .
Il s' affirme dès 1924 avec un manifeste publié par Karel Teige , qui conçoit la poésie comme une création intégrale , donnant libre cours à l' imagination et au sens ludique .
Ses représentants les plus éminents furent Jaroslav Seifert et surtout Vítězslav Nezval , dont Soupault souligna l' audace des images et symboles .
Par l' écriture automatique , les surréalistes ont voulu donner une voix aux désirs profonds , refoulés par celle de la société , cette " violente et traîtresse maîtresse d' école " , selon le mot de Michel de Montaigne .
Le peintre Max Ernst , de son côté , découvre pour son art une méthode analogue à l' écriture automatique , méthode que déjà Léonard de Vinci avait esquissée .
Ils feront grands bruits dans la société québécoise avec la sortie en 1948 du Refus Global qui s' oppose à toute l' idéologie des autorités au pouvoir , qu' elles soient politiques ou religieuses .
Les automatistes se regroupent autour du peintre Paul-Émile Borduas et sont de toutes les formes artistiques .
Ces principes débouchent sur l' engagement politique : certains écrivains surréalistes adhèrent , temporairement , au Parti communiste français .
Aucun parti , cependant , ne répondait exactement aux aspirations des surréalistes , ce qui fut à l' origine des tensions avec le Parti communiste français .
André Breton n' a pas de mots assez forts pour flétrir " l' ignoble mot d ' engagement qui sue une servilité dont la poésie et l' art ont horreur. "
Dès 1930 , pourtant , Louis Aragon acceptait de soumettre son activité littéraire " à la discipline et au contrôle du parti communiste " .
