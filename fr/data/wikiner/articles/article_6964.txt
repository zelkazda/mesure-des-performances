Cet article présente un résumé de l' histoire de Washington ( District de Columbia ) , la capitale des États-Unis .
Le nom de Washington est utilisé à partir du 9 septembre 1791 , tandis que le nom officiel " District of Columbia " est implanté le 6 mai 1796 .
La ville de Georgetown reste une entité indépendante jusqu' au 21 février 1871 , jour où elle est annexée à la ville de Washington .
La destruction de la capitale des jeunes États-Unis doivent démoraliser l' ennemi .
Cela provoque la retraite du président James Madison dans les montagnes de Virginie .
Des témoins ont rapporté que l' incendie était visible depuis Baltimore .
L' occupation de Washington prend fin lorsque les troupes britanniques sont envoyées contre Baltimore .
C' est la Guerre de Sécession ( 1861-1865 ) qui lui donne sa légitimité de capitale fédérale .
Lorsque la guerre s' achève , Washington a gagné des habitants , mais aussi une place à part dans le cœur des Américains .
