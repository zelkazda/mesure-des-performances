Le Mexique est un pays d' Amérique du Nord , situé au sud des États-Unis et bordé au sud par le Guatemala et le Belize .
Les premiers hommes seraient arrivés par le Nord , en traversant le détroit de Béring .
La présence humaine attestée au Mexique remonte à environ 50000 ans [ réf. nécessaire ] .
À cette époque , les Aztèques étaient le groupe dominant dans la population locale .
Pour cette raison , les Aztèques n' opposèrent initialement que peu de résistance à l' avance des conquistadors , mais plus tard , ils marquèrent leur opposition lorsqu' ils se rendirent compte qu' ils n' étaient pas les messagers divins d' abord admirés .
Les Aztèques furent défaits en 1521 et leur capitale rasée .
Il s' agit de la période qui s' étend entre la défaite de l' empire aztèque et l' indépendance du Mexique , c' est-à-dire entre 1521 et 1821 .
L' actuel Mexique faisait partie de la vice-royauté de la Nouvelle-Espagne .
La frontière nordique de la Nouvelle-Espagne n' était d' ailleurs pas définie avec précision mais peut se lire au caractère hispanique des noms géographiques conservé de nos jours .
L' arrivée d' un gouvernement libéral en Espagne convainquit les créoles qui voulaient s' approprier les privilèges des espagnols de proclamer l' indépendance .
L' histoire du Mexique entre 1820 et 1848 pourrait presque se résumer à une succession de troubles qui suscitèrent les réactions des puissances européennes , inquiètes des dettes non remboursées .
A la suite de la guerre contre les États-Unis ( lire ci-dessous ) entre 1846 et 1848 , un apaisement se fit sentir .
Face à l' incapacité du gouvernement de Benito Juárez de payer les dettes mexicaines , les gouvernements français , espagnol et britannique envoyèrent une force expéditionnaire occuper le port de Veracruz .
Ce régime dura environ trente ans de 1876 à 1910 , bien que Díaz eût déclaré avant d' arriver au pouvoir qu' il était opposé aux réélections .
Díaz encouragea les investissements étrangers ; il s' appuya aussi sur les conseils de positivistes connus sous le nom de cientificos .
Porfirio Diaz inaugura le monument de l' indépendance et l' hémicycle a Benito Juárez et le 15 septembre 1910 ( centenaire de l' indépendance du Mexique ) tout le centre de Mexico est illuminé ainsi que la cathédrale .
Porfirio Díaz , au pouvoir depuis une trentaine d' années , voulait à nouveau se représenter à l' élection présidentielle de 1910 , mais Francisco Madero annonça aussi sa candidature .
Parmi les meneurs des mouvements armées se trouvaient Francisco Madero , Pascual Orozco , Francisco Villa et Emiliano Zapata .
En 1925 est fondée l' organisation qui deviendra le PRI , qui prend le pouvoir en 1929 et le gardera jusqu' en 2000 .
En 1938 , le président Lázaro Cárdenas nationalisa la production de pétrole en créant Pemex .
À la fin de la Guerre d' Espagne , le gouvernement mexicain offrit l' asile aux opposants à Franco .
L' aviation mexicaine ( escadrille 201 ) participa à la Guerre du Pacifique .
La campagne électorale de 1994 fut marquée par l' assassinat de Luis Donaldo Colosio .
D' une part , l' ALENA , accord de libre échange entre les trois pays d' Amérique du Nord , entra en vigueur le 1 er janvier ; d' autre part , une récession temporaire entraina une dévaluation du peso .
En outre , Ernesto Zedillo succéda à Carlos Salinas à la présidence .
Vicente Fox Quesada , membre du PAN ( Partido Acción Nacional ) , remporta l' élection présidentielle et devint le premier président n' appartenant pas au PRI ( Partido Revolucionario Institucional ) depuis plus de 70 ans .
En effet , Vicente Fox avait recueilli 43 % des voix , alors que Francisco Labastida ( PRI ) obtenait 37 % des suffrages et Cuauhtémoc Cárdenas 17 % ( Partido de la Revolución Democrática ) ; le PRI et le PRD sont membres de l' Internationale socialiste .
Dans le meme temps , les États-Unis refusent de régulariser la situation des quelques quatre millions de clandestins mexicains se trouvant sur leur territoire .
Ce désintéret soudain des États-Unis , remonte au brusque renforcement de la politique migratoire depuis les attentats du 11 septembre 2001 .
Des anomalies toutes simples comme des erreurs arithmétiques ( par exemple nombre de votants supérieur ou inférieur au nombre de bulletins déposés ) sont mises en avant ( les mêmes " erreurs " sont détectées en faveur du PRD ) .
