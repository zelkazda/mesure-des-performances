Stratford-upon-Avon est une ville du centre de l' Angleterre où William Shakespeare est né en 1564 .
La troupe d' acteurs " Royal Shakespeare Company " est originaire de la ville , et s' y produit encore régulièrement .
