Les deux rappeurs Joeystarr et Kool Shen revendiquent leurs origines banlieusardes du département 93 , c' est-à-dire , de jeunes ayant vécu et survécu dans un milieu difficile .
Ainsi , le groupe s' appelle à ses débuts : 93 NTM , identité représentative de leurs origines , .
Si NTM est connu avant 1998 pour son hostilité à l' égard de la police , des paroles virulentes , , et une bataille juridique avec les autorités françaises , il est aussi connu pour ses paroles ouvertement critiques sur le racisme et les inégalités de classe dans la société française , et qui portent un constat d' urgence sur l' état des banlieues , .
Dans ce groupe voulu " réaliste " ou " authentique " par ses auteurs , le contraste entre les styles est marqué : alors que Joeystarr a un flow relativement lent , des paroles agressives , des textes plutôt engagés , une voix profonde , résonnante et éraillée , Kool Shen a un flow plus lyrique , des paroles plus mélancoliques et poétiques .
Au total , six albums ont été publiés par Sony Music Entertainment sous le label Epic Records .
En 1998 , le groupe sort son dernier album avec des airs originaux sous le label NTM , alors que Joeystarr et Kool Shen ont lancé chacun leurs propres labels , faisant la promotion de nouveaux groupes et créant de nouvelles branches dans d' autres domaines tels que l' industrie du vêtement .
Malgré la déclaration de Kool Shen en 2004 : " on en a fini avec NTM en 98 " , le groupe se reforme le temps d' un succès d' une tournée nationale en 2008 .
À Bercy , les trois premiers concerts tournent à guichets fermés confirmant le succès de retrouvailles attendues par les fans .
Avant 1988 , le groupe fut composé principalement et initialement de JoeyStarr ( Didier Morville ) , de Kool Shen ( Bruno Lopes ) et de DJ S ( Franck Loyer ) , tous trois originaires du département de la Seine-Saint-Denis en banlieue parisienne .
DJ S et Kool Shen se connaissent depuis l' enfance .
Kool Shen apprend que le futur Joey Starr y était aussi , il va à sa rencontre dans la cité voisine et ils commencent tous deux à s' entraîner à la danse .
Joeystarr et Kool Shen se font ainsi d' abord connaître par la danse ( le breakdance pour Kool Shen et le smurf pour Joey Starr ) , puis et surtout le graffiti ( notamment sur la ligne 13 du métro parisien ) : ils évoluent alors dans un posse nommé NTM .
de Saint-Denis , ce qui donnera la fusion " 93 NTM " .
En fin de compte , les deux compères se tournent vers une autre discipline du hip-hop , après avoir vu leurs copains de l' époque le groupe Assassin en faire : le rap .
Joey n' a qu' une ambition , réussir : " Il faut savoir que nous avons commencé la musique avec cette ambition : soit on cartonne , soit c' est la honte " ; " On venait pour tout brûler , pas pour faire de l' entre-deux .
En 1990 ils font leur premier concert à l' élysée Montmartre ainsi qu' un concert mémorable avec le groupe Zebda en première partie .
Un an plus tard sort Authentik , leur premier album .
Après une tournée en France , NTM remplit le Zénith de Paris le 24 janvier 1992 , c' est dans un froid glacial qu' attendent des centaines de jeunes des cités de la Seine-Saint-Denis et d' autres fans venu de tout paris pour découvrir NTM sur scène pour un concert légendaire : NTM donne tout sur scène , le public est complètement déchainé , les 6200 personnes qui assistent au concert savent qu' ils vivent un moment historique pour le groupe .
L' album est une déception commerciale [ réf. nécessaire ] par rapport au précédent opus et n' aboutira que sur une mini tournée seulement 3 dates en France et une autre le 7 mai 1994 au Palais des Sports de Paris .
et Lucien .
Le 9 juin 1995 , NTM se retrouve au Zénith de Paris pour la deuxième fois de son histoire .
Devant le succès de sa tournée , NTM la prolonge même avec 3 concerts au Bataclan de Paris le 18 , 19 et 20 novembre 1995 .
Ces tubes , conduisent NTM à effectuer une tournée dans toute la France dont le Zénith de Paris le 25 novembre 1998 , qui bénéficie d' une deuxième date en raison de l' affluence importante .
La série de concert , s' achève le 18 décembre 1998 par l' ultime concert à Genève , qui sera le dernier de la carrière de NTM avant de se reformer dix ans plus tard .
Les rumeurs de séparation sont de plus en plus importantes mais Joey Starr et Kool Shen n' excluent pas , à l' époque , la réalisation d' un cinquième album .
et IV My People sortent l' album Le Clash , compilation , faisant suite à une série de maxis , contenant les morceaux du groupe remixés par chaque label .
Bien qu' en apparence les deux membres du groupe paraissaient proches , il n' en demeurait pas moins qu' hors scène ou caméra , Kool Shen et Joeystarr menaient leurs vies séparément .
Depuis 2001 , Joeystarr et Kool Shen poursuivent des carrières solo .
Kool Shen a sorti l' album Dernier round en 2004 avant de se retirer du rap ( en tant qu' interprète ) .
Aujourd'hui , Kool Shen s' occupe de produire et de dénicher des artistes " nouvelle génération " via sa structure IV My People .
Il n' abandonne cependant pas son label , IV My People .
JoeyStarr , lui , continue de manier le micro .
Son premier album en solo , Gare au jaguarr , est sorti le 16 octobre 2006 .
En 2007 , joeystarr s' offre deux concerts à l' Olympia de Paris le 17 et 18 février 2007 .
Malgré leur séparation , le groupe NTM doit encore honorer un album dans le contrat les liant à leur maison de disque .
Les deux rappeurs , malgré leur séparation , se vouent un grand respect mutuel , Joeystarr ayant déjà qualifié Kool Shen de meilleur rappeur français .
Le 13 mars 2008 , les deux rappeurs sont présents sur le plateau de l' émission Le Grand Journal de Michel Denisot diffusée sur Canal+ .
Pascal Obispo insiste et rend hommage aux deux rappeurs : " certains textes et paroles de NTM sont à placer au niveau de Léo Ferré " .
Quelques questions ont été posées lors de l' interview de Michel Denisot , malgré le caractère impromptu de leurs retrouvailles récentes , ils avouent avoir le sentiment " de s' être quittés , il y a à peine une heure " :
Malgré le fait historique que chaque sortie d' album se solde par une actualité sociale chargée dans le domaine des banlieues , NTM se défend de faire du rap politique , mais d' être plutôt , selon leurs dires , le haut-parleur et non les leaders pour une prise de conscience dans les banlieues .
Le 18 mars 2008 , Marc-Olivier Fogiel les accueille dans son émission T'empêches tout le monde de dormir et commence par parler d' un véritable engouement des fans pour ces retrouvailles : 45000 places vendues en l' espace de trois jours , depuis le 15 mars .
Mais , Joey précise : " rien n' est impossible , puisque Kool Shen voulait arrêter le rap en 2007 " .
Au terme de la discussion , Joey reconfirme sa position ( la même qu' il avait déclaré auparavant le 13 mars à Michel Denisot ) : " On ne fait pas Bercy pour gagner avant toute chose de l' argent , sinon on aurait préparé un packaging " : des tee-shirts , un album , etc .
Le 18 avril 2008 , prés d' un mois après , une tournée dans toute la France ( Marseille , Strasbourg , Bordeaux , Toulouse , Montpellier , Lyon , Lille , Saint Herblain ) est annoncée du 2 au 24 octobre 2008 .
Elle commencera à Genève et finira au Zénith de Nantes .
Deux dates supplémentaires au POPB de Bercy sont ajoutées : le 22 et le 23 septembre 2008 .
Le 15 juin 2008 , Kool Shen réitère le sérieux de son engagement : " Si on s' est remis pour faire Bercy , ce n' est pas pour blaguer .
Le 23 juin 2008 a lieu à l' Olympia un concert " privé " exceptionnel du groupe ou Joey déclarera finalement : " On n' est pas là pour régler des comptes avec le hip-hop français " .
Lord Kossity était l' invité surprise .
Et Sefyu jouait en première partie .
Malgré son énorme popularité en France , dans le reste de l' Europe et même jusqu' au Canada , NTM a été très critiqué pour la virulence de ses paroles .
C' est le début de l' affaire NTM .
Historiquement , l' adjectif " Suprême " a été ajouté devant les trois initiales " NTM " à la demande de leur maison de disque , qui pensait que le nom serait mal perçu à ses débuts à cause de sa signification trop insultante .
Dans une interview , Joeystarr confie une explication de l' adjectif , qui sous-entend l' excellence du groupe mais qui peut aussi contenir un sens d' auto-dérision , pour ne pas trop se prendre au sérieux .
NTM est l' abréviation des initiales de Nique Ta Mère .
L' expression Nique Ta Mère étant plus commune dans les banlieues , elle choque moins par son usage dans ce milieu là .
Ils ont débuté dans le hip hop dans les block-partys de Dee Nasty où ils côtoyaient notamment le groupe Assassin .
Ceux-ci ont pu influencer , du moins ont été appréciés par NTM : Lucien , Dee Nasty .
