Avec la ruée vers l' or de 1859 et l' argent ( 1879-1893 ) , un grand nombre de colons vint peupler la région de Denver .
D' autres se sont reconverties ou ont prospéré grâce à d' autres activités : Aspen ( station de ski ) , Telluride et Cripple Creek .
Il est bordé au nord par le Wyoming ; au nord-est par le Nebraska ; à l' est par le Kansas ; au sud par l' Oklahoma et le Nouveau-Mexique ; et à l' ouest par l' Utah .
Liste des principales villes du Colorado , avec le nombre d' habitants en 2005
Par la suite , seuls les démocrates Harry Truman , Lyndon Johnson et Barack Obama en 2008 y ont été élus à la majorité absolue .
Suite à l' installation du commandement spatial américain à Colorado Springs , des industries technologiques de pointe telles que Lockheed Martin se sont implantées dans la région .
En 2003 , le revenu par habitant était de 34 561 dollars ( 8 e rang des États-Unis ) .
Le Colorado a de nombreux atouts touristiques : bien desservi par l' aéroport de Denver , il attire par ses stations de ski :
