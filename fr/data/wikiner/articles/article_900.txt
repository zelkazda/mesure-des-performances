Le mètre fut officiellement défini pour la première fois le 26 mars 1791 par l' Académie des sciences comme étant la dix-millionième partie d' un quart de méridien terrestre , ou d' un quart de grand cercle passant par les pôles .
Il fut adopté par la France le 7 avril 1795 comme mesure de longueur officielle .
En juin 1792 , Jean-Baptiste Joseph Delambre fut chargé de mesurer la distance entre Dunkerque et Rodez pendant que Pierre Méchain mesura celle de Barcelone à Rodez .
Ils devaient se retrouver à Rodez .
En 1793 , à Montjuïc à Barcelone , Méchain détecta une incohérence entre les longueurs relevées et le relevé astronomique de la position des étoiles .
Cet écart , qui n' était en fait pas dû à une erreur de manipulation mais à l' incertitude des instruments utilisés , le plongea dans un profond trouble et il mit tout en œuvre pour éviter de devoir rendre compte de ses travaux à Paris .
En 1889 , le Bureau des poids et mesures redéfinit le mètre comme étant la distance entre deux points sur une barre d' un alliage de platine et d' iridium .
Cette barre est toujours conservée au Pavillon de Breteuil à Sèvres .
