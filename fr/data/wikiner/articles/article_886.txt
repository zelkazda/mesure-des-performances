La norme complète , de référence ISO 7498 est globalement intitulée " Modèle basique de référence pour l' interconnexion des systèmes ouverts ( OSI ) " et est composée de 4 parties :
Ce cas se présente en particulier dans les réseaux X.25 où le contrôle de flux est une option , négociée à l' ouverture de la connexion .
Cette section illustre quelques cas où une architecture réseau ne peut entrer complètement dans le cadre du modèle OSI .
On est donc obligé de superposer SSL sur TCP .
Les réseaux téléphoniques ( réseaux fixes RNIS et réseaux UMTS ) ont aussi un découpage en plan similaire .
Les 2 protocoles TCP et UDP ont dans leur en-tête une somme de contrôle pour la détection des erreurs .
Cela se remarque notamment au fait que lors de passage de IP version 4 à IP version 6 , il faut redéfinir la façon de calculer ces somme de contrôle alors que les protocoles eux-mêmes n' ont pas réellement changé .
