Fondé en mai 1954 par Hubert Beuve-Méry comme supplément au quotidien Le Monde .
En 2010 , Le Monde détient encore 51 % du Monde diplomatique mais ce dernier a une rédaction indépendante et est édité par une société distincte du Monde .
Le journal est couramment appelé " Le Diplo " .
Autrefois simple supplément du quotidien , le " Diplo " a acquis progressivement son autonomie .
Suite à l' accession à la direction du Monde de Jean-Marie Colombani , il en devient en 1996 une filiale à hauteur de 51 % .
Par ailleurs , le " Diplo " affirme préserver sa ligne éditoriale vis-à-vis des pressions des annonceurs en limitant la part de ses revenus générée par la publicité .
Depuis juillet 2002 , le siège du journal se trouve au n o 1 , avenue Stéphen-Pichon dans le 13 e arrondissement de Paris .
À rythme triennal , le " Diplo " publie également deux atlas , traitant respectivement de sujets d' ordre géopolitique et environnementaux .
Ignacio Ramonet , ancien directeur de la rédaction , a quitté son poste en même temps que Bernard Cassen , et a été remplacé par Serge Halimi .
L' édition anglophone est née en 1999 d' un partenariat avec The Guardian Weekly .
Le Monde diplomatique traite d' une grande variété de sujets :
Certains détracteurs -- Frédéric Encel notamment -- lui reprochent , par exemple , des positions qualifiées de " propalestiniennes " et d ' " antisémites " , ou encore des articles jugés favorables à Fidel Castro ou Hugo Chávez .
Mais à l' inverse , l' américain Edward Herman qualifie Le Monde diplomatique de " média dissident " et le considère comme " probablement le meilleur journal au monde " .
L' éditorial célèbre d' Ignacio Ramonet , publié en 1995 , a ainsi mis en circulation le terme de " pensée unique " pour critiquer le dogme néolibéral .
Ainsi , Ignacio Ramonet pouvait écrire :
La rédaction a pris une part active dans l' émergence , en France , du mouvement altermondialiste .
Ainsi , c' est à la suite de la parution d' un éditorial écrit par Ignacio Ramonet en décembre 1997 que fut créée l' association ATTAC .
Le journal a relayé des campagnes d' ATTAC ( par exemple contre les paradis fiscaux et le secret bancaire ) .
Le Monde diplomatique publie aussi régulièrement des articles critiquant l' oligarchie française ou l ' " hyperbourgeoisie " mondiale .
C' est dans son édition de février 2007 , dans un article de Frédéric Lordon , que l' idée d' un impôt innovant appelé SLAM est née .
Le journal défend Castro et Chavez , quitte à prêter le flanc à une critique l' accusant de complaisance excessive .
Le journal désapprouve les violations des droits de l' homme à Cuba , mais il les relativise ( par rapport à d' autres pays ) , les explique et les justifie par les pressions américaine et le " blocus " américain sur Cuba .
Philippe Val , rédacteur en chef de Charlie Hebdo , accusa la rédaction du Monde diplomatique , et Ignacio Ramonet en particulier , d' une amitié avec les dirigeants Fidel Castro et Hugo Chávez .
Bernard-Henri Lévy dénonce lui aussi une position qui serait , selon lui , modérée vis-à-vis du régime communiste de Fidel Castro à Cuba [ réf. nécessaire ] .
Au sujet de ces accusations , Ignacio Ramonet dénonce un " anticastrisme primaire " et répond en avril 2002 :
Il est par ailleurs arrivé à plusieurs reprises au Monde diplomatique de critiquer la politique cubaine .
Sans nuance quand il s' agit de la France ou des États-Unis , cette ligne éditoriale s' adapte aux autres conceptions du journal quand c' est nécessaire , notamment lorsqu' il voit dans l ' " impérialisme américain " ou " néo-libéral " la vraie cause profonde du problème .
Le journal a aussi donné une tribune à l' historienne Annie Lacroix-Riz qui critique l' interprétation de l' Holodomor .
Il ouvre régulièrement ses colonnes à des personnalités pro-palestiniennes , comme le journaliste Michel Warschawski , la cinéaste Simone Bitton , le médecin et ancien président de Médecins sans frontières Rony Brauman , le journaliste Uri Avnery et l' historien post-sioniste Ilan Pappé .
Le Monde diplomatique donne également la parole à plusieurs tendances de la gauche israélienne : Amram Mitzna ou Yossi Beilin du Parti travailliste israélien mais aussi à des intellectuels palestiniens : Edward Saïd , Mahmoud Darwich ou Fayçal Husseini .
Le reproche est fait au journal [ réf. nécessaire ] de partager les vues pro-palestiniennes d' un certain nombre de personnalités qui interviennent régulièrement dans ses colonnes , comme le journaliste Michel Warschawski , la cinéaste Simone Bitton , le médecin et ancien président de Médecins sans frontières Rony Brauman , le journaliste Uri Avnery et l' historien post-sioniste Ilan Pappé .
Au sujet du sionisme , Alain Finkielkraut a émis une critique virulente , écrivant que :
Alain Ménargues a dénoncé ce qu' il considère comme : Il a dit s' étonner qu' un mensuel qui se veut ouvert au débat comme le Monde diplomatique cède à ce qu' il juge être des " pressions injustifiées " .
-- une crise interne qui a provoqué un changement à la tête de la rédaction du Monde Diplomatique .
Fin 2005 , des désaccords apparaissent au sein de l' association ATTAC , recoupant ceux au sein du Monde diplomatique .
Les divergences entre Bernard Cassen , Jacques Nikonoff , Ignacio Ramonet et Maurice Lemoine d' une part , Dominique Vidal et Alain Gresh d' autre part , amènent ces derniers à démissionner en janvier 2006 de leur poste de directeurs de rédaction du Monde diplomatique , restant membres de la rédaction comme journalistes .
Le quotidien Libération estime que : Toujours selon Libération , les tensions viennent notamment de divergences sur la question de la laïcité et du voile , la position de Ignacio Ramonet au sujet du régime cubain et de désaccords au sujet des FARC colombiens .
En novembre et décembre 2003 , des annonces publicitaires pour IBM et pour Renault occupèrent deux pages complètes .
Dans les éditions de février et mars 2004 apparurent des annonces de Microsoft , pourfendeur du logiciel libre , alors même que le mensuel avait publié des articles favorables aux logiciels libres et qu' il les utilise pour son site internet .
Le site du journal a été l' un des premiers à utiliser SPIP .
