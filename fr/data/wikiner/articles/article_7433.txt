La place de la Bastille est une place de Paris , lieu symbolique de la Révolution française , où l' ancienne forteresse de la Bastille fut détruite , entre le 14 juillet 1789 et le 14 juillet 1790 .
Une bastille , fortification placée sur les remparts de Paris , est construite de 1370 à 1383 sous Charles V .
Cette tente est représentée sur une peinture à la gouache sur carton , pièce du musée Carnavalet , de Henri-Joseph Van Blarenberghe , ancien peintre militaire , qui a également peint des images de la prise de la bastille .
Palloy , l' entrepreneur de travaux qui avait démoli la forteresse , fournit la première pierre , mais la construction en reste là .
Les citoyens réclamèrent son déplacement à la place du Trône-Renversé ( actuelle place de la Nation ) .
Le nombre de personnes guillotinées sur la place de la Bastille est de 75 .
L' architecte Jean-Antoine Alavoine commença les travaux en 1833 , mais seule une maquette en plâtre grandeur nature due au sculpteur Pierre-Charles Bridan fut élevée .
Louis-Philippe décida en 1833 de construire la Colonne de Juillet , déjà prévue en 1792 , mais pour , cette fois , commémorer les Trois Glorieuses .
La place de la Bastille est le lieu régulier de différentes foires , concerts , et marchés .
La place de la Bastille est le point de départ , de passage ou d' arrivée de très nombreuses manifestations sociales , politiques ou syndicales .
