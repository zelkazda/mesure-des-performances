Guy Bedos , né le 15 juin 1934 à Alger est un humoriste , artiste de music-hall , acteur et scénariste français , pied noir .
En 1965 , il débute au music-hall au côté de Barbara , puis se lance dans une carrière d' humoriste en formant un duo avec Sophie Daumier qu' il épouse en 1965 .
Il a aussi joué dans des pièces de théâtre comme La Résistible Ascension d' Arturo Ui de Bertolt Brecht .
Il aurait également joué dans des pièces d' Arthur Miller .
Il contribue aussi régulièrement dans l' hebdomadaire satirique Siné Hebdo crée par le dessinateur Siné dont il a pris la défense lorsque celui-ci fut accusé d' antisémitisme .
