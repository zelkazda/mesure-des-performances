Black découvrit également que lorsque le dioxyde de carbone est introduit dans une solution calcaire ( hydroxyde de calcium ) , il en résulte un précipité de carbonate de calcium .
En 1781 , le chimiste français Antoine Lavoisier mit en évidence le fait que ce gaz est le produit de la combustion du carbone avec le dioxygène .
Le dioxyde de carbone fut liquéfié pour la première fois en 1823 par Humphry Davy et Michael Faraday .
Le dioxyde de carbone sous forme solide est également présent en abondance aux pôles de la planète Mars , où il forme de véritables calottes glaciaires .
500 scientifiques ont été conviés à Iéna , en Allemagne , du 13 au 19 septembre 2009 pour faire le point sur la connaissance , lors de la 8 e conférence internationale de recherche sur le dioxyde de carbone ( la première s' est tenue en 1981 ) .
