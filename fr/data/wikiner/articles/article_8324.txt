rectifie Christine .
Christine donne chez elle des leçons de violon .
Elle est bientôt enceinte et Antoine , qui a perdu son emploi de fleuriste à la suite d' une mauvaise manipulation chimique , entre dans une importante entreprise américaine qui effectue des recherches et expériences hydrauliques .
Christine remarque que les tulipes s' ouvrent les unes après les autres laissant tomber les mots sur la table .
Un soir , au restaurant , alors qu' il dîne avec elle , Antoine quitte la table à plusieurs reprises au cours du repas pour téléphoner à Christine .
Antoine retrouve Christine : le couple se reforme .
" Aux côtés de Jean-Pierre Léaud dont on vante beaucoup les mérites , il serait injuste de ne pas mettre sur le même plan Claude Jade , l' épouse ; elle est en réalité l' élément fort du couple et son interprétation le souligne clairement .
Claude Jade a l' autorité dans le charme , la fierté dans l' amour , toutes choses en somme qui ne vont pas toujours ensemble chez les personnages de comédie . "
