La même année , Andy Summers remplace Padovani .
Il connut son apogée en 1983 avec l' album Synchronicity .
Les titres phares les plus célèbres du groupe sont Message in a Bottle , Walking on the Moon , Every Breath You Take et Roxanne dont une adaptation sous forme de tango est présente dans la bande son du film Moulin Rouge ! de Baz Luhrmann .
The Police est classé numéro 70 dans la liste des 100 plus grands artistes de tous les temps du magazine Rolling Stone .
Il a détesté le groupe , mais adoré Sting et celui-ci veut justement s' installer à Londres .
L' histoire retiendra donc de ce groupe que c' est par son biais qu' Andy Summers rencontra Sting et Copeland .
Suite à cette rencontre , Andy Summers rejoint Police en juillet 1977 et le trio initial de Sting , Padovani et Copeland , passe à quatre .
Mais le courant ne passe guère entre Henry Padovani , proche dans l' esprit du mouvement punk et Andy Summers , musicien de studio appliqué , et leurs altercations sont fréquentes .
De retour à Londres , le groupe entre en studio avec le producteur John Cale , mais les choses se passent mal entre John Cale et Andy Summers et ravivent d' autant les tensions entre les deux guitaristes .
En 1978 , le premier album du groupe Outlandos d' Amour est lancé .
L' album Reggatta de Blanc sorti en 1979 obtiendra un fort succès dans de nombreux pays grâce aux singles Message in a Bottle ( leur premier numéro 1 britannique ) et Walking on the Moon .
En octobre 1979 , The Police amorce leur première tournée mondiale .
L' album Zenyattà Mondatta sorti le 3 octobre 1980 .
Sting devient une star importante en décidant de devenir acteur .
En 1981 , The Police s' envola pour l' île de Montserrat pour la création de leur quatrième album .
Des tensions commencent alors à apparaître entre les membres du groupe , en particulier entre Sting et Stewart Copeland .
L' album Ghost in the Machine sort le 2 octobre 1981 .
The Police prend une pause en 1982 , pendant que Sting poursuit sa carrière d' acteur .
The Police lance leur dernier album , Synchronicity en 1983 .
Il devint numéro 1 au Royaume-Uni ( où il débuta 1 e ) et aux États-Unis .
Il ne restera 1 e que 2 semaines au Royaume-Uni mais le restera pendant 17 semaines aux États-Unis .
L' album Synchronicity fut , à sa sortie , le seul à déclasser l' album Thriller de Michael Jackson .
Le groupe est accompagné de chanteuses que Sting souhaitait avoir avec eux .
L' apogée de la tournée se tiendra au Shea Stadium à New York .
Un concert est enregistré à Atlanta le 13 et le 14 novembre 1983 , au plus fort de la tournée .
Sting décide de quitter le groupe et démarre sa carrière solo avec l' album The Dream of the Blue Turtles .
Le 10 mars 2003 , The Police a été introduit au Rock and Roll Hall of Fame .
Cependant , début 2006 , des rumeurs circulent sur l' éventuelle reformation de The Police pour la commémoration du 30 e anniversaire du 1 er single du groupe et un projet de tournée mondiale .
Le film est mis en vente la même année ( novembre 2006 en France ) .
Ce double album rétrospectif compte 28 morceaux dont Roxanne , Message in a Bottle ou encore Walking on the Moon .
En Europe , le groupe est passé en Angleterre , en Italie , en Suisse , en France ( les 29 et 30 septembre 2007 au Stade de France ) , en Belgique , au Portugal , au Royaume-Uni , au Danemark et aux Pays-Bas .
La première partie de chaque concert est assurée par le groupe Fiction Plane dont le leader , Joe Sumner qui est le fils de Sting .
Lors des concerts au Stade de France , pour le dernier morceau , Sting a introduit sur scène Henry Padovani , premier guitariste du groupe .
En entrevue , Sting a annoncé que la tournée sera la dernière du groupe et qu' il n' y aura aucun album de The Police de prévu après .
Le groupe a joué son dernier concert le 7 août 2008 au Madison Square Garden .
