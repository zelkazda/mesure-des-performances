L' idée de Rome antique est inséparable de celle de la culture latine .
Mais les Romains sont parvenus à résoudre les difficultés internes nées de la conquête sous la République en transformant leurs institutions républicaines .
La Rome antique contribue grandement à l' élaboration du droit , des constitutions et des lois , de la guerre , de l' art et la littérature , de l' architecture et la technologie et des langues dans le monde occidental , et son histoire continue d' avoir une influence majeure sur le monde d' aujourd'hui .
Les premiers Romains sont organisés en divisions héréditaires appelées gentes ou " clans " .
Bien avant la date traditionnelle de la fondation de Rome , une communauté s' est fusionnée en confédération , la ville d' Albe-la-Longue ( Alba Longa ) constituant son point de rassemblement .
Néanmoins , après un certain temps , le siège de cette confédération se déplace à Rome .
Le site même de la ville , avec ses sept collines et un espace marécageux au bord du Tibre , dans la plaine du Latium , est propice aux échanges commerciaux .
Les premiers Romains se sont probablement installés sur la rive gauche du Tibre , à environ 20 km de l' embouchure du fleuve .
Le premier village indépendant se trouve sûrement sur le Palatin .
D' autres se sont formées sur le Quirinal , l' Esquilin , le Capitole et sur les collines du Caelius .
Très tôt , ces villages ont procédé à un synoecisme ( réunion de maisons ) pour former la ville de Rome .
Autour de cette période , il existe probablement des extensions vers le sud ainsi que le long de la rive gauche jusqu' à l' embouchure du Tibre .
Après de nombreuses aventures et des amours contrariées avec Didon , la reine de Carthage , il débarque dans le Latium où il fonde la ville de Lavinium .
Son fils Ascagne fonde Albe-la-Longue .
Cette légende permet de donner à Jules César et son héritier Auguste une origine divine puisqu' ils se présentent comme les descendants d' Ascagne , , .
Après Ascagne , douze rois se succèdent à Albe .
Le treizième , Numitor , est détrôné par son frère Amulius .
Mais le dieu Mars tombe amoureux d' elle et de leur union naissent des jumeaux , Romulus et Rémus .
La jeune vestale est emmurée vivante et ses fils sont exposés sur le Tibre ( selon Denys d' Halicarnasse de nombreuses versions existent , tout aussi bien sur le viol que sur la peine infligée ) .
Au cours de la bagarre , Romulus tue Rémus .
La Monarchie peut être divisée en deux périodes .
Rome s' engage dans plusieurs guerres de conquête .
Les Romains sont divisés en trois groupes ethniques .
Afin d' organiser la ville , ces familles patriciennes l' ont divisé en unités appelées curies , bien que selon la légende , cette organisation soit imputée au premier roi , Romulus .
Les premiers Romains s' expriment démocratiquement au travers d' une comitia ( " assemblée " ou " comice " ) .
Enfin , cette seconde période voit les seuls rois étrangers ayant régné sur Rome avec leurs successions basées sur l' hérédité .
Sans se pencher en détail sur le degré de véracité de ces légendes , il est très probable que de telles conquêtes aient bien eu lieu à la fin de la Monarchie .
Le premier roi étrusque de Rome , Tarquin l' Ancien , succède à Ancus Marcius .
Il a été suggéré que Rome a été envahi par les Étrusques , bien que cela reste improbable .
Leur vie quotidienne et leur système de gouvernement restent les mêmes , mais leurs villes perdent leur indépendance vis-à-vis de Rome .
Néanmoins , un certain nombre vient à Rome .
Comme Rome s' agrandit , de plus en plus de soldats sont nécessaires aux conquêtes .
Pour faire revenir les plébéiens dans l' armée , le roi Servius Tullius abolit l' ancien système qui organise les armées sur la base des curies et le remplace par un système basé sur la propriété terrienne .
Suivant la réorganisation de Servius Tullius , deux nouvelles unités sont créées .
Le philologue et comparatiste Georges Dumézil voit dans la succession des premiers rois un exemple des fonctions tripartites indo-européennes , : Romulus le fondateur et le pieux Numa Pompilius exercent la fonction souveraine , à la fois organisatrice et sacerdotale , Tullus Hostilius la fonction guerrière , Ancus Marcius la fonction productrice .
En effet , les Romains sont divisés en deux groupes , les patriciens et les plébéiens .
On ne peut donc en écrire l' histoire qu' à partir des récits historiques qu' en donnent les Romains eux-mêmes , récits souvent imprécis , parfois contradictoires , où la légende et la réécriture à des fins politiques se mêlent au souvenir des évènements les plus anciens .
Néanmoins , bien qu' il soit évident que la tradition enjolive les faits pour ne pas donner à Rome le mauvais rôle , il est aujourd'hui admis que la tradition romaine se base sur des faits historiques , même s' il est très difficile et souvent impossible de démêler le vrai du faux .
Les auteurs modernes remettent en cause tous les divers évènements obscurs narrés par les historiographes romains , notamment l' épisode de Porsenna .
La bataille du lac Régille , ainsi que l' échec de Porsenna , marque définitivement la fin du règne des Tarquins à Rome .
Rome et Véies s' opposent pour des motifs économiques .
Grâce à Fidènes , située en amont de Rome sur un gué du Tibre , Véies contrôle la Via Salaria et le trafic du sel dans la région , .
Le territoire romain double presque de taille et Rome prend l' ascendant dans l' alliance éternelle entre égaux conclue avec la Ligue latine , dominant les autres cités .
Ainsi , Rome n' a jamais été si forte et aucune cité latine ou étrusque ne semble pouvoir lui faire de l' ombre .
Si l' on en croit les traditions , Rome est totalement mise à sac , détruite et brûlée , seul le Capitole est épargné , défendu héroïquement .
Par la Lex Publilia Voleronis , les plébéiens s' organisent par tribu , , se rendant politiquement indépendants des patriciens .
La loi des Douze Tables est rédigée en deux fois .
La seconde commission de décemvirs tente de maintenir son pouvoir absolu , mais devant la sécession de la plèbe , retirée sur le Mont Sacré , ils doivent démissionner , et la loi est approuvée par le peuple romain , , .
La loi des Douze Tables constitue le premier corpus de lois romaines écrites .
Il s' agit de lois politiques , économiques et sociales , visant à partager le pouvoir suprême entre plébéiens et patriciens , à lutter contre l' accaparement par les patriciens des terres récemment annexées autour de Rome ( ager publicus ) , et à soulager la plèbe qui est écrasée de dettes : les mesures proposées sont le rétablissement du consulat , avec obligatoirement un élu plébéien parmi les deux consuls , l' interdiction d' occuper plus de 500 jugères sur l' ager publicus et la déduction du capital des intérêts déjà payés et l' étalement du remboursements des dettes sur trois ans , ainsi que la suppression du nexum .
Après les réformes de Camille , ils forment des légions d' environ 4 500 hommes , composées notamment des hastati , les jeunes citoyens ( iuniores ) bien entraînés en première ligne , des principes , eux aussi iuniores mais plus expérimentés , en deuxième ligne , et des triarii , les seniores , qui forment la dernière ligne et la réserve .
Pour les Romains , cet épisode est vécu comme une catastrophe nationale .
L' interpénétration des élites est si importante que l' on parle parfois d ' " état romano-campanien " , toujours est-il qu' un mécanisme essentiel des conquêtes à venir s' est mis en place : Rome s' appuie sur les aristocraties locales , ou sur une partie de ces aristocrates , pour étendre son territoire , en échange elle offre à ces aristocraties la stabilité politique et l' insertion valorisante dans un ensemble plus vaste , l' accès à une échelle supérieure .
Une tentative de la part des peuples latins d' acquérir leur indépendance de Rome est la principale cause de la guerre .
La fondation romaine de Frégelles à la frontière samnite et de graves tensions à Naples provoque une réaction hostile immédiate des Samnites .
Tarente fait appel au jeune roi d' Épire , Pyrrhus I er , pour ralentir la progression romaine .
Pyrrhus accepte et Rome s' allie à Carthage , pour protéger la Sicile des visées de Pyrrhus .
L' avancée de Pyrrhus est foudroyante .
Toutes les populations et toutes les cités italiennes étaient divisées avant que Rome ne s' impose , et cette dernière réussit à entretenir des différences entre chaque , traitant avec chaque ville et chaque peuple , sous des conditions différentes et pour des statuts différents .
Ce système de colonie latine , où les colons ne sont pas citoyens romains mais possèdent un certain nombre de droits au sein de la colonie quasi-indépendante de Rome , va perdurer longtemps , étant à l' avantage de Rome et des colons .
Ils sont le plus souvent liés à Rome par une alliance inégale , et dans tous les cas , ces alliés doivent fournir à Rome un certain nombre de troupes et de fournitures militaires , sans que Rome doive les payer , ce qui soulage les citoyens romains .
Les assemblées législatives , qui sont considérées comme l' incarnation du peuple de Rome , font les lois domestiques qui gouvernent le peuple .
Seul le peuple de Rome ( plébéiens et patriciens ) a le droit de conférer ces pouvoirs à un magistrat , , , , .
Carthage , ancienne colonie phénicienne a développé d' abord des comptoirs commerciaux , puis des points d' appui et des colonies dans toute la Méditerranée occidentale et notamment à l' ouest de la Sicile grâce à son esprit d' entreprise .
Rome se méfie des ambitions carthaginoises en Sicile .
C' est la cause de la première Guerre punique qui dure près de vingt-cinq ans .
Les Carthaginois prennent d' abord la ville de Messine , reprise par surprise par les Romains , déclenchant le début de la guerre .
Il s' ensuit vingt ans de guerres avec des fortunes diverses , les premières victoires sont romaines , plus les Carthaginois se reprennent et stoppent l' avancée romaine .
Finalement , Rome prend le contrôle des mers et la victoire navale devant les îles Égades contraint Carthage à signer une paix humiliante .
Après la première Guerre punique , Rome s' étend en Illyrie , après avoir vaincu les Ligures , les Insubres et réduit la Gaule cisalpine en province romaine .
De son côté , Carthage se lance à la conquête de l' Hispanie .
Là , il écrase par deux fois , à Trasimène et à Cannes , les armées romaines .
Celui-ci s' installe à Capoue .
Rome refuse de s' incliner , remporte plusieurs succès , à Nole , Syracuse avec Claudius Marcellus , puis en Hispanie et enfin en Afrique sous la direction de Scipion l' Africain .
Malgré de nombreuses révoltes , l' Hispanie reste romaine .
Pendant la deuxième Guerre punique , Philippe V de Macédoine s' allie à Hannibal Barca .
La Macédoine est divisée en quatre républiques dirigées par des marionnettes que Rome commande .
Rome écrase complètement une rébellion macédonienne et ne se retire pas de la région , formant la province romaine de Macédoine , établissant un pouvoir romain permanent sur la péninsule grecque .
Les petits fermiers font faillite et convergent alors vers Rome , grossissant les rangs des assemblées populaires , où leur statut économique leur permet , pour la plupart , de voter pour le candidat qui leur promet le meilleur avenir .
Cependant , la citoyenneté romaine n' est que très peu étendue et les rancœurs et motifs de révoltes s' accumulent contre le pouvoir central à Rome , aveugle .
L' organisation générale de l' Italie n' a pas évolué depuis près de deux siècles , alors que le territoire romain s' étend maintenant sur une grande partie du bassin méditerranéen .
Le nombre d' esclaves est démultiplié et leurs biens acquis par Rome .
L' esclavage devient le moteur de la société romaine après la deuxième Guerre punique , lorsque les riches Romains commencent à créer des grandes propriétés dans les provinces conquises .
Les Romains les plus riches envoient leurs enfants dans les écoles grecques .
Les rangs des citoyens petits propriétaires se sont éclaircis , surtout pendant la deuxième Guerre punique .
Le blé importé de Sicile concurrence celui des petits producteurs latins qui , ruinés , vendent leurs terres à bas prix aux grands propriétaires et s' en vont à Rome rejoindre la plèbe urbaine .
Rome devient une ville bigarrée rassemblant , à côté des citoyens romains , des Italiques , des Grecs , des affranchis de tous horizons .
Pourtant une tentative de réforme se dessine avec les Gracques , issus d' une grande famille noble .
Des Germains envahissent la Gaule et écrasent à plusieurs reprises les armées romaines .
La nouvelle armée permet à Rome et à Marius de triompher face à deux menaces .
Après les Gracques , vient le temps des ambitieux qui luttent pour le pouvoir .
Grâce à la réforme militaire et à ses victoires en Afrique et en Gaule , Marius domine la vie politique , associant les chefs du parti populaire à son pouvoir .
Rome réussit à endiguer la rébéllion en concédant le droit de cité aux alliés qui ne s' était pas revoltés et ensuite à tous ceux qui déposeraient les armes .
Par la suite Sylla parvient à vaincre les dernières cités irréductibles .
Pompée et Crassus font face aux rébellions et aux ennemis de Rome , avec succès .
Pompée s' allie alors à Crassus et à Jules César en pleine ascension politique .
Il peut alors se consacrer à son ambition suprême , la conquête du pouvoir à Rome .
Il sait qu' il peut compter sur la loyauté de ses légions et de soutiens politiques à Rome .
Il pratique une politique favorable aux pauvres : remise des dettes , lotissement des vétérans , grands travaux pour embellir Rome .
À la mort de Jules César , son petit neveu et fils adoptif , Octave , son lieutenant , Marc Antoine et le proconsul de la Gaule transalpine , Lépide s' entendent pour se partager le pouvoir .
Après la destitution de Lépide en tant que triumvir par Octave , les deux hommes se retrouvent face à face .
Octave reste le seul maître de Rome .
Rome est avec un million d' habitants la plus grande ville du monde méditerranéen .
Des travaux sont entrepris pour stabiliser les rives du Tibre .
À sa mort , c' est son dernier fils adoptif , Tibère fils d' un premier mariage de Livie , qui devient empereur .
L' Empire prospère et accumule des fonds qui contribuent alors à assainir les finances , mais son règne est aussi marqué par les meurtres de personnalités politiques , et il meurt haï , .
Caligula , son petit-neveu et petit-fils par adoption , troisième fils de Germanicus , prend par la suite le pouvoir .
Pendant six mois , les Romains peuvent se féliciter d' un empereur juste , utile et libéral , qui leur font oublier la sinistre fin du règne de Tibère ; mais une grave maladie fait changer dramatiquement Caligula , qui devient un tyran .
Une énième conspiration a raison de lui et c' est Claude , son oncle , qui lui succède , .
Malgré son manque d' expérience politique , Claude se montre un administrateur capable et un grand bâtisseur public .
Il étend la citoyenneté romaine à beaucoup de provinces , dont la Gaule où il est né .
Mais c' est un empereur faible , et il meurt empoisonné à l' instigation d' Agrippine en 54 , après avoir , sur les conseils de celle-ci , adopté son fils Néron .
Les premières années de son règne sont connues comme des exemples de bonne administration , puis de nombreux scandales éclatent , ainsi le grand incendie de Rome .
Mal entouré , il prend de mauvaises décisions et ordonne un dernier suicide , celui d' un excellent général , Corbulo , ce qui provoque la rébellion de plusieurs généraux .
C' est la fin des Julio-Claudiens .
Des généraux , Galba , Othon et Vitellius sont tour à tour nommés empereurs par leurs troupes puis assassinés en 69 .
Il rétablit l' ordre et de la paix à Rome et dans les provinces révoltées .
Tout comme ses prédécesseurs , il multiplie les constructions publiques , notamment le Colisée qu' il entreprend .
Son fils Titus , qui a joué un grand rôle sous son règne , lui succède mais n' est empereur que deux ans .
Le deuxième fils de Vespasien devient alors à son tour empereur .
La conquête de la Bretagne par Agricola se poursuit avec brio et Domitien lance une offensive surprise contre le peuple germain du Rhin le plus puissant à l' époque , les Chattes , qu' il vainc .
Mais très vite la situation se dégrade sur le Danube .
Les Daces viennent de s' unir et Domitien intervient en personne avec la garde prétorienne pour les chasser .
Finalement , après des revers de généraux romains , Domitien préfère traiter et fait la paix avec le roi dace , Décébale , qui devient un roi client et perçoit des subsides .
Au début du règne Domitien se montre libéral et juste .
Trajan , tout en s' attachant à favoriser l' agriculture et à développer l' administration , fait la conquête de la Dacie , de l' Empire parthe et annexe l' Arabie .
La conquête de la Parthie ne lui survit pas .
L' empereur Hadrien s' attache à mener une politique plus défensive .
Sous son règne , dans plusieurs régions frontières , en Afrique et en Bretagne notamment , des fortifications importantes se développent , souvent appelées limes .
Le règne d' Antonin le Pieux n' est pas marqué de conquêtes , mais plutôt par une volonté de consolidation de l' état actuel .
C' est traditionnellement durant son règne qu' on considère que l' Empire romain est à son apogée , du fait de l' absence de guerre et de révolte majeure en province .
C' est pourtant cette politique défensive et attentiste qui annonce les difficultés financières et militaires de l' Empire romain .
Marc Aurèle et Lucius Verus succèdent à Antonin .
Marc Aurèle choisit son fils , Commode comme successeur .
Son assassinat en décembre 192 ouvre une crise politique comme à la fin de la dynastie des Julio-Claudiens .
La garde prétorienne assassine le nouvel empereur Pertinax et porte au pouvoir Didius Julianus .
Les prétoriens qui ont fait et défaits tant d' empereurs sont recrutées parmi les légions du Danube fidèles à Septime Sévère .
Il meurt assassiné sur le front parthe sur ordre du préfet du prétoire Macrin qui ne réussit à prendre sa place que peu de temps .
Le cousin de Caracalla , Élagabal devient ensuite empereur mais tout occupé au culte du dieu du même nom il laisse le gouvernement à sa grand-mère , Julia Maesa .
Il est tué par les prétoriens et son cousin Sévère Alexandre lui succède pour un règne de 13 ans .
Comme Jules César , ils portent le titre de grand pontife qui fait d' eux les chefs de la religion romaine .
Le choix le plus logique est , même aux yeux des Romains , de désigner son fils ou d' en adopter un .
De plus dans les croyances populaires , Scipion l' Africain , Marius et Sylla ont un caractère divin .
César développe autour de lui une légende de divinité prétendant descendre de Vénus et d' Énée .
L' empereur Auguste met en place le culte impérial .
Il fait diviniser César et ainsi , en tant que son héritier , il s' élève ainsi au-dessus de l' humanité .
Il se dit fils d' Apollon .
Auguste refuse d' être divinisé de son vivant .
Il laisse cependant se construire des temples qui lui sont consacrés surtout dans l' Orient habitué à considérer ses souverains comme des dieux vivants , à condition que son nom soit associé à celui de Rome divinisée .
Pendant le règne d' Hadrien , la divinisation de l' empereur vivant progresse encore en Orient .
L' Italie jouit d' un statut privilégié .
Celle-ci constitue pour les Romains , le cadre de vie idéal .
Là où il n' en existait pas , essentiellement en Occident , les Romains en ont créées .
Auguste annexe l' Illyrie et tente vainement de conquérir la Germanie .
Claude fait la conquête de la Bretagne , Trajan , celle de la Dacie , de l' Arabie .
Il fait l' éphémère conquête de la Parthie .
On lui doit le fameux mur d' Hadrien au nord de la Bretagne .
Aux frontières de la Germanie , de l' Orient et de l' Afrique des murs sont érigés .
En tout , les Romains ont 9 000 km de frontière à défendre .
À partir d' Hadrien , une partie des auxiliaires se distinguent de l' armée romaine car ils gardent leur armement traditionnel .
Les Romains l' appellent tout simplement l' urbs , la ville .
Elle est avec Alexandrie , la plus grande ville du monde romain .
Ces nombreux monuments symbolisent la grandeur de Rome et l' art de vivre de Romains .
Mais Rome est avant tout dans l' imagination populaire , la ville des jeux .
En 64 , après l' incendie de Rome , Néron fait reconstruire la ville avec des axes larges et aérés .
L' architecture romaine s' épanouit dans les villes , l' architecture impériale innove dans la généralisation de la voûte en plein cintre , et l' emploi systématique du mortier ( opus caementicium ) puis de la brique ( opus latericium ) , réalisant des monuments de plus en plus audacieux à Rome ( Panthéon , Colisée , forums impériaux , thermes , etc . )
et dans les provinces ( pont du Gard , arènes de Nîmes , etc .
Les grandes métropoles comme Carthage , Antioche refleurissent .
La Méditerranée est ouverte de mars à octobre , c' est-à-dire que la navigation y est autorisée .
Les liens commerciaux atteignent aussi la Baltique , l' Afrique noire via les caravanes transsahariennes , l' Inde et la Chine .
Le dispositif militaire romain , et l' organisation du pouvoir impérial sont aussi très peu adaptés à une guerre simultanée sur deux fronts , en Orient et sur l' ensemble Rhin -- Danube .
Mais l' accent est désormais aussi mis sur la diversité des situations régionales , le maintien d' une prospérité en Afrique , sur l' existence de période de redressement ou sur les capacités de relèvement et de résistance , induisant plus une période de mutation qu' une crise et un déclin continus .
Ainsi Maximin I er le Thrace est le premier militaire de carrière à devenir empereur par la volonté seule de ses soldats .
Il déploie une grande énergie pour sécuriser la frontière face aux Daces et aux Sarmates .
À la fin de 238 , Gordien III , le petit-fils de Gordien I er devient empereur .
Il périt assassiné à l' instigation du préfet du prétoire , Philippe l' Arabe qui doit éliminer plusieurs concurrents avant d' être tué en affrontant Dèce .
Dèce est le premier empereur tué par des barbares , lors de la lourde défaite d' Abrittus face aux Goths en 251 .
Trébonien Galle et Émilien se succèdent à un rythme rapproché .
Valérien règne associé à son fils Gallien .
Les successeurs de Gallien sont tous des militaires à qui l' armée a donné une grande rigueur et la foi en l' éternité de l' Empire romain .
Gallien entame une mutation profonde de la stratégie militaire .
Il répartit en profondeur les moyens de défense en plaçant dans les principaux nœuds routiers de l' Illyrie des détachements des légions frontalières .
Aurélien est divinisé de son vivant .
Dèce , à partir de 250 puis Valérien renouvelle l' obligation de sacrifices , ce qui entraine des persécutions envers les réfractaires .
En 260 , son fils Gallien publie un édit de tolérance maintenu par ses successeurs pendant 40 ans .
Dioclétien reste cependant au sommet .
Cette nouvelle organisation permet d' éliminer les usurpateurs qui semaient le trouble en Gaule , de repousser les barbares .
La victoire sur les Sassanides permet de renforcer la présence romaine en Mésopotamie avec la constitution de cinq nouvelles provinces .
Dioclétien se retire ensuite à Spalato .
La seconde tétrarchie se heurte aux ambitions de Maxence et Constantin , fils respectifs de Maximien et de Constance Chlore .
En 313 , deux empereurs restent en lice , Constantin I er , installé à Nicomédie , et Licinius .
Constantin , premier empereur à s' être converti au christianisme , reste alors le seul souverain .
Cette même année , il choisit l' ancienne colonie grecque de Byzance , installée sur la rive européenne du détroit du Bosphore pour fonder une nouvelle capitale qui portera son nom , Constantinople .
Construite sur le modèle de Rome , elle est inaugurée en 330 .
Quand Constantin meurt en 337 , il n' a pas réglé sa succession .
Un des césars , Julien , en charge de la Gaule , remporte une grande victoire sur les Alamans en 357 .
Ses soldats le proclament empereur à son corps défendant à Lutèce .
Constance II meurt l' année suivante .
Julien , cousin du défunt empereur renonce au christianisme par amour de la pensée grecque , d' où son surnom d ' apostat .
Ses successeurs , Jovien , Valentinien I er en Occident et Valens en Orient reviennent à une absolue neutralité religieuse .
Après la mort de Valens lors de la bataille d' Andrinople en 378 , Gratien se choisit un nouveau collègue pour l' Orient , Théodose le Jeune .
Gratien est assassiné en 383 .
Valentinien II , le jeune frère de Gratien , reste alors seul auguste de l' Occident avec à ses côtés général franc , Arbogast qui l' assassine en 392 .
En 394 , Théodose bat l' usurpateur à la Bataille de la Rivière Froide où les deux armées perdent l' essentiel de leurs forces .
Arcadius l' aîné reçoit l' Orient et Honorius l' Occident .
Mais l' Occident d' Honorius est affaibli par des années de guerres civiles et contre les barbares .
Pour Constantin comme pour Dioclétien , l' autorité impériale est de nature divine .
Dioclétien et Galère , son fils adoptif , se prétendent descendants de Jupiter .
Constantin ne cherche pas à affirmer une filiation divine .
À Constantinople , il construit son palais comme si c' était une église ; il affirme avoir reçu une vision du Christ comme s' il était un apôtre ; il agit comme un évêque lors du concile de Nicée convoqué par lui-même mais il ne l' est pas .
Eusèbe de Césarée , reprenant les thèses de Méliton de Sardes , élabore , à cette époque , la théologie de l' empire chrétien .
Dioclétien augmente le nombre de militaires .
Constantin achève la transformation de l' armée et met en place le comitatus , l' armée de campagne .
En cas de besoin , des maîtres des milices peuvent être créés pour une région particulière comme en Illyrie .
Pour pallier les difficultés de recrutement , Dioclétien impose de nouvelles règles .
Ce système est supprimé en 375 , mais uniquement pour l' Orient .
Dioclétien et Constantin I er recrutent des auxiliaires d' origine barbare pour veiller sur le limes .
Sous Théodose , l' armée se barbarise davantage .
Sous Dioclétien , les distinctions entre provinces sénatoriales et provinces impériales sont supprimées .
Elle est , sous Dioclétien , payée en nature ou en espèces .
Sous le règne de Théodose , la fiscalité se durcit encore provoquant des révoltes ( Antioche en 387 ) .
Constantin prend la décision de supprimer l' ordre équestre dont les membres entrent presque tous dans l' ordre sénatorial .
Les décrets de Valentinien I er interdisant les mariages romano-barbares montrent qu' il existe déjà un métissage non négligeable à cette époque .
Les nouvelles résidences impériales : Trèves , Milan , Sirmium , Nicomédie bénéficient de la présence des troupes et des empereurs .
Les Romains révolutionnent le support écrit des livres , en lui donnant la forme moderne que nous connaissons : ils généralisèrent le codex , volume de feuilles reliées , plus maniable et plus aisé à lire que le traditionnel rouleau .
Tout en racontant la vie des saints à la manière de Suétone ou Plutarque , elle se concentre sur les vertus chrétiennes de saints pour en faire des exemples pour le lecteur .
Il s' agit de la La Cité de Dieu d' Augustin d' Hippone , achevée en 423 .
Il réplique de manière magistrale aux détracteurs du christianisme qui rendaient la religion responsable du sac de Rome de 410 .
Dans sa théorie des deux cités , il développe l' idée que Rome est une cité terrestre donc mortelle .
En Occident , les provinces méditerranéennes sont plus touchées par la nouvelle religion que les autres .
Il lutte contre le donatisme en Afrique et l' arianisme en Orient .
Constantin finit par se convertir à cette forme de christianisme et se fait baptiser sur son lit de mort par un prêtre arien .
Son fils , Constance II est un arien convaincu .
La défaite d' Andrinople face aux Wisigoths ariens permet aux catholiques orthodoxes de passer à l' offensive .
Gratien finit par s' orienter vers une condamnation de l' arianisme sous l' influence conjuguée de son collègue Théodose et d' Ambroise .
Il convoque un concile à Aquilée , en 381 , dirigé par Ambroise .
Après la mort de Gratien , le parti arien est de nouveau très influent à la cour .
Ambroise refuse de concéder une basilique extra muros aux ariens fort du soutien du peuple et des hautes sphères de Milan .
L' historien païen Zosime nous apprend à ce sujet que la nouvelle religion n' était pas encore répandue dans tout l' Empire romain , le paganisme s' étant maintenu assez longtemps dans les villages après son extinction dans les villes .
En 356 , Constance II interdit tous les sacrifices , de nuit comme de jour , fait fermer des temples isolés et menace de la peine de mort tous ceux qui pratiquent la magie et la divination .
L' empereur Julien , acquis au paganisme , promulgue en 361 un édit de tolérance permettant de pratiquer le culte de son choix .
Le 24 février 391 , une loi de Théodose interdit à toute personne d' entrer dans un temple , d' adorer les statues des dieux et de célébrer des sacrifices , " sous peine de mort " .
En 392 , Théodose interdit les Jeux olympiques liés à Zeus et à Héra , mais aussi à cause de la nudité du corps des compétiteurs , le culte du corps et la nudité , étant dénigré par le christianisme .
Pour l' éradiquer , le pape Gélase I er décide en 495 de célébrer la fête de saint Valentin , le 14 février , un jour avant la fête des Lupercales pour célébrer les amoureux .
Le port d' amulettes , les cultes aux arbres et aux sources n' ont pas disparu de la Gaule méridionale .
Deux cent mille d' entre eux sont établis au sud du Danube , en Mésie en échange de levée de recrues .
Des esclaves , des colons et des travailleurs des mines se joignent à eux pour ravager la Thrace .
Sans attendre l' arrivée de son neveu Gratien , retenu par les Alamans en Occident , l' empereur Valens engage le combat avec sa seule armée et est tué lors de la bataille d' Andrinople en 378 où la cavalerie wisigothe met à mal la légion romaine .
Les Goths ont le droit de s' installer en Thrace .
Arcadius négocie à prix d' or leur retrait vers l' ouest .
En 402 , alors que les Ostrogoths envahissent les provinces danubiennes , les Wisigoths pénètrent en Italie .
En 410 , ils saccagent Rome .
Cet épisode est ressenti comme une catastrophe par les Romains .
Saint Jérôme y voit le châtiment des pêchés des hommes .
Mais entre temps , le 31 décembre 406 , les Vandales , les Sarmates , les Suèves , les Alains et les Alamans franchissent le Rhin bientôt suivis par les Burgondes .
Ils ravagent la Gaule et l' île de Bretagne , qui est , dès lors , définitivement abandonnée .
L' empereur , installé à Ravenne , est contraint d' accepter l' installation de nouveaux royaumes barbares en Gaule .
En 435 , les Vandales obtiennent à leur tour le statut de fédérés en Afrique orientale .
L' Empire romain d' Occident se réduit à l' Italie et une partie de la Gaule .
Il bat les Burgondes grâce à son armée composée de Huns -- Aetius a été otage à la cour des Huns pendant son enfance , où il y est devenu un ami du jeune Attila -- et les transfère en Sapaudia où en 434 , Valentinien III les autorise à s' installer en tant que peuple fédéré .
En 451 , grâce à une armée plus barbare que romaine , -- elle comprend un fort contingent wisigoth -- , il parvient à repousser Attila à la bataille des champs Catalauniques .
Mais il est égorgé en 454 par Valentinien III lui-même , jaloux de ses succès .
L' empereur est à son tour assassiné par les partisans d' Aetius .
L' Empire romain d' Occident connaît alors une instabilité politique avec des empereurs impuissants , contestés par des usurpateurs .
En 455 , Rome est pillée pendant plus d' un mois par les Vandales de Genséric .
L' un d' eux , Odoacre , dépose le tout jeune empereur Romulus Augustule et envoie les insignes impériaux à Constantinople en 476 .
En sa qualité de représentant du pouvoir impérial , Théodoric tente d' étendre son pouvoir sur les autres royaumes barbes , ariens comme lui .
Pour Théodoric , les Goths sont les protecteurs des Romains .
La politique et la culture romaines ont une grande influence sur les Goths .
Sous le règne de Théodose II , la ville de Constantinople continue à s' agrandir et reçoit une nouvelle enceinte , le mur de Théodose .
Un code juridique est publié , le Code de Théodose .
Un tribut et l' octroi d' une dignité romaine à Attila permettent d' éloigner le danger .
Son petit-fils Léon II ne règne que quelques mois .
C' est donc son gendre Zénon qui revêt la pourpre impériale pendant quinze ans de 476 à 491 .
Il est donc le seul empereur du monde romain mais son autorité sur l' Occident n' est que théorique .
Justinien ( 527 -- 565 ) est le dernier empereur romain .
L' Occident est donc le premier objectif de Justinien .
Il conquiert l' Afrique sur les Vandales en quelques mois .
En 554 , les Byzantins font la conquête une partie de l' Espagne wisigothique jusqu' à Cordoue .
De plus , les conquêtes de Justinien sont fort coûteuses .
En 568 , seules les régions de Ravenne et de Rome sont encore aux mains de Byzantins .
Bien que leur contenu soit souvent sujet à caution , ils sont une source majeure d' information sur l' histoire politique de la Rome antique .
En outre , la littérature latine , dont on a conservé de nombreux textes , fournit de nombreux renseignements sur la mentalité et l' histoire culturelle de Rome .
