Meuvaines est une commune littorale du Calvados située à douze kilomètres de Bayeux , dans le Bessin .
Au premier recensement républicain , en 1793 , Meuvaines comptait 400 habitants , population jamais dépassée depuis ( égalée en 1806 ) .
