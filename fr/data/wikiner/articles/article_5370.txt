Pour la signification de ce toponyme anglo-scandinave , voir Hautot-sur-Seine ( Seine-maritime ) .
Une gare a été ouverte en 1879 sur la ligne Mézidon -- Dives-Cabourg , prolongée jusqu' à la gare de Trouville-Deauville en 1884 .
La section entre la gare de Mézidon et la gare de Dives-Cabourg a été fermée au trafic voyageur en 1938 et au fret en 1969 .
En 1973 , Hotot-en-Auge ( 206 habitants en 1968 ) absorbe les communes de Brocottes ( 104 habitants ) et Le Ham ( 35 habitants ) .
