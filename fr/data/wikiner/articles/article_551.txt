Le gujarâtî est parlé par environ 46 millions de personnes dans le monde , dont 45,5 millions en Inde , 250 000 en Tanzanie , 150 000 en Ouganda , 100 000 au Pakistan et 50 000 au Kenya .
La langue compte également des locuteurs en Afrique du Sud qui , selon l' article 6 de sa constitution , en fait la promotion .
Un nombre important de locuteurs vivent aux États-Unis et au Royaume-Uni ( Leicester et Wembley ) .
