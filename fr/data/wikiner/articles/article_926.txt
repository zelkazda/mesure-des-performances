Elle est protectrice de Rome et patronne des artisans .
J.-C. , elle a été assimilée à la déesse grecque Athéna , héritant d' une grande partie des mythes liés à celle-ci .
Le portrait casqué de la déesse Minerve ( vue du profil droit ) est l' emblème officiel de l' Institut de France .
Minerve est la fille de Jupiter .
Elle est sortie du crâne de Jupiter , vêtue d' une armure , après que Vulcain eut frappé avec une hache le crâne de Jupiter , qui souffrait de cette gestation .
