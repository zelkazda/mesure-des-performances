Les années 1960 , dans le prolongement de la décennie 1950 , voient un développement de l' économie américaine et le développement de nouvelles technologies conduisant à une révolution dans les domaines de l' aéronautique ( mise en service des premiers avions de ligne à réaction , qui entraîne une montée en puissance massive du trafic aérien ) , l' astronautique ( mise en œuvre du programme Apollo ) et l' informatique ( conception de l' ARPANET ) .
A bien des égards , la guerre du Viêt Nam est le prolongement direct de la guerre d' Indochine française .
Après la victoire des forces communistes vietnamiennes ( Viêt-minh ) sur l' armée coloniale française lors de la bataille de Diên Biên Phu en 1954 , la colonie accède à l' indépendance .
La confiance de l' opinion américaine dans la " lumière au bout du tunnel " est balayée en 1968 quand l' ennemi , que l' on disait sur le point de s' effondrer , organise l' offensive du Têt .
Trente ans plus tard , des organisations de vétérans de la guerre du Golfe feront entendre leur opposition à la guerre en Irak .
Un autre démocrate , Eugene McCarthy , se présente contre lui pour l' investiture sur une plate-forme hostile à la guerre .
McCarthy perd les premières élections primaires au New Hampshire , mais crée la surprise en réalisant un score élevé contre le sortant .
Le vice-président de Johnson , Hubert Humphrey , se porte également candidat , promettant , lui , de continuer à aider le gouvernement du Sud Viêt Nam .
Robert Kennedy est assassiné durant l' été , et McCarthy se montre incapable de contrer le soutien dont jouit Humphrey auprès de l' élite du parti .
Humphrey gagne l' investiture de son parti , et se présente contre Richard Nixon dans les élections générales .
En 1969 , le massacre de Mỹ Lai , commis un an auparavant , devient un scandale national .
C' est ainsi que les renforts militaires promis pour la défense du gouvernement du Sud Viêt Nam ne sont pas envoyés ; l' aide économique , toutefois , se poursuit .
Saigon est prise le 30 avril 1975 .
Henry Kissinger a été impliqué dans plusieurs de ces actions .
Pendant longtemps , avec moins de 300 militaires officiellement permanents et aucune base pérenne notable , la présence militaire américaine en Afrique subsaharienne reste modeste .
Dans un contexte de retrait des petites puissances , la politique américaine repose sur le principe d' opposition à l' URSS .
John Fitzgerald Kennedy instaure une " politique africaine " , mais , fondamentalement , il s' agit là plus d' une politique antisoviétique que d' une véritable politique africaine .
En outre , les intérêts économiques y sont relativement limités , à quelques exceptions près , comme en Afrique du Sud ( en raison de la richesse minérale du pays ) ou en Angola ( où la richesse pétrolière intéresse l' entreprise américaine Gulf Oil ) .
