L' Autriche est membre de l' Union européenne ( UE ) et de la zone euro .
L' Autriche est un pays neutre , qui ne fait par exemple pas partie de l' OTAN , à la différence de la plupart des pays européens .
Le Conseil national autrichien ( Nationalrat , 183 sièges ) est depuis le 28 septembre 2008 composé comme suit :
Le président fédéral , Heinz Fischer du SPÖ fut élu le 25 avril 2004 avec 52,41 % des voix contre 47,59 % des voix pour Benita Ferrero-Waldner .
