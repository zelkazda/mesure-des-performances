Henri IV ( 3 avril ( ? )
Henri naît au château de Bolingbroke ( dans le comté de Lincolnshire ) .
Leur fils Henri , futur Henri V d' Angleterre , naît à Monmouth en 1387 .
Il effectue un pèlerinage à Jérusalem en 1393 .
En 1399 , il débarque secrètement à Ravenspurn dans le Yorkshire et vainc puis capture le roi Richard II , contraint d' abdiquer .
Le parlement reconnaît aussitôt son avènement , sous le nom de Henry IV .
Son fils , le futur Henry V , se révolte en 1411 contre lui ( révolte manquée ) .
Il meurt ( peut-être de la lèpre ) à l' abbaye de Westminster , mais il est enterré à la Cathédrale de Cantorbéry .
