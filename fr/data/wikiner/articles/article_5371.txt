Commune renommée Hottot-les-Bagues le 27 août 1955 .
Sur le territoire de la commune se trouve le cimetière militaire d' Hottot-les-Bagues , où sont enterrés des soldats alliés et allemands de la Seconde Guerre mondiale .
