Cédric en tombe très vite amoureux .
Cédric est un petit garçon turbulent , il arrive à sa nouvelle école lors du premier tome .
Marie-Rose , la mère de Cédric , est mère au foyer .
Elle adore Cédric , même si parfois il la met dans tous ses états .
Ainsi , quand Cédric ramène son bulletin , il entre souvent dans une colère folle .
Le grand-père de Cédric déteste le mari de sa fille et il ne lui cache pas .
Il dit que le père de Cédric est un vendeur de " carpettes " , ce qui agace le père au plus haut point .
Il partage une très grande complicité avec Cédric , son petit-fils .
Par exemple , une fois , Cédric est au courant des difficultés financières de ses parents .
Cédric fait tout pour lui plaire mais elle ne s' en rend même pas compte .
C' est l' institutrice de Cédric .
C' est le meilleur ami de Cédric .
Il a des lunettes et se plaint souvent quand Cédric les casse .
Il lui rappelle souvent que sa sœur est dingue de son meilleur ami mais Cédric réplique en disant toujours que sa sœur est moche .
La famille de Cédric est unie malgré les disputes fréquentes qui jalonnent les albums .
En règle générale , cela part d' une réflexion de Cédric sur un de ses problèmes quotidiens .
Le passé de la famille de Cédric est plus ou moins flou .
En effet on apprend dans un album que ses parents se sont rencontrés lors d' un achat de tapis pour la grand mère de Cédric , aujourd'hui décédée .
Cédric est un enfant dans la moyenne de son âge .
Cédric a souvent des traits de caractère et des pensées extrêmement égocentriques ( à rapprocher de ses tendances caractérielles ) et machistes .
Au total , 24 albums ont été publiés aux Éditions Dupuis et diffusés à plus de 8 millions d' exemplaires .
Cédric a un lectorat de fidèles .
Depuis sa création , les gags de Cédric paraissent chaque semaine dans le magazine hebdomadaire belge Spirou .
Et pendant tout un temps dans " Le journal de Mickey " hebdomadaire français pour les jeunes de 7 à 14 ans .
C' est en 2002 que Cédric y fait sa première apparition dans un livre relié sans images .
Cédric inaugure sa carrière à la télévision en septembre 2001 sur Canal J et vingt jours après sur France 3 , avec une série de dessins animés , adaptation plus ou moins libre des albums parus .
Tous les épisodes sont racontés à la première personne par Cédric à la manière d' un journal intime ( qu' il range dans sa table de nuit d' ailleurs ) .
Chaque épisode se conclut par la réplique suivante prononcée par Cédric : " Quelle vie on mène quand on a huit ans " , prononcée avant qu' il ne s' endorme .
