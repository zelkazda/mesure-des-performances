Née à Paris , Irène Jacob , fille du physicien Maurice Jacob , passe son enfance à Genève , où ses parents s' installent alors qu' elle n' a que trois ans .
Elle doit sa première apparition au cinéma à Louis Malle qui lui confie le rôle d' une professeur de piano pour quelques scènes du film Au revoir les enfants en 1987 .
En 1991 , elle incarne l' héroïne de La Double Vie de Véronique de Krzysztof Kieslowski .
Pour ce rôle , elle obtient le prix d' interprétation féminine à Cannes .
Parallèlement à sa carrière de comédienne , elle a également chanté en duo -- entre autres -- avec le groupe de rock français Weepers Circus ainsi qu' avec Vincent Delerm .
