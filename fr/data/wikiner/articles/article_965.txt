La Neustrie avait été créée lors du partage qui suivit la mort de Clovis I er , en 511 , et revint à Clotaire I er , qui , au terme de son long règne de cinquante ans , avait réussi à reconstituer le royaume de son père .
Elle s' étendait jusqu' en Flandre au nord .
C' est ce dernier qui hérite de la Neustrie .
Âgé seulement de quelques mois , Clotaire II , fils de Chilpéric I er , hérite de la Neustrie à la mort de son père en 584 .
À la mort de Thierry II de Bourgogne , en 613 , Clotaire II récupère l' Austrasie et devient ainsi roi des Francs qui récupère aussi l' Aquitaine .
Après la mort de Clotaire III en 673 , la Neustrie , après la déposition d' Ébroïn se voit imposer un roi -- Thierry III -- par les Austrasiens , l' Aquitaine ayant retrouvé auparavant son indépendance .
Cependant la distinction entre Neustrie , Austrasie et Bourgogne va subsister encore , bien que s' effaçant progressivement .
Après le traité de Verdun en 843 , le nom de Neustrie ne désigne plus que la partie ouest du royaume .
La population franque de cette région ne sera pas chassée lors de cette annexion et constituera la Bretagne de langue romane .
