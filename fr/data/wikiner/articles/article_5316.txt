Le Danube est le plus long cours d' eau d' Europe ( après la Volga , mais cette dernière prend sa source en Russie ) , et le plus long parcourant l' Union européenne .
Il prend sa source dans la Forêt-Noire en Allemagne lorsque deux cours d' eau , la Brigach et le Breg , se rencontrent à Donaueschingen ; c' est à partir de ce point que le cours d' eau prend son nom de Danube .
La source précise du Danube est celle du Breg et donc la Brigach est le premier affluent important du Breg , nommé Danube dès leur confluence .
Le Danube mesure 2970 km environ , à partir de Donaueschingen et mesure en fait 3019 km à partir de sa source officielle .
Il se jette dans la mer Noire par le delta du Danube situé en Roumanie et en Ukraine , delta qui figure dans la liste du patrimoine mondial de l' UNESCO .
Le Danube est depuis des siècles une importante voie fluviale .
Connu dans l' histoire comme une des frontières de l' Empire romain , le cours d' eau s' écoule le long des frontières de dix pays : l' Allemagne ( 7,5 % ) , l' Autriche ( 10,3 % ) , la Slovaquie ( 5,8 % ) , la Hongrie ( 11,7 % ) , la Croatie ( 4,5 % ) , la Serbie , la Bulgarie ( 5,2 % ) , la Roumanie ( 28,9 % ) , la Moldavie ( 1,7 % ) et l' Ukraine ( 3,8 % ) .
Son bassin versant comporte une partie de neuf autres pays : l' Italie ( 0,15 % ) , la Pologne ( 0,09 % ) , la Suisse ( 0,32 % ) , la République tchèque ( 2,6 % ) , la Slovénie ( 2,2 % ) , la Bosnie-Herzégovine ( 4,8 % ) , le Monténégro , la République de Macédoine et l' Albanie ( 0,03 % ) .
Cet étymon est un cognat du nom farsi dānu , signifiant " rivière " ou " courant " , désignant aussi le Danube .
Diodore de Sicile
Le Danube est le seul grand fleuve européen qui s' écoule d' ouest en est .
Il atteint , après un parcours de 2852 kilomètres ( longueur abrégée ) , la mer Noire dans la région du delta du Danube ( 4300 km 2 ) , en Roumanie et en Ukraine .
Contrairement aux autres fleuves , les kilomètres du Danube sont comptabilisés depuis l' embouchure jusqu' à la source , le point " zéro " officiel étant matérialisé par le phare de Sulina en bordure de la mer Noire .
Ne sont donc pas pris en compte le parcours de la Breg , le cours initial du Danube , et le parcours principal dans son delta .
Le bassin versant du Danube a une superficie de 802266 km 2 .
Une étymologie commune a donné le même nom à deux d' entre eux : le Morava .
Les divers affluents du Danube présentent une grande hétérogénéité dans leur régime : régime pluvial océanique en Bavière occidentale , nivo-pluvial de montagne en Autriche , pluvio-nival de plaine en Hongrie , nival de plaine en Valachie -- Moldavie .
Le régime pluvio-nival complexe du Danube rend compte de ces diverses influences .
Le fleuve est alors sensible à la rétention hivernale et la fusion nivale lui donne à Linz un débit minimum en décembre et un maximum en mai ou juin ( pour une moyenne de 1710 m 3 ⋅s -1 ) .
Ces précipitations sont responsables d' inondations catastrophiques , le Danube roulant jusqu' à 5 fois son débit habituel : 8000 m 3 ⋅s -1 en juin 1965 et 1970 , 9000 m 3 ⋅s -1 en juillet 1899 .
À Budapest et à Bratislava , la fonte hivernale de la plaine maintient le maximum d' abondance en mai-juin .
L' apport des eaux de la Tisza et de la Save rendent les hautes eaux plus précoces , désormais au printemps ( avril-mai ) , et creusent les basses eaux de juin à septembre ( c' est le cas à Giurgiu où le débit atteint 5900 m 3 ⋅s -1 ) .
À partir des Portes de Fer , le Danube devient sensible au régime climatique annonçant celui de la steppe russe et donnant des débits estivaux très bas .
Les hivers rudes liés au climat continental font que le Danube charrie des glaces presque tous les ans et se trouve pris à un quelconque point du cours ( les défilés , surtout ) un an sur deux ou trois .
Les principaux dégâts sont enregistrés en Hongrie dont la plaine est régulièrement envahie par les eaux , ce qui a nécessité une politique d' aménagement avec construction de digues et rectification du cours .
Le Rhin est le seul fleuve alpin qui s' écoule vers le nord en direction de la mer du Nord .
Les gorges de l' actuel Jura souabe , aujourd'hui dénuées de cours d' eau , sont des restes du lit de cet ancien fleuve qui était nettement plus important que le Danube que nous connaissons .
Comme ces grandes quantités d' eau érodent de plus en plus cette roche calcaire , on suppose que le Danube supérieur disparaîtra un jour complètement au profit du Rhin .
On appelle ce phénomène la " disparition du Danube " .
Comme ces périodes de sécheresse ont fortement augmenté ces dernières années , une partie de l' eau du Danube a été dérivée de cette zone à travers une galerie souterraine .
Le Danube est formé de deux ruisseaux descendant de la Forêt-Noire , la Breg et la Brigach .
La Breg prend sa source près de Furtwangen , à 1078 mètres d' altitude .
Ayant un parcours plus long , sa source , qui ne se situe qu' à cent mètres de la ligne de partage des eaux du bassin du Rhin , est considérée comme la source géographique du Danube .
La République de Moldavie a obtenu en 1990 un accès de quelques 300 mètres à la rive gauche du fleuve à Giurgiuleşti ( entre Galaţi et Reni ) .
Il est classé au patrimoine mondial par l' Unesco depuis 1991 .
La Roumanie , qui a inauguré en 1984 un canal de 64 km à partir de Cernavodă directement vers la mer Noire comme raccourci de 400 km , s' inquiète des répercussions sur l' environnement de l' aménagement du canal de Bystroe par l' Ukraine .
La contribution des différents pays riverains au débit du Danube est la suivante : Autriche ( 22,1 % ) , Roumanie ( 17,6 % ) , Allemagne ( 14,5 % ) , Serbie ( 11,3 % ) , Bosnie ( 8,8 % ) , Croatie ( 6,4 % ) , Hongrie ( 4,3 % ) , Ukraine ( 4,3 % ) , Bulgarie ( 3,7 % ) , Slovénie ( 3,1 % ) , Slovaquie ( 1,9 % ) , République tchèque ( 1,2 % ) , Moldavie ( 0,7 % ) .
Dix pays se trouvent en bordure du Danube .
Quatre pays ne se situent que sur un seul rivage ( la Croatie , la Bulgarie , la Moldavie et l' Ukraine ) .
La Serbie manque dans le tableau ci-dessus : environ 550km rive gauche et 580 rive droite .
Les plus grandes villes situées en bordure du fleuve sont Tuttlingen , Sigmaringen , Ulm , Neu-Ulm , Ingolstadt , Regensburg ( Ratisbonne ) , Straubing et Passau .
A Passau , c' est d' abord l' Ilz qui s' écoule par la gauche dans le Danube et juste après c' est l' Inn par la droite .
L' eau de l' Inn qui provient des Alpes est verte , l' eau du Danube est bleue et l' eau de l' Ilz , qui provient d' une région marécageuse , est noire .
La prédominance de l' eau verte de l' Inn une fois les trois cours d' eau réunis est due d' une part à la grande quantité d' eau charriée par l' Inn lors de la fonte des neiges ainsi qu' à la grande différence de profondeur de l' Inn et du Danube ( 1,90 mètre pour le premier contre 6,80 mètres pour le second ) .
En fait l' eau de l' Inn " surnage " au-dessus du Danube .
L' Autriche n' a aujourd'hui plus que 350 kilomètres du fleuve sur son territoire , ce qui place ce pays à la sixième place des pays riverains .
Par contre les cours d' eau de presque tout le pays alimentent le Danube et donc la Mer Noire .
Un peu plus de 70 kilomètres après la frontière , le Danube traverse Linz , la troisième plus grande ville d' Autriche .
Ensuite , le fleuve passe sur près de 36 kilomètres au milieu d' un des paysages les plus pittoresques de la vallée du Danube , la Wachau , qui s' étend de Dürnstein jusqu' à Krems .
La ville fut durant des siècles la ville danubienne la plus grande et la plus importante mais de nos jours elle doit partager ce statut avec Belgrade et Budapest .
La ville tient son nom d' un affluent , la Vienne , qui rejoint le Danube à cet endroit .
Lors de son entrée en Slovaquie , le Danube marque d' abord la frontière avec l' Autriche puis à 45 kilomètres seulement de Vienne , il traverse Bratislava , la capitale slovaque , où il est rejoint par la rivière Morava .
Finalement , il matérialise encore la frontière entre la Slovaquie et la Hongrie .
Les villes situées le long du fleuve en Slovaquie sont , en dehors de Bratislava déjà citée , essentiellement Komárno , un centre peuplé par la minorité hongroise en Slovaquie , où le Váh , la plus grande rivière slovaque , conflue avec le Danube .
En suivant le Danube qui forme alors la frontière entre la Hongrie et la Slovaquie , la première ville importante rencontrée est Győr , au confluent du Danube et de la Raab .
Près du confluent avec l' Ipel , près de Szob , le Danube passe entièrement la frontière et est désormais hongrois sur ses deux rives .
Vient ensuite la " boucle du Danube " , près de Visegrád , où le fleuve pivote de 90° vers le sud .
Après avoir parcouru environ 40 kilomètres , le Danube traverse la plus grande ville de son périple , Budapest ( 1,8 million d' habitants ) , la capitale de la Hongrie .
Avec seulement 137 kilomètres , la Croatie a , après la Moldavie , la plus petite part du Danube sur son territoire .
Le fleuve arrive en Croatie à Batina , un port danubien situé au point de rencontre de la Croatie , de la Hongrie et de la Serbie .
Ensuite , il sert de frontière naturelle entre la Croatie et la Serbie .
La ville croate la plus importante située au bord du Danube est Vukovar , qui a subi des dégâts importants lors de la guerre avec la Serbie .
Une autre grande ville croate , Osijek , se situe également à proximité du fleuve , à environ vingt kilomètres du confluent du Danube et de la Drave , son second plus long affluent .
Au début , la Croatie ( rive droite ) se partage le Danube avec la Serbie ( rive gauche ) .
Près de Bačka Palanka , le Danube forme une boucle et traverse alors la Serbie vers le sud-est en s' éloignant de la frontière croate et se rapprochant de la frontière roumaine .
En aval de cette ville , le fleuve passe Novi Sad dont les ponts ont été gravement endommagés en 1999 lors de la guerre du Kosovo .
Ceci perturba la navigation sur le Danube car le pont ne fut ouvert que trois fois par semaine .
Après avoir parcouru 70 kilomètres supplémentaires , le Danube atteint Belgrade , la troisième plus grande ville riveraine du fleuve avec 1,6 million d' habitants .
Le site est habité depuis 7000 ans , ce qui en fait une des plus vieilles cités habitées en permanence sur les berges du Danube .
Elle est construite autour du confluent avec la Save et son centre est dominé par l' imposante forteresse Kalemegdan .
En continuant son parcours à travers la Serbie , le Danube passe par les villes industrielles de Pančevo , où le Danube conflue avec le Tamiš , et Smederevo , où la Morava se jette dans le Danube .
Sur la rive serbe se trouve le parc national de Djredap contenant la Table de Trajan .
Au niveau de la Bulgarie , le Danube marque la frontière entre le nord de ce pays et la Roumanie : c' est la rive droite qui est bulgare .
Le long de cette frontière de 500 kilomètres , il n' existe qu' un seul pont qui relie depuis 1954 la plus grande ville danubienne bulgare , Ruse , et la ville roumaine de Giurgiu .
Pour la Bulgarie , le fleuve a , malgré sa longueur en bordure de son territoire , une importance moindre que pour les autres pays riverains .
Comme il s' agit de la seule voie navigable du pays et que celle-ci se trouve de surcroît dans l' extrême nord peu peuplé de son territoire , le Danube n' a qu' une importance régionale pour la petite flotte marchande .
Dans la ville de Svishtov le Danube atteint son point le plus méridional .
À partir de là , il remonte vers le nord en territoire roumain , et quitte le territoire bulgare juste après Silistra .
Sur 1075 kilomètres soit environ un tiers de sa longueur totale , le Danube est roumain .
Ainsi la Roumanie possède de loin la plus grande part du fleuve .
Au début le fleuve forme la frontière avec la Serbie et la Bulgarie puis , dans la région située entre le Bărăgan et la Dobroudja , il effectue un virage vers le nord avant de se jeter dans la Mer Noire après avoir marqué la frontière avec l' Ukraine .
Après avoir atteint Orşova , il traverse la célèbre " percée du Danube " et passe à Drobeta Turnu Severin .
Ensuite le Danube poursuit son chemin vers l' est où il forme sur 400 kilomètres la frontière avec la Bulgarie .
Ce faisant il passe par les villes de Dăbuleni , Corabia , Turnu Măgurele , Zimnicea , Giurgiu ( située juste en face de la ville bulgare de Ruse ) , Olteniţa , où la rivière Argeş se jette dans le Danube , et Călăraşi .
Sur une centaine de kilomètres après Galaţi , le cours du Danube sert de frontière entre la Roumanie , la Moldavie ( sur 570 mètres ) et l' Ukraine .
La Moldavie a le plus petit tronçon du Danube sur son territoire .
Juste après avoir été rejointe par la rivière Prut en aval de Galaţi , la rive gauche du Danube devient moldave et le fleuve marque la frontière entre la Roumanie et la Moldavie sur 570 mètres de long , près de Giurgiuleşti .
La Moldavie n' avait à l' origine que 340 mètres de rivage , mais en 1999 , l' Ukraine lui céda lors d' un échange de territoires 230 mètres supplémentaires .
Bien qu' elle puisse accéder aux ports roumain de Galaţi et ukrainiens de Reni , d' Izmail et de Kilia , la Moldavie envisage d' utiliser son accès au Danube pour la construction d' un port .
Après la frontière moldave , la rive gauche du Danube devient ukrainienne et le fleuve marque la frontière entre la Roumanie et l' Ukraine sur 47 kilomètres .
Après Vylkove , le bras de Chilia passe entièrement en Ukraine et se déverse quelques kilomètres plus loin dans la Mer Noire .
Quelques-unes des plus anciennes civilisations européennes se sont implantées dans le bassin du Danube .
Parmi les civilisations du néolithique danubien , on trouve notamment les civilisations rubanées du milieu du bassin du Danube .
Tant que le fleuve ne gelait pas , cette flotte suffisait à empêcher les Germains , les Daces et les Scythes de traverser , car ils n' avaient pas de technologie pour la contrer .
Les Romains dominent le fleuve jusqu' à Valentinien I er ( 364 -- 375 ) exception faite de quelques années très froides ( 256 à 259 , lorsque les bases et de nombreux bateaux sont pris par surprise ) .
Cette victoire de l' empereur Trajan sur les Daces sous les ordres de Décébale a permis la création de la province de Dacie qui fut à nouveau perdue en 271 .
Le fleuve permettait aux Ottomans d' avancer rapidement et dès 1440 ils livraient les premières batailles pour Belgrade située à 2000 kilomètres de l' embouchure du fleuve .
Comme le roi Louis II de Hongrie fut tué pendant la bataille , la Hongrie fut intégrée à l' Autriche des Habsbourgs .
Ainsi fut stoppée l' expansion des Ottomans le long du Danube et à partir de la bataille de Mohács de 1687 , ils perdent peu à peu du terrain et de la puissance .
Le Danube joua alors non seulement le rôle d' artère militaire et commerciale mais également de lien politique , culturel et religieux entre l' Orient et l' Occident .
Après la Seconde Guerre mondiale , une nouvelle réglementation du trafic fluvial qui devait remplacer les accords de Paris de 1921 fut élaborée en 1946 .
Tous les pays limitrophes du fleuve ont participé à la conférence de Belgrade de 1948 sauf les pays vaincus , l' Allemagne et l' Autriche .
Lors de la signature du traité , il fut également signé un avenant qui accepta l' Autriche au sein de la commission du Danube .
Sur les centaines de kilomètres de son parcours , le Danube traverse une multitude de zones climatiques et de paysages différents ce qui explique que la faune et la flore en bordure du fleuve est tout aussi variée .
Malgré de nombreuses interventions humaines parfois très importantes , de nombreuses sections du Danube présentent toujours une grande variété biologique , en partie grâce à la mise en place d' espaces protégés dans les zones les plus sensibles .
Au total , ce sont plus de 300 espèces d' oiseaux qui vivent en bordure du Danube .
Pour le monde ornithologique , la région la plus importante est le delta du Danube , un carrefour central des routes migratoires en Europe et en même temps un point de rencontre entre la faune européenne et la faune asiatique .
On rencontre plus de 150 espèces de poissons dans le delta du Danube comme par exemple l' esturgeon , le béluga européen , la carpe , le silure , la sandre , le brochet et la perche .
Dans les prairies alluviales à bois durs , on peut noter la présence du frêne à feuilles étroites que l' on rencontre en aval de Vienne ainsi que l' orme et le chêne pédonculé .
Dans le Danube même , on trouve des plantes aquatiques rares comme le piège à loup et l' utriculaire .
Comme beaucoup d' autres fleuves , le Danube a subi de nombreuses atteintes importantes à son milieu naturel depuis le début de l' ère industrielle .
À côté de la progression de la pollution liée à l' industrie , à l' agriculture , au tourisme et aux eaux usées ainsi qu' à la régularisation par des digues , des barrages , des écluses et des canaux , se sont surtout les grands projets qui perturbent fortement l' écosystème du Danube .
Une protection internationale efficace de celui-ci s' avère difficile car ce ne sont pas moins de dix pays , dont certains des plus pauvres d' Europe , qui veulent profiter économiquement de leur situation au bord du fleuve .
Pour l' environnement également , la construction du barrage n' est pas restée sans suite , ainsi les esturgeons ne peuvent plus remonter le Danube pour frayer .
Dans les accords du 16 septembre 1977 conclus entre la Tchécoslovaquie et la Hongrie , la construction d' un énorme ensemble de barrages et d' écluses destinés à la production d' énergie a été décidée .
Aucun compromis n' a encore été trouvé et cette situation envenime les relations entre la Hongrie et la Slovaquie jusqu' à aujourd'hui .
Comme le delta du Danube est très proche , des produits souillés , surtout en cas d' avarie , pourraient y arriver très rapidement et sans êtres dilués ce qui menace fortement cet écosystème protégé .
Le 27 août 2004 , le chantier du canal de Bystroe fut rouvert dans la petite ville ukrainienne de Vylkove .
Des organisations de défense de la nature , le gouvernement roumain et le commissaire de l' environnement de l' Union Européenne ont donc protesté contre ce canal .
L' Ukraine n' a donc pas tenu compte de ces protestations , et le canal a été inauguré le 14 mai 2007 .
Le Danube est une importante source d' eau potable pour dix millions de personnes qui vivent le long du cours d' eau .
D' autres villes comme Ulm ou Passau utilisent également encore pour une grande part de l' eau du Danube comme eau potable .
En Autriche en revanche , 99 % de l' eau potable est puisée dans les nappes phréatiques et des sources et seulement très rarement , par exemple pendant des périodes de canicule , dans l' eau du Danube .
La même chose vaut pour la Hongrie qui utilise de l' eau des nappes phréatiques à 91 % .
Seules des communes situées sur son rivage en Roumanie , où le fleuve est à nouveau plus propre , utilisent à nouveau de l' eau du Danube ( Drobeta Turnu-Severin , delta du Danube ) .
Néanmoins , le Danube n' atteignit jamais l' importance qu' il occupe plus en aval en Allemagne car le débit du fleuve est encore faible et donc pauvre en énergie .
En Autriche , la situation est complètement différente même si la construction de la première centrale hydroélectrique ne remonte qu' à 1953 .
Aujourd'hui , l' Autriche est le pays en Europe , juste après l' Islande et la Norvège , dans lequel l' énergie hydraulique représente la plus grande part de la production énergétique et 20 % de la production totale d' énergie est assurée par les centrales du Danube .
Mais cette évolution n' a pas que des côtés positifs : la monoculture de l' énergie hydraulique , qui en Autriche est essentiellement concentrée le long du Danube et ce de la frontière allemande jusqu' à Vienne ( à l' exclusion de la Wachau ) , modifie le tracé et la vitesse du débit du cours d' eau et empêche l' inondation périodique des forêts alluviales écologiquement de grande importance .
En Slovaquie , l' énergie hydraulique est , avec 16 % de la production totale , la deuxième source énergie la plus importante après le charbon .
L' eau du Danube est aussi utilisée pour le refroidissement de deux centrales nucléaires :
Le Danube n' est navigable qu' à partir de Kelheim , à presque 500 kilomètres de sa source , sur une distance totale de 2415 km jusqu' à l' embouchure .
Le canal du Main au Danube , qui conflue avec celui-ci près de Kelheim , permet également d' effectuer la liaison fluviale entre la mer du Nord , en passant par le Rhin et le Main et la mer Noire .
Le Danube figure parmi les plus anciennes et les plus importantes routes commerciales européennes .
De ce fait les bateaux destinés à la navigation sur le Danube étaient de construction plus simple et moins nécessiteuse en bois que les radeaux .
Des embarcations plus grandes , avec des longueurs de trente mètres et deux tonnes de charge utile , nommées bateaux ordinaires de Kelheim ou d' Ulm , étaient néanmoins parfois chargées de marchandises plus onéreuses , comme le vin ou le sel alimentaire , et tirées à contre-courant .
Avec l' apparition des bateaux à vapeur et plus tard des locomotives , la navigation historique sur la Danube entama son déclin et c' est vers 1900 que les derniers convois furent tirés à contre-courant le long du fleuve .
En 1812 le premier bateau à vapeur entra en service à Vienne .
On peut visiter l' un des derniers exemplaires de ce type de bateau à Ratisbonne .
De telles chaînes ont d' abord été posées sur la ligne Vienne -- Bratislava mais aussi près de Ybbs et Ratisbonne .
Tous les pays héritiers de l' Autriche-Hongrie ont eu une flotte de navires à vapeur et à aubes après 1918 , pour la plupart de construction allemande et autrichienne , quelques-uns de construction tchécoslovaque ou roumaine .
À l' origine , le Danube était un fleuve marchand ouvert , utilisable par tout le monde .
120 ans plus tard , le 18 août 1948 , ce droit fut à nouveau entériné lors de la conférence de Belgrade .
La navigation sur le Danube est permise aux bateaux de toutes les nationalités à l' exclusion des navires de guerre étrangers .
Le respect de ces règles et la conservation de la navigabilité sont surveillés par la commission internationale du Danube .
En plus d' une centaine de bateaux-hôtels qui circulent principalement entre Passau , Budapest et la mer Noire , il existe de nombreux bateaux qui font des excursions journalières , en particulier en Allemagne dans la région de Passau et en Autriche dans la région de la Wachau , ainsi que d' innombrables petites embarcations de plaisance .
En Autriche , la pêche est encore un peu pratiquée autour des villes de Linz et Vienne mais dans le delta du Danube , elle est encore pratiquée de manière plus intensive .
Le Danube est également une région viticole , dans deux pays essentiellement .
En Hongrie , la vigne est cultivée sur presque tout le long du Danube entre Visegrád et la frontière sud du pays .
À côté des nombreux centres d' intérêts célèbres situés le long du Danube , de nombreux paysages et parc nationaux ( déjà décrits ci-dessus ) sont également importants pour le tourisme .
Il existe aussi de nombreux endroits , en particulier sur le Danube supérieur non navigable , où l' on peut pratiquer le canoë , la barque et le pédalo .
Le fleuve est par ailleurs bordé de belles pistes cyclables , en Allemagne et en Autriche surtout ( Eurovéloroute 6 ) .
La navigation de croisière fluviale est aussi très active sur le Danube où , en dehors du tronçon très fréquenté de Vienne à Budapest , certains bateaux naviguent également de Passau jusqu' au delta .
