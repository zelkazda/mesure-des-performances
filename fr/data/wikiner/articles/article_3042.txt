Tony Dreyfus est né à Paris en 1939 .
Entré dans la vie publique à 20 ans , il fut l' un des animateurs de l' UNEF jusqu' en 1963 .
Il sera notamment l' avocat de la CFDT et des ouvriers de l' usine Lip de Besançon .
Conseiller de Paris et du 10 e arrondissement à partir de 1989 , Tony Dreyfus a été secrétaire d' état de 1988 à 1991 au sein du gouvernement de Michel Rocard .
Devant les militants de la section socialiste , Tony Dreyfus s' engage , en mai 2006 , à ne pas briguer de nouveau mandat à la tête de la mairie en 2008 et de mettre ainsi un terme à une situation de cumul .
