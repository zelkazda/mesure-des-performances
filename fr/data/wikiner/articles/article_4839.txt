Dans son enfance , Pierre Pairault écrivait déjà des histoires dont il vendait les chapitres à ses camarades de classe auxquels il les lisait à la récré contre la modique somme d' un sou .
À la fin de la Seconde Guerre mondiale , il obtint son diplôme de chirurgien dentiste .
Parallèlement à son métier de chirurgien-dentiste , Pierre Pairault écrit sous le pseudonyme de Stefan Wul à partir de 1956 .
Stefan Wul participa également à la création des dessins de couverture de ses romans en envoyant à l' illustrateur René Brantonne plusieurs croquis de travail .
Du point de vue de la méthode , Stefan Wul a toujours déclaré travailler sans plan ni ligne directrice , partant d' une simple idée de départ développée peu à peu au fil de l' écriture .
Son plus grand succès français reste le roman post-apocalyptique Niourk qui retrace l' histoire d' un enfant noir , rejeté par sa tribu , parti en quête de la ville mythique de Niourk .
Les romans de Stefan Wul sont présentés par ordre de parution :
