Thorgal est une série de bande dessinée apparue en 1977 aux Éditions du Lombard et créée par Jean Van Hamme au scénario et Grzegorz Rosinski au dessin .
Depuis 2007 , la série est scénarisée par Yves Sente suite à la décision de Van Hamme d' arrêter de scénariser la plupart de ses séries .
Pris dans une tempête en pleine mer , une expédition Viking découvre un bébé dans une mystérieuse embarcation ( qui se révélera être une capsule spatiale ) .
Il s' agirait des Atlantes .
Malheureusement , une erreur d' atterrissage les dépose sur une île du Pôle Nord où la plupart succomberont ( on retrouvera le père et le grand-père de Thorgal dans certaines histoires ) .
Thorgal en réchappe car il est transporté dans une capsule de sauvetage dans laquelle sa mère le dépose avant d' être ensuite recueilli par les Vikings .
Louve , quant à elle , possède le don de communiquer avec les animaux .
Parmi les albums récompensés , citons L' Île des mers gelées , Au-delà des ombres , Les Archers et Le Pays Qâ .
Thorgal Les Archers ( 1991 )
