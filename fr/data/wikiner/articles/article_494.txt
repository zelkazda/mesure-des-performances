Le Fonds monétaire international ( FMI ) est une institution internationale multilatérale regroupant 186 pays , dont le rôle est de " promouvoir la coopération monétaire internationale , de garantir la stabilité financière , de faciliter les échanges internationaux , de contribuer à un niveau élevé d' emploi , à la stabilité économique et de faire reculer la pauvreté " .
Le FMI a ainsi pour fonction d' assurer la stabilité du système monétaire international et la gestion des crises monétaires et financières .
Lors d' une crise financière , pour éviter qu' un pays ne fasse " défaut " ( c' est-à-dire que ce pays ne puisse plus rembourser ses créanciers , voire ne plus payer ses dépenses courantes ) , le FMI lui prête de l' argent le temps que la confiance des agents économiques revienne .
Le FMI conditionne l' obtention de prêts à la mise en place de certaines réformes économiques visant de manière générale à améliorer la gestion des finances publiques et une croissance économique équilibrée à long terme .
Après 1976 et la disparition d' un système de change fixe , le FMI a hérité d' un nouveau rôle face aux problèmes d' endettement des pays en développement et à certaines crises financières .
Le nouvel ordre économique proposé par le représentant américain Harry Dexter White reposait sur trois règles :
Le FMI est plus ou moins complémentaire des autres grandes institutions économiques créées à l' époque : la BIRD qui fut créée en même temps que le FMI , et le GATT ( General Agreement on Tariffs and Trade ) signé peu de temps après .
Elle aurait signifié pour les États-Unis une perte de souveraineté vis-à-vis d' une institution internationale et les aurait empêché de profiter de la position dominante du dollar américain à l' époque .
Le rôle du FMI a été de tenter de garantir le bon fonctionnement du système monétaire de Bretton Woods .
Si le FMI le juge nécessaire , il peut prêter à ce pays jusqu' à 125 % de sa quote-part .
Le FMI fonctionne par ailleurs sur un système d' élection à majorité , où les voix sont pondérées par le montant de la " quote-part " .
Ainsi à sa création , les États-Unis à eux seuls détiennent 25 % des voix .
Les États-Unis sont par ailleurs les seuls à disposer d' un droit de veto au sein de cette organisation .
On remarque que dans un tel système , seuls les États-Unis n' ont pas besoin de se soucier , a priori , du cours de leur monnaie vu qu' elle fait office d' étalon .
Ainsi les États-Unis pouvaient connaître d' importants déficits sans être soumis aux remarques du FMI .
C' est-à-dire qu' une banque nationale comme la Banque de France devait , si un allemand possédant des francs lui demandait , lui échanger contre des dollars américains ou de l' or .
En 1959 , certains pays demandèrent aux États-Unis la conversion de leurs dollars en or , ce qui provoqua une première crise du système .
Face à cette crise , l' économiste belge Robert Triffin ( L' or et la crise du dollar , 1960 ) propose une réforme du FMI .
Il souhaite donc un renforcement du rôle de l' organisation en permettant des prêts plus importants qui seraient octroyés , non en monnaie nationale , mais dans une unité de compte commune et propre au FMI .
Cette nouvelle unité de compte sous le contrôle du FMI permettrait la stabilité du système monétaire international , et résoudrait la contradiction qui découle du rôle prépondérant du dollar américain .
Cette proposition célèbre ne sera pas retenue , bien que le diagnostic de Robert Triffin se révélera exact .
En 1969 , face à l' incapacité du dollar à jouer son ancien rôle , le FMI va créer de toute pièce une nouvelle monnaie , toujours existante , le DTS ( droit de tirage spécial ) .
Le rôle initial principal du FMI , garantir la stabilité des taux de change dans une marge de 1 % , a disparu .
Depuis 1976 , le rôle du FMI consiste en premier lieu à soutenir les pays connaissant des difficultés financières .
Lorsqu' un pays est confronté à une crise financière , le FMI lui octroie des prêts afin de garantir sa solvabilité et d' empêcher l' éclatement d' une crise financière semblable à celle qui frappa les États-Unis en 1929 .
Plus généralement , et conformément à ses autres buts , le FMI a pour responsabilité d' assurer la stabilité du système financier international. "
Le FMI est en ce sens , le responsable de dernier ressort de la liquidité du système financier international , pour éviter le blocage des échanges et la contagion à tout le système ( risque systémique ) de problèmes momentanés de solvabilité d' un pays ou d' une banque centrale donné .
Dans le cadre des prêts qu' il accorde , le FMI se doit de garantir auprès de ses contributeurs la bonne utilisation des fonds alloués à tel ou tel pays .
Ainsi le FMI exige des emprunteurs qu' ils mettent en place les politiques économiques qu' il préconise : les " politiques d' ajustement structurel " .
Finalement les trois grandes missions du FMI sont :
Le FMI est gouverné par ses 185 pays membres , chacun ayant une voix pondérée par sa participation financière à l' organisation ( sa " quote-part " ) .
8 d' entre elles ont un représentant permanent ( États-Unis , Royaume-Uni , France , Allemagne , Japon , République populaire de Chine , Russie et Arabie saoudite ) , les 16 autres sont élus par les pays membres .
Cependant , compte tenu des modalités de prise de décision au sein du FMI , qui supposent une majorité qualifiée correspondant à 85 % des droits de vote , les États-Unis , ou l' Union européenne dans son ensemble , disposent de fait d' un droit de veto sur les décisions du FMI puisqu' ils disposent chacun de plus de 15 % des droits de vote .
Cependant , les pays de l' UE ne sont pas toujours coordonnés .
Les ressources du FMI liées aux quotes-parts sont d' environ 210 milliards de DTS ( soit 300 milliards de dollars américains ) , auxquels s' ajoute la possibilité pour le FMI de recourir à des emprunts envers les grandes puissances économiques ( ces crédits sont de l' ordre de 50 milliards de dollars ) .
Lors du sommet du G20 de Londres le 2 avril 2009 il a été décidé d' accroître significativement les ressources du FMI à hauteur de 1000 milliards de dollars pour mieux faire face à la crise mondiale .
Le FMI compte environ 2700 employés .
La seule langue officielle du FMI est l' anglais .
Toutefois le FMI est aussi parfois intervenu dans des pays développés , comme en Corée du Sud à la fin des années 1990 .
La déréglementation du marché du travail est une idée fréquemment mise en avant par le FMI pour soutenir la croissance économique .
Le FMI utilise ainsi l ' " indice de protection de l' emploi " ( créé par l' Organisation de coopération et de développement économiques ( OCDE ) ) , pays par pays , et encourage à sa diminution .
Par exemple , le FMI a étudié le cas de la France , et encourage le gouvernement à lutter contre les rigidités sur le marché du travail .
Pour ce qui est des pays en développement , l' analyse des experts du FMI est simple .
Depuis la fin de la Seconde guerre mondiale , les pays ayant choisi d' opter pour une croissance introvertie , comme longtemps la République populaire de Chine et l' Inde , n' ont pas connu de réussite économique , tandis que d' autres , comme les " dragons asiatiques " , ont su profiter de leurs avantages , une main-d'œuvre abondante en particulier , pour s' ouvrir au commerce international et prospérer .
Le FMI cherche donc généralement à contraindre les pays en développement à s' ouvrir au commerce extérieur .
Au début des années 1990 , les financiers internationaux avaient relancé leurs prêts au Mexique dans un contexte de réforme de marché qui visait à libéraliser l' économie .
La crise financière qui découla de ce soudain changement d' humeur des marchés entraîna un renflouement immédiat des investisseurs par le FMI et la Réserve fédérale des États-Unis ( Fed ) .
Pour certains analystes , le redressement rapide du Mexique n' est pas imputable au FMI mais au rôle des crédits commerciaux américains et à l' intégration du pays dans la toute nouvelle ALENA .
La crise financière asiatique touche d' abord l' Indonésie en 1997 où le FMI impose une politique d' austérité monétaire et de rigueur budgétaire , malgré l' instabilité sociale et ethnique du pays .
Face à l' expansion de la crise , les pays d' Asie ont adopté des positions différentes vis-à-vis des politiques préconisées par le Fonds monétaire international .
La Malaisie n' a elle non plus pas suivi les conseils de l' institution et a pris des mesures telles que le contrôle des mouvements de capitaux ce qui a suscité des reproches du FMI .
Toutefois d' autres pays ont connu un relèvement rapide en adoptant partiellement les mesures préconisées par le FMI .
C' est le cas de la Corée du Sud , qui se garda pourtant d' adopter toutes les mesures proposées .
Durant la crise asiatique certains pays se sont par ailleurs montrés extrêmement critiques vis-à-vis de la gestion de cette dernière par le FMI .
D' après lui , les emprunts concédés à ce pays ont permis aux créanciers d' accorder des prêts sans se soucier de la condition économique réelle des emprunteurs : ils pensaient qu' ils seraient de toute manière renfloués par le FMI .
Les États-Unis , via le FMI , avaient aussi imposé une libéralisation des flux de capitaux et une déréglementation du système bancaire .
Si les deux pays ont connu en effet une certaine réussite , l' Argentine a fini par voir son économie s' écrouler en 2001 .
Ensuite , c' est au contraire en n' appliquant pas les recommandations du FMI que l' économie du pays s' est redressée .
Le Chili continue quant à lui son développement sur un très bon rythme de croissance .
Selon Daniel Cohen , le FMI qui a préconisé pendant un temps un libéralisme très idéologique ( au cours des années 1990 ) , est passé depuis à une situation de " pragmatisme absolu " .
Les États-Unis sont le contributeur principal du FMI , et possèdent ainsi 16,79 % des droits de vote , .
L' Union européenne possède 32,1 % des droits de vote .
Les 10 premiers pays , qui représentent plus de 50 % du PIB mondial , ont la majorité des droits de vote alors que le FMI compte 185 pays membres .
Ce qui fait dire aux détracteurs du FMI qu' il est un instrument au service des grands pays , qui financeraient le FMI pour imposer les vues économiques de l' organisation sur les pays qui choisissent d' avoir recours aux financements du FMI .
Selon une règle tacite , le directeur du FMI est un européen ( l' Europe se choisit un candidat susceptible de recueillir l' approbation du conseil d' administration ) , alors que le président de la Banque mondiale est un américain .
La répartition des droits de vote pose pour certains la question de l' équité du FMI : dans La Grande Désillusion , l' économiste américain Joseph Stiglitz fait par exemple du FMI une institution au service de son principal actionnaire , les États-Unis .
Friedman … ) ou de la Banque mondiale .
L' argument principal se base sur le fait que le FMI préconise les mêmes recommandations économiques et globalement les mêmes plans d' ajustement structurel ( essentiellement des privatisations et des ouvertures du marché intérieur ) à tout pays demandeur d' aide , sans analyser en profondeur la structure de chacun .
On peut prendre comme exemple l' Argentine , qui était considérée comme un pays modèle par le FMI ( pour avoir suivi à la lettre ses recommandations ) , mais qui a connu une grave crise économique en 2001 , entraînant le chaos ( avec cinq présidents en dix jours en 2001 ) .
À ce sujet , l' américain Joseph E. Stiglitz a développé ces critiques , notamment sur la période 1990 -- 2000 , dans son livre La Grande Désillusion ( 2002 ) .
La direction de la Banque mondiale s' est également montrée distante par rapport à la position du FMI et a renforcé l' impression que les deux institutions ne parlent plus systématiquement d' une même voix .
James Wolfensohn neuvième président de la Banque mondiale , dans son discours du 6 octobre 1998 , a déclaré " qu' il souhaiterait que les programmes de sauvetage financier attachent plus d' importance aux préoccupations sociales ( comme le chômage ) et que le FMI insistait trop en revanche sur la stabilisation des monnaies " .
La situation en Guinée et au Ghana est sensiblement la même .
En Mauritanie , la suppression de la propriété collective traditionnelle de la terre a entraîné une concentration de la propriété foncière dans les mains de transnationales agroalimentaires .
Le problème peut être lié à la fois à une mauvaise connaissance du terrain par le FMI , et à une mauvaise communication de sa part .
Après avoir retrouvé dès 2003 d' importants taux de croissance ( autour de 9 % ) , elle a finalement décidé en 2005 le remboursement total de sa dette de façon anticipée prévue normalement en 2007 , afin d' éviter l' étranglement de son économie par les intérêts , ce qui avait conduit à la crise financière selon Néstor Kirchner .
Le FMI s' est félicité du remboursement intégral de la dette de l' Argentine .
