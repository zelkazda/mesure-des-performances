Dans le cadre de la thermodynamique des trous noirs elle permet de reproduire la formule de Bekenstein et Hawking pour l' entropie des trous noirs .
L' Univers élégant de Brian Greene donne à ce sujet des précisions à l' usage des non-spécialistes .
Les diverses études réalisées au sein des accélérateurs de particules contredisent toutes les hypothèses formulées et c' est en 1968 que le physicien Gabriele Veneziano tente d' utiliser la fonction bêta d' Euler pour expliquer la relation entre le spin des électrons et leur énergie .
Elle a été proposée par Edward Witten , en 1995 .
Joseph Polchinski observe cependant que Steven Weinberg a prédit dans les années 1980 une constante cosmologique non-nulle en faisant l' hypothèse d' un multivers , ce qui est précisément une conséquence possible de la théorie des cordes .
Par exemple , si le LHC ne détecte pas les particules superpartenaires , il sera possible de modifier la théorie pour rendre ces particules plus lourdes afin d' expliquer leur non-détection .
Mais certains , comme Lee Smolin pensent que la difficulté à aboutir à une preuve définitive témoigne d' un problème fondamental à ce niveau .
