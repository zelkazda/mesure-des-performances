Au cours de certaines de ces réunions , le plus puissant des feudataires est parfois nommé hégémon ( 霸 ) , prenant ainsi la tête des armées vassales des Zhou .
Au sud , les princes de Chu et de Wu se proclament rois , affirmant ainsi leur indépendance vis-à-vis des Zhou .
Vers le milieu du cinquième siècle , le système féodal mis en place par les Zhou n' est plus réellement appliqué en pratique .
On entre dans la période des Royaumes combattants .
