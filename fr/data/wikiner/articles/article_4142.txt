Lieu unique de par son environnement naturel et architectural , la citadelle de Brouage a aussi un riche passé historique .
C' est un ancien port de commerce du sel , puis port de guerre catholique voulu par le cardinal de Richelieu pour concurrencer la place forte huguenote de La Rochelle .
Brouage est également considérée comme étant la commune de naissance du géographe Samuel de Champlain qui a participé à la fondation et à la colonisation de la Nouvelle-France , et qui est le fondateur de la ville de Québec au Canada .
Hiers-Brouage se situe en bordure de l' océan Atlantique à environ 35 kilomètres au sud de La Rochelle et à 120 kilomètres au nord de Bordeaux .
Cette commune du nord de la Saintonge n' est qu' à 6 km de Marennes et 11 km de Rochefort .
Le bassin de Marennes est constitué des marais de Brouage qui occupent la partie évidée de l' anticlinal de calcaire marneux de Jonzac .
Par ailleurs , un risque sismique léger concerne la commune qui est située non loin de la faille d' Oléron .
Le canal de la Charente à la Seudre traverse la commune au sud-est .
Le havre de Brouage est un chenal qui délimite la commune au nord-est et permet de relier l' océan Atlantique au canal de la Charente à la Seudre grâce au prolongement assuré par le canal de Brouage ( entrepris en 1782 et inauguré en 1807 ) .
Ce climat permet à la commune de Hiers-Brouage , pourtant située à un degré de latitude plus au nord que Montréal , au Québec , ou que les îles Kouriles en Russie , de bénéficier d' un taux d' ensoleillement moyen exceptionnel , proche de celui de la Côte d' Azur , sur la mer Méditerranée .
La Charente-Maritime est le département français qui a été le plus durement touché par l' ouragan Martin du 27 décembre 1999 .
Les records nationaux de vents enregistrés ont été atteints avec 198 km/h sur l' île d' Oléron ( à 10 km de Brouage ) et 194 km/h à Royan ( à 25 km ) .
Le village était à cette époque une île au milieu du golfe de Saintonge , golfe qui se comblera ensuite au fil des siècles pour n' être plus aujourd'hui qu' un marais .
Brouage fut fondée en 1555 sur un ancien dépôt de lest formant des bombements de galets et de vase .
Brouage était l' avant-port du village de Hiers , il est conçu tout d' abord sans intentions militaires mais pour être un centre de négoce .
Dix ans après sa fondation , la cité reçoit la visite de Charles IX .
Le port devint le plus important d' Europe et faisait vivre tout un peuple ( sauniers , mariniers , pêcheurs de morue , etc . )
La cité était alors un lieu d' approvisionnement en sel pour les pêcheurs de morue de Terre-Neuve .
En 1576 , lors de la sixième guerre de religion , le duc de Guise prit la ville afin de compléter l' encerclement de la place protestante de La Rochelle .
Cette même année , Henri de Navarre , futur Henri IV , séjourna dans la citadelle .
Point stratégique , elle devint le cœur logistique de la machine de guerre royale pour conquérir La Rochelle .
En 1628 , Louis XIII visita le port .
Entre 1630 et 1640 , Richelieu ordonna la construction d' une nouvelle enceinte réalisée par Pierre de Conty d' Argencour .
Le bourg de Hiers , de son côté , était devenu l' arrière-cour industrieuse de Brouage : c' est là qu' étaient installés tous les métiers du bâtiment ( charpentiers , maçons … ) de l' armurerie et de la marine .
En 1653 , Mazarin devint gouverneur de Brouage .
En 1659 celui-ci hébergea sa nièce , Marie Mancini pour l' éloigner du jeune Louis XIV qui la courtisait mais qui devait épouser pour des raisons politiques l' infante Marie-Thérèse d' Autriche ( 1638-1683 ) .
En 1685 , Vauban modernisa les bastions et les chemins de ronde .
Né à Brouage entre 1567 et 1580 , , ( selon les sources ) , Samuel de Champlain , explorateur , cartographe .
partit pour la Nouvelle-France pour la première fois en 1603 .
Il réalisa par la suite 21 voyages en tout entre la France et la Nouvelle-France .
Il fonda la ville de Québec en 1608 .
Il mourra à Québec le 25 décembre 1635 sans avoir fini ses préparatifs de la fondation de Montréal qui n' aura lieu qu' en 1642 .
Par ailleurs , l' église Saint-Pierre a été restaurée avec des dons de la ville de Québec .
La Rochelle fit en sorte de rendre l' accès impossible au port brouageois en y faisant couler des bateaux à son entrée .
En 1653 , c' est Mazarin qui devient le gouverneur du lieu .
Il y héberge sa nièce , Marie Mancini , pour l' éloigner de son très amoureux Louis XIV .
Cependant , le déclin de la très cosmopolite ville de Brouage commença .
En raison de la baisse du niveau de la mer et à défaut d' une rivière drainante , l' horizon maritime s' éloigna de plus en plus pour laisser place à une étendue de marais , rendant Brouage désœuvrée dans ses principales activités portuaires .
En 1885 , l' armée quitte définitivement Brouage .
Le 29 août 1970 , le gouvernement du Québec rendit hommage à Champlain en inaugurant un mémorial en son honneur devant sa maison natale .
Brouage est aujourd'hui centre européen d' architecture militaire .
En 2001 , Diane Lemieux , ministre d' état à la culture du Québec , est venue à Brouage inaugurer un vitrail de l' église symbolisant les liens de son pays avec la cité saintongeaise .
À l' occasion du 400 e anniversaire de la fondation de la ville de Québec par l' enfant du pays Samuel de Champlain , de nombreuses festivités ont été organisées à Brouage en 2008 .
L' exposition interactive d' un coût de 2 210 500 euros a été financée conjointement par l' ambassade du Canada en France et par le conseil général de la Charente-Maritime .
Hiers-Brouage fait partie , comme six autres communes proches de Marennes , de la communauté de communes du Bassin de Marennes qui correspond aux sept communes du canton de Marennes .
Les deux villages de Hiers et Brouage sont constitués en très grande majorité de pavillons ( 99,3 % contre 80,6 % pour le département ) qui sont pour la plupart des résidences principales ( 76,8 % contre 71,8 % pour le département ) .
Hiers-Brouage est jumelée avec :
Champlain ( Québec ) , depuis 1973 .
