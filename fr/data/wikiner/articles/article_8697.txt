Les havelî sont des demeures , petits palais ou maisons de maître , parfois fortifiés que l' on trouve au Râjasthân et au Goujerat .
À Jaisalmer , elles sont réputées pour leurs décors de pierre finement sculptés .
C' est lors d' un de ces voyages qu' elle a découvert les havelis du Shekhawati et elle a décidé d' en acquérir une en 1998 .
Son idée première était de s' installer à Fatehpur pour peindre , mais au fil des jours , elle a réalisé que tout ce patrimoine artistique était en grand danger .
L' idée première était de montrer aux habitants du Shekhawati ce qu' il était possible d' obtenir à partir d' une matière première aussi riche , mais trop souvent ignorée .
