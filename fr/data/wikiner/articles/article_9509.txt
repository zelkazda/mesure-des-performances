À l' origine , on ne trouvait des muscadiers que dans les îles Banda dans l' archipel des Moluques en Indonésie .
Les Portugais étaient les mieux placés dans cette quête car ils avaient déjà une base à Goa en Inde .
La muscade fut ensuite exportée vers les Antilles et Grenade pour y être également cultivée .
