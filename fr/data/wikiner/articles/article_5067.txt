L' Uttar Pradesh est bordé au nord par le Népal et l' Uttarakhand , à l' ouest par l' Himachal Pradesh , l' Haryana , le Rajasthan et Delhi , à l' est par le Bihar , au sud par le Madhya Pradesh et le Chhattisgarh .
Le chef de l' exécutif , depuis le 13 mai 2007 , est Mayawati , leader du Bahujan Samaj Party .
Le parti des sans caste ( intouchables ) , le Bahujan Samaj Party , est principalement implanté en Uttar Pradesh .
En décembre 1992 , la destruction de la mosquée d' Ayodhya , provoquée par les nationalistes hindous ( BJP , RSS ) , a causé d' importantes vagues de violences entre hindous et musulmans dans tout le pays , la promulgation de l' état d' urgence en Uttar Pradesh et la destitution du gouvernement local ( BJP ) .
L' Uttar Pradesh est divisés en 70 districts regroupés en 17 divisions territoriales qui sont :
Seuls la Chine , l' Inde elle-même , les États-Unis et l' Indonésie ont une population plus importante que l' Uttar Pradesh .
D' après le recensement de 2001 , 80 % de la population est hindouïste alors que les musulmans représentent 18 % des habitants de l' Uttar Pradesh .
Il comprend notamment les villes d' Agra , la ville du Taj Mahal , du mausolée d' Itimâd-ud-Daulâ , Vârânasî , Sârnâth .
