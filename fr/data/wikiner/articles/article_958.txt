Le premier commandement du Décalogue sur lequel se fonde le monothéisme des juifs et des chrétiens est davantage la formulation d' un monolâtrisme , puisqu' il n' enseigne pas le néant des autres dieux , voire suppose même leur existence .
L' émergence du monothéisme judaïque exclusif est lié à la crise de l' Exil .
Dix ans plus tard , les babyloniens ruinent Jérusalem et détruisent son Temple ; s' ensuit alors une seconde déportation qui semble cependant laisser sur place près de 85 % de la population , essentiellement rurale .
Abraham y est " le père de tous les croyants " : c' est à la fois le père de :
Le christianisme est pour les chrétiens la conclusion du judaïsme , car ils reconnaissent le messie dans la personne de Jésus de Nazareth .
Les monothéismes abrahamiques s' appuient sur deux livres saints : la Bible et le Coran .
La chahada , ou le credo islamique , est la déclaration de foi en l' unité d' Allah et en la croyance de la nature prophétique de Mahomet .
