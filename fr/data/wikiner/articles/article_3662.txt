Alençon est une ville française , préfecture du département de l' Orne , située dans la région Basse-Normandie .
La ville se situe au sud du département de l' Orne et de la région Basse-Normandie .
Sa communauté urbaine inclut plusieurs communes situées dans le département de la Sarthe limitrophe .
Contrairement à une légende restée vivace localement ( et rapportée par erreur dans de nombreux ouvrages ) , ce ne sont pas les notables alençonnais qui ont refusé de voir la ligne Paris -- Brest passer par leur ville , bien au contraire .
La population s' accroît , des banlieues pavillonnaires apparaissent et s' étendent aux communes voisines , des immeubles modernes sortent de terre , le centre ville est remodelé , Alençon perd sa réputation jusqu' alors justifiée de petite ville très vieille France n' ayant pas changé depuis l' époque de Balzac .
Toutefois , Alençon s' illustre de façon remarquable dans le tourisme , vert ou culturel , d' autant plus que la ville est située entre les deux parcs naturels régionaux de Normandie-Maine et du Perche .
Alençon se trouve à 119 km de Caen , la capitale régionale , et à 161 de Rouen , alors que Mans n' est qu' à 54 km .
La ville se situe à mi-chemin entre Paris et Rennes ( à respectivement 192 km et 158 km ) .
Ainsi , la ville est devenue la jonction entre les deux parcs naturels régionaux de Normandie-Maine l' englobant ( voir carte ) , et du Perche l' avoisinant à l' est .
Selon le classement établi par l' Insee , Alençon est une commune urbaine .
Alençon se situe précisément sur la limite du Bassin parisien et du Massif armoricain .
Alençon est sous climat océanique .
Sa région marque la transition entre le climat océanique de Bretagne ( climat océanique du littoral , humide ) et le climat océanique du Bassin Parisien ( davantage continental ) .
En effet , le climat océanique du Bassin Parisien est moins net que celui de Bretagne , le contraste été/hiver est plus marqué .
Ainsi , à Alençon , l' amplitude thermique est prononcée : les hivers sont un peu plus froids et les étés un peu plus chauds qu' à Cherbourg ou Saint-Malo par exemple , bien que le climat reste indubitablement océanique .
L' hygrométrie à Alençon s' élève à 82 % .
Alençon est chef-lieu de trois cantons :
Ces trois cantons appartiennent à l' arrondissement d' Alençon dont Alençon est le chef-lieu .
L' actuel député se nomme Yves Deniaud ( UMP ) .
L' âge du bronze est aussi représenté à Cerisé avec l' existence de cercles funéraires .
Lorsque les archéologues font le bilan des données , les sites se concentrent sous la forme de deux couronnes entourant Alençon .
Ces deux noms de personnes contiennent deux suffixes gaulois connus par ailleurs -- ( a ) nti- comme dans Bregenz ou ( o ) nti- comme dans Besançon ( jadis Vesontio ) .
Alençon s' est développée à l' origine dans un méandre de la rivière Sarthe , soit l' actuel quartier de Montsort .
À cette période , Alençon est dans la Seconde Lyonnaise mais , à partir des années 380 , cette province est coupée en deux .
Les capitales sont respectivement Tours et Rouen , sous l' autorité de deux évêques , anciens militaires : Martin et Victrice .
Saint Victrice entretient des relations soutenues avec Ambroise de Milan .
Des reliques des deux saints sont amenées à Sées cette même année , consacrant la cathédrale .
Il faut en effet empêcher les invasions des Saxons par la mer .
Il est divisé en quatre centenae dont une à Alençon pour chef-lieu administratif : pagus novaciensis avec Neuvy-au-Houlme , pagus saginsis avec Sées , pagus corbonnensis avec Corbon , pagus alencionnensis avec Alençon .
La Sarthe est la limite entre les évêchés du Mans et de Sées .
Alençon est le chef-lieu d' une centenie mérovingienne puis d' une vicarie carolingienne , qui est une division équivalente .
En attendant de revendiquer l' évêché du Mans , des pactes sont passés avec les nobles du secteur , élargissant l' orbite normande .
La famille de Bellême est la plus importante autorité sur la marche méridionale de la Normandie , de Mortagne à Domfront .
Alençon est dès lors dans une position avantageuse .
Première ville de Normandie acquise aux idées calvinistes , celle-ci devient rapidement un foyer de la Réforme , au point que , en 1530 , un réformé allemand qualifiait Alençon de " petite Allemagne " .
Sous le règne d' Henri IV , le maréchal de Biron l' assiégea à la tête de l' armée royale ; son artillerie y fit un dégât considérable , et les ligueurs furent contraints à capituler .
En 1605 , Henri IV engagea la ville et le duché au duc Frédéric I er de Wurtemberg , à qui Marie de Médicis le racheta en 1613 .
Alençon emploiera , à l' apogée de son art , jusqu' à huit mille dentellières .
Cette forte émigration des forces vives de l' économie alençonnaise , occupée dans la dentelle -- dont certains catholiques ne voulant pas perdre leur emploi -- et l' imprimerie , fuiront vers l' Angleterre , les Pays-Bas ou les îles Anglo-Normandes , laissant la ville exsangue .
La Révolution occasionna moins d' épreuves que dans beaucoup d' autres endroits .
La famille Malassis fondera une grande lignée d' imprimeurs .
Alençon fabrique à cette époque , une sorte de toile appelée " fleuret " ou " blancard " .
L' ouverture de l' A28 , entre Alençon et Le Mans en juin 2001 , et entre Alençon et Rouen en octobre 2005 , a permis , en outre , de désenclaver la ville .
Alençon , contrairement à beaucoup de préfectures , n' est pas le siège de l' évêché , puisque celui-ci est à Sées .
L' Union sportive alençonnaise 61 fait évoluer une équipe de football en CFA2 ( équivalent à la 5 e division nationale ) .
Ainsi Alençon se trouve à un carrefour routier duquel partent des routes vers Caen , Rouen , Dreux , Paris , Chartres , Orléans , Le Mans , Laval , Fougères , Rennes , Le Mont-Saint-Michel et Saint-Malo .
Ainsi le trajet Alençon -- Argentan -- Caen par autoroute sera possible dès 2010 lors de l' ouverture intégrale de l' A 88 ( une partie de l' autoroute est en effet en service , une deuxième en construction et une dernière en voie express en cours de mise aux normes autouroutières ) .
Ailleurs , son tracé , limitrophe à la Haute-Normandie et à la région Centre , pour lesquelles , elle n' a aucun intérêt économique , freine son élargissement en cohérence avec le reste .
Alençon dispose d' une gare SNCF , la gare d' Alençon , située sur la ligne Caen-Alençon-Le Mans-Tours .
Des liaisons quotidiennes vers les gares de Caen , du Mans , de Tours et de Saint-Pierre-des-Corps sont donc proposées .
Les meilleurs temps de parcours permettent de faire Alençon -- Paris-Montparnasse en 1 h 40 , Alençon -- Le Mans peut se faire en 31 minutes .
Alençon -- Caen se fait en 1h15 .
Alençon possède une antenne de l' université de Caen Basse-Normandie , une des cinq antennes que l' université dispose dans toute la région Basse-Normandie , comme à Cherbourg-Octeville , Saint-Lô , Lisieux ou Vire .
Alençon dispose de 6 collèges ( 4 publics et 2 privés ) .
Alençon dispose de 7 lycées ( tous types confondus ) .
En tant que chef-lieu de département , Alençon jouit de nombreux enseignements variés
Alençon dispose de 11 écoles primaires publiques réparties dans tous les quartiers de la ville , auxquelles il faut rajouter 5 écoles primaires privées , portant ainsi le total à 16 établissements primaires .
Sont organisés annuellement à Alençon :
" Alençon n' est pas une ville qui affriande l' étranger , elle n' est sur le chemin d' aucune capitale , elle n' a pas de hasards ; les marins qui vont de Brest à Paris ne s' y arrêtent même pas. " ( Honoré de Balzac , La Vieille Fille )
Unités ayant été stationnées à Alençon :
Alençon s' ouvre à l' international depuis plus de 40 ans , elle s' est notamment jumelée avec trois villes étrangères ( deux européennes et une africaine ) .
Alençon est jumelée avec les villes suivantes :
