En 1973 , Feuguerolles-sur-Orne ( 448 habitants en 1968 ) fusionne avec Bully ( 118 habitants ) située au sud de son territoire et qui garde le statut de commune associée .
La commune résultante prend le nom de Feuguerolles-Bully .
