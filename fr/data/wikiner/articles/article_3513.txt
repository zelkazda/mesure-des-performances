Elle est localisée dans la région naturelle du Warndt et dans le bassin de vie de la Moselle-est .
On l' appelle communément la capitale de Moselle-Est , car il s' agit en terme d' habitants de la plus grande ville de l' ancien bassin houiller .
Il s' agit , après Metz , Thionville et Montigny-lès-Metz , de la 4 e ville du département de la Moselle .
La ville de Forbach se situe dans la dépression du Warndt , une demi-boutonnière verte au cœur d' un espace fortement industrialisé .
La gare de Forbach accueille le TGV Est qui relie Paris à Francfort .
L' autoroute A 320 est tangente au territoire de la commune , la plaçant à 45 mn de Metz , 1h15 mn de Strasbourg ou 1h05 de Nancy .
Forbach est desservie par 4 sorties d' autoroute .
Forbach est situé dans une région habitée depuis fort longtemps .
Le nom Forbach est germanique et décrit la situation : un ruisseau , près d' une forêt .
À l' époque gallo-romaine , la grande route militaire de Metz à Mayence passait au sud-est de la forêt du Warndt , par la région de Forbach .
, en l' occurrence Johann Fischart , écrivain satirique prolifique , surnommé le Rabelais allemand , a œuvré comme bailli au château de Forbach où il séjourna de 1583 à 1590 .
La chapelle Sainte Croix fut dévastée au même moment .
À partir de 1775 , elle vécut dans son château de Forbach y entretenant une véritable cour .
En 1738 naît à Forbach Jean-Nicolas Houchard dont le nom est gravé sous la voûte de l' Arc de Triomphe à Paris .
La ligne de chemin de fer entre Metz et Forbach est ouverte en 1851 , la jonction avec Sarrebruck un an plus tard .
Les Wendel firent démarrer l' industrie houillère à Petite-Rosselle , puis à Forbach et Stiring-Wendel , où ils développent la plus puissante usine sidérurgique sous le Second Empire , usine qui périclite toutefois après 1870 .
Ce n' est que le 14 mars 1945 , après de très durs combats , que Forbach sera délivrée par les hommes du 276 e régiment de la 70 e division d' infanterie américaine .
Comme tout le nord de la Moselle , Forbach doit son développement à l' industrialisation de la région .
Cependant , dans les années 1990 , Forbach à son tour a vu sa population diminuer fortement , perdant environ 15 % de sa population entre 1990 et 1999 .
D' après les derniers chiffres avancés de l' INSEE , Forbach compterait 22400 habitants environ , ce qui se traduit par une tendance à la stagnation après une forte baisse , signe d' un renouveau peut-être pour la décennie à venir .
La ville possède d' ailleurs une antenne de la Chambre de commerce et d' industrie de la Moselle .
Bref , Forbach tend à rester un pôle d' ordre commercial et administratif , , .
L' avenue de Spicheren , en pleine mutation aussi , qui a accueilli la deuxième maison du département avec également un projet immobilier en finalisation .
Comme toutes les villes de sa taille , Forbach n' échappe pas à de nombreux projets immobiliers , de loisirs ainsi que de services que ce soit dans le privé ou dans le public .
Néanmoins , Forbach va connaître sur les exercices 2010 -- 2011 de grands chamboulements et ce , sur plusieurs niveaux .
Le site sera véritablement comblé lors de la mise en eau de la carrière centrale du nord de Forbach .
Sur le plan commercial , Forbach et son agglomération ne sont pas en reste .
Enfin , sur une commune voisine , à Œting , un Super U doté d' une station service va s' installer dans le courant de l' année 2010 .
La proximité immédiate de l' Allemagne pose un problème récurrent concernant les fréquences , c' est la raison pour laquelle on dit communément que la région est de la France est une terre pauvre en radios nationales .
L' Union sportive de Forbach est un club français de football fondé en 1909 .
