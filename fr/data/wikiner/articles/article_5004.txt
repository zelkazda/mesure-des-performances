La ville est située sur le plateau de Langres qui permet le passage du Bassin parisien à la vallée de la Saône , près de la source de la Marne .
Les quatre lacs réservoirs : lac de la Liez , lac de la Vingeanne , lac de la Mouche et lac de Charmes ont été construits afin d' alimenter en eaux le Canal de la Marne à la Saône .
Les Lingons sont un des peuples importants de la Gaule .
À l' époque gallo-romaine , Langres est la capitale des Lingons et s' appelle alors Andemantunnum .
Certains de ses évêques furent des commissaires impériaux ( missi dominici ) sous Charlemagne .
Langres passe sous tutelle royale .
Au XIX e siècle , une citadelle à la Vauban vient étendre le domaine fortifié .
En 1884 , la ville décide de marquer le centième anniversaire du décès de Denis Diderot .
Au cours de la Seconde Guerre mondiale , est libérée le 13 septembre par les troupes débarquées en Provence .
En 1972 Langres absorbe la commune de Corlée .
Des expositions ponctuelles rappellent le rayonnement considérable du diocèse de Langres pendant de nombreux siècle .
Œuvre de Bartholdi de 1884 .
Une discrète plaque en pierre signale la naissance de Diderot sur la façade du n o 6 ( au niveau du premier étage ) place Diderot .
Don du roi Louis XV , construit à l' emplacement de l' ancienne maison de ville , il fut plusieurs fois ravagé par des incendies , dont le plus grave fut celui de 1892 .
Actuellement en travaux de rénovation , il accueillera prochainement un musée à Denis Diderot .
Ancien collège des Jésuites où Denis Diderot fit ses études .
Ce cadre inspira Denis Diderot dans plusieurs de ses écrits , en particulier dans sa lettre à Sophie Volland du 3 août 1759 .
Commune associée : Corlée .
Communes limitrophes ( canton de Langres ) Balesmes-sur-Marne , Champigny-lès-Langres , Chatenay-Mâcheron , Humes-Jorquenay , Peigney , Perrancey-les-Vieux-Moulins , Saint-Ciergues , Saint-Vallier-sur-Marne et Saints-Geosmes .
