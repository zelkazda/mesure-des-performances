Tellus est plus fréquent que Terra dans les inscriptions .
L' association [ réf. nécessaire ] de Cérès et de Tellus remonte très haut dans l' antiquité romaine .
Cicéron attribue aux deux divinités des fonctions distinctes : à Cérès la croissance , à Tellus le sol .
Peu à peu , Tellus perd de son crédit religieux [ réf. nécessaire ] et finit par être éliminée au profit de Cérès , sans cesser d' ailleurs complètement d' être honorée avec elle .
À la même époque , Tibulle ne nomme que Cérès et passe Tellus sous silence .
Dans le même texte , Servius nous apprend que Tellus est invoquée pour la célébration des mariages .
Le sacrifice correspondant de la truie dite praesentanea , c' est-à-dire célébrée devant le mort avant la cérémonie funèbre , était tout d' abord offert à Tellus seule ; plus tard à Cérès conjointement avec elle , sous l' influence des idées grecques .
Tellus , dit Ovide , pourvoit les sorciers d' herbes qui ont des vertus surnaturelles ; elle figure à ce titre dans le tableau que le poète trace des pratiques de l' enchanteresse Médée à côté des sombres puissances , parmi les éléments déchaînés .
Nous possédons deux fragments en vers iambiques que les manuscrits attribuent à Antonius Musa , le médecin célèbre de l' empereur Auguste , mais qui ne sentent guère ni la langue ni le goût de cette époque ; même datés de deux siècles plus tard , ces morceaux sont les témoignages curieux d' un culte superstitieux de la Tellus antique .
La poésie philosophique de Lucrèce , sans doute sous l' influence de ses modèles grecs et plus particulièrement d' Empédocle , a tiré un parti assez heureux de la personnification théogonique de Tellus .
Mais en conservant le langage de la poésie religieuse , Lucrèce reste en communication intime avec l' opinion populaire , sans sacrifier à l' illusion des personnifications mythiques .
Dans les provinces , seule la Dacie nous offre des inscriptions assez nombreuses en son honneur .
Un passage de Tacite nous montre comment les soldats romains identifiaient certaines divinités étrangères avec la Terra Mater de leur patrie .
Une médaille frappée en l' honneur d' Antonin le Pieux la représente nue , suivant des yeux un aigle qui enlève l' empereur dans le ciel .
