IPv6 est un protocole réseau sans connexion de la couche 3 du modèle OSI .
IPv6 est le successeur du protocole IPv4 , ce dernier formant la base du réseau Internet .
IPv6 est similaire à IPv4 dans son fonctionnement , mais s' en distingue par son espace d' adressage plus étendu ainsi qu' un certain nombre d' améliorations .
Le protocole IPv4 permet d' utiliser un peu plus de quatre milliards d' adresses différentes pour connecter les ordinateurs et les autres appareils reliés au réseau .
Au début d' Internet , dans les années 1970 , il était pratiquement inimaginable qu' il y aurait un jour suffisamment de machines sur un unique réseau pour que l' on commence à manquer d' adresses disponibles .
Les premières grandes organisations connectées à Internet se sont vues attribuer 16 millions d' adresses par exemple .
L' attribution des adresses est rendue plus efficace et tient compte des besoins réels , tout en permettant un certain niveau d' agrégation , nécessaire au bon fonctionnement du routage sur Internet , ces deux principes étant antagonistes .
En dépit de ces efforts , l' épuisement des adresses IPv4 publiques est inévitable .
C' est la raison principale du développement d' un nouveau protocole Internet .
IPv6 améliore aussi certains aspects du fonctionnement d' IP , à la lumière de l' expérience acquise .
Au début des années 1990 , il est devenu clair que le développement d' Internet allait aboutir à l' épuisement des adresses disponibles .
Le fonctionnement d' IPv6 est très similaire à celui d' IPv4 .
Les protocoles TCP et UDP sont pratiquement inchangés .
Une adresse IPv6 est longue de 128 bits , soit 16 octets , contre 32 bits pour IPv4 .
Certains préfixes d' adresses IPv6 jouent des rôles particuliers :
Le scope d' une adresse IPv6 consiste en son domaine de validité et d' unicité .
Toutes les interfaces où IPv6 est actif ont au moins une adresse de scope link-local ( fe80::/10 ) .
Dans le bloc 2000::/3 qui représente 1/8 e de l' espace d' adressage disponible en IPv6 , on peut donc créer 2 45 , soit 35 milliards de réseaux d' entreprises .
L' en-tête du packet IPv6 est de taille fixe à 40 octets , tandis qu' en IPv4 la taille minimale est de 20 octets , des options pouvant la porter jusqu' à 60 octets , ces options demeurant rares en pratique .
Il est possible qu' un ou plusieurs en-têtes d' extension suivent l' en-tête IPv6 .
En IPv4 , les routeurs qui doivent transmettre un paquet dont la taille dépasse le MTU du lien de destination ont la tâche de le fragmenter , c' est-à-dire de le segmenter en plusieurs paquets IP plus petits .
La MTU minimale autorisée pour les liens a également été portée à 1280 octets ( contre 576 pour l' IPv4 ) .
Comme pour IPv4 , la taille maximale d' un paquet IPv6 hors en-tête est de 65535 octets .
L' en-tête IPv6 peut être suivi d' un certain nombre d' en-tête d' extensions .
Il permet également de découvrir les routeurs et les préfixes routés , le MTU , de détecter les adresses dupliquées , les hôtes devenus inaccessibles et l' autoconfiguration des adresses et éventuellement les adresses des serveurs DNS récursifs .
Il est basé sur ICMPv6 .
Pour pallier cet inconvénient , il est possible d' utiliser des adresses temporaires générées de façon pseudo-aléatoire et modifiées régulièrement ou bien d' utiliser un service d' attribution automatique des adresses IPv6 par un serveur , de façon similaire à ce qui existe pour IPv4 , avec DHCPv6 .
Le multicast , qui permet de diffuser un paquet à un groupe , fait partie des spécifications initiales d' IPv6 .
Il n' y a plus d' adresse broadcast en IPv6 , celle-ci étant remplacée par une adresse multicast spécifique à l' application désirée .
Par exemple , l' adresse ff02::101 permet de contacter les serveurs NTP sur un lien .
Le protocole Multicast Listener Discovery permet d' identifier les groupes actifs sur un segment , à l' instar d' IGMP pour IPv4 .
Le mécanisme utilisé pour construire le nom de domaine inverse est similaire à celui employé en IPv4 , à la différence que les points sont utilisés entre chaque nibble ( groupe de 4 bits ) , ce qui allonge le domaine .
La résolution inverse peut être utilisée par des systèmes de contrôle d' accès ainsi que par des outils de diagnostic comme traceroute .
Le recours à la traduction d' adresse est découragé en IPv6 pour préserver la transparence du réseau , son utilisation n' est plus nécessaire pour économiser des adresses .
IPv6 prévoit des mécanismes pour conserver une même adresse IPv6 pour une machine pouvant être connectée à des réseaux différents , tout en évitant autant que possible le routage triangulaire .
La transition consiste à doter les hôtes IPv4 d' une double pile , c' est-à-dire à la fois d' adresses IPv6 et IPv4 .
La manière la plus simple d' accéder à IPv6 est lors de l' abonnement de choisir un FAI qui offre de l' IPv6 nativement , c' est-à-dire sans recours à des tunnels .
À défaut , et pendant une phase de transition , il est possible d' obtenir une connectivité IPv6 via un tunnel .
Les paquets IPv6 sont alors encapsulés dans des paquets IPv4 , qui peuvent traverser le réseau du FAI jusqu' à un serveur qui prend en charge IPv6 et IPv4 , et où ils sont décapsulés .
Ceci permet de réaliser le multihoming de la même façon qu' en IPv4 .
Lorsque c' est possible , les échanges se font nativement , avec IPv4 et IPv6 qui coexistent sur les mêmes liaisons .
Pour autant que les routeurs soient mis à jour pour la prise en charge d' IPv6 , il n' est pas nécessaire de disposer d' une infrastructure séparée pour IPv6 , les routeurs traitant à la fois le trafic IPv4 et IPv6 .
Depuis 2004 , l' ICANN accepte d' intégrer des serveurs de noms avec des adresses IPv6 dans la zone racine .
D' autre part , en 2010 , 228 des 283 domaines de premier niveau disposent d' au moins un serveur avec une adresse IPv6 .
La taille des paquets DNS en UDP est limitée à 512 octets , ce qui peut poser des problèmes au cas où la réponse est particulièrement volumineuse .
La norme prévoit alors qu' une connexion TCP est utilisée , mais certains pare-feux bloquent le port TCP 53 et cette connexion consomme plus de ressources qu' en UDP .
Les protocoles TCP et UDP fonctionnent comme en IPv4 .
Le pseudo en-tête utilisée pour le calcul du code de contrôle est cependant modifié et inclut les adresses IPv6 source et destination .
Il n' y a pas de support IPv6 dans la version DOCSIS 2.0 .
Une version dite DOCSIS 2.0 + IPv6 existe cependant et ne nécessite qu' une mise à jour micrologicielle .
Le BRAS doit également supporter IPv6 .
En général , les équipements qui travaillent sur la couche de liaison , comme les commutateurs ethernet , n' ont pas besoin de mise à jour pour le support d' IPv6 , sauf éventuellement pour le contrôle et la gestion à distance .
Les systèmes d' accès doivent généralement être revus pour IPv6 , les outils d' attribution des adresses et les bases de données d' enregistrement des adresses notamment .
ont été mis à jour pour la prise en charge d' IPv6 , et c' est également le cas d' autres systèmes embarqués , tels que Symbian , QNX , Windows Mobile ou Wind River .
Certains CPE restent cependant encore incompatibles avec IPv6 , ce qui rend nécessaire la configuration de tunnels .
Les applications reliées au réseau doivent être modifiées pour être compatibles avec IPv6 .
Afin de faciliter le support d' IPv6 dans les logiciels , des outils apparaissent , comme IPv6 CARE par exemple .
Wanadoo ( aujourd'hui Orange ) a proposé en juin 2005 une expérimentation IPv6 pour les particuliers , cette expérimentation est aujourd'hui clôturée .
Depuis décembre 2007 , l' opérateur Free.fr propose une connectivité IPv6 à ses utilisateurs dégroupés .
En 2009 , IPv6 est présent dans leur offre pour les entreprises .
En mai 2009 , Orange Business Services a déployé l' IPv6 sur son réseau MPLS IP VPN , à destination des entreprises .
En Europe , depuis 2003 , le réseau de recherche universitaire pan-européen GÉANT , interconnectant les réseaux nationaux de la recherche et de l' enseignement , utilise une double pile ( IPv4 + IPv6 ) .
En avril 2010 , 27 % des LIR ont obtenu un bloc d' adresse IPv6 , et 8 % ont atteint le niveau le plus élevé de quatre étoiles .
Aujourd'hui , de nombreux serveurs web acceptent les connexions via IPv6 .
Google est par exemple accessible en IPv6 depuis mars 2008 , c' est également le cas de YouTube et Facebook depuis 2010 .
En 2009 , plusieurs opérateurs mondiaux ont commencé à déployer IPv6 , , .
Aux États-Unis , Comcast a commencé en 2010 des tests de diverses technologies autour d' IPv6 , sur son réseau de production , en prévision du déploiement définitif et de l' épuisement des adresses IPv4 .
IPv6 s' impose parfois comme unique moyen d' interconnexion avec les terminaux mobiles itinérants en Asie ; il le sera aussi rapidement en Europe quand les anciennes solutions d' interconnexion basées sur l' adressage GSM devront être remplacées par des solutions IP .
De plus , l' évolution des usages mobiles allant vers une connectivité IP permanente , il deviendra alors impossible d' adresser un nombre important de terminaux mobiles avec un adressage IPv4 .
Un rapport de l' OCDE publié en avril 2010 indique que le niveau d' adoption d' IPv6 est encore faible , avec de 0,25 à 1 % des utilisateurs qui font usage d' IPv6 .
À la fin de l' année 2009 , 1851 numéros d' AS IPv6 étaient visibles , ce nombre ayant doublé en deux ans .
Les freins au déploiement d' IPv6 sont , entre autres , les suivants :
Concernant le développement du support IPv6 chez les fournisseurs de contenu et d' accès , on compare parfois le problème à celui de l' œuf et de la poule :
Concernant les problèmes rencontrés par les FAI qui ont déployé IPv6 :
En 2010 , la prise en charge d' IPv6 n' est pas encore un critère de choix pour le consommateur final .
Les entreprises sont cependant plus attentives à ce problème et évitent d' investir dans des équipement qui pourraient s' avérer incompatibles avec IPv6 .
L' accès aux serveurs IPv6 depuis des clients IPv4 présente également un défi technique .
