Il étudie à Paris au Collège des Quatre-Nations , bien que sa famille n' appartienne pas à strictement parler à l' aristocratie .
Les cours de mathématiques de Pierre Charles Le Monnier le décident à se détourner de la médecine .
Coulomb travaille huit années à diriger les travaux , y contracte des fièvres tropicales , mais réalise aussi plusieurs expériences sur la résistance des maçonneries et la tenue des murs d' escarpe ( soutènements ) , qui lui sont inspirées par les idées de Pieter van Musschenbroek sur le frottement .
Coulomb y emploie le calcul différentiel pour étudier la flexion des poutres , la poussée des remblais sur les murs de revêtement , l' équilibre des voûtes en maçonnerie .
