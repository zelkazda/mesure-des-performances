La commune se situe entre Vire ( 20 kilomètres ) et Aunay-sur-Odon ( 7,5 kilomètres ) , aux confins du Bessin , de la Suisse normande et du Bocage virois .
Danvou-la-Ferrière se trouve dans un paysage de collines , de bois , de haies bocagères , de rivières ( haute vallée de la Druance ) et de ruisseaux .
En 1972 , Danvou ( 137 habitants en 1968 ) fusionne avec La Ferrière-Duval ( 49 habitants ) qui conserve dans un premier temps le statut de commune associée .
À la création des cantons , Danvou est chef-lieu de canton .
Danvou a compté jusqu' à 378 habitants en 1831 .
Cette même année , La Ferrière-Duval égalait son maximum démographique déjà atteint en 1821 avec 180 habitants .
