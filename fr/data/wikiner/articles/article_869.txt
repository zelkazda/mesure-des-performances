Son siège social se situe à Redmond , près de Seattle , et ses meilleures ventes sont le système d' exploitation Windows et la suite bureautique Microsoft Office .
Si bien qu' un observateur note même que la mission originale de Microsoft d' avoir " un ordinateur sur chaque bureau et dans chaque maison , tournant sur Windows " est aujourd'hui pratiquement accomplie .
Microsoft participe aussi dans d' autres secteurs d' activité , comme la chaîne câblée américaine MSNBC , le portail web MSN , les périphériques informatiques ( claviers , souris ) , et les produits de divertissement domestique comme la Xbox et le Zune .
L' introduction en bourse de la société , et l' envolée du prix des actions qui s' ensuivit , a fait quatre milliardaires et environ 12000 millionnaires parmi les employés de Microsoft .
Cette entreprise est surtout connue pour ses logiciels , comme les systèmes d' exploitation MS-DOS et Windows , la suite bureautique Microsoft Office , ses outils de développement , ses jeux vidéo , également pour divers produits matériels ( périphériques pour PC , consoles de jeux Xbox , baladeur numérique Zune ) , et pour ses services Internet ( regroupés sous le nom Windows Live ) .
Microsoft domine depuis plusieurs années le marché des systèmes d' exploitation grand-public .
Son système d' exploitation Windows , régulièrement réédité , s' est imposé comme un standard dans le domaine informatique .
Depuis le 27 mai 2010 , Microsoft est la seconde capitalisations boursières du NASDAQ , derrière Apple .
La marque Microsoft ( en fait , originalement , Micro-Soft : le trait d' union disparaîtra plus tard ) fut déposée le 26 novembre 1976 .
À titre indicatif le prix de vente de l' Altair 8800 étant de 397 dollars , la licence de Microsoft en représentait donc 8,8 % .
Avant la sortie de MS-DOS en 1981 , Microsoft poursuivit son développement en produisant divers compilateurs de langages de programmation comme Fortran ou COBOL .
En 1980 , IBM s' apprêtant à lancer l' IBM PC , a demandé son BASIC à Microsoft .
IBM a , par ailleurs , demandé à la société Digital Research , dirigée par Gary Kildall de lui fournir une version de son système d' exploitation CP/M .
IBM se tourna alors vers Microsoft , et voulut sous-traiter CP/M pour l' IBM PC .
Le contrat avec Microsoft ne le permettant pas , celui-ci dépensa 25000 $ en décembre 1980 pour une licence non exclusive pour un système d' exploitation , disponible à un stade expérimental , clone de CP/M , le QDOS .
En mai 1981 , Microsoft engagea Tim Paterson pour porter QDOS sur l' IBM PC .
IBM vit ainsi sauvé son projet d' IBM PC , mais au prix , qu' elle ignora , de la perte de sa position dominante : cet accord va permettre de réaliser des clones , et surtout , à IBM d' empocher des redevances sur le MS-DOS pour les correctifs qu' elle y a apportés ( débogage ) .
IBM avait détenu jusqu' à 66 % du marché des mainframes propriétaires ; sa part du marché des PC ne dépassa jamais un maximum de 21 % , atteint vers 1983 , puis a décliné pour placer ce constructeur derrière Dell et Compaq ( aujourd'hui intégrée par Hewlett-Packard ) , situation devenue marginale , inimaginable en 1981 .
Microsoft a acheté pour 50000 dollars le logiciel qui va ériger son empire , même si elle a dû en compléter le développement pour répondre au cahier des charges d' IBM .
Celui-ci fut édité sous le nom d' IBM PC-DOS 1.0 lors de l' introduction des IBM PC sur les marchés anglophones , le 12 août 1981 .
Étant plus léger , moins cher et rendu plus disponible que ses deux concurrents , il devint rapidement le système d' exploitation installé d' office sur les IBM PC , puis plus tard des Compatible PC .
Comme pour le BASIC , Microsoft s' est réservé le droit de vendre des licences à d' autres constructeurs sous le nom de MS-DOS .
Avec l' essor des Compatible PC dès le milieu des années 1980 ( de Texas Instruments , Compaq , Seiko Epson , Thomson , Amstrad … ) , MS-DOS s' imposa rapidement et devient de facto la plate-forme de référence professionnelle , et un monopole , selon les points de vue .
En 1987 , des milliers de constructeurs de compatible PC existaient dans le monde , et tous sans exception avaient un point de passage obligé qui était le système d' exploitation de Microsoft , le plus performant de tous , dans un souci , crucial pour le monde professionnel , d' unité , de standardisation , et de portabilité de tous les ordinateurs compatible PC .
C' est ensuite grâce à Windows , que Microsoft s' imposa comme le principal acteur du secteur de la micro informatique .
Cependant , Bill Gates a souvent été accusé d' avoir " volé " le concept de Windows à Apple , qui avait produit peu avant le premier Macintosh , possédant lui aussi une interface graphique similaire à celle de Windows .
Mais cette accusation est infondée puisque l' interface graphique du Macintosh est elle même inspirée par le Xerox Alto qui est le premier ordinateur personnel à avoir été doté d' une interface graphique .
D' abord simple interface graphique pour MS-DOS , Windows est devenu beaucoup plus tard un système d' exploitation à part entière , après quelques versions intermédiaires .
Quelques coups de stratégie de marketing ne sont pas étrangers à ce succès , comme l' ajout de trois touches " Windows " sur les claviers afin de marquer celui-ci dans l' esprit du consommateur comme " étant fait pour Windows " et marginaliser ainsi le concurrent potentiel OS/2 développé par IBM , et co-développé initialement par Microsoft et IBM , jusqu' au divorce officiel entre les deux sociétés en septembre 1991 .
Selon Microsoft , un soin particulier a également été apporté aux questions d' ergonomie , et en particulier à la question des polices de caractères typographiques , dès les versions 3.0 ( Adobe Type Manager ) et 3.1 ( TrueType ) de Windows .
Bien des années plus tard , Microsoft affirmera considérer son avance sur le plan de l' ergonomie comme l' atout qui permettra à Windows de survivre face à la concurrence libre de Linux et de KDE / GNOME .
De fait , Microsoft consacre une part très importante de son budget aux questions d' ergonomie : un service observe toutes les hésitations d' utilisateurs novices , pour rendre les menus plus clairs , démarche fastidieuse et rarement réalisée sur des logiciels gratuits réalisés par des bénévoles .
Windows est alors devenu le standard micro-informatique de facto solidement soutenu par l' effet réseau indirect de milliers de logiciels et de périphériques matériels spécifiques à Windows qui ont nécessité des milliards de journées/hommes de développement .
L' élaboration d' un produit capable de rivaliser avec Windows impliquerait de disposer , comme Microsoft , de revenus réguliers pendant les années nécessaires au développement d' un tel système .
Or , le temps que celui-ci soit développé , Microsoft aurait déjà pris de l' avance , et éventuellement modifié les standards .
La mise à mort d' OS/2 par Microsoft ( contre toute attente ) , avec le consentement d' IBM puisque Windows 95 contient des parties d' OS/2 donc génère des redevances pour IBM , constitue un avertissement qui décourage toute velléité de tenter de concurrencer Windows .
La société Be propose tout de même BeOS , orienté d' emblée dans la gestion de la vidéo , et tout aussi ergonomique que Windows : ce système d' exploitation ne décollera jamais vraiment hors d' un cercle de passionnés .
Et Be intentera d' ailleurs un procès antitrust contre Microsoft pour abus de position dominante , qui s' achèvera par un accord financier à l' amiable entre les deux sociétés .
Microsoft n' était pas la première à proposer une interface graphique pour les PC .
Avant eux , la société Digital Research avait développé un produit très comparable à l' interface du Macintosh , le GEM ( Graphical environment manager ) .
Un procès intenté par Apple à l' encontre de Digital Research s' était traduit par une condamnation de cette société à qui le jugement avait imposé d' enlever toutes les caractéristiques ressemblant au Mac OS d' Apple dans son interface graphique , la rendant ainsi économiquement peu plaisante .
La version 1.0 de Windows , rudimentaire -- les fenêtres ne peuvent même pas se recouvrir -- , n' inquiète pas sérieusement Apple , qui ne réagit pas .
La version 2.0 est une concurrence plus sérieuse , et Apple intente un nouveau procès , cette fois-ci contre Microsoft .
Apple perdit définitivement son procès contre Microsoft en appel en 1994 .
Cet accord comprenait une prise de participation temporaire de Microsoft dans le capital d' Apple ( à hauteur de 150 millions de dollars soit 6 % du capital de la pomme ) , et l' obligation pour Microsoft de développer Internet Explorer et Microsoft Office pour Mac OS au moins jusqu' en 2002 .
En échange , Apple abandonnait ses poursuites .
Un facteur important de l' adoption généralisée de Windows a été son rôle d' interface non seulement graphique , mais également de pilotes .
Sous MS-DOS , chaque éditeur de logiciel devait développer individuellement la gestion de tout le panel des milliers de périphériques compatible PC existants et à venir .
Tâche colossale que les éditeurs de logiciels n' ont plus à gérer sous Windows dans la mesure où celui-ci se charge de gérer lui-même en standard tous les pilotes de périphériques de l' univers compatible PC .
L' histoire de Microsoft ne se résume cependant pas à celle de Windows .
D' autres pans importants de l' activité de Microsoft ont permis sa croissance :
Microsoft a rapidement dominé tout le secteur de l' informatique personnelle et s' est imposé comme un acteur incontournable de ce secteur .
Cette situation lui vaut de nombreuses critiques sur sa position dominante , il apparaît donc que Microsoft ait actuellement les mêmes types de problèmes de position dominante que ceux qu' avait IBM avant 1980 :
Microsoft est l' un des plus importants éditeurs de logiciels au monde et , dans le même temps , la société est présente depuis ses débuts dans le matériel ; la branche matériel ne semble jamais avoir dépassé 10 % du chiffre d' affaires de Microsoft .
Cette famille de systèmes d' exploitation est le principal produit de la firme et a été le second agent de son phénoménal succès de Windows jusqu' à la version 3.0 de ce logiciel , qui atteint alors son seuil de rentabilité ) .
Windows est installé sur presque 90 % des ordinateurs personnels vendus dans le monde , et dégage actuellement 87 % de marge bénéficiaire .
Néanmoins , Windows perd petit à petit des parts de marché au profit de Mac OS X d' Apple .
Mais Windows reste aujourd'hui le produit le plus rentable de l' éditeur , suivi de près par la suite Microsoft Office .
À l' origine , c' était la suite bureautique de l' éditeur , composée de nombreux logiciels dont le traitement de texte Word , le tableur Excel , le logiciel de présentation PowerPoint , l' outil de communication et agenda Outlook et la base de données Access .
Office est un des logiciels les plus rentables de l' éditeur .
Une version d' Internet Explorer est disponible gratuitement pour les systèmes Mac OS d' Apple mais le développement de cette version a toutefois été arrêtée en 2003 .
Néanmoins , le navigateur perd des parts de marché depuis 2004 , avec l' arrivée d' autres navigateurs comme Mozilla Firefox , Google Chrome et Safari .
Le Lecteur Windows Media est aussi disponible pour les systèmes Mac OS d' Apple .
Un nouveau service communautaire a été lancé en 2006 , " Games for Windows " .
Cette gamme accueille plusieurs jeux comme Gears of War ou GTA IV .
Il s' agit d' un serveur web fourni gratuitement par Microsoft depuis la version 4 de Windows NT .
Sa fourniture gratuite a sonné le glas de nombreux produits concurrents payants ( le principe de la concurrence , comme Netscape , était en effet de vendre le serveur et de fournir gratuitement le seul client , comme le fait Adobe pour son logiciel Acrobat ) .
MSE est un logiciel antivirus fourni gratuitement par Microsoft depuis le 29 septembre 2009 .
Il est disponible en français et compatible avec Windows XP , Vista ( 32 et 64 bits ) et Windows 7 .
Visual Studio est la suite de développement de la firme , incluant divers éditeurs et compilateurs , essentiellement pour une version améliorée de BASIC nommée Visual Basic , ainsi que pour des implémentations de C++ et de C # , qui constitue la réponse de Microsoft au langage Java .
Il permet aussi de tirer parti des fonctionnalités du .NET Framework .
SQL Server est le système de gestion de base de données phare de Microsoft , co-développé avec Sybase jusqu' en 1994 .
Le coeur de SQL Server est utilisé dans d' autres produits de la marque tels que Exchange ou Active Directory .
Microsoft s' est lancé en 2001 dans ce secteur hautement concurrentiel , en sortant sa propre console de jeux vidéo , la Xbox .
Elle est remplacée au bout de 4 ans par la Xbox 360 .
Elles proposent toutes deux des centaines de jeux et un mode de jeu en ligne communautaire , le Xbox Live .
La Xbox 360 permet également de se connecter à MSN et d' utiliser d' autres services de Microsoft .
La Xbox 360 possède environ 30 % des parts de marché sur le marché des consoles de jeux vidéo en février 2009 .
Le Zune est un baladeur numérique destiné principalement à écouter de la musique .
Il a été lancé en novembre 2006 pour concurrencer la suprématie de l' iPod aux États-Unis .
Microsoft fabrique également une ligne de périphériques divers pour PC ( souris , claviers , périphériques de jeu , volants … ) .
Actuellement l' activité internet de Microsoft est constitué en trois principaux produits :
Microsoft propose également un programme de formation et certification .
Aux États-Unis , MSNBC est une chaîne d' information en continu .
Il propose également DirectX , une API multimédia ( vidéo , son , réseau , etc . )
pour le développement d' application Windows ( principalement des jeux vidéo ) et aussi Silverlight qui permet de visionner des animations vectorielles ( faisaint ainsi concurrence à Adobe Flash Player ) , mais surtout des contenus multimédia intégrant de l' audio et de la vidéo .
D' après les comptes annuels , le chiffre d' affaires de Microsoft s' élevait en 2005 à 39,788 millions de dollars .
L' action valait 26.37 dollars le 19 juin 2010 pour une capitalisation boursière de 231.7 milliards de dollars , ce qui fait d' elle la seconde valeur technologique mondiale ( juste derrière Apple ) , à comparer par exemple aux 28.3 milliards de dollars de Dell .
La somme versée en dividendes aux actionnaires de Microsoft en 2004 approche donc les 28 milliards USD .
Cependant , cette décision prise au niveau fédéral ne deviendra exécutoire que si Microsoft décide de ne pas faire appel .
Conscient du problème , le W3C ( le consortium qui définit les standards technologiques du web ) est sorti de sa réserve , le 29 octobre 2003 , en demandant à l' Office américain des brevets un ré-examen de la validité du brevet 906 .
À l' appui de sa requête , Microsoft a adressé des " preuves d' antériorité " mettant en évidence , selon cette organisation , l' invalidité du brevet .
Le 24 mars 2004 , à la suite de quatre années d' enquête du commissaire européen à la concurrence Mario Monti , la Commission européenne rend sa décision dans le procès anti-trust qui l' oppose à la firme .
La décision rendue condamne la position monopolistique de Microsoft et ses pratiques illégales .
La commission inflige par ailleurs à Microsoft une sanction historique de 497.5 millions d' euros .
Microsoft devra notamment vendre une version de Windows sans le Lecteur Windows Media .
Un recours engagé par la firme ( 7 juin 2004 ) devant le Tribunal de première instance des Communautés européennes n' étant a priori non suspensif la somme de l' amende a été réglée le 1 er juillet 2004 .
Le 12 juillet 2006 , la commission prononce une nouvelle amende ( 280 millions d' euros ) contre Microsoft pour sanctionner le retard pris par le groupe à publier ses spécifications .
Le 17 septembre 2007 , le TPI confirme la première amende de 497.5 millions d' euros .
Le litige portait sur l' utilisation par Microsoft , dans son système d' exploitation Windows , d' une invention destinée à numériser la voix et protégée par un brevet dont AT & T était titulaire .
Comme Microsoft envoie directement aux sociétés fabricantes d' ordinateurs situées en dehors des États-Unis , l' intégralité des données composant le code de son système d' exploitation afin qu' elles puissent les copier sur leurs machines avant de les vendre , AT & T réclamait d' être dédommagée pour tous les logiciels , comprenant leur invention brevetée , qui avaient été exportés hors du territoire américain .
Les avocats de Microsoft ont reconnu la contrefaçon des brevets AT & T sur le territoire américain et rappelé que ce litige avait fait l' objet d' un accord amiable entre les parties en 2004 .
Par contre , ils se sont fermement opposés à l' idée selon laquelle Windows pourrait se voir reprocher une contrefaçon au niveau international , estimant qu' un code informatique ne constitue pas un composant en tant que tel tant qu' il n' est pas installé sur un ordinateur .
On ne peut selon eux reprocher à Microsoft d' exporter hors du territoire des États-Unis des composants informatiques protégés par brevet .
