L' équipe , qui comprend un gardien de but et cinq joueurs de champ , se déplace à l' aide de patins et manipule la rondelle à l' aide d' une crosse ( terme utilisé en France ) , également appelée bâton de hockey ( Canada ) et canne de hockey .
Cependant les origines du jeu moderne se trouvent en Amérique du Nord .
Le développement du jeu moderne eut lieu à Montréal au Québec .
Durant la même année , un second club fut créé à l' Université d' Oxford .
La Coupe Stanley fut attribuée en 1893 au champion amateur du Canada , les Montréal AAA .
À cette époque , Montréal regroupait une centaine d' équipes et des ligues existaient à travers tout le Canada .
Également en 1893 , des joueurs de Winnipeg eurent l' idée d' utiliser des équipements de cricket pour protéger les jambes des gardiens .
1893 fut également la date du premier match aux États-Unis entre l' Université Yale et l' Université Johns-Hopkins .
En 1908 , le patineur français Louis Magnus créa la Ligue internationale de hockey .
En 1909 est fondée la plus vieille équipe encore existante aujourd'hui : le Club de Hockey Canadien de Montréal .
1917 voit l' apparition de la Ligue nationale de hockey ( National Hockey League en anglais ) .
La LNH qui était descendue à six franchises pendant 25 ans s' ouvre à six nouvelles villes en 1967 .
Les règles internationales du hockey sur glace sont édictées par la Fédération internationale ( IIHF ) .
C' est également le règlement qui s' applique dans les championnats nationaux de la plupart des pays , en dehors de l' Amérique du Nord .
En effet , aux États-Unis et au Canada ( et bien que ces pays soient membres de l' IIHF ) , les ligues peuvent édicter leurs propres règles .
Ainsi la Ligue nationale de hockey ( LNH ) , ligue la plus importante , possède un règlement qui diffère quelque peu de celui de l' IIHF ( notamment au sujet des combats , totalement interdits dans le règlement international ) .
Les arbitres sont au nombre de deux ou trois ( voire quatre pour la LNH ) suivant les catégories et veillent au bon déroulement du match , et entre autres , au signalement des pénalités .
Ainsi , au début des années 1920 , dans la Ligue nationale de hockey , les joueurs de champ jouaient sans casque et ce n' est qu' en 1979 que le casque fut rendu obligatoire dans la LNH et ceci suite à un accident ayant causé le décès de Bill Masterton près de 10 ans plus tôt .
De même , l' usage de la jugulaire a été mis en exergue suite à un accident lors d' un match entre les Sabres de Buffalo et les Blues de Saint-Louis .
Le championnat le plus connu et le plus relevé est celui de la Ligue nationale de hockey ( LNH ) , joué en Amérique du Nord , dont le champion se voit attribuer la Coupe Stanley .
Les championnats européens de meilleur niveau sont ceux de Suède ( Elitserien ) , de Russie , de Finlande ( SM-liiga ) , de République tchèque ( Extraliga ) , de Suisse ( Ligue Nationale A ) et d' Allemagne ( Deutsche Eishockey-Liga ) .
Il existe également un championnat en France , la Ligue Magnus en étant la division d' élite .
Elle possède aussi une des plus vieilles et une des meilleures ligues de hockey sur glace européennes ( la Ligue nationale A ) et est ainsi considérée comme la huitième nation du hockey .
Dans la Ligue nationale de hockey , le club le plus titré est celui des Canadiens de Montréal avec 24 titres .
En Europe , parmi les plus grands clubs , citons le CSKA Moscou ( 32 titres nationaux ) , le HC Davos ( 28 titres nationaux ) , le Djurgårdens IF ( 16 titres nationaux ) , le Jokerit Helsinki ou le HC Sparta Prague ( 8 titres ) .
Plus récemment , les clubs dominants sont les Dragons de Rouen et les Brûleurs de Loups de Grenoble .
Le hockey sur glace est un des quatre sports professionnels majeurs en Amérique du Nord .
C' est le sport d' hiver national au Canada où il jouit d' une immense popularité .
Même si seulement six des trente franchises de la LNH sont implantées au Canada , les joueurs canadiens sont plus nombreux que les Américains avec une proportion de trois joueurs canadiens contre un américain .
À peu près un quart des joueurs de la ligue ne viennent pas d' Amérique du Nord .
En Europe , c' est également le sport le plus populaire en Finlande et l' un des principaux sports dans des pays tels que la Suisse , la Russie , la République tchèque ou l' Allemagne .
Le hockey féminin devient de plus en plus populaire aujourd'hui , en particulier au Canada , aux États-Unis et en Europe .
Bien qu' il n' existe pas autant de ligues féminines que masculines , il en existe de tous niveaux comme la Ligue nationale féminine de hockey , beaucoup de championnats européens , les équipes nationales , olympiques , universitaires et de loisirs .
Le hockey féminin intègre les Jeux olympiques d' hiver depuis l' olympiade de Nagano .
Le hockey sur glace étant un des sports majeurs en Amérique du Nord , il joue un rôle important dans la culture populaire américaine et canadienne .
Ainsi , plusieurs films célèbres d' Hollywood ont pour sujet le hockey .
Ces films incluent La Castagne ( en anglais : " Slap Shot " ) sorti en 1977 , Les Petits Champions sorti en 1992 .
Ce dernier a été assez réussi pour engranger deux suites et entraîner la création d' une franchise de la LNH nommée les Mighty Ducks d' Anaheim , maintenant connue sous le nom de Ducks d' Anaheim ) ou encore Miracle ( 2004 ) .
Au Québec , la série de films Les Boys est un classique de nombreux amateurs de hockey , et est considérée comme le plus grand succès populaire du cinéma québécois .
De plus , le film Maurice Richard est une œuvre majeure du cinéma québécois qui raconte la vraie histoire d' un joueur local devenu héros national .
En raison de la grande popularité du hockey au Canada , il est considéré comme un élément important de la culture .
Les populaires séries télévisées québécoises Lance et compte ( 1986 -- ) ont également le hockey sur glace comme thème central .
Le single fut distribué gratuitement suite à un match des Bruins en novembre 2003 .
Beaucoup de jeux ayant pour thème le hockey ont été réalisés , la plupart proposant de disputer la LNH .
La série la plus connue est certainement la série des NHL d' Electronic Arts , initiée en 1991 avec EA Hockey , et qui depuis , a proposé une version chaque année .
