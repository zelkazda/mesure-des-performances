Elle est ensuite très remarquée dans la pièce La guerre de Troie n' aura pas lieu .
Elle tourne avec Jean-Luc Godard , Michel Deville ou Alain Resnais .
Elle a vécu en couple avec l' acteur Bernard Giraudeau pendant 18 ans .
En 1995 , elle rencontre l' acteur Cris Campion qui sera son compagnon pendant 10 ans .
En 2002 , elle soutenait Lionel Jospin lors de l' élection présidentielle .
Elle est , depuis 1992 , la vedette -- avec Bernard Lecoq -- de la série Une famille formidable , réalisé par Joël Santoni .
Elle a également tourné , toujours sous la direction de Joël Santoni , dans La Vocation d' Adrienne , diffusé en 1997 .
