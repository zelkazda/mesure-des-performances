Lessay se trouve dans la péninsule du Cotentin dans le Bocage normand .
La bourgade est arrosée par l' Ay et est pratiquement située sur la côte de la Manche , au fond de l' estuaire du fleuve côtier formant le havre de Lessay ( ou havre de Saint-Germain-sur-Ay ) .
Lessay est à 85 km à vol d' oiseau à l' ouest de Caen .
Le nom de Lessay est un toponyme ayant pour origine un mot du latin tardif *exaquium , non attesté , mais qui s' apparente au verbe *exaquare , reconstitué comme possible origine de l' ancien français essever , essiaver qui signifie " laisser s' écouler " .
Le rapport avec Lessay est évident puisque cette bourgade est au débouché d' un cours d' eau , l' Ay , et est environnée de marécages aujourd'hui partiellement asséchés .
En 1944 , la bourgade a été détruite suite à la bataille de Normandie dans le Cotentin .
Bien national à la Révolution , les neuf chanoines abandonnent la vie monacale , les bâtiments conventuels sont vendus et l' église abbatiale devient paroissiale .
