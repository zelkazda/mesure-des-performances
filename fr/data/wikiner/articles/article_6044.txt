Elle fait partie de la communauté d' agglomération de Lens -- Liévin ( Communaupole ) qui regroupe 36 communes , soit 250 000 habitants .
Cette commune est partagée entre les cantons d' Avion et de Rouvroy .
L' histoire de la région reste marquée par la catastrophe minière dite Catastrophe de Courrières qui fit 1099 morts le 10 mars 1906 sur les territoires de Billy-Montigny , Méricourt , Noyelles-sous-Lens et Sallaumines .
