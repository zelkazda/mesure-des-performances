La plage de Débarquement de Sword Beach englobait la commune de Ouistreham .
La commune est au débouché du port de Caen-Ouistreham sur le canal de Caen à la mer .
D' un point de vue touristique , la ville fait partie des communes de la Côte de Nacre .
Ouistreham est desservie par les Bus verts :
Le Port de Caen-Ouistreham est le port de commerce de Caen .
La commune est desservie , depuis 1986 , par des ferries trans- Manche vers Portsmouth ( Royaume-Uni ) .
Ces ferries sont actuellement exploités par la compagnie Brittany Ferries .
Le Ham ) , d' où les diminutifs hamel et hamelet .
Ouistreham était un village de pêcheurs et de paysans où l' activité était aussi liée au commerce maritime .
Ouistreham connut l' essor de son port grâce à l' extraction et à l' exportation de la brique de Caen .
Elle restera pendant longtemps la ligne la plus rentable du réseau des Chemins de fer du Calvados .
Lors de la Seconde Guerre mondiale , Ouistreham est occupé par les troupes allemandes et le casino sert de point de surveillance puisque surplombant la plage .
La prospérité du bourg , dont le trafic portuaire était florissant durant toute la période anglo-normande ainsi que le patronage de l' Abbaye aux Dames ont conféré la remarquable qualité de ce monument .
L' intérieur de l' église conserve encore des statues et un mobilier intéressants , ainsi que deux vitraux commémorant la libération de la ville lors de la Seconde Guerre mondiale .
On peut trouver mention de la grange aux dîmes de Ouistreham dès 1257 dans un censier commandé par l' abbesse de Caen : " Y avait une grange à dîmes .
Le phare de Ouistreham est un phare de granit , cylindrique , de 38 m de haut et peint en rouge et blanc .
Le phare de Ouistreham est visible à 16 milles marins .
Catherine Frot est marraine du cinéma ainsi que , depuis 2010 , Jacques Perrin .
Le casino de Ouistreham , sur le thème de la mer , est un établissement du groupe Lucien Barrière .
La commune partage le port de commerce avec Caen .
