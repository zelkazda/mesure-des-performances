Avant la Seconde Guerre mondiale , l' armée française était considérée comme une des plus puissantes du monde .
Néanmoins , malgré les décisions de réarmement prises à partir du gouvernement du Front populaire au pouvoir jusqu' en 1938 , qui furent gênées par l' application des 40 heures et la multiplication des grèves , et amplifiées par le gouvernement Daladier ensuite , elle ne put contenir la puissance retrouvée de l' Allemagne nazie .
En cinq semaines , l' avancée allemande en France entraîna la désintégration de l' armée et une gigantesque panique dans la population .
Le gouvernement , dirigé par Philippe Pétain depuis la démission de Paul Reynaud , fit demander l' armistice , signé le 22 juin 1940 .
Ainsi , Hitler voulait tenir un gouvernement français pour tenir les colonies françaises qui avaient une valeur stratégique fondamentale , comme cela sera confirmé par la suite .
La zone libre durera 28 mois , jusqu' en novembre 1942 ( débarquement allié en Afrique du nord ) , mais permit à la résistance française de se construire des bases au travers de toute la zone .
