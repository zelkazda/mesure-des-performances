j' avais l' air d' un con , ma mère " de Georges Brassens .
Le film Le Dîner de cons de Francis Veber , avec Thierry Lhermitte et Jacques Villeret , dépeint le personnage du con , dans le sens idiot , celui que l' on invite pour s' en gausser .
Une tradition estudiantine légendaire de l' École normale supérieure est de désigner le cuisinier ou l' intendant responsable d' une nourriture particulièrement exécrable de " Quel khon " .
L' exemple le plus célèbre d' utilisation du mot par un homme politique , même s' il peut s' agir d' une citation apocryphe , est celui d' Édouard Daladier après les accords de Munich , qui voyant la foule l' applaudir , dit entre ses dents " Les cons " .
Toutes s' accordent sur la réplique de de Gaulle , mais les circonstances varient très largement .
Par contre , en février 2008 , le président Nicolas Sarkozy a choqué , lors du Salon de l' agriculture , en employant ce même mot en public .
