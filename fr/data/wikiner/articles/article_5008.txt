Il a été adopté le 20 décembre 2000 par le président Vladimir Poutine en remplacement de la Chanson patriotique qui était utilisée depuis 1990 .
La musique est une adaptation de celle de l' hymne de l' Union soviétique datant de 1944 , composée par Aleksandr Aleksandrov .
Elle remplaçait à l' époque l' Internationale , qui était en vigueur depuis 1922 .
Après l' éclatement de l' URSS , l' hymne national de la Russie fut la Chanson Patriotique , qui ne possédait pas de paroles officielles .
De 1991 à 2000 , le comité de l' hymne national reçu plus de 6000 propositions de paroles , la plupart sur la musique de Mikhaïl Glinka ( celle de la Chanson Patriotique ) , d' autres sur la musique d' Aleksandrov .
Les références à Lénine et au communisme furent supprimées ainsi que l' idée d' une union indestructible entre les différentes républiques soviétiques .
La remise de l' hymne soviétique fut vivement controversée en Russie durant toutes les années 2000 .
L' ancien président Boris Eltsine était contre la remise de cet hymne , qu' il avait lui même remplacé en 1990 par la Chanson patriotique .
D' autres pays de l' ex-URSS ont également gardé ou remis l' ancien hymne de la période soviétique , tels que le Tadjikistan , le Bélarus , ou l' Ouzbékistan .
En 2010 , le Parti communiste de la Fédération de Russie demande le changement de l' hymne , en enlevant notamment les références faites à la religion et à " Dieu " .
Il peut être joué lors d' événements sportifs en Russie ou à l' étranger , selon le protocole de l' événement en question .
