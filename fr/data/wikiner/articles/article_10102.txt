The Yardbirds est un groupe de rock britannique des années 1960 , formé en mai 1963 .
Le fondateur du groupe , Keith Relf , était fan de l' auteur américain Jack Kerouac , lequel citait souvent le grand jazzman Charlie " Yardbird " Parker dans ses romans , ce qui l' a inspiré pour trouver un nom au nouveau groupe , .
Là , ils rencontrent le batteur Jim McCarty , le guitariste rythmique Chris Dreja , le bassiste Paul Samwell-Smith et décident de former un groupe qu' ils appellent les Yardbirds .
Son remplacement se fait avec un autre élève de cette école , Eric Clapton .
L' arrivée à la fin de l' année 1963 du jeune Eric Clapton à la guitare permet au groupe de sortir du lot .
La virtuosité de Clapton installe les Yardbirds comme l' un des plus intéressants groupes de sa génération sur scène .
L' album Five Live Yardbirds sorti en 1964 permet de confirmer cette réputation .
Mais jusqu' alors , les Yardbirds se contentent le plus souvent de brillantes reprises des plus éminents bluesmen américains ( Howlin ' Wolf , Sonny Boy Williamson II ... ) , sans posséder leur propre répertoire .
Ce vide est comblé avec la sortie du morceau For Your Love en 1965 .
E. Clapton parti , le groupe part à la recherche d' un nouveau soliste .
Tout d' abord Jimmy Page est contacté .
Guitariste de session très réputé dans le petit monde du rock britannique , Page décline pourtant l' offre et conseille à sa place son ami Jeff Beck .
L' arrivée de Jeff Beck marque un nouveau tournant dans l' orientation musicale du groupe .
Sous son influence , les Yardbirds vont s' évader sur les terres du psychédélisme , mais également défricher un nouveau genre que l' on ne tardera pas à appeler hard rock .
En 1966 , le bassiste et producteur Paul Samwell-Smith quitte le groupe pour convenances personnelles .
Il est remplacé par Jimmy Page , qui cette fois n' a pas décliné l' invitation .
Mais rapidement , Page échange sa place avec le second guitariste du groupe Chris Dreja .
Page désormais seul à la tête du groupe , les Yardbirds impressionnent plus que jamais par leurs prestations sur scène .
D' échecs en projets avortés , sans véritable ligne directrice , Page est lâché en 1968 par le chanteur Keith Relf et le batteur Jim McCarty qui s' en vont fonder un nouveau groupe .
En 1992 , le guitariste rythmique Chris Dreja et le batteur Jim McCarty décident de relancer les Yardbirds .
En 2003 , ils sortent un nouvel album , Birdland , sur lequel de nombreux invités prestigieux font une apparition .
Parmi eux , un vieil ami : Jeff Beck .
