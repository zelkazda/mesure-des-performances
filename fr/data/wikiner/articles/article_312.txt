On les appelle Cyclades car elles forment un cercle autour de l' île sacrée de Délos .
Le nombre des îles considérées comme faisant partie des Cyclades a varié au cours de l' histoire .
Ils étaient totalement indépendants ( " ils ne payaient aucun tribut " ) , mais fournissaient des marins aux navires de Minos .
Les Cyclades connurent une grande prospérité lors du néolithique , en partie grâce à l' obsidienne , dont Milo , île volcanique , était une des principales sources .
On trouve de l' obsidienne mélienne jusqu' en Thessalie et Asie Mineure .
Les îles entrèrent ensuite dans la première Ligue de Délos en 478-477 avant de passer sous domination totale d' Athènes .
Elles connurent alors une relative période d' autonomie avant d' entrer dans la seconde Ligue de Délos et de repasser sous la coupe athénienne .
Elles se révoltèrent lors du conflit de 357-355 , pour finalement passer sous la domination des Macédoniens .
Délos réussit à retourner dans le giron romain .
Vespasien le constitua en province romaine .
Il périt lors du siège de Constantinople .
Léon rétablit brutalement son autorité sur les Cyclades .
Ces derniers s' installèrent en Crète d' où ils menèrent des raids sur les Cyclades pendant plus de cent ans .
En 1204 , la IV e Croisade s' empara de Constantinople , et les vainqueurs se partagèrent l' Empire byzantin .
La souveraineté nominale sur les Cyclades échut aux Vénitiens .
Barberousse prit les îles pour les Turcs à partir de 1537 .
Il ne venait qu' une fois par an , avec toute sa flotte , toucher la somme globale des impôts des Cyclades .
Les Cyclades furent de tous les soulèvements importants , comme en 1770-1774 , lors du bref passage des Russes de Catherine II .
La Grèce attire vers les îles centrales de la mer Égée des centaines de milliers de touristes .
