Il est le quatrième roi de la famille des Valois-Angoulême .
Il est le 3 e fils et le 5 e des 10 enfants d' Henri II ( 1519 -- 1559 ) et de Catherine de Médicis ( 1519 -- 1589 ) .
Il déboucha sur le massacre de la Saint-Barthélemy .
Il est âgé de 10 ans lors de son accession au trône ; la régence est confiée à sa mère Catherine de Médicis qui gouverne jusqu' à la majorité du roi .
Elle nomme Antoine de Bourbon lieutenant général du royaume .
Charles succède alors à son frère aîné François II , disparu après moins d' un an et demi de règne .
Le chancelier Michel de l' Hospital conseille à la régente de libérer les prisonniers de la conjuration d' Amboise .
Le 16 novembre 1561 , le massacre de Cahors , qui fait près de 30 morts protestants , confirme cet échec .
Le 1 er janvier 1562 , l' édit de Saint-Germain-en-Laye permet aux protestants de pratiquer leur culte dans les campagnes et les faubourgs urbains .
Néanmoins , après le massacre de Wassy le 1 er mars 1562 , les protestants prirent les armes , ayant à leur tête le prince de Condé .
Après quelques succès , ils sont battus à Dreux par le duc de Guise le 19 décembre 1562 .
Le 4 février 1563 , François de Guise met le siège devant Orléans , et y décède le 24 février de trois coups de pistolet dans le dos .
Le 19 août 1563 , Charles IX est déclaré majeur mais la reine mère continue d' exercer le pouvoir en son nom .
L' édit de pacification d' Amboise ne satisfait personne , et a du mal à être appliqué : il interdit le culte réformé dans les villes , alors que les protestants sont majoritaires dans de nombreuses places importantes , et sont maîtres de plusieurs provinces .
L' itinéraire passe par les villes les plus agitées du royaume : Sens , Troyes en Champagne .
Dans le Languedoc , le jeune roi passe à Montpellier , Narbonne , Toulouse .
Dans les villes protestantes de Gascogne , il est accueilli respectueusement , sans plus .
À Montauban , où l' entrée se fait le 20 mars 1565 , il faut négocier le désarmement de la ville , qui avait résisté à trois sièges de Monluc .
Toulouse et Bordeaux sont plus tranquilles , étant aux mains des catholiques .
En juillet , la Gascogne est à nouveau traversée , puis en août et septembre , la vallée de la Charente .
Dans ces régions à forte minorité protestante , la paix est extrêmement fragile , et les protestants appliquent non sans réticences l' édit d' Amboise .
Les seules anicroches sont à La Rochelle où les protestants se montrent mécontents , et à Orléans , où le convoi est accueilli par une émeute .
En 1566 , le roi s' arrêta enfin à Moulins , où furent décidées plusieurs réformes .
En juin 1566 à Pamiers , malgré la pacification royale , les hostilités reprennent et les protestants assaillent les églises catholiques .
La répression catholique est féroce : 700 calvinistes sont massacrés à Foix .
Ces derniers se réfugient à Meaux le 24 septembre .
Le 29 , des notables catholiques sont assassinés à Nîmes , puis dans tout le Languedoc .
À la tête des troupes protestantes , le prince de Condé et Gaspard II de Coligny arrivent aux portes de Paris .
Charles IX épouse en 1570 Élisabeth d' Autriche ( 1554 -- 1592 ) , fille de Maximilien II ( 1527 -- 1576 ) , empereur romain germanique , et de Marie d' Espagne ( 1528 -- 1603 ) , infante d' Espagne .
En mars 1571 , la reine et le roi font leur entrée à Paris .
Pour l' occasion , Ronsard écrit :
De cette union est issue une fille morte jeune , Marie-Élisabeth de France ( 1572 -- 1578 ) .
À l' automne 1571 , Coligny rencontre le roi pendant quelques jours .
Craignant un soulèvement , Charles IX décide sur les avis de sa mère Catherine de Médicis et de ses conseillers , l' élimination des chefs protestants , à l' exception de quelques-uns parmi lesquels les princes du sang Henri de Navarre ( futur Henri IV ) et le prince de Condé .
Ce massacre marque un tournant dans le règne de Charles IX .
La guerre reprend et débouche sur le siège de La Rochelle .
A cause de son caractère inattendu et déroutant , le massacre de la Saint-Barthélemy a depuis toujours fait l' objet de débats .
Dès le lendemain , Ambroise Paré procède à une autopsie et confirme que le roi est mort d' une pleurésie faisant suite à une pneumonie tuberculeuse .
Marie-Nicolas Bouillet et Alexis Chassang ( dir .
