Les contours de l' Artois ont varié au cours des siècles , en fonction des aléas de l' histoire et des rattachements ou séparations d' avec les comtés voisins , comté de Boulogne , comté de Flandre , etc .
Saint Louis donna l' Artois en 1237 , avec titre de comté , à Robert , son frère puîné .
Il comporte plusieurs sites : Villeneuve-d'Ascq , Lille , Douai , Arras , Valenciennes , Outreau .
Le siège de l' école interne est à Villeneuve-d'Ascq .
Pendant plus d' un siècle , la vie au sud de l' Artois a été tournée vers l' exploitation du charbon .
