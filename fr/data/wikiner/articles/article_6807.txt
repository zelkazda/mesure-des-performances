Les 81 habitants de la commune associée de Gerville-la-Forêt sont rattachés au canton de la Haye-du-Puits , alors que le reste de la commune fait partie de celui de Lessay .
La région est habitée au moins depuis l' époque gauloise par la tribu des Unelles , avec à leur tête le chef Viridorix .
En 1973 , Vesly a fusionné avec Gerville-la-Forêt , qui a gardé le statut de commune associée et son appartenance au canton de La Haye-du-Puits .
Deux des conseillers dont un maire délégué représentent la commune associée de Gerville-la-Forêt .
Vesly a compté jusqu' à 1512 habitants en 1821 .
