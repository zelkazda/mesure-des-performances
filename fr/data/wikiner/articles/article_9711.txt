Il est , avec Johan Ludvig Runeberg , l' un des Finlandais qui symbolisent le mieux la naissance de l' identité nationale finlandaise , bien qu' il soit suédophone .
Il échoue à devenir violoniste dans le Philharmonique de Vienne et de retour à Helsingfors en 1892 , il y enseigne la théorie musicale .
Dans les années trente , il cesse assez brutalement de composer , notamment en raison du succès du dodécaphonisme et de la musique sérielle , mais aussi vraisemblablement de l' achèvement de sa propre esthétique dans la 7 e symphonie et dans Tapiola .
Sibelius évoquera lui-même " la pure eau froide " [ réf. nécessaire ] de sa 6 e symphonie , aux harmonies modales qui ressuscitent l' ancienne polyphonie grégorienne , publiée en réaction aux " cocktails " musicaux de son temps .
Pourtant , rien ne serait plus faux que de voir en Sibelius un musicien réactionnaire .
69b en sol mineur ) et des thèmes inoubliables parsèment néanmoins son œuvre , à l' image de ceux qui inaugurent dans une atmosphère diaphane de vitrail son Concerto pour violon et sa 6 e symphonie , ou de ceux qui achèvent dans une clameur hymnique certaines de ses symphonies .
Son poème symphonique Finlandia , écrit en 1899 -- 1900 , devint le symbole de la résistance finlandaise vis-à-vis de l' occupation russe .
En cela , il annonce déjà certains compositeurs de musique minimaliste comme Ligeti , duquel il sera d' ailleurs souvent rapproché dans les musiques de films ( comme , une fois encore , 2001 , l' Odyssée de l' espace ) .
L' un de ses élèves les plus brillants est le compositeur Leevi Madetoja , également finlandais .
Les interprètes de Sibelius se partagent entre ceux qui exaltent son particularisme finnois ( Paavo Berglund est le plus emblématique d' entre-eux ) et ceux qui le projettent vers les standards symphoniques germaniques .
Il faut citer aussi Herbert von Karajan ( que Sibelius lui-même a tantôt loué tantôt rejeté ) , très proche de son esthétique nordique à l' anonymat un peu lunaire , Leonard Bernstein , beaucoup plus passionné , et plus récemment sir Colin Davis , Paavo Järvi et Osmo Vänskä .
Enfin , les mélodies , généralement négligées , ont été enregistrées presque intégralement par Anne-Sofie von Otter .
