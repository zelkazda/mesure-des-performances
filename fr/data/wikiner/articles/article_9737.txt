Jirō Taniguchi est un auteur de mangas seinen et gekiga , né le 14 août 1947 à Tottori , au Japon .
Outre Voyage à Tokyo , Barberousse d' Akira Kurosawa et Le Retour d' Andreï Zviaguintsev sont ses films préférés .
Ce film est réalisé par Sam Garbarski , avec Jonathan Zaccaï , Léo Legrand , Alexandra Maria Lara , et Pascal Greggory .
La musique du film est composée par Air , groupe que l' auteur apprécie particulièrement .
