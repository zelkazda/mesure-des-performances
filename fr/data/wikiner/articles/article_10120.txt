Le Yunnan ( chinois simplifié : 云 南 , chinois traditionnel : 雲南 , pinyin Yúnnán ) est une province du sud-ouest de la Chine .
Région essentiellement montagneuse , le Yunnan reste peu avancé sur le plan économique , malgré le développement du tourisme .
Le Yunnan est une mosaïque linguistique .
En plus du mandarin , la langue officielle de la Chine , les langues parlées au Yunnan appartiennent à plusieurs familles linguistiques :
Le Yunnan était autrefois indépendant à l' époque du royaume de Nanzhao 南诏 ( 649-902 ) , puis du royaume de Dian 滇 , dont les élites étaient de langue bai .
Vers cette époque , Marco Polo a voyagé dans le Yunnan et a décrit son émerveillement dans le récit de son voyage .
Le Yunnan possède des paysages d' une très grande diversité , des plateaux arides du Tibet jusqu' aux forêts tropicales du Xishuangbanna .
Province essentiellement agricole dotée de terres très fertiles ( la " terre rouge " du Yunnan ) et d' un climat généralement clément , le Yunnan exporte une très grande variété de fruits et légumes .
Plusieurs fleuves importants traversent le Yunnan dont :
Sa capitale , Kunming , est située dans une cuvette à environ 2000 m d' altitude , et entourée de rizières en étages .
Le Yunnan est une région où l' activité agricole est très forte et très diversifiée du fait de ses climats très contrastés .
À Kunming a régulièrement lieu l' exposition horticole internationale .
Le Yunnan abonde en ressources naturelles .
Parmi les 30000 espèces de plantes supérieures chinoises , 18000 peuvent être trouvées au Yunnan .
Le maïs est produit sur tout le Yunnan .
À Kunming par exemple , vers le mois de septembre , de nombreux restaurants proposent des spécialités de fondues aux champignons , composées de différents champignons de la région .
Le Yunnan est une province très touristique , du fait de sa grande diversité de paysages et de cultures .
Principalement chinois , les touristes rapportent des milliards d' euros au Yunnan [ réf. nécessaire ] .
Le Yunnan se distingue par une grande diversité ethnique et culturelle .
Les Han représentent plus de 60 % de la population , de nombreuses autres nationalités sont présentes dans la province .
On trouve généralement localement les écritures de ces minorités sur les panneaux au côté du chinois mandarin ; tibétain à Xianggelila , thaïlandais à Xishuangbanna ou dongba à Lijiang .
