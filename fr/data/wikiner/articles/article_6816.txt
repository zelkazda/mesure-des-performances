C' est le principal axe routier de la commune avec la route entre la commune suisse du Grand-Saconnex et Prévessin-Moëns .
Ferney-Voltaire n´est pas reliée au reseau ferroviaire .
La voie ferrée de Bellegarde-sur-Valserine à Divonne-les-Bains ne traverse pas Ferney-Voltaire .
Les Transports Publics Genevois pensent à prolonger le tramway de Meyrin à Ferney Voltaire mais cet éventuel projet ne pourra se réaliser qu' à une échéance lointaine .
La gare la plus proche de Ferney-Voltaire est celle de l´ Aéroport international de Genève que les CFF deservent .
La commune bénéfice aussi de cars TER , de Divonne-les-Bains à Bellegarde-sur-Valserine .
On peut noter également que 4 % des habitants de la commune sont des personnes logées gratuitement alors qu' au niveau de l' ensemble de la France le pourcentage est de 4,9 % .
Ferney a pris le nom de Ferney-Voltaire à partir de 1878 en hommage à Voltaire qui y séjourna à partir de 1759 pour sa proximité de la frontière , en cas de problème avec l' administration royale , et de Genève , ville de son rival , Rousseau .
À sa mort en 1778 , Ferney comptait près de 1 000 habitants .
Le canton de Ferney-Voltaire fait partie de l' arrondissement de Gex ou Pays de Gex , compte 8 communes et 28 427 habitants .
