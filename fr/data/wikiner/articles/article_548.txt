Le projet a été fondé par George Woltman , qui est aussi le créateur du logiciel de calcul distribué employé .
Ainsi , le GIMPS a pu remporter le 6 avril 2000 , la première récompense de 50000 USD offerte par l' Electronic Frontier Foundation pour la découverte du premier nombre premier de plus d' un million de chiffres .
Des règles de répartition de la récompense sont prévues par le GIMPS entre l' internaute qui trouve le nombre , le GIMPS , des œuvres caritatives et les autres internautes qui participent au GIMPS et trouvent des nombres premiers .
L' Electronic Frontier Foundation offre d' autres récompenses de 100 000 , 150 000 et de 250000 USD pour , respectivement , la découverte de nombres premiers de plus de dix millions , cent millions et un milliard de chiffres .
