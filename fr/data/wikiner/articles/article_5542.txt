Saint-Loup-Hors est une commune limitrophe au sud-ouest de Bayeux appartenant à la Communauté de communes Bayeux Intercom .
Le territoire est traversé par la ligne de chemin de fer Paris -- Cherbourg .
Selon la légende , il aurait terrassé un monstre avant de le jeter hors dans la Drôme .
Saint-Loup-Hors a compté jusqu' à 333 habitants en 1836 .
