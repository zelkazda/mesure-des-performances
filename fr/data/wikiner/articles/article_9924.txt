Après avoir obtenu un baccalauréat au Trinity College de Cambridge , Charles accomplit son devoir militaire dans la Royal Navy entre 1971 et 1976 .
Ils ont deux enfants , le prince William de Galles né en 1982 et le prince Harry de Galles en 1984 .
Le couple se sépare en 1992 et ils divorcent en 1996 , après que Diana a publiquement accusé le prince d' avoir une relation extra-conjugale avec Camilla Parker Bowles .
Diana meurt dans un accident de voiture en 1997 et , en 2005 , le prince épouse Camilla Parker Bowles .
Charles est le premier petit-fils du roi George VI et de la reine Elizabeth .
Il assiste au couronnement de sa mère à l' Abbaye de Westminster en 1953 , aux côtés de sa grand-mère et de sa tante .
La vie universitaire du prince Charles s' est déroulée au Trinity College , Cambridge , d' où il est diplômé ( 2 e classe , 2 e rang ) d' anthropologie , archéologie et d' histoire .
Charles est le premier membre de la famille royale britannique à obtenir un diplôme universitaire .
L' année suivante , il prend possession de son siège à la chambre des Lords , devenant par la suite le premier membre de la famille royale à assister à une réunion du cabinet depuis George I er .
Charles avait en effet été invité par le premier ministre James Callaghan afin qu' il puisse découvrir le travail du gouvernement .
Le 29 juillet 1981 , il épouse Lady Diana Spencer , dont il divorce le 28 août 1996 .
De cette façon , les enfants de l' héritière présomptive avaient un statut royal et princier , ce qui n' a pas été jugé nécessaire pour les enfants de l' autre fille du roi , Margaret .
