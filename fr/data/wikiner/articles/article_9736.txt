Le Château de Valençay se trouve à Valençay , en Indre ( France ) .
Il fut la propriété de John Law , puis du prince de Talleyrand .
Bien que situé dans le Berry , sa construction l' apparente aux châteaux de la Loire , en particulier au château de Chambord .
La décoration aurait été confiée à Pierre de Cortone et au peintre Jean Mosnier .
Il y crée une filature , plusieurs forges , fait rétablir les ponts sur le Nahon , et refaire la route de Selles-sur-Cher .
Ces forges se trouvaient à Luçay-le-Mâle , " annexe à la seigneurie de Valençay ( ... ) le château de Lucay parait être de la même époque que celui de Valençay : sa position est très belle , il domine la forge , l' étang qui l' alimente , le bourg de Luçay et des ravins pittoresques " .
Par un codicille à son testament du 9 mars 1837 , Talleyrand , qui mourut un an après , assura la perpétuité de l' établissement , et exprima la volonté d' y être inhumé et à cet effet fit creuser une grande crypte sous le chœur de la chapelle de l' école libre .
Ce jour-là , une division allemande ( la 2e division SS Das Reich ) investissait en effet la ville en représailles au meurtre par des maquisards de deux soldats allemands en lisière de la forêt qui entourait le château .
