Engagé dans son ensemble , le Corps européen peut fournir jusqu' à 60000 hommes .
Créé en 1992 , ce corps d' armée est déclaré opérationnel en 1995 , il est une initiative franco-allemande à laquelle se sont successivement joint la Belgique ( 1993 ) , l' Espagne ( 1994 ) puis le Luxembourg ( 1996 ) .
Le Quartier général du Corps européen ou Eurocorps , est installé près de la frontière franco-allemande à Strasbourg , siège de plusieurs institutions européennes .
Le Quartier général est composé des unités suivantes :
Les cinq nations-cadre ont prédésigné les unités suivantes au Corps européen :
Le type et la taille des unités répondant aux besoins de l' Eurocorps doivent être déterminés en fonction de la mission confiée , de l' emploi probable et de l' objectif opérationnel .
