Le premier Macintosh est lancé le 24 janvier 1984 .
Le Macintosh remplace l' Apple II comme principal produit d' Apple .
Cependant les parts de marché d' Apple baissent , avant un renouveau des Macintosh en 1998 , avec la sortie de l' ordinateur grand public tout-en-un iMac , qui permet à Apple d' échapper à une probable faillite et marque même un succès pour la firme .
Les Macintosh actuels visent principalement les marchés des professions artistiques , de l' éducation et des particuliers .
En 2010 , les modèles Macintosh vendus par Apple sont :
Ceci contraste avec les PC , pour lesquels plusieurs constructeurs se chargent de créer du matériel conçu pour utiliser le système d' exploitation d' une autre entreprise .
Entre 1984 et 1994 , les Macintosh fonctionnaient avec des processeurs de la famille 68000 de Motorola , avant d' utiliser entre 1994 et 2006 des processeurs PowerPC de l' Alliance AIM .
Pour faire fonctionner son ordinateur , Apple a développé une famille de systèmes d' exploitation spécifiques au Macintosh .
À l' aube des années 2000 , cette lignée est remplacée par Mac OS X , développé à partir de NeXTSTEP .
Sur les Macintosh Intel , il est possible d' installer des systèmes d' exploitation comme Microsoft Windows , Linux , FreeBSD ou bien d' autres .
Le projet Macintosh débute à la fin des années 1970 .
Jef Raskin , employé d' Apple depuis 1978 , avait dans l' idée de créer un ordinateur simple d' utilisation et peu cher et donc accessible aux consommateurs moyens .
Il présente son idée à Mike Markkula , l' un des trois fondateurs d' Apple Computer , en mars 1979 .
Au fil des années , il rassemble une grande équipe dédiée au développement du Macintosh et de ses logiciels .
Il voulait utiliser un microprocesseur Motorola 6809 , moins cher mais aussi moins performant car ne pouvant adresser plus de 64 kibioctets , ce qui l' aurait rendu rapidement obsolète .
Bud Tribble , à la tête de l' équipe des développeurs du Macintosh , intéressé par l' évolution que prenaient les programmes du Lisa , demande à Burrell Smith d' essayer d' incorporer le Motorola 68000 du Lisa dans le Macintosh tout en essayant de maintenir les coûts le plus bas possible .
Fin 1980 , Michael Scott , CEO d' Apple Computer à l' époque , procède à une restructuration de l' entreprise .
Steve Jobs est alors contraint de quitter le projet Lisa .
Envoyé par Scott pour représenter l' entreprise à son introduction en bourse le 12 décembre 1980 , il ne convainc pas comme manager .
C' est alors qu' il se tourne vers le projet Macintosh de Jef Raskin .
Il y voit une revanche à son exclusion du projet Lisa .
Steve Jobs sortit vainqueur de la confrontation , puisque la souris a bien fait son apparition chez Apple avec le Macintosh .
Ces confrontations répétées et le grand ego des deux personnages ont mené au départ de Jef Raskin du projet Macintosh et d' Apple Computer , officiellement le 1 er mars 1982 , presque deux ans avant le lancement officiel du Macintosh en janvier 1984 .
Le lancement du premier Macintosh est accompagné d' une vaste campagne de publicité .
Pour mettre en valeur la nouvelle interface en attendant l' arrivée des premières applications tierces , ce dernier est livré avec les applications MacPaint et MacWrite .
Steve Jobs y présente pour la première fois le Macintosh , et on y voit l' ordinateur dessiner sur son écran Macintosh , insanely great ! ( " Macintosh , follement génial " ) , ainsi que raconter une blague à l' aide de son synthétiseur vocal intégré .
Les réactions qui suivent le lancement du Macintosh sont globalement positives .
En avril 1984 , Microsoft porte Multiplan de MS-DOS vers le Macintosh , suivi par Microsoft Word en janvier 1985 .
La même année , Lotus Software lance Lotus Jazz après le succès de Lotus 1-2-3 sur l' IBM PC ; c' est cependant un échec .
De son coté , Apple lance Macintosh Office avec la publicité Lemmings diffusée lors de la Super Bowl XIX .
Apple fait évoluer son Macintosh en septembre 1984 .
La combinaison du Macintosh , de l' imprimante LaserWriter d' Apple -- dotée d' un interpréteur pour le langage de description de page PostScript d' Adobe -- et des logiciels spécifiques tels que MacPublisher , puis surtout Aldus PageMaker , permet aux utilisateurs de composer , préparer et visualiser directement des documents destinés à l' impression , sans devoir recourir aux onéreuses stations de travail spécialisées utilisées à cette époque .
Avec 4 ans et 10 mois , il est le Macintosh resté en vente le plus longtemps .
En mars 1987 , sont lancés le Macintosh II et le Macintosh SE .
Le Macintosh SE est le premier Macintosh Classic à disposer d' un port d' extension interne .
Le Macintosh II marque un plus grand virage pour les Macintosh .
Pour la première fois , un Macintosh adopte une architecture ouverte , avec plusieurs connecteurs d' extension et un design plus modulaire , proche de celui de l' IBM PC .
En août 1987 , lors de la Macworld Expo à Boston , Apple dévoile HyperCard et MultiFinder .
Le Motorola 68030 fait son apparition avec les Macintosh IIx en septembre 1988 .
Il s' agit de la première tentative de la firme de faire un Macintosh transportable et alimenté par une batterie .
L' année suivante , en mars 1990 , arrive le Macintosh IIfx sur le marché à un tarif de 9900 USD .
Microsoft Windows 3.0 , sorti en mai 1990 , se rapproche visiblement du système des Macintosh tant en performances qu' en fonctionnalités .
Les PC étaient , à l' époque déjà , des plates-formes alternatives et moins chères que le Macintosh .
Apple , après avoir lancé le Macintosh IIfx , Macintosh le plus cher jamais vendu par la société , se lance sur le marché de l' entrée de gamme avec des machines abordables .
Ainsi la firme lance trois nouvelles machines en octobre 1990 : Ces trois machines se vendent bien , mais les marges d' Apple sur ces Macintosh sont plus faibles que sur les modèles antérieurs .
1991 voit la sortie de Système 7 , une version réécrite en 32 bits du système Macintosh qui apporte des améliorations au niveau graphique , de l' adressage mémoire , des réseaux et de la gestion du multitâche ( optionnel sous Système 6 ) .
En octobre de cette même année , sont lancés le Macintosh Classic II , le Macintosh LC II ainsi que deux nouvelles familles d' ordinateurs : les Macintosh Quadra , avec les modèles 700 et 900 , qui occupent le haut de gamme d' Apple , et les PowerBook , plus proches des ordinateurs portables actuels que le Macintosh Portable qu' ils remplacent .
En 1992 , afin de s' ouvrir un peu plus au grand public , Apple lance la gamme Performa , constituée de Macintosh déjà existants mais rebadgés pour l' occasion à destination des familles et du monde de l' éducation .
Les PowerBook Duo , lancés à la fin de cette même année , premiers ultraportables d' Apple , peuvent être placés sur une station d' accueil afin d' en faire des ordinateurs de bureau avec toute la connectique correspondante .
Cette nouvelle famille de processeurs donne lieu à une nouvelle famille de Macintosh , les Power Macintosh , nom plus tard abrégé en Power Mac .
En janvier 1995 , après moins d' un an de production , Apple annonce en avoir vendu un million , montrant un relatif succès de ces machines .
Cependant , en dépit de ses efforts , la part de marché d' Apple s' érode plus en plus au profit de Microsoft et Intel .
Cette tendance s' amplifie avec la sortie de nouveaux Pentium et celle de Windows 95 .
En réponse à cela , Apple lance un programme de licence de son système d' exploitation , permettant ainsi à d' autres entreprises de vendre leurs propres ordinateurs équipés du Système 7 .
Ces clones étaient censés faire gagner au système des parts de marchés , objectif qui n' est finalement pas atteint car les parts de marché des clones grignotent principalement celles du Macintosh d' Apple .
En 1997 , à la suite du retour de Steve Jobs chez Apple , la version 7.7 du système est renommée MacOS 8 , en lieu et place du défunt projet Copland .
Apple n' ayant licencié que le Système 7 aux constructeurs tiers , cela permet à la firme de mettre fin à la commercialisation des clones .
Pour célébrer les 20 ans d' Apple , la firme sort en mai 1997 Twentieth Anniversary Macintosh , produit à 12000 unités .
Il a la particularité d' être équipé d' un écran plat similaire à ceux trouvés à l' époque sur les PowerBook , une première pour Apple sur un ordinateur de bureau .
En 1998 , après le retour de Steve Jobs à la tête de la société , Apple lance un nouvel ordinateur tout-en-un , l' iMac .
En plus de se démarquer des autres Macintosh par son design , il marque l' abandon des connecteurs ADB et SCSI , remplacés par deux ports USB .
De sa mise en vente , le 15 août 1998 , jusqu' à la fin de l' année , Apple en vend plus de 800000 unités .
Ces ventes , ajoutés à celle des Power Macintosh G3 , permettent à Apple de produire pour la première fois depuis 1995 des bénéfices , .
En 1999 , l' aspect blanc et bleu du boîtier est appliqué au Power Mac G3 et à un nouveau venu , l' iBook , le premier ordinateur portable d' Apple destiné au grand public .
Tel l' iMac , l' année précédente , l' iBook est un succès , devenant l' ordinateur portable le plus vendu aux États-Unis au dernier trimestre de 1999 .
Lors de cette même année , Apple commence à équiper ses machines du processeur PowerPC G4 avec la sortie des premières versions du Power Mac G4 .
Après avoir utilisé de nombreuses couleurs sur les iMac et les iBook , Apple opte pour le polycarbonate blanc pour ses machines grand public .
Ainsi le nouvel iBook sorti en 2001 , l' iMac G4 et l' eMac sortis tous les deux en 2002 , arborent des boîtiers blancs , tandis que pour ses machines destinés aux professionnels , la firme à la pomme opte pour des boîtiers en métaux , titane puis aluminium pour les PowerBook G4 et aluminium pour les Xserve .
Le Power PC G4 , laisse ensuite sa place au PowerPC G5 à partir de 2003 , où il fait son entrée dans le Power Macintosh G5 , puis dans l' iMac en 2004 .
En janvier 2005 , Apple annonce son Mac mini , le Macintosh le moins cher jamais vendu par la firme , disponible à sa sortie pour 499 $ / 529 € .
Après la sortie de Mac OS 8 , le système a continué d' évoluer jusqu' à sa dernière version , la 9.2.2 .
Parmi les améliorations apportées se trouve le support du système de fichiers HFS+ dans la version 8.1 , la restriction aux processeurs PowerPC dans la version 8.5 et l' apparition du nano-kernel dans la version 8.6 , .
Le projet Copland ayant été abandonné , Apple avait racheté , en décembre 1996 , NeXT pour faire du système d' exploitation NeXTSTEP la base du nouveau système d' exploitation des Macintosh , Mac OS X .
Ce dernier est fondé sur le micro-noyau Mach implanté dans le noyau XNU , tous les deux utilisés par NeXTSTEP , et amélioré à partir du code issu de BSD pour être inclus dans le cœur de Mac OS X , Darwin .
Elle contient l' environnement Classic , qui permet de faire fonctionner les applications conçues pour les versions antérieures de Mac OS .
Par la suite , Apple publie des mises à jour majeures pour son système d' exploitation : 10.1 Puma ( 25 septembre 2001 ) , 10.2 Jaguar ( 24 août 2002 ) , 10.3 Panther ( 24 octobre 2003 ) , 10.4 Tiger ( 29 avril 2005 ) , 10.5 Leopard ( 26 octobre 2007 ) et la dernière version en date , 10.6 Snow Leopard ( 28 août 2009 ) .
Il ajoute que le système Mac OS X a été développé dès le début dans l' optique de fonctionner tant sur les architectures x86 que PowerPC .
Le tout nouveau MacBook suit , puis le Mac Pro et , en dernier , les Xserve .
Les ordinateurs portables deviennent tous des MacBook , nom qui sera ensuite décliné en fonction des modèles .
À ce jour , tous les ordinateurs vendus par Apple utilisent des processeurs x86 conçus et fabriqués par Intel .
Ils sont quelques fois appelés Macintel , sur le modèle du terme Wintel utilisé pour désigner les compatibles PC utilisant des processeurs Intel et le système d' exploitation Windows de Microsoft .
Par l' intermédiaire de l' émulateur Rosetta , il est possible d' utiliser des applications PowerPC sur un Macintel , mais à une vitesse inférieure à celle des applications natives .
L' environnement " Classic " n' est cependant pas pris en charge par les Macintel et Mac OS X v10.5 " Leopard " .
L' utilisation de la même architecture de processeur que les PC permet de faire fonctionner un système Microsoft Windows sans l' aide d' un émulateur tel que Virtual PC .
En mars 2006 , un groupe de hackers est parvenu à faire fonctionner Windows XP sur un Macintel avec un outil qu' ils ont ensuite mis à disposition sur leur site .
Les versions suivantes ont ajouté le support de Windows Vista et la correction de bugs , jusqu' à la sortie de la première version finale , intégrée au système Mac OS X 10.5 .
L' iPhone et les applications étant devenues la première source de revenus pour Apple .
Contrairement à IBM , qui a autorisé Microsoft à proposer MS-DOS à des tiers permettant l' apparition des Compatibles PC , Apple contrôle le matériel et les logiciels .
Bien que la conception des Macintosh relève directement d' Apple , leur montage est sous-traité auprès d' OEM asiatiques tels que Asus , Foxconn ou Quanta Computer .
Le processeur utilisé par les Macintosh était , pendant plus de 20 ans , un élément qui les distinguait des compatibles PC .
Ces derniers ont toujours utilisé des processeurs Intel de la famille x86 , du nom du jeu d' instructions introduit sur l' Intel 8086 .
Pour leur part , les Macintosh ont utilisé successivement des processeurs Motorola 68000 , puis des PowerPC , et depuis 2006 des processeurs Intel x86 identiques à ceux utilisés par les PC .
Autre particularité qui distinguait les Macintosh des PC , les souris qui les équipaient ont toujours eu un seul bouton , alors que les souris destinées aux autres plate-formes étaient dotées de deux boutons , voire plus .
Au fil des années , les Macintosh ont intégré les évolutions technologiques traduites , par exemple , par l' apparition ou la disparition d' interfaces .
Ainsi , du côté des cartes d' extension les Macintosh ont connu les NuBus ou les PDS dans la fin des années 1980 et le début des années 1990 , puis le PCI , remplacé ensuite par le PCI Express .
Les claviers et souris ont connu d' abord la solution maison ADB , avant d' utiliser l' USB dès son apparition sur l' iMac G3 .
Malgré l' évolution des technologies utilisées , certaines sont adoptées bien après être devenues des standards sur PC , tel le lecteur de carte SD .
Le remplacement des certains composants par d' autres plus véloces , souvent appelé upgrade , est considéré comme plus difficile sur les Macintosh car les pièces en question y sont souvent moins accessibles .
À titre d' exemple , seule la mémoire vive et la carte AirPort peuvent être remplacées sur les iMac ( à l' exception de la première génération d' iMac G3 ) .
Des machines comme les Power Macintosh ou les Mac Pro , dont l' unité centrale est une tour , possèdent des composants accessibles une fois les baies ouvertes , et il est ainsi possible de remplacer facilement des composants comme le disque dur , les cartes graphiques et d' extension et la mémoire vive .
Le Macintosh 128K est le premier ordinateur personnel utilisant une interface graphique abordable à rencontrer un succès .
En 1997 , avec la version 7.6 , le système est renommé Mac OS .
Cette même année , Apple le remplace en lançant Mac OS X , basé sur NeXTSTEP et Darwin .
Parmi les nouveautés apportées se trouvent l' interface Aqua et le dock .
La compatibilité des logiciels avec les Macintosh lors des phases de transition d' architecture matérielle est gérée par le système d' exploitation par le biais de l' émulation .
De même , l' architecture de Mac OS X étant différente de celle des Mac OS " classic " , il a fallu faire appel à l' émulateur Classic inclus dans Mac OS X , des versions 10.0 à 10.4 , afin de faire fonctionner d' anciennes applications sur ce nouveau système .
Présenté par Apple comme plus sûr que Windows , Mac OS X n' est pas pour autant exempt de problème de sécurité .
Elles sont de manière générale corrigées à travers des mises à jour proposés soit par Apple , soit par des éditeurs tiers lorsqu' ils sont responsables des failles introduites par leurs logiciels .
Bien qu' Apple conseille d' utiliser un logiciel antivirus , la rareté des virus et autres logiciels malveillants sur la plate-forme peut permettre de s' en dispenser , chose peu indiquée sous Windows .
Initialement , Mac OS était le seul système utilisable sur un Macintosh .
La solution principale de contournement , qui fut utilisé même par Apple pour A/UX , consistait à démarrer sur Mac OS puis , grâce à un chargeur d' amorçage , de démarrer le système souhaité .
Cela fut nécessaire jusqu' à l' arrivée de l' Open Firmware .
Aujourd'hui , les Macintosh sont capables de démarrer directement à partir de l' Open Firmware ou de l' Extensible Firmware Interface , s' ouvrant ainsi aux alternatives à Mac OS X sur Macintosh .
Ceux-ci permettent de faire tourner Microsoft Windows ou d' autres systèmes d' exploitation tout en restant sous Mac OS X et ce , presque à la vitesse maximale , en évitant une émulation matérielle coûteuse en ressources .
De son côté , Apple fournit le logiciel Boot camp avec Mac OS X .
Ce dernier permet d' installer facilement Windows XP , Windows Vista ou Windows 7 sur un Macintosh en fournissant les pilotes nécessaires au bon fonctionnement de la machine .
Des initiatives , tel que OSx86 , rendent cette installation partiellement possible .
En raison de l' incompatibilité des logiciels destinés à Windows avec Mac OS X , le catalogue de logiciels disponibles est plus restreint .
Sur le marché des ordinateurs , Apple a souvent représenté un marché de niche .
Alors que l' Apple II a atteint , au meilleur de sa forme , plus de 15 % de parts de marché , le Macintosh est longtemps resté sous la barre des 10 % .
Lors de son lancement en 1984 , le Macintosh occupait 6 % des ventes d' ordinateurs personnels , le plaçant derrière le Commodore 64 , les PC-DOS et les Apple II .
Les mauvaises ventes de l' année 1985 , conséquence de l' absence de nouveaux modèles , ne permettent pas au Macintosh de gagner des parts de marché .
En 1986 , l' arrivée du Macintosh Plus permet de légèrement inverser la tendance , mais les parts de marchés n' atteignent alors plus que 4,2 % .
Cette même année , les Compatible PC atteignent pour la première fois la barre des 50 % des ordinateurs personnels vendus .
À la fin de l' année 1980 , les PC continuent à gagner des parts de marchés , essentiellement au détriment des concurrents du Macintosh -- principalement en devenant la référence pour les jeux vidéo en lieu et place de l' Amiga -- , ce qui permet aux Macintosh d' occuper la deuxième place sur le marché des ordinateurs personnels .
En 1991 , les parts de marché des Macintosh passent de 5 ou 6 % à plus de 11 % .
Ceci coïncide d' un côté avec une baisse des ventes de PC , résultat d' un manque de nouveauté , et , du côté du Macintosh , avec l' arrivée du Système 7 , des Macintosh plus abordables avec les LC et Classic , et des Macintosh aux performances en forte hausse avec l' arrivée des Quadra .
En 1993 , les parts de marchés du Macintosh atteignent un sommet avec 13 % .
Le bonheur des uns faisant le malheur des autres , les autres concurrents du PC disparaissent quasiment du paysage des ordinateurs personnels , laissant les Macintosh et les PC seuls sur le marché .
À partir de 1994 , Apple perd des parts de marché au profit des PC , et ce jusqu' en 1999 , et l' arrivée de l' iMac .
En effet , et bien que les ventes de Macintosh soient en hausse , avec 4.5 millions d' unités vendues en 1995 , un record pour la firme à la pomme à l' époque , l' explosion du marché à partir de cette année avec l' arrivée de Windows 95 profite bien plus aux PC .
Ainsi , les parts de marchés , mais aussi les ventes , sont en baisse du côté d' Apple , pour atteindre 2.8 millions d' unités vendues en 1998 , contre presque 100 millions de PC vendus la même année .
En 1999 , et grâce au succès de l' iMac , les ventes de Macintosh s' améliorent légèrement , pour se stabiliser à 2,7 % de parts de marché , avec 3.5 millions d' unités vendus .
En 2001 , avec l' arrivée de Windows XP et malgré la sortie de Mac OS X , les ventes diminuent , atteignant 2 % , soit un peu plus de 3 millions d' unités vendues en 2004 , laissant la plate-forme PC , essentiellement équipée de Windows , dominer le marché , et ce malgré les alternatives que sont Linux ou AmigaOS , .
Pour accompagner le lancement des ses différents Macintosh , Apple a fait appel de manière récurrente à la publicité .
Après le succès de 1984 , Apple recommence l' année suivante avec Lemmings , une publicité pour son Macintosh Office qui échoue , car elle est ressentie comme offensant les acheteurs potentiels .
Lors de la sortie de Windows 95 , Apple répond par une campagne de dénigrement du système de Microsoft .
Un spot télévisuel est diffusé dans la même lignée : un conférencier est confronté à l' impossibilité de lancer sa présentation sous Windows 95 .
En 1997 , peu après le retour de Steve Jobs à la tête de l' entreprise , Apple lance la campagne Think different , censée redorer son blason , terni par le déclin qu' a connu la firme au milieu des années 1990 .
Think different devient le slogan d' Apple jusqu' en 2002 , année où la campagne Switch lui succède .
Cette dernière met en scène de simples utilisateurs qui ont " switché " ( " sont passés " ) sur un Macintosh et qui racontent leurs déboires avec leur PC .
Depuis 2006 , Apple fait la promotion de ses Macintosh à travers la campagne Get a Mac .
À travers une petite discussion entre les deux protagonistes , les avantages du Macintosh sont mis en avant et les défauts de son concurrent ( Windows ) sont mis en exergue .
En plus de la publicité conventionnelle , Apple organise des conférences pour faire la présentation et la promotion de ses nouveaux produits , dont les Macintosh .
Une large communauté s' est rapidement formée autour du Macintosh dès son apparition , certains se rassemblant sous forme de groupes d' utilisateurs .
Des salons sont organisés pour la communauté en commençant par la Macworld Expo , dont la première édition a lieu à San Francisco en 1985 .
Souvent d' abord limitée aux Macintosh , leur ligne éditoriale s' est étendue avec la diversification d' Apple , notamment avec la sortie de l' iPod et de l' iPhone .
Le Macintosh a aussi inspiré les créateurs de séries d' animation ; c' est le cas par exemple de Futurama et des Simpson , toutes deux créées par Matt Groening , où les ordinateurs qui apparaissent dans plusieurs épisodes s' inspirent grandement des certains modèles de Macintosh .
