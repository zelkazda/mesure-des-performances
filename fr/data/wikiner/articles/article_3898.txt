Issu d' une famille catholique flamande d' industriels , Jacques Brel a été un enfant peu intéressé par l' école , excepté par les cours de français .
Avec son frère , Pierre , de 6 ans son aîné , Jacques connaît une éducation austère entre collège catholique et scoutisme .
Son émigration est à l' origine du prénom de sa deuxième fille France , née le 12 juillet 1953 .
Ce dernier , témoin des premiers jours du débutant , l' accompagnera d' ailleurs lors de son premier passage à l' Olympia en " lever de rideau " ( moment où les spectateurs entrent dans la salle et s' installent à leur place ) .
Les conditions de travail sont difficiles pour Jacques : il n' avait pas de loge et devait se changer derrière le bar de l' Olympia .
À la fin d' une représentation , Bruno Coquatrix qui l' a remarqué , le félicite de sa prestation , l' invitant à lui rendre visite pour discuter d' un prochain passage .
Pour Jacques Brel , les difficultés continuent , encombré qu' il est de ses longs bras , de son grand corps maladroit .
En janvier 1955 , Brel fait ses débuts à l ' " Ancienne Belgique " , célèbre salle de concert bruxelloise , dans l' avant-programme de Bobbejaan Schoepen , et Jacques Canetti continue de l' envoyer dans des tournées où il se produit notamment en vedette américaine de Philippe Clay , Dario Moreno et Catherine Sauvage qui devient son amante .
En 1955 , il fait venir sa femme et ses deux fillettes à Paris .
Ils s' installent à Montreuil .
En 1956 , il rencontre François Rauber , un pianiste , qui devient son accompagnateur .
Les deux musiciens resteront fidèles à Brel et à son œuvre , au-delà même de sa mort , luttant vainement contre la publication de cinq inédits en 2008 que Brel jugeait lui-même inaboutis .
Petit à petit , Brel trouve son style , donc son public et connaît enfin le succès lors de ses spectacles , et ce malgré la vague des yéyé .
Dès lors , les tournées s' enchainent à un rythme infernal , Brel faisant parfois plus de concerts qu' il n' y a de jours dans l' année .
En mars 1962 , Brel quitte la maison de disques Philips pour Barclay ( avec qui il signera un contrat exceptionnel de trente ans en 1972 ) .
Le 6 de ce mois , il enregistre un de ses titres les plus célèbres , Le Plat Pays , hommage à son pays natal .
Pour autant il honore ses contrats pendant encore plus d' un an et fait ses adieux à l' Olympia .
Le 16 mai 1967 , il donne son dernier récital à Roubaix .
Mais il ne reste pas inactif pour autant : durant l' été 1967 , il joue dans son premier long métrage , Les Risques du métier du réalisateur André Cayatte ; le film est un succès public .
En octobre 1968 , à Bruxelles , il crée la version francophone de L' Homme de la Mancha , interprétant le rôle titre de don Quichotte au côté de Dario Moreno dans le rôle de Sancho Pança .
Au début de l' été 1969 , Brel interprète le rôle de Mon oncle Benjamin , dans le film d' Édouard Molinaro , dont il compose également la musique avec François Rauber .
Le succès l' attend aux États-Unis d' Amérique et au Royaume-Uni .
Jacques Brel is alive and well and living in Paris est une comédie musicale américaine qui sera jouée dans le monde entier pendant plusieurs années .
Elle comprend des traductions à rimes , assemblées par un ami de Jacques Brel , Mort Shuman , en 1968 .
Il décide de se retirer aux Marquises .
En 1977 , malgré la maladie , il revient à Paris pour enregistrer son dernier 33 tours qui paraît le 17 novembre , avec un record d' un million de précommandes .
Il retourne aux îles Marquises après cet enregistrement , avant que , en juillet 1978 , son cancer du poumon ne s' aggrave .
Il est rapatrié en France métropolitaine où il meurt le 9 octobre 1978 à l' hôpital Avicenne de Bobigny .
Jacques Brel repose au cimetière d' Atuona à Hiva Oa , dans l' archipel des îles Marquises , non loin de la tombe de Paul Gauguin .
En décembre 2005 , il est élu au rang du plus grand Belge de tous les temps par le public de la RTBF .
