Skye Edwards a quitté le groupe en 2003 , en raison de divergences musicales , selon les membres du groupe .
Elle a été remplacée en 2005 , pour l' album The Antidote par la chanteuse Daisy Martey .
Skye fait son retour dans la formation en 2010 pour le septième album Blood Like Lemonade .
La voix de Skye est rapidement devenue la marque de fabrique de Morcheeba .
Daisy Martey rejoint le groupe en 2005 après le départ de Skye Edwards , qui se lançait alors dans une carrière solo .
La voix de Skye Edwards était devenue un élément primordial du son de Morcheeba dans les esprits de nombre de fans du groupe , aussi ce changement engendra-t-il des réactions partagées .
L' album Blood Like Lemonade , sorti le 7 juin 2010 , marque le retour de Skye Edwards au sein du groupe et un retour aux sources musicales du groupe .
