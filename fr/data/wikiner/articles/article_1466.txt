C' est une préfiguration de la description du chan que l' on prêtera à Bodhidharma : " pas d' écrit , un enseignement différent ( de tous les autres ) , qui touche directement l' esprit pour révéler la vraie nature de bouddha " ( " 不立文字、教外別傳 , 直指人心，見性成佛 " ) .
Liste rapportée par la tradition des vingt-huit patriarches de l' école avant son arrivée en Chine et liste des sept premiers patriarches du chan chinois :
Bodhidharma , vingt-huitième patriarche dans la filiation indienne , serait venu en Chine autour de 520 [ réf. nécessaire ] .
Les différents textes chinois qui le mentionnent ne s' accordent pas exactement sur son origine ( Kanchipuram au sud de l' Inde ou Perse ) , ni sur sa route .
La Corée influença fortement tous les arts qui furent par la suite affiliées au Zen tel qu' on le connaît et reconnaît aujourd'hui .
Notamment les arts esthétiques et les arts martiaux , héritage direct d' une Chine florissante et profondément attachée à la justesse de la voie .
En fait il y aura dix temples , cinq à Kyōto et cinq à Kamakura , qui varieront au fil du temps .
Le courant zen et la pratique du zazen ( méditation assise pratiquée pour atteindre l' éveil ) eurent beaucoup de succès au Japon et s' accompagnèrent du développement par les moines de plusieurs arts et techniques , soit directement importés de Chine , soit créés localement en intégrant des éléments du nord de la Chine et de la Corée .
La tradition en fait le texte de référence de Bodhidharma ; plus récemment , D.T. Suzuki l' a abondamment commenté .
