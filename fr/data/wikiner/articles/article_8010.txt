La Fédération française de spéléologie compte d' ailleurs de plus en plus d' adeptes de la descente de canyon .
Depuis octobre 2004 cependant , la plus grande profondeur atteinte par des spéléologues en cavités naturelles se situe dans le gouffre Krubera-Voronja , localisé en Abkhazie , province occidentale sécessionniste de la Géorgie .
En septembre 2008 , un nouveau record a été établi dans cette faille d' Abkhazie , à 2191 mètres .
