Le prix Victor Rossel est un prix littéraire de Belgique institué en 1938 par le journal Le Soir afin de rappeler le souvenir du fils de son fondateur et pour récompenser un roman ou un recueil de nouvelles belge .
Il constitue un des événements majeurs de la vie littéraire en Belgique .
