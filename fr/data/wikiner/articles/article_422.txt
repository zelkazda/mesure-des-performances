C' est l' un des six départements formant la région Centre et son chef-lieu départemental est Chartres .
Il tire son nom des deux principales rivières qui le traversent , l' Eure , affluent de la Seine , et le Loir , affluent de la Sarthe .
L' Insee et la Poste lui attribuent le code 28 .
Le département a été créé à la Révolution française , le 4 mars 1790 en application de la loi du 22 décembre 1789 , à partir , principalement , de parties des anciennes provinces de l' Orléanais ( Beauce ) et du Maine ( Perche ) , mais aussi de l' Île-de-France .
Les terres d' Eure-et-Loir , par leur intérêt stratégiques , sont donc très tôt ancrées dans la mouvance capétienne et progressivement rattachées aux anciennes provinces de l' Orléanais et de l' Île-de-France .
Le traité de Brétigny , qui met fin provisoirement à la guerre y sera signé près de Chartres .
Le département est également marqué par la présence de Maximilien de Béthune , duc de Sully , décédé en son château de Villebon et inhumé à Nogent-le-Rotrou .
À la Révolution , il est dans un premier temps envisagé de créer un département beauceron .
Chartres conserve essentiellement sa vocation commerciale grâce à son important marché au blé et au commerce de la laine des nombreux élevages de moutons , dont la foire de Châteaudun est aussi un haut lieu .
Le Perche qui s' est peu développé , et a connu une notable émigration vers le Québec les siècles précédents , devient une terre de nourrices réputées pour leur qualités maternelles auprès des familles aisées de Paris .
Dreux devient également une ville industrielle , en particulier après la crise du phylloxéra qui met définitivement à bas les vignes normandes .
Après le coup d' État du 2 décembre 1851 de Napoléon III , l' Eure-et-Loir fait partie des départements placés en état de siège afin de parer à tout soulèvement massif .
La ville de Chartres est partiellement détruite par un bombardement en 1944 qui toucha particulière sa bibliothèque .
La ville de La Loupe est quant à elle presque totalement sinistrée .
Au tournant des années 1980 , Dreux devient une ville politiquement singulière en élisant comme maire en 1977 , puis députée en 1981 , Françoise Gaspard , l' une des premières femmes politiques ayant assumé publiquement son homosexualité , puis en étant le théâtre d' une alliance en 1983 entre la droite locale et le Front national mené par Jean-Pierre Stirbois , dont la veuve Marie-France Stirbois est élue députée en 1989 .
Le blason ( non officiel ) du département d' Eure-et-Loir a été créé à partir de ceux du comté du Perche ( partie occidentale du département ) et de l' Orléanais .
Le conseil général est à droite depuis 1986 et présidé depuis 2001 par Albéric de Montgolfier ( UMP ) , à la tête d' une majorité de 19 membres sur 29 .
Trois députés , Laure de La Raudière , Gérard Hamel et Jean-Pierre Gorges sont membres de l' UMP .
Les trois sénateurs d' Eure-et-Loir , Gérard Cornu , Albéric de Montgolfier et Joël Billard , sont également membres de l' UMP .
Le MPF compte de son côté deux conseillers municipaux à Chartres et plusieurs élus dans des communes rurales .
Elle compte 10 conseillers généraux sur 29 , dont le leader est Jacky Jaulneau ( PS ) .
Le département d' Eure-et-Loir fait partie de la région Centre .
Le département a par ailleurs bénéficié de la création du Parc naturel régional du Perche .
La forêt , avec près de 72000 hectares , est également présente : Senonches et Dreux abritant les massifs les plus importants .
La vallée de l' Eure constitue également une trame verte et boisée qui contraste avec le plateau beauceron attenant .
L' Eure-et-Loir est limitrophe des départements du Loir-et-Cher , du Loiret , de l' Essonne , des Yvelines , de l' Eure , de l' Orne et de la Sarthe .
Le Perche-Gouët marque la transition entre la Beauce et le Perche .
Le relief et la disposition des cours d' eau dans le Perche et en Beauce peuvent se déduire à partir d' une carte .
Quoi qu' il en soit , on constate donc , que dans le Perche , l' eau ruisselle en surface et qu' en Beauce , ce n' est pas le cas .
Le Perche se retrouve donc plus haut que la Beauce .
D' autre part , les anciennes failles hercyniennes ( ici du Massif Armoricain ) ont rejoué et permis l' affaissement du sommet du bourrelet .
Dans le calcaire de Beauce , on trouve des fossiles , notamment , des planorbes et des limnées ( des espèces d' eau douce -- qui existent toujours -- ) : il y avait donc un lac en Beauce , c' est lui qui a permis la formation de ce calcaire .
Ainsi , en Beauce , le calcaire se dissout facilement et permet à l' eau de ruissellement de s' infiltrer et il n' y a pas ( ou peu ) de rivières .
Les constructions anciennes montrent en général le contenu du sous-sol : en Beauce , les anciennes maisons sont en calcaire , dans le Perche , elles sont en torchis , en conglomérat appelé " grison " ( morceaux de silex cimenté par du calcaire et de l' argile ) , en brique et en grès " roussard " ( il est de couleur rousse , car assez riche en fer oxydé : la rouille ) .
L' Eure-et-Loir est un département de tradition agricole ( Beauce ) mais aussi en pointe dans trois filières économiques :
Le Centre et le sud du département connaissent une évolution moins favorable .
Si la région de Courville-sur-Eure , reliée à Paris et Chartres par le rail et proche de l' autoroute A11 se maintient bien , les secteurs de Châteaudun et d' Illiers-Combray , mal desservis , subissent un solde démographique nul , voire négatif .
