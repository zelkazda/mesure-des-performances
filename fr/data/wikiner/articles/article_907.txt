Martin Heidegger ( prononcé [ ˈaɪdœɡœʁ ] ou [ ˈaɪdøɡœʁ ] , parfois [ ˈaɪdeɡɛʁ ] ) , né le 26 septembre 1889 et mort le 26 mai 1976 , est un philosophe allemand .
D' abord disciple d' Edmund Husserl et de la phénoménologie , il s' achemina rapidement vers la question de l' être ou ontologie , qu' il répète dans une sorte de retour à Aristote .
Martin Heidegger est né à Messkirch ( Allemagne ) le 26 septembre 1889 .
Puis , à l' Université de Fribourg-en-Brisgau , il suit un enseignement soutenu en théologie , des cours de philosophie , mais aussi de mathématiques et de sciences .
Après la Première Guerre mondiale , il devient l' assistant personnel de Husserl , avec qui il partage les réflexions et les recherches sur la phénoménologie .
L' année suivante , il fait la connaissance de Hannah Arendt , une élève avec qui il a une liaison clandestine à laquelle le départ de celle-ci pour Fribourg mettra un terme .
Lors des élections de 1932 , il vote pour le parti national-socialiste ( NSDAP ) , y adhère l' année suivante et y reste jusqu' en 1945 .
En 1945 , à la fin de la Seconde Guerre mondiale il est interdit d' enseigner par les vainqueurs de la guerre .
Il meurt le 26 mai 1976 à Fribourg-en-Brisgau .
L' intention de Heidegger pourrait se résumer par une déconstruction ( terme auquel il va donner un nouveau sens pour l' occasion ) de la métaphysique occidentale afin d' y reformuler une ontologie .
L' importance fondamentale de Heidegger tient à ce fait : en lui , l' histoire de la philosophie s' auto-examine , se récapitule et se clôt par un geste hardi où elle se transcende en s' abolissant .
C' est en ce sens , qu' il se tourne à partir des années 1936 , vers la question de la poésie notamment à partir d' une lecture attentive de Hölderlin .
Les sources de Martin Heidegger sont très diverses , et sont surtout centrées sur son intérêt de la question de l' être , dont il dit que toute la métaphysique occidentale a oublié l' expérience originaire .
D' où sa relecture herméneutique des grands auteurs de la tradition philosophique , plus particulièrement Héraclite , Parménide , Platon , Aristote , Kant , Hegel , Nietzsche , de la pensée desquels il diagnostiquera une constitution ontothéologique .
Heidegger étudie d' abord la théologie avant de s' orienter vers la philosophie .
À la fin des années 1910 , il prend ses distances avec le catholicisme , avec qui il restera toutefois en dialogue constant , et se concentre sur l' étude des penseurs protestants , essentiellement Martin Luther et Søren Kierkegaard .
Son cours d' hiver 1920 porte sur Saint Paul , et celui de l' été 1921 sur Saint Augustin .
En 1924 , il prononce une conférence sur le péché chez Luther .
Ses écrits les plus tardifs sont , eux aussi marqués par cette provenance : Le principe de raison , cours de 1955 , analyse longuement les vers du mystique Angelus Silesius , et la sérénité est une notion héritée de Maître Eckhart .
La référence à Leibniz est moins présente , mais Heidegger lui consacre quelques séances de son séminaire en 1928 , et trouve chez lui la question fondamentale de la métaphysique : " Pourquoi il y a l' étant et non pas plutôt rien ?
À partir de 1929 , Heidegger se tourne vers l' idéalisme allemand .
Elle marque un tournant important de la philosophie continentale ( Lévinas -- qui s' opposa pourtant à Heidegger , sur la question de l' ontologie , considéra à sa lecture qu' elle faisait partie des grands textes philosophiques imprescriptibles ) .
Elle interroge le sens de l' être à partir d' une déconstruction de l ' être-là de l' homme , thème fondamental de l' ontologie , définie par Aristote comme étant la question de l' être en tant qu' être .
Il consacre son séminaire d' été 1932 à l' étude d' Anaximandre et Parménide .
Des cours sont encore par la suite consacrés à Parménide ( 1942/43 ) et à Héraclite ( 1943 et 1944 , puis en 1966-1967 en commun avec Eugen Fink ) .
Enfin , Heidegger consacre , à partir des années trente et jusqu' à la fin de sa vie , de nombreuses études à la poésie .
En 1958 , il prononce une conférence sur Hebel .
Enfin , en 1967 , puis en 1970 , Heidegger assiste à Fribourg aux lectures de Paul Celan ( sur cette rencontre , cf .
Heidegger ne proposerait que des définitions " neutres " de l' existence humaine , à partir desquelles aucune analyse politique ne peut être élaborée , ni aucune décision prise par rapport à une conjoncture historique et politique .
Selon lui , Heidegger favorise ainsi l' indifférence en politique .
Heidegger souligne les puissances d' existence factices que sont la communauté et le peuple .
Et , l' intrication des données factices de toute existence humaine individuelle et de l' histoire en devenir d' un peuple , d' une communauté , est nommée par Heidegger , " destin " .
Emmanuel Levinas parle de " la dette [ que ] tout chercheur contemporain [ doit ] à Heidegger -- dette qu' il lui doit souvent à regret " .
Mais de grands noms issus de ce courant ont été influencés par la pensée du philosophe allemand , notamment Richard Rorty .
Il rencontre Jacques Lacan chez qui il séjourne .
Il fut un penseur de référence pour une pléiade d' auteurs tels Jean-Paul Sartre , Maurice Merleau-Ponty , Alexandre Kojève , Paul Ricœur , Emmanuel Lévinas , Michel Henry , Jean-Luc Marion , Claude Romano , dans la lignée de la phénoménologie et des philosophies de l' existence ( existentialisme ) .
Également pour les grands noms du structuralisme Lacan , Foucault , Althusser .
Beaucoup d' écrivains comme Maurice Blanchot , Georges Bataille , René Char , Roger Munier , Michel Deguy .
Dans le cadre des études aristotéliciennes , Pierre Aubenque , puis Rémi Brague ou des platoniciens , Jean-François Mattéi .
Dans la perspective de la déconstruction de la métaphysique , Jacques Derrida , et ses propres élèves : Jean-Luc Nancy , Philippe Lacoue-Labarthe .
Dans une perspective de philosophie de la rhétorique , Barbara Cassin .
Il a influencé également Gérard Granel .
La controverse fut notamment lancée par Karl Löwith en 1946 , dans la revue les Temps modernes et se poursuit encore aujourd'hui .
En 1945 Heidegger propose une explication de son attitude :
