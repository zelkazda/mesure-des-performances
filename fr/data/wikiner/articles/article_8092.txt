Les morilles sèches ( trempées deux bonnes heures dans l' eau tiède ) peuvent être utilisées toute l' année , soit pour relever une blanquette de veau , soit pour accompagner un poulet ou un coq au vin jaune , soit pour farcir les chapons de Noël .
Ceci constitue un délit en France , cette dénomination ayant été interdite par décret en 1991 .
