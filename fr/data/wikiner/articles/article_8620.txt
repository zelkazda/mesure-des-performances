Avant l' arrivée de Christophe Colomb , le Honduras faisait partie d' une région que les historiens appellent Mésoamérique .
Le conquistador espagnol , Hernan Cortes arriva en 1524 .
Les Espagnols commencèrent à bâtir des colonies le long des côtes .
Le Honduras fut inclus dans la Capitainerie générale du Guatemala .
Les villes de Comayagua et Tegucigalpa se développèrent très tôt en raison de l' activité minière voisine .
En fait , les pays de la région restaient associés pour former les Provinces Unies d' Amérique centrale .
Les pays membres étaient : le Guatemala , le Salvador , le Honduras , le Nicaragua et le Costa Rica .
Toutefois , les grandes différences sociales et économiques entre le Honduras et les pays voisins attisèrent les tensions entre les différents pays .
Ainsi , San Pedro Sula s' industrialisa considérablement et devint la seconde ville du pays en nombre d' habitants .
Un bref conflit éclate entre le Salvador et le Honduras en juillet 1969 après que des rencontres de football ont exacerbé les tensions entre ces deux pays ( voir Guerre de cent heures ) .
Le Honduras sert alors aux États-Unis pour déstabiliser le Salvador et le Nicaragua .
Le dispositif d' influence américain fut complété l' année suivant par la nomination de John Negroponte comme ambassadeur .
Comme aux États-Unis , les élections ont lieu en novembre , mais les présidents entrent en fonction en janvier , d' où une présidence qui va de janvier 1982 à janvier 1986 .
Assez logiquement au regard de ce qui précède , après la visite de Ronald Reagan en décembre 1982 , les aides économiques et militaires américaines connurent une forte augmentation .
Déjà confronté à l' insécurité liée au développement de camps de réfugiés venant du Nicaragua et du Salvador , cette épisode explique sans doute le renforcement de sa propension à s' appuyer sur l' armée , qui malgré le retour au pouvoir civil , est de plus en plus présente dans la société .
Carlos Roberto Flores Facussé devint président le 27 janvier 1998 .
L' ouragan Mitch dévasta le pays en octobre 1998 .
Les pays occidentaux envoyèrent de l' aide au Honduras .
Le dimanche 28 juin 2009 à l' aube , le président du Honduras , Manuel Zelaya , est arrêté et expulsé .
Le président vénézuélien Hugo Chavez a placé ce même jour ses troupes en état d' alerte à la suite de ce coup et fait savoir qu' il riposterait militairement au cas où son ambassadeur dans ce pays serait attaqué ou enlevé .
Quant au président américain Barack Obama , il a demandé le strict respect des normes démocratiques au Honduras .
