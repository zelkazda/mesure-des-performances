Le Costa Rica , ou la République du Costa Rica pour les usages officiels , est un pays d' Amérique centrale , bordé par le Nicaragua au nord , le Panama au sud-est , l' océan Pacifique à l' ouest et au sud et la mer des Caraïbes à l' est .
Le Costa Rica est le premier pays à avoir constitutionnellement supprimé son armée .
La capitale est San José .
Christophe Colomb arrive dans la région au cours de son dernier voyage en 1502 .
Le Costa Rica est ensuite gouverné pendant près de trois siècles par la Capitainerie générale du Guatemala pour le compte de l' Espagne .
Le Costa Rica est partagé en sept provinces , divisées elles-mêmes en 81 cantons , eux-mêmes divisés en 470 districts :
Le Costa Rica est organisé topographiquement en trois grands secteurs : une succession de cordillères ( cordillère volcanique de Guanacaste , cordillère volcanique centrale , et cordillère de Talamanca qui culmine à 3820 m au Cerro Chirripó ) d' axe nord-ouest sud-est sépare les plaines lavées de la côte caraïbe , de la côte pacifique accidentée .
Dans la plaine élevée centrale du secteur de la cordillère , le Costa Rica est densément peuplé avec San José , Alajuela , Cartago et Heredia , quelques-unes des plus grandes villes du pays .
San José , la capitale , compte 2 millions d' habitants avec sa grande banlieue .
Le climat du Costa Rica est marqué par une saison sèche ( décembre à avril ) et une saison des pluies ( mai à novembre ) .
Le Costa Rica est situé dans la zone intertropicale ( entre 8 et 11° de latitude nord ) .
Toutefois , d' un endroit à un autre , les précipitations diffèrent considérablement : les précipitations à San José sont de 1867 mm par an , alors qu' à Puerto Limón sur la côte caraïbe , il tombe 3518 mm de pluie par an .
Le Costa Rica possède une flore et une faune exceptionnelles , puisque près de 5 % de la biodiversité mondiale s' y trouve ( pour un pays qui ne représente que 0,03 % des surfaces émergées ) .
Les réserves marines du Costa Rica sont le théâtre de braconnages de requins .
Les importants investissements taïwanais au Costa Rica laissent supposer que le gouvernement ne fait pas de la lutte contre cette activité illégale une priorité de l' agenda de préservation de la faune et de la flore .
Les influences océaniques de l' océan Pacifique et de la mer des Caraïbes , ayant chacun un climat particulier , jouent également un rôle .
Le Costa Rica se classe parmi les 14 nations à avoir placé plus de 23 % de leur territoire sous protection ( 25,6 % de parc nationaux ou réserves écologiques ) .
La situation géographique du Costa Rica dans l' isthme centre-américain permet un accès aisé au marché nord-américain , puisque se trouvant sur le même fuseau horaire que Dallas et Chicago aux États-Unis .
Le Costa Rica a pour codes :
