Il obtint le Prix Nobel de la paix en 1974 pour avoir mené une politique pacifiste et opposée à la prolifération nucléaire .
Il s' aligne souvent sur les États-Unis , sauf lorsque Richard Nixon visite la Chine .
Sa faction au sein du parti soutient plutôt Takeo Fukuda mais c' est le populaire Kakuei Tanaka qui devient premier ministre .
Il reçoit , en compagnie de Sean MacBride , le Prix Nobel de la paix en 1974 pour la politique étrangère pacifique menée par son pays et pour son rôle dans le traité de non prolifération des armes atomiques .
Il meurt l' année suivante le 3 juin 1975 à Tōkyō à l' âge de 74 ans .
