Au milieu des années 1990 , le magazine Dragon commença à écrire " jeu de rôles " ( avec un s à rôle ) .
Actuellement , les deux orthographes sont utilisées , l' orthographe sans s est majoritaire dans les publications , l' orthographe avec un s est surtout utilisée par l' éditeur Asmodée ( on peut lire l' orthographe sans s dans la version 3 de Donjons et dragons et de lire l' orthographe avec un s dans l' édition 3.5 ) .
Gary Gygax créa dans cette mouvance un nouveau type de jeu de guerre lié à la parution du Seigneur des Anneaux en 1966 aux États-Unis .
Ce jeu de guerre , appelé Chainmail , mettait en jeu pour la première fois des créatures fantastiques et de la magie .
Ce jeu a été suivi par Tunnels & Trolls en 1975 et D & D a disposé , le premier , d' une traduction en français au début des années 1980 .
Il faut noter qu' entretemps était sorti le jeu de rôle le plus connu , AD & D ( TSR ) qui a constitué l' essentiel des rôlistes des années 1970-1990 , et qui dès sa première édition de 1977 permettait de jouer dans un univers post-apocalyptique avec les mêmes règles .
Entre 1980 et 1982 parurent chez Iron Crown Enterprises les livrets de ce qui allait être considéré comme un jeu de rôle parmi les plus complexes , Rolemaster .
La première diffusion confidentielle de la version anglaise de Donjons et Dragons en France ouvrit la porte à une traduction , brisant ainsi la barrière de la langue .
Le succès de Donjons et Dragons et des autres jeux de rôle fut important : de 1974 à 1982 , plus de deux millions et demi d' exemplaires de Donjons & Dragons furent vendus .
Contre toute attente , dans les années 1980 , la traduction de L' Appel de Cthulhu remporte un large succès en France , alors qu' il n' a eu que peu de succès aux États-Unis .
À priori , l' explication tient au fait que les joueurs ne sont pas tenus à leurs personnages , font une enquête le temps d' une soirée , le tout baignant dans l' atmosphère des romans d' horreur de H. P. Lovecraft .
Bref , prenant à contre-pieds tous les piliers du genre de Donjons et Dragons .
Folio éditait également , toujours en format de poche : Les Terres de légende et L' Œil noir ( jeu d' origine allemande , introduit en France en 1984 ) .
Tous les jeux de cette époque connaissent encore aujourd'hui une certaine notoriété même si D & D reste " la référence du jeu de rôle " pour le grand public .
En 1991 est publié Vampire : la Mascarade , un jeu plus axé sur les personnages , leur personnalité et leur interprétation par les joueurs .
Ce jeu met en avant la notion d' approche " narrative " du jeu ( le but étant de bâtir une histoire , la logique est donc de faire primer la narration sur les jets de dés , le tout dans l' intérêt de l' ambiance ) , se focalise plus sur des conflits d' intérêts entre factions opposées ( donnant au jeu une saveur politique en conférant un rôle central aux relations interpersonnelles et intrigues qui en découlent ) et donc plus sur le " roleplaying " que sur la progression de puissance à la D & D .
Ce concept remporte un vif intérêt et beaucoup de rôlistes se détournent de D & D pour aller sur des jeux qu' ils considèrent , sans doute à raison , plus matures : l' atmosphère développée par Vampire : la Mascarade , dite " gothique-punk " , se veut résolument adulte .
Le premier ést Magic : l' assemblée , un jeu de cartes à thème médiéval fantastique .
" Chainmail " , " Rolemaster " , etc .
Toujours plus que par le passé , sur un marché qui a toujours été réduit , ces maisons d' édition sont nombreuses , mais produisent de moins en moins ; par ailleurs , on assiste à un développement assez dynamique des productions amateurs , désormais très facilement diffusables par Internet .
De même , il est beaucoup plus facile d' expliquer le jeu de rôle au grand public , juste en citant ses deux références les plus connues : le Seigneur des Anneaux et Donjons et Dragons .
C' est du " Cluedo grandeur nature " en quelque sorte .
Certains auteurs réutilisent en effet un univers déjà existant , issu de la littérature ( Le Seigneur des anneaux , Eragon , ... ) , du cinéma ( La Guerre des étoiles , la trilogie Matrix , ... ) ou des mangas ( Naruto , Saint Seiya , ... ) .
L' utilisation de la webcam est possible avec des logiciels comme Skype .
Les avantages : temps de transport en moins , pas de nécessité de local , recherche de joueurs sur Internet , enregistrer des parties dans l' état , garder l' historique pour faire un résumé en début de partie prochaine , .
En bande dessinée , nous avons déjà cité la série Donjon .
