Persée , qui revenait de sa victoire sur la Gorgone Méduse , trouva Andromède , tua le monstre et libéra la jeune fille .
Persée se maria avec Andromède bien qu' elle fût promise à Phinée , le frère de Céphée .
Lors du mariage , une querelle eut lieu entre les deux prétendants et Phinée fut à son tour changé en pierre grâce à la tête de la Gorgone .
Après sa mort , Andromède fut placée par Athéna parmi les constellations , près de Persée et de Cassiopée .
Sophocle et Euripide , et dans des temps plus modernes , Corneille , écrivirent le récit de sa tragédie , et sa mésaventure est le sujet de nombreuses œuvres d' art anciennes .
