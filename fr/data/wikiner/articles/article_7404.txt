Il semble avoir enseigné à Paris et il assista au concile du Latran en 1179 .
Il habita ensuite Montpellier , vécut quelque temps hors de la clôture monacale et prit finalement sa retraite à Cîteaux , où il mourut en 1202 .
