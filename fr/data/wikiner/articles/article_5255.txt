La découverte est parfois attribuée au mathématicien Hippase de Métaponte pour ces travaux sur la section d' extrême et de moyenne raison , maintenant appelée nombre d' or .
Pendant les cent années suivantes furent introduits les nombres imaginaires qui devinrent un outil puissant forgé par Abraham de Moivre , et plus particulièrement aiguisé par Leonhard Euler .
Au dix-neuvième siècle , la théorie des nombres complexes fut complétée , l' existence des nombres transcendants fut montrée , ce qui amena à diviser les nombres irrationnels en deux catégories , celle des nombres algébriques et celle des nombres transcendants et ainsi à effectuer une étude scientifique d' un sujet presque resté en léthargie depuis Euclide , celui de la théorie des nombres irrationnels .
L' année 1872 , vit la publication des théories de Karl Weierstrass , de Heine , de George Cantor , et de Richard Dedekind .
Méray avait pris en 1869 les mêmes points de départ que Heine , mais la naissance de cette théorie est généralement rattachée à l' année 1872 .
Weierstrass , Cantor , et Heine basèrent leurs théories sur les séries infinies , pendant que Dedekind fonda la sienne sur l' idée d' une coupure dans le système des nombres rationnels , partageant les nombres rationnels en deux classes caractérisées par des propriétés différentes .
Les fractions continues , étroitement liées aux nombres irrationnels , furent prises en considération par Euler , et au début du dix-neuvième siècle , elles prirent de l' importance grâce aux écrits de Joseph Louis Lagrange .
Lambert démontra en 1761 que ne pouvait être rationnel , et que est irrationnel si est rationnel ( sauf si ) .
La démonstration de l' irrationalité de de Lambert , est considérée comme incomplète selon les critères actuels .
En 1873 , George Cantor montra leur existence par une méthode différente , en démontrant que tout intervalle ayant au moins deux réels contient des nombres transcendants .
Charles Hermite ( en 1873 ) fut le premier à démontrer la transcendance de , et Ferdinand von Lindemann ( en 1882 ) , montra à partir des conclusions d' Hermite , la transcendance de .
La démonstration de Lindemann fut largement simplifiée par Weierstrass ( en 1885 ) , et encore davantage par David Hilbert ( en 1893 ) , pour finalement devenir élémentaire grâce à Adolf Hurwitz et Paul Albert Gordan .
