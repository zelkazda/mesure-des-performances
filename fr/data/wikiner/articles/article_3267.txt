Le Gouvernement provisoire de la République française ( GPRF ) décide de procéder le 21 octobre 1945 à un double référendum sur la question des institutions , et à une élection législative .
Les élections législatives , qui ont lieu le même jour , mettent en avant trois partis principaux : le Parti communiste français ( PCF ) ,qui a tiré un grand prestige de son rôle de force de résistance et de la victoire de l' URSS , le Mouvement républicain populaire ( MRP ) d' inspiration démocrate-chrétienne et le Parti socialiste ( SFIO ) .
De Gaulle se méfie des communistes , alors qu' ils sont arrivés en tête , et ne leur accorde que cinq ministères .
On retrouve dans le gouvernement de De Gaulle des personnalités politiques telles que Vincent Auriol , Edmond Michelet ( ministre des armées ) ou André Malraux ( ministre de l' information ) .
Le socialiste Félix Gouin succède à Charles de Gaulle .
Le PCF obtient 26 % des voix mais est dépassé par les centristes du MRP qui totalisent 28,2 % des suffrages .
Les deux présidents de la Quatrième République sont Vincent Auriol , ancien ministre socialiste du Front populaire , et René Coty , classé au centre-droit .
L' Assemblée nationale ( chambre basse ) est élue au suffrage universel direct pour cinq ans : la majorité électorale est fixée à 21 ans et les femmes sont intégrées pour la première fois au corps électoral .
La Seconde Guerre mondiale laisse le pays dans une situation économique désastreuse .
Certaines villes sont pratiquement rasées comme Le Havre , détruite à 80 % .
Pendant la période de reconstruction , la population française va devoir accepter un certain nombre de sacrifices pour en payer le prix : maintien du rationnement jusqu' en 1949 , habitation en baraquements provisoires pour les 5 millions de Français qui ont vu leur logement détruit , baisse du pouvoir d' achat , dévaluation du franc .
Dans ces conditions , la reconstruction de l' infrastructure et de l' économie française est à mettre au crédit de la Quatrième République qui parvient rapidement à ramener la croissance .
Les secteurs les plus stratégiques sont carrément nationalisés : énergie , transports , assurances , Banque de France et banques de dépôt .
La planification gouvernementale ( plan Monnet ) est financée par l' aide américaine à travers une substantielle remise de dettes puis le plan Marshall qui apporte plus de 2,6 milliards de dollars au pays .
La reconstruction des villes permet la mise en pratique des théories de l' urbanisme moderne à l' instar notamment de Le Corbusier et des concepts développés lors des congrès internationaux d' architecture moderne .
Une fois la reconstruction achevée , Edgar Faure , qui conduit la politique financière de juin 1953 à janvier 1956 , cherche à orienter l' investissement vers les biens de consommation .
Le retour au pouvoir des socialistes en 1956 est l' occasion d' accorder aux salariés une troisième semaine de congés payés , qui vient s' ajouter aux deux premières accordées sous le Front populaire .
La quatrième République instaure également les retraites , les bourses d' études et le salaire minimum .
La fin de la Seconde Guerre mondiale marque également les prémices de la Guerre froide .
Si cette recherche en armement , qui sera finalisée sous de Gaulle au début des années 1960 , est antérieure à la Quatrième République , c' est sous celle-ci que sont atteints les principaux paliers dans la constitution d' armes .
Les affaires étrangères sont conduites de 1947 à 1954 par deux personnalités du MRP : Georges Bidault et Robert Schuman .
Après le désastre de Dien Bien Phu en 1954 , Pierre Mendès France tente de résoudre les problèmes que la faiblesse du pouvoir exécutif à laissé empirer .
La France prend conscience de l' intérêt de la construction européenne pour peser davantage : elle doit pour cela surmonter le syndrome qu' ont causé trois grandes guerres avec l' Allemagne .
Six ans plus tard le Traité de Rome dessine les bases de l' actuelle Union européenne .
Par cette union continentale la France peut espérer retrouver quelque peu sa puissance passée en créant une nouvelle aire d' influence .
Le mode de scrutin ainsi que la prédominance de l' Assemblée nationale au sein des institutions entraînent une dérive progressive du régime parlementaire vers un régime d' assemblée .
Un consultant français de la RAND Corporation , Constantin Melnik , établira dans une étude interne que la Quatrième République connaît , en moyenne , un jour de crise ministérielle sur neuf !
Une coalition se forme entre tous les partis hostiles d' une part au communisme et d' autre part au général de Gaulle , qui vient de fonder le Rassemblement du peuple français ( RPF ) .
Mais la troisième force est un regroupement par défaut et les rivalités de personnes ne laissent aucune marge de manœuvre pour prendre des initiatives. Henri Queuille est la principale personnalité des gouvernements de septembre 1948 à août 1951 : son nom restera attaché dans une partie des esprits à l' immobilisme de cette période .
La SFIO confirme son déclin et le MRP perd la moitié de son électorat , principalement au profit du RPF .
La nouvelle majorité de centre-droit se rassemble pour la première fois autour d' Antoine Pinay de mars à décembre 1952 .
C' est finalement Edgar Faure qui s' impose à la tête du gouvernement par souplesse et habilité .
Il espère que la dissolution du RPF par de Gaulle va permettre de renforcer la majorité de centre droit .
La majorité sortante de centre-droit est mise en échec par l' émergence à droite du mouvement protestataire de Pierre Poujade .
À gauche , un mouvement d' opinion se fait jour en faveur de Pierre Mendès France .
Il a le soutien de L' Express , le premier grand magazine d' information fondé par Jean-Jacques Servan-Schreiber .
Son gouvernement doit se consacrer à la guerre d' Algérie qui s' enlise sur le plan militaire , cause un vif débat moral sur la légitimité de ses objectifs et de ses méthodes , et provoque une sérieuse crise financière .
Une nouvelle crise gouvernementale particulièrement grave a lieu le 15 avril 1958 avec la chute du gouvernement dirigé par Félix Gaillard .
La dérive des finances publiques avait fait perdre à Guy Mollet le soutien des modérés mais il s' avère qu' aucune autre personnalité est en mesure de mobiliser une majorité stable .
Ainsi , le 27 mai 1958 , de Gaulle déclare " J' ai entamé hier le processus régulier nécessaire à l' établissement d' un pouvoir républicain capable d' assurer l' unité et l' indépendance du pays " ; il demande par ailleurs aux forces armées de ne prendre part à aucune manifestation .
Pierre Pflimlin , qui a rencontré secrètement de Gaulle en pleine nuit pour s' assurer de ses intentions ( sans rien obtenir d' ailleurs ) , démissionne finalement le 28 mai 1958 .
Il est adopté à une large majorité , instaurant la Cinquième République
Il doit son succès à la grande proportion de communistes dans la Résistance française au nazisme .
Le dirigeant emblématique du PCF sous la Quatrième République est Maurice Thorez ( 1900-1964 ) .
Créée en 1905 , la section française de l' Internationale ouvrière ( SFIO ) , ancêtre du Parti socialiste et portant déjà officieusement ce nom , fut l' un des partis majeurs de la Quatrième République .
La SFIO bénéficia d' une bonne popularité principalement grâce à l' image que les Français conservaient du Front populaire .
Cette union nationale des années 1930 sous l' égide de la SFIO resta gravée dans les mémoires et beaucoup espèraient une telle période de prospérité après la guerre .
La SFIO survécut jusqu' en 1969 , année où elle fusionna avec plusieurs petits mouvements politiques de gauche ( en particulier l' UCRG et la CIR ) pour créer le Parti socialiste .
Le Parti radical , ou Parti républicain , radical et radical-socialiste , créé en 1901 , est le plus vieux parti politique existant encore sous la Quatrième République .
Sous la Cinquième République , le Parti radical fut dissous entre le Parti radical valoisien , de droite et le Parti radical de gauche , bien qu' en 2007 un éventuel rapprochement des deux mouvements fut évoqué .
L' Union démocratique et socialiste de la Résistance ( UDSR ) est un des nouveaux partis de la Quatrième République .
L' UDSR , malgré son nom est généralement classé au centre ( voire au centre-droit ) à cause de ses positions travaillistes ou sociales-libérales .
L' UDSR exista jusqu' en 1967 où elle se fondit dans la Convention des institutions républicaines , celle-ci ayant été elle-même intégrée au Parti socialiste en 1971 .
Le Mouvement républicain populaire ( MRP ) est , comme l' UDSR , un nouveau parti issu des organisations de la Résistance .
Se rapprochant vers 1945 du MRP , il en diverge à propos des institutions à donner à la Quatrième République .
Se voulant un parti au-dessus des autres partis ( de Gaulle était parti du gouvernement en 1946 en dénonçant le " régime exclusif des partis " ) , il va inventer les bases du gaullisme .
Opposé dès sa création à la Quatrième République et à ses institutions , il sera l' un des deux partis , avec le PCF , à s' opposer à la troisième force .
Le RPF réalise tout de même de bons résultats aux élections législatives ( 21,6 % des voix aux élections de 1951 ) mais ne participe à aucun gouvernement .
Certains élus soutiennent les prérogatives des élus CNIP , ce qui aboutit à plusieurs scissions successives .
Après le retour au pouvoir de De Gaulle et la mise en place de la Cinquième République en 1958 , le gaullisme unifié au sein de de l' Union pour la nouvelle République ( UNR ) dominera la vie politique française jusque dans les années 1970 .
Quelques mouvements existent cependant , comme le PRL ou les Républicains indépendants , malgré un très faible score aux premières élections .
En 1949 , la plupart des courants de droite classique , lassés des alliances de centre-gauche ( tripartisme ) , se regroupent dans un parti unique , le Centre national des indépendants ( et paysans ) ( CNIP ) .
