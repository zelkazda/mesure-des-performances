Philippe Auberger est inspecteur des finances honoraire .
En 1995 , il fait partie des députés RPR dits " balladuriens " , qui soutiennent la candidature d' Édouard Balladur à l' élection présidentielle contre Jacques Chirac et Lionel Jospin .
Il préside le conseil de surveillance de la Caisse des dépôts et consignations de 2002 à 2007 .
Trois jours avant le second tour des élections municipales de mars 2008 , Philippe Auberger se trouve au centre d' une polémique après la diffusion d' une lettre " mettant intimement en cause sa vie privée " auprès de commerçants , d' entreprises , d' écoles et de professions libérales de la ville .
