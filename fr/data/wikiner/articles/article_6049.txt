Lui et Kurt Alder reçurent le prix Nobel de chimie en 1950 .
L' année suivante , il prit le poste de professeur de chimie à l' université de Kiel , qu' il ne quitta plus jusqu' à sa retraite en 1945 .
