L′ exploration de l′espace , parfois appelée conquête de l′espace consiste en l' exploration physique de l' espace , c' est-à-dire de tous les objets extérieurs à la Terre .
Depuis , bien que la conquête spatiale soit toujours largement dominée par des agences spatiales nationales ou internationales telles que la NASA ou l' ESA , plusieurs entreprises envisagent de développer des lanceurs commerciaux .
Officiellement , la conquête de l' espace a fait 24 morts à ce jour ( dont la chienne Laïka ) .
Cependant , de nombreuses personnes ont été tuées en ex- URSS , en République populaire de Chine et au Brésil par l' explosion de fusées sur le pas de tir ou par des fusées retombant au sol .
À ce jour , hormis la chienne Laïka , il n' y a jamais eu de mort dans l' espace .
La NASA rend hommage à ces victimes à travers un mémorial .
La Cité de l' espace informe [ réf. nécessaire ] qu' un chiffre tout à fait symbolique a été atteint le 10 décembre 2006 car un astronaute s' est élancé dans l' espace en tant que 1 000 e membre d' équipage d' une mission spatiale .
Il s' agit de Mark Polansky , 50 ans , pilote de la navette spatiale Atlantis pour la mission STS-98 en 2001 , un vol spatial à destination de la Station spatiale internationale ( ISS ) qui dura 13 jours .
Pour sa deuxième mission , STS-116 , il commande la navette Discovery qui a rejoint également l' ISS .
