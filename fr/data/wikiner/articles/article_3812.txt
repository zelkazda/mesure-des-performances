L' Univers en folie est un roman de science-fiction , écrit en 1949 par Fredric Brown ( États-Unis ) .
L' univers en folie est un joyau d' humour , parfois au second degré , ponctué de trouvailles amusantes ( la machine à coudre ouvrant la voie du voyage dans l' espace est une perle du genre ... ) et d' une réflexion mi-légère , mi-sérieuse sur la réalité de notre monde .
Cette veine humoristique décapante se poursuivra avec Martiens , go home ! en 1955 .
