Le Web n' est qu' une des applications d' Internet .
D' autres applications d' Internet sont le courrier électronique , la messagerie instantanée , Usenet , etc .
Le nom du projet originel était WorldWideWeb .
Les mots ont été rapidement séparés en World Wide Web pour améliorer la lisibilité .
Le nom World-Wide Web a également été utilisé par les inventeurs du Web , mais le nom désormais préconisé par le World Wide Web Consortium ( W3C ) sépare les trois mots sans trait d' union .
Le sigle WWW a été largement utilisé pour abréger World Wide Web avant que l' abréviation Web ne prenne le pas .
La prononciation laborieuse de WWW a sans doute précipité son déclin .
WWW est parfois abrégé en W3 , abréviation qu' on retrouve dans le sigle W3C du World Wide Web Consortium .
Cet article fait la distinction entre " le Web " et " un web " , aussi la majuscule est toujours utilisée pour désigner le Web .
En principe , l' expression désigne une certaine évolution de l' usage du Web , les sites étant devenus plus interactifs .
De même , pour décrire l' évolution du Web , de nombreux numéros de versions ont été proposés ( Web 1.0 pour désigner , par opposition , les débuts du Web ) -- certains pour tenter d' établir une terminologie cohérente , d' autres par parodie .
La terminologie propre au Web contient plusieurs dizaines de termes .
L' expression en ligne signifie " connecté à un réseau " , en l' occurrence le réseau informatique Internet .
Cette expression n' est pas propre au Web , on la retrouve à propos du téléphone .
C' est une chaîne de caractères permettant d' indiquer un protocole de communication et un emplacement pour toute ressource du Web .
Les hyperliens du Web sont orientés : ils permettent d' aller d' une source à une destination .
HTML ( pour HyperText Markup Language ) est un langage informatique permettant de décrire le contenu d' un document ( titres , paragraphes , disposition des images , etc . )
Un document HTML est un document décrit avec le langage HTML .
Les documents HTML sont les ressources les plus consultées du Web .
Le HTML est maintenant remplacé par le XHTML ( Extensible HyperText Markup Language ) .
L' expression surfer sur le Web signifie " consulter le Web " .
Elle a été inventée pour mettre l' accent sur le fait que consulter le Web consiste à suivre de nombreux hyperliens de page en page .
Le World Wide Web , en tant qu' ensemble de ressources hypertextes , est modélisable en graphe orienté avec les ressources pour sommets et les hyperliens pour arcs .
Du fait que le graphe est orienté , certaines ressources peuvent constituer des puits ( ou des cul-de-sac , moins formellement ) : il n' existe aucun chemin vers le reste du Web .
À l' inverse , certaines ressources peuvent constituer des sources : il n' existe aucun chemin depuis le reste du Web .
Les analyses ont montré que la structure du Web répondait au modèle des réseaux libres d' échelle présent dans la plupart des réseaux sociaux .
Cela se traduit par la présence de moyeux , les hubs , vers lesquels convergent les liens hypertextes : ce sont les sites les plus importants qui constituent le squelette du Web .
Techniquement , rien ne distingue le World Wide Web d' un quelconque autre Web utilisant les mêmes technologies .
L' exploration récursive du Web à partir de ressources bien choisies est la méthode de base programmée dans les robots d' indexation des moteurs de recherche .
Des études indiquent que la partie invisible du Web représente plus de 99 % du Web .
L' exploration récursive n' est pas le seul moyen utilisé pour indexer le Web et mesurer sa taille .
Cette mesure porte plus sur l' utilisation des technologies du Web que sur le Web lui-même .
Elle permet notamment de trouver des sites publics qui ne sont pas liés au World Wide Web .
Un Web disponible sur un intranet est privé .
Il est soit totalement séparé du Web , soit une source du Web .
Il est une source lorsque l' intranet est relié à Internet et qu' un hyperlien du Web pointe sur une ressource du Web .
Les liens depuis le Web sont en revanche impossibles car par définition un intranet n' offre pas d' accès public .
En ce cas , elle constitue un Web virtuellement privé , car le public ne peut pas le découvrir en suivant des hyperliens .
Le Web change constamment : les ressources ne cessent d' être créées , modifiées et supprimées .
Il existe quelques initiatives d' archives du Web dont le but est de permettre de retrouver ce que contenait un site à une date donnée .
Le projet Internet Archive est l' un d' eux .
Les divers types de ressource du Web ont des usages assez distincts :
Un document HTML contient uniquement du texte : le texte consulté , le texte en langage HTML plus d' éventuels autres langages de script ou de style .
La présentation de documents HTML est la principale fonctionnalité d' un navigateur Web .
HTML laisse au navigateur le soin d' exploiter au mieux les capacités de l' ordinateur pour présenter les ressources .
Les éléments multimédias proviennent toujours de ressources indépendantes du document HTML .
Les documents HTML contiennent des hyperliens pointant sur les ressources multimédias , qui peuvent donc être éparpillées sur Internet .
De nombreux navigateurs Web proposent la possibilité de greffer des logiciels ( plugin ) pour étendre leurs fonctionnalités , notamment le support de types de média non standard .
Les flux ( audio , vidéo ) nécessitent un protocole de communication au fonctionnement différent de HTTP .
L' usage du format de données JPEG est indiqué pour les images naturelles , principalement les photographies .
L' usage du format de données PNG est indiqué pour les images synthétiques ( logos , éléments graphiques ) .
L' usage du format de données GIF est indiqué pour les petites animations .
Pour les images synthétiques , la popularité ancienne de GIF le fait souvent préférer à PNG .
Cependant , GIF souffre de quelques désavantages , notamment la limitation du nombre de couleurs et un degré de compression généralement moindre .
En outre une controverse a entouré l' usage de GIF de 1994 à 2004 car Unisys a fait valoir un brevet couvrant la méthode de compression .
L' usage d' images de format de données XBM est obsolète .
Un script peut être intégré au document HTML ou provenir d' une ressource liée .
Le premier langage de script du Web fut JavaScript , développé par Netscape .
Ensuite Microsoft a développé une variante concurrente sous le nom de JScript .
Finalement , la norme ECMAScript a été proposée pour la syntaxe du langage , et les normes DOM pour l' interface avec les documents .
Le langage CSS a été développé pour gérer en détail la présentation des documents HTML .
Le texte en langage CSS peut être intégré au document HTML ou provenir de ressources liées , les feuilles de style .
Cette séparation permet une gestion séparée de l' information ( contenue dans des documents HTML ) et de sa présentation ( contenue dans des feuilles de style ) .
Lorsque le logiciel correspondant est disponible , les documents et images de tout type sont généralement automatiquement présentés , selon des modalités ( fenêtrage , dialogues ) dépendant du navigateur Web et du logiciel gérant le type .
Pour gérer les ressources de systèmes différents du Web comme le courrier électronique , les navigateurs font habituellement appel à des logiciels séparés .
Enfin , CSS permet de proposer différentes présentations , sélectionnées pour leur adéquation avec l' équipement utilisé .
Le W3C a pour cela créé des normes dans le but de permettre l' indépendance des outils qui servent à créer du contenu avec ceux qui servent à le lire .
L' accessibilité du Web pour les individus handicapés est aussi l' objet d' attentions particulières comme la Web Accessibility Initiative .
Toute page du Web peut contenir un hyperlien vers toute autre ressource accessible d' Internet .
Cette conception décentralisée devait favoriser , et a favorisé , une augmentation rapide de la taille du Web .
Sans ces sites , la recherche d' information dans le Web serait extrêmement laborieuse .
Les standards de codage de caractères et les formats d' image numérique GIF et JPEG ont été développés indépendamment .
Trois technologies ont dû être développées pour le World Wide Web :
Ces premières technologies ont été normalisées comme les autres technologies d' Internet : en utilisant le processus des Request for Comments .
Comme ces techniques ne sortent pas du serveur , elles ne sont pas standardisées par le World Wide Web Consortium .
Tim Berners-Lee travaille comme informaticien à l' Organisation européenne pour la recherche nucléaire ( CERN ) lorsqu' il propose , en 1989 , de créer un système hypertexte distribué sur le réseau informatique pour que les collaborateurs puissent partager les informations au sein du CERN .
L' année suivante , l' ingénieur système Robert Cailliau se joint au projet d' hypertexte au CERN , immédiatement convaincu de son intérêt , et se consacre énergiquement à sa promotion .
Tim Berners-Lee et Robert Cailliau sont reconnus comme les deux personnes à l' origine du World Wide Web .
NCSA Mosaic jette les bases de l' interface graphique des navigateurs modernes et cause un accroissement exponentiel de la popularité du Web .
En 1994 , Netscape Communications Corporation est fondée avec une bonne partie de l' équipe de développement de NCSA Mosaic .
Sorti fin 1994 , Netscape Navigator supplante NCSA Mosaic en quelques mois .
En 1995 , Microsoft essaie de concurrencer Internet avec The Microsoft Network ( MSN ) et échoue .
