En Italie , c' est un plat de ménage , une simple pâte à pain sur laquelle on étale ce qu' on veut , les restes ...
Elle s' est ensuite diversifiée comme en témoigne Alexandre Dumas , en 1843 , dans un récit de voyages : " La pizza est à l' huile , la pizza est au lard , la pizza est au fromage , la pizza est aux tomates , la pizza est aux petits poissons ; c' est un thermomètre gastronomique du marché : elle hausse ou baisse de prix , selon le cours des ingrédients sus-désignés , selon l' abondance ou la disette de l' année. "
La recette de la pizza napolitaine a notamment été inscrite au Journal Officiel Italien en mai 2004 , .
Des compétitions sont organisées ; à l' origine , on les recensait surtout en Italie et aux États-Unis .
Aujourd'hui , ce type de pizza est l' un des symboles de la malbouffe dans certains pays dont la France .
Dans le but de favoriser sa consommation debout , une pizza en forme de cône est apparue en Italie .
