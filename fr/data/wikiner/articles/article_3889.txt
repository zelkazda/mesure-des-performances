Le palladium a été découvert en 1803 par William Hyde Wollaston .
Le nom , donné par Wollaston lui-même l' année suivant sa découverte , dérive de l' astéroïde ( 2 ) Pallas découvert deux ans auparavant .
La panique des marchés qui a suivi a conduit le prix du palladium à un niveau record de 1100 dollars US en janvier 2001 .
À cette époque la Ford Motor Company , craignant des répercussion qu' aurait causé une éventuelle rupture de stock de palladium sur la production automobile , a stocké des quantités colossales du métal à prix fort ( la plupart du palladium est utilisé pour les convertisseurs catalytiques dans l' industrie automobile ) .
Lorsque les prix ont chuté début 2001 , Ford a perdu près de 1 milliard de dollars .
Sachant que la production minière mondiale était de 222 tonnes en 2006 selon les données de l' USGS .
En 2007 , la Russie était le premier exportateur de palladium avec un part mondiale de production de 44 % , suivie par l' Afrique du Sud , 40 % .
Le Canada ( 6 % ) et les États-Unis ( 5 % ) sont les seuls autres producteurs signifiants de palladium .
Le palladium peut être trouvé comme métal libre allié avec l' or et autre métaux du groupe du platine dans des dépôts d' orpaillage dans l' Oural , l' Australie , l' Éthiopie , l' Amérique du Nord et du Sud .
La production mondiale s' élève à environ 200 t , à plus de 70 % comme sous-produit des usines russes de nickel notamment à Norilsk ( Russie ) .
