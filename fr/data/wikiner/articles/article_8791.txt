L' interprétation de la signification des trois couleurs est variable ; la plus communément acceptée est celle selon laquelle le rouge symbolise le sang versé par les Arméniens dans la défense de leur pays , le bleu représente le ciel , et l' orange fait référence au sol fertile de l' Arménie et à ses cultivateurs .
Avec l' adoption du christianisme , l' Arménie adopte de nombreux étendards représentant diverses dynasties .
Après la division de l' Arménie entre la Perse et l' Empire ottoman , l' idée même d' un drapeau arménien disparaît .
Elle ne refait surface qu' en 1885 lorsqu' une association d' étudiants arméniens à Paris désire assister aux funérailles de Victor Hugo avec un drapeau national .
La bande supérieure est rouge , pour le dimanche " rouge " de Pâques , l' intermédiaire est verte , pour le dimanche " vert " de Pâques , et l' inférieure est blanche , une couleur arbitrairement choisie pour compléter l' ensemble .
Sa seconde création est connue en tant que " drapeau des nationalistes arméniens " ; il s' agit à nouveau d' un drapeau tricolore , mais vertical , avec le rouge pour la bande de gauche , le vert pour la bande centrale , et le bleu pour la bande de droite , le tout pour représenter l' arc-en-ciel que Noé vit lorsqu' il posa le pied sur le mont Ararat , .
La fédération est dissoute à la suite de l' indépendance de la Géorgie ( 26 mai 1918 ) , suivie de celles de l' Azerbaïdjan et de l' Arménie ( 28 mai ) .
En 1918 , à son indépendance , la République démocratique d' Arménie adopte un drapeau presque identique à celui d' aujourd'hui .
Le 29 novembre 1920 , les bolcheviks établissent la République socialiste soviétique d' Arménie .
Ce drapeau n' existe que pendant un mois : Arménie , Géorgie et Azerbaïdjan se regroupent dans la République socialiste fédérative soviétique de Transcaucasie .
Le 30 décembre 1922 , cette république devient l' une des quatre premières de l' URSS .
En 1936 , elle est divisée en RSS de Géorgie , RSS d' Arménie et RSS d' Azerbaïdjan .
La première fête du drapeau arménien a eut lieu le 15 juin 2010 à Erevan sur l' avenue Mesrop Machtots .
Le drapeau arménien est directement repris sur l' emblème des Forces armées arméniennes , adopté le 23 janvier 2001 .
Le drapeau est également mentionné dans les deuxième et troisième strophes de Mer Hayrenik , l' hymne national arménien :
