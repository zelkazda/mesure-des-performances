Elle est aussi le siège du comté de Hughes .
Selon le Bureau du recensement des États-Unis , la superficie de la ville est de 33.7 km² ( 13 mi² ) , dont seulement 0,08 % est de l' eau .
Pierre se trouve sur des falaises sur les rives du Missouri .
Le climat de Pierre est continental , avec des hivers froids et des étés chauds .
Records de température :
