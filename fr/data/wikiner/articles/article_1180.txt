La République démocratique du Congo est le troisième plus vaste pays d' Afrique derrière le Soudan et l' Algérie et le plus peuplé d' Afrique centrale .
Elle est également appelée " Congo-Kinshasa " pour la différencier de son voisin la République du Congo ( elle-même parfois nommée " Congo-Brazzaville " pour les mêmes raisons ) , ou simplement Congo quand la situation ne prête pas à confusion .
Il a également porté le nom de Zaïre de 1971 à 1997 .
Il s' étend de l' océan Atlantique au plateau de l' est et correspond à la majeure partie du bassin du fleuve Congo .
À l' extrême ouest , une quarantaine de kilomètres au nord de l' embouchure du fleuve Congo s' étale une côte sur l' océan Atlantique .
Le pays partage ses frontières avec l' enclave de Cabinda ( Angola ) et la République du Congo à l' ouest , la République centrafricaine et le Soudan au nord , l' Ouganda , le Rwanda , le Burundi et la Tanzanie à l' est , la Zambie et l' Angola au sud .
Le territoire du Congo-Kinshasa était anciennement peuplé uniquement par des chasseurs-collecteurs , peut-être en partie ancêtres des peuples pygmées actuels .
La métallurgie du fer se développe de manière indépendante à ces installations , les plus anciennes traces se découvrent en Afrique centrale au nord-ouest , et au nord-est ( région interlacustre ) .
Au Kivu , dès l' installation des premières communautés villageoises , il est probable que le fer est présent , comme l' attestent les nombreux fours de réduction du fer bien connus au Rwanda et au Burundi .
Plus tard , comme l' indiquent des recherches allemandes sur les affluents du fleuve Congo , ces premières populations vont lentement coloniser le cœur de la forêt équatoriale en suivant les axes des cours d' eau de l' aval vers l' amont ; des travaux espagnols dans l' Ituri suggèrent qu' il faut attendre -- 800 ans pour rencontrer les premiers villages dans certains secteurs de la forêt .
Ces frontières ont été reconnues à l' issue de la conférence de Berlin , en 1885 .
Dans la première catégorie se forma en 1949 une association d' abord culturelle et finalement politique , l' Alliance des Bakongo ( ABAKO ) , dont Joseph Kasa-Vubu devint président en 1954 .
Cette tendance se durcit très vite et réclama bientôt l' indépendance immédiate tout en demeurant fédéraliste lorsqu' il s' agit plus tard de discuter le problème du reste du Congo .
Leur manifeste dans ce sens publié le 1 er juillet 1956 fut vigoureusement combattu par l' ABAKO dès son assemblée générale du 23 août 1956 .
La Belgique , qui croyait à la progressivité de la transition vers l' indépendance organisa les premières élections à l' échelon communal , limitées aux grandes villes en 1957 .
Ces jeunes rivalités politiques confrontées aux structures tribales compliquées du Congo allaient former un mélange détonant qui détruirait au bout de cinq années la première démocratie parlementaire congolaise .
La Belgique menace alors d' intervenir militairement .
Au sein de l' armée , devenue complétement africaine , le général Mobutu Sese Seko prend les rènes et installe un gouvernement de commissaires .
Mobutu est bientôt soutenu par les États-Unis , qui voient d' un mauvais oeil le socialisme de Lumumba .
Les autorités du Katanga créent alors leur propre monnaie et leur propre police .
Lumumba accepte la venue des casques bleus .
Ainsi le gouvernement central perd ses deux provinces minières et Lumumba se retrouve sans argent .
La première guerre du Congo commence en 1962 .
L' assassinat de Patrice Lumumba en 1961 et la reprise du Katanga et du Sud-Kasaï ( qui avaient fait sécession au lendemain de l' indépendance ) marqueront le début de l' ascension du général Mobutu Sese Seko .
Le titre " République démocratique " , un nouveau drapeau et une nouvelle devise sont adoptés en 1964 .
En 1965 le Congo est pacifié .
Toutes les révoltes tribales , ethniques ou des partisans de Lumumba ont été matées .
Mobutu Sese Seko s' empare définitivement du pouvoir en 1965 .
La libération de Stanleyville marque le début des années de guerre qui se poursuivirent jusqu' en 1966 .
Dans les années qui suivent la prise du pouvoir par le général Joseph-Désiré Mobutu , ce dernier entame à partir de 1972 une campagne d ' " authenticité " afin de maintenir sa popularité .
Le pays est renommé République du Zaïre en 1971 d' après un mot local pour rivière , et portera ce nom jusqu' en 1997 .
Le général Mobutu prend le nom de Mobutu Sese Seko et oblige tous ses concitoyens à supprimer les prénoms à connotation occidentale et à rajouter un " postnom " .
Mobutu est renversé par Laurent-Désiré Kabila en 1997 lors de la Première guerre du Congo .
Le pays retrouve son nom de République démocratique du Congo .
La République démocratique Congo s' étend de l' Océan atlantique au plateau de l' est et correspond à la majeure partie du bassin du fleuve Congo , véritable colonne vertébrale du pays .
Le fleuve Congo donne au pays son seul accès à l' océan Atlantique dans la ville portuaire de Banana ( dans un étroit corridor sur la rive gauche du fleuve traversant le territoire de l' Angola , qui dispose de la rive gauche , et dont il crée une petite exclave sur la côte atlantique entre le nord du fleuve et la frontière de la république voisine du Congo ) .
En raison de sa grande superficie , de ses énormes richesses et de son importante population , la République Démocratique du Congo est l' un des " géants " de l' Afrique , avec l' Égypte , le Nigeria et l' Afrique du Sud au centre de l' Afrique .
Le Congo est divisé en 11 provinces .
Autrefois les régions congolaises étaient impliquées dans le commerce triangulaire , la déportation d' esclaves contre des produits finis ou des fruits d' Amérique était assurée par les trafiquants européens .
Dans le détail , la République Démocratique du Congo possède un important potentiel de ressources naturelles et minérales .
Les principales exploitations de cuivre et de cobalt sont dominées par la Gécamines et de ses partenariats .
Vue sa taille , le Congo-Kinshasa est peu peuplé avec 20 habitants au km 2 , la population se concentre sur les plateaux , dans la savane près des fleuves et des lacs ; le nord et le centre du pays , domaine de la jungle sont quasiment vides .
L' exode rural a gonflé les villes et surtout Kinshasa .
Les grandes agglomérations sont Kinshasa ( 8 millions d' habitants ) , Lubumbashi , Kisangani , Mbuji-Mayi , Kananga , Mbandaka , Bukavu , etc .
Le régime de Léopold II a conduit à des massacres de grande ampleur et a encore plus diminué la population .
Ce n' est qu' avec la crise de 1929 et la fin de la Seconde Guerre mondiale que la population commence à augmenter rapidement .
Le régime de Mobutu a encouragé la natalité d' après le slogan " plus de population pour avoir plus de poids sur la scène internationale " .
L' explosion démographique a transformé le Congo des années 1960 et ses 15 millions d' habitants en géant de 63 millions d' habitants .
Durant la guerre interafricaine ( 1997-2005 ) 3,9 millions de Congolais sont décédés majoritairement de maladies infectieuses dues à la malnutrition et l' exode .
C' est le conflit le plus meurtrier depuis la Seconde guerre mondiale .
La République démocratique du Congo abritait environ 177500 réfugiés et demandeurs d' asile à la fin de 2007 .
Ceux-ci provenaient de l' Angola , du Rwanda , du Burundi , de l' Ouganda et du Soudan .
Le paludisme fait des ravages en RDC .
La République démocratique du Congo est l' un des pays les plus pauvres du monde , avec des inégalités très marquées malgré ses multiples et diverses richesses .
Les populations de l' est du pays vivaient en moyenne avec 32 dollars par an et par habitant alors que celles du sud disposaient de 138 dollars et celles de la province de Kinshasa , de 323 dollars , dix fois plus qu' à l' est .
La population du Congo parle plus de 200 langues .
Sur le plan linguistique , il est l' un des pays les plus multilingues de toute l' Afrique .
Tous les Congolais parlent l' une des quelques 200 langues " ethniques " , voire plus de 400 dialectes .
La plupart des Congolais parlent plusieurs langues .
Avec plus de 66 millions d' habitants estimés en 2008 , selon certaines sources , la République démocratique du Congo est désormais le premier pays francophone du monde , devant la France .
36 millions de Congolais utilisent le lingala comme première ou seconde langue , il est également parlé au Congo-Brazzaville , en Centrafrique et en Angola , ce qui peut faciliter les communications vers ces pays .
Le lingala , parlé initialement par les Ngala , s' est répandu car il était la plus parlée par les militaires et les missionnaires pendant la colonisation belge .
Les Kabila ont favorisé cette langue parlée dans leur région d' origine et elle est la seule avec le français à figurer sur les billets de banque .
L' anglais est parlé par 100000 locuteurs réels ( surtout dans la région de Kisangani et dans le sud ) , auxquels il faut ajouter 200000 anglophones partiels .
Vers les zones frontalières du Cabinda et de l' Angola , le pays compte quelques milliers de lusophones mais leur nombre exact est inconnu .
La République démocratique du Congo a pour code pays CD .
( Le code ZR est obsolète ) .
La République démocratique du Congo a pour codes :
