Les premiers langages de programmation qui ont introduit la récursivité sont LISP et Algol 60 et maintenant tous les langages de programmation modernes proposent une implémentation de la récursivité .
de même que les coefficients binomiaux quand ils sont définis par la formule de Pascal .
qui donnerait en Python :
en Caml :
en Prolog :
en Pascal :
en Java de façon naïve :
et en C ou en C++ :
Notons que les algorithmes ci-dessus , écrits dans des langages de programmation comme Java , ne prennent pas en compte le fait que le factoriel croit de manière exponentielle et dépasse rapidement la capacité de stockage des " int " .
