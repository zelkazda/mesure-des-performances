Cet événement historique ne doit pas être confondu avec la pièce de théâtre du même nom écrite par Jean-Claude Carrière qui présente la conférence comme un débat sur l' humanité des indiens .
Dès la découverte des populations précolombiennes en 1492 , Christophe Colomb , écrit d' elles :
Dès le début du 16e siècle , des voix se font entendre pour condamner les exactions commises sur les indiens : Antonio Montesinos dénonce les injustices dont il a été témoin en annonçant " la voix qui crie dans le désert de cette île , c' est moi , et je vous dis que vous êtes tous en état de pêché mortel à cause de votre cruauté envers une race innocente " .
Antonio Montesinos , en 1511 n' hésite pas à refuser les sacrements aux encomienderos indignes et à les menacer d' excommunication .
Mais pendant une absence de Bartolomé de Las Casas , les indiens en profitent pour tuer plusieurs colons .
Las Casas est favorable à l' application de la philosophie de saint Thomas d' Aquin selon laquelle :
L' ensemble de la thèse Sepulveda englobe des arguments de raison et de droit naturel avec des arguments théologiques .
Juan Ginés de Sepúlveda considère les cas de sacrifices humains , d' anthropophagie , d' inceste royal , pratiqués dans les sociétés précolombiennes et suit des arguments aristotéliciens et humanistes en proposant quatre " justes titres " qui justifient la conquête :
Las Casas réplique en démontrant :
