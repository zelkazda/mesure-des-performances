D' une superficie de 56,42 km² , la commune est située dans la partie est des Monts d' Arrée entre 355m et 92m d' altitude , le bourg se trouvant vers 260 mètres .
Le busard cendré a un lieu de nidification en Bretagne à Berrien .
Au nord et à l' ouest , la commune est limitée par les Monts d' Arrée , dépassant parfois les 300 mètres,ce qui , pour la Bretagne , est une hauteur considérable .
Son cantique nous indique qu' il vint prêcher la bonne parole sur les pentes arides des Monts d' Arrée .
La légende raconte que les femmes de Berrien , fatiguées de voir leurs époux plus préoccupés de l' enseignement de l' anachorète que de leur travail , le chassèrent à coup de pierres .
" Puisque c' est ainsi , s' écria-t-il , je vous prédis que désormais le territoire de Berrien ne sera plus que pierres .
Cette époque fut particulièrement troublée en Bretagne : Guerre de la Ligue , exactions de la bande de brigands commandée par Guy Eder de La Fontenelle .
La commune est enfin démembrée une quatrième fois lors de la création de la commune de Botmeur en 1851 , réclamée depuis longtemps par ses habitants .
Cette anecdote semble quelque peu exagérée , mais illustre quand même le brouillard fréquent des monts d' Arrée .
( ... ) Berrien avait aussi de nombreux contacts avec l' extérieur grâce aux marchands de toile et aux chiffonniers ( pilhaouerien ) qui colportèrent les nouvelles ( ... ) .
La même année , le maire porte plainte contre le " recteur " ( curé en Bretagne ) l' accusant de refuser d' accomplir ses devoirs de prêtre à l' égard de la population alors qu " il " reçoit un traitement de l' état , ainsi que son vicaire " .
La même année encore , un paysan de la commune écrit au préfet du Finistère pour se plaindre de l' intransigeance du recteur qui lui refuse la communion pascale parce qu' il reçoit de " mauvais journaux " .
En 1906 , la " querelle des inventaires " provoque des incidents sérieux lorsque le percepteur du Huelgoat , chargé d' inventorier les biens du clergé , vient à Berrien : il est malmené par les fidèles .
La même année , le maire de Berrien conteste en justice le legs effectué par une habitante de la commune en faveur de l' école privée de filles .
Le courrier également , acheminé ensuite en chars-à-bancs jusqu' à Berrien et Scrignac .
Berrien fait partie de la communauté de communes des monts d' Arrée .
