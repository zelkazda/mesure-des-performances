Ce sport reste très populaire au Japon , même si le baseball et le football le détrônent désormais , notamment chez les jeunes .
L' établissement d' une dictature militaire à Kamakura en 1192 est suivie d' une longue période de guerres et d' instabilité .
Les principaux tournois ( honbasho ) sont diffusés par le service public NHK à la radio depuis 1928 et à la télévision depuis 1953 .
Des tournées de promotion à l' étranger sont régulièrement organisées par l' association ou par les écuries ( clubs des lutteurs ) : à Las Vegas en 2005 , en Israël en 2006 , à Hawaï en 2007 et à Los Angeles en 2008 , alors que celle de 2009 à Londres est annulée faute de moyens suite à la crise économique , .
Il y a six tournois principaux par an , baptisés honbasho et durant 15 jours : Ces tournois sont diffusés à travers tout le Japon et sont suivis fiévreusement par une grande partie de la population bien que la discipline soit victime de la désaffection du public depuis quelques années .
