Lola Bobesco commence sa carrière comme enfant prodige , donnant à six ans son premier récital avec son père .
Elle décroche le premier prix au Conservatoire de Paris en 1934 .
Elle se fait connaître sur la scène internationale en obtenant le septième prix au Concours Eugène-Ysaÿe en 1937 .
Elle fonde en 1958 l' Orchestre royal de chambre de Wallonie .
Elle enregistre des sonates de Beethoven , Fauré , Brahms , Franck et Debussy ainsi que des œuvres du répertoire baroque .
