Bourges est une ville française , préfecture du département du Cher .
Avec une agglomération de plus de 100000 habitants , elle est la 3 e plus grande ville de la région Centre après Tours et Orléans .
À quelques dizaines de kilomètres du centre de la France , et à 240 km de Paris .
Dans l' Antiquité , la ville se nommait Avaricum " le port sur l' Yèvre " .
Des tombes riches , comme le grand tumulus de Lazenay , manifestent par ailleurs la puissance de l' aristocratie biturige contemporaine .
Partout ailleurs en Gaule , Vercingétorix avait mis en place une politique de la terre brûlée : aucune ville , aucune ferme ne devait servir à l' approvisionnement des légions romaines .
Cependant , les habitants d' Avaricum le supplièrent d' épargner leur cité , mettant en avant la sûreté de leur ville protégée par des défenses naturelles ( car située sur une butte entourée d' une rivière et de marais ) et par une puissante muraille au sud .
Bourges devient également le siège d' un archevêché , dont relèvent les diocèses d' Albi , de Cahors , de Clermont , de Mende , du Puy-en-Velay , de Rodez , de Saint-Flour et de Tulle .
Pépin 1er la pris d' assaut en 762 , détruisit ses remparts et l' intégra au domaine royal sous la garde de ses comtes .
Durant ce siècle , la ville connaît un nouvel âge d' or , autour du chantier de la cathédrale , et la construction d' une nouvelle enceinte sous l' impulsion du roi Philippe Auguste .
Son fils futur Louis XI naîtra d' ailleurs dans le palais des archevêques en 1423 .
Jacques Cœur , fils d' un marchand drapier , sera l' un des habitants les plus illustres de cette époque .
Homme ruiné , il va trouver refuge auprès du pape Nicolas V .
La trace la plus marquante qu' il a laissée dans la ville est la construction d' un hôtel particulier encore existant aujourd'hui , le palais Jacques-Cœur .
Montgomery prend la ville en mai 1562 , puis ses hommes la saccagent .
La nouvelle du massacre de la Saint-Barthélemy atteint Bourges le 26 août 1572 , et le massacre des protestants y dure jusqu' au 11 septembre .
En 1585 , son gouverneur La Chatre se rallie à la Ligue dès son lancement .
Ce dernier établissement est le successeur de l' école d' artillerie , implantée à Bourges en 1839 à la suite de pressions intensives des élus locaux .
Blasonnement des armes traditionnelles de la ville Bourges :
C' est seulement à partir du XIX e siècle que Bourges retrouve un véritable essor économique : l' installation d' industries métallurgiques , aéronautiques , chimiques et d' établissements militaires attire une importante main-d'oeuvre .
Depuis 1952 une usine pour la fabrication des pneumatiques Michelin a été édifiée au nord de la ville .
Aujourd'hui , le développement du trafic routier replace Bourges , grâce à sa position géographique , dans une situation privilégiée .
Un autre projet important est en cours : la construction d' un nouveau centre commercial ( avec quelques logements , un parking souterrain et de nombreux commerces ) dans le centre ville , dans le quartier Avaricum .
L' A71 relie la ville à 1 heure d' Orléans , 2h30 de Paris et à 4 heures de Lyon .
Il est possible , via l' A85 à Vierzon , d' aller à Tours en 1h30 .
La commune est aussi desservie par la ligne Ter Centre : Châteauroux ↔ Bourges .
Bourges est une des très rares villes en France à être mal desservie au niveau ferroviaire en fonction de sa taille .
En effet , la gare ne se trouvant pas sur une des grandes radiales ferroviaires françaises , les trains devant aller jusqu' à Bourges doivent être le plus souvent en terminus , limitant ainsi l' effet de desserte de la ville , ou sinon , les gens devant aller jusqu' à la gare de Vierzon pour pouvoir prendre un train jusqu' à Paris .
On peut par exemple compter seulement 4 trains direct aller/retour par jour pour Paris-Austerlitz , sa gare tête-de-ligne .
En comparaison , Nevers , étant une ville plus petite , mais sa gare étant sur une des radiales ferroviaires , bénéficie de 12 trains aller/retour pour Paris au quotidien .
Cependant , en contre-partie , la position de la gare de Bourges lui donne une très bonne desserte " Est-Ouest " .
Bourges est classée Ville d' Art et d' Histoire .
La maison de la Culture de Bourges , inaugurée en 1963 , est l' une des premières du type en France .
En 1860 , en raison de sa position centrale , Bourges fut choisie pour être le centre de l' armement sous le Second Empire .
En 1928 , la firme Hanriot implante son école de pilotage à Bourges .
Des aviateurs prestigieux séjourneront à Bourges et à Avord :
À partir de 1928 les industries aéronautiques vont se succéder à Bourges :
