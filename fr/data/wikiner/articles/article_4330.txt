La vitesse de libération d' un corps quittant la surface de la Terre , dite aussi deuxième vitesse cosmique , est de l' ordre de 11,2 kilomètres par seconde ( soit environ 40 000 km/h ) par rapport à un repère inertiel géocentrique .
Par comparaison , celle de Jupiter est de 59,5 km/s .
La sonde Luna 1 fut , en 1959 , le premier objet construit par l' homme à atteindre la vitesse de libération terrestre lors de son trajet en direction de la Lune .
