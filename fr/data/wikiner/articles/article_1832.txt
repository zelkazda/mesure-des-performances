Il y a , en 2007 , 195 pays reconnus par l' Organisation des Nations unies .
D' autres pays ne sont pas à l' ONU , pour des raisons politiques , par exemple Taïwan .
Ainsi de la même manière que l' on peut dire que le Royaume-Uni est un pays , on peut dire que l' Angleterre est un pays .
