Actuellement , l' Union astronomique internationale ( UAI ) divise le ciel en 88 constellations avec des frontières précises , pour que tout point du ciel appartienne à une constellation .
Différentes cultures ont reconnu des constellations différentes , bien que quelques-uns des regroupements les plus évidents aient tendance à réapparaître fréquemment , quoique sous des appellations différentes , comme par exemple Orion , la Grande Ourse et le Scorpion , à cause de leur brillance .
Les constellations boréales sont les plus anciennes et correspondent au pan de ciel visible depuis les régions de la Méditerranée par les astronomes de l' Antiquité .
Aujourd'hui , un total de 88 constellations a été adopté par l' Union astronomique internationale ( UAI ) .
Elle ne comprend cependant que des étoiles visibles d' Alexandrie où Ptolémée faisait ses observations .
C' est également dans le zodiaque que l' on trouve les vingt huit stations lunaires traditionnelles , astérismes qui servaient de calendrier à ciel ouvert pour suivre les mouvements de la Lune .
Les constellations présentes dans le zodiaque sont : le Bélier , le Taureau , les Gémeaux , le Cancer , le Lion , la Vierge , la Balance , le Scorpion , le Sagittaire , le Capricorne , le Verseau et les Poissons , qui achèvent le cycle .
Les anciens ne l' ont toutefois pas relevé pour des conditions purement esthétiques ou astrologiques : seul le sud de la constellation est traversée par le Soleil et les étoiles brillantes du Scorpion en sont proches .
Les 48 constellations inscrites par Ptolémée dans son Almageste seront utilisées pendant plus de 1000 ans en occident sans aucun changement ni ajout .
En effet , les délimitations des constellations n' ont pas été fixées à l' époque Antique , seule l' appartenance des étoiles brillantes l' ont été .
Par la suite , Johann Bayer puis John Flamsteed recensèrent des étoiles moins brillantes dont ils décidèrent de la constellation d' appartenance .
Il contenait , outre celles de Ptolémée , 12 constellations nouvelles visibles depuis l' hémisphère sud .
Johann Bayer , en produisant une carte du ciel pour chaque constellation , commence à rattacher tout point du ciel à une constellation donnée .
En 1624 , l' astronome allemand Jakob Bartsch définit cinq nouvelles constellations entre plusieurs déjà existantes .
Vers la même époque , Tycho Brahe élève au rang de constellation l' astérisme de la Chevelure de Bérénice .
Vers 1690 , Johannes Hevelius , bourgmestre de Gdańsk , propose plusieurs constellations :
Nicolas Louis de Lacaille est abbé , astronome et mathématicien .
Dans les années 1920 , l' Union astronomique internationale décide de mettre de l' ordre dans les constellations et d' en définir rigoureusement les limites .
L' atlas officiel des constellations , défini en 1930 par Eugène Delporte , divise le ciel suivant des lignes d' ascension droite et de déclinaison .
Leur antériorité manifeste sur le reste du ciel chinois est vraisemblablement dû à leur nécessité pour établir un calendrier , la place du Soleil dans ces astérismes étant un moyen de repérer le cycle des saisons .
Du fait que la Terre tourne sur elle-même , on observe les constellations tourner autour d' un centre que pointe l' axe de rotation terrestre , c' est-à-dire α Ursae Minoris dans l' hémisphère nord , σ Octantis dans l' hémisphère sud .
C' est pourquoi sur les cartes célestes , l' étoile polaire y figure au centre ( σ Octantis est malheureusement trop peu lumineuse pour être facilement observable ) .
À l' inverse , la plupart des constellations ne sont visibles qu' en certaines saisons , comme Orion , visible en hiver , la Lyre en été , le Lion au printemps , ou encore Andromède visible en automne .
