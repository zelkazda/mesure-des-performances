Ces innovations se répandirent partout au Danemark pour créer une série de frontières dialectiques mineures ( voir isoglosse ) de Zélande au Svealand .
La première traduction de la Bible en danois fut publiée en 1550 .
Plusieurs auteurs ont publié leurs œuvres en danois : le philosophe existentialiste Søren Kierkegaard , l' écrivain de contes de fées Hans Christian Andersen , et le dramaturge Ludvig Holberg .
Cette dernière subsiste encore dans quelques noms , comme Aalborg .
Le danois est la langue officielle du Danemark , des Îles Féroé et du Groenland et le danois est généralement compris en Suède et en Norvège .
Aujourd'hui , beaucoup de personnes en Europe du sud et en Afrique du nord en lien avec les destinations de vacances parlent un peu de danois , pour être d' anciens immigrants ou pour avoir travaillé quelque temps au Danemark .
De plus le danois est une des langues officielles de l' Union européenne ( UE ) .
