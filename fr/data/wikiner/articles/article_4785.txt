Ce cas unique dans la littérature française a été longtemps considéré comme un écrivain aussi scandaleux que le marquis de Sade ou Restif de la Bretonne .
Il était un militaire sans illusions sur les relations humaines , et un écrivain amateur , cependant son projet phare était de " faire un ouvrage qui sortît de la route ordinaire , qui fît du bruit , et qui retentît encore sur la terre quand j' y aurais passé " ; de ce point de vue il a largement atteint son but , car la renommée de son livre maître les Liaisons dangereuses est telle qu' il peut être considéré comme un des livres parmi les plus connus au monde .
Le roman a été plusieurs fois porté au cinéma , par Roger Vadim ( 1959 ) , Stephen Frears et Milos Forman ( 1989 ) ou Roger Kumble ( 1999 ) .
Deuxième fils d' un secrétaire à l' intendance de Picardie et d' Artois , d' une famille de robe récente , il est poussé par son père à s' engager dans l' armée , bien que les perspectives de promotion soient restreintes , puisqu' il choisit l' artillerie , arme technique convenant à son esprit mathématique mais aussi à sa peur pour la guerre en général -- qui apparaîtra dans Les Liaisons dangereuses .
Nommé capitaine à l' ancienneté en 1771 -- il le restera durant dix-sept ans jusqu' à la veille de la Révolution -- cet artilleur , froid et logicien , à l' esprit subtil , s' ennuie parmi ses soldats grossiers .
Ses premières pièces , en vers légers , sont publiées dans l' Almanach des Muses .
Cette œuvre n' aura qu' une seule désastreuse représentation , le 19 juillet 1777 devant la reine Marie-Antoinette .
Lors de cette même année 1777 , il reçoit la mission d' installer une nouvelle école d' artillerie à Valence qui recevra notamment Napoléon .
De retour à Besançon en 1778 , il est promu capitaine en second de sapeurs .
Durant ses nombreux temps libres en garnison , il rédige plusieurs œuvres , où il apparaît comme un fervent admirateur de Jean-Jacques Rousseau et de son roman la Nouvelle Héloïse , qu' il considère comme " le plus beau des ouvrages produits sous le titre de roman " .
En 1778 , il commence à rédiger Les Liaisons dangereuses .
Promu en cette fin d' année capitaine de bombardier , il demande un congé de six mois qu' il passe à Paris où il écrit ; il sait que désormais son ambition littéraire doit passer avant son ambition militaire en impasse .
Les Liaisons dangereuses sont donc aussi pour lui une sorte de revanche et une thérapie .
La publication de cet ouvrage sulfureux , considéré comme une attaque contre l' aristocratie , est jugée comme une faute par le commandement de Choderlos de Laclos .
Le thème de l' émancipation féminine avait déjà dans Les Liaisons dangereuses un rôle important .
Après une période de recherche personnelle du meilleur moyen de favoriser son ambition , et diverses tentatives pour approcher un grand seigneur , il entre au service du duc d' Orléans dont il partage les idées sur l' évolution de la royauté .
Grâce à ses activités , il est chargé de l' organisation du camp de Châlons en septembre 1792 et il prépare de façon décisive la victoire de la bataille de Valmy .
Choderlos de Laclos est donc l' inventeur de l' obus .
En 1795 , espérant être réintégré dans l' armée , il rédige un mémoire intitulé " De la guerre et de la paix " qu' il adresse au Comité de salut public , mais sans effet .
Finalement , il fait la connaissance du jeune général Napoléon Bonaparte , le nouveau Premier consul , artilleur comme lui , et se rallie aux idées bonapartistes .
Il est enterré sur place -- au retour des Bourbons en 1815 , sa tombe fut violée et détruite .
D' autre part , les actuels descendants de Choderlos de Laclos prononcent aussi leur nom : sho-der-lo .
