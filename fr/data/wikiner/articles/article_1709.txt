Membre du Parlement norvégien ( le Storting ) dès 1935 , il dirigea divers ministères de 1936 jusqu' à l' invasion de la Norvège par les Allemands .
Le 1 er novembre 1950 , l' Assemblée générale décida de prolonger de 3 ans le mandat de Trygve Lie .
C' est le 10 avril 1953 que le diplomate suédois Dag Hammarskjöld prit sa succession .
Rentré en Norvège , Trygve Lie y occupa différents postes ministériels et mena plusieurs missions diplomatiques .
