Député-maire de Lons-le-Saunier , il préside l' association des maires de France .
Diplômé de l' Institut d' études politiques de Paris , licencié en lettres et titulaire d' un diplôme d' études supérieures de droit obtenu à la faculté de droit et lettres de Lyon , il devient avocat en 1968 et s' installe à Lons-le-Saunier .
Il est élu député dans la première circonscription du Jura le 28 mars 1993 , réélu le 1 er juin 1997 , le 9 juin 2002 et le 10 juin 2007 .
Membre du comité directeur de l' Association des maires de France depuis 1989 , il en devient vice-président en 1995 , puis premier vice-président en 2002 jusqu' à accéder à sa présidence en 2004 .
