Le manga Vision d' Escaflowne a la particularité d' avoir le style shōjo pour ses personnages , mais d' être un shōnen impliquant des mecha .
De même , X de Clamp , publié dans un magazine pour filles , est à bien des égards à même de plaire au public masculin de par les combats souvent sanglants qui sont mis en scène .
