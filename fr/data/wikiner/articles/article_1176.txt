Sa naissance serait attribuée à la déesse Neit , qui aurait mis Rê au monde sous la forme d' un œuf .
Rê sortit de l' œuf et fut aveuglé par la lumière .
Mais il peut apparaître sous bien d' autres formes , comme celle de Khépri , le scarabée bousier symbolisant la renaissance , ou encore sous la forme d' un homme couvert d' or .
Le dieu Rê était également fortement associé au jour de l' an .
Sa fille Tefnout la réprime , mais désormais vulnérable il décide de gagner le ciel .
Il la rappelle à lui et elle se transforme alors pour devenir la vache céleste Hathor formant la voûte céleste destinée à porter en son sein la barque solaire et son cortège divin désormais symbolisé par les étoiles .
Rê voyage chaque jour à travers le ciel à bord de sa barque sacrée ( parcours du Soleil ) , et chaque nuit au travers des mondes souterrains ( les enfers ) .
Chaque lever de soleil était une victoire remportée par Rê sur les " forces des ténèbres " .
Les " forces des ténèbres " sont représentées par le serpent Apophis , qui cherche chaque nuit à déstabiliser la barque solaire et à avaler le monde pour le plonger dans les ténèbres .
Rê est épaulé dans son combat par Seth , divinité guerrière particulièrement crainte .
C' est l' un des rares mythes où Seth a un rôle positif , et les pharaons qui le prendront comme dieu protecteur n' auront de cesse de le rappeler .
Pharaon , après sa mort , prend place sur la barque de Rê pour rejoindre le royaume des morts .
Il a pour but de rattacher charnellement le pharaon à la puissance cosmique de l' univers , Rê .
