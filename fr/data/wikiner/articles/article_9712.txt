Au Canada , la " forêt boréale canadienne " désigne la bordure nord de la forêt coniférienne boréale , et pas simplement le biome .
La taïga apparaît dans l' hémisphère nord comme un vaste anneau circumpolaire , presque continu sur 12000 km , simplement interrompu par le détroit de Béring et par l' océan Atlantique .
C' est la zone la plus au Nord dans laquelle les espèces qui ont besoin de quelques arbres peuvent survivre .
