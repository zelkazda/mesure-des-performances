Ce grade est également utilisé dans les services de police ou de pompiers de certains pays comme la France .
En France , le grade de lieutenant est le troisième des officiers subalternes .
Dans la Marine nationale , il correspond au grade d' enseigne de vaisseau de 1 e classe .
C' est sous Louis XII qu' apparaît ce grade , pour donner un adjoint au capitaine , commandant la compagnie .
En Suisse , le grade de lieutenant est le premier grade des officiers .
Il correspond en France au grade de sous-lieutenant et aux Etats-Unis à celui de second lieutenant .
Le grade de premier-lieutenant correspond au grade français de lieutenant et au first lieutenant de l' armée US .
