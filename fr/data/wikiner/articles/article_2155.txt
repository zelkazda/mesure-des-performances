Tallinn est la capitale de l' Estonie et le principal port du pays ( port marchand de Muuga ) .
Elle est située sur la côte du golfe de Finlande , qui fait partie de la mer Baltique .
Son ancien nom , en usage jusqu' en décembre 1918 , est Reval à l' allemande , ou Revel à la russe .
L' origine du nom Tallinn ( a ) est clairement estonienne , bien que le sens originel soit sujet à débat .
Tallinn bénéficie d' un climat faiblement continental et doux pour une ville située à une latitude aussi septentrionale .
Une forteresse ( en bois ) , construite sur la colline de Toompea , protégeait le port .
Les Danois construisirent une forteresse en pierre .
Les occupants étrangers commencèrent à cette époque à appeler la ville Reval , d' après le nom de la province dont elle était le centre .
À cette époque également apparaît le nom de Tallinn , utilisé par les autochtones
Elle fut achetée par l' ordre des chevaliers Teutoniques en 1346 .
Reval ou Revel , selon l' appellation officielle russe , devient peu après la capitale du gouvernement , ou province, d'Estland .
Cela est en partie dû à la grande vague de construction des années 1970 , qui précéda le déroulement dans Tallinn de la régate pour voiliers des Jeux olympiques de Moscou .
En 1997 , le centre historique de Tallinn a été inclus dans la liste du patrimoine mondial de l' Unesco , qui permet la préservation présente et future de la vieille ville .
Cette pharmacie est la plus ancienne d' Europe qui soit encore en activité .
Tallinn compte de nombreuses églises aux influences gothiques dont l' église du Saint-Esprit , l' église Saint-Nicolas et l' église Saint-Olaf .
La cathédrale orthodoxe Alexandre-Nevsky s' inspire du style moscovite .
La population de Tallinn en mai 2007 est de 400200 habitants .
Cela est principalement dû à l' immigration organisée par l' URSS pendant la période soviétique ( 1944-1991 ) .
Ainsi , de nombreux citoyens soviétiques , pour la plupart des Russes , immigrèrent en Estonie .
Cependant , le fait de résider à Tallinn ne leur permet pas , ni à leur descendants , d' accéder à la citoyenneté estonienne , à moins que ceux-ci ne réussissent le test de connaissance de la langue estonienne nécessaire à l' obtention de la citoyenneté estonienne .
Tallinn est desservie par l' aéroport international Lennart Meri qui est le plus grand d' Estonie , situé approximativement à quatre kilomètres du centre ville .
Une liaison aérienne directe existe entre Paris et Tallinn depuis le printemps 2003 , mais aussi entre Bruxelles et Tallinn .
Chaque jour , des ferries partent pour Helsinki ou bien Stockholm .
Tallinn se trouve à deux heures d' Helsinki et à 17 h de Stockholm .
Chaque année , ce sont plus de 800000 personnes qui transitent par le port de Tallinn .
Tallinn compte 4 lignes de tramways .
Elle possède une grande salle de concerts , le " Saku Suurhall " .
On y trouve également 30 musées , retraçant l' histoire de l' Estonie , de sa capitale , de ses campagnes , etc .
