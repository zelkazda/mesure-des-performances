Les films pornographiques font partie des tous premiers films réalisés suite à l' invention du film cinématographique par les frères Lumière .
Plusieurs tentatives ont eu lieu aux États-Unis dans les années 1970 pour proscrire la pornographie .
Le succès de ce film , qui engrangea des recettes record , engendra bien d' autres films et de nouvelles " stars " comme Marilyn Chambers , Gloria Leonard , Georgina Spelvin ( dans The Devil in Miss Jones ) , Jeremy Cohen , ou encore Bambi Woods .
Les principaux protagonistes de cette époque sont John Holmes , Kay Parker , Seka , Ginger Lynn , Annette Haven , Veronica Hart , Desiree Cousteau , Vanessa del Rio ou encore Hyapatia Lee .
Cependant , à partir des années 1980 , le phénomène de starisation se développa aussi parmi les acteurs masculins , sous l' impulsion d' acteurs comme Rocco Siffredi .
Dans les années 1980 aux États-Unis , le SIDA tua plusieurs acteurs et actrices érotiques , notamment John Holmes et Lisa De Leeuw .
Ainsi , aujourd'hui aux États-Unis , un éventuel séropositif peut-il être identifié , contacté et à nouveau expertisé sous trois à six mois .
Ovidie , qui se qualifiait auparavant de " travailleuse du sexe " , admet que " parfois , il y a des choses qui sont très violentes et qui laissent des marques " .
Comme d' autres femmes notamment l' américaine Annie Sprinkle , Ovidie a abandonné le statut d' actrice pour passer à la réalisation .
