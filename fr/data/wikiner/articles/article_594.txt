C' est la septième plus grande île du monde en superficie , soit une surface légèrement inférieure à l' Ouganda et la deuxième plus peuplée après l' île de Java .
Le point culminant de cette île est le Mont Fuji , volcan actif qui culmine à 3 776 mètres .
