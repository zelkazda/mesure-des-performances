Ce roman , même s' il n' est pas le premier de Michel Jeury dans le domaine de la science-fiction marque comme un coup de cymbale inaugural son entrée sur la scène de la SF française des années 70 .
Pour sa thématique du temps incertain , flou , désarticulé , cette même critique et son éditeur ont rapproché cet ouvrage de ceux de Philip K. Dick et de Ubik en particulier .
