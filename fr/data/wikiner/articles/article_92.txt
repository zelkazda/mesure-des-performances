Si Léonard de Vinci , vers 1500 , imagine des machines volantes , ce n' est qu' en 1783 que les premiers hommes vont pouvoir réaliser le vieux rêve d' Icare avec les montgolfières des frères Montgolfier en 1783 , précédant de très peu les ballons à gaz de Jacques Charles .
Ces engins sont tributaires du vent , l' aéronautique ne va vraiment prendre son essor qu' avec les ballons dirigeables , de Henri Giffard en 1852 .
Ce n' est qu' en 1977 que le premier vol utilisant un " moteur humain " sera réalisé à bord du Gossamer Condor , un avion de moins de 32 kg .
