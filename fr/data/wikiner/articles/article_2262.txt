Berlin est la capitale et la plus grande ville d' Allemagne .
Après 1945 et jusqu' à la chute du mur de Berlin en 1989 , la ville est partagée en quatre secteurs d' occupation .
Berlin est une ville mondiale culturelle et artistique de premier plan .
En 2008 , Berlin a accueilli près de 8 millions de visiteurs .
Berlin se situe dans la plaine germano-polonaise , à 33 m d' altitude , au confluent de la Spree et de la Havel .
Berlin est redevenue la capitale de l' Allemagne le 3 octobre 1990 .
Il a cependant fallu un vote tendu et très serré au Bundestag -le 21 juin 1991 -- pour que la décision soit prise de transférer effectivement les institutions de Bonn à Berlin .
Le transfert du gouvernement et du chancelier à Berlin a eu lieu en 1999 .
Hambourg et Brême possèdent une organisation similaire .
Berlin n' est pas une ville grise et triste mais égayée par plusieurs rivières , canaux , parcs et lacs .
Du fait de ce développement décentralisé , Berlin présente de nombreuses choses à voir , dans son centre comme dans sa périphérie .
Pour diverses raisons , la Porte de Brandebourg ( Brandenburger Tor ) est devenue l' emblème de la ville -- et plus encore , puisqu' elle représente aussi la réunification des deux Allemagne .
Deux tours s' élancent dans le paysage berlinois : la Fernsehturm ( tour de la télévision ) , sur l' Alexanderplatz dans le quartier Mitte , et la Funkturm ( tour de la radio ) qui se trouve dans le parc des expositions de Charlottenburg .
La réputation du clubbing berlinois est reconnue et enviée dans le monde entier grâce à des discothèques légendaires , tel le fameux Kitkatclub et , plus récemment , le Berghain , deux institutions mondialement connues pour leur programmation musicale combinée à une certaine liberté sexuelle de leur clientèle .
Berlin a donc une vie culturelle riche et très diverse .
Aujourd'hui , Berlin doit faire face à de graves difficultés financières , mais les manifestations culturelles continuent .
De plus , la chaîne musicale MTV Allemagne a aussi déménagé son siège de Munich pour Berlin fin avril 2004 .
Enfin , Berlin est aussi une référence pour le cinéma avec l' accueil chaque année en février de la Berlinale , festival international de cinéma dont la récompense suprême est l' Ours d' Or .
L' Île aux Musées abrite :
Berlin accueille chaque année en février la Berlinale .
Quelques films se déroulant à Berlin :
À Berlin , il y a deux jardins zoologiques : Le zoo de Berlin ( Zoologischer Garten Berlin ) , fondé déjà en 1844 , et le Jardin Zoologique de Berlin-Friedrichsfelde , fondé en 1954 .
Toutefois , en 2007 Berlin a dégagé pour la première fois de son histoire un excédent budgétaire .
Elle y a toujours son siège , contrairement à beaucoup d' autres sociétés berlinoises qui ont quitté la ville après la construction du Mur , par peur d' être coupées de leurs fournisseurs et de leurs marchés .
Dans les années 1990 , Berlin s' est largement désindustrialisée .
Si le secteur des services occupe une place croissante à Berlin , la fonction publique reste le premier employeur de la ville .
Berlin est ainsi la ville la plus touristique d' Allemagne .
Berlin dispose également de 6 lignes publiques de ferry ( bateau ) .
La ville est traversée d' est en ouest par le Berliner Stadtbahn .
Une partie des travaux ( notamment concernant la desserte de la gare par les transports urbains ) est retardée , voire suspendue sine die , faute de financements , Berlin étant au bord de la faillite .
Il existe une deuxième rocade qui fait le tour de la ville à une plus grande distance qui est le plus grand périphérique d' Europe .
Pour le transport ferroviaire , la Deutsche Bahn fait rouler des trains et des express régionaux ainsi que des ICE .
Pour le transport aérien , Berlin possède deux aéroports : Tegel et Schönefeld .
Il sera suivi par Tegel , dont la fermeture est prévue en 2011 ou 2012 .
Berlin a accueilli les Jeux olympiques d' été de 1936 et a été une des villes de la Coupe du monde de football de 2006 dont elle a accueilli la finale à l' Olympiastadion .
Le WTA Tour , ensemble des tournois de tennis féminin , comprend l' Open d' Allemagne organisé annuellement dans la ville depuis 1979 .
Berlin est aussi la ville des Eisbären Berlin du Championnat d' Allemagne de hockey sur glace , une équipe qui a été fondée à l' époque de l' Allemagne de l' Est .
Devenue 1 er champion de la toute nouvelle 3. Bundesliga lors de la saison 2008/2009 , l' équipe de football du 1.FC Union Berlin jouera en 2. Bundesliga la saison prochaine .
À Berlin sont nés :
À Berlin sont décédés :
En 2005 , Berlin est en deuxième position pour ce qui est du taux de délinquance en Allemagne ( 15002 délits pour 100000 habitants ) .
