Il fréquente encore la chapelle du lycée Lakanal en 1891-1892 .
D' après son condisciple Albert Mathiez , c' est peu à la fin de cette période qu' il devient " un anticlérical convaincu et pratiquant " .
Il intègre l' École normale supérieure de Paris le 31 juillet 1894 , sixième sur vingt-quatre admis .
Lorsque éclate l' affaire Dreyfus , il se range auprès des dreyfusards .
Cependant , dès 1900 , après la quasi-faillite de sa librairie , il se détache de ses associés Lucien Herr et Léon Blum et fonde dans la foulée Les Cahiers de la quinzaine , au 8 rue de la Sorbonne , revue destinée à publier ses propres œuvres et à faire découvrir de nouveaux écrivains .
Romain Rolland , Julien Benda , Georges Sorel , Daniel Halévy et André Suarès y contribuent .
Sur le plan politique , Péguy soutient longtemps Jean Jaurès , avant qu' il n' en vienne à considérer ce dernier comme un traître à la nation et au socialisme .
Le 16 janvier 1910 paraît Le Mystère de la charité de Jeanne d' Arc , qui s' inscrit clairement dans la perspective d' une méditation catholique et manifeste publiquement sa conversion .
En effet , Charles Péguy n' aurait jamais communié adulte et n' aurait reçu les sacrements qu' un mois avant sa mort , le 15 août 1914 , à Loupmont , alors qu' il était sous l' uniforme .
L' œuvre de Péguy a toujours célébré les valeurs traditionnelles de l' homme : son humble travail , sa terre , sa famille .
C' est à ce titre que Péguy peut apparaître comme un théologien , chantre des valeurs de base .
La réforme scolaire de 1902 , portant sur les humanités modernes et l' enseignement secondaire unique , est sans doute la première occasion à laquelle Péguy exprime aussi violemment son rejet du monde moderne : " Comme le chrétien se prépare à la mort , le moderne se prépare à la retraite " .
Dans ses Cahiers de la quinzaine , il écrit : " Aujourd'hui , dans le désarroi des consciences , nous sommes malheureusement en mesure de dire que le monde moderne s' est trouvé , et qu' il s' est trouvé mauvais. "
Il meurt au combat la veille de la bataille de la Marne , tué d' une balle au front , le 5 septembre 1914 à Villeroy ( ou au Plessis-l'Évêque ) , près de Neufmontiers-lès-Meaux , alors qu' il exhortait sa compagnie à ne pas céder un pouce de terre française à l' ennemi .
Sa famille quitte alors la maison de Bourg-la-Reine et laisse la place au romancier et essayiste Léon Bloy .
