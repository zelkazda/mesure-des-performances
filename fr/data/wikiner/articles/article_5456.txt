Montamy est situé en Bocage virois .
Couvrant 367 hectares , son territoire s' étend de part et d' autre de la route départementale 577 de Caen à Mortain par Vire ( ancienne route nationale 177 ) .
Montamy comptait 252 habitants lors du premier recensement républicain en 1793 , population jamais atteinte depuis .
