Yves Bur est un homme politique français , docteur en chirurgie dentaire , né le 10 mars 1951 à Strasbourg ( Bas-Rhin ) .
Il est député de la 4 e circonscription du Bas-Rhin ainsi que maire de Lingolsheim depuis 1995 .
Ancien membre de l' UDF , il fait actuellement partie de l' UMP .
Il exerce en tant que chirurgien-dentiste libéral à Lingolsheim de 1976 à 2002 .
En 1989 , Yves Bur est réélu aux côtés du maire de Lingolsheim et se voit confier la politique de la ville et de la rénovation urbaine .
Après douze années de " terrain " , il est élu maire de la ville en juin 1995 et sera reconduit dans son mandat en 2001 , année au cours de laquelle il deviendra vice-président de la Communauté Urbaine de Strasbourg .
Parallèlement , Yves Bur se penche sur une autre question centrale du dossier du système de santé : le médicament .
Parallèlement à ces enjeux nationaux , Yves Bur s' implique fortement dans les relations franco-allemandes .
L' année 2004 constitue une étape importante dans l' engagement politique et public d' Yves Bur .
En mars 2007 , suite à la démission de Jean-Louis Debré de la présidence de l' Assemblée nationale , Yves Bur est présenti comme l' un des possibles futurs présidents .
Cependant , c' est Patrick Ollier qui sera élu .
À ce titre , il participe activement au mouvement des refondateurs et rejoint l' UMP dès sa constitution , suite logique de son engagement en faveur de l' UEM en 2001 .
Spécialiste de la santé publique , Yves Bur a présenté le mercredi 2 novembre 2005 la proposition de loi visant à interdire de fumer dans les lieux publics et les lieux de travail pour protéger les salariés contre les dangers du tabagisme passif .
