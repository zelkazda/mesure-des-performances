Elle partage l' île d' Irlande avec l' Irlande du Nord ( les six comtés ) restée partie constitutive du Royaume-Uni .
La république d' Irlande revendique son identité celtique si bien que , sur le plan culturel , elle représente habituellement l' île d' Irlande considérée comme l' une des six " nations celtiques " .
Il existe deux langues officielles en Irlande .
La morphologie de l' île comprend une plaine centrale entourée de montagnes et de collines , particulièrement dans le Donegal et le Wicklow .
Non loin de là , les îles d' Aran font face à la baie de Galway .
A Dublin , les mois les plus frais sont janvier et février des moyennes de 3° pour les minimales et de 8° pour les maximales ; en été , les températures moyennes oscillent entre 12° pour les minimales et 19° pour les maximales .
Les précipitations sont assez importantes ( 733mm de précipitations annuels en moyenne à Dublin ) .
Elles tombent rarement sous forme de neige ( 4 jours de neige par an en moyenne à Dublin ) .
Comme dit un proverbe , " en Irlande , il fait beau ... plusieurs fois par jour !
Ils se sont installés dans la région de l' actuelle Ulster .
C' est de cette époque que date l' exploitation de mines de cuivre dans les régions de Cork et Kerry et d' or dans le Wicklow .
C' est vers -500 que les Celtes font leur apparition en Irlande , leur civilisation sur l' île va durer près de mille ans .
Leur arrivée s' est faite par deux routes différentes , par l' actuelle Grande-Bretagne et par l' Espagne .
En Irlande , les filid ( bardes ) vont devenir les membres les plus influents de cette classe sacerdotale , dont une des prérogatives est de conseiller le roi .
Ces clans vont progressivement fusionner pour constituer quatre royaumes ( ou provinces ) : l' Ulster , le Leinster , le Munster et le Connacht ( Connaught ) .
La christianisation de l' Irlande marque la fin de la religion celtique , du moins en ce qui concerne sa mythologie , car la structure de la société s' est maintenue , avec une classe sacerdotale prédominante .
Padraig serait né en 390 en un lieu incertain de l' île de Bretagne .
Patrick est mort vers 461 .
L' Irlande vit un âge d' or intellectuel par le dynamisme de ses institutions religieuses , mais sur le plan politique l' île est divisée entre cent et cent cinquante tuatha ( les clans ) , à la tête de chacun desquels se trouve un rí ( roi ) .
Ces chefs sont eux-mêmes assujettis au roi d' une des cinq provinces ( Ulster , Connacht , Munster , Leinster et Meath ) .
C' est dans ce contexte d' instabilité que les Vikings arrivent dans l' île .
Dès 812 les raids se concentrent sur la côte ouest , puis sur les rivages de la mer d' Irlande .
Pendant une quarantaine d' années , les Vikings vont multiplier les raids et le razzias , privilégiant les monastères , non pour des raisons religieuses , mais parce que plus riches en trésors .
En 836 , ils empruntent la rivière Shannon et pillent le Connaught .
L' année suivante , deux flottes d' une soixantaine de drakkars chacune , reconnaissent la Liffey et La Boyne , les territoires sont systématiquement ravagés , les habitants massacrés .
L' hiver 840 -- 841 marque une étape , puisque pour la première fois les Vikings passent la saison dans l' île et s' installent dans des places fortifiées qui deviennent aussi des lieux de commerce : Dublin , Annagassan , puis par la suite , Wexford , Cork , Limerick , pour ne citer que quelques établissements .
En 1541 , Henri VIII prend le titre de roi d' Irlande .
Une grande révolte éclate en 1641 , brisée par Oliver Cromwell en 1649 ( massacres de Drogheda et Wexford ) .
En 1704 , Guillaume III promulgue des " lois pénales " anti-catholiques .
Un nouveau soulèvement a lieu en 1798 , nourri aussi bien par l' émancipation des États-Unis que par l' exemple de la Révolution française .
En 1905 , le Sinn Féin indépendantiste est fondé .
Néanmoins le pouvoir suspensif de la Chambre des Lords puis le déclenchement de la Première Guerre mondiale l' empêcheront d' être mis en œuvre .
Mais le Sinn Féin en retire une popularité accrue : il remporte triomphalement les élections de décembre 1918 , constitue un parlement irlandais ( le Dáil Éireann ) et proclame l' indépendance .
C' est pourquoi six des neuf comtés de l' Ulster restèrent britanniques dont 2 avec une faible majorité catholique .
Ce traité fut ratifié de peu par le Dáil Éireann en décembre 1921 , mais fut rejeté par une large majorité de la population .
Cela entraîna la guerre civile d' Irlande qui dura jusqu' en 1923 , opposant les adeptes d' une poursuite de la lutte pour obtenir l' indépendance complète de l' île et les partisans du compromis de 1921 .
Cependant , en 1932 , Fianna Fáil , le parti des opposants au traité , dirigé par Éamon de Valera , remporta les élections ( il resta au pouvoir jusqu' en 1948 ) .
Un traité conclu en 1938 avec le Royaume-Uni , lui laissait ses bases navales en Irlande et entérinait cette indépendance .
En février 1948 , c' est le parti Fine Gael qui remporte les élections .
Le président d' Irlande est élu pour sept ans au suffrage universel direct .
La chambre haute s' appelle Seanad Éireann .
Il y a 26 comtés traditionnels dans la république plus les comtés de Nord-Tipperary et de Sud-Tipperary et les comtés issus de la partition du Comté de Dublin ( Dublin Sud , Fingal et Dun Laoghaire-Rathdown ) .
Il est dû à la relative jeunesse de la population , à un taux de natalité élevé pour l' Europe et surtout à une forte immigration .
En 2006 , 14 % des résidents était nés hors de la république d' Irlande .
Dublin : 505739 , Cork : 119143 , Galway : 72111 , Limerick : 52560 , Waterford : 45748
Le taux de croissance économique en Irlande avait atteint 10,5 % en 2000 ; en 2005 le taux de croissance était de 5,5 % pour 6 % en 2006 .
Au deuxième trimestre de 2006 , 2017000 personnes travaillaient en République d' Irlande , soit 87800 personnes de plus que l' année précédente .
Bien que la croissance économique eut été de 4 % par an entre 1970 et 1985 ( contre 2.7 % en Europe et 3.2 % aux USA ) , cela s' avérait encore insuffisant pour que irlandais atteignent un niveau de vie comparables aux autres pays européens .
Il est cependant à noter que , hormis à Dublin , la société irlandaise était alors jusque dans les années 1980 relativement égalitaire .
Ces réformes d' inspiration libérale ont cependant été menées de manière très pragmatique par les gouvernements successifs quelle que soit leur couleur politique Fianna Fail ( centre droit national ) ou Fine Gael ( centre gauche libéral ) .
Les lotissements à l' américaine ont poussé comme des champignons dans les années 1990 et 2000 à la périphérie de Dublin , Galway et Cork .
Les prix de l' immobilier tant à la location qu' à l' acquistion ont atteint des sommets à Dublin .
Les grosses berlines allemandes et japonaises ainsi que les 4x4 sont désormais légion à Dublin , malgré leur prix prohibitif en Irlande .
Les activités bancaires et financières qui avaient connu un essor sans égal à Dublin sur les bords de la Liffey dans les années 2000 ont été particulièrement touchées par la crise financière d' octobre 2008 , mettant en grandes difficultés les principales banques du pays .
Un réseau de tramway , le Luas , est en cours de construction à Dublin ; un réseau de métro est aussi programmé .
" Luas " est un mot irlandais signifiant " rapide " et non pas un acronyme .
La conduite se fait à gauche comme en Grande-Bretagne .
Principaux ports de commerce : Arklow , Cork , Drogheda , Dublin , Foynes , Galway , Limerick , New Ross , Rosslare Europort et Waterford .
Autres compagnie aériennes basées en Irlande : Ryanair et Aer Arann .
