Né en 1268 à Fontainebleau , il est le fils du roi Philippe III de France ( Philippe le Hardi ) ( 1245 -- 1285 ) et de sa première épouse Isabelle d' Aragon ( 1247 -- 1271 ) .
Son père confie une partie de son éducation à Guillaume d' Ercuis , l' aumônier de son père .
À la différence de son père totalement inculte , Philippe le Bel reçoit par le soin de son précepteur l' aumônier , une bonne éducation .
Philippe le Bel fut un roi qui souleva au cours de son règne beaucoup de polémiques , le pape Boniface VIII le traitant par exemple de " faux-monnayeur " .
Sous le règne de Philippe IV , les traditions féodales sont abandonnées pour mettre en place une administration moderne .
Il est vrai que la politique monétaire de Philippe le Bel est tout sauf stable .
Par ailleurs , le règne de Philippe le Bel se traduit par une période de changements majeurs .
Philippe IV le Bel tenta de pallier ses difficultés financières en essayant d' établir des impôts réguliers , en taxant lourdement les Juifs et les Lombards , parfois en confisquant leurs biens et en pratiquant les dévaluations monétaires .
Il conserva les richesses monétaires de l' ordre des Templiers après l' avoir dissout .
Philippe IV s' entoure de légistes , des conseillers compétents qui jouent un rôle décisif dans sa politique .
Les légistes sont apparus sous Philippe Auguste et sont formés au droit romain pour faire évoluer une monarchie féodale , où les pouvoirs du roi sont limités par ses vassaux , vers une monarchie absolue .
Il termine cette centralisation commencée par son grand-père , Louis IX , mais ce système sera remis en cause par les Valois directs .
L' administration du royaume , limitée à la cour du roi chez ses prédécesseurs , va se diviser en trois sections sous le règne de Philippe le Bel :
Ces transformations rendirent Philippe Le Bel très impopulaire dans tous les niveaux de la société .
Philippe IV a aussi créé l' embryon des états généraux , en ordonnant la tenue d' assemblées formées de représentants des trois ordres : le clergé , la noblesse et la bourgeoisie ( à cette époque on ne parle pas de tiers état ) .
Boniface VIII , qui a alors d' autres préoccupations , se trouve dans l' embarras et , en dépit de son caractère hautain , cède bientôt .
Nogaret est bientôt rejoint par un ennemi personnel de Boniface VIII , Sciarra Colonna , membre de la noblesse romaine , qui lui indique que le pape s' est réfugié à Anagni en Italie .
En voyant Guillaume de Nogaret et Sciarra Colonna approcher , il incline légèrement la tête et déclare : " Voilà ma tête , voilà mon cou , au moins je mourrai en pape ! "
Guillaume de Nogaret recule , impressionné , tandis que Sciarra Colonna , dans sa haine de Boniface VIII , se serait avancé insolemment et lui aurait , dit-on , donné une gifle avec son gantelet de fer .
Peu de temps après le 9 septembre , la population de la ville d' Anagni se révolte et dégage le pape des mains des français mais ce dernier tombe malade et meurt un mois plus tard à Rome le 11 octobre 1303 .
Celui-ci trouve d' ailleurs en la personne du nouveau pape Clément V , successeur de Boniface , une personnalité beaucoup plus malléable qu' il tient sous son pouvoir .
Le vendredi 13 octobre 1307 , les Templiers sont mis en prison puis torturés pour leur faire admettre l' hérésie dans leur ordre .
Le maître de l' ordre , Jacques de Molay , périt sur le bûcher à Paris en 1314 après avoir été déclaré relaps .
C' est lors de son exécution , alors que Jacques de Molay brûlait , qu' il aurait proféré sa célèbre malédiction , exploitée par l' écrivain français Maurice Druon dans son roman historique en sept tomes , les Rois maudits :
En réalité , selon Geoffroi de Paris , témoin oculaire de l' événement et chroniqueur de l' époque , les termes de la malédiction auraient été :
Marguerite de Bourgogne , capétienne , fille du duc Robert II de Bourgogne ( 1248 -- 1306 ) et d' Agnès de France ( 1260 -- 1325 ) , Jeanne de Bourgogne et Blanche de Bourgogne , toutes deux filles du comte Othon IV de Bourgogne et de la comtesse Mahaut d' Artois , épousent respectivement les rois Louis X de France , Philippe V de France et Charles IV de France , les trois fils de Philippe le Bel .
Pendant le règne de Philippe le Bel le domaine royal s' est agrandi grâce à la politique d' assujettissement des grands féodaux et aussi :
Elles auraient trompé leurs maris avec les frères Philippe et Gauthier d' Aunay , tous deux chevaliers de l' hôtel royal .
Les deux amants sont jugés et condamnés pour crime de lèse-majesté ; ils sont exécutés sur-le-champ en place publique à Pontoise : dépecés vivants , leur sexe tranché et jeté aux chiens , ils sont finalement décapités , leurs corps traînés puis pendus par les aisselles au gibet .
Marguerite de Bourgogne est condamnée à être tondue et conduite dans un chariot couvert de draps noirs à Château-Gaillard .
Blanche de Bourgogne est aussi tondue mais bénéficie d' un " traitement de faveur " : elle est emprisonnée sous terre pendant sept ans , puis obtient l' autorisation de prendre l' habit de religieuse .
Quant à la troisième , la comtesse Jeanne de Bourgogne et d' Artois , femme du futur Philippe V de France , elle est enfermée à Dourdan pour avoir gardé ce secret .
En novembre 1314 , Philippe le Bel rend visite à son grand oncle le comte Robert de Clermont [ réf. nécessaire ] et c' est lors d' une partie de chasse au sanglier en forêt de Pont-Sainte-Maxence ( forêt d' Halatte ) qu' il est atteint d' un sérieux malaise cérébral , après avoir fait une chute de cheval .
Il est inhumé dans la basilique de Saint-Denis .
Il agrandit également le territoire du royaume , notamment avec l' annexion de Lille après la signature du traité d' Athis-sur-Orge .
