L' histoire de Lillers débute vers l' an 700 , avec un prince d' Irlande qui se serait rendu en pèlerinage à Rome en compagnie de son frère .
C' est là qu' ils auraient construit une chapelle et leurs habitations autour : c' est ainsi que serait naît Lillers .
En 1327 , l' Artois fut érigé en comté par saint Louis .
En 1303 , la ville de Lillers fut brûlée , pillée par les Flamands .
En 1542 , les Français brûlèrent à leur tour Lillers .
Après une période de paix , la guerre de Trente Ans , sous Louis XIII et Richelieu , dévasta encore la région .
En 1641 , elle appartint de nouveau aux Espagnols .
En 1645 , Lillers fut enlevée par les Français , commandés par le duc de Guise .
En 1659 , fut conclu le traité des Pyrénées qui donna l' Artois à la France , sauf Saint-Omer et Aire .
En 1710 , notre région fut le théâtre de la guerre de Succession d' Espagne .
En 1710 , Lillers fut prise par les Espagnols .
