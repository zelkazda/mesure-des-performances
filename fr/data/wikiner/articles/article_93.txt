En France , en circulation aérienne générale , il existe deux interfaces de contrôle :
Ce système est en cours d' homologation par l' OACI .
L' Australie le teste déjà conjointement avec un système radar pour pouvoir l' utiliser au-dessus des zones semi-désertiques où la mise en place de radars est soit trop onéreuse soit impossible .
La France va bientôt équiper la zone de la Réunion et pourrait étendre ensuite ce système à toutes les zones impossibles à équiper en radar ( archipels polynésiens , Guyane , etc .
Le financement des services de la circulation aérienne en France est assuré par deux types de redevance : la redevance en route et les redevances de services terminaux .
Ainsi par exemple la DGAC française a t-elle perçu en 2008 1068 millions d' euros de redevance de route pour 1249 millions budgétés en dépenses pour le contrôle aérien .
