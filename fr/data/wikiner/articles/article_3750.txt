Il vit à Paris depuis 1975 .
Déçu , il s' éloignera de son pays et vivra en Suisse et en France .
Sa vie se partage entre la France , la Suisse et la Belgique ; entre l' enseignement , la psychanalyse et l' écriture ; entre succès et difficultés financières .
Ici , Œdipe partage avec Orphée la même capacité , celle de ranimer " les trésors perdus de la mémoire " grâce au chant , à la peinture et à l' écriture .
Après avoir surmonté ses peurs , il est " encore , est toujours sur la route " , dira Antigone à la fin .
Antigone , qui l' a accompagné jusqu' au bout , symbolise cette route de la réalisation de soi .
De même , quand elle revient à Thèbes pour tenter d' apaiser la rivalité entre ses deux frères , c' est aussi pour dire " oui " à la vie , au futur , à la beauté et pour refuser , dans sa robe déchirée , toutes les manifestations de pouvoir , toutes les guerres .
