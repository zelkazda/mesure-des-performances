HP-UX est un système d' exploitation propriétaire de type Unix , développé par Hewlett-Packard , utilisé sur des serveurs et des stations de travail .
C' est un concurrent direct de Sun Solaris , IBM AIX et GNU/Linux .
HP-UX a été initialement développé pour les processeurs PA-RISC ( 32 et 64 bits ) , conçus par HP .
HP-UX dispose d' une interface pseudo-graphique d' administration nommée " sam " ( équivalent du SMIT d' AIX ) .
Comme la version précédente , cette version supporte les processeurs PA-RISC et Itanium .
