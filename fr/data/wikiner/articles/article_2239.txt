Le terme clone est utilisé pour la première fois en 1903 par le botaniste H.J. Webber en désignant des plantes reproduites par multiplication asexuée .
Ce mot sera ensuite réutilisé par J.B.S. Haldane .
L' embryologiste chinois Tong Dizhou , fut le premier à cloner un animal ( une carpe ) en 1963 , 33 ans avant la brebis Dolly .
Début 2008 , l' EFSA ( Agence européenne de la sécurité alimentaire ) prépare un nouvel avis sur ces questions .
Arnold Schwarzenegger , gouverneur de la Californie a milité en faveur du clonage humain .
Les États-Unis , avec plus de cinquante autres pays , ont signé un appel à une interdiction totale du clonage humain .
Un autre texte interdisant seulement le clonage reproductif a été rédigé par la Belgique et soutenu par plus de vingt pays , dont la Russie , le Japon , le Royaume-Uni , la Corée du Sud et le Danemark .
En mai 2005 , des chercheurs de Corée du Sud et du Royaume-Uni ont annoncé les premiers clonages d' embryons humains à des fins de recherches thérapeutiques .
