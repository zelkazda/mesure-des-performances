Aux débuts de l' époque historique , le faucon sacré est figuré sur la palette du roi Narmer et dès lors il sera constamment associé à la monarchie pharaonique .
Horus est un dieu à multiples facettes , au point qu' on s' est demandé si le nom ne désigne pas en fait des divinités distinctes :
Pour venger la mort de son père Osiris , Horus affronte son oncle Seth , le combat et reçoit le trône d' Égypte en héritage .
Cependant , sa légitimité sera sans cesse contestée par Seth .
Horus est borgne : lors du combat qui l' oppose à Seth , Horus a perdu son œil gauche , qui est reconstitué par Thot .
La signification du serekh est évidente : le roi dans son palais est l' Horus terrestre , à la fois l' incarnation du dieu et son successeur légitime sur le trône d' Égypte .
Les " quatre fils d' Horus " sont des divinités inférieures représentées sur les vases canopes :
À Horus , fils et héritier d' Osiris , la couronne d' Égypte revient de droit .
Mais Seth , jaloux , s' en empare par la force .
Horus , appuyé de sa mère Isis , fait convoquer le tribunal des dieux à toute fin de régler ce contentieux .
Rê préside , tandis que Thot tient le rôle du greffier .
Le tribunal est même partagé entre les tenants de la royauté légitime ( revenant à Horus ) , et Rê qui voit en Seth son perpétuel défenseur contre Apophis ( le dieu serpent qui est depuis toujours l' ennemi de Rê ) .
C' est donc à Neith , déesse de Saïs , réputée pour son infinie sagesse , que Thot s' adresse .
Sa réponse est sans ambiguïté : la couronne revient à Horus .
Cependant pour ne pas pénaliser Seth , Neith propose de lui offrir les déesses Anat et Astarté comme épouses .
Si le tribunal se réjouit de cette solution , Rê , lui , reste sceptique .
Horus ne serait-il pas un peu jeune pour assumer la direction du royaume ?
Isis , excédée par tant de tergiversations , propose de déplacer les débats à Héliopolis devant Atoum et Khépri .
Seth , furieux , s' y oppose et ordonne que les débats se fassent en l' absence d' Isis .
Elle se réintroduit dans l' enceinte du tribunal sous les traits d' une belle jeune femme qui ne manque pas d' attirer rapidement l' attention de Seth .
" La rusée " Isis se dévoile alors .
Le coup de théâtre laisse Seth sans voix .
Quant à Rê , il a pu juger de l' imprudence de Seth , qui se confia sans prendre garde à une inconnue .
Aussi la couronne revient-elle à Horus des mains de Rê lui-même .
Mais Seth , éternel jaloux , ne semble pas décidé à en rester là .
Il propose à Horus des jeux sportifs .
Mais Isis , qui suit de près les mésaventures de son fils , perturbe la partie et s' attire au final le mécontentement des deux protagonistes : les trois dieux se déchirent en violentes disputes .
Rê , désespérant d' assister enfin à une réconciliation , les invite à faire la paix autour d' un banquet .
Seth va même jusqu' à essayer de féminiser Horus pour le rendre indigne du pouvoir aux yeux des autres dieux en se masturbant pour éjaculer et verser son sperme sur les cuisses de son neveu .
Osiris , resté silencieux , intervient alors et met directement en cause le tribunal qu' il juge trop laxiste .
En tant que dieu de la végétation , il menace de couper les vivres à l' Égypte .
Les dieux , bousculés par tant d' autorité , ne tardent pas à rendre un verdict favorable à Horus .
Mais Seth n' est pas oublié .
Placé aux côtés de Rê , il devient " celui qui hurle dans le ciel " pour que soit fait place devant le dieu créateur .
