Fondée par Jimmy Wales le 9 mars 2000 et financée par Bomis , Nupedia était un projet d' encyclopédie libre , pour laquelle Larry Sanger fut embauché comme éditeur en chef .
Elle se distinguait de Wikipédia par une politique stricte pour l' intégration des nouveaux articles , le comité scientifique visant à en faire une encyclopédie de qualité comparable aux encyclopédies professionnelles sur le marché .
En janvier 2001 , sous l' impulsion de l' argumentaire de la Free Software Foundation de Richard Stallman , Nupedia transféra son contenu sous la Licence de documentation libre GNU .
Cependant , Stallman enclencha aussi , à la même époque , son projet d' encyclopédie Internet , le projet GNUPedia , entrant en compétition avec Nupédia .
En dépit de son contenu sous une licence libre , la participation à Nupédia demeurait soumise à un processus assez lourd de soumission des textes , contrastant avec les conceptions sous-jacentes au mouvement des logiciels libres .
La création de Wikipédia n' apparaissait alors pas comme un fork de Nupédia et n' en reprenait pas les contenus ( chose cependant faite depuis la fermeture de Nupedia ) , mais était plutôt , à l' origine , mise en place comme une sorte de " sas d' entrée " pour les articles devant , éventuellement , être intégrés à Nupédia .
Wikipédia apparut donc parallèlement à Nupédia , en tant que complément , et cette nouvelle donne rallia les partisans des deux approches , en rejoignant ainsi les défenseurs de l' approche plus souple qu' offrait GNUPedia .
En se développant et en attirant ses contributeurs , Wikipédia en est cependant rapidement venue à tracer son propre cheminement et atteignit un mode de fonctionnement largement indépendant de Nupedia , bien que Larry Sanger participât à la formulation de la majorité des politiques d' origine , parallèlement à son travail pour Nupedia .
Tout comme elle contribua à l' avortement du projet GNUPedia , Wikipédia a aussi contribué à graduellement remettre en cause l' existence de Nupedia .
En février 2002 , Bomis mit fin à la rétribution de Larry Sanger pour son travail d' éditeur en chef , ce qui amena celui-ci à quitter les projets Nupedia et Wikipédia peu après .
Après qu' il l' eut quittée , Nupedia s' effaça graduellement derrière Wikipédia ( seulement deux articles franchirent le processus d' évaluation après 2001 ) .
Comme elle devenait de plus en plus stagnante , l' idée que Nupedia puisse constituer une version stabilisée ( validée ) des articles développés par Wikipédia fut occasionnellement évoquée , mais jamais implantée .
Le 16 septembre 2006 , Larry Sanger a lancé un nouveau projet de corpus encyclopédique validé , fondé sur Wikipédia , sous le nom de Citizendium .
Le processus d' édition de Nupedia comprenait sept étapes :
Les conditions pour contribuer à Nupedia étaient relativement élevées .
