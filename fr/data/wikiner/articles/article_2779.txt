Internet , du fait même de sa structure , est un outil de communication et de liberté d' expression très puissant , et échappe ainsi , pour une large part , au contrôle étatique .
Les gouvernements de nombreux pays , inquiets de la mise en péril de leurs pouvoirs par l' outil de communication offert par Internet , essayent d' y appliquer une politique de contrôle voire de censure .
Les associations de défense d' un Internet libre s' inquiètent des mesures prises , depuis quelques années , par les gouvernements de différents pays -- les événements du 11 septembre ayant parfois servi de prétexte à des mesures plus radicales .
Des associations s' opposent à cette atteinte potentielle à la vie privée et exigent que les citoyens puissent utiliser comme ils l' entendent les logiciels de chiffrement , notamment le logiciel PGP ainsi que sa version libre GnuPG .
Cette saisie a fait scandale , le site étant légal en Suède .
La volonté de fermer The Pirate Bay était guidée par une démarche symbolique .
Les dissidents tels que Guillermo Fariñas luttent pour la liberté d' expression et d' information sur internet .
En France , aucune loi ne contraint la liberté d' expression plus que dans les autres médias .
Un seul site est censuré en France : le site de l' AAARGH .
L' AAARGH a réagi en migrant une partie de son contenu vers d' autres adresses .
Renaud Donnedieu de Vabres , le ministre de la culture au moment du DADVSI , a déclaré " qu' il n' y aurait pas de véritable information sans véritable signature " , ce qu' une partie de la blogosphère française a pris comme l' annonce d' une restriction de leur liberté de publier au profit de la presse reconnue .
Un certain nombre de mesures concerne l' Internet .
En 2009 , reporter sans frontière dans son rapport sur internet émet des inquiétudes pour la France dans un article intitulé : Des logiciels-espions qui menacent les libertés ? .
