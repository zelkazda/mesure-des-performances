Elle est une réplique à l' échelle de la France de la Silicon Valley .
La technopole est située sur les communes de Valbonne , Mougins , Biot , Vallauris et Antibes ( les cinq communes fondatrices du site ) , au cœur des parcs naturels de la Valmasque et de la Brague .
Le concept comme le nom -- une marque déposée -- provient de Pierre Laffitte , à l' époque directeur de Mines ParisTech .
Son épouse , prénommée Sophie , a inspiré la première partie du nom de la zone et donné son nom à une place de la zone d' activité , la place Sophie Laffitte .
Cette opération , visant à concentrer en un même lieu , un campus , les chercheurs et des entreprises des sciences et technologies de l' information et de la communication ( STIC ) , a été lancée par le Conseil général des Alpes-Maritimes .
En plus du bâti déjà existant de l' INRIA , de l' IUT et du restaurant universitaire , le cabinet d' architecture Jean-Michel Wilmotte a été chargé d' imaginer de nouveaux bâtiments pour Polytech'Nice-Sophia et pour l' Institut Eurécom et un pôle d' accueil central abritant un amphithéâtre , une cafétéria et un centre de vie .
Sophia Antipolis est la plus importante technopole de France .
Sophia Antipolis , au départ un simple technopôle , le premier en France , aujourd'hui un modèle pour les plus récents , qui fêtera en 2009 ses 40 ans , est devenue une technopole , c' est-à-dire un grand centre urbain , disposant d' un fort potentiel d' enseignement et de recherche , favorable au développement d' industries de pointe .
Grâce à une urbanisation linéaire et continue , tous les habitants de l' aire urbaine de Nice ( 973 231 habitants ) peuvent accéder au réseau TER Provence-Alpes-Côte d' Azur .
Non cadencée , cette ligne est saturée et les trains doivent souvent refuser des voyageurs malgré les TER à double étage .
Actuellement une troisième voie de chemin de fer est en construction entre Antibes et Nice , afin de proposer un service de type RER cadencé au quart d' heure avec deux liaisons semi-directes intercalées à chaque heure .
Il est gratuit mais l' usager doit être muni d' un titre de transport spécial ( voir site d ' Envibus ) .
Sur le territoire communal de Nice , il n' existe qu' un seul aéroport , l' aéroport Nice Côte d' Azur .
La Chambre de commerce et d' industrie Nice Côte d' Azur gère également l' assez proche aéroport de Cannes -- Mandelieu .
Avec 10,4 millions de passagers en 2007 , la plate-forme aéroportuaire de Nice se hisse à la troisième place des aéroports français en nombre de voyageurs , après ceux de Paris .
