Il mène une politique centralisatrice et expansionniste et transfère la capitale de Nankin à Pékin afin de surveiller plus facilement l' activité des Mongols .
Il est l' initiateur de la construction de la Cité interdite de Pékin .
En 1398 , Hongwu meurt et son petit-fils , Zhu Yunwen , lui succède en prenant le nom de règne de Jianwen .
Zhu Di , alors le prince le plus puissant , devance son arrestation et décide de contre-attaquer .
Après quatre ans de combats , son armée occupe Nankin et dans la panique le palais de l' empereur prend feu et Jianwen et sa femme disparaissent , probablement morts brulés .
Il fait construire une flotte considérable qui entreprend de grands voyages d' exploration sous la direction de l' amiral eunuque Zheng He ( 鄭和 ) .
Il commandite la monumentale Encyclopédie de Yongle .
Yongle est enterré dans la tombe de Changling ( 長陵 ) , le mausolée central et le plus important des tombeaux des Ming .
