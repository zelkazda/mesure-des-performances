C' est dans ce contexte qu' Hugues Capet peut instaurer la dynastie capétienne .
Il bénéficie tout d' abord de l' œuvre politique de son père qui parvient à contenir les ambitions de Herbert II de Vermandois , puis à en neutraliser la lignée .
La dynastie capétienne qu' il fonde ainsi dure plus de huit siècles et donne naissance à des lignées de souverains en Espagne , en Italie , en Hongrie , au Portugal et au Brésil .
Les quatre fleuves ( Escaut , Meuse , Saône et Rhône ) constituent ses limites au nord et à l' est , le séparant de l' empire ottonien .
Enfin , le tracé des côtes est très différent de celui que nous connaissons , car les golfes ne sont pas colmatés , en particulier dans le bassin d' Arcachon et le golfe de Saint-Omer , et les embouchures des fleuves évoluent encore .
Plus adapté que la monnaie d' or héritée de l' Antiquité qui ne convient que pour des transactions très onéreuses , le denier d' argent permet l' introduction de millions de producteurs et de consommateurs dans le circuit commercial .
Pierre Bonnassie a montré que , après les grandes famines de 1005-1006 et de 1032-1033 , la population devient de moins en moins exposée aux dérèglements alimentaires et , par voie de conséquence , aux épidémies : la mortalité diminue .
Il ne faudrait pas surestimer cette époque de renouveau économique et social car le changement n' en est qu' à sa genèse et la paysannerie est encore la victime des mauvaises récoltes , comme , sous le règne de Robert le Pieux , où on assiste , selon Raoul Glaber , à des famines foudroyantes où le cannibalisme est de règle dans certaines régions ( 1005-1006 et 1032-1033 ) .
Alors que Charles le Chauve comptait 26 ateliers de frappe monétaire , Hugues Capet et Robert le Pieux n' ont plus que celui de Laon .
Le règne d' Hugues Capet marque l' apogée de la féodalisation de la monnaie .
La marge de manœuvre de Robert le Pieux est faible .
Le choix des abbés s' oriente de plus en plus vers des hommes d' une grande intégrité et certains tels Guillaume d' Aquitaine vont jusqu' à donner l' autonomie et l' immunité à des monastères qui élisent leur abbé .
C' est le cas des abbayes de Gorze , Brogne ou Cluny .
Sous la férule d' abbés dynamiques tels qu' Odon , Maïeul -- un ami personnel d' Hugues Capet -- ou encore Odilon , l' abbaye entraîne d' autres monastères qui lui sont rattachés , et constitue bientôt un ordre très puissant .
Cette notion que Georges Duby centre autour de l' an mil est discutée par Dominique Barthélemy pour lequel cette évolution se déroule sur plusieurs siècles .
Les châteaux en bois ou mottes castrales sont apparus aux alentours de l' an mil entre la Loire et le Rhin .
La codification et la moralisation de la conduite des chevaliers sur des critères religieux entraînent l' élaboration , par l' évêque Adalbéron de Laon , d' une société divisée en trois ordres sociaux : ceux qui travaillent ( laboratores ) , ceux qui prient ( oratores ) et ceux qui combattent ( bellatores ) .
Il gagne encore en puissance quand son grand rival Herbert de Vermandois meurt en 943 , car sa puissante principauté est alors divisée entre ses quatre fils .
Hugues le Grand domine alors de nombreux territoires entre Orléans -- Senlis et Auxerre -- Sens , alors que le souverain carolingien est plutôt replié au nord-est de Paris ( Compiègne , Laon , Soissons ) ( carte 1 ) .
Nous sommes mal renseignés sur le règne d' Hugues Capet .
Hugues est né dans un lieu inconnu vers 939-941 .
Il est le fils de Hedwige de Saxe et d' Hugues le Grand .
Il coule dans les veines d' Hugues Capet un peu de sang carolingien apporté par sa grand-mère paternelle ( Béatrice de Vermandois ) , mais aussi du sang germain par ascension direct .
Cette origine proviendrait de Rhénanie et non pas de Saxe selon Karl Ferdinand Werner .
Enfin , son père s' était allié avec le nouveau roi de Germanie Otton I er , dont il avait épousé la sœur Hedwige de Saxe pour contrecarrer tout désir de Louis IV sur la Lotharingie .
Au total , à la mort de son père , Hugues Capet hérite théoriquement d' un titre prestigieux et d' une puissante principauté .
En 956 , à la mort de son père , Hugues , l' aîné , n' est âgé que d' une quinzaine d' années et a deux frères .
Mais , en contrepartie , le duc doit accepter la nouvelle indépendance acquise par les comtes de Neustrie pendant la vacance du pouvoir .
Son frère Otton n' obtient que le duché de Bourgogne .
Lorsqu' il reçoit sa charge ducale en 960 , Hugues Capet est moins puissant que son père ( carte 1 ) .
En effet , il est jeune , politiquement inexpérimenté et , surtout , il est mis sous tutelle par son oncle Brunon de Cologne , proche du pouvoir ottonien .
Face à cet affaiblissement , une forte poussée d' indépendance se produit chez les vassaux entre Seine et Loire .
Le comte Thibaud de Blois , pourtant un ancien fidèle d' Hugues le Grand qui lui a confié la cité de Laon , s' assure une quasi-indépendance en se proclamant comte de Blois , en faisant fortifier ses principales villes et en s' emparant de Chartres et de Châteaudun .
Enfin , il semblerait pour Hugues que sa place de numéro deux du royaume aurait tendance à lui échapper .
-- Richer de Reims , apr .
Mais il doit rapidement battre en retraite et se réfugier à Étampes chez Hugues .
Otton II s' engage à son tour dans l' offensive et lance ses armées aux portes de Paris .
Mais , aux portes de Paris , Hugues Capet barre la route à l' empereur germanique qui , voyant l' hiver approcher ( on était le 30 novembre ) est contraint de s' enfuir .
Cette victoire permet à Hugues Capet de retrouver ainsi sa place de premier aristocrate du royaume franc .
L' archevêque est assisté par un des esprits les plus avancés de son temps , l' écolâtre et futur pape Gerbert d' Aurillac .
Hugues Capet devient pour eux le candidat idéal , d' autant qu' il soutient activement la réforme monastique dans ses abbayes quand les autres prétendants continuent à distribuer des charges ecclésiales et abbatiales à leur clientèle .
Il complète la chronologie de Flodoard de Reims qui s' arrête en 966 .
La cérémonie se déroule à Compiègne en présence du roi , d' Arnoul , d' Adalbéron de Reims sous la bénédiction d' Hugues .
Sur place , il rencontre l' empereur et le pape , en compagnie de ses fidèles Bouchard de Vendôme et Arnoul d' Orléans .
Hugues se garde bien cette fois-ci d' être de l' expédition .
La contradiction de certains faits donnés par Richer ne nous permet pas de comprendre toute l' action politique d' Hugues à la veille de son couronnement .
On ne sait pas quel est le rôle d' Hugues à ce moment-là , les sources restent vagues .
Mais , au cours d' une partie de chasse , le roi trouve la mort dans une chute de cheval le 21 ou le 22 mai 987 en forêt de Senlis .
En mai 987 , les chroniqueurs , notamment Richer de Reims et Gerbert d' Aurillac , écrivent que , à Senlis , " s' éteignit la race des Charles " .
Depuis une dizaine d' années , Hugues Capet concurrence ouvertement le roi , il semble avoir soumis les grands vassaux , mais , surtout , son adversaire Charles de Lorraine est accusé de tous les maux : il a voulu usurper la couronne ( 978 ) , il est l' allié d' Otton II puis il a accusé d' adultère la reine Emma d' Italie , femme de son frère .
Hugues fait immédiatement acquitter Adalbéron et ce dernier peut alors convoquer une nouvelle assemblée à Senlis ( fief d' Hugues ) et il retourne à Reims écarter toute proposition de la part de Charles de Lorraine .
C' est bien Hugues qui va devenir le nouveau souverain .
C' est la chronologie fournie par Richer de Reims qui pose un problème .
Le moine écrit qu' Hugues est couronné et sacré le 1 er juin .
Il semble plutôt qu' Hugues ait été acclamé roi par l' assemblée de Senlis ( peut-être le 3 juin ) puis couronné et sacré roi le 3 juillet à Noyon .
Hugues Capet a été acclamé par l' assemblée de Senlis ( quelques jours après la mort de Louis V ) , puis il a été couronné et sacré , soit à Reims , soit à Noyon , entre mi-juin et mi-juillet de l' an 987 .
Le choix de Noyon reste obscur : pourquoi avoir choisi une cité autre que Reims alors que le nouveau souverain venait d' être élu sur l' appui d' Adalbéron de Reims ?
On ne sait rien du déroulement du sacre et du couronnement d' Hugues ; en revanche il est à peu près certain qu' il portait un manteau de pourpre tissé d' or ( et peut-être brodé de sujets pieux ) , des bas rouges , des chaussures violettes , une couronne à arche ornée de quatre fleurons et un sceptre .
Il essaye de convaincre Adalbéron de sacrer son fils Robert .
Hugues , venant de recevoir une lettre de Borell II , comte de Catalogne , lui demandant de le soutenir contre Al-Mansur qui vient de razzier Barcelone , fait valoir qu' il a besoin d' avoir un successeur au cas où l' expédition contre les sarrasins tournerait mal .
Rozala d' Italie est de vingt ans son aînée .
D' autre part , sa solide instruction assurée par Gerbert d' Aurillac à Reims , lui permet de traiter des questions religieuses dont il est rapidement le garant ( il dirige le concile de Verzy en 991 et celui de Chelles en 994 ) .
Il est presque certain que , contrairement à son fils , Hugues soit illettré et ne parle pas le latin mais le roman .
Durant son règne , Hugues doit faire face à de nombreux opposants .
En premier lieu , un de ses grands rivaux : Charles de Lorraine .
Ce dernier réapparaît en 988 lorsqu' il s' empare de la ville de Laon , un des derniers bastions carolingiens .
Alors , Hugues décide de ruser .
Quant au pape , il est sollicité par les deux adversaires , tandis que la cour d' Otton III reste neutre , malgré les demandes d' Hugues .
La situation se débloque par la trahison d' Adalbéron de Laon , évêque de Laon , qui s' empare de Charles et d' Arnoul pendant leur sommeil et les livre au roi ( 991 ) .
Bien accueilli à Laon , il jure sur le pain et le vin ( le jour du Dimanche des Rameaux 29 mars ou le jour du Jeudi saint 2 avril 991 ) de conserver sa foi à Charles , avant d' ouvrir les portes de la ville à l' ennemi durant la nuit !
Le dernier carolingien , est emprisonné à Orléans et meurt à une date inconnue .
Adémar de Chabannes nous donne une vision presque " manichéenne " du règne d' Hugues Capet .
Il semblerait que le rejet soit plutôt d' ordre politique ( la capture de Charles de Lorraine ) plutôt que dynastique .
Cette remarque est encore plus explicite dans la cité de Limoges .
Mais cela ne dure pas , quelques mois plus tard , les chartes ne sont plus datées des règnes : il semblerait que le changement soit dû à la prise de connaissance de l' histoire de la capture de Charles de Lorraine et à la trahison de Adalbéron de Laon .
En revanche , les débats sont soutenus par l' évêque Arnoul d' Orléans , un proche du roi .
Arnoul est déposé .
Il abandonne l' archevêché et se rend en Italie .
En parallèle , Abbon de Fleury , qui avait vigoureusement défendu Arnoul , écrit que , à partir du règne d' Hugues Capet , la théorie de la royauté forgée par Hincmar de Reims est reprise : le roi règne avec les conseils des ecclésiastiques .
Abbon rappelle qu' il faut être fidèle au roi et que chacun des grands seigneurs ne se trouve finalement être qu' un dépositaire du service dû au roi .
-- Hugues Capet selon Richer , 990 .
Sous Hugues Capet et encore chez Robert le Pieux , le souverain est largement conseillé et entouré par les évêques dans la tradition carolingienne .
Certains ont montré ouvertement leur hostilité ( Charles de Lorraine , Eudes de Blois ) et d' autres ( surtout les ecclésiastiques ) ont préféré patienter .
On a vu qu' il y a encore , sous le règne d' Hugues , des habitudes carolingiennes .
Enfin , Hugues doit faire face , durant tout son règne , à l' opposition d' Eudes de Blois dont les possessions prennent en tenaille le domaine royal .
Le conflit sans fin est interrompu par la mort d' Eudes en mars 996 , puis par celle d' Hugues Capet vers la fin octobre de la même année .
Eudes de Blois meurt en mars 996 , laisse une veuve dont est épris Robert le Pieux .
Hugues Capet refuse cette union qui apporterait la Bourgogne à son fils , car Berthe de Bourgogne est sa cousine au troisième degré , et le mariage serait consanguin .
Durant l' été 996 , déjà malade , Hugues se serait rendu avec son fidèle Bouchard au monastère de Souvigny où repose son ami saint Mayeul ( mort en 994 ) .
-- Abbon de Fleury , v. 1000 .
Les historiens se sont longtemps demandé pourquoi Hugues n' avait récupéré , suite à son couronnement , qu' un minuscule territoire qui allait constituer le domaine royal .
Dans chacune de ces villes , Hugues Capet dispose d' un palais , d' une troupe de chevaliers et de revenus fonciers et économiques .
Chacune de ses possessions est disjointe des autres puisque de gênants vassaux ( Montmorency , Montlhéry ... ) sont venus s' y intercaler .
Il ne reste quasiment rien du domaine carolingien , si ce n' est autour de Laon .
Il serait cependant illusoire de borner le rayonnement d' Hugues Capet à son seul domaine royal .
Son influence s' étend sur une région beaucoup plus vaste d' Orléans à Amiens ( carte 3 ) .
Comme on l' a dit , nous sommes assez mal renseignés sur le règne d' Hugues Capet .
C' est infime comparé aux plusieurs centaines de son contemporain Otton III .
Hugues Capet apparaît comme un souverain qui reste très " carolingien " dans certains de ses comportements .
Dans un premier temps , il associe son fils unique , Robert , à la couronne .
Mais , face à l' argument d' Hugues , qui affirme ne pas pouvoir laisser le royaume sans chef et sans succession assurée dans un univers hostile , l' archevêque doit céder .
Bref , Richer présente le roi Hugues comme un " roi guerrier " qui accomplit des exploits avec son armée .
Avec Hugues Capet , la situation semble changer .
D' ailleurs , Abbon de Fleury et Richer de Reims sont conscients du changement avec l' ancienne dynastie .
Avec Hugues Capet s' ouvre une nouvelle pratique dans la rédaction des actes .
Dorénavant , le roi doit faire souscrire certains de ses diplômes ( un seul connu pour Hugues Capet ) non par le seul chancelier , mais par les personnes qui l' entourent ( les grands seigneurs ) .
Cette remarque est aujourd'hui discutée puisqu' on considère que ce changement administratif traduit moins un affaiblissement royal qu' un changement de méthode progressif à partir d' Hugues Capet .
Hugues Capet agit en étroite collaboration avec le centre culturel de Saint-Benoît-sur-Loire .
En effet , pour le continuateur de Flodoard , qui poursuit les annales depuis la mort de ce dernier , cette reconstruction est considérée comme un " sacrilège " .
À Paris , à l' époque d' Hugues Capet , la cité est tout entière occupée par le quartier épiscopal à l' est et le palais royal à l' ouest .
Hugues Capet , lui-même abbé , comprend vite tout l' intérêt qu' il peut tirer de la réforme clunisienne .
Il entretient des liens d' amitié avec Maïeul de Cluny , fait montre de dévotion aux cérémonies religieuses et de soutien à la réforme monastique .
Il faut toute l' habileté politique d' Hugues Capet pour le convaincre .
Ce dernier délègue ensuite à Robert le Pieux de réelles responsabilités religieuses et militaires qui l' imposent de fait comme son successeur .
La trahison de l' archevêque Arnoul a porté un rude coup au crédit du roi .
Abbon rappelle qu' il faut être fidèle au roi et que chacun des grands seigneurs ne se trouve finalement être qu' un dépositaire du service dû au roi .
-- Hugues Capet selon Richer , 990 .
Et les deux rois eux-mêmes , sous la plume de Gerbert d' Aurillac , insistent sur cette nécessité de consilium : " Ne voulant en rien abuser de la puissance royale nous décidons toutes les affaires de la res publica en recourant aux conseils et sentences de nos fidèles .
