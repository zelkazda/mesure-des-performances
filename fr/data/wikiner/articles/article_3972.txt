Le Cardinal du Kremlin est un roman de Tom Clancy , paru en 1988 .
Il fait suite au roman À la poursuite d' Octobre Rouge du même auteur .
Mais au passage , ils mettent involontairement la puce à l' oreille du KGB , qui démasque enfin le Cardinal et son réseau d' espions .
