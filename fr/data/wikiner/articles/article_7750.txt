Zodd est un personnage du manga et dessin animé Berserk .
Zodd apparaît dans le volume 5 du manga .
Il est d' abord présenté comme un simple soldat , qui résiste à tous les assauts , puis comme " Zodd l' immortel " , un être mythique , qui errait de champ de bataille en champ de bataille , toujours victorieux .
Guts pénètre alors dans le château qu' il tient pour le tuer : il s' agit d' un monstre aux yeux flamboyants , de taille surhumaine , qui va bientôt se muer en véritable démon devant la résistance de Guts ( un genre de croisement chimérique de chien , bélier et chauve-souris ) .
Auparavant , il fait une prédiction à Guts : " Si cet homme est ton ami , lorsque son ambition se trouvera entravée , la mort viendra te rendre visite , et tu ne pourras pas lui échapper !
Zodd est en fait un apôtre ( tout comme Wyald , ce qui explique qu' ils se connaissent ) .
Ce sont des monstres humanoïdes qui peuplent l' univers de Berserk .
Alors qu' il avait plutôt un rôle neutre auparavant , il se positionne maintenant clairement comme un adversaire de Guts .
