PHP est un langage impératif disposant depuis la version 5 de fonctionnalités de modèle objet complètes .
En raison de la richesse de sa bibliothèque , on désigne parfois PHP comme une plate-forme plus qu' un simple langage .
C' était à l' origine une bibliothèque logicielle en Perl dont il se servait pour conserver une trace des visiteurs qui venaient consulter son CV .
Ce travail aboutit un an plus tard à la version 3 de PHP , devenu alors PHP : Hypertext Preprocessor .
Ce fut ce nouveau moteur , appelé Zend Engine qui servit de base à la version 4 de PHP
Plus d' un quart des vulnérabilités répertoriées sur cette base concerne des applications PHP , plus d' un tiers ces dernières années , et la plupart peuvent être exploitées à distance .
Elle utilise Zend Engine 2 et introduit un véritable modèle objet , une gestion des erreurs fondée sur le modèle des exceptions , ainsi que des fonctionnalités de gestion pour les entreprises .
La prochaine version : PHP 6 est en développement .
PHP peut également générer d' autres formats en rapport avec le Web , comme le WML , le SVG , le format PDF , ou encore des images bitmap telles que JPEG , GIF ou PNG .
Il a été conçu pour permettre la création d' applications dynamiques , le plus souvent dédiées au Web .
La version 6 introduira en interne la bibliothèque ICU donnant au langage la faculté de traiter Unicode de manière native .
PHP appartient à la grande famille des descendants du C , dont la syntaxe est très proche .
En particulier , sa syntaxe et sa construction ressemblent à celles des langages Java et Perl , à la différence que du code PHP peut facilement être mélangé avec du code HTML au sein d' un fichier PHP .
Si la page est identifiée comme un script PHP ( généralement grâce à l' extension .php ) , le serveur appelle l' interprète PHP qui va traiter et générer le code final de la page ( constitué généralement d' HTML ou de XHTML , mais aussi souvent de CSS et de JS ) .
Une étape supplémentaire est souvent ajoutée : celle du dialogue entre PHP et la base de données .
Classiquement , PHP ouvre une connexion au serveur de SGBD voulu , lui transmet des requêtes et en récupère le résultat , avant de fermer la connexion .
Pour réaliser un script PHP exécutable en ligne de commande , il suffit comme en Perl ou en Bash d' insérer dans le code en première ligne le shebang : # ! /usr/bin/php ( /usr/bin/ est le répertoire standard des fichiers binaires exécutables sur la plupart des distributions ) .
PHP possède un grand nombre de fonctions permettant des opérations sur le système de fichiers , la gestion des bases de données , des fonctions de tri et hachage , le traitement de chaînes de caractères , la génération et la modification d' images , des algorithmes de compression ...
Le moteur de Wikipédia est écrit en PHP avec une base MySQL .
Le code PHP doit être inséré entre des balises < ? php et ? > .
Il existe des notations raccourcies : < ? et ? > , voire la notation ASP < % et % > , mais celles-ci sont déconseillées , car elles peuvent être désactivées dans la configuration du serveur : la portabilité du code est ainsi réduite .
De plus , PHP 6 interdira ces notations , ce qui compromettra la compatibilité des scripts utilisant ces balises avec les futures versions de PHP .
Le code PHP est composé d' appel à des fonctions , dans le but d' attribuer des valeurs à des variables , le tout encadré dans des conditions , des boucles .
On peut tout à fait générer du code HTML avec le script PHP , comme ceci par exemple :
Attention toutefois , la présence de beaucoup de code HTML au sein de code PHP dénote souvent une mauvaise conception du programme .
Dans la partie affichage d' une application Web PHP , il faut penser PHP comme un langage de template , comme le montre l' exemple de code suivant ( utilisant une syntaxe alternative pour la structure if/else ) :
Il s' est inspiré de la ressemblance des lettres PHP avec un éléphant , d' où le nom .
Toutes ses œuvres sont distribuées sous licence GNU GPL .
Dans le cas de PHP comme langage serveur , les combinaisons les plus courantes sont celles d' une plateforme LAMP et WAMP .
Une plate-forme WAMP s' installe généralement par le biais d' un seul logiciel qui intègre Apache , MySQL et PHP , comme par exemple EasyPHP , VertrigoServ , WampServer ou UwAmp .
Il existe le même type de logiciels pour les plates-formes MAMP ( Mac OS Apache MySQL PHP ) , à l' exemple du logiciel MAMP .
PHP est à la base un langage interprété , ce qui est au détriment de la vitesse d' exécution du code .
Le Zend Engine compile en interne le code PHP en bytecode exécuté par une machine virtuelle .
Il existe également des projets pour compiler du code PHP :
