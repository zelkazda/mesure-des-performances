Une opinion assez générale la restreint au genre satirique , selon la définition de Boileau : l' épigramme , plus libre en son cours plus borné , n' est souvent qu' un bon mot de deux rimes orné .
La Fontaine , avec sa naïveté pleine de malice , Racine , avec son irritable sensibilité , Voltaire , avec son inexorable bon sens , Piron , Rousseau , Lebrun , etc. , chacun avec ses qualités et ses défauts , se sont illustrés dans ce genre , et en sont devenus les maîtres .
La célèbre épigramme suivante est due à Voltaire :
