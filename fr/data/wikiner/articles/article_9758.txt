Hestia n' est pas mentionnée par Homère .
Hésiode fait d' elle la première-née de Cronos .
En compensation , elle obtient de Zeus le privilège d' être honorée dans chaque demeure humaine et dans tous les temples .
La légende est probablement inventée par l' auteur pour mettre en évidence ses principales caractéristiques : Hestia est une déesse vierge et immuable .
Quand Platon met en scène le cortège des Olympiens , dans le Phèdre , il précise qu' Hestia n' en fait pas partie , car elle demeure en permanence sur l' Olympe .
Ovide mentionne par ailleurs une tentative de Priape d' attenter à son honneur .
Le nom " hestia " désigne le foyer , tout particulièrement celui de la maison ; Hestia en est la personnification .
Bien qu' elle possède une existence propre dès Hésiode , elle reste peu personnalisée .
Parallèlement à ces deux formes de culte , on trouve des sanctuaires privés consacrés à Hestia , par exemple au Pirée et à Olympie , peut-être à Chalcis .
Pindare la nomme ainsi la " patronne des prytanes " lesquels l ' " honorent entre autres divinités , de leurs libations fréquentes et souvent aussi de la graisse des victimes ; ils font résonner la lyre et le chant . "
Quand ce ne sont pas des prytanes , ce sont les " magistrats en chef des cités " , précise Denys d' Halicarnasse .
Camiros , à Rhodes , possède des " damiourgoi d' Hestia " , sortes de surintendants de la cité .
Hestia veille également sur le foyer domestique , centre symbolique de la maison .
Diodore de Sicile déclare même que " Hestia inventa la construction des maisons ; en reconnaissance de ce bienfait , on vénère dans toutes les maisons l' image de cette déesse , et on célèbre des sacrifices en son honneur . "
Dans le culte , Hestia est liée à Apollon : elle veille sur le foyer de Delphes , l' un de ses principaux sanctuaires et à Délos , sa statue est assise sur l' omphalos .
Hestia est rarement représentée dans l' art grec .
L' étymologie du nom dépend de l' existence ou non d' un digamma ( ϝ ) initial , qui n' est attesté que dans la glose d' Hésychios d' Alexandrie et dans un anthroponyme arcadien .
