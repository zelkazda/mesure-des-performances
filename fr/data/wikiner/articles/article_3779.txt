Voyage au centre de la Terre est un roman de science-fiction , écrit en 1864 par Jules Verne .
Ayant découvert un manuscrit runique ancien , un savant , son neveu et leur guide entreprennent un voyage vers le centre de la Terre en y entrant par un volcan islandais éteint .
Comme à l' habitude de Jules Verne , le roman est un habile mélange de données scientifiques , d' extrapolations osées et d' aventure .
Les trois hommes voyagent jusqu' au pied du volcan Sneffels , et en font l' ascension .
