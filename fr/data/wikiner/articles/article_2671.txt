Euskadi Ta Askatasuna est une organisation armée basque indépendantiste d' inspiration marxiste ( révolutionnaire ) .
Depuis 1968 , selon les chiffres officiels et les communiqués d' ETA , ETA a tué plus de 800 personnes , fait des centaines de mutilés , et commis des dizaines de kidnappings .
L' ETA a perdu de nombreux membres dans le conflit .
Plus de 700 membres de l' organisation sont incarcérés dans des prisons en Espagne , en France , et dans d' autres pays .
La plupart des revendications d' ETA portent sur l' indépendance du Pays basque ou Euskal Herria et ce , dans un courant marxiste léniniste .
Créée par des jeunes nationalistes , ETA est rejointe à partir des années 1960 , par une mouvance révolutionnaire .
Elle jouit à ses débuts d' une grande popularité , non seulement au Pays basque , mais aussi dans le reste de l' Espagne pour son opposition frontale au régime dictatorial du général Franco .
En 1965 commencent les attaques à main armée et l' encaissement de l' impôt révolutionnaire ( extorsion de fonds auprès des Basques ) .
L' attentat qui tua en 1973 Luis Carrero Blanco , chef du gouvernement et successeur probable du dictateur Franco , eut un rôle déterminant dans la fin du régime franquiste .
En 1977 les commandos spéciaux ( bereziak ) de ETA " politico-militaire " rejoignent ETA " militaire " .
ETA " politico-militaire " s' auto-dissout cette même année , ses militants abandonnant l' usage de la violence pour atteindre leurs objectifs .
Cependant , ETA " militaire " ( qui est désormais désignée simplement par " ETA " , ETA " politico-militaire " s' étant dissoute ) considéra comme traîtres ceux qui acceptaient de mettre fin à la lutte armée , et fit assassiner des membres d' ETA en exil qui rentraient au pays sous couvert de l' amnistie .
En 1995 , ETA commença à cibler des élus basques , des intellectuels basques ou des policiers basques considérés comme " traîtres " .
Une des actions les plus retentissantes de cette politique est l' assassinat d' un élu du Parti populaire , Miguel Angel Blanco .
Le sentiment de voir l' action de ETA dégénérer en guerre civile , ainsi que la lassitude de la population devant la violence , ont érodé le support populaire à ETA .
En septembre 1998 , ETA décida d' une trêve unilatérale .
Devant l' échec des négociations avec le gouvernement espagnol , ETA rompit la trêve en novembre 1999 .
À partir de 2002 le gouvernement central espagnol de José María Aznar , avec l' appui de l' opposition , poursuit une série d' actions contre les organisations politiques et culturelles basques contrôlées par l' ETA : interdiction de partis politiques , d' associations , fermeture de journaux et de radios .
En 1982 sont créés les Groupes Antiterroristes de Libération qui ont pour but d' eradiquer les militants ( ou supposé tels ) d' ETA , opérant de nombreux attentats et assassinats de 1983 à 1987 dont certains visaient à provoquer la terreur contre des civils .
ETA , une histoire basque , documentaire de 52 minutes diffusé sur France 5 .
