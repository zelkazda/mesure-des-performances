Il est le fils d' Henri IV et de Marie de Médicis et le père de Louis XIV .
L' image de ce roi est inséparable de celle de son principal ministre , le cardinal de Richelieu , son soutien indéfectible dans l' établissement de l' absolutisme .
Louis XIII grandit avec ses frères et sœurs au château de Saint-Germain-en-Laye .
Il partit alors vivre au Louvre au côté de son père pour y apprendre son futur rôle de roi .
Le jeune roi reçoit une éducation assez superficielle de la part de son précepteur , Gilles de Courtenvaux de Souvré .
Louis XIII se révèle aussi excellent dessinateur et bon cavalier , grâce à l' enseignement de son sous-gouverneur , Antoine de Pluvinel .
À la mort d' Henri IV en 1610 , Louis XIII monte sur le trône .
Le pouvoir est alors assuré par sa mère Marie de Médicis , qui gouverne le royaume comme régente .
Tout d' abord , il ne trouve aucun substitut à l' amour paternel auprès de sa mère Marie de Médicis , qui le considère comme quantité négligeable .
En grandissant , Louis XIII devient un être taciturne et ombrageux .
Or , la régence de Marie de Médicis est très difficile : la gestion des affaires par son gouvernement est mauvaise , et les forces du royaume hostiles à la centralisation du pouvoir qu' avait initiée Henri IV en profitent .
La politique pro italienne et pro espagnole de la Reine crée chez le petit roi un très lourd sentiment d' amertume .
Le 21 novembre 1615 à Bordeaux , Marie de Médicis marie le roi à Anne d' Autriche , infante d' Espagne .
C' est par un coup de force , le 24 avril 1617 , que Louis XIII accède au pouvoir .
Poussé par son favori Luynes il ordonne l' assassinat du favori de sa mère , Concino Concini et fait exécuter la Galagai sa femme , sœur de lait et dame de compagnie de sa mère .
Il exile Marie de Médicis à Blois et prend enfin sa place de roi .
À peine la paix faite , le roi se rend à Pau en Navarre , dont il est le souverain , pour y rétablir le culte catholique interdit par les protestants depuis un demi-siècle .
De 1620 à 1628 ( siège de La Rochelle ) , il combat et massacre les protestants , pille et détruit les fortifications de leurs places-fortes .
Celui-ci meurt de la scarlatine durant le siège de Monheurt , alors qu' il était déjà tombé en disgrâce .
Le duc de Rohan défend Montauban puis Montpellier contre les troupes de Louis XIII .
La plupart des historiens mettent en évidence l' étroitesse des relations entre Louis XIII et Richelieu qui écrit : " Je soumets cette pensée comme toutes les autres à votre majesté " pour signifier au roi qu' il ne tentera jamais de gouverner à sa place .
Le programme politique de Richelieu se décline de plusieurs manières : l' abaissement des grands féodaux , la rationalisation du système administratif et la lutte contre la maison de Habsbourg à l' extérieur .
La reddition de cette dernière ville , après un très long siège qui s' achève en 1628 , est suivie de la promulgation de l' édit de grâce d' Alès ( 28 juin 1629 ) , interdisant les assemblées politiques et supprimant les places de sûreté protestantes , mais maintenant la liberté de culte dans tout le royaume sauf à Paris .
Louis XIII doit faire face à l' hostilité d' une partie de la famille royale à l' égard de Richelieu et de sa politique anti-espagnole .
Il se brouille avec sa femme : en 1626 , la reine , poussée par la duchesse de Chevreuse , participe au complot du comte de Chalais , ayant pour but d' assassiner le roi .
Cette journée se termine par l' exil de la reine-mère à Moulins ( le roi ne la revit plus jamais ) , l' emprisonnement du chancelier Michel de Marillac et l' exécution du frère de celui-ci , le maréchal de Marillac , pour des motifs fallacieux .
Louis XIII doit mater plusieurs révoltes organisées par Gaston d' Orléans , son frère et potentiel héritier , et faire enfermer nombre de ses demi-frères comme le duc de Vendôme .
Conscient des dilemmes qui agitent le roi , Pierre Corneille lui dédie plusieurs répliques du Cid .
La légende qui fait de Louis XIII un fantoche soumis à Richelieu a pour origine le refus de nombre de contemporains de donner au roi le crédit des nombreuses exécutions qui eurent lieu sous son règne .
Reprenant la politique de son père , Henri IV , Louis XIII et Richelieu attendent l' occasion favorable pour desserrer cette domination diplomatique .
Depuis la mort de son père , la guerre contre l' Espagne a été à chaque fois reportée .
L' attention du roi est , à partir de 1631 , obnubilée par la guerre de Trente Ans .
La diplomatie française se rapproche des ennemis de l' Espagne , et particulièrement des puissances protestantes et finance ses ennemis .
Pendant plusieurs années , les deux pays se contentent d' une guerre froide ( passage du pas de Suse et Guerre de Succession de Mantoue ) .
Désormais , jusqu' à la fin du règne , le roi est engagé dans une terrible guerre durant laquelle il commande plusieurs fois personnellement ( siège de Corbie ) .
Il occupe ainsi la Catalogne révoltée dans la guerre des Faucheurs ( 1641 ) .
Le souci majeur de Louis XIII , durant son règne , est d' être de nombreuses années sans héritier mâle .
La naissance du dauphin , futur Louis XIV , en 1638 après 23 ans de mariage , alors que le roi et la reine ont 36 ans , le font surnommer " l' enfant du miracle " .
Les mémorialistes diffèrent sur l' attitude du roi à l' égard de son héritier : Tallemant des Réaux dit que le roi considéra son fils d' un œil froid , puis se retira .
Après la mort du cardinal , en décembre 1642 , le roi décide de se réconcilier avec certains des anciens conspirateurs comme son demi-frère , César de Vendôme et ses fils , le duc de Mercœur et le duc de Beaufort .
Son corps est porté à la Basilique Saint-Denis sans aucune cérémonie , selon son propre désir pour ne pas accabler son peuple d' une dépense excessive et inutile .
Anne d' Autriche n' en tient pas compte et le fait casser dès qu' elle en a connaissance .
Louis XIII est très pieux , profondément catholique .
Marie de Médicis a tout de même veillé à ce que son fils reçoive une éducation catholique sévère .
Louis XIII a horreur du péché .
Il rédige aussi , avec son confesseur , le père Nicolas Caussin , un livre de prières .
Sa politique religieuse active rallie le clergé ce qui limite les contestations catholiques à sa diplomatie d' alliance avec les puissances protestantes contre les Habsbourg .
Il achève la construction du pont Neuf , fait creuser le canal de Briare et crée le premier office de recensement des chômeurs et invalides .
Louis XIII est un roi-soldat comme son père .
Au nord , une grande partie du Hainaut est conquise avec la prise d' Arras .
À l' est , la Lorraine est intégralement occupée par les troupes françaises .
Enfin , le roi subventionne les expéditions de Champlain au Canada et favorise le développement de la Nouvelle-France .
De nombreux témoignages historiques ont conduit les historiens à s' interroger sur la sexualité de Louis XIII .
Son épouse est délaissée : dès sa nuit de ses noces avec Anne d' Autriche , le jeune Louis XIII a " de la honte et une haute crainte " selon les mots d' Héroard à aller voir la reine , contrairement à beaucoup de ses prédécesseurs .
Toutefois , la plupart des historiens et des romanciers qui soutiennent la thèse d' une non consommation du mariage de Louis XIII et Anne d' Autriche avant la naissance de Louis XIV oublient que la reine fit trois fausses couches , dont l' une consécutive à une chute accidentelle dans un escalier .
Autre raison ; le souvenir de la mésentente politique et conjugale entre ses parents : outre sa position antiespagnole , Marie de Medicis reprochait à Henri IV ses infidélités ouvertes .
Les récits de Tallemant des Réaux étant pour l' essentiel constitués de témoignages de seconde , voire de troisième main , ce dont l' auteur ne se cache pas , les historiens qui ont depuis examiné la thèse de l' homosexualité ou de la bisexualité de Louis XIII n' ont pu , comme pour la plupart des personnages historiques probablement homosexuels , apporter de preuves définitives , non plus que de preuves de la thèse de la stricte hétérosexualité de Louis XIII .
Ce roi apparaît dans de nombreux films , essentiellement grâce aux diverses adaptations du roman d' Alexandre Dumas , Les Trois Mousquetaires qui a été adapté une trentaine de fois .
Le règne de Louis XIII donne au cinéma de cape et d' épée , notamment dans les années cinquante et soixante , ses heures de gloire .
