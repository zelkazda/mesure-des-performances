Bien que diplômé d' HEC , il se lance tout d' abord dans une carrière de sportif professionnel , footballeur , coureur cycliste , puis s' essaye à la trompette , avant de devenir garagiste .
Sa voix de baryton l' amènera à chanter pour le chœur de la maîtrise de la cathédrale de Reims .
Puis finalement , il entre à l' école d' art dramatique Tania Balachova .
Une longue carrière théâtrale commence alors , et il travaille tout d' abord avec Jean Vilar .
Sur les planches , il jouera avec des metteurs en scène de renom , comme Roger Planchon ou Robert Hossein , jouant les plus grands auteurs de William Shakespeare à Roland Dubillard , en passant par Paul Claudel et Peter Ustinov .
Alain Resnais le fait débuter au cinéma à 28 ans , en 1959 , avec un petit rôle de soldat allemand dans Hiroshima mon amour .
Mais , c' est réellement à la fin des années 60 , avec ce même réalisateur , que ce comédien à la silhouette carrée se distingue notamment dans Je t'aime , je t'aime ou La guerre est finie .
Il jouera pour les plus grands réalisateurs , Jean Renoir , Henri-Georges Clouzot , Roman Polanski , Philippe Labro , Costa-Gavras , Claude Sautet , Nicole Garcia , Christophe Gans , Yves Boisset .
On le verra avec Marcel Bozzuffi , dans la distribution française de la super-production hollywoodienne French connection 2 , de John Frankenheimer .
