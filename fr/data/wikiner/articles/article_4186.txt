Le mont McKinley ou Denali dans une langue locale athapascane est la plus haute montagne d' Amérique du Nord .
Situé au centre de l' Alaska , aux États-Unis , il culmine à 6194 mètres au-dessus du niveau de la mer .
Depuis 1917 , la montagne fait partie intégrante de la zone protégée du parc national de Denali .
Les autochtones ont tendance à utiliser Denali et distinguent les deux selon le contexte .
Le mont McKinley est le point culminant de l' Amérique du Nord .
Le mont McKinley présente un plus gros volume et une plus haute élévation que le mont Everest .
La base du mont McKinley est grossièrement un plateau à 700 mètres d' altitude , soit une élévation verticale de 5500 mètres .
À l' équateur , un sommet de cette altitude aurait 47 % de la quantité de dioxygène disponible au niveau de la mer , mais en raison de sa latitude , la pression au sommet du mont McKinley est encore plus basse .
Le mont McKinley a la particularité de recevoir des précipitations neigeuses plus abondantes sur le versant méridional .
Cela s' explique par le fait que l' air plus chaud et humide vient de l' océan Pacifique , au sud , et est stoppé par l' immense barrière rocheuse de la chaîne d' Alaska .
Le mont McKinley est constitué de granite et de schiste , résultat du métamorphisme dans le massif , et , à une dizaine de kilomètres en profondeur , d' un pluton âgé de 56 millions d' années , .
Toutefois , le 21 mai 1991 , une secousse de 6,1 s' est produite à 112 kilomètres de profondeur , juste sous le mont McKinley .
Une des failles majeures qui résulte de ces mouvements tectoniques est la faille du Denali .
Ils étaient nomades et venaient régulièrement chasser dans les bas plateaux au nord du mont McKinley , au printemps et à l' automne .
Frederick Cook prétend avoir gravi la montagne en 1906 .
Il a été démontré plus tard qu' elle était frauduleuse , avec des preuves cruciales fournies par Bradford Washburn qui faisait des relevés d' un pic secondaire .
Toutefois , le récit de la journée de leur présumé succès est impressionnant : portant un sac de beignes , un thermos de cacao chacun et un poteau en épicéa de 4,20 mètres , deux d' entre eux atteignent le pic Nord et érigent le mât près du sommet .
Ce dernier commenta plus tard : " La vue depuis le sommet du mont McKinley est comme regarder par la fenêtre de l' enfer " .
Le parc de Denali abrite également l' une des plus riches réserves d' oiseaux .
Dans la série de romans Twilight ( Fascination , Tentation , Hésitation ) , écrite par Stephenie Meyer , le Denali est le refuge d' un sabbat de vampires .
