Grâce aux standards du web , les SGC offrent donc un format de données lisible ( HTML et ses dérivés ) , imprimable et stockable par tous , ce qui facilite l' échange et l' accessibilité des documents .
La séparation du contenu et de la forme est toutefois partiellement réalisée par l' usage du concept de styles , à l' image du rendu de l' HTML par des feuilles de style ( Cascading Style Sheet ou CSS ) en texte marqué .
La plupart des projets CMS libres fonctionnant sur le Web proposent de créer des forums associés aux articles pour laisser les visiteurs réagir .
