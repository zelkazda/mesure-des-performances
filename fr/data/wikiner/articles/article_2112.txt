Elle a été instituée le 19 mai 1802 par Napoléon Bonaparte .
Elle récompense depuis ses origines les mérites éminents militaires ou civils rendus à la Nation .
Le plus jeune décoré de l' histoire de la légion d' honneur est le général de corps d' armée Jean Vallette d' Osia en 1917 à l' âge de 19 ans ; il est aussi celui qui l' aura portée le plus longtemps , 82 ans , et sera décoré du grand cordon en 1978 par Valéry Giscard d' Estaing .
L' obtention d' une médaille d' or aux Jeux olympiques fait l' objet d' une promotion spéciale .
De plus , à l' origine , les descendants de trois générations successives de décorés de la Légion d' honneur obtenaient ladite décoration par l' hérédité .
L' insigne est suspendu à un ruban rouge ( que certains estiment peut-être hérité de l' ordre militaire de Saint-Louis ) .
L' ordre de la Légion d' honneur , institué par la loi du 19 mai 1802 prise en application de l' article 87 de la Constitution de l' An VIII , est une communauté constituée de tous ses membres , dotée d' un nom , d' un sceau , d' un statut , d' un patrimoine , et d' une personnalité juridique de droit public .
On le voit sur les photos officielles puis il est déposé au musée de la Légion d' honneur .
Depuis Lacépède , la plupart des chanceliers furent des militaires .
Les responsabilités du grand chancelier sont assez étendues : il a la charge de tous les problèmes liés aux décorations en France .
Il est également grand chancelier de l' ordre national du Mérite .
Le grand chancelier est assisté d' un conseil réunissant des membres divers de la Légion , civils et militaires , à partir du grade de commandeur .
Ce palais abrite aussi le musée de la Légion d' honneur .
Ce fut le cas de Maurice Papon qui , bien que s' étant vu retirer cette décoration , a néanmoins tenu à être enterré avec .
En 1981 , le général d' armée Alain de Boissieu , grand chancelier de la Légion d' honneur depuis 1975 , démissionne avec fracas de la chancellerie pour ne pas avoir à remettre , comme cela est la tradition pour tout président élu , le collier de grand maître de l' ordre à François Mitterrand , parce que ce dernier avait par le passé traité Charles de Gaulle ( dont le général de Boissieu était le gendre ) de " dictateur " , .
