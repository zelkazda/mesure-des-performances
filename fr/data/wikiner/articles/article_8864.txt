Le père de cette philosophie est alors Jeremy Bentham , qui s' inspire notamment de Hume et Helvétius .
Bentham propose une première forme d' utilitarisme , plus tard caractérisée d' utilitarisme hédoniste .
C' est toutefois avec l' apport de John Stuart Mill que l' utilitarisme devient une philosophie véritablement élaborée .
C' est Jeremy Bentham qui introduisit le vocable en 1781 et qui tira de ce principe les implications théoriques et pratiques les plus abouties .
Ce principe est formulé ainsi par Bentham " La nature a placé l' humanité sous l' empire de deux maîtres , la peine et le plaisir .
Bentham affirme qu' il ne peut y avoir de conflit entre l' intérêt de l' individu et celui de la communauté , car si l' un et l' autre fondent leur action sur l ' " utilité " , leurs intérêts seront identiques .
Fils de James Mill , filleul et disciple de Bentham , John Stuart Mill est le successeur immédiat de l' utilitarisme benthamien .
Là où Bentham identifie welfare et plaisir , Mill définit le welfare comme bonheur .
On doit aussi à Mill la reconnaissance de la dimension qualitative des plaisirs .
Contrairement à Bentham , qui ne hiérarchise pas les plaisirs et s' intéresse uniquement à la quantité de ceux-ci , John Stuart Mill défend une différence de qualité entre les plaisirs .
Le philosophe utilitariste Peter Singer se souviendra de cet aspect dans son opposition au spécisme .
Le problème n' est plus dès lors dans l' opposition entre déontologisme et conséquentialisme , mais dans l' interprétation que Kant donne de son propre déontologisme .
Kant veut l' asseoir sur un impératif catégorique , c' est-à-dire une maxime totalement indépendante de ses conséquences , alors qu' en réalité , il s' agit plus probablement d' un impératif hypothétique .
C' est sur la base de l' utilitarisme des préférences que Peter Singer accorde une importance plus grande à la préservation de la vie de certains êtres sensibles -- ceux qui sont capables de former des projets pour leur avenir et donc de préférer rester en vie -- qu' à celle d' autres -- ceux qui n' ont pas cette capacité , et dont les préférences se résument à la recherche à court terme de plaisir et à l' évitement de la souffrance .
Comme le souligne John Stuart Mill , il y a deux fins que la doctrine demande de poursuivre :
( entre autres , Alfred Marshall , Arthur Cecil Pigou et John Harsanyi )
Notons enfin que , si l' on adopte l' utilitarisme à seuil en prenant le revenu par tête comme variable ( objective ) substituée à l' utilité subjective , alors on retrouve à peu près la théorie de la justice distributive proposée par Raymond Boudon pour pallier les défauts du maximin de John Rawls : une maximisation de la moyenne des revenus sous contrainte de plancher .
Sur cette question , on distinguera la position de William Godwin de celle des utilitaristes réels : contrairement à eux , Godwin ne remplit pas le critère d' impartialité du calcul , ce qui l' amène à défendre un sacrifice partial où la maxime " un compte pour un " n' est pas respectée .
Il faut donc faire la part entre le point de vue pseudo-utilitariste de Godwin et celui de l' utilitarisme .
Ce reproche que l' on fait souvent à l' utilitarisme de Mill est un faux procès .
Et c' est aussi l' intuition de Rawls .
Bernard Williams propose un exemple dans lequel les conséquences restent inchangées quel que soit l' agent .
