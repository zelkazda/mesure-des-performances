Durant la quasi-totalité de son cours , il traverse le nord-est de la Bretagne ( Ille-et-Vilaine ) ce qui constituait , dans ses derniers kilomètres , la frontière entre le duché de Bretagne et le duché de Normandie .
Son cours jadis très irrégulier a inspiré le dicton : " Le Couesnon dans sa folie a mis le Mont en Normandie " puisque le mont Saint-Michel se situe du côté normand de son embouchure actuelle .
La frontière entre l' Ille-et-Vilaine et la Manche se situe néanmoins aujourd'hui à 4 kilomètres à l' ouest du fleuve dont les vasières autrefois plus étendues se sont ensablées ou ont été comblées et aménagées par l' homme pour canaliser son cours .
Le Couesnon , la Sée et la Sélune participent au fonctionnement hydraulique particulièrement complexe de la baie du Mont-Saint-Michel .
Ces divers aménagements , ainsi que la digue d' accès au Mont-Saint-Michel accélèrent l' envasement du site .
Pour éviter qu' il ne perde son caractère insulaire , il est prévu de remplacer la digue par un viaduc d' accès et de modifier le barrage du Couesnon pour qu' il puisse à nouveau faire office de chasse .
Le Couesnon a pour affluent la Minette .
Ce fleuve devient la frontière entre la Bretagne et la Normandie en 1009 .
Auparavant , ce fut la Sélune ( entre 933 et 1009 ) .
C' est quelques décennies plus tard que le changement du cours du Couesnon aurait placé le Mont-Saint-Michel en Normandie , puisqu' il était clairement normand à l' époque de sa prise par Philippe-Auguste et Guy de Thouars le 29 avril 1204 -- à moins que le changement de cours du Couesnon ne soit qu' une légende et que le Mont soit normand depuis 1009 .
