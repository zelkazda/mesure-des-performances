Il acquiert aussi ses premières lettres de noblesse grâce à des grands réalisateurs comme Méliès , Griffith ou Dreyer qui en fixent les règles de langage .
La photographie , quant à elle , naît en 1839 sous l' impulsion de Jacques Daguerre .
Eadweard Muybridge a précisément l' idée en 1878 d' aligner vingt-quatre appareils photographiques pour décomposer le mouvement d' un cheval lancé au galop .
Étienne-Jules Marey qui travaille également sur le mouvement des animaux crée en 1882 un fusil photographique qu' il dote ensuite d' une pellicule : le chronophotographe .
Émile Reynaud donne naissance au théâtre optique .
L' américain George Eastman invente la pellicule permettant ainsi d' aligner plusieurs images en négatif sur un film transparent .
Louis Aimée Augustin Le Prince construit et dépose le brevet d' une caméra le 11 janvier 1888 .
Cependant , en 1890 , après avoir amélioré sa caméra , l' inventeur disparaît mystérieusement dans le train express Dijon -- Paris .
En Angleterre l' inventeur William Friese-Greene met au point une caméra capable de fixer dix images par seconde à l' aide d' un film perforé .
Pour alimenter ce commerce , la compagnie d' Edison construit le premier studio de l' histoire : la Black Maria .
Le nom " cinématographe " apparaît lui dans un brevet déposé le 12 février 1892 par Léon Bouly , d' abord sous l' intitulé " cynématographe Léon Bouly " , puis " cinématographe " , dans un autre brevet qu' il dépose le 27 décembre 1893 .
Les frères Lumière déposent le brevet de leur cinématographe le 13 février 1895 .
Le gérant du Musée Grévin , celui des Folies Bergères et Georges Méliès qui y assistent , surenchérissent pour s' accaparer l' appareil .
En vain puisque Auguste Lumière refuse de le leur vendre .
En effet , les frères Lumière conservent pour eux l' exploitation de leur invention .
Au Royaume-Uni , Robert William Paul exploite déjà un plagiat du kinétoscope ; il commercialise bientôt un appareil de projection sous le nom d ' " animatograph " ou " theatrogoraph " .
Les frères Lumière forment et envoient des opérateurs de par le monde pour faire la promotion de leur cinématographe .
Dans les pays qu' ils traversent , Charles Moisson , Francis Doublier , Félix Mesguich , Alexandre Promio , Gabriel Veyre tournent et projettent leurs réalisations devant un public médusé .
Au passage , la société des frères Lumière concède des licences d' exploitation de l' appareil notamment au Royaume-Uni et en Russie .
Le Danemark voit en 1898 son premier cinéaste Peter Elfelt filmer les vacances d' été de la famille royale danoise à l' aide d' un appareil de sa propre fabrication .
La première bande allemande est aussi tournée en 1898 montrant une promenade de jeunes gens dans la Forêt-Noire .
L' Italie se lance dans la production cette même année avec une course automobile .
La Russie , elle , n' entame véritablement son histoire cinématographique qu' en 1907 avec le photographe Alexandre Drankov qui fonde son propre studio .
Thomas Edison déclenche en 1897 ce qu' on a appelé la " guerre des brevets " .
Un an après , ne restent plus que deux studios américains : la Vitagraph Company of America de Thomas Edison , et l' American Mutoscope and Biograph Company , société cofondée par William Dickson , qui exploite le mutoscope puis met au point la biograph , une caméra imposante mais de grande qualité .
Cette même année 1897 , en France , l' incendie du Bazar de la Charité marque suffisamment les esprits pour freiner le développement du cinématographe et entacher sa réputation .
Son inventeur , Thomas Edison , songe dès 1894 à coupler son kinétoscope d' un cylindre phonographique , et commercialise le kinétophone .
Dans ce cadre , Eugène Promio invente le travelling à Venise au printemps 1896 .
Le film obtient une certaine renommée et est exporté aux États-Unis .
Il a recours au trucage avec l' Escamotage d' une dame au théâtre Robert Houdin , arrêtant la prise de vue et la reprenant , substituant au passage une femme à un squelette .
L' homme s' illustre d' abord dans l' actualité reconstituée inspirée des illustrations du Petit Journal .
Mais c' est encore dans la féerie que Georges Méliès s' épanouit , peignant lui-même d' immenses toiles de fond , utilisant les procédés de surimpression déjà connus de la photographie .
Cependant , Georges Méliès dont l' expérience vient du spectacle vivant s' en tient à une narration théâtrale .
Ce procédé primaire de narration est aussi celui adopté par les concurrents de Georges Méliès que sont Charles Pathé et Léon Gaumont .
Charles Pathé recrute Ferdinand Zecca en 1900 .
Quant à Léon Gaumont , plus intéressé par la technique , il confie ses productions à sa secrétaire Alice Guy à qui l' on doit sans doute le premier film narratif original , La Fée aux choux , réalisé dès 1896 avec les moyens du bord .
D' après la définition de la Cinémathèque française selon laquelle un long métrage est un film de plus de soixante minutes , ce film d' une durée de 60 à 70 minutes est le premier long métrage de l' histoire du cinéma .
Ce film raconte l' histoire du bushranger Ned Kelly . .
En 1901 , les deux frères Pathé commencent à fonder leur empire en construisant une usine phonographique à Chatou , ainsi que deux studios de cinéma , dont les films produits sont signés par le label " Pathé " .
En 1904 , Pathé distribue 50 % des films diffusés en Europe et en Amérique .
Dans les années 1910 , le cinéma américain se distingue par ses comiques : Mack Sennett , Charlie Chaplin , Fatty , Buster Keaton , Harold Lloyd ...
Les premières stars apparaissent , ainsi Mary Pickford reçoit 100.000 $ par an .
David Wark Griffith ( avec surtout Naissance d' une nation et Intolérance ) , Cecil Blount DeMille ( avec Forfaiture ) , Erich von Stroheim , Thomas H. Ince font leur début dans le métier aux États-Unis .
En 1919 , Mary Pickford , Douglas Fairbanks , Charles Chaplin et David Wark Griffith créent la United Artists Corporation ( aussi connu sous le nom des Artistes associés ) .
En France c' est les débuts des réalisateurs Abel Gance , Germaine Dulac , Léonce Perret et Marcel L' Herbier .
Un des premiers succès du cinéma français est un serial : Fantomas , film en 5 épisodes , réalisé par Louis Feuillade qui réitèrera l' initiative avec Les Vampires .
Ce sont les débuts de Fritz Lang , Ernst Lubitsch et Robert Wiene ( Le Cabinet du docteur Caligari ) .
En 1917 en Russie , Le Père Serge de Yakov Protazanov donne à Ivan Mosjoukine une stature de célébrité dans le monde du cinéma .
En 1921 , Le Kid rend définitivement célèbre Charlie Chaplin .
En 1922 , Nanouk l' esquimau de Robert Flaherty permet aux spectateurs de découvrir un monde inconnu , celui du grand nord .
En 1928 , Renée Falconetti incarne Jeanne d' Arc dans La Passion de Jeanne d' Arc de Carl Theodor Dreyer .
En août 1932 a lieu la première Mostra de Venise .
Le 28 avril 1937 , les studios de Cinecittà à Rome sont inaugurés , ils devaient faire concurrence à Hollywood dans la pensée de Luigi Freddi , le directeur .
Le 1 er septembre 1939 devait avoir lieu le premier festival de Cannes présidé par Louis Lumière mais la guerre éclate avec l' invasion de la Pologne par l' Allemagne , le festival sera reporté après la guerre .
Dans le cinéma comique , les Marx Brothers apparaissent avec les films Noix de coco et Monnaie de singe , leur comique est basé sur la dérision et le non-sens .
Cette société présidée par Alfred Greven produira 30 films français jusqu' en 1944 ( dont Le Corbeau de Henri-Georges Clouzot et Les Inconnus dans la maison de Henri Decoin ) .
Les accords Blum-Byrnes de 1946 imposent , en contrepartie d' un effacement partiel de la dette de la France envers les États-Unis , la projection de films américains dans les salles françaises , ce qui contribue à l' américanisation du cinéma en France et en Europe .
