Sa musique est , de son propre aveu , profondément ancrée dans la pop , mais Björk a su en repousser les limites .
Björk grandit en Islande aux côtés de sa mère , dans une communauté hippie .
Björk enregistre son premier album à l' âge de 11 ans , durant l' été 1977 .
L' adolescence de Björk est marquée par son appartenance à un grand nombre de groupes locaux .
En 1983 , Björk crée avec quelques amis le groupe punk KUKL , le premier dont les chansons vont traverser les frontières islandaises .
Bien que le succès soit très limité et cantonné au Royaume-Uni , c' est là un premier pas vers une carrière internationale .
En 1987 , les membres de KUKL sabordent le groupe et fondent The Sugarcubes , qui deviendra vite le groupe islandais le plus célèbre au monde ( à l' époque ) .
Ayant produit trois albums ( sans compter un album de remixes et une anthologie ) , The Sugarcubes connaît un succès incroyable ( même si ce succès se centre assez vite sur la personnalité de sa chanteuse , Björk ) et part en tournée dans le monde entier .
Parallèlement , Björk commence à vivre ses premières expériences de collaboration extérieure , enregistrant deux chansons pour 808 State , un groupe de house britannique .
Elle sort également Gling-Gló , un album de jazz composé principalement de reprises en islandais de standards du genre .
Elle travaille en collaboration avec Nellee Hooper , producteur de Massive Attack .
Björk obtient des prix dès ce premier album , comme le Brit Award de la meilleure nouvelle artiste internationale féminine .
Debut contient plusieurs titres qu' elle avait écrits durant son adolescence .
Après la parution de l' album , Björk entame une nouvelle tournée mondiale , lançant ainsi un rythme de travail qu' elle n' a jamais abandonné jusqu' à ce jour : un album , une tournée .
Tout comme Debut , Post est un patchwork de musique nouvelle et de textes écrits longtemps auparavant dans sa carrière .
Si Debut montrait une jeune fille chétive et timide , si Post montrait une adolescente excentrique , Homogenic montre une femme mature , maturité évoquée par la droiture de la posture prise par Björk pour la couverture de l' album ; mais , dans cette image , il reste quelque chose d' inhumain , le thème de l' hybridation persiste .
Homogenic est alors considéré comme l' album le plus expérimental de la chanteuse .
On y retrouve la dualité sentimentale , entre amour et colère , qui existait déjà dans Post .
Björk dit que pour Homogenic , elle n' a gardé que les trois bruits essentiels , les trois bruits qui existent depuis la nuit des temps -- les plus forts : la respiration , le cœur qui bat et les nerfs qui frémissent [ réf. nécessaire ] .
2000 est une année particulière pour Björk .
Dancer in the Dark , le film de Lars von Trier , sort sur les écrans .
Elle y incarne le personnage principal et en compose la musique , que l' on peut retrouver en partie remaniée dans l' album Selmasongs .
L' actrice et le film sont récompensés au Festival de Cannes .
C' est en 2001 que sort l' album Vespertine , album dans lequel Björk crée un univers intimiste et introverti fait de beats légers et fugaces .
C' est après l' expérience de Dancer in the Dark , que la chanteuse dit avoir très mal supporté , qu' elle effectue un retour en Islande , un retour aux origines , qui est un des thèmes de l' album .
L' image médiatique qu' elle s' était créée continue donc sa maturation commencée avec Homogenic , mais maintenant beaucoup plus sombre .
La même année , Björk sort un livre , constitué de photos , de dessins et de quelques textes , laissant la place à plusieurs de ses collaborateurs artistiques , notamment les réalisateurs de ses clips .
En 2004 Björk enregistre Medúlla , un album entièrement consacré à la voix humaine , pour lequel elle s' entoure de plusieurs chanteurs et collaborateurs venus du monde entier .
En dehors de quelques performances , aucune tournée ne fut organisée pour Medúlla .
Björk déclara que c' était pour continuer immédiatement l' écriture de l' album suivant .
En juin 2004 elle déclara dans une interview pour le magazine Rolling Stone : " À chaque album que j' ai fait , au moment où je le terminais , j' étais encore très inspirée , et je pensais pouvoir réaliser encore un album en cinq minutes ; alors , je voulais juste savoir si c' était seulement une fantaisie ou si c' était vrai " .
En juillet-août 2005 , sort Drawing Restraint 9 , bande originale que Björk a composée pour le film éponyme .
Dans ce film , elle joue l' un des rôles principaux , au côté de son mari , qui en est aussi le réalisateur , Matthew Barney .
Drawing Restraint 9 est un film réalisé à l' occasion du 60 e anniversaire commémorant les bombardements atomiques de Hiroshima et Nagasaki .
Le thème du film étant le Japon , elle y utilise des instruments traditionnels japonais .
Ces deux éléments , qui jusque là n' étaient utilisés que de manière fugace dans ses compositions , prendront une place importante dans l' album suivant , Volta .
Le chœur devait se produire lors de deux tournées , accompagné de Björk , lors du millénium en Islande et durant l' été à travers toute l' Europe .
Malheureusement , Björk se ravise pour la seconde partie du projet ; certaines villes auraient toutefois abusé de l' image de la chanteuse pour promouvoir les concerts donnés par le chœur .
Le 17 novembre 2006 , le premier groupe a succès mondial de la chanteuse , les Sugarcubes , se reforme pour un concert d' un soir , à Reykjavík .
Selon les dires de Björk dans la presse après ce concert , elle a choisi de ne pas profiter des gains amassés , dans le but d' aider la scène musicale islandaise à émerger .
En 2006 , ses collaborations avec Antony et Toumani Diabaté sont enregistrées entre l' Islande et la Jamaïque .
Son dernier album , Volta , est sorti en mai 2007 .
Le Tibet ! "
Volta est surtout teinté de cuivres ( cor , tuba , trompette , trombone ... ) et de percussions .
La première chose qui frappe en écoutant Björk , outre sa voix puissante , c' est cette façon très étrange de scander les syllabes , particularité qui semble provenir directement de la tradition musicale médiévale islandaise , les fameux " rímur " , chorales exclusivement masculines .
Le principe que retient Björk est en effet la juxtaposition de références d' origines fortement éloignées .
la ligne mélodique de la voix sur le passage " to enjoy " est caractéristiquement inspirée des lignes de Peter Tosh ou de Bob Marley .
Björk aime inviter des compositeurs et producteurs provenant des tendances les plus diverses sur ses albums ( à l' exception de Debut et , dans une moindre mesure , de Homogenic , qui se veulent profondément inspirés par ses racines islandaises ) .
Cette collaboration se poursuit d' ailleurs au-delà des albums : pour presque chacune de ses chansons , Björk engage une horde hétéroclite de remixeurs pour les réinterpréter .
Cette philosophie du remixage , à mille lieues des traditionnels " boum boum " prévisibles des remixes pour discothèques , trouve son point culminant dans Telegram , disque entièrement voué à cette forme musicale .
Enfin , Björk considère le clip vidéo comme un prolongement à part entière de son œuvre et s' y implique pleinement .
Björk poursuit ses recherches artistiques , en dehors de la musique .
Elle débute sa carrière au cinéma en 1986 dans le rôle principal du film The Juniper Tree .
En photographie , Björk pose pour les photographes les plus en vogue , tels que Jean-Baptiste Mondino .
Enfin , son goût pour la mode d' avant-garde lui vaut parfois les moqueries des commentateurs , mais lui fait rencontrer une foule de stylistes parmi lesquels Alexander McQueen , qui a signé la pochette d ' Homogenic .
Pour ses vidéo-clips , elle fait appel à des réalisateurs plus ou moins renommés , comme Sophie Muller , Stéphane Sednaoui , Michel Gondry , Spike Jonze , Chris Cunningham , Michel Ocelot ou Jean-Baptiste Mondino .
( Tibet , Tibet , lève ton drapeau ! ) .
En 2005 , elle participe au Live 8 , série de concerts organisée pour faire pression sur les dirigeants du G8 pour qu' ils effacent la dette publique des pays les plus pauvres .
Do As Infinity , Good Charlotte et McFly jouaient au même concert .
Entre-temps , elle a multiplié les relations , plus ou moins médiatisées , dont Goldie et Stéphane Sednaoui .
