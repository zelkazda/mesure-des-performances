Au début des années 1970 , il travaille avec Ken Thompson en tant que programmeur dans les Laboratoires Bell sur le développement de Unix .
Pour Unix , il s' avère nécessaire d' améliorer le langage B créé par Ken Thompson et c' est ainsi que Ritchie crée le langage C .
Par la suite , aidé de Brian Kernighan , il promeut le langage et rédige notamment le livre de référence The C Programming Language .
En 1967 , il commença à travailler aux Laboratoires Bell .
Son invention du langage C et sa participation au développement d' Unix au côté de Ken Thompson ont fait de lui un pionnier de l' informatique moderne .
Le langage C reste aujourd'hui un des langages les plus utilisés , tant dans le développement d' applications que de systèmes d' exploitation .
Unix a aussi eu une grande influence en établissant des concepts qui sont désormais totalement incorporés dans l' informatique moderne .
