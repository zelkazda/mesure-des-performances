De par ses revenus , Terry Gilliam se paie une camera 16mm , commence un court métrage qu' il ne finira jamais .
Gilliam participe à la série télé Monty Python 's Flying Circus comme acteur et comme auteur des animations , souvent des collages loufoques , qui relient les sketches entre eux .
Gilliam est le seul américain de la bande .
En 1971 , l' équipe réalise La Première Folie des Monty Python , un film regroupant les meilleures séquences de la série , agrémenté de quelques nouveautés .
En 1975 , il co-réalise avec Terry Jones , Monty Python : Sacré Graal ! .
En 1979 , il écrit et joue dans Monty Python : La Vie de Brian .
En 1981 , il réalise Bandits , bandits , qui raconte l' histoire de six nains ayant volé la carte des trous du temps à l' être suprême , entraînant un garçon de onze ans dans des aventures spatio-temporelles .
Le recul aidant , ce petit film affirmera la singularité de Gilliam .
En 1987 , Gilliam tourne Les Aventures du baron de Münchhausen entre Cinecitta et les studios anglais de Pinewood .
Il travaille ensuite sur l' adaptation de Watchmen , tirée d' une bd de super héros .
Le film devait être tourné avec Nicolas Cage , mais le projet n' aboutit jamais .
En 1995 , il réalise L' armée des douze singes , avec des têtes d' affiche , tels que Bruce Willis et Brad Pitt , adapté du film La Jetée de Chris Marker .
Ensuite , il tourne Las Vegas Parano , avec Johnny Depp film complètement psychédélique , d' après le roman culte de Hunter S. Thompson .
Lorsqu' il réussit à réunir les fonds , exclusivement européens , les acteurs également , tout le nécessaire afin d' amener le film à bien , Gilliam subit tous les malheurs du monde .
Son assistant n' y croit pas , Jean Rochefort souffre d' une hernie , les lieux de tournages s' avèrent être calamiteux ...
Les problèmes se multiplient en à peine dix jours et Gilliam décide d' abandonner le tournage .
Après sept années de galère , Gilliam tourne Les Frères Grimm avec Matt Damon , Heath Ledger et Monica Bellucci .
La sortie du film fut retardée plusieurs fois , et Gilliam profita de ce " temps libre " pour réaliser un film plus personnel Tideland .
En 2009 , Gilliam sort le film L' Imaginarium du docteur Parnassus .
Il s' agit du dernier film de Heath Ledger , mort pendant le tournage .
Les films de Gilliam sont dépeint souvent un univers très sombre et pessimiste .
Tout comme ceux de Tim Burton , les films de Terry Gilliam se distinguent par la création d' un univers poétique -- narratif et visuel -- singulier , à l' esthétique très soignée ( Las Vegas Parano , Le Roi Pêcheur , L' Armée des douze singes , Les Frères Grimm , Tideland … )
À l' instar d' autres cinéastes ( comme Stanley Kubrick ) , son parcours est ponctué de nombreux projets avortés ( voir ci-dessous ) .
Sa propension à échouer ( Gilliam revendique même son droit à l' erreur ) contribue à faire du cinéaste à l' immense créativité artistique un " loser magnifique " .
