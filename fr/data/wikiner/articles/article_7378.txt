Dès le début des années 1800 , Friedrich Fröbel , créateur des écoles maternelles , reconnaissait que l' assemblage , le tressage , le pliage et le découpage du papier étaient des aides pédagogiques pour le développement des enfants .
Joseph Albers , le père de la théorie moderne des couleurs et de l' art minimaliste , a enseigné l' origami et le pliage du papier dans les années 1920 et 1930 .
Le travail du Japonais Akira Yoshizawa , un créateur prolifique de modèles d' origami et auteur de livres sur l' origami , a inspiré la renaissance contemporaine .
La grue d' origami est devenue un symbole de paix en raison de cette légende , et d' une jeune fille japonaise appelée Sadako Sasaki .
Pour un Japonais , l' origami est plus qu' un art : il est culture vivante .
L' origami peut prendre des formes aussi simples qu' un chapeau ou qu' un avion en papier , ou aussi complexe qu' une représentation de la Tour Eiffel , une gazelle ou un stégosaure , qui demandent plus d' une heure et demie de travail .
