Philémon est une série et un personnage de bande dessinée de Fred .
Il se promène entre le monde réel , tel que celui où nous vivons , et un monde où les lettres de l' océan Atlantique forment des îles .
Philémon est un adolescent de grande taille , large d' épaules , un peu voûté .
Philémon a un caractère doux et rêveur .
Par maladresse ou par gentillesse , il se retrouve mêlé à des aventures inattendues , emporté dans le monde onirique des lettres de l' Océan Atlantique .
Dans cette bande dessinée , les personnages principaux qui accompagnent Philémon sont :
