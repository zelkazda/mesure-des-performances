Certains se demandent aussi si la Menorah ne dérive pas d' un antique " arbre sacré " , ce qui expliquerait sa forme arrondie dans le Temple détruit par Titus .
Contrairement à la forme plus fréquente de la Ménorah , la tradition juive ( Maïmonide dans son célèbre dessin et Rachi dans son commentaire à Exode 25 , 32 ) suggère plutôt une forme selon laquelle les six branches seraient des diagonales droites .
Flavius Josèphe écrit : " On lui a donné autant de branches qu' on compte de planètes avec le soleil " .
C' est une " imitation de la sphère céleste archétype " selon Philon .
Clément d' Alexandrie considérait le chandelier à sept branches comme un équivalent de la croix du Christ .
Aujourd'hui , la ménorah est aussi l' emblème de l' état d' Israël .
