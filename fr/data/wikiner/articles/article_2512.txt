La Haute-Loire est subdivisée en 260 communes .
Le tableau suivant en donne la liste , en précisant leur code INSEE , leur code postal principal , leur appartenance aux principales structures intercommunales en 2004 , leur superficie et leur population .
Suivant la classification de l' INSEE , la typologie des communes de la Haute-Loire se répartit ainsi :
Note : les données présentées ici ne concernent que les communes appartenant à la Haute-Loire .
Il est possible qu' une aire urbaine s' étende sur plusieurs départements ( c' est le cas de celle de Saint-Étienne ) .
Les aires urbaines de la Haute-Loire se rattachent aux espaces urbains suivants :
