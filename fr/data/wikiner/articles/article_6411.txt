Bourdieu insiste sur l' importance des facteurs culturels et symboliques dans cette reproduction et critique le primat donné aux facteurs économiques dans les conceptions marxistes .
Ce que Pierre Bourdieu nomme la violence symbolique , qu' il définit comme la capacité à faire méconnaître l' arbitraire de ces productions symboliques , et donc à les faire admettre comme légitimes , est d' une importance majeure dans son analyse sociologique .
Le monde social , dans les sociétés modernes , apparaît à Pierre Bourdieu comme divisé en ce qu' il nomme des champs .
Ainsi , comme les analyses marxistes , Pierre Bourdieu insiste sur l' importance de la lutte et du conflit dans le fonctionnement d' une société .
Pour Bourdieu , les conflits ne se réduisent donc pas aux conflits entre classes sociales sur lesquels se centre l' analyse marxiste .
Pierre Bourdieu a également développé une théorie de l' action , autour du concept d' habitus , qui a exercé une grande influence dans les sciences sociales .
L' œuvre de Bourdieu est ainsi ordonnée autour de quelques concepts recteurs : habitus comme principe d' action des agents , champ comme espace de compétition social fondamental et violence symbolique comme mécanisme premier d' imposition des rapports de domination .
Pierre Bourdieu est né en 1930 dans les Pyrénées-Atlantiques à Denguin , petit village du Béarn .
Sa mère a une origine sociale proche , quoique légèrement supérieure , puisqu' elle est issue d' une lignée de propriétaires à Lasseube .
Il est reçu à l' École normale supérieure de la rue d' Ulm , dans la même promotion qu' Emmanuel Le Roy Ladurie .
Alors que la scène philosophique française est dominée par la figure de Jean-Paul Sartre et par l' existentialisme , Bourdieu réagit comme de nombreux normaliens de sa génération ; ces derniers se sont orientés préférentiellement vers l' étude des " courants dominés " du champ philosophique : le pôle de l' histoire de la philosophie proche de l' histoire des sciences , représenté par Martial Guéroult et Jules Vuillemin , et l' épistémologie enseignée par Gaston Bachelard et Georges Canguilhem .
En plus de son cursus , il suit aussi le séminaire d' Éric Weil à l' EPHE sur la philosophie du droit de Hegel .
Agrégé de philosophie en 1954 , il s' inscrit auprès de Georges Canguilhem pour une thèse de philosophie sur les structures temporelles de la vie affective , qu' il abandonnera en 1957 afin de se consacrer à des études sociologiques de terrain .
Georges Canguilhem place son thésard à proximité de Paris , comme professeur au lycée de Moulins en 1954 -- 1955 .
Mais Pierre Bourdieu doit remplir ses obligations militaires .
Cependant , il est trouvé en possession d' un numéro censuré de L' Express relatif à la question algérienne .
Le terrain ethnologique de la Kabylie ne cessa , même après qu' il eut cessé de s' y rendre , de nourrir l' œuvre anthropologique de Pierre Bourdieu .
En 1960 , il regagne Paris , pour devenir l' assistant de Raymond Aron à l' université de Paris .
Au milieu des années 1960 , il s' installe avec sa famille à Antony , dans la banlieue sud de Paris .
La famille rejoint le Béarn pendant les vacances scolaires .
En 1964 , Bourdieu rejoint l' École pratique des hautes études ( EPHE ) , qui deviendra en 1975 l' École des hautes études en sciences sociales ( EHESS ) .
La structure préservant les missions des deux entités est dirigée par son élève Rémi Lenoir .
La réception des travaux de Pierre Bourdieu va progressivement dépasser le milieu de la sociologie française .
Il est en particulier lu dans les milieux historiens francophones , notamment à l' EHESS .
Grâce notamment à l' appui d' André Miquel , il devient professeur au Collège de France en 1981 -- position la plus prestigieuse au sein du système universitaire français .
Il est le premier sociologue à recevoir la médaille d' or du CNRS en 1993 .
Parallèlement à sa carrière universitaire , Pierre Bourdieu a mené une importante activité d' éditeur , qui lui a permis de pleinement diffuser sa pensée .
Dans cette collection , Pierre Bourdieu publie la plupart de ses livres , ainsi que ceux de chercheurs influencés par lui , favorisant ainsi la diffusion de sa pensée .
Bourdieu publie également des classiques des sciences sociales ( Émile Durkheim , Marcel Mauss , etc . )
ou de la philosophie ( Ernst Cassirer , Erwin Panofsky , etc. ) .
À partir du début des années 1980 , Pierre Bourdieu s' implique davantage dans la vie publique .
Il participe notamment au soutien à Solidarność en partie en raison de la sollicitation de Michel Foucault .
Durant la guerre civile en Algérie , il soutient les intellectuels algériens .
De cette période date également La Misère du monde ( 1993 ) , ouvrage d' entretiens , qui cherche à montrer les effets destructurateurs des politiques néolibérales , et qui remporte un très important succès public .
Sa participation à l' émission Arrêt sur images du 23 janvier 1996 constitue un épisode à la fois marquant et révélateur du rapport que Pierre Bourdieu a pu entretenir avec les médias .
Bourdieu en était l' invité principal .
Travaillant durant ses derniers mois à la théorie des champs , il rédige un ouvrage , resté inachevé , sur le peintre Édouard Manet , en qui il voit une figure centrale de la révolution symbolique fondatrice de l' autonomie du champ artistique moderne .
Sa tombe se situe au cimetière du Père-Lachaise , à Paris , près de celles de Claude Henri de Rouvroy de Saint-Simon et de Jean Anthelme Brillat-Savarin .
Bourdieu est l' héritier de la sociologie classique , dont il a synthétisé , dans une approche profondément personnelle , la plupart des apports principaux .
Ainsi de Max Weber , il a retenu l' importance de la dimension symbolique de la légitimité de toute domination dans la vie sociale ; de même que l' idée des ordres sociaux qui deviendront , dans la théorie bourdieusienne , des champs .
De Karl Marx , il a repris le concept de capital , généralisé à toutes les activités sociales , et non plus seulement économiques .
D' Émile Durkheim , enfin , il hérite un certain style déterministe et , en un sens , à travers Marcel Mauss et Claude Lévi-Strauss , structuraliste .
Il ne faut pas , toutefois , négliger les influences philosophiques chez ce philosophe de formation : Maurice Merleau-Ponty et , à travers celui-ci , la phénoménologie de Husserl ont joué un rôle essentiel dans la réflexion de Bourdieu sur le corps propre , les dispositions à l' action , le sens pratique , l' activité athéorique : c' est-à-dire dans la définition du concept central d' habitus .
Enfin , Bourdieu a placé , à la fin de sa vie , sa sociologie sous le signe de Pascal : " J' avais pris l' habitude , depuis longtemps , lorsqu' on me posait la question , généralement mal intentionnée , de mes rapports avec Marx , de répondre qu' à tout prendre , et s' il fallait à tout prix s' affilier , je me dirais plutôt pascalien [ … ] . "
L' œuvre de Pierre Bourdieu est construite sur la volonté affichée de dépasser une série d' oppositions qui structurent les sciences sociales ( subjectivisme/objectivisme , micro/macro , liberté/déterminisme ) , notamment par des innovations conceptuelles .
Bourdieu veut ainsi souligner que , pour lui , le monde social est constitué de structures qui sont certes construites par les agents sociaux , selon la position constructiviste , mais qui , une fois constituées , conditionnent à leur tour l' action de ces agents , selon la position structuraliste .
On rejoint ici , par d' autres termes , ce que la sociologie anglo-saxonne appelle l' opposition " structure/agency " ( agent déterminé entièrement par des structures le dépassant/acteur créateur libre et rationnel des activités sociales ) dont la volonté de dépassement caractérise particulièrement le travail conceptuel de Bourdieu .
Riche de plus de trente livres et de centaines d' articles , l' œuvre de Bourdieu aborde un nombre très important d' objets empiriques .
Par le concept d' habitus , Bourdieu vise à penser le lien entre socialisation et actions des individus .
Bourdieu pense que ces dispositions sont à l' origine des pratiques futures des individus .
Bourdieu définit ainsi l' habitus comme des " structures structurées prédisposées à fonctionner comme structures structurantes " .
Pour penser cette durabilité des dispositions , Bourdieu introduit le concept d' hystérésis de l' habitus .
Un exemple , que Bourdieu emprunte à Marx , bien que se référant à un personnage de roman , permet d' illustrer ce phénomène : celui de Don Quichotte .
Bourdieu veut dire par là que des dispositions acquises dans une certaine activité sociale , par exemple au sein de la famille , sont transposées dans une autre activité , par exemple le monde professionnel .
Bourdieu retrouve cette insistance sur l' utilité dans le type de vêtements portés par les ouvriers , qui sont avant tout fonctionnels .
Pour Bourdieu , le style de vie des ouvriers se fonde ainsi , fondamentalement , sur le privilège accordé à la substance plutôt qu' à la forme dans l' ensemble des pratiques sociales .
Bourdieu voit dans ce style de vie l' effet des dispositions de l' habitus des ouvriers , qui sont elles-mêmes le produit de leur mode de vie .
Bourdieu , dans de très nombreux textes , entend souligner le caractère " générateur " de l' habitus .
Ce caractère " générateur " de l' habitus est , enfin , lié à une dernière propriété de l' habitus : celle d' être au principe de ce que Bourdieu nomme le " sens pratique " .
Bourdieu veut dire par là que l' habitus étant le reflet d' un monde social , il lui est adapté et permet aux agents , sans que ceux-ci aient besoin d' entreprendre une réflexion " tactique " consciente , de répondre immédiatement et sans même y réfléchir aux évènements auxquels ils font face .
Avec sa théorie du sens pratique , Bourdieu semble retrouver en apparence la théorie de l' acteur rationnel , dominante en économie , en ce qu' il insiste sur le fait que l' habitus est au principe de stratégies par lesquelles les agents accomplissent la recherche d' un intérêt .
La différence est pourtant profonde : Bourdieu veut , au contraire , montrer que les agents ne calculent pas en permanence , en cherchant intentionnellement à maximiser leur intérêt selon des critères rationnels explicites .
Comme Bourdieu l' écrit , " l' habitus enferme la solution des paradoxes du sens objectif sans intention subjective : il est au principe de ces enchaînements de coups qui sont objectivement organisés comme des stratégies sans être le produit d' une véritable intention stratégique . "
Bourdieu prolonge sa critique en refusant l' utilitarisme de la théorie de l' acteur rationnel : l' intérêt ne se résume pas , pour Bourdieu , à un intérêt matériel .
Bourdieu a ainsi proposé de substituer au terme d' intérêt celui d' illusio .
Par ce mot , Bourdieu entend en effet souligner qu' il n' est pas d' intérêt qui ne soit une croyance , une illusion : celle de croire qu' un enjeu social spécifique a une importance telle qu' il faille le poursuivre .
Face au structuralisme , Bourdieu a voulu redonner une capacité d' action autonome au sujet , sans toutefois lui accorder la liberté que lui prêtait l' existentialisme .
La " solution " que propose Bourdieu est de considérer que l' agent a , lors des différents processus de socialisation qu' il a connus , en particulier sa socialisation primaire , incorporé un ensemble de principes d' action , reflets des structures objectives du monde social dans lequel il se trouve , qui sont devenus en lui , au terme de cette incorporation , des " dispositions durables et transposables " , selon l' une des définitions de l' habitus que propose Bourdieu .
C' est pour cela que Bourdieu préfère au terme d' acteur , généralement employé par ceux qui veulent souligner la capacité qu' a l' individu d' agir librement , celui d' agent , qui insiste , au contraire , sur les déterminismes auxquels est soumis l' individu .
L' action des individus est donc , au terme de la théorisation de Bourdieu , fondamentalement le produit des structures objectives du monde dans lequel ils vivent , et qui façonnent en eux un ensemble de dispositions qui vont structurer leurs façons de penser , de percevoir et d' agir .
Dès le milieu des années 1960 , Bourdieu s' intéresse au champ des études de parenté , si cher à l' anthropologie classique .
Ses travaux ethnographiques en Kabylie et , parallèlement , en Béarn ( notamment dans son village natal ) sont l' occasion alors pour lui de proposer un concept nouveau , celui de " stratégie matrimoniale " .
Bourdieu nous dit que l' individu social est un agent mû par un intérêt , personnel ou collectif ( son groupe , sa famille ) , dans un cadre élaboré par l' habitus qui est le sien .
C' est le concept de " stratégie matrimoniale " qui complexifie et affine notre regard sur des situations jusqu' ici peu expliquées , par exemple le fait , en Béarn , de confier à une fille plutôt qu' à un garçon la transmission du patrimoine familial pour éviter de le voir morcelé .
Finalement , en étudiant justement ces situations particulières ( le droit d' aînesse , le primat de la masculinité dans les affaires de succession , la question du mariage du cadet ) , Bourdieu nous montre un modèle d' analyse où le mariage ( l' alliance ) et la succession ( la filiation ) sont avant tout une somme de pratiques dont le sens est construit par l' utilisation réfléchie de chacun .
Pierre Bourdieu définit la société comme une imbrication de champs : champs économique , culturel , artistique , sportif , religieux , etc .
Les interactions se structurent donc en fonction des atouts et des ressources que chacun des agents mobilise , c' est-à-dire , pour reprendre les catégories construites par Bourdieu , de son capital , qu' il soit économique , culturel , social ou symbolique .
Bourdieu refuse cette théorie de l' espace social .
Il pense , en effet , à la suite de Max Weber que les sociétés ne se structurent pas seulement à partir de logiques économiques .
Bourdieu propose ainsi d' ajouter au capital économique , ce qu' il nomme , par analogie , le capital culturel .
Par exemple , la position sociale d' un individu est , pour Bourdieu , tout autant déterminée par le diplôme dont il dispose que par la richesse économique dont il a pu hériter .
Bourdieu construit ainsi une théorie à deux dimensions de l' espace social , qui s' oppose à la théorie unidimensionnelle des marxistes .
Par exemple , parmi les individus dotés d' une grande quantité de capitaux , et qui forment la classe dominante d' une société , Bourdieu oppose ceux qui ont beaucoup de capital économique et moins de capital culturel ( la bourgeoisie industrielle pour l' essentiel ) , situés en haut à droite du schéma ci-dessous , aux individus qui ont beaucoup de capital culturel mais moins de capital économique , situés en haut à gauche du schéma ( les professeurs d' université , par exemple ) .
Bourdieu insiste sur le fait que sa vision de l' espace social est relationnelle : la position de chacun n' existe pas en soi , mais en comparaison des quantités de capital que possèdent les autres agents .
D' autre part , si Bourdieu pense que capital culturel et capital économique sont les deux types de ressources qui structurent le plus en profondeur les sociétés contemporaines , il laisse la place à tout autre type de ressources , qui peuvent , en fonction de chaque société particulière , occuper une place déterminante dans la constitution des hiérarchies sociales .
Bourdieu , à partir de cette théorie de la hiérarchisation de la société , cherche à comprendre comment se construisent les groupes sociaux .
À la différence des marxistes , Bourdieu ne croit pas que les classes sociales existent , en soi , objectivement , conformément à la position dite " réaliste " .
Toutefois , Bourdieu ne pense pas non plus que les classes sociales n' ont aucune réalité , qu' elles ne sont qu' un regroupement arbitraire d' individus , à la façon de la position " nominaliste " .
Bourdieu pense qu' une partie essentielle du travail politique consiste à mobiliser les agents sociaux , à les regrouper symboliquement , afin de créer ce sentiment d' appartenance , et de constituer ainsi des classes sociales " mobilisées " .
Pour Bourdieu , les styles de vie des individus sont le reflet de leur position sociale .
Ainsi , Bourdieu s' efforce de faire apparaître une forte corrélation entre les manières de vivre , sentir et agir des individus , leurs goûts et leurs dégoûts en particulier , et la place qu' ils occupent dans les hiérarchies sociales .
Toutefois , Bourdieu pense que dans cet espace des styles de vie se joue un aspect essentiel de la légitimation de l' ordre social .
Pierre Bourdieu pense ainsi qu' une partie de la lutte entre groupes sociaux prend la forme d' une lutte symbolique .
C' est cette dialectique de la divulgation , de l' imitation et de la recherche de la distinction qui est , pour Bourdieu , à l' origine de la transformation des pratiques culturelles .
On retrouve ici l' idée fondamentale de Bourdieu sur l' espace social : celui-ci est relationnel .
La reproduction de l' ordre social passe , pour Bourdieu , à la fois par la reproduction des hiérarchies sociales et par une légitimation de cette reproduction .
Bourdieu pense que le système d' enseignement joue un rôle important dans cette reproduction , au sein des sociétés contemporaines .
Bourdieu élabore ainsi une théorie du système d' enseignement qui vise à montrer :
Bourdieu croit tout d' abord constater que le système éducatif transmet des savoirs qui sont proches de ceux qui existent dans la classe dominante .
Cela , pour Bourdieu , permet la légitimation de la reproduction sociale .
Autrement dit , pour Bourdieu , en masquant le fait que les membres de la classe dominante réussissent à l' école en raison de la proximité entre leur culture et celle du système éducatif , l' école rend possible la légitimation de la reproduction sociale .
Ce processus de légitimation est , pour Bourdieu , entretenu par deux croyances fondamentales .
Cette " idéologie du don " joue , pour Bourdieu , un rôle déterminant dans l' acceptation par les individus de leur destin scolaire et du destin social qui en découle .
Bourdieu met en avant l' emprise de plus en plus grande de ce qu' il nomme le " mode de reproduction à composante scolaire " , qui fait du diplôme un véritable " droit d' entrée " dans les entreprises bureaucratiques modernes , même pour la bourgeoisie industrielle qui s' en est longtemps passé pour transmettre ses positions sociales .
Ainsi , Bourdieu s' efforce de montrer que les grandes écoles traditionnelles , où les compétences scolaires traditionnelles dominent , sont aujourd'hui concurrencées par de nouvelles écoles , proche du pôle dominant du champ du pouvoir .
L' École normale supérieure a ainsi perdu sa place dominante au profit de l' ENA .
Pierre Bourdieu a , à partir de son appareil conceptuel , abordé l' étude de nombreux sous-champs de la sociologie , comme la sociologie du sport , la sociologie politique , la sociologie religieuse , etc .
Au cours des années 1990 , Pierre Bourdieu s' est intéressé de plus près aux médias .
A l' issue des grèves de 1995-1996 , Pierre Bourdieu sera invité par Daniel Schneidermann dans l' émission télévisée Arrêt sur images aux côtés des journalistes Jean-Marie Cavada et Guillaume Durand face auxquels il se propose de critiquer le système télévisuel aux travers d' extraits de leurs émissions .
Se reconnaissant lui-même pris au piège par la mécanique de ce médium , il reviendra sur son propre passage télévisé dans un article polémique duquel s' ensuivra un échange houleux avec l' animateur , Daniel Schneidermann .
L' œuvre de Pierre Bourdieu a été l' objet d' une attention critique toute particulière , à la mesure de son influence dans les sciences sociales .
Une critique domine , toutefois : celle-ci porte sur la nature des déterminations sociales dans la théorie de Pierre Bourdieu , qui sont décrites comme rigides et simplificatrices ( critique du " déterminisme " ) .
Au concept d' habitus , on a pu ainsi reprocher de poser à nouveau les problèmes qu' il entendait résoudre : entre le déterminisme absolu des structuralistes ( où le sujet est soumis à des règles ) et la liberté sans limite des existentialistes , le concept d' habitus , pensé par Bourdieu pour dépasser cette opposition , n' y parvient sans doute qu' incomplètement , penchant vers une certaine forme de déterminisme .
On peut toutefois objecter ici que cette critique ignore ce que Pierre Bourdieu a rappelé dans de nombreux ouvrages , à savoir que l ' habitus est un principe puissamment générateur et d' invention .
Le philosophe Jacques Bouveresse rappelle que " Bourdieu a été accusé régulièrement de proposer des analyses du monde social qui ne peuvent conduire qu' au nihilisme et à un sentiment d' impuissance plus ou moins radicale " mais souligne " qu' il cherchait [ ... ] exactement le contraire de cela : une forme d' idéalisme réaliste , appuyé sur la connaissance , plutôt que sur les désirs , les rêves , les grandes idées et les bonnes intentions " .
Pierre Bourdieu est également critiqué pour son emploi d' un important jargon et de néologismes derrière lesquels se trouvent des idées que certains de ses critiques considèrent comme simples [ réf. insuffisante ] .
