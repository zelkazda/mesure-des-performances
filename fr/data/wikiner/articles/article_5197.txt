Descendant de Tamerlan par Miran Shah et de Gengis khan par sa mère , il naît le 14 février 1483 à Andijan .
En 1497 , il attaque et prend Samarkand , sur laquelle il pense avoir un droit légitime héréditaire .
En route pour le reconquérir , ses troupes l' abandonnent et il reperd Samarkand .
Pendant trois années , il erre , tentant en vain de récupérer ses possessions perdues , puis en 1504 , rassemblant quelques troupes fidèles , il traverse l' Hindū-Kūsh enneigé , prend la ville forte de Kaboul et se retrouve à la tête d' un riche royaume .
Mais en 1512 il est à nouveau défait par les Uzbek et retourne difficilement à Kaboul en 1514 .
Il rassemble ses forces , 12 000 hommes et quelques pièces d' artillerie et marche sur l' Inde .
Il passe la fin de sa vie à organiser son nouvel empire et à embellir Âgrâ , sa capitale .
En octobre 1530 , son fils aîné et préféré Humâyûn tombe malade .
Alors que tous les médecins s' accordent à annoncer sa mort prochaine , c' est Babur qui meurt car à l' annonce de la maladie de son fils , Babur est anéanti .
Il décède le 26 décembre 1530 durant sa quarante-huitième année et est enterré à Kaboul .
Humâyûn lui succède alors .
Il naquit à Andidjan le 14 février 1483 .
De son père , dont il hérita une bonne partie de la personnalité , Babur nous a laissé ce portrait émouvant de précision :
Il passait une partie de son temps à lire et à méditer le Coran .
Ses lectures favorites était le khamsa , les mesnevi , les livres d' histoire et surtout le Shâh Nâmeh .
Par ses origines autant que par son milieu , Babur était donc un authentique Timouride .
Sa ville natale , bien que de taille provinciale , subissait le rayonnement de la prestigieuse Samarcande qu' il rêva de conquérir sans succès pendant toute sa vie .
