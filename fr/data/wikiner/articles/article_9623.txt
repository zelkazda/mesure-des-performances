Hero est un film de sabre chinois réalisé par Zhang Yimou , sorti en 2002 .
Bien que le nom du roi ne soit jamais mentionné dans le film , on peut l' identifier par son rêve d' unifier la Chine , ce que ses assassins le croient capable de faire : il s' agit probablement du roi Ying Zheng ( -259 -- -210 , règne -246 -- -210 ) .
C' est ce qui donna son nom au pays dans les langues occidentales , bien qu' il s' appelle Zhongguo , l' Empire du milieu , en mandarin .
