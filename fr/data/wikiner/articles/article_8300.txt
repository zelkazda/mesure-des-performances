Bernard Lazare est né à Nîmes le 15 juin 1865 .
Il ne s' en préoccupe pas moins de cette question juive , dont Édouard Drumont fait désormais son fonds de commerce .
Dès 1892 , il est en contact avec Ahad HaAm , l' un des pères du mouvement des Amants de Sion .
Cette parution a lieu à quelques mois de l' arrestation et de la détention d' un capitaine juif , Alfred Dreyfus , accusé de trahison .
Connu pour sa combativité et son courage , Bernard Lazare est contacté par Mathieu Dreyfus pour contribuer à faire éclater l' innocence de son frère Alfred .
Cette tactique est sans doute plus conforme aux désirs de la famille Dreyfus .
qu' il donnera , un peu plus de deux ans plus tard , à Émile Zola qui la fera passer à la postérité " .
Il fera un bout de chemin avec Theodor Herzl , les deux hommes éprouvant l' un pour l' autre une grande estime .
Mais il se séparera de Herzl , en désaccord avec un projet dont il désapprouve " les tendances , les procédés et les actes " .
Il part aussi pour la Russie , où il fait un nouveau reportage sur les Juifs là aussi en danger .
Bernard Lazare ne cherchait pas à plaire .
