Une image de ce type très répandue dans les années 1960 était un portrait de Brigitte Bardot .
À partir des années 1980 , certains artistes ont utilisé des jeux de caractères étendus tels que la page de code 437 , disponible en mode texte sur compatible PC .
Les lecteurs MPlayer et VLC media player permettent même de lire une vidéo en ASCII , dans une interface texte , que ce soit en couleur ou en noir et blanc .
