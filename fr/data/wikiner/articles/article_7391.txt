Avant le divorce de ses parents à 4 ans , ils vivaient dans les Alpes-de-Haute-Provence .
Après le divorce , sa mère , son beau-père et lui-même s' installent à Marseille .
Sa carrière de chanteur débute à l' âge de 16 ans , après qu' un de ses professeurs lui ait fait découvrir son goût pour l' écriture : il cesse alors de jouer avec des amateurs dont les seules chansons sont des reprises de groupes de rock célèbres tels que Pink Floyd , U2 ou Led Zeppelin : il veut faire connaître les chansons qu' il a écrites , pensant qu' elles peuvent conquérir un public nouveau .
Tout juste titulaire du baccalauréat , il se rend à Paris pour tenter de se faire connaître .
Il y enchaîne les petits boulots , avant de rencontrer William Sheller , grâce à qui il signe son premier contrat avec le label Island ( Universal Music ) , en 1999 .
Il est également contacté par Brian de Palma pour participer à la création de la bande originale du film Femme fatale , qui sort en salles en avril 2002 .
Le 31 août 2004 , ayant quitté Island pour Barclay , un autre label Universal Music , il sort son troisième album , Debbie , clairement orienté rock dans son instrumentation , et dont les textes , dans l' ensemble moins engagés qu' auparavant , sont à la fois plus poétiques et plus crus .
Dans cet album , Saez prend plaisir à jouer sur les doubles sens et les sens cachés .
Selon Damien , toutes les chansons s' adressent directement à l' auditeur de par la présence de la deuxième personne du singulier ( " tu " ) dans chacune d' elles .
En juin 2005 , suite à la tournée électrique de son dernier album , il quitte Universal Music ( pour qui il devait encore un album ) et entame sa première grande tournée acoustique : il s' y produit seul sur scène la plupart du temps , entouré de plusieurs guitares , d' un piano et d' un clavier .
Fin 2006 et début 2007 , Damien met en libre écoute sur son MySpace nouvellement créé quatre mixs provisoires de rock en anglais qui seront ensuite retirés dans le courant de l' année 2007 .
Cet album , édité par le label indépendant Cinq7 , sort le 21 avril 2008 dans deux éditions distinctes : l' édition triple complète intitulée Varsovie -- L' Alhambra -- Paris et une édition simple ne comprenant que Paris .
Quant au troisième disque , Paris , il se rapproche davantage de la variété avec une instrumentation plus poussée et mélodieuse , et est plus accessible à l' écoute , d' où le fait qu' il puisse être acheté séparément .
A la mi-juillet 2010 , il annonce d' ailleurs que l' album " J' accuse " serait le premier volet d' une trilogie d' album ... deux autres albums du style du dernier opus devraient donc voir le jour prochainement .
Dès le premier album Damien Saez fait part dans ses textes de son engagement altermondialiste .
Il écrit d' ailleurs dans une lettre postée sur son site à la suite de la censure de l' affiche de son dernier album " J' accuse " : " J' ai honte pour ces gens , honte pour mon pays , honte pour ce qu' il est devenu , honte pour cette auto-censure que la société s' inflige à chaque fois qu' elle ouvre sa bouche " .
