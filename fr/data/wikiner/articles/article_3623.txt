Conformément à la constitution de 1853 , révisée en 1994 , l' Argentine est une république fédérale organisée en 23 provinces et une cité autonome érigée en district fédéral : Buenos Aires .
Hormis la capitale qui a un statut spécial , l' Argentine est divisée en régions , elles-mêmes divisées en provinces .
Il fut formé en 1900 et couvrait alors la totalité de la Puna du nord-ouest du pays , mais , en raison d' un développement et d' une population très faibles , il fut dissous en 1943 , les territoires étant alors incorporés aux provinces de Jujuy , Salta et Catamarca .
