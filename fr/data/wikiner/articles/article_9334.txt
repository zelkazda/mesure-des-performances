Le langage VHDL a été commandé par le Département de la Défense des États-Unis dans le cadre de l' initiative VHSIC .
Dans un effort de rationalisation , le VHDL reprend la même syntaxe que celle utilisée par le langage Ada ( ce dernier étant aussi développé par le département de la défense ) .
Le langage Verilog , bien que très différent du point de vue syntaxique , répondait à des besoins similaires .
La syntaxe du VHDL est tirée du langage Ada , dont les mots clefs ont été adaptés à la conception matérielle .
Les bibliothèques std_logic_arith , std_logic_signed et std_logic_unsigned , malgré leur nom , ne sont pas normalisées par l' IEEE et leur utilisation est fortement déconseillée .
