Gottfried Wilheim Leibniz en 1670 proposa le premier d' appliquer la logique modale à la morale en remarquant l' analogie suivante : " l' obligatoire ( modalité déontique ) est ce qu' il est nécessaire ( modalité aléthique ) que fasse l' homme bon .
Elle s' est développée à partir des années 50 grâce aux travaux du philosophe finlandais Georg Henrik von Wright .
Reprenant la correspondance notée par Leibniz , en les formalisant grâce aux avancées de la logique modale .
Aux axiomes de la logique propositionnelle s' ajoutent les axiomes ( distribution déontique , ou axiome de Kripke ) et ( D ) :
