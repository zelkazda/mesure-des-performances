Jean-Baptiste Rossi , plus connu sous le pseudonyme de Sébastien Japrisot , anagramme de son vrai nom , né le 4 juillet 1931 à Marseille , décédé le 4 mars 2003 à Vichy en France , est un romancier , scénariste , traducteur , réalisateur et parolier français .
Si le roman est ignoré en France , il est bien accueilli dans sa traduction aux États-Unis .
Il en ira de même pour les romans suivants comme Piège pour Cendrillon ( 1963 ) ou L' Été meurtrier ( 1978 ) .
Il travaille aussi comme scénariste sur Adieu l' ami ( 1968 ) , Le Passager de la pluie ( 1969 ) dont il publie les réécritures romanesques , ou encore La Course du lièvre à travers les champs ( 1972 ) .
Aussitôt traduit à l' étranger , le livre connaît un succès foudroyant aux États-Unis .
On lui confie par la suite en 1953 la traduction de L' Attrape-cœurs de J. D. Salinger , l' histoire d' un adolescent fragile .
Il revient huit jours plus tard avec Piège pour Cendrillon , pour toucher la même somme .
Au moment de signer le contrat , il propose Sébastien Japrisot .
Le cinéma s' en empare aussitôt : Costa-Gavras pour Compartiment tueurs ( son premier film , avec Simone Signoret et Yves Montand ) et en 1965 André Cayatte pour Piège pour Cendrillon ( sur une adaptation signée Jean Anouilh ) .
Sébastien Japrisot n' oublie pas Jean-Baptiste Rossi .
En septembre de la même année , Sébastien Japrisot donne enfin un nouveau roman , plus long que les précédents et qu' il écrit en trois semaines : La Dame dans l' auto avec des lunettes et un fusil .
Après bien des tergiversations , c' est finalement Anatole Litvak qui adapte à l' écran le roman ; et il en fait même son œuvre testament ( 1969 ) avec Samantha Eggar dans le rôle titre .
Les prétendantes étaient nombreuses : Brigitte Bardot , Michèle Mercier , Elizabeth Taylor , Julie Christie , Jane Fonda ...
Sébastien Japrisot , dans les années 70-80 , écrit directement pour le cinéma et consolide son rayonnement à l' étranger .
Le film , réalisé en 1968 avec Charles Bronson et Alain Delon , marche très fort .
Serge Silberman le pousse à écrire de nouveau pour lui .
Ainsi naît Le Passager de la pluie , mis en scène par René Clément en 1969 , avec Charles Bronson et Marlène Jobert .
Et de ce fait , La Course du lièvre à travers les champs est l' une de ses œuvres les plus personnelles , qui dégage un charme étrange et poétique .
Serge Silberman le pousse à la réalisation .
Après une " absence " de dix ans , il revient de manière éclatante à la littérature en 1977 avec L' Été meurtrier , qui obtiendra le prix des Deux-Magots en 1978 .
Le roman , puis le film réalisé en 1983 par Jean Becker ( qui n' avait pas tourné depuis seize ans ) et mettant en vedette Isabelle Adjani et Alain Souchon , connaîtront le succès que l' on sait .
Dans le film , Adjani est plus belle et plus provocante que jamais .
Dans cette cérémonie en images d' une vengeance patiente , Adjani incarne la jeune séductrice prête à tout pour dénouer à sa façon une tragédie du passé .
Un long dimanche de fiançailles raconte une histoire d' amour , celle vécue par une jeune fille meurtrie dans sa chair ( elle est handicapée ) et dans son cœur ( elle ne croit pas en la mort de son fiancé parti à la guerre ) .
Les Enfants du Marais ( 1998 ) , tendre chronique de l' entre-deux-guerres dédiée aux petites gens et à la nostalgie d' un bonheur simple comme un rayon de soleil ou un verre de vin .
Toutes les livres de Sébastien Japrisot racontent des histoires passionnelles mêlées à une intrigue criminelle savante .
Sébastien Japrisot vivait à son humeur , alternant littérature et cinéma , avec la chance de pouvoir s' octroyer , parfois , quelques années de silence méritées .
Jean-Jacques Aillagon , Ministre de la Culture et de la Communication , communiqué du 6 mars 2003 :
Milan , coll .
Sébastien Japrisot est un merveilleux raconteur d' histoires , un de ces romanciers désormais rares qui cultive l' art complexe des magiciens .
Sébastien Japrisot occupe une place singulière dans le roman français contemporain .
Surtout , et ce n' est pas sans moindre mérite , s' il ne s' est pas cantonné au roman policier , Sébastien Japrisot a largement contribué à abolir , par la qualité de son écriture et l' originalité de son écriture , la frontière entre littérature policière et littérature tout court .
