Louis de Funès est un des acteurs comiques les plus célèbres du cinéma français d' après-guerre .
Son père , qui ne peut plus exercer sa profession d' avocat ( comme il le faisait en Espagne ) depuis son installation en France , s' improvise diamantaire .
Dans les cours , il a notamment pour condisciple Henri Decaë , qui fut , bien plus tard , directeur de la photographie sur plusieurs de ses films .
Louis de Funès est alors un excellent musicien , selon Eddy Barclay , car il a l' oreille musicale -- ce dont il se servira dans certains de ses films , tels que La Rue Sans Loi , Frou-Frou , Le Corniaud , La Grande Vadrouille , Le Grand Restaurant ou encore L' Homme orchestre -- et possède une bonne connaissance du cinéma de son époque [ réf. nécessaire ] .
À côté de quelques petites figurations théâtrales , l' acteur se démène pour gagner sa vie grâce à ses activités de pianiste , donnant parfois des cours le jour puis jouant la nuit à travers le Paris nocturne .
En 1944 , il a un deuxième fils , Patrick , et , en 1949 , un troisième fils , Olivier , qui tiendra quelques rôles au côté de son père , au cinéma comme au théâtre .
En 1945 , toujours grâce à Daniel Gélin , il débute au cinéma dans La Tentation de Barbizon , de Jean Stelli .
Au début des années 1950 , Sacha Guitry lui confie plusieurs petits rôles , notamment dans La Poison ( 1951 ) , Je l' ai été trois fois ( 1952 ) , Si Paris nous était conté ( 1955 ) et surtout La Vie d' un honnête homme ( 1953 ) où il a un rôle un peu plus consistant de valet de chambre " obséquieux et fourbe , presque inquiétant l' espace d' un plan " .
Dans ce film , son personnage s' affine un peu plus -- " il apparait " au naturel " , sans grimace ni moustache " -- et il est associé pour la première fois à Claude Gensac .
On le retrouve l' année suivante dans l' adaptation à l' écran du spectacle , Ah ! Les belles bacchantes de Jean Loubignac , qui est son premier film en couleurs .
Cette même année , il joue face à Fernandel dans Le Mouton à cinq pattes d' Henri Verneuil et pour la première fois face à Bourvil dans Poisson d' avril de Gilles Grangier .
Il s' impose avec force face à Jean Gabin et Bourvil , dans une prestation de quelques minutes au cours de laquelle il dessine en quelque sorte son futur personnage : lâche devant " le fort " ( Jean Gabin ) et colérique devant " le faible " ( Bourvil ) .
Dès l' année suivante , Maurice Regamey lui offre son premier rôle principal dans Comme un cheveu sur la soupe .
On le retrouve encore dans un rôle principal en 1958 dans Taxi , Roulotte et Corrida , d' André Hunebelle , tourné en Espagne , qui connaît un certain succès .
C' est d' abord au théâtre que la carrière de Louis de Funès va connaître une nouvelle accélération .
Depuis ses débuts , l' acteur ne s' est jamais éloigné des planches et il reprend notamment , en 1957 , aux côtés de Danielle Darrieux et Robert Lamoureux , le rôle créé par Raimu dans Faisons un rêve de Sacha Guitry .
Le succès est tel qu' on lui propose de reprendre la pièce à Paris en janvier 1961 .
On le retrouve par exemple en 1961 dans un petit rôle d' un barman dans Le crime ne paie pas , le troisième film réalisé par Gérard Oury .
Lors du tournage , alors qu' il tient le seul rôle comique du film , Louis de Funès essaie de convaincre le réalisateur qu' il est fait pour tourner des films comiques : " Quant à toi , tu es un auteur comique , et tu ne parviendras à t'exprimer vraiment que lorsque tu auras admis cette vérité là " .
Louis de Funès avait participé à la création de la pièce en 1952 -- il tenait le rôle du maître d' hôtel incarné par Christian Marin dans le film -- mais la pièce n' avait pas connu le succès .
Finalement , malgré cet insuccès et les difficultés rencontrées par le réalisateur auprès des producteurs pour monter le projet autour de Louis de Funès , ce film permet à l' acteur de retrouver un large public et marque le départ de la seconde partie de sa carrière qui ne verra plus sa popularité fléchir .
Pouic-Pouic marque aussi le début de la collaboration entre Louis de Funès et Jean Girault , lui aussi musicien , qui produisit douze films qui rencontreront le plus souvent un très large public .
À peine deux mois plus tard , il triomphe à nouveau dans le rôle d' un représentant de l' ordre dans Fantômas .
Dans ce film construit sur la double composition de Jean Marais , il transcende son rôle de contrepoint comique et parvient à éclipser ses partenaires .
Pendant que les succès populaires s' accumulent , en 1965 , il tourne Le Corniaud , de Gérard Oury , où il partage l' affiche avec Bourvil .
En 1966 , La Grande Vadrouille , de nouveau avec Bourvil et réalisé par Gérard Oury , connaît un succès colossal : le film a en effet détenu longtemps le record du plus grand nombre de places de cinéma vendues en France ( 17 millions ) .
Il n' a été détrôné qu' en 1998 , par le film Titanic de James Cameron mais il demeura le film français ayant obtenu le plus grand nombre d' entrées en salle pendant plus de 40 ans .
Il fut depuis peu devancé par Bienvenue chez les ch'tis .
La Folie des grandeurs de Gérard Oury doit marquer les retrouvailles de Louis de Funès et de Bourvil mais la mort de ce dernier faillit interrompre le projet .
À partir de mars 1973 , il s' investit énormément dans le tournage des Aventures de Rabbi Jacob qui sort le 18 octobre de la même année .
Le lendemain , Louis de Funès est à nouveau sur les planches à la Comédie des Champs-Élysées , pour ce qui fut sa dernière apparition au théâtre .
À partir de là , il se repose au château de Clermont , situé au Cellier en Loire-Atlantique ; il jardine beaucoup et refuse d' entreprendre quoi que ce soit en prévision du tournage très physique du prochain film de Gérard Oury .
Lorsque le film sort le 27 octobre 1976 , le public français retrouve l' acteur amaigri à l' écran et plébiscite son retour -- presque six millions d' entrées -- aux côtés de Coluche .
Louis de Funès réapparait donc à l' écran , mais son médecin est toujours sur le plateau , ainsi qu' une ambulance .
En 1980 , il réalise un vieux rêve : adapter au cinéma une pièce de Molière et en réaliser une version à son image .
C' est ainsi que L' Avare arrive sur les écrans de cinéma , mais ne rencontre qu' un modeste succès .
Le Gendarme et les Gendarmettes est son dernier film .
Il est enterré au cimetière du Cellier le 29 janvier 1983 .
Les capacités de Louis de Funès à mimer et à faire des grimaces sont les principaux aspects de son humour .
Louis de Funès disait que rien ne le faisait plus rire , dans la vie courante , qu' une personne en engueulant une autre , sans que cette dernière puisse répliquer .
Sa petite taille ( 1,64 m ) contrastait avec celle de ses partenaires plus grands ( Bourvil , Yves Montand ) et ajoutait un autre élément comique au personnage .
Même s' il n' a pas souvent eu l' occasion d' y recourir dans les nombreux films auxquels il a participé , Louis de Funès portait volontiers des déguisements pour accentuer , parfois jusqu' à l' outrance , les situations comiques dans lesquelles il faisait évoluer ses personnages .
Le talent de Louis de Funès fonctionnait bien dans le cadre de duos réguliers ou occasionnels avec des acteurs très divers .
Claude Gensac , connue pour le surnom que Cruchot lui donne dans la série des Gendarmes : " Ma biche " , fut la complice féminine des personnages de Louis de Funès ; elle a souvent joué sa femme à l' écran .
Plusieurs scènes de La Folie des grandeurs sont restées célèbres , comme le réveil avec les rimes en " or " ou le nettoyage des oreilles , et font tout de suite penser à Yves Montand .
Louis de Funès a aussi joué de célèbres scènes avec Coluche dans L' Aile ou la Cuisse .
Mais son duo le plus marquant est celui formé avec Bourvil dans Le Corniaud et surtout dans La Grande Vadrouille .
Selon Colette Brosset , Louis de Funès avait la musique et la danse dans la peau .
Ses arabesques font merveille dans les films comme Ah ! Les belles bacchantes , Le Grand Restaurant , L' Homme orchestre ou Les Aventures de Rabbi Jacob .
( mais aussi de fables , de pièces de théâtre -classiques , vaudevilles- , ou encore d' histoires pour enfants ( Les Aristochats ) , le tout gravé sur microsillons )
De Funès déclarait " Ma passion , c' est ma campagne , à 400 kilomètres de Paris ! "
Bien avant que ce ne soit une mode , Louis de Funès était un passionné d' écologie .
Loin de ses tracas parisiens , le comédien aimait s' isoler dans son château du Cellier , en Loire-Atlantique , qui devint sa résidence principale en 1975 .
A un journaliste lui demandant s' il se sentait châtelain , Louis de Funès répondait " Je me sens avant tout propriétaire d' un grand jardin dans lequel je peux faire pousser des fruits et des légumes en y apportant tout l' art et toute la science dont je suis capable . "
Louis de Funès , comparé aux autres artistes de son époque et au nombre de films qu' il a tournés , n' a pas reçu un nombre très important de récompenses .
Les films dans lesquels a joué Louis de Funès ont attiré plus de 160 millions de spectateurs en France .
La Grande Vadrouille est encore n°2 aux box-office français : c' est le 3 e film français qui a cumulé le plus d' entrées en France dans l' histoire du cinéma ( après Titanic de James Cameron et Bienvenue chez les Ch'tis de Dany Boon ) .
Michel Galabru , Elisabeth Wiener et Pierre Tornade participaient aussi à cette vision touchante de la nativité , revue et corrigée .
Une adaptation à la télévision en a été faite , avec Michel Serrault .
