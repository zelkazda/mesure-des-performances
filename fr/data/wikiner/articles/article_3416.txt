Il est député de la première circonscription de Loir-et-Cher depuis juin 2002 .
Il est membre du Nouveau Centre .
Il a été maire de la ville de Blois de 2001 à 2008 .
Il commence par travailler comme consultant en entreprise , chez France Télécom puis dans l' entreprise de son beau-père .
Il remporte son premier mandat à 34 ans quand il est élu conseiller général en 2000 ( Canton de Blois-3 ) avec 3 voix d' avance , lors d' une élection partielle .
Un an plus tard , lors des municipales il bat avec 37 voix d' avance et à la surprise générale le sortant et candidat du PS Jack Lang .
Cette victoire peu prévisible lui valut une notoriété médiatique , la presse le surnommant " le tombeur de Jack Lang " .
Parallèlement , Nicolas Perruchot mène d' importants travaux dans le centre-ville dans le but de rendre le centre historique piétonnier ou semi piétonnier , dynamiser le commerce de proximité et remettre sur le marché les logements vacants ( lancement d' une OPAH ) pour qu' à long terme le centre-ville soit le troisième centre commercial de l' agglomération .
En tant que maire et président de la communauté d' agglomération de Blois , Nicolas Perruchot a entrepris d' importants travaux d' urbanisme , notamment dans la ZUP des quartiers nord de la ville à travers un plan de rénovation urbaine d' un montant de 257454946 € .
En 2007 , après avoir soutenu François Bayrou au premier tour de l' élection présidentielle , il appelle à voter pour Nicolas Sarkozy au second tour .
Il participe à la création du Nouveau Centre , qui constitue le pôle centriste de la majorité présidentielle .
Toutefois il est désavoué des blésois en n' obtenant que 45.7 % des suffrages exprimés sur la seule ville de Blois .
Il a été désigné porte-parole du Nouveau Centre au sein de son organisation provisoire .
Nicolas Perruchot a dénoncé lors d' un point-presse ( début novembre 2007 ) " un calendrier parlementaire trop chargé " et " un manque de cohérence et de lisibilité dans l' action du gouvernement " .
Nicolas Perruchot a été battu sur la ville de Blois lors des élections municipales de 2008 .
