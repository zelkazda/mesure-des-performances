Jacques Champion de Chambonnières est un compositeur et claveciniste français ( vers 1601 -- 1672 ) .
C' est lui qui découvrit le talent de Louis Couperin et le fit venir à Paris .
Sa charge de claveciniste du roi , refusée par Louis Couperin en égard à son bienfaiteur , fut reprise par Jean-Henri d' Anglebert , l' un de ses élèves .
