Il est généralement considéré comme le plus grand -- akbar en arabe -- Moghol .
Akbar est né dans une famille cultivée .
En 1556 , il succède à son père Humâyûn à la tête du petit royaume musulman que ce dernier a regagné à la fin de sa vie après son exil en Perse .
Dorénavant , Akbar règne en maître incontesté sur le nord de l' Inde .
Akbar agrandit son empire en faisant la conquête du Goujerat en 1573 , du Bengale en 1576 , du Sind en 1590 , de l' Orissa en 1592 et du Balouchîstân en 1594 .
Il se lance ensuite à la conquête du sud de l' Inde
Akbar fait preuve d' un grand talent d' administrateur et continue le travail de réorganisation commencé par Sher Shâh Sûrî , qui avait chassé son père hors de l' Inde .
Pour célébrer sa victoire sur le Goujerat , il ordonne la construction ( 1569 -- 1576 ) d' une nouvelle capitale à Fatehpur Sikri , près de Âgrâ , où il fait créer un nouveau style architectural mélangeant les influences musulmanes et hindoues .
Il s' installe à Lahore , plus près de régions instables .
Les dernières années du règne d' Akbar sont marquées par les rébellions fréquentes de son fils Salim , le futur empereur Jahângîr .
Il meurt à Âgrâ le 27 octobre 1605 de dysenterie .
Akbar est le nom du chef des troupes indiennes dans le jeu de stratégie en temps réel Age of Empires III
