La mesure du titre alcoolique à partir de la densité du liquide a été définie par Louis Joseph Gay-Lussac en 1824 , pour les besoins de la taxation des alcools .
Les tables de densité et de correction selon la température ont été établies expérimentalement par Gay-Lussac .
