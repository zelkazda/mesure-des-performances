Ballard naît en 1930 à Shanghaï .
Avec l' invasion de la Chine par le Japon , il est emprisonné en 1942 dans un camp de détention pour civils où il restera jusqu' à la fin de la Seconde Guerre mondiale .
Il a décrit cette expérience dans son livre semi-autobiographique Empire du soleil , qui a été adapté au cinéma par Steven Spielberg .
Il part en 1946 pour l' Angleterre et est choqué par la vie britannique qui lui parait détachée des réalités .
Il commence des études de médecine au King 's College puis de littérature anglaise de l' Université de Londres sans succès .
Il s' engage sur un coup de tête dans l' armée de l' air et part faire son entraînement au Canada .
Il devient peu à peu l' un des romanciers phares de la nouvelle vague de SF britannique aux côtés de Brian Aldiss , John Brunner et Christopher Priest , qui abordent de nouveaux thèmes en soignant particulièrement le style .
À la mort de sa femme en 1964 , Ballard devient écrivain professionnel , ce qui lui permet d' être présent à la maison pour s' occuper de ses enfants .
Il s' intéresse alors aux techniques d' écriture expérimentales de Williams Burroughs .
L' œuvre de Ballard est étrange et sophistiquée et a été très influente malgré son faible succès commercial .
On retrouve l' influence de Ballard dans la musique de groupes de post-punk comme Joy Division , The Normal et John Foxx .
