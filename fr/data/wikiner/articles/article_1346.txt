Dante invente le tercet dans la Divine Comédie en 1307 en instituant la " terza rima " qui prolonge le système de rime de tercet en tercet sur le modèle aba/bcb/cdc … en achevant la cascade par un vers isolé final .
Cette strophe impaire sera exploitée en tant que telle par Pétrarque qui en fera également une des composantes du sonnet .
