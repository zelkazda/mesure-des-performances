Le canton de Genève est un canton de Suisse de superficie modeste mais densément peuplé .
Il est situé à l' extrémité ouest de la Suisse et du lac Léman et entouré presque entièrement par la France .
Son chef-lieu est la ville de Genève .
Vers 400 , Genève est érigée en évêché .
En 443 , une tribu germanique , les Burgondes , se fixe dans la région .
La Réforme protestante y triomphe en 1535 .
Jean Calvin s' y installe en 1536 mais en est chassé en 1538 en raison de son intolérance , puis est rappelé en 1541 .
Sous l' égide de Calvin et de Théodore de Bèze , ils procurent à leur nouveau foyer un grand rayonnement religieux et intellectuel .
Dans la nuit du 11 au 12 décembre 1602 , le duc de Savoie tente une attaque nocturne contre Genève , mais cette tentative échoue .
La défaite savoyarde est commémorée chaque année lors de la fête de l' Escalade .
Après la révocation de l' Édit de Nantes en 1685 , les mesures prises par Louis XIV contre le protestantisme en France font affluer une deuxième grande vague de réfugiés .
Situé à l' extrémité occidentale de la Suisse , au bout du lac Léman , ce canton partage plus de 90 % de ses frontières avec la France ( 100 km contre 4 avec la Suisse ) .
Il est entouré du département de l' Ain à l' ouest , du département de la Haute-Savoie à l' est et au sud , et du canton de Vaud au nord .
On donne le nom de cuvette genevoise à cette région ciselée en trois cadrans par le lac Léman dont les pierres du Niton servent à mesurer l' altitude ( 373,6 mètres ) , le Rhône , et l' Arve provenant du massif du Mont-Blanc via la vallée Blanche .
Elle est ceinturée , sur territoire français , par le massif du Jura au nord-ouest dont le point culminant s' établit au Crêt de la Neige à 1720 mètres , là où le point le plus élevé du canton se situe aux environs de 513 mètres sur la commune de Jussy ( à Monniaz ) .
Par temps clair , on bénéficie d' un panorama allant de Fort l' Écluse , passage du Rhône creusé lors de la dernière glaciation par son glacier que l' on estimait s' étendre jusqu' aux alentours de Lyon et qui façonna les reliefs vallonnés et caillouteux de la plaine , jusqu' à Nyon dans le canton de Vaud .
C' est à Bernex , la bourgade principale , que se trouve le lieu-dit du Signal , le deuxième point le plus haut du canton avec 509,9 mètres , sur lequel on allumait jadis un feu d' alarme destiné à être vu par le plus grand nombre et que l' on relayait de sommets en sommets .
Bernex regroupe sous son administration les villages de Lully et Sézenove , et est en passe de devenir une ville avoisinant les 10000 habitants .
La région comporte principalement des exploitations agricoles mais aussi viticoles et fournit , de par les couches de sédiments déposées par le glacier du Rhône , d' importantes ressources en gravier dont les installations et les excavations défigurent quelque peu le paysage .
Évolution de la population du canton de Genève de 1999 à 2005 :
Genève est le siège international de compagnies comme Merck Serono et STMicroelectronics .
Beaucoup d' autres compagnies multinationales comme Alcon , DuPont de Nemours , Electronic Arts , Hewlett-Packard , IBM , Procter & Gamble , Thomson Reuters et Sun Microsystems y ont installé leurs sièges européens .
Firmenich et Givaudan , deux producteurs importants d' arômes et de parfums y ont leurs sièges et leurs centres de production .
Le Jeûne genevois est un jour férié propre au canton de Genève qui est célébré le jeudi suivant le premier dimanche de septembre .
Le 31 décembre est aussi un autre jour férié propre au canton de Genève .
