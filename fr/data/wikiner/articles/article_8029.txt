Dans la perspective de la conférence de Cancun , les pays les moins avancés ( PMA ) se sont réunis à Dhaka , au Bangladesh .
Ils ont dit leur déception devant le peu de progrès accomplis en ce qui concerne les engagements pris à leur égard lors des conférences de Marrakech ( 1994 ) , Singapour ( 1996 ) , Genève ( 1998 ) .
