Auderville est la commune du cap de la Hague .
Le territoire communal témoigne , par de nombreux restes de fortifications , de l' importante présence des militaires allemands pendant la Seconde Guerre mondiale .
Elle est , avec sa voisine Jobourg , la commune la plus occidentale de Basse-Normandie ( et donc de Normandie ) et seule son autre voisine Saint-Germain-des-Vaux est plus septentrionale sur la région administrative .
Auderville a compté jusqu' à 572 habitants en 1836 .
