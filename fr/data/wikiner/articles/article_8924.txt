Le ramassage des miettes a été inventé par John McCarthy comme faisant partie du premier système Lisp .
Les algorithmes traversants peuvent être modélisés à l' aide de l ' abstraction des trois couleurs , publiée par Dijkstra en 1978 .
" Il est dit que les programmeurs Lisp savent que la gestion de la mémoire est si importante qu' elle ne peut être laissée aux programmeurs , et que les programmeurs C savent que la gestion de la mémoire est si importante qu' elle ne peut être laissée au système " -- Bjarne Stroustrup peut-être tiré d' une source antérieure .
