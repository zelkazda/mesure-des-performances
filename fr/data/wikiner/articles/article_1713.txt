Fondé en 1984 par Leonard Bosack et Sandra Lerner , un couple qui travaillait au service informatique de l' université Stanford , Cisco n' a pas été la première société à créer et vendre des routeurs mais Cisco créa le premier routeur multi-protocoles permettant d' interconnecter des réseaux utilisant des protocoles de communication différents .
Cisco , dont le siège social se trouve à San José en Californie , tire son nom et son logo de la ville où elle a été fondée , San Francisco et son fameux Golden Gate Bridge .
La société est entrée en bourse ( NASDAQ ) en 1990 .
Son CEO actuel ( 2005 ) est John Chambers .
La spécificité de la gamme des produits Cisco est l' uniformité de son système d' exploitation .
En effet , la majorité des produits Cisco utilise un système d' exploitation propriétaire nommé IOS .
Cisco propose aujourd'hui une gamme importante de solutions telles que :
En 2005 , Cisco Systems adopte le nouveau standard baptisé IMS qui permettra aux opérateurs télécoms de proposer sur des mobiles 3G , des services fonctionnant aujourd'hui sur réseaux filaires .
Cisco propose également des certifications pour les professionnels du monde des réseaux .
Les défenseurs du logiciel libre , en particulier la Free Software Foundation négocient avec Cisco depuis 2003 pour qu' ils arrêtent de contrevenir à la licence GPL .
Après 5 ans de négociation et devant le manque de coopération de Cisco , la FSF a porté plainte contre l' entreprise en décembre 2008 .
