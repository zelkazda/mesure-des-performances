Ils affirment que le Jean-Marie Abgrall , de par ses propos outranciers ou diffamatoires , n' est pas un expert fiable .
En réalité ,le docteur Abgrall s' est longuement expliqué sur sa décision d' abandon de toute activité concernant les sectes , décision motivée selon lui par l' absence de cohérence gouvernementale dans la lutte anti sectes et sur l' absence de moyens réels .
Dans les années 1990 , Jean-Marie Abgrall a été cité dans plusieurs affaires concernant les jeux de rôles , ce qui l' a amené à participer à diverses émissions de télévision .
Dans ce même livre , il dénonce aussi le caractère " dangereux " des jeux de rôles , ce qui lui a valu de sévères critiques de la part de joueurs ; Jean-Marie Abgrall aurait depuis nuancé ses propos .
