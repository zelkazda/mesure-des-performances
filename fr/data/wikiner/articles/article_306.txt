Napoléon I er surnomma ainsi le château la " maison des siècles " , évoquant par là les souvenirs historiques dont les lieux sont le témoignage .
Depuis 1981 , le château fait partie avec son parc du patrimoine mondial de l' UNESCO .
Un château-fort est mentionné à cet emplacement pour la première fois en 1137 dans une charte de Louis VII le Jeune .
La date exacte de la fondation du château reste inconnue , mais le premier édifice a probablement été construit sous le règne du père de Louis VII , Louis VI , voire sous celui de son grand-père , Philippe I er , lorsqu' il réunit le Gâtinais au domaine royal français en 1068 .
À la Noël 1191 , Philippe-Auguste fête à Fontainebleau le retour de la troisième croisade .
Philippe le Bel naît au château en 1268 et fait aménager des appartements en 1286 .
En 1313 , Jeanne de Bourgogne , propriétaire du domaine de Fontainebleau , épouse Philippe de Valois , futur Philippe VI , qui y fera des séjours fréquents .
En janvier 1332 , a lieu à Fontainebleau la signature du contrat de mariage entre Jean II le Bon et Bonne de Luxembourg .
Charles VI y séjourne à partir de 1388 .
Le château est cependant abandonné en raison des affrontements de la guerre de Cent Ans , lorsque la cour s' exile au bord de la Loire et à Bourges .
Charles VII y revient après la libération de l' Île-de-France et de Paris en 1436 , privilégiant le lieu pour sa salubrité .
François I er vient chasser à Fontainebleau , accompagné de sa cour et de sa favorite , la duchesse d' Étampes , délaissant ainsi plus ou moins le château de Blois , et annonçant le retour progressif de la cour dans les environs de Paris .
Les noms des architectes du château sont , quant à eux , plus hypothétiques : Sebastiano Serlio , pour sa part , se voyait offrir le 27 décembre 1541 l' assurance de 400 livres par an pour " son état de peintre et d' architecteur au fait de ses édifices et bastiments au dit lieu de Fontainebleau " .
D' autres noms ont été avancés pour identifier l' architecte qui officia sous le règne de François I er .
La fin du règne de François I er , décédé en 1547 , aurait vu le remplacement de la chapelle .
C' est en 1539 que François I er reçoit à Fontainebleau Charles Quint et lui fait visiter son palais , entre le 24 et le 30 décembre .
Ronsard se fera l' écho du faste déployé au château par l' écriture de quelques vers :
Il nomme Philibert Delorme pour vérifier et visiter le château le 3 avril 1548 , date à laquelle la suite des travaux lui est confiée .
Deux jours après la mort d' Henri II en 1559 , Catherine de Médicis remercie Philibert Delorme , protégé de Diane de Poitiers , et confie les travaux au Primatice qui devient surintendant des maisons royales le 12 juillet 1559 .
C' est à cette époque que Niccolo dell' Abate décore le château .
À la mort de Jean Bullant en octobre 1578 , le chantier est confié par Henri III à Baptiste Androuet du Cerceau .
Pendant le règne des trois fils d' Henri II ( François II , Charles IX et Henri III ) , le château de Fontainebleau est moins habité , les monarques lui préférant le Louvre , ou encore les demeures du Val de Loire comme Amboise ou Blois .
Le château accueille entre le 14 et le 21 décembre 1599 la visite de Charles-Emmanuel de Savoie .
Louis XIII , qui hérite en 1610 d' un château encore en chantier , fait achever les travaux sans apporter de modification majeure .
C' est là que le cardinal Barberini , neveu du pape Urbain VIII , est reçu par Louis XIII au château pendant l' été 1625 ; que le maréchal d' Ornano est arrêté le 4 mai 1626 ; qu' est ratifié le traité de paix ( Traité de Fontainebleau ) entre la France et l' Angleterre le 16 septembre 1629 .
Le 25 septembre 1645 est signé à Fontainebleau le contrat de mariage entre Ladislas IV de Pologne et Marie-Louise de Gonzague-Nevers .
Louis XIV , bien que préférant les demeures situées à l' ouest de Paris et accordant toutes ses attentions au château de Versailles , aime venir à Fontainebleau .
Le Grand Condé s' éteint à son tour dans le château le 11 décembre 1686 .
Le 27 octobre 1743 , Fontainebleau est le théâtre de la signature d' un traité d' alliance secret entre la France et l' Espagne .
Le 3 novembre 1762 y est signé le traité de Fontainebleau , traité secret entre la France et l' Espagne au sujet des possessions de la Louisiane .
Le dauphin Louis , fils de Louis XV , meurt de la tuberculose au château le 20 décembre 1765 .
Napoléon I er fait revivre Fontainebleau à partir de 1804 , il le fait meubler , y tient sa cour pour laquelle il fait aménager 40 appartements de maître .
Le pape quittera Fontainebleau le 23 janvier 1814 .
Le 23 mai 1808 , le château accueille la visite de Charles IV d' Espagne et de la reine Marie-Louise .
Le futur Napoléon III est baptisé au château le 4 novembre 1810 , avec 24 autres enfants de dignitaires et généraux .
À la suite de Napoléon , les derniers monarques français y feront plusieurs séjours : le 15 juin 1816 , Marie-Caroline de Bourbon-Sicile , duchesse de Berry , est reçue au château .
Louis XVIII et Charles X y ont dormi .
Sous la monarchie de Juillet , Louis-Philippe entreprend les premiers travaux de restauration ( dirigés par Jean Alaux , Picot , et Abel de Pujol ) et fait redécorer et remeubler l' intérieur , avant que le château ne serve de cadre au mariage de Ferdinand-Philippe d' Orléans avec Hélène de Mecklembourg-Schwerin le 30 mai 1837 .
C' est en 1848 qu' Abel Blouet devient architecte du château et entreprend de nouvelles restaurations .
À sa mort en 1853 , il est remplacé par Hector Lefuel puis Alexis Paccard en 1855 .
Sous le Second Empire , Fontainebleau fait partie , avec Saint-Cloud , Compiègne et Biarritz , des lieux de villégiature de la cour .
L' impératrice Eugénie , épouse de Napoléon III , passe ses soirées dans le petit théâtre construit par son mari .
Elle s' attache au salon chinois , agrémenté par des objets provenant du sac du palais d' Été et par les cadeaux des ambassadeurs du Siam , reçus au château le 27 juin 1861 .
En janvier 1949 , une partie du château est investie par le commandement en chef des forces alliées Centre-Europe ( OTAN ) et y restera jusqu' en juillet 1966 .
Une restauration générale du château est permise par la loi-programme des années 1964 -- 1968 dont André Malraux est l' investigateur .
C' est en 1986 qu' est inauguré dans l' aile Louis XV le musée Napoléon I er .
Philibert Delorme avait présidé à la création de deux oratoires : l' un pour Henri II réalisé en 1557 , l' autre pour Diane de Poitiers .
Les dernières peintures décoratives exécutées dans la chapelle sont les tableaux ovales réalisés sous Louis XVI .
Le principal évènement qui eu lieu dans cette chapelle fut le mariage de Louis XV et Marie Leszczyńska en 1725 .
La décoration et l' ameublement furent revus notamment sous le Second Empire , mais le décor de boiseries des trois salles les plus importantes a été renouvelé dès 1644 .
Les peintures représentent des récits de la mythologie gréco-romaine et des allégories dont le sens nous échappe aujourd'hui ( Marguerite d' Angoulême , sœur de François I er , admettait elle-même la complexité des thèmes et disait " lire en hébreu " sans explication annexe ) , mais qui symbolisent probablement le bon gouvernement du roi et font l' éloge de François I er .
À ses pieds figurent trois allégories de l' air , de la terre et de l' eau , ainsi qu' une cigogne qui symboliserait l' amour filial , celle-ci représentant la mère du roi , Louise de Savoie .
Louis XVI fit dédoubler l' aile en 1786 en ajoutant des appartements , la privant ainsi de son ouverture sur le jardin de Diane , mais faisant réaliser de fausses portes-fenêtres pour garder un aspect symétrique .
François I er avait fait aménager , en 1534 , au rez-de-chaussée de l' aile qui porte aujourd'hui son nom , un ensemble composé de trois salles de bains et de quatre petits salons qui furent décorés de stucs , de grotesques et de fresques , dont certaines étaient dues au Primatice .
Les petits appartements de Napoléon I er se situent à l' emplacement des anciens bains de François I er , transformés sous Louis XV en appartements privés réservés au roi , à M me de Pompadour puis à M me Du Barry .
Ils furent aménagés pour Napoléon I er de 1808 à 1810 .
En 1565 , Catherine de Médicis fait doubler le corps de bâtiment jouxtant le jardin de Diane et multiplie ainsi le nombre d' appartements .
Situé au rez-de-chaussée de l' aile des appartements royaux , les appartements de Joséphine ont été aménagés pour elle en 1808 , à partir d' une suite de pièces aux lambris de style Louis XV .
Ils furent occupés par l' impératrice Marie-Louise à partir de 1810 .
Le mobilier de style Louis XVI , bien qu' incomplet , restitue assez fidèlement l' aspect de la pièce lors de la réalisation des décors muraux .
La soierie des murs brochée et chenillée a été retissée sur le modèle ancien exécuté à Lyon à la fin du règne de Louis XVI .
La pièce est meublée dans son état Louis XVI , cependant incomplet .
Les garnitures sont en velours blanc lamé or et en gros de Tours jaune broché or .
Le portique de Serlio donne sur la cour ovale .
Le portique a vraisemblablement été édifié en 1531 , il est donc antérieur à l' arrivée de Serlio à Fontainebleau .
Il fut déplacé par Henri IV et fut reconstruit en 1893 .
Les peintures du Primatice dont elle est décorée ont été restaurées .
Le tympan est orné de la salamandre de François I er .
Celle du premier étage , fermée par un vitrage sous Louis XIII correspond à l' appartement de Madame de Maintenon .
Sous Louis XV , les appartements sont occupés par la duchesse de Bourbon , puis par Charlotte-Aglaé d' Orléans , et enfin par le comte de la Marche et Marie Fortunée d' Este-Modène .
En 1804 , les appartements deviennent les quartiers privés de Louis Bonaparte .
En 1837 , ils sont occupés par le duc et la duchesse de Broglie , puis par le maréchal Gérard en 1839 , et Madame Adélaïde en 1845 .
La salle de bal , dite parfois " galerie Henri II " , longue de 30 m et large de 10 m , a une superficie qui dépasse 300 m 2 .
François I er puis Henri II décident de la transformer en une grande salle de réception et d' apparat pour y organiser les fêtes royales .
La conception de la salle est confiée à l' architecte Philibert Delorme .
La cour était invitée à des bals masqués extravagants : on a pu voir François I er déguisé en centaure .
En 1642 , le surintendant des bâtiments du roi Sublet des Noyers fait appel à Poussin pour savoir comment éviter des dégradations qui ruinent peu à peu le décor peint .
La porte d' entrée en pierre de taille réalisée par Philibert Delorme date du règne de Henri II et était autrefois peinte , comme le prouve un paiement fait en 1558 à deux peintres .
Construite en pierre de taille et plus dégagée qu' elle ne l' est aujourd'hui , elle apparaissait comme le pendant du portique de Serlio avec lequel elle partageait de nombreux traits " français " : arcs en anse-de-panier , chapiteaux de fantaisie , ici avec le cerf bellifontain .
En 1612 , une commande passée à Ambroise Dubois prévoyait l' exécution de six grandes toiles pour couvrir les fenêtres aveuglées .
Située sous la salle de bal , la salle des colonnes a été amménagée par Louis-Philippe .
Le jeu de paume de Fontainebleau fut surtout utilisé par le roi Henri IV .
Utilisée comme salle des banquets par Louis-Philippe , elle est transformée en bibliothèque sous le Second Empire , en 1858 .
Ses principaux conservateurs au XIX e siècle furent entre autres Auguste Barbier , Vatout , Jacques-Joseph Champollion , Octave Feuillet et Jean-Jacques Weiss .
La galerie conserve également les fontes d' origines des copies de statues antiques exécutées par le Primatice en 1540 .
Située à l' emplacement d' une ancienne porte aux bossages rustiques en grès construite en 1565 par le Primatice et qui constitue aujourd'hui le rez-de-chaussée de l' édifice , la porte triomphale actuelle possède un étage en forme d' arcade surmonté d' un dôme à pans dont le fronton triangulaire est orné de sculptures représentant deux victoires soutenant les armes de Henri IV .
Elle fut édifiée par Louis XV , soucieux de trouver de nouveaux espaces .
Il a été édifié par Jacques-Ange Gabriel en 1750 , avec un toit d' ardoise mansardé et percé de plusieurs œils-de-bœuf .
Les salons qui composent ce musée , aux décors de style Second Empire , furent restaurés en 1991 .
Un escalier d' honneur , édifié en 1768 à l' emplacement d' un ancien escalier du XVI e siècle , est orné sous Louis-Philippe de tableaux d' Alexandre-François Desportes et Jean-Baptiste Oudry représentant des scènes de chasses et des natures mortes .
Au cours du XIX e siècle , le château de Fontainebleau n' a subi que peu de transformations extérieures .
Ces aménagements ont surtout eu lieu sous les règnes de Napoléon I er , Louis XVIII , et Louis-Philippe .
On peut néanmoins noter la construction en 1834 ( sous Louis-Philippe ) , d' un petit pavillon , dit " pavillon Louis-Philippe " , jouxtant la galerie de Diane .
Disposant d' environ 450 places , le théâtre s' inspire des décors de l' opéra royal de Versailles .
Elle acquit son nom grâce à un moulage en plâtre de la statue équestre de Marc Aurèle au Capitole , installé entre 1560 et 1570 , disparu en 1626 , et dont une petite dalle , dans l' allée centrale , rappelle l' emplacement .
Elle fut considérablement modifiée sous Henri IV .
Le parc de Fontainebleau s' étend sur 115 hectares .
Le jardin de Diane , au nord du château , fut élevé par Catherine de Médicis sur un espace déjà aménagé par François I er et portait à l' époque le nom de jardin de la Reine .
Il fut de nouveau remanié sous Louis XIV .
Au XIX e siècle , sous Napoléon I er puis Louis-Philippe , le jardin fut transformé en jardin anglais et l' orangerie détruite .
La statue actuelle , dite Diane à la biche , date de 1684 .
Même après la disparition de ces arbres , le nom lui est resté , et Henri IV y plante le premier platane , essence rare à l' époque .
Au centre d' un vaste étang peuplé de carpes tricentenaires , dont les premiers spécimens , une soixantaine , furent offerts à Henri IV par Charles de Lorraine , s' élève un pavillon d' agrément octogonal à toiture basse , sobrement décoré , édifié sous Henri IV , reconstruit sous Louis XIV en 1662 et restauré par Napoléon I er .
Les terrasses furent plantées de tilleuls sous Napoléon I er .
On pouvait s' y promener en bateau et Louis XIII y fit naviguer une galère .
Il est nécessaire de différencier , dans l' appréciation des chiffres du tourisme sur le site du château de Fontainebleau , le château lui-même , le domaine ( château , jardins et parc ) , et un troisième ensemble plus large englobant le château , ses jardins , son parc , et la forêt de Fontainebleau environnante .
Le château de Fontainebleau a reçu 329960 visiteurs en 2001 , 232087 en 2007 et 230816 en 2008 .
Le château et son parc , constituent en 2008 le troisième site le plus visité du département de Seine-et-Marne , avec une fréquentation de 384039 visiteurs , en hausse de 8 % par rapport à 2007 .
En tout , château , jardins , et forêt de Fontainebleau accueillent quelques 13 millions de visiteurs .
Cependant , cette fréquentation exceptionnelle qui touche à la fois le national et l' international , ne produit que peu de retombées économiques sur Fontainebleau et sa région , du fait d' un manque de services associés ( hébergement , restauration , locations ... ) .
C' est également lors d' un séjour au château que Prosper Mérimée écrit et dicte sa célèbre dictée en 1857 à la demande de l' impératrice Eugénie pour distraire la cour de Napoléon III .
On note la présence des compositeurs Claudin de Sermisy et Clément Janequin dans la cour de François I er .
Dans la dernière moitié du siècle , et en particulier sous le règne d' Henri III , les œuvres de Roland de Lassus et Claude Goudimel sont jouées au château .
Cette démarche artistique sera poursuivie au siècle suivant , avec la venue de Jean-Baptiste Lully , Michel-Richard de Lalande , Marc-Antoine Charpentier , François Couperin , et Marin Marais .
Le XIX e siècle est particulièrement marqué par la venue , sur ordre de Louis-Philippe , de l' opéra de Paris qui interprète en 1835 Le Comte Ory de Rossini .
Le château de Fontainebleau , grâce à son cadre historique , a été le théâtre de nombreux tournages cinématographiques , parmi lesquels :
