Le TCSEC , ouvrage de référence en la matière , est issu du Department of Defense ( DoD ) des États-Unis .
Cela étant , en France , ce sont principalement les grandes sociétés , entreprises du secteur public et administrations qui ont désigné et emploient , à plein temps ou non , des " responsables de la sécurité des systèmes d' information " .
