La Lune s' est formée peu de temps après , sans doute à la suite d' une collision avec un objet de la taille de Mars .
La constitution et la densité de l' atmosphère sont telles que la lumière incidente du Soleil et la lumière réfléchie par les continents et les mers sont diffractées ; donnant sa couleur au ciel , et par réflexion , aux étendues d' eau .
On retrouve cette conception chez Parménide , Platon ou Aristote .
Charlemagne est d' ailleurs représenté sur quelques enluminures comme tenant à la main une représentation d' un petit globe terrestre surmonté d' une croix .
Les récits de voyages de missionnaires , de Marco Polo et de l' explorateur Jean de Mandeville ( avec son Livre des merveilles du monde ) diffusaient dans la société l' image d' une terre sphérique , qui pouvait théoriquement faire l' objet d' une " circumnavigation " .
Le plus ancien globe terrestre connu est fabriqué par Martin Behaim vers la fin du XV e siècle , peu avant que Vasco de Gama , Christophe Colomb ou Magellan entreprennent leurs voyages .
La pesanteur a été définie pour être indépendante du mouvement de l' objet sur Terre .
Ceci permet encore tout juste les éclipses totales sur Terre .
Cette hypothèse explique en partie le fait que la composition de la Lune ressemble particulièrement à celle de la croûte terrestre .
L' idée de Copernic fut soutenue par Johannes Kepler et Galilée .
Le Jour de la Terre a lieu le 22 avril , depuis 1970 .
