Le coup d' État du 11 septembre 1973 au Chili a été un évènement historique marquant à la fois de l' histoire du Chili et de la Guerre froide .
Celui-ci fut planifié par les commandants en chef des trois armées et le chef de la police , et dirigé par le général d' armée Augusto Pinochet .
Le rôle exact des États-Unis a longtemps fait l' objet d' incertitudes .
L' Union soviétique estimait que la principale erreur du président chilien était d' être " trop faible " pour avoir refusé notamment de recourir à la force contre l' opposition ; et elle avait renoncé à lui fournir un soutien de grande ampleur lorsqu' il était apparu que la politique économique du gouvernement souffrait de mauvaise gestion chronique .
Le nouveau président est investi dans ses fonctions le 4 novembre 1970 et met rapidement en place le programme de l' Unité populaire .
Des difficultés d' approvisionnement , l' inflation galopante ( 508 % en 1973 contre 35 % en 1970 ) et les grèves placent le Chili dans une situation difficile .
Selon Raymond Aron : " Les classes atteintes par les réformes , les catégories sociales traumatisées par la menace des nationalisations se révoltent [ ... ] " , notamment les grands propriétaires dont les possessions doivent être nationalisées sans contrepartie sérieuse .
Entre novembre 1970 et septembre 1973 , le président Allende forme six gouvernements , notamment à cause de la démission de ministres ou suite à leur destitution par le parlement .
Auparavant , le 26 mai 1973 , la Cour suprême avait déclaré inconstitutionnelles et illégales de nombreuses dispositions prises par le gouvernement .
Celle-ci rate son objectif de faire destituer légalement le président Allende en n' atteignant pas les 60 % des voix qui lui auraient permis constitutionnellement de renverser le président chilien .
Les partisans d' Allende voient une approbation de la politique gouvernementale dans la progression de la gauche lors de ces élections ( la première fois dans l' histoire chilienne que les partis au pouvoir voyaient leurs résultats électoraux progresser lors d' une élection à mi-mandat ) .
Après avoir sauvé le gouvernement d' un premier putsch , le Tanquetazo , en juin 1973 ( un régiment de chars s' en était pris au palais présidentiel , la Moneda ) , le général Carlos Prats doit démissionner suite à de nouvelles grèves dans les professions libérales et chez les camionneurs .
Il est remplacé par Augusto Pinochet .
En conséquence de cette fausse estimation , les États-Unis n' avaient engagé des fonds que dans une mesure beaucoup plus faible qu' en 1964 .
Le président Richard Nixon est " hors de lui " et décidé à agir .
D' après une note interne de la CIA : " Le président a demandé à l' agence [ la CIA ] d' empêcher Allende d' accéder au pouvoir ou de le destituer et a débloqué à cette fin un budget allant jusqu' à 10 millions de dollars .
L' attitude officielle retenue est cependant " froide mais correcte " , afin d' éviter une confrontation qui renforcerait Allende .
Les États-Unis diminuent l' attribution de crédits mais poursuivent les programmes en cours dont celui de l' armée chilienne ( interrompu pendant l' élection ) et offre par ailleurs des stages aux officiers chiliens .
Entre la date de l' élection présidentielle et l' intronisation d' Allende , les États-Unis cherchent dans la précipitation un moyen d' empêcher son accession au pouvoir .
Kissinger rapporte que cette action , vouée à l' échec , avait pourtant été décommandée et n' avait pas bénéficié d' appui américain .
En septembre 1973 , comme chaque année , l' US Navy et la marine chilienne organisent des manœuvres communes .
Les troupes d' infanteries de marine passent ainsi la journée du 10 septembre 1973 avec quatre navires de la Navy au large de Valparaíso ce qui leur fournit un alibi afin de ne pas attirer l' attention sur les préparatifs du putsch .
De retour à Valparaíso , les troupes d' infanterie de marine coupent les communications .
À 3 heures du matin , le 11 septembre , Valparaíso est aux mains des putschistes sans coup férir .
Le 11 septembre 1973 , à 9 heures du matin , la Moneda ( siège de la présidence chilienne ) est assiégée par l' armée de terre sous le commandement du général Pinochet .
Salvador Allende est retranché , depuis 7 heures du matin , dans le palais présidentiel , avec 42 de ses gardes fortement armés .
Peu avant midi , deux avions de chasse de l' armée bombardent la Moneda à coups de roquettes .
À 14 heures , le palais est envahi mais Salvador Allende est déjà mort .
À sa prise de pouvoir , Pinochet fait en sorte d' être seul à la tête du conseil .
Selon un rapport remis au président Ricardo Lagos dans les années 2000 , près de 27255 personnes ont été torturées .
Depuis 1990 , le Chili est redevenu une démocratie .
Bien qu' inculpé de " génocide , terrorisme et tortures " , Augusto Pinochet est mort en décembre 2006 à l' âge de 91 ans , sans jamais avoir été jugé pour les exactions commises sous son régime .
Il reste l' une des personnalités les plus controversées du Chili .
