La situation de la ville , dans une cuvette protégée par la chaîne des Ârâvalli , lui permettait de se défendre assez aisément , la route de Delhi étant tenue par la forteresse d' Amber , à une dizaine de kilomètres .
Jaipur se trouve à 430 m d' altitude et à 260 km de Delhi .
Située au pied des monts Ârâvalli , elle suit un plan en damier trois par trois et est entourée d' une muraille de 6 m de hauteur et de 4 m de large .
La Jaipur originelle comportait de larges avenues de 34 m de large , le reste des rues composant le quadrillage ayant au moins 4 m de largeur .
Cependant , en prévision de la visite du prince Albert , en 1876 , elle fut peinte en rose dans sa totalité , le rose étant une couleur traditionnelle de bienvenue .
Elle ne cessera d' exister qu' à l' indépendance de l' Inde en 1947 .
La dynastie régnante de Jaipur fournit à l' Empire moghol certains de ses généraux les plus distingués .
Par un traité de 1818 , la principauté passe sous la protection des Britanniques en échange d' un tribut annuel .
Durant la révolte des Cipayes , le mahârâja apporte son soutien aux Britanniques .
