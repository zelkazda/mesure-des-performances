Les Égyptiens avaient une conception dynamique de la pierre .
Dans une inscription à Abou Simbel , datant du règne de Ramsès II ( 1279-1213 av .
) , le dieu Ptah , créateur du monde , dit comment les déserts créent des pierres précieuses .
Les textes les plus anciens sont des œuvres de Bolos de Mendès , et des citations ou courts traités mis sous des noms de personnages célèbres , mythologiques ou divins ( Hermès , Isis , Moïse ... ) ou réels .
Sénèque attribue à Démocrite des réussites alchimiques ou simplement métallurgiques , notamment le moyen d' amollir l' ivoire ou de convertir par la cuisson certaines pierres en émeraude .
En revanche , avec Zosime de Panopolis ( aussi nommé Zosime le panopolitain ) , la technique se double d' une mystique et d' une symbolique .
Geber pose aussi un septénaire , celui des sept métaux : or , argent , cuivre , étain , plomb , fer ( Mars ) , vif-argent ; un autre septénaire , celui des opérations : sublimation , distillation ascendante ou descendante ( filtration ) , coupellation , incinération , fusion , bain-marie , bain de sable .
Le philosophe Algazel ( Al-Ghazâlî 1058-1111 ) parle d' une alchimie de la félicité ( kimiyâ es-saddah ) .
1332 ) , Gérard Dorn reprendront l' idée de mêler pratique et allégorie .
Vers 1250 , Albert le Grand admet la transmutation , il établit l' analogie entre la formation du fœtus et la génération des pierres et métaux .
Roger Bacon soutient que la médecine des métaux prolonge la vie et que l' alchimie , science pratique , justifie les sciences théoriques ( et non plus l' inverse ) : le premier , il voit le côté double ( spéculatif et opératoire ) de l' alchimie .
Petrus Bonus soutient la théorie du mercure seul .
Cette théorie de la quintessence introduit l' idée du " principe actif " possédant au centuple les mêmes propriétés que les simples , dont Galien avait détaillé les effets bénéfiques sur le plan humain .
Élie de Cortone , Gérard de Crémone , Roger Bacon , Jean de Roquetaillade sont des franciscains .
) , pour la première fois , l' image du Christ est comparée à la pierre philosophale .
Quand Rodolphe II de Habsbourg est empereur ( 1576-1612 ) , la capitale de l' alchimie est Prague .
Les adeptes de l' époque y convergent : Heinrich Khunrath , Oswald Croll , Michael Maier .
Le livre est daté de 1399 , mais il ne fut édité en 1612 , il n' a pu être écrit que vers 1590 , peut-être par l' écrivain François Béroalde de Verville ( 1558-1612 ) .
Elle se prolonge par certaines œuvres de Giordano Bruno ou de Jean d' Espagnet .
De 1668 à 1675 Isaac Newton pratique l' alchimie .
En 1722 , le médecin et naturaliste français Étienne-François Geoffroy , inventeur du concept d' affinité chimique ne croit pas à la transmutation , mais ne pense pas possible de démontrer son impossibilité :
En 1783 , Lavoisier décompose l' eau en oxygène et hydrogène .
Attention , je précise , Fulcanelli en 1922 et même avant , c' était un beau vieillard , mais c' était un vieillard . "
Pour Fulcanelli , l' alchimie est ésotérique , l' archimie et la spagyrie exotériques .
La recherche des remèdes d' immortalité fait partie de la culture chinoise antique depuis la période des Royaumes combattants .
L' alchimie exterieure , telle que pratiquée par Ge Hong par exemple , cède la place à l' alchimie intérieure qui domine dès la fin des Dynastie Tang en 907 .
De façon classique la recherche de la pierre philosophale se faisait par la voie dite voie humide , celle-ci est par exemple présentée par Zosime de Panopolis dès 300 .
La voie sèche est beaucoup plus récente et a peut-être été inventée par Basile Valentin , vers 1600 .
Selon Jacques Sadoul la voie sèche est la voie des hautes températures , difficile , tandis que la voie humide est la voie longue ( trois ans ) , mais elle est moins dangereuse .
Fulcanelli dit à ce propos " À l' inverse de la voie humide , dont les ustensiles de verre permettent le contrôle facile et l' observation juste , la voie sèche ne peut éclairer l' opérateur " .
On trouve ces phases dès Zosime de Panopolis .
Roger Bacon veut " prolonger la vie humaine " .
Johannes de Rupescissa ( Jean de Roquetaillade ) ajouta , vers 1352 , la notion de quintessence , préparée à partir de l ' aqua ardens ( alcool ) , distillée des milliers de fois ; il décrit l' extraction de la quintessence à partir du vin et explique que , conjointe à l' or , celle-ci conserve la vie et restaure la santé .
En un sens Paracelse fait donc de l' iatrochimie ( médecine hermétique ) , plutôt que de l' alchimie proprement dite .
De même la légende du comte de Saint-Germain marqua l' alchimie , il aurait eu le souvenir de ses vies antérieures et une sagesse correspondante , ou aurait disposé d' un élixir de longue-vie lui ayant donné une vie longue de deux à quatre mille ans selon lui .
C' est en particulier vrai pour certains positivistes ( dont Marcelin Berthelot ) qui ne considèrent l' alchimie que sous cet angle .
Cette interprétation de l' alchimie comme proto-chimie repose entre autres sur les techniques et les ustensiles de l' alchimie , utilisés par les savants ( Newton , etc.. ) avant la méthode scientifique , continue d' être utilisé de nos jours .
L' interprétation de l' alchimie comme relevant uniquement d' une proto-chimie proviendrait essentiellement d' une erreur d' interprétation de Marcelin Berthelot au XIX e .
La mise en évidence d' un symbolisme alchimique , similaire dans des civilisations éloignées dans le temps et dans l' espace , a conduit Carl Gustav Jung , très tôt , à valoriser l' alchimie , comme processus psychologique .
Gaston Bachelard tient l' alchimie pour une rêverie de célibataire , poétique , mais sans valeur scientifique , à base de désirs masculins inavoués ( La psychanalyse du feu , 1937 ) .
Dans " un champ véritablement anthropologique " se situe également l' œuvre de Gilbert Durand , qui revalorise l' imagination .
Fulcanelli , par exemple , s' emploie à multiplier les indications tout en restant cryptique .
Synésius semble plutôt décrire la matière dans son état avancé .
Cet auteur résume la problématique ainsi : " Car si la force de l' alchimie réside bien dans le seul mercure des philosophes , comme le proclama très tôt Albert le Grand ( 1193-1280 ) , c' est que la substance mercurielle , par excellence protéiforme , est alors envisagée soit comme une materia prima en qui sont latentes toutes les virtualités ( dont celle du soufre ) , soit , après préparation , comme mercure double ( ou hermaphrodite ) en qui a été consommé et fixé l' union des 2 principes " .
à Alexandrie ) a inventé le fameux " bain-marie " , dispositif dans lequel la substance à faire chauffer est contenue dans un récipient lui-même placé un récipient rempli d' eau , ce qui permet d' obtenir une température constante et modérée .
Dans la ville d' Alexandrie , on trouve une importante corporation de parfumeurs , possédant des alambics ( ambikos ) pour distiller des élixirs , des essences florales ; Zosime de Panopolis , vers 300 , présente une illustration d' un alambic pour métaux , raffiné .
Geber , mort vers 800 , découvre divers corps chimiques : l' acide citrique ( à la base de l' acidité du citron ) , l' acide acétique ( à partir de vinaigre ) et l' acide tartrique ( à partir de résidus de vinification ) .
Albert le Grand réussit à préparer la potasse caustique , il est le premier à décrire la composition chimique du cinabre , de la céruse et du minium .
En 1352 , Jean de Roquetaillade ( Jean de Rupescissa ) introduit de la notion de quintessence , obtenue par distillations successives de l ' aqua ardens ( l' alcool ) ; cette idée d' un principe actif sera essentielle dans l' histoire de la médecine , car il introduit un grand nombre de médicaments chimiques , tels que la teinture d' antimoine , le calomel , le sublimé corrosif .
Paracelse est un pionnier de l' utilisation en médecine des produits chimiques et des minéraux , dont le mercure contre la syphilis , l' arsenic contre le choléra .
Basile Valentin décrit vers 1600 l' acide sulfurique et l' acide chlorhydrique .
Jan Baptist Van Helmont , " précurseur de la chimie pneumatique " , révèle vers 1610 , d' une façon scientifique , l' existence des " gaz " , comme il les nomme , et en reconnaît plusieurs .
Isaac Newton s' intéresse aux pratiques alchimiques .
Pourtant , Ernest Rutherford , en 1919 , réalise la première transmutation artificielle : en bombardant de l' azote avec les rayons alpha du radium , il obtient de l' oxygène .
