Daniel Poulou fait son entrée en politique en 1977 en étant élu maire de la ville basque d' Urrugne ( Pyrénées-Atlantiques ) .
Le 28 mars 1993 , il est élu comme suppléant de la députée de la sixième circonscription des Pyrénées-Atlantiques , Michèle Alliot-Marie .
Il siège à l' Assemblée nationale à partir du 2 mai 1993 , lorsque celle-ci est nommée au gouvernement .
Il est alors membre du groupe RPR .
Il démissionne le 26 juillet 1995 , suite au départ de Michèle Alliot-Marie du gouvernement .
Le 1 er juin 1997 , il est à nouveau élu comme suppléant de Michèle Alliot-Marie .
Le 16 juin 2002 , il est de nouveau élu comme suppléant de Michèle Alliot-Marie ( avec 60,88 % des voix au second tour ) et fait son retour à l' Assemblée nationale à partir du 19 juillet 2002 .
Pour la quatrième fois , Daniel Poulou devient ainsi député le 20 juillet 2007 à sa place .
Il est à nouveau membre du groupe UMP .
