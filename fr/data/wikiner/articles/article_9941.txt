Il est ainsi explicitement utilisé dans la première édition du Larousse gastronomique en 1938 ;
Ces fromages -- dits de Brie par référence à la région homonyme -- sont à l' origine un mets populaire consommé principalement par les ouvriers agricoles , appelés à se déplacer de ferme en ferme sur de longues distances pendant plusieurs mois de l' année .
Lors de la deuxième moitié du siècle , le " petit coulommiers " connaît son véritable essor après sa présentation à l ' Exposition universelle de 1878 .
Ce type de fromage est parfois fabriqué dans les régions Champagne-Ardenne , Poitou-Charentes ou ailleurs .
Cela s' explique notamment par la faible nombre de vaches laitières en région Ile-de-France ( 7 000 têtes environ en 2008 , contre 109 000 en Champagne-Ardenne et près de 105 000 en Poitou-Charentes ) .
Elle représente 12 % des tonnages de fromages de vache à pâte molle produits en France en 2008 , dont les principaux sont les camemberts ( 25,3 % ) et les bries ( 23,9 % , qu' ils soient communs ou sous appellation ) ..
En 2007 , les sociétés Bongrain et Sodiaal ont annoncé leur volonté de créer une société commune vouée à produire camemberts , coulommiers et bries sous leurs différentes marques .
La Commission européenne a accepté cette création , en étudiant les risques d' atteinte à la concurrence et d' abus de position dominante par segment de marché .
La nouvelle entité aura une position importante ( 30-40 % ) , mais elle devra compter avec la concurrence de Lactalis ( 45-55 % ) " ; par ailleurs , les coulommiers sous marques de distributeurs représentaient , au moment de cette enquête de marché , 60 % du marché en valeur et 70 % en quantité sur un marché où " les marques semblent avoir joué un rôle moins important que pour le camembert " .
