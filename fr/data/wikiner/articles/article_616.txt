Harpocrate est dans la mythologie grecque le dieu du silence , adaptation de la divinité égyptienne Horus enfant .
C' est sous ce nom que le dieu Horus fut adoré à Alexandrie d' abord , puis dans tout le monde gréco-romain , à côté d' Isis , de Sérapis et d' Anubis .
Catulle emploie par plaisanterie le nom d' Harpocrate pour désigner un personnage discret .
Dans les images proprement gréco-romaines le dieu porte sur le front , comme ses parèdres Isis et Sérapis , une fleur de lotus ou un croissant ' .
Il est généralement nu comme Éros , ou légèrement vêtu ; parfois aussi il a des ailes derrière le dos .
Un carquois rappelle ses attributions de divinité solaire identifiée avec Apollon .
On confondit alors dans la personne d' Harpocrate tous les types de dieux enfants , créés par les artistes antérieurs .
