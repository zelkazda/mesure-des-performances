Daniel Auteuil est un comédien et metteur en scène de théâtre , et acteur de cinéma français , né le 24 janvier 1950 à Alger ( alors en Algérie française ) .
La fréquentation des théâtres où se produisent ses parents , les tournées notamment avec l' Opéra de Paris le prédisposent à embrasser une carrière artistique .
Malgré plusieurs tentatives , il n' est jamais admis au concours d' entrée du CNSAD .
En 1975 , Gérard Pirès lui offre son premier rôle au cinéma dans L' Agression aux côtés de Catherine Deneuve et Jean-Louis Trintignant .
Mais c' est avec son interprétation d' un lycéen flémard , dragueur et peu studieux dans Les Sous-doués en 1980 qu' il accède à la notoriété et acquiert un statut de grand acteur comique .
Le film connaît une suite : Les Sous-doués en vacances .
En 1981 , il a une fille , Aurore , avec l' actrice Anne Jousset .
Entretemps , il épouse Emmanuelle Béart et partage 11 ans de sa vie avec elle .
Dans les années 1990 , Daniel Auteuil enchaîne les rôles d' un registre plus grave grâce à des réalisateurs comme Claude Sautet ( Un cœur en hiver ) , André Téchiné ( Ma saison préférée ) , Régis Wargnier ( Une femme française ) , Christian Vincent ou encore Jaco Van Dormael .
Pour ce dernier film , il remporte , ex æquo avec Pascal Duquenne , le Prix d' interprétation masculine à Cannes .
Il l' épouse à l' âge de 56 ans le samedi 22 juillet 2006 à Porto-Vecchio en Corse-du-Sud avec pour témoins le chanteur Dave et leurs amis Maxime Le Forestier , Christian Clavier et Élie Semoun .
Dans les années 2000 , sa carrière est autant marquée par des rôles sombres : L' Adversaire de Nicole Garcia , 36 quai des Orfèvres d' Olivier Marchal , Caché de Michael Haneke , que par un retour remarqué à la comédie : Le Placard de Francis Veber , Après vous de Pierre Salvadori ou encore La Personne aux deux personnes de Nicolas & Bruno .
Daniel Auteuil est parrain du téléthon 2009 .
