Ils fondèrent également le " royaume vandale d' Afrique " , ou " royaume de Carthage " ( 439 -- 533 ) .
L' origine des Vandales est scandinave .
Il est possible que le nom de " Hasdings " ne fût alors porté que par la famille royale .
C' est au cours de cette période de séjour dans les steppes russes que les Vandales deviennent , comme les Goths , un peuple de cavaliers renommés .
Ils s' associent aux Sarmates , nation nomade d' origine iranienne , et notamment à leur principale tribu , celle des Alains .
En 271 , l' empereur romain Aurélien bat les Goths et les Vandales sur le Danube , et passe un traité avec les Vandales pour la fourniture de 2000 cavaliers servant comme troupes auxiliaires des légions .
Ce procédé explique qu' un des derniers grands généraux de l' empire Stilicon soit d' origine Vandale .
Au troisième siècle , les Vandales et Alains se convertissent en majorité à la religion arienne .
Les Francs repoussent les Vandales , encombrés par leurs familles , leur roi Godégisel est tué au combat : c' est grâce à l' intervention de la cavalerie lourde des Alains que la percée est faite et que les troupes franques sont décimées .
Les Vandales participent ensuite à l' invasion de la Gaule , qu' ils pillent en tous sens durant près de deux ans et demi .
Après quoi , ils migrent en compagnie des Alains et des Suèves vers les Pyrénées .
À l' automne 409 , les Vandales entrent dans la péninsule ibérique , où ils s' installent avec une partie de leurs alliés alains ( dont certains clans sont restés en Gaule , notamment sur la Loire ) .
Les Suèves sont également battus à plusieurs reprises , repoussés et confinés dans le nord-ouest de la péninsule .
Ils restent dans la région de la future Andalousie durant une dizaine d' années .
Cette étape est très importante pour eux , car elle leur permet de devenir le seul peuple barbare maîtrisant la navigation ( après avoir enrôlé des marins de force ) , tandis qu' ils élargissent le champ de leurs actions aux îles Baléares et sur la côte nord-africaine .
La ville tombe à la suite d' un long siège en 431 , durant lequel meurt le célèbre évêque saint Augustin .
Les Romains reconnaissent l' établissement des Vandales en Maurétanie et en Numidie , et tentent de les apaiser en signant un traité avec eux , en 435 .
Néanmoins , les Vandales reprennent leur progression le long de la côte , pour prendre Carthage sans grande difficulté le 19 octobre 439 .
Ils contraignent Rome à ré-établir un traité avec eux en 442 , et constituent un royaume vandale d' Afrique , parfois nommé " royaume de Carthage " , du nom de la riche capitale romaine d' Afrique qu' ils prennent en 439 .
Le royaume vandale disparaît par suite d' une intervention de l' armée byzantine ; défaits , les Vandales se replient sur ce qui leur reste de royaume dans le territoire de l' actuelle Algérie et en Tunisie , où ils se fondirent dans la population .
Plusieurs noms de famille rappellent la présence des Vandales et des Alains .
Les Vandales de la ville de Carthage furent , pour ceux qui étaient capturés , déportés vers Byzance .
En français , le mot vandale est employé pour la première fois dans un sens péjoratif par Voltaire en 1734 .
Leur royaume arien d' Afrique du Nord est organisé avec une méthode exemplaire [ réf. nécessaire ] .
Leur pillage de Rome , effectué sans destructions ni massacres [ réf. nécessaire ] , est un modèle d' organisation : les armées Vandales et berbères passent un accord avec le pape Léon I er [ réf. nécessaire ] , afin de récupérer les richesses de la ville sans violence .
Ils divisent Rome , à cet effet , en îlots qui sont visités successivement , et dont les objets de valeur sont systématiquement emportés .
