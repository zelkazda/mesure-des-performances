Située sur l' Orne , au cœur de la Suisse normande , Pont-d'Ouilly est une commune de tourisme et de loisirs .
La commune se trouve sur la route reliant Falaise à Condé-sur-Noireau .
Depuis le temps des diligences , Pont-d'Ouilly est reconnue comme ville étape .
Ainsi jusqu' en 1947 , la rive droite de l' actuel bourg de Pont-d'Ouilly dépend donc de la commune d' Ouilly-le-Basset , alors que la rive gauche dépend de celle de Saint-Marc-d'Ouilly , Saint-Christophe , en aval du pont , ayant été absorbé en 1826 par Ouilly-le-Basset .
La Deuxième Guerre mondiale est très douloureuse pour l' agglomération de Pont-d'Ouilly .
Face aux difficultés administratives et financières pour la reconstruction , elles décident de fusionner pour former la commune de Pont-d'Ouilly créée par décret en date du 23 août 1947 .
