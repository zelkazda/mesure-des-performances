Au sein du Parti socialiste ( il est membre de sa direction nationale ) , il est un des principaux soutiens de Ségolène Royal et défend sa candidature pour l' élection présidentielle de 2012 .
Il entre au Conseil d' État en 1971 en tant qu' auditeur .
De 1979 à 1981 , il est chargé de mission au syndicat intercommunal de développement des vallées de la Durance et de la Bléone .
Il est le plus jeune à ce poste et y détient le " record de longévité " sous la Cinquième République .
De 1983 à 1991 , Jean-Louis Bianco préside l' Office national des forêts ( ONF ) .
Il devient conseiller municipal de Digne-les-Bains en 1989 .
En 1991 , Jean-Louis Bianco se voit confier le ministère des Affaires sociales et de l' Intégration dans le gouvernement d' Édith Cresson .
En 1995 , il devient maire de Digne-les-Bains .
Il est élu député depuis le 1 er juin 1997 dans la 1 re circonscription des Alpes-de-Haute-Provence .
Ce dernier , ainsi que Jean-Louis Bianco , s' éloignent peu à peu de Gaëtan Gorce au sein du Parti socialiste avant de finalement se rejoindre à l' occasion du congrès de Reims .
Au sein de l' équipe de la candidate , il devient co-directeur de campagne , avec François Rebsamen jusqu' au 6 mai 2007 .
Le 12 décembre 2009 , Jean-Louis Bianco présente à la convention nationale du Parti Socialiste le " contrat socialiste de nos régions " , dont il a coordonné les travaux .
Ce programme est particulièrement bien reçu par les présidents de régions sortants et par les militants et contribuera au succès du PS aux élections régionales .
Fin décembre 2009 , suite à la rupture politique entre Ségolène Royal et Vincent Peillon eu sein de " l' espoir à gauche " et à la création par ce dernier du " rassemblement social , écologique et démocrate " dont Jean-Louis Bianco s' éloigne car trop peu conforme à " l' alliance arc-en-ciel " ( de la gauche de la gauche au centre-gauche ) qu' il défend , il appelle avec Najat Vallaud-Belkacem et Gaëtan Gorce à " transcender les courants " et à " dépasser les clivages corporatistes à l' intérieur du PS " .
En 2010 , il est membre des commissions de travail sur les différentes conventions nationales du PS : " nouveau modèle économique , social et écologique " , " rénovation " , " international " .
Jean-Louis Bianco est le petit-fils de quatre grands-parents étrangers .
