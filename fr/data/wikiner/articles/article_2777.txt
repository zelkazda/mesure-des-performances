Pretty Good Privacy ( ou PGP ) est un logiciel de chiffrement et de signature de données utilisant la cryptographie asymétrique mais également la cryptographie symétrique .
Philip Zimmermann , son développeur , a mis PGP en libre téléchargement en 1991 .
À l' époque où GnuPG , un logiciel libre compatible , n' était pas encore très utilisé , PGP avait la réputation du logiciel gratuit ( pas libre mais " semi-libre " ) de cryptographie asymétrique le plus sûr au monde .
Son code source étant ouvert ( bien qu' il ne soit pas un logiciel libre ) , et étant toujours soutenu par son auteur Philip Zimmerman , il possède encore la confiance d' un grand nombre d' utilisateurs ( en particulier envers la présence éventuelle de backdoors ) .
Zimmermann souligne qu' il a développé PGP dans un souci de droit à la vie privée et de progrès démocratique : " PGP donne aux gens le pouvoir de prendre en main leur intimité .
D' après Zimmermann : " Si nous voulons résister à cette tendance perturbante du gouvernement pour rendre illégal la cryptographie , une mesure que nous pouvons adopter est d' utiliser la cryptographie autant que nous le pouvons actuellement pendant que c' est encore légal .
Par conséquent , utiliser PGP est bon pour préserver la démocratie. "
