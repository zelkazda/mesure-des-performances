C' est le chimiste allemand Hermann Emil Fischer qui , après lui avoir donné son nom en 1884 , a synthétisé la purine en 1898 et montré qu' elle formait une famille chimique .
