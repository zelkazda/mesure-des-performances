Issu d' une famille noble mais sans grandes ressources , Ludovico Ariosto reçut une bonne éducation humaniste , mais regretta toujours de ne pas avoir appris le grec .
Entré en 1504 au service du cardinal Hippolyte d' Este , il accomplit pour ce prince de nombreuses ambassades , notamment auprès du pape Jules II .
Ayant refusé de suivre le prélat en Hongrie , il passa au service d' Alphonse d' Este , duc de Ferrare et frère du cardinal .
Il s' en tira avec honneur et put enfin se retirer dans sa petite maison de Ferrare ( toujours visible ) entouré par l' affection de sa maîtresse et de son fils .
C' est entre ces diverses charges que l' Arioste ne cessa de travailler à son chef-d'œuvre , le Roland furieux ( Orlando furioso ) , subtile parodie du poème chevaleresque , qui se présente comme une suite au Roland amoureux de Matteo Maria Boiardo , son prédécesseur .
Ses compatriotes , dans leur admiration , l' ont surnommé le divin Arioste .
