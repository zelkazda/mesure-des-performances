C' est aussi le cousin germain de Dominique de Villepin .
Il est membre de l' UMP et a été président du Stade rennais entre le 17 décembre 2007 et le 7 mai 2010 .
Il est nommé président du Stade rennais le 4 décembre 2006 pour remplacer Emmanuel Cueff , et occupe en parallèle le poste de directeur des relations publiques du Groupe PPR .
En 1988 , Frédéric de Saint-Sernin fait partie de l' équipe de campagne de Jacques Chirac , pour l' élection présidentielle .
En mars 1993 , il devient député de la Dordogne .
Il est donc remplacé à la tête de sa circonscription par son conseiller , Bernard Mazouaud .
Entre temps , il est représentant du coprince français en Andorre ( 1999 ) .
Il a été conseiller de Jacques Chirac sur les questions institutionnelles et parlementaires , et les études d' opinion et les sondages , fonction qu' il a quittée le 4 décembre 2006 .
