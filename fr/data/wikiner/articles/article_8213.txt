Il épouse sa sœur Arsinoé III ; son épithète , Philopator , signifie ( par ironie ) qui aime son père .
Eratosthène était son précepteur .
Son fils Ptolémée V , encore enfant , lui succède à sa mort .
