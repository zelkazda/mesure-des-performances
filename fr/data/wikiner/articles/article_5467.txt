Aujourd'hui encore le village continue cette fabrication , les artisans exposent leurs marchandises le long de la route départementale Bayeux-Saint-Lô .
Pendant la Seconde Guerre mondiale , les pots en terre de Noron servaient aux soldats pour y mettre leur poudre .
En 1903 , Noron devient Noron-la-Poterie .
Parrain en 2002 de Jacques Cheminade et déjà sollicité par Jean-Marie Le Pen et Olivier Besancenot , il entend ainsi faire parler de sa commune , tourner en dérision le système actuel et la politique dont il se dit un dégoûté .
C' est finalement Rachid Nekkaz qui , en direct sur une chaîne d' information , lui achète ce parrainage pour 1550 euros et le déchire instantanément .
Noron-la-Poterie a compté jusqu' à 365 habitants en 1831 .
