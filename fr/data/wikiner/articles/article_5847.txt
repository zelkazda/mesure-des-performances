C' est une commune avec une petite façade maritime donnant sur la Manche .
Cette commune se trouve à 12 km de Coutances et 40 km de Saint-Lô .
Sa population fait plus que doubler en été , en raison de son attractivité touristique , et de sa proximité avec la commune d' Agon-Coutainville .
On y trouve à proximité la plage d' Agon-Coutainville , ainsi qu' un golf de 18 trous sur cette même commune .
On peut y admirer , sur la route de Gouville-sur-Mer , le château des Ruettes .
En 1936 , Blainville devient Blainville-sur-Mer .
Blainville a compté jusqu' à 1951 habitants en 1806 .
Blainville-sur-Mer est dénommée " commune touristique " depuis décembre 2009 .
Zimmerbach ( France )
