Il a été découvert en 1952 par Albert Ghiorso sous la forme de son isotope 253 , récupéré dans les débris résultant d' une explosion thermonucléaire .
Il a été nommé ainsi en l' honneur d' Albert Einstein .
