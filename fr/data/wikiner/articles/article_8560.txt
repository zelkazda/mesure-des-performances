Frédéric Ozanam et ses amis se veulent proches des idées développées par Lamennais et que l' on désigne généralement par catholicisme libéral .
Ce débat demeurera très vif tout au long du XIX e siècle , notamment en France , à partir de 1870 : beaucoup de religieux rejettent alors l' idée républicaine et font du rétablissement de la monarchie une question de principe .
Un clivage s' établit alors entre les catholiques : -- ceux qui demeurent crispés sur la question du régime , et que l' on retrouvera plus tard au sein de l' Action française , -- ceux qui veulent s' investir prioritairement dans le domaine social , poursuivant en cela une action initiée en 1871 par Albert de Mun et René de la Tour du Pin avec la création des cercles catholiques ouvriers ; -- et ceux qui veulent en plus s' investir dans le domaine politique .
En France , un " un Parti démocratique chrétien " se crée en 1896 , mais disparaît rapidement , victime de la division entre partisans de l' action politique et ceux du combat social .
Le 18 janvier 1901 , Léon XIII fait paraître l' encyclique Graves de communi re qui , pour la première fois dans un document pontifical , explique le terme de démocratie chrétienne qu ' il oppose à celui de démocratie sociale .
Ce document eut un grand retentissement en Italie et en Allemagne .
À l' approche de la guerre , la démocratie chrétienne n' existe alors en France qu' à l' état de courant de pensée , ou de groupements dont l' action est limitée , soit dans l' espace ( groupements locaux ) , soit dans le domaine d' intervention ( la méfiance à l' égard du politique restant forte ) .
La dernière tentative de créer , avant-guerre , un mouvement dynamique entraînant les catholiques sur le terrain politique est le Sillon de Marc Sangnier .
Ce mouvement connaît alors une belle expansion , avant d' être condamné par le pape Pie X dans une lettre adressée à l' épiscopat français le 25 août 1910 .
Le Sillon renaîtra cependant quelques années plus tard sous la forme d' une ligue , Jeune République abandonnant toute référence confessionnelle .
Après la guerre , le pape Benoît XV , dont les efforts pour faire cesser les combats s' étaient avérés vains , multiplie alors les initiatives pour rapprocher les chrétiens des différents pays divisés par la guerre .
En France , Marc Sangnier œuvre notamment à un rapprochement avec l' Allemagne .
En France , le Parti démocrate populaire est ainsi créé en 1924 .
C' est à cette occasion que se créent des contacts entre des hommes tels que Robert Schuman , Konrad Adenauer ou Alcide De Gasperi .
En France , le mouvement démocrate-chrétien s' exprimera cependant assez peu par le biais du Parti démocrate populaire , qui demeurera un parti de cadre plus qu' un parti de masse , mais davantage par le biais d' autres structures telles que les organes de presse ( par exemple Ouest-Eclair ) , les syndicats de travailleurs ou les syndicats agricoles .
À l' approche de la Seconde Guerre mondiale , les militants démocrates-chrétiens des différents pays européens se retrouveront dans leur refus des totalitarismes menaçants à cette époque , même si le rôle de la hiérarchie religieuse pendant la guerre est sujet à controverse .
En France , la droite conservatrice et une partie de l' épiscopat sont discrédités .
Un vaste espace politique s' ouvre pour la création d' un parti d' inspiration démocrate-chrétienne : ce sera le Mouvement républicain populaire .
Marc Sangnier est son président d' honneur .
Il connaîtra d' importants succès électoraux tout au long de la IV e République avant de décliner rapidement , après le retour au pouvoir du général de Gaulle .
La disparition ou l' affaiblissement de la droite traditionnelle au sortir de la guerre ( France , Allemagne ... ) , l' influence de la guerre froide et la force grandissante de la menace communiste dans plusieurs pays occidentaux les a cependant conduits dans ces régions à se positionner plus à droite sur la scène politique , avec des fortunes diverses selon les configurations nationales .
À l' inverse , la fin de la guerre froide et les excès du capitalisme mondialisé ont pu favoriser un rapprochement avec les mouvements de la gauche réformiste à partir des années 2000 , notamment dans les régions où resurgissent des droites décomplexées ( France , Italie ... ) .
