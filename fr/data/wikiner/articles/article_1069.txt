En informatique , le POP ( Post Office Protocol littéralement le protocole du bureau de poste ) , est un protocole qui permet de récupérer les courriers électroniques situés sur un serveur de messagerie électronique .
Ce protocole a été défini dans la RFC 1939 .
POP3S ( POP3 over SSL ) utilise SSL pour sécuriser la communication avec le serveur , tel que décrit dans la RFC 2595 .
POP3S utilise le port 995 .
POP3 et POP3S utilisent tous deux le protocole de transfert TCP .
Voici un exemple de connexion du protocole POP3 sur un interpréteur de commandes :
Puis , il est possible d' utiliser l' une des commandes POP3 suivantes .
