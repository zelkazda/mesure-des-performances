Pour une économie de marché , ce niveau est élevé par rapport aux niveaux des pays développés comparables : l' imposition moyenne des trente pays membres de l' OCDE représente 35.9 % du PIB en 2005 ( inférieure en moyenne de 10 points de PIB ) .
L' administration française utilise la notion de prélèvements obligatoires , définie par l' OCDE , qui représentent 44.2 % du PIB en 2006 .
La définition des prélèvements obligatoires par l' OCDE est plus large que le champ fiscal : elle désigne les " versements obligatoires effectués sans contrepartie au profit des administrations publiques " .
Les cotisations sociales sont explicitement incluses par l' OCDE dans ses statistiques sur les prélèvements obligatoires , à condition qu' elles soient versées à des administrations publiques ou assimilées et bien qu' elles soient perçues dans un but déterminé ( la protection sociale ) et qu' elles soient assorties de contreparties indirectes ( prestations sociales ou couverture d' un risque ) .
La Révolution française mérite bien son nom en matière fiscale .
Le système fiscal français se retrouve actuellement controversé : avec le développement de l' Union Européenne et la mondialisation , la concurrence fiscale s' est fortement accrue .
Le taux global de prélèvement social et fiscal sur le salaire moyen atteignait en 2005 71,3 % du salaire brut , soit le taux le plus élevé de l' OCDE .
Selon l' Insee , la fiscalité réduit le niveau de vie du quintile supérieur de 22 % et augmente celui du quintile inférieur de 40 % .
