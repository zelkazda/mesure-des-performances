Les Helvètes jouèrent un rôle déterminant dans le commencement de la Guerre des Gaules en entamant une migration forcée vers la Saintonge après avoir brûlé leurs terres , sous le roi Orgétorix .
Quant aux autres Helvètes , ils retournèrent dans l' actuel Plateau suisse , au nombre de 110 000 .
En -52 , les Helvètes envoyèrent malgré tout un contingent de 8000 hommes à Vercingétorix , .
