Le Guatemala ( ou Guatémala ) est un pays d' Amérique centrale entouré par le Mexique , le Belize , la mer des Caraïbes , le Honduras , le Salvador et l' océan Pacifique .
Elle s' est développée dans presque tout le Guatemala actuel .
Le Guatemala s' émancipa de l' autorité espagnole en 1821 , faisant alors partie du Mexique .
Très vite , il se sépara du Mexique pour former avec d' autres régions les provinces unies d' Amérique centrale .
Une guerre éclata en 1838 et s' acheva en 1840 , à travers laquelle le Guatemala acquit son territoire actuel .
En juin 1954 , son successeur Jacobo Arbenz Guzmán instaure une taxe sur les exportations et décide une réforme agraire qui oblige entre autres la United Fruit Company à céder une partie importante de ses terres en friche .
Par ailleurs , les frères Dulles possèdent le plus important cabinet juridique de Wall Street et ont pour client United Fruit .
Le général met en action sa politique de la " terre brûlée " : ainsi , 440 villages seront complètement rasés , près de 200000 mayas seront massacrés ou encore jetés par hélicoptère dans l' Océan Pacifique .
40000 réfugiés fuient vers le Mexique .
La guérilla réagit en fondant un mouvement armé , l' URNG .
Óscar Rafael Berger Perdomo accède au pouvoir dès 2004 , à la tête d' un parti conservateur .
Le Guatemala est divisé en 22 départements :
Le Guatemala est un pays montagneux , sauf le long de côtes où l' on trouve des plaines .
Le grand lac Izabal est situé près de la côte donnant sur la mer des Caraïbes .
Le Guatemala possède 1687 km de frontières , dont :
Situé entre les 16 e et 13 e parallèles nord , le Guatemala a un régime tropical dans le Petén ( à l' ouest de Belize ) et dans les plaines côtières , plus larges en bordure de l' Atlantique que le long du Pacifique .
Dans les zones rurales moyennes , chaque famille souvent est contrainte d' envoyer un ou plusieurs de ses membres dans les fincas ( grandes exploitation agricole comme la United Fruit Company ) principalement situés sur les côtes où les terres sont plus fertiles afin de faire survivre la communauté .
L' ouragan Mitch , qui frappa le pays en 1998 , causa des dégâts , toutefois moins graves que dans les pays voisins .
Côté exportations , les premiers partenaires sont les États-Unis , le Salvador et le Honduras .
Plusieurs ruines mayas sont toujours visibles dans le pays à travers divers sites archéologiques , dont le temple du grand jaguar dans la célèbre cité de Tikal .
Guatemala a pour codes : et aussi
