Le canton d' Appenzell Rhodes-Intérieures est un canton du nord-est de la Suisse , le deuxième plus petit en superficie et le moins peuplé du pays .
Son chef-lieu est le village d' Appenzell .
Le canton d' Appenzell Rhodes-Intérieures tire son nom de l' ancien canton d' Appenzell , dont il est issu .
Le canton d' Appenzell était subdivisé en rhodes .
Lors de la séparation du canton en deux demi-cantons en 1597 , Appenzell Rhodes-Intérieures fut formé à partir des rhodes catholiques .
Le monastère de Saint-Gall influence grandement la vie de la population locale .
Herisau est mentionnée pour la première fois en 907 .
Le nom d' Appenzell l' est en 1071 .
En 1513 , Appenzell rejoint l' ancienne Confédération suisse comme 13 e canton .
En 1597 , le canton est divisé pour des régions religieuses en deux demi-cantons : Appenzell Rhodes-Extérieures est la partie protestante , Appenzell Rhodes-Intérieures la partie catholique .
Entre 1798 et 1803 , lors de la République helvétique , le canton est intégré dans le canton du Säntis .
Le canton d' Appenzell Rhodes-Intérieures est situé dans le nord-est de la Suisse et borde les cantons de Appenzell Rhodes-Extérieures et de Saint-Gall .
Les deux cantons d' Appenzell sont enclavés à l' intérieur de Saint-Gall .
Le canton culmine au Säntis , à 2 502 m d' altitude .
Avec 173 km² , Appenzell Rhodes-Intérieures est le deuxième plus petit canton de la Confédération suisse : seul le canton de Bâle-Ville est plus petit .
Le canton d' Appenzell Rhodes-Intérieures compte 15 471 habitants en 2007 , soit 0,2 % de la population totale de la Suisse ; parmi eux , 1 510 ( 9,8 % ) sont étrangers .
Il s' agit du canton le moins peuplé de Suisse .
Les citoyens du canton se réunissent à cette fin le dernier dimanche d' avril sur une place d' Appenzell .
Le canton d' Appenzell Rhodes-Intérieures est organisé en six districts et non pas en communes , même si les fonctions de ces districts sont apparentées à celles des communes des autres cantons .
L' appenzeller est un fromage répandu en Suisse .
