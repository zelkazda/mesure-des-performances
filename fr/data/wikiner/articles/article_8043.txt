Cet article présente la liste des empereurs romains depuis Auguste jusqu' à la déposition de Romulus Augustule .
Le mot latin imperator , d' où dérive " empereur " , ne désignait à l' origine qu' une qualité militaire ( celle de général victorieux ) parmi tous les titres et pouvoirs accumulés par Auguste .
Contrairement à ce que l' on pourrait penser , Jules César ne fait pas partie de la liste canonique des empereurs romains [ réf. nécessaire ] .
C' est Auguste qui ouvre cette liste , pour plusieurs raisons .
Théodose I er est le dernier empereur à diriger effectivement la totalité de l' Empire romain .
L' Empire romain d' Occident disparaît en 476 lorsque son dernier empereur , Romulus Augustule , est déposé par Odoacre .
