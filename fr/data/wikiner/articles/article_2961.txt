Pierre Cardo devient maire de Chanteloup-les-Vignes lors des élections municipales de 1983 , en l' emportant sur la liste du maire sortant socialiste .
Il est constamment réélu jusqu' à sa démission , le 8 février 2009 , pour se consacrer à son mandat de président de la communauté d' agglomération des Deux Rives de la Seine .
Son suppléant , Arnaud Richard , lui succède à l' Assemblée nationale .
