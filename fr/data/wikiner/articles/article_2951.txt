L ' Organisation européenne pour la recherche nucléaire ( nom officiel ) , aussi appelé laboratoire européen pour la physique des particules , plus connue sous l' acronyme CERN ( acronyme de l' organe provisoire institué en 1952 , le Conseil européen pour la recherche nucléaire ) , est le plus grand centre de physique des particules du monde .
Il est situé à quelques kilomètres de Genève , en Suisse , près de la frontière franco-suisse , sur la commune de Meyrin ( canton de Genève ) .
Les anneaux des accélérateurs s' étendent sous les communes françaises de Saint-Genis-Pouilly et Ferney-Voltaire ( département de l' Ain ) .
Au lendemain de la Seconde Guerre mondiale , la recherche européenne en physique est quasi inexistante , alors qu' elle était au sommet de sa gloire quelques années auparavant .
C' est lors d' une réunion à Amsterdam que le site où les installations du CERN seront implantées est choisi : ce sera en Suisse , sur la commune de Meyrin , située contre la frontière franco-suisse , près de Genève .
En 1965 , le gouvernement français accorde le droit au CERN d' agrandir son domaine sur le sol français .
La même année , la construction des anneaux de stockage à intersections ( ISR ) est approuvée , leur mise en service est prévue pour 1971 .
En 1967 , un accord est passé avec la France et l' Allemagne pour la construction d' une chambre à bulles à hydrogène .
En 1971 , un second laboratoire est construit pour y placer le Super Synchrotron à Protons ( SPS ) de 7 kilomètres de circonférence .
Ce n' est qu' avec le LHC , grand collisionneur de hadrons , mis en service le 10 septembre 2008 et qui réutilise son tunnel , qu' il est détrôné .
En octobre 1984 , Carlo Rubbia et Simon van der Meer reçoivent le Prix Nobel de physique pour leur découverte concernant la force électrofaible .
En 1989 -- 1990 , Tim Berners-Lee , rejoint par Robert Cailliau , conçoivent et développent un système d' information hypertexte , le World Wide Web .
En 1992 , Georges Charpak reçoit le Prix Nobel de physique pour des travaux réalisés au CERN en 1968 ( mise au point de la chambre proportionnelle multifils ) .
En 1994 , la construction du Large Hadron Collider ( LHC ou grand collisionneur de hadrons en français ) est approuvée .
Il est suivi en 1997 par les États-Unis .
En mai 2001 , le début du démontage du LEP commence , afin de laisser son tunnel libre pour le LHC , qui envoie son premier faisceau le 10 septembre 2008 .
Le 10 septembre 2008 , mise en service du Large Hadron Collider ( LHC ) , le plus important accélérateur de particules au monde construit à ce jour .
Le 20 novembre 2009 , les réparations sur le LHC sont terminées et les essais reprennent progressivement .
Dès lors , il est prévu de faire fonctionner le LHC pendant une période presque ininterrompu de 18 à 24 mois , de manière à redécouvrir les particules du modèle standard , de manière à valider les différents détecteurs constituant le LHC .
Le CERN n' utilise pas seulement un accélérateur de particules pour étudier la structure de la matière , mais toute une chaîne d' accélérateurs .
La plus puissante installation du CERN est le Large Hadron Collider ( LHC ) , qui a été mis en service le 10 septembre 2008 ( initialement prévu en novembre 2007 ) .
Le LHC se trouve tout au bout de la chaîne d' accélérateurs .
Chaque installation du CERN dispose de un ou plusieurs halls d' expérimentation , disponibles pour les expériences .
Bien que le LHC soit actuellement l' installation la plus importante ( et la plus médiatisée ) , il convient de ne pas oublier que d' autres équipements et travaux de recherche sont présents au CERN .
Un instrument destiné à détecter d' hypothétiques axions en provenance du Soleil .
Si les axions existent il est probable qu' ils soient présent au centre de notre étoile , c' est pour cette raison que CAST est un télescope qui est pointé en direction du Soleil grâce à une plateforme mobile .
Cette installation consiste à produire un faisceau de neutrinos qui est dirigé vers un laboratoire situé en Italie et distant de 732 kilomètres .
Pour cela , des protons accélérés par le SPS sont envoyés sur une cible en graphite .
L' ensemble est orienté de telle manière que le faisceau de neutrinos résultant soit dirigé vers un laboratoire italien installé dans le Gran Sasso , où il sera analysé par des instruments construits à cet effet .
Pour cela elle utilise les protons accélérés par le SPS .
L' objectif est d' atteindre une énergie comparable à celle obtenue au LHC , mais avec cette fois des collisions électron/positrons ( au lieu de collisions protons/protons ) , ce qui ouvrira de nouvelles perspectives .
Depuis son inauguration , le CERN a utilisé plusieurs accélérateurs , qui pour certains ont été démantelés pour en accueillir d' autres plus efficaces ou mieux adaptés aux recherches en cours .
Le CERN a une place importante dans le développement de certaines technologies informatiques .
La plus connue est certainement le World Wide Web ( à travers le développement du protocole HTTP et du langage HTML ) , qui est issue du projet ENQUIRE du début des années 1980 , développé par Tim Berners-Lee et Robert Cailliau .
Ce n' est qu' en 1989 que le projet du World Wide Web voit le jour , toujours développé par ces deux personnes et aidées par plusieurs autres .
L' objectif du World Wide Web était de faciliter l' échange d' informations entre les chercheurs des équipes internationales menant leurs expériences au CERN .
Le premier site web est mis en service en 1991 , et le 30 avril 1993 , le CERN annonce que le World Wide Web pourra être utilisé par tout le monde sans restriction .
Le CERN a également participé à l' introduction des technologies liées à Internet en Europe , avec la mise en service de deux routeurs Cisco au CERN en 1987 , qui sont vraisemblablement les premiers à être introduits sur le continent européen .
Enabling Grids for e-Science ( EGEE ) est le projet le plus avancé actuellement et aura pour but notamment de traiter les données générées par les expériences du LHC .
En janvier 2003 , une collaboration avec des entreprises privées du secteur de l' informatique , comme Hewlett-Packard , Intel ou encore Oracle a été mise en place à travers le projet openlab .
Voici la liste des directeurs généraux depuis la création du CERN :
Le CERN emploie un peu moins de 3000 personnes à plein temps .
En outre , il accueille environ 6500 scientifiques ( représentant 500 universités et plus de 80 nations , soit près de la moitié de la communauté mondiale dans ce domaine ) qui se succèdent pour effectuer leurs expériences au CERN .
