Le canton de Saint-Joseph n' a pas d' homonyme exact .
L' arrondissement de Saint-Pierre a un homonyme exact , dans le département de la Réunion .
Toutefois , les cantons incluant " Saint-Pierre " dans leur nom ne sont pas homonymes : ceux de la Réunion sont numérotés , tandis que celui de la Martinique est appelé " Saint-Pierre " , sans complément .
L' arrondissement de La Trinité n' a pas d' homonyme exact ( seulement un homonyme partiel dans le département du Morbihan ) mais sa commune chef-lieu a des homonymes exacts dans les départements des Alpes-Maritimes , de l' Eure , de la Manche et de la Savoie .
