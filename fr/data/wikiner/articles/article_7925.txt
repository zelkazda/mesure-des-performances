De Gaulle arrive à Londres le 17 juin 1940 avec l' intention de négocier avec les Britanniques , alliés de la France , la poursuite de la guerre , après avoir exposé son plan à Paul Reynaud .
Churchill donne son accord de principe et met à disposition la BBC .
Dans la soirée du 17 , l' écho du discours du maréchal Pétain , nouveau chef du gouvernement français , parvient à Londres .
De Gaulle lit son discours sur les antennes de la BBC à 18 heures , heure locale , le 18 juin 1940 .
Car " cette guerre est une guerre mondiale " et la France pourra s' appuyer sur la force industrielle de ses alliés et notamment celle des États-Unis .
L' appel du 18 Juin marque néanmoins le début de la France libre qui , formée uniquement de volontaires ( au début très peu nombreux ) , poursuit le combat sur terre , sur mer et dans les airs auprès des Britanniques et représente , face au régime de Vichy , la France qui se bat .
Plusieurs précisions sont à apporter au sujet de l' appel du 18 Juin 1940 :
L' exactitude du dossier remis à l' Unesco à cette occasion est néanmoins contestée par l' historien François Delpla .
Le 10 mars 2006 , le 18 juin a été institué par décret journée nationale non chômée " commémorative de l' appel historique du général de Gaulle à refuser la défaite et à poursuivre le combat contre l' ennemi " .
Plusieurs timbres commémoratifs ont été émis par La Poste :
De même , plusieurs pièces commémoratives ont été frappées par La Monnaie de Paris :
