La batucada est un genre de musique avec des percussions traditionnelles du Brésil dont les formules rythmiques en font un sous-genre de la samba .
Par extension , on utilise le terme de batucada , en France , pour désigner un groupe de musiciens pratiquant ce genre musical .
La batucada est née a Rio de Janeiro .
La batterie de percussions est au cœur des écoles de samba de Rio de Janeiro et assure la partie rythmique de leurs compositions musicales ( sambas de enredo ) .
La batucada est née d' un mélange de trois cultures : africaine , portugaise et indienne , qui ont donné au Brésil une identité culturelle unique , notamment grâce à l' apport des anciens esclaves noirs africains .
C' est cependant à Rio de Janeiro que sa présence est la plus importante vu le nombre et la valeur des écoles de samba qui concourent pendant les défilés du carnaval .
