Ce petit bourg est situé en Pays d' Auge , pays des pommiers , des maisons à colombages et des chevaux , à une douzaine de kilomètres de Lisieux et à une vingtaine de kilomètres des plages de la Côte Fleurie ( Honfleur , Trouville , Deauville ) .
Moyaux dispose d' une infrastructure qui permet de passer un moment de détente et de dépaysement à la campagne , mais à la porte d' une ville de 25000 habitants ( Lisieux ) , la mer est à 20 minutes , le pont de Normandie à environ 30 minutes .
Couvrant 1623 hectares , son territoire est le plus étendu du canton de Lisieux-1 .
À la création des cantons , Moyaux est chef-lieu de canton .
Moyaux a compté jusqu' à 1400 habitants en 1821 .
Elle est , après Lisieux , la commune la plus peuplée du canton .
Malsfeld ( Allemagne ) depuis 1984 .
