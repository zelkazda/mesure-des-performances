La société a son siège à Palo Alto dans la Silicon Valley en Californie .
Son chiffre d' affaires annuel est de 118 milliards USD en 2008 , et HP est le premier constructeur d' ordinateurs au monde , devant IBM et Dell .
HP produit environ la moitié des imprimantes dans le monde qui représentent environ le quart de son chiffre d' affaires .
Le 1 er janvier 1939 , la société Hewlett Packard est fondée à Palo Alto par deux grands amis tous les deux ingénieurs en électronique de l' université Stanford promotion 1934 , William Hewlett et David Packard , à quelques kilomètres de San Francisco en Californie dans ce qui ne s' appelle pas encore la Silicon Valley avec 585 dollars .
En fin d' année 1939 , une demi-douzaine de nouveaux produits d' électronique de mesure sont commercialisés avec entre autres un analyseur d' ondes à gros succès commercial qui fait de Hewlett Packard un synonyme de sérieux , de qualité et de fiabilité .
En 1972 , Hewlett Packard invente la calculatrice scientifique de poche avec son premier modèle , la HP35 scientifique utilisant la notation polonaise inverse .
En 1978 , pour faciliter la mise au point de ces matériels et permettre leur évolutivité , le langage graphique HP-GL est créé .
En 1978 , après 40 ans de carrière , William Hewlett et David Packard prennent leur retraite et passent le relais à John A. Young en conservant des fonctions honorifiques dans le groupe .
En 1985 , Hewlett Packard commercialise une station de travail ( trans ) portable sous Unix .
En mars 1986 , Hewlett-Packard enregistre le domaine " hp.com " ; c' est alors la neuvième société au monde à posséder un domaine internet , ce qui lui permet de choisir une adresse internet très courte .
En 1988 , Hewlett Packard commercialise sa gamme de station de travail + serveur multi utilisateurs HP 9000 à base de microprocesseur RISC sous Unix concurrent de Sun Microsystems , Apollo et IBM AS400 …
En 1993 , HP se lance sur le marché grand public au détriment de la production spécifique .
De 1999 à 2005 , la PDG Carly Fiorina est chargée d' acheter et de faire fusionner Compaq avec Hewlett Packard ( mai 2002 ) et de restructurer le groupe en réduisant l' effectif de 15 000 employés ( 10 % de l' effectif total ) .
En juillet 2005 , le nouveau PDG Mark Hurd poursuit la tâche de restructuration et d' optimisation du groupe par réduction des effectifs en supprimant 10 % des emplois .
Le 28 avril 2010 , HP rachète la société Palm pour 1,2 milliard de dollars .
Le 1 er juin 2010 , HP annonce un licenciement qui s' effectuera sur plusieurs années et conduira à la suppression de 9000 postes .
HP mise sur des centres de données automatisés afin de " réinvestir dans la croissance et améliorer la valeur pour les actionnaires " .
