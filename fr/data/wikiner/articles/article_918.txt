C' est la deuxième plus grande région de France ( derrière la Guyane ) avec huit départements et plus de 45000 km² .
Sa plus grande ville est Toulouse qui est également sa préfecture .
La région Midi-Pyrénées est située dans le sud-ouest de la France .
Sa superficie de 45348 km 2 est plus grande que celle de pays tels que la Belgique ou la Suisse .
La limite sud est constituée par la frontière avec l' Espagne et la principauté d' Andorre .
Le Midi-Pyrénées est limitrophe avec quatre régions françaises : l' Aquitaine à l' ouest , le Limousin au nord , l' Auvergne au nord-est et le Languedoc-Roussillon à l' est .
Le Midi-Pyrénées compte huit départements : Lot , Aveyron , Tarn , Haute-Garonne , Ariège , Hautes-Pyrénées , Gers et Tarn-et-Garonne .
Son chef-lieu est Toulouse , qui est aussi le chef-lieu du département de la Haute-Garonne .
La Garonne est le fleuve principal avec ses cinq affluents : le Gers , l' Ariège , le Lot , la Save et le Tarn .
Il y a deux massifs montagneux importants en Midi-Pyrénées : les Pyrénées au sud et le Massif central au nord-est de la région .
L' économie régionale est très largement dominée par le puissant pôle urbain de Toulouse .
L' activité principale de Toulouse est l' industrie aéronautique .
Airbus , premier constructeur mondial d' avions commerciaux , emploie à lui seul environ 20000 personnes dans la région , et à peu prés le même nombre de personnes en sous-traitance .
Le siège de Météo France , le CNES , Astrium , et EADS en font également un pôle spatial européen .
Toulouse est la troisième ville étudiante ( en nombre d' étudiants ) après Paris et Lyon .
L' agglomération de Tarbes , la deuxième de la région , est également un pôle industriel ( GIAT , centre de démantèlement d' avion ... ) .
L' agglomération d' Albi , la deuxième de la région , prend également une part croissante dans l' économie régionale .
Les laboratoires Fabre génèrent également de nombreux emplois dans la région Midi-Pyrénées , notamment à Castres .
On trouve les vignobles de Fronton , de Gaillac et de Cahors .
Le tourisme urbain à Toulouse et dans d' autres villes , mais également le tourisme vert ( Quercy , Lot , Comminges ) croissent dans la région .
Enfin les nombreuses stations de sports d' hiver attirent aussi de nombreux touristes dans les Pyrénées .
La région Midi-Pyrénées compte 2787000 habitants en 2007 dont :
Le département connaissant la plus forte croissance est , sans surprise , la Haute-Garonne .
L' Ariège , le Lot et le Tarn-et-Garonne connaissent une légère augmentation de la population .
Celle du Tarn connaît également une hausse remarquable .
Enfin , la population augmente moins dans les départements de l' Aveyron , du Gers et des Hautes-Pyrénées mais ces départements affichent un solde migratoire positif .
Albi :
Auch :
L' Isle-Jourdain :
Éauze :
Figeac :
Montauban :
Rodez :
Saint-Céré :
Saint-Gaudens :
Toulouse : Nombreuses galeries d' art
Blagnac :
Rodez :
Tarbes :
Toulouse :
Marciac :
