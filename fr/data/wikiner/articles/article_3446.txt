L ' Indre est une rivière du centre de la France , qui coule dans les départements du Cher , de l' Indre et d' Indre-et-Loire .
C' est un affluent direct de la Loire en rive gauche .
La racine inn se reconnaît dans le nom de la rivière helvético-austro-allemande Inn .
Son débit a été observé durant une période de 15 ans ( 1966-1980 ) , à Lignières-de-Touraine , localité située peu avant son confluent avec la Loire .
Le débit moyen interannuel ou module de la rivière à Lignières-de-Touraine est de 18,7 m³ par seconde .
Elles sont cependant nettement moindres que celles qui affectents les affluents de plaine de la partie occidentales du bassin de la Loire , celles-ci coulant sur le vieux socle primaire armoricain , imperméable .
Le débit instantané maximal enregistré à Lignières-de-Touraine durant cette période , a été de 214 m³ par seconde le 1 er février 1977 , tandis que le débit journalier maximal enregistré était de 209 m³ par seconde le 22 février de la même année .
La lame d' eau écoulée dans son bassin versant est de 181 millimètres annuellement , ce qui est nettement inférieur à la moyenne de la France , tous bassins confondus , ainsi d' ailleurs qu' à la moyenne du bassin de la Loire ( 244 millimètres ) , et même nettement moins élevé que la moyenne du bassin du Cher ( 223 millimètres à Tours ) .
