En Amérique du Sud et surtout au Brésil , certaines essences disponibles à la pompe peuvent comporter jusqu' à 20 % d' éthanol et portent le nom d' ethanol blend .
Le procédé le plus élaboré dans ce domaine est actuellement celui développé par l' entreprise canadienne Iogen Corporation .
