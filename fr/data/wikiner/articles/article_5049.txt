Parmi les plus appréciées de ces épices , on compte feuilles de carry , tamarin , coriandre , gingembre , ail , piments , cannelle , clous de girofle , cardamone , cumin , noix muscade , noix de coco , et bien d' autres encore , que fournit en particulier le Kerala voisin .
Les musulmans moghols , en envahissant le nord de l' Inde , y ont apporté leur recettes et modes de cuisson , en particulier les kebabs et la cuisson au tandoor ou tandoori : les naans , le poulet tandoori , etc .
Ainsi , les villes " comptoirs " de pays comme la France et le Portugal ont cuisiné le porc et le bœuf , viandes interdites dans les autres régions .
Par exemple , le porc qui arrivait du Portugal à Goa dans des tonneaux de vinaigre a donné le porc vindaloo .
D' une manière générale , la cuisine de Goa est moins piquante et l' influence occidentale est assez présente .
A part dans certaines régions du pays , la cuisine indienne n' a pas d' entrées ni de plats servis comme en France .
Il y a une très grande variété de pains , de galettes et du riz selon les régions ou les communautés en Inde .
En voici quelques uns , parmi les plus connus en France :
Les plus connus en France sont :
Parmi les boissons les plus connues en France sont : et moins connue :
