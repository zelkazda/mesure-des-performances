Sa souveraineté s' étendait du lac Léman ( annexion du pays de Vaud en 1536 ) à l' Argovie ( 1415 ) .
Membre de la Confédération suisse depuis 1353 , Berne accède , après la Réforme ( 1528 ) , au rang de puissance européenne .
Le canton de Berne se situe dans le nord-ouest de la Suisse , à la frontière entre la partie francophone et germanophone du pays .
Au nord avec le canton du Jura , à l' ouest avec les cantons de Vaud , Neuchâtel , et Fribourg , au sud avec le Valais et à l' est avec Soleure , Argovie , Lucerne , Obwald et Uri .
La partie francophone du canton est située au nord et se nomme le Jura bernois .
Son point le plus haut est le Finsteraarhorn à 4274 m et les autres sommets majeurs sont l' Aletschhorn à 4195 m ainsi que la Jungfrau à 4158 m .
Son point le plus bas se situe au niveau de l' Aar près de Wynau ( 401,5 m ) .
Le canton de Berne compte 962 982 habitants en 2007 , soit 12,7 % de la population totale de la Suisse ; parmi eux , 119 930 ( 12,5 % ) sont étrangers .
Seul le canton de Zurich est plus peuplé .
Un statut particulier est accordé aux 3 districts francophones du Jura bernois ainsi qu' à la population francophone de la région de Bienne .
Les sièges francophones garantis au Jura bernois restent inchangés par la nouvelle loi .
Il est composé de 7 membres ( les conseillers d' état ) élus au suffrage majoritaire par le corps électoral , dont un siège est garanti par la constitution cantonale à la région francophone du Jura bernois .
Le canton de Berne compte quatorze villes de plus de 10 000 habitants au 1 er janvier 2007 :
