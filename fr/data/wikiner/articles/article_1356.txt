Il fut aussi l' un des favoris d' Henri IV , du moins jusqu' à la conversion de celui-ci .
Envoyé à Genève en 1565 , Agrippa d' Aubigné y poursuivit ses études sous la direction de Théodore de Bèze .
D' Aubigné participant aux batailles , comme aux pourparlers de paix , il était , à la suite d' un duel , absent de Paris durant les massacres de 1572 mais il en garda une rancune tenace à la monarchie .
C' est à cette époque qu' il se lie avec le jeune roi de Navarre , qui le nomma son écuyer au mois d' août 1573 .
On ignore si , comme lui , d' Aubigné a feint de se convertir au catholicisme .
Cette amitié entre le roi et le poète dura plusieurs années ; Henri IV le nomma ainsi maréchal de camp en 1586 , puis gouverneur d' Oléron et de Maillezais , que d' Aubigné avait conquis par les armes en 1589 ; puis vice-amiral de Guyenne et de Bretagne .
Mais les divergences politiques et religieuses finissent par séparer les deux hommes , qui ne se doutaient pas que leurs petits-enfants respectifs , Louis XIV et Françoise d' Aubigné , se marieraient en 1683 .
En 1577 , d' Aubigné est grièvement blessé à Casteljaloux .
Après l' assassinat du duc de Guise en 1588 , d' Aubigné reprit part aux combats politiques et militaires de son temps .
Comme de nombreux protestants , d' Aubigné ressent l' abjuration d' Henri IV , en 1593 , comme une trahison , d' autant plus qu' il était l' un de ceux qui s' étaient le plus battus pour amener Henri au trône .
Il est peu à peu écarté de la cour , dont il se retira définitivement après l' assassinat d' Henri IV en 1610 .
D' Aubigné se retira alors à Genève , où est publié l' essentiel de ses œuvres .
Mais d' Aubigné n' est pas l' auteur d' une seule œuvre .
On cite d' Aubigné un trait semblable à celui de Régulus : fait prisonnier par Saint-Luc pendant la guerre civile en 1585 , il obtint sur parole d' aller passer quelques jours à La Rochelle ; dans l' intervalle , il apprit que Catherine de Médicis avait donné l' ordre de sa mort ; il n' en revint pas moins au jour dit .
