William Gibson est né en 1948 à Conway , près de Myrtle Beach en Caroline du Sud .
William Gibson vit ce déménagement comme un bond en arrière dans le passé .
Un nouveau traumatisme que Gibson surmonte , une fois encore , par la lecture .
Gibson a 18 ans , et voit ainsi se matérialiser l' une de ses pires angoisses .
En 1968 , il s' enfuit au Canada pour éviter d' être envoyé au Viêt Nam et s' installe en 1972 à Vancouver .
Gibson , comme les autres , commence à écrire des nouvelles qui attirent l' attention .
Dans les années 80 ses fictions se développent sur le mode du film noir ; des nouvelles publiées dans le magazine Omni commencèrent à esquisser les thèmes qu' il développera dans son premier roman , Neuromancien .
C' est Gibson , avec Neuromancien , qui décroche le premier un immense succès littéraire .
Les trois romans de cette seconde trilogie sont : Lumière virtuelle , Idoru et All Tomorrow 's Parties .
Plus récemment , William Gibson s' est quelque peu éloigné du genre des dystopies fictionnelles qui le rendirent célèbre pour davantage privilégier un style d' écriture plus réaliste , troquant les sauts narratifs caractéristiques de sa première manière contre un flux d' écriture plus continu .
Comme William Gibson l' avait dit dans son blog , la disquette devait se " manger elle-même " après avoir été lue .
William Gibson commença à rédiger son blog à partir de 2003 qui resta actif jusqu' en 2005 , avec une seule grosse coupure .
Gibson écrivit également quelques éléments d' anticipation pour Alien³ dont certains furent intégrés au film du même nom .
Il fit par ailleurs une apparition à l' écran dans la mini-série Wild Palms , une série largement influencée par l' œuvre de Gibson et d' autres auteurs cyberpunk .
Son roman l ' Identification des schémas est actuellement en cours d' adaptation au cinéma par Peter Weir et devrait sortir en 2008 .
