René Dumont est né à Cambrai , le 13 mars 1904 .
Entré brillamment en 1922 à l' Institut national agronomique ( INA ) , il en sort avec un diplôme d' ingénieur agronome en poche .
René Dumont a commencé sa carrière en soutenant le modèle agricole de l' époque , basé sur l' utilisation des fertilisants chimiques et sur le machinisme agricole .
Il fut un expert aux Nations unies et à la FAO et est l' auteur d' une trentaine d' ouvrages .
René Dumont considérait que le développement n' était pas un problème d' argent , d' engrais ou de semences , mais plutôt la résultante d' un équilibre entre les trois .
Brice Lalonde fut son directeur de campagne électorale .
La politique écologique française fondée par Dumont est contre la guerre , contre le capitalisme , pour la solidarité entre les peuples et prenant en compte le monde sous-développé .
Certains voient en René Dumont le père spirituel du parti des Verts , Les Verts dont il soutint régulièrement les candidats à la présidentielle et qui après sa mort créèrent une fondation portant son nom , l' ont toujours considéré comme l' un des leurs .
C' était sûrement un humaniste de gauche , mais surtout un mondialiste , et certainement un altermondialiste ; il était membre fondateur d' Attac .
