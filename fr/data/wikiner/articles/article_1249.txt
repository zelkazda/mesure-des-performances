Le sport dans lequel les Mexicains ont connu un relatif succès international est le football .
Dans le fútbol , les équipes les plus connues sont les diablos rojos de Toluca , Club América et les Chivas de Guadalajara .
Les joueurs les plus célèbres sont Hugo Sanchez , Claudio Suárez , Luis Hernández , Francisco Palencia , Cuauhtémoc Blanco , Jared Borgetti , Rafael Márquez , Ramón Ramírez , Jorge Campos , Omar Bravo , Ramón Morales , Oswaldo Sánchez , Óscar Pérez et Jesús Arellano .
Récemment , le pays a produit des champions , tel que Marco Antonio Barrera , Erik Morales , Julio César Chávez , Rafael Marquez et Juan Manuel Marquez .
Il existe au Mexique un grand nombre de sports traditionnels .
