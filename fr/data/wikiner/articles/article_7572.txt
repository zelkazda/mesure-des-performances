Le Perche , province historique française , est un ancien comté .
Il s' étend sur les départements de l' Orne , d' Eure-et-Loir , de la Sarthe et du Loir-et-Cher .
Un parc naturel régional du Perche a été créé en 1998 .
Sa capitale fut d' abord Mortagne-au-Perche , avant de devenir Nogent-le-Rotrou par la suite .
Le Perche désigne maintenant un parc naturel régional situé à l' ouest du Bassin parisien .
En dépit du démantèlement de la province à la Révolution , l' identité locale perdure .
Ernest Nègre explique ce nom par le latin pertica ( terra ) , " comme ensemble du territoire partagé , à la perche , entre les vétérans d' une colonia " .
La forêt d' Othe dans l' Yonne .
Le comté se constitua par la fusion du comté de Mortagne , du vicomté de Châteaudun et la seigneurie de Nogent-le-Rotrou .
Le principal ministre d' Henri IV , Sully , est marquis de Nogent-le-Rotrou , où il est enterré .
À partir de 1634 un mouvement d' émigration percheronne vers la Nouvelle-France s' amorce , grâce au pouvoir de persuasion de Robert Giffard , un apothicaire de Tourouvre .
Quelques-uns vont revenir au pays , mais la grande majorité choisit de s' établir sur les rives du fleuve Saint-Laurent pour y défricher et faire prospérer les terres nouvelles .
Leur descendance est aujourd'hui estimée à 1500000 personnes au Canada , en dehors du Québec .
La famille qui compte le plus de descendants est la famille Tremblay qui remonte entièrement à un seul ancêtre Pierre Tremblay , natif de Randonnai .
Le Perche conserve une forte identité régionale en dépit de son morcellement en départements à la Révolution entre l' Orne ( Mortagne-au-Perche ) , l' Eure-et-Loir ( Nogent-le-Rotrou ) , la Sarthe ( Montmirail ) et le Loir-et-Cher .
Les clivages politiques toujours d' actualité se forment à cette période : le Perche ornais , longtemps bonapartiste et clérical , reste plutôt conservateur , tandis que le Perche d' Eure-et-Loir a une tradition radicale .
