Sa capitale est Castries .
L' île est ensuite disputée entre la France et le Royaume-Uni , lequel en obtient le contrôle complet en 1814 .
Le pays devient indépendant le 22 février 1979 , en tant que membre du Commonwealth .
Il adhère à l' Organisation des États de la Caraïbe orientale en 1981 .
Sainte-Lucie est inscrite sur la liste noire des paradis fiscaux non coopératifs par la France en février 2010 .
Sainte-Lucie est membre de la communauté des Caraïbes , de l' organisation des États de la Caraïbe orientale et de l' organisation internationale de la Francophonie .
Elle est bordée à l' ouest par la mer des Caraïbes et à l' est par l' océan Atlantique à proprement parler .
Sainte-Lucie est une île volcanique et culmine à 950 m d' altitude au mont Gimie .
La capitale de Sainte-Lucie est Castries , où habite le tiers de la population du pays .
Sainte-Lucie est subdivisée en 11 districts ( quarters en anglais ) :
Sainte-Lucie est également divisée en 17 districts électoraux pour les élections législatives .
En 2008 , Sainte-Lucie compte 172884 habitants .
L' émigration de Sainte-Lucie est principalement dirigée vers les pays anglophones .
Au Royaume-Uni , près de 10000 citoyens britanniques sont nés à Sainte-Lucie et plus de 30000 sont d' origine de cette île .
Aux États-Unis résident près de 14000 personnes d' origine de Sainte-Lucie .
Sainte-Lucie a pour codes :
