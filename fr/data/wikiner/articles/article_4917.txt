Il s' agit alors des livres de l' Ancien Testament et du Nouveau Testament qui sont deux expressions nouvelles signalées :
Auparavant , le concept d' une liste close ( au sens de complète et définitive ) des livres repris dans la Septante est inconcevable .
En outre , ils " font dire " des choses de plus en plus étranges à la Septante .
Les controverses rabbiniques , enregistrées dans le Talmud montrent des discussions qui , sous prétexte d' exégèse imaginative , présentent des opinions sur la pertinence de tel ou tel texte .
Ces polémiques jouent un rôle non négligeable tant dans l' évolution de la pensée rabbinique autour de Gamaliel II que dans l' accouchement du système chrétien .
Il a travaillé essentiellement sur la Septante , c' est-à-dire sur la Bible en grec .
Vers 200 émerge l' idée d' un catalogue des livres composant le Nouveau Testament .
L' influence de Marcion fut déterminante dans la constitution d' un canon .
Le contenu du fragment ruine cette hypothèse sur la construction du Nouveau Testament .
Pour le milieu de Gamaliel II , les chrétiens apparaissent comme des sectaires et des hérétiques .
Leur interprétation de la Septante est mise en cause .
Dans un temps où triomphe l' idée de Plotin que la vérité est une et que le dissensus est haïssable , on ne peut concevoir que chacun des évangiles réputés canoniques avait vocation à se suffire à lui-même et non à compléter les autres .
Chacun d' eux , du point de vue de leurs auteurs , se proposait de devenir le seul témoignage valide de la vie et de l' enseignement de Jésus qui supplanterait tous les autres .
Celle de Tatien perdurera dans le corpus canonique de l' Église syriaque .
La comparaison avec les citations de l' Ancien Testament montre moins de divergences avec les textes de la Septante .
D' autres évangiles ont été écrits qui transmettent d' autres traditions sur les dits et les faits de Jésus .
Des ouvrages comme : conservent des traditions sur Jésus qui ne doivent rien aux évangiles canoniques .
Tatien et Marcion , par le choix de leurs sources et leur entreprise de réécriture témoignent de la résistance à accepter plusieurs témoignages divergents .
Le rôle de Marcion fut décisif , ne serait-ce que dans l' idée de clore une liste pour la dresser contre les autres sources , d' un corpus s' opposant à d' autres corpus disponibles .
Outre les réticences à la réception plurielle d' un témoignage tétramorphe ( néologisme d' Irénée !
Quoique la canonisation d' un texte contemporain ne soit pas interdite , comme le montre celle du Diatessaron de Tatien dans l' Église syriaque , il semble que l' ancienneté attribuée aux textes soit un sésame .
Marcion en connaissait 10 , les autres listes en donnent 13 , voire 14 .
