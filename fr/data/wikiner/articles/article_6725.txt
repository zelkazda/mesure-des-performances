La distinction entre ces deux types de nébuleuses a été faite par Edwin Hubble en 1922 .
Les nébuleuses par réflexion et les nébuleuses en émission sont souvent vues ensemble , comme par exemple la nébuleuse d' Orion et sont parfois rassemblées en un seul type : les nébuleuses diffuses .
Parmi les plus belles nébuleuses de réflexion ont trouve l' entourage des étoiles des Pléiades .
Une nébuleuse par réflexion bleue peut également être vue dans le même secteur du ciel que la nébuleuse Trifide .
L' étoile géante Antarès , qui est très rouge , est entourée par une grande nébuleuse de réflexion rouge .
En 1922 , Edwin Hubble a publié le résultat de ses investigations sur les nébuleuse lumineuses .
Une partie de ce travail est la loi de luminosité de Hubble pour les nébuleuses de réflexion qui établit une relation entre la taille angulaire ( r ) de la nébuleuse et la magnitude apparente ( m ) de l' étoile associée : où k est une constante qui dépend de la sensibilité des instruments de mesure .
