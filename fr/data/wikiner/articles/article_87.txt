Cet ordinateur , conçu pour l' utilisation familiale , répondait au lancement des ZX Spectrum , Oric 1 et Commodore 64 par Sinclair , Oric Corporation et Commodore .
Son succès fut tel que plus d' une dizaine de magazines dédiés furent créés , dont le plus fameux , Amstrad Magazine .
C' est peu après le succès commercial du 464 qu' est apparu AMSDOS .
CP/M qui était antérieur au 464 a été transposé sur cet ordinateur .
Le CPC ne possède pas de mode texte en tant que tel .
Le CPC standard possède une palette de 27 couleurs , constituées des trois teintes primaires ( rouge , vert , bleu ) auxquelles on applique les coefficients 0 ; 0,5 et 1 .
À l' origine le CPC était annoncé avec une palette de 32 couleurs .
Elles sont accessibles directement en Basic , mais n' ont aucun intérêt pratique .
Le CPC dispose de 4 modes graphiques de base utilisant 16 ko de mémoire , dont un non documenté :
