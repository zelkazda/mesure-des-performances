Les données sont tirées du CIA World Factbook millésime 2010 .
Les dépendances et les territoires dont l' indépendance n' est généralement pas reconnue sont indiqués en italique , sous le pays auquel ils sont généralement rattachés , sans prendre position sur le statut de ces territoires .
Les variantes concernant la dénomination des pays peuvent se trouver dans la liste des pays du monde .
