L' Université Princeton est présente dans cette ville depuis 1756 .
Bien que Princeton soit une ville universitaire , il y a dans le voisinage de la ville de nombreux autres centres d' intérêt importants qui enrichissent sa puissance économique .
Un autre facteur qui joue un rôle important dans l' indépendance de cette ville est sa situation géographique ; elle est équidistante de Philadelphie et de New-York .
À Princeton , on peut capter des émetteurs de radio et de télévision de ces deux villes .
Le film Q.I. de 1994 dans lequel jouent Meg Ryan , Tim Robbins et Walter Matthau qui incarne Albert Einstein , a aussi été tourné à Princeton .
