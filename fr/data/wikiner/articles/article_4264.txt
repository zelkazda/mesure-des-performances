Villers-Cotterêts est une ville située au sud du département de l' Aisne .
La commune est dominée à l' est par la forêt domaniale de Retz .
L' origine du nom Villers-Cotterêts serait villa sur la côte de Retz .
On peut dire sans crainte que la ville actuelle doit son origine à François Ier , car les fréquents voyages de ce roi , avec toute sa suite , amenèrent une foule de marchands , d' aubergistes et d' artisans qui s' établirent près du château dans des maisons élevées le long de la route , et il fallut bien loger et nourrir les ouvriers nombreux , occupés à la construction de l' édifice .
Des réunions fastueuses y sont organisées , des fêtes littéraires , avec Rabelais et Clément Marot .
Henri II succède à son père en 1547 et fait entreprendre au château d' importants travaux dirigés par Philibert Delorme .
C' est de la forêt de Villers-Cotterêts que partit l' offensive victorieuse du 18 juillet 1918 , marquant la fin d' un cauchemar qui avait duré plus de quatre années .
Au final , on observe une répartition générale de l' électorat , avec un MoDem et un FN , plus fort qu' au niveau national ; et une gauche et une droite qui réalisent globalement des scores à peu près identiques ; se neutralisant et offrant ainsi des résultats relativement proches .
