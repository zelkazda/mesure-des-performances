En 1985 , alors que ce canton est partagé en deux en raison de son importante population , il choisit de rester dans le nouveau canton de Chambéry-Est .
En 2008 , malgré la victoire au premier tour de Bernadette Laclais à la mairie de Chambéry et le basculement de l' autre canton renouvelable à gauche , il est réélu en 2008 avec 39 voix d' avance sur le candidat socialiste , son canton étant le seul de la ville à rester à droite .
Il est élu député pour la première fois en 1993 , puis réélu en 1997 , 2002 et 2007 dans la 3 e circonscription de la Savoie .
Il fait partie du groupe UMP .
Lors du décès de l' ancien président du RPR , il a souligné qu ' " il y a et il y aura toujours le souvenir et la volonté de perpétuer une tradition d' action politique motivée par les convictions , par l' intérêt national et appelant au courage dans la défense des idées face aux consensus faciles. "
Il siège également au sein du conseil d' orientation d' OSEO .
Parmi les députés , Michel Bouvard est apprécié " pour l' activité intense qu' il consacre au travail parlementaire et à la Savoie " , et " son assiduité et la pertinence de son travail , dans ses instances , ont souvent été saluées par ses collègues parlementaires . "
