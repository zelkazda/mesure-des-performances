Grâce au soutien français , le conclave qui s' ouvre à la mort de Clément XIV ( 1774 ) l' élit à la dignité pontificale .
Il choisit le nom de Pie VI en hommage à Pie V , pape de l' application du concile de Trente et de la bataille de Lépante .
Quelques temps plus tard , Pie VI doit affronter les événements de la Révolution française :
La réaction de Pie VI par rapport à la constitution civile du clergé n' est , aujourd'hui encore , pas élucidée .
Cette question fait l' objet de discussions de la part des historiens des religions ; il existe des archives sur ce sujet en France .
La France annexe Avignon et le Comtat Venaissin .
A la nouvelle de l' assassinat du général Duphot , le Directoire ordonne le 11 janvier 1798 l' occupation de Rome .
Gaspard Monge part le 6 février pour Rome .
Le pape Pie VI est contraint par la république française de renoncer à son pouvoir temporel et de se contenter de son pouvoir spirituel .
On l' oblige à quitter Rome sous deux jours .
Il est successivement emmené à Bologne , Parme , Turin , puis Briançon , Grenoble et enfin Valence ( France ) .
Malgré les bouleversements que connaissait alors la France , le pape octogénaire reçut de nombreuses et touchantes marques de respect , de compassion et de communion dans la foi de la part du peuple , tout au long de sa route , entre Briançon et Valence .
Le poète Paul Claudel le surnommera le père commun des fidèles .
C' est à Valence qu' il fut incarcéré par la République française , et qu' il mourut , épuisé , le 29 août 1799 à l' âge de 82 ans .
Son successeur fut Pie VII .
Pie VI , d' abord enseveli civilement à Valence , est enterré dans la basilique Saint-Pierre de Rome , son corps y ayant été ramené en triomphe le 17 février 1802 .
Le congrès de Vienne les rétablit dès 1814 .
