Dans la Genèse , Joseph est vendu comme esclave par ses frères à des marchands d' épices .
Dans le Cantique des cantiques , un poème biblique , le rédacteur compare sa bien-aimée à de nombreuses formes d' épices .
Puis la ville d' Alexandrie en Égypte devient le centre du commerce des épices grâce à son port .
L' Espagne et le Portugal souhaitaient contourner le quasi monopole exercé par Venise sur l' est de la Méditerranée .
En 1506 , il prend l' archipel de Socotra à l' entrée de la Mer Rouge et , en 1507 , Ormuz à l' entrée du Golfe Persique .
