Yang Liwei , né le 21 juin 1965 est le premier spationaute envoyé par la Chine ( les chinois utilisent le terme taïkonaute ) .
Ancien pilote de combat dans l' unité militaire d' aviation de l' Armée populaire de libération , il a le rang de lieutenant-colonel au moment de sa première mission .
