Le G8 ( pour " Groupe des huit " ) est un groupe de discussion et de partenariat économique de huit pays parmi les plus puissants économiquement du monde : États-Unis , Japon , Allemagne , France , Royaume-Uni , Italie , Canada et Russie .
Durant toute l' année , le G8 dispose d' un agenda chargé dans le pays hôte , où se rencontrent les ministres et chargés de missions dans chaque domaine ( économie et finances , défense et sécurité internationale , éducation , développement , etc .
Ces rencontres sont contestées par des mouvements altermondialistes qui remettent en cause la légitimité du G8 et l' accusent de vouloir " diriger le monde " , au mépris des autres pays , pour imposer une politique libérale .
Autrement dit , le G8 n' a pas de structure particulière , il ne répond pas à un protocole ou à une organisation : il s' agit d' une réunion de dirigeants , dans une " ambiance décontractée ... pour discuter des affaires du monde " .
Le G8 n' est pas une administration transnationale , à la différence d' institutions comme les organisations du système des Nations unies telles que l' ONU ou la Banque mondiale .
Chaque année , les sommets du G8 changent de pays dans cet ordre : France , États-Unis , Royaume-Uni , Russie , Allemagne , Japon , Italie , Canada .
Au cours des années précédentes , il n' y a pratiquement pas eu un seul sommet qui se soit déroulé sans heurts , que ce soit à Gênes ou à Évian .
À Gênes , un manifestant du nom de Carlo Giuliani a été abattu d' une balle dans la tête à bout portant par un policier peu préparé et manifestement paniqué , il est depuis devenu un symbole de la lutte altermondialiste .
Lors du sommet d' Évian , un activiste , suspendu par une corde à un pont autoroutier afin d' y accrocher une banderole a été précipité dans le vide après qu' un policier suisse a coupé la corde pour libérer le passage aux voitures .
De même , les protestations ont été fortes contre les rencontres du FMI à Prague ou à Berlin et contre celle de l' OMC à Seattle .
Ainsi , depuis les manifestations de Gênes en 2001 , les sommets du G8 ne se déroulent habituellement plus dans les métropoles , mais dans des espaces difficilement accessibles et facilement contrôlables .
Heiligendamm est situé à 20 km à l' ouest de Rostock et à 200 km de Berlin .
