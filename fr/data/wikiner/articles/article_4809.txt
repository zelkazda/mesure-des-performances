Zig et Puce est une série de bande dessinée créée en 1925 par Alain Saint-Ogan .
La série Zig et Puce fait ainsi , par hasard , ses débuts dans le n° 11 du 3 mai 1925 .
Zig et Puce sont les premiers héros d' expression française à s' exprimer par bulles ( les fameux phylactères ) de façon régulière .
Dans la même période , chansons et pièces de théâtre mettent en scène les personnages de la série Zig et Puce .
La série Zig et Puce sert de modèle à Hergé , jeune débutant , créateur de Tintin , qui vient rendre visite à Alain Saint-Ogan à Paris en 1931 pour lui demander conseil .
On peut reconnaître l' influence de Zig et Puce dans Quick et Flupke d' Hergé .
La série Zig et Puce connaît ainsi un grand succès jusqu' à son arrêt en 1956 .
La série est reprise , en accord avec Alain Saint-Ogan , par Greg , qui la modernise , entre 1963 et 1970 dans le Journal de Tintin .
