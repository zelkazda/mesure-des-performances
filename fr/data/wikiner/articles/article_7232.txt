La Chine développe les premières idées algébriques à l' origine des vecteurs .
Cette culture n' en reste pas là , Qin Jiushao ( 1202 -- 1261 ) généralise cette étude à des nombres différents des entiers ou rationnels .
Les Babyloniens connaissaient déjà la propriété algébrique de la diagonale d' un carré de côté de longueur un , à savoir que son carré est égal à deux .
Omar Khayyam ( 1048 -- 1131 ) cherche les solutions d' un problème purement algébrique : trouver les racines d' un polynôme du troisième degré .
Filippo Brunelleschi ( 1377 -- 1446 ) découvre les lois de la perspective , issues d' une projection centrale .
Ces résultats sont formalisés par Leon Battista Alberti ( 1404 -- 1472 ) .
Ainsi Piero della Francesca ( vers 1412 -- 1492 ) , auteur d' un traité sur la question , est à la fois peintre et mathématicien .
Giorgio Vasari ( 1511 -- 1574 ) indique , à propos de ses talents de géomètre " il ne fut inférieur à personne de son époque et peut-être de tout temps .
En 1604 , Galileo Galilei ( 1564 -- 1642 ) établit la loi de la chute des corps .
Pierre de Fermat ( 1601 -- 1665 ) , qui connaissait les écrits de Galilée , et René Descartes ( 1596 -- 1650 ) s' écrivent des lettres au sujet de la dioptrique ( la manière dont la lumière se réfléchit sur un miroir ) et à la réfraction ( la déviation d' un rayon lumineux quand il change de milieu , par exemple en passant de l' air à l' eau ) .
Ces résultats sont consignés dans un traité de Descartes .
Pour Descartes , calcul d' arithmétique signifie approximativement ce qui est maintenant appelé algèbre .
Isaac Newton ( 1643 -- 1727 ) développe la géométrie analytique et l' utilise en astronomie .
Ce terme apparait en français sous la plume de Pierre-Simon Laplace ( 1749 -- 1827 ) dans l' expression rayon vecteur , encore dans un contexte astronomique .
Bernard Bolzano ( 1781 -- 1848 ) publie un livre élémentaire contenant une construction axiomatique de la géométrie analogue à celle d' Euclide , fondée sur des points , droites et plans .
August Ferdinand Möbius ( 1790 -- 1868 ) apporte sa pierre à l' édifice en développant le système de coordonnées barycentriques .
Enfin , la formalisation encore actuellement enseignée , à partir des notions de bipoint et d' équipollence , est l' œuvre de Giusto Bellavitis ( 1803 -- 1880 ) .
William Rowan Hamilton ( 1805 -- 1865 ) remarque que les nombres complexes représentent un plan euclidien .
La géométrie euclidienne est la géométrie du plan ou de l' espace fondée sur les axiomes d' Euclide .
On ne trouve pas de vecteurs dans les éléments d' Euclide , mais les notions de point ou de parallélogramme , de l' approche esquissée ci-dessus y sont bien présentes .
David Hilbert a montré comment axiomatiser rigoureusement le plan ou l' espace affine de façon géométrique .
