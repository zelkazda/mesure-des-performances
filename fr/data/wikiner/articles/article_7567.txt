Le succès de leur assimilation a été si complet que peu de traces modernes demeurent à Palerme ou à Londres .
La colonisation viking en Normandie , tout comme la formation du duché normand , s' étale en fait sur plus d' un siècle car , dans les années 1020 , des bandes vikings viennent encore s' installer dans le duché sous le règne du duc Richard l' Irascible .
Le duché de Normandie se constitue surtout sous les successeurs de Rollon , du duc Guillaume Longue-Épée et c' est seulement au siècle suivant , sous le règne du duc Guillaume le Bâtard , que le pouvoir ducal est totalement affirmé ( à partir de 1060 environ ) , 150 ans après le traité de Saint-Clair-sur-Epte .
D' ailleurs , nombre de Vikings à avoir essaimé hors de leur sol natal le firent parce qu' ils en avaient été exilés .
La stricte punition du vol , illustrée par l' histoire du chêne auquel Rollon a suspendu un anneau d' or que personne ne songeait à voler , trouve également sa source dans le folklore danois .
Mais la persistance la plus marquée de l' usage scandinave dans les mœurs des Normands est sans conteste le mariage more danico , " à la danoise " , légalisant la bigamie .
Ce n' est qu' au bout de sept générations , précisément avec Guillaume le Conquérant , que les ducs de Normandie paraissent devenir monogames .
Les Normands étaient depuis longtemps en contact avec l' Angleterre .
En se livrant à la conquête de l' Angleterre , les Normands du duché de Normandie ne font que continuer la vague d' incursions norvégiennes en Angleterre .
Non seulement leurs semblables païens ravageaient déjà les côtes anglaises , mais ils occupaient la plupart des ports importants face à l' Angleterre à travers la Manche .
Cette proximité a produit des liens plus étroits encore avec le mariage de la fille du duc Richard II , Emma , au roi Ethelred II .
Son séjour en Normandie jusqu' en 1016 l' influença ainsi que ses fils .
Après la conquête de l' île par Knut II de Danemark , sa femme Emma resta en Normandie .
Lorsque Édouard le Confesseur revint finalement en Angleterre en 1041 , à l' invitation de son demi-frère Knut III , il avait été extrêmement " normannisé " .
Il engagea même une petite troupe de Normands pour établir et former une force de cavalerie anglaise .
L' arrivée des Normands eut un impact profond sur la culture , l' histoire et l' ethnicité irlandaises .
Ils y construisirent également des châteaux , y compris ceux de Trim et de Dublin , ainsi que des villages .
Edgar Atheling , un des prétendants au trône anglais opposés à Guillaume le Conquérant , avait trouvé refuge en Écosse .
Les Normands connaissaient le Pays de Galles bien avant la conquête normande de l' Angleterre .
Ces Normands commencèrent une longue période de lente conquête au cours de laquelle presque tout le Pays de Galles fut plus ou moins sujet à des interventions normandes .
Parmi les aventuriers normands les plus fameux , on trouve Osmond Quarrel et Rainulf Drengot d' abord , qui arrivent en Italie en 1016 avec trois autres de leurs frères .
Les recettes fiscales de Palerme dépassaient à elles seules celles de l' Angleterre normande tout entière .
Ce royaume devait durer jusqu' en 1194 , lorsqu' il revint par alliance aux Hohenstaufens .
Les Normands ont également laissé leur marque dans le paysage avec de nombreux châteaux , comme la forteresse de Guillaume Bras-de-Fer à Squillace ou les cathédrales , comme celle de Roger II à Cefalù , qui parsèment le pays auquel ils donnent une saveur architecturale complètement distincte du fait de son histoire unique .
Les Normands devaient également devenir très influents dans les affaires italiennes , par exemple , lorsque Robert Guiscard fut le seul appui du pape Grégoire VII dans son conflit contre l' empereur Henri IV .
Plus tard , en Asie mineure , Roscelin de Baieul conquit en 1073 la Galatie dont il s' autoproclamera " prince " et fera d' Ankara sa capitale .
Ils furent d' importants participants étrangers à la Reconquista en Espagne .
En 1064 , pendant la guerre de Barbastro , Guillaume de Montreuil prit , à la tête de l' armée papale , un butin énorme .
Bohémond fut le chef de facto de la croisade pendant son passage de l' Asie mineure .
Dès ce moment , il y avait déjà des mercenaires normands servant aussi loin que Trébizonde et la Géorgie .
Ils étaient basés à Malatya et à Édesse , sous le duc byzantin d' Antioche , Isaac Comnène .
Roussel de Bailleul tentera même de se créer son propre état indépendant en Asie mineure avec l' appui de la population locale avant d' être arrêté par le général byzantin Alexis Comnène .
La capacité d' adaptabilité mentionnée par Geoffroi Malaterra s' est manifestée dans le judicieux dessein des Normands d' engager les hommes de talent locaux et d' épouser les femmes locales de haut rang .
Le succès de leur assimilation fut tel qu' à Palerme , peu de traces d' eux demeurent à l' époque moderne .
