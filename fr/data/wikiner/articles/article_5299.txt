Un cluster Beowulf est une grille de calcul homogène dont les composants sont des PC bon marché .
Généralement les ordinateurs fonctionnent sous GNU-Linux ou d' autres systèmes d' exploitations libres .
Un cluster Beowulf n' implique pas l' usage de logiciels particuliers , seule l' architecture des systèmes est définie .
Des bibliothèques ( telles MPI ou PVM ) sont cependant généralement utilisées pour tirer parti du système en distribuant des tâches aux nœuds et en récupérant les résultats des calculs effectués sur ceux-ci .
