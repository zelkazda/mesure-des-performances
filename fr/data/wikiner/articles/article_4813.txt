Cette dernière deviendra par la suite la Chambre des Lords .
Plusieurs réformes de la Chambre des Lords ont progressivement contribué à démocratiser sa composition et à lui donner un rôle de plus en plus symbolique , car certaines dispositions permettent de se passer de leur consentement , mais cette procédure est peu utilisée .
De manière générale , on peut observer le déclin du bicamérisme tant quantitatif ( institution d' un parlement monocaméral en Suède ) que qualitatif ( phénomène du bicamérisme fonctionnel , avec une chambre haute purement réflexive en Norvège et en Finlande ) .
Le Conseil des Anciens comptait 250 membres renouvelés par tiers tous les ans .
La Révolution de Juillet 1830 supprima l' hérédité des pairs .
Sous l' empire de la loi constitutionnelle du 10 juillet 1940 le Régime de Vichy proroge et ajourne les chambres via l' acte constitutionnel n° 3 du 11 juillet 1940 .
