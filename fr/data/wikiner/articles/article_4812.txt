En effet , si les experts de l' UIT étaient d' accord pour définir une abréviation commune à toutes les langues , ils étaient divisés sur le choix de la langue .
L' utilisation de l' ancienne appellation standard temps moyen de Greenwich est désormais déconseillée parce que sa définition est ambiguë , au contraire d' UTC , qui doit lui être préféré .
