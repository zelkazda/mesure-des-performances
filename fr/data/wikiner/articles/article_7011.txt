Il reste connu pour avoir écrit Le Livre du courtisan , manuel de savoir-vivre qui connut un succès important à sa parution .
Quand le pape Léon X est élu , Castiglione est envoyé à Rome comme ambassadeur d' Urbino .
Il y devient l' ami d' artistes et d' écrivains , notamment de Raphaël , qui a peint son portrait ( aujourd'hui au Musée du Louvre ) .
Il lui avait écrit deux lettres passionnées , lui exprimant ses sentiments profonds , mais celle-ci devait mourir quatre ans plus tard , alors que son époux se trouvait à Rome , en qualité d' ambassadeur du duc de Mantoue .
En 1521 , le pape Léon X lui accorda la tonsure et Castiglione commença une carrière ecclésiastique .
En 1524 , le pape Clément VII l' envoie à Madrid en qualité de nonce apostolique , il suit l' empereur Charles V à Tolède , Séville et Grenade .
En mai 1527 les impériaux envahissent et mettent Rome à sac , le pape reprochera à Castiglione de ne pas l' avoir prévenu des intentions de Charles Quint .
Castiglione enverra une lettre au pape , datée du 10 décembre 1527 , soulignant que le saccage était motivé par l' ambiguïté et les contradictions de la politique du pape .
Aujourd'hui , il semble que Baldassare Castiglione ne soit en rien responsable du sac de Rome , et qu' il ait joué honnêtement son rôle en Espagne .
Ainsi , le bruit que Castiglione soit décédé suite aux remords qu' il aurait pu éprouver est infondée , il est mort de la peste noire .
En 1528 , l' année précédant sa mort , son livre le plus célèbre , Le Livre du courtisan , est publié à Venise .
Il décrit la cour d' Urbino , au temps du duc Guidobaldo Ier de Montefeltro , et son courtisan idéal , au travers de dialogues philosophiques et culturels qui lui ont été rapportés alors qu' il se trouvait en Angleterre .
Baldassare Castiglione est mort à Tolède en 1529 .
Castiglione est l' homme d' un seul livre .
