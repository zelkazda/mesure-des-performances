Ce programme est spécifique à chaque plate-forme ou couple ( machine / système d' exploitation ) et permet aux applications Java compilées en bytecode de produire les mêmes résultats quelle que soit la plate-forme , tant que celle-ci est pourvue de la machine virtuelle Java adéquate .
La machine virtuelle la plus utilisée est celle de Sun Microsystems .
Les machines virtuelles développées par les autres éditeurs peuvent poser parfois des problèmes de compatibilité selon leur conformité aux spécifications fournies par Sun .
De plus , il est rare qu' elles soient à jour par rapport aux dernières version de Sun .
