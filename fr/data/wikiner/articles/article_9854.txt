Le golfe de Finlande est un bras oriental de la mer Baltique séparant la Finlande ( au nord ) de l' Estonie ( au sud ) .
Le delta de la Neva , au fond de ce golfe à l' est , offre une ouverture maritime à la Russie , affirmée par la présence de la deuxième plus grande ville de ce pays , Saint-Pétersbourg .
Le golfe de Finlande a une superficie de 29 500 km² , une longueur de 428 kilomètres et une largeur de 120 kilomètres .
Il y a de nombreuses îles dans le golfe de Finlande .
Le point le plus profond , 121 mètres , se trouve sur la côte estonienne , au nord-est de Tallinn .
Environ 5 % des eaux de la mer Baltique se trouvent dans le golfe de Finlande .
L' état écologique du golfe de Finlande est commun à l' ensemble de la mer Baltique , avec un accroissement continu du phénomène d' eutrophisation dû à une déperdition d' oxygène dans les fonds marins , résultat probable d' un lent remplacement de l' eau de la Baltique .
L' enjeu stratégique de ce golfe est relativement important pour la Russie , puisqu' il représente l' une de ses quatre façades maritimes donnant sur des mers non fermées , avec la mer Blanche ( Mourmansk et Arkhangelsk ) , la mer Noire ( Azov ) et l' océan Pacifique ( Vladivostok ) .
L' ouverture de la Baltique sur les océans est cependant limitée par le Skagerrak , détroit contrôlé par le Danemark , membre de l' OTAN .
