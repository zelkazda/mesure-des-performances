L' isotope léger , présent dans le lithium naturel à raison de 7.5 % , capture les neutrons et donne des noyaux d' hélium et de tritium suivant la réaction : Le lithium 7 exposé à des neutrons de haute énergie peut également subir une réaction ( n , alpha ) endothermique ( réaction découverte lors de l' essai Castle Bravo ; explosion d' énergie 2,5 fois supérieure aux prévisions .
La contamination totale moyenne par des traces de tritium reste environ 100 fois sous la limite donnée par l' OMS pour l' eau potable .
L' IRSN a ainsi longtemps considéré qu' il y avait situation d ' " équilibre-vrai " et que le tritium ne s' accumulait pas préférentiellement dans tel ou tel composant environnemental ou biologique .
Christian Bataille , dans son rapport sur la gestion des déchets nucléaires affirmait cependant que le tritium " présente pour la santé humaine des dangers incontestables qu' il convient de ne jamais oublier . "
L' ASN engage les acteurs concernés à harmoniser les méthodes d' évaluation des doses selon l' espèce physico-chimique du tritium , et selon la voie de contamination ( inhalation , ingestion , passage percutané … ) , et non plus seulement selon la durée d' exposition .
L' ASN a demandé des investigations sur d' éventuels effets cancérigènes ou héréditaires ( études épidémiologiques chez les travailleurs , etc .
L' ASN doit créer un comité de suivi de ce plan .
