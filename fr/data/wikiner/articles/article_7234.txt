Militant dès 1848 du Risorgimento , Ippolito Nievo fait des études de droit à Pavie de 1852 à 1855 .
Il commence cette même année la rédaction de son chef d' œuvre , les Confessions d' un italien , qui ne paraîtra qu' après sa mort en 1867 .
