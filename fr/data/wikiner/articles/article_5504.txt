Rots est une commune située à 6 kilomètres à l' ouest de Caen .
Rots fut le théâtre de la découverte la plus ancienne de traces de civilisation dans le Calvados .
La commune fut âprement disputée lors des combats de la bataille de Normandie .
Rots fut finalement libérée le 12 juin 1944 .
