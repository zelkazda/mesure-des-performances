James Tobin fit ses études à l' université Harvard .
Il fut conseiller économique auprès du gouvernement des États-Unis de John Fitzgerald Kennedy et professeur à l' université Yale .
En 1981 , il fut lauréat du " prix Nobel " d' économie .
James Tobin était un économiste keynésien , c' est-à-dire , entre autres , favorable à l' intervention gouvernementale comme stabilisateur de la production , ayant pour but d' éviter les récessions .
James Tobin accéda à la notoriété en proposant une taxation sur certains mouvements de capitaux ( transactions de change ) .
Il suggéra aussi que les revenus de cette taxe soient affectés au développement des pays du tiers-monde , ainsi qu' au soutien de l' Organisation des Nations unies [ réf. nécessaire ] .
Elle est dénoncée notamment par Robert Mundell .
James Tobin déclara quant à lui , que s' il était toujours favorable à cette taxe , il était d' abord partisan du libre-échange et considérait le produit de la taxe comme secondaire et qu' il ne pouvait donc pas soutenir le mouvement altermondialiste .
