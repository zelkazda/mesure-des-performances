Saint Martin est le patron de plusieurs lieux : Tours , Buenos Aires , Mayence , Utrecht et Lucques .
En tant que fils de magistrat militaire , Martin suit son père au gré des affectations de garnison ; il est pour ainsi dire héréditairement lié à la carrière de son père , voué au culte de l' empereur considéré traditionnellement comme un dieu vivant .
Il est probable que Martin ne s' est laissé convaincre que pour ne pas nuire à la position sociale de ses parents tant sa vocation chrétienne est puissante .
Il n' en reste pas moins vrai que ce n' est pas en simple soldat que Martin entre dans l' armée romaine : en tant que fils de vétéran , il a le grade de circitor [ réf. nécessaire ] avec une double solde ; le rôle du circitor est celui de mener la ronde de nuit et d' inspecter les postes de garde et la surveillance de nuit de la garnison .
Il tranche son manteau ou tout du moins la doublure de sa pelisse et la nuit suivante le Christ lui apparaît en songe vêtu de ce même pan de manteau .
En mars 354 , Martin participe à la campagne sur le Rhin contre les Alamans ( ou allemands ) à Civitas Vangionum en Rhénanie ; ses convictions religieuses lui interdisent de verser le sang et il refuse de se battre .
Selon Sulpice Sévère , Martin sert encore deux années dans l' armée puis il se fait baptiser à Pâques toujours en garnison à Amiens [ réf. nécessaire ] ; cette époque est un temps de transition , la fin d' un règne et le début d' un autre règne où tous , même les soldats , sont pénétrés par les idées nouvelles .
En 356 , ayant pu quitter l' armée il se rend à Poitiers pour rejoindre Hilaire , évêque de la ville depuis 350 .
Son statut d' ancien homme de guerre empêche Martin de devenir prêtre : aussi refuse-t-il la fonction de diacre que lui propose l' évêque .
En Illyrie c' est la foi arienne qui est la foi dominante et Martin qui est un fervent représentant de la foi trinitaire doit sans doute avoir de violentes disputes avec les ariens car il est publiquement fouetté puis expulsé .
Il s' enfuit et se réfugie à Milan mais là aussi les ariens dominent et Martin est à nouveau chassé .
Il se retire en compagnie d' un prêtre dans l' île déserte de Gallinara non loin du port d' Albenga et se nourrit de racines et d' herbes sauvages .
Martin s' empoisonne avec de l' hellébore et il s' en faut de peu qu' il ne meure .
Martin en est informé et revient lui-même à Poitiers .
Martin y crée un petit ermitage , que la tradition situe à 8 km de la ville : l' abbaye de Ligugé où il est rejoint par des disciples .
Il crée ici la première communauté de moines sise en Gaule .
Ce premier monastère est le lieu de l' activité d' évangélisation de saint Martin pendant dix ans .
Les habitants l' enlèvent donc et le proclament évêque le 4 juillet 371 sans son consentement ; Martin se soumet en pensant qu' il s' agit là sans aucun doute de la volonté divine [ réf. nécessaire ] ( Un cas identique de contrainte face à un non-consentement se reproduira en 435 pour Eucher de Lyon ) .
Il crée un nouvel ermitage à 3 km au nord-est des murs de la ville : c' est l' origine de Marmoutier avec pour règle la pauvreté , la mortification et la prière .
Les moines doivent se vêtir d' étoffes grossières sur le modèle de saint Jean-Baptiste qui était habillé de poil de chameau .
Le monastère est construit en bois ; Martin vit dans une cabane de bois dans laquelle il repousse les " apparitions diaboliques et converse avec les anges et les saints " : c' est une vie faite d' un courage viril et militaire que Martin impose à sa communauté .
Tout ce monde voyage à travers les campagnes à pied , à dos d' âne et par la Loire ; car Martin est toujours escorté de ses moines et disciples , sans doute en grande partie pour des raisons de sécurité car il ne manque pas de voyager très loin de Tours .
Ailleurs l' autorité de l' évêque est limitée à l' enceinte de la cité , avec Martin elle sort des murs et pénètre profondément à l' intérieur des terres .
Martin semble avoir largement sillonné le territoire de la Gaule ; là où il n' a pas pu aller , il a envoyé ses moines .
Le pape Sirice s' élevera contre les procédés de Maxime .
Par la suite , Martin de Tours refusa toujours de participer aux assemblées épiscopales , ce qui , avec ses efforts pour sauver de la mort Priscillien , le fit suspecter d' hérésie .
Mais cela n' empêche pas Martin , à la table de l' empereur , de servir en premier le prêtre qui l' accompagne et d' expliquer que le sacerdoce est plus éminent que la pourpre impériale .
Une légende veut que les fleurs se soient mises à éclore en plein novembre , au passage de son corps sur la Loire entre Candes et Tours .
Bien que les miracles de Martin de Tours fussent déjà connus de son vivant par delà les frontières de son diocèse , qu' il ait prêché l' évangile dans les campagnes et que Sulpice Sévère en fasse l' égal des apôtres , il ne semble pas qu' il ait organisé son action .
L' importance historique de Martin de Tours tient surtout au fait qu' il a créé les premiers monastères en Gaule et qu' il a formé des clercs par la voie monastique .
D' abord admiré par ses amis qui l' ont pris pour modèle , son culte a été instauré par ses successeurs au trône épiscopal de Tours , qui surent faire de leur basilique un sanctuaire .
Tours reste par la suite un foyer spirituel important .
À l' époque carolingienne , Alcuin , conseiller de Charlemagne , fut nommé abbé de Saint-Martin de Tours et de Cormery .
Selon la légende , en effet , saint Martin portant la bonne parole sur les côtes flamandes , aurait perdu son âne parti brouter ailleurs , alors qu' il tentait d' évangéliser les pêcheurs d' un petit village , futur Dunkerque .
Pour les remercier , saint Martin a transformé toutes les petites crottes de l' âne en brioches à la forme particulière , que l' on appelle folard , ou craquandoules .
Samhain représente le renouveau et donc les 2 aspects à la fois .
Une tradition similaire existe aussi en Alsace et en Allemagne dans le Pays de Bade ainsi qu' aux Pays-Bas .
Le plat traditionnel est une oie rôtie , volailles qui sont grasses à point début novembre et qui rappellent la légende selon laquelle elles auraient dénoncé le saint homme qui s' était caché au milieu d' elles , ne voulant pas être fait évêque de Tours .
On organisa un tirage au sort , saint Martin fut désigné .
L' institution forma environ une centaine de prêtres pour le diocèse de Tours .
La vie de saint Martin est représentée par quatre bas-reliefs au-dessus des portes d' entrée du Dôme saint Martin de Lucques ; en légende , les inscriptions latines figurant sous chaque bas relief .
