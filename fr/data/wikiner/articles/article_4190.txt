Sa capitale est Munich .
Ses principales villes sont Munich , Nuremberg , Augsbourg , Ratisbonne ( Regensburg ) , Wurtzbourg , Ingolstadt , Fürth , Erlangen et Landshut .
Le bavarois est aussi parlé en Autriche : la moitié de ses locuteurs s' y trouvent d' ailleurs .
Au temps de Jules César , cette contrée paraît avoir encore été désertée , mais sous Auguste on la voit déjà figurer au nombre des provinces romaines ; elle était comprise dans la Vindélicie et le Norique .
Les ducs agilofides continuent à régir la Bavière au nom des rois francs jusqu' à Odilon de Bavière qui en 743 prend le titre de roi .
Dès 739 , Boniface fixe les diocèses de Ratisbonne , Freising , Passau et Salzbourg .
Odilon essaye mais en vain de se soustraire à la suzeraineté de Charles Martel .
En 757 , Tassilon prête serment de fidélité à Pépin le Bref , au plaid de Compiègne .
Il réunit des conciles et le pape Hadrien baptise son fils en 772 .
C' est pourquoi Charlemagne exige un renouvellement de son serment en 787 : Tassilon reçoit alors l' investiture solennelle de son duché de Bavière .
Louis le Débonnaire l' érige en royaume franc ( 814 ) , et le donne à son fils aîné , Lothaire , qui en 817 le cède à Louis le Germanique .
Le royaume de Bavière comprend alors , outre la Bavière propre , la Carinthie , la Carniole , l' Istrie , le Frioul , l' ancienne Pannonie , la Moravie et la Bohême .
Sous les successeurs de ce prince , le duché de Bavière , qui avait été considérablement réduit , reprend de nouveaux accroissements .
Louis IV agrandit considérablement ses domaines ; lorsqu' il meurt ( 1347 ) il possède , outre la Bavière , le Brandebourg , la Hollande , la Zélande , le Tyrol , etc .
En récompense l' empereur Ferdinand II élève le duc Maximilien à la dignité d' électeur ( 1623 ) et rend ce titre héréditaire dans sa famille .
Cette dignité lui est confirmée en 1648 par le traité de Westphalie .
Charles-Albert , qui lui succède , prétend à la succession de l' empereur Charles VI , conquiert la Bohême et l' Autriche , et se fait même couronner à Francfort en 1742 sous le nom de Charles VII ; mais vaincu par François de Lorraine , à la tête des troupes autrichiennes , il se voit forcé non seulement de renoncer à l' empire , mais d' abandonner la Bavière elle-même à François de Lorraine ; il meurt avant la fin de la guerre .
Charles Théodore , électeur palatin , allié à cette famille , parvient cependant à régner en Bavière malgré l' Autriche ; et après sa mort ( 1799 ) , son neveu Maximilien Joseph lui succède .
Par la paix de Lunéville , elle doit céder ses possessions sur la rive gauche du Rhin , mais elle reçoit d' amples compensations .
Longtemps fidèle alliée de la France , elle est obligée de lui fournir de nombreux contingents .
En 1809 , Napoléon bat les Autrichiens à Abensberg .
Pour prix de cette conduite , il reçoit au congrès de Vienne , la confirmation de sa royauté et de ses possessions .
Il abdique en 1848 en faveur de son fils Maximilien II qui , pour maintenir l' importance de la Bavière , s' est constamment opposé à toute tentative de centralisation de l' Allemagne .
Mais son successeur Louis II , a dû subir la suprématie de la Prusse , après la guerre contre la France ( 1870 -- 1871 ) .
Ce roi fut certainement le monarque le plus connu , notamment grâce à la construction de châteaux fantasmagoriques ( comme Neuschwanstein ) et par son décès mystérieux .
La Bavière est divisée en sept districts appelés également circonscriptions ou régions administratives , eux-mêmes subdivisés en soixante-et-onze arrondissements et vingt-cinq municipalités non intégrées à un arrondissement , constituant donc un arrondissement à elles-seules , appelées villes-arrondissements .
Il faut attendre le 25 octobre pour qu' une coalition majoritaire , réunissant les conservateurs et le Parti libéral , soit formée .
Le centre économique de la Bavière est Munich , ville du siège social de nombreuses sociétés .
Le taux de chômage s' élève en septembre 2008 à 3,9 % contre 7,4 % pour l' ensemble de l' Allemagne .
La Bavière est principalement catholique ( 56,4 % de la population ) , mais l ' Église évangélique luthérienne a une forte présence dans de grandes parties de Franconie ( 21 % ) .
Le pape actuel , Benoît XVI , est né à Marktl am Inn en Haute-Bavière et fut archevêque de Munich et Freising .
D' une manière générale en Bavière , le culte est bien suivi et les traditions sont très marquées .
La fête de Noël s' étend sur une semaine durant laquelle la vie s' arrête complètement .
En Bavière , le carême est véritablement un temps de pénitence , ce qui explique la liesse manifestée au cours de la semaine de carnavals qui précède .
En Bavière , les lieux publics tels que hôpitaux , jardins d' enfants ou cantines , ne servent que du poisson le vendredi .
L' incroyable densité d' abbayes , couvents et lieux de pèlerinage où les fidèles se rassemblent lors des chemins de croix et autres processions font de la Haute-Bavière un véritable noyau de la spiritualité catholique .
Marie-Nicolas Bouillet et Alexis Chassang ( dir .
