Il obtient ensuite sa maturité et étudie le droit à l' Université de Zurich , avec un passage à Montpellier et à Paris , il obtient un DEA en droit puis étudie à la faculté de droit de l' Université de Zurich , en même temps que le socialiste Moritz Leuenberger .
Fondateur de l' Action pour une Suisse indépendante et neutre en 1986 , il refuse tout rapprochement avec l' Union européenne et est opposé à l' envoi de militaires suisses à l' étranger .
Son ascension politique commence véritablement avec sa victoire lors du refus populaire de l' adhésion de la Suisse à l' Espace économique européen , le 6 décembre 1992 .
Il s' est cependant opposé sans succès à l' entrée de la Suisse à l' Organisation des Nations unies en 2002 .
En 1983 le patron de l' entreprise EMS Chemie meurt .
Devant cette menace , Blocher -- alors employé sans le sou -- décide d' hypothèquer tout ce qu' il possède et d' emprunter aux banques , afin de pouvoir racheter EMS Chemie .
Son parti devient le premier du pays en 1999 , dépassant légèrement le Parti socialiste suisse .
Il consolide cette place lors des élections du 23 octobre 2003 en obtenant 55 mandats au Conseil national ( chambre basse du parlement ) et 8 au Conseil des États ( chambre haute ) .
Le 17 novembre 2008 , à la suite de la démission du conseiller fédéral Samuel Schmid , l' UDC zurichoise le désigne comme candidat à sa succession .
Il possède la plus grande collection de toiles d' Albert Anker et de nombreux tableaux de Ferdinand Hodler .
