DemoLinux a été diffusé dans de nombreux magazines de presse informatique , dans plusieurs pays .
On peut notamment considérer cette distribution de GNU/Linux comme l' ancêtre de Knoppix .
DemoLinux offrait aux utilisateurs des centaines d' applications ( dont l' environnement de bureau KDE et la suite bureautique StarOffice ) grâce à un système de fichiers compressé .
La version 1 était basée sur Mandrake Linux , les versions 2 et 3 utilisaient un mécanisme indépendant de la distribution et étaient distribuées principalement sur une base Debian .
Ces dernières versions offraient en outre la possibilité d' installer GNU/Linux sur le disque dur , offrant ainsi une procédure d' installation très simple de Debian ( principe réutilisé aujourd'hui par certaines distributions ) .
