Tsahal désigne l' armée israélienne .
Tsahal possède une armée de terre avec un important corps blindé , une marine de guerre , et une armée de l' air .
Tsahal a été longtemps commandée par des généraux issus des corps " terriens " et en particulier du corps blindé mécanisé .
Le 22 janvier , il est remplacé par le général Gabi Ashkenazi .
Celui-ci avait quitté l' armée en 2005 , suite à la nomination de Dan Halutz à ce poste qu' il convoitait .
Tsahal , depuis 1987 et la première intifada palestinienne , vit une profonde mutation , due , d' une part , aux débats de la société israélienne partagée entre l' assouplissement et une ligne plus dure , et d' autre part à une nouvelle stratégie politique et diplomatique , favorisant une recherche de la profondeur stratégique , donnant plus d' importance à l' armée de l' air et à la marine .
La nomination , en août 2005 , d' un ami proche d' Ariel Sharon , le général d' aviation Dan Halutz en tant que chef d' état-major de l' armée , marque le début d' une évolution stratégique de grande importance de Tsahal .
Bien que ce volet de la défense israélienne n' ait jamais été ni confirmé ni infirmé par le gouvernement , les experts considèrent comme certain que Tsahal dispose de l' arme nucléaire .
Le cœur de l' activité nucléaire israélienne repose dans les différentes installations de la centrale nucléaire de Dimona , construite avec la France à partir de 1956 .
Ces vecteurs seraient des missiles balistiques Jericho et des bombes largué par avion .
On spécule sur la capacité des sous-marins de la marine Israélienne d' emporter ou non des armes nucléaires .
