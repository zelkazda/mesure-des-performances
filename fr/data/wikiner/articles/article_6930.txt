Il s' est ensuite imposé sur le Tour d' Espagne 1999 , a été champion olympique de la course en ligne en 2000 et champion du monde du contre-la-montre en 1999 et 2001 .
Il passe pro en 1996 dans l' équipe Deutsche Telekom .
Après , il se contente de gérer Richard Virenque et Marco Pantani en montagne et domine les contre-la-montre .
En 2001 , il redevient champion d' Allemagne , mais ne peut rien faire face à Armstrong et signe sa 4 e seconde place .
Il connait une saison blanche et quitte la Telekom .
Il terminera 4 e derrière Andreas Kloden et Ivan Basso .
En 2005 , il ne peut rien face à Armstrong et Basso et finit 3 e .
Il se prépare spécifiquement en participant au Tour d' Italie et au Tour de Suisse qu' il remporte .
Ce sera sa dernière victoire car il est emporté le 30 juin par l' affaire Puerto .
Il est licencié sur le champ par la T-Mobile .
Il fut ainsi reconnu 2 e du classement UCI en 1997 , et Vélo d' Or en 1997 ( 2 e : 1999 ; 3 e : 2000 ) .
Son nom est apparu dans une affaire de dopage en Espagne , l' affaire Puerto .
Il est licencié le 21 juillet 2006 de l' équipe cycliste T-Mobile .
Le 4 septembre 2006 , la chaîne de télévision publique allemande ARD a annoncé que dès la fin de l' année 2006 , elle mettrait un terme à sa collaboration avec Jan Ullrich .
Le 8 septembre 2006 , le parquet de Bonn déclare que les rapports de la police espagnole contiennent des éléments indiquant que le coureur cycliste se serait procuré des produits interdits dès l' année 2003 .
L' hebdomadaire allemand Focus a révélé le 19 novembre 2007 que Jan Ullrich , maillot jaune du Tour de France 1997 , s' était dopé à l' EPO durant cette période .
