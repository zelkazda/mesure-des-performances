Elles se focalisent généralement sur la Grande pyramide , partant du principe qu' une méthode pouvant expliquer sa construction peut également s' appliquer à toutes les autres pyramides d' Égypte .
Hérodote ne donne en fait aucune indication sur la méthode de construction de la pyramide en elle-même .
L' historien grec Diodore de Sicile
n' émet pas du tout " l' hypothèse de rampes et de traîneaux " , comme on le répète souvent : il rapporte seulement que " les pierres ont été , dit-on , disposées au moyen de terrasses [ ... ] de sel et de nitre qui auraient été dissoutes par les eaux du Nil " .
Ni rampes , ni traîneaux chez Diodore : tout au plus des " terrasses " ( c' est très différent ) , auxquelles il dit d' ailleurs ne pas croire .
Les pyramides étaient couvertes de blocs de calcaire fin et dur parfaitement taillés et polis , provenant des carrières de Tourah .
Les blocs de granite rose parant la base des pyramides de Khéphren et de Mykérinos , également utilisés pour la maçonnerie des appartements funéraires , provenaient des lointaines carrières de Syène ( Assouan ) .
Il existe au musée du Caire un instrument en bois appelé ascenseur oscillant , constitué d' un bloc semi-circulaire et d' un mât central .
Le transport d' énormes monolithes , comme celui du temple haut du complexe funéraire de Khéphren , pesant plus de 400 tonnes pour un volume de 170 mètres cubes , est plus problématique .
Afin d' acheminer les pierres extraites des lointaines carrières ( Assouan est situé à près de mille kilomètres de la région memphite des pyramides ) , le transport fluvial sur le Nil était nécessaire .
Ils avaient à leur disposition des embarcations spécialement adaptées aux lourdes charges ( colonnes monolithiques et sans doute blocs de granite ) , comme l' atteste le bas-relief de la chaussée d' Ounas .
L' activité atteignait son maximum durant la période des inondations , mais , afin de remédier aux difficultés liées aux périodes de décrue , une voie navigable a été creusée parallèlement et à l' ouest du Nil , permettant aux convois de débarquer leurs lourdes charges dans les ports situés à l' emplacement des temples bas des divers chantiers .
Pourtant , quelques vestiges de rampes subsistent , notamment à la pyramide de Meïdoum , à la pyramide de Sekhemkhet et à celle de Khéphren , ainsi qu' à la pyramide de Sinki , et surtout à celle de Sésostris I er , à Licht .
Si elles furent effectivement employées , elles ne pouvaient suffire à achever l' édifice , particulièrement dans la phase de la pose des pierres de parement si parfaitement ajustées , comme à la pyramide de Khéops .
Hérodote rapporte l' utilisation de machines en bois sans équivalent connu par l' égyptologie .
Cette théorie fut développée pour la première fois par l' égyptologue allemand Ludwig Borchardt , interprétant ainsi la présence des vestiges de rampes aux abords de la pyramide de Meïdoum .
Aussi ce principe fut-il modifié , notamment par Jean-Philippe Lauer , qui propose une rampe linéaire unique en brique crue , perpendiculaire à la face orientée vers le Nil , de longueur variable jusqu' à une faible hauteur puis de longueur constante avec inclinaison variable .
Certains objectent à cette théorie le fait que la construction de ces rampes auraient demandé une énergie colossale , que les pentes retenues seraient incompatibles avec la tenue géotechnique des matériaux employés et qu' elle est en contradiction avec le témoignage d' Hérodote .
Elle fut énoncée pour la première fois par l' égyptologue allemand Uvo Hölscher .
L' égyptologue allemand Rainer Stadelmann a dessiné une rampe latérale qui s' étend très largement devant la pyramide , de manière à adoucir la pente .
Son collègue Dieter Arnold a présenté une variante de la rampe précédente , non plus latérale , mais engagée dans la pyramide et dépassant , elle aussi , largement à l' extérieur .
L' un d' eux est la présence dans l' obélisque maçonné d' un temple solaire ( celui de Niouserrê à Abousir ) , de ce qu' il qualifie d' une rampe interne .
La pyramide de Mykérinos , éventrée sur son côté nord , montre de tels gradins .
La pyramide de Meïdoum , pyramide à faces lisses effondrée , ne montre plus que son massif interne en gradins .
Une telle rampe intérieure n' a jamais été détectée ni dans la pyramide de Khéops , ni dans la pyramide de Khéphren .
Le système constructif des pyramides proposé par Pierre Crozat , architecte -- ingénieur polytechnicien EPFL , repose sur l' analyse de l' histoire des techniques de l' art de bâtir , la géologie du site et une lecture au premier degré des écrits d' Hérodote , qui rapportant les paroles des prêtres égyptiens de son temps concernant la construction des pyramides .
Dans ce système , la forme pyramidale de la construction est une conséquence de la technique d' agencement des blocs ( que Crozat appelle " algorithme constructif " ) , c' est-à-dire par encorbellement successifs à l' aide d' un levier posé sur un trépied .
Cette théorie a fait l' objet d' une thèse de doctorat de l' ENSMN .
Jean-Pierre Adam a une préférence pour un système de quatre rampes en zigzag , soit une sur chaque face .
Mais il n' exclut pas l' utilisation complémentaire de chèvres , déjà proposées par Auguste Choisy , ou de cabestans mus à la force des bras .
L' égyptologue allemand Uvo Hölscher défendit d' abord l' idée des rampes en zigzag ( reprises plus récemment par Jean-Pierre Adam ) , puis se pencha sur des moyens de manutention comme les pinces de levage .
Choisy , avec de multiples cabestans et cordages de manœuvre .
Il étudie aussi un type de rampe frontale peu éloigné de celui de Jean-Philippe Lauer , mais de largeur constante .
Les obélisques , quant à eux , descendus au fil du fleuve depuis les carrières d' Assouan , ne nécessitaient aucun halage , mais plutôt un freinage efficace .
Selon ces théories , les blocs de pierre des pyramides d' Égypte n' ont pas été taillés , mais moulés , à la manière du béton .
