Ainsi Zhuangzi explique-t-il que les êtres sont l' émanation fugitive d' une seule et même essence .
En Pologne , consommer des champignons à Noël facilitait les contacts avec les morts .
Dans son roman Voyage au centre de la Terre , Jules Verne évoque une forêt de champignons géants .
Dans la bande dessinée L' Étoile mystérieuse , le héros Tintin est confronté à des champignons géants à la croissance instantanée .
De même dans les aventures du héros de jeu vidéo Super Mario Bros. , le Royaume Champignon extraordinaire et le fait que les champignons lui permettent de changer lui aussi sa taille physique ou encore de gagner une vie ou d' augmenter sa vitesse , sont une allusion probable aux champignons hallucinogènes .
Par exemple un artiste comme Michel Blazy travaille , entre autres , sur les moisissures et pourrissements microscopiques générés par les altérations biologiques sur des installations éphémères .
