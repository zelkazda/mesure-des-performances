Joseph-Louis Lagrange ( 1736 1813 ) met en évidence la relation entre les propriétés des permutations des racines et la possibilité de résolution d' une équation cubique ou quartique .
Paolo Ruffini ( 1765 1822 ) est le premier à comprendre que l' équation générale et particulièrement l' équation quintique n' admet pas de solution .
Les démonstrations de Niels Henrik Abel ( 1802 1829 ) dans deux articles écrits en 1824 et 1826 passent , après des années d' incompréhension , à la postérité .
Évariste Galois ( 1811 1832 ) résout définitivement la problématique en proposant une condition nécessaire et suffisante juste pour la résolvabilité de l' équation par radicaux .
Ses premiers écrits , présentés à l' Académie des sciences dès 1829 , sont définitivement perdus .
Puis Arthur Cayley ( 1821 1895 ) donne une première définition abstraite de la structure de groupe , indépendante de la notion de permutation .
