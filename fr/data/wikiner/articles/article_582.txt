L' Insee et la Poste lui attribuent le code 74 .
Le département de Haute-Savoie a été créé suite au Traité de Turin et après un plébiscite , à partir de la partie nord de la Savoie , annexée à la France .
C' est l' un des derniers grands territoires métropolitains ayant rejoint la France .
Elle est limitrophe des départements de l' Ain et de la Savoie , ainsi que des cantons de Genève , de Vaud et du Valais en Suisse et du Val d' Aoste en Italie .
Une partie de la frontière avec la Suisse est matérialisée par le lac Léman .
La Haute-Savoie est marquée par une activité sismique particulière et le département est classé en " zone de sismicité non négligeable " pouvant aller jusqu' à la destruction de bâtiments avec des séismes de magnitude 6 ( voire 6,5 ou même 7 ) .
Les Alpes sont une montagne jeune d' un point du vue géologique et continuent leur élévation -- en moyenne le mont Blanc s' élève de 1 mm par an .
La faune est celle des pays tempérés de montagne et un effort important de repeuplement a été fait ( chamois , bouquetins , grands rapaces ) dans le cadre des réserves naturelles , lesquelles représentent actuellement une superficie de 180 km 2 , plaçant la Haute-Savoie au 1 er rang des départements français pour la création des réserves naturelles .
( parc naturel régional des Bauges )
Les perturbations d' origine océanique , après leur traversée de la vallée du Rhône , se réactivent au contact des reliefs alpins .
La pluviométrie , de 100 à 150 cm/an dans le bassin d' Annecy , culmine à 150 / 200 cm sur les massifs occidentaux qui protègent quelque peu le massif du Mont-Blanc ( 126 cm/an à Chamonix-Mont-Blanc ) .
Les rives du lac Léman sont cependant plus tempérées .
Avec une croissance très importante et une densité de 161 hab./km 2 , ce qui en fait le deuxième département le plus densément peuplé de la région Rhône-Alpes , derrière le Rhône .
La Haute Savoie est un département dynamique , avec un taux de chômage inférieur à la moyenne national dû en grande partie aux possibilités d' emplois offertes dans la zone de Genève ( 8,7 % de la population active haute-savoyarde selon l' INSEE en 1999 .
Les principales zones dynamiques de la Haute-Savoie sont :
La Haute-Savoie est riche en sites et en activités touristiques .
La Haute-Savoie est un département plutôt à droite .
