Les premiers qui ont envisagé la possibilité du voyage spatial ont été les écrivains du fantastique comme Jules Verne et H. G. Wells .
Les premières personnes qui ont contribué magistralement au développement de l' astronautique ont été Constantin Tsiolkovski , Hermann Oberth , Robert Goddard qui en 1926 lança la première fusée à ergols liquides et Wernher von Braun qui mis au point le premier missile balistique V2 fonctionnant grâce à un moteur-fusée .
Von Braun devint , après la Seconde Guerre mondiale , le principal artisan du programme spatial américain ; c' est grâce à sa Saturn V que l' envoi d' astronautes sur la Lune fut possible .
Sergey Korolev est renommé pour son véhicule porte-satellite Spoutnik lancé 4 octobre 1957 et sa fusée transportant Youri Gagarine le premier astronaute lancée le 12 avril 1961 .
Des raisons commerciales et politiques ont amené l' Europe à débuter un programme spatial ( port et mise en orbite de satellites de communications , d' observations ) comme les autres puissances spatiales ; les américains et les russes ont commencé , de leur côté , à construire des stations spatiales à des fins scientifiques .
Certains projets sont tout de même annulés , comme le X-33 de la NASA , qui devait succéder aux navettes spatiales .
Celui-ci est remplacé par le Programme Constellation , qui fait aussi partie du projet d' exploration lunaire .
Certains organismes comme l' Agence spatiale européenne et la NASA sont en charge et s' intéressent au transport spatial .
