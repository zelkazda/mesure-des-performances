Célèbre à la télévision dans les années 1990 , elle fait ses débuts au cinéma en 1991 avec un petit rôle dans Jungle Fever de Spike Lee .
Très vite , elle obtient des rôles secondaires variés passant du drame ( My Life , 1993 ) , à la science-fiction ( Sphère , 1998 ) ou encore au thriller ( Bone Collector , 1999 ) .
Le véritable tournant dans sa carrière cinématographique se produit en 2002 avec la comédie musicale Chicago .
Elle y dévoile ses talents de chanteuse , danseuse et comédienne aux côtés de Catherine Zeta-Jones et Renée Zellweger .
Fort de ce succès , Queen Latifah se consacre aux comédies et apparaît notamment dans Beauty Shop et New York Taxi , remake américain de Taxi .
En 2006 , l' actrice tient le haut de l' affiche de Vacances sur ordonnance , aux côtés de Gérard Depardieu puis prête sa voix et son bagout au film d' animation L' Âge de glace 2 .
Le 7 juillet 2009 , elle fait un discours au Staples Center de Los Angeles lors de la cérémonie d' hommage à Michael Jackson .
