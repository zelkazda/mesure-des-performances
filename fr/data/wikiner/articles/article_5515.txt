La commune est située dans le Bocage virois , à 20 kilomètres à l' ouest de Vire , 7 kilomètres de Saint-Sever-Calvados et 7 kilomètres de Villedieu-les-Poêles .
Elle est la commune la plus occidentale du département du Calvados .
Saint-Aubin-des-Bois a compté jusqu' à 690 habitants en 1851 .
