Né dans la même rue que Michel Jazy , il est issu d' un milieu modeste ( son père est mineur ) .
Député RPR de Seine-et-Marne depuis 1986 et maire de Coulommiers de 1992 à 2008 , Guy Drut est un proche de Jacques Chirac , qui l' a employé comme chargé de mission auprès du premier ministre en 1975-1976 .
Entre 1985 et 1989 , alors maire de Paris , Chirac en a fait son adjoint chargé des sports .
Il est élu maire de Coulommiers en 1992 .
Guy Drut a inauguré , en 2004 , un complexe sportif à son nom à Saint-Cyr-sur-Loire ( Indre-et-Loire ) .
Guy Drut a été jugé sur l' inculpation d' avoir obtenu d' un emploi fictif dans l' affaire des marchés publics d' Île-de-France .
Il a bénéficié de l' amnistie du président Jacques Chirac en mai 2006 .
En décembre 2007 , il annonce qu' il ne se représentera pas aux élections municipales à Coulommiers .
Le 27 février 2008 , il annonce sa démission , effective le 29 février , de la mairie de Coulommiers pour dénoncer la trahison remontant à décembre de certains de ses colistiers aux municipales .
En 1972 , il remporte sur 50 m haies son premier titre majeur international à l' occasion des Championnats d' Europe " indoor " disputés à Grenoble .
En 1974 , Guy Drut s' adjuge le titre des Championnats d' Europe de Rome avec le temps de 13 s 40 .
L' année suivante , il établit la meilleure performance de sa carrière en signant le temps de 13 s 28 lors du meeting de Saint-Étienne .
Le 28 juillet 1976 , Guy Drut remporte la médaille d' or du 110 m haies des Jeux olympiques de Montréal .
Selon Henri Sérandour , président du comité olympique français , sa présence lors de la candidature de Paris aux Jeux olympiques d' été de 2012 aurait " un peu pesé " sur l' échec de la candidature .
En octobre 2005 , Guy Drut fut jugé avec 46 autres personnes dans le procès du système de corruption présumé au profit de plusieurs partis politiques , en marge des marchés de construction et de rénovation de lycées en Île-de-France ( affaire des marchés publics d' Île-de-France ) .
Par ailleurs , Guy Drut avait " obtenu par la cession de ses actions [ dans une société ] des plus-values très importantes et totalement inexpliquées pour plusieurs millions de francs " .
L' attitude hautaine et désinvolte de Guy Drut devant le tribunal fut notée par les commentateurs .
En France , ces faits doivent donc être étymologiquement oubliés , sous peine de poursuites pénales .
Le casier judiciaire de Guy Drut est donc à nouveau vierge .
Cette amnistie permettra donc à Guy Drut de représenter dignement à nouveau la France au CIO .
Ainsi , Guy Drut , qui avait voté en faveur de cette loi , sera le premier à en bénéficier .
Désormais , en France , comme le prévoit la loi , toute référence à cette condamnation amnistiée pourra être passible d' une amende de 5 000 € .
Guy Drut a pris la décision de se retirer du Comité international olympique , auquel il appartenait depuis 1996 , durant la phase de qualification de Paris , ville candidate pour les Jeux olympiques de 2012 .
Cependant , selon un proche du Comité olympique français , " les JO sont passés et il n' y a aucune échéance importante pour la France avant bien longtemps .
