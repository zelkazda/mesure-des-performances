En 1803 , les États-Unis achètent l' ensemble à Napoléon Bonaparte .
En 1817 , un poste de commerce des fourrures est implanté sur le site actuel de Fort Pierre .
Il est très difficile de savoir lequel du Dakota du Nord ou du Dakota du Sud a été intégré en premier puisque la signature a eu lieu le même jour .
Le président Benjamin Harrison a toujours refusé d' indiquer l' ordre dans lequel il a signé les décrets .
Cependant , la proclamation a été réalisée en premier pour le Dakota du Nord ( en tant que premier par ordre alphabétique ) .
Aussi est-il indiqué avant le Dakota du Sud par la plupart des sources .
Voir aussi : Sitting Bull , Crazy Horse , Wounded Knee .
Le célèbre Wild Bill Hickock fut abattu dans le dos par Jack McCall lorsqu' il jouait au poker dans un saloon de Deadwood .
Lors des deux dernières élections présidentielles , les habitants du Dakota du Sud ont plébiscité le candidat républicain George W. Bush avec 60 % des voix contre 37,5 % à Al Gore en 2000 puis 60 % des voix contre 38,5 % à John Kerry en 2004 .
En 2008 , c' est encore le candidat républicain , John McCain , qui obtient les trois grands électeurs .
Au mois de juillet 2007 , le taux de chômage était de 3 % , le cinquième taux le plus faible des États-Unis .
Les jeunes diplômés fuient le Dakota du Sud pour trouver un emploi .
Les communautés luthériennes ( constituées par les immigrants scandinaves ) et catholiques dominent le paysage religieux du Dakota du Sud .
