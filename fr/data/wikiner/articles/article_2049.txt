Les Territoires du Nord-Ouest sont un territoire du Canada .
Les Territoires du Nord-Ouest recouvrent la partie nord du pays , entre les deux autres territoires , le Yukon à l' ouest et le Nunavut à l' est .
Avec plus de 1300000 km 2 , c' est la troisième plus grande entité du pays , après le Nunavut et le Québec .
Parmi ses caractéristiques géographiques , il faut citer le Grand lac de l' Ours , le plus grand lac situé intégralement au Canada , le Grand lac des Esclaves , le fleuve Mackenzie et les canyons de la réserve de parc national de la Nahanni .
Sa capitale est Yellowknife .
Les Territoires du Nord-Ouest s' étendent , comme leur nom l' indique , sur le nord-ouest du Canada .
La superficie des Territoires du Nord-Ouest atteint 1346106 km 2 , dont 1183085 km 2 de terres et 163021 km 2 d' eaux .
Les Territoires du Nord-Ouest contiennent l' essaim de dykes de Mackenzie , le plus grand essaim de dykes connu sur terre .
Les Territoires du Nord-Ouest recouvrant plus de 1300000 km 2 , le climat varie fortement entre le nord et le sud .
Depuis 1967 , la capitale ( et la plus grande ville ) des Territoires du Nord-Ouest est Yellowknife , sur la rive du Grand lac des Esclaves .
La densité de population est extrêmement faible , avec 0,036 habitants/km² , 100 fois moins que la densité moyenne du Canada .
Les Territoires du Nord-Ouest sont deux fois moins densément peuplés que le Yukon , mais deux fois plus que le Nunavut ; leur densité est comparable à celle du Groenland .
Les Territoires du Nord-Ouest comptent trois parcs nationaux :
En tant que territoire , les Territoires du Nord-Ouest possèdent moins de droits qu' une province .
Les Territoires du Nord-Ouest sont créés en 1870 , suite au transfert des territoires possédés par la Compagnie de la Baie d' Hudson au gouvernement du Canada .
La région est alors divisée en deux territoires , la Terre de Rupert ( bassin de la baie d' Hudson ) et le territoire du Nord-Ouest ( bassin des océans Arctique et Océan Pacifique ) .
L' entrée de ces terres dans la confédération est retardée du fait de la rébellion de la rivière Rouge .
Les Territoires du Nord-Ouest sont créés en 1870 , en même temps que la province du Manitoba ( alors une petite région carrée autour de Winnipeg ) .
Le district de Keewatin , au centre du territoire , est séparé en 1876 .
Le Yukon est formé sur sa partie occidentale en 1898 afin de mieux gérer les intérêts locaux lors de la ruée vers l' or du Klondike .
Le Manitoba est élargi en 1881 , l' Ontario en 1874 et en 1889 , le Québec en 1898 .
Elles sont intégrées aux Territoires du Nord-Ouest .
En 1912 , les Territoires du Nord-Ouest ne conservent que trois districts : Keewatin , Franklin et Mackenzie .
Ils mesurent toutefois 3439296 km 2 , une superficie supérieure à celle de l' Inde .
Lorsqu' en 1905 , les provinces de l' Alberta et de la Saskatchewan sont détachées des Territoires du Nord-Ouest , la population de ces derniers passe de 160 000 à 17 000 habitants .
Après de longues négocations territoriales entre l' Inuit Tapiriit Kanatami et le gouvernement fédéral ( débutées dès 1976 ) , un accord est trouvé en septembre 1992 .
Les cartes suivantes montrent l' évolution territoriale des territoires du Nord-Ouest entre 1867 et la période contemporaine :
En 2009 , la population des Territoires du Nord-Ouest est estimée à 42 940 habitants .
Le Nunavut a été créé en 1999 ; les chiffres de population l' incluent avant 1996 .
En 2006 , la population des Territoires du Nord-Ouest s' élevait à 41 464 habitants .
Depuis 1988 , les Territoires du Nord-Ouest reconnaissent onze langues officielles :
Les résidents des Territoires du Nord-Ouest peuvent utiliser n' importe laquelle des langues officielles dans une cour de justice territoriale , ainsi que dans les débats de l' assemblée .
L' industrie minière est le secteur le plus important des Territoires du Nord-Ouest .
L' agriculture est presque impossible , mis à part au sud du Mackenzie , de façon limitée .
Du fait de leur taille et de leur situation géographique , les transports et les communications dans les Territoires du Nord-Ouest peuvent être difficiles .
Cette situation est l' objet d' un enjeu économique important pour le Canada qui revendique sa souveraineté sur ses eaux intérieures .
