Néanmoins , les langues maternelles restent les langues de communication localement , en particulier en Europe , qui a défini une politique sur ce point .
La Langue des signes française permet par exemple de communiquer entre et avec les malentendants et les non-entendant .
Au Québec il s' agit de la langue des signes québécoise .
Parmi ces études , on retiendra celles de Pierre Musso et de Philippe Breton , qui , sous des arguments un peu différents , portent le même diagnostic : la communication a tendance à être instrumentalisée par les outils de télécommunication et les technologies de l' information .
Pierre Musso note que cette croyance serait fondée sur la philosophie des réseaux , sorte de pseudo- " religion " qui serait la résurgence de la philosophie de Saint-Simon ( voir Claude Henri de Rouvroy , comte de Saint-Simon ) , fondée sur le principe de gravitation universelle .
Ces enjeux sont liés aux différentes fonctions du message ( voir les concepts de Roman Jakobson ) .
Le modèle de Claude Shannon et Weaver désigne un modèle linéaire simple de la communication : cette dernière y est réduite à sa plus simple expression , la transmission d' un message .
À l' origine , les recherches de Shannon ne concernent pas la communication , mais bien le renseignement militaire .
Le modèle dit de Shannon et Weaver n' a en effet de prétention qu' illustrative .
Harold Dwight Lasswell , politologue et psychiatre américain , s' est fait un nom en modélisant la communication de masse .
C' est la stricte reprise des cinq questions que Quintilien adressait à tout apprenti rhéteur .
Pourtant il est critiquable , sur la même base que les critiques émises contre le modèle de Claude Shannon et Weaver .
Ce modèle est à lier par antithèse aux travaux du célèbre Marshall McLuhan et Régis Debray
Cet autre modèle , fondé sur la linguistique , est proposé par Roman Jakobson ( 1896 -- 1982 ) .
À chacun de ces facteurs est lié une fonction du message , explicitée par Jakobson .
Ces travaux sont à lier à l' impulsion linguistique de Ferdinand de Saussure , conceptuelle de Shannon et Weaver , et philosophique de John L. Austin .
George Gerbner , sociologue des années 1950 , avait l' ambition de formuler un modèle général de la communication .
