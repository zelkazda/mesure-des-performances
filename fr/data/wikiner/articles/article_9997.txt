En pratique , en particulier sur Internet , cette règle est loin d' être respectée , car l' espace insécable n' est pas accessible en une seule touche sur un clavier d' ordinateur ( combinaison de deux ou trois touches selon le système d' exploitation ) .
Microsoft Word ( versions récentes ) par exemple remplace automatiquement un deux-points non précédé d' une espace ou un deux-points précédé d' une espace sécable par un deux-points précédé d' une espace insécable s' il pense que le texte est en français .
De même , certains moteurs de wiki ( comme par exemple MediaWiki ) ou de forums interprètent une espace avant un signe de ponctuation double comme insécable .
Cependant , les espaces insécables dans Word ne peuvent être que fines .
En revanche , les espaces correctes sont automatiquement insérées par LaTeX .
