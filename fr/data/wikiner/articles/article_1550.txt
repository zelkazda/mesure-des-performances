Les premières études sur les ultrasons n' étaient pas appliquées à la médecine , mais visaient à permettre la détection des sous-marins à l' occasion de la Première Guerre mondiale .
Au Québec , depuis 2004 , certaines cliniques de procréation et de suivi de grossesse offrent un service d' échographie en 3 dimensions qui permet une vision plus globale du fœtus .
