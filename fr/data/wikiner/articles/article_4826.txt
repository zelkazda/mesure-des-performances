Il décrit l' angle entre le plan de l' orbite et le plan de référence ( généralement le plan de l' écliptique , c' est-à-dire le plan moyen de l' orbite de la Terre , ou le plan équatorial ) .
Dans le système solaire , l' inclinaison de l' orbite d' un corps céleste orbitant autour du Soleil ( planètes , astéroïdes , etc . )
Elle pourrait être mesurée par rapport à un autre plan , comme par exemple le plan équatorial du Soleil ou le plan orbital de Jupiter , mais le plan de l' écliptique est le plus pratique pour des observateurs terrestres .
Parmi les exceptions notables , on trouve les planètes naines Pluton ( 17° ) et Éris ( 44° ) et l' astéroïde ( 2 ) Pallas ( 34° ) .
