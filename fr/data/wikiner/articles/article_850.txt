Dans ce même laboratoire , il travaille alors comme chargé de recherches , puis devient directeur de recherches au CNRS .
Depuis l' an 2000 , il est professeur de mathématiques à l' Institut des hautes études scientifiques ( IHES ) à Bures-sur-Yvette ( France ) .
Il apporte une contribution exceptionnelle dans les domaines de la théorie des nombres et de la géométrie algébrique , en démontrant une partie des conjectures de Langlands .
Le mathématicien ukrainien Vladimir Drinfeld a établi le cas du groupe linéaire en deux variables sur les corps de fonctions des courbes en caractéristique positive .
