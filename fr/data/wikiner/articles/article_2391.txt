Elle est bordée au nord par le Maryland , à l' est par la baie de Chesapeake et l' océan Atlantique , au sud par la Caroline du Nord et le Tennessee , à l' ouest par le Kentucky et la Virginie-Occidentale .
Son nom vient de la reine Élisabeth I re d' Angleterre ( 1533-1603 ) qui était également connue comme la " reine vierge " .
La Virginie fut l' une des premières colonies à contester la tutelle britannique .
Aujourd'hui , l' économie virginienne est diversifiée : elle repose sur les emplois fédéraux et militaires dans le nord et à Hampton Roads , qui possèdent respectivement le plus grand bâtiment de bureaux et la plus grande base navale du monde .
Le triangle historique de la Virginie coloniale comprend Jamestown , Yorktown et Williamsburg , qui attirent des milliers de touristes .
À l' origine , la Virginie fut appelée ainsi en l' honneur de Élisabeth I re d' Angleterre , " La reine vierge " qui était surnommée ainsi car elle resta célibataire .
Il est entouré par la Virginie-Occidentale , le Maryland , et le District de Columbia , la Caroline du Nord , le Tennessee , le Kentucky .
La Virginie est divisée en cinq régions :
Le Commonwealth de Virginie est divisé en 95 comtés et 39 cités indépendantes .
La Virginie a l' habitude depuis 1977 d' élire un gouverneur dont l' appartenance politique est opposée au locataire de la Maison-Blanche .
Lors de la première élection qui suit l' arrivée de Bill Clinton à la Maison-Blanche , la Virginie a basculé de nouveau chez les républicains .
Dès 2001 ( année de l' élection de George W. Bush ) , elle élit de nouveau un démocrate , en l' occurrence Mark Warner .
Lors des élections du 3 novembre 2009 , la tradition est une nouvelle fois respectée : un an après l' élection du démocrate Barack Obama à la Maison Blanche , c' est le républicain Bob McDonnell qui est élu gouverneur avec 59 % des voix contre 41 % au démocrate Creigh Deeds .
L' économie de la Virginie repose sur plusieurs secteurs diversifiés .
De la région d' Hampton Roads jusqu' à Richmond et jusqu' au comté de Lee au sud-ouest , dominent les installations militaires , l' élevage , le tabac et la culture de l' arachide .
D' après le recensement de 2000 , la Virginie possède un grand nombre de circonscriptions socialement favorisées et attire de nombreuses entreprises .
La plus grande base militaire est la Naval Station Norfolk .
La Virginie est riche d' un patrimoine mobilier et architectural de l' époque coloniale .
