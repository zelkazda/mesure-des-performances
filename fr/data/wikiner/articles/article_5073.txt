La théorie mathématique des probabilités trouve ses origines dans l' analyse de jeux de hasard par Gerolamo Cardano au seizième siècle , et par Pierre de Fermat et Blaise Pascal au dix-septième siècle .
Cette idée prend tout son essor dans la théorie moderne des probabilités , dont les fondations ont été posées par Andreï Nikolaevich Kolmogorov .
Kolmogorov combina la notion d' univers , introduite par Richard von Mises et la théorie de la mesure pour présenter son système d' axiomes pour la théorie des probabilités en 1933 .
Une chaîne de Markov est un processus stochastique possédant la propriété markovienne .
