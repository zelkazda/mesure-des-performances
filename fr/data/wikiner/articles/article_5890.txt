Condé-sur-Vire se trouve à 10 km au sud de Saint-Lô qui est la préfecture du département .
La commune se situe à 300 km de Paris et 70 km du mont Saint-Michel , un des lieux les plus visités .
De la préhistoire à 987 , on ne sait rien de Condé .
Condé-sur-Vire est la commune la plus peuplée de son canton , devant le chef-lieu Torigni-sur-Vire .
Sans aucune certitude , saint Jean de Brébeuf , bien que le manoir seigneurial de sa famille se trouvât à Condé-sur-Vire .
