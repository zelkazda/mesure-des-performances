Membre de l' UDR , du RPR puis de l' UMP , il est député de la 2 e circonscription de Paris depuis août 1968 et maire du V e arrondissement de Paris de 1983 à 1995 et depuis 2001 .
Parallèlement , il est élu député de la 5e circonscription de Paris à l' ensemble des élections législatives de 1973 à juin 2007 compris .
Candidat à sa propre succession , il a été battu en mars 2001 par la liste PS -- Verts -- PCF menée par Bertrand Delanoé .
Le taux d' exécution du budget 1996 n' avait ainsi pas dépassé 60 % des crédits votés alors que le budget social , particulièrement élevé à Paris , augmentait de 3 % chaque année au détriment de l' investissement .
Un an avant la fin de sa mandature , le RPR présidé par Michèle Alliot-Marie décide d' organiser une procédure permettant la désignation éventuelle d' autres candidats que Jean Tiberi .
Philippe Séguin , député-maire d' Épinal devient le candidat officiel du RPR allié à Démocratie libérale et l' UDF .
Jean Tiberi maintient cependant sa candidature , protestant contre le fait qu' il n' y ait pas eu de primaires au sein de son parti , et présente des listes dans tous les arrondissements .
Philippe Séguin refusa toute fusion avec des équipes dont il jugeait la fréquentation sulfureuse .
Au soir du second tour , avec près de 51 % des suffrages ( dont 35 % pour les listes de Philippe Seguin ) , la droite est finalement devancée en nombre de sièges par la liste de la gauche plurielle conduite par Bertrand Delanoë ( 12 arrondissements contre 8 ) .
L' actuel mandat de député de Jean Tiberi a été obtenu en juin 2007 grâce à sa réélection au second tour face à la candidate PS Lyne Cohen-Solal avec 52,66 % des suffrages exprimés .
Jean Tiberi est également membre du comité d' honneur du Mouvement initiative et liberté ( MIL ) , organisation d' inspiration gaulliste formée en 1981 .
Jean Tiberi a été impliqué dans un système de fraude électorale dans le V e arrondissement de Paris , responsabilité sanctionnée par un jugement du tribunal correctionnel de Paris du 27 mai 2009 .
Si la peine d' inéligibilité prononcée était confirmée en appel , Jean Tiberi serait obligé de démissionner sans délai de ses mandats de maire et de député .
En tant que maire du 5e arrondissement de Paris , Jean Tiberi a été suspecté à partir de 1997 d' avoir organisé à compter de 1989 un système de fraude électorale .
Le procureur requiert alors contre Jean Tiberi une peine d' inéligibilité d' une durée de cinq ans , un an de prison avec sursis , ainsi que 10000 euros d' amende .
Le 27 mai 2009 , la 16e chambre du tribunal correctionnel de Paris a rendu son jugement et condamné Jean Tiberi , à 10 mois de prison , 10 000 euros d' amende et 3 ans d' inéligibilité .
Jean Tiberi a fait appel de ce jugement .
