Le terme désigne maintenant par extension le phénomène climatique particulier , différent du climat usuel , qui se caractérise par une élévation anormale de la température de l' océan dans la partie est de l' océan Pacifique sud , représentant une extension vers le sud du courant chaud péruvien .
Il a été relié à un cycle de variation de la pression atmosphérique globale entre l' est et l' ouest du Pacifique que l' on nomme l' oscillation australe et l' on unit souvent les deux sous le titre de ENSO .
Son apparition déplace les zones de précipitations vers l' est dans l' océan Pacifique et empêche la remontée d' eau froide le long de la côte de l' Amérique du Sud , ce qui coupe la source de nutriments pour la faune de ces eaux et y nuit considérablement à l' industrie de la pêche .
Cependant , les relations entre ces effets sont moins claires à mesure que l' on s' éloigne du bassin Pacifique .
Tous les ans , peu après Noël et ce jusqu' au mois d' avril , un faible courant côtier inverse ce mouvement et s' écoule vers le sud .
Quand les alizés soufflent à leur pleine puissance , les remontées d' eau froide des profondeurs ( upwelling ) le long du Pacifique équatorial refroidissent l' air qui les surplombe .
On crée ainsi une différence de température entre la côte est du Pacifique et le large .
Ainsi l' air reste libre de nuages pendant les années " normales " dans l' est du Pacifique .
La pluie dans la ceinture équatoriale est alors largement confinée dans l' extrême ouest du bassin , au voisinage de l' Indonésie .
Cette modification des températures de surface océanique est donc responsable du déplacement vers l' est du maximum de pluie sur le Pacifique central .
Ce dernier est donc relié à un affaiblissement temporaire , et très prononcé , de l' anticyclone présent au milieu du Pacifique ( Anticyclone de l' île de Pâques ) , ce qui diminue la force des alizés du sud-est .
Une corrélation est remarquable entre les pressions atmosphériques de l' est et de l' ouest du Pacifique .
En étudiant ces données climatiques et atmosphériques et celle qu' il avait à sa disposition , il parvint à établir , en 1923 , une corrélation temporelle entre les relevés barométriques à l' ouest et à l' est du Pacifique sud .
Ce dernier aurait pour fonction de mesurer l' écart de pression entre l' est et l' ouest de l' océan Pacifique .
Quand l' indice , et donc l' écart , augmentait , la pression était élevée à l' est du Pacifique , et les alizés étaient plus forts .
Lorsque l' indice était plutôt bas , les alizés étaient moins puissants , entraînant des hivers plutôt doux dans le Canada et l' Amérique occidentale .
Le professeur Bjerknes a également établi , quelques années plus tard , le lien entre les changements de températures à la surface de la mer , la puissance des alizés et les fortes précipitations qui accompagnent habituellement les creux barométriques à l' est comme à l' ouest du Pacifique .
L' engin de 2.4 tonnes fut envoyé à une altitude de 1.336 km , faisant un tour de la Terre toutes les 112 minutes , et pouvant observer jusqu' à 90 % des océans .
En Équateur et dans le nord du Pérou environ 250 cm de pluie tombèrent pendant 6 mois .
Plus vers l' ouest , les anomalies du vent ont dérouté les typhons de leurs routes habituelles , vers Hawaii ou Tahiti non préparées à de telles conditions météorologiques .
Les vents qui sont formés dans l' air au-dessus de ces nuages vont déterminer les positions des moussons et les routes des cyclones et ceintures des vents intenses séparant les régions chaudes et froides à la surface de la Terre .
Fin de décembre 1997 , une tempête battant des records a déversé jusqu' à 25 cm de neige dans le sud-est des États-Unis .
Des vagues atteignant 4 mètres de haut ont déferlé au sud de San Francisco .
De violentes tempêtes engendrées ont sévi en Floride , avec des tornades atteignant 400 km/h .
De violents orages , les pires des huit dernières décennies , ont détrempé le Chili .
Vers la fin du mois de décembre , l' Australie subissait la pire des sécheresses d' un siècle surnommé la " super-sèche " .
Des tempêtes meurtrières se sont également déchaînées sur la côte ouest des États-Unis .
Cela permet de donner de façon intuitive une idée des mécanismes qui entraînent les conséquences observées surtout dans la région du Pacifique , mais aussi dans une moindre proportion dans le reste du monde .
