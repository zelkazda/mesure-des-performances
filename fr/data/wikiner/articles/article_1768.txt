Oyonnax ( / ojona / ) est une ville de taille moyenne du centre-est de la France .
La ville a été décorée de la médaille de la Résistance le 16 juin 1947 .
La commune est délimitée par les celles de Dortan , Arbent , Échallon , Charix , Apremont , Bellignat , Géovreisset et Samognat .
La commune est située le long de la ligne Andelot -- La Cluse .
Une gare TER y est implantée .
On trouve pour la commune l' aérodrome d' Oyonnax -- Arbent et son aéroclub Jean Coutty .
C' est le territoire des communes associées " Veyziat " et " Bouvent " , petits villages à flanc de colline , où se développent les lotissements pour une population aisée .
À l' est , le massif du Jura .
À l' extrême sud-est de la commune , à 831 m d' altitude se trouve le lac Genin , d' une superficie de 8 ha et 17 m de profondeur ( partagé entre Oyonnax , Échallon et Charix ) .
On peut noter également que 3 % des habitants de la commune sont des personnes logées gratuitement alors qu' au niveau de l' ensemble de la France le pourcentage est de 4,9 % .
De village , puis bourg , Oyonnax devient une ville et ne cesse de se construire .
100 ans plus tard , en octobre 1989 , Oyonnax est alimentée en gaz naturel .
En 1973 , la commune d' Oyonnax fusionne avec la commune de Bouvent ( fusion simple ) et avec la commune de Veyziat ( fusion association ) .
Oyonnax est chef-lieu du canton d' Oyonnax-Nord et du canton d' Oyonnax-Sud qui comprennent chacun cinq communes .
La commune d' Oyonnax a pour tendance politique , celle de droite .
Voici ci-dessous le partage des sièges au sein du conseil municipal d' Oyonnax :
