Quake II est un jeu de tir subjectif développé par id Software et distribué par Activision en 1997 .
C' est la suite de Quake .
Quake II est un jeu de tir subjectif , le joueur se bat , arme en main .
La musique d' ambiance a été composée par Sonic Mayhem .
Quake II a introduit le railgun , arme mortelle dans les mains de bons joueurs sachant viser .
Quake II n' est pas tout à fait la suite de Quake bien que les références y soient le très nombreuses .
Par contre , le lien entre Quake II et Quake IV est clairement établi .
Dans le jeu , le joueur va s' enfuir de la planète et ... la suite de ses aventures est explicitée dans Quake 4 .
Le code source de Quake II a été distribué le 21 décembre 2001 sous la licence publique générale GNU ; cependant , les autres fichiers de données ( images , textures , sons ... ) sont toujours protégés par copyright .
