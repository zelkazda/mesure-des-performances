Andromède est une constellation de l' hémisphère nord .
Poséidon s' irrita et envoya un monstre ( Cetus ) avec pour mission de ravager les côtes du royaume .
La seule façon de sauver le royaume était d' offrir Andromède en sacrifice pour apaiser la colère du monstre .
Céphée accepta de sacrifier sa fille en l' enchaînant à un rocher ( les chaînes d' Andromède sont en direction de Céphée ) pour laisser Cetus la dévorer .
Heureusement , le héros Persée ( au nord est ) , passant par là , tua le monstre Cetus en le pétrifiant ( avec la tête de la Méduse ) et délivra la belle .
On dit aussi que pour son orgueil , la reine Cassiopée a été enchaînée à son trône , condamnée à tourner autour du pôle Nord et parfois à pendre à l' envers , de façon très peu digne .
La constellation se repère à partir de Algol et du grand carré de Pégase .
Cette diagonale d' Andromède de 60 ° d' amplitude appartient à un immense alignement qui fait le tour du globe , et qui est un axe de repérage majeur de la voûte céleste .
Les trois étoiles principales d' Andromède , qui apparaissent dès la tombée de la nuit , sont situées sur la grande diagonale , et correspondent à la tête ( Alphératz , α , dans le carré de Pégase ) , la hanche ( Mirach , β And ) , et le pied ( Almach , γ And ) , toutes trois assez brillantes ( mag 2 ) .
Le reste des membres est relativement faible ( mag 4 ) : on voit un premier alignement perpendiculaire à la diagonale d' Andromède , au niveau de l' épaule , qui dessine les deux bras .
Côté Nord , le bras pointe sur la chaîne d' Andromède , qui se dessine faiblement ( mag 5 ) entre le carré de Pégase et Céphée .
Les étoiles un peu plus brillantes qui semblent limiter la chaîne d' Andromède forment la petite constellation du Lézard .
La jambe nord d' Andromède part de la hanche vers Cassiopée , et dessine un petit arc de cercle jusqu' au pied droit , que touche presque la main de Persée .
La célèbre galaxie d' Andromède se situe au niveau du genou , à 2 ° vers l' extérieur , et peut y être vue si les conditions de visibilité sont excellentes sous forme d' une brume vaguement lumineuse .
À l' opposé du départ de la jambe d' Andromède , et dans le même axe , on voit une petite étoile intermédiaire ( qui est la pointe de la constellation du Triangle ) , puis on tombe sur la tête du Bélier .
C' est une étoile très chaude , de couleur bleu-blanc , brillant 110 fois plus que le Soleil .
υ And possède un système planétaire avec au moins trois exoplanètes , 0.71 fois , 2.11 fois et 4.61 fois plus massives que Jupiter .
L' objet le plus fameux d' Andromède est M31 , la galaxie d' Andromède , l' un des plus lointains objets visibles à l' œil nu ( M33 est un peu plus éloignée ) .
La nébuleuse planétaire NGC 7662 est l' un des objets les plus faciles à voir avec un télescope amateur et révèle un disque elliptique bleu-vert .
L' amas ouvert NGC 752 couvre une surface assez large .
