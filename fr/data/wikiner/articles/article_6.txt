Ces avenues " rayonnent " en étoile autour de la place , notamment l' avenue de la Grande-Armée , l' avenue de Wagram et , bien sûr , l' avenue des Champs-Élysées .
Pour la conception du monument , l' architecte Chalgrin fut en concurrence avec son confrère Raymond , chargé de collaborer avec lui .
Un arbitrage rendu par le Champagny , ministre de l' intérieur , força Raymond à se retirer honorablement .
Chalgrin supprima alors les colonnes de son projet .
L' architecte mourut assez subitement en 1811 , suivit , huit jours après lui , par son confrère Raymond .
Lors des premières défaites napoléoniennes ( Campagne de Russie en 1812 ) , la construction fut interrompue , puis abandonnée sous la Restauration , avant d' être finalement reprise et achevée entre 1832 et 1836 , sous Louis-Philippe I er .
Les architectes Louis-Robert Goust puis Huyot prirent la relève sous la direction de Héricart de Thury .
L' Arc de triomphe de l' Étoile est inauguré le 29 juillet 1836 pour le sixième anniversaire des Trois Glorieuses .
Au départ avait été prévue une grande revue militaire en présence de Louis-Philippe .
La revue militaire est décommandée et remplacée par un grand banquet offert par le roi à 300 invités , tandis que le monument est inauguré en catimini par Thiers , à sept heures du matin .
L' Arc de Triomphe fait partie maintenant des monuments nationaux à forte connotation historique .
À ses pieds se trouve la tombe du Soldat inconnu de la Première Guerre mondiale .
