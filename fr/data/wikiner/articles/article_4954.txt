Ils constituent dans la religion juive la Torah car , outre les récits mythologiques , on y trouve tout un ensemble de prescriptions ( religieuses , rituelles , culturelles , juridiques , etc . )
Aux premiers temps du christianisme primitif , les lois alimentaires ont été abolies par Paul de Tarse ( Saint Paul ) et non par Jésus , et , vers la fin du 1er siècle , ne concernent plus les chrétiens .
Il en va de même pour les sacrifices d' animaux et un certain nombre de règles figurant dans le Pentateuque .
