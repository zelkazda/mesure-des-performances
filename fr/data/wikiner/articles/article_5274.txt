Mais depuis Aristote jusque et y compris Darwin ( qui avec son " hypothèse de la pangenèse " en proposa une théorie ) , tous les naturalistes croyaient à la transmission des caractères acquis .
Louis Pasteur , en prouvant l' absence de génération spontanée , établit qu' un être vivant possède au moins un ancêtre dont il tire ses caractéristiques .
La première étude sérieuse sur le sujet est réalisée par le moine Gregor Mendel , considéré comme pionnier de la génétique .
August Weismann postula en 1883 l' existence d' un support matériel de l' hérédité .
Bateson fut , en outre le premier à introduire en 1906 le terme de génétique .
La France était à cette époque , du fait de sa tradition lamarckiste scientifique et sociale , bien loin d' accepter une telle idée .
Sur cette base Jacob , Monod et Mayr avanceront en 1961 l' idée que le développement et le fonctionnement des organismes sont le produit d' un programme génétique .
D' autre part , dans le bloc soviétique , la génétique a été interdite ( et ses tenants envoyés au goulag ) par Staline qui avait placé sa confiance dans Lyssenko .
En 1865 , passionné de sciences naturelles , le moine autrichien Gregor Mendel , dans le jardin de la cour de son monastère , décide de travailler sur des pois comestibles présentant sept caractères ( forme et couleur de la graine , couleur de l' enveloppe , etc .
En 1869 l' ADN est isolé par Friedrich Miescher , un médecin suisse .
En 1879 , Walther Flemming décrit pour la première fois une mitose .
En 1880 , Oskar Hertwig et Eduard Strasburger découvrent que la fusion du noyau de l' ovule et du spermatozoïde est l' élément essentiel de la fécondation .
En 1891 , Theodor Boveri démontre et affirme que les chromosomes sont indispensables à la vie
En 1902 , Walter Sutton observe pour la première fois une méiose , propose la théorie chromosomique de l' hérédité , c' est-à-dire que les chromosomes seraient les supports des gènes .
Sa théorie sera démontrée par les travaux de Thomas Morgan .
En 1909 , Wilhelm Johannsen crée le terme gène et fait la différence entre l' aspect d' un être ( phénotype ) et son gène ( génotype ) .
William Bateson , quatre ans avant , utilisait le terme génétique dans un article et la nécessité de nommer les variations héréditaires .
En 1911 , Thomas Morgan démontre l' existence de mutations , grâce à une drosophile ( mouche ) mutante aux yeux blancs .
En 1953 , simultanément aux travaux de recherche de Maurice Wilkins et Rosalind Franklin qui réalisèrent un cliché d' une molécule d' ADN , James Watson et Francis Crick présentent le modèle en double hélice de l' ADN , expliquant ainsi que l' information génétique puisse être portée par cette molécule .
En 1958 , lors de l' examen des chromosomes d' un enfant dit " mongolien " , le professeur Jérôme Lejeune découvre l' existence d' un chromosome en trop sur la 21e paire .
Dans les années 1960 , François Jacob et Jacques Monod élucident le mécanisme de la synthèse des protéines .
En 1961 , François Jacob , Jacques Monod et André Lwoff avancent conjointement l' idée de programme génétique .
Dans les années 1990 , à Évry , des méthodologies utilisant des robots sont mises au point pour gérer toute l' information issue de la génomique .
En 1998 , créée par Craig Venter et Perkin Elmer ( leader dans le domaine des séquenceurs automatiques ) , la société privée Celera Genomics commence elle aussi le séquençage du génome humain en utilisant une autre technique que celle utilisée par le NIH .
En 1999 , un premier chromosome humain , le 22 , est séquencé par une équipe coordonnée par le centre Sanger , en Grande-Bretagne .
En juin 2000 , le NIH et Celera Genomics annoncent chacun l' obtention de 99 % de la séquence du génome humain .
