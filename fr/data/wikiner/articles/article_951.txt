Le mari est également parlé dans les républiques voisines du Tatarstan et d' Oudmourtie , ainsi que dans l' oblast de Perm .
D' après le recensement de 1989 , le nombre de Maris était de 670868 .
La République des Maris -- ou Mari El ' -- atteste quant à elle de quelque 11,6 % d' habitants ne considérant pas le mari comme leur première langue .
