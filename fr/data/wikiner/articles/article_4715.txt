On attribue le terme de " virus informatique " à l' informaticien et spécialiste en biologie moléculaire Leonard Adleman .
Le nombre total de programmes malveillants connus serait de l' ordre 95 000 selon Sophos ( tous types de malwares confondus ) .
La très grande majorité touche la plate-forme Windows .
Bien qu' ils soient extrêmement peu nombreux , il existe aussi des virus sur les systèmes de type Unix / Linux , mais aucun n' est capable de se répandre ou de toucher un nombre significatif de postes .
Le reste est essentiellement destiné à des systèmes d' exploitation qui ne sont plus distribués depuis quelques années , comme les 27 virus -- aucun n' étant dangereux -- frappant Mac OS 9 et ses prédécesseurs .
Comme pour les virus biologiques , où la diversité génétique ralentit les chances de croissance d' un virus , en informatique ce sont les systèmes et logiciels les plus répandus qui sont les plus atteints par les virus : Microsoft Windows , Microsoft Office , Microsoft Outlook , Microsoft Internet Explorer , Microsoft Internet Information Server ...
Les versions professionnelles de Windows permettant de gérer les droits de manière professionnelle ne sont pas immunisés contre ces envahisseurs furtifs .
La banalisation de l' accès à Internet a été un facteur majeur dans la rapidité de propagation à grande échelle des virus les plus récents .
La majorité de ces systèmes , en tant que variantes de l' architecture UNIX ( BSD , Mac OS X ou Linux ) , utilisent en standard une gestion des droits de chaque utilisateur leur permettant d' éviter les attaques les plus simples , les dégâts sont donc normalement circonscrits à des zones accessibles au seul utilisateur , épargnant la base du système d' exploitation .
Le système d' exploitation Linux , au même titre que les systèmes d' exploitation Unix et apparentés , est généralement assez bien protégé contre les virus informatiques .
Cependant , certains virus peuvent potentiellement endommager des systèmes Linux non sécurisés .
Comme les autres systèmes Unix , Linux implémente un environnement multi-utilisateur , dans lequel les utilisateurs possèdent des droits spécifiques correspondant à leur besoin .
Ainsi , les virus ont typiquement moins de capacités à altérer et à infecter un système fonctionnant sous Linux .
C' est pourquoi , aucun des virus écrits pour Linux , y compris ceux cités ci-dessous , n' a pu se propager avec succès .
En outre , les failles de sécurité qui sont exploitées par les virus sont corrigées dans les versions les plus récentes du noyau Linux .
Des scanners de virus sont disponibles pour des systèmes de Linux afin de surveiller l' activité des virus actifs sur Windows .
Ils sont principalement utilisés sur des serveurs mandataires ou des serveurs de courrier électronique , qui ont pour client des systèmes Microsoft Windows .
Cabir est considéré comme le tout premier virus informatique proof of concept recensé se propageant par la téléphonie mobile grâce à la technologie Bluetooth et du système d' exploitation Symbian OS .
Le premier virus ciblant la téléphonie mobile est né en 2004 : il s' agit de Cabir se diffusant par l' intermédiaire des connexions Bluetooth .
Il sera suivi d' un certain nombre dont le CommWarrior en 2005 .
