En 1862 , des batailles telles que Shiloh et Antietam causèrent des pertes comme jamais dans l' histoire militaire américaine .
Grant mena de sanglantes batailles d' usure contre Lee en 1864 , l' obligeant à défendre Richmond en Virginie , la capitale des Confédérés .
La résistance des Confédérés s' effondra après la reddition du général Lee au général Grant à Appomattox le 9 avril 1865 .
Elle est la guerre la plus meurtrière qu' aient connus les États-Unis à ce jour ( plus que tous les autres conflits réunis ) .
La guerre de Sécession puise ses racines profondément dans l' histoire des États-Unis .
Mais en 1787 , la question de l' inefficacité du gouvernement se pose de nouveau lors d' une querelle de frontière entre la Virginie et le Maryland .
Le Nord était protectionniste , tourné vers un marché intérieur et animé par l' égalitarisme .
Alexis de Tocqueville exprime d' ailleurs ses craintes à ce sujet dans De la démocratie en Amérique ( 1835 ) .
En novembre 1860 , le candidat républicain Abraham Lincoln est élu avec seulement 39.8 % des voix .
Un complot pour assassiner le nouveau président avant son investiture , précédé de nombreuses lettres de menace , est déjoué le 23 février 1861 à Baltimore .
Les États confédérés d' Amérique regroupent la Caroline du Sud , le Mississippi , la Floride , l' Alabama , la Géorgie , la Louisiane , le Texas , la Virginie , l' Arkansas , le Tennessee et la Caroline du Nord .
Le Nord demandait des droits de douane pour protéger son industrie naissante .
La guerre de Sécession était prête alors à éclater .
L' US Army , qui en 1860 n' avait que 16367 hommes , avait commencé à mobiliser et à décupler ses effectifs , la conscription n' existant pas au début de ces événements et une partie de ses officiers prenant le parti de la Confédération .
Pendant l' été 1863 , Lee joua son va-tout en envoyant ses troupes dans le Nord jusqu' en Pennsylvanie .
Au bout de trois jours de combats désespérés , les Confédérés durent s' avouer vaincus .
Grant prenait la ville de Vicksburg .
Le conflit s' acheva deux ans plus tard , après une longue campagne où s' affrontèrent les armées commandées par Lee et Grant , grâce à l' apparition progressive d' habiles généraux nordistes comme Ulysses S. Grant et William T. Sherman .
Les Confédérés capitulèrent le 9 avril 1865 .
La bataille d' Appomattox met fin à la guerre de Sécession .
Le général Joseph Johnston se rend à son tour le 26 avril 1865 au général William T. Sherman .
Jefferson Davis , le président de la Confédération sudiste , tente de s' enfuir vers le Mexique mais il est rattrapé par une colonne de cavalerie et sera emprisonné sans jugement pendant deux ans .
Ce dangereux psychopathe pillait et tuait sans hésiter , au nom de la Confédération , qui ne lui avait par ailleurs jamais confié le moindre commandement .
Le Cherokee Stand Watie , par exemple , devint général de brigade dans les rangs confédérés ; à la tête de ses troupes , il fut le dernier des rebelles à se rendre , fin juin 1865 , soit plusieurs semaines après la capitulation de Lee .
La Guerre de Sécession fut l' épisode le plus traumatisant de l' histoire des États-Unis .
Mais elle régla deux problèmes qui tourmentaient les Américains depuis 1776 .
Ainsi , la guerre la plus dure qu' aient jamais livrée les États-Unis aura été sa propre guerre civile .
Chez les Confédérés , la conscription représenta 20 % de leurs effectifs , mais là aussi la peur du recrutement poussa beaucoup de Sudistes à s' engager .
Le Nord disposait d' un avantage considérable sur son ennemi car il possédait 35420 des 49190 km de voies ferrées qui sillonnaient le pays .
Un blocus sévère de l' US Navy , qui captura 1551 bâtiments de commerce et en détruisit 355 , combiné à la perte des liaisons terrestres avec le Mexique fit que le pays vécut en quasi autarcie .
Le Nord vit au contraire son industrie se renforcer pour satisfaire à l' effort de guerre et poursuivre l' œuvre de développement du pays entamé avant la guerre .
Davis était également confronté à des priorités stratégiques contradictoires .
Finalement , Davis préféra la défense frontalière à l' offensive-défensive de Lee , mais adopta une politique de compromis en divisant la Confédération en départements dont les commandants assureraient la défense et le transfert des réserves par chemins de fer .
Cette approche , lente mais sûre , n' obtint pas l' approbation des politiciens ni celle du peuple , pour lesquels le mot de ralliement était : " À Richmond !
Elle n' emporta pas non plus l' adhésion de Lincoln , qui pressait ses généraux de " détruire l' armée rebelle " en une seule bataille décisive .
Les États-Unis de l' époque avaient déjà une grande histoire militaire .
De plus , les Sudistes , à cause d' une pauvreté plus grande , connaissaient bien la rudesse de la vie de campagne , et supportaient beaucoup mieux les privations , le manque d' hygiène , le manque de sommeil , qui étaient très durs à supporter pour les jeunes recrues nordistes souvent citadines .
Les Sudistes pouvaient compter en grande partie sur de l' équipement britannique .
Les Britanniques se mirent officieusement du côté des Sudistes .
Le Nord , bien industrialisé , disposait de nombreuses ressources industrielles et d' hommes d' affaires avertis .
Le Nord eut la chance d' avoir les premières mitrailleuses , d' équiper certains soldats avec des armes semi-automatiques , etc .
Le Nord se retrouva ainsi avec une armée professionnelle composée de volontaires bien entraînés et bien équipés .
Mais cela ne dura que très peu de temps et les Confédérés se mirent à souffrir d' une grosse crise logistique .
Elle dérivait en droite ligne de celle portée durant la guerre de 1812 puis pendant celle contre le Mexique et demeurera semblable tout au long du conflit .
