Né dans l' État de Washington aux États-Unis , il a été missionnaire au Brésil .
Il est membre de l' Église de Jésus-Christ des saints des derniers jours et enseigne actuellement à Salt Lake City .
En 1977 , il publie une première nouvelle , pour laquelle il obtient le prix John Wood Campbell Memorial ( meilleur nouvel auteur en science-fiction ) .
Au cours des années 1990 , il participe à la réalisation de plusieurs jeux vidéos chez LucasArts comme Loom , The Dig ou The Secret of Monkey Island .
Malgré le succès du cycle d' Ender , cet auteur est plutôt orienté vers la fantasy .
En 2005 , il scénarise deux mini-séries de comics Ultimate Iron Man pour Marvel Comics dessinées par Adam Kubert et Pasqual Ferry .
Remarque : ce cycle de l' ombre ne semble pas figurer dans les éditions et critiques anglophones de l' œuvre d' Orson Scott Card .
Chaque personnage a un talent magique plus ou moins développé , y compris des personnages empruntés à l' histoire réelle des États-Unis d' Amérique .
Il décide de lancer une mission de retour vers la Terre .
Cette œuvre en cinq tomes peut être vu sous l' angle d' une réadaptation fantastique du livre de Mormon .
Orson Scott Card a collaboré au développement de certains jeux vidéo de la compagnie LucasArts dans les années 1990 : Loom , The Secret of Monkey Island ( il a écrit les duels d' insultes au sabre ) , The Dig .
L' échec commercial du premier jeu de la série , Advent Rising , a marqué le coup d' arrêt du développement des jeux suivants .
