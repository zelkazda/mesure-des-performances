Elle est aussi le chef-lieu de la province de Liège .
Son agglomération est peuplée d' environ 600000 habitants et est la capitale économique officielle de la Région wallonne .
Par le nombre d' habitants , c' est la troisième agglomération de Belgique après Bruxelles et Anvers et la quatrième ville après Anvers , Gand et Charleroi .
Les communes de Seraing , Saint-Nicolas , Ans , Herstal , Beyne-Heusay , Fléron , Chaudfontaine , Esneux et Flémalle ( en orange sur la carte de la province de Liège ci-contre ) font partie de cette agglomération morphologique .
Ces espaces vides sont de trois types : les versants trop raides , les zones industrielles ( dans la vallée , mais aussi jusqu' aux plateaux où les terrils marquent les anciennes exploitations charbonnières ) et les vastes espaces boisés au sud de l' agglomération morphologique ( Sart-Tilman ) .
Liège et Lutèce viendraient dès lors de lucotaekia ( sur lucot- , souris , cf .
breton logod , vieil irlandais luch , souris ) ou Lutetia ( sur luto- , marais ; cf .
Le nom s' écrivait Liége ( avec l' accent aigu ) jusque 1946 .
Elle est également souvent appelée " Cité ardente " , cette appellation vient du titre d' un roman chevaleresque écrit par Henry Carton de Wiart en 1904 .
L' appellation de " Cité ardente " n' est nullement antérieure à la parution de ce roman .
À la suite de cet évènement , son successeur , Saint Hubert transfère , avec l' approbation du pape , le siège de l' évêché de Maastricht vers Liège .
Liège devient alors rapidement un important lieu de pèlerinage et se transforme petit à petit en une prestigieuse et puissante cité , cœur du Diocèse de Liège avant 1559 , circonscription qui pèse de tout son poids sur l' histoire des Pays-Bas espagnols .
Une statue de Charlemagne , dressée en 1867 , est présente dans le centre-ville .
Plusieurs églises romanes et de nombreuses pièces d' orfèvrerie ( art mosan ) témoignent encore aujourd'hui de l' efflorescence de cette époque , en particulier les fonts baptismaux de la ville , conservés aujourd'hui à Saint-Barthélemy .
Liège fut très tôt ville d' industrie .
Jean Curtius sera l' un des plus grands armuriers d' Europe .
Quant au perron , symbole des libertés liégeoises , il sera transféré à Bruges en guise d' humiliation .
Liège retrouvera son perron et son indépendance relative , en 1478 , à la suite de la mort du Téméraire .
L' intransigeance de son successeur , Hoensbroeck mènera ensuite à la révolution liégeoise .
L' existence de la Principauté de Liège se termine dans le sang , entre 1789 et 1795 .
En 1815 , la défaite de Napoléon Bonaparte à Waterloo met fin au régime français .
La période hollandaise verra la création de l' Université de Liège et de l' Opéra royal de Wallonie .
Charles Rogier est un des leurs et son rôle dans la révolution est capital .
Liège devient la citadelle du libéralisme radical .
Jean-Joseph Merlot à nouveau au pouvoir avec Gaston Eyskens à partir de 1968 , puis André Cools vont jeter les bases du Fédéralisme belge que ce gouvernement met en place .
De 1978 à 1986 , la sidérurgie liégeoise est menacée de faillite et provoque une mobilisation maximale avec les graves manifestations de février et mars 1982 à Bruxelles .
Elle appartient aujourd'hui à une région transfrontalière , " l' Eurorégion Meuse-Rhin " , zone d' influence privilégiée qui compte quelque 3,7 millions d' habitants .
La ville se situe également au carrefour de trois zones géographiques naturelles : au nord , la Hesbaye ( 160 à 200 m ) , une des principales zones agricoles de Belgique ; à l' est , le Pays de Herve ( 200 à 320 m ) , un paysage plus vallonné et arboré , grande région fruitière ; au sud , les plateaux du Condroz ( 200 à 280 m ) , portes de l' Ardenne où dominent landes et forêts et , du haut de ses 694 m , le point de plus haute altitude en Belgique , le signal de Botrange .
Le territoire de la ville est formé pour une partie importante des plaines alluviales de la Meuse , fleuve qui déroule ses méandres sur 950 km depuis le plateau de Langres en France jusqu' aux Pays-Bas où il se lie au Rhin pour se jeter dans la mer du Nord , ainsi que des plaines deux de ses affluents , l' Ourthe et la Vesdre .
La Meuse traverse Liège suivant une direction générale sud-ouest/nord-est , sur une longueur de quelques 12 km entièrement canalisée .
Le Port Autonome de Liège bénéficie d' une situation privilégiée où la ville se voit reliée , par voie d' eau , à la France , à la Flandre et aux Pays-Bas .
Liège a connu néanmoins un phénomène de dépopulation .
Par contre , la ville est le centre d' une agglomération continue de 600000 habitants dont la population reste stable , ce qui indique , comme souvent en Europe , un glissement du centre urbain vers la périphérie .
Liège a longtemps été une grande ville industrielle mais dès les années 1960 , elle subit un long déclin , les usines devenant vétustes .
ou encore le leader mondial de l' armement léger : la FN Herstal mais aussi agro-alimentaire : bières , eaux et limonades ou chocolat ( Galler ) .
Liège mise également beaucoup sur les transports et la multimodalité .
L' Université de Liège héberge également de nombreuses " spin-off " et on retrouve non loin de là de très nombreuses entreprises de haute technologie ( ex .
EVS ) .
Liège est aussi un centre de décision .
Cela s' explique par le nombre élevé des institutions liégeoises comme l' université , les musées , les salles d' arts ou les transports et aussi par le fait que Liège possède toutes les institutions de la Province de Liège .
Enfin , l' ouverture le 21 octobre 2009 de la Médiacité , complexe centré sur l' audiovisuel , comprenant des salles de cinéma , une patinoire olympique , des studios d' enregistrement ( dont le nouveau centre liégeois de la RTBF ) , une galerie commerciale reliée à celle déjà existante du Longdoz , permet de développer l' attractivité du quartier Longdoz .
Liège possède de nombreux atouts logistiques qui lui donnent une place importante au cœur de l' Europe des transports .
Liège possède également un réseau de bus qui couvre à la fois le centre-ville et l' agglomération .
Liège envisage par ailleurs de se porter candidate en 2012 auprès du Bureau international des expositions pour l' organisation d' une exposition internationale en 2017 .
Liège héberge quelques grands musées dont , principalement :
On retrouve le perron liégeois sur le blason de la ville , mais aussi sur celui de la province de Liège , celui de la principauté de Liège ou encore sur de nombreux logotypes représentant les institutions liégeoises .
Liège est jumelée avec :
Parmi quelques personnalités nées à Liège ou ayant vécu dans la cité ardente , nous pouvons citer :
