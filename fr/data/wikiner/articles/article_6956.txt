Charles Nicolle est né à Rouen et y a passé son enfance .
Après une formation à l' Institut Pasteur de Paris , il retourne à Rouen .
Il reçoit le prix Nobel de médecine en 1928 et est élu membre de l' Académie des sciences en 1929 .
Charles Nicolle est resté très attaché à sa Normandie natale mais il a aussi beaucoup aimé la Tunisie qui l' a adopté .
Sur sa tombe , on peut voir deux rameaux entrelacés , pommier et olivier , symboles de la Normandie et de la Tunisie .
L' ancien hôpital civil français de Tunis porte son nom depuis 1946 .
En 1953 , l' Hôpital général de Rouen décide également , en reconnaissance de ses travaux , de prendre son nom .
En plus de nombreux articles scientifiques , Charles Nicolle a écrit tout au long de sa vie des ouvrages de fiction et de philosophie .
