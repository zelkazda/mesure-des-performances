L' Insee et la Poste lui attribuent le code 03 .
Le nom du département provient de celui d' une des rivières qui le parcourent , l' Allier .
L' histoire du département de l' Allier correspond à celle du duché de Bourbon ( Bourbonnais ) dont il partage la presque totalité du territoire .
Ce département a été l' un des 83 départements créés à la Révolution française , le 4 mars 1790 en application de la loi du 22 décembre 1789 , à partir de la majeure partie de l' ancienne province du Bourbonnais .
En 1940 , le gouvernement du maréchal Pétain s' installa dans la ville de Vichy , qui obtint alors le statut de sous-préfecture .
Le département de l' Allier est formé de la presque totalité de l' ancien Bourbonnais .
Ce département comprend trois villes de taille moyenne : Montluçon ( l' industrielle ) , Moulins , ( le chef-lieu ) , Vichy ( la ville jardin ) .
Il comprend aussi trois villes thermales : Bourbon-l'Archambault , Néris-les-Bains , Vichy .
Néris-les-Bains est la seule ville du département à compter plus de 10 % de résidences secondaires : 504 pour 1800 résidences en 1999 .
Pays frontière au milieu des terres , l' Allier constitue réellement une zone franche entre nord et midi .
On relève deux maxima de précipitations en juin et octobre , et un minimum en janvier-février , avec des moyennes de 694 millimètres à Montluçon ( altitude 207 mètres ) ; 763 mm à Moulins ( 245 m . )
; 778 mm à Vichy ( 251 m . )
; 791 mm à Lapalisse ( 285 m . )
L' influence du relief , notamment dans les vals de Cher et d' Allier , donne également des flux sud-nord .
L' Allier est confronté depuis le début des années 1980 à de nombreux handicaps démographiques .
Le taux de fécondité est légèrement inférieur à celui de la moyenne nationale en 2007 , il suffirait pourtant au renouvellement de la population d' Allier mais la pénurie d' emplois incite à l' exode rural des jeunes vers les bassins d' emplois plus propices , confirmant un solde migratoire négatif .
L' Allier compte trois villes importantes , Montluçon , Vichy et Moulins par ordre de taille .
Jusque vers la fin du XIX e siècle pourtant , la population augmenta grâce au développement de ses villes ( industries à Montluçon et à Moulins , thermalisme à Vichy ) qui compensa l' exode rural .
Après les pertes de la Première Guerre mondiale , la population se stabilisa , puis réaugmenta un peu dans les années 1960 .
C' est Mireille Schurch , maire PCF de Lignerolles ( Allier ) , qui a été élue .
Depuis les élections cantonales de mars 2008 , l' Allier a été repris par une majorité de gauche .
Le département se signale par un vote rouge précoce , qui se maintient jusqu' au lendemain de la Seconde Guerre mondiale , pour les deux grands partis politiques de gauche , le PCF et la SFIO , aujourd'hui devenue le PS , et même jusqu' à nos jours .
Une autre figure locale , Pierre Brizon , député en 1910 , est typiquement le député des métayers .
Plus anciennement , on peut relever que Ledru-Rollin y fait un très bon score en 1848 ( 14 % ) , ainsi que les candidats démocrates et socialistes l' année suivante ( 44 % des voix , contre 35 % en France ) .
De même , la résistance au coup d' État du 2 décembre 1851 est importante , après une tentative de soutien à l' insurrection de juin 1849 .
L' Allier est encore aujourd'hui une des terres du communisme rural , dans une cohabitation parfois difficile avec le parti socialiste .
Pour les causes , on peut relever qu' historiquement , l' Allier est un département où la grande propriété se combine à un important métayage .
Le métayage se maintient comme forme de mise en valeur des terres , puisqu' il concerne encore 40 % des terres en 1892 ( 7 % en France ) .
D' après les études de l' INSEE , l' agriculture représenterait de l' ordre de 7 à 8 du produit intérieur brut départemental .
Ce tableau indique les principales communes de l' Allier dont les résidences secondaires et occasionnelles dépassent 10 % des logements totaux .
Ceux-ci ont acquis de nombreuses résidences secondaires en les rénovant , apportant ainsi à l' Allier une diversité culturelle sans égal .
