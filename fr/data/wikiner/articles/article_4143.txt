Vauban a voulu faire de la France un pré carré , selon son expression , protégé par une ceinture de citadelles .
Il dota la France d' un glacis qui la rendit inviolée durant tout le règne de Louis XIV -- à l' exception de la citadelle de Lille qui fut prise une fois -- jusqu' à la fin du XVIII e siècle , où les forteresses furent démodées par les progrès de l' artillerie .
Vauban est apprécié à son époque et jugé depuis comme un homme lucide , franc et sans détours , refusant la représentation et le paraître , telles qu' ils se pratiquaient à la cour de Louis XIV .
Tous ces métiers ont un point commun : le maréchal ingénieur du Roi Soleil s' est toujours fondé sur la pratique , et il a toujours cherché à résoudre et à améliorer des situations concrètes au service des hommes : d' abord , ses soldats dont il a voulu à tout prix protéger la vie dans la boue des tranchées ou dans la fureur sanglante des batailles .
Mais Vauban n' a cessé aussi de s' intéresser aux plus humbles sujets du roi , " accablés de taille , de gabelle , et encore plus de la famine qui a achevé de les épuiser " ( 1695 ) .
Ainsi , dès qu' on aborde , qu' on approche celui que Saint-Simon qualifiait de " petit gentilhomme de campagne , tout au plus " , on ne peut être que frappé par la multitude de ses compétences , de ses centres d' intérêt , de ses pensées , de ses actions : Dans tous les cas , Vauban apparaît comme un réformateur hardi dont les idées se situaient à contre-courant de ce que la majorité de ses contemporains pensait .
Avant tout connu de ses contemporains pour sa maîtrise de l' art de la guerre et de la conduite de siège ainsi que pour ses talents d' ingénieur , Vauban ne se limite donc pas à ces quelques domaines .
Vauban apporte trois innovations majeures décisives aux techniques d' attaque des places fortes :
Fort de son expérience de la poliorcétique , il conçoit ou améliore les fortifications de nombreuses villes et ports français , entre 1667 et 1707 , travaux gigantesques permis par la richesse du pays .
Il dote la France d' un glacis de places fortes pouvant se soutenir entre elles : pour lui , aucune place n' est imprenable mais si on lui donne les moyens de résister suffisamment longtemps des secours pourront prendre l' ennemi à revers et lever le siège ) .
Vauban aurait entre 1667 et 1707 , été le responsable de l' amélioration des fortifications d' environ 300 villes et dirigé la création de 37 nouvelles forteresses et ports fortifiés [ réf. nécessaire ] .
Il refusa de créer le fort Boyard , selon lui techniquement inconstructible , que Napoléon I er tentera de recréer lors de son règne à partir de ses plans sans plus de succès néanmoins .
Finalement , sous Louis-Philippe en 1852 agacé par des tensions entretenues avec les britanniques , le Fort Boyard verra le jour , grâce à une technique de construction du socle avec des caissons de chaux .
Vauban a pris , à partir de la fin des années 1680 , une distance de plus en plus critique par rapport au roi de guerre , en fustigeant une politique qui lui semble s' éloigner de ses convictions de grandeur et de défense de sa patrie , le tout au nom du bien public .
D' autant que travaillant sur le canal du Midi en 1685-1686 , il a vu les effets des dragonnades sur la population .
Dans ce mémoire , Vauban estime le nombre des protestants sortis du royaume à " 80000 ou 100000 personnes de toutes conditions , occasionnant la ruine du commerce et des manufactures , et renforçant d' autant les puissances ennemies de la France " .
Fontenelle , dans l' éloge funèbre qu' il rédigea pour Vauban , l' a très bien exprimé :
Depuis longtemps , en effet , Vauban s' intéressait au sort des plus démunis , attentif avant tout à la peine des hommes .
sont contemporaines des années les plus noires du règne de Louis XIV , en particulier la terrible crise des années 1693-1694 .
Vauban voyage dans une basterne , une chaise de poste de son invention , plus vaste qu' une chaise ordinaire et portée sur quatre brancards par deux mules , l' une devant , l' autre derrière .
Pas de roues , pas de contact avec le sol : les cahots sur les chemins de pierres sont ainsi évités , il peut emprunter les chemins de montagne , et Vauban est ainsi enfermé avec ses papiers et un secrétaire en face de lui .
Il est fortement marqué par cette crise de subsistances des années 1693-1694 , qui affecta surtout la France du nord , provoqua peut être la mort de deux millions de personnes .
C' est sans doute à partir de la mort de Colbert ( 1683 ) , qu' il rédige ce " ramas d' écrits " , extraordinaire et prolifique document , souvent décousu , dans lesquelles il consigne , en forme de vingt-neuf mémoires manuscrits ( soit 3850 pages manuscrites en tout ) ses observations , ses réflexions , ses projets de réformes , témoignant d' une curiosité insatiable et universelle .
Une brève note de Vauban , incluse dans un agenda , daté du 4 mai 1701 , éclaire le recueil alors en cours de constitution :
C' est Fontenelle , qui a révélé , dans son éloge de Vauban , l' existence de ce recueil de " mémoires reliés et collationnés en volumes au nombre de douze " …
Et le tout conduit bientôt Vauban imaginer une " réformation " globale , capable de répondre au problème de la misère et de la pauvreté , auquel il est sans cesse confronté .
Et parallèlement , Vauban a profité de multiples entretiens " avec un grand nombre de personnes et des officiers royaux de toutes espèces qui suivent le roi " .
Mais contrairement à l' idée de Vauban , cet impôt s' ajoutait aux autres , et la plupart des privilégiés , par abonnement ou par rachat , eurent tôt fait de s' en faire dispenser .
En échange , Louis XIV donnerait les villes de Brisach et de Fribourg .
Cette proposition lui vaudra une remontrance de Louvois par un courrier du 24 août 1683 " ( ... ) je vous répondrai en peu de paroles que si vous étiez aussi mauvais ingénieur que politique , vous ne seriez pas si utile que vous êtes au service du roi " .
En octobre 1706 , Vauban se trouve à Dunkerque , une ville forte qu' il considérait comme sa plus belle réussite , qu' il avait transformé en une cité imprenable : un formidable ensemble de forts de défense , de bâtiments , de jetées , de fossés remplis d' eau , et d' un bassin pouvant contenir plus de quarante vaisseaux de haut bord toujours à flot , même à marée basse , grâce à une écluse .
Du reste , à propos de " son " Dunkerque , le 16 décembre 1683 , il écrivait à Louvois , en faisant preuve , une fois n' est pas coutume , de peu de modestie :
Pourquoi est-il à Dunkerque ?
Parce que le roi lui a confié le commandement de la frontière maritime des Flandres alors sérieusement menacée .
Il a aussi obtenu l' autorisation de construire un camp retranché à Dunkerque , puis un deuxième entre Dunkerque et Bergues .
Mais les fonds nécessaires n' arrivent pas et il s' en plaint au maréchal de Villeroy , qui lui répond le 17 juillet :
Vauban écrit à Chamillard , le ministre de la guerre et des finances , le 10 août :
C' est là , à Dunkerque , à " son " Dunkerque , que Vauban demande à être relevé de son commandement .
" J' ai hier demandé mon congé , écrit-il de Dunkerque , le 25 octobre 1706 , car je ne fais rien ici , et le rhume commence à m' attaquer rudement " .
Il souffrait depuis longtemps d' un rhume récurrent , en fait une forme de bronchite chronique , et venait effectivement de subir de violents accès de fièvre ( et sa présence à Dunkerque , dans les marais des plaines du nord n' est pas faite pour le guérir !
Et il s' en inquiète auprès de Chamillart :
L' amertume pour Vauban est alors a son comble .
Après de nombreux détails techniques , Vauban ajoute ces lignes , des lignes particulièrement émouvantes , dans lesquelles le vieux maréchal continue à offrir ses services :
En effet , la contribution majeure de Vauban à la réforme des impôts est la publication en 1707 malgré son interdiction de cet ouvrage , intitulé :
Vauban propose dans cet essai de remplacer les impôts existants par un impôt unique de dix pour cent sur tous les revenus , sans exemption pour les ordres privilégiés ( le roi inclus ) .
Plus exactement , Vauban propose une segmentation en classes fiscales en fonction des revenus , soumises à un impôt progressif de 5 % à 10 % .
Et Vauban s' était mêlé d' une matière qui ne le regardait pas …
Sans réponse de la chancellerie , Vauban décide de poursuivre quand même l' impression .
Mais comment les faire entrer à Paris , entourée , on le sait , de barrière , bien gardées ?
Voici le témoignage de Saint-Simon :
Cette première interdiction n' a pas affecté , semble-t-il , Vauban , qui , tout au contraire , dans une lettre datée du 3 mars , manifeste sa fierté face au succès de son livre :
C' est Saint-Simon , on le sait , qui a fait naître l' idée que Vauban serait mort de chagrin : " Vauban , réduit au tombeau par l' amertume " .
Mais tout cela est une légende : Vauban n' a été ni inquiété , ni disgracié et il est bien mort de maladie , d' une embolie pulmonaire ( fluxion de poitrine ) , des conséquences de ce " rhume " dont il ne cesse de se plaindre depuis des dizaines d' années dans sa correspondance .
Mais son cœur est aux Invalides depuis la décision de Napoléon en 1808 .
Vauban , apôtre de la vérité , apparaît , avec quelques autres contemporains , comme un citoyen sans doute encore un peu solitaire .
De par ses écrits progressistes , Vauban est considéré comme un précurseur des encyclopédistes , des physiocrates et de Montesquieu
Les plans-reliefs réalisés à partir du règne de Louis XIV sont conservés au musée des plans-reliefs , au sein de l' hôtel des Invalides à Paris où 28 d' entre eux sont présentés .
Une partie de la collection ( 16 ) , est , après un long débat , présentée au palais des Beaux-Arts de Lille .
Vauban est intervenu sur la plupart des places représentées .
