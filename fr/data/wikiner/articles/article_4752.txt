Running Man est un roman de Stephen King publié en 1982 ( écrit sous le pseudonyme de Richard Bachman ) .
Ce roman d' anticipation dystopique -- genre moins fréquemment pratiqué par Stephen King que le fantastique -- a un thème similaire à celui d' une nouvelle de 1958 de Robert Sheckley , Le Prix du danger .
King n' a jamais indiqué s' il s' en était inspiré .
Certains aspects évoquent forcément 1984 de Orwell :
