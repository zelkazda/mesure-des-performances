En août 1936 , juste avant les Jeux olympiques de Berlin , il fut nommé président honoraire de la Fédération internationale de basket-ball amateur .
Cette institution porte aujourd'hui son nom et se situe à Springfield , Massachusetts .
En toute logique , il est également membre du FIBA Hall of Fame , le 1 er mars 2007 .
