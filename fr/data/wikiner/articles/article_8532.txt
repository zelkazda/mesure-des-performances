Elle a été chargée , en 2002 , d' engager un débat sur l' avenir de l' Union européenne , dans la perspective d' une convocation d' une conférence intergouvernementale .
Elle aboutit , en juin 2003 , à un projet de Constitution européenne qui a été repris , pour l' essentiel , lors de la signature du traité de Rome de 2004 .
Elle était composée de 105 membres : À ces 102 membres s' ajoutent le président Valéry Giscard d' Estaing et les deux vice-présidents Giuliano Amato et Jean-Luc Dehaene .
Ce programme ne s' est pas réalisé comme prévu par le président de la Convention , Silvio Berlusconi n' ayant pas été capable d' aboutir à un compromis en décembre 2003 .
