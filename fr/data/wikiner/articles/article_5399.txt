L' Odon traverse le sud du territoire communal .
En 1832 , Le Locheur ( 292 habitants en 1831 ) absorbe la commune d' Arry ( 81 habitants ) , au nord-est de son territoire .
Le Locheur a compté jusqu' à 404 habitants en 1836 , recensement faisant suite à la fusion avec Arry , population identique au recensement suivant ( 1841 ) .
