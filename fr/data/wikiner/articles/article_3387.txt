Étienne Mourrut , homme politique français , né le 4 décembre 1939 au Grau-du-Roi ( Gard ) .
Candidat à sa réélection pour la XIII e législature ( 2007 -- 2012 ) dans la même circonscription , il est réélu avec 59,47 % des suffrages .
Etienne Mourrut a signé en 2004 une proposition de loi tendant à rétablir la peine de mort pour les auteurs d' actes de terrorisme .
