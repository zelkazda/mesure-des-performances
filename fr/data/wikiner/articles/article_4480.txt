Arganchy fait partie de la communauté de communes Bayeux Intercom .
En 1829 , Arganchy ( 285 habitants en 1821 ) absorbe Saint-Amator ( 52 habitants ) à l' est de son territoire .
Arganchy a compté jusqu' à 334 habitants en 1841 mais les deux communes d' Arganchy et de Saint-Amator , fusionnées en 1829 totalisaient 362 habitants lors du premier recensement républicain , en 1793 .
