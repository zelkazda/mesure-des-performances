Pour l' Europe , les historiens font généralement commencer le XIX e siècle en 1815 et le font terminer en 1914 ( début de la Première Guerre mondiale ) .
Pour l' Afrique , le XIX e siècle est le siècle de la colonisation européenne , de la soumission et de la fin des anciennes structures géopolitiques .
Comme pour l' Afrique , le XIX e siècle est le siècle de la colonisation européenne , mais à la différence près que les anciennes structures géopolitiques vont dans la grande majorité des cas résister et subsister .
En outre , Haïti accède à l' indépendance en 1804 .
