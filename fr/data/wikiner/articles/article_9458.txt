La ville s' étend au pied du château de Loches , ancienne place-forte puis résidence royale sur un promontoire naturel en rive droite de l' Indre .
Si Perrusson est au sud en amont de Loches , Beaulieu-lès-Loches et Ferrière-sur-Beaulieu sont sur l' autre rive de l' Indre .
L' angevin Geoffroi Grisegonelle s' établit à Loches et fait construire une église .
Son fils Foulques Nerra fait construire l' énorme donjon quadrangulaire toujours visible de nos jours .
Ce donjon faisait partie d' un dispositif militaire angevin de fortifications encerclant la ville de Tours , objet de ses convoitises .
C' est son fils , Geoffroi II Martel , qui mène à terme la construction de cet imposant édifice .
Dès qu' il est libéré , l' impétueux Cœur de Lion accourt et reprend le château de Loches .
Dix ans après , en 1205 , Philippe Auguste prend sa revanche .
Loches est désormais une place-forte royale qui peut servir de prison et les rois capétiens s' efforcent de la rendre inexpugnable .
En effet , ce dernier ne supporte pas la relation d' Agnès avec son père le roi Charles VII .
Un jour il laisse éclater sa rancœur et poursuit , l' épée à la main , l' infortunée Agnès dans les pièces de la maison royale .
Le clergé séculier , du diocèse de Tours , fonde à Loches un collège en 1576 .
À cette époque Loches est " égale en dignité à Tours et à Chinon "
En 1789 , la prison royale de Loches perd son importance et on ne compte plus que trois prisonniers .
Chinon et Loches sont sous le Consulat choisies sous-préfectures du département d' Indre-et-Loire , préservant ainsi une petite autonomie administrative et culturelle face à la ville de Tours hégémonique .
Le Lochois est un pays caractérisé par une forte paysannerie , fière de ses traditions de Touraine qui furent parmi les mieux et les plus longtemps préservées .
Le textile , sous forme de filature de laine , linges et broderies de Touraine cède la place à des activités de constructions mécaniques et de services agricoles ou forestières , en particulier minoteries , scieries , commerce de grains et de vins , articles de chasse .
Loches se retrouve en pointe avancée de la zone sud pendant la première occupation allemande entre juin 1940 et juillet 1942 .
Dans le passé , la principale industrie était la culture en cave des champignons de Paris , qui employait 600 personnes à son apogée .
Paris se trouve à 270 km .
La gare de Loches est le terminus d' une ligne TER Centre non électrifiée reliant la ville à Tours .
Une liaison routière SNCF entre cette dernière et Chateauroux dessert également Loches .
La commune est desservie par la ligne Ter Centre : Châteauroux ↔ Tours .
Cette attribution faite par José Frèches , éminent connaisseur de l' art chinois , n' a pas fait l' unanimité .
Des caméras étaient installées depuis 2000 en plusieurs endroits sans aucune déclaration à la CNIL , l' identification de mineurs par empreintes digitales est de plus illicite .
