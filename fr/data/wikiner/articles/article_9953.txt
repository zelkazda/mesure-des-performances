Thémistocle lui pense plutôt que cette muraille de bois signifie la flotte .
Thémistocle commandait cette flotte grecque , et grâce à sa ruse et à sa stratégie intelligente dans le combat naval , il écrasa la flotte perse .
Thémistocle s' employa à garantir la sécurité d' Athènes , en poursuivant sa politique maritime , en faisant construire les Longs Murs , reliant Athènes au Pirée , en dépit de l' opposition de Sparte .
Mais Thémistocle aimait le luxe , et son orgueil tyrannique irritait beaucoup de ses compatriotes : il entra en conflit avec Cimon , fils de Miltiade sur la stratégie à employer pour assurer l' hégémonie athénienne .
Il fut néanmoins banni de son pays : frappé d' ostracisme , en 471 il se réfugia dans un premier temps à Argos , où il fomenta des révoltes contre Sparte .
