Les Émirats sont l' un des plus importants producteurs et exportateurs de pétrole ; les principales réserves gazières et pétrolières sont dans l' émirat d' Abou Dabi , membre de l' Organisation des pays exportateurs de pétrole et capitale politique du pays tandis que l' émirat de Dubaï , capitale économique du pays , s' est tourné depuis quelques années vers de nouvelles ressources tels que les ports francs , les nouvelles technologies mais surtout le tourisme de luxe .
Outre cette fédération , la côte du golfe persique compte d' autres émirats , le Koweït et du Qatar , ainsi que Bahreïn ( devenu un " royaume " en 2002 ) et le sultanat d' Oman .
Les Britanniques prirent soin de renforcer les liens qui les liaient aux États de la Trêve , afin de freiner les convoitises d' autres grandes puissances européennes..
Il les engageait à ne pas entretenir de relations diplomatiques avec des pays autres que le Royaume-Uni sans le consentement de ce dernier .
En retour , le Royaume-Uni garantit la protection des États de la Trêve contre toute attaque maritime ou terrestre .
Au début des années 1960 , un premier puits de pétrole fut découvert à Abou Dabi , ce qui permit le développement rapide de l' émirat , sous la conduite du cheikh Zayed ben Sultan Al Nahyane , qui fit construire des écoles , des hôpitaux , des logements et des routes .
Dubaï fut également gagné par cet élan de développement économique , aidé par les recettes des exportations pétrolières .
Les différents émirats commencèrent à se rapprocher et à prendre le contrôle des mains des Anglais , notamment en formant un conseil qui leur permit de décider eux-mêmes des enjeux politiques les concernant .
Le 2 décembre 1971 , six émirats accèdent à l' indépendance sous la forme d' une fédération et prennent le nom d' Émirats arabes unis .
Ils seront rejoints en 1972 par Ras el Khaïmah .
Sous son influence , les EAU connurent un grand essor économique leur permettant de se développer rapidement et de devenir une force importante dans la région .
À sa mort , son fils aîné , le cheikh Khalifa ben Zayed Al Nahyane , lui succéda en qualité de président des EAU et de souverain d' Abou Dabi .
Le sud du pays est constitué d' une partie du Rub ' al Khali ( le désert des déserts ) , tandis que l' est et le nord sont occupés par des montagnes .
Quelques oasis ( Al Ain , Manama ... ) permettent de maintenir une vie dans le désert .
Des sebkhas occupent le sud et l' ouest du pays , notamment le long de la côte , à l' ouest d' Abou Dabi .
Des revendications territoriales , sur trois îles du détroit d' Ormuz et du golfe Persique , l' opposent à l' Iran .
Le pays est situé dans une zone de grande importance géostratégique , au sud du détroit d' Ormuz , un lieu de passage vital où est transporté le pétrole .
Le pays partage 530 kilomètres de frontière avec l' Arabie saoudite à l' ouest , au sud et au sud-ouest et 450 kilomètres de frontière avec Oman au sud-ouest et au nord-ouest .
Abou Dabi est l' émirat le plus grand , occupe 87 % de la superficie totale du pays ( 67340 km² ) .
L' émirat le plus petit , Ajman , a une superficie de seulement 259 km² .
s' étendent sur plus de 650 km sur la rive sud du Golfe Persique .
Le plus grand port se trouve à Dubai , mais il y a également des ports à Abou Dabi et Sharjah .
Au sud et à l' ouest d' Abou Dabi , de vastes dunes s' étendent jusqu' à Rub al Khali en Arabie saoudite .
Avant de se retirer du pays en 1971 , le Royaume-Uni délimita les frontières des 7 émirats afin d' éviter des disputes territoriales qui auraient pu ralentir la formation de l' état fédéral .
Les gouverneurs des émirats acceptèrent en grande majorité les frontières imposées par les britanniques , mais il y eut tout de même une dispute territoriale entre Abou Dabi et Dubaï et entre Dubaï et Sharjah .
Les EAU disposent notamment d' une ressource inépuisable en énergie solaire .
Les principaux animaux vivant aux Émirats arabes unis sont les suivants :
L' oryx d' Arabie , qui parcourait jadis l' entière péninsule d' Arabie , a disparu à l' état sauvage au début des années 1960 .
La gazelle des sables , d' un poids maximum de 22 kilos , est la deuxième antilope des Émirats par sa taille .
À la différence de la gazelle d' Arabie , le tahr d' Arabie lui aussi en danger critique d' extinction , a besoin de s' abreuver tous les jours .
Malheureusement , le léopard d' Arabie est également au bord de l' extinction à l' état sauvage .
Le mâle pesant environ 30 kilogrammes et la femelle autour de 20 kilogrammes , le léopard d' Arabie est beaucoup plus petit que la plupart des spécimens des races africaine ou asiatique .
L' émirat de Dubaï s' étend le long du Golfe Persique sur 72 km et occupe 5 % de la superficie totale du pays , soit environ 3,885 km² .
L' émirat de Sharjah s' étend sur 16 km sur la côte et sur 80 km à l' intérieur des terres .
Les émirats du nord Fujaïrah , Ajman , Ras Al Khaimah , et Umm al-Quwain ont une superficie totale de 3,881 km² et occupent 5 % de la superficie totale du pays .
Les Émirats arabes unis comptaient 2 563 212 habitants en 2006 mais 73,9 % de cette population n' était pas émirati .
Selon d' autres estimations ( fin 2006 ) , la population des Émirats arabes unis s' élève à 5,6 millions d' habitants dont 84 % d' immigrés .
En 2007 , on recensait près de 700000 travailleurs immigrés à Dubaï .
Depuis la création de la fédération en 1971 , les sept émirats qui constituent les EAU se sont forgés une identité nationale propre grâce à la consolidation de leur statut fédéral .
ont également fourni une aide financière importante au Pakistan , avec lequel ils ont toujours eu de bonnes relations diplomatiques .
Le Pakistan fut le premier pays à reconnaître la fédération de manière officielle lors de sa formation et est devenu aujourd'hui un partenaire commercial et économique important .
Environ 400,000 expatriés d' origine pakistanaise sont actuellement employés à Dubaï .
Les Émirats arabes unis et l' Iran se disputent la souveraineté de plusieurs îlots dans le Golfe persique mais cela n' a pas eu d' impact sévère sur les relations diplomatiques entre les deux pays .
Suite à l' invasion du Koweït en 1990 , les Émirats arabes unis ont entretenu de bonnes relations avec ses alliés occidentaux dans le domaine militaire .
Ils ont coopéré avec les forces occidentales afin de libérer le Koweït et ont signé des traités de défense et de coopération militaire avec la France et les États-Unis , lesquels lui ont fourni aide et matériel militaires .
Les Émirats arabes unis ont un accord similaire avec les États-Unis qui leur permet de recevoir l' expertise américaine en matière d' énergie nucléaire .
Les Émirats arabes unis entretiennent de très bonnes relations diplomatiques avec la France depuis plus d' un siècle .
En janvier 2008 , la France et les Émirats arabes unis ont signé un accord nucléaire civil , établissant les fondements pour une coopération entre les deux pays dans le domaine de l' énergie nucléaire .
et le projet de construction d' une ville qui ressemblerait goutte pour goutte à la ville de Lyon à Dubaï d' ici 2012 .
Les deux pays ont également coopéré dans le domaine de la santé et il y a plus de 60 infirmières chinoises aux Émirats arabes unis .
Il y a également des centres médicaux chinois à Abou Dabi , Sharjah , Al Ain et Ajman .
On trouve un nombre important d' expatriés chinois aux Émirats arabes unis surtout dans le milieu sportif , le secteur tertiaire et le monde du business .
Les accords signés entre les deux pays sont comme suit : accord sino-émirati sur la coopération culturelle ( 1989 ) , accord sur la coopération technique dans le domaine médical entre les ministères de la santé chinois et émirati , un accord pour l' envoi de médecins chinois aux Émirats arabes unis ( 1992 ) , un accord pour l' envoi d' infirmières chinoises aux Émirats arabes unis ( 1992 ) , un accord sino-émirati pour l' extradition judiciaire ( 2002 ) et un accord pour la coopération culturelle la même année .
Les premiers échanges entre les deux pays étaient surtout commerciaux : on échangeait des vêtements et épices venus d' Inde contre des perles et des dates .
Les Émirats arabes unis ont accueilli plus d' un million d' expatriés indiens qui jouent un rôle actif dans l' économie du pays .
Il y a eu de nombreuses visites officielles et de contacts bilatéraux entre l' Inde et les Émirats arabes unis .
L' Inde a accueilli des professionnels dans le domaine en provenance des Émirats arabes unis et leur a fourni l' entrainement nécessaire .
L' Inde participe également à tous les salons internationaux de défense militaire organisés par les Émirats arabes unis .
L' Inde et les Émirats arabes unis jouissent de très bonnes relations diplomatiques axées sur une longue histoire d' échanges entre les deux pays et sur des valeurs et traditions partagées .
Ces relations n' ont fait que s' améliorer avec les flux d' immigration importants en provenance d' Inde vers les Émirats arabes unis et par des échanges commerciaux en plein essor .
En effet , les Émirats arabes unis représentent le second marché le plus important pour les produits indiens .
Dans le même temps , les indiens sont devenus des investisseurs importants aux Émirats arabes unis .
Les Émirats exportent également beaucoup de produits manufactures en Inde .
En tant que centre commercial du monde Arabe , les Émirats arabes unis sont également devenus le troisième centre de réexportation dans le monde après Hong Kong et Singapour .
L' émergence des Émirats arabes unis en tant que centre de réexportation est reflété dans les échanges commerciaux croissants entre les deux pays .
Le commerce bilatéral entre l' Inde et les Émirats arabes unis a connu une augmentation significative au cours de ces dernières années et les exports indiens vers les Émirats sont passés de 7,3331.38 millions en 2005-2006 à 10,671.88 millions de dollars en 2006-2007 .
Les importations de produits émiratis en Inde sont passés de 3,787.91 millions de dollars en 2005-2006 à 7,500.61 millions de dollars en 2005-2006 .
Les Émirats arabes unis sont le second marché le plus important pour les produits indiens après les États-Unis d' Amérique .
De nombreuses sociétés indiennes ont contribué à l' essor de certains secteurs aux Émirats arabes unis tels que la production et la transmission d' énergie , l' infrastructure et les transports et la télécommunication .
L' Inde exporte des produits très divers aux Émirats arabes unis .
Les Émirats arabes unis , quant à eux , exportent du pétrole , de l' or , de l' argent , du métal , des perles et des pierres précieuses et semi-précieuses vers l' Inde .
C' est pourquoi les États-Unis et les Émirats arabes unis sont des alliés loyaux l' un envers l' autre et partagent des intérêts communs :
L' une des économies les plus innovantes et les plus libérales au monde , les Émirats arabes unis sont un partenaire important pour les États-Unis .
Les relations diplomatiques entre les États-Unis et les Émirats arabes unis ne cessent de s' améliorer , en particulier dans le secteur social et le secteur culturel .
Bien que la consommation de boissons alcoolisées soit autorisée dans les hôtels et les restaurants pour les non musulmans ( dans tous les Émirats sauf celui de Charjah ) , la présence d' alcool dans le sang d' un conducteur est un délit .
La consommation d' alcool est , par ailleurs , interdite dans tous les Émirats pendant les fêtes religieuses et il est interdit d' offrir des boissons alcoolisées à un musulman .
Les Émirats arabes unis sont subdivisés en sept émirats , eux-mêmes formés de plusieurs enclaves dont les frontières ont été abolies mais dont la souveraineté est parfois floue .
Les EAU bloquent l' accès à l' intégralité du domaine de premier niveau israélien .
D' autres sites plus généralistes comme Flickr sont également bloqués .
L' économie des Émirats arabes unis , dont la balance commerciale est largement excédentaire , est étroitement liée à l' industrie du pétrole et du gaz naturel qui représentent un tiers du PIB .
En 2005 , les Émirats arabes unis étaient le troisième producteur de pétrole dans le golfe Persique après l' Arabie saoudite et l' Iran .
Depuis l' indépendance en 1971 , les Émirats arabes unis sont passés d' un niveau de vie très bas au quatrième PIB par habitant au niveau mondial ( 43 400 US $ en 2005 ) .
Le succès touristique des Émirats arabes unis ainsi que d' autres facteurs ( prix modérés des biens de consommation , températures élevées durant toute l' année , projets touristiques immenses , etc . )
Contrairement au plan stratégique d' Abou Dabi , celui de Dubaï minimise l' importance des revenus pétroliers .
Des aménagements tels que Masdar City et l' île de Saadiyat à Abou Dabi transforment la capitale en un marché émergent .
Mais c' est plus particulièrement Dubaï , l' émirat voisin , qui est renommé pour l' exécution de projets innovants avec des projets tels que The Palm et The World .
Le promoteur Nakheel , division de Dubai World , est chargé de la plupart de ces aménagements off-shore .
La construction a commencé en 2001 avec l' île de Palm Jumeirah .
La construction de Palm Jebel Ali a commencé en 2002 .
Deux fois plus grand que Palm Jumeirah , ce projet devrait accueillir 1,7 million de personnes d' ici 2020 .
Le projet de Palm Deira , annoncé en 2004 , aura cinq fois la taille de Palm Jebel Ali , ce qui en fera la plus grande île artificielle du monde .
Le Burj Dubai , conçu par le promoteur immobilier Emaar est la plus haute tour du monde et culmine à 828 mètres .
Plusieurs des îles ont déjà été achetées par des célébrités telles que Richard Branson ou encore Michael Schumacher .
Mais l' aéroport international de Dubaï est l' aéroport principal du pays .
L' aéroport de Dubaï est également l' un des plus fréquentés par les passagers internationaux .
Les autres aéroports importants sont l' aéroport international d' Abou Dabi , l' aéroport international de Charjah et l' aéroport international d' Al Ain .
Les Émirats arabes unis possèdent un système d' autoroutes reliant toutes les villes principales des différents émirats .
Les autres aéroports importants sont l' aéroport international d' Abou Dabi , celui de Sharjah et celui d' Al Ain .
Son hub est l' aéroport international de Dubaï et elle dessert plus de 100 destinations et 6 continents .
Emirates est la compagnie aérienne qui a transporté le plus de passagers internationaux dans le monde .
Etihad Airways , la compagnie aérienne d' Abou Dabi , est également en plein essor avec plus de 200 avions en cours d' acquisition .
Le métro de Dubaï devrait ouvrir ses portes en septembre 2009 et permettre de se rendre d' un bout à l' autre de la ville beaucoup plus rapidement qu' en voiture .
Abou Dabi prépare également un projet pour la construction d' un métro et d' un service ferroviaire national qui relierait toutes les grandes villes .
Les Émirats arabes unis ont l' intention de construire 68 barrages rechargeables dans les 5 prochaines années qui viendront s' ajouter aux 114 barrages déjà existants afin de pallier les besoins croissants de la population en énergie .
Les Émirats arabes unis sont également sur le point de développer un programme nucléaire pacifique afin de générer davantage d' électricité .
ont signé un accord pour le développement des utilisations pacifiques de l' énergie nucléaire avec la France , les États-Unis , et la Corée du Sud , ainsi qu' un accord avec le Royaume-Uni .
Selon Reporters sans frontières , les autorités filtrent les sites dont le contenu pourrait être nuisible aux citoyens , en particulier les sites pornographiques ou dont le contenu est particulièrement offensif aux mœurs et croyances émiraties .
Le programme scolaire est en harmonie avec les principes et le projet de développement des EAU .
L' Ecole polytechnique fédérale de Lausanne ( EPFL ) est également en train d' implémenter un campus à Ras Al-Khaimah .
Les EAU .
Étant donné son caractère cosmopolite , les Émirats arabes unis ont une culture diverse et vibrante .
Le développement socio-économique sans précédent dont le Golfe Persique a joui , a contribué à libéraliser les Émirats arabes unis .
L' aspect cosmopolite de Dubaï en particulier , est de plus en plus évident et il n' est pas rare de trouver des centres culturels asiatiques , des écoles européennes et de nombreux restaurants spécialisés dans la cuisine étrangère .
Cet accoutrement est particulièrement adapté au climat chaud et humide des Émirats arabes unis .
ont adopté la cuisine de ses pays voisins tels que l' Iran , l' Arabie saoudite et Oman .
En raison de nombreuses relations commerciales que la ville entretenait avec l' étranger , les instruments de musique traditionnels , que l' on peut découvrir au cours des célébrations publiques ou privées sont ici les mêmes que ceux utilises dans tout le reste du golfe Arabo-persique .
Un film consacré à cette danse du courage et de la bataille peut être visionnée au musée de Dubaï .
La fondation culturelle d' Abou Dabi est aussi un lieu important pour l' exposition d' art indigène et étranger .
Abou Dabi a pour ambition de devenir un centre d' art à renommée internationale en créant un lieu réservé uniquement à la culture sur l' île de Saadiyat .
Les Émirats , qui jouissent déjà d' une solide réputation sur la scène sportive internationale , accueillent de nombreuses rencontres de premier plan dans des disciplines sportives très diverses , allant du hippisme à la course automobile en passant par le golf , le tennis , le football , le rugby , le cricket , la voile , les courses de hors-bord et presque tous les autres sports de compétition .
Il serait très difficile de dire quel sport n' est pas pratiqué aux Émirats , car de nouveaux clubs , centres ou associations se créent constamment dans la région .
Dubaï est depuis quelques années la destination préférée des amateurs et professionnels de golf .
Avec plus de 12 parcours signés par de grands architectes internationaux , Dubaï s' est bâti une réputation mondiale dans le domaine .
Depuis , de nombreuses courses équestres ont lieu aux Émirats arabes unis tous les ans , qui ont propulsé le pays sur la scène équestre internationale .
Les habitants des Émirats arabes unis , et en particulier la famille régnante et les cheikhs , entretiennent depuis longtemps une relation passionnée avec les magnifiques chevaux arabes .
Les concurrents de la course annuelle d' endurance incluent des membres de la famille régnante et de la famille royale , des habitants des Émirats arabes unis , des cavaliers expatriés et même des touristes étrangers .
La saison des courses d' endurance à Abou Dabi débute normalement en novembre et dure jusqu' en mai de l' année suivante .
Un centre national entièrement consacré à la fauconnerie se trouve à Dubaï et peut se visiter sur la route qui mène à l' hippodrome .
Les Émirats arabes unis ont pour codes :
