Les Bahamas , ou le Commonwealth des Bahamas pour les usages officiels , sont un pays anglophone , qui bien que situé largement au nord de la Mer des Caraïbes , est parfois considéré comme faisant partie de l' Espace Caraïbe .
Sa capitale est Nassau , située sur l' île de New Providence .
Bahamas est un dérivé de l' espagnol " baja mar " ( mer basse ) .
Les deux îles avaient été habitées dès 1648 par des puritains anglais ayant fui l' archipel des Bermudes , parmi lesquels le futur fondateur de la banque d' Angleterre et du schéma du Darién sir William Paterson .
Il mit un terme au règne des pirates , fit pendre Calico Jack et quelques douzaines d' autres .
Elle est représentée aux Bahamas par un gouverneur général , rémunéré par la reine elle-même .
Depuis 1999 , les Bahamas comprennent 32 districts :
L' archipel des Bahamas compte plus de 700 îles et îlots disséminés sur environ 260000 km² .
Celle la plus proche des États-Unis n' est qu' à 89 km de la côte sud-est de la Floride .
La plus grande île des Bahamas est Andros , à l' ouest .
L' île de New Providence , à l' est d' Andros , est le site de la capitale , Nassau et représente les deux-tiers de la population totale .
Les autres îles importantes sont Grand Bahama au nord et Inagua au sud .
La plupart des îles -- des formations de corail -- sont relativement plates , avec quelques collines basses , dont la plus haute est le Mont Alvernia , sur Île Cat , à 63 m .
Les Bahamas forment une nation stable et développée avec une économie fortement dépendante du tourisme ainsi que des banques offshore .
Les Bahamas reçoivent surtout des visiteurs venus des États-Unis .
La plus proche des 700 îles qui composent l' archipel n' est située qu' à 75 km à l' est de Miami .
Les Bahamas font partie des pavillons de complaisance .
L' industrie ( peu développée ) et l' agriculture réunies contribuent à un dixième du PIB et ne progressent que faiblement , malgré les incitations gouvernementales dans ces secteurs pour pallier la dépendance au tourisme provenant des États-Unis :
Le tourisme et ses activités induites représentent , selon les chiffres officiels bahaméens , 60 % des sept milliards de dollars du PIB des Bahamas .
Les Bahamas ont pour codes :
