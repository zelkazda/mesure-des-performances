Tours compte 136578 habitants et est au centre d' une unité urbaine de 307096 habitants .
La ville est située sur le bras de terre séparant la Loire et le Cher , en amont du confluent de ces deux cours d' eau , à un endroit où leur passage semble aujourd'hui aisé .
Elle s' étale au nord sur le plateau de Saint-Symphorien où se trouve le point le plus haut de la ville et au sud sur le plateau de Grandmont .
L' arrondissement de Tours compte lui en population totale 459806 habitants en 2007 ( source Insee ) .
Tours est également une des deux villes-portes du Parc naturel régional Loire-Anjou-Touraine avec Angers en Maine-et-Loire .
Une des figures marquantes de l' histoire de la ville est saint Martin , deuxième évêque après le mythique Gatien .
Martin est un ancien militaire devenu officier romain .
Tours et l' abbaye de Marmoutier tombent dans les mains des pillards en 853 .
Entre ces deux entités subsistaient des espaces de varenne , de vignes et de champs peu densément occupés , à l' exception de l' abbaye Saint-Julien installée en bord de Loire .
Tours est un modèle de la ville double médiévale .
La Touraine devient une véritable capitale de la France entre 1450 et 1550 , séjour continuel des rois et lieu des fastes de la cour .
À ce moment , les catholiques ont repris les choses en main à Angers : l' intendant s' est arrogé le droit de nommer les échevins .
Le massacre de la Saint-Barthélemy qui prend une ampleur démesurée à Paris fin août 1572 n' a pas cours en Touraine .
Tours , qui possède un présidial depuis 1551 , devient en 1577 le siège d' une généralité , qui contrôle seize élections sur la Touraine , l' Anjou et le Maine .
Tours , capitale de la subdégation de Touraine , peut plus que jamais conserver sa prééminence de marché d' approvisionnement , redistribuant les grains , les vins , les fruits et légumes , les produits laitiers et de basse-cour .
Tours , promue préfecture , est une ville en ébullition révolutionnaire après 1791 .
L' écrivain Balzac , endetté par son aventureuse entreprise parisienne , nourrit avec un brin d' amertume ses tableaux provinciaux de ce solide comportement rentier .
Des levées en zones basses ont été établies et des quartiers bourgeois et ouvriers , vulnérables à une montée des eaux , s' établissent entre La Riche à l' ouest et Saint-Pierre-des-Corps à l' est .
Désormais , Tours n' a plus aucune ville concurrente sur le département .
Chinon et Loches sont irrémédiablement provinciales et distancées .
Tours , lieu de rencontre , affirme les valeurs de son grand centre compagnonnique .
La Première Guerre mondiale marque profondément la ville .
L' un de ses petits-enfants , Jack Claude Nezat est l' auteur de livres consacrés à l' histoire et à la sociologie .
Dès les années folles , Tours est une ville d' équipement et de services .
Tours est également marquée par la Seconde Guerre mondiale .
Tours est détruite précocement en 1940 et une partie de sa population connaît ensuite durant quatre années les affres de la vie en baraquements ou en casemates .
Entre le 10 et le 13 juin 1940 , pendant la débâcle , elle accueille le gouvernement français avant son installation à Bordeaux .
Le pont Wilson ( " pont de pierre " ) , qui approvisionne la ville en eau , a été dynamité pour freiner l' avancée de la Wehrmacht .
Un plan de reconstruction et d' aménagement du centre-ville , dessiné par l' architecte tourangeau Camille Lefèvre , est adopté avant même la fin de la guerre .
Pierre Patout lui succède en tant qu' architecte en chef de la reconstruction en 1945 .
Au nord , Saint-Symphorien et Sainte-Radegonde rejoignent Tours en 1964 .
En 1970 est fondée l' université François-Rabelais , dont le centre de gravité est installé en bord de Loire en plein centre-ville , et non comme c' était alors la tendance dans un campus en banlieue .
Jean Germain fait de la réduction de la dette lors de son accession à la mairie en 1995 une priorité .
L' action de Jean Germain reste cependant critiquée par l' opposition municipale pour la faible ambition des projets municipaux : aucun grand chantier comparable à ceux de Jean Royer n' a véritablement été lancé sous son double mandat .
L' université de Tours compte après trente ans d' existence officielle plus de 22800 étudiants au début du millénaire .
Tours a été la terre d' accueil ou de naissance de nombreuses personnalités célèbres , telles que Alcuin , François Rabelais , Honoré de Balzac , Saint Martin , Pierre Bretonneau , Alfred Velpeau , Francis Poulenc ou dans un registre plus récent le journaliste Harry Roselmack , les comédiens et acteurs Jacques Villeret , Jean Carmet , Jean-Hugues Anglade ou le réalisateur Patrice Leconte .
Tours est le centre de la Communauté d' agglomération Tours Plus qui regroupe 19 communes et plus de 278415 habitants .
Tours est divisée en sept cantons , qui ne sont constitués que de fractions de la commune :
Tours dispose d' une situation géographique privilégiée .
À moitié encerclée par le périphérique tourangeau qui sera achevé dans les 15 années à venir , Tours se situe au centre d' une étoile autoroutière à 5 branches .
De nombreuses liaisons transversales d' intérêt national complètent ce maillage N76 , N138 , etc .
) , permettant notamment de relier la ville aux capitales régionales voisines : Poitiers et Limoges .
Tours a toujours été bien située sur le réseau ferroviaire français .
Si aujourd'hui quelques lignes d' intérêt local ont été fermées , le réseau régional TER Centre est , depuis les années 1990 en constant développement .
Les travaux de la LGV Sud Europe Atlantique devraient démarrer vers 2013 .
La proximité des aéroports parisiens ( Roissy CDG est aujourd'hui accessible directement par TGV depuis Tours en 1 h 35 ) est un sérieux handicap de même que la présence à proximité d' aéroports régionaux concurrents comme l' aéroport de Poitiers ( 100 km ) , l' Aéroport Angers Loire ( 110 km ) et l' Aéroport Nantes Atlantique ( 200 km ) .
Tours est le siège de la Chambre de commerce et d' industrie de Touraine .
Tours est aussi appelé " le petit Paris " par les étrangers .
France Telecom s' est aussi implanté dans cette ville et demeure le premier opérateur mobile de touraine .
Plus exactement , elle emploie directement 1930 personnes sur son site tourangeau , 519 à Cinq-Mars la Pile ( sources : Basile , 2006 ) .
Au début des années 1990 l' entreprise Citya immobilier nait à Tours avant de se développer en France .
C' est aussi le siège du 2 e site de production français de Michelin , avec pour principale activité tout le secteur poids-lourd ( 1300 emplois ) .
De plus , STMicroelectronics dispose d' un site de plus de 10 hectares à Tours-Nord avec 1700 employés .
Le nouveau quartier des Deux-Lions , entièrement équipé en fibre optique , concentre les nouvelles implantations comme le centre d' appel de Bouygues Telecom , le siège régional de la MAIF , un multiplex avec 12 salles , l' université de droit , d' économie et de sciences sociales , la section polytechnique ( spécialités en aménagement , informatique et productique ) .
Tours fait partie de l' Espace Metropolitain Val de Loire-Maine .
Tours est d' ailleurs une des deux villes-portes du Parc naturel régional Loire-Anjou-Touraine , avec Angers en Maine-et-Loire .
Les habitants redécouvrant sa grande diversité de faune et de flore depuis son classement au patrimoine mondial de l' UNESCO .
Il existe deux autres cinémas sur Tours : un en centre-ville ( 8 salles ) et le second au quartier des Deux-Lions ( 12 salles ) .
La bibliothèque municipale de Tours , classée , est située place Anatole France .
L' archevêque métropolitain de la ville est à la tête de la province ecclésiastique de Tours , qui comprend cinq diocèses dont celui de Tours même .
Le Tours FC ( ex FC Tours ) , après une période faste au tournant des années 1970-1980 avec quatre saisons dans l' élite et deux demi-finales de Coupe de France , le club a fait l' ascenseur et évolue en Ligue 2 à l' issue de la saison 2007-2008 de National .
Le Tours FC est présidé par Frédéric Sebag , le vice président est Christophe Bouchet , l' équipe quant à elle est entraînée par Daniel Sanchez .
Le club évolue au Stade de la Vallée du Cher .
Mais lors de l' été 2006 , le club est rétrogradé en Fédérale 2 pour raisons financières .
L' US Tours évolue actuellement en Fédérale 1 .
Il figure parmi les meilleurs et les plus appréciés de la région du fait d' un cadre exceptionnel en bord de Loire .
La " classique des lévriers " ou " classique des feuilles mortes " ( ce deuxième surnom est partagé avec le tour de Lombardie ) , la course cycliste Paris-Tours se déroule depuis 1896 , ancrant la ville dans la tradition du vélo .
L' objectif pour Tours est de devenir une métropole incontournable .
Tours est jumelée avec neuf villes étrangères :
