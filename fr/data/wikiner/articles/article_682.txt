Dans cette partie du détroit de Béring , d' après la situation géographique des îles Diomède , il n' y a qu' une vingtaine de kilomètres tout au plus entre deux terres .
Ces chasseurs n' ont jamais atteint la côte sud de l' Alaska et les îles Aléoutiennes .
Ils se sont plutôt répandus rapidement dans le Canada arctique et le Groenland à la poursuite de bœufs musqués ou mammifères marins .
En plus , de minuscules lames triangulaires servant de pointes de projectile constituaient très probablement le premier indice de l' usage de l' arc et de la flèche en Amérique du Nord .
De plus , les langues inuites ont d' importantes affinités avec celle des Aléoutes , ce qui laisse croire qu' elles ont possiblement une même origine .
Nous savons que ces derniers étaient des chasseurs des forêts nordiques de la Sibérie qui se sont adaptés aux régions de toundra et de banquise .
C' était la première phase d' extension territoriale d' une bonne partie de l' Arctique canadien et du Groenland , encore inhabité à cette époque .
En revanche , sur l' île Devon , on a plutôt trouvé de bonnes quantités d' os de phoque , de morse et d' ours polaire .
Dans la région d' Igloolik , un site daté au radiocarbone nous indique qu' il est vieux de 3900 ans .
Le territoire s' étend du district de Thulé au nord jusqu' au district de Nanortalik au sud .
Il semblerait qu' un nombre assez important d' individus ont occupé cette riche région côtière du Groenland .
Ils passaient l' été sur les côtes de la mer de Béring et durant les autres saisons , à l' intérieur des terres à la recherche de caribou et de poissons anadromes .
La technologie microlithique a sûrement pris racine dans la tradition paléolithique de l' Alaska et plus sûrement dans la culture paléosibérienne .
Le terme " paléoesquimau moyen " est utilisé comme expression générique regroupant plusieurs cultures régionales s' étendant de l' île Ellesmere à Terre-Neuve et du delta du fleuve Mackenzie jusqu' au Groenland .
Sur la Terre de Peary , le bœuf musqué était le principal mammifère terrestre disponible , le caribou en était totalement absent .
Nous avons trouvé aussi des sites d' occupations autour de la baie d' Hudson , au Labrador et à Terre-Neuve .
De son côté , les îles Aléoutiennes ont connu un développement graduel qui a débouché sur la culture des Aléoutes d' aujourd'hui .
La côte pacifique de l' Alaska , quant à elle , a connu une évolution technologique basée sur l' ardoise polie qui a pu être à l' origine des cultures esquimaudes de cette région .
Dès cette époque , de nouveaux villages permanents voient le jour le long des côtes de la mer de Béring .
Dès ce moment , il y eut un important accroissement démographique dans la partie septentrionale de l' Alaska .
Cette culture porte ce nom parce que c' est sur la côte nord-ouest du Groenland , près de la communauté de Thulé que l' on a identifié pour la première fois de vieilles maisons de type thuléenne .
Comme nous l' avons énoncé précédemment , la baleine boréale de l' Alaska ( ouest ) et celle du Groenland ( est ) étaient la ressource principale de ces populations .
Dans la région d' Igloolik , ils firent aussi la découverte de nombreux troupeaux de morses .
De plus , ces gens construisaient un autre type d' habitations hivernales complètement inconnu en Alaska : l' igloo .
Des harnais pour chiens apparaissent dans les sites thuléens du Canada à cette époque .
Pour ce qui est de la mer de Beaufort et du fleuve Mackenzie , les gens de l' endroit avaient un mode de vie semblable aux gens du nord de l' Alaska .
Les groupes de la région ont dû mourir de faim ou sont partis rejoindre leurs parents sur la côte nord-ouest du Groenland .
Une analyse isotopique du sang prélevé dans le cordon ombilical des bébés inuits , au moment de la naissance , montre que le plomb qui les contaminent ( 8 fois plus de bébés inuits étaient atteints de saturnisme à la naissance qu' au sud du Québec ) provient des cartouches de chasse , et non de retombées atmosphérique ou des poissons ou phoques , autres hypothèses avancées .
Québec refuse sur le champ , il a aussi des problèmes financiers .
Le 2 avril 1935 , le drame se déplace alors en Cour suprême .
Tout à coup , le Nord du Québec et ses habitants devenaient intéressants pour le gouvernement provincial .
Les élus provinciaux de l' époque découvraient les énormes potentiels de développement hydroélectrique à la baie James et dans le nord québécois .
Certains résidents de la baie d' Hudson ont même menacé de déménager sur les îles Belcher .
Mais le Québec ne voulait plus que ces services soient assurés par des agents fédéraux .
Elle a conclu que les programmes gouvernementaux de réinstallation au Labrador avaient été considérés par le gouvernement comme une fin en soi et non comme un élément d' un processus de développement .
On peut lire dans un rapport de 1943 , que les réinstallés entretenaient toujours la folle idée de retourner à Cape Dorset .
Pour cet auteur anonyme , la solution serait de les déplacer tous , dans deux ou trois villes du sud du Canada .
On pensa en effet , à l' implantation d' un village inuit à Hamilton ( Ontario ) , un autre à Winnipeg ( Manitoba ) et un dernier près d' Edmonton ( Alberta ) .
Encore pour des raisons non avouées de souveraineté sur l' archipel arctique , le gouvernement du Canada préparait l' une des plus tragiques histoires des régions nordiques .
Trois jours plus tard , 3 familles ( 16 personnes ) de Pond Inlet les rejoignent sur le pont d' acier du navire gouvernemental .
Elle est transcrite en caractères latins pour les Territoires du Nord-Ouest et du Labrador .
Pour le Nunavut et le Nunavik la transcription se fait en caractères syllabiques .
Au Nunavik , l' inuktitut n' est pas reconnu en tant que langue officielle mais est toutefois reconnu dans l' administration .
Le 18 décembre 1971 , les autochtones de l' Alaska saluaient le début d' une nouvelle ère .
La population inuite du Canada est composée d' environ 40 000 personnes .
