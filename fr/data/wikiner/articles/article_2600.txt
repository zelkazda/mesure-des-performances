Kenshin le vagabond ( るろうに剣心 , rurōni kenshin , Samurai X ) est un manga de Nobuhiro Watsuki .
Publié en français aux éditions Glénat , il comporte vingt-huit volumes au total et a été adapté en dessin animé .
De plus , une série de quatre OAV racontent le passé de Kenshin , et explique notamment la balafre en forme de croix présente sur sa joue .
Les deux premières saisons de la série ainsi que les OAV et le film sont disponibles en français ( intégral ou sous-titré ) chez Dybex .
L' histoire se passe en 1878 à Tōkyō .
Malgré son désir de ne plus tuer , Kenshin se fait rattraper par son passé et le gouvernement a souvent besoin de lui .
Mais Kenshin remporte la victoire .
C' est l' histoire d' un homme , Kenshin , mais aussi de tous les hommes qui essayent tant bien que mal de racheter un lourd passé .
Car c' est clairement là le thème du manga de Nobuhiro Watsuki : le voyage expiatoire d' un homme poursuivi par son passé .
Tout au long de son périple , Kenshin ne cessera de se battre contre ses blessures cachées , ravivées par les rencontres avec ses anciens adversaires ( voir notamment la liste des personnages ) .
Plus d' une fois , l' instinct d' assassin qui sommeille en lui menacera de reprendre le dessus , mais peu à peu , Kenshin découvrira le vrai sens de son combat .
Mais toujours demeurera l' espoir d' un monde meilleur , pour lequel Kenshin aura bataillé toute sa vie .
Même les pires meurtriers , comme Kenshin ou Tenken no Sôjiro , peuvent devenir des hommes meilleurs .
La série est entièrement disponible en français chez Glénat .
La série bénéficie d' une nouvelle édition appelé Kenshin perfect edition toujours chez Glénat depuis décembre 2009 .
Elle revient sur le passé de Kenshin , l' introduction étant une séquence remontant à la révolution .
Kenshin et ses amis rencontrent un groupe de personnes décidées à renverser le gouvernement actuel par la force .
Série de 4 OAV sur le passé de Kenshin , cela correspond à l' adaptation des chapitres des souvenirs dans le manga .
On y retrouve Kenshin enfant et son parcours durant la révolution qui a fait de lui l' assassin réputé ainsi que l' apparition de sa cicatrice .
Ces OAV apportent beaucoup de renseignements sur Kenshin , ils sont donc un complément non négligeable à la série animée même si le traitement plus dramatique et le dessin sont différents .
Cette histoire apparait néanmoins dans le manga original , de part des bribes de souvenirs de Kenshin .
Sous forme de souvenirs , le début permet de faire un petit résumé des principales aventures de Kenshin et ses compagnons .
