Guaraní s' en alla s' installer dans la région de l' actuel Paraguay où il établit une riche descendance .
Les langues du groupe tupi-guarani s' étendent le long des grands fleuves de l' est de l' Amérique du Sud : le Paraguay , le Parana , l' Uruguay ( tous trois des noms guaranis , guaí signifiant fleuve ) , l' Amazone .
Le groupe tupi-guarani est celui ayant la plus grande extension géographique des groupes linguistiques natifs de l' Amérique du Sud .
