Grace Murray Hopper ( 9 décembre 1906 -- 1 er janvier 1992 ) est une informaticienne et amiral de la marine américaine .
Elle est la conceptrice du premier compilateur en 1951 ( A-0 System ) et du langage COBOL en 1959 .
Elle enseigne les mathématiques au Vassar College et obtient un doctorat de mathématiques en 1934 de l' université Yale .
En 1943 , elle s' engage dans la Marine américaine et est affectée à l' équipe de Howard Aiken pour travailler sur le Harvard Mark I .
À partir de 1957 , elle travaille pour IBM , où elle défend l' idée qu' un programme devrait pouvoir être écrit dans un langage proche de l' anglais plutôt que d' être calqué sur le langage machine , comme l' assembleur .
De cette idée naîtra le langage COBOL en 1959 .
Elle est décorée à cette occasion de la Defense Distinguished Service Medal , plus haute distinction existante pour les non-combattants .
Elle est enterrée avec les honneurs militaires au cimetière national d' Arlington .
