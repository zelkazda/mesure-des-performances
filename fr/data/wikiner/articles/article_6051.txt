Son père , Zheng Zhilong ( 鄭芝龍 ) est un pirate , un marin et un marchand originaire d' une famille de pêcheurs du Fujian .
Il est élevé jusqu' à ses sept ans par sa mère puis son père l' emmène à Nan'an , près de Quanzhou au Fujian pour son éducation .
Il reçoit l' appui de Zheng Zhilong qui s' arrange pour que Zheng Chenggong serve à ses côtés .
La fidélité aux Ming n' est pas la seule raison qu' ont les pirates tels que Koxinga et Zheng Zhilong de combattre les Qing .
Ceux-ci ont en effet restreint le commerce par différentes mesures défavorables et la situation des pirates marchands devient beaucoup plus délicate que sous les Ming .
Koxinga n' est pas de cet avis et continue le combat .
Il pousse ses razzias jusqu' à Nankin mais est repoussé progressivement jusqu' à se sentir acculé .
