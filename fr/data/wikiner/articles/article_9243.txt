Il obtient le même poste pour la deuxième fois en 1998 , et décide d' effectuer des essais nucléaires à Pokharan , bien que l' Inde ne soit pas alors signataire du Traité de non-prolifération nucléaire ( TNP ) .
Sa décision entraîne un embargo de l' Inde sur les matières nucléaires sensibles .
