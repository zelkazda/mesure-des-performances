Il est bordé au nord par le Minnesota , par le Wisconsin et l' Illinois à l' est , par le Missouri au sud et par le Nebraska à l' ouest .
Les premiers explorateurs européens qui entrèrent dans le territoire actuel de l' Iowa furent des Français .
Après la guerre , la population de l' Iowa augmenta rapidement , passant de 674 913 habitants en 1860 à 1 194 020 en 1870 .
À partir de la Première Guerre mondiale , l' agriculture se modernisa et l' industrie se développa , surtout après 1945 .
La géologie de l' Iowa est en grande partie le résultat de la glaciation du Wisconsin : cette période , qui a concerné l' Amérique du Nord entre 85 000 et 7 000 ans avant notre ère , est marquée par l' extension de la calotte glaciaire au sud des Grands Lacs actuels .
Avec la fin de cette période glaciaire , les Grands Lacs se sont formés , alimentés par la fonte de l' inlandsis .
La région située au sud des Grands Lacs est aujourd'hui recouverte par une couche de lœss très fertile .
Le point le plus bas se trouve à Keokuk au sud-est de l' Iowa ( 146 m ) .
L' altitude moyenne de l' Iowa est de 335 m .
L' Iowa a une superficie de 145743 km² et est divisé en 99 comtés .
La capitale est Des Moines , dans le comté de Polk .
Les principaux lacs de l' Iowa sont :
Le climat de l' Iowa est de type continental .
Le total des précipitations est supérieur à celui de New York ( 841 mm ) et la saison la plus arrosée est l' été .
C' est la saison au cours de laquelle l' air polaire descend sur l' Iowa .
Les étés sont chauds et humides à cause des remontées d' air tropical venu du golfe du Mexique .
Il a ainsi succédé au démocrate Tom Vilsack .
L' Iowa est un petit état convoité lors des élections présidentielles .
Les industries de l' Iowa sont liées à l' agro-alimentaire et à l' agriculture .
L' Iowa est le premier producteur d' éthanol des États-Unis .
Des Moines possède un petit centre des affaires où le secteur des assurances est assez développé .
Le groupe de metal Slipknot vient de l' Iowa ( plus particulièrement de Des Moines ) , tout comme Stone Sour .
L' actrice Jean Seberg et l' acteur Toby Huss sont originaires de Marshalltown ( centre ) , et John Wayne est originaire de Winterset .
Les acteurs Elijah Wood et Michael Emerson sont originaires de Cedar Rapids .
