Trois d' entre eux ont donné leur nom à leur province ( Anvers , Liège et Namur ) .
Chacun des 15 comtés du Nouveau-Brunswick a un chef-lieu .
Il s' agit généralement de la principale commune ( sur le plan démographique ) du département , mais pas nécessairement : ainsi Châlons-en-Champagne est la préfecture de la Marne et de la région Champagne-Ardenne , alors que la principale ville tant du département que de la région est Reims .
Le chef-lieu de commune est la localité principale de la commune qui lui donne son nom ( la commune de Valdeblore constituant un contre-exemple ) , les autres localités de la commune étant qualifiées de hameaux .
Les typographes du Journal officiel réservent la majuscule à l' article en début du toponyme aux seules localités ayant rang de commune : on écrit donc la Défense et La Courneuve .
À Wallis-et-Futuna , seule collectivité habitée de la République française à ne pas être subdivisée en commune , mais en royaumes , districts et villages coutumiers , le village de Mata-Utu fait office de chef-lieu .
Le Grand-Duché de Luxembourg est divisé en deux arrondissements , trois districts , douze cantons et cent dix-huit communes .
