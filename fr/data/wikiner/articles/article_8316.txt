Sérant était le frère du journaliste et théoricien catholique Louis Salleron .
Il se rapproche des cercles mystiques du mage Gurdjieff et il prend connaissance des travaux traditionalistes de René Guénon .
Fidèle à cet héritage , il critiquera les positions de Louis Pauwels durant les années 1970 .
Dans ses pamphlets , Sérant critiqua notamment le " centralisme " jacobin .
Ses travaux comptent toutefois plusieurs études sur des figures d' extrême-droite et il défend volontiers des idéaux traditionalistes rappelant ceux de l' Action française .
