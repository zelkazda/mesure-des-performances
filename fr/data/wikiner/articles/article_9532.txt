Sa capitale était Bactres ( actuelle Balkh ) .
En 462 , une révolte de la Bactriane est écrasée .
Elle fut conquise par Ninus .
Lors de la conquête d' Alexandre le Grand ( 328 ) , elle formait alors une des grandes satrapies de la monarchie persane .
Bessus , satrape de Bactriane , assassina Darius III , son maître , afin de se rendre indépendant dans sa satrapie ; mais il n' y réussit pas : Alexandre joignit ce pays à ses conquêtes .
Il fut envahi en 130 par les nomades Yuezhi venu du nord .
À leur chute , les Arsacides de la Parthiène s' emparèrent de toutes leurs conquêtes à l' ouest ; les Scythes , 90 av .
J.-C. , prirent possession du reste , et fondèrent un nouveau royaume de Bactres dont les dimensions varièrent souvent .
La Bactriane de l' âge du bronze ( v. -3000 /v. -1200 ) , contrairement à la Mésopotamie et à l' Indus , n' a , à ce jour , livré aucun document écrit .
Marie-Nicolas Bouillet et Alexis Chassang ( dir .
