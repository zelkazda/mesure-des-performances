Les cellules ATM sont des segments de données de taille fixe de 53 octets ( 48 octets de charge utile et 5 octets d' en-tête ) , à la différence de paquets de longueur variable , utilisés dans des protocoles du type IP ou Ethernet .
La suite complète de standards ATM propose des définitions pour les couches de niveaux 1 , 2 et 3 du modèle OSI classique à 7 couches .
