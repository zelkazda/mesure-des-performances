Nicolas Maes , né à Dordrecht en 1632 et mort à Amsterdam en 1693 , est un peintre hollandais .
Il fut à Amsterdam l' élève de Rembrandt dont il garda une influence notable .
