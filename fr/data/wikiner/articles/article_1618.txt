Auteur de plusieurs essais et romans , Valéry Giscard d' Estaing est membre de l' Académie française depuis 2003 .
Après une classe préparatoire au lycée Louis-le-Grand , il entre à l' École polytechnique ( promotion 1944 ) .
Il séjourne à Montréal en 1948 : il y est professeur au Collège Stanislas .
Il épouse , le 23 décembre 1952 , Anne-Aymone Sauvage de Brantes .
De juin à décembre 1954 , Valéry Giscard d' Estaing , alors âgé de 29 ans , est nommé directeur adjoint au cabinet du président du conseil Edgar Faure .
Il accède en 1958 au mandat de conseiller général du canton de Rochefort-Montagne , mandat qu' il exerce jusqu' en 1974 .
Il fonde dans le même temps la Fédération nationale des républicains indépendants en proclamant : " Nous sommes l' élément centriste et européen de la majorité " .
À ce titre il soutient en 1969 la candidature de la Grande-Bretagne à l' adhésion de la Communauté économique européenne .
Son attitude envers le président de Gaulle devient de plus en plus critique ; il parle notamment d ' " exercice solitaire du pouvoir " et théorise son soutien critique , le " oui , mais " .
Il est , dans le même temps , depuis 1967 , maire de la commune de Chamalières , dans le Puy-de-Dôme , fonction qu' il assure jusqu' en 1974 .
Giscard d' Estaing est élu avec 50,81 % des suffrages le 19 mai 1974 , devenant ainsi le troisième président de la V e République et le plus jeune , à l' âge de 48 ans .
En matière industrielle , il engage la modernisation du transport ferroviaire en lançant l' étude sur le TGV .
Bien que personnellement favorable au remplacement de la peine de mort par la prison à vie " sans possibilité de remise de peine " , Valéry Giscard d' Estaing refusa la grâce à trois condamnés à mort ( Christian Ranucci en 1976 , puis Jérôme Carrein et Hamida Djandoubi en 1977 ) , estimant que tant que la peine de mort existait , elle devait être appliquée .
Ce seront les dernières exécutions en France .
Un sondage publié le mois précédent dans Le Point le donne vainqueur avec 57 % des voix face à Michel Rocard et 61 % face à François Mitterrand .
Le 2 mars 1981 , il annonce depuis le palais de l' Élysée qu' il brigue un second mandat .
Le président du RPR , ayant refusé d' appeler ses partisans à soutenir Valéry Giscard d' Estaing pour le second tour , est accusé de soutenir le candidat socialiste en ne se prononçant qu' à titre personnel en sa faveur .
Le président sortant obtient néanmoins le soutien du gaulliste Michel Debré .
Valéry Giscard d' Estaing dénonce une manipulation politique et nie avoir connu le passé de Papon .
Le 10 mai 1981 , Valéry Giscard d' Estaing perd le second tour de l' élection en recueillant 14642306 voix et 48,24 % des suffrages exprimés .
Valéry Giscard d' Estaing dirige l' Union pour la démocratie française qu' il avait créée en 1978 , de 1988 à 1996 .
Valéry Giscard d' Estaing succède , le 30 juin 1988 , à Jean Lecanuet à la tête du parti dont il a été le fondateur dix ans plus tôt , l' Union pour la démocratie française ( UDF ) .
Il favorisa l' implantation en Auvergne d' un second musée des volcans , Vulcania , qui a ouvert ses portes le 20 février 2002 à Saint-Ours-les-Roches .
Ses mandats furent marqués par le désenclavement de l' Auvergne , il favorisa le réseau routier auvergnat avec la construction entre autres de l' autoroute A89 .
Il fut aussi , de 1986 à 2004 , président du Parc naturel régional des Volcans d' Auvergne .
Ce succès renforce sa position de leader de l' opposition parlementaire , aux côtés de Jacques Chirac .
Valéry Giscard d' Estaing est élu président du Conseil des communes et régions d' Europe en 1997 , fonction qu' il conserve jusqu' en 2004 .
Valéry Giscard d' Estaing a précisé qu' il envisage " la programmation de concerts , de rencontres et de conférences " et souhaite également " faire une place pour [ ses ] archives personnelles de président de la Convention européenne " .
