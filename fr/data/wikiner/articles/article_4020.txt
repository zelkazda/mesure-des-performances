Il était le fils de Louis de Chénier .
Né à Galata d' une mère grecque et d' un père français , Chénier passe quelques années à Carcassonne , traduit dès l' adolescence des poètes grecs et s' enthousiasme pour la poésie classique .
Revenu en France , il fréquente les milieux littéraires et les salons aristocratiques .
Au moment des massacres de septembre , il était arrivé au Havre , d' où il aurait pu embarquer .
Il refusa néanmoins d' émigrer et revint à Paris pour participer aux tentatives faites pour arracher Louis XVI à l' échafaud .
Il fut vraisemblablement enterré avec les autres victimes de la Terreur dans le cimetière de Picpus à Paris .
Considéré par les romantiques comme leur précurseur , sa destinée a inspiré l' opéra vériste d' Umberto Giordano , André Chénier , dont la première eut lieu à La Scala de Milan , le 28 mars 1896 .
Son frère cadet , Marie-Joseph Chénier , était écrivain , dramaturge , et menait de pair une carrière politique .
