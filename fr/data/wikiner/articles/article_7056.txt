Elle fait partie du Parc naturel régional des Caps et Marais d' Opale , et en est d' ailleurs la commune la plus peuplée , avec ses 15004 habitants .
Si la commune offre de nombreux commerces , loisirs et services , elle est fortement dépendante d' Arques et notamment de sa cristallerie Arc International , deuxième employeur privé régional .
Ses habitants sont appelés les Audomarois .
La ville est traversée par l' Aa .
À son pied s' est formée l' agglomération autour de l' abbaye Saint-Bertin , et son sommet accueillait la future cathédrale Notre-Dame .
Saint-Omer est sous-préfecture du Pas-de-Calais , chef-lieu de l' Arrondissement de Saint-Omer , chef-lieu du canton de Saint-Omer-Nord et du canton de Saint-Omer-Sud , et ville-centre de la Communauté d' agglomération de Saint-Omer .
A l' ouest , les remparts créent un dénivelé au bas duquel se trouve un espace vert ; le sol y remonte jusqu' à la limite de Saint-Martin-au-Laërt .
Ainsi la rivière est devenue navigable depuis Saint-Omer jusqu' à la mer .
L' abbaye doit son nom à Bertin qui travailla comme compagnon d' Audomar .
L' Aa est canalisé dès 1165 jusqu' à Gravelines , qui constituera jusqu' à son ensablement l' avant-port de la cité audomaroise .
Saint-Omer fut perdue par le comté de Flandre au traité de Pont-à-Vendin du 25 février 1212 et devint une des principales places du comté d' Artois qui venait de se créer .
Ferrand de Flandre essaya de reprendre la ville mais il fut vaincu à la bataille de Bouvines .
Encore en 1507 la coutume de Saint-Omer précise dans son article 7 que " ses majeurs et eschevins ont accoustumé faire raidigier leurs dictes sentences criminelles en langaige flamang " .
De 1559 à 1790 la ville fut le siège du diocèse de Saint-Omer qui fut réuni en 1801 au diocèse d' Arras .
Siège et bataille de Saint-Omer le 26 juillet 1340 .
La ville fut l' objet de nombreux conflits entre la France et les Pays-Bas de 1477 à 1677 .
Saint-Omer accueillit alors de nombreux collèges et séminaires britanniques et wallons .
Il en va de même pour les autres comtés et duchés des Pays-Bas méridionaux .
Ses armées sont à nouveau devant Saint-Omer le 14 avril .
Les fortifications de la ville furent remaniées par Vauban dès 1678 .
En 1800 Saint-Omer était encore la ville la plus peuplée du département .
La partie centrale de la gare équipée d' un clocheton et d' ouvrages en fer forgé fut endommagée lors de la Seconde Guerre mondiale .
Dans la région de Saint-Omer furent construit par l' armée de l' Allemagne nazie le blockhaus d' Eperlecques en 1942 , et la Coupole d' Helfaut en 1943 .
Ces équipements , qui devaient à l' origine servir de bases de lancement des fusées V2 , furent bombardés par l' armée alliée et n' entrèrent jamais en service .
La vie économique de Saint-Omer et de ses alentours s' est développée autour de l' eau .
Saint-Omer est également un centre tertiaire et juridique important : cour d' assises du Pas-de-Calais , tribunal de grande instance , tribunal de commerce .
Saint-Omer possède une antenne de la Chambre de commerce et d' industrie du Grand Lille .
Saint-Omer est une sous-préfecture du département du Pas-de-Calais .
Saint-Omer fait partie de la communauté d' agglomération de Saint-Omer qui regroupe 19 communes , soit 65 000 habitants .
La ville fait partie du Pays de Saint-Omer .
La ville de Saint-Omer est jumelée avec :
L' évolution du nombre d' habitants depuis 1793 est connue à travers les recensements de la population effectués à Saint-Omer depuis cette date :
Après avoir perdu 3771 habitants entre 1968 et 1990 , Saint-Omer a vu sa population augmenter de 1313 habitants entre 1990 et 1999 grâce à un solde migratoire redevenu positif .
En 1999 Saint-Omer comptait 6709 résidences principales et 76 résidences secondaires .
Les centres hospitaliers les plus proches sont le centre hospitalier de la région de Saint-Omer à Helfaut , à 8 km au sud , et la clinique privée de Longuenesse .
Le centre culturel " La comédie de l' Aa " propose des pièces de théâtre et une programmation musicale .
Le marais Audomarois situé en grande partie sur le commune de Saint-Omer accueille de nombreuses espèces animales et végétales remarquables .
Non loin de Saint-Omer se trouvent :
Il y a , à Saint-Omer , 25 monuments historiques :
