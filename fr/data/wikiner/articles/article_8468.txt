Il faut rapprocher cette diffusion de celle du modèle architectural de Versailles auprès des cours étrangères .
Progressivement , une structure " normalisée " apparaît , et Johann Jakob Froberger est le premier compositeur à grouper de façon presque systématique les quatre mouvements de la suite classique : allemande , courante , sarabande et gigue .
Avant lui , Chambonnières et Louis Couperin composent de nombreuses danses qui ressortissent au genre , mais sans les assembler en des suites de composition imposée .
C' est Nicolas Lebègue qui , le premier , emploie le terme de " suitte " ( sic ) dans l' édition imprimée de ses œuvres .
Dès les années 1730 , si les compositeurs français composent toujours des suites , celles-ci ne comprennent presque plus d' airs de danse traditionnels , qui sont remplacés par des " pièces de caractère " , suivant l' exemple de Couperin et Rameau .
La suite peut aussi s' appeler , selon le compositeur , ordre , ouverture , partita ( en Allemagne ) voire sonate , principalement en Italie avant que ce terme ne soit réservé à une forme musicale différente et spécifique .
François Couperin utilise à plusieurs reprises une suite comme second volet de compositions plus ambitieuses incluant aussi les mouvements d' une sonate à l' italienne -- ce qu' il appelle " les goûts réunis " et ce que le marketing anglo-saxon en matière d' édition musicale appelle une " super-suite " .
Elle a aussi été pratiquée largement hors des frontières de la France .
En Angleterre , on pourrait considérer comme une suite embryonnaire le couple de danses " pavane & gaillarde " , fort en vogue chez les luthistes de l' époque élisabéthaine et chez les virginalistes .
