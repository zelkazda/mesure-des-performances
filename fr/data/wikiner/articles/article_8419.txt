Pour certains Seltz fait aussi partie de l' Outre-Forêt .
Seltz est à la merci des seigneurs de la région .
Prêtres séculiers
Liste des pasteurs réformés de Seltz
Liste des diacres réformés de Seltz
L' annexe de Seltz dépend de la paroisse luthérienne de Niederrœdern ( consistoire et inspection de Wissembourg ) .
En 1900 , la communauté luthérienne de Seltz et celle de Lauterbourg se constitue en vicariat jusqu' en 1919 .
Seltz redevient annexe de Niederrœdern jusqu' en 2006 .
Liste des pasteurs luthériens déservant Seltz
Les juifs de Seltz fréquentent la synagogue de Niederrœdern , érigée en 1869 et détruite en 1945 , rattachée au rabbinat de Lauterbourg jusqu' en 1910 puis au rabbinat de Wissembourg .
Suite à une crûe du Rhin en 1307 et à la destruction de la 2 e abbaye , le pape autorisa la construction de la 3e abbaye .
La chapelle funéraire des Fleckenstein date de la 1 re moitié du 16 e siècle .
En 1682 , Louis XIV donna Seltz aux Jésuites .
