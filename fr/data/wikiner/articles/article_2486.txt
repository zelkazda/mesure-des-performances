Située à 190 kilomètres de la frontière italienne , elle est établie sur les bords de la mer Méditerranée , le long de la rade de Toulon .
Toulon appartient à la Communauté d' agglomération Toulon Provence Méditerranée , qui rassemble douze communes et 428 403 habitants en 2009 soit 41 % de la population du département du Var .
Située entre mer et montagnes , capitale économique du Var , Toulon bénéficie de nombreux atouts naturels .
Le port de commerce de Toulon est le premier port français pour la desserte de la Corse .
En 2008 , 1.032.908 passagers ont embarqué depuis le Var , ce qui représente 65 % du trafic vers la Corse .
Ce boom est lié à la présence , depuis 2001 , de la compagnie maritime Corsica Ferries .
Toulon est enfin dotée de certains équipements culturels importants .
On la considère parfois comme la première ville à l' ouest de la Côte d' Azur .
Au nord de la ville , le mont Faron est un massif calcaire du jurassique supérieur et du crétacé , boisé en pinède .
Il culmine à 584 mètres d' altitude et offre un panorama extraordinaire sur Toulon , sa rade et les villes environnantes .
La rade de Toulon est fermée à l' est par la presqu'île de Giens et au sud par celle de Saint-Mandrier qui la séparent de la pleine mer , elle se divise en grande rade et petite rade à l' ouest , contigüe à Toulon , à La Seyne et à Saint-Mandrier .
C' est dans cette rade que les trois pionniers français de la plongée : Philippe Tailliez , Jacques-Yves Cousteau et Frédéric Dumas surnommés depuis 1975 " les trois mousquemers " , ont commencé leurs carrières et expérimenté leurs premiers engins .
Des lignes régulières relient la Corse au port de commerce de Toulon .
C' est notamment le port d' attache du porte-avions Charles-de-Gaulle .
Il s' étend des limites de la ville de la Seyne-sur-Mer jusqu' à la limite du port civil de Toulon .
Ce marché a inspiré une chanson de Gilbert Bécaud .
Préfet de la ville jusqu' en 1851 , le Baron Haussmann avait pour ambition de restructurer Toulon .
Il faudra attendre 1852 pour que l' empereur se déplace à Toulon et ordonne le commencement des travaux .
Mais Haussmann va tout de même suivre l' agrandissement de la ville .
Ainsi , on dote Toulon d' importants boulevards et autres avenues .
