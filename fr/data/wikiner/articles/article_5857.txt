Il est député de la 12 e circonscription du Pas-de-Calais depuis le 2 juillet 1981 et maire de Liévin depuis le 30 juillet 1981 , deux mandats occupés auparavant par Henri Darras , mort en cours de mandat , dont il était le suppléant .
Avant sa première élection , il enseignait l' histoire-géographie au lycée Henri Darras de Liévin .
