Courtonne-les-Deux-Églises est issue en 1972 de la fusion des communes de Courtonne-la-Ville et de Saint-Paul-de-Courtonne ( 205 habitants , au sud sur la rive gauche ) .
Cette dernière avait auparavant absorbé en 1824 la commune de Livet , au nord-est , qui comptait 46 habitants ( 1821 ) , Saint-Paul-de-Courtonne en comptant alors 406 .
À la création des cantons en 1793 , Courtonne-la-Ville est chef-lieu de canton .
Courtonne-la-Ville a compté jusqu' à 987 habitants en 1831 et c' est 10 ans plus tard que Saint-Paul-de-Courtonne a atteint son maximum démographique avec 410 habitants .
