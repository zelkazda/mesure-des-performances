Notons que quelques villes au monde portent le même nom comme Lucerne , en Suisse .
Ce pays d' Outre-Mer ne peut être que l' Angleterre .
Il est plus que probable que l' appellation Outremer soit une marque de ces liens politiques , religieux et architecturaux d' un royaume qui s' étend des deux côtés de la Manche .
