De parents italiens , il naquit à Alexandrie où sa famille avait émigré , le père travaillant à la construction du canal de Suez .
En 1914 il revint en Italie , et au début de la Première Guerre mondiale s' engagea volontaire pour partager le destin de ses contemporains .
Il mourut à Milan le 2 juin 1970 .
