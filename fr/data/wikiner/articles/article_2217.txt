Chrétien de Troyes , sans doute originaire de Troyes , né vers 1135 et mort vers 1185 , est un poète français , considéré comme un des premiers auteurs de romans de chevalerie .
Dans le prologue de sa dernière œuvre , le Conte du Graal , il indique être au service de Philippe d' Alsace .
Dans cette perspective , l' auteur de Cligès aurait été le fils cadet d' une famille aristocratique qui aurait été destiné à la carrière cléricale .
Mais cette hypothèse se heurte au fait que Chrétien rattache son nom à la ville de Troyes , et non , comme cela aurait l' usage pour un aristocrate , à un château ou à un fief .
Il a été également évoqué la possibilité d' une origine juive de Chrétien , Troyes ayant été l' un des centres européens de la culture judaïque ( avec notamment Rachi , mort en 1105 .
Chrétien a écrit cinq romans chevaleresques en vers octosyllabiques .
Sa principale œuvre est celle des romans de la table ronde avec pour représentant le roi Arthur .
Ce personnage , a priori principal , n' est pourtant pas au centre des quêtes qu' invente Chrétien de Troyes .
La base de ses romans est bien souvent la quête implicite du personnage vers la reconnaissance et la découverte de soi , comme vers la découverte des autres , à l' image d' une intégration à la cour et de l' amour de la reine Guenièvre .
La cour du roi Arthur est un lieu fixe dans tous les romans de Chrétien de Troyes .
Dans l' introduction de Cligès , Chrétien indique qu' il est également l' auteur de cinq autres œuvres antérieures à ses romans : quatre sont des adaptations d' Ovide en langue vernaculaire , dont une seule nous est parvenue ; la cinquième est une version de Tristan et Iseut , malheureusement disparue .
