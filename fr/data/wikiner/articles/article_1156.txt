Il a été le secrétaire général du Parti communiste français de 1994 à 2001 et son président de 2001 à 2002 .
Dès ses seize ans , en 1962 , il adhère à la Jeunesse communiste puis un an plus tard au Parti communiste .
Après des études d' infirmier à Paris , il exerce ce métier en psychiatrie au centre de santé d' Argenteuil .
Au PCF , fidèle de Georges Marchais , il gravit un à un les échelons et est élu en 1977 maire de Montigny-lès-Cormeilles .
Cette affaire intervient quelques jours avant un meeting de Georges Marchais à Montigny-lès-Cormeilles et avant même que le Front national grappille des voix au Parti communiste et que le thème de l' immigration soit porteur .
En 1987 , il entre au comité central du Parti communiste , puis en 1990 au bureau politique .
En 1994 , alors qu' il est quasiment inconnu , Georges Marchais fait de lui son successeur : il devient alors secrétaire national du parti .
Poussé par la chute du mur de Berlin et l' érosion idéologique et politique du Parti communiste , Robert Hue entame une politique de mutation du parti dont il vient de prendre la tête : ouverture vers d' autres mouvements , abandon d' un certain nombre de doctrines , création d' un exécutif à deux têtes ( il devient président du parti alors que Marie-George Buffet devient secrétaire nationale ) ...
Il devient député de la 5 e circonscription du Val-d'Oise , et quelques élus communistes entrent au gouvernement .
Pendant ce temps le PCF continue de perdre du terrain : le nombre d' adhérents passe de 200000 en 1998 à 138000 en 2001 .
Jean-Marie Le Pen , qui à la surprise générale est présent au second tour , clame que le " Parti communiste a disparu " .
Une souscription est lancée alors qu' il quitte la présidence du parti , la laissant aux mains de la secrétaire nationale Marie-George Buffet .
Le poste de président du PCF , créé par lui , cesse d' exister avec son départ .
Un an plus tard , Robert Hue est cependant élu sénateur du Val-d'Oise .
Le 28 novembre 2008 , il quitte le conseil national du PCF , déclarant qu' il ne quitte pas le parti mais effectue néanmoins une " rupture " avec lui , ne le jugeant " plus réformable " .
