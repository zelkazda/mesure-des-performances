La capitale de la Wallonie est Namur .
Selon le mouvement wallon , la Wallonie a toujours été une terre romane depuis l' époque romaine , sous la forme d' une avancée latine au sein de l' Europe germanique .
L' émergence d' une idée identitaire wallonne et d' un mouvement wallon organisé a produit différents symboles et manifestations célébrant la Wallonie .
La première eut lieu à Verviers le 21 septembre 1913 .
La Communauté française de Belgique ne reprendra que les armoiries et le drapeau .
La Wallonie est représentée politiquement par la Région wallonne depuis 1980 .
On utilisa couramment le terme Wallonie pour désigner les territoires des diverses langues régionales romanes en Belgique ( qui devinrent au fil du temps de plus en plus francophones , les langues régionales gardant une réelle vitalité ) , ainsi délimités y compris les extensions de la Province de Liège .
Dans le même temps la Flandre a réussi son envol , notamment en se tournant vers le commerce international , et ajoute à la prépondérance numérique qu' elle exerçait depuis longtemps le poids dominant de sa réussite économique .
Les Communautés bétonnent en 1970 les accords territoriaux de 1932 et de 1963 , ce qui satisfait aux exigences flamandes axées sur le culturel et la défense de la langue .
Namur est la capitale officielle de la Wallonie .
Les autorités wallonnes ont officialisé cette état de fait par la désignation de " capitales " spécialisées : Mons pour la culture , Liège pour l' économie , Charleroi pour le social et Verviers pour l' eau .
Le courant rattachiste voit dans la Wallonie une terre appartenant à la République française , de par sa langue , ses habitants et leurs idées .
La Wallonie peut se situer approximativement à partir du bassin hydrographique de la Meuse , fleuve européen .
À l' exception de la province du Brabant wallon , au sud de Bruxelles , et une grande partie du Hainaut à l' ouest , qui jouxte la France , le reste de la Wallonie -- sauf deux ou trois enclaves , notamment du côté du Grand-Duché de Luxembourg -- appartient au bassin mosan .
La quatrième grande ville , Mons , se situe à l' ouest de Charleroi dans la partie hors bassin mosan du Hainaut .
En 2007 , près de 300 espèces invasives étaient inventoriées en Wallonie , dont 9 % sont reprises dans la liste noire ( espèces ayant un impact environnemental élevé ) .
Forêt : elle est plus présente qu' en Flandre ( 67 % de la population wallonne habite à moins de 700 mètres à vol d' oiseau d' un massif de plus de 5 hectares ) ; 45 % des wallons disent s' y promener plus d' une fois l' an .
Rappelons que les matières culturelles sont de la compétence de la Communauté française de Belgique .
Ce terme ne désigne pas une institution représentant les Français résidant en Belgique , mais bien une entité fédérée compétente dans le domaine de la culture et de l' enseignement dans les régions de langue française ( y compris la région bilingue de Bruxelles-Capitale ) .
Certains politiciens wallons souhaiteraient que les compétences de la Communauté soient transférées à la Région , mais il est incertain de savoir quel support ce type de projet aurait auprès de la population
Il existe enfin un courant important en Wallonie qui est proche du Manifeste pour la culture wallonne et de ce que les Québécois appellent la culture québécoise , soit une volonté de considérer la Wallonie comme émancipée culturellement tant de la Belgique que de la France .
et Ellezelles ( Hainaut ) .
