Il est le père de Robert Hossein .
À partir de 1958 , André Hossein a beaucoup composé pour les films réalisés par son fils , avec une certaine prédilection pour les petits ensembles inspirés du Modern Jazz Quartet ( vibraphone , piano , basse et percussions ) , parfois enrichis de cuivres .
