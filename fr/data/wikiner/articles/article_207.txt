Les îles les plus proches sont l' île Saint-Vincent-et-les Grenadines et Sainte-Lucie , à l' ouest .
La superficie totale de la Barbade est d' environ 430 kilomètres carrés ( 166 miles carrés ) , et est principalement de faible altitude , avec les pics les plus élevés à l' intérieur du pays .
Le point le plus élevé de la Barbade est le mont Hillaby dans la paroisse de Saint Andrew .
L' île a un climat tropical , avec des alizés de l' océan Atlantique maintenant des températures douces .
En 2006 , l' indice de développement humain de la Barbade était le 37 e plus élevé au monde ( 0,889 ) .
La Barbade est divisée en 11 paroisses :
La Barbade est une île relativement plate , se relevant doucement dans la région centrale montagneuse , le point le plus élevé est le mont Hillaby à 336 m .
L' île est située sur une position légèrement excentrée dans l' océan Atlantique comparée aux autres îles des Caraïbes .
La capitale est Bridgetown .
La Barbade a pour codes :
