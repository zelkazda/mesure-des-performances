Johannes Brahms [ joːˈhanəs ˈbʁaːms ] ( né le 7 mai 1833 à Hambourg , mort le 3 avril 1897 à Vienne ) est un compositeur , pianiste et chef d' orchestre allemand .
Johannes Brahms est l' un des plus importants musiciens de la période romantique et est considéré par beaucoup comme le " successeur " de Ludwig van Beethoven .
Sa première symphonie a été décrite par Hans von Bülow comme étant " la dixième symphonie de Beethoven " .
Brahms a passé la plupart de sa carrière à Vienne où il était l' une des figures importantes sur la scène musicale .
À la différence d' autres grands compositeurs de musique classique , Johannes Brahms n' a jamais composé d' opéras .
Étant également un pianiste virtuose , il a donné la première représentation de beaucoup de ses compositions ; il a aussi travaillé avec les musiciens célèbres de son époque , dont notamment la pianiste Clara Schumann et le violoniste Joseph Joachim .
Brahms était un perfectionniste intransigeant qui a détruit beaucoup de ses travaux et laissé quelques-uns non publiés .
Brahms était à la fois un traditionaliste et un innovateur .
Il était un maître du contrepoint , une méthode de composition rigoureuse pour laquelle Bach est célèbre , ainsi que du développement musical , une technique de composition introduite par Haydn , Mozart et Beethoven .
Alors que beaucoup de ses contemporains ont critiqué sa musique qu' ils ont trouvée trop académique , ses œuvres ont été admirées par la suite par des personnalités aussi diverses que le progressiste Arnold Schoenberg et le conservateur Edward Elgar .
Il se produit dans des petits ensembles à Hambourg .
L' art de Johann Sebastian Bach , de Wolfgang Amadeus Mozart et de Ludwig van Beethoven le marqueront à jamais .
Ses talents de pianiste lui permettront d' honorer , dès l' âge de treize ans , des engagements dans les tavernes d' Hambourg .
Plus tard , Brahms confiera :
Le 21 septembre 1848 , il donne son premier concert , qui inclut une fugue de Bach .
Un deuxième concert suivra le 14 avril 1849 : Brahms y joue la sonate opus 53 de Beethoven et des variations de sa composition .
Ce dernier fait la remarque suivante sur Brahms :
Joachim conseille à Brahms de s' adresser à Franz Liszt qui , à cette époque , est chef d' orchestre à la cour de Weimar .
Liszt promet à Brahms de le mentionner dans une lettre à l' éditeur Breitkopf & Härtel .
Cependant , le jeune compositeur ne se trouve que peu d' affinités avec les théories musicales progressistes de Liszt .
Il écrit alors une lettre à Joseph Joachim , datée du 29 juin 1853 , dans laquelle il lui demande de le rejoindre à Göttingen .
Cette période heureuse et insouciante , pleine de rencontres , inspirera à Brahms son Ouverture pour une fête académique .
Joachim et Liszt persuaderont Brahms de rendre visite à Robert Schumann qui est directeur de musique à Düsseldorf .
Schumann écrit :
Schumann demande à l' éditeur Breitkopf & Härtel de publier quelques œuvres de Brahms .
Cet empressement effraie Brahms : dans une lettre à Schumann , il exprime son appréhension de ne pas pouvoir répondre à toutes les attentes du public .
À Düsseldorf , Brahms fait la connaissance de Robert Schumann , et également celle de son épouse Clara .
Ils vivent dans la même maison à Düsseldorf .
Entre 1854 et 1858 , Clara Schumann et Brahms échangent de nombreuses lettres , témoignages qu' ils se sont ensuite accordés à détruire presque entièrement .
Il nous reste encore aujourd'hui quelques lettres de Brahms ; elles reflètent l' image d' une passion grandissante .
Cette lettre sera la dernière avant l' évènement prévisible et pourtant soudain qui bouleversera la nature même de leur liaison : le décès de Robert Schumann le 29 juillet 1856 .
Le 17 octobre 1857 , Brahms finira par résumer ainsi dans une de ses missives :
Pendant toute la période de la maladie de Schumann , Brahms réside à Düsseldorf .
Il est souvent interprété comme le reflet de sa passion vaine pour Clara Schumann ; leur histoire venant tout juste de se terminer .
Il sera joué pour la première fois , le 22 janvier 1859 à Hanovre puis , le 27 du même mois , à Leipzig , sans toutefois récolter le succès espéré .
Un été , il s' adonnera à sa nouvelle passion avec tant de fougue , que Clara Schumann sera vexée qu' il ait rencontré une autre femme aussi vite .
Peu après leurs fiançailles , Brahms change d' avis : il se sent incapable d' avoir une liaison .
En mai 1859 , il revient dans sa ville natale d' Hambourg , trouvant qu' il ne disposait pas assez de temps pour la composition .
Il compose de la musique de chambre et de nombreuses variations pour piano : sur un thème original , sur un thème hongrois , sur un thème de Haendel , sur un thème de Schumann ( à quatre mains ) .
Ce dernier , en éditant ses œuvres , a été un acteur déterminant dans la diffusion de l' œuvre de Brahms auprès du public , car il n' était pas toujours facile pour Brahms dans les années 1860 , de publier ses propres compositions .
Le perfectionnisme de Brahms est un autre obstacle : souvent , il fait patienter son éditeur avant l' envoi de ses manuscrits , car il lui semble qu' il peut encore apporter une amélioration à l' œuvre .
En 1862 , il s' installe définitivement à Vienne .
Brahms confie s' y sentir rapidement chez lui .
Brahms n' affectionne que très peu cet encombrant compliment et craint d' être considéré comme l' égal de Beethoven .
Il rencontre Karl Goldmark tandis que sa renommée ne cesse de croître .
Parmi les œuvres qu' il a écrites par la suite , on trouve notamment le Requiem allemand et les Danses hongroises .
En revanche , la publication des Danses hongroises pour lesquelles Brahms s' est inspiré d' airs tsiganes très connus , a presque causé un scandale .
En 1870 , il rencontre le chef d' orchestre Hans von Bülow qui fera beaucoup pour sa musique .
À cette époque , Brahms est un pianiste couronné de succès et gagne bien sa vie .
Brahms écrit ses quatre symphonies en l' espace de neuf ans , ce qui est un temps record .
Brahms reçoit le titre de docteur " honoris causa " de l' université de Cambridge en 1877 et celui de l' université de Breslau en 1881 .
Par la suite , Brahms a essentiellement composé de la musique de chambre ( sonates pour violon et violoncelle ) .
En 1886 , il devient président d' honneur de l' association des musiciens de Vienne .
Pendant les vingt dernières années de sa vie , Brahms qui est devenu une personnalité influente de la scène musicale internationale , est admiré et vénéré en tant que pianiste , chef d' orchestre , et compositeur .
En 1889 , il devient citoyen d' honneur de la ville de Hambourg .
Brahms décède à Vienne le 3 avril 1897 , à l' âge de soixante-trois ans , d' un cancer du foie selon quelques biographies , mais il s' agirait en réalité d' un cancer du pancréas .
Le 14 septembre 2000 , Johannes Brahms fut la cent-vingt-sixième personne et le treizième compositeur à être reçu dans le Walhalla .
Brahms est encore de nos jours souvent nommé comme le successeur légitime de Ludwig van Beethoven .
Liszt et Richard Wagner avaient commencé à réfléchir à la musique du futur .
Dans l' autre camp , chez les traditionalistes , se trouvaient Karl Goldmark , Joseph Joachim , Brahms et le critique musical Eduard Hanslick , dont la prise de position en faveur de la musique de Brahms a été à la base d' une grande amitié .
Leur but était ce que Brahms avait coutume d' appeler la musique durable , qui était de développer une musique qui soit indépendante de l' histoire .
Brahms et Wagner gardèrent une distance certaine toute leur vie .
Alors que Brahms ne le mentionnait pas , Wagner ne pouvait s' empêcher d' exprimer son dédain pour la musique de Brahms .
Néanmoins , Brahms ne tenait pas Wagner comme un concurrent sérieux , car il avait essentiellement composé des opéras , un genre qui n' a jamais tenté Brahms .
Parmi les compositeurs plus ou moins liés avec Wagner , Brahms n' estimait que Felix Draeseke et Anton Bruckner comme des rivaux sérieux pour leurs compositions en musique de chambre , de chœur et d' orchestre .
Une autre personne s' est révélée un grand admirateur des conservateurs : Hans von Bülow .
C' était initialement un Wagnérien , mais il changea d' opinion après que sa femme Cosima l' eut quitté pour Wagner .
Il n' a pas seulement été influencé par Beethoven , mais aussi par Johann Sebastian Bach , Haendel et Palestrina .
D' influences diverses , marquée par une grande science du contrepoint et de la polyphonie , l' esthétique de Brahms reste , dans ses formes classiques , profondément marquée par la nostalgie de l' époque romantique , mais d' une troublante originalité , avec des couleurs musicales magnifiques , des mélodies inventives et des rythmes surprenants par leur superposition .
Ce balancement lourd et incertain , né de la superposition de valeurs binaires et ternaires que l' on retrouve dans sa musique , est la caractéristique de cette mélancolie brahmsienne née d' une sorte de complexe d' infériorité issu des années de jeunesse que Brahms a passées à jouer dans les tavernes de Hambourg .
Les travaux des musicologues parlent de trois périodes dans la création des œuvres chez Brahms .
La première va jusqu' au Requiem allemand , la deuxième jusqu' au deuxième concerto pour piano , et la troisième commence avec la troisième symphonie .
Brahms n' a que peu écrit pour le piano , sur un catalogue comprenant pourtant près de 122 numéros , mais ces œuvres sont magistrales et sont très souvent jouées .
