Le lac de Paladru est un lac naturel ( glaciaire ) des pré- Alpes .
Cette origine est perceptible par la présence , au sommet des collines environnantes , de blocs erratiques , arrachés aux sommets des Alpes et abandonnés à des altitudes de 800 m lors du retrait du glacier .
Il donne naissance à la Fure dont le nom évoque la violence du torrent primitif .
Une prise en surface et un éclusage furent aménagés en 1869 pour réguler le cours de la Fure en saison sèche et permettre l' exploitation de toutes ces usines .
La pluviométrie est irrégulière avec des maximums en juin et octobre à la faveur des remontées des masses d' air humide de la Méditerranée .
Dès lors , les revendications des communes et des riverains se heurtent aux fermages renouvelés par les anciens propriétaires de retour d' émigration ou revenant sur le renoncement de leurs droits ( cas des Barral ) .
Le site est très fréquenté par les grenoblois ( Grenoble est à 30 minutes en voiture ) .
L' hôtellerie renforce ses capacités pendant la saison ( hôtels-restaurants , chambres d' hôtes , campings ) notamment à Charavines et Paladru .
La vallée de la Bourbre ( Virieu ) et le val d' Ainan ( Chirens et Saint-Geoire-en-Valdaine ) de chaque côté du lac de Paladru apportent un complément culturel au site .
