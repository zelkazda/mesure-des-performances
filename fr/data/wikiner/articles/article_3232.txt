De conviction gaulliste , il est élu sans interruption député de la deuxième circonscription de Seine-et-Marne depuis 1967 .
Il fait partie du groupe RPR , puis UMP .
Didier Julia recevra le soutien du groupe RPR et le gouvernement socialiste reculera .
En 1995 il soutient la candidature d' Édouard Balladur à l' élection présidentielle .
Élu à l' issue du second tour , il entame sa 41 e année à l' Assemblée nationale , confirmant ainsi son statut de doyen de mandat .
Depuis longtemps Didier Julia est très proche et bien connu de la société irakienne , et notamment de Tarek Aziz .
Le but de ce voyage était de convaincre le gouvernement irakien d' accepter la venue sur son territoire d' observateurs de l' ONU , le président américain ayant décidé de suspendre la menace de guerre si les observateurs pouvaient s' assurer de l' absence " d' armes de destruction massive " .
Le 27 septembre , Didier Julia communique au gouvernement français des informations sur leur lieu de détention mais celui-ci ne leur donne " aucune suite en raison de leur caractère imprécis et non étayés " .
Le 1 er mars 2005 , une cassette vidéo a été découverte en Irak où la journaliste française Florence Aubenas , prise en otage depuis le 5 janvier 2005 , fait appel à " Monsieur Julia " pour la sauver .
Didier Julia déclare se mettre à leur disposition dès que les sanctions ( " contrôle judiciaire " ) qui visent ses co-équipiers seront levées .
Après sa libération en juin 2005 , Florence Aubenas a déclaré que l' appel à Didier Julia avait été fait à la demande de ses ravisseurs afin d' obtenir de la publicité .
L' instruction judiciaire , conduite par les juges antiterroristes , se conclut par une ordonnance de non-lieu ( qui comprend 18 pages ) en faveur de Didier Julia et de ses compagnons .
