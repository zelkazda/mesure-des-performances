La cannelle est connue depuis l' Antiquité , et elle était utilisée par les anciens Égyptiens dans le processus de l' embaumement .
La Bible , Hérodote et d' autres auteurs classiques y font référence .
L' arbre est cultivé un peu partout dans le monde , mais la meilleure qualité est produite au Sri Lanka .
Les quantités de coumarine sont toutefois faibles dans la cannelle du Sri Lanka , contrairement à la casse ( ou cannelle casse ) qui en contient beaucoup plus .
