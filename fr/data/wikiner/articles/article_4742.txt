On y trouve des objets bizarres , de la photo originale d' Elvis Presley à l' amulette qui guérit l' arthrite en passant par la carte de baseball dédicacée par un joueur mythique .
Au-delà de la simple exploitation d' un élément fantastique , cette histoire présente l' histoire de mœurs d' une petite ville provinciale américaine , et une galerie intéressante de personnages qui reviennent de façon récurrente dans plusieurs romans ou nouvelles de Stephen King .
Ce roman est aussi considéré par l' auteur comme la partie finale d' une trilogie non-officielle initiée par La part des ténèbres et poursuivi dans Le Molosse surgi du soleil , une nouvelle publié dans Minuit 4 .
Bazaar est un roman de Stephen King comportant beaucoup de références et de connexions à ses autres œuvres .
