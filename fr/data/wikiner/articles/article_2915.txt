Il est député de la 7 e circonscription du Bas-Rhin et maire de Saverne .
Ancien membre de l' UDF , il rejoint l' UMP en 2002 .
En 1998 , il devient député suite à la démission d' Adrien Zeller , devenu président de la région Alsace , et dont il était le suppléant .
