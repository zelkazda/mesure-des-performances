Le roi Nadir Shah le nomme major général en 1932 ; il sert comme commandant militaire de plusieurs provinces et , de 1939 à 1947 , dirige les forces centrales armées de Kaboul .
Le 22 juillet 1975 a lieu dans le Pandjchir la première révolte islamiste , à laquelle participent Burhanuddin Rabbani , Ahmad Shah Massoud et Gulbuddin Hekmatyar .
Des hommes prennent les armes dans tout le nord-est de l' Afghanistan contre le régime de Kaboul considéré comme athée , et sont écrasés par l' armée acquise aux communistes .
