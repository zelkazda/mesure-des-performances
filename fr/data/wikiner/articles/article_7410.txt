Parallèlement , le colonel Georges Picquart , chef du contre-espionnage , constate en mars 1896 que le vrai traître avait été le commandant Ferdinand Walsin Esterházy .
Un processus de scission en deux de la France est entamé , qui se prolonge jusqu' à la fin du siècle .
On dénombre plusieurs morts à Alger .
Le régime politique de la France vient d' affronter trois crises qui n' ont fait que l' affermir .
Cette instabilité gouvernementale se double d' une instabilité présidentielle : au président Sadi Carnot , assassiné le 24 juin 1894 , succède le modéré Jean Casimir-Perier qui démissionne le 15 janvier 1895 et est remplacé par Félix Faure .
Suite à l' échec du gouvernement radical de Léon Bourgeois en 1896 , le président nomme Jules Méline , homme du protectionnisme sous Ferry .
C' est sous ce gouvernement stable qu' éclate réellement l' Affaire Dreyfus .
De nombreux acteurs de l' affaire Dreyfus sont d' ailleurs alsaciens .
La Section de statistiques est créée en 1871 mais ne compte alors qu' une poignée d' officiers et de civils .
Sa mission militaire est claire : récupérer des renseignements sur l' ennemi potentiel de la France , et l' intoxiquer avec de fausses informations .
C' est ce qui l' amène aux origines de l' affaire Dreyfus .
Cette croissance de l' antisémitisme , très virulente depuis la publication de La France juive d' Édouard Drumont en 1886 ( 150000 exemplaires la première année ) , va de pair avec une montée du cléricalisme .
De brillants officiers juifs , atteints par une série d' articles de presse de La Libre Parole , accusés de " trahir par naissance " , défient leurs rédacteurs .
Le lancement de La Libre Parole , dont la diffusion estimée est de 200000 exemplaires en 1892 permet à Drumont d' élargir encore son audience vers un lectorat plus populaire , déjà tenté par l' aventure boulangiste dans le passé .
L' origine de l' affaire Dreyfus , bien que totalement éclaircie depuis les années 1960 , a suscité de nombreuses controverses pendant près d' un siècle .
Celles-ci n' étaient pourtant pas exceptionnelles , puisqu' on privilégiait les officiers de l' est de la France pour leur double connaissance de la langue allemande et de la culture germanique , .
Entre alors en scène le commandant du Paty de Clam , , homme original qui se pique d' expertise graphologique .
Malgré les conseils de prudence et les objections courageusement exprimés par Gabriel Hanotaux lors d' un petit conseil des ministres , il décide de poursuivre .
Déçu , Mercier fait alors appel à Alphonse Bertillon , l' inventeur de l' anthropométrie judiciaire , mais nullement expert en écritures .
Le 15 octobre 1894 au matin , le capitaine Dreyfus subit cette épreuve , mais n' avoue rien .
Le 29 octobre , l' affaire est révélée par le journal antisémite d' Édouard Drumont , La Libre Parole , dans un article qui marque le début d' une très violente campagne de presse jusqu' au procès .
Le 1 er novembre , Mathieu Dreyfus , le frère d' Alfred , appelé d' urgence à Paris , est mis au courant de l' arrestation .
Sans attendre , il se met à la recherche d' un avocat , et retient l' éminent pénaliste Edgar Demange .
Le 3 novembre , à contre-cœur , le général Saussier donne l' ordre d' informer .
Le manque complet de neutralité de l' acte d' accusation conduit Émile Zola à le qualifier de " monument de partialité " .
C' est aussi l' occasion pour les titres extrémistes comme La Libre Parole ou La Croix , de justifier leurs campagnes préalables contre la présence de Juifs dans l' armée , sur le thème " On vous l' avait bien dit !
Ce huis clos n' est d' ailleurs pas conforme juridiquement puisque le commandant Picquart et le préfet Louis Lépine sont présents à certaines audiences en violation du droit , mesure qui permet néanmoins aux militaires de ne pas divulguer le néant du dossier au grand public et d' étouffer les débats .
Les discussions de fond sur le bordereau montrent que le capitaine Dreyfus ne pouvait pas en être l' auteur , .
Alphonse Bertillon , qui n' est pas expert en écritures , est présenté comme un savant de première importance .
De plus , le commandant Hubert-Joseph Henry fait une déclaration théâtrale en pleine audience .
Dans cette éventualité , la Section de statistiques avait préparé un dossier , contenant , en principe , quatre preuves " absolues " de la culpabilité du capitaine Dreyfus , accompagnées d' une note explicative .
Le 5 janvier 1895 , la cérémonie de la dégradation se déroule dans une cour de l' École militaire à Paris .
Le 17 janvier , il est transféré au bagne de l' île de Ré , où il est maintenu plus d' un mois .
Le lendemain , le navire fait cap vers la Guyane .
Le 12 mars , après une traversée pénible de quinze jours , le navire mouille au large des îles du Salut .
Ainsi , le journaliste libertaire Bernard Lazare se penche sur les zones d' ombre de la procédure .
La France reste alors très majoritairement antidreyfusarde .
En mars 1896 , Picquart , qui avait suivi l' affaire Dreyfus dès son origine , exige désormais de recevoir directement les documents volés à l' ambassade d' Allemagne , sans intermédiaire .
Très ému par sa découverte , Picquart diligente une enquête en secret , sans l' accord de ses supérieurs .
Homme à la personnalité trouble , à la réputation sulfureuse , criblé de dettes , il est pour Picquart , un traître probable animé par un mobile certain : l' argent .
Picquart , qui ignore tout du faux Henry , se sent rapidement isolé de ses collègues militaires .
Picquart se confie à son ami , l' avocat Louis Leblois , à qui il fait promettre le secret .
Sans citer Picquart , le sénateur révèle l' affaire aux plus hautes personnalités du pays .
Mathieu Dreyfus avait fait afficher la reproduction du bordereau , publiée par Le Figaro .
Le 11 novembre 1897 , les deux pistes se rejoignent , à l' occasion d' une rencontre entre Scheurer-Kestner et Mathieu Dreyfus .
La collusion destinée à éliminer Picquart semble avoir échoué .
Le mouvement dit dreyfusard , animé par Bernard Lazare , Mathieu Dreyfus , Joseph Reinach et Auguste Scheurer-Kestner s' élargit .
Devant les menaces de désabonnements massifs de ses lecteurs , le directeur du journal cesse de soutenir Zola .
Blum tente fin novembre de faire signer à son ami Maurice Barrès une pétition demandant la révision du procès , mais ce dernier refuse , rompt avec Zola et Blum début décembre , et commence à populariser le terme d ' " intellectuels " .
Si l' Affaire Dreyfus occupe de plus en plus les discussions , le monde politique ne le reconnaît toujours pas , et Jules Méline déclare en ouverture de séance de l' Assemblée nationale , le 7 décembre : " il n' y a pas d' affaire Dreyfus .
Il n' y a pas en ce moment et il ne peut pas y avoir d' affaire Dreyfus . "
Le vrai coupable , lui dit-on , est le lieutenant-colonel Picquart .
En réaction à l' acquittement , d' importantes et violentes émeutes antidreyfusardes et antisémites ont lieu dans toute la France .
Premier grand intellectuel dreyfusard , il est alors au sommet de sa gloire : les vingt volumes des Rougon-Macquart ont été diffusés dans des dizaines de pays .
Clemenceau trouve le titre : " J' Accuse … ! " .
L' article comporte de nombreuses erreurs , majorant ou minorant les rôles de tel ou tel acteur , mais Zola n' a pas prétendu faire œuvre d' historien .
Le but de Zola est de s' exposer volontairement afin de forcer les autorités à le traduire en justice .
D' autre part , les conflits d' opinion tentent de peser sur les juges ou le gouvernement , pour obtenir les uns la révision et les autres la condamnation de Zola .
Le 15 janvier , Le Temps publie une pétition réclamant la révision du procès .
Le procès s' ouvre dans une ambiance de grande violence : Zola fait l' objet " des attaques les plus ignominieuses " , tout comme d' importants soutiens et félicitations
Fernand Labori , l' avocat de Zola , fait citer environ deux cents témoins .
La réalité de l' Affaire Dreyfus , inconnue du grand public , est diffusée dans la presse .
Cependant , les nationalistes , derrière Henri Rochefort , sont alors les plus visibles et organisent des émeutes , forçant le préfet de police à intervenir afin de protéger les sorties de Zola à chaque audience .
Zola est condamné à un an de prison et à 3000 francs d' amende , la peine maximale .
Cependant , le procès Zola est plutôt une victoire pour les dreyfusards .
Le sénateur Ludovic Trarieux et le juriste catholique Paul Viollet fondent la Ligue pour la défense des droits de l' homme .
Plus encore que l' affaire Dreyfus , l' affaire Zola opère un regroupement des forces intellectuelles en deux camps opposés .
Quant au colonel Picquart , il se retrouve à nouveau en prison .
La France expose un arbitraire étatique contredisant les principes républicains fondateurs .
Après une heure d' interrogatoire mené par le ministre lui-même , Henry s' effondre et fait des aveux complets .
Il est placé aux arrêts de forteresse au Mont-Valérien et se suicide , le lendemain en se tranchant la gorge avec un rasoir .
La Libre Parole , journal antisémite de Drumont , propage la notion de " faux patriotique " .
Le même journal lance en décembre une souscription au profit de sa veuve , afin d' ériger un monument à la gloire d' Henry .
Le clivage transcende les religions et milieux sociaux , comme l' illustre la célèbre caricature de Caran d' Ache " Un dîner en famille " .
Le 1 er novembre , le progressiste Charles Dupuy est nommé à la place de Brisson .
Émile Loubet est élu , une avancée pour la cause de la révision , le précédent président en étant un farouche opposant .
Le 23 février , à la faveur des funérailles de Félix Faure , Déroulède tente un coup de force sur l' Élysée .
Le 4 juin , Loubet est agressé sur le champ de course de Longchamp .
Les républicains progressistes antidreyfusards , tel Méline , sont rejetés à droite .
L' affaire Dreyfus a conduit à une recomposition claire du paysage politique français .
Puis Picquart démontre l' ensemble des rouages de l' erreur puis de la conspiration .
Pour de nombreux Dreyfusards cette décision de justice est l' antichambre de l' acquittement du capitaine ; ils oublient de considérer que c' est de nouveau l' armée qui le juge .
Le 9 juin , il quitte l' île du Diable , cap vers la France , enfermé dans une cabine comme un coupable qu' il n' est pourtant plus .
Après cinq années de martyre , il retrouve le sol natal , mais il est immédiatement enfermé dès le 1 er juillet à la prison militaire de Rennes .
Mathieu Dreyfus a imaginé une complémentarité entre les deux avocats .
Rennes est en état de siège .
Pourtant , Mercier se fait huer à la sortie de l' audience .
Le gouvernement , devant le raidissement militaire du procès , pouvait agir encore de deux manières pour infléchir les événements ; en faisant appel à un témoignage de l' Allemagne ou par l' abandon de l' accusation .
L' ambassade d' Allemagne adresse un refus poli au gouvernement .
Le ministre de la guerre , le général Gaston de Galliffet , fait envoyer un mot respectueux au commandant Louis Carrière , commissaire du gouvernement .
Waldeck-Rousseau , dans une position difficile , aborde pour la première fois la grâce .
C' est dans cet esprit que le 17 novembre 1899 , Waldeck-Rousseau dépose une loi d' amnistie couvrant " tous les faits criminels ou délictueux connexes à l' Affaire Dreyfus ou ayant été compris dans une poursuite relative à l' un de ces faits " .
Les réactions en France sont vives , faites de " stupeur et de tristesse " dans le camp révisionniste .
Aussi , les manifestations sont très peu nombreuses en province , alors que l' agitation persiste quelque peu à Paris .
Dans une apostrophe à l' armée , Galliffet annonce : " l' incident est clos " .
La diplomatie des trois puissances , avec l' aide de l' Angleterre , va s' employer à détendre une atmosphère qui ne se dégradera à nouveau qu' à la veille de la Première Guerre mondiale .
À l' occasion de perquisitions à la Section de statistiques , il découvre de très nombreuses pièces dont la majorité sont visiblement fabriquées .
Ce qui est annulé est non seulement l' arrêt de Rennes , mais toute la chaîne des actes antérieurs , à commencer par l' ordre de mise en jugement donné par le général Saussier en 1894 .
Le colonel Picquart est lui aussi réhabilité officiellement et réintégré dans l' armée au grade de général de brigade .
L' affaire Dreyfus a-t-elle laissé une trace ?
Pour certains , l' affaire Dreyfus a marqué la société française au fer rouge .
L' affaire fait revivre l' affrontement des deux France .
À court terme , les forces politiques progressistes , issues des élections de 1893 , confirmées en 1898 , en pleine affaire Dreyfus , disparaissent en 1899 .
Car la progression désinhibée d' un nationalisme de type populiste est une autre grande conséquence de l' événement dans le monde politique français , et ce même s' il n' est pas né avec l' affaire Dreyfus , puisque le nationalisme est théorisé par Maurice Barrès dès 1892 .
Le nationalisme connaît des hauts et des bas , mais parvient à se maintenir en tant que force politique , sous le nom d' Action française , jusqu' à la défaite de 1940 , lorsque après cinquante ans de combat , elle accède au pouvoir et peut , vieux rêve de Drumont , " purifier " l' État avec les conséquences que chacun sait .
Mais son engagement devient résolu , aux côtés de Georges Clemenceau à partir de 1899 , sous l' influence de Lucien Herr .
Les deux partis fusionnent en 1905 en une Section française de l' Internationale ouvrière ( SFIO ) .
La création de la Ligue française pour la défense des droits de l' homme et du citoyen est contemporaine de l' affaire .
Conséquence finale sur le plan politique , le tournant du siècle voit un renouvellement profond du personnel politique , avec la disparition de grandes figures républicaines , à commencer par Auguste Scheurer-Kestner .
L' affaire Dreyfus répand la haine raciale dans toutes les couches de la société , mouvement certes initié par le succès de La France juive de Drumont en 1886 , mais énormément amplifié par les divers épisodes judiciaires et les campagnes de presse pendant près de quinze ans .
Le passage à l' acte est permis par l' avènement du régime de Vichy , qui laisse libre cours à l' expression débridée et complète de la haine raciale .
Cela dit , le rôle de la presse est limité par la diffusion des titres , à la fois importante à Paris et faible à l' échelle nationale .
" L' affaire [ … ] agit comme un catalyseur dans la conversion de Herzl " .
Devant la vague d' antisémitisme qui l' accompagne , Herzl se " convainc de la nécessité de résoudre la question juive " , qui devient " une obsession pour lui " .
L' assimilation ne résoudra pas le problème parce que le monde des gentils ne le permettra pas , comme l' affaire Dreyfus l' a si clairement démontré " .
L' affaire Dreyfus se distingue par le nombre important d' ouvrages publiés à son sujet .
Le grand intérêt de l' étude de l' affaire Dreyfus réside dans le fait que toutes les archives sont aisément disponibles .
À commencer par l' opuscule de Bernard Lazare , premier intellectuel dreyfusard : malgré des erreurs factuelles , il reste un témoignage des étapes vers la révision .
Par ailleurs , l' affaire Dreyfus a fourni le prétexte à de nombreux romans .
D' autres auteurs y contribueront , comme Roger Martin du Gard , Marcel Proust ou Maurice Barrès .
