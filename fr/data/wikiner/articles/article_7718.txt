Siège de l' abbaye cistercienne de Fontenay , fondée en 1119 par saint Bernard , abbé de Clairvaux .
Elle fut pillée au cours de la guerre de Cent ans et des guerres de Religion , puis mise en commende .
