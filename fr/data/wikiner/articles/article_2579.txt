Angel Heart ( エンジェル・ハート , angel heart ? ) est un seinen manga écrit et dessiné par Tsukasa Hōjō .
Angel Heart repose sur l' univers de City Hunter tout en développant une histoire alternative parallèle .
Il ne faut ainsi donc pas voir dans Angel Heart une véritable suite à City Hunter .
Parallèlement , Kaori , qui est sur le point de se marier avec Ryô Saeba , meurt en tentant de sauver un enfant sur le point de se faire renverser par un camion .
Angel Heart permet au mangaka de renouer avec l' atmosphère sombre des premiers volumes de City Hunter .
