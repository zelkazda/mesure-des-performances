Antigua-et-Barbuda est un pays des Antilles ; sa capitale est Saint John 's .
Christophe Colomb y débarqua en 1493 , lors de son second voyage .
Elles furent d' abord colonisées par les Espagnols et les Français , ensuite par les Anglais .
Le seul village de Barbuda porta son nom .
Codrington et les autres propriétaires firent amener des esclaves de la côte ouest de l' Afrique .
En 1981 , Antigua-et-Barbuda devint indépendante et entra au Commonwealth .
Elle adhéra le 18 juin 1981 à l' Organisation des États de la Caraïbe orientale ( OECO ) .
Les îles de Barbuda et Redonda ont toutes deux le statut de dépendance .
Antigua-et-Barbuda ont pour codes :
