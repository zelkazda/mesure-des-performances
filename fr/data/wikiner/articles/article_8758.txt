A l' origine , en reprenant les idées de Henri Fayol le management a pour objectif de veiller aux fonctions :
De même , Fayol indique que le management doit réaliser :
Taylor désirait appliquer les principes généraux d' amélioration de la productivité par la division du travail à l' entreprise qu' Adam Smith avait soulignés ( avant lui Platon au niveau de la société ) .
Il partage aussi l' idée avec Henry Ford qu' une augmentation des rendements peut être obtenue en contrepartie de bons salaires .
Parallèlement , Henri Fayol propose une approche similaire , avec un même souci de précision et de rationalité , à l' administration et à l' organisation bureaucratique .
A partir des années 20 , Mary Parker Follett l' introduit dans la réflexion managériale .
L' analyse du pouvoir auprès de sociologues comme par exemple Max Weber et Michel Crozier , puis de la stratégie de l' entreprise sont enfin devenus des thèmes de management que des auteurs comme Peter Drucker , Henry Mintzberg et Michael Porter parmi d' autres , ont participé à explorer .
Conformément à la règle selon laquelle il faut " expliquer le social par le social " , Émile Durkheim utilise la méthode des variations concomitantes , afin d' établir un lien entre le suicide et d' autres faits sociaux .
Emile Durkheim distingue certains types de suicides pouvant être reliés au management :
