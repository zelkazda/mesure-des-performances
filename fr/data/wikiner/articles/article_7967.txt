On considère généralement la musique de Bach comme la référence de l' équilibre de ces deux aspects de l' écriture musicale .
C' était d' ailleurs la marque des plus grands maîtres que de savoir produire de telles combinaisons de thèmes distincts , et Bach , Haendel , n' y ont pas manqué .
Ces différents procédés sont illustrés de la façon la plus magistrale ( et systématique ) qui soit dans trois œuvres de Jean Sébastien Bach :
