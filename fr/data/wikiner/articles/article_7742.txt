Guts est le personnage principal du manga et de la série animée Berserk .
Les mercenaires cessent alors les recherches , convaincu que Guts est mort .
Griffith , dont l' ambition est sans limite , est persuadé que Guts est celui qui lui permettra de se hisser au sommet .
Celui-ci qui ne l' accepte pas et défie à nouveau Guts en duel , mais est battu .
Guts et Casca furent les seuls survivants , mais à quel prix ?
Guts est donc présenté d' emblée comme un foudre de guerre impitoyable .
Mais bien plus que cette empathie naturelle avec le combat , Guts est un personnage possédant une colère et une rage inexorable , rage qu' il tente d' enfouir au plus profond de son cœur pour ne pas faire du mal à ses proches , mais qui ressort immanquablement tôt au tard .
Par facilité , Guts est donc un personnage très solitaire .
Mais malgré tout , Guts n' arrive pas à évoluer seul , et c' est d' ailleurs l' origine de sa haine envers son ancien chef et seul vrai ami .
Celle-ci en main , Guts met en pièces son ennemi .
Lorsque Guts est pris d' une rage incontrôlable , le casque de l' armure se rabat , symbole du côté sauvage et de la violence aveugle du guerrier .
Une fois activée , l' armure permet à Guts de se battre sans se fatiguer , mais c' est à double tranchant .
