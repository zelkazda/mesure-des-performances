La cuisine indienne ayant eu une influence importante sur la gastronomie réunionnaise , on retrouve une variante de ce mélange à la Réunion sous le nom de massalé .
À La Réunion , on trouve toutes sortes de plats au massalé , le plus connu étant le massalé de cabri ( terme utilisé pour chèvre ) .
