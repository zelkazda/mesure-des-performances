Ils provenaient peut-être de l' île de Gotland .
Au début de notre ère , ils s' installèrent dans la région de l' estuaire de la Vistule .
De là viendrait le nom des Goths .
Mais en 488 , ils conquièrent l' Italie sous Théodoric , à la demande de l' Empire byzantin .
Les Gépides ( peuplade proche des Goths ) , qui ont suivi les Goths dans leur fuite vers le sud formaient dès lors un troisième peuple " gotique " distinct dans la classification de Cassiodore .
Ils restèrent dans l' arrière-pays , proche des Carpates , plutôt subordonnés au pouvoir des Goths .
Les Wisigoths s' établirent au nord du Danube alors que les Ostrogoths colonisèrent l' embouchure de la Dniepr ainsi que la Crimée .
La question de l' origine des Goths est un puzzle historique et philologique important .
D' après les historiens grecs et romains , les Goths s' établirent en 238 sur les bords du Danube .
Il y a aussi une vaste disparition de population dans l' Ostrogothie dans le sud de la Suède au temps de l' apparition des Goths en Pologne .
Les écrits de Jordanès ne comportent que peu de vérité historique ; il s' agit plutôt d' un récit mythologique .
Il semble aujourd'hui que les Goths proviennent plutôt d' une réunion polyethnique de peuplades présentes au niveau de l' actuelle Pologne .
Il est toutefois sûr que les Goths ne formaient pas un peuple homogène .
Le nom des deux derniers peuples ( et peut-être des Gépides ) ont la même signification et semblent avoir une origine commune .
Ce qui est sûr , c' est que les Goths se répandirent relativement lentement le long de la Vistule vers l' amont du fleuve , puis jusqu' à la mer Noire et au Danube .
D' un point de vue archéologique , la culture de Wielbark s' y transforme en culture de Tcherniakov au niveau de l' actuelle Ukraine .
Ils sont encore nommés dans les archives comme Skythai , un terminus technicus de l' historiographie antique pour désigner les peuplades barbares de la mer noire .
Le nouvel empereur Dèce perdit de nombreuses batailles et fut finalement vaincu à la bataille d' Abrittus en 251 .
L' empereur suivant , Trébonien Galle négocia la paix avec les Goths .
Cependant cela n' empêcha pas les Goths de s' agiter à nouveau en 253 , envahissant la Thrace et la Mésie .
Sur place , Marcus Aemilius Aemilianus , gouverneur de Mésie , connut plusieurs succès militaires .
En 254 , les Goths font une incursion jusqu' à Thessalonique .
A partir de 255 , les Goths menèrent des attaques maritimes sur la côte est de la mer noire .
En 267 , les Goths pillèrent la Thrace .
En 268 , une seconde vague d' attaque commence , sur terre comme sur mer contre Byzance , traverse les Dardanelles et pille le Péloponnèse .
l' empereur Claude II le Gothique rassembla toutes les forces disponibles et marcha sur les Balkans , menacés par une invasion de Goths qui sévissaient déjà dans les provinces danubiennes .
Il remporta en 268 à la bataille de Naissus en Mésie supérieure une victoire très difficilement acquise et peu décisive .
En 271 , ils furent repoussés jusqu' au Danube .
Pendant ce temps , les Goths restés en Ukraine établirent un vaste et puissant royaume le long de la mer Noire .
Ils devinrent les Ostrogoths .
Selon Jordanès , la défaite face aux Huns provoqua également le suicide du roi ostrogoth Ermanaric en 378 .
Les termes semblant appartenir au gotique , retrouvés dans les manuscrits postérieurs de Crimée , ne correspondent peut-être pas exactement à la même langue .
Le cadre religieux des Goths est un culte villageois mêlant la vénération des ancêtres et des offrandes sacrificielles
Les Goths parlaient une langue germanique .
Aujourd'hui , la langue des Goths est morte jusqu' aux traces qu' elle a laissées dans le vocabulaire de la langue romaine .
Il faisait partie des choses que les Wisigoths laissèrent durant leur fuite face aux Huns .
L' aigle était le symbole par excellence des Goths depuis l' époque où ils vécurent sur les rives de la mer Noire .
