On peut considérer qu' il s' agit de l' un des plus anciens instrument de musique électronique , avec le thérémin mis au point en Russie en 1917 .
C' est la renaissance de cet instrument , utilisé par des artistes populaires comme Jacques Brel , Joe Jackson , Gorillaz , ou Jonny Greenwood ( du groupe Radiohead ) .
L' ondéa est aussi présente dans les tournées de Yann Tiersen dans le monde .
