On a pensé qu' il avait pu séjourner aussi dans celui de Boussac ( ce qui n' a jamais été établi ) .
Suivant d' autres sources , tout aussi fantaisistes , ces tapisseries auraient été réalisées à Aubusson : on sait qu' il n' en est rien .
Dans cet article , George Sand cite huit tapisseries ( alors que six seulement nous sont connues ) .
Les commentaires qu' elle ajoute à propos de ces tapisseries et de leur relation avec le séjour du prince turc " Zizim " à Bourganeuf relèvent toutefois de l' imagination la plus fertile. < /ref > .
Et c' est elle , très vraisemblablement , qui en signala l' existence à son éphémère amant , Prosper Mérimée , inspecteur des monuments historiques , qui visita la région en 1841 et les fit classer au titre des monuments historiques .
À la fin du roman , Tracy Chevalier a ajouté , avec beaucoup d' honnêteté , deux pages précisant ses sources avec quelques notes ( non exemptes d' erreurs ) sur les origines et l' histoire de ces tapisseries .
