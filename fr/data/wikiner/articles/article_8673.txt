On ne sait rien sur la vie de Gaspard Le Roux ; en dehors de son œuvre,il n' est mentionné que par quelques citations dont une dans une liste de professeurs réputés à Paris .
Gaspard Le Roux précise en préambule que ces œuvres peuvent être jouées sur d' autres instruments .
Cette œuvre s' inscrit dans la tradition française entre celles de D' Anglebert et de Couperin .
On a toujours noté la similitude du thème d' une gigue en la majeur de Le Roux et du prélude de la suite anglaise de même tonalité de Jean-Sébastien Bach .
