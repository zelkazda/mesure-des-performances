Le mot robot , qui apparait pour la première fois dans sa pièce de théâtre de science-fiction R. U. R. , a été inventé par son frère Josef .
Karel Čapek naît en Bohême et fait ses études secondaires à Hradec Králové , qu' il doit quitter pour Brno à la suite de la découverte du cercle anti-autrichien dont il faisait partie .
Il est réformé en raison de problèmes de dos qui lui poseront problème toute sa vie , et dispensé de participer aux combats lors de la Première Guerre mondiale qui néanmoins l' influença et l' inspira .
Dans les années 1925 -- 1933 il est président du PEN club tchécoslovaque .
En 1938 , l' annexion des Sudètes suite aux accords de Munich par les troupes nazies affecte profondément le démocrate nationaliste qu' il est ; sa santé se détériore rapidement et il meurt de pneumonie le 25 décembre de la même année a Prague .
Karel Čapek écrivit avec humour et intelligence sur une grande variété de sujets .
Comme toute l' intelligentsia tchécoslovaque de son temps , Čapek est francophile et participe à la diffusion de la culture française dans son pays .
