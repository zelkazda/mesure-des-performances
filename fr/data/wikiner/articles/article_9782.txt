Jean Reno naît à Casablanca où ses parents , originaires d' Andalousie -- son père était de Sanlúcar de Barrameda et sa mère était de Jerez de la Frontera -- ont fui le régime de Franco .
La famille s' installe ensuite en France en 1960 .
Après avoir accompli son service militaire en Allemagne , Jean Reno , de retour en France , se lance dans une carrière de comédien , montant une compagnie théâtrale avec Didier Flamand .
Il fait des apparitions remarquées dans des films tels que Clair de femme de Costa-Gavras ( 1979 ) ou Le Dernier Combat de Luc Besson ( 1983 ) .
Sa collaboration avec ce dernier pour Le Grand Bleu , Nikita ou encore Léon lui confèrera une notoriété nationale puis internationale ; c' est à cette occasion qu' il apprend l' anglais , le film étant tourné dans cette langue .
Fait rare , Jean Reno est également très connu au Japon , notamment depuis sa prestation d' acteur dans Wasabi en 2001 , avec Ryōko Hirosue .
Jean Réno a été marié trois fois et a cinq enfants :
Par ailleurs , il a déjà chanté à deux reprises avec Johnny Hallyday , pour les 85 ans de Charles Aznavour et pour les Enfoirés .
Il est aussi un grand fan de Formule 1 .
Il avait même lu le programme de Nicolas Sarkozy pour les aveugles sur le site de campagne de ce dernier .
Outre les familles Sarkozy et Hallyday , Jean Reno compte comme amis proches Muriel Robin , Ron Howard , Charles Aznavour , Elton John et Vanessa Paradis .
