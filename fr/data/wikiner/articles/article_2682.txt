Elle est entourée géographiquement à l' ouest par l' Océan Atlantique , au nord par l' Angola , au sud par la République d' Afrique du Sud , à l' est par le Botswana et au nord-est par le Zimbabwe et la Zambie .
La Namibie est une république parlementaire .
Depuis 1990 , la Namibie est divisée en 13 régions :
L' ancien Sud-Ouest africain se divise en 4 grands secteurs :
Dans les autres secteurs , la Namibie reste dépendante de son voisin sud-africain .
En 1999 , la Namibie est le premier pays d' Afrique à proposer l' ouverture de ses réserves naturelles aux chasseurs fortunés du monde entier .
La Namibie est un des pays les plus arides et les moins peuplés du monde .
Des bochimans , de langue khoïsan , forment la population autochtone mais sont très minoritaires , même si les Namaquas sont près de 200000 .
La population est Chrétienne à 80 % .
La République de Namibie a pour codes :
