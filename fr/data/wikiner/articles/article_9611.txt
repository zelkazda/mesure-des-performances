Le groupe de la Banque mondiale est quant à lui un ensemble de cinq établissements .
Son siège est à Washington .
Elle fait partie des institutions spécialisées du système de l' Organisation des Nations unies ( ONU ) .
Le 9 mai 1947 , elle approuva son premier prêt , qui fut accordé à la France pour un montant de 250 millions de dollars .
La Banque mondiale a été créée principalement pour aider l' Europe et le Japon dans leur reconstruction , au lendemain de la Seconde Guerre mondiale , mais avec le mouvement de décolonisation des années soixante , elle se fixa un objectif supplémentaire , celui d' encourager la croissance économique des pays en voie de développement africains , asiatiques et latino-américains .
Au départ , la Banque mondiale a principalement financé de grands projets d' infrastructures ( centrales électriques , autoroutes , aéroports … ) .
L' appellation Groupe de la Banque mondiale désigne depuis juin 2007 cinq institutions :
Les objectifs de la Banque mondiale ont évolué au cours des années .
En réponse aux critiques , la Banque mondiale a adopté une série de politiques en faveurs de la sauvegarde de l' environnement et du social , visant à s' assurer que leurs projets n' aggravaient pas le sort des populations des pays aidés .
En dépit de ces politiques , les projets de la Banque mondiale sont souvent critiqués par les organisations non gouvernementales ( ONG ) pour ne pas lutter efficacement contre la pauvreté , et négliger les aspects sociaux et environnementaux .
En février 2009 , un rapport d' audit interne a indiqué que des employés de la banque avaient autorisé l' injection de fonds dans un projet immobilier en Albanie , mais que les informations qu' ils avaient utilisées étaient incomplètes ou sciemment faussées .
La Banque mondiale compte environ 10000 employés .
Pour le cycle 2005-2008 , la Banque mondiale a versé 17,7 milliards de dollars ( soit 12,2 milliards d' euros ) aux pays en développement .
Les bailleurs de fonds les plus importants étaient les États-Unis ( 13,8 % du total ) , le Royaume-Uni ( 13,2 % ) , le Japon ( 12,2 % ) , l' Allemagne ( 8 % ) et la France ( 7,1 % ) .
Pour la première fois , la Chine est devenue contributeur , et le Royaume-Uni est passé devant les États-Unis comme premier bailleur de fonds .
Selon une règle tacite , le directeur du FMI est désigné par les gouverneurs européens alors que le président de la Banque mondiale est désigné par le gouvernement américain , les États-Unis étant le principal actionnaire de la Banque mondiale .
Depuis le 1 er juillet 2007 , Robert Zoellick est le président de la Banque mondiale .
L' action de la Banque mondiale est souvent critiquée , cependant pour deux raisons opposées .
D' autre part , les mouvements altermondialistes accusent la Banque Mondiale de répondre davantage aux exigences des multinationales qu' à celles des populations locales .
" Des crises de légitimité engendrent des projets de réforme , tant à l' ONU qu' au Fonds monétaire international et à la Banque mondiale .
Bien que les Européens bénéficient réciproquement de la présidence du FMI .
Le 1 er mai 2007 , le président du Venezuela , Hugo Chavez a annoncé son intention de retirer son pays de la Banque mondiale et du Fonds monétaire international avec ces mots : " Il vaut mieux que nous en sortions avant qu' on nous ait pillés " et après avoir procédé en 2006 au remboursement total de la dette extérieure vénézuélienne .
Joseph Stiglitz , Prix Nobel d' économie et ancien responsable de la Banque mondiale , estime que la nomination de Robert Zoellick " est dans la continuité de celle de Paul Wolfowitz , dont le mandat a été " une catastrophe " " .
Pascal Lamy souligne cependant " son talent d' artisan du consensus et sa capacité de tendre la main aux pays en développement " .
