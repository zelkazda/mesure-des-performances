Paul Lafargue voit le jour le 15 janvier 1842 à Santiago de Cuba .
Il émigre à Londres où il rencontre Friedrich Engels et Karl Marx ( en février 1865 ) , dont il épouse la fille , Laura , en 1868 .
Paul Lafargue et Laura Marx sont enterrés au cimetière du Père-Lachaise ( division 77 ) , face au Mur des Fédérés .
