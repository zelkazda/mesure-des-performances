En 1806 , il séjourne brièvement à la cour de Napoléon I er après la conquête du duché de Saxe-Cobourg-Saalfeld par les troupes napoléoniennes .
En 1808 , il accompagne Alexandre I er , pendant sa rencontre avec Napoléon à Erfurt .
Il participe ainsi , en tant que colonel d' un régiment de cavalerie russe , aux campagnes de 1807 , 1808 , 1813 et aux batailles de Lützen , Bautzen et Leipzig contre les troupes Françaises en 1814 .
Il devient ainsi l' un des principaux conseillers de sa nièce , la future reine Victoria .
A la faveur des événements internationaux , le prince Léopold va être amené à quitter le sol britannique .
Il accepte à la condition que soit réglé le problème des frontières et des dettes de la Belgique .
Roi officiellement depuis le 26 juin 1831 , il prête serment à Bruxelles le 21 juillet suivant .
Il défend en personne la route de Bruxelles .
Pour défendre son nouveau titre royal , et en gratitude à la France , il épousera la fille de Louis-Philippe , Louise d' Orléans .
Ce n' est qu' en 1839 que l' indépendance belge sera définitivement garantie grâce à la ratification par les Pays-Bas du traité des XXIV articles établissant l' indépendance du nouveau royaume belge , tant face aux Pays-Bas que face à la France .
Décédé le 10 décembre 1865 à Bruxelles dans le palais de Laeken , il est inhumé dans la crypte royale à l' église Notre-Dame de Laeken .
