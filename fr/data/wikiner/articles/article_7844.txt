Ganymède est le plus grand satellite naturel de Jupiter .
Il s' agit du plus grand satellite du système solaire , plus grand que Mercure .
Il fut observé pour la première fois en 1610 par Galilée , en même temps que les autres lunes galiléennes .
Zeus en fit son amant et l' échanson des dieux de l' Olympe .
Dans la plupart des ouvrages de la littérature astronomique ancienne , il est simplement mentionné par sa désignation en chiffres romains ( un système introduit par Galilée ) comme Jupiter III , ou comme " le troisième satellite de Jupiter " .
Avec environ 5260 km de diamètre , Ganymède est le plus grand satellite naturel du système solaire , légèrement plus grand que Titan ( 5150 km ) , le satellite de Saturne , ou que la planète Mercure ( 4878 km ) .
Dans le système jovien , le deuxième plus grand satellite est Callisto ( 4821 km ) .
Ganymède , s' il reste le plus massif de tous les satellites naturels avec 1.4819×10 23 kg , ne pèse en revanche que la moitié de Mercure ( 3.302×10 23 kg ) , du fait de sa plus faible masse volumique ( 1.942×10 3 kg⋅m -3 contre 5.427×10 3 kg⋅m -3 ) , indicatrice d' une composition interne comportant une forte proportion de glace plutôt que de roche .
De fait , bien qu' il soit presque une fois et demi plus grand , la gravité à la surface de Ganymède est plus faible que sur la Lune ( 0.146 g contre 0.1654 g ) .
La surface de Ganymède est un mélange de deux types de terrains : des régions sombres très anciennes , forcément couvertes de cratères , et des régions plus claires et plus jeunes ( mais néanmoins anciennes ) marquées par de nombreux sillons et dorsales .
La densité de ces cratères donne un âge de 4 milliards d' années pour les régions sombres , similaire à celui des hauts plateaux de la Lune , et plus jeune pour les régions claires ( mais sans pouvoir déterminer de combien ) .
À la différence de ceux de la Lune et de Mercure , les cratères de Ganymède sont assez plats , ne présentant pas les anneaux et les dépressions centrales qui sont communs sur ces corps .
Il est possible que cela soit dû à la nature de la croûte de glace de Ganymède qui peut s' écouler et adoucir les reliefs .
Ganymède est composée de 49 à 59 % de silicates et sa masse volumique concorde avec une forte proportion de glace d' eau .
Selon les données recueillies par la sonde Galileo , Ganymède possède une structure interne différenciée en trois couches : un noyau de silicate contenant également du fer et peut-être du soufre , un manteau composé de roches et de glace et une croûte formée de glace regelée .
Son noyau métallique laisse supposer que Ganymède était plus chaude dans le passé .
Ses couches internes seraient donc similaires à celles d' Io .
En 1972 , une équipe d' astronomes détecta une fine atmosphère autour de Ganymède lors d' une occultation , alors que Jupiter ( et son satellite ) passait devant une étoile .
Des preuves d' une atmosphère de dioxygène ténue , très similaire à celle d' Europe , ont été découvertes depuis par le télescope spatial Hubble .
Le premier survol de Ganymède par la sonde Galileo permit de découvrir que Ganymède possède son propre champ magnétique , contenu dans la magnétosphère de Jupiter .
Ganymède est le seul satellite naturel dont on connaisse une magnétosphère .
Ganymède possède également un champ magnétique induit , indiquant qu' il possède une couche qui agit comme un conducteur .
Ganymède est le corps solide le plus concentré qu' on connaisse dans le système solaire , ce qui suggère qu' il est totalement différencié et possède un noyau métallique .
On suppose que le champ magnétique de Ganymède est produit par convection thermique dans le noyau .
En 1999 , un disque de débris sous forme d' un anneau a été détecté tout comme pour Europe et Callisto .
Comme pour les autres objets du système solaire , la toponymie de la surface de Ganymède obéit à une nomenclature stricte de la part de l' Union astronomique internationale :
En opposition , la magnitude apparente de Ganymède atteint 4.61 ±0.03 ; à son élongation maximale , il peut être possible de le distinguer de Jupiter à l' œil nu dans des conditions d' observation favorables ( les lunes galiléennes et la Lune sont les seuls satellites naturels qu' il est possible d' observer sans l' aide d' un instrument ) .
Ganymède peut être observé sans peine avec les plus petites jumelles .
La découverte de Ganymède est généralement créditée à Galilée qui documenta son existence en 1610 .
Galilée observa les lunes qui portent son nom entre décembre 1609 et janvier 1610 .
Entre le 8 janvier et le 2 mars 1610 , il découvrit un quatrième corps et observa la révolution des quatre lunes galiléennes autour de Jupiter .
Cependant , Marius n' ayant pas publié ses observations avant 1614 , il n' est en général pas crédité de cette découverte .
Ganymède fut visité par la sonde Voyager 2 en juillet 1979 , lors de son survol de Jupiter .
Les images prises par la sonde montrèrent que Ganymède était le plus gros satellite du système solaire et présentait deux types de terrain différents .
La sonde Galileo orbita autour de Jupiter entre décembre 1995 et septembre 2003 .
Pendant cette période , elle eut l' occasion d' observer Ganymède , elle permit de découvrir que le satellite possédait son propre champ magnétique et elle apporta des preuves de l' existence d' une atmosphère ténue et d' une couche d' eau en dessous de la surface .
Ganymède est un des principaux objectifs scientifiques de la mission spatiale Europa Jupiter System Mission prévue vers 2020 .
