Aux États-Unis , le taux de chômage augmente fortement au début des années 1930 : il atteint 9 % en 1930 .
En 1933 , lorsque Roosevelt devient président , 24,9 % de la population active est au chômage et deux millions d' Américains sont sans-abri .
En mars 1930 , 35000 personnes défilent dans les rues de New York .
Comme les banques américaines ont alors des intérêts dans de nombreuses banques et bourses européennes et qu' elles rapatrient d' urgence leurs avoirs aux États-Unis , la crise financière se propage progressivement dans toute l' Europe .
Parallèlement , les échanges économiques internationaux subissent de plein fouet d' abord le ralentissement qui commence aux États-Unis , ensuite l' effet négatif des réactions protectionnistes , d' abord des États-Unis , puis de tous les autres pays quand ils sont touchés à leur tour ; la France et le Royaume-Uni tentent de se replier sur leurs colonies , mettant au point la " préférence impériale " , interdite lors de la Conférence de Berlin ( 1885 ) mais largement pratiquée après 1914 .
Les relations économiques étant à l' époque bien moindre qu' aujourd'hui , ces répercussions mettront du temps à se diffuser : par exemple la France sera touchée à partir du second semestre de 1930 , soit six mois plus tard .
Les réactions gouvernementales en Europe ne seront pas plus adéquates que celles aux États-Unis .
En France la crise sera aggravée par les mesures déflationnistes ( baisse des prix et des salaires ) des gouvernements Tardieu et Laval , bien que ceux-ci tenteront , de façon limitée , quelques grands travaux ( dont l' électrification des campagnes ) .
En Allemagne , le taux de chômage atteint des sommets ( plus de 25 % de la population active en 1932 ) , alimentant la désillusion et la colère de la population , et c' est en promettant de régler le problème de la crise qu' Adolf Hitler parvint au pouvoir le 30 janvier 1933 .
Au Brésil , pour limiter la mévente et faire grimper les cours , le café est brûlé dans les locomotives .
Le monde entier est touché excepté l' Union soviétique de Staline , protégée par son système économique autarcique .
Enfin l' historien de l' économie Charles Kindleberger estime qu' il faut faire appel à plusieurs facteurs pour expliquer la crise .
En effet , pourquoi une crise de spéculation s' est-elle développée à cette date et seulement aux États-Unis ?
J.K Galbraith , qui a été le principal porteur de ces idées dans son livre la Crise économique de 1929 , aurait selon certains plutôt une attitude descriptive et ironique que véritablement explicative .
D' après Ludwig von Mises , " l' effondrement fut l' aboutissement fatal des pressions exercées pour abaisser le taux d' intérêt au moyen de l' expansion du crédit. "
Son explication est institutionnelle comme celle qui sera plus tard développée par Jacques Rueff , mais elle est quasi-exclusivement focalisée sur l' existence d' une banque centrale ( les disciples de l' école autrichienne critiquent vivement les interventions des banques centrales et une partie prônent leur suppression ) .
Les monétaristes , représentés par Milton Friedman , dénoncent la politique monétaire restrictive mise en place par la Réserve fédérale des États-Unis à partir de 1928 , qui entraîne une pénurie de crédits .
La Fed aurait au contraire dû fournir des liquidités au système bancaire : le renchérissement du crédit a forcé les spéculateurs boursiers à retirer leur épargne , ce qui a entrainé la faillite de près de 5000 banques aux États-Unis .
Dans le cas des États-Unis avant 1929 l' endettement global s' est mis à grimper de plus en plus vite jusqu' à dépasser 370 % du PIB .
Les États-Unis après une phase de très forte croissance depuis les difficultés d' avant guerre ont accumulé la richesse du monde et cette richesse n' a pas été assez diffusée dans la société malgré des théories comme le fordisme .
John Maynard Keynes a donné une certaine caution à cette explication en expliquant que les riches dépensaient proportionnellement moins que les pauvres .
Toutefois , l' économiste soviétique Nikolai Kondratiev affirmait que cette crise n' était que cyclique et circonstancielle , et que par conséquent le capitalisme reprendrait son expansion après la crise .
Le taylorisme a en effet permis une augmentation très importante de la production : Robert Boyer a ainsi calculé que la production par tête a augmenté en France de 6 % par an entre 1920 et 1960 .
En revanche , les salaires réels ont progressé de seulement 2 % par an en France sur la même période , ce qui explique l' apparition d' une situation de surproduction et le déclenchement de la crise .
Pour les tenants de cette thèse qui sera plus tard connue sous le nom de théorie de la stabilité hégémonique , si la crise de 1929 a engendré une dépression si longue et si dure c' est que la Grande-Bretagne n' était plus en mesure d' assumer son ancien rôle dirigeant et que les États-Unis ne voulaient pas encore assumer leurs responsabilités à savoir :
La critique de Keynes à partir de son livre majeur expliquera que lorsqu' un équilibre de sous emploi s' est installé , seul l' investissement public permet de retrouver le plein emploi .
Comme l' indiquent les thèses précédemment évoquées , les monétaristes pensent que la FED avait tout faux avant la crise .
Beaucoup d' auteurs comme notamment Ben Bernanke , l' actuel président de la FED , qui fera sa thèse universitaire sur ce thème , considèrent qu' elle aurait dû alimenter massivement les banques en monnaie-banque centrale au lieu de maintenir la ligne de conduite orthodoxe qui proposait moins de laxisme plutôt qu' une inondation de crédits .
Cette pensée est devenue un credo qu' Alan Greenspan puis Ben Bernanke ont mis en application avec détermination au cours de leur présidence de la FED à chaque crise américaine depuis le krach d' octobre 1987 .
Pour Jacques Sapir , cette explication ne tient pas : il explique que " la chute du commerce international a d' autres causes que le protectionnisme " .
Roosevelt prit ses fonctions en mars 1933 et lança plusieurs programmes nationaux afin d' accroître le volume de liquidités et réduire le chômage .
Il redonne espoir aux Américains et Roosevelt sera réélu en 1936 , 1940 et 1944 .
Il fournit aussi aux États-Unis des infrastructures -- routes , aménagements hydroélectriques -- encore utilisées à l' heure actuelle .
La Fed elle-même s' est ralliée à cette thèse et gère maintenant toutes les crises comparables en conséquence : elle fait largement crédit par des taux bas , crédit qu' elle résorbe ensuite par des taux croissant .
La thèse n' est cependant pas universellement acceptée , Charles Kindleberger par exemple la refuse .
Les États-Unis tentèrent également d' assainir les pratiques bancaires en leurs donnant un cadre légal plus strict , afin de protéger et rassurer les clients .
L' Allemagne suivit dès le début des années 30 une politique différente des recettes de l' orthodoxie de l' époque .
Sous la responsabilité financière de Hjalmar Schacht elle se lance dans une politique d' investissement massif , avec des objectifs civils .
Galbraith écrira dans son livre sur " la monnaie " que la politique allemande fut à cette époque une politique keynésienne complète avant l' heure .
La doctrine de Keynes est en effet qu' il faut rétablir par une politique d' investissement public l' équilibre perdu entre épargne et investissement .
C' est de cette époque que date le réseau d' autoroutes allemand ( dont l' équivalent en France ne sera construit que trente ans plus tard ) .
Le plein emploi est quasiment revenu avant même qu' Hitler oriente l' économie allemande vers la production militaire , qui d' ailleurs , pour contourner les traités , est largement réalisée ... en Union Soviétique .
En Italie , où l' exemple allemand n' est suivi que très partiellement , les aventures coloniales extérieures absorbent une partie importante de l' énergie nationale et l' économie restera faible pendant toute la période .
Au lieu de relancer la demande d' investissement public comme en Allemagne , on s' oriente vers une politique malthusienne sur l' offre de travail , avec les congés payés et surtout les " quarante heures " , qui selon Alfred Sauvy , dans son " histoire économique de la France entre les deux guerres " , bloque la reprise qui commençait à se manifester .
Le Japon connait une période d' avant guerre très différente des démocraties du fait de son expansionnisme militaire et de l' encadrement rigoureux de la population .
Les États-Unis connurent une période de forte activité pendant la Seconde Guerre mondiale avec le retour au plein emploi , la mobilisation des hommes jeunes étant compensée par le recours massif à la main d' œuvre féminine dans les usines d' armement .
