Dans les cantons de Genève , du Jura ou de Neuchâtel on utilise parfois quatre-vingts comme en France , mais sans survivance du système vigésimal au delà de la dizaine concernée .
Quoi qu' il en soit , le français de Suisse et de Belgique , principalement , peut utiliser des termes issus du système décimal latin .
