Ce fut ensuite le siège d' un ( évêché de Thérouanne ) diocèse important , érigé par saint Achaire .
Le plus célèbre de ses évêques est saint Omer .
Par sa position stratégique , Thérouanne a représenté un enjeu militaire important , en particulier pendant les guerres d' Italie .
Après un dernier siège , débuté le 11 avril 1553 , Thérouanne a été prise le 20 juin 1553 , puis rasée dans le courant de l' été 1553 sur l' ordre de Charles Quint , qui y aurait même fait répandre symboliquement du sel .
Par la suite , Thérouanne devient un site abandonné .
