L' université de technologie de Belfort-Montbéliard ( UTBM ) est un établissement public d' enseignement supérieur et de recherche situé sur les villes de Belfort , Sevenans et Montbéliard ( Région Franche-Comté , en France ) .
L' UTBM a été fondée en 1999 .
L' université de technologie de Belfort-Montbéliard est un EPSCP .
L' UTBM est membre de la conférence des grandes écoles .
L' admission à l' UTBM se fait à deux niveaux :
Il en existe 5 à l' UTBM :
Durant son parcours , l' étudiant UTBM a la possibilité de réaliser un ensemble de stages en France ou à l' étranger :
7 laboratoires de recherche s' organisent autour de l' UTBM :
Il existe 4 associations étudiantes à l' UTBM :
