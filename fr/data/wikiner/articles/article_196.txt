Pour cette raison en 1999 , l' UNESCO décide de commémorer tous les 21 février comme Journée internationale de la langue maternelle .
Le représentant le plus connu du bengalî est le lauréat du prix Nobel de littérature Rabindranath Tagore .
Ce jour est depuis resté Journée du Mouvement pour la Langue au Bangladesh , puis plus tard en 1999 , l' UNESCO décide de commémorer tous les 21 février comme Journée internationale de la langue maternelle , .
Le bengali est la langue officielle et nationale du Bangladesh et l' une des 23 langues officielles reconnues par l' Inde .
Il est devenu langue officielle au Sierra Leone en l' honneur de la contribution du Bangladesh à la mission des Nations unies en Sierra Leone .
C' est ce bengalî parlé standard : choltibhasha ( চলতিভাষা ) ou cholitobhasha , qui est aujourd'hui accepté comme une forme standard aussi bien au Bengale-Occidental qu' au Bangladesh .
Le Sud-Est du Bengale-Occidental , Kolkata inclus , parle le bengalî parlé standard , au contraire des autres zones , où est parlé le " bengalî " .
La majorité au Bangladesh parle des variantes notablement différentes du bengalî parlé standard .
Dans certains cas , des locuteurs du bengalî standard au Bengale-Occidental utiliseront un mot différent que ceux parlant bengalî standard au Bangladesh , même si chacun de ces mots est natif du bengalî .
langue arabe sukūn ) , peut être ajouté en dessous du graphème consonantique de base ( par exemple ম্ [ m ] ) .
Récemment , afin de faciliter l' apprentissage des jeunes écoliers , des efforts ont été fait par les institutions scolaires dans les deux régions principales bengalophones ( Bengale-Occidental et Bangladesh ) pour amoindrir la nature opaque de nombreuses consonnes combinées , et il en résulte que les textes en bengalî moderne commencent à contenir de plus en plus de forme graphiques " transparentes " pour les consonnes groupées , telles que la consonne constituante est apparente à la lecture dans la forme graphique .
Des experts en Inde , comme au Bangladesh travaillent actuellement afin de résoudre ce problème .
