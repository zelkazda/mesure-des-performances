Les premières divisions administratives du Brésil étaient les capitaineries , qui était des portions de terres louées à des nobles et des commerçants portugais dans le but de les coloniser .
Après l' Union ibérique entre 1580 et 1640 , le territoire colonial du Portugal en Amérique du Sud augmenta fortement , et fut divisé entre des capitaineries , des capitaineries royales et des provinces .
Cependant tout la région était uni sous un seul vice-roi , qui siégeait à Salvador ( et , après 1763 à Rio de Janeiro ) .
Des modifications mineures furent faites en fonction de la politique intérieure , ainsi que des ajouts faisant suite à des traités diplomatiques avant la fin du 19ème siècle ( comme pour l' annexion de l' Amapá ou de Roraima ) .
En 1943 , lors de l' entrée du Brésil dans la Seconde Guerre mondiale , le régime de Getúlio Vargas créa sept nouveaux territoires aux frontières du pays afin de les administrer directement .
En 1977 , le Mato Grosso a été divisée en deux états .
Le Mato Grosso do Sul étant aussi constituée des territoires du Ponta Pora et de la partie nord de l' Iguaçu .
La même année , l ' archipel de Fernando de Noronha est devenu une partie du Pernambuco .
