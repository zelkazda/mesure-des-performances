Ruskin dit lui-même qu' ils furent " très violemment critiqués " , obligeant l' éditeur à interrompre la publication au bout de quatre mois .
Mais Ruskin contre-attaque et publie les quatre essais en livre en mai 1862 .
Unto This Last eut une importance capitale sur la pensée de Mahatma Gandhi .
Dans Unto This Last , Gandhi trouva une grande partie de ses idées sociales et économiques .
Ruskin était concerné par les mêmes problèmes et apportait les solutions qui ont plu à Gandhi comme si elles étaient les siennes .
Le but de Unto This Last est double : définir la richesse , et démontrer que certaines conditions morales sont essentielles pour l' obtenir .
Pour Ruskin , et pour Gandhi , c' est précisément cela que la science ne doit pas faire .
Beaucoup de politiciens et d' industrialistes les comprennent certainement de cette façon , et agissent selon ce qu' ils prennent pour leurs conseils , ce qui suffit à Ruskin et à Gandhi pour démontrer l' irresponsabilité de la méthode .
