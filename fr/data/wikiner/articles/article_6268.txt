La grande barrière de corail est le plus grand récif corallien du monde , .
Il est situé au large du Queensland , en Australie .
Il s' étend sur 2600 kilomètres , de Bundaberg à la pointe du Cap York .
À titre de comparaison , l' Allemagne a une superficie de 357000 km² .
La Grande barrière de corail compte plus de 2000 îles et près de 3000 récifs de toutes sortes .
Elle est inscrite sur la liste du patrimoine mondial de l' UNESCO .
Pour les Australiens , les milliers d' îles , d' îlots et d' atolls composant la Grande Barrière de corail constituent la 8 e merveille du monde .
De nombreuses villes le long de la côte du Queensland offrent des départs quotidiens en bateau vers le récif .
Le premier explorateur européen à avoir vu la grande barrière fut le capitaine britannique James Cook , lors de son voyage de 1768 .
La Grande barrière de corail est relativement jeune .
La menace la plus significative pour l' avenir de la Grande Barrière de corail et d' autres écosystèmes tropicaux est le réchauffement climatique .
Depuis le 1 er juillet 2004 , la pêche est interdite dans un tiers de la Grande Barrière de corail .
Celle-ci fut découvert par les européens quand l' Endeavour , le navire commandé par l' explorateur James Cook , s' y échoua le 11 juin 1770 , subissant de gros dommages .
Le navire fut finalement sauvé après avoir été allégé au maximum pour le remettre à flot pendant une marée montante avant d' être amené à Cooktown pour y être réparé .
Un des plus fameux naufrages fut celui du Pandora , qui coula le 29 août 1791 , occasionnant la mort de 35 marins .
