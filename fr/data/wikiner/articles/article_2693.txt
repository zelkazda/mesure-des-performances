Il se situe au sud-est de l' Afrique à environ neuf cents kilomètres à l' est de Madagascar , à deux cent cinquante kilomètres de la Réunion et au sud des Seychelles .
Cette république d' une superficie de 2040 km 2 est composée de l' île Maurice qui représente 91 % de la superficie du pays avec 1865 km 2 de superficie ainsi que plusieurs petites îles et récifs dont Rodrigues et les archipels de Cargados Carajos et d' Agaléga .
Comme les Seychelles , Maurice est une ancienne colonie britannique , à large majorité francophone .
Des Portugais s' y installèrent pour la première fois en 1505 .
L' île est occupée puis colonisée par les Néerlandais à partir de 1598 .
L' esclavage a été aboli à Maurice le 1er février 1835 .
Le suffrage universel a été introduit à Maurice en 1958 .
Le pays est resté depuis membre du Commonwealth des Nations , bien qu' il soit devenu une république le 12 mars 1992 .
Grâce à une démocratie relativement stable ponctuée d' élections libres et régulières et à un bilan positif en matière de droits de l' homme , le pays a su attirer des investissements étrangers importants et dispose d' un des revenus par tête les plus importants d' Afrique .
La République de Maurice a un système juridique de droit mixte .
Maurice est composé de plusieurs îles dont l' île Maurice , qui représente l' essentiel du pays , qui comprend également l' île de Rodrigues et des archipels de Cargados Carajos et d' Agaléga .
Située dans le nord-ouest , Port-Louis est la capitale de l' île et la plus grande ville .
Les autres villes importantes sont Curepipe ( au centre ) , Vacoas-Phœnix , Quatre Bornes , Rose-Hill et Beau-Bassin .
Mahébourg ( au sud-est ) n' est pas une grande ville , néanmoins c' est la plus proche de l' aéroport international de Plaisance-Sir Seewoosagur Ramgoolam .
Rodrigues , les îles Agaléga et les écueils des Cargados Carajos ( également appelés Saint-Brandon ) constituent les dépendances de Maurice .
L' archipel des Chagos et l' île Tromelin sont revendiqués par Maurice .
Depuis son indépendance acquise en 1968 , Maurice a connu une évolution économique fulgurante .
Maurice a attiré plus de 9000 sociétés offshore , dont beaucoup se consacrent au commerce en Inde et en Afrique du Sud .
Maurice est aussi un pavillon de complaisance .
En effet , les réformes de l' Union européenne concernant le marché sucrier vont lourdement affecter l' exportation du pays .
Grâce à ce nouveau type d' aménagement combiné à l' ouverture du pays aux étrangers , Maurice amorce une nouvelle ère de développement , qui porte déjà ses fruits en termes d' investissement étranger direct et d' emplois créés et qui est appelé à changer la face du pays , à plus long terme .
Dans les années 1970 , Maurice a misé sur le textile pour ne plus dépendre uniquement de la canne à sucre .
Elle doit permettre le développement du secteur des technologies de l' information à Maurice .
Depuis 2006 , avec la signature d' un accord pour le déploiement du câble sous-marin EASSY , Maurice est en passe de combler cette lacune mais repousse au minimum à 2008-2010 l' échéance sécuritaire du projet .
La constitution de Maurice ne mentionne aucune langue officielle pour le pays .
D' après les estimations de 2003 fournies par le gouvernement à l' OIF , le nombre de francophones est estimé à 15 % de la population et le nombre de francophones partiels à 57,7 % soit au total 72,7 % de la population ayant une certaine maîtrise du français .
Le dodo est devenu l' emblème de Maurice .
Le pays est membre de l' Organisation internationale de la francophonie et a statut d' observateur au sein de la Communauté des pays de langue portugaise .
Maurice a pour codes :
