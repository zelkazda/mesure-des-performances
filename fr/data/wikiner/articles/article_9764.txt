Il est principalement connu pour deux de ses romans-feuilletons à caractère social : Les Mystères de Paris ( 1842-1843 ) et le Juif errant ( 1844 -- 1845 ) .
Il étudia au lycée Condorcet .
Eugène Sue est l' auteur , selon ce qu' en rapporte la bibliographie établie par Francis Lacassin , de sept romans exotiques et maritimes , onze romans de mœurs , dix romans historiques , quinze autres romans sociaux , deux recueils de nouvelles , huit ouvrages politiques , dix-neuf œuvres théâtrales ( comédie , vaudeville , drame ) et six ouvrages divers .
Cela influença sa vie personnelle -- il est élu député de la Seine -- et son orientation littéraire .
Malgré sa disparition , le tribunal , suivant le réquisitoire du procureur Ernest Pinard , condamna l' imprimeur et l' éditeur , et ordonna la saisie et la destruction de l' ouvrage .
Il fut député républicain et socialiste de la Seine .
De fait , le roi Victor-Emmanuel II et son chef du gouvernement , Massimo d' Azeglio , sont favorables aux idées libérales .
Il finit par s' installer à Annecy-le-Vieux où il y vécut de 1851 jusqu' à sa mort en 1857 .
