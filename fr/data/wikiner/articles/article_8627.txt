Écrit principalement au château de Saché en Indre-et-Loire dont Balzac s' inspira fortement , ce roman a été publié pour les deux premières parties de novembre à décembre 1835 dans la Revue de Paris .
Puis en raison d' un différend avec l' éditeur François Buloz , la publication fut interrompue .
L' écriture du Lys dans la vallée s' est échelonnée sur plusieurs années .
Laure de Berny eut le manuscrit en main quelques mois avant sa mort .
