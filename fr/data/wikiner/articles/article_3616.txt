Star Trek est un univers de science-fiction créé par Gene Roddenberry dans les années 1960 qui regroupe six séries télévisées , onze longs métrages , des centaines de romans et des dizaines de jeux vidéo , ainsi qu' une fanfiction importante ( des nouvelles ou des romans écrits par les fans ) .
Elle est aussi une franchise de télévision et de cinéma appartenant à Paramount Pictures , propriété de la compagnie CBS .
Les conflits et les dimensions politiques de Star Trek forment des allégories pour des réalités culturelles contemporaines ; la série télévisée originale de Star Trek aborda les questions des années 1960 , tout comme plus tard des séries dérivées ont reflété des questions de leurs époques respectives .
L' univers Star Trek dépeint un futur optimiste , utopique , dans lequel l' humanité a éradiqué la maladie , le racisme , la pauvreté , l' intolérance et la guerre sur Terre .
Ces amateurs inconditionnels ont fait le succès des rediffusions et créé un marché pour les séries suivantes et autres films fondés sur le travail de Gene Roddenberry .
Les histoires de Star Trek font maintenant partie intégrante de la culture occidentale , surtout anglophone cependant , et constituent une sorte de nouvelle mythologie .
C' est d' ailleurs à la suite d' une opération de lobbying des fans de la série que la NASA a accepté de nommer Enterprise le prototype de la navette spatiale .
Souvent , le but de la puissance en question est d' asservir ( ou de détruire ) le vaisseau et son équipage , mais tous deux sont sauvés par le capitaine James T. Kirk , interprété par l' acteur William Shatner .
Certains épisodes font appel à des scénaristes réputés ( par exemple Robert Bloch sur trois épisodes , dont celui concernant Jack l' éventreur ) .
Il n' y a pas d' histoire se prolongeant tout le long de la série originale , chaque épisode formant une structure close , séparée des autres , le seul élément de continuité étant la distribution et certains ennemis récurrents comme les Klingons .
La série originale , par exemple , possède un membre d' équipage féminin et afro-américain Nyota Uhura , rôle interprété par l' actrice Nichelle Nichols , une des premières femmes afro-américaines à tenir un rôle principal à la télévision américaine .
Il fait également intervenir un personnage originaire de Russie -- Pavel Chekov , interprété par Walter Koenig -- et ce en pleine Guerre froide entre les États-Unis et l' Union Soviétique .
Pour illustrer cette vision idéaliste , le premier pilote de la série , The Cage , a été refusé parce que le commandant en second de l ' Enterprise était joué par une femme , ce que la Paramount a jugé " irréaliste " .
Dans l' épisode 3-10 mettant en œuvre un contrôle mental comme prétexte pour briser ce tabou , le capitaine Kirk et Uhura sont forcés à partager ce premier baiser à la télévision américaine .
L' univers de Star Trek a plus que survécu à une longue traversée audiovisuelle du désert grâce à l' écriture , il s' est également enrichi par le partage et le travail collectif .
En 1987 , une nouvelle série est lancée , Star Trek : La nouvelle génération , comportant un nouvel équipage .
Avec l' arrivée du producteur Rick Berman , elle a lentement pris une nature plus basée sur les masques en intégrant de plus en plus des scènes d' animation et des discours cryptés pour certaines audiences .
L' univers de Star Trek est représenté à travers
Voici la chronologie " en-univers Star Trek " des séries télévisées .
On peut voir que les événements de la série Enterprise se déroulent 100 ans avant la série Star Trek et que les autres séries s' y déroulent 100 ans après .
La dernière série télévisée Enterprise ( ou Star Trek : Enterprise , à partir de la troisième saison ) s' est terminée en 2004 .
Un onzième long-métrage , qui peut être à la fois une suite , une préquelle et un relancement de la franchise , intitulé simplement Star Trek , est sorti le 6 mai 2009 et met en vedette Chris Pine dans le rôle du jeune James T. Kirk et Zachary Quinto en Spock .
L' univers Star Trek est aussi une série de romans , s' inscrivant dans tous les univers des différentes séries .
