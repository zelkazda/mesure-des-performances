Les démêlés de Galois avec les autorités , tant scientifiques que politiques , les zones d' ombre entourant sa mort prématurée , contrastant avec l' importance reconnue maintenant à ses travaux , ont contribué à en faire l' incarnation même du génie malheureux .
Évariste Galois naquit le 25 octobre 1811 à Bourg-la-Reine .
Il intégra en 1823 le collège Louis-le-Grand en classe de quatrième et obtint tout d' abord des prix en latin et en grec .
Galois décida de se présenter dès l' été 1828 au concours d' entrée à l' École polytechnique , sans être passé comme il était d' usage par une classe de mathématiques spéciales , et y échoua une première fois .
Galois entra alors à l' École préparatoire .
Selon la légende , le rapporteur des premiers mémoires , à savoir Cauchy , les aurait égarés .
Le manuscrit de Galois avait été envoyé à Fourier , qui mourut en mai ; le mémoire fut annoncé perdu .
La Révolution de juillet 1830 marqua le début de son engagement politique du côté républicain .
Arrêté , il passa un mois en prison avant d' être jugé et acquitté .
En prison , Évariste Galois réussit néanmoins à travailler ses mémoires sur les équations et à entamer d' autres recherches sur les fonctions elliptiques .
En mars 1832 , Galois fut transféré dans une clinique privée à cause d' une épidémie de choléra .
Blessé à l' abdomen , Galois fut transporté à l' hôpital Cochin et mourut le 31 mai 1832 , à l' âge de 20 ans et 7 mois , probablement d' une péritonite , après avoir refusé les offices d' un prêtre .
Galois fut enterré le 2 juin 1832 au cimetière du Montparnasse à Paris en présence de deux à trois mille républicains .
Évariste Galois publia ce premier article à l' âge de 17 ans .
Il était connu depuis les travaux de Joseph-Louis Lagrange que le développement en fractions continues de toute solution d' une équation polynomiale du second degré est périodique ; par exemple le développement de √3 est , après la partie entière 1 , alternativement composée de 1 et de 2 .
Galois prouva que la période est symétrique si et seulement si le polynôme étudié s' écrit sous la forme .
Joseph-Louis Lagrange avait reformulé la question comme la résolution d' une équation polynomiale par radicaux .
En 1813 , Augustin Louis Cauchy s' était déjà intéressé à cette question et étudia les permutations alors appelées substitutions , travaux précurseurs de la théorie des groupes .
Enfin , Abel avait établi l' impossibilité de résoudre par radicaux l' équation générale en degré supérieur à 5 .
Galois présente sans démonstrations trois conditions sur la résolution par radicaux d' équations polynomiales primitives .
La définition d' un polynôme primitif avait été donné par Cauchy .
Ce mémoire fut néanmoins publié en 1846 par Liouville .
Dans ce mémoire , Évariste Galois chercha à étudier la résolubilité des équations polynomiales .
Évariste Galois en déduit que la recherche d' une résolution par radicaux passe par la réduction du groupe associé par adjonctions successives de racines .
Dans son court rapport , Poisson compare d' abord les résultats de Galois à ceux d' Abel sur le même sujet , critique la nature des conditions de résolubilité des équations proposées ainsi que la rédaction du texte : " ses raisonnements ne sont ni assez clairs , ni assez développés pour que nous ayons pu juger de leur exactitude " .
De son vivant , Galois reçut des critiques sur le manque de clarté de ses mémoires .
Dans son court rapport , Poisson compara d' abord les résultats de Galois à ceux d' Abel sur le même sujet , critiqua la nature des conditions de résolubilité des équations proposées ainsi que la rédaction du texte : " ses raisonnements ne sont ni assez clairs , ni assez développés pour que nous ayons pu juger de leur exactitude " .
Abel et Galois ont pu souvent être comparés d' une part par la " brièveté de leur vie " , d' autre part par " le genre de leur talent et l' orientation de leurs recherches " .
Les travaux d' Abel furent publiés dans le premier numéro du Journal de Crelle .
Néanmoins , Galois dit ne pas avoir eu connaissance des travaux d' Abel lorsqu' il soumit ses premiers articles en 1829 .
Les travaux de Galois et d' Abel sont indépendants : Galois " n' avait eu qu' en partie connaissance " des travaux d' Abel sur les sujets qui l' intéressaient .
Dès sa mort dramatique , Évariste Galois a été présenté comme un génie incompris , un valeureux républicain et un mathématicien ignoré de ses contemporains .
Les historiens des mathématiques ont tenté ultérieurement de donner un nouvel éclairage à la vie d' Évariste Galois .
Le ressentiment de Galois a pu être présenté par certains auteurs comme une réelle opposition des mathématiciens de son époque à ses travaux novateurs .
Selon Eric Temple Bell , Évariste Galois aurait rédigé ses travaux sur la résolution d' équations polynomiales par radicaux la veille de sa mort et n' aurait pas eu le temps de donner les détails de la démonstration .
