La province de Grenade est l' une des huit provinces de la communauté autonome d' Andalousie , dans le sud de l' Espagne .
Sa capitale est la ville de Grenade .
La province de Grenade est située au sud-est de la communauté autonome et couvre une superficie de 12531 km 2 .
La province de Grenade est bordée au nord par la province de Jaén , au nord-est par la province d' Albacete ( communauté autonome de Castille-La Manche ) et la région de Murcie , à l' est par la province d' Almería , au sud par la mer Méditerranée , à l' ouest par la province de Malaga et au nord-ouest par la province de Cordoue .
La province de Grenade est subdivisée en dix comarques :
La province de Grenade compte 168 communes ( municipios en espagnol ) .
