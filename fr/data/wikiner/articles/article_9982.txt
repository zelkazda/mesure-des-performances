Il débute au cinéma en 1967 avec Alexandre le bienheureux d' Yves Robert .
Il retrouve Yves Robert pour tourner Le Grand Blond avec une chaussure noire , ainsi que sa suite , Le Retour du grand blond , tous deux scénarisés par Francis Veber .
Ce dernier lui confie ensuite le premier rôle de son premier long-métrage en tant que réalisateur , Le Jouet .
Veber et Richard entament alors , au début des années 1980 , une collaboration fructueuse qui verra naître La Chèvre , Les Compères et Les Fugitifs , trois comédies à succès où Richard partage l' affiche avec Gérard Depardieu .
Il revient à la réalisation avec On peut toujours rêver en 1991 et Droit dans le mur en 1997 .
En 2005 , Pierre Richard est président du jury du Festival des Très Courts .
Il revient au cinéma en 2009 avec deux films : Cinéman , où il joue son propre rôle , et Victor , dans lequel il a le rôle-titre .
Pierre Richard a joué dans tous les films qu' il a réalisés .
