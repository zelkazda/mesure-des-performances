Les formats de données tels que Zip , RAR , gzip , ADPCM , MP3 et JPEG utilisent des algorithmes de compression de données .
Il y a autant d' information après la compression qu' avant , elle est seulement réécrite d' une manière plus concise ( c' est par exemple le cas de la compression gzip pour n' importe quel type de données ou du format PNG pour des images synthétiques destinées au Web ) .
Les standards ouverts les plus courants sont décrits dans plusieurs RFC :
La norme JPEG 2000 , par exemple , arrive généralement à coder des images photographiques sur 1 bit par pixel sans perte visible de qualité sur un écran , soit une compression d' un facteur 24 à 1 .
Les formats MPEG sont des formats de compression avec pertes pour les séquences vidéos .
Ils incluent à ce titre des codeurs audio , comme les célèbres MP3 ou AAC , qui peuvent parfaitement être utilisés indépendamment , et bien sûr des codeurs vidéos -- généralement simplement référencés par la norme dont ils dépendent , ainsi que des solutions pour la synchronisation des flux audio et vidéo , et pour leur transport sur différents types de réseaux .
C' est une compression d' images utilisée pour le fax , standardisée par des recommandations de l' Union internationale des télécommunications ( anciennement appelée CCITT ) .
L' originalité de David A. Huffman est qu' il fournit un procédé d' agrégation objectif permettant de constituer son code dès lors qu' on possède les statistiques d' utilisation de chaque caractère .
La compression LZ77 remplace des motifs récurrents par des références à leur première apparition .
LZ77 est notamment la base d' algorithmes répandus comme Deflate ( ZIP , gzip ) ou LZMA ( 7-Zip )
La compression Lempel-Ziv-Welch est dite de type dictionnaire .
Actuellement , les meilleurs taux de compression sont obtenus par des algorithmes liant pondération de contextes et codage arithmétique , comme PAQ .
