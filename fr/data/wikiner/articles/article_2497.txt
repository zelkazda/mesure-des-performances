Metz ( prononcé m ɛ s ] ) ) est une ville située dans le nord-est de la France .
Elle est le chef-lieu du département de la Moselle et la préfecture de la région Lorraine dont elle est la commune la plus peuplée .
Ancienne capitale du royaume d' Austrasie et berceau de la dynastie carolingienne , Metz témoigne par la richesse de son patrimoine d' une histoire commencée il y a trois mille ans .
Metz occupe une position unique au croisement des grands axes européens de circulation :
Ainsi les villes les plus importantes qui entourent Metz sont Luxembourg à 55 kilomètres au nord , Nancy à 53 kilomètres au sud et Sarrebruck à 60 kilomètres à l' est .
La ville est située à peu près à mi-chemin entre Strasbourg à 130 kilomètres et Reims à 155 kilomètres .
Paris se trouve à 320 km et Francfort-sur-le-Main à 230 km .
Seules les communes de Coincy et Peltre ne sont pas membres de la communauté d' agglomération .
Metz est dominée par le mont Saint-Quentin qui culmine à 358 mètres à l' ouest de l' agglomération hors du territoire de la commune .
La vallée de la Moselle en elle-même draine des sédiments constitutifs des îles .
Les rives de la Moselle font partie intégrante de la ville historique dotée de nombreux quais .
Metz dispose d' un climat océanique dégradé avec une nuance continentale assez marquée .
À noter qu' un élément -metz est relativement répandu dans la toponymie française , tout en ayant une étymologie différente , exemple : Jametz , Limetz , Gometz , etc .
La longue période de la paix et l' intégration à l' Empire romain en fait une ville étape prospère .
Elle devient le foyer le plus important de la civilisation gallo-romaine en Lorraine .
Metz est un important carrefour routier où convergent les voies de Lyon , Reims , Trèves , Mayence et Strasbourg -- préfigurant en quelque sorte les autoroutes actuelles ( A4 -- A31 ) .
Le tracé des routes obéit à des impératifs stratégiques : assurer la défense à l' arrière du Rhin [ réf. souhaitée ] .
L' aqueduc de Gorze à Metz long de 22 km traversant la Moselle et dont on voit les arches à Jouy-aux-Arches alimentait la ville en eau .
Par ailleurs , une vaste nécropole s' étend au sud de la ville de part et d' autre de la voie vers Lyon .
La corporation des nautes de la Moselle est spécialisée dans le transport fluvial de produits lourds , et notamment les matériaux de construction .
Un cimetière se développe au Sablon .
La ville est envahie et détruite une première fois en 253 par les Alamans .
Le Sablon sera sacrifié et laissé en dehors , la ville ainsi forclose représente un rectangle d' environ 1200 sur 600 mètres ; sa superficie est rapportée à 70 hectares .
Une production nouvelle , la vigne , fait son apparition à partir de 283 , date de l' autorisation accordée par l' empereur Probus .
La diffusion du christianisme arrive à Metz à la fin du III e siècle , vers 280 , avec le premier évêque , Clément .
Les Huns tentèrent un premier siège de la ville puis s' en allèrent ravager les villes de Toul , Dieuze et Scarpone .
Seul subsista l' oratoire dédié à saint Étienne et situé à l' endroit de la cathédrale actuelle .
L' oratoire de Saint-Étienne est dans les grâces divines et devient alors fort populaire .
Il accueille le siège de l' évêque et devient en quelque sorte la première cathédrale de Metz , à l' intérieur même de celle-ci .
En 511 à la mort de Clovis , unificateur du peuple franc , ses enfants se partagent son royaume .
Thierry I er reçoit la région nord-est baptisée Austrasie .
À la mort de Clotaire en 561 , Sigebert I er reçoit en héritage de la partie orientale du royaume avec Reims pour capitale .
Il choisit alors la ville comme résidence principale et en fait la capitale d' Austrasie .
Lors du règne de Sigebert , la charge de maire du palais est pour la première fois mentionnée .
Charlemagne eut de constantes préoccupations pour Metz , dont il favorisa tout particulièrement l' église et donna une impulsion nouvelle à sa célèbre école .
Sous l' évêque Drogon , ces possessions débordent les limites du diocèse et se rencontrent en Alsace , dans la région de Liège et jusqu' en Aquitaine .
L' Austrasie revient à Lothaire Ier .
En 959 , après le partage de la Lotharingie par l' évêque Brunon de Cologne , la Haute-Lotharingie devient le duché de Lorraine .
Comme à Nuremberg , les institutions de cette république sont l' apanage d' un cercle de familles riches , ici regroupées à travers six " paraiges " .
Ses foires sont très fréquentées et sa monnaie , la première de la région jusqu' en 1300 , est acceptée dans toute l' Europe .
Metz se transforme .
La ville devient un important foyer protestant qui disparaît précocement suite à la révocation de l' édit de Nantes qui provoque une émigration messine vers Berlin .
L' émigration messine des huguenots vers Berlin s' est traduite par un doublement de la population de Berlin , où ils arrivaient par milliers .
Au cours des dernières campagnes de Napoléon Ier , en 1814 et en 1815 , la ville fut assiégée à deux reprises par les forces coalisées .
Elle ne se rendit qu' à partir du moment où Napoléon signa la capitulation , lorsque la nouvelle atteignit Metz .
En 1866 après la bataille de Sadowa , l' état-major français se rendit compte qu' une prochaine guerre avec la Prusse pourrait déclencher une invasion rapide de l' est de la France .
Il fut décidé que Metz soit protégé par quatre forts situés aux endroits stratégiques ( Plappeville , Saint-Julien-lès-Metz , Queuleu et Saint-Privat ) et qui ne furent pas terminés à temps lorsque la guerre éclata contre la Prusse en 1870 .
Pendant la Guerre franco-allemande de 1870 , l' armée impériale du général français Bazaine s' est réfugiée à Metz .
Après la bataille de Borny-Colombey le 14 août à l' est de la ville , puis celle de Saint-Privat -- Gravelotte à l' ouest le 18 août , Metz est assiégé le 20 août et capitule le 29 octobre .
Pendant l' annexion , et malgré le départ d' une importante portion de ses élites et de dix à quinze mille " optants " pour la France , la ville continue de s' agrandir et de se transformer , dominée par la personnalité de son évêque français Paul Dupont des Loges , qui est élu " député protestataire " au Reichstag .
L' émigration de mosellans vers la France , en particulier vers Nancy et Paris , commence dès l' armistice et se poursuit pendant une vingtaine d' années .
Ces derniers deviennent majoritaires à Metz , dès les années 1890 .
Comme dans le reste de la Moselle , l' enseignement du français est supprimé dans les écoles primaires , où les instituteurs allemands donnent l' enseignement en allemand .
Sous Guillaume I er ( 1871 -- 1888 ) , on continue d' imprimer et d' importer des livres en français .
Metz se transforme sous l' action des autorités allemandes qui décident de faire de son urbanisme une vitrine de l' empire wilhelmien .
Pour ce point stratégique majeur de la défense de l' empire -- il s' agit d' un carrefour routier et ferroviaire de premier ordre -- , l' état-major allemand poursuit les travaux de fortification entamés par Napoléon III sous le Second Empire .
Dès 1871 , le système défensif de la ville avait profondément été corrigé , avec la construction d' une ceinture de forts éloignés de type " von Biehler " autour de l' agglomération , conformément au développement des techniques d' assaut .
Lorsque le comte Gottlieb von Haeseler prend le commandement du 16 e corps d' armée en 1890 , Metz est devenue une place forte inexpugnable .
Ces officiers de carrière , avides de fêtes et de spectacles de qualité , s' installent avec leur famille à Metz .
Au cours d' une de ses visites , il déclare ainsi : " Metz et son corps d' armée constituent une pierre angulaire dans la puissance militaire de l' Allemagne , destinée à protéger la paix de l' Allemagne , voire de toute l' Europe .
En 1914 , Metz est devenue la première place forte au monde , .
La ville sera largement épargnée par les combats de la Première Guerre mondiale .
Les intellectuels mosellans et messins réagiront diversement au rattachement de la Moselle à la France .
Ce combat identitaire , souvent mené par des intellectuels idéalistes , qui s' inscrit parmi des courants de sensibilité à l' œuvre dans l' Europe entière , traduit aussi une crise d' identité propre à l' ensemble des Alsaciens-Lorrains .
Une part importante des notables messins ont été formés à Berlin ou dans de grandes universités allemandes , comme ce fut le cas de Robert Schuman .
À l' encontre des accords signés entre les deux états , le régime nazi applique de facto une politique d' annexion à Metz ainsi que dans les territoires anciennement annexés dans le cadre du reichsland Elsaß-Lothringen .
Le régime de Vichy se limite alors à des protestations si discrètes qu' elles alimentent dans la population l' idée d' un pacte secret .
C' est dans ces conditions que se déroule la bataille de Metz du 27 août au 13 décembre 1944 .
Elle a ainsi pu poursuivre sa croissance , malgré les difficultés économiques qui ont durement frappé la Lorraine , avec la crise du charbon , celle de la sidérurgie ou celle du textile .
Du 6 au 8 avril 1979 se déroula le congrès de Metz du Parti socialiste où s' affrontèrent François Mitterrand et Michel Rocard .
Désormais à 1 heure 20 minutes de Paris grâce au TGV , Metz développe son université qui compte aujourd'hui plus de 20000 étudiants .
La cité , l' une des villes les plus fleuries d' Europe [ réf. nécessaire ] , développe les technologies de l' information et de la communication à travers le Technopôle de Metz .
Le 7 novembre 2006 est posée la première pierre du centre Pompidou-Metz , inauguré le 12 mai 2010 .
Enfin , depuis 1976 , Metz est aussi la préfecture de la région Lorraine .
La commune est le chef-lieu de l' arrondissement de Metz-Ville et de arrondissement de Metz-Campagne dont les cantons ceinturent le premier -- singularité de ville chef-lieu non-inclus dans l' arrondissement que Metz partage avec Strasbourg .
Metz est divisée en quatorze quartiers répartis dans quatre cantons qui forment l' arrondissement de Metz-Ville :
La volonté d' échanger et de bâtir des événements culturels , économiques et historiques avec la ville de Luxembourg aboutit à un premier jumelage qui se concrétise en 1952 -- c' est l' un des plus anciens en Europe [ réf. nécessaire ] .
Ce pacte d' amitié , donne naissance à l' idée d' une région " supra frontalière " affirmée par le maire Raymond Mondon et concrétisée par son successeur Jean-Marie Rausch .
Son dynamisme permet plusieurs fois par an des échanges avec Metz .
Le rapprochement avec Tyler se dessine dans un cadre universitaire .
Karmiel semble très attachée à la langue française et propose à sa jeune population d' améliorer son expression en langue française et d' en découvrir les subtilités .
L' aide humanitaire et financière apportée par Metz durant la guerre du Golfe a aidé à souder les deux villes .
L' aire urbaine incluant notamment celles d' Hagondange et Briey dans un ensemble de plus de 429 588 habitants
est équivalent à celle de Nancy et plaçant les deux villes au dix-septième rang français .
Metz n' a renoué avec l' université qu' en 1970 , à force de persévérance politique .
Elle compte au total près de 21000 étudiants sur trois sites principaux et porte le nom d' université Paul-Verlaine .
La fédérations des commerçants prévoit de créer un grand évènement fin 2010 sur la place de la République redevenue piétonne -- des jeux d' hiver qui pourraient rester jusqu' en février …
Certaines personnalités nées à Metz , comme Paul Verlaine ou Bernard-Marie Koltès , ont suivi ailleurs le cours de leur destin .
Parmi les secteurs économiques représentés à Metz , citons la métallurgie , la pétrochimie , l' automobile , la logistique , le commerce [ précision nécessaire ] …
La plus importante zone d' activité commerciale de la région est situé en périphérie de la ville , à Augny .
Ce réseau de villes ayant pour but le développement transfrontalier de la région regroupe ainsi Luxembourg , Esch-sur-Alzette , Longwy et Arlon .
L' agglomération messine est desservie par une importante infrastructure autoroutière constituée par l' A4 ( qui la relie à Paris , Reims et Strasbourg ) et l' A31 .
L' A31 se trouve en situation d' autoroute urbaine .
Une portion de 20 km manque à cet ensemble et pourrait de plus rejoindre les abords de Woippy .
L' ensemble des routes et voies express qui relient la métropole messine aux autres communes rayonnent autour d' un anneau de grandes avenues et de boulevards qui , aménagé selon le modèle allemand d' un Ring , distribue les accès depuis et vers l' hypercentre .
Par ailleurs un boulevard inter-communal contournant la cité par le sud-ouest , dont l' achèvement est prévu pour 2010 , créera une liaison entre le Technopôle ( situé à l' est ) et le parc de la Seille , en évitant la traversée du quartier de Plantières Queuleu .
L' aéroport régional Metz-Nancy-Lorraine , situé à 20 km du centre , à proximité directe de la gare de Lorraine TGV est relié par une voie expresse à la sortie sud-est de la ville .
L' aéroport international de Luxembourg-Findel est également accessible à 70 km environ par l' autoroute A31 .
L' aéroport de Sarrebruck-Ensheim est quant à lui accessible à la même distance par l' A320 .
Depuis le 10 juin 2007 , la gare de Metz-Ville est directement reliée à Paris en 82 minutes par la ligne à grande vitesse LGV Est européenne , renforçant les échanges entre l' agglomération lorraine , la région parisienne , l' Allemagne et le Luxembourg .
Cet axe ferroviaire fort se poursuit au nord en direction de Luxembourg sur le sillon mosellan , au sud en direction d' Épinal et au sud-est en direction de Lunéville .
Pour le tourisme , Metz est accessible par la Moselle canalisée directement au centre-ville par le biais de son port de plaisance situé sur le plan d' eau .
25 % des échanges internationaux de la Lorraine passent par voie d' eau .
L' augmentation de la hauteur libre entre Metz et la frontière luxembourgeoise par le rehaussement des ponts a été réalisée par le conseil régional afin de garantir le passage de bateaux chargés de plusieurs couches de conteneurs .
Metz possède un réseau de bus géré par les Transports en commun de la région messine ( TCRM ) , administré par la communauté d' agglomération de Metz Métropole et à une filiale de la Caisse des dépôts et consignations .
Metz a disposé d' un réseau de tramway entre 1880 et 1948 , date de leur remplacement par des trolleybus qui ont circulé jusqu' en 1966 .
Elles s' inséraient dans le vaste réseau de pistes de promenade départementales qui reliaient déjà les Vosges jusqu' à Coblence en Allemagne , suivant le cours de la Moselle .
Depuis le 7 juin 2008 , la circulation à vélo est autorisée sur l' ensemble des secteurs piétonniers de la ville et dans les parcs , les jardins et les berges de la Moselle et de la Seille ( ce qui est nouveau ) , ainsi que dans toutes les rues et dans tous les sens de circulations .
Ce sont en tout 43470 km d' équipements cyclables accessibles à Metz et dans ses quartiers .
Les zones 30 seront étendues au quartier du Sablon au printemps 2009 et par la suite à l' ensemble du centre-ville .
Près de 1150 places de stationnement dédiées aux deux-roues sont aménagées à Metz dont 300 sont à proximité du centre-ville .
Les constructions se caractérisent par les tonalités ocre jaune de la pierre de Jaumont .
Lors de l' annexion allemande après 1871 , la ville s' est étendue , avec de nouveaux quartiers à l' architecture prussienne , comme la Nouvelle Ville ou une partie du Sablon .
En 1961 , Metz a fusionné avec trois communes de sa proche périphérie : Borny , Magny et Vallières .
Six mètres au-dessus , il est surplombé par la terrasse de la place Saint-Étienne datant des années 1760 .
Les maisons qui bordent la Moselle étaient habitées essentiellement par des bateliers et des pêcheurs .
Avant 1914 , l' occupant allemand avait prévu l' urbanisation du prolongement du quartier de la gare , le quartier Sainte-Thérèse .
La construction du quartier fut achevée dans les années 1920-1930 , après que Metz fut redevenue française .
Le Jardin botanique de Metz est une enclave dans la commune de Montigny-lès-Metz .
Le quartier Outre-Seille .
Entre 1900 et 1910 , la population du quartier du Sablon évolua presque du simple au triple , passant de 4000 à 11000 habitants .
Le quartier de Plantières Queuleu dont le fort emblématique fut construit entre 1868 et 1870 , est situé en hauteur , entre le Technopôle , le parc de la Seille et l' avenue de Strasbourg .
Le quartier de l' Amphithéâtre qui consiste en l' urbanisation d' anciennes friches ferroviaires , à proximité immédiate du centre-ville , a été conçu par l' agence Nicolas Michelin chargée de la maîtrise d' œuvre urbaine du projet .
La construction du centre Pompidou-Metz doit constituer la pierre d' angle du quartier .
Avec l' impulsion que représente le projet du Centre Pompidou-Metz , ville de la Grande Région , Metz a participé au programme de l' année européenne de Luxembourg et grande région capitale européenne de la culture 2007 .
Ville eau , Metz est traversée par la Seille et la Moselle .
Avant la Révolution , une bibliothèque publique était mise à disposition des messins par les religieux de l' abbaye de Saint-Arnould .
Des espaces Wi-Fi et des accès internet sont disponibles dans les trois médiathèques .
Les musées de la Cour d' Or existent depuis 1839 .
Metz a été l' une des premières villes française à mettre en place un périmètre piétonnier en centre-ville , développant le concept d' écologie urbaine sous l' instigation de Jean-Marie Pelt .
Au cœur de la ville , la place Saint-Jacques située non loin de l' emplacement du forum gallo-romain et la vaste place de la République qui ouvre sa perspective sur le paysage du mont Saint-Quentin à travers le jardin régulier de l' Esplanade , constituent deux des lieux les plus passants et animés ( cafés , cinémas , etc. ) .
Sous l' esplanade est aménagé un important parc de stationnement souterrain ( d' une capacité de 2300 places ) depuis les années 1960 où une galerie bordée de commerces relie l' Arsenal aux Galeries Lafayette .
Le New York Times a classé Metz à la 39 e place des destinations à visiter en 2009 , une recommandation qui doit beaucoup à l' ouverture prochaine du centre Pompidou-Metz et à la richesse architecturale de la ville .
Le lycée hôtelier Raymond-Mondon est considéré comme l' un des meilleurs de France avec ceux de Paris [ réf. nécessaire ] .
Metz fut longtemps connue pour être une ville de garnison , non loin de la frontière avec l' Allemagne .
Dans le cadre de la réforme de la carte militaire de 2008 , le départ d' environ 5000 hommes stationnés à Metz et son agglomération est prévu à partir de 2010 .
