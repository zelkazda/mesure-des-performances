Sous un rayon de soleil est un manga , recueil d' histoires courtes de Tsukasa Hōjō .
Publié en français aux éditions Tonkam , il comporte trois volumes au total .
Sarah est une jeune fille possédant un pouvoir spécial : elle peut parler avec les végétaux et peut même prendre un forme spécial ( elle devient un esprit avec un corps d' adulte ) .
Il est le père de Sarah .
Après un rêve , il abandonne l' idée de le couper et devient ami avec Sarah .
Il découvre le secret de Sarah en même temps que son professeur .
Au contact de Sarah , il s' ouvre de plus en plus à la nature .
Après un rêve dans lequel elle voit la fée des arbres qui n' est autre que Sarah , elle décide de prendre des cours de rééducation .
Il est le premier à avoir des doutes sur Sarah .
Mais ne voyant aucune différence entre la photo et la Sarah actuelle , il décide d' enquêter sur elle .
A la fin , il éprouvera trop tard du regret dans son attitude voyant en Sarah la femme parfaite pour lui .
A cette époque Sarah lui sauva la vie en utilisant ses pouvoirs mais elle dut quitter la ville pour ne pas être persécutée .
Dès le premier jour , il alla chez Sarah pour dissiper ses craintes .
Malheureusement pour lui , ce qu' il voit là-bas est conforme à ses souvenirs et dès qu' il voit Sarah il s' évanouie .
Il se rend compte que ni Sarah ni son père semble le reconnaître .
Lorsque son stage se termina , Sarah le raccompagna à la gare et le remercia pour son importance dans sa vie car elle se souvenait de lui .
