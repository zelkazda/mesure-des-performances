Après avoir participé à la création de la section de biologie au Palais de la Découverte , en 1936 , il fonde à Ville-d'Avray son propre laboratoire indépendant et se tient à l' écart des structures universitaires qu' il juge trop contraignantes .
Jean Rostand commence par publier quelques essais philosophiques , puis partage son temps entre son métier de chercheur et une très abondante production scientifique et littéraire .
Toutefois , Rostand soutint une forme d' eugénisme ( ou eugénisme positif ) , approuvant tant les écrits d' Alexis Carrel que la loi nazie de 1933 prévoyant la stérilisation de personnes atteintes de certaines formes de maladies mentales .
Homme de science , biologiste , pamphlétaire , moraliste , Jean Rostand est aussi pacifiste .
Lors du procès de Bobigny autour de l' avortement , en 1972 , il témoigne en faveur de la défense .
Jean Rostand entre à l' Académie française en 1959 et continue ses campagnes d' information lors de conférences , à la radio ou à la télévision .
Jacques Chardonne , qui fut un de ses proches , a dit de lui : " Jean Rostand , c' est la bonté même , la bonté absolue et dans son plein éclat " .
