Issue de la Rhodésie du Nord britannique , elle fait partie intégrante du Commonwealth .
République démocratique , sa capitale est Lusaka .
La création de la fédération de Rhodésie et du Nyassaland en 1953 , dans le but de développer la région et de limiter les velléités indépendantistes , échoue avec l' accès à l' indépendance du pays le 24 octobre 1964 .
La Zambie est une république parlementaire .
Le climat de la Zambie est tropical , quoique plus tempéré en altitude .
Le fleuve principal est le Zambèze , dont le barrage de Kariba fournit le pays en hydro-électricité .
Quelques villes de Zambie : Lusaka , la capitale , Kitwe , Livingstone , Kabwe , Kasumbalesa et Ndola .
La Zambie produit du maïs .
La Zambie est un grand exportateur de cuivre et de cobalt .
La Zambie a une population d' environ 10 millions d' habitants ( recensement de 2001 ) .
La Zambie est un pays sans scolarité obligatoire .
La Zambie a pour codes :
