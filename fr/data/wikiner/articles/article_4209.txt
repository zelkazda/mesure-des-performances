Sa capitale est Magdebourg .
La Saxe-Anhalt est principalement constituée d' une plaine utilisée pour l' agriculture , et , au sud-ouest , de la partie orientale des montagnes du Harz .
La rivière principale est l' Elbe qui coule du sud-est au nord-ouest .
Le 1er janvier 2004 la Saxe-Anhalt a supprimé ses 3 districts de Dessau , Halle et Magdebourg .
Le 1er juillet 1994 , la Saxe-Anhalt a réduit ses arrondissements de 37 à 21 , puis , le 1er juillet 2007 , de 21 à 11 .
Ce territoire d' Allemagne orientale fut constitué en 1947 par la fusion du land d' Anhalt et de la province de Saxe prussienne .
