Charles Denner , acteur de théâtre et de cinéma , né le 28 mai 1926 à Tarnów en Pologne , mort le 10 septembre 1995 à Dreux .
Né à Tarnów ( Pologne ) , issu d' une famille juive polonaise , Charles Denner arrive à Paris , en France à l' âge de quatre ans .
Il entre à 16 ans dans la Résistance avec Alfred .
Après la guerre , il commence le théâtre en entrant au cours d' art dramatique Charles Dullin .
C' est au Festival d' Avignon également créé par Jean Vilar qu' il donne la réplique à Gérard Philipe en 1951 dans Le Prince de Hombourg ( von Kleist ) .
Au TNP encore , il joue avec Jeanne Moreau , François Périer , Michel Galabru , et bien d' autres comédiens célèbres de cette génération qui firent comme lui leurs débuts dans ce haut lieu de l' art dramatique français .
Après une apparition dans Volpone en 1941 , Yves Allégret lui offre une petit rôle au cinéma en 1955 , dans La Meilleure Part , suivi deux ans plus tard par Louis Malle dans Ascenseur pour l' échafaud .
A partir de 1972 , il joue dans quatre films de Claude Lelouch dont L' aventure c' est l' aventure un magnifique film qui débute la série .
Il est inhumé au cimetière de Bagneux .
