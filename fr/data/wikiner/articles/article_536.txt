Pidgin ( anciennement Gaim ) est un logiciel libre client de messagerie instantanée multiprotocole et multiplate-forme permettant de se connecter , entre autres , à XMPP , IRC , ICQ / AIM , Yahoo ! Messenger et Microsoft Messenger .
Pidgin est un logiciel utilisant la bibliothèque GTK+ , ce qui lui a valu d' être , durant quelques années , le client de messagerie instantanée officiel du bureau GNOME .
Publié sous GNU GPL , il est disponible pour Linux , Windows , BSD et Mac OS X .
Pidgin a été écrit en séparant l' interface graphique du code fonctionnel .
Ainsi , toutes les fonctionnalités ayant trait aux connexions aux réseaux de messagerie instantanée et à la gestion des comptes sont regroupées dans la bibliothèque libpurple , permettant à d' autres logiciels libres d' utiliser ces mêmes fonctionnalités .
Adium , le client de messagerie instantanée sous Mac OS X utilise libpurple , de même que QuteCom .
Gaim ( GTK+ AOL Instant Messenger ) a été renommé en Pidgin le 6 avril 2007 en raison de plaintes de la société AOL .
Le nom utilisant la marque déposée AIM de AOL .
Parmi les protocoles gérés par Pidgin , on trouve ( par ordre alphabétique ) :
