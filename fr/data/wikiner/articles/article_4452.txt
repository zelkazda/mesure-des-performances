La commune tire son nom actuel de l' un des saints fondateurs légendaires de la Bretagne : Saint Pol Aurélien .
Elle fut longtemps le siège d' un évêché aujourd'hui rattaché à celui de Quimper .
Et ici et là , des dizaines de chapelles , de maisons anciennes , de manoirs et de châteaux … Saint-Pol-de-Léon est aujourd'hui une cité fermement tournée vers l' avenir .
