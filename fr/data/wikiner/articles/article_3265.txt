La rigidité du comte de Chambord , qui rappelle celle des derniers rois , et les divisions des royalistes entre légitimistes et orléanistes achèvent de ruiner toute idée de rétablissement de la monarchie , même constitutionnelle .
Ce modèle a déjà échoué par trois fois et a coûté la vie à son brillant promoteur Antoine Barnave .
De plus , la conclusion des deux premières républiques par la dictature d' un empereur milite pour une limitation du pouvoir exécutif : le coup d' État de Napoléon III , premier président élu au suffrage universel masculin , est encore un souvenir vivace .
Dans un second temps , l' usure et un affrontement éprouvant avec l' Allemagne vont mettre en évidence les conséquences néfastes d' un affaiblissement trop marqué du pouvoir exécutif et entraîner la chute du régime .
Un gouvernement de la Défense nationale est constitué avec à sa tête le général Trochu .
Sont également membres de ce gouvernement Jules Favre , Jules Ferry , Léon Gambetta , Ernest Picard , Henri Rochefort , Jules Simon , tous élus de Paris .
L' armée de Bazaine résiste toujours dans Metz assiégée .
Paris est également assiégée à partir du 19 septembre .
Le 7 octobre Gambetta quitte Paris en ballon pour tenter de réorganiser la défense à partir de la province , ce qui n' est pas facile compte tenu de l' organisation en étoile du territoire à partir de la capitale .
La capitulation précipitée de Bazaine et de l' armée de Metz ( 150000 hommes ) le 30 octobre porte un grave coup à la France .
Elle intervient au moment où le gouvernement , parti s' établir à Tours , est parvenu à organiser une Armée de la Loire .
Les Prussiens libérés peuvent alors concentrer leurs forces sur cette nouvelle armée mal entrainée et mal équipée , ce qui oblige le gouvernement à se replier sur Bordeaux .
Le 18 janvier , l' Unité allemande est réalisée , les souverains allemands réunis au château de Versailles proclament le roi Guillaume de Prusse empereur d' Allemagne .
Le 28 janvier , Paris capitule après 132 jours de siège .
Jules Favre négocie les conditions de l' armistice .
Opposé à la trêve , Gambetta démissionne .
Le court délai empêche pratiquement toute campagne , sauf à Paris .
Les Français , lassés de la guerre ou inquiets de la voir se rapprocher de leur région , toujours méfiants vis-à-vis des troubles parisiens , préfèrent voter pour les tenants de la paix sans condition , c' est-à-dire les listes conservatrices dans lesquelles les notables figurent en bonne place .
Ils chargent Adolphe Thiers de ces tâches ingrates .
Les négociations de paix sont menées ultérieurement par Jules Favre et Adolphe Thiers avec Bismarck .
Le 10 mai 1871 le traité de Francfort est signé , Bismarck exige et obtient :
Le 21 mai les Versaillais parviennent à entrer dans la ville .
Les monarchistes conservent Adolphe Thiers au pouvoir le temps de solder les conséquences de la guerre tout en préparant le retour de leurs prétendants .
Le 5 juillet , un message d' Henri d' Artois , duc de Bordeaux , comte de Chambord -- prétendant des légitimistes -- laisse supposer qu' il renonce au drapeau blanc ( de la royauté ) au profit du drapeau tricolore .
Cependant le 3 juillet , le comte de Chambord refuse la visite de Philippe d' Orléans -- prétendant orléaniste -- ruinant les projets de fusions des deux partis .
Thiers , en fin politique , comprend que , s' il veut s' assurer un soutien le plus large , il doit dissimuler ses intentions .
Ce qui permet à Thiers de leur répondre : " Puisque vous êtes la majorité , que n' établissez-vous la monarchie ?
Cependant le 30 octobre 1873 , le comte de Chambord publie un nouveau manifeste pour le drapeau blanc .
Thiers assure le financement des indemnités de guerre par le lancement d' emprunts largement souscrits par les Français et obtient le retrait anticipé des troupes allemandes qui occupent encore en France .
Après la mort de Napoléon III le 7 janvier 1873 , les bonapartistes s' allient aux royalistes pour préserver les chances du prince impérial .
Thiers démissionne le 23 mai 1873 toujours persuadé qu ' après lui ( ce sera ) le chaos , mais la droite avait déjà prévu un remplaçant en la personne de Mac Mahon qui est élu par 390 voix le 24 mai .
Sous la présidence de Mac Mahon , d' obédience légitimiste , la tendance est à l' ordre moral , fondé sur l' encouragement des valeurs religieuses avec par exemple la publication du Pèlerin , l' apparition du pèlerinage de Lourdes , l' édification de la basilique du Sacré-Cœur sur la butte Montmartre .
Le retour du roi semble imminent après une rencontre entre Henri d' Artois et Philippe d' Orléans mais d' Artois refuse toujours de renoncer au drapeau blanc et l' affaire de nouveau échoue .
D' Artois étant déjà âgé , les orléanistes attendent avec impatience sa disparition .
Mais sa majorité s' effrite et le 16 mai 1874 il est remplacé par Ernest Courtot de Cissey , allié aux bonapartistes victorieux lors des élections partielles .
Mac Mahon se résigne et laisse le gouvernement aux républicains .
Jules Ferry est la personnalité dominante des premiers gouvernements républicains de 1879 à 1885 , en charge soit de l' éducation nationale , soit des affaires étrangères .
Seule Paris reste sous la tutelle de son préfet : elle ne pourra élire son maire qu' à partir de 1976 .
Le général Boulanger est ministre de la guerre en 1886 .
Ce scandale , relayé à droite par les boulangistes et à gauche par les ambitieux Jules Ferry et Georges Clemenceau , jette l' opprobre sur la gouvernance d' alors et entraine la démission de Jules Grévy .
Boulanger devient le point de rencontre des espérances les plus contradictoires et donne l' espoir à tous .
Il est élu plusieurs fois député en province en 1888 puis à Paris en 1889 mais il refuse de s' emparer du pouvoir par la force comme le lui demandent certains de ses partisans .
Le 4 février 1889 , la faillite du canal de Panamá provoque la ruine de 85 000 souscripteurs .
En 1892 , Édouard Drumont , un journaliste ouvertement antisémite , dénonce les conditions de financement du canal .
Les anarchistes réalisent à cette époque des attentats spectaculaires comme l' assassinat du président Sadi Carnot le 26 juin 1894 ) .
En 1894 , le capitaine Dreyfus , accusé d' espionnage au profit de l' Allemagne , est condamné à la déportation à vie sur l' Île du Diable en Guyane .
Plusieurs personnalités tentent , en vain , de démontrer l' innocence de Dreyfus .
On parle alors de l' Affaire Dreyfus .
Beaucoup se rassemblent dans la Ligue des droits de l' homme , créée à cette occasion .
L' affaire Dreyfus se traduit en termes électoraux par une nouvelle poussée à gauche aux élections de 1898 , contre les partisans d' un ordre plus autoritaire dont on identifie des bastions dans l' armée et dans l' église .
Le progressiste Waldeck-Rousseau épure la hiérarchie militaire et fait adopter la loi 1901 sur les associations , qui soumet les congrégations religieuses à autorisation administrative .
Une diversification de ses financements pourrait en outre lui donner une plus grande marge de manœuvre par rapport à la France .
Une Triple-Entente se met en place .
L' Autriche-Hongrie avait déjà annexé la Bosnie-Herzégovine en 1908 .
Le socialiste Jean Jaurès , militant infatigable de la paix est assassiné .
L' armée allemande tente d' éliminer la France par un mouvement tournant au nord mais est vaincue par Joffre sur la Marne .
Le président Raymond Poincaré appelle alors à la tête du gouvernement Georges Clemenceau , " le tigre " , qui va mener le pays à la victoire ( novembre 1917-janvier 1920 ) .
Sur le plan militaire , Pétain prête une attention particulière au moral des troupes et rétablit la confiance par des premiers succès .
Mais , contredisant les propos de Guillaume II , la république a tenu le choc de la guerre , contrairement aux empires .
Celles de 1924 sont une victoire moins nette de la gauche : Édouard Herriot forme un gouvernement radical soutenu par les socialistes qui ne dure pas .
Alexandre Millerand cherche à accroître les prérogatives du pouvoir exécutif et a soutenu la droite .
Mais il transige finalement et accepte l' arbitrage de comités d' experts : ce seront les plans Dawes et Young .
La France a été saignée par le conflit .
L' arrivée au pouvoir du radical Édouard Herriot ( 1924 ) est l' occasion d' amorcer une politique de détente avec l' Allemagne dont l' artisan est Aristide Briand .
Aristide Briand s' efforce d' œuvrer pour la paix en favorisant une politique internationale d' arbitrage et de limitation des armements .
La crise économique internationale touche la France avec retard mais y reste plus tenace qu' ailleurs .
D' autre part , la disparition de Poincaré et Briand laisse un vide et l' instabilité ministérielle reprend de plus belle .
Un nouveau gouvernement présidé par Gaston Doumergue est censé aborder la réforme du régime .
En fait partie André Tardieu , un des responsables de la droite , qui souhaite renforcer les pouvoirs de l' exécutif , notamment : Il ne parvient pas à convaincre et quitte la vie politique en 1936 .
C' est à ce moment qu' est unifié le réseau ferré géré désormais par une société nationale , la SNCF .
Les différences de vues , entre autres , sur la guerre d' Espagne affaiblissent la coalition dès juin 1937 .
Le gouvernement , dirigé par Paul Reynaud , quitte Paris menacée , le 10 juin .
La crise anarchiste , comme la crise boulangiste et l' affaire Dreyfus , est liée à un climat d' instabilité économique , sociale et puis politique .
Cette " crise anarchiste " est caractérisée par de nombreux attentats , elle a été provoquée par Ravachol .
Ainsi certaines personnes ont été condamnées pour s' être réjouies de la mort de Sadi Carnot .
