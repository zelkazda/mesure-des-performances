Zheng He était un Hui , un Chinois Han musulman .
Il fait de Zheng He l' amiral de la flotte impériale , sans que celui-ci ne soit jamais allé en mer .
Il lance la construction de centaines de navires à Nankin , sur le Yangzi Jiang ( ce qui réduira de moitié la couverture forestière du sud de la Chine ) et ordonne de grandes expéditions exploratrices dans tout l' océan Indien .
En tant qu' amiral , Zheng He effectue sept voyages de 1405 à 1433 .
Après la découverte d' un gouvernail énorme lors de fouilles dans le sud-est de la Chine et en se fondant sur un récit datant de près de 100 ans après l' époque de Zheng He , certains spécialistes affirment que ces vaisseaux pouvaient atteindre 138 mètres de long et 55 mètres de large et comptaient neuf mâts .
Un parchemin bouddhiste datant de l' époque de Zheng He et représentant des vaisseaux à 4 mâts semble infirmer cette thèse : ceux-ci n' auraient alors mesuré qu' une soixantaine de mètres de long .
Cela reste toutefois bien supérieur aux 30 mètres de long et 8 mètres de large de la Santa Maria , la caraque de Christophe Colomb , qui sera construite environ 70 ans plus tard .
Zheng He explora , durant toutes ces longues années de voyage :
C' est à la suite d' une de ces expéditions qu' en 1414 , le sultan de Malindi ( dans l' actuel Kenya ) inaugure des relations diplomatiques avec la Chine .
La plupart des récits furent retracés par Ma Huan ( 馬歡 ) , fidèle compagnon de route de l' amiral Zheng He .
Durant leurs voyages , Ma Huan a noté minutieusement des choses concernant la géographie , les lois , la politique , les conditions climatiques , l' environnement , l' économie , les coutumes locales .
Les côtes de l' Australie auraient même été atteintes lors de ces voyages d' exploration .
Sur la côte nord de l' île de Java en Indonésie , Zheng He est l' objet d' un culte .
