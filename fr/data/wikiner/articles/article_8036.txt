Sa capitale est Évreux et ses principales forteresses étaient Tillières-sur-Avre et Laigle .
En 1037 , Richard , son fils lui succédera .
En 1316 et à nouveau en 1427 , le comté d' Évreux est érigé en comté-pairie .
