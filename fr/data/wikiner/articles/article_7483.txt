Il étudie la médecine à Montpellier de 1760 à 1764 .
Après l' obtention de son diplôme , il ouvre un cabinet à Lyon et consacre son temps libre à herboriser dans la région .
Il fonde un jardin botanique dans le quartier des Brotteaux , mais l' opération le ruine complètement , car il ne reçoit pas le soutien qu' il espérait de la part de l' administration .
Heureusement , il réussit , grâce au soutien d' Albrecht von Haller ( 1758-1823 ) et d' Antoine Gouan ( 1733-1821 ) , à obtenir un poste à Grodno ville dans laquelle le roi de Pologne , Stanislas II ( 1732-1798 ) souhaite moderniser l' enseignement de l' histoire naturelle et de la médecine .
Il y est remplacé par Georg Forster ( 1754-1794 ) .
Il gardera néanmoins une profonde affection pour la Lituanie .
Certaines d' entre elles sont des rescapées de l' œuvre de Pierre Richer de Belleval ( v. 1564-1632 ) , d' autres proviennent de divers auteurs comme Sébastien Vaillant ( 1669-1722 ) ou Linné .
