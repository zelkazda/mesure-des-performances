Versainville se situe à une trentaine de km au sud de Caen , à proximité immédiate de Falaise , dans le département du Calvados .
L' origine du nom Versainville est très controversée .
Dès le paléolithique , il y a une présence humaine sur le site de Versainville , présence attestée par la découverte d' outils et d' éclats de silex .
Versainville a compté jusqu' à 562 habitants en 1800 .
