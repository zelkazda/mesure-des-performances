Situé à l' ouest du pays , la Rhénanie-Palatinat est frontalière avec trois pays européens et quatre autres lands allemands ( depuis le nord et dans le sens des aiguilles d' une montre ) la Rhénanie-du-Nord-Westphalie , la Hesse , le Bade-Wurtemberg , la France , la Sarre , le Luxembourg et la Belgique ( Région wallonne ) .
Sa capitale ( depuis 1950 ) est la ville de Mayence ( Coblence fut la capitale provisoire de 1947 à 1950 ) .
Le bassin de Coblence a la pluviométrie la plus basse de toute l' Allemagne ( < 500 mm/p.a. ) .
Le Rhin et ses affluents ( Moselle , Nahe et Lahn ) la divisent en quatre parties : Eifel et Hunsrück sur la rive gauche ; Westerwald et Taunus , sur la rive droite .
Les deux derniers plateaux ne se trouvent que partiellement en Rhénanie-Palatinat .
De 1798 à 1815 , une grande partie des territoires de l' actuelle Rhénanie-Palatinat , notamment ceux situés sur la rive gauche du Rhin sont occupés par les armées françaises ( à l' exception donc du Westerwald situé sur la rive droite ) .
Le congrès de Vienne rétrocède ces territoires au Royaume de Prusse et à celui de Bavière , devenus membres de nouvelle Confédération germanique créée par les vainqueurs .
Cette région subit les conséquences de la fin de la Seconde Guerre mondiale avec de lourdes séquelles de guerre , qui s ' ajoutent aux séquelles du développement industriel et agricole .
La région subit particulièrement le passage du nuage radioactif de Tchernobyl . .
Les 24 arrondissements de Rhénanie-Palatinat :
Les 12 villes-arrondissements de Rhénanie-Palatinat :
En raison de sa création récente , le land de Rhénanie-Palatinat ne dispose pas d' armes " historiques " , comme sa voisine la Hesse .
L' université Johannes Gutenberg de Mayence est une université publique allemande localisée dans la ville de Mayence .
Fin janvier 2005 , le taux de chômage en Rhénanie-Palatinat s' élève à 9,4 % contre 7,4 % en mai 2004 .
L' Eifel , le massif le plus étendu , est formée de plusieurs éléments :
Sur l' Eifel , on cultive ( à taille réduite ) des céréales , des cultures sarclées et fourragères .
La vallée de la Moselle est réputée pour son vignoble .
À l' est de la Moselle se trouve le Hunsrück qui est similaire à l' Eifel .
