Disciple de Jacques Champion de Chambonnières , il lui succède en 1663 ou 1664 , après sa disgrâce , comme ordinaire de la musique de la chambre du roi pour le clavecin .
Il publie en 1689 son unique livre de clavecin , ouvrage dans lequel il ajoute , aux suites de danses de sa composition , des transcriptions d' œuvres de Jean-Baptiste Lully dont il est ami .
Ce livre est en France le premier ouvrage imprimé qui comporte une table des ornements indiquant la manière de les exécuter .
