Ce n' est qu' avec l' apparition de l' anatomie grâce à André Vésale et la compréhension croissante des mécanismes de la reproduction et de la formation de l' embryon que l' on commence à pouvoir scientifiquement expliquer les malformations .
L' imaginaire demeure très présent : Paré parle de " monstres célestes " , évoque les animaux exotiques et décrit des créatures à mi-chemin entre l' homme et l' animal .
Paré indique que les jumeaux soudés entre eux sont causés par une " abondance de semence " .
Les travaux de Fabrice d' Acquapendente et William Harvey formeront des bases solides sur l' étude de l' embryon .
Harvey découvre que les jumeaux rattachés l' un à l' autre sont issus de la présence de deux œufs qui se soudent .
Le zoologiste français Étienne Geoffroy Saint-Hilaire est suivi par son fils Isidore Geoffroy Saint-Hilaire qui élabore de 1832 à 1837 " L' histoire générale et particulière des anomalies de l' organisation ches l' homme et les animaux " .
Les Saint-Hilaire et l' anatomiste allemand Johann Friedrich Meckel von Helmsbach ( 1781-1833 ) passent pour être les fondateurs de la tératologie .
