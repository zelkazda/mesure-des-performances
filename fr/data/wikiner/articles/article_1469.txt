Le Zilog Z80 est un microprocesseur 8- bits conçu et fabriqué par Zilog .
Il a été conçu pour être compatible au niveau binaire avec l' Intel 8080 de sorte que la plus grande partie du code 8080 , notamment le système d' exploitation CP/M , fonctionne sans modification dessus .
Des utilisations du processeur comprennent des calculatrices Texas Instruments , les consoles de jeux vidéo portables GameBoy et Game Gear .
Pour le ZX 81 , ils servaient à la gestion du système comme l' affichage , le programmeur en langage machine devait les restaurer après utilisation ce qui était un inconvénient sur cette machine .
