Le canton du Tessin est un canton de Suisse .
Son nom vient de l' affluent du Pô homonyme .
La zone ne fut annexée par Rome qu' assez tardivement .
Plus tard , Hannibal livre bataille dans la plaine du tessin contre les romains , c' est une de ses premières victoires contre cet ennemi .
Les guerres successives menées par le canton suisse d' Uri ( 1440 , 1500 ) , notamment la bataille de Giornico , puis bientôt par la totalité de la Confédération helvétique ( 1512 ) aboutirent à l' annexion de la zone : la haute vallée du Tessin , depuis le massif du Saint-Gothard jusqu' à la ville de Biasca , fut rattachée au canton d' Uri , le reste du territoire étant placé sous gestion collective .
En 1803 , le canton du Tessin est créé par fusion des cantons alors distincts de Lugano et Bellinzone .
Les trois villes principales ( Lugano , Bellinzone et Locarno ) se disputent alors le titre de capitale ; un décret de 1878 fixe définitivement les institutions à Bellinzone .
Le Tessin est le seul canton de Suisse situé entièrement au sud de l' arc alpin .
Il côtoie au nord les cantons du Valais par le col de la Novena , et d' Uri , dont il est séparé par le col du Saint-Gothard .
Il partage sa limite nord-est avec les Grisons ; au sud et à l' ouest , il est frontalier avec la Lombardie et le Piémont .
Canton au climat méridional , le Tessin est la Suisse des palmiers .
Le canton du Tessin compte 328 580 habitants en 2007 , soit 4,3 % de la population totale de la Suisse ; parmi eux , 82 794 ( 25,2 % ) sont étrangers .
Le Tessin est le seul canton où l' italien est l' unique langue officielle .
L' italien est parlé au quotidien par 83 % des habitants du Tessin , tandis que 9 % utilisent l' allemand comme langue d' usage .
