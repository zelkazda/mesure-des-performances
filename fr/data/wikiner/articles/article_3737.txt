Jean Yanne ( de son vrai nom Jean Gouyé ) est un acteur , auteur , réalisateur , producteur et compositeur français .
Né le 18 juillet 1933 aux Lilas ( Seine-Saint-Denis ) , il est mort d' une crise cardiaque le 23 mai 2003 dans sa propriété de Morsains ( Marne ) .
Ancien élève du lycée Chaptal , il avait commencé des études de journalisme qu' il abandonna pour écrire des sketches de cabaret .
Toujours à la recherche de son style , il écrira dans l' hebdomadaire L' Os à Moelle , brièvement repris par Pierre Dac en 1965 .
Avec Jacques Martin , il apparaît dans une émission de télévision , 1 = 3 , très caustique pour son temps , qui est arrêtée après trois numéros .
Mais , passant à 20h30 sur l' unique chaîne de l' époque , les deux compères sont immédiatement connus de la France entière .
Sa manière de plaisanter , agressive , débraillée , versant du vitriol sur des plaies ouvertes , tenant la compassion pour obscénité , choquait un peu la France de l' époque .
Bref , il fut viré de la radio ( le film Tout le monde il est beau , tout le monde il est gentil en parle de façon romancée ) .
En 1967 , il joue dans Week-End de Jean-Luc Godard , puis se révèle véritablement en 1969 dans Que la bête meure de Claude Chabrol , où il incarne un homme intelligent , mais d' une absence de sensibilité qui le rend brutal .
Il enchaîne avec Le Boucher de Claude Chabrol , où il se retrouve en inquiétant commerçant , amoureux et assassin .
Avec Maurice Pialat , en 1971 , il tourne Nous ne vieillirons pas ensemble , où il incarne à nouveau son personnage d' insensible , et pour lequel il obtient le prix d' interprétation au festival de Cannes qu' il refuse .
Avec son compagnon d' écriture Gérard Sire , il brocarde la radio qu' il connaît bien , avec le film Tout le monde il est beau , tout le monde il est gentil en 1972 , la politique avec le film Moi y'en a vouloir des sous en 1973 et Les Chinois à Paris en 1974 , le monde du spectacle avec le film Chobizenesse en 1975 , et celui de la télévision avec le film Je te tiens , tu me tiens par la barbichette en 1978 .
Il réalise ensuite une parodie de péplum , Deux heures moins le quart avant Jésus-Christ en 1982 , qui remporte un gros succès public , puis de nouveau une parodie du monde politique avec Liberté , égalité , choucroute .
Jean Yanne oscillait entre deux faces d' un même personnage :
Présenté par Olivier Lejeune , le programme a créé une polémique , à l' époque de sa diffusion car , lors de l' épreuve finale , les candidats devaient compter des centaines de véritables billets de banque pendant qu' ils étaient déstabilisés par tout un tas d' éléments perturbateurs .
Jean Yanne est inhumé au cimetière des Lilas .
Jean Yanne présente également une facette aujourd'hui peu connue du grand public , celle d' un scénariste et dialoguiste de bande dessinée , en tandem avec le dessinateur Tito Topin .
