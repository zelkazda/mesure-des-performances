Comme l' avaient prévu plusieurs analystes , la disparition du régime de Saddam Hussein et l' occupation du pays par les forces américano-britannique entraîne une augmentation importante de l' activité terroriste d' origine islamiste .
Mais il est important de relativiser : l' augmentation du nombre d' attaques terroristes n' est pas forcément due entièrement à l' occupation de l' Irak .
Elles ont augmenté depuis que la structure pyramidale du réseau Al-Qaïda a été détruite lors de l' invasion de l' Afghanistan .
Les cellules terroristes sont donc maintenant plus autonomes et n' attendent plus les ordres des responsables d' Al-Qaïda .
Le 31 mars 2004 , des images de foules en colère à Falloujah ont été diffusées par les médias occidentaux .
Elles montrent la foule frappant , traînant à travers la ville puis pendant à un pont les corps de quatre employés états-uniens de la société militaire privée Blackwater , tués par des grenades .
Ces images ont rappelé celles de 1993 , à Mogadiscio en Somalie .
L' invasion et l' occupation de l' Irak ont joué un rôle non négligeable dans la vague d' attentats du 7 juillet 2005 à Londres .
D' après les représentants de certains pays européens , comme la France , l' Allemagne ou la Russie , l' invasion de l' Irak en dehors de tout mandat délivré par l' ONU constitue un acte d' agression pur et simple , et non une libération ou un acte d' auto-défense .
Dès la fin de la période de guerre conventionnelle , l' Irak a connu une courte période d' anarchie totale .
Les troupes d' occupation ne sont absolument pas intervenues , ni pour empêcher les destructions des souvenirs liés au régime de Saddam Hussein , ni pour empêcher les pillages des richesses du pays : banques , musées , etc .
Il ne s' agit cependant que d' un faible recul , sur un territoire exigu et surpeuplé , sans ressources en eau , et avec un faible risque pour le gouvernement , car il n' y avait que 8 000 colons israéliens à Gaza ; de plus , elle permet à Israël de se renforcer en Cisjordanie , plus riche en eau et moins peuplée .
Du côté palestinien , le groupe terroriste Hamas remporte les élections législatives palestiniennes de février 2006 .
Là encore , au lieu de faire disparaître le terrorisme , l' intervention des États-Unis a légitimé les groupes terroristes ou amplifié leur action .
La Syrie a cependant été contrainte d' évacuer le Liban après l' assassinat de Rafic Hariri , dirigeant libanais .
Dans un contexte trouble , ce dernier essayant de se débarrasser de la Syrie pourrait avoir été assassiné par le gouvernement de ce dernier , ce que tant à démontrer l' enquête menée sous l' égide de l' ONU .
Courant novembre 2005 , le président Syrien a fait une allocution publique retransmise sur différentes chaînes à travers le monde , dont la chaîne européenne privée Euronews .
Les gouvernements de la Coalition estimait avant le début de cette guerre le coût des opérations militaires et de la reconstruction entre 100 et 200 milliards de $ .
