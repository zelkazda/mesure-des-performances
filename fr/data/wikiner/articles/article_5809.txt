RC5 est un chiffrement par bloc , fonctionnant grâce à une clé , dont la longueur varie de 0 à 2048 bits .
RC6 , basé sur RC5 fut candidat au concours pour devenir le standard actuel de chiffrement ( AES ) .
Contrairement à de nombreux algorithmes , RC5 possède une taille variable de bloc 32 , 64 ou 128 bits ) , une clef allant de 0 à 2048 bits et un nombre de tour de 0 à 255 .
12-tours de RC5 ( avec un bloc de 64-bit ) est considéré susceptible à une attaque différentielle utilisant 2 44 textes clairs .
