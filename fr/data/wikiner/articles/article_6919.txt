Né au Val d' Aoste , en Italie , Maurice Garin est naturalisé français après sa majorité ( en 1892 ou 1901 ) .
Après sa victoire dans le Tour de France en 1903 , seul le premier de ces deux surnoms est mentionné dans la presse .
En 2003 , pour le centième anniversaire de sa victoire , une rue de Maubeuge fut renommée en son honneur .
Après son déclassement du Tour de France 1904 , il est suspendu deux ans et arrête la compétition après une vingtaine de victoires à son actif étalées sur une décennie .
La ville de Lens lui rendra hommage en baptisant son vélodrome du nom de Maurice Garin .
Il crée après la Seconde Guerre mondiale une équipe " Garin " .
Maurice avait deux frères , cyclistes professionnels également :
