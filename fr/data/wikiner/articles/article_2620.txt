Il travaille sur Dragon Ball de 1984 à 1995 .
Pendant ces onze ans , il dessine 42 volumes d' environ 200 pages chacun ( ce qui fait de Dragon Ball une œuvre d' environ 10.000 pages ) .
Grâce à son sens créatif , Toriyama obtient du travail avec plusieurs projets de jeux vidéo tel Dragon Quest .
Il conçoit également les personnages de Chrono Trigger sur Super Nintendo , des jeux de combat Tobal No. 1 et Tobal 2 sur PlayStation , et enfin de Blue Dragon sur Xbox 360 .
Après Dragon Ball , Toriyama produit des one shot ou de courts récits ( de 100 à 200 pages ) , tels que Go ! Go ! Ackman , Cowa , Kajika , Sand Land ou encore Cross epoch en collaboration avec Eiichirō Oda .
Dragon Ball Evolution est malmené par les critiques et par les fans mais l' auteur reste silencieux .
Avec Dragon Ball il devient un des plus importants représentants du shōnen manga , et comme Osamu Tezuka , il voit son style graphique souvent copié et intégré par beaucoup de mangaka .
Son style s' adapte à l' évolution de Dragon Ball , son trait se durcit pendant cette période ( les traits anguleux deviennent plus systématiques ) comparativement à un style plus rond pour ses premières productions , puis pour Dr Slump .
Alors que les histoires devenaient presque exclusivement basées sur le combat , il met fin à la série Dragon Ball .
Il réalise ensuite des histoires ( en one shot ) dans le ton de ses premières productions , ou de certains chapitres de Dr Slump , bien qu' ayant évolué graphiquement .
Akira Toriyama affirme souvent dans ses messages au lecteur être profondément attaché à sa vie à la campagne japonaise , et redouter la vie urbaine .
Cette part de sa personnalité se ressent dans pratiquement toutes les histoires qu' il a créées : le héros vient souvent de la campagne , et le thème de l' enfant qui découvre la ville sans jamais l' avoir vue , ou inversement de la personne habituée à la ville qui subit les aléas de la campagne est souvent réutilisé ( Dr Slump , Sand Land , Dragon Ball , Kajika , etc .
Tous ses mangas se passent dans un même monde à différentes époques , The World , une planète jumelle à la Terre .
Ces séries sont inspirées des œuvres d' Akira Toriyama .
Ces films d' animation sont inspirés des œuvres d' Akira Toriyama .
Ces jeux vidéo sont inspirés des œuvres d' Akira Toriyama .
Ces films d' animation sont inspirés des œuvres d' Akira Toriyama .
Ces films sont inspirés des œuvres d' Akira Toriyama .
Ces mangas sont adaptés des jeux vidéo créés graphiquement par Akira Toriyama .
Ces films d' animation sont inspirés des œuvres d' Akira Toriyama .
