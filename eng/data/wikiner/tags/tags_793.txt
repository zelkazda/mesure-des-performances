The	O
American	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
(	O
AFL	ORGANIZATION
)	O
was	O
a	O
major	O
Professional	ORGANIZATION
Football	ORGANIZATION
league	O
that	O
operated	O
from	O
1960	O
until	O
1969	O
,	O
when	O
it	O
merged	O
with	O
the	O
established	O
National	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
(	O
NFL	ORGANIZATION
)	O
.	O
The	O
upstart	O
AFL	ORGANIZATION
operated	O
in	O
direct	O
competition	O
with	O
the	O
more	O
established	O
NFL	ORGANIZATION
throughout	O
its	O
existence	O
.	O
Overall	O
,	O
AFL	ORGANIZATION
teams	O
signed	O
75	O
%	O
of	O
the	O
league	O
's	O
draft	O
choices	O
that	O
first	O
year	O
.	O
It	O
continued	O
to	O
attract	O
top	O
talent	O
from	O
colleges	O
and	O
the	O
NFL	ORGANIZATION
by	O
the	O
mid-1960s	O
,	O
well	O
before	O
the	O
Common	MISC
Draft	MISC
which	O
began	O
in	O
1967	O
.	O
During	O
its	O
final	O
two	O
years	O
of	O
existence	O
,	O
the	O
AFL	ORGANIZATION
teams	O
won	O
upset	O
victories	O
over	O
the	O
NFL	ORGANIZATION
teams	O
in	O
Super	MISC
Bowl	MISC
III	MISC
and	O
IV	MISC
,	O
the	O
former	O
considered	O
among	O
one	O
of	O
the	O
biggest	O
upsets	O
in	O
American	MISC
sports	O
history	O
.	O
When	O
the	O
merger	O
took	O
place	O
,	O
all	O
ten	O
AFL	ORGANIZATION
franchises	O
became	O
part	O
of	O
the	O
merged	O
league	O
's	O
new	O
American	ORGANIZATION
Football	ORGANIZATION
Conference	ORGANIZATION
(	O
AFC	ORGANIZATION
)	O
,	O
with	O
three	O
teams	O
from	O
the	O
original	O
16-team	O
NFL	ORGANIZATION
(	O
the	O
Pittsburgh	ORGANIZATION
Steelers	ORGANIZATION
,	O
Cleveland	ORGANIZATION
Browns	ORGANIZATION
,	O
and	O
Baltimore	ORGANIZATION
Colts	ORGANIZATION
)	O
joining	O
them	O
.	O
The	O
remaining	O
13	O
original	O
NFL	ORGANIZATION
teams	O
became	O
the	O
inaugural	O
members	O
of	O
the	O
National	ORGANIZATION
Football	ORGANIZATION
Conference	ORGANIZATION
(	O
NFC	ORGANIZATION
)	O
.	O
The	O
AFL	ORGANIZATION
logo	O
was	O
incorporated	O
into	O
the	O
newly	O
minted	O
AFC	ORGANIZATION
logo	O
,	O
although	O
the	O
color	O
of	O
the	O
"	O
A	O
"	O
was	O
changed	O
from	O
blue	O
and	O
white	O
,	O
to	O
red	O
.	O
The	O
NFL	ORGANIZATION
retained	O
its	O
old	O
name	O
and	O
logo	O
and	O
claims	O
the	O
rights	O
to	O
all	O
AFL	ORGANIZATION
products	O
including	O
the	O
eagle	O
logo	O
.	O
One	O
franchise	O
that	O
did	O
not	O
share	O
in	O
the	O
success	O
of	O
the	O
league	O
was	O
the	O
Chicago	ORGANIZATION
Cardinals	ORGANIZATION
,	O
who	O
were	O
overshadowed	O
by	O
the	O
more	O
popular	O
Chicago	ORGANIZATION
Bears	ORGANIZATION
.	O
The	O
team	O
was	O
reportedly	O
for	O
sale	O
(	O
with	O
the	O
intent	O
of	O
relocation	O
)	O
,	O
and	O
one	O
of	O
the	O
men	O
who	O
approached	O
the	O
Cardinals	ORGANIZATION
was	O
Lamar	PERSON
Hunt	PERSON
,	O
son	O
and	O
heir	O
of	O
Texas	LOCATION
millionaire	O
oilman	O
H.	PERSON
L.	PERSON
Hunt	PERSON
.	O
Hunt	PERSON
offered	O
to	O
buy	O
the	O
Cardinals	ORGANIZATION
and	O
move	O
them	O
to	O
Dallas	LOCATION
,	O
Texas	LOCATION
,	O
where	O
he	O
had	O
grown	O
up	O
.	O
While	O
Hunt	PERSON
negotiated	O
with	O
Cardinals	ORGANIZATION
ownership	O
,	O
similar	O
offers	O
were	O
made	O
by	O
Bud	PERSON
Adams	PERSON
,	O
Bob	PERSON
Howsam	PERSON
,	O
and	O
Max	PERSON
Winter	PERSON
.	O
Bell	PERSON
,	O
wary	O
of	O
expanding	O
the	O
12-team	O
league	O
and	O
risking	O
its	O
newfound	O
success	O
,	O
rejected	O
the	O
offer	O
.	O
On	O
his	O
return	O
flight	O
to	O
Dallas	LOCATION
,	O
Hunt	PERSON
conceived	O
the	O
idea	O
of	O
an	O
entirely	O
new	O
league	O
and	O
decided	O
to	O
contact	O
the	O
others	O
who	O
had	O
shown	O
interest	O
in	O
purchasing	O
the	O
Cardinals	ORGANIZATION
.	O
Hunt	PERSON
's	O
first	O
meeting	O
with	O
Adams	PERSON
was	O
held	O
in	O
March	O
1959	O
.	O
Hunt	PERSON
,	O
who	O
felt	O
a	O
regional	O
rivalry	O
would	O
be	O
critical	O
for	O
the	O
success	O
of	O
the	O
new	O
league	O
,	O
convinced	O
Adams	PERSON
to	O
join	O
and	O
found	O
his	O
team	O
in	O
Houston	LOCATION
.	O
Hunt	PERSON
also	O
sought	O
franchises	O
in	O
Los	LOCATION
Angeles	LOCATION
,	O
California	LOCATION
and	O
New	LOCATION
York	LOCATION
City	LOCATION
.	O
During	O
the	O
summer	O
of	O
1959	O
he	O
sought	O
the	O
blessings	O
of	O
the	O
NFL	ORGANIZATION
for	O
his	O
nascent	O
league	O
,	O
as	O
he	O
did	O
not	O
seek	O
a	O
potentially	O
costly	O
rivalry	O
.	O
Within	O
weeks	O
of	O
the	O
July	O
1959	O
announcement	O
of	O
the	O
league	O
's	O
formation	O
,	O
Hunt	PERSON
received	O
commitments	O
from	O
Barron	PERSON
Hilton	PERSON
and	O
Harry	PERSON
Wismer	PERSON
to	O
bring	O
teams	O
to	O
Los	LOCATION
Angeles	LOCATION
and	O
New	LOCATION
York	LOCATION
,	O
respectively	O
.	O
On	O
August	O
22	O
the	O
league	O
officially	O
was	O
named	O
the	O
American	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
.	O
The	O
NFL	ORGANIZATION
's	O
initial	O
reaction	O
was	O
not	O
as	O
openly	O
hostile	O
as	O
it	O
had	O
been	O
with	O
the	O
earlier	O
All-America	ORGANIZATION
Football	ORGANIZATION
Conference	ORGANIZATION
(	O
Bell	PERSON
had	O
even	O
given	O
his	O
public	O
approval	O
)	O
,	O
however	O
individual	O
NFL	ORGANIZATION
owners	O
soon	O
began	O
a	O
campaign	O
to	O
undermine	O
the	O
new	O
league	O
.	O
AFL	ORGANIZATION
owners	O
were	O
approached	O
with	O
promises	O
of	O
new	O
NFL	ORGANIZATION
franchises	O
or	O
ownership	O
stakes	O
in	O
existing	O
ones	O
.	O
The	O
older	O
league	O
also	O
announced	O
on	O
August	O
29	O
that	O
it	O
had	O
conveniently	O
reversed	O
its	O
position	O
against	O
expansion	O
,	O
and	O
planned	O
to	O
bring	O
NFL	ORGANIZATION
expansion	O
teams	O
to	O
Houston	LOCATION
and	O
Dallas	LOCATION
,	O
to	O
start	O
play	O
in	O
1961	O
.	O
Two	O
more	O
cities	O
were	O
awarded	O
AFL	ORGANIZATION
franchises	O
later	O
in	O
the	O
year	O
.	O
Ralph	PERSON
Wilson	PERSON
,	O
who	O
owned	O
a	O
minority	O
interest	O
in	O
the	O
NFL	ORGANIZATION
's	O
Detroit	ORGANIZATION
Lions	ORGANIZATION
,	O
announced	O
he	O
was	O
placing	O
a	O
team	O
in	O
Buffalo	LOCATION
,	O
New	LOCATION
York	LOCATION
after	O
he	O
had	O
been	O
rejected	O
by	O
Miami	LOCATION
.	O
During	O
a	O
league	O
meeting	O
on	O
November	O
22	O
,	O
a	O
10-man	O
ownership	O
group	O
from	O
Boston	LOCATION
,	O
Massachusetts	LOCATION
(	O
led	O
by	O
Billy	PERSON
Sullivan	PERSON
)	O
was	O
awarded	O
the	O
AFL	ORGANIZATION
's	O
eighth	O
team	O
.	O
On	O
November	O
30	O
,	O
1959	O
,	O
Joe	PERSON
Foss	PERSON
,	O
a	O
World	MISC
War	MISC
II	MISC
Marine	ORGANIZATION
fighter	O
ace	O
and	O
former	O
governor	O
of	O
South	LOCATION
Dakota	LOCATION
,	O
was	O
named	O
the	O
AFL	ORGANIZATION
's	O
first	O
commissioner	O
.	O
Foss	PERSON
commissioned	O
a	O
friend	O
of	O
Harry	PERSON
Wismer	PERSON
's	O
to	O
develop	O
the	O
AFL	ORGANIZATION
's	O
eagle-on-football	O
logo	O
.	O
The	O
AFL	ORGANIZATION
's	O
first	O
draft	O
took	O
place	O
the	O
same	O
day	O
Boston	LOCATION
was	O
awarded	O
its	O
franchise	O
,	O
and	O
lasted	O
33	O
rounds	O
.	O
In	O
November	O
1959	O
,	O
Minneapolis	LOCATION
owner	O
Max	PERSON
Winter	PERSON
announced	O
his	O
intent	O
to	O
leave	O
the	O
AFL	ORGANIZATION
to	O
accept	O
a	O
franchise	O
offer	O
from	O
the	O
NFL	ORGANIZATION
.	O
In	O
1961	O
,	O
his	O
team	O
began	O
play	O
in	O
the	O
NFL	ORGANIZATION
as	O
the	O
Minnesota	ORGANIZATION
Vikings	ORGANIZATION
.	O
The	O
AFL	ORGANIZATION
's	O
first	O
major	O
success	O
came	O
when	O
the	O
Houston	ORGANIZATION
Oilers	ORGANIZATION
signed	O
Billy	PERSON
Cannon	PERSON
,	O
the	O
All-American	MISC
and	O
1959	O
Heisman	ORGANIZATION
Trophy	ORGANIZATION
winner	O
from	O
LSU	ORGANIZATION
.	O
On	O
June	O
9	O
,	O
1960	O
,	O
the	O
league	O
signed	O
a	O
five-year	O
television	O
contract	O
with	O
ABC	ORGANIZATION
,	O
which	O
brought	O
in	O
revenues	O
of	O
approximately	O
$	O
2	O
,125,000	O
per	O
year	O
for	O
the	O
entire	O
league	O
.	O
On	O
June	O
17	O
,	O
the	O
AFL	ORGANIZATION
filed	O
an	O
antitrust	O
lawsuit	O
against	O
the	O
NFL	ORGANIZATION
,	O
which	O
was	O
dismissed	O
in	O
1962	O
after	O
a	O
two-month	O
trial	O
.	O
Attendance	O
for	O
the	O
1960	O
season	O
was	O
respectable	O
for	O
a	O
new	O
league	O
,	O
but	O
not	O
nearly	O
that	O
of	O
the	O
NFL	ORGANIZATION
.	O
Whereas	O
the	O
more	O
popular	O
NFL	ORGANIZATION
teams	O
in	O
1960	O
regularly	O
saw	O
attendance	O
figures	O
in	O
excess	O
of	O
50,000	O
per	O
game	O
,	O
AFL	ORGANIZATION
attendance	O
generally	O
hovered	O
between	O
10,000-20,000	O
per	O
game	O
.	O
In	O
an	O
early	O
sign	O
of	O
stability	O
,	O
however	O
,	O
the	O
AFL	ORGANIZATION
did	O
not	O
lose	O
any	O
teams	O
after	O
its	O
first	O
year	O
of	O
operation	O
.	O
In	O
fact	O
,	O
the	O
only	O
major	O
change	O
was	O
the	O
relocation	O
of	O
the	O
Chargers	ORGANIZATION
from	O
L.A.	LOCATION
to	O
San	LOCATION
Diego	LOCATION
.	O
On	O
August	O
8	O
,	O
1961	O
,	O
the	O
AFL	ORGANIZATION
,	O
desperate	O
to	O
prove	O
its	O
legitimacy	O
,	O
challenged	O
the	O
Canadian	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
to	O
an	O
exhibition	O
game	O
.	O
It	O
was	O
decided	O
that	O
the	O
game	O
would	O
be	O
between	O
Hamilton	ORGANIZATION
Tiger-Cats	ORGANIZATION
and	O
the	O
Buffalo	ORGANIZATION
Bills	ORGANIZATION
.	O
After	O
all	O
,	O
the	O
NFL	ORGANIZATION
had	O
sent	O
several	O
teams	O
against	O
teams	O
from	O
the	O
CFL	ORGANIZATION
and	O
had	O
won	O
every	O
game	O
so	O
it	O
was	O
thought	O
that	O
victory	O
against	O
the	O
CFL	ORGANIZATION
would	O
be	O
easy	O
.	O
No	O
team	O
from	O
a	O
U.S.-based	LOCATION
football	O
league	O
has	O
since	O
competed	O
with	O
a	O
team	O
from	O
a	O
Canada-based	LOCATION
league	O
(	O
U.S./Canadian	O
professional	O
team	O
matchups	O
within	O
one	O
league	O
,	O
however	O
,	O
have	O
happened	O
,	O
such	O
as	O
the	O
Toronto	ORGANIZATION
Rifles	ORGANIZATION
of	O
the	O
1960s	O
,	O
the	O
Montreal	ORGANIZATION
Machine	ORGANIZATION
of	O
the	O
early	O
1990s	O
and	O
the	O
CFL	ORGANIZATION
USA	ORGANIZATION
of	O
the	O
mid-1990s	O
)	O
.	O
The	O
Oakland	ORGANIZATION
Raiders	ORGANIZATION
and	O
New	ORGANIZATION
York	ORGANIZATION
Titans	ORGANIZATION
struggled	O
on	O
and	O
off	O
the	O
field	O
during	O
their	O
first	O
few	O
seasons	O
in	O
the	O
league	O
.	O
Oakland	LOCATION
's	O
eight-man	O
ownership	O
group	O
was	O
reduced	O
to	O
just	O
three	O
in	O
1961	O
,	O
after	O
heavy	O
financial	O
losses	O
their	O
first	O
season	O
.	O
Attendance	O
for	O
home	O
games	O
was	O
poor	O
,	O
partly	O
due	O
to	O
the	O
team	O
playing	O
in	O
the	O
San	LOCATION
Francisco	LOCATION
Bay	LOCATION
Area	LOCATION
--	O
which	O
already	O
had	O
an	O
established	O
NFL	ORGANIZATION
team	O
(	O
the	O
San	ORGANIZATION
Francisco	ORGANIZATION
49ers	ORGANIZATION
)	O
--	O
but	O
the	O
product	O
on	O
the	O
field	O
was	O
also	O
to	O
blame	O
.	O
After	O
winning	O
six	O
games	O
their	O
debut	O
season	O
,	O
the	O
Raiders	ORGANIZATION
won	O
a	O
total	O
of	O
three	O
times	O
in	O
the	O
1961	O
and	O
1962	O
seasons	O
.	O
Oakland	LOCATION
took	O
part	O
in	O
a	O
1961	O
supplemental	O
draft	O
meant	O
to	O
boost	O
the	O
weaker	O
teams	O
in	O
the	O
league	O
,	O
but	O
it	O
did	O
little	O
good	O
.	O
The	O
Titans	ORGANIZATION
fared	O
a	O
little	O
better	O
on	O
the	O
field	O
but	O
had	O
their	O
own	O
financial	O
troubles	O
.	O
Attendance	O
was	O
so	O
low	O
for	O
home	O
games	O
that	O
team	O
owner	O
Harry	PERSON
Wismer	PERSON
had	O
fans	O
move	O
to	O
seats	O
closer	O
to	O
the	O
field	O
to	O
give	O
the	O
illusion	O
of	O
a	O
fuller	O
stadium	O
on	O
television	O
.	O
Eventually	O
Wismer	PERSON
could	O
no	O
longer	O
afford	O
to	O
meet	O
his	O
payroll	O
,	O
and	O
on	O
November	O
8	O
,	O
1962	O
the	O
AFL	ORGANIZATION
took	O
over	O
operations	O
of	O
the	O
team	O
.	O
The	O
Titans	ORGANIZATION
were	O
sold	O
to	O
a	O
five-person	O
ownership	O
group	O
headed	O
by	O
Sonny	PERSON
Werblin	PERSON
on	O
March	O
28	O
,	O
1963	O
,	O
and	O
in	O
April	O
the	O
new	O
owners	O
changed	O
the	O
team	O
's	O
name	O
to	O
the	O
New	ORGANIZATION
York	ORGANIZATION
Jets	ORGANIZATION
.	O
The	O
Raiders	ORGANIZATION
and	O
Titans	ORGANIZATION
both	O
finished	O
last	O
in	O
their	O
respective	O
divisions	O
in	O
the	O
1962	O
season	O
.	O
In	O
1963	O
,	O
the	O
Texans	ORGANIZATION
became	O
the	O
second	ORGANIZATION
AFL	ORGANIZATION
team	O
to	O
relocate	O
.	O
Lamar	PERSON
Hunt	PERSON
felt	O
that	O
despite	O
winning	O
the	O
league	O
championship	O
in	O
1962	O
,	O
the	O
Texans	ORGANIZATION
could	O
not	O
succeed	O
financially	O
competing	O
in	O
the	O
same	O
market	O
as	O
the	O
Dallas	ORGANIZATION
Cowboys	ORGANIZATION
,	O
who	O
had	O
entered	O
the	O
NFL	ORGANIZATION
as	O
an	O
expansion	O
team	O
in	O
1960	O
.	O
After	O
meetings	O
with	O
New	LOCATION
Orleans	LOCATION
,	O
Atlanta	LOCATION
and	O
Miami	LOCATION
,	O
Hunt	PERSON
announced	O
on	O
May	O
22	O
that	O
the	O
Texans	ORGANIZATION
'	O
new	O
home	O
would	O
be	O
Kansas	LOCATION
City	LOCATION
,	O
Mo.	LOCATION
.	O
Kansas	LOCATION
City	LOCATION
mayor	O
Harold	PERSON
Roe	PERSON
Bartle	PERSON
(	O
nicknamed	O
"	O
Chief	O
"	O
)	O
was	O
instrumental	O
in	O
his	O
city	O
's	O
success	O
in	O
attracting	O
the	O
team	O
.	O
Partly	O
to	O
honor	O
Bartle	PERSON
,	O
the	O
franchise	O
officially	O
became	O
the	O
Kansas	ORGANIZATION
City	ORGANIZATION
Chiefs	ORGANIZATION
on	O
May	O
26	O
.	O
A	O
series	O
of	O
events	O
throughout	O
the	O
next	O
few	O
years	O
demonstrated	O
the	O
AFL	ORGANIZATION
's	O
ability	O
to	O
achieve	O
a	O
greater	O
level	O
of	O
equality	O
with	O
the	O
NFL	ORGANIZATION
.	O
A	O
new	O
single-game	O
attendance	O
record	O
was	O
set	O
on	O
November	O
8	O
,	O
1964	O
when	O
61,929	O
fans	O
packed	O
Shea	LOCATION
Stadium	LOCATION
to	O
watch	O
the	O
New	ORGANIZATION
York	ORGANIZATION
Jets	ORGANIZATION
and	O
Buffalo	ORGANIZATION
Bills	ORGANIZATION
.	O
The	O
bidding	O
war	O
for	O
players	O
between	O
the	O
AFL	ORGANIZATION
and	O
NFL	ORGANIZATION
escalated	O
in	O
1965	O
.	O
In	O
what	O
was	O
viewed	O
as	O
a	O
key	O
victory	O
for	O
the	O
AFL	ORGANIZATION
,	O
Namath	PERSON
signed	O
a	O
$	O
427,000	O
contract	O
with	O
the	O
Jets	ORGANIZATION
on	O
January	O
2	O
(	O
the	O
deal	O
also	O
came	O
with	O
a	O
new	O
car	O
)	O
.	O
In	O
March	O
1965	O
,	O
Minneapolis	LOCATION
lawyer	O
Joe	PERSON
Robbie	PERSON
met	O
with	O
Commissioner	O
Foss	PERSON
to	O
inquire	O
about	O
an	O
expansion	O
franchise	O
.	O
The	O
Miami	ORGANIZATION
Dolphins	ORGANIZATION
joined	O
the	O
league	O
for	O
a	O
fee	O
of	O
$	O
7.5	O
million	O
,	O
and	O
started	O
play	O
in	O
the	O
AFL	ORGANIZATION
's	O
Eastern	ORGANIZATION
Division	ORGANIZATION
in	O
1966	O
.	O
In	O
1966	O
,	O
the	O
rivalry	O
between	O
the	O
AFL	ORGANIZATION
and	O
NFL	ORGANIZATION
reached	O
an	O
all-time	O
peak	O
.	O
On	O
April	O
7	O
,	O
Joe	PERSON
Foss	PERSON
resigned	O
as	O
AFL	ORGANIZATION
Commissioner	O
.	O
His	O
successor	O
was	O
Oakland	ORGANIZATION
Raiders	ORGANIZATION
head	O
coach	O
and	O
general	O
manager	O
Al	PERSON
Davis	PERSON
,	O
who	O
had	O
been	O
instrumental	O
in	O
turning	O
around	O
the	O
fortunes	O
of	O
that	O
franchise	O
.	O
No	O
longer	O
content	O
with	O
trying	O
to	O
outbid	O
the	O
NFL	ORGANIZATION
for	O
college	O
talent	O
,	O
the	O
AFL	ORGANIZATION
under	O
Davis	PERSON
actively	O
started	O
to	O
recruit	O
players	O
already	O
on	O
NFL	ORGANIZATION
squads	O
.	O
Davis	PERSON
's	O
strategy	O
focused	O
on	O
quarterbacks	O
in	O
particular	O
,	O
and	O
in	O
two	O
months	O
he	O
convinced	O
seven	O
NFL	ORGANIZATION
quarterbacks	O
to	O
sign	O
with	O
the	O
AFL	ORGANIZATION
.	O
But	O
while	O
Davis	PERSON
intended	O
to	O
help	O
the	O
AFL	ORGANIZATION
win	O
the	O
bidding	O
war	O
,	O
some	O
AFL	ORGANIZATION
and	O
NFL	ORGANIZATION
owners	O
saw	O
the	O
escalation	O
as	O
detrimental	O
to	O
both	O
leagues	O
.	O
They	O
held	O
a	O
series	O
of	O
secret	O
meetings	O
in	O
Dallas	LOCATION
to	O
discuss	O
their	O
concerns	O
over	O
rapidly	O
increasing	O
player	O
salaries	O
,	O
as	O
well	O
as	O
the	O
practice	O
of	O
player	O
poaching	O
.	O
The	O
two	O
leagues	O
would	O
be	O
fully	O
merged	O
by	O
1970	O
,	O
and	O
NFL	ORGANIZATION
commissioner	O
Pete	PERSON
Rozelle	PERSON
would	O
remain	O
as	O
commissioner	O
of	O
the	O
merged	O
league	O
.	O
The	O
AFL	ORGANIZATION
also	O
agreed	O
to	O
pay	O
indemnities	O
of	O
$	O
18	O
million	O
to	O
the	O
NFL	ORGANIZATION
over	O
20	O
years	O
.	O
The	O
loss	O
reinforced	O
for	O
many	O
the	O
notion	O
that	O
the	O
AFL	ORGANIZATION
was	O
indeed	O
the	O
inferior	O
league	O
.	O
Packers	ORGANIZATION
head	O
coach	O
Vince	PERSON
Lombardi	PERSON
stated	O
after	O
the	O
game	O
,	O
"	O
I	O
do	O
not	O
think	O
they	O
are	O
as	O
good	O
as	O
the	O
top	O
teams	O
in	O
the	O
National	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
.	O
"	O
The	O
more	O
experienced	O
Packers	ORGANIZATION
capitalized	O
on	O
a	O
number	O
of	O
Raiders	ORGANIZATION
miscues	O
,	O
and	O
never	O
trailed	O
.	O
Green	ORGANIZATION
Bay	ORGANIZATION
defensive	O
tackle	O
Henry	PERSON
Jordan	PERSON
offered	O
a	O
compliment	O
to	O
Oakland	LOCATION
and	O
the	O
AFL	ORGANIZATION
,	O
when	O
he	O
said	O
,	O
"	O
...	O
the	O
AFL	ORGANIZATION
is	O
becoming	O
much	O
more	O
sophisticated	O
on	O
offense	O
.	O
The	O
AFL	ORGANIZATION
added	O
its	O
tenth	O
and	O
final	O
team	O
on	O
May	O
24	O
,	O
1967	O
,	O
when	O
they	O
awarded	O
the	O
league	O
's	O
second	O
expansion	O
franchise	O
to	O
an	O
ownership	O
group	O
from	O
Cincinnati	LOCATION
,	O
Ohio	LOCATION
headed	O
by	O
NFL	ORGANIZATION
legend	O
Paul	PERSON
Brown	PERSON
.	O
While	O
many	O
AFL	ORGANIZATION
players	O
and	O
observers	O
felt	O
their	O
league	O
was	O
the	O
equal	O
of	O
the	O
NFL	ORGANIZATION
,	O
the	O
first	O
two	O
Super	ORGANIZATION
Bowls	ORGANIZATION
did	O
little	O
to	O
prove	O
it	O
.	O
The	O
Colts	ORGANIZATION
,	O
who	O
entered	O
the	O
contest	O
favored	O
by	O
as	O
many	O
as	O
18	O
points	O
,	O
had	O
completed	O
the	O
1968	MISC
NFL	MISC
season	MISC
with	O
a	O
13-1	O
record	O
,	O
and	O
won	O
the	O
NFL	ORGANIZATION
title	O
with	O
a	O
convincing	O
34-0	O
dismantling	O
of	O
the	O
Cleveland	ORGANIZATION
Browns	ORGANIZATION
.	O
Led	O
by	O
their	O
stalwart	O
defense	O
--	O
which	O
allowed	O
a	O
record-low	O
144	O
points	O
--	O
the	O
1968	O
Colts	ORGANIZATION
were	O
considered	O
one	O
of	O
the	O
best-ever	O
NFL	ORGANIZATION
teams	O
.	O
By	O
contrast	O
,	O
the	O
Jets	ORGANIZATION
allowed	O
280	O
points	O
,	O
the	O
highest	O
total	O
for	O
any	O
division	O
winner	O
in	O
the	O
two	O
leagues	O
.	O
Jets	ORGANIZATION
quarterback	O
Joe	PERSON
Namath	PERSON
recalled	O
that	O
in	O
the	O
days	O
leading	O
up	O
to	O
the	O
game	O
,	O
he	O
grew	O
increasingly	O
angry	O
when	O
told	O
N.Y.	LOCATION
had	O
no	O
chance	O
to	O
beat	O
Baltimore	LOCATION
.	O
Namath	PERSON
and	O
the	O
Jets	ORGANIZATION
made	O
good	O
on	O
his	O
guarantee	O
as	O
they	O
held	O
the	O
Colts	ORGANIZATION
scoreless	O
until	O
late	O
in	O
the	O
fourth	O
quarter	O
.	O
The	O
Jets	ORGANIZATION
won	O
,	O
16-7	O
,	O
in	O
what	O
is	O
considered	O
one	O
of	O
the	O
greatest	O
upsets	O
in	O
American	MISC
sports	O
history	O
.	O
With	O
the	O
win	O
,	O
the	O
AFL	ORGANIZATION
finally	O
achieved	O
parity	O
with	O
the	O
NFL	ORGANIZATION
and	O
legitimized	O
the	O
merger	O
of	O
the	O
two	O
leagues	O
.	O
Prior	O
to	O
the	O
start	O
of	O
the	O
1970	MISC
NFL	MISC
season	MISC
,	O
the	O
merged	O
league	O
was	O
split	O
into	O
two	O
conferences	O
of	O
three	O
divisions	O
each	O
.	O
All	O
ten	O
AFL	ORGANIZATION
teams	O
made	O
up	O
the	O
bulk	O
of	O
the	O
new	O
American	ORGANIZATION
Football	ORGANIZATION
Conference	ORGANIZATION
.	O
To	O
avoid	O
having	O
16	O
teams	O
in	O
one	O
conference	O
and	O
10	O
in	O
the	O
other	O
,	O
the	O
leagues	O
voted	O
to	O
move	O
three	O
NFL	ORGANIZATION
teams	O
to	O
the	O
AFC	ORGANIZATION
.	O
All	O
the	O
other	O
NFL	ORGANIZATION
squads	O
became	O
part	O
of	O
the	O
National	ORGANIZATION
Football	ORGANIZATION
Conference	ORGANIZATION
.	O
The	O
American	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
stands	O
as	O
the	O
only	O
Professional	ORGANIZATION
Football	ORGANIZATION
league	O
to	O
successfully	O
compete	O
against	O
the	O
NFL	ORGANIZATION
.	O
When	O
the	O
two	O
leagues	O
merged	O
in	O
1970	O
,	O
all	O
ten	O
AFL	ORGANIZATION
franchises	O
and	O
their	O
statistics	O
became	O
part	O
of	O
the	O
new	O
NFL	ORGANIZATION
.	O
Every	O
other	O
professional	O
league	O
that	O
competed	O
against	O
the	O
NFL	ORGANIZATION
:	O
the	O
three	O
previous	O
leagues	O
named	O
"	O
American	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
"	O
;	O
the	O
AAFC	ORGANIZATION
;	O
the	O
USFL	ORGANIZATION
;	O
the	O
WFL	ORGANIZATION
;	O
and	O
the	O
XFL	MISC
folded	O
completely	O
.	O
The	O
NFL	ORGANIZATION
adopted	O
many	O
ideas	O
introduced	O
by	O
the	O
AFL	ORGANIZATION
,	O
including	O
names	O
on	O
player	O
jerseys	O
and	O
revenue	O
sharing	O
of	O
gate	O
and	O
television	O
receipts	O
.	O
The	O
AFL	ORGANIZATION
played	O
a	O
14-game	O
schedule	O
for	O
its	O
entire	O
existence	O
,	O
starting	O
in	O
1960	O
.	O
The	O
NFL	ORGANIZATION
,	O
which	O
had	O
played	O
a	O
12-game	O
schedule	O
since	O
1947	O
,	O
changed	O
to	O
a	O
14-game	O
schedule	O
in	O
1961	O
,	O
a	O
year	O
after	O
the	O
American	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
instituted	O
it	O
.	O
The	O
AFL	ORGANIZATION
also	O
introduced	O
the	O
two-point	O
conversion	O
to	O
professional	O
football	O
thirty-four	O
years	O
before	O
the	O
NFL	ORGANIZATION
instituted	O
it	O
in	O
1994	O
(	O
college	O
football	O
had	O
adopted	O
the	O
two	O
point	O
conversion	O
in	O
the	O
late	O
1950s	O
)	O
.	O
All	O
of	O
these	O
innovations	O
pioneered	O
by	O
the	O
AFL	ORGANIZATION
,	O
including	O
its	O
more	O
exciting	O
style	O
of	O
play	O
and	O
colorful	O
uniforms	O
,	O
have	O
essentially	O
made	O
today	O
's	O
Professional	ORGANIZATION
Football	ORGANIZATION
more	O
like	O
the	O
AFL	ORGANIZATION
than	O
like	O
the	O
old-line	O
NFL	ORGANIZATION
.	O
Hunt	PERSON
's	O
vision	O
not	O
only	O
brought	O
a	O
new	O
professional	O
football	O
league	O
to	O
Calif.	LOCATION
and	O
New	LOCATION
York	LOCATION
,	O
but	O
introduced	O
the	O
sport	O
to	O
Colo.	LOCATION
,	O
restored	O
it	O
to	O
Texas	LOCATION
and	O
later	O
to	O
fast-growing	O
Florida	LOCATION
,	O
as	O
well	O
as	O
bringing	O
it	O
to	O
New	LOCATION
England	LOCATION
for	O
the	O
first	O
time	O
in	O
12	O
years	O
.	O
Buffalo	O
,	O
having	O
lost	O
its	O
original	ORGANIZATION
NFL	ORGANIZATION
franchise	ORGANIZATION
in	O
1929	O
and	O
turned	O
down	O
by	O
the	O
NFL	ORGANIZATION
at	O
least	O
twice	O
(	O
1940	O
and	O
1950	O
)	O
for	O
a	O
replacement	O
,	O
returned	O
to	O
the	O
NFL	ORGANIZATION
with	O
the	O
merger	O
.	O
In	O
addition	O
,	O
the	O
AFL	ORGANIZATION
also	O
adopted	O
the	O
first-ever	O
cooperative	O
television	O
plan	O
for	O
professional	O
football	O
,	O
in	O
which	O
the	O
league	O
office	O
negotiated	O
an	O
ABC-TV	ORGANIZATION
contract	O
,	O
the	O
proceeds	O
of	O
which	O
were	O
divided	O
equally	O
among	O
member	O
clubs	O
.	O
Further	O
,	O
the	O
success	O
of	O
the	O
expansion	O
process	O
engendered	O
by	O
the	O
AFL-NFL	ORGANIZATION
conflict	O
(	O
as	O
well	O
as	O
later	O
expansion	O
pressures	O
created	O
by	O
the	O
WFL	ORGANIZATION
,	O
USFL	ORGANIZATION
,	O
and	O
even	O
the	O
CFL	ORGANIZATION
)	O
certainly	O
led	O
to	O
more	O
expansion	O
,	O
ultimately	O
to	O
the	O
thirty-two	O
team	O
league	O
as	O
it	O
exists	O
in	O
2009	O
.	O
Thus	O
,	O
if	O
not	O
for	O
the	O
AFL	ORGANIZATION
,	O
at	O
least	O
five	O
of	O
today	O
's	O
NFL	ORGANIZATION
teams	O
would	O
likely	O
never	O
have	O
existed	O
.	O
The	O
AFL	ORGANIZATION
also	O
spawned	O
coaches	O
whose	O
style	O
and	O
techniques	O
profoundly	O
affect	O
the	O
play	O
of	O
professional	O
football	O
until	O
this	O
day	O
.	O
In	O
addition	O
to	O
AFL	ORGANIZATION
greats	O
like	O
Hank	PERSON
Stram	PERSON
,	O
Lou	PERSON
Saban	PERSON
,	O
Sid	PERSON
Gillman	PERSON
and	O
Al	PERSON
Davis	PERSON
were	O
eventual	O
hall	O
of	O
fame	O
coaches	O
such	O
as	O
Bill	PERSON
Walsh	PERSON
,	O
a	O
protegé	O
of	O
Davis	PERSON
with	O
the	O
AFL	ORGANIZATION
Oakland	ORGANIZATION
Raiders	ORGANIZATION
;	O
and	O
Chuck	PERSON
Noll	PERSON
,	O
who	O
worked	O
for	O
Gillman	PERSON
and	O
the	O
AFL	ORGANIZATION
LA/San	ORGANIZATION
Diego	ORGANIZATION
Chargers	ORGANIZATION
from	O
1960	O
through	O
1965	O
.	O
Others	O
include	O
Buddy	PERSON
Ryan	PERSON
(	O
AFL	ORGANIZATION
's	O
New	ORGANIZATION
York	ORGANIZATION
Jets	ORGANIZATION
)	O
,	O
Chuck	PERSON
Knox	PERSON
(	O
Jets	ORGANIZATION
)	O
,	O
Walt	PERSON
Michaels	PERSON
(	O
Jets	ORGANIZATION
)	O
,	O
and	O
John	PERSON
Madden	PERSON
(	O
AFL	ORGANIZATION
's	O
Oakland	ORGANIZATION
Raiders	ORGANIZATION
)	O
.	O
Additionally	O
,	O
many	O
prominent	O
coaches	O
began	O
their	O
pro	O
football	O
careers	O
as	O
players	O
in	O
the	O
AFL	ORGANIZATION
,	O
including	O
Sam	PERSON
Wyche	PERSON
(	O
Cincinnati	ORGANIZATION
Bengals	ORGANIZATION
)	O
,	O
Marty	PERSON
Schottenheimer	PERSON
(	O
Buffalo	ORGANIZATION
Bills	ORGANIZATION
)	O
,	O
Wayne	PERSON
Fontes	PERSON
(	O
Jets	ORGANIZATION
)	O
,	O
and	O
two-time	O
Super	ORGANIZATION
Bowl	ORGANIZATION
winner	O
Tom	PERSON
Flores	PERSON
(	O
Oakland	ORGANIZATION
Raiders	ORGANIZATION
)	O
.	O
Flores	PERSON
also	O
has	O
a	O
Super	ORGANIZATION
Bowl	ORGANIZATION
ring	O
as	O
a	O
player	O
(	O
1969	O
Kansas	ORGANIZATION
City	ORGANIZATION
Chiefs	ORGANIZATION
)	O
.	O
Perhaps	O
the	O
greatest	O
social	O
legacy	O
of	O
the	O
AFL	ORGANIZATION
was	O
the	O
domino	O
effect	O
of	O
its	O
policy	O
of	O
being	O
more	O
liberal	O
than	O
the	O
entrenched	O
NFL	ORGANIZATION
in	O
offering	O
opportunity	O
for	O
black	O
players	O
.	O
While	O
the	O
NFL	ORGANIZATION
was	O
still	O
emerging	O
from	O
thirty	O
years	O
of	O
segregation	O
influenced	O
by	O
a	O
known	O
bigot	O
,	O
Washington	ORGANIZATION
Redskins	ORGANIZATION
'	O
owner	O
George	PERSON
Preston	PERSON
Marshall	PERSON
,	O
the	O
AFL	ORGANIZATION
actively	O
recruited	O
from	O
small	O
and	O
predominantly	O
black	O
colleges	O
.	O
The	O
AFL	ORGANIZATION
's	O
color-blindness	O
led	O
not	O
only	O
to	O
the	O
explosion	O
of	O
black	O
talent	O
on	O
the	O
field	O
,	O
but	O
to	O
the	O
eventual	O
entry	O
of	O
blacks	O
into	O
scouting	O
,	O
coordinator	O
,	O
and	O
ultimately	O
head	O
coaching	O
positions	O
,	O
long	O
after	O
the	O
league	O
ceased	O
to	O
exist	O
.	O
The	O
AFL	ORGANIZATION
's	O
free	O
agents	O
came	O
from	O
several	O
sources	O
.	O
Some	O
were	O
players	O
who	O
could	O
not	O
find	O
success	O
playing	O
in	O
the	O
NFL	ORGANIZATION
,	O
while	O
another	O
source	O
was	O
the	O
Canadian	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
.	O
The	O
American	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
took	O
advantage	O
of	O
the	O
burgeoning	O
popularity	O
of	O
football	O
by	O
locating	O
teams	O
in	O
major	O
cities	O
that	O
lacked	O
NFL	ORGANIZATION
franchises	O
,	O
and	O
by	O
using	O
the	O
growing	O
power	O
of	O
televised	O
football	O
games	O
(	O
bolstered	O
with	O
the	O
help	O
of	O
major	O
network	O
contracts	O
,	O
first	O
with	O
ABC	ORGANIZATION
and	O
later	O
with	O
NBC	ORGANIZATION
)	O
.	O
It	O
featured	O
many	O
outstanding	O
games	O
,	O
such	O
as	O
the	O
classic	O
1962	O
double-overtime	O
American	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
championship	O
game	O
between	O
the	O
Dallas	ORGANIZATION
Texans	ORGANIZATION
and	O
the	O
defending	O
champion	O
Houston	ORGANIZATION
Oilers	ORGANIZATION
.	O
The	O
AFL	ORGANIZATION
appealed	O
to	O
fans	O
by	O
offering	O
a	O
flashier	O
style	O
of	O
play	O
,	O
compared	O
to	O
the	O
more	O
conservative	O
game	O
of	O
the	O
NFL	ORGANIZATION
.	O
Long	O
passes	O
(	O
"	O
bombs	O
"	O
)	O
were	O
commonplace	O
in	O
AFL	ORGANIZATION
offenses	O
,	O
led	O
by	O
such	O
talented	O
quarterbacks	O
as	O
John	PERSON
Hadl	PERSON
,	O
Daryle	PERSON
Lamonica	PERSON
and	O
Len	PERSON
Dawson	PERSON
.	O
After	O
the	O
AFL-NFL	ORGANIZATION
merger	ORGANIZATION
agreement	O
in	O
1966	O
,	O
and	O
after	O
the	O
AFL	ORGANIZATION
's	O
Jets	ORGANIZATION
defeated	O
the	O
"	O
best	O
team	O
in	O
the	O
history	O
of	O
the	O
NFL	ORGANIZATION
"	O
,	O
the	O
Colts	ORGANIZATION
,	O
a	O
popular	O
misconception	O
fostered	O
by	O
the	O
NFL	ORGANIZATION
and	O
spread	O
by	O
media	O
reports	O
was	O
that	O
the	O
AFL	ORGANIZATION
defeated	O
the	O
NFL	ORGANIZATION
because	O
of	O
the	O
Common	MISC
Draft	MISC
instituted	O
from	O
1967	O
on	O
.	O
This	O
apparently	O
was	O
meant	O
to	O
confirm	O
that	O
until	O
the	O
AFL	ORGANIZATION
did	O
not	O
have	O
to	O
compete	O
with	O
the	O
NFL	ORGANIZATION
in	O
the	O
draft	O
,	O
it	O
could	O
not	O
achieve	O
parity	O
.	O
Their	O
stars	O
were	O
honed	O
in	O
the	O
AFL	ORGANIZATION
,	O
many	O
of	O
them	O
since	O
the	O
Titans	ORGANIZATION
days	O
.	O
As	O
noted	O
below	O
,	O
the	O
AFL	ORGANIZATION
got	O
its	O
share	O
of	O
stars	O
long	O
before	O
the	O
"	O
Common	MISC
Draft	MISC
"	O
.	O
Players	O
who	O
chose	O
the	O
AFL	ORGANIZATION
to	O
develop	O
their	O
talent	O
included	O
Lance	PERSON
Alworth	PERSON
and	O
Ron	PERSON
Mix	PERSON
of	O
the	O
Chargers	ORGANIZATION
,	O
who	O
had	O
also	O
been	O
drafted	O
by	O
the	O
NFL	ORGANIZATION
's	O
San	ORGANIZATION
Francisco	ORGANIZATION
49ers	ORGANIZATION
and	O
Baltimore	ORGANIZATION
Colts	ORGANIZATION
respectively	O
.	O
Both	O
eventually	O
were	O
elected	O
to	O
the	O
Pro	ORGANIZATION
Football	ORGANIZATION
Hall	ORGANIZATION
of	ORGANIZATION
Fame	ORGANIZATION
after	O
earning	O
recognition	O
during	O
their	O
careers	O
as	O
being	O
among	O
the	O
best	O
at	O
their	O
positions	O
.	O
Among	O
specific	O
teams	O
,	O
the	O
1964	O
Buffalo	ORGANIZATION
Bills	ORGANIZATION
stood	O
out	O
by	O
holding	O
their	O
opponents	O
to	O
a	O
pro	O
football	O
record	O
913	O
yards	O
rushing	O
on	O
300	O
attempts	O
,	O
while	O
also	O
recording	O
fifty	O
quarterback	O
sacks	O
in	O
a	O
fourteen-game	O
schedule	O
.	O
And	O
these	O
were	O
players	O
selected	O
by	O
the	O
AFL	ORGANIZATION
long	O
before	O
the	O
"	O
Common	MISC
Draft	MISC
"	O
.	O
Despite	O
having	O
a	O
national	O
television	O
contract	O
,	O
the	O
AFL	ORGANIZATION
often	O
found	O
itself	O
trying	O
to	O
gain	O
a	O
foothold	O
,	O
only	O
to	O
come	O
up	O
against	O
roadblocks	O
.	O
In	O
all	O
,	O
the	O
series	O
goes	O
far	O
to	O
show	O
that	O
the	O
AFL	ORGANIZATION
was	O
"	O
major	O
league	O
"	O
in	O
its	O
drafts	O
,	O
its	O
coaching	O
,	O
its	O
play	O
,	O
and	O
the	O
quality	O
of	O
its	O
participants	O
,	O
and	O
deserves	O
accolades	O
as	O
the	O
genesis	O
of	O
modern	O
Professional	ORGANIZATION
Football	ORGANIZATION
.	O
Many	O
coaches	O
who	O
eventually	O
shaped	O
modern	O
Professional	ORGANIZATION
Football	ORGANIZATION
had	O
roots	O
in	O
the	O
American	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
.	O
Bill	PERSON
Walsh	PERSON
is	O
often	O
cited	O
for	O
an	O
impressive	O
"	O
coaching	O
tree	O
"	O
of	O
assistants	O
who	O
went	O
on	O
to	O
become	O
head	O
coaches	O
.	O
Besides	O
Davis	PERSON
,	O
others	O
who	O
coached	O
for	O
Gillman	PERSON
included	O
Dick	PERSON
Vermeil	PERSON
,	O
Chuck	PERSON
Knox	PERSON
,	O
and	O
George	PERSON
Allen	PERSON
,	O
as	O
well	O
as	O
Chuck	PERSON
Noll	PERSON
,	O
the	O
only	O
head	O
coach	O
to	O
win	O
four	O
Super	ORGANIZATION
Bowls	ORGANIZATION
.	O
The	O
numbers	O
on	O
Gillman	PERSON
's	O
coaching	O
tree	O
indicate	O
the	O
Super	ORGANIZATION
Bowls	ORGANIZATION
won	O
by	O
his	O
coaching	O
"	O
descendants	O
"	O
,	O
a	O
total	O
of	O
twenty	O
.	O
As	O
the	O
influence	O
of	O
the	O
AFL	ORGANIZATION
continues	O
through	O
the	O
present	O
,	O
the	O
50th	O
anniversary	O
of	O
its	O
launch	O
was	O
celebrated	O
during	O
2009	O
.	O
The	O
season-long	O
celebration	O
began	O
in	O
August	O
with	O
the	O
2009	O
Pro	MISC
Football	MISC
Hall	MISC
of	MISC
Fame	MISC
Game	MISC
in	O
Canton	LOCATION
,	O
Ohio	LOCATION
between	O
two	O
AFC	ORGANIZATION
teams	O
(	O
as	O
opposed	O
to	O
the	O
AFC-vs-NFC	ORGANIZATION
format	O
the	O
game	O
first	O
adopted	O
in	O
1971	O
)	O
.	O
On-field	MISC
officials	MISC
also	O
wore	O
red-and-white-striped	O
AFL	ORGANIZATION
uniforms	O
during	O
these	O
games	O
.	O
In	O
the	O
fall	O
of	O
2009	O
,	O
the	O
Showtime	ORGANIZATION
pay-cable	O
network	O
premiered	O
Full	MISC
Color	MISC
Football	MISC
:	MISC
The	MISC
History	MISC
of	MISC
the	MISC
American	MISC
Football	MISC
League	MISC
,	O
a	O
5-part	O
documentary	O
series	O
produced	O
by	O
NFL	ORGANIZATION
Films	ORGANIZATION
that	O
features	O
vintage	O
game	O
film	O
and	O
interviews	O
as	O
well	O
as	O
more	O
recent	O
interviews	O
with	O
those	O
associated	O
with	O
the	O
AFL	ORGANIZATION
.	O
Today	O
,	O
two	O
of	O
the	O
NFL	ORGANIZATION
's	O
eight	O
divisions	O
are	O
composed	O
entirely	O
of	O
former	O
AFL	ORGANIZATION
teams	O
,	O
the	O
AFC	ORGANIZATION
West	ORGANIZATION
(	O
Broncos	ORGANIZATION
,	O
Chargers	ORGANIZATION
,	O
Chiefs	ORGANIZATION
,	O
and	O
Raiders	ORGANIZATION
)	O
and	O
the	O
AFC	ORGANIZATION
East	ORGANIZATION
(	O
Bills	ORGANIZATION
,	O
Dolphins	ORGANIZATION
,	O
Jets	ORGANIZATION
,	O
and	O
Patriots	ORGANIZATION
)	O
.	O
Additionally	O
,	O
the	O
Bengals	ORGANIZATION
now	O
play	O
in	O
the	O
AFC	ORGANIZATION
North	ORGANIZATION
and	O
the	O
Tennessee	ORGANIZATION
Titans	ORGANIZATION
play	O
in	O
the	O
AFC	ORGANIZATION
South	ORGANIZATION
.	O
There	O
was	O
no	O
tie-breaker	O
protocol	O
in	O
place	O
,	O
so	O
a	O
one	O
game	O
playoff	O
was	O
held	O
in	O
War	LOCATION
Memorial	LOCATION
Stadium	LOCATION
in	O
December	O
.	O
The	O
Patriots	ORGANIZATION
traveled	O
to	O
San	LOCATION
Diego	LOCATION
as	O
the	O
Chargers	ORGANIZATION
completed	O
a	O
three	O
game	O
season	O
sweep	O
over	O
the	O
weary	O
Patriots	ORGANIZATION
with	O
a	O
51-10	O
victory	O
.	O
The	O
Raiders	ORGANIZATION
beat	O
the	O
Chiefs	ORGANIZATION
41-6	O
in	O
a	O
division	O
playoff	O
to	O
qualify	O
for	O
the	O
AFL	MISC
Championship	MISC
Game	MISC
.	O
In	O
1969	O
,	O
the	O
final	O
year	O
of	O
the	O
independent	O
AFL	ORGANIZATION
,	O
Professional	ORGANIZATION
Football	ORGANIZATION
's	O
first	O
"	O
wild	O
card	O
"	O
playoffs	O
were	O
conducted	O
.	O
The	O
AFL	ORGANIZATION
did	O
not	O
play	O
an	O
All-Star	O
game	O
after	O
its	O
first	O
season	O
in	O
1960	O
,	O
but	O
did	O
stage	O
All-Star	O
games	O
for	O
the	O
1961	O
through	O
1969	O
seasons	O
.	O
That	O
season	O
,	O
the	O
league	O
champion	O
Buffalo	ORGANIZATION
Bills	ORGANIZATION
played	O
all-stars	O
from	O
the	O
other	O
teams	O
.	O
The	O
NFL	ORGANIZATION
considers	O
AFL	ORGANIZATION
statistics	O
and	O
records	O
equivalent	O
to	O
its	O
own	O
.	O
