Helsingborg	LOCATION
Municipality	LOCATION
(	O
Helsingborgs	LOCATION
kommun	LOCATION
)	O
is	O
a	O
municipality	O
in	O
Skåne	LOCATION
County	LOCATION
in	O
Sweden	LOCATION
.	O
Its	O
seat	O
is	O
located	O
in	O
the	O
city	O
of	O
Helsingborg	LOCATION
,	O
which	O
is	O
Sweden	LOCATION
's	O
eighth	O
largest	O
city	O
.	O
There	O
are	O
15	O
urban	O
areas	O
in	O
Helsingborg	LOCATION
Municipality	LOCATION
.	O
1	O
)	O
Rydebäck	LOCATION
is	O
bimunicipal	O
locality	O
as	O
a	O
minor	O
part	O
of	O
it	O
(	O
with	O
about	O
40	O
inhabitants	O
)	O
is	O
located	O
in	O
Landskrona	LOCATION
Municipality	LOCATION
.	O
