In	O
1993	O
,	O
he	O
was	O
elected	O
to	O
the	O
Pro	ORGANIZATION
Football	ORGANIZATION
Hall	ORGANIZATION
of	ORGANIZATION
Fame	ORGANIZATION
.	O
He	O
then	O
transferred	O
to	O
San	ORGANIZATION
José	ORGANIZATION
State	ORGANIZATION
University	ORGANIZATION
,	O
where	O
he	O
played	O
as	O
a	O
tight	O
end	O
and	O
a	O
defensive	O
end	O
.	O
"	O
I	O
was	O
very	O
impressed	O
,	O
individually	O
,	O
by	O
his	O
knowledge	O
,	O
by	O
his	O
intelligence	O
,	O
by	O
his	O
personality	O
and	O
hired	O
him,	O
"	O
Levy	PERSON
said	O
.	O
He	O
then	O
moved	O
to	O
the	O
AFL	ORGANIZATION
expansion	O
Cincinnati	ORGANIZATION
Bengals	ORGANIZATION
in	O
1968	O
,	O
serving	O
under	O
Paul	PERSON
Brown	PERSON
for	O
eight	O
seasons	O
as	O
one	O
of	O
the	O
architects	O
of	O
the	O
team	O
's	O
offense	O
,	O
built	O
around	O
quarterback	O
Ken	PERSON
Anderson	PERSON
and	O
wide	O
receiver	O
Isaac	PERSON
Curtis	PERSON
.	O
"	O
And	O
then	O
when	O
I	O
left	O
him	O
,	O
he	O
called	O
whoever	O
he	O
thought	O
was	O
necessary	O
to	O
keep	O
me	O
out	O
of	O
the	O
NFL	ORGANIZATION
.	O
"	O
His	O
two	O
Stanford	ORGANIZATION
teams	O
went	O
9-3	O
in	O
1977	O
with	O
a	O
win	O
in	O
the	O
Sun	ORGANIZATION
Bowl	ORGANIZATION
,	O
and	O
8-4	O
in	O
1978	O
with	O
a	O
win	O
in	O
the	O
Bluebonnet	MISC
Bowl	MISC
;	O
his	O
notable	O
players	O
at	O
Stanford	ORGANIZATION
included	O
quarterbacks	O
Guy	PERSON
Benjamin	PERSON
and	O
Steve	PERSON
Dils	PERSON
,	O
wide	O
receivers	O
James	PERSON
Lofton	PERSON
and	O
Ken	PERSON
Margerum	PERSON
,	O
and	O
running	O
back	O
Darrin	PERSON
Nelson	PERSON
.	O
He	O
also	O
traded	O
a	O
2nd	O
and	O
4th	O
round	O
pick	O
in	O
the	O
1987	O
draft	O
for	O
Steve	PERSON
Young	PERSON
.	O
His	O
success	O
with	O
the	O
49ers	O
was	O
rewarded	O
with	O
his	O
election	O
to	O
the	O
Professional	ORGANIZATION
Football	ORGANIZATION
Hall	ORGANIZATION
of	ORGANIZATION
Fame	ORGANIZATION
in	O
1993	O
.	O
Four	O
important	O
wins	O
during	O
the	O
1981	O
season	O
were	O
two	O
wins	O
each	O
over	O
the	O
Los	ORGANIZATION
Angeles	ORGANIZATION
Rams	ORGANIZATION
and	O
the	O
Dallas	ORGANIZATION
Cowboys	ORGANIZATION
.	O
The	O
Rams	ORGANIZATION
were	O
only	O
one	O
year	O
removed	O
from	O
a	O
Super	MISC
Bowl	MISC
appearance	O
,	O
and	O
had	O
dominated	O
the	O
series	O
with	O
the	O
49ers	O
for	O
nearly	O
a	O
decade	O
.	O
The	O
49ers	O
'	O
two	O
wins	O
over	O
the	O
Rams	ORGANIZATION
in	O
1981	O
marked	O
the	O
shift	O
of	O
dominance	O
in	O
favor	O
of	O
the	O
49ers	O
that	O
lasted	O
until	O
the	O
late	O
1990s	O
.	O
On	O
Monday	MISC
Night	MISC
Football	MISC
that	O
week	O
,	O
the	O
49ers	O
'	O
win	O
was	O
not	O
included	O
in	O
the	O
famous	O
halftime	O
highlights	O
.	O
Many	O
of	O
his	O
assistant	O
coaches	O
went	O
on	O
to	O
be	O
head	O
coaches	O
,	O
including	O
George	PERSON
Seifert	PERSON
,	O
Mike	PERSON
Holmgren	PERSON
,	O
Mike	PERSON
Shanahan	PERSON
,	O
Ray	PERSON
Rhodes	PERSON
,	O
and	O
Dennis	PERSON
Green	PERSON
.	O
Many	O
former	O
and	O
current	O
NFL	ORGANIZATION
head	O
coaches	O
trace	O
their	O
lineage	O
back	O
to	O
Bill	PERSON
Walsh	PERSON
on	O
his	O
coaching	O
tree	O
,	O
shown	O
below	O
.	O
Stanford	ORGANIZATION
finished	O
the	O
season	O
with	O
an	O
upset	O
victory	O
over	O
Penn	ORGANIZATION
State	ORGANIZATION
in	O
the	O
Blockbuster	MISC
Bowl	MISC
on	O
January	O
1	O
,	O
1993	O
and	O
a	O
#	O
9	O
ranking	O
in	O
the	O
final	O
AP	ORGANIZATION
Poll	ORGANIZATION
.	O
Bill	PERSON
Walsh	PERSON
was	O
also	O
the	O
author	O
of	O
three	O
books	O
,	O
a	O
motivational	O
speaker	O
,	O
and	O
taught	O
classes	O
at	O
the	O
Stanford	ORGANIZATION
Graduate	ORGANIZATION
School	ORGANIZATION
of	ORGANIZATION
Business	ORGANIZATION
.	O
In	O
November	O
2006	O
,	O
he	O
confirmed	O
that	O
he	O
was	O
undergoing	O
treatment	O
for	O
the	O
illness	O
at	O
the	O
Stanford	ORGANIZATION
University	ORGANIZATION
Medical	ORGANIZATION
Center	ORGANIZATION
.	O
Bill	PERSON
Walsh	PERSON
died	O
of	O
leukemia	O
at	O
10:45	O
am	O
on	O
July	O
30	O
,	O
2007	O
,	O
at	O
his	O
home	O
in	O
Woodside	LOCATION
,	O
California	LOCATION
.	O
