Born	O
in	O
Saudi	LOCATION
Arabia	LOCATION
,	O
al-Nami	PERSON
had	O
served	O
as	O
a	O
muezzin	O
and	O
was	O
a	O
college	O
student	O
.	O
He	O
left	O
his	O
family	O
in	O
2000	O
to	O
complete	O
the	O
Hajj	MISC
,	O
but	O
later	O
went	O
to	O
Afghanistan	LOCATION
bound	O
for	O
an	O
Al-Qaeda	ORGANIZATION
training	O
camp	O
where	O
he	O
befriended	O
other	O
future	O
9/11	O
hijackers	O
.	O
He	O
arrived	O
in	O
the	O
U.S.	LOCATION
in	O
May	O
2001	O
on	O
a	O
tourist	O
visa	O
.	O
On	O
September	O
11	O
,	O
2001	O
,	O
al-Nami	PERSON
boarded	O
United	MISC
Airlines	MISC
Flight	MISC
93	MISC
and	O
assisted	O
in	O
the	O
hijacking	O
of	O
the	O
plane	O
,	O
which	O
crashed	O
in	O
a	O
field	O
in	O
Shanksville	LOCATION
,	O
Pennsylvania	LOCATION
after	O
a	O
passenger	O
uprising	O
.	O
During	O
his	O
time	O
at	O
al-Farooq	PERSON
,	O
there	O
is	O
a	O
curious	O
mention	O
under	O
Mushabib	PERSON
al-Hamlan	PERSON
's	O
details	O
that	O
al-Nami	PERSON
had	O
recently	O
had	O
laser	O
eye	O
surgery	O
,	O
an	O
uncited	O
fact	O
that	O
does	O
not	O
reappear	O
.	O
Records	O
at	O
the	O
time	O
only	O
recorded	O
past	O
failures	O
to	O
procure	O
a	O
visa	O
,	O
so	O
the	O
officer	O
had	O
no	O
way	O
of	O
realising	O
that	O
al-Nami	PERSON
had	O
successfully	O
received	O
an	O
earlier	O
visa	O
.	O
This	O
probably	O
followed	O
their	O
return	O
to	O
Saudi	LOCATION
Arabia	LOCATION
to	O
get	O
"	O
clean	O
"	O
passports	O
.	O
While	O
in	O
the	O
United	LOCATION
Arab	LOCATION
Emirates	LOCATION
,	O
al-Nami	PERSON
purchased	O
traveler	O
's	O
cheques	O
presumed	O
to	O
have	O
been	O
paid	O
for	O
by	O
Mustafa	PERSON
Ahmed	PERSON
al-Hawsawi	PERSON
.	O
Five	O
other	O
hijackers	O
also	O
passed	O
through	O
the	O
UAE	LOCATION
and	O
purchased	O
travellers	O
cheques	O
,	O
including	O
Majed	PERSON
Moqed	PERSON
,	O
Saeed	PERSON
Alghamdi	PERSON
,	O
Hamza	PERSON
Alghamdi	PERSON
,	O
Ahmed	PERSON
al-Haznawi	PERSON
and	O
Wail	PERSON
Alshehri	PERSON
.	O
In	O
March	O
2001	O
,	O
Ahmed	PERSON
al-Nami	PERSON
appeared	O
in	O
an	O
Al	ORGANIZATION
Qaeda	ORGANIZATION
farewell	O
video	O
showing	O
13	O
of	O
the	O
muscle	O
hijackers	O
before	O
they	O
left	O
their	O
training	O
centre	O
in	O
Kandahar	LOCATION
;	O
while	O
he	O
does	O
not	O
speak	O
,	O
he	O
is	O
seen	O
studying	O
maps	O
and	O
flight	O
manuals	O
.	O
On	O
May	O
28	O
,	O
al-Nami	PERSON
arrived	O
in	O
the	O
United	LOCATION
States	LOCATION
from	O
Dubai	LOCATION
with	O
fellow-hijackers	O
Mohand	PERSON
al-Shehri	PERSON
and	O
Hamza	PERSON
al-Ghamdi	PERSON
.	O
On	O
September	O
7	O
,	O
all	O
four	O
Flight	MISC
93	MISC
hijackers	O
flew	O
from	O
Fort	LOCATION
Lauderdale	LOCATION
to	O
Newark	LOCATION
International	LOCATION
Airport	LOCATION
aboard	O
Spirit	ORGANIZATION
Airlines	ORGANIZATION
.	O
On	O
September	O
11	O
,	O
al-Nami	PERSON
arrived	O
in	O
Newark	LOCATION
to	O
board	O
United	MISC
Airlines	MISC
Flight	MISC
93	MISC
along	O
with	O
Saeed	PERSON
al-Ghamdi	PERSON
,	O
Ahmed	PERSON
al-Haznawi	PERSON
and	O
Ziad	PERSON
Jarrah	PERSON
.	O
Due	O
to	O
the	O
flight	O
's	O
routine	O
delay	O
,	O
the	O
pilot	O
and	O
crew	O
were	O
notified	O
of	O
the	O
previous	O
hijackings	O
and	O
were	O
told	O
to	O
be	O
on	O
the	O
alert	O
,	O
though	O
within	O
two	O
minutes	O
Jarrah	PERSON
had	O
stormed	O
the	O
cockpit	O
leaving	O
the	O
pilots	O
dead	O
or	O
injured	O
.	O
At	O
least	O
two	O
of	O
the	O
cellphone	O
calls	O
made	O
by	O
passengers	O
indicate	O
that	O
all	O
the	O
hijackers	O
they	O
saw	O
were	O
wearing	O
red	O
bandanas	O
,	O
which	O
some	O
have	O
questioned	O
may	O
have	O
signified	O
an	O
allegiance	O
to	O
the	O
Egyptian	ORGANIZATION
Islamic	ORGANIZATION
Jihad	ORGANIZATION
.	O
It	O
is	O
believed	O
the	O
hijackers	O
crashed	O
the	O
plane	O
into	O
the	O
Pennsylvania	LOCATION
farmland	O
rather	O
than	O
cede	O
control	O
of	O
the	O
plane	O
.	O
After	O
the	O
attacks	O
,	O
an	O
employee	O
of	O
Saudi	ORGANIZATION
Arabian	ORGANIZATION
Airlines	ORGANIZATION
named	O
Ahmed	PERSON
al-Nami	PERSON
came	O
forward	O
to	O
say	O
that	O
he	O
feared	O
his	O
identity	O
had	O
been	O
stolen	O
,	O
although	O
he	O
had	O
never	O
lost	O
his	O
passport	O
.	O
