The	O
ISO	ORGANIZATION
on-line	O
facility	O
only	O
refers	O
back	O
to	O
1978	O
.	O
Generally	O
,	O
a	O
book	O
publisher	O
is	O
not	O
required	O
to	O
assign	O
an	O
ISBN	O
,	O
nor	O
is	O
it	O
necessary	O
for	O
a	O
book	O
to	O
display	O
its	O
number	O
(	O
except	O
in	O
China	LOCATION
;	O
see	O
below	O
)	O
.	O
The	O
cited	O
list	O
of	O
identifiers	O
shows	O
this	O
has	O
happened	O
in	O
China	LOCATION
and	O
in	O
more	O
than	O
a	O
dozen	O
other	O
countries	O
.	O
However	O
,	O
book-ordering	O
systems	O
such	O
as	O
Amazon.com	ORGANIZATION
will	O
not	O
search	O
for	O
a	O
book	O
if	O
an	O
invalid	O
ISBN	O
is	O
entered	O
to	O
its	O
search	O
engine	O
.	O
Partly	O
because	O
of	O
a	O
pending	O
shortage	O
in	O
certain	O
ISBN	O
categories	O
,	O
the	O
International	ORGANIZATION
Organization	ORGANIZATION
for	ORGANIZATION
Standardization	ORGANIZATION
(	O
ISO	ORGANIZATION
)	O
migrated	O
to	O
a	O
thirteen-digit	O
ISBN	O
;	O
the	O
process	O
began	O
January	O
1	O
,	O
2005	O
and	O
was	O
to	O
conclude	O
January	O
1	O
,	O
2007	O
.	O
Hence	O
,	O
many	O
booksellers	O
(	O
e.g.	O
Barnes	ORGANIZATION
&	ORGANIZATION
Noble	ORGANIZATION
)	O
migrated	O
to	O
EAN	O
barcodes	O
as	O
early	O
as	O
March	O
2005	O
.	O
