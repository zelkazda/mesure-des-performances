The	O
nature	O
of	O
art	O
has	O
been	O
described	O
by	O
Richard	PERSON
Wollheim	PERSON
as	O
"	O
one	O
of	O
the	O
most	O
elusive	O
of	O
the	O
traditional	O
problems	O
of	O
human	O
culture	O
"	O
.	O
Leo	PERSON
Tolstoy	PERSON
identified	O
art	O
as	O
a	O
use	O
of	O
indirect	O
means	O
to	O
communicate	O
from	O
one	O
person	O
to	O
another	O
.	O
Benedetto	PERSON
Croce	PERSON
and	O
R.G.	PERSON
Collingwood	PERSON
advanced	O
the	O
idealist	O
view	O
that	O
art	O
expresses	O
emotions	O
,	O
and	O
that	O
the	O
work	O
of	O
art	O
therefore	O
essentially	O
exists	O
in	O
the	O
mind	O
of	O
the	O
creator	O
.	O
The	O
theory	O
of	O
art	O
as	O
form	O
has	O
its	O
roots	O
in	O
the	O
philosophy	O
of	O
Immanuel	PERSON
Kant	PERSON
,	O
and	O
was	O
developed	O
in	O
the	O
early	O
twentieth	O
century	O
by	O
Roger	PERSON
Fry	PERSON
and	O
Clive	PERSON
Bell	PERSON
.	O
More	O
recently	O
,	O
thinkers	O
influenced	O
by	O
Martin	PERSON
Heidegger	PERSON
have	O
interpreted	O
art	O
as	O
the	O
means	O
by	O
which	O
a	O
community	O
develops	O
for	O
itself	O
a	O
medium	O
for	O
self-expression	O
and	O
interpretation	O
.	O
Adorno	PERSON
said	O
in	O
1970	O
,	O
"	O
It	O
is	O
now	O
taken	O
for	O
granted	O
that	O
nothing	O
which	O
concerns	O
art	O
can	O
be	O
taken	O
for	O
granted	O
any	O
more	O
:	O
neither	O
art	O
itself	O
,	O
nor	O
art	O
in	O
relationship	O
to	O
the	O
whole	O
,	O
nor	O
even	O
the	O
right	O
of	O
art	O
to	O
exist	O
.	O
"	O
So	O
,	O
for	O
example	O
,	O
Tang	MISC
Dynasty	MISC
paintings	O
are	O
monochromatic	O
and	O
sparse	O
,	O
emphasizing	O
idealized	O
landscapes	O
,	O
but	O
Ming	LOCATION
Dynasty	LOCATION
paintings	O
are	O
busy	O
,	O
colorful	O
,	O
and	O
focus	O
on	O
telling	O
stories	O
via	O
setting	O
and	O
composition	O
.	O
Lichtenstein	PERSON
thus	O
uses	O
the	O
dots	O
as	O
a	O
style	O
to	O
question	O
the	O
"	O
high	O
"	O
art	O
of	O
painting	O
with	O
the	O
"	O
low	O
"	O
art	O
of	O
comics	O
--	O
to	O
comment	O
on	O
class	O
distinctions	O
in	O
culture	O
.	O
A	O
common	O
view	O
is	O
that	O
the	O
epithet	O
"	O
art	O
"	O
,	O
particular	O
in	O
its	O
elevated	O
sense	O
,	O
requires	O
a	O
certain	O
level	O
of	O
creative	O
expertise	O
by	O
the	O
artist	O
,	O
whether	O
this	O
be	O
a	O
demonstration	O
of	O
technical	O
ability	O
or	O
an	O
originality	O
in	O
stylistic	O
approach	O
such	O
as	O
in	O
the	O
plays	O
of	O
Shakespeare	PERSON
,	O
or	O
a	O
combination	O
of	O
these	O
two	O
.	O
Traditionally	O
skill	O
of	O
execution	O
was	O
viewed	O
as	O
a	O
quality	O
inseparable	O
from	O
art	O
and	O
thus	O
necessary	O
for	O
its	O
success	O
;	O
for	O
Leonardo	PERSON
da	PERSON
Vinci	PERSON
,	O
art	O
,	O
neither	O
more	O
nor	O
less	O
than	O
his	O
other	O
endeavors	O
,	O
was	O
a	O
manifestation	O
of	O
skill	O
.	O
Rembrandt	PERSON
's	O
work	O
,	O
now	O
praised	O
for	O
its	O
ephemeral	O
virtues	O
,	O
was	O
most	O
admired	O
by	O
his	O
contemporaries	O
for	O
its	O
virtuosity	O
.	O
At	O
the	O
turn	O
of	O
the	O
20th	O
century	O
,	O
the	O
adroit	O
performances	O
of	O
John	PERSON
Singer	PERSON
Sargent	PERSON
were	O
alternately	O
admired	O
and	O
viewed	O
with	O
skepticism	O
for	O
their	O
manual	O
fluency	O
,	O
yet	O
at	O
nearly	O
the	O
same	O
time	O
the	O
artist	O
who	O
would	O
become	O
the	O
era	O
's	O
most	O
recognized	O
and	O
peripatetic	O
iconoclast	O
,	O
Pablo	PERSON
Picasso	PERSON
,	O
was	O
completing	O
a	O
traditional	O
academic	O
training	O
at	O
which	O
he	O
excelled	O
.	O
Emin	PERSON
slept	O
(	O
and	O
engaged	O
in	O
other	O
activities	O
)	O
in	O
her	O
bed	O
before	O
placing	O
the	O
result	O
in	O
a	O
gallery	O
as	O
work	O
of	O
art	O
.	O
Hirst	PERSON
came	O
up	O
with	O
the	O
conceptual	O
design	O
for	O
the	O
artwork	O
but	O
has	O
left	O
most	O
of	O
the	O
eventual	O
creation	O
of	O
many	O
works	O
to	O
employed	O
artisans	O
.	O
Hirst	PERSON
's	O
celebrity	O
is	O
founded	O
entirely	O
on	O
his	O
ability	O
to	O
produce	O
shocking	O
concepts	O
.	O
Yet	O
at	O
the	O
same	O
time	O
,	O
the	O
horrific	O
imagery	O
demonstrates	O
Goya	PERSON
's	O
keen	O
artistic	O
ability	O
in	O
composition	O
and	O
execution	O
and	O
produces	O
fitting	O
social	O
and	O
political	O
outrage	O
.	O
Édouard	PERSON
Manet	PERSON
's	O
Le	MISC
Déjeuner	MISC
sur	MISC
l'Herbe	MISC
(	O
1863	O
)	O
,	O
was	O
considered	O
scandalous	O
not	O
because	O
of	O
the	O
nude	O
woman	O
,	O
but	O
because	O
she	O
is	O
seated	O
next	O
to	O
men	O
fully	O
dressed	O
in	O
the	O
clothing	O
of	O
the	O
time	O
,	O
rather	O
than	O
in	O
robes	O
of	O
the	O
antique	O
world	O
.	O
John	PERSON
Singer	PERSON
Sargent	PERSON
's	O
Madame	PERSON
Pierre	PERSON
Gautreau	PERSON
(	O
Madam	PERSON
X	PERSON
)	O
(	O
1884	O
)	O
,	O
caused	O
a	O
huge	O
uproar	O
over	O
the	O
reddish	O
pink	O
used	O
to	O
color	O
the	O
woman	O
's	O
ear	O
lobe	O
,	O
considered	O
far	O
too	O
suggestive	O
and	O
supposedly	O
ruining	O
the	O
high-society	O
model	O
's	O
reputation	O
.	O
The	O
aesthetic	O
theorist	O
John	PERSON
Ruskin	PERSON
,	O
who	O
championed	O
what	O
he	O
saw	O
as	O
the	O
naturalism	O
of	O
J.	PERSON
M.	PERSON
W.	PERSON
Turner	PERSON
,	O
saw	O
art	O
's	O
role	O
as	O
the	O
communication	O
by	O
artifice	O
of	O
an	O
essential	O
truth	O
that	O
could	O
only	O
be	O
found	O
in	O
nature	O
.	O
Though	O
only	O
originally	O
intended	O
as	O
a	O
way	O
of	O
understanding	O
a	O
specific	O
set	O
of	O
artists	O
,	O
Greenberg	PERSON
's	O
definition	O
of	O
modern	O
art	O
is	O
important	O
to	O
many	O
of	O
the	O
ideas	O
of	O
art	O
within	O
the	O
various	O
art	O
movements	O
of	O
the	O
20th	O
century	O
and	O
early	O
21st	O
century	O
.	O
Pop	O
artists	O
like	O
Andy	PERSON
Warhol	PERSON
became	O
both	O
noteworthy	O
and	O
influential	O
through	O
work	O
including	O
and	O
possibly	O
critiquing	O
popular	O
culture	O
,	O
as	O
well	O
as	O
the	O
art	O
world	O
.	O
For	O
example	O
,	O
when	O
the	O
Daily	MISC
Mail	MISC
criticized	O
Hirst	PERSON
's	O
and	O
Emin	PERSON
's	O
work	O
by	O
arguing	O
"	O
For	O
1,000	O
years	O
art	O
has	O
been	O
one	O
of	O
our	O
great	O
civilising	O
forces	O
.	O
Today	O
,	O
pickled	O
sheep	O
and	O
soiled	O
beds	O
threaten	O
to	O
make	O
barbarians	O
of	O
us	O
all	O
"	O
they	O
are	O
not	O
advancing	O
a	O
definition	O
or	O
theory	O
about	O
art	O
,	O
but	O
questioning	O
the	O
value	O
of	O
Hirst	PERSON
's	O
and	O
Emin	PERSON
's	O
work	O
.	O
In	O
1998	O
,	O
Arthur	PERSON
Danto	PERSON
,	O
suggested	O
a	O
thought	O
experiment	O
showing	O
that	O
"	O
the	O
status	O
of	O
an	O
artifact	O
as	O
work	O
of	O
art	O
results	O
from	O
the	O
ideas	O
a	O
culture	O
applies	O
to	O
it	O
,	O
rather	O
than	O
its	O
inherent	O
physical	O
or	O
perceptible	O
qualities	O
.	O
