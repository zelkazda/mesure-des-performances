Guam	LOCATION
has	O
no	O
railways	O
,	O
nor	O
does	O
it	O
have	O
a	O
merchant	O
marine	O
.	O
The	O
largest	O
port	O
is	O
Apra	LOCATION
Harbor	LOCATION
,	O
which	O
serves	O
almost	O
all	O
commercial	O
traffic	O
including	O
cruise	O
,	O
cargo	O
and	O
fishing	O
vessels	O
.	O
There	O
are	O
smaller	O
harbors	O
located	O
on	O
the	O
island	O
(	O
most	O
notably	O
one	O
in	O
Hagatna	LOCATION
and	O
one	O
in	O
Agat	LOCATION
)	O
which	O
serve	O
recreational	O
boaters	O
.	O
Its	O
main	O
commercial	O
airport	O
is	O
the	O
Antonio	LOCATION
B.	LOCATION
Won	LOCATION
Pat	LOCATION
International	LOCATION
Airport	LOCATION
.	O
