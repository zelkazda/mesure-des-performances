The	O
country	O
's	O
only	O
outlet	O
to	O
the	O
Atlantic	LOCATION
Ocean	LOCATION
is	O
a	O
narrow	O
strip	O
of	O
land	O
on	O
the	O
north	O
bank	O
of	O
the	O
Congo	LOCATION
River	LOCATION
.	O
The	O
vast	O
,	O
low-lying	O
central	O
area	O
is	O
a	O
basin-shaped	O
plateau	O
sloping	O
toward	O
the	O
west	O
,	O
covered	O
by	O
tropical	O
rainforest	O
and	O
criss-crossed	O
by	O
rivers	O
,	O
a	O
large	O
area	O
of	O
this	O
has	O
been	O
categorised	O
by	O
the	O
World	ORGANIZATION
Wildlife	ORGANIZATION
Fund	ORGANIZATION
as	O
the	O
Central	LOCATION
Congolian	LOCATION
lowland	LOCATION
forests	LOCATION
ecoregion	O
.	O
The	O
forest	O
centre	O
is	O
surrounded	O
by	O
mountainous	O
terraces	O
in	O
the	O
west	O
,	O
plateaus	O
merging	O
into	O
savannas	O
in	O
the	O
south	O
and	O
southwest	O
,	O
and	O
dense	O
grasslands	O
extending	O
beyond	O
the	O
Congo	LOCATION
River	LOCATION
in	O
the	O
north	O
.	O
High	O
mountains	O
of	O
the	O
Ruwenzori	LOCATION
Range	LOCATION
(	O
some	O
above	O
5000	O
m/16000	O
ft	O
)	O
are	O
found	O
on	O
the	O
eastern	O
borders	O
with	O
Rwanda	LOCATION
and	O
Uganda	LOCATION
.	O
The	O
climate	O
is	O
hot	O
and	O
humid	O
in	O
the	O
river	O
basin	O
and	O
cool	O
and	O
dry	O
in	O
the	O
southern	O
highlands	O
,	O
with	O
a	O
cold	O
,	O
alpine	O
climate	O
in	O
the	O
Ruwenzori	LOCATION
Range	LOCATION
.	O
Land	O
boundaries	O
:	O
total	O
:	O
10,744	O
km	O
border	O
countries	O
:	O
Angola	LOCATION
2,511	O
km	O
,	O
Burundi	LOCATION
233	O
km	O
,	O
Central	LOCATION
African	LOCATION
Republic	LOCATION
1,577	O
km	O
,	O
Republic	LOCATION
of	LOCATION
the	LOCATION
Congo	LOCATION
2,410	O
km	O
,	O
Rwanda	LOCATION
217	O
km	O
,	O
Sudan	LOCATION
628	O
km	O
,	O
Tanzania	LOCATION
473	O
km	O
,	O
Uganda	LOCATION
765	O
km	O
,	O
Zambia	LOCATION
1,930	O
km	O
The	O
high	O
mountains	O
of	O
the	O
Ruwenzori	LOCATION
Range	LOCATION
on	O
the	O
eastern	O
borders	O
.	O
