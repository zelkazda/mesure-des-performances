While	O
packed	O
BCD	O
does	O
not	O
make	O
optimal	O
use	O
of	O
storage	O
(	O
about	O
1	O
/	O
6	O
of	O
the	O
memory	O
used	O
is	O
wasted	O
)	O
,	O
conversion	O
to	O
ASCII	O
,	O
EBCDIC	O
,	O
or	O
the	O
various	O
encodings	O
of	O
Unicode	MISC
is	O
still	O
trivial	O
,	O
as	O
no	O
arithmetic	O
operations	O
are	O
required	O
.	O
Fixed-point	O
decimal	O
numbers	O
are	O
supported	O
by	O
some	O
programming	O
languages	O
(	O
such	O
as	O
COBOL	MISC
and	O
PL/I	MISC
)	O
,	O
and	O
provide	O
an	O
implicit	O
decimal	O
point	O
in	O
front	O
of	O
one	O
of	O
the	O
digits	O
.	O
Some	O
implementations	O
,	O
for	O
example	O
IBM	ORGANIZATION
mainframe	O
systems	O
,	O
support	O
zoned	O
decimal	O
numeric	O
representations	O
.	O
Some	O
languages	O
(	O
such	O
as	O
COBOL	MISC
and	O
PL/I	MISC
)	O
directly	O
support	O
fixed-point	O
zoned	O
decimal	O
values	O
,	O
assigning	O
an	O
implicit	O
decimal	O
point	O
at	O
some	O
location	O
between	O
the	O
decimal	O
digits	O
of	O
a	O
number	O
.	O
IBM	ORGANIZATION
used	O
the	O
terms	O
binary-coded	O
decimal	O
and	O
BCD	O
for	O
6-bit	O
alphameric	O
codes	O
that	O
represented	O
numbers	O
,	O
upper-case	O
letters	O
and	O
special	O
characters	O
.	O
With	O
the	O
introduction	O
of	O
System/360	MISC
,	O
IBM	ORGANIZATION
expanded	O
6-bit	O
BCD	O
alphamerics	O
to	O
8-bit	O
EBCDIC	O
,	O
allowing	O
the	O
addition	O
of	O
many	O
more	O
characters	O
(	O
e.g.	O
,	O
lowercase	O
letters	O
)	O
.	O
Today	O
,	O
BCD	O
data	O
is	O
still	O
heavily	O
used	O
in	O
IBM	ORGANIZATION
processors	O
and	O
databases	O
,	O
such	O
as	O
IBM	MISC
DB2	MISC
,	O
mainframes	O
,	O
and	O
Power6	MISC
.	O
Other	O
computers	O
such	O
as	O
the	O
Digital	ORGANIZATION
Equipment	ORGANIZATION
Corporation	ORGANIZATION
VAX-11	MISC
series	O
could	O
also	O
use	O
BCD	O
for	O
numeric	O
data	O
and	O
could	O
perform	O
arithmetic	O
directly	O
on	O
packed	O
BCD	O
data	O
.	O
The	O
Atari	MISC
8-bit	MISC
family	MISC
of	O
computers	O
used	O
BCD	O
to	O
implement	O
floating-point	O
algorithms	O
.	O
The	O
MOS	MISC
6502	MISC
processor	O
used	O
has	O
a	O
BCD	O
mode	O
that	O
affects	O
the	O
addition	O
and	O
subtraction	O
instructions	O
.	O
Early	O
models	O
of	O
the	O
PlayStation	MISC
3	MISC
store	O
the	O
date	O
and	O
time	O
in	O
BCD	O
.	O
Programmable	O
calculators	O
manufactured	O
by	O
Texas	ORGANIZATION
Instruments	ORGANIZATION
,	O
Hewlett-Packard	ORGANIZATION
,	O
and	O
others	O
typically	O
employ	O
a	O
floating-point	O
BCD	O
format	O
,	O
typically	O
with	O
two	O
or	O
three	O
digits	O
for	O
the	O
(	O
decimal	O
)	O
exponent	O
.	O
The	O
COBOL	MISC
programming	O
language	O
,	O
for	O
example	O
,	O
supports	O
a	O
total	O
of	O
five	O
zoned	O
decimal	O
formats	O
,	O
each	O
one	O
encoding	O
the	O
numeric	O
sign	O
in	O
a	O
different	O
way	O
:	O
