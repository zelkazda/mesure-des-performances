Claude	PERSON
Piron	PERSON
(	O
26	O
February	O
1931	O
--	O
22	O
January	O
2008	O
)	O
,	O
a	O
linguist	O
and	O
psychologist	O
,	O
was	O
a	O
translator	O
for	O
the	O
United	ORGANIZATION
Nations	ORGANIZATION
from	O
1956	O
to	O
1961	O
.	O
After	O
leaving	O
the	O
UN	ORGANIZATION
,	O
he	O
worked	O
the	O
world	O
over	O
for	O
the	O
World	ORGANIZATION
Health	ORGANIZATION
Organization	ORGANIZATION
.	O
He	O
was	O
a	O
prolific	O
author	O
of	O
Esperanto	MISC
works	O
.	O
Piron	PERSON
was	O
a	O
psychotherapist	O
and	O
taught	O
from	O
1973	O
to	O
1994	O
in	O
the	O
psychology	O
department	O
at	O
the	O
University	ORGANIZATION
of	ORGANIZATION
Geneva	ORGANIZATION
in	O
Switzerland	LOCATION
.	O
In	O
a	O
lecture	O
on	O
the	O
current	O
system	O
of	O
international	O
communication	O
Piron	PERSON
argued	O
that	O
"	O
Esperanto	MISC
relies	O
entirely	O
on	O
innate	O
reflexes	O
"	O
and	O
"	O
differs	O
from	O
all	O
other	O
languages	O
in	O
that	O
you	O
can	O
always	O
trust	O
your	O
natural	O
tendency	O
to	O
generalize	O
patterns	O
...	O
The	O
same	O
neuropsychological	O
law	O
...	O
--	O
called	O
by	O
Jean	PERSON
Piaget	PERSON
generalizing	O
assimilation	O
--	O
applies	O
to	O
word	O
formation	O
as	O
well	O
as	O
to	O
grammar	O
.	O
"	O
His	O
diverse	O
Esperanto	MISC
writings	O
include	O
instructional	O
books	O
,	O
books	O
for	O
beginners	O
,	O
novels	O
,	O
short	O
stories	O
,	O
poems	O
,	O
articles	O
and	O
non-fiction	O
books	O
.	O
Gerda	MISC
malaperis	MISC
!	MISC
is	O
a	O
novella	O
which	O
uses	O
basic	O
grammar	O
and	O
vocabulary	O
in	O
the	O
first	O
chapter	O
and	O
builds	O
up	O
to	O
expert	O
Esperanto	MISC
by	O
the	O
end	O
,	O
including	O
word	O
lists	O
so	O
that	O
beginners	O
may	O
easily	O
follow	O
along	O
.	O
He	O
also	O
presents	O
the	O
idea	O
that	O
,	O
once	O
one	O
has	O
learned	O
enough	O
vocabulary	O
to	O
express	O
himself	O
,	O
it	O
is	O
easier	O
to	O
think	O
clearly	O
in	O
Esperanto	MISC
than	O
in	O
many	O
other	O
languages	O
.	O
Piron	PERSON
's	O
view	O
is	O
that	O
,	O
while	O
one	O
may	O
desire	O
happiness	O
,	O
desire	O
is	O
not	O
enough	O
.	O
