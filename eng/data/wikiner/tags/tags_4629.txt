This	O
same	O
approach	O
has	O
been	O
used	O
for	O
the	O
Embraer	ORGANIZATION
170/190	O
jets	O
.	O
Most	O
Airbus	ORGANIZATION
Industries	ORGANIZATION
airliners	O
are	O
operated	O
with	O
side-sticks	O
.	O
Some	O
aircraft	O
,	O
the	O
Panavia	MISC
Tornado	MISC
for	O
example	O
,	O
retain	O
a	O
very	O
basic	O
hydro-mechanical	O
backup	O
system	O
for	O
limited	O
flight	O
control	O
capability	O
on	O
losing	O
electrical	O
power	O
,	O
in	O
the	O
case	O
of	O
the	O
Tornado	MISC
this	O
allows	O
rudimentary	O
control	O
of	O
the	O
tailerons	O
only	O
for	O
pitch	O
and	O
roll	O
axis	O
movements	O
.	O
Boeing	ORGANIZATION
followed	O
with	O
their	O
777	O
and	O
later	O
designs	O
.	O
This	O
was	O
used	O
in	O
Concorde	MISC
,	O
the	O
first	O
production	O
fly-by-wire	O
airliner	O
.	O
This	O
was	O
exploited	O
by	O
the	O
early	O
versions	O
of	O
F-16	MISC
,	O
giving	O
it	O
impressive	O
maneuverability	O
.	O
Digital	O
flight	O
control	O
systems	O
enable	O
inherently	O
unstable	O
combat	O
aircraft	O
,	O
such	O
as	O
the	O
F-117	MISC
Nighthawk	MISC
and	O
the	O
B-2	MISC
Spirit	MISC
flying	O
wing	O
to	O
fly	O
in	O
usable	O
and	O
safe	O
manners	O
.	O
The	O
Boeing	ORGANIZATION
Aircraft	ORGANIZATION
Company	ORGANIZATION
and	O
Airbus	ORGANIZATION
Industries	ORGANIZATION
differ	O
in	O
their	O
approaches	O
in	O
using	O
fly-by-wire	O
systems	O
.	O
In	O
Airbus	ORGANIZATION
airliners	O
,	O
the	O
flight-envelope	O
control	O
system	O
always	O
retains	O
ultimate	O
flight	O
control	O
,	O
and	O
it	O
will	O
not	O
permit	O
the	O
pilots	O
to	O
fly	O
outside	O
these	O
performance	O
limits	O
.	O
In	O
all	O
Boeing	MISC
777	MISC
airliners	O
,	O
the	O
two	O
pilots	O
can	O
completely	O
override	O
the	O
computerized	O
flight-control	O
system	O
,	O
thus	O
permitting	O
the	O
aircraft	O
to	O
be	O
flown	O
beyond	O
its	O
usual	O
flight-control	O
envelope	O
during	O
emergencies	O
.	O
This	O
system	O
is	O
used	O
in	O
the	O
Lockheed	MISC
Martin	MISC
F-35	MISC
and	O
in	O
Airbus	MISC
A380	MISC
backup	O
flight	O
controls	O
.	O
The	O
Boeing	MISC
787	MISC
will	O
also	O
incorporate	O
some	O
electrically	O
operated	O
flight	O
controls	O
(	O
spoilers	O
and	O
horizontal	O
stabilizer	O
)	O
,	O
which	O
will	O
remain	O
operational	O
with	O
either	O
a	O
total	O
hydraulics	O
failure	O
and/or	O
flight	O
control	O
computer	O
failure	O
.	O
Several	O
demonstrations	O
were	O
made	O
on	O
a	O
flight	O
simulator	O
where	O
a	O
Cessna-trained	ORGANIZATION
small-aircraft	O
pilot	O
successfully	O
landed	O
a	O
heavily-damaged	O
full-size	O
concept	O
jet	O
,	O
without	O
prior	O
experience	O
with	O
large-body	O
jet	O
aircraft	O
.	O
