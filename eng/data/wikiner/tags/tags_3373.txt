As	O
the	O
war	O
progressed	O
,	O
illusions	O
of	O
an	O
easy	O
victory	O
were	O
smashed	O
,	O
and	O
Germans	MISC
began	O
to	O
suffer	O
tremendously	O
from	O
what	O
would	O
become	O
a	O
long	O
and	O
enormously	O
costly	O
war	O
.	O
It	O
was	O
designed	O
to	O
explore	O
allegations	O
of	O
the	O
lack	O
of	O
patriotism	O
among	O
German	MISC
Jews	MISC
,	O
but	O
the	O
results	O
of	O
the	O
census	O
disproved	O
the	O
accusations	O
and	O
were	O
not	O
made	O
public	O
.	O
A	O
number	O
of	O
German	MISC
Jews	MISC
viewed	O
the	O
Great	MISC
War	MISC
as	O
an	O
opportunity	O
to	O
prove	O
commitment	O
to	O
the	O
German	MISC
homeland	O
.	O
Krupp	ORGANIZATION
,	O
the	O
steel	O
manufacturer	O
company	O
,	O
actively	O
marketed	O
and	O
sold	O
arms	O
to	O
potential	O
combatants	O
,	O
playing	O
one	O
against	O
the	O
other	O
--	O
an	O
extremely	O
profitable	O
practice	O
.	O
After	O
Russia	LOCATION
's	O
February	MISC
Revolution	MISC
,	O
all	O
members	O
of	O
the	O
Entente	MISC
had	O
nominally	O
democratic	O
governments	O
.	O
Woodrow	PERSON
Wilson	PERSON
's	O
Fourteen	MISC
Points	MISC
were	O
particularly	O
popular	O
among	O
the	O
German	MISC
people	O
.	O
When	O
peace	O
and	O
full	O
restoration	O
were	O
promised	O
by	O
the	O
Allies	MISC
,	O
patriotic	O
enthusiasm	O
began	O
to	O
wane	O
.	O
This	O
coincided	O
with	O
the	O
gutting	O
of	O
the	O
German	MISC
military	O
leaving	O
no	O
hope	O
for	O
a	O
physical	O
response	O
.	O
The	O
non-negotiable	O
peace	O
agreed	O
to	O
by	O
Weimar	ORGANIZATION
politicians	O
in	O
the	O
Treaty	MISC
of	MISC
Versailles	MISC
was	O
certainly	O
not	O
what	O
the	O
German	MISC
peace-seeking	O
populace	O
had	O
expected	O
.	O
These	O
November	MISC
Criminals	MISC
,	O
or	O
those	O
who	O
seemed	O
to	O
benefit	O
from	O
the	O
newly	O
formed	O
Weimar	ORGANIZATION
Republic	ORGANIZATION
,	O
were	O
seen	O
to	O
have	O
"	O
stabbed	O
them	O
in	O
the	O
back	O
"	O
on	O
the	O
home	O
front	O
,	O
by	O
either	O
criticizing	O
German	MISC
nationalism	O
,	O
instigating	O
unrest	O
and	O
strikes	O
in	O
the	O
critical	O
military	O
industries	O
or	O
profiteering	O
.	O
The	O
strikes	O
were	O
seen	O
to	O
be	O
instigated	O
by	O
treasonous	O
elements	O
,	O
with	O
the	O
Jews	MISC
taking	O
most	O
of	O
the	O
blame	O
.	O
The	O
industrialization	O
of	O
war	O
had	O
dehumanized	O
the	O
process	O
,	O
and	O
made	O
possible	O
a	O
new	O
kind	O
of	O
defeat	O
which	O
the	O
Germans	MISC
suffered	O
as	O
a	O
total	O
war	O
emerged	O
.	O
On	O
October	O
30	O
the	O
Ottoman	LOCATION
Empire	LOCATION
capitulated	O
at	O
Mudros	LOCATION
.	O
Nevertheless	O
,	O
the	O
idea	O
of	O
domestic	O
betrayal	O
resonated	O
among	O
its	O
audience	O
,	O
and	O
its	O
claims	O
would	O
provide	O
some	O
basis	O
for	O
public	O
support	O
for	O
the	O
emerging	O
National	ORGANIZATION
Socialist	ORGANIZATION
Party	ORGANIZATION
,	O
under	O
an	O
autocratic	O
form	O
of	O
nationalism	O
.	O
After	O
the	O
last	O
German	MISC
offensive	O
on	O
the	O
Western	MISC
Front	MISC
failed	O
in	O
1918	O
,	O
the	O
war	O
effort	O
was	O
doomed	O
.	O
In	O
response	O
,	O
OHL	MISC
arranged	O
for	O
a	O
rapid	O
change	O
to	O
a	O
civilian	O
government	O
.	O
As	O
the	O
Kaiser	O
had	O
been	O
forced	O
to	O
abdicate	O
and	O
the	O
military	O
relinquished	O
executive	O
power	O
,	O
it	O
was	O
the	O
temporary	O
"	O
civilian	O
government	O
"	O
that	O
sued	O
for	O
peace	O
--	O
the	O
signature	O
on	O
the	O
document	O
was	O
of	O
Matthias	PERSON
Erzberger	PERSON
,	O
a	O
civilian	O
,	O
who	O
was	O
later	O
murdered	O
for	O
his	O
alleged	O
treason	O
;	O
this	O
led	O
to	O
the	O
signing	O
of	O
the	O
Treaty	MISC
of	MISC
Versailles	MISC
.	O
Ludendorff	PERSON
replied	O
with	O
his	O
list	O
of	O
excuses	O
:	O
the	O
home	O
front	O
failed	O
us	O
,	O
etc	O
.	O
The	O
phrase	O
was	O
to	O
Ludendorff	PERSON
's	O
liking	O
,	O
and	O
he	O
let	O
it	O
be	O
known	O
among	O
the	O
general	O
staff	O
that	O
this	O
was	O
the	O
'	O
official	O
'	O
version	O
,	O
then	O
disseminated	O
throughout	O
German	MISC
society	O
.	O
As	O
such	O
the	O
book	O
offered	O
one	O
of	O
the	O
earliest	O
published	O
versions	O
of	O
the	O
Stab-in-the-back	MISC
legend	MISC
.	O
(	O
Maurice	PERSON
later	O
disavowed	O
having	O
used	O
the	O
term	O
himself.	O
)	O
.	O
He	O
had	O
written	O
about	O
the	O
illegal	O
nature	O
of	O
the	O
war	O
from	O
1916	O
onward	O
,	O
and	O
he	O
also	O
had	O
a	O
large	O
hand	O
in	O
the	O
Munich	LOCATION
revolution	O
until	O
he	O
was	O
assassinated	O
in	O
February	O
1919	O
.	O
Many	O
of	O
its	O
representatives	O
such	O
as	O
Matthias	PERSON
Erzberger	PERSON
and	O
Walther	PERSON
Rathenau	PERSON
were	O
assassinated	O
,	O
and	O
the	O
leaders	O
were	O
branded	O
as	O
"	O
criminals	O
"	O
and	O
Jews	MISC
by	O
the	O
right-wing	O
press	O
dominated	O
by	O
Alfred	PERSON
Hugenberg	PERSON
.	O
The	O
Dolchstoss	MISC
was	O
a	O
central	O
image	O
in	O
propaganda	O
produced	O
by	O
the	O
many	O
right-wing	O
and	O
traditionally	O
conservative	O
political	O
parties	O
that	O
sprang	O
up	O
in	O
the	O
early	O
days	O
of	O
the	O
Weimar	ORGANIZATION
Republic	ORGANIZATION
,	O
including	O
Hitler	PERSON
's	O
NSDAP	ORGANIZATION
.	O
For	O
Hitler	PERSON
himself	O
,	O
this	O
explanatory	O
model	O
for	O
World	MISC
War	MISC
I	MISC
was	O
of	O
crucial	O
personal	O
importance	O
.	O
Ebert	PERSON
had	O
meant	O
these	O
sayings	O
as	O
a	O
tribute	O
to	O
the	O
German	ORGANIZATION
soldier	ORGANIZATION
,	O
but	O
it	O
only	O
contributed	O
to	O
the	O
prevailing	O
feeling	O
.	O
