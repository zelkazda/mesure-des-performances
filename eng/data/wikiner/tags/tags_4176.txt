Suizei	PERSON
is	O
almost	O
certainly	O
a	O
legendary	O
emperor	O
.	O
In	O
the	O
Kojiki	MISC
little	O
more	O
than	O
his	O
name	O
and	O
genealogy	O
are	O
recorded	O
.	O
An	O
Imperial	MISC
misasagi	O
or	O
tomb	O
for	O
Suizei	PERSON
is	O
currently	O
maintained	O
,	O
despite	O
the	O
lack	O
of	O
any	O
reliable	O
early	O
records	O
attesting	O
to	O
his	O
historical	O
existence	O
.	O
The	O
Kojiki	MISC
does	O
,	O
however	O
,	O
record	O
his	O
ascent	O
to	O
the	O
throne	O
.	O
The	O
Imperial	LOCATION
Household	LOCATION
Agency	LOCATION
designates	O
this	O
location	O
as	O
his	O
mausoleum	O
.	O
