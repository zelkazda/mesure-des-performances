Eritrea	LOCATION
is	O
a	O
member	O
of	O
the	O
African	ORGANIZATION
Union	ORGANIZATION
(	O
AU	ORGANIZATION
)	O
,	O
the	O
successor	O
of	O
the	O
Organization	ORGANIZATION
of	ORGANIZATION
African	ORGANIZATION
Unity	ORGANIZATION
(	O
OAU	ORGANIZATION
)	O
and	O
is	O
an	O
observing	O
member	O
of	O
the	O
Arab	ORGANIZATION
League	ORGANIZATION
.	O
By	O
supporting	O
those	O
who	O
destroy	O
peace	O
processes	O
in	O
our	O
neighbouring	O
countries	O
,	O
Norway	LOCATION
undermines	O
the	O
Ethiopian	MISC
government	O
's	O
peace	O
work	O
.	O
This	O
action	O
was	O
taken	O
after	O
a	O
long	O
period	O
of	O
increasing	O
tension	O
between	O
the	O
two	O
countries	O
due	O
to	O
a	O
series	O
of	O
cross-border	O
incidents	O
involving	O
the	O
Eritrean	ORGANIZATION
Islamic	ORGANIZATION
Jihad	ORGANIZATION
.	O
After	O
many	O
months	O
of	O
negotiations	O
with	O
the	O
Sudanese	MISC
to	O
try	O
to	O
end	O
the	O
incursions	O
,	O
the	O
Government	ORGANIZATION
of	ORGANIZATION
Eritrea	ORGANIZATION
concluded	O
that	O
the	O
NIF	ORGANIZATION
did	O
not	O
intend	O
to	O
change	O
its	O
policy	O
and	O
broke	O
relations	O
.	O
Subsequently	O
,	O
the	O
Government	ORGANIZATION
of	ORGANIZATION
Eritrea	ORGANIZATION
hosted	O
a	O
conference	O
of	O
Sudanese	MISC
opposition	O
leaders	O
in	O
June	O
1995	O
in	O
an	O
effort	O
to	O
help	O
the	O
opposition	O
unite	O
and	O
to	O
provide	O
a	O
credible	O
alternative	O
to	O
the	O
present	O
government	O
in	O
Khartoum	LOCATION
.	O
A	O
dispute	O
with	O
Yemen	LOCATION
over	O
the	O
Hanish	LOCATION
Islands	LOCATION
in	O
1996	O
resulted	O
in	O
a	O
brief	O
war	O
.	O
As	O
a	O
result	O
,	O
the	O
United	MISC
Nations	MISC
Mission	MISC
in	MISC
Ethiopia	MISC
and	MISC
Eritrea	MISC
(	O
UNMEE	MISC
)	O
is	O
occupying	O
a	O
25	O
km	O
by	O
900	O
km	O
area	O
on	O
the	O
border	O
to	O
help	O
stabilize	O
the	O
region	O
.	O
Central	O
to	O
the	O
continuation	O
of	O
the	O
stalemate	O
is	O
Ethiopia	LOCATION
's	O
failure	O
to	O
abide	O
by	O
the	O
border	O
delimitation	O
ruling	O
and	O
reneging	O
on	O
its	O
commitment	O
to	O
demarcation	O
.	O
The	O
stalemate	O
has	O
led	O
the	O
President	O
of	O
Eritrea	ORGANIZATION
to	O
urge	O
the	O
UN	ORGANIZATION
to	O
take	O
action	O
on	O
Ethiopia	LOCATION
.	O
