The	O
CUC	ORGANIZATION
is	O
a	O
member	O
of	O
the	O
International	ORGANIZATION
Council	ORGANIZATION
of	ORGANIZATION
Unitarians	ORGANIZATION
and	ORGANIZATION
Universalists	ORGANIZATION
.	O
We	O
,	O
the	O
member	O
congregations	O
of	O
the	O
Canadian	ORGANIZATION
Unitarian	ORGANIZATION
Council	ORGANIZATION
,	O
covenant	O
to	O
affirm	O
and	O
promote	O
:	O
The	O
CUC	ORGANIZATION
has	O
created	O
a	O
task	O
force	O
to	O
consider	O
revising	O
them	O
.	O
The	O
CUC	ORGANIZATION
is	O
divided	O
into	O
four	O
regions	O
:	O
"	O
BC	LOCATION
"	O
(	O
British	LOCATION
Columbia	LOCATION
)	O
,	O
"	O
Western	O
"	O
(	O
Alberta	LOCATION
to	O
Thunder	LOCATION
Bay	LOCATION
)	O
,	O
"	O
Central	O
"	O
(	O
between	O
Thunder	LOCATION
Bay	LOCATION
and	O
Kingston	LOCATION
)	O
,	O
and	O
"	O
Eastern	O
"	O
(	O
Kingston	LOCATION
,	O
Ottawa	LOCATION
and	O
everything	O
east	O
of	O
that	O
)	O
.	O
Up	O
until	O
July	O
2002	O
,	O
almost	O
all	O
member	O
congregations	O
of	O
the	O
CUC	ORGANIZATION
were	O
also	O
members	O
of	O
the	O
Unitarian	ORGANIZATION
Universalist	ORGANIZATION
Association	ORGANIZATION
(	O
UUA	ORGANIZATION
)	O
.	O
In	O
the	O
past	O
,	O
most	O
services	O
to	O
CUC	ORGANIZATION
member	O
congregations	O
were	O
provided	O
by	O
the	O
UUA	ORGANIZATION
.	O
However	O
,	O
with	O
an	O
agreement	O
in	O
2001	O
between	O
the	O
UUA	ORGANIZATION
and	O
CUC	ORGANIZATION
,	O
from	O
July	O
2002	O
onwards	O
most	O
services	O
have	O
been	O
provided	O
by	O
the	O
CUC	ORGANIZATION
to	O
its	O
own	O
member	O
congregations	O
.	O
The	O
UUA	ORGANIZATION
continues	O
to	O
provide	O
services	O
relating	O
to	O
ministerial	O
settlement	O
as	O
well	O
as	O
a	O
very	O
small	O
amount	O
of	O
the	O
youth	O
(	O
14	O
--	O
20	O
)	O
and	O
young	O
adult	O
(	O
18	O
--	O
35	O
)	O
programming	O
and	O
services	O
.	O
Changing	O
the	O
name	O
of	O
the	O
CUC	ORGANIZATION
has	O
occasionally	O
been	O
debated	O
,	O
but	O
there	O
have	O
been	O
no	O
successful	O
motions	O
.	O
Note	O
,	O
not	O
all	O
CUC	ORGANIZATION
members	O
like	O
this	O
playful	O
reading	O
and	O
so	O
when	O
these	O
people	O
write	O
the	O
abbreviation	O
they	O
leave	O
out	O
the	O
star	O
(	O
*	O
)	O
,	O
just	O
writing	O
UU	O
instead	O
.	O
