They	O
are	O
members	O
of	O
the	O
North	ORGANIZATION
Division	ORGANIZATION
of	O
the	O
National	ORGANIZATION
Football	ORGANIZATION
Conference	ORGANIZATION
(	O
NFC	ORGANIZATION
)	O
in	O
the	O
National	ORGANIZATION
Football	ORGANIZATION
League	ORGANIZATION
(	O
NFL	ORGANIZATION
)	O
,	O
and	O
play	O
their	O
home	O
games	O
at	O
Ford	LOCATION
Field	LOCATION
in	O
Downtown	LOCATION
Detroit	LOCATION
.	O
Despite	O
success	O
within	O
the	O
NFL	ORGANIZATION
,	O
they	O
could	O
not	O
survive	O
in	O
Portsmouth	LOCATION
,	O
then	O
the	O
NFL	ORGANIZATION
's	O
smallest	O
city	O
.	O
The	O
team	O
was	O
purchased	O
and	O
moved	O
to	O
Detroit	ORGANIZATION
for	O
the	O
1934	O
season	O
.	O
The	O
Lions	ORGANIZATION
are	O
one	O
of	O
four	O
current	O
NFL	ORGANIZATION
teams	O
that	O
have	O
yet	O
to	O
qualify	O
for	O
the	O
Super	ORGANIZATION
Bowl	ORGANIZATION
.	O
The	O
Lions	ORGANIZATION
hold	O
the	O
second	O
longest	O
regular	O
season	O
losing	O
streak	O
in	O
NFL	ORGANIZATION
history	O
;	O
losing	O
19	O
straight	O
games	O
from	O
the	O
final	O
week	O
of	O
the	O
2007	O
season	O
and	O
ending	O
on	O
September	O
27	O
,	O
2009	O
,	O
when	O
the	O
Lions	ORGANIZATION
defeated	O
the	O
Washington	ORGANIZATION
Redskins	ORGANIZATION
19	O
--	O
14	O
.	O
It	O
is	O
second	O
only	O
to	O
the	O
1976	O
--	O
77	O
Tampa	ORGANIZATION
Bay	ORGANIZATION
Buccaneers	ORGANIZATION
'	O
losing	O
streak	O
of	O
26	O
.	O
Also	O
since	O
the	O
NFL	ORGANIZATION
's	O
expansion	O
to	O
32	O
teams	O
in	O
2002	O
the	O
Lions	ORGANIZATION
are	O
the	O
only	O
NFC	ORGANIZATION
team	O
to	O
not	O
make	O
the	O
playoffs	O
.	O
The	O
2008	MISC
Detroit	MISC
Lions	MISC
became	O
the	O
only	O
team	O
in	O
NFL	ORGANIZATION
history	O
to	O
lose	O
all	O
16	O
regular-season	O
games	O
.	O
They	O
are	O
only	O
the	O
second	O
team	O
to	O
go	O
winless	O
without	O
a	O
tie	O
since	O
the	O
AFL-NFL	ORGANIZATION
merger	ORGANIZATION
in	O
1970	O
.	O
The	O
Lions	ORGANIZATION
currently	O
hold	O
a	O
20	O
game	O
road	O
losing	O
streak	O
,	O
the	O
4th	O
longest	O
in	O
NFL	ORGANIZATION
history	O
.	O
Aside	O
from	O
a	O
brief	O
change	O
to	O
maroon	O
in	O
1948	O
instituted	O
by	O
then	O
head	O
coach	O
Bo	PERSON
McMillin	PERSON
(	O
influenced	O
by	O
his	O
years	O
as	O
coach	O
at	O
Indiana	ORGANIZATION
)	O
,	O
the	O
Lions	ORGANIZATION
uniforms	O
have	O
basically	O
remained	O
the	O
same	O
since	O
the	O
team	O
debuted	O
in	O
1930	O
.	O
In	O
1994	O
,	O
every	O
NFL	ORGANIZATION
team	O
wore	O
'	O
throwback	O
'	O
jerseys	O
,	O
and	O
the	O
Lions	ORGANIZATION
'	O
were	O
similar	O
to	O
the	O
jerseys	O
used	O
during	O
their	O
1935	O
championship	O
season	O
.	O
The	O
Lions	ORGANIZATION
officially	O
unveiled	O
new	O
logo	O
designs	O
and	O
uniforms	O
on	O
April	O
20	O
,	O
2009	O
.	O
The	O
Lion	ORGANIZATION
on	O
the	O
helmet	O
now	O
has	O
a	O
flowing	O
mane	O
and	O
fangs	O
,	O
and	O
the	O
font	O
of	O
"	O
Lions	ORGANIZATION
"	O
is	O
more	O
modern	O
.	O
The	O
Lions	ORGANIZATION
'	O
flagship	O
radio	O
stations	O
are	O
WXYT-FM	ORGANIZATION
,	O
97.1	ORGANIZATION
FM	ORGANIZATION
,	O
and	O
WXYT-AM	ORGANIZATION
,	O
1270	ORGANIZATION
AM	ORGANIZATION
.	O
Dan	PERSON
Miller	PERSON
does	O
play-by-play	O
,	O
Jim	PERSON
Brandstatter	PERSON
does	O
color	O
commentary	O
,	O
and	O
Tony	PERSON
Ortiz	PERSON
provides	O
sideline	O
reports	O
.	O
If	O
a	O
conflict	O
with	O
Detroit	ORGANIZATION
Tigers	ORGANIZATION
or	O
Detroit	ORGANIZATION
Red	ORGANIZATION
Wings	ORGANIZATION
coverage	O
arises	O
,	O
only	O
WXYT-FM	ORGANIZATION
serves	O
as	O
the	O
Lions	ORGANIZATION
'	O
flagship	O
.	O
Since	O
2008	O
,	O
WWJ-TV	ORGANIZATION
has	O
been	O
the	O
flagship	O
television	O
station	O
for	O
Lions	ORGANIZATION
pre-season	O
games	O
.	O
The	O
announcers	O
are	O
Matt	PERSON
Shepard	PERSON
on	O
play-by-play	O
and	O
Desmond	PERSON
Howard	PERSON
with	O
color	O
commentary	O
.	O
For	O
regular	O
season	O
games	O
vs	O
NFC	ORGANIZATION
opponents	O
when	O
Fox	MISC
does	O
n't	O
have	O
a	O
double	O
header	O
,	O
WJBK	ORGANIZATION
produces	O
a	O
live	O
postgame	O
show	O
.	O
The	O
Lions	ORGANIZATION
'	O
winless	O
performance	O
in	O
2008	O
led	O
to	O
several	O
local	O
broadcast	O
blackouts	O
,	O
as	O
local	O
fans	O
did	O
not	O
purchase	O
enough	O
tickets	O
by	O
the	O
72	O
hour	O
blackout	O
deadline	O
.	O
The	O
first	O
blackout	O
in	O
the	O
7	O
year	O
history	O
of	O
Ford	LOCATION
Field	LOCATION
was	O
the	O
October	O
26	O
,	O
2008	O
game	O
vs	O
the	O
Washington	MISC
Redskins	MISC
.	O
The	O
second	O
home	O
game	O
of	O
the	O
2009	O
season	O
in	O
which	O
the	O
Lions	ORGANIZATION
broke	O
the	O
losing	O
streak	O
(	O
also	O
against	O
the	O
Washington	ORGANIZATION
Redskins	ORGANIZATION
)	O
was	O
blacked	O
out	O
locally	O
,	O
as	O
well	O
as	O
the	O
come	O
from	O
behind	O
victory	O
over	O
the	O
Cleveland	ORGANIZATION
Browns	ORGANIZATION
.	O
Games	O
were	O
also	O
often	O
blacked	O
out	O
at	O
the	O
Lions	ORGANIZATION
'	O
previous	O
home	O
the	O
80,000	O
seat	O
Pontiac	LOCATION
Silverdome	LOCATION
,	O
despite	O
the	O
success	O
and	O
popularity	O
of	O
Barry	PERSON
Sanders	PERSON
.	O
