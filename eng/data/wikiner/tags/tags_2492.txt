Other	O
regions	O
of	O
the	O
world	O
,	O
including	O
India	LOCATION
and	O
China	LOCATION
,	O
also	O
have	O
early	O
evidence	O
of	O
water	O
clocks	O
,	O
but	O
the	O
earliest	O
dates	O
are	O
less	O
certain	O
.	O
The	O
Greek	MISC
and	O
Roman	MISC
civilizations	O
are	O
credited	O
for	O
initially	O
advancing	O
water	O
clock	O
design	O
to	O
include	O
complex	O
gearing	O
,	O
which	O
was	O
connected	O
to	O
fanciful	O
automata	O
and	O
also	O
resulted	O
in	O
improved	O
accuracy	O
.	O
These	O
advances	O
were	O
passed	O
on	O
through	O
Byzantium	LOCATION
and	O
Islamic	MISC
times	O
,	O
eventually	O
making	O
their	O
way	O
to	O
Europe	LOCATION
.	O
While	O
never	O
reaching	O
the	O
level	O
of	O
accuracy	O
of	O
a	O
modern	O
timepiece	O
,	O
the	O
water	O
clock	O
was	O
the	O
most	O
accurate	O
and	O
commonly	O
used	O
timekeeping	O
device	O
for	O
millennia	O
,	O
until	O
it	O
was	O
replaced	O
by	O
the	O
more	O
accurate	O
pendulum	O
clock	O
in	O
17th	O
century	O
Europe	LOCATION
.	O
None	O
of	O
the	O
first	O
clocks	O
survived	O
from	O
13th	O
century	O
Europe	LOCATION
,	O
but	O
various	O
mentions	O
in	O
church	O
records	O
reveal	O
some	O
of	O
the	O
early	O
history	O
of	O
the	O
clock	O
.	O
The	O
word	O
horologia	O
(	O
from	O
the	O
Greek	MISC
ὡρα	O
,	O
hour	O
,	O
and	O
λεγειν	O
,	O
to	O
tell	O
)	O
was	O
used	O
to	O
describe	O
all	O
these	O
devices	O
,	O
but	O
the	O
use	O
of	O
this	O
word	O
for	O
all	O
timekeepers	O
conceals	O
from	O
us	O
the	O
true	O
nature	O
of	O
the	O
mechanisms	O
.	O
For	O
example	O
,	O
there	O
is	O
a	O
record	O
that	O
in	O
1176	O
Sens	LOCATION
Cathedral	LOCATION
installed	O
a	O
'	O
horologe	O
'	O
but	O
the	O
mechanism	O
used	O
is	O
unknown	O
.	O
The	O
word	O
clock	O
,	O
which	O
gradually	O
supersedes	O
"	O
horologe	O
"	O
,	O
suggests	O
that	O
it	O
was	O
the	O
sound	O
of	O
bells	O
which	O
also	O
characterized	O
the	O
prototype	O
mechanical	O
clocks	O
that	O
appeared	O
during	O
the	O
13th	O
century	O
in	O
Europe	LOCATION
.	O
Canonical	MISC
hours	MISC
varied	O
in	O
length	O
as	O
the	O
times	O
of	O
sunrise	O
and	O
sunset	O
shifted	O
.	O
In	O
1283	O
,	O
a	O
large	O
clock	O
was	O
installed	O
at	O
Dunstable	LOCATION
Priory	LOCATION
;	O
its	O
location	O
above	O
the	O
rood	O
screen	O
suggests	O
that	O
it	O
was	O
not	O
a	O
water	O
clock	O
[	O
citation	O
needed	O
]	O
.	O
In	O
1292	O
,	O
Canterbury	LOCATION
Cathedral	LOCATION
installed	O
a	O
'	O
great	O
horloge	O
'	O
.	O
In	O
1322	O
,	O
a	O
new	O
clock	O
was	O
installed	O
in	O
Norwich	LOCATION
,	O
an	O
expensive	O
replacement	O
for	O
an	O
earlier	O
clock	O
installed	O
in	O
1273	O
.	O
Wallingford	PERSON
's	O
clock	O
had	O
a	O
large	O
astrolabe-type	O
dial	O
,	O
showing	O
the	O
sun	O
,	O
the	O
moon	O
's	O
age	O
,	O
phase	O
,	O
and	O
node	O
,	O
a	O
star	O
map	O
,	O
and	O
possibly	O
the	O
planets	O
.	O
In	O
addition	O
,	O
it	O
had	O
a	O
wheel	O
of	O
fortune	O
and	O
an	O
indicator	O
of	O
the	O
state	O
of	O
the	O
tide	O
at	O
London	LOCATION
Bridge	LOCATION
.	O
Spring-driven	O
clocks	O
appeared	O
during	O
the	O
1400s	O
,	O
although	O
they	O
are	O
often	O
erroneously	O
credited	O
to	O
Nürnberg	LOCATION
watchmaker	O
Peter	PERSON
Henlein	PERSON
around	O
1511	O
.	O
The	O
cross-beat	O
escapement	O
was	O
invented	O
in	O
1584	O
by	O
Jost	PERSON
Bürgi	PERSON
,	O
who	O
also	O
developed	O
the	O
remontoire	O
.	O
These	O
clocks	O
helped	O
the	O
16th-century	O
astronomer	O
Tycho	PERSON
Brahe	PERSON
to	O
observe	O
astronomical	O
events	O
with	O
much	O
greater	O
precision	O
than	O
before	O
.	O
Galileo	PERSON
had	O
the	O
idea	O
to	O
use	O
a	O
swinging	O
bob	O
to	O
regulate	O
the	O
motion	O
of	O
a	O
time	O
telling	O
device	O
earlier	O
in	O
the	O
17th	O
century	O
.	O
Christiaan	PERSON
Huygens	PERSON
,	O
however	O
,	O
is	O
usually	O
credited	O
as	O
the	O
inventor	O
.	O
The	O
reward	O
was	O
eventually	O
claimed	O
in	O
1761	O
by	O
John	PERSON
Harrison	PERSON
,	O
who	O
dedicated	O
his	O
life	O
to	O
improving	O
the	O
accuracy	O
of	O
his	O
clocks	O
.	O
On	O
November	O
17	O
,	O
1797	O
,	O
Eli	PERSON
Terry	PERSON
received	O
his	O
first	O
patent	O
for	O
a	O
clock	O
.	O
The	O
sound	O
is	O
either	O
spoken	O
natural	O
language	O
,	O
(	O
e.g.	O
"	O
The	O
time	O
is	O
twelve	O
thirty-five	O
"	O
)	O
,	O
or	O
as	O
auditory	O
codes	O
(	O
e.g.	O
number	O
of	O
sequential	O
bell	O
rings	O
on	O
the	O
hour	O
represents	O
the	O
number	O
of	O
the	O
hour	O
like	O
the	O
bell	O
Big	LOCATION
Ben	LOCATION
)	O
.	O
John	PERSON
Harrison	PERSON
created	O
the	O
first	O
highly	O
accurate	O
marine	O
chronometer	O
in	O
the	O
mid-18th	O
century	O
.	O
The	O
Noon	LOCATION
gun	LOCATION
in	O
Cape	LOCATION
Town	LOCATION
still	O
fires	O
an	O
accurate	O
signal	O
to	O
allow	O
ships	O
to	O
check	O
their	O
chronometers	O
.	O
