Aedile	O
was	O
an	O
office	O
of	O
the	O
Roman	LOCATION
Republic	LOCATION
.	O
Based	O
in	O
Rome	LOCATION
,	O
the	O
aediles	O
were	O
responsible	O
for	O
maintenance	O
of	O
public	O
buildings	O
and	O
regulation	O
of	O
public	O
festivals	O
.	O
According	O
to	O
Livy	PERSON
(	O
vi	O
.	O
These	O
extravagant	O
expenditures	O
began	O
shortly	O
after	O
the	O
end	O
of	O
Second	MISC
Punic	MISC
War	MISC
,	O
and	O
increased	O
as	O
the	O
spoils	O
returned	O
from	O
Rome	LOCATION
's	O
new	O
eastern	O
conquests	O
.	O
Cicero	PERSON
(	O
Legg.	MISC
iii	O
.	O
Under	O
Augustus	PERSON
the	O
office	O
lost	O
much	O
of	O
its	O
importance	O
,	O
its	O
juidical	O
functions	O
and	O
the	O
care	O
of	O
the	O
games	O
being	O
transferred	O
to	O
the	O
praetor	O
,	O
while	O
its	O
city	O
responsibilities	O
were	O
limited	O
by	O
the	O
appointment	O
of	O
a	O
praefectus	O
urbi	O
.	O
Today	O
in	O
Portugal	LOCATION
the	O
county	O
mayor	O
can	O
still	O
be	O
referred	O
to	O
as	O
'	O
edil	O
'	O
(	O
e.g.	O
'	O
O	O
edil	O
de	O
Coimbra	LOCATION
'	O
,	O
meaning	O
'	O
the	O
mayor	O
of	O
Coimbra	LOCATION
'	O
)	O
.	O
