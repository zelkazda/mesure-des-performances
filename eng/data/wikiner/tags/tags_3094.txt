During	O
the	O
American	MISC
Civil	MISC
War	MISC
of	O
1861-1865	O
,	O
some	O
soldiers	O
pinned	O
paper	O
notes	O
with	O
their	O
name	O
and	O
home	O
address	O
to	O
the	O
backs	O
of	O
their	O
coats	O
.	O
In	O
the	O
Spanish-American	MISC
War	MISC
,	O
soldiers	O
purchased	O
crude	O
stamped	O
identification	O
tags	O
,	O
sometimes	O
with	O
misleading	O
information	O
.	O
The	O
purpose	O
and	O
use	O
of	O
the	O
notch	O
was	O
verified	O
by	O
a	O
Snopes	ORGANIZATION
article	O
,	O
in	O
which	O
Snopes	ORGANIZATION
consulted	O
with	O
US	ORGANIZATION
Army	ORGANIZATION
Mortuary	ORGANIZATION
Affairs	ORGANIZATION
.	O
Canadian	ORGANIZATION
Forces	ORGANIZATION
identity	O
discs	O
(	O
abbreviated	O
"	O
I	O
discs	O
"	O
)	O
are	O
designed	O
to	O
be	O
broken	O
in	O
two	O
in	O
the	O
case	O
of	O
fatality	O
;	O
the	O
lower	O
half	O
is	O
returned	O
to	O
National	MISC
Defence	MISC
Headquarters	MISC
with	O
the	O
member	O
's	O
personal	O
documents	O
,	O
while	O
the	O
upper	O
half	O
remains	O
on	O
the	O
body	O
.	O
Danish	MISC
dog	O
tags	O
are	O
a	O
small	O
metallic	O
plate	O
,	O
designed	O
to	O
be	O
broken	O
in	O
two	O
.	O
German	O
Bundeswehr	ORGANIZATION
ID	O
tags	O
are	O
an	O
oval-shaped	O
disc	O
designed	O
to	O
be	O
broken	O
in	O
half	O
.	O
There	O
are	O
two	O
variations	O
of	O
the	O
Hungarian	MISC
army	O
dog	O
tags	O
.	O
The	O
other	O
type	O
is	O
similar	O
to	O
the	O
United	ORGANIZATION
States	ORGANIZATION
Army	ORGANIZATION
dog	O
tags	O
.	O
Israeli	MISC
dog	O
tags	O
are	O
designed	O
to	O
be	O
broken	O
in	O
two	O
.	O
In	O
case	O
of	O
capture	O
,	O
Israeli	ORGANIZATION
soldiers	ORGANIZATION
are	O
instructed	O
to	O
provide	O
the	O
information	O
that	O
appears	O
on	O
the	O
dog	O
tag	O
and	O
their	O
rank	O
only	O
.	O
Malaysian	ORGANIZATION
Armed	ORGANIZATION
Forces	ORGANIZATION
have	O
two	O
identical	O
oval	O
tags	O
with	O
this	O
information	O
:	O
The	O
Singapore	ORGANIZATION
Armed	ORGANIZATION
Forces-issued	ORGANIZATION
dog	O
tags	O
are	O
inscribed	O
(	O
not	O
embossed	O
)	O
with	O
up	O
to	O
four	O
items	O
:	O
Swiss	ORGANIZATION
Armed	ORGANIZATION
Forces	ORGANIZATION
ID	O
tags	O
are	O
oval-shaped	O
and	O
are	O
designed	O
to	O
be	O
broken	O
in	O
two	O
.	O
