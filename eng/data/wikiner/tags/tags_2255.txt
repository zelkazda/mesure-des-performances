It	O
is	O
interesting	O
to	O
note	O
the	O
historical	O
parallel	O
between	O
musical	O
styles	O
and	O
the	O
style	O
of	O
"	O
sound	O
aesthetic	O
"	O
of	O
the	O
musical	O
instruments	O
used	O
,	O
for	O
example	O
:	O
Robert	PERSON
de	PERSON
Visée	PERSON
played	O
on	O
a	O
baroque	O
guitar	O
with	O
a	O
very	O
different	O
sound	O
aesthetic	O
than	O
the	O
guitars	O
used	O
by	O
Mauro	PERSON
Giuliani	PERSON
and	O
Legnani	PERSON
--	O
they	O
used	O
19th	O
century	O
guitars	O
.	O
As	O
an	O
example	O
:	O
It	O
is	O
impossible	O
to	O
play	O
a	O
historically	O
informed	O
de	PERSON
Visee	PERSON
or	O
Corbetta	PERSON
(	O
baroque	O
guitarist-composers	O
)	O
on	O
a	O
modern	O
classical	O
guitar	O
.	O
Guitar	O
like	O
instruments	O
appear	O
in	O
ancient	O
carvings	O
and	O
statues	O
recovered	O
from	O
the	O
old	O
Iranian	MISC
capital	O
of	O
Susa	LOCATION
.	O
The	O
baroque	O
guitar	O
quickly	O
superseded	O
the	O
vihuela	O
in	O
popularity	O
and	O
Italy	LOCATION
became	O
the	O
center	O
of	O
the	O
guitar	O
world	O
.	O
Some	O
modern	O
guitarists	O
,	O
such	O
as	O
Štěpán	PERSON
Rak	PERSON
,	O
use	O
the	O
little	O
finger	O
independently	O
,	O
compensating	O
for	O
the	O
little	O
finger	O
's	O
shortness	O
by	O
maintaining	O
an	O
extremely	O
long	O
fingernail	O
.	O
John	PERSON
Williams	PERSON
has	O
remarked	O
that	O
since	O
guitarists	O
find	O
it	O
superficially	O
very	O
easy	O
to	O
play	O
even	O
things	O
such	O
as	O
melody	O
with	O
accompaniment	O
(	O
e.g.	O
Giuliani	PERSON
)	O
,	O
[	O
some	O
guitarists	O
'	O
]	O
"	O
approach	O
to	O
tone	O
production	O
is	O
also	O
superficial	O
,	O
with	O
little	O
or	O
no	O
consideration	O
given	O
to	O
voice	O
matching	O
and	O
tonal	O
contrasts	O
"	O
.	O
A	O
guitar	O
recital	O
may	O
include	O
a	O
variety	O
of	O
works	O
,	O
e.g.	O
works	O
written	O
originally	O
for	O
the	O
lute	O
or	O
vihuela	O
by	O
composers	O
such	O
as	O
John	PERSON
Dowland	PERSON
(	O
b	O
.	O
Spain	O
c.	O
1500	O
)	O
,	O
and	O
also	O
music	O
written	O
for	O
the	O
harpsichord	O
by	O
Domenico	PERSON
Scarlatti	PERSON
(	O
b	O
.	O
Italy	LOCATION
1685	O
)	O
,	O
for	O
the	O
baroque	O
lute	O
by	O
Sylvius	PERSON
Leopold	PERSON
Weiss	PERSON
(	O
b	O
.	O
Spain	O
1860	O
)	O
and	O
Enrique	PERSON
Granados	PERSON
(	O
b	O
.	O
The	O
most	O
important	O
composer	O
who	O
did	O
not	O
write	O
for	O
the	O
guitar	O
but	O
whose	O
music	O
is	O
often	O
played	O
on	O
guitar	O
is	O
Johann	PERSON
Sebastian	PERSON
Bach	PERSON
(	O
b	O
.	O
Of	O
the	O
music	O
written	O
originally	O
for	O
guitar	O
the	O
earliest	O
important	O
composers	O
are	O
from	O
the	O
classical	O
period	O
and	O
include	O
Fernando	PERSON
Sor	PERSON
(	O
b	O
.	O
Spain	O
1778	O
)	O
and	O
Mauro	PERSON
Giuliani	PERSON
(	O
b	O
.	O
In	O
the	O
nineteenth	O
century	O
guitar	O
composers	O
such	O
as	O
Johann	PERSON
Kaspar	PERSON
Mertz	PERSON
(	O
b	O
.	O
Francisco	PERSON
Tárrega	PERSON
(	O
b	O
.	O
The	O
aforementioned	O
piano	O
composers	O
Albéniz	PERSON
and	O
Granados	PERSON
were	O
central	O
to	O
this	O
movement	O
and	O
their	O
evocation	O
of	O
the	O
guitar	O
was	O
so	O
successful	O
that	O
guitarists	O
have	O
largely	O
appropriated	O
their	O
music	O
for	O
piano	O
to	O
the	O
guitar	O
.	O
With	O
the	O
twentieth	O
century	O
and	O
the	O
wide-ranging	O
performances	O
of	O
artists	O
such	O
as	O
Andrés	PERSON
Segovia	PERSON
and	O
Agustin	PERSON
Barrios-Mangore	PERSON
the	O
guitar	O
began	O
to	O
regain	O
some	O
of	O
the	O
popularity	O
it	O
had	O
lost	O
to	O
the	O
harpsichord	O
and	O
piano	O
in	O
the	O
eighteenth	O
century	O
.	O
The	O
classical	O
guitar	O
also	O
became	O
widely	O
used	O
in	O
popular	O
music	O
and	O
rock	O
&	O
roll	O
in	O
the	O
1960s	O
after	O
guitarist	O
Mason	PERSON
Williams	PERSON
popularized	O
the	O
instrument	O
in	O
his	O
instrumental	O
hit	O
Classical	MISC
Gas	MISC
.	O
The	O
scale	O
length	O
has	O
remained	O
quite	O
consistent	O
since	O
it	O
was	O
chosen	O
by	O
the	O
originator	O
of	O
the	O
instrument	O
,	O
Antonio	PERSON
de	PERSON
Torres	PERSON
.	O
