It	O
has	O
2,258	O
kilometers	O
of	O
boundaries	O
,	O
shared	O
with	O
Austria	LOCATION
to	O
the	O
west	O
,	O
Serbia	LOCATION
,	O
Croatia	LOCATION
and	O
Slovenia	LOCATION
to	O
the	O
south	O
and	O
southwest	O
,	O
Romania	LOCATION
to	O
the	O
southeast	O
,	O
the	O
Ukraine	LOCATION
to	O
the	O
northeast	O
,	O
and	O
Slovakia	LOCATION
to	O
the	O
north	O
.	O
After	O
World	MISC
War	MISC
II	MISC
,	O
the	O
Trianon	MISC
boundaries	O
were	O
restored	O
with	O
a	O
small	O
revision	O
that	O
benefited	O
Czechoslovakia	LOCATION
.	O
The	O
highest	O
point	O
in	O
the	O
country	O
is	O
Kékes	LOCATION
(	O
1,014	O
m	O
)	O
in	O
the	O
Mátra	LOCATION
Mountains	LOCATION
northeast	O
of	O
Budapest	LOCATION
.	O
The	O
major	O
rivers	O
in	O
the	O
country	O
are	O
the	O
Danube	LOCATION
and	O
Tisza	LOCATION
.	O
The	O
Tisza	LOCATION
River	LOCATION
is	O
navigable	O
for	O
444	O
kilometers	O
in	O
the	O
country	O
.	O
Lake	LOCATION
Balaton	LOCATION
,	O
the	O
largest	O
,	O
is	O
78	O
kilometers	O
long	O
and	O
from	O
3	O
to	O
14	O
kilometers	O
wide	O
,	O
with	O
an	O
area	O
of	O
592	O
square	O
kilometers	O
.	O
Hungarians	MISC
often	O
refer	O
to	O
it	O
as	O
the	O
Hungarian	LOCATION
Sea	LOCATION
.	O
The	O
Transdanubia	LOCATION
region	O
lies	O
in	O
the	O
western	O
part	O
of	O
the	O
country	O
,	O
bounded	O
by	O
the	O
Danube	LOCATION
River	LOCATION
,	O
the	O
Drava	LOCATION
River	LOCATION
,	O
and	O
the	O
remainder	O
of	O
the	O
country	O
's	O
border	O
with	O
Slovenia	LOCATION
and	O
Croatia	LOCATION
.	O
It	O
lies	O
south	O
and	O
west	O
of	O
the	O
course	O
of	O
the	O
Danube	LOCATION
.	O
It	O
contains	O
Lake	LOCATION
Fertő	LOCATION
and	O
Lake	LOCATION
Balaton	LOCATION
.	O
Transdanubia	LOCATION
is	O
primarily	O
an	O
agricultural	O
area	O
,	O
with	O
flourishing	O
crops	O
,	O
livestock	O
,	O
and	O
viticulture	O
.	O
Mineral	O
deposits	O
and	O
oil	O
are	O
found	O
in	O
Zala	LOCATION
county	O
close	O
to	O
the	O
border	O
of	O
Croatia	LOCATION
.	O
The	O
Great	LOCATION
Alföld	LOCATION
contains	O
the	O
basin	O
of	O
the	O
Tisza	LOCATION
River	LOCATION
and	O
its	O
branches	O
.	O
Hungarians	MISC
have	O
inhabited	O
the	O
Great	LOCATION
Plain	LOCATION
for	O
at	O
least	O
a	O
millennium	O
.	O
In	O
earlier	O
centuries	O
,	O
the	O
Great	LOCATION
Plain	LOCATION
was	O
unsuitable	O
for	O
farming	O
because	O
of	O
frequent	O
flooding	O
.	O
In	O
the	O
last	O
half	O
of	O
the	O
nineteenth	O
century	O
,	O
the	O
government	O
sponsored	O
programs	O
to	O
control	O
the	O
riverways	O
and	O
expedite	O
inland	O
drainage	O
in	O
the	O
Great	LOCATION
Plain	LOCATION
.	O
Although	O
the	O
majority	O
of	O
the	O
country	O
has	O
an	O
elevation	O
lesser	O
than	O
300	O
metres	O
,	O
Hungary	LOCATION
has	O
several	O
moderately	O
high	O
ranges	O
of	O
mountains	O
.	O
They	O
can	O
be	O
classified	O
to	O
four	O
geographic	O
regions	O
,	O
from	O
west	O
to	O
east	O
:	O
Alpokalja	LOCATION
,	O
Transdanubian	LOCATION
Medium	LOCATION
Mountains	LOCATION
,	O
Mecsek	LOCATION
and	O
Northern	LOCATION
Medium	LOCATION
Mountains	LOCATION
.	O
Alpokalja	LOCATION
(	O
literally	O
the	O
foothills	O
of	O
the	O
Alps	LOCATION
)	O
is	O
located	O
along	O
the	O
Austrian	MISC
border	O
;	O
its	O
highest	O
point	O
is	O
Írott-kő	LOCATION
with	O
an	O
elevation	O
of	O
882	O
metres	O
.	O
The	O
Transdanubian	LOCATION
Medium	LOCATION
Mountains	LOCATION
stretch	O
from	O
the	O
west	O
part	O
of	O
Lake	LOCATION
Balaton	LOCATION
to	O
the	O
Danube	LOCATION
Bend	LOCATION
near	O
Budapest	LOCATION
,	O
where	O
it	O
meets	O
the	O
Northern	LOCATION
Medium	LOCATION
Mountains	LOCATION
(	O
or	O
Northern	LOCATION
Hills	LOCATION
)	O
.	O
Its	O
tallest	O
peak	O
is	O
the	O
757	O
m	O
high	O
Pilis	LOCATION
.	O
The	O
Northern	LOCATION
Medium	LOCATION
Mountains	LOCATION
lie	O
north	O
of	O
Budapest	LOCATION
and	O
run	O
in	O
a	O
northeasterly	O
direction	O
south	O
of	O
the	O
border	O
with	O
Slovakia	LOCATION
.	O
The	O
highest	O
peak	O
of	O
it	O
is	O
the	O
Kékes	LOCATION
,	O
located	O
in	O
the	O
Mátra	LOCATION
mountain	O
range	O
.	O
Weather	O
conditions	O
in	O
the	O
Great	LOCATION
Plain	LOCATION
can	O
be	O
especially	O
harsh	O
,	O
with	O
hot	O
summers	O
,	O
cold	O
winters	O
,	O
and	O
scant	O
rainfall	O
.	O
The	O
most	O
important	O
agricultural	O
zones	O
are	O
the	O
Little	LOCATION
Hungarian	LOCATION
Plain	LOCATION
(	O
it	O
has	O
the	O
highest	O
quality	O
fertile	O
soil	O
in	O
average	O
)	O
,	O
Transdanubia	LOCATION
,	O
and	O
the	O
Great	LOCATION
Hungarian	LOCATION
Plain	LOCATION
.	O
Puszta	LOCATION
is	O
exploited	O
by	O
sheep	O
and	O
cattle	O
raising	O
.	O
The	O
Hungarian	MISC
word	O
is	O
bor	O
.	O
The	O
arriving	O
Hungarians	MISC
took	O
over	O
the	O
practice	O
and	O
maintained	O
it	O
ever	O
since	O
.	O
The	O
majority	O
of	O
the	O
country	O
's	O
wine	O
regions	O
are	O
located	O
in	O
the	O
mountains	O
or	O
in	O
the	O
hills	O
,	O
such	O
as	O
Transdanubian	LOCATION
Medium	LOCATION
Mountains	LOCATION
,	O
Northern	LOCATION
Medium	LOCATION
Mountains	LOCATION
,	O
Villány	LOCATION
Mountains	LOCATION
,	O
and	O
so	O
on	O
.	O
Important	O
ones	O
include	O
the	O
regions	O
of	O
Eger	LOCATION
,	O
Hajós	LOCATION
,	O
Somló	LOCATION
,	O
Sopron	LOCATION
,	O
Villány	LOCATION
,	O
Szekszárd	LOCATION
,	O
and	O
Tokaj-Hegyalja	LOCATION
.	O
These	O
are	O
mainly	O
mountainous	O
areas	O
,	O
such	O
as	O
the	O
Northern	MISC
and	O
the	O
Transdanubian	LOCATION
Medium	LOCATION
Mountains	LOCATION
,	O
and	O
the	O
Alpokalja	LOCATION
.	O
