The	O
young	O
Dennett	PERSON
and	O
family	O
returned	O
to	O
Massachusetts	LOCATION
in	O
1947	O
after	O
his	O
father	O
died	O
in	O
an	O
unexplained	O
plane	O
crash	O
.	O
He	O
attended	O
Phillips	ORGANIZATION
Exeter	ORGANIZATION
Academy	ORGANIZATION
and	O
spent	O
one	O
year	O
at	O
Wesleyan	ORGANIZATION
University	ORGANIZATION
before	O
receiving	O
his	O
B.A.	O
in	O
philosophy	O
from	O
Harvard	ORGANIZATION
University	ORGANIZATION
in	O
1963	O
,	O
where	O
he	O
was	O
a	O
student	O
of	O
W.V.	PERSON
Quine	PERSON
.	O
Dennett	PERSON
describes	O
himself	O
as	O
"	O
an	O
autodidact	O
--	O
or	O
,	O
more	O
properly	O
,	O
the	O
beneficiary	O
of	O
hundreds	O
of	O
hours	O
of	O
informal	O
tutorials	O
on	O
all	O
the	O
fields	O
that	O
interest	O
me	O
,	O
from	O
some	O
of	O
the	O
world	O
's	O
leading	O
scientists	O
.	O
"	O
He	O
was	O
elected	O
to	O
the	O
American	ORGANIZATION
Academy	ORGANIZATION
of	ORGANIZATION
Arts	ORGANIZATION
and	ORGANIZATION
Sciences	ORGANIZATION
in	O
1987	O
.	O
While	O
other	O
philosophers	O
have	O
developed	O
two-stage	O
models	O
,	O
including	O
William	PERSON
James	PERSON
,	O
Henri	PERSON
Poincaré	PERSON
,	O
Arthur	PERSON
Holly	PERSON
Compton	PERSON
,	O
and	O
Henry	PERSON
Margenau	PERSON
,	O
Dennett	PERSON
defends	O
this	O
model	O
for	O
the	O
following	O
reasons	O
:	O
Leading	O
libertarian	O
philosophers	O
such	O
as	O
Robert	PERSON
Kane	PERSON
have	O
rejected	O
Dennett	PERSON
's	O
model	O
,	O
specifically	O
that	O
random	O
chance	O
is	O
directly	O
involved	O
in	O
a	O
decision	O
,	O
which	O
eliminates	O
the	O
agent	O
's	O
motives	O
and	O
reasons	O
,	O
character	O
and	O
values	O
,	O
and	O
feelings	O
and	O
desires	O
.	O
Dennett	PERSON
has	O
remarked	O
in	O
several	O
places	O
that	O
his	O
overall	O
philosophical	O
project	O
has	O
remained	O
largely	O
the	O
same	O
since	O
his	O
time	O
at	O
Oxford	ORGANIZATION
.	O
In	O
Consciousness	MISC
Explained	MISC
,	O
Dennett	PERSON
's	O
interest	O
in	O
the	O
ability	O
of	O
evolution	O
to	O
explain	O
some	O
of	O
the	O
content-producing	O
features	O
of	O
consciousness	O
is	O
already	O
apparent	O
,	O
and	O
this	O
has	O
since	O
become	O
an	O
integral	O
part	O
of	O
his	O
program	O
.	O
He	O
defends	O
a	O
theory	O
known	O
by	O
some	O
as	O
Neural	MISC
Darwinism	MISC
.	O
Much	O
of	O
Dennett	PERSON
's	O
work	O
since	O
the	O
1990s	O
has	O
been	O
concerned	O
with	O
fleshing	O
out	O
his	O
previous	O
ideas	O
by	O
addressing	O
the	O
same	O
topics	O
from	O
an	O
evolutionary	O
standpoint	O
,	O
from	O
what	O
distinguishes	O
human	O
minds	O
from	O
animal	O
minds	O
,	O
to	O
how	O
free	O
will	O
is	O
compatible	O
with	O
a	O
naturalist	O
view	O
of	O
the	O
world	O
(	O
Freedom	MISC
Evolves	MISC
)	O
.	O
In	O
his	O
2006	O
book	O
,	O
Breaking	MISC
the	MISC
Spell	MISC
:	MISC
Religion	MISC
as	MISC
a	MISC
Natural	MISC
Phenomenon	MISC
,	O
Dennett	PERSON
attempts	O
to	O
subject	O
religious	O
belief	O
to	O
the	O
same	O
treatment	O
,	O
explaining	O
possible	O
evolutionary	O
reasons	O
for	O
the	O
phenomenon	O
of	O
religious	O
adherence	O
.	O
Dennett	PERSON
self-identifies	O
with	O
a	O
few	O
terms	O
:	O
Yet	O
,	O
in	O
Consciousness	MISC
Explained	MISC
,	O
he	O
admits	O
"	O
I	O
am	O
a	O
sort	O
of	O
'	O
teleofunctionalist	O
'	O
,	O
of	O
course	O
,	O
perhaps	O
the	O
original	O
teleofunctionalist	O
'	O
"	O
.	O
In	O
Breaking	MISC
the	MISC
Spell	MISC
:	MISC
Religion	MISC
as	MISC
a	MISC
Natural	MISC
Phenomenon	MISC
he	O
admits	O
to	O
being	O
"	O
a	O
bright	O
"	O
,	O
and	O
defends	O
the	O
term	O
.	O
Dennett	PERSON
sees	O
evolution	O
by	O
natural	O
selection	O
as	O
an	O
algorithmic	O
process	O
(	O
though	O
he	O
spells	O
out	O
that	O
algorithms	O
as	O
simple	O
as	O
long	O
division	O
often	O
incorporate	O
a	O
significant	O
degree	O
of	O
randomness	O
)	O
.	O
This	O
idea	O
is	O
in	O
conflict	O
with	O
the	O
evolutionary	O
philosophy	O
of	O
paleontologist	O
Stephen	PERSON
Jay	PERSON
Gould	PERSON
,	O
who	O
preferred	O
to	O
stress	O
the	O
"	O
pluralism	O
"	O
of	O
evolution	O
(	O
i.e.	O
its	O
dependence	O
on	O
many	O
crucial	O
factors	O
,	O
of	O
which	O
natural	O
selection	O
is	O
only	O
one	O
)	O
.	O
Dennett	PERSON
's	O
views	O
on	O
evolution	O
are	O
identified	O
as	O
being	O
strongly	O
adaptationist	O
,	O
in	O
line	O
with	O
his	O
theory	O
of	O
the	O
intentional	O
stance	O
,	O
and	O
the	O
evolutionary	O
views	O
of	O
biologist	O
Richard	PERSON
Dawkins	PERSON
.	O
In	O
Darwin	MISC
's	MISC
Dangerous	MISC
Idea	MISC
,	O
Dennett	PERSON
showed	O
himself	O
even	O
more	O
willing	O
than	O
Dawkins	PERSON
to	O
defend	O
adaptationism	O
in	O
print	O
,	O
devoting	O
an	O
entire	O
chapter	O
to	O
a	O
criticism	O
of	O
the	O
ideas	O
of	O
Gould	PERSON
.	O
This	O
stems	O
from	O
Gould	PERSON
's	O
long-running	O
public	O
debate	O
with	O
E.	PERSON
O.	PERSON
Wilson	PERSON
and	O
other	O
evolutionary	O
biologists	O
over	O
human	O
sociobiology	O
and	O
its	O
descendant	O
evolutionary	O
psychology	O
,	O
which	O
Gould	PERSON
and	O
Richard	PERSON
Lewontin	PERSON
opposed	O
,	O
but	O
which	O
Dennett	PERSON
advocated	O
,	O
together	O
with	O
Dawkins	PERSON
and	O
Steven	PERSON
Pinker	PERSON
.	O
Dennett	PERSON
's	O
theories	O
have	O
had	O
a	O
significant	O
influence	O
on	O
the	O
work	O
of	O
evolutionary	O
psychologist	O
Geoffrey	PERSON
Miller	PERSON
.	O
Dennett	PERSON
lives	O
with	O
his	O
wife	O
in	O
North	LOCATION
Andover	LOCATION
,	O
Massachusetts	LOCATION
,	O
and	O
has	O
a	O
daughter	O
,	O
a	O
son	O
,	O
and	O
two	O
grandsons	O
.	O
In	O
October	O
2006	O
,	O
Dennett	PERSON
was	O
hospitalized	O
due	O
to	O
an	O
aortic	O
dissection	O
.	O
