After	O
studying	O
at	O
the	O
Jesuit	ORGANIZATION
universities	O
of	O
Dillingen	ORGANIZATION
and	O
Ingolstadt	ORGANIZATION
,	O
he	O
took	O
up	O
the	O
study	O
of	O
medicine	O
at	O
the	O
University	ORGANIZATION
of	ORGANIZATION
Vienna	ORGANIZATION
in	O
1759	O
.	O
That	O
said	O
,	O
in	O
Mesmer	PERSON
's	O
day	O
doctoral	O
theses	O
were	O
not	O
expected	O
to	O
be	O
original	O
.	O
In	O
1768	O
,	O
when	O
court	O
intrigue	O
prevented	O
the	O
performance	O
of	O
La	MISC
Finta	MISC
Semplice	MISC
(	O
K	O
.	O
51	O
)	O
for	O
which	O
a	O
twelve-year-old	O
Wolfgang	PERSON
Amadeus	PERSON
Mozart	PERSON
had	O
composed	O
500	O
pages	O
of	O
music	O
,	O
Mesmer	PERSON
is	O
said	O
to	O
have	O
arranged	O
a	O
performance	O
in	O
his	O
garden	O
of	O
Mozart	PERSON
's	O
Bastien	MISC
und	MISC
Bastienne	MISC
(	O
K	O
.	O
50	O
)	O
,	O
a	O
one-act	O
opera	O
,	O
though	O
Mozart	PERSON
's	O
biographer	O
Nissen	PERSON
has	O
stated	O
that	O
there	O
is	O
no	O
proof	O
that	O
this	O
performance	O
actually	O
took	O
place	O
.	O
Mozart	PERSON
later	O
immortalized	O
his	O
former	O
patron	O
by	O
including	O
a	O
comedic	O
reference	O
to	O
Mesmer	PERSON
in	O
his	O
opera	O
Così	MISC
fan	MISC
tutte	MISC
.	O
In	O
1774	O
,	O
Mesmer	PERSON
produced	O
an	O
"	O
artificial	O
tide	O
"	O
in	O
a	O
patient	O
by	O
having	O
her	O
swallow	O
a	O
preparation	O
containing	O
iron	O
,	O
and	O
then	O
attaching	O
magnets	O
to	O
various	O
parts	O
of	O
her	O
body	O
.	O
Mesmer	PERSON
did	O
not	O
believe	O
that	O
the	O
magnets	O
had	O
achieved	O
the	O
cure	O
on	O
their	O
own	O
.	O
Mesmer	PERSON
said	O
that	O
while	O
Gassner	PERSON
was	O
sincere	O
in	O
his	O
beliefs	O
,	O
his	O
cures	O
were	O
due	O
to	O
the	O
fact	O
that	O
he	O
possessed	O
a	O
high	O
degree	O
of	O
animal	O
magnetism	O
.	O
This	O
confrontation	O
between	O
Mesmer	PERSON
's	O
secular	O
ideas	O
and	O
Gassner	PERSON
's	O
religious	O
beliefs	O
marked	O
the	O
end	O
of	O
Gassner	PERSON
's	O
career	O
as	O
well	O
as	O
,	O
according	O
to	O
Henri	PERSON
Ellenberger	PERSON
,	O
the	O
emergence	O
of	O
dynamic	O
psychiatry	O
.	O
The	O
scandal	O
that	O
followed	O
Mesmer	PERSON
's	O
unsuccessful	O
attempt	O
to	O
treat	O
the	O
blindness	O
of	O
an	O
18-year-old	O
musician	O
,	O
Maria	PERSON
Theresia	PERSON
Paradis	PERSON
,	O
led	O
him	O
to	O
leave	O
Vienna	LOCATION
in	O
1777	O
.	O
The	O
following	O
year	O
Mesmer	PERSON
moved	O
to	O
Paris	LOCATION
,	O
rented	O
an	O
apartment	O
in	O
a	O
part	O
of	O
the	O
city	O
preferred	O
by	O
the	O
wealthy	O
and	O
powerful	O
,	O
and	O
established	O
a	O
medical	O
practice	O
.	O
Paris	LOCATION
soon	O
divided	O
into	O
those	O
who	O
thought	O
he	O
was	O
a	O
charlatan	O
who	O
had	O
been	O
forced	O
to	O
flee	O
from	O
Vienna	LOCATION
and	O
those	O
who	O
thought	O
he	O
had	O
made	O
a	O
great	O
discovery	O
.	O
In	O
his	O
first	O
years	O
in	O
Paris	LOCATION
,	O
Mesmer	PERSON
tried	O
and	O
failed	O
to	O
get	O
either	O
the	O
Royal	ORGANIZATION
Academy	ORGANIZATION
of	ORGANIZATION
Sciences	ORGANIZATION
or	O
the	O
Royal	ORGANIZATION
Society	ORGANIZATION
of	ORGANIZATION
Medicine	ORGANIZATION
to	O
provide	O
official	O
approval	O
for	O
his	O
doctrines	O
.	O
He	O
found	O
only	O
one	O
physician	O
of	O
high	O
professional	O
and	O
social	O
standing	O
,	O
Charles	PERSON
d'Eslon	PERSON
,	O
to	O
become	O
a	O
disciple	O
.	O
Mesmer	PERSON
treated	O
patients	O
both	O
individually	O
and	O
in	O
groups	O
.	O
Mesmer	PERSON
made	O
"	O
passes	O
"	O
,	O
moving	O
his	O
hands	O
from	O
patients	O
'	O
shoulders	O
down	O
along	O
their	O
arms	O
.	O
Mesmer	PERSON
would	O
often	O
conclude	O
his	O
treatments	O
by	O
playing	O
some	O
music	O
on	O
a	O
glass	O
armonica	O
.	O
By	O
1780	O
Mesmer	PERSON
had	O
more	O
patients	O
than	O
he	O
could	O
treat	O
individually	O
and	O
he	O
established	O
a	O
collective	O
treatment	O
known	O
as	O
the	O
"	O
baquet	O
"	O
.	O
At	O
the	O
request	O
of	O
these	O
commissioners	O
the	O
King	O
appointed	O
five	O
additional	O
commissioners	O
from	O
the	O
Royal	ORGANIZATION
Academy	ORGANIZATION
of	ORGANIZATION
Sciences	ORGANIZATION
.	O
These	O
included	O
the	O
chemist	O
Antoine	PERSON
Lavoisier	PERSON
,	O
the	O
physician	O
Joseph-Ignace	PERSON
Guillotin	PERSON
,	O
the	O
astronomer	O
Jean	PERSON
Sylvain	PERSON
Bailly	PERSON
,	O
and	O
the	O
American	MISC
ambassador	O
Benjamin	PERSON
Franklin	PERSON
.	O
The	O
commission	O
conducted	O
a	O
series	O
of	O
experiments	O
aimed	O
,	O
not	O
at	O
determining	O
whether	O
Mesmer	PERSON
's	O
treatment	O
worked	O
,	O
but	O
whether	O
he	O
had	O
discovered	O
a	O
new	O
physical	O
fluid	O
.	O
In	O
1785	O
Mesmer	PERSON
left	O
Paris	LOCATION
.	O
When	O
he	O
sold	O
his	O
house	O
in	O
Vienna	LOCATION
in	O
1801	O
he	O
was	O
in	O
Paris	LOCATION
.	O
Mesmer	PERSON
was	O
driven	O
into	O
exile	O
soon	O
after	O
the	O
investigations	O
on	O
animal	O
magnetism	O
.	O
