Concerns	O
about	O
the	O
safety	O
of	O
adenovirus	O
vectors	O
were	O
raised	O
after	O
the	O
1999	O
death	O
of	O
Jesse	PERSON
Gelsinger	PERSON
while	O
participating	O
in	O
a	O
gene	O
therapy	O
trial	O
.	O
The	O
first	O
operation	O
was	O
carried	O
out	O
on	O
a	O
23	O
year-old	O
British	MISC
male	O
,	O
Robert	MISC
Johnson	MISC
,	O
in	O
early	O
2007	O
.	O
In	O
September	O
2009	O
,	O
the	O
journal	O
Nature	MISC
reported	O
that	O
researchers	O
at	O
the	O
University	ORGANIZATION
of	ORGANIZATION
Washington	ORGANIZATION
and	O
University	ORGANIZATION
of	ORGANIZATION
Florida	ORGANIZATION
were	O
able	O
to	O
give	O
trichromatic	O
vision	O
to	O
squirrel	O
monkeys	O
using	O
gene	O
therapy	O
,	O
a	O
hopeful	O
precursor	O
to	O
a	O
treatment	O
for	O
color	O
blindness	O
in	O
humans	O
.	O
In	O
November	O
2009	O
,	O
the	O
journal	O
Science	MISC
reported	O
that	O
researchers	O
succeeded	O
at	O
halting	O
a	O
fatal	O
brain	O
disease	O
,	O
adrenoleukodystrophy	O
,	O
using	O
a	O
vector	O
derived	O
from	O
HIV	O
to	O
deliver	O
the	O
gene	O
for	O
the	O
missing	O
enzyme	O
.	O
