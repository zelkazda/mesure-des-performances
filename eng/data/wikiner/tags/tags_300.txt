Abensberg	LOCATION
is	O
a	O
town	O
in	O
the	O
Lower	LOCATION
Bavarian	LOCATION
district	O
of	O
Kelheim	LOCATION
,	O
in	O
Bavaria	LOCATION
,	O
Germany	LOCATION
,	O
lying	O
around	O
30	O
km	O
southwest	O
of	O
Regensburg	LOCATION
,	O
40	O
km	O
east	O
of	O
Ingolstadt	LOCATION
,	O
50	O
northwest	O
of	O
Landshut	LOCATION
and	O
100	O
km	O
north	O
of	O
Munich	LOCATION
.	O
It	O
is	O
situated	O
on	O
the	O
Abens	LOCATION
river	O
,	O
a	O
tributary	O
of	O
the	O
Danube	LOCATION
.	O
The	O
town	O
lies	O
on	O
the	O
Abens	LOCATION
river	O
,	O
a	O
tributary	O
of	O
the	O
Danube	LOCATION
,	O
around	O
eight	O
kilometres	O
from	O
the	O
river	O
's	O
source	O
.	O
The	O
area	O
around	O
Abensberg	LOCATION
is	O
characterized	O
by	O
the	O
narrow	O
valley	O
of	O
the	O
Danube	LOCATION
,	O
where	O
the	O
Weltenburg	LOCATION
Abbey	LOCATION
stands	O
,	O
the	O
valley	O
of	O
the	O
Altmühl	LOCATION
in	O
the	O
north	O
,	O
a	O
left	O
tributary	O
of	O
the	O
Danube	LOCATION
,	O
and	O
the	O
famous	O
Hallertau	LOCATION
hops-planting	O
region	O
in	O
the	O
south	O
.	O
He	O
is	O
buried	O
in	O
the	O
former	O
convent	O
of	O
Abensberg	LOCATION
.	O
Johannes	PERSON
Aventinus	PERSON
(	O
1477	O
--	O
1534	O
)	O
is	O
the	O
city	O
's	O
most	O
famous	O
son	O
,	O
the	O
founder	O
of	O
the	O
study	O
of	O
history	O
in	O
Bavaria	LOCATION
.	O
Abensberg	LOCATION
also	O
contained	O
a	O
magistrates	O
'	O
court	O
.	O
On	O
the	O
left	O
are	O
the	O
blue	O
and	O
white	O
rhombuses	O
of	O
Bavaria	LOCATION
,	O
while	O
the	O
right	O
half	O
is	O
split	O
into	O
two	O
sliver	O
and	O
black	O
triangles	O
.	O
The	O
swords	O
recall	O
the	O
Battle	MISC
of	MISC
Abensberg	MISC
.	O
The	O
area	O
around	O
Abensberg	LOCATION
,	O
the	O
so-called	O
sand	O
belt	O
between	O
Siegburg	LOCATION
,	O
Neustadt	LOCATION
an	LOCATION
der	LOCATION
Donau	LOCATION
,	O
Abensberg	LOCATION
and	O
Langquaid	LOCATION
,	O
is	O
used	O
for	O
the	O
intensive	O
farming	O
of	O
asparagus	O
,	O
due	O
to	O
the	O
optimal	O
soil	O
condition	O
and	O
climate	O
.	O
Abensberg	LOCATION
asparagus	O
enjoys	O
a	O
reputation	O
among	O
connoisseurs	O
as	O
a	O
particular	O
delicacy	O
.	O
The	O
Abensberg	LOCATION
railway	O
station	O
is	O
located	O
on	O
the	O
Danube	LOCATION
Valley	LOCATION
Railway	LOCATION
from	O
Regensburg	LOCATION
to	O
Ingolstadt	LOCATION
.	O
In	O
2008	O
,	O
a	O
former	O
goods	O
shed	O
by	O
the	O
main	O
railway	O
station	O
of	O
Abensberg	LOCATION
was	O
converted	O
into	O
a	O
theatre	O
by	O
local	O
volunteers	O
.	O
Abensberg	LOCATION
has	O
a	O
long	O
tradition	O
of	O
museums	O
.	O
At	O
the	O
cemetery	O
in	O
what	O
is	O
now	O
the	O
district	O
of	O
Pullach	LOCATION
stood	O
a	O
memorial	O
stone	O
which	O
was	O
mentioned	O
as	O
recently	O
as	O
1967	O
,	O
but	O
which	O
is	O
no	O
longer	O
at	O
the	O
site	O
.	O
