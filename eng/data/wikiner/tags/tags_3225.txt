As	O
Hugo	PERSON
Ball	PERSON
expressed	O
it	O
,	O
"	O
For	O
us	O
,	O
art	O
is	O
not	O
an	O
end	O
in	O
itself	O
...	O
but	O
it	O
is	O
an	O
opportunity	O
for	O
the	O
true	O
perception	O
and	O
criticism	O
of	O
the	O
times	O
we	O
live	O
in	O
.	O
"	O
In	O
1916	O
,	O
Hugo	PERSON
Ball	PERSON
,	O
Emmy	PERSON
Hennings	PERSON
,	O
Tristan	PERSON
Tzara	PERSON
,	O
Jean	PERSON
Arp	PERSON
,	O
Marcel	PERSON
Janco	PERSON
,	O
Richard	PERSON
Huelsenbeck	PERSON
,	O
Sophie	PERSON
Täuber	PERSON
,	O
Hans	PERSON
Richter	PERSON
,	O
along	O
with	O
others	O
,	O
discussed	O
art	O
and	O
put	O
on	O
performances	O
in	O
the	O
Cabaret	ORGANIZATION
Voltaire	ORGANIZATION
expressing	O
their	O
disgust	O
with	O
the	O
war	O
and	O
the	O
interests	O
that	O
inspired	O
it	O
.	O
Marcel	PERSON
Janco	PERSON
recalled	O
,	O
A	O
single	O
issue	O
of	O
the	O
magazine	O
Cabaret	ORGANIZATION
Voltaire	ORGANIZATION
was	O
the	O
first	O
publication	O
to	O
come	O
out	O
of	O
the	O
movement	O
.	O
Grosz	PERSON
,	O
together	O
with	O
John	PERSON
Heartfield	PERSON
,	O
developed	O
the	O
technique	O
of	O
photomontage	O
during	O
this	O
period	O
.	O
Much	O
of	O
their	O
activity	O
centered	O
in	O
Alfred	PERSON
Stieglitz	PERSON
's	O
gallery	O
,	O
291	O
,	O
and	O
the	O
home	O
of	O
Walter	PERSON
and	PERSON
Louise	PERSON
Arensberg	PERSON
.	O
During	O
this	O
time	O
Duchamp	PERSON
began	O
exhibiting	O
"	O
readymades	O
"	O
(	O
found	O
objects	O
)	O
such	O
as	O
a	O
bottle	O
rack	O
,	O
and	O
got	O
involved	O
with	O
the	O
Society	ORGANIZATION
of	ORGANIZATION
Independent	ORGANIZATION
Artists	ORGANIZATION
.	O
The	O
committee	O
presiding	O
over	O
Britain	LOCATION
's	O
prestigious	O
Turner	ORGANIZATION
Prize	ORGANIZATION
in	O
2004	O
,	O
for	O
example	O
,	O
called	O
it	O
"	O
the	O
most	O
influential	O
work	O
of	O
modern	O
art	O
.	O
"	O
Dada	O
in	O
Paris	LOCATION
surged	O
in	O
1920	O
when	O
many	O
of	O
the	O
originators	O
converged	O
there	O
.	O
The	O
movement	O
became	O
less	O
active	O
as	O
post-World	MISC
War	MISC
II	MISC
optimism	O
led	O
to	O
new	O
movements	O
in	O
art	O
and	O
literature	O
.	O
Dada	O
is	O
a	O
named	O
influence	O
and	O
reference	O
of	O
various	O
anti-art	O
and	O
political	O
and	O
cultural	O
movements	O
including	O
the	O
Situationists	ORGANIZATION
and	O
culture	O
jamming	O
groups	O
like	O
the	O
Cacophony	ORGANIZATION
Society	ORGANIZATION
.	O
Tom	PERSON
Stoppard	PERSON
used	O
this	O
coincidence	O
as	O
a	O
premise	O
for	O
his	O
play	O
Travesties	MISC
(	O
1974	O
)	O
,	O
which	O
includes	O
Tzara	PERSON
,	O
Lenin	PERSON
,	O
and	O
James	PERSON
Joyce	PERSON
as	O
characters	O
.	O
Marcel	PERSON
Duchamp	PERSON
began	O
to	O
view	O
the	O
manufactured	O
objects	O
of	O
his	O
collection	O
as	O
objects	O
of	O
art	O
,	O
which	O
he	O
called	O
"	O
readymades	O
"	O
.	O
Duchamp	PERSON
wrote	O
:	O
"	O
One	O
important	O
characteristic	O
was	O
the	O
short	O
sentence	O
which	O
I	O
occasionally	O
inscribed	O
on	O
the	O
'	O
readymade	O
.	O
'	O
