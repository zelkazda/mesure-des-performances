(	O
The	O
Battle	MISC
of	MISC
Nations	MISC
(	O
1813	O
)	O
and	O
the	O
Battle	LOCATION
of	LOCATION
Gettysburg	LOCATION
(	O
1863	O
)	O
were	O
exceptional	O
in	O
lasting	O
three	O
days	O
.	O
)	O
Improvements	O
in	O
transportation	O
and	O
the	O
sudden	O
evolving	O
of	O
trench	O
warfare	O
,	O
with	O
its	O
siege-like	O
nature	O
during	O
World	MISC
War	MISC
I	MISC
in	O
the	O
20th	O
century	O
,	O
lengthened	O
the	O
duration	O
of	O
battles	O
to	O
days	O
and	O
weeks	O
.	O
Trench	O
warfare	O
had	O
become	O
largely	O
obsolete	O
in	O
conflicts	O
between	O
advanced	O
armies	O
by	O
the	O
start	O
of	O
the	O
Second	MISC
World	MISC
War	MISC
.	O
The	O
use	O
of	O
the	O
term	O
"	O
battle	O
"	O
in	O
military	O
history	O
has	O
led	O
to	O
its	O
misuse	O
when	O
referring	O
to	O
almost	O
any	O
scale	O
of	O
combat	O
,	O
notably	O
by	O
strategic	O
forces	O
involving	O
hundreds	O
of	O
thousands	O
of	O
troops	O
that	O
may	O
be	O
engaged	O
in	O
either	O
a	O
single	O
battle	O
at	O
one	O
time	O
(	O
Battle	MISC
of	MISC
Leipzig	MISC
)	O
or	O
multiple	O
operations	O
(	O
Battle	MISC
of	MISC
Kursk	MISC
)	O
.	O
A	O
"	O
battle	O
"	O
in	O
this	O
broader	O
sense	O
may	O
occupy	O
a	O
large	O
piece	O
of	O
spacetime	O
,	O
as	O
in	O
the	O
case	O
of	O
the	O
Battle	MISC
of	MISC
Britain	MISC
or	O
the	O
Battle	MISC
of	MISC
the	MISC
Atlantic	MISC
.	O
Likewise	O
,	O
the	O
Zulus	MISC
in	O
the	O
early	O
19th	O
century	O
were	O
victorious	O
in	O
battles	O
against	O
their	O
rivals	O
in	O
part	O
because	O
they	O
adopted	O
a	O
new	O
kind	O
of	O
spear	O
,	O
the	O
iklwa	O
.	O
Even	O
so	O
,	O
forces	O
with	O
inferior	O
weapons	O
have	O
still	O
emerged	O
victorious	O
at	O
times	O
,	O
for	O
example	O
in	O
the	O
Wars	MISC
of	MISC
Scottish	MISC
Independence	MISC
and	O
in	O
the	O
First	MISC
Italo	MISC
--	MISC
Ethiopian	MISC
War	MISC
.	O
Hannibal	PERSON
,	O
Julius	PERSON
Caesar	PERSON
and	O
Napoleon	PERSON
Bonaparte	PERSON
were	O
all	O
skilled	O
generals	O
and	O
,	O
consequently	O
,	O
their	O
armies	O
were	O
extremely	O
successful	O
.	O
Air	O
battles	O
have	O
been	O
far	O
less	O
common	O
,	O
due	O
to	O
their	O
late	O
conception	O
,	O
the	O
most	O
prominent	O
being	O
the	O
Battle	MISC
of	MISC
Britain	MISC
in	O
1940	O
.	O
However	O
since	O
the	O
Second	MISC
World	MISC
War	MISC
land	O
or	O
sea	O
battles	O
have	O
come	O
to	O
rely	O
on	O
air	O
support	O
.	O
Indeed	O
,	O
during	O
the	O
Battle	MISC
of	MISC
Midway	MISC
,	O
five	O
aircraft	O
carriers	O
were	O
sunk	O
without	O
either	O
fleet	O
coming	O
into	O
direct	O
contact	O
.	O
A	O
decisive	O
battle	O
is	O
one	O
of	O
particular	O
importance	O
;	O
often	O
by	O
bringing	O
hostilities	O
to	O
an	O
end	O
,	O
such	O
as	O
the	O
Battle	LOCATION
of	LOCATION
Hastings	LOCATION
or	O
the	O
Battle	MISC
of	MISC
Hattin	MISC
,	O
or	O
as	O
a	O
turning	O
point	O
in	O
the	O
fortunes	O
of	O
the	O
belligerents	O
,	O
such	O
as	O
the	O
Battle	MISC
of	MISC
Stalingrad	MISC
.	O
The	O
concept	O
of	O
the	O
decisive	O
battle	O
became	O
popular	O
with	O
the	O
publication	O
in	O
1851	O
of	O
Edward	PERSON
Creasy	PERSON
's	O
The	MISC
Fifteen	MISC
Decisive	MISC
Battles	MISC
of	MISC
the	MISC
World	MISC
.	O
However	O
,	O
during	O
the	O
many	O
wars	O
of	O
the	O
Roman	LOCATION
Empire	LOCATION
,	O
barbarians	O
continued	O
using	O
mob	O
tactics	O
.	O
A	O
new	O
style	O
,	O
during	O
World	MISC
War	MISC
I	MISC
,	O
known	O
as	O
trench	O
warfare	O
,	O
developed	O
nearly	O
half	O
a	O
century	O
later	O
.	O
Chemical	O
warfare	O
also	O
emerged	O
with	O
the	O
use	O
of	O
poisonous	O
gas	O
during	O
World	MISC
War	MISC
I	MISC
.	O
By	O
World	MISC
War	MISC
II	MISC
,	O
the	O
use	O
of	O
the	O
smaller	O
divisions	O
,	O
platoons	O
and	O
companies	O
,	O
became	O
much	O
more	O
important	O
as	O
precise	O
operations	O
became	O
vital	O
.	O
Instead	O
of	O
the	O
locked	O
trench	O
warfare	O
of	O
World	MISC
War	MISC
I	MISC
,	O
during	O
World	MISC
War	MISC
II	MISC
,	O
a	O
dynamic	O
network	O
of	O
battles	O
developed	O
where	O
small	O
groups	O
encountered	O
other	O
platoons	O
.	O
Modern	O
battles	O
now	O
continue	O
to	O
resemble	O
that	O
of	O
World	MISC
War	MISC
II	MISC
,	O
though	O
prominent	O
innovations	O
have	O
been	O
added	O
.	O
A	O
good	O
example	O
of	O
an	O
old	O
naval	O
battle	O
is	O
the	O
Battle	MISC
of	MISC
Salamis	MISC
.	O
Troops	O
were	O
often	O
actually	O
used	O
to	O
storm	O
enemy	O
ships	O
as	O
used	O
by	O
Romans	MISC
and	O
pirates	O
.	O
The	O
ironclad	O
,	O
first	O
used	O
in	O
the	O
American	MISC
Civil	MISC
War	MISC
,	O
resistant	O
to	O
cannons	O
,	O
soon	O
made	O
the	O
wooden	O
ship	O
obsolete	O
.	O
The	O
invention	O
of	O
military	O
submarines	O
,	O
during	O
World	MISC
War	MISC
I	MISC
,	O
brought	O
naval	O
warfare	O
to	O
both	O
above	O
and	O
below	O
the	O
surface	O
.	O
With	O
the	O
development	O
of	O
military	O
aircraft	O
during	O
World	MISC
War	MISC
II	MISC
,	O
battles	O
were	O
fought	O
in	O
the	O
sky	O
as	O
well	O
as	O
below	O
the	O
ocean	O
.	O
Although	O
the	O
use	O
of	O
aircraft	O
has	O
for	O
the	O
most	O
part	O
always	O
been	O
used	O
as	O
a	O
supplement	O
to	O
land	O
or	O
naval	O
engagements	O
,	O
since	O
their	O
first	O
major	O
military	O
use	O
in	O
World	MISC
War	MISC
I	MISC
aircraft	O
have	O
increasingly	O
taken	O
on	O
larger	O
roles	O
in	O
warfare	O
.	O
During	O
World	MISC
War	MISC
I	MISC
,	O
the	O
primary	O
use	O
was	O
for	O
reconnaissance	O
,	O
and	O
small-scale	O
bombardment	O
.	O
Aircraft	O
began	O
becoming	O
much	O
more	O
prominent	O
in	O
the	O
Spanish	MISC
Civil	MISC
War	MISC
and	O
especially	O
World	MISC
War	MISC
II	MISC
.	O
Some	O
of	O
the	O
more	O
notable	O
aerial	O
battles	O
in	O
this	O
period	O
include	O
the	O
Battle	MISC
of	MISC
Britain	MISC
and	O
the	O
Battle	MISC
of	MISC
Midway	MISC
.	O
Occasionally	O
battles	O
are	O
named	O
after	O
the	O
date	O
on	O
which	O
they	O
took	O
place	O
,	O
such	O
as	O
The	MISC
Glorious	MISC
First	MISC
of	MISC
June	MISC
.	O
Sometimes	O
in	O
desert	O
warfare	O
,	O
there	O
is	O
no	O
nearby	O
town	O
name	O
to	O
use	O
;	O
map	O
coordinates	O
gave	O
the	O
name	O
to	O
the	O
Battle	MISC
of	MISC
73	MISC
Easting	MISC
in	O
the	O
First	MISC
Gulf	MISC
War	MISC
.	O
Some	O
place	O
names	O
have	O
become	O
synonymous	O
with	O
the	O
battles	O
that	O
took	O
place	O
there	O
,	O
such	O
as	O
the	O
Passchendaele	MISC
,	O
Pearl	MISC
Harbor	MISC
,	O
the	MISC
Alamo	MISC
,	O
Thermopylae	MISC
,	O
or	O
Waterloo	MISC
.	O
Operation	MISC
Market	MISC
Garden	MISC
and	O
Operation	MISC
Rolling	MISC
Thunder	MISC
are	O
examples	O
of	O
battles	O
known	O
by	O
their	O
military	O
codenames	O
.	O
When	O
a	O
battleground	O
is	O
the	O
site	O
of	O
more	O
than	O
one	O
battle	O
in	O
the	O
same	O
conflict	O
,	O
the	O
instances	O
are	O
distinguished	O
by	O
ordinal	O
number	O
,	O
such	O
as	O
the	O
First	MISC
and	O
Second	MISC
Battles	MISC
of	MISC
Bull	MISC
Run	MISC
.	O
To	O
the	O
soldiers	O
who	O
did	O
the	O
fighting	O
,	O
the	O
distinction	O
was	O
usually	O
academic	O
;	O
a	O
soldier	O
fighting	O
at	O
Beaumont	LOCATION
Hamel	LOCATION
on	O
November	O
13	O
,	O
1916	O
was	O
probably	O
unaware	O
he	O
was	O
taking	O
part	O
in	O
what	O
the	O
committee	O
would	O
call	O
the	O
"	O
Battle	MISC
of	MISC
the	MISC
Ancre	MISC
"	O
.	O
Famous	O
examples	O
include	O
the	O
War	MISC
of	MISC
the	MISC
Roses	MISC
,	O
as	O
well	O
as	O
the	O
Jacobite	MISC
Uprisings	MISC
.	O
Battles	O
also	O
affect	O
the	O
commitment	O
of	O
one	O
side	O
or	O
the	O
other	O
to	O
the	O
continuance	O
of	O
a	O
war	O
,	O
for	O
example	O
the	O
Battle	MISC
of	MISC
Incheon	MISC
and	O
the	O
Battle	MISC
of	MISC
Hue	MISC
during	O
the	O
Tet	MISC
Offensive	MISC
.	O
