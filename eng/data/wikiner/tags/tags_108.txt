Arabic	MISC
has	O
many	O
different	O
,	O
geographically	O
distributed	O
spoken	O
varieties	O
,	O
some	O
of	O
which	O
are	O
mutually	O
unintelligible	O
.	O
Arabic	MISC
has	O
lent	O
many	O
words	O
to	O
other	O
languages	O
of	O
the	O
Islamic	MISC
world	O
,	O
like	O
Turkish	MISC
and	O
Persian	MISC
.	O
The	O
sociolinguistic	O
situation	O
of	O
Arabic	MISC
in	O
modern	O
times	O
provides	O
a	O
prime	O
example	O
of	O
the	O
linguistic	O
phenomenon	O
of	O
diglossia	O
,	O
which	O
is	O
the	O
normal	O
use	O
of	O
two	O
separate	O
varieties	O
of	O
the	O
same	O
language	O
,	O
usually	O
in	O
different	O
social	O
situations	O
.	O
Arabic	MISC
speakers	O
often	O
improve	O
their	O
familiarity	O
with	O
other	O
dialects	O
via	O
music	O
or	O
film	O
.	O
Other	O
languages	O
such	O
as	O
Maltese	MISC
and	O
Kinubi	MISC
derive	O
from	O
Arabic	MISC
,	O
rather	O
than	O
merely	O
borrowing	O
vocabulary	O
or	O
grammar	O
rules	O
.	O
The	O
terms	O
borrowed	O
range	O
from	O
religious	O
terminology	O
(	O
like	O
Berber	MISC
taẓallit	O
"	O
prayer	O
"	O
<	O
salat	O
)	O
,	O
academic	O
terms	O
(	O
like	O
Uyghur	MISC
mentiq	O
"	O
logic	O
"	O
)	O
,	O
economic	O
items	O
to	O
placeholders	O
and	O
everyday	O
conjunctions	O
.	O
Most	O
Berber	MISC
varieties	O
(	O
such	O
as	O
Kabyle	MISC
)	O
,	O
along	O
with	O
Swahili	MISC
,	O
borrow	O
some	O
numbers	O
from	O
Arabic	MISC
.	O
Most	O
Islamic	MISC
religious	O
terms	O
are	O
direct	O
borrowings	O
from	O
Arabic	MISC
,	O
such	O
as	O
salat	O
'	O
prayer	O
'	O
and	O
imam	O
'	O
prayer	O
leader	O
.	O
'	O
Some	O
words	O
in	O
common	O
use	O
,	O
such	O
as	O
"	O
intention	O
"	O
and	O
"	O
information	O
"	O
,	O
were	O
originally	O
calques	O
of	O
Arabic	MISC
philosophical	O
terms	O
.	O
Arabic	MISC
was	O
influenced	O
by	O
other	O
languages	O
as	O
well	O
.	O
Arabic	MISC
is	O
the	O
language	O
of	O
the	O
Qur'an	MISC
.	O
Arabic	MISC
has	O
two	O
kinds	O
of	O
syllables	O
:	O
open	O
syllables	O
and	O
--	O
and	O
closed	O
syllables	O
,	O
,	O
and	O
,	O
the	O
latter	O
two	O
occurring	O
only	O
at	O
the	O
end	O
of	O
the	O
sentence	O
.	O
Arabic	O
phonology	O
recognizes	O
the	O
glottal	O
stop	O
as	O
an	O
independent	O
consonant	O
,	O
so	O
in	O
cases	O
where	O
a	O
word	O
begins	O
with	O
a	O
vowel	O
sound	O
,	O
as	O
the	O
definite	O
article	O
"	O
al	O
"	O
,	O
for	O
example	O
,	O
the	O
word	O
is	O
recognized	O
in	O
Arabic	MISC
as	O
beginning	O
with	O
the	O
consonant	O
[	O
ʔ	O
]	O
(	O
glottal	O
stop	O
)	O
.	O
For	O
example	O
,	O
non-Arabic	MISC
[	O
v	O
]	O
is	O
used	O
in	O
the	O
Maghrebi	LOCATION
dialects	O
as	O
well	O
in	O
the	O
written	O
language	O
mostly	O
for	O
foreign	O
names	O
.	O
Early	O
in	O
the	O
expansion	O
of	O
Arabic	MISC
,	O
the	O
separate	O
emphatic	O
phonemes	O
[	O
dˁ	O
]	O
and	O
[	O
ðˁ	O
]	O
coallesced	O
into	O
a	O
single	O
phoneme	O
,	O
becoming	O
one	O
or	O
the	O
other	O
.	O
These	O
forms	O
,	O
and	O
their	O
associated	O
participles	O
and	O
verbal	O
nouns	O
,	O
are	O
the	O
primary	O
means	O
of	O
forming	O
vocabulary	O
in	O
Arabic	MISC
.	O
When	O
representing	O
a	O
number	O
in	O
Arabic	MISC
,	O
the	O
lowest-valued	O
position	O
is	O
placed	O
on	O
the	O
right	O
,	O
so	O
the	O
order	O
of	O
positions	O
is	O
the	O
same	O
as	O
in	O
left-to-right	O
scripts	O
.	O
For	O
example	O
,	O
24	O
is	O
said	O
"	O
four	O
and	O
twenty	O
"	O
just	O
like	O
in	O
the	O
German	MISC
language	MISC
(	O
vierundzwanzig	O
)	O
,	O
and	O
1975	O
is	O
said	O
"	O
one	O
thousand	O
and	O
nine	O
hundred	O
and	O
five	O
and	O
seventy	O
.	O
"	O
They	O
also	O
publish	O
old	O
and	O
historical	O
Arabic	MISC
manuscripts	O
.	O
Arabic	O
language	O
schools	O
exist	O
to	O
assist	O
students	O
in	O
learning	O
Arabic	MISC
outside	O
of	O
the	O
academic	O
world	O
.	O
Radio	O
series	O
of	O
Arabic	MISC
language	MISC
classes	O
are	O
also	O
provided	O
from	O
some	O
radio	O
stations	O
.	O
A	O
number	O
of	O
websites	O
on	O
the	O
Internet	MISC
provide	O
online	O
classes	O
for	O
all	O
levels	O
as	O
a	O
means	O
of	O
distance	O
education	O
.	O
