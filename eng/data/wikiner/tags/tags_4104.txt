Armstrong	PERSON
was	O
the	O
inventor	O
of	O
modern	O
frequency	O
modulation	O
(	O
FM	O
)	O
radio	O
.	O
He	O
studied	O
at	O
Columbia	ORGANIZATION
University	ORGANIZATION
and	O
later	O
became	O
a	O
professor	O
there	O
.	O
The	O
family	O
moved	O
in	O
1902	O
to	O
Yonkers	LOCATION
.	O
Unlike	O
many	O
engineers	O
,	O
Armstrong	PERSON
was	O
never	O
a	O
corporate	O
employee	O
.	O
Armstrong	PERSON
had	O
a	O
tough	O
personality	O
;	O
the	O
term	O
"	O
compromise	O
"	O
was	O
not	O
in	O
his	O
vocabulary	O
.	O
Armstrong	PERSON
's	O
discovery	O
of	O
regeneration	O
caused	O
him	O
to	O
wake	O
his	O
sister	O
in	O
the	O
middle	O
of	O
the	O
night	O
,	O
shouting	O
"	O
I	O
've	O
done	O
it	O
!	O
"	O
Armstrong	PERSON
's	O
discovery	O
and	O
development	O
of	O
superheterodyne	O
technology	O
made	O
radio	O
receivers	O
,	O
then	O
the	O
primary	O
communications	O
devices	O
of	O
the	O
time	O
,	O
more	O
sensitive	O
and	O
selective	O
.	O
Armstrong	PERSON
is	O
possibly	O
best	O
known	O
for	O
his	O
discovery	O
of	O
wideband	O
frequency	O
modulation	O
.	O
FM	O
was	O
born	O
of	O
a	O
request	O
by	O
David	PERSON
Sarnoff	PERSON
of	O
RCA	ORGANIZATION
as	O
a	O
means	O
to	O
eliminate	O
static	O
in	O
radio	O
reception	O
.	O
Sarnoff	PERSON
came	O
to	O
see	O
FM	O
as	O
a	O
threat	O
and	O
refused	O
to	O
support	O
it	O
further	O
.	O
Armstrong	PERSON
did	O
not	O
live	O
to	O
see	O
the	O
full	O
potential	O
of	O
his	O
discovery	O
,	O
ie	O
multiplexing	O
of	O
FM	O
signals	O
.	O
Many	O
of	O
Armstrong	PERSON
's	O
inventions	O
were	O
ultimately	O
claimed	O
by	O
others	O
in	O
patent	O
lawsuits	O
.	O
In	O
particular	O
,	O
the	O
regenerative	O
circuit	O
,	O
which	O
Armstrong	PERSON
patented	O
in	O
1914	O
as	O
a	O
"	O
wireless	O
receiving	O
system,	O
"	O
was	O
subsequently	O
patented	O
by	O
Lee	PERSON
De	PERSON
Forest	PERSON
in	O
1916	O
;	O
De	PERSON
Forest	PERSON
then	O
sold	O
the	O
rights	O
to	O
his	O
patent	O
to	O
AT	ORGANIZATION
&	ORGANIZATION
T	ORGANIZATION
.	O
Between	O
1922	O
and	O
1934	O
,	O
Armstrong	PERSON
found	O
himself	O
embroiled	O
in	O
a	O
patent	O
war	O
,	O
between	O
himself	O
,	O
RCA	ORGANIZATION
,	O
and	O
Westinghouse	ORGANIZATION
on	O
one	O
side	O
,	O
and	O
De	PERSON
Forest	PERSON
and	O
AT	ORGANIZATION
&	ORGANIZATION
T	ORGANIZATION
on	O
the	O
other	O
.	O
Armstrong	PERSON
won	O
the	O
first	O
round	O
of	O
the	O
lawsuit	O
,	O
lost	O
the	O
second	O
,	O
and	O
stalemated	O
in	O
a	O
third	O
.	O
Before	O
the	O
Supreme	ORGANIZATION
Court	ORGANIZATION
of	ORGANIZATION
the	ORGANIZATION
United	ORGANIZATION
States	ORGANIZATION
,	O
De	PERSON
Forest	PERSON
was	O
granted	O
the	O
regeneration	O
patent	O
in	O
what	O
is	O
today	O
widely	O
believed	O
to	O
be	O
a	O
misunderstanding	O
of	O
the	O
technical	O
facts	O
by	O
the	O
Supreme	ORGANIZATION
Court	ORGANIZATION
.	O
By	O
early	O
1923	O
Armstrong	PERSON
was	O
a	O
millionaire	O
as	O
a	O
result	O
of	O
licensing	O
his	O
patents	O
to	O
RCA	ORGANIZATION
.	O
Even	O
as	O
the	O
regenerative-circuit	O
lawsuit	O
continued	O
,	O
Armstrong	PERSON
was	O
working	O
on	O
another	O
momentous	O
invention	O
.	O
While	O
working	O
in	O
the	O
basement	O
laboratory	O
of	O
Columbia	ORGANIZATION
's	O
Philosophy	LOCATION
Hall	LOCATION
,	O
he	O
created	O
wide-band	O
frequency	O
modulation	O
radio	O
(	O
FM	O
)	O
.	O
Rather	O
than	O
varying	O
the	O
amplitude	O
of	O
a	O
radio	O
wave	O
to	O
create	O
sound	O
,	O
Armstrong	PERSON
's	O
method	O
varied	O
the	O
frequency	O
of	O
the	O
wave	O
instead	O
.	O
However	O
RCA	ORGANIZATION
had	O
its	O
eye	O
on	O
television	O
broadcasting	O
,	O
and	O
chose	O
not	O
to	O
buy	O
the	O
patents	O
for	O
the	O
FM	O
technology	O
.	O
A	O
June	O
17	O
,	O
1936	O
,	O
presentation	O
at	O
the	O
Federal	ORGANIZATION
Communications	ORGANIZATION
Commission	ORGANIZATION
(	O
FCC	ORGANIZATION
)	O
headquarters	O
made	O
headlines	O
nationwide	O
.	O
RCA	ORGANIZATION
began	O
to	O
lobby	O
for	O
a	O
change	O
in	O
the	O
law	O
or	O
FCC	ORGANIZATION
regulations	O
that	O
would	O
prevent	O
FM	O
radios	O
from	O
becoming	O
dominant	O
.	O
By	O
June	O
1945	O
,	O
the	O
RCA	ORGANIZATION
had	O
pushed	O
the	O
FCC	ORGANIZATION
hard	O
on	O
the	O
allocation	O
of	O
electromagnetic	O
frequencies	O
for	O
the	O
fledgling	O
television	O
industry	O
.	O
This	O
change	O
was	O
strongly	O
supported	O
by	O
AT	ORGANIZATION
&	ORGANIZATION
T	ORGANIZATION
,	O
because	O
loss	O
of	O
FM	O
relaying	O
stations	O
forced	O
radio	O
stations	O
to	O
buy	O
wired	O
links	O
from	O
AT	ORGANIZATION
&	ORGANIZATION
T	ORGANIZATION
.	O
A	O
patent	O
fight	O
between	O
RCA	ORGANIZATION
and	O
Armstrong	PERSON
ensued	O
.	O
The	O
undermining	O
of	O
the	O
Yankee	ORGANIZATION
Network	ORGANIZATION
and	O
his	O
costly	O
legal	O
battles	O
brought	O
ruin	O
to	O
Armstrong	PERSON
,	O
by	O
then	O
almost	O
penniless	O
and	O
emotionally	O
distraught	O
.	O
Eventually	O
,	O
after	O
Armstrong	PERSON
's	O
death	O
,	O
many	O
of	O
the	O
lawsuits	O
were	O
decided	O
or	O
settled	O
in	O
his	O
favor	O
,	O
greatly	O
enriching	O
his	O
estate	O
and	O
heirs	O
--	O
but	O
the	O
decisions	O
came	O
too	O
late	O
for	O
Armstrong	PERSON
himself	O
to	O
enjoy	O
his	O
legal	O
vindication	O
.	O
He	O
was	O
an	O
avid	O
tennis	O
player	O
until	O
an	O
injury	O
in	O
1940	O
,	O
and	O
drank	O
an	O
Old	MISC
Fashioned	MISC
with	O
dinner	O
.	O
Upon	O
hearing	O
the	O
news	O
,	O
David	PERSON
Sarnoff	PERSON
remarked	O
"	O
I	O
did	O
not	O
kill	O
Armstrong	PERSON
.	O
"	O
Armstrong	PERSON
was	O
of	O
the	O
opinion	O
that	O
anyone	O
who	O
had	O
actual	O
contact	O
with	O
the	O
development	O
of	O
radio	O
understood	O
that	O
the	O
radio	O
art	O
was	O
the	O
product	O
of	O
experiment	O
and	O
work	O
based	O
on	O
physical	O
reasoning	O
,	O
rather	O
than	O
on	O
the	O
mathematicians	O
'	O
calculations	O
and	O
formulae	O
(	O
known	O
today	O
as	O
part	O
of	O
"	O
mathematical	O
physics	O
"	O
)	O
.	O
The	O
FCC	ORGANIZATION
's	O
decision	O
to	O
use	O
Armstrong	PERSON
's	O
FM	O
system	O
as	O
the	O
standard	O
for	O
NTSC	O
television	O
sound	O
gave	O
Armstrong	PERSON
another	O
chance	O
at	O
royalty	O
payments	O
.	O
However	O
,	O
RCA	ORGANIZATION
refused	O
to	O
pay	O
royalties	O
and	O
encouraged	O
other	O
television	O
makers	O
not	O
to	O
pay	O
them	O
either	O
.	O
He	O
was	O
awarded	O
the	O
1941	O
Franklin	ORGANIZATION
Medal	ORGANIZATION
.	O
He	O
received	O
in	O
1942	O
the	O
AIEEs	ORGANIZATION
Edison	MISC
Medal	MISC
"	O
For	O
distinguished	O
contributions	O
to	O
the	O
art	O
of	O
electric	O
communication	O
,	O
notably	O
the	O
regenerative	O
circuit	O
,	O
the	O
superheterodyne	O
,	O
and	O
frequency	O
modulation	O
"	O
.	O
The	O
ITU	ORGANIZATION
added	O
him	O
to	O
its	O
roster	O
of	O
great	O
inventors	O
of	O
electricity	O
in	O
1955	O
.	O
It	O
is	O
now	O
home	O
to	O
the	O
Goddard	LOCATION
Institute	LOCATION
for	LOCATION
Space	LOCATION
Studies	LOCATION
,	O
a	O
research	O
institute	O
jointly	O
operated	O
by	O
Columbia	ORGANIZATION
and	O
the	O
National	ORGANIZATION
Aeronautics	ORGANIZATION
and	ORGANIZATION
Space	ORGANIZATION
Administration	ORGANIZATION
dedicated	O
to	O
atmospheric	O
and	O
climate	O
science	O
.	O
A	O
storefront	O
in	O
the	O
corner	O
of	O
the	O
building	O
house	O
's	O
Tom	LOCATION
's	LOCATION
Restaurant	LOCATION
,	O
a	O
longtime	O
neighborhood	O
fixture	O
which	O
was	O
featured	O
as	O
the	O
fictional	O
diner	O
"	O
Monk	O
's	O
"	O
in	O
establishing	O
shots	O
in	O
the	O
television	O
series	O
"	O
Seinfeld	MISC
"	O
.	O
The	O
same	O
restaurant	O
also	O
inspired	O
Susanne	PERSON
Vega	PERSON
's	O
song	O
"	O
Tom	MISC
's	MISC
Diner	MISC
"	O
.	O
Armstrong	PERSON
received	O
42	O
patents	O
in	O
total	O
;	O
a	O
selection	O
are	O
listed	O
below	O
:	O
