The	O
false	O
belief	O
that	O
medieval	O
Christianity	MISC
believed	O
in	O
a	O
flat	O
earth	O
has	O
been	O
referred	O
to	O
as	O
"	O
The	O
Myth	MISC
of	MISC
the	MISC
Flat	MISC
Earth	MISC
"	O
.	O
In	O
1945	O
,	O
it	O
was	O
listed	O
by	O
the	O
Historical	ORGANIZATION
Association	ORGANIZATION
(	O
of	O
Britain	LOCATION
)	O
as	O
the	O
second	O
of	O
20	O
in	O
a	O
pamphlet	O
on	O
common	O
errors	O
in	O
history	O
.	O
This	O
is	O
also	O
true	O
of	O
Zhang	PERSON
Heng	PERSON
's	O
often	O
quoted	O
theory	O
(	O
78-139	O
AD	O
)	O
that	O
the	O
universe	O
was	O
in	O
the	O
oval	O
shape	O
of	O
a	O
hen	O
's	O
egg	O
,	O
and	O
the	O
earth	O
itself	O
was	O
like	O
the	O
curved	O
yolk	O
within	O
:	O
Likewise	O
,	O
the	O
13th	O
century	O
scholar	O
Li	PERSON
Ye	PERSON
,	O
arguing	O
that	O
the	O
movements	O
of	O
the	O
round	O
heaven	O
would	O
be	O
hindered	O
by	O
a	O
square	O
earth	O
,	O
did	O
not	O
advocate	O
a	O
spherical	O
earth	O
,	O
but	O
rather	O
that	O
its	O
edge	O
should	O
be	O
rounded	O
off	O
so	O
as	O
to	O
be	O
circular	O
.	O
Beginning	O
in	O
ancient	O
Greece	LOCATION
,	O
the	O
concept	O
of	O
a	O
flat	O
earth	O
was	O
largely	O
rejected	O
in	O
educated	O
circles	O
and	O
appears	O
only	O
rarely	O
.	O
Pythagoras	PERSON
in	O
the	O
6th	O
century	O
BC	O
,	O
apparently	O
on	O
aesthetic	O
grounds	O
,	O
held	O
that	O
all	O
the	O
celestial	O
bodies	O
were	O
spherical	O
.	O
He	O
also	O
remarked	O
that	O
observers	O
can	O
see	O
further	O
when	O
their	O
eyes	O
are	O
elevated	O
,	O
and	O
cited	O
a	O
line	O
from	O
the	O
Odyssey	MISC
as	O
indicating	O
that	O
the	O
poet	O
Homer	PERSON
was	O
already	O
aware	O
of	O
this	O
as	O
early	O
as	O
the	O
7th	O
or	O
8th	O
century	O
BC	O
.	O
Eratosthenes	PERSON
used	O
rough	O
estimates	O
and	O
round	O
numbers	O
,	O
but	O
depending	O
on	O
the	O
length	O
of	O
the	O
stadion	O
,	O
his	O
result	O
is	O
within	O
a	O
margin	O
of	O
between	O
2	O
%	O
and	O
20	O
%	O
of	O
the	O
actual	O
meridional	O
circumference	O
,	O
40008	O
kilometres	O
(	O
24860	O
mi	O
)	O
.	O
Lucretius	PERSON
(	O
1st	O
.	O
After	O
his	O
conversion	O
to	O
Christianity	MISC
,	O
Lactantius	PERSON
(	O
245	O
--	O
325	O
)	O
became	O
a	O
trenchant	O
critic	O
of	O
all	O
pagan	O
philosophy	O
.	O
Diodorus	PERSON
,	O
Severian	PERSON
,	O
and	O
Cosmas	PERSON
Indicopleustes	PERSON
,	O
but	O
also	O
Chrysostom	PERSON
,	O
belonged	O
just	O
to	O
this	O
latter	O
tradition	O
.	O
However	O
there	O
is	O
no	O
record	O
of	O
a	O
globe	O
as	O
a	O
representation	O
of	O
the	O
earth	O
since	O
ancient	O
times	O
till	O
that	O
of	O
Martin	PERSON
Behaim	PERSON
in	O
1492	O
.	O
By	O
the	O
11th	O
century	O
Europe	LOCATION
had	O
learned	O
of	O
Islamic	MISC
astronomy	MISC
.	O
The	O
Renaissance	MISC
of	MISC
the	MISC
12th	MISC
century	MISC
from	O
about	O
1070	O
started	O
an	O
intellectual	O
revitalization	O
of	O
Europe	LOCATION
with	O
strong	O
philosophical	O
and	O
scientific	O
roots	O
,	O
and	O
increased	O
interest	O
in	O
natural	O
philosophy	O
.	O
The	O
author	O
also	O
discusses	O
the	O
existence	O
of	O
antipodes	O
--	O
and	O
he	O
notes	O
that	O
they	O
(	O
if	O
they	O
exist	O
)	O
will	O
see	O
the	O
Sun	O
in	O
the	O
north	O
of	O
the	O
middle	O
of	O
the	O
day	O
,	O
and	O
that	O
they	O
will	O
have	O
opposite	O
seasons	O
of	O
the	O
people	O
living	O
in	O
the	O
Northern	LOCATION
Hemisphere	LOCATION
.	O
In	O
the	O
17th	O
century	O
,	O
the	O
idea	O
of	O
a	O
spherical	O
earth	O
spread	O
in	O
China	LOCATION
due	O
to	O
the	O
influence	O
of	O
the	O
Jesuits	ORGANIZATION
,	O
who	O
held	O
high	O
positions	O
as	O
astronomers	O
at	O
the	O
imperial	O
court	O
.	O
Previous	O
editions	O
of	O
Thomas	PERSON
Bailey	PERSON
's	O
The	MISC
American	MISC
Pageant	MISC
stated	O
that	O
"	O
The	O
superstitious	O
sailors	O
...	O
grew	O
increasingly	O
mutinous	O
...	O
because	O
they	O
were	O
fearful	O
of	O
sailing	O
over	O
the	O
edge	O
of	O
the	O
world	O
"	O
;	O
however	O
,	O
no	O
such	O
historical	O
account	O
is	O
known	O
.	O
However	O
the	O
incidents	O
he	O
cites	O
of	O
opposition	O
by	O
theologians	O
at	O
the	O
1486	O
Salamanca	LOCATION
meeting	O
were	O
mostly	O
recorded	O
far	O
before	O
this	O
.	O
e.g.	O
Ferdinand	PERSON
Columbus	PERSON
,	O
writing	O
in	O
around	O
1538	O
,	O
says	O
Rowbotham	PERSON
also	O
produced	O
studies	O
that	O
purported	O
to	O
show	O
the	O
effects	O
of	O
ships	O
disappearing	O
below	O
the	O
horizon	O
could	O
be	O
explained	O
by	O
the	O
laws	O
of	O
perspective	O
in	O
relation	O
to	O
the	O
human	O
eye	O
.	O
Paul	PERSON
Kruger	PERSON
,	O
President	O
of	O
the	O
Transvaal	LOCATION
Republic	LOCATION
,	O
advanced	O
the	O
same	O
view	O
:	O
"	O
You	O
do	O
n't	O
mean	O
round	O
the	O
world	O
,	O
it	O
is	O
impossible	O
!	O
When	O
the	O
airship	MISC
Italia	MISC
disappeared	O
on	O
an	O
expedition	O
to	O
the	O
North	LOCATION
Pole	LOCATION
in	O
1928	O
he	O
warned	O
the	O
world	O
's	O
press	O
that	O
it	O
had	O
sailed	O
over	O
the	O
edge	O
of	O
the	O
world	O
.	O
In	O
C.	PERSON
S.	PERSON
Lewis	PERSON
'	O
The	MISC
Voyage	MISC
of	MISC
the	MISC
Dawn	MISC
Treader	MISC
the	O
fictional	O
world	O
of	O
Narnia	LOCATION
is	O
"	O
round	O
like	O
a	O
table	O
"	O
(	O
i.e.	O
,	O
flat	O
)	O
,	O
not	O
"	O
round	O
like	O
a	O
ball	O
"	O
,	O
and	O
the	O
characters	O
sail	O
toward	O
the	O
edge	O
of	O
this	O
world	O
.	O
Terry	PERSON
Pratchett	PERSON
's	O
Discworld	MISC
novels	O
(	O
1983	O
onwards	O
)	O
are	O
set	O
on	O
a	O
flat	O
,	O
disc-shaped	O
world	O
that	O
rests	O
on	O
the	O
backs	O
of	O
four	O
huge	O
elephants	O
that	O
stand	O
on	O
the	O
back	O
of	O
an	O
enormous	O
turtle	O
.	O
The	O
satiric	O
nature	O
of	O
the	O
piece	O
is	O
also	O
made	O
clear	O
by	O
a	O
comparison	O
to	O
Bartlett	PERSON
's	O
other	O
publications	O
,	O
which	O
mainly	O
advocate	O
the	O
necessity	O
of	O
curbing	O
population	O
growth	O
.	O
