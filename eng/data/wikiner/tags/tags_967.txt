The	O
Arabian	LOCATION
Sea	LOCATION
(	O
Sanskrit	MISC
:	O
सिन्धु	O
सागर	O
;	O
Arabic	MISC
:	O
بحر	O
العرب	O
‎	O
,	O
;	O
Kannada	MISC
:	O
ಅರೇಬಿಯಾ	O
ಸಮುದ್ರ	O
,	O
(	O
Arabia	LOCATION
samudra	O
)	O
;	O
Marathi	MISC
:	O
अरबी	O
समुद्र	O
,	O
;	O
Malayalam	MISC
:	O
അറബിക്കടല്‍	O
,	O
;	O
Persian	MISC
:	O
دریای	O
عرب	O
,	O
)	O
is	O
a	O
region	O
of	O
the	O
Indian	LOCATION
Ocean	LOCATION
bounded	O
on	O
the	O
east	O
by	O
India	LOCATION
,	O
on	O
the	O
north	O
by	O
Pakistan	LOCATION
and	O
Iran	LOCATION
,	O
on	O
the	O
west	O
by	O
the	O
Arabian	LOCATION
Peninsula	LOCATION
,	O
on	O
the	O
south	O
,	O
approximately	O
,	O
by	O
a	O
line	O
between	O
Cape	LOCATION
Guardafui	LOCATION
in	O
the	O
northeastern	O
Somalia	LOCATION
and	O
Kanyakumari	LOCATION
(	O
Cape	LOCATION
Comorin	LOCATION
)	O
in	O
India	LOCATION
.	O
The	O
Arabian	LOCATION
Sea	LOCATION
's	O
surface	O
area	O
is	O
about	O
3862000	O
km	O
2	O
(	O
1491130	O
sq	O
mi	O
)	O
.	O
The	O
Arabian	LOCATION
Sea	LOCATION
has	O
two	O
important	O
branches	O
--	O
the	O
Gulf	LOCATION
of	LOCATION
Aden	LOCATION
in	O
the	O
southwest	O
,	O
connecting	O
with	O
the	O
Red	LOCATION
Sea	LOCATION
through	O
the	O
strait	O
of	O
Bab-el-Mandeb	LOCATION
;	O
and	O
the	O
Gulf	LOCATION
of	LOCATION
Oman	LOCATION
to	O
the	O
northwest	O
,	O
connecting	O
with	O
the	O
Persian	LOCATION
Gulf	LOCATION
.	O
There	O
are	O
also	O
the	O
gulfs	O
of	O
Cambay	LOCATION
and	O
Kutch	LOCATION
on	O
the	O
Indian	MISC
coast	O
.	O
The	O
largest	O
islands	O
in	O
the	O
Arabian	LOCATION
Sea	LOCATION
are	O
Socotra	LOCATION
(	O
off	O
the	O
Horn	LOCATION
of	LOCATION
Africa	LOCATION
)	O
and	O
Masirah	LOCATION
(	O
off	O
the	O
Omani	LOCATION
coast	O
)	O
as	O
well	O
as	O
the	O
Lakshadweep	LOCATION
archipelago	O
off	O
the	O
Indian	MISC
coast	O
.	O
The	O
countries	O
with	O
coastlines	O
on	O
the	O
Arabian	LOCATION
Sea	LOCATION
are	O
Somalia	LOCATION
,	O
Djibouti	LOCATION
,	O
Yemen	LOCATION
,	O
Oman	LOCATION
,	O
Iran	LOCATION
,	O
Pakistan	LOCATION
,	O
India	LOCATION
and	O
the	O
Maldives	LOCATION
.	O
There	O
are	O
several	O
large	O
cities	O
on	O
the	O
Arabian	LOCATION
Sea	LOCATION
coast	O
including	O
Aden	LOCATION
,	O
Muscat	LOCATION
,	O
Karachi	LOCATION
,	O
Surat	LOCATION
,	O
Mumbai	LOCATION
,	O
Mangalore	LOCATION
,	O
Kozhikode	LOCATION
,	O
Kochi	LOCATION
and	O
Thiruvananthapuram	LOCATION
.	O
Later	O
the	O
kingdom	LOCATION
of	LOCATION
Axum	LOCATION
arose	O
in	O
Ethiopia	LOCATION
to	O
rule	O
a	O
mercantile	O
empire	O
rooted	O
in	O
the	O
trade	O
with	O
Europe	LOCATION
via	O
Alexandria	LOCATION
.	O
The	O
sea	O
forms	O
part	O
of	O
the	O
chief	O
shipping	O
route	O
between	O
Europe	LOCATION
and	O
India	LOCATION
via	O
the	O
Suez	LOCATION
Canal	LOCATION
,	O
which	O
links	O
the	O
Red	LOCATION
Sea	LOCATION
with	O
the	O
Mediterranean	LOCATION
Sea	LOCATION
.	O
