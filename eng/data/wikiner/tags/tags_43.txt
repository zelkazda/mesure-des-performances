Some	O
jurisdictions	O
have	O
specialized	O
appellate	O
courts	O
,	O
such	O
as	O
the	O
Texas	ORGANIZATION
Court	ORGANIZATION
of	ORGANIZATION
Criminal	ORGANIZATION
Appeals	ORGANIZATION
,	O
which	O
only	O
hears	O
appeals	O
raised	O
in	O
criminal	O
cases	O
,	O
and	O
the	O
United	ORGANIZATION
States	ORGANIZATION
Court	ORGANIZATION
of	ORGANIZATION
Appeals	ORGANIZATION
for	ORGANIZATION
the	ORGANIZATION
Federal	ORGANIZATION
Circuit	ORGANIZATION
,	O
which	O
has	O
general	O
jurisdiction	O
but	O
derives	O
most	O
of	O
its	O
caseload	O
from	O
patent	O
cases	O
,	O
on	O
the	O
one	O
hand	O
,	O
and	O
appeals	O
from	O
the	O
Court	ORGANIZATION
of	ORGANIZATION
Federal	ORGANIZATION
Claims	ORGANIZATION
on	O
the	O
other	O
.	O
For	O
example	O
,	O
in	O
the	O
United	LOCATION
States	LOCATION
,	O
both	O
state	O
and	O
federal	O
appellate	O
courts	O
are	O
usually	O
restricted	O
to	O
examining	O
whether	O
the	O
court	O
below	O
made	O
the	O
correct	O
legal	O
determinations	O
,	O
rather	O
than	O
hearing	O
direct	O
evidence	O
and	O
determining	O
what	O
the	O
facts	O
of	O
the	O
case	O
were	O
.	O
Furthermore	O
,	O
U.S.	LOCATION
appellate	O
courts	O
are	O
usually	O
restricted	O
to	O
hearing	O
appeals	O
based	O
on	O
matters	O
that	O
were	O
originally	O
brought	O
up	O
before	O
the	O
trial	O
court	O
.	O
In	O
most	O
U.S.	LOCATION
states	O
,	O
and	O
in	O
U.S.	LOCATION
federal	O
courts	O
,	O
parties	O
before	O
the	O
court	O
are	O
allowed	O
one	O
appeal	O
as	O
of	O
right	O
.	O
