Milne	PERSON
was	O
a	O
noted	O
writer	O
,	O
primarily	O
as	O
a	O
playwright	O
,	O
before	O
the	O
huge	O
success	O
of	O
Pooh	MISC
overshadowed	O
all	O
his	O
previous	O
work	O
.	O
One	O
of	O
his	O
teachers	O
was	O
H.	PERSON
G.	PERSON
Wells	PERSON
who	O
taught	O
there	O
in	O
1889	O
--	O
90	O
.	O
While	O
there	O
,	O
he	O
edited	O
and	O
wrote	O
for	O
Granta	MISC
,	O
a	O
student	O
magazine	O
.	O
Wodehouse	PERSON
got	O
some	O
revenge	O
on	O
his	O
former	O
friend	O
by	O
creating	O
fatuous	O
parodies	O
of	O
the	O
Christopher	PERSON
Robin	PERSON
poems	O
in	O
some	O
of	O
his	O
later	O
stories	O
,	O
and	O
claiming	O
that	O
Milne	PERSON
"	O
was	O
probably	O
jealous	O
of	O
all	O
other	O
writers	O
...	O
.	O
Milne	PERSON
contributed	O
humorous	O
verse	O
and	O
whimsical	O
essays	O
to	O
Punch	MISC
,	O
joining	O
the	O
staff	O
in	O
1906	O
and	O
becoming	O
an	O
assistant	O
editor	O
.	O
During	O
this	O
period	O
he	O
published	O
18	O
plays	O
and	O
3	O
novels	O
,	O
including	O
the	O
murder	O
mystery	O
The	MISC
Red	MISC
House	MISC
Mystery	MISC
(	O
1922	O
)	O
.	O
His	O
son	O
was	O
born	O
in	O
August	O
1920	O
and	O
in	O
1924	O
Milne	PERSON
produced	O
a	O
collection	O
of	O
children	O
's	O
poems	O
When	MISC
We	MISC
Were	MISC
Very	MISC
Young	MISC
,	O
which	O
were	O
illustrated	O
by	O
Punch	MISC
staff	O
cartoonist	O
E.	PERSON
H.	PERSON
Shepard	PERSON
.	O
Some	O
of	O
these	O
films	O
survive	O
in	O
the	O
archives	O
of	O
the	O
British	ORGANIZATION
Film	ORGANIZATION
Institute	ORGANIZATION
.	O
Looking	O
back	O
on	O
this	O
period	O
(	O
in	O
1926	O
)	O
Milne	PERSON
observed	O
that	O
when	O
he	O
told	O
his	O
agent	O
that	O
he	O
was	O
going	O
to	O
write	O
a	O
detective	O
story	O
,	O
he	O
was	O
told	O
that	O
what	O
the	O
country	O
wanted	O
from	O
a	O
"	O
Punch	MISC
humorist	O
"	O
was	O
a	O
humorous	O
story	O
;	O
when	O
two	O
years	O
later	O
he	O
said	O
he	O
was	O
writing	O
nursery	O
rhymes	O
,	O
his	O
agent	O
and	O
publisher	O
were	O
convinced	O
he	O
should	O
write	O
another	O
detective	O
story	O
;	O
and	O
after	O
another	O
two	O
years	O
he	O
was	O
being	O
told	O
that	O
writing	O
a	O
detective	O
story	O
would	O
be	O
in	O
the	O
worst	O
of	O
taste	O
given	O
the	O
demand	O
for	O
children	O
's	O
books	O
.	O
Milne	PERSON
is	O
most	O
famous	O
for	O
his	O
two	O
Pooh	MISC
books	O
about	O
a	O
boy	O
named	O
Christopher	PERSON
Robin	PERSON
after	O
his	O
son	O
,	O
and	O
various	O
characters	O
inspired	O
by	O
his	O
son	O
's	O
stuffed	O
animals	O
,	O
most	O
notably	O
the	O
bear	O
named	O
Winnie-the-Pooh	MISC
.	O
"	O
The	O
pooh	O
"	O
comes	O
from	O
a	O
swan	O
called	O
"	O
Pooh	MISC
"	O
.	O
Winnie-the-Pooh	MISC
was	O
published	O
in	O
1926	O
,	O
followed	O
by	O
The	MISC
House	MISC
at	MISC
Pooh	MISC
Corner	MISC
in	O
1928	O
.	O
A	O
second	O
collection	O
of	O
nursery	O
rhymes	O
,	O
Now	MISC
We	MISC
Are	MISC
Six	MISC
,	O
was	O
published	O
in	O
1927	O
.	O
All	O
three	O
books	O
were	O
illustrated	O
by	O
E.	PERSON
H.	PERSON
Shepard	PERSON
.	O
Milne	PERSON
also	O
published	O
four	O
plays	O
in	O
this	O
period	O
.	O
But	O
once	O
Milne	PERSON
had	O
,	O
in	O
his	O
own	O
words	O
,	O
"	O
said	O
goodbye	O
to	O
all	O
that	O
in	O
70,000	O
words	O
"	O
(	O
the	O
approximate	O
length	O
of	O
his	O
four	O
principal	O
children	O
's	O
books	O
)	O
,	O
he	O
had	O
no	O
intention	O
of	O
producing	O
any	O
reworkings	O
lacking	O
in	O
originality	O
,	O
given	O
that	O
one	O
of	O
the	O
sources	O
of	O
inspiration	O
,	O
his	O
son	O
,	O
was	O
growing	O
older	O
.	O
He	O
also	O
adapted	O
Kenneth	PERSON
Grahame	PERSON
's	O
novel	O
The	MISC
Wind	MISC
in	MISC
the	MISC
Willows	MISC
for	O
the	O
stage	O
as	O
Toad	MISC
of	MISC
Toad	MISC
Hall	MISC
.	O
Several	O
of	O
Milne	PERSON
's	O
children	O
's	O
poems	O
were	O
set	O
to	O
music	O
by	O
the	O
composer	O
Harold	PERSON
Fraser-Simson	PERSON
.	O
Milne	PERSON
wrote	O
over	O
25	O
plays	O
,	O
including	O
:	O
