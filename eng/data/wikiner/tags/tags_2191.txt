Cambridge	LOCATION
is	O
a	O
city	O
in	O
Middlesex	LOCATION
County	LOCATION
,	O
Massachusetts	LOCATION
,	O
U.S.	LOCATION
,	O
in	O
the	O
Greater	LOCATION
Boston	LOCATION
area	O
.	O
Notably	O
,	O
Cambridge	LOCATION
is	O
home	O
to	O
two	O
internationally	O
prominent	O
universities	O
,	O
Harvard	ORGANIZATION
University	ORGANIZATION
and	O
the	O
Massachusetts	ORGANIZATION
Institute	ORGANIZATION
of	ORGANIZATION
Technology	ORGANIZATION
.	O
It	O
is	O
the	O
fourth	O
most	O
populous	O
city	O
in	O
the	O
state	O
,	O
behind	O
Boston	LOCATION
,	O
Worcester	LOCATION
,	O
and	O
Springfield	LOCATION
.	O
Cambridge	LOCATION
is	O
one	O
of	O
the	O
two	O
county	O
seats	O
of	O
Middlesex	LOCATION
County	LOCATION
(	O
Lowell	LOCATION
is	O
the	O
other	O
)	O
.	O
The	O
site	O
for	O
what	O
would	O
become	O
Cambridge	LOCATION
was	O
chosen	O
in	O
December	O
1630	O
,	O
because	O
it	O
was	O
located	O
safely	O
up	O
river	O
from	O
Boston	LOCATION
Harbor	LOCATION
,	O
which	O
made	O
it	O
easily	O
defensible	O
from	O
attacks	O
by	O
enemy	O
ships	O
.	O
Official	O
Mass.	LOCATION
records	O
show	O
the	O
name	O
capitalized	O
as	O
Newe	LOCATION
Towne	LOCATION
by	O
1632	O
.	O
The	O
original	O
village	O
site	O
is	O
in	O
the	O
heart	O
of	O
today	O
's	O
Harvard	LOCATION
Square	LOCATION
.	O
The	O
town	O
included	O
a	O
much	O
larger	O
area	O
than	O
the	O
present	O
city	O
,	O
with	O
various	O
outlying	O
parts	O
becoming	O
independent	O
towns	O
over	O
the	O
years	O
:	O
Newton	LOCATION
in	O
1688	O
,	O
Lexington	LOCATION
in	O
1712	O
,	O
and	O
both	O
West	LOCATION
Cambridge	LOCATION
(	O
originally	O
Menotomy	LOCATION
)	O
and	O
Brighton	LOCATION
(	O
Little	O
Cambridge	LOCATION
)	O
in	O
1807	O
.	O
West	LOCATION
Cambridge	LOCATION
was	O
later	O
renamed	O
Arlington	LOCATION
,	O
in	O
1867	O
,	O
and	O
Brighton	LOCATION
was	O
later	O
annexed	O
by	O
Boston	LOCATION
,	O
in	O
1874	O
.	O
In	O
1636	O
Harvard	ORGANIZATION
College	ORGANIZATION
was	O
founded	O
by	O
the	O
colony	O
to	O
train	O
ministers	O
and	O
the	O
new	O
town	O
was	O
chosen	O
for	O
its	O
site	O
by	O
Thomas	PERSON
Dudley	PERSON
.	O
By	O
1638	O
the	O
name	O
"	O
Newe	LOCATION
Towne	LOCATION
"	O
had	O
"	O
compacted	O
by	O
usage	O
into	O
'	O
Newtowne	LOCATION
'	O
.	O
"	O
The	O
first	O
president	O
(	O
Henry	PERSON
Dunster	PERSON
)	O
,	O
the	O
first	O
benefactor	O
(	O
John	PERSON
Harvard	PERSON
)	O
,	O
and	O
the	O
first	O
schoolmaster	O
(	O
Nathaniel	PERSON
Eaton	PERSON
)	O
of	O
Harvard	LOCATION
were	O
all	O
Cambridge	ORGANIZATION
University	ORGANIZATION
alumni	O
,	O
as	O
was	O
the	O
then	O
ruling	O
(	O
and	O
first	O
)	O
governor	O
of	O
the	O
Massachusetts	LOCATION
Bay	LOCATION
Colony	LOCATION
,	O
John	PERSON
Winthrop	PERSON
.	O
It	O
was	O
Governor	O
Thomas	PERSON
Dudley	PERSON
who	O
in	O
1650	O
signed	O
the	O
charter	O
creating	O
the	O
corporation	O
which	O
still	O
governs	O
Harvard	ORGANIZATION
College	ORGANIZATION
.	O
Cambridge	LOCATION
grew	O
slowly	O
as	O
an	O
agricultural	O
village	O
eight	O
miles	O
(	O
13	O
km	O
)	O
by	O
road	O
from	O
Boston	LOCATION
,	O
the	O
capital	O
of	O
the	O
colony	O
.	O
Between	O
1790	O
and	O
1840	O
,	O
Cambridge	LOCATION
began	O
to	O
grow	O
rapidly	O
,	O
with	O
the	O
construction	O
of	O
the	O
West	LOCATION
Boston	LOCATION
Bridge	LOCATION
in	O
1792	O
,	O
that	O
connected	O
Cambridge	LOCATION
directly	O
to	O
Boston	LOCATION
,	O
making	O
it	O
no	O
longer	O
necessary	O
to	O
travel	O
eight	O
miles	O
(	O
13	O
km	O
)	O
through	O
the	O
Boston	LOCATION
Neck	LOCATION
,	O
Roxbury	LOCATION
,	O
and	O
Brookline	LOCATION
to	O
cross	O
the	O
Charles	LOCATION
River	LOCATION
.	O
In	O
the	O
mid-1800s	O
,	O
Cambridge	LOCATION
was	O
the	O
center	O
of	O
a	O
literary	O
revolution	O
when	O
it	O
gave	O
the	O
country	O
a	O
new	O
identity	O
through	O
poetry	O
and	O
literature	O
.	O
In	O
addition	O
,	O
railroads	O
crisscrossed	O
the	O
town	O
during	O
the	O
same	O
era	O
,	O
leading	O
to	O
the	O
development	O
of	O
Porter	LOCATION
Square	LOCATION
as	O
well	O
as	O
the	O
creation	O
of	O
neighboring	O
town	O
Somerville	LOCATION
from	O
the	O
formerly	O
rural	O
parts	O
of	O
Charlestown	LOCATION
.	O
Cambridge	LOCATION
was	O
incorporated	O
as	O
a	O
city	O
in	O
1846	O
.	O
Its	O
commercial	O
center	O
also	O
began	O
to	O
shift	O
from	O
Harvard	LOCATION
Square	LOCATION
to	O
Central	LOCATION
Square	LOCATION
,	O
which	O
became	O
the	O
downtown	O
of	O
the	O
city	O
.	O
and	O
Alewife	LOCATION
Brook	LOCATION
;	O
the	O
ice-cutting	O
industry	O
launched	O
by	O
Frederic	PERSON
Tudor	PERSON
on	O
Fresh	LOCATION
Pond	LOCATION
;	O
and	O
the	O
carving	O
up	O
of	O
the	O
last	O
estates	O
into	O
residential	O
subdivisions	O
to	O
provide	O
housing	O
to	O
the	O
thousands	O
of	O
immigrants	O
that	O
arrived	O
to	O
work	O
in	O
the	O
new	O
industries	O
.	O
Among	O
the	O
largest	O
businesses	O
located	O
in	O
Cambridge	LOCATION
was	O
the	O
firm	O
of	O
Carter	ORGANIZATION
's	ORGANIZATION
Ink	ORGANIZATION
Company	ORGANIZATION
,	O
whose	O
neon	O
sign	O
long	O
adorned	O
the	O
Charles	LOCATION
River	LOCATION
and	O
which	O
was	O
for	O
many	O
years	O
the	O
largest	O
manufacturer	O
of	O
ink	O
in	O
the	O
world	O
.	O
By	O
1920	O
,	O
Cambridge	LOCATION
was	O
one	O
of	O
the	O
main	O
industrial	O
cities	O
of	O
New	LOCATION
England	LOCATION
,	O
with	O
nearly	O
120,000	O
residents	O
.	O
Harvard	ORGANIZATION
University	ORGANIZATION
had	O
always	O
been	O
important	O
in	O
the	O
city	O
(	O
both	O
as	O
a	O
landowner	O
and	O
as	O
an	O
institution	O
)	O
,	O
but	O
it	O
began	O
to	O
play	O
a	O
more	O
dominant	O
role	O
in	O
the	O
city	O
's	O
life	O
and	O
culture	O
.	O
The	O
end	O
of	O
rent	O
control	O
in	O
1994	O
prompted	O
many	O
Cambridge	LOCATION
renters	O
to	O
move	O
to	O
housing	O
that	O
was	O
more	O
affordable	O
,	O
in	O
Somerville	LOCATION
and	O
other	O
communities	O
.	O
In	O
2005	O
,	O
a	O
reassessment	O
of	O
residential	O
property	O
values	O
resulted	O
in	O
a	O
disproportionate	O
number	O
of	O
houses	O
owned	O
by	O
non-affluent	O
people	O
jumping	O
in	O
value	O
relative	O
to	O
other	O
houses	O
,	O
with	O
hundreds	O
having	O
their	O
property	O
tax	O
increased	O
by	O
over	O
100	O
%	O
;	O
this	O
forced	O
many	O
homeowners	O
in	O
Cambridge	LOCATION
to	O
move	O
elsewhere	O
.	O
As	O
of	O
2006	O
,	O
Cambridge	LOCATION
's	O
mix	O
of	O
amenities	O
and	O
proximity	O
to	O
Boston	LOCATION
has	O
kept	O
housing	O
prices	O
relatively	O
stable	O
.	O
According	O
to	O
the	O
United	ORGANIZATION
States	ORGANIZATION
Census	ORGANIZATION
Bureau	ORGANIZATION
,	O
the	O
city	O
has	O
a	O
total	O
area	O
of	O
7.1	O
square	O
miles	O
(	O
18.5	O
km²	O
)	O
,	O
of	O
which	O
6.4	O
square	O
miles	O
(	O
16.7	O
km²	O
)	O
of	O
it	O
is	O
land	O
and	O
0.7	O
square	O
miles	O
(	O
1.8	O
km²	O
)	O
of	O
it	O
(	O
9.82	O
%	O
)	O
is	O
water	O
.	O
Cambridge	LOCATION
is	O
located	O
in	O
eastern	LOCATION
Massachusetts	LOCATION
,	O
bordered	O
by	O
:	O
The	O
border	O
between	O
Cambridge	LOCATION
and	O
the	O
neighboring	O
city	O
of	O
Somerville	LOCATION
passes	O
through	O
densely	O
populated	O
neighborhoods	O
which	O
are	O
connected	O
by	O
the	O
MBTA	LOCATION
Red	LOCATION
Line	LOCATION
.	O
Some	O
of	O
the	O
main	O
squares	O
,	O
Inman	LOCATION
,	O
Porter	LOCATION
,	O
and	O
to	O
a	O
lesser	O
extent	O
,	O
Harvard	LOCATION
,	O
are	O
very	O
close	O
to	O
the	O
city	O
line	O
,	O
as	O
are	O
Somerville	LOCATION
's	O
Union	LOCATION
and	O
Davis	LOCATION
Squares	LOCATION
.	O
The	O
residential	O
neighborhoods	O
(	O
map	O
)	O
in	O
Cambridge	LOCATION
border	O
,	O
but	O
are	O
not	O
defined	O
by	O
the	O
squares	O
.	O
Consisting	O
largely	O
of	O
densely	O
built	O
residential	O
space	O
,	O
Cambridge	LOCATION
lacks	O
significant	O
tracts	O
of	O
public	O
parkland	O
.	O
At	O
the	O
western	O
edge	O
of	O
Cambridge	LOCATION
,	O
the	O
cemetery	O
is	O
well	O
known	O
as	O
the	O
first	O
garden	O
cemetery	O
,	O
for	O
its	O
distinguished	O
inhabitants	O
,	O
for	O
its	O
superb	O
landscaping	O
(	O
the	O
oldest	O
planned	O
landscape	O
in	O
the	O
country	O
)	O
,	O
and	O
as	O
a	O
first-rate	O
arboretum	O
.	O
Although	O
known	O
as	O
a	O
Cambridge	LOCATION
landmark	O
,	O
much	O
of	O
the	O
cemetery	O
lies	O
within	O
the	O
bounds	O
of	O
Watertown	LOCATION
.	O
The	O
population	O
density	O
was	O
15,766.1	O
people	O
per	O
square	O
mile	O
(	O
6,086.1/km²	O
)	O
,	O
making	O
Cambridge	LOCATION
the	O
fifth	O
most	O
densely	O
populated	O
city	O
in	O
the	O
U.S.	LOCATION
and	O
the	O
second	O
most	O
densely	O
populated	O
city	O
in	O
Massachusetts	LOCATION
behind	O
neighboring	O
Somerville	LOCATION
.	O
Its	O
residents	O
jokingly	O
refer	O
to	O
it	O
as	O
"	O
The	O
People	LOCATION
's	LOCATION
Republic	LOCATION
of	LOCATION
Cambridge	LOCATION
.	O
"	O
Cambridge	LOCATION
is	O
noted	O
for	O
its	O
diverse	O
population	O
,	O
both	O
racially	O
and	O
economically	O
.	O
Residents	O
,	O
known	O
as	O
Cantabrigians	ORGANIZATION
,	O
range	O
from	O
affluent	O
MIT	ORGANIZATION
and	O
Harvard	ORGANIZATION
professors	O
to	O
working-class	O
families	O
to	O
immigrants	O
.	O
The	O
first	O
legal	O
applications	O
in	O
America	LOCATION
for	O
same-sex	O
marriage	O
licenses	O
were	O
issued	O
at	O
Cambridge	LOCATION
's	O
City	LOCATION
Hall	LOCATION
.	O
The	O
other	O
class	O
1	O
departments	O
in	O
New	LOCATION
England	LOCATION
are	O
in	O
Hartford	LOCATION
,	O
Connecticut	LOCATION
and	O
Milford	LOCATION
,	O
Conn.	LOCATION
.	O
Cambridge	LOCATION
is	O
a	O
county	O
seat	O
of	O
Middlesex	LOCATION
County	LOCATION
,	O
Mass.	LOCATION
,	O
along	O
with	O
Lowell	LOCATION
.	O
The	O
employees	O
of	O
Middlesex	LOCATION
County	LOCATION
courts	O
,	O
jails	O
,	O
registries	O
,	O
and	O
other	O
county	O
agencies	O
now	O
work	O
directly	O
for	O
the	O
state	O
.	O
Cambridge	LOCATION
is	O
perhaps	O
best	O
known	O
as	O
an	O
academic	O
and	O
intellectual	O
center	O
,	O
owing	O
to	O
its	O
colleges	O
and	O
universities	O
,	O
which	O
include	O
:	O
The	O
American	ORGANIZATION
Academy	ORGANIZATION
of	ORGANIZATION
Arts	ORGANIZATION
and	ORGANIZATION
Sciences	ORGANIZATION
is	O
also	O
based	O
in	O
Cambridge	LOCATION
.	O
In	O
2003	O
the	O
high	O
school	O
came	O
close	O
to	O
losing	O
its	O
educational	O
accreditation	O
when	O
it	O
was	O
placed	O
on	O
probation	O
by	O
the	O
New	ORGANIZATION
England	ORGANIZATION
Association	ORGANIZATION
of	ORGANIZATION
Schools	ORGANIZATION
and	ORGANIZATION
Colleges	ORGANIZATION
.	O
Both	O
Harvard	ORGANIZATION
and	O
MIT	ORGANIZATION
together	O
employ	O
about	O
20,000	O
.	O
As	O
a	O
cradle	O
of	O
technological	O
innovation	O
,	O
Cambridge	LOCATION
was	O
home	O
to	O
such	O
legendary	O
technology	O
firms	O
as	O
Analog	ORGANIZATION
Devices	ORGANIZATION
,	O
Akamai	ORGANIZATION
,	O
Bolt	ORGANIZATION
,	ORGANIZATION
Beranek	ORGANIZATION
,	ORGANIZATION
and	ORGANIZATION
Newman	ORGANIZATION
(	O
BBN	ORGANIZATION
Technologies	ORGANIZATION
)	O
,	O
General	ORGANIZATION
Radio	ORGANIZATION
(	O
later	O
GenRad	ORGANIZATION
)	O
,	O
Lotus	ORGANIZATION
Development	ORGANIZATION
Corporation	ORGANIZATION
(	O
now	O
part	O
of	O
IBM	ORGANIZATION
)	O
,	O
Polaroid	ORGANIZATION
,	O
Symbolics	MISC
,	O
Thinking	MISC
Machines	MISC
,	O
and	O
VMware	ORGANIZATION
.	O
In	O
1996	O
,	O
Polaroid	ORGANIZATION
,	O
Arthur	ORGANIZATION
D.	ORGANIZATION
Little	ORGANIZATION
,	O
and	O
Lotus	ORGANIZATION
were	O
all	O
top	O
employers	O
with	O
over	O
1,000	O
employees	O
in	O
Cambridge	LOCATION
,	O
but	O
declined	O
or	O
disappeared	O
a	O
few	O
years	O
later	O
.	O
In	O
2005	O
,	O
alongside	O
Harvard	LOCATION
and	O
MIT	ORGANIZATION
,	O
health	O
care	O
and	O
biotechnology	O
firms	O
such	O
as	O
Genzyme	ORGANIZATION
,	O
Biogen	ORGANIZATION
Idec	ORGANIZATION
,	O
and	O
Novartis	ORGANIZATION
dominate	O
the	O
city	O
economy	O
.	O
Biotech	O
firms	O
are	O
located	O
around	O
Kendall	LOCATION
Square	LOCATION
and	O
East	LOCATION
Cambridge	LOCATION
,	O
which	O
decades	O
ago	O
were	O
the	O
center	O
of	O
manufacturing	O
.	O
A	O
number	O
of	O
biotechnology	O
companies	O
are	O
also	O
located	O
in	O
University	LOCATION
Park	LOCATION
at	LOCATION
MIT	LOCATION
,	O
a	O
new	O
development	O
in	O
another	O
former	O
manufacturing	O
area	O
.	O
None	O
of	O
the	O
high	O
technology	O
firms	O
that	O
once	O
dominated	O
the	O
economy	O
was	O
among	O
the	O
25	O
largest	O
employers	O
in	O
2005	O
,	O
but	O
by	O
2008	O
Akamai	ORGANIZATION
and	O
ITA	ORGANIZATION
Software	ORGANIZATION
had	O
grown	O
to	O
be	O
among	O
the	O
largest	O
employers	O
.	O
Video	O
game	O
developer	O
Harmonix	ORGANIZATION
Music	ORGANIZATION
Systems	ORGANIZATION
is	O
based	O
in	O
Central	LOCATION
Square	LOCATION
.	O
The	O
city	O
is	O
also	O
the	O
New	LOCATION
England	LOCATION
headquarters	O
for	O
Miramax	ORGANIZATION
Films	ORGANIZATION
and	O
Time	ORGANIZATION
Warner	ORGANIZATION
Cable	ORGANIZATION
.	O
The	O
proximity	O
of	O
Cambridge	LOCATION
's	O
universities	O
has	O
also	O
made	O
the	O
city	O
a	O
center	O
for	O
nonprofit	O
groups	O
and	O
think	O
tanks	O
,	O
including	O
the	O
Lincoln	ORGANIZATION
Institute	ORGANIZATION
of	ORGANIZATION
Land	ORGANIZATION
Policy	ORGANIZATION
,	O
Cultural	ORGANIZATION
Survival	ORGANIZATION
,	O
and	O
One	ORGANIZATION
Laptop	ORGANIZATION
per	ORGANIZATION
Child	ORGANIZATION
.	O
Several	O
major	O
roads	O
lead	O
to	O
Cambridge	LOCATION
,	O
including	O
Route	LOCATION
2	LOCATION
,	O
Route	LOCATION
16	LOCATION
and	O
the	O
McGrath	LOCATION
Highway	LOCATION
(	O
Route	LOCATION
28	LOCATION
)	O
.	O
The	O
Massachusetts	LOCATION
Turnpike	LOCATION
does	O
not	O
pass	O
through	O
Cambridge	LOCATION
,	O
but	O
provides	O
access	O
by	O
an	O
exit	O
in	O
nearby	O
Allston	LOCATION
.	O
Route	LOCATION
2A	LOCATION
runs	O
the	O
length	O
of	O
the	O
city	O
,	O
chiefly	O
along	O
Massachusetts	LOCATION
Avenue	LOCATION
.	O
The	O
Charles	LOCATION
River	LOCATION
forms	O
the	O
southern	O
border	O
of	O
Cambridge	LOCATION
and	O
is	O
crossed	O
by	O
eleven	O
bridges	O
connecting	O
Cambridge	LOCATION
to	O
Boston	LOCATION
,	O
including	O
the	O
Longfellow	LOCATION
Bridge	LOCATION
and	O
the	O
Harvard	LOCATION
Bridge	LOCATION
,	O
eight	O
of	O
which	O
are	O
open	O
to	O
motorized	O
road	O
traffic	O
.	O
Cambridge	LOCATION
has	O
an	O
irregular	O
street	O
network	O
because	O
many	O
of	O
the	O
roads	O
date	O
from	O
the	O
colonial	O
era	O
.	O
Cambridge	LOCATION
is	O
well	O
served	O
by	O
the	O
MBTA	ORGANIZATION
,	O
including	O
the	O
Porter	LOCATION
Square	LOCATION
stop	LOCATION
on	O
the	O
regional	O
Commuter	ORGANIZATION
Rail	ORGANIZATION
,	O
the	O
Lechmere	LOCATION
stop	LOCATION
on	O
the	O
Green	LOCATION
Line	LOCATION
,	O
and	O
five	O
stops	O
on	O
the	O
Red	LOCATION
Line	LOCATION
(	O
Alewife	LOCATION
,	O
Porter	LOCATION
Square	LOCATION
,	O
Harvard	LOCATION
Square	LOCATION
,	O
Central	LOCATION
Square	LOCATION
,	O
and	O
Kendall	LOCATION
Square/MIT	LOCATION
)	O
.	O
Alewife	LOCATION
Station	LOCATION
,	O
the	O
current	O
terminus	O
of	O
the	O
Red	LOCATION
Line	LOCATION
,	O
has	O
a	O
large	O
multi-story	O
parking	O
garage	O
(	O
at	O
a	O
rate	O
of	O
$	O
7	O
per	O
day	O
as	O
of	O
2009	O
)	O
.	O
The	O
Harvard	LOCATION
Bus	LOCATION
Tunnel	LOCATION
,	O
under	O
Harvard	LOCATION
Square	LOCATION
,	O
reduces	O
traffic	O
congestion	O
on	O
the	O
surface	O
,	O
and	O
connects	O
to	O
the	O
Red	LOCATION
Line	LOCATION
underground	O
.	O
The	O
tunnel	O
was	O
partially	O
reconfigured	O
when	O
the	O
Red	LOCATION
Line	LOCATION
was	O
extended	O
to	O
Alewife	LOCATION
in	O
the	O
early	O
1980s	O
.	O
Cambridge	LOCATION
has	O
several	O
bike	O
paths	O
,	O
including	O
one	O
along	O
the	O
Charles	LOCATION
River	LOCATION
,	O
and	O
the	O
Linear	LOCATION
Park	LOCATION
connecting	O
the	O
Minuteman	LOCATION
Bikeway	LOCATION
at	O
Alewife	LOCATION
with	O
the	O
Somerville	LOCATION
Community	LOCATION
Path	LOCATION
.	O
On	O
several	O
central	O
MIT	ORGANIZATION
streets	O
,	O
bike	O
lanes	O
transfer	O
onto	O
the	O
sidewalk	O
.	O
Cambridge	LOCATION
bans	O
cycling	O
on	O
certain	O
sections	O
of	O
sidewalk	O
where	O
pedestrian	O
traffic	O
is	O
heavy	O
.	O
Cambridge	LOCATION
has	O
an	O
active	O
,	O
official	O
bicycle	O
committee	O
.	O
Walking	O
is	O
a	O
popular	O
activity	O
in	O
Cambridge	LOCATION
.	O
Per	O
year	O
2000	O
data	O
,	O
of	O
the	O
communities	O
in	O
the	O
U.S.	LOCATION
with	O
more	O
than	O
100,000	O
residents	O
,	O
Cambridge	LOCATION
has	O
the	O
highest	O
percentage	O
of	O
commuters	O
who	O
walk	O
to	O
work	O
.	O
Cambridge	LOCATION
's	O
major	O
historic	O
squares	O
have	O
been	O
recently	O
changed	O
into	O
a	O
modern	O
walking	O
landscape	O
,	O
which	O
has	O
sparked	O
a	O
traffic	O
calming	O
program	O
based	O
on	O
the	O
needs	O
of	O
pedestrians	O
rather	O
than	O
of	O
motorists	O
.	O
The	O
MBTA	ORGANIZATION
also	O
has	O
numerous	O
subway	O
stations	O
in	O
Cambridge	LOCATION
and	O
nearby	O
cities	O
and	O
towns	O
that	O
are	O
shared	O
with	O
the	O
regional	O
commuter	O
rail	O
lines	O
it	O
operates	O
.	O
Cambridge	LOCATION
has	O
a	O
large	O
and	O
varied	O
collection	O
of	O
permanent	O
public	O
art	O
,	O
both	O
on	O
city	O
property	O
,	O
and	O
on	O
the	O
campuses	O
of	O
Harvard	LOCATION
and	O
MIT	ORGANIZATION
.	O
An	O
active	O
tradition	O
of	O
street	O
musicians	O
and	O
other	O
performers	O
in	O
Harvard	LOCATION
Square	LOCATION
entertains	O
an	O
audience	O
of	O
tourists	O
and	O
local	O
residents	O
during	O
the	O
warmer	O
months	O
of	O
the	O
year	O
.	O
Despite	O
intensive	O
urbanization	O
during	O
the	O
late	O
19th	O
and	O
20th	O
centuries	O
,	O
Cambridge	LOCATION
has	O
preserved	O
an	O
unusual	O
number	O
of	O
historic	O
buildings	O
,	O
including	O
some	O
dating	O
to	O
the	O
17th	O
century	O
.	O
The	O
city	O
also	O
contains	O
an	O
abundance	O
of	O
innovative	O
contemporary	O
architecture	O
,	O
largely	O
built	O
by	O
Harvard	LOCATION
and	O
MIT	ORGANIZATION
.	O
Cambridge	LOCATION
has	O
nine	O
sister	O
cities	O
,	O
as	O
designated	O
by	O
Sister	ORGANIZATION
Cities	ORGANIZATION
International	ORGANIZATION
:	O
