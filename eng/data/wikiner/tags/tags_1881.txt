Beryl	O
of	O
various	O
colors	O
is	O
found	O
most	O
commonly	O
in	O
granitic	O
pegmatites	O
,	O
but	O
also	O
occurs	O
in	O
mica	O
schists	O
in	O
the	O
Ural	LOCATION
Mountains	LOCATION
,	O
and	O
limestone	O
in	O
Colombia	LOCATION
.	O
Beryl	O
is	O
found	O
in	O
Europe	LOCATION
in	O
Norway	LOCATION
,	O
Austria	LOCATION
,	O
Germany	LOCATION
,	O
Sweden	LOCATION
(	O
especially	O
morganite	O
)	O
,	O
and	O
Ireland	LOCATION
,	O
as	O
well	O
as	O
Brazil	LOCATION
,	O
Colombia	LOCATION
,	O
Madagascar	LOCATION
,	O
Russia	LOCATION
,	O
South	LOCATION
Africa	LOCATION
,	O
the	O
United	LOCATION
States	LOCATION
,	O
and	O
Zambia	LOCATION
.	O
U.S.	LOCATION
beryl	O
locations	O
are	O
in	O
California	LOCATION
,	O
Colo.	LOCATION
,	O
Connecticut	LOCATION
,	O
Idaho	LOCATION
,	O
Maine	LOCATION
,	O
N.H.	LOCATION
,	O
N.C.	LOCATION
,	O
South	LOCATION
Dakota	LOCATION
and	O
Utah	LOCATION
.	O
As	O
of	O
1999	O
,	O
the	O
largest	O
known	O
crystal	O
of	O
any	O
mineral	O
in	O
the	O
world	O
is	O
a	O
crystal	O
of	O
beryl	O
from	O
Madagascar	LOCATION
,	O
18	O
meters	O
long	O
and	O
3.5	O
meters	O
in	O
diameter	O
.	O
Aquamarine	O
(	O
from	O
Lat	MISC
.	O
It	O
occurs	O
at	O
most	O
localities	O
which	O
yield	O
ordinary	O
beryl	O
,	O
some	O
of	O
the	O
finest	O
coming	O
from	O
Russia	LOCATION
.	O
The	O
gem-gravel	O
placer	O
deposits	O
of	O
Sri	LOCATION
Lanka	LOCATION
contain	O
aquamarine	O
.	O
Clear	LOCATION
yellow	O
beryl	O
,	O
such	O
as	O
occurs	O
in	O
Brazil	LOCATION
,	O
is	O
sometimes	O
called	O
aquamarine	O
chrysolite	O
.	O
In	O
the	O
U.S.	LOCATION
,	O
aquamarines	O
can	O
be	O
found	O
at	O
the	O
summit	O
of	O
Mt.	LOCATION
Antero	LOCATION
in	O
the	O
Sawatch	LOCATION
Range	LOCATION
in	O
central	O
Colo.	LOCATION
.	O
In	O
Brazil	LOCATION
,	O
there	O
are	O
mines	O
in	O
the	O
states	O
of	O
Minas	LOCATION
Gerais	LOCATION
,	O
Espírito	LOCATION
Santo	LOCATION
,	O
and	O
Bahia	LOCATION
,	O
and	O
minorly	O
in	O
Rio	LOCATION
Grande	LOCATION
do	LOCATION
Norte	LOCATION
.	O
Emeralds	O
in	O
antiquity	O
were	O
mined	O
by	O
the	O
Egyptians	MISC
and	O
in	O
Austria	LOCATION
,	O
as	O
well	O
as	O
Swat	LOCATION
in	O
northern	O
Pakistan	LOCATION
.	O
A	O
rare	O
type	O
of	O
emerald	O
known	O
as	O
a	O
trapiche	O
emerald	O
is	O
occasionally	O
found	O
in	O
the	O
mines	O
of	O
Colombia	LOCATION
.	O
Fine	O
emeralds	O
are	O
also	O
found	O
in	O
other	O
countries	O
,	O
such	O
as	O
Zambia	LOCATION
,	O
Brazil	LOCATION
,	O
Zimbabwe	LOCATION
,	O
Madagascar	LOCATION
,	O
Pakistan	LOCATION
,	O
India	LOCATION
,	O
Afghanistan	LOCATION
and	O
Russia	LOCATION
.	O
In	O
the	O
US	LOCATION
,	O
emeralds	O
can	O
be	O
found	O
in	O
Hiddenite	LOCATION
,	O
N.C.	LOCATION
.	O
In	O
1998	O
,	O
emeralds	O
were	O
discovered	O
in	O
the	O
Yukon	LOCATION
.	O
Pink	O
beryl	O
of	O
fine	O
color	O
and	O
good	O
sizes	O
was	O
first	O
discovered	O
on	O
an	O
island	O
on	O
the	O
coast	O
of	O
Madagascar	LOCATION
in	O
1910	O
.	O
It	O
was	O
also	O
known	O
,	O
with	O
other	O
gemstone	O
minerals	O
,	O
such	O
as	O
tourmaline	O
and	O
kunzite	O
,	O
at	O
Pala	LOCATION
,	O
California	LOCATION
.	O
The	O
old	O
synonym	O
bixbite	O
is	O
deprecated	O
from	O
the	O
CIBJO	ORGANIZATION
,	O
because	O
of	O
the	O
risk	O
of	O
confusion	O
with	O
the	O
mineral	O
bixbyite	O
(	O
also	O
named	O
after	O
the	O
mineralogist	O
Maynard	PERSON
Bixby	PERSON
)	O
.	O
