Water-soluble	O
artists	O
'	O
acrylic	O
paints	O
became	O
commercially	O
available	O
in	O
the	O
1950s	O
,	O
offered	O
by	O
Liquitex	ORGANIZATION
,	O
with	O
high-viscosity	O
paints	O
similar	O
to	O
those	O
made	O
today	O
becoming	O
available	O
in	O
the	O
early	O
1960s	O
.	O
Acrylic	O
paints	O
with	O
gloss	O
or	O
matte	O
finishes	O
are	O
available	O
,	O
although	O
a	O
satin	O
(	O
semi-matte	O
)	O
sheen	O
is	O
most	O
common	O
;	O
some	O
brands	O
exhibit	O
a	O
range	O
of	O
finish	O
(	O
e.g.	O
heavy-body	O
paints	O
from	O
Golden	ORGANIZATION
,	O
Liquitex	ORGANIZATION
and	O
Winsor	ORGANIZATION
&	ORGANIZATION
Newton	ORGANIZATION
)	O
.	O
