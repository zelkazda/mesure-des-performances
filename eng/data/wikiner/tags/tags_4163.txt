Emperor	O
Temmu	PERSON
was	O
the	O
40th	O
emperor	O
of	O
Japan	LOCATION
,	O
according	O
to	O
the	O
traditional	O
order	O
of	O
succession	O
.	O
Temmu	PERSON
's	O
reign	O
lasted	O
from	O
672	O
until	O
his	O
death	O
in	O
686	O
.	O
Temmu	PERSON
was	O
the	O
youngest	O
son	O
of	O
Emperor	O
Jomei	PERSON
and	O
Empress	O
Saimei	PERSON
,	O
and	O
the	O
younger	O
brother	O
of	O
the	O
Emperor	O
Tenji	PERSON
.	O
He	O
was	O
succeeded	O
by	O
Empress	O
Jitō	PERSON
,	O
who	O
was	O
both	O
his	O
niece	O
and	O
his	O
wife	O
.	O
Temmu	PERSON
also	O
had	O
other	O
consorts	O
whose	O
fathers	O
were	O
influential	O
courtiers	O
.	O
Through	O
Prince	O
Kusakabe	PERSON
,	O
Temmu	PERSON
had	O
two	O
emperors	O
and	O
two	O
empresses	O
among	O
his	O
descendents	O
.	O
Empress	O
Shōtoku	PERSON
was	O
the	O
last	O
of	O
these	O
imperial	O
rulers	O
from	O
his	O
lineage	O
.	O
Emperor	O
Temmu	PERSON
is	O
the	O
first	O
monarch	O
of	O
Japan	LOCATION
,	O
to	O
whom	O
the	O
title	O
tenno	O
was	O
assigned	O
contemporaneously	O
--	O
not	O
only	O
by	O
later	O
generations	O
.	O
However	O
,	O
it	O
was	O
edited	O
by	O
his	O
son	O
,	O
Prince	O
Toneri	PERSON
,	O
and	O
the	O
work	O
was	O
written	O
during	O
the	O
reigns	O
of	O
his	O
wife	O
and	O
children	O
,	O
causing	O
one	O
to	O
suspect	O
its	O
accuracy	O
and	O
impartiality	O
.	O
Temmu	PERSON
's	O
father	O
died	O
while	O
he	O
was	O
young	O
,	O
and	O
he	O
grew	O
up	O
mainly	O
under	O
the	O
guidance	O
of	O
Empress	O
Saimei	PERSON
.	O
Tenji	O
was	O
suspicious	O
that	O
Temmu	PERSON
might	O
be	O
so	O
ambitious	O
as	O
to	O
attempt	O
to	O
take	O
the	O
throne	O
,	O
and	O
felt	O
the	O
necessity	O
to	O
strengthen	O
his	O
position	O
through	O
politically	O
advantageous	O
marriages	O
.	O
In	O
671	O
Temmu	PERSON
felt	O
himself	O
to	O
be	O
in	O
danger	O
and	O
volunteered	O
to	O
resign	O
the	O
office	O
of	O
crown	O
prince	O
to	O
become	O
a	O
monk	O
.	O
He	O
moved	O
to	O
the	O
mountains	O
in	O
Yoshino	LOCATION
,	O
Yamato	LOCATION
province	LOCATION
,	O
officially	O
for	O
reasons	O
of	O
seclusion	O
.	O
The	O
army	O
of	O
Temmu	PERSON
and	O
the	O
army	O
of	O
the	O
young	O
Emperor	O
Kōbun	PERSON
fought	O
in	O
the	O
northwestern	O
part	O
of	O
Mino	LOCATION
.	O
The	O
actual	O
site	O
of	O
Temmu	PERSON
's	O
grave	O
is	O
known	O
.	O
Each	O
clan	O
received	O
a	O
new	O
kabane	O
according	O
to	O
its	O
closeness	O
to	O
the	O
imperial	O
bloodline	O
and	O
its	O
loyalty	O
to	O
Temmu	PERSON
.	O
Temmu	PERSON
attempted	O
to	O
keep	O
a	O
balance	O
of	O
power	O
among	O
his	O
sons	O
.	O
Once	O
he	O
traveled	O
to	O
Yoshino	LOCATION
together	O
with	O
his	O
sons	O
,	O
and	O
there	O
had	O
them	O
swear	O
to	O
cooperate	O
and	O
not	O
to	O
make	O
war	O
on	O
each	O
other	O
.	O
This	O
turned	O
out	O
to	O
be	O
ineffective	O
:	O
one	O
of	O
his	O
sons	O
,	O
Prince	PERSON
Ōtsu	PERSON
,	O
was	O
later	O
executed	O
for	O
treason	O
after	O
the	O
death	O
of	O
Temmu	PERSON
.	O
Temmu	PERSON
used	O
religious	O
structures	O
to	O
increase	O
the	O
authority	O
of	O
the	O
imperial	O
throne	O
.	O
During	O
his	O
reign	O
there	O
was	O
increased	O
emphasis	O
on	O
the	O
tie	O
between	O
the	O
imperial	O
household	O
and	O
the	O
Grand	LOCATION
Shrine	LOCATION
of	LOCATION
Ise	LOCATION
(	O
dedicated	O
to	O
the	O
ancestor	O
goddess	O
of	O
the	O
emperors	O
,	O
Amaterasu	PERSON
)	O
by	O
sending	O
his	O
daughter	O
Princess	PERSON
Oku	PERSON
as	O
the	O
newly	O
established	O
Saiō	PERSON
of	O
the	O
shrine	O
,	O
and	O
several	O
festivals	O
were	O
financed	O
from	O
the	O
national	O
budget	O
.	O
Court	O
lady	O
:	O
Nukata	PERSON
no	PERSON
Ōkimi	PERSON
(	O
額田王	O
)	O
