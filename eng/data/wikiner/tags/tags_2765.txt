William	PERSON
"	PERSON
Captain	PERSON
"	PERSON
Kidd	PERSON
(	O
c.	O
1645	O
--	O
May	O
23	O
,	O
1701	O
)	O
was	O
a	O
Scottish	MISC
sailor	O
remembered	O
for	O
his	O
trial	O
and	O
execution	O
for	O
piracy	O
after	O
returning	O
from	O
a	O
voyage	O
to	O
the	O
Indian	LOCATION
Ocean	LOCATION
.	O
Some	O
modern	O
historians	O
deem	O
his	O
piratical	O
reputation	O
unjust	O
,	O
as	O
there	O
is	O
evidence	O
that	O
Kidd	PERSON
acted	O
only	O
as	O
a	O
privateer	O
.	O
Kidd	PERSON
's	O
fame	O
springs	O
largely	O
from	O
the	O
sensational	O
circumstances	O
of	O
his	O
questioning	O
before	O
the	O
English	PERSON
Parliament	PERSON
and	O
the	O
ensuing	O
trial	O
.	O
Captain	O
William	PERSON
Kidd	PERSON
was	O
either	O
one	O
of	O
the	O
most	O
notorious	O
pirates	O
in	O
history	O
,	O
or	O
one	O
of	O
its	O
most	O
unjustly	O
vilified	O
and	O
prosecuted	O
privateers	O
in	O
an	O
age	O
typified	O
by	O
the	O
rationalization	O
of	O
empire	O
.	O
Kidd	PERSON
was	O
born	O
in	O
Greenock	LOCATION
,	O
Scotland	LOCATION
around	O
1645	O
.	O
There	O
they	O
renamed	O
the	O
ship	O
the	O
Blessed	O
William	PERSON
.	O
During	O
the	O
War	MISC
of	MISC
the	MISC
Grand	MISC
Alliance	MISC
,	O
on	O
orders	O
from	O
the	O
province	O
of	O
New	LOCATION
York	LOCATION
,	O
Massachusetts	LOCATION
,	O
Kidd	PERSON
captured	O
an	O
enemy	O
privateer	O
,	O
which	O
duty	O
he	O
was	O
commissioned	O
to	O
perform	O
off	O
of	O
the	O
New	LOCATION
England	LOCATION
coast	O
.	O
Shortly	O
thereafter	O
,	O
Kidd	PERSON
was	O
awarded	O
£	O
150	O
for	O
successful	O
privateering	O
in	O
the	O
Caribbean	MISC
.	O
One	O
year	O
later	O
,	O
"	PERSON
Captain	PERSON
"	PERSON
Culliford	PERSON
,	O
a	O
notorious	O
pirate	O
,	O
had	O
stolen	O
Kidd	PERSON
's	O
ship	O
while	O
he	O
was	O
ashore	O
at	O
Antigua	LOCATION
in	O
the	O
West	LOCATION
Indies	LOCATION
.	O
This	O
request	O
preceded	O
the	O
voyage	O
which	O
established	O
Kidd	PERSON
's	O
reputation	O
as	O
a	O
pirate	O
,	O
and	O
marked	O
his	O
image	O
in	O
history	O
and	O
folklore	O
.	O
Kidd	PERSON
and	O
an	O
acquaintance	O
,	O
Colonel	O
Robert	PERSON
Livingston	PERSON
,	O
orchestrated	O
the	O
whole	O
plan	O
and	O
paid	O
for	O
the	O
rest	O
.	O
Kidd	PERSON
had	O
to	O
sell	O
his	O
ship	O
Antigua	LOCATION
to	O
raise	O
funds	O
.	O
The	O
new	O
ship	O
,	O
the	O
Adventure	MISC
Galley	MISC
,	O
was	O
well	O
suited	O
to	O
the	O
task	O
of	O
catching	O
pirates	O
;	O
weighing	O
over	O
284	O
tons	O
,	O
she	O
was	O
equipped	O
with	O
34	O
cannons	O
,	O
oars	O
,	O
and	O
150	O
men	O
.	O
The	O
oars	O
were	O
a	O
key	O
advantage	O
as	O
they	O
would	O
enable	O
the	O
Adventure	MISC
Galley	MISC
to	O
maneuver	O
in	O
a	O
battle	O
when	O
the	O
winds	O
had	O
calmed	O
and	O
other	O
ships	O
were	O
dead	O
in	O
the	O
water	O
.	O
Kidd	PERSON
took	O
pride	O
in	O
personally	O
selecting	O
the	O
crew	O
,	O
choosing	O
only	O
those	O
he	O
deemed	O
to	O
be	O
the	O
best	O
and	O
most	O
loyal	O
officers	O
.	O
To	O
make	O
up	O
for	O
the	O
lack	O
of	O
officers	O
,	O
Kidd	PERSON
picked	O
up	O
replacement	O
crew	O
in	O
New	LOCATION
York	LOCATION
,	O
the	O
vast	O
majority	O
of	O
whom	O
were	O
known	O
and	O
hardened	O
criminals	O
,	O
some	O
undoubtedly	O
former	O
pirates	O
.	O
In	O
September	O
1696	O
,	O
Kidd	PERSON
weighed	O
anchor	O
and	O
set	O
course	O
for	O
the	O
Cape	LOCATION
of	LOCATION
Good	LOCATION
Hope	LOCATION
.	O
A	O
third	O
of	O
his	O
crew	O
soon	O
perished	O
on	O
the	O
Comoros	LOCATION
due	O
to	O
an	O
outbreak	O
of	O
cholera	O
,	O
the	O
brand-new	O
ship	O
developed	O
many	O
leaks	O
,	O
and	O
he	O
failed	O
to	O
find	O
the	O
pirates	O
he	O
expected	O
to	O
encounter	O
off	O
Madagascar	LOCATION
.	O
According	O
to	O
Edward	PERSON
Barlow	PERSON
,	O
a	O
captain	O
employed	O
by	O
the	O
British	ORGANIZATION
East	ORGANIZATION
India	ORGANIZATION
Company	ORGANIZATION
,	O
Kidd	PERSON
attacked	O
a	O
Mughal	MISC
convoy	O
here	O
under	O
escort	O
by	O
Barlow	PERSON
's	O
East	MISC
Indiaman	MISC
,	O
and	O
was	O
beaten	O
off	O
.	O
If	O
the	O
report	O
is	O
true	O
,	O
this	O
marked	O
Kidd	PERSON
's	O
first	O
foray	O
into	O
piracy	O
.	O
Some	O
of	O
the	O
crew	O
deserted	O
Kidd	PERSON
the	O
next	O
time	O
the	O
Adventure	MISC
Galley	MISC
anchored	O
offshore	O
,	O
and	O
those	O
who	O
decided	O
to	O
stay	O
behind	O
made	O
constant	O
open-threats	O
of	O
mutiny	O
.	O
Kidd	PERSON
killed	O
one	O
of	O
his	O
own	O
crewmen	O
on	O
October	O
30	O
,	O
1697	O
.	O
But	O
Kidd	PERSON
seemed	O
unconcerned	O
,	O
later	O
explaining	O
to	O
his	O
surgeon	O
that	O
he	O
had	O
"	O
good	O
friends	O
in	O
England	LOCATION
,	O
that	O
will	O
bring	O
me	O
off	O
for	O
that	O
.	O
"	O
Acts	O
of	O
savagery	O
on	O
Kidd	PERSON
's	O
part	O
were	O
reported	O
by	O
escaped	O
prisoners	O
,	O
who	O
told	O
stories	O
of	O
being	O
hoisted	O
up	O
by	O
the	O
arms	O
and	O
drubbed	O
with	O
a	O
nude	O
cutlass	O
.	O
When	O
Kidd	PERSON
found	O
out	O
what	O
had	O
happened	O
,	O
he	O
was	O
outraged	O
and	O
forced	O
his	O
men	O
to	O
return	O
most	O
of	O
the	O
stolen	O
property	O
.	O
Kidd	PERSON
was	O
declared	O
a	O
pirate	O
very	O
early	O
in	O
his	O
voyage	O
by	O
a	O
Royal	ORGANIZATION
Navy	ORGANIZATION
officer	O
to	O
whom	O
he	O
had	O
promised	O
"	O
thirty	O
men	O
or	O
so	O
"	O
.	O
Kidd	PERSON
sailed	O
away	O
during	O
the	O
night	O
to	O
preserve	O
his	O
crew	O
,	O
rather	O
than	O
subject	O
them	O
to	O
Royal	ORGANIZATION
Navy	ORGANIZATION
impressment	O
.	O
In	O
an	O
attempt	O
to	O
maintain	O
his	O
tenuous	O
control	O
over	O
his	O
crew	O
,	O
Kidd	PERSON
relented	O
and	O
kept	O
the	O
prize	O
.	O
When	O
this	O
news	O
reached	O
England	LOCATION
,	O
it	O
confirmed	O
Kidd	PERSON
's	O
reputation	O
as	O
a	O
pirate	O
,	O
and	O
various	O
naval	O
commanders	O
were	O
ordered	O
to	O
"	O
pursue	O
and	O
seize	O
the	O
said	O
Kidd	PERSON
and	O
his	O
accomplices	O
"	O
for	O
the	O
"	O
notorious	O
piracies	O
"	O
they	O
had	O
committed	O
.	O
On	O
April	O
1	O
,	O
1698	O
,	O
Kidd	PERSON
reached	O
Madagascar	LOCATION
.	O
Two	O
contradictory	O
accounts	O
exist	O
of	O
how	O
Kidd	PERSON
reacted	O
to	O
his	O
encounter	O
with	O
Culliford	PERSON
.	O
However	O
,	O
his	O
crew	O
,	O
despite	O
their	O
previous	O
eagerness	O
to	O
seize	O
any	O
available	O
prize	O
,	O
refused	O
to	O
attack	O
Culliford	PERSON
and	O
threatened	O
instead	O
to	O
shoot	O
Kidd	PERSON
.	O
Both	O
accounts	O
agree	O
that	O
most	O
of	O
Kidd	PERSON
's	O
men	O
now	O
abandoned	O
him	O
for	O
Culliford	PERSON
.	O
He	O
is	O
alleged	O
to	O
have	O
deposited	O
some	O
of	O
his	O
treasure	O
on	O
Gardiners	LOCATION
Island	LOCATION
,	O
hoping	O
to	O
use	O
his	O
knowledge	O
of	O
its	O
location	O
as	O
a	O
bargaining	O
tool	O
.	O
He	O
lured	O
Kidd	PERSON
into	O
Boston	LOCATION
with	O
false	O
promises	O
of	O
clemency	O
,	O
then	O
ordered	O
him	O
arrested	O
on	O
July	O
6	O
,	O
1699	O
.	O
The	O
conditions	O
of	O
Kidd	PERSON
's	O
imprisonment	O
were	O
extremely	O
harsh	O
,	O
and	O
appear	O
to	O
have	O
driven	O
him	O
at	O
least	O
temporarily	O
insane	O
.	O
Whilst	O
awaiting	O
trial	O
,	O
Kidd	PERSON
was	O
confined	O
in	O
the	O
infamous	O
Newgate	LOCATION
Prison	LOCATION
and	O
wrote	O
several	O
letters	O
to	O
King	O
William	PERSON
requesting	O
clemency	O
.	O
He	O
was	O
hung	O
on	O
May	O
23	O
,	O
1701	O
,	O
at	O
'	O
Execution	LOCATION
Dock	LOCATION
'	O
,	O
Wapping	LOCATION
,	O
in	O
London	LOCATION
.	O
During	O
the	O
execution	O
,	O
the	O
hangman	O
's	O
rope	O
broke	O
and	O
Kidd	PERSON
was	O
hanged	O
on	O
the	O
second	O
attempt	O
.	O
Kidd	PERSON
's	O
Whig	ORGANIZATION
backers	O
were	O
embarrassed	O
by	O
his	O
trial	O
.	O
These	O
passes	O
(	O
and	O
others	O
dated	O
1700	O
)	O
resurfaced	O
in	O
the	O
early	O
twentieth	O
century	O
,	O
misfiled	O
with	O
other	O
government	O
papers	O
in	O
a	O
London	LOCATION
building	O
.	O
These	O
passes	O
call	O
the	O
extent	O
of	O
Kidd	PERSON
's	O
guilt	O
into	O
question	O
.	O
The	O
belief	O
that	O
Kidd	PERSON
had	O
left	O
a	O
buried	O
treasure	O
contributed	O
considerably	O
to	O
the	O
growth	O
of	O
his	O
legend	O
.	O
It	O
is	O
the	O
most	O
southern	O
island	O
,	O
named	O
Takarajima	LOCATION
,	O
which	O
translates	O
literally	O
as	O
"	O
Treasure	MISC
Island	MISC
"	O
.	O
Afterwards	O
,	O
Kidd	PERSON
hid	O
his	O
treasure	O
in	O
one	O
of	O
the	O
caves	O
,	O
never	O
coming	O
back	O
for	O
it	O
due	O
to	O
his	O
execution	O
in	O
England	LOCATION
.	O
It	O
has	O
been	O
proven	O
to	O
be	O
the	O
Quedagh	PERSON
Merchant	PERSON
.	O
For	O
years	O
,	O
people	O
and	O
treasure	O
hunters	O
have	O
tried	O
to	O
locate	O
the	O
Quedagh	PERSON
Merchant	PERSON
.	O
It	O
was	O
reported	O
on	O
December	O
13	O
,	O
2007	O
,	O
that	O
"	O
wreckage	O
of	O
a	O
pirate	O
ship	O
abandoned	O
by	O
Captain	O
Kidd	PERSON
in	O
the	O
17th	O
century	O
has	O
been	O
found	O
by	O
divers	O
in	O
shallow	O
waters	O
off	O
the	O
Dominican	LOCATION
Republic	LOCATION
.	O
"	O
The	O
ship	O
is	O
believed	O
to	O
be	O
"	O
the	O
remains	O
of	O
Quedagh	PERSON
Merchant	PERSON
"	O
.	O
