The	O
AIM-7	MISC
Sparrow	MISC
is	O
an	O
American-made	MISC
,	O
medium-range	O
semi-active	O
radar	O
homing	O
air-to-air	O
missile	O
operated	O
by	O
the	O
United	ORGANIZATION
States	ORGANIZATION
Air	ORGANIZATION
Force	ORGANIZATION
,	O
United	ORGANIZATION
States	ORGANIZATION
Navy	ORGANIZATION
and	O
United	ORGANIZATION
States	ORGANIZATION
Marine	ORGANIZATION
Corps	ORGANIZATION
,	O
as	O
well	O
as	O
various	O
allied	O
air	O
forces	O
and	O
navies	O
.	O
It	O
remains	O
in	O
service	O
,	O
although	O
it	O
is	O
being	O
phased	O
out	O
in	O
aviation	O
applications	O
in	O
favor	O
of	O
the	O
more	O
advanced	O
AIM-120	MISC
AMRAAM	MISC
.	O
The	O
Sparrow	MISC
was	O
used	O
as	O
the	O
basis	O
for	O
a	O
surface-to-air	O
missile	O
,	O
the	O
RIM-7	MISC
Sea	MISC
Sparrow	MISC
,	O
which	O
is	O
used	O
by	O
the	O
United	ORGANIZATION
States	ORGANIZATION
Navy	ORGANIZATION
for	O
air	O
defense	O
of	O
its	O
ships	O
.	O
The	O
Sparrow	MISC
emerged	O
from	O
a	O
late-1940s	O
United	ORGANIZATION
States	ORGANIZATION
Navy	ORGANIZATION
program	O
to	O
develop	O
a	O
guided	O
rocket	O
weapon	O
for	O
air-to-air	O
use	O
.	O
The	O
airframe	O
was	O
developed	O
by	O
Douglas	ORGANIZATION
Aircraft	ORGANIZATION
Company	ORGANIZATION
.	O
The	O
diameter	O
of	O
the	O
HVAR	MISC
proved	O
to	O
be	O
inadequate	O
for	O
the	O
electronics	O
,	O
leading	O
Douglas	ORGANIZATION
to	O
expand	O
the	O
missile	O
's	O
airframe	O
to	O
8-inch	O
(	O
203	O
mm	O
)	O
diameter	O
.	O
By	O
1955	O
Douglas	ORGANIZATION
proposed	O
going	O
ahead	O
with	O
development	O
,	O
intending	O
it	O
to	O
be	O
the	O
primary	O
weapon	O
for	O
the	O
F5D	MISC
Skylancer	MISC
interceptor	O
.	O
Canadair	ORGANIZATION
continued	O
development	O
until	O
the	O
Arrow	MISC
was	O
cancelled	O
in	O
1958	O
.	O
The	O
first	O
of	O
these	O
weapons	O
entered	O
United	ORGANIZATION
States	ORGANIZATION
Navy	ORGANIZATION
service	O
in	O
1958	O
.	O
The	O
Sparrows	MISC
became	O
the	O
AIM-7	MISC
series	O
.	O
Improved	O
versions	O
of	O
the	O
AIM-7	MISC
were	O
developed	O
in	O
the	O
1970s	O
in	O
an	O
attempt	O
to	O
address	O
the	O
weapon	O
's	O
limitations	O
.	O
The	O
AIM-7F	MISC
,	O
which	O
entered	O
service	O
in	O
1976	O
,	O
had	O
a	O
dual-stage	O
rocket	O
motor	O
for	O
longer	O
range	O
,	O
solid-state	O
electronics	O
for	O
greatly	O
improved	O
reliability	O
,	O
and	O
a	O
larger	O
warhead	O
.	O
It	O
was	O
used	O
to	O
good	O
advantage	O
in	O
the	O
1991	O
Gulf	MISC
War	MISC
,	O
where	O
it	O
scored	O
many	O
USAF	ORGANIZATION
air-to-air	O
kills	O
;	O
however	O
its	O
kill	O
probability	O
,	O
overall	O
,	O
is	O
still	O
less	O
than	O
40	O
%	O
.	O
Sparrow	MISC
is	O
now	O
being	O
phased	O
out	O
with	O
the	O
availability	O
of	O
the	O
active-radar	O
AIM-120	MISC
AMRAAM	MISC
,	O
but	O
is	O
likely	O
to	O
remain	O
in	O
service	O
for	O
a	O
number	O
of	O
years	O
.	O
After	O
Douglas	ORGANIZATION
dropped	O
out	O
of	O
this	O
program	O
,	O
Canadair	ORGANIZATION
continued	O
on	O
with	O
it	O
until	O
the	O
termination	O
of	O
the	O
Arrow	MISC
.	O
Skyflash	MISC
entered	O
service	O
with	O
the	O
Royal	ORGANIZATION
Air	ORGANIZATION
Force	ORGANIZATION
(	O
RAF	ORGANIZATION
)	O
on	O
their	O
Phantom	MISC
FG.1/FGR.2	MISC
in	O
1976	O
,	O
and	O
later	O
on	O
the	O
Tornado	MISC
F3	MISC
.	O
The	O
Sparrow	MISC
has	O
four	O
major	O
sections	O
:	O
guidance	O
section	O
,	O
warhead	O
,	O
control	O
,	O
and	O
rocket	O
motor	O
.	O
Although	O
the	O
external	O
dimensions	O
of	O
the	O
Sparrow	MISC
remained	O
relatively	O
unchanged	O
from	O
model	O
to	O
model	O
,	O
the	O
internal	O
components	O
of	O
newer	O
missiles	O
represent	O
major	O
improvements	O
,	O
with	O
vastly	O
increased	O
capabilities	O
.	O
