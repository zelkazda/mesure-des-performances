The	O
earliest	O
evidence	O
of	O
written	O
mathematics	O
dates	O
back	O
to	O
the	O
ancient	O
Sumerians	MISC
,	O
who	O
built	O
the	O
earliest	O
civilization	O
in	O
Mesopotamia	LOCATION
.	O
From	O
around	O
2500	O
BC	O
onwards	O
,	O
the	O
Sumerians	MISC
wrote	O
multiplication	O
tables	O
on	O
clay	O
tablets	O
and	O
dealt	O
with	O
geometrical	O
exercises	O
and	O
division	O
problems	O
.	O
The	O
earliest	O
traces	O
of	O
the	O
Babylonian	MISC
numerals	O
also	O
date	O
back	O
to	O
this	O
period	O
.	O
Babylonian	MISC
advances	O
in	O
mathematics	O
were	O
facilitated	O
by	O
the	O
fact	O
that	O
60	O
has	O
many	O
divisors	O
.	O
Egyptian	MISC
mathematics	O
refers	O
to	O
mathematics	O
written	O
in	O
the	O
Egyptian	MISC
language	MISC
.	O
According	O
to	O
legend	O
,	O
Pythagoras	PERSON
traveled	O
to	O
Egypt	LOCATION
to	O
learn	O
mathematics	O
,	O
geometry	O
,	O
and	O
astronomy	O
from	O
Egyptian	MISC
priests	O
.	O
Thales	PERSON
used	O
geometry	O
to	O
solve	O
problems	O
such	O
as	O
calculating	O
the	O
height	O
of	O
pyramids	O
and	O
the	O
distance	O
of	O
ships	O
from	O
the	O
shore	O
.	O
Euclid	PERSON
(	O
c.	O
300	O
BC	O
)	O
is	O
the	O
earliest	O
example	O
of	O
the	O
format	O
still	O
used	O
in	O
mathematics	O
today	O
,	O
definition	O
,	O
axiom	O
,	O
theorem	O
,	O
and	O
proof	O
.	O
The	O
most	O
important	O
of	O
these	O
is	O
The	MISC
Nine	MISC
Chapters	MISC
on	MISC
the	MISC
Mathematical	MISC
Art	MISC
,	O
the	O
full	O
title	O
of	O
which	O
appeared	O
by	O
AD	O
179	O
,	O
but	O
existed	O
in	O
part	O
under	O
other	O
titles	O
beforehand	O
.	O
[	O
citation	O
needed	O
]	O
Liu	PERSON
Hui	PERSON
commented	O
on	O
the	O
work	O
by	O
the	O
3rd	O
century	O
AD	O
,	O
and	O
gave	O
a	O
value	O
of	O
π	O
accurate	O
to	O
5	O
decimal	O
places	O
.	O
Though	O
more	O
of	O
a	O
matter	O
of	O
computational	O
stamina	O
than	O
theoretical	O
insight	O
,	O
in	O
the	O
5th	O
century	O
AD	O
Zu	PERSON
Chongzhi	PERSON
computed	O
the	O
value	O
of	O
π	O
to	O
seven	O
decimal	O
places	O
,	O
which	O
remained	O
the	O
most	O
accurate	O
value	O
of	O
π	O
for	O
almost	O
the	O
next	O
1000	O
years	O
.	O
The	O
Surya	MISC
Siddhanta	MISC
(	O
c.	O
400	O
)	O
introduced	O
the	O
trigonometric	O
functions	O
of	O
sine	O
,	O
cosine	O
,	O
and	O
inverse	O
sine	O
,	O
and	O
laid	O
down	O
rules	O
to	O
determine	O
the	O
true	O
motions	O
of	O
the	O
luminaries	O
,	O
which	O
conforms	O
to	O
their	O
actual	O
positions	O
in	O
the	O
sky	O
.	O
In	O
the	O
5th	O
century	O
AD	O
,	O
Aryabhata	PERSON
wrote	O
the	O
Aryabhatiya	MISC
,	O
a	O
slim	O
volume	O
,	O
written	O
in	O
verse	O
,	O
intended	O
to	O
supplement	O
the	O
rules	O
of	O
calculation	O
used	O
in	O
astronomy	O
and	O
mathematical	O
mensuration	O
,	O
though	O
with	O
no	O
feeling	O
for	O
logic	O
or	O
deductive	O
methodology	O
.	O
Though	O
about	O
half	O
of	O
the	O
entries	O
are	O
wrong	O
,	O
it	O
is	O
in	O
the	O
Aryabhatiya	MISC
that	O
the	O
decimal	O
place-value	O
system	O
first	O
appears	O
.	O
In	O
the	O
12th	O
century	O
,	O
Bhāskara	PERSON
II	PERSON
first	O
conceived	O
differential	O
calculus	O
,	O
along	O
with	O
the	O
concepts	O
of	O
the	O
derivative	O
,	O
differential	O
coefficient	O
,	O
and	O
differentiation	O
.	O
Mathematical	O
progress	O
in	O
India	LOCATION
stagnated	O
from	O
the	O
late	O
16th	O
century	O
to	O
the	O
20th	O
century	O
,	O
due	O
to	O
political	O
turmoil	O
.	O
Al-Khwarizmi	PERSON
is	O
often	O
called	O
the	O
"	O
father	O
of	O
algebra	O
"	O
,	O
for	O
his	O
fundamental	O
contributions	O
to	O
the	O
field	O
.	O
This	O
is	O
the	O
operation	O
which	O
Al-Khwarizmi	PERSON
originally	O
described	O
as	O
al-jabr	O
.	O
Further	O
developments	O
in	O
algebra	O
were	O
made	O
by	O
Al-Karaji	PERSON
in	O
his	O
treatise	O
al-Fakhri	PERSON
,	O
where	O
he	O
extends	O
the	O
methodology	O
to	O
incorporate	O
integer	O
powers	O
and	O
integer	O
roots	O
of	O
unknown	O
quantities	O
.	O
Also	O
in	O
the	O
10th	O
century	O
,	O
Abul	PERSON
Wafa	PERSON
translated	O
the	O
works	O
of	O
Diophantus	PERSON
into	O
Arabic	MISC
and	O
developed	O
the	O
tangent	O
function	O
.	O
Ibn	PERSON
al-Haytham	PERSON
was	O
the	O
first	O
mathematician	O
to	O
derive	O
the	O
formula	O
for	O
the	O
sum	O
of	O
the	O
fourth	O
powers	O
,	O
using	O
a	O
method	O
that	O
is	O
readily	O
generalizable	O
for	O
determining	O
the	O
general	O
formula	O
for	O
the	O
sum	O
of	O
any	O
integral	O
powers	O
.	O
In	O
the	O
late	O
12th	O
century	O
,	O
Sharaf	PERSON
al-Dīn	PERSON
al-Tūsī	PERSON
introduced	O
the	O
concept	O
of	O
a	O
function	O
,	O
and	O
he	O
was	O
the	O
first	O
to	O
discover	O
the	O
derivative	O
of	O
cubic	O
polynomials	O
.	O
In	O
the	O
13th	O
century	O
,	O
Nasir	PERSON
al-Din	PERSON
Tusi	PERSON
(	O
Nasireddin	PERSON
)	O
made	O
advances	O
in	O
spherical	O
trigonometry	O
.	O
He	O
also	O
wrote	O
influential	O
work	O
on	O
Euclid	PERSON
's	O
parallel	O
postulate	O
.	O
In	O
the	O
15th	O
century	O
,	O
Ghiyath	PERSON
al-Kashi	PERSON
computed	O
the	O
value	O
of	O
π	O
to	O
the	O
16th	O
decimal	O
place	O
.	O
Boethius	PERSON
provided	O
a	O
place	O
for	O
mathematics	O
in	O
the	O
curriculum	O
when	O
he	O
coined	O
the	O
term	O
quadrivium	O
to	O
describe	O
the	O
study	O
of	O
arithmetic	O
,	O
geometry	O
,	O
astronomy	O
,	O
and	O
music	O
.	O
Thomas	PERSON
Bradwardine	PERSON
proposed	O
that	O
speed	O
(	O
V	O
)	O
increases	O
in	O
arithmetic	O
proportion	O
as	O
the	O
ratio	O
of	O
force	O
to	O
resistance	O
increases	O
in	O
geometric	O
proportion	O
.	O
Bradwardine	PERSON
expressed	O
this	O
by	O
a	O
series	O
of	O
specific	O
examples	O
,	O
but	O
although	O
the	O
logarithm	O
had	O
not	O
yet	O
been	O
conceived	O
,	O
we	O
can	O
express	O
his	O
conclusion	O
anachronistically	O
by	O
writing	O
:	O
V	O
=	O
log	O
(	O
F/R	O
)	O
.	O
Heytesbury	PERSON
and	O
others	O
mathematically	O
determined	O
the	O
distance	O
covered	O
by	O
a	O
body	O
undergoing	O
uniformly	O
accelerated	O
motion	O
(	O
today	O
solved	O
by	O
integration	O
)	O
,	O
stating	O
that	O
"	O
a	O
moving	O
body	O
uniformly	O
acquiring	O
or	O
losing	O
that	O
increment	O
[	O
of	O
speed	O
]	O
will	O
traverse	O
in	O
some	O
given	O
time	O
a	O
[	O
distance	O
]	O
completely	O
equal	O
to	O
that	O
which	O
it	O
would	O
traverse	O
if	O
it	O
were	O
moving	O
continuously	O
through	O
the	O
same	O
time	O
with	O
the	O
mean	O
degree	O
[	O
of	O
speed	O
]	O
"	O
.	O
Nicole	PERSON
Oresme	PERSON
at	O
the	O
University	ORGANIZATION
of	ORGANIZATION
Paris	ORGANIZATION
and	O
the	O
Italian	MISC
Giovanni	PERSON
di	PERSON
Casali	PERSON
independently	O
provided	O
graphical	O
demonstrations	O
of	O
this	O
relationship	O
,	O
asserting	O
that	O
the	O
area	O
under	O
the	O
line	O
depicting	O
the	O
constant	O
acceleration	O
,	O
represented	O
the	O
total	O
distance	O
traveled	O
.	O
In	O
a	O
later	O
mathematical	O
commentary	O
on	O
Euclid	MISC
's	MISC
Elements	MISC
,	O
Oresme	PERSON
made	O
a	O
more	O
detailed	O
general	O
analysis	O
in	O
which	O
he	O
demonstrated	O
that	O
a	O
body	O
will	O
acquire	O
in	O
each	O
successive	O
increment	O
of	O
time	O
an	O
increment	O
of	O
any	O
quality	O
that	O
increases	O
as	O
the	O
odd	O
numbers	O
.	O
Since	O
Euclid	PERSON
had	O
demonstrated	O
the	O
sum	O
of	O
the	O
odd	O
numbers	O
are	O
the	O
square	O
numbers	O
,	O
the	O
total	O
quality	O
acquired	O
by	O
the	O
body	O
increases	O
as	O
the	O
square	O
of	O
the	O
time	O
.	O
It	O
is	O
important	O
to	O
note	O
that	O
Pacioli	PERSON
himself	O
had	O
borrowed	O
much	O
of	O
the	O
work	O
of	O
Piero	PERSON
Della	PERSON
Francesca	PERSON
whom	O
he	O
plagiarized	O
.	O
While	O
there	O
is	O
no	O
direct	O
relationship	O
between	O
algebra	O
and	O
accounting	O
,	O
the	O
teaching	O
of	O
the	O
subjects	O
and	O
the	O
books	O
published	O
often	O
intended	O
for	O
the	O
children	O
of	O
merchants	O
who	O
were	O
sent	O
to	O
reckoning	O
schools	O
(	O
in	O
Flanders	LOCATION
and	O
Germany	LOCATION
)	O
or	O
abacus	O
schools	O
(	O
known	O
as	O
abbaco	O
in	O
Italy	LOCATION
)	O
,	O
where	O
they	O
learned	O
the	O
skills	O
useful	O
for	O
trade	O
and	O
commerce	O
.	O
In	O
part	O
because	O
he	O
wanted	O
to	O
help	O
Kepler	PERSON
in	O
his	O
calculations	O
,	O
John	PERSON
Napier	PERSON
,	O
in	O
Scotland	LOCATION
,	O
was	O
the	O
first	O
to	O
investigate	O
natural	O
logarithms	O
.	O
Kepler	PERSON
succeeded	O
in	O
formulating	O
mathematical	O
laws	O
of	O
planetary	O
motion	O
.	O
Simon	PERSON
Stevin	PERSON
(	O
1585	O
)	O
created	O
the	O
basis	O
for	O
modern	O
decimal	O
notation	O
capable	O
of	O
describing	O
all	O
numbers	O
,	O
whether	O
rational	O
or	O
irrational	O
.	O
In	O
addition	O
to	O
the	O
application	O
of	O
mathematics	O
to	O
the	O
studies	O
of	O
the	O
heavens	O
,	O
applied	O
mathematics	O
began	O
to	O
expand	O
into	O
new	O
areas	O
,	O
with	O
the	O
correspondence	O
of	O
Pierre	PERSON
de	PERSON
Fermat	PERSON
and	O
Blaise	PERSON
Pascal	PERSON
.	O
Pascal	PERSON
and	O
Fermat	PERSON
set	O
the	O
groundwork	O
for	O
the	O
investigations	O
of	O
probability	O
theory	O
and	O
the	O
corresponding	O
rules	O
of	O
combinatorics	O
in	O
their	O
discussions	O
over	O
a	O
game	O
of	O
gambling	O
.	O
Pascal	PERSON
,	O
with	O
his	O
wager	O
,	O
attempted	O
to	O
use	O
the	O
newly	O
developing	O
probability	O
theory	O
to	O
argue	O
for	O
a	O
life	O
devoted	O
to	O
religion	O
,	O
on	O
the	O
grounds	O
that	O
even	O
if	O
the	O
probability	O
of	O
success	O
was	O
small	O
,	O
the	O
rewards	O
were	O
infinite	O
.	O
The	O
most	O
influential	O
mathematician	O
of	O
the	O
1700s	O
was	O
arguably	O
Leonhard	PERSON
Euler	PERSON
.	O
His	O
contributions	O
range	O
from	O
founding	O
the	O
study	O
of	O
graph	O
theory	O
with	O
the	O
Seven	LOCATION
Bridges	LOCATION
of	LOCATION
Königsberg	LOCATION
problem	O
to	O
standardizing	O
many	O
modern	O
mathematical	O
terms	O
and	O
notations	O
.	O
In	O
the	O
19th	O
century	O
lived	O
Carl	PERSON
Friedrich	PERSON
Gauss	PERSON
(	O
1777	O
--	O
1855	O
)	O
.	O
Augustin-Louis	PERSON
Cauchy	PERSON
,	O
Bernhard	PERSON
Riemann	PERSON
,	O
and	O
Karl	PERSON
Weierstrass	PERSON
reformulated	O
the	O
calculus	O
in	O
a	O
more	O
rigorous	O
fashion	O
.	O
Abel	PERSON
and	O
Galois	PERSON
's	O
investigations	O
into	O
the	O
solutions	O
of	O
various	O
polynomial	O
equations	O
laid	O
the	O
groundwork	O
for	O
further	O
developments	O
of	O
group	O
theory	O
,	O
and	O
the	O
associated	O
fields	O
of	O
abstract	O
algebra	O
.	O
In	O
the	O
later	O
19th	O
century	O
,	O
Georg	PERSON
Cantor	PERSON
established	O
the	O
first	O
foundations	O
of	O
set	O
theory	O
,	O
which	O
enabled	O
the	O
rigorous	O
treatment	O
of	O
the	O
notion	O
of	O
infinity	O
and	O
has	O
become	O
the	O
common	O
language	O
of	O
nearly	O
all	O
mathematics	O
.	O
Cantor	PERSON
's	O
set	O
theory	O
,	O
and	O
the	O
rise	O
of	O
mathematical	O
logic	O
in	O
the	O
hands	O
of	O
Peano	PERSON
,	O
L.	PERSON
E.	PERSON
J.	PERSON
Brouwer	PERSON
,	O
David	PERSON
Hilbert	PERSON
,	O
Bertrand	PERSON
Russell	PERSON
,	O
and	O
A.N.	PERSON
Whitehead	PERSON
,	O
initiated	O
a	O
long	O
running	O
debate	O
on	O
the	O
foundations	O
of	O
mathematics	O
.	O
The	O
first	O
international	O
,	O
special-interest	O
society	O
,	O
the	O
Quaternion	MISC
Society	MISC
,	O
was	O
formed	O
in	O
1899	O
,	O
in	O
the	O
context	O
of	O
a	O
vector	O
controversy	O
.	O
In	O
1976	O
,	O
Wolfgang	PERSON
Haken	PERSON
and	O
Kenneth	PERSON
Appel	PERSON
used	O
a	O
computer	O
to	O
prove	O
the	O
four	O
color	O
theorem	O
.	O
Paul	PERSON
Cohen	PERSON
and	O
Kurt	PERSON
Gödel	PERSON
proved	O
that	O
the	O
continuum	O
hypothesis	O
is	O
independent	O
of	O
(	O
could	O
neither	O
be	O
proved	O
nor	O
disproved	O
from	O
)	O
the	O
standard	O
axioms	O
of	O
set	O
theory	O
.	O
Differential	O
geometry	O
came	O
into	O
its	O
own	O
when	O
Einstein	PERSON
used	O
it	O
in	O
general	O
relativity	O
.	O
Entire	O
new	O
areas	O
of	O
mathematics	O
such	O
as	O
mathematical	O
logic	O
,	O
topology	O
,	O
and	O
John	PERSON
von	PERSON
Neumann	PERSON
's	O
game	O
theory	O
changed	O
the	O
kinds	O
of	O
questions	O
that	O
could	O
be	O
answered	O
by	O
mathematical	O
methods	O
.	O
Grothendieck	PERSON
and	O
Serre	PERSON
recast	O
algebraic	O
geometry	O
using	O
sheaf	O
theory	O
.	O
The	O
development	O
and	O
continual	O
improvement	O
of	O
computers	O
,	O
at	O
first	O
mechanical	O
analog	O
machines	O
and	O
then	O
digital	O
electronic	O
machines	O
,	O
allowed	O
industry	O
to	O
deal	O
with	O
larger	O
and	O
larger	O
amounts	O
of	O
data	O
to	O
facilitate	O
mass	O
production	O
and	O
distribution	O
and	O
communication	O
,	O
and	O
new	O
areas	O
of	O
mathematics	O
were	O
developed	O
to	O
deal	O
with	O
this	O
:	O
Alan	PERSON
Turing	PERSON
's	O
computability	O
theory	O
;	O
complexity	O
theory	O
;	O
Claude	PERSON
Shannon	PERSON
's	O
information	O
theory	O
;	O
signal	O
processing	O
;	O
data	O
analysis	O
;	O
optimization	O
and	O
other	O
areas	O
of	O
operations	O
research	O
.	O
Hence	O
mathematics	O
can	O
not	O
be	O
reduced	O
to	O
mathematical	O
logic	O
,	O
and	O
David	PERSON
Hilbert	PERSON
's	O
dream	O
of	O
making	O
all	O
of	O
mathematics	O
complete	O
and	O
consistent	O
needed	O
to	O
be	O
reformulated	O
.	O
Paul	PERSON
Erdős	PERSON
published	O
more	O
papers	O
than	O
any	O
other	O
mathematician	O
in	O
history	O
,	O
working	O
with	O
hundreds	O
of	O
collaborators	O
.	O
This	O
describes	O
the	O
"	O
collaborative	O
distance	O
"	O
between	O
a	O
person	O
and	O
Paul	PERSON
Erdős	PERSON
,	O
as	O
measured	O
by	O
joint	O
authorship	O
of	O
mathematical	O
papers	O
.	O
There	O
is	O
an	O
increasing	O
drive	O
towards	O
open	O
access	O
publishing	O
,	O
first	O
popularized	O
by	O
the	O
arXiv	MISC
.	O
