Thus	O
Alexios	PERSON
Angelos	PERSON
was	O
a	O
member	O
of	O
the	O
extended	O
imperial	O
family	O
.	O
His	O
younger	O
brother	O
Isaac	PERSON
was	O
threatened	O
with	O
execution	O
under	O
orders	O
of	O
their	O
first	O
cousin	O
once	O
removed	O
Andronikos	PERSON
I	PERSON
Komnenos	PERSON
on	O
September	O
11	O
,	O
1185	O
.	O
Isaac	O
made	O
a	O
desperate	O
attack	O
on	O
the	O
imperial	O
agents	O
and	O
killed	O
their	O
leader	O
Stephen	PERSON
Hagiochristophorites	PERSON
.	O
He	O
then	O
took	O
refuge	O
in	O
the	O
church	O
of	O
Hagia	LOCATION
Sophia	LOCATION
and	O
from	O
there	O
appealed	O
to	O
the	O
populace	O
.	O
Alexios	PERSON
was	O
now	O
closer	O
to	O
the	O
imperial	O
throne	O
than	O
ever	O
before	O
.	O
By	O
1190	O
Alexios	PERSON
Angelos	PERSON
had	O
returned	O
to	O
the	O
court	O
of	O
his	O
younger	O
brother	O
,	O
from	O
whom	O
he	O
received	O
the	O
elevated	O
title	O
of	O
sebastokratōr	O
.	O
In	O
1195	O
,	O
while	O
Isaac	PERSON
II	PERSON
was	O
away	O
hunting	O
in	O
Thrace	LOCATION
,	O
Alexios	PERSON
was	O
acclaimed	O
as	O
emperor	O
by	O
the	O
troops	O
with	O
the	O
conniving	O
of	O
Alexios	PERSON
'	O
wife	O
Euphrosyne	PERSON
Doukaina	PERSON
Kamatera	PERSON
.	O
The	O
emperor	O
's	O
attempts	O
to	O
bolster	O
the	O
empire	O
's	O
defenses	O
by	O
special	O
concessions	O
to	O
Byzantine	MISC
and	O
Bulgarian	MISC
notables	O
in	O
the	O
frontier	O
zone	O
backfired	O
,	O
as	O
the	O
latter	O
built	O
up	O
regional	O
autonomy	O
.	O
Byzantine	MISC
authority	O
survived	O
,	O
but	O
in	O
a	O
much	O
weakened	O
state	O
.	O
Soon	O
Alexios	PERSON
was	O
threatened	O
by	O
a	O
new	O
and	O
yet	O
more	O
formidable	O
danger	O
.	O
Alexios	PERSON
IV	PERSON
Angelos	PERSON
,	O
the	O
son	O
of	O
the	O
deposed	O
Isaac	PERSON
II	PERSON
,	O
had	O
recently	O
escaped	O
from	O
Constantinople	LOCATION
and	O
now	O
appealed	O
to	O
the	O
crusaders	O
,	O
promising	O
to	O
end	O
the	O
schism	O
of	O
East	MISC
and	O
West	PERSON
,	O
to	O
pay	O
for	O
their	O
transport	O
,	O
and	O
to	O
provide	O
military	O
support	O
to	O
the	O
crusaders	O
if	O
they	O
helped	O
him	O
to	O
depose	O
his	O
uncle	O
and	O
sit	O
on	O
his	O
father	O
's	O
throne	O
.	O
The	O
crusaders	O
,	O
whose	O
objective	O
had	O
been	O
Egypt	LOCATION
,	O
were	O
persuaded	O
to	O
set	O
their	O
course	O
for	O
Constantinople	LOCATION
before	O
which	O
they	O
appeared	O
in	O
June	O
1203	O
,	O
proclaiming	O
Alexios	PERSON
as	O
emperor	O
and	O
inviting	O
the	O
populace	O
of	O
the	O
capital	O
to	O
depose	O
his	O
uncle	O
.	O
Alexios	PERSON
III	PERSON
took	O
no	O
efficient	O
measures	O
to	O
resist	O
,	O
and	O
his	O
attempts	O
to	O
bribe	O
the	O
crusaders	O
failed	O
.	O
In	O
July	O
,	O
the	O
crusaders	O
,	O
led	O
by	O
the	O
aged	O
Doge	O
Enrico	PERSON
Dandolo	PERSON
,	O
scaled	O
the	O
walls	O
and	O
took	O
control	O
of	O
a	O
major	O
section	O
.	O
But	O
his	O
courage	O
failed	O
,	O
and	O
the	O
Byzantine	MISC
army	O
returned	O
to	O
the	O
city	O
without	O
a	O
fight	O
.	O
His	O
courtiers	O
demanded	O
action	O
,	O
and	O
Alexios	PERSON
promised	O
to	O
fight	O
.	O
Instead	O
,	O
that	O
night	O
(	O
July	O
17/18	O
)	O
,	O
Alexios	PERSON
III	PERSON
hid	O
in	O
the	O
palace	O
,	O
and	O
finally	O
,	O
with	O
one	O
of	O
his	O
daughters	O
,	O
Eirene	PERSON
,	O
and	O
such	O
treasures	O
(	O
1,000	O
pounds	O
of	O
gold	O
)	O
as	O
he	O
could	O
collect	O
,	O
got	O
into	O
a	O
boat	O
and	O
escaped	O
to	O
Debeltos	LOCATION
in	O
Thrace	LOCATION
,	O
leaving	O
his	O
wife	O
and	O
his	O
other	O
daughters	O
behind	O
.	O
Isaac	PERSON
II	PERSON
,	O
drawn	O
from	O
his	O
prison	O
and	O
robed	O
once	O
more	O
in	O
the	O
imperial	O
purple	O
,	O
received	O
his	O
son	O
in	O
state	O
.	O
Alexios	PERSON
attempted	O
to	O
organize	O
a	O
resistance	O
to	O
the	O
new	O
regime	O
from	O
Adrianople	LOCATION
and	O
then	O
Mosynopolis	LOCATION
,	O
where	O
he	O
was	O
joined	O
by	O
the	O
later	O
usurper	O
Alexios	PERSON
V	PERSON
Doukas	PERSON
Mourtzouphlos	PERSON
in	O
April	O
1204	O
,	O
after	O
the	O
definitive	O
fall	O
of	O
Constantinople	LOCATION
to	O
the	O
crusaders	O
and	O
the	O
establishment	O
of	O
the	O
Latin	MISC
Empire	MISC
.	O
At	O
first	O
Alexios	PERSON
III	PERSON
received	O
Alexios	PERSON
V	PERSON
well	O
,	O
even	O
allowing	O
him	O
to	O
marry	O
his	O
daughter	O
Eudokia	PERSON
Angelina	PERSON
.	O
Later	O
Alexios	PERSON
V	PERSON
was	O
blinded	O
and	O
deserted	O
by	O
his	O
father-in-law	O
,	O
who	O
fled	O
from	O
the	O
crusaders	O
into	O
Thessaly	LOCATION
.	O
Trying	O
to	O
escape	O
Boniface	PERSON
's	O
"	O
protection	O
"	O
,	O
Alexios	PERSON
III	PERSON
attempted	O
to	O
seek	O
shelter	O
with	O
Michael	PERSON
I	PERSON
Komnenos	PERSON
Doukas	PERSON
,	O
the	O
ruler	O
of	O
Epirus	MISC
,	O
in	O
1205	O
.	O
Here	O
Alexios	PERSON
III	PERSON
conspired	O
against	O
his	O
son-in-law	O
after	O
the	O
latter	O
refused	O
to	O
recognize	O
Alexios	PERSON
'	O
authority	O
,	O
and	O
received	O
the	O
support	O
of	O
Kay	PERSON
Khusrau	PERSON
I	PERSON
,	O
the	O
sultan	O
of	O
Rûm	LOCATION
.	O
In	O
the	O
battle	O
of	O
Antioch	MISC
on	O
the	O
Maeander	LOCATION
in	O
1211	O
,	O
the	O
sultan	O
was	O
defeated	O
and	O
killed	O
,	O
and	O
Alexios	PERSON
III	PERSON
was	O
captured	O
by	O
Theodore	PERSON
Laskaris	PERSON
.	O
Alexius	PERSON
III	PERSON
was	O
then	O
confined	O
to	O
a	O
monastery	O
at	O
Nicaea	LOCATION
,	O
where	O
he	O
died	O
later	O
in	O
1211	O
.	O
By	O
his	O
marriage	O
to	O
Euphrosyne	PERSON
Doukaina	PERSON
Kamatera	PERSON
Alexios	PERSON
had	O
three	O
daughters	O
:	O
