Bananas	O
,	O
formerly	O
the	O
country	O
's	O
second-largest	O
export	O
until	O
being	O
virtually	O
wiped	O
out	O
by	O
1998	O
's	O
Hurricane	MISC
Mitch	MISC
,	O
recovered	O
in	O
2000	O
to	O
57	O
%	O
of	O
pre-Mitch	MISC
levels	O
.	O
Honduras	O
is	O
the	O
third	O
poorest	O
country	O
in	O
the	O
Western	LOCATION
Hemisphere	LOCATION
;	O
only	O
Haiti	LOCATION
and	O
Nicaragua	LOCATION
are	O
poorer	O
.	O
It	O
not	O
been	O
swift	O
to	O
implementing	O
structural	O
changes	O
such	O
as	O
privatization	O
of	O
the	O
publicly	O
owned	O
telephone	O
and	O
energy	O
distribution	O
companies	O
--	O
changes	O
which	O
are	O
desired	O
by	O
the	O
IMF	ORGANIZATION
and	O
other	O
international	O
lenders	O
.	O
The	O
government	O
's	O
daunting	O
task	O
then	O
became	O
how	O
to	O
create	O
an	O
economic	O
base	O
able	O
to	O
compensate	O
for	O
the	O
withdrawal	O
of	O
much	O
United	LOCATION
States	LOCATION
assistance	O
without	O
becoming	O
solely	O
dependent	O
on	O
traditional	O
agricultural	O
exports	O
.	O
Funds	O
from	O
the	O
multilateral	O
lending	O
institutions	O
,	O
which	O
eventually	O
would	O
help	O
fill	O
the	O
gap	O
left	O
by	O
the	O
reduction	O
of	O
U.S.	LOCATION
aid	O
,	O
were	O
still	O
under	O
negotiation	O
in	O
1989	O
and	O
would	O
be	O
conditioned	O
first	O
on	O
payment	O
of	O
arrears	O
on	O
the	O
country	O
's	O
enormous	O
external	O
debt	O
.	O
External	O
financing	O
--	O
mostly	O
bilateral	O
credit	O
from	O
the	O
United	LOCATION
States	LOCATION
--	O
rose	O
dramatically	O
until	O
it	O
reached	O
87	O
percent	O
of	O
the	O
public	O
deficit	O
in	O
1985	O
,	O
rising	O
even	O
further	O
in	O
subsequent	O
years	O
.	O
The	O
fund	O
created	O
public	O
works	O
programs	O
such	O
as	O
road	O
maintenance	O
and	O
provided	O
U.S.	LOCATION
surplus	O
food	O
to	O
mothers	O
and	O
infants	O
.	O
The	O
industry	O
was	O
dependent	O
,	O
however	O
,	O
on	O
larvae	O
imported	O
from	O
the	O
United	LOCATION
States	LOCATION
to	O
augment	O
its	O
unstable	O
natural	O
supply	O
.	O
