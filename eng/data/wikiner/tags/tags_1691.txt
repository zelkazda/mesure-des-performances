It	O
has	O
even	O
been	O
argued	O
that	O
a	O
design	O
by	O
Galileo	PERSON
Galilei	PERSON
(	O
during	O
the	O
17th	O
century	O
)	O
,	O
was	O
that	O
of	O
a	O
ballpoint	O
pen	O
[	O
citation	O
needed	O
]	O
.	O
László	PERSON
Bíró	PERSON
,	O
a	O
Hungarian	MISC
newspaper	O
editor	O
,	O
was	O
frustrated	O
by	O
the	O
amount	O
of	O
time	O
that	O
he	O
wasted	O
in	O
filling	O
up	O
fountain	O
pens	O
and	O
cleaning	O
up	O
smudged	O
pages	O
,	O
and	O
the	O
sharp	O
tip	O
of	O
his	O
fountain	O
pen	O
often	O
tore	O
his	O
pages	O
of	O
newsprint	O
.	O
Bíró	PERSON
had	O
noticed	O
that	O
the	O
type	O
of	O
ink	O
used	O
in	O
newspaper	O
printing	O
dried	O
quickly	O
,	O
leaving	O
the	O
paper	O
dry	O
and	O
smudge	O
free	O
.	O
Bíró	PERSON
fitted	O
this	O
pen	O
with	O
a	O
tiny	O
ball	O
in	O
its	O
tip	O
that	O
was	O
free	O
to	O
turn	O
in	O
a	O
socket	O
.	O
This	O
pen	O
was	O
widely	O
known	O
as	O
the	O
rocket	O
in	O
the	O
U.S.	LOCATION
into	O
the	O
late	O
1950s	O
.	O
The	O
International	ORGANIZATION
Organization	ORGANIZATION
for	ORGANIZATION
Standardization	ORGANIZATION
has	O
published	O
standards	O
for	O
ball	O
point	O
and	O
roller	O
ball	O
pens	O
:	O
ISO	ORGANIZATION
14145-1938	O
is	O
when	O
the	O
pen	O
was	O
invented	O
