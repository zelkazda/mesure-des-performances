He	O
is	O
best	O
known	O
for	O
his	O
novel	O
Moby-Dick	MISC
and	O
the	O
posthumous	O
novella	O
Billy	MISC
Budd	MISC
.	O
He	O
was	O
the	O
first	O
writer	O
to	O
have	O
his	O
works	O
collected	O
and	O
published	O
by	O
the	O
Library	MISC
of	MISC
America	MISC
.	O
Herman	PERSON
visited	O
him	O
in	O
Boston	LOCATION
,	O
and	O
his	O
father	O
turned	O
to	O
him	O
in	O
his	O
frequent	O
times	O
of	O
financial	O
need	O
.	O
The	O
new	O
venture	O
,	O
however	O
,	O
was	O
unsuccessful	O
;	O
the	O
War	MISC
of	MISC
1812	MISC
had	O
ruined	O
businesses	O
that	O
tried	O
to	O
sell	O
overseas	O
and	O
he	O
was	O
forced	O
to	O
declare	O
bankruptcy	O
.	O
He	O
died	O
soon	O
afterward	O
,	O
leaving	O
his	O
family	O
penniless	O
,	O
when	O
Herman	PERSON
was	O
12	O
.	O
Herman	PERSON
's	O
younger	O
brother	O
,	O
Thomas	PERSON
Melville	PERSON
,	O
eventually	O
became	O
a	O
governor	O
of	O
Sailors	LOCATION
Snug	LOCATION
Harbor	LOCATION
.	O
Redburn	MISC
:	MISC
His	MISC
First	MISC
Voyage	MISC
(	O
1849	O
)	O
is	O
partly	O
based	O
on	O
his	O
experiences	O
of	O
this	O
journey	O
.	O
The	O
three	O
years	O
after	O
Albany	ORGANIZATION
Academy	ORGANIZATION
(	O
1837	O
to	O
1840	O
)	O
were	O
mostly	O
occupied	O
with	O
teaching	O
school	O
,	O
except	O
for	O
the	O
voyage	O
to	O
Liverpool	LOCATION
in	O
1839	O
.	O
After	O
working	O
as	O
a	O
clerk	O
for	O
four	O
months	O
,	O
he	O
joined	O
the	O
crew	O
of	O
the	O
frigate	O
USS	MISC
United	MISC
States	MISC
,	O
which	O
reached	O
Boston	LOCATION
in	O
October	O
1844	O
.	O
These	O
experiences	O
were	O
described	O
in	O
Typee	MISC
,	O
Omoo	MISC
,	O
and	O
White-Jacket	MISC
,	O
which	O
were	O
published	O
as	O
novels	O
mainly	O
because	O
few	O
believed	O
their	O
veracity	O
.	O
The	O
Boston	LOCATION
publisher	O
subsequently	O
accepted	O
Omoo	MISC
sight	O
unseen	O
.	O
Redburn	MISC
and	O
White-Jacket	MISC
had	O
no	O
problem	O
finding	O
publishers	O
.	O
Mardi	MISC
was	O
a	O
disappointment	O
for	O
readers	O
who	O
wanted	O
another	O
rollicking	O
and	O
exotic	O
sea	O
yarn	O
.	O
While	O
living	O
at	O
Arrowhead	LOCATION
,	O
he	O
befriended	O
the	O
author	O
,	O
Nathaniel	PERSON
Hawthorne	PERSON
,	O
who	O
lived	O
in	O
nearby	O
Lenox	LOCATION
.	O
(	O
The	O
customs	O
house	O
was	O
coincidentally	O
on	O
Gansevoort	PERSON
St.	O
,	O
named	O
after	O
his	O
mother	O
's	O
prosperous	O
family	O
.	O
)	O
His	O
uncle	O
,	O
Peter	PERSON
Gansevoort	PERSON
,	O
by	O
a	O
bequest	O
,	O
paid	O
for	O
the	O
publication	O
of	O
the	O
massive	O
epic	O
in	O
1876	O
.	O
His	O
novella	O
Billy	MISC
Budd	MISC
,	MISC
Sailor	MISC
,	O
unpublished	O
at	O
the	O
time	O
of	O
his	O
death	O
,	O
was	O
published	O
in	O
1924	O
.	O
Later	O
it	O
was	O
turned	O
into	O
an	O
opera	O
by	O
Benjamin	PERSON
Britten	PERSON
,	O
a	O
play	O
,	O
and	O
a	O
film	O
by	O
Peter	PERSON
Ustinov	PERSON
.	O
The	O
critic	O
Lewis	PERSON
Mumford	PERSON
found	O
a	O
copy	O
of	O
the	O
poem	O
in	O
the	O
New	LOCATION
York	LOCATION
Public	LOCATION
Library	LOCATION
in	O
1925	O
"	O
with	O
its	O
pages	O
uncut	O
"	O
--	O
in	O
other	O
words	O
,	O
it	O
had	O
sat	O
there	O
unread	O
for	O
50	O
years	O
.	O
In	O
1951	O
,	O
Newton	PERSON
Arvin	PERSON
published	O
the	O
critical	O
biography	O
Herman	PERSON
Melville	PERSON
,	O
which	O
won	O
the	O
nonfiction	O
National	MISC
Book	MISC
Award	MISC
.	O
In	O
recent	O
years	O
,	O
Billy	MISC
Budd	MISC
has	O
become	O
a	O
central	O
text	O
in	O
the	O
field	O
of	O
legal	O
scholarship	O
known	O
as	O
law	O
and	O
literature	O
.	O
