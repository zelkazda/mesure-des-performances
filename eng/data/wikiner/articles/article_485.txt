His mother , Queen Maria Christina , was appointed regent during his minority .
Alfonso 's reign began well .
During the First World War , because of his family connections with both sides and the division of popular opinion , Spain remained neutral .
However , he became gravely ill during the 1918 flu pandemic and , since Spain was neutral and thus under no wartime censorship restrictions , his illness and subsequent recovery were covered worldwide , giving the false impression ( in the absence of real news from anywhere else ) that Spain was the most-affected area .
When the Second Spanish Republic was proclaimed on 14 April 1931 , he fled and left Spain , but did not abdicate the throne .
First , he went into exile in France .
He died in Rome a month-and-a-half later .
