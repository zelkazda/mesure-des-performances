Neisser 's point of view endows the discipline with a scope beyond high-level concepts such as " reasoning " that other works often espouse as defining cognitive psychology .
Neisser 's definition of " cognition " illustrates this well :
Cognitive psychology is one of the more recent additions to psychological research , having only developed as a separate area within the discipline since the late 1950s and early 1960s following the " cognitive revolution " initiated by Noam Chomsky 's 1959 critique of behaviorism and empiricism more generally .
The origins of cognitive thinking such as computational theory of mind can be traced back as early as Descartes in the 17th century , and proceeding up to Alan Turing in the 1940s and ' 50s .
Since that time , the dominant paradigm in the area has been the information processing model of cognition that Broadbent put forward .
