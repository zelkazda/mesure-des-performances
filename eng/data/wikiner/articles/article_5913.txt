He was commissioned as a second lieutenant into the Blues and Royals of the Household Cavalry Regiment -- serving temporarily with his brother -- and completed his training as a tank commander .
Prince Harry began to accompany his parents on official visits at an early age ; his first overseas royal tour was with his parents to Italy in 1985 .
Clarence House made public the Prince 's disappointment with the decision , though he said he would abide by it .
Every survivor declares : " I 'm Spartacus ! "
For his service , Prince Harry was decorated with the Operational Service Medal for Afghanistan by his aunt , the Princess Royal , at the Combermere Barracks in May 2008 .
In October 2008 , the news was revealed that Prince Harry was to follow his brother , father , and uncle with the wish to fly military helicopters .
The brothers ' new household released a statement -- complete with their own cyphers at the top -- announcing that they have established their own office at nearby St. James 's Palace to look after their public , military , and charitable activities .
Prince Harry has spent much of his free time in sporting activities , playing competitive polo , as well as skiing and motocross .
Prince Harry is a supporter of Arsenal Football Club .
It later emerged that Prince Harry had personally apologised to the soldier .
Through his maternal grandfather , Prince Harry is descended from King Henry IV , King Charles II and King James II and VII .
Prince Harry is descended from all kings and queens of England , Great Britain , and the United Kingdom with surviving offspring from William I onwards except for these five : King Henry V , King Henry VIII ( their lines are both extinct ) , King George IV and King William IV ( neither of whom had any surviving legitimate children ) and King Edward VIII .
