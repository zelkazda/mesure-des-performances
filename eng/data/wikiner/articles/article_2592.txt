The coyote ( pronounced /kaɪˈoʊtiː , ˈkaɪ.oʊt/ ) , also known as the American jackal or the prairie wolf , is a species of canine found throughout North and Central America , ranging from Panama in the south , north through Mexico , the United States and Canada .
It occurs as far north as Alaska and all but the northernmost portions of Canada .
There are currently 19 recognized subspecies , with 16 in Canada , Mexico and the United States , and 3 in Central America .
This trait , however , is absent in the large New England coyotes , which are thought to have some wolf ancestry .
For example , as New England became increasingly settled and the resident wolves were eliminated , the coyote population increased , filling the empty biological niche .
Coyotes will sometimes mate with domestic dogs , usually in areas like Texas and Okla. , where the coyotes are plentiful and the breeding season is extended because of the warm weather .
Breeding experiments in Germany with poodles , coyotes , and later on with the resulting dog-coyote hybrids showed that unlike wolfdogs , coydogs show a decrease in fertility , significant communication problems as well as an increase of genetic diseases after three generations of interbreeding .
A study showed that of 100 coyotes collected in Maine , 22 had half or more wolf ancestry , and one was 89 percent wolf .
A theory has been proposed that the large eastern coyotes in Canada are actually hybrids of the smaller western coyotes and wolves that met and mated decades ago as the coyotes moved toward New England from their earlier western ranges .
Genetic distance calculations have indicated that red wolves are intermediate between coyotes and gray wolves , and that they bear great similarity to wolf/coyote hybrids in southern Quebec and Minnesota .
Until the wolves returned , Yellowstone National Park had one of the densest and most stable coyote populations in America due to a lack of human impacts .
In Grand Teton , coyote densities were 33 % lower than normal in the areas where they coexisted with wolves , and 39 % lower in the areas of Yellowstone where wolves were reintroduced .
Yellowstone coyotes have had to shift their territories as a result , moving from open meadows to steep terrain .
In Calif. , coyote and bobcat populations are not negatively correlated across different habitat types , but predation by coyotes is an important source of mortality in bobcats .
It originally ranged primarily in the western half of North America , but it has adapted readily to the changes caused by human occupation and , since the early 19th century , has been steadily and dramatically extending its range .
Coyotes have moved into most of the areas of North America formerly occupied by wolves , and are often observed foraging in suburban garbage bins .
A study by wildlife ecologists at Ohio State University yielded some surprising findings in this regard .
Researchers studied coyote populations in Chicago over a seven-year period ( 2000 -- 2007 ) , proposing that coyotes have adapted well to living in densely populated urban environments while avoiding contact with humans .
The researchers estimate that there are up to 2,000 coyotes living in " the greater Chicago area " and that this circumstance may well apply to many other urban landscapes in North America .
However , coyote attacks on humans have increased since 1998 in the state of California .
In June , 2010 a 3-year-old girl and a 6-year-old girl were attacked and seriously injured in separate attacks by coyotes in Rye , New York , a suburb of New York City .
There are only two recorded fatalities in North America from coyote attacks .
In 1981 in Glendale , California , a coyote attacked toddler Kelly Keen , who was rescued by her father , but died in surgery due to blood loss and a broken neck .
In October 2009 , Taylor Mitchell , a 19-year-old folk singer on tour , died from injuries sustained in an attack by a pair of coyotes while hiking in the Skyline Trail of the Cape Breton Highlands National Park in Nova Scotia , Canada .
Recent studies have shown , however , that the large northeastern coyotes responsible for this attack may in fact be coyote-wolf hybrids ( or coywolves ) due to absorption of wolves when coyotes moved into eastern North America .
Coyotes are presently the most abundant livestock predators in western North America , causing the majority of sheep , goat and cattle losses .
For example , according to the National Agricultural Statistics Service , coyotes were responsible for 60.5 % of the 224,000 sheep deaths that were attributed to predation in 2004 .
During the 1983-86 seasons , N.D. buyers purchased an average of 7,913 pelts annually , for an average annual combined return to takers of $ 255,458 .
In 1986-87 , South Dakota buyers purchased 8,149 pelts for a total of $ 349,674 to takers .
The harvest of coyote pelts in Tex. has varied over the past few decades , but has generally followed a downward trend .
The Phoenix Coyotes are a National Hockey League franchise based in Arizona .
The mascot of the University of South Dakota is the coyote .
Oken 's idiosyncratic nomenclatorial ways , however , aroused the scorn of a number of zoological systematists .
A few authors , however , Ernest Thompson Seton being among them , accepted Oken 's nomenclature , and went as far as referring to the coyote as American jackal .
