It is bordered to the north by the North Sea , Denmark , and the Baltic Sea ; to the east by Poland and the Czech Republic ; to the south by Austria and Switzerland ; and to the west by France , Luxembourg , Belgium , and the Netherlands .
The territory of Germany covers 357021 square kilometers ( 137847 sq mi ) and is influenced by a temperate seasonal climate .
With 81.8 million inhabitants in January 2010 , it is the most populated country located entirely in Europe , has the largest population among member states of the European Union , and it is also home to the third-largest number of international migrants worldwide .
During the 16th century , northern Germany became the centre of the Protestant Reformation .
As a modern nation-state , the country was first unified amidst the Franco-Prussian War in 1871 .
Germany is a federal parliamentary republic of sixteen states .
The capital and largest city is Berlin .
Germany is a member of the United Nations , NATO , G7 , G8 , G20 , OECD , and the WTO .
In absolute terms , Germany allocates the second biggest annual budget of development aid in the world , while its military expenditure ranked seventh .
It holds a key position in European affairs and maintains a multitude of close partnerships on a global level .
Germany is recognised as a scientific and technological leader in several fields .
Its territory stretched from the Eider River in the north to the Mediterranean coast in the south .
The edict of the Golden Bull in 1356 provided the basic constitution of the empire that lasted until its dissolution .
Beginning in the 15th century , the emperors were elected nearly exclusively from the Habsburg dynasty of Austria .
The monk Martin Luther publicised his 95 Theses in 1517 , challenging practices of the Roman Catholic Church , initiating the Protestant Reformation .
Bismarck successfully waged war on Denmark in 1864 .
The empire was a unification of all the scattered parts of Germany except Austria .
Most alliances in which Germany had been previously involved were not renewed , and new alliances excluded the country .
Aside from its contacts with Austria-Hungary , Germany became increasingly isolated .
Germany 's imperialism reached outside of its own country and joined many other powers in Europe in claiming their share of Africa .
Germany owned several pieces of land in Africa including German East Africa , South-West Africa , Togo , and Cameroon .
The Scramble for Africa caused tension between the great powers that may have contributed to the conditions that led to World War I .
Its negotiation , contrary to traditional post-war diplomacy , excluded the defeated Central Powers .
The treaty was perceived in Germany as a humiliating continuation of the war .
At the beginning of the German Revolution , Germany was declared a republic and the monarchy collapsed .
However , the struggle for power continued , with radical-left communists seizing power in Bavaria , but failing to take control of all of Germany .
The revolution came to an end in August 1919 , when the Weimar Republic was formally established .
The Weimar Constitution came into effect with its signing by President Friedrich Ebert on 11 August 1919 .
This was exacerbated by a widespread right-wing Dolchstoßlegende , which promoted the view that Germany had lost World War I because of the efforts and influence of those who wanted to overthrow the government .
Many conservatives were drawn towards the reactionary/revolutionary right , particularly the National Socialist German Workers Party -- the Nazi Party .
On 27 February 1933 , the Reichstag building went up in flames , and a consequent emergency decree abrogated basic citizen rights .
Using his powers to crush any actual or potential resistance , Hitler established a centralised totalitarian state within months .
In 1935 , Germany reacquired control of the Saar and in 1936 military control of the Rhineland , both of which had been lost by the Treaty of Versailles .
In 1938 and 1939 , Austria and Czechoslovakia were brought under control and the invasion of Poland prepared .
On 22 June 1941 , Germany broke the Hitler-Stalin pact and invaded the Soviet Union .
Although the German army advanced into the Soviet Union quite rapidly , the Battle of Stalingrad marked a major turning point in the war .
Subsequently , the German army started to retreat on the Eastern front .
In September 1943 , Germany 's ally Italy surrendered , and German forces were forced to defend an additional front in Italy .
Germany 's defeat soon followed .
In what later became known as The Holocaust , the Third Reich regime enacted governmental policies directly subjugating many dissidents and minorities .
World War II and the Nazi genocide were responsible for more than 40 million dead in Europe .
The remaining national territory and Berlin were partitioned by the Allies into four military occupation zones .
While claiming to be a democracy , political power was solely executed by leading members of the communist-controlled SED ( Socialist Unity Party of Germany ) .
Their power was ensured by the Stasi , a secret service of immense size , and a variety of SED suborganizations controlling every aspect of society .
A Soviet-style command economy was set up ; later , the GDR became a Comecon state .
This permitted German reunification on 3 October 1990 , with the accession of the five re-established states in the former GDR ( New states or " neue Länder " ) .
Since reunification , Germany has taken a more active role in the European Union and NATO .
These deployments were controversial , since after the war , Germany was bound by domestic law only to deploy troops for defence roles .
In 2005 Angela Merkel was elected the first female Chancellor of Germany .
Following general elections on September 27 , 2009 , Merkel built the current coalition government replacing the Social Democrats with Free Democratic Party ( FDP ) .
The territory of Germany covers 357021 km 2 ( 137847 sq mi ) , consisting of 349223 km 2 ( 134836 sq mi ) of land and 7798 km 2 ( 3011 sq mi ) of water .
It is the seventh largest country by area in Europe and the 63rd largest in the world .
Elevation ranges from the mountains of the Alps ( highest point : the Zugspitze at 2962 metres / 9718 feet ) in the south to the shores of the North Sea in the north-west and the Baltic Sea ( Ostsee ) in the north-east .
Between lie the forested uplands of central Germany and the low-lying lands of northern Germany ( lowest point : Wilstermarsch at 3.54 metres / 11.6 feet below sea level ) , traversed by some of Europe 's major rivers such as the Rhine , Danube and Elbe .
Its neighbours are Denmark in the north , Poland and the Czech Republic in the east , Austria and Switzerland in the south , France and Luxembourg in the south-west and Belgium and the Netherlands in the north-west .
Germany comprises 16 states , which are further subdivided into 439 districts and cities .
Most of Germany has a temperate seasonal climate in which humid westerly winds predominate .
This warmer water affects the areas bordering the North Sea including the area along the Rhine , which flows into the North Sea .
Central and southern Germany are transition regions which vary from moderately oceanic to continental .
The majority of Germany is covered by either arable land ( 33 % ) or forestry and woodland ( 31 % ) .
Fish abound in the rivers and the North Sea .
Various migratory birds cross Germany in the spring and autumn .
Germany is known for its many zoological gardens , wildlife parks , aquaria , and bird parks .
More than 400 registered zoos and animal parks operate in Germany , which is believed to be the largest number in any single country of the world .
The Zoologischer Garten Berlin is the oldest zoo in Germany and presents the most comprehensive collection of species in the world .
Germany is known for its environmental consciousness .
The government under Chancellor Schröder announced the intention to end the use of nuclear power for producing electricity .
Germany is a federal , parliamentary , representative democratic republic .
The Chancellor -- currently Angela Merkel -- is the head of government and exercises executive power , similar to the role of a Prime Minister in other parliamentary democracies .
The Bundestag is elected through direct elections , by proportional representation ( mixed-member ) .
The President , Christian Wulff , is the head of state and invested primarily with representative responsibilities and powers .
The Chancellor can be removed by a constructive motion of no confidence by the Bundestag , where constructive implies that the Bundestag simultaneously has to elect a successor .
Since 1949 , the party system has been dominated by the Christian Democratic Union and the Social Democratic Party of Germany with all chancellors hitherto being member of either party .
However , the smaller liberal Free Democratic Party ( which has had members in the Bundestag since 1949 ) and the Alliance ' 90/The Greens ( which has controlled seats in parliament since 1983 ) have also played important roles , as they are regularly the smaller partner of a coalition government .
The Bundesverfassungsgericht ( Federal Constitutional Court ) , located in Karlsruhe , is the German Supreme Court responsible for constitutional matters , with power of judicial review .
For civil and criminal cases , the highest court of appeal is the Federal Court of Justice , located in Karlsruhe and Leipzig .
In such cases , final appeal to the Federal Administrative Court is possible .
The alliance was especially close in the late 1980s and early 1990s under the leadership of Christian Democrat Helmut Kohl and Socialist François Mitterrand .
For a number of decades after WWII , the Federal Republic of Germany kept a notably low profile in international relations , because of both its recent history and its occupation by foreign powers .
The governments of Germany and the United States are close political allies .
The other way around , 8.8 % of U.S. exports ship to Germany and 9.8 % of U.S. imports come from Germany .
Germany 's official development aid and humanitarian aid for 2007 amounted to 8.96 billion euros ( 12.26 billion dollars ) , an increase of 5.9 per cent from 2006 .
Germany spent 0.37 per cent of its gross domestic product ( GDP ) on development , which is below the government 's target of increasing aid to 0.51 per cent of GDP by 2010 .
Germany 's military , the Bundeswehr , is a military force with Heer , Marine ( Navy ) , Luftwaffe , Zentraler Sanitätsdienst ( Central Medical Services ) and Streitkräftebasis branches .
If Germany went to war , which according to the constitution is allowed only for defensive purposes , the Chancellor would become commander in chief of the Bundeswehr .
The Bundeswehr employs 200,500 professional soldiers , 55,000 18 -- 25 year-old conscripts who serve for at least six months under current rules , and 2,500 active reservists at any given time .
With 82 million inhabitants in January 2010 , Germany is the most populous country in the European Union .
With death rates continuously exceeding low-level birth rates , Germany is one of a few countries for which the demographic transition model would require a fifth stage in order to capture its demographic development .
Germany has a number of large cities ; the most populous are : Berlin , Hamburg , Munich , Cologne , Frankfurt , and Stuttgart .
By far the largest conurbation is the Rhine-Ruhr region ( 12 million ) , including Düsseldorf ( the capital of North Rhine-Westphalia ) , Cologne , Essen , Dortmund , Duisburg , and Bochum .
Germans make up 91 % of the population of Germany .
As of 2008 , the largest national group of people with a migrant background was from Turkey ( 2.5 million ) , followed by Italy ( 776,000 ) and Poland ( 687,000 ) .
Hinduism has some 90,000 adherents ( 0.1 % ) and Sikhism 75,000 ( 0.09 % ) .
All other religious communities in Germany have fewer than 50,000 ( or less than 0.05 % ) adherents .
Protestantism is concentrated in the north and east and Roman Catholicism is concentrated in the south and west .
The current Pope , Benedict XVI , was born in Bavaria .
Non-religious people , including atheists and agnostics might make as many as 55 % , and are especially numerous in the former East Germany and major metropolitan areas .
Germany has Europe 's third-largest Jewish population ( after France and the United Kingdom ) .
Large cities with significant Jewish populations include Berlin , Frankfurt and Munich .
German is the official and predominant spoken language in Germany .
Germany has a social market economy characterised by a highly qualified labour force , a developed infrastructure , a large capital stock , a low level of corruption ( ranked 14th ) and a high level of innovation ( ranked 19th ) .
Germany was the world 's largest exporter from 2003 to 2008 .
Germany is the leading producer of wind turbines and solar power technology in the world .
In 2007 the ten largest were Daimler , Volkswagen , Allianz ( the most profitable company ) , Siemens , Deutsche Bank ( 2nd most profitable company ) , E.ON , Deutsche Post , Deutsche Telekom , Metro , and BASF .
Among the largest employers are also Deutsche Post , Robert Bosch GmbH , and Edeka .
Well known global brands are Mercedes Benz , SAP , BMW , Adidas , Audi , Porsche , Volkswagen , and Nivea .
With its central position in Europe , Germany is an important transportation hub .
Germany has established a polycentric network of high-speed trains .
Germany is the world 's fifth largest consumer of energy , and two-thirds of its primary energy was imported in 2002 .
In the same year , Germany was Europe 's largest consumer of electricity , totaling 512.9 terawatt-hours .
Germany has been the home of some of the most prominent researchers in various scientific fields .
They were preceded by physicists such as Hermann von Helmholtz , Joseph von Fraunhofer , and Gabriel Daniel Fahrenheit .
This accomplishment made him the first winner of the Nobel Prize in Physics in 1901 .
Heinrich Rudolf Hertz 's work in the domain of electromagnetic radiation was pivotal to the development of modern telecommunication .
Through his construction of the first laboratory at the University of Leipzig in 1879 , Wilhelm Wundt is credited with the establishment of psychology as an independent empirical science .
Alexander von Humboldt 's work as a natural scientist and explorer was foundational to biogeography .
Numerous significant mathematicians were born in Germany , including Carl Friedrich Gauss , David Hilbert , Bernhard Riemann , Gottfried Leibniz , Karl Weierstrass and Hermann Weyl .
Important research institutions in Germany are the Max Planck Society , the Helmholtz-Gemeinschaft and the Fraunhofer Society .
The prestigious Gottfried Wilhelm Leibniz Prize is granted to ten scientists and academics every year .
Germany 's universities are recognised internationally , indicating the high education standards in the country .
In the ARWU ranking for 2008 , six of the top 100 universities in the world are in Germany , and 18 in the top 200 .
Germany claims some of the world 's most renowned classical music composers , including Ludwig van Beethoven , Johann Sebastian Bach , Johannes Brahms and Richard Wagner .
As of 2006 , Germany is the fifth largest music market in the world and has influenced pop and rock music through artists such as Tokio Hotel , Kraftwerk , Scorpions and Rammstein .
Germany was particularly important in the early modern movement , especially through the Bauhaus movement founded by Walter Gropius .
Ludwig Mies van der Rohe , also from Germany , became one of the world 's most renowned architects in the second half of the 20th century .
Influential authors of the 20th century include Thomas Mann , Berthold Brecht , Hermann Hesse , Heinrich Böll , and Günter Grass .
Germany is home to some of the world 's largest media conglomerates , including Bertelsmann and the Axel Springer AG .
It represents 18 % of all the books published worldwide and puts Germany in third place among the world 's book producers .
The Frankfurt Book Fair is considered to be the most important book fair in the world for international deals and trading and has a tradition that spans over 500 years .
German cinema dates back to the very early years of the medium with the work of Max Skladanowsky .
The Nazi era produced mostly propaganda films although the work of Leni Riefenstahl still introduced new aesthetics to film .
More recently , films such as Good Bye Lenin ! ( 2003 ) , Gegen die Wand ( Head-on ) ( 2004 ) , Der Untergang ( Downfall ) ( 2004 ) , and Der Baader Meinhof Komplex ( 2008 ) have enjoyed international success .
The Berlin Film Festival , held annually since 1951 , is one of the world 's foremost film festivals .
The annual European Film Awards ceremony is held every second year in the city of Berlin , where the European Film Academy ( EFA ) is located .
The Babelsberg Studios in Potsdam are the oldest large-scale film studios in the world and a centre for international film production .
With more than 6.3 million official members , the German Football Association ( Deutscher Fußball-Bund ) is the largest sports organisation of its kind worldwide .
The German national football team won the FIFA World Cup in 1954 , 1974 and 1990 and the UEFA European Football Championship in 1972 , 1980 and 1996 .
Germany has hosted the FIFA World Cup in 1974 and 2006 and the UEFA European Football Championship in 1988 .
Among the most successful and renowned footballers are Franz Beckenbauer , Gerd Müller , Jürgen Klinsmann , Lothar Matthäus , and Oliver Kahn .
Germany is one of the leading motorsports countries in the world .
Race winning cars , teams and drivers have come from Germany .
Porsche has won the 24 hours of Le Mans , a prestigious annual race held in France , 16 times .
The Deutsche Tourenwagen Masters is a popular series in Germany [ citation needed ] .
In the 2008 Summer Olympics , Germany finished fifth in the medal count , while in the 2006 Winter Olympics they finished first .
Germany has hosted the Summer Olympic Games twice , in Berlin in 1936 and in Munich in 1972 .
The Winter Olympic Games took place in Germany once in 1936 when they were staged in the Bavarian twin towns of Garmisch and Partenkirchen .
Pork , beef , and poultry are the main varieties of meat consumed in Germany , with pork being the most popular .
More than 1500 different types of sausage are produced in Germany .
As a country with many immigrants , Germany has adopted many international dishes into its cuisine and daily eating habits .
Among high-profile restaurants in Germany , the Michelin guide has awarded nine restaurants three stars , the highest designation , while 15 more received two stars .
Although wine is becoming more popular in many parts of Germany , the national alcoholic drink is beer .
Among 18 surveyed western countries , Germany ranked 14th in the list of per capita consumption of soft drinks in general , while it ranked third in the consumption of fruit juices .
Since the 2006 World Cup celebrations the internal and external perception of Germany 's national image has changed .
Germany has been named the world 's most valued nation among 50 countries in 2008 .
Another global opinion poll based on 29,977 responses in 28 countries for the BBC revealed that Germany is recognised for the most positive influence in the world in 2010 , leading 28 investigated countries .
Germany is a legally and socially tolerant country towards homosexuals .
During the last decade of the 20th century Germany has transformed its attitude towards immigrants considerably .
Many guest workers were of Turkish origins and brought an interesting influence into the country .
