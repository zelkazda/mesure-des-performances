Dirk Benedict ( born Dirk Niewoehner ; March 1 , 1945 ) is an American movie , television and stage actor , perhaps best known for playing the characters Lieutenant Templeton " Faceman " Peck in The A-Team television series and Lieutenant Starbuck in the original Battlestar Galactica film and television series .
Benedict 's film debut was in the 1972 film Georgia , Georgia , which was entered into the 23rd Berlin International Film Festival .
While there , he appeared as a guest lead on Hawaii Five-O .
The producers of a horror film called Ssssss saw Benedict 's performance in Hawaii Five-O and promptly cast him as the lead in that movie .
He next played the psychotic wife-beating husband of Twiggy in her American film debut , W . Benedict starred in the television series Chopper One which aired for one season in 1974 .
He also made an appearance in Charlie 's Angels .
Benedict 's career break came in 1978 when he appeared as Lieutenant Starbuck in the movie and television series Battlestar Galactica .
He did not appear in the pilot film -- creators Stephen J. Cannell and Frank Lupo had wanted him from the beginning , but the network executives insisted that they wanted a different sort of actor in the role .
Tim Dunigan won the part ; but upon completion of the pilot , the executives decided that he was n't quite right for the role , and it was given to Benedict .
He played " Faceman " from 1982 to 1986 ( although the series did n't air until January 1983 , and the final episode was n't shown until 1987 re-runs ) .
In the opening credits , the character is seen looking bemused as a Cylon walks by him -- an in-joke to his previous role in Battlestar Galactica .
In 1993 Benedict starred in Shadow Force .
Dirk finished in third place overall .
Benedict is a macrobiotic follower .
In 1975 , Benedict was diagnosed with prostate cancer and was told that the cure would be castration .
Benedict was sharply critical of the revived series , and the changes to the story and characters .
In the new un-imagined , re-imagined world of Battlestar Galactica everything is female driven .
Benedict also added that " Men hand out cigars .
