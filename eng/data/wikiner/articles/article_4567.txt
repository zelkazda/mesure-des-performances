The new Republican government sought to institute , among other reforms , a new social and legal system , a new system of weights and measures ( which became the metric system ) , and a new calendar .
Natural constants , multiples of ten , and Latin derivations formed the fundamental blocks from which the new systems were built .
They associated to their work the chemist Louis-Bernard Guyton de Morveau , the mathematician and astronomer Joseph-Louis Lagrange , the astronomer Joseph Jérôme Lefrançois de Lalande , the mathematician Gaspard Monge , the astronomer and naval geographer Alexandre Guy Pingré , and the poet , actor and playwright Fabre d'Églantine , who invented the names of the months , with the help of André Thouin , gardener at the Jardin des Plantes of the Muséum National d'Histoire Naturelle in Paris .
It is because of his position as rapporteur of the commission that the creation of the republican calendar is attributed to Romme .
Napoléon finally abolished the calendar with effect from 1 January 1806 , a little over twelve years after its introduction .
Like the French originals , they suggest a meaning related to the season but are not actual words .
This is inconsistent with Romme 's assertion that the new calendar should be faithful to natural cycles and should not perpetuate past mistakes :
The proposal for the new calendar is a litany of criticism of previous efforts , the previous quote of Romme being representative .
Perhaps the most famous date in this calendar was immortalised by Karl Marx in the title of his pamphlet , The 18th Brumaire of Louis Napoléon ( 1852 ) .
The 18 Brumaire An VIII ( 9 November 1799 ) is considered , by many historians , the end of the French Revolution .
For example , Trotsky and his followers used this term about Stalin .
Émile Zola 's novel Germinal takes its name from the calendar .
The French frigates of the Floréal class all bear names of Republican months .
