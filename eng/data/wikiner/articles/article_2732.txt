Seven states declared their secession from the United States before Lincoln took office on March 4 , 1861 :
After the Confederate attack on Fort Sumter on April 12 , 1861 , and Lincoln 's subsequent call for troops on April 15 , four more states declared their secession :
The border states of Kentucky and Missouri declared neutrality very early in the war .
Delaware , also a slave state , never considered secession , nor did Washington , D.C .
Although the slave states of Maryland and Delaware did not secede , citizens from those states did exhibit divided loyalties .
Only Delaware among the slave states did not produce a full regiment to fight for the Confederacy .
However , 24 of those counties had voted in favor of Va. 's secession , and control of these counties , as well as some counties that had voted against secession , remained contested until the end of the war .
Confederate declarations of martial law checked attempts to secede from the Confederate States of America by some counties in East Tennessee .
Citizens at Mesilla and Tucson in the southern part of New Mexico Territory ( modern day N.M. and Ariz. ) formed a secession convention , which voted to join the Confederacy on March 16 , 1861 and appointed Lewis Owings as the new territorial governor .
After the battle , Baylor established a territorial government for the Confederate Arizona Territory and named himself governor .
Although Confederates briefly occupied the territorial capital of Santa Fe , they suffered defeat at Glorietta Pass in March and retreated , never to return .
The five tribal governments of the Indian Territory -- which became Okla. in 1907 -- mainly supported the Confederacy , providing troops and one general officer .
After 1863 the tribal governments sent representatives to the Confederate Congress : Elias Cornelius Boudinot representing the Cherokee and Samuel Benton Callahan representing the Seminole and Creek people .
Civil War historian James M. McPherson wrote :
Texas mentioned slavery twenty-one times , but also listed the failure of the federal government to live up to its obligations , in the original annexation agreement , to protect settlers along the exposed western frontier .
Texas further stated :
The American Civil War broke out in April 1861 with the Battle of Fort Sumter in Charleston , South Carolina .
Federal troops of the U.S. had retreated to Fort Sumter soon after South Carolina declared its secession on 20 December 1860 .
U.S. President Abraham Lincoln also attempted to resupply Sumter .
Lincoln notified South Carolina Governor Francis W. Pickens that " an attempt will be made to supply Fort Sumter with provisions only , and that if such attempt be not resisted , no effort to throw in men , arms , or ammunition will be made without further notice , [ except ] in case of an attack on the fort . "
However , suspecting just such an attempt to reinforce the fort , the Confederate cabinet decided at a meeting in Montgomery to capture Fort Sumter before the relief fleet arrived .
After the war , Confederate Vice President Alexander H. Stephens maintained that Lincoln 's attempt to resupply Sumter was a disguised reinforcement and had provoked the war .
Virginia , Arkansas , Tennessee , and North Carolina joined the Confederacy , bringing the total to eleven states .
Once Virginia had joined , the Confederate States moved their capital from Montgomery , Alabama , to Richmond , Va. .
All but two major battles ( Antietam and Gettysburg ) took place in Confederate territory .
The surrender of the Army of Northern Virginia by General Lee at the Appomattox Court House on April 9 , 1865 marked the end of the Confederacy .
The U.S. Army took control of the Confederate areas and there was no post-surrender insurgency or guerrilla warfare against the army , but there was a great deal of local violence , feuding and revenge killings .
State legislatures had the power to impeach officials of the Confederate government in some cases .
During the debates on drafting the Confederate Constitution , one proposal would have allowed states to secede from the Confederacy .
The proposal was tabled with only the South Carolina delegates voting in favor of considering the motion .
The only person to serve as president was Jefferson Davis , due to the Confederacy being defeated before the completion of his term .
As its legislative branch , the Confederate States of America instituted the Confederate Congress .
The Confederate Constitution outlined a judicial branch of the government , but the ongoing war and resistance from states-rights advocates , particularly on the question of whether it would have appellate jurisdiction over the state courts , prevented the creation or seating of the " Supreme Court of the Confederate States " ; the state courts generally continued to operate as they had done , simply recognizing the CSA as the national government .
Confederate district courts began reopening in the spring of 1861 handling many of the same type cases as had been done before .
Supreme Court -- not established .
Montgomery , Ala. served as the capital of the Confederate States of America from February 4 until May 29 , 1861 .
The naming of Richmond , Va. as the new capital took place on May 30 , 1861 .
Shortly before the end of the war , the Confederate government evacuated Richmond , planning to relocate farther south .
Little came of these plans before Lee 's surrender at Appomattox Court House on April 9 , 1865 .
Danville , Va. , served as the last capital of the Confederate States of America , from April 3 to April 10 , 1865 .
Both the individual Confederate states and later the Confederate government printed Confederate States of America dollars as paper currency in various denominations , much of it signed by the Treasurer Edward C. Elmore .
The Confederate government initially financed the war effort mostly through tariffs on imports , export taxes , and voluntary donations of coins and bullion .
However the four half dollars with a CSA ( rather than USA ) reverse , mentioned below , used an obverse die that had a small crack .
Thus " regular " 1861-O halves with this crack probably were among the 962,633 pieces struck under Confederate authority .
In 1861 plans also originated to produce Confederate coins .
The New Orleans Mint produced dies and four specimen half dollars , but a lack of bullion prevented any further minting .
More details and pictures of the original issues appear in A Guide Book of United States Coins .
When Lincoln released the two , however , tensions cooled , and in the end the episode did not aid the Confederate cause .
No nation ever sent an ambassador or an official delegation to Richmond .
Some state governments in northern Mexico negotiated local agreements to cover trade on the Texas border .
Historian Frank Lawrence Owsley argued that the Confederacy " died of states ' rights . "
Georgia 's governor Joseph Brown warned that he saw the signs of a deep-laid conspiracy on the part of Jefferson Davis to destroy states ' rights and individual liberty .
Brown declaimed : " Almost every act of usurpation of power , or of bad faith , has been conceived , brought forth and nurtured in secret session . "
He saw granting the Confederate government the power to draft soldiers as the " essence of military despotism . "
Zebulon Vance , the governor of N.C. , had a reputation for hostility to Davis and to his demands .
North Carolina showed intense opposition to conscription , resulting in very poor results for recruiting .
Governor Vance 's faith in states ' rights drove him into a stubborn opposition .
Despite political differences within the Confederacy , no political parties were formed .
Almost nobody , even Davis 's most fervent antagonists , advocated parties . "
The survival of the Confederacy depended on a strong base of civilians and soldiers devoted to victory .
This contraction of civic vision was more than a crabbed libertarianism ; it represented an increasingly widespread disillusionment with the Confederate experiment . "
During the four years of its existence , the Confederate States of America asserted its independence and appointed dozens of diplomatic agents abroad .
Jefferson Davis , former President of the Confederacy , and Alexander Stephens , its former Vice-President , both penned arguments in favor of secession 's legality , most notably Davis ' The Rise and Fall of the Confederate Government .
The Confederate States of America claimed a total of 2,919 miles ( 4,698 km ) of coastline , thus a large part of its territory lay on the seacoast with level and often sandy or marshy ground .
The highest point ( excluding Arizona and New Mexico ) was Guadalupe Peak in Texas at 8,750 feet ( 2,667 m ) .
Much of the area claimed by the Confederate States of America had a humid subtropical climate with mild winters and long , hot , humid summers .
The climate and terrain varied from vast swamps ( such as those in Florida and Louisiana ) to semi-arid steppes and arid deserts west of longitude 96 degrees west .
The outbreak of war had a depressing effect on the economic fortunes of the railroad system in Confederate territory .
For the early years of the war , the Confederate government had a hands-off approach to the railroads .
Only in mid-1863 did the Confederate government initiate an overall policy , and it was confined solely to aiding the war effort .
In the last year before the end of the war , the Confederate railroad system stood permanently on the verge of collapse .
The area claimed by the Confederate States of America consisted overwhelmingly of rural land .
Only 13 Confederate-controlled cities ranked among the top 100 U.S. cities in 1860 , most of them ports whose economic activities vanished or suffered severely in the Union blockade .
The population of Richmond swelled after it became the national capital , reaching an estimated 128,000 in 1864 .
The cities of the Confederacy included most prominently in order of size of population :
The Confederacy started its existence as an agrarian economy with exports , to a world market , of cotton , and , to a lesser extent , tobacco and sugarcane .
The lack of adequate financial resources led the Confederacy to finance the war through printing money , which led to high inflation .
The requirements of its military encouraged the Confederate government to take a dirigiste-style approach to industrialization .
( Figures for Va. include the future West Virginia . )
In 1860 the areas that later formed the eleven Confederate States ( and including the future West Virginia ) had 132,760 ( 1.46 % ) free blacks .
The military armed forces of the Confederacy comprised three branches :
Many had served in the Mexican-American War ( including Robert E. Lee and Jefferson Davis ) , but some such as Leonidas Polk ( who had attended West Point but did not graduate ) had little or no experience .
The Confederate officer corps consisted of men from both slave-owning and non-slave-owning families .
The Confederacy appointed junior and field grade officers by election from the enlisted ranks .
A naval academy was established at Drewry 's Bluff , Virginia in 1863 , but no midshipmen graduated before the Confederacy 's end .
The soldiers of the Confederate armed forces consisted mainly of white males aged between sixteen and twenty-eight .
Some freed blacks and men of color served in local state militia units of the Confederacy , primarily in Louisiana and S.C. , but their officers deployed them for " local defense , not combat . "
In the spring of 1865 , the Confederate Congress , influenced by the public support by General Lee , approved the recruitment of black infantry units .
Military leaders of the Confederacy ( with their state or country of birth and highest rank ) included :
