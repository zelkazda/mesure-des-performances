The study of plants is vital because they are a fundamental part of life on Earth , which generates the oxygen , food , fibres , fuel and medicine that allow humans and other life forms to exist .
The genetic laws of inheritance were discovered in this way by Gregor Mendel , who was studying the way pea shape is inherited .
What Mendel learned from studying plants has had far reaching benefits outside of botany .
Additionally , Barbara McClintock discovered ' jumping genes ' by studying maize .
There was also the 11th century scientists and statesmen Su Song and Shen Kuo , who compiled treatises on herbal medicine and included the use of mineralogy .
Approximately 1300-1400 different plant species were known under Roman reign .
His student Ibn al-Baitar ( d. 1248 ) wrote a pharmaceutical encyclopedia describing 1,400 plants , foods , and drugs , 300 of which were his own original discoveries .
German physician Leonhart Fuchs ( 1501 -- 1566 ) was one of the three founding fathers of botany , along with Otto Brunfels ( 1489-1534 ) and Hieronymus Bock ( 1498 -- 1554 ) ( also called Hieronymus Tragus ) .
In 1665 , using an early microscope , Robert Hooke discovered cells in cork , and a short time later in living plant tissue .
The increased knowledge on anatomy , morphology and life cycles , lead to the realization that there were more natural affinities between plants , than the sexual system of Linnaeus indicated .
Adanson ( 1763 ) , de Jussieu ( 1789 ) , and Candolle ( 1819 ) all proposed various alternative natural systems that were widely followed .
