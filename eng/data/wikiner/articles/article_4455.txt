The full name of the movement is حركة التحرير الوطني الفلسطيني ḥa rakat al-ta ḥrīr al-waṭanī al-f ilasṭīnī , meaning the " Palestinian National Liberation Movement " .
Two most important decision-making bodies is Central Committee of Fatah and the Fatah Revolutionary Council .
The founders included Yasser Arafat who was head of the General Union of Palestinian Students ( GUPS ) ( 1952 -- 56 ) in Cairo University , Salah Khalaf , Khalil al-Wazir , Khaled Yashruti was head of the GUPS in Beirut ( 1958 -- 62 ) .
Fatah joined the Palestine Liberation Organization ( PLO ) in 1967 .
According to the BBC , " Mr Arafat took over as chairman of the executive committee of the PLO in 1969 , a year that Fatah is recorded to have carried out 2,432 guerrilla attacks on Israel . "
The town 's name is the Arabic word for " dignity, " which elevated its symbolism to the Arab people , especially after the Arab defeat in 1967 .
Knowledge of the operation was available well ahead of time , and the government of Jordan ( as well as a number of Fatah commandos ) informed Arafat of Israel 's large-scale military preparations .
Upon hearing the news , many guerrilla groups in the area , including George Habash 's newly formed group the Popular Front for the Liberation of Palestine ( PFLP ) and Nayef Hawatmeh 's breakaway organization the Democratic Front for the Liberation of Palestine ( DFLP ) , withdrew their forces from the town .
On the night of March 21 , the IDF attacked Karameh with heavy weaponry , armored vehicles and fighter jets .
Fatah held its ground , surprising the Israeli military .
Despite the higher Arab death toll , Fatah considered themselves victorious because of the Israeli army 's rapid withdrawal .
Two thousand Fatah fighters managed to enter Syria .
They crossed the border into Lebanon to join Fatah forces in that country , where they set up their new headquarters .
Some militant groups that affiliated themselves to Fatah , and some of the fedayeen within Fatah itself , carried out civilian plane hijackings and terrorist attacks , attributing them to Black September , Abu Nidal 's Fatah-Revolutionary Council , Abu Musa 's group , the PFLP , and the PFLP-GC .
The People 's Republic of China also provided munitions .
Although hesitant at first to take sides in the conflict , Arafat and Fatah played an important role in the Lebanese Civil War .
Although originally aligned with Fatah , Syrian President Hafez al-Assad feared a loss of influence in Lebanon and switched sides .
Phalangist forces killed twenty-six Fatah trainees on a bus in April 1975 , marking the official start of the 15 year long Lebanese civil war .
The PLO and LNM retaliated by attacking the town of Damour , a Phalangist stronghold .
Arafat and Abu Jihad blamed themselves for not successfully organizing a rescue effort .
PLO cross-border raids against Israel grew somewhat during the late 1970s .
A force of nearly a dozen Fatah fighters landed their boats near a major coastal road connecting the city of Haifa with Tel Aviv-Yafo .
The IDF achieved this goal , and Fatah withdrew to the north into Beirut .
Israel invaded Lebanon again in 1982 .
Despite the exile many Fatah commanders and fighters remained in Lebanon .
In the period 1982 -- 1993 , Fatah 's leadership resided in Tunisia .
Farouk Kaddoumi is the current Fatah chairman , elected to the post soon after Arafat 's death in 2004 .
Fatah endorsed Mahmoud Abbas in the Palestinian presidential election of 2005 .
Political analyst Salah Abdel-Shafi told BBC about the difficulties of Fatah leadership : " I think it 's very , very serious -- it 's becoming obvious that they ca n't agree on anything . "
Fatah is " widely seen as being in desperate need of reform " , as " the PA 's performance has been a story of corruption and incompetence -- and Fatah has been tainted . "
There have been numerous other expressions of discontent within Fatah , which is just holding its first general congress in two decades .
Several of them gained their positions thanks to personal followings or support from Arafat , who balanced above the different factions , and the era after his death in 2004 has seen increased infighting among these groups , who jockey for influence over future development , the political line , funds , and constituencies .
The prospect of Abbas leaving power in the coming years has also exacerbated tensions .
There have been no significant overt splits within the older generation of Fatah politicians since the 1980s , however .
Since Arafat 's death , he is formally head of Fatah 's political bureau and chairman , but his actual political following within Fatah appears limited .
He has at times openly challenged the legitimacy of Abbas and harshly criticized both him and Mohammed Dahlan , but despite threats to splinter the movement , he remains in his position , and his challenges have so far come to nothing .
Another influential veteran , Hani al-Hassan , has also openly criticized the present leadership .
The younger generations of Fatah , especially within the militant al-Aqsa Martyrs ' Brigades , have been more prone to splits , and a number of lesser networks in Gaza and the West Bank have established themselves as either independent organizations or joined Hamas .
However , such overt breaks with the movement have still been rather uncommon , despite numerous rivalries inside and between competing local Fatah groups .
Some 400 Fatah members from the Gaza Strip were unable to attend the conference in Bethlehem after Hamas barred them from traveling to the West Bank .
Israeli Defense Minister Ehud Barak described the adopted Fatah platform as not very promising .
However , Mahmoud Abbas hailed the elections as " democratic and successful . "
Fatah has maintained a number of militant groups since its founding .
Fatah has since its inception created , led or sponsored a number of armed groups and militias , some of which have had an official standing as the movement 's armed wing , and some of which have not been publicly or even internally recognized as such .
The group has also dominated various PLO and Palestinian Authority forces and security services which were/are not officially tied to Fatah , but in practice have served as wholly pro-Fatah armed units , and been staffed largely by members .
This name has since been applied more generally to Fatah armed forces , and does not correspond to a single unit today .
Other militant groups associated with Fatah include :
