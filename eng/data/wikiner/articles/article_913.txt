Born in Scotland , he served in the British Army during the French and Indian War before settling in Pennsylvania , where he held local office .
During the American Revolutionary War , he rose to the rank of major general in the Continental Army , but lost his command after a controversial retreat .
Although an investigation exonerated him , St. Clair resigned his army commission .
He continued to serve as territorial governor until 1802 , when he retired to Pa. .
St. Clair was born in Thurso , Caithness , Scotland .
He reportedly attended the University of Edinburgh before being apprenticed to the renowned physician William Hunter .
In 1757 , St. Clair purchased a commission in the British Army , Royal American Regiment , and came to America with Admiral Edward Boscawen 's fleet for the French and Indian War .
On April 17 , 1759 , he received a lieutenant 's commission and was assigned to the command of General James Wolfe , under whom he served at the Battle of the Plains of Abraham .
On April 16 , 1762 , he resigned his commission , and , in 1764 , he settled in Ligonier Valley , Pa. , where he purchased land and erected mills .
He was the largest landowner in Western Pennsylvania .
In 1770 , St. Clair became a justice of the court , of quarter sessions and of common pleas , a member of the proprietary council , a justice , recorder , and clerk of the orphans ' court , and prothonotary of Bedford and Westmoreland counties .
In 1774 , the colony of Virginia took claim of the area around Pittsburgh , Pennsylvania , and some residents of Western Pennsylvania took up arms to eject them .
St. Clair issued an order for the arrest of the officer leading the Virginia troops .
Lord Dunmore 's War eventually settled the boundary dispute .
In January 1776 , he accepted a commission in the Continental Army as a colonel of the 3rd Pennsylvania Regiment .
He first saw service in the later days of the Quebec invasion , where he saw action in the Battle of Trois-Rivières .
He was appointed a brigadier general in August 1776 , and was sent by Gen. George Washington to help organize the N.J. militia .
Many biographers credit St. Clair with the strategy which led to Washington 's capture of Princeton , New Jersey in the following days .
It was shortly after this that St. Clair was promoted to Major General .
In April 1777 , St. Clair was sent to defend Fort Ticonderoga .
St. Clair was forced to retreat at the Siege of Fort Ticonderoga on July 5 , 1777 .
In 1778 he was court-martialed for the loss of Ticonderoga .
He still saw action , however , as an aide-de-camp to General Washington , who retained a high opinion of him .
St. Clair was at Yorktown when Lord Cornwallis surrendered his army .
Congress enacted its most important piece of legislation , the Northwest Ordinance , during St. Clair 's tenure as president .
Under the Northwest Ordinance of 1787 , which created the Northwest Territory , General St. Clair was appointed governor of what is now Ohio , Indiana , Illinois , Michigan , along with parts of Wisconsin and Minnesota .
He named Cincinnati , Ohio , after the Society of the Cincinnati , and it was there that he established his home .
When the territory was divided in 1800 , he served as governor of the Ohio Territory .
In 1791 , St. Clair succeeded Harmar as the senior general of the United States Army .
He personally led a punitive expedition comprising of two Regular Army regiments and some militia .
After this debacle , he resigned from the army at the request of President Washington , but continued to serve as Governor of the Northwest Territory .
In 1802 , his opposition to plans for Ohio statehood led President Thomas Jefferson to remove him from office as territorial governor .
He thus played no part in the organizing of the state of Ohio in 1803 .
The first Ohio Constitution provided for a weak governor and a strong legislature , in part due to a reaction to St. Clair 's method of governance .
Places named in honor of Arthur St. Clair include :
