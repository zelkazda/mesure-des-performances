Tank destroyers of World War II-vintage quickly became obsolete after the war as main battle tanks became more capable .
Dedicated anti-tank vehicles made their first major appearance in the Second World War , as combatants developed effective armored vehicles and tactics .
They suffered heavy losses during the invasion of Poland before entering battles due to air bombardment , penetrating their weak top armor .
Similarly , Panzer II tanks were used on the eastern front .
The Panzer 38 ( t ) chassis was also used to make the Jagdpanzer 38 ' Hetzer ' casemate style tank destroyer .
In 1943 , the Soviets also shifted all production of light tanks like the T-70 to much simpler and better-armed SU-76 self-propelled guns , which used the same drive train .
U.S. Army and counterpart British designs were very different in conception .
As a result , there was extra impetus given to the development of anti-tank weaponry , which culminated in the Ordnance QF 17 pounder , widely considered one of the best anti-tank guns of the war .
The self-propelled guns that were built in the " tank destroyer " mold came about through the desire to field the formidable QF 17 pounder anti-tank gun and simultaneous lack of suitable tanks to carry it .
The Tank , Cruiser , Challenger ( A30 ) was a project to bring a 17 pdr tank into use .
The heavy assault tank known as Tortoise was well armoured and had a very powerful 32 pounder gun but did not reach service use .
Initially this gave each platoon of Shermans one powerfully-armed tank .
Some provisions were made for the fitting of a 105 mm cannon , and many of the vehicles were modified to fire HOT or TOW missiles in place of a main gun .
Italy and Spain use the Italian-built Centauro , a wheeled tank destroyer with a 105 mm cannon .
The gun-armed tank destroyer may possibly see revival in the US Army through the introduction of the Stryker , more specifically , the M1128 Mobile Gun System , a Stryker variant armed with a 105 mm cannon which has remote control and autoloading capabilities .
