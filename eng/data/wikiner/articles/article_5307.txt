After studying in Magdeburg , Zellerfeld , and Hildesheim , Telemann entered the University of Leipzig to study law , but eventually settled on a career in music .
He held important positions in Leipzig , Żary , Eisenach , and Frankfurt before settling in Hamburg in 1721 , where he became musical director of the city 's five main churches .
While Telemann 's career prospered , his personal life was always troubled : his first wife died only a few months after their marriage , and his second wife had extramartial affairs and accumulated a large gambling debt before leaving Telemann .
Telemann was born in Magdeburg , the capital of the Duchy of Magdeburg , Brandenburg-Prussia , into an upper middle class family .
They confiscated all of the boy 's instruments and forbade him any musical activities , yet Telemann continued composing , in secret .
In late 1693 or early 1694 his mother sent him to a school Zellerfeld , hoping that this would influence her son into choosing a different career .
Here too his talents were recognized and in demand : the rector himself commissioned music from Telemann .
Composers such as Antonio Caldara , Arcangelo Corelli , and Johann Rosenmuller were early influences .
Telemann also continued studying various instruments , and eventually became an accomplished multi-instrumentalist : at Hildesheim he taught himself flute , oboe , chalumeau , viola da gamba , double bass , and bass trombone .
In his 1718 autobiography Telemann explained that this decision was taken because of his mother 's urging .
This was not to come : according to Telemann himself , a setting of Psalm 6 by him inexplicably found its way into his luggage and was found by his roommate at the university .
The work was subsequently performed and so impressed those who heard it that the mayor of Leipzig himself approached Telemann and commissioned him to regularly compose works for the city 's two main churches .
Once he established himself as a professional musician in Leipzig , Telemann became increasingly active in organizing the city 's musical life .
During his time at Leipzig , he was continually influenced by the music of Handel , whom he met earlier , in 1701 .
However , Telemann 's growing prominence and methods caused a conflict between him and Kuhnau .
By employing students Telemann took away a major resource for Kuhnau 's choir ( and church music in Leipzig in general ) ; Kuhnau was also concerned that students were too frequently performing in operas , leaving them with less time to devote to church music .
In the end , however , his efforts proved fruitless , and the only thing the council did was to forbid Telemann to appear on the operatic stage .
Kuhnau 's rights were never fully restored , not even after Telemann left Leipzig .
Leipzig authorities only granted him resignation in early 1705 , however , and he arrived in Sorau in June .
In performing his duties at the courty , Telemann was as prolific as in Leipzig , composing at least 200 ouvertures , by his own recollection , and other works .
Unfortunately , the Great Northern War put an end to Telemann 's career at Sorau .
He spent some time in Frankfurt an der Oder before returning to Sorau in the summer .
The details of how Telemann obtained his next position are unknown .
Unfortunately , the mother died soon afterwards ; Telemann 's marriage lasted only for 15 months .
He declined an offer from the Dresden court , since he wanted to work with greater artistic freedom ; Telemann wanted a post similar to the one he had in Leipzig .
The application was successful and Telemann arrived to Frankfurt on 18 March 1712 .
Telemann 's new duties were similar to those he had in Leipzig .
The couple had nine children ( none became musicians ) , but the marriage would later prove disastrous for Telemann .
The composer accepted ; he remained in Hamburg for the rest of his life .
Initially , however , Telemann encountered a number of problems : some church officials found opera and collegium musicum performances to be objectionable ( for " inciting lasciviousness " ) , and the city printer was displeased with Telemann publishing printed texts for his yearly Passions .
The former matter was resolved quickly , but Telemann 's exclusive right to publish his own work was only recognized in full in 1757 .
Telemann 's opera productions were not particularly popular , and eventually the opera house had to be closed down in 1738 .
Telemann declined the position , but only after using the offer as leverage to secure a pay raise for his position in Hamburg .
In Hamburg Telemann started publishing his literary works : poems , texts for vocal music , sonnets , poems on the deaths of friends and colleagues .
She outlived her husband by some eight years and died in 1775 at a convent in Frankfurt .
In late September or early October 1737 Telemann took an extended leave from Hamburg and went to Paris .
Telemann returned to Hamburg by the end of May 1738 .
Around 1740 his musical output fell sharply , even though he continued fulfilling his duties as Hamburg music director .
He also took up gardening and cultivating rare plants , a popular Hamburg hobby which was shared by Handel .
In his later years , Telemann 's eyesight began to deteriorate , and he was increasingly troubled by health problems .
He was succeeded at his Hamburg post by Carl Philipp Emmanuel Bach .
Telemann was the most prolific composer of his time : his oeuvre comprises more than 3000 pieces .
During his lifetime and the latter half of the 18th century Telemann was very highly regarded by colleagues and critics alike .
Numerous theorists ( Marpurg , Mattheson , Quantz , and Scheibe , among others ) cited his works as models , and major composers such as J.S. Bach and Handel bought and studied his published works .
After the Bach revival , Telemann 's works were judged as inferior to Bach 's and lacking in religious fervour .
Particularly striking examples of such unfair judgements were produced by noted Bach biographers Philipp Spitta and Albert Schweitzer , who criticized Telemann 's cantatas and then praised works they thought were composed by Bach -- but which were , in fact , composed by Telemann , as was shown by later research .
The last performance of a substantial work by Telemann occurred in 1832 , and it was not until the 20th century that his music started being performed again .
Today each of Telemann 's works is usually given a TWV number .
TWV stands for Telemann-Werke-Verzeichnis .
Equally important for the history of music were Telemann 's publishing activities .
The same attitude informed his public concerts , where Telemann would frequently perform music originally composed for ceremonies attended only by a select few members of the upper class .
