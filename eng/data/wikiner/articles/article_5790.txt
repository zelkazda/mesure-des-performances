Henry won the throne when he defeated Richard III at the Battle of Bosworth Field .
He founded a long-lasting dynasty and was peaceably succeeded by his son , Henry VIII , after a reign of 23 years .
Although Henry can be credited with the restoration of political stability in England , and a number of commendable administrative , economic and diplomatic initiatives , the latter part of his reign was characterised by a financial rapacity which stretched the bounds of legality .
According to the contemporary historian Polydore Vergil , simple " greed " in large part underscored the means by which royal control was over-asserted in Henry 's final years .
Henry VII was born at Pembroke Castle in the west of Wales on 28 January 1457 .
One of their sons was Edmund Tudor , father of Henry VII .
Katherine was Gaunt 's mistress for around 25 years ; when they married in 1396 , they already had four children , including Henry 's great-grandfather John Beaufort .
Thus Henry 's claim was somewhat tenuous : it was from a woman , and by illegitimate descent .
Henry also made some political capital out of his Welsh ancestry ; for instance , in attracting military support and safeguarding his army 's passage through Wales on its way to the Battle of Bosworth .
He came from an old-established Anglesey family which claimed descent from Cadwaladr and on occasion , Henry displayed the red dragon of Cadwaladr .
He took it , as well as the standard of St George , on his procession through London after victory at Bosworth .
A contemporary writer and Henry 's biographer , Bernard André , also made much of Henry 's Welsh descent .
In reality , however , his hereditary connections to Welsh aristocracy were not strong .
When Edward IV became King in 1461 , Jasper Tudor went into exile abroad .
When the Yorkist Edward IV regained the throne in 1471 , Henry fled with other Lancastrians to Brittany , where he spent most of the next 14 years .
By 1483 , his mother , despite being married to a Yorkist ( Lord Stanley ) , was actively promoting Henry as an alternative to Richard III .
Henry then received the homage of his supporters .
Richard III attempted to extradite Henry from Brittany , but Henry escaped to France .
Richard only needed to avoid being killed in order to keep his throne .
Richard III 's death at Bosworth Field effectively ended the Wars of the Roses , although it was not the last battle Henry had to fight .
The first concern Henry had was to secure his hold on the throne .
His claim to the throne was that he was the last reasonably legitimate male descendant of Edward III .
The marriage took place on 18 January 1486 at Westminster .
In addition , Henry had Parliament repeal Titulus Regius , the statute that declared Edward IV 's marriage as invalid and his children illegitimate , thus legitimizing his wife .
Henry 's second action was to declare himself king retroactively from the day before Bosworth Field .
This meant that anyone who had fought for Richard against him would be guilty of treason .
Thus , Henry could legally confiscate the lands and property of Richard III while restoring his own .
Henry secured his crown principally by dividing and undermining the power of the nobility , especially through the aggressive use of bonds and recognisances to secure loyalty .
Henry was threatened by several rebellions in the next few years .
The first was the Stafford and Lovell Rebellion of 1486 , which collapsed without fighting .
Henry made the boy Simnel a servant in the royal kitchen .
In 1497 Warbeck landed in Cornwall with a few thousand troops , but was soon captured and executed .
However , a level of paranoia [ citation needed ] continued , so much that anyone with blood ties to the Plantagenets was suspected of coveting the throne .
It is generally accepted that Henry VII was a fiscally prudent monarch who restored the fortunes of an effectively bankrupt exchequer [ citation needed ] by introducing ruthlessly efficient mechanisms of taxation ( though many of his policies can be seen to have been built on foundations laid by Richard III in his brief reign ) .
Henry VII 's policy was both to maintain peace and to create economic prosperity .
However , this treaty came at a slight price , as Henry mounted a minor invasion of Brittany in November 1492 .
This act of war was a bluff by Henry , as he had no intention of fighting over the winter .
By means of this marriage , Henry VII hoped to break the Auld Alliance between Scotland and France .
Henry 's most successful economic-related diplomatic achievement was the Magnus Intercursus ( " great agreeement " ) of 1496 .
The stand-off eventually paid off for Henry .
Philip died shortly after the negotiations .
Henry 's principal problem was to restore royal authority in a realm recovering from the Wars of the Roses .
In 1502 , Henry VII 's heir , Arthur , died suddenly at Ludlow Castle .
Henry made half-hearted plans to remarry and beget more heirs , but these never came to anything .
He was buried at Westminster Abbey .
Upon his succession as king , Henry became entitled to bear the arms of his kingdom .
Henry and Elizabeth 's children were :
