Andrew II the Jerosolimitan ( c. 1177 -- 21 September 1235 ) was King of Hungary ( 1205 -- 1235 ) .
However , the boyars of Halych rebelled against his rule and expelled the Hungarian troops .
When his brother and his infant son died , Andrew ascended the throne and started to grant royal domains to his partisans .
He participated in the Fifth Crusade but he could not achieve any major military success .
As younger son , Andrew had no hope to inherite the Kingdom of Hungary from his father who wanted to ensure the inheritance of his elder son , Emeric and had him crowned already in 1182 .
The child Andrew 's rule in Halych must have been only nominal ; he did not even visit his principality .
Although , the young prince 's troops could get the mastery in 1189 when the boyars of Halych rose against his rule , but shortly afterwards Prince Vladimir II Yaroslavich managed to escape from his captivity and he expelled the Hungarian troops from Halych .
However , Andrew used the money to recruit followers among the barons and also sought the assistance of Leopold V , Duke of Austria .
In the beginning of 1198 , Pope Innocent III requested that Andrew fulfill his father 's last wishes and lead a Crusade to the Holy Land .
However , instead of a Crusade , Andrew led a campaign against the neighbouring provinces and occupied Zahumlje and Rama .
The king had his brother arrested , but Andrew managed to escape shortly afterwards .
Nevertheless , the king whose health was failing , wanted to secure the ascension of his young son , Ladislaus , who had been crowned on 26 August 1204 .
Shortly afterwards , the king reconciled with Andrew whom he appointed to govern the kingdom during his son 's minority .
Andrew made preparations for a war against Austria , but the child king died on 7 May 1205 , thus Andrew inherited the throne .
Andrew made a radical alteration in the internal policy followed by his predecessors and he began to bestow the royal estates to his partisans .
During the first years of his reign , Andrew was occupied with the discords within the Principality of Halych .
In 1205 , he led his armies to the principality to ensure the rule of the child Prince Danylo .
In 1211 , he granted the Burzenland to the Teutonic Knights in order to ensure the security of the southeastern borders of his kingdom against the Cumans .
However , the Teutonic Knights began to establish a country independent of the King of Hungary .
In 1211 , Andrew provided military assistance to Prince Danylo to reoccupy Halych .
Shortly afterwards , Prince Danylo , was obliged to leave his country and he sought again Andrew 's assistance .
Andrew left for his campaign in the summer 1213 when he was informed that a group of conspirators had murdered his queen on 28 September and he had to return .
Following his return , he ordered the execution only the leader of the conspirators and he forgave the other members of the group , which resulted in the emerging antipaty of his son , Béla .
Nevertheless , in 1214 , Andrew had his son crowned .
Their allied troops occupied the neighbouring principality which was granted to Andrew 's younger son , Coloman .
In the meantime , Andrew began to deal with the problems of the southern borders of his kingdom .
In February 1215 , Andrew married Yolanda , the niece of Henry I , the Emperor of Constantinople .
Andrew and his troops embarked on 23 August 1217 in Spalato ( Split ) .
Before his departure from the city of Split , he had made over to the Templars the Castle of Klis , a strategic point in the hinterland of Split which controlled the approaches to the town .
They landed on 9 October on Cyprus from where they sailed to Acre .
The catapults and trebuchets did n't arrive on time , so he had fruitless assaults on the fortresses of the Lebanon and on Mount Tabor .
Afterwards , Andrew spent his time collecting alleged relics .
Andrew set home on ( 18 January 1218 ) .
When he was staying in Nicaea , his cousins , who had been living there , made an unsuccessful attempt to take his life .
In 1220 , Andrew entrusted the government of Slavonia , Dalmatia and Croatia to his son , Béla .
Andrew also enforced Béla to separate from his wife .
Finally , Andrew made an agreement with his son with the mediation of Pope Honorius III and the junior king took over again the government of Slavonia , Dalmatia and Croatia .
In the same year , Andrew expelled the Teutonic Knights from Transylvania because they had ignored his overlordship .
Andrew opposed his son 's policy and he entrusted Béla with the government of Transylvania while his younger son , Coloman became the governor of Béla 's former provinces .
In the second half of 1226 , Andrew lead his armies to Halych on the request of his youngest son , Andrew .
During 1228 , Andrew 's two sons started again to take back the former royal domains in their provinces , and they persuaded Andrew to confiscate the estates of the barons who had taken part in the conspiracy against their mother .
Therefore , Pope Gregory IX requested him to give up this practise .
In the second half of 1231 , Andrew lead his armies to Halych and managed to ensure his youngest son 's rule in the principality .
Nevertheless , upon Andrew 's request , the Archbishop withdrew the ecclesiastic punishments soon and the Pope promised that the dignitaries of the King of Hungary would never be excommunicated without his special authorization .
On 14 May 1234 , Andrew , who had lost his second wife in the previous year , married Beatrice D'Este who was thirty years younger than himself .
Andrew appealed to the Pope against the bishop 's measure .
In the autumn of 1234 , Prince Danylo laid siege to the capital of Andrew 's youngest son who died during the siege .
Thus , the Hungarian supremacy over Halych disappeared .
In the beginning of 1235 , Andrew made a campaign against Austria and enforced Duke Frederick II to make a peace .
He was still alive when one of his daughters , Elisabeth , who had died some years before , was canonized on 28 May 1235 .
Before his death , he was absolved from the excommunication ; moreover , the Pope also promised that the King of Hungary and his relatives would not be excommunicated without the special permission of the Pope .
