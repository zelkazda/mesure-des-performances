Flavius Honorius ( 9 September 384 -- 15 August 423 AD ) was Roman Emperor ( 393 -- 395 ) and then Western Roman Emperor from 395 until his death .
Even by the standards of the rapidly declining Western Empire , Honorius ' reign was precarious and chaotic .
Stilicho 's generalship helped preserve some level of stability , but with his execution , the Western Roman Empire moved closer to collapse .
To strengthen his bonds with the young emperor , Stilicho married his daughter Maria to him .
The epithalamion written for the occasion by Stilicho 's court poet Claudian survives .
To counter Priscus , Honorius tried to negotiate with Alaric .
Alaric withdrew his support for Priscus in 410 , but the negotiations with Honorius broke down .
Alaric again entered Italy and sacked Rome .
The revolt of Constantine III in the west continued through this period .
In 409 , Gerontius , Constantine III 's general in Hispania , rebelled against him , proclaimed Maximus Emperor , and besieged Constantine at Arles .
Honorius now found himself an able commander , Constantius , who defeated Maximus and Gerontius , and then Constantine , in 411 .
In 417 , Constantius married Honorius ' sister , Galla Placidia .
In 421 , Honorius recognized him as co-emperor Constantius III , but he died early in 422 .
In 420-422 , another Maximus ( or perhaps the same ) gained and lost power in Hispania .
Honorius died of dropsy in 423 , leaving no heir .
In the subsequent interregnum Joannes was nominated emperor .
The following year , however , the Eastern Emperor Theodosius II elected his cousin Valentinian III , son of Galla Placidia and Constantius III , as emperor .
Unfortunately , this course of action appeared to be the product of Honorius ' indecisive character and he suffered much criticism for it both from contemporaries and later historians .
After listing the disasters of those 28 years , Bury concludes that Honorius " himself did nothing of note against the enemies who infested his realm , but personally he was extraordinarily fortunate in occupying the throne till he died a natural death and witnessing the destruction of the multitude of tyrants who rose up against him . "
The last known gladiatorial fight took place during the reign of Honorius .
