Elias Canetti was a Bulgarian-born novelist and non-fiction writer of Sephardi Jewish ancestry who wrote in German .
He won the Nobel Prize in Literature in 1981 .
His ancestors were Sephardi Jews who had been expelled from Spain in 1492 .
His paternal ancestors had settled in Ruse from Ottoman Adrianople .
In Ruse , Elias ' father and grandfather were successful merchants who operated out of a commercial building , which they had built in 1898 .
Before settling in Ruse , they had lived in Livorno in the 17th century .
Canetti spent his childhood years , from 1905 to 1911 , in Ruse until the family moved to England .
In 1912 his father died suddenly , and his mother moved with their children to Vienna in the same year .
They lived in Vienna from the time Canetti was aged seven onwards .
His mother insisted that he speak German , and taught it to him .
Subsequently the family moved first ( from 1916 to 1921 ) to Zürich and then ( until 1924 ) to Frankfurt , where Canetti graduated from high school .
Canetti went back to Vienna in 1924 in order to study chemistry .
However , his primary interests during his years in Vienna became philosophy and literature .
Introduced into the literary circles of first-republic -- Vienna , he started writing .
Politically leaning towards the left , he participated in the July Revolt of 1927 .
He gained a degree in chemistry from the University of Vienna in 1929 , but never worked as a chemist .
Canetti however remained open to relationships with other women .
His name has also been linked with that of the author Iris Murdoch .
Canetti 's wife died in 1963 .
For his last 20 years , Canetti mostly lived in Zürich .
In 1981 , Canetti won the Nobel Prize in Literature " for writings marked by a broad outlook , a wealth of ideas and artistic power " .
He is known chiefly for his novel Auto-da-Fé , and for Crowds and Power , a study of crowd behaviour as it manifests itself in human activities ranging from mob violence to religious congregations .
He died in Zürich .
