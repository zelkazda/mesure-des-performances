Although Basic English was not built into a program , similar simplifications have been devised for various international uses .
Most notably , Ogden allowed only 18 verbs , which he called " operators " .
Ogden 's word lists include only word roots , which in practice are extended with the defined set of affixes and the full set of forms allowed for any available word ( noun , pronoun , or the limited set of verbs ) .
However , Ogden prescribed that any student should learn an additional 150 word list for everyday work in some particular field , by adding a word list of 100 words particularly useful in a general field ( e.g. , science , verse , business , etc. ) , along with a 50-word list from a more specialised subset of that general field , to make a basic 1000 word vocabulary for everyday work and life .
Moreover , Ogden assumed that any student already should be familiar with ( and thus may only review ) a core subset of around 350 " international " words .
Ogden 's rules of grammar for Basic English help people use the 850 words to talk about things and events in a normal way .
In the novel The Shape of Things to Come , published in 1933 , H.G. Wells depicted Basic English as the lingua franca of a new elite which after a prolonged struggle succeeds in uniting the world and establishing a totalitarian world government .
In the future world of Wells ' vision , virtually all members of humanity know this language .
From 1942 to 1944 George Orwell was a proponent of Basic English , but in 1945 he became critical of universal languages .
In his story " Gulf " , science fiction writer Robert A. Heinlein used a constructed language , in which every Basic English word is replaced with a single phoneme , as an appropriate means of communication for a race of genius supermen .
