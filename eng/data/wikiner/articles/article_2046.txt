Colorado ( i i i / l ə ˈ r ɑː d oʊ / or i i i / l ə ˈ r æ d oʊ / ) is a state that encompasses most of the Southern Rocky Mountains as well as the northeastern portion of the Colorado Plateau and the western edge of the Great Plains .
Colorado is bordered on the north by Wyoming and Nebraska , on the east by Nebraska and Kansas , on the south by Oklahoma and New Mexico , and on the west by Utah .
The four states of Colorado , N.M. , Arizona , and Utah meet at one common point known as the Four Corners .
Colo. is noted for its vivid landscape of mountains , plains , mesas , and canyons .
The 30 highest major summits of the Rocky Mountains of North America all lie within the state .
Denver is the capital and the most populous city of Colo. .
Colorado , Wyoming and Utah are the only states which have boundaries defined solely by lines of latitude and longitude .
When placing the border markers for the Territory of Colorado , minor surveying errors resulted in several nearly imperceptible kinks , most notably along the border with the Territory of Utah .
The summit of Mount Elbert at 14440 feet ( 4401 m ) in elevation in Lake County is the highest point of Colo. and the Rocky Mountains .
The point where the Arikaree River flows out of Yuma County , Colorado , and into Cheyenne County , Kansas , is the lowest point in Colorado at 3317 feet ( 1011 m ) elevation .
Nearly one third of the area of Colo. is flat or rolling land -- in stark contrast to Colo. 's rugged Rocky Mountains .
The plains states of Kansas and Nebraska border Colorado to the east and northeast .
The Colorado plains are usually thought of as prairies , but actually they have patches of deciduous forests .
In eastern Colo. , a good deal of irrigation water is available from the South Platte , the Arkansas River , and a few other streams , and also from subterranean sources , including artesian wells .
Most of Colorado 's population resides along the eastern edge of the Rocky Mountains in the Front Range Urban Corridor between Cheyenne , Wyoming , and Pueblo , Colorado .
The only other significant population centers are at Grand Junction and Durango in far western Colorado .
To the west of Great Plains of Colo. rises the eastern slope of the Rocky Mountains .
This area drains to the east and the southeast , ultimately either via the Mississippi River or the Rio Grande into the Gulf of Mexico .
Within the interior of the Rocky Mountains are several large so-called " parks " or high broad basins .
The North Park is drained by the North Platte River , which flows north into Wyoming and Neb. .
The South Park of Colorado is the region of the headwaters of the South Platte River .
In southmost Colorado is the large San Luis Valley , where the headwaters of the Rio Grande are located .
The Rio Grande drains due south into New Mexico , Mexico , and Texas .
Across the Sangre de Cristo Range to the east of the San Luis Valley lies the Wet Mountain Valley .
These basins , particularly the San Luis Valley , lie along the Rio Grande Rift , a major geological formation of the Rocky Mountains , and its branches .
The Rocky Mountains within Colorado contain about 54 peaks that are 14000 feet ( 4267 m ) or higher in elevation above sea level , known as fourteeners .
Only small parts of the Colorado Rockies are snow-covered year round .
The Colorado Mineral Belt , stretching from the San Juan Mountains in the southwest to Boulder and Central City on the front range , contains most of the historic gold-and silver-mining districts of Colorado .
The Western Slope of Colorado is drained by the Colorado River and its tributaries ( primarily the Green River and the San Juan River ) , or by evaporation in its arid areas .
The city of Grand Junction , Colo. , is the largest city on the Western Slope , Grand Junction and Durango are the only major centers of radio and television broadcasting , newspapers , and higher education on the Western Slope .
Grand Junction is located along Interstate 70 , the only major highway of Western Colorado .
Grand Junction is also along the major railroad of the Western Slope , the Denver and Rio Grande Western Railroad , which also provides the tracks for AMTRAK 's California Zephyr passenger train , which crosses the Rocky Mountains between Denver and Grand Junction via a route on which there are no continuous highways .
To the southeast of Grand Junction is the Grand Mesa , said to be the world 's largest flat-topped mountain .
Other towns of the Western Slope include Glenwood Springs with its resort hot springs , and the ski resorts of Aspen , Vail , Crested Butte , Steamboat Springs , and Telluride .
The northwestern corner of Colorado is a sparsely-populated region , and it contains part of the noted Dinosaur National Monument , which is not only a paleontological area , but is also a scenic area of high , rocky hills , canyons , and streambeads .
Here , the Green River briefly crosses over into Colo. .
The famous Pikes Peak is located just west of Colorado Springs .
Its isolated peak is visible from nearly the Kansas border on clear days , and also far to the north and the south .
A main climatic division in Colorado occurs between the Rocky Mountains on the west and the plains on the east with the foothills forming a transitional zone between the two .
West of the plains and foothills , the weather of Colorado is much less uniform .
Generally , the wettest season in western Colorado is winter while June is the driest month .
The San Luis Valley is generally dry with little rain or snow , although the snow that falls tends to stay on the ground all winter .
Extreme weather is a common occurrence in Colorado .
Much of Colorado is a relatively dry state averaging only 17 inches ( 430 mm ) of rain per year statewide and rarely experiences a time when some portion of the state is not in some degree of drought .
The lack of precipitation contributes to the severity of wildfires in the state such as the Hayman Fire , one of the largest wildfires in American history .
However , there are some of the mountainous regions of Colorado which receive a huge amount of moisture via winter snowfalls .
The spring melts of these snows often cause great waterflows in such rivers as the Yampa River , the Grand River , the Colorado River , the Rio Grande , the Arkansas River , Cherry Creek , the North Platte River , and the South Platte River .
The eastern edge of the Rocky Mountains was a major migration route that was important to the spread of early peoples throughout the Americas .
The Arapaho Nation and the Cheyenne Nation moved west to hunt across the High Plains .
The remainder of the Missouri Territory , including what would become northeastern Colo. , became unorganized territory , and would remain so for 33 years over the question of slavery .
The Texian Revolt of 1835 -- 1836 fomented a dispute between the United States and Mexico which eventually erupted into the Mexican-American War in 1846 .
Mexico surrendered its northern territory to the United States with the Treaty of Guadalupe Hidalgo at the conclusion of the war in 1848 .
The original boundaries of Colo. remain unchanged today .
The name Colorado was chosen because it was commonly believed that the Colorado River originated in the territory .
The Colorado River was thought to originate at the confluence of its two major tributaries , the Green River and the Grand River .
In 1869 , this confluence was located by an expedition led by John Wesley Powell in what is now Canyonlands National Park , Utah .
On April 12 , 1861 , South Carolina artillery opened fire on Fort Sumter to start the American Civil War .
In 1862 , a force of Tex. cavalry invaded the Territory of New Mexico and captured Santa Fe on March 10 .
The Confederacy made no further attempts to seize the Southwestern United States .
Chivington reported that his troops killed more than 500 warriors .
Three U.S. Army inquiries condemned the action , and incoming President Andrew Johnson asked Governor Evans for his resignation , but none of the perpetrators was ever punished .
The Denver Pacific Railway reached Denver in June of the following year , and the Kansas Pacific arrived two months later to forge the second line across the continent .
The discovery of a major silver lode near Leadville in 1878 , triggered the Colorado Silver Boom .
The Sherman Silver Purchase Act of 1890 invigorated silver mining , and Colo. 's last , but greatest , gold strike at Cripple Creek a few months later lured a new generation of gold seekers .
Colo. women were granted the right to vote beginning on November 7 , 1893 , making Colorado the second state to grant universal suffrage and the first one by a popular vote ( of Colorado men ) .
The United States Census Bureau estimated that the population of Colorado exceeded five million in 2009 .
The first USS Colorado was named for the Colorado River .
The later two ships were named in honor of the state , including the battleship USS Colorado which served in World War II in the Pacific beginning in 1941 .
At the time of the Attack on Pearl Harbor , this USS Colorado was located at the naval base in San Diego , Calif. and hence went unscathed .
Colorado 's most populous city , and capital , is Denver .
The Denver-Aurora-Boulder Combined Statistical Area with an estimated 2009 population of 3,110,436 , is home to 61.90 % of the state 's residents .
As of 2005 , Colo. has an estimated population of 4,665,177 , which is an increase of 63,356 , or 1.4 % , from the prior year and an increase of 363,162 , or 8.4 % , since the year 2000 .
Immigration from outside the United States resulted in a net increase of 112,217 people , and migration within the country produced a net increase of 47,740 people .
The largest increases are expected in the Front Range Urban Corridor , especially in the Denver metropolitan area .
The state 's fastest-growing counties are Douglas and Weld .
The Denver metropolitan area is considered more liberal and diverse than much of the state when it comes to political issues and environmental concerns .
There were a total of 70,331 births in Colorado in 2006 .
Major religious affiliations of the people of Colo. are :
At 25 % , Colo. also has an above average proportion of citizens who claim no religion .
The U.S. average is 17 % .
Colorado also has a reputation for being a state of active and athletic people .
Colorado Governor Bill Ritter commented : " As an avid fisherman and bike rider , I know first-hand that Colorado provides a great environment for active , healthy lifestyles, " although he highlighted the need for continued education and support to slow the growth of obesity in the state .
The Bureau of Economic Analysis estimates that the total state product in 2008 was $ 248.6 billion .
Per capita personal income in 2007 was $ 41,192 , ranking Colo. eleventh in the nation .
Other industries include food processing , transportation equipment , machinery , chemical products , the extraction of metals such as gold ( see Gold mining in Colorado ) , silver , and molybdenum .
Colorado now also has the largest annual production of beer of any state .
Denver is an important financial center .
A number of nationally known brand names have originated in Colorado factories and laboratories .
From Golden came Coors beer in 1873 , CoorsTek industrial ceramics in 1920 , and Jolly Rancher candy in 1949 .
CF & I railroad rails , wire , nails and pipe debuted in Pueblo in 1892 .
Estes model rockets were launched in Penrose in 1958 .
Celestial Seasonings herbal teas have been made in Boulder since 1969 .
Rocky Mountain Chocolate Factory made its first candy in Durango in 1981 .
Colo. has a flat 4.63 % income tax , regardless of income level .
Unlike most states , which calculate taxes based on federal adjusted gross income , Colorado taxes are based on taxable income -- income after federal exemptions and federal itemized ( or standard ) deductions .
Colorado 's state sales tax is 2.9 % on retail sales .
When state revenues exceed state constitutional limits , full-year Colorado residents can claim a sales tax refund on their individual state income tax return .
Real estate and personal business property are taxable in Colorado .
The state 's senior property tax exemption was temporarily suspended by the Colorado Legislature in 2003 .
Colorado ranks thirty-eighth in the country in the percent of income donated to charity , 3.4 percent , as opposed to the US average of 3.6 percent .
[ citation needed ] Southwest Colorado gives the highest percentage , slightly more than 4 percent , topping the national average .
Colorado has significant hydrocarbon resources .
Conventional and unconventional natural gas output from several Colo. basins typically account for more than 5 percent of annual U.S. natural gas production .
Colorado 's oil shale deposits hold an estimated 1 trillion barrels ( 160 km 3 ) of oil -- nearly as much oil as the entire world 's proven oil reserves ; the economic viability of the oil shale , however , has not been demonstrated .
Kimberlite volcanic pipes have been found in Colorado ; the Kelsey Lake Diamond Mine operated for several years , recovering gem quality diamonds .
Colo. 's high Rocky Mountain ridges and eastern plains offer wind power potential , and geologic activity in the mountain areas provides potential for geothermal power development .
Major rivers flowing from the Rocky Mountains offer hydroelectric power resources .
Just like all the states , Colorado 's state constitution provides for three branches of government : the legislative , the executive , and the judicial branches .
The Colorado Supreme Court is the highest judicial court in the state .
The state legislative body is the Colorado General Assembly , which is made up of two houses , the House of Representatives and the Senate .
The 2005 Colorado General Assembly was the first to be controlled by the Democrats in forty years .
The incumbent governor is Democrat August William " Bill " Ritter , Jr. .
Colorado is considered a swing state in both state and federal elections .
In presidential politics , Colorado supported Democrats Bill Clinton in 1992 and Barack Obama in 2008 , and supported Republicans Robert J. Dole in 1996 and George W. Bush in 2000 and 2004 .
The presidential outcome in 2008 was the second closest to the national popular vote , after Virginia .
Colorado politics has the contrast of conservative cities such as Colorado Springs and liberal cities such as Boulder .
Former Colo. senator and attorney general Ken Salazar is the current United States Secretary of the Interior ( as of January 20 , 2009 ) .
Colorado is divided into 64 counties , including two counties with consolidated city and county governments .
Colleges and universities in Colorado :
