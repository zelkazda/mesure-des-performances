Pulitzer Prize winning historian James McPherson , writing for the American Historical Association , described the importance of revisionism :
However , the historian and philosopher of science , Thomas Kuhn , pointed out that in contrast to the sciences , in which there tends to be ( except in times of paradigm shift ) a single reigning paradigm , the social sciences are characterized by a " tradition of claims , counterclaims , and debates over fundamentals . "
Historian C. Vann Woodward sees this as a positive influence .
Speaking of the changes that occurred after the end of World War II , he wrote :
For example , philosopher Karl Popper echoed Woodward 's sentiments regarding revisionism when he noted that " each generation has its own troubles and problems , and therefore its own interests and its own point of view " and :
Revisionist scholars led by historian Elizabeth A. R. Brown have rejected the term .
The military leadership of the British Army during the First World War was frequently condemned as poor by historians and politicians for decades after the war ended .
However , during the 1960s historians such as John Terraine began to challenge this interpretation .
Furthermore , military leadership improved throughout the war culminating in the Hundred Days Offensive advance to victory in 1918 .
In reaction to the orthodox interpretation enshrined in the Versailles Treaty , the self-described " revisionist " historians of the 1920s rejected the orthodox view and presented a complex causation in which several other countries were equally guilty .
