Sony first publicly demonstrated an optical digital audio disc in September 1976 .
Later in 1979 , Sony and Philips Consumer Electronics ( Philips ) set up a joint task force of engineers to design a new digital audio disc .
Led by Kees Schouhamer Immink and Toshitada Doi , the research pushed forward laser and optical disc technology that began independently by Philips and Sony in 1977 and 1975 , respectively .
Philips contributed the general manufacturing process , based on video Laserdisc technology .
Philips also contributed eight-to-fourteen modulation ( EFM ) , which offers both a long playing time and a high resilience against disc defects such as scratches and fingerprints , while Sony contributed the error-correction method , CIRC .
The first public demonstration was on the BBC television program Tomorrow 's World when The Bee Gees ' album Living Eyes ( 1981 ) was played .
In August 1982 the real pressing was ready to begin in the new factory , not far from the place where Emil Berliner had produced his first gramophone record 93 years earlier .
The first CD to be manufactured at the new factory was The Visitors ( 1981 ) by ABBA .
The document is known colloquially as the " Red Book " after the color of its cover .
Four-channel sound is an allowable option within the Red Book format , but has never been implemented .
The device that turns an analog audio signal into PCM audio , which in turn is changed into an analog video signal is called a PCM adaptor .
There was a long debate over whether to use 14-bit ( Philips ) or 16-bit ( Sony ) quantization , and 44,056 or 44,100 samples/s ( Sony ) or around 44,000 samples/s ( Philips ) .
Philips found a way to produce 16-bit quality using its 14-bit DAC by using four times oversampling .
The partners aimed at a playing time of 60 minutes with a disc diameter of 100 mm ( Sony ) or 115 mm ( Philips ) .
According to a Sunday Tribune interview , the story is slightly more involved .
In 1979 , Philips owned Polygram , one of the world 's largest distributors of music .
Sony did not yet have such a facility .
If Sony had agreed on the 115-mm disc , Philips would have had a significant competitive edge in the market .
Sony decided that something had to be done .
The main parameters of the CD ( taken from the September 1983 issue of the Red Book ) are as follows :
( Information Society 's Hack was one of very few CD releases to do this , following a release with an equally obscure CD+G feature . )
Super Audio CD ( SACD ) is a high-resolution read-only optical audio disc format that provides much higher fidelity digital audio reproduction than the Red Book .
Introduced in 1999 , it was developed by Sony and Philips , the same companies that created the Red Book .
Hence , unlike Red Book , these recordings are not audio .
Video CD is a standard digital format for storing video media on a CD .
SVCD was intended as a successor to VCD and an alternative to DVD-Video , and falls somewhere between both in terms of technical capability and picture quality .
SVCD has two-thirds the resolution of DVD , and over 2.7 times the resolution of VCD .
While no specific limit on SVCD video length is mandated by the specification , one must lower the video bit rate , and therefore quality , in order to accommodate very long videos .
It is usually difficult to fit much more than 100 minutes of video onto one SVCD without incurring significant quality loss , and many hardware players are unable to play video with an instantaneous bit rate lower than 300 to 600 kilobits per second .
The images can also be printed out on photographic paper with a special Kodak machine .
This format is unusual because it hides the initial tracks which contains the software and data files used by CD-i players by omitting the tracks from the disc 's TOC ( table of contents ) .
This causes audio CD players to skip the CD-i data tracks .
CDi was the leading format of its time but was supplanted by the politics of competition .
The Red Book audio specification , except for a simple ' anti-copy ' bit in the subcode , does not include any copy protection mechanism .
Philips has stated that such discs are not permitted to bear the trademarked Compact Disc Digital Audio logo because they violate the Red Book specifications .
