Brainfuck programs can be translated into C using the following substitutions , assuming ptr is of type unsigned char* and has been initialized to point to an array of zeroed bytes :
Although brainfuck programs , especially complicated ones , are difficult to write , it is quite trivial to write an interpreter for brainfuck in a more typical language such as C due to its simplicity .
In fact , using six symbols equivalent to the respective brainfuck commands + , -- , < , > , [ , ] , Böhm provided an explicit program for each of the basic functions that together serve to compute any computable function .
This assumption is also consistent with most of the world 's sample code for C and other languages , in that they use ' \n ' , or 10 , for their newlines .
On systems that use CRLF line endings , the C standard library transparently remaps " \n " to " \r\n " on output and " \r\n " to " \n " on input for streams not opened in binary mode .
Some implementations set the cell at the pointer to 0 , some set it to the C constant EOF ( in practice this is usually -1 ) , some leave the cell 's value unchanged .
However , it is not obvious that those C translations are to be taken as normative .
