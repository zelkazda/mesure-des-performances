Whitney 's invention made short staple cotton into a profitable crop , which strengthened the economic foundation of slavery .
Despite the social and economic impact of his invention , Whitney lost his profits in legal battles over patent infringement , closed his business and nearly filed for bankruptcy .
At age 14 he operated a profitable nail manufacturing operation in his father 's workshop during the Revolutionary War .
Because his stepmother opposed his wish to attend college , Whitney worked as a farm laborer and schoolteacher to save money .
Instead of reaching his destination , he was convinced to visit Georgia .
In the closing years of the 18th century , Ga. was a magnet for New Englanders seeking their fortunes .
Though Whitney is popularly credited with the invention of a musket that could be manufactured with interchangeable parts , the idea predated him .
By the late 1790s , Whitney was on the verge of bankruptcy and the cotton gin litigation had left him deeply in debt .
His New Haven cotton gin factory had burned to the ground , and litigation sapped his remaining resources .
The War Department issued contracts for the manufacture of 10,000 muskets .
Whitney , who had never made a gun in his life , obtained a contract in January 1798 to deliver 10,000 -- 15,000 muskets in 1800 .
After spending most of 1799-1801 in cotton gin litigation , Whitney began promoting the idea of interchangeable parts , and even arranged a public demonstration of the concept in order to gain time .
When the government complained that Whitney 's price per musket compared unfavorably with those produced in government armories , Whitney was able to calculate an actual price per musket by including fixed costs such as insurance and machinery , which the government had not included .
Whitney occasionally told a story where he was pondering an improved method of seeding the cotton and he was inspired by observing a cat attempting to pull a chicken through a fence , and could only pull through some of the feathers .
Whitney received a patent for his cotton gin on March 14 , 1794 ; however , it was not validated until 1807 .
One oft-overlooked point is that there were drawbacks to Whitney 's first design .
While the cotton gin did not earn Whitney the fortune he had hoped for , it did give him fame .
Subsequent work by other historians suggests that Whitney was among a group of contemporaries all developing milling machines at about the same time ( 1814 to 1818 ) .
Despite his humble origins , Whitney was keenly aware of the value of social and political connections .
In building his arms business , he took full advantage of the access that his status as a Yale alumnus gave him to other well-placed graduates , such as Secretary of War Oliver Wolcott ( Class of 1778 ) and New Haven developer and political leader James Hillhouse .
Whitney died at age 59 of prostate cancer on January 8 , 1825 , in New Haven , Conn. , leaving a widow and four children .
Eli Whitney Blake ( 1820-1894 ) assumed control of the armory in 1841 .
While this enterprise addressed the city 's need for water , it also enabled Whitney to increase the amount of power available for his manufacturing operations at the expense of the water company 's stockholders .
A new dam made it possible to consolidate his operations -- originally located in three sites along the Mill River -- in a single plant .
He served as president of the water company until his death and was a major New Haven business and civic leader .
In the 1970s , as part of the Bicentennial celebration , interested citizens organized the Eli Whitney Museum , which opened to the public in 1984 .
The site today includes the boarding house and barn that served Eli Whitney 's original workers and a stone storage building from the original armory .
Eli Whitney and his descendants are buried in New Haven 's historic Grove Street Cemetery .
