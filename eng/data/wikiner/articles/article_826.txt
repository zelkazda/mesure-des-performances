Ann Noreen Widdecombe ( born 4 October 1947 ) is a British Conservative Party politician and , since 2000 , television presenter and novelist .
She is a member of the Conservative Christian Fellowship and a supporter of traditional family values .
She is also a convert to Catholicism .
From 1976 to 1978 , Widdecombe was a Runnymede District councillor .
Widdecombe holds some conservative views , including opposition to abortion ; it was understood during her time in frontline politics that she would not become health secretary as long as this involved overseeing abortions .
Along with John Gummer MP , she converted from the Church of England to the Roman Catholic Church following the decision of the Church of England that women could become priests .
She called for a zero tolerance policy of prosecution , albeit with only £ 100 fines as the punishment , for users of cannabis in her speech at the 2000 Conservative conference , which was well-received by rank-and-file Conservative delegates .
Out of the 17 parliamentary votes considered by the Public Whip website to concern equal rights for homosexuals , Widdecombe has taken the opposing position in 15 cases , not being present at the other two votes .
She is a committed animal lover and one of the few Conservative MPs to have consistently voted for the ban on fox hunting .
In 2007 , she wrote that she did not want to belittle the issue but was sceptical of the claims that specific actions would prevent catastrophe , then in 2008 that her doubts had been " crystalised " by the book An Appeal to Reason , before giving a statement in 2009 that " There is no climate change , has n't anybody looked out of their window recently ?
She first supported Michael Ancram , who was eliminated in the first round , and then Kenneth Clarke , who lost in the final round .
She afterwards declined to serve in an Iain Duncan Smith shadow cabinet ( although she indicated on camera during her time with Louis Theroux prior to the leadership contest that she wished to retire to the backbenches anyway ) .
In the 2005 leadership election , she initially supported Kenneth Clarke again .
Once he was eliminated , she turned support towards Liam Fox .
Following Fox 's subsequent elimination , she took time to reflect before finally declaring for David Davis .
Widdecombe is one of the 98 MPs who voted to keep their expense details secret .
When the expenses claims were leaked , Widdecombe was described by the The Daily Telegraph as one of the " saints " amongst all MPs .
Widdecombe currently lives in London .
She had a constituency home in the village of Sutton Valence , Kent , which she sold upon deciding to retire at the next general election .
In November 2007 on BBC Radio 4 she described how a journalist once produced a profile on her with the assumption that she had had at least " one sexual relationship " , to which Widdecombe replied : " Be careful , that 's the way you get sued . "
Widdecombe is a practising Roman Catholic , a faith to which she converted in 1993 after leaving the Church of England in protest at the ordination of female priests .
In 1997 , during the Conservative leadership election that picked William Hague , Widdecombe spoke out against Michael Howard , under whom she had served when he was Home Secretary .
It was considered to be extremely damaging , and Howard was frequently portrayed as a vampire in satire from that time on , and came last in the poll .
However , he went on to become party leader in 2003 , and Ann Widdecombe said " I explained fully what my objections were in 1997 and I do not retract anything I said then .
In 2002 , she took part in the ITV programme Celebrity Fit Club .
Also in 2002 she took part in a Louis Theroux documentary , depicting her life in and out of politics .
In March 2004 she briefly became The Guardian newspaper 's agony aunt , introduced with an Emma Brockes interview .
In 2005 , she appeared in a new series of Celebrity Fit Club , but this time as a panel member dispensing wisdom and advice to the celebrities taking part .
She was the guest host of news quiz Have I Got News for You twice , in 2006 and 2007 .
Following her second appearance , Widdecombe vowed she would never appear on the show again because of comments made by panellist Jimmy Carr .
She did , however , stand by her appraisal of regular panellists Ian Hislop and Paul Merton , whom she has called " the fastest wits in showbusiness " .
In 2006 , she launched a boycott against British Airways for suspending a worker who refused to hide her cross which ended when British Airways reversed their suspension .
She awarded the 2007 University Challenge trophy .
A fourth episode was screened on 18 September 2008 in which she travelled around London and Birmingham talking to girl gangs .
Ann Widdecombe has made appearances on television and radio , and presented the Lent Talks on BBC Radio 4 on 12 March 2008 .
Ann Widdecombe is scheduled to be President of the North of England Education Conference taking place in January 2011 in Blackpool .
