IGAD 's mandate is for regional cooperation and economic integration .
Both countries share ownership of the Addis Ababa-Djibouti Railroad ; however , this utility is in need of repairs and upgraded capacity .
The railroad is tied to the Port of Djibouti , which provides port facilities and trade ties to landlocked Ethiopia .
Djibouti 's military and economic agreements with France provide continued security and economic assistance .
The first U.S. Ambassador to the Republic of Djibouti arrived in October 1980 .
Over the past decade , the United States has been a principal provider of humanitarian assistance for famine relief , and has sponsored health care , education , good governance , and security assistance programs .
U.S. service members provide humanitarian support and development and security assistance to people and governments of the Horn of Africa and Yemen .
As a victim of past international terrorist attacks , President Guelleh continues to take a very proactive position against terrorism .
This article incorporates public domain material from websites or documents of the United States Department of State .
