Al-Qaeda has attacked civilian and military targets in various countries , most notably the September 11 attacks on New York City and Washington , D.C. in 2001 .
Activities ascribed to it may involve members of the movement , who have taken a pledge of loyalty to Osama bin Laden , or the much more numerous " al-Qaeda-linked " individuals who have undergone training in one of its camps in Afghanistan , Iraq or Sudan , but not taken any pledge .
Al-Qaeda 's management philosophy has been described as " centralization of decision and decentralization of execution . "
Following the War on Terrorism , it is thought that al-Qaeda 's leadership has " become geographically isolated " , leading to the " emergence of decentralized leadership " of regional groups using the al-Qaeda " brand name " .
Many terrorism experts do not believe that the global jihadist movement is driven at every level by Osama bin Laden and his followers .
Although Osama bin Laden still has huge ideological sway over some Muslim extremists , experts argue that al-Qaeda has fragmented over the years into a variety of disconnected regional movements that have little connection with each other .
Marc Sageman , a psychiatrist and former CIA officer , said that Al-Qaeda would now just be a " loose label for a movement that seems to target the west " .
We like to create a mythical entity called [ al-Qaeda ] in our minds but that is not the reality we are dealing with . "
Bruce Hoffman , a terrorism expert at Georgetown University , said " It amazes me that people do n't think there is a clear adversary out there , and that our adversary does not have a strategic approach . "
Al Qaeda has the following direct franchises :
Abdel Bari Atwan summarizes this strategy as comprising five stages :
In Arabic , al-Qaeda has four syllables ( Arabic pronunciation : [ ælˈqɑːʕɪdɐ ] ) .
[ citation needed ] Al-Qaeda 's name can also be transliterated as al-Qaida , al-Qa'ida , el-Qaida , or al Qaeda .
The name comes from the Arabic noun qā'idah , which means foundation or basis and can also refer to a military base .
The initial al-is the Arabic definite article the , hence the base .
It has been argued that two documents seized from the Sarajevo office of the Benevolence International Foundation prove that the name was not simply adopted by the mujahid movement and that a group called al-Qaeda was established in August 1988 .
Some have argued that " without the writings " of Islamic author and thinker Sayyid Qutb " al-Qaeda would not have existed . "
Qutb had an even greater influence on Osama bin Laden 's mentor and another leading member of al-Qaeda , Ayman al-Zawahiri .
Abdel Bari Atwan writes that
Researchers have described five distinct phases in the development of al-Qaeda : the beginning in the late 1980s , the " wilderness " period in 1990 -- 1996 , its " heyday " in 1996 -- 2001 , the network period 2001 -- 2005 , and a period of fragmentation from 2005 to today .
According to Wright , the group 's real name was n't used in public pronouncements because " its existence was still a closely held secret . "
The origins of al-Qaeda as a network inspiring terrorism around the world and training operatives can be traced to the Soviet war in Afghanistan ( December 1979 -- February 1989 ) .
Maktab al-Khidamat was established by Abdullah Azzam and Bin Laden in Peshawar , Pakistan , in 1984 .
Beginning in 1987 , Azzam and bin Laden started creating camps inside Afghanistan .
The Soviet Union finally withdrew from Afghanistan in 1989 .
One of these was the organization that would eventually be called al-Qaeda , formed by Osama bin Laden with an initial meeting held on August 11 , 1988 .
Bin Laden wished to establish nonmilitary operations in other parts of the world ; Azzam , in contrast , wanted to remain focused on military campaigns .
After Azzam was assassinated in 1989 , the MAK split , with a significant number joining bin Laden 's organization .
He traveled to Afghanistan and Pakistan and became " deeply involved with bin Laden 's plans . "
In 1991 , Ali Mohammed is said to have helped orchestrate Osama bin Laden 's relocation to Sudan .
Following the Soviet Union 's withdrawal from Afghanistan in February 1989 , Osama bin Laden returned to Saudi Arabia .
Bin Laden offered the services of his mujahideen to King Fahd to protect Saudi Arabia from the Iraqi army .
The Saudi monarch refused bin Laden 's offer , opting instead to allow U.S. and allied forces to deploy troops into Saudi territory .
The deployment angered Bin Laden , as he believed the presence of foreign troops in the " land of the two mosques " ( Mecca and Medina ) profaned sacred soil .
During this time , bin Laden assisted the Sudanese government , bought or set up various business enterprises , and established camps where insurgents trained .
Due to bin Laden 's continuous verbal assault on King Fahd of Saudi Arabia , on 5 March 1994 Fahd sent an emissary to Sudan demanding bin Laden 's passport ; bin Laden 's Saudi citizenship was also revoked .
His family was persuaded to cut off his monthly stipend , the equivalent of $ 7 million a year , and his Saudi assets were frozen .
There is controversy over whether and to what extent he continued to garner support from members of his family and/or the Saudi government .
According to Pakistani-American businessman Mansoor Ijaz , the Sudanese government offered the Clinton Administration numerous opportunities to arrest bin Laden .
The Commission has found no credible evidence that this was so .
After the Soviet withdrawal , Afghanistan was effectively ungoverned for seven years and plagued by constant infighting between former allies and various mujahideen groups .
The town is situated near Peshawar in Pakistan but largely attended by Afghan refugees .
Bin Laden 's contacts were still laundering most of these donations , using " unscrupulous " Islamic banks to transfer the money to an " array " of charities which serve as front groups for al-Qaeda or transporting cash-filled suitcases straight into Pakistan .
Another four of the Taliban 's leaders attended a similarly funded and influenced madrassa in Kandahar , Afghanistan .
The continuing internecine strife between various factions , and accompanying lawlessness following the Soviet withdrawal , enabled the growing and well-disciplined Taliban to expand their control over territory in Afghanistan , and they came to establish an enclave which it called the Islamic Emirate of Afghanistan .
In 1994 , they captured the regional center of Kandahar , and after making rapid territorial gains thereafter , conquered the capital city Kabul in September 1996 .
By the end of 2008 , the Taliban had severed any remaining ties with al-Qaeda .
According to senior U.S. military intelligence officials , there are fewer than 100 members of Al-Qaeda remaining in Afghanistan .
In 1996 , al-Qaeda announced its jihad to expel foreign troops and interests from what they considered Islamic lands .
Bin Laden issued a fatwa , which amounted to a public declaration of war against the United States of America and any of its allies , and began to refocus al-Qaeda 's resources towards large-scale , propagandist strikes .
While Al Qaeda leaders are hiding in the tribal areas along the Afghanistan-Pakistan border , the middle-tier of the extremist movement display heightened activity in Somalia and Yemen .
In August 2009 , they made the first assassination attempt against a member of the Saudi royal dynasty in decades .
Al Qaeda in the Arabian Peninsula claimed responsibility for the 2009 bombing attack on Northwest Airlines Flight 253 by Umar Farouk Abdulmutallab .
[ US officials called Awlaki an " example of al-Qaeda reach into " the United States in 2008 after probes into his ties to the September 11 hijackers .
An unnamed official claimed there was good reason to believe Awlaki " has been involved in very serious terrorist activities since leaving the United States [ after 9/11 ] , including plotting attacks against America and our allies . "
He has most recently been associated with Iman University in Yemen where he currently resides .
Though the current structure of al-Qaeda is unknown , information mostly acquired from Jamal al-Fadl provided American authorities with a rough picture of how the group was organized .
Al-Qaeda 's network was built from scratch as a conspiratorial network that draws on leaders of all its regional nodes " as and when necessary to serve as an integral part of its high command . "
Al Qaeda is a way of working ... but this has the hallmark of that approach ... Al Qaeda clearly has the ability to provide training ... to provide expertise ... and I think that is what has occurred here . "
What exactly al-Qaeda is , or was , remains in dispute .
Author and journalist Adam Curtis contends that the idea of al-Qaeda as a formal organization is primarily an American invention .
The name of the organization and details of its structure were provided in the testimony of Jamal al-Fadl , who claimed to be a founding member of the organization and a former employee of Osama bin Laden .
In 2006 , it was estimated that al-Qaeda had several thousand commanders embedded in forty different countries .
According to the award winning BBC documentary The Power of Nightmares , al-Qaeda is so weakly linked together that it is hard to say it exists apart from Osama bin Laden and a small clique of close associates .
The lack of any significant numbers of convicted al-Qaeda members despite a large number of arrests on terrorism charges is cited by the documentary as a reason to doubt whether a widespread entity that meets the description of al-Qaeda exists at all .
Therefore the extent and nature of al-Qaeda remains a topic of dispute .
The first , numbering in the tens of thousands , was " organized , trained , and equipped as insurgent combat forces " in the Soviet-Afghan war .
It was made up primarily of foreign mujahideen from Saudi Arabia and Yemen .
It has been estimated that 62 % of al-Qaeda members have university education .
Al-Qaeda usually does not disburse funds for attacks , and very rarely makes wire transfers .
On December 29 , 1992 , al-Qaeda 's first terrorist attack took place as two bombs were detonated in Aden , Yemen .
Internally , al-Qaeda considered the bombing a victory that frightened the Americans away , but in the United States the attack was barely noticed .
No Americans were killed because the soldiers were staying in a different hotel altogether , and they went on to Somalia as scheduled .
However little noticed , the attack was pivotal as it was the beginning of al-Qaeda 's change in direction , from fighting armies to killing civilians .
In 1993 , Ramzi Yousef used a truck bomb to attack the World Trade Center in New York City .
There he began developing the Bojinka Plot plans to blow up a dozen American airliners simultaneously , to assassinate Pope John Paul II and President Bill Clinton , and to crash a private plane into CIA headquarters .
He was later captured in Pakistan .
In 1996 , bin Laden personally engineered a plot to assassinate Clinton while the president was in Manila for the Asia Pacific Economic Cooperation .
However , intelligence agents intercepted a message just minutes before the motorcade was to leave , and alerted the United States Secret Service .
A barrage of cruise missiles launched by the U.S. military in response devastated an al-Qaeda base in Khost , Afghanistan , but the network 's capacity was unharmed .
Inspired by the success of such a brazen attack , al-Qaeda 's command core began to prepare for an attack on the United States itself .
The September 11 , 2001 , attacks were the most devastating terrorist acts in American history , killing approximately 3,000 people .
The attacks were conducted by al-Qaeda , acting in accord with the 1998 fatwa issued against the U.S. and its allies by military forces under the command of bin Laden , al-Zawahiri , and others .
Evidence points to suicide squads led by al-Qaeda military commander Mohamed Atta as the culprits of the attacks , with bin Laden , Ayman al-Zawahiri , Khalid Shaikh Mohammed , and Hambali as the key planners and part of the political and military command .
Messages issued by bin Laden after September 11 , 2001 , praised the attacks , and explained their motivation while denying any involvement .
He also claimed the 9/11 attacks were not targeted at women and children , but ' America 's icons of military and economic power ' .
Evidence has since come to light that the original targets for the attack may have been nuclear power stations on the east coast of the U.S .
The targets were later altered by al-Qaeda , as it was feared that such an attack " might get out of hand " .
Al-Qaeda has been designated a terrorist organization by a number of organizations , including :
Before the U.S. attacked , it offered Taliban leader Mullah Omar a chance to surrender bin Laden and his top associates .
The Taliban offered to turn over bin Laden to a neutral country for trial if the United States would provide evidence of bin Laden 's complicity in the attacks .
U.S. President George W. Bush responded by saying : " We know he 's guilty .
As a result of the U.S. using its special forces and providing air support for the Northern Alliance ground forces , both Taliban and al-Qaeda training camps were destroyed , and much of the operating structure of al-Qaeda is believed to have been disrupted .
After being driven from their key positions in the Tora Bora area of Afghanistan , many al-Qaeda fighters tried to regroup in the rugged Gardez region of the nation .
Nevertheless , a significant Taliban insurgency remains in Afghanistan , and al-Qaeda 's top two leaders , bin Laden and al-Zawahiri , evaded capture .
In September 2004 , the U.S. government commission investigating the September 11 attacks officially concluded that the attacks were conceived and implemented by al-Qaeda operatives .
[ citation needed ] Mohammed Atef and several others were killed .
From 1991 to 1996 , Osama bin Laden and other Al-Qaeda leaders were based in Sudan .
Islamist rebels in the Sahara calling themselves Al-Qaeda in the Islamic Maghreb have stepped up their violence in recent years .
French officials [ citation needed ] say the rebels have no real links to the al-Qaeda leadership , but this is a matter of some dispute in the international press and amongst security analysts .
It seems likely that bin Laden approved the group 's name in late 2006 , and the rebels " took on the al Qaeda franchise label " , almost a year before the violence began to escalate .
Some had previously met Osama Bin Laden , and although they specifically declined to pledge allegiance to Al-Qaeda they asked for its blessing and help .
The massively complex police and MI5 investigation of the plot involved more than a year of surveillance work conducted by over two hundred officers .
Although it is unlikely bin Laden or Saudi al-Qaeda were directly involved , the personal connections they made would be established over the next decade and used in the USS Cole bombing .
In Iraq , al-Qaeda forces loosely associated with the leadership were embedded in the Jama'at al-Tawhid wal-Jihad organization commanded by Abu Musab al-Zarqawi .
Although they played a small part in the overall insurgency , between 30 % and 42 % of all suicide bombings which took place in the early years were claimed by Zarqawi 's organization .
Rather , large groups such as Hamas and Palestinian Islamic Jihad -- which cooperate with al-Qaeda in many respects -- have had difficulties accepting a strategic alliance , fearing that Al-Qaeda will co-opt their smaller cells .
By 2001 Kashmiri militant group Harkat-ul-Mujahideen had become a part of the Al-Qaeda coalition .
He proposed hi tech ground sensors along the line of control to prevent militants from infiltrating into Indian administered Kashmir .
In 2006 Al-Qaeda claim they have established wing in Kashmir this has worried the Indian government .
He however stated that Alqaeda had strong ties with Kashmir militant groups Lashkar-e-Taiba and Jaish-e-Mohammed based in Pakistan .
It has been noted that Waziristan has now become the new battlefield for Kashmiri militants who were now fighting NATO in support of Al-Qaeda and Taliban .
Maulana Masood Azhar the founder of another Kashmiri group Jaish-e-Mohammed is believed to have met Osama bin laden several times and received funding from him .
In 2002 Jaish-e-Mohammed organized the kidnapping and murder of Daniel Pearl in an operation run in conjunction with Al-qaeda and funded by Bin Laden .
Lashkar-e-Taiba a Kashmiri militant group which is thought to be behind 2008 Mumbai attacks is also known to have strong ties to senior Al-qaeda leaders living in Pakistan .
Lashkar-e-Taiba is a member of al Qaeda . '
On September 2009 U.S. Drone strike reportedly killed Ilyas Kashmiri who was the chief of Harkat-ul-Jihad al-Islami a Kashmiri militant group associated with Al Qaeda .
Kashmiri was described by Bruce Riedel as a ' prominent ' Al-qaeda member .
while others have described him as head of military operations for Al-Qaeda .
Abu Ayyub al-Masri 's al-Qaeda movement in Iraq regularly releases short videos glorifying the activity of jihadist suicide bombers .
In addition , both before and after the death of Abu Musab al-Zarqawi ( the former leader of al-Qaeda in Iraq ) , the umbrella organization to which al-Qaeda in Iraq belongs , the Mujahideen Shura Council , has a regular presence on the Web .
Other decapitation videos and pictures , including those of Paul Johnson , Kim Sun-il , and Daniel Pearl , were first posted on jihadist websites .
In December 2004 an audio message claiming to be from Bin Laden was posted directly to a website , rather than sending a copy to al Jazeera as he had done in the past .
Bin Laden 's December 2004 message was much more vehement than usual in this speech , lasting over an hour .
Alneda was initially taken down by American Jon Messner , but the operators resisted by shifting the site to various servers and strategically shifting content .
Al-Qaeda is believed to be operating a clandestine aviation network including " several Boeing 727 aircraft " , turboprops and executive jets , according to a Reuters story .
Based on a US Department of Homeland Security report , the story said that Al-Qaeda is possibly using aircraft to transport drugs and weapons from South America to various unstable countries in West Africa .
A Boeing 727 can carry up to 10 tons of cargo .
According to Peter Bergen , known for conducting the first television interview with Osama bin Laden in 1997 , the idea that " the CIA funded bin Laden or trained bin Laden ... [ is ] a folk myth .
... Bin Laden had his own money , he was anti-American and he was operating secretly and independently .
The real story here is the CIA did n't really have a clue about who this guy was until 1996 when they set up a unit to really start tracking him . "
But as Bergen himself admitted , in one " strange incident " the CIA did appear to give visa help to mujahideen-recruiter Omar Abdel-Rahman .
