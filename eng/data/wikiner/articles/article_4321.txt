Edwin S. Porter is generally thought to be the American filmmaker who first put film editing to use .
Porter worked as an electrician before joining the film laboratory of Thomas Alva Edison in the late 1890s .
Early films by Thomas Edison ( whose company invented a motion camera and projector ) and others were short films that were one long , static , locked-down shot .
Porter made the breakthrough film Life of an American Fireman in 1903 .
Porter 's ground-breaking film , The Great Train Robbery is still shown in film schools today as an example of early editing form .
Being one of the first film hyphenates ( film director , editor and engineer ) Porter also invented and utilized some of the very first ( albeit primitive ) special effects such as double exposures , miniatures and split-screens .
Porter discovered important aspects of motion picture language : that the screen image does not need to show a complete person from head to toe and that splicing together two shots creates in the viewer 's mind a contextual relationship .
That is , The Great Train Robbery contains scenes shot on sets of a telegraph station , a railroad car interior , and a dance hall , with outdoor scenes at a railroad water tower , on the train itself , at a point along the track , and in the woods .
Sometime around 1918 , Russian director Lev Kuleshov did an experiment that proves this point .
Today , most films are edited digitally ( on systems such as Avid or Final Cut Pro ) and bypass the film positive workprint altogether .
In the U.S. , under DGA rules , directors receive a minimum of ten weeks after completion of principal photography to prepare their first cut .
There have been several conflicts in the past between the director and the studio , sometimes leading to the use of the " Alan Smithee " credit signifying when a director no longer wants to be associated with the final release .
A good example of a continuity error is in the film Braveheart with Mel Gibson .
Lev Kuleshov was among the very first to theorize about the relatively young medium of the cinema in the 1920s .
Although , strictly speaking , U.S. film director D.W. Griffith was not part of the montage school , he was one of the early proponents of the power of editing -- mastering cross-cutting to show parallel action in different locations , and codifying film grammar in other ways as well .
Sergei Eisenstein was briefly a student of Kuleshov 's , but the two parted ways because they had different ideas of montage .
Eisenstein regarded montage as a dialectical means of creating meaning .
One famous example of montage was seen in the 1968 film 2001 : A Space Odyssey ( film ) , depicting the start of man 's first development from apes to humans .
Early Russian filmmakers such as Lev Kuleshov further explored and theorized about editing and its ideological nature .
Both filmmakers , Clair and Buñuel , experimented with editing techniques long before what is referred to as " MTV style " editing .
Stanley Kubrick noted that the editing process is the one phase of production that is truly unique to motion pictures .
Kubrick was quoted as saying : " I love editing .
Murch assigned notional percentage values to each of the criteria .
