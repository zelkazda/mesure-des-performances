The theory is not applied on European integration which rejects the idea of neofunctionalism .
The theory , initially proposed by Stanley Hoffmann suggests that national governments control the level and speed of European integration .
Various intergovernmentalist approaches have been developed in the literature and these claim to be able to explain both periods of radical change in the European Union ( because of the converging governmental preferences ) and periods of inertia ( due to the diverging national interests ) .
