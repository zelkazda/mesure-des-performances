Interest in the field increased dramatically after nuclear fusion was reported in a tabletop experiment involving electrolysis of heavy water on a palladium electrode by Martin Fleischmann , then one of the world 's leading electro-chemists , and Stanley Pons in 1989 .
In 1989 , the majority of a review panel organized by the US Department of Energy found that the evidence for the discovery of a new nuclear process was not persuasive .
The term " cold fusion " was used as early as 1956 in a New York Times article about Luis W. Alvarez ' work on muon-catalyzed fusion .
E. Paul Palmer of Brigham Young University also used the term " cold fusion " in 1986 in an investigation of " geo-fusion " , the possible existence of fusion in a planetary core .
The ability of palladium to absorb hydrogen was recognized as early as the nineteenth century by Thomas Graham .
Martin Fleischmann of the University of Southampton and Stanley Pons of the University of Utah hypothesized that the high compression ratio and mobility of deuterium that could be achieved within palladium metal using electrolysis might result in nuclear fusion .
The grant proposal was turned over for peer review , and one of the reviewers was Steven E. Jones of Brigham Young University .
Fleischmann and Pons and co-workers met with Jones and co-workers on occasion in Utah to share research and techniques .
Jones , however , was measuring neutron flux , which was not of commercial interest .
Fleischmann and Pons , however , pressured by the University of Utah which wanted to establish priority on the discovery , broke their apparent agreement , submitting their paper to the Journal of Electroanalytical Chemistry on March 11 , and disclosing their work via a press conference on March 23 .
Jones , upset , faxed in his paper to Nature after the press conference .
Fleischmann and Pons ' announcement drew wide media attention .
One of the more prominent reports of success came from a group at the Georgia Institute of Technology , which observed neutron production .
The Georgia Tech group later retracted their announcement .
Another team , headed by Robert Huggins at Stanford University also reported early success , but it was called into question by a colleague who reviewed his work .
In May 1989 , the American Physical Society held a session on cold fusion , at which were heard many reports of experiments that failed to produce evidence of cold fusion .
In July and November 1989 , Nature published papers critical of cold fusion claims .
Negative results were also published in several scientific journals including Science , Physical Review Letters , and Physical Review C ( nuclear physics ) .
The United States Department of Energy organized a special panel to review cold fusion theory and research .
The Nobel Laureate Julian Schwinger , in a shock to most physicists , declared himself a supporter of cold fusion after much of the response to the initial reports had turned negative .
He tried to publish theoretical papers supporting the possibility of cold fusion in Physical Review Letters , was deeply insulted by their rejection , and resigned from the American Physical Society in protest .
In 2007 , the American Chemical Society 's ( ACS ) held an " invited symposium " on cold fusion and low-energy nuclear reactions while explaining that this does not show a softening of skepticism .
An ACS program chair said that " with the world facing an energy crisis , it is worth exploring all possibilities . "
In 1994 , David Goodstein described cold fusion as " a pariah field , cast out by the scientific establishment .
Fleischmann and Pons reported a neutron flux of 4,000 neutrons per second , as well as tritium , while the classical branching ratio for previously known fusion reactions that produce tritium would predict , with 1 watt of power , the production of 10 12 neutrons per second , levels that would have been fatal to the researchers .
The Massachusetts Institute of Technology ( MIT ) announced on 12 April 1989 that it had applied for its own patents based on theoretical work of one of its researchers , Peter L. Hagelstein , who had been sending papers to journals from the 5th to the 12th of April .
The U.S. Patent and Trademark Office ( USPTO ) now rejects patents claiming cold fusion .
