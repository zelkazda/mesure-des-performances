An international reply coupon ( IRC ) is a coupon that can be exchanged for one or more postage stamps representing the minimum postage for an unregistered priority airmail letter of up to twenty grams sent to another Universal Postal Union ( UPU ) member country .
UPU member postal services are obliged to exchange an IRC for postage , but are not obliged to sell them .
The IRC was introduced in 1906 at a Universal Postal Union congress in Rome .
As of 2006 an IRC is exchangeable in a UPU member country for the minimum postage of a priority or unregistered airmail letter to a foreign country .
They are generally available at large post offices ; in the U.S. , they are requisitioned along with regular domestic stamps by any post office that has sufficient demand for them .
In the U.S. in late 2008 , the purchase price was $ 2.10 USD .
