The catastrophic Finnish famine of 1866 -- 1868 was followed by eased economic regulation and political development .
The country joined the European Union in 1995 .
Among the finds is the net of Antrea , one of the oldest fishing nets ever excavated ( calibrated carbon dating : ca. 8300 BCE ) .
Cultural influences from all points of the compass are visible in Finnish archeological finds from the very first settlements onwards .
Swedish language differentiated from the eastern Norse dialects by the 13th century .
Österland ( lit .
The concept of a Finnish " country " in the modern sense developed only slowly during the period of the 15th -- 18th centuries .
Western Karelians were from then on viewed as part of the western cultural sphere , while eastern Karelians turned culturally to Russia and Orthodoxy .
In the early 14th century , the first documents of Finnish students at Sorbonne appear .
In the south-western part of the country , an urban settlement evolved in Turku .
During the 13th century , the bishopric of Turku was established .
Occasional raids and clashes between Swedes and Novgorodians occurred during the late 14th and 15th centuries , but for most of the time an uneasy peace prevailed .
In 1521 the Kalmar Union collapsed and Gustav Vasa became the King of Sweden .
During his rule , the Swedish church was reformed ( 1527 ) .
Following the policies of the Reformation , in 1551 Mikael Agricola , bishop of Turku , published his translation of the New Testament into the Finnish language .
In 1550 Helsinki was founded by Gustav Vasa under the name of Helsingfors , but remained little more than a fishing village for more than two centuries .
King Erik XIV started an era of expansion when the Swedish crown took the city of Tallinn in Estonia under its protection in 1561 .
The Livonian War was the beginning of a warlike era which lasted for 160 years .
An important part of the 16th century history of Finland was growth of the area settled by the farming population .
Some of the wilderness settled was traditional hunting and fishing territory of Karelian hunters .
During the 1580s , this resulted in a bloody guerrilla warfare between the Finnish settlers and Karelians in some regions , especially in Ostrobothnia .
In 1611-1632 Sweden was ruled by King Gustavus Adolphus , whose military reforms transformed the Swedish army from a peasant militia into an efficient fighting machine , possibly the best in Europe .
The conquest of Livonia was now completed , and some territories were taken from internally divided Russia in the Treaty of Stolbova .
The Finnish light cavalry was known as the Hakkapeliitat .
After the Peace of Westphalia in 1648 , the Swedish Empire was one of the most powerful countries in Europe .
According to some theories , the spirit of early capitalism in the tar-producing province of Ostrobothnia may have been the reason for the witch-hunt wave that happened in this region during the late 17th century .
At least half of the immigrants were of Finnish origin .
The 17th century was an era of very strict Lutheran orthodoxy .
Every subject of the realm was required to confess the Lutheran faith and church attendance was mandatory .
On the other hand , the Lutheran requirement of the individual study of Bible prompted the first attempts at wide-scale education .
The church required from each person a degree of literacy sufficient to read the basic texts of the Lutheran faith .
In 1697 -- 99 , a famine caused by climate [ citation needed ] killed approximately 30 % of the Finnish population .
The border with Russia came to lie roughly where it returned to after World War II .
The absolute monarchy was ended in Sweden .
Both the ascending Russian Empire and pre-revolutionary France aspired to have Sweden as a client state .
In 1788 , he started a new war against Russia .
In 1789 , the new constitution of Sweden strengthened the royal power further , as well as improving the status of the peasantry .
These occupations were a seed of a feeling of separateness and otherness , that in a narrow circle of scholars and intellectuals at the university in Turku was forming a sense of a separate Finnish identity representing the eastern part of the realm .
The new king was not a particularly talented ruler ; at least not talented enough to steer his kingdom through the dangerous era of the French Revolution and Napoleonic wars .
However , gradually the rulers of Russia granted large estates of land to their non-Finnish favorites , ignoring the traditional landownership and peasant freedom laws of Old Finland .
Before that , German , Latin and Swedish were important languages beside native-spoken Finnish .
In 1863 , the Finnish language gained a position in administration , and 1892 Finnish finally became an equal official language and gained a status comparable to that of Swedish .
Within a generation Finnish clearly dominated in government and society .
The October Revolution turned Finnish politics upside down .
On November 15 , 1917 , the Bolsheviks declared a general right of self-determination , including the right of complete secession , " for the Peoples of Russia " .
The latter proclaimed a Finnish Socialist Workers ' Republic .
Neighboring Sweden was in the midst of her own process of democratization , with socialists in government for the first time .
Nevertheless , the residents did not approve the offer , and the dispute over the islands was submitted to the League of Nations .
At the same time , an international treaty was concluded on the neutral status of Åland , under which it was prohibited to place military headquarters or forces on the islands .
The Winter War ended on 13 March 1940 with the Moscow peace treaty .
After the Winter War the Finnish army was in very bad shape , and needed recovery and support as soon as possible .
It was , however , punished harsher than other German co-belligerents and allies , having to pay large reparations and resettle an eighth of its population after having lost an eighth of the territory including one of its industrial heartlands and the second-largest city of Viipuri .
In late 1940 , German-Finnish co-operation had begun , and was unique when compared to relations with The Axis .
The commander of Finnish armed forces during the Winter War and the Continuation War , Carl Gustaf Emil Mannerheim , became the president of Finland after the war .
In 1950 half of the Finnish workers were occupied in agriculture and a third lived in urban towns .
When baby boomers entered the workforce , the economy did not generate jobs fast enough and hundreds of thousands emigrated to the more industrialized Sweden , migration peaking in 1969 and 1970 .
The 1952 Summer Olympics brought international visitors .
In 1972 Max Jakobson was a candidate for Secretary-General of the UN .
Mauno Koivisto and later Tarja Halonen classified documents about their and other politicians ' involvement in the crisis .
Before the parliamentary decision to join the EU , a consultative referendum was held on April 16 , 1994 in which 56.9 % of the votes were in favour of joining .
In the economic policy , the EU membership forced large changes .
During Prime Minister Paavo Lipponen 's two successive governments 1995 -- 2003 , several large state companies were privatized fully or partially .
1000 troops ( a high per-capita amount ) are simultaneously committed in NATO and UN operations .
