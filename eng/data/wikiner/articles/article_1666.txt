This state of matter was first predicted by Satyendra Nath Bose and Albert Einstein in 1924 -- 25 .
Bose first sent a paper to Einstein on the quantum statistics of light quanta ( now called photons ) .
This phenomenon was predicted in 1925 by generalizing Satyendra Nath Bose 's work on the statistical mechanics of ( massless ) photons to ( massive ) atoms .
( The Einstein manuscript , once believed to be lost , was found in a library at Leiden University in 2005 . )
Einstein demonstrated that cooling bosonic atoms to a very low temperature would cause them to fall ( or " condense " ) into the lowest accessible quantum state , resulting in a new form of matter .
Bose -- Einstein condensation remains , however , fundamental to the superfluid properties of helium-4 .
About four months later , an independent effort led by Wolfgang Ketterle at MIT created a condensate made of sodium-23 .
Ketterle 's condensate had about a hundred times more atoms , allowing him to obtain several important results such as the observation of quantum mechanical interference between two different condensates .
Further experimentation on attractive condensates was performed by the JILA team in 2000 .
The JILA team instrumentation now had better control over the condensate so experimentation was made on naturally attracting atoms of another rubidium isotope , rubidium-85 ( having negative atom-atom scattering length ) .
Nevertheless , they have proven useful in exploring a wide range of questions in fundamental physics , and the years since the initial discoveries by the JILA and MIT groups have seen an explosion in experimental and theoretical activity .
Bose -- Einstein condensates composed of a wide range of isotopes have been produced .
