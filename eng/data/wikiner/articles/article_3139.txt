Christopher Columbus named the island after the day of the week on which he spotted it -- a Sunday -- which fell on November 3 , 1493 .
In 1761 a British expedition against Dominica led by Lord Rollo was successful and the island was conquered along with several other Caribbean islands .
The 1805 invasion burned much of Roseau to the ground .
Following World War I , an upsurge of political consciousness throughout the Caribbean led to the formation of the representative government association .
In 1961 , a Dominica Labor Party government led by Edward Oliver LeBlanc was elected .
In August 1979 , Hurricane David , packing winds of 150 mph ( 240 km/h ) , struck the island with devastating force .
In 1995 the government was defeated in elections by the United Workers Party of Edison James .
James became prime minister , serving until the February 2000 elections , when the Dominica United Workers Party was defeated by the Dominica Labour Party ( DLP ) , led by Rosie Douglas .
However , these were somewhat quieted when he formed a coalition with the more conservative Dominica Freedom Party .
Douglas died suddenly after only eight months in office , on October 1 , 2000 , and was replaced by Pierre Charles , also of the DLP .
In 2003 , Nicholas Liverpool was elected and sworn in as president , succeeding Vernon Shaw .
On January 6 , 2004 , Prime Minister Pierre Charles , who had been suffering from heart problems since 2003 , died .
The foreign minister , Osborne Riviere immediately became prime minister , but the education minister , Roosevelt Skerrit succeeded him as prime minister and became the new leader of the Dominica Labour Party .
