Diana ( lt .
Diana was known to be the virgin goddess and looked after virgins and women .
She was one of the three maiden goddesses , Diana , Minerva and Vesta , who swore never to marry .
Along with her main attributes , Diana was an emblem of chastity .
According to mythology , Diana was born with her twin brother Apollo on the island of Delos , daughter of Jupiter and Latona .
The image of Diana is complex and shows various archaic features .
The uranic character of Diana is well reflected in her connexion to light , inaccessibility , virginity , dwelling on high mountains and in sacred woods .
Diana is thus the representation of the heavenly world ( dium ) in its character of sovereignty , supremacy , impassibility , indifference towards secular matters as the fate of men and states , while at the same time ensuring the succession of kings and the preservation of mankind through the protection of childbirth .
2 ) Diana was also worshipped by women who sought pregnancy or asked for an easy delivery .
Diana is a female god but has exactly the same functions , preserving mankind through childbirth and king succession .
Diana often showed as a young girl , age around 13 to 19 .
But as a goddess of moon , Diana wore a long robe , sometimes a veil covered her head .
Diana was initially just the hunting goddess, [ citation needed ] associated with wild animals and woodlands .
She also later became a moon goddess , supplanting Luna .
Diana was regarded with great reverence by lower-class citizens and slaves ; slaves could receive asylum in her temples .
The deer may also offer a covert reference to the myth of Acteon ( or Actaeon ) , who saw her bathing naked .
Diana transformed Acteon into a stag and set his own hunting dogs to kill him .
( Acts 19:28 , New English Bible ) .
( See To Ride A Silver Broomstick and/or Sacred-texts.com ) .
Goddess Diana created the world of her own being having in herself the seeds of all creation yet to come .
It is said that out of herself she divided into the darkness and the light , keeping for herself the darkness of creation and creating her brother Apollo , the light .
Goddess Diana loved and ruled with her brother Apollo , the god of the Sun .
In Shakespeare 's play , Romeo and Juliet , many references are made to Diana .
In Jean Cocteau 's 1946 film La Belle et la Bête it is Diana 's power which has transformed and imprisoned the beast .
Diana had become one of the most popular theme of arts .
Most of stories that being exposed are the stories of Diana with Actaeon , story of Callisto , and when she rested after hunting .
Some famous work of arts with Diana theme are :
On the mantel he painted an image of Diana riding in a chariot pulled possibly by a stag .
