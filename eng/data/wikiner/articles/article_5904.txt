She was sworn in as Mayor in November 2000 , replacing Jimmie Yee , and served until December 2008 , when she was replaced by Kevin Johnson .
Heather Fargo grew up in the privileged atmosphere of the wealthy and powerful Fargo family , decedents of one of the founders of Wells Fargo bank .
Other than Fargo , three other councilmembers were also seeking the mayorship .
Fargo did not face as stiff competition in her 2004 re-election .
Virtually unopposed against candidates far less funded , Fargo won solidly in the primary election , thus no runoff was necessary .
During the primary election campaign , Fargo initially claimed that she had the support of all the city councilmembers .
During her tenure Mayor Fargo became a member of the Mayors Against Illegal Guns Coalition , an organization formed in 2006 and co-chaired by New York City mayor Michael Bloomberg and Boston mayor Thomas Menino .
In the late 1990s and early 2000s , Mayor Fargo made several attempts to provide taxpayer financing of a new stadium for the Kings NBA basketball franchise .
Because Fargo received a majority of the votes in the primary election , no general election was necessary .
