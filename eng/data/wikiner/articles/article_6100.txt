They are often used with network address translators to connect to the global public Internet .
In 1981 , the Internet addressing specification was revised with the introduction of classful network architecture .
Today , when needed , such private networks typically connect to the Internet through network address translation ( NAT ) .
At these levels , actual address utilization rates will be small on any IPv6 network segment .
IPv6 has facilities that automatically change the routing prefix of entire networks should the global connectivity or the routing policy change without requiring internal redesign or renumbering .
In IPv6 , these are referred to as unique local addresses ( ULA ) .
This feature is used extensively , and invisibly to most users , in the lower layers of IPv6 network administration .
None of the private address prefixes may be routed in the public Internet .
In IPv6 , every interface , whether using static or dynamic address assignments , also receives a local-link address automatically in the fe80::/10 subnet .
These addresses are not routable and like private addresses can not be the source or destination of packets traversing the Internet .
An address obtained from a DNS server comes with a time to live , or caching time , after which it should be looked up to confirm that it has not changed .
Firewalls are common on today 's Internet .
Although iproute2 's " ip " command is sometimes more appropriate for Linux .
