National Alliance was a conservative political party in Italy .
National Alliance was formed by Gianfranco Fini from the Italian Social Movement ( MSI ) , the ex-neo-fascist party , which was declared dissolved in January 1995 , and conservative elements of the former Christian Democracy , which had disbanded in 1994 after two years of scandals and various splits due to corruption at its highest levels , exposed by the Mani Pulite investigation , and the Italian Liberal Party , disbanded in the same year .
Former MSI members were however still the bulk of the new party .
The logo followed a template very similar to the Democratic Party of the Left , with the previous logo in a small circle ( as a means of legally preventing others from using it ) .
In January 1995 a party congress officially proclaimed the dissolution of MSI , the rejection of most MSI 's ideological stances and the establishment of National Alliance .
The party was part of all three House of Freedoms coalition governments led by Silvio Berlusconi .
He also referred to the Italian Social Republic as belonging to the most shameful pages of the past , and considered fascism part of an era of " absolute evil " , something which was hardly acceptable to the few remaining hardliners of the party .
As a result , Alessandra Mussolini , the granddaughter of the former fascist dictator Benito Mussolini , who had been at odds with the party on a number of issues for a long time , and some hardliners left the party and formed Social Action .
The centre-right lost by 24,000 votes in favor of the centre-left The Union .
In July 2007 a group of splinters led by Francesco Storace formed The Right party , which was officially founded on 10 November .
In November 2007 Silvio Berlusconi announced that Forza Italia would have soon merged or transformed into the The People of Freedom ( PdL ) party .
In an atmosphere of reconciliation with Gianfranco Fini , Berlusconi also stated that the new party could see the participation of other parties .
Finally , on 8 February , Berlusconi and Fini agreed to form a joint list under the banner of the " The People of Freedom " , allied with Lega Nord .
National Alliance 's political program emphasized :
With most hardliners leaving the party , it sought to present itself as a respectable conservative party and to join forces with Forza Italia in the European People 's Party and , eventually , in a united party of the centre-right .
Regarding institutional reforms , the party was a long-time supporter of presidentialism and first-past-the-post , and came to support also federalism and to fully accept the alliance with Lega Nord , although the relations with that party were tense at times , especially about issues regarding national unity .
Gianfranco Fini , a modernizer who sees Nicolas Sarkozy and David Cameron as models , impressed an ambitious political line to the party , combining the pillars of conservative ideology like security , family values and patriotism with a progressive approach in other areas such as stem-cell research and supporting voting rights for legal aliens .
National Alliance was a heterogeneous political party and within it members were divided in different factions , some of them very organized :
The party had a good showing in the first general election to which it took part ( 13.5 % in 1994 ) and reached 15.7 % in 1996 , when Fini tried for the first time to replace Silvio Berlusconi as leader of the centre-right .
From that moment the party suffered an electoral decline , but remains the third force of Italian politics .
