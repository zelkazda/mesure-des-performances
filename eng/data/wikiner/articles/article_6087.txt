Galton argued that due to heredity , white aristocrats were intellectually superior to other humans ; nurture , meanwhile , played a lesser role in intellectual capacity .
French psychologist Alfred Binet gave more weight to nurture , arguing that intelligence could be improved .
English psychologist Charles Spearman , however , did not believe there were different types of intelligence .
He devised a correlational formula to define a common intellective factor ( which he called " general intelligence " ) , a factor that Binet argued did not exist .
A team of psychologists led by eugenicist Robert Yerkes assisted the US army to rapidly assess and assign huge numbers of personnel .
The outcome was that 0.5 % of recruits were discharged as mentally inferior , whilst Yerkes would have preferred to have discharged 3 % whose results showed a mental age of under 10 .
In contrast , another team of psychologists led by Walter Dill Scott defined intelligence as a diverse complex of capacities , as Binet had .
The army abolished Yerkes ' team after the war but employed two psychologists to continue intelligence testing research .
But controversy followed when one of Yerkes ' team , Goddard , admitted that they had been guilty of bad logic in finding that 45 % of army recruits had a mental age of 12 or less .
This procedure was pioneered by David Wechsler .
A study of 11,282 individuals in Scotland who took intelligence tests at ages 7 , 9 and 11 in the 1950s and 1960s , found an " inverse linear association " between childhood IQ scores and hospital admissions for injuries in adulthood .
Research in Scotland has also shown that a 15-point lower IQ meant people had a fifth less chance of living to 76 , while those with a 30-point disadvantage were 37 % less likely than those with a higher IQ to live that long .
One of the most notable researchers arguing for a strong hereditary basis is Arthur Jensen .
One study found a correlation of .82 between g ( general intelligence factor ) and SAT scores ; another has found correlation of .81 between g and GCSE scores .
On pg 568 of The g Factor , Arthur Jensen claims that although the correlation between IQ and income averages a moderate 0.4 ( one sixth or 16 % of the variance ) , the relationship increases with age , and peaks at middle age when people have reached their maximum career potential .
The paper breaks down IQ averages by U.S. states using the federal government 's National Assessment of Educational Progress math and reading test scores as a source .
In 2008 , intelligence researcher Helmuth Nyborg examined whether IQ relates to denomination and income , using representative data from the National Longitudinal Study of Youth .
In the United States , certain public policies and laws regarding military service , education , public benefits , crime , and employment incorporate an individual 's IQ or similar measurements into their decisions .
Daniel Schacter , Daniel Gilbert , and others have moved beyond general intelligence and IQ as the sole means to describe intelligence .
The APA journal that published the statement , American Psychologist , subsequently published eleven critical responses in January 1997 , several of them arguing that the report failed to examine adequately the evidence for partly-genetic explanations .
