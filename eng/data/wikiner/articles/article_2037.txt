Physical cosmology , as it is now understood , began with the twentieth century development of Albert Einstein 's general theory of relativity and better astronomical observations of extremely distant objects .
In 1915 , Albert Einstein developed his theory of general relativity .
Einstein added a cosmological constant to his theory to try to force it to allow for a static universe with matter in it .
The so-called Einstein universe is , however , unstable .
The other possibility was Fred Hoyle 's steady state model in which new matter would be created as the galaxies moved away from each other .
One consequence of this is that in standard general relativity , the universe began with a singularity , as demonstrated by Stephen Hawking and Roger Penrose in the 1960s .
The basic theory of nucleosynthesis was developed in 1948 by George Gamow , Ralph Asher Alpher and Robert Herman .
The radiation , first observed in 1965 by Arno Penzias and Robert Woodrow Wilson , has a perfect thermal black-body spectrum .
Cosmological perturbation theory , which describes the evolution of slight inhomogeneities in the early universe , has allowed cosmologists to precisely calculate the angular power spectrum of the radiation , and it has been measured by the recent satellite experiments ( COBE and WMAP ) and many ground and balloon-based experiments ( such as Degree Angular Scale Interferometer , Cosmic Background Imager , and Boomerang ) .
The recent measurements made by WMAP , for example , have placed limits on the neutrino masses .
Newer experiments , such as QUIET and the Atacama Cosmology Telescope , are trying to measure the polarization of the cosmic microwave background .
Steven Weinberg and a number of string theorists ( see string landscape ) have used this as evidence for the anthropic principle , which suggests that the cosmological constant is so small because life ( and thus physicists , to make observations ) can not exist in a universe with a large cosmological constant , but many people find this an unsatisfying explanation .
