According to the Constitution of the Czech Republic , the President is the head of state while the Prime Minister is the head of government , exercising supreme executive power .
The Czech political scene supports a broad spectrum of parties ranging from Communist Party on the far left to various nationalistic parties on the extreme right .
Generally , the ( liberal ) right beyond the specific case of huge and conservative Civic Democratic Party is splintered and has failed in several attempts to unite .
A government formed of a coalition of the ODS , KDU-ČSL , and the Green Party ( SZ ) , and led by the leader of the ODS Mirek Topolánek finally succeeded in winning a vote of confidence on January 19 , 2007 .
On March 23 , 2009 , the government of Mirek Topolánek lost a vote of no-confidence .
The President of the Czech Republic is elected by joint session of the parliament for five-year term ( no more than two consecutive ) .
The Senate ( Senát ) has 81 members , in single-seat constituencies elected by two-round runoff voting for a six-year term , with one third renewed every even year in the autumn .
