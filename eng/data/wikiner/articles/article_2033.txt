In Japan , they were made of tortoise shell or animal horn .
Falloppio 's treatise is the earliest uncontested description of condom use : it describes linen sheaths soaked in a chemical solution and allowed to dry before use .
:51,54-5 Falloppio claimed that an experimental trial of the linen sheath demonstrated protection against syphilis .
After this , the use of penis coverings to protect from disease is described in a wide variety of literature throughout Europe .
Casanova in the 18th century was one of the first reported using " assurance caps " to prevent impregnating his mistresses .
Freud was especially opposed to the condom because he thought it cut down on sexual pleasure .
In 1839 , Charles Goodyear discovered a way of processing natural rubber , which is too stiff when cold and too soft when warm , in such a way as to make it elastic .
The rubber vulcanization process was patented by Goodyear in 1844 .
Due to increased demand and greater social acceptance , condoms began to be sold in a wider variety of retail outlets , including in supermarkets and in discount department stores such as Wal-Mart .
There is however no evidence of the existence of such a person , and condoms had been used for over one hundred years before King Charles II ascended to the throne .
In 1990 the ISO set standards for condom production , and the EU followed suit with its CEN standard .
According to Consumer Reports , condoms lubricated with spermicide have no additional benefit in preventing pregnancy , have a shorter shelf life , and may cause urinary-tract infections in women .
The World Health Organization says that spermicidally lubricated condoms should no longer be promoted .
As of 2005 , nine condom manufacturers have stopped manufacturing condoms with nonoxynol-9 and Planned Parenthood has discontinued the distribution of condoms so lubricated .
According to a 2000 report by the National Institutes of Health ( NIH ) , correct and consistent use of latex condoms reduces the risk of HIV / AIDS transmission by approximately 85 % relative to risk when unprotected , putting the seroconversion rate ( infection rate ) at 0.9 per 100 person-years with condom , down from 6.7 per 100 person-years .
Analysis published in 2007 from the University of Texas Medical Branch and the World Health Organization found similar risk reductions of 80 -- 95 % .
The 2000 NIH review concluded that condom use significantly reduces the risk of gonorrhea for men .
A Family Health International publication also offers the view that education can reduce the risk of breakage and slippage , but emphasizes that more research needs to be done to determine all of the causes of breakage and slippage .
Some commercial sex workers from Nigeria reported clients sabotaging condoms in retaliation for being coerced into condom use .
Japan has the highest rate of condom usage in the world : in that country , condoms account for almost 80 % of contraceptive use by married women .
A recent American Psychological Association ( APA ) press release supported the inclusion of information about condoms in sex education , saying " comprehensive sexuality education programs ... discuss the appropriate use of condoms " , and " promote condom use for those who are sexually active . "
Ongoing military utilization begun during World War II includes :
Experts , such as AVERT , recommend condoms be disposed of in a garbage receptacle , as flushing them down the toilet may cause plumbing blockages and other problems .
The United States Environmental Protection Agency also has expressed concerns that many animals might mistake the litter for food .
The Roman Catholic Church opposes all sexual acts outside of marriage , as well as any sexual act in which the chance of successful conception has been reduced by direct and intentional acts ( e.g. , surgery to prevent conception ) or foreign objects ( e.g. , condoms ) .
This view was most recently reiterated in 2009 by Pope Benedict XVI .
The Roman Catholic Church is the largest organized body of any world religion .
However , a 2004 study in Germany detected nitrosamines in 29 out of 32 condom brands tested , and concluded that exposure from condoms might exceed the exposure from food by 1.5-to 3-fold .
Among the Massai in Tanzania , condom use is hampered by an aversion to " wasting " sperm , which is given sociocultural importance beyond reproduction .
Massai women believe that , after conceiving a child , they must have sexual intercourse repeatedly so that the additional sperm aids the child 's development .
Frequent condom use is also considered by some Massai to cause impotence .
Perhaps because of this restricted access to hormonal contraception , Japan has the highest rate of condom usage in the world : in 2008 , 80 % of contraceptive users relied on condoms .
If approved , the condom would be marketed under the Durex brand .
