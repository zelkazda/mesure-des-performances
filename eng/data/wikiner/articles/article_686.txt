Applesoft BASIC was a dialect of BASIC supplied with the Apple II series of computers .
Applesoft BASIC was supplied by Microsoft and its name is derived from the names of both Apple and Microsoft .
Apple employees , including Randy Wigginton , adapted Microsoft 's interpreter for the Apple II and added several features .
The first version of Applesoft was released in 1977 only on cassette tape and lacked proper support for high-resolution graphics .
It is this latter version , which has some syntax differences from the first as well as support for the Apple II high-resolution graphics modes , that most people mean by the term " Applesoft . "
Apple 's customers were demanding a version of BASIC that supported floating point calculations .
Applesoft was similar to ( and indeed had a common code base with ) BASIC implementations on other 6502-based computers , such as Commodore BASIC : it used line numbers , and spaces were not necessary in lines .
While Applesoft was slower than Integer BASIC , it had many features that the older BASIC lacked :
Apple Business BASIC shipped with the Apple /// computer .
