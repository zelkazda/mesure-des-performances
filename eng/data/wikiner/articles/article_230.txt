If a series is drawn then the country already holding the Ashes retains them .
The urn is erroneously believed by some to be the trophy of the Ashes series , but it has never been formally adopted as such and Bligh always considered it to be a personal gift .
Replicas of the urn are often held aloft by victorious teams as a symbol of their victory in an Ashes series , but the actual urn has never been presented or displayed as a trophy in this way .
Since the 1998-99 Ashes series , a Waterford Crystal representation of the Ashes urn has been presented to the winners of an Ashes series as the official trophy of that series .
A celebrated poem appeared in Punch on Saturday , 9 September .
In the 20 years following Bligh 's campaign the term " The Ashes " largely disappeared from public use .
The first mention of " The Ashes " in Wisden Cricketers ' Almanack occurs in 1905 , while Wisden 's first account of the legend is in the 1922 edition .
Nevertheless , several attempts had been made to embody The Ashes in a physical memorial .
The prime evidence for this theory was provided by a descendant of Clarke .
This is the fourth verse of a song-lyric published in Melbourne Punch on 1 February 1883 :
MCC 's wish for it to be seen by as wide a range of cricket enthusiasts as possible has led to its being mistaken for an official trophy .
The urn arrived on 17 October 2006 , going on display at the Museum of Sydney .
As its condition is fragile and it is a prized exhibit at the MCC Cricket Museum , the MCC were reluctant to agree .
As a compromise , the MCC commissioned a trophy in the form of a larger replica of the urn in Waterford Crystal to award to the winning team of each series from 1998 -- 99 .
Australia resoundingly won the 1897 -- 98 series by 4 -- 1 under the captaincy of Harry Trott .
The last wicket pair of George Hirst and Rhodes were left with 15 runs to get , and duly got them .
England retained the Ashes when they won the 1912 Triangular Tournament , which also featured South Africa .
Jack Hobbs and Herbert Sutcliffe took the score to 49 -- 0 at the end of the second day , a lead of 27 .
In spite of the very difficult batting conditions , however , Hobbs and Sutcliffe took their partnership to 172 before Hobbs was out for exactly 100 .
Australian captain Herbie Collins was stripped of all captaincy positions down to club level , and some accused him of throwing the match .
Australia 's aging post-war team broke up after 1926 , with Collins , Charlie Macartney and Warren Bardsley all departing , and Gregory breaking down at the start of the 1928 -- 29 series .
England had a very strong batting side , with Wally Hammond contributing 905 runs at an average of 113.12 , and Hobbs , Sutcliffe and Patsy Hendren all scoring heavily ; the bowling was more than adequate , without being outstanding .
Bradman himself thought that his 254 in the preceding match , at Lord 's , was a better innings .
Australia had one of the strongest batting line-ups ever in the early 1930s , with Bradman , Archie Jackson , Stan McCabe , Bill Woodfull and Bill Ponsford .
Jardine 's comment was : " I 've not travelled 6,000 miles to make friends .
I 'm here to win the Ashes " .
Australia recovered the Ashes in 1934 and held them until 1953 , although no international cricket was possible during the Second World War .
Australia then scraped home by five wickets inside three days in a low-scoring match at Headingley to retain the urn .
However , Bradman made a second ball duck , bowled by a Eric Hollies googly that sent him into retirement with a career average of 99.94 .
England then lost the 1975 series 0 -- 1 , but at least restored some pride under new captain Tony Greig .
Australia won the 1977 Centenary Test which was not an Ashes contest , but then a storm broke as Kerry Packer announced his intention to form World Series Cricket .
England , despite being 135 for 7 , produced a second innings total of 356 , Botham scoring 149* .
In 1985 David Gower 's England team was strengthened by the return of Gooch and Emburey as well as the emergence at international level of Tim Robinson and Mike Gatting .
Mike Gatting was the captain in 1986 -- 87 but his team started badly and attracted some criticism .
Well led by Allan Border , the team included the young cricketers Mark Taylor , Merv Hughes , David Boon , Ian Healy and Steve Waugh , who were all to prove long-serving and successful Ashes competitors .
England , now led once again by David Gower , suffered from injuries and poor form .
The wicketkeeper-batsman position was held by Ian Healy for most of the 1990s and by Adam Gilchrist from 2001 to 2006 -- 07 .
This elevated them to second in the ICC Test Championship .
Experienced journalists including Richie Benaud rated the series as the most exciting in living memory .
Damien Martyn also retired during the series .
England then achieved their first Ashes win at Lord 's since 1934 to go 1-0 up .
A team must win a series to gain the right to hold the Ashes .
A drawn series results in the previous holders retaining the Ashes .
The Ashes is one of the most fiercely contested competitions in cricket .
The best-known and longest-running of these events is the rugby league contest between Great Britain and Australia ( see The Ashes ( rugby league ) ) .
The urn is also featured in the science fiction comedy novel Life , the Universe and Everything , the third " Hitchhiker 's Guide To The Galaxy " book by Douglas Adams .
The Ashes featured in the film The Final Test , released in 1953 , based on a television play by Terence Rattigan .
