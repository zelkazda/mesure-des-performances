In the mid-1990s , after writing , directing , and starring in the independent film Sling Blade , he won an Academy Award for Best Writing .
Thornton graduated high school in 1973 .
A good high school baseball player , he tried out for the Kansas City Royals , but was let go after an injury .
Thornton initially had a difficult time succeeding as an actor , and worked in telemarketing , offshore wind farming , and fast food management between auditioning for acting jobs .
While Thornton worked as a waiter for an industry event , he served film director and screenwriter Billy Wilder , who is famous for films such as Double Indemnity and Sunset Boulevard .
Thornton struck up a conversation with Wilder , who advised Thornton to consider a career as a screenwriter , for which he eventually won an Oscar in the category of best screenplay .
His role as the villain in 1992 's One False Move , which he also co-wrote , brought him to the attention of critics .
Thornton put Wilder 's advice to good use , and went on to write , direct and star in the independent film Sling Blade , which was released in 1996 .
The film , an expansion of a short film titled Some Folks Call It a Sling Blade , introduced the story of Karl Childers , a mentally handicapped man imprisoned for a gruesome and seemingly inexplicable murder .
Sling Blade garnered international acclaim .
The negative experience ( he was forced to cut more than an hour ) led to his decision to never direct another film ( a subsequent release , Daddy and Them , had been filmed earlier ) .
During the late 1990s , Thornton , who has had a life-long love for music , began a career as a singer-songwriter .
He released a roots rock album titled Private Radio in 2001 , and two more albums , The Edge of the World ( 2003 ) and Hobo ( 2005 ) .
Thornton 's manager , David Spero , helped his Edge of the World album get off the ground with a summer tour .
Thornton 's screen persona has been described by the press as that of a " tattooed , hirsute man 's man " .
Thornton has stated that , following Bad Santa 's success , audiences " like to watch [ him ] play that kind of guy, " and " they [ casting directors ] call [ him ] up when they need an asshole .
In 2004 he played Davy Crockett in The Alamo .
He appeared in the comic film School for Scoundrels , which was released on September 29 , 2006 .
In the film , he plays a self-help doctor ; the role was written specifically for Thornton .
In September 2008 , Thornton starred in the big brother action movie Eagle Eye along side Shia LaBeouf and Michelle Monaghan .
He will next star in the drama Peace Like a River .
Thornton received a star on the Hollywood Walk of Fame on October 7 , 2004 .
On April 8 , 2009 , Thornton and his musical group The Boxmasters appeared on CBC Radio One program Q , interviewed by Jian Ghomeshi in which Thornton 's responses were either incomprehensible or discourteous .
Eventually , Thornton explained he had " instructed " the show 's producers to not ask questions about his movie career .
Thornton has frequently disclosed that he has obsessive -- compulsive disorder .
He and rock singer Warren Zevon became close friends after sharing their common experiences with the disorder .
In a 2004 interview with The Independent , Thornton explained : " It 's just that I wo n't use real silver .
The Louis XIV type .
Thornton has been married five times , most notably to actress Angelina Jolie .
Each of Thornton 's marriages ended in divorce .
Thornton and Jolie were known for their eccentric behavior , which reportedly included wearing vials of each other 's blood around their necks ; Thornton later clarified that the " vials " were , instead , two small lockets , each containing only a single drop of blood .
Thornton has stated that he will likely not marry again , specifying that he believes marriage " does n't work " for him .
