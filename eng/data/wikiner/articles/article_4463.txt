Contrary to the view popularised by Dan Brown 's novel The Da Vinci Code , there is no evidence to suggest that the Biblical canon , the list of books decided to be authorative as scripture , was even discussed at the Council of Nicaea , let alone established or edited .
Another result of the council was an agreement on when to celebrate the Easter , the most important feast of the ecclesiastical calendar .
It authorized the Bishop of Alexandria to announce annually the exact date to his fellow bishops .
To most bishops , the teachings of Arius were heretical and dangerous to the salvation of souls .
In the summer of 325 , the bishops of all provinces were summoned to Nicaea ( now known as İznik , in modern-day Turkey ) , a place easily accessible to the majority of delegates , particularly those of Asia Minor , Syria , Palestine , Egypt , Greece , and Thrace .
Eusebius speaks of an almost innumerable host of accompanying priests , deacons and acolytes .
In these discussions , some dominant figures were Arius , with several adherents .
This profession of faith was adopted by all the bishops " but two from Libya who had been closely associated with Arius from the beginning . "
The exact meaning of many of the words used in the debates at Nicaea were still unclear to speakers of other languages .
Further on it says " That they all may be one ; as thou , Father , art in me , and I in thee , that they also may be one in us : that the world may believe that thou hast sent me " ; John 17:21 .
Several creeds were already in existence ; many creeds were acceptable to the members of the council , including Arius .
The text of this profession of faith is preserved in a letter of Eusebius to his congregation , in Athanasius , and elsewhere .
Hosius stands at the head of the lists of bishops , and Athanasius ascribes to him the actual formulation of the creed .
The initial number of bishops supporting Arius was small .
Meletius , it was decided , should remain in his own city of Lycopolis in Egypt , but without exercising authority or the power to ordain new clergy ; he was forbidden to go into the environs of the town or to enter another diocese for the purpose of ordaining its subjects .
As to Meletius himself , episcopal rights and prerogatives were taken from him .
The twenty as listed in the Nicene and Post-Nicene Fathers are as follows :
The long-term effects of the Council of Nicaea were significant .
