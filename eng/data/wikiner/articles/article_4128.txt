The collective was officially founded in Denver , Colorado by childhood friends Robert Schneider , Bill Doss , Will Cullen Hart , and Jeff Mangum .
It was Schneider who created the E6 record label when he moved to Denver , Colorado in late 1991 to attend the University of Colorado at Boulder .
There , he and new friends founded The Apples ( which eventually became The Apples in Stereo ) .
Recorded by April 1993 , The Apples ' Tidal Wave 7 " EP was the first E6 release .
They released California Demise as their first recording , and E6 's second .
Several Elephant 6 projects began to find commercial success in the late 1990s , including Beulah , Dressy Bessy , Elf Power , The Music Tapes , and of Montreal , as well as the founding bands .
Most of the bands subsequently signed with major record labels ; E6 as an entity slowly deteriorated until the collective called it quits -- due to recording difficulties and lack of organization -- in 2002 .
Though most of the collective 's members span the country , many live together on the Orange Twin Conservation Community in Athens .
The term " Elephant 6 " has since come to refer to a broad range of bands and spin-off projects that the record label has spawned .
The Elephant 6 logo has become a symbol for the circle of friends sharing similar ideas and goals .
In 2007 The Apples in Stereo featured the Elephant 6 logo on their album New Magnetic Wonder , announcing " The Elephant 6 Recording Company re-opens our doors and windows , and invites the world : join together with your friends and make something special , something meaningful , something to remember when you are old .
In 2008 , the Elephant 6 logo was also used when Julian Koster released his long awaited Music Tapes For Clouds and Tornadoes under the name The Music Tapes .
This ensemble tour was widely seen as a resurgence of Elephant 6 as a productive , cohesive entity .
On March 4 , 2010 Robert Schneider along with The Apples in Stereo launched stepthroughtheportal.com , an interactive website where the user can explore the past , present and future of The Apples in Stereo .
The site incudes a short film featuring Elijah Wood , free song , time machine streaming songs from their past album and more .
