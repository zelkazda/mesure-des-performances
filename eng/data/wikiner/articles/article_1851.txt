The boundaries of the triangle cover the Straits of Florida , the Bahamas and the entire Caribbean island area and the Atlantic east to the Azores .
Sand 's article was the first to suggest a supernatural element to the Flight 19 incident .
Lloyd 's of London determined that large numbers of ships had not sunk there .
United States Coast Guard records confirm their conclusion .
The Coast Guard is also officially skeptical of the Triangle , noting that they collect and publish , through their inquiries , much documentation contradicting many of the incidents written about by the Triangle authors .
This has led to the production of vast amounts of material on topics such as the Bermuda Triangle .
Finally , if the Triangle is assumed to cross land , such as parts of Puerto Rico , the Bahamas , or Bermuda itself , there is no evidence for the disappearance of any land-based vehicles or persons .
Triangle writers have used a number of supernatural concepts to explain the events .
One explanation pins the blame on leftover technology from the mythical lost continent of Atlantis .
Sometimes connected to the Atlantis story is the submerged rock formation known as the Bimini Road off the island of Bimini in the Bahamas , which is in the Triangle by some definitions .
Followers of the purported psychic Edgar Cayce take his prediction that evidence of Atlantis would be found in 1968 as referring to the discovery of the Bimini Road .
This idea was used by Steven Spielberg for his science fiction film Close Encounters of the Third Kind , which features the lost Flight 19 aircrews as alien abductees .
Charles Berlitz , author of various books on anomalous phenomena , lists several theories attributing the losses in the Triangle to anomalous or unexplained forces .
Compass problems are one of the cited phrases in many Triangle incidents .
But the public may not be as informed , and think there is something mysterious about a compass " changing " across an area as large as the Triangle , which it naturally will .
Whether deliberate or accidental , humans have been known to make mistakes resulting in catastrophe , and losses within the Bermuda Triangle are no exception .
These storms have in the past caused a number of incidents related to the Triangle .
However , according to another of their papers , no large releases of gas hydrates are believed to have occurred in the Bermuda Triangle for the past 15,000 years .
Flight 19 was a training flight of TBM Avenger bombers that went missing on December 5 , 1945 while over the Atlantic .
Adding to the intrigue is that the Navy 's report of the accident was ascribed to " causes or reasons unknown . "
Adding to the mystery , a search and rescue Mariner aircraft with a 13-man crew was dispatched to aid the missing squadron , but the Mariner itself was never heard from again .
The mysterious abandonment in 1872 of the 282-ton brigantine Mary Celeste is often but inaccurately connected to the Triangle , the ship having been abandoned off the coast of Portugal .
Theodosia Burr Alston was the daughter of former United States Vice President Aaron Burr .
Her disappearance has been cited at least once in relation to the Triangle .
The planned route is well outside all but the most extended versions of the Bermuda Triangle .
S.V. Spray was a derelict fishing boat refitted as an ocean cruiser by Joshua Slocum and used by him to complete the first ever single-handed circumnavigation of the world , between 1895 and 1898 .
Neither he nor Spray were ever seen again .
There is no evidence they were in the Bermuda Triangle when they disappeared , nor is there any evidence of paranormal activity .
The boat was considered in poor condition and a hard boat to handle that Slocum 's skill usually overcame .
A five-masted schooner built in 1919 , the Carroll A. Deering was found hard aground and abandoned at Diamond Shoals , near Cape Hatteras , North Carolina on January 31 , 1921 .
On December 28 , 1948 , a Douglas DC-3 aircraft , number NC16002 , disappeared while on a flight from San Juan , Puerto Rico , to Miami .
Both were Avro Tudor IV passenger aircraft operated by British South American Airways .
One plane was not heard from long before it would have entered the Triangle .
On August 28 , 1963 a pair of US Air Force KC-135 Stratotanker aircraft collided and crashed into the Atlantic .
The Triangle version of this story specifies that they did collide and crash , but there were two distinct crash sites , separated by over 160 miles ( 260 km ) of water .
A pleasure yacht was found adrift in the Atlantic south of Bermuda on September 26 , 1955 ; it is usually stated in the stories that the crew vanished while the yacht survived being at sea during three hurricanes .
The plane was witnessed by many air traffic controllers in Cockburn 's airport to circle the island for 30 minutes , after which , it flew away apparently for another island .
Some incidents mentioned as having taken place within the Triangle are found only in these sources :
The following websites have either online material that supports the popular version of the Bermuda Triangle , or documents published from official sources as part of hearings or inquiries , such as those conducted by the United States Navy or United States Coast Guard .
These books are often the only source material for some of the incidents that have taken place within the Triangle .
