Liebig and Wöhler were already able to find three decomposition products of the newly discovered amygdalin : sugar , benzaldehyde , and prussic acid ( hydrogen cyanide ) .
In 1972 , Memorial Sloan-Kettering Cancer Center ( MSKCC ) board member Benno C. Schmidt , Sr. convinced the hospital to test laetrile so that he could assure others of its ineffectiveness " with some conviction . "
After testing , the scientist in charge Kanematsu Sugiura , found that laetrile inhibited secondary tumors in mice though it did not destroy the primary tumors .
Given this collection of results , MSKCC concluded that " laetrile showed no beneficial effects . "
In 1974 , the American Cancer Society officially labelled laetrile as quackery , but advocates for laetrile dispute this label , asserting that financial motivations have tainted the published research .
Vale was convicted in 2003 for , among other things , marketing laetrile .
As a result , Vale was ordered to reimburse the government $ 31,000 for the costs of his appointed defense attorney .
The US Food and Drug Administration continues to seek jail sentences for vendors selling laetrile for cancer treatment , calling it a " highly toxic product that has not shown any effect on treating cancer . "
A 2006 systematic review by the Cochrane Collaboration concluded : " The claim that [ l ] aetrile has beneficial effects for cancer patients is not supported by data from controlled clinical trials .
Given the lack of evidence , laetrile has not been approved by the U.S. Food and Drug Administration .
The U.S. National Institutes of Health evaluated the evidence separately and concluded that clinical trials of amgydalin showed little or no effect against cancer .
