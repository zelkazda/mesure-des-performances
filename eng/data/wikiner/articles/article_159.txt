He and his younger brother Vladimir Andreevich Markov ( 1871 -- 1897 ) proved Markov brothers ' inequality .
His son , another Andrey Andreevich Markov ( 1903 -- 1979 ) , was also a notable mathematician , making contributions on constructive mathematics and recursive function theory .
As a 17-year-old grammar school student he informed Bunyakovsky , Korkin and Yegor Zolotarev about an apparently new method to solve linear ordinary differential equations and was invited to the so-called Korkin Saturdays , where Korkin 's students regularly met .
In 1874 he finished the school and began his studies at the physico-mathematical faculty of St Petersburg University .
Later he lectured alternately on " introduction to analysis " , probability theory ( succeeding Chebyshev who had left the university in 1882 ) and calculus of differences .
In 1890 , after the death of Viktor Bunyakovsky , Markov became extraordinary member of the academy .
His promotion to an ordinary professor of St Petersburg University followed in autumn 1894 .
In 1896 , he was elected ordinary member of the academy as the successor of Chebyshev .
In connection with student riots in 1908 , professors and lecturers of Saint Petersburg University were ordered to observe their students .
Markov initially refused to accept this decree and wrote an explanation in which he declined to be an " agent of the governance " .
Markov was rejected from a further teaching activity at the Saint Petersburg University , and he eventually decided to retire from the university .
In 1913 the council of Saint Petersburg elected nine scientists honorary members of the university .
Markov was among them , but his election was not affirmed by the minister of education .
Markov then resumed his teaching activities and lectured probability theory and calculus of differences until his death in 1922 .
In 1912 Markov in protest to Leo Tolstoy 's excommunication from the Russian Orthodox Church requested that he too be excommunicated .
