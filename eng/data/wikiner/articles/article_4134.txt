This was proved by Gabriel Lamé in 1844 , and marks the beginning of computational complexity theory .
In Euclid 's original version of the algorithm , the quotient and remainder are found by repeated subtraction ; that is , r k −1 is subtracted from r k −2 repeatedly until the remainder r k is smaller than r k −1 .
In the subtraction-based version defined by Euclid , the remainder calculation ( b = a mod b ) is replaced by repeated subtraction .
The algorithm was probably not discovered by Euclid , who compiled results from earlier mathematicians in his Elements .
Finite fields are often called Galois fields , and are abbreviated as GF ( p ) or GF ( p m ) .
As shown first by Gabriel Lamé in 1844 , the number of steps required for completion is never more than five times the number h of digits ( base 10 ) of the smaller number b .
For comparison , Euclid 's original subtraction-based algorithm can be much slower .
Euclid uses this algorithm to treat the question of incommensurable lengths .
Lamé 's approach required the unique factorization of numbers of the form x + ω y , where x and y are integers , and ω = e 2 i π/ n is an n th root of 1 , that is , ω n = 1 .
