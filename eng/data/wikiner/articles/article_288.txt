Gaius Julius Caesar Augustus was the first ruler of the Roman Empire , which he ruled alone from January 27 BC until his death .
Born Gaius Octavius Thurinus , he was adopted posthumously by his great-uncle Gaius Julius Caesar in 44 BC , and between then and 31 BC was officially named Gaius Julius Caesar .
Because of the various names he bore , it is common to call him Octavius when referring to events between 63 and 44 BC , Octavian ( or Octavianus ) when referring to events between 44 and 27 BC , and Augustus when referring to events after 27 BC .
In 43 BC , Octavian joined forces with Mark Antony and Marcus Aemilius Lepidus in a military dictatorship known as the Second Triumvirate .
It took several years to determine the exact framework by which a formally republican state could be led by a sole ruler ; the result became known as the Roman Empire .
The rule of Augustus initiated an era of relative peace known as the Pax Romana , or Roman peace .
Augustus expanded the Roman Empire , secured its boundaries with client states , and made peace with Parthia through diplomacy .
He was succeeded by his stepson , former son-in-law and adopted son , Tiberius .
While his paternal family was from the town of Velitrae , about 25 miles from Rome , Augustus was born in the city of Rome on 23 September 63 BC .
He was given the name Gaius Octavius Thurinus , his cognomen possibly commemorating his father 's victory at Thurii over a rebellious band of slaves .
Due to the crowded nature of Rome at the time , Octavius was taken to his father 's home village at Velitrae to be raised .
Octavius only mentions his father 's equestrian family briefly in his memoirs .
His paternal great-grandfather was a military tribune in Sicily during the Second Punic War .
His mother Atia was the niece of Julius Caesar .
His mother married a former governor of Syria , Lucius Marcius Philippus .
Because of this , Octavius was raised by his grandmother ( and Julius Caesar 's sister ) , Julia Caesaris .
In 52 or 51 BC , Julia Caesaris died .
Octavius delivered the funeral oration for his grandmother .
Upon his adoption , Octavius assumed his great-uncle 's name , Gaius Julius Caesar .
After Decimus Brutus refused to give up Cisalpine Gaul , Antony besieged him at Mutina .
Cicero also defended Octavian against Antony 's taunts about Octavian 's lack of noble lineage ; he stated " we have no more brilliant example of traditional piety among our youth . "
This was in part a rebuttal to Antony 's opinion of Octavian , as Cicero quoted Antony saying to Octavian , " You , boy , owe everything to your name . "
In addition , Octavian was granted imperium ( commanding power ) , which made his command of troops legal , sending him to relieve the siege along with Hirtius and Pansa ( the consuls for 43 BC ) .
In April of 43 BC , Antony 's forces were defeated at the battles of Forum Gallorum and Mutina , forcing Antony to retreat to Transalpine Gaul .
However , both consuls were killed , leaving Octavian in sole command of their armies .
Instead , Octavian stayed in the Po Valley and refused to aid any further offensive against Antony .
Octavian also demanded that the decree declaring Antony a public enemy should be rescinded .
He encountered no military opposition in Rome , and on 19 August 43 BC was elected consul with his relative Quintus Pedius as co-consul .
In a meeting near Bologna in October of 43 BC , Octavian , Antony , and Lepidus formed a junta called the Second Triumvirate .
This explicit arrogation of special powers lasting five years was then supported by law passed by the plebs , unlike the unofficial First Triumvirate formed by Gnaeus Pompey Magnus , Julius Caesar and Marcus Licinius Crassus .
The estimation that 300 senators were proscribed was presented by Appian , although his earlier contemporary Livy asserted that only 130 senators had been proscribed .
Marcus Velleius Paterculus asserted that Octavian tried to avoid proscribing officials whereas Lepidus and Antony were to blame for initiating them .
Cassius Dio defended Augustus as trying to spare as many as possible , whereas Antony and Lepidus , being older and involved in politics longer , had many more enemies to deal with .
This claim was rejected by Appian , who maintained that Octavian shared an equal interest with Lepidus and Antony in eradicating his enemies .
Suetonius presents the case that Octavian , although reluctant at first to proscribe officials , nonetheless pursued his enemies with more rigor than the other triumvirs .
Plutarch describes the proscriptions as a ruthless and cutthroat swapping of friends and family between Antony , Lepidus , and Octavian .
Mark Antony would later use the examples of these battles as a means to belittle Octavian , as both battles were decisively won with the use of Antony 's forces .
In addition to claiming responsibility for both victories , Antony also branded Octavian as a coward for handing over his direct military control to Marcus Vipsanius Agrippa instead .
After Philippi , a new territorial arrangement was made among the members of the Second Triumvirate .
Lepidus was left with the province of Africa , stymied by Antony who conceded Hispania to Octavian instead .
The tens of thousands who had fought on the republican side with Brutus and Cassius , who could easily ally with a political opponent of Octavian if not appeased , also required land .
Meanwhile , Octavian asked for a divorce from Clodia Pulchra , the daughter of Fulvia and her first husband Publius Clodius Pulcher .
Fulvia decided to take action .
Lucius and his allies ended up in a defensive siege at Perusia ( modern Perugia ) , where Octavian forced them into surrender in early 40 BC .
Perusia was also pillaged and burned as a warning for others .
Scribonia conceived Octavian 's only natural child , Julia , who was born the same day that he divorced Scribonia to marry Livia Drusilla , little more than a year after his marriage .
However , this new conflict proved untenable for both Octavian and Antony .
Meanwhile in Sicyon , Antony 's wife Fulvia died of a sudden illness while Antony was en route to meet her .
Fulvia 's death and the mutiny of their centurions allowed the two remaining triumvirs to effect a reconciliation .
To further cement relations of alliance with Mark Antony , Octavian gave his sister , Octavia Minor , in marriage to Antony in late 40 BC .
The territorial agreement amongst the triumvirs and Sextus Pompeius began to crumble once Octavian divorced Scribonia and married Livia on 17 January 38 BC .
In supporting Octavian , Antony expected to gain support for his own campaign against Parthia , desiring to avenge Rome 's defeat at Carrhae in 53 BC .
However , Octavian sent only a tenth the number of those promised , which was viewed by Antony as an intentional provocation .
Despite setbacks for Octavian , the naval fleet of Sextus Pompeius was almost entirely destroyed on 3 September by general Agrippa at the naval battle of Naulochus .
However , Lepidus ' troops deserted him and defected to Octavian since they were weary of fighting and found Octavian 's promises of money to be enticing .
Meanwhile , Antony 's campaign against Parthia turned disastrous , tarnishing his image as a leader , and the mere 2,000 legionaries sent by Octavian to Antony were hardly enough to replenish his forces .
In 36 BC , Octavian used a political ploy to make himself look less autocratic and Antony more the villain by proclaiming that the civil wars were coming to an end , and that he would step down as triumvir if only Antony would do the same ; Antony refused .
While Agrippa cut off Antony and Cleopatra 's main force from their supply routes at sea , Octavian landed on the mainland opposite the island of Corcyra ( modern Corfu ) and marched south .
It was there that Antony 's fleet faced the much larger fleet of smaller , more maneuverable ships under commanders Agrippa and Gaius Sosius in the battle of Actium on 2 September 31 BC .
Antony and his remaining forces were only spared due to a last-ditch effort by Cleopatra 's fleet that had been waiting nearby .
Octavian pursued them , and after another defeat in Alexandria on 1 August 30 BC , Antony and Cleopatra committed suicide ; Antony fell on his own sword and into Cleopatra 's arms , while she let a poisonous snake bite her .
Octavian 's aims from this point forward were to return Rome to a state of stability , traditional legality and civility by lifting the overt political pressure imposed on the courts of law and ensuring free elections in name at least .
Although Octavian was no longer in direct control of the provinces and their armies , he retained the loyalty of active duty soldiers and veterans alike .
The careers of many clients and adherents depended on his patronage , as his financial power in the Roman Republic was unrivaled .
To a large extent the public was aware of the vast financial resources Augustus commanded .
The senate 's proposal was a ratification of Octavian 's extra-constitutional power .
Through the senate Octavian was able to continue the appearance of a still-functional constitution .
Moreover , command of these provinces provided Octavian with control over the majority of Rome 's legions .
While Octavian acted as consul in Rome , he dispatched senators to the provinces under his command as his representatives to manage provincial affairs and ensure his orders were carried out .
Octavian became the most powerful political figure in the city of Rome and in most of its provinces , but did not have sole monopoly on political and martial power .
After the harsh methods employed in consolidating his control , the change in name would also serve to demarcate his benign reign as Augustus from his reign of terror as Octavian .
His new title of Augustus was also more favorable than Romulus , the previous one he styled for himself in reference to the story of Romulus and Remus ( founders of Rome ) , which would symbolize a second founding of Rome .
However , the title of Romulus was associated too strongly with notions of monarchy and kingship , an image Octavian tried to avoid .
Augustus was granted the right to hang the corona civica , the " civic crown " made from oak , above his door and have laurels drape his doorposts .
However , Augustus renounced flaunting insignia of power such as holding a scepter , wearing a diadem , or wearing the golden crown and purple toga of his predecessor Julius Caesar .
In the late spring Augustus suffered a severe illness , and on his supposed deathbed made arrangements that would put in doubt the senators ' suspicions of his anti-republicanism .
Augustus prepared to hand down his signet ring to his favored general Agrippa .
This was a surprise to many who believed Augustus would have named an heir to his position as an unofficial emperor .
Soon after his bout of illness subsided , Augustus gave up his permanent consulship .
The only other times Augustus would serve as consul would be in the years 5 and 2 BC .
This was a clever ploy by Augustus ; by stepping down as one of two consuls , this allowed aspiring senators a better chance to fill that position , while at the same time Augustus could " exercise wider patronage within the senatorial class . "
Augustus was also granted the power of a tribune ( tribunicia potestas ) for life , though not the official title of tribune .
Legally it was closed to patricians , a status that Augustus had acquired years ago when adopted by Julius Caesar .
In addition to tribunician authority , Augustus was granted sole imperium within the city of Rome itself : all armed forces in the city , formerly under the control of the prefects and consuls , were now under the sole authority of Augustus .
In 19 BC , Lucius Cornelius Balbus , governor of Africa and conqueror of the Garamantes , was the first man of provincial origin to receive this award , as well as the last .
In 22 , 21 , and 19 BC , the people rioted in response , and only allowed a single consul to be elected for each of those years , ostensibly to leave the other position open for Augustus .
In 22 BC there was a food shortage in Rome which sparked panic , while many urban plebs called for Augustus to take on dictatorial powers to personally oversee the crisis .
This seems to have assuaged the populace ; regardless of whether or not Augustus was actually a consul , the importance was that he appeared as one before the people .
On 5 February 2 BC , Augustus was also given the title pater patriae , or " father of the country " .
By the year 13 , Augustus boasted 21 occasions where his troops proclaimed " imperator " as his title after a successful battle .
By the end of his reign , the armies of Augustus had conquered northern Hispania ( modern Spain and Portugal ) , the Alpine regions of Raetia and Noricum , Illyricum and Pannonia , and extended the borders of the Africa Province to the east and south .
After the reign of the client king Herod the Great ( 73 -- 4 BC ) , Judea was added to the province of Syria when Augustus deposed his successor Herod Archelaus .
When the rebellious tribes of Cantabria in modern-day Spain were finally quelled in 19 BC , the territory fell under the provinces of Hispania and Lusitania .
The poet Horace dedicated an ode to the victory , while the monument Trophy of Augustus near Monaco was built to honor the occasion .
It was recorded that the pious Tiberius walked in front of his brother 's body all the way back to Rome .
To protect Rome 's eastern territories from the Parthian Empire , Augustus relied on the client states of the east to act as territorial buffers and areas which could raise their own troops for defense .
Tiberius was responsible for restoring Tigranes V to the throne of Armenia .
Although Parthia always posed a threat to Rome in the east , the real battlefront was along the Rhine and Danube rivers .
Victory in battle was not always a permanent success , as newly conquered territories were constantly retaken by Rome 's enemies in Germania .
This union produced five children , three sons and two daughters : Gaius Caesar , Lucius Caesar , Vipsania Julia , Agrippina the Elder , and Postumus Agrippa , so named because he was born after Marcus Agrippa died .
Augustus also showed favor to his stepsons , Livia 's children from her first marriage , Nero Claudius Drusus Germanicus and Tiberius Claudius , granting them military commands and public office , and seeming to favor Drusus .
In that year , Tiberius was also granted the powers of a tribune and proconsul , emissaries from foreign kings had to pay their respects to him , and by 13 was awarded with his second triumph and equal level of imperium with that of Augustus .
Postumus Agrippa was murdered at his place of exile either shortly before or after the death of Augustus .
There are a few known written works by Augustus that have survived .
However , historians are able to analyze existing letters penned by Augustus to others for additional facts or clues about his personal life .
He was intelligent , decisive , and a shrewd politician , but he was not perhaps as charismatic as Julius Caesar , and was influenced on occasion by his third wife , Livia ( sometimes for the worse ) .
The city of Rome was utterly transformed under Augustus , with Rome 's first institutionalized police force , fire fighting force , and the establishment of the municipal prefect as a permanent office .
With Rome 's civil wars at an end , Augustus was also able to create a standing army for the Roman Empire , fixed at a size of 28 legions of about 170,000 soldiers .
In the year 6 Augustus established the aerarium militare , donating 170 million sesterces to the new military treasury that provided for both active and retired soldiers .
Although the most powerful individual in the Roman Empire , Augustus wished to embody the spirit of Republican virtue and norms .
In the year 29 BC , Augustus paid 400 sesterces each to 250,000 citizens , 1,000 sesterces each to 120,000 veterans in the colonies , and spent 700 million sesterces in purchasing land for his soldiers to settle upon .
Had Augustus died earlier , matters might have turned out differently .
However , for his rule of Rome and establishing the principate , Augustus has also been subjected to criticism throughout the ages .
Tacitus , however , records two contradictory but common views of Augustus :
Tacitus was of the belief that Nerva ( r. 96 -- 98 ) successfully " mingled two formerly alien ideas , principate and liberty . "
The 3rd century historian Cassius Dio acknowledged Augustus as a benign , moderate ruler , yet like most other historians after the death of Augustus , Dio viewed Augustus as an autocrat .
In his criticism of Augustus , the admiral and historian Thomas Gordon ( 1658 -- 1741 ) compared Augustus to the puritanical tyrant Oliver Cromwell ( 1599 -- 1658 ) .
Thomas Gordon and the French political philosopher Montesquieu ( 1689 -- 1755 ) both remarked that Augustus was a coward in battle .
This reform greatly increased Rome 's net revenue from its territorial acquisitions , stabilized its flow , and regularized the financial relationship between Rome and the provinces , rather than provoking fresh resentments with each new arbitrary exaction of tribute .
The measures of taxation in the reign of Augustus were determined by population census , with fixed quotas for each province .
Private contractors that raised taxes had been the norm in the Republican era , and some had grown powerful enough to influence the amount of votes for politicians in Rome .
Rome 's revenue was the amount of the successful bids , and the tax farmers ' profits consisted of any additional amounts they could forcibly wring from the populace with Rome 's blessing .
The month of August is named after Augustus ; until his time it was called Sextilis .
Commonly repeated lore has it that August has 31 days because Augustus wanted his month to match the length of Julius Caesar 's July , but this is an invention of the 13th century scholar Johannes de Sacrobosco .
Sextilis in fact had 31 days before it was renamed , and it was not chosen for its length ( see Julian calendar ) .
According to a senatus consultum quoted by Macrobius , Sextilis was renamed to honor Augustus because several of the most significant events in his rise to power , culminating in the fall of Alexandria , fell in that month .
On his deathbed , Augustus boasted " I found Rome of bricks ; I leave it to you of marble " .
Marble could be found in buildings of Rome before Augustus , but it was not extensively used as a building material until the reign of Augustus .
He also built the Temple of Caesar , the Baths of Agrippa , and the Forum of Augustus with its Temple of Mars Ultor .
Even his Mausoleum of Augustus was built before his death to house members of his family .
After the death of Agrippa in 12 BC , a solution had to be found in maintaining Rome 's water supply system .
This came about because it was overseen by Agrippa when he served as aedile , and was even funded by him afterwards when he was a private citizen paying at his own expense .
Augustus created the senatorial group of the curatores viarum for the upkeep of roads ; this senatorial commission worked with local officials and contractors to organize regular repairs .
Augustus 's only child was his daughter .
Whether there were any descendants of Augustus after the early decades of the 3rd century , is unknown .
