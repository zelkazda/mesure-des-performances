The study of flags is known as vexillology , from the Latin vexillum meaning flag or banner .
The usage of flags spread from India and China , where they were almost certainly invented , to neighboring Burma , Siam , and southeastern Asia .
Several countries ( including the United Kingdom and the Soviet Union ) have had unique flags flown by their armed forces , rather than the national flag .
Other countries ' armed forces ( such as those of the United States or Switzerland ) use their standard national flag .
The Philippines ' armed forces may use their standard national flag , but during times of war the flag is turned upside down -- the only known case where an upside down national flag signifies a state of war ( and not merely distress . )
Although it is reported to have been certified by a representative of Guinness World Records , it is not yet shown as adjudicated on their website .
On many Australian beaches there is a slight variation with beach condition signaling .
A red flag signifies a closed beach ( or , in the UK , some other danger ) , yellow signifies strong current or difficult swimming conditions , and green represents a beach safe for general swimming .
In Ireland , a red and yellow flag indicates that it is safe to swim ; a red flag that it is unsafe ; and no flag indicates that there are no lifeguards on duty .
Signal flag " India " ( a black circle on a yellow square ) is frequently used to denote a " blackball " zone where surfboards can not be used but other water activities are permitted .
Some of these political flags have become national flags , such as the red flag of the Soviet Union and national socialist banners for Nazi Germany .
The highest flagpole in the world , at 160 m ( 525 ft ) , is that at Gijeong-dong in North Korea , the flag weighing about 270 kilograms ( 600 pounds ) when dry .
Since 2008 with 133 m ( 436 ft ) the tallest free-standing flagpole in the world is the Ashgabat Flagpole in Turkmenistan , beating the formerly record holding Aqaba Flagpole in Jordan ( size : 132 m ; 433 ft ) .
The Raghadan Flagpole in Amman is currently the third tallest free-standing flagpole in the world .
