From the 10th century to the 16th century Holland proper was a unified political region , a county ruled by the Count of Holland .
This spelling variation remained in use until around the 14th century , at which time the name stabilised as Holland .
Popular , but incorrect , etymology [ citation needed ] holds that Holland is derived from hol land ( " hollow land " ) and was inspired by the low-lying geography of Holland .
Today this refers specifically to people from the current provinces of North Holland and South Holland .
A maritime region , Holland lies on the North Sea at the mouths of the Rhine and the Meuse ( Maas ) .
To the south is Zealand .
Holland is protected from the sea by a long line of coastal dunes .
At present the lowest point in Holland is a polder near Rotterdam , which is about seven meters below sea level .
Continuous drainage is necessary to keep Holland from flooding .
The landscape was ( and in places still is ) dotted with windmills , which have become a symbol of Holland .
The main cities in Holland are Amsterdam , Rotterdam and The Hague .
Amsterdam is formally the capital of the Netherlands and its most important city .
The Port of Rotterdam is Europe 's largest and most important harbour and port .
These cities , combined with Utrecht and other smaller municipalities , effectively form a single city -- a conurbation called Randstad .
The Randstad area is one of the most densely populated regions of Europe , but still relatively free of urban sprawl .
The language primarily spoken in Holland is Dutch .
In the south on the island of Goeree-Overflakkee , Zealandic is spoken .
However , to a certain extent at least , the history of Holland is the history of the Netherlands , and vice versa .
See the article on " History of the Netherlands " for a more detailed history .
The land that is now Holland had never been stable .
The main rivers , the Rhine and the Meuse , flooded regularly and changed course repeatedly and dramatically .
The people of Holland found themselves living in an unstable , watery environment .
To the south of Holland , in Zealand , and to the north , in Frisia , this development led to catastrophic storm floods literally washing away entire regions , as the peat layer disintegrated or became detached and was carried away by the flood water .
This inland sea threatened to link up with the " drowned lands " of Zealand in the south , reducing Holland to a series of narrow dune barrier islands in front of a lagoon .
As a result , historical maps of mediaeval and early modern Holland bear little resemblance to the maps of today .
This ongoing struggle to master the water played an important role in the development of Holland as a maritime and economic power and in the development of the character of the people of Holland .
The area was part of Frisia .
In this time a part of Frisia , West Friesland , was conquered .
In the 16th century the county became the most densely urbanised region in Europe , with the majority of the population living in cities .
The dialect of urban Holland became the standard language .
The formation of the Batavian Republic , inspired by the French revolution , led to a more centralised government .
Holland became a province of a unitary state .
From 1806 to 1810 Napoleon styled his vassal state , governed by his brother Louis Napoleon and shortly by the son of Louis , Napoleon Louis Bonaparte , as the " Kingdom of Holland " .
After 1813 , Holland was restored as a province of the United Kingdom of the Netherlands .
Holland was divided into the present provinces North Holland and South Holland in 1840 , after the Belgian Revolution of 1830 .
This is a reaction to the perceived threat that Holland poses to the identities and local cultures of the other provinces .
They take Holland 's cultural dominance for granted .
Holland tends to be associated with a particular image .
The stereotypical image of Holland is an artificial amalgam of tulips , windmills , clogs , cheese and traditional dress ( klederdracht ) .
As is the case with many stereotypes , this is far from the truth and reality of life in Holland .
