They typically include a sublanguage and accompanying engine for processing queries with interpretive statements analogous to but not the same as SQL .
Example object query languages are OQL , LINQ , JDOQL , JPAQL and others .
The Internet Movie Database is one example .
The Worldwide web can be thought of as a database , albeit one spread across millions of independent computing systems .
Web browsers " process " this data one page at a time , while web crawlers and other software provide the equivalent of database indexes to support search and other activities .
Paradoxically , this allows products that are historically pre-relational , such as PICK and MUMPS , to make a plausible claim to be post-relational .
Some use virtual memory-mapped files to make the native language ( C++ , Java etc. ) objects persistent .
In the United Kingdom , database privacy regulation falls under the Office of the Information Commissioner .
