It was discovered by Robert Hooke and is the functional unit of all known living organisms .
In 1835 , before the final cell theory was developed , Jan Evangelista Purkyně observed small " granules " while looking at the plant tissue through a microscope .
The cell theory , first developed in 1839 by Matthias Jakob Schleiden and Theodor Schwann , states that all organisms are composed of one or more cells , that all cells come from preexisting cells , that vital functions of an organism occur within cells , and that all cells contain the hereditary information necessary for regulating cell functions and for transmitting information to the next generation of cells .
The word cell comes from the Latin cellula , meaning , a small room .
The descriptive term for the smallest living biological structure was coined by Robert Hooke in a book he published in 1665 when he compared the cork cells he saw through his microscope to the small rooms monks lived in .
One is that they came from meteorites ( see Murchison meteorite ) .
