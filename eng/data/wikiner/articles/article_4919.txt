The word geology was first used by Jean-André Deluc in 1778 and introduced as a fixed term by Horace-Bénédict de Saussure in 1779 .
In the Roman period , Pliny the Elder wrote in detail of the many minerals and metals then in practical use , and correctly noted the origin of amber .
Nicolas Steno ( 1638 -- 1686 ) is credited with the law of superposition , the principle of original horizontality , and the principle of lateral continuity : three defining principles of stratigraphy .
William Smith ( 1769 -- 1839 ) drew some of the first geological maps and began the process of ordering rock strata ( layers ) by examining the fossils contained in them .
James Hutton is often viewed as the first modern geologist .
Hutton published a two-volume version of his ideas in 1795 .
Sir Charles Lyell first published his famous book , Principles of Geology , in 1830 .
The book , which influenced the thought of Charles Darwin , successfully promoted the doctrine of uniformitarianism .
Though Hutton believed in uniformitarianism , the idea was not widely accepted at the time .
In Hutton 's words : " the past history of our globe must be explained by what can be seen to be happening now .
Based on principles laid out by William Smith almost a hundred years before the publication of Charles Darwin 's theory of evolution , the principles of succession were developed independently of evolutionary thought .
Transform boundaries , such as the San Andreas fault system , resulted in widespread powerful earthquakes .
This can result in the emplacement of dike swarms , such as those that are observable across the Canadian shield , or rings of dikes around the lava tube of a volcano .
Even older rocks , such as the Acasta gneiss of the Slave craton in northwestern Canada , the oldest known rock in the world have been metamorphosed to the point where their origin is undiscernable without laboratory analysis .
One of these is the Phoenix lander , which analyzed Martian polar soil for water and chemical and mineralogical constituents related to biological processes .
