Edinburgh is the seat of the Scottish Parliament .
In the 2009 mid year population estimates , Edinburgh had a total resident population of 477,660 .
The most famous of these events are the Edinburgh Fringe ( the largest performing arts festival in the world ) , the Edinburgh International Festival , the Edinburgh Military Tattoo , and the Edinburgh International Book Festival .
By the 12th century Edinburgh was well established , founded upon the famous castle rock , the volcanic crag and tail geological feature shaped by 2 million years of glacial activity .
Many of the stone-built structures can still be seen today in the Old Town .
From early times , and certainly from the 14th century , Edinburgh ( like other royal burghs of Scotland ) used armorial devices in many ways , including on seals .
During the Jacobite rising of 1745 , Edinburgh was briefly occupied by Jacobite forces before their march into England .
The city was at the heart of the Scottish Enlightenment and renowned throughout Europe at this time , as a hotbed of talent and ideas and a beacon for progress .
The city is affectionately nicknamed Auld Reekie , because when buildings were heated by coal and wood fires , chimneys would spew thick columns of smoke into the air .
Edinburgh has also been known as Dunedin , deriving from the Scottish Gaelic , Dùn Èideann .
One such example is Castle Rock which forced the advancing icepack to divide , sheltering the softer rock and forming a mile-long tail of material to the east , creating a distinctive crag and tail formation .
Glacial erosion on the northern side of the crag gouged a large valley resulting in the now drained Nor Loch .
This structure , along with a ravine to the south , formed an ideal natural fortress which Edinburgh Castle was built upon .
The residential areas of Marchmont and Bruntsfield are built along a series of drumlin ridges located south of the city centre which were deposited as the glacier receded .
Other viewpoints in the city such as Calton Hill and Corstorphine Hill are similar products of glacial erosion .
The Braid Hills and Blackford Hill are a series of small summits to the south west of the city commanding expansive views over the urban area of Edinburgh and northwards to the Forth .
The Water of Leith Walkway is a mixed use trail that follows the river for 19.6 km ( 12.2 miles ) from Balerno to Leith .
With an average width of 3.2 km ( 2 miles ) the principal objective of the green belt was to contain the outward expansion of Edinburgh and to prevent the agglomeration of urban areas .
Similarly , urban villages such as Juniper Green and Balerno sit on green belt land .
One feature of the green belt in Edinburgh is the inclusion of parcels of land within the city which are designated as green belt even though they do not adjoin the main peripheral ring .
Examples of these independent wedges of green belt include Holyrood Park and Corstorphine Hill .
Like much of the rest of Scotland , Edinburgh has a temperate , maritime climate which is relatively mild despite its northerly latitude .
Winters are especially mild , with daytime temperatures rarely falling below freezing , and compare favourably with places such as Moscow , Labrador and Newfoundland which lie in similar latitudes .
Edinburgh , the capital of Scotland , is divided into areas that generally encompass a park ( sometimes known as " links " ) , a main local street ( i.e. street of local retail shops ) , a high street ( the historic main street , not always the same as the main local street , such as in Corstorphine ) and residential buildings .
In Edinburgh many residences are tenements , although the more southern and western parts of the city have traditionally been more affluent and have a greater number of detached and semi-detached villas .
The historic centre of Edinburgh is divided into two by the broad green swath of Princes Street Gardens .
To the south the view is dominated by Edinburgh Castle , perched atop the extinct volcanic crag , and the long sweep of the Old Town trailing after it along the ridge .
To the north lies Princes Street and the New Town .
The gardens were begun in 1816 on bogland which had once been the Nor Loch .
Probably the most noticeable building here is the circular sandstone building that is the Edinburgh International Conference Centre .
The Old Town has preserved its medieval plan and many Reformation-era buildings .
One end is closed by the castle and the main artery , the Royal Mile , leads away from it ; minor streets ( called closes or wynds ) lead downhill on either side of the main spine in a herringbone pattern .
Due to space restrictions imposed by the narrowness of the " tail " , the Old Town became home to some of the earliest " high rise " residential buildings .
Today there are tours of Edinburgh which take you into the underground city , Edinburgh Vaults .
The New Town was an 18th century solution to the problem of an increasingly crowded Old Town .
In 1766 a competition to design the New Town was won by James Craig , a 22-year-old architect .
The principal street was to be George Street , which follows the natural ridge to the north of the Old Town .
Either side of it are the other main streets of Princes Street and Queen Street .
At the east and west ends are St. Andrew Square and Charlotte Square respectively .
Bute House , the official residence of the First Minister of Scotland , is on the north side of Charlotte Square .
Craig 's original plans included the ornamental Princes Street Gardens and a canal in place of the Nor Loch .
However excess soil from the construction of the buildings was dumped into the loch , creating what is now The Mound and the canal idea was abandoned .
In the mid-19th century the National Gallery of Scotland and Royal Scottish Academy Building were built on The Mound , and tunnels to Waverley Station driven through it .
The New Town was so successful that it was extended greatly .
Today , the literary connection continues , with the area being home to the authors J. K. Rowling , Ian Rankin , and Alexander McCall Smith .
Leith is the port of Edinburgh .
It still retains a separate identity from Edinburgh , and it was a matter of great resentment when , in 1920 , the burgh of Leith was merged into the county of Edinburgh .
Leith also has the Royal Yacht Britannia , berthed behind the Ocean Terminal and Easter Road , the home ground of Hibernian .
The urban area of Edinburgh is almost entirely contained within the City of Edinburgh Council boundary , merging with Musselburgh in East Lothian .
At the United Kingdom Census 2001 , Edinburgh had a population of 448,624 , a rise of 7.1 % on 1991 .
This makes Edinburgh the second largest city in Scotland after Glasgow .
In 2001 , 22 % of the population were born outside Scotland with the largest group of people within this category being born in England at 12.1 % .
This includes some well known faces such as Alistair Darling , the former Chancellor of the Exchequer .
Since the 2004 enlargement of the European Union , a large number of migrants from the accession states such as Poland , Lithuania and Latvia have settled in the city , with many working in the service industry .
There is evidence of human habitation on Castle Rock from as early as 3,000 years ago .
As the population swelled , overcrowding problems in the Old Town , particularly in the cramped tenements that lined the present day Royal Mile and Cowgate , were exacerbated .
The construction of James Craig 's masterplanned New Town from 1766 onwards witnessed the migration of the professional classes from the Old Town to the lower density , higher quality surroundings taking shape on land to the north .
Early 20th century population growth coincided with lower density suburban development in areas such as Gilmerton , Liberton and South Gyle .
Throughout the early to mid 20th century many new estates were built in areas such as Craigmillar , Niddrie , Pilton , Muirhouse , Piershill and Sighthill , linked to slum clearances in the Old Town .
The longest established festival is the Edinburgh International Festival , which first ran in 1947 .
The Edinburgh International Science Festival is held annually in April and is one of the most popular science festivals in the world .
During the street party Princes Street is accessible by ticket only , allowing access into Princes Street where there are live bands playing , food and drink stalls , and a clear view of the castle and fireworks .
On the night of 30 April , the Beltane Fire Festival takes place on Edinburgh 's Calton Hill .
Edinburgh is home to a large number of museums and libraries , many of which are national institutions .
Edinburgh has a long literary tradition , going back to the Scottish Enlightenment .
Writers such as James Boswell , Robert Louis Stevenson , Sir Arthur Conan Doyle , and Sir Walter Scott all lived and worked in Edinburgh .
J K Rowling , author of the Harry Potter novels , is a resident of Edinburgh .
Edinburgh has also become associated with the crime novels of Ian Rankin , and the work of Irvine Welsh , whose novels are mostly set in the city and are often written in colloquial Scots .
Edinburgh is also home to Alexander McCall Smith .
Outside festival season , Edinburgh continues to support a number of theatres and production companies .
Youth Music Theatre : UK has a regional office in the city .
The Usher Hall is Edinburgh 's premier venue for classical music , as well as the occasional prestige popular music gig .
The Scottish Chamber Orchestra is based in Edinburgh .
Edinburgh has a healthy popular music scene .
The city hosts several of Scotland 's galleries and organisations dedicated to contemporary visual art .
In 2010 , PRS for Music listed Edinburgh amongst the UK 's top ten ' most musical ' cities .
Edinburgh is home to Scotland 's five National Galleries as well as numerous smaller galleries .
The national collection is housed in the National Gallery of Scotland , located on the Mound , and now linked to the Royal Scottish Academy , which holds regular major exhibitions of painting .
The contemporary collections are shown in the Scottish National Gallery of Modern Art , and the nearby Dean Gallery .
The Scottish National Portrait Gallery focuses on portraits and photography .
Edinburgh has a large number of pubs , clubs and restaurants .
Stockbridge and the waterfront at Leith are also increasingly fashionable areas , with a number of pubs , clubs and restaurants .
A fortnightly publication , The List , is dedicated to life in Edinburgh and around , and contains listings of all nightclubs , as well as music , theatrical and other events .
Princes Street is the main shopping area in the city centre , with a wide range of stores from souvenir shops , from chains such as Boots and H & M and institutions like Jenners .
George Street , north of Princes Street , is home to a number of upmarket chains and independent stores .
The St. James Centre , at the eastern end of George Street and Princes Street , hosts a substantial number of national chains including a large John Lewis .
Edinburgh also has substantial retail developments outside the city centre .
The Royal Yacht Britannia lies in dock here next to the centre .
Edinburgh Zoo is a non-profit zoological park located in Corstorphine .
The land lies on Corstorphine Hill and provides extensive views of the city .
Edinburgh has two professional football clubs -- Heart of Midlothian and Hibernian .
They are known locally as Hearts and Hibs and both teams currently play in the Scottish Premier League .
Hearts play at Tynecastle Stadium in Gorgie , while Hibs play at Easter Road Stadium , which straddles the former boundary between Edinburgh and Leith .
Edinburgh was also home to senior sides St Bernard 's , and Leith Athletic .
Most recently , Meadowbank Thistle played at Meadowbank Stadium until 1995 , when the club moved to Livingston , becoming Livingston F.C. .
It is the largest capacity stadium in Scotland .
Previously Edinburgh was represented by the Murrayfield Racers and the Edinburgh Racers .
The Scottish cricket team , who represent Scotland at cricket internationally and in the Friends Provident Trophy , play their home matches at The Grange .
1992 saw the team repeat as national champions , becoming the first team to do so in league history and saw the start of the club 's first youth team , the Blue Jays .
For the Games in 1970 the city built major Olympic standard venues and facilities including the Royal Commonwealth Pool and the Meadowbank Stadium .
The city 's most successful non-professional team are the Edinburgh Wolves who currently play at Meadowbank Stadium .
Edinburgh Eagles are a rugby league team who play in the Rugby League Conference Scotland Division .
The economy of Edinburgh is largely based on the services sector -- centered around banking , financial services , higher education , and tourism .
As of March 2010 unemployment in Edinburgh is comparatively low at 3.6 % , and remains consistently below the Scottish average of 4.5 % .
The Royal Bank of Scotland opened its new global headquarters at Gogarburn in the west of the city in October 2005 .
Edinburgh has recently become home to the headquarters of Tesco Bank and Virgin Money .
As the centre of Scotland 's government , as well as its legal system , the public sector plays a central role in the economy of Edinburgh with many departments of the Scottish Government located in the city .
Other major employers include NHS Scotland and local government administration .
Today , the City of Edinburgh Council is the administrative body for the local authority and has its powers stipulated by the Local Government etc ( Scotland ) Act 1994 .
In terms of national governance , Edinburgh is represented in the Scottish Parliament .
For electoral purposes , the city area is divided between six of the nine constituencies in the Lothians electoral region .
One of the local constituencies , Edinburgh South West , is represented by Alistair Darling , the former UK Chancellor of the Exchequer .
Edinburgh Airport is Scotland 's busiest airport and principal international gateway to the capital , handling just over 9 million passengers in 2009 .
In anticipation of rising passenger numbers , the airport operator BAA outlined a draft masterplan in 2006 to provide for the expansion of the airfield and terminal building .
As an important hub on the East Coast Main Line , Edinburgh Waverley is the primary railway station serving the city .
With more than 14 million passengers per year , the station is the second busiest in Scotland behind Glasgow Central .
To the west of the city centre lies Haymarket railway station which is an important commuter stop .
Opened in 2003 , Edinburgh Park station serves the adjacent business park located in the west of the city and the nearby Gogarburn headquarters of the Royal Bank of Scotland .
The Edinburgh Crossrail connects Edinburgh Park with Haymarket , Waverley and the suburban stations of Brunstane and Newcraighall in the east of the city .
Services further afield operate from the Edinburgh Bus Station off St. Andrew Square .
In 2007 , the average daily ridership of Lothian Buses was over 312,000 -- a 6 % rise on the previous year .
A new facility at Straiton opened in October 2008 .
Edinburgh has been without a tram system since 16 November 1956 .
However , following parliamentary approval in 2007 , construction began on a new Edinburgh tram network in early 2008 .
The first phase will see trams running from the airport in the west of the city , through the centre of Edinburgh and down Leith Walk to Ocean Terminal and Newhaven .
Future proposals include a line going west from the airport to Ratho and Newbridge , and a line running along the length of the waterfront .
There are four universities in Edinburgh with over 100,000 students studying in the city .
Heriot-Watt traces its origins to 1821 , when a school for technical education of the working classes was opened .
Based in Riccarton to the west of the city , Heriot-Watt specialises in the disciplines of engineering , business and mathematics .
Edinburgh Napier University has campuses in the south and west of the city , including the former Craiglockhart Hydropathic and Merchiston Tower .
It is home to the Screen Academy Scotland .
Further education colleges in the city include Jewel and Esk College , Telford College , opened in 1968 , and Stevenson College , opened in 1970 .
The Scottish Agricultural College also has a campus in south Edinburgh .
The Trustees Drawing Academy of Edinburgh was founded in 1760 -- an institution that became the Edinburgh College of Art in 1907 .
There are 18 nursery , 94 primary and 23 secondary schools in Edinburgh administered by the city council .
In addition , the city is home to a large number of independent , fee-paying schools including George Heriot 's School , Fettes College , Merchiston Castle School , George Watson 's College , Edinburgh Academy and Stewart 's Melville College .
The Church of Scotland claims the largest membership of any religious denomination in Edinburgh .
In the south east of the city is the 12th century Duddingston Kirk .
The Church of Scotland Offices are located in Edinburgh , as is the Assembly Hall and New College on The Mound .
The Roman Catholic Church also has a sizeable presence in the city .
It was opened in the late 1990s and the construction was largely financed by a gift from King Fahd of Saudi Arabia .
The first recorded presence of a Jewish community in Edinburgh dates back to the late 17th century .
Scotland has a rich history in science and engineering , with Edinburgh contributing its fair share of famous names .
James Clerk Maxwell , the founder of the modern theory of electromagnetism , was born here and educated at the Edinburgh Academy and University of Edinburgh , as was the engineer and telephone pioneer Alexander Graham Bell .
The lighthouse engineering family , the Stevenson family was based in Edinburgh .
Famous city artists include the portrait painters Sir Henry Raeburn , Sir David Wilkie and Allan Ramsay .
Historians such as Douglas Johnson and Arthur Marwick had roots here .
The city has produced or been home to musicians that have been extremely successful in modern times , particularly Ian Anderson , frontman of the band Jethro Tull ; Wattie Buchan , lead singer and founding member of punk band The Exploited ; Shirley Manson , lead singer for the band Garbage ; The Proclaimers ; the Bay City Rollers ; Boards of Canada and Idlewild .
Edinburgh is the hometown of the former Prime Minister of the United Kingdom , Tony Blair , who was born in the city and attended Fettes College ; Robin Harper the co-convener of the Scottish Green Party ; and John Witherspoon , the only clergyman to sign the United States Declaration of Independence , and later president of Princeton University .
The City of Edinburgh has entered into 11 international twinning arrangements since 1954 .
