Ajmer ( Hindi : अजमेर , pronounced [ ədʒmeːr ] ( listen ) ) , formerly written Ajmere , is a city in Ajmer District in India 's Rajasthan state .
Surrounded by the Aravalli Mountains , Ajmer , also known as Ajaymeru , was the city once ruled by Prithviraj Chauhan .
On November 1 , 1956 , it was merged into Rajasthan state .
Ajmer is an ancient crowded city with modern developments in the outskirts .
Ajmer is now a trade center for manufactured goods including wool textiles , hosiery , shoes , soap , and pharmaceuticals .
The nearby town of Kishangarh is one of the largest markets for marble and marble products .
Its internal government , however , was handed over to the Chauhan rulers upon the payment of a heavy tribute to the conquerors .
Ajmer then remained feudatory to Delhi until 1365 , when it was captured by the ruler of Mewar .
Ajmer was conquered by the Mughal emperor Akbar in 1559 .
After independence in 1947 , Ajmer retained its position as a centrally administrated state under a Chief Commissioner for some time .
This is the most celebrated site for the worship of Brahma .
Pushkar is also famous for its annual Pushkar Camel Fair .
The Emperor Akbar , with his queen , used to come here by foot on pilgrimage from Agra every year in observance of a vow he had made when praying for a son .
This gives it the reputation of being one of the oldest hill forts of the world , and it is definitely the oldest among the hill forts in India .
The architecture of the school buildings evoke the grandeur of erstwhile princely Rajasthan .
It appears as flat as a pancake , and offers the eye-catching sights of the neighboring Aravalli mountains .
Other educational institutions which prominently shaped the academic environment of Ajmer before India became independent are :
As of 2001 India census , Ajmer had a population of 485,197 .
Ajmer has an average literacy rate of 74 % , higher than the national average of 59.5 % ; with 56 % of the males and 44 % of females literate .
