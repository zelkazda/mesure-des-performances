Archeology delivers traces of dance from prehistoric times such as the 9,000 year old Rock Shelters of Bhimbetka paintings in India and Egyptian tomb paintings depicting dancing figures from circa 3300 BC .
During the first millennium BCE in India , many texts were composed which attempted to codify aspects of daily life .
In the matter of dance , Bharata Muni 's Natyashastra ( literally " the text of dramaturgy " ) is the one of the earlier texts .
The Natyashastra categorised dance into four groups and into four regional varieties , naming the groups : secular , ritual , abstract , and , interpretive .
During the reign of Louis XIV , himself a dancer , dance became more codified .
Early pioneers of what became known as modern dance include Loie Fuller , Isadora Duncan , Mary Wigman and Ruth St. Denis .
Eurythmy , developed by Rudolf Steiner and Marie Steiner-von Sivers , combines formal elements reminiscent of traditional dance with the new freer style , and introduced a complex new vocabulary to dance .
In the 1920s , important founders of the new style such as Martha Graham and Doris Humphrey began their work .
