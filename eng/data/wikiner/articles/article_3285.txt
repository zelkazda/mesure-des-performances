David Hayes Agnew ( November 24 , 1818 -- March 22 , 1892 ) was an American surgeon .
He was born on November 24 , 1818 in Lancaster County , Pennsylvania .
For twenty-six years ( 1863 -- 1889 ) he was connected with the medical faculty of the University of Pennsylvania , being elected professor of operative surgery in 1870 and professor of the principles and practice of surgery in the following year .
From 1865 to 1884 -- except for a brief interval -- he was a surgeon at the Pennsylvania Hospital .
In 1889 he became the subject of the largest painting ever made by the Philadelphia artist Thomas Eakins , called The Agnew Clinic , in which he is shown conducting a mastectomy operation before a gallery of students and doctors .
During the American Civil War he was consulting surgeon in the Mower Army Hospital , near Philadelphia , and acquired considerable reputation for his operations in cases of gun-shot wounds .
He attended as operating surgeon when President Garfield was fatally wounded by the bullet of an assassin in 1881 .
He died at Philadelphia on March 22 , 1892 , and is buried in West Laurel Hill Cemetery .
