While dogs that resemble the basenji in some respects are commonplace over much of Africa , the breed 's original foundation stock came from the old growth forest regions of the Congo Basin , where its structure and type were fixed by adaptation to its habitat , as well as use ( primarily net hunting in extremely dense old-growth forest vegetation ) .
The basenji is recognized in the following standard colorations : red , black , tricolor ( black with tan in the traditional pattern ) , and brindle ( black stripes on a background of red ) , all with white , by the FCI , KC , AKC , and UKC .
There are additional variations , such as the " trindle " , which is a tricolor with brindle points , and several other colorations exist in the Congo such as liver , shaded reds and sables , and " capped " tricolors ( creeping tan ) .
There is apparently only one completed health survey of basenjis , a 2004 UK Kennel Club survey .
Basenjis in the 2004 UK Kennel Club survey had a medium longevity of 13.6 years ( sample size of 46 deceased dogs ) , which is 1 -- 2 years longer than the median longevity of other breeds of similar size .
Originating on the continent of Africa , basenji-like dogs have lived with humans for thousands of years .
In the Congo , the basenji is also known as " dog of the bush . "
Several attempts were made to bring the breed to England , but the earliest imports succumbed to disease .
It was not until the 1930s that foundation stock was successfully established in England , and then to the United States by animal importer Henry Trefflich .
The breed was officially accepted into the AKC in 1943 .
An American led expedition collected breeding stock in villages in the Basankusu area of the Democratic Republic of Congo , in 2010 .
