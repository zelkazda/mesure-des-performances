George the Bearded , Duke of Saxony ( b. Meissen , 27 August 1471 -- d .
George , as the eldest son , received an excellent training in theology and other branches of learning , and was thus much better educated than most of the princes of his day .
As early as 1488 , when his father was in Friesland fighting on behalf of the emperor , George was regent of the ducal possessions , which included the Margraviate of Meissen with the cities of Dresden and Leipzig .
At Maastricht , 14 February 1499 , Albert settled the succession to his possessions , and endeavoured by this arrangement to prevent further partition of his domain .
The Saxon occupation of Friesland , however , was by no means secure and was the source of constant revolts in that province .
These troubles outside of his Saxon possessions did not prevent George from bestowing much care on the government of the ducal territory proper .
In 1519 , despite the opposition of the theological faculty of the university , he originated the Disputation of Leipzig , with the idea of helping forward the cause of truth , and was present at all the discussions .
Some years later , he wrote a forcible preface to a translation of the New Testament issued at his command by his private secretary , Hieronymus Emser , as an offset to Luther 's version .
The only one of George 's sons then living was the weak-minded and unmarried Frederick .
The intention of his father was that Frederick should rule with the aid of a council .
George was an excellent and industrious ruler , self-sacrificing , high-minded , and unwearying in the furtherance of the highest interests of his land and people .
