He was born into a family of writers , the most well known of whom was his paternal aunt , Anna Letitia Barbauld , a woman of letters who wrote poetry and essays as well as early children 's literature .
His father , Dr. John Aikin , was a medical doctor , historian , and author .
His sister was Lucy Aikin ( 1781 -- 1864 ) , a historical writer .
Arthur Aikin studied chemistry under Joseph Priestley in the New College at Hackney , and gave attention to the practical applications of the science .
He was one of the founders of the Geological Society of London in 1807 and was its honorary secretary in 1812-1817 .
He contributed papers on the Wrekin and the Shropshire coalfield , among others , to the transactions of that society .
Later he became secretary of the Royal Society of Arts .
He was founder of the Chemical Society of London in 1841 , being its first Treasurer and second President .
He died in London .
