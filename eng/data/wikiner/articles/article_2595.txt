Les statues meurent aussi ( 1953 ) which he codirected with Alain Resnais was one of the first anticolonial films .
He became known internationally for the short film La Jetée ( 1962 ) .
This film was the inspiration for Mamoru Oshii 's debut live action feature The Red Spectacles ( 1987 ) ( later for Avalon ) and also inspired Terry Gilliam 's Twelve Monkeys ( 1995 ) .
It also inspired many of director Mira Nair 's shots of the recent film , The Namesake .
In 1982 Marker finished Sans Soleil , stretching the limits of what could be called a documentary .
The main themes are Japan , Africa , memory and travel .
A sequence in the middle of the film takes place in San Francisco , and heavily references Alfred Hitchcock 's Vertigo .
In the 2007 Criterion Collection release of La Jetée and Sans Soleil , Marker included a short essay " Working on a shoestring budget " .
He confessed to shooting all of Sans Soleil with a silent film camera and recording all the audio on a primitive audio cassette recorder .
Marker also reminds the reader that only one short scene in La Jetée is of a moving image , as Marker could only borrow a movie camera for one afternoon while working on the film .
