Carlo Lorenzini ( November 24 , 1826 -- October 26 , 1890 ) , better known by the pen name Carlo Collodi , was a Florentine children 's writer known for the world-renowned fairy tale novel , The Adventures of Pinocchio .
During the Wars of Independence in 1848 and 1860 Collodi served as a volunteer with the Tuscan army .
Lorenzini became fascinated by the idea of using an amiable , rascally character as a means of expressing his own convictions through allegory .
Lorenzini died in Florence in 1890 , unaware of the fame and popularity that awaited his work : as in the allegory of the story , Pinocchio eventually went on to lead his own independent life , distinct from that of the author .
Lorenzini is interred at San Miniato al Monte Basilica .
