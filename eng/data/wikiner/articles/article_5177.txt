The GNU implementation of gzip was created by Jean-Loup Gailly and Mark Adler .
These implementations originally come from NetBSD , and supports decompression of bzip2 and Unix pack ( 1 ) format .
DEFLATE was intended as a replacement for LZW and other patent-encumbered data compression algorithms which , at the time , limited the usability of compress and other popular archivers .
" gzip " is often also used to refer to the gzip file format , which is :
Although its file format also allows for multiple such streams to be concatenated ( zipped files are simply decompressed concatenated as if they were originally one file ) , gzip is normally used to compress just single files .
gzip is not to be confused with the ZIP archive format , which also uses DEFLATE .
Both commands call the same binary ; gunzip has the same effect as gzip -d .
Many client libraries , browsers , and server platforms support gzip .
Since the late 1990s , bzip2 , a file compression utility based on a block-sorting algorithm , has gained some popularity as a gzip replacement .
bzip2-compressed tarballs are conventionally named either .tar.bz2 or simply .tbz .
The gzip utility on UNIX systems has some alternative names .
When gzip is invoked as gunzip , it decompresses the data ( a file or stdin ) .
gunzip is equivalent to gzip -d .
When gzip is invoked as zcat , it also decompresses the data , but behaves similarly to cat .
zcat is equivalent to gzip -d -c .
