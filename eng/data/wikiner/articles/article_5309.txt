Gershwin 's compositions spanned both popular and classical genres , and his most popular melodies are widely known .
He wrote most of his vocal and theatrical works , including more than a dozen Broadway shows , in collaboration with his elder brother , lyricist Ira Gershwin .
George Gershwin composed music for both Broadway and the classical concert hall , as well as popular songs that brought his work to an even wider public .
Gershwin was named Jacob Gershowitz at birth in Brooklyn on September 26 , 1898 .
( George changed the spelling of the family name to ' Gershwin ' after he became a professional musician ; other members of his family followed suit . )
George Gershwin was the second of four children .
His parents had bought a piano for lessons for his older brother Ira , but to his parents ' surprise and Ira 's relief , it was George who played it .
Although his younger sister Frances Gershwin was the first in the family to make money from her musical talents , she married young and devoted herself to being a mother and housewife .
She gave up her performing career , but settled into painting for another creative outlet -- painting was also a hobby of George Gershwin .
At home , following such concerts , young Gershwin would attempt to reproduce at the piano the music that he had heard .
He later studied with classical composer Rubin Goldmark and avant-garde composer-theorist Henry Cowell .
It was published in 1916 when Gershwin was only 17 years old and earned him a sum total of $ 5 , although he was promised much more .
As well as recording piano rolls , Gershwin made a brief foray into vaudeville , accompanying both Nora Bayes and Louise Dresser on the piano .
In the early 1920s Gershwin frequently worked with the lyricist Buddy DeSylva .
In 1924 , George and Ira Gershwin collaborated on a musical comedy Lady Be Good , which included such future standards as " Fascinating Rhythm " and " Oh , Lady Be Good ! " .
This was followed by Oh , Kay ! ( 1926 ) , Funny Face ( 1927 ) , Strike Up the Band ( 1927 and 1930 ) , Show Girl ( 1929 ) , Girl Crazy ( 1930 ) , which introduced the standard " I Got Rhythm " ; and Of Thee I Sing ( 1931 ) , the first musical comedy to win a Pulitzer Prize .
Gershwin stayed in Paris for a short period , where he applied to study composition with Nadia Boulanger .
Boulanger , along with several other prospective tutors such as Maurice Ravel , rejected him , however , afraid that rigorous classical study would ruin his jazz-influenced style .
His most ambitious composition was Porgy and Bess ( 1935 ) .
Porgy and Bess contains some of Gershwin 's most sophisticated music , including a fugue , a passacaglia , the use of atonality , polytonality and polyrhythm , and a tone row .
Even the " set numbers " are some of the most refined and ingenious of Gershwin 's output .
( For the performances , Gershwin collaborated with Eva Jessye , whom he picked as the musical director .
One of the outstanding musical alumnae of Western University in Kansas , she had created her own choir in New York and performed widely with them . )
After Porgy and Bess , Gershwin eventually was commissioned by RKO Pictures in 1936 to compose songs and the underscore for Shall We Dance , starring Fred Astaire and Ginger Rogers .
Gershwin 's extended score , which would marry ballet with jazz in a new way , runs over an hour in length .
It took Gershwin several months to write and orchestrate it .
Early in 1937 , Gershwin began to complain of blinding headaches and a recurring impression that he was smelling burned rubber .
The surgeon 's description of Gershwin 's tumor as a right temporal lobe cyst with a mural nodule is much more consistent with a pilocytic astrocytoma , a very low-grade of brain tumor .
Further , Gershwin 's initial olfactory hallucination ( the unpleasant smell of burning rubber ) was in 1934 .
Thus , it is possible that Gershwin 's prominent chronic gastrointestinal symptoms ( which he called his " composer 's stomach " ) were a manifestation of temporal lobe epilepsy caused by his tumor .
If this is correct , then Gershwin was not " a notorious hypochondriac, " as suggested by his biographer Edward Jablonski .
It was in Hollywood , while working on the score of The Goldwyn Follies , that he collapsed .
John O'Hara remarked : " George Gershwin died on July 11 , 1937 , but I do n't have to believe it if I do n't want to .
The nomination was posthumous ; Gershwin died two months after the film 's release .
Gershwin had a 10-year affair with composer Kay Swift and frequently consulted her about his music .
After Gershwin died , Swift arranged some of his music , transcribed some of his recordings , and collaborated with his brother Ira on several projects .
Gershwin died intestate .
The Gershwin estate continues to collect significant royalties from licensing the copyrights on Gershwin 's work .
The estate supported the Sonny Bono Copyright Term Extension Act because its 1923 cutoff date was shortly before Gershwin had begun to create his most popular works .
The copyrights on those works expired at the end of 2007 in the European Union .
According to Fred Astaire 's letters to Adele Astaire , Gershwin whispered Astaire 's name before passing away .
In 2005 , The Guardian determined using " estimates of earnings accrued in a composer 's lifetime " that George Gershwin was the wealthiest composer of all time .
I have heard of George Gershwin 's works and I find them intriguing . "
The orchestrations in Gershwin 's symphonic works often seem similar to those of Ravel ; likewise , Ravel 's two piano concertos evince an influence of Gershwin .
Gershwin asked to study with Ravel .
When Ravel heard how much Gershwin earned , Ravel replied with words to the effect of , " You should give me lessons . "
( Some versions of this story feature Igor Stravinsky rather than Ravel as the composer ; however Stravinsky confirmed that he originally heard the story from Ravel . )
Gershwin 's own Concerto in F was criticized for being related to the work of Claude Debussy , more so than to the expected jazz style .
He also asked Schoenberg for composition lessons .
Schoenberg refused , saying " I would only make you a bad Schoenberg , and you 're such a good Gershwin already . "
There has been some disagreement about the nature of Schillinger 's influence on Gershwin .
After the posthumous success of Porgy and Bess , Schillinger claimed he had a large and direct influence in overseeing the creation of the opera ; Ira completely denied that his brother had any such assistance for this work .
What set Gershwin apart was his ability to manipulate forms of music into his own unique voice .
Although George Gershwin would seldom make grand statements about his music , he believed that " true music must reflect the thought and aspirations of the people and time .
In 2007 , the Library of Congress named their Prize for Popular Song after George and Ira Gershwin .
On March 1 , 2007 , the first Gershwin Prize was awarded to Paul Simon .
Early in his career Gershwin made dozens of player piano piano roll recordings , which were a main source of income for him .
Compared to the piano rolls , there are few accessible audio recordings of Gershwin 's playing .
The recording took place before Swanee became famous as an Al Jolson specialty in early 1920 .
Gershwin and the same orchestra made an electrical recording of the abridged version for Victor in 1927 .
However , a dispute in the studio over interpretation angered Paul Whiteman and he left .
The conductor 's baton was taken over by Victor 's staff conductor Nathaniel Shilkret .
When it was realized no one had been hired to play the brief celeste solo , Gershwin was asked if he could and would play the instrument , and he agreed .
Gershwin can be heard , rather briefly , on the recording during the slow section .
Gershwin appeared on several radio programs , including Rudy Vallee 's , and played some of his compositions .
He also recorded a run-through of his Second Rhapsody , conducting the orchestra and playing the piano solos .
Gershwin recorded excerpts from Porgy and Bess with members of the original cast , conducting the orchestra from the keyboard ; he even announced the selections and the names of the performers .
In 1935 RCA Victor asked him to supervise recordings of highlights from Porgy and Bess ; these were his last recordings .
It is entitled Gershwin Plays Gershwin : The Piano Rolls .
In October 2009 , it was reported by Rolling Stone that Brian Wilson is completing at least two unfinished compositions by George Gershwin for possible release in 2010 .
