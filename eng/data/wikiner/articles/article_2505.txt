Centuries later , the Islamic philosopher Avicenna initiated a full-fledged inquiry into the question of being , in which he distinguished between essence and existence .
Many other philosophers and theologians have posited cosmological arguments both before and since Aquinas .
Aquinas observed that , in nature , there were things with contingent existences .
Thus , according to Aquinas , there must have been a time when nothing existed .
This distinction is an excellent example of the difference between a deistic view and a theistic view ( Aquinas ) .
David Hume highlighted this problem of induction and argued that causal relations were not true a priori ( deductively ) .
According to Kaku , these molecules could move forever , without beginning or end .
