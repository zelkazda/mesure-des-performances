This goal was rapidly met with great success , as the nation joined NATO in 1999 and the European Union in 2004 , and held the Presidency of the European Union during the first half of 2009 .
It is a member of the General Agreement on Tariffs and Trade .
It maintains diplomatic relations with more than 85 countries , of which 63 have permanent representation in Prague .
Government of the Czech Republic agrees ( while 67 % Czechs disagree and only about 22 % support it ) to host a missile defense radar on its territory while a base of missile interceptors is supposed to be built in Poland .
U.S. President Woodrow Wilson and the United States played a major role in the establishment of Czechoslovakia on 28 October 1918 .
