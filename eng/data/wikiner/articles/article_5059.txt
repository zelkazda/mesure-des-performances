Besides denoting the human spirit or soul , both of the living and the deceased , the Old English word is used as a synonym of Latin spiritus also in the meaning of " breath , blast " from the earliest ( 9th century ) attestations .
the " Holy Ghost " .
The now prevailing sense of " the soul of a deceased person , spoken of as appearing in a visible form " only emerges in Middle English ( 14th century ) .
The word has no commonly accepted etymology ; OED notes " of obscure origin " only .
An association with the verb writhe was the etymology favored by J. R. R. Tolkien .
Tolkien 's use of the word in the naming of the creatures known as the Ringwraiths has influenced later usage in fantasy literature .
The bodies found in many tumuli ( kurgan ) had been ritually bound before burial , and the custom of binding the dead persists , for example , in rural Anatolia .
The Hebrew Torah and the Bible contain few references to ghosts , associating spiritism with forbidden occult activities cf. Deuteronomy 18:11 .
In a similar vein , Jesus ' followers at first believe him to be a ghost ( spirit ) when they see him walking on water .
Ghosts appeared in Homer 's Odyssey and Iliad , in which they were described as vanishing " as a vapor , gibbering and whining into the earth . "
Homer 's ghosts had little interaction with the world of the living .
The ancient Romans believed a ghost could be used to exact revenge on an enemy by scratching a curse on a piece of lead or pottery and placing it into a grave .
Another celebrated account of a haunted house from the ancient classical world is given by Pliny the Younger ( c. 50 AD ) .
Pliny describes the haunting of a house in Athens by a ghost bound in chains .
The writers Plautus and Lucian also wrote stories about haunted houses .
Lucian relates how he persisted in his disbelief despite practical jokes perpetrated by " some young men of Abdera " who dressed up in black robes with skull masks in order to give him a fright .
This account by Lucian notes something about the popular classical expectation of how a ghost should look .
The living could tell them apart by demanding their purpose in the name of Jesus Christ .
From the medieval period an apparition of a ghost is recorded from 1211 , at the time of the Albigensian Crusade .
Haunted houses are featured in the 9th century Arabian Nights .
The Child ballad Sweet William 's Ghost ( 1868 ) recounts the story of a ghost returning to beg a woman to free him from his promise to marry her , as he obviously can not being dead ; her refusal would mean his damnation .
The Unquiet Grave expresses a belief even more widespread , found in various locations over Europe : ghosts can stem from the excessive grief of the living , whose mourning interferes with the dead 's peaceful rest .
Spiritualism is currently practiced primarily through various denominational Spiritualist Churches in the United States and United Kingdom .
Joe Nickell of the Committee for Skeptical Inquiry , wrote that there was no credible scientific evidence that any location was inhabited by spirits of the dead .
Some researchers , such as Michael Persinger of Laurentian University , Canada , have speculated that changes in geomagnetic fields could stimulate the brain 's temporal lobes and produce many of the experiences associated with hauntings .
The beliefs , legends and stories are as diverse as the people of the Philippines .
There are many references to ghosts in Chinese culture .
The common word for ghosts in Bengali is bhoot ( ভূত ) .
There is extensive and varied belief in ghosts in Mexican culture .
Mexican literature and movies include many stories of ghosts interacting with the living .
It has been adapted for film and television many times , most notably in Sleepy Hollow , a successful 1999 feature film .
Oscar Wilde 's comedy The Canterville Ghost has been adapted for film and television on several occasions .
Henry James 's The Turn of the Screw has also appeared in a number of adaptations , notably the film The Innocents and Benjamin Britten 's opera The Turn of the Screw .
Mahal became one of the biggest box office hits of 1949 in India .
Professional parapsychologists and " ghosts hunters " , such as Harry Price and Peter Underwood , published accounts of their experiences with ostensibly true ghost stories such as The Most Haunted House in England , and Ghosts of Borley .
Children 's benevolent ghost stories became popular , such as Casper the Friendly Ghost , created in the 1930s and appearing in comics , animated cartoons , and eventually a 1995 feature film .
A common theme in the romantic genre from this period is the ghost as a benign guide or messenger , often with unfinished business , such as 1989 's Field of Dreams , the 1990 film Ghost , and the 1993 comedy Heart and Souls .
In the horror genre , 1980 's The Fog , and the A Nightmare on Elm Street series of films from the 1980s and 1990s are notable examples of the trend for the merging of ghost stories with scenes of physical violence .
The ghost hunting theme has been featured in reality television series , such as Ghost Adventures , Ghost Hunters , Ghost Hunters International , Ghost Lab , Most Haunted , and A Haunting .
It is also represented in children 's television by such programs as The Ghost Hunter and Ghost Trackers .
Examples of films from this period include 1999s The Sixth Sense and 2001s The Others .
