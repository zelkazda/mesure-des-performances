Italian Fascism and most other fascist movements promote a corporatist economy whereby , in theory , representatives of capital and labour interest groups work together within sectoral corporations to create both harmonious labour relations and maximization of production that would serve the national interest .
The fasces , which consisted of a bundle of rods that were tied around an axe , was an ancient Roman symbol of the authority of the civic magistrate .
For example the Falange symbol is a bunch of arrows joined together by a yoke .
There were a variety of factions within Italian Fascism on both the left and the right .
Stanley Payne states that pre-war fascism found a coherent identity through alliances with right-wing movements Roger Griffin argues that since the end of World War II , fascist movements have become intertwined with the radical right , describing certain groups as part of a " fascist radical right " .
Payne says " fascists were unique in their hostility to all the main established currents , left right and center " , noting that they allied with both left and right , but more often with the right .
The position that fascism is neither right nor left is supported by a number of contemporary historians and sociologists , including Seymour Martin Lipset and Roger Griffin .
However , Italian fascism also declared its objection to excessive capitalism , which it called supercapitalism .
Mussolini promoted ambiguity about fascism 's positions in order to rally as many people to it as possible , saying fascists can be " aristocrats or democrats , revolutionaries and reactionaries , proletarians and anti-proletarians , pacifists and anti-pacifists " .
Mussolini claimed that Italian Fascism 's economic system of corporatism could be identified as either state capitalism or state socialism , which in either case involved " the bureaucratisation of the economic activities of the nation . "
Mussolini described fascism in any language he found useful .
Following the defeat of the Axis powers in World War II and the publicity surrounding the atrocities committed during the period of fascist governments , the term fascist has been used as a pejorative word , often referring to widely varying movements across the political spectrum .
Richard Griffiths argued in 2005 that " fascism " is the " most misused , and over-used word , of our times " .
Prior to World War I , syndicalists overwhelmingly focused on class identity and supported class conflict , while they were hostile toward national wars , resulting in antimilitarism .
At the outbreak of World War I in August 1914 , a number of socialist parties initially supported intervention in the war .
At the same time , Benito Mussolini decided to join the interventionist cause .
The Arditi were soldiers who were specifically trained for a life of violence and wore unique blackshirt uniforms and fezzes .
Mussolini himself remained in Milan to await the results of the actions .
Mussolini 's coalition government initially pursued economically liberal policies under the direction of liberal finance minister Alberto de Stefani , including balancing the budget through deep cuts to the civil service .
Initially , little drastic change in government policy had occurred and repressive police actions against communist and d'Annunzian rebels were limited .
At the same time , however , Mussolini consolidated his control over the National Fascist Party by creating a governing executive for the party , the Grand Council of Fascism , whose agenda he controlled .
The Acerbo Law was passed in spite of numerous abstentions from the vote .
This resulted in an aggressive military campaign against natives in Libya , including mass killings , the use of concentration camps , and the forced starvation of thousands of people .
The most important new fascist regime was Nazi Germany , under the leadership of Adolf Hitler .
The fascist Iron Guard movement in Romania soared in political support after 1933 , gaining representation in the Romanian government , and an Iron Guard member assassinated Romanian prime minister Ion Duca .
However , Mussolini claimed that the industrial developments of earlier " heroic capitalism " were valuable and continued to support private property as long as it was productive .
Benito Mussolini had a strong attachment to the works of Plato .
Similarly , Vilfredo Pareto 's endorsement of an elite minority-led oligarchical government was an influence on fascists .
Mussolini and Margherita Sarfatti identified Plato and Pareto as the sources of fascism 's constantly changing character .
Mussolini modeled his dictatorship and totalitarian aims on Julius Caesar .
Mussolini described his personal admiration of Caesar , claiming that Caesar had " the resolve of a warrior and the resourcefulness of a wise man " .
Shortly after seizing power with the March on Rome , Mussolini went to the Roman Forum and stood before the ruins to pay homage to Caesar .
He admired Machiavelli as a capable statesman and a thinker .
Mussolini identified Machiavelli 's conception of " the prince " as the personification of the state and sympathized with Machiavelli 's negative conception of most people as tending to be self-centred and unethical .
Mussolini , like Machiavelli , claimed that populations were unfit to govern themselves , and that they needed leadership to direct their lives .
Fascism is connected to the theories of Georg Wilhelm Friedrich Hegel .
One of fascism 's major philosophers , Giovanni Gentile , was a Hegelian .
After the Second Italo-Abyssinian War , Gentile 's influence in the National Fascist Party ( PNF ) collapsed , with philosophical influence being centralized to Mussolini 's will .
Nietzsche , d'Annunzio , and Mussolini all held contempt for Christianity , the bourgeoisie , democracy , and reformist politics .
D'Annunzio supported the creation of a new state based on an aristocracy of intellectuals , a cult of strength , and opposition to democracy .
He believed that the best ideology to exemplify Nietzsche 's themes was aggressive nationalism .
Mussolini saw Nietzsche as similar to Jean-Marie Guyau , who advocated a philosophy of action .
Mussolini 's use of Nietzsche made him a highly unorthodox socialist , because of Nietzsche 's promotion of elitism and anti-egalitarian views .
Unlike fascists , however , Nietzsche did not admire the state ; in his work Thus Spoke Zarathustra , he referred to the state as " the coldest of all monsters " .
Alessandro Mussolini 's political outlook combined the views of anarchist figures like Carlo Cafiero and Mikhail Bakunin , the military authoritarianism of Garibaldi , and the nationalism of Mazzini .
In 1902 , at the anniversary of Garibaldi 's death , Benito Mussolini made a public speech in praise of the republican nationalist .
Syndicalist philosopher Georges Sorel is considered a major inspiration for both Bolshevism and fascism , both of which Sorel supported because they challenged bourgeois democracy .
Sorel argued that socialists should reject the materialism and rationalism of Marx and instead adopt moral and emotional appeals of ideals and myths to promote their cause .
Sorel believed that this was beneficial , because the proletariat would be more willing to accept moral renewal .
Fascism initially had close connections to futurism ; the Futurist Manifesto ( 1909 ) by Filippo Tommaso Marinetti " glorified action , technology , and war " and promoted irrationalism over rationalism ; the revolutionary entrenchment of modernist and violent art and aesthetics ; the destruction of all past aesthetic traditions to liberate modern aesthetics ; the promotion of patriotism and militarism ; and contempt of women and feminism .
The theories and perspectives of Oswald Spengler also influenced fascism .
Spengler 's ideas were openly admired by a number of leading fascist figures , including Mussolini , Benedetto Croce , and Alfred Rosenburg .
While fascists respected Spengler 's works , they typically rejected his fatalism and pessimism .
If a nation continues on this path without resistance , Gini claimed , it would enter a final decadent stage where the nation would degenerate , as noted by decreasing birth rate , decreasing cultural output , and the lack of imperial conquest .
Benito Mussolini stated in 1922 , " For us the nation is not just territory but something spiritual ...
Benito Mussolini spoke of war idealistically as a source of masculine pride , and spoke negatively of pacifism :
Some have argued that , in spite of Italian Fascism 's attempt at totalitarianism , it became an authoritarian cult of personality around Mussolini .
Hitler declared them unreliable , useless , and even dangerous .
Their eugenics program also stemmed from the " progressive biomedical model " of Weimar Germany .
Initially , Italian Fascism officially stood in favour of expanding voting rights to women .
The regime was criticized by the Roman Catholic Church , which claimed that these activities were causing " masculinization " of women .
Mussolini perceived women 's primary role as childbearers , while men were warriors ; he once said , " war is to man what maternity is to the woman " .
Mussolini went on to say that the solution to unemployment for men was the " exodus of women from the work force " .
The British Union of Fascists opposed homosexuality and pejoratively questioned their opponents ' heterosexuality .
The Romanian Iron Guard opposed homosexuality as undermining society .
Italian Fascism involved corporatism , a political system in which the economy is collectively managed by employers , workers , and state officials by formal mechanisms at the national level .
Italian Fascism 's economy was based on corporatism , and a number of other fascist movements similarly promoted corporatism .
Oswald Mosley of the British Union of Fascists , describing fascist corporatism , said that " it means a nation organized as the human body , with each organ performing its individual function but working in harmony with the whole " .
In 1933 , Benito Mussolini declared Italian Fascism 's opposition to the " decadent capitalism " that he claimed prevailed in the world at the time , but he did not denounce capitalism entirely .
Mussolini claimed that capitalism had degenerated in three stages , starting with dynamic or heroic capitalism ( 1830 -- 1870 ) , followed by static capitalism ( 1870 -- 1914 ) , and reaching its final form of decadent capitalism or " supercapitalism " beginning in 1914 .
Mussolini denounced supercapitalism for causing the " standardization of humankind " and for causing excessive consumption .
Mussolini claimed that at the stage of supercapitalism , " a capitalist enterprise , when difficulties arise , throws itself like a dead weight into the state 's arms .
Spanish Falangist leader José Antonio Primo de Rivera did not believe that corporatism was effective and denounced it as a propaganda ploy , saying " this stuff about the corporative state is another piece of windbaggery " .
Mussolini wrote approvingly of the notion that profits should not be taken away from those who produced them by their own labour , saying " I do not respect -- I even hate -- those men that leech a tenth of the riches produced by others " .
Health and welfare spending grew dramatically under Italian fascism , with welfare rising from 7 % of the budget in 1930 to 20 % in 1940 .
It is estimated that , by 1936 , the OND had organised 80 percent of salaried workers and , by 1939 , 40 percent of the industrial workforce .
Adolf Hitler was opposed to egalitarian and universal social welfare because , in his view , it encouraged the preservation of the degenerate and feeble .
Mussolini , in a 1919 speech denouncing Soviet Russia , claimed that Jewish bankers in London and New York City were bound by the chains of race to Moscow and that 80 percent of the Soviet leaders were Jews .
In 1928 he noted the high birth-rate of blacks in the United States , and that they had surpassed the population of whites in certain areas , such as Harlem in New York City .
He described their greater racial consciousness in comparison with American whites as contributing to their growing strength .
On the issue of the low birth rate of whites , Mussolini said in 1928 :
According to Mussolini , only through promoting natality and eugenics could this be reversed .
According to Payne , such " would-be " religious fascists only gain hold where traditional belief is weakened or absent , since fascism seeks to create new non-rationalist myth structures for those who no longer hold a traditional view .
The Ustaše in Croatia had strong Catholic overtones , with some clerics in positions of power .
The fascist movement in Romania , known as the Iron Guard or the Legion of Archangel Michael , preceded its meetings with a church service , and their demonstrations were usually led by priests carrying icons and religious flags .
In the annexed territory of Reichsgau Wartheland , churches were systematically closed , and most priests were either killed , imprisoned , or deported to the General Government .
Of those murdered by the Nazi regime , 108 are regarded as blessed martyrs .
Among them , Maximilian Kolbe was canonized as a saint .
In the Dachau concentration camp alone , 2,600 Catholic priests from 24 different countries were killed .
Along these lines , Yale political scientist , Juan Linz and others have noted that secularization had created a void which could be filled by a total ideology , making totalitarianism possible , and Roger Griffin has characterized fascism as a type of anti-religious political religion .
In 1921 , Mussolini was elected to the Chamber of Deputies for the first time ; he was later appointed Prime Minister by the King in 1922 .
An early example was his bombardment of Corfu in 1923 .
It was his dream to make the Mediterranean mare nostrum ( " our sea " in Latin ) .
The National Socialist German Workers ' Party ( Nazi Party ) ruled Germany from 1933 until 1945 .
Drexler was ousted from party leadership in 1921 by Hitler , who secured the position of undisputed and permanent leader of the party .
Joseph Goebbels , Hitler 's chief propagandist , credited Italian Fascism with starting a conflict against liberal democracy :
He , like Mussolini , profoundly admired Ancient Rome , and he repeatedly mentioned it in Mein Kampf as a model for Germany .
Hitler considered the ancient Romans a master race .
Nazism differed from Italian fascism in that it had a stronger emphasis on race , religion , and ethnicity , especially exhibited as antisemitism .
The Iron Guard was a fascist movement and political party in Romania from 1927 to 1941 .
It was founded by Corneliu Zelea Codreanu on 24 July 1927 as the " Legion of the Archangel Michael " , and Codreanu led it until his death in 1938 .
Adherents to the movement continued to be widely referred to as " legionnaires " ( sometimes " legionaries " ; Romanian : legionari ) and the organization as the " Legion " or the " Legionary Movement " ( Mişcarea Legionară ) , despite various changes of the intermittently-banned organization 's name .
Falangism was a form of fascism founded by José Antonio Primo de Rivera in 1934 , emerging during the Second Spanish Republic .
Primo de Rivera was the son of Spain 's former dictator Miguel Primo de Rivera .
Following the establishment of the Second Spanish Republic , Spain transitioned from a kingdom into a republic .
Primo de Rivera , inspired by Mussolini , founded the Falange Española party , which merged a year later with the Juntas de Ofensiva Nacional-Sindicalista party of Ramiro Ledesma and Onésimo Redondo .
The Falange participated in the Spanish general election , 1936 with low results compared to the leftist Popular Front , but soon afterward it increased in membership rapidly .
Franco balanced several different interests of elements in his party in an effort to keep them united , especially in regard to the question of monarchy .
Franco 's traditionalist , conservative stance means the Francoist regime is not generally considered to be fascist , since it lacked any revolutionary , transformative aspect .
Stanley G. Payne , author of Falange : a history of Spanish fascism and supporter of minority revisionist historians who see the Spanish civil war as a result of leftist influences , wrote : " scarcely any of the serious historians and analysts of Franco consider the generalissimo to be a core fascist . "
Falangism was significant in Lebanon through the Kataeb Party and its founder Pierre Gemayel , fighting for national independence , which was won in 1943 .
Brazilian Integralism was a form of fascism founded by Plinio Salgado in Brazil in October 1932 .
The Fatherland Front , like fascism , promoted corporatism , but unlike fascism it did not promote it along secular and totalitarian grounds .
Salazar created an authoritarian conservative nationalist regime that gave him complete control of government affairs and instituted a police state .
Salazar also instituted economic corporatism and substantial state control over the economy , and , like fascist leaders , he denounced democracy as detrimental to nations .
Salazar described his regime 's nationalism as " sane and non-aggressive " .
He criticized the " pagan " nature of Italian Fascism , claiming that it " recognizes no moral or legal order " .
Under Metaxas , Greek officials adopted the straight-armed salute .
A paramilitary organization , it was under the command of the ministry 's general director , Dr. Sami Shawkat , and it was modeled after the Hitler Youth .
Of post-World War II regimes , Stanley G. Payne writes :
Its ideology is laced with racism , especially against Persians , Jews , Kurds , and other minorities , and mainstream media , including journalists at PBS , BBC , and the New York Times , widely consider it fascist .
Although Saddam Hussein never acknowledged the training of a youth brigade , he had , in several speeches , spoken admiringly of the Hitler Youth .
According to the History Channel documentary Saddam and the Third Reich , " Few people realize that the Ba'ath party was actually formed upon the principles and organizational structure of the Nazi party . "
Fascism has been widely criticized since the end of World War II for a variety of reasons .
As a result , even some communist regimes have been declared " fascist " under such interpretations , including those of Cuba under Fidel Castro and Vietnam under Ho Chi Minh .
