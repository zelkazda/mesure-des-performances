George Hoyt Whipple ( August 28 , 1878 -- February 1 , 1976 ) was an American physician , pathologist , biomedical researcher , and medical school educator and administrator .
Whipple shared the Nobel Prize in Physiology or Medicine in 1934 with George Richards Minot and William Parry Murphy " for their discoveries concerning liver therapy in cases of anemia . "
He attended medical school at the Johns Hopkins University from which he received the M.D. degree in 1905 .
Whipple died in 1976 at the age of 97 and is interred in Rochester 's Mount Hope Cemetery .
Whipple 's main research was concerned with anemia and with the physiology and pathology of the liver .
... Whipple 's experiments were planned exceedingly well , and carried out very accurately , and consequently their results can lay claim to absolute reliability .
Whipple was also the first person to describe an unknown disease he called lipodystrophia intestinalis because there were abnormal lipid deposits in the small intestine wall .
Whipple also correctly pointed to the bacterial cause of the disease in his original report in 1907 .
