Arkansas ( i i i / k ən s ɔː / AR -kən-saw ) is a state located in the southern region of the United States .
Ark. shares a border with six states , with its eastern border largely defined by the Mississippi River .
Its diverse geography ranges from the mountainous regions of the Ozarks and the Ouachita Mountains , which make up the U.S. Interior Highlands , to the eastern lowlands along the Mississippi River .
The capital and most populous city is Little Rock , located in the central portion of the state .
The name " Arkansas " derives from the same root as the name for the state of Kan. .
One wanted to pronounce the name i i i / æ n z ə s / ar-KAN -zəs and the other wanted i i i / ən s ɔ ː / AR -kən-saw .
In 2007 , the state legislature officially declared the possessive form of the state 's name to be Arkansas 's .
The Mississippi River forms most of Arkansas 's eastern border , except in Clay and Greene counties where the St. Francis River forms the western boundary of the Missouri Bootheel , and in dozens of places where the current channel of the Mississippi has meandered from where it had last been legally specified .
Ark. shares its southern border with La. , its northern border with Missouri , its eastern border with Tennessee and Mississippi , and its western border with Texas and Oklahoma .
Arkansas is a land of mountains and valleys , thick forests and fertile plains .
The Arkansas Delta is a flat landscape of rich alluvial soils formed by repeated flooding of the adjacent Mississippi .
A narrow band of rolling hills , Crowley 's Ridge rises from 250 to 500 feet ( 150 m ) above the surrounding alluvial plain and underlies many of the major towns of eastern Arkansas .
These mountain ranges are part of the U.S. Interior Highlands region , the only major mountainous region between the Rocky Mountains and the Appalachian Mountains .
The highest point in the state is Mount Magazine in the Ozark Mountains ; it rises to 2753 feet ( 839 m ) above sea level .
Ark. is home to many caves , such as Blanchard Springs Caverns .
( near Murfreesboro ) .
Ark. generally has a humid subtropical climate , which borders on humid continental in some northern highland areas .
While not bordering the Gulf of Mexico , Arkansas is still close enough to this warm , large body of water for it to influence the weather in the state .
Generally , Arkansas has hot , humid summers and cold , slightly drier winters .
This is not only due to its closer proximity to the plains states , but also to the higher elevations found throughout the Ozark and Ouachita mountains .
The half of the state south of Little Rock gets less snow , and is more apt to see ice storms , however , sleet and freezing rain are expected throughout the state during the winter months , and can significantly impact travel and day to day life .
Ark. is known for extreme weather .
Between both the Great Plains and the Gulf States , Arkansas receives around 60 days of thunderstorms .
While being sufficiently away from the coast to be safe from a direct hit from a hurricane , Arkansas can often get the remnants of a tropical system which dumps tremendous amounts of rain in a short time and often spawns smaller tornadoes .
High water pouring down the White River caused historic flooding in cities along its path in eastern Arkansas .
The Territory of Arkansas was organized on July 4 , 1819 .
Arkansas played a key role in aiding Texas in its war for independence from Mexico ; it sent troops and materials to Tex. to help fight the war .
The proximity of the city of Washington to the Tex. border involved the town in the Texas Revolution of 1835-36 .
Some evidence suggests Sam Houston and his compatriots planned the revolt in a tavern at Wash. in 1834 .
When the fighting began , a stream of volunteers from Arkansas and the southeastern states flowed through the town toward the Texas battle fields .
When the Mexican-American War began in 1846 , Wash. became a rendezvous for volunteer troops .
Arkansas refused to join the Confederate States of America until after United States President Abraham Lincoln called for troops to respond to the Confederate attack upon Fort Sumter , South Carolina .
The State of Arkansas declared its secession from the Union on May 6 , 1861 .
While not often cited in historical accounts , the state was the scene of numerous small-scale battles during the American Civil War .
Also of note was Major General Thomas C. Hindman .
A former United States Representative , Hindman commanded Confederate forces at the Battle of Cane Hill and Battle of Prairie Grove .
The state came under almost exclusive control of carpetbaggers lead by newly elected Governor Powell Clayton , marking a time of great upheaval and racial violence in the state between state militia and the Ku Klux Klan .
In 1874 , the Brooks-Baxter War , a political struggle between factions of the Republican Party shook Little Rock and the state governorship .
It was settled only when President Ulysses S. Grant ordered Joseph Brooks to disperse his militant supporters .
Following the Brooks-Baxter War , a new state constitution was ratified re-enfranchising former Confederates .
It also brought new development into different parts of the state , including the Ozarks , where some areas were developed as resorts .
In a few years at the end of the 19th century , for instance , Eureka Springs in Carroll County grew to 10,000 people , rapidly becoming a tourist destination and the fourth largest city of the state .
It appealed to a wide variety of classes , becoming almost as popular as Hot Springs .
Democrats wanted to prevent their alliance .
By 1900 the Democratic Party expanded use of the white primary in county and state elections , further denying blacks a part in the political process .
Only in the primary was there any competition among candidates , as Democrats held all the power .
The state was a Democratic one-party state for decades , until after the Civil Rights Act of 1964 and Voting Rights Act of 1965 were passed .
By the fall of 1959 , the Little Rock high schools were completely integrated .
Bill Clinton , the 42nd President of the United States , was born in Hope , Ark. .
Before his presidency , Clinton served as the 40th and 42nd Governor of Ark. , a total of nearly twelve years .
As of 2006 , Arkansas has an estimated population of 2,810,872 , which is an increase of 29,154 , or 1.1 % , from the prior year and an increase of 105,756 , or 4.0 % , since the year 2000 .
Immigration from outside the U.S. resulted in a net increase of 21,947 people , and migration within the country produced a net increase of 35,664 people .
From 2000 through 2006 Arkansas has had a population growth of 5.1 % or 137,472 .
The center of population of Arkansas is located in the far northeast corner of Perry County .
Asian Americans made up 1.1 % of the state 's population .
Multiracial Americans made up 1.4 % of the state 's population .
In 2006 , Ark. has a larger percentage of tobacco smokers than the national average , with 24.0 % of adults smoking .
The largest denominations by number of adherents in 2000 were the Southern Baptist Convention with 665,307 ; the United Methodist Church with 179,383 ; the Roman Catholic Church with 115,967 ; and the American Baptist Association with 115,916 .
Its per capita household median income ( in current dollars ) for 2004 was $ 35,295 , according to the U.S. Census Bureau .
Several global companies are headquartered in the northwest corner of Arkansas , including Wal-Mart ( the world 's largest public corporation by revenue in 2007 ) , J.B. Hunt and Tyson Foods .
In recent years , automobile parts manufacturers have opened factories in eastern Arkansas to support auto plants in other states .
The first $ 9,000 of military pay of enlisted personnel is exempt from Arkansas tax ; officers do not have to pay state income tax on the first $ 6,000 of their military pay .
Residents of Texarkana , Ark. are exempt from Ark. income tax ; wages and business income earned there by residents of Texarkana , Texas are also exempt .
Arkansas 's gross receipts ( sales ) tax and compensating ( use ) tax rate is currently 6 % .
Along with the state sales tax , there are more than 300 local taxes in Arkansas .
Little Rock National Airport ( Adams Field ) and Northwest Arkansas Regional Airport in Highfill in Benton County are Arkansas 's main air terminals .
Passenger service is also available at Fort Smith , as well as limited service at Texarkana , Russellville , Pine Bluff , Harrison , Ozark Regional Airport Mountain Home , Hot Springs , El Dorado and Jonesboro .
Many air travelers in eastern Ark. use Memphis International Airport .
The Amtrak Texas Eagle passenger train makes several stops in Ark. daily on its run from Chicago to San Antonio to L.A. .
The current Governor of Ark. is Mike Beebe , a Democrat , who was elected on November 7 , 2006 .
Three seats are held by Democrats -- Robert Marion Berry ( map ) , Vic Snyder ( map ) , and Mike Ross ( map ) .
The state 's lone Republican congressman is John Boozman ( map ) .
The Democratic Party holds super-majority status in the Arkansas General Assembly .
A majority of local and statewide offices are also held by Democrats .
Arkansas had the distinction in 1992 of being the only state in the country to give the majority of its vote to a single candidate in the presidential election -- native son Bill Clinton -- while every other state 's electoral votes were won by pluralities of the vote among the three candidates .
Arkansas has become more reliably Republican in presidential elections in recent years .
The state voted for John McCain in 2008 by a margin of 20 percentage points , making it one of the few states in the country to vote more Republican than it had in 2004 .
In the latter area , Republicans have been known to get 90 percent or more of the vote .
The rest of the state is more Democratic .
Although Democrats have an overwhelming majority of registered voters , Arkansas Democrats tend to be slightly more moderate than their national counterparts , particularly outside Little Rock .
Two of Ark. ' three Democratic congressmen are members of the Blue Dog Coalition , which tends to be more pro-business , pro-military , and socially conservative than the center-left Democratic mainstream .
In Ark. , the lieutenant governor is elected separately from the governor and thus can be from a different political party .
Arkansas governors served two-year terms until a referendum lengthened the term to four years , effective with the 1986 general election .
Some of Arkansas 's counties have two county seats , as opposed to the usual one seat .
Arkansas is the only state to specify the pronunciation of its name by law ( AR-kan-saw ) .
It is the largest in Arkansas .
The Fayetteville-Springdale-Rogers metropolitan area is increasingly important to the state and its economy .
