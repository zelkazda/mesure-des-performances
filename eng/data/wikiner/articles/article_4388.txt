Most of the previous misunderstandings about motion and force were eventually corrected by Sir Isaac Newton ; with his mathematical insight , he formulated laws of motion that remained unchanged for nearly three hundred years .
By the early 20th century , Einstein developed a theory of relativity that correctly predicted the action of forces on objects with increasing momenta near the speed of light , and also provided insight into the " forces " produced by gravitation and inertia .
Analysis of the characteristics of forces ultimately culminated in the work of Archimedes who was especially famous for formulating a treatment of buoyant forces inherent in fluids . .
Avicenna also developed the idea of momentum , when attempting to provide a quantitive relation between the weight and velocity of a moving body .
His theory of motion was also consistent with the concept of inertia , an idea that was also expressed by Alhazen , who stated that a body moves perpetually unless an external force stops it or changes its direction of motion .
Avempace developed the concept that there is always a reaction force for every force exerted .
Averroes defined and measured force as " the rate at which work is done in changing the kinetic condition of a material body " and correctly theorized that " the effect and measure of force is change in the kinetic condition of a materially resistant mass . "
Many of these concepts were later incorporated in the theory of impetus , which reached its most developed form under Jean Buridan in the 14th century .
Sir Isaac Newton sought to describe the motion of all objects using the concepts of inertia and force , and in doing so , he found that they obey certain conservation laws .
This law is an extension of Galileo 's insight that constant velocity was associated with a lack of net force ( see a more detailed description of this below ) .
Albert Einstein extended the principle of inertia further when he explained that reference frames subject to constant acceleration , such as those free-falling toward a gravitating object , were physically equivalent to inertial reference frames .
Notable physicists , philosophers and mathematicians who have sought a more explicit definition of the concept of " force " include Ernst Mach , Clifford Truesdell and Walter Noll .
These were all formulated and experimentally verified before Isaac Newton expounded his three laws of motion .
Galileo realized that simple velocity addition demands that the concept of an " absolute rest frame " did not exist .
Galileo concluded that motion in a constant velocity was completely equivalent to rest .
Simple experiments showed that Galileo 's understanding of the equivalence of constant velocity and rest to be correct .
Michael Faraday and James Clerk Maxwell demonstrated that electric and magnetic forces were unified through one consistent theory of electromagnetism .
Einstein tried and failed at this endeavor , but currently the most popular approach to answering this question is string theory .
What we now call gravity was not identified as a universal force until the work of Isaac Newton .
Galileo was instrumental in describing the characteristics of falling objects by determining that the acceleration of every object in free-fall was constant and independent of the mass of the object .
The formalism was exact enough to allow mathematicians to predict the existence of the planet Neptune before it was observed .
When Albert Einstein finally formulated his theory of general relativity ( GR ) he turned his attention to the problem of Mercury 's orbit and found that his theory added a correction which could account for the discrepancy .
The origin of electric and magnetic fields would not be fully explained until 1864 when James Clerk Maxwell unified a number of earlier theories into a set of 20 scalar equations , which were later reformulated into 4 vector equations by Oliver Heaviside and Willard Gibbs .
