Richard Buckminster " Bucky " Fuller ( July 12 , 1895 -- July 1 , 1983 ) was an American architect , author , designer , inventor , and futurist .
Fuller published more than 30 books , inventing and popularizing terms such as " Spaceship Earth " , ephemeralization , and synergetics .
Fuller earned a machinist 's certification , and knew how to use the press brake , stretch press , and other tools and equipment used in the sheet metal trade .
He was expelled from Harvard twice : first for spending all his money partying with a vaudeville troupe , and then , after having been readmitted , for his " irresponsibility and lack of interest . "
Between his sessions at Harvard , Fuller worked in Canada as a mechanic in a textile mill , and later as a labourer for the meat-packing industry .
He also served in the U.S. Navy in World War I , as a shipboard radio operator , as an editor of a publication , and as a crash-boat commander .
Although the geodesic dome had been created some 30 years earlier by Dr. Walther Bauersfeld , Fuller was awarded US patents .
To prove his design , and to awe non-believers , Fuller hung from the structure 's framework several students who had helped him build it .
The U.S. government recognized the importance of his work , and employed him to make small domes for the army .
For the next half-century , Fuller developed many ideas , designs and inventions , particularly regarding practical , inexpensive shelter and transportation .
He documented his life , philosophy and ideas scrupulously by a daily diary ( later called the Dymaxion Chronofile ) , and by twenty-eight publications .
From 1959 to 1970 , Fuller taught at Southern Illinois University Carbondale .
Fuller believed human societies would soon rely mainly on renewable sources of energy , such as solar-and wind-derived electricity .
Fuller was awarded 28 US patents and many honorary doctorates .
In 1960 , he was awarded the Frank P. Brown Medal from The Franklin Institute .
Fuller 's last filmed interview took place on April 3 , 1983 , in which he presented his analysis of Simon Rodia 's Watts Towers as a unique embodiment of the structural principles found in nature .
Portions of this interview appear in I Build the Tower , a documentary film on Rodia 's architectural masterpiece .
Fuller died on July 1 , 1983 , 11 days before his 88th birthday .
During the period leading up to his death , his wife had been lying comatose in a Los Angeles hospital , dying of cancer .
Buckminster Fuller was an early environmental activist .
He was very aware of the finite resources the planet has to offer , and promoted a principle that he termed " ephemeralization " , which , in essence -- according to futurist and Fuller disciple Stewart Brand -- Fuller coined to mean " doing more with less " .
Fuller also introduced synergetics , an encompassing term which he used broadly as a metaphoric language for communicating experiences using geometric concepts and , more specifically , to reference the empirical study of systems in transformation , with an emphasis on total system behavior unpredicted by the behavior of any isolated components .
Fuller coined this term long before the term synergy became popular .
Buckminster Fuller was one of the first to propagate a systemic worldview , and he explored principles of energy and material efficiency in the fields of architecture , engineering and design .
Fuller was concerned about sustainability and about human survival under the existing socio-economic system , yet remained optimistic about humanity 's future .
Defining wealth in terms of knowledge , as the " technological ability to protect , nurture , support , and accommodate all growth needs of life, " his analysis of the condition of " Spaceship Earth " caused him to conclude that at a certain time during the 1970s , humanity had attained an unprecedented state .
Fuller also claimed that the natural analytic geometry of the universe was based on arrays of tetrahedra .
Fuller was most famous for his lattice shell structures -- geodesic domes , which have been used as parts of military radar stations , civic buildings , environmental protest camps and exhibition attractions .
However , the original design came from Dr. Walther Bauersfeld .
The patent for geodesic domes was awarded during 1954 , part of Fuller 's exploration of nature 's constructing principles to find design solutions .
Fuller worked with professional colleagues for three years beginning in 1932 on a design idea Fuller had derived from aircraft technologies .
The aerodynamic , somewhat tear-shaped body was large enough to seat eleven people and was about 18 feet ( 5.5 m ) long , resembling a blend of a light aircraft ( without wings ) and a Volkswagen van of 1950s vintage .
Fuller contributed a great deal of his own money to the project , in addition to funds from one of his professional collaborators .
Fuller anticipated the cars could travel on an open highway safely at up to about 160 km/h ( 100 miles per hour ) , but , in practise , they were difficult to control and steer above 80 km/h ( 50 mph ) .
Fuller referred to these buildings as monolithic ferroconcrete geodesic domes .
The ability to build large complex load bearing concrete spanning structures in free space would open many possibilities in architecture , and is considered as one of Fuller 's greatest contributions .
Fuller was a frequent flier , often crossing time zones .
Fuller introduced a number of concepts , and helped develop others .
However , contrary to Fuller 's hopes , domes are not an everyday sight in most places .
Fuller was followed ( historically ) by other designers and architects , such as Sir Norman Foster and Steve Baer , willing to explore the possibilities of new geometries in the design of buildings , not based on conventional rectangles .
Buckminster Fuller spoke and wrote in a unique style and said it was important to describe the world as accurately as possible .
Fuller often created long run-on sentences and used unusual compound words ( omniwell-informed , intertransformative , omni-interaccommodative , omniself-regenerative ) as well as terms he himself invented .
The words " down " and " up " , according to Fuller , are awkward in that they refer to a planar concept of direction inconsistent with human experience .
" World-around " is a term coined by Fuller to replace " worldwide " .
Fuller held that unthinking use of obsolete scientific ideas detracts from and misleads intuition .
As well as contributing significantly to the development of tensegrity technology , Fuller invented the term " tensegrity " , a portmanteau of tensional integrity .
These were three words that Fuller used repeatedly to describe his design .
Fuller also helped to popularise the concept of Spaceship Earth : " The most important fact about Spaceship Earth : an instruction manual did n't come with it . "
Among the many designers and architects who were influenced by Buckminster Fuller are :
