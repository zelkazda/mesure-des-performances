He disappeared after 1888 , but circumstantial evidence suggests that he died in the Great Hinckley Fire in 1894 , although this remains impossible to substantiate .
Corbett was born in London , England .
His family emigrated to New York City .
It has been suggested that the fumes of mercury used in the hatter 's trade caused Corbett 's later mental problems .
Corbett married , but his wife died in childbirth .
Following her death , he moved to Boston , and continued working as a hatter .
In an attempt to imitate Jesus , he began to wear his hair very long .
On July 16 , 1858 , in order to avoid the temptation of prostitutes , Corbett castrated himself with a pair of scissors .
Corbett would later testify for the prosecution in the trial of the commandant of Andersonville , Captain Henry Wirz .
Herold surrendered , but Booth remained inside .
Corbett was positioned near a large crack in the barn wall .
He saw Booth moving about inside and shot him with his Colt revolver despite Secretary of War Edwin McMasters Stanton 's orders that Booth should be taken alive .
Booth was struck in the neck , the bullet damaging his spinal cord , and he died a few hours later .
Corbett was immediately arrested for violation of his orders , but Stanton later had the charges dropped .
Stanton remarked , " The rebel is dead .
Corbett received his share of the reward money , amounting to $ 1,653.84 .
In his official statement , Corbett claimed he shot Booth because he thought Lincoln 's assassin was preparing to use his weapons .
After his discharge from the army in August 1865 , Corbett went back to work as a hatter , first in Boston , later in Conn. , and by 1870 in New Jersey .
In 1887 , because of his fame as Booth 's killer , Corbett was appointed assistant doorkeeper of the Kansas House of Representatives in Topeka .
He is thought to have died in the Great Hinckley Fire of September 1 , 1894 .
A small sign also was placed to mark the dug hole where Corbett for a time had lived .
