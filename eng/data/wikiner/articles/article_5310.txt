For example , in their nominative singular forms Polish nouns are typically feminine if they have the ending-a , neuter when they end with-o ,-e , or-ę , and masculine if they have no gender suffix ( null morpheme ) .
On the whole , gender marking has been lost in Welsh , both on the noun , and often , on the adjective .
These languages have different pronouns and inflections in the third person only to differentiate between people and inanimate objects ( and even this distinction is commonly waived in spoken Finnish ) .
The following " highly contrived " Old English sentence serves as an example of gender agreement .
Old English had three genders , masculine , feminine and neuter , but gender inflections were greatly simplified by sound changes , and then completely lost ( as well as number inflections , to a lesser extent ) .
In a sense , the neuter gender has grown to encompass most nouns , including many that were masculine or feminine in Old English .
However , in some local dialects of German , all nouns for female persons have shifted to the neuter gender , but the feminine gender remains for some words denoting objects .
Similarly , there are two Swedish words for " boat . "
But their Portuguese translations are feminine : viagem , paisagem , coragem .
The Latin word via , from whom both variants ( viaje and viagem ) derived was feminine .
Also , in Polish the word księżyc " moon " is masculine , but its Russian counterpart луна is feminine .
The Russian word for " sun " , солнце , is neuter , while Latin word of the same archaic root sol is masculine .
Sometimes the gender switches : Russian тополь ( poplar ) is now masculine , but less than 200 years ago ( in writings of Lermontov ) it was feminine .
The modern loanword виски ( from whisky/whiskey ) was originally feminine ( in a translation of Jack London stories , 1915 ) , then masculine ( in a song of Alexander Vertinsky , 1920s or 1930s ) , and today it has become neuter ( the masculine variant is typically considered archaic , and the feminine one is completely forgotten ) .
In Polish kometa ( comet ) is nowadays feminine , but less than 200 years ago ( in writing of Mickiewicz ) it was masculine .
In Polish , the nouns mężczyzna " man " and książę " prince " are masculine , even though words with the ending -a are normally feminine and words that end with -ę are usually neuter .
Most German nouns give no morphological or semantic clue as to their gender .
Note however that the word " gender " derives from Latin genus ( also the root of genre ) originally meant " kind " , so it does not necessarily have a sexual meaning .
Another example is Polish , which can be said to distinguish five genders : personal masculine ( referring to male humans ) , animate non-personal masculine , inanimate masculine , feminine , and neuter .
Belarusian and Ukrainian have the same system as Russian .
The Dyirbal language is well known for its system of four noun classes , which tend to be divided along the following semantic lines :
The Ngangikurrunggurr language has noun classes reserved for canines , and hunting weapons , and the Anindilyakwa language has a noun class for things that reflect light .
The Diyari language distinguishes only between female and other objects .
Some languages have only two classes , while the Bats language has eight .
The Andi language has a noun class reserved for insects .
Ubykh shows some inflections along the same lines , but only in some instances , and in some of these instances inflection for noun class is not even obligatory .
The Zande language distinguishes four noun classes :
Many of the exceptions have a round shape , and some can be explained by the role they play in Zande mythology .
In Basque there are two classes , animated and inanimated ; however , the only difference is in the declension of locative cases ( inessive , locative genitive , adlative , terminal adlative , ablative and directional ablative ) .
There are a few words with both masculine and feminine forms , generally words for relatives ( cousin : lehengusu ( m ) /lehengusina ( f ) ) or words borrowed from Latin ( " king " : errege , from the Latin word regem ; " queen " : erregina , from reginam ) .
One of the examples he provides is the Israeli word for " brush " : mivréshet .
