It might also have been a pun on " all-has-read " , since Lovecraft was an avid reader in youth .
Ibn al-Haytham is said to have pretended to be mad to escape the wrath of a ruler .
Again according to Lovecraft 's " History " :
Alhazred had been kidnapped in Damascus and brought to the Nameless City , where he had earlier studied and learned some of the Necronomicon ' s lore .
