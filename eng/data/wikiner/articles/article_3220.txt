There are also Danish language communities in Argentina , the U.S. and Canada .
Moreover , the øy diphthong changed into ø as well , as in the Old Norse word for " island " .
Danish is the national language of Denmark , and one of two official languages of the Faroes ( alongside Faroese ) .
Until 2009 , it had also been one of two official languages of Greenland ( alongside Greenlandic ) .
In addition , there is a small community of Danish speakers in Southern Schleswig , the portion of Germany bordering Denmark , where it is an officially recognized regional language , just as German is north of the border .
Furthermore , Danish is one of the official languages of the European Union and one of the working languages of the Nordic Council .
There is no law stipulating an official language for Denmark , making Danish the de facto language only .
Standard Danish ( rigsdansk ) is the language based on dialects spoken in and around the capital , Copenhagen .
More than 25 % of all Danish speakers live in the metropolitan area of the capital and most government agencies , institutions and major businesses keep their main offices in Copenhagen , something that has resulted in a very homogeneous national speech norm .
In contrast , though Oslo ( Norway ) and Stockholm ( Sweden ) are quite dominant in terms of speech standards , cities like Bergen , Göteborg and the Malmö -- Lund region are large and influential enough to create secondary regional norms , making the standard language more varied than is the case with Danish .
Danish is divided into three distinct dialect groups :
The background for this lies in the loss of the originally Danish provinces of Blekinge , Halland and Scania to Sweden in 1658 .
The sound system of Danish is in many ways unique among the world 's languages .
At the beginning of a word or after a consonant , it is pronounced as a uvular fricative , [ ʁ ] , but in most other positions it is either realized as a non-syllabic low central vowel , [ ɐ̯ ] ( which is almost identical to how /r/ is often pronounced in German ) or simply coalesces with the preceding vowel .
For example the present tense form of the Danish infinitive verb spise ( " to eat " ) is spiser ; this form is the same regardless of whether the subject is in the first , second , or third person , or whether it is singular or plural .
Standard Danish nouns fall into only two grammatical genders : common and neuter , while some dialects still often have masculine , feminine and neuter .
While the majority of Danish nouns ( ca. 75 % ) have the common gender , and neuter is often used for inanimate objects , the genders of nouns are not generally predictable and must in most cases be memorized .
Danish words are largely derived from the Old Norse language , with new words formed by compounding .
A large percentage of Danish words , however , hail from Middle Low German ( for example , betale = to pay ) .
One peculiar feature of the Danish language is the fact that numerals from 40 through 90 are ( somewhat like the French numerals 80 and 90 ) based on a vigesimal system , formerly also used in Norwegian and Swedish .
Thus , in modern Danish forty is called fyrre derived from the original fyrretyve ( i.e. four score ) , which is now only used in extremely formal settings or for humorous effect .
For large numbers ( one billion or larger ) , Danish uses the long scale , so that e.g. one U.S. billion ( 1,000,000,000 ) is called milliard , and one U.S. trillion ( 1,000,000,000,000 ) is billion .
The same spelling reform changed the spelling of a few common words , such as the past tense vilde ( would ) , kunde ( could ) and skulde ( should ) , to their current forms of ville , kunne and skulle ( making them identical to the infinitives in writing , as they are in speech ) , and did away with the practice of capitalizing all nouns , which is still done in German .
