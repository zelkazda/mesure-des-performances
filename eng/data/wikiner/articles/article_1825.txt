From the merger to 1999 Bob Young was Red Hat 's CEO .
After leaving Red Hat he started Lulu , a self-publishing web-site that claims to be the world 's fastest-growing provider of print-on-demand books .
He is Lulu 's CEO .
Young also co-founded Linux Journal in 1994 , and in 2003 , he purchased the Hamilton Tiger-Cats , a Canadian Football League franchise .
He launched the prize partly as a means of promoting Lulu .
Young is strong believer in user-generated content and open source software .
Joyce Young , Bob Young 's aunt , purchased stock in Red Hat Inc. shortly after its founding .
When Red Hat 's stock rose significantly after its initial public offering in 1999 , they sold enough stock to recoup their initial investment , and retained some stocks .
The couple also made significant donations to the Royal Military College of Canada .
