In addition , her father was a minister in the Augustana Evangelical Lutheran Church , Lutheran Church in America , and later the Evangelical Lutheran Church in America .
In 1963 , she graduated from Washburn High School .
In March , she moved into Los Angeles proper in west Los Angeles .
Once she returned , she continued being politically active and was involved with the Symbionese Liberation Army .
Hall died in a shootout ( May 17 , 1974 ) with police in which five other SLA members were killed .
