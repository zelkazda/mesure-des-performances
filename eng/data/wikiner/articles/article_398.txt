by Aegisthus , the lover of his wife Clytemnestra , who herself slew Cassandra , Agamemnon 's unfortunate concubine , as she clung to him .
In old versions of the story : " The scene of the murder , when it is specified , is usually the house of Aegisthus , who has not taken up residence in Agamemnon 's palace , and it involves an ambush and the deaths of Agamemnon 's followers too " .
In some later versions Clytemnestra herself does the killing , or they do it together , in his own home .
This is a possible prototype of the Agamemnon of mythology .
Agamemnon 's life was shadowed under prophecy .
Atreus , Agamemnon 's father , murdered the children of his twin brother Thyestes , excepting Aegisthus , and fed them to Thyestes , who vowed gruesome revenge on Atreus ' children .
Aegisthus took possession of the throne of Mycenae and ruled jointly with Thyestes .
During this period Agamemnon and his brother , Menelaus , took refuge with Tyndareus , king of Sparta .
There they respectively married Tyndareus 's daughters Clytemnestra and Helen .
Agamemnon and Clytemnestra had four children : one son , Orestes , and three daughters , Iphigenia , Electra and Chrysothemis .
Menelaus succeeded Tyndareus in Sparta , while Agamemnon , with his brother 's assistance , drove out Aegisthus and Thyestes to recover his father 's kingdom .
Finally , the prophet Calchas announced that the wrath of the goddess could only be propitiated by the sacrifice of Agamemnon 's daughter Iphigenia .
Classical dramatisations differ on how willing either father or daughter were to this fate , some include such trickery as claiming she was to be married to Achilles , but Agamemnon did eventually sacrifice Iphigenia .
Hesiod said she became the goddess Hecate .
Agamemnon 's teamster , Halaesus , later fought with Aeneas in Italy .
The Iliad tells the story of the quarrel between Agamemnon and Achilles in the final year of the war .
Agamemnon took an attractive slave , one of the spoils of war , Briseis from Achilles .
Although not the equal of Achilles in bravery , Agamemnon was a dignified representative of kingly authority .
After a stormy voyage , Agamemnon and Cassandra landed in Argolis or were blown off course and landed in Aegisthus ' country .
Clytemnestra , Agamemnon 's wife , had taken a lover , Aegisthus , son of Thyestes , and when Agamemnon came home he was treacherously slain either by Aegisthus ( in the oldest versions of the story ) or by his wife , Clytemnestra .
According to the account given by Pindar and the tragedians , Agamemnon was slain by his wife alone in a bath , a blanket of cloth or a net having first been thrown over him to prevent resistance .
Clytemnestra also killed Cassandra .
Her wrath at the sacrifice of Iphigenia , her jealousy of Cassandra , and Agamemnon 's having gone to war over Helen , are variously said to have been motives for her crime .
1602 ) , thus explaining Aegisthus ' action as justified by his father 's curse .
Agamemnon 's murder was eventually avenged by his son Orestes with the help or encouragement of his sister Electra , by murdering Aegisthus and their own mother , and so enduring the wrath of the Erinyes , winged goddesses who would track down egregiously impious wrongdoers ( like Orestes who commit matricide ) with their hounds ' noses and drive men to insanity .
The fortunes of Agamemnon have formed the subject of numerous tragedies , ancient and modern , the most famous being the Oresteia of Aeschylus .
His tomb was pointed out among the ruins of Mycenae and at Amyclae .
In works of art there is considerable resemblance between the representations of Zeus , king of the gods , and Agamemnon , king of men .
She was also one of two horses driven by Menelaus at the funeral games of Patroclus .
Recent interpretations depict Agamemnon in a completely different light .
In the end , during the Sack of Troy , he attacks Briseis , whose romance with Achilles nearly cost him the Trojan War , and tells her she will be his personal slave .
This portrayal of Agamemnon was an invention of the writers of the movie and has practically nothing in common with the ancient sources in which he is a noble hero as opposed to a ruthless tyrant .
In Terry Gilliam 's film Time Bandits , Agamemnon is played by Sean Connery .
Agamemnon possesses a strong temper and a prideful streak similar to Achilles .
He is not as strong as Achilles , nor is he an exceptional warrior .
Agamemnon clearly has a stubborn streak that one can argue makes him extremely arrogant .
As a king Agamemnon , it can perhaps be argued , is loyal , as he insists upon leading a battle for his brother , Menelaus ' stolen bride ; though this may well be only a pretext for a raid for booty .
Unlike Achilles , Agamemnon 's deepest concern is for himself , and therefore he sees others , depending on how they relate to him .
