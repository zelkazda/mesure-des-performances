Korzybski ( 1879 -- 1950 ) had determined that two forms of the verb ' to be ' -- the ' is ' of identity and the ' is ' of predication -- had structural problems .
Korzybski pointed out the circularity of many dictionary definitions , and suggested adoption of the convention , then recently introduced among mathematicians , of acknowledging some minimal ensemble of terms as necessarily ' undefined ' ; he chose ' structure ' , ' order ' , and ' relation ' .
For instance , Arabic , like Russian , lacks a verb form of " to be " in the present tense .
If one wanted to assert , in Arabic , that an apple looks red , one would not literally say " the apple is red " , but " the apple red " .
Similarly , the Ainu language consistently does not distinguish between " be " and " become " ; thus ne means both " be " and " become " , and pirka means " good " , " be good " , and " become good " equally .
Noam Chomsky , " [ r ] egarded as the father of modern linguistics " , commented on Korzybski 's " insight " :
