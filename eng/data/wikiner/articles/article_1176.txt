The Arno is a river in the Tuscany region of Italy .
It is the most important river of central Italy after the Tiber .
The river originates on Mount Falterona in the Casentino area of the Apennines , and takes initially a southward curve .
The river turns to the west near Arezzo passing through Florence , Empoli and Pisa , flowing into the Tyrrhenian Sea at Marina di Pisa .
It crosses Florence , where it passes below the Ponte Vecchio and the Santa Trìnita bridge ( built by Bartolomeo Ammanati , but inspired by Michelangelo ) .
The flow rate of the Arno is irregular .
At the point where the Arno leaves the Apennines , flow measurements can vary between 0.56 m³/s and 3,540 m³/s .
New dams built upstream of Florence have greatly alleviated the problem in recent years .
