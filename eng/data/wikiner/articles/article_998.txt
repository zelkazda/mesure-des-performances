The early life of the sculptor Andreas Schlüter is obscured as at least three different persons of that names are documented .
He later created statues for King John III Sobieski 's Wilanów Palace in Warsaw and sepulchral sculptures in Żółkiew ( Zhovkva ) .
In 1689 , he moved to Warsaw and made the pediment reliefs and sculptural work of Krasiński Palace .
While the more visible reliefs on the outside had to praise fighting , the statues of dying warriors in the interior denounced war and gave an indication of his pacifist religious beliefs ( he is said to have been a Mennonite ) .
The Berlin City Palace , and many of his works , were partially destroyed by bombing in World War II and by the subsequent Communist regime .
A similar fate probably befell the Amber Room , made between 1701 and 1709 , Schlüter 's most famous work of architecture .
In 1713 Schlüter 's fame brought him to work for Tsar Peter the Great in Saint Petersburg , where he died of an illness after creating several designs .
