The formula was discovered independently by Leonhard Euler and Colin Maclaurin around 1735 ( and later generalized as Darboux 's formula ) .
Euler needed it to compute slowly converging infinite series while Maclaurin used it to calculate integrals .
The Euler -- MacLaurin summation formula then follows as an integral over the latter .
