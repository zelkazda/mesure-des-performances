An example of such a state within the common law jurisdiction , and using the black letter legal doctrine is Canada .
Sometimes this is referred to as " hornbook law " meaning treatise or textbook , often relied upon as authoritative , competent , and generally accepted in the field of Canadian law .
