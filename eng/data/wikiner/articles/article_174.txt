This is primarily due to the widespread usage of the Aramaic language as both a lingua franca and the official language of the Neo-Assyrian , and its successor , the Achaemenid Empire .
Its widespread usage led to the gradual adoption of the Aramaic alphabet for writing the Hebrew language .
After the fall of the Achaemenid Empire , the unity of the Imperial Aramaic script was lost , diversifying into a number of descendant cursives .
The Old Turkic script evident in epigraphy from the 8th century likely also has its origins in the Aramaic script
Mandaic is written in the Mandaic alphabet .
