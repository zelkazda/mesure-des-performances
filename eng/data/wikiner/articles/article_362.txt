ALGOL is a family of imperative computer programming languages originally developed in the mid 1950s which greatly influenced many other languages and became the de facto way algorithms were described in textbooks and academic works for almost the next 30 years .
It was designed to avoid some of the perceived problems with FORTRAN and eventually gave rise to many other programming languages , including BCPL , B , Pascal , Simula , C , and many others .
ALGOL introduced code blocks and the begin and end pairs for delimiting them and it was also the first language implementing nested function definitions with lexical scope .
Fragments of ALGOL-like syntax are sometimes still used as pseudocode ( notations used to describe algorithms for human readers ) .
The official ALGOL versions are named after the year they were first published .
The International Algorithmic Language ( IAL ) was extremely influential and generally considered the ancestor of most of the modern programming languages ( the so-called Algol-like languages ) .
Additionally , in computer science , ALGOL object code was a simple and compact and stack-based instruction set architecture mainly used in teaching compiler construction and other high order language ( of which Algol is generally considered the first ) .
C. A. R. Hoare remarked : " Here is a language so far ahead of its time that it was not only an improvement on its predecessors but also on nearly all its successors . "
However , call-by-name is still beloved of ALGOL implementors for the interesting " thunks " that are used to implement it .
Here 's an example of how to produce a table using Elliott 803 ALGOL .
