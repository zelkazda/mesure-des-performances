Ashoka , popularly known as Ashoka the Great , was an Indian emperor of the Maurya Dynasty who ruled almost all of the Indian subcontinent from 269 BC to 232 BC .
One of India 's greatest emperors , Ashoka reigned over most of present-day India after a number of military conquests .
He conquered the kingdom named Kalinga , which no one in his dynasty had conquered starting from Chandragupta Maurya .
His reign was headquartered in Magadha ( present-day Bihar , India ) .
Ashoka was a devotee of ahimsa ( nonviolence ) , love , truth , tolerance and vegetarianism .
Ashoka is remembered in history as a philanthropic administrator .
His name " aśoka " means " without sorrow " in Sanskrit ( a= no/without , soka= sorrow or worry ) .
Renowned British author and social critic H. G. Wells in his bestselling two-volume work , The Outline of History ( 1920 ) , wrote of emperor Ashoka :
After two thousand years , the influence of Ashoka is seen in Asia and especially the Indian subcontinent .
An emblem excavated from his empire is today the national Emblem of India .
Ashoka had several elder siblings ( all half-brothers from other wives of Bindusara ) .
Because of his exemplary intellect and warrior skills , he was said to have been the favorite of his grandfather Chandragupta Maurya .
Ashoka found the sword and kept it , in spite of his grandfather 's warning .
Ashoka , in his adolescence , was rude and naughty .
Ashoka was very well known for his sword fighting .
Ashoka was a frightening warrior and a heartless general .
Because of this quality he was sent to destroy the riot of Avanti .
Developing into an impeccable warrior general and a shrewd statesman , Ashoka went on to command several regiments of the Mauryan army .
His growing popularity across the empire made his elder brothers wary of his chances of being favored by Bindusara to become the next emperor .
Taxshila was a highly volatile place because of the war-like Indo-Greek population and mismanagement by Susima himself .
Ashoka complied and left for the troubled area .
As news of Ashoka 's visit with his army trickled in , he was welcomed by the revolting militias and the uprising ended without a conflict .
( The province revolted once more during the rule of Ashoka , but this time the uprising was crushed with an iron fist )
Ashoka 's success made his stepbrothers more wary of his intentions of becoming the emperor and more incitements from Susima led Bindusara to send Ashoka into exile .
He went into Kalinga and stayed there incognito .
There he met a fisher woman named Kaurwaki , with whom he fell in love .
Meanwhile , there was again a violent uprising in Ujjain .
Emperor Bindusara summoned Ashoka out of exile after two years .
Ashoka went into Ujjain and in the ensuing battle was injured , but his generals quelled the uprising .
Ashoka was treated in hiding so that loyalists of the Susima group could not harm him .
The following year passed quite peacefully for him , and Devi was about to deliver his first child .
In the meanwhile , Emperor Bindusara died .
As the news of the unborn heir to the throne spread , Prince Susima planned the execution of the unborn child ; however , the assassin who came to kill Devi and her child killed his mother instead .
Ashoka beheads his elder brother to ascend the throne .
While the early part of Ashoka 's reign was apparently quite bloodthirsty , he became a follower of the Buddha 's teaching after his conquest of Kalinga on the east coast of India in the present-day state of Orissa .
Kalinga was a state that prided itself on its sovereignty and democracy .
The pretext for the start of the Kalinga War ( 265 BC or 263 BC ) is uncertain .
One of Susima 's brothers might have fled to Kalinga and found official refuge there .
This enraged Ashoka immensely .
He was advised by his ministers to attack Kalinga for this act of treachery .
Ashoka then asked Kalinga 's royalty to submit before his supremacy .
When they defied this diktat , Ashoka sent one of his generals to Kalinga to make them submit .
The general and his forces were , however , completely routed through the skilled tact of Kalinga 's commander-in-chief .
Ashoka , baffled at this defeat , attacked with the greatest invasion ever recorded in Indian history until then .
Kalinga put up a stiff resistance , but they were no match for Ashoka 's brutal strength .
The whole of Kalinga was plundered and destroyed .
Ashoka 's later edicts state that about 100,000 people were killed on the Kalinga side and 10,000o from Ashoka 's army .
As the legend goes , one day after the war was over , Ashoka ventured out to roam the city and all he could see were burnt houses and scattered corpses .
During the remaining portion of Ashoka 's reign , he pursued an official policy of nonviolence ( ahimsa ) .
Limited hunting was permitted for consumption reasons but Ashoka also promoted the concept of vegetarianism .
Ashoka also showed mercy to those imprisoned , allowing them leave for the outside a day of the year .
He is acclaimed for constructing hospitals for animals and renovating major roads throughout India .
His edicts , which talk of friendly relations , give the names of both Antiochus of the Seleucid empire and Ptolemy III of Egypt .
The fame of the Mauryan empire was widespread from the time that Ashoka 's grandfather Chandragupta Maurya defeated Seleucus Nicator , the founder of the Seleucid Dynasty .
The source of much of our knowledge of Ashoka is the many inscriptions he had carved on pillars and rocks throughout the empire .
One also gets some primary information about the Kalinga War and Ashoka 's allies plus some useful knowledge on the civil administration .
The Ashoka Pillar at Sarnath is the most popular of the relics left by Ashoka .
It has a four-lion capital ( four lions standing back to back ) which was adopted as the emblem of the modern Indian republic .
The lion symbolizes both Ashoka 's imperial rule and the kingship of the Buddha .
In translating these monuments , historians learn the bulk of what is assumed to have been true fact of the Mauryan Empire .
It is difficult to determine whether or not some actual events ever happened , but the stone etchings clearly depict how Ashoka wanted to be thought of and remembered .
Ashoka 's own words as known from his Edicts are : " All men are my children .
Ashoka also claims that he encouraged the development of herbal medicine , for human and nonhuman animals , in their territories :
Ashoka ruled for an estimated forty years .
After his death , the Mauryan dynasty lasted just fifty more years .
Ashoka had many wives and children , but many of their names are lost to time .
Mahindra and Sanghamitra were twins born by his first wife , Devi , in the city of Ujjain .
In his old age , he seems to have come under the spell of his youngest wife Tishyaraksha .
It is said that she had got his son Kunala , the regent in Takshashila , blinded by a wily stratagem .
In Pataliputra , Ashoka hears Kunala 's song , and realizes that Kunala 's misfortune may have been a punishment for some past sin of the emperor himself and condemns Tishyaraksha to death , restoring Kunala to the court .
Kunala was succeeded by his son , Samprati , but his rule did not last long after Ashoka 's death .
The reign of Ashoka Maurya could easily have disappeared into history as the ages passed by , and would have had he not left behind a record of his trials .
What Ashoka left behind was the first written language in India since the ancient city of Harappa .
Pusyamitra Sunga founded the Sunga dynasty ( 185 BC-78 BC ) and ruled just a fragmented part of the Mauryan Empire .
Many of the northwestern territories of the Mauryan Empire ( modern-day Iran , Afghanistan and Pakistan ) became the Indo-Greek Kingdom .
In 1992 , Ashoka was ranked # 53 on Michael H. Hart 's list of the most influential figures in history .
In 2001 , a semi-fictionalized portrayal of Ashoka 's life was produced as a motion picture under the title Asoka .
Following Ashoka 's example , kings established monasteries , funded the construction of stupas , and supported the ordination of monks in their kingdom .
Many rulers also took an active role in resolving disputes over the status and regulation of the sangha , as Ashoka had in calling a conclave to settle a number of contentious issues during his reign .
Ashoka also said that all his courtiers were true to their self and governed the people in a moral manner .
His main interests were Sanchi and Sarnath besides Harappa and Mohenjodaro .
Edicts of Ashoka-The Edicts of Ashoka are a collection of 33 inscriptions on the Pillars of Ashoka , as well as boulders and cave walls , made by the Emperor Ashoka of the Mauryan dynasty during his reign from 272 to 231 BC .
It is very important in dating the consecration of the Maurya emperor Ashoka .
Sanghamitta 's birth -- 281 BC
Third Buddhist council -- 250 -- 253 BC
Ashoka also helped to develop viharas ( intellectual hubs ) such as Nalanda and Taxila .
Ashoka helped to construct Sanchi and Mahabodhi Temple .
Ashoka also helped to organize the Third Buddhist council ( c. 250 BC ) at Pataliputra ( today 's Patna ) .
In his neighboring countries Ashoka helped humans as well as animals .
Ashoka also planted trees in his empire and his neighboring countries .
Ashoka was perhaps the first emperor in human history to ban slavery , hunting , fishing and deforestation .
Ashoka also banned the death sentence and asked the same for the neighboring countries .
Ashoka commanded his people to serve the orders of their elders parents ) and religious monks .
Ashoka also recommended his people study all religions and respect all religions .
According to Ashoka , to harm another 's religion is a harm to someone 's owns religion .
Ashoka asked people to live with harmony , peace , love and tolerance .
Ashoka called his people as his children , and they could call him when they need him .
Ashoka also believed in dharmacharana ( dhammacharana ) and dharmavijaya ( dhammavijaya ) .
Ashoka 's aim was not to expand the territories but the welfare of all of his subjects ( sarvajansukhay ) .
Ashoka was the true devotee of nonviolence , peace and love .
Ashoka was against any discrimination among humans .
According to Ashoka , hatred gives birth to hatred and a feeling of love gives birth to love and mercy .
Ashoka allowed females to be educated .
The Ashoka chakra was built by Ashoka during his reign .
Chakra is a Sanskrit word which also means cycle or self repeating process .
Sarvepalli Radhakrishnan , who later became India 's first Vice President , clarified the adopted flag and described its significance as follows :
Originally , there must have been many pillars of Ashoka although only ten with inscriptions still survive .
It was originally placed atop the Aśoka pillar at Sarnath , now in the state of Uttar Pradesh , India .
The four animals in the Sarnath capital are believed to symbolize different steps of Lord Buddha 's life .
Besides the religious interpretations , there are some non-religious interpretations also about the symbolism of the Ashoka capital pillar at Sarnath .
According to them , the four lions symbolize Ashoka 's rule over the four directions , the wheels as symbols of his enlightened rule and the four animals as symbols of four adjoining territories of India .
