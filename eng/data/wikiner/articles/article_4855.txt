The General Motors Company , also known as GM , is a United States-based automaker with its headquarters in Detroit , Michigan .
The company plans to focus its business on its four core North American brands : Chevrolet , Buick , GMC , and Cadillac .
In Europe , following a period of negotiation to sell a majority stake in its Opel and Vauxhall brands , the company decided to retain full ownership of these operations .
In 2009 , GM employed approximately 244,500 people around the world .
The Renaissance Center , located in Detroit , Mich. , United States , is GM 's global headquarters .
In 2008 , GM sold 8.35 million cars and trucks globally .
GM has had collaborations with various automakers including Fiat ( see GM/Fiat Premium platform ) and the Ford Motor Company .
[ citation needed ] GM retains various stakes in different automakers .
GM 's best success internationally has been its performance in China , where GM 's sales rose 66.9 percent in 2009 , selling 1,830,000 vehicles and accounting for 13.4 percent of the market .
GM is structured into the following segments :
On December 4 , 2009 , GM announced leadership changes in a press release .
GM is a conglomerate .
The company was founded on September 16 , 1908 , in Flint , Michigan , as a holding company for Buick , then controlled by William C. Durant .
GM 's co-founder was Charles Stewart Mott , whose carriage company was merged into Buick prior to GM 's creation .
It acquired Oldsmobile later that year .
In 1909 , Durant brought in Cadillac , Elmore , Oakland and several others .
Durant lost control of GM in 1910 to a bankers ' trust , because of the large amount of debt taken on in its acquisitions coupled with a collapse in new vehicle sales .
Durant took back control of the company after one of the most dramatic proxy wars in American business history .
Durant then reorganized General Motors Company into General Motors Corporation .
Alfred P. Sloan was picked to take charge of the corporation and led it to its post-war global dominance .
This unprecedented growth of GM would last into the early 1980s when it employed 349,000 workers and operated 150 assembly plants .
GM previously led in global sales for 77 consecutive years ( 1931 to 2008 ) , longer than any other automaker .
GM 's remaining pre-petition creditors ' claims are paid from the remaining assets of Motors Liquidation Company , the new name of the former General Motors Corporation , although the directors of that company believe its debts far outweigh its assets .
This means that while the former GM 's bondholders may recover a small portion of their investment , former GM shareholders ( now shareholders of Motors Liquidation Company ) will likely not receive anything .
Also on July 10 , 2009 , GM announced plans to trim its U.S. workforce by 20,000 employees as part of its reorganization by the end of 2009 due to economic conditions .
The following table is a comparison ( estimates ) of the new GM and the old GM :
In North America , GM will focus primarily on its four core brands -- Chevrolet , Cadillac , Buick , and GMC -- while selling , discontinuing , or scaling back its other brands .
In the middle of 2005 , GM announced that its corporate chrome emblem " Mark of Excellence " would begin appearing on all recently introduced and all-new 2006 model vehicles produced and sold in North America .
However , in 2009 the " New GM " reversed this , saying that emphasis on its four core brands would dictate downplaying the GM name .
During the first 6 months of 2008 , GM lost $ 18.8 billion ; by late October , its stock had dropped 76 percent , and it was considering a merger with Chrysler .
In only twelve months ( October 2007 -- 2008 ) , GM sales in the U.S. dropped 45 percent .
GM has yet to confirm future product plans for the idled facilities .
" As part of General Motors Company ( GM ) 's restructuring , it plans to revive one of its idled U.S. factories for the production of a small car .
The Buick brand is especially strong in China , led by the Buick Excelle subcompact .
The last emperor of China owned a Buick .
The Cadillac brand was introduced in China in 2004 , starting with exports to China .
GM pushed the marketing of the Chevrolet brand in China in 2005 as well , moving the former Buick Sail to that marque .
The SAIC-GM-Wuling Automobile joint-venture is also successfully selling trucks and vans under the Wuling marque ( 34 percent owned by GM ) .
GM increased its sales in China by 68 percent to 230,048 vehicles in March 2010 , outsold its U.S. sales of 188,546 by 22 percent .
And the company said it is " on track " to sell more than 2 million vehicles in China in 2010 , four years ahead of its plan .
GM plans to invest $ 250 million to create a research facility in Shanghai to develop hybrid cars and alternative fuel vehicles .
On September 24 , 2007 , GM workers represented by the United Auto Workers union went on the first nationwide strike against the company since 1970 .
The ripple effect of the strike reached into Canada the following day as two car assembly plants and a transmission facility were forced to close .
By the following day , all GM workers in both countries were back to work .
A new labor contract was ratified by UAW members exactly one week after the tentative agreement was reached , passing by a majority 62 percent vote .
Also included is a VEBA ( Voluntary Employee Beneficiary Association ) which will transfer retiree health care obligations to the UAW by 2010 .
This eliminates more than $ 50 billion from GM 's healthcare tab .
It will be funded by $ 30 billion in cash and $ 1.4 billion in GM stock paid to the UAW over the next four years of the contract .
Together with the UAW , GM created a joint venture dedicated to the quality of life needs of employees in 1985 .
In response , CAW members staged a twelve-day blockade of the GM Canada headquarters .
After further discussions with the CAW , GM agreed to compensate workers at the truck plant , as well as making product commitments for the Oshawa car assembly plant .
Canada .
The closure was announced in 2008 during contract talks with CAW and GM .
GM announced elimination of lifetime health benefits for about 100,000 of its white collar retirees at the end of 2008 .
In particular , the Chevrolet Corvette has long been popular and successful in international road racing .
GM also is a supplier of racing components , such as engines , transmissions , and electronics equipments .
GM has also done much work in the development of electronics for GM auto racing .
Recently , the Cadillac V-Series has entered motorsports racing .
GM has also used many cars in the American racing series NASCAR .
Currently the Chevrolet Impala is the only entry in the series but in the past the Pontiac Grand Prix , Buick Regal , Oldsmobile Cutlass , Chevrolet Lumina , Chevrolet Malibu , and the Chevrolet Monte Carlo were also used .
Chevrolet competes with a Chevrolet Cruze in the FIA World Touring Car Championship ( WTCC ) .
In 1987 , GM , in conjunction with AeroVironment , built the Sunraycer , which won the inaugural World Solar Challenge and was a showcase of advanced technology .
These hybrids did not use electrical energy for propulsion , like GM 's later designs .
GM has hinted at new hybrid technologies to be employed that will be optimized for higher speeds in freeway driving .
GM currently offers two types of hybrid systems .
GM was the first American company ( in the modern era ) to release an all-electric automobile .
It was the first car with zero-emissions marketed in the US in over three decades .
In 1999 GM decided to cease production of the vehicles .
All of the EV1 's were eventually returned to General Motors and , with the exception of a few which were donated to museums , all were destroyed .
General Motors has announced that it is building a prototype two-seat electric vehicle with Segway .
On September 16 , 2008 , as part of its 100th anniversary celebration , GM unveiled the " production " version of the Chevrolet Volt at the GM headquarters in Detroit .
GM also plans to build an automotive battery laboratory in Michigan .
GM will be responsible for battery management systems and power electronics .
The company will build a new factory in Michigan , but a specific site has yet to be announced , in part because negotiations are ongoing with state and local authorities on the usual financial incentives and approvals .
GM has prided its research and prototype development of hydrogen powered vehicles , to be produced in early 2010 , using a support infrastructure still in a prototype state .
The economic feasibility of the technically challenging hydrogen car , and the low-cost production of hydrogen to fuel it , has also been discussed by other automobile manufacturers such as Ford and Chrysler .
Due to the success and rapid consumer acceptance of the flex versions , GM sold 192,613 flex vehicles and 135,636 gasoline-powered automobiles in 2005 , jumping to 501,681 flex-fuel vehicles , while only 949 cars and 6,834 light trucks powered by gasoline were sold in 2007 , and reaching new car sales of 535.454 flex fuels in 2008 , representing 97 percent of all cars and light duty trucks sold in that year .
General Motors is a leading contributor to charity .
In 2004 , GM gave $ 51,200,000 in cash contributions and $ 17,200,000 in-kind donations to charitable causes .
There was also 10000 cubic yards ( 7600 m 3 ) of contaminated sludge from the active wastewater treatment plant on the General Motors property .
In September 2006 , the state of Calif. filed suit against General Motors , Chrysler , Nissan , Toyota , Honda , and Ford .
The companies were accused of producing cars that emitted over 289 million metric tons of carbon per year in the U.S. , accounting for nearly 20 percent of carbon emissions in the United States and 30 percent of carbon emissions in California .
The Union of Concerned Scientists ranked General Motors as seventh out of the eight world 's largest automakers in 2007 for environmental performance .
