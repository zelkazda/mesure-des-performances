He won the Nobel Prize in Literature in 1954 .
Hemingway 's fiction was successful because the characters he presented exhibited authenticity that reverberated with his audience .
His first novel , The Sun Also Rises , was written in 1924 .
After divorcing Hadley Richardson in 1927 Hemingway married Pauline Pfeiffer ; they divorced following Hemingway 's return from covering the Spanish Civil War , after which he wrote For Whom the Bell Tolls .
Both were well-educated and well-respected in the conservative community of Oak Park .
Frank Lloyd Wright , a resident of Oak Park , said of the village : " So many churches for so many good people to go to " .
Hemingway claimed to dislike his name , which he " associated with the naive , even foolish hero of Oscar Wilde 's play The Importance of Being Earnest " .
Hemingway 's mother frequently performed in concerts around the village .
Her insistence that he learn to play the cello became a " source of conflict " , but he later admitted the music lessons were useful in his writing , as in the " contrapunctal structure " of For Whom the Bell Tolls .
Hemingway attended Oak Park and River Forest High School from 1913 until 1917 .
Like Mark Twain , Stephen Crane , Theodore Dreiser and Sinclair Lewis , Hemingway was a journalist before becoming a novelist ; after leaving high school he went to work for The Kansas City Star as a cub reporter .
Although he stayed there for only six months he relied on the Star 's style guide as a foundation for his writing : " Use short sentences .
He described the incident in his non-fiction book Death in the Afternoon : " I remember that after we searched quite thoroughly for the complete dead we collected fragments " .
A few days later he was stationed at Fossalta di Piave .
Still only eighteen , Hemingway said of the incident : " When you go to war as a boy you have a great illusion of immortality .
Hemingway spent six months in the hospital , where he met and fell in love with Agnes von Kurowsky , a Red Cross nurse seven years his senior .
During his six months in recuperation Hemingway met and formed a strong friendship with " Chink " Dorman-Smith that lasted for decades .
Hemingway returned home early in 1919 to a time of readjustment .
The trip became the inspiration for his short story " Big Two-Hearted River " , in which the semi-autobiographical character Nick Adams takes to the country to find solitude after returning from war .
Hadley was red-haired , with a " nurturing instinct " , and eight years older than Hemingway .
Despite the difference in age , Hadley , who had an overprotective mother , seemed less mature than usual for a young woman her age .
There Hemingway would meet writers such as Gertrude Stein , James Joyce and Ezra Pound who " could help a young writer up the rungs of a career " .
A regular at Stein 's salon , Hemingway met influential painters such as Pablo Picasso , Joan Miro , and Juan Gris .
However , Hemingway eventually withdrew from Stein 's influence and their relationship deteriorated into a literary quarrel that spanned decades .
They forged a strong friendship and in Hemingway , Pound recognized and fostered a young talent .
Hemingway was devastated on learning that Hadley had lost a suitcase filled with his manuscripts at the Gare de Lyon as she was traveling to Geneva to meet him in December 1922 .
During their absence Hemingway 's first book , Three Stories and Ten Poems , was published .
Hemingway helped Ford Madox Ford edit the Transatlantic Review in which were published works by Pound , John Dos Passos , and Gertrude Stein as well as some of Hemingway 's own early stories such as " Indian Camp " .
When In Our Time ( with capital letters ) was published in 1925 , the dust jacket had comments from Ford .
Six months earlier , Hemingway met F. Scott Fitzgerald , and the pair formed a friendship of " admiration and hostility " .
The trip inspired Hemingway 's first novel , The Sun Also Rises , which he began to write immediately after the fiesta , finishing in September .
Hemingway decided to slow his pace and devoted six months to the novel 's rewrite .
Scribner 's published the novel in October .
The Sun Also Rises epitomized the post-war expatriate generation , received good reviews and is " recognized as Hemingway 's greatest work " .
However , Hemingway himself later wrote to his editor Max Perkins that the " point of the book " was not so much about a generation being lost , but that " the earth abideth forever " ; he believed the characters in The Sun Also Rises may have been " battered " but were not lost .
Hemingway 's marriage to Hadley deteriorated as he was working on The Sun Also Rises .
They split their possessions while Hadley accepted Hemingway 's offer of the proceeds from The Sun Also Rises .
After a honeymoon in Le Grau-du-Roi , where he contracted anthrax , Hemingway planned his next collection of short stories , Men Without Women , published in October 1927 .
When Hemingway was asked about the scar he was reluctant to answer .
Hemingway was devastated , having earlier sent a letter to his father telling him not to worry about financial difficulties ; the letter arrived minutes after the suicide .
He realized how Hadley must have felt after her own father 's suicide in 1903 , and he suggested , " I 'll probably go the same way . "
A Farewell to Arms was published on September 27 .
While in Key West he enticed his friends to join him on fishing expeditions -- inviting Waldo Peirce , John Dos Passos , and Max Perkins -- with one all male trip to the Dry Tortugas , and he frequented the local bar , Sloppy Joe 's .
The 10-week trip provided material for Green Hills of Africa , as well as the short stories " The Snows of Kilimanjaro " and " The Short Happy Life of Francis Macomber " .
They visited Mombasa , Nairobi , and Machakos in Kenya , then on to Tanganyika , where they hunted in the Serengeti , around Lake Manyara and west and southeast of the present-day Tarangire National Park .
On his return to Key West in early 1934 Hemingway began work on Green Hills of Africa , published in 1935 to mixed reviews .
In 1935 he first arrived at Bimini , where he spent a considerable amount of time .
In 1937 Hemingway agreed to report on the Spanish Civil War for the North American Newspaper Alliance ( NANA ) .
Ivens , who was filming The Spanish Earth , needed Hemingway as a screenwriter to replace John Dos Passos , who left the project when his friend José Robles was arrested and later executed .
In the spring of 1939 , Hemingway crossed to Cuba in his boat to live in the Hotel Ambos Mundos in Havana .
As he had after his divorce from Hadley , he changed locations ; moving his primary summer residence to Ketchum , Idaho , just outside the newly built resort of Sun Valley , and his winter residence to Cuba .
He was at work on For Whom the Bell Tolls , which he started in March 1939 , finished in July 1940 , and was published in October 1940 .
Hemingway was present at heavy fighting in the Hürtgenwald near the end of 1944 .
On December 17 , a feverish and ill Hemingway had himself driven to Luxembourg to cover what would later be called The Battle of the Bulge .
However , as soon as he arrived , Lanham handed him to the doctors , who hospitalized him with pneumonia , and by the time he recovered a week later , the main fighting was over .
He was recognized for his valor in having been " under fire in combat areas in order to obtain an accurate picture of conditions " , with the commendation that " through his talent of expression , Mr. Hemingway enabled readers to obtain a vivid picture of the difficulties and triumphs of the front-line soldier and his organization in combat " .
The last time he saw her was in March 1945 , as he was preparing to return to Cuba .
Meanwhile , he had asked Mary Welsh to marry him on their third meeting .
Hemingway said he " was out of business as a writer " from 1942 to 1945 .
Hemingway became depressed as his literary friends died : in 1939 Yeats and Ford Madox Ford ; in 1940 Scott Fitzgerald ; in 1941 Sherwood Anderson and James Joyce ; in 1946 Gertrude Stein ; and the following year in 1947 , Max Perkins , Hemingway 's long time Scribner 's editor and friend .
Nonetheless , early in 1946 he began work on The Garden of Eden , finishing 800 pages by June .
The next year he wrote the draft of Old Man and the Sea in eight weeks , considering it " the best I can write ever for all of my life " .
On their way to photograph Murchison Falls from the air , the plane struck an abandoned utility pole and " crash landed in heavy brush . "
The next day , attempting to reach medical care in Entebbe , they boarded a second plane that exploded at take-off with Hemingway suffering burns and another concussion , this one serious enough to cause leaking of cerebral fluid .
They eventually arrived in Entebbe to find reporters covering the story of Hemingway 's death .
After the plane crashes , Hemingway , who had been " a thinly controlled alcoholic throughout much of his life , drank more heavily than usual to combat the pain of his injuries . "
In October 1954 Hemingway received the Nobel Prize in Literature .
He modestly told the press that Carl Sandburg , Isak Dinesen and Bernard Berenson deserved the prize , but the prize money would be welcome .
From the end of the year in 1955 to early 1956 , Hemingway was bedridden .
During the trip Hemingway became ill again , and was treated for " high blood pressure , liver disease , and arteriosclerosis " .
Back in Cuba in 1957 he started to shape the recovered work into his memoir A Moveable Feast .
By 1959 he culminated a period of intense activity : finished A Moveable Feast ( scheduled to be released the following year ) ; brought True at First Light to 200,000 words ; added chapters to The Garden of Eden ; and worked on Islands in the Stream .
The latter three were stored in a safe deposit box in Havana , as he focused on the finishing touches for A Moveable Feast .
By January he returned to Cuba and continued work on the Life magazine series .
The manuscript grew to 63,000 words -- Life wanted only 10,000 words -- and he asked A. E. Hotchner to help organize the work that would become The Dangerous Summer .
The first installments of The Dangerous Summer were published in Life in September 1960 to good reviews .
Hemingway suffered from physical problems as well : his health declined and his eyesight was failing .
He was released in late June and arrived home in Ketchum on June 30 .
Two days later , in the early morning hours of July 2 , 1961 , Hemingway " quite deliberately " shot himself with his favorite shotgun .
Hemingway 's chin , mouth , and lower cheeks were left , but the upper half of his head was blown away .
Despite his finding that Hemingway " had died of a self-inflicted wound to the head " , the story told to the press was that the death had been " accidental " .
During his final years , Hemingway 's behavior was similar to his father 's before he himself committed suicide ; his father may have had the genetic disease hemochromatosis , in which the inability to metabolize iron culminates in mental and physical deterioration .
Medical records made available in 1991 confirm that Hemingway 's hemochromatosis had been diagnosed in early 1961 .
Added to Hemingway 's physical ailments was the additional problem that he had been a heavy drinker for most of his life .
In a press interview five years later Mary Hemingway admitted her husband had committed suicide .
The New York Times wrote in 1926 of Hemingway 's first novel : " No amount of analysis can convey the quality of The Sun Also Rises .
The Sun Also Rises is written in the spare , tightly written prose , for which Hemingway is famous ; a style that has influenced countless crime and pulp fiction novels .
In 1954 , when Hemingway was awarded the Nobel Prize for Literature , it was for " his mastery of the art of narrative , most recently demonstrated in The Old Man and the Sea , and for the influence that he has exerted on contemporary style . "
Henry Louis Gates believes Hemingway 's style was fundamentally shaped " in reaction to [ his ] experience of world war " .
Hemingway referred to his style as the iceberg theory : in his writing the facts float above water ; the supporting structure and symbolism operate out-of-sight .
Hemingway believed the writer could describe one thing ( such as Nick Adams fishing in " The Big Two-Hearted River " ) though an entirely different thing occurs below the surface ( Nick Adams concentrating on fishing to the extent that he does not have to think about anything else ) .
Hemingway offers a " multi-focal " photographic reality .
In his literature , and in his personal writing , Hemingway habitually used the word " and " in place of commas .
Many of Hemingway 's followers misinterpreted his lead and frowned upon all expression of emotion ; Saul Bellow satirized this style as " Do you have emotions ?
However , Hemingway 's intent was not to eliminate emotion , but to portray it more scientifically .
Hemingway thought it would be easy , and pointless , to describe emotions ; he sculpted collages of images in order to grasp " the real thing , the sequence of motion and fact which made the emotion and which would be as valid in a year or in ten years or , with luck and if you stated it purely enough , always . "
This use of an image as an objective correlative is characteristic of Ezra Pound , T. S. Eliot , James Joyce , and Proust .
Hemingway 's letters refer to Proust 's Remembrance of Things Past several times over the years , and indicate he read the book at least twice .
His writing was likely also influenced by the Japanese poetic canon .
Although Hemingway writes about sports , Carlos Baker believes the emphasis is more on the athlete than the sport .
Feminist critics attacked Hemingway as " public enemy number one " , although more recent re-evaluations of his work " have given new visibility to Hemingway 's female characters ( and their strengths ) and have revealed his own sensitivity to gender issues , thus casting doubts on the old assumption that his writings were one-sidedly masculine . "
The theme of women and death is evident in stories as early as " Indian Camp " .
The theme of death permeates Hemingway 's work .
Young believes the emphasis in " Indian Camp " was not so much on the woman who gives birth or the father who commits suicide , but on Nick Adams who witnesses these events as a child , and becomes a " badly scarred and nervous young man . "
Young believes " Indian Camp " holds the " master key " to " what its author was up to for some thirty-five years of his writing career . "
Francis Macomber dies happy because the last hours of his life are authentic ; the bullfighter in the corrida represents the pinnacle of a life lived with authenticity .
The theme of emasculation is prevalent in Hemingway 's work , most notably in The Sun Also Rises .
Baker believes Hemingway 's work emphasizes the " natural " versus the " unnatural " .
Hemingway 's work has been characterized as misogynistic and homophobic .
She found , particularly in the 1980s , " critics interested in multiculturalism " simply ignored Hemingway ; although some " apologetics " have been written .
The extent of Hemingway 's influence is seen in the tributes and echoes of his fiction in popular culture .
Montblanc offers a Hemingway fountain pen , and a line of Hemingway safari clothes has been created .
The International Imitation Hemingway Competition was created in 1977 to publicly acknowledge his influence and the comically misplaced efforts of lesser authors to imitate his style .
