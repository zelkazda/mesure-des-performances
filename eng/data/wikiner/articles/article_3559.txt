The name was transmitted via the Latin delphinus , which in Middle Latin became dolfinus and in Old French daulphin , which reintroduced the ph into the word .
Another learned behavior was discovered among river dolphins in Brazil , where some male dolphins use weeds and sticks as part of a sexual display .
Reports of cooperative human-dolphin fishing date back to the ancient Roman author and natural philosopher Pliny the Elder .
A modern human-dolphin partnership currently operates in Laguna , Santa Catarina , Brazil .
In some parts of the world such as Taiji in Japan and the Faroe Islands , dolphins are traditionally considered as food , and killed in harpoon or drive hunts .
The Ancient Greeks welcomed dolphins ; spotting dolphins riding in a ship 's wake was considered a good omen .
The series , created by Ivan Tors , portrayed a dolphin as a kind of seagoing Lassie .
Flipper was remade in 1996 .
The 1993 movie Free Willy made a star of the Orca playing Willy , Keiko .
The 1977 horror movie Orca paints a less friendly picture of the species .
Here , a male Orca takes revenge on fishermen after the killing of his mate .
The 1973 movie The Day of the Dolphin portrayed kidnapped dolphins performing a naval military assassination using explosives .
In the U.S. , the best known are the SeaWorld marine mammal parks .
Since the late 1960s , SeaWorld 's Shamu orca shows have become popular .
In 1988 , SeaWorld had Southwest Airlines paint three aircraft in Shamu colours to promote their parks .
Organizations such as the Mote Marine Laboratory rescue and rehabilitate sick , wounded , stranded or orphaned dolphins , while others , such as the Whale and Dolphin Conservation Society and Hong Kong Dolphin Conservation Society , work on dolphin conservation and welfare .
The Vikramshila Gangetic Dolphin Sanctuary has been created in the Ganges river for the protection of the animals .
However , no evidence to support these rumors ever surfaced , and the United States Navy denies that at any point dolphins were trained for combat .
In 2000 the press reported that dolphins trained to kill by the Soviet Navy had been sold to Iran .
Dolphins play a military role for dolphins in William Gibson 's short story Johnny Mnemonic , in which cyborg dolphins find submarines and decode encrypted information .
Dolphins play a role as sentient patrollers of the sea enhanced with a deeper empathy toward humans in Anne McCaffrey 's The Dragonriders of Pern series .
Their story is told in So Long , and Thanks for All the Fish .
Much more serious is their major role in David Brin 's Uplift series .
In the book The Music of Dolphins by author Karen Hesse , dolphins raise a girl from the age of four until the coast guard eventually discovers her .
