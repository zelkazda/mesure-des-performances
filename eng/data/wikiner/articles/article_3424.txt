Diprotodon spp. fossils have been found in many places across Australia , including complete skulls and skeletons , as well as hair and foot impressions .
For example , hundreds of individuals were found in Lake Callabonna with well preserved lower bodies but crushed and distorted heads .
Diprotodon was named by Owen ( 1838 ) .
According to Gause 's " competitive exclusion principle " no two species with identical ecological requirements can coexist in a stable environment .
Diprotodonts , along with a wide range of other Australian megafauna , became extinct shortly after humans arrived in Australia about 50,000 years ago .
The recent ice ages produced no significant glaciation in mainland Australia but long periods of cold and very dry weather .
Either way , the trend is toward the modern Australian environment of highly flammable open sclerophyllous forests , woodlands and grasslands , none of which are suitable for large , slow-moving browsing animals -- and either way , the changed microclimate produces substantially less rainfall .
