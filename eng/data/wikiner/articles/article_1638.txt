The Bishopric of Brandenburg was a Roman Catholic diocese established by Otto the Great in 948 , including the territory between the Elbe on the west , the Oder on the east , and the Schwarze Elster on the south , and taking in the Uckermark to the north .
Its seat was Brandenburg upon Havel .
The diocese was originally a suffragan of Mainz , but in 968 it came under the archiepiscopal jurisdiction of Magdeburg .
