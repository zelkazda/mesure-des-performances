Modern scholars have come to question the existence of at least the first nine emperors ; and Sujin is the first many agree might have actually existed , in third or fourth century .
The name Sujin Tennō was assigned to him posthumously by later generations .
Sujin is regarded by historians as a " legendary emperor " because of the paucity of information about him , which does not necessarily imply that no such person ever existed .
If Sujin did exist , there is no evidence to suggest that the title tennō was used during the time period to which his reign has been assigned .
It is much more likely that he was a chieftain , or local clan leader , and the polity he ruled would have only encompassed a small portion of modern day Japan .
He founded some important shrines in Yamato province , sent generals to subdue local provinces and defeated a prince who rebelled against him .
He was credited with having subdued Queen Himiko or her successor ; and yet there is another theory that Himiko was a paternal great-aunt of the Emperor Sujin .
Sujin is a posthumous name .
The actual site of Sujin 's grave is not known .
The Imperial Household Agency designates this location as Sujin 's mausoleum .
