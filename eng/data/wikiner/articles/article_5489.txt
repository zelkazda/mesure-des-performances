Historical materialism is a methodological approach to the study of society , economics , and history , first articulated by Karl Marx ( 1818 -- 1883 ) .
Marx himself never used the term but referred to his approach as " the materialist conception of history . "
While this may seem obvious it was only with Marx that this was seen as foundation for understanding human society and historical development .
[ citation needed ] Marx then extended this premise by asserting the importance of the fact that , in order to carry out production and exchange , people have to enter into very definite social relations , most fundamentally production relations .
For Marx , productive forces refer to the means of production such as the tools , instruments , technology , land , raw materials , and human knowledge and abilities in terms of using these means of production .
Ancient society was based on a ruling class of slave owners and a class of slaves ; feudalism based on landowners and serfs ; and capitalism based on the capitalist class and the working class .
Marx identified the production relations of society ( arising on the basis of given productive forces ) as the economic base of society .
The theory shows what Marx called a " coherence " in human history , because each generation inherits the productive forces developed previously and in turn further develops them before passing them on to the next generation .
In his analysis of the movement of history , Marx predicted the breakdown of capitalism ( as a result of class struggle and the falling rate of profit ) , and the establishment in time of a communist society in which class-based human conflict would be overcome .
This sketch is abstract -- real historical understanding needed for developing political strategy and tactics must involve " concrete analysis of concrete conditions " ( V.I. Lenin ) .
Capitalism , Marx argued , completely separates the economic and political forces , leaving them to have relations through a limiting government .
Marx 's attachment to materialism arose from his doctoral research on the philosophy of Epicurus , as well as his reading of Adam Smith and other writers in classical political economy .
Friedrich Engels wrote : " I use ' historical materialism ' to designate the view of the course of history , which seeks the ultimate causes and the great moving power of all important historic events in the economic development of society , in the changes in the modes of production and exchange , with the consequent division of society into distinct classes and the struggles of these classes . "
Marx himself took care to indicate that he was only proposing a guideline to historical research , and was not providing any substantive " theory of history " or " grand philosophy of history " , let alone a " master-key to history " .
Numerous times , he and Engels expressed irritation [ citation needed ] with dilettante academics who sought to knock up their skimpy historical knowledge as quickly as possible into some grand theoretical system that would explain " everything " about history .
But having abandoned abstract philosophical speculation in his youth , Marx himself showed great reluctance during the rest of his life about offering any generalities or universal truths about human existence or human history .
The first explicit and systematic summary of the materialist interpretation of history published , Anti-Dühring , was written by Friedrich Engels .
Marx goes on to illustrate how the same factors can in different historical contexts produce very different results , so that quick and easy generalisations are not really possible .
But what is true is that insofar as Marx and Engels regarded historical processes as law-governed processes , the possible future directions of historical development were to a great extent limited and conditioned by what happened before .
Towards the end of his life , Engels commented several times about the abuse of historical materialism .
Finally , in a letter to Franz Mehring , Frederick Engels dated 14 July 1893 , Engels stated :
" ... there is only one other point lacking , which , however , Marx and I always failed to stress enough in our writings and in regard to which we are all equally guilty .
In 1880 , about three years before Marx died , Friedrich Engels indicated that he accepted the usage of the term " historical materialism " .
In his old age , Engels speculated about a new cosmology or ontology which would show the principles of dialectics to be universal features of reality .
Isaac Deutscher provides an anecdote about the knowledge of Marx in that era :
But , he said , Karl Kautsky had read it , and written a popular summary of the first volume .
He had n't read this either , but Kazimierz Kelles-Krauz , the party theoretician , had read Kautsky 's pamphlet and summarised it .
In the early years of the 20th century , historical materialism was often treated by socialist writers as interchangeable with dialectical materialism , a formulation never used by Friedrich Engels however .
Jürgen Habermas believes historical materialism " needs revision in many respects " , especially because it has ignored the significance of communicative action .
Regulation theory , especially in the work of Michel Aglietta draws extensively on historical materialism .
