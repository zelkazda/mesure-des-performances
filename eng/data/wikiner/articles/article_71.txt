The Atlantic Ocean is the second-largest of the world 's oceanic divisions .
The oldest known mention of " Atlantic " is in The Histories of Herodotus around 450 BC ( I 202 ) ; see also : Atlas Mountains .
Another name historically used was the ancient term Ethiopic Ocean , derived from Ethiopia , whose name was sometimes used as a synonym for all of Africa and thus for the ocean .
As one component of the interconnected global ocean , it is connected in the north to the Arctic Ocean ( which is sometimes considered a sea of the Atlantic ) , to the Pacific Ocean in the southwest , the Indian Ocean in the southeast , and the Southern Ocean in the south .
( Other definitions describe the Atlantic as extending southward to Antarctica . )
The equator subdivides it into the North Atlantic Ocean and South Atlantic Ocean .
It connects to the Arctic Ocean through the Denmark Strait , Greenland Sea , Norwegian Sea and Barents Sea .
To the east , the boundaries of the ocean proper are Europe , the Strait of Gibraltar ( where it connects with the Mediterranean Sea , one of its marginal seas and , in turn , the Black Sea ) and Africa .
In the southeast , the Atlantic merges into the Indian Ocean .
The 20° East meridian , running south from Cape Agulhas to Antarctica defines its border .
In the southwest , the Drake Passage connects it to the Pacific Ocean .
The man-made Panama Canal links the Atlantic and Pacific .
Besides those mentioned , other large bodies of water adjacent to the Atlantic are the Caribbean Sea , the Gulf of Mexico , Hudson Bay , the Arctic Ocean , the Mediterranean Sea , the North Sea , the Baltic Sea , and the Celtic Sea .
The land that drains into the Atlantic covers four times that of either the Pacific or Indian oceans .
The volume of the Atlantic with its adjacent seas is 354,700,000 cubic kilometers ( 85,100,000 cu mi ) and without them 323,600,000 cubic kilometres ( 77,640,000 cu mi ) .
The average depth of the Atlantic , with its adjacent seas , is 3339 metres ( 10955 ft ) ; without them it is 3926 metres ( 12881 ft ) .
The greatest depth , 8605 metres ( 28232 ft ) , is in the Puerto Rico Trench .
The Atlantic 's width varies from 2848 kilometres ( 1770 mi ) between Brazil and Sierra Leone to over 6400 km ( 4000 mi ) in the south .
Today , it can be referred to in a humorously diminutive way as the Pond in idioms , in reference to the geographical and cultural divide between North America and Europe .
The principal feature of the bathymetry ( bottom topography ) is a submarine mountain range called the Mid-Atlantic Ridge .
The South Atlantic Ocean has an additional submarine ridge , the Walvis Ridge .
The Mid-Atlantic Ridge separates the Atlantic Ocean into two large troughs with depths from 3700 -- 5500 metres ( 12100 -- 18000 ft ) .
On average , the Atlantic is the saltiest major ocean ; surface water salinity in the open ocean ranges from 33 to 37 parts per thousand ( 3.3 -- 3.7 % ) by mass and varies with latitude and season .
The Atlantic Ocean consists of four major water masses .
Within the North Atlantic , ocean currents isolate the Sargasso Sea , a large elongated body of water , with above average salinity .
The south tides in the Atlantic Ocean are semi-diurnal ; that is , two high tides occur during each 24 lunar hours .
Hurricanes develop in the southern part of the North Atlantic Ocean .
The Atlantic Ocean appears to be the second youngest of the five oceans .
The Atlantic has been extensively explored since the earliest settlements along its shores .
As a result , the Atlantic became and remains the major artery between Europe and the Americas ( known as transatlantic trade ) .
The term Ethiopian Ocean sometimes appeared until the mid-19th century .
The Atlantic has contributed significantly to the development and economy of surrounding countries .
Besides major transatlantic transportation and communication routes , the Atlantic offers abundant petroleum deposits in the sedimentary rocks of the continental shelves .
The Atlantic hosts the world 's richest fishing resources , especially in the waters covering the shelves .
From October to June the surface is usually covered with sea ice in the Labrador Sea , Denmark Strait , and Baltic Sea .
The Mid-Atlantic Ridge , a rugged north-south centerline for the entire Atlantic basin , first discovered by the Challenger Expedition dominates the ocean floor .
The Atlantic has irregular coasts indented by numerous bays , gulfs , and seas .
These include the Norwegian Sea , Baltic Sea , North Sea , Labrador Sea , Black Sea , Gulf of Saint Lawrence , Bay of Fundy , Gulf of Maine , Mediterranean Sea , Gulf of Mexico , and Caribbean Sea .
The Atlantic harbors petroleum and gas fields , fish , marine mammals ( seals and whales ) , sand and gravel aggregates , placer deposits , polymetallic nodules , and precious stones .
Icebergs are common from February to August in the Davis Strait , Denmark Strait , and the northwestern Atlantic and have been spotted as far south as Bermuda and Madeira .
Municipal pollution comes from the eastern U.S. , southern Brazil , and eastern Argentina ; oil pollution in the Caribbean Sea , Gulf of Mexico , Lake Maracaibo , Mediterranean Sea , and North Sea ; and industrial waste and municipal sewage pollution in the Baltic Sea , North Sea , and Mediterranean Sea .
In 2005 , there was some concern that warm northern European currents were slowing down , but no scientific consensus formed from that evidence .
