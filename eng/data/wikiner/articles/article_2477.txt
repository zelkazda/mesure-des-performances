Corcovado , meaning " hunchback " in Portuguese , is a mountain in central Rio de Janeiro , Brazil .
The 710-metre ( 2329 ft ) granite peak is located in the Tijuca Forest , a national park .
Corcovado hill lies just west of the city center but is wholly within the city limits and visible from great distances .
It is known worldwide for the 38-meter ( 125 ft ) statue of Jesus atop its peak , entitled Cristo Redentor or " Christ the Redeemer " .
The peak and statue can be accessed via a narrow road or by the 3.8 kilometer ( 2.4 mi ) Corcovado Rack Railway which was opened in 1884 and refurbished in 1980 .
The most popular attraction of Corcovado mountain is the statue and viewing platform at its peak , drawing over 300,000 visitors per year .
The peak of Corcovado is a big granite dome , which describes a generally vertical rocky formation .
Corcovado is a famous bossa nova song written by Antônio Carlos Jobim in the mid-1960s .
