Kournikova 's professional tennis career has been curtailed for the past several years , and possibly ended , by serious back and spinal problems .
Anna says : " I played two times a week from age five .
In 1989 , at the age of eight , Anna began appearing in junior tournaments , and by the following year , was attracting attention from tennis scouts across the world .
Following her arrival in the United States , Anna exploded onto the tennis scene , making her the internationally recognized tennis star she is today .
She debuted in professional tennis at age 14 in the Fed Cup for Russia , the youngest player ever to participate and win .
The same year Kournikova reached her first WTA Tour doubles final at the Kremlin Cup .
After beating Shi-Ting Wang in the first round of Italian Open with 6 -- 3 , 6 -- 4 , Kournikova lost to Amanda Coetzer 6 -- 2 , 4 -- 6 , 6 -- 1 in the second .
Kournikova then lost to Anke Huber 6 -- 0 , 6 -- 1 in the first round of the Los Angeles Open , also reaching the doubles semifinals with Ai Sugiyama .
At the 1997 US Open she lost in the second round to the eleventh seed Irina Spîrlea 6 -- 1 , 3 -- 6 , 6 -- 3 .
Kournikova played her last WTA Tour event in 1997 at Porsche Tennis Grand Prix in Filderstadt , losing to Amanda Coetzer 3 -- 6 6 -- 3 6 -- 4 in the second round of singles , and 6 -- 2 , 6 -- 4 in the first round of doubles to Lindsay Davenport and Jana Novotná with Likhovtseva .
She also scored impressive victories over Martina Hingis , Lindsay Davenport , Steffi Graf and Monica Seles .
She also partnered Larisa Neiland in doubles , and lost to Elena Likhovtseva and Caroline Vis in the quarterfinals 3 -- 6 , 6 -- 2 , 7 -- 5 .
She then reached the second round of both singles and doubles at the Medibank International in Sydney , losing to Lindsay Davenport in the second round of singles with 6 -- 2 , 6 ( 4 ) -- 7 , 6 -- 3 .
She also partnered with Larisa Neiland in women 's doubles , and they lost to eventual champions Hingis and Mirjana Lučić 7 -- 5 , 6 -- 2 in the second round .
Although she lost in the second round of Paris Open to Anke Huber in singles , Kournikova reached the her second doubles WTA Tour final partnering with Larisa Neiland .
They lost to Sabine Appelmans and Miriam Oremans 1 -- 6 , 6 -- 3 , 7 -- 6 ( 3 ) .
In singles , Kournikova reached the third round .
Kournikova then reached two consecutive quarterfinals , at Amelia Island and Italian Open , losing , respectively , 7 -- 5 , 6 -- 3 to Lindsay Davenport , and 6 -- 2 , 6 -- 4 to Martina Hingis .
At the German Open , she reached the semifinals in both singles and doubles , with Larisa Neiland , losing 6 -- 0 , 6 -- 1 to Conchita Martínez and 6 -- 1 , 6 -- 4 to Alexandra Fusai and Nathalie Tauziat , respectively .
At the 1998 French Open Kournikova reached her best result at this tournament , losing to Jana Novotná 6 ( 2 ) -- 7 , 6 -- 3 , 6 -- 3 in the fourth round .
During her quarterfinals match at the Eastbourne Open versus Steffi Graf , Kournikova injured her thumb , which would eventually force her to withdraw from the 1998 Wimbledon Championships .
However , she won that match 6 ( 4 ) -- 7 , 6 -- 3 , 6 -- 4 , but then withdraw from her semifinals match against Arantxa Sánchez Vicario .
Kournikova returned for the Du Maurier Open , defeating Alexandra Fusai and Ruxandra Dragomir before losing to Conchita Martínez in the third round 6 -- 0 , 6 -- 3 .
At the Pilot Pen Tennis in New Haven she lost to Amanda Coetzer 1 -- 6 , 6 -- 4 , 7 -- 5 in the second round .
At the 1998 US Open Kournikova reached the fourth round and lost to Arantxa Sánchez Vicario .
She then made a series of low results at Toyota Princess Cup , Porsche Tennis Grand Prix , Zürich Open and Kremlin Cup , but due to good results during the year , she qualified for the 1998 WTA Tour Championships .
She lost to Monica Seles in the first round 6 -- 4 , 6 -- 3 .
She then played at the Australian Open , losing to Mary Pierce 6 -- 0 , 6 -- 4 in the fourth round .
However , Kournikova won her first doubles grand slam title , partnering Martina Hingis .
The two defeated Lindsay Davenport and Natasha Zvereva in the final .
Kournikova then lost in the quarterfinals of Toray Pan Pacific Open to Monica Seles 7 -- 5 , 6 -- 3 .
In Oklahoma City she was defeated by Amanda Coetzer 6 -- 4 , 6 -- 2 in the semifinals , by Silvia Farina Elia in the first round of Evert Cup , and by Barbara Schett at Lipton Championships .
At Tier I Family Circle Cup , Kournikova reached her second WTA Tour final , but lost to Martina Hingis 6 -- 4 , 6 -- 3 .
She then defeated Jennifer Capriati , Lindsay Davenport and Patty Schnyder on her route to the Bausch & Lomb Championships semifinals , losing to Ruxandra Dragomir 6 -- 3 , 7 -- 5 .
After round robin results at Italian Open and German Open , Kournikova reached the fourth round of 1999 French Open , losing to eventual champion Steffi Graf 6 -- 3 , 7 -- 6 .
At 1999 Wimbledon Championships , Kournikova lost to Venus Williams in the fourth round 3 -- 6 , 6 -- 3 , 6 -- 2 .
She also reached the 1999 Wimbledon final in mixed doubles , partnering with Jonas Björkman , but they lost to Leander Paes and Lisa Raymond 6 -- 4 , 3 -- 6 , 6 -- 3 .
Also at times during 1999 , she was the most searched athlete in the world on Yahoo ! , the premier search engine of the day .
Kournikova was more successful in doubles that season .
Anna Kournikova and Martina Hingis were presented with the WTA Award for Doubles Team of the Year .
She then reached the singles semifinals at Medibank International Sydney , losing to Lindsay Davenport 6 -- 3 , 6 -- 2 .
At the 2000 Australian Open , she reached the fourth round in singles and the semifinals in doubles .
That season , Kournikova reached eight semifinals ( Sydney , Scottsdale , Stanford , San Diego , Luxembourg , Leipzig and 2000 WTA Tour Championships ) , seven quarterfinals and one final .
Despite being a domestic player , Kournikova lost to Martina Hingis 6 -- 3 , 6 -- 1 in the final of Kremlin Cup .
Kournikova was , once again , more successful in doubles .
She reached the final of the 2000 US Open in mixed doubles , partnering with Max Mirnyi , but they lost to Jared Palmer and Arantxa Sánchez Vicario 6 -- 4 , 6 -- 3 .
Her 2001 season was dominated by injury , including a left foot stress fracture which forced her withdrawal from twelve tournaments , including the French Open and Wimbledon .
She reached her second career grand slam quarterfinals , at the Australian Open .
Kournikova then withdrew from several events due to continuing problems with her left foot and did not return until Leipzig .
With Barbara Schett , she won the doubles title in Sydney .
Hingis and Kournikova also won the Kremlin Cup .
Kournikova was quite successful in 2002 .
This was Kournikova 's last singles finals and the last chance to win a single title .
With Martina Hingis , Anna Kournikova lost in the finals of Sydney , but they won their second grand slam title together , Australian Open in women 's doubles .
They also lost in the quarterfinals of U.S. Open .
With Chanda Rubin , Anna Kournikova played the semifinals of Wimbledon , but they lost to Serena and Venus Williams .
In 2003 , Anna Kournikova collected her first grand slam match victory in two years at the Australian Open .
She defeated Henrieta Nagyová in the 1st round , and then lost to Justine Henin-Hardenne in the 2nd round .
Kournikova retired in the 1st round of the Charleston due to a left adductor strain .
As a personality Kournikova was among the most common search strings for both articles and images in her prime .
Kournikova has not played on the WTA Tour since 2003 , but still plays exhibition matches for charitable causes .
In late 2004 , she participated in three events organized by Elton John and by fellow tennis players Serena Williams and Andy Roddick .
In January 2005 , she played in a doubles charity event for the Indian Ocean tsunami with John McEnroe , Andy Roddick , and Chris Evert .
In November 2005 , she teamed up with Martina Hingis , playing against Lisa Raymond and Samantha Stosur in the WTT finals for charity .
Kournikova is also a member of the St. Louis Aces in the World Team Tennis ( WTT ) , playing doubles only .
She won that race for women 's K-Swiss team .
She partnered Tim Wilkison and Karel Nováček .
She played doubles with Andy Roddick ( they were coached by Elton John ) versus Martina Navratilova and Jesse Levine ; Kournikova and Roddick won 5 -- 4 ( 3 ) .
She is the current K-Swiss spokesperson .
In a feature for Elle magazine 's July 2005 issue , Kournikova stated that if she were 100 % fit , she would like to come back and compete again .
As a player , Kournikova was noted for her footspeed and aggressive baseline play , and excellent angles and dropshots ; however , her relatively flat , high-risk groundstrokes tended to produce frequent errors , and her serve was sometimes unreliable in singles .
Kournikova plays right-handed with a two-handed backhand .
She has been compared to such doubles specialists as Pam Shriver and Peter Fleming .
Kournikova started dating pop star Enrique Iglesias in late 2001 , and rumors that the couple had secretly married circulated in 2003 and again in 2005 .
Kournikova herself has consistently refused to directly confirm or deny the status of her personal relationships .
But in May 2007 , Enrique Iglesias was ( mistakenly , as he would clarify later ) quoted in the New York Sun that he had no intention of marrying Kournikova and settling down because they had split up .
In June 2008 , Iglesias told the Daily Star that he had married Kournikova the previous year and that they are currently separated .
Enrique has stated in interviews after that it was simply a joke , and they are still very much together .
In an interview with Graham Norton in 2010 , Kournikova confirmed that she and Iglesias have been together for over eight years but have no plans to marry in the near future .
She became an American citizen in late 2009 .
Most of Kournikova 's fame has come from the publicity surrounding her personal life , as well as numerous modeling shoots .
During Kournikova 's debut at the 1996 U.S. Open at the age of 15 , the world noticed her beauty , and soon pictures of her appeared in numerous magazines worldwide .
In 2000 , Kournikova became the new face for Berlei 's shock absorber sports bras , and appeared in the highly successful " only the ball should bounce " billboard campaign .
Kournikova was named one of People 's 50 Most Beautiful People in 1998 , 2000 , 2002 , and 2003 and was voted " hottest female athlete " and " hottest couple " ( with Iglesias ) on ESPN.com .
