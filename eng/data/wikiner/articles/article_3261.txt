David Michael Letterman ( born April 12 , 1947 ) is an American television host and comedian .
He hosts the late night television talk show , Late Show with David Letterman broadcast on CBS .
Letterman has been a fixture on late night television since the 1982 debut of Late Night with David Letterman on NBC .
Only Letterman 's friend and mentor Johnny Carson has had a longer late-night hosting career .
Letterman is also a television and film producer .
His company Worldwide Pants produces his show as well as its network followup The Late Late Show with Craig Ferguson .
Letterman was born in Indianapolis , Ind. .
In 2000 , he told an interviewer for Esquire that , while growing up , he admired his father 's ability to tell jokes and be the life of the party .
The fear of losing his father was constantly with Letterman as he grew up .
The elder Letterman died of a second heart attack at age 57 .
Letterman began his broadcasting career as an announcer and newscaster at the college 's student-run radio station -- WBST -- a 10-watt campus station which now is part of Indiana public radio .
In 1971 , Letterman appeared as a pit road reporter for ABC Sports ' tape-delayed coverage of the Indianapolis 500 .
He started off by writing material for comedian Jimmie Walker .
He also began performing stand-up comedy at The Comedy Store , a famed comedy club and proving ground for young comics .
Letterman had a stint as a cast member on Mary Tyler Moore 's variety show , Mary ; a guest appearance on Mork & Mindy ( as a parody of EST leader Werner Erhard ) ; and appearances on game shows such as The $ 20,000 Pyramid , The Gong Show , Password Plus and Liar 's Club .
His dry , sarcastic humor caught the attention of scouts for The Tonight Show Starring Johnny Carson , and Letterman was soon a regular guest on the show .
Letterman became a favorite of Carson 's and was a regular guest host for the show beginning in 1978 .
Letterman personally credits Carson as the person who influenced his career the most .
On June 23 , 1980 , Letterman was given his own morning comedy show on NBC , The David Letterman Show .
The show was a critical success , winning two Emmy Awards , but was a ratings disappointment and was canceled in October 1980 .
NBC kept Letterman under contract to try him in a different time slot .
Late Night with David Letterman debuted February 1 , 1982 ; the first guest on the first show was Bill Murray .
Letterman 's reputation as an acerbic interviewer was borne out in verbal sparring matches with Cher , Shirley MacLaine , Charles Grodin , and Madonna .
The show also featured inventive comedy segments and running characters , in a style heavily influenced by the 1950s and ' 60s programs of Steve Allen .
Although Ernie Kovacs is often cited as an influence on the show , Letterman has denied this .
In one infamous appearance , in 1982 , Andy Kaufman ( who was already wearing a neck brace ) appeared to be slapped and knocked to the ground by professional wrestler Jerry Lawler ( though Lawler and Kaufman 's friend Bob Zmuda later revealed that the event was staged .
The following night , guest Ted Koppel asked Letterman " May I insert something here ? "
In 1993 , Letterman departed NBC to host his own late-night show on CBS , opposite The Tonight Show at 11:30 p.m. , called the Late Show with David Letterman .
The new show debuted on August 30 , 1993 and was taped at the historic Ed Sullivan Theater , on which CBS had spent $ 14 million in renovations for Letterman 's arrival .
In addition to that cost , CBS also signed Letterman to a lucrative three-year , $ 14 million/year contract , doubling his Late Night salary .
But while the expectation was that Letterman would retain his unique style and sense of humor with the move , Late Show was not an exact replica of his old NBC program .
Recognizing the more formal mood ( and wider audience ) of his new time slot and studio , Letterman eschewed his trademark blazer / khaki pants/white sneakers wardrobe combination in favor of expensive shoes and tailored suits .
The monologue was lengthened and Paul Shaffer and the " World 's Most Dangerous Band " followed Letterman to CBS , but they added a brass section and were rebranded the " CBS Orchestra " as a short monologue and a small band were mandated by Carson while Letterman occupied the 12:30 slot .
Additionally , because of intellectual property disagreements , Letterman was unable to import many of his Late Night segments verbatim , but he sidestepped this problem by simply renaming them
In 1993 and 1994 , The Late Show consistently gained higher ratings than Tonight .
But in 1995 , ratings dipped and Leno 's show consistently beat Letterman 's in the ratings ; Leno typically attracted about 5 million nightly viewers between 1999 and 2009 .
The Late Show lost nearly half its audience during its competition with Leno , attracting 7.1 million viewers nightly in its 1993 -- 94 season and about 3.8 million per night as of Leno 's departure in 2009 .
In his final months as host of The Tonight Show , Leno beat Letterman in the ratings by a 1.3 million viewer margin ( 5.2 million to 3.9 million ) , and Nightline and The Late Show were virtually tied .
Letterman 's shows have garnered both critical and industry praise , receiving 67 Emmy Award nominations , winning twelve times in his first 20 years in late night television .
For example , in 2003 and 2004 Letterman ranked second in that poll , behind only Oprah Winfrey , a year that Leno was ranked fifth .
Leno was higher than Letterman on that poll three times during the same period , in 1998 , 2007 , and 2008 .
On March 27 , 1995 , Letterman acted as the host for the 67th Academy Awards ceremony .
Critics blasted Letterman for what they deemed a poor hosting of the Oscars , noting that his irreverent style undermined the importance and glamor of the event .
Oprah ... Uma !
Oprah , Uma ... Keanu ! "
Although Letterman attracted the highest ratings to the annual telecast since 1983 , many felt that the bad publicity garnered by Letterman 's hosting caused a decline in the Late Show 's ratings .
Letterman recycled the apparent debacle into a long-running gag .
On his first show after the Oscars , he joked , " Looking back , I had no idea that thing was being televised . "
He lampooned his stint in the following year , during Billy Crystal 's opening Oscar skit , which also parodied the plane-crashing scenes from that year 's chief nominated film , The English Patient .
For years afterward , Letterman recounted his horrible hosting at the Oscars , although the Academy of Motion Picture Arts and Sciences still holds Letterman in high regard and it has been rumored they have asked him to host the Oscars again .
On January 14 , 2000 , a routine check-up revealed that an artery in Letterman 's heart was severely constricted .
During the initial weeks of his recovery , reruns of the Late Show were shown and introduced by friends of Letterman including Drew Barrymore , Ray Romano , Robin Williams , Bonnie Hunt , Megan Mullally , Bill Murray , Regis Philbin , Charles Grodin , Nathan Lane , Julia Roberts , Bruce Willis , Jerry Seinfeld , Martin Short , Steven Seagal , Hillary Rodham Clinton , Danny DeVito , Steve Martin , and Sarah Jessica Parker .
Subsequently , while still recovering from surgery , Letterman revived the late night tradition that had virtually disappeared on network television during the 1990s of ' guest hosts ' by allowing Bill Cosby , Kathie Lee Gifford ( recommended by Regis Philbin , who was asked first but had no time in his schedule ) , Dana Carvey , Janeane Garofalo , and others to host new episodes of The Late Show .
Cosby -- the show 's first guest host -- refused to sit at Letterman 's desk out of respect , using the couch instead ; Garofalo followed suit , utilizing a set of grade-school desks instead .
Upon his return to the show on February 21 , 2000 , Letterman brought all of the doctors and nurses on stage who had participated in his surgery and recovery ( with extra teasing of a nurse who had given him bed baths -- " This woman has seen me naked !
" ) , including Dr. O .
In a show of emotion , Letterman was nearly in tears as he thanked the health care team with the words " These are the people who saved my life ! "
The episode earned an Emmy nomination .
Letterman became friends with his doctors and nurses .
In 2008 , a Rolling Stone interview stated " he hosted a doctor and nurse who 'd helped perform the emergency quintuple-bypass heart surgery that saved his life in 2000 .
Additionally , Letterman invited the band Foo Fighters to play " Everlong " , introducing them as " my favorite band , playing my favorite song . "
Letterman again handed over the reins of the show to several guest hosts ( including Bill Cosby , Brad Garrett , Elvis Costello , John McEnroe , Vince Vaughn , Will Ferrell , Bonnie Hunt , Luke Wilson and bandleader Paul Shaffer ) in February 2003 , when he was diagnosed with a severe case of shingles .
Later that year , Letterman made regular use of guest hosts -- including Tom Arnold and Kelsey Grammer -- for new shows broadcast on Fridays .
In March 2007 , Adam Sandler -- who had been scheduled to be the lead guest -- served as a guest host while Letterman was ill with a stomach virus .
In March 2002 , as Letterman 's contract with CBS neared expiration , ABC offered him the time slot for long-running news program Nightline with Ted Koppel .
Letterman was interested as he believed he could never match Leno 's ratings at CBS due to weaker lead-ins from the network 's late local news programs , but was reluctant to replace Koppel .
Letterman addressed his decision to re-sign on the air , stating that he was content at CBS and that he had great respect for Koppel .
On December 4 , 2006 , CBS revealed that David Letterman signed a new contract to host The Late Show with David Letterman through the fall of 2010 .
" [ citation needed ] Letterman further joked about the subject by pulling up his right pants leg , revealing a tattoo , presumably temporary , of the ABC logo .
" His presence on our air is an ongoing source of pride , and the creativity and imagination that the Late Show puts forth every night is an ongoing display of the highest quality entertainment .
We are truly honored that one of the most revered and talented entertainers of our time will continue to call CBS ' home .
According to a 2007 article in Forbes magazine , Letterman earned $ 40 million a year .
A 2009 article in The New York Times , however , said his salary was estimated at $ 32 million per year .
In June 2009 , Letterman and CBS reached agreement to extend his contract to host The Late Show until August 2012 .
Letterman was represented in negotiations by the Creative Artists Agency , who have represented him since his 1993 departure from NBC .
The Late Show went off air for eight weeks during the months of November and December because of the Writers Guild of America strike .
David Letterman 's production company -- Worldwide Pants -- was the first company to make an individual agreement with the WGA , thus allowing his show to come back on air on January 2 , 2008 .
NBC 's decision to select Jay Leno and not Letterman to succeed Johnny Carson as host of The Tonight Show was an embarrassment to Carson , who had all but promised the succession to Letterman .
Letterman maintained a close relationship with Carson through his break with NBC .
Letterman would mock the film for months afterward , specifically on how the actor playing him , John Michael Higgins , did not resemble him in the least .
Carson later made a few cameo appearances as a guest on Letterman 's show .
Carson 's final television appearance came May 13 , 1994 on a Late Show episode taped in Los Angeles , when he made a surprise appearance during a ' Top 10 list ' segment .
The audience went wild as Letterman stood up and proudly invited Carson to sit at his desk .
The applause was so protracted that Carson was unable to say anything , and he finally returned backstage as the applause continued ( it was later explained that Carson had laryngitis , though Carson can be heard talking to Letterman during his appearance ) .
In early 2005 , it was revealed that Carson still kept up with current events and late-night TV right up to his death that year , and that he occasionally sent jokes to Letterman , who used these jokes in his monologue ; according to CBS senior vice president Peter Lassally ( a onetime producer for both men ) , Carson got " a big kick out of it . "
Letterman would do a characteristic Johnny Carson golf swing after delivering one of Carson 's jokes .
In a tribute to Carson , all of the opening monologue jokes during the first show following Carson 's death were written by Carson .
On September 10 , 2007 , Letterman made his first appearance on The Oprah Winfrey Show at Madison Square Garden in New York City .
Oprah had previously appeared on Letterman 's show when he was hosting NBC 's Late Night on May 2 , 1989 .
Winfrey and Letterman also appeared together in a Late Show promo that aired during CBS 's coverage of Super Bowl XLI in February 2007 , with the two sitting next to each other on the couch watching the game .
Three years later , during CBS 's coverage of Super Bowl XLIV , the two appeared again , this time with Winfrey sitting on a couch between Letterman and Jay Leno .
The appearance was Letterman 's idea : Leno flew to New York City in an NBC corporate jet , sneaking into the Ed Sullivan Theater during the Late Show 's February 4th taping wearing a disguise , meeting Winfrey and Letterman at a living room set created in the theater 's balcony where they taped their promo .
Letterman started his own production company -- Worldwide Pants Incorporated -- which produced his show and several others , including Everybody Loves Raymond , The Late Late Show , and several critically-acclaimed , but short-lived television series for Bonnie Hunt .
Worldwide Pants also produced the dramedy program Ed , which aired on NBC from 2000 -- 2004 .
It was Letterman 's first association with NBC since he left the network in 1993 .
In 2007 , Worldwide Pants produced the ABC comedy series , Knights of Prosperity .
Rahal Letterman Racing ( RLR ) is an auto racing team that currently races in the American Le Mans Series , and part-time in the Indy Racing League .
The team won the 2004 Indianapolis 500 with driver Buddy Rice .
Letterman was a pit reporter for ABC in the 1971 Indianapolis 500 .
American Foundation for Courtesy and Grooming is Letterman 's private foundation .
On September 7 , 2007 , Letterman visited his alma mater , Ball State University in Muncie , Indiana , for the dedication of a communications facility named in his honor .
Thousands of Ball State students , faculty , and local residents welcomed Letterman back to Indiana .
Letterman 's emotional speech touched on his struggles as a college student and his late father , and also included the " top ten good things about having your name on a building . "
Letterman received the honor for his dedication to the university throughout his career as a comedian .
Letterman finished with , " If reasonable people can put my name on a $ 21 million building , anything is possible . "
Letterman appeared in issue 239 of the Marvel comic book The Avengers , in which the title characters are guests on Late Night .
He also had a cameo in the feature film Cabin Boy , with Chris Elliott , who worked as a writer on Letterman 's show .
He also appeared as himself in the Howard Stern biopic Private Parts as well as the 1999 Andy Kaufman biopic Man on the Moon , in a few episodes of Garry Shandling 's 1990s TV series The Larry Sanders Show and in " The Abstinence " , a 1996 episode of the sitcom Seinfeld .
He also had a long-term relationship with former head writer and producer on Late Night , Merrill Markoe .
Letterman announced the marriage during the taping of his March 23 show , shortly after congratulating Bruce Willis for getting married the previous week .
Letterman told the audience he nearly missed the ceremony because his truck became stuck in mud two miles from their house .
The family resides in North Salem , New York , on a 108-acre ( 44 ha ) estate .
Beginning in May 1988 , Letterman was stalked by Margaret Mary Ray , a woman suffering from schizophrenia .
Letterman occasionally referenced her in his show , although not by name .
Letterman publicly expressed sympathy upon her death .
On his October 1 , 2009 show , Letterman announced that he had been the victim of an extortion attempt by someone threatening to reveal that he had had sex with several of his female employees .
Letterman stated that three weeks earlier ( on September 9 , 2009 ) someone had left a package in his car with material he said he would write into a screenplay and a book if Letterman did not pay him $ 2 million .
Letterman said that he contacted the Manhattan District Attorney 's office , ultimately cooperating with them to conduct a sting operation involving giving the man a phony check .
The extortionist , Robert J. " Joe " Halderman , a producer of the CBS true crime journalism series 48 Hours , was subsequently arrested after trying to deposit the check .
Stephanie Birkitt , one of Letterman 's assistants who has been linked romantically to Letterman , had previously been a member of the CBS page program , and had worked as an intern for both Letterman 's show and for 48 Hours before joining Letterman 's staff .
In the days following the initial announcement of the affairs and the arrest , several prominent women , including Kathie Lee Gifford , co-host of NBC 's Today Show , and NBC news anchor Ann Curry questioned whether Letterman 's affairs with subordinates created an unfair working environment .
A spokesman for Worldwide Pants said that the company 's sexual harassment policy did not prohibit sexual relationships between managers and employees .
On October 5 , 2009 , Letterman devoted a segment of his show to a public apology to his wife and staff .
His nomination record is second only to producer Jac Venza , who holds the record for the most Emmy nominations for an individual ( 57 ) .
