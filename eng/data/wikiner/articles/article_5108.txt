Examples include both Dylan and Young and others such as Chris Martin , Nick Zinner or Pete Townshend .
Based on different criteria , these lists , particularly Rolling Stone polls , often meet with criticism and ridicule by many guitarists but they are also praised by others .
A 2009 book , The 100 Greatest Metal Guitarists , attempted to define the best players of the heavy metal field and caused much controversy in doing so .
The first in this list is the American guitarist Jimi Hendrix introduced by Pete Townshend , guitarist for The Who , who was , in his turn , ranked at # 50 of the list .
The online magazine Blogcritics criticized the list for introducing some undeserving guitarists while forgetting some artists perceived being perhaps more worthy , such as Phil Keaggy or John Petrucci .
Despite the appearance in other magazines like Billboard , this publication by Guitar World was criticized for including no female musicians within its selection .
