Although he was not as clever as the likes of Odysseus or Nestor , Heracles used his wits on several occasions when his strength did not suffice , such as when laboring for the king Augeas of Elis , wrestling the giant Antaeus , or tricking Atlas into taking the sky back onto his shoulders .
Heracles was an extremely passionate and emotional individual , capable of doing both great deeds for his friends ( such as wrestling with Thanatos on behalf of Prince Admetus , who had regaled Heracles with his hospitality , or restoring his friend Tyndareus to the throne of Sparta after he was overthrown ) and being a terrible enemy who would wreak horrible vengeance on those who crossed him , as Augeas , Neleus and Laomedon all found out to their cost .
It is also said that when Heracles died he shed his mortal skin , which went down to the underworld and he went up to join the gods for being the greatest hero ever known .
Thus Eusebius , Preparation of the Gospel ( 10.12 ) , reported that Clement could offer historical dates for Hercules as a king in Argos : " from the reign of Hercules in Argos to the deification of Hercules himself and of Asclepius there are comprised thirty-eight years , according to Apollodorus the chronicler : and from that point to the deification of Castor and Pollux fifty-three years : and somewhere about this time was the capture of Troy . "
A major factor in the well-known tragedies surrounding Heracles is the hatred that the goddess Hera , wife of Zeus , had for him .
A full account of Heracles must render it clear why Heracles was so tormented by Hera , when there are many illegitimate offspring sired by Zeus .
Heracles was the son of the affair Zeus had with the mortal woman Alcmene .
Zeus made love to her after disguising himself as her husband , Amphitryon , home early from war ( Amphitryon did return later the same night , and Alcmene became pregnant with his son at the same time , a case of heteropaternal superfecundation , where a woman carries twins sired by different fathers ) .
His twin mortal brother , son of Amphitryon was Iphicles , father of Heracles ' charioteer Iolaus .
Hera did this knowing that while Heracles was to be born a descendant of Perseus , so too was Eurystheus .
Once the oath was sworn , Hera hurried to Alcmene 's dwelling and slowed the birth of Heracles by forcing Ilithyia , goddess of childbirth , to sit crosslegged with her clothing tied in knots , thereby causing Heracles to be trapped in the womb .
Meanwhile , Hera caused Eurystheus to be born prematurely , making him High King in place of Heracles .
She would have permanently delayed Heracles ' birth had she not been fooled by Galanthis , Alcmene 's servant , who lied to Ilithyia , saying that Alcmene had already delivered the baby .
Upon hearing this , she jumped in surprise , untying the knots and inadvertently allowing Alcmene to give birth to her twins , Heracles and Iphicles .
The child was originally given the name Alcides by his parents ; it was only later that he became known as Heracles .
He was renamed Heracles in an unsuccessful attempt to mollify Hera .
A few months after he was born , Hera sent two serpents to kill him as he lay in his cot .
Heracles throttled a snake in each hand and was found by his nurse playing with their limp bodies as if they were child 's toys .
After killing his music tutor Linus with a lyre , he was sent to tend cattle on a mountain by his foster father Amphitryon .
In a fit of madness , induced by Hera , Heracles killed his children by Megara .
He was directed to serve King Eurystheus for ten years and perform any task , which he required .
Driven mad by Hera , Heracles slew his own children .
To expiate the crime , Heracles was required to carry out ten labors set by his archenemy , Eurystheus , who had become king in Heracles ' place .
Heracles accomplished these tasks , but Eurystheus did not accept the cleansing of the Augean stables because Heracles was going to accept pay for the labor .
Apollodorus ( 2.5.1-2.5.12 ) gives the following order :
After completing these tasks , Heracles joined the Argonauts in a search for the Golden Fleece .
They rescued heroines , conquered Troy , and helped the gods fight against the Gigantes .
Heracles won but Eurytus abandoned his promise .
Heracles ' advances were spurned by the king and his sons , except for one : Iole 's brother Iphitus .
Heracles killed the king and his sons -- excluding Iphitus -- and abducted Iole .
Iphitus became Heracles ' best friend .
However , once again , Hera drove Heracles mad and he threw Iphitus over the city wall to his death .
Once again , Heracles purified himself through three years of servitude -- this time to Queen Omphale of Lydia .
Omphale was a queen or princess of Lydia .
As penalty for a murder , Heracles was her slave .
He was forced to do women 's work and wear women 's clothes , while she wore the skin of the Nemean Lion and carried his olive-wood club .
After some time , Omphale freed Heracles and married him .
It was at that time that the cercopes , mischievous wood spirits , stole Heracles ' weapons .
As Argonauts , they only participated in part of the journey .
In Mysia , Hylas was kidnapped by a nymph .
Heracles , heartbroken , searched for a long time but Hylas had fallen in love with the nymphs and never showed up again .
Either way , the Argo set sail without them .
Heracles freed the Titan from his chains and his torments .
Prometheus then made predictions regarding further deeds of Heracles .
Before the Trojan War , Poseidon sent a sea monster to attack Troy .
Laomedon planned on sacrificing his daughter Hesione to Poseidon in the hope of appeasing him .
Laomedon agreed .
Heracles killed the monster , but Laomedon went back on his word .
Accordingly , in a later expedition , Heracles and his followers attacked Troy and sacked it .
Then they slew all Laomedon 's sons present there save Podarces , who was renamed Priam , who saved his own life by giving Heracles a golden veil Hesione had made .
Telamon took Hesione as a war prize ; they were married and had a son , Teucer .
During the course of his life , Heracles married four times .
His first marriage was to Megara , whose children he murdered in a fit of madness .
Apollodoros ( Bibliotheke ) recounts that Megara was unharmed and given in marriage to Iolaus , while in Euripides ' version Heracles killed Megara , too .
His second wife was Omphale , the Lydian queen or princess to whom he was delivered as a slave .
His third marriage was to Deianira , for whom he had to fight the river god Achelous .
( Upon Achelous ' death , Heracles removed one of his horns and gave it to some nymphs who turned it into the cornucopia . )
Soon after they wed , Heracles and Deianira had to cross a river , and a centaur named Nessus offered to help Deianira across but then attempted to rape her .
Enraged , Heracles shot the centaur from the opposite shore with a poisoned arrow ( tipped with the Lernaean Hydra 's blood ) and killed him .
As he lay dying , Nessus plotted revenge , told Deianira to gather up his blood and spilled semen and , if she ever wanted to prevent Heracles from having affairs with other women , she should apply them to his vestments .
Nessus knew that his blood had become tainted by the poisonous blood of the Hydra , and would burn through the skin of anyone it touched .
Later , when Deianira suspected that Heracles was fond of Iole , she soaked a shirt of his in the mixture , creating the poisoned shirt of Nessus .
Heracles ' servant , Lichas , brought him the shirt and he put it on .
Heracles chose a voluntary death , asking that a pyre be built for him to end his suffering .
Because his mortal parts had been incinerated , he could now become a full god and join his father and the other Olympians on Mount Olympus .
He then married Hebe .
Heracles complied and they all became pregnant and all bore sons .
As symbol of masculinity and warriorship , Heracles also had a number of male lovers .
Of these , the one most closely linked to Heracles is the Theban Iolaus .
According to a myth thought to be of ancient origins , Iolaus was Heracles ' charioteer and squire .
Heracles in the end helped Iolaus find a wife .
Plutarch reports that down to his own time , male couples would go to Iolaus 's tomb in Thebes to swear an oath of loyalty to the hero and to each other
One of Heracles ' male lovers , and one represented in ancient as well as modern art , is Hylas .
Though it is of more recent vintage ( dated to the third century ) than that with Iolaus , it had themes of mentoring in the ways of a warrior and help finding a wife in the end .
Abdera 's eponymous hero , Abderus , was another of Heracles ' lovers .
Heracles founded the city of Abdera in Thrace in his memory , where he was honored with athletic games .
Another myth is that of Iphitus .
His role as lover was perhaps to explain why he was the only son of Neleus to be spared by the hero .
Telephus is the son of Heracles and Auge .
The sons of Heracles and Hebe are Alexiares and Anicetus .
Having wrestled and defeated Achelous , god of the Acheloos river , Heracles takes Deianeira as his wife .
Travelling to Tiryns , a centaur , Nessus , offers to help Deianeira across a fast flowing river while Heracles swims it .
However , Nessus is true to the archetype of the mischievous centaur and tries to steal Deianara away while Heracles is still in the water .
Several years later , rumor tells Deianeira that she has a rival for the love of Heracles .
Deianeira , remembering Nessus ' words , gives Heracles the bloodstained shirt .
Lichas , the herald , delivers the shirt to Heracles .
However , it is still covered in the Hydra 's blood from Heracles ' arrows , and this poisons him , tearing his skin and exposing his bones .
Before he dies , Heracles throws Lichas into the sea , thinking he was the one who poisoned him ( according to several versions , Lichas turns to stone , becoming a rock standing in the sea , named for him ) .
No one but Heracles ' friend Philoctetes would light his funeral pyre ( in an alternate version , it is Iolaus who lights the pyre ) .
Philoctetes confronted Paris and shot a poisoned arrow at him .
The Hydra poison would subsequently lead to the death of Paris .
Herodotus connected Heracles both to Phoenician god Melqart and to the Egyptian god Shu .
Heracles was canonized by Aleister Crowley as a saint in Ecclesia Gnostica Catholica .
He claims to be the god of strength himself , descended from Olympus .
In television , Hercules is the mentor and ancestor of Herry Hercules from Class of the Titans .
