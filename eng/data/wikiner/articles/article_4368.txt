The subversive and imaginative element in his art , as well as his bold handling of paint , provided a model for the work of later generations of artists , notably Manet and Picasso .
He spent his childhood in Fuendetodos , where his family lived in a house bearing the family crest of his mother .
About 1749 , the family bought a house in the city of Zaragoza and some years later moved into it .
Their correspondence from the 1770s to the 1790s is a valuable source for understanding Goya 's early career at the court of Madrid .
Goya submitted entries for the Royal Academy of Fine Art in 1763 and 1766 , but was denied entrance .
He studied with Francisco Bayeu y Subías and his painting began to show signs of the delicate tonalities for which he became famous .
He also painted a canvas for the altar of the Church of San Francisco El Grande in Madrid , which led to his appointment as a member of the Royal Academy of Fine Art .
In 1786 , Goya was given a salaried position as painter to Charles III .
After the death of Charles III in 1788 and revolution in France in 1789 , during the reign of Charles IV , Goya reached his peak of popularity with royalty .
His portraits are notable for their disinclination to flatter , and in the case of Charles IV of Spain and His Family , the lack of visual diplomacy is remarkable .
At some time between late 1792 and early 1793 , a serious illness , whose exact nature is not known , left Goya deaf , and he became withdrawn and introspective .
His experimental art -- that would encompass paintings , drawings as well as a bitter series of aquatinted etchings , published in 1799 under the title Caprichos -- was done in parallel to his more official commissions of portraits and religious paintings .
In 1798 , he painted luminous and airy scenes for the pendentives and cupula of the Real Ermita ( Chapel ) of San Antonio de la Florida in Madrid .
King Ferdinand VII returned to Spain in 1814 but relations with Goya were not cordial .
In 1819 , with the idea of isolating himself , he bought a country house by the Manzanares river just outside of Madrid .
There he created the Black Paintings with intense , haunting themes , reflective of the artist 's fear of insanity , and his outlook on humanity .
Several of these , including Saturn Devouring His Son , were painted directly onto the walls of his dining and sitting rooms .
He returned to Spain in 1826 , but returned to Bordeaux , where he died in 1828 at the age of 82 .
The style of these Black Paintings prefigure the expressionist movement .
The paintings were never publicly exhibited during Goya 's lifetime .
They were owned by Manuel de Godoy , the Prime Minister of Spain and a favorite of the Queen , María Luisa .
Courtyard with Lunatics is a horrifying and imaginary vision of loneliness , fear and social alienation , a departure from the rather more superficial treatment of mental illness in the works of earlier artists such as Hogarth .
And this intention is to be taken into consideration since one of the essential goals of the enlightenment was to reform the prisons and asylums , a subject common in the writings of Voltaire and others .
The condemnation of brutality towards prisoners ( whether they were criminals or insane ) was the subject of many of Goya 's later paintings .
As he completed this painting , Goya was himself undergoing a physical and mental breakdown .
If this is the case , from here on -- we see an insidious assault of his faculties , manifesting as paranoid features in his paintings , culminating in his black paintings and especially Saturn Devouring His Sons .
In 1799 Goya published a series of 80 prints titled Caprichos depicting what he called
Additionally , one can discern a thread of the macabre running through Goya 's work , even in his earlier tapestry cartoons .
This painting is one of 14 in a series known as the Black Paintings .
After his death the wall paintings were transferred to canvas and remain some of the best examples of the later period of Goya 's life when , deafened and driven half-mad by what was probably an encephalitis of some kind , he decided to free himself from painterly strictures of the time and paint whatever nightmarish visions came to him .
Many of these works are in the Prado museum in Madrid .
In the 1810s , Goya created a set of aquatint prints titled The Disasters of War which depict scenes from the Peninsular War .
The prints were not published until 1863 , 35 years after Goya 's death .
The findings of research published since 2003 have raised questions regarding the authenticity of some of Goya 's late works .
One study claims that the Black Paintings were applied to walls that did not exist in Goya 's home before he left for France .
In 2008 the Prado Museum reverted the traditional attribution of The Colossus , and expressed doubts over the authenticity of three other paintings attributed to Goya as well .
On 27 January 2009 , the Prado announced they had come to the conclusion that The Colossus was painted by one of Goya 's apprentices and even bore the signature of the painter .
Enrique Granados composed a piano suite ( 1911 ) and later an opera ( 1916 ) , both called Goyescas , inspired by the artist 's paintings .
Mario CastelNuovo Tedesco wrote 24 classical guitar pieces , opus 195 ( 1968 ) inspired from the Caprichos .
Gian Carlo Menotti wrote a biographical opera about him titled Goya ( 1986 ) , commissioned by Plácido Domingo , who created the role ; this production has been presented on television .
Goya also inspired Michael Nyman 's opera Facing Goya ( 2000 ) .
Several films portray Goya 's life .
