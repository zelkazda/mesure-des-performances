He was born at Mainz , but owing to the political disarray of the time , his parents moved to Aschaffenburg in Bavaria .
In the library , Bopp had access not only to the rich collection of Sanskrit manuscripts ( mostly brought from India by Jean François Pons in the early 18th century ) but also to the Sanskrit books which had up to that time been issued from the Calcutta and Serampore presses .
In this first book Bopp entered at once the path on which he would focus the philological researches of his whole subsequent life .
On the publication , in Calcutta , of the whole Mahabharata , Bopp discontinued editing Sanskrit texts and confined himself thenceforth exclusively to grammatical investigations .
Bopp started work on a new edition in Latin , for the following year , completed in 1832 ; a shorter grammar appeared in 1834 .
At the same time he compiled a Sanskrit and Latin glossary ( 1830 ) in which , more especially in the second and third editions ( 1847 and 1868-71 ) , he also took account of the cognate languages .
As Bopp based his research on the best available sources and incorporated every new item of information that came to light , his work continued to widen and deepen in the making .
