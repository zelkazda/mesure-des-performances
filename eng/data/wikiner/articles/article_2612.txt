The Cheka was the first of a succession of Soviet state security organizations .
It was created by a decree issued on December 20 , 1917 , by Vladimir Lenin and subsequently led by aristocrat turned communist Felix Dzerzhinsky .
From its founding , the Cheka was an important military and security arm of the Bolshevik communist government .
A member of Cheka was called a chekist .
In The Gulag Archipelago , Alexander Solzhenitsyn recalls that zeks in the labor camps used " old Chekist " as " a mark of special esteem " for particularly experienced camp administrators .
The term is still found in use in Russia today .
In the first month and half after the October Revolution the duties of extinguishing the resistance of exploiters were assigned to the Petrograd Military Revolutionary Committee .
It represented a temporary , extraordinary body working under directives of Sovnarkom and Central Committee of RDSRP .
On the position of a chairman of the commission was offered a candidacy of Felix Dzerzhinsky ( the Iron Felix ) who was directed by Sovnarkom to its next meeting present a list of the commission members and compose measures in fight against sabotage .
Upon the end of the meeting Dzerzhinsky reported to the Sovnarkom with the requested information .
The commission was created not under the VTsIK as it was previously anticipated , but rather under the Council of the People 's Commissars .
Simultaneously an issue of speculation was raised at the same meeting which was handed over to Peters to solve and report with results to one of the next meetings of the commission .
By march 1918 at the time of arrival to Moscow it contained following sections : against counterrevolution , speculation , nonresidents , and informational .
On January 14 , 1918 Sovnarkom ordered Dzerzhinsky to organize teams of energetic and ideological ones out of sailors in the fight against speculation .
Sovnarkom has recognized the desirability of including in the composition of collegiate of the Cheka five representatives of Left Socialist-Revolutionary faction of VTsIK .
One of the first was founded the Moscow Cheka .
On March 7 , 1918 because of transferring out of Petrograd to Moscow it was decided to create the Petrograd Cheka .
On March 9 was created a section for combating counterrevolution at the Omsk Soviet .
There were also created the extraordinary commissions in Penza , Perm , Novgorod , Cherepovets , Rostov , Taganrog .
In August 1918 in the Soviet Republic had accounted for some 75 uyezd extraordinary commissions .
By the end of the year there were established 365 uyezd Cheka .
It included oblast , guberniya , raion , uyezd , and volost Cheka , with raion and volost extraordinary commissioners .
The head of it was appointed Kedrov .
It was replaced by the State Political Administration or GPU , a section of the NKVD of the Russian Soviet Federative Socialist Republic ( RSFSR ) .
Initially formed to fight against counter-revolutionaries and saboteurs as well as financial speculators , Cheka classified them in its own manner .
At the direction of Lenin , the Cheka performed mass arrests , imprisonments , and executions of " enemies of the people " .
However , within a month the Cheka had extended its repression to all political opponents of the communist government , including anarchists and others on the left .
On April 11 -- 12 , 1918 , an attack on 26 anarchist political centres in Moscow occurred .
40 anarchists were killed by Cheka forces , about 500 arrested and jailed after a pitched battle took place between them .
In the autumn of 1918 the Cheka has openly and proudly announced that it is the terrorist organization in the name of a working class .
In May 1919 , two Cheka agents sent to assassinate Makhno were caught and executed .
Many victims of Cheka repression were ' bourgeois hostages ' rounded up and held in readiness for summary execution in reprisal for any alleged counter-revolutionary act .
Lenin 's dictum that it is better to arrest 100 innocent people than to risk one enemy going free ensured that wholesale , indiscriminate arrests became an integral part of the system .
It is believed that more than three million deserters escaped from the Red Army in 1919 and 1920 .
Throughout the course of the civil war , several thousand deserters were shot -- a number comparable to that of belligerents during World War I .
In September 1918 , according to The Black Book of Communism in only twelve provinces of Russia , 48,735 deserters and 7,325 " bandits " were arrested , 1,826 were killed and 2,230 were executed .
The Cheka later played a major role in suppressing the Kronstadt Rebellion by Soviet sailors in 1921 .
Estimates on Cheka executions vary widely .
The lowest figures are provided by Dzerzhinsky 's lieutenant Martyn Latsis , limited to RSFSR over the period 1918 -- 1920 :
For example , he refutes the claim made by Latsis that only 22 executions were carried out in the first six months of the Cheka 's existence by providing evidence that the true number was 884 executions .
Donald Rayfield concurs , noting that " plausible evidence reveals that the actual numbers .
Some believe it is possible more people were murdered by the Cheka than died in battle .
Lenin himself seemed unfazed by the killings .
The Cheka is reported to have practiced torture .
Anton Denikin 's investigation discovered corpses whose lungs , throats , and mouths had been packed with earth .
Women and children were also victims of Cheka terror .
Cheka was actively and openly utilizing kidnapping methods .
With kidnapping methods Cheka was able to extinguish numerous cases of discontent especially among the rural population .
Among the notorious ones was the Tambov rebellion .
Cheka departments were organized not only in big cities and guberniya seats , but also in each uyezd , at any front-lines and military formations .
A lot who was hired to head those departments were so called nestlings of Kerensky , the former convicts ( political and criminal ) that released by the Kerensky amnesty .
