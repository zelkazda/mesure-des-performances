Bodmin is a civil parish and major town in Cornwall , United Kingdom .
It is situated in the centre of the county southwest of Bodmin Moor .
It is bordered to the east by Cardinham parish , to the southeast by Lanhydrock parish , to the southwest and west by Lanivet parish , and to the north by Helland parish .
Bodmin has a population of 12,778 ( 2001 census ) .
Bodmin lies in the centre of Cornwall , south-west of Bodmin Moor .
It has been suggested that the town 's name comes from an archaic word in the Cornish " bod " ( meaning a dwelling ; the later word is " bos " ) and a contraction of " menegh " ( monks ) .
For most of Bodmin 's history , the tin industry was a mainstay of the economy .
This became known as the Prayer Book Rebellion .
The font of a type common in Cornwall is of the 12th century : large and finely carved .
The Roman Catholic parish of Bodmin includes a large area of North Cornwall and there are churches also at Wadebridge , Padstow and Tintagel .
It was also used for temporarily holding prisoners sentenced to transportation , awaiting transfer to the prison hulks lying in the highest navigable reaches of the River Fowey .
William Robert Hicks the humorist was domestic superintendent in the mid-19th century .
Bodmin College is a large state comprehensive school for ages 11 -- 18 on the outskirts of the town and on the edge of Bodmin Moor .
Bodmin Parkway railway station is served by main line trains and is situated on the Cornish Main Line about 3½ miles ( 5½ km ) south-east from the town centre .
The bus link to Bodmin , Wadebridge and Padstow starts from outside the main entrance of Bodmin Parkway .
Bodmin Riding is a traditional annual ceremony .
He was--- according to the Dictionary of National Biography -- a very good man of business .
The game is organised by the Rotary club of Bodmin and was last played in 2010 .
