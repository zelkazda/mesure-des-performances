He is well known in Angola , because of him the country was a Portuguese colony and has close ties with Portugal .
1482 ) to open up the African coast still further beyond the equator .
He raised the river Congo which he considered as the access road towards the realm of Prester John up to the neighborhood of the site of Matadi .
According to one authority ( a legend on the 1489 map of Henricus Martellus Germanus ) , Cão died off Cape Cross ; but João de Barros and others make him return to the Congo , and take thence a native envoy to Portugal .
