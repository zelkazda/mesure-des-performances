The country has land borders with Albania , the Republic of Macedonia and Bulgaria to the north , and Turkey to the east .
The Aegean Sea lies to the east of mainland Greece , the Ionian Sea to the west , and the Mediterranean Sea to the south .
Greece has the tenth longest coastline in the world at 14880 km ( 9246 mi ) in length , featuring a vast number of islands ( approximately 1400 , of which 227 are inhabited ) , including Crete , the Dodecanese , the Cyclades , and the Ionian Islands among others .
It is also a founding member of the United Nations , the OECD , and the Black Sea Economic Cooperation Organization .
Athens is the capital .
Other major cities include Thessaloniki , Piraeus , Patras , Heraklion and Larissa .
Byzantium remained a major cultural and military power for the next 1,123 years , until the Fall of Constantinople to the Ottoman Turks in 1453 .
In 1877 , Charilaos Trikoupis , who is attributed with the significant improvement of the country 's infrastructure , curbed the power of the monarchy to interfere in the assembly by issuing the rule of vote of confidence to any potential prime minister .
As a result of the Balkan Wars , Greece increased the extent of its territory and population .
In the following years , the struggle between King Constantine I and charismatic Prime Minister Eleftherios Venizelos over the country 's foreign policy on the eve of World War I dominated the country 's political scene , and divided the country into two opposed groups .
According to various sources , several hundred thousand Pontic Greeks died during this period .
The Greek population in Istanbul dropped from 300,000 at the turn of the century to around 3,000 in the city today .
The next 20 years were characterized by marginalisation of the left in the political and social spheres but also by rapid economic growth , propelled in part by the Marshall Plan .
King Constantine 's dismissal of George Papandreou 's centrist government in July 1965 prompted a prolonged period of political turbulence which culminated in a coup d'état on 21 April 1967 by the United States-backed Regime of the Colonels .
Meanwhile , Andreas Papandreou founded the Panhellenic Socialist Movement ( PASOK ) in response to Karamanlis 's conservative New Democracy party , with the two political formations alternating in government ever since .
Greece rejoined NATO in 1980 .
Widespread investments in industrial enterprises and heavy infrastructure , as well as funds from the European Union and growing revenues from tourism , shipping and a fast-growing service sector have raised the country 's standard of living to unprecedented levels .
The country adopted the euro in 2001 and successfully hosted the 2004 Olympic Games in Athens .
Greece is a parliamentary republic .
From the Constitutional amendment of 1986 the President 's duties were curtailed to a significant extent , and they are now largely ceremonial ; most political power thus lies in the hands of the Prime Minister .
Since the restoration of democracy , the Greek two-party system is dominated by the liberal-conservative New Democracy ( ND ) and the social-democratic Panhellenic Socialist Movement ( PASOK ) .
Other significant parties include the Communist Party of Greece ( KKE ) , the Coalition of the Radical Left ( SYRIZA ) and the Popular Orthodox Rally ( LAOS ) .
Administratively , Greece consists of thirteen peripheries subdivided into a total of fifty-one prefectures .
There is also one autonomous area , Mount Athos , which borders the periphery of Central Macedonia .
Greece consists of a mountainous , peninsular mainland jutting out into the sea at the southern end of the Balkans , the Peloponnesus peninsula , and numerous islands ( 1400 , 227 of which are inhabited ) , including Crete , Euboea , Lesbos , Chios , the Dodecanese and the Cycladic groups of the Aegean Sea as well as the Ionian Sea islands .
Greece has the tenth longest coastline in the world with 14880 km ( 9246 mi ) ; its land boundary is 1160 km ( 721 mi ) .
Western Greece contains a number of lakes and wetlands and is dominated by the Pindus mountain range .
Pindus is characterized by its high , steep peaks , often dissected by numerous canyons and a variety of other karstic landscapes .
Most notably , the impressive Meteora formation consisting of high , steep boulders provides a breathtaking experience for the hundreds of thousands of tourists who visit the area each year .
The Pindus mountain range strongly affects the climate of the country by making the western side of it ( areas prone to the south-westerlies ) wetter on average than the areas lying to the east of it ( lee side of the mountains ) .
The Mediterranean type of climate features mild , wet winters and hot , dry summers .
Temperatures rarely reach extreme values along the coasts , although , with Greece being a highly mountainous country , snowfalls occur frequently in winter .
It sometimes snows even in the Cyclades or the Dodecanese .
The city 's northern suburbs are dominated by the temperate type while the downtown area and the southern suburbs enjoy a typical Mediterranean type .
Annual growth of Greek GDP has surpassed the respective levels of most of its EU partners .
The Greek labor force totals 4.9 million , and it is the second most industrious between OECD countries , after South Korea .
However , the Greek economy also faces significant problems , including rising unemployment levels , an inefficient government bureaucracy and widespread corruption .
In 2009 , Greece had the EU 's second lowest Index of Economic Freedom ( after Poland ) , ranking 81 st in the world .
The country suffers from high levels of political and economic corruption and low global competitiveness relative to its EU partners .
By the end of 2009 , as a result of a combination of international ( financial crisis ) and local ( uncontrolled spending prior to the October 2009 national elections ) factors , the Greek economy faced its most severe crisis since 1993 , with the second highest budget deficit as well as the second highest debt ( after Italy ) to GDP ratio in the EU .
Greece 's public debt has risen from 94.6 % of GDP in 2008 to roughly 125 % of GDP in 2010 .
The shipping industry is a key element of Greek economic activity dating back to ancient times .
During the 1960s , the size of the Greek fleet nearly doubled , primarily through the investment undertaken by the shipping magnates Onassis and Niarchos .
According to the BTS , the Greek-owned maritime fleet is today the largest in the world , with 3,079 vessels accounting for 18 % of the world 's fleet capacity ( making it the largest of any other country ) with a total dwt of 141,931 thousand ( 142 million dwt ) .
In terms of ship categories , Greece ranks first in both tankers and dry bulk carriers , fourth in the number of containers , and fourth in other ships .
An important percentage of Greece 's income comes from tourism .
In Greece , the euro was introduced in 2002 .
For instance , a € 10 Greek commemorative coin can not be used in any other country .
Since the 1980s , the roads and rail network of Greece has been significantly modernized .
Important works include the Egnatia highway that connects north west Greece ( Igoumenitsa ) with northern and north east Greece .
The Rio-Antirio bridge ( the longest suspension cable bridge in Europe ) ( 2250 m or 7382 ft long ) connects the western Peloponnesus from Rio ( 7 km or 4 mi from Patras ) with Antirion on the central Greek mainland .
An expansion of the Patras-Athens national motorway towards Pyrgos in the western Peloponnese is scheduled to be completed by 2014 .
As statistics from 1971 , 1981 , and 2001 show , the Greek population has been aging the past several decades .
Greek society has also rapidly changed with the passage of time .
Greece 's largest municipalities in 2001 were : Athens , Thessaloniki , Piraeus , Patras , Iraklio , Larissa , and Volos .
The migration trend however has now been reversed after the important improvements of the Greek economy since the 80 's .
Due to the complexity of Greek immigration policy , practices and data collection , truly reliable data on immigrant populations in Greece is difficult to gather and therefore subject to much speculation .
Thessaloniki is the second largest cluster , with 27,000 , reaching 7 % of local population .
At the same time , Albanians constituted some 56 % of total immigrants , followed by Bulgarians ( 5 % ) , Georgians ( 3 % ) and Romanians ( 3 % ) .
The rest were around 690,000 persons of non-EU or non-homogeneis ( of non-Greek heritage ) status .
According to the same study , the foreign population ( documented and undocumented ) residing in Greece may in reality figure upwards to 8.5 % or 10.3 % , that is approximately meaning 1.15 million -- if immigrants with homogeneis cards are accounted for .
Greece is a gateway for the entry of illegal immigrants to Europe .
The main objective is to facilitate the smooth integration of legal immigrants and their children in the Greek social reality .
The constitution of Greece recognizes the Greek Orthodox faith as the " prevailing " religion of the country , while guaranteeing freedom of religious belief for all .
Judaism has existed in Greece for more than 2,000 years .
Greek members of Roman Catholic faith are estimated at 50,000 with the Roman Catholic immigrant community approximating 200,000 .
There are not official statistics about Free Apostolic Church of Pentecost , but the Orthodox Church estimates the followers in 20,000 .
The Jehovah 's Witnesses report having 28,243 active members .
The Jewish community in Greece traditionally spoke Ladino ( Judeo-Spanish ) , today maintained only by a small group of a few thousand speakers .
Greece 's post-compulsory secondary education consists of two school types : unified upper secondary schools and technical -- vocational educational schools .
Additionally , students over twenty-two years old may be admitted to the Hellenic Open University through a form of lottery .
Some of the main universities in Greece include :
A new period of philosophy started with Socrates .
Aspects of Socrates were first united from Plato , who also combined with them many of the principles established by earlier philosophers , and developed the whole of this material into the unity of a comprehensive system .
Aristotle of Stagira , the most important disciple of Plato , shared with his teacher the title of the greatest philosopher of antiquity but while Plato had sought to elucidate and explain things from the supra-sensual standpoint of the forms , his pupil preferred to start from the facts given us by experience .
Of the hundreds of tragedies written and performed during the classical age , only a limited number of plays by three authors have survived : Aeschylus , Sophocles , and Euripides .
The surviving plays by Aristophanes are also a treasure trove of comic presentation , while Herodotus and Thucydides are two of the most influential historians in this period .
It is a verse romance written around 1600 by Vitsentzos Kornaros ( 1553 -- 1613 ) .
Internet cafes that provide net access , office applications and multiplayer gaming are also a common sight in the country , while mobile internet on 3G cellphone networks and public wi-fi hotspots are existent , but not as extensive .
In 1994 , Greece and ESA signed their first cooperation agreement .
Having formally applied for full membership in 2003 , Greece became ESA 's sixteenth member on 16 March 2005 .
Some dishes can be traced back to ancient Greece like skordalia [ citation needed ] ( a thick purée of potatoes , walnuts , almonds , crushed garlic and olive oil ) , lentil soup , retsina ( white or rosé wine sealed with pine resin ) and pasteli ( candy bar with sesame seeds baked with honey ) .
Throughout Greece people often enjoy eating from small dishes such as meze with various dips such as tzatziki , grilled octopus and small fish , feta cheese , dolmades ( rice , currants and pine kernels wrapped in vine leaves ) , various pulses , olives and cheese .
Greek cuisine differs widely from different parts of the mainland and from island to island also uses some flavorings more often than other Mediterranean cuisines do : oregano , mint , garlic , onion , dill and bay laurel leaves .
Many Greek recipes , especially in the northern parts of the country , use " sweet " spices in combination with meat , for example cinnamon and cloves in stews .
However , the isolation of Byzantiumafter 1453 , which kept music away from polyphony , along with centuries of continuous culture , enabled monophonic music to develop to the greatest heights of perfection , despite the ottoman influences .
The klephtic cycle came into being between the late Byzantine period and the start of the Greek War of Independence struggle in 1821 .
The events and social changes of the 20th century changed the fate of the folk song in Greece .
Greece , home to the first modern Olympics , holds a long tradition in sports .
The Greek Super League is the highest professional football league in the country comprising 16 teams .
The most successful are Olympiacos , Panathinaikos , AEK Athens and PAOK FC .
The Greek national basketball team has a decades-long tradition of excellence in the sport .
They have won the European Championship twice in 1987 and 2005 , and have reached the final four in three of the last four FIBA World Championships , taking second place in 2006 .
The domestic top basketball league , A1 Ethniki , is composed of fourteen teams .
The most successful Greek teams are Panathinaikos , Olympiacos , AEK Athens and PAOK .
Water polo and volleyball are also practiced widely in Greece while cricket , handball are relatively popular in Corfu and Veroia respectively .
As the birth place of the Olympic Games , Greece was most recently host of 2004 Summer Olympics and the first modern Olympics in 1896 .
In 2009 , Greece beat France in the under-20 European Basketball championship .
The Hellenic Armed Forces are overseen by the Hellenic National Defense General Staff and consists of three branches :
The civilian authority for the Greek military is the Ministry of National Defence .
Furthermore , Greece maintains the Hellenic Coast Guard for law enforcement in the sea and search and rescue .
Greece currently has universal compulsory military service for males while females ( who may serve in the military ) are exempted from conscription .
As of 2009 , Greece has mandatory military service of 9 months for male citizens between the ages of 19 and 45 .
As a member of NATO , the Greek military participates in exercises and deployments under the auspices of the alliance .
