Less commonly , it can refer to the individual teaching of Calvin himself .
John Calvin 's international influence on the development of the doctrines of the Protestant Reformation began in 1534 when Calvin was 25 .
That marks his start on the first edition of Institutes of the Christian Religion ( published 1536 ) .
Evangelical churches began to form after Martin Luther was excommunicated from the Catholic Church .
He had signed the Lutheran Augsburg Confession as it was revised by Melancthon in 1540 .
However , his influence was first felt in the Swiss Reformation whose leader was Ulrich Zwingli .
John Marrant had organized a congregation there under the auspices of the Huntingdon Connection .
Especially large are those in Indonesia , Korea and Nigeria .
Today , the World Alliance of Reformed Churches has 75 million believers .
They were rejected in 1619 at the Synod of Dort , more than 50 years after the death of Calvin .
As the regulative principle is reflected in Calvin 's own thought , it is driven by his evident antipathy toward the Roman Catholic Church and her worship , and it associates musical instruments with icons , which he considered violations of the Ten Commandments ' prohibition of graven images .
The influential Westminster Confession of Faith also teaches the infralapsarian view , but is sensitive to those holding to supralapsarianism .
R.C. Sproul believes there is confusion about what the doctrine of limited atonement actually teaches .
While he considers it possible for a person to believe four points without believing the fifth , he claims that a person who really understands the other four points must believe in limited atonement because of what Martin Luther called a resistless logic .
Its intellectual founder , the late Rousas J. Rushdoony , based much of his understanding on the apologetical insights of Cornelius Van Til , father of presuppositionalism and professor at Westminster Theological Seminary ( although Van Til himself did not hold to such a view ) .
Such a connection was advanced in influential works by R. H. Tawney ( 1880 -- 1962 ) and by Max Weber ( 1864 -- 1920 ) .
