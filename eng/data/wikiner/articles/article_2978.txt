Charles Francis Hockett ( January 17 , 1916 -- November 3 , 2000 ) was an American linguist who developed many influential ideas in American structuralist linguistics .
He represents the post-Bloomfieldian phase of structuralism often referred to as " distributionalism " or " taxonomic structuralism " .
His academic career spanned over half a century in Cornell and Rice universities .
While studying at Yale , Hockett studied with several other influential linguists such as Edward Sapir , George P. Murdock , and Benjamin Whorf .
In 1948 his dissertation was published as a series in the International Journal of American Linguistics .
In 1957 , Hockett became a member of Cornell 's anthropology department and continued to teach anthropology and linguistics until he retired to emeritus status in 1982 .
Charles Hockett held membership among many academic institutions such as the National Academy of Sciences the American Academy of Arts and Sciences , and the Society of Fellows at Harvard University .
Outside the realm of linguistics and anthropology , Hockett practiced musical performance and composition .
One of Hockett 's most important contributions was his development of the design-feature approach to comparative linguistics where he attempted to distinguish the similarities and differences among animal communication systems and human language .
Hockett argued that while every communication system has some of the 13 design features , only human , spoken language has all 13 features .
While Hockett believed that all communication systems , animal and human alike , share many of these features , only human language contains all of the 13 design features .
