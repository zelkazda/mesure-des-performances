It was created in Brazil by slaves from Africa , sometime after the sixteenth century .
African-derived combat games similar to wrestling and stick fighting were also witnessed and documented in seventeenth-century Barbados , eighteenth-century Jamaica , and nineteenth century Venezuela .
Stick fighting was and still is practiced in Trinidad , Carriacou , Dominica , and Haiti .
Having saved capoeira from illegality , Mestre Bimba began being challenged by other capoeira masters who possessed their own unique capoeira styles , such as capoeira angola .
Another common explanation is that slaves in Brazil were commonly shackled at the wrists , restricting them from using their hands [ citation needed ] .
The word comes from the historical folklore of Brasil , in which men who were marginalized from main stream society and possessed street smarts were called malandros .
Developed by other people from Bimba 's regional , this type of game is characterized by high jumps , acrobatics , and spinning kicks .
