He succeeded him and became the second master of that school where he counted Anaximenes and Pythagoras amongst his pupils .
Like many thinkers of his time , Anaximander 's contributions to philosophy relate to many disciplines .
His knowledge of geometry allowed him to introduce the gnomon in Greece .
With his assertion that physical forces , rather than supernatural means , create order in the universe , Anaximander can be considered the first scientist .
It is debatable whether Thales actually was the teacher of Anaximander , but there is no doubt that Anaximander was influenced by Thales ' theory that everything is derived from water .
But if we follow carefully the course of Anaximander 's ideas , we will notice that there was not such an abrupt break as initially appears .
He proposed the theory of the apeiron in direct response to the earlier theory of his teacher , Thales , who had claimed that the primary substance was water .
For Anaximander , the principle of things , the constituent of all substances , is nothing determined and not an element such as water in Thales ' view .
Anaximander argues that water can not embrace all of the opposites found in nature -- for example , water can only be wet , never dry -- and therefore can not be the one primary substance ; nor could any of the other candidates .
Anaximander maintains that all dying things are returning to the element from which they came ( apeiron ) .
The one surviving fragment of Anaximander 's writing deals with this matter .
Simplicius transmitted it as a quotation , which describes the balanced and mutual changes of the elements :
Simplicius mentions that Anaximander said all these " in poetic terms " , meaning that he used the old mythical language .
The goddess Justice ( Dike ) keeps the cosmic order .
Anaximander 's bold use of non-mythological explanatory hypotheses considerably distinguishes him from previous cosmology writers such as Hesiod .
Anaximander was the first to conceive a mechanical model of the world .
It goes further than Thales ' claim of a world floating on water , for which Thales faced the problem of explaining what would contain this ocean , while Anaximander solved it by introducing his concept of infinite ( apeiron ) .
Furthermore , according to Diogenes Laertius , he built a celestial sphere .
The doxographer and theologian Aetius attributes to Pythagoras the exact measurement of the obliquity .
According to Simplicius , Anaximander already speculated on the plurality of worlds , similar to atomists Leucippus and Democritus , and later philosopher Epicurus .
In addition to Simplicius , Hippolytus reports Anaximander 's claim that from the infinite comes the principle of beings , which themselves come from the heavens and the worlds ( several doxographers use the plural when this philosopher is referring to the worlds within , which are often infinite in quantity ) .
Cicero writes that he attributes different gods to the countless worlds .
Anaximander attributed some phenomena , such as thunder and lightning , to the intervention of elements , rather than to divine causes .
Anaximander speculated about the beginnings and origin of animal life .
The 3rd century Roman writer Censorinus reports :
Both Strabo and Agathemerus claim that , according to the geographer Eratosthenes , Anaximander was the first to publish a map of the world .
Strabo viewed both as the first geographers after Homer .
The centre or " navel " of the world ( ὀμφαλός γῆς / omphalós gẽs ) could have been Delphi , but is more likely in Anaximander 's time to have been located near Miletus .
The Aegean Sea was near the map 's centre and enclosed by three continents , themselves located in the middle of the ocean and isolated like islands by sea and rivers .
Europe was bordered on the south by the Mediterranean Sea and was separated from Asia by the Black Sea ) , the Lake Maeotis , and , further east , either by the Phasis River ( now called the Rioni ) or the Tanais .
The Suda relates that Anaximander explained some basic notions of geometry .
It also mentions his interest in the measurement of time and associates him with the introduction in Greece of the gnomon .
In Lacedaemon , he participated in the construction , or at least in the adjustment , of sundials to indicate solstices and equinoxes .
However , the invention of the gnomon itself can not be attributed to Anaximander because its use , as well as the division of days into twelve parts , came from the Babylonians .
On the other hand , equinoxes do not correspond to the middle point between the positions during solstices , as the Babylonians thought .
The city collapsed when the top of the Taygetus split like the stern of a ship .
Pliny the Elder also mentions this anecdote , suggesting that it came from an " admirable inspiration " , as opposed to Cicero , who did not associate the prediction with divination .
Bertrand Russell in the History of Western Philosophy interprets Anaximander 's theories as an assertion of the necessity of an appropriate balance between earth , fire , and water , all of which may be independently seeking to aggrandize their proportions relative to the others .
Anaximander seems to express his belief that a natural order ensures balance between these elements , that where there was fire , ashes ( earth ) now exist .
In other words , Anaximander viewed " ... all coming-to-be as though it were an illegitimate emancipation from eternal being , a wrong for which destruction is the only penance " .
According to the Suda :
