Before 1873 , England had two parallel court systems : courts of " law " that could only award money damages and recognized only the legal owner of property , and courts of " equity " ( courts of chancery ) that could issue injunctive relief ( that is , a court order to a party to do something , give something to someone , or stop doing something ) and recognized trusts of property .
This split propagated to many of the colonies , including the United States .
For most purposes , most jurisdictions , including the U.S. federal system and most states , have merged the two courts .
Judge Cardozo held :
Note that Cardozo 's new " rule " exists in no prior case , but is inferable as a synthesis of the " thing of danger " principle stated in them , merely extending it to " foreseeable danger " even if " the purposes for which it was designed " were not themselves " a source of great danger . "
MacPherson takes some care to present itself as foreseeable progression , not a wild departure .
Note that Judge Cardozo continues to adhere to the original principle of Winterbottom , that " absurd and outrageous consequences " must be avoided , and he does so by drawing a new line in the last sentence quoted above : " There must be knowledge of a danger , not merely possible , but probable . "
But while adhering to the underlying principle that some boundary is necessary , MacPherson overruled the prior common law by rendering the formerly dominant factor in the boundary , that is , the privity formality arising out of a contractual relationship between persons , totally irrelevant .
It was settled in the case of United States v. Hudson and Goodwin , 11 U.S. 32 ( 1812 ) , which decided that federal courts had no jurisdiction to define new common law crimes , and that there must always be a ( constitutional ) statute defining the offense and the penalty for it .
In contrast , in non-common-law countries , and jurisdictions with very weak respect for precedent ( example , the U.S. Patent Office ) , fine questions of law are redetermined anew each time they arise , making consistency and prediction more difficult , and procedures far more protracted than necessary because parties can not rely on written statements of law as reliable guides .
This is the reason for the frequent choice of the law of the State of New York in commercial contracts .
Somewhat surprisingly , contracts throughout the world often choose the law of New York , even where the relationship of the parties and transaction to New York is quite attenuated .
Because of its history as the nation 's commercial center , New York common law has a depth and predictability not ( yet ) available in any other jurisdiction .
Similarly , corporations are often formed under Delaware corporate law , and contracts relating to corporate law issues ( merger and acquisitions of companies , rights of shareholders , etc. ) include a Del. choice of law clause , because of the deep body of law in Delaware on these issues .
For that reason , civil law statutes tend to be somewhat more detailed than statutes written by common law legislatures -- but , conversely , that tends to make the statute more difficult to read ( the U.S. tax code is an example ) .
The term " common law " originally derives from after the Norman Conquest .
The " common law " was the law that emerged as " common " throughout the realm , as the king 's judges imposed a unified common law throughout England .
The doctrine of precedent developed under the inquisitorial system in England during the 12th and 13th centuries , as the collective judicial decisions that were based in tradition , custom and precedent .
Before the Norman conquest in 1066 , justice was administered primarily by county courts , presided by the diocesan bishop and the sheriff , exercising both ecclesiastical and civil jurisdiction .
In 1154 , Henry II became the first Plantagenet king .
Among many achievements , Henry institutionalized common law by creating a unified system of law " common " to the country through incorporating and elevating local custom to the national , ending local control and peculiarities , eliminating arbitrary remedies and reinstating a jury system -- citizens sworn on oath to investigate reliable criminal accusations and civil claims .
Henry II developed the practice of sending judges from his own central court to hear the various disputes throughout the country .
Henry II 's creation of a powerful and unified court system , which curbed somewhat the power of canonical ( church ) courts , brought him ( and England ) into conflict with the church , most famously with Thomas Becket , the Archbishop of Canterbury .
Eventually , Becket was murdered inside Canterbury Cathedral by four knights who believed themselves to be acting on Henry 's behalf .
Henry was forced to repeal the disputed laws and to abandon his efforts to hold church members accountable for secular crimes ( see also Constitutions of Clarendon ) .
Judge-made common law operated as the primary source of law for several hundred years , before Parliament acquired legislative powers to create statutory law .
Alexander Hamilton emphasized in The Federalist that this New York constitutional provision expressly made the common law subject " to such alterations and provisions as the legislature shall from time to time make concerning the same . "
Nathan Dane , the primary author of the Northwest Ordinance , viewed this provision as a default mechanism in the event that federal or territorial statutes were silent about a particular matter ; he wrote that if " a statute makes an offence , and is silent as to the mode of trial , it shall be by jury , according to the course of the common law . "
In this way , the common law was eventually incorporated into the legal systems of every state except Louisiana .
Judicial decisions and treatises of the 17th and 18th centuries , such at those of Lord Chief Justice Edward Coke , presented the common law as a collection of such maxims .
In the early 20th century , Louis Brandeis , later appointed to the United States Supreme Court , became noted for his use of policy-driving facts and economics in his briefs , and extensive appendices presenting facts that lead a judge to the advocate 's conclusion .
A famous example is the fictional case of Jarndyce and Jarndyce in Bleak House , by Charles Dickens .
In the United States , parallel systems of law ( providing money damages , with cases heard by a jury upon either party 's request ) and equity ( fashioning a remedy to fit the situation , including injunctive relief , heard by a judge ) survived well into the 20th century .
For example , most proceedings before U.S. federal and state agencies are inquisitorial in nature , at least the initial stages ( e.g. , a patent examiner , a social security hearing officer , etc. ) even though the law to be applied is developed through common law processes .
The state of N.Y. , which also has a civil law history from its Dutch colonial days , also began a codification of its law in the 19th century .
The only part of this codification process that was considered complete is known as the Field Code applying to civil procedure .
When the English captured pre-existing colonies they continued to allow the local settlers to keep their civil law .
The reason for the enactment of the codes in Calif. in the nineteenth century was to replace a pre-existing system based on Spanish civil law with a system based on common law , similar to that in most other states .
The California courts have treated portions of the codes as an extension of the common-law tradition , subject to judicial development in the same manner as judge-made common law .
The United States federal government ( as opposed to the states ) only partially has a common law system .
Indian laws also adhere to the United Nations guidelines on human rights law and the environmental law .
Certain international trade laws , such as those on intellectual property , are also enforced in India .
The exception to this rule is in the state of Goa , where a Portuguese uniform civil code is in place , in which all religions have a common law regarding marriages , divorces and adoption .
Ancient India represented a distinct tradition of law , and had an historically independent school of legal theory and practice .
The Arthashastra , dating from 400 BC and the Manusmriti , from 100 AD , were influential treatises in India , texts that were considered authoritative legal guidance .
Manu 's central philosophy was tolerance and pluralism , and was cited across Southeast Asia .
Criminal law , which is uniform throughout Canada , is based on the common law in all provinces .
Canadian federal statutes , when they refer to matters that are within provincial jurisdiction , such as property ownership , must use the terminology of both the common law and civil law for those matters ; this is referred to as legislative bijuralism .
Examples of common law being replaced by statute or codified rule in the United States include criminal law ( since 1812 , U.S. courts have held that criminal law must be embodied in statute if the public is to have fair notice ) , commercial law and procedure .
The next definitive historical treatise on the common law is Commentaries on the Laws of England , written by Sir William Blackstone and first published in 1765 -- 1769 .
While he was still on the Massachusetts Supreme Judicial Court , and before being named to the U.S. Supreme Court , Justice Oliver Wendell Holmes Jr. published a short volume called The Common Law , which remains a classic in the field .
The Corpus Juris Secundum is an encyclopedia whose main content is a compendium of the common law and its variations throughout the various state jurisdictions .
