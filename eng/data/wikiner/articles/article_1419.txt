Then , in 1828 , Friedrich Wöhler published a paper on the synthesis of urea , proving that organic compounds can be created artificially .
The dawn of biochemistry may have been the discovery of the first enzyme , diastase ( today called amylase ) , in 1833 by Anselme Payen .
Eduard Buchner contributed the first demonstration of a complex biochemical process outside of a cell in 1896 : alcoholic fermentation in cell extracts of yeast .
In the 1950s , James D. Watson , Francis Crick , Rosalind Franklin , and Maurice Wilkins were instrumental in solving DNA structure and suggesting its relationship with genetic transfer of information .
In 1988 , Colin Pitchfork was the first person convicted of murder with DNA evidence , which led to growth of forensic science .
