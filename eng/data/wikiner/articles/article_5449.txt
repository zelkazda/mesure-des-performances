Sienkiewicz was meticulous in attempting to recreate the authenticity of historical language .
He was baptized in the neighbouring village of Okrzeja in a church founded by his great-grandmother .
His family moved several times and in the end settled in Warsaw in 1861 .
In 1858 , Henryk began secondary school in Warsaw .
According to his parents ' wishes , he passed the examination to the medical department at Warsaw University .
In 1869 Sienkiewicz debuted as a journalist .
Sienkiewicz also visited his relative Jadwiga Łuszczewska and the actress Helena Modrzejewska , as their dinner parties were very popular .
He stayed for some time in Calif. .
In France he had got a chance to familiarize himself with naturalism , a new trend in literature .
Finally , when I noticed that every man has got hair like Sienkiewicz and all of the young men , one by one , grow a royal beard and try to have a statuesque and swarthy face , I realised that I wanted to meet him personally. ( ... )
It takes place during the reign of King Jan Kazimierz , during the Cossack revolt . "
With Fire and Sword was enthusiastically received by readers and won national recognition for the author .
Many readers wrote to Sienkiewicz , asking about the next adventures of their favorite characters .
In 1884 Jacek Malczewski exhibited tableaux vivants inspired by With Fire and Sword .
The novel quickly became a best-seller and it established Sienkiewicz 's position in society .
Sienkiewicz used this money to open the scholarship fund ( named after his wife ) designed for artists endangered by tuberculosis .
In 1893 Sienkiewicz began preparations for his next novel , Quo Vadis .
Their wedding took place on November 11 , 1893 , but the bride soon left the author and Sienkiewicz obtained papal consent to dissolution of the marriage .
In February 1895 Sienkiewicz wrote the first chapters of Quo Vadis , for which he had been gathering materials since 1893 .
The popularity of Quo Vadis at that time was supported by the fact that the horses competing in Grand Prix de Paris were given names of the characters from the book .
In 1913 Quo Vadis was screened and the novel was filmed several more times .
In 1900 Sienkiewicz celebrated an anniversary of his artistic work .
In the same year the Jagiellonian University awarded Sienkiewicz an honorary doctorate .
Sienkiewicz involved himself in social matters .
In 1901 he made an appeal in a cause of children in Września .
He actually received it " for his outstanding merits as an epic writer " although Quo vadis perhaps brought him the widest international recognition .
He died on November 15 , 1916 , in Vevey , where he was buried .
In 1924 , after Poland had regained its independence , the writer 's ashes were repatriated to Warsaw , Poland , and placed in the crypt of St. John 's Cathedral .
