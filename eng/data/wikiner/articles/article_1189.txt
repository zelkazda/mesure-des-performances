Like his father , Woody Guthrie , Arlo often sings songs of protest against social injustice .
One of Guthrie 's works is " Alice 's Restaurant Massacree " , a satirical talking blues song of about 18 minutes in length .
Arlo Guthrie was born in Brooklyn , New York , the son of folk singer and composer Woody Guthrie and his wife Marjorie Mazia Guthrie .
His sister is Nora Guthrie .
Arlo Guthrie received religious training for his bar mitzvah from Rabbi Meir Kahane , who would go on to form the Jewish Defense League .
" Rabbi Kahane was a really nice , patient teacher, " Guthrie later recalled , " but shortly after he started giving me my lessons , he started going haywire .
As a singer , songwriter and lifelong political activist , Guthrie carries on the legacy of his legendary father .
His most famous work is " Alice 's Restaurant Massacree " , a talking blues song that lasts 18 minutes and 34 seconds in its original recorded version .
Guthrie has pointed out that this was also the exact length of one of the famous gaps in Richard Nixon 's Watergate tapes .
The song , a bitingly satirical protest against the Vietnam War draft , although Guthrie stated in a 2009 interview with Ron Bennington that Alice 's Restaurant is more an " anti-stupidity " song than an anti-war song , is based on a true incident .
For a short period of time after its release in 1967 , " Alice 's Restaurant " was heavily played on U.S. college and counter-culture radio stations .
A 1969 film , directed and co-written by Arthur Penn , was based on the story .
In addition to acting in this film , also called Alice 's Restaurant , Guthrie has had minor roles in several movies and television series .
Guthrie 's memorable appearance at the 1969 Woodstock Festival was documented in the Michael Wadleigh film Woodstock .
In 1972 Guthrie made famous Steve Goodman 's song " City of New Orleans " , a paean to long-distance passenger rail travel .
Guthrie 's first trip on that train was in December 2005 .
Guthrie 's 1976 album Amigo received a 5-star ( highest rating ) from Rolling Stone , and may be his best-received work ; unfortunately that milestone album , like Guthrie 's earlier Warner Brothers albums , is rarely heard today even though each boasts compelling folk and folk rock music accompanied by top-notch musicians such as Ry Cooder .
Although the band received good reviews , it never gained the popularity that Guthrie did while playing solo .
They did play many of Guthrie 's most famous songs , which were most requested .
By this time , Guthrie had developed his own sound in versions of " talking blues " songs .
Though Guthrie is best known for being a musician , singer , and composer , throughout the years he has also appeared as an actor in films and on television .
The film Alice 's Restaurant ( 1969 ) is his best known role , but he has had small parts in several films and even co-starred in a television drama , Byrds of Paradise .
I look forward to the day when we can work out the differences we have with the same revolutionary vision and enthusiasm that is our American legacy . "
Like his father , Woody Guthrie , Guthrie often sings songs of protest against social injustice .
He collaborated with poet Adrian Mitchell to tell the story of Chilean folk singer and activist Víctor Jara in song .
He regularly performed with folk legend Pete Seeger , one of his father 's longtime partners .
