In law , affiliation ( from Latin ad-filiare , to adopt as a son ) is the term to describe a partnership between two or more parties .
Any woman who is single , a widow , or a married woman living apart from her husband , may make an application for a summons , and it is immaterial where the child is begotten , provided it is born in England .
There is no such special legislation with regard to sailors in the Royal Navy .
In the British colonies , and in the states of the United States ( with the ( usually termed filiation ) akin to that described above , by means of which a mother can obtain a contribution to the support of her illegitimate child from the putative father .
On the continent of Europe , however , the legislation of the various countries differs rather widely .
Affiliation , in France , is a term applied to a species of adoption by which the person adopted succeeds equally with other heirs to the acquired , but not to the inherited , property of the deceased .
