He also composed oratorios , including The Dream of Gerontius , chamber music and songs .
Elgar has been described as " the first composer to take the gramophone seriously . "
After the microphone was invented , making realistic recording possible , he conducted new recordings of most of his major orchestral works , and excerpts from The Dream of Gerontius .
Edward was the fourth of their seven children .
At his instigation , masses by Cherubini and Hummel were first heard at the Three Choirs Festival by the orchestra in which he played the violin .
All the Elgar children received a musical upbringing .
By the age of eight , Elgar was taking piano and violin lessons , and his father , who tuned the pianos at many grand houses in Worcestershire , would sometimes take him along , giving him the chance to display his skill to important local figures .
Elgar 's mother had " a taste and inclination for the arts " and encouraged his musical development .
Elgar said " my first music was learnt in the Cathedral … from books borrowed from the music library , when I was eight , nine or ten . "
He later said that he had been most helped by Hubert Parry 's articles in the Grove Dictionary of Music and Musicians .
Years later a profile in The Musical Times considered that his failure to get to Leipzig was fortunate for Elgar 's musical development : " Thus the budding composer escaped the dogmatism of the schools . "
However , it was a disappointment to Elgar that on leaving school in 1872 he went not to Leipzig but to the office of a local solicitor as a clerk .
After a few months , Elgar left the solicitor to embark on a musical career , giving piano and violin lessons and working occasionally in his father 's shop .
At 22 he took up the post of conductor of the attendants ' band at the Worcester and County Lunatic Asylum in Powick , three miles southwest of Worcester .
Elgar coached the players and wrote and arranged their music , including quadrilles and polkas , for the unusual combination of instruments .
The Musical Times wrote , " This practical experience proved to be of the greatest value to the young musician .
He held the post for five years , from 1879 , travelling to Powick once a week .
Another post he held in his early days was professor of the violin at the Worcester College for the Blind Sons of Gentlemen .
Although somewhat solitary and introspective by nature , Elgar thrived in Worcester 's musical circles .
Elgar arranged " dozens of pieces by Mozart , Beethoven , Haydn , and other masters " for the quintet , honing his arranging and compositional skills .
He heard Camille Saint-Saëns play the organ at the Madeleine , and attended concerts by first-rate orchestras .
In 1882 he wrote , " I got pretty well dosed with Schumann ( my ideal !
) , Brahms , Rubinstein & Wagner , so had no cause to complain . "
They were married on 8 May 1889 , at Brompton Oratory .
As an engagement present , Elgar dedicated his short violin and piano piece Salut d'Amour to her .
Elgar took full advantage of the opportunity to hear unfamiliar music .
Among these were masters of orchestration from whom he learned much , such as Berlioz and Wagner .
For example , an offer from the Royal Opera House , Covent Garden , to run through some of his works was withdrawn at the last second when Sir Arthur Sullivan arrived unannounced to rehearse some of his own music .
Sullivan was horrified when Elgar later told him what had happened .
Elgar conducted the first performance in Worcester in September 1890 .
During the 1890s , Elgar gradually built up a reputation as a composer , chiefly of works for the great choral festivals of the English Midlands .
Other works of this decade included the Serenade for Strings ( 1892 ) and Three Bavarian Dances ( 1897 ) .
Elgar was of enough consequence locally to recommend the young composer Samuel Coleridge-Taylor to the Three Choirs Festival for a concert piece , which helped establish the younger man 's career .
Elgar was catching the attention of prominent critics , but their reviews were polite rather than enthusiastic .
His friend August Jaeger tried to lift his spirits , " A day 's attack of the blues ... will not drive away your desire , your necessity , which is to exercise those creative faculties which a kind providence has given you .
Elgar dedicated the work " To my friends pictured within " .
The enigma is that , although there are fourteen variations on the " original theme " , there is another overarching theme , never identified by Elgar , which he said " runs through and over the whole set " but is never heard .
Elgar 's next major work was eagerly awaited .
For the Birmingham Triennial Music Festival of 1900 , he set John Henry Newman 's poem The Dream of Gerontius for soloists , chorus and orchestra .
Richter conducted the premiere , which was marred by a poorly prepared chorus , which sang badly .
Elgar was deeply depressed , but the critics recognised the mastery of the piece despite the defects in performance .
... Elgar stands on the shoulders of Berlioz , Wagner , and Liszt , from whose influences he has freed himself until he has become an important individuality .
Since the days of Liszt nothing has been produced in the way of oratorio … which reaches the greatness and importance of this sacred cantata . "
Elgar is probably best known for the first of the five Pomp and Circumstance Marches , which were composed between 1901 and 1930 .
When the theme of the slower middle section ( technically called the " trio " ) of the first march came into his head , he told his friend Dora Penny , " I 've got a tune that will knock ' em -- will knock ' em flat " .
To mark the coronation of Edward VII , Elgar was commissioned to set A. C. Benson 's Coronation Ode for a gala concert at the Royal Opera House in June 1901 .
The approval of the king was confirmed , and Elgar began work .
The contralto Clara Butt had persuaded him that the trio of the first Pomp and Circumstance march could have words fitted to it , and Elgar invited Benson to do so .
The publishers of the score recognised the potential of the vocal piece , " Land of Hope and Glory " , and asked Benson and Elgar to make a further revision for publication as a separate song .
He regretted the controversy and was glad to hand on the post to his friend Granville Bantock in 1908 .
Both W.S. Gilbert and Thomas Hardy sought to collaborate with Elgar in this decade .
Elgar refused , but would have collaborated with George Bernard Shaw had Shaw been willing .
Elgar 's principal composition in 1905 was the Introduction and Allegro for Strings , dedicated to Samuel Sanford , professor at Yale University .
His next large-scale work was the sequel to The Apostles -- the oratorio The Kingdom ( 1906 ) .
It was well-received but did not catch the public imagination as The Dream of Gerontius had done and continued to do .
As Elgar approached his fiftieth birthday , he began work on his first symphony , a project that had been in his mind in various forms for nearly ten years .
His Symphony No. 1 , in A-flat ( 1908 ) was a national and international triumph .
The Violin Concerto in B minor ( 1910 ) was commissioned by Fritz Kreisler , one of the leading international violinists of the time .
Elgar wrote it during the summer of 1910 , with occasional help from the violinist W. H. " Billy " Reed , the leader of the London Symphony Orchestra , who helped the composer with advice on technical points .
Reed , with the composer at the piano , gave a run-through of the completed work to Elgar 's friends shortly before the premiere .
The work was presented by the Royal Philharmonic Society , with Kreisler and the London Symphony Orchestra ( LSO ) , conducted by the composer .
So great was the impact of the concerto that Kreisler 's rival Eugène Ysaÿe spent much time with Elgar going through the work .
The Violin Concerto was Elgar 's last popular triumph .
There Elgar composed his last two large-scale works of the pre-war era , the choral ode , The Music Makers ( for the Birmingham Festival , 1912 ) and the symphonic study Falstaff .
Land of Hope and Glory , already popular , became still more so , and Elgar wished in vain to have new , less nationalistic , words sung to the tune .
His last large-scale composition of the war years was The Fringes of the Fleet , settings of verses by Rudyard Kipling , performed with great popular success around the country , until Kipling for unexplained reasons objected to their performance in theatres .
Elgar conducted a recording of the work for the Gramophone Company .
Towards the end of the war , Elgar was in poor health .
There Elgar recovered his strength and , in 1918 and 1919 , he produced four large-scale works .
On hearing the work in progress , Alice Elgar wrote in her diary , " E. writing wonderful new music " .
The Times wrote , " Elgar 's sonata contains much that we have heard before in other forms , but as we do not at all want him to change and be somebody else , that is as it should be . "
The quartet and quintet were premiered at the Wigmore Hall on 21 May 1919 .
The Manchester Guardian wrote , " This quartet , with its tremendous climaxes , curious refinements of dance-rhythms , and its perfect symmetry , and the quintet , more lyrical and passionate , are as perfect examples of chamber music as the great oratorios were of their type . "
By contrast , the remaining work , the Cello Concerto in E minor , had a disastrous premiere , at the opening concert of the London Symphony Orchestra 's 1919 -- 20 season in October 1919 .
Apart from the Elgar work , which the composer conducted , the rest of the programme was conducted by Albert Coates , who overran his rehearsal time at the expense of Elgar 's .
Lady Elgar wrote , " that brutal selfish ill-mannered bounder ... that brute Coates went on rehearsing . "
The critic of The Observer , Ernest Newman , wrote , " There have been rumours about during the week of inadequate rehearsal .
The work itself is lovely stuff , very simple -- that pregnant simplicity that has come upon Elgar 's music in the last couple of years -- but with a profound wisdom and beauty underlying its simplicity . "
Elgar attached no blame to his soloist , Felix Salmond , who played for him again later .
Although in the 1920s Elgar 's music was no longer in fashion , Elgar 's admirers continued to present his works when possible .
Also in 1920 , Landon Ronald presented an all-Elgar concert at the Queen 's Hall .
Alice Elgar wrote with enthusiasm about the reception of the symphony , but this was one of the last times she heard Elgar 's music played in public .
Elgar was devastated by the loss of his wife .
For much of the rest of his life Elgar indulged himself in his several hobbies .
He had from his youth enjoyed football , supporting Wolverhampton Wanderers , and in his later years he frequently attended horseraces .
His protégés , the conductor Malcolm Sargent and violinist Yehudi Menuhin , both recalled rehearsals with Elgar at which he swiftly satisfied himself that all was well and then went off to the races .
Almost nothing is recorded about the events that Elgar encountered during the trip , which gave the historical novelist James Hamilton-Paterson considerable latitude when writing Gerontius , a fictional account of the journey .
From 1926 onwards , Elgar made a series of recordings of his own works .
Elgar , " the first composer to take the gramophone seriously " , had already recorded much of his music by the early acoustic-recording process for His Master 's Voice from 1914 onwards , but the introduction of electrical microphones in 1926 transformed the gramophone from a novelty into a realistic medium for reproducing orchestral and choral music .
Elgar was the first composer to take full advantage of this technological advance .
Later in the series of recordings , Elgar also conducted two newly-founded orchestras , Boult 's BBC Symphony Orchestra and Sir Thomas Beecham 's London Philharmonic Orchestra .
It is believed to be the only surviving sound film of Elgar , who makes a brief remark before conducting the London Symphony Orchestra , asking the musicians to " play this tune as though you 've never heard it before . "
In his final years , Elgar experienced a musical revival .
The BBC organised a festival of his works to celebrate his seventy-fifth birthday , in 1932 .
He was sought out by younger musicians such as Adrian Boult , Malcolm Sargent and John Barbirolli , who championed his music when it was out of fashion .
The continental composers who most influenced Elgar were Handel , Dvořák and , to some degree , Brahms .
Elgar began composing when a still a child , and all his life he drew on his early sketchbooks for themes and inspiration .
His early adult works included violin and piano pieces , music for the wind quintet in which he and his brother played between 1878 -- 81 , and music of many types for the Powick Asylum band .
In this period and later , Elgar wrote songs and partsongs .
Elgar 's best-known works were composed within the twenty-one years between 1899 and 1920 .
Reed wrote , " Elgar 's genius rose to its greatest height in his orchestral works " and quoted the composer as saying that , even in his oratorios , the orchestral part is the most important .
The Enigma Variations made Elgar 's name nationally .
Elgar wrote it after setting aside an early attempt to compose a symphony .
The work reveals his continuing progress in writing sustained themes and orchestral lines , although some critics find that in the middle part " Elgar 's inspiration burns at less than its brightest . "
This work is based , unlike much of Elgar 's earlier writing , not on a profusion of themes but on only three .
Gustav Mahler 's Seventh Symphony , composed at the same time , runs for well over an hour .
The Violin Concerto , composed in 1909 as Elgar reached the height of his popularity , and written for the instrument dearest to his heart , is lyrical throughout and rhapsodical and brilliant by turns .
Between the two concertos came Elgar 's symphonic study Falstaff , which has divided opinion even among Elgar 's strongest admirers .
Elgar himself thought Falstaff the highest point of his purely orchestral work .
The major works for voices and orchestra of the twenty-one years of Elgar 's middle period are three oratorios , The Dream of Gerontius ( 1900 ) , The Apostles ( 1903 ) and The Kingdom ( 1906 ) ; and two odes , the Coronation Ode ( 1902 ) and The Music Makers ( 1912 ) .
The first of the odes , as a pièce d'occasion has rarely been revived after its initial success , with the culminating " Land of Hope and Glory " .
The oratorios were all successful , although the first , Gerontius , was and remains the best-loved and most performed .
On the manuscript Elgar wrote , quoting John Ruskin , " This is the best of me ; for the rest , I ate , and drank , and slept , loved and hated , like another .
Elgar 's other works of his middle period include incidental music for Grania and Diarmid , a play by George Moore and W. B. Yeats ( 1901 ) , and for The Starlight Express , a play based on a story by Algernon Blackwood ( 1916 ) .
Of the former , Yeats called Elgar 's music " wonderful in its heroic melancholy " .
After the Cello Concerto , Elgar completed no more large-scale works .
For most of the rest of the twentieth century , it was generally agreed that Elgar 's creative impulse ceased after his wife 's death .
Elgar left the opening of the symphony complete in full score , and those pages , along with others , show Elgar 's orchestration changed markedly from the richness of his pre-war work .
The Gramophone described the opening of the new work as something " thrilling … unforgettably gaunt " .
By contrast , the critic W. J. Turner , in the mid-twentieth century , wrote of Elgar 's " Salvation Army symphonies . "
Elgar 's immense popularity was not long-lived .
Henry Wood and younger conductors such as Boult , Sargent and Barbirolli championed Elgar 's music , but in the recording catalogues and the concert programmes of the middle of the century , his works were not well-represented .
This point about Elgar 's transmuting his influences had been touched on before .
Among Elgar 's admirers there is disagreement about which of his works are to be regarded as masterpieces .
The Enigma Variations is generally among them .
Many rate the Violin Concerto equally highly , but some do not .
Falstaff also divides opinion .
In a Musical Times 1957 centenary symposium on Elgar led by Ralph Vaughan Williams , by contrast , several contributors share Eric Blom 's view that Falstaff is the greatest of all Elgar 's works .
However , in the 1957 centenary symposium , several leading admirers of Elgar express reservations about one or both symphonies .
Despite the fluctuating critical assessment of the various works over the years , Elgar 's major works taken as a whole have in the twenty-first century recovered strongly from their neglect in the 1950s .
Similarly in the concert hall , Elgar 's works , after a period of neglect are once again frequently programmed .
The house in Lower Broadheath where Elgar was born is now the Elgar Birthplace Museum , devoted to his life and work .
The Elgar Society dedicated to the composer and his works was formed in 1951 .
It features Elgar with his bicycle .
The change generated controversy , particularly because 2007 was the 150th anniversary of Elgar 's birth .
From 2007 the Elgar notes were phased out , ceasing to be legal tender on 30 June 2010 .
Elgar had two locomotives named in his honour .
On 25 February 1984 , this locomotive was officially named ' Sir Edward Elgar ' at Paddington Station by Simon Rattle , then conductor of the City of Birmingham Symphony Orchestra .
Elgar 's life and music have inspired works of literature including the novel Gerontius and several plays .
Perhaps the best-known work depicting Elgar , is Ken Russell 's 1962 BBC television film Elgar , made when the composer was still largely out of fashion .
This hour-long film contradicted the view of Elgar as a jingoistic and bombastic composer , and evoked the more pastoral and melancholy side of his character and music .
