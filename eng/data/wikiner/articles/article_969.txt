Early releases of AutoCAD used primitive entities -- lines , polylines , circles , arcs , and text -- to construct more complex objects .
AutoCAD 2010 introduced parametric functionality and mesh modeling .
Currently , AutoCAD only runs under Microsoft Windows operating systems .
Versions for Unix and Mac OS were released in the 1980s and 1990s , but these were later dropped .
The AutoCAD command set is localized as a part of the software localization .
The C version was , at the time , one of the most complex program in that language to date .
Autodesk even had to work with the compiler developer to fix certain limitations to get AutoCAD to run .
In addition to being sold directly by Autodesk , it can also be purchased at computer stores , unlike the full version of AutoCAD which must be purchased from official Autodesk dealers .
AutoCAD is licensed at a significant discount over commercial retail pricing to qualifying students and teachers , with both a 14 month and perpetual license available .
The student version of AutoCAD is functionally identical to the full commercial version , with one exception : DWG files created or edited by a student version have an internal bit-flag set ( the " educational flag " ) .
When such a DWG file is printed by any version of AutoCAD ( commercial or student ) , the output will include a plot stamp / banner on all four sides .
The Autodesk student community provides registered students with free access to different Autodesk applications .
Autodesk has also developed a few vertical programs , for discipline-specific enhancements .
AutoCAD 's native file format , DWG , and to a lesser extent , its interchange file format , DXF , have become de facto standards for CAD data interoperability .
In 2006 , Autodesk estimated the number of active DWG files to be in excess of one billion .
In the past , Autodesk has estimated the total number of DWG files in existence to be more than three billion .
