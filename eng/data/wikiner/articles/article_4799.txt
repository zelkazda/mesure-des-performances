King Kong vs. Godzilla is a 1962 tokusatsu kaiju film directed by Ishiro Honda with visual effects by Eiji Tsuburaya .
Unfortunately , this is the same iceberg that the mutant dinosaur , Godzilla was trapped in by the Japanese Self-Defense Forces back in 1955 , and the submarine is destroyed by the monster .
The base itself , is ineffective against Godzilla .
Unfortunately , during all this , Kong wakes up from his drunken state and breaks free from the raft .
Kong throws some large rocks at Godzilla , but Godzilla shoots his atomic breath at Kong 's chest , forcing the giant ape to retreat .
In a fielded area outside the city , they dig a large pit laden with explosives and try to lure Godzilla into it .
They succeed and set off the explosives , but Godzilla is unharmed and crawls out of the pit .
The JSDF then decide to transport Kong via balloons to Godzilla , in hope that they will fight each other to their deaths .
Godzilla has the advantage at first , eventually knocking Kong down with a vicious drop kick , and battering the gorilla unconscious with powerful tail attacks to his forehead .
When Godzilla tries to kill Kong with his atomic breath , an electrical storm arrives and revives Kong , giving him the power of an electric grasp .
The two begin to fight again , with the revitalized Kong swinging Godzilla around by his tail , shoving a tree into Godzilla 's mouth , and judo tossing him over his shoulder .
The script is special to me ; it makes me emotional because it was King Kong that got me interested in the world of special photographic techniques when I saw it in 1933 . "
This approach was not favoured by most of the effects crew , who " could n't believe " some of the things Tsuburaya asked them to do , such as Kong and Godzilla volleying a giant boulder back and forth .
But Tsuburaya wanted to appeal to children 's sensibilities and broaden the genre 's audience .
This approach was favoured by Toho and to this end , King Kong vs. Godzilla has a much lighter tone than the previous two Godzilla films and contains a great deal of humor within the action sequences .
Ishiro Honda was not a fan of the " dumbing down " of the monsters .
The bulk of the film was shot on Oshima instead .
During pre-production , Ishiro Honda had toyed with the idea of using Willis O'Brien 's stop motion technique instead of the suitmation process used in the first two Godzilla films , but budgetary concerns prevented him from using the process except in a few , isolated scenes .
A brand new Godzilla suit was designed for this film and some slight alterations were done to his overall appearance .
These new features gave Godzilla a more reptilian/dinosaurian appearance .
Another puppet ( from the waist up ) was also designed that had a nozzle in the mouth to spray out liquid mist simulating Godzilla 's fire breath .
Finally a separate prop of Godzilla 's tail was also built for closeup practical shots when his tail would be used ( such as the scene where Godzilla trips Kong with his tail ) .
The first suit was rejected for being too fat with long legs giving Kong an almost cute look .
A few other designs were done before Tsuburaya would approve the final look that was ultimately used in the film .
Besides the suit with the two separate arm attachments , a meter high model and a puppet of Kong ( used for closeups ) were also built .
The fourth became special effects director Eiji Tsuburaya 's dinner .
Due to this film 's great box office success , Toho announced plans to do a sequel almost immediately .
Also due to the great box office success of this film , Toho was convinced to build a franchise around the character of Godzilla and started producing sequels on a yearly basis .
The next project was to pit Godzilla against another famous movie monster icon : a giant version of the Frankenstein monster .
Ultimately , Toho rejected the script and the next year pitted Mothra against Godzilla instead , in the 1964 film Mothra vs. Godzilla .
They worked with the character again in 1967 though , when they helped Rankin/Bass co produce their film King Kong Escapes .
The creature did reappear at the beginning of the films sequel War of the Gargantuas this time being retained in the finished film .
Even though it was only featured in this one film ( although it was used for a couple of brief shots in Mothra vs. Godzilla ) , this Godzilla suit was always one of the more popular designs among fans from both sides of the Pacific .
Scenes of the giant octopus attack were reused in black and white for episode 23 of the television show Ultra Q .
Mechani-Kong was replaced by Mechagodzilla , and the project eventually evolved into Godzilla vs. Mechagodzilla II in 1993 .
The film was referenced in Da Lench Mob 's 1992 single " Guerillas in tha Mist " .
The article was reprinted in various issues of Famous Monsters of Filmland in the years following such as issues 51 , and 114 .
" , and states that the correct answer is " Godzilla " .
As well , through the years , this myth has been misreported by various members of the media , and has been misreported by reputable news organizations such as The LA Times .
Kong and Godzilla crash into the ocean , and Kong is the only monster to emerge and swims home .
