American Airlines was listed at # 120 on the Fortune 500 list of companies .
In May 2008 , American served 260 cities ( excluding codeshares with partner airlines ) with 655 aircraft .
American has four hubs : Dallas/Fort Worth ( DFW ) , Chicago ( ORD ) , Miami ( MIA ) , and N.Y. ( JFK ) .
Dallas/Fort Worth is the airline 's largest hub , with AA operating 85 percent of flights at the airport and traveling to more destinations than from its other hubs .
Los Angeles ( LAX ) , San Juan ( SJU ) , and Boston serve as focus cities and international gateways .
American currently operates maintenance bases at Tulsa , Kansas City , and Fort Worth Alliance ( AFW ) , but American has announced that the Kansas City base will close in September 2010 .
American Airlines has two regional affiliates :
American Airlines is a founding member of the Oneworld airline alliance .
American Airlines was developed from a conglomeration of 82 small airlines through acquisitions and reorganizations : initially , American Airways was a common brand by a number of independent carriers .
On January 25 , 1930 , American Airways was incorporated as a single company , based in N.Y. , with routes from Boston , N.Y. and Chicago to Dallas , and from Dallas to L.A. .
The airline operated wood and fabric-covered Fokker Trimotors and all-metal Ford Trimotors .
In 1934 American began flying Curtiss Condor biplanes with sleeping berths .
American Airlines provided advertising and free usage of its aircraft in the 1951 film Three Guys Named Mike .
American Airlines introduced the first transcontinental jet service using Boeing 707s on January 25 , 1959 .
American invested $ 440 million in jet aircraft up to 1962 , launched the first electronic booking system with IBM , and built an upgraded terminal at Idlewild ( now JFK ) Airport in New York City which became the airline 's largest base .
In the 1960s , Mattel released a series of American Airlines stewardess Barbie dolls , signifying their growing commercial success .
By September 1970 , American Airlines was offering its first long haul international flights from St. Louis to Honolulu and onto Sydney and Auckland via American Samoa and Nadi .
The freighter featured the then-new " AA " logo on the hull , along with the crew uniforms and several set pieces .
From 1971 -- 1978 Beverly Lynn Burns worked as a stewardess for AA .
She went on to become the first woman Boeing 747 airline captain when , on the afternoon of July 18 , 1984 , she commanded People Express flight # 17 ( aircraft 604 ) departing Newark International Airport at 3:30pm to Los Angeles International Airport .
In a prearranged effort , this honor was shared with another female People Express captain Lynn Rippelmeyer , who flew flight # 2 from Newark to London Gatwick at 7:35pm that same day .
After moving headquarters to Fort Worth in 1979 , American changed its routing to a hub-and-spoke system in 1981 , opening its first hubs at DFW and Chicago O'Hare .
In the late 1980s , American opened three hubs for north-south traffic .
San Jose International Airport was added after American purchased AirCal .
American also built a terminal and runway at Raleigh-Durham International Airport for the growing Research Triangle Park nearby and compete with USAir 's hub in Charlotte .
Nashville was also a hub .
In 1988 , American Airlines received its first Airbus A300B4-605R aircraft .
In 1990 , American Airlines bought the assets of TWA 's operations at London Heathrow for $ 445 million , giving American a hub there .
American discontinued most of Reno Air 's routes , and sold most of the Reno Air aircraft , as they had with Air California 12 years earlier .
During this time , concern over airline bankruptcies and falling stock prices brought a warning from American 's CEO Robert Crandall .
" I 've never invested in any airline " , Crandall said .
And I always said to the employees of American , ' This is not an appropriate investment .
Crandall noted that since airline deregulation of the 1970s , 150 airlines had gone out of business .
On October 15 , 1998 American Airlines became the first airline to offer electronic ticketing in the 44 countries it serves .
Robert Crandall left in 1998 and was replaced by Donald J. Carty , who negotiated the purchase of the near bankrupt Trans World Airlines ( it would file for its 3rd bankruptcy as part of the purchase agreement ) and its hub in St. Louis in April 2001 .
In the merger , 60 percent of former TWA pilots moved to the bottom of the seniority list at AA .
The senior TWA captains were integrated at the same seniority level as AA captains hired years later .
The junior TWA pilots were mostly furloughed .
The extensive furloughs of former TWA pilots in the wake of the 9/11 attacks disproportionately affected St. Louis and resulted in a significant influx of American Airlines pilots into this base .
For cabin crews , all former TWA flight attendants ( approximately 4,200 ) were furloughed by mid-2003 due to the AA flight attendants ' union putting TWA flight attendants at the bottom of their seniority list .
American Airlines began losing money in the wake of the TWA merger and the September 11 , 2001 attacks ( in which two of its planes were involved ) .
Carty negotiated wage and benefit agreements with the unions but resigned after union leaders discovered he was planning to award executive compensation packages at the same time .
This undermined AA 's attempts to increase trust with its workforce and to increase its productivity .
The St. Louis hub was also downsized .
On July 20 , 2005 , American announced a quarterly profit for the first time in 17 quarters ; the airline earned $ 58 million in the second quarter of 2005 .
This furlough is in addition to the furlough of 20 MD-80 aircraft .
On June 26 , 2009 , rumors of a merger with US Airways resurfaced to much speculation within the online aviation community .
In August 2009 , American was placed under credit watch , along with United Airlines and US Airways .
On October 28 , 2009 , American notified its employees that it would close its Kansas City maintenance base in September 2010 , and would also close or make cutbacks at five smaller maintenance stations , resulting in the loss of up to 700 jobs .
American Airlines canceled 1,000 flights to inspect wire bundles over three days in April 2008 to make sure they complied with government safety regulations .
In May 2008 , a month after mass grounding of aircraft , American announced capacity cuts and fees to increase revenue and help cover high fuel prices .
On September 12 , 2009 , American Airlines ' parent company , AMR Corporation announced that they were looking into buying some of the financially struggling Japan Airlines .
AMR is not the only company planning to buy a stake in the airline : rival Delta Air Lines is also looking into investing in the troubled airline , along with Delta 's partner Air France-KLM .
Japan Airlines called off negotiations of the possible deal with all airlines on October 5 , 2009 .
On November 18 , 2009 , Delta , with help from TPG , made a bid of $ 1 billion for JAL to partner with them .
On February 9 , 2010 , Japan Airlines officially announced that it will strengthen its relationship with American Airlines and Oneworld .
Next , in February 2010 , the USDOT granted AA preliminary antitrust immunity to allow the airline to work with British Airways , Iberia Airlines , Finnair and Royal Jordanian Airlines on transatlantic routes .
The partnership was officially approved by the USDOT on July 20 , 2010 .
In 1978 American announced that it would move its headquarters to a site at Dallas/Fort Worth International Airport in 1979 .
Mayor of New York City Ed Koch described this move as " betrayal " of New York City .
The airline finished moving into a $ 150 million ( 1983 dollars ) , 550000-square-foot ( 51000 m 2 ) facility in Fort Worth on January 17 , 1983 ; $ 147 million in Dallas/Fort Worth International Airport bonds financed the headquarters .
The union was created in 1963 after the pilots disposed of the ALPA union .
In 1967 , Massimo Vignelli designed the famous AA logo .
Thirty years later , in 1997 , American Airlines was able to make its logo internet-compatible by buying the domain AA.com .
The original AA logo is still in use today , being " one of the few logos that simply needs no change " .
American Airline 's wastewater treatment plant recycles water used at the base of the wash aircraft , process rinse water tanks , and irrigates landscape .
In addition to that , American Airlines has also won the award for the reduction of hazardous waste that saved them $ 229,000 after a $ 2,000 investment .
The eagle became a symbol of the company and inspired the name of American Eagle Airlines .
In the late 1960s , American commissioned an industrial designer to develop a new livery .
The original design called for a red , white , and blue stripe on the fuselage , and a simple " AA " logo , without an eagle , on the tail .
In 1999 , American painted a new Boeing 757 in its 1959 international orange livery .
One Boeing 777 and one Boeing 757 are painted in standard livery with a pink ribbon on the sides and on the tail , in support for the Susan G. Komen for the Cure .
American is the only major U.S. airline that leaves the majority of its aircraft surfaces unpainted .
This was because C. R. Smith hated painted aircraft , and refused to use any liveries that involved painting the entire plane .
By the early 1980s , however , NASA decided to discontinue using the American livery and replaced it with its own livery , consisting of a white fuselage and blue pinstriping .
American Airlines Vacations is a member of the International Air Transport Association ( IATA ) .
American Airlines serves four continents .
( Continental Airlines serves five , while Delta Air Lines and United Airlines both serve six . )
Lambert-St. Louis International Airport has served as a regional as well for several years .
American serves the second largest number of international destinations , second to Continental Airlines .
American is the only U.S. airline with scheduled flights to Anguilla , Bolivia , Dominica , Grenada , Saint Vincent and the Grenadines , and Uruguay .
American also launched non-stop service from Chicago to Nagoya-Centrair , but that too ended within a year .
Also in 2005 , American launched service from Chicago to Delhi .
In April 2006 , American began service from Chicago to Shanghai , also profitably .
American has codeshare agreements with the following carriers .
An asterisk indicates that the designated airline is a member of the Oneworld alliance .
On Tuesday March 30 , 2010 , rumors surfaced in online aviaton forums over a interline agreement between JetBlue Airways and American Airlines .
Eighteen of JetBlue 's destinations that are not served by American and twelve of American 's international destinations from John F. Kennedy International Airport are included in the agreement .
Also , American is giving JetBlue 16 slots at Ronald Reagan Washington National Airport for 8 round trips and 2 at Westchester County Airport .
In return , JetBlue is giving American 12 slots or 6 round trips at JFK Airport .
As of March 2010 , the American Airlines fleet consists of 621 aircraft .
On August 20 , 2008 , American Airlines became the first to offer full inflight internet service .
In October 2008 , American announced plans to order the Boeing 787-9 Dreamliner .
American is the largest operator of the McDonnell Douglas MD-80 , with some 255 of the type .
American Airlines has stated that they have MD-80 leases running until as late as 2024 .
In August 2009 , American officially retired its fleet of Airbus A300 aircraft , after 21 years of service .
American has not made plans to replace this fleet .
American Airlines had an average fleet age of 15.3 years in August 2009 .
In February 2010 American Airlines quietly announced that it would eliminate free blankets in coach and sell an $ 8 packet that includes a pillow and blanket starting May 1 .
They also receive similar privileges from AA 's partner airlines , particularly those in Oneworld .
Increased competition following the 1978 Airline Deregulation Act prompted airline marketing professionals to develop ways to reward repeat customers and build brand loyalty .
The first idea at American , a special " loyalty fare " , was modified and expanded to offer free first class tickets and upgrades to first class for companions , or discounted coach tickets .
The name was selected by AA 's advertising agency , and is consistent with other American Airlines programs featuring " AA " in the name and logo .
The logo was designed by Massimo Vignelli .
In addition to its Oneworld , American Connection , and American Eagle partnerships , American Airlines offers frequent flier partnerships with the following airlines and railways :
During the airport 's construction , New York Mayor Fiorello LaGuardia had an upper-level lounge set aside for press conferences and business meetings .
At one such press conference , he noted that the entire terminal was being offered for lease to airline tenants ; after a reporter asked whether the lounge would be leased as well , LaGuardia replied that it would , and a vice president of AA immediately offered to lease the premises .
Additionally , complimentary Lenovo computer terminals with free internet access , complimentary T-Mobile hotspot access , and complimentary printing is available at most locations , as are shower facilities .
