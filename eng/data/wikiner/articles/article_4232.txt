Wood 's popularity waned soon after his biggest " name " star , Béla Lugosi , died .
He was able to salvage a saleable feature from Lugosi 's last moments on film , but his career declined thereafter .
Tim Burton 's biopic of the director 's life , Ed Wood , earned two Academy Awards .
Wood enlisted in the Marines at age 17 , just months after the Attack on Pearl Harbor .
He served from 1942 -- 1946 and claimed that he had participated in the Battle of Guadalcanal [ citation needed ] while secretly wearing a brassiere and panties beneath his uniform .
He was a womanizer in his younger days , but in later life he was faithful to his girlfriends ( most notably Dolores Fuller ) and wife .
Wood 's film career began after moving to Hollywood in 1947 .
Glen or Glenda , shot in just four days for $ 26,000 , was done in a semi-documentary style .
Béla Lugosi , who was not told the film was about a transvestite , was paid $ 1,000 in cash for one day of filming .
Producer George Weiss added to the confusion by inserting footage of flagellation and bondage , reminiscent of the fetish films of Irving Klaw , from another production .
Wood also co-wrote the screenplay with writer-producer friend Alex Gordon .
Lugosi was originally cast as the father of the lead character , but dropped out due to illness .
In 1955 , he produced and directed his first horror film , Bride of the Monster ( originally titled Bride of the Atom ) .
Béla Lugosi , in his last speaking role , stars as a mad scientist bent on creating an army of atomic supermen .
The immense , 400-pound Tor Johnson plays Lobo , his lumbering henchman .
The style and content of the film is highly reminiscent of the low-budget horror movies Lugosi made for Monogram Pictures in the 1940s , particularly The Corpse Vanishes .
Wood 's script even recreates a laboratory scene from that film ( Lugosi 's mad scientist flogging his henchman ) with the same fake , painted stone wall backdrop .
In the finale , the frail , elderly Lugosi was reduced to thrashing about in the mud with a large rubber octopus when the motor needed to turn it into a flailing beast could not be located .
The story was mainly a rehash of Bride of the Monster in a western setting .
Lugosi , recently out of rehab for morphine addiction , was to star as the undertaker/mad scientist .
Gene Autry and Lon Chaney , Jr. were also attached to the project for a time .
Plan 9 from Outer Space incorporated the final Lugosi scenes into a new story that combined horror and science fiction .
Wood 's chiropractor , his face hidden behind a cape , doubled for Lugosi in several scenes .
Tor Johnson and wasp-waisted Vampira ( Maila Nurmi ) are memorable , even iconic , as zombies risen from the grave by alien invaders .
There are frequent references to the mad scientist ( Lugosi ) and monster from the previous film , and Tor Johnson reprises his Lobo role -- his face now half-destroyed from the fire .
( Tim Burton 's Ed Wood bio-pic opens with a faithful recreation of this scene ) .
Criswell also plays a character role as one of the vengeful ghosts seen at the climax of the film .
The finale , with the ghouls reduced to skeletons and Criswell 's epilogue , were used again in 1965 for Orgy of the Dead .
Wood 's transitional film , once again combining two genres , horror and grindhouse skin-flick , was Orgy of the Dead ( 1965 ) .
Wood 's 1971 film Necromania was believed lost for years until an edited version resurfaced at a yard sale in 1992 , followed by a complete unedited print in 2001 .
But mostly the book concerns the pitfalls of Hollywood , and has some advice from Ed Wood Jr. , for anyone who is setting their sights on making it in Hollywood .
Wood does not candy coat the experience that a first timer in Hollywood will shockingly discover .
His primary film work in the 1970s was working with friend Stephen C. Apostolof , usually cowriting scripts , but also serving as an assistant director and associate producer .
Outside of a brief New York theatrical engagement , the film did not receive a commercial release in the United States , and was only available on video in Germany due to contractual difficulties .
Three of his films ( Bride of the Monster , The Violent Years , and The Sinister Urge ) have been featured on the television series Mystery Science Theater 3000 , which has given those works wider exposure .
Producers considered including Plan 9 , but found it had too much dialogue for the show 's format .
Series head writer and host Michael J. Nelson would go on to do an audio commentary for a 2005/2006 DVD release of the film , which was newly colorized .
The cult status of Ed Wood 's movies is also represented in the music industry .
Horror film director and heavy metal musician Rob Zombie titled his 2001 album The Sinister Urge after Wood 's film .
Wood was played by Johnny Depp and Lugosi by Martin Landau , who won an Academy Award for Best Supporting Actor .
The film also won an Academy Award for Best Makeup for Rick Baker .
Eager to embrace Burton , Disney accepted the project , monochrome and all .
