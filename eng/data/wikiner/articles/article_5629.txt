In historical terms , Hastings can claim fame through its connection with the Norman conquest of England ; and also because it became one of the medieval Cinque Ports .
The attraction of Hastings as a tourist destination continues ; although the numbers of hotels has decreased , it caters for wider tastes , being home to internationally-based cultural and sporting events , such as chess and running .
At this time they began to exploit the iron ( Wealden rocks provide a plentiful supply of the ore ) , and so the port was useful to them .
With the departure of the Romans the town suffered setbacks .
The original Roman port could now well be under the sea .
Bulverhythe , where its original site is conjectured , suggests that :-hythe or hithe means a port or small haven .
As a borough , Hastings had a corporation consisting of a " bailiff , jurats , and commonalty " .
As a port , Hastings ' days were finished .
Hastings had suffered over the years from the lack of a natural harbour , and there have been attempts to create a sheltered harbour .
Attempts were made to build a stone harbour during the reign of Elizabeth I , but the foundations were destroyed by the sea in terrible storms .
Hastings was now a small fishing settlement , but it was soon discovered that the new taxes on luxury goods could be made profitable by smuggling , and the town was ideally located for that .
Once that move away from the old town had begun , it led to the further expansion along the coast , eventually linking up with the new St Leonards .
Seaside resorts were starting to go out of fashion : Hastings perhaps more than most .
Hastings Borough Council is now in the second tier of local government , below East Sussex County Council .
The most notable suburbs of Hastings are Ore , St Leonards on Sea , Silverhill , Bulverhythe , and Hollington .
Beyond Bulverhythe , the western end of Hastings is marked by low-lying land known as Glyne Gap , separating it from Bexhill-on-Sea .
Combe Haven is another site of biological interest , with alluvial meadows , and the largest reed bed in the county , providing habitat for breeding birds .
It is in the West St Leonards ward , stretching into the parish of Crowhurst .
85 % of the firms ( in 2005 ) employed fewer than 10 people ; as a consequence the unemployment rate was 3.3 % ( cf. East Sussex 1.7 % ) ; and almost one-third of the employable population had no skills at all in 2001 .
From being the third tourist resort in the country 50 years ago , Hastings has still not been able to shake off its over-reliance on tourism .
Until the development of tourism , fishing was Hastings ' major industry .
The fleet has been based on the same beach , below the cliffs at Hastings , for at least 400 , possibly 600 , years .
There are two major roads in Hastings : the A21 trunk road to London ; and the A259 coastal road .
Both are beset with traffic problems : although the London road , which has to contend with difficult terrain , has had several sections of widening over the past decades there are still many delays .
Long-term plans for a much improved A259 east -- west route ( including a Hastings bypass ) were abandoned in the 1990s , but a new road to Bexhill-on-Sea is planned to relieve the congested coastal route .
Hastings is also linked to Battle via the A2100 , the original London road .
The A28 road connects Hastings to Ashford , Canterbury and the Isle of Thanet .
The A27 road starts nearby at Pevensey .
The Ring road includes parts of most of the main roads .
The town is served by Stagecoach buses on routes that serve the town ; and also extend to Bexhill , Eastbourne and Dover .
National Express Coaches run service 538 to London .
Hastings has four rail links : two to London , one to Brighton and one to Ashford .
Trains to Brighton also use the East Coastway Line .
The Marshlink Line runs via Rye to Ashford where a connection can be made with Eurostar services , and is unelectrified except for the Hastings-Ore segment .
Hastings is served by two rail companies : Southeastern and Southern .
Southeastern services run along the Hastings Line , generally terminating at Hastings , with some peak services extending to Ore ; the other lines are served by Southern , with services terminating at Ore or Ashford .
West Marina station ( on the LBSCR line ) was very near West St Leonards and was closed some years ago .
A new station has been proposed at Glyne Gap in Bexhill , which would also serve residents from western Hastings .
A high speed railway from Bexhill to Ore has been proposed .
There are two funicular railways , known locally as the West Hill and East Hill Lifts respectively .
Hastings had a network of trams from 1905 to 1929 .
In a similar vein , the old town of Hastings is certainly a landmark .
It is now the only all-boys secondary school in East Sussex .
The proposed sponsors for the academies are University of Brighton ( lead sponsor ) , British Telecom and East Sussex County Council itself .
The University Centre Hastings is an institute of higher education located in the town , with courses validated by the University of Brighton .
Among other uses to which the main theatre is put is to host the annual Hastings Musical Festival .
The Hastings International Chess Congress which started in 1882 attracts international players to Hastings .
The Hastings Writers ' Group claims to be one of the oldest in the country : it was established in 1947 .
Hastings has long been known as a retreat for artists and painters .
On or around the beginning of March each year , there is the annual Hastings Musical Festival .
Another family pool ( although outside the borough ) with wave machine and water slide is situated at Glyne Gap , on the coast mid-way between Bexhill and Hastings .
As for team sports , Hastings is home to one senior football club , Hastings United , who play in the Isthmian League Premier Division and use The Pilot Field as their home ground .
The town 's premier cricket venue is now Horntye Park Sports Complex , home of Hastings Priory .
The previous venue , where Priory Meadow Shopping Centre now stands , saw the final game played in 1989 .
The Hastings Open Bowls Tournament has been held annually in June since 1911 and attracts many entrants country-wide .
