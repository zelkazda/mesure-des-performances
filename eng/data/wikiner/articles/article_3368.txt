It has gained its modern-day function through the innovations of Gotthold Ephraim Lessing , a playwright and theatre practitioner who worked in Germany in the 18th century .
In the United Kingdom , dramaturgs function similarly although they are more often , themselves , also playwrights .
In the USA , where this position was until recently relatively uncommon , it has enjoyed a recent growth , particularly in theater companies that focus on developing new plays and those that produce plays where the socio-historical background is important to the understanding of the production .
