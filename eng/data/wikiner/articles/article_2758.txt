Around three quarters of world cacao bean production takes place in West Africa .
However , as William Bright noted the word " chocolatl " does n't occur in central Mexican colonial sources making this an unlikely derivation .
In 1689 , noted physician and collector Hans Sloane developed a milk chocolate drink in Jamaica which was initially used by apothecaries , but later sold to the Cadbury brothers in 1897 .
Roughly two-thirds of the entire world 's cocoa is produced in Western Africa , with 43 % sourced from Côte d'Ivoire , where child labor is a common practice to obtain the product .
There is some dispute about the genetic purity of cocoas sold today as Criollo , as most populations have been exposed to the genetic influence of other varieties .
The flavor of Criollo is described as delicate yet complex , low in classic chocolate flavor , but rich in " secondary " notes of long duration .
The most commonly grown bean is forastero , a large group of wild and cultivated cacaos , most likely native to the Amazon basin .
They are significantly hardier and of higher yield than Criollo .
Currently , the U.S. Food and Drug Administration ( FDA ) does not allow a product to be referred to as " chocolate " if the product contains any of these ingredients .
A study reported by the BBC indicated that melting chocolate in one 's mouth produced an increase in brain activity and heart rate that was more intense than that associated with passionate kissing , and also lasted four times as long after the activity had ended .
In the United States , some large chocolate manufacturers lobbied the federal government to permit confection containing cheaper hydrogenated vegetable oil in place of cocoa butter to be sold as " chocolate " .
Both The Hershey Company and Mars have become the largest manufacturers in the world .
Other significant players include Cadbury , Nestlé , Kraft Foods and Lindt .
However , Europe accounts for 45 % of the worlds chocolate revenue .
Chocolate slavery today is widespread in West African countries such as Mali , the Ivory Coast , Cameroon , Ghana , and Nigeria .
In Mali , children are bought and sold for $ 30 in U.S. currency from poor areas .
On Valentine 's Day , a box of chocolate is traditional , usually with flowers and a greeting card .
It may be gifted on other holidays , including Christmas , Easter , Thanksgiving , and birthdays .
At Easter , chocolate eggs are traditional .
In 1964 , Roald Dahl published a children 's novel titled Charlie and the Chocolate Factory .
The novel centers around a poor boy named Charlie Bucket who takes a tour through the greatest chocolate factory in the world , owned by Willy Wonka .
The first was Willy Wonka & the Chocolate Factory , a 1971 film which later became a cult classic .
Thirty-four years later , a second film adaptation was produced , titled Charlie and the Chocolate Factory .
Like Water for Chocolate ( Como agua para chocolate ) is a 1989 love story by novelist Laura Esquivel that was adapted to film in 1992 .
The plot incorporates magical realism with Mexican cuisine and the title is a double entendre in its native language , referring both to a recipe for hot chocolate and to an idiom that is a metaphor for sexual arousal .
The film earned 11 Ariel Awards from the Academia Mexicana de Artes y Ciencias Cinematográficas including Best Picture .
Chocolat is a 1999 novel by Joanne Harris .
