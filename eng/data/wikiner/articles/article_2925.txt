In folk music , early examples included Woody Guthrie 's 1940 debut album Dust Bowl Ballads and Merle Travis 's 1947 box set Folk Songs of the Hills , in which each song is introduced by a short narrative .
Frank Sinatra released many thematically programmed albums of the 1950s for Capitol Records starting with the ten-inch 33s Songs for Young Lovers and Swing Easy .
Perhaps the first full Sinatra concept album example is In the Wee Small Hours from 1955 , where the songs -- all ballads -- were specifically recorded for the album , and organized around a central mood of late-night isolation and aching lost love , with the album cover strikingly reinforcing that theme .
Perhaps the first examples from rock were the albums of The Ventures .
However , later in 1966 , Brian Wilson had begun work on the Smile album , which was intended as a narrative .
Frank Zappa and The Mothers of Invention 's Freak Out ! , a sardonic farce about rock music and America as a whole , and The Kinks ' Face to Face , the first collection of Ray Davies 's idiosyncratic character studies of ordinary people are conceptually oriented albums .
With the release of The Beatles album Sgt. Pepper 's Lonely Hearts Club Band in June 1967 , the notion of the concept album came to the forefront of the popular and critical mind .
While debate exists over the extent to which Sgt. Pepper qualifies as a true concept album , its reputation as such helped inspire other artists to produce concept albums of their own , and inspired the public [ citation needed ] to anticipate them .
The Moody Blues ' Days of Future Passed , released the same year as Sgt. Pepper 's , was another foray into the concept album .
The Who Sell Out by The Who followed with its concept of a pirate radio broadcast .
Within the record , joke commercials recorded by the band and actual jingles from recently outlawed pirate radio station Radio London were interspersed between the songs , ranging from pop songs to hard rock and psychedelic rock , culminating with a mini-opera titled " Rael " .
The Who 's rock opera Tommy .
composed by Pete Townshend , was released in April 1969 .
The Who went on to further explorations of the concept album format with their follow-up project Lifehouse , which was abandoned before completion , and with their 1973 rock opera , Quadrophenia .
These were : Lola Versus Powerman and the Moneygoround , Part One ( 1970 ) , Muswell Hillbillies ( 1971 ) , Preservation : Act 1 ( 1973 ) , Preservation : Act 2 ( 1974 ) , Soap Opera ( 1975 ) and Schoolboys in Disgrace ( 1976 ) .
Most notably , Pink Floyd recast itself from its 1960s guise as a psychedelic band into a commercial mega-success with its series of concept albums , beginning with The Dark Side of the Moon in 1973 , then Wish You Were Here from 1975 , Animals from 1977 , 1979 's rock opera The Wall and its lesser-known follow-up The Final Cut in 1983 , with Roger Waters behind the themes and storylines .
The Wall also shares many themes -- both conceptual and melodic -- with bassist Roger Waters first solo album The Pros and Cons of Hitchhiking , the two albums being conceived at roughly the same time .
Thick as a Brick is a concept album by English progressive rock band Jethro Tull , released in 1972 .
Its lyrics are built around a poem written by a fictitious boy , " Gerald Bostock " a.k.a. " Little Milton " , about the trials of growing up .
The formula was successful , and the album reached number one on the charts in the United States .
Yes also put out concept albums during the 70s , most notably Tales from Topographic Oceans , which became a defining album of progressive rock but whose critical backlash led to the genre 's decline in popularity and the rise of punk rock .
Another progressive rock act , Genesis , with Peter Gabriel in the lead , released the concept album The Lamb Lies Down on Broadway in 1974 , a double disc that tells the story of the street punk Rael .
The progressive bands that were still around were still having major successes with concept albums , notably The Alan Parsons Project .
Styx continued to have multiplatinum albums with their 1981 release Paradise Theater and 1983 's Kilroy Was Here ( a science fiction rock opera about a future where moralists imprison rockers ) .
In 1982 A Flock of Seagulls released their concept album A Flock of Seagulls based on a futuristic tale of alien abduction .
American Idiot follows the transformation of a disillusioned suburban teenager ( " Jesus of Suburbia " ) into a darker , city-living persona .
Green Day also created another rock opera in 2009 called 21st Century Breakdown .
Marilyn Manson has released three concept albums .
Songs accredited to Manson deal with subjects like death , alienation , overdose , depression , bitterness and pain , while songs accredited to " Omēga " deal more with debauchery , drug use , sex , indulgence and promiscuity .
In an interview , Manson explained that Mechanical Animals was his way of identifying himself with someone like Jesus Christ , whereas Antichrist Superstar had been his way of identifying with " someone more like Lucifer . "
The power and glory that they 've found proceeds to strip them of their original guiding principles , turning them into fake , hollow , profiteering monsters just like the previous denizens of Holy Wood .
Interestingly , the character of Coma White is paralleled on the Holy Wood album as " Coma Black . "
In more recent years , Manson claims he has been moving away from the concept albums and trying to make albums that are more personal and told from his own perspective and experiences , rather than albums expressed through made-up characters .
His revolution is a bittersweet success in that , while he does eradicate Holy Wood , the void assumes " the rejects " into becoming the mainstream and status quo .
The new-found power and influence quickly erode their original guiding principles and they soon devolve into the same fake , profiteering , and hollow monsters as the denizens of Holy Wood .
My Chemical Romance are known for producing concept albums .
Their 2001 debut album I Brought You My Bullets , You Brought Me Your Love is believed to be about two Bonnie and Clyde-like characters ( known as the Demolition Lovers , who are seen on the cover art for Three Cheers for Sweet Revenge and Life on the Murder Scene ) , whose tale involves vampires ( Vampires Will Never Hurt You ) and murder , before they are gunned down in a desert .
The 2004 album Three Cheers for Sweet Revenge follows this story .
The 2006 rock opera The Black Parade has a story that fans are far more aware of .
As he dies , he is confronted by death in the form of a marching band , the Black Parade .
Another band known for concept albums is Coheed and Cambria .
Since 2002 , each of the band 's five albums have been telling chapters of the story known as The Amory Wars , a science fictional saga conceived by frontman Claudio Sanchez .
The first album , The Second Stage Turbine Blade was released in 2002 , followed by three more records continuing the succession of the story .
Their most recent record , Year of the Black Rainbow , was released in April 2010 and is a prequel record which reveals the origins of characters .
The second album , I-Empire , released the following year , is considered a continuation of that story .
Deltron 3030 , the debut album by hip hop supergroup Deltron 3030 is a concept album set in a dystopian year 3030 .
The Streets ' second album A Grand Do n't Come for Free , released in May 2004 follows the story of a protagonist losing £ 1000 and striving to make up the money .
