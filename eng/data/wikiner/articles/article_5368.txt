Quine held ( on a perhaps simplistic construal ) that there are no beliefs that one ought to hold come what may -- in other words , that all beliefs are rationally revisable .
