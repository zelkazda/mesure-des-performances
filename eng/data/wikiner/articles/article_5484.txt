Gerard recommends it , in addition to its uses in coughs and colds , to ' those that have drunk poison or have been bitten of serpents, ' and it was also administered for ' mad dogge 's biting .
Horehound was introduced to southern Australia in the 19th century as a medicinal herb .
