The first electrical engineer was probably William Gilbert who designed the versorium : a device that detected the presence of statically charged objects .
In 1775 Alessandro Volta 's scientific experimentations devised the electrophorus , a device that produced a static electric charge , and by 1800 Volta developed the voltaic pile , a forerunner of the electric battery .
The Darmstadt University of Technology founded the first chair and the first faculty of electrical engineering worldwide in 1882 .
In 1884 Sir Charles Parsons invented the steam turbine which today generates about 80 percent of the electric power in the world using a variety of heat sources .
In 1887 , Nikola Tesla filed a number of patents related to a competing form of power distribution known as alternating current .
In his classic UHF experiments of 1888 , Heinrich Hertz transmitted ( via a spark-gap transmitter ) and detected radio waves using electrical equipment .
In 1897 , Karl Ferdinand Braun introduced the cathode ray tube as part of an oscilloscope , a crucial enabling technology for electronic television .
John Fleming invented the first radio tube , the diode , in 1904 .
Two years later , Robert von Lieben and Lee De Forest independently developed the amplifier tube , called the triode .
In 1895 , Guglielmo Marconi furthered the art of hertzian wireless methods .
In 1920 Albert Hull developed the magnetron which would eventually lead to the development of the microwave oven in 1946 by Percy Spencer .
In 1941 Konrad Zuse presented the Z3 , the world 's first fully functional and programmable computer .
In 1946 the ENIAC ( Electronic Numerical Integrator and Computer ) of John Presper Eckert and John Mauchly followed , beginning the computing era .
The arithmetic performance of these machines allowed engineers to develop completely new technologies and achieve new objectives , including the Apollo missions and the NASA moon landing .
The invention of the transistor in 1947 by William B. Shockley , John Bardeen and Walter Brattain opened the door for more compact devices and led to the development of the integrated circuit in 1958 by Jack Kilby and independently in 1959 by Robert Noyce .
The Intel 4004 was a 4-bit processor released in 1971 , but in 1973 the Intel 8080 , an 8-bit processor , made the first personal computer , the Altair 8800 , possible .
Professional bodies of note for electrical engineers include the Institute of Electrical and Electronics Engineers ( IEEE ) and the Institution of Engineering and Technology ( IET ) .
The IEEE claims to produce 30 % of the world 's literature in electrical engineering , has over 360,000 members worldwide and holds over 3,000 conferences annually .
The IET publishes 21 journals , has a worldwide membership of over 150,000 , and claims to be the largest professional engineering society in Europe .
