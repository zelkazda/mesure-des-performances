Aristarchus first argued for Homer 's priority , a claim that was generally accepted by later antiquity .
Some scholars have doubted whether Hesiod alone conceived and wrote the poems attributed to him .
As with Homer , legendary traditions have accumulated around Hesiod .
22-35 ) , gave Hesiod the gift of poetic inspiration one day while he tended sheep ( compare the legend of Cædmon ) .
Plutarch assumed this date much too late for a contemporary of Homer , but most Homeric academics would now accept it .
The account of this contest , followed by an allusion to the Trojan War , inspired the later tales of a competition between Hesiod and Homer .
Two different -- yet early -- traditions record the site of Hesiod 's grave .
Of the many works attributed to Hesiod , three survive complete and many more in fragmentary state .
Hesiod 's works , especially Works and Days , are from the view of the small independent farmer , while Homer 's view is from nobility or the rich .
This is rejected by Paul Cartledge as Hesiod advocates a not-forgetting without any attempt at verification .
Scholars have interpreted this work against a background of agrarian crisis in mainland Greece , which inspired a wave of documented colonisations in search of new land .
This work lays out the five Ages of Man , as well as containing advice and wisdom , prescribing a life of honest labour and attacking idleness and unjust judges as well as the practice of usury .
As M.L. West writes , " Both bear the marks of a distinct personality : a surly , conservative countryman , given to reflection , no lover of women or life , who felt the gods ' presence heavy about him . "
A short poem traditionally no longer attributed to Hesiod is The Shield of Heracles .
Several additional poems were sometimes ascribed to Hesiod :
Scholars generally classify all these as later examples of the poetic tradition to which Hesiod belonged , not as the work of Hesiod himself .
of Works and Days :
of Shield of Heracles :
