Little contemporary biographical information on Bodhidharma is extant , and subsequent accounts became layered with legend , but most accounts agree that he was from the southern region of India , born as a prince to a royal family .
The accounts differ on the date of his arrival , with one early account claiming that he arrived during the Liú Sòng Dynasty ( 420 -- 479 ) and later accounts dating his arrival to the Liáng Dynasty ( 502 -- 557 ) .
Bodhidharma was primarily active in the lands of the Northern Wèi Dynasty ( 386 -- 534 ) .
There are two known extant accounts written by contemporaries of Bodhidharma .
Secondly , more detail is provided concerning Bodhidharma 's journeys .
This implies that Bodhidharma had travelled to China by sea , and that he had crossed over the Yangtze River .
Finally , Dàoxuān provides information concerning Bodhidharma 's death .
Bodhidharma is said to have been a disciple of Prajñātāra , thus establishing the latter as the 27th patriarch in India .
After a three-year journey , Bodhidharma reaches China in 527 during the Liang Dynasty .
Bodhidharma 's tomb was then opened , and only a single sandal was found inside .
Moreover , his encounter with the Wei official indicates a date of death no later than 554 , three years before the fall of the last Wei kingdom .
In the first case , it may be confused with another of his rivals , Bodhiruci . "
These are the first mentions in the historical record of what may be a type of meditation being ascribed to Bodhidharma .
From Palembang , he went north into what are now Malaysia and Thailand .
Bodhidharma answered , " None , good deeds done with selfish intent bring no merit . "
The emperor then asked Bodhidharma , " So what is the highest meaning of noble truth ? "
Bodhidharma answered , " There is no noble truth , there is only void . "
The emperor then asked Bodhidharma , " Then , who is standing before me ? "
From then on , the emperor refused to listen to whatever Bodhidharma had to say .
Although Bodhidharma came from India to China to become the first patriarch of China , the emperor refused to recognize him .
Bodhidharma knew that he would face difficulty in the near future , but had the emperor been able to leave the throne and yield it to someone else , he could have avoided his fate of starving to death .
According to the teaching , Emperor Wu 's past life was as a bhikshu .
Supposedly , that monkey was reincarnated into Hou Jing of the Northern Wei Dynasty , who led his soldiers to attack Nanjing .
After Nanjing was taken , the emperor was held in captivity in the palace and was not provided with any food , and was left to starve to death .
Though Bodhidharma wanted to save him and brought forth a compassionate mind toward him , the emperor failed to recognize him , so there was nothing Bodhidharma could do .
Thus , Bodhidharma had no choice but to leave Emperor Wu to die and went into meditation in a cave for nine years .
Instead , Bodhidharma left the presence of the Emperor once Bodhidharma saw that the Emperor was unable to understand .
The Emperor regretted his having let Bodhidharma leave and was going to dispatch a messenger to go and beg Bodhidharma to return .
The biographical tradition is littered with apocryphal tales about Bodhidharma 's life and circumstances .
According to the legend , as his eyelids hit the floor the first tea plants sprang up ; and thereafter tea would provide a stimulant to help keep students of Chán awake during meditation .
The most popular account relates that Bodhidharma was admitted into the Shaolin temple after nine years in the cave and taught there for some time .
While Bodhidharma was born into the warrior caste in India and thus certainly studied and must have been proficient in self-defense , it is unlikely that he contributed to the development of self-defense technique specifically within China .
However , the legend of his education of the monks at Shaolin in techniques for physical conditioning would imply ( if true ) a substantial contribution to Shaolin knowledge that contributed later to their renown for fighting skill .
In one legend , Bodhidharma refused to resume teaching until his would-be student , Dazu Huike , who had kept vigil for weeks in the deep snow outside of the monastery , cut off his own left arm to demonstrate sincerity .
When asked why he was holding his shoe , Bodhidharma answered " You will know when you reach Shaolin monastery " .
