Asia tops the list of casualties due to natural disaster .
Rising frequency , amplitude and number of natural disasters and attendant problem coupled with loss of human lives prompted the General Assembly of the United Nations to proclaim 1990s as the International Decade for Natural Disaster Reduction ( IDNDR ) through a resolution 44/236 of December 22 , 1989 to focus on all issues related to natural disaster reduction .
In spite of IDNDR , there had been a string of major disaster throughout the decade .
Nevertheless , by establishing the rich disaster management related traditions and by spreading public awareness the IDNDR provided required stimulus for disaster reduction .
