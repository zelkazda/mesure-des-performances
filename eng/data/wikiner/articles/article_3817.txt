Einhard ( also Eginhard or Einhart ) ( c. 775 -- March 14 , 840 , in Seligenstadt , Germany ) was a Frankish scholar and courtier .
Einhard was from the eastern German-speaking part of the Frankish Kingdom .
Born into a family of relatively low status , his parents sent him to be educated by the monks of Fulda -- one of the most impressive centres of learning in the Frankish lands -- perhaps due to his small stature ( Einhard referred to himself as a " tiny manlet " ) which restricted his riding and sword-fighting ability , Einhard concentrated his energies towards scholarship and especially to the mastering of Latin .
Despite such humble origins , he was accepted into the hugely wealthy court of Charlemagne around 791 or 792 .
Charlemagne actively sought to amass scholarly men around him and established a royal school led by the Northumbrian scholar Alcuin .
Einhard evidently was a talented builder and construction manager , because Charlemagne put him in charge of the completion of several palace complexes including Aachen and Ingelheim .
Despite the fact that Einhard was on intimate terms with Charlemagne , he never achieved office in his reign .
In 814 , on Charlemagne 's death his son Louis the Pious made Einhard his private secretary .
Einhard retired from court during the time of the disputes between Louis and his sons in the spring of 830 .
Though he was undoubtedly devoted to her , Einhard wrote nothing of his wife until after her death on 13 December 835 , when he wrote to a friend that he was reminded of her loss in ' every day , in every action , in every undertaking , in all the administration of the house and household , in everything needing to be decided upon and sorted out in my religious and earthly responsibilities ' .
Although unsure as to why these saints should choose such a " sinner " as their patron , Einhard nonetheless set about ensuring they continued to receive a resting place fitting of their honour .
It has been contended that in the last decade of his life Einhard 's strong religious beliefs led to him retiring to a monastery .
Charlemagne found them at Seligenstadt and forgave them .
This account is used to explain the name " Seligenstadt " by folk etymology .
The most famous of Einhard 's works was produced at the request of Charlemagne 's son and successor Louis the Pious , his biography of Charlemagne , the Vita Karoli Magni , " The Life of Charlemagne " ( c. 817 -- 836 ) , which provides much direct information about Charlemagne 's life and character .
His work was written as a praise of Charlemagne , whom he regarded as a foster-father and to whom he was a debtor " in life and death " .
The work thus contains an understandable degree of bias , Einhard taking care to exculpate Charlemagne in some matters , not mention others , and to gloss over certain issues which would be of embarrassment to Charlemagne , such as the morality of his daughters .
