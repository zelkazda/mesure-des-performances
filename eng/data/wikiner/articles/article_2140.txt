The following page shows the foreign relations of Croatia from past history , current events , international disputes and foreign support .
The SDP-led government slowly relinquished control over public media companies and did not interfere with freedom of speech and independent media , though it did n't complete the process of making Croatian Radiotelevision independent .
Major Croatian advances in foreign relations during this period have included :
The EU application was the last major international undertaking of the Račan government , which submitted a 7,000-page report in reply to the questionnaire by the European Commission .
The country was finally accepted as EU candidate in July 2004 .
The main issue , the flight of general Gotovina , however , remained unsolved and despite the agreement on an accession negotiation framework , the negotiations did not begin in March 2005 .
This has been the main condition demanded by EU foreign ministers for accession negotiations .
The main objective of the Croatian foreign policy is admittance to the European Union .
It applied in 2003 , and began with accession negotiations in 2005 ( see also : Accession of Croatia to the European Union ) .
Government officials in charge of foreign policy include the Minister of Foreign Affairs and European Integration , currently Gordan Jandroković , and the President of the Republic , currently Ivo Josipović .
Relations with neighbouring states have normalized somewhat since the breakup of Yugoslavia .
Work has begun -- bilaterally and within the Stability Pact for South Eastern Europe since 1999 -- on political and economic cooperation in the region .
The Zagreb -- Bihać -- Split railway line is still closed for major traffic due to this issue .
A river island between the two towns is under Croatian control , but is also claimed by Bosnia .
Although the convention does make it clear that any decision to declare an exclusive economic zone should be made in co-operation with all interested parties , Croatian sources claim that Slovenia 's self-description as a geographically disadvantaged state amounts to an admission that it is a country without access to international waters .
A small number of pockets of land on the right-hand side of the river Dragonja in Istria have remained under Croatian jurisdiction after the river was re-routed after the Second World War .
However , an old Yugoslav People 's Army barracks building on the Croatian part of the border is still occupied by a small number of Slovenian army personnel .
This policy has been in place since late 2004 but excludes the EU countries .
This agreement applies to Montenegro since its independence .
Further south , near Vukovar and near Šarengrad , there are two river islands ( Vukovarska ada and Šarengradska ada ) which have been part of SR Croatia ( during Yugoslavia ) but during the war they came under Serbian control .
Serbia is refusing to return the islands ( and disregards the committee decision ) with the explanation that they are nearer to the Serbian side of the river so they are Serbian .
These islands are now under Serbian police control .
Another problem that arose a couple of years ago deals with the fishing zones in the Adriatic sea .
