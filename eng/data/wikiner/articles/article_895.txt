Abbas II was the great-great-grandson of Muhammad Ali .
He succeeded his father , Tewfik Pasha , as Khedive of Egypt and Sudan .
He then went to school in Lausanne , and from there passed on to the Theresianum in Vienna .
He was still in college in Vienna when he assumed the throne of the Khedivate of Egypt upon the sudden death of his father .
The establishment of a sound system of native justice , the great remission of taxation , the reconquest of Sudan , the inauguration of the substantial irrigation works at Aswan , and the increase of cheap , sound education , each received his formal approval .
Muhammad Abdul Mun'im , the heir-apparent , was born on 20 February 1899 .
Kitchener often complained about " that wicked little Khedive " and wanted to depose him .
Abbas finally accepted the new order of things on 12 May 1931 and abdicated .
