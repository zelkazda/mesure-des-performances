The 15th film in Toho 's Godzilla series , it was directed by Ishiro Honda with special effects by Teruyoshi Nakano .
It is the second film to feature Mechagodzilla and the only film to feature Titanosaurus .
Akira Ifukube provides the music score .
Terror of Mechagodzilla was the last movie in the Showa series of Godzilla movies before The Return of Godzilla began the Heisei series of Godzilla films in 1984 .
As a result , Terror of Mechagodzilla had the lowest attendance figures of all the movies in the series .
In response to the incident , Interpol begins to investigate .
And when the situation gets desperate , Godzilla comes to the rescue .
The film was released to television in late 1978 , this time under the title Terror of Mechagodzilla .
It included an odd prologue about the history of Godzilla , with footage from Monster Zero and Godzilla 's Revenge ( comprising scenes from Godzilla vs. the Sea Monster and Son of Godzilla ) .
