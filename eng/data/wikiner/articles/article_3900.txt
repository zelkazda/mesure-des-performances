In the Roman period , it was for many years the second largest city of the Roman Empire ; ranking behind Rome , the empire 's capital .
The temple was destroyed in 401 AD by a mob led by St. John Chrysostom .
Emperor Constantine I rebuilt much of the city and erected new public baths .
The city 's importance as a commercial center declined as the harbor was slowly silted up by the Cayster River ( Küçük Menderes ) .
Ephesus was one of the seven churches of Asia that are cited in the Book of Revelation .
Today 's archaeological site lies 3 kilometers southwest of the town of Selçuk , in the Selçuk district of İzmir Province , Turkey .
The ruins of Ephesus are a favorite international and local tourist attraction , partly owing to their easy access from Adnan Menderes Airport and via the port of Kuşadası .
According to the legend , he founded Ephesus on the place where the oracle of Delphi became reality ( " A fish and a boar will show you the way " ) .
About 650 BC , Ephesus was attacked by the Cimmerians , who razed the city , including the temple of Artemis .
When the Cimmerians had been driven away , the city was ruled by a series of tyrants .
He treated the inhabitants with respect , despite ruling harshly , and even became the main contributor to the reconstruction of the temple of Artemis .
His signature has been found on the base of one of the columns of the temple ( now on display in the British Museum ) .
Croesus made the populations of the different settlements around Ephesus regroup ( synoikismos ) in the vicinity of the Temple of Artemis , enlarging the city .
Ephesus continued to prosper .
As a result , rule over the cities of Ionia was ceded again to Persia .
These wars did not much affect daily life in Ephesus .
Ephesus even had female artists .
The inhabitants of Ephesus at once set about restoring the temple and even planned a larger and grander one than the original .
But the inhabitants of Ephesus demurred , claiming that it was not fitting for one god to build a temple to another .
The people of Ephesus were forced to move to a new settlement two kilometers further on , when the king flooded the old city by blocking the sewers .
After Lysimachus had destroyed the nearby cities of Lebedos and Colophon in 292 BC , he relocated their inhabitants to the new city .
After the death of Lysimachus the town again was named Ephesus .
Thus Ephesus became part of the Seleucid Empire .
After a series of battles , he was defeated by Scipio Asiaticus at the Battle of Magnesia in 190 BC .
When his grandson Attalus III died without male children of his own , he left his kingdom to the Roman Republic .
Ephesus became subject of the Roman Republic .
Many had lived in Ephesus .
Mithridates took revenge and inflicted terrible punishments .
Ephesus became , for a short time , self-governing .
When Augustus became emperor in 27 BC , he made Ephesus instead of Pergamum the capital of proconsular Asia , which covered western Asia Minor .
Ephesus entered an era of prosperity .
It was second in importance and size only to Rome .
Ephesus has been estimated to be in the range of 400,000 to 500,000 inhabitants in the year 100 , making it the largest city in Roman Asia and of the day .
The city was famed for the Temple of Artemis ( Diana ) , who had her chief shrine there , the Library of Celsus , and its theatre , which was capable of holding 25,000 spectators .
The city and temple were destroyed by the Goths in 263 AD .
Ephesus remained the most important city of the Byzantine Empire in Asia after Constantinople in the 5th and 6th centuries .
The emperor Constantine I rebuilt much of the city and erected a new public bath .
In 406 John Chrysostom , archbishop of Constantinople , ordered the destruction of the Temple of Artemis .
Emperor Flavius Arcadius raised the level of the street between the theatre and the harbour .
The basilica of St. John was built during the reign of emperor Justinian I in the sixth century .
The importance of the city as a commercial center declined as the harbor was slowly silted up by the river ( today , Küçük Menderes ) despite repeated dredging during the city 's history .
The loss of its harbor caused Ephesus to lose its access to the Aegean Sea , which was important for trade .
Even the temple of Artemis was completely forgotten by the local population .
Shortly afterwards , it was ceded to the Aydınoğulları principality that stationed a powerful navy in the harbour of Ayasuluğ ( the present-day Selçuk , next to Ephesus ) .
The town knew again a short period of flourishing during the 14th century under these new Seljuk rulers .
They were incorporated as vassals into the Ottoman Empire for the first time in 1390 .
After a period of unrest , the region was again incorporated into the Ottoman Empire in 1425 .
Ephesus was eventually completely abandoned in the 15th century and lost her former glory .
He wrote between 53 and 57 AD the letter 1 Corinthians from Ephesus ( possibly from the " Paul tower " close to the harbour , where he was imprisoned for a short time ) .
Later Paul wrote the Epistle to Ephesians while he was in prison in Rome ( around 62 AD ) .
The church at Ephesus had given their support for Ignatius , who was taken to Rome for execution .
The Church of Mary close to the harbor of Ephesus was the setting for the Third Ecumenical Council in 431 , which resulted in the condemnation of Nestorius .
A Second Council of Ephesus was held in 449 , but its controversial acts were never approved by the Catholics .
It is now surrounded by Selçuk .
The Temple of Artemis , one of the Seven Wonders of the Ancient World , is represented only by one inconspicuous column , revealed during an archaeological excavation by the British Museum in the 1870s .
The upper part of the theater was decorated with red granite pillars in the Corinthian style .
A number of figures are depicted in the reliefs , including the emperor Theodosius I with his wife and eldest son .
The temple and its statue are some of the few remains connected with Domitian .
Ephesus is believed to be the city of the Seven Sleepers .
