The same name is applied to several military units , originally raised among the Batavi .
The tribal name , probably a derivation from batawjō , refers to the region 's fertility , today known as the fruitbasket of the Netherlands ( the Betuwe ) .
The Batavi were mentioned by Julius Caesar in his commentary Commentarii de Bello Gallico , as living on an island formed by the Rhine River after it splits , one arm being the Waal the other the Lower Rhine / Old Rhine .
It is unclear whether the existing inhabitants were simply subjugated with the Batavi forming a ruling elite , or the existing inhabitants simply displaced .
Well-regarded for their skills in horsemanship and swimming -- for men and horses could cross the Rhine without losing formation , according to Tacitus .
But the sources suggest the Batavi were able to swim across rivers actually wearing full armour and weapons .
This would only have been possible by the use of some kind of buoyancy device : Ammianus Marcellinus mentions that the Cornuti regiment swam across a river floating on their shields " as on a canoe " ( 357 ) .
Numerous altars and tombstones of the cohorts of Batavi , dating to the 2nd century and 3rd century , have been found along Hadrian 's Wall , notably at Castlecary and Carrawburgh , Germany , Yugoslavia , Hungary , Romania and Austria .
A bridge was built over the river Nabalia , where the warring parties approached each other on both sides to negotiate peace .
Following the uprising , Legio X Gemina was housed in a stone castra to keep an eye on the Batavians .
Constantius Gallus added inhabitants of Batavia to his legions , " of whose discipline we still make use . "
