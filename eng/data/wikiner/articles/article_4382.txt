Most home computers had a primary OS ( and often BASIC ) stored permanently in on-board ROM , with the option of loading a more advanced disk operating system from a floppy , whether it be a proprietary system , CP/M , or later , DOS .
By the early 1990s , the increasing size of software meant that many programs demanded multiple diskettes ; a large package like Windows or Adobe Photoshop could use a dozen disks or more .
In some cases , such as with the Zip drive , the failure in market penetration was exacerbated by the release of newer higher-capacity versions of the drive and media that were not backward compatible with the original drives , thus fragmenting the user base between new users and early adopters who were unwilling to pay for an upgrade so soon .
Later , advancements in flash-based devices and widespread adoption of the USB interface provided another alternative that , in turn , made even optical storage obsolete for some purposes .
An attempt to continue the traditional diskette was the SuperDisk ( LS-120 ) in the late 1990s , with a capacity of 120 MB which was backward compatible with standard 3½-inch floppies .
Many modern systems even provide firmware support for booting to a USB-mounted floppy drive .
Which means that formats used by C64 , Amiga , Macintosh , etc. ca n't be read by these devices .
The earliest floppy disks , invented at IBM , were 8 inches in diameter .
Disks in this form factor were produced and improved upon by IBM and other companies such as Memorex , Shugart Associates , and Burroughs Corporation .
In 1976 Shugart Associates introduced the first 5¼-inch FDD and associated media .
These disk drives could be added to existing older model PCs .
Sony introduced its own small-format 90.0 mm × 94.0 mm disk .
A variant on the Sony design , introduced in 1982 by a large number of manufacturers , was then rapidly adopted .
This made them appear to the operating system as a hard drive instead of a floppy , meaning that most PCs were unable to boot from them .
The most popular of these , by far , was the LS-120 , mentioned below .
In 1994 , Iomega introduced the Zip drive .
A major reason for the failure of the Zip Drives is also attributed to the higher pricing they carried ( partly because of royalties that 3rd-party manufacturers of drives and disks had to pay ) .
It was upgraded ( as the " LS-240 " ) to 240 MB ( 240.75 MB ) .
The BIOS of many motherboards even to this day supports LS-120 drives as boot options .
LS-120 compatible drives were available as options on many computers , including desktop and notebook computers from Compaq Computer Corporation .
In the case of the Compaq notebooks , the LS-120 drive replaced the standard floppy drive in a multibay configuration .
Sony introduced its own floptical-like system in 1997 as the " 150 MB Sony HiFD " which could hold 150 megabytes ( 157.3 actual megabytes ) of data .
Although by this time the LS-120 had already garnered some market penetration , industry observers nevertheless confidently predicted the HiFD would be the real standard-floppy-killer and finally replace standard floppies in all machines .
Sony then re-engineered the device for a quick re-release , but then extended the delay well into 1998 instead , and increased the capacity to " 200 MB " ( approximately 210 megabytes ) while they were at it .
By this point the market was already saturated by the Zip disk , so it never gained much market share .
The UHD144 drive surfaced early in 1998 as the it drive , and provided 144 MB of storage while also being compatible with the standard 1.44 MB floppies .
It was used to detect the start of each track , and whether or not the disk rotated at the correct speed ; some operating systems , such as Apple DOS , did not use index sync , and often the drives designed for such systems lacked the index hole sensor .
On Apple Macintosh computers with built-in floppy drives , the disk is ejected by a motor instead of manually ; there is no eject button .
In PC-type machines , a floppy disk can be inserted or ejected manually at any time ( evoking an error message or even lost data in some cases ) , as the drive is not continuously monitored for status and so programs can make assumptions that do not match actual status ( e.g. , disk 123 is still in the drive and has not been altered by any other agency ) .
With Apple Macintosh computers , disk drives are continuously monitored by the OS ; a disk inserted is automatically searched for content , and one is ejected only when the software agrees the disk should be ejected .
External 3.5 " floppy drives from Apple were equipped with eject buttons .
The button was ignored when the drive was plugged into a Mac , but would eject the disk if the drive was used with an Apple II , as ProDOS did not support or implement software-controlled eject .
The 3-inch disk , widely used on Amstrad CPC machines , bears much similarity to the 3½-inch type , with some unique and somewhat curious features .
This made the disk look more like a greatly oversized present day memory card or a standard PC card notebook expansion card rather than a floppy disk .
Additionally , the increasing availability of broadband and wireless Internet connections decreased the overall utility of removable storage devices ( humorously named sneakernet ) .
In 1998 , Apple introduced the iMac which had no floppy drive .
On 29 January 2007 the British computer retail chain PC World issued a statement saying that only 2 % of the computers that they sold contained a built-in floppy disk drive and , once present stocks were exhausted , no more standard floppies would be sold .
In 2009 , Hewlett-Packard stopped supplying standard floppy drives on business desktops .
In the release of Microsoft Office 2010 , the floppy disk is still used as the symbol for saving files .
For example , all but the earliest models of Apple Macintosh computers that have built-in floppy drives included a disk controller that can read , write and format IBM PC-format 3½-inch diskettes .
However , few IBM-compatible computers use floppy disk drives that can read or write disks in Apple 's variable speed format .
Within the world of IBM-compatible computers , the three densities of 3½-inch floppy disks are partially compatible .
The holes on the right side of a 3½-inch disk can be altered as to ' fool ' some disk drives or operating systems ( others such as the Acorn Archimedes simply do not care about the holes ) into treating the disk as a higher or lower density one , for backward compatibility or economical reasons [ citation needed ] .
Apple eventually gave up on the format and used constant angular velocity with HD floppy disks on their later machines ; these drives were still unique to Apple as they still supported the older variable-speed format .
Commodore started its tradition of special disk formats with the 5¼-inch disk drives accompanying its PET/CBM , VIC-20 and Commodore 64 home computers , the same as the 1540 and 1541 drives used with the later two machines .
Unique among personal computer architectures , the operating system on the computer itself was unaware of the details of the disk and filesystem ; disk operations were handled by Commodore DOS instead , which was implemented with an extra MOS-6502 processor on the disk drive .
The DOS ' 2.0 disk bitmap provides information on sector allocation , counts from 0 to 719 .
As a result , sector 720 could not be written to by the DOS .
Some companies used a copy protection scheme where " hidden " data was put in sector 720 that could not be copied through the DOS copy option .
The Amiga floppy controller was basic but much more flexible than the one on the PC : it was free of arbitrary format restrictions , encoding such as MFM and GCR could be done in software , and developers were able to create their own proprietary disc formats .
With the correct filesystem driver , an Amiga could theoretically read any arbitrary format on the 3½-inch floppy , including those recorded at a slightly different rotation rate .
Commodore never upgraded the Amiga chip set to support high-density floppies , but sold a custom drive that spun at half speed ( 150 RPM ) when a high-density floppy was inserted , enabling the existing floppy controller to be used .
This drive was introduced with the launch of the Amiga 3000 , although the later Amiga 1200 was only fitted with the standard DD drive .
ADF has been around for almost as long as the Amiga itself though it was not initially called by that name .
Only with the advent of the Internet and Amiga emulators has it become a popular way of distributing disk images .
Acorn however used standard disk controllers -- initially FM , though they quickly transitioned to MFM .
The original disk implementation for the BBC Micro stored 100 KB ( 40 track ) or 200 KB ( 80 track ) per side on 5¼-inch disks in a custom format using the Disc Filing System .
For their Electron floppy disk add-on added , Acorn picked 3½-inch disks and developed the Advanced Disc Filing System ( ADFS ) .
ADFS also stored some metadata about each file , notably a load address , an execution address , owner and public privileges , and a " lock " bit .
The Archimedes used special values in the ADFS load/execute address metadata to store a 12-bit filetype field and a 40-bit timestamp .
With RISC OS 3 , the Archimedes could also read and write disk formats from other machines , for example the Atari ST and the IBM PC .
The Amiga 's disks could not be read as they used unusual sector gap markers .
Because of this modular design , it was easy in RISC OS 3 to add support for so-called image filing systems .
These were used to implement completely transparent support for IBM PC format floppy disks , including the slightly different Atari ST format .
Computer Concepts released a package that implemented an image filing system to allow access to high density Macintosh format disks .
The prospective users , both inside and outside IBM , preferred standardization to what by release time were small cost reductions , and were unwilling to retool packaging , interface chips and applications for a proprietary design .
The product never appeared in the light of day , and IBM wrote off several hundred million dollars of development and manufacturing facility .
IBM developed , and several companies copied , an autoloader mechanism that could load a stack of floppies one at a time into a drive unit .
A number of companies , including IBM and Burroughs , experimented with using large numbers of unenclosed disks to create massive amounts of storage .
The Burroughs system used a stack of 256 12-inch disks , spinning at a high speed .
The LS-240 drive supports a ( rarely used ) 32 MB capacity on standard 3½-inch HD floppies [ citation needed ] -- it is , however , a write-once technique , and can not be used in a read/write/read mode .
The format also requires an LS-240 drive to read .
The only serious attempt to speed up a 3½ " floppy drive beyond 2x was the X10 accelerated floppy drive .
