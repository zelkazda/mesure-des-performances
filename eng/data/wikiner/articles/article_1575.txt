Black Sabbath are an English rock band , formed in Birmingham in 1968 by Ozzy Osbourne ( lead vocals ) , Tony Iommi ( guitar ) , Geezer Butler ( bass guitar ) , and Bill Ward ( drums ) .
The band has since experienced multiple lineup changes , with Tony Iommi the only constant presence in the band through the years .
A total of twenty-two musicians have at one time been members of Black Sabbath .
Originally formed as a heavy blues-rock band named Earth , the band began incorporating occult -- and horror-inspired lyrics with tuned-down guitars , changing their name to Black Sabbath and achieving multiple platinum records in the 1970s .
Despite an association with occult and horror themes , Black Sabbath also composed songs dealing with social and political issues such as drugs and war .
As one of the first and most influential heavy metal bands of all time , Black Sabbath helped define the genre with releases such as quadruple-platinum Paranoid , released in 1970 .
Rolling Stone has posited the band as ' the heavy-metal kings of the ' 70s ' .
Vocalist Ozzy Osbourne 's drinking led to his firing from the band in 1979 .
He was replaced by former Rainbow vocalist Ronnie James Dio .
After a few albums with Dio 's vocals and his songwriting collaborations , Black Sabbath endured a revolving lineup in the 1980s and 1990s that included vocalists Ian Gillan , Glenn Hughes , Ray Gillen and Tony Martin .
In 1992 , Iommi and Butler rejoined Dio and drummer Vinny Appice to record Dehumanizer .
The original lineup reunited with Osbourne in 1997 and released a live album , Reunion .
The 1979 -- 1982 and 1991 -- 1992 line-up featuring Iommi , Butler , Dio , and Appice reformed in 2006 under the moniker Heaven & Hell until Dio 's death on May 16 , 2010 .
Following the breakup of their previous band Mythology in 1968 , guitarist Tony Iommi and drummer Bill Ward sought to form a heavy blues band in Aston , Birmingham .
In December 1968 , Iommi abruptly left Earth to join Jethro Tull .
" It just was n't right , so I left " , Iommi said .
" At first I thought Tull were great , but I did n't much go for having a leader in the band , which was Ian Anderson 's way .
When I came back from Tull , I came back with a new attitude altogether .
A movie theatre across the street from the band 's rehearsal room was showing the 1963 Boris Karloff horror film Black Sabbath directed by Mario Bava .
While watching people line up to see the film , Butler noted that it was " strange that people spend so much money to see scary movies " .
Following that , Butler wrote the lyrics for a song called " Black Sabbath " , which was inspired by the work of occult writer Dennis Wheatley , along with a vision that Butler had of a black silouetted figure standing at the foot of his bed .
Inspired by the new sound , the band changed their name to Black Sabbath in August 1969 , and made the decision to focus on writing similar material , in an attempt to create the musical equivalent of horror films .
Black Sabbath were signed to Philips Records in December 1969 , and released their first single , " Evil Woman " through Philips subsidiary Fontana Records in January 1970 .
Later releases were handled by Philips ' newly formed progressive rock label , Vertigo Records .
Although the single failed to chart , the band were afforded two days of studio time in late January to record their debut album with producer Rodger Bain .
Iommi recalls recording live : " We thought ' We have two days to do it and one of the days is mixing . '
Ozzy was singing at the same time , we just put him in a separate booth and off we went .
While the album was a commercial success , it was widely panned by critics , with Lester Bangs of Rolling Stone dismissing the album as " discordant jams with bass and guitar reeling like velocitised speedfreaks all over each other 's musical perimeters , yet never quite finding synch " .
The new album was initially set to be named War Pigs after the song " War Pigs " , which was critical of the Vietnam War .
However Warner changed the title of the album to Paranoid , fearing backlash by supporters of the Vietnam War .
The album 's lead-off single " Paranoid " was written in the studio at the last minute .
In 2003 , the album was ranked number 130 on Rolling Stone magazine 's list of the 500 greatest albums of all time .
In February 1971 , Black Sabbath returned to the studio to begin work on their third album .
Following the chart success of Paranoid , the band were afforded more studio time , along with a " briefcase full of cash " to buy drugs .
" We were getting into coke , bigtime " , Ward explained .
As Bill Ward explained : " The band started to become very fatigued and very tired .
While struggling to record the song " Cornucopia " after " sitting in the middle of the room , just doing drugs " , Bill Ward was nearly fired from the band .
" I hated the song , there were some patterns that were just ... horrible " Ward said .
With more time in the studio , Volume 4 saw the band starting to experiment with new textures , such as strings , piano , orchestration and multi-part songs .
The song " Tomorrow 's Dream " was released as a single -- the band 's first since Paranoid -- but failed to chart .
With new musical innovations of the era , the band were surprised to find that the room they had used previously at the Record Plant was replaced by a " giant synthesiser " .
The band rented a house in Bel Air and began writing in the summer of 1973 , but in part because of substance issues and fatigue , they were unable to complete any songs .
" Ideas were n't coming out the way they were on Volume 4 and we really got discontent " Iommi said .
While working in the dungeon , Iommi stumbled onto the main riff of " Sabbath Bloody Sabbath " , which set the tone for the new material .
Yes keyboardist Rick Wakeman was brought in as a session player , appearing on " Sabbra Cadabra " .
In November 1973 , Black Sabbath released the critically acclaimed Sabbath Bloody Sabbath .
The band began a world tour in January 1974 , which culminated at the California Jam festival in Ontario , California on 6 April 1974 .
Again the album initially saw favourable reviews , with Rolling Stone stating " Sabotage is not only Black Sabbath 's best record since Paranoid , it might be their best ever " , although later reviewers such as Allmusic noted that " the magical chemistry that made such albums as Paranoid and Volume 4 so special was beginning to disintegrate " .
Although the album 's only single " Am I Going Insane ( Radio ) " failed to chart , Sabotage features fan favourites such as " Hole in the Sky " , and " Symptom of the Universe " .
Black Sabbath toured in support of Sabotage with openers Kiss , but were forced to cut the tour short in November 1975 , following a motorcycle accident in which Osbourne ruptured a muscle in his back .
In December 1975 , the band 's record companies released a greatest hits record without input from the band , titled We Sold Our Soul for Rock ' n ' Roll .
Black Sabbath began work for their next album at Criteria Studios in Miami , Florida , in June 1976 .
Technical Ecstasy , released on 25 September 1976 , was met with mixed reviews .
For the first time the reviews did not become more favorable as time passed , two decades after its release AllMusic gave the album two stars , and noted that the band was " unravelling at an alarming rate " .
In November 1977 , while in rehearsal for their next album , and just days before the band was set to enter the studio , Ozzy Osbourne quit the band .
" The last Sabbath albums were just very depressing for me " , Osbourne said .
Former Fleetwood Mac and Savoy Brown vocalist Dave Walker was brought into rehearsals in October 1977 , and the band began working on new songs .
As the new band were in rehearsals in January 1978 , Osbourne had a change of heart and rejoined Black Sabbath .
" Three days before we were due to go into the studio , Ozzy wanted to come back to the band, " Iommi explained .
" It took quite a long time, " Iommi said .
Reviewers called Black Sabbath 's performance " tired and uninspired " , a stark contrast to the " youthful " performance of Van Halen , who were touring the world for the first time .
" At that time , Ozzy had come to an end " , Iommi said .
" We were all doing a lot of drugs , a lot of coke , a lot of everything , and Ozzy was getting drunk so much at the time .
When I 'm drunk I am horrible , I am horrid, " Ward said .
" Alcohol was definitely one of the most damaging things to Black Sabbath .
Sharon Arden , ( later Sharon Osbourne ) daughter of Black Sabbath manager Don Arden , suggested former Rainbow vocalist Ronnie James Dio to replace Ozzy Osbourne in 1979 .
Dio officially joined in June , and the band began writing their next album .
With a notably different vocal style from Osbourne 's , Dio 's addition to the band marked a change in Black Sabbath 's sound .
" They were totally different altogether " , Iommi explains .
Ozzy was a great showman , but when Dio came in , it was a different attitude , a different voice and a different musical approach , as far as vocals .
Dio would sing across the riff , whereas Ozzy would follow the riff , like in " Iron Man " .
Dio 's term in Black Sabbath has also brought the " metal horns " gesture to popularity in heavy metal subculture .
Dio adopted it , originally a superstitious move to ward off the " evil eye " , as a greeting to audience .
Geezer Butler temporarily left the band in September 1979 , and was initially replaced by Geoff Nicholls of Quartz on bass .
The new lineup returned to Criteria Studios in November to begin recording work , with Butler returning to the band in January 1980 , and Nicholls moving to keyboards .
Produced by Martin Birch , Heaven and Hell , was released on 25 April 1980 , to critical acclaim .
Over a decade after its release AllMusic said the album was " one of Sabbath 's finest records , the band sounds reborn and re-energised throughout " .
The next day , the band appeared at the 1980 Day on the Green at Oakland Coliseum .
While on tour , Black Sabbath 's former label in England issued a live album culled from a seven-year old performance , entitled Live at Last without any input from the band .
" I was sinking very quickly " , Ward later said .
Concerned with Ward 's declining health , Iommi brought in drummer Vinny Appice , without informing Ward .
The band completed the Heaven and Hell world tour in February 1981 , and returned to the studio to begin work on their next album .
Black Sabbath 's second studio album produced by Martin Birch and featuring Ronnie James Dio as vocalist , Mob Rules was released in October 1981 , to be well received by fans , but less so by the critics .
The album 's title track " The Mob Rules " , which was recorded at John Lennon 's old house in England , also featured in the 1981 animated film Heavy Metal , although the film version is an alternate take , and differs from the album version .
During the mixing process for the album , Iommi and Butler had a falling out with Dio .
In addition , Dio was not satisfied with the pictures of him in the artwork .
Live Evil is when it all fell apart .
Because they 're not as good as me , so I do what I want to do, " Dio later said .
" I refuse to listen to Live Evil , because there are too many problems .
Live Evil was released in January 1983 , but was overshadowed by Ozzy Osbourne 's Speak of the Devil , a platinum selling live album that contained only Black Sabbath songs , released five months earlier .
The two original members left , Tony Iommi and Geezer Butler , began auditioning new singers for the band 's next release .
While the project was not initially set to be called Black Sabbath , pressures from the record label forced the group to retain the name .
The band entered The Manor Studios in Shipton-on-Cherwell , Oxfordshire , England , in June 1983 with a returned and newly sober Bill Ward on drums .
Born Again was met with mixed reviews from fans and critics alike .
Although he performed on the album , drummer Bill Ward was unable to tour because of the pressures of the road , and quit the band after the commencement of the Born Again album .
" I fell apart with the idea of touring, " Ward later said .
The band headlined the 1983 Reading Festival , adding the Deep Purple song " Smoke on the Water " to their set list .
The tour in support of Born Again included a giant set of the Stonehenge monument .
In a move that would be later parodied in the mockumentary This Is Spinal Tap , the band made a mistake in ordering the set piece .
As Geezer Butler later explained :
Following the completion of the Born Again tour in March 1984 , vocalist Ian Gillan left Black Sabbath to re-join Deep Purple , which was reforming after a long hiatus .
Bevan left at the same time , and Gillan remarked that he and Bevan were made to feel like " hired help " by Iommi .
The new lineup wrote and rehearsed throughout 1984 , and eventually recorded a demo with producer Bob Ezrin in October .
" When Ian Gillan took over that was the end of it for me " , Butler later said .
I got really disillusioned with it and Gillan was really pissed off about it .
Following Butler 's exit , sole remaining original member Tony Iommi put Black Sabbath on hiatus , and began work on a solo album with long-time Sabbath keyboardist Geoff Nicholls .
The event marked the first time the original lineup appeared on stage since 1978 , and also featured reunions of The Who and Led Zeppelin .
Returning to his solo work , Iommi enlisted bassist Dave Spitz and drummer Eric Singer , and initially intended to use multiple singers , including Rob Halford of Judas Priest , ex-Deep Purple and Trapeze vocalist Glenn Hughes , and ex-Black Sabbath vocalist Ronnie James Dio .
Glenn Hughes came along to sing on one track and we decided to use him on the whole album . "
The band spent the remainder of the year in the studio , recording what would become Seventh Star .
Warner Bros. refused to release the album as a Tony Iommi solo release , instead insisting on using the name Black Sabbath .
" It opened up a whole can of worms really, " Iommi explained , " because I think if we could have done it as a solo album , it would have been accepted a lot more . "
The new lineup rehearsed for six weeks , preparing for a full world tour , although the band were again forced to use the Black Sabbath name .
" I was into the ' Tony Iommi project ' , but I was n't into the Black Sabbath moniker, " Hughes said .
" The idea of being in Black Sabbath did n't appeal to me whatsoever .
Glenn Hughes singing in Black Sabbath is like James Brown singing in Metallica .
He has insisted that he was a singer in Black Sabbath between January and May 1985 .
Tony Iommi has never confirmed this , as he was working on a solo release that was later named as a Sabbath album .
Black Sabbath began work on new material in October 1986 at Air Studios in Montserrat with producer Jeff Glixman .
Bassist Dave Spitz quit over " personal issues " , and ex-Rainbow bassist Bob Daisley was brought in .
Drummer Bev Bevan refused to play the shows , and was replaced by Terry Chimes , formerly of The Clash .
After nearly a year in production , The Eternal Idol was released on 8 December 1987 and ignored by contemporary reviewers .
" It was a completely new start " , Iommi said .
Iommi enlisted ex-Rainbow drummer Cozy Powell , long-time keyboardist Nicholls and session bassist Laurence Cottle , and rented a " very cheap studio in England " .
Black Sabbath released Headless Cross in April 1989 , and again ignored by contemporary reviewers .
Following the album 's release , the band added touring bassist Neil Murray , formerly of Whitesnake and Gary Moore 's backing band .
The band returned to the studio in February 1990 to record Tyr , the follow-up to Headless Cross .
While not technically a concept album , some of the album 's lyrical themes are loosely based on Norse mythology .
Following the show , the two expressed interest in rejoining Black Sabbath .
Butler convinced Iommi , who in turn broke up the current lineup , dismissing vocalist Tony Martin and bassist Neil Murray .
" I do regret that in a lot of ways " , Iommi said .
We decided to [ reunite with Dio ] and I do n't even know why , really .
Ronnie James Dio and Geezer Butler joined Tony Iommi and Cozy Powell in the fall of 1990 to begin working on the next Black Sabbath release .
The year-long recording process was plagued with problems , primarily stemming from writing tension between Iommi and Dio , and some songs were re-written multiple times .
" Dehumanizer took a long time , it was just hard work " , Iommi said .
Dio later recalled the album as difficult , but worth the effort .
The resulting album , Dehumanizer was released on 22 June 1992 .
Additionally , the perception by many fans of a return of some semblance of the " real " Black Sabbath provided the band with some much needed momentum .
Black Sabbath began touring in support of Dehumanizer in July 1992 with Testament , Danzig , Prong , and Exodus .
While on tour , former vocalist Ozzy Osbourne announced his first retirement , and invited Black Sabbath to open for his solo band at the final two shows of his No More Tours tour in Costa Mesa , California .
The band agreed , aside from vocalist Ronnie James Dio , who said :
Dio quit Black Sabbath following a show in Oakland , California on 13 November 1992 , one night before the band were set to appear at Osbourne 's retirement show .
Judas Priest vocalist Rob Halford stepped in at the last minute , performing two nights with the band .
Iommi and Butler also joined Osbourne and former drummer Bill Ward on stage for the first time since 1985 's Live Aid concert , performing a brief set of Black Sabbath songs .
Drummer Vinny Appice left the band following the reunion show to join Ronnie James Dio 's solo band , later appearing on Dio 's Strange Highways and Angry Machines .
Iommi and Butler enlisted former Rainbow drummer Bobby Rondinelli , and reinstated former vocalist Tony Martin .
The band returned to the studio to work on new material , again not originally intended to be released under the Black Sabbath name .
As Geezer Butler explains :
Tony Iommi asked me to join BLACK SABBATH !
Under pressure from their record label , the band released their seventeenth studio album , Cross Purposes , on 8 February 1994 , under the Black Sabbath name .
Following the touring cycle for Cross Purposes , bassist Geezer Butler again quit the band .
" I finally got totally disillusioned with the last Sabbath album , and I much preferred the stuff I was writing to the stuff Sabbath were doing " .
Butler formed a solo project called GZR , and released Plastic Planet in 1995 .
Following Butler 's departure , newly returned drummer Bill Ward once again left the band .
Iommi reinstated former members Neil Murray on bass , and Cozy Powell on drums , effectively reuniting the Tyr lineup .
Black Sabbath embarked on a world tour in July 1995 with openers Motörhead and Tiamat , but two months into the tour , drummer Cozy Powell left the band , citing health issues , and was replaced by former drummer Bobby Rondinelli .
In 1997 , Tony Iommi disbanded the current lineup to officially reunite with Ozzy Osbourne and the original Black Sabbath lineup .
Vocalist Tony Martin claimed that an original lineup reunion had been in the works since the band 's brief reunion at Ozzy Osbourne 's 1992 Costa Mesa show , and that the band released subsequent albums to fulfill their record contract with I.R.S. records .
I.R.S. Records released a compilation album in 1996 to fulfill the band 's contract , entitled The Sabbath Stones , which featured songs from Born Again to Forbidden .
In the summer of 1997 , Tony Iommi , Geezer Butler , and Ozzy Osbourne officially reunited to co-headline the Ozzfest festival tour along side Osbourne 's solo band .
In December 1997 , the group was joined by Ward , marking the first reunion of the original four members since Osbourne 's 1992 " retirement show " .
The original lineup recorded two shows at the Birmingham NEC , which were released as the double live album Reunion on 20 October 1998 .
Following the Ozzfest appearances , the band was put on hiatus while members worked on solo material .
Tony Iommi released his first official solo album , Iommi , in 2000 , while Osbourne continued work on his next solo release , Down to Earth .
Black Sabbath returned to the studio to work on new material with all four original members and producer Rick Rubin in the spring of 2001 , but the sessions were halted when Osbourne was called away to finish tracks for his solo album in the summer of 2001 .
" It just came to an end " , Iommi said .
" We did n't go any further , and it 's a shame because [ the songs ] were really good " . Iommi commented on the difficulty getting all of the band members together to work on material :
The show introduced Osbourne to a broader audience and to capitalise , the band 's back catalogue label , Sanctuary Records released a double live album Past Lives , which featured concert material recorded in the ' 70s , including the previously unofficial Live at Last album .
The band remained on hiatus until the summer of 2004 when they returned to headline Ozzfest 2004 and 2005 .
At the awards ceremony Metallica played two Black Sabbath songs , " Hole in the Sky " and " Iron Man " in tribute to the band .
For the release , Iommi , Butler , Dio and Appice reunited to write and record three new songs as Black Sabbath .
While the lineup of Osbourne , Butler , Iommi and Ward were still officially called Black Sabbath , the new lineup opted to call themselves Heaven & Hell , after the album of the same name , to avoid confusion .
Drummer Bill Ward was initially set to participate , but dropped out before the tour began due to musical differences with " a couple of the band members " .
He was replaced by former drummer Vinny Appice , effectively reuniting the lineup that had featured on the Mob Rules and Dehumanizer albums .
In November 2007 , Dio confirmed that the band have plans to record a new studio album , which was recorded in the following year .
The box set , The Rules of Hell , featuring remastered versions of all the Dio fronted Black Sabbath albums , was supported by the Metal Masters Tour .
In 2009 , the band announced the name of their debut studio album , The Devil You Know , released on April 28 .
Iommi noted that he has been the only band member for the full forty one years of the band , and that his bandmates relinquished their rights to the name in the 1980s , therefore claiming more rights to the name of the band .
Although , in the suit , Osbourne is seeking 50 % ownership of the trademark , he has said that he hopes the proceedings will lead to equal ownership among the four original members .
Ozzy states " I 'm not gon na say I 've written [ a reunion ] out forever , but right now I do n't think there is any chance .
On May 16 , 2010 , Ronnie James Dio passed away after a long fought battle against stomach cancer .
In June , 2010 the legal battle between Ozzy Osbourne and Tony Iommi over the trademarking of the Black Sabbath name ended , but the terms of the settlement have not been disclosed .
Although Black Sabbath have gone through many lineups and stylistic changes , their original sound focused on ominous lyrics and doomy music , often making use of the musical tritone , also called the " devil 's interval " .
Standing in stark contrast to popular music of the early 1970s , Black Sabbath 's dark sound was dismissed by rock critics of the era .
As the band 's primary songwriter , Tony Iommi wrote the majority of Black Sabbath 's music , while Osbourne would write vocal melodies , and bassist Geezer Butler would write lyrics .
The process was sometimes frustrating for Iommi , who often felt pressured to come up with new material .
On Iommi 's influence , Osbourne later said :
Early Black Sabbath albums feature tuned-down guitars , which contributed to the dark feel of the music .
In 1966 , before forming Black Sabbath , guitarist Tony Iommi suffered an accident while working in a sheet metal factory , losing the tips of two fingers on his right hand .
Iommi almost gave up music , but was urged by a friend to listen to Django Reinhardt , a jazz guitarist who lost the use of two fingers .
Inspired by Reinhardt , Iommi created two thimbles made of plastic and leather to cap off his missing fingers .
Black Sabbath are arguably one of the most influential heavy metal bands of all time .
The band helped to create the genre with ground breaking releases such as Paranoid , an album that Rolling Stone magazine said " changed music forever " , and called the band " the Beatles of heavy metal " .
Black Sabbath have influenced many acts including Iron Maiden , Slayer , Death , Korn , Mayhem , Venom , Judas Priest , Guns N ' Roses , Metallica , Alice in Chains , Anthrax , Disturbed , Opeth , Pantera , Megadeth , The Smashing Pumpkins , Slipknot , the Foo Fighters , Fear Factory , Candlemass , and Godsmack .
Metallica 's Lars Ulrich , who , along with bandmate James Hetfield inducted Black Sabbath into the Rock and Roll Hall of Fame in 2006 , said " Black Sabbath is and always will be synonymous with heavy metal " , while Hetfield said " Sabbath got me started on all that evil-sounding shit , and it 's stuck with me .
Tony Iommi is the king of the heavy riff . "
It just opens up your mind to another dimension ... Paranoid is the whole Sabbath experience ; very indicative of what Sabbath meant at the time .
Anthrax guitarist Scott Ian said " I always get the question in every interview I do , ' What are your top five metal albums ? '
I make it easy for myself and always say the first five Sabbath albums . "
Lamb of God 's Chris Adler said : " If anybody who plays heavy metal says that they were n't influenced by Black Sabbath 's music , then I think that they 're lying to you .
I think all heavy metal music was , in some way , influenced by what Black Sabbath did . "
Sabbath were also one of the earliest to turn gothic music into a genre .
