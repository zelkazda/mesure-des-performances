For more than two months , Nelson chased the French , on several occasions only missing them by a matter of hours .
With the French army ashore , the fleet anchored in Aboukir Bay , a station 20 miles ( 32 km ) northeast of Alexandria , in a formation that its commander , Vice-Admiral François-Paul Brueys D'Aigalliers , believed created a formidable defensive position .
The battle reversed the strategic situation in the Mediterranean , allowing the Royal Navy to assume a dominant position it retained for the rest of the war .
The battle has remained prominent in the popular consciousness , with perhaps the best-known representation being Felicia Hemans 's 1826 poem Casabianca .
However , the French Navy was dominant in the Mediterranean Sea , following the withdrawal of the British fleet after the outbreak of war between Britain and Spain in 1796 .
The destination of the expedition was kept top secret ; most of the army 's officers did not know of its target , and Bonaparte himself did not publicly reveal his goal until the first stage of the expedition was complete .
Bonaparte 's armada sailed from Toulon on 19 May 1798 , making rapid progress through the Ligurian Sea and collecting more ships at Genoa before sailing southwards along the Sardinian coast , passing Sicily on 7 June .
On 9 June the fleet arrived off Malta , then under the ownership of the Knights of St. John of Jerusalem , ruled by Grand Master Ferdinand von Hompesch zu Bolheim .
Bonaparte demanded that his fleet be permitted entry to the fortified harbour of Valetta , and when the demand was refused the French general responded by ordering a large scale invasion of the Maltese Islands , overrunning the defenders after 24 hours of skirmishing .
Within a week Bonaparte had resupplied his ships , and on 19 June his fleet departed for Alexandria in the direction of Crete , leaving 4,000 men at Valetta under General Claude-Henri Vaubois to ensure French control of the islands .
While Bonaparte was sailing to Malta , the Royal Navy re-entered the Mediterranean for the first time in over a year .
However in July 1797 he had lost an arm at the Battle of Santa Cruz de Tenerife and had been forced to return to Britain to recuperate .
Returning to the fleet at the Tagus in late April 1798 , he was ordered to collect the squadron stationed at Gibraltar and sail for the Ligurian Sea .
The remainder of the squadron was scattered ; the ships of the line sheltered at San Pietro Island off Sardinia , while the frigates were blown to the west and failed to return .
On 7 June , following hasty repairs to his flagship , Nelson was joined off Toulon by a fleet of ten ships of the line and a fourth rate ship .
On 22 June , Nelson encountered a brig sailing from Ragusa and was told that the French had sailed eastwards from Malta on 16 June .
After conferring with his captains , the admiral decided that the French target must be Egypt and set off in pursuit .
On the evening of 22 June , Nelson 's fleet passed the French in the darkness , overtaking the slow invasion convoy without realising how close they were to their target .
Making rapid time on a direct route , Nelson reached Alexandria on 28 June and discovered that the French were not there .
Concerned by his close encounter with Nelson , Bonaparte ordered an immediate invasion , his troops coming ashore in a poorly managed amphibious operation in which at least 20 drowned .
As a result , an alternative anchorage was selected at Aboukir Bay , 20 miles ( 32 km ) northeast of Alexandria .
Nelson 's fleet reached Syracuse on Sicily on 19 July and took on essential supplies .
On 28 July at Coron , intelligence was finally obtained describing the French attack on Egypt and Nelson turned south across the Mediterranean .
His scouts , HMS Alexander and HMS Swiftsure , discovered the French transport fleet at Alexandria on the afternoon of 1 August .
Aboukir Bay is a coastal indentation 16 nautical miles ( 30 km ) across , stretching from the village of Abu Qir in the west to the town of Rosetta to the east , where one of the mouths of the River Nile empties into the Mediterranean .
The fort was garrisoned by French soldiers and armed with at least four cannon and two heavy mortars .
The van of the French line was led by Guerrier , positioned 2400 yards ( 2200 m ) southeast of Aboukir Island and about 1000 yards ( 910 m ) from the edge of the shoals that surrounded the island .
It also created areas within the French line that were not covered by the broadside of any ship .
British vessels could anchor in those spaces and engage the French without reply .
At 14:00 on 1 August , lookouts on HMS Zealous reported the French anchored in Aboukir Bay , its signal lieutenant just beating the lieutenant on HMS Goliath with the signal , but inaccurately describing 16 French ships of the line instead of 13 .
At the same time , French lookouts on Heureux , the ninth ship in the French line , sighted the British fleet approximately 9 nautical miles ( 17 km ) off the mouth of Aboukir Bay .
Troubridge 's ship HMS Culloden was also some distance from the main body , towing a captured merchant ship .
At the sight of the French , Troubridge abandoned the vessel and made strenuous efforts to rejoin Nelson .
Nelson then gave orders for his leading ships to slow down in order to allow the British fleet to approach in a more organised formation .
Nelson ordered the fleet to slow down at 16:00 to allow his ships to rig " springs " on their anchor cables , a system of attaching the bow anchor that increased stability and allowed his ships to swing their broadsides to face an enemy while stationary .
The direction of the wind meant that the French rear division would be unable to easily join the battle and would be cut off from the front portions of the line .
None of Nelson 's captains fell for the ruse and the British fleet continued undeterred .
Hood replied that he would take careful soundings as he advanced to test the depth of the water , and that " If you will allow the honour of leading you into battle , I will keep the lead going . "
The convention in naval warfare of the time was that ships of the line did not attack frigates when there were ships of equal size to engage , but in firing first French Captain Claude-Jean Martin had negated the rule and Saumarez waited until the frigate was at close range before replying .
At 19:00 the identifying lights in the mizenmasts of the British fleet were lit .
Captain Darby recognised that his position was untenable and ordered the anchor cables cut at 20:20 .
The French admiral took fifteen minutes to die , remaining on deck and refusing to be carried below by his men .
The remainder of the crew , numbering over a thousand men , were killed , including Captain Casabianca and his son .
More than half of Franklin 's crew had been killed or wounded .
Although Captain Du Petit Thouars had lost both legs and an arm he remained in command , insisting on having the tricolour nailed to the mast to prevent it from being struck and giving orders from his position propped up on deck in a bucket of wheat .
Throughout the engagement the French rear had kept up an arbitrary fire on the battling ships ahead .
The only noticeable effect was the smashing of Timoléon ' s rudder by misdirected fire from the neighbouring Généreux .
The surviving French ships of the line , covering their retreat with gunfire , gradually pulled to the east away from the shore at 06:00 .
Two other French ships still flew the tricolour , but neither was in a position to either retreat or fight .
Despite strenuous efforts , Captain Hood 's isolated ship came under heavy fire and was unable to cut off the trailing Justice as the French survivors escaped seawards .
For the remainder of 2 August Nelson 's ships made improvised repairs and boarded and consolidated their prizes .
French casualties are harder to calculate but were significantly higher .
The French ships suffered severe damage : two ships of the line and two frigates were destroyed ( as well as a bomb vessel scuttled by its crew ) , and three other captured ships were too battered to ever sail again .
Nelson , who on surveying the bay on the morning of 2 August said " Victory is not a name strong enough for such a scene " , remained at anchor in Aboukir Bay for the next two weeks , preoccupied with recovering from his wound , writing dispatches and assessing the military situation in Egypt using documents captured on board one of the prizes .
Nelson 's head wound was recorded as being " three inches long " , with " the cranium exposed for one inch " .
Over the next few days all but 200 of the captured prisoners were landed on shore under strict terms of parole , although Bonaparte later ordered them to be formed into an infantry unit and added to his army .
On 8 August the fleet 's boats stormed Aboukir Island , which surrendered without a fight .
The landing party removed four of the guns and destroyed the rest along with the fort they were mounted in , renaming the island " Nelson 's Island " .
The messenger was a staff officer sent by the Governor of Alexandria General Jean Baptiste Kléber , and the report had been hastily written by Admiral Ganteaume , who had subsequently rejoined Villeneuve 's ships at sea .
One account reports that when he was handed the message , Bonaparte read it without emotion before calling the messenger to him and demanding further details .
Bonaparte 's most immediate concern however was with his own officers , who began to question the wisdom of the entire expedition .
When they replied that they were " marvellous " , Bonaparte responded that it was just as well , since he would have them shot if they continued " fostering mutinies and preaching revolt . "
King George III addressed the Houses of Parliament on 20 November with the words :
Saumarez 's convoy of prizes stopped first at Malta , where Saumarez provided assistance to a rebellion on the island among the Maltese population .
The remaining prizes underwent basic repairs and then sailed for Britain , eventually arriving at Plymouth .
The Honourable East India Company presented Nelson with £ 10,000 ( £ 846210 as of 2010 ) in recognition of the benefit his action had on their holdings and similar awards were made by the cities of London , Liverpool and other municipal and corporate bodies .
From his own captains , Nelson was presented with a sword and a portrait as " proof of their esteem " .
However the French ships were hampered by their inadequate deployment , reduced crews and the failure of the rear division under Villeneuve to meaningfully participate , all of which contributed to their defeat .
The Battle of the Nile has been called " arguably , the most decisive naval engagement of the great age of sail " , and " the most splendid and glorious success which the British Navy gained . "
The ensuing Siege of Malta lasted for two years before the defenders were finally starved into surrender .
The French general returned to France without his army late in the year , leaving Kléber in command of Egypt .
This led to a series of campaigns that slowly sapped the strength from the French army trapped in Egypt .
The potential of a successful engagement at sea to change the course of history is underscored by the list of French army officers carried aboard the convoy who later formed the core of the generals and marshals under Emperor Napoleon .
In addition to Bonaparte himself , Louis Alexandre Berthier , Auguste de Marmont , Jean Lannes , Joachim Murat , Louis Desaix , Jean Reynier , Antoine-François Andréossy , Jean-Andoche Junot , Louis-Nicolas Davout and Dumas were all passengers on the cramped Mediterranean crossing .
Another memorial , the Nile Clumps near Amesbury , are stands of beech trees purportedly planted by Lord Queensbury at the bequest of Lady Hamilton and Thomas Hardy after Nelson 's death .
A similar arboreal memorial is thought to have been planted near Alnwick by Nelson 's agent Alexander Davison .
The work was later taken over by Franck Goddio , who led a major project to explore the bay in 1998 .
He found that material was scattered over an area 500 metres ( 550 yd ) in diameter , and in addition to military and nautical equipment recovered a large number of gold and silver coins from countries across the Mediterranean , some from the seventeenth century .
These graves , which included a woman and three children , were relocated in 2005 to a cemetery at Shatby in Alexandria .
