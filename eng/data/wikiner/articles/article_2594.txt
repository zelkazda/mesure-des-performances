Conan the Barbarian is a fictional character in books , comics and movies .
Conan is often associated with the fantasy subgenre of sword-and-sorcery and heroic fantasy .
He was created by Texan writer Robert E. Howard in 1932 via a series of fantasy stories sold to Weird Tales magazine .
Conan the Barbarian was created by Robert E. Howard and was the spiritual successor to an earlier character , Kull of Atlantis .
For months , Howard had been in search of a new character to market to the burgeoning pulp outlets of the early 1930s .
Some Howard scholars believe this Conan to be a forerunner of the more famous character .
In February 1932 , Howard vacationed at a border town on the lower Rio Grande to enjoy the local culture .
During this trip , he further conceived the character of Conan and also wrote the poem " Cimmeria " , much of which echoes specific passages in Plutarch 's Lives .
Having digested these prior influences after he returned from his trip , Howard rewrote the rejected Kull story " By This Axe I Rule ! "
( May 1929 ) with his new hero in mind , re-titling it " The Phoenix on the Sword " .
Although " The Frost-Giant 's Daughter " was rejected , the magazine accepted " The Phoenix on the Sword " after it received the requested polishing .
" The Phoenix on the Sword " appeared in Weird Tales in December 1932 , thus marking Conan 's first appearance in print .
Weird Tales would become famous for its unique stable of notable authors , including H. P. Lovecraft , Clark Ashton Smith , Tennessee Williams , Robert Bloch , Seabury Quinn and others .
The publication and success of " The Tower of the Elephant " would spur Howard to write many more Conan stories for Weird Tales .
By the time of Howard 's suicide in 1936 , he had written twenty-one complete tales , seventeen of which had been published , as well as a number of unfinished fragments .
Following Howard 's death , the copyright of the Conan stories passed through several hands .
Eventually , under the guidance of L. Sprague de Camp and Lin Carter , the stories were expurgated , revised and sometimes rewritten .
For roughly forty years , the original versions of Howard 's Conan stories remained out of print .
In the 1980s and 1990s , the copyright holders of the Conan franchise permitted Howard 's stories to go out of print entirely , while continuing to sell Conan works by other authors .
The Gollancz edition mostly used the versions of the stories as published in Weird Tales .
The first book , Conan of Cimmeria : Volume One ( 1932 -- 1933 ) includes Howard 's notes on his fictional setting , as well as letters and poems concerning the genesis of his ideas .
Among them , the three books include all of the original unedited Conan stories .
This is a specific epoch in a fictional timeline created by Howard for many of the low fantasy tales of his artificial legendary .
The reasons behind the invention of the Hyborian Age were perhaps commercial : Howard had an intense love for history and historical dramas ; however , at the same time , he recognized the difficulties and the time-consuming research work needed in maintaining historical accuracy .
By conceiving a timeless setting -- " a vanished age " -- and by carefully choosing names that resembled human history , Howard shrewdly avoided the problem of historical anachronisms and the need for lengthy exposition .
Conan is a Cimmerian ( based somewhat loosely on the Celts ) , a barbarian of the far north .
After its destruction , he was struck by wanderlust and began the adventures chronicled by Howard , encountering skulking monsters , evil wizards , tavern wenches , and beautiful princesses .
This observation could however be said to be in contrast to the assumption that Conan was merely another barbarian hero as envisioned by Howard for Weird Tales magazine .
Conan has " sullen " or " smoldering " blue eyes and a black " square-cut mane " .
Howard once describes him as having a hairy chest and , while comic book interpretations often portray Conan as wearing a loincloth or other minimalist clothing , Howard describes the character as wearing whatever garb is typical for the land and culture in which Conan finds himself .
Though Howard never gave a strict height or weight for Conan in a story , only describing him in loose terms like " giant " and " massive " , he did once state that Conan and another of Howard 's characters , the crusader Cormac Fitzgeoffrey , were " physical doubles " at 6 ' 2 " and 210 lb ( 188 cm and 95 kg , a description of Howard 's own height and weight ) .
In the tales no human is ever described as stronger than Conan , although several are mentioned as taller or of larger bulk .
Although Conan is muscular , Howard frequently compares his agility and way of moving to that of a panther ( see for instance " Jewels of Gwahlur " , " Beyond the Black River " or " Rogues in the House " ) .
In his fictional historical essay The Hyborian Age , Howard describes how the people of Atlantis -- the land where his character King Kull originated -- had to move east after a great cataclysm changed the face of the world and sank their island , settling where northern Ireland and Scotland would eventually be located .
In the same work , Howard also described how the Cimmerians eventually moved south and east after the age of Conan ( presumably in the vicinity of the Black Sea , where the historical Cimmerians dwelt ) .
Despite his brutish appearance , Conan uses his brains as well as his brawn .
The Cimmerian is a talented fighter , but his travels have given him vast experience in other trades , especially as a thief ; he is also a talented commander , tactician and strategist , as well as a born leader .
In addition , Conan speaks many languages , including advanced reading and writing abilities : in certain stories , he is able to recognize , or even decipher , certain ancient or secret signs and writings .
Conan is a formidable armed and unarmed combatant .
With his back to the wall Conan is capable of engaging and killing opponents by the score .
Conan is far from untouchable and has been captured and defeated several times ( on one occasion knocking himself out drunkenly running into a doorhinge ) .
Howard frequently corresponded with H. P. Lovecraft , and the two would sometimes insert references or elements of each others ' settings in their works .
Later editors reworked many of the original Conan stories by Howard , thus diluting this connection .
As Conan remarks in one story :
Additionally , fans such as comic book artist Mark Schultz have concluded that Conan was an idealized alter ego for Howard .
Unlike the modern , stereotypical view of a brainless barbarian , Howard originally created Conan as a thoughtful figure , although primarily a man of action rather than a man of deep thought or brooding .
A closer alter ego for Howard , often depicted as a melancholic man who often battled with depression , much like Howard himself ( the writer eventually committed suicide ) is King Kull , Conan 's original forebear ( cf " By This Axe , I Rule " and " The Mirrors of Tuzun Thune " ) .
The character of Conan has proven durably popular , resulting in Conan stories by later writers such as Poul Anderson , Leonard Carpenter , Lin Carter , L. Sprague de Camp , Roland J. Green , John C. Hocking , Robert Jordan , Sean A. Moore , Björn Nyberg , Andrew J. Offutt , Steve Perry , John Maddox Roberts , Harry Turtledove , and Karl Edward Wagner .
Some of these writers have finished incomplete Conan manuscripts by Howard .
Others were created by rewriting Howard stories which originally featured entirely different characters from entirely different milieus .
In total , more than fifty novels and dozens of short stories featuring the Conan character have been written by authors other than Howard .
The later volumes contain some stories rewritten by L. Sprague de Camp ( like " The Treasure of Tranicos " ) , including several non-Conan Howard stories , mostly historical exotica situated in the Levant at the time of the crusades , which he turned into Conan yarns .
The Gnome edition also issued the first Conan story written by an author other than Howard -- the final volume published , which is by Björn Nyberg and revised by de Camp .
These were completed by de Camp and Carter , and new stories written entirely by the two were added as well .
Its covers featured dynamic images by Frank Frazetta that , for many fans , presented the " definitive " impression of Conan and his world .
For decades to come , most other portrayals of the Cimmerian and his imitators were heavily influenced by the cover paintings of this series .
Note that no consistent timeline has yet accommodated every single Conan story .
The very first Conan cinematic project was planned by Edward Summer .
Summer envisioned a series of Conan movies , much like the James Bond franchise .
However , the resulting film , Conan the Barbarian ( 1982 ) , was a creative mixture of screenwriters ' Oliver Stone and John Milius ideas as well as plots from Conan stories ( written also by Howard 's successors , notably Lin Carter and L. Sprague de Camp ) .
The plot of Conan the Barbarian ( 1982 ) begins with Conan being enslaved by the Vanir raiders of Thulsa Doom , a malevolent warlord who is responsible for the slaying of Conan 's parents and the genocide of his people .
The film was directed by John Milius and produced by Dino De Laurentiis .
The character of Conan was played by Arnold Schwarzenegger and was his break-through role as an actor .
This film was followed by a less popular sequel , Conan the Destroyer in 1984 .
This sequel was a more typical fantasy-genre film and was even less faithful to Howard 's Conan stories .
Warner Bros. spent seven years trying to get the project off the ground , with development attempts made by Larry and Andy Wachowski , John Milius , and Robert Rodriguez who was closest to completing development but left the project for Grindhouse .
Boaz Yakin was hired in 2006 to start again .
In November 2008 , Brett Ratner was prematurely announced to be the director of Conan to The Hollywood Reporter by Lerner , something which displeased him as he pointed out : " I am not doing Conan now . "
June 2009 revealed Marcus Nispel would take the reins of director to the film .
In January 2010 , Jason Momoa was selected for the role of Conan .
In February 2010 , on the eve of production , Sean Hood was brought in to retool the screenplay .
There have been three television series related to Conan : A live-action TV series and animated cartoon series -- both entitled Conan the Adventurer , as well as a second animated series entitled Conan and the Young Warriors .
Conan the Barbarian has appeared in comics nearly non-stop since 1970 .
Marvel Comics launched Conan the Barbarian ( 1970 -- 1993 ) and the now-classic Savage Sword of Conan ( 1974 -- 1995 ) .
Dark Horse Comics is currently publishing compilations of the 1970s Marvel Comics series in trade paperback format .
Marvel Comics introduced a relatively lore-faithful version of Conan the Barbarian in 1970 with Conan the Barbarian , written by Roy Thomas and illustrated by Barry Windsor-Smith .
Smith was succeeded by penciller John Buscema , while Thomas continued to write for many years .
The highly successful Conan the Barbarian series spawned the more adult , black-and-white Savage Sword of Conan in 1974 .
Written by Roy Thomas and most art by John Buscema or Alfredo Alcala , the Savage Sword of Conan soon became one of the most popular comic series in the 1970s and is now-considered a cult classic .
The Marvel Conan stories were also adapted as a newspaper comic strip which appeared daily and Sunday from 4 September 1978 to 12 April 1981 .
Originally written by Roy Thomas and illustrated by John Buscema , the strip was continued by several different Marvel artists and writers .
Dark Horse Comics began their comic adaptation of the Conan saga in 2003 .
Entitled simply Conan , the series was first written by Kurt Busiek and pencilled by Cary Nord .
This series is an interpretation of the original Conan material by Robert E. Howard with no connection whatsoever to the earlier Marvel comics or any Conan story not written or envisioned by Howard supplemented by wholly original material .
Seven video games have been released based on the Conan mythos .
TSR signed a license agreement in 1984 to publish the Conan game .
The following characters have prominent roles in the Conan mythos .
However , since Robert E. Howard published his Conan stories at a time when the date of publication was the marker ( 1932 through 1963 ) , and any new owners failed to renew them to maintain the copyrights , the exact copyright status of all of Howard 's Conan works are in question .
