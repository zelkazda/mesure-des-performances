Haiti is the poorest country in the Americas .
Contrary to popular belief [ citation needed ] , however , it is not the poorest in the Western Hemisphere .
A macroeconomic program developed in 2005 with the help of the International Monetary Fund helped the economy grow 1.8 % in 2006 , the highest growth rate since 1999 .
Under President René Préval , the country 's economic agenda included trade and tariff liberalization , measures to control government expenditure and increase tax revenues , civil service downsizing , financial sector reform , and the modernization of state-owned enterprises through their sale to private investors , the provision of private sector management contracts , or joint public-private investment .
Structural adjustment agreements with the International Monetary Fund , World Bank , Inter-American Development Bank , and other international financial institutions are aimed at creating necessary conditions for private sector growth , have proved only partly successful .
Following the coup , the United States adopted mandatory sanctions , and the OAS instituted voluntary sanctions aimed at restoring constitutional government .
The assembly sector , heavily dependent on U.S. markets for its products , employed nearly 80,000 workers in the mid-1980s .
Government agreement with the International Monetary Fund ( IMF ) on a staff monitored program , followed by its payment of its $ 32 million arrears to the IDB in July , paved the way for renewed IDB lending .
The IDB disbursed $ 35 million of a $ 50 million policy-based loan in July and began disbursing four previously approved project loans totaling $ 146 million .
The IDB , IMF , and World Bank also discussed new lending with the government .
Overall foreign assistance levels have declined since FY 1995 , the year elected government was restored to power under a UN mandate , when over $ 600 million in aid was provided by the international community .
Other major export partners in 2005 included the Dominican Republic and Canada .
Aid was restored in July 2004 after an interim administration was named .
The United Nations embargo of 1994 put out of work most of the 80,000 workers in the assembly sector .
When reelected in 2000 , President Aristide promised to remedy this situation but instead introduced a non-sustainable plan of " cooperatives " that guaranteed investors a 10 percent rate of return .
The United Nations and the International Monetary Fund have led efforts to diversify and expand the finance sector , making credit more available to rural populations .
Most sources that we do have available come from U.S. agencies such as the Agency for International Development ( USAID ) .
Still , given that the sources of this data has remained the same for the past 15 years , we can at least see a trend of unemployment staying high throughout this period , but rising sharply in the mid to late 90 's peaking at 70 % in 1999 ( 2000 CIA World Factbook is the source for that number ) , and then decreasing to the usual rates of around 50 % in recent years .
Much of this article is based on public domain material from the U.S. government .
