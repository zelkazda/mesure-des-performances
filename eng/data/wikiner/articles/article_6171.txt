The IPX/SPX protocol stack is supported by Novell 's NetWare network operating system .
Because of Netware 's popularity through the late 1980s into the mid 1990s , IPX became a popular internetworking protocol .
IPX can be transmitted over Ethernet using one of the following 4 encapsulation types :
