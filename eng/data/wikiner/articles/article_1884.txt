Bundaberg Rum is a dark rum produced in Bundaberg , Australia , often referred to as " Bundy " .
This company does not produce Bundaberg Ginger Beer .
Bundaberg Rum originated because the local sugar mills had a problem with what to do with the waste molasses after the sugar was extracted ( it was heavy , difficult to transport and the costs of converting it to stock feed were rarely worth the effort ) .
Bundaberg rum was first produced 1888 , production ceased from 1907 to 1914 and from 1936 to 1939 after fires , the second of which caused rum from the factory to spill into the nearby Burnett River .
The Bundaberg Rum distillery is open to visitors for tours of the facility .
There is also a museum and offers free samples of Bundaberg Rum products for visitors .
Bundaberg is also a sponsor of the NSW Waratahs .
Bundaberg Rum also sponsors the rugby league ANZAC Test till 2009 .
Previously Bundaberg Rum had sponsored a stadium in Cairns , Australia which was formally known as Bundaberg Rum Stadium but has been renamed to Cazaly 's Stadium .
Bundaberg Rum has also been criticised for targeting its advertising towards young people and boys , through television commercials during NRL broadcasts , and other promotions .
" They will abuse bar staff , half a dozen a night , normally gangs of blokes , the marketing is directed at yobbos , " one bar owner told The Age newspaper .
Some young Australian men warn them about ' drop bears ' , saying that they 're " like a koala , only bigger and meaner " and " they drop from the trees " .
There are currently a number of products available which are distributed by Diageo :
