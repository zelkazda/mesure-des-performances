Aveiro is a city in Aveiro Municipality in Portugal , with a total area of 199.9 km² , a total population of 73,559 inhabitants , and 59,860 electors ( 2006 ) .
It is the second most populous city in the Centro Region of Portugal , after Coimbra .
However , the city of Aveiro together with neighbouring Ílhavo , make one conurbation which has a population of 113,908 inhabitants , making it one of the most important by population density in the Centro Region .
The municipalityis composed of 14 parishes ( freguesias , and is located in Aveiro District and the chief city of Baixo Vouga .
The seat of the municipality is the city of Aveiro , with about 67,003 inhabitants in the 5 urban city ( cidade ) parishes .
Located on the shore of the Atlantic Ocean , Aveiro is an industrial city with an important seaport .
Port/Harbour ( harbor ) : Porto de Aveiro .
Aveiro and environs have several shopping centers and malls .
Aveiro is known in Portugal for its traditional sweets , ovos moles and trouxas de ovos , both made from eggs .
The same storm also created a reef barrier at the Atlantic Ocean .
The famous explorer João Afonso was born in Aveiro .
Unfortunately , never came back to Portugal ; he died in the region and is credited for bringing to Lisbon the first pepper that ever came out of those parts .
He was also credited as one of the discoverers of the Newfoundland fisheries .
Zeca Afonso was singer and composer .
The University of Aveiro was created in 1973 and is considered one of the most dynamic and innovative universities of Portugal , attracting thousands of students to the city .
Aveiro 's sister cities are :
