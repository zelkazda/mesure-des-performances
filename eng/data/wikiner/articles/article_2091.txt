The history of the Central African Republic .
In 1875 the Sudanese sultan Rabih az-Zubayr governed Upper-Oubangui , which included present-day C.A.R .
In 1906 , the Oubangui-Chari territory was united with the Chad colony ; in 1910 , it became one of the four territories of the Federation of French Equatorial Africa ( A.E.F .
) , along with Chad , Republic of the Congo , and Gabon .
The assembly in C.A.R .
Boganda ruled until his death in a March 1959 plane crash .
His cousin , David Dacko , replaced him , governing the country until 1965 and overseeing the country 's declaration of independence on 13 August 1960 .
Moreover , an African judicial commission reported that he had " almost certainly " taken part in the massacre of some 100 children for refusing to wear the compulsory school uniforms .
Bokassa made an unexpected return in October 1986 and was retried .
He died of a heart attack in Bangui on 3 November 1996 at age 75 .
Dacko 's efforts to promote economic and political reforms proved ineffectual , and on 20 September 1981 , he in turn was overthrown in a bloodless coup by General André Kolingba .
Economic difficulties caused by the looting and destruction during the 1996 and 1997 mutinies , energy crises , and government mismanagement continued to trouble Patassé 's government through 2000 .
On 15 March 2003 rebels who controlled part of the country moved into Bangui and installed their commander , General François Bozizé , as president , while President Patassé was out of the country .
In June later that year , the African Union ( AU ) lifted sanctions against the country , which had been applied after the 2003 usurpation of power .
