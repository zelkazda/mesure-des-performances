The Commodore 1571 was Commodore 's high-end 5¼ " floppy disk drive .
The 1571 was released to match the Commodore 128 , both design-wise and feature-wise .
It was announced in the summer of 1985 , at the same time as the C128 , and became available in quantity later that year .
Depending on format , CP/M disks would format to 360 KB , with a mechanical maximum capacity of a 400 KB format .
The 1571 featured a " burst mode " when used in conjunction with the C128 ( although not when used with the Commodore 64 or VIC-20 ) .
A C128 in CP/M mode equipped with a 1571 was capable of reading and writing floppy disks formatted for many CP/M computers ; specifically , the following formats :
Other formats were possible if their characteristics were added to the CP/M C128-specific source code and the CP/M operating system were re-assembled .
With additional software , it was possible to read and write to DOS-formatted floppies as well .
Although the C128 could not run any DOS-based software , this capability allowed data files to be exchanged with PC users .
Commodore announced a dual-drive version of the 1571 , to be called the 1572 , but quickly cancelled it , reportedly due to technical difficulties with the 1572 DOS .
