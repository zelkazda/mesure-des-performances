Commercial messages and political campaign displays have been found in the ruins of Pompeii and ancient Arabia .
Lost and found advertising on papyrus was common in Ancient Greece and Ancient Rome .
N. W. Ayer & Son was the first full-service agency to assume responsibility for advertising content .
In the early 1950s , the DuMont Television Network began the modern practice of selling advertisement time to multiple sponsors .
Previously , DuMont had trouble finding sponsors for many of their programs and compensated by selling smaller blocks of advertising time to several businesses .
However , it was still a common practice to have single sponsor shows , such as The United States Steel Hour .
The single sponsor model is much less prevalent now , a notable exception being the Hallmark Hall of Fame .
The late 1980s and early 1990s saw the introduction of cable television and particularly MTV .
As cable and satellite television became increasingly prevalent , specialty channels emerged , including channels entirely devoted to advertising , such as QVC , Home Shopping Network , and ShopTV Canada .
At the turn of the 21st century , a number of websites including the search engine Google , started a change in online advertising by emphasizing contextually relevant , unobtrusive ads intended to help , rather than inundate , users .
This reflects an increasing trend of interactive and " embedded " ads , such as via product placement , having consumers vote through text messages , and various innovations utilizing social network services such as Facebook .
Public service advertising reached its height during World Wars I and II under the direction of more than one government .
Advertising on the World Wide Web is a recent phenomenon .
Prices of Web-based advertising space are dependent on the " relevance " of the surrounding web content and the traffic that the website receives .
By 2007 the value of mobile advertising had reached $ 2.2 billion and providers such as Admob delivered billions of mobile ads .
Among others , Comcast Spotlight is one such advertiser employing this method in their video on demand menus .
The resulting ads were among the most-watched and most-liked Super Bowl ads .
Founded in 2007 , Zooppa has launched ad competitions for brands such as Google , Nike , Hershey 's , General Mills , Microsoft , NBC Universal , Zinio , and Mini Cooper .
Greece 's regulations are of a similar nature , " banning advertisements for children 's toys between 7 am and 10 pm and a total ban on advertisement for war toys " .
To counter this effect , many advertisers have opted for product placement on TV shows like Survivor .
Advertising education has become widely popular with bachelor , master and doctorate degrees becoming available in the emphasis .
