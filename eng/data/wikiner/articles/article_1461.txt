The Battle of Ramillies ( pronounced /ˈræmɪliːz/ ) was a major engagement of the War of the Spanish Succession fought on 23 May 1706 .
In less than four hours , Villeroi 's army was utterly defeated .
On 11 January 1706 , Marlborough finally reached London at the end of his diplomatic tour , but he had already been planning his strategy for the coming season .
The Duke left The Hague on 9 May .
Far from standing on the defensive therefore -- and unbeknown to Marlborough -- Louis XIV was persistently goading his marshal into action .
" [ Villeroi ] began to imagine, " wrote St Simon , " that the King doubted his courage , and resolved to stake all at once in an effort to vindicate himself . "
Accordingly , on 18 May , Villeroi set off from Leuven at the head of 70 battalions , 132 squadrons and 62 cannon -- comprising an overall force of some 60,000 troops -- and crossed the river Dyle to seek battle with the enemy .
With a short lift in the mist , Cadogan soon discovered the smartly ordered lines of Villeroi 's advance guard some four miles ( 6 km ) off ; a galloper hastened back to warn Marlborough .
The battlefield of Ramillies is very similar to that of Blenheim , for here too there is an immense area of arable land unimpeded by woods or hedges .
His centre was secured by Ramillies itself , lying on a slight eminence which gave distant views to the north and east .
Meanwhile Villeroi deployed his forces .
Villeroi also positioned powerful batteries near Ramillies .
Villeroi 's right flank fell into chaos and was now open and vulnerable .
Villeroi had given his personal attention to that wing and strengthened it with large bodies of horse and foot that ought to have been taking part in the decisive struggle south of Ramillies .
At around 15:30 , Overkirk advanced his massed squadrons on the open plain in support of the infantry attack on Ramillies .
A crisis threatened the centre , but from his vantage point Marlborough was at once aware of the situation .
Thanks to a combination of battle-smoke and favourable terrain , his redeployment went unnoticed by Villeroi who made no attempt to transfer any of his own 50 unused squadrons .
Nevertheless the danger passed , enabling the Duke to attend to the positioning of the cavalry reinforcements feeding down from his right flank -- a change of which Villeroi remained blissfully unaware .
Even the squadrons currently being scrambled together by Villeroi behind Ramillies could not withstand the onslaught .
What was left of Villeroi 's army was now broken in spirit ; the imbalance of the casualty figures amply demonstrates the extent of the disaster for Louis XIV 's army .
Villeroi also lost 52 artillery pieces and his entire engineer pontoon train .
Villeroi was helpless to arrest the process of collapse .
" This, " wrote Marlborough wearily , " I take to be owing to our late success . "
Only later when Cadogan and Churchill went to take charge did the town 's defences begin to fail .
Louis XIV however , was more forgiving , greeting his old friend with the kind words -- " At our age , Marshal , we must no longer expect good fortune . "
In the mean time , Marlborough invested the elaborate fortress of Menin which , after a costly siege , capitulated on 22 August .
Dendermonde finally succumbed on 6 September followed by Ath -- the last conquest of 1706 -- on 2 October .
In Marlborough , however , Correlli Barnett puts the total casualty figure as high as 30,000 -- 15,000 dead and wounded with an additional 15,000 taken captive .
Trevelyan estimates Villeroi 's casualties at 13,000 , but adds , ' his losses by desertion may have doubled that number ' .
