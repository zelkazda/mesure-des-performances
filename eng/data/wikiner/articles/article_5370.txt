He is popular for his films from a wide range of genres such as Scarface ( 1932 ) , Bringing Up Baby ( 1938 ) , Only Angels Have Wings ( 1939 ) , His Girl Friday ( 1940 ) , Sergeant York ( 1941 ) , The Big Sleep ( 1946 ) , Red River ( 1948 ) , Gentlemen Prefer Blondes ( 1953 ) , Rio Bravo ( 1959 ) , and El Dorado ( 1967 ) .
Hawks reworked the scripts of most of films he directed without taking official credit for his work .
For Howard Hughes he directed Scarface ( 1932 ) ; for RKO , Bringing Up Baby ( 1938 ) and for Columbia , Only Angels Have Wings ( 1939 ) and His Girl Friday ( 1940 ) .
His film , Sergeant York ( 1941 ) , starring Gary Cooper , was the highest-grossing film of its year and won two Academy Awards ( Best Actor and Best Editing ) .
In 1944 , Hawks filmed the first of two films starring Humphrey Bogart and Lauren Bacall , To Have and Have Not , which was the first film pairing of the couple .
He followed that with The Big Sleep ( 1946 ) .
In 1948 , he filmed Red River , with John Wayne and Montgomery Clift .
In 1951 , he produced -- and , reputedly , also directed ( without credit ) -- The Thing from Another World .
1959 's Rio Bravo , starring John Wayne , Dean Martin and Walter Brennan , was remade twice by Hawks -- in 1967 ( El Dorado ) and again in 1970 ( Rio Lobo ) .
Both starred John Wayne .
Hawks was married three times :
Hawks 's own functional definition of what constitutes a " good movie " is revealing of his no-nonsense style : " Three great scenes , no bad ones . "
Hawks also defined a good director as " someone who does n't annoy you " .
While Hawks was not sympathetic to feminism , he popularized the Hawksian woman archetype , which could be considered a prototype of the modern post-feminist movement .
His directorial style and the use of natural , conversational dialogue in his films were cited a major influence on many noted filmmakers , including Robert Altman , John Carpenter , and Quentin Tarantino .
Critic Leonard Maltin labeled Hawks " the greatest American director who is not a household name, " noting that , while his work may not be as well known as Ford , Welles , or DeMille , he is no less a talented filmmaker .
Scarface ( 1932 ) , was rated " culturally significant " by the U.S. Library of Congress .
