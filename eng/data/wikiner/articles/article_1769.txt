This formula and the triangular arrangement of the binomial coefficients are often attributed to Blaise Pascal , who described them in the 17th century , but they were known to many mathematicians who preceded him .
Around 1665 , Isaac Newton generalized the formula to allow real exponents other than nonnegative integers , and in fact it can be generalized further , to complex exponents .
