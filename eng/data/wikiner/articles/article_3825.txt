The letter of Jude was one of the disputed books of the Canon .
Although its canonical status was contested , its authenticity was never doubted by the Early Church .
" More remarkable is the evidence that by the end of the second century Jude was widely accepted as canonical. "
Eusebius classified it with the " disputed writings , the antilegomena . "
The debate has continued over the author 's identity as the apostle , the brother of Jesus , both , or neither .
Not a lot is known of Jude , which would explain the apparent need to identify him by reference to his better-known brother .
It is agreed that he is not the Jude who betrayed Jesus , Judas Iscariot .
The Epistle of Jude is a brief book of only a single chapter with 25 verses .
The epithets contained in this writing are considered to be some of the strongest found in the New Testament .
The epistle concludes with a doxology , which is considered to be one of the highest in quality contained in the Bible .
The Epistle of Jude references at least two other books , one of which is non-canonical in all churches , the other non-canonical in most churches .
The Book of Enoch is not considered canonical by most churches , although it is by the Ethiopian Orthodox church .
It is generally accepted by scholars that the author of the Epistle of Jude was familiar with the Book of Enoch and was influenced by it in thought and diction .
Online translations of the Epistle of Jude :
