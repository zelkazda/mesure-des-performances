It was also popular in the Dutch East Indies as well as in the Dutch New Netherland Colony ( New York ) .
It was worth eight reals ( hence the nickname " pieces of eight " ) , and was widely circulated during the 18th century in the Spanish colonies in the New World , and in the Philippines .
However Federal Reserve banks are only required to deliver credits instead of money .
The silver content of US coinage was mostly removed in 1965 and the dollar essentially became a baseless free-floating fiat currency , though the US Mint continues to make silver $ 1 coins at this weight .
This would explain a reference to the sum of " Ten thousand dollars " mentioned in Macbeth .
The real Macbeth , upon whom the play was based , lived in the 11th century , making the reference anachronistic ; however anachronisms are not rare in Shakespeare 's work .
This expression appeared again in the 1940s , when US troops came to the UK during World War II .
