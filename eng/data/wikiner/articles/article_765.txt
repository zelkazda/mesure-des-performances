AZ , an acronym for Alkmaar Zaanstreek , is an association football club from the city of Alkmaar , Netherlands .
AZ was founded in 1967 as AZ ' 67 , [ citation needed ] but is most commonly known as AZ Alkmaar .
In 1981 , they became Dutch champions and UEFA Cup finalists .
In 1988 , AZ were relegated from the Eredivisie .
AZ returned to the Eredivisie in 1998 .
In 2004 -- 05 , they reappeared in the UEFA Cup tournament , advancing to the semi-finals .
They also finished third in the Eredivisie that season , again qualifying for the UEFA Cup , after spending most of the season in the top two spots .
A remarkable achievement , since AZ is financially not a big club and it does not have a similar fanbase like their Eredivisie rivals : AZ 's home ground in the 2005 -- 06 season , the Alkmaarderhout , had a spectator capacity of 8,390 .
In the summer of 2006 , the club moved to a new 17,000 capacity stadium AZ Stadion .
AZ had a very good 2006 -- 07 season , which ended , however , in disaster .
Going into the last game of the 2006 -- 07 season , AZ led PSV and Ajax by goal-differential for the Eredivisie championship , but ended up third after losing this last match against bottom-dweller Excelsior , playing with 10 men for 80 minutes .
Further setbacks followed when AZ lost the KNVB Cup finals to Ajax , 8 -- 7 in penalty kicks after a drawn game , and also lost to Ajax over two playoff games for the Champions League .
After the season , key players like Tim de Cler , Danny Koevermans , and Shota Arveladze left the team .
In the 2007 -- 08 season , AZ performed so badly ( first round loss in the KNVB Cup , elimination from the UEFA Cup before winter break and a final 11th position in the Eredivisie ) , that team manager Louis van Gaal felt obliged to hand in his resignation in March 2008 .
However , after protests from the players and direction , van Gaal withdrew his resignation to finish his contractual obligations .
The 2008 -- 09 season had an unpromising start with two defeats against NAC Breda and ADO Den Haag .
However , starting with a 1 -- 0 victory over defending league champions PSV , the team did n't lose in the next 28 matches , including a stretch of 11 matches in which the opposing teams did not score a single goal .
Three weeks before the end of the season , despite their second home defeat of the season ( against Vitesse ) , AZ became Eredivisie champions when nearest rivals Twente and Ajax both lost their matches .
AZ 's second championship was the first Eredivisie title in 28 years , and the first time since then that the Eredivisie was won by a team other than the " big three " ( Ajax , PSV and Feyenoord ) .
Ronald Koeman , succeeded Louis van Gaal after the 2008 -- 09 season .
Van Gaal had already left for Bayern Munich after becoming league champions with AZ .
Koeman became the manager for AZ on 17 May 2009 .
Former Zenit St. Petersburg manager Dick Advocaat took over for the rest of the season .
For the 2010 -- 2011 season , Gertjan Verbeek , coming from Heracles Almelo , will be the new manager .
AZ play their home games at the AZ Stadion , located in the southern part of the city of Alkmaar .
The stadium , which is owned directly by the club , was inaugurated in 2006 and replaced the old Alkmaarderhout venue .
In order to further grow the club 's budget , the AZ board of directors decided to extend the capacity of the new stadium to a minimum of 30,000 seated spectators .
In October 2009 sponsor DSB Bank was declared bankrupt .
The stadiumname changed from DSB Stadion to AZ Stadion , as it was considered undesirable that the stadium was linked with a non-existent bank .
Below is a table with AZ 's international results in the past seasons .
Below is a table with AZ 's domestic results since the introduction of professional football in 1956 .
