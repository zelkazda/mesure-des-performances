While his family remained in South Africa , Pessoa returned to Lisbon in 1905 , to study diplomacy .
In 1915 a group of artists and poets , including Fernando Pessoa , Almada Negreiros , Mário de Sá-Carneiro and others , created the literary magazine Orpheu , which introduced modernist literature to Portugal .
If Franz Kafka is the writer of Prague , Fernando Pessoa is certainly the writer of Lisbon .
From 1905 to 1921 , when his family returned from Pretoria after the death of his stepfather , he lived in fifteen different places around the city , mooving from a rented room to another according to his financial troubles and the troubles of the young Portuguese Republic .
In fact , from 1907 until his death , in 1935 , Pessoa worked in twenty one firms located in Lisbon 's downtown , sometimes in two or three of them simultaneously .
The statue of Fernando Pessoa ( above ) can be seen outside A Brasileira , one of the places where he would meet friends , writers and artists during the period of Orpheu .
However , in 1920 the prestigious literary review Athenaeum included one of those poems .
His interest in occultism led Pessoa to correspond with Aleister Crowley .
He later helped Crowley plan an elaborate fake suicide when he visited Portugal in 1930 .
In 1988 ( the centenary of his birth ) , Pessoa 's remains were moved to the Jerónimos Monastery , in Lisbon , where Vasco da Gama , Luís de Camões , and Alexandre Herculano are also buried .
According to Pessoa himself , there were three main heteronyms : Alberto Caeiro , Álvaro de Campos and Ricardo Reis .
Ricardo Reis , after an abstract meditation , which suddenly takes concrete shape in an ode .
Campos , when I feel a sudden impulse to write and do n't know what .
For each of his ' voices ' , Pessoa conceived a highly distinctive poetic idiom and technique , a complex biography , a context of literary influence and polemics and , most arrestingly of all , subtle interrelations and reciprocities of awareness .
None of this triad resembles the metaphysical solitude , the sense of being an occultist medium which characterise Pessoa 's ' own ' intimate verse . "
Alberto Caeiro was Pessoa 's first great heteronym ; summarized by Pessoa , writing : He sees things with the eyes only , not with the mind .
Octavio Paz called him the innocent poet .
Paz made a shrewd remark on the heteronyms : In each are particles of negation or unreality .
Never question it .
Ricardo Reis is the main character of José Saramago 's 1986 novel The Year of the Death of Ricardo Reis .
Álvaro de Campos manifests , in a way , as an hyperbolic version of Pessoa himself .
As such , his poetry is the most emotionally intense and varied , constantly juggling two fundamental impulses : on the one hand a feverish desire to be and feel everything and everyone , declaring that ' in every corner of my soul stands an altar to a different god ' ( alluding to Walt Whitman 's desire to ' contain multitudes ' ) , on the other , a wish for a state of isolation and a sense of nothingness .
' Fernando Pessoa-himself ' is not the ' real ' Fernando Pessoa .
For this reason ' Fernando Pessoa-himself ' stands apart from the poet proper .
Pessoa-himself has been described as indecisive and doubt plagued , as restless .
Like Campos he can be melancholic , weary , resigned .
The strength of Pessoa-himself 's poetry rests in his ability to suggest a sense of loss ; of sorrow for what can never be .
This definition does not sufficiently encompass the peculiar brand of tedium experienced by Pessoa-himself .
Kierkegaard tells how if asked to choose between the two ; between a perpetual state of boredom , or eternal bodily pain ; he would choose -- eternal bodily pain .
The first two poems ( " The castles " and " The escutcheons " ) draw inspiration from the material and spiritual natures of Portugal .
Another well-known quote from Mensagem is the first line from Ulysses , " O mito é o nada que é tudo " ( a possible translation is " The myth is the nothing that is all " ) .
This poem refers Ulysses , king of Ithaca , as Lisbon 's founder .
The first series of two articles engage the issue ' The new Portuguese poetry viewed sociologically ' ( nos. 4 and 5 ) ; the second series of three articles is entitled ' The psychological aspect of the new Portuguese poetry ' ( nos. 9,11 and 12 ) .
