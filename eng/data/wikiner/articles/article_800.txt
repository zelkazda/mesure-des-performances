Abdul Rashid Dostum ( born 1954 ) is a former pro-Soviet fighter during the Soviet war in Afghanistan and is considered by many to be the leader of Afghanistan 's Uzbek community and the party Junbish-e Milli-yi Islami-yi Afghanistan .
He is a general and the Chief of Staff to the Commander in Chief of the Afghan National Army a role often viewed as ceremonial .
Dostum spent a year living in Turkey .
Human rights groups have accused his troops of human rights violations , charges which Dostum denies .
In 1970 he began to work in a state-owned gas refinery in Sheberghan , Jowzjan Province , participating in union politics , as the new government started to arm the staff of the workers in the oil and gas refineries .
Because of the new communist ideas entering Afghanistan in the 1970s , he enlisted himself in the army .
The KGB reported that Amin sought to cut ties with the Soviet Union and instead ally itself with the People 's Republic of China and Pakistan .
Soviet military commander announced to Radio Kabul that Afghanistan had been " liberated " from Amin 's rule .
By this time Dostum was commanding a militia battalion to fight and rout rebel forces .
Dostum and his new division reported directly to then-President Mohammad Najibullah .
Later on he became the commander of the military unit 374 in Jowzjan .
He defended the communist Republic of Afghanistan against the American and Pakistani-backed mujahideens in the 1980s .
They militia forces were deployed in the city Qandahar in 1988 when Soviet forces withdrew in 1989 .
After the fall of the Soviet Union in 1991 the communist regime faced economic problems .
The Soviet Union was Afghanistan 's main trading partner from the start in 1978 .
This eventually led to government officials swapping allegiances and would eventually lead to Mohammad Najibullahs governments fall in 1992 .
Dostum army forces would become an important factor in the fall of Kabul in 1992 .
On April 18 , 1992 the mujahideen began their revolt against the government of Najibullah .
In 1994 , Dostum allied himself with the forces of Gulbuddin Hekmatyar .
Again , Dostum was laying a siege on Kabul which started in 1995 and ended in 1997 .
This time he was fighting against the government Burhanuddin Rabbani and Massoud .
Following the rise of the Taliban and their capture of Herat and Kabul , Dostum aligned himself with Rabbani against the Taliban .
Dostum however retreated to the city of Mazar-i-Sharif .
In October 1996 Dostum came to an agreement with Massoud to form the anti-Taliban coalition that outside Afghanistan became known as the Northern Alliance .
Much like other northern alliance leaders , Dostum also faced infighting within his group and was later forced tor retreat from power thanks to his General Abdul Malik Pahlawan .
Because of this treason , Dostum was forced to flee to Turkey .
He then rejoined forces with the Northern Alliance , and turned against his erstwhile allies , helping to drive them from Mazar-i-Sharif .
But in 1998 he was forced to flee to Turkey again .
Dostum returned in 2001 .
At this time Massoud had used his CIA funds to fly Dostum and his commanders back to open a new front in the campaign against Taliban .
Along with General Mohammed Fahim and Ismail Khan , Dostum was one of three leaders of the Northern Alliance .
On November 24 , 2000 , 300 Taliban soldiers retreated after the Siege of Kunduz by American and Aghan military forces .
The taliban laid down their weapons a few miles from the city of Mazar-i-Sharif .
They eventually surrendered peacefully to Dostum .
A small group of armed foreign fighters drove to Mazar-i-Sharif and were moved to the 19th century prison fortress , Qala-i-Jangi .
The uprising eventually overpowered the Northern Alliance soldiers placed to defend the prison .
Dostum served as a deputy defense minister for Karzai in the national government in Kabul .
In March 2003 , Dostum established a North Zone of Afghanistan , against the wishes of interim president Hamid Karzai .
The two are now generally politically allied as part of a broader ideological effort to protect the interests of Afghanistan 's war veterans and to preserve their own power .
On March 1 , 2005 President Hamid Karzai appointed him Chief of Staff to the Commander in Chief .
Early on February 3 , Dostum 's house was surrounded by police .
According to the authorities , the stand-off at Dostum 's home between his fighters and the police ended with Dostum 's agreement to cooperate with the authorities in an investigation of the incident .
The consequences will be very dangerous -- catastrophic -- for the stability of Afghanistan . "
He asserted that the size of warlords private armies was increasing , fueled by illicit profits from Afghanistan 's Opium trade .
According to Dostum , this was " not in line with the law " , and he said that he would request Karzai 's intervention .
Three allies of Dostum -- Latif Pedram and two members of parliament -- were also summoned for the investigation .
Some media reports beginning December 4 said that Dostum was " seeking political asylum " in Turkey , while others said he was exiled .
In most ethnic-Uzbek dominated areas in which Dostum has control or influence , he encourages women to live and work freely , as well as encouraging music , sports and allowing for freedom of religion .
Senior Afghan government officials do not trust Dostum as they show great concern that Dostum is covertly rearming his forces .
Late at night on 16 August 2009 , Dostum made a requested return from exile to Kabul to support President Hamid Karzai in his bid for re-election .
The next day , the last day of campaigning , he flew by helicopter to his northern stronghold of Sheberghan , where he was greeted by 20,000 supporters in the local stadium .
