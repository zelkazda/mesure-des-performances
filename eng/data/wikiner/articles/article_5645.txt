" Heart of Oak " is the official march of the Royal Navy of the United Kingdom .
It is also the official march of the Royal Canadian Navy .
Heart of Oak was originally written as an opera .
Britain 's continued success in the war boosted the song 's popularity .
The phrase " hearts of oak " appears in The Aeneid .
In the Star Trek : The Next Generation episode " Allegiance " , an alien double of Capt. Jean-Luc Picard leads his crew in singing this song , much to their surprise .
