Revelation brings together the worlds of heaven , earth , and hell in a final confrontation between the forces of good and evil .
Revelation 's cryptic nature has ensured that it would always be a source of controversy .
Nevertheless , it has not only endured , but captured the imagination of generations of Bible students , both professionals and laypeople alike .
Most scholars think that Revelation was written near the end of the 1st century .
While Revelation 's larger themes are abundantly clear , its details provide a considerable challenge for interpreters .
This section will discuss Revelation 's history , authorship , and some of the controversies surrounding it .
The last document of the New Testament is commonly known today as the Book of Revelation , or simply , Revelation .
From this latter noun comes the title in the Authorized King James Version , the Revelation of Saint John the Divine , divine being a seventeenth century word for theologian .
The author also states that he was on Patmos when he received his first vision .
[ citation needed ] Charles Erdman ( 1866 -- 1960 ) advocated apostolic authorship and wrote that only the Apostle John fits the image of the author derived from the text
Irenaeus ( c. 115-202 ) assumes it as a conceded point .
Eusebius ( ca. 263 -- 339 ) was inclined to class the Apocalypse with the accepted books .
Jerome ( 347-420 ) relegated it to second class .
It is not included in the Peshitta .
Most commentators accept Revelation as the unified text of one author , but this opinion is by no means universal .
It has also been contended that the core verses of the book , in general chapters 4 through 22 , are surviving records of the prophecies of John the Baptist .
In this view , the Lamb of God references and other hallmarks of Revelation are linked to what is known of John the Baptist , though it must be confessed that very little is known of him .
[ citation needed ] Revelation 's place in the canon was not guaranteed , with doubts raised as far back as the second century about its character , symbolism , and apostolic authorship .
According to Merrill Unger and Gary Larson , in spite of the objections that have been raised over the years , Revelation provides a logical conclusion , not just to the New Testament , but to the Bible as a whole , and there is a continuous tradition dating back to the second century which supports the authenticity of the document , and which indicates that it was generally included within the , as yet unformalized , canon of the early church .
According to early tradition , this book was composed near the end of Domitian 's reign , around the year 95 AD .
Others contend for an earlier date , 68 or 69 AD , in the reign of Nero or shortly thereafter .
Irenaeus writes , " was seen no very long time since , but almost in our day , towards the end of Domitian 's reign . "
While some recent scholars have questioned the existence of a large-scale Domitian persecution , others believe that Domitian 's insistence on being treated as a god may have been a source of friction between the Church and Rome .
The date of writing would then be between 64 AD , when persecution broke out , and 68 AD , when Nero died .
Some interpreters attempt to reconcile the two dates by placing the visions themselves at the earlier date ( during the 60s ) and the publication of Revelation under Domitian , who reigned in the 90s when Irenaeus says the book was written .
A number of literary elements can be identified in Revelation , such as structure , plot , major characters , and unifying themes .
The repeated occurrence of the number seven contributes to the overall unity of Revelation .
John 's exile to Patmos , together with the phrase , " your brother and companion in tribulation, " implies a time of persecution .
A plot , or general storyline , can be identified in Revelation .
Revelation has an array of colorful characters .
Archangel Michael defeats the dragon .
However , a few ancient manuscripts of the Revelation say the number is 616 , fifty less than the more well known numeral .
This woman is associated with the Beast and refers to a counterfeit " bride " to the church .
The following outline does not attempt to interpret Revelation , but presents the details of the book in the manner , and in the order , that they appear .
In the late classical and medieval eras , the Church disavowed the millennium as a literal thousand-year kingdom .
Midtribulationists believe that the rapture of the faithful will occur approximately halfway through the Tribulation , after it begins but before the worst part of it occurs .
( Pretribulationist Tim LaHaye admits a post-tribulation rapture is the closest of the three views to that held by the early church . )
Tim LaHaye promotes the belief that Babylon will be the capital of a worldwide empire .
Books about the " rapture " by authors like Hal Lindsey , and the more recent Left Behind novels ( by Jerry Jenkins and Tim LaHaye ) and movies , have done much to popularize this school of thought .
Papias , believed to be a disciple of the Apostle John , was a premillenialist , according to Eusebius .
Also Justin Martyr and Irenaeus expressed belief in premillennialism in their writings .
This view was held by Jonathan Edwards .
Historicists hold that the events predicted in the Bible have been taking place in history .
Preterism holds that the contents of Revelation constitute a prophecy of events that were fulfilled in the first century .
Some preterists see the second half of Revelation as changing focus to Rome , its persecution of Christians , and the fall of the Roman Empire .
Eastern Orthodoxy treats the text as simultaneously describing contemporaneous events and as prophecy of events to come , for which the contemporaneous events were a form of foreshadow .
Book of Revelation is the only book of the New Testament that is not read during services by the Eastern Orthodox Church .
The esoterist views Revelation as bearing multiple levels of meaning , the lowest being the literal or " dead-letter . "
This view is held by teachers such as H.P. Blavatsky , Eliphas Levi , Rudolf Steiner .
Many literary writers and theorists have contributed to a wide range of views about the origins and purpose of the Book of Revelation .
His lasting contribution has been to show how much more meaningful prophets , such as the scribe of Revelation , are when treated as poets first and foremost .
Had he done so , he would have had to use their ( Hebrew ) poetry whereas he wanted to write his own .
It was a literal translation that had to comply with the warning at Revelation 22:18 that the text must not be corrupted in any way .
Later , the Ephesians claimed this fugitive had actually been the beloved disciple himself .
Torrey showed how the three major songs in Revelation each fall naturally into four regular metrical lines plus a coda .
Other dramatic moments in Revelation , such as 6 : 16 where the terrified people cry out to be hidden , behave in a similar way .
In her view , what Revelation has to teach is patience .
Her book , which is largely written in prose , frequently breaks into poetry or jubilation , much like Revelation itself .
Recently , aesthetic and literary modes of interpretation have developed , which focus on Revelation as a work of art and imagination , viewing the imagery as symbolic depictions of timeless truths and the victory of good over evil .
Her view that Revelation 's message is not gender-based has caused dissent .
D. H. Lawrence took an opposing , pessimistic view of Revelation in the final book he wrote , Apocalypse .
He saw the language which Revelation used as being bleak and destructive ; a ' death-product ' .
The other enemy he styled " vulgarity " and that was what he found in Revelation .
And nowhere does this happen so splendiferously than in Revelation . "
His specific aesthetic objections to Revelation were that its imagery was unnatural and that phrases like " the wrath of the Lamb " were " ridiculous " .
He saw Revelation as comprising two discordant halves .
He began his work , " The purpose of this book is to show that the Apocalypse is a manual of spiritual development and not , as conventionally interpreted , a cryptic history or prophecy " .
Thus , his letter ( written in the apocalyptic genre ) is pastoral in nature , and the symbolism of Revelation is to be understood entirely within its historical , literary and social context .
Thomas Jefferson omitted it along with most of the Biblical canon , from the Jefferson Bible , and wrote that at one time , he considered it as " merely the ravings of a maniac , no more worthy nor capable of explanation than the incoherences of our own nightly dreams . "
George Bernard Shaw described it as " a peculiar record of the visions of a drug addict " .
Martin Luther changed his perspective on Revelation over time .
John Calvin " had grave doubts about its value . "
There is much in Revelation which harnesses ancient sources .
Although the Old Testament provides the largest reservoir for such sources , it is not the only one .
They liked to intersperse their text of Revelation with the prophecy they thought was being promised fulfilment .
The message is that everything in Revelation will happen in its previously appointed time .
Perhaps significantly , Revelation chooses different sources than other New Testament books .
Yet , with Revelation , the problems might be judged more fundamental than this .
In trying to identify this something new , he argues that Ezekiel provides the ' backbone ' for Revelation .
He sets out a comparative table listing the chapters of Revelation in sequence then identifying against most of them the structurally corresponding chapter in Ezekiel .
John , on this theory , rearranges Ezekiel to suit his own purposes .
