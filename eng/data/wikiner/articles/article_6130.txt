In Latin the word idiota ( " ordinary person , layman " ) preceded the Late Latin meaning " uneducated or ignorant person . "
Its modern meaning and form dates back to Middle English around the year 1300 , from the Old French idiote ( " uneducated or ignorant person " ) .
Examples of such usage are William Faulkner 's The Sound and the Fury and William Wordsworth 's The Idiot Boy .
Nietzsche claimed , in his The Antichrist , that Jesus was an idiot .
This resulted from his description of Jesus as having an aversion toward the material world .
