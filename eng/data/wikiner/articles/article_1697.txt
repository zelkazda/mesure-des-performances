Burton Stephen " Burt " Lancaster ( November 2 , 1913 -- October 20 , 1994 ) was an American film actor and star , noted for his athletic physique , distinctive smile and , later , his willingness to play roles that went against his initial " tough guy " image .
Lancaster was nominated four times for Academy Awards and won once , for his work in Elmer Gantry in 1960 .
He also won a Golden Globe for that performance , and BAFTA Awards for The Birdman of Alcatraz ( 1962 ) and Atlantic City ( 1980 ) .
His production company , Hecht-Hill-Lancaster , was the most successful and innovative star-driven independent production company in 1950s Hollywood , making movies such as Marty ( 1955 ) , Trapeze ( 1956 ) , and Sweet Smell of Success ( 1957 ) .
Lancaster grew up in East Harlem and spent much of his time on the streets , where he developed great interest and skill in gymnastics while attending the DeWitt Clinton High School .
He also acted in theater productions and learned circus arts at Union Settlement , one of the city 's oldest settlement houses .
Later , he worked as a circus acrobat with childhood friend Nick Cravat -- who later appeared in nine films with Lancaster -- until an injury forced Lancaster to give up the circus .
Though initially unenthusiastic about acting , he returned from service , auditioned for a Broadway play and was offered a role .
( Hecht and Lancaster later formed several production companies in the 50 's to give Lancaster greater creative control . )
In two , The Flame and the Arrow and The Crimson Pirate , a friend from his circus years , Nick Cravat , played a leading role , and both actors impressed audiences with their acrobatic prowess .
This film is generally regarded as one of Lancaster 's favourite roles .
Lancaster made several films over the years with Kirk Douglas , including I Walk Alone ( 1948 ) , Gunfight at the OK Corral ( 1957 ) , The Devil 's Disciple ( 1959 ) , Seven Days in May ( 1964 ) , and Tough Guys ( 1986 ) , which fixed the notion of the pair as something of a team in the public imagination .
Douglas was always second-billed under Lancaster in these films , but with the exception of I Walk Alone , in which Douglas played a villain , their roles were usually more or less the same size .
During the later part of his career , Lancaster left adventure and acrobatic movies behind and portrayed more distinguished characters .
Lancaster sought demanding roles and , if he liked a part or a director , was prepared to work for much lower pay than he might have earned elsewhere ; he even helped to finance movies whose artistic value he believed in .
Lancaster vigorously guarded his private life .
He was romantically involved with Deborah Kerr during the filming of From Here to Eternity in 1953 .
He also had an affair with Joan Blondell .
Lancaster was a vocal supporter of liberal political causes , and frequently spoke out with support for racial minorities .
At one point , he was rumored to be a member of the Communist Party , because of his involvement in many liberal causes [ citation needed ] .
In 1968 , Lancaster actively supported the presidential candidacy of antiwar Senator Eugene McCarthy of Minn. , and frequently spoke on his behalf in the Democratic primaries .
In 1985 , Lancaster , a longtime supporter of gay rights , joined the fight against AIDS after his close friend , Rock Hudson , contracted the disease .
As Lancaster grew older , heart trouble increasingly hindered him from working .
Lancaster died in his Century City apartment in Los Angeles from a third heart attack on October 20 , 1994 .
