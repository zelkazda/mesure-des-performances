The Amazon , which has the largest drainage basin in the world , accounts for approximately one-fifth of the world 's total river flow .
In its upper stretches the Amazon river is called Apurímac ( in Peru ) and Solimões ( in Brazil ) .
During the wet season , parts of the Amazon exceed 190 kilometres ( 120 mi ) in width .
At no point is the Amazon crossed by bridges .
This is not because of its huge dimensions ; in fact , for most of its length , the Amazon 's width is well within the capability of modern engineers to bridge .
While the Amazon is the largest river in the world by most measures , the current consensus within the geographic community holds that the Amazon is the second longest river , just slightly shorter than the Nile .
However , some scientists , particularly from Brazil and Peru , dispute this ( see section below ) .
The Amazon Basin , the largest drainage basin in the world , covers about 40 percent of South America , an area of approximately 7050000 square kilometres ( 2720000 sq mi ) .
Its most remote sources are found on the inter-Andean plateau , just a short distance from the Pacific Ocean .
The Amazon River and its tributaries more than triples over the course of a year .
In an average dry season , 110000 square kilometres ( 42000 sq mi ) of land are water-covered , while in the wet season , the flooded area of the Amazon Basin rises to 350000 square kilometres ( 140000 sq mi ) .
The quantity of water released by the Amazon to the Atlantic Ocean is enormous : up to 300000 cubic metres ( 11000000 cu ft ) per second in the rainy season , with an average of 209,000 m³/s from 1973 to 1990 .
The Amazon is responsible for about 20 % of the total volume of freshwater entering the ocean .
There is a natural water union between the Amazon and the Orinoco basins , the so-called Casiquiare canal .
Actually the Casiquiare is a river distributary of the upper Orinoco , which flows southward into the Rio Negro , which in turn flows into the Amazon .
The Casiquiare is the largest river on the planet that links two major river systems , a so called bifurcation .
The Amazon river has a series of major river systems in Peru and Ecuador , some of which flow into the Marañón and Ucayali , others directly into the Amazon proper .
The most distant source of the Amazon was firmly established in 1996 , 2001 , and 2007 as a glacial stream on a snowcapped 5597 m ( 18363 ft ) peak called Nevado Mismi in the Peruvian Andes , roughly 160 km ( 99 mi ) west of Lake Titicaca and 700 km ( 430 mi ) southeast of Lima .
Soon thereafter the darkly colored waters of the Rio Negro meet the sandy colored Rio Solimões , and for over 6 km ( 4 mi ) these waters run side by side without mixing .
After the confluence of Río Apurímac and Ucayali , the river leaves Andean terrain and is instead surrounded by floodplain .
From this point to'the Marañón , some 1600 km ( 990 mi ) , the forested banks are just out of water , and are inundated long before the river attains its maximum flood-line .
The low river banks are interrupted by only a few hills , and the river enters the enormous Amazon Rainforest .
The river systems and flood plains in Brazil , Peru , Ecuador , Colombia and Venezuela whose waters drain into the Solimões and its tributaries are called the " Upper Amazon " .
The Amazon River proper runs mostly through Brazil and Peru , and it has tributaries reaching into Venezuela , Colombia , Ecuador , and Bolivia .
Not all of the Amazon 's tributaries flood at the same time of the year .
The rise of the Rio Negro starts in February or March , and it also begins to recede in June .
The Madeira rises and falls two months earlier than most of the rest of the Amazon .
The main river ( which is between approximately one and six miles ( 10 km ) wide ) is navigable for large ocean steamers to Manaus , 1500 kilometres ( 930 mi ) upriver from the mouth .
Smaller ocean vessels of 3,000 tons or 9,000 tons and 5.5 metres ( 18 ft ) draft can reach as far as Iquitos , Peru , 3600 kilometres ( 2200 mi ) from the sea .
At Óbidos , a bluff 17 m ( 56 ft ) above the river is backed by low hills .
The lower Amazon seems to have once been a gulf of the Atlantic Ocean , the waters of which washed the cliffs near Óbidos .
Only about 10 % of the water discharged by the Amazon enters the mighty stream downstream of Óbidos , very little of which is from the northern slope of the valley .
The drainage area of the Amazon Basin above Óbidos city is about 5 million square kilometres ( 2,000,000 sq mi ) , and , below , only about 1 million square kilometres ( 400,000 sq mi or around 20 % ) , exclusive of the 1.4 million square kilometres ( 540,000 sq mi ) of the Tocantins basin .
The definition of what exactly and how wide is the mouth of the Amazon is a matter of dispute , because of the area 's peculiar geography .
Most particularly , sometimes the Pará River is included , whereas sometimes it is just considered the independent lower reach of the Tocantins River .
The Pará river estuary alone is 60 km ( 37 mi ) wide .
If the Pará river and the Marajó island ocean frontage are included , the Amazon estuary is some 330 kilometres ( 210 mi ) wide .
By this criterion , the Amazon is wider at its mouth than the entire length of the River Thames in England .
The tension between the river 's strong push and the Atlantic tides causes a phenomenon called a tidal bore , a powerful tidal wave that flows rapidly inland from the sea up the Amazon mouth and nearby coastal rivers several times a year at high tide .
Tidal bores also occur in other river mouths around the world , but the Amazon 's are among the world 's highest and fastest , probably second only to those of Qiantang River in China .
In the Amazon , the phenomenon is locally known as the pororoca .
It starts with a very loud roar , constantly increasing , and advances at the rate of 15 -- 25 km/h ( 9 -- 16 mph ) , with a breaking wall of water 1.5 -- 4.0-metres ( 5 -- 13 ft ) high that may travel violently several kilometres up the Amazon and other rivers close to its mouth .
It is particularly intense in the rivers of the coast of the state of Amapá north of the mouth of the Amazon , such as the Araguari River , but can be observed in Pará rivers as well .
More than one-third of all species in the world live in the Amazon Rainforest , a giant tropical forest and river basin with an area that stretches more than 5.4 million square kilometres ( 2.1 million sq mi ) , and is the richest tropical forest in the world .
The Amazon River has over 3,000 recognized species of fish and that number is still growing .
The boto is the subject of a very famous legend in Brazil about a dolphin that turns into a man and seduces maidens by the riverside .
The tucuxi , also a dolphin species , is found both in the rivers of the Amazon Basin and in the coastal waters of South America .
The bull shark has been reported 4000 kilometres ( 2500 mi ) up the Amazon River at Iquitos in Peru .
The arapaima , known in Brazil as the pirarucu , is a South American tropical freshwater fish .
The anaconda snake is found in shallow waters in the Amazon Basin .
He was ordered to follow the Coca River and return when the river reached its confluence .
After 170 km , the Coca River joined the Napo River , and his men threatened to mutiny if he followed his orders and the expedition turned back .
In fact , true cinnamon is not native to South America .
Other related cinnamon-containing plants do occur and Orellana must have observed some of these .
Teixeira 's expedition was massive -- some 2000 people in 37 large canoes .
From 1648 to 1652 , António Raposo Tavares lead one of the longest known expeditions from São Paulo to the mouth of the Amazon , investigating many of its tributaries , including the Rio Negro , and covering a distance of more than 10000 km ( 6214 mi ) .
In what is currently Brazil , Ecuador , Bolivia , Colombia , Peru , and Venezuela , a number of colonial and religious settlements were established along the banks of primary rivers and tributaries for the purpose of trade , slaving and evangelization among the indigenous peoples of the vast rain forest .
Charles Marie de La Condamine accomplished the first scientific exploration of the Amazon River .
The Cabanagem , one of the bloodiest regional wars ever in Brazil , which was chiefly directed against the white ruling class , reduced the population of Pará from about 100,000 to 60,000 .
The first direct foreign trade with Manaus was commenced around 1874 .
In 1960 , the construction of the new capital city of Brasília in the interior also contributed to the opening up of the Amazon Basin .
Many settlements grew along the road from Brasilia to Belém , but rainforest soil proved difficult to cultivate .
Roads were cut through the forests , and in 1970 , the work on the Trans-Amazonian highway network began .
With a current population of 1.8 million people , Manaus is the Amazon 's largest city .
While debate as to whether the Amazon or the Nile is the world 's longest river has gone on for many years , the historic consensus of geographic authorities has been to regard the Amazon as the second longest river in the world , with the Nile being the longest .
However , the Amazon has been measured by different geographers as being anywhere between 6,259 and 6,800 kilometres ( 3,889 -- 4,225 mi ) long .
Using Nevado Mismi , which in 2001 was labeled by the National Geographic Society as the Amazon 's source , these scientists have made new calculations of the Amazon 's length .
The Amazon has over 1,100 tributaries in total , 17 of which are over 1500 kilometres ( 930 mi ) .
