Outside Italy , the club is often called Inter Milan .
Inter are also the reigning European champions .
At the international level , they have won 3 European Cup/Champions League ; first of all two back-to-back European Cups in 1964 and 1965 and then , after 45 years , in 2010 , completing an unprecedented ( for an italian team ) treble after winning in the same season the Coppa Italia and the Scudetto .
The club won also three UEFA Cups in 1991 , 1994 and 1998 , and two Intercontinental Cups in 1964 and 1965 .
The club was founded on 9 March 1908 as Football Club Internazionale Milano , following a " schism " from the Milan Cricket and Football Club ( 43 members ) .
The club won its very first Scudetto ( championship ) in 1910 and its second in 1920 .
After the end of World War II the club re-emerged under a name close to their original one , Internazionale FC Milano , which they have kept ever since .
Following the war , Internazionale won their sixth championship in 1953 and the seventh in 1954 .
During this period with Helenio Herrera as head coach , the club won three league championships in 1963 , 1965 and 1966 .
The most famous moments during this decade also include Inter 's two back-to-back European Cup wins .
Following the golden era of the 1960s , Inter managed to win their 11th league title in 1971 and their twelfth in 1980 .
Inter were defeated for the second time in five years in the final of the European Cup , going down 2 -- 0 to Johan Cruyff 's Ajax in 1972 .
[ citation needed ] € 19.5 million for Ronaldo from Barcelona in 1997 and € 31 million for Christian Vieri from Lazio in 1999 .
For Inter fans , it was difficult to identify who in particular might be to blame for these troubled times and this led to some icy relations between president , managers , and even some individual players .
In the 1999 -- 00 season , Massimo Moratti made some major changes , once again with some high-profile signings .
A major coup for Inter was the appointment of former Juventus manager Marcello Lippi .
Inter were seen by the majority of the fans and press to have finally put together a winning formula .
Once again they failed to win the elusive Scudetto .
However , they did manage to come close to their first domestic success since 1989 when they reached the Coppa Italia final only to be defeated by Lazio allowing them to win the Scudetto and domestic cup double .
Throughout this period , Inter suffered mockery from their neighbours Milan ; Milan were having a period of success both domestically and in Europe .
Marco Tardelli , chosen to replace Lippi , failed to improve results , and is remembered by Inter fans as the manager that lost this match .
Other members of the Inter squad during this period that suffered were the likes of Christian Vieri and Fabio Cannavaro , both of whom had their restaurants in Milan vandalised after defeats against Milan .
Although the signing of Ivan Cordoba from San Lorenzo , which was an squad improvement for the next years .
Inter fans ' protests throughout this period ranged from vandalism to banners being unfurled in the stadium to protest against certain players .
Inter were in this period often deemed to be one of the favourites for the championship .
However , a defeat would see Juventus , who were second , or even Roma , in third place , take the title from them , should these sides win .
As a result , some Lazio fans were actually openly supporting Inter during this match , as an Inter victory would prevent their bitter rivals Roma from winning the championship .
Inter were 2 -- 1 up after only 24 minutes .
Lazio equalised during first half injury time and then scored two more goals in the second half to clinch victory that eventually saw Juventus win the championship after their 2 -- 0 victory away to Udinese .
The date of this match -- 5 May 2002 -- still haunts Inter .
Although they drew on aggregate 1 -- 1 with Milan , Inter lost on the away goals rule , even though both matches were played in the same stadium .
However , once again Massimo Moratti 's impatience got the better of him , Hernán Crespo was sold after just one season , and Hector Cuper was fired after only a few games .
Alberto Zaccheroni stepped in , a life-long Inter fan but also the man who had been in charge of Lazio 's 4 -- 2 win over Inter in 2002 ; the fans were sceptical .
They were eliminated from the UEFA Champions League in the first round after finishing third in the group .
Furthermore , they only just managed to qualify for the Champions League by finishing in fourth place , only a point ahead of Parma .
Inter 's only saving grace in 2003 -- 04 was the arrival of Dejan Stanković and Adriano in January 2004 , both solid players that filled the gap left by the departures of Hernán Crespo and Clarence Seedorf .
On 11 May 2006 , Inter retained their Coppa Italia trophy by once again , defeating Roma with a 4 -- 1 aggregate victory .
The 5 -- 2 away win at Catania on 25 February 2007 broke the original record of 15 matches held by both Bayern Munich and Real Madrid from the " Big 5 " .
Inter 's form dipped a little as they recorded 0 -- 0 and 2 -- 2 draws against relegation-battlers Reggina and slumping to Palermo ( respectively ) , the latter game featuring a second-half comeback after Palermo went up 2 -- 0 at halftime .
They could not keep their invincible form up near the end of the season as well , as they lost their first game of the domestic season to Roma at the San Siro 3 -- 1 , thanks to two late Roma goals .
Italian World Cup winning defender Marco Materazzi scored both goals in the 18th and 60th minute , with the latter being a penalty .
After being eliminated by Liverpool in the Champions League , Mancini then announced his intention to leave his job , only to change his mind the following day .
Following this win , the club decided to sack Mancini on 29 May , citing his declarations following the Champions League defeat to Liverpool as the reason .
On 2 June , Inter announced on their official website that they had appointed former FC Porto and Chelsea boss José Mourinho as new head coach , with Giuseppe Baresi as his assistant .
This made Mourinho the only foreign coach in Italy in the 2008 -- 09 season kick-off .
Mourinho made only three additions to the squad during the summer transfer window of 2008 in the form of Mancini , Sulley Muntari , and Ricardo Quaresma .
In winning the league title for the fourth consecutive time , Inter joined Torino and Juventus as the only teams to do this and the first to accomplish this feat in the last 60 years .
Inter enjoyed more luck in the 2009 -- 10 Champions League , managing to progress to the quarter-finals by eliminating Mourinho 's former team Chelsea in a 3 -- 1 aggregate win ; this was the first time in three years that the Nerazzurri had passed the first knockout round .
Inter managed to achieve a 3 -- 1 win over incumbent champions Barcelona in the first leg of the semi-final .
They won the match 2 -- 0 thanks to two goals from Diego Milito , and were crowned champions of Europe .
Inter also won the 2009 -- 2010 Serie A title by two points over Roma , and the 2010 Coppa Italia by defeating the same side 1 -- 0 in the final. .
By comparison , Milan have been relegated twice .
The current president and owner of Internazionale is Massimo Moratti .
His father Angelo was the president of Inter during the club 's golden era of the 1960s .
The first design incorporated the letters ' FCIM ' in the center of a series of circles that formed the badge of the club .
Since its founding in 1908 , Inter have worn black and blue stripes .
For a period of time , however , Inter was forced to abandon their black and blue uniforms .
In 2008 , Inter celebrated their centenary with a red cross on their away shirt .
For the 2010 -- 11 season Inter 's away kit will feature the serpent .
3 -- Giacinto Facchetti , left back , 1960 -- 1978 ( posthumous honour ) .
Inter have had numerous presidents over the course of their history , some of which have been the owners of the club , others have been honorary presidents .
In Internazionale 's history , 55 coaches have coached the club .
The first manager was Virgilio Fossati .
Politically , the ultras of Inter are usually considered right-wing and they have good relationships with Lazio .
The other most significant rivalry is with Juventus ; the two participate in the Derby d'Italia .
Kit sponsors Nike and Pirelli contributed € 18.1m and € 9.3m respectively to commercial revenues , while broadcasting revenues were boosted € 1.6m ( 6 % ) by Champions League distribution .
This is predicted to result in lower broadcasting revenues for Inter , with smaller clubs gaining from the loss .
Inter 's matchday revenues amounted to only € 1.1m per home game , compared to € 2.6m among the top six earners .
