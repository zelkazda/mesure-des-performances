Other well-known novels include The Space Merchants ( written with Cyril M. Kornbluth ) and Gateway .
His writing also won him three Hugos and multiple Nebula Awards .
The family settled in Brooklyn when Pohl was around seven .
It was not until 2009 that he was awarded an honorary diploma from Brooklyn Tech .
Pohl has said that after the Molotov -- Ribbentrop Pact of 1939 , the party line changed and he could no longer support it , at which point he left .
Pohl has been married five times .
During 1948 , he married Judith Merril ; they divorced in 1952 .
Stories by Pohl often appeared in these magazines , but never under his own name .
For Pohl 's solo work , stories were credited to James MacCreigh
Pohl started a career as a literary agent in 1937 , but it was a sideline for him until after WWII , when he began doing it full time .
He ended up " representing more than half the successful writers in science fiction " -- for a short time , he was the only agent Isaac Asimov ever had -- though his agenting business went bankrupt in the early 1950s .
Pohl began publishing material under his own name in the early 1950s .
Pohl hired Judy-Lynn del Rey as his assistant editor at Galaxy and if .
Gateway also won the 1978 Hugo Award for Best Novel .
A novel begun by Arthur C. Clarke called The Last Theorem was finished by Pohl and published on August 5 , 2008 .
His works include not only science fiction but also articles for Playboy and Family Circle and nonfiction books .
For a time , he was the official authority for the Encyclopædia Britannica on the subject of Emperor Tiberius .
He was a frequent guest on Long John Nebel 's radio show , from the 1950s to the early 1970s , and an international lecturer .
He is a member of the all-male literary banqueting club the Trap Door Spiders , which served as the basis of Isaac Asimov 's fictional group of mystery solvers the Black Widowers .
