It is a Micro Focus company .
On May 7 , 2008 , the company announced it had sold the CodeGear subsidiary to Embarcadero Technologies for $ 23 million .
On May 6 , 2009 , the company announced it was to be acquired by Micro Focus for $ 75 million .
The transaction was approved by Borland shareholders on July 22 , 2009 , with Micro Focus acquiring the company for $ 1.50/share .
Following Micro Focus shareholder approval and the required corporate filings , the transaction was completed in late July 2009 .
Schroders was the lead investment banker .
Borland developed a series of well-regarded software development tools .
Its first product was Turbo Pascal , using the compiler developed by Anders Hejlsberg .
Adam Bosworth initiated and headed up the Quattro project until moving to Microsoft later in 1990 to take over the project which eventually became Access .
Turbo C was released on May 18 , 1987 and an estimated 100,000 copies were shipped in the first month of its release .
The Quattro Pro spreadsheet was launched in 1989 with , at the time , a notable improvement and charting capabilities .
Additionally , Borland was known for its practical and creative approach towards software piracy and intellectual property ( IP ) , introducing its " Borland no-nonsense license agreement " .
In September 1991 Borland purchased Ashton-Tate , bringing the dBase and InterBase databases to the house , in an all stock transaction .
Competition with Microsoft was fierce .
During the early 1990s Borland 's implementation of C and C++ outsold Microsoft 's .
Borland survived as a company , but no longer had the dominance in software tools that it once had .
The internal problems that arose with the Ashton-Tate merger were a large part of the fall .
A change in market conditions also contributed to Borland 's fall from prominence .
Borland had done an excellent job marketing to those with a highly technical bent .
This required new kinds of marketing and support materials from software vendors , but Borland remained focused on quality and software craftsmanship .
During 1993 Borland explored ties with WordPerfect as a possible way to form a suite of programs to rival Microsoft 's nascent integration strategy .
WordPerfect was then bought by Novell .
Philippe Kahn and the Borland board came to a disagreement on how to focus the company , and Philippe Kahn resigned as Chairman , CEO and President of Borland , a position he had held for 12 years , in January 1995 .
However , the parting was amicable as Kahn remained on the Borland board until November 7 , 1996 , when he resigned from that position .
Philippe Kahn co-founded Starfish Software in 1994 and pioneered wireless synchronization .
The company is now owned by Nokia .
Kahn then founded LightSurf in 1998 .
The Delphi 1 rapid application development ( RAD ) environment was launched in 1995 , under the leadership of Anders Hejlsberg .
On November 25 , 1996 , Del Yocam was hired as Borland CEO and Chairman .
On April 29 , 1998 , Borland refocused its efforts on targeting enterprise applications development , and went through a name change to Inprise Corporation .
For a number of years ( both before and during the Inprise name ) Borland suffered from serious financial losses and very poor public image .
When the name was changed to Inprise , many thought Borland had gone out of business .
Keith Gottfried served in senior executive positions with the company from 2000 to 2004 .
A proposed merger between Inprise and Corel was announced in February 2000 , aimed at producing Linux based products .
The scheme was abandoned when Corel 's shares fell and it became clear that there was really no strategic fit .
InterBase 6.0 was made available as an open source product in July 2000 .
In January 2001 , the Inprise name was abandoned and the company became " Borland " once more .
Kylix was launched in 2001 .
Plans to spin off the InterBase division as a separate company were abandoned after Borland and the people who were to run the new company could not agree on terms for the separation .
Borland stopped open source releases of InterBase and has developed and sold new versions at a fast pace .
C # Builder was released in 2003 as a native C # development tool , competing with Visual Studio .NET .
The latest releases of JBuilder and Delphi integrate these tools to give developers a broader set of tools for development .
Former CEO Dale Fuller was fired in July 2005 after a series of financial and commercial blunders , but remained on the board of directors .
On February 8 , 2006 , Borland announced the divestiture of their IDE division , including Delphi , JBuilder , and InterBase .
The new spinoff is called CodeGear .
On May 7 , 2008 , Borland announced the sale of CodeGear division to Embarcadero Technologies for an expected $ 23 million price and $ 7 million in CodeGear accounts receivables retained by Borland .
On May 6 , 2009 , the company announced it was to be acquired by Micro Focus for $ 75 million .
Borland 's current product line includes :
