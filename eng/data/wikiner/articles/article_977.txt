Together with Control Data Corporation ( who manufactured the hard drive part ) and Compaq Computer ( into whose systems these drives would initially go ) , they developed the connector , the signaling protocols , and so on with the goal of remaining software compatible with the existing ST-506 hard drive interface .
The first such drives appeared in Compaq PCs in 1986 .
Since the original ATA interface is essentially just a 16-bit ISA slot in disguise , the bridge was especially simple in case of an ATA connector being located on an ISA interface card . )
This relieved the mainboard and interface cards in the host computer of the chores of stepping the disk head arm , moving the head arm in and out , and so on , as had to be done with earlier ST-506 and ESDI hard drives .
It was included on the sound card because early business PCs did not include support for more than simple beeps from the internal speaker , and tuneful sound playback was considered unnecessary for early business software .
This second ATA interface on the sound card eventually evolved into the second motherboard ATA interface which was long included as a standard component in all PCs .
The BIOS in early PCs imposed smaller limits such as 8.46 GB , with a maximum of 1024 cylinders , 256 heads and 63 sectors , but this was not a limit imposed by the ATA interface .
However , some hard drives have a special setting called single for this configuration ( Western Digital , in particular ) .
In 2003 , the County of Los Angeles , California , US requested that , when possible , suppliers stop using the terms because the county found them unacceptable in light of its " cultural diversity and sensitivity " .
A device connected externally needs additional cable length to form a U-shaped bend so that the external device may be placed alongside , or on top of the computer case , and the standard cable length is too short to permit this .
USB is the most common external interface , followed by Firewire .
A bridge chip inside the external devices converts from the USB interface to PATA , and typically only supports a single external device without cable select or master/slave .
Also , standard-floppy disk drive emulation proved to be unsuitable for certain high-capacity floppy disk drives such as Iomega Zip drives .
