An OPS of .900 or higher in Major League Baseball puts the player in the upper echelon of hitters .
As a point of reference , the OPS for all of Major League Baseball in 2008 was .749 .
The popularity of OPS gradually spread , and by 2004 it began appearing on Topps baseball cards .
The top ten Major League Baseball players in lifetime OPS , with at least 3,000 plate appearances through 2009 ( active players in bold )
Albert Pujols has the highest career OPS for a right-handed batter .
The top ten single-season performances in MLB are ( all left-handed hitters ) :
The highest single-season mark for a right-handed hitter was 1.2449 by Rogers Hornsby in ( 1925 ) , ( 13th on the all-time list ) .
Since 1925 , the highest single-season OPS for a right-hander is 1.2224 by Mark McGwire in ( 1998 ) , which is good for 16th all-time .
Babe Ruth , 207 2 .
Ted Williams , 191 3 .
Barry Bonds , 182 4 .
Lou Gehrig , 179 5 .
Rogers Hornsby , 175 6 .
Mickey Mantle , 172 6 .
Albert Pujols , 172 8 .
Dan Brouthers , 170 8 .
Joe Jackson , 170 10 .
Ty Cobb , 167 11 .
Jimmie Foxx , 163
The only purely right-handed batters to appear on this list are Hornsby , Pujols , and Foxx .
Mantle is the only switch-hitter in the group .
This would also eliminate the only right-handed batter in the list , Barnes .
