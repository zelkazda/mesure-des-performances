Assembly languages and some low-level languages , such as BCPL , generally lack support for data structures .
Many high-level programming languages , on the other hand , have special syntax or other built-in support for certain data structures , such as vectors ( one-dimensional arrays ) in the C programming language , multi-dimensional arrays in Pascal , linked lists in Common Lisp , and hash tables in Perl and in Python .
Examples are the C++ Standard Template Library , the Java Collections Framework , and Microsoft 's .NET Framework .
Object-oriented programming languages , such as C++ , .NET Framework and Java , use classes for this purpose .
