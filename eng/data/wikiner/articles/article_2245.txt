To avoid problems arising from the ambiguity , the PRC government never uses this character in official documents , but uses 万亿 ( wànyì ) instead .
( the ROC government in Taiwan uses 兆 ( zhào ) to mean 10 12 in official documents . )
