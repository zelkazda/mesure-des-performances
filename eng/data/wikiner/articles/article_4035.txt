The ballet Coppelia is based on two other stories that Hoffmann wrote .
Hoffmann 's ancestors , both maternal and paternal , were jurists .
Hoffmann was to regret his estrangement from his father .
Although she died when he was only three years old , he treasured her memory and embroidered stories about her to such an extent that later biographers sometimes assumed her to be imaginary , until proofs of her existence were found after World War II .
The provincial setting was not , however , conducive to technical progress , and despite his many-sided talents he remained relatively ignorant , both of classical forms and of the new artistic ideas that were then developing in Germany .
Around 1787 he became friends with Theodor Gottlieb von Hippel the Younger ( 1775-1843 ) , the son of a pastor , and nephew of Theodor Gottlieb von Hippel the Elder , the well-known writer friend of Immanuel Kant .
During February 1796 , her family protested against his attentions , and , with his faltering consent , they asked another of his uncles to arrange employment for him in Glogau ( Głogów ) , Prussian Silesia .
After passing further examinations he visited Dresden , where he was amazed by the paintings in the gallery , particularly those of Correggio and Raphael .
During the summer of 1798 his uncle was promoted to a court in Berlin , and the three of them moved there during August -- Hoffmann 's first residence in a large city .
By the time the latter responded , Hoffmann had passed his third round of examinations and had already left for Posen ( Poznań ) in South Prussia in the company of his old friend Hippel , with a brief stop in Dresden to show him the gallery .
It was immediately deduced who had drawn them , and complaints were made to authorities in Berlin , who were reluctant to punish the promising young official .
The problem was solved by " promoting " Hoffmann to Płock in New East Prussia , the former capital of Poland ( 1079-1138 ) where administrative offices were relocated from Thorn ( Toruń ) .
They moved to Płock during August 1802 .
Hoffmann despaired because of his exile , and drew caricatures of himself drowning in mud alongside ragged villagers .
One of his tasks was to devise surnames for Jews .
At the beginning of 1804 he obtained a post at Warsaw .
He was never to return to Königsberg .
In Warsaw he found the same atmosphere he had enjoyed in Berlin , renewing his friendship with Zacharias Werner , and meeting his future biographer , a neighbour and fellow jurist called Julius Eduard Itzig ( who changed his name to Hitzig after his baptism ) .
He moved in the circles of August Wilhelm Schlegel , Adelbert von Chamisso , Friedrich de la Motte Fouqué , Rahel Levin , and David Ferdinand Koreff .
As they refused to grant him a passport to Vienna , he was forced to return to Berlin .
He visited his family in Posen before arriving in Berlin on 18 June 1807 , hoping to further his career there as an artist and writer .
The next fifteen months were some of the worst in Hoffmann 's life .
The city of Berlin was also occupied by Napoleon 's troops .
On 1 September 1808 he arrived with his wife in Bamberg , where he began a job as theatre manager .
He began work as music critic for the Allgemeine musikalische Zeitung , a newspaper in Leipzig , and his articles on Beethoven were especially well received , and highly regarded by the composer himself .
Prussia had declared war against France on 16 March during the War of the Sixth Coalition , and their journey was fraught with difficulties .
That same day Hoffmann was surprised to meet Hippel , whom he had not seen for nine years .
During the day , Hoffmann would roam , watching the fighting with curiosity .
Finally , on 20 May , they left for Leipzig , only to be involved in an accident which killed one of the passengers in their coach and injured his wife .
On 4 June an armistice began , which allowed the company to return to Dresden .
But on 22 August , after the end of the armistice , the family was forced to relocate from their pleasant house in the suburbs into the town , and during the next few days the Battle of Dresden raged .
After a long period of continued disturbance the town surrendered on 11 November , and on 9 December the company travelled to Leipzig .
When asked to accompany them on their journey to Dresden in April , he refused , and they left without him .
But during July his friend Hippel visited , and soon he found himself being guided back into his old career as a jurist .
During the period from 1819 Hoffmann involved with legal disputes , while fighting ill health .
Prince Klemens Wenzel von Metternich 's anti-liberal crusades began to put Hoffmann in situations that tested his conscience .
These ended when Hoffmann 's illness was found to be life-threatening .
His story Der Sandmann ( " The Sandman " , 1816 ) similarly inspired Léo Delibes 's ballet Coppélia ( 1870 ) .
Yet Hoffmann 's stories , as with the majority of his literary work , also have philosophical themes .
It is through stories that Hoffmann expresses his aesthetic , ethical and political concerns .
Moreover , the original Hoffmann stories ( including The Nutcracker ) often have dark themes .
Hoffmann also influenced 19th century musical opinion directly through his music criticism .
Hoffmann strove for artistic polymathy .
His masterpiece novel Lebensansichten des Katers Murr deals with such issues as the aesthetic status of ' true ' artistry and the modes of self-transcendence that accompany any genuine endeavour to create .
The self-conscious effort to impress must , according to Hoffmann , be divorced from the self-aware effort to create .
