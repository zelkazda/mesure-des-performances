The first documented instance of hop cultivation was in 736 , in the Hallertau region of present-day Germany , although the first mention of the use of hops in brewing in that country was 1079 .
Not until the thirteenth century in Germany did hops begin to start threatening the use of gruit for flavoring .
In Britain , hopped beer was first imported from Holland around 1400 ; however , hops were condemned in 1519 as a " wicked and pernicious weed " .
It was another century before hop cultivation began in the present-day U.S. in 1629 .
The principal production centres in the UK are in Kent and Worcestershire .
For example , many of those hop picking in Kent , a hop region first mechanised in the 1960s , were Eastenders .
In Kent , the numbers of hop-pickers who came down from the city meant that many growers issued their own currency to those doing the labor .
Sonoma County in California was , pre-mechanization , a major US producer of hops .
The remnants of this significant hop industry are still noticeable in the form of old hop kilns that survive in Sonoma County .
Tom 's of Maine deodorant uses hops for its antibacterial activity .
