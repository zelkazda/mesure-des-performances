Apostolic succession is to be distinguished from the Petrine supremacy .
Also noteworthy is that the Apostle Paul , though given spiritual authority directly by Christ , did not embark on his apostleship without conferring with those who were apostles before him as he notes in his Epistle to the Galatians .
These churches hold that Christ entrusted the leadership of the community of believers , and the obligation to transmit and preserve the " deposit of faith " to the apostles , and the apostles passed on this role by ordaining bishops after them .
The Eastern Orthodox generally recognize Roman Catholic orders , but have a different concept of the apostolic succession as it exists outside of Eastern Orthodoxy .
The validity of a priest 's ordination is decided by each autocephalic Orthodox church .
As such , apostolic succession is a foundational doctrine of authority in the Catholic Church .
These churches generally hold that Jesus Christ founded a community of believers and selected the apostles to serve , as a group , as the leadership of that community .
On June 29 , 2007 the Congregation for the Doctrine of the Faith , under the prefecture of Cardinal William Levada , explained why apostolic succession is important to the Catholic Church .
Roman Catholic theology holds that the apostolic succession effects the power and authority to administer the sacraments except for baptism and matrimony .
The bishop , of course , must be from an unbroken line of bishops stemming from the original apostles selected by Jesus Christ .
He says that Catholic theologians today would point to Vatican II 's declaration that apostolic succession is " by divine institution " .
There are the differing interpretations offered by the Congregation for the Doctrine of the Faith on the one side , and by many Catholic theologians on the other .
( see Orthodox Patriarch of Jerusalem )
At first the Church of England continued to adhere to the doctrinal and liturgical norms of the Catholic Church .
Wide variations exist within Lutheranism on this issue .
By this document the full communion between the Evangelical Lutheran Church in America and the Episcopal Church was established .
Luther 's reform movement , however , usually did not as a rule abrogate the ecclesiastic office of Bishop .
In light of Wesley 's episcopal consecration , the Methodist Church can lay a claim on apostolic succession , as understood in the traditional sense .
Protestants denied this and asserted that the traditional definition of apostolic succession was not revealed in the Bible , but was formulated later by the post-apostolic church .
One reason often given for traditional apostolic succession is the need for institutional continuity so that Christian doctrine , not only the written texts an important consideration ) but also their proper orthodox interpretation , could be better maintained .
Furthermore , they claim that in the Bible there 's no evidence showing that the office must be conveyed by laying on of hands and no Biblical command that it must be by a special class of bishops ( the laying on of hands is repeatedly used to give a commission to some person in scripture , however ; for example , it was done to St. Paul before his missions work ; St. Paul also instructed St. Timothy to not be hasty in laying on hands , which may be interpreted as giving authority ) .
