Bamberg is a town in Bavaria , Germany .
It is located in Upper Franconia on the river Regnitz , close to its confluence with the river Main .
Bamberg is home to nearly 7,000 foreign nationals , including over 4,100 members of the United States Army and their dependents .
The town , first mentioned in 902 , grew up by the castle which gave its name to the Babenberg family .
On their extinction it passed to the Saxon house .
The church was enriched with gifts from the pope , and Henry II had it dedicated in honor of him .
The emperor and his wife Cunigunde gave large temporal possessions to the new diocese , and it received many privileges out of which grew the secular power of the bishop .
Henry and Cunigunde were both buried in the cathedral .
In 1248 and 1260 the see obtained large portions of the estates of the Counts of Meran , partly through purchase and partly through the appropriation of extinguished fiefs .
By the changes resulting from the Reformation , the territory of this see was reduced nearly one half in extent .
Bambrzy ( Ger .
In 1759 , the possessions and jurisdictions of the diocese situated in Austria were sold to that state .
Bamberg thus lost its independence in 1802 , becoming part of Bavaria in 1803 .
Bamberg was first connected to the German rail system in 1844 , which has been an important part of its infrastructure ever since .
In February 1926 Bamberg served as the venue for the famous Bamberg Conference , convened by Adolf Hitler in his attempt to foster unity and to stifle dissent within the young NSDAP .
Bamberg is located in Franconia , 63 km ( 39 mi ) north of Nuremberg by railway and 101 km ( 63 mi ) east of Würzburg , also by rail .
It is situated on the Regnitz river , 3 km ( 1.9 mi ) before it flows into the Main river .
Bamberg extends over seven hills , each crowned by a beautiful church .
It was founded in 1004 by the emperor Henry II , finished in 1012 and consecrated on May 6 , 1012 .
Of its many historic works of art may be mentioned the magnificent marble tomb of the founder and his wife , considered the masterpiece of the sculptor Tilman Riemenschneider , and carved between 1499 and 1513 .
Another treasure of the cathedral is an equestrian statue known as the Bamberg Horseman .
The Altenburg is located at the highest of Bamberg 's seven hills .
Between 1251 and 1553 it was the residence of Bamberg 's bishops .
His friend , the famous German writer E.T.A. Hoffmann , who was very impressed by the building , lived there for a while .
The Altenburg serves as a high-class restaurant and has a beautiful view .
The royal lyceum , formerly a Jesuit college , contains notable collections and the royal library of over 300,000 volumes .
The University of Bamberg , named Otto-Friedrich University , offers higher education in the areas of social science , business studies and the humanities , and is attended by more than 9300 students .
The InterCityExpress main line # 28 ( Munich -- Nuremberg -- Leipzig -- Berlin / Hamburg ) runs through Bamberg .
To Munich the train journey takes less than two hours .
Bamberg is connected to other towns in eastern Upper Franconia such as Bayreuth , Coburg , and Kronach , with usually at least an hourly regional service .
Connections to the west are hourly regional trains to Würzburg , which is fully connected to the ICE network .
Tourists arriving at Frankfurt International Airport will have to change trains in Würzburg to get to Bamberg or take a detour via Nuremberg .
But it is nevertheless well connected to the network : the A70 from Schweinfurt ( connecting to the A7 there ) to Bayreuth ( connecting to the A9 ) runs along the northern edge of the town .
The A73 on the eastern side of town connects Bamberg to Nuremberg ( connecting to the A9 ) and Thuringia , ending at Suhl .
Most international tourists who travel by plane arrive at Frankfurt International Airport or Munich Airport .
The nearest bigger airport is Nuremberg Airport which can be reached within half an hour by car or one hour by train and subway .
The Rhine-Main-Danube Canal begins near Bamberg .
Local transport within Bamberg relies exclusively on buses .
Bamberg is an independent city .
Bamberg is twinned with :
