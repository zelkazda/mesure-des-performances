There is no defined line between a language and a dialect , but the linguist Max Weinreich is credited as saying that " a language is a dialect with an army and a navy " .
Constructed languages such as Esperanto , programming languages , and various mathematical formalisms are not necessarily restricted to the properties shared by human languages .
His famous example of this is using ancient Egypt and looking at the ways they built themselves out of media with very different properties stone and papyrus .
