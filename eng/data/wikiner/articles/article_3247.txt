Doris Day ( born April 3 , 1922 ) is an American actress , singer , and animal rights activist .
Day 's entertainment career began in her late teens as a big band singer .
In 1945 she had her first hit recording , " Sentimental Journey " , and , in 1948 , appeared in her first film , Romance on the High Seas .
As of 2009 , Day was the top-ranking female box office star of all time and ranked sixth among the top ten box office performers ( male and female ) .
Specifically she states , " [ I was ] named by my mother in honor of her favorite actress , Doris Kenyon , a silent screen star of that year 1924 . "
Day was married four times .
Day developed an early interest in dance , and in the mid-1930s formed a dance duo that performed locally in Cincinnati .
While recovering , Day took singing lessons , and at 17 she began performing locally .
This song is still associated with Day , and was rerecorded by her on several occasions , as well as being included in her 1971 television special .
Her popularity as a radio performer and vocalist , which included a second hit record My Dreams Are Getting Better All the Time , led directly to a career in films .
Her personal circumstances at the time and her reluctance to perform contributed to an emotive performance of Embraceable You , which greatly impressed Styne and his partner , Sammy Cahn .
They then recommended her for a role in Romance on the High Seas which they were working on for Warner Brothers .
The withdrawal of Betty Hutton due to pregnancy left the main role to be re-cast , and Day got the part .
She continued to make minor and frequently nostalgic period musicals such as Starlift , On Moonlight Bay , By the Light of the Silvery Moon , and Tea For Two for Warner Brothers .
In 1953 Day appeared as Calamity Jane , winning the Academy Award for Best Original Song for Secret Love ( her recording of which became her fourth U.S .
After filming Young at Heart ( 1954 ) with Frank Sinatra , Day chose not to renew her contract with Warner Brothers .
Day subsequently took on more dramatic roles , including her 1954 portrayal of singer Ruth Etting in Love Me or Leave Me .
Day would later call it , in her autobiography , her best film .
She was also paired with such top stars as Jack Lemmon , James Stewart , Cary Grant , David Niven , and Clark Gable .
After recording the number , she reportedly told a friend of Livingston , " That 's the last time you 'll ever hear that song " .
However , the song was used again in Please Do n't Eat the Daisies ( 1960 ) , and was reprised as a brief duet with Arthur Godfrey in The Glass Bottom Boat ( 1966 ) .
In 1959 , Day entered her most successful phase as a film actress with a series of romantic comedies , starting with Pillow Talk , co-starring Rock Hudson , who became a lifelong friend .
Day received a nomination for an Academy Award for Best Actress .
Day also teamed up with James Garner , starting with 1963 's The Thrill of It All , followed later that year by Move Over , Darling .
The film was suspended following the firing of Monroe and her subsequent death .
A year later , it was renamed and recast with Day as the lead character .
Critics and comics dubbed Day " the world 's oldest virgin " and audiences began to shy away from her repetitive roles .
In her published memoirs , Day said that she had rejected the part on moral grounds .
Her final feature film , With Six You Get Eggroll , was released in 1968 .
Day 's last major hit single came in the UK in 1964 with " Move Over , Darling " , co-written by her son specifically for her .
The recording was a notable departure for Day , with its distinctly contemporary-sounding arrangement .
In 1967 , Day recorded her last album , The Love Album , essentially concluding her recording career , though this album was not released until 1995 .
Melcher died April 20 , 1968 .
In April 1979 , he filed a suit to set aside the $ 6 million settlement with Day and recover damages from everybody involved in agreeing to the payment supposedly without his permission .
He also named Day as a co-defendant , describing her as an " unwilling , involuntary plaintiff whose consent can not be obtained " .
[ citation needed ] Terry Melcher commented that it was only Martin Melcher 's premature death that saved Day from financial ruin .
Day stated publicly that she believes Melcher innocent of any deliberate wrongdoing , stating that Melcher " simply trusted the wrong person " until it was too late .
According to Day 's autobiography , as told to A. E. Hotchner , the usually athletic and healthy Melcher had an enlarged heart .
Author David Kaufman asserts that one of Day 's costars , actor Louis Jourdan , maintained that Day herself disliked her husband , but Day 's statements regarding her relationship with Melcher contradict that assertion .
Upon her husband 's death on April 20 , 1968 , Day learned that he had committed her to a television series , which became The Doris Day Show .
Day hated the idea of doing television , but felt obligated .
Day grudgingly persevered ( she needed the work to help pay off her debts ) , but only after CBS ceded creative control to her and her son .
The show was successful , enjoyed a five-year run , and functioned as a curtain-raiser for The Carol Burnett Show .
It was not as widely syndicated as many of its contemporaries were , and has been little seen outside the United States and the United Kingdom .
Recently Day has participated in telephone interviews with a radio station that celebrates her birthday with an annual Doris Day music marathon .
After her autobiography was published , Day was married one more time ; this marriage also ended in divorce .
While promoting the book , Day caused a stir by rejecting the " girl next door " and " virgin " labels so often attached to her .
As she remarked in her book , " The succession of cheerful , period musicals I made , plus Oscar Levant 's highly publicized comment about my virginity ( ' I knew Doris Day before she became a virgin . ' )
Day said she believed people should live together prior to marriage , something that she herself would do if the opportunity arose .
At the conclusion of this book tour , Day seemed content to focus on her charity and pet work and her business interests .
Her son Terry Melcher first made a brief attempt to become a surf music singing star , then became a staff producer for Columbia Records in the 1960s , and was famous for producing some latter-day recordings by The Beach Boys and The Byrds .
Day later wrote she was wracked with guilt and loneliness .
