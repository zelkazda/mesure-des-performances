9/11 Commission reported that the original plan for the September 11 attacks called for the hijacking of ten planes , one of which was to be crashed into Empire State Building .
The building and its street floor interior are designated landmarks of the New York City Landmarks Preservation Commission , and confirmed by the New York City Board of Estimate .
In 2007 , it was ranked number one on the List of America 's Favorite Architecture according to the AIA .
The Empire State Building is the third tallest skyscraper in the Americas ( after the Willis Tower and Trump International Hotel and Tower both in Chicago ) , and the 15th tallest in the world .
The Empire State building is currently undergoing a $ 550 million renovation , with $ 120 million utilized in an effort to transform the building into a more energy efficient and eco-friendly structure .
John W. Bowser was project construction superintendent .
Governor Smith 's grandchildren cut the ribbon on May 1 , 1931 .
The construction was part of an intense competition in New York for the title of " world 's tallest building " .
Two other projects fighting for the title , 40 Wall Street and the Chrysler Building , were still under construction when work began on the Empire State Building .
Each held the title for less than a year , as the Empire State Building surpassed them upon its completion , just 410 days after construction commenced .
The Empire State Building rises to 1250 ft ( 381 m ) at the 102nd floor , and including the 203 ft ( 62 m ) pinnacle , its full height reaches 1,453 ft -- 8 9 ⁄ 16 in ( 443.09 m ) .
The Empire State Building was the first building to have more than 100 floors .
It has a total floor area of 2768591 sq ft ( 257211 m 2 ) ; the base of the Empire State Building is about 2 acres ( 8094 m 2 ) .
As of 2007 , approximately 21,000 employees work in the building each day , making the Empire State Building the second-largest single office complex in America , after the Pentagon .
Its original 64 elevators are located in a central core ; today , the Empire State Building has 73 elevators in all , including service elevators .
The Empire State Building cost $ 40,948,900 to build .
After the eightieth birthday and subsequent death of Frank Sinatra , for example , the building was bathed in blue light to represent the singer 's nickname " Ol ' Blue Eyes " .
After the death of actress Fay Wray ( King Kong ) in late 2004 , the building stood in complete darkness for 15 minutes .
The floodlights bathed the building in red , white , and blue for several months after the destruction of the World Trade Center , then reverted to the standard schedule .
This would also be shown after the Westminster Dog Show .
Traditionally , in addition to the standard schedule , the building will be lit in the colors of New York 's sports teams on the nights they have home games ( orange , blue and white for the New York Knicks , red , white and blue for the New York Rangers , and so on ) .
The building is illuminated in tennis-ball yellow during the US Open tennis tournament in late August and early September .
It was twice lit in scarlet to support nearby Rutgers University : once for a football game against the University of Louisville on November 9 , 2006 , and again on April 3 , 2007 when the women 's basketball team played in the national championship game .
In 1995 , the building was lit up in blue , red , green and yellow for the release of Microsoft 's Windows 95 operating system , which was launched with a $ 300 million campaign .
The building has also been known to be illuminated in purple and white in honor of graduating students from New York University .
Every year in September , the building is lit in black , red , and yellow , with the top lights off ( for black ) to celebrate the German-American Steuben Parade on Fifth Avenue .
In December 2007 , the building was lit yellow to signify the home video release of The Simpsons Movie .
Starting in 2008 , the building along with New York City and many other cities around the world , participated in Earth Hour .
In September 2009 , the building was lit for one night in orange colors , in celebration of the exploration of Manhattan Island by Henry Hudson 400 years earlier .
The building 's vacancy was exacerbated by its poor location on 34th Street , which placed it relatively far from public transportation , as Grand Central Terminal , the Port Authority Bus Terminal , and Penn Station are all several blocks away .
Other more successful skyscrapers , such as the Chrysler Building , do not have this problem .
Elevator operator Betty Lou Oliver survived a plunge of 75 stories inside an elevator , which still stands as the Guinness World Record for the longest survived elevator fall recorded .
The Empire State Building remained the tallest man-made structure in the world for 23 years before it was surpassed by the Griffin Television Tower Oklahoma ( KWTV Mast ) in 1954 .
It was also the tallest free-standing structure in the world for 36 years before it was surpassed by the Ostankino Tower in 1967 .
One World Trade Center , currently under construction in New York City , is expected to exceed the height of the Empire State Building upon completion .
It will be 1,776 feet ( 541 m ) tall , becoming the tallest building in the city , the country and the Americas .
The Empire State Building has one of the most popular outdoor observatories in the world , having been visited by over 110 million people .
The skyscraper 's observation deck plays host to several cinematic , television , and literary classics including , An Affair To Remember , Love Affair and Sleepless in Seattle .
The Empire State Building also has a motion simulator attraction , located on the 2nd floor .
After September 11th , however , the ride was closed , and an updated version debuted in mid-2002 with actor Kevin Bacon as the pilot .
The new version of the narration attempted to make the attraction more educational , and included some minor post-9/11 patriotic undertones with retrospective footage of the World Trade Center .
When the World Trade Center was being constructed , it caused serious problems for the television stations , most of which then moved to the World Trade Center as soon as it was completed .
As of 2009 , the Empire State Building is home to the following stations :
The record time is 9 minutes and 33 seconds , achieved by Australian professional cyclist Paul Crake in 2003 , at a climbing rate of 6593 ft ( 2010 m ) per hour .
