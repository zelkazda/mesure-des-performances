The Cybermen had replaced much of their bodies with mechanical prostheses and were now supposedly emotionless creatures driven only by logic .
Isaac Asimov 's short story " The Bicentennial Man " explored cybernetic concepts .
Asimov also explored the idea of the cyborg in relation to robots in his short story " Segregationist " , collected in The Complete Robot .
The 1972 science fiction novel Cyborg , by Martin Caidin , told the story of a man whose damaged body parts are replaced by mechanical devices ( " bionics " ) .
In 1974 , Marvel Comics writer Rich Buckler introduced the cyborg Deathlok the Demolisher , and a dystopian post-apocalyptic future , in Astonishing Tales # 25 .
Buckler 's character dealt with rebellion and loyalty , with allusion to Frankenstein 's monster , in a twelve-issue run .
Deathlok was later resurrected in Captain America , followed by two others in 1990 and 1999 .
The 1982 film Blade Runner featured creatures called replicants , bio-engineered or bio-robotic beings .
The Nexus series -- genetically designed by the Tyrell Corporation -- are virtually identical to an adult human , but have superior strength , agility , and variable intelligence depending on the model .
A derogatory term for a replicant is " skin-job, " a term heard again extensively in Battlestar Galactica .
The 1987 science fiction action film RoboCop features a cyborg protagonist .
There are cyborg kaiju in the Godzilla films such as Gigan and Mechagodzilla .
Although frequently referred to onscreen as a cyborg , The Terminator might be more properly an android .
The endoskeletons beneath are fully functional robots and have been seen operating independently , especially during the future segments of the Terminator movies .
The cyborg ninja suit has been donned by multiple characters , most recently by the character Raiden in Metal Gear Solid 4 : Guns of the Patriots .
One of the most famous cyborgs is Darth Vader from the Star Wars films .
A direct brain-to-computer interface is a valuable , but expensive , luxury in Larry Niven and Jerry Pournelle 's novel Oath of Fealty .
