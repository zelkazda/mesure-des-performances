Harwich is a town on Cape Cod , in Barnstable County in the state of Massachusetts in the United States .
Barnstable County is coextensive with Cape Cod .
The town is a popular vacation spot , located near the Cape Cod National Seashore .
Harwich has 3 active harbors .
Harwich was first settled in 1670 as the eastern parish of the town of Yarmouth .
The town was officially incorporated in 1694 , and originally included the lands of the current town of Brewster .
According to the United States Census Bureau , the town has a total area of 33.2 square miles ( 85.9 km² ) , of which , 21.0 square miles ( 54.5 km² ) of it is land and 12.1 square miles ( 31.4 km² ) of it ( 36.53 % ) is water .
Harwich is on the southern side of Cape Cod , just west of the southeastern corner .
It is bordered by Brewster to the north , Orleans and Chatham to the east , Nantucket Sound to the south , and Dennis to the west .
Harwich is approximately twelve miles east of Barnstable , twenty-eight miles east of the Cape Cod Canal , thirty-five miles south of Provincetown , and eighty miles southeast of Boston .
On the national level , Harwich is a part of Massachusetts 's 10th congressional district , and is currently represented by Bill Delahunt .
The junior ( Class I ) Senator , elected in a special election on January 19 , 2010 , is Scott Brown .
Harwich is governed by the open town meeting form of government , led by a town administrator and a board of selectmen .
There are post offices , in Harwich Center , Harwich Port , South Harwich , West Harwich , and East Harwich .
Harwich operates its own school system for its approximately 1,400 students .
Harwich is known for its excellent boys basketball , girls basketball , girls field hockey and baseball teams .
Two of Cape Cod 's major east-west routes , U.S. Route 6 and Route 28 , cross the town .
A portion of the Cape Cod Rail Trail , as well as several other bicycle routes , are in town .
In recent years parts of Cape Cod have introduced bus service , especially during the summer to help cut down on traffic .
