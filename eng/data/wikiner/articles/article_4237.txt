A number of flutes dating to about 40,000 to 35,000 years ago have been found in the Swabian Alb region of Germany .
The oldest flute ever discovered may be a fragment of the femur of a juvenile cave bear , with two to four holes , found at Divje Babe in Slovenia and dated to about 43,000 years ago .
In 2008 another flute dated back to at least 35,000 years ago was discovered in Hohle Fels cave near Ulm , Germany .
The researchers involved in the discovery officially published their findings in the journal Nature , in August 2009 .
The flute , one of several found , was found in the Hohle Fels cavern next to the Venus of Hohle Fels and a short distance from the oldest known human carving .
A three-holed flute , 18.7 cm long , made from a mammoth tusk was discovered in 2004 , and two flutes made from swan bones excavated a decade earlier ( from the same cave in Germany , dated to circa 36,000 years ago ) are among the oldest known musical instruments .
The earliest extant transverse flute is a chi ( 篪 ) flute discovered in the Tomb of Marquis Yi of Zeng at the Suizhou site , Hubei province , China .
It dates from 433 BC , of the later Zhou Dynasty .
The size and placement of tone holes , the key mechanism , and the fingering system used to produce the notes in the flute 's range were evolved from 1832 to 1847 by Theobald Boehm , and greatly improved the instrument 's dynamic range and intonation over those of its predecessors .
This technique was introduced by T. R. Mahalingam in the mid-20th century .
There are many varieties of di with different sizes , structures ( with or without resonance membrane ) and number of holes ( from 6 to 11 ) and intonations ( playing in different keys ) in China .
The bamboo flute playing vertically is called " xiao " ( 簫 ) which is a different category of wind instrument in China
