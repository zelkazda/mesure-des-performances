The most prominent defenders of this hypothesis are Duesberg himself , biochemist and vitamin proponent David Rasnick , and journalist Celia Farber .
The majority of the scientific community considers that Duesberg 's arguments are the result of cherry-picking predominantly outdated scientific data and selectively ignoring evidence in favour of HIV 's role in AIDS .
Duesberg argues that there is a statistical correlation between trends in recreational drug use and trends in AIDS cases .
Duesberg 's claim that recreational drug use , rather than HIV , was the cause of AIDS has been specifically examined and found to be false .
Duesberg 's claim that antiviral medication causes AIDS is regarded as disproven by the scientific community .
Several studies have specifically addressed Duesberg 's claim that recreational drug abuse or sexual promiscuity were responsible for the manifestations of AIDS .
Duesberg argued in 1989 that a significant number of AIDS victims had died without proof of HIV infection .
Since AIDS is now defined partially by the presence of HIV , Duesberg claims it is impossible by definition to offer evidence that AIDS does n't require HIV .
Robert Gallo suggested that Duesberg should infect himself with HIV if he insists on maintaining his stance that HIV is harmless .
Duesberg claims that he would do so , but that it would be impossible for him to receive the required ethical approval of the U.S. National Institutes of Health and the university that employs him to work with HIV .
Peter Duesberg argues that retroviruses like HIV must be harmless to survive : they do not kill cells and they do not cause cancer , he maintains .
Duesberg elsewhere states that " the typical virus reproduces by entering a living cell and commandeering the cell 's resources in order to make new virus particles , a process that ends with the disintegration of the dead cell " .
Duesberg also rejects the involvement of retroviruses and other viruses in cancer .
To him , virus-associated cancers are " freak accidents of nature " that do not warrant research programs such as the War on Cancer .
Duesberg claims that the supposedly innocuous nature of all retroviruses is supported by what he considers to be their normal mode of proliferation : infection from mother to child in utero .
Duesberg does not suggest that HIV is an endogenous retrovirus , a virus integrated into the germ line and genetically heritable :
In the December 9 , 1994 issue of Science , Duesberg 's methods and claims were evaluated .
The vast majority of people with AIDS have never received antiretroviral drugs , including those in developed countries prior to the licensure of AZT ( zidovudine ) in 1987 , and people in developing countries today where very few individuals have access to these medications .
