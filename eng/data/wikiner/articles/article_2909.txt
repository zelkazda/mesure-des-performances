The word " corporation " derives from corpus , the Latin word for body , or a " body of people . "
Many European nations chartered corporations to lead colonial ventures , such as the Dutch East India Company or the Hudson 's Bay Company , and these corporations came to play a large part in the history of corporate colonialism .
Investors in the VOC were issued paper certificates as proof of share ownership , and were able to trade their shares on the original Amsterdam stock exchange .
By 1611 , shareholders in the East India Company were earning an almost 150 % return on their investment .
In the United States , government chartering began to fall out of vogue in the mid-1800s .
Many private firms in the 19th century avoided the corporate model for these reasons ( Andrew Carnegie formed his steel operation as a limited partnership , and John D. Rockefeller set up Standard Oil as a trust ) .
N.J. was the first state to adopt an " enabling " corporate law , with the goal of attracting more business to the state .
Delaware followed , and soon became known as the most corporation-friendly state in the country after New Jersey raised taxes on the corporations , driving them out .
New Jersey reduced these taxes after this mistake was realized , but by then it was too late ; even today , most major public corporations are set up under Delaware law .
The Corporation as a whole was labeled an " artificial person, " possessing both individuality and immortality .
According to Lord Chancellor Haldane ,
When no stockholders exist , a corporation may exist as a non-stock corporation ( in the United Kingdom , a " company limited by guarantee " ) and instead of having stockholders , the corporation has members who have the right to vote on its operations .
However , each of these listed in the Articles of Incorporation are members of the Corporation .
In some jurisdictions , such as Germany , the control of the corporation is divided into two tiers with a supervisory board which elects a managing board .
Germany is also unique in having a system known as co-determination in which half of the supervisory board consists of representatives of the employees .
Historically , some corporations were named after their membership : for instance , " The President and Fellows of Harvard College . "
This requirement generally applies in Europe , but not in the U.S. , except for publicly traded corporations , where financial disclosure is required for investor protection .
Some jurisdictions ( Washington , D.C. , for example ) separate corporations into for-profit and non-profit , as opposed to dividing into stock and non-stock .
The institution most often referenced by the word " corporation " is a publicly-traded or publicly traded corporation , the shares of which are traded on a public stock exchange ( e.g. , the New York Stock Exchange or Nasdaq in the United States ) where shares of stock of corporations are bought and sold by and to the general public .
The main difference in most countries is that publicly traded corporations have the burden of complying with additional securities laws , which ( especially in the U.S. ) may require additional periodic disclosure ( with more stringent requirements ) , stricter corporate governance standards , and additional procedural obligations in connection with major corporate transactions ( e.g. mergers ) or events ( e.g. elections of directors ) .
Most U.S. businesses are closely held corporations .
A mutual benefit nonprofit corporation is a corporation formed in the United States solely for the benefit of its members .
In Canada both the federal government and the provinces have corporate statutes , and thus a corporation may have a provincial or a federal charter .
The BBC is the oldest and best known corporation within the UK that is still in existence .
Others , such as the British Steel Corporation , were privatized in the 1980s .
In the United Kingdom , ' corporation ' can also refer to a corporation sole , which is an office held by an individual natural person , and has a legal entity separate from that person .
Several types of conventional corporations exist in the United States .
American corporations can be either profit-making companies or non-profit entities .
The process is called " incorporation, " referring to the abstract concept of clothing the entity with a " veil " of artificial personhood ( embodying , or " corporating " it , ' corpus ' being the Latin word for ' body ' ) .
Companies set up for privacy or asset protection often incorporate in Nevada , which does not require disclosure of share ownership .
Harvard College ( a component of Harvard University ) , formally the President and Fellows of Harvard College ( also known as the Harvard Corporation ) , is the oldest corporation in the western hemisphere .
Founded in 1636 , the second of Harvard 's two governing boards was incorporated by the Great and General Court of Massachusetts in 1650 .
Many nations have modeled their own corporate laws on American business law .
One solution to this ( as in the case of the Australian and UK tax systems ) is for the recipient of the dividend to be entitled to a tax credit which addresses the fact that the profits represented by the dividend have already been taxed .
In other systems , dividends are taxed at a lower rate than other income ( e.g. in the US ) or shareholders are taxed directly on the corporation 's profits and dividends are not taxed .
