The proprietary limited company is a statutory business form in several countries , including Australia .
In the United States , corporations are generally incorporated , or organized , under the laws of a particular state .
Americans in the 1790s knew of a variety of corporations established for various purposes , including those of commerce , education , and religion .
As theorists such as Ronald Coase have pointed out , all business organizations represent an attempt to avoid certain costs associated with doing business .
