As with London and N.Y. , Dublin has a number of dialects which differ significantly based on class and age group .
These are roughly divided into three categories : " local Dublin " , or the broad-working class dialect ; " mainstream Dublin " , the typical accent spoken by middle-class or suburban speakers ; and " new Dublin " , an accent among younger people ( born after 1970 ) .
Local Dublin is often non-rhotic , although some variants may be variably or very lightly rhotic .
When describing less astonishing or significant events , a structure resembling the German perfect can be seen :
In addition , in some areas in Leinster , north Connacht and parts of Ulster , the hybrid word ye-s , pronounced " yis " , may be used .
The pronunciation does differ however , with that of the northwestern being [ jiːz ] and the Leinster pronunciation being [ jɪz ] .
In parts of Connacht and Ulster however the verb mitch is often replaced by the verb scheme .
Words such as " fry " are used in large towns such as Sligo .
Also more prevalent in Cork is a profligation of colourful emphasis-words ; in general any turn of phrase associated with a superlative action is used to mean very , and are often calculated to express these in a negative light and therefore often unpleasant by implication -- " he 's a howling / thundering / rampaging / galloping / screeching langer , so he is . "
The word is also used at the end of sentences ( primarily in Munster ) , for instance " I was only here five minutes ago , sure ! "
