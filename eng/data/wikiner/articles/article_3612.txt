This position is essentially Ryle 's , who argued that a failure to acknowledge the distinction between knowledge that and knowledge how leads to vicious regresses .
In recent times , some epistemologists ( Sosa , Greco , Kvanvig , Zagzebski ) have argued that epistemology should evaluate people 's properties ( i.e. , intellectual virtues ) and not just the properties of propositions or propositional mental attitudes .
Edmund Gettier is remembered for his 1963 argument which called into question the theory of knowledge that had been dominant among philosophers for thousands of years .
In a few pages , Gettier argued that there are situations in which one 's belief may be justified and true , yet fail to count as knowledge .
That is , Gettier contended that while justified belief in a true proposition is necessary for that proposition to be known , it is not sufficient .
According to Gettier , there are certain circumstances in which one does not have knowledge , even when all of the above conditions are met .
Gettier proposed two thought experiments , which have come to be known as " Gettier cases, " as counterexamples to the classical account of knowledge .
It is far from clear that Gettier was the first to present this challenge to the justified-true-belief triumvirate .
Some scholars attribute an extremely similar idea to Bertrand Russell .
The responses to Gettier have been varied .
Nozick further claims this condition addresses a case of the sort described by D. M. Armstrong : A father believes his son innocent of committing a particular crime , both because of faith in his son and ( now ) because he has seen presented in the courtroom a conclusive demonstration of his son 's innocence .
Timothy Williamson has advanced a theory of knowledge according to which knowledge is not justified true belief plus some extra condition ( s ) .
René Descartes , prominent philosopher and supporter of internalism wrote that , since the only method by which we perceive the external world is through our senses , and that , since the senses are not infallible , we should not consider our concept of knowledge to be infallible .
Though Descartes could doubt his senses , his body and the world around him , he could not deny his own existence , because he was able to doubt and must exist in order to do so .
However from this Descartes did not go as far as to define what he was .
Kant held that all mathematical propositions are synthetic .
The constructivist point of view is pragmatic ; as Vico said : " The norm of the truth is to have made it . "
Susan Haack is the philosopher who conceived it , and it is meant to be a unification of foundationalism and coherentism .
Whereas , say , infinists regard the regress of reasons as " shaped " like a single line , Susan Haack has argued that it is more like a crossword puzzle , with multiple lines mutually supporting each other .
