Fritz Lang himself was baptized on 28 December 1890 at the Schottenkirche in Vienna .
His mother took this conversion seriously and was dedicated to raising Fritz as a Catholic .
Lang never had an interest in his Jewish heritage and identified himself as Catholic .
Although he was not a particularly devout Catholic , he " regularly used Catholic images and themes in [ ] his films " .
After finishing school , Lang briefly attended the Technical University of Vienna , where he studied civil engineering and eventually switched to art .
In 1920 , he met his future wife , the writer and actress Thea von Harbou .
Although some consider Lang 's work to be simple melodrama , he produced a coherent oeuvre that helped to establish the characteristics of film noir , with its recurring themes of psychological conflict , paranoia , fate and moral ambiguity .
His work influenced filmmakers as disparate as Jacques Rivette and William Friedkin .
Adolf Hitler came to power in January 1933 , and by March 30 , the new regime banned it as an incitement to public disorder .
No evidence has been discovered in any of Goebbels 's writings to affirm the suggestion that he was planning to offer Lang any position .
Jean-Luc Godard 's film Contempt ( 1963 ) , in which Lang appeared as himself , presents a bare outline of the story as fact .
after his marriage to Thea von Harbou , who stayed behind , ended in 1933 .
This was Lang 's only film in French .
Upon his arrival in Hollywood in 1936, [ citation needed ] Lang joined the MGM studio and directed the crime drama Fury .
Lang made twenty-one features in the next twenty-one years , working in a variety of genres at every major studio in Hollywood , occasionally producing his films as an independent .
One of his most famous film noirs is the police drama The Big Heat ( 1953 ) , noted for its uncompromising brutality , especially for a scene in which Lee Marvin throws scalding coffee on Gloria Grahame 's face .
Lang was approaching blindness during the production , making it his final project .
