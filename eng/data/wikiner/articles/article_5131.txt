Even the rebellion by Adam and Eve has been seen as a prefiguration of original sin .
In Hebrew the book is called Bereishit , meaning " originally . "
This title is the first word of the Hebrew text -- a method by which all five books of the Torah are named .
This was in line with the Septuagint use of subject themes as book names .
Abraham dwells in the land as a sojourner , as does his son Isaac and his grandson Jacob , whose name is changed to Israel .
God forms Adam " from the dust of the ground ... and man became a living being .
The man names his wife Eve , " because she was the mother of all living " .
Adam and Eve had two sons , Cain and Abel .
Cain was a farmer and Abel was a shepherd .
Seth is born to replace Abel .
God reveals himself to Abram , tenth in descent from Noah and twentieth from Adam , and instructs him to travel to the land which Canaan has forfeited .
Abraham protests that it is not just " to slay the righteous with the wicked, " and asks if the whole city can be spared if even ten righteous men are found there .
God tests Abraham by commanding that he sacrifice Isaac .
She gives birth to twins , the elder of whom is Pharez , ancestor of the future royal house of David .
Also worthy of note are the Samaritan and Syriac translations .
While these manuscripts are not as old as the Dead Sea Scrolls and the Septuagint , they have noteworthy differences .
In 1929 Albrecht Alt proposed that the Hebrews arrived in Canaan at different times and as different groups , each with its nameless " gods of the fathers . "
According to Alt , the theology of the earliest period and of later fully developed monotheistic Judaism were nevertheless identical : both Yahweh and the tribal gods revealed himself/themselves to the patriarchs , promised them descendants , and protected them in their wanderings ; they in turn enjoyed a special relationship with their god , worshiped him , and established holy places in his honor .
In 1962 Frank Moore Cross concluded that the name Yahweh developed as one of the many epithets of El : " El the creator , he who causes to be . "
To judge from the names of Abraham 's relations and the cult of his home town , his ancestors at least were moon-god worshippers .
Whether he continued to honour this gods [ sic ] identifying him with El , or converted to El , is unclear . "
The covenants are integral to the understanding not only of Genesis but also of the entire Bible .
Otto Eissfeldt , an early scholar of the Ugarit texts , recognised that in Ugarit the promise of a son was given to kings together with promises of blessing and numerous descendants , a clear parallel to the pattern of Genesis .
Westermann saw the promise of a son in Genesis 16:11 and 18:1-15 as genuine , as well as the promise of land behind 15:7-21 and 28:13-15 ; the rest he saw as representing later editors .
It is clearly called a covenant , with a certain obligation on Noah and certain promises from the Lord .
Genesis 15 and Genesis 17 .
The Lord has contracted this covenant with Abraham with strong emphasis on the promise ( especially in Gen. 17 ) .
It is obvious , e.g. from the book of Exodus , that the promise of a large offspring is regarded as fulfilled ( cf. Exodus 1:7-22 ) .
This covenant is seen as significant throughout the entire Old Testament .
