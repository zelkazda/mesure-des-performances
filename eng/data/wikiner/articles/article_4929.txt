The philosopher Thomas Hobbes figured that people were rational animals and thus saw submission to a government dominated by a sovereign as preferable to anarchy .
According to Hobbes , people in a community create and submit to government for the purpose of establishing for themselves , safety and public order .
He ( in the voice of Socrates ) asked if the purpose of government was to help one 's friends and hurt one 's enemies , for example .
Many centuries later , John Locke addressed the question of abuse of power by writing on the importance of checks and balances to prevent or at least constrain abuse .
It is believed that Thomas Jefferson was influenced by John Locke .
Social contract theorists , such as Thomas Hobbes and Jean-Jacques Rosseau , believe that governments reduce people 's freedom/rights in exchange for protecting them , and maintaining order .
Other statist theorists , like David Hume , reject social contract theory on the grounds that , in reality , consent is not involved in state-individual relationships and instead offer different definitions of legitimacy based on practicality and usefulness .
