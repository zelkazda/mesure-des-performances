The government embarked upon a series of economic reform programs supported by the World Bank and IMF beginning in the late 1980s .
The government failed to meet the conditions of the first four IMF programs .
Cameroon has an investment guaranty agreement and a bilateral accord with the United States .
