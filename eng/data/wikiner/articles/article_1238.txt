It is considered one of Beethoven 's masterpieces and one of the greatest musical compositions ever written .
They were taken from the " Ode to Joy " , a poem written by Friedrich Schiller in 1785 and revised in 1803 , with additions made by the composer .
The Philharmonic Society of London originally commissioned the symphony in 1817 .
Beethoven started the work in 1818 and finished early in 1824 .
However , both the words and notes of the symphony have sources dating from earlier in Beethoven 's career .
The title of Schiller 's poem " An die Freude " is literally translated as " To Joy " , but is normally called the " Ode to Joy " .
Beethoven had made plans to set this poem to music as far back as 1793 , when he was 22 years old .
Beethoven 's sketchbooks show that bits of musical material that ultimately appeared in the symphony were written in 1811 , and 1817 .
In addition , the symphony also emerged from other pieces by Beethoven that , while completed works in their own right , are also in some sense sketches for the future symphony .
As in the Ninth Symphony , the vocal forces sing a theme first played instrumentally , and this theme is highly reminiscent of the corresponding theme in the Ninth Symphony ( for a detailed comparison , see Choral Fantasy ) .
The introduction for the vocal part of the symphony caused many difficulties for Beethoven .
Beethoven 's friend Anton Schindler , later said : " When he started working on the fourth movement the struggle began as never before .
The aim was to find an appropriate way of introducing Schiller 's ode .
One day he [ Beethoven ] entered the room and shouted ' I got it , I just got it ! '
Then he showed me a sketchbook with the words ' let us sing the ode of the immortal Schiller ' " .
When his friends and financiers heard this , they urged him to premiere the symphony in Vienna .
The soprano and alto parts were interpreted by two famous young singers : Henriette Sontag and Caroline Unger .
So this time , he instructed the singers and musicians to ignore the totally deaf Beethoven .
At the beginning of every part , Beethoven , who sat by the stage , gave the tempos .
In any case , Beethoven was not to blame , as violinist Josef Böhm recalled : " Beethoven directed the piece himself ; that is , he stood before the lectern and gesticulated furiously .
When the audience applauded -- testimonies differ over whether at the end of the scherzo or the whole symphony -- Beethoven was several measures off and still conducting .
Because of that , the contralto Caroline Unger walked over and turned Beethoven around to accept the audience 's cheers and applause .
The whole audience acclaimed him through standing ovations five times ; there were handkerchiefs in the air , hats , raised hands , so that Beethoven , who could not hear the applause , could at least see the ovation gestures .
Beethoven left the concert deeply moved .
In 1997 Bärenreiter published an edition by Jonathan Del Mar .
These are by far the largest forces needed for any Beethoven symphony ; at the premiere , Beethoven augmented them further by assigning two players to each wind part .
Haydn , too , had used this arrangement in a number of works .
The opening theme , played pianissimo over string tremolos , so much resembles the sound of an orchestra tuning that many commentators have suggested that was Beethoven 's inspiration .
At times during the piece Beethoven directs that the beat should be one downbeat every three bars , perhaps because of the very fast pace of the majority of the movement which is written in triple time , with the direction ritmo di tre battute ( " rhythm of three bars " ) , and one beat every four bars with the direction ritmo di quattro battute ( " rhythm of four bars " ) .
Beethoven had been criticised before for failing to adhere to standard form for his compositions .
Beethoven wrote this piece in triple time , but it is punctuated in a way that , when coupled with the speed of the metre , makes it sound as though it is in quadruple time .
American pianist and music author Charles Rosen has characterized it as a symphony within a symphony , the view which will be followed below .
It is important to note that many other writers have interpreted its form in different terms , including two of the greatest analysts of the twentieth century , Heinrich Schenker and Donald Tovey .
In Rosen 's view , it contains four movements played without interruption .
This " inner symphony " follows the same overall pattern as the Ninth Symphony as a whole .
Words written by Beethoven ( not Schiller ) are shown in italics .
Wagner had to decide which instrumental lines in the original had to be omitted since the pianist can not play all the orchestral parts , thus giving his reduction a personal signature .
An important theme in the finale of Johannes Brahms ' Symphony No. 1 in C minor is related to the " Ode to Joy " theme from the last movement of Beethoven 's Ninth symphony .
When this was pointed out to Brahms , he is reputed to have retorted " Any ass can see that !
Beethoven 's Ninth Symphony was an influence on the development of the compact disc .
Philips , the company that had started the work on the new audio format , originally planned for a CD to have a diameter of 11.5 cm , the width of the then popular compact cassette , while Sony planned a 10 cm diameter , even more compact but enough for one hour of music .
The 1951 performance of the Ninth Symphony by Furtwängler was brought forward as the perfect excuse for the change .
Like much of Beethoven 's later music , his Ninth Symphony is demanding for all the performers , including the choir and soloists .
As with all of his symphonies , Beethoven has provided his own metronome markings for the Ninth Symphony , and as with all of his metronome markings , there is controversy among conductors regarding the degree to which they should be followed .
Historically , conductors have tended to take a slower tempo than Beethoven marked for the slow movement , and a faster tempo for the military march section of the finale .
For example , since the modern orchestra has larger string sections than in Beethoven 's time , Mahler doubled various wind and brass parts to preserve the balance between strings on the one hand and winds and brass on the other .
Beethoven 's writing for horns and trumpets throughout the symphony ( mostly the 2nd horn and 2nd trumpet ) is often altered by performers to avoid large leaps ( those of a 12th or more ) .
Wilhelm Furtwängler conducted the Berlin Philharmonic on April 19 , 1942 , on the eve of Hitler 's 53rd birthday .
The London Philharmonic Choir débuted on 15 May 1947 performing the Ninth Symphony with the London Philharmonic Orchestra under the baton of Victor De Sabata at the Royal Albert Hall .
This version has been used by NBC News for The Huntley-Brinkley Report ( the second movement played over the closing credits ) .
The first stereo recording of the Ninth Symphony was by Ferenc Fricsay conducting the Berlin Philharmonic in 1958 .
He made his second recording of the piece with the Vienna Philharmonic for Deutsche Grammophon , in 1979 .
Daniel Barenboim , who had recorded the work twice before , conducted the West-Eastern Divan in concert in Berlin on 27 August 2006 .
Roger Norrington conducting the London Classical Players recorded it with period instruments for a 1987 release by EMI Records .
Sir John Eliot Gardiner recorded his period-instrument version of the Ninth Symphony , conducting his Monteverdi Choir and Orchestre Révolutionnaire et Romantique in 1992 .
It was first released by Deutsche Grammophon in 1994 on their early music Archiv Produktion label as part of his complete cycle of the Beethoven symphonies .
An additional period-instrument recording by Christopher Hogwood and the Academy of Ancient Music was released in 1997 under the label Éditions de l'Oiseau-Lyre .
Franz Liszt arranged the whole symphony for piano , and that arrangement has been recorded by Konstantin Scherbakov .
David Zinman 's 1997 recording with the Zürich Tonhalle Orchestra was a modern instrument recording that used the Baerenreiter edition edited by Jonathan Del Mar .
In 1985 , the European Union chose Beethoven 's music as the EU anthem .
When Kosovo declared independence in 2008 , it lacked an anthem , so for the independence ceremonies it used Ode to Joy , in recognition of the European Union 's role in its independence .
Additionally , the Ode to Joy was adopted as the national anthem of Rhodesia in 1974 as Rise O Voices of Rhodesia .
