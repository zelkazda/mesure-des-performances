The bildungsroman ( German pronunciation : [ ˈbɪldʊŋs.ʁoˌmaːn ] ; German : " formation novel " ) is a genre of the novel which focuses on the psychological and moral growth of the protagonist from youth to adulthood .
One of the first books of this type is Hayy ibn Yaqdhan by Ibn Tufail ( 1100s ) .
