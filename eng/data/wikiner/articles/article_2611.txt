Interest in the Cottingley Fairies gradually declined after 1921 .
Today , the photographs and two of the cameras used are on display in the National Media Museum in Bradford .
Kodak declined to issue a certificate of authenticity .
The prints were also examined by another photographic company , Ilford , who reported unequivocally that there was " some evidence of faking " .
Margaret McMillan , the educational and social reformer , wrote : " How wonderful that to these dear children such a wonderful gift has been vouchsafed . "
The novelist Henry de Vere Stacpoole decided to take the fairy photographs and the girls at face value .
Public interest in the Cottingley Fairies gradually subsided after 1921 .
In 1978 , the magician and scientific skeptic James Randi and a team from the Committee for the Scientific Investigation of Claims of the Paranormal examined the photographs , using a " computer enhancement process " .
In 1983 , the cousins admitted in an article published in the magazine The Unexplained that the photographs had been faked , although both maintained that they really had seen fairies .
The 1997 films Fairy Tale : A True Story , and Photographing Fairies , were inspired by the events surrounding the Cottingley Fairies .
