Under the name King Yannai , he appears as a wicked tyrant in the Talmud , reflecting his conflict with the Pharisee party .
During the twenty-seven year reign of Alexander Jannaeus , he was almost constantly involved in military conflict .
This support was particularly crucial during the war with Ptolemy Lathyrus ( discussed later ) .
This act of cannibalism was used to terrify the Judean people and their military .
It is clear that a strong rift existed between the Pharisees and Alexander Jannaeus .
The crowd responded with shock at his mockery and showed their displeasure by pelting Alexander with the etrogim ( citrons ) that they were holding in their hands .
The soldiers slew more than 6,000 people in the Temple courtyard .
