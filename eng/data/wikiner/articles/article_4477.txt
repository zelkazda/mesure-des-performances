Flåklypa Grand Prix is a Norwegian stop motion -- animated feature film directed by Ivo Caprino .
It was released in 1975 and is based on characters from a series of books by Norwegian cartoonist and author Kjell Aukrust .
Pinchcliffe Grand Prix ) .
The film is heavily inspired by the birthplace of Kjell Aukrust 's father , Lom .
The Flåklypa-mountain is a stylized version of a real mountain , where , ironically enough , the valley underneath it is named Flåklypa .
When it came out in 1975 , Flåklypa Grand Prix was an enormous success in Norway , selling 1 million tickets in its first year of release .
The movie was shown in cinemas every day of the week for 28 years , from 1975 until 2003 -- mainly in Norway , Moscow and Tokyo .
The lead designer was Joe Dever .
The game got ported to Nintendo DS in 2010 .
The movie inspired a young Christian von Koenigsegg to create the Koenigsegg CC .
The podrace sequence in the Star Wars prequel The Phantom Menace is said to be inspired by the race in this movie .
