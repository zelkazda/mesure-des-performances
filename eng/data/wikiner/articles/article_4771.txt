Both the Catholic and Jewish traditions have even set aside days for gambling , , although religious authorities generally disapprove of gambling .
The involvement of governments , through regulation and taxation , has led to a close connection between many governments and gaming organizations , where legal gambling provides significant government revenue , such as in Monaco or Macau .
Gambling has been legal in Nev. since 1931 , forming the backbone of the state 's economy , and the city of Las Vegas is perhaps the best known gambling destination in the world .
In addition many bookmakers offer fixed odds on a number of non-sports related outcomes , for example the direction and extent of movement of various financial indices , the winner of television competitions such as Big Brother , and election results .
For example , millions of Britons play the football pools every week .
