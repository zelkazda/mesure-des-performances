He was born in Oviedo in 759 or 760 .
He was put under the guardianship of his aunt Adosinda after his father 's death , but one tradition relates his being put in the monastery of Samos .
He was the governor of the palace during the reign of Adosinda 's husband Silo .
On Silo 's death , he was elected king by Adosinda 's allies , but the magnates raised his uncle Mauregatus to the throne instead .
Alfonso fled to Álava to live with his maternal relatives .
Alfonso was subsequently elected king on 14 September 791 .
What is known is that he maintained contact with the court of Charlemagne .
He sent delegations to Aachen in 796 , 797 , and 798 , but we do not know the purposes .
He took Lisbon in 798 .
Alfonso also moved the capital from Pravia , where Silo had located it , to Oviedo , the city of his father 's founding and his birth .
Tradition relates that in 814 , the body of Saint James the Greater was discoverred in Compostela and that Alfonso was the first pilgrim to that famous medieval ( and modern ) shrine .
