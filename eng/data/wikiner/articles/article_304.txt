The school deals primarily with the premodern world ( before the French Revolution ) , with little interest in later topics .
Prominent leaders include co-founders Marc Bloch ( 1886 -- 1944 ) and Lucien Febvre ( 1878 -- 1956 ) .
Bloch was highly interdisciplinary , influenced by the geography of Paul Vidal de la Blache ( 1845 -- 1918 ) and the sociology of Émile Durkheim ( 1858 -- 1917 ) .
It was during this time that he mentored Fernand Braudel , who would become one of the best-known exponents of this school .
Bloch was not concerned with the effectiveness of the royal touch -- he acted instead like an anthropologist in asking why people believed it and how it shaped relations between king and commoner .
Fernand Braudel became the leader of the second generation after 1945 .
Braudel 's followers admired his use of the longue durée approach to stress slow , and often imperceptible effects of space , climate and technology on the actions of human beings in the past .
