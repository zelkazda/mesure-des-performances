The Finnish Defence Forces ( Finnish : puolustusvoimat , Swedish : försvarsmakten ) are responsible for the defence of Finland .
The Finnish Defence Forces are under the command of the Chief of Defence ( currently General Ari Puheloinen ) , who is directly subordinate to the President of the Republic in matters related to the military command .
In the beginning of January 2008 , the Finnish Army organization was overhauled .
The military training of the reservists is primarily the duty of the Defence Forces , but it is assisted by the National Defence Training Association of Finland .
In the training , most of the instructors are volunteers , but when Defence Forces materiel is used , the training always takes place under the direct supervision of career military personnel .
In addition , the Defence Forces support the voluntary training by providing instructors and giving logistical support .
On the other hand , the Defence Forces may request the association to run specialized courses for personnel placed in reserve units .
The Finnish defence forces is based on a universal male conscription .
The inhabitants of the demilitarized Åland islands are exempt from military service .
The non-military service of Åland islands has not been arranged since the introduction of the act , and there are no plans to institute it .
The inhabitants of Åland islands can also volunteer for military service on the mainland .
Also exempt from military service are the Jehovah 's Witnesses .
Only during the Crimean War was the allotment system reintroduced .
The Military of the Grand Duchy of Finland consisted of eight conscripted sharp-shooter battalions and existed from 1881 until 1903 .
The language of official business was Swedish .
On January 25 , 1918 the White Guard were declared to be the official troops of the white government .
In February 1919 the White Guard separated from the armed forces and became an independent organisation .
Peace terms in the Continuation War included disbanding the White Guard .
Also certain weapons such as guided missiles , submarines , proximity mines , torpedo boats , bombers with internal bomb racks and any weapons of German origin were forbidden .
The force strength restrictions were interpreted to mean the peace-time strength of the Defence Forces and a large reserve was trained .
In the late 1980s , the mobilization strength of Finnish Defence Forces was around 700,000 .
After the second world war , the Finnish Defence Forces relied largely on war-time material .
This resulted in the commissioning of several new weapons systems and strengthening the defence of Finnish Lapland by establishing new garrisons there .
During the 1970s and 1980s , the Defence Forces capabilities were developed from this basis .
The Finnish military doctrine is based on the concept of total defence .
The Border Guard has the responsibility for border security in all situations .
One of the projected uses for the Border Guard is guerrilla warfare in areas temporarily occupied by enemy .
