Arcturus is the brightest star in the constellation Boötes .
With a visual magnitude of −0.05 , it is also the third brightest star in the night sky , after Sirius and Canopus .
It is , however , fainter than the combined light of the two main components of Alpha Centauri , which are too close together for the eye to resolve as separate sources of light , making Arcturus appear to be the fourth brightest .
As one of the brightest stars in the sky , Arcturus has been significant to observers since antiquity .
In Ancient Greece , the star 's celestial activity was supposed to portend tempestuous weather .
Traveling east and north they eventually crossed the equator and reached the latitude at which Arcturus would appear directly overhead in the summer night sky .
If Hōkūleʻa could be kept directly overhead , they landed on the southeastern shores of the Big Island of Hawaiʻi .
For a return trip to Tahiti the navigators could use Sirius , the zenith star of that island .
As one of the brightest stars in the sky , Arcturus has been the subject of a number of studies in the emerging field of asteroseismology .
High precision photometry from the Hipparcos satellite 's observations showed Arcturus is now known to be slightly variable , by about 0.04 magnitudes over 8.3 days .
In the case of Arcturus , this was an interesting discovery as it is known that the redder a giant gets , the more variable it will be .
Extreme cases like Mira undergo large swings over hundreds of days ; Arcturus is not very red and is a borderline case between variability and stability with its short period and tiny range .
This is not too uncommon in red giants , but Arcturus has a particularly strong case of the phenomenon .
Arcturus is notable for its high proper motion , larger than any first magnitude star in the stellar neighborhood other than α Centauri .
Arcturus is thought to be an old disk star , and appears to be moving with a group of 52 other such stars .
Arcturus is likely to be considerably older than the Sun , and much like what the Sun will be in its red giant phase .
Hipparcos also suggested that Arcturus is a binary star , with the companion about twenty times dimmer than the primary and orbiting close enough to be at the very limits of our current ability to make it out .
Recent results remain inconclusive , but do support the marginal Hipparcos detection of a binary companion .
In 1993 , radial velocity measurements of Aldebaran , Arcturus and Pollux showed that Arcturus exhibited a long-period radial velocity oscillation , which could be interpreted as a substellar companion .
In Arabic , it is one of two stars called al-simāk " the uplifted one " , the other being Spica .
Arcturus , then , is السماك الرامح as-simāk ar-rāmiħ " the uplifted one of the lancer " .
