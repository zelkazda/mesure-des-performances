Cheers is an American situation comedy television series that ran for eleven seasons from 1982 to 1993 .
It was produced by Charles/Burrows/Charles Productions in association with Paramount Network Television for NBC , having been created by the team of James Burrows , Glen Charles , and Les Charles .
The show is set in the Cheers bar ( named for the toast " Cheers " ) in Boston , Massachusetts , where a group of locals meet to drink , relax , chat and have fun .
However , Cheers eventually became a highly rated television show in the United States , earning a top-ten rating during eight of its eleven seasons , including one season at # 1 , and spending the bulk of its run on NBC 's " Must See Thursday " lineup .
The character Frasier Crane ( played by Kelsey Grammer ) was featured in his own successful spin-off , Frasier , which also ran for eleven seasons and included guest appearances by all of the major Cheers characters , except for Kirstie Alley and the deceased Nicholas Colasanto .
Cheers maintained an ensemble cast , keeping roughly the same set of characters for the entire run .
The character of Cliff Clavin was created for John Ratzenberger after he auditioned for the role of Norm Peterson .
The role of Norm went to George Wendt .
Kirstie Alley joined the cast when Shelley Long left , and Woody Harrelson joined when Nicholas Colasanto died .
Although Cheers operated largely around that main ensemble cast , guest stars did occasionally supplement them .
Some television stars also made guest appearances as themselves such as Alex Trebek , Arsenio Hall , Dick Cavett , Robert Urich , and Johnny Carson .
Christopher Lloyd guest starred as a tortured artist who wanted to paint Diane .
John Mahoney once appeared as an inept jingle writer , which included a brief conversation with Frasier Crane , whose father he later portrayed on the spin-off Frasier .
In the final episode of Kirstie Alley 's run as Rebecca , she was literally wooed away from Cheers by the guy who came to fix one of the beer keg taps -- surprising for a " high-class " lady -- but it happened to be Tom Berenger .
" Al " , played by Al Rosen , appeared in 38 episodes , and was known for his surly quips .
The concept for Cheers was the end result of a long consideration process .
The original idea was a group of workers who interacted like a family , hoping to be similar to The Mary Tyler Moore Show .
When the creators settled on a bar as their setting the show began to resemble the radio show Duffy 's Tavern .
The Bull & Finch Pub in Boston that Cheers was styled after was originally chosen from a phone book .
When Glen Charles asked the owner to shoot initial exterior and interior shots the owner agreed , charging $ 1 .
The crew of Cheers numbered in the hundreds ; as such , this section only provides a brief summary of the many crewmembers for the show .
The three creators -- James Burrows , Glen Charles , and Les Charles .
kept offices on Paramount 's lot for the duration of the Cheers run .
In the final seasons , however , they handed over much of the show to Burrows .
Burrows is regarded as being a factor in the show 's longevity , directing 243 of the episodes and supervising the show 's production .
Over its eleven-season run , Cheers and its cast and crew earned many awards .
Cheers earned a record 111 Emmy Award nominations , with a total of 26 wins .
In addition , Cheers has earned 31 Golden Globe nominations , with a total of six wins .
The following table summarizes awards won by the Cheers cast and crew .
Nearly all of Cheers took place in the front room of the bar , but they often went into the rear pool room or the bar 's office .
Cheers did n't show any action outside the bar until the first episode of the second season , which took the action to Diane 's apartment .
Cheers had some running gags , such as Norm arriving in the bar greeted by a loud " Norm ! "
The show 's main theme in its early seasons was the romance between the intellectual waitress Diane Chambers and bar owner Sam Malone , a former major league baseball pitcher for the Boston Red Sox and a recovering alcoholic .
Many Cheers scripts centered around or touched on a variety of social issues , albeit humorously .
The " upper class " -- represented by characters like Diane Chambers , Frasier Crane , Lilith Sternin and ( initially ) Rebecca Howe -- rubbed shoulders with middle and working class characters -- Sam Malone , Carla Tortelli , Norm Peterson and Cliff Clavin .
An extreme example of this was the relationship between Woody Boyd and millionaire 's daughter Kelly Gaines .
Many viewers enjoyed Cheers in part because of this focus on character development in addition to plot development .
It is later revealed on Frasier that her husband struck it rich and left her , upon which Rebecca returned to Cheers as a patron .
Lilith was a high profile psychatrist with many degrees and awards and commanded respect with her strong and rather stern demeanor .
Like Rebecca , she was an executive woman of the 1980s who put much emphasis on her professional life .
She often was portrayed to have the upper hand in her and Frasier 's relationship , and was portrayed as an ice queen , but proved to have a fiery libido and an emotionally maternal nature .
Homosexuality was dealt with from the very first season , a rare move for American network television in the early 1980s .
Finally , the final episode included a gay man who gets into trouble with his boyfriend ( played by Anthony Heald ) after agreeing to pose as Diane 's husband .
Norm 's alcoholism ( his bar tab was said to be compiled by NASA ) was never a main focus of the show .
He begs for his job back and is hired by Rebecca as a bartender .
Aside from the storylines that spanned across the series , Cheers had several themes that followed no storylines but that recurred throughout the series .
Norm Peterson continually searched for gainful employment as an accountant but spent most of the series unemployed , thereby explaining his constant presence in Cheers at the same stool .
Carla Tortelli carried a reputation of being both highly fertile and matrimonially inept .
The last husband she had on the show , Eddie LeBec , was a washed-up ice hockey goaltender who ended up dying in an ice show accident involving a zamboni .
Cheers was critically acclaimed in its first season , though it landed a disappointing 74th out of only 74 shows in that year 's ratings .
This critical support , coupled with early success at the Emmys and the support of the president of NBC 's entertainment division Brandon Tartikoff , is thought to be the main reason for the show 's survival and eventual success .
With the growing popularity of Family Ties which ran in the slot ahead of Cheers from January 1984 until Family Ties was moved to Sundays in 1987 and the placement of The Cosby Show in front of both at the start of their third season ( 1984 ) , the line-up became a runaway ratings success that NBC eventually dubbed " Must See Thursday " .
The next season , Cheers ratings increased dramatically after Woody Boyd became a regular character as well .
Some critics now use Frasier and Cheers as a model of a successful spin-off for a character from an already successful series to compare to modern spin-offs .
Cheers began with a limited five-character ensemble consisting of Ted Danson , Shelley Long , Rhea Perlman , Nicholas Colasanto and George Wendt .
By the time season 10 began , Cheers held 8 front characters in its roster .
What was notable about Cheers was its ability to gradually phase in characters such as Cliff , Frasier , Lilith , Rebecca , and Woody .
NBC dedicated a whole night to the final episode of Cheers , following the one-hour season finale of Seinfeld ( which was its lead-in ) .
The show began with a " pregame " show hosted by Bob Costas , followed by the final 98-minute episode itself .
NBC affiliates then aired tributes to Cheers during their local newscasts , and the night concluded with a special Tonight Show broadcast live from the Bull & Finch Pub .
The episode originally aired in the usual Cheers spot of Thursday night and was then rebroadcast on Sunday .
Some estimate that while the original broadcast did not outperform the M*A*S*H finale , the combined non-repeating audiences for the Thursday and Sunday showings did .
Toasting Cheers also notes that television had greatly changed between the M*A*S*H and Cheers finales , leaving Cheers with a broader array of competition for ratings .
Some of the actors and actresses from Cheers brought their characters into other television shows , either in a guest appearance or in a new spin-off .
The most successful Cheers spin-off was the show Frasier which directly followed Frasier Crane after he moved back to Seattle , Washington ( on the other end of Interstate 90 ) to live with his recently-disabled father and to host a call-in radio show .
In that episode , Frasier , on a trip to Boston , meets the Cheers gang ( though not at Cheers itself ) and Cliff thinks Frasier has flown out specifically for his ( Cliff 's ) retirement party , which Frasier ends up attending .
Frasier was on the air for as many seasons as Cheers , going off the air in 2004 after an eleven-season run .
Although Frasier was the most successful spin-off , The Tortellis was the first series to spin off from Cheers , premiering in 1987 .
In addition to direct spin-offs , several Cheers characters had guest appearance crossovers with other shows .
The show lent itself naturally to the development of " Cheers " bar-related merchandise , culminating in the development of a chain of " Cheers " themed pubs .
Paramount 's licensing group , led by Tom McGrath , developed the " Cheers " pub concept initially in partnership with Host Marriott which placed " Cheers " themed pubs in 24+ airports around the world .
Cheers grew in popularity as it aired on American television and entered into syndication .
When the show went off the air in 1993 , Cheers was syndicated in 38 countries with 179 American television markets and 83 million viewers .
Then , after going off the air , Cheers entered a long , successful , and continuing syndication run on Nick at Nite , then moving to TV Land in 2004 .
TV Land has since stopped airing reruns .
Then , the series began airing on Hallmark Channel in the United States in 2008 , and WGN America in 2009 , where it continues to air on both channels .
While the quality of some earlier footage of Cheers had begun to degrade , it underwent a careful restoration in 2001 due to its continued success .
The latter was cancelled mid-episode on its only broadcast by Kerry Packer , who pulled the plug after a phone call .
Cheers was aired by NCRV in the Netherlands .
After the last episode , NCRV simply began re-airing the series , and then again , thus airing the show three times in a row , showing an episode nightly .
On July 4 , 2009 , Nick at Nite announced it will relaunch Cheers in the summer of 2010 .
Kelsey Grammer was arguably the most successful with his spin-off Frasier , which lasted for the same eleven-season run Cheers had , as well as a recurring guest role on The Simpsons as Sideshow Bob .
By the final season of Frasier , Grammer had become the highest paid actor on television , earning about $ 1 .6 million an episode .
Woody Harrelson has also had a successful career following Cheers , including appearances in a number of notable films that have established him as a box-office draw , such as White Men Ca n't Jump , Natural Born Killers , Indecent Proposal , Kingpin and No Country for Old Men .
He also earned an Academy Award nominations in 1997 for The People vs. Larry Flynt and in 2010 for The Messenger .
Ted Danson , who had been the highest paid Cheers cast member earning $ 450,000 an episode in the final season , has starred in the successful sitcom Becker as well as the unsuccessful sitcoms Ink and Help Me Help You and currently appears in the successful drama series Damages .
He has starred in a number of movies , including Cousins , Three Men and a Baby and Made in America .
John Ratzenberger has voice acted in all of Pixar 's computer-animated feature films and currently hosts the Travel Channel show Made in America .
On Made in America he travels around the U.S. showing the stories of small towns and the goods they produce .
Coincidentally , Ted Danson starred in a film also called Made in America .
She also did voice work for All Dogs Go to Heaven 2 and All Dogs Go to Heaven : The Series .
At the time of her departure from Cheers , Shelley Long was criticized for leaving the series , with many believing that leaving the show was a bad career move .
In addition to continuing careers after Cheers , some of the cast members have had personal problems .
In 2004 , Shelley Long grew depressed after divorcing her husband of 23 years and appears to have attempted suicide by overdosing on drugs .
Kirstie Alley gained a significant amount of weight after Cheers , which somewhat affected her career .
She went on to write and star in a sitcom partly based on her life and weight gain , Fat Actress .
She formerly was a spokeswoman for weight loss and nutrition company Jenny Craig .
The Host Marriott Corporation installed 46 bars modeled after Cheers in their hotel and airport lounges .
Paramount Pictures licensed the characters and details of the show , allowing the bars to have fake memorabilia such as Sam Malone 's supposed jersey while playing for the Red Sox .
Some believe the case could have had significant implications in Hollywood , as its outcome would have determined whether rights over a character imply rights to reproduce the actor 's image with or without his or her permission , so long as the image is of the actor as the character .
Rather , Paramount settled with the two before a ruling in the suit was delivered .
( The first location outside the bar ever seen was Diane 's apartment . )
The exterior location shots of the bar were of the Bull & Finch Pub , located directly north of the Boston Public Garden , which has become a tourist attraction because of its association with the series and draws in nearly a million visitors annually .
It has since been renamed Cheers Beacon Hill , though its interior is different from the TV bar .
To further capitalize on the show 's popularity , another bar , Cheers Faneuil Hall , was built to be a replica of the show 's set to provide tourists with a bar whose interior was closer to the one they saw on TV .
It is near Faneuil Hall , about a mile from the Bull & Finch Pub .
Like Cheers Faneuil Hall , Cheers London is an exact replica of the set .
The gala opening was attended by James Burrows and cast members George Wendt and John Ratzenberger .
