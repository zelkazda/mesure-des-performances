Some of the most elaborate columns in the ancient world were those of Persia especially the massive stone columns erected in Persepolis .
The Hall of Hundred Columns at Persepolis , measuring 70 × 70 meters was built by the Achaemenid king Darius I ( 524 -- 486 BC ) .
It is often referred to as the masculine order because it is represented in the bottom level of the Colosseum and the Parthenon , and was therefore considered to be able to hold more weight .
However , according to the architectural historian Vitruvius , the column was created by the sculptor Callimachus , probably an Athenian , who drew acanthus leaves growing around a votive basket .
It is sometimes called the feminine order because it is on the top level of the Colosseum and holding up the least weight , and also has the slenderest ratio of thickness to height .
