Although he saved his life by turning informer , he was condemned to partial loss of civil rights and forced to leave Athens .
He engaged in commercial pursuits , and returned to Athens under the general amnesty that followed the restoration of the democracy ( 403 BC ) , and filled some important offices .
In 391 BC he was one of the ambassadors sent to Sparta to discuss peace terms , but the negotiations failed .
Andocides was no professional orator ; his style is simple and lively , natural but inartistic .
