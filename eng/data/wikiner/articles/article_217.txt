The Azerbaijan Democratic Republic ( ADR ) had originally formed its own armed forces from 26 June 1918 .
After the Soviet Union dissolved in 1991-92 the armed forces were reformed based on Soviet bases and equipment left on Azeri soil .
The armed forces have three branches : the Azerbaijani Land Forces , the Azerbaijani Air and Air Defence Force , a single unified branch , and the Azerbaijani Navy .
Azeri troops are still serving in Afghanistan .
As many as 20,000 of the total 30,000 soldiers died resisting what was effectively a Russian reconquest .
Much of the Soviet Union 's oil on the Eastern Front was supplied by Baku .
The 4th Army also included missile and air defense brigades and artillery and rocket regiments .
The transfer of the property of the 4th Army and the 49th arsenal was completed in 1992 .
There are also reports that 50 combat aircraft from the disbanded 19th Army of the Soviet Air Defence Forces came under Azeri control .
The Azerbaijani Land Forces number 85,000 strong , according to UK Advanced Research and Assessment Group estimates .
The 2,500 men of the National Guard are also part of the ground forces .
( IISS 2007 , p. 157 )
The unit secured the hydroelectric power station and reservoir in Al Haditha from August 2003 .
The Azerbaijani Air and Air Defence Force is a single unified service branch .
The Azerbaijani Air and Air Defence Force has around 106 aircraft and 35 helicopters .
Nasosnaya Air Base has fighters , Kyurdamir Air Base a bomber regiment , Ganja Air Base transports , and Baku Kala Air Base the helicopter unit .
The Azeri Air Force uses MiG-21 , MiG-23 , Su-24 and Su-25 aircraft , as well as the MiG-29 purchased from Ukraine in 2006 and Il-76 transport aircraft .
MiG-25s previously in service have been retired seemingly in the 2007-09 period .
Jane 's Information Group and the IISS give figures which agree with only a single aircraft 's difference .
These may be around Baku and the central part to cover the whole Azeri aerospace .
The Qabala Radar is a bistatic phased-array installation , currently operated by the Russian Space Forces under a contract until 2012 .
The radar station has a range of up to 6,000 kilometres ( 3,728 mi ) , and was designed to detect intercontinental ballistic missile launches as far as from the Indian Ocean .
The Azerbaijan Navy has about 2,200 personnel .
There are four minesweepers consisting of 2 Sonya class minesweeper and 2 Yevgenya class minesweepers .
( Jane 's Fighting Ships 2010 )
There is also an agreement to provide US support to refurbish Azeri warships in the Caspian Sea .
In 2007 an agreement between Azeri Navy and a US military company was concluded , which stated that a part of the Azeri Navy would be equipped with advanced laser marksmanship systems .
The US company specialists were also to give training on the use of the new equipment .
The ministry is cooperating with the defense sectors of Ukraine , Belarus and Pakistan .
Oklahoma National Guard troops have been sent on training and humanitarian missions to Baku .
