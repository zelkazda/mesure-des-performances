The balalaika is a stringed instrument of Russian origin , with a characteristic triangular body and three strings ( or sometimes six , in three courses ) .
Another theory is : Before Tsar Peter I , instruments were not allowed in Russia .
A number of Andreyev 's students also toured the west in 1909-12 .
Folk music and folk musical instruments was considered the music of the working classes and as a result it was heavily supported by the Soviet establishment .
Not surprisingly , the concept of the balalaika orchestra was adopted wholeheartedly by the Soviet government as something distinctively proletarian ( that is , from the working classes ) .
The world-famous Red Army Choir used a normal orchestra , except that the violins , violas and violoncellos were replaced with orchestral balalaikas and domras .
