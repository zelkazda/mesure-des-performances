Water from Lake Vyrnwy is added to bring the strength of Bombay Sapphire down to 40.0 % .
Their works , varying from martini glasses to tiles and cloth patterns , are labelled as " Inspired by Bombay Sapphire " .
Bombay Sapphire also showcases the designers ' work in the Bombay Sapphire endorsed blue room , which is actually a design exhibition touring the world each year .
