These include the United Church of Canada , and the United Church of Christ .
Various parts of the Lutheran Church hold stances on the issue ranging from declaring homosexual acts as sin to acceptance of homosexual relationships .
For example , the Lutheran Church -- Missouri Synod , the Lutheran Church of Australia , and the Wisconsin Evangelical Lutheran Synod recognize homophile behavior as intrinsically sinful and seek to minister to those who are struggling with homosexual inclinations .
For example , the Friends United Meeting and the Evangelical Friends International believe the sexual relations are condoned only in marriage , which they define to be between a man and a woman .
The Eastern Orthodox Church , the Roman Catholic Church and most Fundamentalist and Evangelical Protestant churches do not sanction same-sex sexual relations .
The late Yale University Church historian John Boswell argued for the existence of a rite of adelphopoiesis as a religiously sanctioned same-sex union .
Critics of Boswell have pointed out that many earlier doctrinal sources condemn homosexuality in ethical terms without prescribing a punishment , and that Boswell 's citations reflected a general trend towards harsher penalties from the 12th century onwards .
While they may hold the document as sacred , and most certainly as central to Christianity , they are also aware of the historical and cultural context in which it was originally written through archaeological and from critical study .
Christian objections to homosexual behavior are often based upon their interpretations of the Bible .
The Catechism of the Catholic Church states " men and women who have deep-seated homosexual tendencies ... must be accepted with respect , compassion , and sensitivity . "
The Catholic Church requires those who are attracted to people of the same sex to practice chastity , because sexuality should only be practiced within marriage , which it regards as permanent , procreative , heterosexual , and monogamous .
There are concerns with copying errors , forgery , and biases among the translators of later Bibles .
Jesus exemplified this principle in his teaching on divorce .
Furthermore , it is said that Jesus Christ instituted a virtue ethic , whereby the worth of one 's action is to be adjudged by one 's interior disposition .
She argued that Barna had formulated his report with undue irony and skepticism , and that he had failed to take into account the reasons for the data which enkindled his " arrière pensée . "
Exodus International and the associated Love Won Out are examples of such ministries .
Alan Chambers , the president of Exodus , says the term incorrectly implies a complete change in sexual orientation , though the group Parents and Friends of Ex-Gays and Gays continues to use the term .
such as gay Anglicans in Nigeria .
Organizations such as Exodus International and Parents and Friends of Ex-Gays believe that , regardless of one 's sexual orientation , " practicing homosexuality " is always a deliberate choice .
Similarly , the Roman Catholic Church and the LDS Church regard homosexual intercourse , but not homosexual attraction , as sinful .
