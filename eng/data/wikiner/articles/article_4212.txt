ELO was formed to accommodate Roy Wood and Jeff Lynne 's desire to create modern rock and pop songs with classical overtones .
They soon gained a cult following despite lukewarm reviews back in their native U.K. .
ELO collected 21 RIAA awards , 38 BPI awards , and sold well over 50 million records worldwide , not including singles .
In the late 1960s , Roy Wood -- guitarist , vocalist and songwriter of The Move -- had an idea to form a new band that would use violins , cellos , string basses , horns and woodwinds to give their music a classical sound , taking rock music in the direction " that The Beatles had left off . "
Jeff Lynne , frontman of fellow Birmingham group The Idle Race , was excited by the concept .
To help finance the fledgling band , two more Move albums were released during the lengthy ELO recordings .
The resulting debut album The Electric Light Orchestra was released in 1971 ( 1972 in the United States as No Answer ) and " 10538 Overture " became a UK top ten hit .
The new lineup performed at the 1972 Reading Festival .
The band released their second album , ELO 2 in 1973 , which produced their first US chart single , a hugely elaborate version of the Chuck Berry classic " Roll Over Beethoven " .
ELO also made their first appearance on American Bandstand .
Louis Clark joined the band as string arranger .
ELO had become successful in the United States at this point and they were a star attraction on the stadium and arena circuit , as well as regularly appearing on The Midnight Special ( 1973 , 1975 , 1976 & 1977 ) more than any other band in that show 's history with four appearances .
Face the Music was released in 1975 , producing the hit singles " Evil Woman " and " Strange Magic " .
The opening instrumental " Fire On High " , with its mix of strings and blazing acoustic guitars , saw heavy exposure as background music on CBS Sports Spectacular montages , though most viewers had no idea of the song 's origins .
The group toured extensively from 3 February till 13 April 1976 promoting the album in the USA , playing 68 shows in 76 days .
It was on the American tour that ELO first debuted their use of coloured lasers .
Despite the recognition and success they enjoyed in the states they were still largely ignored in the United Kingdom until their sixth album , A New World Record , hit the top ten there in 1976 .
It contained the hit singles " Livin ' Thing " , " Telephone Line " , " Rockaria ! "
and " Do Ya " , a rerecording of a Move song .
The band also played at the Wembley Stadium for eight straight sold-out nights during the tour as well , another record at that time .
The first of these shows was recorded and televised , and later released as a CD and DVD .
Although the biggest hit on the album ( and ELO 's biggest hit overall ) was the rock song " Do n't Bring Me Down " , the album was noted for its heavy disco influence .
Discovery also produced the hits " Shine a Little Love " , " Last Train to London " , " Confusion " and " The Diary of Horace Wimp " .
Although there would be no live tour associated with Discovery , the band recorded the entire album in video form .
The Discovery music videos would be the last time the " classic " late 1970s lineup would be seen together , as the violinist , Mik Kaminski , and the two cellists , Hugh McDowell and Melvyn Gale , were dismissed shortly thereafter .
The Electric Light Orchestra finished 1979 as the biggest selling act in the United Kingdom .
[ citation needed ] ELO had reached the peak of their stardom , selling millions of albums and singles and even inspiring a parody / tribute song on the Randy Newman album Born Again .
In 1980 Jeff Lynne was asked to write for the soundtrack of the musical film Xanadu , with the other half written by John Farrar and performed by the film 's star Olivia Newton-John .
The album spawned hit singles from both Newton-John ( " Magic, " # 1 in the United States , and " Suddenly " with Cliff Richard ) and ELO ( " I 'm Alive " , which went gold , " All Over the World " and " Do n't Walk Away " ) .
The title track , performed by both Newton-John and ELO , is ELO 's only song to top the singles chart in the United Kingdom .
In 1981 ELO 's sound changed again with the science fiction concept album Time , a throwback to earlier , more progressive rock albums like Eldorado .
Time topped the U.K. charts for two weeks and was the last ELO studio album to date to be certified platinum in the United Kingdom .
It was the first ELO tour without cellists , although Mik Kaminski returned to play his famous " blue violin . "
Jeff Lynne wanted to follow Time with a double album .
The new album was edited down from double album to a single disc and released as Secret Messages in 1983 .
( Many of the outtakes were later released on " Afterglow " or as b-sides of singles . )
The album was an instant hit in the UK reaching the top 5 .
Rumours from fans about the group disbanding were publicly denied by Bevan .
Although Secret Messages debuted at number four in the United Kingdom , it fell off the charts , failing to catch fire with a lack of hit singles in the U.K. and a lukewarm media response .
The album was absent of actual strings , replaced once again by synthesisers , played by Tandy .
The album also shed the customary ELO logo that had appeared on every album since 1976 .
The Birmingham Heart Beat Charity Concert 1986 was a charity concert organised by Bevan in ELO 's hometown of Birmingham on 15 March 1986 .
Bevan continued on in 1988 as ELO Part II , initially with no other former ELO members except Clark .
ELO Part II released their debut album Electric Light Orchestra Part Two in 1990 .
Mik Kaminski , Kelly Groucutt and Hugh McDowell joined the band for the first tour in 1991 .
Bevan retired from the lineup in 1999 and sold his share of the ELO name to Jeff Lynne in 2000 .
In 2001 Zoom , ELO 's first album since 1986 , was released .
Though billed and marketed as an ELO album , the only returning member other than Jeff Lynne was Richard Tandy , who performed on one track .
Zoom took on a more organic sound , with less emphasis on strings and electronic effects .
Guest musicians included former Beatles Ringo Starr and George Harrison .
Former ELO member Richard Tandy rejoined the band a short time afterwards for two television live performances : VH1 Storytellers and a PBS concert shot at CBS Television City , later titled Zoom Tour Live , that was released on DVD .
The ELO tour was not rescheduled .
Included amongst the remastered album tracks were unreleased songs and outtakes , including 2 new singles " Surrender " which registered on the lower end of the UK Singles Chart at # 81 , some 30 years after it was written in 1976 .
Jeff Lynne returned to the song and finished it in preparation for the remastered version of Out of the Blue .
A lost demo from 1977 was finished and released in the United Kingdom as a download single on 6 February 2007 , titled " Latitude 88 North " .
Among the many features was the original Jet Records label on the disc and original inner sleeves and lyrics .
It 's the follow-up to All Over the World : The Very Best of Electric Light Orchestra and is called Ticket to the Moon : The Very Best of Electric Light Orchestra Volume 2 .
The US will see a slightly edited release on 24 August 2010 .
The group 's name is an intended pun based not only on electric light ( as in a light bulb as seen on early album covers ) but also using " electric " rock instruments combined with a " light orchestra " ( orchestras with only a few cellos and violins that were popular in Britain during the 1960s ) .
The official band logo ( left ) , designed in 1976 by artist Kosh , was first seen on their 1976 album A New World Record and is based on a 1946 Wurlitzer jukebox model 4008 speaker .
The 4008 speaker was itself based upon the upper cabinet of the Wurlitzer model 1015 jukebox .
The band 's previous logo ( right ) was similar to the General Electric logo .
For instance , on 1977 's Out of the Blue , the logo was turned into a huge flying saucer space station , an enduring image now synonymous with the band .
On the follow up album Discovery , the logo became a small glowing artifact on top of a treasure chest .
Bev Bevan usually displayed the logo on his drum kit .
