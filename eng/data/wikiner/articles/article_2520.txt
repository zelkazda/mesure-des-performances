Romantic era repertoire includes the Schumann Concerto , the Dvořák Concerto as well as the two sonatas and the Double Concerto by Brahms .
The cello 's versatility made it popular with composers in the mid-to late twentieth century such as Prokofiev , Shostakovich , Britten , Ligeti and Dutilleux , encouraged by soloists who specialized in contemporary music ( such as Siegfried Palm and Mstislav Rostropovich ) commissioning from and collaborating with composers .
The violoncello da spalla ( sometimes " violoncello piccolo da spalla " or " violoncello da span " ) was the first cello referred to in print ( by Jambe de Fer in 1556 ) .
The invention of wire-wound strings ( fine wire around a thin gut core ) , around 1660 in Bologna , allowed for a finer bass sound than was possible with purely gut strings on such a short body .
To extend the technique in this area , Frances-Marie Uitti has invented a two-bow system : one bow plays above the strings and one below , allowing for sustained triple and quadruple stops .
The cellos of Stradivari , for example , can be clearly divided into two models , the style made before 1702 characterized by larger instruments ( of which only three exist in their original size and configuration ) , and the style made during and after 1702 , when Stradivari , presumably in response to the " new " strings , began making smaller cellos .
There are numerous cello concertos -- where a solo cello is accompanied by an orchestra -- notably 25 by Vivaldi , 12 by Boccherini , 4 by Haydn , 3 by C.P.E. Bach , 2 by Saint-Saëns , 2 by Dvořák , and one each by Schumann , Lalo , and Elgar .
This was partly due to the influence of virtuoso cellist Mstislav Rostropovich who inspired , commissioned and/or premiered dozens of new works .
In addition , Hindemith , Barber , Honegger , Villa-Lobos , Myaskovsky , Walton , Glass , Rodrigo , Arnold , Penderecki and Ligeti also wrote major concertos for other cellists , notably for Gregor Piatigorsky , Siegfried Palm and Julian Lloyd Webber .
Those written by Beethoven , Mendelssohn , Chopin , Brahms , Grieg , Rachmaninoff , Debussy , Fauré , Shostakovich , Prokofiev , Poulenc , Carter , and Britten are the most famous .
The Twelve Cellists of the Berlin Philharmonic Orchestra specialize in this repertoire and have commissioned many works , including arrangements of well-known popular songs .
The cello is rarely part of a group 's standard lineup but like its cousin the violin it is becoming more common in mainstream pop ( i.e. the baroque rock band Arcade Fire uses the cello in their songs ) .
In the 1960s , artists such as the Beatles and Cher used the cello in popular music , in songs such as " Bang Bang " , " Eleanor Rigby " and " Strawberry Fields Forever " .
Most notably , Pink Floyd included a cello solo in their 1970 epic instrumental Atom Heart Mother .
More recent bands using the cello are Aerosmith , Nirvana , Oasis , Murder by Death , Cursive , Smashing Pumpkins , and OneRepublic .
Heavy metal band System of a Down has also made use of the cello 's rich sound .
The indie rock band The Stiletto Formal are known for using a cello as a major staple of their sound , similarly , the indie rock band Canada employs two cello players in their lineup .
The orch-rock group , The Polyphonic Spree , which has pioneered the use of stringed and symphonic instruments , employs the cello in very creative ways for many of their " psychedelic-esque " melodies .
The first wave screamo band I Would Set Myself On Fire For You featured a cello as well as a viola to create a more folk-oriented sound .
In jazz , bassists Oscar Pettiford and Harry Babasin were among the first to use the cello as a solo instrument ; both tuned their instrument in fourths , an octave above the double bass .
Lindsay Mac is becoming well known for playing the cello like a guitar , with her cover of The Beatles Blackbird a big hit on The Bob & Tom Show .
