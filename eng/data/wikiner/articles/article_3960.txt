The European Commission is the executive body of the European Union .
One of the 27 is the Commission President ( currently José Manuel Durão Barroso ) appointed by the European Council .
The first Barroso Commission took office in late 2004 and its successor , under the same President , took office in 2010 .
The first Commission originated in 1951 as the nine-member " High Authority " under President Jean Monnet ( see Monnet Authority ) .
The High Authority was the supranational administrative executive of the new European Coal and Steel Community ( ECSC ) .
It took office first on 10 August 1952 in Luxembourg .
In 1958 the Treaties of Rome had established two new communities alongside the ECSC : the European Economic Community and the European Atomic Energy Community ( Euratom ) .
It achieved agreement on a contentious cereal price accord as well as making a positive impression upon third countries when it made its international debut at the Kennedy Round of General Agreement on Tariffs and Trade ( GATT ) negotiations .
Little heed was taken of his administration at first but , with help from the European Court of Justice , his Commission stamped its authority solidly enough to allow future Commissions to be taken more seriously .
The three bodies co-existed until 1 July 1967 where , by means of the Merger Treaty , they were combined into a single administration under President Jean Rey .
The Malfatti and Mansholt Commissions followed with work on monetary co-operation and the first enlargement to the north in 1973 .
With that enlargement the Commission 's membership increased to thirteen under the Ortoli Commission , which dealt with the enlarged community during economic and international instability at that time .
The external representation of the Community took a step forward when President Roy Jenkins became the first President to attend a G8 summit on behalf of the Community .
Delors was seen as giving the Community a sense of direction and dynamism .
Although he was a little-known former French finance minister , he breathed life and hope into the EC and into the dispirited Brussels Commission .
However the Santer Commission did carry out work on the Amsterdam Treaty and the euro .
Following Santer , Romano Prodi took office .
The Amsterdam Treaty had increased the Commission 's powers and Prodi was dubbed by the press as something akin to a Prime Minister .
Powers were strengthened again with the Nice Treaty in 2001 giving the President more power over the composition of their Commission .
Due to the opposition Barroso was forced to reshuffle his team before taking office .
Following the accession of Romania and Bulgaria in January 2007 , this clause took effect for the next Commission .
This aspect has been changed by the Treaty of Lisbon , after which the Commission exercises its powers just by virtue of the treaties .
Powers are more restricted than most national executives , in part due to the Commission 's lack of power over areas like foreign policy -- that power is held by the European Council , which some analyses have described as another executive .
However , it is the Commission which currently holds executive powers over the European Union .
The Commission differs from the other institutions in that it alone has legislative initiative in the European Union , meaning only the Commission can make formal proposals for legislation -- legislative proposals can not originate in the legislative branches .
The Commission 's powers in proposing law have usually centred on economic regulation .
Thus , the Commission often proposes stricter legislation than other countries .
Their right to propose criminal law was challenged in the European Court of Justice but upheld .
In adopting the necessary technical measures , the Commission is assisted by committees made up of representatives of member states and of the public and private lobbies ( a process known in jargon as " comitology " ) .
In particular the Commission has a duty to ensure the treaties and law are upheld , potentially by taking member states or other institutions to the Court of Justice in a dispute .
It is also usual for the President to attend meetings of the G8 .
Even though each member is appointed by a national government , one per state , they do not represent their state in the Commission ( however in practice they do occasionally press for their national interest ) .
The President of the Commission is first nominated by the European Council ; that nominee is then officially elected by the European Parliament .
In the end , a centre-right candidate was chosen : José Manuel Barroso of the European People 's Party .
Following the election of the President , and the High Representative by the same procedure , each Commissioner is nominated by their member state ( except for those states who provided the President and High Representative ) in consultation with the Commission President , although he holds little practical power to force a change in candidate .
However the more capable the candidate is , the more likely the Commission President will assign them a powerful portfolio , the distribution of which is entirely at his discretion .
The President 's team is then subject to hearings at the European Parliament which will question them and then vote on their suitability as a whole .
If members of the team are found to be too inappropriate , the President must then reshuffle the team or request a new candidate from the member state or risk the whole Commission being voted down .
Once the team is approved by parliament , it is formally put into office by the European Council .
Following their appointment , the President appoints a number of Vice-Presidents ( the High Representative is mandated to be one of them ) from among the commissioners .
This might increase with the creation of the President of the European Council under the Treaty of Lisbon .
The Commission is primarily based in Brussels , with the President 's office and the Commission 's meeting room based on the 13th floor of the Berlaymont building .
The Commission also operates out of numerous other buildings in Brussels and Luxembourg .
According to figures published by the Commission , 23,043 persons were employed by the Commission as officials and temporary agents in April 2007 .
The single largest DG is the Directorate-General for Translation , with a 2186-strong staff , while the largest group by nationality is Belgian ( 21.4 % ) , probably due to a majority ( 16,626 ) of staff being based in the country .
Communication with the press is handled by the Directorate-General Communication .
It has been noted by one researcher that the press releases issued by the Commission are uniquely political .
However since the global downturn by 2010 the press corps in Brussels shrunk by a third .
The Treaty of Lisbon may go some way to resolving the deficit in creating greater democratic controls on the Commission , including enshrining the procedure of linking elections to the selection of the Commission president .
The alternative viewpoint on the Commission states that the policy areas in which it has power to initiate legislation are ill suited to an institution subject to electoral pressures .
