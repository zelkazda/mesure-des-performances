The Battle of Actium was the decisive confrontation of the Final War of the Roman Republic .
It was fought between the forces of Octavian and the combined forces of Mark Antony and Cleopatra .
The battle took place on 2 September 31 BC , on the Ionian Sea near the Roman colony of Actium in Greece .
Octavian 's fleet was commanded by Marcus Vipsanius Agrippa , while Antony 's fleet was supported by the ships of his beloved , Queen Cleopatra of Ptolemaic Egypt .
Octavian 's victory enabled him to consolidate his power over Rome and its dominions .
As Augustus , he would retain the trappings of a restored Republican leader ; however , historians generally view this consolidation of power and the adoption of these honorifics as the end of the Roman Republic and the beginning of the Roman Empire .
Octavian 's prestige and , more importantly , the loyalty of his legions had been initially boosted by Julius Caesar 's legacy of 44 BC , by which the then seventeen-year-old Octavian had been officially adopted as the only son of the great Roman general and also established as the sole legitimate heir of his enormous wealth .
Mark Antony had been the main and most successful top officer in Julius Caesar 's army ( magister equitum ) and , thanks to his military record , could claim a substantial share of the political support of the Caesar 's soldiers and veterans .
Both Octavian and Mark Antony had fought against their common enemies in the civil war that followed the assassination of Julius Caesar .
After years of loyal cooperation with Octavian , Mark Antony started to act independently , eventually raising the suspicion that he was vying to become the sole master of Rome .
As a personal challenge to Octavian 's prestige , Antony tried to get Caesarion accepted as a true heir of Julius Caesar , even though the legacy did not mention him at all .
Being a son of Julius Caesar , such an entitlement was obviously felt as a threat to the Roman republican traditions .
In fact , according to a widespread belief , Mark Antony had once offered the crown to Julius Caesar .
It was also said that Antony intended to move the capital of the empire to Alexandria .
He hoped that he might be regarded by them as their champion against the ambition of Octavian , whom he presumed would not be willing to abandon his position in a similar manner .
The consuls of that year had determined to conceal the extent of Antony 's demands .
Ahenobarbus seems to have wished to keep quiet ; but G. Sosius on 1 January made an elaborate speech in favor of Antony , and would have proposed the confirmation of his acta had it not been vetoed by a tribune .
Octavian was not behindhand in preparations .
But , by the publication of Antony 's will , which had been put into his hands by the traitor Plancus , and by carefully letting it be known at Rome what preparations were going on at Samos , and how entirely Antony was acting as the agent of Cleopatra , Octavian produced such a violent outburst of feeling that he easily obtained his deposition from the consulship of 31 , for which he was designated , and a vote for a proclamation of war against Cleopatra , well understood to mean against Antony , though he was not named .
But finding the sea guarded by a squadron of Octavian 's ships , he retired to overwinter at Patrae , while his fleet for the most part lay in the Ambracian Gulf , and his land forces encamped near the promontory of Actium , while the opposite side of the narrow strait into the Ambracian Gulf was also protected by a tower and a body of troops .
Octavian 's proposals for a conference with Antony having been scornfully rejected , both sides prepared for the final struggle next year .
The early months passed without notable event , beyond some successes of Agrippa on the coasts of Greece , meant to divert Antony 's attention .
It was not until the latter part of August that troops were brought by land into the neighbourhood of Antony 's camp on the north side of the strait .
Still Antony could not be tempted out .
But during these months not only was Agrippa continuing his descent upon Greek towns and coasts , but in various cavalry skirmishes , Octavian had so far prevailed that Antony abandoned the north side of the strait and confined his soldiers to the southern camp .
Cleopatra now earnestly advised that garrisons should be put into strong towns , and that the main fleet should return to Alexandria .
Octavian learnt of this and determined to prevent it .
When the trumpet signal for the start rang out , Antony 's fleet began issuing from the straits , and the ships moving into line remained quiet .
Octavian , after a short hesitation , ordered his vessels to steer to the right and pass the enemy 's ships .
Then for fear of being surrounded , Antony was forced to give the word to attack .
The two fleets met outside the Gulf of Actium , on the morning of 2 September 31 BC .
Antony 's fleet numbered 500 , of which were 230 large wargalleys furnished with towers full of armed men .
Octavian had about 250 warships .
Octavian 's fleet was waiting beyond the straits , led by the experienced admiral Agrippa , commanding from the left wing of the fleet , Lucius Arruntius commanding the centre and Marcus Lurius commanding from the right .
Titus Statilius Taurus commanded Octavian 's armies , who observed the battle from shore to the north of the straits .
Gaius Sosius launched the initial attack from the left wing of the fleet , while Antony 's chief lieutenant Publius Canidius Crassus was in command of the triumvir 's land forces .
The majority of Mark Antony 's warships were quinqueremes , huge galleys with massive rams , that could weigh up to three hundred tons .
Antony 's ships were often furnished with grappling irons , which were effective if hurled successfully ; but , if they failed , were apt to damage the ship , or to cause so much delay as to expose the men on board to the darts from the smaller vessel .
Unfortunately for Antony , many of his ships were undermanned ; there had been a severe malaria outbreak while they were waiting for Octavian 's fleet to arrive .
Octavian 's fleet was largely made up of smaller , fully manned Liburnian vessels , armed with better-trained , fresher crews .
Octavian 's ships were generally smaller , but more manageable in the heavy surf , capable of reversing their course on short notice and returning to the charge or , after pouring in a volley of darts on some huge adversary , able to retreat out of shot with speed .
Prior to the battle , one of Mark Antony 's generals , Quintus Dellius , had defected to Octavian , bringing with him Mark Antony 's battle plans .
Antony had hoped to use his biggest ships to drive back Agrippa 's wing on the north end of his line , but Octavian 's entire fleet , aware of this strategy , stayed out of range .
Shortly after midday , Antony was forced to extend his line from the protection of the shore and finally engage the enemy .
Cleopatra , in the rear , could not bear the suspense , and in an agony of anxiety , gave the signal for retreat .
Cleopatra 's fleet retreated to open sea without engaging .
Antony had not observed the signal , and believing that it was mere panic and that all was lost , followed the flying squadron .
Mark Antony transferred to a smaller vessel with his flag and managed to escape , taking a few ships with him as an escort to help break through Octavian 's lines .
Those that remained left behind , however , were captured or sunk by Octavian 's forces .
To try to turn this to his advantage , Antony gathered his ships around him in a quasi-horseshoe formation , staying close to the shore for safety .
Then , should Octavian 's ships approach Antony 's , the sea would push them into the shore .
Antony foresaw that he would not be able to defeat Octavian 's forces , so Cleopatra and he stayed in the rear of the formation .
Eventually , Antony sent the ships on the northern part of the formation to attack .
He had them move out to the north , spreading out Octavian 's ships which up until now were tightly arranged .
He sent Gaius Sosius down to the south to spread the remaining ships out to the south .
This left a hole in the middle of Octavian 's formation .
Antony seized the opportunity and with Cleopatra on her ship and him on a different ship , sped through the gap and escaped , abandoning his entire force .
With the end of the battle , Octavian exerted himself to save the crews of the burning vessels , and had to spend the whole night on board .
Under cover of darkness some 19 legions and 12,000 cavalry fled before Antony was able to engage Octavian in a land battle .
After Mark Antony lost his fleet , his army , which had been equal to that of Octavian , deserted in large numbers .
Antony , though he had not laid down his imperium , was a fugitive and a rebel , without that shadow of a legal position which the presence of the consuls and senators had given him in the previous year .
At Samos , Octavian received a message from Cleopatra with the present of a gold crown and throne , offering to abdicate in favour of her sons .
The queen was allowed to believe that she would be well treated , for Octavian was anxious to secure her for his triumph .
Despite a victory at Alexandria on 31 July 30 BC , more of Mark Antony 's men deserted , leaving him with insufficient forces to fight Octavian .
The slight success over Octavian 's tired soldiers encouraged him to make a general attack , in which he was decisively beaten .
Mark Antony then tried to flee from the battle , and as a result of a communication breakdown , came to believe that Cleopatra had been captured , and hence committed suicide .
Failing to escape on board ship , he stabbed himself ; and , as he did not die at once , insisted on being taken to the mausoleum in which Cleopatra was shut up , and there died in her arms .
The queen was shortly afterwards brought from this place to the palace and vainly attempting to move Octavian 's pity .
After Mark Antony 's death , Cleopatra eluded the vigilance of her guard Epaphroditus and committed suicide , on 12 August 30 BC .
Octavian had Caesarion killed later that year , finally securing his legacy as Julius Caesar 's only ' son ' .
