All of the major transportation systems in Haiti are located near or run through the capital , Port-au-Prince .
This route links the capital and Le Cap to the central plateau ; however , due to its poor condition , it sees limited use .
The port at Port-au-Prince , Port international de Port-au-Prince , has more registered shipping than any of the over dozen ports in the country .
The port is underused , possibly due to the substantially high port fees compared to ports in the Dominican Republic .
These cities , together with their surrounding areas , contain about six million of Haïti 's eight million people .
The company had facilities in Port-au-Prince and their ocean liners stopped there .
Air Haïti , Tropical Airways and a handful of major airlines from Europe , the Caribbean , and the Americas serve the airport .
Domestic flights are also available through Mission Aviation Fellowship to 14 airstrips .
