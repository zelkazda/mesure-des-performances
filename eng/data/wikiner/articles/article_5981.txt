The geography of Iraq is diverse and falls into four main regions : the desert ( west of the Euphrates River ) , Upper Mesopotamia ( between the upper Tigris and Euphrates rivers ) , the northern highlands of Iraqi Kurdistan , and Lower Mesopotamia , the alluvial plain extending from around Tikrit to the Persian Gulf .
The mountains in the northeast are an extension of the alpine system that runs eastward from the Balkans through southern Turkey , northern Iraq , Iran , and Afghanistan , eventually reaching the Himalayas .
The desert is in the southwest and central provinces along the borders with Saudi Arabia and Jordan and geographically belongs with the Arabian Peninsula .
The uplands region , between the Tigris north of Samarra and the Euphrates north of Hit , is known as Al Jazira ( the island ) and is part of a larger area that extends westward into Syria between the two rivers and into Turkey .
Here the Tigris and Euphrates rivers lie above the level of the plain in many places , and the whole area is a river delta interlaced by the channels of the two rivers and by irrigation canals .
Because the waters of the Tigris and Euphrates above their confluence are heavily silt -- laden , irrigation and fairly frequent flooding deposit large quantities of silty loam in much of the delta area .
The Tigris and Euphrates also carry large quantities of salts .
In general , the salinity of the soil increases from Baghdad south to the Persian Gulf and severely limits productivity in the region south of Al Amarah .
The salinity is reflected in the large lake in central Iraq , southwest of Baghdad , known as Bahr al Milh ( Sea of Salt ) .
There are two other major lakes in the country to the north of Bahr al Milh : Buhayrat ath Tharthar and Buhayrat al Habbaniyah .
The northeastern highlands begin just south of a line drawn from Mosul to Kirkuk and extend to the borders with Turkey and Iran .
Here , too , are the great oil fields near Mosul and Kirkuk .
The desert zone , an area lying west and southwest of the Euphrates River , is a part of the Syrian Desert , which covers sections of Syria , Jordan , and Saudi Arabia .
A widely ramified pattern of wadis -- watercourses that are dry most of the year -- runs from the border to the Euphrates .
It then winds through a gorge , which varies from two to 16 kilometers in width , until it flows out on the plain at Ar Ramadi .
Below Al Kifl , the river follows two channels to As-Samawah , where it reappears as a single channel to join the Tigris at Al Qurnah .
At the Kut Barrage much of the water is diverted into the Shatt al-Hayy , which was once the main channel of the Tigris .
Water from the Tigris thus enters the Euphrates through the Shatt al-Hayy well above the confluence of the two main channels at Al Qurnah .
Both the Tigris and the Euphrates break into a number of channels in the marshland area , and the flow of the rivers is substantially reduced by the time they come together at Al Qurnah .
Moreover , the swamps act as silt traps , and the Shatt al Arab is relatively silt free as it flows south .
Below Basra , however , the Karun River enters the Shatt al Arab from Iran , carrying large quantities of silt that present a continuous dredging problem in maintaining a channel for ocean-going vessels to reach the port at Basra .
This problem has been superseded by a greater obstacle to river traffic , however , namely the presence of several sunken hulls that have been rusting in the Shatt al Arab since early in the Iran-Iraq war .
The waters of the Tigris and Euphrates are essential to the life of the country , but they sometimes threaten it .
In 1954 , for example , Baghdad was seriously threatened , and dikes protecting it were nearly topped by the flooding Tigris .
Since Syria built a dam on the Euphrates , the flow of water has been considerably diminished and flooding was no longer a problem in the mid-1980s .
In 1988 Turkey was also constructing a dam on the Euphrates that would further restrict the water flow .
Some attention was given to problems of flood control and drainage before the revolution of July 14 , 1958 , but development plans in the 1960s and 1970s were increasingly devoted to these matters , as well as to irrigation projects on the upper reaches of the Tigris and Euphrates and the tributaries of the Tigris in the northeast .
In the rural areas of the alluvial plain and in the lower Diyala region , settlement almost invariably clusters near the rivers , streams , and irrigation canals .
The pattern holds for farming communities in the Kurdish highlands of the northeast as well as for those in the alluvial plain .
Sometimes , particularly in the lower Tigris and Euphrates valleys , soil salinity restricts the area of arable land and limits the size of the community dependent on it , and it also usually results in large unsettled and uncultivated stretches between the villages .
For example , in the mid-1970s a substantial number of the residents of Baqubah , the administrative center and major city of Diyala Governorate , were employed in agriculture .
The Marsh Arabs ( the Madan ) of the south usually live in small clusters of two or three houses kept above water by rushes that are constantly being replenished .
Also , in early 1988 , the marshes had become the refuge of deserters from the Iraqi army who attempted to maintain life in the fastness of the overgrown , desolate areas while hiding out from the authorities .
The war has also affected settlement patterns in the northern Kurdish areas .
There , the persistence of a stubborn rebellion by Kurdish guerrillas has goaded the government into applying steadily escalating violence against the local communities .
Starting in 1984 , the government launched a scorched-earth campaign to drive a wedge between the villagers and the guerrillas in the remote areas of two provinces of Kurdistan in which Kurdish guerrillas were active .
In the process whole villages were torched and subsequently bulldozed , which resulted in the Kurds flocking into the regional centers of Irbil and As Sulaymaniyah .
The majority of Kurdish villages , however , remained intact in early 1988 .
Until the recent development of flood control , Baghdad and other cities were subject to the threat of inundation .
The growth of Baghdad , for example , was restricted by dikes on its eastern edge .
In April 1975 , an agreement signed in Baghdad fixed the borders of the countries .
In 1988 the boundary with Kuwait was another outstanding problem .
Land boundaries : total : 3631 km ( 2256 mi ) border countries : Iran 1458 km ( 906 mi ) , Saudi Arabia 814 km ( 506 mi ) , Syria 605 km ( 376 mi ) , Turkey 331 km ( 206 mi ) , Kuwait 242 km ( 150 mi ) , Jordan 181 km ( 112 mi )
