The United Nations considered adopting such a reformed calendar for a while in the 1950s , but these proposals have lost most of their popularity .
However , calculating for Easter Sunday is difficult because the calculation requires the knowledge of the full moon cycle .
There are only 14 different calendars when Easter Sunday is not involved .
However , when Easter Sunday is included , there are 70 different calendars .
Examples include the Prussian Academy of Sciences and the University of Helsinki , which had a monopoly on the sale of calendars in Finland until the 1990s .
