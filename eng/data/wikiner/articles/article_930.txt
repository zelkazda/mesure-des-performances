That same year , he was murdered at Cadiz while fleeing from a battle in which he had been deserted by the very supporters which had brought him into power .
His brief reign was similar to that of Abd ar-Rahman V Mostadir .
