Both Persia and Iran are used interchangeably in cultural contexts ; however , Iran is the name used officially in political contexts .
The 18th largest country in the world in terms of area at 1,648,195 km² , Iran has a population of over 74 million .
Iran is bordered on the north by Armenia , Azerbaijan and Turkmenistan .
As Iran is a littoral state of the Caspian Sea , which is an inland sea and condominium , Kazakhstan and Russia are also Iran 's direct neighbors to the north .
Iran is bordered on the east by Afghanistan and Pakistan , on the south by the Persian Gulf and the Gulf of Oman , on the west by Iraq and on the northwest by Turkey .
Tehran is the capital , the country 's largest city and the political , cultural , commercial and industrial center of the nation .
Iran is a regional power , and holds an important position in international energy security and world economy as a result of its large reserves of petroleum and natural gas .
Iran is home to one of the world 's oldest continuous major civilizations .
Iran is a founding member of the UN , NAM , OIC and OPEC .
In this inscription , the king 's appellation in Middle Persian contains the term ērān , while in the Parthian language inscription that accompanies it , the term aryān describes Iran .
In Ardeshir 's time , ērān retained this meaning , denoting the people rather than the state .
Notwithstanding this inscriptional use of ērān to refer to the Iranian peoples , the use of ērān to refer to the geographical empire is also attested in the early Sassanid period .
Now both terms are common , but " Iran " is used mostly in the modern political context and " Persia " in a cultural and historical context .
Since the Iranian Revolution of 1979 , the official name of the country has been the " Islamic Republic of Iran . "
Iran is the eighteenth largest country in the world , with an area of 1648000 km 2 ( 636000 sq mi ) .
Its borders are with Azerbaijan ( 432 km/268 mi ) and Armenia ( 35 km/22 mi ) to the north-west ; the Caspian Sea to the north ; Turkmenistan ( 992 km/616 mi ) to the north-east ; Pakistan ( 909 km/565 mi ) and Afghanistan ( 936 km/582 mi ) to the east ; Turkey ( 499 km/310 mi ) and Iraq ( 1458 km/906 mi ) to the west ; and finally the waters of the Persian Gulf and the Gulf of Oman to the south .
Iran consists of the Iranian Plateau with the exception of the coasts of the Caspian Sea and Khuzestan .
The eastern part consists mostly of desert basins such as the Dasht-e Kavir , Iran 's largest desert , in the north-central portion of the country , and the Dasht-e Lut , in the east , as well as some salt lakes .
The only large plains are found along the coast of the Caspian Sea and at the northern end of the Persian Gulf , where Iran borders the mouth of the Shatt al-Arab ( or the Arvand Rūd ) river .
Smaller , discontinuous plains are found along the remaining coast of the Persian Gulf , the Strait of Hormuz and the Sea of Oman .
On the northern edge of the country ( the Caspian coastal plain ) temperatures rarely fall below freezing and the area remains humid for the rest of the year .
To the west , settlements in the Zagros basin experience lower temperatures , severe winters with below zero average daily temperatures and heavy snowfall .
The coastal plains of the Persian Gulf and Gulf of Oman in southern Iran have mild winters , and very humid and hot summers .
The pheasant , partridge , stork , eagles and falcon are also native to Iran .
Iran has one of the highest urban growth rates in the world .
The United Nations predicts that by 2030 , 80 % of the population will be urban .
Most internal migrants have settled near the cities of Tehran , Isfahan , Ahvaz , and Qom .
Tehran , like many big cities , suffers from severe air pollution .
It is the centre of tourism in Iran and between 15 and 20 million pilgrims go to the Imam Reza 's shrine every year .
The growth of the suburban area around the city has turned Isfahan into Iran 's second most populous metropolitan area ( 3,430,353 ) .
Tabriz had been the second largest city in Iran until the late 1960s and one of its former capitals and residence of the crown prince under the Qajar dynasty .
The fifth major city is Karaj ( population 1,377,450 ) , located in Tehran province and situated 20 km west of Tehran , at the foot of the Alborz mountains ; however , the city is increasingly becoming an extension of metropolitan Tehran .
The Elamite civilization to the west greatly influenced the area which soon came to be known as Persis .
The ruins of Persepolis and Pasargadae , two of the four capitals of the Achaemenid Empire , are located in or near Shiraz .
Persepolis was the ceremonial capital of the Achaemenid Empire and is situated 70 km northeast of modern Shiraz .
Early agricultural communities such as Chogha Bonut in 8000 BC , Susa ( now a city still existing since 7000 BC ) and Chogha Mish dating back to 6800 BC .
The emergence of written records from around 3000 BC also parallels Mesopotamian history .
Other tribes began to settle on the eastern edge , as far as on the mountainous frontier of north-western Indian subcontinent and into the area which is now Balochistan .
Others , such as the Scythian tribes spread as far west as the Balkans and as far east as Xinjiang .
After Cyrus ' death , his son Cambyses continued his father 's work of conquest , making significant gains in Egypt .
Following a power struggle after Cambyses ' death , Darius I was declared king ( ruled 522 -- 486 BC ) .
Fighting ended with the peace of Callias in 449 BC .
The rules and ethics emanating from Zoroaster 's teachings were strictly followed by the Achaemenids who introduced and adopted policies based on human rights , equality and banning of slavery .
After the conquests of Media , Assyria , Babylonia and Elam , the Parthians had to organize their empire .
Parthia was the arch-enemy of the Roman Empire in the east , limiting Rome 's expansion beyond Cappadocia ( central Anatolia ) .
By using a heavily armed and armoured cataphract cavalry , and lightly armed but highly mobile mounted archers , the Parthians " held their own against Rome for almost 300 years " .
Rome 's acclaimed general Mark Antony led a disastrous campaign against the Parthians in 36 BC , in which he lost 32,000 men .
The end of the Parthian Empire came in 224 AD , when the empire was loosely organized and the last king was defeated by Ardashir I , one of the empire 's vassals .
Ardashir I then went on to create the Sassanid Empire .
During their reign , Sassanid battles with the Roman Empire caused such pessimism in Rome that the historian Cassius Dio wrote :
Iran was defeated in the Battle of al-Qâdisiyah , paving way for the Islamic conquest of Persia .
Under the Sassanids , Iran expanded relations with China .
Arts , music , and architecture greatly flourished , and centers such as the School of Nisibis and Academy of Gundishapur became world renowned centers of science and scholarship .
After the Islamic conquest of Persia , most of the urban lands of the Sassanid empire , with the exception of Caspian provinces and Transoxiana , came under Islamic rule .
Thus in 822 , the governor of Khorasan , Tahir , proclaimed his independence and founded a new Persian dynasty of Tahirids .
And by the Samanid era , Iran 's efforts to regain its independence had been well solidified .
Ferdowsi , Iran 's greatest epic poet , is regarded today as the most important figure in maintaining the Persian language .
After an interval of silence Iran re-emerged as a separate , different and distinctive element within Islam .
In 1218 , the eastern Khwarazmid provinces of Transoxiana and Khorasan suffered a devastating invasion by Genghis Khan .
He was followed by yet another conqueror , Tamerlane , who established his capital in Samarkand .
The waves of devastation prevented many cities such as Neishabur from reaching their pre-invasion population levels until the 20th century , eight centuries later .
In 1387 , Tamerlane avenged a revolt in Isfahan by massacring 70,000 people .
Arabic writer Ibn Khaldun has remarked that the sedentary culture which was necessary for the development of civilization was rooted in the Persian empire .
The New Persian language was an evolution of Middle Persian , which in turn was derived from Old Persian .
The Safavid peak was during the rule of Shah Abbas The Great .
The Safavid Dynasty frequently warred with the Ottoman Empire , Uzbek tribes and the Portuguese Empire .
The Safavids moved their capital from Tabriz to Qazvin and then to Isfahan , where their patronage for the arts propelled Iran into one of its most aesthetically productive eras .
The Zand dynasty lasted three generations , until Aga Muhammad Khan executed Lotf Ali Khan , and founded his new capital in Tehran , marking the dawn of the Qajar Dynasty in 1794 .
The Qajar chancellor Amir Kabir established Iran 's first modern college system , among other modernizing reforms .
In 1925 , Reza Khan overthrew the weakening Qajar Dynasty and became Shah .
The Shah was forced to abdicate in favor of his son , Mohammad Reza Pahlavi .
In 1951 , after the assassination of prime minister Ali Razmara , Dr. Mohammed Mossadegh , was elected prime minister by a parliamentary vote which was then ratified by the Shah .
As prime minister , Mossadegh became enormously popular in Iran after he nationalized Iran 's petroleum industry and oil reserves .
The operation was successful , and Mossadegh was arrested on 19 August 1953 .
The coup was the first time the US had openly overthrown an elected , civil government .
After Operation Ajax , Mohammad Reza Pahlavi 's rule became increasingly autocratic .
Ayatollah Ruhollah Khomeini became an active critic of the Shah 's White Revolution and publicly denounced the government .
Khomeini was arrested and imprisoned for 18 months .
After his release in 1964 Khomeini publicly criticized the U.S. government .
The Shah was persuaded to send him into exile by General Hassan Pakravan .
The Iranian Revolution , also known as the Islamic Revolution , began in January 1978 with the first major demonstrations against the Shah .
After strikes and demonstrations paralysed the country and its economy , the Shah fled the country in January 1979 and Ayatollah Khomeini returned from exile to Tehran .
The Pahlavi Dynasty collapsed ten days later , on 11 February , when Iran 's military declared itself " neutral " after guerrillas and rebel troops overwhelmed troops loyal to the Shah in armed street fighting .
Iran 's relationship with the United States deteriorated rapidly during the revolution .
While the student ringleaders had not asked for permission from Khomeini to seize the embassy , Khomeini nonetheless supported the embassy takeover after hearing of its success .
In January 1981 the hostages were set free according to the Algiers declaration .
The once-strong Iranian military had been disbanded during the revolution .
Saddam sought to expand Iraq 's access to the Persian Gulf by acquiring territories that Iraq had claimed earlier from Iran during the Shah 's rule .
On the unilateral behalf of the United Arab Emirates , the islands of Abu Musa and the Greater and Lesser Tunbs became objectives as well .
The war then continued for six more years until 1988 , when Khomeini , in his words , " drank the cup of poison " and accepted a truce mediated by the United Nations .
Rafsanjani served until 1997 when he was succeeded by the moderate reformist Mohammad Khatami .
During his two terms as president , Khatami advocated freedom of expression , tolerance and civil society , constructive diplomatic relations with other states including EU and Asian governments , and an economic policy that supported free market and foreign investment .
However , Khatami is widely regarded as having been unsuccessful in achieving his goal of making Iran more free and democratic .
The Interior Ministry , announced incumbent president Mahmoud Ahmadinejad had won the election with 62.63 % receiving 24.5 million vote , while Mir-Hossein Mousavi had come in second place with 13.2 milion votes 33,75 % .
The European Union and several western countries expressed concern over alleged irregularities during the vote , and some analysts and journalists from the United States and United Kingdom news media voiced doubts about the authenticity of the results .
Protests , in favour of Mousavi and against the alleged fraud , broke out in Tehran .
Khamenei then announced there would be an investigation into vote-rigging claims .
Accordingly , it is the duty of the Islamic government to furnish all citizens with equal and appropriate opportunities , to provide them with work , and to satisfy their essential needs , so that the course of their progress may be assured .
Unlike many other states , the executive branch in Iran does not control the armed forces .
As of 2008 , the Legislature of Iran ( also known as the Majlis of Iran ) is a unicameral body .
The Majlis of Iran comprises 290 members elected for four-year terms .
The Assembly of Experts , which meets for one week annually , comprises 86 " virtuous and learned " clerics elected by adult suffrage for eight-year terms .
Iran maintains diplomatic relations with almost every member of the United Nations , except for Israel , which Iran does not recognize , and the U.S. since the Iranian Revolution .
The U.S. Director of National Intelligence said in February 2009 that Iran would not realistically be able to a get a nuclear weapon until 2013 , if it chose to develop one .
Iran has a paramilitary , volunteer militia force within the IRGC , called the Basij , which includes about 90,000 full-time , active-duty uniformed members .
Up to 11 million men and women are members of the Basij who could potentially be called up for service ; GlobalSecurity.org estimates Iran could mobilize " up to one million men " .
In 2007 , Iran 's military spending represented 2.6 % of the GDP or $ 102 per capita , the lowest figure of the Persian Gulf nations .
Iran 's military doctrine is based on deterrence .
In recent years , official announcements have highlighted the development of weapons such as the Hoot , Kowsar , Zelzal , Fateh-110 , Shahab-3 and Sajjil missiles , and a variety of unmanned aerial vehicles .
Iran 's economy is a mixture of central planning , state ownership of oil and other large enterprises , village agriculture , and small-scale private trading and service ventures .
As of 2007 , Iran has earned $ 70 billion in foreign exchange reserves mostly ( 80 % ) from crude oil exports .
Iran 's official annual growth rate was at 6 % ( 2008 ) .
About 1,659,000 foreign tourists visited Iran in 2004 ; most came from Asian countries , including the republics of Central Asia , while a small share came from the countries of the European Union and North America .
Iran currently ranks 89th in tourist income , but is rated among the " 10 most touristic countries " in the world .
The administration continues to follow the market reform plans of the previous one and indicated that it will diversify Iran 's oil-reliant economy .
Iran has also developed a biotechnology , nanotechnology , and pharmaceuticals industry .
The strong oil market since 1996 helped ease financial pressures on Iran and allowed for Tehran 's timely debt service payments .
Today , Iran possesses a good manufacturing industry , despite restrictions imposed by foreign countries .
Iran ranks second in the world in natural gas reserves and third in oil reserves .
It is OPEC 's 2nd largest oil exporter .
In 2005 , Iran spent $ 4 billion on fuel imports , because of contraband and inefficient domestic use .
In 2004 , Iran opened its first wind-powered and geothermal plants , and the first solar thermal plant is to come online in 2009 .
Demographic trends and intensified industrialization have caused electric power demand to grow by 8 % per year .
Iran 's first nuclear power plant at Bushehr is set to go online by 2010 .
Turkic languages and dialects ( most importantly Azeri ) are spoken in different areas in Iran .
Additionally , Arabic is spoken in the southwestern parts of the country .
According to them Persian is spoken as a mother tongue by at least 65 % of the population and as a second language by a large proportion of the remaining 35 % .
However according to them Persian and its dialects are spoken as first language by 58 % while Azeri is spoken by 26 % , Kurdish by 9 % , Luri by 3 % , Balochi by 1 % , Arabic by 1 % and that some 2 % have other languages as first language .
Iran 's population increased dramatically during the latter half of the 20th century , reaching about 75 million by 2009 .
In recent years , however , Iran 's birth rate has dropped significantly .
Studies project that Iran 's rate of population growth will continue to slow until it stabilizes above 105 million by 2050 .
Iran hosts one of the largest refugee populations in the world , with more than one million refugees , mostly from Afghanistan and Iraq .
However the Bahá'í Faith , Iran 's largest religious minority , is not officially recognized , and has been persecuted during its existence in Iran .
Since the 1979 revolution the persecution of Bahá'ís has increased with executions , the denial of civil rights and liberties , and the denial of access to higher education and employment .
According to the Iranian Constitution , the government is required to provide every citizen of the country with access to social security that covers retirement , unemployment , old age , disability , accidents , calamities , health and medical treatment and care services .
The most noticeable one of them is commemoration of Husayn ibn Ali .
The Iranian New Year ( Nowruz ) is an ancient tradition celebrated on 21 March to mark the beginning of spring in Iran .
Iran is also famous for its caviar .
Iranian movies have won over three hundred awards in the past twenty-five years .
One of the best-known directors is Abbas Kiarostami .
The oldest records in Old Persian date to the Achaemenid Empire , and examples of Old Persian have been found in present-day Iran , Iraq , Turkey and Egypt .
Persian literature has been considered by such thinkers as Goethe as one of the four main bodies of world literature .
The main building types of classical Iranian architecture are the mosque and the palace .
Iran ranks seventh among countries in the world with the most archeological architectural ruins and attractions from antiquity as recognized by UNESCO .
The first windmill appeared in Iran in the 9th century .
Khwarizmi is widely hailed as the father of algebra .
The Academy of Gundishapur was a renowned centre of learning in the city of Gundeshapur during late antiquity and was the most important medical centre of the ancient world during the sixth and seventh centuries .
During this period , Persia became a centre for the manufacture of scientific instruments , retaining its reputation for quality well into the 19th century .
Iranian scientists are also helping construct the Compact Muon Solenoid , a detector for CERN 's Large Hadron Collider .
In the biomedical sciences , Iran 's Institute of Biochemistry and Biophysics is a UNESCO chair in biology .
Iran ranks 15th in the world in nanotechnologies .
The Iranian nuclear program was launched in the 1950s .
Iran is the 7th country in production of uranium hexafluoride .
Iran now controls the entire cycle for producing nuclear fuel .
Iran 's current facilities includes several research reactors , a uranium mine , an almost complete commercial nuclear reactor , and uranium processing facilities that include a uranium enrichment plant .
The Iranian Space Agency launched its first reconnaissance satellite named Sina-1 in 2006 , and a space rocket in 2007 , which aimed at improving science and research for university students .
Iran placed its domestically built satellite , Omid into orbit on the 30th anniversary of the Iranian Revolution , on February 2 , 2009 , through Safir rocket , becoming the ninth country in the world capable of both producing a satellite and sending it into space from a domestically made launcher .
Iranian scientists outside Iran have also made some major contributions to science .
In 1960 , Ali Javan co-invented the first gas laser and fuzzy set theory was introduced by Lotfi Zadeh .
Iran is home to several unique skiing resorts , with the Tochal resort being the world 's fifth-highest ski resort ( 3730 m/12238 ft at its highest station ) , and located only fifteen minutes away from Tehran .
Being a mountainous country , Iran is a venue for hiking , rock climbing , and mountain climbing .
