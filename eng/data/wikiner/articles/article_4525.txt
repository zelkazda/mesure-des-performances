As a respectable upper-class politician he supplied a center around which opposition to the dictatorship of Porfirio Díaz could coalesce .
His assassination was followed by the most violent period of the revolution ( 1913 -- 1917 ) until the Constitution of 1917 and revolutionary president Venustiano Carranza achieved some degree of stability .
Madero was educated at the Jesuit college in Saltillo , but this early Catholic education had little lasting impact .
In 1893 , the 20 year old Madero returned to Mexico and assumed management of the Madero family 's hacienda at San Pedro , Coahuila .
He installed new irrigation works , introduced American cotton , and built a soap factory and an ice factory .
On April 2 , 1903 , Bernardo Reyes , governor of Nuevo León , violently crushed a political demonstration , an example of the increasingly authoritarian policies of president Porfirio Díaz .
In 1905 , Madero became increasingly involved in opposition to the government of Porfirio Díaz .
Madero 's preferred candidate was again defeated by Porfirio Díaz 's preferred candidate in the 1905 governmental elections .
In a 1908 interview with U.S. journalist James Creelman published in Pearson 's Magazine , Porfirio Díaz said that Mexico was ready for a democracy and that the 1910 presidential election would be a free election .
Madero spent the bulk of 1908 writing a book at the directions of the spirits , which now included the spirit of Benito Juárez himself .
The book proclaimed that the concentration of absolute power in the hands of one man -- Porfirio Díaz -- for so long had made Mexico sick .
Madero acknowledged that Porfirio Díaz had brought peace and a measure of economic growth to Mexico .
However , Madero argued that this was counterbalanced by the dramatic loss of freedom which included the brutal treatment of the Yaqui people , the repression of workers in Cananea , excessive concessions to the U.S. , and an unhealthy centralization of politics around the person of the president .
Porfirio Díaz could either run in a free election or retire .
Madero sold off much of his property -- often at a considerable loss -- in order to finance anti-reelection activities throughout Mexico .
Madero traveled throughout Mexico giving antireelectionist speeches , and everywhere he went he was greeted by crowds of thousands .
Madero set out campaigning across the country and everywhere he was met by tens of thousands of cheering supporters .
Madero 's father used his influence with the state governor and posted a bond to gain Madero the right to move about the city on horseback during the day .
On October 4 , 1910 , Madero galloped away from his guards and took refuge with sympathizers in a nearby village .
As such , Madero decided to postpone the revolution .
In February 1911 he entered Mexico and led 130 men in an attack on Casas Grandes , Chihuahua .
He spent the next several months as the head of the Mexican Revolution .
Madero successfully imported arms from the United States , with the American government under William Howard Taft doing little to halt the flow of arms to the Mexican revolutionaries .
On April 1 , 1911 , Porfirio Díaz claimed that he had heard the voice of the people of Mexico , replaced his cabinet , and agreed to restitution of the lands of the dispossessed .
Madero then attended a meeting with the other revolutionary leaders -- they agreed to a fourteen-point plan which called for pay for revolutionary soldiers ; the release of political prisoners ; and the right of the revolutionaries to name several members of cabinet .
Madero was moderate , however .
The revolutionaries won this battle decisively and on May 21 , 1911 , the Treaty of Ciudad Juárez was signed .
Although Madero had forced Porfirio Díaz from power , he did not assume the presidency in June 1911 .
Madero now called for the disbanding of all revolutionary forces , arguing that the revolutionaries should henceforth proceed solely by peaceful means .
In the south , revolutionary leader Emiliano Zapata was skeptical about disbanding his troops , but Madero traveled south to meet with Zapata at Cuernavaca and Cuautla , Morelos .
Madero assured Zapata that the land redistribution promised in the Plan of San Luis Potosí would be carried out when Madero became president .
Madero once again traveled south to urge Zapata to disband his troops peacefully , but Zapata refused on the grounds that Huerta 's troops were advancing on Yautepec de Zaragoza .
Zapata 's suspicions proved accurate as Huerta 's troops moved violently into Yautepec de Zaragoza .
However , he promised the Zapatistas that once he became president , things would change .
Most Zapatistas had grown suspicious of Madero , however .
Madero became president in November 1911 , and , intending to reconcile the nation , appointed a cabinet which included many of Porfirio Díaz 's supporters .
After years of censorship , Mexican newspapers took the opportunity of their newfound freedom of the press to roundly criticize Madero 's performance as president .
Gustavo A. Madero , the president 's brother , remarked " the newspapers bite the hand that took off their muzzle . "
Francisco Madero refused the recommendation of some of his advisors that he bring back censorship , however .
The press was particularly critical of Madero 's handling of three rebellions that broke out against his rule shortly after he became president :
Huerta was more successful , defeating Orozco 's troops in three major battles and forcing Orozco to flee to the U.S. in September 1912 .
Huerta ordered Villa 's execution , but Madero commuted the sentence and Villa was set free .
This rebellion was quickly crushed and Félix Díaz was imprisoned .
Madero was prepared to have Félix Díaz executed , but the Supreme Court of Mexico declared that Félix Díaz would be imprisoned , but not executed .
Besides managing rebellions , Madero did have a number of accomplishments during his presidency :
Madero 's brother and advisor Gustavo A. Madero was kidnapped off the street , tortured , and killed .
Following Huerta 's coup d'état on February 18 , 1913 , Madero was forced to resign .
Francisco Madero was shot four days later , aged 39 .
