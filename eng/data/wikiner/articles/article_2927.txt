The name is derived from the Latin word clavis , meaning " key " ( associated with more common clavus , meaning " nail , rod , etc. " ) and chorda meaning " string , especially of a musical instrument " .
In the late 1890s , Arnold Dolmetsch revived clavichord construction and Violet Gordon-Woodhouse , among others , helped to popularize the instrument .
Guy Sigsworth has played clavichord in a rock setting with Bjork , notably on the studio recording of " All Is Full Of Love " .
The Beatles ' " For No One " ( 1966 ) features Paul McCartney playing the clavichord .
Among recent clavichord recordings , those by Christopher Hogwood , break new ground .
