Guinea , is a country in West Africa .
Formerly known as French Guinea ( Guinée française ) , it is today sometimes called Guinea-Conakry to distinguish it from its neighbor Guinea-Bissau .
Conakry is the capital , the seat of the national government , and the largest city .
Guinea has almost 246000 square kilometres ( 94981 sq mi ) .
It forms a crescent by curving from its western border on the Atlantic Ocean toward the east and the south .
Its northern border is shared with Guinea-Bissau , Senegal , and Mali , the southern one with Sierra Leone , Liberia , and Côte d'Ivoire .
The Niger River arises in Guinea and runs eastward .
Guinea 's 10,000,000 people belong to twenty-four ethnic groups .
The land that is now Guinea belonged to a series of empires until France colonized it in the 1890s , and made it part of French West Africa .
Guinea declared its independence from France on 2 October 1958 .
Since its independence , Guinea has had autocratic rulers who have made Guinea one of the poorest countries in the world .
Ahmed Sékou Touré became President upon Guinea 's independence .
By a quick coup d'état , Lansana Conté became the President after Touré .
By despotic means , Conté clung to power until his death in 2008 .
Despite extraordinary aluminium rich ressources , he was unable to improve the desperate economic plight into which Touré had plunged the country .
On 23 December 2008 , Moussa Dadis Camara seized control of Guinea as the head of a junta .
On 28 September 2009 , the junta ordered its soldiers to attack people who had gathered to protest any attempt by Camara to become President .
On 3 December 2009 , an aide shot Camara during a dispute about the rampage of September 2009 .
Vice-President ( and defense minister ) Sékouba Konaté flew back from Lebanon to run the country in Camara 's absence .
It was agreed that the military would not contest the forthcoming elections , and Camara would continue his convalescence outside Guinea .
On 21 January 2010 the military junta appointed Jean-Marie Doré as Prime Minister of a six-month transition government , leading up to elections .
Guinea is divided into four natural regions with distinct human , geographic , and climatic characteristics :
The national capital , Conakry , ranks as a special zone .
Its neighbours are Côte d'Ivoire ( Ivory Coast ) , Guinea-Bissau , Liberia , Mali , Senegal and Sierra Leone .
The highest point in Guinea is Mont Nimba at 1750 m .
Guinea has abundant natural resources including 25 % or more of the world 's known bauxite reserves .
Guinea also has diamonds , gold , and other metals .
Under French rule , and at the beginning of independence , Guinea was a major exporter of bananas , pineapples , coffee , peanuts , and palm oil .
In addition , Guinea 's mineral wealth includes more than 4-billion tonnes of high-grade iron ore , significant diamond and gold deposits , and undetermined quantities of uranium .
Guinea has considerable potential for growth in agricultural and fishing sectors .
Joint venture bauxite mining and alumina operations in northwest Guinea historically provide about 80 % of Guinea 's foreign exchange .
Guinea has the potential to develop , if the government carries out its announced policy reforms , and if the private sector responds appropriately .
So far , corruption and favoritism , lack of long-term political stability , and lack of a transparent budgeting process continue to dampen foreign investor interest in major projects in Guinea .
In July 1996 , President Lansana Conté appointed a new government , which promised major economic reforms , including financial and judicial reform , rationalization of public expenditures , and improved government revenue collection .
Under 1996 and 1998 International Monetary Fund ( IMF ) / World Bank agreements , Guinea continued fiscal reforms and privatization , and shifted governmental expenditures and internal reforms to the education , health , infrastructure , banking , and justice sectors .
Foreign investments outside Conakry are entitled to more favorable terms .
Guinea and the United States have an investment guarantee agreement that offers political risk insurance to American investors through the Overseas Private Investment Corporation ( OPIC ) .
In addition , Guinea has inaugurated an arbitration court system , which allows for the quick resolution of commercial disputes .
In 2002 , the IMF suspended Guinea 's Poverty Reduction and Growth Facility ( PRGF ) because the government failed to meet key performance criteria .
In reviews of the PRGF , the World Bank noted that Guinea had met its spending goals in targeted social priority sectors .
The Guinea franc was trading at 2550 to the dollar in January 2005 .
Despite the opening in 2005 of a new road connecting Guinea and Mali , most major roadways remain in poor repair , slowing the delivery of goods to local markets .
Even though there are many problems plaguing Guinea 's economy , not all foreign investors are reluctant to come to Guinea .
Alcoa and Alcan are proposing a slightly smaller refinery worth about $ 1.5 billion .
He said the firm would help build ports , railway lines , power plants , low-cost housing and even a new administrative centre in the capital , Conakry .
However , analysts say that the timing of the deal is likely to stir controversy , as the legitimacy of Guinea 's government is under question .
Guinea needs an adequate policy to address the concerns of the urban youth .
Many petroleum experts believe that the offshore region from Ghana west to Senegal , which includes Guinea , might one day supply the United States nearly 30 % of its oil needs before 2020 .
The railway which operated from Conakry to Kankan ceased operating in the mid-1980s [ citation needed ] .
Most vehicles in Guinea are 20+ years old , and cabs are any four-door vehicle which the owner has designated as being for hire .
Iron mining at Simandou in the southeast beginning in 2007 and at Kalia in the east is likely to result in the construction of a new heavy-duty standard gauge railway and deepwater port .
The population of Guinea is estimated at 10.2 million .
Conakry , the capital and largest city , is the hub of Guinea 's economy , commerce , education , and culture .
The official language of Guinea is French .
Other significant languages spoken are Maninka ( Malinke ) , Susu , Pular , Kissi , Kpelle , and Loma .
The population of Guinea comprises about 24 ethnic groups .
Islam is the majority religion .
There is a small Baha'i community .
The Guinean armed forces are divided into four branches .
By far the largest branch of the armed forces , with about 15,000 personnel , the army is mainly responsible for protecting the state borders , the security of administered territories , and defending Guinea 's national interests .
The force 's equipment includes several Russian-supplied fighter planes and transports .
Guinea has been reorganizing its health system since the Bamako Initiative of 1987 formally promoted community-based methods of increasing accessibility of drugs and health care services to the population , in part by implementing user fees .
The law provides for the protection and promotion of health and for the rights and duties of the individual , the family , and community throughout the territory of the Republic of Guinea .
Like other West African countries , Guinea has a rich musical tradition .
The group Bembeya Jazz became popular in the 1960s after Guinean independence .
