Cambodia is a country in Southeastern Asia , bordering the Gulf of Thailand , between Thailand , Vietnam , and Laos .
Its 2,572 km border is split among Vietnam ( 1,228 km ) , Thailand ( 803 km ) and Laos ( 541 km ) , as well as 443 km of coastline .
Roughly square in shape , the country is bounded on the north by Thailand and by Laos , on the east and southeast by Vietnam , and on the west by the Gulf of Thailand and by Thailand .
Mekong river basically runs through Asia except for some countries .
Total area 181,035 square kilometers , about the size of the U.S. state of Missouri ; country shares 803-kilometer border with Thailand on north and west , 541-kilometer border with Laos on northeast , 1,228-kilometer border with Vietnam on east and southeast , for a total of 2,572 kilometers of land borders ; coastline along Gulf of Thailand about 443 kilometres .
Cambodia falls within several well-defined geographic regions .
The basin and delta regions are rimmed with mountain ranges to the southwest by the Cardamom Mountains and the Elephant Range and to the north by the Dangrek Mountains .
The Cardamom Mountains in the southwest , oriented generally in a northwest-southeast direction , rise to more than 1,500 meters .
The highest mountain in Cambodia -- Phnom Aural , at 1,771 meters -- is in the eastern part of this range .
The Elephant Range , an extension running toward the south and the southeast from the Cardamom Mountains , rises to elevations of between 500 and 1,000 meters .
This area was largely isolated until the opening of the port of Kampong Saom ( formerly called Sihanoukville ) and the construction of a road and railroad connecting Kampong Saom , Kampot , Takev , and Phnom Penh in the 1960s .
The escarpment faces southward and is the southern edge of the Korat Plateau in Thailand .
The watershed along the escarpment marks the boundary between Thailand and Cambodia .
The Mekong Valley , which offers a communication route between Cambodia and Laos , separates the eastern end of the Dangrek Mountains and the northeastern highlands .
Cambodia 's climate , like that of the rest of Southeast Asia is dominated by monsoons , which are known as tropical wet and dry because of the distinctly marked seasonal differences .
Tropical cyclones that often devastate coastal Vietnam rarely cause damage in Cambodia .
Except for the smaller rivers in the southeast , most of the major rivers and river systems in Cambodia drain into the Tonle Sap or into the Mekong River .
The Cardamom Mountains and Elephant Range form a separate drainage divide .
To the east the rivers flow into the Tonle Sap , while on the west they flow into the Gulf of Thailand .
Toward the southern end of the Elephant Mountains , however , because of the topography , some small rivers flow southward on the eastern side of the divide .
The flow of water into the Tonle Sap is seasonal .
In September or in October , the flow of the Mekong River , fed by monsoon rains , increases to a point where its outlets through the delta can not handle the enormous volume of water .
After the Mekong 's waters crest -- when its downstream channels can handle the volume of water -- the flow reverses , and water flows out of the engorged lake .
As the level of the Tonle Sap retreats , it deposits a new layer of sediment .
The sediment deposited into the lake during the Mekong 's flood stage appears to be greater than the quantity carried away later by the Tonle Sap River .
The 800-kilometre boundary with Thailand , coincides with a natural feature , the watershed of the Dangrek Mountains , only in its northern sector .
Elevation extremes : lowest point : Gulf of Thailand 0 m highest point : Phnum Aoral 1,810 m
Environment -- current issues : illegal logging activities throughout the country and strip mining for gems in the western region along the border with Thailand have resulted in habitat loss and declining biodiversity ( in particular , destruction of mangrove swamps threatens natural fisheries ) ; soil erosion ; in rural areas , most of the population does not have access to potable water ; declining fish stocks because of illegal fishing and overfishing
