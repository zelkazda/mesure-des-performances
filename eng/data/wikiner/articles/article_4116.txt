Archaeological exploration of the Pre-Islamic period of Afghanistan began in Afghanistan in earnest after World War II and proceeded until the Soviet invasion of Afghanistan disrupted it in December 1979 .
The area that is now Afghanistan seems in prehistory , as well as in ancient and modern times , to have been closely connected by culture and trade with the neighbouring regions to the east , west , and north .
It is also believed that the region had early trade contacts with Mesopotamia .
They were an early tribe that forged the first empire on the present Iranian plateau and were rivals of the Persians whom they initially dominated in the province of Fars to the south .
Median domination of Afghanistan would last until the Persians challenged and ultimately replaced them from their original base in Fars in southern Iran near ancient Elam .
In what is today southern Iran , the Persians emerged to challenge Median supremacy on the Iranian plateau .
By 550 BC , the Persians had replaced Median rule with their own dominion and even began to expand past previous Median imperial borders .
Bactria had a special position in old Afghanistan , being the capital of a vice-kingdom .
Although distant provinces like Bactriana had often been restless under Achaemenid rule , Bactrian troops nevertheless fought in the decisive Battle of Gaugamela in 330 BC against the advancing armies of Alexander the Great .
It had taken Alexander only six months to conquer Persia ( Iran ) , but it took him nearly three years ( from about 330 BC -- 327 BC ) to subdue Afghanistan .
Their union reportedly produced one sole heir , Alexander IV , who was later killed in Greece by Cassander .
It reach its high point under Emperor Ashoka whose edicts , roads , and rest stops were found throughout the subcontinent .
Early in the 2nd century under Kanishka , the most powerful of the Kushan rulers , the empire reached its greatest geographic and cultural breadth to become a center of literature and art .
Kanishka was a patron of religion and the arts .
Sassanian control was tenuous at times as numerous challenges from Central Asian tribes led to instability and constant warfare in the region .
The Hephthalites ( or White Huns ) swept out of Central Asia around the fourth century into Bactria and to the south , overwhelming the last of the Kushan and Sassanian kingdoms .
Some have speculated that the name Afghanistan land of the Afghans derives from which could be an adjective such as brave , chivlarious , valour , which was to use for the people in today 's Afghanistan .
Historians believe that Hepthalite control continued for a century and was marked by constant warfare with the Sassanians to the west who exerted nominal control over the region .
By the middle of the sixth century the Hephthalites were defeated in the territories north of the Amu Darya ( the Oxus River of antiquity ) by another group of Central Asian nomads , the Göktürks , and by the resurgent Sassanians in the lands south of the Amu Darya .
The two massive sandstone Buddhas of Bamyan , thirty-five and fifty-three meters high overlooked the ancient route through Bamyan to Balkh and dated from the third and fifth centuries .
They survived until 2001 , when they were destroyed by the Taliban .
In this and other key places in Afghanistan , archaeologists have located frescoes , stucco decorations , statuary , and rare objects from as far away as China , Phoenicia , and Rome , which were crafted as early as the 2nd century and bear witness to the influence of these ancient civilizations upon Afghanistan .
