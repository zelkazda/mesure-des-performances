Grus is a constellation in the southern sky .
Its name is Latin for the crane , a species of bird .
The stars that form Grus were originally considered part of Piscis Austrinus ( the southern fish ) , and the Arabic names of many of its stars reflect this classification .
The stars were first defined as a separate constellation by Petrus Plancius , who created twelve new constellations based on the observations of Pieter Dirkszoon Keyser and Frederick de Houtman .
Its first depiction in a celestial atlas was in Johann Bayer 's Uranometria of 1603 .
