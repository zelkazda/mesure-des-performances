Many refer to Jesus Christ either directly or indirectly .
These hymns can be found in the United Methodist Hymnal .
Exceptions include the Coptic Orthodox tradition which makes use of the sistrum , and the Ethiopian Orthodox Tewahedo Church , which also uses drums , cymbals and other instruments on certain occasions .
The Protestant Reformation resulted in two conflicting attitudes to hymns .
All hymns that were not direct quotations from the Bible fell into this category .
The other Reformation approach , the normative principle of worship , produced a burst of hymn writing and congregational singing .
Martin Luther is notable not only as a reformer , but as the author of many hymns including Ein ' feste Burg ist unser Gott ( A Mighty Fortress Is Our God ) which is sung today even by Roman Catholics .
Luther and his followers often used their hymns , or chorales , to teach tenets of the faith to worshipers .
Wesley wrote :
As examples of the distinction , " Amazing Grace " is a hymn ( no refrain ) , but " How Great Thou Art " is a gospel song .
The most prominent names among Welsh hymn-writers are William Williams Pantycelyn and Ann Griffiths .
The second half of the nineteenth century witnessed an explosion of hymn tune composition and choir singing in Wales .
