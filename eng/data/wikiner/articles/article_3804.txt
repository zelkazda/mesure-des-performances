The sequential subset of Erlang is a functional language , with strict evaluation , single assignment , and dynamic typing .
It was designed by Ericsson to support distributed , fault-tolerant , soft-real-time , non-stop applications .
The first version was developed by Joe Armstrong in 1986 .
Erlang was originally a proprietary language within Ericsson , but was released as open source in 1998 .
While threads are considered a complicated and error-prone topic in most languages , Erlang provides language-level features for creating and managing processes with the aim of simplifying concurrent programming .
Though all concurrency is explicit in Erlang , processes communicate using message passing instead of shared variables , which removes the need for locks .
Erlang was designed with the aim of improving the development of telephony applications .
The initial version of Erlang was implemented in Prolog .
The philosophy used to develop Erlang fits equally well with the development of Erlang based systems .
A factorial algorithm implemented in Erlang :
Erlang has eight primitive data types : And two compound data types : Two forms of syntactic sugar are provided :
Erlang 's main strength is support for concurrency .
Processes are the primary means to structure an Erlang application .
Erlang processes loosely follow the communicating sequential processes model ( CSP ) .
They are neither operating system processes nor operating system threads , but lightweight processes somewhat similar to Java 's original " green threads " .
A message may comprise any Erlang structure , including primitives ( integers , floats , characters , atoms ) , tuples , lists , and functions .
Concurrency supports the primary method of error-handling in Erlang .
This is also used , for example , in the Erlang shell .
In practice , systems are built up using design principles from the Open Telecom Platform which leads to more code upgradable designs .
Successful hot code loading is a tricky subject ; code needs to be written to make use of Erlang 's facilities .
In 1998 , Ericsson released Erlang as open source to ensure its independence from a single vendor and to increase awareness of the language .
Erlang , together with libraries and the real-time distributed database Mnesia , forms the Open Telecom Platform ( OTP ) collection of libraries .
Ericsson and a few other companies offer commercial support for Erlang .
Since the open source release , Erlang has been used by several firms worldwide , including Nortel and T-Mobile .
Although Erlang was designed to fill a niche and has remained an obscure language for most of its existence , its popularity is growing due to demand for concurrent services .
Erlang has inspired several clones of its concurrency facilities for other languages :
