Eureka is a city located in St. Louis County , Missouri , U.S. , between St. Louis , Missouri and Pacific , Missouri along Interstate 44 .
The city is two miles ( 3 km ) west of the former site of Times Beach , the site of dioxin contamination discovered in the 1980s ; the area was cleaned up and became Route 66 State Park .
Since 1971 , Eureka has been known as the home of the amusement park formerly known as Six Flags Over Mid-America and now called Six Flags St. Louis .
The village of Eureka was platted in 1858 along the route of the Pacific Railroad .
Thus , Eureka was founded .
Eureka was incorporated as a fourth-class city on April 7 , 1954 .
In 1985 it was annexed by the city of Eureka .
Allenton was declared blighted by St. Louis County , Missouri in 1973 .
The redevelopment proposal would include land and homes purchased by Eureka as part of a previously proposed redevelopment plan .
Eureka is known for its antique shopping .
Once a year the candlelight walk is held in old town Eureka .
According to the United States Census Bureau , the city has a total area of 10.1 square miles ( 26.2 km² ) , of which , 10.1 square miles ( 26.0 km² ) of it is land and 0.1 square miles ( 0.2 km² ) of it is water .
