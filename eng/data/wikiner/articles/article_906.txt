They divided the government in four but Aegeas became king .
Aegeus did not understand the prophecy and was disappointed .
This puzzling oracle , forced Aegeus to visit Pittheus , king of Troezen , who was famed for his wisdom and skill at expounding oracles .
Pittheus understood the prophecy and introduced Aegeus to his daughter , Aethra , when he was drunk .
They slept with each other and then , in some versions , Aethra waded out to the sea to the island of Sphairia , and bedded also with Poseidon .
When she fell pregnant , Aegeus decided to go back to Athens .
Upon his return to Athens , Aegeus married Medea who had fled from Corinth and the wrath of Jason .
While visiting in Athens , King Minos ' son , Androgeus " breeder of men " , managed to defeat Aegeus in every contest during the Panathenaic Games .
Minos was angry and declared war on Athens .
He offered the Athenians peace , however , under the condition that Athens would send seven young men and seven young women every nine years to Crete to be fed to the Minotaur , a vicious monster .
This continued until Theseus killed the Minotaur with the help of Ariadne , Minos ' daughter .
In Troezen , Theseus grew up and became a brave young man .
His mother then told him the identity of his father and that he should take the weapons back to him at Athens and be acknowledged .
Theseus decided to go to Athens and had the choice of going by sea , which was the safe way , or by land , following a dangerous path with thieves and bandits all the way .
Young , brave and ambitious , Theseus decided to go to Athens by land .
When Theseus arrived , he did not reveal his true identity .
He was welcomed by Aegeas , who was suspicious about the stranger who came to Athens .
She tried to poison him , but at the last second , Aegeas recognized his sword and knocked the poisoned cup out of Theseus ' hand .
Theseus departed for Crete .
Upon his departure , Aegeus told him to put up the white sails when returning if he was successful in killing the Minotaur .
However , when Theseus returned he forgot these instructions .
When Aegeus saw the black sails coming into Athens he jumped into the sea and drowned , mistaken in his belief that his son had been slain .
Henceforth , this sea was known as the Aegean Sea .
Sophocles ' tragedy Aegeus has been lost , but Aegeus features in Euripedes ' Medea .
