At the age of nineteen , she left Haworth working as a governess between 1839 and 1845 .
She wrote a volume of poetry with her sisters ( Poems by Currer , Ellis , and Acton Bell , 1846 ) and in short succession she wrote two novels .
Agnes Grey , based upon her experiences as a governess , was published in 1847 .
Her second and last novel , The Tenant of Wildfell Hall appeared in 1848 .
Anne 's life was cut short with her death of pulmonary tuberculosis when she was 29 years old .
Anne Brontë is often overshadowed by her more famous sisters , Charlotte , author of four novels including Jane Eyre ; and Emily , author of Wuthering Heights .
Anne 's two novels , written in a sharp and ironic style , are completely different from the romanticism followed by her sisters .
Her novels , like those of her sisters , have become classics of English literature .
In 1802 , at the age of twenty-six , he won a place at Cambridge to study theology at St. John 's College .
In 1807 he was ordained in the priesthood in the Church of England .
The following year he was appointed an examiner of Bible knowledge at a Wesleyan academy , Woodhouse Grove School .
There , at age thirty-five , he met his future wife , Maria Branwell , the headmaster 's niece .
Though from vastly different backgrounds , within three months Patrick Brontë and Maria Branwell were married on 29 December 1812 .
Four more children would follow : Charlotte , ( 1816 -- 1855 ) , Patrick Branwell ( 1817 -- 1848 ) , Emily , ( 1818 -- 1848 ) and Anne ( 1820 -- 1849 ) .
When Anne was born , her father was the curate of Thornton and she was baptised there on 25 March 1820 .
Shortly after , Anne 's father took a perpetual curacy , a secure but not enriching vocation , in Haworth , a remote small town some seven miles ( 11 km ) away .
This five-room building became the Brontës ' family home for the rest of their lives .
Anne was barely a year old when her mother became ill of what is believed to have been uterine cancer .
Maria Branwell died on 15 September 1821 .
There was little affection between her and the eldest children , but to Anne , her favorite according to tradition , she did relate .
Anne shared a room with her aunt , they were particularly close , and this may have strongly influenced Anne 's personality and religious beliefs .
In Elizabeth Gaskell 's biography , Anne 's father remembered her as precocious , reporting that once , when she was four years old , in reply to his question about what a child most wanted , " she answered : age and experience " .
For the next five years , all the Brontë children were educated at home , largely by their father and aunt .
The young Brontës made little attempt to mix with others outside the parsonage , but relied upon each other for friendship and companionship .
The bleak moors surrounding Haworth became their playground .
Anne 's studies at home included music and drawing .
Those readings fed the Brontës ' imaginations .
The children 's creativity soared after their father presented Branwell with a set of toy soldiers in June 1826 .
She described Anne at this time : " Anne , dear gentle Anne was quite different in appearance from the others , and she was her aunt 's favourite .
Within a few months , Emily was unable to adapt to life at school , and by October , was physically ill from homesickness .
She was withdrawn from the school and replaced by Anne .
At some point before December 1837 , Anne became seriously ill with gastritis and underwent a religious crisis .
Little is known about Anne 's life during 1838 , but in 1839 , a year after leaving the school and at the age of nineteen , she was actively looking for a teaching position .
The children in Anne 's charge were spoilt and wild , and persistently disobeyed and tormented her .
Twenty-five years old , he had obtained a two-year licentiate in theology from the University of Durham .
It may or may not be relevant that the source of Agnes Grey ' s renewed interest in poetry is the curate to whom she is attracted .
It seems clear that he was a good-looking , engaging young man , whose easy humour and kindness towards the Brontë sisters made a considerable impression .
Nor does it follow that Anne believed him to be interested in her .
Anne expressed her grief for his death in her poem " I will not mourn thee , lovely one " , in which she called him " our darling " .
Anne missed her home and family , commenting in a diary paper in 1841 that she did not like her situation and wished to leave it .
However , despite her outwardly placid appearance , Anne was determined and with the experience she gradually gained , she eventually made a success of her position , becoming well liked by her new employers .
She was also obliged to accompany the family on their annual holidays to Scarborough .
Between 1840 and 1844 , Anne spent around five weeks each summer at the resort , and loved the place .
A number of locations in Scarborough formed the setting for Agnes Grey ' s final scenes .
Anne 's vaunted calm appears to have been the result of hard-fought battles , balancing deeply felt emotions with careful thought , a sense of responsibility , and resolute determination .
All three Brontë sisters had spent time working as governesses or teachers , and all had experienced problems controlling their charges , gaining support from their employers , and coping with homesickness -- but Anne was the only one who persevered and made a success of her work .
When Anne and her brother returned home for the holidays in June 1845 , she resigned her position .
Branwell was sternly dismissed when his employer found out about his relationship with his wife .
Once free of her position as a governess , Anne took Emily to visit some of the places she had come to know and love in the past five years .
An initial plan of going to the sea at Scarborough fell through , and the sisters went instead to York , where Anne showed her sister the York Minster .
Anne also revealed her own poems .
They told neither Branwell , nor their father , nor their friends about what they were doing .
Poems by Currer , Ellis , and Acton Bell was available for sale in May 1846 .
On 7 May 1846 , the first three copies of the book were delivered to Haworth Parsonage .
Anne , however , began to find a market for her more recent poetry .
However , Jane Eyre was the first to appear in print .
Meanwhile , Anne and Emily were obliged to pay fifty pounds to help meet the publishing costs .
Their publisher , urged on by the success of Jane Eyre , finally published Emily 's Wuthering Heights and Anne 's Agnes Grey in December 1847 .
These too sold exceptionally well , but Agnes Grey was distinctly outshone by Emily 's much more dramatic Wuthering Heights .
Anne 's second novel , The Tenant of Wildfell Hall , was published in the last week of June 1848 .
The Tenant of Wildfell Hall is perhaps the most shocking of the Brontës ' novels .
In seeking to present the truth in literature , Anne 's depiction of alcoholism and debauchery were profoundly disturbing to nineteenth century readers .
Anne 's heroine eventually leaves her husband to protect their young son from his influence .
In the second edition of The Tenant of Wildfell Hall , which appeared in August 1848 , Anne clearly stated her intentions in writing it .
Anne also sharply castigated reviewers who speculated on the sex of the authors , and the appropriateness of their writing to their sex , in words that do little to reinforce the stereotype of Anne as meek and gentle .
Only in their late twenties , a highly successful literary career appeared a certainty for Anne and her sisters .
Within the next ten months , three of the siblings , including Anne , would be dead .
Branwell 's health had gradually deteriorated over the previous two years , but its seriousness was half disguised by his persistent drunkenness .
The whole family had suffered from coughs and colds during the winter of 1848 and it was Emily who next became severely ill .
Emily 's death deeply affected Anne and her grief further undermined her physical health .
Her symptoms intensified , and in early January , her father sent for a Leeds physician , who diagnosed her condition as consumption , and intimated that it was quite advanced leaving little hope of a recovery .
Anne met the news with characteristic determination and self-control .
Unlike Emily , Anne took all the recommended medicines , and responded to all the advice she was given .
In February 1849 , Anne seemed somewhat better .
By this time , she had decided to make a return visit to Scarborough in the hope that the change of location and fresh sea air might initiate a recovery , and give her a chance to live .
En route , the three spent a day and a night in York , where , escorting Anne around in a wheelchair , they did some shopping , and at Anne 's request , visited the colossal York Minster .
However , it was clear that Anne had little strength left .
Anne received the news quietly .
Anne was buried not in Haworth with the rest of her family , but in Scarborough .
The funeral was held on Wednesday , 30 May , which did not allow time for Patrick Brontë to make the 70-mile ( 110 km ) trip to Scarborough , had he wished to do so .
P. Brontë , Incumbent of Haworth , Yorkshire .
Anne was actually twenty-nine at her death .
This act was the predominant cause of Anne 's relegation to the back seat of the Brontë bandwagon .
The controlled , reflective camera eye of Agnes Grey is closer to Jane Austen 's Persuasion than to Charlotte Brontë 's Jane Eyre .
The painstaking realism and social criticism of The Tenant of Wildfell Hall directly counters the romanticized violence of Wuthering Heights .
Anne 's religious concerns , reflected in her books and expressed directly in her poems , were not concerns shared by her sisters .
Anne 's subtle prose has a fine ironic edge ; her novels also reveal Anne to be the most socially radical of the three .
A re-appraisal of Anne 's work has begun , gradually leading to her acceptance , not as a minor Brontë , but as a major literary figure in her own right .
