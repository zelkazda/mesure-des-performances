Comet Hale-Bopp ( formally designated C/1995 O1 ) was arguably the most widely observed comet of the twentieth century , and one of the brightest seen for many decades .
Although predicting the brightness of comets with any degree of accuracy is very difficult , Hale-Bopp met or exceeded most predictions when it passed perihelion on April 1 , 1997 .
The passage of Hale-Bopp was notable also for inciting a comet-related panic .
Rumours that an alien spacecraft was following the comet gained remarkable currency , and inspired a mass suicide among followers of the Heaven 's Gate cult .
The comet was discovered in 1995 by two independent observers , Alan Hale and Thomas Bopp , both in the U.S. .
Hale had spent many hundreds of hours searching for comets without success , and was tracking known comets from his driveway in New Mexico when he chanced upon Hale-Bopp just after midnight .
The comet had an apparent magnitude of 10.5 and lay near the globular cluster M70 in the constellation of Sagittarius .
Hale first established that there was no other deep-sky object near M70 , and then consulted a directory of known comets , finding that none were known to be in this area of the sky .
Once he had established that the object was moving relative to the background stars , he emailed the Central Bureau for Astronomical Telegrams , the clearing house for astronomical discoveries .
Bopp did not own a telescope .
He realized he might have spotted something new when , like Hale , he checked his star maps to determine if any other deep-sky objects were known to be near M70 , and found that there were none .
I mean , by the time that telegram got here , Alan Hale had already e-mailed us three times with updated coordinates . "
The following morning , it was confirmed that this was a new comet , and it was named Comet Hale-Bopp , with the designation C/1995 O1 .
The discovery was announced in International Astronomical Union circular 6187 .
In Pepi 's pyramid in Saqqara is a text referring to an " nhh-star " as a companion of the pharaoh in the heavens , where " nhh " is the hieroglyph for long hair .
Most comets at this distance are extremely faint , and show no discernible activity , but Hale-Bopp already had an observable coma .
An image taken at the Anglo-Australian Telescope in 1993 was found to show the then-unnoticed comet some 13 AU from the sun , a distance at which most comets are essentially unobservable .
Its great distance and surprising activity indicated that Comet Hale-Bopp might become very bright indeed when it reached perihelion in 1997 .
Comet Kohoutek in 1973 had been touted as a ' comet of the century ' and turned out to be unspectacular .
Hale-Bopp became visible to the naked eye in May 1996 , and although its rate of brightening slowed considerably during the latter half of that year , scientists were still cautiously optimistic that it would become very bright .
The Internet was a growing phenomenon at the time , and numerous websites that tracked the comet 's progress and provided daily images from around the world became extremely popular .
The Internet played a large role in encouraging the unprecedented public interest in comet Hale-Bopp .
It shone brighter than any star in the sky except Sirius , and its dust tail stretched 40 -- 45 degrees across the sky .
However , in April 1996 the comet passed within 0.77 AU of Jupiter , close enough for its orbit to be affected by the planet 's gravity .
Hale-Bopp has about a 15 % chance of eventually becoming a sungrazing comet through this process .
It has been calculated that the previous visit by Hale-Bopp occurred in July 2215 BC .
Hale-Bopp may have had a near collision with Jupiter in early June 2215 BC , which probably caused a dramatic change in its orbit , and 2215 BC may have been its first passage through the inner solar system .
Comet Hale-Bopp was observed intensively by astronomers during its perihelion passage , and several important advances in cometary science resulted from these observations .
It is also to be noted that the comet Hale-Bopp showed highest ever linear polarization detected for any comet .
It further confirms that the dust grains in the coma of comet Hale-Bopp were smaller than inferred in any other comet .
In addition to the well-known gas and dust tails , Hale-Bopp also exhibited a faint sodium tail , only visible with powerful instruments with dedicated filters .
Hale-Bopp 's sodium tail consisted of neutral atoms ( not ions ) , and extended to some 50 million kilometres in length .
It is not yet established which mechanism is primarily responsible for creating Hale-Bopp 's sodium tail , and the narrow and diffuse components of the tail may have different origins .
Theoretical modelling of ice formation in interstellar clouds suggests that Comet Hale-Bopp formed at temperatures of around 25 -- 45 kelvins .
Hale-Bopp was the first comet where the noble gas argon was detected .
Comet Hale-Bopp 's activity and outgassing were not spread uniformly over its nucleus , but instead came from several specific jets .
In 1999 a paper was published that hypothesised the existence of a binary nucleus to fully explain the observed pattern of Comet Hale-Bopp 's dust emission .
The findings of this paper were disputed by observational astronomers , as even with the Hubble Space Telescope images of the comet revealed no trace of a double nucleus .
UFO enthusiasts , such as remote viewing proponent Courtney Brown , soon concluded that there was an alien spacecraft following the comet .
Later , Art Bell even claimed to have obtained an image of the object from an anonymous astrophysicist who was about to confirm its discovery .
A few months later , in March 1997 , the Heaven 's Gate cult chose the appearance of the comet as a signal for their mass cult suicide .
For instance , 69 % of Americans had seen Hale-Bopp by April 9 , 1997 .
It was a record-breaking comet -- the furthest comet from the Sun discovered by amateurs , with the largest well-measured cometary nucleus known after 95P/Chiron , and it was visible to the naked eye for twice as long as the previous record-holder .
