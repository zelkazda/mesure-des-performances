In particular , it is evident from the ruins of Buhen , Mohenjo-daro and Harappa .
In pre-modern China , brick-making was the job of a lowly and unskilled artisan , but a kiln master was respected as a step above the former .
Early traces of bricks were found in a ruin site in Xi'an in 2009 dated back about 3800 years ago .
The idea of signing the worker 's name and birth date on the brick and the place where it was made was not new to the Ming era and had little or nothing to do with vanity .
As far back as the Qin Dynasty ( 221 BC -- 206 BC ) , the government required blacksmiths and weapon-makers to engrave their names onto weapons in order to trace the weapons back to them , lest their weapons should prove to be of a lower quality than the standard required by the government .
During the building boom of the nineteenth century in the eastern seaboard cities of Boston and New York City , for example , locally made bricks were often used in construction in preference to the brownstones of N.J. and Connecticut for these reasons .
In India , brick making is typically a manual process .
It is very common in Sweden , especially in houses built or renovated in the ' 70s .
Historically , this meant that bigger bricks were necessary in colder climates ( see for instance the slightly larger size of the Russian brick in table below ) , while a smaller brick was adequate , and more economical , in warmer regions .
In England , the length and the width of the common brick has remained fairly constant over the centuries , but the depth has varied from about two inches ( about 51 mm ) or smaller in earlier times to about two and a half inches ( about 64 mm ) more recently .
In the United States , modern bricks are usually about 8 × 4 × 2.25 inches ( 203 × 102 × 57 mm ) .
In the United Kingdom , the usual ( " work " ) size of a modern brick is 215 × 102.5 × 65 mm ( about 8.5 × 4 × 2.5 inches ) , which , with a nominal 10 mm mortar joint , forms a " coordinating " or fitted size of 225 × 112.5 × 75 mm , for a ratio of 6:3:2 .
The compressive strength of bricks produced in the United States ranges from about 1000 lbf/in² to 15,000 lbf/in² , varying according to the use to which the brick are to be put .
In the USA , brick pavement was found incapable of withstanding heavy traffic , but it is coming back into use as a method of traffic calming or as a decorative surface in pedestrian precincts .
For example , in the early 1900s , most of the streets in the city of Grand Rapids , Michigan were paved with brick .
There is a large refractory brick industry , especially in the U.K. , Japan and the U.S. .
In the United Kingdom , bricks have been used in construction for centuries .
Although many houses in the UK are now built using a mixture of concrete blocks and other materials , many houses are skinned with a layer of bricks on the outside for aesthetic appeal .
