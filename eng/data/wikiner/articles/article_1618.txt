There was an extremely pious man named Job .
God asks Satan his opinion on Job , apparently a truly pious man .
All of Job 's possessions are destroyed and a ' ruach ' ( wind/spirit ) causes the house of the firstborn to collapse killing all of Job 's offspring who were gathered for a feast .
Satan , therefore , smites him with dreadful boils , and Job , seated in ashes , scrapes his skin with broken pottery .
Three friends of Job , Eliphaz the Temanite , Bildad the Shuhite , and Zophar the Naamathite , come to console him .
The friends spend 7 days sitting on the ground with Job , without saying anything to him because they see that he is suffering and in much pain .
Job at last breaks his silence and " curses the day he was born " .
As the speeches progress , Job 's friends increasingly berate him for refusing to confess his sins , although they themselves are at a loss as to which sin he has committed .
Elihu says he spoke last because he is much younger than the other three friends , but says that age makes no difference when it comes to insights and wisdom .
Elihu takes a distinct view of the kind of repentance required by Job .
Job 's three friends claim that repentance requires Job to identify and renounce the sins that gave rise to his suffering .
God 's answer underscores that Job shares the world with numerous powerful and remarkable creatures .
( Also compare Job 41:18-21 with Job 15:12-13 which was possibly in response to Job 7:11-16 ) .
Job is restored to health , gaining double the riches he possessed before and having 7 sons and 3 daughters ( his wife did not die in this ordeal ) .
Job is blessed once again and lives on another 140 years after the ordeal , living to see his children to the fourth generation and dying peacefully of old age .
The term " Satan " appears in the prose prologue of Job , with Satan 's usual connotation of " the adversary " , as a distinct being .
The dialogue that ensues characterizes Satan as a member of the divine council who observes human activity , but with the purpose of searching out men 's sins and appearing as their accuser .
Satan challenges Job 's righteousness by saying that his belief is built only upon the material goods he has been given , and that his faith will disappear as soon as they are taken from him .
Throughout the ordeal , she survives and lives on with Job to bear him ten more children .
) , but it is clear that Job honors her by the way he talks about her in chapter 31 .
In this regard , it is notable that the Hebrew word in the Masoretic text most often translated as " curse " is " ברך, " which primarily means to " kneel " or to " bless " and only euphemistically denotes " curse . "
Job 's critical response to her may then be interpreted as challenging the notion that , when undergoing severe suffering , one should necessarily be resigned to one 's imminent demise .
While " there is an intentional editorial unity with a cohesive purpose and message in the canonical form of the book, " Job contains many separate elements , some of which may have had an independent existence prior to being incorporated into the present text .
The medieval exegete Abraham ibn Ezra believed that Job was translated from another language and it is therefore unclear " like all translated books " ( Ibn Ezra Job 2:11 ) .
If the prologue and epilogue were added to the central poem , then this would have happened before 100 BCE or the time attributed to the Dead Sea Scrolls .
Job is prominent in haggadic legends .
Professor Kramer drew an inference that the Hebrew version is in some way derived from a Sumerian predecessor .
The most common such claims are of two kinds : the " parallel texts " , which are parallel developments of the corresponding passages in the base text , and the speeches of Elihu , which consist of a polemic against the ideas expressed elsewhere in the poem , and so are claimed to be interpretive interpolations .
The speeches of Elihu ( who is not mentioned in the prologue ) are claimed to contradict the fundamental opinions expressed by the " friendly accusers " in the central body of the poem , according to which it is impossible that the righteous should suffer , all pain being a punishment for some sin .
Subjects of further contention among scholars are the identity of claimed corrections and revisions of Job 's speeches , which are claimed to have been made for the purpose of harmonizing them with the orthodox doctrine of retribution .
A prime example of such a claim is the translation of the last line Job speaks ( 42:6 ) , which is extremely problematic in the Hebrew .
This is consonant with the central body of the poem and Job 's speeches , other mortal encounters with the divine in the Bible , and the fact that there would have been no restoration without Job 's humble repentant acknowledgment of mortality faced with divinity in all its majesty and glory .
Exegesis of Job largely concerns the question , " Is misfortune always a divine punishment for something ? "
Job 's three friends argued in the affirmative , stating that Job 's misfortunes were proof that he had committed some sins for which he was being punished .
His friends also advanced the converse position that good fortune is always a divine reward , and that if Job would renounce his supposed sins , he would immediately experience the return of good fortune .
In response , Job asserted that he was a righteous man , and that his misfortune was therefore not a punishment for anything .
Instead , Job responded with equanimity : " The Lord gave , and the Lord has taken away ; blessed be the name of the Lord . "
Some see it as an attempt to humble Job .
Job chapter 28 rejects these efforts to fathom divine wisdom .
There are legendary details such as the fate of Job 's wife , the inheritance of Job 's daughters , and the ancestry of Job .
Like many other Testament of ... works in the Old Testament apocrypha , it gives the narrative a framing-tale of Job 's last illness , in which he calls together his sons and daughters to give them his final instructions and exhortations .
Job is equally portrayed differently ; Satan is shown to directly attack Job , but fails each time due to Job 's willingness to be patient , unlike the Biblical narrative where Job falls victim but retains faith .
The latter section of the work , dedicated like the Biblical text to Job 's comforters , deviates even further from the Biblical narrative .
The Talmud occasionally discusses Job .
Most traditional Torah scholarship has not doubted Job 's existence .
There is a minority view among the rabbis of the Talmud , that of Rabbi Shimon ben Lakish , that Job never existed .
In this view , Job was a literary creation by a prophet who used this form of writing to convey a divine message or parable .
On the other hand , the Talmud goes to great lengths trying to ascertain when Job actually lived , citing many opinions and interpretations by the leading sages .
Job is further mentioned in the Talmud as follows :
Two further Talmudic traditions hold that Job either lived in the time of Abraham or of Jacob .
Others argue that it was written by Job himself ( see Job 19:23-24 ) , or by Elihu , or Isaiah .
Some of the laws and customs of mourning in Judaism are derived from the Book of Job 's depiction of Job 's mourning and the behavior of his companions .
Maimonides , a twelfth century rabbi , discusses Job in his work The Guide for the Perplexed .
According to Maimonides , the correct view of providence lies with Elihu , who teaches Job that one must examine his/her religion ( Job 33 ) .
A habit religion , such as that originally practiced by Job , is never enough .
Elihu believed in the concepts of divine providence , rewards to individuals , as well as punishments .
He believed , according to Maimonides , that one has to practice religion in a rational way .
In the beginning , Job was an unexamining , pious man , not a philosopher , and he did not have providence .
God , according to Elihu , did not single out Job for punishment , but rather abandoned him and let him be dealt with by natural , unfriendly forces .
In fact , Shestov used the story of Job as a central signifier for his core philosophy :
" The whole book is one uninterrupted contest between the ' cries ' of the much-afflicted Job and the ' reflections ' of his rational friends .
The friends , as true thinkers , look not at Job but at the ' general . '
Job , however , does not wish to hear about the ' general ' ; he knows that the general is deaf and dumb -- and that it is impossible to speak with it .
And , indeed , shall rocks really be removed from their place for the sake of Job ?
( Speculation and Apocalypse ) .
According to the mystical approach , Job is being punished because he is a heretic .
According to Nachmanides , Job 's children did not die in the beginning of the story , but rather were taken captive and then return from captivity by the end of the story .
The character of Job is also mentioned in the New Testament , as an example of perseverance in suffering .
4:14 to Job 7:6 ; compare Heb. 2:6 with Job 7:17 ; compare Heb. 12:26 with Job 9:6 ; Rom. 9:20 alludes to Job 9:32 ; Rom .
11:33 parallels Job 10:7 ; compare Acts 17:28 with Job 12:10 ; compare 1 Cor. 4:5 with Job 12:22 ; compare 1 Pet. 1:24 with Job 14:2 ; compare Lk. 19:22 with Job 15:6 ; Rom .
1:9 parallels Job 16:19 ; compare 1 John 3:2 with Job 19:26 ; Rev. 14:10 , 19:15 parallel Job 21:20 ; both Rom .
4:6 and 1 Pet .
1:5 with Job 32:8 ; 1 Jo .
5:4 alludes to Job 34:28 ; Rev. 16:21 alludes to Job 38:22-23 ; Mt. 6:26 alludes to Job 38:41 ; and finally , Rom .
11:35 quotes Job 41:11 .
( see Good News Bible special edition )
This idea of a divine arbiter is returned to at Job 16:19 .
Job 's faith in this arbiter is again brought up in chapter 19 .
It is commonly accepted that the " Redeemer " of 19:25 is the same person as the witness of 16:19 .
There is also a tomb of Job outside the city of Salalah in Oman .
