It was the naval engineers that also constructed the first World War I " tanks " giving rise to armoured fighting vehicles protected by vehicle armour .
Aerial armour has been used , notably , in protecting the pilots during the Second World War , and in designing heavily armoured aircraft that would be expected to suffer more than usual damage from ground fire .
Soldiers in the American Civil War bought iron and steel vests from peddlers ( both sides had considered but rejected body armour for standard issue ) .
The most intensive use of armoured trains was during the Russian Civil War ( 1918 -- 1920 ) .
Churchill and many of the train 's garrison were captured , though many others escaped , including wounded placed on the train 's engine .
Towards the end of World War I , armies on both sides were experimenting with plate armour as protection against shrapnel and ricocheting projectiles .
Mechanical problems , poor mobility and piecemeal tactical deployment limited the military significance of the tank in World War I and the tank did not fulfil its promise of rendering trench warfare obsolete .
The development of effective anti-aircraft artillery before the Second World War meant that the pilots , once the " knights of the air " during the First World War were left far more vulnerable to ground fire .
This not only created the requirement for the introduction of the cockpit armour plating that eventually came to be known variously as the " bucket " or the " bathtub " , but also the design of such aircraft as the Il-2 which also were heavily armoured to protect the fuel and engine , allowing much greater survivability during ground assaults .
[ citation needed ] Dragon Skin body armour is another ballistic vest which is currently in testing with mixed results .
Tank armour has progressed from the Second World War armour forms , now incorporating not only harder composites , but also reactive armour designed to defeat shaped charges .
As a result of this , the main battle tanks designed since Cold War era can survive multiple RPG strikes with minimal effect on the crew or the operation of the vehicle .
The armoured personnel carrier ( APC ) is a relatively recent development , stemming from trials and experiences during the Second World War .
Naval armour has fundamentally changed from the Second World War doctrine of thicker plating to defend against shells , bombs and torpedos .
