Alexander was named for Pope Alexander II .
He was the younger brother of King Edgar , who was unmarried , and his brother 's heir presumptive by 1104 ( and perhabs earlier ) .
In that year he was the senior layman present at the examination of the remains of Saint Cuthbert at Durham prior to their reinterrment .
Alexander had at least one illegitimate child , Máel Coluim mac Alaxandair , who was later to be involved in a revolt against David I in the 1130s .
He was imprisoned at Roxburgh for many years afterwards , perhaps until his death some time after 1157 .
He was responsible for foundations at Scone and Inchcolm .
His mother 's chaplain and hagiographer Thurgot was named Bishop of Saint Andrews in 1107 , presumably by Alexander 's order .
The case of Thurgot 's would-be successor Eadmer shows that Alexander 's wishes were not always accepted by the religious community , perhaps because Eadmer had the backing of the Archbishop of Canterbury , Ralph d'Escures , rather than Thurstan of York .
For all his religiosity , Alexander was not remembered as a man of peace .
He manifested the terrible aspect of his character in his reprisals in the Mormaerdom of Moray .
The king referred to is Alexander 's father , Malcolm III , and Domnall was Alexander 's half brother .
