Alberta i i i / b ɜr t ə / is the most populous of Canada 's three prairie provinces .
It covers about the same land area as France or Texas , and had a population of 3.7 million in 2009 .
It became a province on September 1 , 1905 , on the same day as Saskatchewan .
Edmonton , the capital city of Alberta , is located just south of the centre of the province .
Other municipalities in the province include Red Deer , Lethbridge , Medicine Hat , Fort McMurray , Grande Prairie , Camrose , Lloydminster , Brooks , Wetaskiwin , Banff , Cold Lake , and Jasper .
Alberta is named after Princess Louise Caroline Alberta ( 1848 -- 1939 ) , the fourth daughter of Queen Victoria and her husband , Prince Albert .
Lake Louise , the village of Caroline , and Mount Alberta were also named in honour of Princess Louise .
Since December 14 , 2006 , the Premier of the province has been Ed Stelmach , a Progressive Conservative .
This makes it the fourth largest province after Quebec , Ontario , and British Columbia .
Its highest point is 3747 metres ( 12293 ft ) at the summit of Mount Columbia in the Rocky Mountains along the southwest border , while its lowest point is 152 metres ( 499 ft ) on the Slave River in Wood Buffalo National Park in the northeast .
Alberta contains numerous rivers and lakes used for swimming , water skiing , fishing and a full range of other water sports .
Part of Lake Athabasca ( 7898 square kilometres ( 3049 sq mi ) ) lies in the province of Saskatchewan .
Lake Claire ( 1436 square kilometres ( 554 sq mi ) ) lies just west of Lake Athabasca in Wood Buffalo National Park .
Lesser Slave Lake ( 1168 square kilometres ( 451 sq mi ) ) is northwest of Edmonton .
The longest river in Alberta is the Athabasca River which travels 1538 kilometres ( 956 mi ) from the Columbia Icefield in the Rocky Mountains to Lake Athabasca .
Edmonton is the most northerly major city in Canada , and serves as a gateway and hub for resource development in northern Canada .
Alberta 's other major city , Calgary , is located approximately 280 kilometres ( 170 mi ) south of Edmonton and 240 kilometres ( 150 mi ) north of Montana , surrounded by extensive ranching country .
Almost 75 % of the province 's population lives in the Calgary-Edmonton Corridor , in and between the two major cities .
Most of the northern half of the province is boreal forest , while the Rocky Mountains along the southwestern boundary are largely forested .
The central aspen parkland region extending in a broad arc between the prairies and the forests , from Calgary , north to Edmonton , and then east to Lloydminster , contains the most fertile soil in the province and most of the population .
Much of the unforested part of Alberta is given over either to grain or to dairy farming , with mixed farming more common in the north and centre , while ranching and irrigated agriculture predominate in the south .
The Alberta badlands are located in southeastern Alberta , where the Red Deer River crosses the flat prairie and farmland , and features deep canyons and striking landforms .
Dinosaur Provincial Park , near Brooks , Alberta , showcases the badlands terrain , desert flora , and remnants from Alberta 's past when dinosaurs roamed the then lush landscape .
Alberta has a dry continental climate with warm summers and cold winters .
As the fronts between the air masses shift north and south across Alberta , temperature can change rapidly .
Because Alberta extends for over 1200 kilometres ( 750 mi ) from north to south , its climate varies considerably .
The climate is also influenced by the presence of the Rocky Mountains to the southwest , which disrupt the flow of the prevailing westerly winds and cause them to drop most of their moisture on the western slopes of the mountain ranges before reaching the province , casting a rain shadow over much of Alberta .
The northerly location and isolation from the weather systems of the Pacific Ocean cause Alberta to have a dry climate with little moderation from the ocean .
Annual precipitation ranges from 300 millimetres ( 12 in ) in the southeast to 450 millimetres ( 18 in ) in the north , except in the foothills of the Rocky Mountains where rainfall can reach 600 millimetres ( 24 in ) annually .
Alberta is a sunny province .
The long summer days make summer the sunniest season of the year in Alberta .
In southwestern Alberta , the winter cold is frequently interrupted by warm , dry chinook winds blowing from the mountains , which can propel temperatures upward from frigid conditions to well above the freezing point in a very short period .
The agricultural area of southern Alberta has a semi-arid steppe climate because the annual precipitation is less than the water that evaporates or is used by plants .
The southeastern corner of Alberta , part of the Palliser Triangle , experiences greater summer heat and lower rainfall than the rest of the province , and as a result suffers frequent crop yield problems and occasional severe droughts .
Western Alberta is protected by the mountains and enjoys the mild temperatures brought by winter chinook winds .
After southern Ontario , Central Alberta is the most likely region in Canada to experience tornadoes .
Thunderstorms , some of them severe , are frequent in the summer , especially in central and southern Alberta .
The region surrounding the Calgary-Edmonton Corridor is notable for having the highest frequency of hail in Canada , which is caused by orographic lifting from the nearby Rocky Mountains , enhancing the updraft/downdraft cycle necessary for the formation of hail .
The province of Alberta , as far north as about 53° north latitude , was a part of Rupert 's Land from the time of the incorporation of the Hudson 's Bay Company ( 1670 ) .
The North West Company of Montreal occupied the northern part of Alberta territory before the Hudson 's Bay Company arrived from Hudson Bay to take possession of it .
His cousin , Sir Alexander Mackenzie , followed the North Saskatchewan River to its northernmost point near Edmonton , then setting northward on foot , trekked to the Athabasca River , which he followed to Lake Athabasca .
It was there he discovered the mighty outflow river which bears his name -- the Mackenzie River -- which he followed to its outlet in the Arctic Ocean .
Returning to Lake Athabasca , he followed the Peace River upstream , eventually reaching the Pacific Ocean , and so he became the first white man to cross the North American continent north of Mexico .
Most of Alberta 's territory was included in Rupert 's Land , transferred to Canada in 1870 .
The district of Alberta was created as part of the North-West Territories in 1882 .
After a long campaign for autonomy , in 1905 the district of Alberta was enlarged and given provincial status , with the election of Alexander Cameron Rutherford as the first premier .
Alberta has enjoyed a relatively high rate of growth in recent years , mainly because of its burgeoning economy .
Between 2003 and 2004 , the province had high birthrates ( on par with some larger provinces such as British Columbia ) , relatively high immigration , and a high rate of interprovincial migration when compared to other provinces .
Many of Alberta 's cities and towns have also experienced very high rates of growth in recent history .
Alberta has considerable ethnic diversity .
As of the Canada 2001 Census the largest religious group was Roman Catholic , representing 25.7 % of the population .
Alberta had the second highest percentage of non-religious residents in Canada ( after British Columbia ) at 23.1 % of the population .
Of the remainder , 13.5 % of the population identified themselves as belonging to the United Church of Canada , while 5.9 % were Anglican .
The Mormons of Alberta reside primarily in the extreme south of the province and made up 1.7 % of the population .
Canada 's oldest mosque the Al-Rashid Mosque is located in Edmonton .
Jews constituted 0.4 % of Alberta 's population .
Most of Alberta 's 13,000 Jews live in Calgary ( 7,500 ) and Edmonton ( 5,000 ) .
Alberta is the third most diverse province in terms of visible minorities after British Columbia and Ontario with 13.9 % of the population consisting of visible minorities .
Nearly one-fourth of the populations of Calgary and Edmonton belong to a visible minority group .
Alberta 's economy is one of the strongest in Canada , supported by the burgeoning petroleum industry and to a lesser extent , agriculture and technology .
According to the 2006 census , the median annual family income after taxes was $ 70,986 in Alberta ( compared to $ 60,270 in Canada as a whole ) .
The Calgary-Edmonton Corridor is the most urbanized region in the province and one of the densest in Canada .
In 2001 , the population of the Calgary-Edmonton Corridor was 2.15 million ( 72 % of Alberta 's population ) .
According to the Fraser Institute , Alberta also has very high levels of economic freedom .
Alberta is the largest producer of conventional crude oil , synthetic crude , natural gas and gas products in the country .
Alberta is the world 's 2nd largest exporter of natural gas and the 4th largest producer .
Two of the largest producers of petrochemicals in North America are located in central and north central Alberta .
In both Red Deer and Edmonton , world class polyethylene and vinyl manufacturers produce products shipped all over the world , and Edmonton 's oil refineries provide the raw materials for a large petrochemical industry to the east of Edmonton .
The Athabasca Oil Sands have estimated unconventional oil reserves approximately equal to the conventional oil reserves of the rest of the world , estimated to be 1.6 trillion barrels ( 254 km³ ) .
With the development of new extraction methods such as steam assisted gravity drainage , which was developed in Alberta , bitumen and synthetic crude oil can be produced at costs close to those of conventional crude .
As of late 2006 there were over $ 100 billion in oil sands projects under construction or in the planning stages in northeastern Alberta .
With concerted effort and support from the provincial government , several high-tech industries have found their birth in Alberta , notably patents related to interactive liquid crystal display systems .
With a growing economy , Alberta has several financial institutions dealing with civil and private funds .
The province has over three million head of cattle , and Alberta beef has a healthy worldwide market .
Alberta is one of the prime producers of plains buffalo ( bison ) for the consumer market .
Wheat and canola are primary farm crops , with Alberta leading the provinces in spring wheat production ; other grains are also prominent .
Alberta has been a tourist destination from the early days of the twentieth century , with attractions including outdoor locales for skiing , hiking and camping , shopping locales such as West Edmonton Mall , Calgary Stampede , outdoor festivals , professional athletic events , international sporting competitions such as the Commonwealth Games and Olympic Games , as well as more eclectic attractions .
There are also natural attractions like Elk Island National Park , Wood Buffalo National Park , and the Columbia Icefield .
Banff , Jasper and the Rocky Mountains are visited by about three million people per year .
Alberta tourism relies heavily on Southern Ontario tourists , as well as tourists from other parts of Canada , the United States , and many international countries .
Alberta 's Rocky Mountains include well known tourist destinations Banff National Park and Jasper National Park .
The two mountain parks are connected by the scenic Icefields Parkway .
Banff is located 128 km ( 80 mi ) west of Calgary on Highway 1 , and Jasper is located 366 km ( 227 mi ) west of Edmonton on Yellowhead Highway .
About 700,000 people enjoy Edmonton 's Capital Ex ( formerly Klondike Days ) .
Located in east-central Alberta is Alberta Prairie Railway Excursions , a popular tourist attraction operated out of Stettler .
Alberta Prairie Railway Excursions caters to tens of thousands of visitors every year .
Alberta is an important destination for tourists who love to ski and hike ; Alberta boasts several world-class ski resorts such as Sunshine Village , Lake Louise , Marmot Basin , Norquay and Nakiska .
The Alberta tax system maintains a progressive flavour by allowing residents to earn $ 16,161 before becoming subject to provincial taxation in addition to a variety of tax deductions for persons with disabilities , students , and the aged .
Alberta 's municipalities and school jurisdictions have their own governments which ( usually ) work in co-operation with the provincial government .
Alberta has over 180000 km ( 111847 mi ) of highways and roads , of which nearly 50000 km ( 31069 mi ) are paved .
Highway 4 , which effectively extends Interstate 15 into Alberta and is the busiest U.S. gateway to the province , begins at the Coutts border crossing and ends at Lethbridge .
Highway 3 joins Lethbridge to Fort Macleod and links Highway 4 to Highway 2 .
Highway 2 travels northward through Fort Macleod , Calgary , Red Deer , and Edmonton .
The section of Highway 2 between Calgary and Edmonton has been named the Queen Elizabeth II Highway to commemorate the visit of the monarch in 2005 .
Highway 43 travels northwest into Grande Prairie and the Peace River Country ; Highway 63 travels northeast to Fort McMurray , the location of the Athabasca Oil Sands .
Alberta has two main east-west corridors .
The southern corridor , part of the Trans-Canada Highway system , enters the province near Medicine Hat , runs westward through Calgary , and leaves Alberta through Banff National Park .
One of the most scenic drives is along the Icefields Parkway , which runs for 228 km ( 142 mi ) between Jasper and Lake Louise , with mountain ranges and glaciers on either side of its entire length .
The highway connects many of the smaller towns in central Alberta with Calgary and Edmonton , as it crosses Highway 2 just west of Red Deer .
Urban stretches of Alberta 's major highways and freeways are often called trails .
Calgary , Edmonton , Red Deer , Medicine Hat , and Lethbridge have substantial public transit systems .
In addition to buses , Calgary and Edmonton operate light rail transit ( LRT ) systems .
Edmonton LRT , which is underground in the downtown core and on the surface outside of it , was the first of the modern generation of light rail systems to be built in North America , while the Calgary C-Train has one of the highest number of daily riders of any LRT system in North America .
Alberta is well-connected by air , with international airports in both Calgary and Edmonton .
Calgary 's airport is a hub for WestJet Airlines and a regional hub for Air Canada .
There are over 9000 km ( 5592 mi ) of operating mainline railway , and many tourists see Alberta aboard Via Rail or Rocky Mountaineer .
The Canadian Pacific Railway and Canadian National Railway companies operate railway freight across the province .
Municipalities where the same body act as both local government and school board are formally referred to as " counties " in Alberta .
Her duties in Alberta are carried out by Lieutenant Governor Donald Ethell .
Although the lieutenant governor is technically the most powerful person in Alberta , he is in reality a figurehead whose actions are restricted by custom and constitutional convention .
The current premier is Ed Stelmach who was elected as leader of the governing Progressive Conservatives on December 2 , 2006 .
The City of Edmonton is the seat of the provincial government -- the capital of Alberta .
Alberta has traditionally had three political parties , the Progressive Conservatives , the Liberals , and the social democratic New Democrats .
A fourth party , the strongly conservative Social Credit Party , was a power in Alberta for many decades , but fell from the political map after the Progressive Conservatives came to power in 1971 .
Since that time , no other political party has governed Alberta .
Alberta has had occasional surges in separatist sentiment .
There are several groups wishing to promote the independence of Alberta in some form currently active in the province .
Alberta became Canada 's second province ( after Saskatchewan ) to adopt a Tommy Douglas-style program in 1950 , a precursor to the modern medicare system .
Alberta 's health care budget is currently $ 13.2 billion during the 2008-2009 fiscal year ( approximately 36 % of all government spending ) , making it the best funded health care system per-capita in Canada .
A highly educated population and burgeoning economy have made Alberta a national leader in health education , research , and resources .
Currently under construction in Edmonton is the new $ 909 million Edmonton Clinic , which will provide a similar research , education , and care environment as the Mayo Clinic in the United States .
The Shock Trauma Air Rescue Society , a nonprofit organization , provides an air ambulance service to all but the most remote areas of Alberta , and some adjoining areas of British Columbia .
There are forty-two public school jurisdictions in Alberta , and seventeen operating separate school jurisdictions .
Prior to 1994 public and separate school boards in Alberta had the legislative authority to levy a local tax on property , as supplementary support for local education .
Alberta 's oldest and largest university is Edmonton 's University of Alberta established in 1908 .
The University of Calgary , once affiliated with the University of Alberta , gained its autonomy in 1966 and is now the second largest university in Alberta .
There is also Athabasca University , which focuses on distance learning , and the University of Lethbridge , both of which are located in their title cities .
In early September , 2009 , Mount Royal University became Calgary 's second public university , and in late September , 2009 , a similar move made Grant MacEwan University Edmonton 's second public university .
There are 15 colleges that receive direct public funding , along with two technical institutes , Northern Alberta Institute of Technology and Southern Alberta Institute of Technology .
In 2005 , Premier Ralph Klein made a promise that he would freeze tuition and look into ways of reducing schooling costs .
Summer brings many festivals to the province of Alberta , especially in Edmonton .
The Edmonton Fringe Festival is the world 's second largest after Edinburgh 's .
The Stampede is Canada 's biggest rodeo festival and features various races and competitions , such as calf roping and bull riding .
In line with the western tradition of rodeo are the cultural artisans that reside and create unique Alberta western heritage crafts .
The Banff Centre also hosts a range of festivals and other events including the internationally known Mountain Film Festival .
These cultural events in Alberta highlight the province 's cultural diversity and love of entertainment .
Both Calgary and Edmonton are home to Canadian Football League and National Hockey League teams .
Soccer , rugby union and lacrosse are also played professionally in Alberta .
In central and northern Alberta the arrival of spring brings the prairie crocus anemone , the three flowered avens , golden bean , wild rose and other early flowers .
The southern and east central parts of Alberta are covered by a short , nutritious grass , which dries up as summer lengthens , to be replaced by hardy perennials such as the prairie coneflower , fleabane , and sage .
On the north side of the North Saskatchewan River evergreen forests prevail for hundreds of thousands of square kilometres .
The four climatic regions ( alpine , boreal forest , parkland , and prairie ) of Alberta are home to many different species of animals .
The buffalo population was decimated during early settlement , but since then buffalo have made a strong comeback and thrive on farms and in parks all over Alberta .
Alberta is home to many large carnivores .
Moose , mule deer , and white-tail deer are found in the wooded regions , and pronghorn can be found in the prairies of southern Alberta .
Bighorn sheep and mountain goats live in the Rocky Mountains .
Alberta is home to only one variety of venomous snake , the prairie rattlesnake .
Vast numbers of ducks , geese , swans and pelicans arrive in Alberta every spring and nest on or near one of the hundreds of small lakes that dot northern Alberta .
Eagles , hawks , owls and crows are plentiful , and a huge variety of smaller seed and insect-eating birds can be found .
Alberta , like other temperate regions , is home to mosquitoes , flies , wasps , and bees .
Frogs and salamanders are a few of the amphibians that make their homes in Alberta .
In 2009 , several rats were found and captured , in small pockets in Southern Alberta , putting Alberta 's rat-free status in jeopardy .
