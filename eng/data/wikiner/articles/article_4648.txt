Flamenco is a style of music and dance which is considered part of the culture of Spain , although it is actually native to only one region : Andalusia .
Other regions , mainly Extremadura and Murcia , have also contributed to the development of flamenco , and many flamenco artists have been born outside the gitano community .
George Borrow claims the word flemenc [ sic ] is synonymous with " gypsy " .
( Philip , that is . )
However , the zarabandas and jácaras are the oldest written musical forms in Spain to use the 12-beat metre , a combination of terciary and binary rhythms .
Early flamencologists were amateurs and relied on a limited number of historical sources , mainly the work of 19th century folklorist Demófilo and notes made by foreign travellers .
Traditional flamenco commentators such as Demófilo see this period as the start of the commercial debasement of flamenco .
A flamenco show became an essential part of any trip to Spain , even outside Andalusia .
The leading artist at the time was Pepe Marchena , who sang in a sweet falsetto voice , using spectacular vocal runs reminiscent of bel canto coloratura .
Other critics disagree : great figures of traditional cante like La Niña de los Peines or Manolo Caracol enjoyed great success , and palos like siguiriyas or soleá were never completely abandoned , not even by the most typical singers of the ópera flamenca style like Marchena or Valderrama .
There are also " tablaos " , establishments that developed during the 1960s throughout Spain replacing the " café cantante " .
Camarón de la Isla was one artist who popularized this style .
Modern guitarists such as Ramón Montoya , have also introduced other positions .
Garcia Lorca is perhaps the best known of these poets .
In the 1920s he , along with the composer Manuel de Falla and other intellectuals , crusaded to raise the status of flamenco as an art form and preserve its purity .
Cante flamenco can be categorized in a number of ways .
