In 2005 hosted the Community of Democracies ministerial conference .
An associate member of Mercosur and a full member of APEC .
This was primarily due to a border dispute : both nations claimed the totality of the Patagonia region .
He established the current border in the Patagonia region .
The Beagle conflict began to brew in the 1960s , when Argentina began to claim that the Picton , Lennox and Nueva islands in the Beagle Channel were rightfully hers .
On 25 January 1978 the Argentina military junta led by General Jorge Videla declared the award fundamentally null and intensified their claim over the islands .
December 1978 , Argentina started the Operation Soberania over the disputed islands , but the invasion was halted due to : and in cite 46 :
In the 1990s , under presidents Frei and Menem both countries solved almost all of the remaining border disputes during bilateral talks .
The entire disputed area was awarded to Argentina .
In August 2006 , however , a tourist map was published in Argentina placing the disputed region within the borders of that country .
Peru had , in 1873 , signed a secret pact with Bolivia in which the two countries agreed to fight together against any nation that threatened either of them .
Diplomatic relations with Bolivia continued to be strained because of Bolivia 's continuing aspiration to the sea .
They are also two of the three most important economies in South America along with Argentina .
Nevertheless , in 1873 Peru signed a secret defensive pact with Bolivia in which it agreed to help that nation in case of foreign attack .
The moment came in 1879 , when the War of the Pacific began .
Peru also had to hand over the departments of Arica and Tacna .
In 1975 , both countries were at the brink of war , only a few years before the centennial of the War of the Pacific .
However , he was successfully dissuaded from starting the invasion on that date by his advisor , General Francisco Morales Bermúdez , whose original family was from the former Peruvian region of Tarapacá .
However , former Chilean president Michelle Bachelet and Peru 's Alan García established a positive diplomatic relationship , and it is very unlikely any hostilities will break out because of the dispute .
Peruvian police stopped a group of nearly 2,000 people just ten kilometers from the border , preventing them from reaching their intended destination .
Both countries raised the agenda of rekindling ties in 2005 as a precursor to the attempted Free Trade Area of the Americas trading bloc .
Israel has an embassy in Santiago .
