At around the same time a league from Glassboro State College also formed a similar baseball league and had its first draft in 1976 .
George Blanda was the first player taken in the first draft in 1963 .
Because Okrent was a member of the media , other journalists , especially sports journalists , were introduced to the game .
Fantasy fans often used James ' statistical tools and analysis as a way to improve their teams .
In the few years after Okrent helped popularize fantasy baseball , a host of experts and business emerged to service the growing hobby .
Hunt started the first high-profile experts league , the League of Alternate Baseball Reality which first included notables as Peter Gammons , Keith Olbermann and Bill James .
RotoNews.com also launched in January 1997 and published its first player note on February 16 , 1997 .
A trade group for the industry , the Fantasy Sports Trade Association was formed in 1998 .
The company would re-emerge as RotoWire.com .
Sportsline moved back to a pay model for commissioner services ( which it largely still has today ) .
RotoWire.com moved from a free model to a pay model in 2001 as well. .
The NFL began running promotional television ads for fantasy football featuring current players for the first time .
In 1996 , STATS , Inc. , a major statistical provider to fantasy sports companies , won a court case , along with Motorola , on appeal against the NBA in which the NBA was trying to stop STATS from distributing in game score information via a special wireless device created by Motorola .
If MLB prevailed , it just would have been a matter of time before they followed up .
The Unlawful Internet Gambling Enforcement Act of 2006 , which was an amendment to the larger and unrelated Safe Port Act , included " carve out " language that clarified the legality of fantasy sports .
It was signed into law on October 13 , 2006 by President George W. Bush .
The Fantasy Sports Trade Association was formed in 1999 to represent the growing industry .
The Fantasy Sports Writers Association was formed in 2004 to represent the growing numbers of journalists covering fantasy sports exclusively .
The Fantasy Sports Association was formed in 2006 .
