The city is part of the Detroit -- Ann Arbor -- Flint , MI CSA .
Ann Arbor was founded in 1824 , with one theory stating that it is named after the spouses of the city 's founders and for the stands of trees in the area .
Today , Ann Arbor is home to the University of Michigan , which is the dominant institution of higher learning in the city .
Ann Arbor has increasingly found itself grappling with the effects of sharply rising land values and gentrification , as well as urban sprawl stretching far into the outlying countryside .
Ann Arbor was founded in 1824 by land speculators John Allen and Elisha Rumsey .
Regional Michigan Ojibwa named the settlement kaw-goosh-kaw-nick , after the sound of Allen 's sawmill .
Ann Arbor became the seat of Washtenaw County in 1827 , and was incorporated as a village in 1833 .
In 1837 , the property was accepted instead as the site of the University of Michigan , which moved from Detroit .
Since the university 's establishment in the city in 1837 , the history of both the University of Michigan and Ann Arbor are closely linked .
The town became a regional transportation hub in 1839 with the arrival of the Michigan Central Railroad , and a north -- south railway connecting Ann Arbor to Toledo and other markets to the south was established in 1878 .
Throughout 1840s and the 1850s settlers continued to come to Ann Arbor .
Ann Arbor saw increased growth in manufacturing , particularly in milling .
The first major meetings of the national left-wing campus group Students for a Democratic Society took place in Ann Arbor in 1960 ; in 1965 , the city was home to the first U.S. teach-in against the Vietnam War .
These influences washed into municipal politics during the early and mid-1970s when three members of the Human Rights Party won city council seats on the strength of the student vote .
Alongside these liberal and left-wing efforts , a small group of conservative institutions were born in Ann Arbor .
These include Word of God ( established in 1967 ) , a charismatic inter-denominational movement ; and the Thomas More Law Center ( established in 1999 ) , a religious-conservative advocacy group .
Following a 1956 vote the city of East Ann Arbor merged with Ann Arbor to encompass the eastern sections of the city .
In the past several decades , Ann Arbor has grappled with the effects of sharply rising land values , gentrification , and urban sprawl stretching into outlying countryside .
On November 4 , 2003 , voters approved a greenbelt plan under which the city government bought development rights to pieces of land adjacent to Ann Arbor to preserve them from sprawling development .
Ann Arbor consistently ranks in the " top places to live " lists published by various mainstream media outlets every year .
In 2008 , it was ranked 27th out of 100 " America 's best small cities . "
According to the United States Census Bureau , the city has an area of 27.7 square miles ( 72 km 2 ) ; 27.0 square miles ( 70 km 2 ) is land and 0.7 square miles ( 1.8 km 2 ) or 2.43 % is water , much of which is part of the Huron River .
Ann Arbor is about 35 miles ( 56 km ) west of Detroit .
Ann Arbor Charter Township adjoins the city 's north and east sides .
Ann Arbor is situated on the Huron River in a productive agricultural and fruit-growing region .
The landscape of Ann Arbor consists of hills and valleys , with the terrain becoming steeper near the Huron River .
The elevation ranges from about 750 feet ( 230 m ) along the Huron River to over 1000 feet ( 300 m ) on the city 's west side , near I-94 .
Several large city parks and a university park border sections of the Huron River .
Nichols Arboretum , operated by the University of Michigan , is a 123-acre ( 50 ha ) preserve that contains hundreds of plant and tree species .
The city 's commercial districts are composed mostly of two-to four-story structures , although downtown and the area near Briarwood Mall contain a small number of high-rise buildings .
Ann Arbor 's residential neighborhoods contain architectural styles ranging from classic 19th-century and early-20th-century designs to ranch-style houses .
Surrounding the University of Michigan campus are houses and apartment complexes occupied primarily by student renters .
Tower Plaza , a 26-story condominium building located between the University of Michigan campus and downtown , is the tallest building in Ann Arbor .
There were 49,982 housing units at an average density of 1,748.0 per square mile ( 675.0/km² ) , making it less densely populated than inner-ring Detroit suburbs like Oak Park and Ferndale ( and than Detroit proper ) , but more densely populated than outer-ring suburbs like Livonia .
Ann Arbor 's crime rate was below the national average in 2000 .
The University of Michigan shapes Ann Arbor 's economy significantly .
Automobile manufacturers , such as General Motors and Visteon , also employ residents .
Pfizer , once the city 's second largest employer , operated a large pharmaceutical research facility on the northeast side of Ann Arbor .
On January 22 , 2007 , Pfizer announced it would close operations in Ann Arbor by the end of 2008 .
The city is the home of other research and engineering centers , including those of General Dynamics and the National Oceanic and Atmospheric Administration ( NOAA ) .
The city is also home to NSF International , the nonprofit non-governmental organization that develops what are considered the generally accepted standards for a variety of public health related industries and subject areas .
The Borders chain is still based in the city , as is its flagship store .
Another Ann Arbor-based company is Zingerman 's Delicatessen , which serves sandwiches , and has developed businesses under a variety of brand names .
The North American Students of Cooperation ( NASCO ) is an international association of cooperatives headquartered in Ann Arbor .
Many Ann Arbor cultural attractions and events are sponsored by the University of Michigan .
The Ann Arbor Hands-On Museum , located in a renovated and expanded historic downtown fire station , contains more than 250 interactive exhibits featuring science and technology .
Multiple art galleries exist in the city , notably in the downtown area and around the University of Michigan campus .
The Ann Arbor District Library maintains four branch outlets in addition to its main downtown building .
The city is also home to the Gerald R. Ford Presidential Library .
Several annual events -- many of them centered on performing and visual arts -- draw visitors to Ann Arbor .
One such event is the Ann Arbor Art Fairs , a set of four concurrent juried fairs held on downtown streets , which began in 1960 .
One event that is not related to visual and performing arts is Hash Bash , held on the first Saturday of April , ostensibly in support of the reform of marijuana laws .
Ann Arbor has a major scene for college sports , notably at the University of Michigan , a member of the Big Ten Conference .
The stadium is colloquially known as " The Big House . "
Crisler Arena and Yost Ice Arena play host to the school 's basketball and ice hockey teams , respectively .
Concordia University , a member of the NAIA , also fields sports teams .
In A Prairie Home Companion broadcast from Ann Arbor , Garrison Keillor described Ann Arbor as " a city where people discuss socialism , but only in the fanciest restaurants . "
Ann Arbor sometimes appears on citation indexes as an author , instead of a location , often with the academic degree MI , a misunderstanding of the abbreviation for Mich. .
The Ann Arbor News , owned by the Michigan-based Booth Newspapers chain , was the major daily newspaper serving Ann Arbor and the rest of Washtenaw County .
It has been replaced by AnnArbor.com , which has a bi-weekly print operation in addition to its website .
The University of Michigan campus area is served by many student publications , including the independent Michigan Daily .
Car and Driver magazine and Automobile Magazine are also based in Ann Arbor .
The city 's FM stations include NPR affiliate WUOM 91.7 ; country station WWWW 102.9 and adult-alternative station WQKL 107.1 .
WPXD channel 31 , an affiliate of the ION Television network , is licensed to the city .
Ann Arbor has a council-manager form of government .
The mayor of Ann Arbor is John Hieftje ( Democrat ) , who has served in that capacity since the 2000 election .
In 1974 , Kathy Kozachenko 's victory in an Ann Arbor city-council race made her the country 's first openly homosexual candidate to win public office .
In 1975 , Ann Arbor became the first U.S. city to use instant-runoff voting for a mayoral race .
As of August 2009 , Democrats hold the mayorship and all council seats .
The University of Michigan is the dominant institution of higher learning in Ann Arbor , providing the city with a distinct college-town atmosphere .
Washtenaw Community College is located in neighboring Ann Arbor Township .
Thomas M. Cooley Law School has acquired the law school buildings for a branch campus .
The Ann Arbor Public School District handles local public education .
The system -- which enrolls 16,539 students ( September 2008 head count ) -- consists of twenty-one elementary schools , five middle schools , three traditional high schools ( Pioneer , Huron , and Skyline ) , and three alternative high schools .
Ann Arbor Public Schools also operates a preschool and family center , with programs for at-risk infants and at-risk children before kindergarten .
Ann Arbor is home to more than 20 private schools , including the Rudolf Steiner School of Ann Arbor , Clonlara School and Greenhills School , a prep school near Concordia University .
The city provides sewage disposal and water supply services , with water coming from the Huron River and groundwater sources .
The city 's water department also operates four dams along the Huron River , two of which provide hydroelectric power .
Electrical power and gas are provided by DTE Energy .
AT & T , the successor to Michigan Bell , Ameritech , and SBC Communications , is the primary wired telephone service provider for the area .
The city is belted by three freeways : I-94 , which runs along the southern portion of the city ; US 23 , which primarily runs along the eastern edge of Ann Arbor ; and M-14 , which runs along the northern edge of the city .
Other nearby highways include US 12 , M-17 , and M-153 .
The streets in downtown Ann Arbor conform to a grid pattern , though this pattern is less common in the surrounding areas .
Several of the major surface arteries lead to the I-94/M-14 juncture in the west , US 23 in the east , and the city 's southern areas .
Ann Arbor Municipal Airport is a small , city run general aviation airport located south of I-94 .
Detroit Metropolitan Airport , the area 's large international airport , is about 25 miles ( 40 km ) east of the city , in Romulus .
Willow Run Airport east of the city near Ypsilanti serves freight , corporate , and general aviation clients .
The city was also served by the Michigan Central Railroad starting in 1837 .
Amtrak provides service to Ann Arbor , operating its Wolverine three times daily in each direction between Chicago and Pontiac , via Detroit .
Ann Arbor has seven sister cities :
