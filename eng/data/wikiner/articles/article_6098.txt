The International Court of Justice is the primary judicial organ of the United Nations .
It is based in the Peace Palace in The Hague , Netherlands .
Its main functions are to settle legal disputes submitted to it by states and to give advisory opinions on legal questions submitted to it by duly authorized international organs , agencies , and the UN General Assembly .
The ICJ should not be confused with the International Criminal Court , which potentially also has " global " jurisdiction .
The United States withdrew from compulsory jurisdiction in 1986 , and so accepts the court 's jurisdiction only on a case-to-case basis .
Presently there are twelve cases on the World Court 's docket .
Judges of the ICJ are not able to hold any other post , nor act as counsel .
Despite these provisions , the independence of ICJ judges has been questioned .
For example , before becoming a UN member state , Switzerland used this procedure in 1948 to become a party .
And Nauru became a party in 1988 .
The issue of jurisdiction is considered in the two types of ICJ cases : contentious issues and advisory opinions .
In contentious cases ( adversarial proceedings seeking to settle a dispute ) , the ICJ produces a binding ruling between states that agree to submit to the ruling of the court .
The key principle is that the ICJ has jurisdiction only on the basis of consent .
An advisory opinion derives its status and authority from the fact that it is the official pronouncement of the principal judicial organ of the United Nations .
A decision on the merits has not been given since the parties ( United Kingdom , United States and Libya ) settled the case out of court in 2003 .
Article 59 makes clear that the common law notion of precedent or stare decisis does not apply to the decisions of the ICJ .
In reality , the ICJ rarely departs from its own previous decisions and treats them as precedent in a way similar to superior courts in common law systems .
Additionally , international lawyers commonly operate as though ICJ judgments had precedential value .
So far the International Court of Justice has dealt with about 130 cases .
The ICJ is vested with the power to make its own rules .
Cases before the ICJ will follow a standard pattern .
As with United Nations criticisms as a whole , many of these criticisms refer more to the general authority assigned to the body by member states through its charter than to specific problems with the composition of judges or their rulings .
