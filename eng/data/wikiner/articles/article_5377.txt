The destruction of the Hindenburg airship was an infamous example of hydrogen combustion ; the cause is debated , but the visible flames were the result of combustible materials in the ship 's skin .
Two-thirds of the Hindenburg passengers survived the fire , and many deaths were instead the result of falls or burning diesel fuel .
It has also been observed in the upper atmosphere of the planet Jupiter .
In 1671 , Robert Boyle rediscovered and described the reaction between iron filings and dilute acids , which results in the production of hydrogen gas .
In 1766 , Henry Cavendish was the first to recognize hydrogen gas as a discrete substance , by identifying the gas from a metal-acid reaction as " flammable air " and further finding in 1781 that the gas produces water when burned .
In 1783 , Antoine Lavoisier gave the element the name hydrogen when he and Laplace reproduced Cavendish 's finding that water is produced when hydrogen is burned .
Deuterium was discovered in December 1931 by Harold Urey , and tritium was prepared in 1934 by Ernest Rutherford , Mark Oliphant , and Paul Harteck .
Heavy water , which consists of deuterium in the place of regular hydrogen , was discovered by Urey 's group in 1932 .
Edward Daniel Clarke invented the hydrogen gas blowpipe in 1819 .
The Döbereiner 's lamp and limelight were invented in 1823 .
The first hydrogen-filled balloon was invented by Jacques Charles in 1783 .
Hydrogen provided the lift for the first reliable form of air-travel following the 1852 invention of the first hydrogen-lifted airship by Henri Giffard .
