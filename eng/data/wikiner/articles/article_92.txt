The nature of art has been described by Richard Wollheim as " one of the most elusive of the traditional problems of human culture " .
Leo Tolstoy identified art as a use of indirect means to communicate from one person to another .
Benedetto Croce and R.G. Collingwood advanced the idealist view that art expresses emotions , and that the work of art therefore essentially exists in the mind of the creator .
The theory of art as form has its roots in the philosophy of Immanuel Kant , and was developed in the early twentieth century by Roger Fry and Clive Bell .
More recently , thinkers influenced by Martin Heidegger have interpreted art as the means by which a community develops for itself a medium for self-expression and interpretation .
Adorno said in 1970 , " It is now taken for granted that nothing which concerns art can be taken for granted any more : neither art itself , nor art in relationship to the whole , nor even the right of art to exist . "
So , for example , Tang Dynasty paintings are monochromatic and sparse , emphasizing idealized landscapes , but Ming Dynasty paintings are busy , colorful , and focus on telling stories via setting and composition .
Lichtenstein thus uses the dots as a style to question the " high " art of painting with the " low " art of comics -- to comment on class distinctions in culture .
A common view is that the epithet " art " , particular in its elevated sense , requires a certain level of creative expertise by the artist , whether this be a demonstration of technical ability or an originality in stylistic approach such as in the plays of Shakespeare , or a combination of these two .
Traditionally skill of execution was viewed as a quality inseparable from art and thus necessary for its success ; for Leonardo da Vinci , art , neither more nor less than his other endeavors , was a manifestation of skill .
Rembrandt 's work , now praised for its ephemeral virtues , was most admired by his contemporaries for its virtuosity .
At the turn of the 20th century , the adroit performances of John Singer Sargent were alternately admired and viewed with skepticism for their manual fluency , yet at nearly the same time the artist who would become the era 's most recognized and peripatetic iconoclast , Pablo Picasso , was completing a traditional academic training at which he excelled .
Emin slept ( and engaged in other activities ) in her bed before placing the result in a gallery as work of art .
Hirst came up with the conceptual design for the artwork but has left most of the eventual creation of many works to employed artisans .
Hirst 's celebrity is founded entirely on his ability to produce shocking concepts .
Yet at the same time , the horrific imagery demonstrates Goya 's keen artistic ability in composition and execution and produces fitting social and political outrage .
Édouard Manet 's Le Déjeuner sur l'Herbe ( 1863 ) , was considered scandalous not because of the nude woman , but because she is seated next to men fully dressed in the clothing of the time , rather than in robes of the antique world .
John Singer Sargent 's Madame Pierre Gautreau ( Madam X ) ( 1884 ) , caused a huge uproar over the reddish pink used to color the woman 's ear lobe , considered far too suggestive and supposedly ruining the high-society model 's reputation .
The aesthetic theorist John Ruskin , who championed what he saw as the naturalism of J. M. W. Turner , saw art 's role as the communication by artifice of an essential truth that could only be found in nature .
Though only originally intended as a way of understanding a specific set of artists , Greenberg 's definition of modern art is important to many of the ideas of art within the various art movements of the 20th century and early 21st century .
Pop artists like Andy Warhol became both noteworthy and influential through work including and possibly critiquing popular culture , as well as the art world .
For example , when the Daily Mail criticized Hirst 's and Emin 's work by arguing " For 1,000 years art has been one of our great civilising forces .
Today , pickled sheep and soiled beds threaten to make barbarians of us all " they are not advancing a definition or theory about art , but questioning the value of Hirst 's and Emin 's work .
In 1998 , Arthur Danto , suggested a thought experiment showing that " the status of an artifact as work of art results from the ideas a culture applies to it , rather than its inherent physical or perceptible qualities .
