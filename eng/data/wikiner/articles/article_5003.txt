Pierre Teilhard de Chardin , a paleontologist and geologist , believed that evolution unfolded from cell to organism to planet to solar system and ultimately the whole universe , as we humans see it from our limited perspective .
A variant of this hypothesis was developed by Lynn Margulis , a microbiologist , in 1979 .
Her model is more limited in scope than the one that Lovelock proposed .
These views dominate some such groups , e.g. the Bioneers .
