In North American beavers,50 % have pale brown fur , 25 % are reddish brown , 20 % are brown and 6 % are blackish .
Much of the early exploration of North America was driven by the quest for this animal 's fur .
The actions of beavers for hundreds of thousands of years in the Northern Hemisphere have kept these watery systems healthy and in good repair , although a human observing all the downed trees might think that the beavers were doing just the opposite .
For example , in the 1940s , beavers were brought to the island and province of Tierra Del Fuego in southern Argentina for commercial fur production .
As the habitat is not adapted to withstand the impact of beavers ( e.g. , unlike in the Northern Hemisphere , trees native to Tierra del Fuego do not coppice ) , beavers have had a detrimental impact .
Yupik medicine used dried beaver testicles like willow bark to relieve pain .
Beaver testicles were exported from Levant ( a region centered on Lebanon and Israel ) from the tenth to nineteenth century .
Claudius Aelianus comically described beavers chewing off their testicles to preserve themselves from hunters , which is not possible because the beaver 's testicles are inside its body .
Castoreum was described in the 1911 British Pharmaceutical Codex for use in dysmenorrhea and hysterical conditions ( i.e. pertaining to the womb ) , for raising blood pressure and increasing cardiac output .
The beaver is also the symbol of many units and organizations within the Canadian Forces , such as on the cap badges of the Royal 22 e Régiment and the Canadian Military Engineers .
Toronto Police Services , London Police Service , Canadian Pacific Railway Police Service and Canadian Pacific Railway crest bears the beaver on their crest or coat of arms .
In the 17th century , based on a question raised by the Bishop of Quebec , the Roman Catholic Church ruled that the beaver was a fish for purposes of dietary law .
This is similar to the Church 's classification of the capybara , another semi-aquatic rodent .
In the 1976/1977 season , 500,000 beaver pelts were harvested in North America .
