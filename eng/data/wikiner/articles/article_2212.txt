It was formed by people who left Mebyon Kernow on 28 May 1975. .
The party ceased to exist in 2005 , although it claimed to have reformed in April 2009 following a conference in Bodmin . .
A separate party with a similar name ( the Cornish National Party ) existed from 1969 .
The split was down to the same debate that was occurring in most of the political parties campaigning for autonomy from the United Kingdom at the time ( for example the Scottish National Party and Plaid Cymru ) , whether to be a centre-left party appealing to the electorate on a social democratic line , or whether to appeal emotionally on a centre-right cultural line .
The CNP essentially represented the party 's right wing , who were not willing to accept that economic arguments were more likely to win votes than cultural .
The CNP worked to preserve the identity of Cornwall and improve its economy , and encouraged links with Cornish people overseas and with other regions which have distinct identities .
It also gave support to Unified Cornish , the language of the Cornish revival in modern times , and commemorated Thomas Flamank , a leader of the Cornish Rebellion in 1497 , at an annual ceremony at Bodmin on 27th June each year .
While the CNP were not a racist organisation there was a perceived image problem relating to the similarly-styled British National Party ( BNP ) .
It was announced in April 2009 that the Cornish Nationalist Party ( now abbreviated CPN ) had re-grouped to contest future elections .
