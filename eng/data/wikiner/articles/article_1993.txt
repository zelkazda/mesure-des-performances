While the ancient Celts undoubtedly had their own musical styles , the actual sound of their music remains a complete mystery .
Ireland , Scotland , and Brittany have living traditions of language and music , and there has been a recent major revival of interest in Wales .
However , Cornwall and the Isle of Man have only small-scale revivalist movements that have yet to take hold .
A few of those include bagadoù ( Breton pipe bands ) , Fairport Convention , Pentangle , Steeleye Span and Horslips .
Traces of Clannad 's legacy can be heard in the music of many artists , including Enya , Altan , Capercaillie , The Corrs , Loreena McKennitt , Anúna , Riverdance and U2 .
One of his major works was to bring " Hen Wlad Fy Nhadau " ( the Welsh national anthem ) back in Brittany and create lyrics in Breton .
In 1978 Runrig recorded an album in Scottish Gaelic .
Since about 2005 , Oi Polloi ( from Scotland ) have recorded in Scottish Gaelic .
Mill a h-Uile Rud recorded in the language in 2004 .
The same phenomenon occurs in Brittany , where many singers record songs in Breton , traditional or modern ( hip hop , rap , etc. ) .
