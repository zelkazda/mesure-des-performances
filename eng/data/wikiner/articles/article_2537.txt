There is a substantial feral population of dromedaries estimated at up to 1,000,000 in central parts of Australia , descended from individuals introduced as transport animals in the 19th century and early 20th century .
The government of South Australia has decided to cull the animals using aerial marksmen , because the camels use too much of the limited resources needed by sheep farmers .
A descendant of one of these was seen by a backpacker in Los Padres National Forest in 1972 .
In some places , such as Australia , some of the camels have become feral and are considered to be dangerous to travelers on camels .
The United States Army had an active camel corps stationed in California in the 19th century , and the brick stables may still be seen at the Benicia Arsenal in Benicia , California , now converted to artists ' and artisans ' studio spaces .
During the American Civil War , camels were used at an experimental stage , but were not used any further , as they were unpopular with the men .
It is used as a medicinal product in India [ citation needed ] and as an aphrodisiac in Ethiopia .
It has been recorded by ancient Greek writers as an available dish in ancient Persia at banquets , usually roasted whole .
The ancient Roman emperor Heliogabalus enjoyed camel 's heel .
According to Jewish tradition , camel meat and milk are not kosher .
