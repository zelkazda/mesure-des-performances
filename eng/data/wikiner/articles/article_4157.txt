Emperor Sushun was the 32nd emperor of Japan , according to the traditional order of succession .
He was the twelfth son of Emperor Kimmei .
He came to the throne with the support of the Soga clan and Empress Suiko , his half sister and the widow of Emperor Bidatsu .
It is said that one day , he saw a wild boar and proclaimed , " I want to kill Soga Umako like this wild boar . "
Emperor Sushun 's reign lasted for five years before his death at the age of 72 .
The Imperial Household Agency designates this location as Yōmei 's mausoleum .
