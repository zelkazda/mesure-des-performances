It has been used as an anticonvulsant in the UK since 1965 , and has been approved in the U.S. since 1974 .
In the UK , testing is generally performed much less frequently for long-term carbamazepine patients -- typically once per year .
