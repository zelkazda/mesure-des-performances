The genre was pioneered in the mid 1980s by rappers such as Schooly D and Ice T , and was popularized in the later part of the 1980s by groups like N.W.A .
After the national attention that Ice-T and N.W.A created in the late 1980s and early 1990s , gangsta rap became the most commercially lucrative subgenre of hip hop .
The Last Poets member Jalal Mansur Nuriddin delivers rhyming vocals in the urban slang of his time , and together with the other Last Poets members , was quite influential on later hip hop groups , such as Public Enemy .
Many rappers , such as Ice T and Mac Dre , have credited pimp and writer Iceberg Slim with influencing their rhymes .
Rudy Ray Moore 's stand-up comedy and films based on his Dolemite hustler-pimp alter ego also had an impact on gangsta rap and are still a popular source for samples .
For example , the opening skit on Snoop Dogg 's Doggystyle is an homage to the famous bathtub scene in the 1972 film Super Fly , while the rapper Notorious B.I.G. took his alias " Biggie Smalls " from a character in the 1975 film Let 's Do It Again .
Ice-T continued to release gangsta albums for the remainder of the decade : Rhyme Pays in 1987 , Power in 1988 and The Iceberg/Freedom of Speech ... Just Watch What You Say in 1989 .
Ice-T 's lyrics also contained strong political commentary , and often played the line between glorifying the gangsta lifestyle and criticizing it as a no-win situation .
The latter is the most gangsta-themed song of the three ; in it , KRS-One boasts about shooting a crack dealer and his posse to death ( in self-defense ) .
The album Criminal Minded followed in 1987 , and was the first rap album to have fire arms on its cover .
Shortly after the release of this album , BDP 's DJ , Scott LaRock was shot and killed .
After this , BDP 's subsequent records were more focused with the inadequate rationale removed .
The first blockbuster gangsta rap album was N.W.A 's Straight Outta Compton , released in 1988 .
In the early 1990s , former N.W.A member Ice Cube would further influence gangsta rap with his hardcore , socio-political solo albums , which suggested the potential of gangsta rap as a political medium to give voice to inner-city youth .
The Beastie Boys were one of the first groups to identify themselves as " gangsters " , and one of the first popular rap groups to talk about violence and drug and alcohol use .
The Geto Boys are also known for being the first rap group to sample from the movie Scarface , which became the staple for lots of mafioso rappers to sample from .
Ice-T released one of the seminal albums of the genre , OG : Original Gangster in 1991 .
The group attracted a lot of media attention for the Cop Killer controversy .
His next album , Home Invasion , was postponed as a result of the controversy , and was finally released in 1993 .
After that , he left Warner Bros. Records .
Ice-T 's subsequent releases went back to straight gangsta-ism , but were never as popular as his earlier releases .
In 1992 , former N.W.A member Dr. Dre released The Chronic , a massive seller ( eventually going triple platinum ) which showed that explicit gangsta rap could hold mass commercial appeal just like more pop-oriented rappers such as MC Hammer , The Fresh Prince , and Tone Lōc .
One of the genre 's biggest crossover stars was Dre 's protégé Snoop Doggy Dogg ( Doggystyle , 1993 ) , now known as Snoop Dogg , whose exuberant , party-oriented themes made songs such as " Gin and Juice " club anthems and top hits nationwide .
Not long afterward , his shocking murder brought gangsta rap into the national headlines and propelled his posthumous The Don Killuminati : The 7 Day Theory album ( which eerily featured an image of 2Pac being crucified on the front cover ) to the top of the charts .
Mafioso rap is a hardcore hip hop sub-genre founded by Kool G Rap in the late 1980s .
Though the genre died down for several years , it re-emerged in 1995 when Wu-Tang Clan member Raekwon released his critically acclaimed solo album , Only Built 4 Cuban Linx ... . 1995 also saw the release of Doe or Die by Nas ' protégé AZ .
Similarly , in recent years , many rappers , such as T.I. , Jadakiss , Jim Jones , and Cassidy have maintained popularity with lyrics about self-centered urban criminal lifestyles or " hustling " .
In an interview for The Independent in 1994 , the Wu-Tang Clan 's GZA commented on the term " gangsta rap " and its association with his group 's music and hip hop at the time :
Dr. Dre , at the MTV Video Music Awards , claimed that " gangsta rap was dead " .
After the deaths of Tupac Shakur and Biggie Smalls and the media attention they generated , gangsta rap became an even greater commercial force .
Atlanta already featured established acts like Goodie Mob , OutKast and Ludacris , all of whom would achieve their greatest popularity in the late 1990s and early 2000s .
More recently , Atlanta has produced successfully rappers like Gucci Mane , T.I. and Young Jeezy .
Master P 's No Limit Records label , based out of New Orleans , also became quite popular in the late 1990s , though critical success was very scarce , with the exceptions of some later additions like Mystikal .
No Limit had begun its rise to national popularity with Master P 's The Ghetto Is Trying to Kill Me ! ( 1994 ) , and had major hits with Silkk the Shocker ( Charge It 2 Da Game , 1998 ) and C-Murder ( Life or Death , 1998 ) .
Cash Money Records , also based out of New Orleans , had enormous commercial success beginning in the late 1990s with a similar musical style and quantity-over-quality business approach as No Limit .
Memphis collective Hypnotize Minds , led by Three 6 Mafia and Project Pat , have taken gangsta rap to some of its darker extremes .
Led by in-house producers DJ Paul and Juicy J , the label became known for its pulsating , menacing beats and uncompromisingly thuggish lyrics .
However , in the mid-2000s , the group began attaining more mainstream popularity , eventually culminating in the Three 6 Mafia winning an Academy Award for the song " It 's Hard Out Here for a Pimp " from Hustle and Flow .
Midwest Hip Hop was originally distinctive for its faster-paced flow .
Houston first came on to the national scene in the late 1980s with the violent and disturbing stories told by the Geto Boys , with member Scarface achieving major solo success in the mid-90s .
The late DJ Screw , a South Houston DJ , is credited with the creation of and early experimentation with the genre .
[ citation needed ] DJ Screw began making mixtapes of the slowed-down music in the early 1990s and began the Screwed Up Click .
The earlier , somewhat controversial crossover success enjoyed by popular gangsta rap songs like " Gin and Juice " gave way to gangsta rap 's becoming a widely-accepted staple on the pop charts in the late 1990s .
For example , between the release of The Notorious B.I.G. 's debut album Ready to Die in 1994 and his follow-up , the posthumous Life After Death in 1997 , his sound changed from a darker , tense production , with lyrics projecting desperation and paranoia , to a cleaner , more laid-back sound , fashioned for popular consumption ( though the references to guns , drug dealing and life as a thug on the street remained ) .
Also achieving similar levels of success with a similar sound at the same time as Bad Boy was Master P and his No Limit label in New Orleans , as well as the New Orleans upstart Cash Money label .
Mase and Cam'ron were typical of a more relaxed , casual flow that became the pop-gangsta norm .
Some gangsta rappers such as 50 Cent and G-Unit attempt to appeal to both the pop rap and gangsta rap audiences .
