The Formula One Group is the legal holder of the commercial rights .
However Fangio won the title in 1951 , 1954 , 1955 , 1956 & 1957 , his streak interrupted ( after an injury ) by two-time champion Alberto Ascari of Ferrari .
The period was highlighted by teams run by road car manufacturers -- Alfa Romeo , Ferrari , Mercedes Benz and Maserati -- all of whom had competed before the war .
Mercedes drivers won the championship for two years , before the team withdrew from all motorsport in the wake of the 1955 Le Mans disaster .
In 1962 , Lotus introduced a car with an aluminium sheet monocoque chassis instead of the traditional space frame design .
In 1968 , Lotus painted Imperial Tobacco livery on their cars , thus introducing sponsorship to the sport .
In the late 1970s , Lotus introduced ground effect aerodynamics that provided enormous downforce and greatly increased cornering speeds ( previously used on Jim Hall 's Chaparral 2J in 1970 ) .
When Ecclestone bought the Brabham team in 1971 he gained a seat on the Formula One Constructors ' Association and in 1978 became its President .
Previously the circuit owners controlled the income of the teams and negotiated with each individually , however Ecclestone persuaded the teams to " hunt as a pack " through FOCA .
The result was the 1981 Concorde Agreement , which guaranteed technical stability , as teams were to be given reasonable notice of new regulations .
By then , however , turbocharged engines , which Renault had pioneered in 1977 , were producing over 700 bhp and were essential to be competitive .
By 1986 , a BMW turbocharged engine achieved a flash reading of 5.5 bar pressure , estimated to be over 1300 bhp in qualifying for the Italian Grand Prix .
To reduce engine power output and thus speeds , the FIA limited fuel tank capacity in 1984 and boost pressures in 1988 before banning turbocharged engines completely in 1989 .
By 1987 , this system had been perfected and was driven to victory by Ayrton Senna in the Monaco Grand Prix that year .
The FIA , due to complaints that technology was determining the outcome of races more than driver skill , banned many such aids for 1994 .
This led to cars that were previously dependent on electronic aids becoming very " twitchy " and difficult to drive ( notably the Williams FW16 ) , and many observers felt the ban on driver aids was in name only as they " have proved difficult to police effectively " .
The teams signed a second Concorde Agreement in 1992 and a third in 1997 , which expired on the last day of 2007 .
On the track , the McLaren and Williams teams dominated the 1980s and 1990s , with Brabham also being competitive in the early part of the 1980s , winning two drivers ' championships with Nelson Piquet .
The FIA worked to improve the sport 's safety standards since that weekend , during which Roland Ratzenberger also lost his life in an accident during Saturday qualifying .
Since the deaths of Ayrton Senna and Roland Ratzenberger , the FIA has used safety as a reason to impose rule changes which otherwise , under the Concorde Agreement , would have had to be agreed upon by all the teams -- most notably the changes introduced for 1998 .
This , according to the FIA , was to promote driver skill and provide a better spectacle .
This increased financial burden , combined with four teams ' dominance ( largely funded by big car manufacturers such as Mercedes-Benz ) , caused the poorer independent teams to struggle not only to remain competitive , but to stay in business .
This has prompted former Jordan owner Eddie Jordan to say the days of competitive privateers are over .
Michael Schumacher and Ferrari won an unprecedented five consecutive drivers ' championships and six consecutive constructors ' championships between 1999 and 2004 .
In 2006 , Renault and Alonso won both titles again .
During this period the championship rules were frequently changed by the FIA with the intention of improving the on-track action and cutting costs .
Team orders , legal since the championship started in 1950 , were banned in 2002 after several incidents in which teams openly manipulated race results , generating negative publicity , most famously by Ferrari at the 2002 Austrian Grand Prix .
The sole exception was McLaren , which at the time was part-owned by Mercedes Benz .
In 2008 and 2009 Honda , BMW , and Toyota all withdrew from Formula One racing within the space of a year , blaming the economic recession .
In the 2010 season Mercedes Benz re-entered the sport as a manufacturer after its purchase of Brawn GP , and split with McLaren after 15 seasons with the team .
This leaves Mercedes , Renault , McLaren and Ferrari as the only car manufacturers in the sport .
The exit of car manufacturers has also paved the way for teams representing their countries , with some having the financial backing of their respective national governments ( such as Lotus ) , something not seen since the 1930s .
The teams ( excepting Ferrari and the other major manufacturers -- Renault and Alfa Romeo in particular ) were of the opinion that their rights and ability to compete against the larger and better funded teams were being negatively affected by a perceived bias on the part of the controlling organisation toward the major manufacturers .
In addition , the battle revolved around the commercial aspects of the sport ( the FOCA teams were unhappy with the disbursement of proceeds from the races ) and the technical regulations which , in FOCA 's opinion , tended to be malleable according to the nature of the transgressor more than the nature of the transgression .
The war culminated in a FOCA boycott of the 1982 San Marino Grand Prix months later .
The FIA President Max Mosley proposed numerous cost cutting measures for the following season , including an optional budget cap for the teams ; teams electing to take the budget cap would be granted greater technical freedom , adjustable front and rear wings and an engine not subject to a rev limiter .
The Formula One Teams Association ( FOTA ) believed that allowing some teams to have such technical freedom would have created a ' two-tier ' championship , and thus requested urgent talks with the FIA .
However talks broke down and FOTA teams announced , with the exception of Williams and Force India , that ' they had no choice ' but to form a breakaway championship series .
It was agreed teams must cut spending to the level of the early 1990s within two years ; exact figures were not specified , and Max Mosley agreed he would not stand for re-election to the FIA presidency in October .
After further disagreements after Max Mosley suggested he would stand for re-election , FOTA made it clear that breakaway plans were still being pursed .
On 8 July , FOTA issued a press release stating they had been informed they were not entered for the 2010 season , and an FIA press release said the FOTA representatives had walked out of the meeting .
On 1 August , it was announced FIA and FOTA had signed a new Concorde Agreement , bringing an end to the crisis and securing the sport 's future until 2012 .
This has not always been the case , and in the earlier history of Formula One many races took place outside the world championship .
In 1952 and 1953 , when the world championship was run for Formula Two cars , a full season of non-championship Formula One racing took place .
Under normal circumstances the winner of the race is the first driver to cross the finish line having completed a set number of laps , which added together should give a distance of approximately 305 km ( 190 mi ) ( 260 km ( 160 mi ) for Monaco ) .
This has happened on only five occasions in the history of the championship , with the last occurrence at the 2009 Malaysian Grand Prix when the race was called off after 31 laps due to torrential rain , and decided the championship winner on at least one occasion .
This requirement distinguishes the sport from series such as the IndyCar Series which allows teams to purchase chassis , and " spec series " such as GP2 , which require all cars be kept to an identical specification .
Ferrari is the only still-active team which competed in 1950 .
Early manufacturer involvement came in the form of a " factory team " or " works team " ( that is , one owned and staffed by a major car company ) , such as those of Alfa Romeo , Ferrari , or Renault .
After having virtually disappeared by the early 1980s , factory teams made a comeback in the 1990s and 2000s and formed up to half the grid with Ferrari , Jaguar BMW , Renault , Toyota , and Honda either setting up their own teams or buying out existing ones .
Mercedes-Benz owned 40 % of the McLaren team and manufactures the team 's engines .
Companies such as Climax , Repco , Cosworth , Hart , Judd and Supertec , which had no direct team affiliation , often sold engines to teams that could not afford to manufacture them .
Cosworth was the last independent engine supplier , but lost its last customers after the 2006 season .
Super Aguri started the season using a modified Honda Racing RA106 chassis ( used by Honda in the 2006 season ) , while Scuderia Toro Rosso used a modified Red Bull Racing RB3 chassis ( same as the one used by Red Bull in the 2007 season ) .
After not being able to secure a package from McLaren , Prodrive 's intention to enter the 2008 season was dropped after Williams threatened legal action against them .
In this case the drivers for the team of the previous year 's champion are given numbers 0 ( Damon Hill , on both occasions ) and 2 ( Prost himself and Ayrton Senna -- replaced after his death by David Coulthard and occasionally Nigel Mansell -- respectively ) .
For many years , for example , Ferrari held numbers 27 and 28 , regardless of their finishing position in the world championship .
Jochen Rindt is the only posthumous World Champion after his points total was not overhauled despite his fatal accident at the 1970 Italian Grand Prix .
Others , such as Damon Hill and Jackie Stewart take active roles in running motorsport in their own countries .
The British Grand Prix , for example , though held every year since 1950 , alternated between Brands Hatch and Silverstone from 1963 to 1986 .
The only other race to have been included in every season is the Italian Grand Prix .
The most recent addition to the calendar is the Abu Dhabi Grand Prix , which hosted the final race of the 2009 season , becoming the first day-to-night race .
Several other circuits are also completely or partially laid out on public roads , such as Spa-Francorchamps .
The only exception being on that of the 2009 specification Red Bull Racing car which uses pullrod suspension at the rear , the first car in over 20 years to do so .
For the 2009 Formula One season the engines have been further restricted to 18,000 RPM .
A high-performance road car like the Ferrari Enzo only achieves around 1g .
Permanent circuits , however , can generate revenue all year round from leasing the track for private races and other races , such as MotoGP .
The Istanbul Park circuit cost $ 150 million to build .
Not all circuits make profits -- Albert Park , for example , lost $ 32 million in 2007 .
The spot was eventually awarded to former B.A.R. and Benetton team principal David Richards ' Prodrive organisation , but the team pulled out of the 2008 season in November 2007 .
The FIA is responsible for making rules to combat the spiralling costs of Formula One racing ( which affects the smaller teams the most ) and for ensuring the sport remains as safe as possible , especially in the wake of the deaths of Roland Ratzenberger and Ayrton Senna in 1994 .
To this end the FIA have instituted a number of rule changes , including new tyre restrictions , multi-race engines , and reductions on downforce .
More recently the FIA has added efficiency to its priorities .
Currently the FIA and manufacturers are discussing adding bio-fuel engines and regenerative braking for the 2011 season or from the start of the 2013 season .
During the early 2000s , Formula One Group created a number of trademarks , an official logo , and an official website for the sport in an attempt to give it a corporate identity .
Prices were too high for viewers , considering they could watch both the qualifying and the races themselves free on ITV .
However , upon the commencement of its coverage for the 2009 season , the BBC reintroduced complementary features such as the " red button " in-car camera angles , multiple soundtracks ( broadcast commentary , CBBC commentary for children , or ambient sound only ) and a rolling highlights package .
For example , in the List of Formula One drivers , Clemente Biondetti is shown with 1 race against his name .
Similarly , several Indy 500 winners technically won their first world championship race , though most record books choose to ignore this and instead only record regular participants .
14 of the 20 drivers ended up not racing due to problems with their Michelin tyres , and the failure to find a suitable solution to the problem left 9 of the ten teams in agreement about hosting a non championship race .
