" Love and Theft " is singer-songwriter Bob Dylan 's 31st studio album , released by Columbia Records in September 2001 .
The album continued Dylan 's artistic comeback following 1997 's Time out of Mind , and was given an even more enthusiastic reception .
Though often referred to without quotations , the correct title is " Love and Theft " .
In 2003 , the album was ranked # 467 on Rolling Stone 's 500 Greatest Albums of All Time , while Newsweek magazine pronounced it the 2nd best album of its decade .
" They 're the kind of twisted , instantly memorable characters one meets in John Ford 's westerns , Jack Kerouac 's road novels , but , most of all , in the blues and country songs of the 1920s , ' 30s and ' 40s .
Subsequently the Dixie Chicks made it a mainstay of their Top of the World , Vote for Change , and Accidents & Accusations Tours .
' It 's tough out there, ' Dylan rasps .
The album closes with " Sugar Baby " , a lengthy , dirge-like ballad , noted for its evocative , apocalyptic imagery and sparse production drenched in echo .
he should recall that Dylan called his first cover album Self Portrait .
All pop music is love and theft , and in 40 years of records whose sources have inspired volumes of scholastic exegesis , Dylan has never embraced that truth so warmly .
If Time Out of Mind was his death album -- it was n't , but you know how people talk -- this is his immortality album .
Entertainment Weekly put it on its end-of-the-decade , " best-of " list , saying , " The predictably unpredictable rock poet greeted the new millennium with a folksy , bluesy instant classic . "
Another line from " Floater " is " My old man , he 's like some feudal lord . "
