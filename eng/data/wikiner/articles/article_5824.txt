The early hacker culture and resulting philosophy originated at the Massachusetts Institute of Technology ( MIT ) in the 1950s and 1960s .
The term ' hacker ethic ' is attributed to journalist Steven Levy as described in his book titled Hackers : Heroes of the Computer Revolution , written in 1984 .
The term " hack " arose from MIT lingo as the word had long been used to describe college pranks that MIT students would regularly devise .
Hackers push programs beyond what they are designed to do .
Levy notes that , at other universities , professors were making public proclamations that computers would never be able to beat a human being in chess .
Hackers knew better .
As Levy stated in the preface of Hackers : Heroes of the Computer Revolution , the general tenets or principles of hacker ethic include :
According to Levy 's account , sharing was the norm and expected within the non-corporate hacker culture .
The principle of sharing stemmed from the atmosphere and resources at MIT .
During the early days of computers and programming , the hackers at MIT would develop a program and share it .
A particular organization of hackers that was concerned with sharing computers with the general public was a group called Community Memory .
PCC opened a computer center where anyone could use the computers there for fifty cents per hour .
In fact , when Bill Gates ' version of BASIC for the Altair was shared among the hacker community , Gates claimed to have lost a considerable sum of money because few users paid for the software .
As a result , Gates wrote an Open Letter to Hobbyists .
This letter was published by several computer magazines and newsletters -- most notably that of the Homebrew Computer Club where much of the sharing occurred .
As Levy described in chapter 2 , " Hackers believe that essential lessons can be learned about the systems -- about the world -- from taking things apart , seeing how they work , and using this knowledge to create new and more interesting things . "
For example , when the computers at MIT were protected either by physical locks or login programs , the hackers there systematically worked around them in order to have access to the machines .
Hackers assumed a " wilful blindness " in the pursuit of perfection .
It is important to note that this behavior was not malicious in nature -- the MIT hackers did not seek to harm the systems or their users ( although occasional practical jokes were played using the computer systems ) .
For example , in Levy 's Hackers , each generation of hackers had geographically based communities where collaboration and sharing occurred .
For the hackers at MIT , it was the labs where the computers were running .
For the hardware hackers ( second generation ) and the game hackers ( third generation ) the geographic area was centered in Silicon Valley where the Homebrew Computer Club and the People 's Computer Company helped hackers network , collaborate , and share their work .
Eric S. Raymond identifies and explains this concept shift in The Cathedral and the Bazaar .
Levy identifies several " true hackers " who significantly influenced the hacker ethic .
Levy also identified the " hardware hackers " ( the " second generation " , mostly centered in Silicon Valley ) and the " game hackers " ( or the " third generation " ) .
All three generations of hackers , according to Levy , embodied the principles of the hacker ethic .
Some Levy 's " second-generation " hackers include :
Levy 's " third generation " practitioners of hacker ethic include :
Free and open source software ( often termed FOSS ) is the descendant of the hacker ethics that Levy described .
