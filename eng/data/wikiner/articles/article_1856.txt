Bolventor is a hamlet on Bodmin Moor in Cornwall , U.K. .
It is situated in Altarnun civil parish between Launceston and Bodmin .
Bolventor is the location of the famous Jamaica Inn coaching inn .
It is bypassed by a dual carriageway section of the A30 trunk road ; before the bypass was built the village straddled the A30 road .
Bolventor parish was established in 1846 but has now been merged with Altarnun .
