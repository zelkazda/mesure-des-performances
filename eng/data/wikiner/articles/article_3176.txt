The Dumbarton Bridge is the southernmost of the highway bridges that span the San Francisco Bay in Calif. .
Its eastern terminus is in Fremont , near Newark in the San Francisco Bay National Wildlife Refuge , and its western terminus is in Menlo Park .
Like the San Mateo Bridge to the north , power lines parallel the bridge across the bay .
There are six toll lanes at the plaza ; the leftmost two are dedicated FasTrak lanes .
When the current bridge was planned in the 1970s , Caltrans conducted extensive environmental research on the aquatic and terrestrial environment .
Principal concerns of the public were air pollution and noise pollution impacts , particularly in some residential areas of Menlo Park and East Palo Alto .
On both sides of the eastern terminus of the bridge are large salt ponds and levee trails belonging to the Don Edwards San Francisco Bay National Wildlife Refuge .
North of the eastern bridge terminus is Coyote Hills Regional Park , with its network of trails running over tall hills .
The earlier bridge , opened on January 17 , 1927 , was the first vehicular bridge to cross San Francisco Bay .
The bridge is part of State Route 84 , and is directly connected to Interstate 880 by a freeway segment north of the Fremont end .
However , it is not directly connected to U.S. 101 at its southwestern end in Menlo Park .
Although the present situation has resulted in severe traffic problems on the bridge itself and in Menlo Park and East Palo Alto , Caltrans has been unable to upgrade the relevant portion of Highway 84 to freeway standards for several decades , due to opposition from the cities of Menlo Park , Atherton and Palo Alto .
A sequence in the movie Harold and Maude takes place on the 1927 span and its eastern approach .
The Dumbarton Bridge is also mentioned in the dialog of the 1992 film Sneakers as well as shown .
