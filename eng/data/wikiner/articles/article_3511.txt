The Detroit River is a 32 mile ( 51 km ) long strait in the Great Lakes system .
The Detroit River has served an important role in the history of Detroit and is one of the busiest waterways in the world .
The river divides the major metropolitan areas of Detroit , Mich. and Windsor , Ontario -- an area referred to as Detroit-Windsor .
The two are connected by the Ambassador Bridge and the Detroit-Windsor Tunnel .
When Detroit underwent rapid industrialization at the turn of the twentieth century , the Detroit River became notoriously polluted and toxic .
There are numerous islands in the Detroit River , and much of the lower portion of the river is incorporated into the Detroit River International Wildlife Refuge .
The portion of the river in the city of Detroit has been organized into the Detroit International Riverfront and the William G. Milliken State Park and Harbor .
The Detroit River flows for 32 miles ( 51 km ) from Lake St. Clair to Lake Erie .
However , today , the Detroit River is rarely referred to as a strait , because bodies of water referred to as straits are typically much wider .
The Detroit River is only between 0.5 -- 2.5 miles ( 0.8 -- 4.0 km ) wide .
The Detroit River starts on an east to west flow but then bends and runs north to south .
The deepest portion of Detroit River is 53 feet ( 16 m ) deep in the northern portion of the river .
The river only drops three feet before entering into Lake Erie at 571 feet ( 174 m ) .
The watershed basin for the Detroit River is approximately 700 sq mi ( 1,813 km² ) .
Its largest tributary is the River Rouge in Mich. , which is actually four times longer than the Detroit River and contains most of the basin .
The only other major American tributary to the Detroit River is the much-smaller Ecorse River .
The discharge for the Detroit River is relatively high for a river of its size .
The river on the American side is all under the jurisdiction of Wayne County , Mich. , and the Canadian side is under the administration of Essex County , Ontario .
The largest city along the Detroit River is Detroit , and most of the population along the river lives in Mich. .
The Detroit River has only two traffic crossings connecting the United States and Canada : the Ambassador Bridge and the Detroit-Windsor Tunnel .
Both of these are heavily protected by the United States Border Patrol and the Canada Border Services Agency .
The upper portion of the river is one of the few places where a Canadian city lies directly south of an American city .
In this case , the city of Detroit is directly north of the city of Windsor , Ontario .
The American cities and communities directly south of Detroit along the river are unofficially referred to as the Downriver area , because those areas are said to be " down the river " from Detroit .
Several of these areas do not actually border the Detroit River ( or are even directly south of Detroit ) , and the term Downriver subjectively binds together the various suburban communities south of Detroit .
The Detroit River contains numerous islands .
The majority of islands are on the American side of the river .
There are no islands in the Detroit River that are divided by the international border .
Most of the islands in the Detroit River are around and closely connected to Grosse Ile .
The Detroit River was first navigated by non-natives in the 17th century .
The French later claimed the area for New France .
The famed sailing ship Le Griffon reached the mouth of the Detroit River in mid-August 1679 on its maiden voyage through the Great Lakes .
Later , when the French began settling in the area , they navigated the river using canoes made of birch or elm bark .
French explorer Antoine Laumet de La Mothe , sieur de Cadillac sailed up the Detroit River on July 23 , 1701 .
The next day , he settled Fort Ponchartrain du Détroit ( now known as Detroit ) .
The river itself became known as the Rivière du Détroit , in which détroit is French for strait .
For that reason , the Detroit River was heavily patrolled , even though it was far removed from any real combat .
Such an attack by the CSA never happened .
At the beginning of the 20th century , Detroit 's industrialization took off on an unprecedented scale .
In 1907 , the Detroit River carried 67,292,504 tons ( 61 billion kg ) of shipping commerce through Detroit to locations all over the world .
There were no bridges connecting Canada and the U.S. until the Ambassador Bridge was finished in 1929 and the Detroit-Windsor Tunnel in 1930 .
Since ferry services were inoperable during the winter months , " rum-runners " traveled across the frozen Detroit River by car to Canada and back with trunk loads of alcohol .
Detroit became the leader in the illegal importation of alcohol , which found its way all over the country .
Much of the land surrounding the Detroit River is urbanized and , in some places , heavily industrialized .
Much of the garbage and sewage from Detroit 's rapid industrialization found its way into the river .
Much of the Detroit River and its shoreline were heavily polluted and unsafe for recreational use .
Large quantities of this pollution collected around the mouth of the Detroit River at Lake Erie .
Because much of this pollution drained into and affected Lake Erie , the lake itself was considered " dead " and unable to support aquatic life .
In 1961 , the Wyandotte National Wildlife Refuge was founded by congressional order .
However , there was little support toward cleaning up the river , because it would negatively affect Detroit 's industrialism and economy .
In 1970 , the entire fishing industry in the St. Clair River , Lake St. Clair , the Detroit River , and Lake Erie had to be closed due to toxic levels of mercury found in the water .
This , in turn , spurred a massive conservation effort aimed at cleaning up the Detroit River .
In 2001 , the Wyandotte National Wildlife Refuge was absorbed into the much larger Detroit River International Wildlife Refuge , which is a cooperative effort between the U.S. and Canada to preserve the area as a ecological refuge .
Millions of dollars so far have been used to dredge pollutants out of the river , and the recent cleaning up and restoration of the Detroit River is remarkable , although other problems are still at hand .
Historically , the Detroit River was used primarily for shipping and trading .
When the fur trade decreased , Mich. had already began to exploit the lumber-rich areas of the Upper Peninsula .
Detroit turned into a major industrial region , largely in part because of the Detroit River .
The only way a ship could travel out of the Great Lakes system was to travel down the Detroit River .
First hand , the Detroit River also provides a substantial amount of revenue for the local economies .
A study in 1991 showed that $ 20.1 million came from sales related to waterfowl hunting along the Detroit River .
During the same year , bird watching , photography , and other non-consumptive uses of waterfowl contributed an additional $ 192.8 million to Michigan 's economy .
There are over 800,000 recreation boats in the state of Michigan , and more than half of those are regularly used on or near the Detroit River .
It is estimated that walleye fishing alone brings in $ 1 million to the economy of communities along the lower Detroit River each spring .
A percentage of the tourist revenue also depends on the Detroit River , which is the most noticeable environmental feature in Detroit .
Popular river destinations in the city of Detroit include the Detroit International Riverfront and Belle Isle Park -- both of which host a number of events throughout the year .
The iconic Renaissance Center is located on the banks of the Detroit River .
According to a 2004 study , 150,000 jobs and $ 13 billion in annual production depend on the river crossings connecting Detroit to Windsor .
In 2004 , the total American trade with Ontario alone was $ 407 billion , in which 28 % ( $ 113.3 billion ) crossed the Detroit River .
Because this puts a large strain on the flow for the only two traffic crossings over the Detroit River , proposals have been made to create a third crossing to connect Detroit and Windsor .
This is a list of bridges and other crossings of the Detroit River from Lake Erie upstream to Lake St. Clair .
The only two traffic routes that completely cross the river are the Ambassador Bridge and the Detroit-Windsor Tunnel .
In Michigan , there are two bridges connecting the mainland to Grosse Ile and one bridge to Belle Isle Park .
All crossings on the American side are secured by the United States Border Patrol and on the Canadian side by the Canada Border Services Agency .
The Detroit River International Crossing is a new bridge project expected to be completed in approximately 2013 ; it will directly connect Highway 401 in Canada to Interstate 75 in the US .
