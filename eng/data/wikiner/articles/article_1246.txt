It is located in the Atlantic Ocean north of Cuba , Hispaniola ( Dominican Republic and Haiti ) and the Caribbean Sea , northwest of the Turks and Caicos Islands , and southeast of the United States of America ( nearest to the state of Florida ) .
Its capital is Nassau .
Geographically , the Bahamas lie in the same island chain as Cuba , Hispaniola ( Dominican Republic and Haiti ) and Turks and Caicos Islands , the designation of the Bahamas refers normally to the commonwealth and not the geographic chain .
The islands were mostly deserted from 1513 to 1650 , when British colonists from Bermuda settled on the island of Eleuthera .
During proprietary rule , the Bahamas became a haven for pirates , including the infamous Blackbeard .
The capital of Nassau on the island of New Providence was occupied by US Marines for a fortnight .
These Americans established plantations on several islands and became a political force in the capital .
Slavery itself was finally abolished in the British Empire on August 1 , 1834 .
Modern political development began after the Second World War .
In 1967 , Lynden Pindling of the Progressive Liberal Party became the first black premier of the colony , and in 1968 the title was changed to prime minister .
In 1973 , The Bahamas became fully independent , but retained membership in the Commonwealth of Nations .
However , there remain significant challenges in areas such as education , health care , international narcotics trafficking and illegal immigration from Haiti .
The origin of the name " Bahamas " is unclear .
The closest island to the United States is Bimini , which is also known as the gateway to The Bahamas .
The island of Abaco is to the east of Grand Bahama .
The southeasternmost island is Inagua .
The largest island is Andros Island .
Other inhabited islands include Eleuthera , Cat Island , Long Island , San Salvador Island , Acklins , Crooked Island , Exuma and Mayaguana .
Nassau , capital city of The Bahamas , lies on the island of New Providence .
To the southeast , the Turks and Caicos Islands , and three more extensive submarine features called Mouchoir Bank , Silver Bank , and Navidad Bank , are geographically a continuation of The Bahamas , but not part of the Commonwealth of The Bahamas .
Hurricane Andrew hit the northern islands during the 1992 Atlantic hurricane season , and Hurricane Floyd hit most of the islands during the 1999 Atlantic hurricane season .
The Bahamas is a sovereign independent nation .
The Bahamas is a parliamentary democracy with two main parties , the Free National Movement and the Progressive Liberal Party .
The Bahamas is a member of the Commonwealth of Nations , with Queen Elizabeth II as head of state ( represented by a Governor-General ) .
The current governor-general is Sir Arthur Foulkes and the current Prime Minister is Hubert Ingraham .
The Bahamas has a largely two-party system dominated by the centre-left Progressive Liberal Party and the centre-right Free National Movement .
Although The Bahamas is not geographically located in the Caribbean , it is a member of the Caribbean Community .
The districts other than New Providence are :
The shield is supported by a marlin and a flamingo , which are the national animals of the Bahamas .
Below this is the actual shield , the main symbol of which is a ship representing the Santa María of Christopher Columbus , shown sailing beneath the sun .
The yellow elder was chosen as the national flower of the Bahamas because it is native to the Bahama Islands , and it blooms throughout the year .
One of the most prosperous countries in the Caribbean region , the Bahamas relies on tourism to generate most of its economic activity .
They form the largest minority group in the Bahamas at 12 % of the population .
The practice of obeah is however illegal in the Bahamas and punishable by law .
