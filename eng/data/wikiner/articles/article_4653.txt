The men discussed a recent spate of UFO reports and an Alan Dunn cartoon facetiously blaming the disappearance of municipal trashcans on marauding aliens .
Such signals could be either " accidental " by-products of a civilization , or deliberate attempts to communicate , such as the Communication with Extraterrestrial Intelligence 's Arecibo message .
However , the Big Ear only looks at each point on the sky for 72 seconds , and re-examinations of the same spot have found nothing .
In 2003 , Radio source SHGb02+14a was isolated by SETI @ home analysis , although it has largely been discounted by further study .
New refinements in exoplanet detection methods , and use of existing methods from space , ( such as the Kepler Mission , launched in 2009 ) are expected to detect and characterize terrestrial-size planets , and determine if they are within the habitable zones of their stars .
In 1959 , Dr. Freeman Dyson observed that every developing human civilization constantly increases its energy consumption , and theoretically , a civilization of sufficient age would require all the energy produced by its star .
Geoffrey Miller proposes that human intelligence is the result of runaway sexual selection , which takes unpredictable directions .
In 1966 Sagan and Shklovskii suggested that technological civilizations will either tend to destroy themselves within a century of developing interstellar communicative capability or master their self-destructive tendencies and survive for billion-year timescales .
From a Darwinian perspective , self-destruction would be a paradoxical outcome of evolutionary success .
In 1981 , cosmologist Edward Harrison argued that such behavior would be an act of prudence : an intelligent species that has overcome its own self-destructive tendencies might view any other species bent on galactic expansion as a kind of virus .
Humans are moving to directional or guided transmission channels such as electrical cables , optical fibers , narrow-beam microwave and lasers , and conventional radio with non-directional antennas is increasingly reserved for low-power , short-range applications such as cell phones and Wi-Fi networks .
Analog television , developed in the mid-twentieth century , contains strong carriers to aid reception and demodulation .
Surprisingly early treatments , such as Lewis Padgett 's short story Mimsy were the Borogoves ( 1943 ) , suggest a migration of advanced beings out of the presently known physical universe into a different and presumably more agreeable alternative one .
The simulation argument by Bostrom holds that although such a simulation may contain other life , such life can not be much in advance of us since a far more advanced civilization may be correspondingly hard to simulate .
For example , in Contact , Carl Sagan briefly speculated that an alien species might have a thought process orders of magnitude slower ( or faster ) than humans .
Carl Sagan and Iosif Shklovsky argued for serious consideration of " paleocontact " with extraterrestrials in the early historical era , and for examination of myths and religious lore for evidence of such contact .
