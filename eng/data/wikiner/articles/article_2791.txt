Chick Publications ' best-known products are Chick tracts , which are comic tracts that are available in nearly 100 languages .
Chick 's " espous [ ing ] a variety of hateful doctrines " have moved the Southern Poverty Law Center to label the organization as a hate group .
Chick Publications has its headquarters in Rancho Cucamonga , while it has an Ontario , Calif. mailing address .
All of Chick Publications ' tracts , and several excerpts from his full-length comics , may be read without charge at the Chick website .
Many older tracts are out of print ; however , Chick Publications will print a minimum 10,000 tract special order of any out-of-print series .
On the company 's website they also note that " Our ministry is primarily publishing the gospel tracts of Jack T. Chick , but we do occasionally publish a manuscript in book form . "
Like many other religious publishing organizations , Chick Publications does not distribute their tracts for free ; they are normally purchased in bulk by the people handing them out .
Chick Publications also publishes conventional non-graphical books on these same topics , by authors other than Chick .
Chick Publications , and its founder and namesake Jack T. Chick , have been regularly criticized for publishing strong opinions seen as negative such as opposition to Freemasonry , and Catholicism .
Many of the products also oppose secularized holidays such as Halloween and Thanksgiving , as well as several forms of entertainment such as role-playing games and popular music .
Defenders of the comics assert all his comics carry the same message : salvation through Jesus .
Other endings provide a contrast between those who accept Jesus and those who reject Jesus ; in some tracts , a convert receives entry to heaven , while in other tract , a non-believer is condemned to hell .
Most Chick tracts end with a suggested prayer for the reader to pray to accept Christ .
The graphics in Chick 's tracts are often simple , but eye-catching .
Chick 's tracts and other publications make many controversial claims .
For example , Chick claims that the theory of evolution is false , homosexuality is sinful , and abortion is murder .
A recurring theme in Chick 's tracts is the role of the Roman Catholic Church , which he presents as one of the most powerful and insidious branches of this conspiracy .
When that failed , they arranged the assassination of Abraham Lincoln and created the Ku Klux Klan , which happens to be anti-Catholic itself .
Communism would rise up as the new strong daughter of the Vatican . "
Most forms of fantasy and depictions of magic , including Harry Potter , Dungeons & Dragons .
and Halloween celebrations are portrayed as an attempt to draw children into witchcraft .
In reality , Osiris was never a solar deity , but rather associated with fertility , agriculture , and death .
Many of Chick 's critics consider these sources to be frauds or fantasists .
Nevertheless , many Chick supporters believe their claims to be legitimate .
The Chick Publications website is blocked in Singapore .
Some of the criticism of Chick tracts has taken the form of parody tracts pointing out all the errors ( logical and factual ) in the originals .
Jack Chick claims that cartoons are a more effective medium for witnessing than conventional text based tracts .
Chick 's more controversial claims are usually accompanied by supporting references to the Bible ( always quoting the King James Version ) , other books ( often also published by Chick ) , and historical facts ; debate commonly focuses on the reliability of these sources and of Chick 's representation of them .
Jack T. Chick 's tracts have had an unintentional influence on pop culture .
Comedian David Cross ( a self-professed atheist ) released a DVD of his comedy works entitled Let America Laugh with the chapters titled after Chick tracts .
