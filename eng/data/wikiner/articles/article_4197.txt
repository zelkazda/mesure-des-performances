Emperor Ninken , also known as Ninken-okimi , was the 24th emperor of Japan , according to the traditional order of succession .
Ninken is considered to have ruled the country during the late-5th century , but there is a paucity of information about him .
The two young princes were said to be grandsons of Emperor Richū .
Each of these brothers would ascend the throne as adopted heirs of Seinei , although it is unclear whether they had been " found " in Seinei 's lifetime or only after that .
Ninken 's contemporary title would not have been tennō , as most historians believe this title was not introduced until the reigns of Emperor Tenmu and Empress Jitō .
Ninken was succeeded by his son , who would accede as Emperor Buretsu .
The actual site of Ninken 's grave is not known .
The Imperial Household Agency designates this location as Ninken 's mausoleum .
