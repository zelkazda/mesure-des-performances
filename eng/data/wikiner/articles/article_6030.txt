Lev Vygotsky suggested that mental functions , such as concepts , language , voluntary attention and memory , are cultural tools acquired through social interactions [ citation needed ] .
