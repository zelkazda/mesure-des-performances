A direct descendant of David will be the Mashiach .
God withdraws his favor from Saul , king of Israel .
The prophet Samuel seeks a new king from the sons of Jesse of Bethlehem .
and Jesse answers , " There is still the youngest but he is tending the sheep . "
God sends an evil spirit to torment Saul and his attendants suggest he send for David , a young warrior famed for his bravery and for his skill with the harp .
Then relief would come to Saul ; he would feel better , and the evil spirit would leave him . "
The boy David is bringing food to his older brothers who are with King Saul .
David tells Saul he is prepared to face Goliath and Saul allows him to make the attempt .
He is victorious , striking Goliath in the forehead with a stone from his sling , and the Philistines flee in terror .
Saul sends to know the name of the young champion , and David tells him that he is the son of Jesse .
Saul makes David a commander over his armies and offers him his daughter Michal in marriage .
David is successful in many battles , and his popularity awakes Saul 's fears -- " What more can he have but the kingdom ? "
By various stratagems the jealous king seeks his death , but the plots only endear David the more to the people , and especially to Saul 's son Jonathan , who loves David ( 1 Samuel 18:1 , 2 Samuel 1:25-26 ) .
Achish marches against Saul , but David is excused from the war on the accusation of the Philistine nobles that his loyalty to their cause can not be trusted .
War ensues between Ish-Bosheth and David , until Ish-Bosheth is murdered .
The assassins bring the head of Ish-Bosheth to David hoping for reward , but David executes them for their crime against the Lord 's anointed .
With Yahweh 's help David is victorious over his people 's enemies .
The Philistines are subdued , the Moabites to the east pay tribute , and Hadadezer of Zobah , from whom David takes gold shields and bronze vessels .
David commits adultery with Bathsheba , the wife of Uriah the Hittite .
Uriah refuses to do so while his companions are in the field of battle and David sends him back to Joab , the commander , with a message instructing him to abandon Uriah on the battlefield , " that he may be struck down , and die . "
David marries Bathsheba and she bears his child , " but the thing that David had done displeased the Lord . "
You have smitten Uriah the Hittite with the sword , and have taken his wife to be your wife . "
David leaves his lamentations , dresses himself , and eats .
David replies : " While the child was still alive , I fasted and wept ; for I said , who knows whether Yahweh will be gracious to me , that the child may live ?
David 's son Absalom rebels against his father , and they come to battle in the Wood of Ephraim .
Absalom is caught by his hair in the branches of an oak and David 's general Joab kills him as he hangs there .
When the news of the victory is brought to David he does not rejoice , but is instead shaken with grief : " O my son Absalom , my son , my son Absalom !
When David has become old and bedridden Adonijah , his eldest surviving son and natural heir , declares himself king .
Bathsheba , David 's favourite wife , and Nathan the prophet , fearing that they will be killed by Adonijah , go to David and procure his agreement that Solomon , Bathsheba 's son , should sit on the throne .
And so the plans of Adonijah collapse , and Solomon becomes king .
David is an important figure in Judaism .
Historically , David 's reign represented the formation of a coherent Jewish kingdom centered in Jerusalem .
David is an important figure within the context of Jewish messianism .
In the Hebrew Bible , it is written that a human descendant of David will occupy the throne of a restored kingdom and usher a messianic age .
In modern Judaism David 's descent from a convert is taken as proof of the importance of converts within Judaism .
David is also viewed as a tragic figure ; his acquisition of Bathsheba , and the loss of his son are viewed as his central tragedies .
Many legends have grown around the figure of David .
Only at his anointing by Samuel -- when the oil from Samuel 's flask turned to diamonds and pearls -- was his true identity as Jesse 's son revealed .
David 's adultery with Bathsheba was only an opportunity to demonstrate the power of repentance , and some Talmudic authors stated that it was not adultery at all , quoting a Jewish practice of divorce on the eve of battle .
Furthermore , according to David 's apologists , the death of Uriah was not to be considered murder , on the basis that Uriah had committed a capital offence by refusing to obey a direct command from the King .
David was also given the most beautiful voice of all mankind , just as Joseph was given the most beautiful appearance .
In one hadith , Abu Hurairah narrates that Muhammad said , " The reciting of the Zabur was made easy for David .
He used to order that his riding animals be saddled , and would finish reciting the Zabur before they were saddled . "
The Mesha Stele from Moab , dating from approximately the same period , may also contain the name David in line 12 , where the interpretation is uncertain , and in line 31 , where one destroyed letter must be supplied .
The biblical account about David comes from the book of Samuel , and the book of Chronicles .
The question of David 's historicity therefore becomes the question of the date , textual integrity , authorship and reliability of 1st and 2nd Samuel .
Samuel 's account of David " seems to have undergone two separate acts of editorial slanting .
The original writers show a strong bias against Saul , and in favour of David and Solomon .
Within this gamut some interesting studies of David have been written .
According to Ruth 4:18-22 , David is the tenth generation descendant from Judah , the fourth son of the patriarch Jacob .
His father was named Jesse .
David had seven brothers and was the youngest of them all .
The Book of Chronicles lists David 's sons by various wives and concubines .
Her rape leads to Amnon 's death .
Famous sculptures of David include ( in chronological order ) those by :
