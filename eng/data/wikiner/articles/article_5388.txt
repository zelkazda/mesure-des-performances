Its capital is Budapest .
Today , Hungary is a high-income economy , and a regional leader regarding certain markers .
From 9 BC to the end of the 4th century , Pannonia was part of the Roman Empire on a part of later Hungary 's area .
Among the first to arrive were the Huns , who built up a powerful empire under Attila the Hun .
Attila was regarded as an ancestral ruler of the Hungarians , however , this claim is rejected today by most scholars .
Neither of these two nor others were able to create a lasting state in the region , and in the late 9th century the land was inhabited only by a sparse population of Slavs .
The freshly unified Magyars led by Árpád settled in the Carpathian Basin starting in 895 .
A later defeat at the Battle of Lechfeld in 955 signaled an end to most campaigns on foreign territories .
and the first Roman Catholic bishopric was established under his reign .
Duke Koppány took up arms , and many people in Transdanubia joined him .
Hungary was recognized as a Catholic Apostolic Kingdom under Saint Stephen I , the son of Géza and thus a descendant of Árpád .
Applying to Pope Sylvester II , Stephen received the insignia of royalty from the papacy .
He was crowned in December 1000 , in the capital , Esztergom .
Stephen established a network of 10 episcopal and 2 archiepiscopal sees , and ordered the building of monasteries , churches and cathedrals .
The country switched to using the Latin language and alphabet under Stephen , and until as late as 1844 , Latin remained the official language of Hungary .
Stephen followed the Frankish administrative model : The country was divided into counties ( Hungarian : megye ) , each under a royal official called an ispán or count ( Latin : comes ) -- later főispán ( lord lieutenant or prefect ) ( Latin : supremus comes ) .
Cumans constituted perhaps up to 7-8 % of the population of Hungary in the second half of the 13th century .
Over the centuries they were fully assimilated into the Hungarian population , and their language disappeared , but they preserved their identity and their regional autonomy until 1876 .
These castles proved to be very important later in the long struggle with the Ottoman Empire .
Árpád 's direct descendants in the male line ruled the country until 1301 .
During the reigns of the Árpád dynasty , the Kingdom of Hungary reached its greatest extent , yet royal power was weakened as the major landlords greatly increased their influence .
One of the primary sources of his power was the wealth derived from the gold mines of eastern and northern Hungary .
After Italy , Hungary was the first European country where the renaissance appeared .
Louis I established a university in Pécs in 1367 ( by papal accordance ) .
King Louis died without a male heir , and after years of anarchy the country was stabilized only when Sigismund ( reigned 1387 -- 1437 ) , a prince of the Luxembourg line , succeeded to the throne by marrying the daughter of Louis the Great , Queen Mary .
It was not for entirely selfless reasons that one of the leagues of barons helped him to power : Sigismund had to pay for the support of the lords by transferring a sizeable part of the royal properties .
The first Hungarian Bible translation was completed in 1439 .
For a half year in 1437 , there was an antifeudal and anticlerical peasant revolt in Transylvania which was strongly influenced by Hussite ideas .
From a small noble family in Transylvania , John Hunyadi grew to become one of the country 's most powerful lords , thanks to his outstanding capabilities as a mercenary commander .
In 1446 , the parliament elected the great general John Hunyadi governor ( 1446 -- 1453 ) , then regent ( 1453 -- 1456 ) .
András Hess set up a printing press in Buda in 1472 , which was very unique at that time in Europe .
This was the first time in the history of the Hungarian kingdom that a member of the nobility , without dynastic ancestry and relationship , mounted the royal throne .
By the early 16th century , the Ottoman Empire had become the second most populous state in the world ; this enabled the creation of the largest armies of the era .
Hungary 's international role declined , its political stability shaken , and social progress was deadlocked .
The resulting degradation of order paved the way for Ottoman pre-eminence .
In 1526 , the Hungarian army was crushed at the Battle of Mohács by the Ottomans .
The childless young king Louis II , and the leader of the Hungarian army , Pál Tomori died on the battlefield .
The Ottomans gained a decisive victory over the Hungarian army at the Battle of Mohács in 1526 .
Even with a decisive 1552 victory over the Ottomans at the Siege of Eger , which raised the hopes of the Hungarians , the country remained divided until the end of the 17th century .
The remaining central area ( mostly present-day Hungary ) , including the capital of Buda was known as the Pashalik of Buda .
Nagyszombat ( today Trnava ) in turn , became the religious center in 1541 .
There were a series of other successful and unsuccessful anti-Habsburg /i.e .
The Hungarian aristocracy successfully preserved its former positions in the political and economic sphere .
He later fled to France , finally Turkey , and lived to the end of his life ( 1735 ) in nearby Rodosto .
Therefore the achievements were mostly of national character ( e.g. introduction of Hungarian as one of the official languages of the country , instead of the former Latin ) .
Count István Széchenyi , one of the most prominent statesmen of the country recognized the urgent need of modernization and his message got through .
The Hungarian Parliament was reconvened in 1825 to handle financial needs .
Habsburg monarchs tried to preclude [ citation needed ] the industrialisation of the country .
A remarkable upswing started as the nation concentrated its forces on modernisation even though the Habsburg monarchs obstructed all important liberal laws about the human civil and political rights and economic reforms .
Many reformers ( like Lajos Kossuth , Mihály Táncsics ) were imprisoned by the authorities .
Faced with revolution both at home and in Vienna , Austria first had to accept Hungarian demands .
In July 1849 the Hungarian Parliament proclaimed and enacted the first laws of ethnic and minority rights in the world .
Lajos Kossuth escaped into exile .
Major military defeats of Austria , like the Battle of Königgrätz ( 1866 ) , forced the Emperor to concede internal reforms .
Austria-Hungary was geographically the second largest country in Europe after the Russian Empire ( 239,977 sq .
In 1873 , the old capital Buda and Óbuda were officially merged with the third city , Pest , thus creating the new metropolis of Budapest .
The dynamic Pest grew into the country 's administrative , political , economic , trade and cultural hub .
That level of growth compared very favorably to that of other European nations such as Britain ( 1.00 % ) , France ( 1.06 % ) , and Germany ( 1.51 % ) .
Many of the state institutions and the modern administrative system of Hungary were established during this period .
Austria -- Hungary drafted 9 million ( fighting forces : 7,8 million ) soldiers in World War I ( Over 4 million from the Kingdom of Hungary ) .
In World War I Austria -- Hungary was fighting on the side of Germany , Bulgaria and Turkey .
The Central Powers conquered Serbia .
Romania declared war .
On November 1916 Emperor Franz Joseph died , the new monarch Charles IV sympathized with the pacifists .
With great difficulty , the Central powers stopped and repelled the attacks of the Russian Empire .
The Austro-Hungarian Empire then withdrew from all defeated countries .
On the Italian front , the Austro-Hungarian army could not make more successful progress against Italy after January 1918 .
Austria-Hungary signed general armistice in Padua on 3 November 1918 .
In October 1918 , the personal union with Austria was dissolved .
Károlyi was a devotee of Entente from the beginning of the World War .
By a notion of Woodrow Wilson 's pacifism , Károlyi ordered the full disarmament of Hungarian Army .
Hungary remained without national defense in the darkest hour of its history .
The Károlyi government pronounced illegal all armed associations and proposals which wanted to defend the integrity of the country .
The Károlyi government also dissolved the gendarme and police , the lack of police force caused big problems in the country .
On March 21 , after the Entente military representative demanded more and more territorial concessions from Hungary , Károlyi resigned .
Károlyi ( with a new Czechoslovakian passport and Czechoslovak diplomatic help ) moved to Paris .
The Communist Party of Hungary , led by Béla Kun , came to power and proclaimed the Hungarian Soviet Republic .
The Red Army of Hungary was a little voluntary army ( 53,000 men ) .
Most soldiers of the Red Army were armed factory workers from Budapest .
The Soviet Red Army was never able to aid the new Hungarian republic .
Despite the great military successes against Czechoslovakian army , the communist leaders gave back all recaptured lands .
The Hungarian Red Army was dissolved before it could successfully complete its campaigns .
In the aftermath of a coup attempt , the government took a series of actions called the Red Terror , murdering several hundred people ( mostly intellectuals ) , which alienated much of the population .
All these events , and in particular the final military defeat , led to a deep feeling of dislike among the general population against the Soviet Union ( which had not kept its promise to offer military assistance ) and the Jews ( since most members of Kun 's government were Jewish ) .
His government gradually restored security police and gendarmee , stopped terror , and set up authorities , but thousands of supporters of the leftist-liberal Károlyi and communist Kun regimes were imprisoned .
In March , the parliament restored the Hungarian monarchy but postponed electing a king until civil disorder had subsided .
Hungary 's signing of the Treaty of Trianon on June 4 , 1920 , ratified the country 's dismemberment .
The territorial provisions of the treaty , which ensured continued discord between Hungary and its neighbors , required Hungary to surrender more than two-thirds of its pre-war lands .
New international borders separated Hungary 's industrial base from its sources of raw materials and its former markets for agricultural and industrial products .
Hungary lost 84 % of its timber resources , 43 % of its arable land , and 83 % of its iron ore .
In addition , 61 % of arable land , 74 % of public road , 65 % of canals , 62 % of railroads , 64 % of hard surface roads , 83 % of pig iron output , 55 % of industrial plants , 100 % of gold , silver , copper , mercury and salt mines , and 67 % of credit and banking institutions of the former Kingdom of Hungary lay within the territory of Hungary 's neighbors .
Because most of the country 's pre-war industry was concentrated near Budapest , Hungary retained about 51 % of its industrial population , 56 % of its industry .
Horthy appointed Count Pál Teleki as Prime Minister in July 1920 .
His government issued a numerus clausus law , limiting admission of " political insecure elements " ( these were often Jews ) to universities and , in order to quiet rural discontent , took initial steps toward fulfilling a promise of major land reform by dividing about 3,850 km 2 from the largest estates into smallholdings .
Teleki 's government resigned , however , after , Charles IV , unsuccessfully attempted to retake Hungary 's throne in March 1921 .
Horthy then appointed Bethlen prime minister .
Charles IV died soon after he failed a second time to reclaim the throne in October 1921 .
As prime minister , Bethlen dominated Hungarian politics between 1921 and 1931 .
Bethlen restored order to the country by giving the radical counterrevolutionaries payoffs and government jobs in exchange for ceasing their campaign of terror against Jews and leftists .
Bethlen brought Hungary into the League of Nations in 1922 and out of international isolation by signing a treaty of friendship with Italy in 1927 .
The revision of the Treaty of Trianon rose to the top of Hungary 's political agenda and the strategy employed by Bethlen consisted by strengthening the economy and building relations with stronger nations .
In 1932 Horthy appointed a new prime-minister , Gyula Gömbös , that changed the course of Hungarian policy towards closer cooperation with Germany .
Imrédy 's ( a Jewish descendant ) attempts to improve Hungary 's diplomatic relations with the United Kingdom initially made him very unpopular with Germany and Italy .
This definition altered the status of those who had formerly converted from Judaism to Christianity .
In 1941 Hungary participated in its first military manoeuvres as part of the Axis .
Thus the Hungarian army was part of the invasion of Yugoslavia , gaining some more territory .
In late 1941 , the Hungarian troops on the Eastern Front experienced success at the Battle of Uman .
By 1943 , after the Hungarian Second Army suffered extremely heavy losses at the River Don , the Hungarian government sought to negotiate a surrender with the Allies .
By then it was clear that Hungarian politics was suppressed by Hitler 's intent to hold the country in war on the side of the Nazi Third Reich because of its strategic location .
On October 15 , 1944 , Miklós Horthy made a token effort to disengage Hungary from the war .
In late 1944 , Hungarian troops on the Eastern Front again experienced success at the Battle of Debrecen , but this was followed immediately by the Soviet invasion of Hungary and the Battle of Budapest .
Hundreds of Hungarian people were executed by the Arrow Cross Party for sheltering Jews .
The war left Hungary devastated destroying over 60 % of the economy and causing huge loss of life .
On February 13 , 1945 , the Hungarian capital city surrendered unconditionally .
On May 8 , 1945 , World War II in Europe officially ended .
Following the fall of Nazi Germany , Soviet troops occupied all of the country and through their influence Hungary gradually became a communist satellite state of the Soviet Union .
Mátyás Rákosi now attempted to impose authoritarian rule on Hungary .
Hungary experienced one of the harshest dictatorships in Europe .
Rákosi had difficulty managing the economy and the people of Hungary saw living standards fall .
His government became increasingly unpopular , and when Joseph Stalin died in 1953 , Mátyás Rákosi was replaced as prime minister by Imre Nagy .
However , he retained his position as general secretary of the Hungarian Workers Party and over the next three years the two men became involved in a bitter struggle for power .
As Hungary 's new leader , Imre Nagy removed state control of the mass media and encouraged public discussion on political and economic reform .
Nagy also released anti-communists from prison and talked about holding free elections and withdrawing Hungary from the Warsaw Pact .
Nagy was removed by Soviets .
The rule of the Rákosi government was nearly unbearable for Hungary 's war-torn citizens .
This led to the 1956 Hungarian Revolution and Hungary 's temporary withdrawal from the Warsaw Pact .
The multi-party system was restored by Nagy .
Soviets and Hungarian political police ( AVH ) shot at peaceful demonstrators and many demonstrators died throughout the country , which made the events irreversible .
The immense Soviet preponderance suffered heavy losses , by 30 October most Soviet troops had withdrawn from Budapest to garrisons in the Hungarian countryside .
The Soviet Union sent new armies to Hungary .
On 4 November 1956 , the Soviets retaliated massively with military force , sending in over 150,000 troops and 2,500 tanks .
János Kádár ( who was the appointed leader by the Soviets ) reorganized the communist party as the puppet of the Soviets .
Once he was in power , Kádár led an attack against revolutionaries .
Imre Nagy , the legal Prime Minister of the country was condemned to death .
This was under the autocratic rule of its controversial communist leader , János Kádár .
It was the so called Kádár era ( 1956 -- 1988 ) .
With the Soviet Union gone the transition to a market economy began .
In June 1987 Károly Grósz took over as premier .
Opposition demonstrations filled the streets of Budapest with more than 75,000 marchers .
The last speaker , 26-year-old Viktor Orbán publicly called for Soviet troops to leave Hungary .
In July U.S. President George Bush visited Hungary .
The resulting exodus shook East Germany and hastened the fall of the Berlin Wall .
On October 23 , Mátyás Szűrös declared Hungary a republic .
This was not enough to shake off the stigma of four decades of autocratic rule , however , and the 1990 election was won by the centre-right Hungarian Democratic Forum ( MDF ) , which advocated a gradual transition towards capitalism .
The liberal Alliance of Free Democrats ( SZDSZ ) , which had called for much faster change , came second and the Socialist Party trailed far behind .
As Gorbachev looked on , Hungary changed political systems with scarcely a murmur and the last Soviet troops left Hungary in June 1991 .
In coalition with two smaller parties , the MDF provided Hungary with sound government during its hard transition to a full market economy .
This in no way implied a return to the past , and party leader Gyula Horn was quick to point out that it was his party that had initiated the whole reform process in the first place ( as foreign minister in 1989 Horn played a key role in opening Hungary 's border with Austria ) .
In March 1996 , Horn was re-elected as Socialist Party leader and confirmed that he would push ahead with the party 's economic stabilisation programme .
In 1997 in a national referendum 85 % voted in favour of Hungary joining the NATO .
A year later the European Union began negotiations with Hungary on full membership .
In 1999 Hungary joined NATO .
Hungary voted in favour of joining the EU , and joined in 2004 .
The world 's first institution of technology was founded in Selmecbánya , Hungarian Kingdom ( today Slovakia ) in 1735 .
Budapest University of Technology and Economics ( BME ) is considered the oldest institution of technology in the world , which has university rank and structure .
Hungary is famous for its excellent mathematics education which has trained numerous outstanding scientists .
Famous Hungarian mathematicians include János Bolyai , designer of modern geometry in 1831 .
First electric motor ( 1827 ) and first electrical generator ( Ányos Jedlik ) .
Ottó Bláthy , Miksa Déri and Károly Zipernowsky invented the transformer in 1885 .
The letter led directly to the establishment of research into nuclear fission by the U.S. government and ultimately to the creation of the Manhattan Project .
Szilárd , with Enrico Fermi , patented the nuclear reactor ) .
The unicameral , 386-member National Assembly ( Országgyűlés ) is the highest organ of state authority and initiates and approves legislation sponsored by the Prime Minister .
An 11-member Constitutional Court has power to challenge legislation on grounds of unconstitutionality .
Administratively , Hungary is divided into 19 counties .
In addition , the capital ( főváros ) , Budapest , is independent on any county government .
The counties and the capital are the 20 NUTS third-level units of Hungary .
Since 1996 , the counties and City of Budapest have been grouped into 7 regions for statistical and development purposes .
These seven regions constitute NUTS ' second-level units of Hungary .
The governing coalition , comprising the Hungarian Socialist Party and the liberal Alliance of Free Democrats , prevailed in the April 2006 general election .
Hungary needs to reduce government spending and further reform its economy in order to meet the 2012 -- 2013 target date for accession to the euro zone .
Hungary has continued to demonstrate economic growth as one of the newest member countries of the European Union ( since 2004 ) .
Today , Hungary is a full member of OECD and the World Trade Organization .
OECD was the first international organization to accept Hungary as a full member in 1996 , after six years of successful cooperation .
Hungary 's strategic position in Europe and its relative high lack of natural resources also have dictated a traditional reliance on foreign trade .
For instance , its largest car manufacturer , Magomobil , produced a total of a few thousand units .
In the face of economic stagnation , Hungary opted to try further liberalization by passing a joint venture law , instating an income tax , and joining the International Monetary Fund ( IMF ) and the World Bank .
By 1988 , Hungary had developed a two-tier banking system and had enacted significant corporate legislation which paved the way for the ambitious market-oriented reforms of the post-communist years .
Another form of Soviet subsidizing that greatly affected Hungary after the fall of communism was the loss of social welfare programs .
Following privatization and tax reductions on Hungarian businesses , unemployment suddenly rose to 12 % in 1991 ( it was 1.7 % in 1990 ) , gradually decreasing until 2001 .
With the stabilization of the new market economy , Hungary has experienced growth in foreign investment with a " cumulative foreign direct investment totaling more than $ 60 billion since 1989 . "
The Antall government of 1990 -- 94 began market reforms with price and trade liberation measures , a revamped tax system , and a nascent market-based banking system .
The external debt burden , one of the highest in Europe , reached 250 % of annual export earnings , while the budget and current account deficits approached 10 % of GDP .
In March 1995 , the government of Prime Minister Gyula Horn implemented an austerity program , coupled with aggressive privatization of state-owned enterprises and an export-promoting exchange raw regime , to reduce indebtedness , cut the current account deficit , and shrink public spending .
The Government of Hungary no longer requires IMF financial assistance and has repaid all of its debt to the fund .
Consequently , Hungary enjoys favorable borrowing terms .
These successes allowed the government to concentrate in 1996 and 1997 on major structural reforms such as the implementation of a fully funded pension system ( partly modelled after Chile 's pension system with major modifications ) , reform of higher education , and the creation of a national treasury .
Recently , the overriding goal of Hungarian economic policy has been to prepare the country for entry into the European Union , which it joined in late 2004 .
Prior to the change of regime in 1989 , 65 % of Hungary 's trade was with Comecon countries .
Trade with EU countries and the OECD now comprises over 70 % and 80 % of the total , respectively .
Germany is Hungary 's single most important trading partner .
Of this , about $ 6 billion came from American companies .
By 2006 Hungary 's economic outlook had deteriorated .
In 2006 Prime Minister Ferenc Gyurcsány was reelected on a platform promising economic " reform without austerity . "
In foreign investments , Hungary has seen a shift from lower-value textile and food industry to investment in luxury vehicle production , renewable energy systems , high-end tourism , and information technology .
Hungary 's low employment rate remains a key structural handicap to achieving higher living standards .
Hungary , which joined the European Union in 2004 , has been hit hard by the late-2000s recession because of its heavy dependence on foreign capital to finance its economy and has one of the biggest public deficits in the EU .
According to the conservative think tank Heritage Foundation , Hungary 's economy was 67.2 percent " free " in 2008 , which makes it the world 's 43rd-freest economy .
General government net lending was 9.2 % in 2006 , instead of estimated 10.1 % ( but still the largest in Europe ) because of the austerity program of the government , and was 5.5 % in 2007 , and recent estimates of the government says 4 % in 2008 .
The 2008 financial crisis hit Hungary mainly in October 2008 .
Slightly more than one half of Hungary 's landscape consists of flat to rolling plains of the Pannonian Basin : the most important plain regions include the Little Hungarian Plain in the west , and the Great Hungarian Plain in the southeast .
Transdanubia is a primarily hilly region with a terrain varied by low mountains .
Hungary is divided in two by its main waterway , the Danube ; other large rivers include the Tisza and Dráva , while Transdanubia contains Lake Balaton , a major body of water .
The second largest lake in the Pannonian Basin is the artificial Lake Tisza ( Tisza-tó ) .
According to the WWF , the territory of Hungary belongs to the ecoregion of Pannonian mixed forests .
Hungary has 10 national parks , 145 minor nature reserves and 35 landscape protection areas .
Hungary has a continental climate , with hot summers with low overall humidity levels but frequent rainshowers and mildly cold snowy winters .
The Military of Hungary , or " Hungarian Armed Forces " currently has two branches , the " Hungarian Ground Force " and the " Hungarian Air Force " .
The term Honvédség is the name of the military of Hungary since 1848 referring to its purpose ( véd in Honvéd ) of defending the country .
The Hungarian Army is called Magyar Honvédség .
The rank equal to a Private is a Honvéd .
The Hungarian Air Force is the air force branch of the Hungarian Army .
Hussar : A type of irregular light horsemen was already well established by the 15th century in medieval Hungary .
By the 17th century , Hungary had once again become predominantly Catholic .
Hungary has been the home of a sizable Armenian community as well .
They still worship according to the Armenian Rite , but they have reunited with the Catholic Church ( Armenian Catholics ) under the primacy of the Pope .
According to the same pattern , a significant number of Orthodox Christians became re-united with the rest of the Catholic world .
Faith Church accepts the results and spiritual , moral values of both early Christianity and the Reformation , as well as other revival movements serving the progress of the Christian faith .
Hungary has historically been home to a significant Jewish community , especially since the 19th century when many Jews , persecuted in Russia , found refuge in the Kingdom of Hungary .
The largest synagogue in Europe is located in Budapest .
The census of January 1941 found that 6.2 % of the population , i.e. 846,000 people , were considered Jewish according to the racial laws of that time .
From this number , 725,000 were Jewish by religion .
Hungarian traditional music tends to have a strong dactylic rhythm , as the language is invariably stressed on the first syllable of each word .
Hungary also has a number of internationally renowned composers of contemporary classical music , György Ligeti , György Kurtág , Péter Eötvös and Zoltán Jeney among them .
One of the greatest Hungarian composers , Béla Bartók was also among the most significant musicians of the 20th century .
His music was invigorated by the themes , modes , and rhythmic patterns of the Hungarian and neighboring folk music traditions he studied , which he synthesized with influences from his contemporaries into his own distinctive style .
Hungary has made many contributions to the fields of folk , popular and classical music .
It is also strong in the Szabolcs-Szatmár area and in the southwest part of Transdanubia , near the border with Croatia .
Hungarian classical music has long been an " experiment , made from Hungarian antedecents and on Hungarian soil , to create a conscious musical culture [ using the ] musical world of the folk song " .
For example , Béla Bartók and Zoltán Kodály , two of Hungary 's best known composers , used folk themes in their music .
Older veteran underground bands such as Beatrice from the 1980s also remain popular .
In the earliest times Hungarian language was written in a runic-like script ( although it was not used for literature purposes in the modern interpretation ) .
Both are in Latin .
Renaissance literature flourished under the reign of King Matthias ( 1458 -- 1490 ) .
The most important poets of the period was Bálint Balassi ( 1554 -- 1594 ) and Miklós Zrínyi ( 1620 -- 1664 ) .
The first enlightened writers were Maria Theresia 's bodyguards .
The greatest poets of the time were Mihály Csokonai Vitéz and Dániel Berzsenyi .
The greatest figure of the language reform was Ferenc Kazinczy .
The Hungarian language became feasible for all type of scientific explanations from this time , and furthermore many new words were coined for describing new inventions .
Hungarian literature has recently gained some renown outside the borders of Hungary .
Some modern Hungarian authors have become increasingly popular in Germany and Italy especially Sándor Márai , Péter Esterházy , Péter Nádas and Imre Kertész .
Other well-known Hungarian authors are Ferenc Móra , Géza Gárdonyi , Zsigmond Móricz , Gyula Illyés , Albert Wass and Magda Szabó .
The csárda is the most distinctive type of Hungarian inn , an old-style tavern offering traditional cuisine and beverages .
Pálinka : is a fruit brandy , distilled from fruit grown in the orchards situated on the Great Hungarian Plain .
It is a spirit native to Hungary and comes in a variety of flavours including apricot ( barack ) and cherry ( cseresznye ) .
Hungarian wine regions offer a great variety of style : the main products of the country are elegant and full-bodied dry whites with good acidity , although complex sweet whites ( Tokaj ) , elegant ( Eger ) and full-bodied robust reds ( Villány and Szekszárd ) .
Hungary is a land of thermal water .
A passion for spa culture and Hungarian history have been connected from the very beginning .
Because of an advantageous geographical location thermal water can be found with good quality and in great quantities on over 80 % of Hungary 's territory .
The spa culture was revived during the Turkish Invasion who used the thermal springs of Buda for the construction of a number of bathhouses , some of which are still functioning ( Király Baths , Rudas Baths ) .
Csárdás : New style dances developed in the 18-19th centuries is the Hungarian name for the national dances , with Hungarian embroidered costumes and energetic music .
Verbunkos : a solo man 's dance evolved from the recruiting performances of the Austro-Hungarian army .
Nearly all the manifestations of folk art practiced elsewhere in Europe also flourished among the Magyar peasantry at one time or another , their ceramics and textile being the most highly developed of all .
In the Sárköz region the women 's caps show black and white designs as delicate as lace and give evidence of the people 's wonderfully subtle artistic feeling .
These vessels , made of black clay , reflect more than three hundred years of traditional Transdanubian folk patterns and shapes .
Founded in 1826 , Herend Porcelain is one of the world 's largest ceramic factories , specializing in luxury hand painted and gilded porcelain .
In the mid-19th century it was purveyor to the Habsburg Dynasty and aristocratic customers throughout Europe .
The list of events always includes renowned foreign guests as well as distinguished artists and groups from the Hungarian musical life .
Haydn at Eszterháza : During its first quarter century , the palace was the primary home of the celebrated composer Joseph Haydn , who wrote the majority of his symphonies for the Prince 's orchestra .
The basic aim of the festival is to evoke the musical paradise that Eszterháza was in Haydn 's time , within the original walls , with the help of period instruments and performing practice .
The programmes focus mainly on the works composed during the Eszterháza period of Haydn 's creative life , and among these , on compositions belonging to the most important genres ( symphonies , string quartets , keyboard sonatas and trios ) .
In addition , however , the concert programmes regularly include works by the " unknown Haydn " ( baryton pieces , rarely heard church compositions , wind divertimenti , etc. ) .
The festival aims to provide opportunities for the world 's most outstanding Haydn performers to meet here , to gain inspiration from the atmosphere and acoustics of the place , and to inspire one another through shared music-making .
At the all time total medal count for Olympic Games , Hungary reaches the 9th rank out of 211 participating nations , with a total of 465 medals .
Some of the world 's best sabre fencing athletes have historically hailed from Hungary .
in 2009 the Hungarian national ice hockey team has qualified for their first IIHF World Championship in more than seventy years .
The Hungarian national football team represents Hungary in international football and is controlled by the Hungarian Football Federation .
Hungarian football is known for one of the most formidable and influential sides in football history , which revolutionized the play of the game .
lit Golden Team ) of the " Magnificent Magyars " , captivated the football world with an exciting brand of play drawn from new tactical nuances and amassed , barring the 1954 World Cup Final , a remarkable record of 43 victories , 7 ties , and no defeats from the 15th of June 1952 to the end of its historic unbeaten run on February 18 , 1956 .
Hungary posted the highest ever Elo football rating of 2173 points in June ( 1954 ) along with the second highest with 2153 ( 1956 ) ; surpassing that of Brazil , England , Argentina and Germany in all-time competition .
The footballer Ferenc Puskás ( 1927 -- 2006 ) scored 84 goals in 85 internationals for Hungary , and 511 goals in 533 matches in the Hungarian and Spanish leagues .
With the Hungarian Golden Team , Puskás played the 1954 World Cup final against West Germany .
In 1958 , after the Hungarian Revolution , he emigrated to Spain where he played in the legendary Real Madrid team that also included Alfredo Di Stéfano , and Francisco Gento .
Hungary has won gold at the Olympic three times , in 1952 , 1964 , and 1968 .
Since the 1976 reshuffle by UEFA , the under-23s are now classified with the under-21s .
Hungary holds the longest consecutive run of matches unbeaten with 33 international games between 14 May 1950 and 4 July 1954 , when they lost the World Cup final to Germany .
Argentina and Spain jointly hold the second longest string of 31 unbeaten matches .
The 1986 World Cup is seen by many fans as the final confirmation of Hungary 's decline .
Since then , Hungary has continued to produce fine individual talent-notably Béla Illés and Krisztián Lisztes -- but further success as a team has eluded them .
Today , Hungary are a lesser force and have n't qualified for a World Cup since 1986 , or for the European Championship finals since 1972 .
Republic of Hungary is located in the central part of the Pannonian Vale , and is a landlocked country , which is conducive to the development of all forms of traffic .
Hungary has developed road , railway , air and water traffic .
Budapest , the capital of the state , to the measures is an important node in the public transport network , to say that " all roads lead to Budapest " .
In Budapest , the three main railway stations are the Eastern , Western ( Nyugati ) and Southern , with other outlying stations like Kelenföld .
Other important railway stations countrywide include Szolnok ( the most important railway intersection outside Budapest ) , Tiszai Railway Station in Miskolc and the stations of Pécs , Győr , Szeged and Székesfehérvár .
Four Hungarian cities ( Budapest , Debrecen , Miskolc and Szeged ) have tram network .
The only city with an underground railway system is Budapest with its Metro .
In Budapest there is also a suburban rail service in and around the city , operated under the name HÉV .
New motorway sections are being added to the existing network , that already connects many major economically important cities to the Capital City .
The most important port is Budapest , the capital .
Other important ones include Dunaújváros and Baja .
The Budapest Metro is the metro system in the Hungarian capital Budapest .
The Budapest Metro trains start running at 4:30 in the morning , and the last train leaves at 11:10 p.m. from the terminus .
