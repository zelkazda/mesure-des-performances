The 2510 km ( 1560 mi ) river rises in the western Himalayas in the Uttarakhand .
The Ganges Basin drains 1000000-square-kilometre ( 390000 sq mi ) and supports one of the world 's highest density of humans .
The river has been declared as India 's National River .
Although many small streams comprise the headwaters of the Ganges , the six longest headstreams and their five confluences are given both cultural and geographical emphasis ( see the map showing the headwaters of the river ) .
The Bhagirathi is the source stream ; it rises at the foot of Gangotri Glacier , at Gaumukh , at an elevation of 3892 m ( 12769 ft ) .
The headwaters of the Alaknanda are formed by snowmelt from such peaks as Nanda Devi , Trisul , and Kamet .
After flowing 200 km through its narrow Himalayan valley , the Ganges debouches on the Gangetic Plain at the pilgrimage town of Haridwar .
There , a dam diverts some of its waters into the Ganges Canal , which irrigates the Doab region of Uttar Pradesh .
Further , the river follows an 800 km curving course passing through the city of Kanpur before being joined from the southwest by the Yamuna at Allahabad .
This point is known as the Sangam at Allahabad .
At Bhagalpur , the river meanders past the Rajmahal Hills , and begins to run south .
At Pakur , the river begins its attrition with the branching away of its first distributary , the Bhāgirathi-Hooghly , which goes on to form the Hooghly River .
Near the border with Bangladesh the Farakka Barrage , built in 1974 , controls the flow of the Ganges , diverting some of the water into a feeder canal linking the Hooghly to keep it relatively silt-free .
After entering Bangladesh , the main branch of the Ganges is known as the Padma River until it is joined by the Jamuna River , the largest distributary of the Brahmaputra .
Only two rivers , the Amazon and the Congo , have greater discharge than the combined flow of the Ganges , the Brahmaputra and the Surma-Meghna river system .
George Harrison of The Beatles had his ashes scattered in the Ganga by his family in a private ceremony soon after his death .
Varanasi has hundreds of temples along the banks of the Ganga which often become flooded during the rains .
There are two major dams on the Ganges .
This caused severe deterioration to the water flow in the Ganges , and is a major cause for the decay of Ganges as an inland waterway .
The other dam is a serious hydroelectric affair at Farakka , close to the point where the main flow of the river enters Bangladesh , and the tributary Hooghly ( also known as Bhagirathi ) continues in West Bengal past Calcutta .
Bangladesh feels that the lack of flow in the summer months causes sedimentation and makes Bangladesh more prone to flood damages .
At the same time , proposals for linking the Brahmaputra to the Ganges to improve the water flow in the Ganges is hanging fire .
Now only smaller ocean traffic can make it through the Hooghly to Calcutta , beyond which the silting prevents all deep-draft vessels .
There is also a controversial dam at Tehri , on the Bhagirathi , one of the main source rivers of Ganges .
The upper and lower Ganges canal , which is actually the backbone of a network of canals , runs from Haridwar to Allahabad , but maintenance has not been very good .
The project is located on the Bhagirathi tributory in Uttarkashi district of Uttarakhand state .
This is 1st project in downstream from origin of Ganges at Gangotri .
He did so several times in his work Indica : " India , again , possesses many rivers both large and navigable , which , having their sources in the mountains which stretch along the northern frontier , traverse the level country , and not a few of these , after uniting with each other , fall into the river called the Ganges .
Now this river , which at its source is 30 stadia broad , flows from north to south , and empties its waters into the ocean forming the eastern boundary of the Gangaridai , a nation which possesses a vast force of the largest-sized elephants . "
In Rome 's Piazza Navona , a famous sculpture , Fontana dei Quattro Fiumi ( fountain of the four rivers ) designed by Gian Lorenzo Bernini was built in 1651 .
It symbolizes four of the world 's great rivers ( the Ganges , the Nile , the Danube , and the Río de la Plata ) , representing the four continents known at the time .
The Ganges Basin with its fertile soil is instrumental to the agricultural economies of India and Bangladesh .
The Ganges and its tributaries provide a perennial source of irrigation to a large area .
The rapids of the Ganges also are popular for river rafting , attracting hundreds of adventure seekers in the summer months .
The Ganges river has been considered one of the dirtiest rivers in the world .
The extreme pollution of the Ganges affects 400 million people who live close to the river .
Gangotri and Uttarkashi are good examples too .
The Ganges river 's long-held reputation as a purifying river appears to have a basis in science .
The Tibetan Plateau contains the world 's third-largest store of ice .
Around Varanasi the river once had an average depth of 60 metres ( 197 ft ) , but in some places it is now only 10 metres ( 33 ft ) .
