Chievo slipped into the relegation zone on the final match day of 2006 -- 2007 and was demoted to Serie B , but subsequently rebounded to clinch promotion back to the top-flight after one year in the cadetteria .
The team was founded in 1929 by a small number of football fans from the small borough of Chievo , a Verona neighbourhood .
In 1959 , after the restructuring of the football leagues , Chievo was admitted to play the " Seconda Categoria " , a regional league placed next-to-last in the Italian football pyramid .
As a consequence of promotion , Chievo was forced to move to the Stadio Marcantonio Bentegodi , the main venue in Verona ; another promotion , to Serie C1 , followed in 1989 .
In 1990 , the team changed its name to its current one , " A.C. ChievoVerona " .
The club finally ended the season with a highly respectable fifth place finish , qualifying the team to play in the UEFA Cup .
The 2004/2005 season is remembered as one of the toughest ever in Chievo 's history .
In 2005/2006 , Giuseppe Pillon of Treviso FBC was appointed as new coach .
Juventus , AC Milan and Fiorentina , who had all originally qualified for the 2006 -- 07 Champions League , and Lazio , who had originally qualified for the 2006 -- 07 UEFA Cup , were all banned from UEFA competition for the 2006/07 season , although AC Milan were allowed to enter the Champions League after their appeal to FIGC .
Chievo took up a place in the third qualifying stage of the competition along with AC Milan and faced Bulgarian side Levski Sofia .
Chievo lost the first leg 2 -- 0 in Sofia and managed a 2 -- 2 home draw on the second leg and were eliminated by a 4 -- 2 aggregate score with Levski advancing to the Champions League group stage .
As a Champions League third round qualifying loser , Chievo was given a place in the UEFA Cup final qualifying round .
On August 25 , 2006 Chievo was drawn to face Portuguese Braga .
The return match , played on September 28th in Verona , although won by Chievo 2 -- 1 resulted in a 3 -- 2 aggregate loss and the club 's elimination from the competition .
The difference between the clubs is high-lighted during local derby games played at the clubs ' shared stadium when , for Chievo 's " home " fixtures , the Chievo fans are located in away end of the stadium .
Chievo bounced back quickly from the disappointment of their relegation on the last matchday of 2006/07 , going in search of an immediate promotion back to the top flight .
Giuseppe Iachini replaced him and the captain , Lorenzo D'Anna , gave way to Sergio Pellissier at the end of the transfer window .
A new squad was constructed , most notably including the arrivals of mid-fielders Maurizio Ciaramitaro and Simone Bentivoglio , defender Cesar and forward Antimo Iunco .
A largely unchanged lineup earned safety the following season with 4 matchdays to spare , and Chievo will be part of the inaugural Lega Calcio Serie A in 2010 -- 11 , their third consecutive season ( and ninth season in the last ten years ) in the top flight of Italian football .
The club 's historic nickname is gialloblu ( from the club colors of yellow and blue ) although throughout Italian football the team recognised by most fans as " Gialloblu " are the original team from Verona -- " Hellas Verona " .
However , with later successes by Chievo and contemporaneous Serie B and Serie C1 struggles for Hellas Verona , Chievo fans have now largely embraced the nickname as a badge of honour .
The current club crest represents Cangrande I della Scala , an ancient seignor from Verona .
At the end of the 2008/09 season , Chievo had an average crowd attendance of 13,352 .
