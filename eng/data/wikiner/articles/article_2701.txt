She successfully appealed for the lives of the rebels involved in the Evil May Day for the sake of their families .
Henry VIII 's move to have their 24-year marriage annulled set in motion a chain of events that led to England 's break with the Roman Catholic Church .
When Pope Clement VII refused to annul the marriage , Henry defied him by assuming supremacy over religious matters .
This allowed him to marry Anne Boleyn on the judgement of clergy in England , without reference to the Pope .
He was motivated by the hope of fathering a male heir to the Tudor dynasty .
The children of John and Katherine , while legitimized , were barred from ever inheriting the English throne .
Saint Thomas More would reflect later in her lifetime that in regards to her appearance " There were few women who could compete with the Queen in her prime . "
The couple later met on 4 November at Dogmersfield in Hampshire .
Little is known about their first impressions of each other , but Arthur did write to his parents-in-law that he would be ' a true and loving husband ' and told his parents that he was immensely happy to ' behold the face of his lovely bride ' .
Ten days later , on 14 November 1501 , they were married at St. Paul 's Cathedral .
At this point , Henry VII faced the challenge of avoiding returning her dowry to her father .
She lived as a virtual prisoner at Durham House in London .
Marriage to Arthur 's brother depended on the Pope granting a dispensation because canon law forbade men to marry their brother 's widows .
On Saturday 23 June , the traditional eve-of-coronation procession to Westminster was greeted by an extremely large and very enthusiastic crowd .
As was the custom , they spent the night before their coronation at the Tower of London .
Henry , however , still considered a male heir essential .
The Tudor dynasty was new , and its legitimacy might still be tested .
The disasters of civil war were still fresh in living memory from the Wars of the Roses .
In 1525 , Henry VIII became enamoured of Anne Boleyn , a maid-of-honour to Queen Catherine who was between 10 and 17 years younger than Henry ( Anne 's exact year of birth is unknown ) .
Henry began pursuing her .
It is possible that the idea of annulment had been suggested to Henry much earlier than this , and is highly probable that it was motivated by his desire for a son .
It soon became the one absorbing object of Henry 's desires to secure an annulment .
William Knight , the King 's secretary , was sent to Pope Clement VII to sue for an annulment , on the grounds that the dispensing bull of Pope Julius II was obtained by false pretences .
In the end , Henry 's envoy had to return without accomplishing much .
Henry now had no choice but to put his great matter into the hands of Thomas Wolsey , and Wolsey did all he could to secure a decision in Henry 's favour .
She bows low to Henry , puts herself at his mercy , states her case with irrefutable eloquence and then sweeps out of the courtroom , a woman both formidable and clearly wronged .
The Pope had no intention of allowing a decision to be reached in England , and his legate was recalled .
( How far the pope was influenced by Charles V is difficult to say , but it is clear Henry saw that the Pope was unlikely to annul his marriage to the Emperor 's aunt . )
Wolsey had failed and was dismissed from public office in 1529 .
Wolsey then began a secret plot to have Anne Boleyn forced into exile and began communicating with the Pope , to that end .
When this was discovered , Henry ordered Wolsey 's arrest and , had he not been terminally ill and died in 1530 , he might have been executed for treason .
When Archbishop of Canterbury William Warham died , the Boleyn family 's chaplain , Thomas Cranmer , was appointed to the vacant position .
He appeared in the legates ' court on her behalf , where he shocked people with the directness of his language , and by declaring that , like John the Baptist , he was ready to die on behalf of the indissolubility of marriage .
Whilst some sources claim that Anne was probably already pregnant at the time , one of the aspects that made her most attractive to Henry , was the fact that she refused to sleep with him until they were married .
On Anne 's side , she had seen what Henry had done to her sister , Mary Boleyn , and her unwillingness to be his mistress was part of her plan .
Five days later , on 28 May 1533 , Cranmer declared the marriage of Henry and Anne valid .
In 1535 she was transferred to the decaying and remote Kimbolton Castle .
While she was permitted to receive occasional visitors , she was forbidden to see her daughter , Mary .
Henry offered them both better quarters and each other 's company if they would acknowledge Anne Boleyn as his new Queen .
In late December 1535 , sensing death was near , she made her will , and wrote to her nephew , the Emperor Charles V , asking him to protect her daughter .
She died at Kimbolton Castle , on 7 January 1536 .
Certainly , later in the day it is reported that Henry and Anne both individually and privately wept for her death .
Henry did not attend the funeral and refused to allow Mary to attend either .
Her tomb in Peterborough Cathedral can be seen and there is hardly ever a time when it is not decorated with flowers or pomegranates , her heraldic symbol .
Every year at Peterborough Cathedral there is a service in her memory .
There is a statue of her in her birthplace of Alcalá de Henares , as a young woman holding a book and a rose .
However , most of the rest of the play is an attempt to absolve many , especially Henry VIII , and the timing of key incidents ( including Katherine 's death ) are changed and other events are avoided ( the play makes Henry nearly an innocent pawn in the hands of a dastard Cardinal Wolsey , and the play stops short of Anne Boleyn 's execution ) .
