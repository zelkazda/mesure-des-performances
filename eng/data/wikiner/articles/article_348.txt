He was born and raised in the Republic of Venice .
Vivaldi also had some success with stagings of his operas in Venice , Mantua and Vienna .
After meeting the Emperor Charles VI , Vivaldi moved to Vienna hoping for preferment .
The Emperor died soon after Vivaldi 's arrival , and the composer died a pauper , without a steady source of income .
Well received during his lifetime , Vivaldi 's music went into a decline until it was rediscovered in the first half of the 20th century .
Vivaldi 's music is popular with modern audiences .
Antonio Lucio Vivaldi was born in Venice , the capital of the Republic of Venice in 1678 .
In the trauma of the earthquake , Vivaldi 's mother may have dedicated him to the priesthood .
Vivaldi 's official church baptism ( the rites that remained other than the baptism itself ) did not take place until two months later .
Vivaldi 's health was problematic .
Vivaldi only said mass as a priest a few times .
While Vivaldi is most famous as a composer , he was regarded as an exceptional technical violinist as well .
There were four similar institutions in Venice ; their purpose was to give shelter and education to children who were abandoned or orphaned , or whose families could not support them .
Shortly after Vivaldi 's appointment , the orphans began to gain appreciation and esteem abroad , too .
Vivaldi wrote concertos , cantatas and sacred vocal music for them .
The position of maestro di coro , which was at one time filled by Vivaldi , required a lot of time and work .
The vote on Vivaldi was seldom unanimous , and went 7 to 6 against him in 1709 .
He was a musician himself , and Vivaldi probably met him in Venice .
In February 1711 , Vivaldi and his father traveled to Brescia , where his setting of the Stabat Mater was played as part of a religious festival .
In early 18th century Venice , opera was the most popular musical entertainment .
It proved most profitable for Vivaldi .
The following year , Vivaldi became the impresario of the Teatro Sant'Angelo in Venice , where his opera Orlando finto pazzo ( RV 727 ) was performed .
In 1715 , he presented Nerone fatto Cesare ( RV 724 , now lost ) , with music by seven different composers , of which he was the leader .
Vivaldi got the censor to accept the opera the following year , and it was a resounding success .
It was also performed in Prague in 1732 .
His progressive operatic style caused him some trouble with more conservative musicians , like Benedetto Marcello , a magistrate and amateur musician who wrote a pamphlet denouncing him and his operas .
The pamphlet , Il teatro alla moda , attacks Vivaldi without mentioning him directly .
The Marcello family claimed ownership of the Teatro Sant'Angelo , and a long legal battle had been fought with the management for its restitution , without success .
Only around 50 operas by Vivaldi have been discovered , and no other documentation of the remaining operas exists .
Vivaldi may have exaggerated , but it is possible that he did write 94 operas .
He moved there for three years and produced several operas , among which was Tito Manlio ( RV 738 ) .
In 1721 , he was in Milan , where he presented the pastoral drama La Silvia ( RV 734 , 9 arias survive ) .
In 1722 he moved to Rome , where he introduced his operas ' new style .
The new pope Benedict XIII invited Vivaldi to play for him .
In 1725 , Vivaldi returned to Venice , where he produced four operas in the same year .
The inspiration for the concertos was probably the countryside around Mantua .
They were a revolution in musical conception : in them Vivaldi represented flowing creeks , singing birds ( of different species , each specifically characterized ) , barking dogs , buzzing mosquitoes , crying shepherds , storms , drunken dancers , silent nights , hunting parties from both the hunters ' and the prey 's point of view , frozen landscapes , children ice-skating , and warming winter fires .
Each concerto is associated with a sonnet , possibly by Vivaldi , describing the scenes depicted in the music .
In 1728 , Vivaldi met the emperor while he was visiting Trieste to oversee the construction of a new port .
He gave Vivaldi the title of knight , a gold medal and an invitation to Vienna .
The printing was probably delayed , forcing Vivaldi to gather an improvised collection for the emperor .
Accompanied by his father , Vivaldi traveled to Vienna and Prague in 1730 , where his opera Farnace ( RV 711 ) was presented .
La Griselda was rewritten by the young Carlo Goldoni from an earlier libretto by Apostolo Zeno .
Like many composers of the time , the final years of Vivaldi 's life found him in financial difficulties .
His compositions were no longer held in such high esteem as they once were in Venice ; changing musical tastes quickly made them outmoded .
In response , Vivaldi chose to sell off sizeable numbers of his manuscripts at paltry prices to finance his migration to Vienna .
The reasons for Vivaldi 's departure from Venice are unclear , but it seems likely that , after the success of his meeting with Emperor Charles VI , he wished to take up the position of a composer in the imperial court .
It is also likely that Vivaldi went to Vienna to stage operas , especially since he took up residence near the Kärntnertortheater .
Shortly after Vivaldi 's arrival in Vienna , Charles VI died , a stroke of bad luck that left the composer without royal protection or a steady source of income .
Vivaldi died a pauper not long after the emperor , on the night between July 27 and 28 , 1741 , of " internal infection " , in a house owned by the widow of a Viennese saddlemaker .
He was buried next to Karlskirche , in an area now part of the site of the Technical Institute .
Only three portraits of Vivaldi are known to survive : an engraving , an ink sketch and an oil painting .
Vivaldi 's music was innovative .
Johann Sebastian Bach was deeply influenced by Vivaldi 's concertos and arias .
Bach transcribed six of Vivaldi 's concerti for solo keyboard , three for organ , and one for four harpsichords , strings , and basso continuo based upon the concerto for four violins , two violas , cello , and basso continuo ( RV 580 ) .
In the early 20th century , Fritz Kreisler 's Vivaldi-styled concerto ( which he passed off as an original Vivaldi work ) helped revive Vivaldi 's reputation .
This led to renewed interest in Vivaldi by , among others , Mario Rinaldi , Alfredo Casella , Ezra Pound , Olga Rudge , Desmond Chute , Arturo Toscanini , Arnold Schering , and Louis Kaufman .
These figures were instrumental in the Vivaldi revival of the 20th century .
Historically informed performances seem to have increased Vivaldi 's fame further .
Unlike many of his contemporaries , whose music is rarely heard outside an academic or special-interest context , Vivaldi is popular among modern audiences .
Vivaldi scholar Michael Talbot called RV 803 " arguably the best nonoperatic work from Vivaldi 's pen to come to light since ... the 1920s " .
Another film inspired by the life of the composer was in a preproduction state for several years and has the working title Vivaldi .
The music of Vivaldi , Mozart , Tchaikovsky , and Corelli , has been included in the theories of Alfred Tomatis on the effects of music on human behaviour and used in music therapy .
It is part of Il cimento dell'armonia e dell'inventione .
Vivaldi wrote more than 500 other concertos .
As well as about 46 operas , Vivaldi composed a large body of sacred choral music .
