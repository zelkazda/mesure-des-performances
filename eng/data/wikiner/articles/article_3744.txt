He also published influential biographies of William Morris ( 1955 ) and ( posthumously ) William Blake ( 1993 ) and was a prolific journalist and essayist .
He also published the novel The Sykaos Papers and a collection of poetry .
His older brother was William Frank Thompson ( 1920 -- 1944 ) , an officer in World War II who died while aiding the Bulgarian communist partisans .
In 1946 he formed the Communist Party Historians Group along with Christopher Hill , Eric Hobsbawm , Rodney Hilton , Dona Torr and others .
In 1952 this group launched the influential journal Past and Present .
Thompson 's first major work was his biography of William Morris , written while he was a member of the Communist Party .
It was also an attempt to take Morris back from the critics who for more than 50 years had emphasised his art and downplayed his politics .
But Thompson remained what he called a " socialist humanist " .
The New Reasoner combined with the Universities and Left Review to form New Left Review in 1960 , though Thompson and others fell out with the group around Perry Anderson who took over the journal in 1962 .
Thompson subsequently allied himself with the annual Socialist Register publication .
Thompson 's most influential work was and remains The Making of the English Working Class , published in 1963 while he was working at the University of Leeds .
In his preface to this book , Thompson set out his approach to writing history from below :
Thompson 's work was also significant because of the way he defined " class " .
To Thompson , class was not a structure , but a relationship :
By re-defining class as a relationship that changed over time , Thompson proceeded to demonstrate how class was worthy of historical investigation .
From 1980 , Thompson was the most prominent intellectual of the revived movement for nuclear disarmament , revered by activists throughout the world .
Thompson played a key role in both END and CND throughout the 1980s , speaking at innumerable public meetings , corresponding with hundreds of fellow activists and sympathetic intellectuals , and doing more than his fair share of committee work .
An excerpt from a speech given by Thompson featured in the computer game Deus Ex Machina ( 1984 ) .
The last book Thompson finished was Witness Against the Beast : William Blake and the Moral Law ( 1993 ) .
Thompson married fellow left-wing historian Dorothy Towers in 1948 .
Kate Thompson , the award-winning children 's writer , is their youngest child .
E. P. Thompson himself died in Worcester .
