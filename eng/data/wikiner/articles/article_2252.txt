Even worldwide celebrities might still be obscure to certain people in isolated countries like North Korea , villagers without access to international news media or people who are simply uninterested in celebrities .
Examples are appearing on the cover of Time Magazine , being spoofed in Mad Magazine , having a wax statue made of you in Madame Tussauds , receiving a star on the Hollywood Walk of Fame , and others .
Pablo Picasso 's style and name are famous even to people who are not interested in art .
Harry Houdini is the archetypal illusionist , people who do n't use computers know who Bill Gates is , the most famous scientist is Albert Einstein , Wolfgang Amadeus Mozart and Ludwig von Beethoven are the most famous classical composers and if someone has to name a famous opera singer Luciano Pavarotti might as well be the first name to come in mind .
If one has to name a famous wizard Merlin , Harry Potter or Gandalf will be first to come in mind .
Mickey Mouse is perhaps the most famous cartoon character and fictional mouse in the world .
The most famous movie monsters are King Kong and Godzilla , the archetypical detective is Sherlock Holmes and most people 's idea of a spy is James Bond .
Superman , Spider-Man , and Wonderwoman are superhero celebrities while the comic book artists and writers who created them are well known only within fandom circles .
Stan Lee , Jack Kirby , and Bob Kane are examples of figures whose celebrity status is limited to a certain genre fandom rather than the general public .
A few humanitarian leaders such as Mother Teresa have even achieved fame because of their charitable work .
In Bob Greene 's article " The new stardom that does n't require paying any dues, " he argues for " most of man 's history ... people of talent would work to create something -- something written , something painted , something sculpted , something acted out -- and it would be passed on to audiences . "
With the rise of reality TV shows , Greene points out audiences have been turned into the creators .
He argues the " alleged stars of the reality shows Survivor and Big Brother , have become famous not for doing , but merely for being . "
Greene says " You simply have to be present , in the right place at the right time . "
Greene states with reality TV , " one can become a public person just by being a person , in public . "
Hedges criticizes the " moral nihilism " inherent in celebrity culture , in which human beings become commodities while those who possess true power -- corporations and the oligarchic elite -- are veiled and rendered invisible .
Andy Warhol coined the phrase " 15 minutes of fame " .
An example of this is reality TV contestant Tiffany Pollard also known as " New York " , from VH1 's Flavor of Love .
Lorenzo de ' Medici was a famous Florentine statesman during the 15th century , but today only people who are familiar with history might know his name .
Film actors like Harold Lloyd and Louise Brooks , who were world famous in the 1920s are not as well known by the general public nowadays as they were back then .
MC Hammer is famous to people who were young in 1990 , but later generations are less familiar with his name or music .
Painter Rembrandt van Rijn and composer Wolfgang Amadeus Mozart , [ citation needed ] who were successful during their lifetimes , both died almost forgotten .
Vincent van Gogh was obscure during his lifetime and only sold one painting in his life .
Blues singer Robert Johnson only recorded a handful of songs in the 1930s and then died , only becoming well known in the vicinity of the state where he used to live .
Antonio Salieri was a famous and well known 18th century composer , who sank into obscurity the next two centuries .
He was rediscovered thanks to the musical and film Amadeus , but his fictional portrayal as an antagonist has been more famous than his music since the end of the 20th century .
Cleopatra lives in the memory of most people as a beautiful woman according to our modern tastes , while she did n't look like a modern thin photo model beauty at all .
Roscoe " Fatty " Arbuckle and O.J. Simpson are more notorious for the murder scandal in which they were involved than for their respective movie and sports careers .
Ronald Reagan is more famous as a politician today than as a movie actor .
Centuries after his death , Andrea Mantegna now better known as the mentor of Leonardo Da Vinci than for his own paintings .
Celebrities have been flocking to social networking sites such as Twitter , Facebook , and MySpace .
Social media humanizes celebrities in a way that arouses public fascination as evident by the success of magazines such as Us Weekly and People Weekly .
Tila Tequila for example , shot to stardom on MySpace .
