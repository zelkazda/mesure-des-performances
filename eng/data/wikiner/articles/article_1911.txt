Dr. Benjamin D. Santer ( June 3 , 1955 in Washington , DC , United States ) is a climate researcher at Lawrence Livermore National Laboratory .
In 1998 Santer was awarded a MacArthur Foundation grant of $ 270,000 for research supporting the finding that human activity contributes to global warming .
Similar charges were made by the Global Climate Coalition , a consortium of industry interests .
Santer and 40 other scientists responded to the Wall Street Journal that all IPCC procedural rules were followed , and that IPCC procedures required changes to the draft in response to comments from governments , individual scientists , and non-governmental organizations .
