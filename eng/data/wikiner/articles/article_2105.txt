Buses are now the main means of long distance transportation in Chile , following the decline of the rail network .
The bus system covers the whole country , from Arica to Santiago ( a 30 hour journey ) and from Santiago to Punta Arenas ( about 40 hours , with a change at Osorno ) .
Santiago began its public bus system Transantiago in 2007 .
The rail system once served the entire country , running rail lines from Arica in the north to Puerto Montt in the south .
The Ferrocarril de Antofagasta a Bolivia is a metre gauge railway in the north of the country .
The northern rail line out of Santiago is now disused past the intersection with the Valparaíso line .
The southern line runs as far as Puerto Montt and is electrified as far as the city of Temuco , from where diesel locomotives are used .
There have been repeated case studies regarding the installation of a high speed line between the cities of Valparaíso and Santiago , some even considering maglev trains , but no serious action has ever been taken on the matter .
This article incorporates public domain material from websites or documents of the CIA World Factbook .
