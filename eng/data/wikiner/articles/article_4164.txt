Empress Jitō was the 41st emperor of Japan , according to the traditional order of succession .
Empress Jitō was the daughter of Emperor Tenji .
She acceded to the throne in 687 in order to ensure the eventual succession of her son , Kusakabe-shinnō .
He eventually would become known as Emperor Mommu .
Empress Jitō reigned for eleven years .
Although there were seven other reigning empresses , their successors were most often selected from amongst the males of the paternal Imperial bloodline , which is why some conservative scholars argue that the women 's reigns were temporary and that male-only succession tradition must be maintained in the 21st century .
Empress Gemmei , who was followed on the throne by her daughter , Empress Genshō , remains the sole exception to this conventional argument .
