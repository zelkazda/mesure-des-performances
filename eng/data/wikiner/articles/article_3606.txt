The English Channel ( French : la Manche , the " sleeve " ) is an arm of the Atlantic Ocean that separates Great Britain from northern France , and joins the North Sea to the Atlantic .
It is about 560 km ( 350 mi ) long and varies in width from 240 km ( 150 mi ) at its widest , to only 34 km ( 21 mi ) in the Strait of Dover .
It is the smallest of the shallow seas around the continental shelf of Europe , covering an area of some 75000 km 2 ( 29000 sq mi ) .
The name is usually said to refer to the Channel 's sleeve shape .
During this period the North Sea and almost all of the British Isles were covered with ice .
The sea level was about 120 m lower than it is today , and the channel was an expanse of low-lying tundra , through which passed a river which drained the Rhine and Thames towards the Atlantic to the west .
As the ice sheet melted , a large freshwater lake formed in the southern part of what is now the North Sea .
As the meltwater could still not escape to the north ( as the northern North Sea was still frozen ) the outflow channel from the lake entered the Atlantic Ocean in the region of Dover and Calais .
The channel has been the key natural defence for Britain , halting invading armies while in conjunction with control of the North Sea allowing Britain to blockade the continent .
In more peaceful times the channel served as a link joining shared cultures and political structures , particularly the huge Angevin Empire from 1135 -- 1217 .
This traffic continued until the Roman departure from Britain in 410 AD , after which we encounter early Anglo-Saxons who left less clear historical records .
The attack on Lindisfarne in 793 is generally considered the beginning of the Viking Age .
According to the Anglo-Saxon Chronicle they began to settle in Britain in 851 .
They continued to settle in the British Isles and the continent until around 1050 .
Rollo had besieged Paris but in 911 entered vassalage to the king of the West Franks Charles the Simple through the Treaty of St.-Claire-sur-Epte .
In exchange for his homage and fealty , Rollo legally gained the territory he and his Viking allies had previously conquered .
With the rise of William the Conqueror the North Sea and Channel began to lose some of its importance .
During the Seven Years ' War , France attempted to launch an invasion of Britain .
However on July 25 , 1909 Louis Blériot successfully made the first Channel crossing from Calais to Dover in an airplane .
During the Second World War , naval activity in the European theatre was primarily limited to the Atlantic .
The early stages of the Battle of Britain featured air attacks on Channel shipping and ports , and until the Normandy landings with the exception of the Channel Dash the narrow waters were too dangerous for major warships .
However , despite these early successes against shipping , the Germans did not win the air supremacy necessary for a cross Channel invasion .
The town of Dieppe was the site of the ill-fated Dieppe Raid by Canadian and British armed forces .
More successful was the later Operation Overlord ( also known as D-Day ) , a massive invasion of German-occupied France by Allied troops .
The English Channel is densely populated on both shores , on which are situated a number of major ports and resorts possessing a combined population of over 3.5 million people .
The most significant towns and cities along the Channel ( each with more than 20,000 inhabitants , ranked in descending order ; populations are the urban area populations from the 1999 French census , 2001 UK census , and 2001 Jersey census ) are as follows :
The Channel , with traffic in both the UK-Europe and North Sea-Atlantic routes , is one of the world 's busiest seaways carrying over 400 ships per day .
The village of Kingsand was evacuated for 3 days because of the risk of explosion , and the ship was stranded for 11 days .
As a busy shipping lane , the English Channel experiences environmental problems following accidents involving ships with toxic cargo and oil spills .
Indeed over 40 % of the UK incidents threatening pollution occur in or very near the Channel .
The ship had been damaged and was en route to Portland when much nearer harbours were available .
Many travellers cross beneath the English Channel using the Channel Tunnel .
This engineering feat , first proposed in the early 19th century and finally realised in 1994 , connects the UK and France by rail .
It is now routine to travel between Paris or Brussels and London on the Eurostar train .
Cars can also travel on special trains between Folkestone and Calais .
The coastal resorts of the channel , such as Brighton and Deauville , inaugurated an era of aristocratic tourism in the early 19th century , which developed into the seaside tourism that has shaped resorts around the world .
However , there are also a number of minority languages that are/were found on the shores and islands of the English Channel , which are listed here , with the Channel 's name following them .
Dutch previously had a larger range , and extended into parts of the modern-day French state .
As one of the narrowest but most famous international waterways lacking dangerous currents , crossing the Channel has been the first objective of numerous innovative sea , air and human powered technologies .
It was able to make the journey across the Straits of Dover in around three hours .
The first ferry crossed under the command of Captain Hayward .
The Mountbatten class hovercraft entered commercial service in August 1968 initially operated between Dover and Boulogne , but later craft also made the Ramsgate ( Pegwell Bay ) to Calais route .
The journey time , Dover to Boulogne , was roughly 35 minutes , with six trips per day at peak times .
Clarkson believed it might be possible to break the world record for crossing the channel in this manner , but the team were unsuccessful .
