Afghanistan is a landlocked nation in south-central Asia .
The northern plains pass almost imperceptibly into the plains of Turkmenistan .
The greater part of the northern border and a small section of the border with Pakistan are marked by rivers ; the remaining boundary lines are political rather than natural .
The northern frontier extends approximately 1,689 km southwestward , from the Pamir Mountains in the northeast to a region of hills and deserts in the west , at the border with Iran .
The border with Iran runs generally southward from the Hari River across swamp and desert regions before reaching the northwestern tip of Pakistan .
Its southern section crosses the Helmand River .
Its longest border is the poorly marked Durand Line , accounting for its entire southern and eastern boundary with Pakistan .
The shortest one , bordering China 's Xinjiang province , is a mere 76 km at the end of the Wakhan Corridor , a narrow sliver of land 241 km long that extends eastward between Tajikistan and Pakistan .
The border with Pakistan runs eastward from Iran through the Chagai Hills and the southern end of the Registan Desert , then northward through mountainous country .
It then follows an irregular northeasterly course before reaching the Durand Line , established in 1893 .
This line continues on through mountainous regions to the Khyber Pass area .
Beyond this point it rises to the crest of the Hindu Kush , which it follows eastward to the Pamir Mountains .
Mostly rugged mountains -- the Hindu Kush and connected ranges ; plains in north and southwest and large areas of sandy desert near the southern border with Pakistan .
In the north east the Wakhan Corridor lies between the Hindu Kush and the Pamirs , and leads to the Wakhjir Pass into Xinjiang in China .
However , the remarkable feature of Afghan climate is its extreme range of temperature within limited periods .
In Kabul the snow lies for two or three months ; the people seldom leave their houses , and sleep close to stoves .
All over Kandahar province the summer heat is intense , and the simoom is not unknown .
At Kabul the summer sun has great power , though the heat is tempered occasionally by cool breezes from the Hindu Kush , and the nights are usually cool .
At Kandahar snow seldom falls on the plains or lower hills ; when it does , it melts at once .
Although Herat is approximately 240 m ( 787 ft ) lower than Kandahar , the summer climate there is more temperate , and the climate throughout the year is far from disagreeable .
The eastern reaches of the Hari River , including the rapids , are frozen hard in the winter , and people travel on it as on a road .
In the absence of monsoon influences there are steadier weather indications than in India .
It loses about two-thirds of its water from snow and rain annually , which flows mainly to Pakistan , Iran and Tajikistan .
The Afghan government is asking for a $ 12 billion investment from the international community to construct a number of dams for the production of electricity and to help the irrigation of the country .
The Amu Darya on the northern border , the country 's other major river , has the next largest drainage area .
The 2,661 lm long Amu Darya originates in the glaciers of the Pamir Mountains in the northeast .
During its flood period the upper course of the Amu Darya , swollen by snow and melting ice , carries along much gravel and large boulders .
With its many tributaries , the most important of which is the Arghandab River , it drains more than 298 km 2 .
Take , for example , the Safed Koh .
