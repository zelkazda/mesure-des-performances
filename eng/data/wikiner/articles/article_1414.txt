Bastille Day is the French national holiday which is celebrated on 14 July each year .
The parade opens with cadets from the École Polytechnique , Saint-Cyr , École Navale , and so forth , then other infantry troops , then motorized troops ; aviation of the Patrouille de France flies above .
Nicolas Sarkozy , elected president in 2007 , has chosen not to give it .
The President also holds a garden party at the Palais de l'Elysée .
In 2007 , President Sarkozy declined to continue the practice .
They were gradually joined by delegates of the other estates ; Louis started to recognize their validity on 27 June .
The assembly re-named itself the National Constituent Assembly on 9 July , and began to function as a legislature and to draft a constitution .
In the wake of the 11 July dismissal of Jacques Necker , the people of Paris , fearful that they and their representatives would be attacked by the royal military , and seeking to gain ammunition and gunpowder for the general populace , stormed the Bastille , a fortress-prison in Paris which had often held people jailed on the basis of lettres de cachet , arbitrary royal indictments that could not be appealed .
Besides holding a large cache of ammunition and gunpowder , the Bastille had been known for holding political prisoners whose writings had displeased the royal government , and was thus a symbol of the absolutism of the monarchy .
The storming of the Bastille was more important as a rallying point and symbolic act of rebellion than a practical act of defiance .
The Fête de la Fédération of the 14 July 1790 was a huge feast and official event to celebrate the uprising of the short-lived constitutional monarchy in France and what people considered the happy conclusion of the French Revolution .
A mass was celebrated by Talleyrand , bishop of Autun .
On 30 June 1878 , a feast had been arranged in Paris by official decision to honour the French Republic ( the event was commemorated in a painting by Claude Monet ) .
All through France , as Le Figaro wrote on the 16th , " people feasted much to honour the Bastille " .
It was the consecration of the unity of France .
The parade passes down the Champs-Elysées from the Arc de Triomphe to the Place de la Concorde where the President of the French Republic , his government and foreign ambassadors to France are standing .
Over 50 U.S. cities conduct annual celebrations
