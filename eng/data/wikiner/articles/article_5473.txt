Hamburg is the second-largest city in Germany ( second to Berlin ) and the seventh-largest city in the European Union .
The city is home to over 1.8 million people , while the Hamburg Metropolitan Region has more than 4.3 million inhabitants .
Hamburg 's official name is the Free and Hanseatic City of Hamburg .
It has become a media and industrial center , with factories such as Airbus , Blohm + Voss and Aurubis .
The radio and television broadcaster Norddeutscher Rundfunk and publishers such as Gruner + Jahr and Spiegel-Verlag represent the important media industry in Hamburg .
The city takes its name from the first permanent building on the site , a fortress ordered built by Emperor Charlemagne in 808 AD .
The castle was built on rocky ground in a marsh between the River Alster and the River Elbe as a defence against Slavic incursion .
Hamburg was destroyed and occupied several times .
In 845 , a fleet of 600 Viking ships came up the River Elbe and destroyed Hamburg , at that time a town of around 500 inhabitants .
In 1030 , the city was burned down by King Mieszko II Lambert of Poland .
Hamburg had several great fires , the most notable ones in 1284 and 1842 .
This charter , along with Hamburg 's proximity to the main trade routes of the North Sea and Baltic Sea , quickly made it a major port in Northern Europe .
It is considered the first constitution of Hamburg .
Hamburg was briefly annexed by Napoleon I to the First French Empire ( 1810 -- 14 ) .
Russian forces under General Bennigsen finally freed the city in 1814 .
Hamburg reassumed its pre-1811 status as city-state in 1814 .
The Vienna Congress of 1815 confirmed Hamburg 's independence and it became one of 39 sovereign states of the German Confederation ( 1815 -- 66 ) .
In 1860 , the state of Hamburg adopted a republican constitution .
With Albert Ballin as its director , the Hamburg-America Line became the world 's largest transatlantic shipping company at the turn of the century .
Hamburg surrendered without a fight to British Forces on May 3 , 1945 .
After World War II , Hamburg was in the British Zone of Occupation and became a state of the then still West German Federal Republic of Germany in 1949 .
On February 16 , 1962 , the North Sea flood of that year caused the Elbe to rise to an all-time high , inundating one-fifth of Hamburg and killing more than 300 people .
The inner German border -- only 50 kilometres ( 30 mi ) east of Hamburg -- separated the city from most of its hinterland and further reduced Hamburg 's global trade .
The central city area is situated around the Binnenalster and the Außenalster both of which are originally the river Alster but retained as lakes .
The island of Neuwerk and two other islands in the North Sea are also part of Hamburg , located in the Hamburg Wadden Sea National Park .
Hamburg experiences an oceanic climate .
Hamburg 's proximity to coastal areas influences the area 's climate by sending marine air masses from the Atlantic Ocean .
Hamburg has architecturally significant buildings in a wide range of styles .
On the other hand , churches like St. Nicholas 's church , the world 's tallest building in the 19th century , are important landmarks .
The skyline of Hamburg features the high spires of the principal churches St. Michaelis Church , St. Peter 's Church , St. Jacobi Church and St. Catherine 's Church covered with copper plates , and of course the Heinrich-Hertz-Turm , the once publicly accessible radio and television tower .
The many streams , rivers and canals in Hamburg are crossed by over 2300 bridges , more than Amsterdam or Venice .
Hamburg has more bridges inside its city limits than any other city in the world .
The Chilehaus , a brick stone office building built in 1922 and designed by architect Fritz Höger is spectacularly shaped like an ocean liner .
The many parks of Hamburg are distributed over the whole city , which makes Hamburg a very verdant city .
The park and its buildings were also designed by Fritz Schumacher in the 1910s .
Hamburg is made up of 7 boroughs and subdivided into 105 quarters .
As of 2008 , the areal organization is regulated by the Constitution of Hamburg and several laws .
Most of the quarters were former independent cities , towns or villages annexed into Hamburg proper .
In 1938 , the last large incorporation was done through the Greater Hamburg Act of 1937 , when the cities Altona , Harburg and Wandsbek were merged into the state of Hamburg .
The boroughs of Hamburg are not independent municipalities .
Altona is the westernmost urban borough on the right bank of the Elbe river .
Altona was an independent city until 1937 .
In 2006 Bergedorf was the largest of the seven boroughs and a quarter within this borough .
In 2006 the population of Eimsbüttel was 246,087 .
Hamburg-Mitte covers mostly the urban center of the city of Hamburg .
In 2006 , the population of Hamburg-Nord was 280,229 in an area of 57.5 square kilometres ( 22 sq mi ) .
Harburg is a borough of the city and a quarter in this borough .
The borough Harburg lies on the southern shores of the river Elbe and covers parts of the port of Hamburg , residential and rural areas and some research institutes .
In 2006 , Wandsbek was the second-largest of seven boroughs that make up the city of Hamburg .
Like the other boroughs of Hamburg , Wandsbek is divided into quarters .
Hamburg offers more than 40 theatres , 60 museums and 100 music venues and clubs .
In 2008 , the Internationales Maritimes Museum Hamburg opened in the HafenCity quarter .
The world 's largest model railway museum Miniatur Wunderland with 12 km ( 7.46 mi ) total railway length is also situated near Landungsbrücken in a former warehouse .
Hamburg State Opera is a leading opera company .
Its orchestra is the Philharmoniker Hamburg .
The city 's other well-known orchestra is the North German Radio Symphony Orchestra .
The Laeiszhalle is also home to a third orchestra , the Hamburger Symphoniker .
György Ligeti and Alfred Schnittke taught at the Hochschule für Musik und Theater Hamburg .
This density , the highest in Germany , is partly due to the major musical production company Stage Entertainment being based in the city .
Hamburg is the birthplace of Johannes Brahms , who spent his formative early years in the city .
Hamburg is home to a number of pop musicians .
The most well known among these is Nena .
Bands such as Helloween , Running Wild and Grave Digger started their careers in Hamburg .
Hamburg is also a global center for psychedelic trance music .
It is home to record labels such as Spirit Zone , mushroom magazine , the world 's best known and longest running psy-trance magazine , as well as parties and club nights .
Hamburg is home to dark electronic project , Yendri .
The Alter Botanischer Garten Hamburg is a historic botanical garden , located in the Planten un Blomen park , which now exists primarily in greenhouses .
The Botanischer Garten Hamburg is a modern botanical garden maintained by the University of Hamburg .
In 2007 , Hamburg attracted more than 3,985,105 visitors ( +3.7 % to 2006 ) with 7,402,423 overnight accommodations ( +3.1 % ) .
More than 175,000 full-time employees and a revenue of € 9.3 billion make the tourism industry a major economic factor in the Hamburg Metropolitan Region .
Hamburg has one of the fastest-growing tourism industries in Germany .
From 2001 to 2007 , the overnight stays in the city grew about 55.2 % ( Berlin +52.7 % , Mecklenburg-Western Pomerania +33 % ) .
As Hamburg is one of the world 's largest harbours many visitors take one of the harbour and/or canal boat tours which start from the Landungsbrücken .
Hamburg 's famous zoo , the Tierpark Hagenbeck , was founded in 1907 by Carl Hagenbeck as the first zoo with moated , barless enclosures .
People may visit Hamburg because of a specific interest , notably one of the musicals , a sports event , a congress or fair .
In 2005 the average visitor spent two nights in Hamburg .
From a total of 8 million overnight stays the majority of visitors came from Germany .
There are six departures planned from 2010 onwards with Queen Mary 2 from Hamburg harbour .
Hamburg is noted for several festivals and regular events .
The Hamburger DOM is a northern Germany 's biggest fun fair held three times a year .
The biker 's divine service in Saint Michael 's Church attracts tens of thousands of bikers .
Regular sports events -- some open to pro and amateur participants -- are the cycling competition Vattenfall Cyclassics , Hamburg Marathon , the biggest marathon in Germany after Berlin , the tennis tournament Hamburg Masters and equestrian events like Deutsches Derby .
Since 2007 Hamburg has a music and art festival called Dockville .
It takes place every year in summer in the district Wilhelmsburg .
Hamburg is sometimes called Germany 's capital of sport since no other city is home to more first league teams and international sports events .
Hamburger SV , one of the most successful teams in Germany , is a football team in the Bundesliga .
The HSV is the oldest team of the Bundesliga , playing in the league since its beginning in 1963 .
They play at the HSH Nordbank Arena ( average attendance in the 06/07 season was 56 100 ) .
St. Pauli 's home games take place at the Millerntor-Stadion .
The Hamburg Freezers represent Hamburg in the DEL , the highest ice hockey league in Germany .
Both teams play in the O2 World Hamburg .
Hamburg is the nation 's field hockey capital and dominates the men 's as well as the women 's Bundesliga .
There are also several minority sports clubs ; Hamburg has four cricket clubs and Hamburg is also home to one of Germany 's top lacrosse teams , the .
In 2008 the German Tennis Federation and the ATP were divided about the status of the Hamburg Masters tournament as event of the ATP Masters Series .
The Hamburg Marathon is the biggest marathon in Germany after Berlin .
The HSH Nordbank Arena ( formerly the AOL Arena and originally Volksparkstadion ) was used a site for the 2006 World Cup .
It is still in use , albeit by a minority and rarely in public , probably due to a hostile climate between World War II and the early 1980s .
The gross domestic product ( GDP ) in Hamburg is total € 88.9 billion .
The city has the highest GDP in Germany -- € 50,000 per capita -- and a relatively high employment rate , with 88 percent of the working-age population .
Although situated 68 miles ( 110 km ) up the Elbe , it is considered a sea port due to its ability to handle large ocean-going vessels .
Airbus , which has an assembly plant in Hamburg , employs over 13,000 people in the Finkenwerder quarter .
Heavy industry includes the making of steel , aluminum , copper and a number of shipyards such as Blohm + Voss .
Some of Germany 's largest publishing companies , Axel Springer AG , Gruner + Jahr , Bauer Media Group are located in the city .
Many national newspapers and magazines such as Der Spiegel and Die Zeit are produced in Hamburg , as well as some special-interest newspapers such as Financial Times Deutschland .
Hamburger Abendblatt and Hamburger Morgenpost are daily regional newspapers with having a large circulation .
Hamburg was one of the locations for the film Tomorrow Never Dies of the James Bond series .
The Reeperbahn street was location for many sets , among other the 1994 Beatles film Backbeat .
On December 31 , 2006 there were 1,754,182 registered people living in Hamburg ( up from 1,652,363 in 1990 ) in an area of 755.3 km 2 ( 291.6 sq mi ) .
The metropolitan area of the Hamburg region ( Hamburg Metropolitan Region ) is home to about 4.3 million in an area of 19000 km 2 ( 7300 sq mi ) .
There were 856,132 males and 898,050 females in Hamburg .
In 2006 there were 16,089 births in Hamburg , of which 33.1 % were given by unmarried women , 6,921 marriages and 4,583 divorces .
257,060 resident aliens were living in Hamburg ( 14.8 % of the population ) .
Hamburg is seat of one of the three bishops of the North Elbian Evangelical Lutheran Church and seat of the Roman Catholic Archdiocese of Hamburg .
There are several mosques , including the Islamic Centre Hamburg and a growing Jewish community .
Hamburg is home to 54 hospitals .
The University Medical Center Hamburg-Eppendorf with about 1300 beds is a large medical school .
On December 31 , 2007 there were about 12,600 hospital beds in Hamburg proper .
In 2006 1,061 day-care centers for children , 3,841 physicians in private practice and 462 pharmacies were counted in Hamburg .
Hamburg is a major transportation hub in Germany .
Hamburg Airport is the oldest airport in Germany still in operation .
There is also the smaller Hamburg Finkenwerder Airport , used only as a company airport for Airbus .
Some airlines market Lübeck Airport in Lübeck as serving Hamburg .
Tickets sold by one company in this Hamburger Verkehrsverbund ( Hamburg traffic group ) ( HVV ) are valid on all other HVV companies ' services .
The HVV was the first organization of this kind worldwide .
The Hamburg S-Bahn ( heavy railway system ) system comprises six lines and the Hamburg U-Bahn three lines .
Another heavy railway system , the AKN railway , connects satellite towns in Schleswig-Holstein to the city .
Except at the three bigger stations in the center of Hamburg , like Hamburg central station , Hamburg Dammtor station , or Hamburg-Altona station , the regional trains hardly stop inside the area of the city .
Hamburg has no trams or trolley-buses , but has hydrogen-fueled buses operating pilot services .
While mainly used by Hamburg citizens and dock workers , they can also be used for sightseeing tours .
Lufthansa is the homecarrier with the most flights followed from Air Berlin .
It 's about 6 miles away from the city centre and a non public airport for the Airbus Operations GmbH plant .
It 's the second biggest airbus plant after Tolouse and the third biggest aviation manufacturing plant after Seattle and Tolouse .
Vattenfall Europe owns nuclear power plants near Hamburg , Brokdorf Nuclear Power Plant , Brunsbüttel Nuclear Power Plant and Krümmel Nuclear Power Plant .
There are 33 public libraries in Hamburg proper .
17 universities are located in Hamburg .
Six universities are public , like the largest , the University of Hamburg with the University Medical Center Hamburg-Eppendorf , the University of Music and Theatre , the Hamburg University of Applied Sciences and the Hamburg University of Technology .
Seven universities are private , like the Bucerius Law School .
The city has also smaller private colleges and universities , including many religious and special-purpose institutions , such as the Helmut Schmidt University ( Former : University of the Federal Armed Forces Hamburg ) .
Hamburg has nine twin towns and sister cities around the world .
In 1994 Chicago became the newest sister city of Hamburg .
