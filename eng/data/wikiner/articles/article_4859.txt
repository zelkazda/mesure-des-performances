Often the combination of Göta älv and Klarälven ( a river ending at Vänern ) is mentioned .
The Bohus Fortress is located by the river at Kungälv .
At Trollhättan there is a dam , canal locks and a hydropower station in the river .
The artificial parts are called Trollhätte Canal .
The river and the canal is part of a mostly inland waterway , Göta Canal , all the way to Stockholm .
The power station supplied electric power to the heavy steel industry concentrated around Trollhättan Falls , contributing to its industrial revolution .
There are worries if the maximum discharge of 1000 m³/s is enough if heavy rain floods the lake Vänern which would cause big damage .
In this situation , Göta älv was allowed a discharge of 1100 m³/s for months causing big risk of landslides .
