Bergson convinced many thinkers that immediate experience and intuition are more significant than rationalism and science for understanding reality .
While at the lycée Bergson won a prize for his scientific work and another , in 1877 when he was eighteen , for the solution of a mathematical problem .
When he was nineteen , he entered the famous École Normale Supérieure .
During this period , he read Herbert Spencer .
The same year he received a teaching appointment at the lycée in Angers , the ancient capital of Anjou .
While teaching and lecturing in this part of his country ( the Auvergne region ) , Bergson found time for private study and original work .
The work was published in the same year by Félix Alcan .
( Bergson owed much to both of these teachers of the École Normale Supérieure .
There , he read Charles Darwin and gave a course on him .
Bergson had spent years of research in preparation for each of his three large works .
This essay on the meaning of comedy stemmed from a lecture which he had given in his early days in the Auvergne .
The study of it is essential to an understanding of Bergson 's views of life , and its passages dealing with the place of the artistic in life are valuable .
Following the appearance of this book , Bergson 's popularity increased enormously , not only in academic circles , but among the general reading public .
He also most certainly had read , apart from Darwin , Haeckel , from whom he retained his idea of a unity of life and of the ecological solidarity between all living beings , as well as Hugo de Vries , whom he quoted his mutation theory of evolution ( which he opposed , preferring Darwin 's gradualism ) .
Bergson served as a juror with Florence Meyer Blumenthal in awarding the Prix Blumenthal , a grant given between 1919-1954 to painters , sculptors , decorators , engravers , writers , and musicians .
Bergson quoted the first two of these articles in his 1889 work , Time and Free Will .
In the following years 1890-91 appeared the two volumes of James 's monumental work , The Principles of Psychology , in which he refers to a pathological phenomenon observed by Bergson .
Some writers , taking merely these dates into consideration and overlooking the fact that James 's investigations had been proceeding since 1870 , have mistakenly dated Bergson 's ideas as earlier than James 's .
Although James was slightly ahead in the development and enunciation of his ideas , he confessed that he was baffled by many of Bergson 's notions .
James certainly neglected many of the deeper metaphysical aspects of Bergson 's thought , which did not harmonize with his own , and are even in direct contradiction .
In addition to this , Bergson can hardly be considered a pragmatist .
Nevertheless , William James hailed Bergson as an ally .
He remarks on the encouragement he has received from Bergson 's thought , and refers to the confidence he has in being " able to lean on Bergson 's authority . "
( Also see James 's reservations about Bergson below ) .
The influence of Bergson had led James " to renounce the intellectualist method and the current notion that logic is an adequate measure of what can or can not be " .
In August 1910 James died .
In the following year the translation was completed and still greater interest in Bergson and his work was the result .
In it he expressed sympathetic appreciation of James 's work , coupled with certain important reservations .
In 1914 Bergson 's fellow-countrymen honoured him by his election as a member of the Académie française .
The Roman Catholic Church however took the step of banning Bergson 's three books , accused of pantheism by placing them upon the Index of prohibited books .
Bergson was not , however , silent during the conflict , and he gave some inspiring addresses .
Signs of Bergson 's growing interest in social ethics and in the idea of a future life of personal survival are manifested .
The volume is a most welcome production and serves to bring together what Bergson wrote on the concept of mental force , and on his view of " tension " and " detension " as applied to the relation of matter and mind .
He retained the chair , but no longer delivered lectures , his place being taken by his disciple , the mathematician and philosopher Edouard Le Roy , who supported a conventionalist stance on the foundations of mathematics , which was adopted by Bergson .
Le Roy , who also succeeded to Bergson at the Académie française and was a fervent Catholic , extended to revealed truth his conventionalism , leading him to privilege faith , heart and sentiment to dogmas , speculative theology and abstract reasonings .
Duration and simultaneity took advantage of Bergson 's experience at the League of Nations , where he presided starting in 1920 the International Commission on Intellectual Cooperation ( the ancestor of the UNESCO , which included Einstein , Marie Curie , etc. ) .
It was respectfully received by the public and the philosophical community , but all by that time realized that Bergson 's days as a philosophical luminary were past .
A Roman Catholic priest said prayers at his funeral per his request .
Bergson rejected what he saw as the overly mechanistic predominant view of causality ( as expressed in , say , finalism ) .
While Kant saw free will as something beyond time and space and therefore ultimately a matter of faith , Bergson attempted to redefine the modern conceptions of time , space , and causality in his concept of Duration , making room for a tangible marriage of free will with causality .
Seeing Duration as a mobile and fluid concept , Bergson argued that one can not understand Duration through " immobile " analysis , but only through experiential , first-person intuition .
Bergson also discussed the nature and mechanism of laughter .
Bergson considers the appearance of novelty as a result of pure undetermined creation , instead of as the predetermined result of mechanistic forces .
Intelligence , for Bergson , is a practical faculty rather than a pure speculative faculty , a product of evolution used by man to survive .
Indeed , he considers that finalism is unable to explain " duration " and the " continuous creation of life " , as it only explains life as the progressive development of an initially determined program -- a notion which remains , for example , in the expression of a " genetic program " ; such a description of finalism was adopted , for instance , by Leibniz .
Bergson regarded planning beforehand for the future as impossible , since time itself unravels unforeseen possibilities .
The foundation of Henri Bergson 's philosophy , his theory of Duration , he discovered when trying to improve the inadequacies of Herbert Spencer 's philosophy .
Kant believed that free will could only exist outside of time and space , that we could therefore not know whether or not it exists , and that it is nothing but a pragmatic faith .
Bergson responded by showing that Kant , along with many other philosophers , had confused time with its spatial representation .
In reality , Duration is unextended yet heterogeneous , and so its parts can not be juxtaposed as a succession of distinct parts , with one causing the other .
This made determinism an impossibility and freewill pure mobility , which is what Bergson identified as being the Duration .
Duration then is a unity and a multiplicity , but , being mobile , it can not be grasped through immobile concepts .
Hence one can grasp it only through Bergson 's method of intuition .
One can only grasp this through intuition ; likewise the experience of reading a line of Homer .
Élan vital ranks as Bergson 's third essential concept , after Duration and intuition .
In the idiosyncratic Laughter : An Essay on the Meaning of the Comic , Bergson develops a theory not of laughter itself , but of how laughter can be provoked .
However , Bergson warns us that laughter 's criterion of what should be laughed at is not a moral criterion and that it can in fact cause serious damage to a person 's self-esteem .
The mathematician Edouard Le Roy became Bergson 's main disciple .
Alfred North Whitehead acknowledged Bergson 's influence on his process philosophy in his 1929 Process and Reality .
Many writers of the early 20th century criticized Bergson 's intuitionism , indeterminism , psychologism and interpretation of the scientific impulse .
Still others have characterized his philosophy as a materialist emergentism -- Samuel Alexander and C. Lloyd Morgan explicitly claimed Bergson as their forebear .
Charles Sanders Peirce took strong exception to those who associated him with Bergson .
In response to a letter comparing his work with that of Bergson he wrote , " a man who seeks to further science can hardly commit a greater sin than to use the terms of his science without anxious care to use them with strict accuracy ; it is not very gratifying to my feelings to be classed along with a Bergson who seems to be doing his utmost to muddle all distinctions . "
William James 's students resisted the assimilation of his work to that of Bergson .
See , for example , Horace Kallen 's book on the subject James and Bergson .
Gide even went so far as to say that future historians will over-estimate Bergson 's influence on art and philosophy just because he was the self-appointed spokesman for " the spirit of the age " .
According to Santayana and Russell , Bergson projected false claims onto the aspirations of scientific method , claims which Bergson needed to make in order to justify his prior moral commitment to freedom .
According to Russell , Bergson uses an outmoded spatial metaphor ( " extended images " ) to describe the nature of mathematics as well as logic in general .
" Bergson only succeeds in making his theory of number possible by confusing a particular collection with the number of its terms , and this again with number in general " , writes Russell .
Furthermore , writers such as Russell , Wittgenstein , and James saw élan vital as a projection of subjectivity onto the world .
Bergson 's concepts :
