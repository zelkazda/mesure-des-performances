The Battle of Jutland was the largest naval battle of World War I , and the only full-scale clash of battleships in that war .
It was the third major fleet action between steel battleships , following the battles of the Yellow Sea and Tsushima during the Russo-Japanese War .
It was fought on 31 May -- 1 June 1916 , in the North Sea near Jutland , Denmark .
The German fleet 's intention was to lure out , trap and destroy a portion of the Grand Fleet , as the German numbers were insufficient to engage the entire British fleet at one time .
The Germans ' plan was to use Vice-Admiral Franz Hipper 's fast scouting group of five modern battlecruisers to lure Vice-Admiral Sir David Beatty 's battlecruiser squadrons through a submarine picket line and into the path of the main German fleet .
On the afternoon of 31 May , Beatty encountered Hipper 's battlecruiser force long before the Germans had expected , which eliminated any submarine influence .
After sunset , and throughout the night , Jellicoe manoeuvred to cut the Germans off from their base , in hopes of continuing the battle next morning .
But , under cover of darkness , Scheer crossed the wake of the British fleet and returned to port .
But Scheer 's plan of destroying a substantial portion of the British fleet also failed .
Subsequent reviews commissioned by the Royal Navy generated strong disagreement between supporters of Jellicoe and Beatty , and the two admirals ' performance in the battle ; this debate continues today .
With 16 dreadnought class battleships , compared with the Royal Navy 's 28 , the German High Seas Fleet stood little chance of winning a head-to-head clash .
According to Scheer , the German naval strategy should be :
On 25 April , a decision was made by the German admiralty to halt indiscriminate attacks by submarine on merchant shipping .
Scheer believed that it would not be possible to continue attacks on these terms , which took away the advantage of secret approach by submarines and left them vulnerable to even relatively small guns on the target ships .
The hope was that Scheer would thus be able to ambush a section of the British fleet and destroy it .
During the initial North sea patrol the ships were instructed to sail only north-south so that any enemy who chanced to encounter one would believe they were departing or returning from operations on the west coast .
On 22 May it was discovered that Seydlitz was still not watertight after repairs and would not now be ready until the 29th .
It had intercepted and decrypted a German signal on 28 May ordering all ships to be ready for sea on the 30th .
At 11:00 on 30 May Jellicoe was warned that the German fleet seemed prepared to sail the following morning .
A position further west was unnecessary as that area of the North Sea could be patrolled by air using blimps and scouting aircraft .
Hipper 's raiding force did not leave the Outer Jade Roads until 01:00 on 31 May heading west of Heligoland island following a cleared channel through the minefields , heading north at 16 knots .
Beatty 's faster force of six ships of the 1st and 4th battlecruiser squadrons plus the 5th battle squadron of four fast battleships left the Firth of Forth on the next day [ citation needed ] , and Jellicoe 's intention was to rendezvous with him 90 miles ( 170 km ) west of the mouth of the Skagerrak off the coast of Jutland and wait for the Germans or for their intentions to become clear .
The planned position gave him the widest range of responses to likely German intentions .
Jellicoe was to achieve this twice in one hour against the High Seas Fleet at Jutland , but on both occasions Scheer managed to turn away and disengage , thereby avoiding a decisive action .
British battlecruisers sacrificed weight of armour for greater speed , while their German counterparts were armed with lighter guns .
Admiral Fisher , responsible for reconstruction of the British fleet in the pre-war period , favoured large guns and speed .
Admiral Tirpitz , responsible for the German fleet , favoured unsinkable ships and chose to sacrifice some gun size for improved armour .
Jellicoe 's Grand Fleet was split into two sections .
Accompanying them were eight armoured cruisers ( classified by the Royal Navy since 1913 as " cruisers " ) , twelve light cruisers , fifty-one destroyers , and a minelayer .
The German High Seas Fleet under Scheer was also split into a main force and a separate reconnaissance force .
The German scouting force , commanded by Franz Hipper , consisted of five battlecruisers , five light cruisers and thirty torpedo-boats .
At 03:40 she sighted the cruisers Galatea and Phaeton leaving the Forth at eighteen knots .
At 05:00 she had to crash dive when the cruiser Duke of Edinburgh appeared from the mist heading towards her .
This was followed by another cruiser Boadicea , and then eight battleships .
Jellicoe 's ships proceeded to their rendezvous undamaged and undiscovered .
They had replied that it was currently transmitting from Wilhelmshaven .
It was known to the intelligence staff that Scheer deliberately used a different call sign when at sea , but no one asked for this information or explained the reason behind the query , to locate the German fleet .
By around 14:00 Beatty 's ships were proceeding westward at roughly the same latitude as Hipper 's squadron , which was heading north .
Beatty 's ships were divided into three columns , with the two battlecruiser squadrons leading in parallel lines three miles apart .
Engadine 's plane did locate and report some German light cruisers just before 15:30 , and received antiaircraft gunfire , but attempts to relay the plane 's reports failed .
Beatty 's standing instructions expected his officers to use initiative and keep position with the flagship .
As a result , the four Queen Elizabeth class battleships , which were the fastest and most heavily-armed in the world at that time , remained on the previous course for several minutes , ending up ten miles ( 16 km ) behind rather than five .
Beatty also had opportunity during the previous hours to concentrate his forces , and no reason not to do so , whereas he steamed ahead at full speed faster than the battleships could manage .
With visibility favouring the Germans , at 15:22 Hipper 's battlecruisers , steaming approximately northwest , sighted Beatty 's squadron at a range of about 15 miles ( 28 km ) , while Beatty 's forces did not identify Hipper 's battlecruisers until 15:30 .
At 15:45 Hipper turned southeast to lead Beatty towards Scheer , who was 46 miles ( 85 km ) southeast with the main force of the High Seas Fleet .
Beatty was to windward of Hipper , and therefore funnel and gun smoke from his own ships tended to obscure his targets , while Hipper 's smoke blew clear .
The Germans drew first blood .
Indefatigable was not so lucky ; at 16:02 , just 14 minutes into the slugging match , she was smashed aft by three 280 mm ( 11-inch ) shells from Von der Tann , causing damage sufficient to knock her out of line and detonating " X " magazine aft .
Soon after , despite the near-maximum range , Von der Tann put another 280 mm ( 11-inch ) salvo on Indefatigable 's " A " turret forward .
The plunging shells probably pierced the thin upper armour and seconds later Indefatigable was ripped apart by another magazine explosion , sinking immediately with her crew of 1,019 officers and men , leaving only two survivors .
But he knew his baiting mission was close to completion as his force was rapidly closing with Scheer 's main body .
At 16:25 the battlecruiser action intensified again when Queen Mary was hit by what may have been a combined salvo from Derfflinger and Seydlitz ; she disintegrated when both forward magazines exploded , sinking with all but nine of her 1,275 man crew lost .
( In popular legend , Beatty also immediately ordered his ships to " turn two points to port " , i.e. two points nearer the enemy , but there is no official record of any such command or course change . )
This was the first news that Beatty and Jellicoe had that Scheer and his battlefleet were even at sea .
Though taking on water , Seydlitz maintained speed .
As soon as he himself sighted the vanguard of Scheer 's distant battleship line 12 miles ( 22 km ) away , at 16:40 , Beatty turned his battlecruiser force 180 degrees , heading north to draw the Germans towards Jellicoe .
At 16:48 , at extreme range , Scheer 's leading battleships opened fire .
The order to turn in succession would have resulted in all four ships turning in the same patch of sea as they reached it one by one , giving the High Seas Fleet repeated opportunity with ample time to find the proper range .
In the event , the captain of the trailing ship ( Malaya ) turned early , mitigating the adverse results .
Since visibility and firepower now favoured the Germans , there was no incentive for Beatty to risk further battlecruiser losses when his own gunnery could not be effective : illustrating the imbalance , Beatty 's battlecruisers did not score any hits on the Germans in this phase until 17:45 , but they had rapidly received five more before he opened the range .
Only Valiant was unscathed .
The four super-dreadnoughts were far better suited to take this sort of pounding than the battlecruisers , and none were lost , though Malaya suffered heavy damage , an ammunition fire , and heavy crew casualties .
Jellicoe was now aware that full fleet engagement was nearing , but had insufficient information on the position and course of the Germans .
Hood 's flagship Invincible disabled the light cruiser Wiesbaden shortly after 17:56 .
A chaotic destroyer action in mist and smoke ensued as German torpedo-boats attempted to blunt the arrival of this new formation , but Hood 's battlecruisers dodged all the torpedoes fired at them .
In the meantime Beatty and Evan-Thomas had resumed their engagement with Hipper 's battlecruisers , this time with the visual conditions to their advantage .
Jellicoe twice demanded the latest position of the German battlefleet from Beatty , who could not see the German battleships and failed to respond to the question until 18:14 .
Meanwhile Jellicoe received confusing sighting reports of varying accuracy and limited usefulness from light cruisers and battleships on the starboard ( southern ) flank of his force .
Jellicoe was in a worrying position .
He needed to know the location of the German fleet to judge when and how to deploy his battleships from their cruising formation ( six columns of four ships each ) into a single battle-line .
The deployment could be on either the westernmost or the easternmost column , and had to be carried out before the Germans arrived ; but early deployment could mean losing any chance of a decisive encounter .
In one of the most critical and difficult tactical command decisions of the entire war , Jellicoe ordered deployment to the east at 18:15 .
Meanwhile Hipper had rejoined Scheer , and the combined High Seas Fleet was heading north , directly toward Jellicoe .
Scheer had no indication that Jellicoe was at sea , let alone that he was bearing down from the northwest , and was distracted by the intervention of Hood 's ships to his north and east .
Warspite was brought back under control and survived the onslaught , but was badly damaged , had to reduce speed , and withdrew northward ; later ( at 21:07 ) , she was ordered back to port by Evan-Thomas .
Warspite went on to a long and illustrious career , serving also in World War II .
The officers on the lead German battleships , and Scheer himself , were taken completely by surprise when they emerged from drifting clouds of smoky mist to suddenly find themselves facing the massed firepower of the entire Grand Fleet main battle line , which they did not know was even at sea .
The Germans were hampered by poor visibility , in addition to being in an unfavourable tactical position , just as Jellicoe had intended .
Realizing he was heading into a death trap , Scheer ordered his fleet to turn and flee at 18:33 .
Under a pall of smoke and mist , Scheer 's forces succeeded in disengaging by an expertly executed 180-degree turn in unison ( " battle about turn to starboard " ) , which was a well-practiced emergency manoeuvre of the High Seas Fleet .
Starting at 18:40 , battleships at the rear of Jellicoe 's line were in fact sighting and avoiding torpedoes , and at 18:54 Marlborough was hit by a torpedo , which reduced her speed to 16 knots ( 19 km/h ) .
Meanwhile , Scheer , knowing that it was not yet dark enough to escape and that his fleet would suffer terribly in a stern chase , doubled back to the east at 18:55 .
But the turn to the east took his ships , again , directly towards Jellicoe 's fully-deployed battle line .
By 19:15 , Jellicoe had crossed Scheer 's " T " again .
At 19:17 , for the second time in less than an hour , Scheer turned his outnumbered and outgunned fleet to the west using the " battle about turn " , but this time it was executed only with difficulty as the High Seas Fleet 's lead squadrons began to lose formation under concentrated gunfire .
Hipper was still aboard the destroyer G39 and was unable to command his squadron for this attack .
Derfflinger had two main gun turrets destroyed , with most of their crews killed , but survived the pounding and veered away with the other battlecruisers once Scheer was out of trouble and the German destroyers were moving in to attack .
In this brief but intense portion of the engagement , from about 19:05 to about 19:30 , the Germans sustained a total of thirty-seven heavy hits while inflicting only two , Derfflinger alone receiving fourteen .
While his battlecruisers drew the fire of the British fleet , Scheer slipped away , laying smoke screens .
British light forces also sank V48 , which had previously been disabled by Shark .
At 21:00 , Jellicoe , conscious of the Grand Fleet 's deficiencies in night-fighting , decided to try to avoid a major engagement until early dawn .
He placed a screen of cruisers and destroyers five miles ( 8 km ) behind his battle fleet to patrol the rear as he headed south to guard Scheer 's expected escape route .
In reality Scheer opted to cross Jellicoe 's wake and escape via Horns Reef .
Many of the destroyers failed to make the most of their opportunities to attack discovered ships , despite Jellicoe 's expectations that the destroyer forces would , if necessary , be able to block the path of the German fleet .
Jellicoe and his commanders did not understand that the furious gunfire and explosions to the north indicated that the German heavy ships were breaking through the screen astern of the British fleet .
Instead , it was believed that the fighting was the result of night attacks by German destroyers .
While the nature of Scheer 's escape , and Jellicoe 's inaction , indicate the overall German superiority in night-fighting , the results of the night action were no more clear-cut than were those of the battle as a whole .
At the cost of five destroyers sunk and some others damaged , they managed to torpedo the light cruiser Rostock , which sank several hours later , and the pre-dreadnought Pommern , which blew up and sank with all hands ( 839 officers and men ) at 03:10 during the last wave of attacks before dawn .
Another German cruiser , Elbing , was accidentally rammed by the dreadnought Posen and abandoned , sinking early the next day .
At 02:25 they sighted the rear of the German line .
Von der Tann sighted the torpedo and was forced to steer hard a starboard to avoid it as it passed close to her bows .
Seydlitz , critically damaged and very nearly sinking , barely survived the return voyage and after grounding and taking on even more water in the evening of 1 June , had to be assisted in to port stern first , where she dropped anchor at 07:30 on the morning of 2 June .
One message was transmitted to Jellicoe at 23:15 that accurately reported the German fleets course and speed as of 21:14 .
However the erroneous signal from earlier in the day that reported the German fleet still in port , and an intelligence signal received at 22.45 giving another unlikely position for the German fleet , had reduced his confidence in intelligence reports .
By the time Jellicoe finally learned of Scheer 's whereabouts at 04:15 , Scheer was too far away to catch and it was clear that the battle could no longer be resumed .
Several other ships were badly damaged , such as Lion and Seydlitz .
On the other hand , the British fleet remained in control of the North Sea at the end of the battle .
The High Seas Fleet survived as a fleet in being .
Most of its losses were made good within a month -- even Seydlitz , the most badly damaged ship to survive the battle , was fully repaired by October and officially back in service by November .
The Germans had failed in their objective of destroying a substantial portion of the British Fleet .
No progress had been made towards the goal of allowing the High Seas Fleet to operate in the Atlantic Ocean .
The German fleet would only sortie twice more , with a raid on 19 August and in October 1916 .
Apart from these two ( abortive ) operations the High Seas Fleet -- unwilling to risk another encounter with the British fleet -- remained inactive for the duration of the war .
Jutland thus ended the German challenge to British naval supremacy .
One month after the battle the Grand Fleet was therefore stronger than it had been before sailing to Jutland .
A third view , presented in a number of recent evaluations , is that Jutland , the last major fleet action between battleships , illustrated the irrelevance of battleship fleets following the development of the submarine , mine and torpedo .
In this view , the most important consequence of Jutland was the subsequent decision of the Germans to engage in unrestricted submarine warfare .
In the event , battleships played a relatively minor role in World War II , in which the aircraft carrier emerged as the dominant offensive weapon of naval warfare .
The issue of poorly performing shells had been known to Jellicoe , who as third sea lord from 1908 to 1910 had ordered new shells to be designed .
Hipper later commented , ' it was nothing but the poor quality of their bursting charges which saved us from disaster ' .
Efforts to replace the shells were initially resisted by the admiralty and action was not taken until Jellicoe became first sea lord in December 1916 .
Derfflinger and Seydlitz sustained 22 hits each but made it to port ( though Seydlitz just barely ) .
An impression was formed by Jellicoe , Beatty and other senior officers that the cause of the ships ' loss had been their relatively weak armour .
The design of a new battlecruiser HMS Hood , which had just started building at the time of the battle , was altered to give her 5000 tons of additional armour .
British and German propellant charges differed in packaging , handling , and chemistry .
German propellant was less vulnerable and less volatile in composition .
The Royal Navy also emphasized speed in ammunition handling over established safety protocol .
By staging charges in the chambers between the gun turret and magazine , the Royal Navy enhanced their rate of fire but left their ships vulnerable to chain reaction ammunition fires and magazine explosions .
By this time Jellicoe had been promoted to First Sea Lord , and Beatty to command of the Grand Fleet .
The Royal Navy used centralised fire control systems on their capital ships , directed from a point high up on the ship where fall of shells could best be seen , utilising a director sight for both training and elevating the guns .
This had been installed on ships progressively as the war went on because of its demonstrated advantages , and was installed on the main guns of all but two of the Grand Fleet 's capital ships .
The German battlecruisers controlled the fire of turrets using a training-only director , which also did not fire the guns at once .
The rest of the German capital ships were without this innovation .
The Royal Navy was prompted to install director firing systems to cruisers and destroyers where it had not thus far been used , and to secondary armament on battleships .
The Germans used a ' ladder system ' whereby an initial volley of three shots at different ranges was used , with the centre shot at the best guess range .
At the time , Jellicoe was criticised for his caution and for allowing Scheer to escape .
Jellicoe was promoted away from active command to become First Sea Lord , the professional head of the Royal Navy , while Beatty replaced him as commander of the Grand Fleet .
Criticism focused on Jellicoe 's decision at 19:15 .
Scheer had ordered his cruisers and destroyers forward in a torpedo attack to cover the turning away of his battleships .
Jellicoe chose to turn to the southeast , and so keep out of range of the torpedoes .
If , instead , he had turned to the west , could his ships have dodged the torpedoes and destroyed the German fleet ?
Supporters of Jellicoe , including the historian Cyril Falls , pointed to the potential folly of risking defeat in battle when you already have command of the sea .
The stakes were high , the pressure on Jellicoe immense , and his caution certainly understandable .
Although Beatty was undeniably brave , his mismanagement of the initial encounter with Hipper 's squadron and the High Seas Fleet cost considerable advantage in the first hours of the battle .
His most glaring failure was in not providing Jellicoe with periodic information on the position , course and speed of the High Seas Fleet .
Though Beatty 's larger 13.5 inch ( 343 mm ) guns outranged Hipper 's 11 inch ( 280 mm ) and 12 inch ( 305 mm ) guns by thousands of meters , Beatty held his fire for 10 minutes and closed the enemy squadron until within range of the Germans ' superior gunnery , under lighting conditions that favoured the Germans .
One ship survives and is still in commission as a Royal Naval Reserve depot in Belfast , Northern Ireland : the light cruiser HMS Caroline .
Note that due to the time zone difference , the times in some of the German accounts are two hours ahead of the times in this article .
