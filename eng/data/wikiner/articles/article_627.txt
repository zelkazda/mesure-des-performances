It was launched on November 14 , 1969 , four months after Apollo 11 .
The landing site for the mission was the Ocean of Storms .
Key objectives were achievement of a more precise landing ( which had not been achieved by Apollo 11 ) , and to visit the Surveyor 3 probe to remove parts for analysis .
Apollo 12 launched on schedule from Kennedy Space Center , during a rainstorm .
It was the first rocket launch attended by an incumbent US president , Richard Nixon .
The loss of all three fuel cells put the CSM entirely on batteries .
Bean put the fuel cells back on line , and with telemetry restored , the launch continued successfully .
Once in earth parking orbit , the crew carefully checked out their spacecraft before re-igniting the S-IVB third stage for trans-lunar injection .
After lunar module separation , the S-IVB was intended to fly into solar orbit .
The S-IVB auxiliary propulsion system was fired and the remaining propellants vented to slow it down to fly past the moon 's trailing edge ( the Apollo spacecraft always approached the moon 's leading edge ) .
It was discovered by amateur astronomer Bill Yeung who gave it the temporary designation J002E3 before it was determined to be an artificial object .
The Apollo 12 mission landed on an area of the Ocean of Storms that had been visited earlier by several unmanned missions ( Luna 5 , Surveyor 3 , and Ranger 7 ) .
The International Astronomical Union , recognizing this , christened this region Mare Cognitum .
Most of the descent was automatic , with manual control assumed by Conrad during the final few hundred feet of descent .
But the actual touchdown point -- 600 feet ( 180 m ) from Surveyor 3 -- did cause a thin film of dust to coat the probe , giving it a light tan hue .
Conrad later said he was never able to collect the money .
Unfortunately , when Bean carried the camera to the place near the lunar module where it was to be set up , he inadvertently pointed it directly into the Sun , destroying the vidicon tube .
Apollo 12 successfully landed within walking distance of the Surveyor 3 probe .
However , this finding has since been disputed : see Reports of Streptococcus mitis on the moon .
The instruments were part of the first complete nuclear-powered ALSEP station set up by astronauts on the moon to relay long-term data from the lunar surface .
The instruments on Apollo 11 were not as extensive or designed to operate long term .
The astronauts also took photographs , although by accident Bean left several rolls of exposed film on the lunar surface .
Meanwhile Gordon , on board the Yankee Clipper in lunar orbit , took multi-spectral photographs of the surface .
During splashdown , a 16 mm camera dislodged from storage and struck Bean in the forehead , rendering him briefly unconscious .
He trained with Conrad and Gordon as part of the back-up crew for what would be the Apollo 9 mission , and would have been assigned as Lunar Module pilot for Apollo 12 .
In 2009 , the Lunar Reconnaissance Orbiter photographed the Apollo 12 landing site .
Conrad , Gordon and Bean were portrayed by Paul McCrane , Tom Verica and Dave Foley , respectively .
Conrad had been portrayed by a different actor , Peter Scolari , in the first two episodes .
