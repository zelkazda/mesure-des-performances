He attended the University of Idaho .
Her father had moved to Boise , Idaho when the children were young , and later sent for his family ; he died while they were en route in 1905 .
Smith earned his master 's degree in chemistry from the George Washington University in 1917 , studying under Dr. Charles E. Munroe .
Sam Moskowitz instead gives the date of the degree as 1919 , which may reflect the differences between the thesis submission date , its defense date , and the degree certification date .
Finally , upon seeing the April 1927 issue of Amazing Stories , he submitted it to the magazine ; it was accepted , initially for $ 75 , later raised to $ 125 .
It was such a success that managing editor T. O'Conor Sloane requested a sequel before the second installment had been published .
In this novel he took pains to avoid the scientific impossibilities which had bothered some readers of the Skylark novels .
This book would be Triplanetary , " in which scientific detail would not be bothered about , and in which his imagination would run riot . "
The January 1933 issue of Astounding announced that Triplanetary would appear in the March issue , and that issue 's cover illustrated a scene from the story , but Astounding 's financial difficulties prevented the story from appearing .
Shortly after it was accepted , F. Orlin Tremaine , the new editor of the revived Astounding , offered one cent a word for Triplanetary ; when he learned that he was too late , he suggested a third Skylark novel instead .
Tremaine accepted the rough draft for $ 850 , and announced it in the June 1934 issue , with a full-page editorial and a three-quarter page advertisement .
Astounding 's circulation rose by 10,000 for the first issue , and its two main competitors , Amazing and Wonder Stories fell into financial difficulties , both skipping issues within a year .
Tremaine responded extremely positively to a brief description of the idea .
After the outline was complete , he wrote a more detailed outline of Galactic Patrol , plus a detailed graph of its structure , with " peaks of emotional intensity and the valleys of characterization and background material . "
Gray Lensman , the second book in the series , appeared in Astounding 's October 1939 through January 1940 issues .
Gray Lensman ( and its cover illustration ) was extremely well received .
Spacehounds of IPC is not a part of the series , despite occasional erroneous statements to the contrary .
Smith 's long-time friend , Dave Kyle , wrote three authorized added novels in the Lensman series that provided background about the major non-human Lensmen .
An influence that is inarguable was described in an 11 June 1947 letter to Doc from John W. Campbell ( the editor of Astounding magazine , where much of the Lensman series was originally published ) .
In your story , you reached the situation the Navy was in -- more communication channels than integration techniques to handle it .
One underlying theme of the later Lensman novels was the difficulty in maintaining military secrecy -- as advanced capabilities are revealed , the opposing side can often duplicate them .
This point was also discussed extensively by John Campbell in his letter to Doc .
The beginning of the story the Skylark of Space describes in relative detail the protagonists research into separation of platinum group residues , subsequent experiments involving electrolysis and the discovery of a process evocative of cold fusion ( over 50 years before Stanley Pons and Martin Fleischmann ) .
He describes a nuclear process yielding large amounts of energy and producing only negligible radioactive waste -- which then goes on to form the basis of the adventures in the Skylark books .
Another theme of the Skylark novels involves precursors of modern information technology .
This is itself derived from a discussion of reductionist atomic theory in the second novel , Skylark Three , which brings to mind modern quark and sub-quark theories of elementary particle physics .
Family D'Alembert
