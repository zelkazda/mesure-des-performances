Herman Charles Bosman ( February 3 , 1905 -- October 14 , 1951 ) is the South African writer widely regarded as South Africa 's greatest short story writer .
While Bosman was still young , his family moved to Johannesburg where he went to school at Jeppe High School for Boys in Kensington .
When Bosman was sixteen , he started writing short stories for the national Sunday newspaper .
He attended the University of the Witwatersrand submitting various pieces to student 's literary competitions .
During the school holidays in 1926 , he returned to visit his family in Johannesburg .
Needing a break , he then toured overseas for nine years , spending most of his time in London .
At the start of the Second World War , he returned to South Africa and worked as a journalist .
Because I know Johannesburg .
The last of these contains much new research and deals in detail with aspects of Bosman 's life and parentage that in the first were considered to be taboo .
