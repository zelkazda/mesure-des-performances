Since 2005 , he has appeared as Lloyd Mullaney in the long-running soap opera Coronation Street .
Craig Charles was born to a multiracial family in Liverpool ; his father was black and his mother was Irish .
He grew up on the Cantril Farm estate and went to school with Micky Quinn , who grew up to be a professional footballer .
Before turning to entertainment , Charles played professional football , most notably for Tranmere Rovers .
Charles acquired cult status in 1988 as the Liverpudlian slob , Dave Lister , in BBC2 's long-running sci-fi comedy television series Red Dwarf .
This was a role Charles played in all eight series until 1999 and in the three part special for television channel Dave in 2009 .
Charles ' younger brother Emile Charles guest-starred there .
Charles has appeared briefly in a number of television shows such as EastEnders , Holby City , The Bill , Lexx , The 10 Percenters , Doctors and Celebrity Weakest Link .
He was also involved in the controversial mockumentary Ghostwatch in 1992 .
Charles ' other acting work includes briefly playing the title role in the short-lived Channel 4 sitcom Captain Butler ( 1997 ) .
In 2005 , Charles joined the cast of ITV 's long-running soap opera Coronation Street , playing philandering taxicab driver Lloyd Mullaney .
Later that year , he participated in the Channel 4 reality sports show , The Games , coming fourth overall in the men 's competition .
He was briefly suspended from Coronation Street and BBC 6 Music in June 2006 whilst the production companies investigated allegations of crack cocaine usage .
In 1984 , at the age of 20 , Charles married English actress Cathy Tyson .
In 1994 , Charles and a friend were arrested and remanded in custody for several months on a rape charge .
In February 1995 , both Charles and his friend were acquitted in their trial .
Whilst in prison Charles was attacked by a man wielding a knife .
After being cleared , Charles spoke of the need to restore anonymity for those accused of rape .
In June 2006 , a photograph was printed in the Daily Mirror newspaper purporting to show Charles smoking crack cocaine in the back seat of a taxi .
These allegations resulted in Charles being suspended from Coronation Street until February 2007 , and from BBC 6 Music while an investigation was held .
In an interview , Charles blamed his relapse into drug use on the death of his father , combined with the bitterness stemming from being falsely accused of rape .
