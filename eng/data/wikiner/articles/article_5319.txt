Founded in 1848 , in Northwood , Ohio , the college moved to its present location in 1880 , where it continues to educate a student body of about 1400 traditional undergraduates in over 30 majors , as well as graduate students in a handful of master 's programs .
It is the only undergraduate institution affiliated with the Reformed Presbyterian Church of North America ( RPCNA ) .
After briefly closing during the American Civil War , the college continued operating in Northwood until 1880 .
The college constructed its current campus on land donated by the Harmony Society .
Old Main , the oldest building on campus , was completed in 1881 .
A major project to reroute Pennsylvania Route 18 , which runs through the campus , was completed in November 2007 .
Improvements to Reeves Stadium and the construction of a campus entrance and pedestrian mall were completed in time for the fall semester in 2009 .
In response , the state argued that the college 's requirement that faculty members subscribe to the Christian religion amounted to discrimination , to which the college responded that the faculty religious test constituted a bona fide occupational qualification under existing federal employment law .
Geneva offers undergraduate degree programs in the arts and sciences , such as elementary education , business , engineering , student ministry , biology , and psychology .
Geneva also offers graduates studies in several fields .
Geneva College is a member institution of the Council for Christian Colleges and Universities , Council of Independent Colleges , and National Association of Independent Colleges and Universities .
The current football coach is Geno DeMarco .
Geneva 's traditional sports rivalry is with Westminster College in nearby New Wilmington , Pennsylvania .
