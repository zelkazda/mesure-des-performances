She was the patroness of such literary figures as Wace , Benoît de Sainte-More , and Chrétien de Troyes .
Three months after her accession she married Louis VII , son and junior co-ruler of her guardian , King Louis VI .
Soon after the Crusade was over , Louis VII and Eleanor agreed to dissolve their marriage , because of Eleanor 's own desire for divorce and also because the only children they had were two daughters -- Marie and Alix .
Over the next thirteen years , she bore Henry eight children : five sons , two of whom would become king , and three daughters .
However , Henry and Eleanor eventually became estranged .
She was imprisoned between 1173 and 1189 for supporting her son Henry 's revolt against her husband , King Henry II .
Eleanor was widowed on 6 July 1189 .
Her husband was succeeded by their son , Richard the Lionheart , who immediately moved to release his mother .
Now queen mother , Eleanor acted as a regent for her son while he went off on the Third Crusade .
Eleanor survived her son Richard and lived well into the reign of her youngest son King John .
By all accounts , Eleanor 's father ensured that she had the best possible education .
Although her native tongue was Poitevin , she was taught to read and speak Latin , was well versed in music and literature , and schooled in riding , hawking , and hunting .
Eleanor was extroverted , lively , intelligent , and strong willed .
Eleanor became the heir presumptive to her father 's domains .
Later , during the first four years of Henry II 's reign , all three siblings joined Eleanor 's royal household .
In 1137 , Duke William X set out from Poitiers to Bordeaux , taking his daughters with him .
Upon reaching Bordeaux , he left Eleanor and Petronilla in the charge of the Archbishop of Bordeaux , one of the Duke 's few loyal vassals who could be entrusted with the safety of the duke 's daughters .
William requested the King to take care of both the lands and the duchess , and to also find her a suitable husband .
However , until a husband was found , the King had the legal right to Eleanor 's lands .
Despite his immense obesity and impending mortality , however , Louis the Fat remained clear-minded .
On 25 July 1137 the couple were married in the Cathedral of Saint-André in Bordeaux by the Archbishop of Bordeaux .
On 1 August , Eleanor 's father-in-law died and her husband became sole monarch .
Louis was personally involved in the assault and burning of the town of Vitry .
In response , Eleanor broke down , and meekly excused her behaviour , claiming to be bitter because of her lack of children .
In response to this , Bernard became more kindly towards her : " My child , seek those things which make for peace .
In April 1145 , Eleanor gave birth to a daughter , Marie .
She insisted on taking part in the Crusades as the feudal leader of the soldiers from her duchy .
The story that she and her ladies dressed as Amazons is disputed by serious historians , sometime confused with the account of King Conrad 's train of ladies during this campaign .
Her testimonial launch of the Second Crusade from Vézelay , the rumored location of Mary Magdalene ´s burial , dramatically emphasized the role of women in the campaign .
The Crusade itself achieved little .
Louis and Eleanor stayed in the Philopation palace , just outside the city walls .
Since he was Eleanor 's vassal , many believed that it was she who had been ultimately responsible for the change in plan , and thus the massacre .
From here the army was split by a land march with the royalty taking the sea path to Antioch .
She introduced those conventions in her own lands , on the island of Oleron in 1160 and later in England as well .
Clearly , Eleanor supported his desire to re-capture the nearby County of Edessa , the cause of the Crusade ; in addition , having been close to him in their youth , she now showed excessive affection towards her uncle -- whilst many historians today dismiss this as familial affection ( noting their early friendship , and his similarity to her father and grandfather ) , most at the time firmly believed the two to be involved in an incestuous and adulterous affair .
Failing in this attempt , they retired to Jerusalem , and then home .
Although they escaped this predicament unharmed , stormy weather served to drive Eleanor 's ship far to the south , and to similarly lose her husband .
On 21 March , the four archbishops , with the approval of Pope Eugenius , granted an annulment due to consanguinity within the fourth degree .
On 18 May 1152 ( Whit Sunday ) , six weeks after her annulment , Eleanor married Henry ' without the pomp and ceremony that befitted their rank ' .
A marriage between Henry and Eleanor 's daughter , Marie , had indeed been declared impossible for this very reason .
Over the next thirteen years , she bore Henry five sons and three daughters : William , Henry , Richard , Geoffrey , John , Matilda , Eleanor , and Joan .
Eleanor 's marriage to Henry was reputed to be tumultuous and argumentative , although sufficiently cooperative to produce at least eight pregnancies .
Henry was by no means faithful to his wife and had a reputation for philandering .
Their son , William , and Henry 's illegitimate son , Geoffrey , were born just months apart .
Henry fathered other illegitimate children throughout the marriage .
Little is known of Eleanor 's involvement in these events .
By late 1166 , and the birth of her final child , however , Henry 's notorious affair with Rosamund Clifford had become known , and her marriage to Henry appears to have become terminally strained .
Afterwards , Eleanor proceeded to gather together her movable possessions in England and transport them on several ships in December to Argentan .
At the royal court , celebrated there that Christmas , she appears to have agreed to a separation from Henry .
Of all her influence on culture , Eleanor 's time in Poitiers was perhaps the most critical and yet very little is known as to what happened .
At the time Henry went off to do his own business , most likely deciding to become invisible for a short while as the drama caused by Becket 's murder cooled-off .
We know that the court culminated in 1174 , and Eleanor was about 52 ( a woman far superior in age and status to those around her ) .
Several women , including Eleanor and her daughter Marie de Champagne , would sit and listen to the quarrels of lovers and act as a jury to the questions of the court that revolved around acts of courtly love .
In March 1173 , aggrieved at his lack of power and egged on by his father 's enemies , the younger Henry launched the Revolt of 1173 -- 1174 .
He fled to Paris .
Once her sons had left for Paris , Eleanor encouraged the lords of the south to rise up and support them .
On 8 July 1174 , Henry took ship for England from Barfleur .
He brought Eleanor on the ship .
Eleanor was imprisoned for the next sixteen years , much of the time in various locations in England .
During her imprisonment , Eleanor had become more and more distant with her sons , especially Richard ( who had always been her favorite ) .
She did not have the opportunity to see her sons very often during her imprisonment , though she was released for special occasions such as Christmas .
Henry lost his great love , Rosamund Clifford , in 1176 .
He had met her in 1166 and began the liaison in 1173 , supposedly contemplating divorce from Eleanor .
Had she done so , Henry might have appointed Eleanor abbess of Fontevrault ( Fontevraud ) , requiring her to take a vow of poverty , thereby releasing her titles and nearly half their empire to him , but Eleanor was much too wily to be provoked into this .
Nevertheless , rumours persisted , perhaps assisted by Henry 's camp , that Eleanor had poisoned Rosamund .
No one knows what Henry believed , but he did donate much money to the Godstow Nunnery in which Rosamund was buried .
In debt and refused control of Normandy , he tried to ambush his father at Limoges .
Henry 's troops besieged the town , forcing his son to flee .
When his father 's ring was sent to him , he begged that his father would show mercy to his mother , and that all his companions would plead with Henry to set her free .
Eleanor had had a dream in which she foresaw her son Henry 's death .
In 1193 she would tell Pope Celestine III that she was tortured by his memory .
For this reason Henry summoned Eleanor to Normandy in the late summer of 1183 .
She stayed in Normandy for six months .
This was the beginning of a period of greater freedom for the still supervised Eleanor .
Eleanor went back to England probably early in 1184 .
Over the next few years Eleanor often traveled with her husband and was sometimes associated with him in the government of the realm , but still had a custodian so that she was not free .
Upon Henry 's death on 6 July 1189 , Richard was his undisputed heir .
She ruled England as regent while Richard went off on the Third Crusade .
Eleanor survived Richard and lived well into the reign of her youngest son King John .
Now 77 , Eleanor set out from Poitiers .
Eleanor selected the younger daughter , Blanche .
Late in March , Eleanor and her granddaughter Blanche journeyed back across the Pyrenees .
This tragedy was too much for the elderly Queen , who was fatigued and unable to continue to Normandy .
The exhausted Eleanor went to Fontevrault , where she remained .
In early summer , Eleanor was ill and John visited her at Fontevrault .
Eleanor was again unwell in early 1201 .
When war broke out between John and Philip , Eleanor declared her support for John , and set out from Fontevrault for her capital Poitiers to prevent her grandson Arthur , John 's enemy , from taking control .
As soon as John heard of this he marched south , overcame the besiegers and captured Arthur .
Eleanor then returned to Fontevrault where she took the veil as a nun .
Eleanor died in 1204 and was entombed in Fontevraud Abbey next to her husband Henry and her son Richard .
Her tomb effigy shows her reading a Bible and is decorated with magnificent jewelry .
By the time of her death she had outlived all of her children except for King John and Queen Eleanor .
Eleanor was very beautiful : all contemporary sources agree on this point .
When she was around 30 , which would have been considered middle aged by medieval standards , Bernard de Ventadour , a noted troubadour , called her " gracious , lovely , the embodiment of charm, " extolling her " lovely eyes and noble countenance " and declaring that she was " one meet to crown the state of any king . "
However , no one left a more detailed description of Eleanor .
The 12th-century ideal of beauty was blonde hair and blue eyes ; thus many have suggested that the chroniclers would not have been so exuberant in their praises if Eleanor had not conformed to this ideal .
The mural , which was painted during Eleanor 's lifetime in a region in which she was well known and almost certainly depicts her , shows a woman with reddish-brown hair .
What is certain is that from an early age Eleanor attracted the attention of men , not only because of her looks but also because of her " welcoming " manner and inherent flirtatiousness and wit .
The Abbot Suger stops to chat with Eleanor and stays to wait , too .
The flashbacks trace the highlights of Eleanor 's life from 1137 to her death in 1204 .
Eleanor has also featured in a number of screen versions of Ivanhoe and the Robin Hood story .
She was portrayed by Lynda Bellingham in the BBC series Robin Hood .
