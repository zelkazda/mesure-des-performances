HIV infection in humans is considered pandemic by the World Health Organization .
According to current estimates , HIV is set to infect 90 million people in Africa , resulting in a minimum estimate of 18 million orphans .
For example , env codes for a protein called gp160 that is broken down by a viral enzyme to form gp120 and gp41 .
These are transported to the plasma membrane of the host cell where gp41 anchors the gp120 to the membrane of the infected cell .
For example , less than 1 % of the sexually active urban population in Africa have been tested and this proportion is even lower in rural populations .
In July 2010 , a vaginal gel containing tenofovir , a reverse transcriptase inhibitor , was shown to reduce HIV infection rates by 39 percent in a trial conducted in South Africa .
South Africa has the largest number of HIV patients in the world followed by Nigeria .
In the 35 African nations with the highest prevalence , average life expectancy is 48.3 years -- 6.5 years less than it would be without the disease .
In Africa , the number of mother-to-child-transmission ( MTCT ) cases and the prevalence of AIDS is beginning to reverse decades of steady progress in child survival .
Gallo said that it was " a disappointment " that he was not named a co-recipient .
I 'm very sorry for Robert Gallo . "
