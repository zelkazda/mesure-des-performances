The universal use of elections as a tool for selecting representatives in modern democracies is in contrast with the practice in the democratic archetype , ancient Athens .
Referenda are particularly prevalent and important in direct democracies , such as Switzerland .
The women 's suffrage movement gave women in many countries the right to vote , and securing the right to vote freely was a major goal of the American civil rights movement .
Further limits may be imposed : for example , in Kuwait , only people who have been citizens since 1920 or their descendants are allowed to vote , a condition that the majority of residents do not fulfill .
Other states ( e.g. , the United Kingdom ) only set maximum time in office , and the executive decides exactly when within that limit it will actually go to the polls .
