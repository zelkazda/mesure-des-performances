On the death of Pope Honorius II , which occurred on February 14 , 1130 , a schism broke out in the Church .
In 1139 , Bernard assisted at the Second Council of the Lateran .
Bernard denounced the teachings of Peter Abelard to the Pope , who called a council at Sens in 1141 to settle the matter .
Having previously helped end the schism within the Church , Bernard was now called upon to combat heresy .
The last years of Bernard 's life were saddened by the failure of the crusaders , the entire responsibility for which was thrown upon him .
Bernard died at age 63 , after 40 years spent in the cloister .
Bernard was the third of a family of seven children , six of whom were sons .
Bernard had a great taste for literature and devoted himself for some time to poetry .
Bernard wanted to excel in literature in order to take up the study of the Bible .
In opposition to the rational approach to divine understanding that the scholastics adopted , Bernard would preach an immediate faith , in which the intercessor was the Virgin Mary ,
Bernard was only nineteen years of age when his mother died .
The beginnings of Clairvaux Abbey were trying and painful .
Disciples flocked to it in great numbers and put themselves under the direction of Bernard .
In addition to these victories , Bernard also had his trials .
This was the occasion of the longest and most emotional of Bernard 's letters .
Though not yet 30 years old , Bernard was listened to with the greatest attention and respect , especially when he developed his thoughts upon the revival of the primitive spirit of regularity and fervour in all the monastic orders .
In the first part , he proved himself innocent of the charges of Cluny and in the second he gave his reasons for his counterattacks .
He protested his profound esteem for the Benedictines of Cluny whom he declared he loved equally as well as the other religious orders .
The zeal of Bernard extended to the bishops , the clergy , and lay people .
The bishops made Bernard secretary of the council , and charged him with drawing up the synodal statutes .
After the council , the Bishop of Verdun was deposed .
Bernard answered the letter by saying that , if he had assisted at the council , it was because he had been dragged to it by force .
In his response Bernard wrote ,
Bernard 's influence was soon felt in provincial affairs .
On the death of Pope Honorius II , which occurred on February 14 , 1130 , a schism broke out in the Church by the election of two popes , Pope Innocent II and Pope Anacletus II .
He decided in favour of Innocent II .
Towards the end of 1134 , he made a second journey into Aquitaine , where William X had relapsed into schism .
William yielded and the schism ended .
For this , he was offered , and he refused , the Archbishopric of Milan .
He then returned to Clairvaux .
In 1139 , Bernard assisted at the Second Council of the Lateran , in which the surviving adherents of the schism were definitively condemned .
Malachy would die at Clairvaux in 1148 .
The movement found an ardent and powerful advocate in Peter Abelard .
However , Abelard continued to develop his teachings , which were controversial in some quarters .
But once out of Bernard 's presence , he reneged .
Bernard then denounced Abelard to the Pope and cardinals of the Curia .
Abelard sought a debate with Bernard , but Bernard initially declined , saying he did n't feel matters of such importance should be settled by logical analyses .
Abelard continued to press for a public debate , and made his challenge widely known , making it hard for Bernard to decline .
In 1141 , at the urgings of Abelard , the archbishop of Sens called a council of bishops , where Abelard and Bernard were to put their respective cases so Abelard would have a chance to clear his name .
Bernard lobbied the prelates on the evening before the debate , swaying many of them to his view .
The next day , after Bernard made his opening statement , Abelard decided to retire without attempting to answer .
The council found in favour of Bernard and their judgment was confirmed by the Pope .
Some of these , at the command of Innocent II , took possession of Three Fountains Abbey , from which Pope Eugenius III would be chosen in 1145 .
Pope Innocent II died in the year 1143 .
Having previously helped end the schism within the Church , Bernard was now called upon to combat heresy .
He also preached against the Cathars .
The Kingdom of Jerusalem and the other Crusader states were threatened with similar disaster .
The Pope commissioned Bernard to preach a Second Crusade and granted the same indulgences for it which Pope Urban II had accorded to the First Crusade .
Bernard found it expedient to dwell upon the taking of the cross as a potent means of gaining absolution for sin and attaining grace .
On 31 March , with King Louis present , he preached to an enormous crowd in a field at Vézelay .
When Bernard was finished the crowd enlisted en masse ; they supposedly ran out of cloth to make crosses .
Bernard is said to have given his own outer garments to be cut up to make more .
Bernard wrote to the Pope a few days afterwards , " Cities and castles are now empty .
The Archbishop of Cologne and the Archbishop of Mainz were vehemently opposed to these attacks and asked Bernard to denounce them .
He then found Radulphe in Mainz and was able to silence him , returning him to his monastery .
The last years of Bernard 's life were saddened by the failure of the Second Crusade he had preached , the entire responsibility for which was thrown upon him .
When his attempt to call a new crusade failed , he tried to disassociate himself from the fiasco of the Second Crusade altogether .
The death of his contemporaries served as a warning to Bernard of his own approaching end .
From the beginning of the year 1153 , Bernard felt his death approaching .
The passing of Pope Eugenius had struck the fatal blow by taking from him one whom he considered his greatest friend and consoler .
Bernard died at age sixty-three on August 20 , 1153 , after forty years spent in the cloister .
He was buried at the Clairvaux Abbey , but after its dissolution in 1792 by the French revolutionary government , his remains were translated to the Troyes Cathedral .
The works of Bernard are as follows :
Dante 's choice appears to be based on Bernard 's contemplative mysticism , his devotion to Mary , and his reputation for eloquence .
