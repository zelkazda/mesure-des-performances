Bill Haley ( pronounced /ˈheɪliː/ ) ( July 6 , 1925 -- February 9 , 1981 ) was one of the first American rock and roll musicians .
He is credited by many with first popularizing this form of music in the early 1950s with his group Bill Haley & His Comets and their hit song " Rock Around the Clock " .
Haley 's father played the banjo , and his mother was a technically accomplished keyboardist with classical training .
Haley told the story that when he made a simulated guitar out of cardboard , his parents bought him a real one .
The anonymous sleeve notes accompanying the 1956 Decca album " Rock Around The Clock " describe Haley 's early life and career thus : " Bill got his first professional job at the age of 13 , playing and entertaining at an auction for the fee of $ 1 a night .
The sleeve notes continue : " When Bill Haley was fifteen [ c.1940 ] he left home with his guitar and very little else and set out on the hard road to fame and fortune .
In 1953 , a song called " Rock Around the Clock " was written for Haley .
When " Rock Around the Clock " appeared behind the opening credits of the 1955 film Blackboard Jungle starring Glenn Ford , it soared to the top of the American Billboard chart for eight weeks .
The single is commonly used as a convenient line of demarcation between the " rock era " and the music industry that preceded it ; Billboard separated its statistical tabulations into 1890-1954 and 1955-present .
Haley continued to score hits throughout the 1950s such as " See You Later , Alligator " and he starred in the first rock and roll musical movies Rock Around the Clock and Do n't Knock the Rock , both in 1956 .
A self-admitted alcoholic ( as indicated in a 1974 radio interview for the BBC ) , Haley fought a battle with alcohol into the 1970s .
It also reported that a doctor at the clinic where Haley had been taken said , " The tumor ca n't be operated on anymore .
Haley made a succession of bizarre , mostly monologue late-night phone calls to friends and relatives in which he seemed incoherently drunk or ill .
Haley 's first wife has been quoted as saying , " He would call and ramble and dwell on the past , his mind was really warped " .
A belligerent phone call to a business associate was taped and gives evidence of Haley 's troubled state of mind .
Haley was posthumously inducted into the Rock and Roll Hall of Fame in 1987 .
He has a star on the Hollywood Walk of Fame .
In February 2006 , the International Astronomical Union announced the naming of asteroid 79896 Billhaley to mark the 25th anniversary of Bill Haley 's death .
Married three times , Bill Haley had at least eight children .
Unlike his contemporaries , Bill Haley has rarely been portrayed on screen .
Following the success of The Buddy Holly Story in 1978 , Haley expressed interest in having his life story committed to film , but this never came to fruition .
In the 1980s and early 1990s , numerous media reports emerged that plans were underway to do a biopic based upon Haley 's life , with Beau Bridges , Jeff Bridges and John Ritter all at one point being mentioned as actors in line to play Haley .
Bill Haley has also been portrayed -- not always in a positive light -- in several " period " films :
In March 2005 , the British network Sky TV reported that Tom Hanks was planning to produce a biopic on the life of Bill Haley , with production tentatively scheduled to begin in 2006 .
However this rumor was quickly debunked by Hanks .
Bill Haley recorded prolifically during the 1940s , often at the radio stations where he worked , or in formal studio settings .
Still more demos , alternate takes , and wholly unheard-before recordings have been released since Haley 's death .
NME -- October 1955
NME -- January 1957
