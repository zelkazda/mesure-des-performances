In Israel , it is an official calendar for civil purposes and provides a framework for agriculture .
For example , until the Tannaitic period , the months were set by observation of a new crescent moon , with an additional month added every two or three years to keep Passover in the spring , again based on observation of natural events , namely the ripening of barley to reach the stage of " aviv " ( nearly ripened crop ) .
The principles and rules appear to have been settled by the time Maimonides compiled the Mishneh Torah .
Concurrently there is a weekly cycle of seven days , mirroring the seven-day period of the Book of Genesis in which the world is created .
The first month of the festival year is Nisan .
15 Nisan is the start of the festival of Pesach , corresponding to the full moon of Nisan .
Pesach is a spring festival associated with the barley harvest , so the leap-month mentioned above is intercalated periodically to keep this festival in the northern hemisphere 's spring season .
Similarly , see Genesis 1:8 , 1:13 , 1:19 , 1:23 , 1:31 and 2.2 .
The practice in the time of Gamaliel II ( c. 100 CE ) was for witnesses to select the appearance of the moon from a collection of drawings that depicted the crescent in a variety of orientations , only a few of which could be valid in any given month .
The Bible does not directly mention the addition of " embolismic " or intercalary months .
Nisan 1 is referred to as the ecclesiastical new year .
Passover begins on 14 Nisan , which corresponds to the full moon of Nisan .
As Passover is a spring festival , 14 Nisan begins on the night of a full moon after the vernal equinox .
If the barley was not ripe an intercalary month would be added before Nisan .
( see Ezekiel 40:1 , which uses the phrase " beginning of the year " . )
The practice of the Kingdom of Israel was also that of Babylon , as well as other countries of the region .
The month of Elul is the new year for counting animal tithes ( ma'aser ) .
The era year was then called " year of the captivity of Jehoiachin " .
Karaites use the lunar month and the solar year , but the Karaite calendar differs from the Rabbinic calendar in a number of ways .
For Karaites , the beginning of each month , the Rosh Chodesh , can be calculated , but is confirmed by the observation in Israel of the first sightings of the new moon .
The addition of the leap month is determined by observing in Israel the ripening of barley ( called aviv ) , rather than using the calculated and fixed calendar of Rabbinic Judaism .
Occasionally this results in Karaites being one month ahead of other Jews using the calculated Rabbinic calendar .
The " lost " month would be " picked up " in the next cycle when Karaites would observe a leap month while other Jews would not .
Furthermore , the seasonal drift of the Rabbinic calendar is avoided , resulting in the years affected by the drift starting one month earlier in the Karaite calendar .
Also , the four rules of postponement of the Rabbinic calendar are not applied , as they are not found in the Tanakh .
This affects the dates observed for all the Jewish holidays by one day .
Furthermore , the Mishnah contains laws that reflect the uncertainties of an empirical calendar .
Another Mishnah takes it for granted that it can not be known in advance whether a year 's lease is for twelve or thirteen months .
Hence it is a reasonable conclusion that the Mishnaic calendar was actually used in the Mishnaic period .
The accuracy of the Mishnah 's claim that the Mishnaic calendar was also used in the late 2nd temple period is less certain .
One scholar has noted that there are no laws from Second Temple period sources that indicate any doubts about the length of a month or of a year .
Except for the epoch year number , the calendar rules reached their current form by the beginning of the 9th century , as described by al-Khwarizmi in 823 .
In 921 , Aaron ben Meir proposed changes to the calendar .
The Talmuds do , however , indicate at least the beginnings of a transition from a purely empirical to a computed calendar .
In 1178 , Maimonides included all the rules for the calculated calendar and their scriptural basis , including the modern epochal year in his work , Mishneh Torah .
A variant of this pattern of naming includes another letter which specifies the day of the week for the first day of Pesach ( Passover ) in the year .
But by the twelfth century that source had been forgotten , causing Maimonides to speculate that there were 1080 parts in an hour because that number was evenly divisible by all numbers from 1 to 10 except 7 .
According to Maimonides , nightfall occurs when three medium-sized stars become visible after sunset .
Within the Mishnah , however , the numbering of the hours starts with the " first " hour after the start of the day .
The impact of the drift is reflected in the drift of the date of Passover from the vernal full moon :
The modern molad moments match the mean solar times of the lunar conjunction moments near the meridian of Kandahar , Afghanistan , more than 30° east of Jerusalem .
Consequently the actual lunar conjunction moments can range from 12 hours earlier than to 16 hours later than the molad moment , in terms of Jerusalem mean solar time .
If the intention of the calendar is that Passover should fall near the first full moon after the northward equinox , or that the northward equinox should land within one lunation before 16 days after the molad of Nisan , then this is still the case in about 80 % of years , but in about 20 % of years Passover is a month late by these criteria .
This discrepancy has mounted up to six days , which is why the earliest Passover ( in year 16 of the cycle ) currently falls around 27 March .
If the traditional dates of events before the Second Temple era are assumed to be using the standard Hebrew calendar , they refer to different objective years than those of the secular dates .
It is possible that the traditional dates did not use a consistent calendar matching the year count of the standard Hebrew calendar .
The latter accounts for the irregular intercalation ( adding of extra months ) that was performed in three successive years in the early second century , according to the Talmud .
The Jewish New Year is a two-day public holiday in Israel .
The disparity between the two calendars is especially noticeable with regard to commemoration of the assassinated Prime Minister Yitzchak Rabin .
Wall calendars commonly used in Israel are hybrids .
