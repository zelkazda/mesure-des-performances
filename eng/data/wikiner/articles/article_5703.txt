Hatfield is a town in Hertfordshire in the south of England in the borough of Welwyn Hatfield .
It has Saxon origins .
From the 1930s when de Havilland opened a factory until the 1990s when British Aerospace closed , Hatfield was associated with aircraft design and manufacture , which employed more people than any other industry .
Hatfield was one of the post-war New Towns built around London and has much modernist architecture from the period .
The University of Hertfordshire is based there .
The town was then called Bishop 's Hatfield .
The town grew up around the gates of Hatfield House .
It was taken over by Hawker Siddeley in 1960 and merged into British Aerospace in 1978 .
During the Second World War it produced the Mosquito fighter bomber .
After the war , facilities were expanded and it developed the Vampire , Comet , the Trident airliner , and an early bizjet , the DH125 .
BAE closed the Hatfield site in 1993 .
The land was used as a film set for Steven Spielberg 's movie Saving Private Ryan and most of the BBC / HBO television drama Band of Brothers .
Hatfield 's aerospace history is recorded in the names of local streets and pubs .
Part of the BAe land was intended to be the site of a £ 500 million new hospital to replace the Queen Elizabeth II Hospital in Welwyn GC and a new campus for Oaklands College , but both projects were canceled .
It is twinned with the Dutch port town of Zierikzee .
Hatfield is part of the Welwyn Hatfield constituency , which includes Welwyn Garden City .
The MP for Welwyn Hatfield is Grant Shapps , ( Conservative ) .
Hatfield contains numerous primary and secondary schools , including Onslow St Audrey 's School and Bishops Hatfield Girls School .
The University of Hertfordshire is based in Hatfield .
The university maintains its campus at St Albans , which houses law students .
Hatfield is 20 miles ( 32 km ) to the north of London .
The East Coast railway line from London to York runs through the town .
