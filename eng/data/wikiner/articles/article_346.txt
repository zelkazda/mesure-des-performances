One of the first to discuss the possibility of an absolute minimal temperature was Robert Boyle .
Amontons therefore argued that the zero of his thermometer would be that temperature at which the spring of the air in it was reduced to nothing .
Pierre-Simon Laplace and Antoine Lavoisier , in their 1780 treatise on heat , arrived at values ranging from 1,500 to 3,000 below the freezing-point of water , and thought that in any case it must be at least 600 below .
After J.P. Joule had determined the mechanical equivalent of heat , Lord Kelvin approached the question from an entirely different point of view , and in 1848 devised a scale of absolute temperature which was independent of the properties of any particular substance and was based solely on the fundamental laws of thermodynamics .
Max Planck 's strong form of the third law of thermodynamics states the entropy of a perfect crystal vanishes at absolute zero .
Even the less detailed Einstein model shows this curious drop in specific heats .
This state of matter was first predicted by Satyendra Nath Bose and Albert Einstein in 1924 -- 25 .
Bose first sent a paper to Einstein on the quantum statistics of light quanta ( now called photons ) .
