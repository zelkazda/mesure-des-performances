The Aegadian Islands , are a group of small mountainous islands in the Mediterranean Sea off the northwest coast of Sicily , Italy , near the city of Trapani , with a total area of 14.46 square miles ( 37.45 km 2 ) .
For administrative purposes the archipelago constitutes the comune of Favignana in the Province of Trapani .
The main occupation of the islanders is fishing and this is where the largest tuna fishery in Sicily can be found .
The islands were the scene of the Battle of the Aegates Islands of 241 BC , in which the Carthaginian fleet was defeated by C. Lutatius Catulus ; the engagement ended the First Punic War .
