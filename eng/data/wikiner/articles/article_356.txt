Considered one of the most important archaeological sites of Ancient Egypt , the sacred city of Abydos was the site of many ancient temples , including a Umm el-Qa'ab , a royal necropolis where early pharaohs were entombed .
Its equivalent in Arabic is أبيدوس .
The pharaohs of the first dynasty were buried in Abydos , including Narmer , who is regarded as founder of the first dynasty , and his successor , Aha .
Some pharaohs of the second dynasty were also buried in Abydos .
Abydos became the centre of the worship of the Isis and Osiris cult .
In the twelfth dynasty a gigantic tomb was cut into the rock by Senusret III .
Merneptah added the Osireion just to the north of the temple of Seti .
Ahmose II in the twenty-sixth dynasty rebuilt the temple again , and placed in it a large monolith shrine of red granite , finely wrought .
The latest building was a new temple of Nectanebo I , built in the thirtieth dynasty .
A tradition developed that the Early Dynastic cemetery was the burial place of Osiris and the tomb of Djer was reinterpreted as that of Osiris .
A vase of Menes with purple hieroglyphs inlaid into a green glaze and tiles with relief figures are the most important pieces found .
The noble statuette of Cheops in ivory , found in the stone chamber of the temple , gives the only portrait of this great pharaoh .
The temple was rebuilt entirely on a larger scale by Pepi I in the sixth dynasty .
Soon after , Mentuhotep II entirely rebuilt the temple , laying a stone pavement over the area , about 45 ft ( 14 m ) square , and added subsidiary chambers .
Soon thereafter in the twelfth dynasty , Senusret I laid massive foundations of stone over the pavement of his predecessor .
The temple of Seti I was built on entirely new ground half a mile to the south of the long series of temples just described .
This surviving building is best known as the Great Temple of Abydos , being nearly complete and an impressive sight .
Except for the list of pharaohs and a panegyric on Ramesses II , the subjects are not historical , but mythological .
The sculptures had been published mostly in hand copy , not facsimile , by Auguste Mariette in his Abydos , i .
The adjacent temple of Ramesses II was much smaller and simpler in plan ; but it had a fine historical series of scenes around the outside that lauded his achievements , of which the lower parts remain .
The outside of the temple was decorated with scenes of the Battle of Kadesh .
Others also built before Menes are 15×25 ft ( 4.6 x 7.6 m ) .
The probable tomb of Menes is of the latter size .
Some of the offerings included sacrificed animals , such as the asses found in the tomb of Merneith .
Many hundreds of funeral steles were removed by Mariette 's workmen , without any record of the burials being made .
Later excavations have been recorded by Edward R. Ayrton , Abydos , iii .
Known as Shunet ez Zebib , it is about 450 × 250 ft ( 137 x 76 m ) over all , and one still stands 30-ft ( 9 m ) high .
It was built by Khasekhemwy , the last pharaoh of the second dynasty .
Another structure nearly as large adjoined it , and probably is older than that of Khasekhemwy .
This concept was adopted in the plot of the Stargate series .
