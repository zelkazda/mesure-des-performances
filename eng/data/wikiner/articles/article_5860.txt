The Thompson family resided in the Cherokee Triangle neighborhood of the Highlands in Louisville .
Thompson attended the I.N .
Its members at the time , generally drawn from Louisville 's wealthy upper-class families , included Porter Bibb , who became the first publisher of Rolling Stone .
He served 30 days of his sentence , and joined the U.S. Air Force a week after his release .
Thompson claimed in a mock press release he wrote about the end of his duty to have been issued a " totally unclassifiable " status .
There he attended Columbia University 's School of General Studies part-time on the G.I. Bill , taking classes in short-story writing .
During this time he worked briefly for Time , as a copy boy for $ 51 a week .
While working , he used a typewriter to copy F. Scott Fitzgerald 's The Great Gatsby and Ernest Hemingway 's A Farewell to Arms in order to learn about the writing styles of the authors .
In 1959 , Time fired him for insubordination .
Later that year , he worked as a reporter for The Middletown Daily Record in Middletown , New York .
While there , he was able to publish his first magazine feature in the nationally distributed Rogue magazine on the artisan and bohemian culture of Big Sur .
During this period , Thompson wrote two novels , Prince Jellyfish and The Rum Diary , and submitted many short stories to publishers with little success .
While working on the story , Thompson symbolically stole a pair of elk antlers hanging above the front door of Hemingway 's cabin .
Thompson and the editors at the Observer eventually had a falling out after the paper refused to print Thompson 's review of Tom Wolfe 's 1965 essay collection The Kandy-Kolored Tangerine-Flake Streamline Baby , and he moved to San Francisco , immersing himself in the drug and hippie culture that was taking root in the area .
The relationship broke down when the bikers suspected that Thompson was only friends with them so he could make money from his writing .
Random House published the hard cover Hell 's Angels : The Strange and Terrible Saga of the Outlaw Motorcycle Gangs in 1966 .
A reviewer for The New York Times praised it as an " angry , knowledgeable , fascinating and excitedly written book " , that shows the Hells Angels " not so much as dropouts from society but as total misfits , or unfits -- emotionally , intellectually and educationally unfit to achieve the rewards , such as they are , that the contemporary social order offers . "
The reviewer also praised Thompson as a " spirited , witty , observant and original writer ; his prose crackles like motorcycle exhaust . "
Following the success of Hells Angels , Thompson was able to publish articles in a number of well-known magazines during the late 1960s , including The New York Times Magazine , Esquire , Pageant , and others .
From his hotel room in Chicago , Thompson watched the clashes between police and protesters , which he wrote had a great effect on his political views .
A few weeks after the contract was signed , however , Johnson announced that he would not stand for re-election , and the deal was canceled .
In early 1969 , Thompson finally received a $ 15,000 royalty check for the paperback sales of Hells Angels and used two-thirds of the money for a down payment on a modest home and property where he would live for the rest of his life .
In 1970 Thompson ran for sheriff of Pitkin County , Colorado , as part of a group of citizens running for local offices on the " Freak Power " ticket .
Thompson , having shaved his head , referred to his opponent as " my long-haired opponent " , as the Republican candidate had a crew cut .
Thus , Thompson 's first article in Rolling Stone was published as The Battle of Aspen with the byline " By : Dr. Hunter S. Thompson . "
Despite the publicity , Thompson ended up narrowly losing the election .
Thompson later remarked that the Rolling Stone article mobilized his opposition far more than his supporters .
Also in 1970 , Thompson wrote an article entitled The Kentucky Derby Is Decadent and Depraved for the short-lived new journalism magazine Scanlan 's Monthly .
Ralph Steadman , who would later collaborate with Thompson on several projects , contributed expressionist pen-and-ink illustrations .
Horatio Alger gone mad on drugs in Las Vegas .
Salazar had been shot in the head at close range with a tear gas canister fired by officers of the Los Angeles County Sheriff 's Department during the National Chicano Moratorium March against the Vietnam War .
Thompson first submitted to Sports Illustrated a manuscript of 2,500 words , which was , as he later wrote , " aggressively rejected . "
Rolling Stone publisher Jann Wenner was said to have liked " the first 20 or so jangled pages enough to take it seriously on its own terms and tentatively scheduled it for publication -- which gave me the push I needed to keep working on it " , Thompson later wrote .
The result of the trip to Las Vegas became the 1972 book Fear and Loathing in Las Vegas which first appeared in the November 1971 issues of Rolling Stone as a two-part series .
Coming to terms with the failure of the 1960s countercultural movement is a major theme of the novel , and the book was greeted with considerable critical acclaim , including being heralded by the New York Times as " by far the best book yet written on the decade of dope " .
The articles were soon combined and published as Fear and Loathing on the Campaign Trail ' 72 .
As the title suggests , Thompson spent nearly all of his time traveling the " campaign trail " , focusing largely on the Democratic Party 's primaries ( Nixon , as an incumbent , performed little campaign work ) in which McGovern competed with rival candidates Edmund Muskie and Hubert Humphrey .
Thompson was an early supporter of McGovern , and it could be argued that his unflattering coverage of the rival campaigns in the increasingly widely read Rolling Stone played a role in the senator 's nomination .
Thompson went on to become a fierce critic of Nixon , both during and after his presidency .
After Nixon 's death in 1994 , Thompson famously described him in Rolling Stone as a man who " could shake your hand and stab you in the back at the same time " and said " his casket [ should ] have been launched into one of those open-sewage canals that empty into the ocean just south of Los Angeles .
The one passion they shared was a love of football , which is discussed in Fear and Loathing on the Campaign Trail ' 72 .
Thompson was to provide Rolling Stone similar coverage for the 1976 Presidential Campaign that would appear in a book published by the magazine .
Reportedly , as Thompson was waiting for a $ 75,000 advance cheque to arrive , he learned that Rolling Stone publisher Jann Wenner had pulled the plug on the endeavor without telling Thompson .
Wenner then asked Thompson to travel to Vietnam to report on what appeared to be the closing of the Vietnam War .
Thompson accepted , and left for Saigon immediately .
Thompson 's story about the fall of Saigon would not be published in Rolling Stone until ten years later .
These two incidents severely strained the relationship between the author and the magazine , and Thompson contributed far less to the publication in later years .
Murray would go on to become one of Thompson 's only trusted friends [ citation needed ] .
In 1983 , he covered the U.S. invasion of Grenada but would not discuss these experiences until the publication of Kingdom of Fear 20 years later .
In 1990 former porn director Gail Palmer visited Thompson 's home in Woody Creek .
A six person 11 hour search of Thompson 's home turned up various kinds of drugs and a few sticks of dynamite .
Thompson would later describe this experience at length in Kingdom of Fear .
He falls in love and gets in even more trouble than he was in the sex theater in San Francisco " .
The novel was slated to be released by Random House in 1999 , and was even assigned ISBN 0-679-40694-8 , but was not published .
Thompson continued to contribute irregularly to Rolling Stone .
Rather than embarking on the campaign trail as he had done in previous presidential elections , Thompson monitored the proceedings from cable television ; Better Than Sex : Confessions of a Political Junkie , his account of the 1992 Presidential Election campaign , is composed of reactionary faxes sent to Rolling Stone .
Thompson was named a Kentucky Colonel by the Governor of Kentucky in a December 1996 tribute ceremony where he also received keys to the city of Louisville .
Despite publishing a novel and numerous newspaper and magazine articles , the majority of Thompson 's literary output after the late 1970s took the form of a 4-volume series of books called The Gonzo Papers .
Beginning with The Great Shark Hunt in 1979 and ending with Better Than Sex in 1994 , the series is largely a collection of rare newspaper and magazine pieces from the pre-gonzo period , along with almost all of his Rolling Stone short pieces , excerpts from the Fear and Loathing ... books , and so on .
By the late 1970s Thompson received complaints from critics , fans and friends that he was regurgitating his past glories without much new on his part ; these concerns are alluded to in the introduction of The Great Shark Hunt , where Thompson suggested that his " old self " committed suicide .
Perhaps in response to this , as well as the strained relationship with Rolling Stone , and the failure of his marriage , Thompson became more reclusive after 1980 .
He would often retreat to his compound in Woody Creek and reject assignments or refuse to complete them .
Thompson 's work was popularized again with the 1998 release of the film Fear and Loathing in Las Vegas , which opened to considerable fanfare .
The novel was reprinted to coincide with the film , and Thompson 's work was introduced to a new generation of readers .
Soon thereafter , Thompson 's " long lost " novel The Rum Diary was published , as were the first two volumes of his collected letters , which were greeted with critical acclaim .
Thompson 's next , and penultimate , collection , Kingdom of Fear , was a combination of new material , selected newspaper clippings , and some older works .
Hunter married Anita Bejmuk , his long-time assistant , on April 23 , 2003 .
Thompson ended his journalism career in the same way it had begun : writing about sports .
Simon & Schuster bundled many of the columns from the first few years and released it in mid-2004 as Hey Rube : Blood Sport , the Bush Doctrine , and the Downward Spiral of Dumbness -- Modern History from the Sports Desk .
Thompson 's son , daughter-in-law and grandson ( Will Thompson ) were visiting for the weekend at the time of his suicide .
The police report concerning his death stated that in a typewriter in front of Thompson , they found " a piece of paper carrying the date ' Feb 22 ' 05 ' and the single word ' counselor ' . "
They reported to the press that they do not believe his suicide was out of desperation , but was a well-thought out act resulting from Thompson 's many painful and chronic medical conditions .
What family and police describe as a suicide note was written by Thompson four days before his death , and left for his wife .
It was later published by Rolling Stone .
Artist and friend Ralph Steadman wrote :
Depp told the Associated Press , " All I 'm doing is trying to make sure his last wish comes true .
The video footage of Steadman and Thompson drawing the plans and outdoor footage showing where he wanted the cannon constructed were planned prior to the unveiling of his cannon at the funeral .
Thompson almost always wrote in the first person , while extensively using his own experiences and emotions to color " the story " he was trying to follow .
While Thompson 's approach clearly involved injecting himself as a participant in the events of the narrative , it also involved adding invented , metaphoric elements , thus creating , for the uninitiated reader , a seemingly confusing amalgam of facts and fiction notable for the deliberately blurred lines between one and the other .
Tom Wolfe would later describe Thompson 's style as " ... part journalism and part personal memoir admixed with powers of wild invention and wilder rhetoric . "
Or as one description of the differences between Thompson and Wolfe 's styles would elaborate , " While Tom Wolfe mastered the technique of being a fly on the wall , Thompson mastered the art of being a fly in the ointment . "
The majority of Thompson 's most popular and acclaimed work appeared within the pages of Rolling Stone magazine .
Nevertheless , his articles were always peppered with a wide array of pop music references ranging from Howlin ' Wolf to Lou Reed .
Armed with early fax machines wherever he went , he became notorious for haphazardly sending sometimes illegible material to the magazine 's San Francisco offices as an issue was about to go to press .
Love called fact-checking Thompson 's work " one of the sketchiest occupations ever created in the publishing world " , and " for the first-timer ... a trip through a journalistic fun house , where you did n't know what was real and what was n't .
Hunter was a stickler for numbers , for details like gross weight and model numbers , for lyrics and caliber , and there was no faking it . "
In the late sixties , Thompson obtained his famous title of " Doctor " from the Universal Life Church .
A number of critics have commented that as he grew older the line that distinguished Thompson from his literary self became increasingly blurred .
Thompson himself admitted during a 1978 BBC interview that he sometimes felt pressured to live up to the fictional self that he had created , adding " I 'm never sure which one people expect me to be .
When I get invited to , say , speak at universities , I 'm not sure if they are inviting Duke or Thompson .
Thompson 's writing style and eccentric persona gave him a cult following in both literary and drug circles , and his cult status expanded into broader areas after being twice portrayed in major motion pictures .
Hence , both his writing style and persona have been widely imitated , and his likeness has even become a popular costume choice for Halloween .
In the documentary Breakfast With Hunter , Hunter S. Thompson is seen in several scenes wearing different Che Guevara t-shirts .
Additionally , actor and friend Benicio del Toro has stated that Thompson kept a " big " picture of Che in his kitchen .
Thompson organized rallies , provided legal support , and co-wrote an article in the June 2004 issue of Vanity Fair , outlining the case .
Thompson was a firearms and explosives enthusiast ( in his writing and in real life ) and owned a vast collection of handguns , rifles , shotguns , and various automatic and semi-automatic weapons , along with numerous forms of gaseous crowd control and many other homemade devices .
Thompson was also an ardent supporter of drug legalization and became known for his less-than-shy accounts of his own drug usage .
He was an early supporter of the National Organization for the Reform of Marijuana Laws and served on the group 's advisory board for over 30 years until his death .
He suggested to several interviewers that it may have been conducted by the U.S. Government or with the government 's assistance .
In 2002 , Thompson told a radio show host " You sort of wonder when something like that happens , well , who stands to benefit ?
Thompson wrote many letters and they were his primary means of personal conversation .
Thompson made carbon copies of all his letters , usually typed , a habit that began in his teenage years .
Douglas Brinkley , who edits the letter series , said that for every letter included , fifteen were cut .
Brinkley estimated Thompson 's own archive to contain over 20,000 letters .
Amazon.com currently lists the publication date on its site as February 1 , 2011 .
Many biographies have been written about Thompson , although he did not write an autobiography .
Some of these letters were already bundled into Thompson 's Kingdom of Fear , though it is not considered an autobiography .
Steadman and Thompson developed a close friendship , and often traveled together .
Though his illustrations occur in most of Thompson 's books , they are conspicuously featured in full page color in Thompson 's The Curse of Lono , set in Hawaii .
Thompson 's snapshots were a combination of the subjects he was covering , stylized self-portraits , and artistic still life photos .
The London Observer called the photos " astonishingly good " and that " Thompson 's pictures remind us , brilliantly in every sense , of very real people , real colours " .
The film Where the Buffalo Roam ( 1980 ) depicts Thompson 's attempts at writing stories for both the Super Bowl and the 1972 U.S. presidential election .
Bruce Robinson is directing the cast which along with Depp includes Giovanni Ribisi , Aaron Eckhart and Amber Heard .
It can be found on disc 2 of " Fear and Loathing in Las Vegas " The Criterion Collection edition .
Wayne Ewing created three documentaries about Thompson .
It documents Thompson 's work on the movie Fear and Loathing in Las Vegas , his arrest for drunk driving , and his subsequent fight with the court system .
The film premiered on January 20 , 2008 at the Sundance Film Festival .
Gibney uses intimate , never-before-seen home videos , interviews with friends , enemies and lovers , and clips from films adapted from Thompson 's material to document his turbulent life .
Set in the writing den of Thompson 's Woody Creek home , the show presents the life of Hunter during the years between 1968 and 1971 .
