Aeacus was a mythological king of the island of Aegina in the Saronic Gulf .
He was son of Zeus and Aegina , a daughter of the river-god Asopus .
According to some accounts Aeacus was a son of Zeus and Europa .
Some traditions related that at the time when Aeacus was born , Aegina was not yet inhabited , and that Zeus changed the ants ( μύρμηκες ) of the island into men over whom Aeacus ruled , or that he made men grow up out of the earth .
Aeacus prayed , and it ceased in consequence .
Aeacus was believed in later times to be buried under the altar in this sacred enclosure .
When the work was completed , three dragons rushed against the wall , and while the two of them which attacked those parts of the wall built by the gods fell down dead , the third forced its way into the city through the part built by Aeacus .
Aeacus was also believed by the Aeginetans to have surrounded their island with high cliffs to protect it against pirates .
Several other incidents connected with the story of Aeacus are mentioned by Ovid .
In works of art he was represented bearing a sceptre and the keys of Hades .
Aeacus had sanctuaries both at Athens and in Aegina , and the Aeginetans regarded him as the tutelary deity of their island .
In The Frogs ( 405 BC ) by Aristophanes , Dionysus descends to Hades and announces himself as Heracles .
Alexander the Great traced his ancestry ( through his mother ) to Aeacus .
