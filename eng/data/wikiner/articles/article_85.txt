Albert Einstein ; 14 March 1879 -- 18 April 1955 ) was a theoretical physicist , philosopher and author who is widely regarded as one of the most influential and best known scientists and intellectuals of all time .
Einstein published more than 300 scientific and over 150 non-scientific works ; he additionally wrote and commentated prolifically on various philosophical and political subjects .
His great intelligence and originality has made the word " Einstein " synonymous with genius .
His father was Hermann Einstein , a salesman and engineer .
His mother was Pauline Einstein ( née Koch ) .
Their son attended a Catholic elementary school from the age of five until ten .
Although Einstein had early speech difficulties , he was a top student in elementary school .
His father once showed him a pocket compass ; Einstein realized that there must be something causing the needle to move , despite the apparent " empty space . "
As he grew , Einstein built models and mechanical devices for fun and began to show a talent for mathematics .
When the family moved to Pavia , Einstein stayed in Munich to finish his studies at the Luitpold Gymnasium .
His father intended for him to pursue electrical engineering , but Einstein clashed with authorities and resented the school 's regimen and teaching method .
In the spring of 1895 , he withdrew to join his family in Pavia , convincing the school to let him go by using a doctor 's note .
Einstein applied directly to the Eidgenössische Polytechnische Schule ( ETH ) in Zürich , Switzerland .
In Aarau , Einstein studied Maxwell 's electromagnetic theory .
Over the next few years , Einstein and Marić 's friendship developed into romance .
In a letter to her , Einstein called Marić " a creature who is my equal and who is as strong and independent as I am . "
In early 1902 , Einstein and Mileva Marić had a daughter they named Lieserl in their correspondence , who was born in Novi Sad where Marić 's parents lived .
Einstein and Marić married in January 1903 .
In May 1904 , the couple 's first son , Hans Albert Einstein , was born in Bern , Switzerland .
Their second son , Eduard , was born in Zurich in July 1910 .
Marić and Einstein divorced on 14 February 1919 , having lived apart for five years .
Einstein married Elsa Löwenthal ( née Einstein ) on 2 June 1919 , after having had a relationship with her since 1912 .
In 1935 , Elsa Einstein was diagnosed with heart and kidney problems and died in December 1936 .
Much of his work at the patent office related to questions about transmission of electric signals and electrical-mechanical synchronization of time , two technical problems that show up conspicuously in the thought experiments that eventually led Einstein to his radical conclusions about the nature of light and the fundamental connection between space and time .
With friends he met in Bern , Einstein formed a weekly discussion club on science and philosophy , which he jokingly named " The Olympia Academy . "
Their readings included the works of Henri Poincaré , Ernst Mach , and David Hume , which influenced his scientific and philosophical outlook .
In 1901 , Einstein had a paper on the capillary forces of a straw published in the prestigious Annalen der Physik .
By 1908 , he was recognized as a leading scientist , and he was appointed lecturer at the University of Berne .
The following year , he quit the patent office and the lectureship to take the position of physics professor at the University of Zurich .
He became a full professor at Karl-Ferdinand University in Prague in 1911 .
He became a member of the Prussian Academy of Sciences .
In 1916 , Einstein was appointed president of the German Physical Society ( 1916 -- 1918 ) .
International media reports of this made Einstein world famous .
( Much later , questions were raised whether the measurements were accurate enough to support Einstein 's theory . )
In 1921 , Einstein was awarded the Nobel Prize in Physics .
When asked where he got his scientific ideas , Einstein explained that he believed scientific work best proceeds from an examination of physical reality and a search for underlying axioms , with consistent explanations that apply in all instances and avoid contradicting each other .
In Einstein 's talk to the audience , he expressed his happiness over the event :
In 1933 , Einstein was compelled to emigrate to the United States due to the rise to power of the Nazis under Germany 's new chancellor , Adolf Hitler .
A month later , notes Einstein biographer , Walter Isaacson , " a parade of swastica-wearing students and beer-hall thugs carrying torches tossed books into a huge bonfire .
' Jewish intellectualism is dead, ' propaganda minister Joseph Goebbels , his face fiery , yelled from the podium . "
Einstein also learned that his name was on a list of assassination targets , with a " $ 5,000 bounty on his head . "
In 1935 , Einstein traveled to the United States via Albania .
With so many other Jewish scientists now forced by circumstances to live in America , often working side by side , Einstein wrote to a friend , " For me the most beautiful thing is to be in contact with a few fine Jews -- a few millennia of a civilized past do mean something after all . "
In another letter he writes , " In my whole life I have never felt so Jewish as now . "
He took up a position at the Institute for Advanced Study at Princeton , N.J. , an affiliation that lasted until his death in 1955 .
His last assistant was Bruria Kaufman , who later became a renowned physicist .
As a result of Einstein 's letter , the U.S. entered the " race " to develop the bomb first , drawing on its " immense material , financial , and scientific resources . "
He became an American citizen in 1940 .
According to Isaacson , he recognized the " right of individuals to say and think what they pleased, " without social barriers , and as result , the individual was " encouraged " to be more creative , a trait he valued from his own early education .
Einstein writes :
After the death of Israel 's first president , Chaim Weizmann , in November 1952 , Prime Minister David Ben-Gurion offered Einstein the position of President of Israel , a mostly ceremonial post .
The offer was presented by Israel 's ambassador in Washington , Abba Eban , who explained that the offer " embodies the deepest respect which the Jewish people can repose in any of its sons .
He took the draft of a speech he was preparing for a television appearance commemorating the State of Israel 's seventh anniversary with him to the hospital , but he did not live long enough to complete it .
Einstein refused surgery , saying : " I want to go when I want .
Einstein 's remains were cremated and his ashes were scattered around the grounds of the Institute for Advanced Study .
Throughout his life , Einstein published hundreds of books and articles .
Einstein 's early papers all come from attempts to demonstrate that atoms exist and have a finite nonzero size .
At the time of his first paper in 1902 , it was not yet completely accepted by physicists that atoms were real , even though chemists had good evidence ever since Antoine Lavoisier 's work a century earlier .
Ludwig Boltzmann was a leading 19th century atomist physicist , who had struggled for years to gain acceptance for atoms .
Boltzmann had given an interpretation of the laws of thermodynamics , suggesting that the law of entropy increase is statistical .
In Boltzmann 's way of thinking , the entropy is the logarithm of the number of ways a system could be configured inside .
While Boltzmann 's statistical interpretation of entropy is universally accepted today , and Einstein believed it , at the turn of the 20th century it was a minority position .
James Clerk Maxwell , another leading atomist , had found the distribution of velocities of atoms in a gas , and derived the surprising result that the viscosity of a gas should be independent of density .
Notable among the skeptics was Ernst Mach , whose positivist philosophy led him to demand that if atoms are real , it should be possible to see them directly .
Einstein opposed this position .
Einstein 's earliest papers were concerned with thermodynamics .
Einstein pointed out that the statistical fluctuations of a macroscopic object , like a mirror suspended on spring , would be completely determined by the second derivative of the entropy with respect to the position of the mirror .
The law of friction for a small ball in a viscous fluid like water was discovered by George Stokes .
This relation could be used to calculate how far a small ball in water would travel due to its random thermal motion , and Einstein noted that such a ball , of size about a micron , would travel about a few microns per second .
Einstein was able to identify this motion with that predicted by his theory .
Unlike the other methods , Einstein 's required very few theoretical assumptions or new physics , since it was directly measuring atomic motion on visible grains .
Einstein 's thinking underwent a transformation in 1905 .
Einstein sought new principles of this sort , to guide the production of physical ideas .
This was understood by Hermann Minkowski to be a generalization of rotational invariance from space to space-time .
Other principles postulated by Einstein and later vindicated are the principle of equivalence and the principle of adiabatic invariance of the quantum number .
The use of a-priori principles is a distinctive unique signature of Einstein 's early work , and has become a standard tool in modern theoretical physics .
In his paper on mass -- energy equivalence , which had previously been considered to be distinct concepts , Einstein deduced from his equations of special relativity what has been called the twentieth century 's best-known equation : E = mc 2 .
Einstein 's 1905 work on relativity remained controversial for many years , but was accepted by leading physicists , starting with Max Planck .
In a 1905 paper , Einstein postulated that light itself consists of localized particles ( quanta ) .
Einstein 's light quanta were nearly universally rejected by all physicists , including Max Planck and Niels Bohr .
Einstein 's paper on the light particles was almost entirely motivated by thermodynamic considerations .
Einstein continued his work on quantum mechanics in 1906 , by explaining the specific heat anomaly in solids .
Since Planck 's distribution for light oscillators had no problem with infinite specific heats , the same idea could be applied to solids to fix the specific heat problem there .
Einstein showed in a simple model that the hypothesis that solid motion is quantized explains why the specific heat of a solid goes to zero at zero temperature .
Einstein 's model treats each atom as connected to a single spring .
Instead of connecting all the atoms to each other , which leads to standing waves with all sorts of different frequencies , Einstein imagined that each atom was attached to a fixed point in space by a spring .
Einstein then assumes that the motion in this model is quantized , according to the Planck law , so that each independent spring motion has energy which is an integer multiple of hf , where f is the frequency of oscillation .
With this assumption , he applied Boltzmann 's statistical method to calculate the average energy of the spring .
So Einstein concluded that quantum mechanics would solve the main problem of classical physics , the specific heat anomaly .
Because all of Einstein 's springs have the same stiffness , they all freeze out at the same temperature , and this leads to a prediction that the specific heat should go to zero exponentially fast when the temperature is low .
This was done by Peter Debye , and after this modification Einstein 's quantization method reproduced quantitatively the behavior of the specific heats of solids at low temperatures .
After Ernest Rutherford discovered the nucleus and proposed that electrons orbit like planets , Niels Bohr was able to show that the same quantum mechanical postulates introduced by Planck and developed by Einstein would explain the discrete motion of electrons in atoms , and the periodic table of the elements .
Einstein contributed to these developments by linking them with the 1898 arguments Wilhelm Wien had made .
Einstein noted in 1911 that the same adiabatic principle shows that the quantity which is quantized in any mechanical motion must be an adiabatic invariant .
Arnold Sommerfeld identified this adiabatic invariant as the action variable of classical mechanics .
In 1908 , he became a privatdozent at the University of Bern .
This paper introduced the photon concept ( although the name photon was introduced later by Gilbert N. Lewis in 1926 ) and inspired the notion of wave-particle duality in quantum mechanics .
Einstein returned to the problem of thermodynamic fluctuations , giving a treatment of the density variations in a fluid at its critical point .
Einstein 's physical intuition led him to note that Planck 's oscillator energies had an incorrect zero point .
He modified Planck 's hypothesis by stating that the lowest energy state of an oscillator is equal to 1 ⁄ 2 hf , to half the energy spacing between levels .
This argument , which was made in 1913 in collaboration with Otto Stern , was based on the thermodynamics of a diatomic molecule which can split apart into two free atoms .
In 1907 , while still working at the patent office , Einstein had what he would call his " happiest thought " .
This gave him confidence that the scalar theory of gravity proposed by Gunnar Nordström was incorrect .
When Einstein finished the full theory of general relativity , he would rectify this error and predict the correct amount of light deflection by the sun .
From Prague , Einstein published a paper about the effects of gravity on light , specifically the gravitational redshift and the gravitational deflection of light .
German astronomer Erwin Finlay-Freundlich publicized Einstein 's challenge to scientists around the world .
Einstein thought about the nature of the gravitational field in the years 1909 -- 1912 , studying its properties by means of simple thought experiments .
Einstein imagined an observer making experiments on a rotating turntable .
He noted that such an observer would find a different value for the mathematical constant pi than the one predicted by Euclidean geometry .
Since Einstein believed that the laws of physics were local , described by local fields , he concluded from this that spacetime could be locally curved .
While developing general relativity , Einstein became confused about the gauge invariance in the theory .
Simultaneously less elegant and more difficult than general relativity , after more than two years of intensive work Einstein abandoned the theory in November , 1915 after realizing that the hole argument was mistaken .
In 1912 , Einstein returned to Switzerland to accept a professorship at his alma mater , the ETH .
For a while Einstein thought that there were problems with the approach , but he later returned to it and , by late 1915 , had published his general theory of relativity in the form in which it is used today .
In 1917 , several astronomers accepted Einstein 's 1911 challenge from Prague .
Nobel laureate Max Born praised general relativity as the " greatest feat of human thinking about nature " ; fellow laureate Paul Dirac was quoted saying it was " probably the greatest scientific discovery ever made " .
The international media guaranteed Einstein 's global renown .
To fix this , Einstein modified the general theory by introducing a new notion , the cosmological constant .
This article showed that the statistics of absorption and emission of light would only be consistent with Planck 's distribution law if the emission of light into a mode with n photons would be enhanced statistically compared to the emission of light into an empty mode .
Einstein discovered Louis de Broglie 's work , and supported his ideas , which were received skeptically at first .
This paper would inspire Schrödinger 's work of 1926 .
It was not until 1995 that the first such condensate was produced experimentally by Eric Allin Cornell and Carl Wieman using ultra-cooling equipment built at the NIST -- JILA laboratory at the University of Colorado at Boulder .
Einstein argued that this is true for fundamental reasons , because the gravitational field could be made to vanish by a choice of coordinates .
This approach has been echoed by Lev Landau and Evgeny Lifshitz , and others , and has become standard .
The use of non-covariant objects like pseudotensors was heavily criticized in 1917 by Erwin Schrödinger and others .
Following his research on general relativity , Einstein entered into a series of attempts to generalize his geometric theory of gravitation , which would allow the explanation of electromagnetism .
Although he continued to be lauded for his work , Einstein became increasingly isolated in his research , and his efforts were ultimately unsuccessful .
In his pursuit of a unification of the fundamental forces , Einstein ignored some mainstream developments in physics , most notably the strong and weak nuclear forces , which were not well understood until many years after his death .
Mainstream physics , in turn , largely ignored Einstein 's approaches to unification .
Einstein 's dream of unifying other laws of physics with gravity motivates modern quests for a theory of everything and in particular string theory , where geometrical fields emerge in a unified quantum-mechanical setting .
Einstein collaborated with others to produce a model of a wormhole .
These properties led Einstein to believe that pairs of particles and antiparticles could be described in this way .
In 1935 , Einstein returned to the question of quantum mechanics .
This principle distilled the essence of Einstein 's objection to quantum mechanics .
So Einstein proposed that the path of a singular solution , like a black hole , would be determined to be a geodesic from general relativity itself .
In addition to his well-accepted results , some of Einstein 's views are regarded as controversial :
In addition to long time collaborators Leopold Infeld , Nathan Rosen , Peter Bergmann and others , Einstein also had some one-shot collaborations with various scientists .
Erwin Schrödinger applied this to derive the thermodynamic properties of a semiclassical ideal gas .
Schrödinger urged Einstein to add his name as co-author , although Einstein declined the invitation .
Throughout the November Revolution in Germany Einstein signed an appeal for the foundation of a nationwide liberal and democratic party , which was published in the Berliner Tageblatt on 16 November 1918 , and became a member of the German Democratic Party .
Einstein was a socialist .
Albert Einstein described a chaotic capitalist society , a source of evil to be overcome , as the " predatory phase of human development " .
Days before his death , Einstein signed the Russell -- Einstein Manifesto , which led to the Pugwash Conferences on Science and World Affairs .
Einstein 's friendship with activist Paul Robeson , with whom he served as co-chair of the American Crusade to End Lynching , lasted twenty years .
Einstein said " Politics is for the moment , equation for the eternity " , stating that physics was more important in his life .
He declined the presidency of Israel in 1952 .
The letters were included in the papers bequeathed to The Hebrew University .
Corbis , successor to The Roger Richman Agency , licenses the use of his name and associated imagery , as agent for the university .
Always I am mistaken for Professor Einstein . "
Einstein has been the subject of or inspiration for many novels , films , plays , and works of music .
In 1936 , Einstein was awarded the Franklin Institute 's Franklin Medal for his extensive work on relativity and the photo-electric effect .
The statue , commissioned in 1979 , is located in a grove of trees at the southwest corner of the grounds of the National Academy of Sciences on Constitution Avenue .
The chemical element 99 , einsteinium , was named for him in August 1955 , four months after Einstein 's death .
2001 Einstein is an inner main belt asteroid discovered on 5 March 1973 .
In 1999 Time magazine named him the Person of the Century , ahead of Mahatma Gandhi and Franklin Roosevelt , among others .
In the words of a biographer , " to the scientifically literate and the public at large , Einstein is synonymous with genius . "
Also in 1999 , an opinion poll of 100 of leading physicists ranked Einstein the " greatest physicist ever " .
The winner is selected by a committee ( the first of which consisted of Einstein , Oppenheimer , von Neumann and Weyl ) of the Institute for Advanced Study , which administers the award .
The Albert Einstein Medal is an award presented by the Albert Einstein Society in Bern , Switzerland .
First given in 1979 , the award is presented to people who have " rendered outstanding services " in connection with Einstein .
