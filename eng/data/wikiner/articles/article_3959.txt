Davis Langdon carried out the project management , Sir Robert McAlpine and Alfred McAlpine did the construction and MERO designed and built the biomes .
Land Use Consultants led the masterplan and landscape design .
The ETFE technology was supplied and installed by the firm Vector Foiltec , which is also responsible for ongoing maintenance of the cladding .
It provides the Eden Project with an education facility , incorporating classrooms and exhibition spaces designed to help communicate Eden 's central message about the relationship between people and plants .
The copper was obtained from traceable sources , and the Eden Project is working with Rio Tinto to explore the possibility of encouraging further traceable supply routes for metals , which would enable users to avoid metals mined unethically .
The services and acoustic design was carried out by Buro Happold .
The mechanical and electrical engineering design was by Buro Happold .
The Eden Project includes environmental education focusing on the interdependence of plants and people ; plants are labelled with their medicinal uses .
Rio Tinto is set to begin mining in Madagascar for titanium dioxide .
This will involve the removal of a large section of coastal forest , and may cause extensive damage to the unique biodiversity of the Malagasy flora and fauna .
The first part of the Eden Project , the visitor centre , opened to the public in May 2000 .
It was also used as a filming location for the 2002 James Bond film , Die Another Day ( starring Pierce Brosnan ) .
It also provided some plants for the British Museum 's Africa garden .
This features an ice rink covering the lake , with a small café/bar attached , as well as a Christmas market .
In 2007 , there was press criticism that the Eden Project received too much public funding , £ 130 million from various sources , and that it should be more self-supporting .
Artists have included Amy Winehouse , James Morrison , Muse , Lily Allen , Snow Patrol , Pulp , Brian Wilson and The Magic Numbers .
2008 's summer headliners were : The Verve , Kaiser Chiefs and KT Tunstall .
Oasis were also set to play in the summer of 2008 , but the concert was postponed because Noel Gallagher was unable to perform after breaking three ribs in a stage invasion incident several weeks before .
