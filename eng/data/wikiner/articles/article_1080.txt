While not designed as autonomous per se , Fuller 's concern with sustainable and efficient design is congruent with the goal of autonomy , and showed that it was theoretically possible .
1990s architects such as William McDonough and Ken Yeang applied environmentally responsible building design to large commercial buildings , such as office buildings , making them largely self-sufficient in energy production .
One major bank building ( ING 's Amsterdam headquarters ) in the Netherlands was constructed to be autonomous and artistic as well .
The Vales , among others , have shown that living off-grid can be a practical , logical lifestyle choice -- under certain conditions .
In the driest areas , it might require a cistern of 30 m³ ( 8400 U.S. gallons ) .
NASA 's bioreactor is an extremely advanced biological sewage system .
NASA plans to use it in the manned Mars mission .
Another method is NASA 's urine-to-water distillation system .
In the Great Plains of the United States a 10 m turbine can supply enough energy to heat and cool a well-built all-electric house .
Wimax and forms of packet radio can also be used .
Finally , the Internet Radio Linking Project provides potential for blending older ( cheap ) local radio broadcasting with the increased range of the internet .
