The Berlin Wall ( German : Berliner Mauer ) was a barrier constructed by the German Democratic Republic ( GDR , East Germany ) starting August 13 , 1961 , that completely cut off West Berlin from surrounding East Germany and from East Berlin .
After several weeks of civil unrest , the East German government announced on November 9 , 1989 that all GDR citizens could visit West Germany and West Berlin .
After the end of World War II in Europe , what remained of pre-war Germany west of the Oder-Neisse line was divided into four occupation zones ( per the Potsdam Agreement ) , each one controlled by one of the four occupying Allied powers : the United States , United Kingdom , France and the Soviet Union .
The capital of Berlin , as the seat of the Allied Control Council , was similarly subdivided into four sectors despite the city 's location deep inside the Soviet zone .
Within two years , political divisions increased between the Soviets and the other occupying powers .
These included the Soviets ' refusal to agree to reconstruction plans making post-war Germany self-sufficient and to a detailed accounting of the industrial plants , goods and infrastructure already removed by the Soviets .
Britain , France , the United States and the Benelux countries later met to combine the non-Soviet zones of the country into one zone for reconstruction and approve the extension of the Marshall Plan .
The major task of the ruling communist party in the Soviet zone was to channel Soviet orders down to both the administrative apparatus and the other bloc parties , while pretending that these were initiatives of its own .
Property and industry was nationalized in the East German zone .
The East Germans created an elaborate political police apparatus that kept the population under close surveillance , including Soviet SMERSH secret police .
In 1948 , following disagreements regarding reconstruction and a new German currency , Stalin instituted the Berlin Blockade , preventing food , materials and supplies from arriving in West Berlin .
The Soviets mounted a public relations campaign against the western policy change .
The German Democratic Republic ( East Germany ) was declared on October 7 , 1949 .
The Soviets had unlimited power over the occupation regime and penetrated East German administrative , military and secret police structures .
As West Germany 's economy grew and its standard of living continually improved , many East Germans wanted to move to West Germany .
Taking advantage of the zonal border between occupied zones in Germany , the number of GDR citizens moving to West Germany totaled 187,000 in 1950 ; 165,000 in 1951 ; 182,000 in 1952 ; and 331,000 in 1953 .
Up until 1952 , the lines between East Germany and the western occupied zones could be easily crossed in most places .
Stalin agreed , calling the situation " intolerable " .
He advised the East Germans to build up their border defenses , telling them that " The demarcation line between East and West Germany should be considered a border -- and not just any border , but a dangerous one ...
The Germans will guard the line of defence with their lives . "
This resulted in Berlin becoming a magnet for East Germans desperate to escape life in the GDR , and also a flashpoint for tension between the U.S. and the Soviet Union .
In 1955 , the Soviets gave East Germany authority over civilian movement in Berlin , passing control to a regime not recognized in the West .
Initially , East Germany granted " visits " to allow its residents access to West Germany .
However , following the defection of large numbers of East Germans under this regime , the new East German state legally restricted virtually all travel to the West in 1956 .
With the closing of the inner German border officially in 1952 , the border in Berlin remained considerably more accessible then because it was administered by all four occupying powers .
Accordingly , Berlin became the main route by which East Germans left for the West .
On December 11 , 1957 , East Germany introduced a new passport law that reduced the overall number of refugees leaving Eastern Germany .
It had the unintended result of drastically increasing the percentage of those leaving through West Berlin from 60 % to well over 90 % by the end of 1958 .
The 3.5 million East Germans who had left by 1961 totaled approximately 20 % of the entire East German population .
The emigrants tended to be young and well-educated , leading to the " brain drain " feared by officials in East Germany .
Andropov reported that , while the East German leadership stated that they were leaving for economic reasons , testimony from refugees indicated that the reasons were more political than material .
The direct cost of manpower losses has been estimated at $ 7 billion to $ 9 billion , with East German party leader Walter Ulbricht later claiming that West Germany owed him $ 17 billion in compensation , including reparations as well as manpower losses .
In addition , the drain of East Germany 's young population potentially cost it over 22.5 billion marks in lost educational investment .
The brain drain of professionals had become so damaging to the political credibility and economic viability of East Germany that the re-securing of the German communist frontier was imperative .
The record of a telephone call between Nikita Khrushchev and Ulbricht on August 1 in the same year , suggests that it was Khrushchev from whom the initiative for the construction of the wall came .
There Ulbricht signed the order to close the border and erect a wall .
At midnight , the police and units of the East German army began to close the border ; and by Sunday morning , August 13 , the border with West Berlin was closed .
East German troops and workers had begun to tear up streets running alongside the border to make them impassable to most vehicles , and to install barbed wire entanglements and fences along the 156 kilometres ( 97 miles ) around the three western sectors , and the 43 kilometres ( 27 miles ) that divided West and East Berlin .
The barrier was built slightly inside East Berlin or East German territory to ensure that it did not encroach on West Berlin at any point .
Later it was built up into the Wall proper , the first concrete elements and large blocks being put in place on August 15 .
During the construction of the Wall , National People 's Army ( NVA ) and Combat Groups of the Working Class ( KdA ) soldiers stood in front of it with orders to shoot anyone who attempted to defect .
Additionally , chain fences , walls , minefields , and other obstacles were installed along the length of the inner-German border between East and West Germany .
West Berlin became an isolated enclave in a hostile land .
The Wall violated postwar Potsdam Agreements , which gave the U.K. , France and the United States a say over the administration of the whole of Berlin .
Thus they concluded that the possibility of a Soviet military conflict over Berlin decreased .
The East German government claimed that the Wall was an " anti-fascist protective rampart " intended to dissuade aggression from the West .
East Germans and others greeted such statements with skepticism , as most of the time , the border was only closed for citizens of East Germany traveling to the West , but not for residents of West Berlin traveling to the East .
The construction of the Wall had caused considerable hardship to families divided by it .
The view that the Wall was mainly a means of preventing the citizens of East Germany from entering West Berlin or fleeing was widely accepted .
An East German SED propaganda booklet published in 1955 dramatically described the serious nature of ' flight from the republic ' :
Kennedy appointed General Lucius D. Clay , an anti-communist known to have a firm attitude towards the Soviets , as his special advisor , sending him to Berlin with ambassadorial rank .
They arrived in a city defended by three Allied brigades -- one each from the UK , the US , and France .
On August 16 , Kennedy had given the order for them to be reinforced .
On Sunday morning , U.S. troops marched from West Germany through East Germany , bound for West Berlin .
Lead elements -- arranged in a column of 491 vehicles and trailers carrying 1,500 men , divided into five march units -- left the Helmstedt-Marienborn checkpoint at 06:34 .
The column was 160 kilometres ( 100 mi ) long , and covered 177 kilometres ( 110 mi ) from Marienborn to Berlin in full battle gear .
East German police watched from beside trees next to the autobahn all the way along .
The front of the convoy arrived at the outskirts of Berlin just before noon , to be met by Clay and Johnson , before parading through the streets of Berlin to an adoring crowd .
By stemming the exodus of people from East Germany , the East German government was able to reassert its control over the country : in spite of discontent with the wall , economic problems caused by dual currency and the black market were largely eliminated .
The economy in the GDR began to grow .
But , the Wall proved a public relations disaster for the communist bloc as a whole .
Western powers used it in propaganda as a symbol of communist tyranny , particularly after East German border guards shot and killed would-be defectors .
The Berlin Wall was more than 140 kilometres ( 87 mi ) long .
In June 1962 , a second , parallel fence some 100 metres ( 110 yd ) farther into East German territory was built .
Through the years , the Berlin Wall evolved through four versions :
The concrete provisions added to this version of the Wall were done so to prevent escapees from driving their cars through the barricades .
At strategic points the wall was constructed to a somewhat weaker standard so that East German and Soviet armored vehicles could break through easily in the event of war .
Besides the sector-sector boundary within Berlin itself , the wall also separated West Berlin from the present-day state of Brandenburg .
The following present-day municipalities , listed in counter-clockwise direction , share a border with former West Berlin :
Several other border crossings existed between West Berlin and surrounding East Germany .
After the 1972 agreements , new crossings were opened to allow West Berlin waste to be transported into East German dumps , as well as some crossings for access to West Berlin 's exclaves ( see Steinstücken ) .
Access to West Berlin was also possible by railway ( four routes ) and by boat for commercial shipping via canals and rivers .
When the Wall was erected , Berlin 's complex public transit networks , the S-Bahn and U-Bahn , were divided with it .
Three western lines traveled through brief sections of East Berlin territory , passing through eastern stations without stopping .
Both the eastern and western networks converged at Friedrichstraße , which became a major crossing point for those with permission to cross .
Usually this involved application of a visa at an East German embassy several weeks in advance .
However , East German authorities could refuse entry permits without stating a reason .
It was forbidden to export East German currency from the East , but money not spent could be left at the border for possible future visits .
However , East German authorities could still refuse entry permits .
Likewise , Soviet military patrols could enter and exit West Berlin .
For this reason , elaborate procedures were established to prevent inadvertent recognition of East German authority when engaged in travel through the GDR and when in East Berlin .
Regarding travel to East Berlin , such persons could also use the Friedrichstraße train station to enter and exit the city , in addition to Checkpoint Charlie .
During the years of the Wall , around 5,000 people successfully defected to West Berlin .
A historic research group at the Center for Contemporary Historical Research ( ZZF ) in Potsdam has confirmed 136 deaths .
The East German government issued shooting orders to border guards dealing with defectors , though such orders are not the same as " shoot to kill " orders .
GDR officials denied issuing the latter .
East German authorities no longer permitted apartments near the wall to be occupied , and any building near the wall had its windows boarded and later bricked up .
On August 15 , 1961 , Conrad Schumann was the first East German border guard to escape by jumping the barbed wire to West Berlin .
But a West German policeman intervened , firing his weapon at the East German border guards .
East Germans successfully defected by a variety of methods : digging long tunnels under the wall , waiting for favorable winds and taking a hot air balloon , sliding along aerial wires , flying ultralights , and in one instance , simply driving a sports car at full speed through the basic , initial fortifications .
The East Germans then built zig-zagging roads at checkpoints .
DDR-WOH is still flying today , but under the registration D-EWOH .
The guards often let fugitives bleed to death in the middle of this ground , as in the most notorious failed attempt , that of Peter Fechter ( aged 18 ) .
Fechter 's death created negative publicity worldwide that led the leaders of East Berlin to place more restrictions on shooting in public places , and provide medical care for possible " would-be escapers " .
The last person to be shot while trying to cross the border was Chris Gueffroy on February 6 , 1989 .
These East Germans flooded the West German embassy and refused to return to East Germany .
The East German government responded by disallowing any further travel to Hungary , but allowed those already there to return .
This triggered a similar incident in neighboring Czechoslovakia .
On this occasion , the East German authorities allowed them to leave , providing that they used a train which transited East Germany on the way .
This was followed by mass demonstrations within East Germany itself .
( See Monday demonstrations in East Germany . )
The longtime leader of East Germany , Erich Honecker , resigned on October 18 , 1989 , and was replaced by Egon Krenz a few days later .
Honecker had predicted in January of that year that the wall would stand for a " hundred more years " if the conditions which had caused its construction did not change .
Meanwhile the wave of refugees leaving East Germany for the West had increased and had found its way through Hungary via Czechoslovakia , tolerated by the new Krenz government and in agreement with the communist Czechoslovak government .
To ease the complications , the politburo led by Krenz decided on November 9 , to allow refugees to exit directly through crossing points between East Germany and West Germany , including West Berlin .
Günter Schabowski , a spokesperson for the politburo , had the task of announcing this ; however he had not been involved in the discussions about the new regulations and had not been fully updated .
These regulations had only been completed a few hours earlier and were to take effect the following day , so as to allow time to inform the border guards -- however , nobody had informed Schabowski .
After further questions from journalists he confirmed that the regulations included the border crossings towards West Berlin , which he had not mentioned until then .
" East Germany has announced that , starting immediately , its borders are open to everyone . "
After hearing the broadcast , East Germans began gathering at the wall , demanding that border guards immediately open its gates .
The surprised and overwhelmed guards made many hectic telephone calls to their superiors about the problem , but it became clear that there was no one among the East German authorities who would dare to take personal responsibility for issuing orders to use lethal force , so there was no way for the vastly outnumbered soldiers to hold back the huge crowd of East German citizens .
November 9 is considered the date the Wall fell , but the Wall in its entirety was not torn down immediately .
New border crossings continued to be opened through the middle of 1990 , including the Brandenburg Gate on December 22 , 1989 .
Until then they could only visit East Germany and East Berlin under restrictive conditions that involved application for a visa several days or weeks in advance , and obligatory exchange of at least 25 DM per day of their planned stay , all of which hindered spontaneous visits .
Television coverage of citizens demolishing sections of the wall on the evening of November 9 , and the new border crossings opened weeks later , led some foreigners to think the Wall was torn down quickly .
Technically , the Wall remained guarded for some time after November 9 , though at a decreasing intensity .
In the first months , the East German military even tried to repair some of the damages done by the " wall peckers " .
On June 13 , 1990 , the official dismantling of the Wall by the East German military began in Bernauer Straße .
On July 1 , the day East Germany adopted the West German currency , all de jure border controls ceased , although the inter-German border had become meaningless for some time before that .
The dismantling continued to be carried out by military units ( after unification under the Bundeswehr ) and lasted until November 1991 .
The fall of the Wall was the first step toward German reunification , which was formally concluded on October 3 , 1990 .
David Hasselhoff performed his song " Looking for Freedom " , which was very popular in Germany at that time , standing on the Berlin wall .
Nobel Laureate Elie Wiesel criticized the first euphoria , noting that " they forgot that Nov. 9 has already entered into history -- 51 years earlier it marked the Kristallnacht . "
As reunification was not official and complete until October 3 ; that day was finally chosen as German Unity Day .
A high point was when over 1000 colorfully designed foam domino tiles , each over 8 feet tall , that were stacked along the former route of the wall in the city center were toppled in stages , converging in front of the Brandenburg Gate .
A Berlin Twitter Wall was set up to allow Twitter users to post messages commemorating the 20th anniversary .
Twenty symbolic wall bricks were sent from Berlin starting in May 2009 .
The MTV Europe Music Awards , on the 5th of November , had U2 and Tokio Hotel perform songs dedicated to , and about the Berlin Wall .
The Trabant was the East German people 's car that many used to leave DDR after the collapse .
The stamps splendidly illustrate that even twenty years on , veterans of service in Berlin still regard their service there as one of the high points of their lives .
Little is left of the Wall at its original site , which was destroyed almost everywhere .
None still accurately represents the Wall 's original appearance .
Fragments of the Wall were taken and some were sold around the world .
Appearing both with and without certificates of authenticity , these fragments are now a staple on the online auction service eBay as well as German souvenir shops .
Today , the eastern side is covered in graffiti that did not exist while the Wall was guarded by the armed soldiers of East Germany .
In most places only the " first " wall is marked , except near Potsdamer Platz where the stretch of both walls is marked , giving visitors an impression of the dimension of the barrier system .
Fifteen years after the fall , a private museum rebuilt a 200-metre ( 656 ft ) section close to Checkpoint Charlie , although not in the location of the original wall .
They temporarily erected more than 1,000 crosses in memory of those who died attempting to flee to the West .
A poll taken in October 2009 on the occasion of the 20th anniversary of the fall of the wall indicated , however , that only about a tenth of the population was still unhappy with the unification ( 8 percent in the East ; 12 percent in the West ) .
Ten percent of people surveyed thought Berlin residents built it themselves .
Fifty-eight percent said they did not know who built it , with just 24 percent correctly naming the Soviet Union and its then-communist ally East Germany .
They can be found , for instance in presidential and historical museums , lobbies of hotels and corporations , at universities and government buildings , including the Pentagon near Washington , DC .
