In academic literature , the practice was first documented by Judith Donath ( 1999 ) .
Donath 's paper outlines the ambiguity of identity in a disembodied " virtual community " such as Usenet :
Donath provides a concise overview of identity deception games which trade on the confusion between physical and epistemic community :
For example , a New York Times article discussed troll activity at the /b/ board on 4chan and at Encyclopedia Dramatica , which it described as " an online compendium of troll humor and troll lore " .
These websites include but are not limited to ; Myspace , Facebook , Youtube , Stickam , and Chatroulette .
On March 31 , 2010 , the Today Show ran a segment detailing the deaths of three separate adolescent girls and trolls ' subsequent reactions to their deaths .
In the wake of these events , Facebook responded by strongly urging administrators to be aware of ways to ban users and remove inappropriate content from Facebook pages .
The case of Zeran v. America Online , Inc. resulted primarily from trolling .
