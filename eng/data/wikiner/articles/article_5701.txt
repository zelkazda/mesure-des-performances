The game is popular among both males and females in many parts of the world , particularly in Europe , Asia , Australia , New Zealand and South Africa .
The governing body is the 116-member International Hockey Federation ( FIH ) .
While current field hockey appeared in the mid-18th century in England , primarily in schools , it was not until the first half of the 19th century that it became firmly established .
The first club was created in 1849 at Blackheath in south-east London .
Field hockey is the national sport of India and Pakistan .
The game is played all over North America , Europe and in many other countries around the world to varying extent .
It is the most popular sport in Canada , Finland , Latvia , the Czech Republic , and in Slovakia .
The governing body is the 66-member International Ice Hockey Federation ( IIHF ) .
Women 's ice hockey was added to the Winter Olympics in 1998 .
North America 's National Hockey League ( NHL ) is the strongest professional ice hockey league , drawing top ice hockey players from around the globe .
The NHL rules are slightly different from those used in Olympic ice hockey : the periods are 20 minutes long , counting downwards .
There are early representations and reports of ice hockey-type games being played on ice in the Netherlands , and reports from Canada from the beginning of the nineteenth century , but the modern game was initially organized by students at McGill University , Montreal in 1875 who , by two years later , codified the first set of ice hockey rules and organized the first teams .
The rules are very similar to IIHF ice hockey rules .
Canada is a recognized international leader in the development of the sport , and of equipment for players .
Much of the equipment for the sport was first developed in Canada , such as sledge hockey sticks laminated with fiberglass , as well as aluminum shafts with hand carved insert blades and special aluminum sledges with regulation skate blades .
