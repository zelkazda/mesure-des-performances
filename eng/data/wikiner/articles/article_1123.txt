Operating Systems : Design and Implementation and MINIX were Linus Torvalds ' inspiration for the Linux kernel .
In his autobiography Just For Fun , Torvalds describes it as " the book that launched me to new heights " .
The Amsterdam Compiler Kit is a toolkit for producing portable compilers .
It was started sometime before 1981 , and Andrew Tanenbaum was the architect from the start until version 5.5 .
Within three months , a USENET newsgroup , comp.os.minix , had sprung up with over 40,000 readers discussing and improving the system .
On October 5 , 1991 , Torvalds announced his own ( POSIX like ) kernel , called Linux , which originally used the MINIX file system but is not based on MINIX code .
Although MINIX and Linux have diverged , MINIX continues to be developed , now as a production system as well as an educational one .
MINIX 3 , as the current version is called , is available under the BSD license for free at www.minix3.org .
He stated that he created the site as an American who " knows first hand what the world thinks of America and it is not a pretty picture at the moment .
I want people to think of America as the land of freedom and democracy , not the land of arrogance and blind revenge .
I want to be proud of America again . "
After 7 months of legal battling , Al Franken won this race by 312 votes ( 0.01 % ) .
The award was given in the academic senate chamber , after which Tanenbaum gave a lecture on his vision of the future of the computer field .
Tanenbaum has been keynote speaker at numerous conferences , most recently
