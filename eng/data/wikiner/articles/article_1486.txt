Bretwalda , also brytenwalda and bretenanwealda , is an Anglo-Saxon word , the first record of which comes from the late 9th century Anglo-Saxon Chronicle .
Kemble , however , derives bretwalda from the Old English word breotan ( Modern English : to distribute ) , which he translates as " widely ruling " .
The chronicler also wrote down the names of seven kings Bede had listed in his Historia ecclesiastica gentis Anglorum in 731 .
Bede wrote in Latin and never used the term , and his list of kings holding imperium should be treated with great caution , not least in that he overlooks kings such as Penda of Mercia who clearly held some kind of dominance in their time .
This shows that the concept of the overlordship of the whole of Britain was at least recognised in the period , whatever was meant by the term .
The fact that Bede never mentioned a special title for the kings in his list implies that he was unaware of one .
