The term is no longer commonly used , as it connotes the " orientalism " of the 19th century as described by Edward Said .
This combination of cultural and geographic subjectivism was well illustrated in 1939 by the Prime Minister of Australia , R. G Menzies .
