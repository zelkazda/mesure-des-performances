The government 's policy of offering incentives to high-technology companies and financial institutions to locate on the Island has expanded employment opportunities in high-income industries .
Trade is mostly with the U.K. .
The Manx government also promotes island locations for making films by contributing to the production costs .
Exports -- partners : UK
Imports -- partners : UK
