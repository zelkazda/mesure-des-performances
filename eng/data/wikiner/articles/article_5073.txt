It is mentioned in dance manuals from England , France , Spain , Germany , and Italy , among others .
For example , 16th century Italian dances in Fabritio Caroso 's ( 1581 ) and Cesare Negri 's ( 1602 ) dance manuals often have a galliard section .
These steps are found in Negri 's manual , and involve a galliard step ending with a 180 degree or 360 degree spin , during which the dancer kicks out to kick a tassel suspended between knee and waist height .
