The Free Software Definition , written by Richard Stallman and published by Free Software Foundation ( FSF ) , defines free software , as a matter of liberty , not price .
The canonical source for the document is in the philosophy section of the GNU Project website .
FSF publishes a list of licenses which meet this definition .
Despite the fundamental philosophical differences between the free software movement and the open source movement , the official definitions of free software by the Free Software Foundation and of open source software by the Open Source Initiative basically refer to the same software licenses , with a few minor exceptions .
