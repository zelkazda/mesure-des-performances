Deep Space 1 ( DS1 ) of the NASA New Millennium Program is a spacecraft dedicated to testing its payload of advanced , high risk technologies .
The Deep Space mission carried out a flyby of asteroid 9969 Braille which was selected as the mission 's science target .
Its mission was extended twice to include an encounter with comet Borrelly and further engineering testing .
Although ion engines had been developed at NASA since the late 1950s , with the exception of the SERT missions in the 1960s , the technology had not been demonstrated in flight .
It was a primary mission of the Deep Space 1 demonstration to show long duration use of an ion thruster on a science mission .
Deep Space 1 was the first use of ion engines on an operational science spacecraft .
Remote Agent ( remote intelligent self-repair software ) , developed at NASA Ames Research Center and JPL , was the first artificial intelligence control system to control a spacecraft without human supervision .
The flyby of the asteroid 9969 Braille was only a partial success .
Deep Space 1 was intended to perform the flyby at 56,000 km/h at only 240 meters from the asteroid .
Due to technical difficulties , including a software crash shortly before approach , the craft instead passed Braille at a distance of 26 km .
This , plus Braille 's lower albedo , meant that the asteroid was not bright enough for the autonav to focus the camera in the right direction , and the picture shoot was delayed by almost an hour .
Despite having no debris shields , the DS1 spacecraft survived the comet passage intact .
Deep Space 1 succeeded in its primary and secondary objectives including flybys of the asteroid Braille and of Comet Borrelly , returning valuable science data and images .
