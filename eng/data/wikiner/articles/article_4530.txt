Fealty comes from the Latin fidelitas and denotes the fidelity owed by a vassal to his feudal lord .
In the 20th century , the historian François-Louis Ganshof was very influential on the topic of feudalism .
Ganshof defined feudalism from a narrow legal and military perspective , arguing that feudal relationships existed only within the medieval nobility itself .
Bloch approached feudalism not so much from a legal and military point of view but from a sociological one .
Bloch conceived of feudalism as a type of society that was not limited solely to the nobility .
Like Ganshof , he recognized that there was a hierarchical relationship between lords and vassals , but Bloch saw as well a similar relationship obtaining between lords and peasants .
It is this radical notion that peasants were part of feudal relationship that sets Bloch apart from his peers .
According to Bloch , other elements of society can be seen in feudal terms ; all the aspects of life were centered on " lordship " , and so we can speak usefully of a feudal church structure , a feudal courtly ( and anti-courtly ) literature , and a feudal economy .
Karl Marx also used the term in political analysis .
In the 19th century , Marx described feudalism as the economic situation coming before the inevitable rise of capitalism .
For Marx , what defined feudalism was that the power of the ruling class ( the aristocracy ) rested on their control of arable land , leading to a class society based upon the exploitation of the peasants who farm these lands , typically under serfdom .
Marx thus considered feudalism within a purely economic model .
The following are historical examples given by Susan Reynolds that call into question the traditional use of the term feudalism :
The system of land tenure in Scotland was until recently overwhelmingly feudal in nature .
Unique in England , the village of Laxton in Nottinghamshire continues to retain some vestiges of the feudal system , where the land is still farmed using the open field system .
