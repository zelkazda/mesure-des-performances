A hybrid configuration receiving new attention is the Doherty amplifier , invented in 1934 by William H. Doherty for Bell Laboratories ( whose sister company , Western Electric , was then an important manufacturer of radio transmitters ) .
In Doherty 's design , even with zero modulation , a transmitter could achieve at least 60 % efficiency .
The stages are split and combined through 90-degree phase shifting networks as in the Doherty amplifier .
