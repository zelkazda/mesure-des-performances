In some cases , corporations have redefined their core values in the light of business ethical considerations ( e.g. BP 's " beyond petroleum " environmental tilt ) .
The Bhopal disaster and the fall of Enron are instances of the major disasters triggered by bad corporate ethics .
It should be noted that the idea of business ethics caught the attention of academics , media and business firms by the end of the overt Cold War .
On the one side , following ideologists like Milton Friedman and Ayn Rand , it is argued that the only ethics in marketing is maximizing profit for the shareholder .
Cases : Benetton .
Cases : Ford Pinto scandal , Bhopal disaster , asbestos / asbestos and the law , Peanut Corporation of America .
John Rawls and Robert Nozick are both notable contributors .
Milton Friedman is the pioneer of the view .
The historical and global importance of religious views on business ethics is sometimes underestimated in standard introductions to business ethics according to Dr. Todd Albertson author of The Gods of Business book .
Instead of looking at the politics the corporate firms play in modifying rules of accounting practices , diluting labour laws , weakening regulatory mechanisms etc. , and it tends to look at the ethical collapse of firms like Enron and Arthur Andersen as if it were isolated instances of individuals slipping away from their ethical responsibilities .
