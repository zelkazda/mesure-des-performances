Chornobyl , or Chernobyl , is categorized as a city in northern Ukraine , in Kiev Oblast , near the border with Belarus .
The city was evacuated in 1986 due to the Chernobyl disaster at the Chernobyl Nuclear Power Plant , located 14.5 kilometers ( 9 miles ) north-northwest .
When the power plant was under construction , Prypiat , a city larger and closer to the power plant , had been built as home for the power plant workers .
Workers on watch and administrative personnel of the Zone of Alienation are stationed in the city on a long term basis .
The city of Slavutych was built specifically for the evacuated population of Chernobyl .
The city is named after the Ukrainian word for mugwort or wormwood , which is " chornobyl " .
It was a crown village of the Grand Duchy of Lithuania in the 13th century .
The village was granted as a fiefdom to Filon Kmita , a captain of the royal cavalry , in 1566 .
Chernobyl had a rich religious history .
The Chernobyl Hasidic dynasty had been founded by Rabbi Menachem Nachum Twersky .
Since the 1880s , Chernobyl has seen many changes of fortune .
In 1898 Chernobyl had a population of 10,800 , including 7,200 Jews .
From 1921 , it was incorporated into the Ukrainian SSR .
During the period 1929 -- 33 , Chernobyl suffered greatly from mass killings during Stalin 's collectivization campaign , and in the Holodomor ( famine ) that followed .
The Jewish community was murdered during the German occupation of 1941 -- 44 .
Twenty years later , the area was chosen as the site of the first nuclear power station on Ukrainian soil .
The Duga-3 over-the-horizon radar array several miles out of Chernobyl was the origin of the infamous Russian Woodpecker , designed as part of Russia 's anti-ballistic missile early warning radar network .
With the collapse of the Soviet Union in 1991 , Chernobyl remained part of Ukraine , now an independent nation .
The explosion took place at around one in the morning while the neighboring town of Pripyat slept .
In 2003 , the United Nations Development Programme launched a project called the Chernobyl Recovery and Development Programme for the recovery of the affected areas .
Chernobylite is the name cited by two media sources for highly radioactive , unusual and potentially novel crystalline formations found at the Chernobyl power-plant after the explosion .
