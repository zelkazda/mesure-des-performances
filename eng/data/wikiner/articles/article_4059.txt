EDGE is considered a 3G radio technology and is part of ITU 's 3G definition .
EDGE was deployed on GSM networks beginning in 2003 -- initially by Cingular in the United States .
EDGE can be used for any packet switched application , such as an Internet connection .
EDGE requires no hardware or software changes to be made in Global System for Mobile Communications core networks .
This effectively triples the gross data rate offered by GSM .
EDGE is part of ITU 's 3G definition and is considered a 3G radio technology .
