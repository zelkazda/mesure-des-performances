Anthropologist John Tooby and psychologist Leda Cosmides note :
EP is , to quote Steven Pinker , " not a single theory but a large set of hypotheses " and a term which " has also come to refer to a particular way of applying evolutionary theory to the mind , with an emphasis on adaptation , gene-level selection , and modularity . "
EP uses Nikolaas Tinbergen 's four categories of questions and explanations of animal behavior .
Firstly , the study of animal social behavior ( including humans ) generated sociobiology , defined by its pre-eminent proponent Edward O. Wilson in 1975 as " the systematic study of the biological basis of all social behavior " and in 1978 as " the extension of population biology and evolutionary theory to social organization " .
Those proposed by David Buss state that :
Thus , the type of selective process that is involved here is what Darwin called " sexual selection " .
Inclusive fitness theory , which was proposed by William D. Hamilton in 1964 as a revision to evolutionary theory , is essentially a combination of natural selection , sexual selection , and kin selection .
The term environment of evolutionary adaptedness was coined by John Bowlby as part of attachment theory .
According to Paul Baltes , the benefits granted by evolutionary selection decrease with age .
After his seminal work in developing theories of natural selection , Charles Darwin devoted much of his final years to the study of animal emotions and psychology .
He wrote two books; The Descent of Man , and Selection in Relation to Sex in 1871 and The Expression of the Emotions in Man and Animals in 1872 that dealt with topics related to evolutionary psychology .
Darwin pondered why humans and animals were often generous to their group members .
Darwin felt that acts of generosity decreased the fitness of generous individuals .
Darwin concluded that while generosity decreased the fitness of individuals , generosity would increase the fitness of a group .
Darwin anticipated evolutionary psychology with this quote from the Origin of Species :
According to Noam Chomsky , the founder of evolutionary psychology might have been Peter Kropotkin , who argued in his 1902 book Mutual Aid : A Factor of Evolution for the evolutionary benefits of behavioral traits related to mutual aid .
While Darwin 's theories on natural selection gained acceptance in the early part of the 20th century , his theories on evolutionary psychology were largely ignored .
Wilson included a chapter on human behavior .
E O Wilson argues that the field of evolutionary psychology is essentially the same as sociobiology .
According to Wilson , the heated controversies surrounding Sociobiology:The New Synthesis , significantly stigmatized the term " sociobiology " .
