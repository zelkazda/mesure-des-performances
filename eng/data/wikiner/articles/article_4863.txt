The economy of Greece is the twenty-seventh largest economy in the world by nominal gross domestic product ( GDP ) and the thirty-third largest by purchasing power parity , according to the data given by the International Monetary Fund for the year 2008 .
The Greek economy is a developed economy with the 22nd highest standard of living in the world .
The evolution of the Greek economy during the 19th century has been little researched .
Official Eurostat calculation according to the current methodology is still pending for the 1999 budget deficit ( entry application reference year ) ; according to the same calculations , the budget deficit criterions were met in 2006 .
According to Eurostat data , GDP per inhabitant in purchasing power standards ( PPS ) stood at 95 per cent of the EU average in 2008 .
However , the Greek economy also faces significant problems , including rising unemployment levels , inefficient bureaucracy , tax evasion and corruption .
The country suffers from high levels of political and economic corruption and low global competitiveness compared to its EU partners .
By the end of 2009 , as a result of a combination of international ( financial crisis ) and local ( uncontrolled spending prior to the October 2009 national elections ) factors , the Greek economy faced its most severe crisis since 1993, [ citation needed ] with the second highest budget deficit as well as the second highest debt to GDP ratio in the EU .
The CEE Council has argued that the predicament some mainland EU countries find themselves in today is the result of a combination of factors , including over-expansion of the eurozone , and a combination of monetarist policy , tax evasion , pursued by local policy makers and EU central bankers .
The IMF had said it was " prepared to move expeditiously on this request " .
Standard & Poor 's estimates that in the event of default investors would lose 30 -- 50 % of their money .
As of July 19 , 2010 reports on evolutions in the Greek economy have been modestly positive , citing a 46 % budget deficit reduction in the first half of 2010 , major labor and pension system reforms and early indications of a recession milder than originally feared .
