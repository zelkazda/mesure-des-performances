This resulted in the delivery of eight MiG-21PFMs and a MiG-21U in 1986 to replace the remaining MiG-17s .
Today the air force 's personnel total about 700 ; its equipment includes several Russian-supplied transport planes .
Upon independence in 1958 , France cut all ties and immediately began to repatriate Guinean soldiers serving in the French Army .
This followed military dissatisfaction over the creation of a PDG control element in each army unit .
The army resisted the Portuguese invasion of Guinea in November 1970 .
The last Guinean troops were withdrawn in 1974 .
In early 1975 the Guinean military consisted of an army of around 5,000 , an air force of 300 , and a naval component of around 200 .
In the early 1970s the armed forces were organised into four military zones , corresponding to the four geographical regions ( Lower Guinea , Middle Guinea , Upper Guinea , and Forest Guinea ) .
The engineer battalion had companies in Conakry , Kankan , and Boké , and was engaged in constructing and repairing buildings and roads .
Increasing mistrust of the regular armed forces after the Labé plot led to the militia assuming greater importance .
The militia had grown out of a 1961 Democratic Party of Guinea ( PDG ) decision to create workplace ' committees for the defence of the revolution . '
The elements in the Conakry area were issued small arms and given military training .
Thus the militia was re-organised in multiple tiers , with a staff in Conakry , some combat units , and the remainder of the permanent element serving as a cadre for reserve militia units in villages , industrial sites , and schools .
Conté had to suppress his first revolt in July 1985 , by his immediate deputy , Colonel Diarra Traoré .
Conté , ' civilianised ' since a rigged election in 1993 , had to make significant concessions in order to save his regime .
