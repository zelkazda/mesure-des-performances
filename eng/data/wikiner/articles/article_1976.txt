Following the end of the Khmer Empire the language lost the standardizing influence of being the language of government and accordingly underwent a turbulent period of change in morphology , phonology and lexicon .
As such , its closest relatives are the languages of the Pearic , Bahnaric , and Katuic families spoken by the hill tribes of the region .
However , the colloquial Phnom Penh dialect has developed a marginal tonal contrast ( a level vs. a peaking tone ) to compensate for the elision of /r/ .
Classifying particles for use between numerals and nouns exist although are not always obligatory as in , for example , Thai .
This is due to its distinct accent influenced by the surrounding tonal language , Thai , lexical differences and its phonemic differences in both vowels and distribution of consonants .
A notable characteristic of Phnom Penh casual speech is merging or complete elision of syllables , considered by speakers from other regions as a " relaxed " pronunciation .
Another characteristic of Phnom Penh speech is observed in words with an " r " either as an initial consonant or as the second member of a consonant cluster .
