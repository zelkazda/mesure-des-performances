-esse replaced Old English-icge .
Goddess is attested in Middle English from 1350 .
Each of the three goddesses had a separate shrine near Mecca .
Ushas is the main goddess of the Rigveda .
Such aspects of one god as male god ( Shaktiman ) and female energy , working as a pair are often envisioned as male gods and their wives or consorts and provide many analogues between passive male ground and dynamic female energy .
For example , Brahma pairs with Sarasvati .
Shiva likewise pairs with Parvati who later is represented through a number of avatars ( incarnations ) : Sati and the warrior figures , Durga and Kali .
Lilith ran into the wilderness in despair .
Lilith then takes the form of the serpent in her jealous rage at being displaced as Adam 's wife .
It is worthwhile to note here that in religions pre-dating Judaism , the serpent was known to be associated with wisdom and re-birth ( with the shedding of its skin ) .
For example , Campbell states that , " There have been systems of religion where the mother is the prime parent , the source ...
We talk of Mother Earth .
