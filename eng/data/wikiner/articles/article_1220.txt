British Columbia i i i / r ɪ t ɪ ʃ k ə ˈ l ʌ m b ɪ ə / ( BC )
In 1871 , it became the sixth province of Canada .
The capital of British Columbia is Victoria , the fifteenth largest metropolitan region in Canada .
In 2009 , British Columbia had an estimated population of 4,419,974 ( about two million of whom were in Metro Vancouver ) .
British Columbia 's land area is 944735 square kilometres ( 364800 sq mi ) .
British Columbia 's rugged coastline stretches for more than 27000 kilometres ( 17000 mi ) , and includes deep , mountainous fjords and about six thousand islands , most of which are uninhabited .
British Columbia 's capital is Victoria , located at the southeastern tip of Vancouver Island .
The province 's most populous city is Vancouver , which is not on Vancouver Island but rather is located in the southwest corner of the mainland ( an area often called the Lower Mainland ) .
Other major cities include Surrey , Burnaby , Coquitlam , Richmond , Delta , and New Westminster in the Lower Mainland ; Abbotsford , Pitt Meadows , Maple Ridge and Langley in the Fraser Valley ; Nanaimo on Vancouver Island ; and Kelowna and Kamloops in the Interior .
Prince George is the largest city in the northern part of the province , while a village northwest of it , Vanderhoof , is near the geographic centre of the province .
The Coast Mountains and the Inside Passage 's many inlets provide some of British Columbia 's renowned and spectacular scenery , which forms the backdrop and context for a growing outdoor adventure and ecotourism industry .
The Okanagan area is one of three wine-growing regions in Canada and also produces excellent ciders .
Much of the western part of Vancouver Island and the rest of the coast is covered by temperate rainforest .
The province 's mainland away from the coastal regions is not as moderated by the Pacific Ocean and ranges from desert and semi-arid plateau to the range and canyon districts of the interior plateau .
12.5 % ( 114000 km 2 ( 44000 sq mi ) ) of British Columbia is currently considered protected under one of the 14 different designations that includes over 800 distinct areas .
British Columbia 's provincial parks system is the second largest parks system in Canada .
British Columbia 's cold winters can be severe in the interior and the north .
Besides the cold , British Columbia can also have hot , dry summers in the interior and on the south coast often .
For example , in Okanagan temperatures can climb to 35 degrees sometimes , and not often over 40 degrees .
It is the second highest ever recorded in Canada .
On the south coast , British Columbia is very mild and wet , moderated by the ocean and the coastal mountains .
The discovery of stone tools on the Beatton River near Fort St. John date human habitation in British Columbia to at least 11,500 years ago .
Mackenzie and these other explorers -- notably John Finlay , Simon Fraser , Samuel Black , and David Thompson -- were primarily concerned with extending the fur trade , rather than political considerations .
In 1794 , by the third of a series of agreements known as the Nootka Conventions , Spain conceded its claims of exclusivity in the Pacific .
This co-occupancy was ended with the Oregon Treaty of 1846 .
The major supply route was the York Factory Express between Hudson Bay and Fort Vancouver .
Among the places in British Columbia that began as fur trading posts are Fort St. John ( established 1794 ) ; Hudson 's Hope ( 1805 ) ; Fort Nelson ( 1805 ) ; Fort St. James ( 1806 ) ; Prince George ( 1807 ) ; Kamloops ( 1812 ) ; Fort Langley ( 1827 ) ; Fort Victoria ( 1843 ) ; Yale ( 1848 ) ; and Nanaimo ( 1853 ) .
Fur company posts that became cities in what is now the United States include Vancouver , Washington ( Fort Vancouver ) , formerly the " capital " of Hudson 's Bay operations in the Columbia District , Colville , Washington and Walla Walla , Wash. ( old Fort Nez Percés ) .
With the amalgamation of the two fur trading companies in 1821 , the region now comprising British Columbia existed in three fur trading departments .
The bulk of the central and northern interior was organized into the New Caledonia district , administered from Fort St. James .
The northeast corner of the province east of the Rockies , known as the Peace River Block , was attached to the much larger Athabasca District , headquartered in Fort Chipewyan , in present day Alberta .
Until 1849 , these districts were a wholly unorganized area of British North America under the de facto jurisdiction of HBC administrators .
Unlike Rupert 's Land to the north and east , however , the territory was not a concession to the company .
The Colony of Vancouver Island was created in 1849 , with Victoria designated as the capital .
With the Fraser Canyon Gold Rush in 1858 , an influx of Americans into New Caledonia prompted the colonial office to formally designate the mainland as the Colony of British Columbia , with New Westminster as its capital .
The Vancouver Island colony was facing financial crises of its own , and pressure to merge the two eventually succeeded in 1866 .
Several factors motivated this agitation , including the fear of annexation to the United States , the overwhelming debt created by rapid population growth , the need for government-funded services to support this population , and the economic depression caused by the end of the gold rush .
The borders of the province were not completely settled until 1903 , however , when the province 's territory shrank somewhat after the Alaska boundary dispute settled the vague boundary of the Alaska Panhandle .
Population in British Columbia continued to expand as the province 's mining , forestry , agriculture , and fishing sectors were developed .
Agriculture attracted settlers to the fertile Fraser Valley , and cattle ranchers and later fruit growers came to the drier grasslands of the Thompson River area , the Cariboo , the Chilcotin , and the Okanagan .
The booming logging town of Granville , near the mouth of the Burrard Inlet was selected as the terminus of the railway , prompting the incorporation of the community as Vancouver in 1886 .
By 1923 , almost all Chinese immigration had been blocked except for merchants and investors
In 1914 , the last spike of a second transcontinental rail line , the Grand Trunk Pacific , linking north-central British Columbia from the Yellowhead Pass through Prince George to Prince Rupert was driven at Fort Fraser .
This opened up the north coast and the Bulkley Valley region to new economic opportunities .
These formed the basis of the fractured labour-political spectrum that would generate a host of fringe leftist and rightist parties , including those who would eventually form the Co-operative Commonwealth and the early Social Credit splinter groups .
Much of Vancouver 's prosperity and opulence in the 1920s results from this " pirate economy " , although growth in forestry , fishing and mining continued .
Increasingly desperate times led to intense political organizing efforts , an occupation of the main Post Office at Granville and Hastings which was violently put down by the police and an effective imposition of martial law on the docks for almost three years .
While the Liberals won the most number of seats , they actually received fewer votes than the socialist Co-operative Commonwealth Federation ( CCF ) .
The CCF was invited to join the coalition but refused .
The pretext for continuing the coalition after the end of World War II was to prevent the CCF , which had won a surprise victory in Saskatchewan in 1944 , from ever coming to power in British Columbia .
During his tenure , major infrastructure continued to expand , and the agreement with Alcan to build the Kemano -- Kitimat hydro and aluminum complex was put in place .
With the election of the Social Credit Party , British Columbia embarked a phase of rapid economic development .
Bennett and his party governed the province for the next twenty years , during which time the government initiated an ambitious programme of infrastructure development , fuelled by a sustained economic boom in the forestry , mining , and energy sectors .
Vancouver and Victoria become cultural centres as poets , authors , artists , musicians , as well as dancers , actors , and haute cuisine chefs flocked to the beautiful scenery and warmer temperatures .
The rise of Japan and other Pacific economies was a great boost to British Columbia 's economy .
Tensions emerged , also , from the counterculture movement of the late 1960s , of which Vancouver and Nanaimo were centres .
By the end of the decade , with social tensions and dissatisfaction with the status quo rising , the Bennett government 's achievements could not stave off its growing unpopularity .
On August 27 , 1969 , the Social Credit Party was re-elected in a general election for what would be Bennett 's final term in power .
However , BC Hydro reported its first loss , which was the beginning of the end for Bennett and the Social Credit Party .
The Coquihalla Highway project became the subject of a scandal after revelations that the premier 's brother bought large tracts of land needed for the project before it was announced to the public , and also because of graft investigations of the huge cost overruns on the project .
As the province entered a sustained recession , Bennett 's popularity and media image were in decline .
Tens of thousands participated in protests and many felt that a general strike would be the inevitable result unless the government backed down from its policies they had claimed were only about restraint and not about recrimination against the NDP and the left .
A tense winter of blockades at various job sites around the province ensued , as among the new laws were those enabling non-union labour to work on large projects and other sensitive labour issues , with companies from Alberta and other provinces brought in to compete with union-scale British Columbia companies .
These scandals forced Vander Zalm 's resignation , and Rita Johnston became premier of the province .
The NDP 's unprecedented creation of new parkland and protected areas was popular , and helped boost the province 's growing tourism sector .
Harcourt was not directly implicated , but he resigned nonetheless in respect of constitutional conventions calling for leaders under suspicion to step aside .
Glen Clark , a former president of the BC Federation of Labour , was chosen the new leader of the NDP , which won a second term in 1996 .
More scandals dogged the party , most notably the Fast Ferry Scandal involving the province trying to develop the shipbuilding industry in British Columbia .
He was succeeded on an interim basis by Dan Miller who was in turn followed by Ujjal Dosanjh .
In the 2001 general election Gordon Campbell 's BC Liberals defeated the NDP party , gaining 77 out of 79 seats total seats in the provincial legislature .
Campbell instituted various reforms and removed some of the NDP 's policies including scrapping the " fast ferries " project , lowering income taxes , and the controversial sale of BC Rail to CN Rail .
However , Campbell still managed to lead his party to victory in the 2005 general election , against a substantially strengthened NDP opposition .
Campbell won a third term in the British Columbia general election , 2009 , marking the first time in 23 years that a premier has been elected to a third term .
British Columbia has also been significantly affected by demographic changes within Canada and around the world .
Vancouver ( and to a lesser extent some other parts of British Columbia ) was a major destination for many of the immigrants from Hong Kong who left the former UK colony ( either temporarily or permanently ) in the years immediately prior to its handover to the People 's Republic of China .
Another 15.2 % refer to their ethnicity as " Irish " , though not distinguishing between Northern Ireland and the Republic of Ireland .
Of the provinces , British Columbia had the highest proportion of visible minorities , representing 24.8 % of its population .
British Columbia has a resource dominated economy , centred on the forestry industry but also with increasing importance in mining .
The economic history of British Columbia is replete with tales of dramatic upswings and downswings , and this boom and bust pattern has influenced the politics , culture and business climate of the province .
Transportation played a major role in British Columbia history .
The Rocky Mountains and the ranges west of them constituted a significant obstacle to overland travel until the completion of the transcontinental railway in 1885 .
Fur trade routes were only marginally used for access to British Columbia through the mountains .
Nearly all travel and freight to and from the region occurred via the Pacific Ocean , primarily through the ports of Victoria and New Westminster .
Because of its size and rugged , varying topography , British Columbia requires thousands of kilometres of provincial highways to connect its communities .
British Columbia 's roads systems were notoriously poorly maintained and dangerous until a concentrated programme of improvement was initiated in the 1950s and 1960s .
From south to north they are : BC Highway 3 through the Crowsnest Pass , the Vermilion Pass and the Kicking Horse Pass , the latter being used by the Trans-Canada Highway entering Alberta through Banff National Park , the Yellowhead Highway through Jasper National Park , and Highway 2 through Dawson Creek .
The longest highway is Highway 97 , running 2081 km ( 1293 mi ) from the British Columbia-Washington border at Osoyoos north to Watson Lake , Yukon and which includes the British Columbia portion of the Alaska Highway .
Prior to 1978 , surface public transit was administered by BC Hydro , the provincially owned electricity utility .
Subsequently , the province established BC Transit to oversee and operate all municipal transportation systems .
In 1998 , the Greater Vancouver Transportation Authority ( TransLink ) ( now South Coast British Columbia Transportation Authority ) , a separate authority for routes within the Greater Vancouver Regional District ( now Metro Vancouver ) , was established .
Public transit in British Columbia consists mainly of diesel buses , although Vancouver is also serviced by a fleet of trolleybuses .
Presently , extensions of the line east to Coquitlam and Port Moody ( the Evergreen Line ) are being developed .
Rail development expanded greatly in the decades after the completion of the Canadian Pacific Railway in 1885 and was the chief mode of long-distance surface transportation until the expansion and improvement of the provincial highways system began in the 1950s .
The E & N Railway , rebranded as Southern Railway of Vancouver Island , serves the commercial and passenger train markets of Vancouver Island by owning the physical rail lines .
Passenger train service on Vancouver Island is operated by VIA Rail .
BC Ferries was established as a provincial crown corporation in 1960 to provide passenger and vehicle ferry service between Vancouver Island and the Lower Mainland as a cheaper and more reliable alternative to the service operated by the Canadian Pacific Railway .
Major ports are located at Vancouver , Roberts Bank ( near Tsawwassen ) , Prince Rupert , and Victoria .
Vancouver , Victoria , and Prince Rupert are also major ports of call for cruise ships .
In 2007 , a large maritime container port was opened in Prince Rupert with an inland sorting port located in Prince George .
There are over 200 airports located throughout British Columbia , the major ones being the Vancouver International Airport , the Victoria International Airport , the Kelowna International Airport , and the Abbotsford International Airport , the first three of which each served over 1,000,000 passengers in 2005 .
Vancouver International Airport is the 2nd busiest airport in the country with an estimated 17.9 million travellers passing through in 2008 .
The Lieutenant-Governor of British Columbia , Steven Point , is the Queen of Canada 's representative in the Province of British Columbia .
In practice , this is usually the Chief Justice of British Columbia .
Currently , the province is governed by the Liberal Party under Premier Gordon Campbell .
Campbell had previously led the largest landslide election in British Columbia history in 2001 , with 77 of 79 seats , but the legislature has been more evenly divided between Liberals and NDP following the 2005 ( 46 of 79 ) and 2009 ( 49 of 85 ) provincial elections .
Historically , there have commonly been third parties present in the legislature ( including the Liberals themselves from 1952 to 1975 ) , but there are presently none .
Prior to the rise of the Liberal Party , British Columbia 's main political party was the British Columbia Social Credit Party which ruled British Columbia for 20 continuous years .
While sharing some ideology with the current Liberal government , they were more right-wing although undertook nationalization of various important monopolies , notably BC Hydro and BC Ferries .
In an April 2008 poll by polling firm Ipsos-Reid , the BC Liberals were shown as having the support of 49 % of voters , compared to 32 % for the NDP .
British Columbia is known for having politically active labour unions who have traditionally supported the NDP or its predecessor , the CCF .
British Columbia 's political history is typified by scandal and a cast of colourful characters , beginning with various colonial-era land scandals and abuses of power by early officials ( such as those that led to McGowan 's War in 1858 -- 59 ) .
Also in the metropolitan area but not represented in the regional district are the University Endowment Lands .
The second largest concentration of British Columbia population is located at the southern tip of Vancouver Island , which is made up of the 13 municipalities of Greater Victoria , Victoria , Saanich , Esquimalt , Oak Bay , View Royal , Highlands , Colwood , Langford , Central Saanich / Saanichton , North Saanich , Sidney , Metchosin , Sooke , which are part of the Capital Regional District .
Almost half of the Vancouver Island population is located in Greater Victoria .
Besides salmon and trout , sport-fishers in B.C. also catch halibut , steelhead , bass , and sturgeon .
Environment Canada subdivides British Columbia into a system of 6 ecozones :
Boreal Plains Ecozones .
Given its varied mountainous terrain and its coasts , lakes , rivers , and forests , British Columbia has long been enjoyed for pursuits like hiking and camping , rock climbing and mountaineering , hunting and fishing .
Sea kayaking opportunities abound on the British Columbia coast with its fjords .
The 2010 Winter Olympics downhill events were held in Whistler Blackcomb area of the province , while the indoor events were conducted in the Vancouver area .
In Vancouver and Victoria ( as well as some other cities ) , opportunities for joggers and bicyclists have been developed .
British Columbia has produced many outstanding athletes , especially in aquatic and winter sports .
A number of British Columbia farmers offer visitors to combine tourism with farm work , e.g. through the WWOOF Canada program .
With the actual growing of marijuana , British Columbia is responsible for 40 % of all cannabis produced in Canada .
