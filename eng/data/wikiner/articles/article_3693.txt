Politics of Estonia takes place in a framework of a parliamentary representative democratic republic , whereby the Prime Minister of Estonia is the head of government , and of a multi-party system .
Executive power is exercised by the Government which is led by the Prime Minister .
Estonian civil service is relatively young .
Estonia has a relatively low number of bureaucrats , 18,998 in the central government and 4500 in local governments .
The Estonian Declaration of Independence was issued in 1918 .
A parliamentary republic was formed by the Estonian Constituent Assembly and the first Constitution of Estonia was adopted on June 15 , 1920 .
In 1940 Estonia was occupied by the Soviet Union .
A year later , the Soviet occupation was taken over by a Nazi German one .
In 1991 the Republic of Estonia was restored on the basis of continuity with the constitution prior to 1938 , with the public approving a new constitution in 1992 .
Only Estonian citizens may participate in parliamentary elections .
Estonia uses a voting system based on proportional representation .
Estonia does not have a state church , religious freedom is guaranteed by the constitution .
On June 28 , 1992 , Estonian voters approved the constitutional assembly 's draft constitution and implementation act , which established a parliamentary government with a president as chief of state and with a government headed by a prime minister .
He chose 32-year-old historian and Christian Democratic Party founder Mart Laar as prime minister .
In 1996 , Estonia ratified a border agreement with Latvia and completed work with Russia on a technical border agreement .
President Meri was re-elected in free and fair indirect elections in August and September in 1996 .
In fall 2001 Arnold Rüütel became the President of the Republic of Estonia .
In January 2002 Prime Minister Laar stepped down and President Rüütel appointed Siim Kallas the new prime minister .
The United People 's Party failed to meet the 5 % threshold .
From this coalition President Rüütel chose the leader of the Res Publica party , Juhan Parts , to form a government .
On 14 September 2003 , following negotiations that began in 1998 , the citizens of Estonia were asked in a referendum whether or not they wished to join the European Union .
In February 2004 the People 's Party Moderates renamed themselves as Social Democratic Party of Estonia .
32 MPs ( Res Publica and Centre Party ) did not take part .
On 4 April 2005 , President Rüütel nominated Reform party leader Andrus Ansip as Prime Minister designate by and asked him to form a new government , the 8th in 12 years .
Ansip formed a government out of a coalition of his Reform Party with the People 's Union and the Centre Party .
The general consensus in the Estonian media seems to be that the new cabinet , on the level of competence , is not necessarily an improvement over the old one .
On 4 April 2006 , Fatherland Union and Res Publica decided to form a united right-conservative party .
The joined party name is Isamaa ja Res Publica Liit ( Union of Pro Patria and Res Publica ) .
The Prime Minister of Estonia is the head of government of the Republic of Estonia .
Estonia numbers 15 main administrative subdivisions .
Due to the geographical and demographic size of these subdivisions , they are to be considered counties rather than states ( Estonian : pl. maakonnad ; sg .
