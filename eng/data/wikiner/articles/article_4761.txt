It is also the capital city of the local Guatemala Department and the largest city in Central America .
Kaminaljuyu dates back some 900 years and is one of America 's most notable archaeological sites .
The center of Kaminaljuyu was located a short distance from the oldest part of Guatemala City .
Guatemala City was the scene of the declaration of independence of Central America from Spain , and became the capital of the United Provinces of Central America in 1821 .
The city also functions as the main port of entry into the country , with Central America 's largest international airport , La Aurora International Airport and most major highways in the country originating or leading to the city .
Despite its location in the tropics and the many micro climates found within the country , Guatemala City 's elevation and the resulting moderating influence of the higher altitude enable it to enjoy a subtropical highland climate , though depending on location , it also borders on a tropical savanna climate .
Guatemala City is generally mild , almost springlike , throughout the course of the year .
The weather in Guatemala City is also very windy ; this may decrease the apparent temperature even more .
Guatemala City is subdivided into 22 zones designed by the urban engineering of Raúl Aguilar Batres , each one with its own streets and avenues , making it very easy to find addresses in the city .
Guatemala City 's population has experienced drastic growth since the 1970s with the influx of indigenous migrants from the outlying departments as well as a large influx of foreign groups .
For this reason along with several others , Guatemala City has experienced some growth problems such as transportation saturation , availability of safe potable water in some areas at certain times as well as increased crime .
In other words , Guatemala City faces problems common to many other rapidly expanding cities .
Guatemala City is subdivided into 22 zones designed by the urban engineering of Raúl Aguilar Batres , each one with its own streets and avenues , making it very easy to find addresses in the city .
Guatemala City possesses several sportsgrounds and is home to many sports clubs .
Football is the most popular sport , with CSD Municipal , Aurora FC and Comunicaciones being the main clubs .
An important multi-functional hall is the Domo Polideportivo de la CDAG .
The city has hosted several promotional functions and some international sports events : in 1950 it hosted the VI Central American and Caribbean Games , and in 2000 the FIFA Futsal World Championship .
On July 4 , 2007 the International Olympic Committee gathered in Guatemala City and voted Sochi to become the host for the 2014 Winter Olympics and Paralympics .
In 2008 , approximately 40 murders a week were reported in Guatemala City alone .
Guatemala City has been affected several times by earthquakes .
The nearest and most active is Pacaya , which at times expels a considerable amount of ash .
This hole , which is classified by geologists as either a " piping feature " or " piping pseudokarst " and incorrectly referred to as a " sinkhole " by the popular press , was 100 m ( 330 ft ) deep , and apparently was created by fluid from a sewer eroding uncemented volcanic ash and other pyroclastic deposits underlying Guatemala City .
Guatemala City is twinned with :
