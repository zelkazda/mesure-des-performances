Arthur Schopenhauer ( 22 February 1788 -- 21 September 1860 ) was a German philosopher known for his pessimism and philosophical clarity .
At age 25 , he published his doctoral dissertation , On the Fourfold Root of the Principle of Sufficient Reason , which examined the fundamental question of whether reason alone can unlock answers about the world .
Schopenhauer 's metaphysical analysis of will , his views on human motivation and desire , and his aphoristic writing style influenced many well-known thinkers including Friedrich Nietzsche , Richard Wagner , Ludwig Wittgenstein , Erwin Schrödinger , Albert Einstein , Sigmund Freud , Otto Rank , Carl Gustav Jung , Leo Tolstoy , and Jorge Luis Borges .
When the Kingdom of Prussia acquired the Polish-Lithuanian Commonwealth city of Danzig in 1793 , Schopenhauer 's family moved to Hamburg .
In 1805 , Schopenhauer 's father might have committed suicide .
After one year , Schopenhauer left the family business in Hamburg to join her .
Schopenhauer became a student at the University of Göttingen in 1809 .
There he studied metaphysics and psychology under Gottlob Ernst Schulze , the author of Aenesidemus , who advised him to concentrate on Plato and Kant .
In Dresden in 1819 , Schopenhauer fathered an illegitimate child who was born and died the same year .
In 1820 , Schopenhauer became a lecturer at the University of Berlin .
He scheduled his lectures to coincide with those of the famous philosopher G. W. F. Hegel , whom Schopenhauer described as a " clumsy charlatan " .
However , only five students turned up to Schopenhauer 's lectures , and he dropped out of academia .
She asked for damages , alleging that Schopenhauer had pushed her .
According to Schopenhauer 's court testimony , she deliberately annoyed him by raising her voice while standing right outside his door .
Schopenhauer had a notably strained relationship with his mother Johanna Schopenhauer .
After his father 's death , Arthur Schopenhauer endured two long years of drudgery as a merchant , in honor of his dead father .
But by that time she had already opened her infamous salon , and Arthur was not compatible with the vain , ceremonious ways of the salon .
There , he wrote his first book , On the Fourfold Root of the Principle of Sufficient Reason .
In a fit of temper Arthur told her that his work would be read long after the rubbish she wrote would have been totally forgotten .
Schopenhauer had a robust constitution , but in 1860 his health began to deteriorate .
A key focus of Schopenhauer was his investigation of individual motivation .
For Schopenhauer , human desire was futile , illogical , directionless , and , by extension , so was all human action in the world .
For Schopenhauer , human desiring , " willing, " and craving cause suffering or pain .
Schopenhauer 's moral theory proposed that of three primary moral incentives , compassion , malice and egoism , compassion is the major motivator to moral expression .
According to Schopenhauer , whenever we make a choice , " we assume as necessary that that decision was preceded by something from which it ensued , and which we call the ground or reason , or more accurately the motive , of the resultant action . "
" The murderer, " wrote Schopenhauer , " who is condemned to death according to the law must , it is true , be now used as a mere means , and with complete right .
Schopenhauer disagreed with those who would abolish capital punishment .
People , according to Schopenhauer , can not be improved .
Schopenhauer declared that " real moral reform is not at all possible , but only determent from the deed … . "
Schopenhauer declared that their teaching was corrupted by subsequent errors and therefore was in need of clarification .
Schopenhauer was perhaps even more influential in his treatment of man 's psychology than he was in the realm of philosophy .
Philosophers have not traditionally been impressed by the tribulations of sex , but Schopenhauer addressed it and related concepts forthrightly :
Schopenhauer refused to conceive of love as either trifling or accidental , but rather understood it to be an immensely powerful force lying unseen within man 's psyche and dramatically shaping the world :
These ideas foreshadowed Darwin 's discovery of evolution and Freud 's concepts of the libido and the unconscious mind .
Schopenhauer 's politics were , for the most part , an echo of his system of ethics .
Schopenhauer did , however , share the view of Thomas Hobbes on the necessity of the state , and of state violence , to check the destructive tendencies innate to our species .
Schopenhauer , by his own admission , did not give much thought to politics , and several times he writes proudly of how little attention he had paid " to political affairs of [ his ] day " .
Schopenhauer possessed a distinctly hierarchical conception of the human races , attributing civilizational primacy to the northern " white races " due to their sensitivity and creativity :
Schopenhauer additionally maintained a marked metaphysical and political anti-Judaism .
According to Bernard Bonnejean , Schopenhauer 's politic and social theories represent the first stage of a necessary awareness .
Schopenhauer 's controversial writings have influenced many , from Friedrich Nietzsche to nineteenth-century feminists .
Schopenhauer 's biological analysis of the difference between the sexes , and their separate roles in the struggle for survival and reproduction , anticipates some of the claims that were later ventured by sociobiologists and evolutionary psychologists in the twentieth century .
After the elderly Schopenhauer sat for a sculpture portrait by Elisabet Ney , he told Richard Wagner 's friend Malwida von Meysenbug , " I have not yet spoken my last word about women .
Schopenhauer believed that a person inherited level of intellect through one 's mother , and personal character through one 's father .
On the question of eugenics , Schopenhauer wrote :
Analysts have suggested that Schopenhauer 's advocacy of anti-egalitarianism and eugenics influenced the neo-aristocratic philosophy of Friedrich Nietzsche , who initially considered Schopenhauer his mentor .
As a consequence of his philosophy , Schopenhauer was very concerned about the welfare of animals .
Schopenhauer even went so far as to protest against the use of the pronoun " it " in reference to animals because it led to the treatment of them as though they were inanimate things .
To reinforce his points , Schopenhauer referred to anecdotal reports of the look in the eyes of a monkey who had been shot and also the grief of a baby elephant whose mother had been killed by a hunter .
Schopenhauer criticized Spinoza 's belief that animals are to be used as a mere means for the satisfaction of humans .
This was through his neighbor of two years , Karl Christian Friedrich Krause .
Krause had also mastered Sanskrit , unlike Schopenhauer , and the two developed a professional relationship .
Thus three of the four " truths of the Buddha " correspond to Schopenhauer 's doctrine of the will .
Other influences were : Shakespeare , Jean-Jacques Rousseau , John Locke , Baruch Spinoza , Matthias Claudius , George Berkeley , David Hume , René Descartes .
Some commentators suggest that Schopenhauer claimed that the noumenon , or thing-in-itself , was the basis for Schopenhauer 's concept of the will .
Other commentators suggest that Schopenhauer considered will to be only a subset of the " thing-in-itself " class , namely that which we can most directly experience .
This is explained more fully in Schopenhauer 's doctoral thesis , On the Fourfold Root of the Principle of Sufficient Reason .
Kant 's philosophy was formulated as a response to the radical philosophical skepticism of David Hume , who claimed that causality could not be observed empirically .
When Schopenhauer identifies the noumenon with the desires , needs , and impulses in us that we name " will, " what he is saying is that we participate in the reality of an otherwise unachievable world outside the mind through will .
The rational mind is , for Schopenhauer , a leaf borne along in a stream of pre-reflective and largely unconscious emotion .
It is for this reason that Schopenhauer identifies the noumenon with what we call our will .
Schopenhauer disagreed .
According to Schopenhauer , objects are intuitively perceived by understanding and are discursively thought by reason the understanding thinks objects through concepts and that ( 2 ) reason seeks the unconditioned or ultimate answer to " why ? " ) .
Instead Schopenhauer relied upon the Neoplatonist interpretation of the biographer Diogenes Laërtius from Lives and Opinions of Eminent Philosophers .
Diogenes Laërtius Plato ideas in natura velut exemplaria dixit subsistere ; cetera his esse similia , ad istarum similitudinem consistencia .
Schopenhauer expressed his dislike for the philosophy of his contemporary Georg Wilhelm Friedrich Hegel many times in his published works .
Schopenhauer thought that Hegel used deliberately impressive but ultimately vacuous verbiage .
Schopenhauer has had a massive influence upon later thinkers , though more so in the arts ( especially literature and music ) and psychology than in philosophy .
Nevertheless , a number of recent publications have reinterpreted and modernised the study of Schopenhauer .
