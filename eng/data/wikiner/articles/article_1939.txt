Bruno was from a noble family of Querfurt ( now in Saxony-Anhalt ) .
While still a youth he was made a canon of Magdeburg cathedral .
The fifteen-year-old Otto III made Bruno a part of his royal court .
Bruno spent much time at the monastery where Adalbert had become a monk and where abbot John Canaparius may have written a life of Saint Adalbert .
Later , Bruno entered a monastery near Ravenna , founded by Otto , and underwent severe ascetic training under the guidance of St. Romuald .
Bruno tried to convert Ahtum , the Duke of Banat , who was an Eastern Orthodox Christian to Catholicism , but this precipitated a large controversy leading to organized opposition from Orthodox monks .
Bruno spent five months there and baptized some thirty adults .
He helped to bring about a peace treaty between them and the ruler of Kiev .
Soon after his death , Bruno and his companions were venerated as martyrs and Bruno was soon after canonized .
It was said that Braunsberg was named after St Bruno .
