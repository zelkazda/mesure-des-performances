Frederick Charles Copleston , SJ , CBE ( 10 April 1907 , Taunton , Somerset , England -- 3 February 1994 , London , England ) was a Jesuit priest and historian of philosophy .
He studied and later lectured at Heythrop College and , seeing the poor standard of philosophical teaching in seminaries , he was author of an influential eleven-volume History of Philosophy ( 1946-75 ) , which is highly respected .
One of Copleston 's most significant contributions to modern philosophy was his work on the theories of St Thomas Aquinas .
He was appointed a member of the British Academy in 1970 and CBE in 1993 .
