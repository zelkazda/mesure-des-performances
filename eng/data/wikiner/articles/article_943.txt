The district was traditionally associated with the coal mining collieries , ironworks and tinplate works of the South Wales coalfield and South Wales Valleys , although all have now closed ; the town , which lies in the middle portion of the Ebbw valley , being situated on the south-eastern flank of the once great mining region of Glamorgan and Monmouthshire .
The area was part of the ancient Monmouthshire parish of Mynyddislwyn until the late 19th century .
In 1892 a local board of health and local government district of Abercarn was formed .
This became Abercarn urban district in 1894 , governed by an urban district council of twelve members .
Further local government organisation in 1996 placed the area in the county borough of Caerphilly .
The former urban district corresponds to the three communities of Abercarn , Crumlin and Newbridge .
