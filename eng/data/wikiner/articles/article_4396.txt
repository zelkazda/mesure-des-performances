Since at least World War II , achieving and maintaining air superiority has been a key component of victory in warfare , particularly conventional warfare between regular armies ( as opposed to guerrilla warfare ) .
The U.S. Army called their fighters " pursuit " aircraft ( reflected by their designation in the " P " series ) from 1916 until the late 1940s .
For example , in World War II the US Navy would later favor fighters over dedicated dive bombers , and the P-47 Thunderbolt would be favored for ground attack .
The controversial F-111 would be employed as a strike bomber as the fighter variant was abandoned .
Some of the most expensive fighters such as the F-14 Tomcat , F-22 Raptor and F-15 Eagle were employed as all-weather interceptors as well as air superiority combat aircraft , only developing air-to-ground roles late in their careers .
Multirole fighter-bombers such as the F/A-18 Hornet are often less expensive and tasked with ground attack as part of a " high-low mix " , or in the case of the Super Hornet , replacing a range of specialized aircraft types .
Fighters were developed in response to the fledgling use of aircraft and dirigibles in World War I for reconnaissance and ground-attack roles .
By World War II , fighters were predominantly all-metal monoplanes with wing-mounted batteries of cannons or machine guns .
Armament consists primarily of air-to-air missiles ( from as few as two on some lightweight day fighters to as many as eight or twelve on air superiority fighters like the Sukhoi Su-27 or Boeing F-15 Eagle ) , with a cannon as backup armament ( typically between 20 and 30 mm in caliber ) ; however , they can also employ air-to-surface missiles , as well as guided and unguided bombs .
One method was to build a " pusher " scout such as the Airco DH.2 , with the propeller mounted behind the pilot .
Nevertheless , a machine gun firing over the propeller arc did have some advantages , and was to remain in service from 1915 ( Nieuport 11 ) until 1918 ( Royal Aircraft Factory S.E.5 ) .
French aircraft designer Raymond Saulnier patented a practical device in April 1914 , but trials were unsuccessful because of the propensity of the machine gun employed to hang fire due to unreliable ammunition .
Garros ' modified monoplane was first flown in March 1915 and he began combat operations soon thereafter .
Like the D.I , they were biplanes ( only very occasionally monoplanes or triplanes ) .
They were armed with two Maxim-type machine guns -- which had proven much easier to synchronize than other types -- firing through the propeller arc .
This had obvious implications in case of accidents , but enabled jams ( to which Maxim-type machine guns always remained liable ) to be cleared in flight and made aiming much easier .
As collective combat experience grew , the more successful pilots such as Oswald Boelcke , Max Immelmann , and Edward Mannock developed innovative tactical formations and maneuvers to enhance their air units ' combat effectiveness and accelerate the learning -- and increase the expected lifespan -- of newer pilots reaching the front lines .
Fighter development slowed between the wars , with the most significant change coming late in the period , when the classic World War I type machines started to give way to metal monocoque or semi-monocoque monoplanes , with cantilever wing structures .
Considering that many aircraft were constructed similarly to World War I designs ( albeit with aluminum frames ) , it was not considered unreasonable to use World War I-style armament to counter them .
The rotary engine , popular during World War I , quickly disappeared , replaced chiefly by the stationary radial engine .
Aircraft engines increased in power several-fold over the period , going from a typical 180 hp in the 1918 Fokker D.VII to 900 hp in the 1938 Curtiss P-36 .
Aircraft designed for these races pioneered innovations like streamlining and more powerful engines that would find their way into the fighters of World War II .
At the very end of the inter-war period came the Spanish Civil War .
The Spanish Civil War also provided an opportunity for updating fighter tactics .
The finger-four would become widely adopted as the fundamental tactical formation over the course of World War II .
Aerial combat formed an important part of World War II military doctrine .
Fighters such as the Messerschmitt Bf 109 , the Supermarine Spitfire , the Yakovlev Yak-1 and the Curtiss P-40 Warhawk were all designed for high level speeds and a good rate of climb .
During the first few months of the invasion , Axis air forces were able to destroy large numbers of Red Air Force aircraft on the ground and in one-sided dogfights .
These facilities produced more advanced monoplane fighters , such as the Yak-1 , Yak-3 , LaGG-3 , and MiG-3 , to wrest air superiority from the Luftwaffe .
Also from that time , the Eastern Front became the largest arena of fighter aircraft use in the world ; fighters were used in all of the roles typical of the period , including close air support , interdiction , escort and interception roles .
The Curtiss P-36 Hawk had a 900 hp radial engine but was soon redesigned as the P-40 Warhawk with a 1100 hp in-line engine .
By 1943 , the latest P-40N had a 1300 hp Allison engine .
The Spitfire Mk I of 1939 was powered by a 1030 hp Merlin II ; its 1945 successor , the Spitfire F.Mk 21 , was equipped with the 2035 hp Griffon 61 .
New designs such as the Messerschmitt Me 262 and Gloster Meteor demonstrated the effectiveness of the new propulsion system .
Dive brakes were added to jet fighters late in World War II to minimize these problems and restore control to pilots .
Over the course of the Korean War , however , it became obvious that the day of the piston-engined fighter was coming to a close and that the future would lie with the jet fighter .
One which did enter service -- with the U.S. Navy in March 1945 -- was the Ryan FR-1 Fireball ; production was halted with the war 's end on VJ-Day , with only 66 having been delivered , and the type was withdrawn from service in 1947 .
The first rocket-powered aircraft was the Lippisch Ente , which made a successful maiden flight in March 1928 .
Later variants of the Me 262 were also fitted with rocket powerplants , while earlier models were fitted with rocket boosters , but were not mass-produced with these modifications .
The first generation of jet fighters comprises the initial , subsonic jet fighter designs introduced late in World War II and in the early post-war period .
Top speeds for fighters rose steadily throughout World War II as more powerful piston engines were developed , and had begun approaching the transonic flight regime where the efficiency of piston-driven propellers drops off considerably .
The first jets were developed during World War II and saw combat in the last two years of the war .
Nevertheless , the Me 262 indicated the obsolescence of piston-driven aircraft .
A few designs combining piston and jet engines for propulsion -- such as the Ryan FR Fireball -- saw brief use , but by the end of the 1940s virtually all new combat aircraft were jet-powered .
Many squadrons of piston-engined fighters were retained until the early-to-mid 1950s , even in the air forces of the major powers ( though the types retained were the best of the World War II designs ) .
The Lockheed P-80 Shooting Star ( soon re-designated F-80 ) was less elegant than the swept-wing Me 262 , but had a cruise speed ( 660 km/h [ 410 mph ] ) as high as the combat maximum of many piston-engined fighters .
The two aircraft had different strengths , but were similar enough that the superior technology such as a radar ranging gunsight and skills of the veteran United States Air Force pilots allowed them to prevail .
The development of second-generation fighters was shaped by technological breakthroughs , lessons learned from the aerial battles of the Korean War , and a focus on conducting operations in a nuclear warfare environment .
The prospect of a potential third world war featuring large mechanized armies and nuclear weapon strikes led to a degree of specialization along two design approaches : interceptors ( like the English Electric Lightning and Mikoyan-Gurevich MiG-21F ) and fighter-bombers ( such as the Republic F-105 Thunderchief and the Sukhoi Su-7 ) .
Air-to-surface missiles ( ASM ) equipped with electro-optical ( E-O ) contrast seekers -- such as the initial model of the widely used AGM-65 Maverick -- became standard weapons , and laser-guided bombs became widespread in effort to improve precision-attack capabilities .
It also led to the development of new automatic-fire weapons , primarily chain-guns that use an electric engine to drive the mechanism of a cannon ; this allowed a single multi-barrel weapon ( such as the 20 mm Vulcan ) to be carried and provided greater rates of fire and accuracy .
Dedicated ground-attack aircraft ( like the Grumman A-6 Intruder , SEPECAT Jaguar and LTV A-7 Corsair II ) offered longer range , more sophisticated night attack systems or lower cost than supersonic fighters .
With variable-geometry wings , the supersonic F-111 introduced the Pratt & Whitney TF30 , the first turbofan equipped with afterburner .
The F-16 's manoeuvrability was further enhanced by its being designed to be slightly aerodynamically unstable .
The F-16 's sole reliance on electronics and wires to relay flight commands , instead of the usual cables and mechanical linkage controls , earned it the sobriquet of " the electric jet " .
Unlike interceptors of the previous eras , most fourth-generation air-superiority fighters were designed to be agile dogfighters ( although the Mikoyan MiG-31 and Panavia Tornado ADV are notable exceptions ) .
The need for both types of fighters led to the " high/low mix " concept which envisioned a high-capability and high-cost core of dedicated air-superiority fighters ( like the F-15 and Su-27 ) supplemented by a larger contingent of lower-cost multi-role fighters ( such as the F-16 and MiG-29 ) .
Most fourth-generation fighter-bombers , such as the Boeing F/A-18 Hornet and Dassault Mirage 2000 , are true multirole warplanes , designed as such from the start .
The earlier approaches of adding on strike capabilities or designing separate models specialized for different roles generally became passé ( with the Panavia Tornado being an exception in this regard ) .
Dedicated attack roles were generally assigned either to interdiction strike aircraft such as the Sukhoi Su-24 and Boeing F-15E Strike Eagle or to armored " tank-plinking " close air support ( CAS ) specialists like the Fairchild-Republic A-10 Thunderbolt II and Sukhoi Su-25 .
The first stealth aircraft to be introduced were the Lockheed F-117 Nighthawk attack aircraft ( introduced in 1983 ) and the Northrop Grumman B-2 Spirit bomber ( which first flew in 1989 ) .
The end of the Cold War in 1991 led many governments to significantly decrease military spending as a " peace dividend " .
The Su-30MKI and MiG-35 use two-and three-dimensional thrust vectoring engines respectively so as to enhance maneuvering .
Of the 4.5th generation designs , only the Super Hornet , Strike Eagle , and the Rafale have seen combat action .
The fifth generation was ushered in by the Lockheed Martin/Boeing F-22 Raptor in late 2005 .
Some attention has also been paid to reducing IR signatures , especially on the F-22 .
Detailed information on these signature-reduction techniques is classified , but in general includes special shaping approaches , thermoset and thermoplastic materials , extensive structural use of advanced composites , conformal sensors , heat-resistant coatings , low-observable wire meshes to cover intake and cooling vents , heat ablating tiles on the exhaust troughs ( seen on the Northrop YF-23 ) , and coating internal and external metal areas with radar-absorbent materials and paint .
To spread the development costs -- and production base -- more broadly , the Joint Strike Fighter program enrolls eight other countries as cost-and risk-sharing partners .
