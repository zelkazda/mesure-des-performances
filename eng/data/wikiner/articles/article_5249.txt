The German Navy ( Deutsche Marine ( listen ) is the navy of Germany and part of the Bundeswehr ( German Armed Forces ) .
From 1945 to 1956 , the German Mine Sweeping Administration and its successor organisations , made up of former members of the Kriegsmarine , became something of a transition stage for the German Navy , allowing the future Bundesmarine to draw on experienced personnel upon its formation .
In total , there are 50 commissioned ships in the German Navy , excluding the 10 submarines and 43 auxiliary ships .
The German Navy is part of the German armed forces ( Bundeswehr ) , and is deeply integrated into the NATO alliance .
The German Navy is also engaged in operations against international terrorism such as Operation Enduring Freedom and NATO Operation Active Endeavour .
Presently the largest operation the German Navy is participating in is UNIFIL II off the coast of Lebanon .
The strength of the Navy is about 17,000 men and women .
The navy as a part of the Bundeswehr is responsible for developing and providing the maritime capabilities of the German armed forces .
