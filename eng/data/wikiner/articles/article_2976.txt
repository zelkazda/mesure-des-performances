Christine de Pisan ( also seen as de Pizan ) ( 1365 -- c. 1434 ) was a Venetian-born woman of the medieval era who strongly challenged misogyny and stereotypes prevalent in the male-dominated realm of the arts .
Christine de Pizan was born in 1365 in Venice .
Pizan did not assert her intellectual abilities , or establish her authority as a writer until she was widowed at the age of twenty-four .
Christine de Pizan 's participation in a literary quarrel , in 1401 -- 1402 , allowed her to move beyond the courtly circles , and ultimately to establish her status as a writer concerned with the position of women in society .
Written in the thirteenth century , the Romance of the Rose satirizes the conventions of courtly love while critically depicting women as nothing more than seducers .
Her critique primarily stems from her belief that Jean de Meun was purposely slandering women through the debated text .
The debate itself is extensive and at its end , the principal issue was no longer Jean de Meun 's literary capabilities .
In The Book of the City of Ladies Christine de Pizan created a symbolic city in which women are appreciated and defended .
She makes special mention of a manuscript illuminator we know only as Anastasia who she described as the most talented of her day .
Christine de Pizan contributed to the rhetorical tradition by counteracting the contemporary discourse .
