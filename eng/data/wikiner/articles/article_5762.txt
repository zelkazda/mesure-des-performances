During the classical period , Berber civilization was already at a stage in which agriculture , manufacturing , trade , and political organization supported several states .
Trade links between Carthage and the Berbers in the interior grew , but territorial expansion also resulted in the enslavement or military recruitment of some Berbers and in the extraction of tribute from others .
As Carthaginian power waned , the influence of Berber leaders in the hinterland grew .
After that king Massinissa managed to unify Numidia under his rule .
each Maghreb region is made up of several tribes .
The Almohads were able to unify the Maghreb .
The imams gained a reputation for honesty , piety , and justice , and the court of Tahirt was noted for its support of scholarship .
The movement 's initial impetus was religious , an attempt by a tribal leader to impose moral discipline and strict adherence to Islamic principles on followers .
But the Almoravid movement shifted to engaging in military conquest after 1054 .
Like the Almoravids , the Almohads ( " unitarians " ) found their inspiration in Islamic reform .
The Almohads took control of Morocco by 1146 , captured Algiers around 1151 , and by 1160 had completed the conquest of the central Maghrib .
The zenith of Almohad power occurred between 1163 and 1199 .
The final triumph of the 700-year Christian reconquest of Spain was marked by the fall of Granada in 1492 .
In 1516 Aruj moved his base of operations to Algiers but was killed in 1518 .
For 300 years , Algeria was a province of the Ottoman Empire under a regency that had Algiers as its capital ( see Dey ) .
Subsequently , with the institution of a regular Ottoman administration , governors with the title of pasha ruled .
Although Algiers remained a part of the Ottoman Empire , the Ottoman government ceased to have effective influence there .
Hussein Dey went into exile .
On May 1 the followers of his Parti du Peuple Algérien ( PPA ) participated in demonstrations which were violently put down by the police .
The Algerian War of Independence ( 1954 -- 1962 ) , brutal and long , was the most recent major turning point in the country 's history .
The Evian accords also provided for continuing economic , financial , technical , and cultural relations , along with interim administrative arrangements until a referendum on self-determination could be held .
On September 8 , 1963 , a constitution was adopted by referendum , and later that month , Ahmed Ben Bella was formally elected the first president .
A new constitution drawn up under close FLN supervision was approved by nationwide referendum in September 1963 , and Ben Bella was confirmed as the party 's choice to lead the country for a five-year term .
Under the new constitution , Ben Bella as president combined the functions of chief of state and head of government with those of supreme commander of the armed forces .
As minister of defense , Houari Boumédienne had no qualms about sending the army to put down regional uprisings because he felt they posed a threat to the state .
However , when Ben Bella attempted to co-opt allies from among some of those regionalists , tensions increased between Houari Boumédienne and Ahmed Ben Bella .
In 1965 the military toppled Ahmed Ben Bella , and Houari Boumedienne became head of state .
On June 19 , 1965 , Houari Boumédienne deposed Ahmed Ben Bella in a military coup d'état that was both swift and bloodless .
Houari Boumédienne 's position as head of government and of state was initially not secure partly because of his lack of a significant power base outside the armed forces ; he relied strongly on a network of former associates known as the Oujda group , but he could not fully dominate the fractious regime .
Among the scores of parties that sprang up under the new constitution , the militant Islamic Salvation Front was the most successful , winning more than 50 % of all votes cast in municipal elections in June 1990 as well as in first stage of national legislative elections held in December 1991 .
Although seven candidates qualified for election , all but Abdelaziz Bouteflika , who appeared to have the support of the military as well as the FLN , withdrew on the eve of the election amid charges of electoral fraud .
Bouteflika went on to win with 70 percent of the cast votes .
Following his election to a five-year term , Bouteflika concentrated on restoring security and stability to the strife-ridden country .
President Bouteflika was rewarded for his efforts at stabilizing the country when he was elected to another five-year term in April 2004 , in an election contested by six candidates without military interference .
