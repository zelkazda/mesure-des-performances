The organ was first used for popular music by Milt Herth , who played it live on WIND soon after it was invented .
However , by the 1950s , jazz musicians such as Jimmy Smith began to use the organ 's distinctive sound .
In Britain the organ became associated with elevator music and ice rinks music .
Examples of their work toured the world with bands such as Uriah Heep , Kansas and Procol Harum .
Artists such as Bob Ralston and Ethel Smith played these organs .
Jazz organists such as Jimmy Smith developed the ability to perform fluent walking-bass lines on the bass pedals , mostly on ballad tempo tunes .
Rhoda Scott is said to have originated the barefoot playing method , which has gained popularity in recent years .
Hammond organs are also widely used in 1970s progressive rock music bands such as Pink Floyd 's Rick Wright ; Genesis 's Tony Banks ; Kansas , notably on their song " Carry on Wayward Son " .
None of the other crew of the Red Dwarf spaceship particularly enjoy Rimmer 's taste in music .
