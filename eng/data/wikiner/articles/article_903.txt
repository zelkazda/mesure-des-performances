The statement in Strabo ( ii .
Their territory thus included the greater part of the modern departments of Saône-et-Loire , Côte-d'Or and Nièvre .
According to Livy ( v .
Before Caesar 's time they had attached themselves to the Romans , and were honoured with the title of brothers and kinsmen of the Roman people [ citation needed ] .
The oration of Eumenius , in which he pleaded for the restoration of the schools of his native place Augustodunum , shows that the district was neglected .
