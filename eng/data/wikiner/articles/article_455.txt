Andrew Johnson ( December 29 , 1808 -- July 31 , 1875 ) was the 17th President of the United States ( 1865 -- 1869 ) .
He and Lincoln were elected in November 1864 and inaugurated on March 4 , 1865 .
Johnson succeeded to the presidency upon Lincoln 's assassination on April 15 , 1865 .
Johnson 's party status was ambiguous during his presidency .
Asked in 1868 why he did not become a Democrat , he said , " It is true I am asked why do n't I join the Democratic Party .
For these reasons he is usually counted as a Democrat when identifying presidents by their political parties .
Johnson 's mother then took in work spinning and weaving to support her family , and she later remarried .
Johnson had no formal education and taught himself how to read and write .
At age 16 or 17 , Johnson left his apprenticeship and ran away with his brother to Greeneville , Tennessee , where he found work as a tailor .
At the age of 19 , Johnson married 17 year-old Eliza McCardle in 1827 .
Johnson participated in debates at the local academy at Greeneville , Tenn. and later organized a worker 's party that elected him as alderman in 1829 .
In 1835 , he was elected to the Tennessee House of Representatives where , after serving a single term , he was defeated for re-election .
Johnson was attracted to the states rights Democratic Party of Andrew Jackson .
As the slavery question became more critical , Johnson continued to take a middle course .
He supported President Buchanan 's administration .
However , he was not nominated in 1856 because of a split within the Tennessee delegation .
Lincoln appointed Johnson military governor of occupied Tenn. in March 1862 with the rank of brigadier general .
According to tradition and local lore , on August 8 , 1863 , Johnson freed his personal slaves .
During the election , Johnson replaced Hannibal Hamlin as Lincoln 's running mate .
At the ceremony , Johnson , who had been drinking to offset the pain of typhoid fever ( as he explained later ) , gave a rambling speech and appeared intoxicated to many .
In early 1865 , Johnson talked harshly of hanging traitors like Jefferson Davis , which endeared him to radicals .
On April 15 , 1865 , the morning after Lincoln 's assassination , Johnson was sworn in as President of the United States by the newly appointed Chief Justice Salmon P. Chase .
Johnson was the first vice president to succeed to the presidency upon the assassination of a president and the third vice president to become a president upon the death of a sitting president .
However , when he succeeded Lincoln as president , Johnson took a much softer line , commenting , " I say , as to the leaders , punishment .
Johnson favored a very quick restoration , similar to the plan of leniency that Lincoln advocated before his death .
Congress also renewed the Freedman 's Bureau , but Johnson vetoed it .
It extended citizenship to every person born in the United States , penalized states that did not give the vote to freedmen , and most importantly , created new federal civil rights that could be protected by federal courts .
Johnson unsuccessfully sought to block ratification of the amendment .
Johnson campaigned vigorously , undertaking a public speaking tour of the north that was known as the " Swing Around the Circle " ; the tour proved politically disastrous , with Johnson widely ridiculed and occasionally engaging in hostile arguments with his audiences .
There were two attempts to remove President Andrew Johnson from office .
Johnson had wanted to replace Stanton with former general Ulysses S. Grant , who refused to accept the position .
Johnson had vetoed the act , claiming it was unconstitutional .
Thomas attempted to move into the war office , for which Stanton had Thomas arrested .
William M. Evarts served as his counsel .
Johnson 's defense was based on a clause in the Tenure of Office Act stating that the then-current secretaries would hold their posts throughout the term of the president who appointed them .
Since Lincoln had appointed Stanton , it was claimed , the applicability of the act had already run its course .
Earlier amnesties , requiring signed oaths and excluding certain classes of people , had been issued by Lincoln and by Johnson .
Andrew Johnson is one of only four presidents who did not appoint a judge to serve on the Supreme Court .
Johnson forced the French out of Mexico by sending an army to the border and issuing an ultimatum .
Secretary of State Seward negotiated the purchase of Alaska from Russia on April 9 , 1867 for $ 7.2 million .
Johnson 's purchase of Alaska from the Russian Empire in 1867 was his most important foreign policy action .
Johnson served from March 4 , 1875 , until his death from a stroke near Elizabethton , Tennessee , on July 31 that year .
The cemetery is now part of the Andrew Johnson National Historic Site .
They rated Johnson " near great " , but have since reevaluated and now consider Johnson " a flat failure " .
