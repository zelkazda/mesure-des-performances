Earlier emperors resided in Kyoto for nearly eleven centuries .
But in December 2006 , the Imperial Household Agency reversed its position and decided to allow researchers to enter some of the kofun with no restrictions .
Emperors have been known to come into conflict with the reigning shogun from time to time ; a notable example is the Hōgen Rebellion of 1156 , in which former Emperor Sutoku attempted to seize power from the then current Emperor Go-Shirakawa , both of whom were supported by different clans of samurai .
In keeping with the analogy , they even used the term " Emperor " in reference to the shogun/regent , e.g. in the case of Toyotomi Hideyoshi , who missionaries called " Emperor Taicosama " .
Prior to Emperor Meiji , the names of the eras were changed more frequently , and the posthumous names of the emperors were chosen in a different manner .
After a decision decreed by Emperor Ichijō , some emperors even had two empresses simultaneously ( kōgō and chugu are the two separate titles for that situation ) .
Earlier , the emperors had married women from families of the government-holding Soga lords , and women of the imperial clan itself , i.e. various-degree cousins and often even their own sisters ( half-sisters ) .
Several imperials of the 5th and 6th centuries such as Prince Shōtoku were children of a couple of half-siblings .
These marriages often were alliance or succession devices : the Soga lord ensured the domination of a prince , to be put as puppet to the throne ; or a prince ensured the combination of two imperial descents , to strengthen his own and his children 's claim to the throne .
Since that law was repealed in the aftermath of World War II , the present Emperor Akihito became the first crown prince for over a thousand years to have an empress outside the previously eligible circle .
There is suspicion that Emperor Keitai ( c. 500 CE ) may have been an unrelated outsider , though the sources state that he was a male-line descendant of Emperor Ōjin .
Four empresses , Empress Suiko , Empress Kōgyoku ( also Empress Saimei ) and Empress Jitō , as well as the mythical Empress Jingū , were widows of deceased emperors and princesses of the blood imperial in their own right .
One , Empress Gemmei , was the widow of a crown prince and a princess of the blood imperial .
The other four , Empress Genshō , Empress Kōken ( also Empress Shōtoku ) , Empress Meishō and Empress Go-Sakuramachi , were unwed daughters of previous emperors .
It also prevented branches , other than the branch descending from Taishō , from being imperial princes any longer .
Until the birth of Prince Hisahito , son of Prince Akishino , on September 6 , 2006 , there was a potential succession problem , since Prince Akishino was the only male child to be born into the imperial family since 1965 .
On January 20 , 2006 , Prime Minister Junichiro Koizumi devoted part of his annual keynote speech to the controversy , pledging to submit a bill allowing women to ascend the throne to ensure that the succession continues in the future in a stable manner .
However , shortly after the announcement that Princess Kiko was pregnant with her third child , Koizumi suspended such plans .
Her son , Prince Hisahito , is the third in line to the throne under the current law of succession .
