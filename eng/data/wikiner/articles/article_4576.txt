He is generally identified with Fosite , a god of the Frisians .
However , in other Old Norse words , for example forboð , " forbidding , ban, " the prefix for-has a pejorative sense .
So it is more plausible that Fosite is the older name and Forseti a folk etymology .
According to Snorri Sturluson in the Prose Edda , Forseti is the son of Baldr and Nanna .
This suggests skill in mediation and is in contrast to his fellow god Týr , who " is not called a reconciler of men . "
However , as de Vries points out , the only basis for associating Forseti with justice seems to have been his name ; there is no corroborating evidence in Norse mythology .
Willebrord defiled the spring by baptizing people in it and killing a cow there .
Altfrid tells the same story of St. Liudger .
The stranger and the spring are identified with Fosite and the sacred spring of Fositesland .
