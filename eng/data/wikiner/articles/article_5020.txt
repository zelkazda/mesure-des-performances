Trying to identify a contemporary vernacular term and the associated nation with a classical name , Latin writers from the 10th century onwards used the learnèd adjective teutonicus to refer to East Francia and its inhabitants .
The adjective derived from this noun , * þiudiskaz , " popular " , was later used with reference to the language of the people in contrast to the Latin language ( earliest recorded example 786 ) .
Tacitus mentioned 40 , Ptolemy 69 peoples .
Classical ethnography applied the name Suebi to many tribes in the first century .
Some of the ethnic names mentioned by the ethnographers of the first two centuries on the shores of the Oder and the Vistula ( Gutones , Vandali ) reappear from the 3rd century on in the area of the lower Danube and north of the Carpathian Mountains .
These peoples were classified as Scyths and often deducted from the ancient Getae .
Germanic expansions during early Roman times are known only generally , but it is clear that the forebears of the Goths were settled on the southern Baltic shore by 100 CE .
In the absence of large-scale political unification , such as that imposed forcibly by the Romans upon the peoples of Italy , the various tribes remained free , led by their own hereditary or chosen leaders .
The tribal homelands to the north and east emerged collectively in the records as Germania .
The peoples of this area were sometimes at war with Rome , but also engaged in complex and long-term trade relations , military alliances , and cultural exchanges with Rome as well .
Caesar 's wars helped establish the term Germania .
At the end of the 1st century two provinces west of the Rhine called Germania inferior and Germania superior were established .
In England , the Angles merged with the Saxons and other groups , as well as absorbing some natives , to form the Anglo-Saxons .
More to the south , in Belgium , archeological results of this period point to immigration from the north .
Odoacer , who deposed Romulus Augustulus , is the ultimate example .
Germanic monarchy was elective ; the king was elected by the free men from among eligible candidates of a family ( OE cynn ) tracing their ancestry to the tribe 's divine or semi-divine founder .
Among the Anglo-Saxons , a regular free man ( a ceorl ) had a weregild of 200 shillings ( i.e. solidi or gold pieces ) , classified as a twyhyndeman " 200-man " for this reason , while a nobleman commanded a fee of six times that amount ( twelfhyndeman " 1200-man " ) .
Similarly , among the Alamanni the basic weregild for a free men was 200 shillings , and the amount could be doubled or tripled according to the man 's rank .
Common clothing styles are known from the remarkably well-preserved corpses that have been found in former marshes on several locations in Denmark , and included woolen garments and brooches for women and trousers and leather caps for men .
Several centuries later , Anglo-Saxon and Frankish missionaries and warriors undertook the conversion of their Saxon neighbours .
Eventually , the conversion was forced by armed force , successfully completed by Charlemagne , in a series of campaigns ( the Saxon Wars ) , that also brought Saxon lands into the Frankish empire .
The latter , Orkney and Shetland , though now part of Scotland , were nominally part of the Kingdom of Norway until the 15th century .
The Normans also conquered and ruled Sicily and parts of southern Italy for a time .
Crimean Gothic communities appear to have survived intact until the late 1700s , when many were deported by Catherine the Great .
