IBM mainframes , are large computer systems produced by IBM from 1952 to the present .
( Some IBM 7094s remained in service into the 1980s . )
A desk size machine with a different instruction set , the IBM 1130 , was released concurrent with the System/360 to address the 1620s niche .
All that changed with the announcement of the System/360 ( S/360 ) in April , 1964 .
The System/360 was a single series of compatible models for both commercial and scientific use .
System/360 incorporated features which had previously been present on only either the commercial line ( such as decimal arithmetic and byte addressing ) or the technical line ( such as floating point arithmetic ) .
Some of the arithmetic units and addressing features were optional on some models of the System/360 .
The System/360 was also the first computer in wide use to include dedicated hardware provisions for the use of operating systems .
The smaller models in the System/360 line ( e.g. the 360/30 ) were intended to replace the 1400 series while providing an easier upgrade path to the larger 360s .
Many customers kept using their old software and one of the features of the later System/370 was the ability to switch to emulation mode and back under operating system control .
The System/360 later evolved into the System/370 , the System/390 , the zSeries , the System z9 , and today 's System z10 .
The different processors on a current IBM mainframes are :
A few systems run MUSIC/SP and UTS .
