Pliny the Elder , however , distinguishes between male and female forms of antimony ; his male form is probably the sulfide , while the female form , which is superior , heavier , and less friable , is probably native metallic antimony .
Later Latin authors adapted the word to Latin as stibium .
Littré suggests the first form , which is the earliest , derives from stimmida , ( one ) accusative for stimmi .
An artifact made of antimony dating to about 3000 BC was found at Tello , Chaldea ( part of present-day Iraq ) , and a copper object plated with antimony dating between 2500 BC and 2200 BC has been found in Egypt .
There is some uncertainty as to the description of the artifact from Tello .
This book precedes the more famous 1556 book in Latin by Agricola , De re metallica , even though Agricola has been often incorrectly credited with the discovery of metallic antimony .
In 2005 , China was the top producer of antimony with about 84 % world share followed at a distance by South Africa , Bolivia and Tajikistan , reports the British Geological Survey .
A coin made of antimony was issued in the Keichow Province of China in 1931 .
