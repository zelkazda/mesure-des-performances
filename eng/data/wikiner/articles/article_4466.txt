Fornax is a constellation in the southern sky .
Its name is Latin for furnace .
Fornax was identified by Nicolas Louis de Lacaille in 1756 .
Fornax has been the target of investigations into the furthest reaches of the universe .
The Hubble Ultra Deep Field is located within Fornax , and the Fornax Cluster , a small cluster of galaxies , lies primarily within Fornax .
At a meeting of the Royal Astronomical Society in Britain , a team from University of Queensland described 40 unknown " dwarf " galaxies in this constellation ; follow-up observations with the Hubble Space Telescope and the European Southern Observatory 's Very Large Telescope revealed that ultra compact dwarfs are much smaller than previously known dwarf galaxies , about 120 light-years across .
NGC 1316 is a notably bright elliptical galaxy within the Fornax Cluster .
