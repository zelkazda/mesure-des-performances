While the bodies that proposed , argued , agreed upon and ultimately adopted existing international agreements vary according to each agreement , certain conferences -- including 1972 's United Nations Conference on the Human Environment , 1983 's World Commission on Environment and Development , 1992 's United Nations Conference on Environment and Development and 2002 's World Summit on Sustainable Development have been particularly important .
Meanwhile , the U.S. judicial system reviews not only the legislative enactments , but also the administrative decisions of the many agencies dealing with environmental issues .
The federal and state judiciaries have played an important role in the development of environmental law in the U.S. , in many cases resolving significant controversy regarding the application of federal environmental laws in favor of environmental interests .
The common law also continues to play a leading role in American water law , in the doctrines of riparian rights and prior appropriation .
In the U.S. , responsibilities for the administration of environmental laws are divided between numerous federal and state agencies with varying , overlapping and sometimes conflicting missions .
The U.S. Environmental Protection Agency‎ ( EPA ) is the most well-known federal agency , with jurisdiction over many of the country 's national air , water and waste and hazardous substance programs .
Other federal agencies , such as the U.S. Fish and Wildlife Service and National Park Service pursue primarily conservation missions , while still others , such as the United States Forest Service and the Bureau of Land Management , tend to focus more on beneficial use of natural resources .
U.S. state governments , therefore , administering state law adopted under state police powers or federal law by delegation , uniformly include environmental agencies .
In the United States , violations of environmental laws are generally civil offenses , resulting in monetary penalties and , perhaps , civil sanctions such as injunction .
EPA 's Office of Enforcement and Compliance Assurance is one such agency .
Others , such as the United States Park Police , carry out more traditional law enforcement activities .
Indeed , in the U.S. estimates of the environmental regulation 's total costs reach 2 % of GDP , and any new regulation will arguably contribute in some way to that burden .
Environmental law courses are offered as elective courses in the second and third years of JD study at many American law schools .
The environmental law reviews at Yale , Harvard , Stanford , Columbia and NYU law schools are regularly the most-cited such publications .
