The idea was introduced by the architect Christopher Alexander in the field of architecture and has been adapted for various other disciplines , including computer science .
Christopher Alexander describes common design problems as arising from " conflicting forces " -- such as the conflict between wanting a room to be sunny and wanting it not to overheat on summer afternoons .
Alexander , for example , suggests that enough windows should be included to direct light all around the room .
