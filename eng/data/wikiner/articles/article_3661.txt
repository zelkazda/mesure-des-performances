Both stations mostly broadcast mainly to the Greater Cairo region .
Egyptian ground-broadcast television ( ERTU ) is government controlled and depends heavily on commercial revenue .
Currently , there are three companies which offer cellular communication service : Mobinil , Vodafone Egypt and Etisalat Egypt .
Telephone system : large system ; underwent extensive upgrading during 1990s and is reasonably modern ; Telecom Egypt , the landline monopoly , has been increasing service availability and in 2006 fixed-line density stood at 14 per 100 persons ; as of 2007 there were three mobile-cellular networks and service is expanding rapidly
