He was elected to the New York State Senate in 1867 .
Tweed was convicted for stealing between $ 40 million and $ 200 million ( based on the inflation or devaluation rate of the dollar since 1870 of 2.7 % , this is between 1.5 and 8 billion 2010 dollars ) from New York City taxpayers through political corruption .
He died in the Ludlow Street Jail .
Tweed was born April 3 , 1823 , on the Lower East Side of Manhattan .
Tweed and Gould became the subjects of political cartoons by Thomas Nast in 1869 .
By 1869 , Tweed , as commissioner of public works , led a ring that controlled the government of New York City .
In the words of Albert Bigelow Paine , " their methods were curiously simple and primitive .
For example , the construction cost of the New York County Courthouse , begun in 1861 , grew to nearly $ 13 million ( about $ 178 million in today 's dollars , and nearly twice the cost of the Alaska Purchase in 1867 ) .
When his attempt at blackmail failed , he provided the evidence he had collected to The New York Times .
The New York Times began publishing the incriminating account on July 8 , 1871 .
In October 1871 , Tweed was arrested and held on $ 8 million bail .
The efforts of political reformers William H. Wickham ( 1875 New York City mayor ) and Samuel J. Tilden ( later the 1876 Democratic presidential nominee ) resulted in Tweed 's trial and conviction in 1873 .
Tweed was given a 12-year prison sentence , which was reduced by a higher court and he served one year .
He was then re-arrested on civil charges , sued by New York State for $ 6 million and held in debtor 's prison until he could post $ 3 million as bail .
He was delivered to authorities in New York City on November 23 , 1876 , and was returned to prison .
He was recognized in Spain from political cartoons showing his corruption .
Tweed died in the Ludlow Street Jail on April 12 , 1878 from severe pneumonia .
He was buried in the Brooklyn Green-Wood Cemetery .
In studies of Tweed and the Tammany Hall organization , historians have emphasized the thievery and conspiratorial nature of Boss Tweed along the Upper West Side , and securing land for the Metropolitan Museum of Art .
From 1869 to 1871 , under Tweed 's influence , the state of New York spent more on charities than for the entire time period from 1852 to 1868 combined .
[ citation needed ] Tweed also pushed through funding for a teachers college and prohibition of corporal punishment in schools , as well as salary increases for school teachers .
The 2002 film Gangs of New York has Tweed , played by Jim Broadbent , as a major supporting character .
Both Tammany Hall and the type of corruption Tweed was known for have prominent places in the film .
