Transport in Denmark is developed and modern .
Bridges across the Great Belt and the Øresund have done much to improve traffic flow across the country and between Denmark and Sweden .
The largest railway operator in Denmark is Danske Statsbaner ( DSB ) -- Danish State Railways .
Arriva operates some routes in Jutland , and several other smaller operators provide local services .
The Oresund Bridge provides a rail connection with Malmö , Sweden .
These trains have a 20-minute schedule , and continue further into Skåne County .
There are railway connections from Hamburg to Copenhagen via :
Several daytime trains Hamburg -- Copenhagen still use this ferry .
When the Fehmarn Belt bridge is constructed ( expected completion in 2018 ) , it will greatly shorten the journey time between Copenhagen and Hamburg .
S-trains are electric trains connecting the city center with the suburbs of Copenhagen .
S-trains run from 05:00 hours ( weekdays ) ( 06:00 Sundays ) until about 0:30 hours .
Copenhagen Metro : An automated driverless rail rapid transit system serving Copenhagen and Frederiksberg , Denmark .
The system opened in 2002 and currently has two lines with 22 stations and 21.3 km of track , following the 2007 opening of an extension to Copenhagen Airport .
Nearly one-fifth of all trips in Copenhagen are by bicycle , and for home-to-work commutes , 36 % of all trips are by bicycle .
Odense has been named the " bicycle city of the year " because of the great number of bicycle lanes in the city .
A complete network of 350 km all-weather serviced lanes exists in the town -- this is as much as some states in Germany .
As an experiment started at the end of 2006 , Nørrebrogade , the main street in the Nørrebro area in Copenhagen , has had its traffic lights set for a " green wave " at 20 km/h to let the bikes get through without stopping , while cars typically have to stop at every light .
During the summer months , there are free " city bikes " stationed at various spots in the downtown area of Copenhagen .
It is located at Kastrup , 8 km south-east of central Copenhagen .
It is connected by train to Copenhagen Central Station and beyond as well as to Malmö and other towns in Sweden .
For the west of the country , the major airport is Billund ( 1,261,000 departing passengers in 2008 ) although both Aalborg ( 519,000 departing passengers in 2008 ) and Aarhus ( 287,000 departing passengers in 2008 ) have smaller airports with regular connections to Copenhagen .
In 2007 , 288 cruise ships visited Copenhagen .
