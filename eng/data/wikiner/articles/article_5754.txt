The civilizations in Mesopotamia , the Indus Valley , and China shared many similarities and likely exchanged technologies and ideas such as mathematics and the wheel .
The northern part of the continent , covering much of Siberia was also inaccessible to the steppe nomads due to the dense forests and the tundra .
These areas in Siberia were very sparsely populated .
The Caucasus , Himalaya , Karakum Desert , and Gobi Desert formed barriers that the steppe horsemen could only cross with difficulty .
In southern Mesopotamia were the alluvial plains of Sumer and Elam .
The Ubaid culture from flourished from 5500 BCE .
China and Vietnam were also centres of metalworking .
In the 17th century , the Manchu conquered China and established the Qing Dynasty , although this was in decline by the nineteenth century and had been overthrown in 1912 .
Conflicts such as the Korean War , Vietnam War and Soviet invasion of Afghanistan were fought between communists and anti-communists .
After the Soviet Union 's collapse in 1991 , there were many new independent nations in Central Asia .
