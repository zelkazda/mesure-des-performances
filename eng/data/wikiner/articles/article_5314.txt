Gosford is a city located on the Central Coast of New South Wales , Australia , approximately 76 km north of the Sydney central business district .
The city is situated at the northern extremity of Brisbane Water , an extensive northern branch of the Hawkesbury River estuary and Broken Bay .
The city is the administrative centre of the Central Coast region , which is the third largest urban area in New South Wales after Sydney and Newcastle .
Along with the other land around the Hawkesbury River estuary , the Brisbane Water district was explored during the early stages of the settlement of Sydney .
Gosford itself was explored by Governor Phillip between 1788 and 1789 .
The first road between Hawkesbury , to Brisbane Water was only a cart wheel track even in 1850 .
Convicts once lived and worked in the Gosford area .
In 1825 , Gosford 's population reached 100 , of which 50 % were convicts .
East Gosford was the first centre of settlement .
In 1887 , the rail link to Sydney was completed , requiring a bridge over the Hawkesbury River and a tunnel through the sandstone ridge west of Woy Woy .
The introduction of this transport link , and then the Pacific Highway in 1930 accelerated the development of the region .
Gosford became a town in 1885 and was declared a municipality in 1886 .
Gosford has a oceanic climate / humid subtropical climate with hot , humid summers and cool , wet winters .
The Central Coast Highway runs past Gosford 's waterfront area , while its predecessor the Pacific Highway takes on several names through the CBD itself .
More recently , the Gosford CBD has suffered a decline as more commercial activity has relocated to nearby Erina , particularly Erina Fair .
Gosford is also home to :
