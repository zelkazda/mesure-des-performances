Celibacy not only for religious and monastics ( brothers/monks and sisters/nuns ) but also for bishops is upheld by the Roman Catholic Church traditions .
In late 16th-century Venice , nearly 60 % of all patrician women joined convents , and only a minority of these women did so voluntarily .
Catholics understand celibacy as the calling of some , but not of all .
It became obligatory for all priests in the west in the 12th century at the First Lateran Council ( 1123 ) , Second Lateran Council ( 1139 ) , and the Council of Trent ( 1545 -- 64 ) .
Married men may become deacons , and married clergy who have converted from other denominations may become Catholic priests without becoming celibate .
All rites of the Catholic Church maintain the ancient tradition where marriage is not allowed after ordination .
Clerical celibacy began to be demanded in the 4th century , including papal decretals beginning with Pope Siricius .
This characterization by Jesus Christ of the future status of all persons ( in heaven ) is officially designated " universal celibacy " by the Roman Catholic Church : " For in the resurrection they neither marry nor are given in marriage , but are like angels in heaven . "
