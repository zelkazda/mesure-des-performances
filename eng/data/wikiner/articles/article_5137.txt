To make this system of royal criminal justice more effective , Henry employed the method of inquest used by William the Conqueror in the Domesday Book .
The grand jury was later recognized by King John in Magna Carta in 1215 on demand of the nobility .
In the early decades of the United States grand juries played a major role in public matters .
In Ireland grand juries were active from medieval times in the English-controlled parts of the island , mainly functioning as local government authorities at the county level , as well as having a pre-trial judicial function for serious criminal cases .
From 1691 to 1793 Roman Catholics were excluded from membership .
Grand juries are today virtually unknown outside the U.S. .
England abandoned grand juries in 1933 and instead uses a committal procedure , as do all Australian jurisdictions .
New Zealand abolished the grand jury in 1961 .
Today approximately half of the states in the U.S. employ them , and only twenty-two require their use , to varying extents .
In the state of Louisiana there is a third option , " By pretermitting entirely the matter investigated " .
The Federal Rules of Criminal Procedure prescribe that a grand jury must have from sixteen to twenty-three members and that an indictment may issue only if at least 12 jurors concur .
Thomas E. Dewey was appointed as an independent prosecutor and would rocket to fame on his prosecutions .
In the U.S. , the states of Arizona , California , Massachusetts , Minnesota , N.Y. , Nevada , North Carolina , and Oregon have grand juries at the county level .
In Calif. , each county is required by the state constitution to have at least one grand jury impaneled at all times .
In addition , grand juries are not subject to the Brown Act .
Most county grand juries in California do not consider criminal matters , though by law they are able to .
Hennepin County , Minnesota ( which contains Minneapolis ) keeps a grand jury impaneled at all times .
