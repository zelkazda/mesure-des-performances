There is evidence that they developed about 100 square kilometres ( 39 sq mi ) of volcanic floodplains in the vicinity of Lake Condah into a complex of channels and dams , that they used woven traps to capture eels , and that capturing and smoking eels supported them year round .
Aquaculture was operating in China circa 2500 BC .
A fortunate genetic mutation of carp led to the emergence of goldfish during the Tang Dynasty .
Japanese cultivated seaweed by providing bamboo poles and , later , nets and oyster shells to serve as anchoring surfaces for spores .
Romans bred fish in ponds .
Hawaiians constructed oceanic fish ponds .
Californians harvested wild kelp and attempted to manage supply circa 1900 , later labeling it a wartime resource .
Aquaculture is an especially important economic activity in China .
In 2005 , China accounted for 70 % of world production .
It is currently one of the fastest growing areas of food production in the U.S .
Approximately 90 % of all U.S. shrimp consumption is farmed and imported .
In recent years salmon aquaculture has become a major export in southern Chile , especially in Puerto Montt , Chile 's fastest-growing city .
China overwhelmingly dominates the world in reported aquaculture output .
However , there are issues with the accuracy of China 's returns .
China disputes this claim .
However , the FAO accepts there are issues with the reliability of China 's statistical returns , and currently treats data from China , including the aquaculture data , apart from the rest of the world .
In 2009 , researchers in Australia managed for the first time to coax tuna to breed in landlocked tanks .
Abalone farming began in the late 1950s and early 1960s in Japan and China .
About 75 % of farmed shrimp is produced in Asia , in particular in China and Thailand .
Thailand is the largest exporter .
Shrimp farming has changed from its traditional , small-scale form in Southeast Asia into a global industry .
The global annual production of freshwater prawns ( excluding crayfish and crabs ) in 2003 was about 280000 tonnes of which China produced 180000 tonnes followed by India and Thailand with 35000 tonnes each .
In China , sea cucumbers are farmed in artificial ponds as large as 1000 acres ( 400 ha ) .
The US produced 1500000 tonnes of tilapia in 2005 , with 2500000 tonnes projected by 2010 .
