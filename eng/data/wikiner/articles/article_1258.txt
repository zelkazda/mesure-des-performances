Apart from its coastline with the South China Sea , it is completely surrounded by the state of Sarawak , Malaysia , and in fact it is separated into two parts by Limbang , which is part of Sarawak .
It is the only sovereign state completely on the island of Borneo , with the remainder of the island belonging to Malaysia and Indonesia .
It later became a vassal state of Majapahit [ citation needed ] before embracing Islam in the 15th century .
At the peak of its empire , the sultanate had control that extended over the coastal regions of modern-day Sarawak and Sabah , the Sulu archipelago , and the islands off the northwest tip of Borneo .
The thalassocracy was visited by Ferdinand Magellan in 1521 and fought the Castille War in 1578 against Spain .
Brunei regained its independence from the United Kingdom on 1 January 1984 .
Economic growth during the 1970s and 1990s , averaging 56 % from 1999 to 2008 , has transformed Brunei Darussalam into a newly industrialised country .
According to the International Monetary Fund ( IMF ) , Brunei is ranked 4th in the world by gross domestic product per capita at purchasing power parity .
and thus , the name " Brunei " was derived from his words .
The word " Borneo " is of the same origin .
It has been debated when Islam first arrived in Brunei .
A number of relics show that Islam may have been practiced in Brunei by the 12th century .
If this is so , Islam has actually arrived in Brunei in the year of 977 .
Most likely there were two waves of Islamic teachings that came to Brunei .
By the 16th century , Brunei had built one of its biggest mosques .
Islam was firmly rooted in Brunei by the 16th century .
European influence gradually brought an end to this regional power .
Later , there was a brief war with Spain , in which Brunei 's capital was occupied .
Eventually the sultanate was victorious but lost territories to Spain .
Brunei was a British protectorate from 1888 to 1984 , and occupied by Japan from 1941 to 1945 during World War II .
There was a small rebellion against the monarchy during the 1960s , which was suppressed with help from the U.K. .
The rebellion partially affected Brunei 's decision to opt out of the Malaysian Federation .
Under Brunei 's 1959 constitution , His Majesty Paduka Seri Baginda Sultan Haji Hassanal Bolkiah Mu'izzaddin Waddaulah is the head of state with full executive authority , including emergency powers , since 1962 .
The country has been under hypothetical martial law since Brunei Revolt of 1962 .
With its traditional ties with the United Kingdom , it became the 49th member of the Commonwealth immediately on the day of its independence on 1 January 1984 .
As its first initiatives toward improved regional relations , Brunei joined ASEAN on January 7 , 1984 , becoming the sixth member .
Brunei is recognized by every nation in the world .
It shares a close relationship particularly with the Philippines and other nations such as Singapore .
Brunei also maintains historical ties with Malaysia , the United Kingdom , and the U.S. .
However , Kuraman Island is recognized as Malaysia territory by Brunei .
The status of Limbang as part of Sarawak was disputed by Brunei since the area was first annexed in 1890 .
Brunei is divided into four districts ( daerah ) :
Brunei Darussalam consists of two unconnected parts with the total area of 5,766 sq .
77 % of the population lives in the eastern part of Brunei , while only about 10,000 live in the mountainous south eastern part ( the district of Temburong ) .
The total population of Brunei Darussalam is approximately 428,000 ( 2010 ) of which around 130,000 live in the capital Bandar Seri Begawan .
Other major towns are the port town of Muara , the oil producing town of Seria and its neighboring town , Kuala Belait .
Jerudong Park , a well known amusement park , is located on the west of Bandar Seri Begawan .
Brunei Darussalam has a tropical rainforest climate .
Brunei 's leaders are concerned that steadily increased integration in the world economy will undermine internal social cohesion although it became a more prominent player by serving as chairman for the 2000 Asia-Pacific Economic Cooperation ( APEC ) forum .
Brunei is increasingly importing from other countries .
Brunei also aims to build confidence in the brand through strategies that will both ensure the halal integrity of the products and unfaltering compliance with set rules governing the sourcing of raw materials , manufacturing process , logistics and distribution .
All Brunei citizens have access to free health care from public hospitals .
Brunei is accessible by air , sea and land transport .
Brunei International Airport is the main entry point to the country .
Royal Brunei Airlines is the national carrier .
The speedboats provide passenger and goods transportation to the Temburong district .
Brunei has one main sea port located at Muara .
Islam is the official religion of Brunei at 67 percent , and the sultan is the head of the religion in the country .
