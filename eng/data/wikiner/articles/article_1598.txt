Well-known bodybuilders include Charles Atlas , Steve Reeves , Reg Park , Arnold Schwarzenegger , and Lou Ferrigno .
Sandow had a stage show built around these displays through his manager , Florenz Ziegfeld .
The role of Sandow was played by actor Nat Pendleton .
Sandow became so successful at flexing and posing his physique , he later created several businesses around his fame and was among the first to market products branded with his name alone .
This is how Sandow built his own physique and in the early years , men were judged by how closely they matched these " ideal " proportions .
In the early 20th century , Bernarr Macfadden and Charles Atlas , continued to promote bodybuilding across the world .
Bodybuilding became more popular in the 1950s and 1960s with the emergence of strength and gymnastics champions joining the sport , and the simultaneous popularization of muscle training , most of all by Charles Atlas , whose advertising in comic books and other publications encouraged many young men to undertake weight training to improve their physiques to resemble the comic books ' muscular superheroes .
Of bodybuilder-actors perhaps the most famous were Steve Reeves and Reg Park , who were featured in roles portraying Hercules , Samson and other legendary heroes .
Dave Draper gained public fame through appearances in Muscle Beach Party , part of the " beach party " series of films featuring Annette Funicello and Frankie Avalon that began with Beach Blanket Bingo , and also in cameo appearances in television series such as the Beverly Hillbillies .
Other rising stars in this period were Larry Scott , Serge Nubret , and Sergio Oliva .
In the 1970s , bodybuilding had major publicity thanks to Arnold Schwarzenegger and others in the 1977 film Pumping Iron .
By this time the International Federation of BodyBuilding & Fitness ( IFBB ) dominated the sport and the Amateur Athletic Union ( AAU ) took a back seat .
The late 1980s and early 1990s saw the decline of AAU sponsored bodybuilding contests .
In 1999 , the AAU voted to discontinue its bodybuilding events .
In bodybuilding lore , this is partly attributed to the rise of " mass monsters " , beginning with Arnold Schwarzenegger but including Franco Columbu , Lou Ferrigno , Dorian Yates , Lee Haney , Ronnie Coleman and Paul DeMayo and also the emergence of athletes such as Rich Gaspari and Andreas Munzer , who defied their genetics to attain size and hardness previously unimagined .
To combat this , and in the hopes of becoming a member of the IOC , the IFBB introduced doping tests for both steroids and other banned substances .
In 1990 , wrestling promoter Vince McMahon announced he was forming a new bodybuilding organization , the World Bodybuilding Federation ( WBF ) .
McMahon wanted to bring WWF-style showmanship and bigger prize money to the sport of bodybuilding .
A number of IFBB stars were recruited but the roster was never very large , with the same athletes competing ; the most notable winner and first WBF champion was Gary Strydom .
McMahon formally dissolved the WBF in July 1992 .
In the early 2000s , the IFBB was attempting to make bodybuilding an Olympic sport .
It obtained full IOC membership in 2000 and was attempting to get approved as a demonstration event at the Olympics which would hopefully lead to it being added as a full contest .
Olympic recognition for bodybuilding remains controversial since many argue that bodybuilding is not a sport .
In 2003 , Joe Weider sold Weider Publications to AMI , which owns The National Enquirer .
In the modern bodybuilding industry , " professional " generally means a bodybuilder who has won qualifying competitions as an amateur and has earned a " pro card " from the IFBB .
In 1985 , a movie called Pumping Iron II : The Women was released .
Rachel McLish would closely resemble what is thought of today as a fitness and figure competitor instead of what is now considered a female bodybuilder .
These are known as " shock micro-cycles " and were a key training technique used by Soviet athletes .
