At the end of the Bush War there was a transition to majority rule in 1980 .
The United Kingdom ceremonially granted Zimbabwe independence on April 18 , 1980 in accordance with the Lancaster House Agreement .
From about 1250 until 1450 , Mapungubwe was eclipsed by the Kingdom of Zimbabwe .
In 1898 , the name Southern Rhodesia was adopted .
Cecil Rhodes presented this concession to persuade the government of the U.K. to grant a royal charter to his British South Africa Company ( BSAC ) over Matabeleland , and its subject states such as Mashonaland .
The region to the north was administered separately by the BSAC and later named Northern Rhodesia ( now Zambia ) .
Rhodesians served on behalf of the United Kingdom during World War II , mainly in the East African Campaign against Axis forces in Italian East Africa .
The U.K. deemed this an act of rebellion , but did not re-establish control by force .
A civil war ensued , with Joshua Nkomo 's ZAPU and Robert Mugabe 's ZANU using assistance from the governments of Zambia and Mozambique .
Zimbabwe Rhodesia regained its independence as Zimbabwe on April 18 , 1980 .
Many foreign dignitaries also attended , including Prime Minister Indira Gandhi of India , President Shehu Shagari of Nigeria , President Kenneth Kaunda of Zambia , President Seretse Khama of Botswana , and Prime Minister Malcolm Fraser of Australia , representing the Commonwealth of Nations .
Mugabe 's government changed the capital 's name from Salisbury to Harare on April 18 , 1982 in celebration of the second anniversary of independence .
Reverend Canaan Banana served as the first President .
The constitutional changes came into effect on 1 January 1988 with Robert Mugabe as President .
Many white MPs joined ZANU which then reappointed them .
Mugabe , returning from a visit with United States President Jimmy Carter in New York City , said , " One thing is quite clear -- we are not going to have disloyal characters in our society . "
Nkomo ( ZAPU ) left for exile in Britain and did not return until Mugabe guaranteed his safety .
In 1982 government security officials discovered large caches of arms and ammunition on properties owned by ZAPU , accusing Nkomo and his followers of plotting to overthrow the government .
Mugabe fired Nkomo and his closest aides from the cabinet .
As a result of what they saw as persecution of Nkomo and his party , PF-ZAPU supporters , army deserters began a campaign of dissidence against the government .
It involved attacks on government personnel and installations , armed banditry aimed at disrupting security and economic life in the rural areas , and harassment of ZANU-PF members .
In 1983 to 1984 the government declared a curfew in areas of Matabeleland and sent in the army in an attempt to suppress dissidents .
ZANU-PF increased its majority in the 1985 elections , winning 67 of the 100 seats .
The majority gave Mugabe the opportunity to start making changes to the constitution , including those with regard to land restoration .
Fighting did not cease until Mugabe and Nkomo reached an agreement in December 1987 whereby ZAPU became part of ZANU-PF and the government changed the constitution to make Mugabe the country 's first executive president and Nkomo one of two vice presidents .
Mugabe was panicked by demonstrations by Zanla ex-combatants , war veterans , who had been the heart of incursions 20 years earlier in the Bush War .
Mugabe raised this issue of land ownership by white farmers .
Opposition to President Mugabe and the ZANU-PF government grew considerably after the mid-1990s in part due to worsening economic and human rights conditions .
The Movement for Democratic Change ( MDC ) was established in September 1999 as an opposition party founded by trade unionist Morgan Tsvangirai .
The MDC 's first opportunity to test opposition to the Mugabe government came in February 2000 , when a referendum was held on a draft constitution proposed by the government .
Among its elements , the new constitution would have permitted President Mugabe to seek two additional terms in office , granted government officials immunity from prosecution , and authorised government seizure of white-owned land .
Parliamentary elections held in June 2000 were marred by localised violence , and claims of electoral irregularities and government intimidation of opposition supporters [ citation needed ] .
In the months leading up to the poll , ZANU-PF , with the support of the army .
[ citation needed ] , -- very few of whom actually fought in the Second Chimurenga against the Smith regime in the 1970s [ citation needed ] -- set about wholesale intimidation and suppression of the MDC-led opposition [ citation needed ] .
Despite strong international criticism [ citation needed ] , these measures , together with organised subversion of the electoral process , ensured a Mugabe victory [ citation needed ] .
In mid-2004 , vigilantes loyal to Mr. Tsvangirai began attacking members who were mostly loyal to Ncube , climaxing in a September raid on the party 's Harare headquarters in which the security director was nearly thrown to his death .
An internal party inquiry later established that aides to Tsvangirai had tolerated , if not endorsed , the violence .
[ citation needed ] Zimbabwean parliamentary election , 2005 were held in March 2005 in which ZANU-PF won a two-thirds majority , were again criticised by international observers as being flawed .
[ citation needed ] Mugabe 's political operatives were thus able to weaken the opposition internally and the security apparatus of the state was able to destabilize it externally by using violence in anti-Mugabe strongholds to prevent citizens from voting .
Additionally Mugabe had started to appoint judges sympathetic to the government [ citation needed ] , making any judicial appeal futile .
Ncube 's supporters argued that the M.D.C .
should field a slate of candidates ; Tsvangirai 's argued for a boycott .
When party leaders voted on the issue , Ncube 's side narrowly won , but Mr. Tsvangirai declared that as president of the party he was not bound by the majority 's decision .
Mugabe 's party won 24 of the 31 constituencies where elections were held amid low voter turnout .
The operation was " the latest manifestation of a massive human rights problem that has been going on for years " , said Amnesty International .
In September 2005 Mugabe signed constitutional amendments that reinstituted a national senate ( abolished in 1987 ) and that nationalised all land .
The MDC split over whether to field candidates and partially boycotted the vote .
The split in the MDC hardened into factions , each of which claimed control of the party .
In December 2006 , ZANU-PF proposed the " harmonisation " of the parliamentary and presidential election schedules in 2010 ; the move was seen by the opposition as an excuse to extend Mugabe 's term as president until 2010 .
The event garnered an international outcry and was considered particularly brutal and extreme , even considering the reputation of Mugabe 's government .
Harare 's drinking water became unreliable in 2006 and as a consequence dysentery and cholera swept the city in December 2006 and January 2007 .
The country used to be one of Africa 's richest and is now one of its poorest .
With all this and the forced and violent removal of white farmers in a brutal land redistribution program , Mugabe has earned himself widespread scorn from the international arena .
It features mansions , manicured lawns , full shops with fully stocked shelves containing an abundance of fruit and vegetables , big cars and a golf club give is the home to President Mugabe 's out-of-town retreat .
The three major candidates were incumbent President Robert Mugabe of the Zimbabwe African National Union -- Patriotic Front ( ZANU-PF ) , Morgan Tsvangirai of the Movement for Democratic Change -- Tsvangirai ( MDC-T ) , and Simba Makoni , an independent .
As no candidate received an outright majority in the first round , a second round was held on June 27 , 2008 between Tsvangirai ( with 47.9 % of the first round vote ) and Mugabe ( 43.2 % ) .
Tsvangirai withdrew from the second round a week before it was scheduled to take place , citing violence against his party 's supporters .
The second round went ahead , despite widespread criticism , and led to victory for Mugabe .
Mugabe 's opponents were critical of the handling of the electoral process , and the government was accused of planning to rig the election ; Human Rights Watch said that the election was likely to be " deeply flawed " .
An independent projection placed Tsvangirai in the lead , but without the majority needed to avoid a second round .
The MDC declared that Tsvangirai won a narrow majority in the first round and initially refused to participate in any second round .
ZANU-PF has said that Mugabe will participate in a second round ; the party alleged that some electoral officials , in connection with the MDC , fraudulently reduced Mugabe 's score , and as a result a recount was conducted .
Despite Tsvangirai 's continuing claims to have won a first round majority , he initially decided to participate in the second round .
On June 22 , 2008 , Tsvangirai announced that he was withdrawing from the run-off , describing it as a " violent sham " and saying that his supporters risked being killed if they voted for him .
The second round nevertheless went ahead as planned with Mugabe as the only actively participating candidate , although Tsvangirai 's name remained on the ballot .
Mugabe won the second round by an overwhelming margin and was sworn in for another term as President on June 29 .
The sanctions were vetoed by Russia and China .
The talks were mediated by South African President Thabo Mbeki .
On 15 September 2008 , the leaders of the 14-member Southern African Development Community witnessed the signing of the power-sharing agreement , brokered by South African leader Thabo Mbeki .
On 11 February 2009 Tsvangirai was sworn in as the Prime Minister of Zimbabwe .
On 6 March 2009 , Tsvangirai 's wife was killed in a car accident in which he was also injured .
According to news reports , Mr Tsvangirai was taken to hospital with head and neck injuries .
