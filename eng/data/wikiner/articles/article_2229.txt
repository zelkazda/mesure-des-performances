The first compiler was written by Grace Hopper , in 1952 , for the A-0 programming language .
The FORTRAN team led by John Backus at IBM is generally credited as having introduced the first complete compiler , in 1957 .
COBOL was an early language to be compiled on multiple architectures , in 1960 .
Since the 1970s it has become common practice to implement a compiler in the language it compiles , although both Pascal and C have been popular choices for implementation language .
Before the development of FORTRAN , the first higher-level language , in the 1950s , machine-dependent assembly language was widely used .
Some language specifications spell out that implementations must include a compilation facility ; for example , Common Lisp .
However , there is nothing inherent in the definition of Common Lisp that stops it from being interpreted .
Other languages have features that are very easy to implement in an interpreter , but make writing a compiler much harder ; for example , APL , SNOBOL4 , and many scripting languages allow programs to construct arbitrary source code at runtime with regular string operations , and then execute that code by passing it to a special evaluation function .
Practical examples of this approach are the GNU Compiler Collection , LLVM , and the Amsterdam Compiler Kit , which have multiple front-ends , shared analysis and multiple back-ends .
Thus , partly driven by the resource limitations of early systems , many early languages were specifically designed so that they could be compiled in a single pass ( e.g. , Pascal ) .
Interprocedural analysis and optimizations are common in modern commercial compilers from HP , IBM , SGI , Intel , Microsoft , and Sun Microsystems .
The open source GCC was criticized for a long time for lacking powerful interprocedural optimizations , but it is changing in this respect .
Another open source compiler with full analysis and optimization infrastructure is Open64 , which is used by many organizations for research and commercial purposes .
