Dean L. Kamen ( born April 5 , 1951 ) is an American entrepreneur and inventor from New Hampshire .
He is the son of Jack Kamen , an illustrator for Mad , Weird Science and other EC Comics publications .
Kamen is best known for inventing the product that eventually became known as the Segway PT , an electric , self-balancing human transporter with a complex , computer-controlled gyroscopic stabilization and control system .
The machine 's development was the object of much speculation and hype after segments of a book quoting Steve Jobs and other notable IT visionaries espousing its society-revolutionizing potential were leaked in December 2001 .
Kamen claims that his company DEKA is now working on solar power inventions .
Kamen is also the co-inventor of a compressed-air-powered device which would launch a human into the air in order to quickly launch SWAT teams or other emergency workers to the roofs of tall , inaccessible buildings .
In 1989 , Kamen founded FIRST ( For Inspiration and Recognition of Science and Technology ) , a program for students to get people interested in science , technology , and engineering .
Kamen remains the driving force behind the organization , providing over 2,500 high schools with the tools needed to learn valuable engineering skills .
FIRST has many robotic programs , including the Jr.FLL ( Junior FIRST Lego League ) and the FLL ( FIRST Lego League ) for younger students , and the FTC ( FIRST Tech Challenge ) and the FRC ( FIRST Robotics Competition ) for high school aged students .
Kamen says that the FIRST competition is the invention he is most proud of , and predicts that the 1 million students who have taken part in the contents so far will be responsible for some significant technological advances in years to come .
During his career Kamen has won numerous awards .
He was elected to the National Academy of Engineering in 1997 for his biomedical devices and for making engineering more popular among high school students .
In April 2002 , Kamen was awarded the Lemelson-MIT Prize for inventors , for his invention of the Segway and of an infusion pump for diabetics .
Kamen owns two helicopters , which he regularly uses to commute to work , and has a hangar built into the house as well .
The prize for the winning team was a visit and guided tour of Dean Kamen 's house and property .
His company , DEKA , annually creates intricate mechanical presents for him .
