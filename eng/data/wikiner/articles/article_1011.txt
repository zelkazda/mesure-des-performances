It should foster mutual regional trade among the member states , as well as with the U.S. and the European Union .
Compared e.g. to the European Union the political and economic integration is very limited .
The grouping was originally Argentina , Brazil , and Chile in one group , Colombia , Chile , Peru , Uruguay , and Venezuela in the second group , and the last group which included Bolivia , Ecuador , and Paraguay .
Cuba was the last to accede , becoming a full member on August 26 , 1999 .
A system of preferences -- which consists of market opening lists , special cooperation programs ( business rounds , preinvestment , financing , technological support ) and countervailing measures on behalf of the landlocked countries -- has been granted to the countries deemed to be less developed ( Bolivia , Ecuador and Paraguay ) , to favour their full participation in the integration process .
