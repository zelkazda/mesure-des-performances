Buffalo ( pronounced /ˈbʌfəloʊ/ ) is the second most populous city in the state of New York , behind New York City .
Located in Western New York on the eastern shores of Lake Erie and at the head of the Niagara River across from Fort Erie , Ontario , Buffalo is the principal city of the Buffalo-Niagara Falls metropolitan area and the seat of Erie County .
Originating around 1789 as a small trading community near the eponymous Buffalo Creek , Buffalo grew quickly after the opening of the Erie Canal in 1825 , with the city as its western terminus .
By 1900 , Buffalo was the 8th largest city in the country , and went on to become a major railroad hub , the largest grain-milling center in the country , and the home of the largest steel-making operation in the world .
With the start of Amtrak in the 1970s , Buffalo Central Terminal was also abandoned , and trains were rerouted to nearby Depew , N.Y. ( Buffalo-Depew ) and Exchange Street Station .
This growth has been maintained , in part , by major expansions of the Buffalo Niagara Medical Campus and the University at Buffalo .
A recent study showed that Buffalo 's unemployment rate is lower than both the New York State and national averages .
In 2010 Forbes rated Buffalo the 10th best place to raise a family .
What is clear is that there were no bison in the area ; that the settlement of Buffalo took its name from Buffalo Creek ; and that Buffalo Creek first appeared on a map in 1759 -- 1760 .
Prior to the Iroquois occupation of the region , the region was settled by the Neutral Nation .
In 1804 , Joseph Ellicott , a principal agent of the Holland Land Company , designed a radial street and grid system that branches out from downtown like bicycle spokes , and is one of the few radial street plans in the US .
On November 4 , 1825 the Erie Canal was completed with Buffalo strategically positioned at the western end of the system .
The Erie Canal brought a surge in population and commerce which led Buffalo to incorporate as a city in 1832 , with a population of about 10,000 people .
In 1845 , construction was begun on the Macedonia Baptist Church ( commonly called the Michigan Street Baptist Church ) .
During the 1840s , Buffalo 's port continued to develop .
Both passenger and commercial traffic expanded with some 93,000 passengers heading west from the port of Buffalo .
Grover Cleveland served as Sheriff of Erie County ( 1871-1873 ) , and was Mayor of Buffalo in 1882 .
In May , 1896 , the Ellicott Square Building opened .
It was named for surveyor Joseph Ellicott .
The city got the nickname City of Light at this time due to the widespread electric lighting .
City of Light ( 1999 ) was the title of Buffalo native Lauren Belfer 's historical novel set in 1901 , which in turn engendered a listing of real vs. fictional persons and places featured in her pages .
An international bridge , known as the Peace Bridge , linking Buffalo to Fort Erie , Ontario was opened in 1927 .
During World War II , Buffalo saw a period of prosperity and low unemployment due to its position as a manufacturing center .
The American Car and Foundry company , which manufactured railcars , reopened their Buffalo plant in 1940 to manufacture munitions during the war years .
Buffalo is located on the eastern end of Lake Erie , opposite Fort Erie , Ontario in Canada , and at the beginning of the Niagara River , which flows northward over Niagara Falls and into Lake Ontario .
According to the United States Census Bureau , the city has a total area of 52.5 square miles ( 136 km 2 ) .
Buffalo has a reputation for snowy winters , but it is rarely the snowiest city in New York State .
The region experiences a fairly humid , continental-type climate , but with a definite maritime flavor due to strong modification from the Great Lakes .
The transitional seasons are very brief in Buffalo and Western New York .
Winters in Western New York are generally cold and snowy , but are changeable and include frequent thaws and rain as well .
Winters can also be quite long in Western New York , usually spanning from mid-November to mid-March .
Due to the prevailing winds , areas south of Buffalo receive much more lake effect snow than locations to the north .
The lake snow machine starts as early as mid-November , peaks in December , then virtually shuts down after Lake Erie freezes in mid to late January .
The most well-known snowstorm in Buffalo 's history , the Blizzard of ' 77 , was not a lake effect snowstorm in Buffalo in the normal sense of that term ( Lake Erie was frozen over at the time ) , but instead resulted from a combination of high winds and snow previously accumulated both on land and on frozen Lake Erie .
Snow does not typically impair the city 's operation , but did cause significant damage as with the October 2006 storm .
Buffalo has the sunniest and driest summers of any major city in the Northeast , but still has enough rain to keep vegetation green and lush .
The stabilizing effect of Lake Erie continues to inhibit thunderstorms and enhance sunshine in the immediate Buffalo area through most of July .
Like most formerly industrial cities of the Great Lakes region , Buffalo has suffered through several decades of population decline brought about by the loss of its industrial base .
Traditionally , Polish-Americans were the predominant occupants of the East Side , while Italian-Americans composed a close-knit neighborhood in the west side .
Buffalo is currently in the process of a $ 1 billion city school rebuilding plan .
Most private schools have a Roman Catholic affiliation .
There are schools affiliated with other religions such as Islam and Judaism .
There are also nonsectarian options including The Buffalo Seminary ( the only private , nonsectarian , all-girls school in Western New York state ) , and Nichols School .
Buffalo is home to four State University of New York ( SUNY ) institutions .
Buffalo and the surrounding area were long involved in railroad commerce , steel manufacture , automobile production , aircraft/aerospace design and production , Great Lakes shipping , and grain storage .
Overall , employment in Buffalo has shifted as its population has declined and manufacturing has left .
Buffalo 's 2005 unemployment rate was 6.6 % , contrasted with New York State 's 5.0 % rate .
And from the fourth quarter of 2005 to the fourth quarter of 2006 , Erie County had no net job growth , ranking it 271st among the 326 largest counties in the country .
Buffalo has increasingly become a center for bioinformatics and human genome research , including work by researchers at the University at Buffalo and the Roswell Park Cancer Institute .
This consortium is known as the Buffalo Niagara Medical Campus .
HSBC Bank USA also has major operations in Buffalo ( The sports arena , which hosts the Buffalo Sabres NHL franchise , is named HSBC Arena ) .
Other banks , such as Bank of America and KeyBank have corporate operations in Buffalo .
Citigroup also has regional offices in Amherst , Buffalo 's largest suburb .
Buffalo has also become a hub of the debt collection industry .
First Niagara Bank recently moved its headquarters to downtown Buffalo from nearby Lockport .
First Niagara has branches from Buffalo to Albany , N.Y. , and since September 2009 has had branches as far south as Pittsburgh .
The company says facilities in Lockport will remain open and fully staffed .
First Niagara , which had been considering expanding into Western Pennsylvania for some time , benefited from PNC Financial Services being required by the United States Department of Justice to sell off 50 National City branches in the Pittsburgh area and 11 more branches in and around Erie to competitors , since the two banks had significant overlap in Western Pennsylvania and had potential antitrust issues in that area .
The move affected the area by creating 200 more jobs , some in the Buffalo area .
Buffalo is home to Rich Products , one of the world 's largest family-owned food manufacturers .
The city is the heart of the Canadian-American corridor .
Over 80 % of all U.S.-Canada trade occurs via border crossings in the eastern United States and with five bridges to Canada , the Buffalo area is one of the key eastern border crossing locations .
New Era Cap Company , the largest sports-licensed headwear company in the United States , is based in Buffalo .
The Trico company operated three major manufacturing facilities but has since closed all of them and moved operations to Mexico .
For many years , Buffalo was the nation 's second largest rail center , after Chicago .
Delaware North Companies are headquartered in Buffalo .
Buffalo also serves as the seat of Erie County with 6 of the 15 county legislators representing at least a portion of Buffalo .
The last time anyone other than a Democrat held the position of Mayor in Buffalo was 1954 .
Griffin switched political allegiance several times during his 14 years as Mayor , generally hewing to socially conservative platforms .
His successor , Democrat Anthony M. Masiello ( elected in 1993 ) continued to campaign on social conservatism , often crossing party lines in his endorsements and alliances .
One of Buffalo 's many monikers is the City of Trees , which describes the abundance of green in the city .
In fact , Buffalo has more than 20 parks with multiple ones being accessible from any part of the city .
Begun in 1868 by Frederick Law Olmsted and his partner Calvert Vaux , the system was integrated into the city and marks the first attempt in America to lay out a coordinated system of public parks and parkways .
Situated at the confluence of Lake Erie and the Buffalo and Niagara Rivers , Buffalo is a waterfront city .
Buffalo 's waterfront remains , though to a lesser degree , a hub of commerce , trade , and industry .
As of 2009 , a significant portion of Buffalo 's waterfront is being transformed into a focal point for social and recreational activity .
Buffalo 's intent is to stress its relatively architectural and historical heritage , creating a tourism destination .
The loss of traditional jobs in manufacturing , rapid suburbanization and high costs of labor have led to economic decline , making Buffalo one of the poorest amongst U.S. cities with populations of more than 250,000 people .
An estimated 28.7 % of Buffalo residents live below the poverty line ; only Detroit and Cleveland have higher rates .
This , in part , has led to the Buffalo-Niagara Falls metropolitan area having the most affordable housing market in the U.S. today .
Buffalo faces issues with vacant and abandoned houses , as the city ranks second only to St. Louis on the list of American cities with the most vacant properties per capita .
Mayor Byron W. Brown recently unveiled a $ 100 million , five-year plan to demolish 5,000 more houses .
In July 2005 , Reader 's Digest ranked Buffalo as the third cleanest large city in the nation .
" The Queen City " , Buffalo 's most common moniker , first appeared in print in the 1840s , referring to the city 's status as the second largest city in New York State after New York City .
" The Queen City " was also used during the 1800s to describe Buffalo as the second largest American city on the Great Lakes after Chicago .
In the early 20th century , the city began calling itself the City of Light both because of the plentiful hydroelectric power made possible by nearby Niagara Falls and because it was the first city in America to have electric street lights .
Buffalo was first settled primarily by New Englanders .
The newest immigrants are from Somalia , Sudan and Asia .
Buffalo 's Jewish Community centers are located in the Delaware District and Amherst .
Buffalo is also a two-time winner of the All-America City Award .
As a melting pot of cultures , cuisine in the Buffalo area reflects a variety of influences .
Teressa Bellissimo , the chef/owner of the city 's Anchor Bar , first prepared the now-widespread chicken wings here on October 3 , 1964 .
Buffalo 's pizza is unique .
Labatt USA , the US operation for Labatt Beer , a Toronto-based brewer , is headquartered in Buffalo .
Buffalo has several specialty import/grocery stores in old ethnic neighborhoods , and is home to an eclectic collection of cafes and restaurants that serve adventurous , cosmopolitan fare .
Several well-known food companies are based in Buffalo .
His company , Rich Products , is one of the city 's largest private employers .
One of the country 's largest cheese manufacturers , Sorrento , has been here since 1947 .
Archer Daniels Midland operates its largest flour mill in the city .
[ citation needed ] Buffalo is home to one of the largest privately held food companies in the world , Delaware North Companies , which operates concessions in sports arenas , stadiums , resorts , and many state & federal parks .
Buffalo is home to over 50 private and public art galleries , most notably the Albright-Knox Art Gallery , home to a world-class collection of modern and contemporary art .
The Buffalo Philharmonic Orchestra , which performs at Kleinhans Music Hall , is one of the city 's most prominent performing arts institutions .
Shea 's Performing Arts Center , long known as Shea 's Buffalo , is an 1920s movie palace that continues to show productions and concerts .
Buffalo is also home to the second largest free outdoor Shakespeare festival in the United States , Shakespeare in Delaware Park .
Buffalo is also the founding city for several mainstream bands and musicians , most famously Rick James , and The Goo Goo Dolls .
Jazz fusion band Spyro Gyra also got its start in Buffalo .
10,000 Maniacs are from nearby Jamestown , but got their start in Buffalo , which led to lead singer Natalie Merchant launching a successful solo career .
Malevolent Creation and moe. also both started off in Buffalo .
The Reign of Kindo , with their unique blend of smooth jazz mixed with hard driven piano and beats , has brought a new flavor to Buffalo music .
All of the major American architects of the 19th and early 20th century built masterpieces in Buffalo , most of which are still standing .
The country 's largest intact parks system designed by Frederick Law Olmsted and Calvert Vaux , including Delaware Park .
Buffalo was the first city for which Olmsted designed ( in 1869 ) an interconnected park and parkway system rather than stand-alone parks .
The Guaranty Building , by Louis Sullivan , was one of the first steel-supported , curtain-walled buildings in the world , and its thirteen stories made it , at the time it was built ( 1895 ) , the tallest building in Buffalo and one of the world 's first true skyscrapers .
The grounds of this hospital were designed by Olmsted .
Though currently in a state of disrepair , New York State has allocated funds to restore this treasure .
There are several buildings by Frank Lloyd Wright , including the Darwin D. Martin House , George Barton House , William R. Heath House , Walter V. Davidson House , The Graycliff Estate , as well as the now demolished Larkin Administration Building .
Buffalo has more Frank Lloyd Wright buildings than any other city except Chicago .
Last call is at 4 a.m. in Buffalo , rather than 2 a.m. as in most other areas of the U.S .
It is also because N.Y. law allows bars to be open until 4 a.m. ( However , local municipalities can override it to an earlier time . )
Buffalo is served by the Buffalo Niagara International Airport , located in Cheektowaga .
The Niagara Frontier Transportation Authority ( NFTA ) operates Buffalo Niagara International Airport and Niagara Falls International Airport , and the public transit system throughout the Buffalo area .
The NFTA operates bus lines throughout the city and suburbs , as well as the Metro Rail transit system in the city .
The Metro Rail is a 6.4 miles ( 10.3 km ) long , single line light rail system that extends from Erie Canal Harbor in downtown Buffalo to the University Heights district in the northern part of the city .
A large amount of hazardous cargo also crosses through the Buffalo area , such as liquid propane and anhydrous ammonia .
Buffalo is at the eastern end of Lake Erie , one of the Great Lakes , which boasts the greatest variety of freshwater sportfish in the country .
The city has an extensive breakwall system protecting its inner and outer Lake Erie harbors , which are maintained at commercial navigation depths for Great Lakes freighters .
A Lake Erie tributary that flows through south Buffalo is the Buffalo River , for which the city is named .
The field office covers all of Western New York and parts of the Southern Tier and Central New York .
Buffalo is also the location of the chief judge , U.S. Attorney , and administrative offices for the United States District Court for the Western District of New York .
Buffalo has a number of sister cities as designated by Sister Cities International :
Buffalo also has parternships with the following towns :
