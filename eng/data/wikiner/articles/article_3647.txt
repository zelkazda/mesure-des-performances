Ethan Allen ( January 21 , 1738 [ O.S. January 10 , 1737 ] -- February 12 , 1789 ) was a farmer , businessman , land speculator , philosopher , writer , and American Revolutionary War patriot , hero , and politician .
Born in rural Connecticut , Allen had a frontier upbringing but also received an education that included some philosophical teachings .
In the late 1760s he became interested in the New Hampshire Grants , buying land there and becoming embroiled in the legal disputes surrounding the territory .
In September 1775 Allen led a failed attempt on Montreal that resulted in his capture by British authorities .
First imprisoned aboard Royal Navy ships , he was paroled in New York City , and finally released in a prisoner exchange in 1778 .
Allen wrote accounts of his exploits in the war that were widely read in the 19th century , as well as philosophical treatises and documents relating to the politics of Vermont 's formation .
His business dealings included successful farming operations , one of Connecticut 's early iron works , and land speculation in the Vermont territory .
Land purchased by Allen and his brothers included tracts of land that eventually became Burlington , Vermont .
The family moved to Cornwall shortly after his birth .
Seven siblings , all of whom survived to adulthood , joined the family between Allen 's birth and 1751 .
Although not very much is known about Allen 's childhood , the town of Cornwall was frontier territory in the 1740s .
By the time Allen reached his teens , the area , while still a difficult area in which to make a living , began to resemble a town , with wood-frame houses beginning to replace the rough cabins of the early settlers .
Allen had , before his father 's death , begun studies under a minister in the nearby town of Salisbury with the goal of gaining admission to Yale College .
Allen 's brother Ira recalled that , even at a young age , Ethan was curious and interested in learning .
Allen was forced to end his studies upon his father 's death .
Even though the French and Indian War continued over the next several years , Allen did not apparently participate in any further military activities , and is presumed to have tended his farm , at least until 1762 .
In that year , he became part owner of an iron furnace in Salisbury .
Allen bought a small farm and proceeded to develop the iron works .
The Allen brothers sold their interest in the iron works in October 1765 .
By most accounts Allen 's marriage was an unhappy one .
In contrast Allen 's behavior was sometimes quite flamboyant , and he maintained an interest in learning .
Allen 's exploits in those years introduced him to the wrong side of the justice system , which would become a recurring feature of his life .
The neighbor sued to have the animals returned to him ; Allen plead his own case , and lost .
He was also called to court in Salisbury for inoculating himself against smallpox , a procedure that at the time required the sanction of the town selectmen .
When he moved to Salisbury , Allen met Thomas Young , a doctor living and practicing just across the provincial boundary in New York .
He was asked to leave Northampton in July 1767 by the authorities ; while no official reason is known , biographer Michael Bellesiles suggests that religious differences and Allen 's tendency to be disruptive may have played a role in his departure .
It is likely that his first visits to the New Hampshire Grants occurred during these years .
While Sheffield would be the family home for ten years , Allen was often absent for extended periods .
As early as 1749 , Benning Wentworth , New Hampshire 's governor , was selling land grants in the area west of the Connecticut River , to which New Hampshire had always laid somewhat dubious claim .
Before departing for Bennington with the news , Allen was visited by Duane , who offered Allen payments which Duane described in his diary as payment " for going among the people to quiet them " .
Allen denied taking any money ; even if he did , none of his actions appeared to honor Duane 's request .
According to Allen 's account he was outraged , and left his visitors with veiled threats , indicating that attempts to enforce the judgment would be met with resistance .
These discussions resulted in the formation of the Green Mountain Boys , with local militia companies in each of the surrounding towns .
Allen was named their Colonel Commandant , and cousins Seth Warner and Remember Baker were captains of two of the companies .
Further meetings resulted in the creation of committees of safety , and laid down rules by which to resist attempts by the N.Y. provincial government to establish its authority .
While Allen participated in some of the actions to drive surveyors away , he also spent much time exploring the entire territory , probably ranging as far north as the site of Burlington in his wanderings .
After selling off some of his Conn. properties , he began buying wild lands further north in the territory , which he sold at a profit as the southern settlements grew and people began to move further north .
Allen detained two of the settlers and forced them to watch the torching of their newly-constructed cabins .
In response , New York 's governor , William Tryon , issued warrants for the arrests of those responsible , and eventually put a price of £ 20 on the heads of six participants , including Allen .
Allen and his cohorts countered by issuing offers of their own .
Most of the resistance activity had until then taken place on the west side of the Green Mountains ; on March 13 , a small riot in the shire town of Westminster , on the east side of the mountains , resulted in the death of two men .
The preparation of the petition was assigned to a committee which included Allen .
Less than a week after the Westminster convention ended , while Allen and the committee worked on their petition , the American Revolutionary War began .
In late April , following the battles of Lexington and Concord , Allen received a message from members of an irregular Connecticut militia that they were planning to capture Fort Ticonderoga , requesting his assistance in the effort .
Allen , whether motivated by patriotic impulses ( as he describes in his account of the events ) , or by the realization that the action might improve the political position of his side in the grants disputes , agreed to help , and began rounding up the Green Mountain Boys .
The next morning , Allen was elected to lead the expedition , and a dawn raid was planned for May 10 .
On the afternoon of May 9 , Benedict Arnold quite unexpectedly arrived on the scene .
The assembled men refused to acknowledge his authority , insisting they would only follow Allen 's lead .
In a private discussion , Allen and Arnold reached an accommodation , the essence of which was that Arnold and Allen would both be at the front of the troops when the attack on the fort was made .
However , only 83 men made it to the other side of the lake before Allen and Arnold , concerned that dawn was approaching , decided to attack .
Allen went directly to the fort commander 's quarters , seeking to force his surrender .
A detachment of the boys under Seth Warner 's command went to nearby Fort Crown Point and captured the small garrison there .
After two days without significant food ( which they had forgotten to provision in the boats ) , Allen 's small fleet met Arnold 's on its way back to Ticonderoga near the foot of the lake .
Allen , likely both stubborn in his determination , and envious of Arnold , persisted .
Rather than attempt an ambush on those troops , which significantly outnumbered his tired company , Allen withdrew to the other side of the river , where the men collapsed with exhaustion and slept without sentries through the night .
When the expedition returned to Ticonderoga two days later , some of the men were greatly disappointed that they had nothing to show for the effort and risks they took .
Both Allen and Arnold protested these measures , pointing out that doing so would leave the northern border wide open .
Allen , in one instance , wrote that " I will lay my life on it , that with fifteen hundred men , and a proper artillery , I will take Montreal " .
Following a brief visit to their families , they returned to Bennington to spread the news .
Allen went to Ticonderoga to join Schuyler , while Warner and others raised the regiment .
By a wide margin , Seth Warner was elected to lead the regiment .
Warner was viewed as a more stable and quieter choice , and was someone that also commanded respect .
The history of Warner 's later actions in the revolution ( notably at Hubbardton and Bennington ) may be seen as a confirmation of the choice made by the Dorset meeting .
In the end , Allen took the rejection in stride , and managed to convince Schuyler and Warner to permit him to accompany the regiment as a civilian scout .
The American invasion of Quebec departed from Ticonderoga on August 28 .
They were successful enough in gaining support from the habitants that Quebec 's governor , General Guy Carleton , reported that " they have injured us very much " .
When he returned from that expedition 8 days later , Brigadier General Richard Montgomery had assumed command of the invasion due to Schuyler 's illness .
Accompanied by a small number of Americans , he again set out , traveling through the countryside to Sorel , before turning to follow the Saint Lawrence River up toward Montreal , recruiting upwards of 200 men .
Allen and about 100 men crossed the Saint Lawrence that night , but Brown and his men , who were to cross the river at Laprairie , did not .
General Carleton , alerted to Allen 's presence , mustered every man he could , and , in the Battle of Longue-Pointe , scattered most of Allen 's force , and captured him and about 30 men .
General Schuyler , upon learning of Allen 's capture , wrote , " I am very apprehensive of disagreeable consequences arising from Mr. Allen 's imprudence .
Much of what is known of Allen 's captivity is known only from his own account of the time ; where contemporary records are available , they tend to confirm those aspects of his story .
He was kept in solitary confinement and chains , and General Richard Prescott had , according to Allen , ordered him to be treated " with much severity " .
The people of Cork , when they learned that the famous Ethan Allen was in port , took up a collection to provide him and his men with clothing and other supplies .
Much of the following year was spent on prison ships off the American coast .
With the financial assistance of his brother Ira , he lived comfortably , if out of action , until August 1777 .
According to another prisoner 's account , Allen wandered off after learning of his son 's death .
There he remained while Vt. declared independence , and John Burgoyne 's campaign for the Hudson River met a stumbling block near Bennington in August 1777 .
On May 3 , 1778 he was transferred to Staten Island .
Allen stayed there for two days and was treated politely .
On the third day Allen was exchanged for Colonel Archibald Campbell , who was conducted to the exchange by Colonel Elias Boudinot , the American commissary general of prisoners appointed by General George Washington .
Following the exchange , Allen reported to Washington at Valley Forge .
On May 14 , he was breveted a colonel in the Continental Army in " reward of his fortitude , firmness and zeal in the cause of his country , manifested during his long and cruel captivity , as well as on former occasions, " and given military pay of $ 75 per month .
The brevet rank , however , meant that there was no active role , until called , for Allen .
Allen 's services were never requested , and eventually the payments stopped .
Following his visit to Valley Forge , Allen traveled to Salisbury , arriving on May 25 , 1778 .
He then set out for Bennington , where news of his impending return preceded him , and he was met with all of the honor due a military war hero .
There he learned that the Vermont Republic had declared independence in 1777 , that a constitution had been drawn up , and that elections had been held .
Allen wrote of this homecoming that " we passed the flowing bowl , and rural felicity , sweetened with friendship , glowed in every countenance " .
The next day he went to Arlington to see his family and his brother Ira , whose prominence in Vermont politics had risen considerably during Allen 's captivity .
Allen spent the next several years involved in Vermont 's political and military matters .
While his family remained in Arlington , he spent most of his time either in Bennington or on the road , where he could avoid his wife 's nagging .
Allen was appointed to be one of the judges responsible for deciding whose property was subject to seizure under the law .
( This law was so successful at collecting revenue that Vt. did not impose any taxes until 1781 . )
Clinton , who considered Vt. to still be a part of New York , did not want to honor the actions of the Vt. tribunals ; Stark , who had custody of the men , disagreed with Clinton .
Eventually the dispute made its way to George Washington , who essentially agreed with Stark since he desperately needed the general 's services .
The prisoners were eventually transported to West Point , where they remained in " easy imprisonment " .
Clinton 's response , once he recovered his temper , was to issue another proclamation little different from the first .
While largely accurate , it notably omits Benedict Arnold from the capture of Ticonderoga , and Seth Warner as the leader of the Green Mountain Boys .
Allen appeared before the Continental Congress as early as September 1778 on behalf of Vermont , seeking recognition as an independent state .
The negotiations , once details of them were published , were often described by opponents of Vermont statehood as treasonous , but no such formal charges were ever laid against anyone involved .
Vermont 's government had also become more than a clique dominated by the Allen and Chittenden families due to the territory 's rapid population growth .
While they had not always been close , and Allen 's marriage had often been strained , Allen felt these losses deeply .
The work was a typical Allen polemic , but its target was religious , not political .
Specifically targeted against Christianity , it was an unbridled attack against the Bible , established churches , and the powers of the priesthood .
While historians disagree over the exact authorship of the work , the writing contains clear indications of Allen 's style .
Allen 's publisher had forced him to pay the publication costs up front , and only 200 of the 1,500 volumes printed were sold .
The theologically conservative future president of Yale , Timothy Dwight , opined that " the style was crude and vulgar , and the sentiments were coarser than the style .
Allen took the financial loss and the criticism in stride , observing that most of the critics were clergymen , whose livelihood he was attacking .
Fanny had a settling effect on Allen ; for the remainder of his years he did not embark on many great adventures .
In his later years , independent Vermont continued to experience rapid population growth , and Allen sold a great deal of his land , but also reinvested much the proceeds in more land .
Allen and his family moved to Burlington in 1787 , which was no longer a small frontier settlement but a small town , and much more to Allen 's liking than the larger community that Bennington had become .
While accounts of the return journey are not entirely consistent , Allen apparently suffered an apoplectic fit en route , and was unconscious by the time they returned home .
Allen died at home several hours later , without ever regaining consciousness .
The funeral was attended by dignitaries from the Vermont government , and by large numbers of common folk who turned out to pay respects to a man many considered their champion .
Allen 's death made nationwide headlines .
Yale 's Timothy Dwight expressed satisfaction that the world no longer had to deal with a man of " peremptoriness and effrontery , rudeness and ribaldry " .
Two of Allen 's sons went on to serve in the United States military .
His daughter Fanny achieved notice when she converted to Roman Catholicism and entered a convent .
Sometime in the early 1850s , the original plaque marking Allen 's grave disappeared ; its original text was preserved by early war historian Benson Lossing in the 1840s .
While there is a vault beneath the 1858 monument , it contains a time capsule from the time of the monument 's erection , and not Allen 's remains .
No likenesses of Allen made from life have been found , in spite of numerous attempts to locate them .
The nearest potential images included one claimed to be by noted Revolutionary War era engraver Pierre Eugene du Simitiere that turned out to be a forgery , and a reference to a portrait possibly by Ralph Earl that has not been found .
" His figure was that of a robust , large-framed man , worn down by confinement and hard fare ; but he was now recovering his flesh and spirits ; and a suit of blue clothes , with a gold laced hat that had been presented to him by the gentlement of Cork , enabled him to make a very passable appearance for a rebel colonel ... I have seldom met with a man , possessing , in my opinion , a stronger mind , or whose mode of expression was more vehement and oratorical .
Notwithstanding that Allen might have had something of the insubordinate , lawless frontier spirit in his composition ... he appeared to me to be a man of generosity and honor . "
Situated in Burlington , Allen 's homestead is available for viewing via guided tours .
Allen 's name is the trademark of the furniture and housewares manufacturer , Ethan Allen Inc. , which was founded in 1932 in Beecher Falls , Vt. .
The Ethan Allen Express , an Amtrak train line running from New York City to Rutland , Vt. , is also named after him .
Allen is known to have written the following publications :
