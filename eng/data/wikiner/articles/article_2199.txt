The word " community " is derived from the Old French communité which is derived from the Latin communitas ( cum , " with/together " + munus , " gift " ) , a broad term for fellowship or organized society .
Since the advent of the Internet , the concept of community no longer has geographical limitations , as people can now virtually gather in an online community and share common interests regardless of physical location .
Social capital is defined by Robert D. Putnam as " the collective value of all social networks ( who people know ) and the inclinations that arise from these networks to do things for each other ( norms of reciprocity ) . "
Putnam found that over the past 25 years , attendance at club meetings has fallen 58 percent , family dinners are down 33 percent , and having friends visit has fallen 45 percent .
Sociologist Ray Oldenburg states in The Great Good Place that people need three places : 1 ) the home , 2 ) the office , and , 3 ) the community hangout or gathering place .
Gad Barzilai has critically examined both liberalism and communitarianism and has developed the theory of critical communitarianism .
Gad Barzilai has accordingly offered how to protect human rights , individual rights , and multiculturalism in inter-communal context that allows to generating cultural relativism .
The norms of tolerance , reciprocity , and trust are important " habits of the heart, " as de Tocqueville put it , in an individual 's involvement in community .
Peck believes that conscious community building is a process of deliberate design based on the knowledge and application of certain rules .
More recently Peck remarked that building a sense of community is easy but maintaining this sense of community is difficult in the modern world .
The ARISE Detroit ! coalition and the Toronto Public Space Committee are examples of activist networks committed to shielding local communities from government and corporate domination and inordinate influence .
[ citation needed ] M. Scott Peck expresses this in the following way : " There can be no vulnerability without risk ; there can be no community without vulnerability ; there can be no peace , and ultimately no life , without community . "
