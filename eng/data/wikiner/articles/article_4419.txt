The word " functor " was borrowed by mathematicians from the philosopher Rudolf Carnap .
Carnap used the term " functor " to stand in relation to functions analogously as predicates stand in relation to properties .
For Carnap then , unlike modern category theory 's use of the term , a functor is a linguistic item .
