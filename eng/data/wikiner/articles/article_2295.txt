The term originates from the Latin punctus contra punctum meaning " point against point " .
Bach 's counterpoint -- often considered the most profound synthesis of the two dimensions ever achieved -- is extremely rich harmonically and always clearly directed tonally , while the individual lines remain fascinating .
Hence , the earlier composer Josquin des Prez is said to have written polyphonic music .
Species counterpoint is a type of so-called strict counterpoint , developed as a pedagogical tool , in which a student progresses through several " species " of increasing complexity , always working with a very plain given part in the cantus firmus ( Latin for " fixed melody " ) .
Zacconi , unlike later theorists , included a few extra contrapuntal techniques as species , for example invertible counterpoint .
By far the most famous pedagogue to use the term , and the one who made it famous , was Johann Joseph Fux .
As the basis for his simplified and often over-restrictive codification of Palestrina 's practice ( see General notes , below ) , Fux described five species :
A succession of later theorists imitated Fux 's seminal work quite closely , but often with some small and idiosyncratic modifications in the rules .
A good example is Luigi Cherubini .
A few further rules given by Fux , by study of the Palestrina style , and usually given in the works of later counterpoint pedagogues , are as follows .
Fux 's book and its concept of " species " was purely a method of teaching counterpoint , not a definitive or rigidly prescriptive set of rules for it .
He arrived at his method of teaching by examining the works of Palestrina , an important late 16th-century composer who in Fux 's time was held in the highest esteem as a contrapuntist .
Works in the contrapuntal style of the 16th century -- the " prima pratica " or " stile antico " , as it was called by later composers -- were often said by Fux 's contemporaries to be in " Palestrina style . "
Indeed , Fux 's treatise is a compendium of Palestrina 's actual techniques , simplified and regularised for pedagogical use ( and so permitting fewer liberties than occurred in actual practice ) .
Dissonant counterpoint was first theorized by Charles Seeger as " at first purely a school-room discipline, " consisting of species counterpoint but with all the traditional rules reversed .
Seeger was not the first to employ dissonant counterpoint , but was the first to theorize and promote it .
Other composers who have used dissonant counterpoint , if not in the exact manner prescribed by Charles Seeger , include Ruth Crawford-Seeger , Carl Ruggles , Henry Cowell , Henry Brant , Dane Rudhyar , Lou Harrison , Fartein Valen , and Arnold Schoenberg .
Gould called this method " contrapuntal " radio .
