Dewar flasks are named after their inventor , James Dewar , the man who first liquefied hydrogen .
( This was a bone of contention between him and rival engine designer Valentin Glushko , who felt that cryogenic fuels were impractical for large-scale rockets such as the ill-fated N-1 rocket spacecraft . )
