Some calves are ear tagged soon after birth , especially those that are stud cattle in order to correctly identify their dams , or in areas ( such as the EU ) where tagging is a legal requirement for cattle .
Many calves are also weaned when they are taken to the large weaner auction sales that are conducted in the south eastern states of Australia .
In the United States these weaners may be known as feeders and would be placed directly into feedlots .
