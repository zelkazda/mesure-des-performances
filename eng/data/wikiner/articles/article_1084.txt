The Aleuts are the indigenous people of the Aleutian Islands of Alaska , United States and Kamchatka Krai , Russia .
The homeland of the Aleuts includes the Aleutian Islands , the Pribilof Islands , the Shumagin Islands , and the far western part of the Alaska Peninsula .
During the 19th century , the Aleuts were deported from the Aleutian Islands to the Commander Islands ( now part of Kamchatka Krai ) by the Russian-American Company .
After the arrival of missionaries in the late 18th century , many Aleuts became Christian by joining the Russian Orthodox Church .
The locally resident Nicoleño nation sought a payment from the Aleut hunters for the large number of otters being killed in the area .
Prior to major influence from outside , there were approximately 25,000 Aleuts on the archipelago .
Traditional arts of the Aleuts include hunting , weapon-making , building of baidarkas ( special hunting boats ) , and weaving .
Andrew Gronholdt of the Shumagin Islands played a vital role in reviving the ancient art of building the chaguda-x or traditional bentwood hats .
Aleut basketry is some of the finest in the world , and the tradition began in prehistoric times .
Early Aleut women created baskets and woven mats of exceptional technical quality using only an elongated and sharpened thumbnail as a tool .
Today , Aleut weavers continue to produce woven pieces of a remarkable cloth-like texture , works of modern art with roots in ancient tradition .
The Aleut term for grass basket is qiigam aygaaxsii .
Fishing , hunting and gathering were the only way Aleuts could find food .
These days Aleuts eat their traditional food but also with the new processed foods the outside world brought in .
Traditional Aleut clothing for the men was a seal skin kamleika ( long robe ) often embroidered with wool and beach rye for aristocrats .
The famous Aleut visor was only worn outside in cold weather or in dances .
Harpoons , spears , bows and arrows , paddle clubs were all made by the Aleuts .
The Pribilof Islands boast the highest number of active speakers of Aleutian .
Dana Stabenow has published a series of mystery novels set in Alaska .
