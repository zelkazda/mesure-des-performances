The Armed Forces of the Democratic Republic of Congo ( French : Forces Armées de la République Démocratique du Congo ( FARDC ) ) is the state military organisation responsible for defending the Democratic Republic of Congo .
The FARDC is being rebuilt as part of the peace process which followed the end of the Second Congo War in July 2003 .
The majority of FARDC members are land forces , but it also has a small air force and an even smaller navy .
The government in the capital city Kinshasa , the United Nations , the European Union , and bilateral partners which include Angola , South Africa , and Belgium are attempting to create a viable force with the ability to provide the DRC with stability and security .
To assist the new government , since February 2000 the United Nations has had the United Nations Mission in the Democratic Republic of Congo ( MONUC ) , which currently has a strength of over 16,000 peacekeepers in the country .
Its principal tasks are to provide security in key areas , such as the Sud-Kivu and Nord-Kivu in the east , and to assist the government in reconstruction .
Foreign rebel groups are also in the Congo , as they have been for most of the last half-century .
It was first conceived in 1885 by King Léopold who held the Congo Free State as his private property , ordered his Secretary of the Interior to create military and police forces for the state .
In 1908 , under international pressure Léopold ceded administration of the colony to the government of Belgium as the Belgian Congo .
This was because the FP had always only been officered by Belgian or other expatriate whites .
During the crucial period of July-August 1960 , Mobutu Sese Seko built up " his " national army by channeling foreign aid to units loyal to him , by exiling unreliable units to remote areas , and by absorbing or dispersing rival armies .
This invasion is sometimes known as Shaba I . Mobutu had to request assistance , which was provided by Morocco in the form of regular troops who routed the MPLA and their Cuban advisors out of Katanga .
He also redeployed his forces throughout the country instead of keeping them close to Kinshasa , as had previously been the case .
It is organized into three battalions assigned to Mbandaka , Kisangani , and Kamina , but only the battalion at Kamina is adequately staffed ; the others are little more than skeleton " units .
What operational abilities the armed forces had were gradually destroyed by politicisation of the forces , tribalisation , and division of the forces , included purges of suspectedly disloyal group , intended to allow Mobutu to divide and rule .
All this occurred against the background of increasing deterioration of state structures under the kleptocratic Mobutu regime .
Within the largest refugee camps , beginning in Goma in Nord-Kivu , were Rwandan Hutu fighters , which were eventually organised into the Rassemblement Démocratique pour le Rwanda , who launched repeated attacks into Rwanda .
On being asked by a Belgian journalist what was the actual army command structure apart from himself , Kabila answered ' We are not going to expose ourselves and risk being destroyed by showing ourselves openly ... .
The FARDC performed poorly throughout the Second Congo War and " demonstrated little skill or recognisable military doctrine " .
As well as providing expeditionary forces , these countries unsuccessfully attempted to retrain the DRC Army .
North Korea and Tanzania also provided assistance with training .
These successes contributed to the Lusaka Ceasefire Agreement which was signed in July 1999 .
Katanga and Equateur would fall under the fourth and fifth regions , respectively , while Kasai Occidental and Bandundu would form the sixth region .
Kinshasa and Bas-Congo would form the seventh and eighth regions , respectively .
This force was intended to support the FARDC and national police but never became effective .
The Lusaka Ceasefire Agreement was not successful in ending the war , and fighting resumed in September 1999 .
The defeats in 2000 are believed to have been the cause of President Kabila 's assassination in January 2001 .
Following the assassination Joseph Kabila assumed the presidency and was eventually successful in negotiating an end to the war in 2003 .
In the far northeast this is due primarily to the Ituri conflict .
In 2009 , several United Nations officials stated that the army is a major problem , largely due to corruption that results in food and pay meant for soldiers being diverted and a military structure top-heavy with colonels , many of whom are former warlords .
In a 2009 report itemizing FARDC abuses , Human Rights Watch urged the UN to stop supporting government offensives against eastern rebels until the abuses ceased .
Below the Chief of Staff , the current organisation of the FARDC is not fully clear .
It should be made clear also that Joseph Kabila does not trust the military ; the Republican Guard is the only component he trusts .
Previously Numbi negotiated the agreement to carry out the mixage process with Laurent Nkunda .
Those who choose to stay within the FARDC are then transferred to one of six integration centres for a 45-day training course , which aims to build integrated formations out of factional fighters previously heavily divided along ethnic , political and regional lines .
Amid the other difficulties in building new armed forces for the DRC , in early 2007 the integration and training process was distorted as the DRC government under Kabila attempted to use it to gain more control over the dissident general Laurent Nkunda .
Due to Nkunda 's troops having greater cohesion , Nkunda effectively gained control of all five brigades -- not what the DRC central government had been hoping !
However after Nkunda used the mixage brigades to fight the FDLR , strains arose between the FARDC and Nkunda-loyalist troops within the brigades and they fell apart in the last days of August 2007 .
Attempting to list the equipment available to the DRC 's land forces is difficult ; most figures are unreliable estimates based on known items delivered in the past .
They are still deployed at Kisangani 's Bangoka airport , where they appear to answer to no local commander and have caused trouble with MONUC troops there .
There are currently large numbers of United Nations troops stationed in the DRC .
Groups of anti-Rwandan government rebels like the FDLR , and other foreign fighters remain inside the DRC .
Finally there is a government paramilitary force , created in 1997 under President Laurent Kabila .
Foreign private military companies have reportedly been contracted to provide the DRC 's aerial reconnaissance capability using small propeller aircraft fitted with sophisticated equipment .
It was initially placed under command of the MLC when the transition began : the current situation is uncertain .
Before the downfall of Mobutu a small navy operated on the Congo river .
One of its installations was at the village of N'dangi near the presidential residence in Gbadolite .
The port at N'dangi was the base for several patrol boats , helicopters and the presidential yacht .
