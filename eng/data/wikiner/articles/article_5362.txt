His parents , Gregory and Nonna , were wealthy land-owners .
Upon finishing his education he taught rhetoric in Athens for a short time .
However , Basil urged him to return home to assist his father , which he did for the next year .
Basil , who had long displayed inclinations to the episcopacy , was elected bishop of the see of Caesarea in Cappadocia in 370 .
He made little effort to administer his new diocese , complaining to Basil that he preferred instead to pursue a contemplative life .
He instead focused his attention on his new duties as co-adjutor of Nazianzus .
At the end of 375 he withdrew to a monastery at Seleukia , living there for three years .
Near the end of this period his friend Basil died .
Emperor Valens died in 378 .
Was it better to blaze his own path or follow the course mapped for him by his father and Basil ?
He is especially noted for his contributions to the field of pneumatology -- that is , theology concerning the nature of the Holy Spirit .
His orations were cited as authoritative by the First Council of Ephesus in 431 .
Following his death , Saint Gregory was buried at Nazianzus .
Part of the relics were taken from Constantinople by Crusaders during the Fourth Crusade , in 1204 , and ended up in Rome .
On November 27 , 2004 , those relics , along with those of John Chrysostom , were returned to Istanbul -- Constantinople by Pope John Paul II , with the Vatican retaining a small portion of both .
The Church of England commemorates him , with Basil the Great , on January 2 ; the Episcopal Church commemorates him on March 9 .
