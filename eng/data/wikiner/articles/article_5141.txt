The GRE physics test is an examination administered by the Educational Testing Service ( ETS ) .
The scope of the test is largely that of the first three years of a standard U.S. undergraduate physics curriculum , since many students who plan to continue to graduate school apply during the first half of the fourth year .
The table below indicates the relative weights , as asserted by ETS , and detailed contents of the major topics .
