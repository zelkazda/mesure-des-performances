The Demographics of Armenia is about the demographic features of the population of Armenia , including population growth , population density , ethnicity , education level , health , economic status , religious affiliations , and other aspects of the population .
After registering a steady increase all through the Soviet period , the population of Armenia declined from 3.3 million in 1989 to less than 3.1 million in 2003 .
The Armenian Apostolic Church can trace its roots back to the 3rd and 4th centuries .
There has been a problem of population decline due to elevated levels of emigration after the break-up of the USSR .
The rates of emigration and population decline , however , have decreased drastically in the recent years , and a moderate influx of Armenians returning to Armenia have been the main reasons for the trend , which is expected to continue .
In fact Armenia is expected to resume its positive population growth by 2010 .
Armenian is the only official language even though Russian is widely used , especially in education , and could be considered as de facto " second language " .
94 % of adult Armenians considers that it 's important their children learn Russian .
This article incorporates public domain material from the CIA World Factbook document " 2006 edition " .
