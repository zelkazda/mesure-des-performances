Henry was educated at Swansea Grammar School , and in 1837 was called to the bar .
During this time , he became involved in the management of the Dowlais Iron Company .
In 1876 he was elected a Fellow of the Royal Society ; from 1878 to 1891 he was president of the Royal Historical Society ; and in 1881 he became president of both the Royal Geographical Society and the Girls ' Day School Trust .
They had two sons and seven daughters , of whom the youngest was the mountaineer Charles Granville Bruce .
Lady Aberdare died in April 1897 .
