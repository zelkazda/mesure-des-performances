The Arab diaspora is a global diaspora estimated at between 30 and 50 million people distributed across every continent and almost every country in the world .
The Qur'an does not use the word ʿarab , only the nisba adjective ʿarabiy .
The arrival of Islam united many tribes in Arabia , who then moved northwards to conquer the Levant and Iraq .
They established garrison towns at Ramla , ar-Raqqah , Basra , Kufa , Mosul and Samarra , all of which developed into major cities .
Caliph Umar II strove to resolve the conflict when he came to power in 717 .
By now , discontent with the Umayyads swept the region and an uprising occurred in which the Abbasids came to power and moved the capital to Baghdad .
The Abbassids were influenced by the Qur'anic injunctions and hadith such as " The ink of the scholar is more holy than the blood of martyrs " stressing the value of knowledge .
It is believed that these groups migrated to the Caucasus in the 16th century .
Iranian Arab communities are also found in Khorasan Province .
Some tribes had converted to Christianity or Judaism .
In Lebanon they number about 39 % of the population .
In Palestine before the creation of Israel estimates ranged as high as 25 % , but is now 3.8 % due largely to the 1948 Palestinian exodus .
In West Bank and in Gaza , Arab Christians make up 8 % and 0.8 % of the populations , respectively .
In Israel , Arab Christians constitute 1.7 % .
Arab Christians make up 6 % of the population of Jordan .
From the late 1940s to the early 1960s , following the creation of the state of Israel , most of these Jews left or were expelled from their countries of birth and are now mostly concentrated in Israel .
See Jewish exodus from Arab lands .
Arabic music is the music of Arabic-speaking people or countries , especially those centered around the Arabian Peninsula .
