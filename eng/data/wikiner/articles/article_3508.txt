Keno Don Hugo Rosa , known simply as Don Rosa , ( born June 29 , 1951 ) is an American comic book writer and illustrator known for his stories about Scrooge McDuck , Donald Duck and other characters created by Carl Barks for Disney comics , such as The Life and Times of Scrooge McDuck .
He emigrated to Kentucky , U.S. around 1900 , established a successful tile and terrazzo company , then returned to Italy to marry and start a family .
Don Rosa was born Keno Don Hugo Rosa on June 29 , 1951 in Louisville , Kentucky .
Rosa began drawing comics before being able to write .
Rosa entered the University of Kentucky in 1969 .
His first published comic ( besides the spot illustrations in his grade school and high school newspapers ) was a comic strip featuring his own character , Lancelot Pertwillaby entitled " The Pertwillaby Papers " .
He created the strip in 1971 for The Kentucky Kernel , a college newspaper of the University of Kentucky which wanted the strip to focus on political satire .
( The title is a reference to Lost in the Andes ! , a Donald Duck story by Carl Barks , first published in April , 1949 . )
The so-called Pertwillaby Papers included 127 published episodes by the time Rosa graduated in 1973 .
Meanwhile Rosa participated in contributing art and articles to comic collector fanzines .
By now having become a locally known comics collector and cartoonist , Rosa accepted an offer from the editor of the local newspaper to create a weekly comic strip .
Captain Kentucky was the superhero alter ego of Lancelot Pertwillaby .
The pay was $ 25/week and not worth the 12+ hours each week 's strip entailed , but Rosa did it as part of his hobby .
In 1985 , he discovered a Gladstone Publishing comic book .
This was the first American comic book that contained Disney-characters after the 1970s .
Since early childhood Don Rosa had been fascinated by Carl Barks ' stories about Donald Duck and Scrooge McDuck .
Artist Carl Barks was an especially big idol for him and would remain so for the rest of his career .
He immediately called the editor , Byron Erickson , and told him that he was the only American who was born to write and draw one Scrooge McDuck adventure .
Rosa did a few more comics for Gladstone till 1989 .
He then stopped working for them because the policies of their licensor Disney did not allow for the return of original art for a story to its creators .
This was unacceptable to Don Rosa , since a part of his income came from selling the originals , and the original art is the property of the freelance artists unless otherwise agreed upon .
They even offered him a much higher salary than the one he received at Gladstone .
Rosa joined Egmont in 1990 .
In 1991 he started creating The Life and Times of Scrooge McDuck , a twelve chapter story about his favorite character .
The series was a success , and in 1995 he won an Eisner Award for best continuing series .
Some of the extra chapters were turned down by Egmont because they were not interested in any more episodes .
Some of these chapters were recently compiled as The Life and Times of Scrooge McDuck Companion .
During early summer 2002 , Rosa suddenly laid down work .
As an artist he could not live under the conditions Egmont was offering him , but he did not want to give up making Scrooge McDuck comics either .
So his only choice was to put down work for a while and try to come to an agreement with Egmont .
Rosa had discovered too often that his stories were printed with incorrect pages of art , improper colors , poor lettering , or pixelated computer conversions of the illustrations .
Rosa has never , to this day , received a penny in royalties for a single use of any of his stories worldwide .
He came to an agreement with Egmont in December of the same year , which gave him a bit more control over the stories and the manner in which they were publicized .
Rosa 's eyesight had been very poor since his childhood .
In March 2008 Rosa suffered a severe retinal detachment and underwent emergency eye surgery that ultimately proved to be not completely successful .
Rosa is popular with readers in Europe .
They live in a log house on a 25 acre ( 10 hectare ) nature preserve in the Kentucky hills near Louisville , the maintenance of which now takes up most of the retired couple 's time .
In Europe , Rosa is recognized as one of the best Disney comics creators .
Carl Barks and Rosa are some of the few artists who have their name written on the covers of Disney magazines when their stories are published .
Rosa enjoys including subtle references to his favorite works of fiction as well as his own previous work .
The cover for the comic book was a spoof of a famous painting by Akseli Gallen-Kallela .
With a bachelor of arts degree in civil engineering as his only real drawing education , Rosa has some unusual drawing methods , as he writes himself : " I suspect nothing I do is done the way anyone else does it . "
Because of being self-taught in making comics , Rosa relies mostly on the skills he learned in engineering school -- which means using technical pens and templates a lot .
Rosa 's drawing style is considered much more detailed and " dirtier " than that of most other Disney artists , living or dead , and often likened to that of underground artists , and he is frequently compared to Robert Crumb .
When Rosa was first told of this similarity , he said that he " drew that bad " long before he discovered underground comics during college .
Rosa 's idol when it comes to comics is Carl Barks .
Rosa builds almost all his stories on characters and locations that Barks invented .
Many of Rosa 's stories contain references to some fact pointed out in a Barks story .
At the request of publishers in response to reader demands , Rosa has even created sequels of old Barks stories .
To add more to his admiration and consistency to Barks and Barks ' stories , Rosa makes all his ducks ' stories set in the 50s .
Barks either created most of the characters used by Rosa or is credited for greatly developing their personalities .
Rosa thus feels obliged to make his stories factually consistent .
Especially The Life and Times of Scrooge McDuck was based mostly on the earlier works of Barks .
Rosa admitted however that a scene of the first chapter was inspired by a story by Tony Strobl .
As most of the characters Rosa uses were created by Barks , and because Rosa considers Scrooge rather than Donald to be the main character of the Duck universe , he does not regard himself as a pure Disney artist , nor the characters really as Disney 's .
Barks even claimed to have also created Huey , Dewey and Louie while working as a writer on Donald Duck animated cartoons in 1937 . "
Because of his idolization of Barks , he repeatedly discourages his fans to use an absolutist way of saying his clearly different drawing style would be better than Barks 's , and he found that notion confirmed when Barks himself spoke about Rosa 's style in a critical tone though it is uncertain whether those comments were Barks ' or those of his temporary " business managers " who filtered his communications .
Beside Rosa 's constant effort to remain faithful to the universe Barks created , there is also a number of notable differences between the two artists .
Barks had over 600 Duck stories to his name while Rosa only created 85 until his eye trouble set in , but whereas Barks made many short one and two-pagers centered around a subtle , compact gag , Rosa 's oeuvre consists almost exclusively of long adventure stories .
looked too much like one , Don Rosa has made a habit of hiding the letters in various unlikely places .
D.U.C.K. is also often hidden in Rosa 's cover-art , which he makes for his own stories and reprints of old Carl Barks stories .
Rosa is only interested in creating stories featuring the Duck family , but he often hides small Mickey Mouse heads or figures in the pictures , sometimes in a humiliating or unwanted situation .
An example of this is in the story The Terror of the Transvaal where a flat Mickey can be seen under an elephant 's foot .
Rosa has admitted to neither liking nor disliking Mickey Mouse , but being indifferent to him .
In the second Rosa story featuring The Three Caballeros , Donald Duck is shocked by the sight of a capybara standing on its hind legs , with shrubs , leaves and fruit in front of its body , coincidentally making it look like Mickey Mouse .
José Carioca and Panchito Pistoles , never having seen Mickey Mouse , ask Donald what is wrong , but Donald replies he is just tired .
Mickey can be seen among them .
In The Quest for Kalevala this running gag can be seen on the original , Akseli Gallen-Kallela-inspired cover art .
Heidi MacDonald of Comics Buyer 's Guide also mentioned Rosa 's 1994 story Guardians of the Lost Library as " possibly the greatest comic book story of all time " .
This work not only discusses all of Rosa 's creative life up to 1997 , but it also gives a comprehensive biography , lists up to that date his Disney work and presents an extensive interview with Rosa .
