Ajmer-Merwara remained a province of India from independence in 1947 to 1950 , when it became the state of Ajmer .
The Aravalli Range is the distinguishing feature of the district .
The rain which falls on the southeastern slopes drains into the Chambal , and so into the Bay of Bengal ; that which falls on the northwest side into the Luni River , which discharges itself into the Rann of Kutch .
The south-west monsoon sweeps up the Narmada valley from Bombay and crossing the tableland at Neemuch gives copious supplies to Malwa , Jhalawar and Kota and the countries which lie in the course of the Chambal River .
The agriculturist of Ajmer-Merwara could never rely upon two good harvests in succession .
Trading centers include Beawar and Kekri .
