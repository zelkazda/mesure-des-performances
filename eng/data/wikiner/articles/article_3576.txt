Logical layers process data for a class of devices such as Ethernet ports or disk drives .
The Microsoft Windows .sys files and Linux .ko modules contain loadable device drivers .
Microsoft has attempted to reduce system instability due to poorly written device drivers by creating a new framework for driver development , called Windows Driver Foundation ( WDF ) .
This includes User-Mode Driver Framework ( UMDF ) that encourages development of certain types of drivers -- primarily those that implement a message-based protocol for communicating with their devices -- as user mode drivers .
Device drivers , particularly on modern Windows platforms , can run in kernel-mode or in user-mode .
They are used to emulate a hardware device , particularly in virtualization environments , for example when a DOS program is run on a Microsoft Windows computer or when a guest operating system is run on , for example , a Xen host .
Solaris descriptions of commonly-used device drivers
