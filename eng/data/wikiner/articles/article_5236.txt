Goshen ( pronounced /ˈɡoʊʃən/ ) is a city located in Elkhart County , Ind. , United States .
Goshen is located 10 miles south of Elkhart , 25 miles southeast of South Bend , 120 miles east of Chicago , and 150 miles north of Indianapolis .
According to a 2008 estimate , Goshen 's population is 31,800 .
According to the United States Census Bureau , the city has a total area of 13.4 square miles ( 34.7 km² ) , of which , 13.2 square miles ( 34.2 km² ) of it is land and 0.2 square miles ( 0.5 km² ) of it ( 1.57 % ) is water .
There is a distinct economic/social divide between the north and south sides of the city , the north side being regarded as the area north of the Norfolk Southern Railway tracks .
It is the largest county fair in Indiana and the second largest in attendance in the U.S. .
Goshen is known for the invention of the sirk , which is a covering for shoes with a cloth inside and rubber outside .
In fact , it was the Amish that built the stable with lumber and other supplies donated by Wal-Mart .
In April 2006 , Goshen was the site for an immigration march .
