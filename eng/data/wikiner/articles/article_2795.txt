This form of collective lawsuit originated in the United States and is still predominantly a U.S. phenomenon , at least the U.S. variant of it .
As a result , there are two separate treatises devoted solely to the complex topic of California class actions .
Like most Americans then and since , Story took individualism for granted ; on that basis , he simply could not comprehend a rule that allowed a court to bind someone who had never been a party to litigation purportedly conducted on his behalf .
Story 's confusion was apparently typical of 19th century American lawyers , as even the legendary Christopher Columbus Langdell also could not understand the old cases .
The second development was the rise of the African-American civil rights movement , environmentalism , and consumerism .
In 1999 the National Arbitration Forum began advocating that such contracts should be drafted so as to force consumers to waive the right to a class action completely , and such provisions have become very popular among businesses .
For example , in the United States , class lawsuits sometimes bind all class members with a low settlement .
Quebec was the first province to enact U.S.-style class proceedings legislation in 1978 .
Legislation in Saskatchewan , Manitoba , Ontario , and Nova Scotia expressly or by judicial opinion have been read to allow for what are informally known as national " opt-out " class actions,whereby residents of other provinces may be included in the class definition and potentially be bound by the court 's judgment on common issues unless they opt-out in a prescribed manner and time .
Nevertheless , the bill was withdrawn in January 2007 at the request of Minister of Health Xavier Bertrand .
It is not like class actions in the U.S. -- it only applies to parties who have already filed suit and does not allow a claim to be brought in the name of an unknown group of claimants .
