Cadmium was discovered simultaneously by Friedrich Stromeyer and Karl Samuel Leberecht Hermann , both in Germany , as an impurity in zinc carbonate .
The metal was named after the Latin word for calamine , since the metal was found in this zinc compound .
Even though cadmium and its compounds may be toxic in certain forms and concentrations , the British Pharmaceutical Codex from 1907 states that cadmium iodide was used as a medication to treat " enlarged joints , scrofulous glands , and chilblains " .
In 1927 , the International Conference on Weights and Measures redefined the meter in terms of a red cadmium spectral line ( 1 m = 1,553,164.13 wavelengths ) .
One place where metallic cadmium can be found is the Vilyuy River basin in Siberia .
