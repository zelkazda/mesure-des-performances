He founded the Ottonian dynasty .
Henry designated his son Otto , who was elected King in Aachen in 936 , to be his successor .
A marriage alliance with the widowed queen of Italy gave Otto control over that nation as well .
Otto had gained much of his power earlier , when , in 955 , the Magyars were defeated in the Battle of Lechfeld .
This changed after Henry II died in 1024 without any children .
Conrad II , first of the Salian Dynasty , was then elected king in 1024 only after some debate .
Each king preferred to spend most time in his own homelands ; the Saxons , for example , spent much time in palatinates around the Harz mountains , among them Goslar .
Also , his successors , Henry II , Conrad II , and Henry III , apparently managed to appoint the dukes of the territories .
The Pope however , announced that the oaths of loyalty made to Henry by his vassals were no longer binding , since he 'd been excommunicated .
Suddenly , the emperor found himself with almost no political support and was forced to make the famous Walk to Canossa in 1077 .
Conrad III came to the throne in 1138 .
In order to solve the problem that the emperor was no longer as able to use the church as a mechanism to maintain power , the Staufer increasingly lent land to ministerialia , formerly non-free service men , which Frederick hoped would be more reliable than local dukes .
Cities that were founded in the 12th century include Freiburg , possibly the economic model for many later cities , and Munich .
On the one hand , he concentrated on establishing an innovative state in Sicily , with public services , finances , and other reforms .
In the 1220 Confoederatio cum principibus ecclesiasticis , Frederick gave up a number of regalia in favour of the bishops , among them tariffs , coining , and fortification .
After the death of Frederick II in 1250 , none of the dynasties worthy of producing the king proved able to do so , and the leading dukes elected several competing kings .
After a delay , a fourth elector , Bohemia , joined this choice .
Was the King of Bohemia entitled to change his vote , or was the election complete when four electors had chosen a king ?
Were the four electors together entitled to depose Richard a couple of months later , if his election had been valid ?
This is also revealed in the way the post-Staufen kings attempted to sustain their power .
In 1282 , Rudolph I thus lent Austria and Styria to his own sons .
After him all kings and emperors relied on the lands of their own family : Louis IV of Wittelsbach ( king 1314 , emperor 1328 -- 1347 ) relied on his lands in Bavaria ; Charles IV of Luxembourg , the grandson of Henry VII , drew strength from his own lands in Bohemia .
During this time , the concept of " reform " emerged , in the original sense of the Latin verb re-formare , to regain an earlier shape that had been lost .
For the first time , the assembly of the electors and other dukes was now called Reichstag .
While Frederick refused , his more conciliatory son finally convened the Reichstag at Worms in 1495 , after his father 's death in 1493 .
From 1515 to 1523 , the Habsburg government in the Netherlands also had to contend with the Frisian peasant rebellion , led first by Pier Gerlofs Donia and then by his nephew Wijerd Jelckama .
A side effect of that conflict was the Cologne War , which ravaged much of the upper Rhine .
Imperial power sharply deteriorated by the time of Rudolf 's death in 1612 .
The Peace of Westphalia in 1648 , which ended the Thirty Years ' War , gave the territories almost complete sovereignty .
The Swiss Confederation , which had already established quasi-independence in 1499 , as well as the Northern Netherlands , left the empire .
The German dualism between Austria and Prussia dominated the empire 's history after 1740 .
The German Mediatisation was the series of mediatisations and secularisations that occurred in 1795 -- 1814 , during the latter part of the era of the French Revolution and then the Napoleonic Era .
After the end of the Napoleonic Wars a new German union , the German Confederation , was established in 1815 .
During the Thirty Years ' War , the Duke of Bavaria was given the right to vote as the eighth elector .
No law required him to be a Catholic , though imperial law assumed that he was .
The second class , the Council of Princes , consisted of the other princes .
The Council of Princes was divided into two " benches, " one for secular rulers and one for ecclesiastical ones .
Nevertheless , their participation was formally acknowledged only as late as in 1648 with the Peace of Westphalia ending the Thirty Years ' War .
