The word ansible was coined by Ursula K. Le Guin in her 1966 novel , Rocannon 's World .
Le Guin states that she derived the name from " answerable, " as the device would allow its users to receive answers to their messages in a reasonable amount of time , even over interstellar distances .
The name of the device has since been borrowed by authors such as Orson Scott Card , Vernor Vinge , Elizabeth Moon , Jason Jones , L.A. Graf , and Dan Simmons .
Similarly functioning devices are present in the works of numerous others , such as Frank Herbert and Philip Pullman , who called it a lodestone resonator .
Isaac Asimov solved the same communication problem with the hyper-wave relay in The Foundation Series .
Le Guin 's ansible was said to communicate " instantaneously " , but other authors have adopted the name for devices explicitly only capable of finite-speed communication ( though still faster than light ) .
The subspace radio , best known today from Star Trek and named for the series ' method of achieving faster-than-light travel , was the most commonly used name for such a faster-than-light ( FTL ) communicator in the science fiction of the 1930s to the 1950s .
In The Word for World Is Forest , Le Guin explains that in order for communication to work with any pair of ansibles , at least one " must be on a large-mass body , the other can be anywhere in the cosmos . "
In The Left Hand of Darkness , the ansible " does n't involve radio waves , or any form of energy .
Unlike McCaffrey 's black crystal transceivers , Le Guin 's ansibles are not mated pairs as it is possible for an ansible 's coordinates to be set to any known location of a receiving ansible .
Moreover , the ansibles Le Guin uses in her stories apparently have a very limited bandwidth which only allows for at most a few hundred characters of text to be communicated in any transaction of a dialog session .
Instead of a microphone and speaker , Le Guin 's ansibles are attached to a keyboard and small display to perform text messaging .
Orson Scott Card 's Ender 's Game series uses the ansible as a plot device .
His description of ansible functions in Xenocide involve a fictional subatomic particle , the philote , and contradicts not only standard physical theory but the results of empirical particle accelerator experiments .
In the " Enderverse " , the two quarks inside a pi meson can be separated by an arbitrary distance while remaining connected by " philotic rays " .
