His works established and popularized deductive methodologies for scientific inquiry , often called the Baconian method or simply , the scientific method .
He entered Trinity College , Cambridge , on 5 April 1573 at the age of twelve , living for three years there together with his older brother Anthony under the personal tutelage of Dr John Whitgift , future Archbishop of Canterbury .
He was also educated at the University of Poitiers .
During his travels , Bacon studied language , statecraft , and civil law while performing routine diplomatic tasks .
Having borrowed money , Bacon got into debt .
To support himself , he took up his residence in law at Gray 's Inn in 1579 .
Bacon 's threefold goals were to uncover truth , to serve his country , and to serve his church .
In 1580 , through his uncle , Lord Burghley , he applied for a post at court , which might enable him to pursue a life of learning .
For two years he worked quietly at Gray 's Inn , until admitted as an outer barrister in 1582 .
Bacon 's opposition to a bill that would levy triple subsidies in half the usual time offended many people .
Likewise , Bacon failed to secure the lesser office of Solicitor-General in 1595 .
In 1598 Bacon was arrested for debt .
Gradually , Bacon earned the standing of one of the learned counsels , though he had no commission or warrant and received no salary .
The accession of James I brought Bacon into greater favour .
The following year , during the course of the uneventful first parliament session , Bacon married Alice Barnham .
In June 1607 he was at last rewarded with the office of Solicitor-General .
He sought further promotion and wealth by supporting King James and his arbitrary policies .
In 1613 , Bacon was finally appointed attorney general , after advising the king to shuffle judicial appointments .
As attorney general , Bacon prosecuted Somerset in 1616 .
Bacon 's public career ended in disgrace in 1621 .
He was sentenced to a fine of £ 40,000 , remitted by King James , to be committed to the Tower of London during the king 's pleasure ( his imprisonment lasted only a few days ) .
More seriously , parliament declared Bacon incapable of holding future office or sitting in parliament .
There seems little doubt that Bacon had accepted gifts from litigants , but this was an accepted custom of the time and not necessarily evidence of deeply corrupt behaviour .
When he was 36 , Bacon engaged in the courtship of Elizabeth Hatton , a young widow of 20 .
Reportedly , she broke off their relationship upon accepting marriage to a wealthier man -- Edward Coke .
Years later , Bacon still wrote of his regret that the marriage to Elizabeth had not taken place .
Several authors believe that despite his marriage Bacon was primarily attracted to the same sex .
On 9 April 1626 Bacon died while at Arundel mansion at Highgate outside London of pneumonia .
An influential account of the circumstances of his death was given by John Aubrey .
Aubrey has been criticized for his evident credulousness in this and other works ; on the other hand , he knew Thomas Hobbes , Bacon 's fellow-philosopher and friend .
Aubrey 's vivid account , which portrays Bacon as a martyr to experimental scientific method , had him journeying to Highgate through the snow with the King 's physician when he is suddenly inspired by the possibility of using the snow to preserve meat :
They alighted out of the coach and went into a poor woman 's house at the bottom of Highgate hill , and bought a fowl , and made the woman exenterate it " .
After stuffing the fowl with snow , Bacon contracted a fatal case of pneumonia .
Being unwittingly on his deathbed , the philosopher wrote his last letter to his absent host and friend Lord Arundel :
Bacon did not propose an actual philosophy , but rather a method of developing philosophy .
Bacon claimed that :
He published Of the Proficience and Advancement of Learning , Divine and Human in 1605 .
A year prior to the release of New Atlantis , Bacon published an essay that reveals a version of himself not often seen in history .
In 1623 Bacon expressed his aspirations and ideals in New Atlantis .
The plan and organization of his ideal college , " Solomon 's House " , envisioned the modern research university in both applied and pure science .
In this work , we see the development of the Baconian method ( Or scientific method ) , consisting of procedures for isolating the form , nature or cause of a phenomenon , employing the method of agreement , method of difference , and method of concomitant variation devised by Avicenna in 1025 .
Many of Bacon 's writings were only published after his death in 1626 .
Bacon 's ideas about the improvement of the human lot were influential in the 1630s and 1650s among a number of Parliamentarian scholars .
In the nineteenth century his emphasis on induction was revived and developed by William Whewell , among others .
He has been connected to the mysterious Oak Island buried treasure .
In 1910 Newfoundland issued a postage stamp to commemorate Bacon 's role in establishing Newfoundland .
The Baconian theory of Shakespearean authorship , first proposed in the mid-19th century , contends that Sir Francis Bacon wrote the plays conventionally attributed to William Shakespeare , in opposition to the mainstream view that William Shakespeare of Stratford wrote the poems and plays that bear his name .
Mainstream scholars reject all such arguments in favour of Bacon , and criticise Bacon 's attributed poetry as too dissimilar to Shakespeare 's for it to have been written by the same person .
Francis Bacon often gathered with the men at Gray 's Inn to discuss politics and philosophy , and to try out various theatrical scenes that he admitted writing .
Francis Bacon 's influence can also be seen on a variety of religious and spiritual authors , and on groups that have utilized his writings in their own belief systems .
