A Funny Thing Happened on the Way to the Forum is a musical with music and lyrics by Stephen Sondheim and a book by Burt Shevelove and Larry Gelbart .
Inspired by the farces of the ancient Roman playwright Plautus ( 251 -- 183 BC ) , specifically Pseudolus , Miles Gloriosus and Mostellaria , it tells the bawdy story of a slave named Pseudolus and his attempts to win his freedom by helping his young master woo the girl next door .
A Funny Thing Happened on the Way to the Forum opened on Broadway on May 8 , 1962 at the Alvin Theatre .
The show 's creators originally wanted Phil Silvers in the lead role of Pseudolus , but he turned them down , allegedly because he would have to perform onstage without his glasses , and his vision was so poor that he feared tripping into the orchestra pit .
Milton Berle also passed on the role .
Eventually , Zero Mostel was cast .
Out of town during pre-Broadway tryouts , the show was attracting little business and not playing well .
Director and choreographer Jerome Robbins was called in by Abbott and Prince to give advice and make changes .
Karen Black , originally cast as the ingenue , was replaced out of town .
The show won several Tony Awards : best musical , best actor , best supporting actor ( Burns ) , best book , and best director .
Two songs were dropped from the show , and two new Sondheim songs were added .
The production ran 156 performances , but had to close soon after Phil Silvers suffered a stroke .
The production , directed by Jerry Zaks , ran for 715 performances .
Bruce Dow originally performed the role of Pseudolus , but was forced to withdraw from the entire 2009 season due to an injury , and the role was then performed by Sean Cullen as of September 5 , 2009 .
Bruce Dow and Sean Cullen will alternate the lead role .
In ancient Rome , some neighbors live in three adjacent houses .
The song was cut from the show and replaced with " Comedy Tonight " .
The song was later featured in the film The Birdcage ( 1996 ) and performed by Robin Williams and Christine Baranski .
