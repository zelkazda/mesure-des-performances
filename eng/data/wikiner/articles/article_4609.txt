Many such shelters were constructed as civil defense measures during the Cold War .
During the Cold War many countries built fallout shelters for high-ranking government officials and crucial military facilities .
( A letter from President Kennedy advising the use of fallout shelters appeared in the September , 1961 issue of Life magazine . )
Interest in fallout shelters has largely dropped , as the perceived threat of global nuclear war reduced after the end of the Cold War .
In Switzerland , most residential shelters are no longer stocked with the food and water required for prolonged habitation and a large number have been converted by the owners to other uses ( e.g. wine cellars , ski rooms , gyms ) .
These shelters also provide a haven from natural disasters such as tornadoes and hurricanes , although Switzerland is rarely subject to such natural phenomena .
One of Switzerland 's solutions is to utilise road tunnels passing through the mountains ; with some of these shelters being able to protect tens of thousands .
In many countries ( such as the U.S. ) civilian radio stations have emergency generators with enough fuel to operate for extended periods without commercial electricity .
It is possible to construct an electrometer-type radiation meter called the Kearny Fallout Meter from plans with just a coffee can or pail , gypsum board , monofilament fishing line , and aluminum foil .
Data collected after the Chernobyl accident can serve in a simulation of fallout shelter efficacy , reconstructing the contribution of different radioisotopes to the radiation dose over time .
Using the data for the source term ( radioactive release ) from Chernobyl , and other literature data it is possible to estimate how much protection a wall of concrete will offer in the event of a Chernobyl like accident .
In the long term it is important to consider the protection which is offered by a person 's home in the months and years after an event such as the Chernobyl accident .
The idealized fallout shelter can be seen in the motion picture Blast from the Past .
The Twilight Zone episode The Shelter , from a Rod Serling script , deals with the consequences of actually using a shelter .
In 1999 the film Blast from the Past was released .
It is a romantic comedy film about a nuclear physicist , his wife , and son that enter a well-equipped spacious fallout shelter during the 1962 Cuban Missile Crisis .
The Fallout series of computer games depicts the remains of human civilization after an immensely destructive nuclear war , and as such heavily features a number of shelters and bunkers .
