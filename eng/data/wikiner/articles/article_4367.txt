The Metamorphosis ( German : Die Verwandlung ) is a novella by Franz Kafka , first published in 1915 .
It is often cited as one of the seminal works of short fiction of the 20th century and is widely studied in colleges and universities across the western world ; Elias Canetti described it as " one of the few great and perfect works of the poetic imagination written during this century " .
The story begins with a traveling salesman , Gregor Samsa , waking to find himself transformed into a gigantic insect .
Gregor Samsa awakes one morning to find himself inexplicably transformed from a human into a monstrous insect .
Kafka 's sentences often deliver an unexpected impact just before the full stop -- that being the finalizing meaning and focus .
A stage adaptation was performed by Steven Berkoff in 1969 .
