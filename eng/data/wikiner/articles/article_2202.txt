Parts of his uncompleted mechanisms are on display in the London Science Museum .
Built to tolerances achievable in the 19th century , the success of the finished engine indicated that Babbage 's machine would have worked .
Nine years later , the Science Museum completed the printer Babbage had designed for the difference engine , an astonishingly complex device for the 19th century .
Considered a " father of the computer " , Babbage is credited with inventing the first mechanical computer that eventually led to more complex designs .
His date of birth was given in his obituary in The Times as 26 December 1792 .
However after the obituary appeared , a nephew wrote to say that Charles Babbage was born one year earlier , in 1791 .
His father 's money allowed Charles to receive instruction from several schools and tutors during the course of his elementary education .
His parents ordered that his " brain was not to be taxed too much " and Babbage felt that " this great idleness may have led to some of my childish reasonings . "
The academy had a well-stocked library that prompted Babbage 's love of mathematics .
Of the first , a clergyman near Cambridge , Babbage said , " I fear I did not derive from it all the advantages that I might have done . "
Babbage arrived at Trinity College , Cambridge in October 1810 .
In response , he , John Herschel , George Peacock , and several other friends formed the Analytical Society in 1812 .
Babbage , Herschel , and Peacock were also close friends with future judge and patron of science Edward Ryan .
In 1812 Babbage transferred to Peterhouse , Cambridge .
He was the top mathematician at Peterhouse , but did not graduate with honours .
Charles ' father , wife , and at least one son all died in 1827 .
These deaths caused Babbage to go into a mental breakdown which delayed the construction of his machines .
In 1983 the autopsy report for Charles Babbage was discovered and later published by one of his descendants .
Babbage sought a method by which mathematical tables could be calculated mechanically , removing the high rate of human error .
Three different factors seem to have influenced him : a dislike of untidiness ; his experience working on logarithmic tables ; and existing work on calculating machines carried out by Wilhelm Schickard , Blaise Pascal , and Gottfried Leibniz .
He first discussed the principles of a calculating engine in a letter to Sir Humphry Davy in 1822 .
Babbage 's machines were among the first mechanical computers , although they were not actually completed , largely because of funding problems and personality issues .
Although Babbage 's machines were mechanical and unwieldy , their basic architecture was very similar to a modern computer .
In Babbage 's time , numerical tables were calculated by humans who were called ' computers ' , meaning " one who computes " , much as a conductor is " one who conducts " .
At Cambridge , he saw the high error-rate of this human-driven process and started his life 's work of trying to calculate the tables mechanically .
Unlike similar efforts of the time , Babbage 's difference engine was created to calculate a series of values automatically .
He later designed an improved version , " Difference Engine No. 2 " , which was not constructed until 1989 -- 1991 , using Babbage 's plans and 19th century manufacturing tolerances .
It performed its first calculation at the London Science Museum returning results to 31 digits , far more than the average modern pocket calculator .
The two models that have been constructed are not replicas ; until the assembly of the first Difference Engine No 2 by the London Science Museum , no model of the Difference Engine No 2 existed .
Soon after the attempt at making the difference engine crumbled , Babbage started designing a different , more complex machine called the Analytical Engine .
The main difference between the two engines is that the Analytical Engine could be programmed using punched cards .
Ada Lovelace , an impressive mathematician , and one of the few people who fully understood Babbage 's ideas , created a program for the Analytical Engine .
Based on this work , Lovelace is now widely credited with being the first computer programmer .
In 1979 , a contemporary programming language was named Ada in her honour .
In 1824 , Babbage won the Gold Medal of the Royal Astronomical Society " for his invention of an engine for calculating mathematical and astronomical tables . "
The book is a work of natural theology , and incorporates extracts from correspondence he had been having with John Herschel on the subject .
Babbage also achieved notable results in cryptography .
In 1838 , Babbage invented the pilot ( also called a cow-catcher ) , the metal frame attached to the front of locomotives that clears the tracks of obstacles .
Babbage also invented an ophthalmoscope , but although he gave it to a physician for testing it was forgotten , and the device only came into use after being independently invented by Hermann von Helmholtz .
Babbage noted that highly skilled -- and thus generally highly paid -- workers spend parts of their job performing tasks that are ' below ' their skill level .
This principle was criticised by Karl Marx who argued that it caused labour segregation and contributed to alienation .
The Babbage principle is an inherent assumption in Frederick Winslow Taylor 's scientific management .
Babbage has been commemorated by a number of references , as shown on this list .
