The speech delivered by President Roosevelt incorporated the following
FDR called for " a world-wide reduction of armaments " as a goal for " the future days , which we seek to make secure " but one that was " attainable in our own time and generation . "
… ' " -- Franklin D. Roosevelt
The four paintings were published in The Saturday Evening Post on February 20 , February 27 , March 6 and March 13 in 1943 .
( See also , Freedom from Fear ( painting ) ) .
FDR commissioned sculptor Walter Russell to design a monument to be dedicated to the first hero of the war .
The Franklin D. Roosevelt Four Freedoms Park was a park designed by the architect Louis Kahn for the south point of Roosevelt Island .
