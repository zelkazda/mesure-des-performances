Later record keeping aids throughout the Fertile Crescent included clay shapes , which represented counts of items , probably livestock or grains , sealed in containers .
The " castle clock " , an astronomical clock invented by Al-Jazari in 1206 , is considered to be the earliest programmable analog computer .
The engineers in the Apollo program that sent a man to the moon made many of their calculations on slide rules , which were accurate to three or four significant figures .
He built twenty of these machines ( called the Pascaline ) in the following ten years .
Leibniz once said " It is unworthy of excellent men to lose hours like slaves in the labour of calculation which could safely be relegated to anyone else if machines were used . "
( Nearly all Friden calculators had a ten-key auxiliary keyboard for entering the multiplier when doing multiplication . )
Most machines made by the three companies mentioned did not print their results , although other companies , such as Olivetti , did make printing calculators .
Friden made a calculator that also provided square roots , basically by doing division , but with added mechanism that automatically incremented the number in the keyboard in a systematic fashion .
In 1954 , IBM , in the U.S. , demonstrated a large all-transistor calculator and , in 1957 , the company released the first commercial all-transistor calculator , the IBM 608 , though it was housed in several cabinets and cost about $ 80,000 .
The ANITA sold well since it was the only electronic desktop calculator available , and was silent and quick .
The Olivetti Programma 101 was introduced in late 1965 ; it was a stored program machine which could read and write magnetic cards and displayed results on its built-in printer .
The Olivetti Programma 101 won many industrial design awards .
The first handheld calculator was developed by Texas Instruments in 1967 .
The first portable calculators appeared in Japan in 1970 , and were soon marketed around the world .
One of the first low-cost calculators was the Sinclair Cambridge , launched in August 1973 .
Meanwhile Hewlett Packard ( HP ) had been developing its own pocket calculator .
In 1973 , Texas Instruments ( TI ) introduced the SR-10 , an algebraic entry pocket calculator for $ 150 .
In 1978 a new company , Calculated Industries , came onto the scene , focusing on the specific markets .
The first programmable pocket calculator was the HP-65 , in 1974 ; it had a capacity of 100 instructions , and could store and retrieve programs with a built-in magnetic card reader .
Two years later the HP-25C introduced continuous memory , i.e. programs and data were retained in CMOS memory during power-off .
In 1979 , HP released the first alphanumeric , programmable , expandable calculator , the HP-41C .
This series of calculators was also noted for a large number of highly counter-intuitive mysterious undocumented features , somewhat similar to " synthetic programming " of the American HP-41 , which were exploited by applying normal arithmetic operations to error messages , jumping to non-existent addresses and other techniques .
A number of respected monthly publications , including the popular science magazine " Наука и жизнь " ( " Science and Life " ) , featured special columns , dedicated to optimization techniques for calculator programmers and updates on undocumented features for hackers , which grew into a whole esoteric science with many branches , known as " eggogology " ( " еггогология " ) .
A similar hacker culture in the USA was centered around the HP-41 , which was also noted for a large number of undocumented features and was much more powerful than B3-34 .
The first calculator capable of symbolic computation was the HP-28C , released in 1987 .
The first graphing calculator was the Casio FX-7000G released in 1985 .
The two leading manufacturers , HP and TI , released increasingly feature-laden calculators during the 1980s and 1990s .
At the turn of the millennium , the line between a graphing calculator and a handheld computer was not always clear , as some very advanced calculators such as the TI-89 , the Voyage 200 and HP-49G could differentiate and integrate functions , solve differential equations , run word processing and PIM software , and connect by wire or IR to other calculators/computers .
The HP 12c financial calculator is still produced .
Personal computers often come with a calculator utility program that emulates the appearance and functionality of a calculator , using the graphical user interface to portray a calculator .
