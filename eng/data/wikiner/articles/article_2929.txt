The term centripetal force comes from the Latin words centrum ( " center " ) and petere ( " tend towards " , " aim at " ) , signifying that the force is directed inward toward the center of curvature of the path .
Isaac Newton 's description was : " A centripetal force is that by which bodies are drawn or impelled , or in any way tend , towards a point as to a center . "
Alternatively , some sources , including Newton , refer to the entire gravitational force as centripetal , though it is not strictly centripetally directed when the orbit is not circular .
If this acceleration is multiplied by the particle mass , the leading term is the centripetal force and the negative of the second term related to angular acceleration is sometimes called the Euler force .
Consequently , in the general case , it is not straightforward to disentangle the centripetal and Euler terms from the above general acceleration equation .
From a qualitative standpoint , the path can be approximated by an arc of a circle for a limited time , and for the limited time a particular radius of curvature applies , the centrifugal and Euler forces can be analyzed on the basis of circular motion with that radius .
