The Futurians were a group of science fiction fans , many of whom became editors and writers as well .
The Futurians were based in New York City and were a major force in the development of science fiction writing and science fiction fandom in the years 1937-1945 .
Other sources indicate that Donald A. Wollheim was pushing for a more left-wing direction with a goal of leading fandom toward a political ideal , all of which Moskowitz resisted .
We changed clubs the way Detroit changes tailfins , every year had a new one , and last year 's was junk " .
There were several club names during that period , before finally settling on the Futurians .
Most of these projects had small editorial budgets , and relied in part , or occasionally entirely , on contributions from fellow Futurians for their contents .
At the time the Futurians were formed , Donald Wollheim was strongly attracted by communism and believed that followers of science fiction " should actively work for the realization of the scientific world-state as the only genuine justification for their activities and existence " .
It was to this end that Wollheim formed the Futurians , and many of its members were in some degree interested in the political applications of science fiction .
On the other hand several members were political moderates or apolitical , and in the case of James Blish arguably right-wing .
More solid evidence is that Blish admired the work of Oswald Spengler .
