Curling is thought to have been invented in late medieval Scotland , with the first written reference to a contest using stones on ice coming from the records of Paisley Abbey , Renfrewshire , in February 1541 .
Kilsyth also claims the oldest purpose-built curling pond in the world at Colzium , in the form of a low dam creating a shallow pool some 100 × 250 metres in size , though this is now very seldom in condition for curling because of warmer winters .
The verbal noun curling is formed from the Scots verb curl , which describes the motion of the stone .
It is recorded that in Darvel , East Ayrshire , the weavers relaxed by playing curling matches .
Outdoor curling was very popular in Scotland between the 16th and 19th centuries , as the climates provided good ice conditions every winter .
Scotland is home to the international governing body for curling , the World Curling Federation , Perth , which originated as a committee of the Royal Caledonian Curling Club , the mother club of curling .
The first curling club in the United States began in 1830 , and the game was introduced to Switzerland and Sweden before the end of the 19th century , also by Scots .
Today , curling is played all over Europe and has spread to Japan , Australia , New Zealand , China , and Korea .
The first world curling championship in the sport was limited to men and was known as the " Scotch Cup " , held in Falkirk and Edinburgh , Scotland , in 1959 .
The first world title was won by the Canadian team from Regina , Saskatchewan , skipped by Ernie Richardson .
Curling has been an official sport in the Winter Olympic Games since the 1998 Winter Olympics .
In February 2006 , the International Olympic Committee retroactively decided that the curling competition from the 1924 Winter Olympics would be considered official Olympic events and no longer be considered demonstration events .
A demonstration tournament was also held during the 1932 Winter Olympic Games between four teams from Canada and four teams from the United States , with Canada winning 12 games to 4 .
Large events , such as the Brier or other national championships , are typically held in an arena that presents a challenge to the ice maker , who must constantly monitor and adjust the ice and air temperatures as well as air humidity levels to ensure a consistent playing surface .
The curling stone , ( also sometimes rock , North America ) as defined by the World Curling Federation is a thick stone disc weighing between 38 and 44 pounds ( 17 and 20 kg ) with a handle attached to the top .
However , there 's a movement on the World Curling Tour to make the games only eight ends .
This rule is known as the four-rock rule or the free guard zone rule ( for a while in Canada , a " three-rock rule " was in place , but that rule has been replaced by the four-rock rule ) .
This strategy had developed ( mostly in Canada ) as ice-makers had become skilled at creating a predictable ice surface and the adoption of brushes allowed greater control over the rock .
Before the game , teams typically decide who gets the hammer in the first end either by chance ( such as a coin toss ) , by a " draw-to-the-button " contest , where a representative of each team shoots a single stone to see who gets closer to the centre of the rings , or , particularly in tournament settings like the Winter Olympics , by a comparison of each team 's win-loss record .
In the Winter Olympics , a team may concede after finishing any end during a round-robin game , but can only concede after finishing eight ends during the knockout stages .
The example illustrates the men 's final at the 2006 Winter Olympics .
Probably the best-known snowman came at the 2006 Players ' Championships .
Curling is played in many countries including Canada , U.K. ( especially Scotland ) , the United States , Norway , Sweden , Switzerland , Denmark , Finland and Japan , all of which compete in the world championships .
Curling is particularly popular in Canada .
Improvements in ice making and changes in the rules to increase scoring and promote complex strategy have increased the already high popularity of the sport in Canada , and large television audiences watch annual curling telecasts , especially the Scotties Tournament of Hearts ( the national championship for women ) , the Tim Hortons Brier ( the national championship for men ) , and the women 's and men 's world championships .
The Tournament of Hearts and the Brier are contested by provincial and territorial champions , and the world championships by national champions .
Ernie Richardson and his family team dominated Canadian and international curling during the late 1950s and early 1960s and are generally considered to be the best male curlers of all time .
Sandra Schmirler led her team to the first ever gold medal in women 's curling in the 1998 Winter Olympics .
While Canadian bonspiels ( tournaments ) offer cash prizes , there are very few full-time professional curlers .
Still , curling survives as a people 's sport , returning to the Winter Olympics in 1998 with men 's and women 's tournaments after not having been on the official Olympic program since 1924 ( that year 's curling competition , for men only , was confirmed as official by the IOC in 2006 ) .
However , there are many young teams who turn heads , and junior curling is quite popular , with national finals being televised nationwide in Canada .
