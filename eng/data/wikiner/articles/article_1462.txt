Brian Wilson Kernighan ( pronounced /ˈkɛərnɨhæn/ , the ' g ' is silent ; born January 1942 in Toronto ) is a Canadian computer scientist who worked at Bell Labs alongside Unix creators Ken Thompson and Dennis Ritchie and contributed greatly to Unix and its school of thought .
He is also coauthor of the AWK and AMPL programming languages .
The ' K ' of K & R C and the ' K ' in AWK both stand for ' Kernighan ' .
Kernighan has said that he had no part in the design of the C language ( " it 's entirely Dennis Ritchie 's work " ) [ citation needed ] .
He authored many Unix programs , including ditroff , and cron for Version 7 Unix .
Kernighan was the software editor for Prentice Hall International .
He has said that if stranded on an island with only one programming language it would have to be C .
Kernighan coined the term Unix in the 1970s .
The original term he coined was Unics , which was later changed to Unix .
Kernighan 's term is used to indicate that WYSIWYG systems might throw away information in a document that could be useful in other contexts .
