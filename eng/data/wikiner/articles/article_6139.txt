The Intergovernmental Panel on Climate Change ( IPCC ) is a scientific intergovernmental body tasked with evaluating the risk of climate change caused by human activity .
The panel was established in 1988 by the World Meteorological Organization ( WMO ) and the United Nations Environment Programme ( UNEP ) , two organizations of the United Nations .
The IPCC does not carry out its own original research , nor does it do the work of monitoring climate or related phenomena itself .
The IPCC bases its assessment mainly on peer reviewed and published scientific literature .
The IPCC is only open to member states of the WMO and UNEP .
IPCC reports are widely cited in almost any debate related to climate change .
National and international responses to climate change generally regard the UN climate panel as authoritative .
The stated aims of the IPCC are to assess scientific information relevant to :
The major conclusion was that research since 1990 did " not affect our fundamental understanding of the science of the greenhouse effect and either confirm or do not justify alteration of the major conclusions of the first IPCC scientific assessment " .
Climate Change 1995 , the IPCC Second Assessment Report ( SAR ) , was finished in 1996 .
He opposed it in the Leipzig Declaration of S. Fred Singer 's Science and Environmental Policy Project .
Santer 's position was supported by fellow IPCC authors and senior figures of the American Meteorological Society and University Corporation for Atmospheric Research .
The Third Assessment Report ( TAR ) consists of four reports , three of them from its working groups :
IPCC uses the best available predictions and their reports are under strong scientific scrutiny .
The IPCC concedes that there is a need for better models and better scientific understanding of some climate phenomena , as well as the uncertainties involved .
The predictions are based on scenarios , and the IPCC did not assign any probability to the 35 scenarios used .
In IPCC statements " most " means greater than 50 % , " likely " means at least a 66 % likelihood , and " very likely " means at least a 90 % likelihood .
As it has been the case in the past , the outline of the AR5 will be developed through a scoping process which involves climate change experts from all relevant disciplines and users of IPCC reports , in particular representatives from governments .
The scoping meeting of experts to define the outline of the AR5 took place on 13 -- 17 July 2009 .
The objectives of the IPCC-NGGIP are :
The IPCC has published four comprehensive assessment reports reviewing the latest climate science , as well as a number of special reports on particular topics .
The IPCC does not carry out research nor does it monitor climate related data .
The responsibility of the lead authors of IPCC reports is to assess available information about climate change drawn mainly from the peer reviewed and published scientific/technical literature .
Reports of the workshops held so far are available at the IPCC website .
The IPCC reports are a compendium of peer reviewed and published science .
In December 2007 , the IPCC was awarded the Nobel Peace Prize " for their efforts to build up and disseminate greater knowledge about man-made climate change , and to lay the foundations for the measures that are needed to counteract such change . "
Various criticisms have been raised , both about the specific content of IPCC reports , as well as about the process undertaken to produce the reports .
The IPCC has since acknowledged that the date is incorrect , while reaffirming that the conclusion in the final summary was robust .
They expressed regret for " the poor application of well-established IPCC procedures in this instance " .
The IPCC needs to look at this trend in the errors and ask why it happened . "
The methodology used to produce this graph was criticized in a paper by Stephen McIntyre and Ross McKitrick .
Some critics have contended that the IPCC reports tend to underestimate dangers , understate risks , and report only the " lowest common denominator " findings .
The study compared IPCC 2001 projections on temperature and sea level change with observations .
Over the six years studied , the actual temperature rise was near the top end of the range given by IPCC 's 2001 projection , and the actual sea level rise was above the top of the range of the IPCC projection .
An example of scientific research which has indicated that previous estimates by the IPCC , far from overstating dangers and risks , has actually understated them ( this may be due , in part , to the expanding human understanding of climate ) is a study on projected rises in sea levels .
When the researchers ' analysis was " applied to the possible scenarios outlined by the Intergovernmental Panel on Climate Change ( IPCC ) , the researchers found that in 2100 sea levels would be 0.5 -- 1.4 m above 1990 levels .
These values are much greater than the 9 -- 88 cm as projected by the IPCC itself in its Third Assessment Report , published in 2001 .
It commented on the IPCC process :
The conclusions of the Stern Review have been contested , however .
The structural elements of the IPCC processes have been criticized in other ways , with the design of the processes during the formation of the IPCC making its reports prone not to exaggerations , but to underestimating dangers , under-stating risks , and reporting only the " least common denominator " findings which by design make it through the bureaucracy .
Since the IPCC does not carry out its own research , it operates on the basis of scientific papers and independently documented results from other scientific bodies , and its schedule for producing reports requires a deadline for submissions prior to the report 's final release .
Rajendra Pachauri , the IPCC chair , admitted at the launch of this report that since the IPCC began work on it , scientists have recorded " much stronger trends in climate change " , like the unforeseen dramatic melting of polar ice in the summer of 2007 , and added , " that means you better start with intervention much earlier " .
Scientists who participate in the IPCC assessment process do so without any compensation other than the normal salaries they receive from their home institutions .
In May 2010 Pachauri noted that the IPCC currently had no process for responding to errors or flaws once it issued a report .
The problem , according to Pachauri , was that once a report was issued the panels of scientists producing the reports were disbanded .
They suggested a range of new organizational options , from tightening the selection of lead authors and contributors , to dumping it in favor of a small permanent body , or even turning the whole climate science assessment process into a moderated " living " Wikipedia-IPCC .
In March 2010 , at the invitation of the United Nations secretary-general and the chair of the IPCC , the InterAcademy Panel on International Issues was asked to review the IPCC 's processes for developing its reports .
The panel 's review is funded by the UN .
The panel , chaired by Harold Tafler Shapiro , convened on 14 May 2010 and expected to release its report by 1 September 2010 .
Various scientific bodies have issued official statements praising the IPCC and endorsing their findings .
