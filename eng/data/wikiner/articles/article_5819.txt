Haddocks ' Eyes is a poem by Lewis Carroll from Through the Looking-Glass .
By the time Alice heard it , she was already tired of poetry .
It is a parody of " Resolution and Independence " by William Wordsworth .
The White Knight explains a confusing nomenclature for the song .
In 1856 , Carroll published the following poem anonymously under the name Upon the Lonely Moor .
It bears an obvious resemblance to " Haddocks ' Eyes . "
