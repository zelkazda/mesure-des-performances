" The Ballad Of The Green Berets " is a patriotic song in the ballad style about the Green Berets , an elite special force in the U.S. Army .
Moore also wrote a non-fiction book , The Green Berets , about the force .
Sadler debuted the song on television on January 30 , 1966 on The Ed Sullivan Show .
It is currently used as one of the four primary marching tunes of the Fightin ' Texas Aggie Band .
The song is heard in a choral rendition by Ken Darby in the 1968 John Wayne film , The Green Berets , based on Moore 's book .
The score of the movie was never released as an album until Film Score Monthly released it in 2005 .
The song appears in the films More American Graffiti and Canadian Bacon .
It can be heard in the gun show scene from the 2002 film Showtime , and in the film Jesus ' Son , in a scene that features a hitch-hiking Jack Black .
