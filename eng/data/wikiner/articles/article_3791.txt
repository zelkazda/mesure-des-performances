Plato believed that children would never learn unless they wanted to learn .
The ultimate development of the grammar school was by Joseph Lancaster and Adam Bell who developed the monitorial system .
With fully-developed internal economies , Lancaster schools provided a grammar-school education for a cost per student near $ 40 per year in 1999 U.S. dollars .
For example , Lancaster students , motivated to save scrip , ultimately rented individual pages of textbooks from the school library , and read them in groups around music stands to reduce textbook costs .
These elites said that Lancaster schools might become dishonest , provide poor education and were not accountable to established authorities .
Lancaster 's supporters responded that any schoolchild could avoid cheats , given the opportunity , and that the government was not paying for the education , and thus deserved no say in their composition .
Lancaster , though motivated by charity , claimed in his pamphlets to be surprised to find that he lived well on the income of his school , even while the low costs made it available to the poorest street-children .
Ironically , Lancaster lived on the charity of friends in his later life .
Jean-Jacques Rousseau has been called the father of the child-study movement .
It has been said that Rousseau " discovered " the child ( as an object of study ) .
Rousseau 's principal work on education is Emile : Or , On Education , in which he lays out an educational program for a hypothetical newborn 's education to adulthood .
His ideas were rarely implemented directly , but were influential on later thinkers , particularly Johann Heinrich Pestalozzi and Friedrich Wilhelm August Fröbel , the inventor of the kindergarten .
H. D. Thoreau 's Walden and reform essays in the mid-19th century were influential also .
For a look at transcendentalist life , read Louisa May Alcott 's Little Women .
Her father , A. Bronson Alcott , a close friend of Thoreau 's , pioneered progressive education for young people as early as the 1830s .
Since most modern schools copy the Prussian models , children start school at an age when their language skills remain plastic , and they find it easy to learn the national language .
In other countries , such as the Soviet Union , France , Spain , and Germany this approach has dramatically improved reading and math test scores for linguistic minorities .
John Dewey , a philosopher and educator , was heavily influential in American and international education , especially during the first four decades of the twentieth century .
He was a harsh critic of " dead " knowledge disconnected from practical human life , foreshadowing Paulo Freire 's attack on the " banking concept of education . "
Dewey 's influence began to decline in the time after the Second World War and particularly in the Cold War era , as more conservative educational policies came to the fore .
While influenced particularly in its rhetoric by Dewey and even more by his popularizers , administrative progressivism was in its practice much more influenced by the industrial revolution and the concept economies of scale .
The administrative progressives are responsible for many features of modern American education , especially American high schools : counseling programs , the move from many small local high schools to large centralized high schools , curricular differentiation in the form of electives and tracking , curricular , professional , and other forms of standardization , and an increase in state and federal regulation and bureaucracy , with a corresponding reduction of local control at the school board level .
These reforms have since become heavily entrenched , and many today who identify themselves as progressives are opposed to many of them , while conservative education reform during the Cold War embraced them as a framework for strengthening traditional curriculum and standards .
Evidence suggests that higher-order thinking skills are unused by many people ( cf. Jean Piaget , Isabel Myers , and Katharine Cook Briggs ) .
Some authorities say that this refutes key assumptions of progressive thinkers such as Dewey .
He showed by widely reproduced experiments that most young children do not analyze or synthesize as Dewey expected .
Some authorities therefore say that Dewey 's reforms do not apply to the primary education of young children .
This information was confirmed ( on another research track ) by Jean Piaget , who discovered that nearly 60 % of adults never habitually use what he called " formal operational reasoning " , a term for the development and use of theories and explicit logic .
The synthesis resulting from this two-part critique is a " neoclassical " learning theory similar to that practiced by Marva Collins , in which both learning styles are accommodated .
Diane Ravitch argues that " progressive " reformers have replaced a challenging liberal arts curriculum with ever-lower standards and indoctrination , particularly in inner-city schools , thereby preventing vast numbers of students from achieving their full potential .
In the 1980s , some of the momentum of education reform moved from the left to the right , with the release of A Nation at Risk , Ronald Reagan 's efforts to reduce or eliminate the United States Department of Education .
In the latter half of the decade , E.D. Hirsch put forth an influential attack on one or more versions of progressive education , advocating an emphasis on " cultural literacy " -- the facts , phrases , and texts that Hirsch asserted every American had once known and that now only some knew , but was still essential for decoding basic texts and maintaining communication .
Hirsch 's ideas remain significant through the 1990s and into the 21st century , and are incorporated into classroom practice through textbooks and curricula published under his own imprint .
See also Uncommon Schools .
Although many people have claimed that U.S. public schools are underfunded , there are few countries that spend as much per student on education .
However , the United States is well known for huge inequalities in the economics of school districts .
Despite this high level of funding , U.S. public schools lag behind the schools of other rich countries in the areas of reading , math , and science .
This is the third highest level of funding per student out of the 100 biggest school districts in the U.S .
Libertarian theorists such as Milton Friedman advocate school choice to promote excellence in education through competition .
A recent Fordham Institute study found that some labor agreements with teachers ' unions may restrict the ability of school systems to implement merit pay and other reforms .
The " beliefs " of school districts are optimistic that quite literally " all students will succeed " , which in the context of high school graduation examination in the United States , all students in all groups , regardess of heritage or income will pass tests that in the introduction typically fall beyond the ability of all but the top 20 to 30 percent of students .
This strategy was first adopted to unify related linguistic groups in Europe , such as Germany and Italy .
In Iran , increased primary education was correlated with increased farming efficiencies and income .
The Internet 's communication capabilities make it potentially useful for collaboration , and foreign language learning .
In some cases classrooms have been moved entirely online , while in other instances the goal is more to learn how the Internet can be more than a classroom .
The evidence of Lancaster schools suggests using students as teachers .
If the culture supports it , perhaps the economic discipline of the Lancaster school can reduce costs even further .
However , much of the success of Lancaster 's " school economy " was that the children were natives of an intensely mercantile culture .
This is an important part of Marva Collins ' method .
In Taiwan in the 1990s and 2000s a movement tried to prioritize reasoning over mere facts , reduce the emphasis on central control and standardized testing .
