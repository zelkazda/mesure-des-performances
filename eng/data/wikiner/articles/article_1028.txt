Artemis was venerated in Lydia as Artimus .
All accounts agree , however , that she was the daughter of Zeus and Leto and that she was the twin sister of Apollo .
An account by Callimachus has it that Hera forbade Leto to give birth on either terra firma ( the mainland ) or on an island .
Hera was angry with Zeus , her husband , because he had impregnated Leto .
But the island of Delos disobeyed Hera , and Leto gave birth there .
A scholium of Servius on Aeneid iii .
The myths also differ as to whether Artemis was born first , or Apollo .
Most stories depict Artemis as born first , becoming her mother 's mid-wife upon the birth of her brother Apollo .
A poem of Callimachus to the goddess " who amuses herself on mountains with archery " imagines some charming vignettes : at three years old , Artemis asked her father , Zeus , while sitting on his knee , to grant her six wishes .
Also , she asked for a silver bow like her brother Apollo .
All of her companions remained virgins and Artemis guarded her own chastity closely .
All of her companions remained virgins and Artemis guarded her own chastity closely .
Artemis spent her childhood looking for all the things she would need to be a huntress .
The Okeanus ' daughters were affraid to see them , but young Artemis bravely came to them and asked for bow and arrows .
Artemis then visited Pan , the god of forest and he gave her one seven bitches and six dogs .
Young Artemis often exercised her new power by shooting trees and wild beasts .
As a goddess of hunting , Artemis wore a girlish knee-length tunic , and carried bow and quiver on her shoulder .
When portrayed as a goddess of the moon , Artemis wore a long robe and sometimes a veil covered her head .
Artemis ' chariot was made of gold and being pulled by four golden horned deers .
Although quite seldom , Artemis sometimes being portrayed with a hunting spear .
As a goddess of maiden dances and songs , Artemis often portrayed with a lyre .
Deer was the only animal that sacred by Artemis herself for her .
Artemis once saw a deer larger than a bull with horns shinning .
Artemis forgave him but targeted Eurystheus by her wrath .
Pan gave Artemis two dogs black-and-white , three reddish , and one spotted , which able to hunt even lion .
Oineus and Adonis were both killed by Artemis ' boar .
The sacrifice of bear for Artemis started from the Brauron cult .
Every year , a little girl age not more than ten and less than five sent to Artemis ' temple at Brauron .
Artemis sent a plague for what they did .
The Athenians consulted an oracle of how to end the plague .
The oracle suggested that the price they must pay for the blood of the bear is every young virgins were n't allowed to marry a man until she served Artemis in her temple ( played the bear for the goddess .
Hawk became a favorite bird of most gods , including Apollo and Artemis .
Artemis loved a buzzard hawk .
In other versions , Artemis killed Adonis for revenge .
Orion was a hunting companion of the goddess Artemis .
In some versions of his story he was killed by Artemis , while in others he was killed by a scorpion sent by Gaia .
In some versions , Orion tried to seduce Opis , one of her followers , and she killed him .
In yet another version , Apollo sent the scorpion .
According to Hyginus Artemis once loved Orion ( in spite of the late source , this version appears to be a rare remnant of her as the pre-Olympian goddess , who took consorts , as Eos did ) , but was tricked into killing him by her brother Apollo , who was " protective " of his sister 's maidenhood .
The growth of Aloadae never stop and they said when they height could reach heaven , they would kidnap Artemis and Hera and made them as wives .
The gods were afraid of the , except for Artemis who tried to trick the giants .
The Aloadae threw their spears which mistakenly hit each other and killed them .
As a young virgin , Artemis had interested many gods and men , but none of them successfully won her heart , except Orion in some myth , which then being killed by the goddess herself accidentally or by Gaia .
A river god Alpheus was in love with Artemis , but he realized nothing he could do to win her heart .
She saw Artemis and had a thought of raping her .
This boy , either because accidentally saw Artemis bath or trying to rape her , was turned into a girl by the goddess .
Zeus appeared to her disguised as Artemis , or in some stories Apollo , gained her confidence , then took advantage of her ( or raped her , according to Ovid ) .
Enraged , Hera or Artemis ( some accounts say both ) changed her into a bear .
The seer Calchis advised Agamemnon that the only way to appease Artemis was to sacrifice his daughter Iphigenia .
Artemis then snatches Iphigenia from the altar and substitutes a deer .
When Artemis and Apollo heard this impiety , Apollo killed her sons as they practiced athletics , and Artemis shot her daughters , who died instantly without a sound .
Apollo and Artemis used poisoned arrows to kill them , though according to some versions two of the Niobids were spared , one boy and one girl .
A devastated Niobe and her remaining children were turned to stone by Artemis as they wept .
She was beloved by two gods , Hermes and Apollo and boasted that she was prettier than Artemis and made two gods in love to her at once .
Artemis was raging and killed her with her arrow or dumb her by shooting her tongue .
But some version of this myth said Apollo and Hermes protected her from Artemis ' wrath .
Artemis saved the infant Atalanta from dying of exposure after her father abandoned her .
But she later sent a bear to hurt Atalanta because people said Atalanta was a better hunter .
Among other adventures , Atalanta participated in the hunt for the Calydonian Boar , which Artemis had sent to destroy Calydon because King Oeneus had forgotten her at the harvest sacrifices .
In the hunt , Atalanta drew the first blood , and was awarded the prize of the skin .
She hung it in a sacred grove at Tegea as a dedication to Artemis .
She was a virgin huntress just like Artemis and proud of her maidenhood .
One day , she claimed that Artemis ' body was too womanly and she doubted her virginity .
Hera struck Artemis on the ears with her own quiver , causing the arrows to fall out .
As Artemis fled crying to Zeus , Leto gathered up the bow and arrows which had fallen out of the quiver .
Artemis played quite a large part in this war .
He once promised the goddess she would sacrifice the dearest thing to him for her which was Iphigenia but broke the promise .
Artemis then saved Iphigenia for her braveness and saved her .
In some myth , Artemis made her as her attendant or turned her into Hecate , goddess of night , witchcraft , and underworld .
Aeneas was being helped by Artemis , Leto , and Apollo .
Artemis , the goddess of forests and hills , was worshipped throughout ancient Greece .
As Aeginaea , she was worshiped in Sparta ; the name means either huntress of chamois , or the wielder of the javelin ( αιγανέα ) .
She was worshipped at Naupactus as Aetole ; in her temple in that town there was a statue of white marble representing her throwing a javelin .
As Locheia , she was the goddess of childbirth and midwives .
The festival of Artemis Orthia was observed in Sparta .
A myth explaining this servitude relates that a bear had formed the habit of regularly visiting the town of Brauron , and the people there fed it , so that over time the bear became tame .
Either way , the girl 's brothers killed the bear , and Artemis was enraged .
Virginal Artemis was worshipped as a fertility/childbirth goddess in some places , assimilating Ilithyia , since , according to some myths , she assisted her mother in the delivery of her twin .
This winged Artemis lingered in ex-votos as Artemis Orthia , with a sanctuary close by Sparta .
Her darker side is revealed in some vase paintings , where she is shown as the death-bringing goddess whose arrows fell young maidens and women , such as the daughters of Niobe .
It was probably the best known center of her worship except for Delos .
They had been traditionally interpreted as multiple accessory breasts , or as sacrificed bull testes , as some newer scholars claimed , until excavation at the site of the Artemision in 1987-88 identified the multitude of tear-shaped amber beads that had adorned her ancient wooden xoanon .
