Frank Lloyd Wright was an American architect , interior designer , writer and educator , who designed more than 1,000 projects , which resulted in more than 500 completed works .
Wright also often designed many of the interior elements of his buildings , such as the furniture and stained glass .
Wright authored 20 books and many articles , and was a popular lecturer in the U.S. and in Europe .
His colorful personal life often made headlines , most notably for the 1914 fire and murders at his Taliesin studio .
Already well-known during his lifetime , Wright was recognized in 1991 by the American Institute of Architects as " the greatest American architect of all time " .
Frank Lloyd Wright was born in the farming town of Richland Center , Wisconsin , United States , in 1867 .
Both of Wright 's parents were strong-willed individuals with idiosyncratic interests that they passed on to him .
The blocks , known as Froebel Gifts , were the foundation of his innovative kindergarten curriculum .
Young Wright spent much time playing with the blocks .
Wright 's autobiography talks about the influence of these exercises on his approach to design .
Soon after Wright turned 14 his parents separated .
As the only male left in the family , Wright assumed financial responsibility for his mother and two sisters .
Wright attended a Madison high school but there is no evidence he ever graduated .
He was admitted to the University of Wisconsin -- Madison as a special student in 1886 .
In 1887 , Wright left the school without taking a degree .
In 1887 , Wright arrived in Chicago in search of employment .
He later recalled that his first impressions of Chicago were that of grimy neighborhoods , crowded streets and disappointing architecture , yet he was determined to find work .
Within days , and after interviews with several prominent firms , he was hired as a draftsman with the architectural firm of Joseph Lyman Silsbee .
In his autobiography , Wright accounts that he also had a short stint in another Chicago architecture office .
However , Wright soon realized that he was not ready to handle building design by himself ; he left his new job to return to Joseph Silsbee -- this time with a raise in salary .
Still , Wright aspired for more progressive work .
After less than a year had passed in Silsbee 's office , Wright learned that Adler & Sullivan , the forerunning firm in Chicago , were " looking for someone to make the finish drawings for the interior of the Auditorium [ Building ] . "
Wright demonstrated that he was a competent impressionist of Louis Sullivan 's ornamental designs and two short interviews later , was an official apprentice in the firm .
Wright did not get along well with Sullivan 's other draftsmen ; he wrote that several violent altercations occurred between them during the first years of his apprenticeship .
For that matter , Sullivan showed very little respect for his employees as well .
In spite of this , " Sullivan took [ Wright ] under his wing and gave him great design responsibility . "
Sullivan did his part to facilitate the financial success of the young couple by granting Wright a five year employment contract .
Wright made one more request : " Mr. Sullivan , if you want me to work for you as long as five years , could n't you lend me enough money to build a little house ? "
According to an 1890 diagram of the firm 's new , 17th floor space atop the Auditorium Building , Wright soon earned a private office next to Sullivan 's own .
However , that office was actually sharded with friend and draftsman George Elmslie , who was hired by Sullivan at Wright 's request .
Wright had risen to head draftsman and handled all residential design work in the office .
Wright was occupied by the firm 's major commissions during office hours , so house designs were relegated to evening and weekend overtime hours at his home studio .
Despite Sullivan 's loan and overtime salary , Wright was constantly short on funds .
Wright admitted that his poor finances were likely due to his expensive tastes in wardrobe and vehicles , and the extra luxuries he designed into his house .
To compound the problem , Wright 's children -- including first born Lloyd ( b.1890 ) and John ( b.1892 ) -- would share similar tastes for fine goods .
To supplement his income and repay his debts , Wright accepted independent commissions for at least nine houses .
Sullivan knew nothing of the independent works until 1893 , when he recognized that one of the houses was unmistakably a Frank Lloyd Wright design .
Aside from the location , the geometric purity of the composition and balcony tracery in the same style as the Charnley House likely gave away Wright 's involvement .
Since Wright 's five year contract forbade any outside work , the incident led to his departure from Sullivan 's firm .
A variety of stories recount the break in the relationship between Sullivan and Wright ; even Wright later told two different versions of the occurrence .
When Sullivan learned of them , he was angered and offended ; he prohibited any further outside commissions and refused to issue Wright the deed to his Oak Park house until after he completed his five years .
Wright could n't bear the new hostility from his master and thought the situation was unjust .
Dankmar Adler , who was more sympathetic to Wright 's actions , later sent him the deed .
Regardless of the correct series of events , Wright and Sullivan did not meet or speak for twelve years .
Mahony , the first licensed female architect in the U.S. , also designed furniture , leaded glass windows , and light fixtures , among other features , for Wright 's houses .
Wright 's projects during this period followed two basic models .
On one hand , there was his first independent commission , the Winslow House , which combined Sullivanesque ornamentation with the emphasis on simple geometry and horizontal lines that is typical in Wright houses .
For more conservative clients , Wright conceded to design more traditional dwellings .
As an emerging architect , Wright could not afford to turn down clients over disagreements in taste , but even his most conservative designs retained simplified massing and occasional Sullivan inspired details .
Burnham had been impressed by the Winslow House and other examples of Wright 's work ; he offered to finance a four year education at the École des Beaux-Arts and two years in Rome .
To top it off , Wright would have a position in Burnham 's firm upon his return .
In spite of guaranteed success and support of his family , Wright declined the offer .
Wright relocated his practice to his home in 1898 in order to bring his work and family lives closer .
This move made further sense as the majority of the architect 's projects at that time were in Oak Park or neighboring River Forest .
The space , which included a hanging balcony within the two story drafting room , was one of Wright 's first experiments with innovative structure .
The studio was a poster for Wright 's developing aesthetics and would become the laboratory from which the next ten years of architectural creations would emerge .
By 1901 , Wright had completed about 50 projects , including many houses in Oak Park .
Meanwhile , the Thomas House and Willits House received recognition as the first mature examples of the new style .
At the same time , Wright gave his new ideas for the American house widespread awareness through two publications in the Ladies ' Home Journal .
The articles were a answer to an invitation from the president of Curtis Publishing Company , Edward Bok , as part of a project to improve modern house design .
Bok also extended the offer to other architects , but Wright was the sole responder .
Although neither of the affordable house plans were ever constructed , Wright received increased requests for similar designs in following years .
Many examples of this work are in Buffalo , N.Y. as a result of friendship between Wright and Darwin D. Martin , an executive of the Larkin Soap Company .
Wright came to Buffalo and designed not only the Larkin Administration Building ( completed in 1904 , demolished in 1950 ) , but also homes for three of the company 's executives including the Darwin D. Martin House in 1904 .
The Robie House , with its soaring , cantilevered roof lines , supported by a 110-foot-long ( 34 m ) channel of steel , is the most dramatic .
Local gossips noticed Wright 's flirtations , and he developed a reputation in Oak Park as a man-about-town .
Mamah Cheney was a modern woman with interests outside the home .
She was an early feminist and Wright viewed her as his intellectual equal .
The two fell in love , even though Wright had been married for almost 20 years .
Often the two could be seen taking rides in Wright 's automobile through Oak Park , and they became the talk of the town .
In 1909 , even before the Robie House was completed , Wright and Mamah Cheney eloped to Europe ; leaving their own spouses and children behind .
Wright was not getting larger commissions for commercial or public buildings , which frustrated him .
This chance also allowed Wright to deepen his relationship with Mamah Cheney .
After Wright 's return to the U.S. in late 1910 , Wright persuaded his mother to buy land for him in Spring Green , Wisconsin .
The land , bought on April 10 , 1911 , was adjacent to land held by his mother 's family , the Lloyd-Joneses .
Wright began to build himself a new home , which he called Taliesin , by May 1911 .
He was nearly lynched on the spot , but was taken to the Dodgeville jail .
Wright estimated the loss at $ 250,000 to $ 500,000 .
Wright rebuilt the living quarters again , naming the home " Taliesin III " .
Wright also built several houses in the Los Angeles area .
Currently open to the public are the Hollyhock House in Hollywood and the Anderton Court Shops in Beverly Hills .
During the past two decades the Ennis House has become popular as an exotic , nearby shooting location to Hollywood television and movie makers .
Graycliff , ( 1926-1931 ) is a complex of three buildings set within a 8.4 acre landscape also designed by Wright .
Graycliff is the most extensive summer residence of Wright 's mature career .
Although there are fourteen other summer homes designed by Wright , most are cottages of less than 1000 sq feet , roughly one-fifth the size of Graycliff .
The extensive landscape , one of the very few ( if not only ) complete landscape designs in his own hand , emphasizes Wright 's conception of buildings set within landscape as a unique and complex whole .
It was designed according to Wright 's desire to place the occupants close to the natural surroundings , with a stream and waterfall running under part of the building .
Kaufmann 's own engineers argued that the design was not sound .
They were overruled by Wright , but the contractor secretly added extra steel to the horizontal concrete elements .
Taliesin West , Wright 's winter home and studio complex in Scottsdale , AZ , was a laboratory for Wright from 1937 to his death in 1959 .
Wright is responsible for a series of concepts of suburban development united under the term Broadacre City .
Usonian houses most commonly featured flat roofs and were mostly constructed without basements , completing the excision of attics and basements from houses , a feat Wright had been attempting since the early 20th century .
Many features of modern American homes date back to Wright , including open plans , slab-on-grade foundations , and simplified construction techniques that allowed more mechanization and efficiency in building .
The Solomon R. Guggenheim Museum in New York City occupied Wright for 16 years ( 1943 -- 1959 ) and is probably his most recognized masterpiece .
The building rises as a warm beige spiral from its site on Fifth Avenue ; its interior is similar to the inside of a seashell .
Its unique central geometry was meant to allow visitors to easily experience Guggenheim 's collection of nonobjective geometric paintings by taking an elevator to the top level and then viewing artworks by walking down the slowly descending , central spiral ramp , which features a floor embedded with circular shapes and triangular light fixtures to complement the geometric nature of the structure .
Unfortunately , when the museum was completed , a number of important details of Wright 's design were ignored , including his desire for the interior to be painted off-white .
The only realized skyscraper designed by Wright is the Price Tower , a 19-story tower in Bartlesville , Oklahoma .
It is also one of the two existing vertically-oriented Wright structures ( the other is the S.C. Johnson Wax Research Tower in Racine , Wis. ) .
Wright designed over 400 built structures of which about 300 survive as of 2005 .
The Imperial Hotel , in Tokyo ( 1913 ) survived the Great Kantō earthquake but was demolished in 1968 due to urban developmental pressures .
One of his projects , Monona Terrace , originally designed in 1937 as municipal offices for Madison , Wisconsin , was completed in 1997 on the original site , using a variation of Wright 's final design for the exterior with the interior design altered by its new purpose as a convention center .
Monona Terrace was accompanied by controversy throughout the 60 years between the original design and the completion of the structure .
It is the world 's largest single-site collection of Frank Lloyd Wright architecture .
Few Tahoe locals know of the iconic American architect 's plan for their natural treasure .
A design that Wright signed off on shortly before his death in 1959 -- possibly his last completed design -- was realised in late 2007 in the Republic of Ireland .
The completed house , in only the fourth country in which a Wright design has been realised , is attracting broad interest from the international architectural community .
Frank Lloyd Wright was interested in site and community planning throughout his career .
This view of decentralization was later reinforced by theoretical Broadacre City design .
In this decentralized America , all services and facilities could coexist " factories side by side with farm and home . "
For a time , Wright made more from selling art than from his work as an architect .
The following year , he helped organize the world 's first retrospective exhibition of works by Hiroshige , held at the Art Institute of Chicago .
In 1920 , however , rival art dealers began to spread rumors that Wright was selling retouched prints ; this combined with Wright 's tendency to live beyond his means , and other factors , led to great financial troubles for the architect .
Wright continued to collect , and deal in , prints until his death in 1959 , frequently using prints as collateral for loans , frequently relying upon his art business to remain financially solvent
These discoveries , and subsequent research , led to a renewed understanding of Wright 's career as an art dealer .
That year , it was learned that her dying wish had been that Wright , she and her daughter by a first marriage all be cremated and relocated to Scottsdale , Arizona .
Although the garden had yet to be finished , his remains were prepared and sent to Scottsdale where they waited in storage for an unidentified amount of time before being interred in the memorial area .
Wright 's creations took his concern with organic architecture down to the smallest details .
Wright was also one of the first architects to design and install custom-made electric light fittings , including some of the very first electric floor lamps , and his very early use of the then-novel spherical glass lampshade ( a design previously not possible due to the physical restrictions of gas lighting ) .
As Wright 's career progressed , so did the mechanization of the glass industry .
Wright fully embraced glass in his designs and found that it fit well into his philosophy of organic architecture .
In 1928 , Wright wrote an essay on glass in which he compared it to the mirrors of nature : lakes , rivers and ponds .
One of Wright 's earliest uses of glass in his works was to string panes of glass along whole walls in an attempt to create light screens to join together solid walls .
By utilizing this large amount of glass , Wright sought to achieve a balance between the lightness and airiness of the glass and the solid , hard walls .
Wright responded to the transformation of domestic life that occurred at the turn of the 20th century , when servants became a less prominent or completely absent from most American households , by developing homes with progressively more open plans .
Much of modern architecture , including the early work of Mies van der Rohe , can be traced back to Wright 's innovative work .
Wright also designed some of his own clothing .
Wright rarely credited any influences on his designs , but most architects , historians and scholars agree he had five major influences :
But , as with any architect , Wright worked in a collaborative process and drew his ideas from the work of others .
Schindler 's friend Richard Neutra also worked briefly for Wright and became an internationally successful architect .
Later in the Taliesin days , Wright employed many architects and artists who later become notable , such as Aaron Green , John Lautner , E. Fay Jones , Henry Klumb and Paolo Soleri in architecture and Santiago Martinez Delgado in the arts .
However , Wright suggested that he first take voice lessons to help overcome a speech impediment .
Bruce Goff never worked for Wright but maintained correspondence with him .
Later in his life and well after his death in 1959 , Wright received much honorary recognition for his lifetime achievements .
He was awarded the Franklin Institute 's Frank P. Brown Medal in 1953 .
He received honorary degrees from several universities ( including his " alma mater " , the University of Wisconsin ) and several nations named him as an honorary board member to their national academies of art and/or architecture .
On that list , Wright was listed along with many of the USA 's other greatest architects including Eero Saarinen , I.M. Pei , Louis Kahn , Philip Johnson and Ludwig Mies van der Rohe , and he was the only architect who had more than one building on the list .
The other three buildings were the Guggenheim Museum , the Frederick C. Robie House and the Johnson Wax Building .
In 2000 , Work Song : Three Views of Frank Lloyd Wright , a play based on the relationship between the personal and working aspects of Wright 's life , debuted at the Milwaukee Repertory Theater .
Perhaps one of the most unique ways that Wright is recognized today is that several properties designed by him are available to house overnight guests who , more than simply touring his houses , want to " live " in one , if only for a night or two .
The upper floors of the 19-story Price Tower in Okla. are a 19-room boutique hotel .
Frank Lloyd Wright was married three times and fathered seven children : four sons and three daughters .
He also adopted Svetlana Wright Peters , the daughter of his third wife , Olgivanna Lloyd Wright .
One of Wright 's sons , Frank Lloyd Wright Jr. , known as Lloyd Wright , was also a notable architect in Los Angeles .
Her widower , William Wesley Peters , was later briefly married to Svetlana Alliluyeva , the youngest child and only daughter of Joseph Stalin .
They divorced after she could not adjust to the communal lifestyle of the Wright communities , which she compared to life in the Soviet Union under her father , and because of the constant interference of Wright 's widow .
Peters served as Chairman of the Frank Lloyd Wright Foundation from 1985 to 1991 .
The architect 's personal archives are located at Taliesin West in Scottsdale , Arizona .
The Frank Lloyd Wright archives include photographs of his drawings , indexed correspondence beginning in the 1880s and continuing through Wright 's life , and other ephemera .
by Professor Anthony Alofsin , which is available at larger libraries .
