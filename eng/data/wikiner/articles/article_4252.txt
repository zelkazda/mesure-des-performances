A literary interest in the popular ballad was not new : it dates back to Thomas Percy and William Wordsworth .
Lloyd rejected this in favour of a simple distinction of economic class yet for him too folk music was , in Charles Seeger 's words , " associated with a lower class in societies which are culturally and socially stratified , that is , which have developed an elite , and possibly also a popular , musical culture . "
The term " folk " , by the start of the 21st century , could cover " singer song-writers , such as Donovan and Bob Dylan , who emerged in the 1960s and much more " or perhaps even " a rejection of rigid boundaries , preferring a conception , simply of varying practice within one field , that of ' music ' . "
The Red Army Choir recorded many albums .
The Cambridge Folk Festival in Cambridge , England is noted for having a very wide definition of who can be invited as folk musicians .
Such composers as Percy Grainger , Ralph Vaughan Williams and Béla Bartók , made field recordings or transcriptions of folk singers and musicians .
In the 1930s Jimmie Rodgers , in the 1940s Burl Ives and in the 1950s Seeger 's group The Weavers , Harry Belafonte , The Kingston Trio , and The Limeliters found a popularity that culminated in the Hootenanny television series and the associated magazine ABC-TV Hootenanny in 1963 -- 1964 .
Sing Out ! magazine helped spread both traditional and composed songs , as did folk-revival-oriented record companies .
In the 1960s , folk singers and songwriters such as Joan Baez , Bob Dylan , Phil Ochs , and Tom Paxton followed in Guthrie 's footsteps , writing " protest music " and topical songs and expressing support for the American Civil Rights Movement .
In the United Kingdom , the folk revival fostered young artists like The Watersons , Martin Carthy and Roy Bailey and a generation of singer-songwriters such as Bert Jansch , Ralph McTell , Donovan and Roy Harper .
In the second half of the 1990s , once more , folk music made an impact on the mainstream music via a younger generation of artists such as Eliza Carthy , Kate Rusby and Spiers and Boden .
Traditional folk music merged with rock and roll to form folk rock performers such as The Byrds , Simon & Garfunkel and The Mamas & the Papas .
Since the 1970s a genre of " contemporary folk " fueled by new singer-songwriters has continued with such artists as Chris Castle , Steve Goodman , and John Prine .
The Pogues and Ireland 's The Corrs brought traditional tunes back into the album charts .
In the 1980s artists like Phranc and The Knitters propagated cowpunk or folk punk , which eventually evolved into alt country .
More recently the same spirit has been embraced and expanded on by performers such as Dave Alvin , Miranda Stone and Steve Earle .
Hard rock and heavy metal bands such as Korpiklaani , Skyclad , Waylander and Finntroll meld elements from a wide variety of traditions , including in many cases instruments such as fiddles , tin whistles , accordions and bagpipes .
