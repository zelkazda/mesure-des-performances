A growing taste for louder and more aggressive sounding instruments has been the chief cause of this trend , especially since the advent of bebop in the post World War II era .
The legendary jazz pioneer Buddy Bolden played the cornet , and Louis Armstrong started off on the cornet but later switched to the trumpet .
Other influential jazz cornetists include King Oliver , Bix Beiderbecke , Ruby Braff and Nat Adderley .
Notable performances on cornet by players generally associated with the trumpet include Freddie Hubbard 's on Empyrean Isles by Herbie Hancock and Don Cherry 's on The Shape of Jazz to Come by Ornette Coleman .
