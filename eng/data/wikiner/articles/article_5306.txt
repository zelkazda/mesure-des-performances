Granville is located 22 kilometres ( 14 mi ) west of the Sydney central business district , in the local government area of the City of Parramatta .
South Granville is a separate suburb with the distinguishing feature of a light industrial area .
The area evolved primarily after 1855 , when it became the final stop of the first railway line of New South Wales .
135 millimetres of rain fell between 11.30pm and 12.30pm at Guildford , with the ensuing flood doing major damage through Granville .
The nearby RSL underwent damage and many of the club 's old photographs and honour boards were destroyed .
83 people perished , making it the worst rail disaster in Australian history .
Granville has a mixture of residential , commercial and industrial developments .
The commercial and residential developments are mostly around Granville railway station and Parramatta Road .
Granville is primarily dominated by freestanding weatherboard , fibro and unrendered brick buildings .
Granville railway station is a major station on the South line and Western line of the CityRail network .
It is also an inter-city stop on the Blue Mountains Line .
Granville is also home to a sub-branch club of the RSL .
