In many countries the guarantee against being " twice put in jeopardy " is a constitutional right ; these include Canada , India , Israel , Mexico and the U.S. .
Public outcry following the overturning of his conviction ( for perjury ) by the High Court has led to widespread calls for reform of the law along the lines of the UK legislation .
The Constitution of India under article 20 ( 3 ) only provides for autrefois convict .
Thus in India if a person is acquitted once he can be tried again .
Only the acquittal in the Supreme Court is the final acquittal which prevents any further retrial .
In the Netherlands , the state prosecution can appeal against a not-guilty verdict at the bench .
The doctrines of autrefois acquit and autrefois convict persisted as part of the common law from the time of the Norman conquest of England ; they were regarded as essential elements of protection of the liberty of the subject and respect for due process of law in that there should be finality of proceedings .
Fong Foo v. United States , 369 U.S. 141 ( 1962 ) .
The Supreme Court has also upheld laws allowing the government to appeal criminal sentences in limited circumstances ) .
For example , in United States v. Felix 503 U.S. 378 ( 1992 ) , the Supreme Court ruled : ' a [ n ] ... offense and a conspiracy to commit that offense are not the same offense for double jeopardy purposes . '
The ' separate sovereigns ' exception to double jeopardy arises from the unique nature of the American federal system , in which states are sovereigns with plenary power that have relinquished a number of enumerated powers to the federal government .
It is important to note that only the states and tribal jurisdictions are recognized as possessing a separate sovereignty , whereas U.S. territories , the military and the national capital Washington , D.C. are exclusively under federal sovereignty .
For example , O.J. Simpson was acquitted of a double homicide in a California criminal prosecution , but lost a civil wrongful death claim brought over the same victims .
The most famous U.S. court case invoking the claim of double jeopardy is probably the second 1876 murder trial of Jack McCall , killer of Wild Bill Hickok .
