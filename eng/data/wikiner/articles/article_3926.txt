The Hebrew קהלת is a feminine participle related to the root קהל meaning " to gather . "
Greek translators used " ecclesia " to render קהל ( qahal ) of the same Hebrew root .
In view of the meaning of the Hebrew root ( " gather , assemble , convene " ) one might opt for the translation " Speaker " .
The latest possible date for it is set by the fact that Ben Sirach ( written ca. 180 BCE ) repeatedly quotes or paraphrases it , as from a canonic rather than a contemporary writing .
Many modern conservative scholars today also suggest that Solomon is an unlikely author .
R ' Nachman Krochmal suggests that the term " son of David " should be interpreted to mean " descendant of David " .
For example , the book contains several Aramaic and two Persian words .
The influence of Aramaic is characteristic of late Hebrew .
But as the Jewish Encyclopedia has it :
Ecclesiastes is accepted as canonical by both Judaism and Christianity .
It was accepted because of its attribution to Solomon , and to the orthodox statement at 12:12-14 .
( This view has been disputed , as Solomon 's father , David , expressed a belief in the afterlife upon the death of Solomon 's older brother , claiming with certainty that he would see his deceased son again .
A meaningless life followed by oblivion is consistent with the purport of much ( though not all ) of the rest of the Tanakh as to the state of the dead .
Both these sets of verses contain messianic allusions , which makes the entire book a pronouncement of the sage Messiah .
This opens up the possibility of viewing the figure as the Messiah rather than as Solomon .
In the non-canonical Psalms of Solomon , the Messiah is associated with wisdom ; and Ben Sira associates Wisdom with Jerusalem .
