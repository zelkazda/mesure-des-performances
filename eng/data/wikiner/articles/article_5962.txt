In the United States , use of interrogatories is governed by the law where the case has been filed .
In nearly all U.S. jurisdictions , interrogatories are called just that and are supposed to be custom-written , although many questions can be reused from one case to the next .
