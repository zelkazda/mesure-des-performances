Brooks has received many awards in his lifetime , including the Turing Award in 1999 .
Howard Aiken was his advisor .
He worked on the architecture of the Stretch ( a $ 10m scientific supercomputer for the Los Alamos Scientific Laboratory ) and Harvest computers and then was manager for the development of the System/360 family of computers and the OS/360 software they ran .
This has since come to be known as " Brooks 's law " .
Apple executive Jean-Louis Gassée had ordered a copy of The Mythical Man-Month for every Apple engineer , technical writer , and other product-development employee .
The lecture hall was filled with Apple employees , most holding well-thumbed copies of the book .
After the lecture , Brooks had a number of interesting conversations with Apple employees , many of whom had learned the book 's lessons the hard way .
A " 20th anniversary " edition of The Mythical Man-Month with four additional chapters was published in 1995 .
