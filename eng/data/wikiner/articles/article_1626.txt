The Northrop Grumman B-2 Spirit ( also known as the Stealth Bomber ) is an American heavy bomber with " low observable " stealth technology designed to penetrate dense anti-aircraft defenses and deploy both conventional and nuclear weapons .
The B-2 is the only aircraft that can carry large air to surface standoff weapons in a stealth configuration .
The program has been the subject of espionage and counter-espionage activity and the B-2 has provided prominent public spectacles at air shows since the 1990s .
The B-2 Spirit originated from the Advanced Technology Bomber black project that began in 1979 .
In 2007 , it was revealed publicly that MIT scientists helped assess the mission effectiveness of the aircraft under classified contract during the 1980s .
This initial viewing was heavily guarded and guests were not allowed to see the rear of the B-2 .
Its first public flight was on 17 July 1989 from Palmdale .
By the early 1990s , the Soviet Union had disintegrated , which effectively rendered void the Spirit 's primary Cold War mission .
Noshir Gowadia , a design engineer who worked on the B-2 's propulsion system , was arrested in October 2005 for selling B-2 related classified information to foreign countries .
The program was the subject of public controversy for its costs to American taxpayers .
In 1996 the General Accounting Office disclosed that the USAF 's B-2 bombers " will be , by far , the most costly bombers to operate on a per aircraft basis " , costing over three times as much as the B-1B and over four times as much as the B-52H .
In September 1997 , each hour of B-2 flight necessitated 119 hours of maintenance in turn .
The procurement cost per aircraft as detailed in General Accounting Office ( GAO ) reports , which include spare parts and software support , was $ 929 million per aircraft in 1997 dollars .
In its consideration of the fiscal year 1990 defense budget , the House Armed Services Committee trimmed $ 800 million from the B-2 research and development budget , while at the same time staving off a motion to kill the bomber .
The growing cost of the B-2 program , and evidence of flaws in the aircraft 's ability to elude detection by radar , were among factors which drove opposition .
The contract provides advanced state-of-the-art radar components , with the aim of sustained operational viability of the B-2 fleet into the future .
A modification to the radar was needed since the U.S. Department of Commerce required the B-2 to use a different radar frequency .
It was reported on 22 July 2009 that the B-2 had passed the second of the two USAF audit milestones associated with this upgraded AESA radar capability .
The B-2 's low-observable , or " stealth " , characteristics give it the ability to penetrate an enemy 's most sophisticated anti-aircraft defenses to attack its most heavily defended targets .
The B-2 's composite materials , special coatings and flying wing design , which reduces the number of leading edges , contribute to its stealth characteristics .
The Spirit has a radar signature of about 0.1 m 2 .
Each B-2 requires a climate-controlled hangar large enough for its 172-foot ( 52 m ) wingspan to protect the operational integrity of its sophisticated radar absorbent material and coatings .
The blending of low-observable technologies with high aerodynamic efficiency and large payload gives the B-2 significant advantages over previous bombers .
The U.S. Air Force reports its range as approximately 6000 nautical miles ( 6900 mi ; 11000 km ) .
Also , its low-observation ability provides the B-2 greater freedom of action at high altitudes , thus increasing its range and providing a better field of view for the aircraft 's sensors .
The B-2 has a crew of two : a pilot in the left seat , and mission commander in the right .
The B-2 has provisions for a third crew member if needed .
For comparison , the B-1B has a crew of four and the B-52 has a crew of five .
B-2 crews have been used to pioneer sleep cycle research to improve crew performance on long sorties .
The B-2 is highly automated , and , unlike two-seat fighters , one crew member can sleep , use a toilet or prepare a hot meal while the other monitors the aircraft .
As with the B-52 Stratofortress and B-1 Lancer , the B-2 provides the versatility inherent in manned bombers .
The prime contractor , responsible for overall system design , integration and support , is Northrop Grumman .
Boeing , Raytheon ( formerly Hughes Aircraft ) , G.E. and Vought Aircraft Industries , are subcontractors .
The first operational aircraft , christened Spirit of Missouri , was delivered to Whiteman Air Force Base , Missouri , where the fleet is based , on 17 December 1993 .
The B-2 reached initial operational capability on 1 January 1997 .
The B-2 has seen service in three campaigns .
Its combat debut was during the Kosovo War in 1999 .
The B-2 has been used to drop bombs on Afghanistan in support of the ongoing War in Afghanistan .
With the support of aerial refueling , the B-2 flew one of its longest missions to date from Whiteman Air Force Base , Missouri to Afghanistan and back .
Other sorties in Iraq have launched from Whiteman AFB .
The designated " forward operating locations " have been previously designated as Guam and RAF Fairford , where new climate controlled hangars have been constructed .
The B-2 's combat use preceded a U.S. Air Force declaration of " full operational capability " in December 2003 .
On 23 February 2008 , a B-2 crashed on the runway shortly after takeoff from Andersen Air Force Base in Guam .
The Spirit of Kansas , 89-0127 had been operated by the 393rd Bomb Squadron , 509th Bomb Wing , Whiteman Air Force Base , Missouri , and had logged 5,176 flight hours .
It was the first crash of a B-2 .
Because of its high cost , strategic bombing role , and the still-classified aspects of its low observable coatings , no production B-2 has been placed on permanent display .
Although not an actual replica of a B-2 , the mock-up was close enough to the B-2 's design to arouse suspicion that Honda had intercepted classified , top secret information , as the B-2 project was still officially classified in 1988 .
Honda donated the model to the museum in 1989 , on condition that the model be destroyed if it was ever replaced with a different aircraft .
Later avionics and equipment improvements allow B-2A to carry JSOW , GBU-28 , and GBU-57A/Bs as well .
The Spirit is also designated as a delivery aircraft for the AGM-158 JASSM when the missile enters service .
