In medieval Europe , industry became dominated by the guilds in cities and towns , who offered mutual support for the member 's interests , and maintained standards of industry workmanship and ethical conduct .
Early instances of industrial warfare were the Crimean War and the American Civil War , but its full potential showed during the world wars .
