Stith Thompson made a major attempt to index the motifs of both folklore and mythology , providing an outline into which new motifs can be placed , and scholars can keep track of all older motifs .
Sometimes " folklore " is religious in nature , like the tales of the Welsh Mabinogion or those found in Icelandic skaldic poetry .
Examples of such Christian mythology are the themes woven round Saint George or Saint Christopher .
The familiar folktale , " Hansel and Gretel " , is an example of this fine line .
Propp discovered a uniform structure in Russian fairy tales .
Folklorist William Bascom states that folklore has many cultural aspects , such as allowing for escape from societal consequences .
Examples of artists which have used folkloric themes in their music would be : Bill Monroe , Flatt and Scruggs , Old Crow Medicine Show , Jim Croce , and many others .
