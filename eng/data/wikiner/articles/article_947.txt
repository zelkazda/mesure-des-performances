During the 1980s and 1990s she became one of the first gospel artists to cross over into mainstream pop , on the heels of her successful albums Unguarded and Heart in Motion , the latter of which included the number-one single " Baby Baby " .
Heart in Motion is her highest selling album , with over five million copies sold in the United States alone .
She was honored with a star on Hollywood Walk of Fame in 2005 for her contributions to the entertainment industry .
During 1976 , Grant wrote her first song , performed in public for the first time -- at Harpeth Hall School -- the all-girls school she attended , recorded a demo tape for her parents with church youth-leader Brown Bannister , then later when Bannister was dubbing a copy of the tape , Chris Christian , the owner of the recording studio , heard the demo and called Word Records .
In 1977 , she recorded her first album titled Amy Grant , produced by Brown Bannister ( who would also produce her next eleven albums ) .
In May 1979 , while at the album release party for her second album , My Father 's Eyes , Grant met Gary Chapman , writer of the title track ( and future husband ) .
In the fall of 1980 , she transferred to Vanderbilt University , where she was a member of the sorority Kappa Alpha Theta .
1982 saw the release of her breakthrough album Age to Age .
In the mid-1980s , Grant began touring and recording with young up-and-coming songwriter Michael W. Smith .
During the 1980s , Grant was also a backup singer for Bill Gaither .
Unguarded ( 1985 ) surprised some fans for its very mainstream sound ( and Grant 's leopard-print jacket , in four poses for four different covers ) .
In 1989 she appeared in a Target ad campaign , performing songs off the album .
When Heart in Motion was released in 1991 , many fans were surprised that the album was so clearly one of contemporary pop music .
Grant 's desire to widen her audience was frowned upon by the confines of the popular definitions of ministry at the time .
The track " Baby Baby " became a pop hit , and Grant was established as a name in the mainstream music world .
Heart in Motion is Grant 's best-selling album , having sold over 5 million copies according to the RIAA .
House of Love in 1994 continued in the same vein , boasting catchy pop songs mingled with spiritual lyrics .
Although " Takes A Little Time " was a moderate hit single , the album failed to sell like the previous two albums , which had both gone multi-platinum .
The video for " Takes A Little Time " was a new direction for Grant ; with a blue light filter , acoustic guitar , the streets and characters of New York City , and a plot , Grant was re-cast as an adult light rocker .
Grant returned to her gospel music roots with the 2002 release of Legacy ... Hymns and Faith .
The album featured a Vince Gill-influenced mix of bluegrass and gospel and marked Grant 's 25th anniversary in the music industry .
Grant followed this up with the pop release Simple Things in 2003 .
The same year , Grant was inducted into the Gospel Music Hall of Fame by the Gospel Music Association , an industry trade organization of which she is a longstanding member , in her first year of eligibility .
Grant released a sequel to her hymns collection in 2005 titled Rock of Ages ... Hymns & Faith .
Although neither of her latest hymn releases have captured the popularity of her previous gospel career , Grant still remains a popular concert draw and enjoys popularity amongst both fan bases .
Grant joined the reality television phenomenon by hosting Three Wishes , a show in which she and a team of helpers make wishes come true for small-town residents .
The show debuted on NBC in the fall of 2005 and was canceled at the end of its first season because of high production costs .
In a February 2006 webchat , Amy stated she believes her " best music is still ahead " .
In addition to receiving a star on the Hollywood Walk of Fame , media appearances included write-ups in CCM Magazine , and a performance on The View .
In a February 2007 web chat on her web site , Amy discussed a book she was working on entitled " Mosaic : Pieces of My Life So Far " : " It 's not an autobiography , but more a collection of memories , song lyrics , poetry and a few pictures . "
In the same web chat , Amy noted that she is " anxious to get back in the studio after the book is finished , and reinvent myself as an almost-50 performing woman . "
2007 was Grant 's 30th year in music .
The two-disc release includes the original album and a second disc with new acoustic recordings , live performances from 1989 , and interviews with Amy .
Grant recreated the Lead Me On tour in the fall of 2008 .
On June 27 , 2008 , Grant surprised everyone at the Creation Northeast Festival by being the special guest .
She performed " Lead Me On " and a few other songs backed with the Hawk Nelson band .
( Pearl 's real name was Sarah Ophelia Colley Cannon . )
Citing " irreconcilable differences " , Grant filed for divorce from Chapman in March 1999 , and the divorce was final in June 1999 .
" I did n't get a divorce because I had a great marriage and then along came Vince Gill .
In an interview early in her career , Grant stated " I have a healthy sense of right and wrong , but sometimes , for example , using foul , exclamation-point words among friends can be good for a laugh . "
Within the same article , Grant expressed an opinion that those most opposed to premarital sex and rock music often base their views in part on having experienced emotional distress .
