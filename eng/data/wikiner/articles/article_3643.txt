Easter eggs or spring eggs are special eggs that are often given to celebrate Easter or springtime .
The sculptures on the walls of Persepolis show people carrying eggs for Nowrooz to the king .
A tradition exists in some parts of the United Kingdom ( such as Scotland and North East England ) of rolling painted eggs down steep hills on Easter Sunday .
When boiling hard-cooked eggs for Easter , a popular tan colour can be achieved by boiling the eggs with onion skins .
In the UK the dance is called the hop-egg .
In some Mediterranean countries , especially in Lebanon , chicken eggs are boiled and decorated by dye and/or painting and used as decoration around the house .
Then , with the coming of Easter , Pascha the eating of eggs resumes .
One would have been forced to hard boil the eggs that the chickens produced so as not to waste food , and for this reason the Spanish dish hornazo ( traditionally eaten on and around Easter ) contains hard-boiled eggs as a primary ingredient .
In Hungary , easter eggs are used sliced in potato casseroles around Easter .
