C. S. Lewis , in The Abolition of Man , maintained that there are certain acts that are universally considered evil , such as rape and murder .
Up until the mid-19th century , the United States -- along with many other countries -- practiced forms of slavery .
Plato wrote that there are relatively few ways to do good , but there are countless ways to do evil , which can therefore have a much greater impact on our lives , and the lives of other beings capable of suffering .
For this reason , philosophers such as Bernard Gert maintain that preventing evil is more important than promoting good in formulating moral rules and in conduct .
When we label someone as bad or evil , Rosenberg claims , it invokes the desire to punish or inflict pain .
Prominent American psychiatrist M. Scott Peck on the other hand , describes evil as " militant ignorance " .
The original Judeo-Christian concept of " sin " is as a process that leads us to " miss the mark " and fall short of perfection .
Peck argues that while most people are conscious of this at least on some level , those that are evil actively and militantly refuse this consciousness .
Peck characterizes evil as a malignant type of self-righteousness which results in a projection of evil onto selected specific innocent victims ( often children or other people in relatively powerless positions ) .
Peck considers those he calls evil to be attempting to escape and hide from their own conscience ( through self deception ) and views this as being quite distinct from the apparent absence of conscience evident in sociopaths .
According to Peck , an evil person :
He also considers certain institutions may be evil , as his discussion of the My Lai Massacre and its attempted coverup illustrate .
Anton LaVey , the late founder of the Church of Satan , asserts that evil is actually good ( an often-used slogan is , " evil is live spelled backwards " ) .
Even Martin Luther allowed that there are cases where a little evil is a positive good .
In certain schools of political philosophy , leaders are encouraged to be indifferent to good or evil , taking actions based solely on practicality ; this approach to politics was put forth by Niccolò Machiavelli , a 16th-century Florentine writer who advised politicians " ... it is far safer to be feared than loved . "
Machiavelli wrote : " ... there will be traits considered good that , if followed , will lead to ruin , while other traits , considered vices which if practiced achieve security and well being for the Prince . "
While some forms of Judaism , do not personify evil in Satan ; these instead consider the human heart to be inherently bent toward deceit , although human beings are responsible for their choices , whereas in Judaism , there is no prejudice in one 's becoming good or evil at time of birth .
Guayota is the king of evil genies .
One of the slogans of Google is " Do n't be evil " , and the tagline of independent music recording company Magnatune is " we are not evil, " referring to the alleged evils of the RIAA .
