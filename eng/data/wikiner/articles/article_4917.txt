The transportation sector comprises the physical facilities , terminals , fleets and ancillary equipment of all the various modes of transport operating in Guyana , the transport services , transport agencies providing these services , the organisations and people who plan , build , maintain , and operate the system , and the policies that mould its development .
In the early 1970s a two-lane road with modern geometry and surfaced with laterite was built between Linden and Rockstone .
In the period 1974 to 1978 , an attempt was made to build a road between Rockstone and Kurupung to facilitate the construction of a large hydroelectric station .
Linden is therefore one of the main hubs for road transportation in the hinterland .
The Demerara Harbour Bridge is a two-lane floating bridge , 1.2 miles long , near the mouth of the Demerara River .
The bridge was completed in 2009 , enabling economic interests in northern Brazil to link by road to the port at Georgetown .
In the Matthew 's Ridge area , there is a 32-mile ( 51.5 km ) railway service .
A railway service was once operated in Linden for the movement of bauxite ore .
The infrastructure that supports water transport in Guyana is located along the banks of the navigable rivers , namely , the Essequibo River , Demerara River and Berbice River .
The main port of Georgetown , located at the mouth of the Demerara River , comprises several wharves , most of which are privately owned .
In addition , three berths are available for oceangoing vessels at Linden .
However , recent improvements in the channel in the Berbice River have made it possible for ships of up to 55,000 DWT to dock there .
In the case of sugar , for example , 98 percent of exports is delivered by barge to the port of Georgetown for export .
Only two ferry services consistently show profits : the Rosignol-New Amsterdam and the Parika-Adventure .
5,900 km total of navigable waterways ; Berbice River , Demerara River , and Essequibo River are navigable by oceangoing vessels for 150 km , 100 km , and 80 km , respectively
