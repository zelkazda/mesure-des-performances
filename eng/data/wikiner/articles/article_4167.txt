Empress Kōken , also known as Empress Shōtoku was the 46th and the 48th emperor of Japan , according to the traditional order of succession .
Her reign as Saimei encompassed 764-770 .
Empress Kōken reigned for ten years .
Although there were seven other reigning empresses , their successors were most often selected from amongst the males of the paternal Imperial bloodline , which is why some conservative scholars argue that the women 's reigns were temporary and that male-only succession tradition must be maintained in the 21st century .
Empress Gemmei , who was followed on the throne by her daughter , Empress Genshō , remains the sole exception to this conventional argument .
Empress Shōtoku rule for ten years .
As with the seven other reigning empresses whose successors were most often selected from amongst the males of the paternal Imperial bloodline , she was followed on the throne by a male cousin , which is why some conservative scholars argue that the women 's reigns were temporary and that male-only succession tradition must be maintained in the 21st century .
Empress Gemmei , who was followed on the throne by her daughter , remains the sole exception to this conventional argument .
Shōtoku died of smallpox , after which she was succeeded by her first cousin twice removed , Emperor Kōnin .
The kugyō during Shōtoku 's reign included :
The years of Shōtoku 's reign are more specifically identified by more than one era name or nengō .
