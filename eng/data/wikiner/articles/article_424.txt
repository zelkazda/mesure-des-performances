He was the pupil and successor of Gorgias and taught at Athens at the same time as Isocrates , whose rival and opponent he was .
According to Alcidamas , the highest aim of the orator was the power of speaking extempore on every conceivable subject .
Mahaffy , 1891 , pl .
