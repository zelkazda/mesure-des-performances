The First Lateran Council was called by Pope Callistus II whose reign began February 1 , 1119 .
Pope Paschal II confirmed these which were received in general terms , on October 20 , 1112 .
Guido was later created cardinal by Pope Paschal II .
On the death of Paschal II , January 21 , 1118 , Gelasius II was elected pope .
However , it soon became clear that Henry was not willing to concede his presumed and ancient right to name the pope and bishops within his kingdom .
Perhaps to demonstrate conciliation or because of political necessity , Henry withdrew his support for antipope Gregory VIII .
However , Henry showed up with an army of thirty thousand men .
It was clear by now that Henry was in no mood to reconcile and a compromise with him was not to be had .
Henry was forced by circumstances to seek a peace with Callistus .
Callistus died December 13 , 1124 .
He was succeeded by Pope Honorius II .
Priests , deacons , and subdeacons are forbidden to live with women other than such as were permitted by the Nicene Council .
We absolutely forbid priests , deacons , and subdeacons to associate with concubines and women , or to live with women other than such as the Nicene Council ( canon 3 ) for reasons of necessity permitted , namely , the mother , sister , or aunt , or any such person concerning whom no suspicion could arise .
Taxes paid to bishops by monks since Gregory VII must be continued .
The tax ( servitium ) which monasteries and their churches have rendered to the bishops since the time of Gregory VII , shall be continued .
Historians have largely considered his rule to be a disaster , calling it The Anarchy .
In the end , Henry V died the monarch of a much diminished kingdom .
