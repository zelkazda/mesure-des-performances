Amphetamine ( USAN ) or amfetamine ( INN ) is a psychostimulant drug that is known to produce increased wakefulness and focus in association with decreased fatigue and appetite .
The European Monitoring Centre for Drugs and Drug Addiction reports the typical retail price of amphetamine in Europe varied between € 10 and € 15 ( $ 14.38 to $ 21.55 USD ) a gram in half of the reporting countries .
The related compound methamphetamine was first synthesized from ephedrine in Japan in 1918 by chemist Akira Ogata , via reduction of ephedrine using red phosphorus and iodine .
Alexander Shulgin , one of the most experienced biochemical investigators and the discoverer of many new psychotropic substances , has tried to contact the Texas A & M researchers and verify their findings .
American bomber pilots use amphetamine ( " go pills " ) to stay awake during long missions .
The Tarnak Farm incident , in which an American F-16 pilot killed several friendly Canadian soldiers on the ground , was blamed by the pilot on his use of amphetamine .
Amphetamine use has historically been especially common among Major League Baseball players and is usually known by the slang term " greenies " .
In 2006 , the MLB banned the use of amphetamine .
However , the MLB has received some criticism because the consequences for amphetamine use are dramatically less severe than for anabolic steroid use , with the first offense bringing only a warning and further testing .
From the 1960s onward , amphetamine has been popular with many youth subcultures in Britain ( and other parts of the world ) as a recreational drug .
Jack Kerouac was a particularly avid user of amphetamine , which was said to provide him with the stamina needed to work on his novels for extended periods of time .
Amphetamine is frequently mentioned in the work of American journalist Hunter S. Thompson .
Erdős won the bet , but complained during his abstinence that mathematics had been set back by a month : " Before , when I looked at a piece of blank paper my mind was filled with ideas .
Many songs have been written about amphetamine , for example in the track entitled " St. Ides Heaven " from singer/songwriter , Elliott Smith 's self-titled album .
Hüsker Dü , Jesus and Mary Chain 's and The Who were keen amphetamine users early in their existence .
Land Speed Record is an allusion to Hüsker Dü 's amphetamine use .
Motorhead named themselves after the slang for an amphetamine addict .
Producer David O. Selznick was an amphetamine user , and would often dictate long and rambling memos under the influence of amphetamine to his directors .
In the film Requiem for a Dream , Ellen Burstyn portrays Sara Goldfarb , an elderly widow who becomes addicted to weight-loss amphetamine pills .
