Nevertheless , composers from the 1750s to the present day continue to write and study fugue for various purposes ; they appear in the works of Mozart and Beethoven , and many composers such as Felix Mendelssohn ( 1809 -- 1847 ) , Anton Reicha ( 1770 -- 1836 ) and Dmitri Shostakovich ( 1906 -- 1975 ) wrote cycles of fugues .
While the voices of a fugue may enter in any order desired , Bach frequently reserves the bass entry until last , for dramatic effect .
While not all of Bach 's fugues contain episodes , most have at least one serving as a modulatory bridge passage between the middle-entries , on which the greater emphasis is usually laid .
In the fugues of J. S. Bach the first middle-entry very commonly occurs in the relative major or minor of the work 's overall key , and is followed by an entry the dominant of the relative major or minor when the fugue 's subject requires a tonal answer .
In the fugues of earlier composers ( notably Buxtehude and Pachelbel ) , middle entries in keys other than the tonic and dominant tend to be the exception , and non-modulation the norm .
Here Bach has altered countersubject 2 to accommodate the change of mode .
The fugue of Passacaglia and Fugue in C minor , BWV 582 is unusual in this sense , since it does have episodes between permutation expositions .
Palestrina 's imitative motets differed from fugues in that each phrase of the text had a different subject which was introduced and worked out separately , whereas a fugue continued working with the same subject or subjects throughout the entire length of the piece .
Jan Pieterszoon Sweelinck , Girolamo Frescobaldi , Johann Jakob Froberger and Dieterich Buxtehude all wrote fugues , and George Frideric Handel included them in many of his oratorios .
The second movement of a sonata da chiesa , as written by Arcangelo Corelli and others , was usually fugal .
Fux 's work was largely based on the practice of Palestrina 's modal fugues .
Haydn , for example , taught counterpoint from his own summary of Fux , and thought of it as the basis for formal structure .
Johann Sebastian Bach often entered into contests where he would be given a subject with which to spontaneously improvise a fugue on the organ or harpsichord .
The Art of Fugue is a collection of fugues ( and four canons ) on a single theme that is gradually transformed as the cycle progresses .
Bach also wrote smaller single fugues , and put fugues into many of his works that were not fugues per se .
Nevertheless , both Haydn and Mozart had periods of their careers in which they in some sense " rediscovered " fugal writing and used it frequently in their work .
This was a practice that Haydn repeated only once later in his quartet-writing career , with the finale of his quartet op. 50 no. 4 ( 1787 ) .
Some of the earliest examples of Haydn 's use of counterpoint , however , are in three symphonies that date from 1762 -- 63 .
However , the major impetus to fugal writing for Mozart was the influence of Baron Gottfried van Swieten in Vienna around 1782 .
Later , Mozart incorporated fugal writing into the finale of his Symphony No. 41 and his opera Die Zauberflöte .
During his early career in Vienna , Beethoven attracted notice for his performance of these fugues .
There are fugal sections in Beethoven 's early piano sonatas , and fugal writing is to be found in the second and fourth movements of the Eroica Symphony ( 1805 ) .
Beethoven incorporated fugue in his sonatas , and reshaped the episode 's purpose and compositional technique for later generations of composers .
Nevertheless , fugues did not take on a truly central role in Beethoven 's work until his " late period . "
Beethoven 's last piano sonata , op. 111 ( 1822 ) integrates fugal texture throughout the first movement , written in sonata form .
One manual explicitly stated that the hallmark of contrapuntal style was the style of J. S. Bach .
The 19th century 's taste for academicism -- setting of forms and norms by explicit rules -- found Marpurg , and the fugue , to be a congenial topic .
The writing of fugues also remained an important part of musical education throughout the 19th century , particularly with the publication of the complete works of Bach and Handel , and the revival of interest in Bach 's music .
The finale of Giuseppe Verdi 's opera Falstaff is a ten-voice fugue .
Felix Mendelssohn was obsessed with fugal writing , as it can be found prominently in the Scottish Symphony , Italian Symphony , and the Hebrides Overture .
In the last movement of his Fifth Symphony Anton Bruckner wrote the development section in form of a big double fugue .
Another composer of this time , whose work is strongly influenced by fugal textures , was Felix Draeseke .
Robert Schumann , and Johannes Brahms also included fugues in many of their works .
The final part of Schumann 's Piano Quintet is a double fugue , and his opus numbers 126 , 72 and 60 are all sets of fugues for the piano .
Sergei Rachmaninoff , despite writing in a lush post-romantic idiom , was highly skilled in counterpoint ( as is highly evident in his Vespers ) ; a well known fugue occurs in his Symphony No. 2 .
The second movement of his Sonata for Solo Violin is also a fugue .
Igor Stravinsky also incorporated fugues into his works , including the Symphony of Psalms and the Dumbarton Oaks concerto .
In a different direction , the tonal fugue movement of Charles Ives ' fourth symphony evokes a nostalgia for an older , halcyon time .
The melodic material in this fugue is totally chromatic , with melismatic ( running ) parts overlaid onto skipping intervals , and use of polyrhythm ( multiple simulataneous subdivisions of the measure ) , blurs everything both harmonically and rhythmically so as to create an aural aggregate , thus highlighting the theoretical/aesthetic question of the next section as to whether fugue is a form or a texture , Ligeti 's using the form ( actually , its counterpoint ) to create a texture .
Benjamin Britten composed a fugue for orchestra in his The Young Person 's Guide to the Orchestra , consisting of subject entries by each instrument once .
John Cage wrote a fugue for unpitched percussion instruments which substitutes complex rhythmic ratios for key areas .
20th-century fugue writing explored many of the directions implied by Beethoven 's Grosse Fuge , and what came to be termed " free counterpoint " as well as " dissonant counterpoint . "
Fugal technique as described by Marpurg became part of the theoretical basis for Schoenberg 's twelve-tone technique .
It is important to note that Gould wrote traditional tonal-style canons with his rows as opposed to adopting Webern-like canonic principles where the tone row dictates the phrase entry .
However , it is to be noted that while certain related keys are more commonly explored in fugal development , the overall structure of a fugue does not limit its harmonic structure as much as Ratz would have us believe .
The philosopher Theodor Adorno , a skilled pianist and interpreter of Beethoven 's music , expressed a sense of the arduousness and also the inauthenticity of modern fugue composition , or any composing of fugue in a contemporary context , i.e. , as an anachronism [ citation needed ] .
Adorno 's conservative and historically bound view of Bach is not found among most modern fugue composers , such as David Diamond , Paul Hindemith or Dmitri Shostakovich .
Others , such as Alfred Mann , argued that fugue writing , by focusing the compositional process actually improves or disciplines the composer towards musical ideas .
