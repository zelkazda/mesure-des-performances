The backbone cabal was a group of large-site administrators who pushed through the Great Renaming of Usenet newsgroups during most of the 1980s .
It was created in an effort to stabilize the Usenet propagation : While many news servers operated during night time to save the cost of long distance communication , servers of the backbone cabal were available 24 hours a day .
As Usenet has few technologically or legally enforced hierarchies , just about the only ones that formed were social hierarchies .
People exerted power through force of will ( often via intimidating flames ) , garnering authority and respect by spending much time and effort contributing to the community ( by being a maintainer of a FAQ , for example ; see also Kibo , etc. ) .
