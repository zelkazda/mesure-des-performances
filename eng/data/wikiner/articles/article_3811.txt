The book ends with the construction of the Tabernacle .
According to tradition , Exodus and the other four books of the Torah were written by Moses .
The pharaoh 's daughter finds the child , and names him Moses , and brings him up as her own .
Moses and his brother Aaron do so , but pharaoh refuses .
The Exodus begins .
Yahweh asks whether they will agree to be his people , and they accept .
Moses descends and writes down Yahweh 's words and the people agree to keep them .
Yahweh calls Moses up the mountain to receive a set of stone tablets containing the law , and he and Joshua go up , leaving Aaron in charge .
Yahweh appears on the mountain " like a consuming fire " and calls Moses to go up , and Moses goes up the mountain .
Aaron is appointed as the first High Priest , and the priesthood is to be hereditary in his line .
Moses descends from the mountain , and his face is transformed , so that from that time onwards he has to hide his face with a veil .
More than a century of archaeological research has discovered nothing which could support the narrative elements of the book of Exodus .
Scholars who hold the Exodus to represent historical truth concede that the most the evidence can suggest is plausibility .
Equally unsettled is the question of the structure of Exodus -- it has been divided by scholars into anywhere from two to five sections , all reflecting various aspects of the book 's internal logic , but there is no single analysis which captures all the possible features that need to be taken into account .
Another consideration is the possibility that Exodus as we have it may simply be a by-product of the size of the scrolls used by the ancient scribes , since it was originally part of what was apparently conceived as part of the single narrative of the Torah .
