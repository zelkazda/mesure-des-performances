The EFA and European Green Party form The Greens -- European Free Alliance political grouping in the European Parliament .
Since the 1979 European Parliament election regionalists and separatists have been represented in the European Parliament .
It was not until the 1989 European Parliament election that the EFA members formed a united group in the European Parliament .
In 1989 the regionalists , including EFA-members , formed a group called the Rainbow Group as well .
In the 1994 European Parliament election the regionalists lost considerably .
Moreover they had suspended the membership of Lega Nord for entering in a government with the post-fascist National Alliance and the Basque Nationalist Party had joined the European People 's Party .
This group was called European Radical Alliance .
The cooperation between EFA and the Greens was continued .
After the election the New-Flemish Alliance also joined the EFA .
The EFA sees itself as an alliance of stateless peoples , which are striving towards independence or autonomy .
It believes however that Europe should move away from further centralisation .
The EFA stands on the left of the political spectrum , and in the Brussels declaration it emphasizes the protection of human rights , sustainable development and social justice .
In 2007 the EFA congress in Bilbao added several progressive principles to the declaration : including a commitment to fight against racism , antisemitism , discrimination , xenophobia and islamophobia and a commitment to get full citizenship for migrants , including voting rights .
It is the supreme council of the EFA .
Only member parties can participate in the EFA .
The EFA also has observers .
Before becoming member a party needs to have been observer of the EFA for at least one year .
The EFA also recognizes friends of the EFA , a special status for regionalist parties outside of the European Union .
It is chaired by Eric Defoort , a member of the New-Flemish Alliance .
Several prominent regional , regionalist , secessionist or minority parties are not members of EFA .
