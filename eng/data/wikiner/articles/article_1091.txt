Many descriptions of the amphisbaena say its eyes glow like candles or lightning , but the poet Nicander seems to contradict this by describing it as " always dull of eye " .
In the Dungeons & Dragons fantasy role-playing game , the Amphisbaena is depicted as in traditional myth as a giant serpent with a head at both ends .
