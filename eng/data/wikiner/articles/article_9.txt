Inspired by time Gershwin had spent in Paris , it is in the form of an extended tone poem evoking the sights and energy of the French capital in the 1920s .
It is one of Gershwin 's best-known compositions .
Gershwin composed the piece on commission from the New York Philharmonic .
Gershwin brought back some Parisian taxi horns for the New York premiere of the composition which took place on December 13 , 1928 in Carnegie Hall with Walter Damrosch conducting the New York Philharmonic .
Arthur Fiedler and the Boston Pops Orchestra recorded the work for RCA Victor , including one of the first stereo recordings of the music .
A part of the symphonic composition is also featured in As Good as It Gets , released in 1997 .
