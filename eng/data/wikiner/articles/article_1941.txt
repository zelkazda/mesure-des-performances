In the last years of his career he featured in several of Ed Wood 's low budget films .
At the age of 12 , Lugosi dropped out of school .
He went on to Shakespeare plays and other major roles .
During World War I , he served as an infantry lieutenant in the Austro-Hungarian Army from 1914 to 1916 .
Due to his activism in the actors union in Hungary during the time of the Hungarian Revolution of 1919 , he was forced to flee his homeland .
He first went to Vienna , Austria , and then settled in Germany , where he continued acting .
Eventually , he traveled to New Orleans as a crewman aboard a merchant ship .
Lugosi 's first film appearance was in the 1917 movie Az ezredes .
Lugosi made twelve films in Hungary between 1917 and 1918 before leaving for Germany .
Following the collapse of Béla Kun 's Hungarian Soviet Republic in 1919 , leftists and trade unionists became vulnerable .
Lugosi was proscribed from acting due to his participation in the formation of an actor 's union .
In exile in Germany , he began appearing in a small number of well received films , including adaptations of the Karl May novels , Auf den Trümmern des Paradieses ( On the Brink of Paradise ) , and Die Todeskarawane ( The Caravan of Death ) , opposite the ill-fated Jewish actress Dora Gerson .
Lugosi left Germany in October 1920 , intending to emigrate to the United States , and entered the country at New Orleans in December 1920 .
He made his way to New York and was legally inspected for immigration at Ellis Island in March 1921 .
He declared his intention to become a U.S. citizen in 1928 , and on June 26 , 1931 , he was naturalized .
On his arrival in America , the 6 foot 1 inch ( 1.85 m ) , 180 lb .
( 82 kg ) Béla worked for some time as a laborer , then entered the theater in New York City 's Hungarian immigrant colony .
His first American film role came in the 1923 melodrama The Silent Command .
Several more silent roles followed , as villains or continental types , all in productions made in the N.Y. area .
The Horace Liveright production was successful , running 261 performances before touring .
A persistent rumor asserts that director Tod Browning 's long-time collaborator Lon Chaney was Universal 's first choice for the role , and that Lugosi was chosen only due to Chaney 's death shortly before production .
This is questionable , because Chaney had been under long-term contract to Metro-Goldwyn-Mayer since 1925 , and had negotiated a lucrative new contract just before his death .
Chaney and Browning had worked together on several projects ( including four of Chaney 's final five releases ) , but Browning was only a last-minute choice to direct the movie version of Dracula after the death of director Paul Leni , who was originally slated to direct .
Following the success of Dracula , Lugosi received a studio contract with Universal .
They had a child , Bela G. Lugosi , in 1938 .
Through his association with Dracula ( in which he appeared with minimal makeup , using his natural , heavily accented voice ) , Lugosi found himself typecast as a horror villain in such movies as Murders in the Rue Morgue , The Raven , and Son of Frankenstein for Universal , and the independent White Zombie .
Lugosi did attempt to break type by auditioning for other roles .
It is an erroneous popular belief that Lugosi declined the offer to appear in Frankenstein due to make-up .
Lugosi may not have been happy with the onerous makeup job and lack of dialogue , but was still willing to play the part of Frankenstein 's monster .
Nonetheless , James Whale , the film 's director , replaced Lugosi and would do this again in Bride of Frankenstein ( Lugosi was supposed to play the role of Septimus Pretorius ) .
Regardless of controversy , five films at Universal -- The Black Cat , The Raven , The Invisible Ray , Son of Frankenstein , Black Friday ( plus minor cameo performances in 1934 's Gift of Gab ) and one at RKO Pictures , The Body Snatcher -- paired Lugosi with Karloff .
Despite the relative size of their roles , Lugosi inevitably got second billing , below Karloff .
Lugosi 's attitude toward Karloff is the subject of contradictory reports , some claiming that he was openly resentful of Karloff 's long-term success and ability to get good roles beyond the horror arena , while others suggested the two actors were -- for a time , at least -- good friends .
When this proved not to be the case , according to Karloff , Lugosi settled down and they worked together amicably ( though some have further commented that Karloff 's on-set demand to break from filming for mid-afternoon tea annoyed Lugosi ) .
Lugosi 's thick accent also hindered the variety of roles he was offered .
A number of factors worked against Lugosi 's career in the mid-1930s .
These low-budget thrillers indicate that Lugosi was less discriminating than Karloff in selecting screen vehicles , but the exposure helped Lugosi financially if not artistically .
The same year saw Lugosi playing a straight character role in a major motion picture : he was a stern commissar in MGM 's Greta Garbo comedy Ninotchka .
At Universal , he often received star billing for what amounted to a supporting part .
In 1943 , he finally played the role of Frankenstein 's monster in Universal 's Frankenstein Meets the Wolfman , which this time contained dialogue .
He also got to recreate the role of Dracula a second and last time on film in Abbott and Costello Meet Frankenstein in 1948 .
By this time , Lugosi 's drug use was so notorious that the producers were n't even aware that Lugosi was still alive , and had penciled in actor Ian Keith for the role .
Abbott and Costello Meet Frankenstein was Bela Lugosi 's last " A " movie .
Upon his return to America , Lugosi was interviewed for television , and revealed his ambition to play more comedy , though wistfully noting , " Now I am the boogie man . "
Lugosi memorized the script for the skit , but became confused on the air when Berle began to ad lib .
This was depicted in the Tim Burton film Ed Wood , with Martin Landau as Lugosi .
Though Burton did not actually identify the comedian in the biopic , the events depicted were correct .
Late in his life , Bela Lugosi again received star billing in movies when filmmaker Ed Wood , a fan of Lugosi , found him living in obscurity and near-poverty and offered him roles in his films , such as Glen or Glenda and as a Dr. Frankenstein-like mad scientist in Bride of the Monster .
During post-production of the latter , Lugosi decided to seek treatment for his drug addiction , and the premiere of the film was said to be intended to help pay for his hospital expenses .
According to Kitty Kelley 's biography of Frank Sinatra , when the entertainer heard of Lugosi 's problems , he helped with expenses and visited at the hospital .
Lugosi would recall his amazement , since he did n't even know Sinatra .
The extras on an early DVD release of Plan 9 from Outer Space include an impromptu interview with Lugosi upon his exit from the treatment center in 1955 , which provide some rare personal insights into the man .
This footage ended up in Plan 9 from Outer Space .
Lugosi died of a heart attack on August 16 , 1956 while lying on a couch in his Los Angeles home .
Lugosi was buried wearing one of the Dracula stage play costumes , per the request of his son and fourth wife , in the Holy Cross Cemetery in Culver City , California .
One of Lugosi 's roles was released posthumously .
Wood had taken a few minutes of silent footage of Lugosi , in his Dracula cape , for a planned vampire picture but was unable to find financing for the project .
The court ruled that under Calif. law any rights of publicity , including the right to his image , terminated with Lugosi 's death .
In Tim Burton 's 1994 Ed Wood , Lugosi is played by Martin Landau in an interpretation for which Landau received an Academy Award for Best Supporting Actor .
The film fabricated much about the Hungarian actor .
Three Lugosi projects were featured on the television show Mystery Science Theater 3000 .
A statue of Lugosi can be seen today on one of the corners of the Vajdahunyad Castle in Budapest .
The cape Lugosi wore in the 1931 film Dracula still survives today in the ownership of Universal Studios .
The theatrical play Lugosi -- a vámpír árnyéka is based on Lugosi 's life , telling the story of his life as he becomes typecast as Dracula and as his drug addiction worsens .
He was played by one of Hungary 's most renowned actors , Ivan Darvas .
