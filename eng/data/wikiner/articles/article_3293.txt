Doraemon ( ドラえもん ) is a Japanese manga series created by Fujiko F. Fujio ( the pen name of Hiroshi Fujimoto ) and Fujiko A. Fujio ( the pen name of Motō Abiko ) which later became an anime series and Asian franchise .
Doraemon was awarded the Japan Cartoonists Association Award for excellence in 1973 .
Doraemon was awarded the first Shogakukan Manga Award for children 's manga in 1982 , and the first Osamu Tezuka Culture Award in 1997 .
The name " Doraemon " translates roughly to " stray " .
" Dora " is from " dora neko " ( stray cat , どら猫 ) , and is a corruption of nora ( stray ) .
" Dora " is not from dora meaning gong , but due to the homophony , the series puns on this , with Doraemon loving dorayaki .
Doraemon has a pocket from which he produces many gadgets , medicines , and tools from the future .
Thousands of dōgu have been featured in Doraemon .
It is this constant variety which makes Doraemon popular even among adult readers/viewers .
In the series , the availability of dogu depends sometimes on the money Doraemon has available , and he often says some dogu are expensive in the future .
Although he can hear perfectly well , Doraemon has no ears : his robotic ears were eaten by a mouse , giving him a series-long phobia of the creatures .
In December 1969 , the Doraemon manga appeared simultaneously in six different children 's monthly magazines .
In 1977, CoroCoro Comic was launched as a magazine of Doraemon .
Original manga based on the Doraemon movies were also released in CoroCoro Comic .
Since the debut of Doraemon in 1969 , the stories have been selectively collected into forty-five books published from 1974 to 1996 , which had a circulation of over 80 million in 1992 .
In addition , Doraemon has appeared in a variety of manga series by Shōgakukan .
Doraemon was discontinued in two media because readers were advancing in grades and an ending was believed to be needed .
When the Fujiko Fujio duo broke up in 1987 , the very idea of an official ending to the series was never discussed .
He successfully resurrected Doraemon in the future as a robotics professor , became successful as an AI developer , and thus lived happily ever after , thus relieving his progeny of the financial burdens that caused Doraemon to be sent to his space-time in the first place .
He happily runs home and asked his mother whether Doraemon came back and finds out the truth .
Since the potion was still in effect , when he arrives his room he finds Doraemon there , and they have a happy reunion , but due to the effects of the potion , all his greets and joyful words have to be spoken in the opposite way like I am so unhappy that we can never be together again. .
Instead of hearing Doraemon 's voice explaining the use of the potion , he finds a card inside the box describing the use of the potion ) .
In 1980 , Toho released the first of a series of annual feature length animated films based on the lengthly special volumes published annually .
The films are more action-adventure oriented and unlike the anime and manga , some based on the stories in the volumes , they have more of a shōnen demographic , taking the familiar characters of Doraemon and placing them in a variety of exotic and perilous settings .
Some of the films are based on legends such as Atlantis , and on literary works such as Journey to the West and Arabian Nights .
The musical debuted at Tokyo Metropolitan Art Space on September 4 , 2008 running through September 14 .
Wasabi Mizuta voiced Doraemon .
Doraemon can also be seen in Namco 's popular Taiko no Tatsujin rhythm game series like Taiko no Tatsujin , Meccha ! Taiko no Tatsujin DS : 7tsu no Shima no Daibouken , Taiko no Tatsujin Wii , Taiko no Tatsujin Plus , and Taiko no Tatsujin DS : Dororon ! Yokai Daikessen ! ! .
In Murakami 's analysis , he states that Doraemon 's formulaic plotlines typified the " wish fulfilment " mentality of 1970s Japan , where the electronics revolution glamorized the idea that one could solve their problems with machines , gadgets , and intelligence rather than hard work .
Doraemon has been considered to be a prototype of the modern slapstick cartoon series for children such as the above-mentioned Fairly OddParents , SpongeBob SquarePants ( another US-made show made by Nickelodeon ) , and Fujiko Fujio 's own Kiteretsu Daihyakka .
Doraemon is a term of common knowledge in Japan .
Newspapers also regularly make references to Doraemon and his pocket as a something with the ability to satisfy all wishes .
Doraemon was awarded the first Shogakukan Manga Award for children 's manga in 1982 , and the first Osamu Tezuka Culture Award in 1997 .
Doraemon is a cultural phenomenon in Japan and can be seen in many places .
Doraemon toys and novelties are also often found in Japan , with literally thousands of items for sale .
Doraemon is also mentioned in several anime and manga by other mangakas .
