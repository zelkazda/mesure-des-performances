By the 1850s , bunyip had also become a " synonym for imposter , pretender , humbug and the like " in the broader Australian community .
In the early 1990s it was famously used by Prime Minister Paul Keating to describe members of the conservative Liberal Party of Australia opposition .
The word bunyip can be still found in a number of Australian contexts including placenames such as the Bunyip River and the town of Bunyip , Victoria .
He provided examples of seals found as far inland as Overland Corner , Loxton and Conargo and reminded readers " the smooth fur , prominent ' apricot ' eyes and the bellowing cry are characteristic of the seal . "
Sydney 's Reverend John Dunmore Lang announced the find as " convincing proof of the deluge . "
Shortly after this account appeared , it was repeated in other Australian newspapers .
In January 1846 , a peculiar skull was taken from the banks of Murrumbidgee River near Balranald , New South Wales .
At the same time however , the so-called bunyip skull was put on display in the Australian Museum for two days .
Visitors flocked to see it and The Sydney Morning Herald said that it prompted many people to speak out about their ' bunyip sightings . '
Another early written account is attributed to escaped convict William Buckley in his 1852 biography of 30 years living with the Wathaurong people .
Buckley 's account suggests he saw such a creature on several occasions .
A bunyip had an important role in the 1930s classic novel Mountain of the Moon ( Chander Pahar in Bengali version ) , written by Bengali author Bibhutibhushan Banerjee .
The bunyip is also a monster in AdventureQuest .
Here , the bunyips were depicted as small , furry , mischievous cryptids that resemble the Tasmanian Devil of Looney Tunes with small antlers .
