IEEE 802.15.2-2003 addresses the issue of coexistence of wireless personal area networks ( WPAN ) with other wireless devices operating in unlicensed frequency bands such as wireless local area networks .
IEEE 802.15.3a was an attempt to provide a higher speed UWB PHY enhancement amendment to IEEE 802.15.3 for applications which involve imaging and multimedia .
On May 5 , 2006 , the IEEE 802.15.3b-2005 amendment was released .
IEEE 802.15.4-2003 deals with low data rate but very long battery life ( months or even years ) and very low complexity .
The standard defines both the physical and data-link ( Layer 2 ) layers of the OSI model .
The IEEE 802.15 task group 4b was chartered to create a project for specific enhancements and clarifications to the IEEE 802.15.4-2003 standard , such as resolving ambiguities , reducing unnecessary complexity , increasing flexibility in security key usage , considerations for newly available frequency allocations , and others .
IEEE 802.15.4b was approved in June 2006 and was published in September 2006 as IEEE 802.15.4-2006 .
