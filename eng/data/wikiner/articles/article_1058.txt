The individual honeycombs are listed with names given to them by Norman Johnson .
The space-filling truss of packed octahedra and tetrahedra was apparently first discovered by Alexander Graham Bell and independently re-discovered by Buckminster Fuller ( who called it the octet truss and patented it in the 1940s ) .
