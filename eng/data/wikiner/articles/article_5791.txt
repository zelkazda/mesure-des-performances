Henry VIII ( 28 June 1491 -- 28 January 1547 ) was King of England from 21 April 1509 until his death .
Besides his six marriages , he is more popularly known for his role in the separation of the Church of England from the Roman Catholic Church .
He changed religious ceremonies and rituals and suppressed the monasteries , while remaining a fervent believer in core Catholic theological teachings , even after his excommunication from the Roman Catholic Church .
Henry was , by all accounts , an attractive and charismatic man in his prime , educated and accomplished .
He ruled with absolute power , perhaps the last English monarch to do so .
Of the young Henry 's six siblings , only three -- Arthur , Prince of Wales ; Margaret ; and Mary -- survived infancy , the other three were Elizabeth , Edmund and Katherine .
As it was expected that the throne would pass to Prince Arthur , Henry 's older brother , Henry was prepared for a life in the church .
In 1502 , Arthur died at the age of 15 .
His death thrust all his duties upon his younger brother , Henry , who then became Prince of Wales .
For the new Prince of Wales to marry his brother 's widow , a dispensation from the Pope was normally required to overrule the impediment of affinity because , as told in the book of Leviticus , " If a brother is to marry the wife of a brother they will remain childless . "
Catherine swore that her marriage to Prince Arthur had not been consummated .
So , 14 months after her young husband 's death , Catherine found herself betrothed to his even younger brother , Henry .
Continued diplomatic manoeuvring over the fate of the proposed marriage lingered until the death of Henry VII in 1509 .
Only 17 years old , Henry married Catherine on 11 June 1509 and , on 24 June 1509 , the two were crowned at Westminster Abbey .
Two days after his coronation he arrested his father 's two most unpopular ministers , Sir Richard Empson and Edmund Dudley .
This was to become Henry 's primary tactic for dealing with those who stood in his way .
( A son , Henry , Duke of Cornwall , had been born in 1511 but lived only a few weeks . )
For example , Henry expanded the Royal Navy from 5 to 53 ships .
Wolsey disappointed the king when he failed to secure a quick divorce from Queen Catherine .
The treasury was empty after years of extravagance ; the peers and people were dissatisfied and Henry needed an entirely new approach ; Wolsey had to be replaced .
Henry then took full control of his government , although at court numerous complex factions continued to try to ruin and destroy each other .
Parliamentary consent did not mean that the king had yielded any of his authority ; Henry VIII was a paternalistic ruler who did not hesitate to use his power .
As long as Cardinal Wolsey dominated the government the widespread sentiment for reform could go nowhere .
Henry 's reformation of the English church involved more complex motives and methods than his desire for a new wife and an heir .
Henry asserted that his first marriage had never been valid , but the divorce issue was only one factor in Henry 's desire to reform the church .
In 1536 -- 37 , he instituted a number of statutes-the act of appeal , the act of succession , the act of supremacy and others-that dealt with the relationship between the king and the pope and the structure of the Church of England .
During these years , Henry also suppressed monasteries and pilgrimage shrines in his attempt to reform the church .
Even so the era saw movement away from religious orthodoxy , the more so as the pillars of the old beliefs , especially Thomas More and John Fisher , had been unable to accept the change and had been executed in 1535 for refusing to renounce papal authority .
England possessed numerous religious houses that owned large tracts of land worked by tenants .
Henry made radical changes in traditional religious practices .
The reforms , which closed down monasteries that were the only support of the impoverished , alienated most of the population outside of London and helped provoke the great northern rising of 1536 -- 1537 , known as the Pilgrimage of Grace .
It was the only real threat to Henry 's security on the throne in all his reign .
Some 30,000 rebels in nine groups were led by the charismatic Robert Aske , together with most of the northern nobility .
Aske went to London to negotiate terms ; once there he was arrested , charged with treason and executed .
Elsewhere the changes were accepted and welcomed , as those who clung to Catholic rites kept quiet or moved in secrecy .
They reemerged in the reign ( 1553 -- 58 ) of Henry 's daughter Mary .
In spite of his popular image , Henry may not have had very many affairs outside marriage and ( apart from women he later married ) the identities of only two mistresses are completely undisputed : Elizabeth Blount and Mary Boleyn ( although it is unlikely that they were the only two ) .
In 1533 , FitzRoy married Mary Howard , Anne Boleyn 's first cousin , but died three years later without any successors .
At the time of FitzRoy 's death , the king was trying to pass a law that would allow his otherwise illegitimate son to become king .
Mary Boleyn was Henry 's mistress before her sister , Anne , became his second wife .
She is thought to have been Catherine 's lady-in-waiting at some point between 1519 and 1526 .
There has been speculation that Mary 's two children , Catherine and Henry , were fathered by Henry , but this has never been proven and the King never acknowledged them , as he did Henry FitzRoy .
Eustace Chapuys wrote , " the husband of that lady went away , carried her off and placed her in a convent sixty miles from here , that no one may see her . "
Henry also seems to have had an affair with one of the Shelton sisters in 1535 .
Traditionally it has been believed that this was Margaret , but recent research has led to the claim that this was actually Mary .
Henry became impatient with what he perceived as Catherine 's inability to produce the heir he desired .
All of Catherine 's children died in infancy except their daughter Mary .
Henry wanted a male heir , to avoid rival claims to the crown like those that caused the Wars of the Roses before Henry 's father , Henry VII , became king .
The controversial reign of Matilda , England 's only female monarch to that point , may also have weighed on Henry 's mind .
In 1525 , as Henry grew more impatient , he became enamoured of a charismatic young woman in the Queen 's entourage , Anne Boleyn .
Anne at first resisted his attempts to seduce her , and refused to become his mistress as her sister Mary Boleyn had .
This refusal made Henry even more attracted , and he pursued her relentlessly .
Eventually , Anne saw her opportunity in Henry 's infatuation and determined she would only yield to his embraces as his acknowledged queen .
It soon became the King 's absorbing desire to annul his marriage to Catherine .
It is possible that the idea of annulment had suggested itself to the King much before he noticed Anne , and it was most probably motivated by his desire for a male heir .
Instead , Henry 's secretary , William Knight , was sent to Pope Clement VII to sue for the annulment .
The grounds were that the bull of Pope Julius II was obtained by false pretences , because Catherine 's brief marriage to the sickly Arthur had been consummated .
Henry also petitioned , in the event of annulment , a dispensation to marry again to any woman even in the first degree of affinity , whether the affinity was contracted by lawful or unlawful connection .
This clearly had reference to Anne .
However , as the pope was at that time imprisoned by Emperor Charles V , Knight had difficulty in getting access to him , and so only managed to obtain the conditional dispensation for a new marriage .
Henry now had no choice but to put the matter into the hands of Wolsey .
Wolsey did all he could to secure a decision in the King 's favour , going so far as to arrange an ecclesiastical court to meet in England , with a representative from the Pope .
She bows low to Henry , put herself at his mercy , states her case with irrefutable eloquence and then sweeps out of the courtroom , a woman both formidable and clearly wronged .
Charles V resisted the annulment of his aunt 's marriage , but it is not clear how far this influenced the pope .
But it is clear that Henry saw that the Pope was unlikely to give him an annulment from the Emperor 's aunt .
Wolsey bore the blame .
Convinced that he was treacherous , Anne Boleyn maintained pressure until Wolsey was dismissed from public office in 1529 .
He then began a secret plot to have Anne forced into exile and began communication with Queen Catherine and the Pope to that end .
When this was discovered , Henry ordered Wolsey 's arrest and had it not been for his death from illness in 1530 , he might have been executed for treason .
A year later , Queen Catherine was banished from court and her old rooms were given to Anne .
With Wolsey gone , Anne now had considerable power over government appointments and political matters .
When Archbishop of Canterbury William Warham died , Anne had the Boleyn family 's chaplain , Thomas Cranmer , appointed to the vacant position .
Following these acts , Thomas More resigned as Chancellor , leaving Cromwell as Henry 's chief minister .
Immediately upon returning to Dover in England , Henry and Anne went through a secret wedding service .
She soon became pregnant and there was a second wedding service , which took place in London on 25 January 1533 .
Five days later , on 28 May 1533 , Cranmer declared the marriage of Henry and Anne to be good and valid .
Catherine was formally stripped of her title as queen , and Anne was consequently crowned queen consort on 1 June 1533 .
Several more laws were passed in England .
Theological and practical reforms would follow only under Henry 's successors ( see end of section ) .
The royal couple enjoyed periods of calm and affection , but Anne refused to play the submissive role expected of her .
The vivacity and opinionated intellect that had made her so attractive as an illicit lover made her too independent for the largely ceremonial role of a royal wife , given that Henry expected absolute obedience from those who interacted with him in an official capacity at court .
For his part , Henry disliked Anne 's constant irritability and violent temper .
As early as Christmas 1534 , Henry was discussing with Cranmer and Cromwell the chances of leaving Anne without having to return to Catherine .
Opposition to Henry 's religious policies was quickly suppressed in England .
Henry VIII promised the rebels he would pardon them and thanked them for raising the issues to his attention , then invited the rebel leader , Robert Aske to a royal banquet .
At the banquet , Henry asked Aske to write down what had happened so he could have a better idea of the problems he would " change . "
Aske did what the King asked , although what he had written was later used against him as a confession .
The King 's word could not be questioned so Aske told the rebels they had been successful and they could disperse and go home .
However , because Henry saw the rebels as traitors , he did not feel obliged to keep his promises .
The leaders , including Aske , were arrested and executed for treason .
Henry called for public displays of joy regarding Catherine 's death .
Her life could be in danger , as with both wives dead , Henry would be free to remarry and no one could claim that the union was illegal .
When news of this accident reached the queen she was sent into shock and miscarried a male child that was about 15 weeks old , on the day of Catherine 's funeral , 29 January 1536 .
Given the King 's desperate desire for a son , the sequence of Anne 's pregnancies has attracted much interest .
Most sources attest only to the birth of Elizabeth in September 1533 , a possible miscarriage in the summer of 1534 , and the miscarriage of a male child , of almost four months gestation , in January 1536 .
As Anne recovered from her final miscarriage , Henry declared that his marriage had been the product of witchcraft .
The King 's new mistress , Jane Seymour , was quickly moved into new quarters .
Five men , including Anne 's own brother , were arrested on charges of incest and treason , accused of having sexual relationships with the queen .
On 2 May 1536 Anne was arrested and taken to the Tower of London .
George Boleyn and the other accused men were executed on 17 May 1536 .
At 8 a.m. on 19 May 1536 , the queen was executed on Tower Green .
One day after Anne 's execution in 1536 Henry became engaged to Jane Seymour , one of the Queen 's ladies-in-waiting to whom the king had been showing favour for some time .
At about the same time as this , his third marriage , Henry granted his assent to the Laws in Wales Act 1535 , which legally annexed Wales , uniting England and Wales into one unified nation .
In 1537 , Jane gave birth to a son , Prince Edward , the future Edward VI .
The birth was difficult and the queen died at Hampton Court Palace on 24 October 1537 from an infection .
After Jane 's death , the entire court mourned with Henry for an extended period .
Henry considered Jane to be his " true " wife , being the only one who had given him the male heir he so desperately sought .
In 1540 , Henry sanctioned the destruction of shrines to saints .
At this time , Henry desired to marry once again to ensure the succession .
Although it has been said that he painted her in a more flattering light , it is unlikely that the portrait was highly inaccurate , since Holbein remained in favour at court .
After regarding Holbein 's portrayal , and urged by the complimentary description of Anne given by his courtiers , Henry agreed to wed Anne .
Henry wished to annul the marriage so he could marry another .
Queen Anne was intelligent enough not to impede Henry 's quest for an annulment .
Henry was said to have come into the room each night and merely kissed his new bride on the forehead before retiring .
Cromwell , meanwhile , fell out of favour for his role in arranging the marriage and was subsequently attainted and beheaded .
On 28 July 1540 , ( the same day Cromwell was executed ) Henry married the young Catherine Howard , Anne Boleyn 's first cousin and a lady-in-waiting of Anne 's .
Soon after her marriage , however , Queen Catherine had an affair with the courtier Thomas Culpeper .
She also employed Francis Dereham , who was previously informally engaged to her and had an affair with her prior to her marriage , as her secretary .
Thomas Cranmer , who was opposed to the powerful Roman Catholic Howard family , brought evidence of Queen Catherine 's activities to the king 's notice .
Though Henry originally refused to believe the allegations , he allowed Cranmer to conduct an investigation , which resulted in Queen Catherine 's implication .
As was the case with Anne Boleyn , Catherine Howard could not technically have been guilty of adultery , as the marriage was officially null and void from the beginning .
Again , this point was ignored , and Catherine was executed on 13 February 1542 .
Abbots and priors lost their seats in the House of Lords ; only archbishops and bishops came to comprise the ecclesiastical element of the body .
Henry married his last wife , the wealthy widow Catherine Parr , in 1543 .
She argued with Henry over religion ; she was a reformer , but Henry remained a conservative .
The same act allowed Henry to determine further succession to the throne in his will .
The cruelty and tyrannical egotism of Henry became more apparent as he advanced in years and his health began to fail .
Late in life , Henry became obese ( with a waist measurement of 54 inches/137 cm ) and had to be moved about with the help of mechanical inventions .
The jousting accident is also believed to have caused in Henry mood swings , which may have had a dramatic effect on his personality and temperament .
Concurrently , Henry also developed a binge-eating habit , consisting of a diet of mainly fatty red meats and few vegetables .
Henry 's obesity undoubtedly hastened his death at the age of 55 , which occurred on 28 January 1547 in the Palace of Whitehall , on what would have been his father 's 90th birthday .
The theory that Henry suffered from syphilis was first promoted approximately 100 years after his death [ citation needed ] , but has been disregarded by most serious historians .
Henry VIII was buried in St George 's Chapel in Windsor Castle , next to his wife Jane Seymour .
Over a hundred years later Charles I was buried in the same vault .
Within a little more than a decade after his death , all three of his royal heirs sat on the English throne , and all three left no descendants .
Since Edward was only nine years old at the time , he could not exercise actual power .
Henry 's will designated 16 executors to serve on a council of regency until Edward reached the age of 18 .
If Mary 's issue also failed , the crown was to go to Henry 's daughter by Anne Boleyn , Princess Elizabeth , and her heirs .
Finally , if Elizabeth 's line also became extinct , the crown was to be inherited by the descendants of Henry VIII 's deceased younger sister , Mary .
The descendants of Henry 's sister Margaret Tudor -- the royal family of Scotland -- were therefore excluded from succession according to this act .
Henry worked hard to present an image of unchallengeable authority and irresistible power .
In addition Cardinal Wolsey died in prison .
More than pastimes , they were political devices that served multiple goals , from enhancing his athletic royal image to impressing foreign emissaries and rulers , to conveying Henry 's ability to suppress any rebellion .
Thus he arranged a jousting tournament at Greenwich in 1517 , where he wore gilded armour , gilded horse trappings , and outfits of velvet , satin and cloth of gold dripping with pearls and jewels .
Henry finally retired from the lists in 1536 after a heavy fall from his horse left him unconscious for two hours , but he continued to sponsor two lavish tournaments a year .
He then started adding weight and lost that trim athletic look that had made him so handsome ; Henry 's courtiers began dressing in heavily padded clothes to emulate -- and flatter -- their increasingly stout monarch .
Henry was an intellectual .
The first well-educated English king , he was thoroughly at home in his well-stocked library ; he personally annotated many books and wrote and published his own book .
To promote the public support for the reformation of the church , Henry had numerous pamphlets and lectures prepared .
In the polemical plays they presented , the pope and Catholic priests and monks were mocked as foreign devils , while the glorious king was hailed as a brave and heroic defender of the true faith .
Henry VIII was an avid gambler and dice player .
He was also an accomplished musician , author , and poet ; his best known piece of music is " Pastime with Good Company " .
The only surviving piece of clothing worn by Henry VIII is a cap of maintenance awarded to the Mayor of Waterford , along with a bearing sword , in 1536 .
It currently resides in the Waterford Museum of Treasures .
A suit of Henry 's armour is on display in the Tower of London .
In the centuries since his death , Henry has inspired or been mentioned in numerous artistic and cultural works .
Henry inherited a vast fortune from his father Henry VII who had , in contrast to his son , been frugal and careful with money .
Much of this wealth was spent by Henry on maintaining his court and household , including many of the building works he undertook on royal palaces .
But Henry had to debase the coinage in 1526 and 1539 in order to solve his financial problems , and despite his ministers efforts to reduce costs and waste at court , Henry died in debt .
Though mainly motivated by dynastic and personal concerns , and despite never really abandoning the fundamentals of the Roman Catholic Church , Henry ensured that the greatest act of his reign would be one of the most radical and decisive of any English monarch .
Henry 's decision to entrust the regency of his son Edward 's minor years to a decidedly reform-oriented regency council , dominated by Edward Seymour , most likely for the simple tactical reason that Seymour seemed likely to provide the strongest leadership for the kingdom , ensured that the English Reformation would be consolidated and even furthered during his son 's reign .
During the reign of Henry VIII , as many as 72,000 people are estimated to have been executed .
The power of the state was magnified , yet so too ( at least after Henry 's death ) were demands for increased political participation by the middle class .
Together with Alfred the Great and Charles II , Henry is traditionally cited as one of the founders of the Royal Navy .
His reign featured some naval warfare and , more significantly , large royal investment in shipbuilding ( including a few spectacular great ships such as Mary Rose ) , dockyards ( such as HMNB Portsmouth ) and naval innovations ( such as the use of cannon on board ship -- although archers were still deployed on medieval-style forecastles and bowcastles as the ship 's primary armament on large ships , or co-armament where cannons were used ) .
However , in some ways this is a misconception since Henry did not bequeath to his immediate successors a navy in the sense of a formalised organisation with structures , ranks , and formalised munitioning structures but only in the sense of a set of ships .
These were also known as Henry VIII 's Device Forts .
In 1536 , the phrase " of the Church of England " changed to " of the Church of England and also of Ireland " .
Arthur Tudor the older brother of Henry VIII died after 20 weeks of marriage when Henry was age 10 .
