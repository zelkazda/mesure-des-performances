Alan Curtis Kay ( born May 17 , 1940 ) is an American computer scientist , known for his early pioneering work on object-oriented programming and windowing graphical user interface design , and for coining the phrase , " The best way to predict the future is to invent it . "
He is also on the advisory board of TTI/Vanguard .
There , he worked with Ivan Sutherland , who had done pioneering graphics programs including Sketchpad .
This greatly inspired Kay 's evolving views on objects and programming .
As he grew busier with ARPA research , he quit his career as a professional musician .
In 1968 , he met Seymour Papert and learned of the Logo programming language , a dialect of Lisp optimized for educational use .
In the 1970s he was one of the key members there to develop prototypes of networked workstations using the programming language Smalltalk .
These inventions were later commercialized by Apple Computer in their Lisa and Macintosh computers .
Kay is one of the fathers of the idea of object-oriented programming , which he named , along with some colleagues at PARC and predecessors at the Norwegian Computing Center .
Because the Dynabook was conceived as an educational platform , Kay is considered to be one of the first researchers into mobile learning , and indeed , many features of the Dynabook concept have been adopted in the design of the One Laptop Per Child educational platform , with which Kay is actively involved .
After 10 years at Xerox PARC , Kay became Atari 's chief scientist for three years .
After Disney , in 2001 he founded Viewpoints Research Institute , a non-profit organization dedicated to children , learning , and advanced software development .
In December 1995 , when he was still at Apple , Kay collaborated with many others to start the open source Squeak dynamic media software , and he continues to work on it .
As part of this effort , in November 1996 , his team began research on what became the Etoys system .
Tweak added mechanisms of islands , asynchronous messaging , players and costumes , language extensions , projects , and tile scripting .
Tweak objects are created and run in Tweak project windows .
In November 2005 , at the World Summit on the Information Society , the MIT research laboratories unveiled a new laptop computer , for educational use around the world .
It has many names : the $ 100 Laptop , the One Laptop per Child program , the Children 's Machine , and the XO-1 .
The program was begun and is sustained by Kay 's friend , Nicholas Negroponte , and is based on Kay 's Dynabook ideal .
Kay is a prominent co-developer of the computer , focusing on its educational software using Squeak and Etoys .
On 31 August 2006 , Kay 's proposal to the United States National Science Foundation , NSF , was granted , thus funding Viewpoints Research Institute for several years .
Alan Kay has received many awards and honors .
Kay is an avid and skilled [ citation needed ] musician who plays keyboard instruments and guitar [ citation needed ] .
He is married to Bonnie MacBird , a writer , actress , artist and television producer who shares his passion for music [ citation needed ] .
