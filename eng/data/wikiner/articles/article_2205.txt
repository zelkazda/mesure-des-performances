Primary exponents of the cyberpunk field include William Gibson , Neal Stephenson , Bruce Sterling , Pat Cadigan , Rudy Rucker , and John Shirley .
Many influential films , such as Blade Runner and the Matrix trilogy can be seen as prominent examples of the cyberpunk style and theme .
Computer games , board games , and role-playing games , such as Cyberpunk 2020 and Shadowrun , often feature storylines that are heavily influenced by cyberpunk writing and movies .
Cyberpunk is also featured prominently in anime , Akira and Ghost in the Shell being among the most notable .
Gibson defined cyberpunk 's antipathy towards utopian SF in his 1981 short story " The Gernsback Continuum " , which pokes fun at and , to a certain extent , condemns utopian science fiction .
Protagonists in cyberpunk writing usually include computer hackers , who are often patterned on the idea of the lone hero fighting injustice , such as Robin Hood .
One of the cyberpunk genre 's prototype characters is Case , from Gibson 's Neuromancer .
Case is a " console cowboy " , a brilliant hacker who had betrayed his organized criminal partners .
Like Case , many cyberpunk protagonists are manipulated , placed in situations where they have little or no choice , and although they might see things through , they do not necessarily come out any further ahead than they previously were .
In the words of author and critic David Brin :
Cyberpunk stories have also been seen as fictional forecasts of the evolution of the Internet .
The earliest descriptions of a global communications network came long before the World Wide Web entered popular awareness , though not before traditional science-fiction writers such as Arthur C. Clarke and some social commentators such as James Burke began predicting that such networks would eventually form .
The term was quickly appropriated as a label to be applied to the works of William Gibson , Bruce Sterling , Pat Cadigan and others .
Of these , Sterling became the movement 's chief ideologue , thanks to his fanzine Cheap Truth . John Shirley wrote articles on Sterling and Rucker 's significance .
William Gibson with his novel Neuromancer ( 1984 ) is likely the most famous writer connected with the term cyberpunk .
After Gibson 's popular debut novel , Count Zero ( 1986 ) and Mona Lisa Overdrive ( 1988 ) followed .
According to the Jargon File , " Gibson 's near-total ignorance of computers and the present-day hacker culture enabled him to speculate about the role of computers and hackers in the future in ways hackers have since found both irritatingly naïve and tremendously stimulating " .
Furthermore , while Neuromancer 's narrator may have had an unusual " voice " for science fiction , much older examples can be found : Gibson 's narrative voice , for example , resembles that of an updated Raymond Chandler , as in his novel The Big Sleep ( 1939 ) .
Others noted that almost all traits claimed to be uniquely cyberpunk could in fact be found in older writers ' works -- often citing J. G. Ballard , Philip K. Dick , Harlan Ellison , Stanislaw Lem , Samuel R. Delany , and even William S. Burroughs .
For example , Philip K. Dick 's works contain recurring themes of social decay , artificial intelligence , paranoia , and blurred lines between objective and subjective realities , and the influential cyberpunk movie Blade Runner is based on one of his books .
Other important predecessors include Alfred Bester 's two most celebrated novels , The Demolished Man and The Stars My Destination , as well as Vernor Vinge 's novella True Names .
Science-fiction writer David Brin describes cyberpunk as " the finest free promotion campaign ever waged on behalf of science fiction " .
Cyberpunk made science fiction more attractive to academics , argues Brin ; in addition , it made science fiction more profitable to Hollywood and to the visual arts generally .
Although the " self-important rhetoric and whines of persecution " on the part of cyberpunk fans were irritating at worst and humorous at best , Brin declares that the " rebels did shake things up .
Cyberpunk further inspired many professional writers who were not among the " original " cyberpunks to incorporate cyberpunk ideas into their own works , such as George Alec Effinger 's When Gravity Fails .
Although Blade Runner was largely unsuccessful in its first theatrical release , it found a viewership in the home video market and became a cult film .
Since the movie omits the religious and mythical elements of Dick 's original novel , it falls more strictly within the cyberpunk genre than the novel does .
William Gibson would later reveal that upon first viewing the film , he was surprised at how the look of this film matched his vision when he was working on Neuromancer .
The film 's tone has since been the staple of many cyberpunk movies , such as The Matrix ( 1999 ) , which uses a wide variety of cyberpunk elements .
The number of films in the genre or at least using a few genre elements has grown steadily since Blade Runner .
Several of Philip K. Dick 's works have been adapted to the silver screen .
The films Johnny Mnemonic and New Rose Hotel , both based upon short stories by William Gibson , flopped commercially and critically .
It includes many cyberpunk films such as Blade Runner , The Terminator , 12 Monkeys , The Lawnmower Man , and Strange Days .
In Japan , where cosplay is popular and not only teenagers display such fashion styles , cyberpunk has been accepted and its influence is widespread .
William Gibson 's Neuromancer , whose influence dominated the early cyberpunk movement , was also set in Chiba , one of Japan 's largest industrial areas , although at the time of writing the novel Gibson did not know the location of Chiba and had no idea how perfectly it fit his vision in some ways .
Even though most anime and manga is written in Japan , the cyberpunk anime and manga have a more futuristic and therefore international feel to them so they are widely accepted by all .
William Gibson is now a frequent visitor to Japan , and he came to see that many of his visions of Japan have become a reality :
Cyberpunk has influenced many anime and manga including the ground-breaking Akira and Battle Angel Alita .
Cyberpunk 2020 was designed with the settings of William Gibson 's writings in mind , and to some extent with his approval [ citation needed ] , unlike the approach taken by FASA in producing the transgenre Shadowrun game .
In 1990 , in a convergence of cyberpunk art and reality , the U.S. Secret Service raided Steve Jackson Games 's headquarters and confiscated all their computers .
This was allegedly because the GURPS Cyberpunk sourcebook could be used to perpetrate computer crime .
Steve Jackson Games later won a lawsuit against the Secret Service , aided by the freshly minted Electronic Frontier Foundation .
All published editions of GURPS Cyberpunk have a tagline on the front cover , which reads " The book that was seized by the U.S. Secret Service ! "
Netrunner is a collectible card game introduced in 1996 , based on the Cyberpunk 2020 role-playing game .
Computer games have frequently used cyberpunk as a source of inspiration , such as the Deus Ex series .
Electronic Arts and Tilted Mill also published a game called SimCity Societies in 2007 which features the ability for a cyberpunk society along with numerous others .
Some real life places have been described as cyberpunk , such as Japan and the Sony Center in the Potsdamer Platz public square of Berlin , Germany .
Early adherents included Timothy Leary , Mark Frauenfelder and R. U. Sirius .
The term was originally coined around 1987 as a joke to describe some of the novels of Tim Powers , James P. Blaylock , and K.W. Jeter , but by the time Gibson and Sterling entered the subgenre with their collaborative novel The Difference Engine the term was being used earnestly as well .
Paul Di Filippo is seen as the most prominent biopunk writer , including his half-serious ribofunk .
Bruce Sterling 's Shaper/Mechanist cycle is also seen as a major influence .
In addition , some people consider works such as Neal Stephenson 's The Diamond Age to be postcyberpunk .
Bands whose music has been classified as cyberpunk include Psydoll , Front Line Assembly , and Sigue Sigue Sputnik .
Billy Idol 's Cyberpunk drew heavily from cyberpunk literature and the cyberdelic counter culture in its creation .
1. Outside , a cyberpunk narrative fueled concept album by David Bowie , was warmly met by critics upon its release in 1995 .
