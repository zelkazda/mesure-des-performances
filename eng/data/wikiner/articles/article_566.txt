Alberta 's economy is one of the strongest in Canada , supported by the petroleum industry and , to a lesser extent , agriculture and technology .
In 2006 , the deviation from the national average was the largest for any province in Canadian history .
Oil sands are found in the northeast , especially around Fort McMurray ( the Athabasca Oil Sands ) .
The Calgary-Edmonton Corridor is the most urbanized region in the province and one of the densest in Canada .
In 2001 , the population of the Calgary-Edmonton Corridor was 2.15 million .
A 2003 study by TD Bank Financial Group found the corridor is the only Canadian urban centre to amass a U.S level of wealth while maintaining a Canadian-style quality of life , offering universal health care benefits .
Seeing Calgary and Edmonton are part of a single economic region as the TD study did in 2003 was novel .
Calgary is home to most oil company head offices , while Edmonton and more so its surrounding region ( Nisku , Leduc , Fort Saskatchewan ) is the site of most oil-and-gas related industry including refining and manufacturing .
In 1947 a major oil field was discovered near Edmonton .
In both Red Deer and Edmonton , world class polyethylene and vinyl manufacturers produce products shipped all over the world , and Edmonton 's oil refineries provide the raw materials for a large petrochemical industry to the east of Edmonton .
The Athabasca Oil Sands have estimated unconventional oil reserves approximately equal to the conventional oil reserves of the rest of the world , estimated to be 1.6 trillion barrels ( 250 × 10 ^ 9 m 3 ) .
Fort McMurray , one of Canada 's fastest growing cities , has grown enormously in recent years because of the large corporations which have taken on the task of oil production .
The Athabasca River region produces oil for internal and external use .
The Athabasca Oil Sands contain the largest proven reserves of oil in the world outside Saudi Arabia .
Notable gas reserves were discovered in the 1883 near Medicine Hat .
The town of Medicine Hat began using gas for lighting the town , and supplying light and fuel for the people , and a number of industries using the gas for manufacturing .
When Rudyard Kipling visited Medicine Hat he described it as the city " with all hell for a basement " .
In the valley of the Bow River , alongside the Canadian Pacific Railway , valuable beds of anthracite coal are still worked .
These are largely worked at Lethbridge in southern Alberta and Edmonton in the centre of the province .
The Edmonton area , and in particular Nisku is a major centre for manufacturing oil and gas related equipment .
As well Edmonton 's Refinery Row is home to a petrochemical industry .
Several companies and services in the biotech sector are clustered around the University of Alberta , for example ColdFX .
Owing to the strength of agriculture , food processing was one a major part of the economies of Edmonton and Calgary , but this sector has increasingly moved to smaller centres such as Brooks , the home of Lakeside Packers .
Edmonton is one CN Rail 's most important hubs .
Calgary is the main hub for the WestJet airline , and an important centre for CP Rail .
WestJet ( Canada 's second largest air carrier ) is headquartered in Calgary , by Calgary International Airport .
Prior to its dissolution , Canadian Airlines was headquartered in Calgary by the airport .
Prior to its dissolution , Air Canada subsidiary Zip was headquartered in Calgary .
Over three million cattle are residents of the province at one time or another , and Albertan beef has a healthy worldwide market .
Calgary is head office for many major oil and gas related companies , and many financial service business have grown up around them .
