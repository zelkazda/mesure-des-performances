August William Derleth ( February 24 , 1909 -- July 4 , 1971 ) was an American writer and anthologist .
Derleth can also be considered a pioneering naturalist and conservationist in his writing .
Derleth wrote his first fiction at age 13 .
Derleth wrote throughout his four years at the University of Wisconsin , where he received a B.A. in 1930 .
Returning to Sauk City in the summer of 1931 , Derleth worked in a local canning factory and collaborated with childhood friend Mark Schorer .
In 1941 , he became literary editor of The Capital Times newspaper in Madison , a post he held until his resignation in 1960 .
Derleth 's true avocation , however , was hiking the terrain of his native Wis. lands , and observing and recording nature with an expert eye .
Derleth once wrote of his writing methods , " I write very swiftly , from 750,000 to a million words yearly , very little of it pulp material . "
Derleth wrote more than 150 short stories and more than 100 books during his lifetime .
This , and other early work by Derleth , made him a well known figure among the regional literary figures of his time : early Pulitzer Prize winners Hamlin Garland and Zona Gale , as well as Sinclair Lewis , the latter both an admirer and critic of Derleth .
At publication , The Detroit News wrote : " Certainly with this book Mr. Derleth may be added to the American writers of distinction .
This work Derleth considered among his finest .
What The Milwaukee Journal called " this beautiful little love story, " is an autobiographical novel of first love beset by small town religious bigotry .
The work received critical praise : The New Yorker considered it a story told " with tenderness and charm, " while the Chicago Tribune concluded : " It 's as though he turned back the pages of an old diary and told , with rekindled emotion , of the pangs of pain and the sharp , clear sweetness of a boy 's first love . "
In November 1945 , however , Derleth 's work was attacked by his one-time admirer and mentor , Sinclair Lewis .
Detective fiction represented another substantial body of Derleth 's work .
Most notable among this work was a series of 70 stories in affectionate pastiche of Sherlock Holmes , whose creator , Sir Arthur Conan Doyle , he admired greatly .
Despite close similarities to Doyle 's creation , Pons lived in the post-World War I era , in the decade of the 1920s .
Some of the stories were self-published , through a new imprint called " Mycroft & Moran " , an appellation of humorous significance to Holmesian scholars .
Derleth wrote many and varied children 's works , including biographies meant to introduce younger readers to explorer Fr. Marquette , as well as Ralph Waldo Emerson and Henry David Thoreau .
Derleth 's own writing emphasized the struggle between good and evil , in line with his own Christian world view and in contrast with Lovecraft 's depiction of an amoral universe .
When Lovecraft died in 1937 , Derleth and Donald Wandrei assembled a collection of that author 's stories and tried to get them published .
Following Lovecraft 's death , Derleth wrote a number of stories based on fragments and notes left by Lovecraft .
These were published in Weird Tales and later in book form , under the byline " H. P. Lovecraft and August Derleth " , with Derleth calling himself a " posthumous collaborator . "
This practice has raised objections in some quarters that Derleth simply used Lovecraft 's name to market what was essentially his own fiction ; S. T. Joshi refers to the " posthumous collaborations " as marking the beginning of " perhaps the most disreputable phase of Derleth 's activities " .
Important as was Derleth 's work to rescue H.P. Lovecraft from literary obscurity at the time of Lovecraft 's death , Derleth also built a body of horror and spectral fiction of his own ; still frequently anthologized .
The best of this work , recently reprinted in four volumes of short stories -- most of which were originally published in Weird Tales , illustrates Derleth 's original abilities in the genre .
While Derleth considered his work in this genre less than his most serious literary efforts , the compilers of these four anthologies , including Ramsey Campbell , note that the stories still resonate after more than fifty years .
Derleth wrote several volumes of poems , as well as biographies of Zona Gale , Ralph Waldo Emerson and Henry David Thoreau .
Derleth 's papers and comic book collection ( valued at a considerable sum upon his death ) were donated to the Wisconsin Historical Society in Madison .
