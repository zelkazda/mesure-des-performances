He was active in Alexandria during the reign of Ptolemy I ( 323 -- 283 BC ) .
His Elements is one of the most influential works in the history of mathematics , serving as the main textbook for teaching mathematics ( especially geometry ) from the time of its publication until the late 19th or early 20th century .
Euclid also wrote works on perspective , conic sections , spherical geometry , number theory and rigor .
Little is known about Euclid 's life , as there are only a handful of references to him .
Although the purported citation of Euclid by Archimedes has been judged to be an interpolation by later editors of his works , it is still believed that Euclid wrote his works before those of Archimedes .
In addition , the " royal road " anecdote is questionable since it is similar to a story told about Menaechmus and Alexander the Great .
In the only other key reference to Euclid , Pappus briefly mentioned in the fourth century that Apollonius " spent a very long time with the pupils of Euclid at Alexandria , and it was thus that he acquired such a scientific habit of thought . "
It is further believed that Euclid may have studied at Plato 's Academy in Greece .
The date and place of Euclid 's birth and the date and circumstances of his death are unknown , and only roughly estimated in proximity to contemporary figures mentioned in references .
No likeness or description of Euclid 's physical appearance made during his lifetime survived antiquity .
Therefore , Euclid 's depiction in works of art is the product of the artist 's imagination .
Although many of the results in Elements originated with earlier mathematicians , one of Euclid 's accomplishments was to present them in a single , logically coherent framework , making it easy to use and easy to reference , including a system of rigorous mathematical proofs that remains the basis of mathematics 23 centuries later .
Although best-known for its geometric results , the Elements also includes number theory .
The geometrical system described in the Elements was long known simply as geometry , and was considered to be the only geometry possible .
In addition to the Elements , at least five works of Euclid have survived to the present day .
They follow the same logical structure as Elements , with definitions and proved propositions .
Other works are credibly attributed to Euclid , but have been lost .
