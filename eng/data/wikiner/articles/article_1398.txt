In Major League Baseball , the National League Division Series ( NLDS ) determine which two teams from the National League will advance to the National League Championship Series .
The Atlanta Braves have currently played in the most NL division series with eleven appearances .
One team has yet to play in an NL division series , the Pittsburgh Pirates .
The two series winners move on to the best-of-seven NLCS .
The winner of the wild card has won the first round 7 out of the 11 years since the re-alignment and creation of the NLDS .
According to Nate Silver , the advent of this playoff series , and especially of the wild card , has caused teams to focus more on " getting to the playoffs " rather than " winning the pennant " as the primary goal of the regular season .
Historically , MLB had also used a 2-3 format in a best-of-5 series , but no longer uses that format .
Since the NLDS ' inception , six matchups have occurred more than once .
The Atlanta Braves and Houston Astros have played five division series , the Braves winning the first three series and the Astros the last two .
The San Diego Padres and St. Louis Cardinals have played three series , all of which the Cardinals won , and the Cardinals have also played the Arizona Diamondbacks twice .
The Florida Marlins have played the San Francisco Giants twice , and won both series , 3 games to 0 and 3 games to 1 .
The Chicago Cubs and the Atlanta Braves have met twice in the NLDS , with the Braves winning in 1998 and the Cubs winning in 2003 .
The Philadelphia Phillies and the Colorado Rockies have also met twice in the NLDS , with the Rockies winning in 2007 and the Phillies winning in 2009 .
