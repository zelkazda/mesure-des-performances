Handloaders also have the flexibility to make reduced-power rounds for hunting rifles , such as handloading to an equivalent of a milder-recoiling rifle ( e.g. , .243 Winchester ) , to encourage recoil-averse hunters to become proficient with a full power hunting rifle .
In the United States of America , handloading is not only legal and requires no permit , it indeed is quite popular .
Cartridges like the .56-50 Spencer , for example , are not readily obtainable in rimfire form , but can be made from shortened .50-70 cartridges or even purchased in loaded form from specialty dealers .
Cast lead bullets may also be fired in full power magnum handgun rounds like the .44 Magnum with the addition of a gas check , which is a thin zinc or copper washer or cup that is crimped over a tiny heel on the base of appropriate cast bullets .
This is especially true for reloads intended for military rifles with intentionally large chambers such as the Lee-Enfield in .303 British .
