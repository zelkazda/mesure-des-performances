Costa Rica ( pronounced /ˌkoʊstə ˈrikə/ ( listen ) ) , officially the Republic of Costa Rica is a country in Central America , bordered by Nicaragua to the north , Panama to the east and south , the Pacific Ocean to the west and south and the Caribbean Sea to the east .
The country is ranked 3rd in the world , and 1st among the Americas , in terms of the 2010 Environmental Performance Index .
In 2007 the Costa Rican government announced plans for Costa Rica to become the first carbon neutral country by 2021 .
The northwest of the country , the Nicoya Peninsula , was the southernmost point of the Nahuatl cultural influence when the Spanish conquerors ( conquistadores ) arrived in the sixteenth century .
The central and southern portions of the country had Chibcha influences .
During Spanish Colonial times , the largest city in Central America was Guatemala City .
For all these reasons Costa Rica was by and large unappreciated and overlooked by the Crown and left to develop on its own .
It is believed that the circumstances during this period led to the formation of many of the idiosyncrasies for which Costa Rica has become known , whereas concomitantly setting the stage for Costa Rica 's development as a more egalitarian society than the rest of its neighbors .
Costa Rica became a " rural democracy " with no oppressed mestizo or indigenous class .
In 1824 the capital was moved to San José , but violence briefly ensued through an intense rivalry with Cartago .
Costa Rica 's disinterest in participating as a province in a greater Central American government was one of the deciding factors in the break-up of the fledgling federation into independent states , which still exist today .
As a result , bananas came to rival coffee as the principal Costa Rican export , while foreign-owned corporations ( including the United Fruit Company ) began to hold a major role in the national economy .
Since the late nineteenth century , however , Costa Rica has experienced two significant periods of violence .
In 1917 -- 19 , Federico Tinoco Granados ruled as a dictator until he was overthrown and forced into exile .
Again in 1948 , José Figueres Ferrer led an armed uprising in the wake of a disputed presidential election .
After the coup d'état , Figueres became a national hero , winning the country 's first democratic election under the new constitution in 1953 .
Since then , Costa Rica has held 13 presidential elections , the latest being in 2010 .
Costa Rica also borders Nicaragua to the north ( 309 km or 192 mi of border ) and Panama to the south-southeast ( 639 km or 397 mi of border ) .
In total , Costa Rica comprises 51100 square kilometres ( 19700 sq mi ) plus 589 square kilometres ( 227 sq mi ) of territorial waters .
The highest point in the country is Cerro Chirripó , at 3819 metres ( 12530 ft ) , and is the fifth highest peak in Central America .
The highest volcano in the country is the Irazú Volcano ( 3431 m or 11257 ft ) .
The largest lake in Costa Rica is Lake Arenal .
Costa Rica also comprises several islands .
Cocos Island ( 24 square kilometres / 9.3 square miles ) stands out because of its distance from continental landmass , 300 mi ( 480 km ) from Puntarenas , but Calero Island is the largest island of the country ( 151.6 square kilometres / 58.5 square miles ) .
Costa Rica is a democratic republic with a strong constitution .
The president , vice presidents , and 57 Legislative Assembly delegates are elected for four-year terms .
In April 2003 , the constitutional amendment ban on presidential re-election was reversed , allowing Óscar Arias ( Nobel Peace Prize laureate , 1987 ) to run for president for a second term .
In 2007 , Óscar Arias was elected in a tight and highly contested election , running on a platform promoting free trade .
He was succeeded by Laura Chinchilla who won the election of February 7 , 2010 , and took office on May 8 , 2010 .
Certain other state agencies enjoy considerable operational independence and autonomy ; they include the electrical power , the nationalized commercial banks ( which are open to competition from private banks ) , and the social security agency , all of which have played an important role in the development of the Costa Rican high-indexed quality of life .
Costa Rica is composed of seven provinces , which in turn are divided into 81 cantons , each of which is directed by a mayor .
Several global high tech corporations have already started developing in the area exporting goods including chip manufacturer Intel , pharmaceutical company GlaxoSmithKline , and consumer products company Procter & Gamble .
In 2006 Intel 's microprocessor facility alone was responsible for 20 % of Costa Rican exports and 4.9 % of the country 's GDP .
Trade with South East Asia and Russia boomed during 2004 and 2005 , and the country obtained full Asia-Pacific Economic Cooperation Forum ( APEC ) membership in 2007 after becoming an observer in 2004 .
In recent times pharmaceuticals , financial outsourcing , software development , and ecotourism have become the prime industries in Costa Rica 's economy .
Coffee production has played a key role in Costa Rica 's history and economy and by 2006 was the third cash crop export .
In a countrywide referendum on October 7 , 2007 , the voters of Costa Rica narrowly backed a free trade agreement , with 51.6 % of " Yes " votes .
In 2008 most visitors came from the United States ( 38.6 % ) , neighboring Nicaragua ( 21.8 % ) , Europe ( 11.3 % ) and Canada ( 5.2 % ) .
Costa Rica was a pioneer in this type of tourism , and the country is recognized as one of the few with real ecotourism .
Just considering the sub-index natural resources , Costa Rica ranks 6th worldwide in terms of the natural resources pillar , but 89th in terms of its cultural resources .
Costa Rica is an active member of the United Nations and the Organization of American States .
The Inter-American Court of Human Rights and the United Nations University of Peace are based in Costa Rica .
Costa Rica is also a member of many other international organizations related to human rights and democracy .
A main foreign policy objective of Costa Rica is to foster human rights and sustainable development as a way to secure stability and growth .
Costa Rica also has a long-term disagreement with Nicaragua over the San Juan River which defines the border between the two countries ; the disagreement arises because the river , on Nicaraguan soil , is the only way to reach several communities in Costa Rica served by the Costa Rican police .
Nicaragua can also impose timetables on Costa Rican traffic .
Nicaragua may require Costa Rican boats to display the flag of Nicaragua but may not charge them for departure clearance from its ports .
On June 1 , 2007 , Costa Rica broke diplomatic ties with the Republic of China in Taiwan , switching recognition to the People 's Republic of China .
President Óscar Arias Sánchez admitted the action was a response to economic exigency .
Costa Rica is home to a rich variety of plants and animals .
One national park that is internationally renowned among ecologists for its biodiversity ( including big cats and tapirs ) and where visitors can expect to see an abundance of wildlife is the Corcovado National Park .
The Monteverde Cloud Forest Reserve is home to about 2,000 plant species , including numerous orchids .
As a whole , around 800 species of birds have been identified in Costa Rica .
Costa Rica is a center of biological diversity for reptiles and amphibians , including the world 's fastest running lizard , the spiny-tailed iguana .
As of 2010 , Costa Rica has an estimated population of 4,640,000 .
Costa Rica hosts many refugees , mainly from Colombia and Nicaragua .
As a result of that and illegal immigration , an estimated 10-15 % ( 400,000 -- 600,000 ) of the Costa Rican population is made up of Nicaraguans .
Some Nicaraguans migrate for seasonal work opportunities and then return to their country .
The central and southern portions of the country had Chibcha influences .
This is how Costa Rican cuisine today is very varied , with every new ethnic group who had recently became part of the country 's population influencing the country 's cuisine .
There are only a few schools in Costa Rica that go beyond the 12th grade .
