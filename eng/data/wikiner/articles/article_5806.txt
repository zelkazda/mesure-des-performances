Radio stations like BBC , Radio France Internationale , China Radio International , Voice of Russia , Voice of America , Deutsche Welle , and IRIB broadcast in Hausa .
It is taught at universities in Africa and around the world .
The Kano dialect is the ' standard ' variety of Hausa .
The Ghanaian Hausa dialect , forms a separate group , as it is falls outside of the contiguous Hausa-dominant area , and is usually identified by the use of c for ky , and j for gy .
Non-native Hausa is a term which defines the Hausa language as spoken by non-native speakers ( especially as Hausa language is used as a lingua franca in West Africa ) .
Non-native pronunciation vastly differs from native pronunciation by way of key omissions of implosive and ejective consonants present in native Hausa dialects , such as ɗ , ɓ and kʼ/ƙ , which are pronounced by non-native speakers as d , b and k respectively .
This presents confusion among non-native and native Hausa speakers , as there exists a lack of difference between the pronunciation of words like daidai ( correct ) and ɗaiɗai ( one-by-one ) in non-native Hausa .
Another difference between native and non-native Hausa is the omission of vowel length in words and change in the standard tone of native Hausa dialects .
Use of masculine and feminine gender nouns and sentence structure are usually omitted or interchanged , and many native Hausa nouns and verbs are substituted for non-native terms from local languages .
Non-native speakers of Hausa number around 15 million , and in some areas live in close proximity to native Hausa .
Barikanchi is a pidgin formerly used in the military of Nigeria .
Hausa has between 23 and 25 consonant phonemes depending on the speaker .
Hausa has glottalic consonants ( implosives and ejectives ) at four or five places of articulation ( depending on the dialect ) .
ch ' , an ejective [ tʃʼ ] ( does not occur in Kano dialect )
Hausa is a tone language .
In standard written Hausa , tone is not marked .
The letter ƴ is used only in Niger ; in Nigeria it is written ʼy .
At least three other writing systems for Hausa have been proposed or " discovered . "
