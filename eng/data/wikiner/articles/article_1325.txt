It is one of the Turkish Straits , along with the Dardanelles .
The world 's narrowest strait used for international navigation , it connects the Black Sea with the Sea of Marmara ( which is connected by the Dardanelles to the Aegean Sea , and thereby to the Mediterranean Sea ) .
The shores of the strait are heavily populated as the city of Istanbul ( with a metropolitan area in excess of 11 million inhabitants ) straddles it .
The exact cause for the formation of the Bosphorus remains the subject of vigorous debate among geologists .
Thousands of years ago , the Black Sea became disconnected from the Aegean Sea .
On the other hand , there is also evidence for a flood of water going in the opposite direction , from the Black Sea into the Sea of Marmara [ citation needed ] around 7000 or 8000 BC .
As the only passage between the Black Sea and the Mediterranean , the Bosporus has always been of great commercial and strategic importance .
The strategic importance of the Bosporus remains high , and control over it has been an objective of a number of hostilities in modern history , notably the Russo -- Turkish War , 1877 -- 1878 , as well as of the attack of the Allied Powers on the Dardanelles during the 1915 battle of Gallipoli in the course of World War I .
Following World War I , the 1920 Treaty of Sèvres demilitarized the strait and made it an international territory under the control of the League of Nations .
This was amended under the 1923 Treaty of Lausanne , which restored the straits to Turkish territory -- but allowed all foreign warships and commercial shipping to traverse the straits freely .
The reversion to this old regime was formalized under the Montreux Convention Regarding the Regime of the Turkish Straits of July 1936 .
In more recent years , the Turkish Straits have become particularly important for the oil industry .
The first , the Bosphorus Bridge , is 1074 m ( 3524 ft ) long and was completed in 1973 .
The second , Fatih Sultan Mehmet ( Bosphorus II ) Bridge , is 1090 m ( 3576 ft ) long , and was completed in 1988 about 5 km ( 3 mi ) north of the first bridge .
It forms part of the Trans-European Motorway .
Another crossing , Marmaray , is a 13.7 km ( 8.5 mi ) long undersea railway tunnel currently under construction and is expected to be completed in 2012 .
There are also tourist rides available in various places along the coasts of the Bosphorus .
