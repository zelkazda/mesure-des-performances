The railway links Agordat and Asmara with the port of Massawa ; however , it was nonoperational since 1978 except for about a 5 kilometre stretch that was reopened in Massawa in 1994 .
By 2003 the line had been restored from Massawa all the way through to Asmara .
There are three international airports , one in the capital , Asmara International Airport and the two others in the coastal cities , Massawa ( Massawa International Airport ) and Assab ( Assab International Airport ) .
The airport in Asmara receives all international flights into the country as of March 2007 , as well as being the main airport for domestic flights .
The Asmara-Massawa Cableway , built by Italy in the 1930s , connected the port of Massawa with the city of Asmara .
The British later dismantled it during their eleven year occupation after defeating Italy in World War II .
