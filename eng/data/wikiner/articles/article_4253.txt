The name was an homage to Richard Buckminster Fuller , whose geodesic domes it resembles .
Buckminsterfullerene was named after Richard Buckminster Fuller , a noted architectural modeler who popularized the geodesic dome .
Buckminsterfullerene ( IUPAC name [ 5,6 ] fullerene ) is the smallest fullerene molecule in which no two pentagons share an edge ( which can be destabilizing , as in pentalene ) .
Trimetasphere carbon nanomaterials were discovered by researchers at Virginia Tech and licensed exclusively to Luna Innovations .
One proposed use of carbon nanotubes is in paper batteries , developed in 2007 by researchers at Rensselaer Polytechnic Institute .
Popular Science has published articles about the possible uses of fullerenes in armor .
In 1999 , researchers from the University of Vienna demonstrated that wave-particle duality applied to molecules such as fullerene .
One of the co-authors of this research , Julian Voss-Andreae , has since created several sculptures symbolizing wave-particle duality in fullerenes .
Sir Harry Kroto , who shared the 1996 Nobel Prize in Chemistry for the discovery of " buckyballs " commented : " This most exciting breakthrough provides convincing evidence that the buckyball has , as I long suspected , existed since time immemorial in the dark recesses of our galaxy . "
