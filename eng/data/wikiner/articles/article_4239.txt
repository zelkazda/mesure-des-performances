First National was an association of independent theater owners in the United States that expanded from exhibiting movies to distributing them , and eventually to producing them as a movie studio .
First National was the brainchild of Thomas L. Tally , who was reacting to the overwhelming influence of Paramount Pictures , which dominated the market .
Between 1917 and 1918 , they made contracts with Mary Pickford and Charlie Chaplin , the first million-dollar deals in the history of film .
Adolph Zukor of Paramount Pictures was threatened by First National 's financial power and its control over the lucrative first run theaters and decided to enter the cinema business as well .
With a $ 10 million dollar investment , Paramount built their own chain of first-run movie theaters after a secret plan to merge with First National failed .
Ironically , this led to the foundation of United Artists by Douglas Fairbanks , D. W. Griffith , Pickford , and Chaplin , and to the loss of First National 's biggest stars .
In the early twenties , Paramount attempted a hostile takeover , buying several of First National 's member firms .
With the success of The Jazz Singer and The Singing Fool , Warner Bros. purchased a majority interest in First National in September 1928 .
Warner Bros. acquired access to the First National 's affiliated chain of theaters , while First National acquired access to Vitaphone sound equipment .
But the trademarks were kept separate , and films by First National continued to be credited solely to " First National Pictures " until 1936 .
The resurrected First National Pictures name will be used to brand no-frills digital releases of children 's , documentary , and special interest titles .
