He was a career soldier who spent much of his military career in India , where he developed a love of the country and a lasting affinity for the soldiers he commanded .
He passed his retirement in Marrakesh , dying there at the age of 96 .
His father died in 1892 , when he was eight years old , and Auchinleck grew up in impoverished circumstances , but he was able , through hard work and scholarships , to graduate from Wellington College and the Royal Military Academy , Sandhurst .
He was able to learn Punjabi rapidly and , able to speak fluently with his soldiers , he absorbed a knowledge of local dialects and customs .
Auchinleck took a number of practical lessons from his experiences in Mesopotamia .
Between the wars , Auchinleck served in India .
He was both a student and an instructor ( 1930 -- 1933 ) at the Staff College at Quetta and also attended the Imperial Defence College .
After this he was appointed to command the Meerut District in India in July 1938 .
Auchinleck , however , acted decisively , sending a battalion of the King 's Own Royal Regiment by air to Habbaniya and shipping Indian 10th Infantry Division by sea to Basra .
Auchinleck relieved Cunningham , and ordered the battle to continue .
Auchinleck then appointed Ritchie to command Eighth Army .
Once more , Auchinleck 's appreciation of the situation was faulty .
The Eighth Army retreated into Egypt ; Tobruk fell on 21 June .
Once more Auchinleck stepped in to take direct command of the Eighth Army , having lost confidence in Ritchie 's ability to control and direct his forces .
Auchinleck discarded Ritchie 's plan to stand at Mersa Matruh , deciding to fight only a delaying action there , while withdrawing to the more easily defendable position at El Alamein .
His controversial chief of operations , Major-General Dorman-Smith , was regarded with considerable distrust by many of the senior commanders in Eighth Army .
Like his foe Rommel ( and his predecessor Wavell and successor Montgomery ) , Auchinleck was subjected to constant political interference , having to weather a barrage of hectoring telegrams and instructions from Prime Minister Churchill throughout late 1941 and the spring and summer of 1942 .
He badgered Auchinleck immediately after the Eighth Army had all but exhausted itself after the first battle of El Alamein .
The post was accepted in his stead by General Sir Henry Maitland Wilson .
Auchinleck continued in the post after the end of the war , being promoted field marshal in June 1946 .
A memorial plaque was erected in the crypt of St Paul 's Cathedral .
The tour guides relate how in 1979 , as plaques for the other great Second World War military leaders were being installed , no one in the establishment had been in contact with his family for some years .
