The accordion is often used in folk music in Europe , North America and South America .
The accordion is one of several European inventions of the early 19th century that used free reeds driven by a bellows .
The piano accordion was played in Germany , then all over Europe .
Three players : Pietro Frosini , and the two brothers Count Guido Deiro and Pietro Deiro were major influences at this time .
During the 1950s through the 1980s the accordion received great exposure on television with performances by Myron Floren on the Lawrence Welk Show .
John Mellencamp has included the accordion in most of his music since 1987 's The Lonesome Jubilee .
In 1993 , during their MTV Unplugged performance , Nirvana 's Krist Novoselic used accordion while covering The Vaselines song Jesus Wants Me for a Sunbeam .
Perhaps the most famous accordionist in popular music is " Weird Al " Yankovic , who has used the accordion in every album he has recorded , most extensively on his debut album .
The first composer to write specifically for the chromatic accordion was Paul Hindemith .
The experimental composer Howard Skempton began his musical career as an accordionist , and has written numerous solo works for it .
The accordion is a traditional instrument in Bosnia and Herzegovina .
It is the dominant instrument used in sevdalinka , a traditional genre of folk music from Bosnia and Herzegovina .
The accordion is a traditional instrument in Brazil .
Luiz Gonzaga is known as the king of baião .
Another famous player is Dominguinhos .
The accordion is also traditional instrument in Colombia .
Vallenato has come to symbolize the folk music of Colombia .
The lead vocalist for the pirate metal band Alestorm plays a keytar and often uses it to make accordion sounds .
