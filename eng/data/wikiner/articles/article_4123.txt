He is venerated by Christians throughout the world , and especially in the Syriac Orthodox Church , as a saint .
Ephrem wrote a wide variety of hymns , poems , and sermons in verse , as well as prose biblical exegesis .
So popular were his works , that , for centuries after his death , Christian authors wrote hundreds of pseudepigraphous works in his name .
Ephrem 's works witness to an early form of Christianity in which western ideas take little part .
Ephrem was born around the year 306 in the city of Nisibis ( the modern Turkish town of Nusaybin , on the border with Syria , which had come into Roman hands only in 298 ) .
Internal evidence from Ephrem 's hymnody suggests that both his parents were part of the growing Christian community in the city , although later hagiographers wrote that his father was a pagan priest .
Numerous languages were spoken in the Nisibis of Ephrem 's day , mostly dialects of Aramaic .
The culture included pagan religions , Judaism and early Christian sects .
Jacob , the first bishop of Nisibis , was appointed in 308 , and Ephrem grew up under his leadership of the community .
Ephrem was baptized as a youth , and almost certainly became a son of the covenant , an unusual form of Syrian proto-monasticism .
Nisibis was besieged in 338 , 346 and 350 .
Ephrem celebrated what he saw as the miraculous salvation of the city in a hymn which portrayed Nisibis as being like Noah 's Ark , floating to safety on the flood .
One important physical link to Ephrem 's lifetime is the baptistery of Nisibis .
In that year Shapur attacked again .
The cities around Nisibis were destroyed one by one , and their citizens killed or deported .
Constantius II was unable to respond ; the campaign of Julian ended with his death in battle .
Ephrem , in his late fifties , applied himself to ministry in his new church , and seems to have continued his work as a teacher , perhaps in the School of Edessa .
After a ten-year residency in Edessa , in his sixties , Ephrem succumbed to the plague as he ministered to its victims .
Over four hundred hymns composed by Ephrem still exist .
The church historian Sozomen credits Ephrem with having written over three million lines .
It seems that Bardaisan and Mani composed madrāšê , and Ephrem felt that the medium was a suitable tool to use against their claims .
Ephrem used these to warn his flock of the heresies which threatened to divide the early church .
Ephrem also wrote verse homilies ( ܡܐܡܖ̈ܐ , mêmrê ) .
The third category of Ephrem 's writings is his prose work .
He also wrote refutations against Bardaisan , Mani , Marcion and others .
Some of his works are only extant in translation ( particularly in Armenian ) .
Syriac churches still use many of Ephrem 's hymns as part of the annual cycle of worship .
Ephrem 's artful meditations on the symbols of Christian faith and his stand against heresy made him a popular source of inspiration throughout the church .
This occurred to the extent that there is a huge corpus of Ephrem pseudepigraphy and legendary hagiography .
Some of these compositions are in verse , often a version of Ephrem 's heptosyllabic couplets .
Soon after Ephrem 's death , legendary accounts of his life began to circulate .
However , internal evidence from his authentic writings suggest that he was raised by Christian parents .
This legend may be anti-pagan polemic or reflect his father 's status prior to converting to Christianity .
The second legend attached to Ephrem is that he was a monk .
In Ephrem 's day , monasticism was in its infancy in Egypt .
Later hagiographers often painted a picture of Ephrem as an extreme ascetic , but the internal evidence of his authentic writings show him to have had a very active role , both within his church community and through witness to those outside of it .
In the Eastern Orthodox scheme of hagiography , Ephrem is counted as a Venerable Father ( i.e. , a sainted Monk ) .
Ephrem is popularly believed to have taken legendary journeys .
Ephrem is also supposed to have visited Saint Pishoy in the monasteries of Scetes in Egypt .
As with the legendary visit with Basil , this visit is a theological bridge between the origins of monasticism and its spread throughout the church .
This proclamation was made before critical editions of Ephrem 's authentic writings were available .
Ephrem also shows that poetry is not only a valid vehicle for theology , but is in many ways superior to philosophical discourse for the purpose of doing theology .
He also encourages a way of reading the Bible that is rooted more in faith than in critical analysis .
Ephrem displays a deep sense of the interconnectedness of all created things , which could develop his role in the church into that of a ' saint of ecology ' .
There are modern studies into Ephrem 's view of women that see him as a champion of women in the church .
Other studies have focused on the importance of ' healing ' imagery in Ephrem .
His Roman Catholic feast day of 9 June conforms to his date of death .
