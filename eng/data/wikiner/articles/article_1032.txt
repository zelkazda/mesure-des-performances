Amr Diab was born in Port Said , Egypt to a highly educated middle class family .
By 1992 , he became the first Arabic artist to start making high-tech music videos . "
He became the first Arab singer to make a video to accompany his songs .
The video for this song was the most lavish and expensive project in the Arab music production field and it set a new standard of video-making for his contemporaries .
The clip of this song was made in the Czech Republic with much success .
The album 's cover was from the promotional photo shoot of Pepsi .
The album cost a reported $ 4 million to record , and was Rotana 's biggest album of 2005 .
The music video was shot in London and was released on a late notice after the release of the album .
Rumors surfaced in late 2005 and early 2006 stated that Amr is considering terminating his contract with Rotana due to the poor promotion of his latest album , Kammel Kalamak .
The album El Leila De was originally set to be released after Ramadan 2006 .
The video clip was filmed in Santa Monica and Malibu , and also near Hollywood .
Wayah was released for sale on the internet on June 27 ; however , the album was leaked online and was downloaded illegally amid complaints of slow download speed on the official site .
August 6 , 2009 saw the release of Amr Diab 's latest video clip for his smashing hit Wayah .
The video clip was filmed mostly on Green Screen and in Amr Diab 's personal villa in Cairo , Egypt .
The biggest surprise was the appearance of Amr Diab 's children alongside his niece , whom represented the younger generation of children who are still inspired by the music of Amr Diab .
Amr Diab has taken part in an internationally known Pepsi commercial featuring four other pop musicians : Beyoncé Knowles , Pink , Jennifer Lopez , and Britney Spears .
In 2009 Pepsi decided not to renew his contract .
Over 18 international companies have produced studio albums , compilations and exclusive music for Amr Diab across the globe .
Various international musicians have collaborated with Amr Diab :
Amr Diab 's songs have appeared in several films , including :
