The former settled in the eastern coastal regions , while the latter settled primarily in the area known today as the Highveld -- the large , relatively high central plateau of southern Africa .
Tensions also escalated with the Boer settlers from the Transvaal .
A European-African advisory council was formed in 1951 , and the 1961 constitution established a consultative legislative council .
The seat of government was moved from Mafikeng in South Africa , to newly established Gaborone in 1965 .
The presidency passed to the sitting vice president , Ketumile Masire , who was elected in his own right in 1984 and re-elected in 1989 and 1994 .
The presidency passed to the sitting vice president , Festus Mogae , who was elected in his own right in 1999 and re-elected in 2004 .
In April 2008 , Vice President Lt. Gen. Seretse Khama Ian Khama ( Ian Khama ) , son of Seretse Khama the first president , succeeded to the presidency when Festus Mogae retired .
