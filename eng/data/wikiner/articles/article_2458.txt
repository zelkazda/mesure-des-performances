Carolyn Ann Mayer-Beug ( December 11,1953 -- September 11 , 2001 ) was a filmmaker and video producer from Santa Monica , California .
She was killed at the age of 48 in the crash of American Airlines Flight 11 in the September 11 , 2001 attacks .
She won an award for the Van Halen music video to their song " Right Now " , which she produced .
She was returning home from taking her daughters to college at the Rhode Island School of Design .
She hosted an annual backyard barbecue for the Santa Monica High School girls track team , which her daughters captained .
