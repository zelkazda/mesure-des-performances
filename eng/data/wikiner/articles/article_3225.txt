As Hugo Ball expressed it , " For us , art is not an end in itself ... but it is an opportunity for the true perception and criticism of the times we live in . "
In 1916 , Hugo Ball , Emmy Hennings , Tristan Tzara , Jean Arp , Marcel Janco , Richard Huelsenbeck , Sophie Täuber , Hans Richter , along with others , discussed art and put on performances in the Cabaret Voltaire expressing their disgust with the war and the interests that inspired it .
Marcel Janco recalled ,
A single issue of the magazine Cabaret Voltaire was the first publication to come out of the movement .
Grosz , together with John Heartfield , developed the technique of photomontage during this period .
Much of their activity centered in Alfred Stieglitz 's gallery , 291 , and the home of Walter and Louise Arensberg .
During this time Duchamp began exhibiting " readymades " ( found objects ) such as a bottle rack , and got involved with the Society of Independent Artists .
The committee presiding over Britain 's prestigious Turner Prize in 2004 , for example , called it " the most influential work of modern art . "
Dada in Paris surged in 1920 when many of the originators converged there .
The movement became less active as post-World War II optimism led to new movements in art and literature .
Dada is a named influence and reference of various anti-art and political and cultural movements including the Situationists and culture jamming groups like the Cacophony Society .
Tom Stoppard used this coincidence as a premise for his play Travesties ( 1974 ) , which includes Tzara , Lenin , and James Joyce as characters .
Marcel Duchamp began to view the manufactured objects of his collection as objects of art , which he called " readymades " .
Duchamp wrote : " One important characteristic was the short sentence which I occasionally inscribed on the ' readymade . '
