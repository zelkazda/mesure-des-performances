Hertfordshire ( i i i / ɑr t f ər d ʃ ɪər / or i i i / f ər d ʃ ər / ; abbreviated Herts ) is a ceremonial and non-metropolitan county in the East region of England .
The county town is Hertford .
Hertfordshire was originally the area assigned to a fortress constructed at Hertford under the rule of Edward the Elder in 913 .
The name Hertfordshire first appears in the Anglo-Saxon Chronicle in 1011 .
The Norman conquest in 1066 reached its climax at Berkhamsted where William the Conqueror accepted the final Saxon surrender .
The Domesday Book recorded the county as having nine hundreds .
In 1903 , Letchworth became the world 's first garden city and Stevenage became the first town to redevelop under the New Towns Act 1946 .
Many well-known films were made here including the first three Star Wars movies ( IV , V , & VI ) .
The studios generally used the name of Elstree ( the adjoining village ) .
In more recent times , Elstree has had the likes of Big Brother UK and Who Wants To Be A Millionaire ? filmed there , whilst EastEnders is also filmed at the studios .
On 17 October 2000 , the Hatfield rail crash killed four people with 170 injured .
The crash exposed the shortcomings of Railtrack , which consequently saw speed restrictions and major track replacement .
On 10 May 2002 , the second of the Potters Bar rail accidents occurred killing seven people ; the train was at high speed when it derailed and flipped into the air when one of the carriage 's slid along the platform where it came to rest .
In early December 2005 the 2005 Hemel Hempstead fuel depot explosions occurred at the Hertfordshire Oil Storage Terminal .
Much of the county is part of the London commuter belt .
To the east of Hertfordshire is Essex , to the west is Buckinghamshire and to the north are Bedfordshire and Cambridgeshire .
They were amended when , in 1965 under the London Government Act 1963 , East Barnet Urban District and Barnet Urban District were abolished and their area was transferred to Greater London to form part of the present-day London Borough of Barnet .
At the same time the Potters Bar Urban District of Middlesex was transferred to Hertfordshire .
The highest point in the county is 803 feet ( 245 m ) above sea level , a quarter mile ( 400 m ) from the village of Hastoe near Tring .
The rocks of Hertfordshire belong to the great shallow syncline known as the London Basin .
The beds dip in a south-easterly direction towards the syncline 's lowest point roughly under the River Thames .
One product , now largely defunct , was water-cress , based in Hemel Hempstead and Berkhamsted supported by reliable , clean chalk rivers .
Some quarrying of sand and gravel occurs in the St. Albans area .
In the past , clay has supplied local brick-making and still does in Bovingdon , just south-west of Hemel Hempstead .
Fresh water is supplied to London from Ware , using the New River built by Hugh Myddleton and opened in 1613 .
Local rivers , although small , supported developing industries such as paper production at Nash Mills .
Hertfordshire affords habitat for a variety of flora and fauna .
Hemel Hempstead is home to DSG International .
Tesco are based in Cheshunt .
Pure Digital the DAB radio maker is based in Kings Langley .
JD Wetherspoon is in Watford .
Comet and Skanska are in Rickmansworth , whilst GlaxoSmithKline has plants in Ware and Stevenage .
Now the site is a business park and new campus for the University of Hertfordshire .
This major new employment site is home to , among others , T-Mobile , Computacenter and Ocado .
A subsidiary of BAE Systems , EADS and Finmeccanica in Stevenage , MBDA , develops missiles .
In the same town EADS Astrium produces satellites .
The loss of aircraft manufacture at Hatfield is just one of a number of industrial losses as companies capitalise on land values and move to regions where land is cheaper and recruitment is easier .
Below is a list of places , large and small , to visit in Hertfordshire .
The county has always been traversed by some of the principal roads in England , originally the A1 ( Great North Road ) to Yorkshire and Scotland , A5 ( Watling Street ) to North Wales , A6 to North West England and the A41 ( Sparrows Herne turnpike ) to the Midlands and now the M1 , M11 , A1 ( M ) and the M25 .
Principal rail routes lie through Stevenage to Yorkshire and Scotland , and through Watford to the Midlands , north Wales , the North West and Glasgow .
Lesser routes serve St. Albans ( and the East Midlands ) and Royston ( to Cambridge and Norwich ) .
Commuter routes supplement the through routes and the London Underground extends to Watford .
Two international airports lie just outside the county Stansted and Luton .
At Elstree , there is a busy airfield for light aircraft .
The Grand Union Canal passes west Hertfordshire , through Watford , Hemel Hempstead and Berkhamsted .
Hertfordshire has 26 independent schools and 73 state secondary schools .
The tertiary colleges , each with multiple campuses , are Hertford Regional College , North Hertfordshire College , Oaklands College and West Herts College .
The University of Hertfordshire is a modern university based largely in Hatfield .
Jane Austen 's novel Pride and Prejudice is primarily set in Hertfordshire .
