Searle asserts that there is no essential difference between the role the computer plays in the first case and the role he plays in the latter .
Searle argues that without " understanding " ( what philosophers call " intentionality " ) , we can not describe what the machine is doing as " thinking " .
Because it does not think , it does not have a " mind " in anything like the normal sense of the word , according to Searle .
Despite the controversy ( or perhaps because of it ) , the paper has become " something of a classic in cognitive science, " according to Harnad .
Varol Akman agrees , and has described Searle 's paper as " an exemplar of philosophical clarity and purity " .
For example , in 1955 , AI founder Herbert Simon declared that " there are now in the world machines that think , that learn and create " and claimed that they had " solved the venerable mind-body problem , explaining how a system composed of matter can have the properties of mind . "
John Haugeland wrote that " AI wants only the genuine article : machines with minds , in the full and literal sense .
Each of the following , according to Harnad , is a " tenet " of computationalism :
Alan Turing writes , " all digital computers are in a sense equivalent . "
AI researchers Alan Newell and Herbert Simon called this kind of machine a physical symbol system .
Searle emphasizes the fact that this kind of symbol manipulation is syntactic ( borrowing a term from the study of grammar ) .
Searle 's argument applies specifically to computers ( that is , devices that can only manipulate symbols without knowing what they mean ) and not to machines in general .
Searle does not disagree that machines can have consciousness and understanding , because , as he writes , " we are precisely such machines " .
Searle 's holds that the brain is , in fact , a machine , but the brain gives rise to consciousness and understanding using machinery that is non-computational .
Searle writes " brains cause minds " and that " actual human mental phenomena [ are ] dependent on actual physical-chemical properties of actual human brains " , a position called " biological naturalism " ( as opposed to alternatives like dualism , behaviorism , functionalism or identity theory ) .
Searle 's original argument centered on ' understanding ' -- that is , mental states with what philosophers call ' intentionality ' -- and did not directly address other closely related ideas such as ' consciousness ' .
David Chalmers argued that " it is fairly clear that consciousness is at the root of the matter " .
Searle 's argument does not limit the intelligence with which machines can behave or act ; indeed , it does not address this issue directly .
Since the primary mission of artificial intelligence research is only to create useful systems that act intelligently , Searle 's arguments are not usually considered an issue for AI research .
Kurzweil is concerned primarily with the amount of intelligence displayed by the machine , whereas Searle 's argument sets no limit on this , as long as it is understood that it is a simulation and not the real thing .
Searle responds to this position by asking what happens if the man memorizes the rules and keeps track of everything in his head .
Then , Searle argues , the whole system is the man himself .
The response to Searle 's counter-argument is the " virtual mind " reply .
Searle responds that such a simulation is incomplete .
These replies address Searle 's concerns about intentionality , symbol grounding and syntax vs. semantics .
Hans Moravec comments : ' If we could graft a robot to a reasoning program , we would n't need a person to provide the meaning anymore : it would come from the physical world . "
Nevertheless , the person in the room is still just following the rules , and does not know what the symbols mean. Searle writes " he does n't see what comes into the robot 's eyes . "
The symbols Searle manipulates are already meaningful , they 're just not meaningful to him .
Searle says that the symbols only have a " derived " meaning , like the meaning of words in books .
The room , according to Searle , has no understanding of its own .
Searle agrees that this background exists , but he does not agree that it can be built into programs .
Hubert Dreyfus has also criticized the idea that the " background " can be represented symbolically .
To each of these suggestions , Searle 's response is the same : no matter how much knowledge is written into the program and no matter how the program is connected to the world , he is still in the room manipulating symbols according to rules .
Searle writes " syntax is insufficient for semantics . "
Searle replies that such a simulation will not have reproduced the important features of the brain -- its causal and intentional states .
Searle is adamant that " human mental phenomena [ are ] dependent on actual physical-chemical properties of actual human brains . "
Searle 's intuition , however , is never shaken .
However , by raising doubts about Searle 's intuitions they support other positions , such as the system and robot replies .
This brings the clarity of Searle 's intuition into doubt .
An especially vivid version of the speed and complexity reply is from Paul and Patricia Churchland .
Stevan Harnad is critical of speed and complexity replies when they stray beyond addressing our intuitions .
This reply points out that Searle 's argument is a version of the problem of other minds , applied to machines .
Nils Nilsson writes " If a program behaves as if it were multiplying , most of us would say that it is , in fact , multiplying .
For all I know , Searle may only be behaving as if he were thinking deeply about these matters .
Alan Turing ( writing 30 years before Searle presented his argument ) noted that people never consider the problem of other minds when dealing with each other .
Searle believes that there are " causal properties " in our neurons that give rise to the mind .
To make this point clear , Daniel Dennett suggests this version of the " other minds " reply :
Searle disagrees with this analysis and insists that we must " presuppose the reality and knowability of the mental . "
However , some critics believe that Searle 's argument relies entirely on intuitions .
Ned Block writes " Searle 's argument depends for its force on intuitions that certain entities do not think . "
These arguments , if accepted , prevent Searle from claiming that his conclusion is obvious by undermining the intuitions that his certainty requires .
Searle posits that these lead directly to this conclusion :
Searle claims that we can derive " immediately " and " trivially " that :
