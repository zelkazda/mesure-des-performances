Arthur C. Clarke formulated the following three " laws " of prediction :
A model to other writers of hard science fiction , Clarke postulates advanced technologies without resorting to flawed engineering concepts ( as Jules Verne sometimes did ) or explanations grounded in incorrect science or engineering ( a hallmark of " bad " science fiction ) , or taking cues from trends in research and engineering ( which dates some of Larry Niven 's novels ) .
Accordingly , the powers of any future superintelligence or hyper-intelligence which Clarke often described would seem astonishing .
But in novels such as The City and the Stars and the story " The Sentinel " ( upon which 2001 : A Space Odyssey was based ) Clarke goes further ; he presents us with ultra-advanced technologies developed by hyperintelligences limited only by fundamental science .
In Against the Fall of Night the human race has mysteriously regressed after a full billion years of civilization .
