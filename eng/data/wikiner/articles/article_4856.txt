As part of both U.S. Route 101 and California State Route 1 , it connects the city of San Francisco on the northern tip of the San Francisco Peninsula to Marin County .
In 1999 , it was ranked fifth on the List of America 's Favorite Architecture by the American Institute of Architects .
Before the bridge was built , the only practical short route between San Francisco and what is now Marin County was by boat across a section of San Francisco Bay .
Ferry service began as early as 1820 , with regularly scheduled service beginning in the 1840s for purposes of transporting water to San Francisco .
Once for railroad passengers and customers only , Southern Pacific 's automobile ferries became very profitable and important to the regional economy .
The trip from the San Francisco Ferry Building took 27 minutes .
Many wanted to build a bridge to connect San Francisco to Marin County .
One who responded , Joseph Strauss , was an ambitious but dreamy engineer and poet who had , for his graduate thesis , designed a 55-mile ( 89 km ) long railroad bridge across the Bering Strait .
The Department of War was concerned that the bridge would interfere with ship traffic ; the navy feared that a ship collision or sabotage to the bridge could block the entrance to one of its main harbors .
Southern Pacific Railroad , one of the most powerful business interests in Calif. , opposed the bridge as competition to its ferry fleet and filed a lawsuit against the project , leading to a mass boycott of the ferry service .
Many locals persuaded Morrow to paint the bridge in the vibrant orange color instead of the standard silver or gray , and the color has been kept ever since .
Senior engineer Charles Alton Ellis , collaborating remotely with famed bridge designer Leon Moisseiff , was the principal engineer of the project .
An official song , " There 's a Silver Moon on the Golden Gate " , was chosen to commemorate the event .
When the celebration got out of hand , the SFPD had a small riot in the uptown Polk Gulch area .
In 1957 , Michigan 's Mackinac Bridge surpassed the Golden Gate Bridge 's total length to become the world 's longest two-tower suspension bridge in total length between anchorages , but the Mackinac Bridge has a shorter suspended span ( between towers ) compared to the Golden Gate Bridge .
As the only road to exit San Francisco to the north , the bridge is part of both U.S. Route 101 and California Route 1 .
The speed limit on the Golden Gate Bridge was reduced from 55 mph ( 89 km/h ) to 45 mph ( 72 km/h ) on 1 October 1996 .
The color was selected by consulting architect Irving Morrow because it complements the natural surroundings and enhances the bridge 's visibility in fog .
On 2 September 2008 , the auto cash toll for all southbound motor vehicles was raised from $ 5 to $ 6 , and the FasTrak toll was increased from $ 4 to $ 5 .
People have been known to travel to San Francisco specifically to jump off the bridge , and may take a bus or cab to the site ; police sometimes find abandoned rental cars in the parking lot .
New barriers have eliminated suicides at other landmarks around the world , but were opposed for the Golden Gate Bridge for reasons of cost , aesthetics , and safety ( the load from a poorly designed barrier could significantly affect the bridge 's structural integrity during a strong windstorm ) .
Since its completion , the Golden Gate Bridge has been closed due to weather conditions only three times : on 1 December 1951 , because of gusts of 69 mph ( 111 km/h ) ; on 23 December 1982 , because of winds of 70 mph ( 113 km/h ) ; and on 3 December 1983 , because of wind gusts of 75 mph ( 121 km/h ) .
Modern knowledge of the effect of earthquakes on structures led to a program to retrofit the Golden Gate to better resist seismic events .
The proximity of the bridge to the San Andreas Fault places it at risk for a significant earthquake .
Once thought to have been able to withstand any magnitude of foreseeable earthquake , the bridge was actually vulnerable to complete structural failure ( i.e. , collapse ) triggered by the failure of supports on the 320-foot ( 98 m ) arch over Fort Point .
The elevated approach to the Golden Gate Bridge through the San Francisco Presidio is popularly known as Doyle Drive .
The highway carries approximately 91,000 vehicles each weekday between downtown San Francisco and suburban Marin County .
