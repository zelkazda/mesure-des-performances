In most cases , the discipline is self-governed by the entities which require the programming , and sometimes very strict environments are defined ( e.g. United States Air Force use of AdaCore and security clearance ) .
Charles Babbage adopted the use of punched cards around 1830 to control his Analytical Engine .
In the late 1880s , Herman Hollerith invented the recording of data on a medium that could then be read by a machine .
In 1896 he founded the Tabulating Machine Company ( which later became the core of IBM ) .
In 1954 , FORTRAN was invented ; it was the first high level programming language to have a functional implementation , as opposed to just a design on paper .
The program text , or source , is converted into machine instructions using a special program called a compiler , which translates the FORTRAN program into machine language .
Many other languages were developed , including some for commercial programming , such as COBOL .
Methods of measuring programming language popularity include : counting the number of job advertisements that mention the language , the number of books teaching the language that are sold ( this overestimates the importance of newer languages ) , and estimates of the number of existing lines of code written in the language ( this underestimates the number of users of business languages such as COBOL ) .
