The national flag of the United States of America consists of thirteen equal horizontal stripes of red ( top and bottom ) alternating with white , with a blue rectangle in the canton ( referred to specifically as the " union " ) bearing fifty small , white , five-pointed stars arranged in nine offset horizontal rows of six stars ( top and bottom ) alternating with rows of five stars .
When Alaska and Hawaii were being considered for statehood in the 1950s , more than 1,500 designs were spontaneously submitted to President Dwight D. Eisenhower .
At the time , credit was given by the executive department to the United States Army Institute of Heraldry for the design .
Of these proposals , one created by 17-year old Robert G. Heft in 1958 as a school project has received the most publicity .
Heft 's flag design was chosen and adopted by presidential proclamation after Alaska and before Hawaii was admitted into the union in 1959 .
The first recorded use of fringe on a flag dates from 1835 , and the Army used it officially in 1895 .
However , according to the Army Institute of Heraldry , which has official custody of the flag designs and makes any change ordered , there are no implications of symbolism in the use of fringe .
On Memorial Day it is common to place small flags by war memorials and next to the graves of U.S. war veterans .
The United States Flag Code outlines certain guidelines for the use , display , and disposal of the flag .
Team captain Martin Sheridan is famously quoted as saying " this flag dips to no earthly king " , though the true provenance of this quotation is unclear . )
Significantly , the Flag Code prohibits using the flag " for any advertising purpose " and also states that the flag " should not be embroidered , printed , or otherwise impressed on such articles as cushions , handkerchiefs , napkins , boxes , or anything intended to be discarded after temporary use " .
One of the most commonly ignored and misunderstood aspects of the Flag Code is section 8 .
Section 3 of the Flag Code defines a flag for the purposes of the code .
President Dwight D. Eisenhower issued the first proclamation on March 1 , 1954 , standardizing the dates and time periods for flying the flag at half-staff from all federal buildings , grounds , and naval vessels ; other congressional resolutions and presidential proclamations ensued .
The 48 star flag first appeared on the General Pulaski issue of 1931 however the depiction there is noticeable at best .
Though not part of the official Flag Code , according to military custom , flags should be folded into a triangular shape when not in use .
The 48-star version went unchanged for 47 years , until the 49-star version became official on July 4 , 1959 ( the first July 4 following Alaska 's admission to the union on January 3 , 1959 ) .
Flag Day is now observed on June 14 of each year .
A false tradition holds that the new flag was first hoisted in June 1777 by the Continental Army at the Middlebrook encampment .
The apocryphal story credits Betsy Ross for sewing the first flag from a pencil sketch handed to her by George Washington .
Another woman , Rebecca Young , has also been credited as having made the first flag by later generations of her family .
Rebecca Young 's daughter was Mary Pickersgill , who made the Star Spangled Banner Flag .
In 1795 , the number of stars and stripes was increased from 13 to 15 ( to reflect the entry of Vermont and Kentucky as states of the union ) .
It was the 15-star , 15-stripe flag that inspired Francis Scott Key to write " The Star-Spangled Banner " , now the national anthem .
The most recent change , from 49 stars to 50 , occurred in 1960 when the present design was chosen , after Hawaii gained statehood in August 1959 .
Before that , the admission of Alaska in January 1959 prompted the debut of a short-lived 49-star flag .
According to author and U.S. Naval officer George H. Preble :
The United States Army Institute of Heraldry has prepared designs for flags with up to 56 stars , should additional states accede , using a similar staggered star arrangement .
