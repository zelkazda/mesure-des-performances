It borders Peru to the north , Bolivia to the northeast , Argentina to the east , and the Drake Passage in the far south .
With Ecuador , it is one of two countries in South America which do not border Brazil .
The Pacific coastline of Chile is 6,435 kilometres ( 4000mi ) .
Chile also claims about 1250000 square kilometres ( 480000 sq mi ) of Antarctica , although all claims are suspended under the Antarctic Treaty .
The shape of Chile is a distinctive , multi-coloured ribbon of land 4300 kilometres ( 2700 mi ) long and on average 175 kilometres ( 109 mi ) wide .
The relatively small central area dominates in terms of population and agricultural resources , and is the cultural and political center from which Chile expanded in the late 19th century , when it incorporated its northern and southern regions .
Chile declared its independence from Spain on February 12 , 1818 .
In the War of the Pacific ( 1879 -- 83 ) , Chile defeated Peru and Bolivia and won its current northern territory .
Although relatively free of the coups and arbitrary governments that blighted South America , Chile endured a 17-year military dictatorship ( 1973 -- 1990 ) that left more than 3,000 people dead or missing .
Today , Chile is one of South America 's most stable and prosperous nations .
In May 2010 Chile became the first South American country to join the OECD .
Chile is also a founding member of both the United Nations and the Union of South American Nations .
There are various theories about the origin of the word Chile .
In 1520 , while attempting to circumnavigate the earth , Ferdinand Magellan discovered the southern passage now named after him , the Strait of Magellan .
The conquest of Chile began in earnest in 1540 and was carried out by Pedro de Valdivia , one of Francisco Pizarro 's lieutenants , who founded the city of Santiago on February 12 , 1541 .
The first general census was performed by the government of Agustín de Jáuregui between 1777 and 1778 .
A national junta in the name of Ferdinand -- heir to the deposed king -- was formed on September 18 , 1810 .
With Carrera in prison in Argentina , O'Higgins and anti-Carrera cohort José de San Martín , hero of the Argentine War of Independence , led an army that crossed the Andes into Chile and defeated the royalists .
On February 12 , 1818 , Chile was proclaimed an independent republic .
As a result of the War of the Pacific with Peru and Bolivia ( 1879 -- 83 ) , Chile expanded its territory northward by almost one-third , eliminating Bolivia 's access to the Pacific , and acquired valuable nitrate deposits , the exploitation of which led to an era of national affluence .
The Chilean Civil War in 1891 brought about a redistribution of power between the President and Congress , and Chile established a parliamentary style democracy .
By the 1920s , the emerging middle and working classes were powerful enough to elect a reformist president , Arturo Alessandri Palma , whose program was frustrated by a conservative congress .
A military coup led by General Luis Altamirano in 1924 set off a period of great political instability that lasted until 1932 .
During the period of Radical Party dominance ( 1932 -- 52 ) , the state increased its role in the economy .
The 1964 presidential election of Christian Democrat Eduardo Frei Montalva by an absolute majority initiated a period of major reform .
By 1967 , however , Frei encountered increasing opposition from leftists , who charged that his reforms were inadequate , and from conservatives , who found them excessive .
At the end of his term , Frei had not fully achieved his party 's ambitious goals .
Allende was not elected with an absolute majority , receiving fewer than 355 votes .
Despite pressure from the United States government , the Chilean Congress conducted a runoff vote between the leading candidates , Allende and former president Jorge Alessandri and keeping with tradition , chose Allende by a vote of 153 to 35 .
Frei refused to form an alliance with Alessandri to oppose Allende , on the grounds that the Christian Democrats were a workers party and could not make common cause with the right-wing .
An economic depression that began in 1972 bottomed out in 1975 , exacerbated by capital flight , plummeting private investment , and withdrawal of bank deposits in response to Allende 's socialist program .
Allende adopted measures including price freezes , wage increases , and tax reforms , to increase consumer spending and redistribute income downward .
Industrial output increased sharply and unemployment fell during the Allende administration 's first year .
The measure was passed unanimously by Congress .
In addition , American financial pressure restricted international economic credit to Chile .
The economic problems were also exacerbated by Allende 's public spending which was financed mostly by printing money and poor credit ratings given by commercial banks .
On 26 May 1973 , Chile 's Supreme Court , which was opposed to Allende 's government , unanimously denounced the Allende disruption of the legality of the nation .
As the armed forces bombarded the presidential palace of ( Palacio de La Moneda ) , Allende reportedly had committed suicide .
A military junta , led by General Augusto Pinochet Ugarte , took over control of the country .
According to the Rettig Report and Valech Commission , at least 2,115 were killed , and at least 27,265 were tortured ( including 88 children younger than 12 years old ) .
Chile moved toward a free market economy that saw an increase in domestic and foreign private investment , although the copper industry and other important mineral resources were not opened for competition .
In a plebiscite on October 5 , 1988 , General Pinochet was denied a second 8-year term as president ( 56 % against 44 % ) .
It is situated within the Pacific Ring of Fire .
The northern Atacama Desert contains great mineral wealth , primarily copper and nitrates .
This area also is the historical center from which Chile expanded in the late nineteenth century , when it integrated the northern and southern regions .
The Andes Mountains are located on the eastern border .
Chile is the longest north-south country in the world , and also claims 1250000 km 2 ( 480000 sq mi ) of Antarctica as part of its territory .
However , this latter claim is suspended under the terms of the Antarctic Treaty , of which Chile is a signatory .
Easter Island is today a province of Chile .
Also controlled but only temporally inhabited ( by some local fishermen ) are the small islands of Sala y Gómez , San Ambrosio and San Felix .
These islands are notable because they extend Chile 's claim to territorial waters out from its coast into the Pacific .
The climate of Chile comprises a wide range of weather conditions across a large geographic scale , extending across 38 degrees in latitude , making generalisations difficult .
Chile 's botanical zones conform to the topographic and climatic regions .
On the slopes of the Andes , besides the scattered tola desert brush , grasses are found .
Because of the distance between the mainland and Easter Island , Chile uses 4 different UTC offsets :
Chile has a dynamic market-oriented economy which is characterized by a high level of foreign trade .
During the early 1990s , Chile 's reputation as a role model for economic reform was strengthened when the democratic government of Patricio Aylwin -- who took over from the military in 1990 -- deepened the economic reform initiated by the military government .
Chile 's economy has since recovered and has seen growth rates of 5-7 % over the past several years .
Sound economic policies , maintained consistently since the 1980s , have contributed to steady economic growth in Chile and have more than halved poverty rates .
Chile is strongly committed to free trade and has welcomed large amounts of foreign investment .
Chile has signed free trade agreements with a whole network of countries , including an FTA with the United States that was signed in 2003 and implemented in January 2004 .
The economic international organization the OECD agreed to invite Chile to be among four countries to open discussions in becoming an official member .
High domestic savings and investment rates helped propel Chile 's economy to average growth rates of 8 % during the 1990s .
The privatized national pension system has encouraged domestic investment and contributed to an estimated total domestic savings rate of approximately 21 % of GDP .
Critics in Chile , however , argue true that poverty figures are considerably higher than those officially published .
Chile registered an inflation rate of 3.2 % in 2006 .
Much of the jump in FDI in 2006 was also the result of acquisitions and mergers , but has done little to create new employment in Chile .
As of 2006 , Chile invested only 0.6 % of its annual GDP in research and development .
Beyond its general economic and political stability , the government has also encouraged the use of Chile as an " investment platform " for multinational corporations planning to operate in the region , but this will have limited value given the developing business climate in Chile itself .
Internal Government of Chile figures show that even when factoring out inflation and the recent high price of copper , bilateral trade between the U.S. and Chile has grown over 60 % since then .
Total trade with Europe also grew in 2006 , expanding by 42 % .
Total trade with Asia also grew significantly at nearly 31 % .
Chile 's total trade with China reached U.S. $ 8.8 billion in 2006 , representing nearly 66 % of the value of its trade relationship with Asia .
Chile 's overall trade profile has traditionally been dependent upon copper exports .
Chile has made an effort to expand nontraditional exports .
After two years of negotiations , the U.S. and Chile signed an agreement in June 2003 that will lead to completely duty-free bilateral trade within 12 years .
Chile unilaterally lowered its across-the-board import tariff for all countries with which it does not have a trade agreement to 6 % in 2003 .
The price bands were ruled inconsistent with Chile 's World Trade Organization ( WTO ) obligations in 2002 , and the government has introduced legislation to modify them .
Under the terms of the U.S.-Chile FTA , the price bands will be completely phased out for U.S. imports of wheat , wheat flour , and sugar within 12 years .
Chile is the first OECD member in South America .
Chile 's private pension system , with assets worth roughly $ 70 billion at the end of 2006 , has been an important source of investment capital for the capital market .
In 2006 , the Government of Chile ran a surplus of $ 11.3 billion , equal to almost 8 % of GDP .
The Government of Chile continues to pay down its foreign debt , with public debt only 3.9 % of GDP at the end of 2006 .
Chile 's 2002 census reported a population of 15,116,435 .
About 85 % of the country 's population lives in urban areas , with 40 % living in Greater Santiago .
The largest agglomerations according to the 2002 census are Greater Santiago with 5.6 million people , Greater Concepción with 861,000 and Greater Valparaíso with 824,000 .
Studies on the ethnic structure of Chile are inconclusive and might vary significantly from one study to the next .
The Afro-Chilean population has always been tiny , reaching a high of 2,500 during the colonial period ; their current percentage of the population is less than one percent .
Chile is one of the twenty countries to have signed and ratified the only binding international law concerning indigenous peoples , Indigenous and Tribal Peoples Convention , 1989 .
It was adopted in 1989 as the International Labour Organization Convention 169 .
Chile ratified the convention in 2008 .
The largest contingent of people to arrive in Chile came from Spain and the Basque country , beginning in the sixteenth century .
Estimates of the number of people in Chile who can trace descent from Basques range from 10 % ( 1,600,000 ) to as high as 27 % ( 4,500,000 ) .
; most live in or near either Santiago or Antofagasta .
Perhaps five percent of the Chilean population has some French ancestry .
Chile has recently become a new magnet for immigrants , mostly from neighboring Argentina , Peru , and Bolivia .
According to the 2002 national census , Chile 's foreign-born foreign population has increased by 75 % since 1992 .
In the most recent census ( 2002 ) , 70 percent of the population over age 14 identified as Roman Catholic and 15.1 percent as evangelical .
In the census , the term " evangelical " referred to all non-Catholic Christian churches with the exception of the Orthodox Church , the Church of Jesus Christ of Latter-day Saints , Seventh-day Adventists , and Jehovah 's Witnesses .
The 1999 law on religion prohibits religious discrimination ; however , the Catholic Church enjoys a privileged status and occasionally receives preferential treatment .
The current Constitution of Chile was approved in a highly irregular national plebiscite in September 1980 , under the military government of Augusto Pinochet .
In September 2005 , President Ricardo Lagos signed into law several constitutional amendments passed by Congress .
This was Chile 's fifth presidential election since the end of the Pinochet era .
The Congress of Chile has a 38-seat Senate and a 120-member Chamber of Deputies .
The current Senate has a 20-18 split in favor of the opposition coalition .
The current lower house-the Chamber of Deputies-contains 58 members of the governing center-right coalition , 54 from the center-left opposition and 8 from small parties or independents .
The Congress is located in the port city of Valparaíso , about 140 kilometres ( 84 mi ) west of the capital , Santiago .
Chile 's congressional elections are governed by a binomial system that rewards the two largest representations .
Therefore , there are only two senate and two deputy seats apportioned to each electoral district , parties are forced to form wide coalitions and , historically , the two largest coalitions ( Concertación and Alianza ) split most of the seats in a district .
In the 2001 congressional elections , the conservative Independent Democratic Union surpassed the Christian Democrats for the first time to become the largest party in the lower house .
Chile 's judiciary is independent and includes a court of appeal , a system of military courts , a constitutional tribunal , and the Supreme Court of Chile .
In June 2005 , Chile completed a nation-wide overhaul of its criminal justice system .
Chile is divided into 15 regions , each headed by an intendant appointed by the president .
The only exception is the Santiago Metropolitan Region which is designated RM ( Región Metropolitana ) .
Two new regions were created in 2006 and became operative in October 2007 ; Los Ríos in the south , and Arica y Parinacota in the north .
Those ships are based in Valparaiso .
The Navy operates its own aircraft for transport and patrol ; there are no Navy fighter or bomber aircraft .
The Navy also operates four submarines based in Talcahuano .
Air assets are distributed among five air brigades headquartered in Iquique , Antofagasta , Santiago , Puerto Montt , and Punta Arenas .
Since the early decades after independence , Chile has always had an active involvement in foreign affairs .
The war dissolved the confederation while distributing power in the Pacific .
A second international war , the War of the Pacific ( 1879 -- 83 ) , further increased Chile 's regional role , while adding considerably to its territory .
German influence came from the organization and training of the army by Prussians .
On June 26 , 1945 , Chile participated as a founding member of the United Nations being among 50 countries that signed the United Nations Charter in San Francisco , California .
With the military coup of 1973 , Chile became isolated politically as a result of widespread human rights abuses .
Since its return to democracy in 1990 , Chile has been an active participant in the international political arena .
The country is an active member of the UN family of agencies and participates in UN peacekeeping activities .
It settled its territorial disputes with Argentina during the 1990s .
Chile and Bolivia severed diplomatic ties in 1978 over Bolivia 's desire to reacquire territory it lost to Chile in 1879-83 War of the Pacific .
Music in Chile ranges from folkloric music , popular music and also to classical music .
Chile 's most famous poet , however , is Pablo Neruda , who also won the Nobel Prize for Literature ( 1971 ) and is world-renowned for his extensive library of works on romance , nature , and politics .
His three highly personalized homes , located in Isla Negra , Santiago and Valparaíso are popular tourist destinations .
Chile 's most popular sport is association football .
The main football clubs are Colo-Colo , Universidad de Chile and Universidad Católica .
Colo-Colo is the country 's most successful football club , having both the most national and international championships , including the coveted Copa Libertadores South American club tournament .
Universidad Católica was the last international champion ( Interamerican Cup 1994 ) .
At the 2004 Summer Olympics the country captured gold and bronze in men 's singles and gold in men 's doubles .
Luis Ayala was twice a runner-up at the French Open and both Ríos and Fernando González reached the Australian Open men 's singles finals .
At the Olympic Games Chile boasts two gold medals ( tennis ) , seven silver medals ( athletics , Equestrian , boxing , shooting and tennis ) and four bronze medals ( tennis , boxing and football ) .
Polo is professionally practiced within Chile and in 2008 Chile achieved top prize in the World Polo Championship a tournament where the country has earned both second and third places medals in previous editions .
Basketball is a popular sport in which Chile has earned a bronze medal in the first men 's FIBA World Championship held in 1950 and winning a second bronze medal when Chile hosted the 1959 FIBA World Championship .
Chile hosted the first FIBA World Championship for Women in 1953 finishing the tournament with the silver medal .
Tourism in Chile has experienced sustained growth over the last few decades .
Throughout the central Andes there are many ski resorts of international repute , like Portillo , Valle Nevado and Termas de Chillán .
The central port city of Valparaíso , with its unique architecture , is also popular .
Arica , Iquique , Antofagasta , La Serena and Coquimbo are the main summer centres in the north , and Pucón on the shores of Lake Villarrica is the main one in the south .
Because of its proximity to Santiago , the coast of the Valparaíso Region , with its many beach resorts , receives the largest number of tourists .
