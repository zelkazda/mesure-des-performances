The team reported " serious deficiencies in Searle ' operations and practices " in general , although not necessarily in the aspartame studies .
The FDA sought to authenticate 15 of the submitted studies against the supporting data , in 1979 the Center for Food Safety and Applied Nutrition concluded that , as any problems with the aspartame studies were minor and did not affect the conclusions , the studies could be used to assess aspartame 's safety .
In 1983 , the FDA further approved aspartame for use in carbonated beverages , and for use in other beverages , baked goods , and confections in 1993 .
In 1996 , the FDA removed all restrictions from aspartame , allowing it to be used in all foods .
The U.S. patent on aspartame expired in 1992 .
In 2004 the market for aspartame , in which the company Ajinomoto , the world 's largest aspartame manufacturer , had a 40 percent share , was 14,000 metric tons a year , and consumption of the product was rising by 2 percent a year .
In June 2010 , an appeal court reversed the decision , allowing Ajinomoto to pursue a case against Asda to protect aspartame 's reputation .
Asda said that it would continue to use the term " no nasties " on its own-label products .
The artificial sweetener aspartame has been the subject of several controversies and hoaxes since its initial approval by the U.S. Food and Drug Administration ( FDA ) in 1974 .
Critics allege that conflicts of interest marred the FDA 's approval of aspartame , question the quality of the initial research supporting its safety , and postulate that numerous health risks may be associated with aspartame .
In 1987 , the U.S. Government Accountability Office concluded that the food additive approval process had been followed properly for aspartame .
Aspartame has been found to be safe for human consumption by more than ninety countries worldwide , with FDA officials describing aspartame as " one of the most thoroughly tested and studied food additives the agency has ever approved " and its safety as " clear cut " .
