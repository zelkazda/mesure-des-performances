With 112 caps he is third only to Fabio Cannavaro and Paolo Maldini in number of appearances for the Azzurri .
In 2004 Pelé named him as one of the 125 greatest living footballers .
In the same year he made his debut for Italy , playing against Bulgaria in the quarter final of the 1968 European Championships .
He followed in the footsteps of compatriot Giampiero Combi ( 1934 ) as only the second goalkeeper to captain a World Cup-winning side .
In 1990 he was sacked , despite winning the UEFA Cup .
He then joined Lazio , where he became president in 1994 .
In 2005 , he was named the coach of Fiorentina .
