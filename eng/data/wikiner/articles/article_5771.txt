The site is located towards the southern end of the Jutland Peninsula .
It developed as a trading centre at the head of a narrow , navigable inlet known as the Schlei , which connects to the Baltic Sea .
The location was favorable because there is a short portage of less than 15 km to the Treene River , which flows into the Eider with its North Sea estuary , making it a convenient place where goods and ships could be ported overland for an almost uninterrupted seaway between the Baltic and the North Sea and avoid a dangerous circumnavigation of Jutland .
The city of Schleswig was later founded on the other side of the Schlei , and gave the duchy its name .
Hedeby was abandoned after its destruction in 1066 .
The site of Hedeby is located in the Duchy of Schleswig , which was traditionally the personal territory of the kings of Denmark .
But the Kingdom of Denmark lost the area to Austria and Prussia in 1864 in the Second Schleswig War , and it is now in Germany .
Haddeby is now by far the most important archaeological site in Schleswig-Holstein .
Ancient names for the nearby town of Schleswig are :
Hedeby is first mentioned in the Frankish chronicles of Einhard ( 804 ) who was in the service of Charlemagne , but was probably founded around 770 .
In 808 the Danish king Godfred ( Lat .
Godofredus ) destroyed a competing Slav trade centre named Reric , and it is recorded in the Frankish chronicles that he moved the merchants from there to Hedeby .
The same sources record that Godfred strengthened the Danevirke , an earthen wall that stretched across the south of the Jutland peninsula .
The Danevirke joined the defensive walls of Hedeby to form an east-west barrier across the peninsula , from the marshes in the west to the Schlei inlet leading into the Baltic in the east .
On the eastern side , the town was bordered by the innermost part of the Schlei inlet and the bay of Haddebyer Noor .
Between 800 and 1000 the growing economic power of the Vikings led to its dramatic expansion as a major trading centre .
A Swedish dynasty founded by Olof the Brash is said to have ruled Hedeby during the last decades of the 9th century and the first part of the 10th century .
Life was short and crowded in Hedeby .
Ibrahim ibn Yaqub al-Tartushi , a late 10th-century traveller from al-Andalus , provides one of the most colourful and often quoted descriptions of life in Hedeby .
He set the town on fire by sending several burning ships into the harbour , the charred remains of which were found at the bottom of the Schlei during recent excavations .
After Harald 's sack of Hedeby , Slavs plundered and again destroyed the town in 1066 .
The inhabitants then abandoned Hedeby and moved across the Schlei inlet to the town of Schleswig .
The most important finds resulting from the excavations are now on display in the adjoining Haithabu Museum .
Based on the results of archaeological analyses , exact copies of some of the original Viking houses have been built .
