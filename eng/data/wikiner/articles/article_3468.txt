There exist , as yet , few comprehensive studies of the global division of labour ( an intellectual challenge for researchers ) , although the ILO and national statistical offices can provide plenty of data on request for those who wish to try .
Petty also applied the principle to his survey of Ireland .
Bernard de Mandeville discusses the matter in the second volume of The Fable of the Bees .
Unlike Plato , Smith famously argued that the difference between a street porter and a philosopher was as much a consequence of the division of labour as its cause .
Therefore , while for Plato the level of specialisation determined by the division of labour was externally determined , for Smith it was the dynamic engine of economic progress .
However , in a further chapter of the same book Smith criticises the division of labour saying it leads to a ' mental mutilation ' in workers ; they become ignorant and insular as their working lives are confined to a single repetitive task .
The contradiction has led to some debate over Smith 's opinion of the division of labour .
Smith saw the importance of matching skills with equipment -- usually in the context of an organisation .
Smith 's insight suggests that the huge increases in productivity obtainable from technology or technological progress are possible because human and physical capital are matched , usually in an organisation .
See also a short discussion of Adam Smith 's theory in the context of business processes .
This viewpoint was extended and refined by Karl Marx .
Marx wrote that " with this division of labour " , the worker is " depressed spiritually and physically to the condition of a machine " .
Marx 's most important theoretical contribution is his sharp distinction between the social division and the technical or economic division of labour .
Marx also suggests that the capitalist division of labour will evolve over time such that the maximum amount of labour is productive labour , where productive labour is defined as labour which creates surplus value .
In Marx 's imagined communist society , the division of labour is transcended , meaning that balanced human development occurs where people fully express their nature in the variety of creative work that they do .
Thoreau criticized the division of labour in Walden ( published in 1854 ) , on the basis that it removes people from a sense of connectedness with society and with the world at large , including nature .
Émile Durkheim wrote about a fractionated , unequal world by dividing it along the lines of " human solidarity " , its essential moral value is division of labour .
The OECD recently advised ( 28 June 2005 ) that :
