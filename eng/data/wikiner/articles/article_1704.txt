Hollywood stars like Ava Gardner , Rita Hayworth and Lana Turner tried similar swimwear or beachwear .
Pin ups of Hayworth and Esther Williams in the costume were widely distributed .
In 1994 , the bikini became the official uniform of women 's Olympic beach volleyball , sparking controversy , with some sports officials considering it exploitative and unpractical in colder weather , and athletes admitting that the regulation uniform is intended to be " sexy " and to draw attention .
The popularity of Dead or Alive : Xtreme Beach Volleyball , a video game for Xbox , was attributed to the scantily clad women .
In 2007 , fans voted for contestants in the WWE Diva contest after watching them playing beach volleyball in skimpy bikinis .
In the 2004 and 2008 Olympic Games , inclusion of bikini-clad athletes raised eyebrows , while a controversy broke out around bikini-clad cheerleaders performing at a beach volleyball match .
In the 2007 South Pacific Games , players were made to wear shorts and cropped sports tops instead of bikinis .
In the West Asian Games 2006 , bikini-bottoms were banned for female athletes , who were asked to wear long shorts .
It was popularized by Sacha Baron Cohen when he donned one in the film Borat .
The buzz around the film started building during the Cannes Film Festival in May 2006 , when Baron Cohen posed in character on the beach in a neon green mankini , alongside four models .
