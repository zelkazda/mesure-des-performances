Equuleus is a constellation .
Its name is Latin for ' little horse ' , i.e. a foal .
It was one of the 48 constellations listed by the 2nd century astronomer Ptolemy , and remains one of the 88 modern constellations .
It is the second smallest of the modern constellations ( after Crux ) , spanning only 72 square degrees .
The brightest star in Equuleus is α Equ ( Kitalpha ) , at magnitude 3.92m .
There are few variable stars in Equuleus .
Equuleus contains some double stars of interest .
γ Equ consists of a primary star with a magnitude around 4.7m ( slightly variable ) and a secondary star of magnitude 11.6 , separated by 2 arcseconds .
Celeris was given to Castor by Mercury .
Other myths say that Equuleus is the horse struck from Neptune 's trident , during the contest between him and Athena when deciding which would be the superior .
