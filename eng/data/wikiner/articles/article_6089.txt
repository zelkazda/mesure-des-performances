The name comes from the Latin insula for " island " .
In 1869 Paul Langerhans , a medical student in Berlin , was studying the structure of the pancreas under a microscope when he identified some previously un-noticed tissue clumps scattered throughout the bulk of the pancreas .
In 1889 , the Polish-German physician Oscar Minkowski in collaboration with Joseph von Mering removed the pancreas from a healthy dog to test its assumed role in digestion .
Several days after the dog 's pancreas was removed , Minkowski 's animal keeper noticed a swarm of flies feeding on the dog 's urine .
In 1906 George Ludwig Zuelzer was partially successful treating dogs with pancreatic extract but was unable to continue his work .
Israel Kleiner demonstrated similar effects at Rockefeller University in 1919 , but his work was interrupted by World War I and he did not return to it .
( 1919 is after World War I had already ended , this statement must be incorrect in some way )
Use of his techniques was patented in Romania , though no clinical use resulted .
Macleod was initially skeptical , but eventually agreed to let Banting use his lab space while he was on vacation for the summer .
Best won the coin toss , and took the first shift as Banting 's assistant .
Banting 's method was to tie a ligature ( string ) around the pancreatic duct , and , when examined several weeks later , the pancreatic digestive cells had died and been absorbed by the immune system , leaving thousands of islets .
Banting and Best presented their results to Macleod on his return to Toronto in the fall of 1921 , but Macleod pointed out flaws with the experimental design , and suggested the experiments be repeated with more dogs and better equipment .
He then supplied Banting and Best with a better laboratory , and began paying Banting a salary from his research grants .
Several weeks later , it was clear the second round of experiments was also a success ; and Macleod helped publish their results privately in Toronto that November .
Banting suggested that they try to use fetal calf pancreas , which had not yet developed digestive glands ; he was relieved to find that this method worked well .
In December 1921 , Macleod invited the biochemist James Collip to help with this task , and , within a month , the team felt ready for a clinical test .
On January 11 , 1922 , Leonard Thompson , a 14-year-old diabetic who lay dying at the Toronto General Hospital , was given the first injection of insulin .
Over the spring of 1922 , Best managed to improve his techniques to the point where large quantities of insulin could be extracted on demand , but the preparation remained impure .
The drug firm Eli Lilly and Company had offered assistance not long after the first publications in 1921 , and they took Lilly up on the offer in April .
In November , Lilly made a major breakthrough and were able to produce large quantities of highly refined , ' pure ' insulin .
The amino-acid structure of insulin was characterized in the 1950s and the first synthetic insulin was produced simultaneously in the labs of Panayotis Katsoyannis at the University of Pittsburgh and Helmut Zahn at RWTH Aachen University in the early 1960s .
They were awarded the Nobel Prize in Physiology or Medicine in 1923 for the discovery of insulin .
Banting , insulted that Best was not mentioned , shared his prize with Best , and Macleod immediately shared his with James Collip .
The patent for insulin was sold to the University of Toronto for one dollar .
He was awarded the 1958 Nobel Prize in Chemistry for this work .
She had been awarded a Nobel Prize in Chemistry in 1964 for the development of crystallography .
Rosalyn Sussman Yalow received the 1977 Nobel Prize in Medicine for the development of the radioimmunoassay for insulin .
