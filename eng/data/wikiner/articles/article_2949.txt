The January 2007 estimated population of the island was 281,000 , while the figure for the March 1999 census , when most of the studies -- though not the linguistic survey work referenced in this article -- were performed , was about 261,000 ( see under Corsica ) .
Only a certain percentage of the population at either time spoke Corsu with any fluency .
In 1980 about 70 % of the population " had some command of the Corsican language . "
The language appeared to be in serious decline when the French government reversed its non-supportive stand and began some strong measures to save it .
No recent statistics on Corsu are available .
UNESCO classifies the Corsican language as a potentially endangered language , as it has " a large number of children speakers " but is " without an official or prestigious status . "
The University of Corsica Pascal Paoli at Corte took a central role in the planning .
A mythology concerning the Corsican language is to some degree current [ citation needed ] among foreigners , that it was a spoken language only or was only recently written .
At that time the monasteries held considerable land on Corsica and many of the churchmen were notaries .
Between 1200 and 1425 the monastery of Gorgona , Benedictine for much of that time and in the territory of Pisa , acquired about 40 legal papers of various sorts written on Corsica .
These documents were moved to Pisa before the monastery closed its doors and were published there .
If the natives of that time were speaking Latin they must have acquired it during the late empire .
The dialect of Ajaccio has been described as in transition .
On Maddalena archipelago the local dialect was brought by fishermen and shepherds from Bonifacio during immigration in the 17th-18th centuries .
Sassarese , is spoken in Sassari and in its neighbourhood , in the north-west of Sardinia .
