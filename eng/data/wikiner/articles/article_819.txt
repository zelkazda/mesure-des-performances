Alban Maria Johannes Berg ( February 9 , 1885 -- December 24 , 1935 ) was an Austrian composer .
Berg had little formal music education before he became a student of Arnold Schoenberg in October 1904 .
With Schoenberg he studied counterpoint , music theory , and harmony .
The early sonata sketches eventually culminated in Berg 's Piano Sonata ( 1907 -- 1908 ) ; it is one of the most formidable " first " works ever written .
Berg studied with Schoenberg for six years until 1911 .
Berg admired him as a composer and mentor , and they remained close lifelong friends .
Berg may have seen the older composer as a father figure , as Berg 's father had died when he was only 15 .
Among Schoenberg 's teaching was the idea that the unity of a musical composition depends upon all its aspects being derived from a single basic idea ; this idea was later known as developing variation .
Berg passed this on to his students , one of whom , Theodor Adorno , stated : " The main principle he conveyed was that of variation : everything was supposed to develop out of something else and yet be intrinsically different " .
The Piano Sonata is an example -- the whole composition is derived from the work 's opening quartal gesture and its opening phrase .
Berg was a part of Vienna 's cultural elite during the heady fin de siècle period .
His circle included the musicians Alexander von Zemlinsky and Franz Schreker , the painter Gustav Klimt , the writer and satirist Karl Kraus , the architect Adolf Loos , and the poet Peter Altenberg .
This was a crippling blow to Berg 's self-confidence : he effectively withdrew the work , which is surely one of the most extraordinarily innovative and assured first orchestral compositions in the literature , and it was not performed in full until 1952 .
After the end of World War I , he settled again in Vienna where he taught private pupils .
He also helped Schoenberg run his Society for Private Musical Performances , which sought to create the ideal environment for the exploration and appreciation of unfamiliar new music by means of open rehearsals , repeat performances , and the exclusion of professional critics .
Three excerpts from Wozzeck were performed in 1924 , and this brought Berg his first public success .
The opera , which Berg completed in 1922 , was first performed on December 14 , 1925 , when Erich Kleiber directed the first performance in Berlin .
Today Wozzeck is seen as one of the century 's most important works .
Berg completed the orchestration of only the first two acts of his later three-act opera Lulu , before he died .
The complete opera has rapidly entered the repertoire as one of the landmarks of contemporary music and , like Wozzeck , remains a consistent audience draw .
This profoundly elegiac work , composed at unaccustomed speed and posthumously premièred , has become Berg 's best-known and beloved composition .
Like much of his mature work , it employs an idiosyncratic adaptation of Schoenberg 's twelve tone technique that enables the composer to produce passages openly evoking tonality , including quotations from historical tonal music , such as a Bach chorale and a Carinthian folk song .
