His best known operatic works include Il barbiere di Siviglia ( The Barber of Seville ) , La Cenerentola , La gazza ladra ( The Thieving Magpie ) and Guillaume Tell ( William Tell ) .
Until his retirement in 1829 , Rossini had been the most popular opera composer in history .
Rossini 's parents began his musical training early , and by the age of six he was playing the triangle in his father 's musical group .
When Austria restored the old regime in 1796 , Rossini 's father was sent to prison and his mother took him to Bologna , making a living as a leading singer at various theatres of the Romagna region .
Her husband would ultimately join her in Bologna .
During this time , Rossini was frequently left in the care of his aging grandmother , who had difficulty supervising the boy .
He remained at Bologna in the care of a pork butcher while his father played the horn in the orchestras of the theatres at which his wife sang .
These qualities made him a subject for ridicule in the eyes of the young Rossini .
Often transcribed for string orchestra , these sonatas reveal the young composer 's affinity for Haydn and Mozart , already showing signs of operatic tendencies , punctuated by frequent rhythmic changes and dominated by clear , songlike melodies .
Though it was Rossini 's first opera , written when he was thirteen or fourteen , the work was not staged until the composer was twenty years old , premiering as his sixth official opera .
In 1813 , Tancredi and L'italiana in Algeri were even bigger successes , and catapulted the 20-year-old composer to international fame .
Traces of Ferdinando Paer and Giovanni Paisiello were undeniably present in fragments of the music .
Some older composers in Naples , notably Zingarelli and Paisiello , were inclined to intrigue against the success of the youthful composer , but all hostility was rendered futile by the enthusiasm that greeted the court performance of his Elisabetta , regina d'Inghilterra , in which Isabella Colbran , who subsequently became the composer 's wife , took a leading part .
The opera was the first in which Rossini wrote out the ornaments of the airs instead of leaving them to the fancy of the singers , and also the first in which the recitativo secco was replaced by a recitative accompanied by a string quartet .
Much is made of how quickly Rossini 's opera was written , scholarship generally agreeing upon two or three weeks .
Later in life , Rossini claimed to have written the opera in only twelve days .
It was a colossal failure when it premiered as Almaviva ; Paisiello 's admirers were extremely indignant , sabotaging the production by whistling and shouting during the entire first act .
However , not long after the second performance , the opera became so successful that the fame of Paisiello 's opera was transferred to Rossini 's , to which the title The Barber of Seville passed as an inalienable heritage .
Later in 1822 , a 30-year-old Rossini succeeded in meeting Ludwig van Beethoven , who was then aged 51 , deaf , cantankerous and in failing health .
So you 're the composer of The Barber of Seville .
Between 1815 and 1823 Rossini produced 20 operas .
Of these Otello formed the climax to his reform of serious opera , and offers a suggestive contrast with the treatment of the same subject at a similar point of artistic development by the composer Giuseppe Verdi .
Conditions of stage production in 1817 are illustrated by Rossini 's acceptance of the subject of Cinderella for a libretto only on the condition that the supernatural element should be omitted .
In 1822 , four years after the production of this work , Rossini married the renowned opera singer Isabella Colbran .
Here he made friends with Chateaubriand and Dorothea Lieven .
The production of his Guillaume Tell in 1829 brought his career as a writer of opera to a close .
In 1829 he returned to Bologna .
Six movements of his Stabat Mater were written in 1832 by Rossini himself and the other six by Giovanni Tadolini , a good musician who was asked by Rossini to complete the work .
However , Rossini composed the rest of the score in 1841 .
Political disturbances compelled Rossini to leave Bologna in 1848 .
Rossini had been a well-known gourmand and an excellent amateur chef his entire life , but he indulged these two passions fully once he retired from composing , and today there are a number of dishes with the appendage " alla Rossini " to their names that were either created by him or specifically for him .
Often whimsical , these pieces display Rossini 's natural ease of composition and gift for melody , showing obvious influences of Beethoven and Chopin , with many flashes of the composer 's long buried desire for serious , academic composition .
He died at his country house at Passy on Friday , November 13 , 1868 .
Rossini was a foreign associate of the institute , grand officer of the Legion of Honour and recipient of innumerable orders .
In 1989 the conductor Helmuth Rilling recorded the original " Requiem for Rossini " in its world premiere .
In his compositions , Rossini plagiarized freely from himself , a common practice among deadline-pressed opera composers of the time .
Moreover , four of his best known overtures , namely those of ( La cambiale di matrimonio , Tancredi , La Cenerentola and The Barber of Seville ) , share operas apart from those with which they are most famously associated .
A characteristic mannerism in Rossini 's orchestral scoring is a long , steady building of sound over an ostinato figure , creating " tempests in teapots by beginning in a whisper and rising to a flashing , glittering storm " .
A few of Rossini 's operas remained popular throughout his lifetime and continuously since his demise ; others were resurrected from semi-obscurity in the last half of the 20th century , during the so-called " bel canto revival . "
