The Apple III could be viewed as an enhanced Apple II -- then the newest heir to a line of 8-bit machines dating back to 1976 .
Officially , however , the Apple III was not part of the Apple II line , but rather a close cousin ( akin to the Apple Lisa 's relation to the Macintosh ) .
In 1981 , International Business Machines unveiled the IBM Personal Computer ( IBM PC ) -- a completely new 16-bit design soon available in a wide range of inexpensive clones .
The business market moved rapidly towards the PC-DOS / MS-DOS platform , eventually pulling away from the Apple 8-bit computer line .
Despite numerous stability issues and recalls , Apple was eventually able to produce a reliable and dependable version of the machine , however damage to the computer 's reputation had already been done and it failed to do well commercially as a direct result .
In the end , an estimated 65,000 Apple III computers were sold .
Apple co-founder Steve Wozniak stated that the primary reason for the Apple III 's failure was that the system was designed by Apple 's marketing department , unlike Apple 's previous engineering-driven projects .
The Apple III 's failure led to Apple reevaluating their plan to phase out the Apple II and eventually continued on with its development .
As a result , later Apple II models incorporated some hardware [ citation needed ] and software technologies of the Apple III .
The Apple III was designed to be a business computer and an eventual successor for the Apple II .
While the Apple II contributed to the inspirations of several important business products , such as VisiCalc , Multiplan and Apple Writer , the computer 's hardware architecture , operating system and developer environment were limited .
The Apple III addressed these weaknesses .
Third-party vendors also produced memory upgrade kits that allowed the Apple III to reach up to 512 KB .
Other Apple III built-in features included an 80-column display with upper and lowercase characters , a numeric keypad , dual-speed ( pressure sensitive ) cursor control keys , 6-bit audio , 16-color high-resolution graphics , and a built-in 140 KB 5.25 " floppy disk drive .
These choices could not be changed while programs were running , unlike the Apple IIc , which had a keyboard switch directly above the keyboard , allowing switching on the fly .
The Apple III introduced an advanced operating system called Apple SOS , pronounced " apple sauce " .
Its ability to address resources by name instead of a physical location allowed the Apple III to be more scalable .
Apple SOS also allowed the full capacity of a storage device to be used as a single volume , such as the Apple ProFile hard disk drive .
And , Apple SOS supported a hierarchical file system .
Some of the features and code base of Apple SOS made their way into the Apple II 's ProDOS and GS/OS operating systems , as well as Lisa 7/7 and Macintosh system software .
The Apple III also introduced a new BASIC interpreter called Apple III Business BASIC , and later an implementation of UCSD Pascal for more structured programming .
Originally intended as a direct replacement to the Apple II series , it was designed for backwards-compatibility of Apple II software in order to migrate users over .
Several Apple-produced peripherals were made available because of the Apple III .
The original Apple III came with a built-in real-time clock , which was recognized by Apple SOS .
For additional storage , Apple produced the ProFile external hard disk system .
The ProFile was not made available until the release of the revised Apple III over a year later .
The 14,000 units of the original Apple III sold were returned and replaced with the entirely new revised model .
The keyboard was designed in the style of the earlier beige Apple IIe .
Owners of the earlier Apple III could obtain the newer logic board as a service replacement .
Steve Jobs forced on the idea of no fan or air vents -- in order to make the computer run quietly .
Jobs would later push this same ideology onto almost all Macintosh models he had control of -- from the Apple Lisa and Macintosh 128K to the iMac .
To allow the computer to dissipate heat , the base of the Apple III was made of heavy cast aluminum , which supposedly acted as a heat sink .
And , unlike the Apple II series , the power supply was stored -- without its own shell -- in a compartment separate from the logic board .
However , many Apple III 's experienced heating issues , allegedly caused by insufficient cooling and inability to dissipate the heat efficiently .
To address the heat problem , later Apple III 's were fitted with heat sinks .
Some users stated that their Apple III became so hot that the chips started dislodging from the board , the screen would display garbled data , or their disk would come out of the slot " melted " .
[ citation needed ] Jerry Manock , the case designer , refuted these case design flaw charges and maintained that the unit adequately dissipated the internal heat , which he proved with various tests .
Apple designed a new circuit board -- with more layers and normal-width traces .
Earlier Apple III units came with a built-in real time clock , manufactured by National Semiconductor .
While it was assumed that a vendor would test parts before shipping them , Apple did not perform this level of testing .
Apple was soldering chips directly to boards and could not easily change out a bad chip if one was found .
Eventually , Apple solved this problem by removing the real-time clock from the Apple III 's specification , rather than shipping the Apple III with the clock pre-installed , and sold the peripheral as a level 1 technician add-on .
For a variety of reasons , the Apple III was a commercial failure .
In the end , Apple had to replace the first 14,000 Apple III machines , free of charge .
However , for new customers in late 1981 , Apple " reintroduced " a newly revised system , with twice as much memory , which sold for a much lower introductory price of $ 3,495 .
The filesystem and some design ideas from Apple SOS , the Apple III 's operating system , were part of Apple ProDOS and Apple GS/OS , the major operating systems for the Apple II series following the demise of the Apple III .
