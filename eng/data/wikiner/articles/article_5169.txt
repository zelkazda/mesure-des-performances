Puccini was born in Lucca in Tuscany , into a family with five generations of musical history behind them , including composer Domenico Puccini .
Later , Puccini took the position of church organist and choir master in Lucca , but it was not until he saw a performance of Verdi 's Aida that he became inspired to be an opera composer .
In 1880 , with the help of a relative and a grant , Puccini enrolled in the Milan Conservatory to study composition with Amilcare Ponchielli and Antonio Bazzini .
Although he did not win , Le Villi was later staged in 1884 at the Teatro Dal Verme and it caught the attention of Giulio Ricordi , head of G. Ricordi & Co. music publishers , who commissioned a second opera , Edgar , in 1889 .
While renting a house there , he spent time hunting but regularly visited Lucca .
He lived there until 1921 when pollution produced by peat works on the lake forced him to move to Viareggio , a few kilometres north .
Following his passion for driving fast cars , Puccini was nearly killed in a major accident in 1903 .
Finally , in 1912 , the death of Giulio Ricordi , Puccini 's editor and publisher , ended a productive period of his career .
However , Puccini completed La fanciulla del West in 1910 and finished the score of La rondine in 1917 .
Of the three , Gianni Schicchi has remained the most popular , containing the popular aria " O mio babbino caro " .
A diagnosis of throat cancer led his doctors to recommend a new and experimental radiation therapy treatment , which was being offered in Brussels .
Puccini and his wife never knew how serious the cancer was , as the news was only revealed to his son .
Puccini died there on 29 November 1924 , from complications from the treatment ; uncontrolled bleeding led to a heart attack the day after surgery .
He was buried in Milan , but in 1926 his son arranged for the transfer of his father 's remains to a specially created chapel inside the Puccini villa at Torre del Lago .
Turandot , his final opera , was left unfinished , and the last two scenes were completed by Franco Alfano based on the composer 's sketches .
When Arturo Toscanini conducted the premiere performance in April 1926 , he chose not to perform Alfano 's portion of the score .
The performance reached the point where Puccini had completed the score , at which time Toscanini stopped the orchestra .
( Some record that then Toscanini picked up the baton , turned to the audience , and announced , " But his disciples finished his work . "
However , some musicians consider Alfano I to be a more dramatically complete version .
In 2002 , an official new ending was composed by Luciano Berio from original sketches , but this finale has , to date , been performed only infrequently .
Unlike Verdi and Wagner , Puccini did not appear to be active in the politics of his day .
In addition , it can be noted that had Puccini done so , his close friend Toscanini would probably have severed all friendly connection with him and ceased conducting his operas .
The subject of Puccini 's style is one that was once treated dismissively by musicologists ; this can be attributed to a perception that his work , with its emphasis on melody and evident popular appeal , lacked " depth . "
Despite the place Puccini clearly occupies in the popular tradition of Verdi , his style of orchestration also shows the strong influence of Wagner , matching specific orchestral configurations and timbres to different dramatic moments .
The structures of Puccini 's works are also noteworthy .
While it is to an extent possible to divide his operas into arias or numbers ( like Verdi 's ) , his scores generally present a very strong sense of continuous flow and connectivity , perhaps another sign of Wagner 's influence .
Like Wagner , Puccini used leitmotifs to connote characters and sentiments ( or combinations thereof ) .
Unlike Wagner , though , Puccini 's motifs are static : where Wagner 's motifs develop into more complicated figures as the characters develop , Puccini 's remain more or less identical throughout the opera ( in this respect anticipating the themes of modern musical theatre ) .
Another distinctive quality in Puccini 's works is the use of the voice in the style of speech i.e. canto parlando ; characters sing short phrases one after another as if they were in conversation .
Puccini is also celebrated for his melodic gift and many of his melodies are both memorable and enduringly popular .
Pulitzer Prize-winning music critic Lloyd Schwartz summarized Puccini thus : " Is it possible for a work of art to seem both completely sincere in its intentions and at the same time counterfeit and manipulative ?
Puccini built a major career on these contradictions .
Although Puccini is mainly known for his operas , he also wrote some other orchestral pieces , sacred music , chamber music and songs for voice and piano .
