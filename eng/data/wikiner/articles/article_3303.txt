David Lawrence Angell ( April 10 , 1946 -- September 11 , 2001 ) was an American producer of sitcoms .
Angell won multiple Emmy Awards as the creator and executive producer , along with Peter Casey and David Lee , of the comedy series Frasier .
He entered the army upon graduation and served at the Pentagon until 1972 .
He then moved to Boston and worked as a methods analyst at an engineering company and later at an insurance firm in Rhode Island .
Angell moved to Los Angeles in 1977 .
Five years later , he sold his second script to Archie Bunker 's Place .
In 1983 , he joined Cheers as a staff writer .
In 1985 , Angell joined forces with Peter Casey and David Lee as Cheers supervising producers/writers .
In 1990 , they created and executive-produced the comedy series Wings .
In delivering his eulogy , David Lee credited Angell with coining the word " boinking " as a euphemism for sex on Cheers .
Kelsey Grammer and Ted Danson also spoke at the funeral .
On the Frasier series finale in 2004 , the characters Daphne and Niles had a son , named David ( in dedication to Angell ) .
