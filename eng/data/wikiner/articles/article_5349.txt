In his autobiography , Cardano claimed that his mother had attempted to abort him .
Shortly before his birth , his mother had to move from Milan to Pavia to escape the plague ; her three other children died from the disease .
In 1520 , he entered the University of Pavia and later in Padua studied medicine .
He published the solutions to the cubic and quartic equations in his 1545 book Ars Magna .
The solution to one particular case of the cubic , x 3 + ax = b ( in modern notation ) , was communicated to him by Niccolò Fontana Tartaglia ( who later claimed that Cardano had sworn not to reveal it , and engaged Cardano in a decade-long fight ) , and the quartic was solved by Cardano 's student Lodovico Ferrari .
Cardano was notoriously short of money and kept himself solvent by being an accomplished gambler and chess player .
The generating circles of these hypocycloids were later named Cardano circles or cardanic circles and were used for the construction of the first high-speed printing presses .
He was familiar with a report by Rudolph Agricola about a deaf mute who had learned to write .
Cardano 's eldest and favorite son was executed in 1560 after he confessed to having poisoned his cuckolding wife .
Cardano himself was accused of heresy in 1570 because he had computed and published the horoscope of Jesus in 1554 .
Apparently , his own son contributed to the prosecution , bribed by Tartaglia .
