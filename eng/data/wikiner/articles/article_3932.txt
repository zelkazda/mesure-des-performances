Euskadi Ta Askatasuna or ETA , is an armed Basque nationalist and separatist organization .
Since 1968 , ETA has killed over 800 individuals , injured thousands and undertaken dozens of kidnappings .
ETA has changed its internal structure on several occasions for different reasons , commonly security ones .
ETA has divided the three substructures into a total of eleven .
ETA 's intention is to disperse its members and reduce the impact of detentions .
ETA 's armed operations are organized in different taldes ( " groups " ) or commandos , generally composed of three to five members , whose objective is to conduct attacks in a specific geographic zone .
The small cellars used to hide the people kidnapped are named by ETA and ETA 's supporters " people 's jails " .
Among its members , ETA distinguishes between legales/legalak ( " legal ones " ) , those members who do not have police records and live apparently normal lives ; liberados ( " liberated " ) members known to the police that are on ETA 's payroll and working full time for ETA ; and apoyos ( " support " ) who just give occasional help and logistics support to the organization when required .
Many of the present-day members of ETA started their collaboration with the organisation as participants in the kale borroka .
It generally received 8 to 15 % of the vote in the Basque Autonomous Community .
Batasuna 's political status has been a very controversial issue .
It was considered to be the political wing of ETA .
The court considered proven that Batasuna had links with ETA and that it constituted in fact part of ETA 's structure .
Nevertheless , most of their members and certainly most of their leadership were former Batasuna supporters or affiliates .
The bulk of Batasuna supporters voted in this election for PCTV , a virtually unknown political formation until then .
PCTV obtained 9 seats of 75 ( 12.44 % of votes ) at the Basque Parliament .
Spain 's transition to democracy from 1975 on and ETA 's progressive radicalisation have resulted in a steady loss of support , which became especially apparent at the time of their 1997 kidnapping and countdown assassination of Miguel Ángel Blanco .
There were also concerns that Spain 's " judicial offensive " against alleged ETA supporters constitute a threat to human rights .
According to Amnesty International , torture was still " persistent " , though not " systematic . "
Another 10 % agreed with ETA 's ends , but not their means .
3 % said that their attitude towards ETA was mainly one of fear , 3 % expressed indifference and 3 % were undecided or did not answer .
About 3 % gave ETA " justified , with criticism " support ( supporting the group but critizising some of their actions ) and only 1 % gave ETA total support .
Even within Batasuna voters , at least 48 % rejected ETA 's violence .
This poll also reveals that the hope of a peaceful resolution to the issue of the constitutional status of the Basque region has fallen to 78 % ( from 90 % in April ) .
These polls did not cover Navarre , where support for Basque nationalist electoral options is weaker ( around 25 % of population ) or the Northern Basque Country where support is even weaker ( around 15 % of population ) .
ETA was founded by young nationalists , who were for a time affiliated with the PNV .
Their split from the PNV was apparently because they considered the PNV too moderate in its opposition to Franco 's dictatorship .
This was an era of wars of national liberation such as the anti-colonial war in Algeria .
It is not clear when exactly ETA first began a policy of assassination , nor is it clear who committed the first assassinations identified with ETA .
this attack was not claimed by ETA or any other group .
The assassination had been planned for months and was executed by placing a bomb in the sewer below the street where Blanco 's car passed every day .
Both ETA ( m ) and ETA ( pm ) refused offers of amnesty , instead continuing and intensifying their violent struggle .
The years 1978 -- 80 were to prove ETA 's most deadly , with 68 , 76 , and 91 fatalities , respectively .
This caused a new division in ETA ( pm ) between the seventh and eighth assemblies .
With no factions existing anymore , ETA ( m ) revamped the original name of Euskadi Ta Askatasuna .
The GAL committed assassinations , kidnappings and torture , not only of ETA members but of civilians supposedly related to those , some of whom turned out to have nothing to do with ETA .
27 people were murdered by GAL .
The airing of the state-sponsored " dirty war " scheme and the imprisonment of officials responsible for GAL in the early 1990s led to a political scandal in Spain .
Premier Felipe González was quoted as saying that the constitutional state has to defend itself " also in the sewers " something which , for some , indicated at least his knowledge of the scheme .
However , his involvement with the GAL could never be proven .
ETA members and supporters routinely claim torture at the hands of any police force .
There have been some successful prosecutions of proven tortures during the " dirty war " period of the mid-1980s , although the penalties have been considered by Amnesty International as unjustifiably light and lenient with co-conspirators and enablers .
In this regard , Amnesty International has shown concern for the continuous disregard on the recommendations issued by the agency to prevent the alleged abuses to possibly take place .
Also in this regard , ETA 's manuals have been found instructing its members and supporters to claim routinely that they had been tortured while detained .
As a result of ETA 's violence , threats and killings of journalists , Reporters Without Borders has included Spain in all six editions of its annual watchlist on press freedom .
Thus , this NGO has included ETA in its watchlist " Predators of Press Freedom " .
These were the first systematic demonstrations in the Basque Country against terrorist violence .
The long road trips had caused accidental deaths that are protested against by ETA supporters .
After a two-month truce , ETA adopted even more radical positions .
The appearance of these groups was attributed by many to the supposed weakness of ETA , which obliged them to resort to minors to maintain or augment their impact on society after arrests of leading militants , including the " cupola " .
ETA also began to menace leaders of other parties besides rival Basque nationalist parties .
Later came acts of violence such as the November 6 , 2001 car bomb in Madrid , which injured sixty-five , and attacks on football stadiums and tourist destinations .
The September 11 , 2001 attacks appeared to have dealt a hard blow to ETA , owing to the toughening of " antiterrorist " measures ( such as the freezing of bank accounts ) , the increase in international police coordination , and the end of the toleration some countries had , up until then , extended to ETA .
On March 1 , 2004 , in a place between Alcalá de Henares and Madrid , a light truck with 536 kg of explosives was discovered by the Guardia Civil .
ETA was initially blamed for the 2004 Madrid bombings by the outgoing government and large sections of the press .
The judicial investigation currently states that there is no relationship between ETA and the Madrid bombings .
ETA 's tactics include :
After decreasing peaks in the fatal casualties in 1987 and 1991 , 2000 remains to date as the last year when ETA could kill more than 20 in a single year .
Since 2002 to date , the yearly number of ETA 's fatal casualties has been reduced to single digits .
To date , about 65 % of ETA 's killings have been committed in the Basque Country , followed by Madrid with roughly 15 % .
Navarre and Catalonia also register significant numbers .
Although ETA used robbery as a means of financing its activities in its early days , it has since been accused both of arms trafficking and of benefiting economically from its political counterpart Batasuna .
Extortion remains ETA 's main source of funds .
There are Basque nationalist parties with similar goals as those of ETA ( namely , independence ) but who openly reject their violent means .
In addition a number of left-wing parties , such as Ezker Batua , Batzarre and some sectors of the EAJ-PNV party , also support self-determination but are not in favour of independence .
This implied a change from the organization 's previous low-profile in the French Basque Country , which successive ETA leaders had used to discreetly managing their activities in Spain .
ETA considers its prisoners political prisoners .
Until 2003 , ETA consequently forbade them to ask penal authorities for progression to tercer grado ( a form of open prison that allows single-day or weekend furloughs ) or parole .
Some were assassinated by ETA for leaving the organisation and going through reinsertion programs .
The law has resulted in the banning of Herri Batasuna and its successor parties unless they condemn explicitly terrorist actions and , at times , imprisoning or trying some of its leaders who have been indicted for cooperation with ETA .
Judge Baltasar Garzón has initiated a judicial procedure ( coded as 18/98 ) , aimed towards the support structure of ETA .
In August 1999 Judge Baltasar Garzón authorized the reopening of the newspaper and the radio , but they coulnd n't reopen due to economic difficulties .
Furthermore , the Interior Minister said that those members of ETA now arrested had ordered the latest terrorist attacks , and that the man considered to be the head of the terrorists , Francisco Javier López Peña was " not just another arrest because he is , in all probability , the man who has most political and military weight in the terrorist group . "
Interior Minister Alfredo Perez Rubalcaba said about the arrests : " We ca n't say this is the only ETA unit but it was the most active , most dynamic and of course the most wanted one . "
The European Union and the United States list ETA as a terrorist organization in their relevant watch lists .
The Canadian Parliament listed ETA as a terrorist organization on April 2 , 2003 .
At 6:00 P.M. , José Luis Rodríguez Zapatero released a statement stating that the " peace process " had been discontinued .
In another case in the same week , 21 people were convicted whose work on behalf of ETA prisoners actually belied secretive links to the armed separatists themselves .
[ citation needed ] ETA reacted to this actions by placing three major car bombs in less than 24 hours in northern Spain .
In April 2009 Jurdan Martitegi was arrested , making the fourth consecutive of ETA 's military chiefs being captured within a single year , an unprecedented police record further weakening the group .
The group , and therefore the violence resurged in summer 2009 , with several ETA attacks leaving three people dead and dozens injured around Spain .
In December 2009 , Spain raised its terror alert after warning that ETA could be planning major attacks or a high-profile kidnappings during Spain 's European Union presidency .
The next day , after being asked by the opposition , Alfredo Pérez Rubalcaba said that warning was part of a strategy .
Documentary films about ETA
Novels about ETA
