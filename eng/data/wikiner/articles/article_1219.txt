There is no single version : both the individual books ( Biblical canon ) and their order vary .
The Tanakh ( Hebrew : תנ " ך ) consists of 24 books .
The Torah comprises the following five books :
The Hebrew book titles come from some of the first words in the respective texts .
The remaining four books of the Torah tell the story of Moses , who lived hundreds of years after the patriarchs .
There is some dispute as to how to divide these up ( mainly between the Ramban and Rambam ) .
The Book of Jonah is read on Yom Kippur .
The Nevi'im comprise the following eight books :
In Rabbinic Judaism , the Oral Torah is essential for understanding the Written Torah literally ( as it includes neither vowels nor punctuation ) and exegetically .
However , the order of the books is not entirely the same as that found in Hebrew manuscripts and in the ancient versions and varies from Judaism in interpretation and emphasis .
They also sometimes adopt variants that appear in other texts e.g. those discovered among the Dead Sea Scrolls .
The Roman Catholic Church recognizes :
Some other Eastern Orthodox Churches include :
Jesus is its central figure .
Of these , a small number accept the Syriac Peshitta as representative of the original .
See Aramaic primacy .
A general outline , as described by C. S. Lewis , is as follows :
In early Christianity " canon " referred to a list of books approved for public reading .
Under Latin usage from the fourth century on , canon came to stand for a closed and authoritative list in the sense of rule or norm .
These events , taken together , may have caused the Jews to close their " canon . "
In addition to the Septuagint , Christianity subsequently added various writings that would become the New Testament .
During the Protestant Reformation , certain reformers proposed different canonical lists to those currently in use .
Some include 2 Esdras .
Marcion , an early Christian heretic , and his followers , had a Bible that excluded the Old Testament .
Bible versions are discussed below , while Bible translations can be found on a separate page .
There are several different ancient versions of the Tanakh in Hebrew , mostly differing by spelling , and the traditional Jewish version is based on the version known as Aleppo Codex .
The primary biblical text for early Christians was the Septuagint or ( LXX ) .
Especially since the Protestant Reformation , Bible translations for many languages have been made .
In the 17th century Thomas Hobbes collected the current evidence and became the first scholar [ citation needed ] to conclude outright that Moses could not have written the bulk of the Torah .
Scholars intrigued by the hypothesis that Moses had not written the Pentateuch considered other authors .
These were synthesized by Julius Wellhausen ( 1844 -- 1918 ) , who suggested a historical framework for the composition of the documents and their redaction ( combination ) into the final document known as the Pentateuch .
If you want to pose a new model , you compare its merits with those of Wellhausen 's model . "
The development of the hypothesis has not stopped with Wellhausen .
The documentary hypothesis has more recently been refined by later scholars such as Martin Noth ( who in 1943 provided evidence that Deuteronomy plus the following six books make a unified history from the hand of a single editor ) , Harold Bloom , Frank Moore Cross and Richard Elliot Friedman .
The documentary hypothesis , at least in the four-document version advanced by Wellhausen , has been controversial since its formulation .
The direction of this criticism is to question the existence of separate , identifiable documents , positing instead that the biblical text is made up of almost innumerable strands so interwoven as to be hardly untangleable -- the J document , in particular , has been subjected to such intense dissection that it seems in danger of disappearing .
There are a wide range of interpretations of the existing Biblical archaeology .
Another example involves the story of Noah 's Ark .
Biblical literalists support a theory of a worldwide flood as described in the story and are looking for archaeological evidence in the region of the mountains of Ararat in north-east Turkey where Genesis says Noah 's Ark came to rest .
Mainstream scientists discount a literal interpretation of the Ark story , on the basis of geology and other sciences .
