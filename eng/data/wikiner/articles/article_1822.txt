Barry Morris Goldwater ( January 1 , 1909 -- May 29 , 1998 ) was a five-term United States Senator from Arizona ( 1953 -- 1965 , 1969 -- 1987 ) and the Republican Party 's nominee for President in the 1964 election .
He lost the 1964 presidential election to incumbent Democrat Lyndon B. Johnson by one of the largest landslides in history , bringing down many Republican candidates as well .
The Johnson campaign and other critics painted him as a reactionary , while supporters praised his crusades against the Soviet Union , labor unions , and the welfare state .
In 1974 , as an elder statesman of the party , Goldwater forced the resignation of President Nixon when the evidence of cover-up became overwhelming and impeachment was imminent .
The family department store made the Goldwaters comfortably wealthy .
Goldwater came to know former president Herbert Hoover , whose conservative politics he admired greatly .
With the American entry into World War II , Goldwater received a reserve commission in the United States Army Air Forces .
He also flew " the hump " over the Himalayas to deliver supplies to the Republic of China .
In 1940 , Goldwater became one of the first people to run the Colorado River recreationally through Grand Canyon when he participated as an oarsman on Norman Nevills ' second commercial river trip .
Goldwater emphasized his strong opposition to the worldwide spread of communism in his 1960 book The Conscience of a Conservative .
In 1961 , Goldwater told an audience of Atlanta Republicans that " we 're not going to get the negro vote as a block in 1964 and 1968 , so we ought to go hunting where the ducks are " .
In 1964 , Goldwater ran a conservative campaign that emphasized " states ' rights . "
Goldwater 's 1964 campaign was a magnet for conservatives .
Goldwater broadly opposed strong action by the federal government .
Although he had supported all previous federal civil rights legislation , Goldwater made the decision to oppose the Civil Rights Act of 1964 .
In the end , Johnson swept the election .
What distinguished him from his predecessors was , according to Hans J. Morgenthau , his firmness of principle and determination , which did not allow him to be content with rhetoric .
Goldwater fought in 1971 to stop U.S. funding of the United Nations after the People 's Republic of China was admitted to the organization .
In 1964 , he fought and won a bitterly contested , multi-candidate race for the Republican Party 's presidential nomination .
His main rival was New York Governor Nelson Rockefeller , whom he defeated by a narrow margin in the bitterly fought California primary .
His nomination was opposed by liberal Republicans who thought Goldwater 's demand for rollback -- defeat of the Soviet Union -- would foment a nuclear war .
Goldwater remained popular in Arizona , and in the 1968 Senate election he was reelected ( this time to the seat of retiring Senator Carl Hayden ) .
The 1974 election saw Goldwater easily reelected .
Goldwater said later that the close result convinced him not to run again .
Both were best-sellers but failed to bolster Goldwater 's electoral prospects .
Goldwater seriously considered retirement in 1980 before deciding to run for reelection .
Goldwater faced a surprisingly tough battle for reelection .
Arizona 's changing population also hurt Goldwater .
The state 's population had soared , and a huge portion of the electorate had not lived in the state when Goldwater was previously elected , hence many voters were not familiar with Goldwater , who was on the defensive for much of the campaign .
At around daybreak , Goldwater learned that he had been reelected thanks to absentee votes , which were among the last to be counted .
Goldwater 's surprisingly close victory in 1980 came despite Reagan 's 61 % landslide over Jimmy Carter in Arizona .
Yet Goldwater remained staunchly anti-communist and " hawkish " on military issues .
His most important legislative achievement may have been the Goldwater-Nichols Act , which reorganized the U.S. military 's senior-command structure .
Goldwater disliked Johnson ( who he said " used every dirty trick in the bag " , and Richard M. Nixon of California , whom he later called " the most dishonest individual I have ever met in my life . " )
He was viewed by many traditional Republicans as being too far on the right wing of the Republican spectrum to appeal to the mainstream majority necessary to win a national election .
As a result , more liberal Republicans recruited a series of opponents , including New York Governor Nelson Rockefeller , Henry Cabot Lodge , Jr. , of Massachusetts and Pa. Governor William Scranton , to challenge Goldwater .
Goldwater would defeat Rockefeller in the winner-take-all California primary and secure the nomination .
He also had solid southern Republican backing .
A bright young Birmingham lawyer , John Grenier , secured commitments from 271 of 279 southern convention delegates to back Goldwater .
Grenier went on to serve as executive director of the national GOP during the Goldwater campaign .
Goldwater famously declared in his bold acceptance speech at the 1964 Republican Convention : " I would remind you that extremism in the defense of liberty is no vice .
This paraphrase of Cicero was included at the suggestion of Harry V. Jaffa , though the speech was primarily written by Karl Hess .
Due to President Johnson 's popularity , however , Goldwater held back from attacking the president directly ; he did not even mention Johnson by name in his convention speech .
Past comments came back to haunt Goldwater throughout his campaign .
Bush 's son , George H.W. Bush , was also a strong Goldwater supporter in both the nomination and general election campaigns .
Goldwater was painted as a dangerous figure by the Johnson campaign , which countered Goldwater 's slogan " In your heart , you know he 's right " with the lines " In your guts , you know he 's nuts, " and " In your heart , you know he might " ( that is , might actually use nuclear weapons , as opposed to merely subscribing to deterrence ) .
Johnson himself did not mention Goldwater in his own acceptance speech at the 1964 Democratic National Convention .
Regarding Vietnam , Goldwater charged that Johnson 's policy was devoid of " goal , course , or purpose, " leaving " only sudden death in the jungles and the slow strangulation of freedom . "
Goldwater 's own rhetoric on nuclear war was viewed by many as quite uncompromising , a view buttressed by off-hand comments such as , " Let 's lob one into the men 's room at the Kremlin . "
Goldwater did his best to counter the Johnson attacks , criticizing the Johnson administration for its perceived ethical lapses , and stating in a commercial that " … we , as a nation , are not far from the kind of moral decay that has brought on the fall of other nations and people … I say it is time to put conscience back in government .
And by good example , put it back in all walks of American life . "
Goldwater campaign commercials included statements of support by actor Raymond Massey and moderate Republican senator Margaret Chase Smith .
The two main articles contended that Goldwater was mentally unfit to be president .
After the election , Goldwater sued the publisher , the editor and the magazine for libel .
He went on to say " Goldwater sued me for $ 2 million .
The campaign advertisement ended with a plea to vote for Johnson , implying that Goldwater ( who was not mentioned by name ) would provoke a nuclear war if elected .
The advertisement , which featured only a few spoken words of narrative and relied on imagery for its emotional impact , was one of the most provocative in American political campaign history , and many analysts credit it as being the birth of the modern style of " negative political ads " on television .
In all , Johnson won an overwhelming 486 electoral votes , to Goldwater 's 52 .
Goldwater , with his customary bluntness , remarked : " We would have lost even if Abraham Lincoln had come back and campaigned with us . "
Goldwater 's poor showing pulled down many supporters .
Goldwater maintained later in life that he would have won the election if the country had not been in a state of extended grief ( referring to the assassination of John F. Kennedy ) , and that it was simply not ready for a third President in just fourteen months .
The columnist George Will remarked after the 1980 Presidential election that it took 16 years to count the votes from 1964 and Goldwater won .
He played little part in the election or administration of Richard Nixon , but he helped force Nixon 's resignation in 1974 .
In 1976 he helped block Rockefeller 's renomination as Vice President .
When Reagan challenged Ford for the presidential nomination in 1976 , Goldwater endorsed Ford , looking for consensus rather than conservative idealism .
Goldwater viewed abortion as a matter of personal choice , not intended for government intervention .
Goldwater also disagreed with the Reagan administration on certain aspects of foreign policy .
After his retirement in 1987 , Goldwater described the conservative Ariz. Governor Evan Mecham as " hardheaded " and called on him to resign , and two years later stated that the Republican party had been taken over by a " bunch of kooks " .
) Goldwater also had harsh words for his one-time political protege , President Reagan , particularly after the Iran-Contra Affair became public in 1986 .
" He was sitting in his office with his hands on his cane ... and he said to me , ' Well , are n't you going to ask me about the Iran arms sales ? '
It had just been announced that the Reagan administration had sold arms to Iran .
' " , though aside from the Iran-Contra scandal , Goldwater thought nonetheless that Reagan was a good president .
Some of Goldwater 's statements in the 1990s aggravated many social conservatives .
He endorsed Democrat Karan English in an Arizona congressional race , urged Republicans to lay off Bill Clinton over the Whitewater scandal , and criticized the military 's ban on homosexuals : " Everyone knows that gays have served honorably in the military since at least the time of Julius Caesar . "
You are extremists , and you 've hurt the Republican party much more than the Democrats have . "
In 1996 , he told Bob Dole , whose own presidential campaign received lukewarm support from conservative Republicans : " We 're the new liberals of the Republican party .
In that same year , with Senator Dennis DeConcini , Goldwater endorsed an Ariz. initiative to legalize medical marijuana against the countervailing opinion of social conservatives .
He had personal and financial relationships with two racketeers -- Willie Bioff and Gus Greenbaum -- both of whom were later murdered in gangland executions .
The latter is now used by an Arizona club honoring him as a commemorative call .
During the Vietnam War , he spent many hours giving servicemen overseas the ability to talk to their families at home over the Military Affiliate Radio System ( MARS ) .
Goldwater was also a prominent spokesman for amateur radio and its enthusiasts .
Eventually his doll collection included 437 items , presented in 1969 to the Heard Museum in Phoenix .
Goldwater was an accomplished amateur photographer and in his estate left some 15,000 of his images to three Ariz. institutions .
Ansel Adams wrote a foreword to the 1976 book .
Goldwater further wrote that there were rumors the evidence would be released , and that he was " just as anxious to see this material as you are , and I hope we will not have to wait much longer . "
The April 25 , 1988 issue of The New Yorker carried an interview where Goldwater said he repeatedly asked his friend , Gen. Curtis LeMay , if there was any truth to the rumors that UFO evidence was stored in a secret room at Wright-Patterson Air Force Base , and if he ( Goldwater ) might have access to the room .
According to Goldwater , an angry LeMay gave him " holy hell " and said , " Not only ca n't you get into it but do n't you ever mention it to me again . "
He died on May 29 , 1998 , at the age of 89 in Paradise Valley , Ariz. , of complications from the stroke .
" We can not allow the American flag to be shot at anywhere on earth if we are to retain our respect and prestige . "
