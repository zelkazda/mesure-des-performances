Tatian 's harmony follows the gospels closely in terms of text but puts the text in a new , different sequence .
Like other harmonies , the Diatessaron resolves apparent contradictions .
It also omits the genealogies in Matthew and Luke .
Tatian omitted duplicated text , especially among the synoptics .
In the early Church , the gospels at first circulated independently , with Matthew the most popular .
The Diatessaron is notable evidence for the authority already enjoyed by the four gospels by the mid-second century .
Twenty years after Tatian 's harmony , Irenaeus expressly proclaimed the authoritative character of the four gospels .
And an ancient commentary in Armenian exists .
Ephrem did not comment on all passages in the Diatessaron , and nor does he always quote commentated passages in full ; but for those phrases that he does quote , the commentary provides for the first time a dependable witness to Tatian 's original ; and also confirms its content and their sequence .
Gradually , without extant copies to which to refer , the Diatessaron developed a reputation for having been heretical .
( Note that this Ammonius may or may not be the Ammonius Saccas who taught Origen and Plotinus ) .
