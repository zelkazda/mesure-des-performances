Irwin Allen Ginsberg ( pronounced /ˈɡɪnzbərɡ/ ; June 3 , 1926 -- April 5 , 1997 ) was an American poet who vigorously opposed militarism , materialism and sexual repression .
The poem , dedicated to writer Carl Solomon , has a memorable opening :
Ginsberg 's Howl electrified the audience .
According to fellow poet Michael McClure , it was clear " that a barrier had been broken , that a human voice and body had been hurled against the harsh wall of America and its supporting armies and navies and academies and institutions and ownership systems and power support bases . "
In 1957 , Howl attracted widespread publicity when it became the subject of an obscenity trial in which a San Francisco prosecutor argued it contained " filthy , vulgar , obscene , and disgusting language . "
The poem seemed especially outrageous in 1950s America because it depicted both heterosexual and homosexual sex at a time when sodomy laws made homosexual acts a crime in every U.S. state .
Howl reflected Ginsberg 's own bisexuality and his homosexual relationships with a number of men , including Peter Orlovsky , his lifelong partner .
In Howl and in his other poetry , Ginsberg drew inspiration from the epic , free verse style of the 19th century American poet Walt Whitman .
Both wrote passionately about the promise ( and betrayal ) of American democracy ; the central importance of erotic experience ; and the spiritual quest for the truth of everyday existence .
J. D. McClatchy , editor of the Yale Review called Ginsberg " the best-known American poet of his generation , as much a social force as a literary phenomenon . "
McClatchy added that Ginsberg , like Whitman , " was a bard in the old manner -- outsized , darkly prophetic , part exuberance , part prayer , part rant .
Ginsberg 's political activism was consistent with his religious beliefs .
The literary critic , Helen Vendler , described Ginsberg as " tirelessly persistent in protesting censorship , imperial politics , and persecution of the powerless . "
Other honors included the National Arts Club gold medal and his induction into the American Academy and Institute of Arts and Letters , both in 1979 .
Ginsberg was born into a Jewish family in Newark , New Jersey , and grew up in nearby Paterson .
She was also an active member of the Communist Party and took Ginsberg and his brother Eugene to party meetings .
Ginsberg later said that his mother " made up bedtime stories that all went something like : ' The good king rode forth from his castle , saw the suffering workers and healed them . ' "
As a young teenager , Ginsberg began to write letters to The New York Times about political issues , such as World War II and workers ' rights .
Also while in high school , Ginsberg began reading Walt Whitman , inspired by his teacher 's passionate reading .
Carr also introduced Ginsberg to Neal Cassady , for whom Ginsberg had a long infatuation .
In 1948 in an apartment in Harlem , Ginsberg had an auditory hallucination while reading the poetry of William Blake ( later referred to as his " Blake vision " ) .
Ginsberg believed that he had witnessed the interconnectedness of the universe .
Ginsberg claims he was immediately attracted to Corso , who was straight , but understanding of homosexuality after three years in prison .
Ginsberg was even more struck by reading Corso 's poems , realizing Corso was " spiritually gifted . "
Ginsberg introduced Corso to the rest of his inner circle .
Ginsberg was living with the woman and took Corso over to their apartment , where the woman proposed sex and Corso , still very young , fled in fear .
Ginsberg introduced Corso to Kerouac and Burroughs and they began to travel together .
Ginsberg and Corso remained life-long friends and collaborators .
In 1954 , in San Francisco , Ginsberg met Peter Orlovsky ( 1933-2010 ) , with whom he fell in love and who remained his life-long partner .
There , Ginsberg also met three budding poets and Zen enthusiasts who were friends at Reed College : Gary Snyder , Philip Whalen , and Lew Welch .
At first , Ginsberg refused , but once he 'd written a rough draft of " Howl " , he changed his " fucking mind " , as he put it .
Of more personal significance to Ginsberg : that night was the first public reading of " Howl " , a poem that brought worldwide fame to Ginsberg and to many of the poets associated with him .
An account of that night can be found in Kerouac 's novel The Dharma Bums , describing how change was collected from audience members to buy jugs of wine , and Ginsberg reading passionately , drunken , with arms outstretched .
Ginsberg 's principal work , " Howl " , is well-known for its opening line : " I saw the best minds of my generation destroyed by madness , starving hysterical naked ... . " " Howl " was considered scandalous at the time of its publication , because of the rawness of its language .
Shortly after its 1956 publication by San Francisco 's City Lights Bookstore , it was banned for obscenity .
Ginsberg claimed at one point that all of his work was an extended biography .
Ginsberg also later claimed that at the core of " Howl " were his unresolved emotions about his schizophrenic mother .
" Howl " chronicles the development of many important friendships throughout Ginsberg 's life .
In the poem , Ginsberg focused on " Carl Solomon !
In 1957 , Ginsberg surprised the literary world by abandoning San Francisco .
They were soon joined by Burroughs and others .
This period was documented by the photographer Harold Chapman , who moved in at about the same time , and took pictures constantly of the residents of the " hotel " until it closed in 1963 .
During 1962-3 , Ginsberg and Orlovsky traveled extensively across India , living half a year at a time in Benaras and Calcutta .
Also during this time , he formed friendships with some of the prominent young Bengali poets of the time including Shakti Chattopadhyay and Sunil Gangopadhyay .
In May , 1965 , Allen Ginsberg arrived at Better Books , London , and offered to read anywhere for free .
Shortly after his arrival , he gave a reading at Better Books , which was described by Jeff Nuttall as " the first healing wind on a very parched collective mind " .
Soon after the reading at Better Books , plans were hatched for the International Poetry Incarnation , which was to be held at the Royal Albert Hall in London on June 11 , 1965 .
The event attracted an audience of 7,000 people to readings and live and tape performances by a wide variety of figures , including Allen Ginsberg , Adrian Mitchell , Alexander Trocchi , Harry Fainlight , Anselm Hollo , Christopher Logue , George Macbeth , Gregory Corso , Lawrence Ferlinghetti , Michael Horovitz , Simon Vinkenoog , Spike Hawkins , Tom McGrath and William S. Burroughs .
Peter Whitehead documented the event on film and released it as Wholly Communion .
A key feature of this term seems to be a friendship with Ginsberg .
Part of their dissatisfaction with the term came from the mistaken identification of Ginsberg as the leader .
Ginsberg never claimed to be the leader of a movement .
Later in his life , Ginsberg formed a bridge between the beat movement of the 1950s and the hippies of the 1960s , befriending , among others , Timothy Leary , Ken Kesey , and Bob Dylan .
Ginsberg donated money , materials , and his reputation to help the Swami establish the first temple , and toured with him to promote his cause .
Music and chanting were both important parts of Ginsberg 's live delivery during poetry readings .
According to Richard Brookhiser , an associate of Buckley 's , the host commented that it was " the most unharried Krishna I 've ever heard . "
Ginsberg won the National Book Award for his book The Fall of America .
He died April 5 , 1997 , surrounded by family and friends in his East Village loft in New York City , succumbing to liver cancer via complications of hepatitis .
Ginsberg 's willingness to talk about taboo subjects made him a controversial figure during the conservative 1950s , and a significant figure in the 1960s .
In the mid-1950s , no reputable publishing company would even consider publishing " Howl " .
At the time , such " sex talk " employed in " Howl " was considered a form of vulgar pornography , and could even be prosecuted under law .
The sex that Ginsberg painted did not portray the conventional sex between married couples , or even long time lovers .
Instead , Ginsberg promotes casual sex , and used this to echo the emptiness and constant hunger in the lives of Americans .
For example , in " Howl " , Ginsberg praises the man " who sweetened the snatches of a million girls " .
Ginsberg also went a step further , discussing the taboo topic of homosexuality .
Ginsberg 's publisher was brought up on charges for publishing pornography , and the outcome led to a judge going on record dismissing charges because the poem carried " redeeming social importance " , thus setting an important legal precedent .
Even so , Ginsberg continued to broach controversial subjects throughout the 1970s , ' 80s , and ' 90s .
Ginsberg traveled to Barger 's home in Oakland to talk the situation through .
It is rumored that he offered Barger and other members of the Hell 's Angels LSD as a gesture of friendship and goodwill .
They vowed not to attack the next day 's protest march , and , furthermore , deemed Ginsberg a man who was worth helping out .
Pike was arrested , and was later released when Ginsberg , sporting a black eye , refused to press charges .
For example , in 1965 Ginsberg was deported from Cuba for publicly protesting Cuba 's anti-marijuana stance .
Václav Havel points to Ginsberg as an important inspiration in striving for freedom [ citation needed ] .
Ginsberg was an early proponent of freedom for gay people .
He was a staunch supporter of others whose expression challenged obscenity laws ( William S. Burroughs and Lenny Bruce , for example ) .
Ginsberg also spoke out in defense of the freedom of expression of the North American Man/Boy Love Association ( NAMBLA ) .
In the essay , he referred to NAMBLA " as a forum for reform of those laws on youthful sexuality which members deem oppressive , a discussion society not a sex club . "
Ginsberg also talked often about drug use .
Throughout the 1960s he took an active role in the demystification of LSD , and , with Timothy Leary , worked to promote its common use .
Though early on he had intentions to be a labor lawyer , Ginsberg wrote poetry for most of his life .
Most of his very early poetry was written in formal rhyme and meter like his father or like his idol William Blake .
His admiration for the writing of Jack Kerouac inspired him to take poetry more seriously .
Though he took odd jobs to support himself , in 1955 , upon the advice of a psychiatrist , Ginsberg dropped out of the working world to devote his entire life to poetry .
Soon after , he wrote " Howl " , the poem that brought him and his friends much fame and allowed him to live as a professional poet for the rest of his life .
Since Ginsberg 's poetry is intensely personal , and since much of the vitality of those associated with the beat generation comes from mutual inspiration , much credit for style , inspiration , and content can be given to Ginsberg 's friends .
Ginsberg claimed throughout his life that his biggest inspiration was Kerouac 's concept of " spontaneous prose " .
However , Ginsberg was much more prone to revise than Kerouac .
For example , when Kerouac saw the first draft of " Howl " he disliked the fact that Ginsberg had made editorial changes in pencil ( transposing " negro " and " angry " in the first line , for example ) .
Kerouac only wrote out his concepts of Spontaneous Prose at Ginsberg 's insistence because Ginsberg wanted to learn how to apply the technique to his poetry .
Much of the final section of the first part of " Howl " is a description of this .
However , Moloch is mentioned a few times in the Torah and references to Ginsberg 's Jewish background are not infrequent in his work .
Moloch has subsequently been interpreted as any system of control , including the conformist society of post-World War II America , focused on material gain , which Ginsberg frequently blamed for the destruction of all those outside of societal norms .
He also made sure to emphasize that Moloch is a part of all of us : the decision to defy socially created systems of control -- and therefore go against Moloch -- is a form of self-destruction .
Many of the characters Ginsberg references in " Howl " , such as Neal Cassady and Herbert Huncke , destroyed themselves through excessive substance abuse or a generally wild lifestyle .
The personal aspects of " Howl " are perhaps as important as the political aspects .
Ginsberg later admitted that the drive to write " Howl " was fueled by sympathy for his ailing mother , an issue which he was not yet ready to deal with directly .
He studied poetry under William Carlos Williams , who was then in the middle of writing his epic poem Paterson about the industrial city near his home .
Ginsberg , after attending a reading by Williams , sent the older poet several of his poems and wrote an introductory letter .
Williams hated the poems .
He told Ginsberg later , " In this mode perfection is basic , and these poems are not perfect . "
Though he hated the early poems , Williams loved the exuberance in Ginsberg 's letter .
He taught Ginsberg not to emulate the old masters , but to speak with his own voice and the voice of the common American .
Williams taught him to focus on strong visual images , in line with Williams ' own motto " No ideas but in things . "
His time studying under Williams led to a tremendous shift from the early formalist work to a loose , colloquial free verse style .
Carl Solomon introduced Ginsberg to the work of Antonin Artaud , and Jean Genet .
Ginsberg also claimed other more traditional influences , such as : Franz Kafka , Herman Melville , Fyodor Dostoevsky , Edgar Allan Poe , and even Emily Dickinson .
He noticed in viewing Cézanne 's paintings that when the eye moved from one color to a contrasting color , the eye would spasm , or " kick . "
Ginsberg used this technique in his poetry , putting together two starkly dissimilar images : something weak with something strong , an artifact of high culture with an artifact of low culture , something holy with something unholy .
The example Ginsberg most often used was " hydrogen jukebox " ( which later became the title of a song cycle composed by Philip Glass with lyrics drawn from Ginsberg 's poems ) .
The phrases " eyeball kick " and " hydrogen jukebox " both show up in " Howl " , as well as a direct quote from Cézanne : " Pater Omnipotens Aeterna Deus " .
Consequently , Ginsberg often had to defend his choice to break away from traditional poetic structure , often citing Williams , Pound , and Whitman as precursors .
Ginsberg 's style may have seemed to critics chaotic or unpoetic , but to Ginsberg it was an open , ecstatic expression of thoughts and feelings that were naturally poetic .
Like Williams , Ginsberg 's line breaks were often determined by breath : one line in " Howl " , for example , should be read in one breath .
Ginsberg claimed he developed such a long line because he had long breaths ( saying perhaps it was because he talked fast , or he did yoga , or he was Jewish ) .
The long line could also be traced back to his study of Walt Whitman ; Ginsberg claimed Whitman 's long line was a dynamic technique few other poets had ventured to develop further .
Whitman is often compared to Ginsberg because their poetry sexualized aspects of the male form -- though there is no direct evidence Whitman was homosexual .
Many of Ginsberg 's early long line experiments contain some sort of anaphoric repetition , or repetition of a " fixed base " ( for example " who " in " Howl " , " America " in " America " ) , and this has become a recognizable feature of Ginsberg 's style .
In the original draft of " Howl " , each line is in a " stepped triadic " format reminiscent of Williams .
In " America " , he experimented with a mix of longer and shorter lines .
