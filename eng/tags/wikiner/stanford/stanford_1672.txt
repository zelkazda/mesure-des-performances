The	O
Social	ORGANIZATION
Democratic	ORGANIZATION
Party	ORGANIZATION
(	O
SDP	ORGANIZATION
)	O
was	O
a	O
political	O
party	O
in	O
the	O
U.K.	LOCATION
which	O
was	O
created	O
on	O
March	O
26	O
1981	O
and	O
that	O
existed	O
until	O
1988	O
.	O
For	O
the	O
1983	O
and	O
1987	O
General	ORGANIZATION
Elections	ORGANIZATION
,	O
the	O
SDP	ORGANIZATION
joined	O
the	O
Liberal	ORGANIZATION
Party	ORGANIZATION
in	O
the	O
SDP-Liberal	ORGANIZATION
Alliance	ORGANIZATION
.	O
There	O
were	O
long-running	O
claims	O
of	O
corruption	O
and	O
administrative	O
decay	O
within	O
Labour	ORGANIZATION
at	O
local	O
level	O
,	O
and	O
concerns	O
that	O
experienced	O
and	O
able	O
Labour	O
MPs	O
could	O
be	O
deselected	O
(	O
i.e.	O
,	O
lose	O
the	O
Labour	ORGANIZATION
Party	ORGANIZATION
nomination	O
)	O
by	O
those	O
wanting	O
to	O
put	O
into	O
a	O
safe	O
seat	O
their	O
friends	O
,	O
family	O
or	O
members	O
of	O
their	O
own	O
Labour	ORGANIZATION
faction	O
.	O
In	O
some	O
areas	O
,	O
the	O
Militant	O
tendency	O
were	O
held	O
to	O
be	O
systematically	O
targeting	O
weak	O
local	O
party	O
branches	O
in	O
safe	O
seat	O
areas	O
in	O
order	O
to	O
have	O
their	O
own	O
candidates	O
selected	O
,	O
and	O
thus	O
become	O
MPs	O
.	O
Eddie	PERSON
Milne	PERSON
at	O
Blyth	LOCATION
and	O
Dick	PERSON
Taverne	PERSON
in	O
Lincoln	LOCATION
were	O
both	O
victims	O
of	O
such	O
intrigues	O
during	O
the	O
1970s	O
,	O
but	O
in	O
both	O
cases	O
there	O
was	O
enough	O
of	O
a	O
local	O
outcry	O
by	O
party	O
members	O
--	O
and	O
the	O
electorate	O
--	O
for	O
them	O
to	O
fight	O
and	O
win	O
their	O
seats	O
as	O
independent	O
candidates	O
against	O
the	O
official	O
Labour	ORGANIZATION
candidates	O
.	O
In	O
October	O
1972	O
he	O
resigned	O
his	O
seat	O
to	O
force	O
a	O
by-election	O
in	O
which	O
he	O
fought	O
as	O
a	O
Democratic	ORGANIZATION
Labour	ORGANIZATION
candidate	O
against	O
the	O
official	O
party	O
candidate	O
.	O
They	O
argued	O
that	O
a	O
new	O
type	O
of	O
political	O
force	O
was	O
needed	O
to	O
challenge	O
the	O
Conservative	ORGANIZATION
Party	ORGANIZATION
.	O
At	O
the	O
end	O
,	O
one	O
asked	O
him	O
why	O
they	O
should	O
vote	O
for	O
him	O
,	O
and	O
Healey	PERSON
answered	O
"	O
You	O
have	O
nowhere	O
else	O
to	O
go	O
"	O
(	O
to	O
stop	O
the	O
left-winger	O
Michael	PERSON
Foot	PERSON
from	O
winning	O
)	O
.	O
Healey	PERSON
's	O
arrogance	O
convinced	O
many	O
that	O
their	O
days	O
as	O
members	O
of	O
the	O
Labour	ORGANIZATION
Party	ORGANIZATION
were	O
now	O
over	O
.	O
The	O
opening	O
statement	O
of	O
principles	O
contained	O
in	O
the	O
preamble	O
of	O
the	O
party	O
's	O
constitution	O
stated	O
that	O
"	O
The	O
SDP	ORGANIZATION
exists	O
to	O
create	O
and	O
defend	O
an	O
open	O
,	O
classless	O
and	O
more	O
equal	O
society	O
which	O
rejects	O
prejudices	O
based	O
upon	O
sex	O
,	O
race	O
,	O
colour	O
or	O
religion	O
"	O
.	O
Twenty-eight	O
Labour	O
MPs	O
eventually	O
joined	O
the	O
new	O
party	O
,	O
along	O
with	O
one	O
member	O
of	O
the	O
Conservative	ORGANIZATION
Party	ORGANIZATION
,	O
Christopher	PERSON
Brocklebank-Fowler	PERSON
.	O
Much	O
of	O
the	O
party	O
's	O
initial	O
membership	O
came	O
from	O
the	O
Social	ORGANIZATION
Democratic	ORGANIZATION
Alliance	ORGANIZATION
.	O
The	O
party	O
also	O
received	O
a	O
boost	O
with	O
the	O
recruitment	O
of	O
former	O
student	O
leaders	O
from	O
outside	O
the	O
Labour	ORGANIZATION
Party	ORGANIZATION
.	O
Claret	O
is	O
an	O
"	O
agreeable	O
"	O
wine	O
,	O
and	O
a	O
metaphor	O
for	O
the	O
party	O
's	O
harmonious	O
internal	O
relations	O
compared	O
to	O
those	O
of	O
the	O
strife-torn	O
Labour	ORGANIZATION
Party	ORGANIZATION
of	O
the	O
period	O
.	O
At	O
the	O
party	O
's	O
first	O
electoral	O
contest	O
,	O
Jenkins	PERSON
narrowly	O
failed	O
to	O
win	O
a	O
by-election	O
at	O
Warrington	ORGANIZATION
in	O
July	O
1981	O
,	O
describing	O
it	O
as	O
his	O
"	O
first	O
defeat	O
,	O
but	O
by	O
far	O
(	O
his	O
)	O
greatest	O
victory	O
"	O
.	O
In	O
the	O
Glasgow	LOCATION
Hillhead	LOCATION
by-election	O
in	O
March	O
1982	O
,	O
another	O
candidate	O
named	O
Roy	PERSON
Jenkins	PERSON
was	O
nominated	O
by	O
Labour	ORGANIZATION
Party	ORGANIZATION
activists	O
to	O
contest	O
the	O
seat	O
in	O
order	O
to	O
confuse	O
voters	O
and	O
split	O
his	O
potential	O
vote	O
.	O
Ultimately	O
,	O
the	O
SDP	ORGANIZATION
's	O
Jenkins	PERSON
was	O
elected	O
.	O
Later	O
in	O
the	O
year	O
,	O
Shirley	PERSON
Williams	PERSON
defeated	O
Bill	PERSON
Rogers	PERSON
in	O
the	O
ballot	O
to	O
become	O
SDP	ORGANIZATION
president	O
.	O
The	O
SDP	ORGANIZATION
formed	O
the	O
SDP-Liberal	ORGANIZATION
Alliance	ORGANIZATION
with	O
the	O
Liberal	ORGANIZATION
Party	ORGANIZATION
late	O
in	O
1981	O
,	O
under	O
the	O
joint	O
leadership	O
of	O
Roy	ORGANIZATION
Jenkins	ORGANIZATION
(	O
SDP	O
)	O
and	O
Liberal	ORGANIZATION
leader	O
David	PERSON
Steel	PERSON
.	O
The	O
Liberal	ORGANIZATION
Party	ORGANIZATION
,	O
and	O
in	O
particular	O
its	O
leader	O
,	O
David	PERSON
Steel	PERSON
,	O
had	O
applauded	O
the	O
formation	O
of	O
the	O
SDP	ORGANIZATION
from	O
the	O
sidelines	O
from	O
the	O
very	O
start	O
.	O
During	O
an	O
era	O
of	O
public	O
disillusionment	O
with	O
the	O
two	O
main	O
parties	O
--	O
Labour	O
and	O
the	O
Conservatives	O
--	O
and	O
widescale	O
unemployment	O
,	O
the	O
Alliance	ORGANIZATION
achieved	O
considerable	O
success	O
in	O
parliamentary	O
by-elections	O
.	O
In	O
early	O
1982	O
,	O
after	O
public	O
disagreements	O
over	O
who	O
could	O
fight	O
which	O
seats	O
in	O
the	O
forthcoming	O
election	O
,	O
the	O
poll	O
rating	O
dipped	O
,	O
but	O
the	O
party	O
remained	O
ahead	O
of	O
both	O
Labour	O
and	O
the	O
Conservatives	O
.	O
However	O
,	O
following	O
the	O
outbreak	O
of	O
the	O
Falklands	O
War	O
on	O
April	O
2	O
1982	O
,	O
the	O
Conservative	O
government	O
of	O
Margaret	PERSON
Thatcher	PERSON
soared	O
from	O
third	O
to	O
first	O
place	O
in	O
the	O
public	O
opinion	O
polls	O
.	O
The	O
standing	O
of	O
the	O
Alliance	ORGANIZATION
and	ORGANIZATION
Labour	ORGANIZATION
declined	O
.	O
During	O
the	O
1983-87	O
parliament	O
some	O
SDP	ORGANIZATION
members	O
started	O
to	O
become	O
unsettled	O
at	O
what	O
appeared	O
to	O
be	O
the	O
increasingly	O
right	O
wing	O
course	O
taken	O
by	O
SDP	ORGANIZATION
leader	O
David	PERSON
Owen	PERSON
.	O
Roy	PERSON
Jenkins	PERSON
was	O
amongst	O
those	O
who	O
lost	O
their	O
seats	O
.	O
From	O
the	O
outset	O
,	O
the	O
formation	O
of	O
the	O
Alliance	ORGANIZATION
had	O
raised	O
questions	O
as	O
to	O
whether	O
it	O
would	O
lead	O
to	O
a	O
merged	O
party	O
,	O
or	O
the	O
two	O
parties	O
were	O
destined	O
to	O
compete	O
with	O
each	O
other	O
.	O
This	O
in	O
turn	O
led	O
to	O
grassroots	O
tensions	O
in	O
some	O
areas	O
between	O
Liberal	O
and	O
SDP	ORGANIZATION
branches	O
that	O
impaired	O
their	O
ability	O
to	O
mount	O
joint	O
campaigns	O
successfully	O
.	O
After	O
the	O
disappointment	O
of	O
1987	O
,	O
Steel	ORGANIZATION
proposed	O
a	O
formal	O
merger	O
of	O
the	O
two	O
parties	O
.	O
Jenkins	PERSON
and	O
Steel	ORGANIZATION
had	O
believed	O
this	O
to	O
be	O
eventually	O
inevitable	O
after	O
the	O
party	O
failed	O
to	O
break	O
through	O
at	O
the	O
1983	O
election	O
.	O
Jenkins	PERSON
denied	O
that	O
a	O
merger	O
had	O
been	O
his	O
original	O
intent	O
.	O
But	O
the	O
majority	O
of	O
the	O
SDP	ORGANIZATION
's	O
membership	O
voted	O
in	O
favour	O
of	O
the	O
union	O
.	O
Most	O
members	O
of	O
the	O
SDP	ORGANIZATION
who	O
joined	O
the	O
Liberal	ORGANIZATION
Democrats	ORGANIZATION
have	O
remained	O
in	O
that	O
party	O
.	O
There	O
have	O
been	O
a	O
few	O
exceptions	O
:	O
notably	O
Roger	PERSON
Liddle	PERSON
.	O
Polly	PERSON
Toynbee	PERSON
was	O
among	O
those	O
who	O
left	O
the	O
Labour	ORGANIZATION
Party	ORGANIZATION
to	O
return	O
to	O
their	O
ranks	O
later	O
.	O
