Although	O
Haiti	LOCATION
averages	O
approximately	O
250	O
people	O
per	O
square	O
kilometer	O
(	O
650	O
per	O
sq	O
.	O
Haiti	LOCATION
has	O
many	O
close	O
cultural	O
ties	O
with	O
its	O
neighbors	O
,	O
Cuba	LOCATION
especially	O
,	O
and	O
the	O
neighboring	O
Dominican	LOCATION
Republic	LOCATION
due	O
to	O
the	O
history	O
involving	O
those	O
countries	O
and	O
the	O
post-imperialist	O
era	O
.	O
French	O
the	O
other	O
official	O
language	O
.	O
It	O
is	O
spoken	O
more	O
frequently	O
near	O
the	O
border	O
with	O
the	O
Dominican	LOCATION
Republic	LOCATION
.	O
The	O
state	O
religion	O
is	O
Roman	O
Catholicism	O
which	O
50-55	O
%	O
of	O
the	O
population	O
professes	O
.	O
The	O
following	O
demographic	O
statistics	O
are	O
from	O
the	O
CIA	ORGANIZATION
World	ORGANIZATION
Factbook	ORGANIZATION
.	O
