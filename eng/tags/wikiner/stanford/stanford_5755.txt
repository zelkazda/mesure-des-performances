Stone	O
tools	O
,	O
particularly	O
projectile	O
points	O
and	O
scrapers	O
,	O
are	O
the	O
primary	O
evidence	O
of	O
the	O
earliest	O
well	O
known	O
human	O
activity	O
in	O
the	O
Americas	LOCATION
.	O
At	O
its	O
peak	O
,	O
between	O
the	O
12th	O
and	O
13th	O
centuries	O
,	O
Cahokia	LOCATION
may	O
have	O
been	O
the	O
most	O
populous	O
city	O
in	O
North	LOCATION
America	LOCATION
.	O
The	O
civilizations	O
did	O
not	O
develop	O
extensive	O
livestock	O
as	O
there	O
were	O
few	O
suitable	O
species	O
,	O
although	O
alpacas	O
and	O
llamas	O
were	O
domesticated	O
for	O
use	O
as	O
beasts	O
of	O
burden	O
and	O
sources	O
of	O
wool	O
and	O
meat	O
in	O
the	O
Andes	LOCATION
.	O
The	O
course	O
of	O
further	O
agricultural	O
development	O
was	O
greatly	O
altered	O
by	O
the	O
arrival	O
of	O
Europeans	O
.	O
They	O
left	O
behind	O
the	O
great	O
city	O
Monte	LOCATION
Alban	LOCATION
.	O
This	O
civilization	O
was	O
thought	O
to	O
be	O
the	O
first	O
in	O
America	LOCATION
to	O
develop	O
a	O
writing	O
system	O
.	O
They	O
continue	O
to	O
live	O
on	O
in	O
the	O
state	O
of	O
Michoacan	LOCATION
.	O
Their	O
capital	O
city	O
Tenochtitlan	LOCATION
was	O
one	O
of	O
the	O
largest	O
cities	O
of	O
all	O
time	O
.	O
Columbus	LOCATION
came	O
at	O
a	O
time	O
in	O
which	O
many	O
technical	O
developments	O
in	O
sailing	O
techniques	O
and	O
communication	O
made	O
it	O
possible	O
to	O
report	O
his	O
voyages	O
easily	O
and	O
to	O
spread	O
word	O
of	O
them	O
throughout	O
western	O
Europe	LOCATION
.	O
The	O
American	O
Revolutionary	O
War	O
lasted	O
until	O
1783	O
.	O
Simón	O
Bolívar	O
and	O
José	PERSON
de	PERSON
San	PERSON
Martín	PERSON
,	O
among	O
others	O
,	O
led	O
their	O
independence	O
struggle	O
.	O
The	O
cotton	O
,	O
tobacco	O
,	O
and	O
sugar	O
cane	O
harvested	O
by	O
slaves	O
became	O
important	O
exports	O
for	O
the	O
U.S.	LOCATION
and	O
the	O
Caribbean	LOCATION
countries	O
.	O
In	O
the	O
ensuing	O
Conscription	O
Crisis	O
of	O
1917	O
,	O
riots	O
broke	O
out	O
on	O
the	O
streets	O
of	O
Montreal	LOCATION
.	O
In	O
neighboring	O
Newfoundland	LOCATION
,	O
the	O
new	O
dominion	O
suffered	O
a	O
devastating	O
loss	O
on	O
July	O
1	O
,	O
1916	O
,	O
the	O
First	O
day	O
on	O
the	O
Somme	O
.	O
The	O
U.S.	LOCATION
was	O
then	O
able	O
to	O
play	O
a	O
crucial	O
role	O
at	O
the	O
Paris	LOCATION
Peace	O
Conference	O
of	O
1919	O
that	O
shaped	O
interwar	O
Europe	LOCATION
.	O
Mexico	LOCATION
was	O
not	O
part	O
of	O
the	O
war	O
as	O
the	O
country	O
was	O
embroiled	O
in	O
the	O
Mexican	O
Revolution	O
at	O
the	O
time	O
.	O
The	O
incident	O
happened	O
in	O
spite	O
of	O
Mexico	LOCATION
's	O
neutrality	O
at	O
that	O
time	O
.	O
The	O
destruction	O
of	O
Europe	LOCATION
wrought	O
by	O
the	O
war	O
vaulted	O
all	O
North	O
American	O
countries	O
to	O
more	O
important	O
roles	O
in	O
world	O
affairs	O
.	O
The	O
United	LOCATION
States	LOCATION
especially	O
emerged	O
as	O
a	O
"	O
superpower	O
"	O
.	O
This	O
led	O
to	O
the	O
Canada-United	LOCATION
States	LOCATION
Free	O
Trade	O
Agreement	O
in	O
January	O
1989	O
.	O
Mexican	O
presidents	O
Miguel	PERSON
de	O
la	O
Madrid	LOCATION
,	O
in	O
the	O
early	O
80s	O
and	O
Carlos	PERSON
Salinas	PERSON
de	PERSON
Gortari	PERSON
in	O
the	O
late	O
80s	O
,	O
started	O
implementing	O
liberal	O
economic	O
strategies	O
that	O
were	O
seen	O
as	O
a	O
good	O
move	O
.	O
However	O
,	O
Mexico	LOCATION
experienced	O
a	O
strong	O
economic	O
recession	O
in	O
1982	O
and	O
the	O
Mexican	O
peso	O
suffered	O
a	O
devaluation	O
.	O
In	O
the	O
United	LOCATION
States	LOCATION
president	O
Ronald	PERSON
Reagan	PERSON
attempted	O
to	O
move	O
the	O
U.S.	LOCATION
back	O
towards	O
a	O
hard	O
anti-communist	O
line	O
in	O
foreign	O
affairs	O
,	O
in	O
what	O
his	O
supporters	O
saw	O
as	O
an	O
attempt	O
to	O
assert	O
moral	O
leadership	O
in	O
the	O
world	O
community	O
.	O
Domestically	O
,	O
Reagan	PERSON
attempted	O
to	O
bring	O
in	O
a	O
package	O
of	O
privatization	O
and	O
regulation	O
to	O
stimulate	O
the	O
economy	O
.	O
The	O
End	O
of	O
the	O
Cold	O
War	O
and	O
the	O
beginning	O
of	O
the	O
era	O
of	O
sustained	O
economic	O
expansion	O
coincided	O
during	O
the	O
1990s	O
.	O
Despite	O
the	O
failure	O
of	O
a	O
lasting	O
political	O
union	O
,	O
the	O
concept	O
of	O
Central	O
American	O
reunification	O
,	O
though	O
lacking	O
enthusiasm	O
from	O
the	O
leaders	O
of	O
the	O
individual	O
countries	O
,	O
rises	O
from	O
time	O
to	O
time	O
.	O
In	O
1856-1857	O
the	O
region	O
successfully	O
established	O
a	O
military	O
coalition	O
to	O
repel	O
an	O
invasion	O
by	O
U.S.	LOCATION
adventurer	O
William	PERSON
Walker	PERSON
.	O
In	O
1907	O
a	O
Central	ORGANIZATION
American	ORGANIZATION
Court	ORGANIZATION
of	ORGANIZATION
Justice	ORGANIZATION
was	O
created	O
.	O
On	O
December	O
13	O
,	O
1960	O
,	O
Guatemala	LOCATION
,	O
El	LOCATION
Salvador	LOCATION
,	O
Honduras	LOCATION
,	O
and	O
Nicaragua	LOCATION
established	O
the	O
Central	O
American	O
Common	O
Market	O
(	O
"	O
CACM	O
"	O
)	O
.	O
Costa	LOCATION
Rica	LOCATION
,	O
because	O
of	O
its	O
relative	O
economic	O
prosperity	O
and	O
political	O
stability	O
,	O
chose	O
not	O
to	O
participate	O
in	O
the	O
CACM	O
.	O
The	O
project	O
was	O
an	O
immediate	O
economic	O
success	O
,	O
but	O
was	O
abandoned	O
after	O
the	O
1969	O
"	O
Football	O
War	O
"	O
between	O
El	LOCATION
Salvador	LOCATION
and	O
Honduras	LOCATION
.	O
A	O
Central	ORGANIZATION
American	ORGANIZATION
Parliament	ORGANIZATION
has	O
operated	O
,	O
as	O
a	O
purely	O
advisory	O
body	O
,	O
since	O
1991	O
.	O
In	O
the	O
1960s	O
and	O
1970s	O
,	O
the	O
governments	O
of	O
Argentina	LOCATION
,	O
Brazil	LOCATION
,	O
Chile	LOCATION
,	O
and	O
Uruguay	LOCATION
were	O
overthrown	O
or	O
displaced	O
by	O
U.S.-aligned	O
military	O
dictatorships	O
.	O
These	O
dictatorships	O
detained	O
tens	O
of	O
thousands	O
of	O
political	O
prisoners	O
,	O
many	O
of	O
whom	O
were	O
tortured	O
and/or	O
killed	O
(	O
on	O
inter-state	O
collaboration	O
,	O
see	O
Operation	O
Condor	O
)	O
.	O
In	O
recent	O
years	O
South	O
American	O
governments	O
have	O
drifted	O
to	O
the	O
left	O
,	O
with	O
socialist	O
leaders	O
being	O
elected	O
in	O
Chile	LOCATION
,	O
Bolivia	LOCATION
,	O
Brazil	LOCATION
,	O
Venezuela	LOCATION
,	O
and	O
a	O
leftist	O
president	O
in	O
Argentina	LOCATION
and	O
Uruguay	LOCATION
.	O
Despite	O
the	O
move	O
to	O
the	O
left	O
,	O
South	LOCATION
America	LOCATION
is	O
still	O
largely	O
capitalist	O
.	O
With	O
the	O
founding	O
of	O
the	O
Union	ORGANIZATION
of	ORGANIZATION
South	ORGANIZATION
American	ORGANIZATION
Nations	ORGANIZATION
,	O
South	LOCATION
America	LOCATION
has	O
started	O
down	O
the	O
road	O
of	O
economic	O
integration	O
,	O
with	O
plans	O
for	O
political	O
integration	O
in	O
the	O
European	ORGANIZATION
Union	ORGANIZATION
style	O
.	O
