Antonio	PERSON
Agliardi	PERSON
(	O
September	O
4	O
,	O
1832	O
--	O
May	O
1	O
,	O
1915	O
)	O
was	O
a	O
Roman	O
Catholic	O
Cardinal	O
,	O
archbishop	O
,	O
and	O
papal	O
diplomat	O
.	O
He	O
was	O
born	O
at	O
Cologno	LOCATION
al	LOCATION
Serio	LOCATION
,	O
Province	O
of	O
Bergamo	LOCATION
,	O
Italy	LOCATION
.	O
In	O
1887	O
he	O
again	O
visited	O
India	LOCATION
,	O
to	O
carry	O
out	O
the	O
terms	O
of	O
the	O
concordat	O
arranged	O
with	O
Portugal	LOCATION
.	O
In	O
1889	O
he	O
became	O
papal	O
nuncio	O
at	O
Munich	LOCATION
and	O
in	O
1892	O
at	O
Vienna	LOCATION
.	O
He	O
died	O
in	O
Rome	LOCATION
and	O
was	O
buried	O
in	O
Bergamo	LOCATION
.	O
