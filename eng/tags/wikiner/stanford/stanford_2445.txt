Plato	O
(	O
427-347	O
BC	O
)	O
took	O
over	O
the	O
four	O
elements	O
of	O
Empedocles	LOCATION
.	O
This	O
makes	O
water	O
the	O
element	O
with	O
the	O
greatest	O
number	O
of	O
sides	O
,	O
which	O
Plato	O
regarded	O
as	O
appropriate	O
because	O
water	O
flows	O
out	O
of	O
one	O
's	O
hand	O
when	O
picked	O
up	O
,	O
as	O
if	O
it	O
is	O
made	O
of	O
tiny	O
little	O
balls	O
.	O
Ap	O
(	O
áp-)	O
is	O
the	O
Vedic	O
Sanskrit	O
term	O
for	O
"	O
water	O
"	O
,	O
in	O
Classical	O
Sanskrit	O
occurring	O
only	O
in	O
the	O
pluralis	O
not	O
an	O
element.v	O
,	O
āpas	O
(	O
sometimes	O
re-analysed	O
as	O
a	O
thematic	O
singular	O
,	O
āpa-)	O
,	O
whence	O
Hindi	O
āp	O
.	O
