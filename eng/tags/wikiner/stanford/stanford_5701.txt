The	O
game	O
is	O
popular	O
among	O
both	O
males	O
and	O
females	O
in	O
many	O
parts	O
of	O
the	O
world	O
,	O
particularly	O
in	O
Europe	LOCATION
,	O
Asia	LOCATION
,	O
Australia	LOCATION
,	O
New	LOCATION
Zealand	LOCATION
and	O
South	LOCATION
Africa	LOCATION
.	O
The	O
governing	O
body	O
is	O
the	O
116-member	O
International	ORGANIZATION
Hockey	ORGANIZATION
Federation	ORGANIZATION
(	O
FIH	O
)	O
.	O
While	O
current	O
field	O
hockey	O
appeared	O
in	O
the	O
mid-18th	O
century	O
in	O
England	LOCATION
,	O
primarily	O
in	O
schools	O
,	O
it	O
was	O
not	O
until	O
the	O
first	O
half	O
of	O
the	O
19th	O
century	O
that	O
it	O
became	O
firmly	O
established	O
.	O
The	O
first	O
club	O
was	O
created	O
in	O
1849	O
at	O
Blackheath	O
in	O
south-east	O
London	LOCATION
.	O
Field	O
hockey	O
is	O
the	O
national	O
sport	O
of	O
India	LOCATION
and	O
Pakistan	LOCATION
.	O
The	O
game	O
is	O
played	O
all	O
over	O
North	LOCATION
America	LOCATION
,	O
Europe	LOCATION
and	O
in	O
many	O
other	O
countries	O
around	O
the	O
world	O
to	O
varying	O
extent	O
.	O
It	O
is	O
the	O
most	O
popular	O
sport	O
in	O
Canada	LOCATION
,	O
Finland	LOCATION
,	O
Latvia	LOCATION
,	O
the	O
Czech	LOCATION
Republic	LOCATION
,	O
and	O
in	O
Slovakia	LOCATION
.	O
The	O
governing	O
body	O
is	O
the	O
66-member	O
International	ORGANIZATION
Ice	ORGANIZATION
Hockey	ORGANIZATION
Federation	ORGANIZATION
(	O
IIHF	O
)	O
.	O
Women	O
's	O
ice	O
hockey	O
was	O
added	O
to	O
the	O
Winter	O
Olympics	O
in	O
1998	O
.	O
North	LOCATION
America	LOCATION
's	O
National	ORGANIZATION
Hockey	ORGANIZATION
League	ORGANIZATION
(	O
NHL	ORGANIZATION
)	O
is	O
the	O
strongest	O
professional	O
ice	O
hockey	O
league	O
,	O
drawing	O
top	O
ice	O
hockey	O
players	O
from	O
around	O
the	O
globe	O
.	O
The	O
NHL	ORGANIZATION
rules	O
are	O
slightly	O
different	O
from	O
those	O
used	O
in	O
Olympic	O
ice	O
hockey	O
:	O
the	O
periods	O
are	O
20	O
minutes	O
long	O
,	O
counting	O
downwards	O
.	O
There	O
are	O
early	O
representations	O
and	O
reports	O
of	O
ice	O
hockey-type	O
games	O
being	O
played	O
on	O
ice	O
in	O
the	O
Netherlands	LOCATION
,	O
and	O
reports	O
from	O
Canada	LOCATION
from	O
the	O
beginning	O
of	O
the	O
nineteenth	O
century	O
,	O
but	O
the	O
modern	O
game	O
was	O
initially	O
organized	O
by	O
students	O
at	O
McGill	ORGANIZATION
University	ORGANIZATION
,	O
Montreal	LOCATION
in	O
1875	O
who	O
,	O
by	O
two	O
years	O
later	O
,	O
codified	O
the	O
first	O
set	O
of	O
ice	O
hockey	O
rules	O
and	O
organized	O
the	O
first	O
teams	O
.	O
The	O
rules	O
are	O
very	O
similar	O
to	O
IIHF	ORGANIZATION
ice	O
hockey	O
rules	O
.	O
Canada	LOCATION
is	O
a	O
recognized	O
international	O
leader	O
in	O
the	O
development	O
of	O
the	O
sport	O
,	O
and	O
of	O
equipment	O
for	O
players	O
.	O
Much	O
of	O
the	O
equipment	O
for	O
the	O
sport	O
was	O
first	O
developed	O
in	O
Canada	LOCATION
,	O
such	O
as	O
sledge	O
hockey	O
sticks	O
laminated	O
with	O
fiberglass	O
,	O
as	O
well	O
as	O
aluminum	O
shafts	O
with	O
hand	O
carved	O
insert	O
blades	O
and	O
special	O
aluminum	O
sledges	O
with	O
regulation	O
skate	O
blades	O
.	O
