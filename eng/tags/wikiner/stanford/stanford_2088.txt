From	O
the	O
earliest	O
settlement	O
of	O
the	O
Cayman	LOCATION
Islands	LOCATION
,	O
economic	O
activity	O
was	O
hindered	O
by	O
isolation	O
and	O
a	O
limited	O
natural	O
resource	O
base	O
.	O
The	O
Cayman	ORGANIZATION
Islands	ORGANIZATION
Stock	ORGANIZATION
Exchange	ORGANIZATION
was	O
opened	O
in	O
1997	O
.	O
Population	O
below	O
poverty	O
line	O
:	O
NA	O
%	O
Household	O
income	O
or	O
consumption	O
by	O
percentage	O
share	O
:	O
lowest	O
10	O
%	O
:	O
NA	O
%	O
highest	O
10	O
%	O
:	O
NA	O
%	O
Industrial	O
production	O
growth	O
rate	O
:	O
NA	O
%	O
Economic	O
aid	O
--	O
recipient	O
:	O
$	O
NA	O
This	O
article	O
incorporates	O
public	O
domain	O
material	O
from	O
the	O
CIA	ORGANIZATION
World	ORGANIZATION
Factbook	ORGANIZATION
document	O
"	O
2006	O
edition	O
"	O
.	O
