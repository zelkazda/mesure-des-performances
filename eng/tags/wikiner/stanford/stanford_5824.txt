The	O
early	O
hacker	O
culture	O
and	O
resulting	O
philosophy	O
originated	O
at	O
the	O
Massachusetts	ORGANIZATION
Institute	ORGANIZATION
of	ORGANIZATION
Technology	ORGANIZATION
(	O
MIT	O
)	O
in	O
the	O
1950s	O
and	O
1960s	O
.	O
The	O
term	O
'	O
hacker	O
ethic	O
'	O
is	O
attributed	O
to	O
journalist	O
Steven	PERSON
Levy	PERSON
as	O
described	O
in	O
his	O
book	O
titled	O
Hackers	O
:	O
Heroes	O
of	O
the	O
Computer	O
Revolution	O
,	O
written	O
in	O
1984	O
.	O
The	O
term	O
"	O
hack	O
"	O
arose	O
from	O
MIT	ORGANIZATION
lingo	O
as	O
the	O
word	O
had	O
long	O
been	O
used	O
to	O
describe	O
college	O
pranks	O
that	O
MIT	ORGANIZATION
students	O
would	O
regularly	O
devise	O
.	O
Hackers	O
push	O
programs	O
beyond	O
what	O
they	O
are	O
designed	O
to	O
do	O
.	O
Levy	PERSON
notes	O
that	O
,	O
at	O
other	O
universities	O
,	O
professors	O
were	O
making	O
public	O
proclamations	O
that	O
computers	O
would	O
never	O
be	O
able	O
to	O
beat	O
a	O
human	O
being	O
in	O
chess	O
.	O
Hackers	O
knew	O
better	O
.	O
As	O
Levy	PERSON
stated	O
in	O
the	O
preface	O
of	O
Hackers	O
:	O
Heroes	O
of	O
the	O
Computer	O
Revolution	O
,	O
the	O
general	O
tenets	O
or	O
principles	O
of	O
hacker	O
ethic	O
include	O
:	O
According	O
to	O
Levy	PERSON
's	O
account	O
,	O
sharing	O
was	O
the	O
norm	O
and	O
expected	O
within	O
the	O
non-corporate	O
hacker	O
culture	O
.	O
The	O
principle	O
of	O
sharing	O
stemmed	O
from	O
the	O
atmosphere	O
and	O
resources	O
at	O
MIT	ORGANIZATION
.	O
During	O
the	O
early	O
days	O
of	O
computers	O
and	O
programming	O
,	O
the	O
hackers	O
at	O
MIT	ORGANIZATION
would	O
develop	O
a	O
program	O
and	O
share	O
it	O
.	O
A	O
particular	O
organization	O
of	O
hackers	O
that	O
was	O
concerned	O
with	O
sharing	O
computers	O
with	O
the	O
general	O
public	O
was	O
a	O
group	O
called	O
Community	O
Memory	O
.	O
PCC	ORGANIZATION
opened	O
a	O
computer	O
center	O
where	O
anyone	O
could	O
use	O
the	O
computers	O
there	O
for	O
fifty	O
cents	O
per	O
hour	O
.	O
In	O
fact	O
,	O
when	O
Bill	PERSON
Gates	PERSON
'	O
version	O
of	O
BASIC	O
for	O
the	O
Altair	O
was	O
shared	O
among	O
the	O
hacker	O
community	O
,	O
Gates	PERSON
claimed	O
to	O
have	O
lost	O
a	O
considerable	O
sum	O
of	O
money	O
because	O
few	O
users	O
paid	O
for	O
the	O
software	O
.	O
As	O
a	O
result	O
,	O
Gates	PERSON
wrote	O
an	O
Open	O
Letter	O
to	O
Hobbyists	O
.	O
This	O
letter	O
was	O
published	O
by	O
several	O
computer	O
magazines	O
and	O
newsletters	O
--	O
most	O
notably	O
that	O
of	O
the	O
Homebrew	ORGANIZATION
Computer	ORGANIZATION
Club	ORGANIZATION
where	O
much	O
of	O
the	O
sharing	O
occurred	O
.	O
As	O
Levy	PERSON
described	O
in	O
chapter	O
2	O
,	O
"	O
Hackers	O
believe	O
that	O
essential	O
lessons	O
can	O
be	O
learned	O
about	O
the	O
systems	O
--	O
about	O
the	O
world	O
--	O
from	O
taking	O
things	O
apart	O
,	O
seeing	O
how	O
they	O
work	O
,	O
and	O
using	O
this	O
knowledge	O
to	O
create	O
new	O
and	O
more	O
interesting	O
things	O
.	O
"	O
For	O
example	O
,	O
when	O
the	O
computers	O
at	O
MIT	ORGANIZATION
were	O
protected	O
either	O
by	O
physical	O
locks	O
or	O
login	O
programs	O
,	O
the	O
hackers	O
there	O
systematically	O
worked	O
around	O
them	O
in	O
order	O
to	O
have	O
access	O
to	O
the	O
machines	O
.	O
Hackers	O
assumed	O
a	O
"	O
wilful	O
blindness	O
"	O
in	O
the	O
pursuit	O
of	O
perfection	O
.	O
It	O
is	O
important	O
to	O
note	O
that	O
this	O
behavior	O
was	O
not	O
malicious	O
in	O
nature	O
--	O
the	O
MIT	ORGANIZATION
hackers	O
did	O
not	O
seek	O
to	O
harm	O
the	O
systems	O
or	O
their	O
users	O
(	O
although	O
occasional	O
practical	O
jokes	O
were	O
played	O
using	O
the	O
computer	O
systems	O
)	O
.	O
For	O
example	O
,	O
in	O
Levy	PERSON
's	O
Hackers	O
,	O
each	O
generation	O
of	O
hackers	O
had	O
geographically	O
based	O
communities	O
where	O
collaboration	O
and	O
sharing	O
occurred	O
.	O
For	O
the	O
hackers	O
at	O
MIT	ORGANIZATION
,	O
it	O
was	O
the	O
labs	O
where	O
the	O
computers	O
were	O
running	O
.	O
For	O
the	O
hardware	O
hackers	O
(	O
second	O
generation	O
)	O
and	O
the	O
game	O
hackers	O
(	O
third	O
generation	O
)	O
the	O
geographic	O
area	O
was	O
centered	O
in	O
Silicon	LOCATION
Valley	LOCATION
where	O
the	O
Homebrew	ORGANIZATION
Computer	ORGANIZATION
Club	ORGANIZATION
and	O
the	O
People	ORGANIZATION
's	ORGANIZATION
Computer	ORGANIZATION
Company	ORGANIZATION
helped	O
hackers	O
network	O
,	O
collaborate	O
,	O
and	O
share	O
their	O
work	O
.	O
Eric	PERSON
S.	PERSON
Raymond	PERSON
identifies	O
and	O
explains	O
this	O
concept	O
shift	O
in	O
The	O
Cathedral	O
and	O
the	O
Bazaar	O
.	O
Levy	PERSON
identifies	O
several	O
"	O
true	O
hackers	O
"	O
who	O
significantly	O
influenced	O
the	O
hacker	O
ethic	O
.	O
Levy	PERSON
also	O
identified	O
the	O
"	O
hardware	O
hackers	O
"	O
(	O
the	O
"	O
second	O
generation	O
"	O
,	O
mostly	O
centered	O
in	O
Silicon	LOCATION
Valley	LOCATION
)	O
and	O
the	O
"	O
game	O
hackers	O
"	O
(	O
or	O
the	O
"	O
third	O
generation	O
"	O
)	O
.	O
All	O
three	O
generations	O
of	O
hackers	O
,	O
according	O
to	O
Levy	PERSON
,	O
embodied	O
the	O
principles	O
of	O
the	O
hacker	O
ethic	O
.	O
Some	O
Levy	PERSON
's	O
"	O
second-generation	O
"	O
hackers	O
include	O
:	O
Levy	PERSON
's	O
"	O
third	O
generation	O
"	O
practitioners	O
of	O
hacker	O
ethic	O
include	O
:	O
Free	O
and	O
open	O
source	O
software	O
(	O
often	O
termed	O
FOSS	O
)	O
is	O
the	O
descendant	O
of	O
the	O
hacker	O
ethics	O
that	O
Levy	PERSON
described	O
.	O
