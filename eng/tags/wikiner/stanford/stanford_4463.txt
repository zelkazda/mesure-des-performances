Contrary	O
to	O
the	O
view	O
popularised	O
by	O
Dan	PERSON
Brown	PERSON
's	O
novel	O
The	O
Da	O
Vinci	O
Code	O
,	O
there	O
is	O
no	O
evidence	O
to	O
suggest	O
that	O
the	O
Biblical	O
canon	O
,	O
the	O
list	O
of	O
books	O
decided	O
to	O
be	O
authorative	O
as	O
scripture	O
,	O
was	O
even	O
discussed	O
at	O
the	O
Council	ORGANIZATION
of	ORGANIZATION
Nicaea	ORGANIZATION
,	O
let	O
alone	O
established	O
or	O
edited	O
.	O
Another	O
result	O
of	O
the	O
council	O
was	O
an	O
agreement	O
on	O
when	O
to	O
celebrate	O
the	O
Easter	O
,	O
the	O
most	O
important	O
feast	O
of	O
the	O
ecclesiastical	O
calendar	O
.	O
It	O
authorized	O
the	O
Bishop	O
of	O
Alexandria	LOCATION
to	O
announce	O
annually	O
the	O
exact	O
date	O
to	O
his	O
fellow	O
bishops	O
.	O
To	O
most	O
bishops	O
,	O
the	O
teachings	O
of	O
Arius	O
were	O
heretical	O
and	O
dangerous	O
to	O
the	O
salvation	O
of	O
souls	O
.	O
In	O
the	O
summer	O
of	O
325	O
,	O
the	O
bishops	O
of	O
all	O
provinces	O
were	O
summoned	O
to	O
Nicaea	LOCATION
(	O
now	O
known	O
as	O
İznik	LOCATION
,	O
in	O
modern-day	O
Turkey	LOCATION
)	O
,	O
a	O
place	O
easily	O
accessible	O
to	O
the	O
majority	O
of	O
delegates	O
,	O
particularly	O
those	O
of	O
Asia	LOCATION
Minor	LOCATION
,	O
Syria	LOCATION
,	O
Palestine	LOCATION
,	O
Egypt	LOCATION
,	O
Greece	LOCATION
,	O
and	O
Thrace	LOCATION
.	O
Eusebius	LOCATION
speaks	O
of	O
an	O
almost	O
innumerable	O
host	O
of	O
accompanying	O
priests	O
,	O
deacons	O
and	O
acolytes	O
.	O
In	O
these	O
discussions	O
,	O
some	O
dominant	O
figures	O
were	O
Arius	O
,	O
with	O
several	O
adherents	O
.	O
This	O
profession	O
of	O
faith	O
was	O
adopted	O
by	O
all	O
the	O
bishops	O
"	O
but	O
two	O
from	O
Libya	LOCATION
who	O
had	O
been	O
closely	O
associated	O
with	O
Arius	O
from	O
the	O
beginning	O
.	O
"	O
The	O
exact	O
meaning	O
of	O
many	O
of	O
the	O
words	O
used	O
in	O
the	O
debates	O
at	O
Nicaea	LOCATION
were	O
still	O
unclear	O
to	O
speakers	O
of	O
other	O
languages	O
.	O
Further	O
on	O
it	O
says	O
"	O
That	O
they	O
all	O
may	O
be	O
one	O
;	O
as	O
thou	O
,	O
Father	O
,	O
art	O
in	O
me	O
,	O
and	O
I	O
in	O
thee	O
,	O
that	O
they	O
also	O
may	O
be	O
one	O
in	O
us	O
:	O
that	O
the	O
world	O
may	O
believe	O
that	O
thou	O
hast	O
sent	O
me	O
"	O
;	O
John	PERSON
17:21	O
.	O
Several	O
creeds	O
were	O
already	O
in	O
existence	O
;	O
many	O
creeds	O
were	O
acceptable	O
to	O
the	O
members	O
of	O
the	O
council	O
,	O
including	O
Arius	O
.	O
The	O
text	O
of	O
this	O
profession	O
of	O
faith	O
is	O
preserved	O
in	O
a	O
letter	O
of	O
Eusebius	LOCATION
to	O
his	O
congregation	O
,	O
in	O
Athanasius	PERSON
,	O
and	O
elsewhere	O
.	O
Hosius	PERSON
stands	O
at	O
the	O
head	O
of	O
the	O
lists	O
of	O
bishops	O
,	O
and	O
Athanasius	PERSON
ascribes	O
to	O
him	O
the	O
actual	O
formulation	O
of	O
the	O
creed	O
.	O
The	O
initial	O
number	O
of	O
bishops	O
supporting	O
Arius	O
was	O
small	O
.	O
Meletius	PERSON
,	O
it	O
was	O
decided	O
,	O
should	O
remain	O
in	O
his	O
own	O
city	O
of	O
Lycopolis	LOCATION
in	O
Egypt	LOCATION
,	O
but	O
without	O
exercising	O
authority	O
or	O
the	O
power	O
to	O
ordain	O
new	O
clergy	O
;	O
he	O
was	O
forbidden	O
to	O
go	O
into	O
the	O
environs	O
of	O
the	O
town	O
or	O
to	O
enter	O
another	O
diocese	O
for	O
the	O
purpose	O
of	O
ordaining	O
its	O
subjects	O
.	O
As	O
to	O
Meletius	PERSON
himself	O
,	O
episcopal	O
rights	O
and	O
prerogatives	O
were	O
taken	O
from	O
him	O
.	O
The	O
twenty	O
as	O
listed	O
in	O
the	O
Nicene	O
and	O
Post-Nicene	O
Fathers	O
are	O
as	O
follows	O
:	O
The	O
long-term	O
effects	O
of	O
the	O
Council	O
of	O
Nicaea	LOCATION
were	O
significant	O
.	O
