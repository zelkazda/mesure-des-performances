The	O
City	O
of	O
London	LOCATION
is	O
a	O
small	O
area	O
within	O
Greater	LOCATION
London	LOCATION
,	O
England	LOCATION
.	O
It	O
is	O
the	O
historic	O
core	O
of	O
London	LOCATION
around	O
which	O
the	O
modern	O
conurbation	O
grew	O
and	O
has	O
held	O
city	O
status	O
since	O
time	O
immemorial	O
.	O
It	O
is	O
often	O
referred	O
to	O
as	O
the	O
City	O
(	O
often	O
written	O
on	O
maps	O
as	O
"	O
City	O
"	O
)	O
or	O
the	O
Square	LOCATION
Mile	LOCATION
,	O
as	O
it	O
is	O
just	O
over	O
one	O
square	O
mile	O
(	O
1.12	O
sq	O
mi/2.90	O
km	O
2	O
)	O
in	O
area	O
.	O
These	O
terms	O
are	O
also	O
often	O
used	O
as	O
metonyms	O
for	O
the	O
U.K.	LOCATION
's	O
financial	O
services	O
industry	O
,	O
which	O
has	O
historically	O
been	O
based	O
here	O
.	O
In	O
the	O
medieval	O
period	O
,	O
the	O
City	O
was	O
the	O
full	O
extent	O
of	O
London	LOCATION
.	O
The	O
local	O
authority	O
for	O
the	O
City	O
,	O
the	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
Corporation	ORGANIZATION
,	O
is	O
unique	O
in	O
the	O
United	LOCATION
Kingdom	LOCATION
,	O
and	O
has	O
some	O
unusual	O
responsibilities	O
for	O
a	O
local	O
authority	O
in	O
Britain	LOCATION
,	O
such	O
as	O
being	O
the	O
police	O
authority	O
for	O
the	O
City	O
.	O
It	O
also	O
has	O
responsibilities	O
and	O
ownerships	O
beyond	O
the	O
City	O
's	O
boundaries	O
.	O
The	O
City	O
is	O
today	O
a	O
major	O
business	O
and	O
financial	O
centre	O
,	O
ranking	O
on	O
a	O
par	O
with	O
New	LOCATION
York	LOCATION
City	LOCATION
as	O
the	O
leading	O
centre	O
of	O
global	O
finance	O
;	O
throughout	O
the	O
19th	O
century	O
,	O
the	O
City	O
served	O
as	O
the	O
world	O
's	O
primary	O
business	O
centre	O
,	O
and	O
continues	O
to	O
be	O
a	O
major	O
meeting	O
point	O
for	O
businesses	O
to	O
this	O
day	O
.	O
London	LOCATION
came	O
top	O
in	O
the	O
Worldwide	O
Centres	O
of	O
Commerce	ORGANIZATION
Index	ORGANIZATION
,	O
published	O
in	O
2008	O
.	O
The	O
City	O
has	O
a	O
resident	O
population	O
of	O
approximately	O
8,000	O
,	O
but	O
around	O
320,000	O
people	O
work	O
there	O
,	O
mainly	O
in	O
the	O
financial	O
services	O
sector	O
.	O
The	O
City	O
of	O
London	LOCATION
is	O
England	LOCATION
's	O
smallest	O
ceremonial	O
county	O
by	O
area	O
and	O
population	O
,	O
and	O
the	O
fourth	O
most	O
densely	O
populated	O
.	O
The	O
size	O
of	O
the	O
City	O
was	O
constrained	O
by	O
a	O
defensive	O
perimeter	O
wall	O
,	O
known	O
as	O
London	LOCATION
Wall	O
,	O
which	O
was	O
built	O
by	O
the	O
Romans	O
in	O
the	O
late	O
2nd	O
century	O
to	O
protect	O
their	O
strategic	O
port	O
city	O
.	O
However	O
the	O
boundaries	O
of	O
the	O
City	O
of	O
London	LOCATION
no	O
longer	O
coincide	O
with	O
the	O
old	O
city	O
wall	O
,	O
as	O
the	O
City	O
expanded	O
its	O
jurisdiction	O
slightly	O
over	O
time	O
.	O
During	O
the	O
medieval	O
era	O
,	O
the	O
City	O
's	O
jurisdiction	O
expanded	O
westwards	O
,	O
crossing	O
the	O
historic	O
western	O
border	O
of	O
the	O
original	O
settlement	O
--	O
the	O
River	O
Fleet	O
--	O
along	O
Fleet	LOCATION
Street	LOCATION
to	O
Temple	O
Bar	O
.	O
The	O
City	O
also	O
took	O
in	O
the	O
other	O
"	O
City	O
bars	O
"	O
which	O
were	O
situated	O
just	O
beyond	O
the	O
old	O
walled	O
area	O
,	O
such	O
as	O
at	O
Holborn	LOCATION
,	O
Aldersgate	O
,	O
Bishopsgate	ORGANIZATION
and	O
Aldgate	LOCATION
.	O
These	O
were	O
the	O
important	O
entrances	O
to	O
the	O
City	O
and	O
their	O
control	O
was	O
vital	O
in	O
maintaining	O
the	O
City	O
's	O
special	O
privileges	O
over	O
certain	O
trades	O
.	O
A	O
section	O
near	O
the	O
Museum	O
of	O
London	LOCATION
was	O
revealed	O
after	O
the	O
devastation	O
of	O
an	O
air	O
raid	O
on	O
29	O
December	O
1940	O
at	O
the	O
height	O
of	O
the	O
Blitz	O
.	O
In	O
the	O
process	O
the	O
City	O
lost	O
small	O
parcels	O
of	O
land	O
,	O
though	O
there	O
was	O
an	O
overall	O
net	O
gain	O
of	O
land	O
.	O
Most	O
notably	O
,	O
the	O
changes	O
placed	O
the	O
(	O
then	O
recently	O
developed	O
)	O
Broadgate	O
estate	O
entirely	O
in	O
the	O
City	O
.	O
Today	O
it	O
forms	O
part	O
of	O
the	O
London	LOCATION
Borough	O
of	O
Southwark	LOCATION
.	O
The	O
Tower	O
of	O
London	LOCATION
has	O
always	O
been	O
outside	O
the	O
City	O
and	O
today	O
comes	O
under	O
the	O
London	LOCATION
Borough	O
of	O
Tower	O
Hamlets	O
.	O
Beginning	O
in	O
the	O
west	O
,	O
where	O
the	O
City	O
borders	O
Westminster	PERSON
,	O
the	O
boundary	O
crosses	O
the	O
Victoria	O
Embankment	O
from	O
the	O
Thames	LOCATION
,	O
passes	O
to	O
the	O
west	O
of	O
Middle	LOCATION
Temple	LOCATION
,	O
then	O
turns	O
for	O
a	O
short	O
distance	O
along	O
Strand	O
and	O
then	O
north	O
up	O
Chancery	LOCATION
Lane	LOCATION
,	O
where	O
it	O
borders	O
Camden	LOCATION
.	O
As	O
it	O
crosses	O
Farringdon	LOCATION
Road	LOCATION
it	O
becomes	O
the	O
boundary	O
with	O
Islington	LOCATION
.	O
The	O
boundary	O
then	O
turns	O
south	O
at	O
Norton	LOCATION
Folgate	LOCATION
and	O
becomes	O
the	O
border	O
with	O
Tower	O
Hamlets	O
.	O
It	O
then	O
turns	O
south-west	O
,	O
crossing	O
the	O
Minories	LOCATION
,	O
so	O
as	O
to	O
exclude	O
the	O
Tower	O
of	O
London	LOCATION
from	O
the	O
City	O
,	O
and	O
then	O
reaches	O
the	O
river	O
.	O
The	O
boundaries	O
of	O
the	O
City	O
are	O
marked	O
by	O
black	O
bollards	O
bearing	O
the	O
City	O
's	O
emblem	O
,	O
and	O
at	O
major	O
entrances	O
,	O
such	O
as	O
at	O
Temple	ORGANIZATION
Bar	ORGANIZATION
on	ORGANIZATION
Fleet	ORGANIZATION
Street	ORGANIZATION
,	O
a	O
grander	O
monument	O
,	O
with	O
a	O
dragon	O
facing	O
outwards	O
,	O
marks	O
the	O
boundary	O
.	O
Since	O
the	O
1990s	O
the	O
eastern	O
fringe	O
of	O
the	O
City	O
,	O
extending	O
into	O
Hackney	PERSON
and	O
Tower	O
Hamlets	O
,	O
has	O
increasingly	O
been	O
a	O
focus	O
for	O
large	O
office	O
developments	O
due	O
to	O
the	O
availability	O
of	O
large	O
sites	O
there	O
compared	O
to	O
within	O
the	O
City	O
.	O
It	O
is	O
believed	O
that	O
Roman	O
London	LOCATION
was	O
established	O
as	O
a	O
trading	O
port	O
by	O
merchants	O
on	O
the	O
tidal	O
Thames	O
around	O
47	O
AD	O
.	O
The	O
new	O
settlement	O
and	O
port	O
was	O
centred	O
where	O
the	O
shallow	O
valley	O
of	O
the	O
Walbrook	PERSON
meets	O
the	O
Thames	LOCATION
.	O
However	O
in	O
AD	O
60	O
or	O
61	O
,	O
little	O
more	O
than	O
ten	O
years	O
after	O
Londinium	PERSON
was	O
founded	O
,	O
it	O
was	O
sacked	O
by	O
the	O
Iceni	ORGANIZATION
,	O
led	O
by	O
the	O
their	O
queen	O
Boudica	PERSON
.	O
Londinium	O
was	O
rebuilt	O
as	O
a	O
planned	O
settlement	O
(	O
a	O
civitas	O
)	O
soon	O
after	O
and	O
the	O
new	O
town	O
was	O
prosperous	O
and	O
grew	O
to	O
become	O
the	O
largest	O
settlement	O
in	O
Roman	LOCATION
Britain	LOCATION
by	O
the	O
end	O
of	O
the	O
first	O
century	O
.	O
By	O
the	O
beginning	O
of	O
the	O
2nd	O
century	O
,	O
Londinium	PERSON
had	O
replaced	O
Colchester	LOCATION
as	O
the	O
capital	O
of	O
Roman	O
Britain	O
(	O
"	O
Britannia	O
"	O
)	O
.	O
However	O
already	O
by	O
the	O
time	O
of	O
the	O
construction	O
of	O
the	O
London	O
Wall	O
,	O
the	O
city	O
's	O
fortunes	O
were	O
in	O
decline	O
,	O
with	O
problems	O
of	O
plague	O
and	O
fire	O
.	O
The	O
Roman	O
Empire	O
entered	O
a	O
long	O
period	O
of	O
instability	O
and	O
decline	O
,	O
including	O
for	O
example	O
the	O
Carausian	O
Revolt	O
in	O
Britain	LOCATION
.	O
The	O
name	O
survives	O
today	O
as	O
Aldwych	O
(	O
the	O
"	O
old	O
market-place	O
"	O
)	O
,	O
now	O
a	O
name	O
given	O
to	O
a	O
street	O
and	O
an	O
area	O
which	O
lies	O
in	O
the	O
City	O
of	O
Westminster	O
between	O
Westminster	LOCATION
and	O
the	O
City	O
of	O
London	LOCATION
.	O
He	O
eventually	O
crossed	O
the	O
River	O
Thames	O
at	O
Wallingford	LOCATION
,	O
pillaging	O
the	O
land	O
as	O
he	O
went	O
.	O
The	O
City	O
was	O
not	O
covered	O
by	O
the	O
Domesday	O
Book	O
.	O
This	O
'	O
commune	O
'	O
was	O
the	O
origin	O
of	O
the	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
Corporation	ORGANIZATION
and	O
the	O
citizens	O
gained	O
the	O
right	O
to	O
appoint	O
,	O
with	O
the	O
king	O
's	O
consent	O
,	O
a	O
Mayor	O
in	O
1189	O
and	O
to	O
directly	O
elect	O
the	O
Mayor	O
from	O
1215	O
.	O
There	O
was	O
a	O
folkmoot	O
for	O
the	O
whole	O
of	O
the	O
city	O
held	O
at	O
the	O
outdoor	O
cross	O
of	O
St	ORGANIZATION
Paul	ORGANIZATION
's	ORGANIZATION
Cathedral	ORGANIZATION
.	O
Many	O
of	O
the	O
medieval	O
positions	O
and	O
traditions	O
continue	O
to	O
the	O
present	O
day	O
,	O
demonstrating	O
the	O
unique	O
institution	O
which	O
the	O
City	O
,	O
and	O
its	O
Corporation	O
,	O
is	O
.	O
The	O
City	O
was	O
burned	O
severely	O
on	O
a	O
number	O
of	O
occasions	O
,	O
the	O
worst	O
being	O
in	O
1123	O
and	O
then	O
again	O
(	O
and	O
more	O
famously	O
)	O
in	O
the	O
Great	O
Fire	O
of	O
London	LOCATION
in	O
1666	O
.	O
Both	O
of	O
these	O
fires	O
were	O
referred	O
to	O
as	O
the	O
Great	O
Fire	O
.	O
After	O
the	O
fire	O
of	O
1666	O
,	O
a	O
number	O
of	O
plans	O
were	O
drawn	O
up	O
to	O
remodel	O
the	O
City	O
and	O
its	O
street	O
pattern	O
into	O
a	O
renaissance-style	O
city	O
with	O
planned	O
urban	O
blocks	O
,	O
squares	O
and	O
boulevards	O
.	O
The	O
urban	O
area	O
expanded	O
beyond	O
the	O
borders	O
of	O
the	O
City	O
of	O
London	LOCATION
,	O
most	O
notably	O
during	O
this	O
period	O
towards	O
the	O
West	O
End	O
and	O
Westminster	O
.	O
In	O
1708	O
Christopher	PERSON
Wren	PERSON
's	O
masterpiece	O
,	O
St.	ORGANIZATION
Paul	ORGANIZATION
's	ORGANIZATION
Cathedral	ORGANIZATION
,	O
was	O
completed	O
on	O
his	O
birthday	O
.	O
Expansion	O
continued	O
and	O
became	O
more	O
rapid	O
by	O
the	O
beginning	O
of	O
the	O
19th	O
century	O
,	O
with	O
London	LOCATION
growing	O
in	O
all	O
directions	O
.	O
To	O
the	O
East	O
the	O
Port	O
of	O
London	LOCATION
grew	O
rapidly	O
during	O
the	O
century	O
,	O
with	O
the	O
construction	O
of	O
many	O
docks	O
,	O
needed	O
as	O
the	O
Thames	LOCATION
at	O
the	O
City	O
could	O
not	O
cope	O
with	O
the	O
volume	O
of	O
trade	O
.	O
The	O
arrival	O
of	O
the	O
railways	O
and	O
the	O
Tube	O
meant	O
that	O
London	LOCATION
could	O
expand	O
over	O
a	O
much	O
greater	O
area	O
.	O
By	O
the	O
mid-19th	O
century	O
,	O
with	O
London	LOCATION
still	O
rapidly	O
expanding	O
in	O
population	O
and	O
area	O
,	O
the	O
City	O
had	O
already	O
become	O
only	O
a	O
small	O
part	O
of	O
the	O
wider	O
metropolis	O
.	O
An	O
attempt	O
was	O
made	O
in	O
1894	O
to	O
amalgamate	O
the	O
City	O
and	O
the	O
surrounding	O
County	LOCATION
of	O
London	LOCATION
,	O
but	O
it	O
did	O
not	O
succeed	O
.	O
The	O
City	O
of	O
London	LOCATION
therefore	O
survived	O
,	O
and	O
does	O
so	O
to	O
this	O
day	O
,	O
despite	O
its	O
situation	O
within	O
the	O
London	LOCATION
conurbation	O
and	O
numerous	O
local	O
government	O
reforms	O
.	O
Today	O
it	O
is	O
included	O
wholly	O
in	O
the	O
Cities	O
of	O
London	LOCATION
and	O
Westminster	LOCATION
constituency	O
,	O
and	O
statute	O
requires	O
that	O
it	O
not	O
be	O
divided	O
between	O
two	O
neighbouring	O
areas	O
.	O
The	O
largest	O
residential	O
section	O
of	O
the	O
City	O
today	O
is	O
the	O
Barbican	O
Estate	O
,	O
constructed	O
between	O
1965	O
and	O
1976	O
.	O
Here	O
a	O
major	O
proportion	O
of	O
the	O
City	O
's	O
population	O
now	O
live	O
.	O
Whilst	O
St	PERSON
Paul	PERSON
's	O
Cathedral	O
survived	O
the	O
onslaught	O
,	O
large	O
swathes	O
of	O
the	O
City	O
did	O
not	O
.	O
A	O
major	O
rebuilding	O
programme	O
therefore	O
occurred	O
in	O
the	O
decades	O
following	O
the	O
war	O
,	O
in	O
some	O
parts	O
(	O
such	O
as	O
at	O
the	O
Barbican	O
)	O
dramatically	O
altering	O
the	O
City	O
's	O
urban	O
landscape	O
.	O
The	O
destruction	O
of	O
the	O
City	O
's	O
older	O
historic	O
fabric	O
however	O
allowed	O
,	O
and	O
continues	O
to	O
allow	O
,	O
the	O
construction	O
of	O
modern	O
and	O
larger-scale	O
developments	O
in	O
parts	O
of	O
the	O
City	O
,	O
whereas	O
in	O
those	O
parts	O
not	O
so	O
badly	O
affected	O
by	O
bomb	O
damage	O
,	O
the	O
City	O
retains	O
its	O
older	O
character	O
of	O
smaller	O
buildings	O
.	O
The	O
street	O
pattern	O
,	O
which	O
is	O
still	O
largely	O
medieval	O
,	O
was	O
altered	O
slightly	O
in	O
certain	O
places	O
,	O
although	O
there	O
is	O
a	O
more	O
recent	O
trend	O
of	O
reversing	O
some	O
of	O
the	O
post-war	O
modernist	O
changes	O
made	O
,	O
such	O
as	O
at	O
Paternoster	LOCATION
Square	LOCATION
.	O
The	O
City	O
has	O
its	O
own	O
flag	O
and	O
coat	O
of	O
arms	O
.	O
London	LOCATION
is	O
the	O
capital	O
city	O
of	O
England	LOCATION
and	O
the	O
United	LOCATION
Kingdom	LOCATION
,	O
however	O
the	O
City	O
of	O
London	LOCATION
by	O
itself	O
is	O
not	O
regarded	O
as	O
being	O
the	O
capital	O
.	O
Westminster	O
is	O
a	O
city	O
in	O
its	O
own	O
right	O
--	O
the	O
City	O
of	O
Westminster	O
.	O
The	O
City	O
falls	O
within	O
the	O
historic	O
county	O
of	O
Middlesex	LOCATION
,	O
though	O
it	O
was	O
always	O
largely	O
independent	O
from	O
it	O
.	O
The	O
hundred	O
of	O
Ossulstone	LOCATION
bounded	O
the	O
City	LOCATION
to	O
the	O
west	O
,	O
north	O
and	O
east	O
.	O
To	O
the	O
south	O
,	O
on	O
the	O
other	O
side	O
of	O
the	O
River	LOCATION
Thames	LOCATION
,	O
and	O
including	O
Southwark	LOCATION
,	O
is	O
the	O
historic	O
county	O
of	O
Surrey	LOCATION
.	O
Some	O
of	O
the	O
extra	O
accommodation	O
is	O
in	O
small	O
pre-World	O
War	O
II	O
listed	O
buildings	O
,	O
which	O
are	O
not	O
suitable	O
for	O
occupation	O
by	O
the	O
large	O
companies	O
which	O
now	O
provide	O
much	O
of	O
the	O
City	O
's	O
employment	O
.	O
Since	O
the	O
1990s	O
,	O
the	O
City	O
has	O
diversified	O
away	O
from	O
near	O
exclusive	O
office	O
use	O
in	O
other	O
ways	O
.	O
For	O
example	O
,	O
several	O
hotels	O
and	O
the	O
City	O
's	O
first	O
department	O
store	O
have	O
opened	O
.	O
However	O
,	O
large	O
sections	O
of	O
the	O
City	LOCATION
remain	O
very	O
quiet	O
at	O
weekends	O
,	O
especially	O
those	O
areas	O
in	O
the	O
eastern	O
section	O
of	O
the	O
City	O
,	O
and	O
it	O
is	O
quite	O
common	O
to	O
find	O
pubs	O
and	O
cafes	O
closed	O
on	O
these	O
days	O
.	O
A	O
number	O
of	O
skyscrapers	O
have	O
been	O
built	O
in	O
recent	O
years	O
in	O
the	O
City	O
of	O
London	LOCATION
and	O
further	O
skyscrapers	O
are	O
either	O
under	O
construction	O
or	O
planned	O
to	O
be	O
built	O
.	O
Census	O
figures	O
(	O
and	O
estimates	O
for	O
some	O
of	O
the	O
most	O
recent	O
years	O
)	O
for	O
the	O
resident	O
population	O
of	O
the	O
City	O
of	O
London	LOCATION
:	O
The	LOCATION
City	LOCATION
vies	O
with	O
New	LOCATION
York	LOCATION
City	LOCATION
as	O
the	O
financial	O
capital	O
of	O
the	O
world	O
,	O
many	O
banking	O
and	O
insurance	O
institutions	O
have	O
their	O
headquarters	O
there	O
.	O
In	O
the	O
City	O
are	O
the	O
London	ORGANIZATION
Stock	ORGANIZATION
Exchange	ORGANIZATION
(	O
shares	O
and	O
bonds	O
)	O
,	O
Lloyd	ORGANIZATION
's	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
(	O
insurance	O
)	O
and	O
the	O
Bank	ORGANIZATION
of	O
England	LOCATION
.	O
The	O
Alternative	O
Investment	O
Market	O
,	O
a	O
market	O
for	O
trades	O
in	O
equities	O
of	O
smaller	O
firms	O
,	O
is	O
a	O
recent	O
development	O
.	O
Since	O
1991	O
Canary	ORGANIZATION
Wharf	ORGANIZATION
,	O
a	O
few	O
miles	O
east	O
of	O
the	O
City	LOCATION
in	O
Tower	O
Hamlets	O
,	O
has	O
become	O
a	O
second	O
centre	O
for	O
London	LOCATION
's	O
financial	O
services	O
industry	O
and	O
houses	O
many	O
banks	O
and	O
other	O
institutions	O
formerly	O
located	O
in	O
the	O
Square	LOCATION
Mile	LOCATION
.	O
This	O
development	O
does	O
not	O
appear	O
to	O
have	O
damaged	O
the	O
City	O
:	O
growth	O
has	O
continued	O
in	O
both	O
locations	O
.	O
In	O
2008	O
,	O
the	O
City	O
of	O
London	LOCATION
accounted	O
for	O
4	O
%	O
of	O
UK	LOCATION
GDP	O
.	O
London	LOCATION
is	O
the	O
world	O
's	O
greatest	O
foreign	O
exchange	O
market	O
,	O
with	O
much	O
of	O
the	O
trade	O
conducted	O
in	O
the	O
City	O
of	O
London	LOCATION
.	O
Of	O
the	O
$	O
3.98	O
trillion	O
daily	O
global	O
turnover	O
,	O
as	O
measured	O
in	O
2007	O
,	O
trading	O
in	O
London	LOCATION
accounted	O
for	O
around	O
$	O
1.36	O
trillion	O
,	O
or	O
34.1	O
%	O
of	O
the	O
total	O
.	O
Whilst	O
the	O
financial	O
sector	O
,	O
and	O
related	O
businesses	O
and	O
institutions	O
,	O
continue	O
to	O
dominate	O
the	O
City	O
,	O
the	O
City	O
's	O
economy	O
is	O
not	O
limited	O
to	O
that	O
sector	O
.	O
The	O
legal	O
profession	O
has	O
a	O
strong	O
presence	O
in	O
the	O
City	O
,	O
especially	O
in	O
the	O
western	O
half	O
.	O
The	O
City	O
has	O
a	O
number	O
of	O
visitor	O
attractions	O
,	O
mainly	O
based	O
on	O
its	O
historic	O
heritage	O
as	O
well	O
as	O
the	O
Barbican	O
Centre	O
and	O
adjacent	O
Museum	O
of	O
London	LOCATION
,	O
though	O
tourism	O
is	O
not	O
at	O
present	O
a	O
major	O
contributor	O
to	O
the	O
City	O
's	O
economy	O
or	O
character	O
.	O
The	O
City	O
has	O
many	O
pubs	O
,	O
bars	O
and	O
restaurants	O
,	O
and	O
the	O
"	O
night-time	O
"	O
economy	O
does	O
feature	O
in	O
the	O
Bishopsgate	ORGANIZATION
area	O
,	O
towards	O
Shoreditch	LOCATION
.	O
In	O
the	O
east	O
of	O
the	O
City	O
is	O
Leadenhall	O
Market	O
,	O
a	O
fresh	O
food	O
market	O
that	O
is	O
also	O
a	O
visitor	O
attraction	O
.	O
Historically	O
its	O
system	O
of	O
government	O
was	O
not	O
unusual	O
,	O
but	O
it	O
was	O
not	O
reformed	O
by	O
the	O
Municipal	O
Reform	O
Act	O
1835	O
and	O
little	O
changed	O
by	O
later	O
reforms	O
.	O
It	O
is	O
administered	O
by	O
the	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
Corporation	ORGANIZATION
,	O
headed	O
by	O
the	O
Lord	O
Mayor	O
of	O
London	LOCATION
(	O
not	O
the	O
same	O
as	O
the	O
more	O
recently	O
created	O
position	O
of	O
Mayor	O
of	O
London	LOCATION
)	O
,	O
which	O
is	O
responsible	O
for	O
a	O
number	O
of	O
functions	O
and	O
owns	O
a	O
number	O
of	O
locations	O
beyond	O
the	O
City	O
's	O
boundaries	O
.	O
The	O
City	O
is	O
made	O
up	O
of	O
25	O
wards	O
,	O
which	O
had	O
their	O
boundaries	O
changed	O
in	O
2003	O
,	O
though	O
the	O
number	O
of	O
wards	O
and	O
their	O
names	O
did	O
not	O
change	O
.	O
They	O
can	O
be	O
described	O
as	O
being	O
both	O
electoral/political	O
divisions	O
and	O
permanent	O
ceremonial	O
,	O
geographic	O
and	O
administrative	O
entities	O
within/sub-divisions	O
of	O
the	O
City	O
.	O
Following	O
changes	O
to	O
the	O
City	O
of	O
London	LOCATION
's	O
boundary	O
in	O
1994	O
and	O
later	O
reform	O
of	O
the	O
business	O
vote	O
in	O
the	O
City	O
,	O
a	O
major	O
boundary	O
and	O
electoral	O
representation	O
revision	O
took	O
place	O
to	O
the	O
wards	O
in	O
2003	O
.	O
Particular	O
churches	O
,	O
livery	O
company	O
halls	O
and	O
other	O
historic	O
buildings	O
and	O
structures	O
are	O
associated	O
with	O
specific	O
wards	O
,	O
such	O
as	O
St	ORGANIZATION
Paul	ORGANIZATION
's	O
Cathedral	O
with	O
Castle	PERSON
Baynard	PERSON
,	O
or	O
London	LOCATION
Bridge	LOCATION
with	O
Bridge	O
.	O
They	O
are	O
:	O
Portsoken	LOCATION
,	O
Queenhithe	O
,	O
Aldersgate	O
and	O
Cripplegate	O
.	O
The	O
City	O
has	O
a	O
unique	O
electoral	O
system	O
.	O
Most	O
of	O
its	O
voters	O
are	O
representatives	O
of	O
businesses	O
and	O
other	O
bodies	O
that	O
occupy	O
premises	O
in	O
the	O
City	O
.	O
In	O
elections	O
,	O
both	O
the	O
businesses	O
based	O
in	O
the	O
City	O
and	O
the	O
residents	O
of	O
the	O
City	O
vote	O
.	O
The	O
principal	O
justification	O
for	O
the	O
non-resident	O
vote	O
is	O
that	O
about	O
450,000	O
non-residents	O
constitute	O
the	O
city	O
's	O
day-time	O
population	O
and	O
use	O
most	O
of	O
its	O
services	O
,	O
far	O
outnumbering	O
the	O
City	O
's	O
residents	O
,	O
who	O
are	O
fewer	O
than	O
10,000	O
.	O
The	O
business	O
vote	O
was	O
abolished	O
in	O
all	O
other	O
UK	LOCATION
local	O
authority	O
elections	O
in	O
1969	O
.	O
The	O
present	O
system	O
is	O
seen	O
by	O
some	O
as	O
undemocratic	O
[	O
citation	O
needed	O
]	O
,	O
but	O
adopting	O
a	O
more	O
conventional	O
system	O
would	O
place	O
the	O
7,800	O
residents	O
of	O
the	O
City	O
in	O
control	O
of	O
the	O
local	O
planning	O
and	O
other	O
functions	O
of	O
a	O
major	O
financial	O
capital	O
that	O
provides	O
most	O
of	O
its	O
services	O
to	O
hundreds	O
of	O
thousands	O
of	O
non-residents	O
.	O
One	O
proposal	O
floated	O
as	O
a	O
possible	O
reform	O
is	O
to	O
allow	O
those	O
who	O
work	O
in	O
the	O
City	O
to	O
each	O
have	O
a	O
direct	O
individual	O
vote	O
,	O
rather	O
than	O
businesses	O
being	O
represented	O
by	O
appointed	O
voters	O
.	O
Inner	O
Temple	O
and	O
Middle	O
Temple	O
(	O
which	O
neighbour	O
each	O
other	O
)	O
are	O
two	O
of	O
the	O
few	O
remaining	O
liberties	O
,	O
an	O
old	O
name	O
for	O
a	O
geographic	O
division	O
.	O
They	O
are	O
independent	O
extra-parochial	O
areas	O
,	O
historically	O
not	O
governed	O
by	O
the	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
Corporation	ORGANIZATION
(	O
and	O
are	O
today	O
regarded	O
as	O
local	O
authorities	O
for	O
most	O
purposes	O
)	O
and	O
equally	O
outside	O
the	O
ecclesiastical	O
jurisdiction	O
of	O
the	O
Bishop	O
of	O
London	LOCATION
.	O
They	O
geographically	O
fall	O
within	O
the	O
boundaries	O
and	O
liberties	O
of	O
the	O
City	O
,	O
but	O
can	O
be	O
thought	O
of	O
as	O
independent	O
enclaves	O
.	O
They	O
are	O
both	O
part	O
of	O
the	O
Farringdon	O
Without	O
ward	O
of	O
the	O
City	O
.	O
The	O
City	O
of	O
London	LOCATION
did	O
however	O
form	O
a	O
single	O
civil	O
parish	O
from	O
1907	O
,	O
until	O
it	O
was	O
abolished	O
on	O
1	O
April	O
1965	O
(	O
along	O
with	O
all	O
civil	O
parishes	O
in	O
the	O
then	O
newly-created	O
Greater	LOCATION
London	LOCATION
administrative	O
area	O
)	O
.	O
The	O
rest	O
of	O
Greater	LOCATION
London	LOCATION
is	O
policed	O
by	O
the	O
Metropolitan	ORGANIZATION
Police	ORGANIZATION
Service	ORGANIZATION
,	O
based	O
at	O
New	LOCATION
Scotland	LOCATION
Yard	LOCATION
.	O
The	O
City	O
of	O
London	LOCATION
has	O
one	O
hospital	O
,	O
St	ORGANIZATION
Bartholomew	ORGANIZATION
's	O
Hospital	O
.	O
Founded	O
in	O
1123	O
and	O
commonly	O
known	O
as	O
'	O
Barts	O
'	O
,	O
the	O
hospital	O
is	O
at	O
Smithfield	ORGANIZATION
,	O
and	O
is	O
undergoing	O
a	O
long-awaited	O
regeneration	O
after	O
many	O
doubts	O
as	O
to	O
it	O
continuing	O
in	O
use	O
during	O
the	O
1990s	O
.	O
The	O
City	O
is	O
the	O
third	O
largest	O
UK	LOCATION
funding-patron	O
of	O
the	O
arts	O
.	O
It	O
oversees	O
the	O
Barbican	O
Centre	O
and	O
subsidises	O
several	O
important	O
performing	O
arts	O
companies	O
.	O
Three	O
National	O
Rail	O
termini	O
stations	O
are	O
located	O
in	O
the	O
City	O
,	O
at	O
Liverpool	LOCATION
Street	LOCATION
,	O
Fenchurch	ORGANIZATION
Street	ORGANIZATION
and	O
Cannon	LOCATION
Street	LOCATION
,	O
and	O
London	LOCATION
Bridge	LOCATION
station	O
is	O
on	O
the	O
other	O
end	O
of	O
London	LOCATION
Bridge	LOCATION
in	O
Southwark	LOCATION
.	O
As	O
well	O
as	O
being	O
an	O
Underground	O
station	O
,	O
Moorgate	LOCATION
is	O
the	O
terminus	O
of	O
the	O
Northern	LOCATION
City	LOCATION
Line	LOCATION
.	O
The	O
whole	O
of	O
the	O
City	O
of	O
London	LOCATION
lies	O
in	O
Travelcard	ORGANIZATION
Zone	ORGANIZATION
1	O
.	O
The	O
high	O
capacity	O
west-east	O
Crossrail	LOCATION
railway	O
line	O
,	O
which	O
is	O
scheduled	O
to	O
be	O
completed	O
by	O
2017	O
,	O
will	O
run	O
underground	O
across	O
the	O
north	O
of	O
the	O
City	O
,	O
with	O
two	O
stations	O
at	O
Farringdon	ORGANIZATION
(	O
linked	O
also	O
to	O
Barbican	O
)	O
and	O
Liverpool	ORGANIZATION
Street	ORGANIZATION
(	O
linked	O
also	O
to	O
Moorgate	LOCATION
)	O
.	O
The	O
national	O
A1	O
,	O
A3	O
,	O
and	O
A4	O
road	O
routes	O
begin	O
in	O
the	O
City	O
of	O
London	LOCATION
.	O
The	O
following	O
bridges	O
,	O
listed	O
west	O
to	O
east	O
(	O
heading	O
downstream	O
)	O
,	O
cross	O
the	O
River	O
Thames	O
from	O
the	O
City	O
of	O
London	LOCATION
to	O
the	O
southern	O
bank	O
:	O
Blackfriars	LOCATION
Bridge	LOCATION
,	O
Blackfriars	LOCATION
Railway	LOCATION
Bridge	LOCATION
,	O
Millennium	O
Bridge	O
(	O
footbridge	O
)	O
,	O
Southwark	LOCATION
Bridge	LOCATION
,	O
Cannon	LOCATION
Street	LOCATION
Railway	LOCATION
Bridge	LOCATION
and	O
London	LOCATION
Bridge	LOCATION
.	O
The	O
famous	O
landmark	O
,	O
the	O
Tower	LOCATION
Bridge	LOCATION
,	O
is	O
not	O
in	O
the	O
City	O
of	O
London	LOCATION
.	O
The	O
City	O
,	O
like	O
most	O
of	O
central	O
London	LOCATION
,	O
is	O
well	O
served	O
by	O
buses	O
,	O
including	O
night	O
buses	O
.	O
One	O
London	ORGANIZATION
River	ORGANIZATION
Services	ORGANIZATION
pier	O
exists	O
on	O
the	O
Thames	LOCATION
along	O
the	O
City	O
of	O
London	LOCATION
shore	O
,	O
the	O
Blackfriars	O
Millennium	O
Pier	O
,	O
though	O
the	O
Tower	O
Millennium	O
Pier	O
lies	O
adjacent	O
to	O
the	O
City	O
's	O
boundary	O
,	O
near	O
the	O
Tower	O
of	O
London	LOCATION
.	O
One	O
of	O
the	O
Port	O
of	O
London	LOCATION
's	O
25	O
safeguarded	O
wharfs	O
in	O
central	O
London	LOCATION
,	O
Walbrook	LOCATION
Wharf	LOCATION
,	O
is	O
located	O
on	O
the	O
City	O
of	O
London	LOCATION
's	O
shore	O
,	O
adjacent	O
to	O
Cannon	LOCATION
Street	LOCATION
station	O
,	O
and	O
is	O
used	O
by	O
the	O
Corporation	O
of	O
London	LOCATION
to	O
transfer	O
waste	O
via	O
the	O
river	O
.	O
Before	O
then	O
,	O
Tower	O
Pier	O
is	O
to	O
be	O
extended	O
.	O
A	O
public	O
riverside	O
walk	O
exists	O
along	O
the	O
entire	O
shoreline	O
of	O
the	O
City	O
,	O
having	O
been	O
instigated	O
in	O
stages	O
in	O
recent	O
years	O
,	O
with	O
the	O
only	O
remaining	O
section	O
not	O
running	O
along	O
the	O
river	O
being	O
a	O
short	O
stretch	O
at	O
Queenhithe	LOCATION
.	O
The	O
walk	O
runs	O
along	O
Walbrook	LOCATION
Wharf	LOCATION
and	O
is	O
only	O
closed	O
to	O
pedestrians	O
at	O
this	O
point	O
when	O
waste	O
is	O
being	O
transferred	O
onto	O
barges	O
.	O
City	O
residents	O
may	O
send	O
their	O
children	O
to	O
schools	O
in	O
neighbouring	O
Local	ORGANIZATION
Education	ORGANIZATION
Authorities	ORGANIZATION
,	O
such	O
as	O
Islington	LOCATION
,	O
Tower	O
Hamlets	O
,	O
Westminster	PERSON
and	O
Southwark	LOCATION
.	O
The	O
City	O
controls	O
three	O
very	O
well	O
regarded	O
independent	O
schools	O
,	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
School	ORGANIZATION
(	O
a	O
boys	O
school	O
)	O
and	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
School	ORGANIZATION
for	ORGANIZATION
Girls	ORGANIZATION
(	O
girls	O
)	O
which	O
are	O
in	O
the	O
City	O
itself	O
,	O
and	O
the	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
Freemen	ORGANIZATION
's	O
School	O
(	O
co-educational	O
day	O
and	O
boarding	O
)	O
which	O
is	O
in	O
Ashtead	LOCATION
,	O
Surrey	LOCATION
.	O
The	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
School	ORGANIZATION
for	ORGANIZATION
Girls	ORGANIZATION
has	O
its	O
own	O
preparatory	O
department	O
for	O
entrance	O
at	O
age	O
seven	O
.	O
It	O
is	O
also	O
the	O
principal	O
sponsor	O
of	O
the	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
Academy	ORGANIZATION
which	O
is	O
based	O
in	O
Southwark	LOCATION
.	O
The	O
College	ORGANIZATION
of	ORGANIZATION
Law	ORGANIZATION
has	O
its	O
London	LOCATION
campus	O
in	O
Moorgate	LOCATION
.	O
These	O
range	O
from	O
formal	O
gardens	O
such	O
as	O
the	O
one	O
in	O
Finsbury	O
Circus	O
,	O
containing	O
a	O
bowling	O
green	O
and	O
bandstand	O
,	O
to	O
churchyards	O
such	O
as	O
one	O
belonging	O
to	O
the	O
church	O
of	O
St	LOCATION
Olave	LOCATION
Hart	LOCATION
Street	LOCATION
,	O
to	O
water	O
features	O
and	O
artwork	O
found	O
in	O
some	O
of	O
the	O
courtyards	O
and	O
pedestrianised	O
lanes	O
.	O
The	O
Thames	O
and	O
its	O
riverside	O
walks	O
are	O
increasingly	O
being	O
valued	O
as	O
open	O
space	O
for	O
the	O
City	O
and	O
in	O
recent	O
years	O
efforts	O
have	O
been	O
made	O
to	O
increase	O
the	O
ability	O
for	O
pedestrians	O
to	O
access	O
and	O
walk	O
along	O
the	O
river	O
.	O
The	O
City	O
has	O
its	O
own	O
territorial	O
police	O
force	O
,	O
the	O
City	ORGANIZATION
of	ORGANIZATION
London	ORGANIZATION
Police	ORGANIZATION
,	O
which	O
is	O
a	O
separate	O
organisation	O
to	O
the	O
Metropolitan	ORGANIZATION
Police	ORGANIZATION
Service	ORGANIZATION
which	O
covers	O
the	O
rest	O
of	O
Greater	LOCATION
London	LOCATION
.	O
City	O
police	O
sergeants	O
and	O
constables	O
wear	O
crested	O
helmets	O
whilst	O
on	O
foot	O
patrol	O
.	O
The	O
City	O
's	O
position	O
as	O
the	O
U.K.	LOCATION
's	O
financial	O
centre	O
and	O
a	O
critical	O
part	O
of	O
the	O
country	O
's	O
economy	O
,	O
contributing	O
about	O
2.5	O
%	O
of	O
the	O
UK	LOCATION
's	O
gross	O
national	O
product	O
,	O
has	O
resulted	O
in	O
it	O
becoming	O
a	O
target	O
for	O
political	O
violence	O
.	O
The	O
Provisional	O
IRA	ORGANIZATION
exploded	O
several	O
bombs	O
in	O
the	O
City	O
in	O
the	O
early	O
1990s	O
,	O
including	O
the	O
1993	O
Bishopsgate	ORGANIZATION
bombing	O
.	O
The	O
area	O
is	O
also	O
spoken	O
of	O
as	O
a	O
possible	O
target	O
for	O
al-Qaeda	O
.	O
There	O
is	O
one	O
fire	O
station	O
within	O
the	O
City	O
,	O
at	O
Dowgate	LOCATION
,	O
with	O
one	O
pumping	O
appliance	O
.	O
Within	O
the	O
City	O
the	O
first	O
fire	O
engine	O
is	O
in	O
attendance	O
in	O
roughly	O
five	O
minutes	O
on	O
average	O
,	O
the	O
second	O
when	O
required	O
in	O
a	O
little	O
over	O
five	O
and	O
a	O
half	O
minutes	O
.	O
No	O
one	O
has	O
died	O
in	O
an	O
event	O
arising	O
from	O
a	O
fire	O
in	O
the	O
City	O
in	O
the	O
last	O
four	O
years	O
prior	O
to	O
2007	O
.	O
A	O
growing	O
number	O
of	O
tall	O
buildings	O
and	O
skyscrapers	O
exist	O
in	O
the	O
City	O
,	O
principally	O
for	O
use	O
by	O
the	O
financial	O
sector	O
.	O
Almost	O
all	O
are	O
situated	O
in	O
the	O
eastern	O
side	O
of	O
the	O
Square	LOCATION
Mile	LOCATION
,	O
in	O
what	O
is	O
the	O
City	O
's	O
financial	O
core	O
.	O
The	O
Barbican	O
Estate	O
,	O
in	O
the	O
north	O
of	O
the	O
City	O
,	O
has	O
three	O
tall	O
residential	O
towers	O
and	O
another	O
is	O
under	O
construction	O
close	O
by	O
.	O
The	O
thirteen	O
tallest	O
buildings	O
(	O
those	O
taller	O
than	O
100	O
m	O
)	O
in	O
the	O
City	O
are	O
:	O
