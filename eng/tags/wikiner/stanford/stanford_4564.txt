The	O
First	O
Epistle	O
of	O
John	PERSON
,	O
usually	O
referred	O
to	O
simply	O
as	O
First	O
John	PERSON
and	O
often	O
written	O
1	O
John	PERSON
,	O
is	O
a	O
book	O
of	O
the	O
New	O
Testament	O
.	O
The	O
work	O
was	O
written	O
to	O
counter	O
the	O
heresies	O
that	O
Jesus	PERSON
did	O
not	O
come	O
"	O
in	O
the	O
flesh,	O
"	O
but	O
only	O
as	O
a	O
spirit	O
.	O
Some	O
modern	O
scholars	O
believe	O
that	O
the	O
apostle	O
John	PERSON
wrote	O
none	O
of	O
the	O
New	O
Testament	O
books	O
traditionally	O
attributed	O
to	O
him	O
.	O
Such	O
teachers	O
were	O
considered	O
Antichrists	O
(	O
2.18-19	O
)	O
who	O
had	O
once	O
been	O
church	O
leaders	O
but	O
whose	O
teaching	O
became	O
heterodox	O
.	O
These	O
verses	O
do	O
not	O
appear	O
in	O
any	O
version	O
of	O
the	O
text	O
prior	O
to	O
the	O
ninth	O
century	O
,	O
but	O
do	O
appear	O
in	O
the	O
King	O
James	PERSON
Bible	PERSON
,	O
something	O
Isaac	PERSON
Newton	PERSON
commented	O
on	O
in	O
An	O
Historical	O
Account	O
of	O
Two	O
Notable	O
Corruptions	O
of	O
Scripture	O
.	O
The	O
majority	O
of	O
modern	O
translations	O
(	O
for	O
example	O
English	O
Standard	O
Version	O
and	O
New	O
American	O
Standard	O
Bible	O
)	O
do	O
not	O
include	O
this	O
text	O
.	O
Albert	PERSON
Barnes	PERSON
(	O
1798	O
--	O
1870	O
)	O
said	O
regarding	O
its	O
authenticity	O
:	O
Online	O
translations	O
of	O
the	O
First	O
Epistle	O
of	O
John	PERSON
