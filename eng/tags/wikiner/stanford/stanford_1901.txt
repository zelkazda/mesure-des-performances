It	O
was	O
later	O
rebroadcast	O
on	O
A	O
&	O
E	O
Network	O
.	O
However	O
,	O
before	O
the	O
third	O
season	O
could	O
start	O
production	O
,	O
Peppard	PERSON
quit	O
the	O
show	O
in	O
order	O
to	O
prevent	O
his	O
ex-wife	O
Elizabeth	PERSON
Ashley	PERSON
from	O
receiving	O
a	O
larger	O
percentage	O
of	O
his	O
earnings	O
as	O
part	O
of	O
their	O
divorce	O
settlement	O
.	O
One	O
of	O
Banacek	PERSON
's	O
verbal	O
signatures	O
was	O
the	O
quotation	O
of	O
strangely	O
worded	O
yet	O
curiously	O
cogent	O
"	O
Polish	O
"	O
proverbs	O
such	O
as	O
:	O
"	O
If	O
you	O
're	O
not	O
sure	O
that	O
it	O
's	O
potato	O
borscht	O
,	O
there	O
could	O
be	O
children	O
working	O
in	O
the	O
mines	O
"	O
;	O
"	O
Though	O
the	O
hippopotamus	O
has	O
no	O
sting	O
,	O
the	O
wise	O
man	O
would	O
prefer	O
to	O
be	O
sat	O
upon	O
by	O
the	O
bee	O
"	O
;	O
"	O
A	O
wise	O
man	O
never	O
plays	O
leapfrog	O
with	O
a	O
unicorn	O
"	O
;	O
"	O
If	O
a	O
wolf	O
is	O
after	O
your	O
sleigh	O
--	O
throw	O
him	O
a	O
raisin	O
cookie	O
,	O
but	O
do	O
n't	O
stop	O
to	O
bake	O
him	O
a	O
cake	O
"	O
;	O
and	O
"	O
Just	O
because	O
the	O
cat	O
has	O
her	O
kittens	O
in	O
the	O
oven	O
does	O
n't	O
make	O
them	O
biscuits	O
.	O
"	O
was	O
shot	O
on	O
location	O
at	O
the	O
California	ORGANIZATION
Institute	ORGANIZATION
of	ORGANIZATION
the	ORGANIZATION
Arts	ORGANIZATION
around	O
the	O
time	O
the	O
school	O
first	O
opened	O
.	O
