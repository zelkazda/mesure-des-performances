Earthdawn	O
is	O
a	O
fantasy	O
role-playing	O
game	O
,	O
originally	O
produced	O
by	O
FASA	ORGANIZATION
in	O
1993	O
.	O
The	O
game	O
is	O
similar	O
to	O
fantasy	O
games	O
like	O
Dungeons	O
&	O
Dragons	O
,	O
but	O
draws	O
more	O
inspiration	O
from	O
games	O
like	O
RuneQuest	O
.	O
Like	O
many	O
role-playing	O
games	O
from	O
the	O
nineties	O
,	O
Earthdawn	O
focuses	O
much	O
of	O
its	O
detail	O
on	O
its	O
setting	O
,	O
a	O
province	O
called	O
Barsaive	O
.	O
Starting	O
in	O
1993	O
,	O
FASA	ORGANIZATION
released	O
over	O
20	O
gaming	O
supplements	O
describing	O
this	O
universe	O
;	O
however	O
,	O
it	O
closed	O
down	O
production	O
of	O
Earthdawn	LOCATION
in	O
January	O
1999	O
.	O
During	O
that	O
time	O
several	O
novels	O
and	O
short-story	O
anthologies	O
set	O
in	O
the	O
Earthdawn	O
universe	O
were	O
also	O
released	O
.	O
Each	O
book	O
has	O
over	O
500	O
pages	O
,	O
and	O
summarizes	O
much	O
of	O
what	O
FASA	ORGANIZATION
published	O
--	O
not	O
only	O
the	O
game	O
mechanics	O
,	O
but	O
also	O
the	O
setting	O
,	O
narrations	O
,	O
and	O
stories	O
.	O
In	O
Barsaive	LOCATION
,	O
magic	O
,	O
like	O
many	O
things	O
in	O
nature	O
,	O
goes	O
through	O
cycles	O
.	O
The	O
primary	O
setting	O
of	O
Earthdawn	LOCATION
is	O
Barsaive	PERSON
,	O
a	O
former	O
province	O
of	O
the	O
Theran	O
Empire	O
.	O
Raw	O
casting	O
is	O
perhaps	O
the	O
most	O
dangerous	O
aspect	O
of	O
the	O
Earthdawn	O
magic	O
system	O
.	O
One	O
of	O
the	O
most	O
innovative	O
ideas	O
in	O
Earthdawn	LOCATION
is	O
how	O
magical	O
items	O
work	O
.	O
