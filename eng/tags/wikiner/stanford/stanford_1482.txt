The	O
beginnings	O
of	O
a	O
single	O
ruling	O
bishop	O
can	O
perhaps	O
be	O
traced	O
to	O
the	O
offices	O
occupied	O
by	O
Timothy	PERSON
and	O
Titus	O
in	O
the	O
New	O
Testament	O
.	O
1:3	O
and	O
Titus	O
1:5	O
)	O
.	O
Paul	PERSON
commands	O
them	O
to	O
ordain	O
presybters/bishops	O
and	O
to	O
exercise	O
general	O
oversight	O
,	O
telling	O
Titus	O
to	O
"	O
rebuke	O
with	O
all	O
authority	O
"	O
(	O
Titus	O
2:15	O
)	O
.	O
Linus	PERSON
,	O
Cletus	PERSON
and	O
Clement	PERSON
were	O
probably	O
prominent	O
presbyter-bishops	O
but	O
not	O
necessarily	O
monarchical	O
bishops	O
.	O
The	O
most	O
usual	O
term	O
for	O
the	O
geographic	O
area	O
of	O
a	O
bishop	O
's	O
authority	O
and	O
ministry	O
,	O
the	O
diocese	O
,	O
began	O
as	O
part	O
of	O
the	O
structure	O
of	O
the	O
Roman	LOCATION
Empire	LOCATION
under	O
Diocletian	PERSON
.	O
The	O
Lord	O
Chancellor	O
of	O
England	LOCATION
was	O
almost	O
always	O
a	O
bishop	O
up	O
until	O
the	O
dismissal	O
of	O
Cardinal	O
Thomas	PERSON
Wolsey	PERSON
by	O
Henry	PERSON
VIII	PERSON
.	O
Eastern	ORGANIZATION
Orthodox	ORGANIZATION
bishops	O
,	O
along	O
with	O
all	O
other	O
members	O
of	O
the	O
clergy	O
,	O
are	O
canonically	O
forbidden	O
to	O
hold	O
political	O
office	O
.	O
During	O
the	O
period	O
of	O
the	O
English	O
Civil	O
War	O
,	O
the	O
role	O
of	O
bishops	O
as	O
wielders	O
of	O
political	O
power	O
and	O
as	O
upholders	O
of	O
the	O
established	O
church	O
became	O
a	O
matter	O
of	O
heated	O
political	O
controversy	O
.	O
The	O
controversy	O
eventually	O
led	O
to	O
Laud	O
's	O
impeachment	O
for	O
treason	O
by	O
a	O
bill	O
of	O
attainder	O
in	O
1645	O
,	O
and	O
subsequent	O
execution	O
.	O
In	O
Byzantine	O
usage	O
,	O
an	O
antimension	O
signed	O
by	O
the	O
bishop	O
is	O
kept	O
on	O
the	O
altar	O
partly	O
as	O
a	O
reminder	O
of	O
whose	O
altar	O
it	O
is	O
and	O
under	O
whose	O
omophorion	O
the	O
priest	O
at	O
a	O
local	O
parish	O
is	O
serving	O
.	O
Roman	O
Catholic	O
doctrine	O
holds	O
that	O
one	O
bishop	O
can	O
validly	O
ordain	O
another	O
male	O
(	O
priest	O
)	O
as	O
a	O
bishop	O
.	O
In	O
the	O
Catholic	ORGANIZATION
Church	ORGANIZATION
the	ORGANIZATION
Congregation	ORGANIZATION
for	O
Bishops	O
oversees	O
the	O
selection	O
of	O
new	O
bishops	O
with	O
the	O
approval	O
of	O
the	O
pope	O
.	O
Most	O
Eastern	O
Orthodox	O
churches	O
allow	O
varying	O
amounts	O
of	O
formalised	O
laity	O
and/or	O
lower	O
clergy	O
influence	O
on	O
the	O
choice	O
of	O
bishops	O
.	O
The	O
position	O
of	O
Roman	O
Catholicism	O
is	O
slightly	O
different	O
.	O
Although	O
ELCA	ORGANIZATION
agreed	O
with	O
the	O
Episcopal	ORGANIZATION
Church	ORGANIZATION
to	O
limit	O
ordination	O
to	O
the	O
bishop	O
"	O
ordinarily	O
"	O
,	O
ELCA	ORGANIZATION
pastor-ordinators	O
are	O
given	O
permission	O
to	O
perform	O
the	O
rites	O
in	O
"	O
extraordinary	O
"	O
circumstance	O
.	O
It	O
should	O
also	O
be	O
noted	O
that	O
the	O
second	O
largest	O
of	O
the	O
three	O
predecessor	O
bodies	O
of	O
the	O
ELCA	ORGANIZATION
,	O
the	O
American	ORGANIZATION
Lutheran	ORGANIZATION
Church	ORGANIZATION
,	O
was	O
a	O
congregationalist	O
body	O
,	O
with	O
national	O
and	O
synod	O
presidents	O
before	O
they	O
were	O
re-titled	O
as	O
bishops	O
in	O
the	O
1980s	O
.	O
In	O
the	O
United	ORGANIZATION
Methodist	ORGANIZATION
Church	ORGANIZATION
bishops	O
serve	O
as	O
administrative	O
and	O
pastoral	O
superintendents	O
of	O
the	O
church	O
.	O
Within	O
the	O
United	ORGANIZATION
Methodist	ORGANIZATION
Church	ORGANIZATION
only	O
bishops	O
are	O
empowered	O
to	O
consecrate	O
bishops	O
and	O
ordain	O
clergy	O
.	O
In	O
all	O
of	O
these	O
areas	O
,	O
bishops	O
of	O
the	O
United	ORGANIZATION
Methodist	ORGANIZATION
Church	ORGANIZATION
function	O
very	O
much	O
in	O
the	O
historic	O
meaning	O
of	O
the	O
term	O
.	O
United	ORGANIZATION
Methodist	ORGANIZATION
bishops	O
may	O
be	O
male	O
or	O
female	O
,	O
with	O
the	O
Rev.	O
Marjorie	PERSON
Matthews	PERSON
being	O
the	O
first	O
woman	O
to	O
be	O
consecrated	O
a	O
bishop	O
in	O
1980	O
.	O
The	O
collegial	O
expression	O
of	O
episcopal	O
leadership	O
in	O
the	O
United	ORGANIZATION
Methodist	ORGANIZATION
Church	ORGANIZATION
is	O
known	O
as	O
the	O
Council	O
of	O
Bishops	O
.	O
Coke	ORGANIZATION
soon	O
returned	O
to	O
England	LOCATION
,	O
but	O
Asbury	LOCATION
was	O
the	O
primary	O
builder	O
of	O
the	O
new	O
church	O
.	O
In	O
the	O
Christian	ORGANIZATION
Methodist	ORGANIZATION
Episcopal	ORGANIZATION
Church	ORGANIZATION
,	O
bishops	O
are	O
administrative	O
superintendents	O
of	O
the	O
church	O
;	O
they	O
are	O
elected	O
by	O
"	O
delegate	O
"	O
votes	O
for	O
as	O
many	O
years	O
deemed	O
until	O
the	O
age	O
of	O
74	O
,	O
then	O
he/she	O
must	O
retire	O
.	O
The	O
General	O
Conference	O
,	O
a	O
meeting	O
every	O
four	O
years	O
,	O
has	O
an	O
equal	O
number	O
of	O
clergy	O
and	O
lay	O
delegates	O
.	O
CME	ORGANIZATION
Church	ORGANIZATION
bishops	O
may	O
be	O
male	O
or	O
female	O
.	O
In	O
The	O
Church	O
of	O
Jesus	PERSON
Christ	PERSON
of	O
Latter-day	O
Saints	O
,	O
the	O
Bishop	O
is	O
the	O
leader	O
of	O
a	O
local	O
congregation	O
,	O
called	O
a	O
ward	O
.	O
It	O
is	O
therefore	O
believed	O
that	O
he	O
has	O
both	O
the	O
right	O
and	O
ability	O
to	O
receive	O
divine	O
inspiration	O
(	O
through	O
the	O
Holy	O
Spirit	O
)	O
for	O
the	O
ward	O
under	O
his	O
direction	O
.	O
A	O
bishop	O
is	O
the	O
president	O
of	O
the	O
Aaronic	O
priesthood	O
in	O
his	O
ward	O
.	O
The	O
Presiding	O
Bishop	O
has	O
two	O
counselors	O
;	O
the	O
three	O
together	O
form	O
the	O
Presiding	ORGANIZATION
Bishopric	ORGANIZATION
.	O
The	O
Caeremoniale	O
Episcoporum	O
recommends	O
,	O
but	O
does	O
not	O
impose	O
,	O
that	O
in	O
solemn	O
celebrations	O
a	O
bishop	O
should	O
also	O
wear	O
a	O
dalmatic	O
,	O
which	O
can	O
always	O
be	O
white	O
,	O
beneath	O
the	O
chasuble	O
,	O
especially	O
when	O
administering	O
the	O
sacrament	O
of	O
holy	O
orders	O
,	O
blessing	O
an	O
abbot	O
or	O
abbess	O
,	O
and	O
dedicating	O
a	O
church	O
or	O
an	O
altar	O
.	O
The	O
Caeremoniale	O
Episcoporum	O
no	O
longer	O
makes	O
mention	O
of	O
episcopal	O
gloves	O
,	O
episcopal	O
sandals	O
,	O
liturgical	O
stockings	O
(	O
also	O
known	O
as	O
buskins	O
)	O
,	O
or	O
the	O
accoutrements	O
that	O
it	O
once	O
prescribed	O
for	O
the	O
bishop	O
's	O
horse	O
.	O
