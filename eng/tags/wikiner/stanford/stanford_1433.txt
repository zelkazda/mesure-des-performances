The	O
first	O
written	O
reference	O
is	O
found	O
in	O
a	O
book	O
by	O
Miguel	PERSON
de	PERSON
Cervantes	PERSON
,	O
who	O
is	O
most	O
famous	O
for	O
writing	O
Don	PERSON
Quixote	PERSON
,	O
among	O
other	O
works	O
.	O
In	O
most	O
non-U.S.	O
casinos	O
,	O
a	O
'	O
no	O
hole	O
card	O
'	O
game	O
is	O
played	O
,	O
meaning	O
that	O
the	O
dealer	O
does	O
not	O
draw	O
nor	O
consult	O
his	O
second	O
card	O
until	O
after	O
all	O
players	O
have	O
finished	O
making	O
decisions	O
.	O
Most	O
Las	LOCATION
Vegas	LOCATION
Strip	LOCATION
casinos	O
hit	O
on	O
soft	O
17	O
.	O
All	O
such	O
techniques	O
are	O
based	O
on	O
the	O
value	O
of	O
the	O
cards	O
to	O
the	O
player	O
and	O
the	O
casino	O
,	O
as	O
originally	O
conceived	O
by	O
Edward	PERSON
O.	PERSON
Thorp	PERSON
.	O
Arnold	PERSON
Snyder	PERSON
's	O
articles	O
in	O
Blackjack	ORGANIZATION
Forum	ORGANIZATION
magazine	O
brought	O
shuffle	O
tracking	O
to	O
the	O
general	O
public	O
.	O
Jerry	PERSON
L.	PERSON
Patterson	PERSON
also	O
developed	O
and	O
published	O
a	O
shuffle-tracking	O
method	O
for	O
tracking	O
favorable	O
clumps	O
of	O
cards	O
and	O
cutting	O
them	O
into	O
play	O
and	O
tracking	O
unfavorable	O
clumps	O
of	O
cards	O
and	O
cutting	O
them	O
out	O
of	O
play	O
.	O
Spanish	O
21	O
provides	O
players	O
with	O
many	O
liberal	O
blackjack	O
rules	O
,	O
such	O
as	O
doubling	O
down	O
any	O
number	O
of	O
cards	O
(	O
with	O
the	O
option	O
to	O
'	O
rescue	O
'	O
,	O
or	O
surrender	O
only	O
one	O
wager	O
to	O
the	O
house	O
)	O
,	O
payout	O
bonuses	O
for	O
five	O
or	O
more	O
card	O
21s	O
,	O
6-7-8	O
21s	O
,	O
7-7-7	O
21s	O
,	O
late	O
surrender	O
,	O
and	O
player	O
blackjacks	O
always	O
winning	O
and	O
player	O
21s	O
always	O
winning	O
,	O
at	O
the	O
cost	O
of	O
having	O
no	O
10	O
cards	O
in	O
the	O
deck	O
(	O
though	O
there	O
are	O
jacks	O
,	O
queens	O
,	O
and	O
kings	O
)	O
.	O
In	O
2002	O
,	O
professional	O
gamblers	O
around	O
the	O
world	O
were	O
invited	O
to	O
nominate	O
great	O
blackjack	O
players	O
for	O
admission	O
into	O
the	O
Blackjack	ORGANIZATION
Hall	ORGANIZATION
of	ORGANIZATION
Fame	ORGANIZATION
.	O
