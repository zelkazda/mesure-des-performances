Excalibur	O
is	O
the	O
legendary	O
sword	O
of	O
King	O
Arthur	PERSON
,	O
sometimes	O
attributed	O
with	O
magical	O
powers	O
or	O
associated	O
with	O
the	O
rightful	O
sovereignty	O
of	O
Great	LOCATION
Britain	LOCATION
.	O
The	O
sword	O
was	O
associated	O
with	O
the	O
Arthurian	O
legend	O
very	O
early	O
.	O
In	O
Welsh	O
,	O
the	O
sword	O
is	O
called	O
Caledfwlch	O
.	O
The	O
name	O
Excalibur	O
apparently	O
derives	O
from	O
the	O
Welsh	O
Caledfwlch	O
which	O
combines	O
the	O
elements	O
caled	O
(	O
"	O
battle	O
,	O
hard	O
"	O
)	O
,	O
and	O
bwlch	O
(	O
"	O
breach	O
,	O
gap	O
,	O
notch	O
"	O
)	O
.	O
In	O
Arthurian	O
romance	O
,	O
a	O
number	O
of	O
explanations	O
are	O
given	O
for	O
Arthur	PERSON
's	O
possession	O
of	O
Excalibur	O
.	O
In	O
Robert	PERSON
de	PERSON
Boron	PERSON
's	O
Merlin	LOCATION
,	O
Arthur	PERSON
obtained	O
the	O
throne	O
by	O
pulling	O
a	O
sword	O
from	O
a	O
stone	O
.	O
In	O
this	O
account	O
,	O
the	O
act	O
could	O
not	O
be	O
performed	O
except	O
by	O
"	O
the	O
true	O
king,	O
"	O
meaning	O
the	O
divinely	O
appointed	O
king	O
or	O
true	O
heir	O
of	O
Uther	O
Pendragon	O
.	O
This	O
sword	O
is	O
thought	O
by	O
many	O
to	O
be	O
the	O
famous	O
Excalibur	O
,	O
and	O
its	O
identity	O
is	O
made	O
explicit	O
in	O
the	O
later	O
so-called	O
Vulgate	O
Merlin	O
Continuation	O
,	O
part	O
of	O
the	O
Lancelot-Grail	LOCATION
cycle	O
.	O
In	O
the	O
Vulgate	O
Mort	PERSON
Artu	PERSON
,	O
Arthur	PERSON
orders	O
Girflet	PERSON
to	O
throw	O
the	O
sword	O
into	O
the	O
enchanted	O
lake	O
.	O
In	O
Welsh	O
legend	O
,	O
Arthur	PERSON
's	O
sword	O
is	O
known	O
as	O
Caledfwlch	O
.	O
The	O
legend	O
was	O
expanded	O
upon	O
in	O
the	O
Vulgate	O
Cycle	O
,	O
also	O
known	O
as	O
the	O
Lancelot-Grail	O
Cycle	O
,	O
and	O
in	O
the	O
Post-Vulgate	O
Cycle	O
which	O
emerged	O
in	O
its	O
wake	O
.	O
This	O
is	O
in	O
contrast	O
to	O
later	O
versions	O
,	O
where	O
Excalibur	O
belongs	O
solely	O
to	O
the	O
king	O
.	O
In	O
many	O
versions	O
,	O
Excalibur	O
's	O
blade	O
was	O
engraved	O
with	O
words	O
on	O
opposite	O
sides	O
.	O
In	O
addition	O
,	O
when	O
Excalibur	PERSON
was	O
first	O
drawn	O
,	O
in	O
the	O
first	O
battle	O
testing	O
Arthur	PERSON
's	O
sovereignty	O
,	O
its	O
blade	O
blinded	O
his	O
enemies	O
.	O
Excalibur	PERSON
's	O
scabbard	O
was	O
said	O
to	O
have	O
powers	O
of	O
its	O
own	O
.	O
The	O
scabbard	O
is	O
stolen	O
by	O
Morgan	PERSON
le	O
Fay	PERSON
and	O
thrown	O
into	O
a	O
lake	O
,	O
never	O
to	O
be	O
found	O
again	O
.	O
Excalibur	O
is	O
by	O
no	O
means	O
the	O
only	O
weapon	O
associated	O
with	O
Arthur	PERSON
,	O
nor	O
the	O
only	O
sword	O
.	O
