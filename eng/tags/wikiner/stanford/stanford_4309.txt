The	O
French	O
Southern	O
and	O
Antarctic	O
Lands	O
(	O
French	O
:	O
Terres	O
australes	O
et	O
antarctiques	O
françaises	O
,	O
abbreviated	O
TAAF	O
)	O
,	O
full	O
name	O
Territory	O
of	O
the	O
French	O
Southern	O
and	O
Antarctic	LOCATION
Lands	LOCATION
,	O
consist	O
of	O
:	O
The	O
territory	O
is	O
also	O
often	O
called	O
the	O
French	O
Southern	O
Lands	O
(	O
French	O
:	O
Terres	O
australes	O
françaises	O
)	O
or	O
French	O
Southern	O
Territories	O
,	O
which	O
excludes	O
Adélie	O
Land	O
where	O
French	O
sovereignty	O
is	O
not	O
recognized	O
internationally	O
.	O
The	O
lands	O
are	O
not	O
connected	O
to	O
France	PERSON
Antarctique	PERSON
,	O
a	O
former	O
French	O
colony	O
in	O
Brazil	LOCATION
.	O
The	O
French	O
Southern	O
and	O
Antarctic	O
Lands	O
have	O
formed	O
a	O
territoire	O
d'outre-mer	O
(	O
an	O
overseas	O
territory	O
)	O
of	O
France	LOCATION
since	O
1955	O
.	O
Each	O
district	O
is	O
headed	O
by	O
a	O
district	O
chief	O
,	O
which	O
has	O
powers	O
similar	O
to	O
those	O
of	O
a	O
French	O
mayor	O
(	O
including	O
recording	O
births	O
and	O
deaths	O
and	O
being	O
an	O
officer	O
of	O
judicial	O
police	O
)	O
.	O
The	O
"	O
Adélie	O
Land	O
"	O
of	O
about	O
432,000	O
km²	O
and	O
the	O
islands	O
,	O
totalling	O
7781	O
km²	O
,	O
have	O
no	O
indigenous	O
inhabitants	O
,	O
though	O
in	O
1997	O
there	O
were	O
about	O
100	O
researchers	O
whose	O
numbers	O
varied	O
from	O
winter	O
(	O
July	O
)	O
to	O
summer	O
(	O
January	O
)	O
.	O
The	O
islands	O
in	O
the	O
Indian	LOCATION
Ocean	LOCATION
are	O
supplied	O
by	O
the	O
special	O
ship	O
Marion	PERSON
Dufresne	PERSON
sailing	O
out	O
of	O
Le	LOCATION
Port	LOCATION
in	O
Réunion	LOCATION
Island	LOCATION
.	O
This	O
fleet	O
is	O
maintained	O
as	O
a	O
subset	O
of	O
the	O
French	O
register	O
that	O
allows	O
French-owned	O
ships	O
to	O
operate	O
under	O
more	O
liberal	O
taxation	O
and	O
manning	O
regulations	O
than	O
permissible	O
under	O
the	O
main	O
French	O
register	O
.	O
The	O
territory	O
's	O
natural	O
resources	O
are	O
limited	O
to	O
fish	O
and	O
crustaceans	O
;	O
economic	O
activity	O
is	O
limited	O
to	O
servicing	O
meteorological	O
and	O
geophysical	O
research	O
stations	O
and	O
French	O
and	O
other	O
fishing	O
fleets	O
.	O
Both	O
are	O
poached	O
by	O
foreign	O
fleets	O
;	O
because	O
of	O
this	O
,	O
the	O
French	ORGANIZATION
Navy	ORGANIZATION
and	O
occasionally	O
other	O
services	O
patrol	O
the	O
zone	O
and	O
arrest	O
poaching	O
vessels	O
.	O
Marion	PERSON
Dufresne	PERSON
can	O
host	O
a	O
limited	O
number	O
of	O
fee-paying	O
tourists	O
,	O
who	O
will	O
be	O
able	O
to	O
visit	O
the	O
islands	O
as	O
the	O
ship	O
calls	O
.	O
The	O
French	O
Southern	O
Territories	O
(	O
i.e.	O
excluding	O
Adélie	O
Land	O
)	O
is	O
given	O
the	O
following	O
country	O
codes	O
:	O
FS	O
and	O
TF	O
(	O
ISO	O
3166-1	O
alpha-2	O
)	O
.	O
