From	O
that	O
work	O
we	O
learn	O
that	O
the	O
higher	O
education	O
of	O
the	O
youth	O
of	O
Baghdad	LOCATION
consisted	O
principally	O
in	O
a	O
minute	O
and	O
careful	O
study	O
of	O
the	O
rules	O
and	O
principles	O
of	O
grammar	O
,	O
and	O
in	O
their	O
committing	O
to	O
memory	O
the	O
whole	O
of	O
the	O
Qur'an	O
,	O
a	O
treatise	O
or	O
two	O
on	O
philology	O
and	O
jurisprudence	O
,	O
and	O
the	O
choicest	O
Arabic	O
poetry	O
.	O
After	O
attaining	O
to	O
great	O
proficiency	O
in	O
that	O
kind	O
of	O
learning	O
,	O
Abdallatif	PERSON
applied	O
himself	O
to	O
natural	O
philosophy	O
and	O
medicine	O
.	O
To	O
enjoy	O
the	O
society	O
of	O
the	O
learned	O
,	O
he	O
went	O
first	O
to	O
Mosul	LOCATION
(	O
1189	O
)	O
,	O
and	O
afterwards	O
to	O
Damascus	LOCATION
.	O
He	O
afterwards	O
formed	O
one	O
of	O
the	O
circles	O
of	O
learned	O
men	O
whom	O
Saladin	PERSON
gathered	O
around	O
him	O
at	O
Jerusalem	LOCATION
.	O
He	O
taught	O
medicine	O
and	O
philosophy	O
at	O
Cairo	LOCATION
and	O
at	O
Damascus	LOCATION
for	O
a	O
number	O
of	O
years	O
,	O
and	O
afterwards	O
,	O
for	O
a	O
shorter	O
period	O
,	O
at	O
Aleppo	LOCATION
.	O
His	O
love	O
of	O
travel	O
led	O
him	O
to	O
visit	O
different	O
parts	O
of	O
Armenia	LOCATION
and	O
Asia	LOCATION
Minor	LOCATION
in	O
his	O
old	O
age	O
.	O
Abdallatif	PERSON
was	O
undoubtedly	O
a	O
man	O
of	O
great	O
knowledge	O
and	O
of	O
an	O
inquisitive	O
and	O
penetrating	O
mind	O
.	O
This	O
fraudulent	O
practice	O
continues	O
to	O
the	O
present	O
day	O
,	O
with	O
rich	O
businessmen	O
in	O
Egypt	LOCATION
still	O
being	O
deceived	O
by	O
local	O
treasure	O
hunters	O
.	O
It	O
contains	O
a	O
vivid	O
description	O
of	O
a	O
famine	O
caused	O
,	O
during	O
the	O
author	O
's	O
residence	O
in	O
Egypt	LOCATION
,	O
by	O
the	O
Nile	O
failing	O
to	O
overflow	O
its	O
banks	O
.	O
This	O
was	O
one	O
of	O
the	O
earliest	O
examples	O
of	O
a	O
postmortem	O
autopsy	O
,	O
through	O
which	O
he	O
discovered	O
that	O
Galen	PERSON
was	O
incorrect	O
regarding	O
the	O
formation	O
of	O
the	O
bones	O
of	O
the	O
lower	O
jaw	O
and	O
sacrum	O
.	O
The	O
Arabic	O
manuscript	O
was	O
discovered	O
in	O
1665	O
by	O
Edward	PERSON
Pococke	PERSON
the	O
orientalist	O
,	O
and	O
preserved	O
in	O
the	O
Bodleian	LOCATION
Library	LOCATION
.	O
He	O
then	O
published	O
the	O
Arabic	O
manuscript	O
in	O
the	O
1680s	O
.	O
Pococke	PERSON
's	O
complete	O
Latin	O
translation	O
was	O
eventually	O
published	O
by	O
Joseph	PERSON
White	PERSON
of	O
Oxford	LOCATION
in	O
1800	O
.	O
The	O
work	O
was	O
then	O
translated	O
into	O
French	O
,	O
with	O
valuable	O
notes	O
,	O
by	O
Silvestre	PERSON
de	PERSON
Sacy	PERSON
in	O
1810	O
.	O
Al-Baghdadi	PERSON
was	O
also	O
the	O
author	O
of	O
a	O
major	O
book	O
dealing	O
with	O
diabetes	O
.	O
