In	O
cell	O
biology	O
,	O
the	O
nucleus	O
(	O
pl.	O
nuclei	O
;	O
from	O
Latin	O
nucleus	O
or	O
nuculeus	O
,	O
meaning	O
kernel	O
)	O
,	O
also	O
sometimes	O
referred	O
to	O
as	O
the	O
"	O
control	O
center	O
"	O
,	O
is	O
a	O
membrane-enclosed	O
organelle	O
found	O
in	O
eukaryotic	O
cells	O
.	O
The	O
probably	O
oldest	O
preserved	O
drawing	O
dates	O
back	O
to	O
the	O
early	O
microscopist	O
Antonie	PERSON
van	PERSON
Leeuwenhoek	PERSON
(	O
1632	O
--	O
1723	O
)	O
.	O
Brown	PERSON
was	O
studying	O
orchids	O
microscopically	O
when	O
he	O
observed	O
an	O
opaque	O
area	O
,	O
which	O
he	O
called	O
the	O
areola	O
or	O
nucleus	O
,	O
in	O
the	O
cells	O
of	O
the	O
flower	O
's	O
outer	O
layer	O
.	O
Franz	PERSON
Meyen	PERSON
was	O
a	O
strong	O
opponent	O
of	O
this	O
view	O
having	O
already	O
described	O
cells	O
multiplying	O
by	O
division	O
and	O
believing	O
that	O
many	O
cells	O
would	O
have	O
no	O
nuclei	O
.	O
The	O
idea	O
that	O
cells	O
can	O
be	O
generated	O
de	O
novo	O
,	O
by	O
the	O
"	O
cytoblast	O
"	O
or	O
otherwise	O
,	O
contradicted	O
work	O
by	O
Robert	PERSON
Remak	PERSON
(	O
1852	O
)	O
and	O
Rudolf	PERSON
Virchow	PERSON
(	O
1855	O
)	O
who	O
decisively	O
propagated	O
the	O
new	O
paradigm	O
that	O
cells	O
are	O
generated	O
solely	O
by	O
cells	O
.	O
Between	O
1876	O
and	O
1878	O
Oscar	PERSON
Hertwig	PERSON
published	O
several	O
studies	O
on	O
the	O
fertilization	O
of	O
sea	O
urchin	O
eggs	O
,	O
showing	O
that	O
the	O
nucleus	O
of	O
the	O
sperm	O
enters	O
the	O
oocyte	O
and	O
fuses	O
with	O
its	O
nucleus	O
.	O
Eduard	PERSON
Strasburger	PERSON
produced	O
the	O
same	O
results	O
for	O
plants	O
(	O
1884	O
)	O
.	O
In	O
1873	O
August	O
Weismann	PERSON
postulated	O
the	O
equivalence	O
of	O
the	O
maternal	O
and	O
paternal	O
germ	O
cells	O
for	O
heredity	O
.	O
