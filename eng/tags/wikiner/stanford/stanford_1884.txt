Bundaberg	LOCATION
Rum	O
is	O
a	O
dark	O
rum	O
produced	O
in	O
Bundaberg	LOCATION
,	O
Australia	LOCATION
,	O
often	O
referred	O
to	O
as	O
"	O
Bundy	O
"	O
.	O
This	O
company	O
does	O
not	O
produce	O
Bundaberg	ORGANIZATION
Ginger	ORGANIZATION
Beer	ORGANIZATION
.	O
Bundaberg	LOCATION
Rum	O
originated	O
because	O
the	O
local	O
sugar	O
mills	O
had	O
a	O
problem	O
with	O
what	O
to	O
do	O
with	O
the	O
waste	O
molasses	O
after	O
the	O
sugar	O
was	O
extracted	O
(	O
it	O
was	O
heavy	O
,	O
difficult	O
to	O
transport	O
and	O
the	O
costs	O
of	O
converting	O
it	O
to	O
stock	O
feed	O
were	O
rarely	O
worth	O
the	O
effort	O
)	O
.	O
Bundaberg	LOCATION
rum	O
was	O
first	O
produced	O
1888	O
,	O
production	O
ceased	O
from	O
1907	O
to	O
1914	O
and	O
from	O
1936	O
to	O
1939	O
after	O
fires	O
,	O
the	O
second	O
of	O
which	O
caused	O
rum	O
from	O
the	O
factory	O
to	O
spill	O
into	O
the	O
nearby	O
Burnett	LOCATION
River	LOCATION
.	O
The	O
Bundaberg	LOCATION
Rum	O
distillery	O
is	O
open	O
to	O
visitors	O
for	O
tours	O
of	O
the	O
facility	O
.	O
There	O
is	O
also	O
a	O
museum	O
and	O
offers	O
free	O
samples	O
of	O
Bundaberg	LOCATION
Rum	O
products	O
for	O
visitors	O
.	O
Bundaberg	LOCATION
is	O
also	O
a	O
sponsor	O
of	O
the	O
NSW	ORGANIZATION
Waratahs	ORGANIZATION
.	O
Bundaberg	LOCATION
Rum	O
also	O
sponsors	O
the	O
rugby	O
league	O
ANZAC	O
Test	O
till	O
2009	O
.	O
Previously	O
Bundaberg	O
Rum	O
had	O
sponsored	O
a	O
stadium	O
in	O
Cairns	LOCATION
,	O
Australia	LOCATION
which	O
was	O
formally	O
known	O
as	O
Bundaberg	LOCATION
Rum	LOCATION
Stadium	LOCATION
but	O
has	O
been	O
renamed	O
to	O
Cazaly	LOCATION
's	LOCATION
Stadium	LOCATION
.	O
Bundaberg	LOCATION
Rum	O
has	O
also	O
been	O
criticised	O
for	O
targeting	O
its	O
advertising	O
towards	O
young	O
people	O
and	O
boys	O
,	O
through	O
television	O
commercials	O
during	O
NRL	ORGANIZATION
broadcasts	O
,	O
and	O
other	O
promotions	O
.	O
"	O
They	O
will	O
abuse	O
bar	O
staff	O
,	O
half	O
a	O
dozen	O
a	O
night	O
,	O
normally	O
gangs	O
of	O
blokes	O
,	O
the	O
marketing	O
is	O
directed	O
at	O
yobbos	O
,	O
"	O
one	O
bar	O
owner	O
told	O
The	O
Age	O
newspaper	O
.	O
Some	O
young	O
Australian	O
men	O
warn	O
them	O
about	O
'	O
drop	O
bears	O
'	O
,	O
saying	O
that	O
they	O
're	O
"	O
like	O
a	O
koala	O
,	O
only	O
bigger	O
and	O
meaner	O
"	O
and	O
"	O
they	O
drop	O
from	O
the	O
trees	O
"	O
.	O
There	O
are	O
currently	O
a	O
number	O
of	O
products	O
available	O
which	O
are	O
distributed	O
by	O
Diageo	O
:	O
