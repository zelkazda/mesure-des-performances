He	O
became	O
interested	O
in	O
painting	O
after	O
seeing	O
some	O
works	O
of	O
Eugène	PERSON
Delacroix	PERSON
.	O
Bazille	PERSON
began	O
studying	O
medicine	O
in	O
1859	O
,	O
and	O
moved	O
to	O
Paris	LOCATION
in	O
1862	O
to	O
continue	O
his	O
studies	O
.	O
His	O
close	O
friends	O
included	O
Claude	PERSON
Monet	PERSON
,	O
Alfred	PERSON
Sisley	PERSON
,	O
and	O
Édouard	PERSON
Manet	PERSON
.	O
Bazille	PERSON
was	O
generous	O
with	O
his	O
wealth	O
,	O
and	O
helped	O
support	O
his	O
less	O
fortunate	O
associates	O
by	O
giving	O
them	O
space	O
in	O
his	O
studio	O
and	O
materials	O
to	O
use	O
.	O
His	O
father	O
travelled	O
to	O
the	O
battlefield	O
a	O
few	O
days	O
later	O
to	O
take	O
his	O
body	O
back	O
for	O
burial	O
at	O
Montpellier	ORGANIZATION
over	O
a	O
week	O
later	O
.	O
