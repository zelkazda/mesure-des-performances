David	PERSON
Lawrence	PERSON
Angell	PERSON
(	O
April	O
10	O
,	O
1946	O
--	O
September	O
11	O
,	O
2001	O
)	O
was	O
an	O
American	O
producer	O
of	O
sitcoms	O
.	O
Angell	PERSON
won	O
multiple	O
Emmy	O
Awards	O
as	O
the	O
creator	O
and	O
executive	O
producer	O
,	O
along	O
with	O
Peter	PERSON
Casey	PERSON
and	O
David	PERSON
Lee	PERSON
,	O
of	O
the	O
comedy	O
series	O
Frasier	O
.	O
He	O
entered	O
the	O
army	O
upon	O
graduation	O
and	O
served	O
at	O
the	O
Pentagon	ORGANIZATION
until	O
1972	O
.	O
He	O
then	O
moved	O
to	O
Boston	LOCATION
and	O
worked	O
as	O
a	O
methods	O
analyst	O
at	O
an	O
engineering	O
company	O
and	O
later	O
at	O
an	O
insurance	O
firm	O
in	O
Rhode	LOCATION
Island	LOCATION
.	O
Angell	PERSON
moved	O
to	O
Los	LOCATION
Angeles	LOCATION
in	O
1977	O
.	O
Five	O
years	O
later	O
,	O
he	O
sold	O
his	O
second	O
script	O
to	O
Archie	PERSON
Bunker	PERSON
's	O
Place	O
.	O
In	O
1983	O
,	O
he	O
joined	O
Cheers	O
as	O
a	O
staff	O
writer	O
.	O
In	O
1985	O
,	O
Angell	PERSON
joined	O
forces	O
with	O
Peter	PERSON
Casey	PERSON
and	O
David	PERSON
Lee	PERSON
as	O
Cheers	O
supervising	O
producers/writers	O
.	O
In	O
1990	O
,	O
they	O
created	O
and	O
executive-produced	O
the	O
comedy	O
series	O
Wings	O
.	O
In	O
delivering	O
his	O
eulogy	O
,	O
David	PERSON
Lee	PERSON
credited	O
Angell	PERSON
with	O
coining	O
the	O
word	O
"	O
boinking	O
"	O
as	O
a	O
euphemism	O
for	O
sex	O
on	O
Cheers	O
.	O
Kelsey	PERSON
Grammer	PERSON
and	O
Ted	PERSON
Danson	PERSON
also	O
spoke	O
at	O
the	O
funeral	O
.	O
On	O
the	O
Frasier	O
series	O
finale	O
in	O
2004	O
,	O
the	O
characters	O
Daphne	PERSON
and	O
Niles	PERSON
had	O
a	O
son	O
,	O
named	O
David	PERSON
(	PERSON
in	O
dedication	O
to	O
Angell	PERSON
)	O
.	O
