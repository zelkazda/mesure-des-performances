Xilinx	ORGANIZATION
continued	O
unchallenged	O
and	O
quickly	O
growing	O
from	O
1985	O
to	O
the	O
mid-1990s	O
,	O
when	O
competitors	O
sprouted	O
up	O
,	O
eroding	O
significant	O
market-share	O
.	O
By	O
1993	O
,	O
Actel	ORGANIZATION
was	O
serving	O
about	O
18	O
percent	O
of	O
the	O
market	O
.	O
Xilinx	ORGANIZATION
and	O
Altera	ORGANIZATION
are	O
the	O
current	O
FPGA	ORGANIZATION
market	O
leaders	O
and	O
long-time	O
industry	O
rivals	O
.	O
Together	O
,	O
they	O
control	O
over	O
80	O
percent	O
of	O
the	O
market	O
,	O
with	O
Xilinx	ORGANIZATION
alone	O
representing	O
over	O
50	O
percent	O
.	O
Both	O
Xilinx	ORGANIZATION
and	O
Altera	ORGANIZATION
provide	O
free	O
Windows	O
and	O
Linux	O
design	O
software	O
.	O
