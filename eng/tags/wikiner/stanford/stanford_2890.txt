Clark	PERSON
Joseph	PERSON
Kent	PERSON
is	O
a	O
fictional	O
character	O
created	O
by	O
Jerry	PERSON
Siegel	PERSON
and	O
Joe	PERSON
Shuster	PERSON
.	O
He	O
serves	O
as	O
the	O
civilian	O
and	O
secret	O
identity	O
of	O
the	O
superhero	O
Superman	O
.	O
This	O
personality	O
is	O
typically	O
described	O
as	O
"	O
mild-mannered,	O
"	O
perhaps	O
most	O
famously	O
by	O
the	O
opening	O
narration	O
of	O
Max	PERSON
Fleischer	PERSON
's	O
Superman	O
animated	O
theatrical	O
shorts	O
.	O
These	O
traits	O
extended	O
into	O
Clark	PERSON
's	O
wardrobe	O
,	O
which	O
typically	O
consists	O
of	O
a	O
bland-colored	O
business	O
suit	O
,	O
a	O
red	O
necktie	O
,	O
black-rimmed	O
glasses	O
,	O
combed-back	O
hair	O
,	O
and	O
occasionally	O
a	O
fedora	O
.	O
Superman	PERSON
usually	O
stores	O
his	O
Clark	PERSON
Kent	PERSON
clothing	O
compressed	O
in	O
a	O
secret	O
pouch	O
within	O
his	O
cape	O
,	O
though	O
some	O
stories	O
have	O
shown	O
him	O
leaving	O
his	O
clothes	O
in	O
some	O
covert	O
location	O
(	O
such	O
as	O
a	O
telephone	O
booth	O
)	O
for	O
later	O
retrieval	O
.	O
Following	O
One	O
Year	O
Later	O
,	O
Clark	PERSON
adopts	O
some	O
tricks	O
to	O
account	O
for	O
his	O
absences	O
,	O
such	O
as	O
feigning	O
illness	O
or	O
offering	O
to	O
call	O
the	O
police	O
.	O
These	O
,	O
as	O
well	O
as	O
his	O
slouching	O
posture	O
,	O
are	O
references	O
to	O
his	O
earlier	O
mild-mannered	O
Pre-Crisis	O
versions	O
,	O
but	O
he	O
still	O
maintains	O
a	O
sense	O
of	O
authority	O
and	O
his	O
assertive	O
self	O
.	O
He	O
subsequently	O
developed	O
Clark	PERSON
's	O
timid	O
demeanor	O
as	O
a	O
means	O
of	O
ensuring	O
that	O
no	O
one	O
would	O
suspect	O
any	O
connection	O
between	O
the	O
two	O
alter-egos	O
.	O
In	O
the	O
modern	O
age	O
continuity	O
of	O
comics	O
,	O
Clark	PERSON
Kent	PERSON
's	O
favorite	O
movie	O
is	O
To	O
Kill	O
a	O
Mockingbird	O
(	O
in	O
which	O
Gregory	PERSON
Peck	PERSON
wears	O
glasses	O
not	O
unlike	O
Kent	PERSON
's	O
)	O
.	O
They	O
gave	O
him	O
the	O
name	O
of	O
Clark	PERSON
Kent	PERSON
,	O
and	O
he	O
later	O
got	O
a	O
job	O
as	O
a	O
newspaper	O
reporter	O
under	O
that	O
name	O
.	O
This	O
was	O
followed	O
nearly	O
two	O
decades	O
later	O
by	O
a	O
fifth	O
film	O
called	O
Superman	O
Returns	O
with	O
Brandon	PERSON
Routh	PERSON
giving	O
a	O
performance	O
very	O
similar	O
to	O
Reeve	PERSON
's	O
.	O
Clark	PERSON
Kent	PERSON
's	O
character	O
is	O
given	O
one	O
of	O
its	O
heaviest	O
emphases	O
in	O
the	O
1990s	O
series	O
Lois	O
&	O
Clark	PERSON
:	O
The	O
New	O
Adventures	O
of	O
Superman	O
.	O
It	O
is	O
made	O
very	O
clear	O
during	O
the	O
series	O
,	O
even	O
discussed	O
directly	O
by	O
the	O
characters	O
,	O
that	O
Clark	PERSON
Kent	PERSON
is	O
who	O
he	O
really	O
is	O
,	O
rather	O
than	O
his	O
superheroic	O
alter-ego	O
.	O
In	O
Lois	PERSON
and	O
Clark	PERSON
,	O
Kent	PERSON
(	PERSON
Dean	PERSON
Cain	PERSON
)	O
is	O
a	O
stereotypical	O
wide-eyed	O
farm	O
kid	O
from	O
Kansas	LOCATION
with	O
the	O
charm	O
,	O
grace	O
and	O
humor	O
of	O
George	PERSON
Reeves	PERSON
,	O
but	O
without	O
the	O
awkward	O
geekiness	O
of	O
Christopher	PERSON
Reeve	PERSON
.	O
Clark	PERSON
Kent	PERSON
is	O
played	O
by	O
Tom	PERSON
Welling	PERSON
,	O
with	O
others	O
portraying	O
Clark	PERSON
as	O
an	O
infant	O
.	O
A	O
modest	O
amount	O
of	O
religious	O
imagery	O
is	O
seen	O
occasionally	O
in	O
the	O
series	O
,	O
but	O
to	O
a	O
lesser	O
degree	O
than	O
in	O
the	O
Christopher	PERSON
Reeve	PERSON
series	O
.	O
Smallville	O
's	O
Kent	PERSON
is	O
particularly	O
inwardly	O
conflicted	O
as	O
he	O
attempts	O
to	O
live	O
the	O
life	O
of	O
a	O
normal	O
human	O
being	O
,	O
while	O
keeping	O
the	O
secret	O
of	O
his	O
alien	O
heritage	O
from	O
his	O
friends	O
.	O
Throughout	O
the	O
series	O
he	O
has	O
an	O
intermittent	O
relationship	O
with	O
Lana	PERSON
Lang	PERSON
,	O
which	O
is	O
strained	O
by	O
his	O
secret	O
.	O
Clark	PERSON
's	O
powers	O
appear	O
over	O
time	O
.	O
In	O
contrast	O
to	O
previous	O
incarnations	O
of	O
the	O
character	O
,	O
this	O
Clark	PERSON
Kent	PERSON
starts	O
out	O
best	O
friends	O
with	O
Lex	PERSON
Luthor	PERSON
,	O
whom	O
he	O
meets	O
after	O
saving	O
the	O
latter	O
's	O
life	O
.	O
In	O
Smallville	O
,	O
Clark	PERSON
and	O
Lex	PERSON
remain	O
entangled	O
for	O
most	O
of	O
the	O
series	O
.	O
Lex	PERSON
would	O
like	O
to	O
transcend	O
his	O
family	O
background	O
and	O
be	O
a	O
better	O
person	O
than	O
his	O
father	O
,	O
but	O
after	O
multiple	O
setbacks	O
he	O
slowly	O
slips	O
into	O
evil	O
.	O
In	O
turn	O
,	O
Clark	PERSON
Kent	PERSON
has	O
a	O
slightly	O
dark	O
side	O
with	O
which	O
he	O
comes	O
to	O
grips	O
over	O
time	O
.	O
The	O
younger	O
Luthor	PERSON
slightly	O
envies	O
Clark	PERSON
's	O
'	O
clean-cut	O
'	O
and	O
wholesome	O
parents	O
(	O
who	O
disapprove	O
of	O
Clark	PERSON
's	O
friendship	O
with	O
Luthor	PERSON
)	O
,	O
while	O
Clark	PERSON
is	O
impressed	O
with	O
Luthor	PERSON
's	O
wealth	O
.	O
Even	O
in	O
his	O
better	O
days	O
,	O
Luthor	PERSON
is	O
highly	O
ambitious	O
for	O
power	O
and	O
wealth	O
,	O
at	O
one	O
time	O
noting	O
that	O
he	O
shares	O
his	O
name	O
with	O
Alexander	PERSON
the	PERSON
Great	PERSON
.	O
Smallville	O
'	O
s	O
Kent	PERSON
has	O
also	O
appeared	O
in	O
various	O
literature	O
(	O
including	O
comics	O
and	O
over	O
a	O
dozen	O
young	O
adult	O
novels	O
)	O
based	O
on	O
the	O
television	O
series	O
,	O
none	O
of	O
which	O
directly	O
continue	O
from	O
or	O
into	O
the	O
television	O
episodes	O
.	O
At	O
the	O
near	O
end	O
of	O
each	O
short	O
,	O
Clark	PERSON
gives	O
out	O
a	O
smile	O
and	O
a	O
wink	O
to	O
the	O
audience	O
.	O
In	O
the	O
Superman	O
:	O
The	O
Animated	O
Series	O
of	O
the	O
late	O
1990s	O
,	O
Kent	PERSON
is	O
based	O
on	O
John	PERSON
Byrne	PERSON
's	O
version	O
of	O
him	O
,	O
becoming	O
more	O
assertive	O
and	O
intelligent	O
.	O
In	O
the	O
1970s	O
,	O
one	O
suggestion	O
was	O
that	O
the	O
lenses	O
of	O
Clark	PERSON
Kent	PERSON
's	O
glasses	O
(	O
made	O
of	O
Kryptonian	O
materials	O
)	O
constantly	O
amplified	O
a	O
low-level	O
super-hypnosis	O
power	O
,	O
thereby	O
creating	O
the	O
illusion	O
of	O
others	O
viewing	O
Clark	PERSON
Kent	PERSON
as	O
a	O
weak	O
and	O
frailer	O
being	O
.	O
However	O
,	O
this	O
reason	O
was	O
abandoned	O
almost	O
as	O
quickly	O
as	O
it	O
was	O
introduced	O
,	O
since	O
it	O
had	O
various	O
flaws	O
(	O
such	O
as	O
stories	O
where	O
Batman	PERSON
would	O
disguise	O
himself	O
as	O
Clark	PERSON
Kent	PERSON
,	O
among	O
others	O
)	O
.	O
Clark	PERSON
's	O
glasses	O
diffuse	O
the	O
color	O
and	O
make	O
his	O
eyes	O
appear	O
more	O
human	O
in	O
that	O
identity	O
.	O
It	O
is	O
also	O
stated	O
by	O
Barry	PERSON
Allen	PERSON
that	O
"	O
Clark	PERSON
slouches	O
,	O
wears	O
clothes	O
two	O
sizes	O
too	O
big	O
and	O
raises	O
his	O
voice	O
an	O
octave	O
"	O
as	O
part	O
of	O
the	O
disguise	O
,	O
making	O
him	O
seem	O
shorter	O
and	O
overweight	O
instead	O
of	O
muscular	O
.	O
Some	O
fans	O
have	O
noted	O
that	O
in	O
order	O
for	O
the	O
disguise	O
to	O
be	O
credible	O
,	O
Clark	PERSON
has	O
to	O
be	O
at	O
least	O
as	O
skilled	O
an	O
actor	O
as	O
Christopher	PERSON
Reeve	PERSON
.	O
The	O
films	O
have	O
Clark	PERSON
Kent	PERSON
being	O
massively	O
clumsy	O
,	O
paranoid	O
and	O
of	O
course	O
mild	O
mannered	O
.	O
The	O
actor	O
's	O
portrayal	O
of	O
Clark	PERSON
in	O
the	O
Superman	O
film	O
series	O
was	O
praised	O
for	O
making	O
the	O
disguise	O
's	O
effectiveness	O
credible	O
to	O
audiences	O
.	O
In	O
his	O
book	O
Still	O
Me	O
,	O
Reeve	PERSON
says	O
he	O
based	O
Clark	PERSON
Kent	PERSON
on	O
Cary	PERSON
Grant	PERSON
's	O
nerdy	O
character	O
in	O
Bringing	O
up	O
Baby	O
.	O
[	O
citation	O
needed	O
]	O
Reeves	PERSON
played	O
Clark	PERSON
as	O
moderately	O
assertive	O
,	O
often	O
taking	O
charge	O
in	O
dangerous	O
or	O
risky	O
situations	O
and	O
unafraid	O
to	O
take	O
reasonable	O
risks	O
.	O
Actor	O
Dean	PERSON
Cain	PERSON
's	O
approach	O
in	O
the	O
1990s	O
series	O
Lois	O
&	O
Clark	PERSON
:	O
The	O
New	O
Adventures	O
of	O
Superman	PERSON
was	O
to	O
have	O
Clark	PERSON
as	O
a	O
normal	O
,	O
shy	O
,	O
everyday	O
guy	O
demonstrating	O
occasional	O
touches	O
of	O
clumsiness	O
(	O
e.g.	O
,	O
pretending	O
to	O
burn	O
his	O
mouth	O
on	O
coffee	O
)	O
,	O
but	O
still	O
a	O
highly	O
skilled	O
journalist	O
,	O
much	O
like	O
the	O
current	O
post-Crisis	O
portrayal	O
.	O
In	O
the	O
comic	O
books	O
and	O
in	O
the	O
George	PERSON
Reeves	PERSON
television	O
series	O
,	O
he	O
favors	O
the	O
Daily	O
Planet	O
'	O
s	O
store	O
room	O
for	O
his	O
changes	O
of	O
identities	O
.	O
(	O
The	O
heroic	O
change	O
between	O
identities	O
within	O
the	O
store	O
room	O
is	O
almost	O
always	O
seen	O
in	O
the	O
comics	O
,	O
but	O
never	O
viewed	O
in	O
the	O
Reeves	O
series	O
.	O
)	O
In	O
Lois	PERSON
&	O
Clark	PERSON
,	O
Clark	PERSON
's	O
usual	O
method	O
of	O
changing	O
was	O
to	O
either	O
"	O
suddenly	O
"	O
remember	O
something	O
urgent	O
that	O
required	O
his	O
immediate	O
attention	O
or	O
leave	O
the	O
room/area	O
under	O
the	O
pretense	O
of	O
contacting	O
a	O
source	O
,	O
summoning	O
the	O
police	O
,	O
heading	O
to	O
a	O
breaking	O
story	O
's	O
location	O
,	O
etc.	O
Clark	PERSON
also	O
developed	O
a	O
method	O
of	O
rapidly	O
spinning	O
into	O
his	O
costume	O
at	O
super	O
speed	O
which	O
became	O
a	O
trademark	O
change	O
,	O
especially	O
during	O
the	O
third	O
and	O
fourth	O
seasons	O
of	O
the	O
series	O
,	O
and	O
extremely	O
popular	O
with	O
the	O
show	O
's	O
fans	O
.	O
As	O
a	O
dramatic	O
plot	O
device	O
,	O
Clark	PERSON
often	O
has	O
to	O
quickly	O
improvise	O
in	O
order	O
to	O
find	O
a	O
way	O
to	O
change	O
unnoticed	O
.	O
Later	O
in	O
the	O
film	O
,	O
when	O
the	O
need	O
to	O
change	O
is	O
more	O
urgent	O
(	O
as	O
he	O
believes	O
the	O
city	O
is	O
about	O
to	O
be	O
poisoned	O
by	O
Lex	PERSON
Luthor	PERSON
)	PERSON
,	O
he	O
simply	O
jumps	O
out	O
a	O
window	O
of	O
the	O
Daily	O
Planet	O
offices	O
,	O
changing	O
at	O
super-speed	O
as	O
he	O
falls	O
,	O
and	O
flies	O
off	O
.	O
In	O
one	O
scene	O
of	O
Batman	O
:	O
The	O
Dark	O
Knight	O
Returns	O
,	O
Clark	PERSON
becomes	O
aware	O
of	O
an	O
emergency	O
while	O
talking	O
with	O
Bruce	PERSON
Wayne	PERSON
,	O
and	O
in	O
the	O
next	O
panel	O
he	O
has	O
flown	O
out	O
of	O
his	O
Kent	O
clothing	O
and	O
glasses	O
so	O
quickly	O
that	O
they	O
have	O
had	O
no	O
time	O
to	O
fall	O
.	O
This	O
reveals	O
to	O
the	O
citizens	O
of	O
Metropolis	LOCATION
that	O
a	O
superhero	O
is	O
among	O
them	O
,	O
and	O
the	O
name	O
"	O
The	O
Red-Blue	O
Blur	O
"	O
is	O
coined	O
.	O
When	O
Jimmy	PERSON
Olsen	PERSON
becomes	O
suspicious	O
,	O
Clark	PERSON
decides	O
to	O
reserve	O
his	O
usual	O
red-and-blue	O
for	O
saving	O
people	O
.	O
As	O
a	O
result	O
of	O
their	O
rearing	O
,	O
Kal-El	PERSON
has	O
grown	O
to	O
think	O
of	O
himself	O
as	O
Clark	PERSON
Kent	PERSON
,	O
and	O
in	O
fact	O
was	O
completely	O
unaware	O
of	O
his	O
alien	O
heritage	O
until	O
he	O
was	O
well	O
into	O
adulthood	O
.	O
It	O
should	O
be	O
noted	O
that	O
"	O
bumbling	O
"	O
Clark	PERSON
is	O
an	O
act	O
,	O
but	O
some	O
fans	O
dislike	O
the	O
portrayal	O
of	O
Clark	PERSON
as	O
bumbling	O
and	O
goofy	O
,	O
as	O
they	O
feel	O
it	O
marginalizes	O
his	O
importance	O
to	O
the	O
character	O
.	O
Rather	O
the	O
reverse	O
relationship	O
exists	O
between	O
Bruce	PERSON
Wayne	PERSON
and	O
Batman	PERSON
,	O
in	O
whose	O
case	O
Bruce	PERSON
Wayne	PERSON
is	O
the	O
fiction	O
and	O
Batman	PERSON
is	O
the	O
reality	O
.	O
A	O
good	O
example	O
of	O
this	O
view	O
is	O
an	O
adventure	O
published	O
in	O
the	O
1960s	O
when	O
Kent	PERSON
finds	O
himself	O
at	O
a	O
loose	O
end	O
when	O
staff	O
at	O
the	O
Daily	O
Planet	O
go	O
on	O
strike	O
and	O
seriously	O
considers	O
it	O
a	O
chance	O
to	O
try	O
out	O
a	O
new	O
identity	O
in	O
case	O
he	O
has	O
"	O
to	O
abandon	O
[	O
his	O
]	O
Clark	PERSON
Kent	PERSON
role	O
permanently	O
"	O
.	O
His	O
options	O
include	O
becoming	O
a	O
full-time	O
policeman	O
or	O
even	O
a	O
mere	O
tramp	O
"	O
whom	O
no	O
one	O
would	O
ever	O
suspect	O
of	O
being	O
the	O
Man	ORGANIZATION
of	ORGANIZATION
Steel	ORGANIZATION
.	O
"	O
I	O
need	O
to	O
be	O
Clark	PERSON
.	O
Both	O
Richard	PERSON
Donner	PERSON
,	O
director	O
of	O
the	O
first	O
Reeve	PERSON
movie	O
and	O
Bryan	PERSON
Singer	PERSON
,	O
director	O
of	O
the	O
Brandon	PERSON
Routh	PERSON
followup	O
,	O
have	O
stated	O
that	O
Clark	PERSON
Kent	PERSON
is	O
intended	O
to	O
be	O
the	O
disguise	O
.	O
Clark	PERSON
is	O
who	O
I	O
am	O
.	O
"	O
Clark	PERSON
ends	O
up	O
on	O
Krypton	O
,	O
where	O
he	O
is	O
adopted	O
by	O
Jor-El	PERSON
and	O
becomes	O
the	O
planet	O
's	O
Green	O
Lantern	O
.	O
