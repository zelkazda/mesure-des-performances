During	O
its	O
3,000-year	O
history	O
,	O
Aramaic	O
has	O
served	O
variously	O
as	O
a	O
language	O
of	O
administration	O
of	O
empires	O
and	O
as	O
a	O
language	O
of	O
divine	O
worship	O
.	O
Aramaic	O
's	O
long	O
history	O
and	O
diverse	O
and	O
widespread	O
use	O
has	O
led	O
to	O
the	O
development	O
of	O
many	O
divergent	O
varieties	O
which	O
are	O
sometimes	O
treated	O
as	O
dialects	O
.	O
Therefore	O
,	O
there	O
is	O
no	O
one	O
singular	O
Aramaic	O
language	O
,	O
but	O
each	O
time	O
and	O
place	O
has	O
had	O
its	O
own	O
variation	O
.	O
The	O
Aramaic	O
languages	O
are	O
considered	O
to	O
be	O
endangered	O
.	O
During	O
the	O
Neo-Assyrian	O
and	O
the	O
Neo-Babylonian	O
period	O
,	O
Aramaeans	O
,	O
the	O
native	O
speakers	O
of	O
Aramaic	O
,	O
began	O
to	O
settle	O
in	O
greater	O
numbers	O
in	O
Mesopotamia	LOCATION
(	O
modern-day	O
Syria	LOCATION
,	O
Iraq	LOCATION
and	O
eastern	O
Turkey	LOCATION
)	O
.	O
The	O
influx	O
eventually	O
resulted	O
in	O
the	O
Assyrian	O
and	O
Babylonian	O
empires	O
becoming	O
operationally	O
bilingual	O
,	O
with	O
Aramaic	O
used	O
alongside	O
Akkadian	O
.	O
As	O
these	O
empires	O
,	O
and	O
the	O
Persian	O
Empire	O
that	O
followed	O
,	O
extended	O
their	O
influence	O
in	O
the	O
region	O
,	O
Aramaic	O
gradually	O
became	O
the	O
lingua	O
franca	O
of	O
most	O
of	O
Western	LOCATION
Asia	LOCATION
and	O
Egypt	LOCATION
.	O
The	O
turbulence	O
of	O
the	O
last	O
two	O
centuries	O
has	O
seen	O
speakers	O
of	O
first-language	O
and	O
literary	O
Aramaic	O
dispersed	O
throughout	O
the	O
world	O
.	O
Traditionally	O
,	O
Aramaic	O
is	O
considered	O
a	O
single	O
language	O
.	O
Some	O
Aramaic	O
dialects	O
are	O
mutually	O
intelligible	O
,	O
whereas	O
others	O
are	O
not	O
.	O
It	O
is	O
also	O
helpful	O
to	O
draw	O
a	O
distinction	O
between	O
those	O
Aramaic	O
languages	O
that	O
are	O
modern	O
living	O
languages	O
,	O
those	O
that	O
are	O
still	O
in	O
use	O
as	O
literary	O
languages	O
,	O
and	O
those	O
that	O
are	O
extinct	O
and	O
are	O
only	O
of	O
interest	O
to	O
scholars	O
.	O
In	O
time	O
,	O
Aramaic	O
developed	O
its	O
distinctive	O
'	O
square	O
'	O
style	O
.	O
The	O
history	O
of	O
Aramaic	O
is	O
broken	O
down	O
into	O
three	O
broad	O
periods	O
:	O
As	O
such	O
,	O
the	O
term	O
covers	O
over	O
thirteen	O
centuries	O
of	O
the	O
development	O
of	O
Aramaic	O
.	O
This	O
vast	O
time	O
span	O
includes	O
all	O
Aramaic	O
that	O
is	O
now	O
effectively	O
extinct	O
.	O
After	O
the	O
fall	O
of	O
the	O
Achaemenid	LOCATION
Empire	LOCATION
,	O
local	O
vernaculars	O
became	O
increasingly	O
prominent	O
,	O
fanning	O
the	O
divergence	O
of	O
an	O
Aramaic	O
dialect	O
continuum	O
and	O
the	O
development	O
of	O
differing	O
written	O
standards	O
.	O
It	O
was	O
the	O
language	O
of	O
the	O
Aramaean	O
city-states	O
of	O
Damascus	LOCATION
,	O
Hamath	PERSON
and	O
Arpad	PERSON
.	O
These	O
inscriptions	O
are	O
mostly	O
diplomatic	O
documents	O
between	O
Aramaean	LOCATION
city-states	O
.	O
Different	O
dialects	O
emerged	O
in	O
Mesopotamia	LOCATION
,	O
Babylonia	LOCATION
,	O
the	O
Levant	PERSON
and	O
Egypt	LOCATION
.	O
However	O
,	O
the	O
Akkadian-influenced	O
Aramaic	O
of	O
Assyria	LOCATION
,	O
and	O
then	O
Babylon	LOCATION
,	O
started	O
to	O
come	O
to	O
the	O
fore	O
.	O
As	O
described	O
in	O
2	O
Kings	O
18:26	O
,	O
Hezekiah	PERSON
,	O
king	O
of	O
Judah	PERSON
,	O
negotiates	O
with	O
Assyrian	O
ambassadors	O
in	O
Aramaic	O
so	O
that	O
the	O
common	O
people	O
would	O
not	O
understand	O
.	O
'	O
Chaldee	O
'	O
or	O
'	O
Chaldean	O
Aramaic	O
'	O
used	O
to	O
be	O
common	O
terms	O
for	O
the	O
Aramaic	O
of	O
the	O
Chaldean	O
dynasty	O
of	O
Babylonia	LOCATION
.	O
It	O
is	O
not	O
to	O
be	O
confused	O
with	O
the	O
modern	O
language	O
Chaldean	PERSON
Neo-Aramaic	PERSON
.	O
Around	O
500	O
BCE	O
,	O
following	O
the	O
Achaemenid	O
conquest	O
of	O
Mesopotamia	LOCATION
under	O
Darius	PERSON
I	O
,	O
Aramaic	O
(	O
as	O
had	O
been	O
used	O
in	O
that	O
region	O
)	O
was	O
adopted	O
by	O
the	O
conquerors	O
as	O
the	O
"	O
vehicle	O
for	O
written	O
communication	O
between	O
the	O
different	O
regions	O
of	O
the	O
vast	O
empire	O
with	O
its	O
different	O
peoples	O
and	O
languages	O
.	O
Imperial	ORGANIZATION
Aramaic	ORGANIZATION
was	O
highly	O
standardised	O
;	O
its	O
orthography	O
was	O
based	O
more	O
on	O
historical	O
roots	O
than	O
any	O
spoken	O
dialect	O
,	O
and	O
the	O
inevitable	O
influence	O
of	O
Persian	O
gave	O
the	O
language	O
a	O
new	O
clarity	O
and	O
robust	O
flexibility	O
.	O
Many	O
of	O
the	O
extant	O
documents	O
witnessing	O
to	O
this	O
form	O
of	O
Aramaic	O
come	O
from	O
Egypt	LOCATION
,	O
and	O
Elephantine	O
in	O
particular	O
.	O
Achaemenid	O
Aramaic	O
is	O
sufficiently	O
uniform	O
that	O
it	O
is	O
often	O
difficult	O
to	O
know	O
where	O
any	O
particular	O
example	O
of	O
the	O
language	O
was	O
written	O
.	O
A	O
group	O
of	O
thirty	O
Aramaic	O
documents	O
from	O
Bactria	LOCATION
have	O
been	O
recently	O
discovered	O
.	O
The	O
conquest	O
by	O
Alexander	PERSON
the	PERSON
Great	PERSON
did	O
not	O
destroy	O
the	O
unity	O
of	O
Aramaic	O
language	O
and	O
literature	O
immediately	O
.	O
Aramaic	O
that	O
bears	O
a	O
relatively	O
close	O
resemblance	O
to	O
that	O
of	O
the	O
fifth	O
century	O
BCE	O
can	O
be	O
found	O
right	O
up	O
to	O
the	O
early	O
second	O
century	O
BCE	O
.	O
During	O
Seleucid	O
rule	O
,	O
defiant	O
Jewish	O
propaganda	O
shaped	O
Aramaic	O
Daniel	PERSON
.	O
Under	O
the	O
category	O
of	O
post-Achaemenid	O
is	O
Hasmonaean	O
Aramaic	O
,	O
the	O
official	O
language	O
of	O
Hasmonaean	LOCATION
Judaea	LOCATION
(	O
142	O
--	O
37	O
BCE	O
)	O
.	O
It	O
is	O
written	O
quite	O
differently	O
from	O
Achaemenid	O
Aramaic	O
;	O
there	O
is	O
an	O
emphasis	O
on	O
writing	O
as	O
words	O
are	O
pronounced	O
rather	O
than	O
using	O
etymological	O
forms	O
.	O
The	O
original	O
,	O
Hasmonaean	O
targums	O
had	O
reached	O
Babylon	LOCATION
sometime	O
in	O
the	O
second	O
or	O
third	O
centuries	O
CE	O
.	O
They	O
were	O
then	O
reworked	O
according	O
to	O
the	O
contemporary	O
dialect	O
of	O
Babylon	LOCATION
to	O
create	O
the	O
language	O
of	O
the	O
standard	O
targums	O
.	O
This	O
combination	O
formed	O
the	O
basis	O
of	O
Babylonian	O
Jewish	O
literature	O
for	O
centuries	O
to	O
follow	O
.	O
It	O
is	O
the	O
mixing	O
of	O
literary	O
Hasmonaean	O
with	O
the	O
dialect	O
of	O
Galilee	LOCATION
.	O
It	O
is	O
the	O
dialect	O
of	O
Babylonian	O
private	O
documents	O
,	O
and	O
,	O
from	O
the	O
twelfth	O
century	O
,	O
all	O
Jewish	O
private	O
documents	O
are	O
in	O
Aramaic	O
.	O
It	O
is	O
based	O
on	O
Hasmonaean	O
with	O
very	O
few	O
changes	O
.	O
Some	O
Nabataean	O
Aramaic	O
inscriptions	O
exist	O
from	O
the	O
early	O
days	O
of	O
the	O
kingdom	O
,	O
but	O
most	O
are	O
from	O
the	O
first	O
four	O
centuries	O
CE	O
.	O
The	O
western	O
regional	O
dialects	O
of	O
Aramaic	O
followed	O
a	O
similar	O
course	O
to	O
those	O
of	O
the	O
east	O
.	O
This	O
is	O
the	O
dialect	O
of	O
the	O
oldest	O
manuscript	O
of	O
Enoch	PERSON
(	PERSON
c.	O
170	O
BCE	O
)	O
.	O
It	O
is	O
generally	O
believed	O
that	O
in	O
the	O
first	O
century	O
CE	O
,	O
Jews	O
in	O
Judaea	LOCATION
primarily	O
spoke	O
Aramaic	O
with	O
a	O
dwindling	O
number	O
using	O
Hebrew	O
as	O
a	O
native	O
language	O
.	O
Many	O
learned	O
Hebrew	O
as	O
a	O
liturgical	O
language	O
.	O
Latin	O
was	O
spoken	O
in	O
the	O
Roman	LOCATION
army	O
,	O
but	O
had	O
almost	O
no	O
impact	O
on	O
the	O
linguistic	O
landscape	O
.	O
In	O
addition	O
to	O
the	O
formal	O
,	O
literary	O
dialects	O
of	O
Aramaic	O
based	O
on	O
Hasmonaean	LOCATION
and	O
Babylonian	O
there	O
were	O
a	O
number	O
of	O
colloquial	O
Aramaic	O
dialects	O
.	O
Samaria	LOCATION
had	O
its	O
distinctive	O
Samaritan	O
Aramaic	O
,	O
where	O
the	O
consonants	O
'	O
he	O
'	O
,	O
'	O
heth	O
'	O
and	O
'	O
'	O
ayin	O
'	O
all	O
became	O
pronounced	O
as	O
'	O
aleph	O
'	O
.	O
The	O
three	O
languages	O
mutually	O
influenced	O
each	O
other	O
,	O
especially	O
Hebrew	O
and	O
Aramaic	O
.	O
Hebrew	O
words	O
entered	O
Jewish	O
Aramaic	O
(	O
mostly	O
technical	O
religious	O
words	O
but	O
also	O
everyday	O
words	O
like	O
'	O
ēṣ	O
'	O
wood	O
'	O
)	O
.	O
Vice	O
versa	O
,	O
Aramaic	O
words	O
entered	O
Hebrew	O
(	O
not	O
only	O
Aramaic	O
words	O
like	O
māmmôn	O
'	O
wealth	O
'	O
but	O
Aramaic	O
ways	O
of	O
using	O
words	O
like	O
making	O
Hebrew	O
rā'ûi	O
,	O
'	O
seen	O
'	O
mean	O
'	O
worthy	O
'	O
in	O
the	O
sense	O
of	O
'	O
seemly	O
'	O
,	O
which	O
is	O
a	O
loan	O
translation	O
of	O
Aramaic	O
ḥāzê	O
meaning	O
'	O
seen	O
'	O
and	O
'	O
worthy	O
'	O
)	O
.	O
The	O
2004	O
film	O
The	O
Passion	O
of	O
the	O
Christ	O
is	O
notable	O
for	O
its	O
use	O
of	O
much	O
dialogue	O
in	O
Aramaic	O
only	O
,	O
specially	O
reconstructed	O
by	O
a	O
scholar	O
,	O
but	O
not	O
an	O
Aramaic	O
specialist	O
,	O
William	PERSON
Fulco	PERSON
.	O
During	O
that	O
century	O
,	O
the	O
nature	O
of	O
the	O
various	O
Aramaic	O
languages	O
and	O
dialects	O
begins	O
to	O
change	O
.	O
This	O
period	O
began	O
with	O
the	O
translation	O
of	O
the	O
Bible	O
into	O
the	O
language	O
:	O
the	O
Peshitta	LOCATION
and	O
the	O
masterful	O
prose	O
and	O
poetry	O
of	O
Ephrem	O
the	O
Syrian	O
.	O
It	O
is	O
most	O
commonly	O
identified	O
with	O
the	O
language	O
of	O
the	O
Babylonian	O
Talmud	O
(	O
which	O
was	O
completed	O
in	O
the	O
seventh	O
century	O
)	O
and	O
of	O
post-Talmudic	O
literature	O
,	O
which	O
are	O
the	O
most	O
important	O
cultural	O
products	O
of	O
Babylonian	O
Jewry	O
.	O
The	O
most	O
important	O
epigraphic	O
sources	O
for	O
the	O
dialect	O
are	O
the	O
hundreds	O
of	O
Aramaic	O
magic	O
bowls	O
written	O
in	O
the	O
Jewish	O
script	O
.	O
Mandaic	O
is	O
a	O
sister	O
dialect	O
to	O
Jewish	O
Babylonian	O
Aramaic	O
,	O
though	O
it	O
is	O
both	O
linguistically	O
and	O
culturally	O
distinct	O
.	O
In	O
135	O
,	O
after	O
the	O
Bar	LOCATION
Kokhba	LOCATION
revolt	O
,	O
many	O
Jewish	O
leaders	O
,	O
expelled	O
from	O
Jerusalem	LOCATION
,	O
moved	O
to	O
Galilee	LOCATION
.	O
This	O
dialect	O
was	O
spoken	O
not	O
only	O
in	O
Galilee	LOCATION
,	O
but	O
also	O
in	O
the	O
surrounding	O
parts	O
.	O
The	O
Aramaic	O
dialect	O
of	O
the	O
Samaritan	O
community	O
is	O
earliest	O
attested	O
by	O
a	O
documentary	O
tradition	O
that	O
can	O
be	O
dated	O
back	O
to	O
the	O
fourth	O
century	O
.	O
The	O
Aramaic	O
speakers	O
have	O
preserved	O
their	O
traditions	O
with	O
printing	O
presses	O
and	O
now	O
with	O
electronic	O
media	O
.	O
The	O
last	O
two-hundred	O
years	O
have	O
not	O
been	O
good	O
to	O
Aramaic	O
speakers	O
.	O
For	O
Aramaic-speaking	O
Jews	O
1950	O
is	O
a	O
watershed	O
year	O
:	O
the	O
founding	O
of	O
the	O
state	O
of	O
Israel	LOCATION
and	O
consequent	O
Jewish	O
exodus	O
from	O
Arab	O
lands	O
,	O
including	O
Iraq	LOCATION
,	O
led	O
most	O
Iraqi	O
Jews	O
,	O
both	O
Aramaic-speaking	O
and	O
Arabic-speaking	O
Iraqi	O
Jews	O
,	O
to	O
emigrate	O
to	O
Israel	LOCATION
.	O
The	O
practical	O
extinction	O
of	O
many	O
Jewish	O
dialects	O
seems	O
imminent	O
.	O
However	O
,	O
they	O
also	O
have	O
roots	O
in	O
numerous	O
,	O
previously	O
unwritten	O
,	O
local	O
Aramaic	O
varieties	O
,	O
and	O
are	O
not	O
purely	O
the	O
direct	O
descendants	O
of	O
the	O
language	O
of	O
Ephrem	O
the	O
Syrian	O
.	O
The	O
Jewish	O
Modern	O
Aramaic	O
languages	O
are	O
now	O
mostly	O
spoken	O
in	O
Israel	LOCATION
,	O
and	O
most	O
are	O
facing	O
extinction	O
.	O
The	O
Jewish	O
varieties	O
that	O
have	O
come	O
from	O
communities	O
that	O
once	O
lived	O
between	O
Lake	LOCATION
Urmia	LOCATION
and	O
Mosul	LOCATION
are	O
not	O
all	O
mutually	O
intelligible	O
.	O
In	O
others	O
,	O
the	O
plain	O
of	O
Mosul	LOCATION
for	O
example	O
,	O
the	O
varieties	O
of	O
the	O
two	O
faith	O
communities	O
are	O
similar	O
enough	O
to	O
allow	O
conversation	O
.	O
A	O
related	O
language	O
,	O
Mlahsô	O
,	O
has	O
recently	O
become	O
extinct	O
.	O
It	O
is	O
quite	O
distinct	O
from	O
any	O
other	O
Aramaic	O
variety	O
.	O
Very	O
little	O
remains	O
of	O
Western	O
Aramaic	O
.	O
Each	O
dialect	O
of	O
Aramaic	O
has	O
its	O
own	O
distinctive	O
pronunciation	O
,	O
and	O
it	O
would	O
not	O
be	O
feasible	O
here	O
to	O
go	O
into	O
all	O
these	O
properties	O
.	O
Aramaic	O
has	O
a	O
phonological	O
palette	O
of	O
25	O
to	O
40	O
distinct	O
phonemes	O
.	O
In	O
particular	O
,	O
some	O
modern	O
Jewish	O
Aramaic	O
pronunciations	O
lack	O
the	O
series	O
of	O
'	O
emphatic	O
'	O
consonants	O
.	O
Other	O
dialects	O
have	O
borrowed	O
from	O
the	O
inventories	O
of	O
surrounding	O
languages	O
,	O
particularly	O
Arabic	O
,	O
Azerbaijani	O
,	O
Kurdish	O
,	O
Persian	O
and	O
Turkish	O
.	O
The	O
various	O
alphabets	O
used	O
for	O
writing	O
Aramaic	O
languages	O
have	O
twenty-two	O
letters	O
(	O
all	O
of	O
which	O
are	O
consonants	O
)	O
.	O
Aramaic	O
classically	O
uses	O
a	O
series	O
of	O
lightly	O
contrasted	O
plosives	O
and	O
fricatives	O
:	O
Each	O
member	O
of	O
a	O
certain	O
pair	O
is	O
written	O
with	O
the	O
same	O
letter	O
of	O
the	O
alphabet	O
in	O
most	O
writing	O
systems	O
(	O
that	O
is	O
,	O
p	O
and	O
f	O
are	O
written	O
with	O
the	O
same	O
letter	O
)	O
,	O
and	O
are	O
near	O
allophones	O
.	O
A	O
distinguishing	O
feature	O
of	O
Aramaic	O
phonology	O
is	O
the	O
presence	O
of	O
'	O
emphatic	O
'	O
consonants	O
.	O
Not	O
all	O
dialects	O
of	O
Aramaic	O
give	O
these	O
consonants	O
their	O
historic	O
values	O
.	O
Aramaic	O
classically	O
has	O
a	O
set	O
of	O
four	O
sibilants	O
:	O
In	O
addition	O
to	O
these	O
sets	O
,	O
Aramaic	O
has	O
the	O
nasal	O
consonants	O
m	O
and	O
n	O
,	O
and	O
the	O
approximants	O
r	O
(	O
usually	O
an	O
alveolar	O
trill	O
)	O
,	O
l	O
,	O
y	O
and	O
w	O
.	O
Aramaic	O
nouns	O
and	O
adjectives	O
are	O
inflected	O
to	O
show	O
gender	O
,	O
number	O
and	O
state	O
.	O
Aramaic	O
has	O
two	O
grammatical	O
genders	O
,	O
masculine	O
and	O
feminine	O
.	O
Jewish	O
varieties	O
,	O
however	O
,	O
often	O
use	O
he	O
instead	O
,	O
following	O
Hebrew	O
orthography	O
.	O
Aramaic	O
nouns	O
and	O
adjectives	O
can	O
exist	O
in	O
one	O
of	O
three	O
states	O
;	O
these	O
states	O
correspond	O
in	O
part	O
to	O
the	O
role	O
of	O
cases	O
in	O
other	O
languages	O
.	O
However	O
,	O
some	O
Jewish	O
Aramaic	O
texts	O
employ	O
the	O
letter	O
he	O
for	O
the	O
feminine	O
absolute	O
singular	O
.	O
Likewise	O
,	O
some	O
Jewish	O
Aramaic	O
texts	O
employ	O
the	O
Hebrew	O
masculine	O
absolute	O
singular	O
suffix	O
-îm	O
instead	O
of	O
-în	O
.	O
The	O
alternative	O
is	O
sometimes	O
called	O
the	O
'	O
gentilic	O
plural	O
'	O
for	O
its	O
prominent	O
use	O
in	O
ethnonyms	O
(	O
yəhûḏāyê	O
,	O
'	O
the	O
Jews	O
'	O
,	O
for	O
example	O
)	O
.	O
Possessive	O
phrases	O
in	O
Aramaic	O
can	O
either	O
be	O
made	O
with	O
the	O
construct	O
state	O
or	O
by	O
linking	O
two	O
nouns	O
with	O
the	O
relative	O
particle	O
d	O
[	O
î	O
]	O
--	O
.	O
The	O
Aramaic	O
verb	O
has	O
gradually	O
evolved	O
in	O
time	O
and	O
place	O
,	O
varying	O
between	O
varieties	O
of	O
the	O
language	O
.	O
Aramaic	O
also	O
employs	O
a	O
system	O
of	O
conjugations	O
,	O
or	O
verbal	O
stems	O
,	O
to	O
mark	O
intensive	O
and	O
extensive	O
developments	O
in	O
the	O
lexical	O
meaning	O
of	O
verbs	O
.	O
Aramaic	O
has	O
two	O
proper	O
tenses	O
:	O
perfect	O
and	O
imperfect	O
.	O
Because	O
this	O
variant	O
is	O
standard	O
in	O
Akkadian	O
,	O
it	O
is	O
possible	O
that	O
its	O
use	O
in	O
Aramaic	O
represents	O
loanwords	O
from	O
that	O
language	O
.	O
Aramaic	O
also	O
has	O
two	O
proper	O
tenses	O
:	O
the	O
perfect	O
and	O
the	O
imperfect	O
.	O
The	O
syntax	O
of	O
Aramaic	O
(	O
the	O
way	O
sentences	O
are	O
put	O
together	O
)	O
usually	O
follows	O
the	O
order	O
verb-subject-object	O
.	O
