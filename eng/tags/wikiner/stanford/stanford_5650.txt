The	O
word	O
is	O
derived	O
from	O
the	O
Hebrew	O
root	O
that	O
means	O
to	O
go	O
or	O
to	O
walk	O
.	O
Halakha	LOCATION
constitutes	O
the	O
practical	O
application	O
of	O
the	O
613	O
mitzvot	O
(	O
"	O
commandments	O
"	O
,	O
singular	O
:	O
mitzvah	O
)	O
in	O
the	O
Torah	O
,	O
(	O
the	O
five	O
books	O
of	O
Moses	O
,	O
the	O
"	O
Written	O
Law	O
"	O
)	O
as	O
developed	O
through	O
discussion	O
and	O
debate	O
in	O
the	O
classical	O
rabbinic	O
literature	O
,	O
especially	O
the	O
Mishnah	ORGANIZATION
and	O
the	O
Talmud	LOCATION
,	O
and	O
as	O
codified	O
in	O
the	O
Mishneh	O
Torah	O
or	O
Shulchan	ORGANIZATION
Aruch	ORGANIZATION
Halakha	ORGANIZATION
has	O
been	O
developed	O
and	O
pored	O
over	O
throughout	O
the	O
generations	O
since	O
before	O
500	O
BCE	O
,	O
in	O
a	O
constantly	O
expanding	O
collection	O
of	O
religious	O
literature	O
consolidated	O
in	O
the	O
Talmud	LOCATION
.	O
It	O
is	O
also	O
the	O
subject	O
of	O
intense	O
study	O
in	O
yeshivas	O
;	O
see	O
Torah	O
study	O
.	O
Classical	ORGANIZATION
Rabbinic	ORGANIZATION
Judaism	ORGANIZATION
has	O
two	O
basic	O
categories	O
of	O
laws	O
:	O
As	O
a	O
practical	O
matter	O
,	O
the	O
mitzvot	O
also	O
may	O
be	O
classified	O
in	O
line	O
with	O
how	O
they	O
might	O
be	O
implemented	O
after	O
the	O
destruction	O
of	O
the	O
Temple	O
.	O
Many	O
laws	O
pertaining	O
to	O
holiness	O
and	O
purity	O
can	O
no	O
longer	O
be	O
performed	O
,	O
absent	O
the	O
holy	O
Sanctuary	O
in	O
Jerusalem	LOCATION
.	O
However	O
,	O
Talmudic	O
texts	O
often	O
deal	O
with	O
laws	O
outside	O
these	O
apparent	O
subject	O
categories	O
.	O
Judaism	ORGANIZATION
regards	O
the	O
violation	O
of	O
the	O
commandments	O
,	O
the	O
mitzvot	O
,	O
to	O
be	O
a	O
sin	O
.	O
The	O
generic	O
Hebrew	O
word	O
for	O
any	O
kind	O
of	O
sin	O
is	O
aveira	O
(	O
"	O
transgression	O
"	O
)	O
.	O
Based	O
on	O
the	O
Tanakh	ORGANIZATION
Judaism	ORGANIZATION
describes	O
three	O
levels	O
of	O
sin	O
:	O
Since	O
the	O
fall	O
of	O
the	O
Temple	ORGANIZATION
,	O
executions	O
have	O
been	O
forbidden	O
.	O
Since	O
the	O
fall	O
of	O
the	O
autonomous	O
Jewish	O
communities	O
of	O
Europe	LOCATION
,	O
the	O
other	O
punishments	O
have	O
also	O
fallen	O
by	O
the	O
wayside	O
.	O
The	O
details	O
to	O
these	O
laws	O
are	O
codified	O
from	O
the	O
Talmudic	O
texts	O
in	O
the	O
Mishneh	O
Torah	O
.	O
In	O
branches	O
of	O
Judaism	O
that	O
follow	O
halakha	O
,	O
lay	O
individuals	O
make	O
numerous	O
ad-hoc	O
decisions	O
,	O
but	O
are	O
regarded	O
as	O
not	O
having	O
authority	O
to	O
decide	O
definitively	O
.	O
Since	O
the	O
days	O
of	O
the	O
Sanhedrin	LOCATION
,	O
however	O
,	O
no	O
body	O
or	O
authority	O
has	O
been	O
generally	O
regarded	O
as	O
having	O
the	O
authority	O
to	O
create	O
universally	O
recognized	O
precedents	O
.	O
Depending	O
on	O
the	O
stature	O
of	O
the	O
posek	O
and	O
the	O
quality	O
of	O
the	O
decision	O
,	O
an	O
interpretation	O
may	O
also	O
be	O
gradually	O
accepted	O
by	O
rabbis	O
and	O
members	O
of	O
similar	O
Jewish	O
communities	O
.	O
Within	O
certain	O
Jewish	O
communities	O
,	O
formal	O
organized	O
bodies	O
do	O
exist	O
.	O
Takkanot	PERSON
,	O
in	O
general	O
,	O
do	O
not	O
affect	O
or	O
restrict	O
observance	O
of	O
Torah	O
mitzvot	O
.	O
In	O
Talmudic	O
and	O
classical	O
halakhic	O
literature	O
,	O
this	O
authority	O
refers	O
to	O
the	O
authority	O
to	O
prohibit	O
some	O
things	O
that	O
would	O
otherwise	O
be	O
biblically	O
sanctioned	O
(	O
shev	O
v'al	O
ta'aseh	O
)	O
.	O
Another	O
rare	O
and	O
limited	O
form	O
of	O
takkanah	O
involved	O
overriding	O
Torah	O
prohibitions	O
.	O
(	O
Sanhedrin	O
)	O
Hermeneutics	O
is	O
the	O
study	O
of	O
rules	O
for	O
the	O
exact	O
determination	O
of	O
the	O
meaning	O
of	O
a	O
text	O
;	O
it	O
played	O
a	O
notable	O
role	O
in	O
early	O
rabbinic	O
Jewish	O
discussion	O
.	O
The	O
sages	O
investigated	O
the	O
rules	O
by	O
which	O
the	O
requirements	O
of	O
the	O
oral	O
law	O
were	O
derived	O
from	O
and	O
established	O
by	O
the	O
written	O
law	O
,	O
i.e.	O
the	O
Torah	O
.	O
It	O
is	O
certain	O
,	O
however	O
,	O
that	O
the	O
seven	O
middot	O
of	O
Hillel	PERSON
and	O
the	O
thirteen	O
of	O
Ishmael	PERSON
are	O
earlier	O
than	O
the	O
time	O
of	O
Hillel	PERSON
himself	O
,	O
who	O
was	O
the	O
first	O
to	O
transmit	O
them	O
.	O
The	O
middot	O
seem	O
to	O
have	O
been	O
first	O
laid	O
down	O
as	O
abstract	O
rules	O
by	O
the	O
teachers	O
of	O
Hillel	PERSON
,	O
though	O
they	O
were	O
not	O
immediately	O
recognized	O
by	O
all	O
as	O
valid	O
and	O
binding	O
.	O
They	O
propose	O
that	O
Judaism	ORGANIZATION
has	O
entered	O
a	O
phase	O
of	O
ethical	O
monotheism	O
,	O
and	O
that	O
the	O
laws	O
of	O
Judaism	O
are	O
only	O
remnants	O
of	O
an	O
earlier	O
stage	O
of	O
religious	O
evolution	O
,	O
and	O
need	O
not	O
be	O
followed	O
.	O
They	O
were	O
then	O
recorded	O
in	O
the	O
Mishnah	LOCATION
,	O
and	O
explained	O
in	O
the	O
Talmud	LOCATION
and	O
commentaries	O
throughout	O
history	O
,	O
including	O
today	O
.	O
Haredi	O
Jews	O
generally	O
hold	O
that	O
even	O
minhagim	O
(	O
customs	O
)	O
must	O
be	O
retained	O
and	O
existing	O
precedents	O
can	O
not	O
be	O
reconsidered	O
.	O
Rabbi	O
Yosef	PERSON
proceeded	O
to	O
systematically	O
discredit	O
the	O
evidence	O
that	O
the	O
former	O
marriage	O
had	O
ever	O
taken	O
place	O
.	O
Rabbi	O
Yosef	PERSON
then	O
found	O
reason	O
to	O
doubt	O
that	O
the	O
new	O
husband	O
was	O
ever	O
the	O
father	O
,	O
finding	O
that	O
because	O
the	O
ex-husband	O
occasionally	O
delivered	O
alimony	O
personally	O
,	O
an	O
ancient	O
presumption	O
(	O
one	O
of	O
many	O
)	O
that	O
any	O
time	O
a	O
husband	O
and	O
wife	O
are	O
alone	O
together	O
the	O
law	O
presumes	O
intercourse	O
has	O
taken	O
place	O
governed	O
the	O
case	O
.	O
The	O
Torah	O
and	O
the	O
Talmud	O
are	O
not	O
formal	O
codes	O
of	O
law	O
:	O
they	O
are	O
sources	O
of	O
law	O
.	O
