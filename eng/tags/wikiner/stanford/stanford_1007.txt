The	O
plum	O
pudding	O
model	O
of	O
the	O
atom	O
by	O
J.	PERSON
J.	PERSON
Thomson	PERSON
,	O
who	O
discovered	O
the	O
electron	O
in	O
1897	O
,	O
was	O
proposed	O
in	O
1904	O
before	O
the	O
discovery	O
of	O
the	O
atomic	O
nucleus	O
.	O
In	O
this	O
model	O
,	O
the	O
atom	O
is	O
composed	O
of	O
electrons	O
(	O
which	O
Thomson	PERSON
still	O
called	O
"	O
corpuscles	O
"	O
,	O
though	O
G.	PERSON
J.	PERSON
Stoney	PERSON
had	O
proposed	O
that	O
atoms	O
of	O
electricity	O
be	O
called	O
electrons	O
in	O
1894	O
)	O
surrounded	O
by	O
a	O
soup	O
of	O
positive	O
charge	O
to	O
balance	O
the	O
electrons	O
'	O
negative	O
charges	O
,	O
like	O
negatively-charged	O
"	O
plums	O
"	O
surrounded	O
by	O
positively-charged	O
"	O
pudding	O
"	O
.	O
With	O
this	O
model	O
,	O
Thomson	PERSON
abandoned	O
his	O
earlier	O
"	O
nebular	O
atom	O
"	O
hypothesis	O
in	O
which	O
the	O
atom	O
was	O
composed	O
of	O
immaterial	O
vorticies	O
.	O
Now	O
,	O
at	O
least	O
part	O
of	O
the	O
atom	O
was	O
to	O
be	O
composed	O
of	O
Thomson	PERSON
's	O
particulate	O
negative	O
corpuscles	O
,	O
although	O
the	O
rest	O
of	O
the	O
positively-charged	O
part	O
of	O
the	O
atom	O
remained	O
somewhat	O
nebulous	O
and	O
ill-defined	O
.	O
Finally	O
,	O
after	O
Henry	PERSON
Moseley	PERSON
's	O
work	O
showed	O
in	O
1913	O
that	O
the	O
nuclear	O
charge	O
was	O
very	O
close	O
to	O
the	O
atomic	O
number	O
,	O
Antonius	PERSON
Van	PERSON
den	PERSON
Broek	PERSON
suggested	O
that	O
atomic	O
number	O
is	O
nuclear	O
charge	O
.	O
In	O
Thomson	PERSON
's	O
model	O
,	O
electrons	O
were	O
free	O
to	O
rotate	O
in	O
rings	O
which	O
were	O
further	O
stabilized	O
by	O
interactions	O
between	O
the	O
electrons	O
,	O
and	O
spectra	O
were	O
to	O
be	O
accounted	O
for	O
by	O
energy	O
differences	O
of	O
different	O
ring	O
orbits	O
.	O
Thomson	PERSON
attempted	O
to	O
make	O
his	O
model	O
account	O
for	O
some	O
of	O
the	O
major	O
spectral	O
lines	O
known	O
for	O
some	O
elements	O
,	O
but	O
was	O
not	O
notably	O
successful	O
at	O
this	O
.	O
