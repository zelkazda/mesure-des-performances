The	O
geography	O
of	O
El	LOCATION
Salvador	LOCATION
is	O
unique	O
among	O
the	O
nations	O
of	O
Central	LOCATION
America	LOCATION
.	O
The	O
country	O
borders	O
the	O
North	LOCATION
Pacific	LOCATION
Ocean	LOCATION
to	O
the	O
south	O
and	O
southwest	O
,	O
with	O
Guatemala	LOCATION
to	O
the	O
north-northwest	O
and	O
Honduras	LOCATION
to	O
the	O
north-northeast	O
.	O
In	O
the	O
southeast	O
,	O
the	O
Golfo	PERSON
de	PERSON
Fonseca	PERSON
separates	O
it	O
from	O
Nicaragua	LOCATION
.	O
El	LOCATION
Salvador	LOCATION
is	O
the	O
smallest	O
Central	O
American	O
country	O
in	O
area	O
and	O
is	O
the	O
only	O
one	O
without	O
a	O
coastline	O
on	O
the	O
Caribbean	LOCATION
sea	LOCATION
.	O
Most	O
of	O
Central	LOCATION
America	LOCATION
and	O
the	O
Caribbean	LOCATION
Basin	LOCATION
rests	O
on	O
the	O
relatively	O
motionless	O
Caribbean	LOCATION
Plate	LOCATION
.	O
The	O
Pacific	LOCATION
Ocean	LOCATION
floor	O
,	O
however	O
,	O
is	O
being	O
carried	O
northeast	O
by	O
the	O
underlying	O
motion	O
of	O
the	O
Cocos	ORGANIZATION
Plate	ORGANIZATION
.	O
The	O
subduction	O
of	O
the	O
Cocos	ORGANIZATION
Plate	ORGANIZATION
accounts	O
for	O
the	O
frequency	O
of	O
earthquakes	O
near	O
the	O
coast	O
.	O
San	LOCATION
Salvador	LOCATION
was	O
destroyed	O
in	O
1756	O
and	O
1854	O
,	O
and	O
it	O
suffered	O
heavy	O
damage	O
in	O
the	O
1919	O
,	O
1982	O
,	O
and	O
1986	O
tremors	O
.	O
The	O
remaining	O
coastal	O
plains	O
are	O
referred	O
to	O
as	O
the	O
Pacific	LOCATION
lowlands	O
.	O
The	O
northern	O
range	O
of	O
mountains	O
,	O
the	O
Sierra	LOCATION
Madre	LOCATION
,	O
form	O
a	O
continuous	O
chain	O
along	O
the	O
border	O
with	O
Honduras	LOCATION
.	O
A	O
narrow	O
plain	O
extends	O
from	O
the	O
coastal	O
volcanic	O
range	O
to	O
the	O
Pacific	LOCATION
Ocean	LOCATION
.	O
This	O
region	O
has	O
a	O
width	O
ranging	O
from	O
one	O
to	O
thirty-two	O
kilometers	O
with	O
the	O
widest	O
section	O
in	O
the	O
east	O
,	O
adjacent	O
to	O
the	O
Golfo	PERSON
de	PERSON
Fonseca	PERSON
.	O
Originating	O
in	O
Guatemala	LOCATION
,	O
the	O
Rio	LOCATION
Lempa	LOCATION
cuts	O
across	O
the	O
northern	O
range	O
of	O
mountains	O
,	O
flows	O
along	O
much	O
of	O
the	O
central	O
plateau	O
,	O
and	O
finally	O
cuts	O
through	O
the	O
southern	O
volcanic	O
range	O
to	O
empty	O
into	O
the	O
Pacific	LOCATION
.	O
Other	O
rivers	O
are	O
generally	O
short	O
and	O
drain	O
the	O
Pacific	LOCATION
lowlands	O
or	O
flow	O
from	O
the	O
central	O
plateau	O
through	O
gaps	O
in	O
the	O
southern	O
mountain	O
range	O
to	O
the	O
Pacific	LOCATION
.	O
The	O
largest	O
lake	O
,	O
the	O
Lago	LOCATION
de	LOCATION
Ilopango	LOCATION
,	O
lies	O
just	O
to	O
the	O
east	O
of	O
the	O
capital	O
.	O
Izalco	PERSON
has	O
erupted	O
at	O
least	O
51	O
times	O
since	O
1770	O
.	O
The	O
Pacific	LOCATION
lowlands	O
are	O
uniformly	O
hot	O
;	O
the	O
central	O
plateau	O
and	O
mountain	O
areas	O
are	O
more	O
moderate	O
.	O
Rainfall	O
during	O
this	O
season	O
generally	O
comes	O
from	O
low	O
pressure	O
over	O
the	O
Pacific	LOCATION
and	O
usually	O
falls	O
in	O
heavy	O
afternoon	O
thunderstorms	O
.	O
The	O
highest	O
point	O
is	O
Cerro	ORGANIZATION
El	ORGANIZATION
Pital	ORGANIZATION
,	O
at	O
2,730	O
m	O
.	O
Environment	O
--	O
current	O
issues	O
:	O
deforestation	O
;	O
soil	O
erosion	O
;	O
water	O
pollution	O
;	O
contamination	O
of	O
soils	O
from	O
disposal	O
of	O
toxic	O
wastes	O
;	O
Hurricane	O
Mitch	O
damage	O
