Hannes	PERSON
Bok	PERSON
,	O
pseudonym	O
for	O
Wayne	PERSON
Woodard	PERSON
(	O
July	O
2	O
,	O
1914	O
--	O
April	O
11	O
,	O
1964	O
)	O
,	O
was	O
an	O
American	O
artist	O
and	O
illustrator	O
,	O
as	O
well	O
as	O
an	O
amateur	O
astrologer	O
and	O
writer	O
of	O
fantasy	O
fiction	O
and	O
poetry	O
.	O
His	O
paintings	O
achieved	O
a	O
luminous	O
quality	O
through	O
the	O
use	O
of	O
an	O
arduous	O
glazing	O
process	O
,	O
which	O
was	O
learned	O
from	O
his	O
mentor	O
,	O
Maxfield	PERSON
Parrish	PERSON
.	O
Bok	PERSON
was	O
the	O
first	O
artist	O
to	O
win	O
a	O
Hugo	O
Award	O
.	O
The	O
pseudonym	O
derives	O
from	O
Johann	PERSON
Sebastian	PERSON
Bach	PERSON
.	O
In	O
1937	O
,	O
Bok	PERSON
moved	O
to	O
Los	LOCATION
Angeles	LOCATION
,	O
where	O
he	O
met	O
Ray	PERSON
Bradbury	PERSON
.	O
and	O
became	O
acquainted	O
with	O
artists	O
like	O
Mark	PERSON
Tobey	PERSON
and	O
Morris	PERSON
Graves	PERSON
.	O
Bok	PERSON
had	O
corresponded	O
with	O
and	O
had	O
met	O
Maxfield	PERSON
Parrish	PERSON
(	O
ca	O
.	O
)	O
,	O
and	O
the	O
influence	O
of	O
Parrish	PERSON
's	O
art	O
on	O
Bok	PERSON
's	O
is	O
evident	O
in	O
his	O
choice	O
of	O
subject	O
matter	O
,	O
use	O
of	O
color	O
,	O
and	O
application	O
of	O
glazes	O
.	O
Bok	PERSON
was	O
also	O
gay	O
,	O
according	O
to	O
friends	O
Forrest	PERSON
J	PERSON
Ackerman	PERSON
and	O
Emil	PERSON
Petaja	PERSON
;	O
the	O
erotic	O
fantasy	O
elements	O
of	O
his	O
artwork	O
,	O
especially	O
his	O
male	O
nude	O
subjects	O
,	O
display	O
homoerotic	O
overtones	O
unusual	O
for	O
the	O
time	O
.	O
Like	O
his	O
contemporary	O
Virgil	PERSON
Finlay	PERSON
,	O
Hannes	PERSON
Bok	PERSON
broke	O
into	O
commercial	O
art	O
and	O
achieved	O
initial	O
career	O
success	O
as	O
a	O
Weird	O
Tales	O
artist	O
--	O
though	O
he	O
did	O
so	O
through	O
one	O
of	O
the	O
stranger	O
events	O
in	O
the	O
history	O
of	O
science	O
fiction	O
and	O
fantasy	O
.	O
In	O
the	O
summer	O
of	O
1939	O
,	O
Ray	PERSON
Bradbury	PERSON
carried	O
samples	O
of	O
Bok	PERSON
's	O
art	O
eastward	O
to	O
introduce	O
his	O
friend	O
's	O
work	O
to	O
magazine	O
editors	O
at	O
the	O
1st	O
World	O
Science	O
Fiction	O
Convention	O
.	O
This	O
was	O
a	O
bold	O
move	O
,	O
since	O
Bradbury	PERSON
was	O
a	O
neophyte	O
with	O
no	O
connections	O
to	O
commercial	O
art	O
or	O
the	O
magazine	O
industry	O
;	O
but	O
it	O
reflects	O
the	O
close	O
ties	O
within	O
the	O
fan	O
and	O
professional	O
community	O
.	O
Bradbury	PERSON
was	O
,	O
at	O
the	O
time	O
,	O
a	O
19-year-old	O
newspaper	O
seller	O
,	O
and	O
he	O
borrowed	O
funds	O
for	O
the	O
trip	O
from	O
fellow	O
science	O
fiction	O
fan	O
Forrest	PERSON
J	PERSON
Ackerman	PERSON
.	O
Bradbury	PERSON
succeeded	O
;	O
Farnsworth	PERSON
Wright	PERSON
,	O
editor	O
of	O
Weird	O
Tales	O
,	O
accepted	O
Bok	PERSON
's	O
art	O
,	O
which	O
debuted	O
in	O
the	O
December	O
1939	O
issue	O
of	O
Weird	O
Tales	O
.	O
More	O
than	O
50	O
issues	O
of	O
the	O
magazine	O
featured	O
Bok	PERSON
's	O
pen-and-ink	O
work	O
until	O
March	O
1954	O
.	O
Bok	PERSON
also	O
executed	O
six	O
color	O
covers	O
for	O
Weird	O
Tales	O
between	O
March	O
1940	O
and	O
March	O
1942	O
.	O
Weird	O
Tales	O
also	O
published	O
five	O
of	O
Bok	PERSON
's	O
stories	O
and	O
two	O
of	O
his	O
poems	O
between	O
1942	O
and	O
1951	O
.	O
As	O
the	O
years	O
passed	O
,	O
Bok	PERSON
became	O
prone	O
to	O
disagreements	O
with	O
editors	O
over	O
money	O
and	O
artistic	O
issues	O
;	O
he	O
grew	O
reclusive	O
and	O
mystical	O
,	O
and	O
preoccupied	O
with	O
the	O
occult	O
.	O
He	O
died	O
,	O
apparently	O
of	O
a	O
heart	O
attack	O
(	O
he	O
"	O
starved	O
to	O
death	O
"	O
according	O
to	O
Forrest	PERSON
J	PERSON
Ackerman	PERSON
)	O
,	O
at	O
the	O
age	O
of	O
49	O
.	O
Both	O
novels	O
have	O
been	O
repeatedly	O
re-issued	O
,	O
as	O
in	O
the	O
Ballantine	O
Adult	O
Fantasy	O
series	O
.	O
Bok	PERSON
also	O
was	O
allowed	O
to	O
complete	O
two	O
novellas	O
left	O
unfinished	O
by	O
A.	PERSON
Merritt	PERSON
at	O
the	O
time	O
of	O
his	O
death	O
in	O
1943	O
.	O
(	PERSON
Bok	PERSON
's	O
commitment	O
to	O
fantasy	O
and	O
science	O
fiction	O
had	O
occurred	O
in	O
1927	O
in	O
connection	O
with	O
Merritt	PERSON
's	O
The	O
Moon	O
Pool	O
in	O
Amazing	O
Stories	O
--	O
one	O
of	O
those	O
conversion	O
experiences	O
common	O
among	O
young	O
SF	O
fans	O
.	O
)	O
Bok	PERSON
is	O
better	O
known	O
for	O
his	O
art	O
than	O
for	O
his	O
fiction	O
.	O
His	O
striking	O
wraparound	O
cover	O
for	O
the	O
November	O
1963	O
issue	O
of	O
F	O
&	O
SF	O
,	O
illustrating	O
Roger	PERSON
Zelazny	PERSON
's	O
"	O
A	O
Rose	O
for	O
Ecclesiastes	O
"	O
,	O
was	O
published	O
in	O
the	O
last	O
months	O
of	O
his	O
life	O
.	O
A	O
member	O
of	O
the	O
Futurians	O
,	O
Bok	PERSON
won	O
the	O
first	O
Hugo	O
Award	O
for	O
Best	O
Cover/Professional	O
Artist	O
in	O
1953	O
.	O
The	O
science	O
fiction	O
and	O
fantasy	O
author	O
Emil	PERSON
Petaja	PERSON
(	O
1915	O
--	O
2000	O
)	O
was	O
lifelong	O
friend	O
of	O
Bok	PERSON
and	O
collector	O
of	O
his	O
work	O
.	O
The	O
two	O
had	O
much	O
in	O
common	O
--	O
including	O
an	O
interest	O
in	O
fantasy	O
fiction	O
,	O
the	O
Kalevala	LOCATION
,	O
and	O
the	O
music	O
of	O
Sibelius	PERSON
.	O
They	O
also	O
immersed	O
themselves	O
in	O
the	O
primordial	O
Los	LOCATION
Angeles	LOCATION
science	O
fiction	O
scene	O
.	O
Where	O
we	O
met	O
Ray	PERSON
Bradbury	PERSON
.	O
He	O
produced	O
a	O
dozen	O
color	O
paintings	O
based	O
on	O
Peer	O
Gynt	O
,	O
as	O
samples	O
to	O
show	O
book	O
publishers	O
what	O
he	O
could	O
do	O
.	O
