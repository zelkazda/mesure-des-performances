A	O
basic	O
objective	O
of	O
the	O
first	O
normal	O
form	O
defined	O
by	O
Codd	PERSON
in	O
1970	O
was	O
to	O
permit	O
data	O
to	O
be	O
queried	O
and	O
manipulated	O
using	O
a	O
"	O
universal	O
data	O
sub-language	O
"	O
grounded	O
in	O
first-order	O
logic	O
.	O
(	O
SQL	O
is	O
an	O
example	O
of	O
such	O
a	O
data	O
sub-language	O
,	O
albeit	O
one	O
that	O
Codd	PERSON
regarded	O
as	O
seriously	O
flawed	O
.	O
)	O
One	O
of	O
Codd	PERSON
's	O
important	O
insights	O
was	O
that	O
this	O
structural	O
complexity	O
could	O
always	O
be	O
removed	O
completely	O
,	O
leading	O
to	O
much	O
greater	O
power	O
and	O
flexibility	O
in	O
the	O
way	O
queries	O
could	O
be	O
formulated	O
(	O
by	O
users	O
and	O
applications	O
)	O
and	O
evaluated	O
(	O
by	O
the	O
DBMS	O
)	O
.	O
