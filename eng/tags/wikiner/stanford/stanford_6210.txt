The	O
theory	O
is	O
not	O
applied	O
on	O
European	O
integration	O
which	O
rejects	O
the	O
idea	O
of	O
neofunctionalism	O
.	O
The	O
theory	O
,	O
initially	O
proposed	O
by	O
Stanley	PERSON
Hoffmann	PERSON
suggests	O
that	O
national	O
governments	O
control	O
the	O
level	O
and	O
speed	O
of	O
European	O
integration	O
.	O
Various	O
intergovernmentalist	O
approaches	O
have	O
been	O
developed	O
in	O
the	O
literature	O
and	O
these	O
claim	O
to	O
be	O
able	O
to	O
explain	O
both	O
periods	O
of	O
radical	O
change	O
in	O
the	O
European	ORGANIZATION
Union	ORGANIZATION
(	O
because	O
of	O
the	O
converging	O
governmental	O
preferences	O
)	O
and	O
periods	O
of	O
inertia	O
(	O
due	O
to	O
the	O
diverging	O
national	O
interests	O
)	O
.	O
