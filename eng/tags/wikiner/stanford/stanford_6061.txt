In	O
the	O
Arabic	O
language	O
,	O
as	O
in	O
the	O
Hebrew	O
language	O
,	O
the	O
"	O
first	O
day	O
"	O
of	O
the	O
week	O
corresponds	O
with	O
Sunday	O
of	O
the	O
planetary	O
week	O
.	O
That	O
corresponded	O
to	O
the	O
year	O
AD	O
570	O
or	O
571	O
,	O
though	O
many	O
western	O
scholars	O
say	O
the	O
expedition	O
of	O
Abraha	PERSON
was	O
in	O
fact	O
much	O
earlier	O
.	O
Thus	O
,	O
Malaysia	LOCATION
,	O
Indonesia	LOCATION
,	O
and	O
a	O
few	O
others	O
begin	O
each	O
month	O
at	O
sunset	O
on	O
the	O
first	O
day	O
that	O
the	O
moon	O
sets	O
after	O
the	O
sun	O
(	O
moonset	O
after	O
sunset	O
)	O
.	O
Some	O
jurists	O
see	O
no	O
contradiction	O
between	O
Muhammad	PERSON
's	O
teachings	O
and	O
the	O
use	O
of	O
calculations	O
to	O
determine	O
the	O
beginnings	O
of	O
lunar	O
months	O
.	O
They	O
consider	O
that	O
Muhammad	PERSON
's	O
recommendation	O
was	O
adapted	O
to	O
the	O
culture	O
of	O
the	O
times	O
,	O
and	O
should	O
not	O
be	O
confused	O
with	O
the	O
acts	O
of	O
worship	O
.	O
So	O
did	O
the	O
"	O
Fiqh	ORGANIZATION
Council	ORGANIZATION
of	ORGANIZATION
North	ORGANIZATION
America	ORGANIZATION
"	O
in	O
2006	O
and	O
the	O
"	O
European	ORGANIZATION
Council	ORGANIZATION
for	ORGANIZATION
Fatwa	ORGANIZATION
and	ORGANIZATION
Research	ORGANIZATION
"	O
(	O
ECFR	O
)	O
in	O
2007	O
.	O
The	O
country	O
also	O
uses	O
the	O
Umm	O
al-Qura	O
calendar	O
,	O
based	O
on	O
astronomical	O
calculations	O
,	O
but	O
this	O
is	O
restricted	O
to	O
administrative	O
purposes	O
.	O
Since	O
the	O
beginning	O
of	O
AH	O
1423	O
(	O
March	O
16	O
,	O
2002	O
)	O
,	O
the	O
rule	O
has	O
been	O
clarified	O
a	O
little	O
by	O
requiring	O
the	O
geocentric	O
conjunction	O
of	O
the	O
sun	O
and	O
moon	O
to	O
occur	O
before	O
sunset	O
,	O
in	O
addition	O
to	O
requiring	O
moonset	O
to	O
occur	O
after	O
sunset	O
at	O
Mecca	LOCATION
.	O
In	O
2007	O
,	O
the	O
Islamic	ORGANIZATION
Society	ORGANIZATION
of	ORGANIZATION
North	ORGANIZATION
America	ORGANIZATION
,	O
the	O
Fiqh	ORGANIZATION
Council	ORGANIZATION
of	ORGANIZATION
North	ORGANIZATION
America	ORGANIZATION
and	O
the	O
European	ORGANIZATION
Council	ORGANIZATION
for	ORGANIZATION
Fatwa	ORGANIZATION
and	ORGANIZATION
Research	ORGANIZATION
announced	O
that	O
they	O
will	O
henceforth	O
use	O
a	O
calendar	O
based	O
on	O
calculations	O
,	O
using	O
the	O
same	O
parameters	O
as	O
the	O
Umm	O
al-Qura	O
calendar	O
,	O
to	O
determine	O
(	O
well	O
in	O
advance	O
)	O
the	O
beginning	O
of	O
all	O
lunar	O
months	O
(	O
and	O
therefore	O
the	O
days	O
associated	O
with	O
all	O
religious	O
observances	O
)	O
.	O
