Eureka	O
Stockade	O
in	O
Ballarat	LOCATION
,	O
Victoria	LOCATION
,	O
Australia	LOCATION
on	O
3	O
December	O
1854	O
was	O
prompted	O
by	O
grievances	O
over	O
heavily	O
priced	O
mining	O
items	O
,	O
the	O
expense	O
of	O
a	O
Miner	O
's	O
Licence	O
,	O
taxation	O
(	O
via	O
the	O
licence	O
)	O
without	O
representation	O
and	O
the	O
actions	O
of	O
the	O
government	O
and	O
its	O
agents	O
(	O
the	O
police	O
and	O
military	O
)	O
.	O
The	O
role	O
of	O
the	O
Eureka	O
Stockade	O
in	O
generating	O
public	O
support	O
for	O
these	O
demands	O
beyond	O
the	O
goldfields	O
resulted	O
in	O
Eureka	LOCATION
being	O
controversially	O
identified	O
with	O
the	O
birth	O
of	O
democracy	O
in	O
Australia	LOCATION
.	O
Rather	O
than	O
hear	O
the	O
grievances	O
,	O
he	O
increased	O
the	O
police	O
presence	O
in	O
the	O
gold	O
fields	O
and	O
summoned	O
reinforcements	O
from	O
Melbourne	LOCATION
.	O
On	O
28	O
November	O
1854	O
,	O
the	O
reinforcements	O
marching	O
from	O
Melbourne	LOCATION
were	O
attacked	O
by	O
a	O
crowd	O
of	O
miners	O
.	O
In	O
the	O
rising	O
tide	O
of	O
anger	O
and	O
resentment	O
amongst	O
the	O
miners	O
,	O
a	O
more	O
militant	O
leader	O
,	O
Peter	PERSON
Lalor	PERSON
,	O
was	O
elected	O
.	O
In	O
the	O
words	O
of	O
Lalor	PERSON
:	O
"	O
it	O
was	O
nothing	O
more	O
than	O
an	O
enclosure	O
to	O
keep	O
our	O
own	O
men	O
together	O
,	O
and	O
was	O
never	O
erected	O
with	O
an	O
eye	O
to	O
military	O
defence	O
"	O
.	O
Lalor	PERSON
was	O
the	O
leader	O
of	O
the	O
miners	O
who	O
fought	O
at	O
the	O
Eureka	LOCATION
Stockade	LOCATION
,	O
and	O
the	O
author	O
of	O
the	O
oath	O
of	O
allegiance	O
used	O
by	O
the	O
miners	O
at	O
the	O
Eureka	O
Stockade	O
which	O
he	O
swore	O
to	O
their	O
affirmation	O
.	O
After	O
the	O
battle	O
,	O
Lalor	PERSON
wrote	O
in	O
a	O
statement	O
to	O
the	O
colonists	O
of	O
Victoria	LOCATION
,	O
"	O
There	O
are	O
two	O
things	O
connected	O
with	O
the	O
late	O
outbreak	O
(	O
Eureka	O
)	O
which	O
I	O
deeply	O
regret	O
.	O
As	O
he	O
was	O
the	O
Eureka	LOCATION
hero	O
his	O
policies	O
were	O
not	O
scrutinised	O
at	O
all	O
before	O
the	O
election	O
and	O
his	O
later	O
voting	O
record	O
as	O
a	O
parliamentarian	O
shows	O
he	O
once	O
opposed	O
a	O
bill	O
to	O
introduce	O
full	O
white-male	O
suffrage	O
in	O
the	O
colony	O
of	O
Victoria	LOCATION
.	O
During	O
the	O
height	O
of	O
the	O
battle	O
,	O
Lalor	PERSON
was	O
shot	O
in	O
his	O
right	O
arm	O
,	O
took	O
refuge	O
under	O
some	O
timber	O
and	O
was	O
smuggled	O
out	O
of	O
the	O
stockade	O
and	O
hidden	O
.	O
Early	O
in	O
the	O
battle	O
"	O
Captain	O
"	O
Henry	PERSON
Ross	PERSON
was	O
shot	O
dead	O
.	O
According	O
to	O
Lalor	PERSON
's	O
report	O
,	O
fourteen	O
miners	O
died	O
inside	O
the	O
stockade	O
and	O
an	O
additional	O
eight	O
died	O
later	O
from	O
injuries	O
they	O
sustained	O
.	O
Three	O
months	O
after	O
the	O
Eureka	LOCATION
Stockade	LOCATION
,	O
Peter	PERSON
Lalor	PERSON
wrote	O
:	O
"	O
As	O
the	O
inhuman	O
brutalities	O
practised	O
by	O
the	O
troops	O
are	O
so	O
well	O
known	O
,	O
it	O
is	O
unnecessary	O
for	O
me	O
to	O
repeat	O
them	O
.	O
As	O
historian	O
Geoffrey	PERSON
Blainey	PERSON
has	O
commented	O
,	O
"	O
Every	O
government	O
in	O
the	O
world	O
would	O
probably	O
have	O
counter-attacked	O
in	O
the	O
face	O
of	O
the	O
building	O
of	O
the	O
stockade	O
.	O
"	O
He	O
was	O
tried	O
and	O
convicted	O
of	O
seditious	O
libel	O
by	O
a	O
Melbourne	LOCATION
jury	O
on	O
23	O
January	O
1855	O
and	O
,	O
after	O
a	O
series	O
of	O
appeals	O
,	O
sentenced	O
to	O
six	O
months	O
imprisonment	O
on	O
23	O
March	O
.	O
The	O
members	O
of	O
the	O
commission	O
were	O
appointed	O
before	O
Eureka	O
...	O
they	O
were	O
men	O
who	O
were	O
likely	O
to	O
be	O
sympathetic	O
to	O
the	O
diggers	O
.	O
"	O
When	O
its	O
report	O
was	O
handed	O
down	O
,	O
it	O
was	O
scathing	O
in	O
its	O
assessment	O
of	O
all	O
aspects	O
of	O
the	O
administration	O
of	O
the	O
gold	O
fields	O
,	O
and	O
particularly	O
the	O
Eureka	LOCATION
Stockade	LOCATION
affair	O
.	O
After	O
12	O
months	O
,	O
all	O
but	O
one	O
of	O
the	O
demands	O
of	O
the	O
Ballarat	ORGANIZATION
Reform	ORGANIZATION
League	ORGANIZATION
had	O
been	O
granted	O
.	O
Over	O
the	O
next	O
thirty	O
years	O
,	O
press	O
interest	O
in	O
the	O
events	O
that	O
had	O
taken	O
place	O
at	O
the	O
Eureka	LOCATION
Stockade	LOCATION
dwindled	O
,	O
but	O
Eureka	LOCATION
was	O
kept	O
alive	O
at	O
the	O
campfires	O
and	O
in	O
the	O
pubs	O
,	O
and	O
in	O
memorial	O
events	O
in	O
Ballarat	LOCATION
.	O
When	O
it	O
opened	O
in	O
Melbourne	LOCATION
,	O
the	O
exhibition	O
was	O
an	O
instant	O
hit	O
.	O
The	O
Age	O
reported	O
in	O
1891	O
that	O
"	O
it	O
afforded	O
a	O
very	O
good	O
opportunity	O
for	O
people	O
to	O
see	O
what	O
it	O
might	O
have	O
been	O
like	O
at	O
Eureka	LOCATION
"	O
.	O
The	O
people	O
of	O
Melbourne	LOCATION
flocked	O
to	O
the	O
cyclorama	O
,	O
paid	O
up	O
and	O
had	O
their	O
picture	O
taken	O
before	O
it	O
.	O
The	O
Eureka	O
Stockade	O
(	O
and	O
other	O
events	O
)	O
has	O
been	O
characterised	O
as	O
the	O
"	O
Birth	O
of	O
Australia	LOCATION
"	O
.	O
The	O
Eureka	O
Stockade	O
was	O
certainly	O
the	O
most	O
prominent	O
rebellion	O
in	O
Australia	LOCATION
's	O
history	O
and	O
,	O
depending	O
on	O
how	O
one	O
defines	O
rebellion	O
,	O
can	O
be	O
regarded	O
as	O
the	O
only	O
such	O
event	O
.	O
Others	O
,	O
however	O
,	O
maintain	O
that	O
Eureka	PERSON
was	O
a	O
seminal	O
event	O
and	O
that	O
it	O
marked	O
a	O
major	O
change	O
in	O
the	O
course	O
of	O
Australian	O
history	O
.	O
The	O
debate	O
remains	O
active	O
and	O
may	O
remain	O
so	O
as	O
long	O
as	O
Eureka	LOCATION
is	O
remembered	O
.	O
The	O
surviving	O
7	O
minute	O
fragment	O
(	O
original	O
length	O
unknown	O
)	O
shows	O
street	O
scenes	O
of	O
Ballarat	LOCATION
is	O
believed	O
to	O
be	O
part	O
of	O
the	O
1907	O
film	O
,	O
the	O
second	O
feature	O
film	O
made	O
in	O
Australia	LOCATION
(	O
after	O
the	O
1906	O
production	O
,	O
The	O
Story	O
of	O
the	O
Kelly	O
Gang	O
)	O
.	O
The	O
film	O
impressed	O
critics	O
of	O
the	O
time	O
and	O
was	O
found	O
to	O
be	O
a	O
stirring	O
portrayal	O
of	O
the	O
events	O
surrounding	O
the	O
Eureka	O
Stockade	O
,	O
but	O
failed	O
to	O
connect	O
with	O
audiences	O
during	O
the	O
two	O
weeks	O
it	O
was	O
screened	O
.	O
The	O
surviving	O
307	O
feet	O
of	O
the	O
35mm	O
film	O
(	O
5	O
mins	O
@	O
18fps	O
)	O
is	O
stored	O
at	O
the	O
National	O
Film	O
and	O
Sound	O
Archive	O
.	O
