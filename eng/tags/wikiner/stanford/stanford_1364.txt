For	O
a	O
detailed	O
discussion	O
of	O
the	O
differences	O
,	O
see	O
"	O
Biblical	O
canon	O
"	O
.	O
Note	O
that	O
this	O
table	O
uses	O
current	O
spellings	O
of	O
the	O
NAB	O
.	O
The	O
Third	O
Epistle	O
to	O
the	O
Corinthians	O
and	O
the	O
Testaments	O
of	O
the	O
Twelve	O
Patriarchs	O
were	O
once	O
considered	O
part	O
of	O
the	O
Armenian	O
Orthodox	O
Bible	O
,	O
but	O
are	O
no	O
longer	O
printed	O
with	O
modern	O
editions	O
.	O
Anglicanism	O
,	O
as	O
stated	O
in	O
the	O
Thirty-nine	O
Articles	O
,	O
consider	O
the	O
apocrypha	O
to	O
be	O
"	O
read	O
for	O
example	O
of	O
life	O
"	O
but	O
not	O
used	O
"	O
to	O
establish	O
any	O
doctrine	O
"	O
.	O
