The	O
word	O
can	O
nowadays	O
refer	O
to	O
the	O
geometrical	O
shape	O
unrelated	O
to	O
its	O
Christian	O
significance	O
.	O
For	O
example	O
,	O
celtic	O
coins	O
minted	O
many	O
centuries	O
before	O
the	O
Christian	O
era	O
may	O
have	O
an	O
entire	O
side	O
showing	O
this	O
type	O
of	O
cross	O
,	O
sometimes	O
with	O
the	O
cardinal	O
points	O
marked	O
by	O
concave	O
depressions	O
in	O
the	O
same	O
style	O
as	O
in	O
stone	O
age	O
carvings	O
.	O
It	O
appears	O
on	O
the	O
national	O
flags	O
of	O
Australia	LOCATION
,	O
Brazil	LOCATION
,	O
New	LOCATION
Zealand	LOCATION
,	O
Niue	LOCATION
,	O
Papua	LOCATION
New	LOCATION
Guinea	LOCATION
and	O
Samoa	LOCATION
.	O
The	O
tallest	O
cross	O
,	O
at	O
152.4	O
metres	O
high	O
,	O
is	O
part	O
of	O
Francisco	PERSON
Franco	PERSON
's	O
monumental	O
"	O
Valley	O
of	O
the	O
Fallen	O
"	O
,	O
the	O
Monumento	ORGANIZATION
Nacional	ORGANIZATION
de	ORGANIZATION
Santa	ORGANIZATION
Cruz	ORGANIZATION
del	ORGANIZATION
Valle	ORGANIZATION
de	ORGANIZATION
los	ORGANIZATION
Caidos	ORGANIZATION
in	O
Spain	LOCATION
.	O
