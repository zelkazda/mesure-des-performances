Only	O
in	O
the	O
Gospel	O
of	O
John	O
does	O
Jesus	O
talk	O
at	O
length	O
about	O
himself	O
and	O
his	O
divine	O
role	O
,	O
including	O
a	O
substantial	O
amount	O
of	O
material	O
Jesus	O
shared	O
with	O
the	O
disciples	O
only	O
.	O
The	O
gospel	O
includes	O
gnostic	O
elements	O
and	O
teaches	O
that	O
salvation	O
can	O
only	O
be	O
achieved	O
through	O
revealed	O
wisdom	O
,	O
specifically	O
belief	O
in	O
(	O
literally	O
belief	O
into	O
)	O
Jesus	O
.	O
Prominent	O
contemporary	O
scholars	O
regard	O
the	O
Gospel	O
of	O
John	O
as	O
more	O
theological	O
and	O
less	O
historical	O
than	O
the	O
synoptics	O
,	O
and	O
they	O
dispute	O
that	O
the	O
Apostle	O
John	O
was	O
the	O
author	O
.	O
John	O
's	O
picture	O
of	O
Jesus	O
is	O
very	O
different	O
from	O
that	O
which	O
emerges	O
as	O
a	O
recognisably	O
common	O
theme	O
,	O
underlying	O
the	O
accounts	O
in	O
the	O
synoptics	O
.	O
As	O
the	O
gospel	O
's	O
name	O
implies	O
,	O
the	O
author	O
has	O
traditionally	O
been	O
understood	O
to	O
be	O
the	O
Apostle	O
John	PERSON
.	O
Johannine	O
authorship	O
was	O
also	O
evidenced	O
by	O
Polycarp	O
,	O
(	O
who	O
is	O
said	O
to	O
have	O
known	O
the	O
apostles	O
)	O
,	O
Irenaeus	O
and	O
Eusebius	O
.	O
The	O
express	O
testimony	O
of	O
the	O
author	O
states	O
that	O
he	O
is	O
an	O
eyewitness	O
to	O
Jesus	O
and	O
that	O
his	O
testimony	O
is	O
true	O
.	O
In	O
their	O
view	O
,	O
the	O
Gospel	O
of	O
John	PERSON
is	O
an	O
account	O
composed	O
by	O
an	O
unknown	O
writer	O
who	O
may	O
have	O
never	O
met	O
Jesus	PERSON
.	O
They	O
also	O
argue	O
the	O
traditional	O
identification	O
of	O
the	O
book	O
's	O
author	O
,	O
denoted	O
in	O
the	O
text	O
as	O
the	O
"	O
beloved	O
disciple	O
"	O
,	O
with	O
the	O
apostle	O
John	O
is	O
false	O
.	O
Raymond	PERSON
E.	PERSON
Brown	PERSON
summarizes	O
a	O
prevalent	O
theory	O
regarding	O
the	O
development	O
of	O
this	O
gospel	O
.	O
Instead	O
,	O
it	O
tells	O
the	O
story	O
of	O
Jesus	O
for	O
both	O
believers	O
and	O
nonbelievers	O
.	O
The	O
hypothesis	O
of	O
the	O
Gospel	O
of	O
John	O
being	O
composed	O
in	O
layers	O
over	O
a	O
period	O
of	O
time	O
had	O
its	O
start	O
with	O
Rudolf	PERSON
Bultmann	PERSON
in	O
1941	O
.	O
Bultmann	O
's	O
conclusion	O
was	O
so	O
controversial	O
that	O
heresy	O
proceedings	O
were	O
instituted	O
against	O
him	O
and	O
his	O
writings	O
.	O
Most	O
scholars	O
believe	O
that	O
the	O
Gospel	O
of	O
John	O
was	O
composed	O
as	O
an	O
independent	O
source	O
from	O
the	O
synoptic	O
gospels	O
.	O
The	O
author	O
has	O
Jesus	PERSON
foretell	O
that	O
new	O
knowledge	O
will	O
come	O
to	O
his	O
followers	O
after	O
his	O
death	O
.	O
The	O
Rylands	O
Library	O
Papyrus	O
P52	O
,	O
which	O
records	O
a	O
fragment	O
of	O
this	O
gospel	O
,	O
is	O
usually	O
dated	O
to	O
the	O
first	O
half	O
of	O
the	O
second	O
century	O
.	O
In	O
the	O
1970s	O
,	O
Leon	PERSON
Morris	PERSON
and	O
John	PERSON
A.T.	PERSON
Robinson	O
independently	O
suggested	O
earlier	O
dates	O
for	O
the	O
gospel	O
's	O
composition	O
.	O
Many	O
phrases	O
are	O
duplicated	O
in	O
the	O
Gospel	O
of	O
John	PERSON
and	O
the	O
Dead	O
Sea	O
Scrolls	O
.	O
P52	O
is	O
small	O
,	O
and	O
although	O
a	O
plausible	O
reconstruction	O
can	O
be	O
attempted	O
for	O
most	O
of	O
the	O
fourteen	O
lines	O
represented	O
,	O
nevertheless	O
the	O
proportion	O
of	O
the	O
text	O
of	O
the	O
Gospel	O
of	O
John	O
for	O
which	O
it	O
provides	O
a	O
direct	O
witness	O
is	O
so	O
small	O
that	O
it	O
is	O
rarely	O
cited	O
in	O
textual	O
debate	O
.	O
Much	O
current	O
research	O
on	O
the	O
textual	O
history	O
of	O
the	O
Gospel	O
of	O
John	O
is	O
being	O
done	O
by	O
the	O
International	O
Greek	O
New	O
Testament	O
Project	O
.	O
This	O
section	O
recounts	O
Jesus	O
'	O
public	O
ministry	O
.	O
This	O
section	O
opens	O
with	O
an	O
account	O
of	O
the	O
Last	O
Supper	O
that	O
differs	O
significantly	O
from	O
that	O
found	O
in	O
the	O
synoptics	O
.	O
Here	O
,	O
Jesus	O
washes	O
the	O
disciples	O
feet	O
instead	O
of	O
ushering	O
in	O
a	O
new	O
covenant	O
of	O
his	O
body	O
and	O
blood	O
.	O
John	O
's	O
revelation	O
of	O
divinity	O
is	O
Jesus	O
'	O
triumph	O
over	O
death	O
,	O
the	O
eighth	O
and	O
greatest	O
sign	O
.	O
The	O
major	O
events	O
covered	O
by	O
the	O
Gospel	O
of	O
John	PERSON
include	O
:	O
The	O
synoptics	O
describe	O
much	O
more	O
of	O
Jesus	O
'	O
life	O
,	O
miracles	O
,	O
parables	O
,	O
and	O
exorcisms	O
.	O
In	O
the	O
synoptics	O
,	O
Jesus	O
speaks	O
mostly	O
about	O
the	O
Kingdom	O
of	O
God	O
.	O
John	O
also	O
promises	O
eternal	O
life	O
for	O
those	O
who	O
believe	O
in	O
Jesus	O
.	O
The	O
Jesus	O
Seminar	O
rated	O
this	O
account	O
as	O
black	O
,	O
containing	O
no	O
historically	O
accurate	O
information	O
.	O
E.	O
P.	O
Sanders	PERSON
and	O
other	O
critical	O
scholars	O
conclude	O
that	O
the	O
Gospel	O
of	O
John	O
contains	O
an	O
"	O
advanced	O
theological	O
development	O
,	O
in	O
which	O
meditations	O
of	O
the	O
person	O
and	O
work	O
of	O
Jesus	O
are	O
presented	O
in	O
the	O
first	O
person	O
as	O
if	O
Jesus	O
said	O
them	O
.	O
"	O
The	O
Gospel	O
of	O
John	O
also	O
differs	O
from	O
the	O
synoptic	O
gospels	O
in	O
respect	O
of	O
its	O
narrative	O
of	O
Jesus	O
'	O
life	O
and	O
ministry	O
;	O
but	O
here	O
there	O
is	O
a	O
lower	O
degree	O
of	O
consensus	O
that	O
the	O
synoptic	O
tradition	O
is	O
to	O
be	O
preferred	O
.	O
"	O
:p.26	O
Sanders	O
points	O
out	O
that	O
the	O
author	O
would	O
regard	O
the	O
gospel	O
as	O
theologically	O
true	O
as	O
revealed	O
spiritually	O
even	O
if	O
its	O
content	O
is	O
not	O
historically	O
accurate	O
.	O
There	O
is	O
no	O
a	O
priori	O
reason	O
to	O
reject	O
the	O
report	O
of	O
Jesus	O
and	O
his	O
disciples	O
'	O
conducting	O
a	O
ministry	O
of	O
baptism	O
for	O
a	O
time	O
.	O
A	O
distinctive	O
feature	O
of	O
the	O
Gospel	O
of	O
John	O
,	O
is	O
that	O
it	O
provides	O
a	O
very	O
different	O
chronology	O
of	O
Jesus	O
'	O
ministry	O
from	O
that	O
in	O
the	O
synoptics	O
.	O
and	O
the	O
unnamed	O
landlord	O
of	O
the	O
upper	O
room	O
,	O
who	O
knows	O
Jesus	O
as	O
'	O
the	O
Master	O
'	O
.	O
In	O
two	O
of	O
the	O
synoptics	O
,	O
Jesus	O
appears	O
to	O
recall	O
several	O
previous	O
preaching	O
ministries	O
in	O
Jerusalem	O
,	O
when	O
his	O
message	O
had	O
nevetheless	O
been	O
generally	O
spurned	O
.	O
John	O
was	O
written	O
somewhere	O
near	O
the	O
end	O
of	O
the	O
first	O
century	O
,	O
probably	O
in	O
Ephesus	O
,	O
in	O
Anatolia	O
.	O
Like	O
the	O
previous	O
gospels	O
,	O
it	O
circulated	O
separately	O
until	O
Irenaeus	PERSON
proclaimed	O
all	O
four	O
gospels	O
to	O
be	O
scripture	O
.	O
Online	O
translations	O
of	O
the	O
Gospel	O
of	O
John	PERSON
:	O
