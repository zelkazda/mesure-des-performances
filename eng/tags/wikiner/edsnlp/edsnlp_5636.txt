He	O
rigorously	O
documented	O
his	O
work	O
,	O
strongly	O
influencing	O
later	O
designers	O
;	O
for	O
this	O
reason	O
,	O
Lilienthal	PERSON
is	O
one	O
of	O
the	O
best	O
known	O
and	O
most	O
influential	O
early	O
aviation	O
pioneers	O
.	O
The	O
biplane	O
hang	O
glider	O
was	O
very	O
widely	O
publicized	O
in	O
public	O
magazines	O
with	O
plans	O
for	O
building	O
;	O
such	O
biplane	O
hang	O
gliders	O
were	O
constructed	O
and	O
flown	O
in	O
several	O
nations	O
since	O
Octave	PERSON
Chanute	PERSON
and	O
his	O
tailed	O
biplane	O
hang	O
gliders	O
were	O
demonstrated	O
.	O
In	O
1960-1962	O
Barry	PERSON
Hill	PERSON
Palmer	O
adapted	O
the	O
flexible	O
wing	O
concept	O
to	O
make	O
foot-launched	O
hang	O
gliders	O
with	O
four	O
different	O
control	O
arrangements	O
.	O
