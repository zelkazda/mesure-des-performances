Some	O
linguists	O
consider	O
these	O
three	O
varieties	O
,	O
despite	O
their	O
mutual	O
unintelligibility	O
,	O
to	O
be	O
dialects	O
of	O
one	O
single	O
Frisian	O
language	O
,	O
while	O
others	O
consider	O
them	O
to	O
be	O
three	O
separate	O
languages	O
,	O
as	O
do	O
their	O
speakers	O
.	O
Of	O
the	O
three	O
,	O
the	O
North	O
Frisian	O
language	O
especially	O
is	O
further	O
segmented	O
into	O
several	O
strongly	O
diverse	O
dialects	O
.	O
Stadsfries	O
is	O
not	O
Frisian	O
,	O
but	O
a	O
Dutch	O
dialect	O
influenced	O
by	O
Frisian	O
.	O
An	O
increasing	O
number	O
of	O
native	O
Dutch	O
speakers	O
in	O
the	O
province	O
are	O
learning	O
Frisian	O
as	O
a	O
second	O
language	O
.	O
In	O
the	O
Nordfriesland	O
(	O
North	O
Frisia	O
)	O
region	O
of	O
the	O
German	O
province	O
of	O
Schleswig-Holstein	O
,	O
there	O
are	O
10,000	O
North	O
Frisian	O
speakers	O
.	O
While	O
many	O
of	O
these	O
Frisians	O
live	O
on	O
the	O
mainland	O
,	O
most	O
are	O
found	O
on	O
the	O
islands	O
,	O
notably	O
Sylt	O
,	O
Föhr	O
,	O
Amrum	O
,	O
and	O
Heligoland	O
.	O
The	O
local	O
corresponding	O
North	O
Frisian	O
dialects	O
are	O
still	O
in	O
use	O
.	O
The	O
Ried	O
fan	O
de	O
Fryske	PERSON
Beweging	PERSON
took	O
care	O
of	O
the	O
Frisian	O
language	O
varieties	O
being	O
constitutionalized	O
.	O
Nevertheless	O
,	O
Saterland	O
Frisian	O
and	O
most	O
dialects	O
of	O
North	O
Frisian	O
are	O
seriously	O
endangered	O
.	O
At	O
that	O
time	O
,	O
the	O
Frisian	O
language	O
was	O
spoken	O
along	O
the	O
entire	O
southern	O
North	O
Sea	O
coast	O
.	O
Although	O
the	O
earliest	O
definite	O
written	O
examples	O
of	O
Frisian	O
are	O
from	O
approximately	O
the	O
9th	O
century	O
,	O
there	O
are	O
a	O
few	O
examples	O
of	O
runic	O
inscriptions	O
from	O
the	O
region	O
which	O
are	O
probably	O
older	O
and	O
possibly	O
in	O
the	O
Frisian	O
language	O
.	O
Up	O
until	O
the	O
fifteenth	O
century	O
Frisian	O
was	O
a	O
language	O
widely	O
spoken	O
and	O
written	O
,	O
but	O
from	O
1500	O
onwards	O
it	O
became	O
an	O
almost	O
exclusively	O
oral	O
language	O
,	O
mainly	O
used	O
in	O
rural	O
areas	O
.	O
This	O
was	O
in	O
part	O
due	O
to	O
the	O
occupation	O
of	O
its	O
stronghold	O
,	O
the	O
Dutch	LOCATION
province	LOCATION
of	LOCATION
Friesland	LOCATION
(	LOCATION
Fryslân	LOCATION
)	O
,	O
in	O
1498	O
,	O
by	O
Duke	O
Albert	PERSON
of	O
Saxony	LOCATION
,	O
who	O
replaced	O
Frisian	O
as	O
the	O
language	O
of	O
government	O
with	O
Dutch	O
.	O
Afterwards	O
this	O
practice	O
was	O
continued	O
under	O
the	O
Habsburg	O
rulers	O
of	O
the	O
Netherlands	O
,	O
and	O
even	O
when	O
the	O
Netherlands	O
became	O
independent	O
,	O
in	O
1585	O
,	O
Frisian	O
did	O
not	O
regain	O
its	O
former	O
status	O
.	O
The	O
reason	O
for	O
this	O
was	O
the	O
rise	O
of	O
Holland	O
as	O
the	O
dominant	O
part	O
of	O
the	O
Netherlands	O
,	O
and	O
its	O
language	O
,	O
Dutch	O
,	O
as	O
the	O
dominant	O
language	O
in	O
judicial	O
,	O
administrative	O
and	O
religious	O
affairs	O
.	O
In	O
this	O
period	O
the	O
great	O
Frisian	O
poet	O
Gysbert	PERSON
Japiks	PERSON
(	O
1603-66	O
)	O
,	O
a	O
schoolteacher	O
and	O
cantor	O
from	O
the	O
city	O
of	O
Bolsward	LOCATION
,	O
who	O
largely	O
fathered	O
modern	O
Frisian	O
literature	O
and	O
orthography	O
,	O
was	O
really	O
an	O
exception	O
to	O
the	O
rule	O
.	O
His	O
example	O
was	O
not	O
followed	O
until	O
the	O
nineteenth	O
century	O
,	O
when	O
entire	O
generations	O
of	O
Frisian	O
authors	O
and	O
poets	O
appeared	O
.	O
Each	O
of	O
the	O
Frisian	O
languages	O
has	O
several	O
dialects	O
.	O
