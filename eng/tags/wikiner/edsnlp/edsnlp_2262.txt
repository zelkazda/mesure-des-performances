Since	O
2005	O
,	O
he	O
has	O
appeared	O
as	O
Lloyd	PERSON
Mullaney	PERSON
in	O
the	O
long-running	O
soap	O
opera	O
Coronation	O
Street	O
.	O
Craig	PERSON
Charles	PERSON
was	O
born	O
to	O
a	O
multiracial	O
family	O
in	O
Liverpool	LOCATION
;	O
his	O
father	O
was	O
black	O
and	O
his	O
mother	O
was	O
Irish	O
.	O
He	O
grew	O
up	O
on	O
the	O
Cantril	O
Farm	O
estate	O
and	O
went	O
to	O
school	O
with	O
Micky	PERSON
Quinn	PERSON
,	O
who	O
grew	O
up	O
to	O
be	O
a	O
professional	O
footballer	O
.	O
Before	O
turning	O
to	O
entertainment	O
,	O
Charles	PERSON
played	O
professional	O
football	O
,	O
most	O
notably	O
for	O
Tranmere	O
Rovers	O
.	O
Charles	PERSON
acquired	O
cult	O
status	O
in	O
1988	O
as	O
the	O
Liverpudlian	O
slob	O
,	O
Dave	PERSON
Lister	PERSON
,	O
in	O
BBC2	O
's	O
long-running	O
sci-fi	O
comedy	O
television	O
series	O
Red	O
Dwarf	O
.	O
This	O
was	O
a	O
role	O
Charles	O
played	O
in	O
all	O
eight	O
series	O
until	O
1999	O
and	O
in	O
the	O
three	O
part	O
special	O
for	O
television	O
channel	O
Dave	O
in	O
2009	O
.	O
Charles	PERSON
'	O
younger	O
brother	O
Emile	PERSON
Charles	PERSON
guest-starred	O
there	O
.	O
Charles	PERSON
has	O
appeared	O
briefly	O
in	O
a	O
number	O
of	O
television	O
shows	O
such	O
as	O
EastEnders	O
,	O
Holby	O
City	O
,	O
The	O
Bill	O
,	O
Lexx	O
,	O
The	O
10	O
Percenters	O
,	O
Doctors	O
and	O
Celebrity	O
Weakest	O
Link	O
.	O
He	O
was	O
also	O
involved	O
in	O
the	O
controversial	O
mockumentary	O
Ghostwatch	O
in	O
1992	O
.	O
Charles	O
'	O
other	O
acting	O
work	O
includes	O
briefly	O
playing	O
the	O
title	O
role	O
in	O
the	O
short-lived	O
Channel	O
4	O
sitcom	O
Captain	O
Butler	O
(	O
1997	O
)	O
.	O
In	O
2005	O
,	O
Charles	PERSON
joined	O
the	O
cast	O
of	O
ITV	O
's	O
long-running	O
soap	O
opera	O
Coronation	O
Street	O
,	O
playing	O
philandering	O
taxicab	O
driver	O
Lloyd	PERSON
Mullaney	PERSON
.	O
Later	O
that	O
year	O
,	O
he	O
participated	O
in	O
the	O
Channel	O
4	O
reality	O
sports	O
show	O
,	O
The	O
Games	O
,	O
coming	O
fourth	O
overall	O
in	O
the	O
men	O
's	O
competition	O
.	O
He	O
was	O
briefly	O
suspended	O
from	O
Coronation	O
Street	O
and	O
BBC	O
6	O
Music	O
in	O
June	O
2006	O
whilst	O
the	O
production	O
companies	O
investigated	O
allegations	O
of	O
crack	O
cocaine	O
usage	O
.	O
In	O
1984	O
,	O
at	O
the	O
age	O
of	O
20	O
,	O
Charles	PERSON
married	O
English	O
actress	O
Cathy	PERSON
Tyson	PERSON
.	O
In	O
1994	O
,	O
Charles	PERSON
and	O
a	O
friend	O
were	O
arrested	O
and	O
remanded	O
in	O
custody	O
for	O
several	O
months	O
on	O
a	O
rape	O
charge	O
.	O
In	O
February	O
1995	O
,	O
both	O
Charles	PERSON
and	O
his	O
friend	O
were	O
acquitted	O
in	O
their	O
trial	O
.	O
Whilst	O
in	O
prison	O
Charles	PERSON
was	O
attacked	O
by	O
a	O
man	O
wielding	O
a	O
knife	O
.	O
After	O
being	O
cleared	O
,	O
Charles	PERSON
spoke	O
of	O
the	O
need	O
to	O
restore	O
anonymity	O
for	O
those	O
accused	O
of	O
rape	O
.	O
In	O
June	O
2006	O
,	O
a	O
photograph	O
was	O
printed	O
in	O
the	O
Daily	O
Mirror	O
newspaper	O
purporting	O
to	O
show	O
Charles	PERSON
smoking	O
crack	O
cocaine	O
in	O
the	O
back	O
seat	O
of	O
a	O
taxi	O
.	O
These	O
allegations	O
resulted	O
in	O
Charles	PERSON
being	O
suspended	O
from	O
Coronation	O
Street	O
until	O
February	O
2007	O
,	O
and	O
from	O
BBC	O
6	O
Music	O
while	O
an	O
investigation	O
was	O
held	O
.	O
In	O
an	O
interview	O
,	O
Charles	PERSON
blamed	O
his	O
relapse	O
into	O
drug	O
use	O
on	O
the	O
death	O
of	O
his	O
father	O
,	O
combined	O
with	O
the	O
bitterness	O
stemming	O
from	O
being	O
falsely	O
accused	O
of	O
rape	O
.	O
