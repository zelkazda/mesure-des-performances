Throughout	O
the	O
late	O
1950s	O
and	O
into	O
the	O
1960s	O
,	O
the	O
United	O
States	O
had	O
been	O
developing	O
a	O
series	O
of	O
missile	O
systems	O
with	O
the	O
ability	O
to	O
shoot	O
down	O
incoming	O
ICBM	O
warheads	O
.	O
During	O
this	O
period	O
the	O
US	O
maintained	O
a	O
lead	O
in	O
the	O
number	O
and	O
sophistication	O
of	O
their	O
delivery	O
systems	O
,	O
and	O
considered	O
the	O
defense	O
of	O
the	O
US	O
as	O
a	O
part	O
of	O
reducing	O
the	O
overall	O
damage	O
inflicted	O
in	O
a	O
full	O
nuclear	O
exchange	O
.	O
As	O
part	O
of	O
this	O
defense	O
,	O
Canada	O
and	O
the	O
US	O
established	O
the	O
North	O
American	O
Air	O
Defense	O
Command	O
(	O
now	O
called	O
North	O
American	O
Aerospace	O
Defense	O
Command	O
NORAD	O
)	O
.	O
A	O
number	O
of	O
serious	O
concerns	O
about	O
the	O
technical	O
abilities	O
of	O
the	O
system	O
came	O
to	O
light	O
,	O
many	O
of	O
which	O
reached	O
popular	O
magazines	O
such	O
as	O
Scientific	O
American	O
.	O
This	O
was	O
based	O
on	O
lack	O
of	O
intelligence	O
information	O
and	O
reflected	O
the	O
American	O
nuclear	O
warfare	O
theory	O
and	O
military	O
doctrines	O
.	O
At	O
about	O
the	O
same	O
time	O
,	O
the	O
USSR	O
reached	O
strategic	O
parity	O
with	O
the	O
US	O
in	O
terms	O
of	O
ICBM	O
forces	O
.	O
A	O
nuclear	O
war	O
would	O
no	O
longer	O
be	O
a	O
favorable	O
exchange	O
for	O
the	O
US	O
,	O
but	O
both	O
countries	O
would	O
be	O
devastated	O
.	O
Soviet	O
military	O
theory	O
fully	O
involved	O
the	O
mass	O
use	O
of	O
nuclear	O
devices	O
,	O
in	O
combination	O
with	O
massive	O
conventional	O
forces	O
.	O
The	O
sites	O
were	O
Moscow	O
for	O
the	O
USSR	O
and	O
Grand	O
Forks	O
Air	O
Force	O
Base	O
,	O
N.D.	O
,	O
since	O
its	O
Safeguard	O
facility	O
was	O
already	O
under	O
construction	O
,	O
for	O
the	O
US	O
.	O
It	O
was	O
perceived	O
as	O
requiring	O
two	O
enemies	O
to	O
agree	O
not	O
to	O
deploy	O
a	O
potentially	O
useful	O
weapon	O
,	O
deliberately	O
to	O
maintain	O
the	O
balance	O
of	O
power	O
and	O
as	O
such	O
,	O
was	O
also	O
taken	O
as	O
confirmation	O
of	O
the	O
Soviet	O
adherence	O
to	O
the	O
MAD	O
doctrine	O
.	O
The	O
treaty	O
was	O
undisturbed	O
until	O
Ronald	O
Reagan	O
announced	O
his	O
Strategic	O
Defense	O
Initiative	O
(	O
SDI	O
)	O
on	O
March	O
23	O
,	O
1983	O
.	O
The	O
project	O
was	O
a	O
blow	O
to	O
Yuri	O
Andropov	O
's	O
so-called	O
"	O
peace	O
offensive	O
"	O
.	O
SDI	O
research	O
went	O
ahead	O
,	O
although	O
it	O
did	O
not	O
achieve	O
the	O
hoped	O
result	O
.	O
SDI	O
research	O
was	O
cut	O
back	O
following	O
the	O
end	O
of	O
Reagan	O
's	O
presidency	O
,	O
and	O
in	O
1995	O
it	O
was	O
reiterated	O
in	O
a	O
presidential	O
joint	O
statement	O
that	O
"	O
missile	O
defense	O
systems	O
may	O
be	O
deployed	O
...	O
[	O
that	O
]	O
will	O
not	O
pose	O
a	O
realistic	O
threat	O
to	O
the	O
strategic	O
nuclear	O
force	O
of	O
the	O
other	O
side	O
and	O
will	O
not	O
be	O
tested	O
to	O
...	O
[	O
create	O
]	O
that	O
capability	O
.	O
"	O
The	O
competitive	O
pressure	O
of	O
SDI	O
did	O
not	O
,	O
in	O
fact	O
,	O
add	O
additional	O
strain	O
to	O
the	O
Soviet	O
economy	O
,	O
in	O
large	O
part	O
because	O
Soviet	O
scientists	O
did	O
not	O
believe	O
such	O
a	O
system	O
was	O
feasible	O
.	O
This	O
was	O
the	O
first	O
time	O
in	O
recent	O
history	O
the	O
U.S.	O
has	O
withdrawn	O
from	O
a	O
major	O
international	O
arms	O
treaty	O
.	O
This	O
led	O
to	O
the	O
eventual	O
creation	O
of	O
the	O
Missile	O
Defense	O
Agency	O
.	O
The	O
construction	O
of	O
a	O
missile	O
defense	O
system	O
was	O
also	O
feared	O
to	O
enable	O
the	O
US	O
to	O
attack	O
with	O
a	O
nuclear	O
first	O
strike	O
.	O
In	O
the	O
case	O
of	O
Russia	O
,	O
the	O
U.S.	O
stated	O
that	O
it	O
intended	O
to	O
discuss	O
a	O
bilateral	O
reduction	O
in	O
the	O
numbers	O
of	O
nuclear	O
warheads	O
,	O
which	O
would	O
allow	O
Russia	O
to	O
reduce	O
its	O
spending	O
on	O
missiles	O
without	O
decrease	O
of	O
comparative	O
strength	O
.	O
Discussions	O
led	O
to	O
the	O
signing	O
of	O
the	O
Strategic	O
Offensive	O
Reductions	O
Treaty	O
in	O
Moscow	O
on	O
May	O
24	O
,	O
2002	O
.	O
