Most	O
of	O
those	O
territories	O
had	O
been	O
ceded	O
to	O
it	O
by	O
the	O
secret	O
agreement	O
portion	O
of	O
the	O
Molotov-Ribbentrop	O
Pact	O
with	O
Nazi	O
Germany	O
.	O
In	O
East	O
Germany	O
after	O
local	O
election	O
losses	O
,	O
a	O
forced	O
merger	O
of	O
political	O
parties	O
in	O
the	O
Socialist	O
Unity	O
Party	O
(	O
"	O
SED	O
"	O
)	O
,	O
followed	O
by	O
elections	O
in	O
1946	O
where	O
political	O
opponents	O
were	O
oppressed	O
.	O
Fraudulent	O
Polish	O
elections	O
held	O
in	O
January	O
1947	O
resulted	O
in	O
Poland	LOCATION
's	O
official	O
transformation	O
to	O
the	O
People	O
's	O
Republic	O
of	O
Poland	O
.	O
In	O
Hungary	O
,	O
when	O
the	O
Soviets	O
installed	O
a	O
communist	O
government	O
,	O
Mátyás	PERSON
Rákosi	PERSON
was	O
appointed	O
General	O
Secretary	O
of	O
the	O
Hungarian	O
Communist	O
Party	O
,	O
which	O
began	O
one	O
of	O
the	O
harshest	O
dictatorships	O
in	O
Europe	O
under	O
the	O
People	O
's	O
Republic	O
of	O
Hungary	O
.	O
With	O
Soviet	O
backing	O
,	O
the	O
Communist	O
Party	O
of	O
Czechoslovakia	O
assumed	O
undisputed	O
control	O
over	O
the	O
government	O
of	O
Czechoslovakia	O
in	O
the	O
Czechoslovak	O
coup	O
d'état	O
of	O
1948	O
,	O
ushering	O
in	O
a	O
dictatorship	O
.	O
In	O
1946	O
,	O
Albania	O
was	O
declared	O
the	O
People	O
's	O
Republic	O
of	O
Albania	O
.	O
They	O
were	O
economically	O
communist	O
and	O
depended	O
upon	O
the	O
Soviet	O
Union	O
for	O
significant	O
amounts	O
of	O
materials	O
.	O
The	O
burdens	O
the	O
Red	O
Army	O
and	O
the	O
Soviet	O
Union	O
endured	O
had	O
earned	O
it	O
massive	O
respect	O
which	O
,	O
had	O
it	O
been	O
fully	O
exploited	O
by	O
Joseph	PERSON
Stalin	PERSON
,	O
had	O
a	O
good	O
chance	O
of	O
resulting	O
in	O
a	O
communist	O
Europe	O
.	O
Communist	O
parties	O
achieved	O
a	O
significant	O
popularity	O
in	O
such	O
nations	O
as	O
China	O
,	O
Greece	O
,	O
Iran	O
,	O
and	O
the	O
Republic	O
of	O
Mahabad	O
.	O
Communist	O
parties	O
had	O
already	O
come	O
to	O
power	O
in	O
Romania	O
,	O
Bulgaria	O
,	O
Albania	O
,	O
and	O
Yugoslavia	O
.	O
Having	O
lost	O
27	O
million	O
people	O
in	O
the	O
war	O
,	O
the	O
Soviet	O
Union	O
was	O
determined	O
to	O
destroy	O
Germany	O
's	O
capacity	O
for	O
another	O
war	O
,	O
and	O
pushed	O
for	O
such	O
in	O
wartime	O
conferences	O
.	O
The	O
resulting	O
Morgenthau	O
plan	O
policy	O
foresaw	O
returning	O
Germany	O
to	O
a	O
pastoral	O
state	O
without	O
heavy	O
industry	O
.	O
The	O
United	O
States	O
concluded	O
that	O
a	O
solution	O
could	O
not	O
wait	O
any	O
longer	O
.	O
This	O
would	O
have	O
left	O
the	O
two	O
nations	O
,	O
in	O
particular	O
Greece	O
,	O
on	O
the	O
brink	O
of	O
a	O
communist-led	O
revolution	O
.	O
In	O
a	O
meeting	O
with	O
congressional	O
leaders	O
,	O
the	O
argument	O
of	O
"	O
apples	O
in	O
a	O
barrel	O
infected	O
by	O
one	O
rotten	O
one	O
"	O
was	O
used	O
to	O
convince	O
them	O
of	O
the	O
significance	O
in	O
supporting	O
Greece	O
and	O
Turkey	O
.	O
Although	O
based	O
on	O
a	O
simplistic	O
analysis	O
of	O
internal	O
strife	O
in	O
Greece	O
and	O
Turkey	O
,	O
it	O
became	O
the	O
single	O
dominating	O
influence	O
over	O
U.S.	O
policy	O
until	O
at	O
least	O
the	O
Vietnam	O
War	O
.	O
The	O
U.S.	O
brandished	O
its	O
role	O
as	O
the	O
leader	O
of	O
the	O
"	O
free	O
world	O
.	O
"	O
Meanwhile	O
,	O
the	O
Soviet	O
Union	O
brandished	O
its	O
position	O
as	O
the	O
leader	O
of	O
the	O
"	O
progressive	O
"	O
and	O
"	O
anti-imperialist	O
"	O
camp	O
.	O
Historical	O
studies	O
,	O
official	O
accounts	O
,	O
memoirs	O
and	O
textbooks	O
published	O
in	O
the	O
Soviet	O
Union	O
used	O
that	O
depiction	O
of	O
events	O
until	O
the	O
Soviet	O
Union	O
's	O
dissolution	O
.	O
Thereafter	O
,	O
street	O
and	O
water	O
communications	O
were	O
severed	O
,	O
rail	O
and	O
barge	O
traffic	O
was	O
stopped	O
and	O
the	O
Soviets	O
initially	O
stopped	O
supplying	O
food	O
to	O
the	O
civilian	O
population	O
in	O
the	O
non-Soviet	O
sectors	O
of	O
Berlin	LOCATION
.	O
By	O
February	O
1948	O
,	O
because	O
of	O
massive	O
post-war	O
military	O
cuts	O
,	O
the	O
entire	O
U.S.	O
army	O
had	O
been	O
reduced	O
to	O
552,000	O
men	O
.	O
Soviet	O
military	O
forces	O
in	O
the	O
Soviet	O
sector	O
that	O
surrounded	O
Berlin	LOCATION
totaled	O
one	O
and	O
a	O
half	O
million	O
men	O
.	O
The	O
two	O
United	O
States	O
regiments	O
in	O
Berlin	LOCATION
would	O
have	O
provided	O
little	O
resistance	O
against	O
a	O
Soviet	O
attack	O
.	O
Thereafter	O
,	O
a	O
massive	O
aerial	O
supply	O
campaign	O
of	O
food	O
,	O
water	O
and	O
other	O
goods	O
was	O
initiated	O
by	O
the	O
United	O
States	O
,	O
Britain	O
,	O
France	O
and	O
other	O
countries	O
.	O
The	O
Soviets	O
derided	O
"	O
the	O
futile	O
attempts	O
of	O
the	O
Americans	O
to	O
save	O
face	O
and	O
to	O
maintain	O
their	O
untenable	O
position	O
in	O
Berlin	O
.	O
"	O
The	O
split	O
created	O
two	O
separate	O
communist	O
forces	O
in	O
Europe	O
.	O
This	O
resulted	O
in	O
the	O
persecution	O
of	O
many	O
major	O
party	O
cadres	O
,	O
including	O
those	O
in	O
East	O
Germany	O
.	O
The	O
Soviet	O
Union	O
,	O
Albania	O
,	O
Czechoslovakia	O
,	O
Hungary	O
,	O
East	O
Germany	O
,	O
Bulgaria	O
,	O
Romania	O
,	O
and	O
Poland	O
founded	O
this	O
military	O
alliance	O
.	O
U.S.	O
officials	O
quickly	O
moved	O
to	O
escalate	O
and	O
expand	O
"	O
containment	O
.	O
"	O
In	O
a	O
secret	O
1950	O
document	O
,	O
NSC-68	O
,	O
they	O
proposed	O
to	O
strengthen	O
their	O
alliance	O
systems	O
,	O
quadruple	O
defense	O
spending	O
,	O
and	O
embark	O
on	O
an	O
elaborate	O
propaganda	O
campaign	O
to	O
convince	O
the	O
U.S.	O
public	O
to	O
fight	O
this	O
costly	O
cold	O
war	O
.	O
The	O
USSR	O
had	O
signed	O
a	O
Treaty	O
of	O
Friendship	O
with	O
the	O
Kuomintang	O
in	O
1945	O
and	O
disavowed	O
support	O
for	O
the	O
Chinese	O
Communists	O
.	O
In	O
addition	O
,	O
the	O
Chinese	O
Communists	O
were	O
able	O
to	O
fill	O
the	O
political	O
vacuum	O
left	O
in	O
Manchuria	O
after	O
Soviet	O
forces	O
withdrew	O
from	O
the	O
area	O
and	O
thus	O
gained	O
China	O
's	O
prime	O
industrial	O
base	O
.	O
On	O
October	O
1	O
,	O
1949	O
,	O
Mao	PERSON
Zedong	PERSON
proclaimed	O
the	O
People	O
's	O
Republic	O
of	O
China	O
(	O
PRC	O
)	O
.	O
In	O
December	O
1949	O
,	O
Chiang	O
proclaimed	O
Taipei	LOCATION
the	O
temporary	O
capital	O
of	O
the	O
Republic	O
of	O
China	O
and	O
continued	O
to	O
assert	O
his	O
government	O
as	O
the	O
sole	O
legitimate	O
authority	O
in	O
China	O
.	O
Though	O
the	O
U.S.	O
refused	O
to	O
aide	O
Chiang	LOCATION
Kai-shek	LOCATION
in	O
his	O
hope	O
to	O
"	O
recover	O
the	O
mainland,	O
"	O
it	O
continued	O
supporting	O
the	O
Republic	O
of	O
China	O
with	O
military	O
supplies	O
and	O
expertise	O
to	O
prevent	O
Taiwan	O
from	O
falling	O
into	O
PRC	O
hands	O
.	O
Korea	O
had	O
been	O
divided	O
at	O
the	O
end	O
of	O
World	O
War	O
II	O
along	O
the	O
38th	O
parallel	O
into	O
Soviet	O
and	O
U.S.	O
occupation	O
zones	O
,	O
in	O
which	O
a	O
communist	O
government	O
was	O
installed	O
in	O
the	O
North	O
by	O
the	O
Soviets	O
,	O
and	O
an	O
elected	O
government	O
in	O
the	O
South	O
came	O
to	O
power	O
after	O
UN-supervised	O
elections	O
in	O
1948	O
.	O
In	O
June	O
1950	O
,	O
Kim	PERSON
Il-Sung	PERSON
's	O
North	O
Korean	O
People	O
's	O
Army	O
invaded	O
South	O
Korea	O
.	O
A	O
joint	O
UN	O
force	O
of	O
personnel	O
from	O
South	O
Korea	O
,	O
the	O
United	O
States	O
,	O
Britain	O
,	O
Turkey	O
,	O
Canada	O
,	O
Australia	O
,	O
France	O
,	O
the	O
Philippines	O
,	O
the	O
Netherlands	O
,	O
Belgium	O
,	O
New	O
Zealand	O
and	O
other	O
countries	O
joined	O
to	O
stop	O
the	O
invasion	O
.	O
A	O
cease-fire	O
was	O
approved	O
in	O
July	O
1953	O
after	O
the	O
death	O
of	O
Stalin	PERSON
,	O
who	O
had	O
been	O
insisting	O
that	O
the	O
North	O
Koreans	O
continue	O
fighting	O
.	O
A	O
hydrogen	O
bomb	O
--	O
which	O
produced	O
nuclear	O
fusion	O
instead	O
of	O
nuclear	O
fission	O
--	O
was	O
first	O
tested	O
by	O
the	O
United	O
States	O
in	O
November	O
1952	O
and	O
the	O
Soviet	O
Union	O
in	O
August	O
1953	O
.	O
The	O
1951	O
children	O
's	O
film	O
Duck	O
and	O
Cover	O
is	O
a	O
prime	O
example	O
.	O
