They	O
are	O
a	O
member	O
of	O
the	O
American	O
Football	O
Conference	O
's	O
North	O
Division	O
in	O
the	O
National	O
Football	O
League	O
(	O
NFL	O
)	O
.	O
Its	O
triumph	O
over	O
the	O
New	O
York	O
Giants	O
in	O
Super	O
Bowl	O
XXXV	O
at	O
the	O
conclusion	O
of	O
the	O
2000	O
season	O
remains	O
its	O
best	O
season	O
.	O
The	O
Baltimore	O
Ravens	O
came	O
into	O
existence	O
in	O
1996	O
when	O
Art	PERSON
Modell	PERSON
,	O
then	O
owner	O
of	O
the	O
Cleveland	O
Browns	O
,	O
announced	O
his	O
intention	O
to	O
relocate	O
his	O
team	O
from	O
Cleveland	O
to	O
Baltimore	LOCATION
.	O
The	O
controversy	O
ended	O
when	O
representatives	O
of	O
Cleveland	O
and	O
the	O
NFL	O
reached	O
a	O
settlement	O
on	O
February	O
8	O
,	O
1996	O
.	O
The	O
agreement	O
stipulated	O
that	O
the	O
Browns	O
'	O
name	O
,	O
colors	O
,	O
uniform	O
design	O
and	O
franchise	O
records	O
would	O
remain	O
in	O
Cleveland	LOCATION
.	O
The	O
franchise	O
history	O
included	O
Browns	O
club	O
records	O
and	O
connections	O
with	O
Pro	O
Football	O
Hall	O
of	O
Fame	O
players	O
.	O
Modell	O
's	O
Baltimore	O
team	O
,	O
while	O
retaining	O
all	O
current	O
player	O
contracts	O
,	O
would	O
officially	O
be	O
the	O
expansion	O
team	O
,	O
a	O
"	O
new	O
franchise	O
.	O
"	O
Not	O
all	O
players	O
,	O
staff	O
or	O
front	O
office	O
would	O
make	O
the	O
move	O
to	O
Baltimore	O
,	O
however	O
.	O
Modell	O
relocated	O
the	O
team	O
and	O
hired	O
Ted	PERSON
Marchibroda	PERSON
as	O
head	O
coach	O
.	O
Ozzie	PERSON
Newsome	PERSON
,	O
the	O
Browns	O
'	O
tight	O
end	O
for	O
many	O
seasons	O
,	O
joined	O
Modell	PERSON
in	O
Baltimore	LOCATION
as	O
director	O
of	O
football	O
operations	O
.	O
Brian	PERSON
Billick	PERSON
took	O
over	O
as	O
head	O
coach	O
in	O
1999	O
.	O
He	O
was	O
joined	O
by	O
receiver	O
Qadry	PERSON
Ismail	PERSON
,	O
who	O
posted	O
a	O
1,000-yard	O
season	O
.	O
Due	O
to	O
continual	O
financial	O
hardships	O
,	O
the	O
NFL	O
directed	O
Modell	O
to	O
initiate	O
the	O
sale	O
of	O
his	O
franchise	O
.	O
On	O
April	O
9	O
,	O
2004	O
the	O
NFL	O
approved	O
Steve	PERSON
Bisciotti	PERSON
's	O
purchase	O
of	O
the	O
majority	O
stake	O
in	O
the	O
club	O
.	O
Baltimore	O
's	O
season	O
started	O
strong	O
with	O
a	O
5-1	O
record	O
.	O
Coach	O
Brian	PERSON
Billick	PERSON
announced	O
that	O
the	O
change	O
at	O
quarterback	O
would	O
be	O
for	O
the	O
rest	O
of	O
the	O
season	O
.	O
Since	O
the	O
divisional	O
rival	O
Tennessee	O
Titans	O
had	O
a	O
record	O
of	O
13	O
--	O
3	O
,	O
Baltimore	O
had	O
to	O
play	O
in	O
the	O
wild	O
card	O
round	O
.	O
They	O
were	O
preceded	O
by	O
the	O
NFL	O
Colts	O
in	O
1958	O
,	O
1959	O
and	O
1970	O
,	O
the	O
USFL	O
Stars	O
in	O
1985	O
and	O
the	O
CFL	O
Stallions	O
in	O
1995	O
.	O
Baltimore	O
ran	O
into	O
salary	O
cap	O
problems	O
entering	O
the	O
2002	O
season	O
and	O
was	O
forced	O
to	O
part	O
with	O
a	O
number	O
of	O
impact	O
players	O
.	O
They	O
then	O
traded	O
their	O
2003	O
2nd	O
round	O
pick	O
and	O
2004	O
1st	O
round	O
pick	O
to	O
the	O
New	O
England	O
Patriots	O
for	O
the	O
19th	O
overall	O
selection	O
which	O
they	O
used	O
to	O
draft	O
Cal	O
quarterback	O
Kyle	PERSON
Boller	PERSON
.	O
Jamal	PERSON
Lewis	PERSON
ran	O
for	O
2,066	O
yards	O
.	O
With	O
a	O
10	O
--	O
6	O
record	O
,	O
Baltimore	O
won	O
their	O
first	O
AFC	O
North	O
division	O
title	O
.	O
The	O
Titans	O
won	O
20-17	O
on	O
a	O
late	O
field	O
goal	O
,	O
and	O
Baltimore	O
's	O
season	O
ended	O
early	O
.	O
Ray	PERSON
Lewis	PERSON
was	O
named	O
Defensive	O
Player	O
of	O
the	O
Year	O
for	O
the	O
second	O
time	O
in	O
his	O
career	O
.	O
The	O
2006	O
Baltimore	O
Ravens	O
season	O
began	O
with	O
the	O
team	O
trying	O
to	O
improve	O
on	O
their	O
6	O
--	O
10	O
record	O
of	O
2005	O
.	O
Baltimore	O
had	O
secured	O
the	O
AFC	O
North	O
title	O
,	O
the	O
#	O
2	O
AFC	O
playoff	O
seed	O
,	O
and	O
clinched	O
a	O
1st-round	O
bye	O
by	O
season	O
's	O
end	O
.	O
McNair	O
threw	O
two	O
costly	O
interceptions	O
,	O
including	O
one	O
at	O
the	O
1-yard	O
line	O
.	O
The	O
eventual	O
Super-Bowl-Champion	O
Colts	O
won	O
15-6	O
,	O
ending	O
Baltimore	O
's	O
season	O
.	O
Eight	O
victories	O
in	O
its	O
last	O
ten	O
regular	O
season	O
matches	O
enabled	O
them	O
to	O
clinch	O
the	O
sixth	O
seed	O
in	O
the	O
AFC	O
playoffs	O
.	O
With	O
Jonathan	PERSON
Ogden	PERSON
retiring	O
after	O
the	O
2007	O
season	O
and	O
Matt	PERSON
Stover	PERSON
going	O
into	O
free	O
agency	O
,	O
Baltimore	O
's	O
only	O
remaining	O
player	O
from	O
its	O
first	O
season	O
was	O
Ray	PERSON
Lewis	PERSON
.	O
In	O
the	O
season	O
opener	O
,	O
the	O
offense	O
continued	O
its	O
improvements	O
from	O
the	O
year	O
before	O
as	O
it	O
scored	O
38	O
points	O
and	O
accounted	O
for	O
over	O
500	O
yards	O
in	O
a	O
38-24	O
victory	O
over	O
the	O
Kansas	O
City	O
Chiefs	O
.	O
Joe	PERSON
Flacco	PERSON
made	O
28	O
out	O
of	O
43	O
passing	O
attempts	O
and	O
threw	O
for	O
a	O
career	O
high	O
385	O
yards	O
,	O
and	O
Ray	PERSON
Rice	PERSON
ran	O
for	O
117	O
yards	O
.	O
The	O
very	O
next	O
week	O
they	O
hosted	O
the	O
Denver	O
Broncos	O
,	O
who	O
were	O
undefeated	O
(	O
6-0	O
)	O
.	O
However	O
they	O
were	O
out	O
played	O
on	O
both	O
sides	O
of	O
the	O
ball	O
,	O
suffered	O
a	O
crucial	O
miss	O
by	O
Hauschka	PERSON
,	O
and	O
lost	O
17-7	O
.	O
This	O
led	O
coach	O
John	PERSON
Harbaugh	PERSON
to	O
release	O
Hauschka	PERSON
and	O
replace	O
him	O
with	O
Billy	PERSON
Cundiff	PERSON
.	O
They	O
looked	O
ahead	O
to	O
their	O
division	O
rivals	O
,	O
the	O
Steelers	O
,	O
who	O
were	O
coming	O
off	O
a	O
dramatic	O
last-second	O
win	O
against	O
the	O
Packers	O
.	O
Mason	PERSON
notably	O
dropped	O
a	O
pass	O
in	O
the	O
end	O
zone	O
.	O
They	O
played	O
against	O
the	O
New	O
England	O
Patriots	O
in	O
the	O
wild	O
card	O
round	O
.	O
Two	O
touchdowns	O
late	O
in	O
the	O
first	O
half	O
gave	O
the	O
Colts	O
a	O
17-3	O
lead	O
at	O
intermission	O
,	O
and	O
Baltimore	O
miscues	O
in	O
the	O
second	O
half	O
ensured	O
the	O
end	O
of	O
their	O
season	O
,	O
by	O
a	O
20-3	O
score	O
.	O
They	O
also	O
drafted	O
tight	O
ends	O
Dennis	PERSON
Pitta	PERSON
and	O
Ed	PERSON
Dickson	PERSON
in	O
the	O
2010	O
NFL	O
Draft	O
.	O
The	O
Baltimore	O
Sun	O
ran	O
a	O
poll	O
showing	O
three	O
designs	O
for	O
new	O
helmet	O
logos	O
.	O
Art	PERSON
Modell	PERSON
announced	O
that	O
he	O
would	O
honor	O
this	O
preference	O
but	O
still	O
wanted	O
a	O
letter	O
B	O
to	O
appear	O
somewhere	O
in	O
the	O
design	O
.	O
The	O
secondary	O
logo	O
is	O
a	O
shield	O
that	O
honors	O
Baltimore	O
's	O
history	O
of	O
heraldry	O
.	O
A	O
number	O
of	O
NFL	O
teams	O
have	O
since	O
donned	O
the	O
look	O
,	O
beginning	O
with	O
the	O
all-black	O
home	O
uniform	O
worn	O
in	O
three	O
games	O
by	O
the	O
2001	O
New	O
Orleans	O
Saints	O
.	O
It	O
was	O
believed	O
to	O
be	O
due	O
to	O
the	O
fact	O
that	O
John	PERSON
Harbaugh	PERSON
does	O
n't	O
like	O
the	O
"	O
blackout	O
"	O
look	O
.	O
They	O
also	O
selected	O
Ray	PERSON
Lewis	PERSON
with	O
the	O
26th	O
pick	O
.	O
The	O
Hearst-Argyle	O
stations	O
were	O
in	O
their	O
first	O
season	O
of	O
game	O
coverage	O
,	O
replacing	O
longtime	O
stations	O
WJFK	O
/	O
WQSR	O
.	O
