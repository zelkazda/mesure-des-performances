Catharism	PERSON
had	O
its	O
roots	O
in	O
the	O
Paulician	O
movement	O
in	O
Armenia	LOCATION
and	LOCATION
the	LOCATION
Bogomils	LOCATION
of	LOCATION
Bulgaria	LOCATION
with	O
whom	O
the	O
Paulicians	O
merged	O
[	O
citation	O
needed	O
]	O
.	O
They	O
also	O
became	O
influenced	O
by	O
dualist	O
and	O
Manichaean	O
beliefs	O
[	O
citation	O
needed	O
]	O
.	O
The	O
Cathari	O
did	O
not	O
believe	O
in	O
one	O
all-encompassing	O
god	O
,	O
but	O
in	O
two	O
,	O
both	O
equal	O
and	O
comparable	O
in	O
status	O
.	O
Cathars	O
vehemently	O
repudiated	O
the	O
significance	O
of	O
the	O
crucifixion	O
and	O
the	O
cross	O
.	O
The	O
Catholic	O
Church	O
regarded	O
the	O
sect	O
as	O
dangerously	O
heretical	O
.	O
Faced	O
with	O
the	O
rapid	O
spread	O
of	O
the	O
movement	O
across	O
the	O
Languedoc	O
region	O
,	O
the	O
Church	O
first	O
sought	O
peaceful	O
attempts	O
at	O
conversion	O
,	O
undertaken	O
by	O
Dominicans	O
.	O
The	O
anti-Cathar	O
Albigensian	O
Crusade	O
,	O
and	O
the	O
inquisition	O
which	O
followed	O
it	O
,	O
entirely	O
eradicated	O
the	O
Cathars	O
.	O
There	O
is	O
consensus	O
that	O
Cathars	O
is	O
a	O
name	O
given	O
to	O
the	O
movement	O
and	O
not	O
one	O
that	O
its	O
members	O
chose	O
.	O
The	O
Cathars	O
were	O
also	O
sometimes	O
referred	O
to	O
as	O
the	O
Albigensians	O
(	O
Albigeois	O
)	O
.	O
The	O
name	O
refers	O
to	O
the	O
town	O
of	O
Albi	LOCATION
,	O
northeast	O
of	O
Toulouse	LOCATION
.	O
Use	O
of	O
the	O
name	O
came	O
from	O
the	O
fact	O
that	O
a	O
debate	O
was	O
held	O
in	O
Albi	O
between	O
priests	O
and	O
the	O
Cathars	O
;	O
no	O
conclusion	O
was	O
reached	O
,	O
but	O
from	O
then	O
on	O
it	O
was	O
assumed	O
in	O
France	O
that	O
Cathars	O
were	O
supporters	O
of	O
the	O
"	O
Albigensian	O
doctrine	O
"	O
.	O
(	O
Indeed	O
,	O
the	O
mammoth	O
cathedral	O
at	O
Albi	LOCATION
was	O
built	O
in	O
this	O
period	O
,	O
as	O
something	O
of	O
a	O
show	O
of	O
force	O
against	O
Catharism	O
.	O
)	O
There	O
are	O
a	O
few	O
texts	O
from	O
the	O
Cathars	O
themselves	O
which	O
were	O
preserved	O
by	O
their	O
opponents	O
which	O
give	O
a	O
glimpse	O
of	O
the	O
inner	O
workings	O
of	O
their	O
faith	O
,	O
but	O
these	O
still	O
leave	O
many	O
questions	O
unanswered	O
.	O
A	O
landmark	O
in	O
the	O
"	O
institutional	O
history	O
"	O
of	O
the	O
Cathars	O
was	O
the	O
Council	O
,	O
held	O
in	O
1167	O
at	O
Saint-Félix-Lauragais	LOCATION
,	O
attended	O
by	O
many	O
local	O
figures	O
and	O
also	O
by	O
the	O
Bogomil	O
papa	O
Nicetas	O
,	O
the	O
Cathar	O
bishop	O
of	O
(	O
northern	O
)	O
France	O
and	O
a	O
leader	O
of	O
the	O
Cathars	O
of	O
Lombardy	LOCATION
.	O
Cathars	O
,	O
in	O
general	O
,	O
formed	O
an	O
anti-sacerdotal	O
party	O
in	O
opposition	O
to	O
the	O
Catholic	O
Church	O
,	O
protesting	O
what	O
they	O
perceived	O
to	O
be	O
the	O
moral	O
,	O
spiritual	O
and	O
political	O
corruption	O
of	O
the	O
Church	O
.	O
The	O
Cathars	O
believed	O
there	O
existed	O
within	O
mankind	O
a	O
spark	O
of	O
divine	O
light	O
.	O
The	O
goal	O
of	O
Cathar	O
eschatology	O
was	O
liberation	O
from	O
the	O
realm	O
of	O
limitation	O
and	O
corruption	O
identified	O
with	O
material	O
existence	O
.	O
The	O
Cathars	O
accepted	O
the	O
idea	O
of	O
reincarnation	O
.	O
The	O
consolamentum	O
was	O
the	O
baptism	O
of	O
the	O
Holy	O
Spirit	O
,	O
baptismal	O
regeneration	O
,	O
absolution	O
,	O
and	O
ordination	O
all	O
in	O
one	O
.	O
Catharism	O
was	O
above	O
all	O
a	O
populist	O
religion	O
and	O
the	O
numbers	O
of	O
those	O
who	O
considered	O
themselves	O
"	O
believers	O
"	O
in	O
the	O
late	O
twelfth	O
century	O
included	O
a	O
sizeable	O
portion	O
of	O
the	O
population	O
of	O
Languedoc	LOCATION
,	O
counting	O
among	O
them	O
many	O
noble	O
families	O
and	O
courts	O
.	O
It	O
was	O
claimed	O
by	O
Catharism	O
's	O
opponents	O
that	O
by	O
such	O
self-imposed	O
starvation	O
,	O
the	O
Cathars	O
were	O
committing	O
suicide	O
in	O
order	O
to	O
escape	O
this	O
world	O
.	O
Other	O
than	O
at	O
such	O
moments	O
of	O
extremis	O
,	O
no	O
evidence	O
exists	O
to	O
suggest	O
this	O
was	O
a	O
common	O
Cathar	O
practice	O
.	O
These	O
are	O
views	O
similar	O
to	O
those	O
of	O
Marcion	PERSON
,	O
though	O
Marcion	O
never	O
identified	O
the	O
creative	O
demiurge	O
with	O
Satan	O
,	O
nor	O
said	O
that	O
he	O
was	O
(	O
strictly	O
speaking	O
)	O
evil	O
,	O
merely	O
harsh	O
and	O
dictatorial	O
.	O
The	O
Cathari	O
claimed	O
that	O
this	O
god	O
was	O
in	O
fact	O
a	O
blind	O
usurper	O
who	O
under	O
the	O
most	O
false	O
pretexts	O
,	O
tormented	O
and	O
murdered	O
those	O
whom	O
he	O
called	O
,	O
all	O
too	O
possessively	O
,	O
"	O
his	O
children	O
"	O
.	O
For	O
the	O
Cathars	PERSON
,	O
this	O
world	O
was	O
the	O
only	O
hell	O
--	O
there	O
was	O
nothing	O
to	O
fear	O
after	O
death	O
,	O
save	O
perhaps	O
rebirth	O
.	O
From	O
the	O
theological	O
underpinnings	O
of	O
the	O
Cathar	PERSON
faith	O
there	O
came	O
practical	O
injunctions	O
that	O
were	O
considered	O
destabilising	O
to	O
the	O
morals	O
of	O
medieval	O
society	O
.	O
As	O
a	O
consequence	O
of	O
their	O
rejection	O
of	O
oaths	O
,	O
Cathars	O
also	O
rejected	O
marriage	O
vows	O
.	O
In	O
1147	O
,	O
Pope	O
Eugene	PERSON
III	PERSON
sent	O
a	O
legate	O
to	O
the	O
Cathar	O
district	O
in	O
order	O
to	O
arrest	O
the	O
progress	O
of	O
the	O
Cathars	O
.	O
When	O
Pope	O
Innocent	O
III	O
came	O
to	O
power	O
in	O
1198	O
,	O
he	O
was	O
resolved	O
to	O
deal	O
with	O
them	O
.	O
At	O
first	O
Innocent	O
tried	O
pacific	O
conversion	O
,	O
and	O
sent	O
a	O
number	O
of	O
legates	O
into	O
the	O
Cathar	O
regions	O
.	O
They	O
had	O
to	O
contend	O
not	O
only	O
with	O
the	O
Cathars	O
,	O
the	O
nobles	O
who	O
protected	O
them	O
,	O
and	O
the	O
people	O
who	O
venerated	O
them	O
,	O
but	O
also	O
with	O
many	O
of	O
the	O
bishops	O
of	O
the	O
region	O
,	O
who	O
resented	O
the	O
considerable	O
authority	O
the	O
Pope	O
had	O
conferred	O
upon	O
his	O
legates	O
.	O
Saint	O
Dominic	O
met	O
and	O
debated	O
the	O
Cathars	O
in	O
1203	O
during	O
his	O
mission	O
to	O
the	O
Languedoc	O
.	O
He	O
concluded	O
that	O
only	O
preachers	O
who	O
displayed	O
real	O
sanctity	O
,	O
humility	O
and	O
asceticism	O
could	O
win	O
over	O
convinced	O
Cathar	O
believers	O
.	O
The	O
official	O
Church	O
as	O
a	O
rule	O
did	O
not	O
possess	O
these	O
spiritual	O
warrants	O
.	O
His	O
conviction	O
led	O
eventually	O
to	O
the	O
establishment	O
of	O
the	O
Dominican	O
Order	O
in	O
1216	O
.	O
However	O
,	O
even	O
St.	O
Dominic	O
managed	O
only	O
a	O
few	O
converts	O
among	O
the	O
Cathari	O
.	O
This	O
war	O
pitted	O
the	O
nobles	O
of	O
the	O
north	O
of	O
France	O
against	O
those	O
of	O
the	O
south	O
.	O
The	O
widespread	O
northern	O
enthusiasm	O
for	O
the	O
Crusade	O
was	O
partially	O
inspired	O
by	O
a	O
papal	O
decree	O
permitting	O
the	O
confiscation	O
of	O
lands	O
owned	O
by	O
Cathars	O
and	O
their	O
supporters	O
.	O
In	O
the	O
first	O
significant	O
engagement	O
of	O
the	O
war	O
,	O
the	O
town	O
of	O
Béziers	LOCATION
was	O
besieged	O
on	O
22	O
July	O
1209	O
.	O
The	O
Catholic	O
inhabitants	O
of	O
the	O
city	O
were	O
granted	O
the	O
freedom	O
to	O
leave	O
unharmed	O
,	O
but	O
many	O
refused	O
and	O
opted	O
to	O
stay	O
and	O
fight	O
alongside	O
the	O
Cathars	O
.	O
The	O
Cathars	O
spent	O
much	O
of	O
1209	O
fending	O
off	O
the	O
crusaders	O
.	O
The	O
leader	O
of	O
the	O
crusaders	O
,	O
Simon	PERSON
de	O
Montfort	O
,	O
resorted	O
to	O
primitive	O
psychological	O
warfare	O
.	O
This	O
only	O
served	O
to	O
harden	O
the	O
resolve	O
of	O
the	O
Cathars	O
.	O
The	O
Béziers	O
army	O
attempted	O
a	O
sortie	O
but	O
was	O
quickly	O
defeated	O
,	O
then	O
pursued	O
by	O
the	O
crusaders	O
back	O
through	O
the	O
gates	O
and	O
into	O
the	O
city	O
.	O
The	O
permanent	O
population	O
of	O
Béziers	LOCATION
at	O
that	O
time	O
was	O
then	O
probably	O
no	O
more	O
than	O
5,000	O
,	O
but	O
local	O
refugees	O
seeking	O
shelter	O
within	O
the	O
city	O
walls	O
could	O
conceivably	O
have	O
increased	O
the	O
number	O
to	O
20,000	O
.	O
Peter	PERSON
died	O
fighting	O
against	O
the	O
crusade	O
on	O
12	O
September	O
1213	O
at	O
the	O
Battle	O
of	O
Muret	O
.	O
The	O
independence	O
of	O
the	O
princes	O
of	O
the	O
Languedoc	O
was	O
at	O
an	O
end	O
.	O
But	O
in	O
spite	O
of	O
the	O
wholesale	O
massacre	O
of	O
Cathars	O
during	O
the	O
war	O
,	O
Catharism	O
was	O
not	O
yet	O
extinguished	O
.	O
In	O
1215	O
,	O
the	O
bishops	O
of	O
the	O
Catholic	O
Church	O
met	O
at	O
the	O
Fourth	O
Council	O
of	O
the	O
Lateran	O
under	O
Pope	O
Innocent	O
III	O
.	O
One	O
of	O
the	O
key	O
goals	O
of	O
the	O
council	O
was	O
to	O
combat	O
the	O
heresy	O
of	O
the	O
Cathars	O
.	O
Operating	O
in	O
the	O
south	O
at	O
Toulouse	LOCATION
,	O
Albi	O
,	O
Carcassonne	LOCATION
and	O
other	O
towns	O
during	O
the	O
whole	O
of	O
the	O
13th	O
century	O
,	O
and	O
a	O
great	O
part	O
of	O
the	O
14th	O
,	O
it	O
finally	O
succeeded	O
in	O
extirpating	O
the	O
movement	O
.	O
Cathars	O
who	O
refused	O
to	O
recant	O
were	O
hanged	O
,	O
or	O
burnt	O
at	O
the	O
stake	O
.	O
From	O
May	O
1243	O
to	O
March	O
1244	O
,	O
the	O
Cathar	O
fortress	O
of	O
Montségur	LOCATION
was	O
besieged	O
by	O
the	O
troops	O
of	O
the	O
seneschal	O
of	O
Carcassonne	LOCATION
and	O
the	O
archbishop	O
of	O
Narbonne	LOCATION
.	O
On	O
16	O
March	O
1244	O
,	O
a	O
large	O
and	O
symbolically	O
important	O
massacre	O
took	O
place	O
,	O
where	O
over	O
200	O
Cathar	O
perfects	O
were	O
burnt	O
in	O
an	O
enormous	O
fire	O
at	O
the	LOCATION
prat	LOCATION
des	LOCATION
cramats	LOCATION
near	O
the	O
foot	O
of	O
the	O
castle	O
.	O
A	O
popular	O
though	O
as	O
yet	O
unsubstantiated	O
theory	O
holds	O
that	O
a	O
small	O
party	O
of	O
Cathar	O
perfects	O
escaped	O
from	O
the	O
fortress	O
before	O
the	O
massacre	O
at	O
prat	O
des	O
cramats	O
.	O
It	O
is	O
widely	O
held	O
in	O
the	O
Cathar	O
region	O
to	O
this	O
day	O
that	O
the	O
escapees	O
took	O
with	O
them	O
le	O
tresor	O
cathar	O
.	O
Having	O
recanted	O
,	O
they	O
were	O
obliged	O
to	O
sew	O
yellow	O
crosses	O
onto	O
their	O
outdoor	O
clothing	O
and	O
to	O
live	O
apart	O
from	O
other	O
Catholics	O
,	O
at	O
least	O
for	O
a	O
while	O
.	O
The	O
last	O
known	O
Cathar	O
perfectus	O
in	O
the	O
Languedoc	O
,	O
Guillaume	PERSON
Bélibaste	PERSON
,	O
was	O
executed	O
in	O
1321	O
.	O
It	O
is	O
possible	O
that	O
Cathar	O
ideas	O
were	O
too	O
.	O
After	O
the	O
suppression	O
of	O
Catharism	O
,	O
the	O
descendants	O
of	O
Cathars	O
were	O
at	O
times	O
required	O
to	O
live	O
outside	O
towns	O
and	O
their	O
defences	O
.	O
They	O
thus	O
retained	O
a	O
certain	O
Cathar	O
identity	O
,	O
despite	O
having	O
returned	O
to	O
the	O
Catholic	O
religion	O
.	O
Any	O
use	O
of	O
the	O
term	O
"	O
Cathar	O
"	O
to	O
refer	O
to	O
people	O
after	O
the	O
suppression	O
of	O
Catharism	O
in	O
the	O
14th	O
century	O
is	O
a	O
cultural	O
or	O
ancestral	O
reference	O
,	O
and	O
has	O
no	O
religious	O
implication	O
.	O
Nevertheless	O
,	O
interest	O
in	O
the	O
Cathars	O
,	O
their	O
history	O
,	O
legacy	O
and	O
beliefs	O
continues	O
.	O
Also	O
,	O
the	O
Cathars	O
have	O
been	O
depicted	O
in	O
popular	O
books	O
such	O
as	O
The	O
Holy	O
Blood	O
and	O
the	O
Holy	O
Grail	O
.	O
These	O
areas	O
have	O
ruins	O
from	O
the	O
wars	O
against	O
the	O
Cathars	O
which	O
are	O
still	O
visible	O
today	O
.	O
The	O
Cathars	O
sought	O
refuge	O
at	O
these	O
sites	O
.	O
This	O
does	O
not	O
mean	O
they	O
claim	O
to	O
be	O
Cathar	O
by	O
religion	O
(	O
the	O
local	O
religion	O
is	O
overwhelmingly	O
Roman	O
Catholicism	O
)	O
.	O
It	O
can	O
be	O
safely	O
assumed	O
that	O
many	O
inhabitants	O
of	O
these	O
areas	O
have	O
some	O
ancestors	O
who	O
were	O
Cathars	O
.	O
