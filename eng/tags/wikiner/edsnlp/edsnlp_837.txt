However	O
,	O
perhaps	O
the	O
first	O
organised	O
attempt	O
to	O
conserve	O
cultural	O
patrimony	O
was	O
the	O
Society	O
for	O
the	O
Protection	O
of	O
Ancient	O
Buildings	O
in	O
the	O
UK	O
,	O
influenced	O
by	O
the	O
writings	O
of	O
John	PERSON
Ruskin	PERSON
the	O
society	O
was	O
founded	O
by	O
William	PERSON
Morris	PERSON
and	O
Philip	PERSON
Webb	PERSON
in	O
1877	O
.	O
This	O
department	O
had	O
been	O
created	O
by	O
the	O
museum	O
to	O
address	O
objects	O
in	O
the	O
collection	O
that	O
had	O
begun	O
to	O
rapidly	O
deteriorate	O
as	O
a	O
result	O
of	O
being	O
stored	O
in	O
the	O
London	O
Underground	O
tunnels	O
during	O
the	O
First	O
World	O
War	O
.	O
Art	O
historians	O
and	O
theorists	O
such	O
as	O
Cesare	PERSON
Brandi	PERSON
have	O
also	O
played	O
a	O
significant	O
role	O
in	O
developing	O
conservation-restoration	O
theory	O
.	O
This	O
is	O
acknowledged	O
by	O
the	O
American	O
Institute	O
for	O
Conservation	O
who	O
advise	O
"	O
Specific	O
admission	O
requirements	O
differ	O
and	O
potential	O
candidates	O
are	O
encouraged	O
to	O
contact	O
the	O
programs	O
directly	O
for	O
details	O
on	O
prerequisites	O
,	O
application	O
procedures	O
,	O
and	O
program	O
curriculum	O
"	O
.	O
One	O
early	O
example	O
is	O
the	O
founding	O
in	O
1877	O
of	O
the	O
Society	O
for	O
the	O
Protection	O
of	O
Ancient	O
Buildings	O
in	O
Britain	O
to	O
protect	O
the	O
built	O
heritage	O
,	O
this	O
society	O
continues	O
to	O
be	O
active	O
today	O
.	O
