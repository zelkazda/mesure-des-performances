The	O
Ark	O
and	O
its	O
sanctuary	O
were	O
considered	O
"	O
the	O
beauty	O
of	O
Israel	O
"	O
.	O
Rashi	O
and	O
some	O
Midrashim	O
suggest	O
that	O
there	O
were	O
two	O
arks	O
--	O
a	O
temporary	O
one	O
made	O
by	O
Moses	O
himself	O
,	O
and	O
a	O
later	O
one	O
constructed	O
by	O
Bezalel	O
.	O
When	O
the	O
Ark	O
was	O
borne	O
by	O
priests	O
into	O
the	O
bed	O
of	O
the	O
Jordan	O
,	O
water	O
in	O
the	O
river	O
separated	O
,	O
opening	O
a	O
pathway	O
for	O
the	O
entire	O
host	O
to	O
pass	O
through	O
(	O
Josh	PERSON
.	O
The	O
city	O
of	O
Jericho	LOCATION
was	O
taken	O
with	O
no	O
more	O
than	O
a	O
shout	O
after	O
the	O
Ark	O
of	O
Covenant	O
was	O
paraded	O
for	O
seven	O
days	O
around	O
its	O
wall	O
by	O
seven	O
priests	O
sounding	O
seven	O
trumpets	O
of	O
rams	O
'	O
horns	O
(	O
Josh	PERSON
.	O
Over	O
time	O
,	O
the	O
accounts	O
of	O
the	O
Ark	O
have	O
gathered	O
a	O
number	O
of	O
references	O
in	O
popular	O
culture	O
.	O
The	O
Hebrew	O
word	O
aron	O
as	O
used	O
in	O
the	O
Bible	O
refers	O
to	O
any	O
type	O
of	O
ark	O
,	O
chest	O
or	O
coffer	O
(	O
Book	O
of	O
Genesis	O
50:26	O
;	O
2	O
Kings	O
12:9	O
,	O
10	O
)	O
.	O
The	O
Ark	O
of	O
the	O
Covenant	O
is	O
distinguished	O
from	O
all	O
others	O
by	O
such	O
titles	O
as	O
:	O
Four	O
rings	O
of	O
gold	O
are	O
to	O
be	O
put	O
into	O
its	O
four	O
feet	O
--	O
two	O
on	O
each	O
side	O
--	O
and	O
through	O
these	O
rings	O
staves	O
of	O
shittim-wood	O
overlaid	O
with	O
gold	O
for	O
carrying	O
the	O
Ark	O
are	O
to	O
be	O
inserted	O
;	O
and	O
these	O
are	O
not	O
to	O
be	O
removed	O
.	O
A	O
golden	O
cover	O
,	O
adorned	O
with	O
golden	O
cherubim	O
,	O
is	O
to	O
be	O
placed	O
above	O
the	O
Ark	O
.	O
The	O
Ark	O
is	O
finally	O
to	O
be	O
placed	O
behind	O
a	O
veil	O
,	O
a	O
full	O
description	O
of	O
which	O
is	O
also	O
given	O
.	O
The	O
Ark	O
of	O
the	O
Covenant	O
is	O
mentioned	O
in	O
both	O
the	O
Bible	O
and	O
the	O
Qur'an	O
.	O
In	O
the	O
Book	O
of	O
Jeremiah	O
,	O
it	O
is	O
referenced	O
by	O
Jeremiah	PERSON
,	O
who	O
,	O
speaking	O
in	O
the	O
days	O
of	O
Josiah	O
(	O
Jer.	O
3:16	O
)	O
,	O
prophesied	O
a	O
future	O
time	O
when	O
the	O
Ark	O
will	O
no	O
longer	O
be	O
talked	O
about	O
or	O
be	O
made	O
again	O
.	O
In	O
the	O
New	O
Testament	O
,	O
the	O
Ark	O
is	O
mentioned	O
in	O
the	O
Book	O
of	O
Hebrews	O
and	O
the	O
Book	O
of	O
Revelation	O
.	O
Hebrews	O
9:4	O
states	O
that	O
the	O
Ark	O
contained	O
"	O
the	O
golden	O
pot	O
that	O
had	O
manna	O
,	O
and	O
Aaron	PERSON
's	O
rod	O
that	O
budded	O
,	O
and	O
the	O
tablets	O
of	O
the	O
covenant	O
.	O
"	O
It	O
is	O
mentioned	O
in	O
the	O
middle	O
of	O
the	O
narrative	O
of	O
the	O
choice	O
of	O
Saul	PERSON
to	O
be	O
king	O
.	O
During	O
the	O
crossing	O
,	O
the	O
river	O
grew	O
dry	O
as	O
soon	O
as	O
the	O
feet	O
of	O
the	O
priests	O
carrying	O
the	O
Ark	O
touched	O
its	O
waters	O
;	O
and	O
remained	O
so	O
until	O
the	O
priests	O
--	O
with	O
the	O
Ark	O
--	O
left	O
the	O
river	O
,	O
after	O
the	O
people	O
had	O
passed	O
over	O
(	O
Josh	PERSON
.	O
As	O
memorials	O
,	O
twelve	O
stones	O
were	O
taken	O
from	O
the	O
Jordan	O
at	O
the	O
place	O
where	O
the	O
priests	O
had	O
stood	O
(	O
Josh	PERSON
.	O
On	O
the	O
seventh	O
day	O
the	O
seven	O
priests	O
sounding	O
the	O
seven	O
trumpets	O
of	O
rams	O
'	O
horns	O
before	O
the	O
Ark	O
compassed	O
the	O
city	O
seven	O
times	O
and	O
with	O
a	O
great	O
shout	O
,	O
Jericho	O
's	O
wall	O
fell	O
down	O
flat	O
and	O
the	O
people	O
took	O
the	O
city	O
(	O
Josh	PERSON
.	O
The	O
Ark	O
is	O
next	O
spoken	O
of	O
as	O
being	O
in	O
the	O
Tabernacle	O
at	O
Shiloh	O
during	O
Samuel	O
's	O
apprenticeship	O
(	O
1	O
Sam	O
.	O
The	O
Ark	O
was	O
taken	O
by	O
the	O
Philistines	O
(	O
1	O
Sam	O
.	O
4:3-11	O
)	O
who	O
subsequently	O
sent	O
it	O
back	O
after	O
retaining	O
it	O
for	O
seven	O
months	O
(	O
1	O
Sam	O
.	O
The	O
news	O
of	O
its	O
capture	O
was	O
at	O
once	O
taken	O
to	O
Shiloh	O
by	O
a	O
messenger	O
"	O
with	O
his	O
clothes	O
rent	O
,	O
and	O
with	O
earth	O
upon	O
his	O
head	O
.	O
"	O
The	O
old	O
priest	O
,	O
Eli	PERSON
,	O
fell	O
dead	O
when	O
he	O
heard	O
it	O
;	O
and	O
his	O
daughter-in-law	O
,	O
bearing	O
a	O
son	O
at	O
the	O
time	O
the	O
news	O
of	O
the	O
capture	O
of	O
the	O
Ark	O
was	O
received	O
,	O
named	O
him	O
Ichabod	O
--	O
explained	O
as	O
"	O
Where	O
is	O
glory	O
?	O
"	O
in	O
reference	O
to	O
the	O
loss	O
of	O
the	O
Ark	O
(	O
1	O
Sam	O
.	O
The	O
Philistines	O
took	O
the	O
Ark	O
to	O
several	O
places	O
in	O
their	O
country	O
,	O
and	O
at	O
each	O
place	O
misfortune	O
befell	O
them	O
(	O
1	O
Sam	O
.	O
At	O
Ashdod	O
it	O
was	O
placed	O
in	O
the	O
temple	O
of	O
Dagon	O
.	O
The	O
next	O
morning	O
Dagon	O
was	O
found	O
prostrate	O
,	O
bowed	O
down	O
,	O
before	O
it	O
;	O
and	O
on	O
being	O
restored	O
to	O
his	O
place	O
,	O
he	O
was	O
on	O
the	O
following	O
morning	O
again	O
found	O
prostrate	O
and	O
broken	O
.	O
The	O
people	O
of	O
Ashdod	LOCATION
were	O
smitten	O
with	O
tumors	O
;	O
a	O
plague	O
of	O
rats	O
was	O
sent	O
over	O
the	O
land	O
(	O
1	O
Sam	O
.	O
The	O
affliction	O
of	O
boils	O
was	O
also	O
visited	O
upon	O
the	O
people	O
of	O
Gath	LOCATION
and	O
of	O
Ekron	LOCATION
,	O
whither	O
the	O
Ark	O
was	O
successively	O
removed	O
(	O
1	O
Sam	O
.	O
The	O
Ark	O
was	O
set	O
in	O
the	O
field	O
of	O
Joshua	O
the	O
Beth-shemite	O
,	O
and	O
the	O
Beth-shemites	O
offered	O
sacrifices	O
and	O
burnt	O
offerings	O
(	O
1	O
Sam	O
.	O
Out	O
of	O
curiosity	O
the	O
men	O
of	O
Beth-shemesh	LOCATION
gazed	O
at	O
the	O
Ark	O
;	O
and	O
as	O
a	O
punishment	O
,	O
seventy	O
of	O
them	O
(	O
fifty	O
thousand	O
seventy	O
in	O
some	O
ms.	O
)	O
were	O
smitten	O
by	O
the	O
Lord	O
(	O
1	O
Sam	O
.	O
Kirjath-jearim	O
remained	O
the	O
abode	O
of	O
the	O
Ark	O
for	O
twenty	O
years	O
.	O
Under	O
Saul	PERSON
,	O
the	O
Ark	O
was	O
with	O
the	O
army	O
before	O
he	O
first	O
met	O
the	O
Philistines	O
,	O
but	O
the	O
king	O
was	O
too	O
impatient	O
to	O
consult	O
it	O
before	O
engaging	O
in	O
battle	O
.	O
In	O
1	O
Chronicles	O
13:3	O
it	O
is	O
stated	O
that	O
the	O
people	O
were	O
not	O
accustomed	O
to	O
consult	O
the	O
Ark	O
in	O
the	O
days	O
of	O
Saul	O
.	O
At	O
the	O
beginning	O
of	O
his	O
reign	O
,	O
King	O
David	O
removed	O
the	O
Ark	O
from	O
Kirjath-jearim	O
amid	O
great	O
rejoicing	O
.	O
6:12-16	O
,	O
20-22	O
;	O
1	O
Chron.	O
15	O
)	O
.	O
6:17-20	O
;	O
1	O
Chron.	O
16:1-3	O
;	O
2	O
Chron.	O
1:4	O
)	O
.	O
7:1-17	O
;	O
1	O
Chron.	O
17:1-15	O
;	O
28:2	O
,	O
3	O
)	O
.	O
The	O
Ark	O
was	O
with	O
the	O
army	O
during	O
the	O
siege	O
of	O
Rabbah	O
(	O
2	O
Sam	O
.	O
When	O
Abiathar	O
was	O
dismissed	O
from	O
the	O
priesthood	O
by	O
King	O
Solomon	PERSON
for	O
having	O
taken	O
part	O
in	O
Adonijah	O
's	O
conspiracy	O
against	O
David	O
,	O
his	O
life	O
was	O
spared	O
because	O
he	O
had	O
formerly	O
borne	O
the	O
Ark	O
(	O
1	O
Kings	O
2:26	O
)	O
.	O
Holy	O
of	O
Holies	O
)	O
,	O
was	O
prepared	O
to	O
receive	O
and	O
house	O
the	O
Ark	O
(	O
1	O
Kings	O
6:19	O
)	O
;	O
and	O
when	O
the	O
Temple	O
was	O
dedicated	O
,	O
the	O
Ark	O
--	O
containing	O
the	O
original	O
tablets	O
of	O
the	O
Ten	O
Commandments	O
--	O
was	O
placed	O
therein	O
.	O
When	O
the	O
priests	O
emerged	O
from	O
the	O
holy	O
place	O
after	O
placing	O
the	O
Ark	O
there	O
,	O
the	O
Temple	O
was	O
filled	O
with	O
a	O
cloud	O
,	O
"	O
for	O
the	O
glory	O
of	O
the	O
Lord	O
had	O
filled	O
the	O
house	O
of	O
the	O
Lord	O
"	O
(	O
1	O
Kings	O
8:10-11	O
;	O
2	O
Chron.	O
5:13	O
,	O
14	O
)	O
.	O
King	O
Josiah	PERSON
had	O
the	O
Ark	O
put	O
in	O
the	O
Temple	O
(	O
2	O
Chron.	O
35:3	O
)	O
,	O
whence	O
it	O
appears	O
to	O
have	O
again	O
been	O
removed	O
by	O
one	O
of	O
his	O
successors	O
.	O
In	O
586	O
BC	O
,	O
the	O
Babylonians	O
besieged	O
Jerusalem	LOCATION
and	O
,	O
once	O
captured	O
,	O
plundered	O
and	O
destroyed	O
Solomon	O
's	O
Temple	O
.	O
Some	O
historians	O
suggest	O
that	O
the	O
Ark	O
was	O
probably	O
taken	O
away	O
by	O
Nebuchadnezzar	O
or	O
perhaps	O
destroyed	O
in	O
battle	O
.	O
Consequently	O
,	O
the	O
rebuilt	O
Second	O
Temple	O
did	O
not	O
house	O
the	O
Ark	O
in	O
its	O
Holy	O
of	O
Holies	O
room	O
.	O
Since	O
its	O
disappearance	O
,	O
the	O
Ark	O
entered	O
the	O
domain	O
of	O
legend	O
,	O
and	O
some	O
have	O
claimed	O
to	O
have	O
discovered	O
or	O
have	O
possession	O
of	O
the	O
Ark	O
.	O
However	O
,	O
digging	O
beneath	O
the	O
Temple	O
Mount	O
itself	O
is	O
heavily	O
restricted	O
due	O
to	O
the	O
religious	O
and	O
political	O
sensitivity	O
surrounding	O
the	O
area	O
.	O
The	O
Ethiopian	O
Orthodox	O
Church	O
claims	O
to	O
possess	O
the	O
Ark	O
of	O
the	O
Covenant	O
or	O
tabot	O
in	O
Axum	O
.	O
The	O
object	O
is	O
now	O
kept	O
under	O
guard	O
in	O
a	O
treasury	O
near	O
the	O
Church	O
of	O
Our	O
Lady	O
Mary	O
of	O
Zion	LOCATION
,	O
and	O
used	O
occasionally	O
in	O
ritual	O
processions	O
.	O
The	O
Kebra	O
Nagast	O
,	O
composed	O
to	O
legitimise	O
the	O
new	O
dynasty	O
ruling	O
Ethiopia	O
following	O
its	O
establishment	O
in	O
1270	O
,	O
narrates	O
how	O
the	O
real	O
Ark	O
of	O
the	O
Covenant	O
was	O
brought	O
to	O
Ethiopia	O
by	O
Menelik	O
I	O
with	O
divine	O
assistance	O
,	O
while	O
a	O
forgery	O
was	O
left	O
in	O
the	O
Temple	O
in	O
Jerusalem	LOCATION
.	O
Although	O
the	O
Kebra	O
Nagast	O
is	O
the	O
best-known	O
account	O
of	O
this	O
belief	O
,	O
this	O
belief	O
predates	O
this	O
document	O
.	O
The	O
following	O
day	O
,	O
on	O
26	O
June	O
2009	O
,	O
the	O
patriarch	O
announced	O
that	O
he	O
would	O
not	O
unveil	O
the	O
Ark	O
after	O
all	O
,	O
but	O
that	O
instead	O
he	O
could	O
attest	O
to	O
its	O
current	O
status	O
.	O
On	O
14	O
April	O
2008	O
,	O
in	O
a	O
UK	O
Channel	O
4	O
documentary	O
broadcast	O
,	O
Tudor	PERSON
Parfitt	PERSON
,	O
taking	O
a	O
literalist	O
approach	O
to	O
the	O
Biblical	O
story	O
,	O
described	O
his	O
research	O
into	O
this	O
claim	O
.	O
He	O
says	O
that	O
the	O
object	O
described	O
by	O
the	O
Lemba	PERSON
has	O
attributes	O
similar	O
to	O
the	O
Ark	O
.	O
Lemba	O
tradition	O
maintains	O
that	O
the	O
Ark	O
spent	O
some	O
time	O
in	O
Sena	O
in	O
Yemen	O
.	O
According	O
to	O
their	O
oral	O
traditions	O
,	O
some	O
time	O
after	O
the	O
arrival	O
of	O
the	O
Lemba	O
with	O
the	O
Ark	O
,	O
it	O
self-destructed	O
.	O
Using	O
a	O
core	O
from	O
the	O
original	O
,	O
the	O
Lemba	PERSON
priests	O
constructed	O
a	O
new	O
one	O
.	O
Parfitt	O
suggests	O
that	O
the	O
Ark	O
he	O
found	O
was	O
the	O
descendant	O
of	O
the	O
Ark	O
of	O
War	O
and	O
that	O
a	O
wooden	O
chest	O
being	O
used	O
as	O
a	O
weapon	O
was	O
replicated	O
at	O
least	O
once	O
,	O
and	O
possibly	O
many	O
times	O
.	O
Parfitt	O
offers	O
the	O
suggestion	O
that	O
the	O
wooden	O
ark	O
may	O
always	O
have	O
been	O
a	O
drum	O
as	O
well	O
as	O
a	O
weapon	O
of	O
some	O
sort	O
,	O
like	O
the	O
ngoma	O
.	O
It	O
was	O
often	O
found	O
in	O
musical	O
processions	O
,	O
David	O
danced	O
in	O
front	O
of	O
it	O
and	O
it	O
was	O
covered	O
over	O
with	O
a	O
piece	O
of	O
leather	O
.	O
Parfitt	O
,	O
however	O
,	O
offers	O
no	O
explanation	O
of	O
the	O
original	O
principal	O
contents	O
of	O
the	O
Ark	O
,	O
the	O
stone	O
tablets	O
.	O
