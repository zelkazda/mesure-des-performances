Traditionally	O
positions	O
are	O
given	O
using	O
degrees	O
,	O
minutes	O
,	O
and	O
seconds	O
of	O
angles	O
in	O
two	O
measurements	O
:	O
one	O
for	O
latitude	O
,	O
the	O
angle	O
north	O
or	O
south	O
of	O
the	O
equator	O
;	O
and	O
one	O
for	O
longitude	O
,	O
the	O
angle	O
east	O
or	O
west	O
of	O
the	O
Prime	O
Meridian	O
.	O
The	O
dwarf	O
planet	O
Pluto	O
has	O
proven	O
difficult	O
to	O
resolve	O
because	O
its	O
angular	O
diameter	O
is	O
about	O
0.1	O
arcsecond	O
.	O
For	O
example	O
,	O
the	O
Hubble	O
space	O
telescope	O
can	O
reach	O
an	O
angular	O
size	O
of	O
stars	O
down	O
to	O
about	O
0.1	O
"	O
.	O
