Alexander	PERSON
Graham	PERSON
Bell	PERSON
considered	O
the	O
invention	O
of	O
the	O
hydroplane	O
a	O
very	O
significant	O
achievement	O
,	O
and	O
after	O
reading	O
the	O
article	O
began	O
to	O
sketch	O
concepts	O
of	O
what	O
is	O
now	O
called	O
a	O
hydrofoil	O
boat	O
.	O
With	O
his	O
chief	O
engineer	O
Casey	PERSON
Baldwin	PERSON
,	O
Bell	O
began	O
hydrofoil	O
experiments	O
in	O
the	O
summer	O
of	O
1908	O
.	O
Baldwin	O
studied	O
the	O
work	O
of	O
the	O
Italian	O
inventor	O
Enrico	PERSON
Forlanini	PERSON
and	O
began	O
testing	O
models	O
based	O
on	O
his	O
designs	O
,	O
which	O
led	O
them	O
to	O
the	O
development	O
of	O
hydrofoil	O
watercraft	O
.	O
Baldwin	O
described	O
it	O
as	O
being	O
as	O
smooth	O
as	O
flying	O
.	O
Using	O
Renault	O
engines	O
,	O
a	O
top	O
speed	O
of	O
87	O
km/h	O
(	O
54	O
mph	O
)	O
was	O
achieved	O
,	O
accelerating	O
rapidly	O
,	O
taking	O
waves	O
without	O
difficulty	O
,	O
steering	O
well	O
and	O
showing	O
good	O
stability	O
.	O
During	O
the	O
same	O
period	O
the	O
Soviet	O
Union	O
experimented	O
extensively	O
with	O
hydrofoils	O
,	O
constructing	O
hydrofoil	O
river	O
boats	O
and	O
ferries	O
with	O
streamlined	O
designs	O
during	O
the	O
cold	O
war	O
period	O
and	O
into	O
the	O
1980s	O
.	O
One	O
of	O
the	O
most	O
successful	O
Soviet	O
designer/inventor	O
in	O
this	O
area	O
was	O
Rostislav	PERSON
Alexeyev	PERSON
who	O
some	O
consider	O
the	O
'	O
father	O
'	O
of	O
the	O
modern	O
hydrofoil	O
due	O
to	O
his	O
1950	O
's	O
era	O
high	O
speed	O
hydrofoil	O
designs	O
.	O
The	O
Bras	O
d'Or	O
was	O
a	O
surface-piercing	O
type	O
which	O
performed	O
well	O
during	O
her	O
trials	O
,	O
reaching	O
a	O
maximum	O
speed	O
of	O
63	O
knots	O
(	O
117	O
km/h	O
)	O
.	O
The	O
U.S.	O
Navy	O
operated	O
combat	O
hydrofoils	O
,	O
such	O
as	O
the	O
Pegasus	O
class	O
,	O
from	O
1977	O
through	O
1993	O
.	O
The	O
Italian	O
Navy	O
has	O
used	O
six	O
hydrofoils	O
of	O
the	O
Nibbio	O
class	O
from	O
the	O
late	O
1970s	O
.	O
Three	O
similar	O
boats	O
were	O
built	O
for	O
the	O
Japan	O
Maritime	O
Self-Defense	O
Force	O
.	O
In	O
September	O
2009	O
,	O
the	O
Hydroptère	O
set	O
new	O
sailcraft	O
world	O
speed	O
records	O
in	O
the	O
500	O
m	O
category	O
,	O
with	O
a	O
speed	O
of	O
51.36	O
knots	O
(	O
95.12	O
km/h	O
)	O
and	O
in	O
the	O
one	O
nautical	O
mile	O
category	O
with	O
a	O
speed	O
of	O
50.17	O
knots	O
(	O
92.91	O
km/h	O
)	O
.	O
The	O
Moth	O
dinghy	O
has	O
evolved	O
into	O
some	O
radical	O
foil	O
configurations	O
.	O
A	O
new	O
kayak	O
design	O
,	O
called	O
Flyak	O
,	O
has	O
hydrofoils	O
that	O
lift	O
the	O
kayak	O
enough	O
to	O
significantly	O
reduce	O
drag	O
,	O
allowing	O
speeds	O
of	O
up	O
to	O
27	O
km/h	O
(	O
17	O
mph	O
)	O
.	O
Soviet	O
Union	O
built	O
Voskhods	O
are	O
one	O
of	O
the	O
most	O
successful	O
passenger	O
hydrofoil	O
designs	O
.	O
The	O
Boeing	O
929	O
is	O
widely	O
used	O
in	O
Asia	O
for	O
passenger	O
services	O
between	O
the	O
many	O
islands	O
of	O
Japan	O
,	O
between	O
Hong	LOCATION
Kong	LOCATION
and	LOCATION
Macau	LOCATION
and	O
on	O
the	O
Korean	O
peninsula	O
.	O
