Valhalla	O
is	O
located	O
within	O
Asgard	O
.	O
Odin	PERSON
and	O
his	O
wife	O
,	O
Frigg	PERSON
are	O
the	O
rulers	O
of	O
Asgard	O
.	O
The	O
Prose	O
Edda	PERSON
presents	O
two	O
views	O
regarding	O
Asgard	O
.	O
Snorri	PERSON
further	O
writes	O
that	O
Asgard	O
is	O
a	O
land	O
more	O
fertile	O
than	O
any	O
other	O
,	O
blessed	O
also	O
with	O
a	O
great	O
abundance	O
of	O
gold	O
and	O
jewels	O
.	O
Snorri	O
proposes	O
the	O
location	O
of	O
Asgard	O
as	O
Troy	O
,	O
the	O
center	O
of	O
the	O
earth	O
.	O
The	O
latter	O
was	O
raised	O
in	O
Thrace	O
.	O
In	O
Gylfaginning	O
,	O
Snorri	O
presents	O
the	O
mythological	O
version	O
taken	O
no	O
doubt	O
from	O
his	O
sources	O
.	O
A	O
revelation	O
of	O
the	O
ancient	O
myths	O
follows	O
,	O
but	O
at	O
the	O
end	O
the	O
palace	O
and	O
the	O
people	O
disappear	O
in	O
a	O
clap	O
of	O
thunder	O
and	O
Gylfi	O
finds	O
himself	O
alone	O
on	O
the	O
plain	O
,	O
having	O
been	O
deluded	O
(	O
Section	O
59	O
)	O
.	O
In	O
Gylfi	O
's	O
delusion	O
,	O
ancient	O
Asgard	O
was	O
ruled	O
by	O
the	O
senior	O
god	O
,	O
the	O
all-father	O
,	O
who	O
had	O
twelve	O
names	O
.	O
Odin	PERSON
is	O
identified	O
as	O
the	O
all-father	O
.	O
Asgard	O
is	O
conceived	O
as	O
being	O
on	O
the	O
earth	O
.	O
Snorri	O
quips	O
:	O
"	O
There	O
is	O
a	O
huge	O
crowd	O
there	O
,	O
and	O
there	O
will	O
be	O
many	O
more	O
still	O
...	O
.	O
"	O
(	O
Section	O
39	O
)	O
.	O
Toward	O
the	O
end	O
of	O
the	O
chapter	O
Snorri	O
becomes	O
prophetic	O
,	O
describing	O
Ragnarök	O
,	O
the	O
twilight	O
of	O
the	O
gods	O
.	O
Snorri	O
quotes	O
his	O
own	O
source	O
saying	O
:	O
"	O
The	O
sun	O
will	O
go	O
black	O
,	O
earth	O
sink	O
in	O
the	O
sea	O
,	O
heaven	O
be	O
stripped	O
of	O
its	O
bright	O
stars	O
;	O
...	O
.	O
"	O
(	O
Section	O
56	O
)	O
.	O
By	O
the	O
time	O
of	O
the	O
Ynglinga	O
Saga	O
,	O
Snorri	O
had	O
developed	O
his	O
concept	O
of	O
Asgard	O
further	O
,	O
although	O
the	O
differences	O
might	O
be	O
accounted	O
for	O
by	O
his	O
sources	O
.	O
It	O
is	O
never	O
called	O
that	O
prior	O
to	O
the	O
Vikings	O
(	O
Section	O
1	O
)	O
.	O
The	O
river	O
lands	O
are	O
occupied	O
by	O
the	O
Vanir	O
and	O
are	O
called	O
Vanaland	O
or	O
Vanaheim	O
.	O
On	O
the	O
border	O
of	O
Sweden	O
is	O
a	O
mountain	O
range	O
running	O
from	O
northeast	O
to	O
southwest	O
.	O
Snorri	O
evidences	O
no	O
knowledge	O
of	O
them	O
.	O
There	O
also	O
is	O
no	O
mention	O
of	O
Troy	O
,	O
which	O
was	O
not	O
far	O
from	O
Constantinople	O
,	O
capital	O
of	O
the	O
Byzantine	O
empire	O
and	O
militarily	O
beyond	O
the	O
reach	O
of	O
the	O
Vikings	O
.	O
To	O
what	O
extent	O
Snorri	O
's	O
presentation	O
is	O
poetic	O
creation	O
only	O
remains	O
unclear	O
.	O
As	O
a	O
man	O
,	O
however	O
,	O
Odin	O
is	O
faced	O
with	O
the	O
necessity	O
to	O
die	O
.	O
If	O
Asgard	O
is	O
an	O
earthly	O
place	O
,	O
not	O
there	O
.	O
