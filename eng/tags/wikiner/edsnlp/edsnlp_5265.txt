Besides	O
recording	O
as	O
a	O
solo	O
artist	O
,	O
he	O
also	O
worked	O
in	O
several	O
notable	O
bands	O
,	O
including	O
the	O
International	O
Submarine	O
Band	O
,	O
The	O
Byrds	O
and	O
The	O
Flying	O
Burrito	O
Brothers	O
.	O
His	O
career	O
,	O
though	O
short	O
,	O
is	O
described	O
by	O
Allmusic	O
as	O
"	O
enormously	O
influential	O
"	O
for	O
both	O
country	O
and	O
rock	O
,	O
"	O
blending	O
the	O
two	O
genres	O
to	O
the	O
point	O
that	O
they	O
became	O
indistinguishable	O
from	O
each	O
other	O
.	O
"	O
Born	O
in	O
1946	O
,	O
Parsons	PERSON
emerged	O
from	O
a	O
troubled	O
childhood	O
to	O
attend	O
Harvard	O
University	O
.	O
He	O
founded	O
the	O
International	O
Submarine	O
Band	O
in	O
1966	O
,	O
and	O
after	O
several	O
months	O
of	O
delay	O
their	O
debut	O
,	O
Safe	O
at	O
Home	O
,	O
was	O
released	O
in	O
1968	O
,	O
by	O
which	O
time	O
the	O
group	O
had	O
disbanded	O
.	O
After	O
leaving	O
the	O
group	O
in	O
late	O
1968	O
,	O
Parsons	PERSON
and	O
fellow	O
Byrd	O
Chris	PERSON
Hillman	PERSON
formed	O
The	O
Flying	O
Burrito	O
Brothers	O
in	O
1969	O
,	O
releasing	O
their	O
debut	O
,	O
The	O
Gilded	O
Palace	O
of	O
Sin	LOCATION
,	O
the	O
same	O
year	O
.	O
The	O
album	O
was	O
well	O
received	O
but	O
failed	O
commercially	O
;	O
after	O
a	O
sloppy	O
cross-country	O
tour	O
,	O
they	O
hastily	O
recorded	O
Burrito	O
Deluxe	O
.	O
Parsons	PERSON
was	O
fired	O
from	O
the	O
band	O
before	O
its	O
release	O
in	O
early	O
1970	O
.	O
He	O
soon	O
signed	O
with	O
A	O
&	O
M	O
Records	O
,	O
but	O
after	O
several	O
unproductive	O
sessions	O
he	O
canceled	O
his	O
intended	O
solo	O
debut	O
in	O
early	O
1971	O
.	O
Although	O
it	O
received	O
enthusiastic	O
reviews	O
,	O
the	O
release	O
failed	O
to	O
chart	O
;	O
his	O
next	O
album	O
,	O
Grievous	O
Angel	O
(	O
released	O
posthumously	O
in	O
1974	O
)	O
met	O
with	O
a	O
similar	O
reception	O
,	O
and	O
peaked	O
at	O
number	O
195	O
on	O
Billboard	O
.	O
Parsons	PERSON
died	O
of	O
a	O
drug	O
overdose	O
on	O
September	O
19	O
,	O
1973	O
in	O
a	O
hotel	O
room	O
in	O
Joshua	LOCATION
Tree	LOCATION
,	O
California	O
,	O
at	O
the	O
age	O
of	O
26	O
.	O
Since	O
his	O
death	O
,	O
Parsons	PERSON
has	O
been	O
recognized	O
as	O
an	O
extremely	O
influential	O
artist	O
,	O
credited	O
with	O
helping	O
to	O
found	O
both	O
country	O
rock	O
and	O
alt-country	O
.	O
She	O
was	O
the	O
daughter	O
of	O
citrus	O
fruit	O
magnate	O
John	PERSON
A.	PERSON
Snively	PERSON
,	O
who	O
held	O
extensive	O
properties	O
both	O
in	O
Winter	LOCATION
Haven	LOCATION
and	O
in	O
Waycross	LOCATION
;	O
Parsons	PERSON
'	O
father	O
was	O
a	O
famous	O
World	O
War	O
II	O
flying	O
ace	O
,	O
decorated	O
with	O
the	O
Air	O
Medal	O
,	O
who	O
was	O
present	O
at	O
the	O
1941	O
attack	O
on	O
Pearl	O
Harbor	O
.	O
As	O
his	O
family	O
disintegrated	O
around	O
him	O
,	O
Parsons	PERSON
developed	O
strong	O
musical	O
interests	O
,	O
particularly	O
after	O
seeing	O
Elvis	O
Presley	O
perform	O
in	O
concert	O
in	O
1957	O
.	O
They	O
relocated	O
to	O
Los	LOCATION
Angeles	LOCATION
the	O
following	O
year	O
,	O
and	O
after	O
several	O
lineup	O
changes	O
signed	O
to	O
Lee	PERSON
Hazlewood	PERSON
's	O
LHI	O
Records	O
,	O
where	O
they	O
spent	O
late	O
1967	O
recording	O
Safe	O
at	O
Home	O
.	O
Safe	O
at	O
Home	O
would	O
remain	O
unreleased	O
until	O
mid	O
1968	O
,	O
by	O
which	O
time	O
the	O
International	O
Submarine	O
Band	O
had	O
broken	O
up	O
.	O
Parsons	PERSON
had	O
been	O
acquainted	O
with	O
Hillman	PERSON
since	O
the	O
pair	O
had	O
met	O
in	O
a	O
bank	O
during	O
1967	O
and	O
in	O
February	O
1968	O
he	O
passed	O
an	O
audition	O
for	O
the	O
band	O
,	O
being	O
initially	O
recruited	O
as	O
a	O
jazz	O
pianist	O
but	O
soon	O
switching	O
to	O
rhythm	O
guitar	O
and	O
vocals	O
.	O
In	O
later	O
years	O
,	O
this	O
led	O
Hillman	PERSON
to	O
state	O
"	O
Gram	O
was	O
hired	O
.	O
However	O
,	O
Parsons	O
was	O
still	O
under	O
contract	O
to	O
LHI	O
Records	O
and	O
consequently	O
,	O
Hazelwood	O
contested	O
Parsons	O
'	O
appearance	O
on	O
the	O
album	O
and	O
threatened	O
legal	O
action	O
.	O
There	O
has	O
been	O
some	O
doubt	O
expressed	O
by	O
Hillman	O
over	O
the	O
sincerity	O
of	O
Parsons	O
'	O
protest	O
.	O
Immediately	O
after	O
leaving	O
the	O
band	O
,	O
Parsons	PERSON
stayed	O
at	O
Richards	O
'	O
house	O
and	O
the	O
pair	O
developed	O
a	O
close	O
friendship	O
over	O
the	O
next	O
few	O
years	O
,	O
with	O
Parsons	PERSON
reintroducing	O
the	O
guitarist	O
to	O
country	O
music	O
.	O
Returning	O
to	O
Los	LOCATION
Angeles	LOCATION
,	O
Parsons	PERSON
sought	O
out	O
Hillman	PERSON
(	O
both	O
as	O
rhythm	O
guitarists	O
)	O
,	O
and	O
the	O
two	O
formed	O
the	O
Flying	PERSON
Burrito	PERSON
Brothers	O
with	O
bassist	O
Chris	PERSON
Ethridge	PERSON
and	O
pedal	O
steel	O
player	O
Sneaky	PERSON
Pete	PERSON
Kleinow	PERSON
.	O
The	O
album	O
's	O
original	O
songs	O
were	O
the	O
result	O
of	O
a	O
very	O
productive	O
songwriting	O
partnership	O
between	O
Parsons	PERSON
and	O
Hillman	PERSON
,	O
who	O
were	O
sharing	O
a	O
bachelor	O
pad	O
in	O
LA	LOCATION
during	O
this	O
period	O
.	O
The	O
atypically	O
pronounced	O
(	O
for	O
Parsons	O
)	O
gospel	O
soul	O
influence	O
on	O
this	O
album	O
likely	O
comes	O
from	O
his	O
frequent	O
jamming	O
with	O
Delaney	O
and	O
Bonnie	O
and	O
Richards	O
.	O
The	O
album	O
was	O
recorded	O
without	O
a	O
permanent	O
drummer	O
,	O
but	O
the	O
group	O
soon	O
added	O
original	O
Byrd	PERSON
Michael	PERSON
Clarke	PERSON
on	O
drums	O
.	O
Parsons	O
was	O
frequently	O
indulging	O
in	O
massive	O
quantities	O
of	O
psilocybin	O
and	O
cocaine	O
,	O
so	O
his	O
performances	O
were	O
erratic	O
at	O
best	O
,	O
while	O
much	O
of	O
the	O
band	O
's	O
repertoire	O
consisted	O
of	O
vintage	O
honky	O
tonk	O
and	O
soul	O
standards	O
with	O
few	O
originals	O
.	O
He	O
was	O
replaced	O
by	O
lead	O
guitarist	O
Bernie	PERSON
Leadon	PERSON
,	O
while	O
Hillman	O
reverted	O
to	O
bass	O
.	O
The	O
resulting	O
album	O
,	O
entitled	O
Burrito	O
Deluxe	O
,	O
was	O
released	O
in	O
April	O
1970	O
.	O
Jagger	O
consented	O
to	O
the	O
cover	O
version	O
,	O
so	O
long	O
as	O
the	O
Flying	O
Burrito	O
Brothers	O
did	O
not	O
issue	O
it	O
as	O
a	O
single	O
.	O
Burrito	O
Deluxe	O
,	O
like	O
its	O
predecessor	O
,	O
underperformed	O
commercially	O
but	O
faced	O
the	O
double	O
whammy	O
of	O
being	O
lambasted	O
by	O
critics	O
.	O
Parsons	PERSON
signed	O
a	O
solo	O
deal	O
with	O
A	O
&	O
M	O
Records	O
and	O
moved	O
in	O
with	O
producer	O
Terry	PERSON
Melcher	PERSON
in	O
early	O
1970	O
.	O
The	O
two	O
shared	O
a	O
mutual	O
penchant	O
for	O
cocaine	O
and	O
heroin	O
,	O
and	O
as	O
a	O
result	O
,	O
the	O
sessions	O
were	O
largely	O
unproductive	O
,	O
with	O
Parsons	O
eventually	O
losing	O
interest	O
in	O
the	O
project	O
.	O
The	O
recording	O
stalled	O
,	O
and	O
the	O
master	O
tapes	O
were	O
checked	O
out	O
,	O
but	O
there	O
is	O
conflict	O
as	O
to	O
whether	O
"	O
Gram	O
...	O
or	O
Melcher	O
took	O
them	O
"	O
.	O
Eventually	O
,	O
Parsons	PERSON
was	O
asked	O
to	O
leave	O
by	O
Anita	PERSON
Pallenberg	PERSON
,	O
Richards	O
'	O
longtime	O
domestic	O
partner	O
.	O
Rumors	O
have	O
persisted	O
that	O
he	O
appears	O
somewhere	O
on	O
the	O
legendary	O
album	O
,	O
and	O
while	O
Richards	O
concedes	O
that	O
it	O
is	O
very	O
likely	O
he	O
is	O
among	O
the	O
chorus	O
of	O
singers	O
on	O
"	O
Sweet	O
Virginia	O
"	O
,	O
to	O
this	O
day	O
nothing	O
has	O
been	O
substantiated	O
.	O
Parsons	PERSON
attempted	O
to	O
rekindle	O
his	O
relationship	O
with	O
the	O
band	O
on	O
their	O
1972	O
tour	O
to	O
no	O
avail	O
.	O
Many	O
of	O
the	O
singer	O
's	O
closest	O
associates	O
and	O
friends	O
claim	O
that	O
Parsons	PERSON
was	O
preparing	O
to	O
commence	O
divorce	O
proceedings	O
at	O
the	O
time	O
of	O
his	O
death	O
;	O
the	O
couple	O
had	O
already	O
separated	O
by	O
this	O
point	O
.	O
With	O
the	O
assistance	O
of	O
Grech	O
and	O
one	O
of	O
the	O
bassist	O
's	O
friends	O
,	O
Hank	PERSON
Wangford	PERSON
a	O
doctor	O
friend	O
who	O
dabbled	O
in	O
country	O
music	O
,	O
Parsons	PERSON
managed	O
to	O
kick	O
his	O
heroin	O
habit	O
once	O
and	O
for	O
all	O
(	O
a	O
treatment	O
suggested	O
by	O
William	O
Burroughs	O
proved	O
unsuccessful	O
)	O
.	O
They	O
became	O
friends	O
and	O
,	O
within	O
a	O
year	O
,	O
he	O
asked	O
her	O
to	O
join	O
him	O
in	O
L.A.	O
for	O
another	O
attempt	O
to	O
record	O
his	O
first	O
solo	O
album	O
.	O
The	O
record	O
,	O
which	O
was	O
released	O
after	O
his	O
death	O
,	O
received	O
even	O
more	O
enthusiastic	O
reviews	O
than	O
had	O
GP	O
,	O
and	O
has	O
since	O
attained	O
classic	O
status	O
.	O
Despite	O
the	O
fact	O
that	O
Parsons	PERSON
only	O
contributed	O
two	O
new	O
songs	O
to	O
the	O
album	O
,	O
Parsons	PERSON
was	O
highly	O
enthused	O
with	O
his	O
new	O
sound	O
and	O
seemed	O
to	O
have	O
finally	O
adopted	O
a	O
serious	O
,	O
diligent	O
mindset	O
to	O
his	O
musical	O
career	O
,	O
eschewing	O
most	O
drugs	O
and	O
alcohol	O
during	O
the	O
sessions	O
.	O
The	O
backing	O
band	O
included	O
Clarence	PERSON
White	PERSON
,	O
Pete	PERSON
Kleinow	PERSON
,	O
and	O
Chris	PERSON
Etheridge	PERSON
.	O
Richman	O
credits	O
Parsons	PERSON
with	O
introducing	O
him	O
to	O
acoustic-based	O
music	O
[	O
citation	O
needed	O
]	O
.	O
In	O
the	O
late	O
1960s	O
,	O
Parsons	PERSON
became	O
enamored	O
of	O
Joshua	O
Tree	O
National	O
Monument	O
in	O
southeastern	O
California	O
.	O
Before	O
his	O
tour	O
was	O
scheduled	O
to	O
commence	O
in	O
October	O
1973	O
,	O
Parsons	PERSON
decided	O
to	O
go	O
on	O
one	O
more	O
excursion	O
.	O
Less	O
than	O
two	O
days	O
after	O
arriving	O
,	O
Parsons	PERSON
died	O
September	O
19	O
,	O
1973	O
in	O
Joshua	LOCATION
Tree	LOCATION
,	O
California	O
at	O
the	O
age	O
of	O
26	O
from	O
an	O
overdose	O
of	O
morphine	O
and	O
alcohol	O
.	O
Parsons	O
'	O
body	O
disappeared	O
from	O
the	O
Los	O
Angeles	O
International	O
Airport	O
where	O
it	O
was	O
being	O
readied	O
to	O
be	O
shipped	O
to	O
La.	LOCATION
for	O
burial	O
.	O
The	O
site	O
of	O
Parsons	O
'	O
cremation	O
was	O
marked	O
by	O
a	O
small	O
concrete	O
slab	O
and	O
is	O
presided	O
over	O
by	O
a	O
large	O
rock	O
flake	O
known	O
to	O
rock	O
climbers	O
as	O
'	O
The	O
Gram	O
Parsons	O
memorial	O
hand	O
traverse	O
'	O
.	O
Stephen	PERSON
Thomas	PERSON
Erlewine	PERSON
of	O
Allmusic	O
describes	O
Parsons	O
as	O
"	O
enormously	O
influential	O
"	O
for	O
both	O
country	O
and	O
rock	O
,	O
"	O
blending	O
the	O
two	O
genres	O
to	O
the	O
point	O
that	O
they	O
became	O
indistinguishable	O
from	O
each	O
other	O
.	O
Both	O
Leadon	PERSON
and	O
Parsons	PERSON
were	O
members	O
of	O
the	O
Flying	O
Burrito	O
Brothers	O
during	O
the	O
late	O
1960s	O
and	O
early	O
1970s	O
.	O
The	O
show	O
featured	O
tunes	O
written	O
by	O
Gram	PERSON
Parsons	PERSON
and	O
Gene	PERSON
Clark	PERSON
as	O
well	O
as	O
influential	O
songs	O
and	O
musical	O
styles	O
from	O
other	O
artists	O
that	O
were	O
part	O
of	O
that	O
era	O
.	O
In	O
February	O
2008	O
,	O
Gram	O
's	O
protégée	O
,	O
Emmylou	PERSON
Harris	PERSON
,	O
was	O
inducted	O
into	O
the	O
Country	O
Music	O
Hall	O
of	O
Fame	O
.	O
