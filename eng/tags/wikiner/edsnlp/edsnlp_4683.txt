Fourier	PERSON
analysis	O
is	O
named	O
after	O
Joseph	PERSON
Fourier	PERSON
,	O
who	O
showed	O
that	O
representing	O
a	O
function	O
by	O
a	O
trigonometric	O
series	O
greatly	O
simplifies	O
the	O
study	O
of	O
heat	O
propagation	O
.	O
Technically	O
,	O
Clairaut	O
's	O
work	O
was	O
a	O
cosine-only	O
series	O
(	O
a	O
form	O
of	O
discrete	O
cosine	O
transform	O
)	O
,	O
while	O
Lagrange	O
's	O
work	O
was	O
a	O
sine-only	O
series	O
(	O
a	O
form	O
of	O
discrete	O
sine	O
transform	O
)	O
;	O
a	O
true	O
cosine+sine	O
DFT	O
was	O
used	O
by	O
Gauss	O
in	O
1805	O
for	O
trigonometric	O
interpolation	O
of	O
asteroid	O
orbits	O
.	O
Euler	O
and	O
Lagrange	O
both	O
discretized	O
the	O
vibrating	O
string	O
problem	O
,	O
using	O
what	O
would	O
today	O
be	O
called	O
samples	O
.	O
