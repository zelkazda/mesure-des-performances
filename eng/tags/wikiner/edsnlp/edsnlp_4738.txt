Fleetwood	PERSON
Mac	O
are	O
a	O
British	O
--	O
American	O
rock	O
band	O
formed	O
in	O
1967	O
in	O
London	LOCATION
.	O
The	O
only	O
original	O
member	O
present	O
in	O
the	O
band	O
is	O
its	O
namesake	O
drummer	O
,	O
Mick	PERSON
Fleetwood	PERSON
.	O
Despite	O
band	O
founder	O
Peter	PERSON
Green	PERSON
naming	O
the	O
group	O
by	O
combining	O
the	O
surnames	O
of	O
two	O
of	O
his	O
former	O
bandmates	O
from	O
John	PERSON
Mayall	PERSON
's	O
Bluesbreakers	O
,	O
bassist	O
John	PERSON
McVie	PERSON
did	O
not	O
play	O
on	O
their	O
first	O
single	O
nor	O
at	O
their	O
first	O
concerts	O
.	O
The	O
keyboardist	O
,	O
Christine	PERSON
McVie	PERSON
,	O
has	O
,	O
to	O
date	O
,	O
appeared	O
on	O
all	O
but	O
two	O
albums	O
,	O
either	O
as	O
a	O
member	O
or	O
as	O
a	O
session	O
musician	O
.	O
She	O
also	O
supplied	O
the	O
artwork	O
for	O
the	O
album	O
Kiln	O
House	O
.	O
John	PERSON
Mayall	PERSON
agreed	O
and	O
Fleetwood	O
became	O
a	O
member	O
of	O
the	O
band	O
.	O
Within	O
weeks	O
of	O
this	O
show	O
,	O
John	PERSON
McVie	PERSON
agreed	O
to	O
become	O
the	O
bassist	O
for	O
the	O
band	O
.	O
Fleetwood	O
Mac	O
's	O
first	O
album	O
,	O
Fleetwood	O
Mac	O
,	O
was	O
a	O
no-frills	O
blues	O
album	O
and	O
was	O
released	O
on	O
the	O
Blue	O
Horizon	O
label	O
in	O
February	O
1968	O
.	O
The	O
album	O
was	O
successful	O
in	O
the	O
UK	O
,	O
hitting	O
no.4	O
,	O
though	O
it	O
did	O
not	O
have	O
any	O
singles	O
on	O
it	O
.	O
The	O
band	O
soon	O
released	O
two	O
singles	O
"	O
Black	O
Magic	O
Woman	O
"	O
(	O
later	O
a	O
big	O
hit	O
for	O
Santana	O
)	O
and	O
"	O
Need	O
Your	O
Love	O
So	O
Bad	O
"	O
.	O
They	O
also	O
added	O
horns	O
and	O
featured	O
a	O
friend	O
of	O
the	O
band	O
on	O
keyboards	O
,	O
Christine	PERSON
Perfect	PERSON
of	O
Chicken	O
Shack	O
.	O
Shortly	O
after	O
the	O
release	O
of	O
their	O
second	O
album	O
Fleetwood	O
Mac	O
added	O
guitarist	O
Danny	PERSON
Kirwan	PERSON
,	O
then	O
just	O
eighteen	O
years	O
old	O
,	O
to	O
their	O
line-up	O
.	O
A	O
mature	O
and	O
accomplished	O
self-taught	O
guitarist	O
,	O
Kirwan	PERSON
's	O
signature	O
vibrato	O
and	O
unique	O
style	O
added	O
a	O
new	O
dimension	O
to	O
an	O
already	O
complete	O
band	O
.	O
These	O
would	O
prove	O
,	O
however	O
,	O
to	O
be	O
Fleetwood	O
Mac	O
's	O
last	O
all-blues	O
recordings	O
.	O
Up	O
until	O
this	O
point	O
they	O
had	O
been	O
on	O
Blue	O
Horizon	O
.	O
With	O
Kirwan	PERSON
in	O
the	O
band	O
,	O
however	O
,	O
the	O
musical	O
possibilities	O
were	O
too	O
great	O
for	O
them	O
to	O
stay	O
on	O
a	O
blues-only	O
label	O
.	O
Immediate	O
was	O
in	O
bad	O
shape	O
and	O
the	O
band	O
shopped	O
around	O
for	O
a	O
new	O
deal	O
.	O
Even	O
though	O
The	O
Beatles	O
wanted	O
the	O
band	O
on	O
Apple	O
Records	O
(	O
Mick	PERSON
Fleetwood	PERSON
and	O
George	O
Harrison	O
were	O
brothers-in-law	O
)	O
,	O
the	O
band	O
's	O
manager	O
Clifford	PERSON
Davis	PERSON
decided	O
to	O
go	O
with	O
Warner	O
Bros.	O
Records	O
(	O
Reprise	O
)	O
,	O
the	O
label	O
they	O
have	O
stayed	O
with	O
ever	O
since	O
.	O
Their	O
first	O
album	O
for	O
Reprise	O
,	O
released	O
in	O
September	O
1969	O
,	O
was	O
the	O
well-regarded	O
Then	O
Play	O
On	O
.	O
In	O
July	O
1969	O
Fleetwood	O
Mac	O
opened	O
for	O
Ten	O
Years	O
After	O
at	O
the	O
Schaefer	O
Music	O
Festival	O
at	O
New	LOCATION
York	LOCATION
City	LOCATION
's	O
Wollman	O
Rink	O
.	O
However	O
,	O
Peter	PERSON
Green	PERSON
,	O
the	O
frontman	O
of	O
the	O
band	O
,	O
was	O
not	O
in	O
good	O
health	O
.	O
He	O
had	O
taken	O
LSD	O
in	O
Munich	O
,	O
which	O
contributed	O
to	O
the	O
onset	O
of	O
his	O
schizophrenia	O
.	O
They	O
were	O
not	O
really	O
interested	O
in	O
Peter	PERSON
Green	PERSON
.	O
His	O
last	O
show	O
with	O
Fleetwood	O
Mac	O
was	O
on	O
20	O
May	O
1970	O
.	O
Mick	PERSON
Fleetwood	PERSON
kept	O
drumming	O
.	O
In	O
September	O
1970	O
,	O
Fleetwood	O
Mac	O
released	O
Kiln	O
House	O
.	O
Kirwan	O
's	O
songs	O
moved	O
the	O
band	O
in	O
the	O
direction	O
of	O
70s	O
rock	O
.	O
Christine	PERSON
Perfect	PERSON
,	O
who	O
had	O
retired	O
from	O
the	O
music	O
business	O
after	O
one	O
unsuccessful	O
solo	O
album	O
,	O
contributed	O
to	O
Kiln	O
House	O
,	O
singing	O
backup	O
vocals	O
,	O
and	O
drawing	O
the	O
album	O
cover	O
.	O
Christine	PERSON
McVie	PERSON
played	O
her	O
first	O
gig	O
as	O
an	O
official	O
member	O
on	O
6	O
August	O
1970	O
in	O
New	LOCATION
Orleans	LOCATION
.	O
Columbia	O
Records	O
,	O
which	O
now	O
owned	O
Blue	O
Horizon	O
,	O
released	O
an	O
album	O
of	O
previously	O
unreleased	O
material	O
from	O
the	O
original	O
Fleetwood	PERSON
Mac	PERSON
called	O
The	O
Original	O
Fleetwood	O
Mac	O
.	O
The	O
band	O
had	O
a	O
few	O
meetings	O
with	O
Welch	PERSON
and	O
decided	O
to	O
hire	O
him	O
,	O
without	O
actually	O
playing	O
with	O
him	O
or	O
listening	O
to	O
any	O
of	O
his	O
recordings	O
.	O
In	O
September	O
1971	O
,	O
the	O
band	O
released	O
Future	O
Games	O
.	O
There	O
were	O
many	O
new	O
fans	O
in	O
America	O
who	O
were	O
becoming	O
more	O
and	O
more	O
interested	O
in	O
the	O
band	O
.	O
In	O
1972	O
,	O
six	O
months	O
after	O
the	O
release	O
of	O
Future	O
Games	O
,	O
the	O
band	O
released	O
the	O
well-received	O
album	O
Bare	O
Trees	O
.	O
In	O
September	O
1972	O
,	O
the	O
band	O
added	O
guitarist	O
Bob	PERSON
Weston	PERSON
and	O
vocalist	O
Dave	PERSON
Walker	PERSON
,	O
formerly	O
of	O
Savoy	PERSON
Brown	PERSON
.	O
Bob	PERSON
Weston	PERSON
was	O
well	O
known	O
for	O
playing	O
slide	O
guitar	O
and	O
had	O
known	O
the	O
band	O
from	O
his	O
touring	O
period	O
with	O
Long	O
John	O
Baldry	PERSON
.	O
The	O
remaining	O
five	O
carried	O
on	O
and	O
recorded	O
Mystery	O
to	O
Me	O
six	O
months	O
later	O
.	O
During	O
the	O
tour	O
,	O
Weston	PERSON
had	O
an	O
affair	O
with	O
Fleetwood	PERSON
's	O
wife	O
,	O
Jenny	PERSON
Boyd	PERSON
Fleetwood	PERSON
,	O
the	O
sister	O
of	O
Pattie	PERSON
Boyd	PERSON
Harrison	PERSON
.	O
Fleetwood	O
soon	O
fired	O
Weston	PERSON
and	O
the	O
tour	O
was	O
cancelled	O
.	O
In	O
what	O
would	O
be	O
one	O
of	O
the	O
most	O
bizarre	O
events	O
in	O
rock	O
history	O
,	O
the	O
band	O
's	O
manager	O
,	O
Clifford	PERSON
Davis	PERSON
,	O
claimed	O
that	O
he	O
owned	O
the	O
name	O
Fleetwood	PERSON
Mac	PERSON
and	O
put	O
out	O
a	O
"	O
fake	O
Mac	O
"	O
.	O
Nobody	O
in	O
the	O
"	O
fake	O
Mac	O
"	O
was	O
ever	O
officially	O
in	O
the	O
real	O
band	O
,	O
although	O
some	O
of	O
them	O
later	O
acted	O
as	O
Danny	PERSON
Kirwan	PERSON
's	O
studio	O
band	O
.	O
But	O
the	O
lawsuit	O
that	O
followed	O
put	O
the	O
real	O
Fleetwood	PERSON
Mac	O
out	O
of	O
commission	O
for	O
almost	O
a	O
year	O
.	O
The	O
issue	O
was	O
who	O
actually	O
owned	O
the	O
name	O
"	O
Fleetwood	PERSON
Mac	O
"	O
.	O
While	O
this	O
did	O
not	O
end	O
the	O
legal	O
battle	O
,	O
the	O
band	O
was	O
able	O
to	O
record	O
as	O
Fleetwood	O
Mac	PERSON
again	O
.	O
Instead	O
of	O
getting	O
another	O
manager	O
,	O
Fleetwood	O
Mac	PERSON
decided	O
to	O
manage	O
themselves	O
.	O
After	O
Warner	O
Bros.	O
made	O
a	O
record	O
deal	O
with	O
the	O
real	O
Fleetwood	O
Mac	O
,	O
the	O
quartet	O
released	O
Heroes	O
Are	O
Hard	O
to	O
Find	O
in	O
September	O
1974	O
.	O
This	O
tour	O
proved	O
to	O
be	O
the	O
last	O
one	O
for	O
Bob	PERSON
Welch	PERSON
.	O
While	O
his	O
tenure	O
was	O
n't	O
a	O
commercial	O
success	O
,	O
Bob	PERSON
Welch	PERSON
provided	O
musical	O
and	O
professional	O
direction	O
to	O
the	O
group	O
,	O
helped	O
the	O
band	O
through	O
three	O
major	O
crises	O
,	O
and	O
left	O
it	O
in	O
a	O
situation	O
where	O
it	O
had	O
a	O
record	O
contract	O
,	O
a	O
direct	O
line	O
to	O
the	O
record	O
company	O
,	O
connections	O
to	O
industry	O
insiders	O
,	O
no	O
pressure	O
from	O
the	O
record	O
company	O
,	O
and	O
a	O
management	O
situation	O
that	O
would	O
help	O
foster	O
creativity	O
.	O
Thus	O
,	O
many	O
feel	O
that	O
Bob	PERSON
Welch	PERSON
had	O
laid	O
the	O
foundations	O
for	O
Fleetwood	PERSON
Mac	PERSON
's	O
future	O
.	O
After	O
Welch	O
announced	O
that	O
he	O
was	O
leaving	O
the	O
band	O
,	O
Fleetwood	PERSON
began	O
searching	O
for	O
a	O
possible	O
replacement	O
.	O
Fleetwood	PERSON
soon	O
asked	O
him	O
to	O
join	O
.	O
Buckingham	PERSON
agreed	O
,	O
on	O
the	O
condition	O
that	O
his	O
musical	O
partner	O
and	O
girlfriend	O
,	O
Stephanie	PERSON
"	O
Stevie	O
"	O
Nicks	PERSON
,	O
also	O
become	O
part	O
of	O
the	O
band	O
;	O
Fleetwood	O
agreed	O
to	O
this	O
.	O
In	O
1975	O
,	O
the	O
new	O
line-up	O
released	O
the	O
eponymous	O
Fleetwood	O
Mac	O
.	O
The	O
album	O
proved	O
to	O
be	O
a	O
breakthrough	O
for	O
the	O
band	O
and	O
became	O
a	O
huge	O
hit	O
,	O
reaching	O
#	O
1	O
in	O
the	O
US	O
and	O
selling	O
over	O
5	O
million	O
copies	O
.	O
The	O
pressure	O
put	O
on	O
Fleetwood	O
Mac	O
to	O
release	O
a	O
successful	O
follow-up	O
album	O
,	O
combined	O
with	O
their	O
new-found	O
wealth	O
,	O
led	O
to	O
creative	O
and	O
personal	O
tensions	O
,	O
fuelled	O
by	O
high	O
consumption	O
of	O
drugs	O
and	O
alcohol	O
.	O
The	O
album	O
the	O
band	O
members	O
released	O
in	O
1977	O
was	O
Rumours	O
,	O
in	O
which	O
they	O
laid	O
bare	O
the	O
emotional	O
turmoil	O
experienced	O
at	O
that	O
time	O
.	O
Buckingham	O
's	O
"	O
Second	O
Hand	O
News	O
"	O
,	O
Nicks	O
'	O
"	O
Gold	O
Dust	O
Woman	O
"	O
and	O
"	O
The	O
Chain	O
"	O
(	O
the	O
only	O
song	O
written	O
by	O
all	O
five	O
bandmates	O
)	O
also	O
received	O
significant	O
radio	O
airplay	O
.	O
By	O
2003	O
,	O
Rumours	O
had	O
sold	O
over	O
19	O
million	O
copies	O
in	O
the	O
U.S.	O
alone	O
(	O
certified	O
as	O
a	O
diamond	O
album	O
by	O
the	O
RIAA	O
)	O
,	O
and	O
a	O
total	O
of	O
40	O
million	O
copies	O
worldwide	O
,	O
maintaining	O
its	O
status	O
as	O
one	O
of	O
the	O
biggest-selling	O
albums	O
of	O
all	O
time	O
.	O
Buckingham	O
was	O
able	O
to	O
convince	O
Fleetwood	PERSON
to	O
allow	O
his	O
work	O
on	O
their	O
next	O
album	O
to	O
be	O
more	O
experimental	O
and	O
to	O
work	O
on	O
tracks	O
at	O
home	O
,	O
then	O
bring	O
them	O
to	O
the	O
band	O
in	O
the	O
studio	O
.	O
The	O
result	O
of	O
this	O
was	O
the	O
quirky	O
20-track	O
double	O
album	O
,	O
Tusk	O
,	O
released	O
in	O
1979	O
.	O
Tusk	O
remains	O
one	O
of	O
Fleetwood	O
Mac	O
's	O
most	O
ambitious	O
albums	O
to	O
date	O
,	O
although	O
selling	O
only	O
four	O
million	O
copies	O
worldwide	O
.	O
This	O
,	O
in	O
comparison	O
to	O
the	O
huge	O
sales	O
of	O
Rumours	O
,	O
inclined	O
the	O
label	O
to	O
deem	O
the	O
project	O
a	O
failure	O
,	O
laying	O
the	O
blame	O
squarely	O
with	O
Buckingham	O
himself	O
.	O
[	O
citation	O
needed	O
]	O
Fleetwood	O
,	O
however	O
,	O
blames	O
the	O
album	O
's	O
relative	O
failure	O
on	O
the	O
RKO	O
radio	O
chain	O
playing	O
the	O
album	O
in	O
its	O
entirety	O
prior	O
to	O
release	O
,	O
thus	O
allowing	O
mass	O
home	O
taping	O
.	O
The	O
band	O
embarked	O
on	O
a	O
huge	O
18-month	O
tour	O
to	O
support	O
and	O
promote	O
Tusk	O
.	O
They	O
travelled	O
extensively	O
across	O
the	O
world	O
,	O
including	O
the	O
USA	O
,	O
Australia	O
,	O
New	O
Zealand	O
,	O
Japan	O
,	O
France	O
,	O
Belgium	O
,	O
Germany	O
,	O
the	O
Netherlands	O
,	O
and	O
the	O
U.K.	O
.	O
In	O
Germany	O
they	O
shared	O
the	O
bill	O
with	O
reggae	O
superstar	O
Bob	PERSON
Marley	PERSON
.	O
It	O
was	O
on	O
this	O
world	O
tour	O
that	O
the	O
band	O
recorded	O
music	O
for	O
the	O
Fleetwood	O
Mac	O
Live	O
album	O
,	O
which	O
was	O
released	O
at	O
the	O
end	O
of	O
1980	O
.	O
The	O
next	O
album	O
,	O
1982	O
's	O
Mirage	O
,	O
following	O
1981	O
solo	O
turns	O
by	O
Nicks	O
(	O
Bella	O
Donna	O
)	O
and	O
Buckingham	O
(	O
Law	O
and	O
Order	O
)	O
,	O
was	O
a	O
return	O
to	O
the	O
more	O
conventional	O
.	O
Buckingham	O
had	O
been	O
chided	O
by	O
critics	O
,	O
fellow	O
bandmembers	O
and	O
music	O
business	O
managers	O
for	O
the	O
lesser	O
commercial	O
success	O
enjoyed	O
by	O
Tusk	O
.	O
Recorded	O
at	O
a	O
château	O
in	O
France	O
,	O
Mirage	O
was	O
an	O
attempt	O
to	O
recapture	O
the	O
huge	O
success	O
of	O
Rumours	PERSON
.	O
Mirage	O
was	O
certified	O
double	O
platinum	O
in	O
the	O
U.S	O
.	O
Following	O
Mirage	O
,	O
the	O
band	O
went	O
on	O
hiatus	O
,	O
which	O
allowed	O
members	O
to	O
pursue	O
solo	O
careers	O
.	O
All	O
three	O
met	O
with	O
success	O
but	O
it	O
was	O
Nicks	O
who	O
became	O
the	O
most	O
popular	O
.	O
However	O
,	O
also	O
during	O
this	O
period	O
,	O
Mick	PERSON
Fleetwood	PERSON
had	O
filed	O
for	O
bankruptcy	O
,	O
Nicks	O
was	O
admitted	O
to	O
the	O
Betty	ORGANIZATION
Ford	ORGANIZATION
Clinic	ORGANIZATION
for	O
addiction	O
problems	O
,	O
and	O
John	PERSON
McVie	PERSON
had	O
suffered	O
an	O
addiction-related	O
seizure	O
--	O
all	O
attributed	O
to	O
the	O
lifestyle	O
of	O
excess	O
afforded	O
to	O
them	O
by	O
their	O
worldwide	O
success	O
.	O
It	O
was	O
rumoured	O
that	O
Fleetwood	O
Mac	O
had	O
finally	O
broken	O
up	O
;	O
however	O
,	O
Buckingham	O
commented	O
that	O
he	O
was	O
unhappy	O
to	O
allow	O
Mirage	O
to	O
remain	O
as	O
the	O
band	O
's	O
last	O
effort	O
.	O
The	O
Rumours	O
line-up	O
of	O
Fleetwood	O
Mac	O
recorded	O
one	O
more	O
album	O
for	O
the	O
time	O
being	O
,	O
Tango	O
in	O
the	O
Night	O
,	O
in	O
1987	O
.	O
Initially	O
,	O
as	O
with	O
various	O
other	O
Fleetwood	O
Mac	O
albums	O
,	O
the	O
material	O
started	O
off	O
as	O
a	O
Buckingham	O
solo	O
album	O
before	O
becoming	O
a	O
group	O
project	O
.	O
The	O
album	O
went	O
on	O
to	O
become	O
their	O
best-selling	O
release	O
since	O
Rumours	O
,	O
especially	O
in	O
the	O
UK	O
where	O
it	O
hit	O
no.	O
1	O
three	O
times	O
over	O
the	O
following	O
year	O
.	O
"	O
Family	O
Man	O
"	O
and	O
"	O
Is	O
n't	O
It	O
Midnight	O
"	O
were	O
also	O
released	O
as	O
singles	O
,	O
with	O
lesser	O
success	O
.	O
The	O
band	O
intended	O
to	O
tour	O
as	O
usual	O
to	O
support	O
the	O
album	O
but	O
Buckingham	O
refused	O
.	O
According	O
to	O
Fleetwood	O
,	O
Buckingham	O
withdrew	O
from	O
Fleetwood	O
Mac	O
following	O
a	O
heated	O
,	O
angry	O
exchange	O
in	O
August	O
1987	O
.	O
Following	O
Buckingham	O
's	O
departure	O
,	O
Fleetwood	O
Mac	PERSON
added	O
two	O
new	O
guitarists	O
to	O
the	O
band	O
,	O
Billy	PERSON
Burnette	PERSON
and	O
Rick	PERSON
Vito	PERSON
.	O
Burnette	O
is	O
the	O
son	O
of	O
Dorsey	PERSON
Burnette	PERSON
and	O
nephew	O
of	O
Johnny	PERSON
Burnette	PERSON
,	O
both	O
of	O
The	O
Rock	O
and	O
Roll	O
Trio	O
.	O
The	O
Greatest	O
Hits	O
album	O
,	O
which	O
peaked	O
at	O
#	O
3	O
in	O
the	O
UK	O
and	O
#	O
14	O
in	O
the	O
US	O
(	O
though	O
has	O
since	O
sold	O
over	O
8	O
million	O
copies	O
there	O
)	O
,	O
was	O
dedicated	O
to	O
Buckingham	O
by	O
the	O
band	O
,	O
with	O
whom	O
they	O
had	O
now	O
reconciled	O
.	O
Following	O
the	O
Greatest	O
Hits	O
collection	O
,	O
Fleetwood	O
Mac	O
recorded	O
Behind	O
the	O
Mask	O
.	O
With	O
this	O
album	O
,	O
the	O
band	O
veered	O
away	O
from	O
the	O
stylised	O
sound	O
that	O
Buckingham	O
had	O
evolved	O
during	O
his	O
tenure	O
in	O
the	O
band	O
(	O
also	O
evident	O
in	O
his	O
solo	O
works	O
)	O
,	O
and	O
ended	O
up	O
with	O
a	O
more	O
adult	O
contemporary	O
style	O
from	O
producer	O
Greg	PERSON
Ladanyi	PERSON
.	O
Behind	O
The	O
Mask	O
only	O
achieved	O
gold	O
album	O
status	O
in	O
the	O
US	O
,	O
peaking	O
at	O
#	O
18	O
on	O
the	O
Billboard	O
album	O
chart	O
,	O
though	O
it	O
entered	O
the	O
UK	O
album	O
chart	O
at	O
#	O
1	O
.	O
It	O
received	O
mixed	O
reviews	O
,	O
and	O
was	O
seen	O
by	O
some	O
music	O
critics	O
as	O
a	O
low	O
point	O
for	O
the	O
band	O
in	O
the	O
absence	O
of	O
Lindsey	PERSON
Buckingham	PERSON
(	O
who	O
had	O
actually	O
made	O
a	O
guest	O
appearance	O
by	O
playing	O
on	O
the	O
title	O
track	O
)	O
.	O
However	O
,	O
in	O
1991	O
,	O
both	O
Nicks	PERSON
and	O
Rick	PERSON
Vito	PERSON
announced	O
they	O
were	O
leaving	O
Fleetwood	O
Mac	O
altogether	O
.	O
In	O
1992	O
,	O
Fleetwood	O
himself	O
arranged	O
a	O
4-disc	O
box	O
set	O
spanning	O
highlights	O
from	O
the	O
band	O
's	O
25	O
year	O
history	O
,	O
titled	O
25	O
Years	O
--	O
The	O
Chain	O
(	O
an	O
edited	O
2-disc	O
set	O
was	O
also	O
available	O
)	O
.	O
As	O
both	O
members	O
had	O
left	O
the	O
band	O
by	O
this	O
point	O
,	O
the	O
track	O
was	O
presumably	O
a	O
leftover	O
from	O
the	O
Behind	O
The	O
Mask	O
sessions	O
.	O
The	O
volume	O
featured	O
many	O
rare	O
photographs	O
and	O
notes	O
(	O
written	O
by	O
Fleetwood	O
himself	O
)	O
detailing	O
the	O
band	O
's	O
25	O
year	O
history	O
.	O
Clinton	O
had	O
made	O
Fleetwood	PERSON
Mac	PERSON
's	O
"	O
Do	O
n't	O
Stop	O
"	O
his	O
campaign	O
theme	O
song	O
.	O
However	O
,	O
just	O
as	O
they	O
made	O
the	O
decision	O
to	O
continue	O
,	O
Billy	PERSON
Burnette	PERSON
announced	O
in	O
March	O
1993	O
,	O
that	O
he	O
was	O
leaving	O
the	O
band	O
to	O
pursue	O
a	O
country	O
album	O
and	O
an	O
acting	O
career	O
.	O
Bekka	PERSON
Bramlett	PERSON
,	O
who	O
had	O
worked	O
a	O
year	O
earlier	O
with	O
Mick	PERSON
Fleetwood	PERSON
's	O
Zoo	O
,	O
was	O
recruited	O
.	O
By	O
March	O
1994	O
,	O
Billy	PERSON
Burnette	PERSON
,	O
himself	O
a	O
good	O
friend	O
and	O
co-songwriter	O
with	O
Delaney	PERSON
Bramlett	PERSON
,	O
returned	O
with	O
Fleetwood	O
's	O
blessing	O
.	O
The	O
tour	O
saw	O
the	O
band	O
perform	O
classic	O
Fleetwood	O
Mac	O
songs	O
from	O
the	O
initial	O
1967	O
--	O
1974	O
era	O
.	O
On	O
10	O
October	O
1995	O
,	O
Fleetwood	O
Mac	O
released	O
the	O
unsuccessful	O
Time	O
album	O
.	O
Bramlett	PERSON
and	O
Burnette	PERSON
subsequently	O
formed	O
a	O
country	O
music	O
duo	O
,	O
Bekka	O
&	O
Billy	O
.	O
Just	O
weeks	O
after	O
disbanding	O
Fleetwood	PERSON
Mac	PERSON
,	O
Mick	PERSON
Fleetwood	PERSON
announced	O
that	O
he	O
was	O
working	O
with	O
Lindsey	PERSON
Buckingham	PERSON
again	O
.	O
Stevie	PERSON
Nicks	PERSON
also	O
enlisted	O
Lindsey	PERSON
Buckingham	PERSON
to	O
produce	O
a	O
song	O
for	O
a	O
soundtrack	O
.	O
This	O
eventually	O
led	O
to	O
a	O
full	O
Rumours	O
line-up	O
reunion	O
when	O
the	O
band	O
officially	O
reformed	O
in	O
March	O
1997	O
.	O
The	O
result	O
came	O
in	O
the	O
form	O
of	O
a	O
live	O
concert	O
recorded	O
on	O
a	O
Warner	O
Bros.	O
Burbank	O
,	O
California	O
soundstage	O
on	O
22	O
May	O
,	O
which	O
resulted	O
in	O
the	O
1997	O
live	O
album	O
The	O
Dance	O
,	O
returning	O
Fleetwood	O
Mac	PERSON
to	O
the	O
top	O
of	O
the	O
US	O
album	O
charts	O
for	O
the	O
first	O
time	O
in	O
15	O
years	O
.	O
The	O
album	O
returned	O
Fleetwood	O
Mac	PERSON
to	O
their	O
superstar	O
commercial	O
status	O
that	O
they	O
had	O
not	O
enjoyed	O
since	O
their	O
Tango	O
in	O
the	O
Night	O
album	O
.	O
The	O
album	O
was	O
certified	O
a	O
5	O
million	O
seller	O
by	O
the	O
RIAA	O
.	O
A	O
successful	O
arena	O
tour	O
followed	O
the	O
MTV	O
premiere	O
of	O
The	O
Dance	O
,	O
which	O
kept	O
the	O
reunited	O
Mac	O
on	O
the	O
road	O
throughout	O
much	O
of	O
1997	O
,	O
the	O
20th	O
anniversary	O
of	O
their	O
Rumours	O
album	O
.	O
In	O
interviews	O
given	O
in	O
November	O
2006	O
to	O
support	O
his	O
solo	O
album	O
Under	O
the	O
Skin	O
,	O
Buckingham	PERSON
stated	O
that	O
plans	O
for	O
the	O
band	O
to	O
reunite	O
once	O
more	O
for	O
a	O
2008	O
tour	O
were	O
still	O
in	O
the	O
cards	O
.	O
However	O
in	O
a	O
more	O
recent	O
interview	O
,	O
Mick	PERSON
Fleetwood	PERSON
said	O
"	O
...	O
be	O
very	O
happy	O
and	O
hopeful	O
that	O
we	O
will	O
be	O
working	O
again	O
.	O
I	O
can	O
tell	O
you	O
everyone	O
's	O
going	O
to	O
be	O
extremely	O
excited	O
about	O
what	O
's	O
happening	O
with	O
Fleetwood	O
Mac	O
.	O
"	O
Crow	O
and	O
Stevie	PERSON
Nicks	O
collaborated	O
a	O
great	O
deal	O
in	O
the	O
past	O
and	O
she	O
has	O
stated	O
that	O
Nicks	O
has	O
been	O
a	O
great	O
teacher	O
and	O
inspiration	O
for	O
her	O
.	O
In	O
a	O
subsequent	O
interview	O
with	O
Buckingham	O
,	O
he	O
said	O
after	O
discussions	O
between	O
the	O
band	O
and	O
Crow	O
,	O
the	O
potential	O
collaboration	O
with	O
Crow	O
"	O
lost	O
its	O
momentum	O
"	O
.	O
On	O
June	O
9	O
,	O
2008	O
,	O
The	O
New	O
York	O
Times	O
reported	O
that	O
Irving	PERSON
Azoff	PERSON
was	O
in	O
the	O
process	O
of	O
negotiating	O
a	O
deal	O
with	O
Wal-Mart	O
for	O
Fleetwood	O
Mac	O
's	O
new	O
album	O
.	O
According	O
to	O
Stevie	PERSON
Nicks	PERSON
,	O
"	O
the	O
group	O
will	O
start	O
working	O
on	O
material	O
and	O
recording	O
probably	O
in	O
October	O
,	O
and	O
finish	O
an	O
album	O
.	O
"	O
In	O
late	O
2008	O
,	O
Fleetwood	O
Mac	O
announced	O
that	O
the	O
band	O
would	O
tour	O
in	O
2009	O
,	O
beginning	O
in	O
March	O
.	O
According	O
to	O
Billboard	O
,	O
Mick	PERSON
Fleetwood	PERSON
said	O
during	O
a	O
teleconference	O
with	O
reporters	O
on	O
12	O
February	O
2009	O
,	O
"	O
This	O
is	O
the	O
first	O
time	O
we	O
've	O
gone	O
on	O
the	O
road	O
without	O
an	O
album	O
.	O
This	O
is	O
truly	O
a	O
new	O
experience	O
for	O
Fleetwood	O
Mac	PERSON
to	O
go	O
out	O
and	O
play	O
songs	O
that	O
we	O
believe	O
and	O
hope	O
people	O
are	O
going	O
to	O
be	O
familiar	O
with	O
and	O
love	O
.	O
"	O
Stevie	PERSON
Nicks	PERSON
stated	O
that	O
,	O
with	O
regard	O
to	O
a	O
new	O
Fleetwood	PERSON
Mac	PERSON
album	O
,	O
"	O
There	O
is	O
n't	O
any	O
plan	O
at	O
this	O
point	O
...	O
for	O
any	O
album	O
.	O
During	O
the	O
concerts	O
mentioned	O
,	O
Buckingham	PERSON
stated	O
,	O
"	O
the	O
time	O
is	O
right	O
to	O
go	O
back	O
to	O
the	O
studio	O
--	O
but	O
only	O
after	O
a	O
tour	O
.	O
During	O
their	O
show	O
on	O
June	O
20	O
,	O
2009	O
in	O
New	LOCATION
Orleans	LOCATION
,	O
Louisiana	O
,	O
Stevie	PERSON
Nicks	PERSON
premiered	O
part	O
of	O
a	O
new	O
song	O
that	O
she	O
had	O
written	O
about	O
Hurricane	O
Katrina	O
.	O
This	O
was	O
a	O
surprise	O
to	O
the	O
audience	O
,	O
as	O
Fleetwood	O
Mac	PERSON
has	O
not	O
been	O
known	O
for	O
playing	O
unreleased	O
songs	O
during	O
their	O
more	O
recent	O
concerts	O
.	O
Also	O
in	O
October	O
The	O
Very	O
Best	O
of	O
Fleetwood	O
Mac	O
was	O
re-released	O
in	O
an	O
extended	O
two	O
disc	O
format	O
(	O
this	O
format	O
having	O
been	O
released	O
in	O
the	O
US	O
in	O
2002	O
)	O
,	O
premiering	O
at	O
number	O
six	O
in	O
the	O
UK	O
album	O
charts	O
.	O
On	O
November	O
1	O
,	O
2009	O
,	O
a	O
new	O
one	O
hour	O
documentary	O
,	O
Fleetwood	O
Mac	O
:	O
Do	O
n't	O
Stop	O
,	O
was	O
broadcast	O
in	O
the	O
UK	O
on	O
BBC	O
One	O
.	O
