Alfonso	PERSON
Arau	PERSON
(	O
born	O
January	O
11	O
,	O
1932	O
)	O
is	O
a	O
Mexican	O
actor	O
and	O
director	O
.	O
He	O
directed	O
the	O
films	O
Zapata	O
:	O
The	O
Dream	O
of	O
a	O
Hero	O
,	O
Like	O
Water	O
for	O
Chocolate	O
,	O
A	O
Walk	O
in	O
the	O
Clouds	O
with	O
Keanu	PERSON
Reeves	PERSON
and	O
Anthony	PERSON
Quinn	PERSON
,	O
and	O
the	O
Hallmark	O
Hall	O
of	O
Fame	O
production	O
A	O
Painted	O
House	O
,	O
adapted	O
from	O
the	O
John	O
Grisham	PERSON
novel	O
of	O
the	O
same	O
name	O
.	O
The	O
movie	O
is	O
set	O
in	O
the	O
1920s	O
in	O
rural	O
Mexico	LOCATION
.	O
