Designed	O
with	O
the	O
same	O
form-factor	O
as	O
the	O
previous	O
generation	O
of	O
semi-active	O
guided	O
Sparrow	O
missiles	O
,	O
it	O
is	O
a	O
fire-and-forget	O
missile	O
with	O
active	O
guidance	O
.	O
It	O
is	O
also	O
commonly	O
known	O
as	O
the	O
Slammer	O
in	O
USAF	O
service	O
.	O
The	O
AIM-7	O
Sparrow	O
medium	O
range	O
missile	O
was	O
developed	O
by	O
the	O
US	O
Navy	O
in	O
the	O
1950s	O
as	O
its	O
first	O
operational	O
BVR	O
air-to-air	O
weapon	O
.	O
The	O
early	O
beam	O
riding	O
versions	O
of	O
the	O
Sparrow	O
missiles	O
were	O
integrated	O
onto	O
the	O
F3H	O
Demon	O
and	O
F7U	O
Cutlass	O
,	O
but	O
the	O
definitive	O
AIM-7	O
Sparrow	O
was	O
the	O
primary	O
weapon	O
for	O
the	O
all-weather	O
F-4	O
Phantom	O
II	O
fighter/interceptor	O
,	O
which	O
lacked	O
an	O
internal	O
gun	O
in	O
the	O
US	O
Navy	O
,	O
USMC	O
and	O
early	O
USAF	O
versions	O
.	O
Together	O
with	O
the	O
short	O
range	O
infrared	O
guided	O
AIM-9	O
Sidewinder	O
,	O
they	O
replaced	O
the	O
AIM-4	O
Falcon	O
IR	O
and	O
radar	O
guided	O
series	O
for	O
use	O
in	O
air	O
combat	O
by	O
the	O
USAF	O
as	O
well	O
.	O
The	O
US	O
Navy	O
later	O
developed	O
the	O
AIM-54	O
Phoenix	O
long	O
range	O
missile	O
(	O
LRM	O
)	O
for	O
the	O
fleet	O
air	O
defense	O
mission	O
.	O
Phoenix	O
was	O
the	O
first	O
US	O
fire-and-forget	O
multiple	O
launch	O
radar-guided	O
missile	O
:	O
one	O
which	O
used	O
its	O
own	O
active	O
guidance	O
system	O
to	O
guide	O
itself	O
without	O
help	O
from	O
the	O
launch	O
aircraft	O
when	O
it	O
closed	O
on	O
its	O
target	O
.	O
This	O
gave	O
a	O
Tomcat	O
with	O
a	O
six	O
Phoenix	O
load	O
the	O
unprecedented	O
capability	O
of	O
tracking	O
and	O
destroying	O
up	O
to	O
six	O
targets	O
as	O
far	O
as	O
100	O
miles	O
(	O
160	O
km	O
)	O
away	O
.	O
The	O
Phoenix	O
could	O
only	O
be	O
carried	O
by	O
the	O
F-14	O
,	O
making	O
the	O
Tomcat	O
the	O
only	O
US	O
fighter	O
with	O
a	O
multiple	O
shot	O
,	O
fire-and-forget	O
radar	O
missile	O
targets	O
for	O
beyond	O
visual	O
range	O
.	O
The	O
US	O
Navy	O
retired	O
its	O
Phoenix	O
capability	O
in	O
2005	O
in	O
light	O
of	O
availability	O
of	O
the	O
AIM-120	O
AMRAAM	O
on	O
the	O
F/A-18	O
Hornet	O
and	O
the	O
pending	O
retirement	O
of	O
the	O
F-14	O
Tomcat	O
from	O
active	O
US	O
Navy	O
service	O
in	O
late	O
2006	O
.	O
What	O
was	O
needed	O
was	O
Phoenix	O
type	O
multiple	O
launch	O
and	O
terminal	O
active	O
capability	O
in	O
a	O
Sparrow	O
size	O
airframe	O
.	O
But	O
while	O
the	O
USAF	O
had	O
passed	O
on	O
the	O
Phoenix	O
and	O
their	O
own	O
similar	O
AIM-47	O
/	O
YF-12	O
to	O
optimize	O
dogfight	O
performance	O
,	O
they	O
still	O
desired	O
the	O
Navy	O
's	O
multiple	O
launch	O
fire	O
and	O
forget	O
capability	O
for	O
the	O
F-15	O
and	O
F-16	O
.	O
AMRAAM	O
would	O
need	O
to	O
be	O
fitted	O
on	O
fighters	O
as	O
small	O
as	O
the	O
F-16	O
,	O
and	O
fit	O
in	O
the	O
same	O
spaces	O
that	O
were	O
designed	O
to	O
fit	O
the	O
Sparrow	O
since	O
the	O
Phantom	O
.	O
The	O
US	O
Navy	O
needed	O
AMRAAM	O
to	O
be	O
carried	O
on	O
the	O
F/A-18	O
Hornet	O
and	O
wanted	O
capability	O
for	O
two	O
to	O
be	O
carried	O
on	O
a	O
launcher	O
that	O
normally	O
carried	O
one	O
Sparrow	O
to	O
allow	O
for	O
more	O
air-to-ground	O
weapons	O
.	O
AMRAAM	O
would	O
eventually	O
be	O
the	O
primary	O
weapon	O
for	O
the	O
F-22	O
Raptor	O
which	O
needed	O
to	O
fit	O
all	O
its	O
missiles	O
in	O
internal	O
weapons	O
bays	O
like	O
the	O
old	O
F-106	O
Delta	O
Darts	O
in	O
order	O
to	O
maintain	O
a	O
stealthy	O
radar	O
cross-section	O
.	O
Eventually	O
ASRAAM	O
was	O
developed	O
solely	O
by	O
the	O
UK	O
with	O
another	O
source	O
for	O
its	O
seeker	O
.	O
After	O
protracted	O
development	O
,	O
deployment	O
of	O
AMRAAM	O
(	O
AIM-120A	O
)	O
began	O
in	O
September	O
1991	O
with	O
USAF	O
F-15	O
Eagle	O
squadrons	O
.	O
The	O
US	O
Navy	O
followed	O
suit	O
in	O
1993	O
with	O
the	O
F/A-18C	O
.	O
The	O
eastern	O
counterpart	O
of	O
AMRAAM	O
is	O
the	O
very	O
similar	O
Russian	O
AA-12	O
Adder	O
,	O
commonly	O
known	O
in	O
the	O
west	O
as	O
"	O
AMRAAMski	O
.	O
"	O
AMRAAM	O
has	O
an	O
all-weather	O
,	O
beyond-visual-range	O
(	O
BVR	O
)	O
capability	O
.	O
It	O
improves	O
the	O
aerial	O
combat	O
capabilities	O
of	O
U.S.	O
and	O
allied	O
aircraft	O
to	O
meet	O
the	O
future	O
threat	O
of	O
enemy	O
air-to-air	O
weapons	O
.	O
AMRAAM	O
serves	O
as	O
a	O
follow-on	O
to	O
the	O
AIM-7	O
Sparrow	O
missile	O
series	O
.	O
AMRAAM	O
uses	O
two-stage	O
guidance	O
when	O
fired	O
at	O
long	O
range	O
.	O
Not	O
all	O
AMRAAM	O
users	O
have	O
elected	O
to	O
purchase	O
the	O
mid-course	O
update	O
option	O
,	O
which	O
limits	O
AMRAAM	O
's	O
effectiveness	O
in	O
some	O
scenarios	O
.	O
The	O
RAF	O
initially	O
opted	O
not	O
to	O
use	O
mid-course	O
update	O
for	O
its	O
Tornado	O
F3	O
force	O
,	O
only	O
to	O
discover	O
that	O
without	O
it	O
,	O
testing	O
proved	O
the	O
AMRAAM	O
was	O
less	O
effective	O
in	O
BVR	O
engagements	O
than	O
the	O
older	O
semi-active	O
radar	O
homing	O
BAE	O
Skyflash	O
weapon	O
--	O
the	O
AIM-120	O
's	O
own	O
radar	O
is	O
necessarily	O
of	O
limited	O
range	O
and	O
power	O
compared	O
to	O
that	O
of	O
the	O
launch	O
aircraft	O
.	O
If	O
the	O
target	O
is	O
not	O
armed	O
with	O
any	O
medium	O
or	O
long-range	O
fire-and-forget	O
weapons	O
,	O
the	O
attacking	O
aircraft	O
need	O
only	O
to	O
get	O
close	O
enough	O
to	O
the	O
target	O
and	O
launch	O
the	O
AMRAAM	O
.	O
In	O
these	O
scenarios	O
,	O
the	O
AMRAAM	O
has	O
a	O
high	O
chance	O
of	O
hitting	O
,	O
especially	O
against	O
low-maneuverability	O
targets	O
.	O
If	O
the	O
targets	O
are	O
armed	O
with	O
missiles	O
,	O
the	O
fire-and-forget	O
nature	O
of	O
the	O
AMRAAM	O
is	O
invaluable	O
,	O
enabling	O
the	O
launching	O
aircraft	O
to	O
fire	O
missiles	O
at	O
the	O
target	O
and	O
subsequently	O
take	O
defensive	O
actions	O
.	O
Even	O
if	O
the	O
targets	O
have	O
longer-range	O
semi-active	O
radar	O
homing	O
(	O
SARH	O
)	O
missiles	O
,	O
they	O
will	O
have	O
to	O
chase	O
the	O
launching	O
aircraft	O
in	O
order	O
for	O
the	O
missiles	O
to	O
track	O
them	O
,	O
effectively	O
flying	O
right	O
into	O
the	O
AMRAAM	O
.	O
However	O
the	O
chance	O
of	O
success	O
is	O
still	O
good	O
and	O
compared	O
to	O
the	O
relative	O
impunity	O
the	O
launching	O
aircraft	O
enjoy	O
,	O
this	O
gives	O
the	O
AMRAAM-equipped	O
aircraft	O
a	O
decisive	O
edge	O
.	O
If	O
one	O
or	O
more	O
missiles	O
fail	O
to	O
hit	O
,	O
the	O
AMRAAM-equipped	O
aircraft	O
can	O
turn	O
and	O
re-engage	O
,	O
although	O
they	O
will	O
be	O
at	O
a	O
disadvantage	O
compared	O
to	O
the	O
chasing	O
aircraft	O
due	O
to	O
the	O
speed	O
they	O
lose	O
in	O
the	O
turn	O
,	O
and	O
would	O
have	O
to	O
be	O
careful	O
that	O
they	O
're	O
not	O
being	O
tracked	O
with	O
SARH	O
missiles	O
.	O
The	O
other	O
main	O
engagement	O
scenario	O
is	O
against	O
other	O
aircraft	O
with	O
fire-and-forget	O
missiles	O
like	O
the	O
Vympel	O
R-77	O
--	O
perhaps	O
MiG-29s	O
,	O
Su-27s	O
or	O
similar	O
.	O
Although	O
in	O
this	O
regard	O
the	O
RVV-AE	O
does	O
have	O
an	O
advantage	O
as	O
it	O
has	O
a	O
greater	O
range	O
than	O
the	O
AMRAAM	O
[	O
citation	O
needed	O
]	O
.	O
There	O
are	O
currently	O
three	O
variants	O
of	O
AMRAAM	O
,	O
all	O
in	O
service	O
with	O
the	O
United	O
States	O
Air	O
Force	O
,	O
USN	O
,	O
and	O
the	O
United	O
States	O
Marine	O
Corps	O
.	O
The	O
AIM-120C	O
has	O
smaller	O
"	O
clipped	O
"	O
aerosurfaces	O
to	O
enable	O
internal	O
carriage	O
on	O
the	O
USAF	O
F-22	O
Raptor	O
.	O
The	O
AIM-120C	O
has	O
been	O
steadily	O
upgraded	O
since	O
it	O
was	O
introduced	O
.	O
The	O
AIM-120C-6	O
contained	O
an	O
improved	O
fuse	O
compared	O
to	O
its	O
predecessor	O
.	O
The	O
AIM-120C-7	O
development	O
began	O
in	O
1998	O
and	O
included	O
improvements	O
in	O
homing	O
and	O
greater	O
range	O
(	O
actual	O
amount	O
of	O
improvement	O
unspecified	O
)	O
.	O
It	O
helped	O
the	O
U.S.	O
Navy	O
replace	O
the	O
F-14	O
Tomcats	O
with	O
F/A-18E/F	O
Super	O
Hornets	O
--	O
the	O
loss	O
of	O
the	O
F-14	O
's	O
long-range	O
AIM-54	O
Phoenix	O
missiles	O
(	O
already	O
retired	O
)	O
can	O
be	O
partially	O
offset	O
with	O
a	O
longer-range	O
AMRAAM	O
,	O
but	O
note	O
that	O
the	O
AMRAAM	O
does	O
not	O
have	O
the	O
long	O
range	O
capability	O
of	O
the	O
Phoenix	O
missile	O
.	O
Raytheon	PERSON
is	O
also	O
working	O
with	O
the	O
Missile	O
Defense	O
Agency	O
to	O
develop	O
the	O
Network	O
Centric	O
Airborne	O
Defense	O
Element	O
(	O
NCADE	O
)	O
,	O
an	O
anti-ballistic	O
missile	O
derived	O
from	O
the	O
AIM-120	O
.	O
In	O
place	O
of	O
a	O
proximity-fused	O
warhead	O
,	O
the	O
NCADE	O
will	O
use	O
a	O
kinetic	O
energy	O
hit-to-kill	O
vehicle	O
based	O
on	O
the	O
one	O
used	O
in	O
the	O
Navy	O
's	O
RIM-161	O
Standard	O
Missile	O
3	O
.	O
The	O
Norwegian	O
Advanced	O
Surface-to-Air	O
Missile	O
System	O
(	O
NASAMS	O
)	O
,	O
developed	O
by	O
Kongsberg	O
Defence	O
&	O
Aerospace	O
,	O
consists	O
of	O
a	O
number	O
of	O
vehicle-pulled	O
launch	O
batteries	O
along	O
with	O
separate	O
radar	O
trucks	O
and	O
control	O
station	O
vehicles	O
.	O
The	O
United	O
Arab	O
Emirates	O
(	O
UAE	O
)	O
has	O
requested	O
the	O
purchasing	O
of	O
SL-AMRAAM	O
as	O
part	O
of	O
a	O
larger	O
7	O
billion	O
dollar	O
foreign	O
military	O
sales	O
package	O
.	O
The	O
US	O
Army	O
has	O
test	O
fired	O
the	O
SL-AMRAAM	O
from	O
a	O
HIMARS	O
artillery	O
rocket	O
launcher	O
as	O
a	O
common	O
launcher	O
.	O
The	O
AMRAAM	O
was	O
used	O
for	O
the	O
first	O
time	O
on	O
27	O
December	O
1992	O
,	O
when	O
an	O
USAF	O
F-16D	O
shot	O
down	O
an	O
Iraqi	O
MiG-25	O
that	O
violated	O
the	O
southern	O
no-fly-zone	O
.	O
At	O
that	O
point	O
three	O
launches	O
in	O
combat	O
resulted	O
in	O
three	O
kills	O
,	O
resulting	O
in	O
the	O
AMRAAM	O
being	O
informally	O
named	O
"	O
slammer	O
"	O
in	O
the	O
second	O
half	O
of	O
the	O
1990s	O
.	O
As	O
of	O
mid	O
2008	O
,	O
the	O
AIM-120	O
AMRAAM	O
has	O
shot	O
down	O
nine	O
enemy	O
aircraft	O
(	O
six	O
MiG-29	O
,	O
one	O
MiG-25	O
,	O
one	O
MiG-23	O
,	O
and	O
one	O
Soko	O
J-21	O
Jastreb	O
)	O
.	O
In	O
2006	O
Poland	O
received	O
AIM-120C-5	O
missiles	O
to	O
arm	O
its	O
new	O
F-16C/D	O
Block	O
52+	O
fighters	O
.	O
In	O
early	O
2006	O
the	O
Pakistan	O
Air	O
Force	O
(	O
PAF	O
)	O
ordered	O
500	O
AIM-120C-5	O
AMRAAM	O
missiles	O
as	O
part	O
of	O
a	O
$	O
650	O
million	O
F-16	O
ammunition	O
deal	O
to	O
equip	O
the	O
PAF	O
's	O
F-16C/D	O
Block	O
52+	O
and	O
F-16A/B	O
MLU	O
fighters	O
.	O
Total	O
value	O
of	O
the	O
package	O
,	O
including	O
launchers	O
,	O
maintenance	O
,	O
spare	O
parts	O
,	O
support	O
and	O
training	O
rounds	O
,	O
was	O
estimated	O
at	O
around	O
$	O
421	O
million	O
USD	O
.	O
2008	O
has	O
brought	O
announcements	O
of	O
new	O
or	O
additional	O
sales	O
to	O
Singapore	O
,	O
Finland	O
,	O
Morocco	O
and	O
South	O
Korea	O
.	O
