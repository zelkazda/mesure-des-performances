Alexander	PERSON
Emanuel	PERSON
Agassiz	PERSON
(	O
December	O
17	O
,	O
1835	O
--	O
March	O
27	O
,	O
1910	O
)	O
,	O
son	O
of	O
Louis	PERSON
Agassiz	PERSON
and	O
stepson	O
of	O
Elizabeth	PERSON
Cabot	PERSON
Agassiz	PERSON
,	O
was	O
an	O
American	O
scientist	O
and	O
engineer	O
.	O
Agassiz	PERSON
was	O
born	O
in	O
Neuchâtel	LOCATION
,	O
Switzerland	O
and	O
emigrated	O
to	O
the	O
United	O
States	O
with	O
his	O
father	O
in	O
1849	O
.	O
He	O
graduated	O
from	O
Harvard	O
University	O
in	O
1855	O
,	O
subsequently	O
studying	O
engineering	O
and	O
chemistry	O
,	O
and	O
taking	O
the	O
degree	O
of	O
bachelor	O
of	O
science	O
at	O
the	O
Lawrence	O
scientific	O
school	O
of	O
the	O
same	O
institution	O
in	O
1857	O
;	O
and	O
in	O
1859	O
became	O
an	O
assistant	O
in	O
the	O
United	O
States	O
Coast	O
Survey	O
.	O
He	O
persuaded	O
them	O
,	O
along	O
with	O
a	O
group	O
of	O
friends	O
,	O
to	O
purchase	O
a	O
controlling	O
interest	O
in	O
the	O
mines	O
,	O
which	O
later	O
became	O
known	O
as	O
the	O
Calumet	O
and	O
Hecla	O
Mining	O
Company	O
based	O
in	O
Calumet	LOCATION
,	O
Michigan	O
.	O
Up	O
until	O
the	O
summer	O
of	O
1866	O
,	O
Agassiz	PERSON
worked	O
as	O
an	O
assistant	O
in	O
the	O
museum	O
of	O
natural	O
history	O
that	O
his	O
father	O
founded	O
at	O
Harvard	O
.	O
But	O
Agassiz	PERSON
refused	O
to	O
give	O
up	O
hope	O
for	O
the	O
mines	O
,	O
and	O
he	O
returned	O
to	O
the	O
mines	O
in	O
March	O
1867	O
with	O
his	O
wife	O
and	O
young	O
son	O
.	O
At	O
that	O
time	O
,	O
Calumet	O
was	O
a	O
remote	O
settlement	O
,	O
virtually	O
inaccessible	O
during	O
the	O
winter	O
and	O
very	O
far	O
removed	O
from	O
civilization	O
even	O
during	O
the	O
summer	O
.	O
Agassiz	PERSON
was	O
a	O
major	O
factor	O
in	O
the	O
mine	O
's	O
continued	O
success	O
and	O
visited	O
the	O
mines	O
twice	O
a	O
year	O
.	O
He	O
innovated	O
by	O
installing	O
a	O
giant	O
engine	O
,	O
known	O
as	O
the	O
Superior	O
,	O
which	O
was	O
able	O
to	O
lift	O
24	O
tons	O
of	O
rock	O
from	O
a	O
depth	O
of	O
1,200	O
metres	O
(	O
4,000	O
ft	O
)	O
.	O
However	O
,	O
after	O
a	O
time	O
the	O
mines	O
did	O
not	O
require	O
his	O
full-time	O
year-round	O
attention	O
and	O
he	O
returned	O
to	O
his	O
interests	O
in	O
natural	O
history	O
at	O
Harvard	O
.	O
In	O
1875	O
he	O
surveyed	O
Lake	O
Titicaca	O
,	O
Peru	O
,	O
examined	O
the	O
copper	O
mines	O
of	O
Peru	LOCATION
and	LOCATION
Chile	LOCATION
,	O
and	O
made	O
a	O
collection	O
of	O
Peruvian	O
antiquities	O
for	O
the	O
Museum	O
of	O
Comparative	O
Zoology	O
,	O
of	O
which	O
he	O
was	O
curator	O
from	O
1874	O
to	O
1885	O
.	O
In	O
1896	O
Agassiz	O
visited	O
Fiji	O
and	O
Queensland	O
and	O
inspected	O
the	O
Great	O
Barrier	O
Reef	O
,	O
publishing	O
a	O
paper	O
on	O
the	O
subject	O
in	O
1898	O
.	O
Agassiz	PERSON
served	O
as	O
a	O
president	O
of	O
the	O
National	O
Academy	O
of	O
Sciences	O
,	O
which	O
since	O
1913	O
has	O
awarded	O
the	O
Alexander	PERSON
Agassiz	PERSON
Medal	O
in	O
his	O
memory	O
.	O
He	O
died	O
in	O
1910	O
onboard	O
the	O
RMS	O
Adriatic	O
en	O
route	O
to	O
New	LOCATION
York	LOCATION
from	LOCATION
Southampton	LOCATION
.	O
