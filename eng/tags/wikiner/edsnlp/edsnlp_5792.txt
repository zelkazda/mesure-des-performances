Haryana	O
is	O
a	O
state	O
in	O
India	O
.	O
It	O
is	O
bordered	O
by	O
Punjab	O
and	O
Himachal	O
Pradesh	O
to	O
the	O
north	O
,	O
and	O
by	O
Rajasthan	O
to	O
the	O
west	O
and	O
south	O
.	O
The	O
river	O
Yamuna	O
defines	O
its	O
eastern	O
border	O
with	O
Uttarakhand	O
and	O
Uttar	O
Pradesh	O
.	O
Haryana	O
also	O
surrounds	O
Delhi	LOCATION
on	O
three	O
sides	O
,	O
forming	O
the	O
northern	O
,	O
western	O
and	O
southern	O
borders	O
of	O
Delhi	LOCATION
.	O
Consequently	O
,	O
a	O
large	O
area	O
of	O
Haryana	O
is	O
included	O
in	O
the	O
National	O
Capital	O
Region	O
.	O
The	O
capital	O
of	O
the	O
state	O
is	O
Chandigarh	O
which	O
is	O
administered	O
as	O
a	O
union	O
territory	O
and	O
is	O
also	O
the	O
capital	O
of	O
Punjab	O
.	O
Several	O
decisive	O
battles	O
were	O
fought	O
in	O
the	O
area	O
,	O
which	O
shaped	O
much	O
of	O
the	O
history	O
of	O
India	O
.	O
These	O
include	O
the	O
epic	O
battle	O
of	O
Mahabharata	O
at	O
Kurukshetra	O
(	O
including	O
the	O
recital	O
of	O
the	O
Bhagavad	O
Gita	O
by	O
Krishna	PERSON
)	O
,	O
and	O
the	O
three	O
battles	O
of	O
Panipat	O
.	O
Haryana	PERSON
is	O
now	O
a	O
leading	O
contributor	O
to	O
the	O
country	O
's	O
production	O
of	O
foodgrain	O
and	O
milk	O
.	O
Haryana	O
is	O
India	O
's	O
largest	O
manufacturer	O
of	O
passenger	O
cars	O
,	O
two-wheelers	O
,	O
and	O
tractors	O
.	O
Since	O
2000	O
,	O
the	O
state	O
has	O
emerged	O
as	O
the	O
largest	O
recipient	O
of	O
investment	O
per	O
capita	O
in	O
India	O
.	O
The	O
city	O
of	O
Gurgaon	LOCATION
has	O
rapidly	O
emerged	O
as	O
a	O
major	O
hub	O
for	O
the	O
information	O
technology	O
and	O
automobile	O
industries	O
.	O
The	O
most	O
extensive	O
center	O
,	O
Rakhigarhi	O
,	O
is	O
now	O
a	O
village	O
in	O
Hisar	LOCATION
District	LOCATION
.	O
Several	O
decisive	O
battles	O
were	O
fought	O
in	O
the	O
area	O
,	O
which	O
shaped	O
much	O
of	O
the	O
history	O
of	O
India	O
.	O
These	O
include	O
the	O
epic	O
Battle	O
of	O
Kurukshetra	O
described	O
in	O
the	O
Mahabharata	O
(	O
including	O
the	O
recital	O
of	O
the	O
Bhagavad	O
Gita	O
by	O
Krishna	O
)	O
and	O
the	O
three	O
battles	O
of	O
Panipat	O
.	O
The	O
region	O
remained	O
strategically	O
important	O
for	O
the	O
rulers	O
of	O
North	O
India	O
even	O
though	O
Thanesar	O
was	O
no	O
more	O
central	O
than	O
Kannauj	O
.	O
Prithviraj	O
Chauhan	O
established	O
forts	O
at	O
Tarori	LOCATION
and	LOCATION
Hansi	LOCATION
in	O
the	O
12th	O
century	O
.	O
Muhammad	PERSON
Ghori	PERSON
conquered	O
this	O
area	O
in	O
the	O
Second	O
Battle	O
of	O
Tarain	O
.	O
Following	O
his	O
death	O
,	O
the	O
Delhi	O
Sultanate	O
was	O
established	O
that	O
ruled	O
much	O
of	O
India	O
for	O
several	O
centuries	O
.	O
The	O
three	O
famous	O
battles	O
of	O
Panipat	O
took	O
place	O
near	O
the	O
modern	O
town	O
of	O
Panipat	LOCATION
in	LOCATION
Haryana	LOCATION
.	O
This	O
included	O
most	O
of	O
Haryana	O
,	O
while	O
the	O
rest	O
were	O
ruled	O
by	O
the	O
princely	O
states	O
of	O
Loharu	O
,	O
Nabha	O
,	O
Jind	O
and	O
Patiala	O
.	O
During	O
the	O
Indian	O
rebellion	O
of	O
1857	O
,	O
several	O
leaders	O
from	O
this	O
region	O
,	O
including	O
Rao	O
Tula	PERSON
Ram	PERSON
,	O
participated	O
actively	O
.	O
People	O
of	O
Haryana	O
took	O
an	O
active	O
part	O
in	O
the	O
Indian	O
Independence	O
movement	O
.	O
Some	O
most	O
important	O
fights	O
were	O
at	O
Sonipat	O
,	O
Rohtak	O
,	O
Sirsa	O
and	O
Hissar	O
.	O
Later	O
,	O
leaders	O
like	O
Sir	O
Chhotu	O
Ram	O
played	O
an	O
important	O
role	O
in	O
the	O
politics	O
of	O
the	O
Punjab	O
province	O
.	O
Rao	O
Tula	PERSON
Ram	PERSON
was	O
one	O
of	O
the	O
important	O
leaders	O
of	O
the	O
Indian	O
Rebellion	O
of	O
1857	O
.	O
According	O
to	O
this	O
report	O
the	O
then	O
districts	O
of	O
Hissar	O
,	O
Mahendragarh	O
,	O
Gurgaon	O
,	O
Rohtak	O
,	O
and	O
Karnal	O
were	O
to	O
be	O
a	O
part	O
of	O
the	O
new	O
state	O
of	O
Haryana	O
.	O
Haryana	O
is	O
a	O
landlocked	O
state	O
in	O
northern	O
India	O
.	O
The	O
altitude	O
of	O
Haryana	O
varies	O
between	O
700	O
to	O
3600	O
ft	O
(	O
200	O
metres	O
to	O
1200	O
metres	O
)	O
above	O
sea	O
level	O
.	O
Haryana	O
has	O
four	O
main	O
geographical	O
features	O
.	O
The	O
river	O
Yamuna	O
flows	O
along	O
its	O
eastern	O
boundary	O
.	O
Passing	O
through	O
Ambala	O
and	O
Hissar	O
,	O
it	O
reaches	O
Bikaner	O
in	O
Rajasthan	O
and	O
runs	O
a	O
course	O
of	O
290	O
miles	O
before	O
disappearing	O
into	O
the	O
deserts	O
of	O
Rajasthan	O
.	O
On	O
reaching	O
Rohtak	O
it	O
branches	O
off	O
into	O
two	O
smaller	O
streams	O
,	O
finally	O
reaching	O
the	O
outskirts	O
of	O
Delhi	O
and	O
flowing	O
into	O
the	O
Yamuna	O
.	O
The	O
species	O
of	O
fauna	O
found	O
in	O
the	O
state	O
of	O
Haryana	O
include	O
black	O
buck	O
,	O
nilgai	O
,	O
panther	O
,	O
fox	O
,	O
mongoose	O
,	O
jackal	O
and	O
wild	O
dog	O
.	O
Haryana	O
,	O
along	O
with	O
neighboring	O
Punjab	O
,	O
has	O
a	O
skewed	O
sex	O
ratio	O
at	O
861	O
,	O
with	O
many	O
more	O
men	O
than	O
women	O
.	O
These	O
days	O
the	O
state	O
is	O
seeing	O
a	O
massive	O
influx	O
of	O
immigrants	O
from	O
across	O
the	O
nation	O
,	O
primarily	O
from	O
Bihar	O
,	O
Bengal	O
,	O
Uttrakhand	O
,	O
Rajasthan	O
,	O
Uttar	O
Pradesh	O
and	O
Nepal	O
.	O
The	O
Chief	O
Minister	O
is	O
the	O
head	O
of	O
the	O
Haryana	O
state	O
government	O
and	O
is	O
vested	O
with	O
most	O
of	O
the	O
executive	O
powers	O
.	O
The	O
present	O
political	O
scenario	O
of	O
the	O
state	O
is	O
clear	O
and	O
it	O
has	O
a	O
stable	O
government	O
under	O
Bhupinder	PERSON
Singh	PERSON
Hooda	PERSON
who	O
is	O
presently	O
the	O
Chief	O
Minister	O
of	O
the	O
state	O
.	O
The	O
people	O
of	O
Haryana	LOCATION
have	O
their	O
own	O
traditions	O
.	O
Famous	O
yoga	O
guru	O
Swami	PERSON
Ramdev	PERSON
is	O
from	O
Mahendragarh	LOCATION
in	LOCATION
Haryana	LOCATION
.	O
Haryana	O
has	O
a	O
variety	O
of	O
folk	O
dances	O
,	O
which	O
like	O
other	O
creative	O
art	O
,	O
help	O
in	O
sublimating	O
the	O
performer	O
's	O
worries	O
and	O
cares	O
.	O
The	O
people	O
of	O
Haryana	LOCATION
have	O
preserved	O
their	O
old	O
religious	O
and	O
social	O
traditions	O
.	O
Their	O
culture	O
and	O
popular	O
art	O
are	O
Saangs	O
,	O
dramas	O
,	O
ballads	O
and	O
songs	O
in	O
which	O
they	O
take	O
great	O
delight	O
.	O
Haryanavi	O
has	O
traditionally	O
been	O
the	O
dominant	O
language	O
spoken	O
by	O
the	O
martial	O
people	O
of	O
Haryana	LOCATION
.	O
Punjabi	O
is	O
second	O
official	O
language	O
of	O
Haryana	O
.	O
Sanskrit	O
is	O
also	O
taught	O
in	O
most	O
of	O
the	O
schools	O
in	O
Haryana	LOCATION
.	O
The	O
most	O
striking	O
feature	O
of	O
Haryana	O
is	O
its	O
language	O
itself	O
;	O
or	O
rather	O
,	O
the	O
manner	O
in	O
which	O
it	O
is	O
spoken	O
.	O
With	O
rapid	O
urbanization	O
,	O
and	O
due	O
to	O
Haryana	O
's	O
close	O
proximity	O
to	O
Delhi	O
,	O
the	O
cultural	O
aspects	O
are	O
now	O
taking	O
a	O
more	O
modern	O
hue	O
.	O
The	O
economy	O
of	O
Haryana	O
relies	O
on	O
manufacturing	O
,	O
business	O
process	O
outsourcing	O
,	O
agriculture	O
and	O
retail	O
.	O
The	O
two	O
financial	O
hubs	O
of	O
Haryana	LOCATION
,	O
Gurgaon	O
and	O
Faridabad	O
lie	O
on	O
the	O
south	O
west	O
of	O
the	O
state	O
.	O
As	O
a	O
result	O
,	O
Haryana	O
's	O
share	O
in	O
national	O
production	O
is	O
50	O
%	O
of	O
passenger	O
cars	O
,	O
50	O
%	O
of	O
motorcycles	O
,	O
30	O
%	O
of	O
refrigerators	O
,	O
25	O
%	O
of	O
tractors	O
,	O
bicycles	O
and	O
sanitary	O
ware	O
,	O
and	O
20	O
%	O
of	O
the	O
country	O
's	O
export	O
of	O
scientific	O
instruments	O
.	O
Yamunanagar	O
district	O
has	O
a	O
paper	O
mill	O
BILT	O
,	O
Haryana	O
has	O
a	O
large	O
production	O
of	O
cars	O
,	O
motorcycles	O
,	O
tractors	O
,	O
sanitary	O
ware	O
,	O
glass	O
container	O
industry	O
,	O
gas	O
stoves	O
and	O
scientific	O
instruments	O
.	O
Faridabad	O
is	O
another	O
big	O
industrial	O
part	O
of	O
Haryana	LOCATION
.	O
Panipat	O
is	O
a	O
city	O
of	O
textiles	O
and	O
carpets	O
.	O
It	O
is	O
the	O
biggest	O
centre	O
for	O
cheap	O
blankets	O
and	O
carpets	O
in	O
India	O
and	O
has	O
a	O
handloom	O
weaving	O
industry	O
.	O
Panipat	O
also	O
has	O
heavy	O
industry	O
,	O
with	O
a	O
refinery	O
of	O
the	O
Indian	O
Oil	O
Corporation	O
,	O
a	O
National	O
Thermal	O
Power	O
Corporation	O
power	O
plant	O
and	O
a	O
National	O
Fertilizers	O
Limited	O
plant	O
.	O
Hissar	O
is	O
another	O
big	O
city	O
where	O
Jindal	O
company	O
has	O
established	O
.	O
Jindal	O
Steel	O
now	O
increasing	O
their	O
business	O
and	O
open	O
new	O
factory	O
in	O
other	O
state	O
also	O
.	O
Gurgaon	O
is	O
considered	O
the	O
best	O
city	O
for	O
setting	O
up	O
a	O
software	O
or	O
BPO	O
centre	O
in	O
India	O
.	O
Gurgaon	O
,	O
has	O
seen	O
emergence	O
of	O
an	O
active	O
information	O
technology	O
industry	O
in	O
the	O
recent	O
years	O
.	O
With	O
organisations	O
like	O
Tata	O
Consultancy	O
Services	O
,	O
IBM	O
,	O
NIIT	O
,	O
Hewitt	O
Associates	O
,	O
Dell	O
,	O
Convergys	O
,	O
United	O
Healthcare	O
and	O
The	O
Boston	O
Consulting	O
Group	O
setting	O
up	O
back	O
offices	O
or	O
contact	O
centers	O
in	O
Gurgaon	O
.	O
Haryana	O
now	O
ranks	O
3rd	O
among	O
states	O
in	O
software	O
exports	O
from	O
India	O
.	O
Despite	O
recent	O
industrial	O
development	O
,	O
Haryana	O
is	O
primarily	O
an	O
agricultural	O
state	O
.	O
Haryana	O
is	O
self-sufficient	O
in	O
food	O
production	O
and	O
the	O
second	O
largest	O
contributor	O
to	O
India	O
's	O
central	O
pool	O
of	O
food	O
grains	O
.	O
The	O
main	O
crops	O
of	O
Haryana	O
are	O
wheat	O
,	O
rice	O
,	O
sugarcane	O
,	O
cotton	O
,	O
oilseeds	O
,	O
pulses	O
,	O
barley	O
,	O
maize	O
,	O
millet	O
etc	O
.	O
Haryana	O
has	O
a	O
livestock	O
population	O
of	O
98.97	O
lakh	O
.	O
Haryana	O
,	O
with	O
660	O
grams	O
of	O
availability	O
of	O
milk	O
per	O
capita	O
per	O
day	O
,	O
ranks	O
at	O
number	O
two	O
in	O
the	O
country	O
as	O
against	O
the	O
national	O
average	O
of	O
232	O
grams	O
.	O
The	O
National	O
Dairy	O
Research	O
Institute	O
at	O
Karnal	O
,	O
and	O
the	O
Central	O
Institute	O
for	O
Research	O
on	O
Buffaloes	O
at	O
Hisar	LOCATION
are	O
instrumental	O
in	O
development	O
of	O
new	O
breeds	O
of	O
cattle	O
and	O
propagation	O
of	O
these	O
breeds	O
through	O
embryo	O
transfer	O
technology	O
.	O
Haryana	O
is	O
a	O
trend	O
setter	O
in	O
the	O
field	O
of	O
passenger	O
transport	O
.	O
It	O
passes	O
through	O
the	O
districts	O
of	O
Sonipat	LOCATION
,	LOCATION
Panipat	LOCATION
,	LOCATION
Karnal	LOCATION
,	LOCATION
Kurukshetra	LOCATION
and	LOCATION
Ambala	LOCATION
in	LOCATION
north	LOCATION
Haryana	LOCATION
where	O
it	O
enters	O
Delhi	O
and	O
subsequently	O
the	O
industrial	O
town	O
of	O
Faridabad	LOCATION
on	O
its	O
way	O
.	O
Haryana	O
is	O
in	O
close	O
contact	O
with	O
the	O
cosmopolitan	O
world	O
,	O
being	O
right	O
next	O
to	O
Delhi	LOCATION
.	O
Two	O
of	O
the	O
16	O
airlines	O
in	O
India	O
are	O
based	O
in	O
Gurgaon	O
which	O
is	O
just	O
a	O
few	O
kilometers	O
from	O
the	O
international	O
airport	O
in	O
Delhi	LOCATION
.	O
Haryana	O
has	O
a	O
state-wide	O
network	O
of	O
highly	O
efficient	O
telecommunication	O
facilities	O
.	O
The	O
state	O
is	O
divided	O
into	O
four	O
divisions	O
for	O
administrative	O
purpose	O
--	O
Ambala	O
Division	O
,	O
Rohtak	O
Division	O
,	O
Gurgaon	O
Division	O
and	O
Hisar	O
Division	O
.	O
Haryana	O
has	O
a	O
total	O
of	O
81	O
cities	O
and	O
towns	O
and	O
6,759	O
villages	O
.	O
The	O
state	O
of	O
Haryana	O
has	O
made	O
tremendous	O
progress	O
in	O
the	O
field	O
of	O
higher	O
education	O
since	O
its	O
inception	O
.	O
Haryana	O
Board	O
of	O
School	O
Education	O
,	O
established	O
in	O
September	O
1969	O
and	O
shifted	O
to	O
Bhiwani	O
in	O
1981	O
,	O
conducts	O
public	O
examinations	O
at	O
middle	O
,	O
matriculation	O
,	O
and	O
senior	O
secondary	O
levels	O
twice	O
a	O
year	O
.	O
The	O
National	O
Dairy	O
Research	O
Institute	O
at	O
Karnal	LOCATION
provides	O
education	O
in	O
the	O
field	O
of	O
dairy	O
science	O
.	O
It	O
has	O
been	O
upgraded	O
to	O
the	O
level	O
of	O
a	O
Deemed	O
University	O
.	O
Pt.	O
B.D.	O
Sharma	PERSON
PGIMS	PERSON
Rohtak	O
is	O
a	O
premier	O
post-graduate	O
medical	O
institute	O
in	O
North	O
India	O
offering	O
courses	O
in	O
major	O
specialties	O
and	O
super	O
specialties	O
of	O
medicine	O
.	O
The	O
Technological	O
Institute	O
of	O
Textile	O
&	O
Sciences	O
came	O
into	O
existence	O
in	O
1947	O
.	O
In	O
team	O
sports	O
,	O
Haryana	PERSON
is	O
the	O
national	O
champion	O
in	O
men	O
's	O
volleyball	O
and	O
women	O
's	O
hockey	O
.	O
Nahar	O
Singh	O
Stadium	O
for	O
international	O
cricket	O
was	O
built	O
in	O
Faridabad	LOCATION
in	O
the	O
year	O
1981	O
.	O
