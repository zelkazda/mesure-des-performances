Upper	O
Germania	O
included	O
the	O
region	O
between	O
the	O
upper	O
Rhine	O
and	O
the	O
upper	O
Danube	O
,	O
(	O
the	O
Black	O
Forest	O
region	O
that	O
was	O
larger	O
than	O
today	O
:	O
see	O
Hercynian	O
Forest	O
)	O
.	O
As	O
a	O
confederation	O
,	O
from	O
the	O
fifth	O
century	O
,	O
they	O
settled	O
the	O
Alsace	O
and	O
expanded	O
into	O
the	O
Swiss	O
Plateau	O
,	O
as	O
well	O
as	O
parts	O
of	O
what	O
are	O
now	O
Bavaria	O
and	O
Austria	O
,	O
reaching	O
the	O
valleys	O
of	O
the	O
Alps	O
by	O
the	O
eighth	O
century	O
.	O
The	O
Alamanni	O
,	O
thereafter	O
became	O
the	O
nation	O
of	O
Alamannia	O
,	O
that	O
was	O
sometimes	O
independent	O
,	O
but	O
more	O
often	O
was	O
ruled	O
by	O
the	O
Franks	O
.	O
The	O
region	O
of	O
the	O
Alamanni	O
was	O
always	O
somewhat	O
sprawling	O
and	O
comprised	O
a	O
number	O
of	O
different	O
districts	O
,	O
reflecting	O
its	O
mixed	O
origins	O
.	O
Alemannia	O
lost	O
its	O
distinct	O
jurisdictional	O
identity	O
when	O
Charles	O
Martel	O
absorbed	O
it	O
into	O
the	O
Frankish	O
empire	O
,	O
early	O
in	O
the	O
8th	O
century	O
.	O
According	O
to	O
Asinius	PERSON
Quadratus	PERSON
their	O
name	O
means	O
"	O
all	O
men	O
"	O
.	O
The	O
Alamanni	O
were	O
first	O
mentioned	O
by	O
Cassius	PERSON
Dio	PERSON
describing	O
the	O
campaign	O
of	O
Caracalla	O
in	O
213	O
.	O
At	O
that	O
time	O
they	O
apparently	O
dwelt	O
in	O
the	O
basin	O
of	O
the	O
Main	O
,	O
to	O
the	O
south	O
of	O
the	O
Chatti	O
.	O
Cassius	PERSON
Dio	PERSON
(	O
78.13.4	O
)	O
portrays	O
the	O
Alamanni	O
as	O
victims	O
of	O
this	O
treacherous	O
emperor	O
.	O
They	O
had	O
asked	O
for	O
his	O
help	O
,	O
says	O
Dio	PERSON
,	O
but	O
instead	O
he	O
colonized	O
their	O
country	O
,	O
changed	O
their	O
place	O
names	O
and	O
executed	O
their	O
warriors	O
under	O
a	O
pretext	O
of	O
coming	O
to	O
their	O
aid	O
.	O
When	O
he	O
became	O
ill	O
,	O
the	O
Alamanni	O
claimed	O
to	O
have	O
put	O
a	O
hex	O
on	O
him	O
(	O
78.15.2	O
)	O
.	O
Caracalla	O
,	O
it	O
was	O
claimed	O
,	O
tried	O
to	O
counter	O
this	O
influence	O
by	O
invoking	O
his	O
ancestral	O
spirits	O
.	O
In	O
retribution	O
Caracalla	O
then	O
led	O
the	O
Legio	O
II	O
Traiana	O
Fortis	O
against	O
the	O
Alamanni	O
,	O
who	O
lost	O
and	O
were	O
pacified	O
for	O
a	O
time	O
.	O
Caracalla	O
left	O
for	O
the	O
frontier	O
,	O
where	O
for	O
the	O
rest	O
of	O
his	O
short	O
reign	O
he	O
was	O
known	O
for	O
his	O
unpredictable	O
and	O
arbitrary	O
operations	O
launched	O
by	O
surprise	O
after	O
a	O
pretext	O
of	O
peace	O
negotiations	O
.	O
Most	O
of	O
them	O
probably	O
were	O
in	O
fact	O
resident	O
in	O
or	O
close	O
to	O
the	O
borders	O
of	O
Germania	O
Superior	O
.	O
Trees	O
from	O
the	O
earliest	O
fortifications	O
found	O
in	O
Germania	O
Inferior	O
are	O
dated	O
by	O
dendrochronology	O
to	O
99/100	O
.	O
Shortly	O
afterwards	O
Trajan	PERSON
was	O
chosen	O
by	O
Nerva	PERSON
to	O
be	O
his	O
successor	O
,	O
adopted	O
with	O
public	O
fanfare	O
in	O
absentia	O
by	O
the	O
old	O
man	O
shortly	O
before	O
his	O
death	O
.	O
Ammianus	PERSON
relates	O
(	O
xvii.1.11	O
)	O
that	O
much	O
later	O
the	O
Emperor	O
Julian	PERSON
undertook	O
a	O
punitive	O
expedition	O
against	O
the	O
Alamanni	O
,	O
who	O
by	O
then	O
were	O
in	O
Alsace	O
,	O
and	O
crossed	O
the	O
Main	O
,	O
entering	O
the	O
forest	O
,	O
where	O
the	O
trails	O
were	O
blocked	O
by	O
felled	O
trees	O
.	O
The	O
early	O
detailed	O
source	O
,	O
the	O
Germania	O
of	O
Tacitus	O
,	O
has	O
sometimes	O
been	O
interpreted	O
in	O
such	O
a	O
way	O
as	O
to	O
provide	O
yet	O
other	O
historical	O
problems	O
.	O
Tacitus	O
stated	O
that	O
they	O
traded	O
with	O
Rhaetia	O
,	O
which	O
in	O
Ptolemy	O
is	O
located	O
across	O
the	O
Danube	O
from	O
Germania	O
Superior	O
.	O
However	O
,	O
no	O
Hermunduri	O
appear	O
in	O
Ptolemy	LOCATION
,	O
though	O
after	O
the	O
time	O
of	O
Ptolemy	O
the	O
Hermunduri	O
joined	O
with	O
the	O
Marcomanni	O
in	O
the	O
wars	O
of	O
166	O
--	O
180	O
against	O
the	O
empire	O
.	O
A	O
careful	O
reading	O
of	O
Tacitus	O
provides	O
one	O
solution	O
.	O
He	O
says	O
that	O
the	O
source	O
of	O
the	O
Elbe	O
is	O
among	O
the	O
Hermunduri	O
,	O
somewhat	O
to	O
the	O
east	O
of	O
the	O
upper	O
Main	O
.	O
Nevertheless	O
some	O
conclusions	O
can	O
be	O
drawn	O
from	O
Ptolemy	O
.	O
Germania	O
Superior	O
is	O
easily	O
identified	O
.	O
On	O
the	O
other	O
side	O
of	O
the	O
northern	O
Black	O
Forest	O
were	O
the	O
Chatti	O
about	O
where	O
Hesse	O
is	O
today	O
,	O
on	O
the	O
lower	O
Main	O
.	O
It	O
did	O
not	O
include	O
the	O
upper	O
Main	O
,	O
but	O
that	O
is	O
where	O
Caracalla	O
campaigned	O
.	O
Moreover	O
,	O
the	O
territory	O
of	O
Germania	O
Superior	O
was	O
not	O
originally	O
included	O
among	O
the	O
Alemanni	O
's	O
possessions	O
.	O
However	O
,	O
if	O
we	O
look	O
for	O
the	O
peoples	O
in	O
the	O
region	O
from	O
the	O
upper	O
Main	O
in	O
the	O
north	O
,	O
south	O
to	O
the	O
Danube	O
and	O
east	O
to	O
the	O
Czech	O
Republic	O
where	O
the	O
Quadi	O
and	O
Marcomanni	O
were	O
located	O
,	O
Ptolemy	O
does	O
not	O
give	O
any	O
tribes	O
.	O
The	O
region	O
between	O
the	O
forest	O
and	O
the	O
Danube	O
on	O
the	O
other	O
hand	O
included	O
about	O
a	O
dozen	O
settlements	O
,	O
or	O
"	O
cantons	O
"	O
.	O
By	O
Caracalla	O
's	O
time	O
the	O
name	O
Alamanni	O
was	O
being	O
used	O
by	O
cantons	O
themselves	O
banding	O
together	O
for	O
purposes	O
of	O
supporting	O
a	O
citizen	O
army	O
(	O
the	O
"	O
war	O
bands	O
"	O
)	O
.	O
The	O
term	O
Suebi	O
has	O
a	O
double	O
meaning	O
in	O
the	O
sources	O
.	O
On	O
the	O
other	O
hand	O
the	O
Suebi	O
of	O
the	O
upper	O
Danube	O
are	O
described	O
as	O
though	O
they	O
were	O
a	O
tribe	O
.	O
The	O
upper	O
Rhine	O
and	O
Danube	O
appear	O
to	O
form	O
a	O
funnel	O
pointing	O
straight	O
at	O
Vesontio	O
.	O
The	O
Suebi	O
were	O
being	O
invited	O
to	O
join	O
.	O
He	O
did	O
not	O
pursue	O
the	O
retreating	O
remnants	O
,	O
leaving	O
what	O
was	O
left	O
of	O
the	O
German	O
army	O
and	O
their	O
dependents	O
intact	O
on	O
the	O
other	O
side	O
of	O
the	O
Rhine	O
.	O
He	O
crossed	O
the	O
Rhine	O
to	O
forestall	O
that	O
event	O
,	O
a	O
successful	O
strategy	O
.	O
As	O
they	O
had	O
left	O
their	O
tribal	O
homes	O
behind	O
,	O
they	O
probably	O
took	O
over	O
all	O
the	O
former	O
Celtic	O
cantons	O
along	O
the	O
Danube	O
.	O
The	O
Alamanni	O
established	O
a	O
series	O
of	O
territorially	O
defined	O
pagi	O
(	O
cantons	O
)	O
on	O
the	O
east	O
bank	O
of	O
the	O
Rhine	O
.	O
Ammianus	O
describes	O
Alamanni	O
rulers	O
with	O
various	O
terms	O
:	O
reges	O
excelsiores	O
ante	O
alios	O
(	O
"	O
paramount	O
kings	O
"	O
)	O
,	O
reges	O
proximi	O
(	O
"	O
neighbouring	O
kings	O
"	O
)	O
,	O
reguli	O
(	O
"	O
petty	O
kings	O
"	O
)	O
and	O
regales	O
(	O
"	O
princes	O
"	O
)	O
.	O
Their	O
territories	O
were	O
small	O
and	O
mostly	O
strung	O
along	O
the	O
Rhine	O
(	O
although	O
a	O
few	O
were	O
in	O
the	O
hinterland	O
)	O
.	O
The	O
Alamanni	O
were	O
continually	O
engaged	O
in	O
conflicts	O
with	O
the	O
Roman	O
Empire	O
.	O
In	O
the	O
early	O
summer	O
of	O
268	O
,	O
the	O
Emperor	O
Gallienus	O
halted	O
their	O
advance	O
into	O
Italy	O
,	O
but	O
then	O
had	O
to	O
deal	O
with	O
the	O
Goths	O
.	O
After	O
efforts	O
to	O
secure	O
a	O
peaceful	O
withdrawal	O
failed	O
,	O
Claudius	PERSON
forced	O
the	O
Alamanni	O
to	O
battle	O
at	O
the	O
Battle	O
of	O
Lake	O
Benacus	O
in	O
November	O
.	O
In	O
the	O
great	O
mixed	O
invasion	O
of	O
406	O
,	O
the	O
Alamanni	O
appear	O
to	O
have	O
crossed	O
the	O
Rhine	O
river	O
a	O
final	O
time	O
,	O
conquering	O
and	O
then	O
settling	O
what	O
is	O
today	O
Alsace	O
and	O
a	O
large	O
part	O
of	O
the	O
Swiss	O
Plateau	O
.	O
In	O
746	O
,	O
Carloman	O
ended	O
an	O
uprising	O
by	O
summarily	O
executing	O
all	O
Alemannic	O
nobility	O
at	O
the	O
blood	O
court	O
at	O
Cannstatt	LOCATION
,	O
and	O
for	O
the	O
following	O
century	O
,	O
Alamannia	O
was	O
ruled	O
by	O
Frankish	O
dukes	O
.	O
Agathias	O
expresses	O
his	O
hope	O
that	O
the	O
Alamanni	O
would	O
assume	O
better	O
manners	O
through	O
prolongued	O
contact	O
with	O
the	O
Franks	O
,	O
which	O
is	O
by	O
all	O
appearances	O
what	O
eventually	O
happened	O
.	O
The	O
establishment	O
of	O
the	O
bishopric	O
of	O
Konstanz	O
can	O
not	O
be	O
dated	O
exactly	O
and	O
was	O
possibly	O
undertaken	O
by	O
Columbanus	O
himself	O
(	O
before	O
612	O
)	O
.	O
