Being	O
located	O
where	O
the	O
Swiss	O
,	O
French	O
and	O
German	O
borders	O
meet	O
,	O
Basel	O
also	O
has	O
suburbs	O
in	O
France	O
and	O
Germany	O
.	O
With	O
830,000	O
inhabitants	O
in	O
the	O
tri-national	O
metropolitan	O
area	O
as	O
of	O
2004	O
,	O
Basel	O
is	O
Switzerland	O
's	O
second-largest	O
urban	O
area	O
.	O
Located	O
in	O
northwest	O
Switzerland	O
on	O
the	O
river	O
Rhine	O
,	O
Basel	O
functions	O
as	O
a	O
major	O
industrial	O
centre	O
for	O
the	O
chemical	O
and	O
pharmaceutical	O
industry	O
.	O
Basel	O
is	O
German-speaking	O
.	O
Basel	O
is	O
among	O
the	O
most	O
important	O
cultural	O
centres	O
of	O
Switzerland	O
.	O
During	O
the	O
days	O
of	O
the	O
Roman	O
Empire	O
,	O
the	O
settlement	O
of	O
Augusta	O
Raurica	O
was	O
founded	O
10	O
or	O
20	O
kilometres	O
upstream	O
of	O
present	O
Basel	LOCATION
,	O
and	O
a	O
castle	O
was	O
built	O
on	O
the	O
hill	O
overlooking	O
the	O
river	O
where	O
the	O
Basel	O
Münster	O
now	O
stands	O
.	O
The	O
city	O
's	O
position	O
on	O
the	O
Rhine	O
long	O
emphasised	O
its	O
importance	O
:	O
Basel	LOCATION
for	O
many	O
centuries	O
possessed	O
the	O
only	O
bridge	O
over	O
the	O
river	O
"	O
between	O
Lake	O
Constance	O
and	O
the	O
sea	O
"	O
[	O
citation	O
needed	O
]	O
.	O
At	O
the	O
same	O
time	O
the	O
new	O
craft	O
of	O
printing	O
was	O
introduced	O
to	O
Basel	LOCATION
by	O
apprentices	O
of	O
Johann	PERSON
Gutenberg	PERSON
.	O
Johann	PERSON
Froben	PERSON
also	O
operated	O
his	O
printing	O
house	O
in	O
Basel	LOCATION
and	O
was	O
notable	O
for	O
publishing	O
works	O
by	O
Erasmus	O
.	O
In	O
1500	O
the	O
construction	O
of	O
the	O
Basel	O
Münster	O
was	O
finished	O
.	O
The	O
first	O
edition	O
of	O
Christianae	O
religionis	O
institutio	O
was	O
published	O
at	O
Basel	O
in	O
March	O
1536	O
.	O
In	O
1543	O
De	O
humani	O
corporis	O
fabrica	O
,	O
the	O
first	O
book	O
on	O
human	O
anatomy	O
,	O
was	O
published	O
and	O
printed	O
in	O
Basel	O
by	O
Andreas	PERSON
Vesalius	PERSON
(	O
1514	O
--	O
1564	O
)	O
.	O
On	O
July	O
3	O
,	O
1874	O
Switzerland	O
's	O
first	O
zoo	O
(	O
the	O
Zoo	O
Basel	O
)	O
opened	O
its	O
doors	O
in	O
the	O
south	O
of	O
the	O
city	O
towards	O
Binningen	LOCATION
.	O
Basel	O
has	O
often	O
been	O
the	O
site	O
of	O
peace	O
negotiations	O
and	O
other	O
international	O
meetings	O
.	O
The	O
Treaty	O
of	O
Basel	O
(	O
1499	O
)	O
ended	O
the	O
Swabian	O
War	O
.	O
Two	O
years	O
later	O
Basel	O
joined	O
the	O
Swiss	O
Confederation	O
.	O
In	O
more	O
recent	O
times	O
,	O
the	O
World	O
Zionist	O
Organization	O
held	O
its	O
first	O
congress	O
in	O
Basel	LOCATION
on	O
September	O
3	O
,	O
1897	O
.	O
Because	O
of	O
the	O
Balkan	O
Wars	O
,	O
the	O
Second	O
International	O
held	O
an	O
extraordinary	O
congress	O
at	O
Basel	O
in	O
1912	O
.	O
The	O
first-class	O
location	O
and	O
the	O
transportation	O
infrastructure	O
make	O
Basel	O
the	O
top	O
logistics	O
center	O
for	O
Switzerland	O
.	O
The	O
outstanding	O
location	O
benefits	O
logistics	O
corporations	O
,	O
which	O
operate	O
globally	O
from	O
Basel	LOCATION
.	O
Basel	LOCATION
has	O
Switzerland	O
's	O
only	O
cargo	O
port	O
,	O
through	O
which	O
goods	O
pass	O
along	O
the	O
navigable	O
stretches	O
of	O
the	O
Rhine	O
and	O
connect	O
to	O
ocean-going	O
ships	O
at	O
the	O
port	O
of	O
Rotterdam	LOCATION
.	O
EuroAirport	O
Basel-Mulhouse-Freiburg	LOCATION
is	O
operated	O
jointly	O
by	O
two	O
countries	O
,	O
France	O
and	O
Switzerland	O
.	O
Contrary	O
to	O
popular	O
belief	O
,	O
the	O
airport	O
is	O
located	O
completely	O
on	O
French	O
soil	O
.	O
The	O
airport	O
itself	O
is	O
split	O
into	O
two	O
architecturally	O
independent	O
sectors	O
,	O
one	O
half	O
serving	O
the	O
French	O
side	O
and	O
the	O
other	O
half	O
serving	O
the	O
Swiss	O
side	O
;	O
there	O
was	O
a	O
customs	O
point	O
at	O
the	O
middle	O
of	O
the	O
airport	O
so	O
that	O
people	O
could	O
"	O
emigrate	O
"	O
to	O
the	O
other	O
side	O
of	O
the	O
airport	O
.	O
Basel	O
has	O
long	O
held	O
an	O
important	O
place	O
as	O
a	O
rail	O
hub	O
.	O
Basel	O
Badischer	O
Bahnhof	O
is	O
on	O
the	O
opposite	O
side	O
of	O
the	O
city	O
.	O
Basel	O
's	O
local	O
rail	O
services	O
are	O
supplied	O
by	O
the	O
Basel	O
Regional	O
S-Bahn	O
.	O
The	O
largest	O
goods	O
railway	O
complex	O
of	O
the	O
country	O
[	O
citation	O
needed	O
]	O
is	O
located	O
just	O
outside	O
the	O
city	O
,	O
spanning	O
the	O
municipalities	O
of	O
Muttenz	LOCATION
and	LOCATION
Pratteln	LOCATION
.	O
Within	O
the	O
city	O
limits	O
,	O
five	O
bridges	O
connect	O
greater	O
and	O
lesser	O
Basel	O
,	O
from	O
upstream	O
to	O
downstream	O
:	O
Basel	O
has	O
an	O
extensive	O
public	O
transportation	O
network	O
serving	O
the	O
city	O
and	O
connecting	O
to	O
surrounding	O
suburbs	O
,	O
including	O
a	O
large	O
tram	O
network	O
.	O
The	O
yellow-colored	O
buses	O
and	O
trams	O
are	O
operated	O
by	O
the	O
BLT	O
Baselland	O
Transport	O
,	O
and	O
connect	O
areas	O
in	O
the	O
nearby	O
half-canton	O
of	O
Baselland	O
to	O
central	O
Basel	LOCATION
.	O
Basel	O
is	O
located	O
at	O
the	O
meeting	O
point	O
of	O
France	O
,	O
Germany	O
and	O
Switzerland	O
and	O
has	O
numerous	O
road	O
and	O
rail	O
crossings	O
between	O
Switzerland	O
and	O
the	O
other	O
two	O
countries	O
.	O
However	O
,	O
Switzerland	O
did	O
not	O
join	O
the	O
EU	O
customs	O
regime	O
and	O
customs	O
checks	O
are	O
still	O
conducted	O
at	O
or	O
near	O
the	O
crossings	O
.	O
France-Switzerland	O
(	O
from	O
east	O
to	O
west	O
)	O
Germany-Switzerland	O
(	O
clockwise	O
,	O
from	O
north	O
to	O
south	O
)	O
Additionally	O
there	O
are	O
many	O
footpaths	O
and	O
cycle	O
tracks	O
crossing	O
the	O
border	O
between	O
Basel	LOCATION
and	O
Germany	LOCATION
.	O
Basel	O
is	O
located	O
on	O
the	O
A3	O
motorway	O
.	O
The	O
Swiss	O
chemical	O
industry	O
operates	O
largely	O
from	O
Basel	LOCATION
,	O
and	O
Basel	O
also	O
has	O
a	O
large	O
pharmaceutical	O
industry	O
.	O
Novartis	O
,	O
Syngenta	O
,	O
Ciba	O
Specialty	O
Chemicals	O
,	O
Clariant	O
,	O
Hoffmann-La	O
Roche	O
,	O
and	O
Basilea	O
Pharmaceutica	O
headquartered	O
there	O
.	O
Banking	O
is	O
extremely	O
important	O
to	O
Basel	LOCATION
:	O
Basel	O
has	O
Switzerland	O
's	O
second	O
tallest	O
building	O
(	O
Basler	O
Messeturm	O
/	O
105m	O
)	O
and	O
Switzerland	O
's	O
tallest	O
tower	O
(	O
St.	O
Chrischona	O
TV	O
tower	O
/	O
250m	O
)	O
.	O
Swiss	O
International	O
Air	O
Lines	O
,	O
the	O
national	O
airline	O
of	O
Switzerland	O
,	O
is	O
headquartered	O
on	O
the	O
grounds	O
of	O
EuroAirport	O
Basel-Mulhouse-Freiburg	O
in	O
Saint-Louis	LOCATION
,	LOCATION
Haut-Rhin	LOCATION
,	O
France	O
,	O
near	O
Basel	LOCATION
.	O
Prior	O
to	O
the	O
formation	O
of	O
Swiss	O
International	O
Air	O
Lines	O
,	O
the	O
regional	O
airline	O
Crossair	O
was	O
headquartered	O
near	O
Basel	LOCATION
.	O
Basel	O
is	O
subdivided	O
into	O
19	O
quarters	O
.	O
The	O
municipalities	O
of	O
Riehen	O
and	O
Bettingen	O
,	O
outside	O
the	O
city	O
limits	O
of	O
Basel	LOCATION
,	O
are	O
included	O
in	O
the	O
canton	O
of	O
Basel-City	LOCATION
as	O
rural	O
quarters	O
.	O
Basel	O
is	O
also	O
host	O
to	O
an	O
array	O
of	O
buildings	O
by	O
internationally	O
renowned	O
architects	O
.	O
These	O
include	O
the	O
Beyeler	O
Foundation	O
by	O
Renzo	PERSON
Piano	PERSON
,	O
or	O
the	O
Vitra	O
complex	O
in	O
nearby	O
Weil	LOCATION
am	LOCATION
Rhein	LOCATION
,	O
composed	O
of	O
buildings	O
by	O
architects	O
such	O
as	O
Zaha	PERSON
Hadid	PERSON
(	O
fire	O
station	O
)	O
,	O
Frank	PERSON
Gehry	PERSON
,	O
Alvaro	PERSON
Siza	PERSON
Vieira	PERSON
(	O
factory	O
building	O
)	O
and	O
Tadao	PERSON
Ando	PERSON
(	O
conference	O
centre	O
)	O
.	O
Basel	O
also	O
features	O
buildings	O
by	O
Mario	PERSON
Botta	PERSON
and	O
Herzog	PERSON
&	PERSON
de	PERSON
Meuron	PERSON
.	O
Basel	O
features	O
a	O
great	O
number	O
of	O
heritage	O
sites	O
of	O
national	O
significance	O
.	O
Basel	O
hosts	O
Switzerland	O
's	O
oldest	O
university	O
,	O
the	O
University	O
of	O
Basel	O
,	O
dating	O
from	O
1459	O
.	O
Erasmus	O
,	O
Paracelsus	O
,	O
Daniel	PERSON
Bernoulli	PERSON
,	O
Leonhard	PERSON
Euler	PERSON
,	O
Jacob	PERSON
Burckhardt	PERSON
,	O
and	O
Friedrich	PERSON
Nietzsche	PERSON
worked	O
here	O
.	O
Basel	O
is	O
at	O
the	O
forefront	O
of	O
a	O
national	O
vision	O
to	O
more	O
than	O
halve	O
energy	O
use	O
in	O
Switzerland	O
by	O
2050	O
.	O
In	O
order	O
to	O
research	O
,	O
develop	O
and	O
commercialise	O
the	O
technologies	O
and	O
techniques	O
required	O
for	O
the	O
country	O
to	O
become	O
a	O
'	O
2000	O
Watt	O
society	O
'	O
,	O
a	O
number	O
of	O
projects	O
have	O
been	O
set	O
up	O
since	O
2001	O
in	O
the	O
Basel	O
metropolitan	O
area	O
.	O
Basel	O
has	O
a	O
reputation	O
in	O
Switzerland	O
as	O
a	O
successful	O
sporting	O
city	O
.	O
The	O
football	O
club	O
FC	O
Basel	LOCATION
continues	O
to	O
be	O
successful	O
and	O
in	O
recognition	O
of	O
this	O
the	O
city	O
was	O
one	O
of	O
the	O
Swiss	O
venues	O
for	O
the	O
2008	O
European	O
Championships	O
,	O
as	O
well	O
as	O
Geneva	O
,	O
Zürich	O
and	O
Bern	O
.	O
The	O
championships	O
were	O
jointly	O
hosted	O
by	O
Switzerland	O
and	O
Austria	O
.	O
BSC	O
Old	O
Boys	O
and	O
Concordia	O
Basel	O
are	O
the	O
other	O
football	O
teams	O
in	O
Basel	LOCATION
.	O
A	O
large	O
indoor	O
tennis	O
event	O
takes	O
place	O
in	O
Basel	LOCATION
every	O
October	O
.	O
Some	O
of	O
the	O
best	O
ATP-Professionals	O
play	O
every	O
year	O
at	O
the	O
Swiss	O
Indoors	O
,	O
including	O
Switzerland	O
's	O
biggest	O
sporting	O
hero	O
and	O
frequent	O
participant	O
Roger	PERSON
Federer	PERSON
,	O
a	O
Basel	O
native	O
who	O
describes	O
the	O
city	O
as	O
"	O
one	O
of	O
the	O
most	O
beautiful	O
cities	O
in	O
the	O
world	O
"	O
.	O
Theater	PERSON
Basel	PERSON
presents	O
a	O
busy	O
schedule	O
of	O
plays	O
in	O
addition	O
to	O
being	O
home	O
to	O
the	O
city	O
's	O
opera	O
and	O
ballet	O
companies	O
.	O
In	O
May	O
2004	O
,	O
the	O
fifth	O
European	O
Festival	O
of	O
Youth	O
Choirs	O
choir	O
festival	O
opened	O
:	O
this	O
Basel	O
tradition	O
started	O
in	O
1992	O
.	O
The	O
carnival	O
of	O
the	O
city	O
of	O
Basel	LOCATION
is	O
a	O
major	O
cultural	O
event	O
in	O
the	O
year	O
.	O
The	O
carnival	O
is	O
the	O
biggest	O
in	O
Switzerland	O
and	O
attracts	O
large	O
crowds	O
every	O
year	O
,	O
despite	O
the	O
fact	O
that	O
it	O
starts	O
at	O
four	O
in	O
the	O
morning	O
and	O
lasts	O
for	O
exactly	O
72	O
hours	O
,	O
taking	O
in	O
various	O
parades	O
.	O
Basler	O
Zeitung	O
is	O
the	O
local	O
newspaper	O
.	O
Zoo	O
Basel	O
is	O
Switzerland	O
's	O
oldest	O
and	O
,	O
by	O
animals	O
,	O
largest	O
zoo	O
.	O
With	O
over	O
1.6	O
million	O
visitors	O
per	O
year	O
,	O
it	O
is	O
Basel	O
's	O
most	O
visited	O
tourist	O
attaction	O
with	O
an	O
entrance	O
fee	O
.	O
Basel	O
is	O
host	O
to	O
the	O
Basel	O
Tattoo	O
,	O
started	O
by	O
the	O
Top	O
Secret	O
Drum	O
Corps	O
.	O
The	O
Basel	O
museums	O
cover	O
a	O
broad	O
and	O
diverse	O
spectrum	O
of	O
collections	O
with	O
a	O
marked	O
concentration	O
in	O
the	O
fine	O
arts	O
.	O
