The	O
incident	O
occurred	O
during	O
a	O
Northern	O
Ireland	O
Civil	O
Rights	O
Association	O
march	O
;	O
the	O
soldiers	O
involved	O
were	O
the	O
First	O
Battalion	O
of	O
the	O
Parachute	O
Regiment	O
(	O
1	O
Para	O
)	O
.	O
Bloody	O
Sunday	O
remains	O
among	O
the	O
most	O
significant	O
events	O
in	O
the	O
Troubles	O
of	O
Northern	O
Ireland	O
,	O
chiefly	O
because	O
it	O
was	O
carried	O
out	O
by	O
the	O
army	O
and	O
not	O
paramilitaries	O
,	O
in	O
full	O
view	O
of	O
the	O
public	O
and	O
the	O
press	O
.	O
The	O
conflict	O
known	O
as	O
the	O
Troubles	O
began	O
in	O
Derry	O
in	O
1968-1969	O
,	O
with	O
confrontations	O
between	O
nationalist	O
demonstrators	O
and	O
the	O
Royal	O
Ulster	O
Constabulary	O
(	O
RUC	O
)	O
.	O
The	O
RUC	O
broke	O
up	O
several	O
illegal	O
marches	O
of	O
the	O
Northern	O
Ireland	O
Civil	O
Rights	O
Association	O
,	O
who	O
were	O
demonstrating	O
against	O
discrimination	O
against	O
Catholics	O
in	O
electoral	O
boundaries	O
,	O
voting	O
rights	O
and	O
allocation	O
of	O
public	O
housing	O
.	O
The	O
rioting	O
escalated	O
beyond	O
the	O
control	O
of	O
the	O
RUC	O
in	O
August	O
1969	O
,	O
in	O
the	O
Battle	O
of	O
the	O
Bogside	O
.	O
This	O
was	O
huge	O
riot	O
in	O
which	O
,	O
after	O
disturbances	O
broke	O
after	O
an	O
Apprentice	O
Boys	O
march	O
,	O
the	O
residents	O
of	O
the	O
nationalist	O
Bogside	O
erected	O
barricades	O
around	O
the	O
area	O
to	O
resist	O
police	O
incursions	O
.	O
Initially	O
they	O
were	O
welcomed	O
by	O
the	O
Catholics	O
as	O
a	O
neutral	O
force	O
compared	O
to	O
the	O
RUC	O
.	O
A	O
British	O
Army	O
memorandum	O
states	O
that	O
as	O
a	O
result	O
of	O
this	O
the	O
situation	O
"	O
changed	O
overnight	O
"	O
,	O
with	O
the	O
Provisional	O
IRA	O
's	O
campaign	O
in	O
the	O
city	O
beginning	O
at	O
that	O
time	O
after	O
previously	O
being	O
regarded	O
as	O
"	O
quiescent	O
"	O
.	O
In	O
response	O
to	O
escalating	O
levels	O
of	O
violence	O
across	O
Northern	O
Ireland	O
,	O
internment	O
without	O
trial	O
was	O
introduced	O
on	O
9	O
August	O
1971	O
.	O
There	O
was	O
disorder	O
across	O
Northern	O
Ireland	O
following	O
the	O
introduction	O
of	O
internment	O
,	O
with	O
21	O
people	O
being	O
killed	O
in	O
three	O
days	O
of	O
rioting	O
.	O
A	O
further	O
six	O
soldiers	O
had	O
been	O
killed	O
in	O
Derry	LOCATION
by	O
mid-December	O
1971	O
.	O
1,932	O
rounds	O
were	O
fired	O
at	O
the	O
British	O
Army	O
,	O
who	O
also	O
faced	O
211	O
explosions	O
and	O
180	O
nail	O
bombs	O
and	O
who	O
fired	O
364	O
rounds	O
in	O
return	O
.	O
Both	O
the	O
Official	O
IRA	O
and	O
Provisional	O
IRA	O
had	O
established	O
"	O
no-go	O
"	O
areas	O
for	O
the	O
British	O
Army	O
and	O
RUC	O
in	O
Derry	LOCATION
through	O
the	O
use	O
of	O
barricades	O
.	O
By	O
the	O
end	O
of	O
1971	O
,	O
29	O
barricades	O
were	O
in	O
place	O
to	O
prevent	O
access	O
to	O
what	O
was	O
known	O
as	O
Free	O
Derry	O
,	O
16	O
of	O
them	O
impassable	O
even	O
to	O
the	O
British	O
Army	O
's	O
one-ton	O
armoured	O
vehicles	O
.	O
In	O
January	O
1972	O
the	O
Northern	O
Ireland	O
Civil	O
Rights	O
Association	O
intended	O
,	O
despite	O
the	O
ban	O
,	O
to	O
organise	O
a	O
march	O
in	O
Derry	O
to	O
protest	O
against	O
internment	O
.	O
A	O
group	O
of	O
teenagers	O
broke	O
off	O
from	O
the	O
march	O
and	O
persisted	O
in	O
pushing	O
the	O
barricade	O
and	O
marching	O
on	O
the	O
Guildhall	O
.	O
They	O
attacked	O
the	O
British	O
army	O
barricade	O
with	O
stones	O
.	O
He	O
was	O
running	O
alongside	O
a	O
priest	O
,	O
Father	O
Edward	PERSON
Daly	PERSON
,	O
when	O
he	O
was	O
shot	O
in	O
the	O
back	O
.	O
Continuing	O
violence	O
by	O
British	O
troops	O
escalated	O
,	O
and	O
eventually	O
the	O
order	O
was	O
given	O
to	O
mobilise	O
the	O
troops	O
in	O
an	O
arrest	O
operation	O
,	O
chasing	O
the	O
tail	O
of	O
the	O
main	O
group	O
of	O
marchers	O
to	O
the	O
edge	O
of	O
the	O
field	O
by	O
Free	O
Derry	O
Corner	PERSON
.	O
In	O
the	O
event	O
,	O
one	O
man	O
was	O
witnessed	O
by	O
Father	O
Edward	PERSON
Daly	PERSON
and	O
others	O
haphazardly	O
firing	O
a	O
revolver	O
in	O
the	O
direction	O
of	O
the	O
paratroopers	O
.	O
Later	O
identified	O
as	O
a	O
member	O
of	O
the	O
Official	O
IRA	O
,	O
this	O
man	O
was	O
also	O
photographed	O
in	O
the	O
act	O
of	O
drawing	O
his	O
weapon	O
,	O
but	O
was	O
apparently	O
not	O
seen	O
or	O
targeted	O
by	O
the	O
soldiers	O
.	O
Various	O
other	O
claims	O
have	O
been	O
made	O
to	O
the	O
Saville	ORGANIZATION
Inquiry	ORGANIZATION
about	O
gunmen	O
on	O
the	O
day	O
.	O
Many	O
witnesses	O
intended	O
to	O
boycott	O
the	O
tribunal	O
as	O
they	O
lacked	O
faith	O
in	O
Widgery	O
's	O
impartiality	O
,	O
but	O
were	O
eventually	O
persuaded	O
to	O
take	O
part	O
.	O
In	O
fact	O
,	O
in	O
1992	O
,	O
John	PERSON
Major	PERSON
,	O
writing	O
to	O
John	PERSON
Hume	PERSON
stated	O
:	O
On	O
29	O
May	O
2007	O
it	O
was	O
reported	O
that	O
General	O
Sir	O
Mike	PERSON
Jackson	PERSON
,	O
second-in-command	O
of	O
1	O
Para	O
on	O
Bloody	O
Sunday	O
,	O
said	O
:	O
"	O
I	O
have	O
no	O
doubt	O
that	O
innocent	O
people	O
were	O
shot	O
"	O
.	O
A	O
second	O
commission	O
of	O
inquiry	O
,	O
chaired	O
by	O
Lord	O
Saville	PERSON
,	O
was	O
established	O
in	O
January	O
1998	O
to	O
re-examine	O
'	O
Bloody	O
Sunday	O
'	O
.	O
A	O
claim	O
was	O
made	O
at	O
the	O
Saville	O
Inquiry	O
that	O
McGuinness	O
was	O
responsible	O
for	O
supplying	O
detonators	O
for	O
nail	O
bombs	O
on	O
Bloody	O
Sunday	O
.	O
Many	O
observers	O
allege	O
that	O
the	O
Ministry	O
of	O
Defence	O
acted	O
in	O
a	O
way	O
to	O
impede	O
the	O
inquiry	O
.	O
By	O
the	O
time	O
the	O
inquiry	O
had	O
retired	O
to	O
write	O
up	O
its	O
findings	O
,	O
it	O
had	O
interviewed	O
over	O
900	O
witnesses	O
,	O
over	O
seven	O
years	O
,	O
making	O
it	O
the	O
biggest	O
investigation	O
in	O
British	O
legal	O
history	O
.	O
The	O
cost	O
of	O
this	O
process	O
has	O
drawn	O
criticism	O
;	O
as	O
of	O
the	O
publication	O
of	O
the	O
Saville	O
Report	O
being	O
£	O
195	O
million	O
.	O
The	O
report	O
concluded	O
,	O
"	O
The	O
firing	O
by	O
soldiers	O
of	O
1	O
PARA	O
on	O
Bloody	O
Sunday	O
caused	O
the	O
deaths	O
of	O
13	O
people	O
and	O
injury	O
to	O
a	O
similar	O
number	O
,	O
none	O
of	O
whom	O
was	O
posing	O
a	O
threat	O
of	O
causing	O
death	O
or	O
serious	O
injury	O
.	O
"	O
Ultimately	O
,	O
the	O
Saville	O
Inquiry	O
was	O
inconclusive	O
on	O
Martin	PERSON
McGuiness	PERSON
'	O
role	O
due	O
to	O
a	O
lack	O
of	O
certainty	O
over	O
his	O
movements	O
,	O
concluding	O
that	O
while	O
he	O
was	O
"	O
engaged	O
in	O
paramilitary	O
activity	O
"	O
during	O
Bloody	O
Sunday	O
,	O
and	O
had	O
probably	O
been	O
armed	O
with	O
a	O
Thompson	O
submachine	O
gun	O
,	O
there	O
was	O
insufficient	O
evidence	O
to	O
make	O
any	O
finding	O
other	O
than	O
they	O
were	O
"	O
sure	O
that	O
he	O
did	O
not	O
engage	O
in	O
any	O
activity	O
that	O
provided	O
any	O
of	O
the	O
soldiers	O
with	O
any	O
justification	O
for	O
opening	O
fire	O
"	O
.	O
Bloody	O
Sunday	O
marked	O
a	O
major	O
negative	O
turning	O
point	O
in	O
the	O
fortunes	O
of	O
Northern	O
Ireland	O
.	O
After	O
Bloody	O
Sunday	O
many	O
Catholics	O
turned	O
on	O
the	O
British	O
army	O
,	O
seeing	O
it	O
no	O
longer	O
as	O
their	O
protector	O
but	O
as	O
their	O
enemy	O
.	O
But	O
you	O
do	O
not	O
defend	O
the	O
British	O
Army	O
by	O
defending	O
the	O
indefensible	O
.	O
"	O
In	O
Harper	O
's	O
book	O
,	O
his	O
comment	O
on	O
the	O
song	O
ends	O
'	O
..there	O
must	O
always	O
be	O
some	O
hope	O
that	O
the	O
children	O
of	O
'	O
Bloody	O
Sunday	O
'	O
,	O
on	O
both	O
sides	O
,	O
can	O
grow	O
into	O
some	O
wisdom	O
'	O
.	O
Paul	PERSON
McCartney	PERSON
issued	O
a	O
single	O
shortly	O
after	O
Bloody	O
Sunday	O
titled	O
"	O
Give	O
Ireland	O
Back	O
to	O
the	O
Irish	O
"	O
,	O
expressing	O
his	O
views	O
on	O
the	O
matter	O
.	O
It	O
was	O
one	O
of	O
few	O
McCartney	PERSON
solo	O
songs	O
to	O
be	O
banned	O
by	O
the	O
BBC	O
.	O
The	O
events	O
of	O
the	O
day	O
have	O
been	O
dramatised	O
in	O
the	O
two	O
2002	O
television	O
dramas	O
,	O
Bloody	O
Sunday	O
(	O
starring	O
James	O
Nesbitt	O
)	O
and	O
Sunday	O
by	O
Jimmy	PERSON
McGovern	PERSON
.	O
Brian	PERSON
Friel	PERSON
's	O
1973	O
play	O
The	O
Freedom	O
of	O
the	O
City	O
deals	O
with	O
the	O
incident	O
from	O
the	O
viewpoint	O
of	O
three	O
civilians	O
.	O
Willie	PERSON
Doherty	PERSON
,	O
a	O
Derry-born	O
artist	O
has	O
amassed	O
a	O
large	O
body	O
of	O
work	O
which	O
addresses	O
the	O
troubles	O
in	O
Northern	O
Ireland	O
.	O
"	O
30	O
January	O
1972	O
"	O
deals	O
specifically	O
with	O
the	O
events	O
of	O
Bloody	O
Sunday	O
.	O
