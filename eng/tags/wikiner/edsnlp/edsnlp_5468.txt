Harlan	PERSON
Jay	PERSON
Ellison	PERSON
(	O
born	O
May	O
27	O
,	O
1934	O
)	O
is	O
an	O
American	O
writer	O
.	O
He	O
was	O
editor	O
and	O
anthologist	O
for	O
two	O
ground-breaking	O
science	O
fiction	O
anthologies	O
,	O
Dangerous	O
Visions	O
and	O
Again	O
,	O
Dangerous	O
Visions	O
.	O
Ellison	PERSON
has	O
won	O
numerous	O
awards	O
--	O
more	O
awards	O
for	O
imaginative	O
literature	O
than	O
any	O
other	O
living	O
author	O
--	O
including	O
multiple	O
Hugos	O
,	O
Nebulas	O
and	O
Edgars	PERSON
.	O
Ellison	PERSON
was	O
born	O
in	O
Cleveland	LOCATION
,	O
Ohio	O
,	O
on	O
May	O
27	O
,	O
1934	O
.	O
He	O
frequently	O
ran	O
away	O
from	O
home	O
,	O
taking	O
an	O
array	O
of	O
odd	O
jobs	O
--	O
including	O
,	O
by	O
age	O
eighteen	O
,	O
"	O
tuna	O
fisherman	O
off	O
the	O
coast	O
of	O
Galveston	O
,	O
itinerant	O
crop-picker	O
down	O
in	O
New	LOCATION
Orleans	LOCATION
,	O
hired	O
gun	O
for	O
a	O
wealthy	O
neurotic	O
,	O
nitroglycerine	O
truck	O
driver	O
in	O
North	O
Carolina	O
,	O
short	O
order	O
cook	O
,	O
cab	O
driver	O
,	O
lithographer	O
,	O
book	O
salesman	O
,	O
floorwalker	O
in	O
a	O
department	O
store	O
,	O
door-to-door	O
brush	O
salesman	O
,	O
and	O
as	O
a	O
youngster	O
,	O
an	O
actor	O
in	O
several	O
productions	O
at	O
the	O
Cleveland	ORGANIZATION
Play	ORGANIZATION
House	ORGANIZATION
"	O
.	O
Ellison	PERSON
attended	O
Ohio	O
State	O
University	O
for	O
18	O
months	O
(	O
1951	O
--	O
53	O
)	O
before	O
being	O
expelled	O
.	O
Ellison	PERSON
moved	O
to	O
New	LOCATION
York	LOCATION
City	LOCATION
in	O
1955	O
to	O
pursue	O
a	O
writing	O
career	O
,	O
primarily	O
in	O
science	O
fiction	O
.	O
In	O
1957	O
,	O
Ellison	PERSON
decided	O
to	O
write	O
about	O
youth	O
gangs	O
.	O
His	O
subsequent	O
writings	O
on	O
the	O
subject	O
include	O
the	O
novel	O
,	O
Web	O
of	O
the	O
City/Rumble	O
,	O
and	O
the	O
collection	O
,	O
The	O
Deadly	O
Streets	O
,	O
and	O
also	O
compose	O
part	O
of	O
his	O
memoir	O
,	O
Memos	O
from	O
Purgatory	O
.	O
In	O
1960	O
,	O
he	O
returned	O
to	O
N.Y.	LOCATION
,	O
living	O
at	O
95	LOCATION
Christopher	LOCATION
Street	LOCATION
in	O
Greenwich	LOCATION
Village	LOCATION
.	O
Moving	O
to	O
Chicago	LOCATION
,	O
Ellison	PERSON
wrote	O
for	O
William	PERSON
Hamling	PERSON
's	O
Rogue	O
magazine	O
.	O
Ellison	PERSON
was	O
fired	O
on	O
his	O
first	O
day	O
of	O
work	O
as	O
a	O
writer	O
at	O
Disney	O
Studios	O
for	O
jokingly	O
suggesting	O
they	O
make	O
a	O
"	O
Disney	O
porn-flick	O
"	O
.	O
Ellison	PERSON
moved	O
to	O
California	O
in	O
1962	O
,	O
and	O
subsequently	O
began	O
to	O
sell	O
his	O
writing	O
to	O
Hollywood	O
.	O
Ellison	O
also	O
sold	O
scripts	O
to	O
many	O
television	O
shows	O
:	O
The	O
Flying	O
Nun	O
,	O
Burke	O
's	O
Law	O
,	O
Route	O
66	O
,	O
The	O
Outer	O
Limits	O
,	O
Star	O
Trek	O
,	O
The	O
Man	O
from	O
U.N.C.L.E.	O
,	O
Cimarron	O
Strip	O
and	O
The	O
Alfred	O
Hitchcock	PERSON
Hour	O
.	O
During	O
the	O
late	O
1960s	O
,	O
Ellison	O
wrote	O
a	O
column	O
about	O
television	O
for	O
the	O
Los	O
Angeles	O
Free	O
Press	O
.	O
Titled	O
"	O
The	O
Glass	O
Teat	O
"	O
,	O
the	O
column	O
addressed	O
political	O
and	O
social	O
issues	O
and	O
their	O
portrayal	O
on	O
television	O
at	O
the	O
time	O
.	O
In	O
1966	O
,	O
in	O
an	O
article	O
that	O
Esquire	O
magazine	O
would	O
later	O
name	O
as	O
the	O
best	O
magazine	O
piece	O
ever	O
written	O
,	O
the	O
journalist	O
Gay	PERSON
Talese	PERSON
wrote	O
about	O
the	O
goings-on	O
around	O
the	O
enigmatic	O
Frank	PERSON
Sinatra	PERSON
.	O
The	O
article	O
,	O
entitled	O
"	O
Frank	PERSON
Sinatra	PERSON
Has	O
a	O
Cold	O
"	O
,	O
briefly	O
describes	O
a	O
clash	O
between	O
the	O
young	O
Harlan	PERSON
Ellison	PERSON
and	O
Frank	PERSON
Sinatra	PERSON
,	O
when	O
the	O
crooner	O
took	O
exception	O
to	O
Ellison	O
's	O
boots	O
during	O
a	O
billiards	O
game	O
.	O
Ellison	O
continued	O
to	O
publish	O
short	O
fiction	O
and	O
nonfiction	O
pieces	O
in	O
various	O
publications	O
,	O
including	O
some	O
of	O
his	O
best	O
known	O
stories	O
.	O
"	O
'	O
Repent	O
,	O
Harlequin	O
!	O
'	O
Said	O
the	O
Ticktockman	O
"	O
(	O
1965	O
)	O
is	O
a	O
celebration	O
of	O
civil	O
disobedience	O
against	O
repressive	O
authority	O
.	O
"	O
A	O
Boy	O
and	O
His	O
Dog	O
"	O
examines	O
the	O
nature	O
of	O
friendship	O
and	O
love	O
in	O
a	O
violent	O
,	O
post-apocalyptic	O
world	O
.	O
He	O
also	O
edited	O
the	O
influential	O
science	O
fiction	O
anthology	O
Dangerous	O
Visions	O
(	O
1967	O
)	O
,	O
which	O
collected	O
stories	O
commissioned	O
by	O
Ellison	PERSON
,	O
accompanied	O
by	O
his	O
commentary-laden	O
biographical	O
sketches	O
of	O
the	O
authors	O
.	O
As	O
an	O
editor	O
,	O
Ellison	O
was	O
influenced	O
and	O
inspired	O
by	O
experimentation	O
in	O
the	O
popular	O
literature	O
of	O
the	O
time	O
,	O
such	O
as	O
the	O
beats	O
.	O
A	O
sequel	O
,	O
Again	O
Dangerous	O
Visions	O
,	O
was	O
published	O
in	O
1972	O
.	O
A	O
third	O
volume	O
,	O
The	O
Last	O
Dangerous	O
Visions	O
,	O
has	O
been	O
repeatedly	O
postponed	O
.	O
Ellison	PERSON
served	O
as	O
creative	O
consultant	O
to	O
the	O
science	O
fiction	O
TV	O
series	O
The	O
Twilight	O
Zone	O
(	O
1980s	O
version	O
)	O
and	O
Babylon	O
5	O
.	O
Ellison	PERSON
has	O
commented	O
on	O
a	O
great	O
many	O
movies	O
and	O
television	O
programs	O
,	O
both	O
negatively	O
and	O
positively	O
.	O
For	O
two	O
years	O
,	O
beginning	O
in	O
1986	O
,	O
Ellison	PERSON
took	O
over	O
as	O
host	O
of	O
the	O
Friday-night	O
radio	O
program	O
,	O
Hour	O
25	O
on	O
Pacifica	O
Radio	O
station	O
KPFK-FM	O
,	O
Los	LOCATION
Angeles	LOCATION
,	O
after	O
the	O
death	O
of	O
Mike	PERSON
Hodel	PERSON
,	O
the	O
show	O
's	O
founder	O
and	O
original	O
host	O
.	O
Ellison	PERSON
had	O
been	O
a	O
frequent	O
and	O
favorite	O
guest	O
on	O
the	O
long-running	O
program	O
.	O
Ellison	PERSON
was	O
hired	O
as	O
a	O
writer	O
for	O
Walt	O
Disney	O
Studios	O
,	O
but	O
was	O
fired	O
on	O
his	O
first	O
day	O
after	O
being	O
overheard	O
by	O
Roy	PERSON
O.	PERSON
Disney	PERSON
in	O
the	O
studio	O
commissary	O
joking	O
about	O
making	O
a	O
pornographic	O
animated	O
film	O
featuring	O
Disney	O
characters	O
.	O
Ellison	PERSON
has	O
provided	O
vocal	O
narration	O
to	O
numerous	O
audiobooks	O
,	O
both	O
of	O
his	O
own	O
writing	O
and	O
others	O
.	O
Ellison	PERSON
has	O
helped	O
narrate	O
books	O
by	O
authors	O
such	O
as	O
Orson	O
Scott	O
Card	PERSON
,	O
Arthur	O
C.	O
Clarke	PERSON
,	O
Jack	PERSON
Williamson	PERSON
and	O
Terry	PERSON
Pratchett	PERSON
.	O
An	O
episode	O
of	O
Burke	O
's	O
Law	O
accredited	O
to	O
Ellison	PERSON
contains	O
a	O
character	O
given	O
this	O
name	O
.	O
Ellison	PERSON
has	O
said	O
,	O
in	O
interviews	O
and	O
in	O
his	O
writing	O
,	O
that	O
his	O
version	O
of	O
the	O
pseudonym	O
was	O
meant	O
to	O
mean	O
"	O
a	O
shoemaker	O
for	O
birds	O
"	O
.	O
Ellison	PERSON
has	O
a	O
reputation	O
for	O
being	O
abrasive	O
and	O
argumentative	O
.	O
Ellison	PERSON
has	O
filed	O
numerous	O
grievance	O
filings	O
and	O
lawsuit	O
attempts	O
that	O
have	O
been	O
characterized	O
as	O
both	O
justifiable	O
and	O
frivolous	O
.	O
His	O
friend	O
Isaac	PERSON
Asimov	PERSON
noted	O
,	O
"	O
Harlan	PERSON
uses	O
his	O
gifts	O
for	O
colorful	O
and	O
variegated	O
invective	O
on	O
those	O
who	O
irritate	O
him	O
--	O
intrusive	O
fans	O
,	O
obdurate	O
editors	O
,	O
callous	O
publishers	O
,	O
offensive	O
strangers	O
.	O
"	O
Another	O
friend	O
,	O
writer	O
Robert	PERSON
Bloch	PERSON
,	O
spoke	O
at	O
a	O
roast	O
for	O
Ellison	PERSON
,	O
saying	O
that	O
other	O
people	O
take	O
infinite	O
pains	O
;	O
"	O
Harlan	PERSON
gives	O
them	O
.	O
"	O
Ellison	PERSON
's	O
segments	O
were	O
broadcast	O
from	O
1994	O
to	O
1997	O
.	O
Ellison	O
has	O
repeatedly	O
criticized	O
how	O
Star	O
Trek	O
creator	O
and	O
producer	O
Gene	PERSON
Roddenberry	PERSON
(	O
and	O
others	O
)	O
rewrote	O
his	O
original	O
script	O
for	O
the	O
episode	O
"	O
The	O
City	O
on	O
the	O
Edge	O
of	O
Forever	O
"	O
.	O
Ellison	O
's	O
original	O
work	O
included	O
a	O
subplot	O
involving	O
drug	O
dealing	O
aboard	O
the	O
Enterprise	O
and	O
other	O
elements	O
that	O
Roddenberry	O
rejected	O
.	O
In	O
1995	O
,	O
White	O
Wolf	O
Publishing	O
released	O
Harlan	PERSON
Ellison	PERSON
's	O
The	O
City	O
on	O
the	O
Edge	O
of	O
Forever	O
,	O
a	O
book	O
that	O
included	O
the	O
original	O
script	O
,	O
several	O
story	O
treatments	O
,	O
and	O
a	O
long	O
introductory	O
essay	O
by	O
Ellison	O
explaining	O
his	O
position	O
on	O
what	O
he	O
called	O
a	O
"	O
fatally	O
inept	O
treatment	O
"	O
.	O
The	O
Last	O
Dangerous	O
Visions	O
,	O
the	O
third	O
volume	O
of	O
Ellison	O
's	O
anthology	O
series	O
,	O
has	O
become	O
science	O
fiction	O
's	O
most	O
famous	O
unpublished	O
book	O
.	O
Priest	O
claims	O
he	O
submitted	O
a	O
story	O
at	O
Ellison	PERSON
's	O
request	O
which	O
Ellison	PERSON
retained	O
for	O
several	O
months	O
until	O
Priest	O
withdrew	O
the	O
story	O
and	O
demanded	O
that	O
Ellison	PERSON
return	O
the	O
manuscript	O
.	O
Ellison	PERSON
has	O
a	O
record	O
of	O
fulfilling	O
obligations	O
in	O
other	O
instances	O
(	O
though	O
sometimes	O
,	O
as	O
with	O
Harlan	PERSON
Ellison	PERSON
's	O
Hornbook	O
for	O
Mirage	O
Press	O
,	O
several	O
decades	O
after	O
the	O
contract	O
was	O
signed	O
)	O
,	O
including	O
to	O
writers	O
whose	O
stories	O
he	O
solicited	O
.	O
Shortly	O
afterwards	O
,	O
Ellison	PERSON
was	O
dropped	O
from	O
the	O
project	O
.	O
Without	O
Ellison	PERSON
,	O
the	O
film	O
came	O
to	O
a	O
dead	O
end	O
,	O
because	O
subsequent	O
scripts	O
were	O
unsatisfactory	O
to	O
potential	O
directors	O
.	O
After	O
a	O
change	O
in	O
studio	O
heads	O
,	O
Warner	O
allowed	O
Ellison	O
's	O
script	O
to	O
be	O
serialized	O
in	O
Isaac	O
Asimov	O
's	O
Science	O
Fiction	O
Magazine	O
and	O
in	O
book	O
form	O
.	O
In	O
the	O
1980s	O
,	O
Ellison	PERSON
allegedly	O
assaulted	O
author	O
and	O
critic	O
Charles	PERSON
Platt	PERSON
at	O
the	O
Nebula	O
Awards	O
banquet	O
.	O
Platt	O
did	O
not	O
pursue	O
legal	O
action	O
against	O
Ellison	O
,	O
and	O
the	O
two	O
men	O
later	O
signed	O
a	O
"	O
non-aggression	O
pact	O
"	O
,	O
promising	O
never	O
to	O
discuss	O
the	O
incident	O
again	O
nor	O
to	O
have	O
any	O
contact	O
with	O
one	O
another	O
.	O
Platt	O
claims	O
that	O
Ellison	O
has	O
often	O
publicly	O
boasted	O
about	O
the	O
incident	O
.	O
The	O
book	O
recounts	O
the	O
history	O
of	O
Fantagraphics	O
and	O
discussed	O
a	O
lawsuit	O
that	O
resulted	O
from	O
a	O
1980	O
Ellison	PERSON
interview	O
with	O
Fantagraphics	O
'	O
industry	O
news	O
magazine	O
,	O
The	O
Comics	O
Journal	O
.	O
In	O
this	O
interview	O
Ellison	O
referred	O
to	O
comic	O
book	O
writer	O
Michael	PERSON
Fleisher	PERSON
,	O
calling	O
him	O
"	O
bugfuck	O
"	O
and	O
"	O
derange-o	O
"	O
.	O
Ellison	PERSON
,	O
after	O
reading	O
unpublished	O
drafts	O
of	O
the	O
book	O
on	O
Fantagraphics	O
's	O
website	O
,	O
believed	O
that	O
he	O
had	O
been	O
defamed	O
by	O
several	O
anecdotes	O
related	O
to	O
this	O
incident	O
.	O
Fantagraphics	O
attempted	O
to	O
have	O
the	O
lawsuit	O
dismissed	O
.	O
In	O
their	O
motion	O
to	O
dismiss	O
,	O
Fantagraphics	O
argued	O
that	O
the	O
statements	O
were	O
both	O
their	O
personal	O
opinions	O
and	O
generally	O
believed	O
to	O
be	O
true	O
anecdotes	O
.	O
On	O
June	O
29	O
,	O
2007	O
,	O
Ellison	O
claimed	O
that	O
the	O
litigation	O
had	O
been	O
resolved	O
pending	O
Fantagraphics	O
'	O
removal	O
of	O
all	O
references	O
to	O
the	O
case	O
from	O
their	O
website	O
.	O
On	O
August	O
26	O
,	O
2006	O
,	O
during	O
the	O
64th	O
World	O
Science	O
Fiction	O
Convention	O
,	O
Ellison	O
grabbed	O
Connie	PERSON
Willis	PERSON
'	O
breast	O
while	O
on	O
stage	O
at	O
the	O
Hugo	O
Awards	O
ceremony	O
.	O
Ellen	PERSON
Datlow	PERSON
described	O
this	O
as	O
"	O
a	O
schtick	O
of	O
Harlan	O
acting	O
like	O
a	O
baby	O
"	O
.	O
Patrick	PERSON
Nielsen	PERSON
Hayden	PERSON
described	O
this	O
as	O
"	O
pathetic	O
and	O
nasty	O
and	O
sad	O
and	O
most	O
of	O
us	O
did	O
n't	O
want	O
to	O
watch	O
it	O
"	O
.	O
Ellison	PERSON
responded	O
three	O
days	O
later	O
,	O
writing	O
,	O
"	O
I	O
was	O
unaware	O
of	O
any	O
problem	O
proceeding	O
from	O
my	O
intendedly-childlike	O
grabbing	O
of	O
Connie	O
Willis	O
's	O
left	O
breast	O
,	O
as	O
she	O
was	O
exhorting	O
me	O
to	O
behave	O
.	O
"	O
Ellison	PERSON
claimed	O
that	O
James	O
Cameron	O
's	O
film	O
The	O
Terminator	O
drew	O
from	O
material	O
from	O
Ellison	O
's	O
"	O
Soldier	O
"	O
and	O
"	O
Demon	O
with	O
a	O
Glass	O
Hand	O
"	O
The	O
Outer	O
Limits	O
episodes	O
.	O
Hemdale	O
,	O
the	O
production	O
company	O
and	O
the	O
distributor	O
Orion	O
Pictures	O
,	O
settled	O
out	O
of	O
court	O
for	O
an	O
undisclosed	O
sum	O
,	O
"	O
gratefully	O
acknowledging	O
"	O
the	O
work	O
of	O
Ellison	PERSON
at	O
the	O
end	O
of	O
the	O
film	O
.	O
Cameron	O
has	O
labeled	O
Ellison	PERSON
's	O
claim	O
a	O
"	O
nuisance	O
suit	O
"	O
and	O
Ellison	PERSON
a	O
"	O
parasite	O
.	O
"	O
Since	O
the	O
publication	O
of	O
the	O
author	O
's	O
last	O
collection	O
of	O
previously	O
uncollected	O
stories	O
,	O
Slippage	O
(	O
1997	O
)	O
,	O
Ellison	O
has	O
published	O
the	O
following	O
works	O
of	O
fiction	O
:	O
Several	O
stories	O
have	O
been	O
adapted	O
and	O
collected	O
into	O
comic	O
book	O
stories	O
for	O
Dark	O
Horse	O
Comics	O
.	O
On	O
the	O
May	O
30	O
,	O
2008	O
broadcast	O
of	O
the	O
PRI	O
radio	O
program	O
Studio	O
360	O
,	O
Ellison	O
announced	O
that	O
he	O
had	O
signed	O
with	O
a	O
"	O
major	O
publisher	O
"	O
to	O
produce	O
his	O
memoirs	O
.	O
In	O
1990	O
,	O
Ellison	O
was	O
honored	O
by	O
International	O
PEN	O
for	O
continuing	O
commitment	O
to	O
artistic	O
freedom	O
and	O
the	O
battle	O
against	O
censorship	O
.	O
In	O
1990	O
,	O
Ellison	PERSON
was	O
honored	O
by	O
International	O
PEN	O
for	O
continuing	O
commitment	O
to	O
artistic	O
freedom	O
and	O
the	O
battle	O
against	O
censorship	O
.	O
Ellison	PERSON
is	O
such	O
a	O
distinctive	O
personality	O
that	O
many	O
other	O
science-fiction	O
authors	O
have	O
inserted	O
thinly-disguised	O
parodies	O
of	O
him	O
into	O
their	O
works	O
,	O
some	O
good-natured	O
,	O
others	O
hostile	O
.	O
One	O
of	O
the	O
more	O
benevolent	O
is	O
the	O
main	O
character	O
in	O
a	O
mystery	O
novel	O
Murder	O
at	O
the	O
A.B.A.	O
by	O
Isaac	PERSON
Asimov	PERSON
.	O
In	O
The	O
Dark	O
Knight	O
Returns	O
,	O
Frank	PERSON
Miller	PERSON
featured	O
Ellison	PERSON
by	O
name	O
as	O
a	O
television	O
talking	O
head	O
.	O
In	O
a	O
somewhat	O
less	O
sympathetic	O
vein	O
,	O
Ellison	PERSON
serves	O
as	O
a	O
partial	O
basis	O
for	O
a	O
composite	O
character	O
in	O
Sharyn	PERSON
McCrumb	PERSON
's	O
Bimbos	O
of	O
the	O
Death	O
Sun	O
.	O
