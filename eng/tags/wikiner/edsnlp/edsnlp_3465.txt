Some	O
existing	O
instruments	O
,	O
such	O
as	O
those	O
by	O
Gasparo	PERSON
da	PERSON
Salò	PERSON
,	O
were	O
converted	O
from	O
sixteenth-century	O
six-string	O
contrabass	O
violoni	O
.	O
Bassists	O
may	O
apply	O
more	O
rosin	O
in	O
works	O
for	O
large	O
orchestra	O
(	O
e.g.	O
,	O
Brahms	O
symphonies	O
)	O
than	O
for	O
delicate	O
chamber	O
works	O
.	O
Bassist	PERSON
Domenico	PERSON
Dragonetti	PERSON
was	O
a	O
prominent	O
musical	O
figure	O
and	O
an	O
acquaintance	O
of	O
Haydn	O
and	O
Ludwig	O
van	O
Beethoven	O
.	O
Beethoven	O
's	O
friendship	O
with	O
Dragonetti	PERSON
may	O
have	O
inspired	O
him	O
to	O
write	O
difficult	O
,	O
separate	O
parts	O
for	O
the	O
double	O
bass	O
in	O
his	O
symphonies	O
,	O
such	O
as	O
the	O
impressive	O
passages	O
in	O
the	O
third	O
movement	O
of	O
the	O
Fifth	O
Symphony	O
and	O
last	O
movement	O
of	O
the	O
Ninth	O
Symphony	O
.	O
Dragonetti	PERSON
wrote	O
ten	O
concertos	O
for	O
the	O
double	O
bass	O
and	O
many	O
solo	O
works	O
for	O
bass	O
and	O
piano	O
.	O
In	O
the	O
19th	O
century	O
,	O
the	O
opera	O
conductor	O
,	O
composer	O
,	O
and	O
bassist	O
Giovanni	PERSON
Bottesini	PERSON
was	O
considered	O
the	O
"	O
Paganini	O
of	O
the	O
double	O
bass	O
"	O
of	O
his	O
time	O
.	O
The	O
leading	O
figure	O
of	O
the	O
double	O
bass	O
in	O
the	O
early	O
20th	O
century	O
was	O
Serge	PERSON
Koussevitzky	PERSON
,	O
best	O
known	O
as	O
conductor	O
of	O
the	O
Boston	O
Symphony	O
Orchestra	O
,	O
who	O
popularized	O
the	O
double	O
bass	O
in	O
modern	O
times	O
as	O
a	O
solo	O
instrument	O
.	O
From	O
the	O
1960s	O
through	O
the	O
end	O
of	O
the	O
century	O
Gary	PERSON
Karr	PERSON
was	O
the	O
leading	O
proponent	O
of	O
the	O
double	O
bass	O
as	O
a	O
solo	O
instrument	O
and	O
was	O
active	O
in	O
commissioning	O
or	O
having	O
hundreds	O
of	O
new	O
works	O
and	O
concerti	O
written	O
especially	O
for	O
him	O
.	O
Other	O
works	O
for	O
this	O
instrumentation	O
written	O
from	O
roughly	O
the	O
same	O
period	O
include	O
those	O
by	O
Johann	PERSON
Nepomuk	PERSON
Hummel	PERSON
,	O
George	PERSON
Onslow	PERSON
,	O
Jan	PERSON
Ladislav	PERSON
Dussek	PERSON
,	O
Louise	PERSON
Farrenc	PERSON
,	O
Ferdinand	PERSON
Ries	PERSON
,	O
Franz	PERSON
Limmer	PERSON
,	O
Johann	PERSON
Baptist	PERSON
Cramer	PERSON
,	O
and	O
Hermann	PERSON
Goetz	PERSON
.	O
Later	O
composers	O
who	O
wrote	O
chamber	O
works	O
for	O
this	O
quintet	O
include	O
Ralph	PERSON
Vaughan	PERSON
Williams	PERSON
,	O
Colin	PERSON
Matthews	PERSON
,	O
Jon	PERSON
Deak	PERSON
,	O
Frank	PERSON
Proto	PERSON
,	O
and	O
John	PERSON
Woolrich	PERSON
.	O
Slightly	O
larger	O
sextets	O
written	O
for	O
piano	O
,	O
string	O
quartet	O
,	O
and	O
double	O
bass	O
have	O
been	O
written	O
by	O
Felix	PERSON
Mendelssohn	PERSON
,	O
Mikhail	PERSON
Glinka	PERSON
,	O
Richard	PERSON
Wernick	PERSON
,	O
and	O
Charles	PERSON
Ives	PERSON
.	O
Slightly	O
smaller	O
string	O
works	O
with	O
the	O
double	O
bass	O
include	O
six	O
string	O
sonatas	O
by	O
Gioachino	PERSON
Rossini	PERSON
,	O
for	O
two	O
violins	O
,	O
cello	O
,	O
and	O
double	O
bass	O
written	O
at	O
the	O
age	O
of	O
twelve	O
over	O
the	O
course	O
of	O
three	O
days	O
in	O
1804	O
.	O
Beethoven	O
paved	O
the	O
way	O
for	O
separate	O
double	O
bass	O
parts	O
,	O
which	O
became	O
more	O
common	O
in	O
the	O
romantic	O
era	O
.	O
The	O
scherzo	O
and	O
trio	O
from	O
Beethoven	O
's	O
Fifth	O
Symphony	O
are	O
famous	O
orchestral	O
excerpts	O
,	O
as	O
is	O
the	O
recitative	O
at	O
the	O
beginning	O
of	O
the	O
fourth	O
movement	O
of	O
Beethoven	O
's	O
Ninth	O
Symphony	O
.	O
Johannes	PERSON
Brahms	PERSON
,	O
whose	O
father	O
was	O
a	O
double	O
bass	O
player	O
,	O
wrote	O
many	O
difficult	O
and	O
prominent	O
parts	O
for	O
the	O
double	O
bass	O
in	O
his	O
symphonies	O
.	O
Richard	PERSON
Strauss	PERSON
assigned	O
the	O
double	O
bass	O
daring	O
parts	O
,	O
and	O
his	O
symphonic	O
poems	O
and	O
operas	O
stretch	O
the	O
instrument	O
to	O
its	O
limits	O
.	O
Benjamin	PERSON
Britten	PERSON
's	O
The	O
Young	O
Person	O
's	O
Guide	O
to	O
the	O
Orchestra	O
contains	O
a	O
prominent	O
passage	O
for	O
the	O
double	O
bass	O
section	O
.	O
Examples	O
include	O
swing	O
era	O
players	O
such	O
as	O
Jimmy	PERSON
Blanton	PERSON
,	O
who	O
played	O
with	O
Duke	O
Ellington	O
,	O
and	O
Oscar	PERSON
Pettiford	PERSON
,	O
who	O
pioneered	O
the	O
instrument	O
's	O
use	O
in	O
bebop	O
.	O
Paul	PERSON
Chambers	PERSON
(	O
who	O
worked	O
with	O
Miles	PERSON
Davis	O
on	O
the	O
famous	O
Kind	O
of	O
Blue	O
album	O
)	O
achieved	O
renown	O
for	O
being	O
one	O
of	O
the	O
first	O
jazz	O
bassists	O
to	O
play	O
bebop	O
solos	O
with	O
the	O
bow	O
.	O
A	O
number	O
of	O
other	O
bassists	O
,	O
such	O
as	O
Niels-Henning	PERSON
Ørsted	O
Pedersen	O
,	O
Ray	PERSON
Brown	PERSON
and	O
Slam	PERSON
Stewart	PERSON
,	O
were	O
central	O
to	O
the	O
history	O
of	O
jazz	O
.	O
Notably	O
,	O
Charles	PERSON
Mingus	PERSON
was	O
a	O
highly	O
regarded	O
composer	O
as	O
well	O
as	O
a	O
bassist	O
noted	O
for	O
his	O
technical	O
virtuosity	O
and	O
powerful	O
sound	O
.	O
Scott	PERSON
LaFaro	PERSON
influenced	O
a	O
generation	O
of	O
musicians	O
by	O
liberating	O
the	O
bass	O
from	O
contrapuntal	O
"	O
walking	O
"	O
behind	O
soloists	O
instead	O
favoring	O
interactive	O
,	O
conversational	O
melodies	O
.	O
Beginning	O
in	O
the	O
1970s	O
bassist	O
Bob	PERSON
Cranshaw	PERSON
,	O
playing	O
with	O
saxophonist	O
Sonny	PERSON
Rollins	PERSON
,	O
and	O
fusion	O
pioneers	O
Jaco	PERSON
Pastorius	PERSON
and	O
Stanley	PERSON
Clarke	PERSON
began	O
to	O
substitute	O
the	O
electric	O
bass	O
guitar	O
for	O
the	O
upright	O
bass	O
.	O
The	O
classical	O
bassist	O
Edgar	PERSON
Meyer	PERSON
has	O
frequently	O
branched	O
out	O
into	O
newgrass	O
,	O
old-time	O
,	O
jazz	O
,	O
and	O
other	O
genres	O
.	O
Even	O
slapping	O
experts	O
such	O
as	O
Mike	PERSON
Bub	PERSON
say	O
,	O
"	O
...	O
do	O
n't	O
slap	O
on	O
every	O
gig	O
"	O
or	O
in	O
songs	O
where	O
it	O
is	O
"	O
not	O
appropriate	O
.	O
"	O
In	O
1952	O
,	O
the	O
upright	O
bass	O
was	O
a	O
standard	O
instrument	O
in	O
rock	O
and	O
roll	O
music	O
,	O
Marshall	O
Lytle	O
of	O
Bill	O
Haley	O
&	O
His	O
Comets	O
being	O
but	O
one	O
example	O
.	O
Louis	PERSON
Jordan	PERSON
,	O
the	O
first	O
innovator	O
of	O
this	O
style	O
,	O
featured	O
a	O
upright	O
bass	O
in	O
his	O
group	O
,	O
the	O
Tympany	O
Five	O
.	O
In	O
the	O
experimental	O
post	O
1960s	O
eras	O
,	O
which	O
saw	O
the	O
development	O
of	O
free	O
jazz	O
and	O
jazz-rock	O
fusion	O
,	O
some	O
of	O
the	O
influential	O
bassists	O
included	O
Charles	PERSON
Mingus	PERSON
(	O
1922	O
--	O
1979	O
)	O
,	O
who	O
was	O
also	O
a	O
composer	O
and	O
bandleader	O
whose	O
music	O
fused	O
hard	O
bop	O
with	O
black	O
gospel	O
music	O
,	O
free	O
jazz	O
and	O
classical	O
music	O
;	O
free	O
jazz	O
and	O
post-bop	O
bassist	O
Charlie	PERSON
Haden	PERSON
(	O
born	O
1937	O
)	O
is	O
best	O
known	O
for	O
his	O
long	O
association	O
with	O
saxophonist	O
Ornette	PERSON
Coleman	PERSON
and	O
for	O
his	O
role	O
in	O
the	O
1970s-era	O
Liberation	O
Music	O
Orchestra	O
,	O
an	O
experimental	O
group	O
;	O
and	O
fusion	O
virtuoso	O
Stanley	PERSON
Clarke	PERSON
(	O
born	O
1951	O
)	O
is	O
notable	O
for	O
his	O
dexterity	O
on	O
both	O
the	O
upright	O
bass	O
and	O
the	O
electric	O
bass	O
.	O
In	O
addition	O
to	O
being	O
a	O
noted	O
classical	O
player	O
,	O
Edgar	PERSON
Meyer	PERSON
is	O
well-known	O
in	O
bluegrass	O
and	O
newgrass	O
circles	O
.	O
Todd	PERSON
Phillips	PERSON
is	O
another	O
prominent	O
bluegrass	O
player	O
.	O
Well-known	O
rockabilly	O
bassists	O
include	O
Bill	PERSON
Black	PERSON
,	O
Marshall	PERSON
Lytle	PERSON
(	O
with	O
Bill	O
Haley	O
&	O
His	O
Comets	O
)	O
and	O
Lee	PERSON
Rocker	PERSON
(	O
with	O
1980s-era	O
rockabilly	O
revivalists	O
the	O
Stray	O
Cats	O
)	O
.	O
Willie	PERSON
Dixon	PERSON
(	O
1915	O
--	O
1992	O
)	O
was	O
one	O
of	O
the	O
most	O
notable	O
figures	O
in	O
the	O
history	O
of	O
rhythm	O
and	O
blues	O
.	O
He	O
also	O
plays	O
bass	O
on	O
numerous	O
Chuck	O
Berry	O
's	O
rock	O
and	O
roll	O
hits	O
.	O
In	O
some	O
styles	O
of	O
music	O
,	O
such	O
as	O
jazz-oriented	O
stage	O
bands	O
,	O
bassists	O
may	O
be	O
asked	O
to	O
sight	O
read	O
printed	O
music	O
or	O
perform	O
standard	O
pieces	O
(	O
e.g.	O
,	O
a	O
jazz	O
standard	O
such	O
as	O
"	O
Now	O
's	O
the	O
Time	O
"	O
)	O
with	O
an	O
ensemble	O
.	O
composing	O
music	O
(	O
e.g.	O
,	O
Dave	O
Holland	O
)	O
,	O
songwriting	O
,	O
conducting	O
(	O
e.g.	O
,	O
David	PERSON
Currie	PERSON
)	O
,	O
or	O
acting	O
as	O
a	O
bandleader	O
(	O
e.g.	O
,	O
Charles	PERSON
Mingus	PERSON
)	O
.	O
