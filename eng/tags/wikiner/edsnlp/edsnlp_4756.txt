He	O
is	O
best	O
known	O
for	O
being	O
the	O
creator	O
of	O
the	O
epic	O
science	O
fiction	O
franchise	O
Star	O
Wars	O
and	O
joint	O
creator	O
of	O
the	O
archaeologist-adventurer	O
character	O
Indiana	O
Jones	PERSON
.	O
Lucas	PERSON
grew	O
up	O
in	O
the	O
sleepy	O
Central	O
Valley	O
town	O
of	O
Modesto	LOCATION
and	O
his	O
early	O
passion	O
for	O
cars	O
and	O
motor	O
racing	O
would	O
eventually	O
serve	O
as	O
inspiration	O
for	O
his	O
Oscar-nominated	O
low-budget	O
phenomenon	O
,	O
American	O
Graffiti	O
.	O
Long	O
before	O
Lucas	PERSON
became	O
obsessed	O
with	O
film	O
making	O
,	O
he	O
wanted	O
to	O
be	O
a	O
race-car	O
driver	O
,	O
and	O
he	O
spent	O
most	O
of	O
his	O
high	O
school	O
years	O
racing	O
on	O
the	O
underground	O
circuit	O
at	O
fairgrounds	O
and	O
hanging	O
out	O
at	O
garages	O
.	O
However	O
,	O
a	O
near-fatal	O
accident	O
in	O
his	O
souped-up	O
Autobianchi	O
Bianchina	O
on	O
June	O
12	O
,	O
1962	O
,	O
just	O
days	O
before	O
his	O
high	O
school	O
graduation	O
,	O
quickly	O
changed	O
his	O
mind	O
.	O
Instead	O
of	O
racing	O
,	O
he	O
attended	O
Modesto	O
Junior	O
College	O
and	O
later	O
got	O
accepted	O
into	O
a	O
junior	O
college	O
to	O
study	O
anthropology	O
.	O
During	O
this	O
time	O
,	O
an	O
experimental	O
filmmaker	O
named	O
Bruce	PERSON
Baillie	PERSON
tacked	O
up	O
a	O
bedsheet	O
in	O
his	O
backyard	O
in	O
1960	O
to	O
screen	O
the	O
work	O
of	O
underground	O
,	O
avant-garde	O
16	O
mm	O
filmmakers	O
like	O
Jordan	PERSON
Belson	PERSON
,	O
Stan	PERSON
Brakhage	PERSON
and	O
Bruce	PERSON
Conner	PERSON
.	O
For	O
the	O
next	O
few	O
years	O
,	O
Baillie	O
's	O
series	O
,	O
dubbed	O
Canyon	O
Cinema	O
,	O
toured	O
local	O
coffeehouses	O
.	O
Already	O
a	O
promising	O
photographer	O
,	O
Lucas	O
became	O
infatuated	O
with	O
these	O
abstract	O
films	O
.	O
It	O
was	O
a	O
season	O
of	O
awakening	O
for	O
Lucas	PERSON
,	O
who	O
had	O
been	O
an	O
uninterested	O
slacker	O
in	O
high	O
school	O
.	O
At	O
an	O
autocross	O
track	O
,	O
Lucas	PERSON
met	O
his	O
first	O
mentor	O
in	O
the	O
film	O
industry	O
--	O
famed	O
cinematographer	O
Haskell	PERSON
Wexler	PERSON
,	O
a	O
fellow	O
aficionado	O
of	O
sleek	O
racing	O
machines	O
.	O
Wexler	O
was	O
impressed	O
by	O
the	O
way	O
the	O
shy	O
teenager	O
handled	O
a	O
camera	O
,	O
cradling	O
it	O
low	O
on	O
his	O
hips	O
to	O
get	O
better	O
angles	O
.	O
Lucas	PERSON
then	O
transferred	O
to	O
the	O
University	O
of	O
Southern	O
California	O
School	O
of	O
Cinematic	O
Arts	O
.	O
USC	O
was	O
one	O
of	O
the	O
earliest	O
universities	O
to	O
have	O
a	O
school	O
devoted	O
to	O
motion	O
picture	O
film	O
.	O
During	O
the	O
years	O
at	O
USC	O
,	O
George	PERSON
Lucas	PERSON
shared	O
a	O
dorm	O
room	O
with	O
Randal	PERSON
Kleiser	PERSON
.	O
He	O
also	O
became	O
very	O
good	O
friends	O
with	O
fellow	O
acclaimed	O
student	O
filmmaker	O
Steven	PERSON
Spielberg	PERSON
.	O
Vorkapich	PERSON
taught	O
the	O
autonomous	O
nature	O
of	O
the	O
cinematic	O
art	O
form	O
,	O
emphasizing	O
the	O
unique	O
dynamic	O
quality	O
of	O
movement	O
and	O
kinetic	O
energy	O
inherent	O
in	O
motion	O
pictures	O
.	O
After	O
graduating	O
with	O
a	O
bachelor	O
of	O
fine	O
arts	O
in	O
film	O
in	O
1967	O
,	O
he	O
tried	O
joining	O
the	O
United	O
States	O
Air	O
Force	O
as	O
an	O
officer	O
,	O
but	O
he	O
was	O
immediately	O
turned	O
down	O
because	O
of	O
his	O
numerous	O
speeding	O
tickets	O
.	O
In	O
1967	O
,	O
Lucas	PERSON
re-enrolled	O
as	O
a	O
USC	O
graduate	O
student	O
in	O
film	O
production	O
.	O
Lucas	PERSON
was	O
awarded	O
a	O
student	O
scholarship	O
by	O
Warner	O
Brothers	O
to	O
observe	O
and	O
work	O
on	O
the	O
making	O
of	O
a	O
film	O
of	O
his	O
choosing	O
.	O
George	PERSON
Lucas	PERSON
is	O
one	O
of	O
the	O
most	O
successful	O
and	O
celebrated	O
filmmakers	O
in	O
cinema	O
history	O
,	O
with	O
a	O
film	O
career	O
dominated	O
by	O
writing	O
and	O
production	O
.	O
Lucas	O
'	O
career	O
path	O
contrasts	O
with	O
that	O
of	O
his	O
friend	O
,	O
collaborator	O
and	O
close	O
contemporary	O
Steven	PERSON
Spielberg	PERSON
,	O
who	O
has	O
similarly	O
worked	O
extensively	O
as	O
a	O
writer	O
and	O
producer	O
but	O
has	O
also	O
directed	O
almost	O
thirty	O
major	O
feature	O
films	O
over	O
a	O
similar	O
time	O
span	O
.	O
In	O
addition	O
,	O
his	O
far-sighted	O
decision	O
to	O
establish	O
his	O
own	O
effects	O
company	O
to	O
make	O
the	O
original	O
Star	O
Wars	O
film	O
has	O
reaped	O
enormous	O
benefits	O
;	O
the	O
award-winning	O
Industrial	O
Light	O
and	O
Magic	O
(	O
ILM	O
)	O
is	O
acknowledged	O
one	O
of	O
the	O
world	O
leaders	O
in	O
the	O
field	O
and	O
has	O
created	O
groundbreaking	O
special	O
effects	O
for	O
many	O
other	O
box	O
office	O
hits	O
.	O
His	O
first	O
full-length	O
feature	O
film	O
produced	O
by	O
the	O
studio	O
,	O
THX	O
1138	O
,	O
was	O
not	O
a	O
success	O
.	O
Lucas	O
then	O
founded	O
his	O
own	O
company	O
,	O
Lucasfilm	O
,	O
Ltd.	O
,	O
and	O
directed	O
a	O
far	O
more	O
successful	O
film	O
,	O
American	O
Graffiti	O
(	O
1973	O
)	O
.	O
He	O
then	O
proposed	O
a	O
new	O
Flash	O
Gordon	O
film	O
adaptation	O
,	O
but	O
the	O
rights	O
were	O
not	O
available	O
.	O
Even	O
so	O
,	O
he	O
encountered	O
difficulties	O
getting	O
Star	O
Wars	O
made	O
.	O
On	O
a	O
return-on-investment	O
basis	O
,	O
Star	O
Wars	O
proved	O
to	O
be	O
one	O
of	O
the	O
most	O
successful	O
films	O
of	O
all	O
time	O
.	O
During	O
the	O
filming	O
of	O
Star	O
Wars	O
,	O
Lucas	O
waived	O
his	O
up-front	O
fee	O
as	O
director	O
and	O
negotiated	O
to	O
own	O
the	O
licensing	O
rights	O
--	O
rights	O
which	O
the	O
studio	O
thought	O
were	O
nearly	O
worthless	O
.	O
Lucas	O
also	O
acted	O
as	O
executive	O
producer	O
and	O
story	O
writer	O
on	O
all	O
four	O
of	O
the	O
Indiana	O
Jones	O
films	O
.	O
Other	O
notable	O
projects	O
as	O
a	O
producer	O
or	O
executive	O
producer	O
in	O
this	O
period	O
include	O
Kurosawa	O
's	O
Kagemusha	O
(	O
1980	O
)	O
,	O
Lawrence	PERSON
Kasdan	PERSON
's	O
Body	O
Heat	O
(	O
1981	O
)	O
,	O
Jim	PERSON
Henson	PERSON
's	O
Labyrinth	O
(	O
1986	O
)	O
,	O
Godfrey	PERSON
Reggio	PERSON
's	O
Powaqqatsi	O
(	O
1986	O
)	O
and	O
the	O
animated	O
film	O
The	O
Land	O
Before	O
Time	O
(	O
1988	O
)	O
.	O
There	O
were	O
also	O
some	O
less	O
successful	O
projects	O
,	O
however	O
,	O
including	O
More	O
American	O
Graffiti	O
(	O
1979	O
)	O
,	O
the	O
ill-fated	O
Howard	O
the	O
Duck	O
(	O
1986	O
)	O
,	O
which	O
was	O
arguably	O
the	O
biggest	O
flop	O
of	O
his	O
career	O
;	O
Willow	O
(	O
1988	O
,	O
which	O
Lucas	O
also	O
wrote	O
)	O
;	O
and	O
Coppola	O
's	O
Tucker	O
:	O
The	O
Man	O
and	O
His	O
Dream	O
(	O
1988	O
)	O
.	O
Between	O
1992	O
and	O
1996	O
,	O
Lucas	O
served	O
as	O
executive	O
producer	O
for	O
the	O
television	O
spinoff	O
The	O
Young	O
Indiana	O
Jones	O
Chronicles	O
.	O
In	O
1997	O
,	O
for	O
the	O
20th	O
anniversary	O
of	O
Star	O
Wars	O
,	O
Lucas	O
went	O
back	O
to	O
his	O
trilogy	O
to	O
enhance	O
and	O
add	O
certain	O
scenes	O
using	O
newly	O
available	O
digital	O
technology	O
.	O
Pixar	O
's	O
early	O
computer	O
graphics	O
research	O
resulted	O
in	O
groundbreaking	O
effects	O
in	O
films	O
such	O
as	O
Star	O
Trek	O
II	O
:	O
The	O
Wrath	O
of	O
Khan	O
and	O
Young	O
Sherlock	O
Holmes	O
,	O
and	O
the	O
group	O
was	O
purchased	O
in	O
1986	O
by	O
Steve	PERSON
Jobs	O
shortly	O
after	O
he	O
left	O
Apple	O
after	O
a	O
power	O
struggle	O
at	O
Apple	O
Computer	O
.	O
The	O
sale	O
reflected	O
Lucas	PERSON
'	O
desire	O
to	O
stop	O
the	O
cash	O
flow	O
losses	O
from	O
his	O
7-year	O
research	O
projects	O
associated	O
with	O
new	O
entertainment	O
technology	O
tools	O
,	O
as	O
well	O
as	O
his	O
company	O
's	O
new	O
focus	O
on	O
creating	O
entertainment	O
products	O
rather	O
than	O
tools	O
.	O
A	O
contributing	O
factor	O
was	O
cash-flow	O
difficulties	O
following	O
Lucas	O
'	O
1983	O
divorce	O
concurrent	O
with	O
the	O
sudden	O
dropoff	O
in	O
revenues	O
from	O
Star	O
Wars	O
licenses	O
following	O
the	O
release	O
of	O
Return	O
of	O
the	O
Jedi	O
.	O
The	O
company	O
was	O
formerly	O
owned	O
by	O
Lucasfilm	O
,	O
and	O
contains	O
equipment	O
for	O
stereo	O
,	O
digital	O
,	O
and	O
theatrical	O
sound	O
for	O
movies	O
,	O
and	O
music	O
.	O
Skywalker	O
Sound	O
and	O
Industrial	O
Light	O
&	O
Magic	O
,	O
the	O
sound	O
and	O
visual	O
effects	O
subdivisions	O
of	O
Lucasfilm	O
,	O
respectively	O
,	O
have	O
become	O
among	O
the	O
most	O
respected	O
firms	O
in	O
their	O
fields	O
.	O
Lucasfilm	O
Games	O
,	O
later	O
renamed	O
LucasArts	O
,	O
is	O
well	O
respected	O
in	O
the	O
gaming	O
industry	O
.	O
In	O
1994	O
,	O
Lucas	O
began	O
work	O
on	O
the	O
screenplay	O
for	O
the	O
prequel	O
Star	O
Wars	O
Episode	O
I	O
,	O
which	O
would	O
be	O
the	O
first	O
film	O
he	O
had	O
directed	O
in	O
over	O
two	O
decades	O
.	O
The	O
Phantom	O
Menace	O
was	O
released	O
in	O
1999	O
,	O
beginning	O
a	O
new	O
trilogy	O
of	O
Star	O
Wars	O
films	O
.	O
In	O
2008	O
,	O
he	O
reteamed	O
with	O
Steven	O
Spielberg	O
for	O
Indiana	O
Jones	O
and	O
the	O
Kingdom	O
of	O
the	O
Crystal	O
Skull	O
.	O
He	O
is	O
also	O
working	O
on	O
a	O
so-far	O
untitled	O
Star	O
Wars	O
live-action	O
series	O
.	O
For	O
the	O
film	O
Red	O
Tails	O
(	O
2010	O
)	O
,	O
Lucas	O
serves	O
as	O
story-writer	O
and	O
executive	O
producer	O
.	O
He	O
also	O
took	O
over	O
direction	O
of	O
reshoots	O
while	O
director	O
Anthony	PERSON
Hemingway	PERSON
worked	O
on	O
other	O
projects	O
.	O
The	O
American	O
Film	O
Institute	O
awarded	O
Lucas	O
its	O
Life	O
Achievement	O
Award	O
on	O
June	O
9	O
,	O
2005	O
.	O
This	O
was	O
shortly	O
after	O
the	O
release	O
of	O
Star	O
Wars	O
Episode	O
III	O
:	O
Revenge	O
of	O
the	O
Sith	O
,	O
about	O
which	O
he	O
joked	O
stating	O
that	O
,	O
since	O
he	O
views	O
the	O
entire	O
Star	O
Wars	O
series	O
as	O
one	O
movie	O
,	O
he	O
could	O
actually	O
receive	O
the	O
award	O
now	O
that	O
he	O
had	O
finally	O
"	O
gone	O
back	O
and	O
finished	O
the	O
movie	O
.	O
"	O
On	O
June	O
5	O
,	O
2005	O
,	O
Lucas	O
was	O
named	O
among	O
the	O
100	O
"	O
Greatest	O
Americans	O
"	O
by	O
the	O
Discovery	O
Channel	O
.	O
Lucas	O
was	O
nominated	O
for	O
four	O
Academy	O
Awards	O
:	O
Best	O
Directing	O
and	O
Writing	O
for	O
American	O
Graffiti	O
,	O
and	O
Best	O
Directing	O
and	O
Writing	O
for	O
Star	O
Wars	O
.	O
He	O
appeared	O
at	O
the	O
79th	O
Academy	O
Awards	O
ceremony	O
in	O
2007	O
with	O
Steven	PERSON
Spielberg	PERSON
and	O
Francis	PERSON
Ford	PERSON
Coppola	O
to	O
present	O
the	O
Best	O
Director	O
award	O
to	O
their	O
friend	O
Martin	PERSON
Scorsese	PERSON
.	O
During	O
the	O
speech	O
,	O
Spielberg	O
and	O
Coppola	O
talked	O
about	O
the	O
joy	O
of	O
winning	O
an	O
Oscar	O
,	O
making	O
fun	O
of	O
Lucas	O
,	O
who	O
has	O
not	O
won	O
a	O
competitive	O
Oscar	O
.	O
On	O
September	O
19	O
,	O
2006	O
,	O
USC	O
announced	O
that	O
George	PERSON
Lucas	PERSON
had	O
donated	O
$	O
175	O
--	O
180	O
million	O
to	O
his	O
alma	O
mater	O
to	O
expand	O
the	O
film	O
school	O
.	O
It	O
is	O
the	O
largest	O
single	O
donation	O
to	O
USC	O
and	O
the	O
largest	O
gift	O
to	O
a	O
film	O
school	O
anywhere	O
.	O
The	O
toss	O
favored	O
Lucas	PERSON
's	O
alma	O
mater	O
,	O
the	O
Trojans	O
.	O
His	O
team	O
,	O
which	O
came	O
into	O
the	O
game	O
as	O
underdogs	O
,	O
went	O
on	O
to	O
defeat	O
the	O
Michigan	O
Wolverines	O
(	O
32	O
--	O
18	O
)	O
.	O
On	O
August	O
25	O
,	O
2009	O
,	O
Governor	PERSON
Schwarzenegger	PERSON
and	O
Maria	PERSON
Shriver	PERSON
announced	O
that	O
Lucas	O
would	O
be	O
one	O
of	O
13	O
California	O
Hall	O
of	O
Fame	O
inductees	O
in	O
The	O
California	O
Museum	O
's	O
yearlong	O
exhibit	O
.	O
In	O
1969	O
,	O
Lucas	O
married	O
film	O
editor	O
Marcia	PERSON
Lou	PERSON
Griffin	PERSON
,	O
who	O
went	O
on	O
to	O
win	O
an	O
Academy	O
Award	O
for	O
her	O
editing	O
work	O
on	O
the	O
original	O
(	O
Episode	O
IV	O
)	O
Star	O
Wars	O
film	O
.	O
All	O
three	O
of	O
his	O
children	O
have	O
appeared	O
in	O
the	O
three	O
Star	O
Wars	O
prequels	O
,	O
as	O
has	O
Lucas	O
himself	O
.	O
Lucas	PERSON
had	O
been	O
in	O
a	O
long	O
relationship	O
with	O
and	O
engaged	O
to	O
singer	O
Linda	PERSON
Ronstadt	PERSON
.	O
Lucas	PERSON
resides	O
in	O
Marin	LOCATION
County	LOCATION
.	O
MacFarlane	O
has	O
said	O
that	O
Lucasfilm	O
was	O
extremely	O
helpful	O
when	O
the	O
Family	O
Guy	O
crew	O
wanted	O
to	O
parody	O
their	O
works	O
.	O
