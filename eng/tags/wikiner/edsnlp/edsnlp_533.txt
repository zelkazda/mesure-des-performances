He	O
left	O
Lyon	LOCATION
in	O
the	O
spring	O
of	O
1245	O
for	O
the	O
Levant	O
.	O
The	O
party	O
set	O
out	O
on	O
27	O
January	O
1249	O
,	O
with	O
letters	O
from	O
King	O
Louis	O
and	O
the	O
papal	O
legate	O
,	O
and	O
rich	O
presents	O
,	O
including	O
a	O
chapel-tent	O
,	O
lined	O
with	O
scarlet	O
cloth	O
and	O
embroidered	O
with	O
sacred	O
pictures	O
.	O
Their	O
route	O
led	O
them	O
through	O
Persia	O
,	O
along	O
the	O
southern	O
and	O
eastern	O
shores	O
of	O
the	O
Caspian	O
Sea	O
,	O
and	O
certainly	O
through	O
Talas	O
,	O
north-east	O
of	O
Tashkent	LOCATION
.	O
The	O
regent-mother	O
Oghul	PERSON
Qaimish	PERSON
seems	O
to	O
have	O
received	O
and	O
dismissed	O
him	O
with	O
presents	O
and	O
a	O
letter	O
for	O
Louis	O
IX	O
,	O
the	O
latter	O
a	O
fine	O
specimen	O
of	O
Mongol	O
insolence	O
.	O
On	O
the	O
other	O
hand	O
,	O
the	O
envoy	O
's	O
account	O
of	O
Mongol	O
customs	O
is	O
fairly	O
accurate	O
,	O
and	O
his	O
statements	O
about	O
Mongol	O
Christianity	O
and	O
its	O
prosperity	O
,	O
though	O
perhaps	O
exaggerated	O
(	O
e.g.	O
as	O
to	O
the	O
800	O
chapels	O
on	O
wheels	O
in	O
the	O
nomadic	O
host	O
)	O
,	O
are	O
based	O
on	O
fact	O
.	O
(	O
Paris	O
,	O
1839	O
)	O
,	O
pp.	O
261	O
,	O
265	O
,	O
279	O
,	O
296	O
,	O
310	O
,	O
353	O
,	O
363	O
,	O
370	O
;	O
Joinville	O
,	O
ed	O
.	O
