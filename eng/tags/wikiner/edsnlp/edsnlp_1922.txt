The	O
Battle	O
of	O
Okinawa	O
,	O
codenamed	O
Operation	O
Iceberg	O
,	O
was	O
fought	O
on	O
the	O
Ryukyu	O
Islands	O
of	O
Okinawa	LOCATION
and	O
was	O
the	O
largest	O
amphibious	O
assault	O
in	O
the	O
Pacific	O
War	O
.	O
The	O
nicknames	O
refer	O
to	O
the	O
ferocity	O
of	O
the	O
fighting	O
,	O
the	O
intensity	O
of	O
gunfire	O
involved	O
,	O
and	O
to	O
the	O
sheer	O
numbers	O
of	O
Allied	O
ships	O
and	O
armored	O
vehicles	O
that	O
assaulted	O
the	O
island	O
.	O
The	O
battle	O
resulted	O
in	O
one	O
of	O
the	O
highest	O
number	O
of	O
casualties	O
of	O
any	O
World	O
War	O
II	O
engagement	O
.	O
Japan	O
lost	O
over	O
100,000	O
troops	O
,	O
and	O
the	O
Allies	O
suffered	O
more	O
than	O
50,000	O
casualties	O
.	O
The	O
main	O
objective	O
of	O
the	O
operation	O
was	O
to	O
seize	O
a	O
large	O
island	O
only	O
340	O
miles	O
away	O
from	O
mainland	O
Japan	O
.	O
In	O
all	O
,	O
the	O
Tenth	O
Army	O
contained	O
102,000	O
Army	O
and	O
81,000	O
Marine	O
Corps	O
personnel	O
.	O
Primary	O
resistance	O
was	O
to	O
be	O
led	O
in	O
the	O
south	O
by	O
Lieutenant	O
General	O
Mitsuru	PERSON
Ushijima	PERSON
,	O
his	O
chief	O
of	O
staff	O
,	O
Lieutenant	O
General	O
Isamu	PERSON
Chō	PERSON
and	O
his	O
chief	O
of	O
operations	O
,	O
Colonel	O
Hiromichi	PERSON
Yahara	PERSON
.	O
In	O
the	O
north	O
,	O
Colonel	O
Takehido	PERSON
Udo	O
was	O
in	O
command	O
.	O
The	O
IJN	O
troops	O
were	O
led	O
by	O
Rear	O
Admiral	O
Minoru	PERSON
Ota	PERSON
.	O
To	O
this	O
would	O
be	O
added	O
the	O
Americans	O
'	O
abundant	O
naval	O
and	O
air	O
firepower	O
.	O
Between	O
the	O
American	O
landing	O
on	O
April	O
1	O
and	O
May	O
25	O
,	O
seven	O
major	O
kamikaze	O
attacks	O
were	O
attempted	O
,	O
involving	O
more	O
than	O
1,500	O
planes	O
.	O
On	O
April	O
10	O
,	O
its	O
attention	O
was	O
shifted	O
to	O
airfields	O
on	O
northern	O
Formosa	O
.	O
On	O
May	O
1	O
,	O
the	O
British	O
Pacific	O
Fleet	O
returned	O
to	O
action	O
,	O
subduing	O
the	O
airfields	O
as	O
before	O
,	O
this	O
time	O
with	O
naval	O
bombardment	O
as	O
well	O
as	O
aircraft	O
.	O
The	O
protracted	O
length	O
of	O
the	O
campaign	O
under	O
stressful	O
conditions	O
forced	O
Admiral	O
Chester	PERSON
W.	PERSON
Nimitz	PERSON
to	O
take	O
the	O
unprecedented	O
step	O
of	O
relieving	O
the	O
principal	O
naval	O
commanders	O
to	O
rest	O
and	O
recuperate	O
.	O
Following	O
the	O
practice	O
of	O
changing	O
the	O
fleet	O
designation	O
with	O
the	O
change	O
of	O
commanders	O
,	O
U.S.	O
naval	O
forces	O
began	O
the	O
campaign	O
as	O
the	O
U.S.	O
Fifth	O
Fleet	O
under	O
Admiral	O
Raymond	O
Spruance	PERSON
,	O
but	O
ended	O
it	O
as	O
the	O
U.S.	O
Third	O
Fleet	O
under	O
Admiral	O
William	O
Halsey	PERSON
.	O
Under	O
attack	O
from	O
more	O
than	O
300	O
aircraft	O
over	O
a	O
two-hour	O
span	O
,	O
the	O
world	O
's	O
largest	O
battleship	O
sank	O
on	O
April	O
7	O
,	O
1945	O
,	O
long	O
before	O
she	O
could	O
reach	O
Okinawa	LOCATION
.	O
U.S.	O
torpedo	O
bombers	O
were	O
instructed	O
to	O
only	O
aim	O
for	O
one	O
side	O
to	O
prevent	O
effective	O
counter	O
flooding	O
by	O
the	O
battleship	O
's	O
crew	O
,	O
and	O
hitting	O
preferably	O
the	O
bow	O
or	O
stern	O
,	O
where	O
armor	O
was	O
believed	O
to	O
be	O
the	O
thinnest	O
.	O
Of	O
the	O
Yamato	O
's	O
screening	O
force	O
,	O
the	O
light	O
cruiser	O
Yahagi	O
,	O
and	O
four	O
out	O
of	O
the	O
eight	O
destroyers	O
were	O
also	O
sunk	O
.	O
In	O
all	O
,	O
the	O
Imperial	O
Japanese	O
Navy	O
lost	O
some	O
3,700	O
sailors	O
,	O
including	O
Itō	O
,	O
at	O
the	O
relatively	O
low	O
cost	O
of	O
ten	O
U.S.	O
aircraft	O
and	O
12	O
airmen	O
.	O
155	O
mm	O
Long	O
Toms	O
went	O
ashore	O
on	O
the	O
islets	O
to	O
cover	O
operations	O
on	O
Okinawa	LOCATION
.	O
Tenth	O
Army	O
swept	O
across	O
the	O
south-central	O
part	O
of	O
the	O
island	O
with	O
relative	O
ease	O
by	O
World	O
War	O
II	O
standards	O
,	O
capturing	O
the	O
Kadena	O
and	O
the	O
Yomitan	O
airbases	O
.	O
Meanwhile	O
,	O
the	O
77th	O
Infantry	O
Division	O
assaulted	O
Ie	O
Shima	O
,	O
a	O
small	O
island	O
off	O
the	O
western	O
end	O
of	O
the	O
peninsula	O
,	O
on	O
April	O
16	O
.	O
There	O
was	O
heavy	O
fighting	O
before	O
Ie	O
Shima	O
was	O
declared	O
secured	O
on	O
April	O
21	O
and	O
became	O
another	O
air	O
base	O
for	O
operations	O
against	O
Japan	O
.	O
The	O
American	O
advance	O
was	O
inexorable	O
but	O
resulted	O
in	O
massive	O
casualties	O
sustained	O
by	O
both	O
sides	O
.	O
General	O
Hodge	O
now	O
had	O
three	O
divisions	O
in	O
the	O
line	O
,	O
with	O
the	O
96th	O
in	O
the	O
middle	O
,	O
and	O
the	O
7th	O
on	O
the	O
east	O
,	O
with	O
each	O
division	O
holding	O
a	O
front	O
of	O
only	O
about	O
1.5	O
miles	O
(	O
2.4	O
km	O
)	O
.	O
Although	O
flame	O
tanks	O
cleared	O
many	O
cave	O
defenses	O
,	O
there	O
was	O
no	O
breakthrough	O
,	O
and	O
the	O
XXIV	O
Corps	O
lost	O
720	O
killed	O
,	O
wounded	O
or	O
missing	O
.	O
At	O
the	O
end	O
of	O
April	O
,	O
the	O
1st	O
Marine	O
Division	O
relieved	O
the	O
27th	O
Infantry	O
Division	O
,	O
and	O
the	O
77th	O
Infantry	O
Division	O
relieved	O
the	O
7th	O
.	O
When	O
the	O
6th	O
Marine	O
Division	O
arrived	O
,	O
III	O
Amphibious	O
Corps	O
took	O
over	O
the	O
right	O
flank	O
and	O
Tenth	O
Army	O
assumed	O
control	O
of	O
the	O
battle	O
.	O
By	O
doing	O
so	O
they	O
were	O
able	O
to	O
fire	O
13,000	O
rounds	O
in	O
support	O
but	O
American	O
counter-battery	O
fire	O
destroyed	O
19	O
guns	O
on	O
May	O
4	O
and	O
40	O
more	O
over	O
the	O
next	O
two	O
days	O
.	O
Buckner	O
launched	O
another	O
American	O
attack	O
on	O
May	O
11	O
.	O
The	O
ground	O
advance	O
began	O
to	O
resemble	O
a	O
World	O
War	O
I	O
battlefield	O
as	O
troops	O
became	O
mired	O
in	O
mud	O
and	O
flooded	O
roads	O
greatly	O
inhibited	O
evacuation	O
of	O
wounded	O
to	O
the	O
rear	O
.	O
Shuri	O
Castle	O
had	O
been	O
shelled	O
for	O
3	O
days	O
prior	O
to	O
this	O
advance	O
by	O
the	O
USS	O
Mississippi	O
(	O
BB-41	O
)	O
.	O
The	O
castle	O
,	O
however	O
,	O
was	O
outside	O
the	O
1st	O
Marine	O
Division	O
's	O
assigned	O
zone	O
and	O
only	O
frantic	O
efforts	O
by	O
the	O
commander	O
and	O
staff	O
of	O
the	O
77th	O
Infantry	O
Division	O
prevented	O
an	O
American	O
air	O
strike	O
and	O
artillery	O
bombardment	O
which	O
would	O
have	O
resulted	O
in	O
many	O
casualties	O
due	O
to	O
friendly	O
fire	O
.	O
On	O
June	O
18	O
,	O
Buckner	PERSON
was	O
killed	O
by	O
enemy	O
artillery	O
fire	O
while	O
monitoring	O
the	O
progress	O
of	O
his	O
troops	O
.	O
Buckner	PERSON
was	O
replaced	O
by	O
Roy	PERSON
Geiger	PERSON
.	O
Upon	O
assuming	O
command	O
,	O
Geiger	PERSON
became	O
the	O
only	O
U.S.	O
Marine	O
to	O
command	O
a	O
numbered	O
army	O
of	O
the	O
U.S.	O
Army	O
in	O
combat	O
.	O
He	O
was	O
relieved	O
five	O
days	O
later	O
by	O
Joseph	PERSON
Stilwell	PERSON
.	O
U.S.	O
losses	O
were	O
over	O
62,000	O
casualties	O
of	O
whom	O
over	O
12,000	O
were	O
killed	O
or	O
missing	O
.	O
This	O
made	O
the	O
battle	O
the	O
bloodiest	O
that	O
U.S.	O
forces	O
experienced	O
in	O
the	O
Pacific	O
war	O
.	O
U.S.	O
forces	O
suffered	O
their	O
highest-ever	O
casualty	O
rate	O
for	O
combat	O
stress	O
reaction	O
during	O
the	O
entire	O
war	O
,	O
at	O
48	O
%	O
,	O
with	O
some	O
14,000	O
soldiers	O
retired	O
due	O
to	O
nervous	O
breakdown	O
.	O
He	O
was	O
the	O
highest-ranking	O
U.S.	O
officer	O
to	O
be	O
killed	O
by	O
enemy	O
fire	O
during	O
the	O
war	O
.	O
Casualties	O
of	O
American	O
ground	O
artillery	O
are	O
unknown	O
.	O
At	O
some	O
battles	O
,	O
such	O
as	O
at	O
Battle	O
of	O
Iwo	O
Jima	O
,	O
there	O
had	O
been	O
no	O
civilians	O
involved	O
,	O
but	O
Okinawa	O
had	O
a	O
large	O
indigenous	O
civilian	O
population	O
and	O
,	O
according	O
to	O
various	O
estimates	O
,	O
somewhere	O
between	O
1/10	O
and	O
1/3	O
of	O
them	O
died	O
during	O
the	O
battle	O
.	O
Okinawan	O
civilian	O
losses	O
in	O
the	O
campaign	O
were	O
estimated	O
to	O
be	O
between	O
42,000	O
and	O
150,000	O
dead	O
(	O
more	O
than	O
100,000	O
according	O
to	O
Okinawa	O
Prefecture	O
)	O
.	O
During	O
the	O
battle	O
,	O
US	O
soldiers	O
found	O
it	O
difficult	O
to	O
distinguish	O
civilians	O
from	O
soldiers	O
.	O
Americans	O
always	O
had	O
great	O
compassion	O
,	O
especially	O
for	O
children	O
.	O
In	O
its	O
history	O
of	O
the	O
war	O
,	O
the	O
Okinawa	O
Prefectural	O
Peace	O
Memorial	O
Museum	O
presents	O
Okinawa	O
as	O
being	O
caught	O
in	O
the	O
fighting	O
between	O
America	O
and	O
Japan	O
.	O
A	O
smaller	O
number	O
of	O
newborn	O
infants	O
fathered	O
by	O
Americans	O
were	O
suffocated	O
.	O
"	O
"	O
It	O
can	O
be	O
said	O
that	O
from	O
the	O
viewpoint	O
of	O
the	O
(	O
Okinawa	O
residents	O
)	O
,	O
they	O
were	O
forced	O
into	O
the	O
mass	O
suicides,	O
"	O
the	O
council	O
report	O
stated	O
.	O
Okinawa	O
provided	O
a	O
fleet	O
anchorage	O
,	O
troop	O
staging	O
areas	O
,	O
and	O
airfields	O
in	O
close	O
proximity	O
to	O
Japan	LOCATION
.	O
Some	O
military	O
historians	O
believe	O
that	O
Okinawa	O
led	O
directly	O
to	O
the	O
atomic	O
bombings	O
of	O
Hiroshima	O
and	O
Nagasaki	O
,	O
as	O
a	O
means	O
of	O
avoiding	O
the	O
planned	O
ground	O
invasion	O
of	O
the	O
Japanese	O
mainland	O
.	O
In	O
1945	O
,	O
Winston	O
Churchill	O
called	O
the	O
battle	O
"	O
among	O
the	O
most	O
intense	O
and	O
famous	O
in	O
military	O
history	O
.	O
"	O
