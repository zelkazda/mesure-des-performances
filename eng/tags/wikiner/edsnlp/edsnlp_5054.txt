Sir	O
Geoffrey	PERSON
Charles	PERSON
Hurst	PERSON
MBE	O
(	O
born	O
8	O
December	O
1941	O
in	O
Ashton-under-Lyne	LOCATION
,	O
Lancashire	O
)	O
is	O
a	O
retired	O
England	O
footballer	O
best	O
remembered	O
for	O
his	O
years	O
with	O
West	O
Ham	O
.	O
He	O
made	O
his	O
mark	O
in	O
World	O
Cup	O
history	O
as	O
the	O
only	O
player	O
to	O
have	O
scored	O
a	O
hat-trick	O
in	O
a	O
World	O
Cup	O
final	O
.	O
The	O
son	O
of	O
a	O
lower-division	O
footballer	O
,	O
Hurst	PERSON
's	O
own	O
footballing	O
career	O
began	O
when	O
he	O
was	O
apprenticed	O
to	O
West	O
Ham	O
United	O
.	O
Hurst	PERSON
was	O
initially	O
a	O
strong-running	O
midfielder	O
but	O
was	O
converted	O
to	O
a	O
centre	O
forward	O
by	O
manager	O
Ron	PERSON
Greenwood	PERSON
.	O
West	O
Ham	O
won	O
the	O
FA	O
Cup	O
in	O
1964	O
with	O
Hurst	PERSON
scoring	O
the	O
second	O
equaliser	O
in	O
a	O
tight	O
and	O
exciting	O
3	O
--	O
2	O
victory	O
over	O
Preston	O
North	O
End	O
at	O
Wembley	O
.	O
A	O
year	O
later	O
,	O
Hurst	PERSON
was	O
back	O
at	O
Wembley	O
for	O
the	O
final	O
of	O
the	O
European	O
Cup	O
Winners	O
Cup	O
against	O
1860	O
Munich	O
,	O
and	O
West	O
Ham	O
won	O
2	O
--	O
0	O
.	O
The	O
following	O
season	O
he	O
was	O
in	O
the	O
West	O
Ham	O
side	O
which	O
lost	O
the	O
League	O
Cup	O
final	O
on	O
aggregate	O
to	O
West	LOCATION
Bromwich	LOCATION
Albion	LOCATION
,	O
and	O
in	O
February	O
1966	O
he	O
was	O
given	O
his	O
debut	O
for	O
England	O
by	O
manager	O
Alf	PERSON
Ramsey	PERSON
.	O
Hurst	O
wound	O
down	O
his	O
career	O
with	O
Stoke	LOCATION
City	LOCATION
and	LOCATION
also	LOCATION
West	LOCATION
Bromwich	LOCATION
Albion	LOCATION
.	O
He	O
then	O
signed	O
for	O
the	O
Seattle	O
Sounders	O
of	O
the	O
NASL	O
in	O
1976	O
.	O
More	O
important	O
than	O
Hurst	O
's	O
numbers	O
was	O
his	O
sense	O
of	O
timing	O
:	O
not	O
only	O
did	O
he	O
score	O
the	O
first	O
and	O
the	O
last	O
goals	O
of	O
the	O
season	O
,	O
5	O
of	O
his	O
8	O
goals	O
were	O
game-winners	O
.	O
Hurst	O
settled	O
into	O
international	O
football	O
quickly	O
but	O
as	O
the	O
World	O
Cup	O
approached	O
,	O
it	O
seemed	O
clear	O
that	O
his	O
inclusion	O
in	O
Ramsey	O
's	O
squad	O
of	O
22	O
would	O
merely	O
be	O
as	O
a	O
different	O
option	O
to	O
the	O
first	O
choice	O
partnership	O
of	O
Jimmy	PERSON
Greaves	PERSON
and	O
Roger	PERSON
Hunt	PERSON
.	O
Greaves	PERSON
and	O
Hunt	PERSON
were	O
indeed	O
picked	O
for	O
the	O
three	O
group	O
games	O
against	O
Uruguay	O
,	O
Mexico	O
and	O
France	O
,	O
but	O
in	O
the	O
latter	O
game	O
,	O
Greaves	O
suffered	O
a	O
deep	O
gash	O
to	O
his	O
leg	O
which	O
required	O
stitches	O
,	O
and	O
Hurst	PERSON
was	O
called	O
up	O
to	O
take	O
his	O
place	O
in	O
the	O
quarter	O
final	O
against	O
Argentina	O
.	O
With	O
captain	O
Bobby	PERSON
Moore	PERSON
and	O
young	O
midfielder	O
Martin	PERSON
Peters	PERSON
already	O
in	O
the	O
side	O
,	O
it	O
completed	O
a	O
trio	O
of	O
West	O
Ham	O
players	O
selected	O
by	O
Ramsey	O
at	O
this	O
most	O
crucial	O
stage	O
of	O
the	O
competition	O
.	O
England	O
won	O
1	O
--	O
0	O
and	O
were	O
in	O
the	O
semi	O
finals	O
.	O
Greaves	O
was	O
not	O
fit	O
for	O
the	O
game	O
against	O
Portugal	O
so	O
Hurst	O
and	O
Hunt	O
continued	O
up	O
front	O
,	O
and	O
England	O
won	O
2	O
--	O
1	O
thanks	O
to	O
a	O
brace	O
from	O
Bobby	PERSON
Charlton	O
,	O
the	O
second	O
of	O
which	O
was	O
set	O
up	O
by	O
Hurst	O
.	O
Ramsey	O
,	O
however	O
,	O
would	O
not	O
be	O
swayed	O
.	O
Hurst	O
had	O
played	O
well	O
enough	O
to	O
keep	O
his	O
place	O
and	O
,	O
with	O
substitutes	O
still	O
disallowed	O
in	O
competitive	O
football	O
,	O
Greaves	O
'	O
hopes	O
of	O
taking	O
part	O
in	O
the	O
final	O
were	O
dashed	O
.	O
Ramsey	O
informed	O
Greaves	O
and	O
Hurst	O
of	O
his	O
decision	O
the	O
day	O
before	O
the	O
game	O
,	O
and	O
would	O
be	O
conclusively	O
vindicated	O
.	O
In	O
the	O
second	O
half	O
,	O
chances	O
went	O
begging	O
for	O
both	O
sides	O
before	O
England	O
won	O
a	O
corner	O
on	O
the	O
right	O
with	O
a	O
quarter	O
of	O
an	O
hour	O
left	O
on	O
the	O
clock	O
.	O
Alan	PERSON
Ball	PERSON
took	O
it	O
,	O
outswinging	O
the	O
ball	O
to	O
Hurst	O
on	O
the	O
edge	O
of	O
the	O
area	O
.	O
Hurst	O
turned	O
to	O
shoot	O
and	O
the	O
ball	O
deflected	O
high	O
into	O
the	O
air	O
,	O
looping	O
down	O
on	O
to	O
the	O
right	O
boot	O
of	O
Peters	PERSON
,	O
who	O
smashed	O
it	O
home	O
.	O
The	O
subsequent	O
30	O
minutes	O
would	O
shape	O
the	O
rest	O
of	O
Hurst	O
's	O
life	O
.	O
In	O
the	O
first	O
period	O
,	O
Ball	O
flicked	O
a	O
pass	O
inside	O
to	O
Hurst	O
in	O
the	O
penalty	O
box	O
who	O
struck	O
a	O
strong	O
shot	O
towards	O
goal	O
with	O
his	O
right	O
foot	O
,	O
falling	O
backwards	O
as	O
he	O
did	O
so	O
.	O
The	O
referee	O
Gottfried	PERSON
Dienst	PERSON
,	O
unsure	O
,	O
decided	O
to	O
consult	O
his	O
linesman	O
,	O
Tofik	PERSON
Bakhramov	PERSON
,	O
on	O
the	O
right	O
flank	O
,	O
who	O
had	O
waved	O
his	O
flag	O
to	O
get	O
the	O
official	O
's	O
attention	O
.	O
The	O
Soviet	O
linesman	O
signalled	O
that	O
the	O
ball	O
had	O
crossed	O
the	O
line	O
,	O
and	O
the	O
goal	O
was	O
given	O
.	O
Ever	O
since	O
,	O
football	O
reporters	O
and	O
commentators	O
on	O
England	O
games	O
have	O
called	O
in	O
jest	O
for	O
a	O
"	O
Russian	O
linesman	O
"	O
whenever	O
there	O
has	O
been	O
a	O
contentious	O
decision	O
to	O
make	O
,	O
especially	O
when	O
that	O
decision	O
has	O
not	O
gone	O
England	O
's	O
way	O
.	O
However	O
,	O
he	O
always	O
believed	O
the	O
ball	O
was	O
in	O
the	O
net	O
because	O
of	O
Hunt	O
's	O
reaction	O
--	O
the	O
Liverpool	O
striker	O
was	O
following	O
in	O
as	O
the	O
ball	O
hit	O
the	O
bar	O
and	O
turned	O
to	O
celebrate	O
a	O
goal	O
instead	O
of	O
trying	O
to	O
knock	O
the	O
rebound	O
into	O
the	O
net	O
.	O
Hurst	O
's	O
argument	O
was	O
that	O
a	O
natural	O
goalscorer	O
such	O
as	O
Hunt	PERSON
would	O
have	O
put	O
the	O
ball	O
into	O
the	O
net	O
himself	O
had	O
he	O
been	O
in	O
any	O
doubt	O
.	O
While	O
spectators	O
ran	O
on	O
the	O
field	O
,	O
Hurst	O
ran	O
on	O
towards	O
the	O
goal	O
,	O
stating	O
later	O
that	O
he	O
intended	O
just	O
to	O
blast	O
it	O
as	O
far	O
away	O
as	O
he	O
could	O
to	O
eat	O
away	O
valuable	O
seconds	O
.	O
Hurst	O
still	O
emerged	O
the	O
hero	O
of	O
the	O
win	O
but	O
,	O
as	O
a	O
result	O
of	O
the	O
third	O
goal	O
,	O
became	O
an	O
icon	O
of	O
world	O
football	O
too	O
.	O
It	O
is	O
stated	O
often	O
that	O
Hurst	O
's	O
hat-trick	O
is	O
technically	O
a	O
"	O
perfect	O
hat-trick	O
"	O
,	O
as	O
he	O
scored	O
with	O
his	O
head	O
,	O
right	O
foot	O
and	O
left	O
foot	O
.	O
The	O
referee	O
had	O
put	O
his	O
whistle	O
to	O
his	O
lips	O
as	O
Moore	O
shaped	O
to	O
play	O
the	O
final	O
pass	O
to	O
Hurst	O
.	O
As	O
Hurst	O
collected	O
the	O
pass	O
,	O
BBC	O
commentator	O
Kenneth	PERSON
Wolstenholme	PERSON
immortalised	O
his	O
own	O
contribution	O
to	O
the	O
day	O
with	O
the	O
most	O
famous	O
piece	O
of	O
football	O
commentary	O
ever	O
:	O
Hurst	O
was	O
immediately	O
jumped	O
on	O
by	O
Alan	PERSON
Ball	PERSON
,	O
the	O
only	O
other	O
player	O
upfield	O
at	O
the	O
time	O
Moore	O
played	O
the	O
pass	O
.	O
Meanwhile	O
,	O
cameras	O
quickly	O
snapped	O
a	O
bemused-looking	O
Greaves	O
in	O
his	O
suit	O
and	O
tie	O
on	O
the	O
England	O
bench	O
,	O
amazed	O
at	O
the	O
achievements	O
of	O
the	O
man	O
who	O
had	O
replaced	O
him	O
.	O
Greaves	O
would	O
later	O
say	O
it	O
was	O
an	O
emotional	O
reaction	O
but	O
he	O
was	O
just	O
as	O
thrilled	O
for	O
Hurst	O
and	O
England	O
as	O
the	O
other	O
squad	O
players	O
who	O
had	O
not	O
been	O
picked	O
for	O
the	O
final	O
.	O
It	O
was	O
n't	O
until	O
the	O
celebratory	O
banquet	O
that	O
evening	O
that	O
Hurst	PERSON
realised	O
he	O
had	O
scored	O
a	O
hat-trick	O
,	O
assuming	O
that	O
the	O
final	O
whistle	O
had	O
been	O
blown	O
before	O
he	O
'd	O
struck	O
the	O
ball	O
into	O
the	O
net	O
for	O
his	O
third	O
goal	O
.	O
He	O
returned	O
it	O
to	O
England	O
more	O
than	O
30	O
years	O
later	O
.	O
As	O
if	O
to	O
prove	O
that	O
life	O
had	O
to	O
go	O
on	O
,	O
Hurst	O
was	O
carrying	O
out	O
the	O
mundane	O
task	O
of	O
mowing	O
his	O
lawn	O
when	O
the	O
journalists	O
turned	O
up	O
.	O
Hurst	O
continued	O
to	O
play	O
and	O
score	O
for	O
England	O
,	O
and	O
although	O
he	O
won	O
no	O
further	O
honours	O
with	O
West	O
Ham	O
or	O
England	O
in	O
the	O
1960s	O
he	O
still	O
maintained	O
his	O
England	O
place	O
for	O
much	O
of	O
the	O
period	O
;	O
for	O
the	O
2	O
--	O
3	O
seasons	O
immediately	O
after	O
1966	O
he	O
was	O
an	O
internationally	O
renowned	O
striker	O
and	O
goalscorer	O
.	O
In	O
1972	O
,	O
West	O
Ham	O
reached	O
the	O
semi	O
final	O
of	O
the	O
League	O
Cup	O
when	O
they	O
played	O
Stoke	LOCATION
City	LOCATION
over	O
two	O
legs	O
.	O
In	O
the	O
home	O
leg	O
for	O
West	O
Ham	O
,	O
they	O
were	O
awarded	O
a	O
penalty	O
which	O
Hurst	O
took	O
.	O
He	O
powerful	O
shot	O
into	O
the	O
top	O
corner	O
was	O
saved	O
by	O
Stoke	O
goalkeeper	O
and	O
Hurst	O
's	O
international	O
team-mate	O
Gordon	PERSON
Banks	PERSON
,	O
who	O
succeeded	O
in	O
deflecting	O
the	O
ball	O
over	O
the	O
bar	O
.	O
Stoke	O
won	O
the	O
tie	O
and	O
ultimately	O
the	O
competition	O
.	O
Hurst	O
left	O
West	LOCATION
Ham	LOCATION
to	O
join	O
them	O
later	O
the	O
same	O
year	O
for	O
£	O
75	O
,000	O
.	O
He	O
had	O
played	O
one	O
game	O
short	O
of	O
500	O
for	O
West	O
Ham	O
,	O
during	O
which	O
time	O
he	O
had	O
scored	O
252	O
goals	O
.	O
He	O
had	O
won	O
49	O
caps	O
and	O
scored	O
24	O
goals	O
,	O
currently	O
putting	O
him	O
11th	O
in	O
the	O
all-time	O
England	O
scorers	O
'	O
list	O
.	O
Upon	O
his	O
retirement	O
from	O
playing	O
,	O
Hurst	O
moved	O
into	O
management	O
and	O
coaching	O
.	O
He	O
was	O
assistant	O
to	O
his	O
ex-West	O
Ham	O
boss	O
Ron	PERSON
Greenwood	PERSON
after	O
the	O
latter	O
took	O
over	O
the	O
England	O
job	O
in	O
1977	O
,	O
player-manager	O
of	O
Telford	O
United	O
and	O
manager	O
of	O
Chelsea	O
from	O
1979	O
--	O
81	O
.	O
When	O
Blanchflower	O
was	O
sacked	O
,	O
Hurst	PERSON
was	O
appointed	O
manager	O
.	O
Things	O
initially	O
went	O
well	O
,	O
and	O
for	O
much	O
of	O
the	O
season	O
Chelsea	O
were	O
on	O
course	O
for	O
promotion	O
,	O
but	O
two	O
wins	O
from	O
their	O
final	O
seven	O
league	O
games	O
ensured	O
the	O
club	O
finished	O
4th	O
.	O
It	O
was	O
a	O
far	O
cry	O
from	O
a	O
decade	O
earlier	O
,	O
when	O
Chelsea	O
had	O
been	O
European	O
Cup	O
Winners	O
'	O
Cup	O
winners	O
.	O
England	O
fans	O
claim	O
that	O
the	O
achievement	O
of	O
Geoff	PERSON
Hurst	PERSON
remains	O
unique	O
in	O
global	O
football	O
,	O
while	O
others	O
[	O
citation	O
needed	O
]	O
point	O
out	O
that	O
he	O
scored	O
only	O
one	O
undisputed	O
goal	O
at	O
full	O
time	O
,	O
plus	O
a	O
controversial	O
one	O
in	O
extra	O
time	O
.	O
Since	O
1966	O
,	O
only	O
three	O
players	O
have	O
come	O
close	O
to	O
emulating	O
Hurst	PERSON
's	O
hat-trick	O
in	O
a	O
World	O
Cup	O
final	O
.	O
Hurst	PERSON
is	O
also	O
one	O
of	O
the	O
few	O
footballers	O
who	O
have	O
been	O
knighted	O
,	O
and	O
this	O
recognises	O
his	O
contribution	O
to	O
the	O
game	O
.	O
In	O
popular	O
culture	O
,	O
a	O
shot	O
bouncing	O
off	O
the	O
crossbar	O
and	O
hitting	O
the	O
line	O
is	O
referred	O
to	O
as	O
a	O
"	O
Geoff	O
Hurst	O
style	O
shot	O
"	O
if	O
no	O
goal	O
is	O
given	O
,	O
or	O
a	O
"	O
Geoff	PERSON
Hurst	PERSON
style	O
goal	O
"	O
.	O
For	O
the	O
purpose	O
of	O
the	O
stunt	O
Hurst	PERSON
acted	O
out	O
a	O
confession	O
that	O
he	O
'd	O
known	O
all	O
along	O
that	O
the	O
ball	O
had	O
not	O
crossed	O
the	O
line	O
.	O
In	O
1975	O
Hurst	PERSON
was	O
decorated	O
with	O
the	O
MBE	O
.	O
In	O
later	O
years	O
,	O
Hurst	PERSON
became	O
a	O
successful	O
businessman	O
,	O
working	O
in	O
the	O
insurance	O
industry	O
.	O
