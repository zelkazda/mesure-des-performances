He	O
sparked	O
a	O
debate	O
with	O
conservative	O
bioethicist	O
Leon	PERSON
Kass	PERSON
,	O
who	O
wrote	O
at	O
the	O
time	O
that	O
"	O
the	O
programmed	O
reproduction	O
of	O
man	O
will	O
,	O
in	O
fact	O
,	O
dehumanize	O
him	O
.	O
"	O
Dr.	O
Preston	O
Estep	PERSON
has	O
suggested	O
the	O
terms	O
"	O
replacement	O
cloning	O
"	O
to	O
describe	O
the	O
generation	O
of	O
a	O
clone	O
of	O
a	O
previously	O
living	O
person	O
,	O
and	O
"	O
persistence	O
cloning	O
"	O
to	O
describe	O
the	O
production	O
of	O
a	O
cloned	O
body	O
for	O
the	O
purpose	O
of	O
obviating	O
aging	O
,	O
although	O
he	O
maintains	O
that	O
such	O
procedures	O
currently	O
should	O
be	O
considered	O
science	O
fiction	O
[	O
citation	O
needed	O
]	O
and	O
current	O
cloning	O
techniques	O
risk	O
producing	O
a	O
prematurely	O
aged	O
child	O
.	O
In	O
Aubrey	PERSON
de	O
Grey	O
's	O
proposed	O
SENS	O
,	O
one	O
of	O
the	O
considered	O
options	O
to	O
repair	O
the	O
cell	O
depletion	O
related	O
to	O
cellular	O
senescence	O
is	O
to	O
grow	O
replacement	O
tissues	O
from	O
stem	O
cells	O
harvested	O
from	O
a	O
cloned	O
embryo	O
.	O
On	O
December	O
12	O
,	O
2001	O
,	O
the	O
United	O
Nations	O
General	O
Assembly	O
began	O
elaborating	O
an	O
international	O
convention	O
against	O
the	O
reproductive	O
cloning	O
of	O
humans	O
.	O
The	O
charter	O
is	O
legally	O
binding	O
for	O
the	O
institutions	O
of	O
the	O
European	O
Union	O
under	O
the	O
Treaty	O
of	O
Lisbon	O
.	O
Some	O
American	O
states	O
ban	O
both	O
forms	O
of	O
cloning	O
,	O
while	O
some	O
others	O
outlaw	O
only	O
reproductive	O
cloning	O
.	O
The	O
2000	O
Arnold	PERSON
Schwarzenegger	PERSON
film	O
The	O
6th	O
Day	O
and	O
2005	O
The	O
Island	O
,	O
directed	O
by	O
Michael	PERSON
Bay	PERSON
,	O
also	O
explores	O
the	O
theme	O
of	O
human	O
cloning	O
.	O
An	O
episode	O
of	O
Star	O
Trek	O
:	O
Enterprise	O
(	O
Similitude	O
)	O
deals	O
with	O
the	O
moral	O
and	O
ethical	O
issues	O
surrounding	O
growing	O
a	O
human	O
clone	O
to	O
harvest	O
tissue	O
for	O
an	O
injured	O
crewman	O
.	O
The	O
famous	O
video	O
game	O
franchise	O
Metal	O
Gear	O
Solid	O
,	O
also	O
revolves	O
around	O
the	O
concept	O
of	O
cloning	O
and	O
genetic	O
alteration	O
.	O
This	O
series	O
was	O
adapted	O
years	O
later	O
into	O
the	O
first	O
part	O
of	O
Robotech	O
(	O
1985	O
)	O
,	O
where	O
the	O
aliens	O
remained	O
the	O
same	O
but	O
had	O
a	O
different	O
origin	O
.	O
