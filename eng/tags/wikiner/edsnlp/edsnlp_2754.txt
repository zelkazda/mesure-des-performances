The	O
data	O
files	O
used	O
by	O
games	O
such	O
as	O
Doom	O
and	O
Quake	O
are	O
examples	O
of	O
this	O
.	O
For	O
instance	O
,	O
Microsoft	O
Word	O
files	O
are	O
normally	O
created	O
and	O
modified	O
by	O
the	O
Microsoft	O
Word	O
program	O
in	O
response	O
to	O
user	O
commands	O
,	O
but	O
the	O
user	O
can	O
also	O
move	O
,	O
rename	O
,	O
or	O
delete	O
these	O
files	O
directly	O
by	O
using	O
a	O
file	O
manager	O
program	O
such	O
as	O
Windows	O
Explorer	O
(	O
on	O
Windows	O
computers	O
)	O
.	O
Microsoft	O
Windows	O
supports	O
multiple	O
file	O
systems	O
,	O
each	O
with	O
different	O
policies	O
regarding	O
case-sensitivity	O
.	O
The	O
common	O
FAT	O
file	O
system	O
can	O
have	O
multiple	O
files	O
whose	O
names	O
differ	O
only	O
in	O
case	O
if	O
the	O
user	O
uses	O
a	O
disk	O
editor	O
to	O
edit	O
the	O
file	O
names	O
in	O
the	O
directory	O
entries	O
.	O
On	O
Windows	O
computers	O
,	O
extensions	O
consist	O
of	O
a	O
dot	O
(	O
period	O
)	O
at	O
the	O
end	O
of	O
a	O
file	O
name	O
,	O
followed	O
by	O
a	O
few	O
letters	O
to	O
identify	O
the	O
type	O
of	O
file	O
.	O
An	O
extension	O
of	O
.txt	O
identifies	O
a	O
text	O
file	O
;	O
a	O
.doc	O
extension	O
identifies	O
any	O
type	O
of	O
document	O
or	O
documentation	O
,	O
commonly	O
in	O
the	O
Microsoft	O
Word	O
file	O
format	O
;	O
and	O
so	O
on	O
.	O
Under	O
Windows	O
,	O
the	O
most	O
commonly	O
used	O
file	O
manager	O
program	O
is	O
Windows	O
Explorer	O
.	O
Systems	O
like	O
the	O
1962	O
Compatible	O
Time-Sharing	O
System	O
featured	O
file	O
systems	O
,	O
which	O
gave	O
the	O
appearance	O
of	O
several	O
"	O
files	O
"	O
on	O
one	O
storage	O
device	O
,	O
leading	O
to	O
the	O
modern	O
usage	O
of	O
the	O
term	O
.	O
File	O
names	O
in	O
CTSS	O
had	O
two	O
parts	O
,	O
a	O
user-readable	O
"	O
primary	O
name	O
"	O
and	O
a	O
"	O
secondary	O
name	O
"	O
indicating	O
the	O
file	O
type	O
.	O
This	O
convention	O
remains	O
in	O
use	O
by	O
several	O
operating	O
systems	O
today	O
,	O
including	O
Microsoft	O
Windows	O
.	O
