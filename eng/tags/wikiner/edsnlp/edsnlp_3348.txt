The	O
discovery	O
of	O
differential	O
cryptanalysis	O
is	O
generally	O
attributed	O
to	O
Eli	PERSON
Biham	PERSON
and	O
Adi	PERSON
Shamir	PERSON
in	O
the	O
late	O
1980s	O
,	O
who	O
published	O
a	O
number	O
of	O
attacks	O
against	O
various	O
block	O
ciphers	O
and	O
hash	O
functions	O
,	O
including	O
a	O
theoretical	O
weakness	O
in	O
the	O
Data	O
Encryption	O
Standard	O
(	O
DES	O
)	O
.	O
While	O
DES	O
was	O
designed	O
with	O
resistance	O
to	O
differential	O
cryptanalysis	O
in	O
mind	O
,	O
other	O
contemporary	O
ciphers	O
proved	O
to	O
be	O
vulnerable	O
.	O
An	O
early	O
target	O
for	O
the	O
attack	O
was	O
the	O
FEAL	O
block	O
cipher	O
.	O
The	O
original	O
proposed	O
version	O
with	O
four	O
rounds	O
(	O
FEAL-4	O
)	O
can	O
be	O
broken	O
using	O
only	O
eight	O
chosen	O
plaintexts	O
,	O
and	O
even	O
a	O
31-round	O
version	O
of	O
FEAL	O
is	O
susceptible	O
to	O
the	O
attack	O
.	O
The	O
scheme	O
can	O
successfully	O
cryptanalyze	O
DES	O
with	O
an	O
effort	O
on	O
the	O
order	O
2	O
47	O
chosen	O
plaintexts	O
.	O
New	O
designs	O
are	O
expected	O
to	O
be	O
accompanied	O
by	O
evidence	O
that	O
the	O
algorithm	O
is	O
resistant	O
to	O
this	O
attack	O
,	O
and	O
many	O
,	O
including	O
the	O
Advanced	O
Encryption	O
Standard	O
,	O
have	O
been	O
proven	O
secure	O
against	O
the	O
attack	O
.	O
