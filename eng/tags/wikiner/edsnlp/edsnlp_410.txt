He	O
was	O
born	O
into	O
a	O
family	O
of	O
writers	O
,	O
the	O
most	O
well	O
known	O
of	O
whom	O
was	O
his	O
paternal	O
aunt	O
,	O
Anna	PERSON
Letitia	PERSON
Barbauld	PERSON
,	O
a	O
woman	O
of	O
letters	O
who	O
wrote	O
poetry	O
and	O
essays	O
as	O
well	O
as	O
early	O
children	O
's	O
literature	O
.	O
His	O
father	O
,	O
Dr.	O
John	PERSON
Aikin	PERSON
,	O
was	O
a	O
medical	O
doctor	O
,	O
historian	O
,	O
and	O
author	O
.	O
His	O
sister	O
was	O
Lucy	PERSON
Aikin	PERSON
(	O
1781	O
--	O
1864	O
)	O
,	O
a	O
historical	O
writer	O
.	O
Arthur	PERSON
Aikin	PERSON
studied	O
chemistry	O
under	O
Joseph	PERSON
Priestley	PERSON
in	O
the	O
New	O
College	O
at	O
Hackney	LOCATION
,	O
and	O
gave	O
attention	O
to	O
the	O
practical	O
applications	O
of	O
the	O
science	O
.	O
He	O
was	O
one	O
of	O
the	O
founders	O
of	O
the	O
Geological	O
Society	O
of	O
London	O
in	O
1807	O
and	O
was	O
its	O
honorary	O
secretary	O
in	O
1812-1817	O
.	O
He	O
contributed	O
papers	O
on	O
the	O
Wrekin	O
and	O
the	O
Shropshire	O
coalfield	O
,	O
among	O
others	O
,	O
to	O
the	O
transactions	O
of	O
that	O
society	O
.	O
Later	O
he	O
became	O
secretary	O
of	O
the	O
Royal	O
Society	O
of	O
Arts	O
.	O
He	O
was	O
founder	O
of	O
the	O
Chemical	O
Society	O
of	O
London	O
in	O
1841	O
,	O
being	O
its	O
first	O
Treasurer	O
and	O
second	O
President	O
.	O
He	O
died	O
in	O
London	LOCATION
.	O
