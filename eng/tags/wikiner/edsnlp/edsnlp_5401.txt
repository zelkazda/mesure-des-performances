Modern	O
humans	O
entered	O
the	O
Iberian	O
Peninsula	O
about	O
32,000	O
years	O
ago	O
.	O
But	O
there	O
are	O
also	O
other	O
opinions	O
in	O
connection	O
with	O
the	O
process	O
of	O
the	O
Reconquista	O
.	O
The	O
Kingdom	O
of	O
Spain	O
was	O
created	O
in	O
1492	O
with	O
the	O
unification	O
of	O
the	O
Kingdom	O
of	O
Castile	O
and	O
the	O
Kingdom	O
of	O
Aragon	O
.	O
It	O
was	O
the	O
most	O
powerful	O
state	O
in	O
Europe	O
and	O
the	O
foremost	O
global	O
power	O
during	O
the	O
16th	O
century	O
and	O
the	O
greater	O
part	O
of	O
the	O
17th	O
century	O
.	O
The	O
eighteenth	O
century	O
saw	O
a	O
new	O
dynasty	O
,	O
the	O
Bourbons	O
,	O
which	O
directed	O
considerable	O
efforts	O
towards	O
the	O
renewal	O
of	O
state	O
institutions	O
,	O
with	O
some	O
success	O
,	O
finishing	O
in	O
a	O
successful	O
involvement	O
in	O
the	O
American	O
War	O
of	O
Independence	O
.	O
Nationalist	O
movements	O
emerged	O
in	O
the	O
last	O
significant	O
remnants	O
of	O
the	O
old	O
empire	O
(	O
Cuba	O
and	O
the	O
Philippines	O
)	O
which	O
led	O
to	O
a	O
brief	O
war	O
with	O
the	O
U.S.	O
and	O
the	O
loss	O
of	O
the	O
remaining	O
old	O
colonies	O
at	O
the	O
end	O
of	O
the	O
century	O
.	O
While	O
tensions	O
remain	O
,	O
modern	O
Spain	O
has	O
seen	O
the	O
development	O
of	O
a	O
robust	O
,	O
modern	O
democracy	O
as	O
a	O
constitutional	O
monarchy	O
with	O
popular	O
King	O
Juan	O
Carlos	O
,	O
one	O
of	O
the	O
fastest-growing	O
standards	O
of	O
living	O
in	O
Europe	O
,	O
entry	O
into	O
the	O
European	O
Community	O
,	O
and	O
the	O
1992	O
Summer	O
Olympics	O
.	O
Around	O
1100	O
BC	O
,	O
Phoenician	O
merchants	O
founded	O
the	O
trading	O
colony	O
of	O
Gadir	O
or	O
Gades	O
(	O
modern	O
day	O
Cádiz	O
)	O
near	O
Tartessos	LOCATION
.	O
Their	O
most	O
important	O
colony	O
was	O
Carthago	LOCATION
Nova	LOCATION
.	O
Hispania	O
was	O
divided	O
:	O
Hispania	O
Ulterior	O
and	O
Hispania	O
Citerior	O
during	O
the	O
late	O
Roman	O
Republic	O
;	O
and	O
,	O
during	O
the	O
Roman	O
Empire	O
,	O
Hispania	O
Taraconensis	O
in	O
the	O
northeast	O
,	O
Hispania	O
Baetica	O
in	O
the	O
south	O
(	O
roughly	O
corresponding	O
to	O
Andalucia	O
)	O
,	O
and	O
Lusitania	O
in	O
the	O
southwest	O
(	O
corresponding	O
to	O
modern	O
Portugal	O
)	O
.	O
In	O
the	O
winter	O
of	O
406	O
,	O
taking	O
advantage	O
of	O
the	O
frozen	O
Rhine	O
,	O
the	O
Vandals	O
and	O
Sueves	O
,	O
and	O
the	O
(	O
Sarmatian	O
)	O
Alans	O
invaded	O
the	O
empire	O
in	O
force	O
.	O
Caliph	PERSON
Al-Walid	PERSON
I	O
paid	O
great	O
attention	O
to	O
the	O
expansion	O
of	O
an	O
organized	O
military	O
,	O
building	O
the	O
strongest	O
navy	O
in	O
the	O
Umayyad	LOCATION
Caliphate	LOCATION
era	LOCATION
.	O
Caliph	PERSON
Al-Walid	PERSON
I	O
's	O
reign	O
is	O
considered	O
as	O
the	O
apex	O
of	O
Islamic	O
power	O
.	O
Emir	PERSON
Abd-ar-rahman	PERSON
I	O
challenged	O
the	O
Abbasids	O
.	O
The	O
first	O
navy	O
of	O
the	O
Caliph	O
of	O
Cordoba	LOCATION
or	LOCATION
Emir	LOCATION
was	O
built	O
after	O
the	O
humiliating	O
Viking	O
ascent	O
of	O
the	O
Guadalquivir	O
in	O
844	O
when	O
they	O
sacked	O
Seville	LOCATION
.	O
In	O
942	O
,	O
pagan	O
Magyars	O
(	O
present	O
day	O
Hungary	O
)	O
raided	O
across	O
Europe	O
as	O
far	O
west	O
as	O
Al-Andalus	O
.	O
(	O
See	O
:	O
Emir	O
Abd-ar-Rahman	O
III	O
912	O
;	O
the	O
Granada	O
massacre	O
1066	O
)	O
.	O
Muslim	O
interest	O
in	O
the	O
peninsula	O
returned	O
in	O
force	O
around	O
the	O
year	O
1000	O
when	O
Al-Mansur	O
(	O
known	O
as	O
Almanzor	O
)	O
,	O
sacked	O
Barcelona	LOCATION
(	O
985	O
)	O
.	O
In	O
the	O
12th	O
century	O
the	O
Almoravid	O
empire	O
broke	O
up	O
again	O
,	O
only	O
to	O
be	O
taken	O
over	O
by	O
the	O
Almohad	O
invasion	O
,	O
who	O
were	O
defeated	O
in	O
the	O
decisive	O
battle	O
of	O
Las	O
Navas	O
de	O
Tolosa	O
in	O
1212	O
.	O
By	O
the	O
15th	O
century	O
,	O
the	O
most	O
important	O
among	O
these	O
were	O
the	O
Kingdom	O
of	O
Castile	O
(	O
occupying	O
a	O
northern	O
and	O
central	O
portion	O
of	O
the	O
Iberian	O
Peninsula	O
)	O
and	O
the	O
Kingdom	O
of	O
Aragon	O
(	O
occupying	O
northeastern	O
portions	O
of	O
the	O
peninsula	O
)	O
.	O
The	O
rulers	O
of	O
these	O
two	O
kingdoms	O
were	O
allied	O
with	O
dynastic	O
families	O
in	O
Portugal	O
,	O
France	O
,	O
and	O
other	O
neighboring	O
kingdoms	O
.	O
Following	O
the	O
War	O
of	O
the	O
Castilian	O
Succession	O
,	O
Isabella	PERSON
retained	O
the	O
throne	O
,	O
and	O
ruled	O
jointly	O
with	O
her	O
husband	O
,	O
King	PERSON
Ferdinand	PERSON
II	PERSON
.	O
They	O
married	O
in	O
1469	O
in	O
Valladolid	LOCATION
,	O
uniting	O
both	O
crowns	O
and	O
effectively	O
leading	O
to	O
the	O
creation	O
of	O
the	O
Kingdom	O
of	O
Spain	O
,	O
at	O
the	O
dawn	O
of	O
the	O
modern	O
era	O
.	O
Isabella	PERSON
ensured	O
long-term	O
political	O
stability	O
in	O
Spain	O
by	O
arranging	O
strategic	O
marriages	O
for	O
each	O
of	O
her	O
five	O
children	O
.	O
Her	O
fifth	O
child	O
,	O
Catherine	PERSON
,	O
married	O
Henry	O
VIII	O
,	O
King	O
of	O
England	O
and	O
was	O
mother	O
to	O
Queen	O
Mary	PERSON
I	PERSON
.	O
The	O
final	O
step	O
was	O
taken	O
by	O
the	O
Catholic	O
Monarchs	O
,	O
who	O
,	O
in	O
1492	O
,	O
ordered	O
the	O
remaining	O
Jews	O
to	O
convert	O
or	O
face	O
expulsion	O
from	O
Spain	O
.	O
Depending	O
on	O
different	O
sources	O
,	O
the	O
number	O
of	O
Jews	O
actually	O
expelled	O
is	O
estimated	O
to	O
be	O
anywhere	O
from	O
40,000	O
to	O
120,000	O
people	O
.	O
In	O
1492	O
,	O
under	O
the	O
Catholic	O
Monarchs	O
,	O
the	O
first	O
edition	O
of	O
the	O
Grammar	O
of	O
the	O
Castilian	O
Language	O
by	O
Antonio	O
de	O
Nebrija	O
was	O
published	O
.	O
The	O
Spanish	O
Empire	O
was	O
one	O
of	O
the	O
first	O
modern	O
global	O
empires	O
.	O
This	O
American	O
empire	O
was	O
at	O
first	O
a	O
disappointment	O
,	O
as	O
the	O
natives	O
had	O
little	O
to	O
trade	O
,	O
though	O
settlement	O
did	O
encourage	O
trade	O
.	O
Charles	O
V	O
became	O
king	O
in	O
1516	O
,	O
and	O
the	O
history	O
of	O
Spain	O
became	O
even	O
more	O
firmly	O
enmeshed	O
with	O
the	O
dynastic	O
struggles	O
in	O
Europe	O
.	O
Formentera	O
was	O
even	O
temporarily	O
left	O
by	O
its	O
population	O
.	O
The	O
most	O
famous	O
corsair	O
was	O
the	O
Turkish	O
Barbarossa	O
(	O
"	O
Redbeard	O
"	O
)	O
.	O
Government	O
policy	O
was	O
dominated	O
by	O
favorites	O
,	O
but	O
it	O
was	O
also	O
the	O
reign	O
in	O
which	O
the	O
geniuses	O
of	O
Cervantes	O
and	O
El	O
Greco	O
flourished	O
.	O
The	O
Spanish	O
Golden	O
Age	O
was	O
a	O
period	O
of	O
flourishing	O
arts	O
and	O
letters	O
in	O
the	O
Spanish	O
Empire	O
,	O
coinciding	O
with	O
the	O
political	O
decline	O
and	O
fall	O
of	O
the	O
Habsburgs	O
(	O
Philip	O
III	O
,	O
Philip	O
IV	O
and	O
Charles	O
II	O
)	O
.	O
The	O
Habsburgs	O
,	O
both	O
in	O
Spain	O
and	O
Austria	O
,	O
were	O
great	O
patrons	O
of	O
art	O
in	O
their	O
countries	O
.	O
El	O
Escorial	O
,	O
the	O
great	O
royal	O
monastery	O
built	O
by	O
King	O
Philip	O
II	O
,	O
invited	O
the	O
attention	O
of	O
some	O
of	O
Europe	O
's	O
greatest	O
architects	O
and	O
painters	O
.	O
Spanish	O
literature	O
blossomed	O
as	O
well	O
,	O
most	O
famously	O
demonstrated	O
in	O
the	O
work	O
of	O
Miguel	PERSON
de	O
Cervantes	O
,	O
the	O
author	O
of	O
Don	O
Quixote	O
de	O
la	O
Mancha	O
.	O
However	O
,	O
the	O
reforming	O
spirit	O
of	O
Charles	O
III	O
was	O
extinguished	O
in	O
the	O
reign	O
of	O
his	O
son	O
,	O
Charles	O
IV	O
,	O
seen	O
by	O
some	O
as	O
mentally	O
handicapped	O
.	O
The	O
chaos	O
unleashed	O
by	O
the	O
Napoleonic	O
intervention	O
would	O
cause	O
this	O
gap	O
to	O
widen	O
greatly	O
.	O
In	O
1820	O
,	O
an	O
expedition	O
intended	O
for	O
the	O
colonies	O
revolted	O
in	O
Cadiz	LOCATION
.	O
Ferdinand	O
himself	O
was	O
placed	O
under	O
effective	O
house	O
arrest	O
for	O
the	O
duration	O
of	O
the	O
liberal	O
experiment	O
.	O
France	O
crushed	O
the	O
liberal	O
government	O
with	O
massive	O
force	O
in	O
the	O
so-called	O
Spanish	O
expedition	O
,	O
and	O
Ferdinand	O
was	O
restored	O
as	O
absolute	O
monarch	O
.	O
While	O
Ferdinand	O
aligned	O
with	O
the	O
conservatives	O
,	O
fearing	O
another	O
national	O
insurrection	O
,	O
he	O
did	O
not	O
view	O
the	O
reactionary	O
policies	O
of	O
his	O
brother	O
as	O
a	O
viable	O
option	O
.	O
Carlos	PERSON
,	O
who	O
made	O
known	O
his	O
intent	O
to	O
resist	O
the	O
sanction	O
,	O
fled	O
to	O
Portugal	O
.	O
Carlos	PERSON
then	O
named	O
the	O
Basque	O
general	O
Tomás	O
de	PERSON
Zumalacárregui	PERSON
his	O
commander-in-chief	O
.	O
Espartero	O
's	O
liberal	O
reforms	O
were	O
opposed	O
,	O
then	O
,	O
by	O
moderates	O
;	O
the	O
former	O
general	O
's	O
heavy-handedness	O
caused	O
a	O
series	O
of	O
sporadic	O
uprisings	O
throughout	O
the	O
country	O
from	O
various	O
quarters	O
,	O
all	O
of	O
which	O
were	O
bloodily	O
suppressed	O
.	O
He	O
was	O
overthrown	O
as	O
regent	O
in	O
1843	O
by	O
Ramón	PERSON
María	PERSON
Narváez	PERSON
,	O
a	O
moderate	O
,	O
who	O
was	O
in	O
turn	O
perceived	O
as	O
too	O
reactionary	O
.	O
Isabella	PERSON
's	O
plan	O
failed	O
and	O
cost	O
Isabella	PERSON
more	O
prestige	O
and	O
favor	O
with	O
the	O
people	O
.	O
Isabella	PERSON
was	O
driven	O
into	O
exile	O
in	O
Paris	LOCATION
.	O
There	O
were	O
calls	O
for	O
socialist	O
revolution	O
from	O
the	O
International	O
Workingmen	O
's	O
Association	O
,	O
revolts	O
and	O
unrest	O
in	O
the	O
autonomous	O
regions	O
of	O
Navarre	O
and	O
Catalonia	O
,	O
and	O
pressure	O
from	O
the	O
Roman	O
Catholic	O
Church	O
against	O
the	O
fledgling	O
republic	O
.	O
His	O
death	O
in	O
1885	O
,	O
followed	O
by	O
the	O
assassination	O
of	O
Canovas	PERSON
del	PERSON
Castillo	PERSON
in	O
1897	O
,	O
destabilized	O
the	O
government	O
.	O
American	O
interests	O
in	O
the	O
island	O
,	O
coupled	O
with	O
concerns	O
for	O
the	O
people	O
of	O
Cuba	O
,	O
aggravated	O
relations	O
between	O
the	O
two	O
countries	O
.	O
A	O
revolt	O
in	O
1909	O
in	O
Catalonia	LOCATION
was	O
bloodily	O
suppressed	O
.	O
(	O
See	O
Abd	O
el-Krim	O
,	O
Annual	O
)	O
.	O
In	O
joint	O
action	O
with	O
France	O
,	O
the	O
Moroccan	O
territory	O
was	O
recovered	O
(	O
1925	O
--	O
1927	O
)	O
,	O
but	O
in	O
1930	O
bankruptcy	O
and	O
massive	O
unpopularity	O
left	O
the	O
king	O
no	O
option	O
but	O
to	O
force	O
Primo	PERSON
de	PERSON
Rivera	PERSON
to	O
resign	O
.	O
Under	O
the	O
Second	O
Spanish	O
Republic	O
,	O
women	O
were	O
allowed	O
to	O
vote	O
in	O
general	O
elections	O
for	O
the	O
first	O
time	O
.	O
Economic	O
turmoil	O
,	O
substantial	O
debt	O
inherited	O
from	O
the	O
Primo	O
de	O
Rivera	O
regime	O
,	O
and	O
fractious	O
,	O
rapidly	O
changing	O
governing	O
coalitions	O
led	O
to	O
serious	O
political	O
unrest	O
.	O
In	O
1933	O
,	O
the	O
right-wing	O
CEDA	O
won	O
power	O
;	O
an	O
armed	O
rising	O
of	O
workers	O
of	O
October	O
1934	O
,	O
which	O
reached	O
its	O
greatest	O
intensity	O
in	O
Asturias	LOCATION
and	LOCATION
Catalonia	LOCATION
,	O
was	O
forcefully	O
put	O
down	O
by	O
the	O
CEDA	O
government	O
.	O
In	O
the	O
1930s	O
,	O
Spanish	O
politics	O
were	O
polarized	O
at	O
the	O
left	O
and	O
right	O
of	O
the	O
political	O
spectrum	O
.	O
The	O
right-wing	O
groups	O
,	O
the	O
largest	O
of	O
which	O
was	O
CEDA	O
,	O
a	O
right	O
wing	O
Roman	O
Catholic	O
coalition	O
,	O
held	O
opposing	O
views	O
on	O
most	O
issues	O
.	O
In	O
1936	O
,	O
the	O
left	O
united	O
in	O
the	O
Popular	O
Front	O
and	O
was	O
elected	O
to	O
power	O
.	O
On	O
the	O
other	O
side	O
,	O
right	O
wing	O
militias	O
(	O
such	O
as	O
the	O
Falange	O
)	O
and	O
gunmen	O
hired	O
by	O
employers	O
assassinated	O
left	O
wing	O
activists	O
.	O
The	O
Siege	O
of	O
the	O
Alcázar	O
at	O
Toledo	O
early	O
in	O
the	O
war	O
was	O
a	O
turning	O
point	O
,	O
with	O
the	O
Nationalists	O
winning	O
after	O
a	O
long	O
siege	O
.	O
Soon	O
,	O
though	O
,	O
the	O
Nationalists	O
began	O
to	O
erode	O
their	O
territory	O
,	O
starving	O
Madrid	O
and	O
making	O
inroads	O
into	O
the	O
east	O
.	O
The	O
north	O
,	O
including	O
the	O
Basque	O
country	O
fell	O
in	O
late	O
1937	O
and	O
the	O
Aragon	O
front	O
collapsed	O
shortly	O
afterwards	O
.	O
The	O
bombing	O
of	O
Guernica	O
was	O
probably	O
the	O
most	O
infamous	O
event	O
of	O
the	O
war	O
and	O
inspired	O
Picasso	O
's	O
painting	O
.	O
When	O
this	O
failed	O
and	O
Barcelona	O
fell	O
to	O
the	O
Nationalists	O
in	O
early	O
1939	O
,	O
it	O
was	O
clear	O
the	O
war	O
was	O
over	O
.	O
Spanish	O
rule	O
in	O
Morocco	O
ended	O
in	O
1967	O
.	O
Spanish	O
Guinea	O
was	O
granted	O
independence	O
as	O
Equatorial	O
Guinea	O
in	O
1968	O
,	O
while	O
the	O
Moroccan	O
enclave	O
of	O
Ifni	O
had	O
been	O
ceded	O
to	O
Morocco	O
in	O
1969	O
.	O
Francisco	PERSON
Franco	PERSON
ruled	O
until	O
his	O
death	O
on	O
20	O
November	O
1975	O
,	O
when	O
control	O
was	O
given	O
to	O
King	O
Juan	PERSON
Carlos	PERSON
.	O
In	O
the	O
last	O
few	O
months	O
before	O
Franco	O
's	O
death	O
,	O
the	O
Spanish	O
state	O
went	O
into	O
a	O
paralysis	O
.	O
in	O
1981	O
the	O
23-F	O
coup	O
d'état	O
attempt	O
took	O
place	O
.	O
Officially	O
,	O
the	O
coup	O
d'état	O
failed	O
thanks	O
to	O
the	O
intervention	O
of	O
King	O
Juan	PERSON
Carlos	PERSON
.	O
On	O
11	O
March	O
2004	O
a	O
number	O
of	O
terrorist	O
bombs	O
exploded	O
on	O
busy	O
commuter	O
trains	O
in	O
Madrid	LOCATION
during	O
the	O
morning	O
rush-hour	O
days	O
before	O
the	O
general	O
election	O
,	O
killing	O
191	O
persons	O
and	O
injuring	O
thousands	O
.	O
Although	O
José	O
María	O
Aznar	O
and	O
his	O
ministers	O
were	O
quick	O
to	O
accuse	O
ETA	O
of	O
the	O
atrocity	O
,	O
soon	O
afterwards	O
it	O
became	O
apparent	O
that	O
the	O
bombing	O
was	O
the	O
work	O
of	O
an	O
extremist	O
Islamic	O
group	O
linked	O
to	O
Al-Qaeda	O
.	O
