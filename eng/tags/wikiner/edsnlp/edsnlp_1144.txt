The	O
AIM-54	O
Phoenix	O
is	O
a	O
radar-guided	O
,	O
long-range	O
air-to-air	O
missile	O
,	O
carried	O
in	O
clusters	O
of	O
up	O
to	O
six	O
missiles	O
--	O
formerly	O
on	O
the	O
U.S.	O
Navy	O
's	O
and	O
currently	O
on	O
the	O
Islamic	O
Republic	O
of	O
Iran	O
Air	O
Force	O
's	O
F-14	O
Tomcat	O
interceptors/multi-role	O
fighters	O
:	O
which	O
is	O
the	O
only	O
aircraft	O
capable	O
of	O
carrying	O
it	O
.	O
The	O
AIM-54	O
was	O
originally	O
developed	O
in	O
the	O
early	O
1960s	O
for	O
the	O
canceled	O
F-111B	O
naval	O
variant	O
,	O
and	O
based	O
on	O
the	O
Eagle	O
project	O
for	O
the	O
canceled	O
F6D	O
Missileer	O
.	O
The	O
Phoenix	O
missile	O
was	O
the	O
United	O
States	O
'	O
only	O
long-range	O
air-to-air	O
missile	O
,	O
and	O
it	O
is	O
the	O
first	O
missile	O
capable	O
of	O
multiple-launch	O
against	O
more	O
than	O
one	O
target	O
.	O
This	O
method	O
meant	O
the	O
aircraft	O
no	O
longer	O
had	O
a	O
search	O
capability	O
while	O
supporting	O
the	O
launched	O
Sparrow	O
,	O
effectively	O
reducing	O
situational	O
awareness	O
.	O
The	O
pilot	O
or	O
Radar	O
Intercept	O
Officer	O
(	O
RIO	O
)	O
could	O
then	O
launch	O
the	O
AIM-54	O
Phoenix	O
missiles	O
when	O
launch	O
parameters	O
were	O
met	O
.	O
Phoenix	O
uses	O
its	O
high	O
altitude	O
to	O
gain	O
gravitational	O
potential	O
energy	O
,	O
which	O
is	O
later	O
converted	O
into	O
kinetic	O
energy	O
as	O
the	O
missile	O
dives	O
at	O
high	O
velocity	O
towards	O
its	O
target	O
during	O
which	O
it	O
activates	O
its	O
active	O
radar	O
to	O
provide	O
terminal	O
guidance	O
.	O
By	O
comparison	O
,	O
the	O
AIM-120	O
AMRAAM	O
radar-guided	O
,	O
medium-range	O
air-to-air	O
missile	O
uses	O
an	O
on-board	O
computer	O
,	O
made	O
possible	O
by	O
digital	O
technology	O
,	O
to	O
compute	O
a	O
collision	O
course	O
to	O
the	O
target	O
.	O
The	O
airframe	O
is	O
a	O
scaled-up	O
version	O
of	O
the	O
USAF	O
AIM-47	O
Falcon	O
with	O
4	O
cruciform	O
fins	O
.	O
A	O
full	O
load	O
of	O
6	O
Phoenix	O
missiles	O
and	O
the	O
unique	O
launch	O
rails	O
weigh	O
in	O
at	O
over	O
8000	O
lb	O
(	O
3600	O
kg	O
)	O
,	O
about	O
twice	O
the	O
weight	O
of	O
Sparrows	O
,	O
so	O
it	O
was	O
more	O
common	O
to	O
carry	O
a	O
mixed	O
load	O
of	O
4	O
Phoenix	O
,	O
2	O
Sparrow	O
and	O
2	O
Sidewinder	O
missiles	O
.	O
The	O
upgraded	O
Phoenix	O
,	O
the	O
AIM-54C	O
,	O
was	O
developed	O
to	O
better	O
counter	O
projected	O
threats	O
from	O
tactical	O
aircraft	O
and	O
cruise	O
missiles	O
,	O
and	O
its	O
final	O
upgrade	O
included	O
a	O
re-programmable	O
memory	O
capability	O
to	O
keep	O
pace	O
with	O
emerging	O
threat	O
ECM	O
.	O
It	O
is	O
thought	O
that	O
the	O
Phoenix	O
was	O
based	O
on	O
the	O
similar	O
AIM-47	O
missile	O
.	O
The	O
U.S.	O
Air	O
Force	O
adopted	O
neither	O
the	O
AIM-47	O
,	O
nor	O
the	O
AIM-54	O
,	O
operationally	O
.	O
The	O
latest	O
model	O
,	O
AIM-120C-7	O
,	O
has	O
a	O
range	O
of	O
30	O
miles	O
(	O
48	O
km	O
)	O
,	O
still	O
significantly	O
less	O
than	O
the	O
retired	O
AIM-54	O
.	O
The	O
associated	O
AWG-9	O
radar	O
system	O
carried	O
by	O
the	O
F-111B	O
and	O
F-14	O
Tomcat	O
was	O
one	O
of	O
largest	O
and	O
most	O
powerful	O
ever	O
fitted	O
to	O
a	O
fighter	O
.	O
The	O
AIM-54	O
Phoenix	O
was	O
retired	O
from	O
USN	O
service	O
on	O
September	O
30	O
,	O
2004	O
.	O
They	O
were	O
replaced	O
by	O
shorter-range	O
AIM-120	O
AMRAAMs	O
,	O
employed	O
on	O
the	O
F/A-18E/F	O
Super	O
Hornet	O
.	O
Both	O
the	O
F-14	O
Tomcat	O
and	O
AIM-54	O
Phoenix	O
missile	O
continue	O
in	O
the	O
service	O
of	O
the	O
Islamic	O
Republic	O
of	O
Iran	O
Air	O
Force	O
,	O
although	O
the	O
operational	O
abilities	O
of	O
these	O
aircraft	O
and	O
the	O
missiles	O
are	O
questionable	O
,	O
since	O
the	O
U.S.	O
refused	O
to	O
supply	O
spare	O
parts	O
and	O
maintenance	O
after	O
the	O
1979	O
revolution	O
;	O
except	O
for	O
a	O
brief	O
period	O
during	O
the	O
Iran-Contra	O
Affair	O
(	O
see	O
F-14	O
Tomcat	O
for	O
more	O
details	O
)	O
.	O
From	O
an	O
engineering	O
and	O
service	O
standpoint	O
,	O
the	O
Phoenix	O
could	O
be	O
said	O
to	O
be	O
a	O
notable	O
success	O
.	O
However	O
,	O
as	O
the	O
only	O
surviving	O
member	O
of	O
the	O
Falcon	O
missile	O
family	O
,	O
it	O
was	O
not	O
adopted	O
by	O
any	O
other	O
nation	O
(	O
besides	O
Iran	O
)	O
,	O
any	O
other	O
U.S.	O
armed	O
service	O
,	O
or	O
even	O
supported	O
by	O
any	O
other	O
aircraft	O
.	O
It	O
was	O
heavy	O
,	O
large	O
,	O
expensive	O
and	O
not	O
practical	O
in	O
close	O
combat	O
compared	O
to	O
the	O
Sparrow	O
or	O
AMRAAM	O
.	O
Reports	O
vary	O
on	O
the	O
use	O
of	O
the	O
285	O
missiles	O
supplied	O
to	O
Iran	O
,	O
during	O
the	O
Iran	O
--	O
Iraq	O
War	O
,	O
from	O
1980-88	O
.	O
It	O
is	O
rumored	O
[	O
citation	O
needed	O
]	O
that	O
U.S.	O
technical	O
personnel	O
sabotaged	O
the	O
aircraft	O
and	O
weapons	O
before	O
they	O
left	O
the	O
country	O
following	O
the	O
1979	O
Iranian	O
Revolution	O
,	O
making	O
it	O
impossible	O
to	O
fire	O
the	O
missile	O
.	O
However	O
,	O
the	O
IRIAF	O
was	O
able	O
to	O
repair	O
the	O
sabotage	O
and	O
the	O
damage	O
only	O
affected	O
a	O
limited	O
number	O
of	O
planes	O
;	O
not	O
the	O
entire	O
fleet	O
.	O
Some	O
western	O
sources	O
claim	O
that	O
it	O
is	O
unlikely	O
that	O
the	O
Phoenix	O
was	O
used	O
operationally	O
.	O
Most	O
informed	O
sources	O
claim	O
that	O
the	O
primary	O
use	O
of	O
the	O
F-14	O
was	O
as	O
an	O
airborne	O
early	O
warning	O
aircraft	O
,	O
guarded	O
by	O
other	O
fighters	O
.	O
Also	O
,	O
some	O
F-14s	O
were	O
modified	O
into	O
specialized	O
airborne	O
early	O
warning	O
aircraft	O
.	O
At	O
worst	O
,	O
during	O
late	O
1987	O
,	O
the	O
stock	O
of	O
AIM-54	O
missiles	O
was	O
at	O
its	O
lowest	O
,	O
with	O
less	O
than	O
50	O
operational	O
missiles	O
available	O
.	O
Iran	O
managed	O
finally	O
,	O
to	O
find	O
a	O
clandestine	O
buyer	O
that	O
supplied	O
it	O
with	O
batteries	O
--	O
though	O
those	O
did	O
cost	O
up	O
to	O
$	O
10,000	O
USD	O
each	O
.	O
