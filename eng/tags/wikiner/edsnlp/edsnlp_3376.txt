After	O
his	O
parents	O
'	O
divorce	O
in	O
1953	O
when	O
he	O
was	O
twenty-four	O
years	O
old	O
,	O
Donaldson	PERSON
's	O
mother	O
was	O
diagnosed	O
with	O
the	O
mental	O
disorder	O
porphyria	O
and	O
abandoned	O
her	O
two	O
sons	O
.	O
At	O
age	O
twelve	O
,	O
Donaldson	O
was	O
expelled	O
from	O
the	O
Boy	O
Scouts	O
for	O
consensual	O
sexual	O
behavior	O
with	O
other	O
boys	O
(	O
who	O
,	O
as	O
recipients	O
,	O
were	O
not	O
punished	O
)	O
.	O
He	O
also	O
became	O
active	O
in	O
politics	O
as	O
a	O
libertarian	O
conservative	O
,	O
supporting	O
Barry	PERSON
Goldwater	PERSON
for	O
president	O
"	O
and	O
"	O
considered	O
joining	O
the	O
Young	O
Americans	O
for	O
Freedom	O
but	O
was	O
so	O
uptight	O
that	O
he	O
first	O
checked	O
with	O
J.	PERSON
Edgar	PERSON
Hoover	PERSON
by	O
letter	O
to	O
inquire	O
whether	O
the	O
YAF	O
was	O
'	O
a	O
communist	O
organization	O
,	O
communist	O
subverted	O
,	O
or	O
in	O
danger	O
of	O
becoming	O
either	O
'	O
"	O
.	O
Hoover	PERSON
sent	O
back	O
a	O
reply	O
"	O
praising	O
his	O
concern	O
about	O
communism	O
and	O
then	O
opened	O
an	O
FBI	O
file	O
on	O
the	O
boy	O
"	O
.	O
Donaldson	O
ran	O
away	O
to	O
N.Y.	O
,	O
where	O
,	O
he	O
later	O
wrote	O
,	O
"	O
he	O
gays	O
of	O
N.Y.	O
welcomed	O
me	O
enthusiastically	O
,	O
offered	O
hospitality	O
,	O
and	O
'	O
brought	O
me	O
out	O
'	O
as	O
a	O
'	O
butch	O
'	O
homosexual	O
(	O
in	O
contrast	O
to	O
the	O
"	O
queens	O
"	O
)	O
.	O
In	O
August	O
1965	O
,	O
Donaldson	O
"	O
had	O
a	O
social	O
worker	O
call	O
the	O
dean	O
's	O
office	O
to	O
ask	O
whether	O
Columbia	O
would	O
register	O
a	O
known	O
homosexual	O
.	O
"	O
He	O
entered	O
Columbia	O
University	O
that	O
fall	O
and	O
began	O
using	O
the	O
pseudonym	O
Stephen	O
Donaldson	O
so	O
he	O
could	O
be	O
open	O
about	O
his	O
sexuality	O
without	O
embarrassing	O
his	O
father	O
.	O
They	O
both	O
were	O
named	O
Robert	PERSON
Martin	PERSON
,	O
and	O
his	O
father	O
taught	O
mathematics	O
at	O
Rider	ORGANIZATION
College	ORGANIZATION
in	O
New	O
Jersey	O
.	O
His	O
first	O
year	O
of	O
college	O
was	O
difficult	O
:	O
he	O
met	O
no	O
other	O
gay	O
students	O
or	O
faculty	O
and	O
had	O
to	O
move	O
from	O
a	O
shared	O
suite	O
to	O
a	O
single	O
room	O
when	O
his	O
suitemates	O
"	O
told	O
the	O
college	O
dean	O
David	O
Truman	O
that	O
they	O
felt	O
uncomfortable	O
living	O
with	O
a	O
homosexual	O
.	O
"	O
Apparently	O
ambivalent	O
,	O
they	O
offered	O
Donaldson	O
"	O
great	O
apologies	O
and	O
said	O
they	O
realized	O
they	O
should	O
n't	O
feel	O
"	O
unwilling	O
to	O
live	O
with	O
him	O
.	O
In	O
the	O
summer	O
of	O
1966	O
,	O
Donaldson	O
began	O
a	O
relationship	O
with	O
gay	O
activist	O
Frank	PERSON
Kameny	PERSON
,	O
who	O
had	O
a	O
great	O
influence	O
on	O
him	O
.	O
Donaldson	O
later	O
wrote	O
:	O
This	O
prevented	O
the	O
group	O
from	O
receiving	O
university	O
funding	O
or	O
holding	O
public	O
events	O
on	O
campus	O
until	O
Donaldson	O
realized	O
that	O
by	O
"	O
recruiting	O
the	O
most	O
prominent	O
student	O
leaders	O
to	O
become	O
pro	O
forma	O
members	O
,	O
he	O
could	O
satisfy	O
the	O
administration	O
without	O
compromising	O
the	O
anonymity	O
of	O
gay	O
students	O
,	O
and	O
Columbia	O
officially	O
chartered	O
the	O
country	O
's	O
first	O
student	O
gay	O
rights	O
group	O
on	O
April	O
19	O
,	O
1967	O
.	O
"	O
The	O
Spectator	O
ran	O
an	O
editorial	O
praising	O
the	O
chartering	O
of	O
the	O
group	O
and	O
printed	O
letters	O
from	O
students	O
attacking	O
and	O
defending	O
the	O
decision	O
.	O
At	O
this	O
point	O
,	O
there	O
was	O
no	O
apparent	O
opposition	O
from	O
Columbia	O
faculty	O
or	O
staff	O
.	O
The	O
article	O
noted	O
that	O
"	O
[	O
f	O
]	O
unds	O
were	O
said	O
to	O
have	O
been	O
supplied	O
for	O
the	O
organization	O
by	O
some	O
Columbia	O
alumni	O
who	O
were	O
reported	O
to	O
have	O
learned	O
about	O
it	O
from	O
advertisements	O
in	O
magazines	O
for	O
homosexuals	O
"	O
and	O
that	O
Donaldson	O
said	O
that	O
the	O
group	O
"	O
maintains	O
liaison	O
"	O
with	O
,	O
but	O
is	O
not	O
controlled	O
by	O
,	O
outside	O
homosexual	O
groups	O
.	O
The	O
publicity	O
also	O
led	O
students	O
at	O
other	O
universities	O
to	O
contact	O
Donaldson	O
about	O
starting	O
chapters	O
.	O
The	O
University	O
of	O
Massachusetts	O
Amherst	O
gained	O
a	O
chapter	O
in	O
1970	O
.	O
Donaldson	O
began	O
his	O
writing	O
career	O
in	O
college	O
by	O
working	O
summers	O
"	O
as	O
a	O
reporter	O
for	O
the	O
Associated	O
Press	O
and	O
Virginian	O
Pilot	O
"	O
and	O
writing	O
"	O
a	O
regular	O
column	O
for	O
the	O
New	O
York	O
newsmagazine	O
Gay	O
Power	O
and	O
occasional	O
reports	O
for	O
the	O
Los	O
Angeles	O
Advocate	O
"	O
.	O
Frank	PERSON
Kameny	PERSON
arranged	O
his	O
first	O
internship	O
,	O
which	O
was	O
in	O
the	O
summer	O
of	O
1966	O
.	O
In	O
New	LOCATION
York	LOCATION
,	O
Donaldson	O
funded	O
"	O
his	O
education	O
by	O
working	O
as	O
a	O
hustler	O
,	O
first	O
at	O
the	O
infamous	LOCATION
intersection	LOCATION
of	LOCATION
Fifty-third	LOCATION
Street	LOCATION
and	LOCATION
Third	LOCATION
Avenue	LOCATION
,	O
then	O
as	O
a	O
call	O
boy	O
through	O
a	O
house	O
.	O
He	O
claimed	O
to	O
have	O
serviced	O
several	O
famous	O
clients	O
,	O
including	O
Rock	PERSON
Hudson	PERSON
and	O
Roy	PERSON
Cohn	PERSON
.	O
"	O
While	O
at	O
Columbia	O
,	O
Donaldson	PERSON
"	O
experimented	O
with	O
cannabis	O
and	O
LSD	O
"	O
and	O
described	O
himself	O
as	O
"	O
ordained	O
in	O
the	O
psychedelic	O
church,	O
"	O
going	O
on	O
to	O
guide	O
first-time	O
LSD	O
users	O
.	O
He	O
was	O
arrested	O
twice	O
for	O
participating	O
in	O
anti-war	O
protests	O
at	O
Columbia	O
,	O
including	O
a	O
"	O
liberation	O
"	O
of	O
Columbia	O
president	O
Grayson	O
Kirk	O
's	O
office	O
,	O
spending	O
an	O
uneventful	O
night	O
in	O
jail	O
in	O
1968	O
.	O
His	O
"	O
growing	O
feeling	O
of	O
discomfort	O
with	O
biphobia	O
in	O
the	O
homophile/gay	O
liberation	O
movement	O
was	O
a	O
major	O
factor	O
"	O
in	O
his	O
deciding	O
to	O
quit	O
the	O
movement	O
and	O
enlist	O
in	O
the	O
Navy	O
after	O
graduating	O
from	O
Columbia	O
in	O
1970	O
.	O
As	O
Randy	PERSON
Shilts	PERSON
wrote	O
in	O
Conduct	O
Unbecoming	O
:	O
Gays	O
and	O
Lesbians	O
in	O
the	O
US	O
Military	O
:	O
Donaldson	O
continued	O
to	O
fight	O
,	O
and	O
,	O
in	O
1977	O
,	O
his	O
discharge	O
was	O
upgraded	O
to	O
"	O
honorable	O
"	O
as	O
part	O
of	O
"	O
President	O
Carter	O
's	O
sweeping	O
amnesty	O
program	O
for	O
Vietnam-era	O
draft	O
evaders	O
,	O
deserters	O
,	O
and	O
service	O
members	O
"	O
,	O
at	O
which	O
time	O
:	O
According	O
to	O
Eisenbach	O
:	O
Donaldson	O
later	O
summarized	O
his	O
military	O
experience	O
and	O
the	O
subsequent	O
transition	O
in	O
his	O
life	O
:	O
Donaldson	O
was	O
involved	O
in	O
the	O
N.Y.	O
bisexual	O
movement	O
in	O
the	O
mid-1970s	O
,	O
for	O
example	O
appearing	O
in	O
1974	O
on	O
a	O
New	O
York	O
Gay	O
Activists	O
Alliance	O
panel	O
with	O
Kate	PERSON
Millet	PERSON
.	O
Donaldson	O
propounded	O
the	O
belief	O
that	O
The	O
protesters	O
,	O
referred	O
to	O
as	O
the	O
"	O
White	O
House	O
7	O
"	O
,	O
were	O
arrested	O
for	O
unlawful	O
entry	O
and	O
were	O
released	O
on	O
bail	O
,	O
except	O
for	O
Donaldson	PERSON
,	O
who	O
refused	O
and	O
spent	O
the	O
night	O
in	O
the	O
DC	O
jail	O
before	O
being	O
released	O
by	O
a	O
judge	O
the	O
next	O
morning	O
.	O
Donaldson	O
again	O
refused	O
to	O
post	O
bail	O
.	O
Liddy	PERSON
wrote	O
in	O
his	O
autobiography	O
that	O
he	O
heard	O
that	O
Donaldson	O
worked	O
for	O
The	O
Washington	O
Post	O
and	O
,	O
suspecting	O
him	O
of	O
being	O
in	O
prison	O
"	O
to	O
try	O
to	O
steal	O
a	O
march	O
on	O
Woodward	O
and	O
Bernstein	O
by	O
getting	O
a	O
firsthand	O
story,	O
"	O
expressed	O
the	O
wish	O
that	O
he	O
be	O
transferred	O
elsewhere	O
.	O
That	O
night	O
,	O
Donaldson	O
was	O
lured	O
in	O
a	O
cell	O
by	O
a	O
prisoner	O
who	O
claimed	O
that	O
he	O
and	O
his	O
friends	O
wanted	O
to	O
discuss	O
pacifism	O
with	O
him	O
.	O
The	O
next	O
day	O
,	O
August	O
24	O
,	O
Donaldson	PERSON
held	O
a	O
press	O
conference	O
,	O
becoming	O
the	O
first	O
male	O
prison	O
rape	O
survivor	O
to	O
publicly	O
recount	O
his	O
experiences	O
,	O
resulting	O
in	O
"	O
massive	O
and	O
prolonged	O
"	O
publicity	O
(	O
under	O
his	O
legal	O
name	O
,	O
Robert	PERSON
Martin	PERSON
)	O
.	O
Donaldson	O
wrote	O
the	O
following	O
year	O
about	O
this	O
"	O
time	O
of	O
agony	O
"	O
:	O
Donaldson	O
At	O
first	O
Donaldson	O
chose	O
to	O
go	O
to	O
jail	O
rather	O
than	O
pay	O
the	O
fine	O
,	O
which	O
he	O
viewed	O
as	O
cooperating	O
with	O
the	O
government	O
.	O
He	O
changed	O
his	O
mind	O
after	O
finding	O
out	O
he	O
would	O
be	O
returned	O
to	O
the	O
same	O
jail	O
in	O
Washington	LOCATION
,	O
DC	O
.	O
As	O
for	O
the	O
August	O
14	O
charge	O
that	O
led	O
to	O
his	O
traumatic	O
imprisonment	O
,	O
Donaldson	O
refused	O
to	O
plead	O
(	O
unlike	O
most	O
of	O
those	O
arrested	O
,	O
who	O
pleaded	O
no	O
contest	O
)	O
and	O
went	O
to	O
trial	O
alone	O
on	O
September	O
28	O
.	O
Donaldson	O
later	O
said	O
:	O
"	O
The	O
government	O
sewed	O
up	O
the	O
tears	O
in	O
my	O
rectum	O
which	O
the	O
government	O
occasioned	O
.	O
"	O
In	O
1982	O
,	O
Donaldson	PERSON
wrote	O
about	O
his	O
lack	O
of	O
success	O
in	O
getting	O
needed	O
psychological	O
counseling	O
after	O
the	O
rapes	O
:	O
Donaldson	O
wrote	O
that	O
he	O
was	O
aided	O
in	O
his	O
sexual	O
recovery	O
by	O
an	O
understanding	O
woman	O
who	O
helped	O
him	O
regain	O
his	O
confidence	O
.	O
While	O
traveling	O
in	O
late	O
1976	O
,	O
Donaldson	PERSON
was	O
arrested	O
after	O
urinating	O
in	O
a	O
motel	O
parking	O
lot	O
,	O
then	O
was	O
charged	O
with	O
possession	O
after	O
the	O
police	O
searched	O
his	O
hotel	O
room	O
and	O
found	O
cannabis	O
.	O
He	O
was	O
placed	O
in	O
a	O
small	O
cell	O
block	O
with	O
four	O
white	O
and	O
eight	O
black	O
prisoners	O
,	O
most	O
Marines	O
from	O
a	O
nearby	O
base	O
,	O
who	O
demanded	O
sexual	O
services	O
.	O
Donaldson	O
later	O
wrote	O
:	O
Donaldson	O
was	O
pleasantly	O
surprised	O
that	O
they	O
treated	O
him	O
not	O
with	O
the	O
contempt	O
he	O
expected	O
but	O
with	O
genuine	O
warmth	O
and	O
affection	O
.	O
Donaldson	O
was	O
defended	O
against	O
the	O
possession	O
charge	O
by	O
the	O
chief	O
counsel	O
of	O
the	O
state	O
chapter	O
of	O
the	O
American	O
Civil	O
Liberties	O
Union	O
.	O
Donaldson	O
was	O
placed	O
in	O
the	O
city	O
jail	O
,	O
where	O
he	O
was	O
gang-raped	O
nightly	O
until	O
the	O
guards	O
were	O
alerted	O
and	O
he	O
was	O
put	O
in	O
solitary	O
confinement	O
for	O
his	O
protection	O
(	O
to	O
which	O
Donaldson	O
vigorously	O
objected	O
,	O
believing	O
the	O
loss	O
of	O
rights	O
and	O
privileges	O
unfair	O
)	O
.	O
After	O
being	O
released	O
into	O
a	O
white	O
cell	O
,	O
he	O
was	O
greeted	O
:	O
"	O
Why	O
,	O
it	O
's	O
Donny	O
the	O
Punk	O
!	O
Donaldson	O
experienced	O
another	O
mental	O
shift	O
:	O
Donaldson	O
wrote	O
:	O
After	O
several	O
months	O
,	O
Donaldson	O
's	O
case	O
was	O
dropped	O
by	O
the	O
prosecution	O
after	O
the	O
arresting	O
officer	O
's	O
suicide	O
.	O
Donaldson	O
wrote	O
:	O
In	O
1980	O
,	O
Donaldson	O
"	O
hit	O
rock	O
bottom	O
"	O
and	O
committed	O
:	O
In	O
a	O
1982	O
essay	O
written	O
from	O
jail	O
,	O
Donaldson	O
described	O
the	O
event	O
:	O
Less	O
than	O
a	O
year	O
into	O
his	O
term	O
,	O
Donaldson	O
had	O
been	O
"	O
raped	O
once	O
,	O
assaulted	O
once	O
,	O
and	O
claimed	O
by	O
five	O
different	O
men	O
"	O
in	O
jail	O
and	O
was	O
fearing	O
his	O
upcoming	O
transfer	O
to	O
his	O
first	O
maximum	O
security	O
prison	O
,	O
where	O
he	O
went	O
on	O
to	O
spend	O
over	O
a	O
year	O
in	O
protective	O
custody	O
,	O
which	O
he	O
described	O
as	O
"	O
a	O
solitary	O
retreat	O
"	O
in	O
a	O
letter	O
to	O
Bo	PERSON
Lozoff	PERSON
.	O
In	O
their	O
correspondence	O
,	O
Donaldson	O
expressed	O
his	O
desire	O
to	O
help	O
other	O
survivors	O
but	O
lamented	O
that	O
:	O
Donaldson	O
was	O
released	O
on	O
parole	O
in	O
April	O
1984	O
and	O
returned	O
to	O
New	LOCATION
York	LOCATION
City	LOCATION
.	O
He	O
violated	O
his	O
parole	O
by	O
visiting	O
India	O
for	O
religious	O
study	O
in	O
1987-1988	O
,	O
which	O
led	O
to	O
his	O
final	O
prison	O
term	O
,	O
in	O
federal	O
prison	O
,	O
in	O
1990	O
.	O
The	O
organization	O
(	O
since	O
2008	O
known	O
as	O
Just	O
Detention	O
International	O
)	O
helps	O
prisoners	O
deal	O
with	O
the	O
psychological	O
and	O
physical	O
trauma	O
of	O
rape	O
,	O
and	O
works	O
to	O
prevent	O
rape	O
from	O
happening	O
.	O
Donaldson	O
was	O
perhaps	O
the	O
first	O
activist	O
against	O
male	O
rape	O
in	O
the	O
U.S.	O
to	O
gain	O
significant	O
media	O
attention	O
.	O
As	O
"	O
Donny	O
the	O
Punk	O
"	O
,	O
Donaldson	PERSON
was	O
already	O
a	O
respected	O
writer	O
and	O
personality	O
in	O
the	O
punk	O
and	O
anti-racist	O
skinhead	O
subcultures	O
.	O
He	O
had	O
published	O
in	O
punk	O
zines	O
such	O
as	O
Maximum	O
RocknRoll	O
,	O
Flipside	O
and	O
J.D.s	O
.	O
This	O
co-operative	O
group	O
met	O
on	O
Sundays	O
before	O
the	O
weekly	O
CBGB	O
Sunday	O
hardcore	O
matinees	O
and	O
organized	O
several	O
benefit	O
concerts	O
.	O
After	O
Donaldson	O
's	O
death	O
,	O
SPR	O
continued	O
to	O
work	O
for	O
prisoners	O
'	O
rights	O
.	O
It	O
contributed	O
to	O
gaining	O
the	O
passage	O
of	O
the	O
first	O
US	O
law	O
against	O
rape	O
in	O
prison	O
Prison	O
Rape	O
Elimination	O
Act	O
of	O
2003	O
.	O
