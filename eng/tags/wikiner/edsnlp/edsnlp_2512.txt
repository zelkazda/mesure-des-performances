Celestines	O
are	O
a	O
Roman	O
Catholic	O
monastic	O
order	O
,	O
a	O
branch	O
of	O
the	O
Benedictines	O
,	O
founded	O
in	O
1244	O
.	O
In	O
1264	O
the	O
new	O
institution	O
was	O
approved	O
by	O
Urban	O
IV	O
.	O
He	O
found	O
time	O
also	O
to	O
visit	O
personally	O
the	O
great	O
Benedictine	O
monastery	O
on	O
Monte	O
Cassino	O
,	O
where	O
he	O
succeeded	O
in	O
persuading	O
the	O
monks	O
to	O
accept	O
his	O
more	O
rigorous	O
rule	O
.	O
After	O
the	O
death	O
of	O
the	O
founder	O
the	O
order	O
was	O
favoured	O
and	O
privileged	O
by	O
Benedict	PERSON
XI	PERSON
,	O
and	O
rapidly	O
spread	O
through	O
Italy	O
,	O
Germany	O
,	O
Flanders	O
,	O
and	O
France	O
,	O
where	O
they	O
were	O
received	O
by	O
Philip	PERSON
the	O
Fair	O
in	O
1300	O
.	O
Paul	PERSON
V	PERSON
was	O
a	O
notable	O
benefactor	O
of	O
the	O
order	O
.	O
