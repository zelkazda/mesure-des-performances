Hipparchus	PERSON
was	O
born	O
in	O
Nicaea	LOCATION
(	LOCATION
now	LOCATION
Iznik	LOCATION
,	LOCATION
Turkey	LOCATION
)	O
,	O
and	O
probably	O
died	O
on	O
the	O
island	O
of	O
Rhodes	LOCATION
.	O
Hipparchus	PERSON
is	O
considered	O
the	O
greatest	O
ancient	O
astronomical	O
observer	O
and	O
,	O
by	O
some	O
,	O
the	O
greatest	O
overall	O
astronomer	O
of	O
antiquity	O
.	O
For	O
this	O
he	O
certainly	O
made	O
use	O
of	O
the	O
observations	O
and	O
perhaps	O
the	O
mathematical	O
techniques	O
accumulated	O
over	O
centuries	O
by	O
the	O
Chaldeans	O
from	O
Babylonia	O
.	O
It	O
would	O
be	O
three	O
centuries	O
before	O
Claudius	O
Ptolemaeus	O
'	O
synthesis	O
of	O
astronomy	O
would	O
supersede	O
the	O
work	O
of	O
Hipparchus	O
;	O
it	O
is	O
heavily	O
dependent	O
on	O
it	O
in	O
many	O
areas	O
.	O
Relatively	O
little	O
of	O
Hipparchus	PERSON
'	O
direct	O
work	O
survives	O
into	O
modern	O
times	O
.	O
Although	O
he	O
wrote	O
at	O
least	O
fourteen	O
books	O
,	O
only	O
his	O
commentary	O
on	O
the	O
popular	O
astronomical	O
poem	O
by	O
Aratus	O
was	O
preserved	O
by	O
later	O
copyists	O
.	O
There	O
is	O
a	O
strong	O
tradition	O
that	O
Hipparchus	PERSON
was	O
born	O
in	O
Nicaea	LOCATION
,	O
in	O
the	O
ancient	O
district	O
of	O
Bithynia	LOCATION
(	LOCATION
modern-day	LOCATION
Iznik	LOCATION
in	LOCATION
province	LOCATION
Bursa	LOCATION
)	O
,	O
in	O
what	O
today	O
is	O
the	O
country	O
Turkey	O
.	O
The	O
exact	O
dates	O
of	O
his	O
life	O
are	O
not	O
known	O
,	O
but	O
Ptolemy	O
attributes	O
to	O
him	O
astronomical	O
observations	O
in	O
the	O
period	O
from	O
147	O
BC	O
to	O
127	O
BC	O
,	O
and	O
some	O
of	O
these	O
are	O
stated	O
as	O
made	O
in	O
Rhodes	O
;	O
earlier	O
observations	O
since	O
162	O
BC	O
might	O
also	O
have	O
been	O
made	O
by	O
him	O
.	O
His	O
birth	O
date	O
(	O
ca.	O
190	O
BC	O
)	O
was	O
calculated	O
by	O
Delambre	O
based	O
on	O
clues	O
in	O
his	O
work	O
.	O
Hipparchus	O
must	O
have	O
lived	O
some	O
time	O
after	O
127	O
BC	O
because	O
he	O
analyzed	O
and	O
published	O
his	O
observations	O
from	O
that	O
year	O
.	O
Hipparchus	O
obtained	O
information	O
from	O
Alexandria	O
as	O
well	O
as	O
Babylon	O
,	O
but	O
it	O
is	O
not	O
known	O
when	O
or	O
if	O
he	O
visited	O
these	O
places	O
.	O
He	O
is	O
believed	O
to	O
have	O
died	O
on	O
the	O
island	O
of	O
Rhodes	LOCATION
,	O
where	O
he	O
seems	O
to	O
have	O
spent	O
most	O
of	O
his	O
later	O
life	O
.	O
It	O
is	O
not	O
known	O
what	O
Hipparchus	O
'	O
economic	O
means	O
were	O
nor	O
how	O
he	O
supported	O
his	O
scientific	O
activities	O
.	O
In	O
the	O
2nd	O
and	O
3rd	O
centuries	O
coins	O
were	O
made	O
in	O
his	O
honour	O
in	O
Bithynia	LOCATION
that	O
bear	O
his	O
name	O
and	O
show	O
him	O
with	O
a	O
globe	O
;	O
this	O
supports	O
the	O
tradition	O
that	O
he	O
was	O
born	O
there	O
.	O
Hipparchus	O
is	O
thought	O
to	O
be	O
the	O
first	O
to	O
calculate	O
a	O
heliocentric	O
system	O
,	O
but	O
he	O
abandoned	O
his	O
work	O
because	O
the	O
calculations	O
showed	O
the	O
orbits	O
were	O
not	O
perfectly	O
circular	O
as	O
believed	O
to	O
be	O
mandatory	O
by	O
by	O
the	O
theology	O
of	O
the	O
time	O
.	O
This	O
is	O
a	O
highly	O
critical	O
commentary	O
in	O
the	O
form	O
of	O
two	O
books	O
on	O
a	O
popular	O
poem	O
by	O
Aratus	O
based	O
on	O
the	O
work	O
by	O
Eudoxus	O
.	O
Hipparchus	O
also	O
made	O
a	O
list	O
of	O
his	O
major	O
works	O
,	O
which	O
apparently	O
mentioned	O
about	O
fourteen	O
books	O
,	O
but	O
which	O
is	O
only	O
known	O
from	O
references	O
by	O
later	O
authors	O
.	O
His	O
famous	O
star	O
catalog	O
was	O
incorporated	O
into	O
the	O
one	O
by	O
Ptolemy	PERSON
,	O
and	O
may	O
be	O
almost	O
perfectly	O
reconstructed	O
by	O
subtraction	O
of	O
two	O
and	O
two	O
thirds	O
degrees	O
from	O
the	O
longitudes	O
of	O
Ptolemy	O
's	O
stars	O
.	O
Hipparchus	O
seems	O
to	O
have	O
been	O
the	O
first	O
to	O
exploit	O
Babylonian	O
astronomical	O
knowledge	O
and	O
techniques	O
systematically	O
.	O
He	O
also	O
used	O
the	O
Babylonian	O
unit	O
pechus	O
(	O
"	O
cubit	O
"	O
)	O
of	O
about	O
2°	O
or	O
2.5°	O
.	O
Hipparchus	O
'	O
use	O
of	O
Babylonian	O
sources	O
has	O
always	O
been	O
known	O
in	O
a	O
general	O
way	O
,	O
because	O
of	O
Ptolemy	O
's	O
statements	O
.	O
For	O
his	O
chord	O
table	O
Hipparchus	O
must	O
have	O
used	O
a	O
better	O
approximation	O
for	O
π	O
than	O
the	O
one	O
from	O
Archimedes	O
of	O
between	O
3	O
+	O
1/7	O
and	O
3	O
+	O
10/71	O
;	O
perhaps	O
he	O
had	O
the	O
one	O
later	O
used	O
by	O
Ptolemy	O
:	O
3;8:30	O
(	O
sexagesimal	O
)	O
;	O
but	O
it	O
is	O
not	O
known	O
if	O
he	O
computed	O
an	O
improved	O
value	O
himself	O
.	O
Hipparchus	O
was	O
the	O
first	O
to	O
show	O
that	O
the	O
stereographic	O
projection	O
is	O
conformal	O
,	O
and	O
that	O
it	O
transforms	O
circles	O
on	O
the	O
sphere	O
that	O
do	O
not	O
pass	O
through	O
the	O
center	O
of	O
projection	O
to	O
circles	O
on	O
the	O
plane	O
.	O
Besides	O
geometry	O
,	O
Hipparchus	O
also	O
used	O
arithmetic	O
techniques	O
developed	O
by	O
the	O
Chaldeans	O
.	O
(	O
Previous	O
to	O
the	O
finding	O
of	O
the	O
proofs	O
of	O
Menelaus	O
a	O
century	O
ago	O
,	O
Ptolemy	PERSON
was	O
credited	O
with	O
the	O
invention	O
of	O
spherical	O
trigonometry	O
.	O
)	O
Ptolemy	O
later	O
used	O
spherical	O
trigonometry	O
to	O
compute	O
things	O
like	O
the	O
rising	O
and	O
setting	O
points	O
of	O
the	O
ecliptic	O
,	O
or	O
to	O
take	O
account	O
of	O
the	O
lunar	O
parallax	O
.	O
Further	O
confirming	O
his	O
contention	O
is	O
the	O
finding	O
that	O
the	O
big	O
errors	O
in	O
Hipparchus	O
's	O
longitude	O
of	O
Regulus	O
and	O
both	O
longitudes	O
of	O
Spica	O
agree	O
to	O
a	O
few	O
minutes	O
in	O
all	O
three	O
instances	O
with	O
a	O
theory	O
that	O
he	O
took	O
the	O
wrong	O
sign	O
for	O
his	O
correction	O
for	O
parallax	O
when	O
using	O
eclipses	O
for	O
determining	O
stars	O
'	O
positions	O
;	O
computation	O
of	O
parallax	O
tables	O
is	O
a	O
problem	O
of	O
spherical	O
trigonometry	O
.	O
These	O
are	O
among	O
a	O
variety	O
of	O
indications	O
that	O
spherical	O
trigonometry	O
was	O
in	O
use	O
centuries	O
before	O
Menelaus	O
.	O
Already	O
al-Biruni	O
and	O
Copernicus	O
noted	O
that	O
the	O
period	O
of	O
4,267	O
lunations	O
is	O
actually	O
about	O
5	O
minutes	O
longer	O
than	O
the	O
value	O
for	O
the	O
eclipse	O
period	O
that	O
Ptolemy	O
attributes	O
to	O
Hipparchus	O
.	O
However	O
,	O
the	O
timing	O
methods	O
of	O
the	O
Babylonians	O
had	O
an	O
error	O
of	O
no	O
less	O
than	O
8	O
minutes	O
.	O
Modern	O
scholars	O
agree	O
that	O
Hipparchus	O
rounded	O
the	O
eclipse	O
period	O
to	O
the	O
nearest	O
hour	O
,	O
and	O
used	O
it	O
to	O
confirm	O
the	O
validity	O
of	O
the	O
traditional	O
values	O
,	O
rather	O
than	O
try	O
to	O
derive	O
an	O
improved	O
value	O
from	O
his	O
own	O
observations	O
.	O
Hipparchus	O
was	O
the	O
first	O
astronomer	O
we	O
know	O
attempted	O
to	O
determine	O
the	O
relative	O
proportions	O
and	O
actual	O
sizes	O
of	O
these	O
orbits	O
.	O
Hipparchus	O
used	O
two	O
sets	O
of	O
three	O
lunar	O
eclipse	O
observations	O
,	O
which	O
he	O
carefully	O
selected	O
to	O
satisfy	O
the	O
requirements	O
.	O
The	O
eccentric	O
model	O
he	O
fitted	O
to	O
these	O
eclipses	O
from	O
his	O
Babylonian	O
eclipse	O
list	O
:	O
22/23	O
December	O
383	O
BC	O
,	O
18/19	O
June	O
382	O
BC	O
,	O
and	O
12/13	O
December	O
382	O
BC	O
.	O
The	O
epicycle	O
model	O
he	O
fitted	O
to	O
lunar	O
eclipse	O
observations	O
made	O
in	O
Alexandria	LOCATION
at	O
22	O
September	O
201	O
BC	O
,	O
19	O
March	O
200	O
BC	O
,	O
and	O
11	O
September	O
200	O
BC	O
.	O
The	O
somewhat	O
weird	O
numbers	O
are	O
due	O
to	O
the	O
cumbersome	O
unit	O
he	O
used	O
in	O
his	O
chord	O
table	O
according	O
to	O
one	O
group	O
of	O
historians	O
,	O
who	O
explain	O
their	O
reconstruction	O
's	O
inability	O
to	O
agree	O
with	O
these	O
four	O
numbers	O
as	O
partly	O
due	O
to	O
some	O
sloppy	O
rounding	O
and	O
calculation	O
errors	O
by	O
Hipparchus	O
,	O
for	O
which	O
Ptolemy	O
criticised	O
him	O
(	O
he	O
himself	O
made	O
rounding	O
errors	O
too	O
)	O
.	O
Ptolemy	O
established	O
a	O
ratio	O
of	O
60	O
:	O
5+1/4.	O
.	O
Before	O
Hipparchus	O
,	O
Meton	O
,	O
Euctemon	O
,	O
and	O
their	O
pupils	O
at	O
Athens	LOCATION
had	O
made	O
a	O
solstice	O
observation	O
(	O
i.e.	O
,	O
timed	O
the	O
moment	O
of	O
the	O
summer	O
solstice	O
)	O
on	O
June	O
27	O
,	O
432	O
BC	O
.	O
Hipparchus	O
himself	O
observed	O
the	O
summer	O
solstice	O
in	O
135	O
BC	O
,	O
but	O
he	O
found	O
observations	O
of	O
the	O
moment	O
of	O
equinox	O
more	O
accurate	O
,	O
and	O
he	O
made	O
many	O
during	O
his	O
lifetime	O
.	O
Ptolemy	O
quotes	O
an	O
equinox	O
timing	O
by	O
Hipparchus	O
(	O
at	O
24	O
March	O
146	O
BC	O
at	O
dawn	O
)	O
that	O
differs	O
by	O
5h	O
from	O
the	O
observation	O
made	O
on	O
Alexandria	O
's	O
large	O
public	O
equatorial	O
ring	O
that	O
same	O
day	O
(	O
at	O
1h	O
before	O
noon	O
)	O
:	O
Hipparchus	O
may	O
have	O
visited	O
Alexandria	O
but	O
he	O
did	O
not	O
make	O
his	O
equinox	O
observations	O
there	O
;	O
presumably	O
he	O
was	O
on	O
Rhodes	LOCATION
(	O
at	O
nearly	O
the	O
same	O
geographical	O
longitude	O
)	O
.	O
He	O
could	O
have	O
used	O
the	O
equatorial	O
ring	O
of	O
his	O
armillary	O
sphere	O
or	O
another	O
equatorial	O
ring	O
for	O
these	O
observations	O
,	O
but	O
Hipparchus	O
(	O
and	O
Ptolemy	O
)	O
knew	O
that	O
observations	O
with	O
these	O
instruments	O
are	O
sensitive	O
to	O
a	O
precise	O
alignment	O
with	O
the	O
equator	O
,	O
so	O
if	O
he	O
were	O
restricted	O
to	O
an	O
armillary	O
,	O
it	O
would	O
make	O
more	O
sense	O
to	O
use	O
its	O
meridian	O
ring	O
as	O
a	O
transit	O
instrument	O
.	O
Ptolemy	O
and	O
Hipparchus	O
apparently	O
did	O
not	O
realize	O
that	O
refraction	O
is	O
the	O
cause	O
.	O
)	O
And	O
Ptolemy	O
claims	O
his	O
solar	O
observations	O
were	O
on	O
a	O
transit	O
instrument	O
set	O
in	O
the	O
meridian	O
.	O
The	O
established	O
value	O
for	O
the	O
tropical	O
year	O
,	O
introduced	O
by	O
Callippus	O
in	O
or	O
before	O
330	O
BC	O
was	O
365	O
+	O
1/4	O
days	O
.	O
(	O
Possibly	O
from	O
Babylonian	O
sources	O
,	O
see	O
above	O
[	O
?	O
?	O
?	O
]	O
.	O
Hipparchus	O
'	O
equinox	O
observations	O
gave	O
varying	O
results	O
,	O
but	O
he	O
himself	O
points	O
out	O
)	O
that	O
the	O
observation	O
errors	O
by	O
himself	O
and	O
his	O
predecessors	O
may	O
have	O
been	O
as	O
large	O
as	O
1/4	O
day	O
.	O
Between	O
the	O
solstice	O
observation	O
of	O
Meton	PERSON
and	O
his	O
own	O
,	O
there	O
were	O
297	O
years	O
spanning	O
108,478	O
days	O
.	O
Another	O
value	O
for	O
the	O
year	O
that	O
is	O
attributed	O
to	O
Hipparchus	PERSON
(	O
by	O
the	O
astrologer	O
Vettius	PERSON
Valens	PERSON
in	O
the	O
1st	O
century	O
)	O
is	O
365	O
+	O
1/4	O
+	O
1/288	O
days	O
(	O
=	O
365.25347	O
...	O
days	O
=	O
365	O
days	O
6	O
hours	O
5	O
min	O
)	O
,	O
but	O
this	O
may	O
be	O
a	O
corruption	O
of	O
another	O
value	O
attributed	O
to	O
a	O
Babylonian	O
source	O
:	O
365	O
+	O
1/4	O
+	O
1/144	O
days	O
(	O
=	O
365.25694	O
...	O
days	O
=	O
365	O
days	O
6	O
hours	O
10	O
min	O
)	O
.	O
It	O
is	O
not	O
clear	O
if	O
this	O
would	O
be	O
a	O
value	O
for	O
the	O
sidereal	O
year	O
(	O
actual	O
value	O
at	O
his	O
time	O
(	O
modern	O
estimate	O
)	O
ca.	O
365.2565	O
days	O
)	O
,	O
but	O
the	O
difference	O
with	O
Hipparchus	O
'	O
value	O
for	O
the	O
tropical	O
year	O
is	O
consistent	O
with	O
his	O
rate	O
of	O
precession	O
(	O
see	O
below	O
)	O
.	O
Before	O
Hipparchus	O
,	O
astronomers	O
knew	O
that	O
the	O
lengths	O
of	O
the	O
seasons	O
are	O
not	O
equal	O
.	O
Hipparchus	O
made	O
observations	O
of	O
equinox	O
and	O
solstice	O
,	O
and	O
according	O
to	O
Ptolemy	O
determined	O
that	O
spring	O
(	O
from	O
spring	O
equinox	O
to	O
summer	O
solstice	O
)	O
lasted	O
94½	O
days	O
,	O
and	O
summer	O
(	O
from	O
summer	O
solstice	O
to	O
autumn	O
equinox	O
)	O
92½	O
days	O
.	O
The	O
value	O
for	O
the	O
eccentricity	O
attributed	O
to	O
Hipparchus	O
by	O
Ptolemy	PERSON
is	O
that	O
the	O
offset	O
is	O
1/24	O
of	O
the	O
radius	O
of	O
the	O
orbit	O
(	O
which	O
is	O
a	O
little	O
too	O
large	O
)	O
,	O
and	O
the	O
direction	O
of	O
the	O
apogee	O
would	O
be	O
at	O
longitude	O
65.5°	O
from	O
the	O
vernal	O
equinox	O
.	O
Hipparchus	O
may	O
also	O
have	O
used	O
other	O
sets	O
of	O
observations	O
,	O
which	O
would	O
lead	O
to	O
different	O
values	O
.	O
His	O
other	O
triplet	O
of	O
solar	O
positions	O
is	O
consistent	O
with	O
94¼	O
and	O
92½	O
days	O
,	O
an	O
improvement	O
on	O
the	O
results	O
(	O
94½	O
and	O
92½	O
days	O
)	O
attributed	O
to	O
Hipparchus	O
by	O
Ptolemy	PERSON
,	O
which	O
a	O
few	O
scholars	O
still	O
question	O
the	O
authorship	O
of	O
.	O
Ptolemy	O
made	O
no	O
change	O
three	O
centuries	O
later	O
,	O
and	O
expressed	O
lengths	O
for	O
the	O
autumn	O
and	O
winter	O
seasons	O
which	O
were	O
already	O
implicit	O
.	O
In	O
the	O
first	O
book	O
,	O
Hipparchus	O
assumes	O
that	O
the	O
parallax	O
of	O
the	O
Sun	O
is	O
0	O
,	O
as	O
if	O
it	O
is	O
at	O
infinite	O
distance	O
.	O
Alexandria	PERSON
and	O
Nicaea	PERSON
are	O
on	O
the	O
same	O
meridian	O
.	O
(	O
It	O
has	O
been	O
contended	O
that	O
authors	O
like	O
Strabo	O
and	O
Ptolemy	O
had	O
fairly	O
decent	O
values	O
for	O
these	O
geographical	O
positions	O
,	O
so	O
Hipparchus	O
must	O
have	O
known	O
them	O
too	O
.	O
This	O
would	O
correspond	O
to	O
a	O
parallax	O
of	O
7	O
'	O
,	O
which	O
is	O
apparently	O
the	O
greatest	O
parallax	O
that	O
Hipparchus	O
thought	O
would	O
not	O
be	O
noticed	O
(	O
for	O
comparison	O
:	O
the	O
typical	O
resolution	O
of	O
the	O
human	O
eye	O
is	O
about	O
2	O
'	O
;	O
Tycho	PERSON
Brahe	PERSON
made	O
naked	O
eye	O
observation	O
with	O
an	O
accuracy	O
down	O
to	O
1	O
'	O
)	O
.	O
Hipparchus	PERSON
thus	O
had	O
the	O
problematic	O
result	O
that	O
his	O
minimum	O
distance	O
(	O
from	O
book	O
1	O
)	O
was	O
greater	O
than	O
his	O
maximum	O
mean	O
distance	O
(	O
from	O
book	O
2	O
)	O
.	O
(	O
In	O
fact	O
,	O
modern	O
calculations	O
show	O
that	O
the	O
size	O
of	O
the	O
190	O
B.C.	O
solar	O
eclipse	O
at	O
Alexandria	O
must	O
have	O
been	O
closer	O
to	O
9/10ths	O
and	O
not	O
the	O
reported	O
4/5ths	O
,	O
a	O
fraction	O
more	O
closely	O
matched	O
by	O
the	O
degree	O
of	O
totality	O
at	O
Alexandria	O
of	O
eclipses	O
occurring	O
in	O
310	O
B.C.	O
and	O
129	O
B.C.	O
which	O
were	O
also	O
nearly	O
total	O
in	O
the	O
Hellespont	O
and	O
are	O
thought	O
by	O
many	O
to	O
be	O
more	O
likely	O
possibilities	O
for	O
the	O
eclipse	O
Hipparchus	O
used	O
for	O
his	O
computations	O
.	O
)	O
Ptolemy	O
later	O
measured	O
the	O
lunar	O
parallax	O
directly	O
,	O
and	O
used	O
the	O
second	O
method	O
of	O
Hipparchus	O
with	O
lunar	O
eclipses	O
to	O
compute	O
the	O
distance	O
of	O
the	O
Sun	O
.	O
He	O
criticizes	O
Hipparchus	O
for	O
making	O
contradictory	O
assumptions	O
,	O
and	O
obtaining	O
conflicting	O
results	O
:	O
but	O
apparently	O
he	O
failed	O
to	O
understand	O
Hipparchus	O
'	O
strategy	O
to	O
establish	O
limits	O
consistent	O
with	O
the	O
observations	O
,	O
rather	O
than	O
a	O
single	O
value	O
for	O
the	O
distance	O
.	O
Apparently	O
Hipparchus	O
later	O
refined	O
his	O
computations	O
,	O
and	O
derived	O
accurate	O
single	O
values	O
that	O
he	O
could	O
use	O
for	O
predictions	O
of	O
solar	O
eclipses	O
.	O
Pliny	O
tells	O
us	O
that	O
Hipparchus	O
demonstrated	O
that	O
lunar	O
eclipses	O
can	O
occur	O
five	O
months	O
apart	O
,	O
and	O
solar	O
eclipses	O
seven	O
months	O
(	O
instead	O
of	O
the	O
usual	O
six	O
months	O
)	O
;	O
and	O
the	O
Sun	O
can	O
be	O
hidden	O
twice	O
in	O
thirty	O
days	O
,	O
but	O
as	O
seen	O
by	O
different	O
nations	O
.	O
Hipparchus	O
apparently	O
made	O
similar	O
calculations	O
.	O
Hipparchus	O
must	O
have	O
been	O
the	O
first	O
to	O
be	O
able	O
to	O
do	O
this	O
.	O
A	O
rigorous	O
treatment	O
requires	O
spherical	O
trigonometry	O
,	O
thus	O
those	O
who	O
remain	O
certain	O
that	O
Hipparchus	O
lacked	O
it	O
must	O
speculate	O
that	O
he	O
may	O
have	O
made	O
do	O
with	O
planar	O
approximations	O
.	O
Pliny	O
also	O
remarks	O
that	O
"	O
he	O
also	O
discovered	O
for	O
what	O
exact	O
reason	O
,	O
although	O
the	O
shadow	O
causing	O
the	O
eclipse	O
must	O
from	O
sunrise	O
onward	O
be	O
below	O
the	O
earth	O
,	O
it	O
happened	O
once	O
in	O
the	O
past	O
that	O
the	O
moon	O
was	O
eclipsed	O
in	O
the	O
west	O
while	O
both	O
luminaries	O
were	O
visible	O
above	O
the	O
earth	O
"	O
,	O
Loeb	O
Classical	O
Library	O
330	O
p.207	O
)	O
.	O
This	O
would	O
be	O
the	O
second	O
eclipse	O
of	O
the	O
345-year	O
interval	O
that	O
Hipparchus	O
used	O
to	O
verify	O
the	O
traditional	O
Babylonian	O
periods	O
:	O
this	O
puts	O
a	O
late	O
date	O
to	O
the	O
development	O
of	O
Hipparchus	O
'	O
lunar	O
theory	O
.	O
Hipparchus	O
and	O
his	O
predecessors	O
used	O
various	O
instruments	O
for	O
astronomical	O
calculations	O
and	O
observations	O
,	O
such	O
as	O
the	O
gnomon	O
,	O
the	O
astrolabe	O
,	O
and	O
the	O
armillary	O
sphere	O
.	O
Hipparchus	O
is	O
credited	O
with	O
the	O
invention	O
or	O
improvement	O
of	O
several	O
astronomical	O
instruments	O
,	O
which	O
were	O
used	O
for	O
a	O
long	O
time	O
for	O
naked-eye	O
observations	O
.	O
With	O
an	O
astrolabe	O
Hipparchus	O
was	O
the	O
first	O
to	O
be	O
able	O
to	O
measure	O
the	O
geographical	O
latitude	O
and	O
time	O
by	O
observing	O
stars	O
.	O
Hipparchus	O
also	O
observed	O
solar	O
equinoxes	O
,	O
which	O
may	O
be	O
done	O
with	O
an	O
equatorial	O
ring	O
:	O
its	O
shadow	O
falls	O
on	O
itself	O
when	O
the	O
Sun	O
is	O
on	O
the	O
equator	O
(	O
i.e.	O
,	O
in	O
one	O
of	O
the	O
equinoctial	O
points	O
on	O
the	O
ecliptic	O
)	O
,	O
but	O
the	O
shadow	O
falls	O
above	O
or	O
below	O
the	O
opposite	O
side	O
of	O
the	O
ring	O
when	O
the	O
Sun	O
is	O
south	O
or	O
north	O
of	O
the	O
equator	O
.	O
Ptolemy	O
quotes	O
)	O
a	O
description	O
by	O
Hipparchus	PERSON
of	O
an	O
equatorial	O
ring	O
in	O
Alexandria	LOCATION
;	O
a	O
little	O
further	O
he	O
describes	O
two	O
such	O
instruments	O
present	O
in	O
Alexandria	O
in	O
his	O
own	O
time	O
.	O
Hipparchus	O
apparently	O
made	O
many	O
detailed	O
corrections	O
to	O
the	O
locations	O
and	O
distances	O
mentioned	O
by	O
Eratosthenes	O
.	O
Late	O
in	O
his	O
career	O
(	O
possibly	O
about	O
135	O
BC	O
)	O
Hipparchus	O
compiled	O
his	O
star	O
catalog	O
,	O
the	O
original	O
of	O
which	O
does	O
not	O
survive	O
.	O
Hipparchus	O
made	O
his	O
measurements	O
with	O
an	O
armillary	O
sphere	O
,	O
and	O
obtained	O
the	O
positions	O
of	O
at	O
least	O
850	O
stars	O
.	O
Ptolemy	O
's	O
catalog	O
in	O
the	O
Almagest	O
,	O
which	O
is	O
derived	O
from	O
Hipparchus	O
'	O
catalog	O
,	O
is	O
given	O
in	O
ecliptic	O
coordinates	O
.	O
As	O
with	O
most	O
of	O
his	O
work	O
,	O
Hipparchus	O
'	O
star	O
catalog	O
was	O
adopted	O
and	O
perhaps	O
expanded	O
by	O
Ptolemy	PERSON
.	O
Ptolemy	O
has	O
even	O
been	O
accused	O
by	O
astronomers	O
of	O
fraud	O
for	O
stating	O
that	O
he	O
observed	O
all	O
1025	O
stars	O
:	O
for	O
almost	O
every	O
star	O
he	O
used	O
Hipparchus	O
'	O
data	O
and	O
precessed	O
it	O
to	O
his	O
own	O
epoch	O
2⅔	O
centuries	O
later	O
by	O
adding	O
2°40	O
'	O
to	O
the	O
longitude	O
,	O
using	O
an	O
erroneously	O
small	O
precession	O
constant	O
of	O
1°	O
per	O
century	O
.	O
In	O
any	O
case	O
the	O
work	O
started	O
by	O
Hipparchus	PERSON
has	O
had	O
a	O
lasting	O
heritage	O
,	O
and	O
was	O
much	O
later	O
updated	O
by	O
Al	O
Sufi	O
(	O
964	O
)	O
and	O
Copernicus	O
(	O
1543	O
)	O
.	O
Hipparchus	O
ranked	O
stars	O
in	O
six	O
magnitude	O
classes	O
according	O
to	O
their	O
brightness	O
:	O
he	O
assigned	O
the	O
value	O
of	O
one	O
to	O
the	O
twenty	O
brightest	O
stars	O
,	O
to	O
weaker	O
ones	O
a	O
value	O
of	O
two	O
,	O
and	O
so	O
forth	O
to	O
the	O
stars	O
with	O
a	O
class	O
of	O
six	O
,	O
which	O
can	O
be	O
barely	O
seen	O
with	O
the	O
naked	O
eye	O
.	O
According	O
to	O
Ptolemy	O
,	O
Hipparchus	O
measured	O
the	O
longitude	O
of	O
Spica	O
and	O
Regulus	O
and	O
other	O
bright	O
stars	O
.	O
Comparing	O
his	O
measurements	O
with	O
data	O
from	O
his	O
predecessors	O
,	O
Timocharis	O
and	O
Aristillus	O
,	O
he	O
concluded	O
that	O
Spica	O
had	O
moved	O
2°	O
relative	O
to	O
the	O
autumnal	O
equinox	O
.	O
Hipparchus	O
concluded	O
that	O
the	O
equinoxes	O
were	O
moving	O
(	O
"	O
precessing	O
"	O
)	O
through	O
the	O
zodiac	O
,	O
and	O
that	O
the	O
rate	O
of	O
precession	O
was	O
not	O
less	O
than	O
1°	O
in	O
a	O
century	O
.	O
The	O
lunar	O
crater	O
Hipparchus	O
and	O
the	O
asteroid	O
4000	O
Hipparchus	O
are	O
more	O
directly	O
named	O
after	O
him	O
.	O
