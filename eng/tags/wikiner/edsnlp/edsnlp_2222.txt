The	O
mythos	O
is	O
not	O
necessarily	O
consistent	O
from	O
story	O
to	O
story	O
,	O
as	O
many	O
writers	O
,	O
including	O
Lovecraft	O
himself	O
,	O
would	O
frequently	O
alter	O
dates	O
,	O
names	O
,	O
and	O
locations	O
to	O
suit	O
the	O
framework	O
of	O
one	O
particular	O
work	O
.	O
During	O
the	O
latter	O
part	O
of	O
Lovecraft	O
's	O
life	O
,	O
there	O
was	O
much	O
borrowing	O
of	O
story	O
elements	O
among	O
the	O
authors	O
of	O
the	O
"	O
Lovecraft	O
Circle	O
"	O
,	O
and	O
many	O
many	O
others	O
,	O
a	O
clique	O
of	O
writers	O
with	O
whom	O
Lovecraft	O
corresponded	O
.	O
This	O
group	O
included	O
Clark	PERSON
Ashton	PERSON
Smith	PERSON
,	O
Robert	PERSON
E.	PERSON
Howard	PERSON
,	O
Robert	PERSON
Bloch	PERSON
,	O
Frank	PERSON
Belknap	PERSON
Long	PERSON
,	O
and	O
Henry	PERSON
Kuttner	PERSON
.	O
Lovecraft	O
recognized	O
that	O
each	O
writer	O
had	O
his	O
own	O
story-cycle	O
,	O
and	O
that	O
an	O
element	O
from	O
one	O
cycle	O
would	O
not	O
necessarily	O
become	O
part	O
of	O
another	O
simply	O
because	O
a	O
writer	O
used	O
it	O
in	O
one	O
of	O
his	O
stories	O
.	O
Howard	O
frequently	O
corresponded	O
with	O
H.	O
P.	O
Lovecraft	PERSON
,	O
and	O
the	O
two	O
would	O
sometimes	O
insert	O
references	O
or	O
elements	O
of	O
each	O
others	O
'	O
settings	O
in	O
their	O
works	O
.	O
Later	O
editors	O
reworked	O
many	O
of	O
the	O
original	O
Conan	O
stories	O
by	O
Howard	O
;	O
thus	O
,	O
diluting	O
this	O
connection	O
.	O
Thus	O
,	O
Lovecraft	O
's	O
"	O
pseudomythology	O
"	O
--	O
a	O
term	O
used	O
by	O
Lovecraft	O
himself	O
and	O
others	O
to	O
describe	O
the	O
beings	O
appearing	O
in	O
his	O
stories	O
--	O
is	O
the	O
backdrop	O
for	O
his	O
tales	O
but	O
is	O
not	O
the	O
primary	O
focus	O
.	O
Furthermore	O
,	O
Lovecraft	O
may	O
not	O
have	O
been	O
serious	O
when	O
he	O
spoke	O
of	O
developing	O
a	O
"	O
myth-cycle	O
"	O
and	O
probably	O
would	O
have	O
had	O
no	O
need	O
to	O
give	O
it	O
a	O
name	O
anyway	O
.	O
Perhaps	O
the	O
best	O
example	O
of	O
the	O
impact	O
of	O
the	O
mythos	O
is	O
Lovecraft	O
's	O
most	O
recognizable	O
contribution	O
to	O
popular	O
culture	O
:	O
the	O
"	O
Necronomicon	O
"	O
,	O
a	O
fictional	O
grimoire	O
referenced	O
in	O
many	O
of	O
the	O
works	O
written	O
by	O
him	O
,	O
his	O
circle	O
,	O
and	O
others	O
;	O
indeed	O
,	O
it	O
has	O
been	O
referenced	O
by	O
authors	O
who	O
appear	O
to	O
be	O
unaware	O
that	O
it	O
's	O
fictional	O
.	O
While	O
fictitious	O
(	O
and	O
Lovecraft	O
himself	O
had	O
to	O
work	O
to	O
dispel	O
rumors	O
of	O
its	O
actual	O
existence	O
)	O
,	O
it	O
is	O
referenced	O
often	O
in	O
popular	O
culture	O
in	O
movies	O
,	O
music	O
and	O
literature	O
as	O
an	O
ancient	O
book	O
of	O
incredible	O
evil	O
.	O
The	O
second	O
stage	O
began	O
with	O
August	PERSON
Derleth	PERSON
,	O
who	O
added	O
to	O
the	O
mythos	O
and	O
developed	O
the	O
elemental	O
system	O
,	O
associating	O
the	O
pantheon	O
with	O
the	O
four	O
elements	O
of	O
air	O
,	O
earth	O
,	O
fire	O
,	O
and	O
water	O
.	O
Derleth	O
combined	O
Lovecraft	O
's	O
various	O
cycles	O
to	O
create	O
a	O
large	O
,	O
singular	O
story-cycle	O
[	O
citation	O
needed	O
]	O
.	O
As	O
Lovecraft	O
famously	O
begins	O
his	O
short	O
story	O
,	O
The	O
Call	O
of	O
Cthulhu	O
,	O
"	O
The	O
most	O
merciful	O
thing	O
in	O
the	O
world	O
,	O
I	O
think	O
,	O
is	O
the	O
inability	O
of	O
the	O
human	O
mind	O
to	O
correlate	O
all	O
its	O
contents	O
.	O
"	O
The	O
Call	O
of	O
Cthulhu	O
was	O
the	O
premiere	O
story	O
in	O
which	O
Lovecraft	O
realized	O
and	O
made	O
full	O
use	O
of	O
these	O
themes,	O
[	O
citation	O
needed	O
]	O
which	O
is	O
why	O
his	O
mythology	O
would	O
later	O
be	O
named	O
after	O
the	O
creature	O
in	O
this	O
story	O
,	O
as	O
it	O
defined	O
a	O
new	O
direction	O
in	O
both	O
his	O
authorship	O
and	O
in	O
the	O
horror	O
fiction	O
genre	O
.	O
In	O
his	O
final	O
years	O
,	O
Lovecraft	PERSON
used	O
fewer	O
supernatural	O
elements	O
to	O
represent	O
the	O
dangers	O
which	O
threaten	O
humanity	O
.	O
This	O
science-fictional	O
trend	O
particularly	O
becomes	O
clear	O
in	O
works	O
such	O
as	O
At	O
the	O
Mountains	O
of	O
Madness	O
.	O
Derleth	O
's	O
take	O
on	O
the	O
mythos	O
drop	O
the	O
themes	O
of	O
human	O
meaninglessness	O
and	O
moved	O
the	O
story	O
more	O
towards	O
his	O
views	O
on	O
Roman	O
Catholic	O
cosmology	O
and	O
moral	O
principles	O
.	O
Instead	O
of	O
a	O
universe	O
of	O
meaninglessness	O
and	O
chaos	O
,	O
Derleth	O
's	O
mythos	O
is	O
a	O
struggle	O
of	O
good	O
versus	O
evil	O
.	O
Derleth	O
once	O
wrote	O
:	O
Lovecraft	O
was	O
an	O
atheist	O
,	O
and	O
claimed	O
that	O
Kant	O
's	O
ethical	O
system	O
"	O
is	O
a	O
joke	O
.	O
Finally	O
,	O
Derleth	O
matched	O
the	O
earth	O
beings	O
against	O
the	O
fire	O
beings	O
and	O
the	O
air	O
beings	O
against	O
the	O
water	O
beings	O
,	O
which	O
is	O
inconsistent	O
with	O
the	O
classical	O
elements	O
dichotomy	O
in	O
which	O
air	O
opposes	O
earth	O
and	O
fire	O
opposes	O
water	O
.	O
Derleth	O
became	O
a	O
publisher	O
of	O
Lovecraft	O
's	O
stories	O
after	O
his	O
death	O
.	O
Lovecraft	O
himself	O
was	O
very	O
critical	O
of	O
his	O
own	O
writings	O
and	O
was	O
often	O
easily	O
discouraged	O
,	O
especially	O
when	O
faced	O
with	O
any	O
rejection	O
of	O
his	O
work	O
.	O
Another	O
influence	O
has	O
been	O
the	O
Call	O
of	O
Cthulhu	O
RPG	O
published	O
by	O
Chaosium	O
in	O
1981	O
.	O
