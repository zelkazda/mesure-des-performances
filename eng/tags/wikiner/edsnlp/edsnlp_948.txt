Arthur	PERSON
William	PERSON
à	O
Beckett	LOCATION
(	O
25	O
October	O
1844	O
Fulham	O
--	O
14	O
January	O
1909	O
London	LOCATION
)	O
was	O
an	O
English	O
journalist	O
and	O
man	O
of	O
letters	O
.	O
He	O
was	O
a	O
younger	O
son	O
of	O
Gilbert	PERSON
Abbott	PERSON
à	O
Beckett	LOCATION
,	O
brother	O
of	O
Gilbert	PERSON
Arthur	PERSON
à	O
Beckett	LOCATION
and	O
educated	O
at	O
Felsted	ORGANIZATION
School	ORGANIZATION
.	O
A	O
childhood	O
friend	O
(	O
and	O
distant	O
relative	O
)	O
of	O
W.	PERSON
S.	PERSON
Gilbert	PERSON
,	O
Beckett	O
briefly	O
feuded	O
with	O
Gilbert	PERSON
in	O
1869	O
,	O
but	O
the	O
two	O
patched	O
up	O
the	O
friendship	O
,	O
and	O
Gilbert	O
even	O
later	O
collaborated	O
on	O
projects	O
with	O
Beckett	PERSON
's	O
brother	O
.	O
