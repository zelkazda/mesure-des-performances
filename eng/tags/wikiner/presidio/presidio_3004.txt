As	O
with	O
the	O
other	O
joint	O
chiefs	O
,	O
the	O
Commandant	O
is	O
an	O
administrative	O
position	O
and	O
has	O
no	O
operational	O
command	O
authority	O
over	O
United	O
States	O
Marine	O
Corps	O
forces	O
.	O
The	O
first	O
Commandant	O
was	O
Samuel	PERSON
Nicholas	PERSON
,	O
who	O
took	O
office	O
as	O
a	O
captain	O
,	O
though	O
there	O
was	O
no	O
office	O
titled	O
"	O
Commandant	O
"	O
at	O
the	O
time	O
,	O
and	O
the	O
Second	O
Continental	O
Congress	O
had	O
authorized	O
that	O
the	O
senior-most	O
Marine	O
could	O
take	O
a	O
rank	O
up	O
to	O
Colonel	O
.	O
In	O
the	O
234-year	O
history	O
of	O
the	O
United	O
States	O
Marine	O
Corps	O
,	O
only	O
one	O
Commandant	O
has	O
ever	O
been	O
fired	O
from	O
the	O
job	O
:	O
Anthony	PERSON
Gale	PERSON
,	O
as	O
a	O
result	O
of	O
a	O
court-martial	O
in	O
1820	O
.	O
If	O
nominated	O
by	O
President	O
Barack	PERSON
Obama	PERSON
,	O
his	O
move	O
would	O
break	O
the	O
tradition	O
of	O
the	O
Commandant	O
being	O
an	O
infantry	O
officer	O
.	O
This	O
article	O
incorporates	O
public	O
domain	O
material	O
from	O
websites	O
or	O
documents	O
of	O
the	O
United	O
States	O
Marine	O
Corps	O
.	O
