The	O
film	O
begins	O
with	O
a	O
squirrel	O
known	O
as	O
Scrat	O
,	O
who	O
is	O
trying	O
to	O
find	O
a	O
location	O
to	O
store	O
his	O
prized	O
acorn	O
.	O
There	O
is	O
also	O
a	O
subplot	O
where	O
an	O
animal	O
named	O
Scrat	O
makes	O
many	O
comical	O
attempts	O
to	O
bury	O
his	O
beloved	O
acorn	O
.	O
The	O
sun	O
slowly	O
melts	O
the	O
cube	O
,	O
thawing	O
Scrat	O
and	O
the	O
ice	O
surrounding	O
his	O
acorn	O
,	O
which	O
is	O
barely	O
out	O
of	O
reach	O
,	O
and	O
ends	O
up	O
being	O
removed	O
from	O
the	O
ice	O
cube	O
by	O
the	O
tide	O
.	O
Scrat	O
then	O
explodes	O
out	O
of	O
the	O
ice	O
cube	O
in	O
anger	O
and	O
hits	O
his	O
head	O
repetitively	O
on	O
a	O
tree	O
,	O
which	O
drops	O
a	O
coconut	O
.	O
Believing	O
it	O
to	O
be	O
a	O
giant	O
acorn	O
,	O
Scrat	PERSON
's	O
anger	O
immediately	O
turns	O
to	O
glee	O
at	O
this	O
new	O
find	O
.	O
He	O
tries	O
to	O
pack	O
it	O
into	O
the	O
ground	O
as	O
he	O
did	O
previously	O
with	O
his	O
acorns	O
,	O
but	O
in	O
the	O
process	O
causes	O
a	O
volcanic	O
eruption	O
,	O
mirroring	O
the	O
opening	O
scene	O
when	O
Scrat	O
causes	O
a	O
break	O
in	O
the	O
ice	O
with	O
an	O
acorn	O
.	O
Ice	O
Age	O
was	O
released	O
into	O
theaters	O
on	O
March	O
15	O
,	O
2002	O
and	O
got	O
a	O
78	O
%	O
approval	O
rating	O
on	O
the	O
film	O
review	O
aggregator	O
Rotten	O
Tomatoes	O
,	O
out	O
of	O
131	O
reviews	O
.	O
Ice	O
Age	O
broke	O
the	O
record	O
for	O
a	O
March	O
opening	O
(	O
first	O
surpassed	O
in	O
2006	O
by	O
its	O
sequel	O
,	O
Ice	O
Age	O
:	O
The	O
Meltdown	O
)	O
and	O
was	O
the	O
then-third-best	O
opening	O
ever	O
for	O
an	O
animated	O
feature	O
--	O
after	O
Monsters	O
Inc	O
.	O
(	O
$	O
62.6	O
million	O
)	O
and	O
Toy	O
Story	O
2	O
(	O
$	O
57.4	O
million	O
)	O
.	O
Ice	O
Age	O
finished	O
its	O
domestic	O
box	O
office	O
run	O
with	O
$	O
176,387,405	O
million	O
and	O
grossed	O
$	O
383,257,136	O
million	O
worldwide	O
,	O
being	O
the	O
9th	O
highest	O
gross	O
of	O
2002	O
in	O
North	LOCATION
America	LOCATION
and	O
the	O
8th	O
best	O
worldwide	O
at	O
the	O
time	O
.	O
A	O
video	O
game	O
tie-in	O
was	O
published	O
by	O
Ubisoft	O
for	O
the	O
Game	O
Boy	O
Advance	O
,	O
and	O
received	O
poor	O
reviews	O
.	O
