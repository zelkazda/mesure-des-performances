Following	O
the	O
band	O
's	O
break-up	O
,	O
he	O
had	O
a	O
successful	O
career	O
as	O
a	O
solo	O
artist	O
and	O
later	O
as	O
part	O
of	O
the	O
Traveling	O
Wilburys	O
,	O
and	O
also	O
as	O
a	O
film	O
and	O
record	O
producer	O
.	O
His	O
later	O
compositions	O
with	O
The	O
Beatles	O
include	O
"	O
Here	O
Comes	O
the	O
Sun	O
"	O
,	O
"	O
Something	O
"	O
and	O
"	O
While	O
My	O
Guitar	O
Gently	O
Weeps	O
"	O
.	O
In	O
addition	O
to	O
his	O
solo	O
work	O
,	O
Harrison	PERSON
co-wrote	O
two	O
hits	O
for	O
Ringo	PERSON
Starr	PERSON
,	O
another	O
former	O
Beatle	PERSON
,	O
as	O
well	O
as	O
songs	O
for	O
the	O
Traveling	O
Wilburys	O
--	O
the	O
supergroup	O
he	O
formed	O
in	O
1988	O
with	O
Bob	PERSON
Dylan	PERSON
,	O
Tom	PERSON
Petty	PERSON
,	O
Jeff	PERSON
Lynne	PERSON
,	O
and	O
Roy	PERSON
Orbison	PERSON
.	O
Besides	O
being	O
a	O
musician	O
,	O
he	O
was	O
also	O
a	O
record	O
producer	O
and	O
co-founder	O
of	O
the	O
production	O
company	O
HandMade	O
Films	O
.	O
In	O
his	O
work	O
as	O
a	O
film	O
producer	O
,	O
he	O
collaborated	O
with	O
people	O
as	O
diverse	O
as	O
the	O
members	O
of	O
Monty	PERSON
Python	PERSON
and	O
Madonna	PERSON
.	O
He	O
was	O
married	O
twice	O
,	O
to	O
the	O
model	O
Pattie	PERSON
Boyd	PERSON
in	O
1966	O
,	O
and	O
to	O
the	O
record	O
company	O
secretary	O
Olivia	PERSON
Trinidad	PERSON
Arias	PERSON
in	O
1978	O
,	O
with	O
whom	O
he	O
had	O
one	O
son	O
,	O
Dhani	PERSON
Harrison	PERSON
.	O
He	O
was	O
a	O
close	O
friend	O
of	O
Eric	PERSON
Clapton	PERSON
.	O
Harrison	PERSON
died	O
of	O
lung	O
cancer	O
in	O
2001	O
.	O
His	O
mother	O
was	O
a	O
Liverpool	LOCATION
shop	O
assistant	O
,	O
and	O
his	O
father	O
was	O
a	O
bus	O
conductor	O
who	O
had	O
worked	O
as	O
a	O
ship	O
's	O
steward	O
on	O
the	O
White	O
Star	O
Line	O
.	O
Harrison	PERSON
was	O
born	O
in	O
the	O
house	O
where	O
he	O
lived	O
for	O
his	O
first	O
six	O
years	O
:	O
12	O
Arnold	PERSON
Grove	PERSON
,	O
Wavertree	LOCATION
,	O
Liverpool	LOCATION
,	O
which	O
was	O
a	O
small	O
2	O
up	O
,	O
2	O
down	O
terraced	O
house	O
in	O
a	O
cul-de-sac	O
,	O
with	O
an	O
alley	O
to	O
the	O
rear	O
.	O
He	O
passed	O
his	O
11-plus	O
examination	O
and	O
achieved	O
a	O
place	O
at	O
the	O
Liverpool	O
Institute	O
for	O
Boys	O
(	O
in	O
the	O
building	O
that	O
now	O
houses	O
the	O
Liverpool	O
Institute	O
for	O
Performing	O
Arts	O
)	O
,	O
which	O
he	O
attended	O
from	O
1954	O
to	O
1959	O
.	O
When	O
Harrison	PERSON
was	O
14	O
years	O
old	O
,	O
he	O
sat	O
at	O
the	O
back	O
of	O
the	O
class	O
and	O
tried	O
drawing	O
guitars	O
in	O
his	O
schoolbooks	O
:	O
"	O
I	O
was	O
totally	O
into	O
guitars	O
.	O
At	O
this	O
school	O
he	O
met	O
Paul	PERSON
McCartney	PERSON
,	O
who	O
was	O
one	O
year	O
older	O
.	O
McCartney	PERSON
later	O
became	O
a	O
member	O
of	O
John	PERSON
Lennon	PERSON
's	PERSON
band	O
called	O
The	O
Quarrymen	O
,	O
which	O
Harrison	PERSON
joined	O
in	O
1958	O
.	O
McCartney	PERSON
told	O
Lennon	PERSON
about	O
his	O
friend	O
George	PERSON
Harrison	PERSON
,	O
who	O
could	O
play	O
"	O
Raunchy	O
"	O
on	O
his	O
guitar	O
.	O
Although	O
Lennon	PERSON
considered	O
him	O
too	O
young	O
to	O
join	O
the	O
band	O
,	O
Harrison	PERSON
hung	O
out	O
with	O
them	O
and	O
filled	O
in	O
as	O
needed	O
.	O
By	O
the	O
time	O
Harrison	PERSON
was	O
16	O
,	O
Lennon	PERSON
and	O
the	O
others	O
had	O
accepted	O
him	O
as	O
one	O
of	O
the	O
band	O
.	O
Since	O
Harrison	PERSON
was	O
the	O
youngest	O
member	O
of	O
the	O
group	O
,	O
he	O
was	O
looked	O
upon	O
as	O
a	O
kid	O
by	O
the	O
others	O
for	O
another	O
few	O
years	O
.	O
Harrison	PERSON
left	O
school	O
at	O
16	O
and	O
worked	O
as	O
an	O
apprentice	O
electrician	O
at	O
local	O
department	O
store	O
Blacklers	O
for	O
a	O
while	O
.	O
In	O
a	O
letter	O
to	O
a	O
fan	O
,	O
Harrison	PERSON
mentioned	O
jelly	O
babies	O
,	O
insisting	O
that	O
no	O
one	O
in	O
the	O
band	O
actually	O
liked	O
them	O
and	O
that	O
the	O
press	O
must	O
have	O
made	O
it	O
up	O
.	O
Harrison	PERSON
's	O
musical	O
involvement	O
and	O
cohesion	O
with	O
the	O
group	O
reached	O
its	O
peak	O
on	O
Revolver	O
in	O
1966	O
with	O
his	O
contribution	O
of	O
three	O
songs	O
and	O
new	O
musical	O
ideas	O
.	O
During	O
the	O
recording	O
of	O
The	O
Beatles	O
in	O
1968	O
,	O
tensions	O
were	O
present	O
in	O
the	O
band	O
;	O
these	O
surfaced	O
again	O
during	O
the	O
filming	O
of	O
rehearsal	O
sessions	O
at	O
Twickenham	O
Studios	O
for	O
the	O
album	O
Let	O
It	O
Be	O
in	O
early	O
1969	O
.	O
Frustrated	O
by	O
ongoing	O
slights	O
,	O
the	O
poor	O
working	O
conditions	O
in	O
the	O
cold	O
and	O
sterile	O
film	O
studio	O
,	O
and	O
Lennon	PERSON
's	O
creative	O
disengagement	O
from	O
the	O
group	O
,	O
Harrison	PERSON
quit	O
the	O
band	O
on	O
10	O
January	O
.	O
The	O
album	O
included	O
"	O
Here	O
Comes	O
the	O
Sun	O
"	O
and	O
"	O
Something	O
"	O
,	O
which	O
was	O
later	O
recorded	O
by	O
Frank	PERSON
Sinatra	PERSON
,	O
who	O
considered	O
it	O
"	O
one	O
of	O
the	O
greatest	O
songs	O
of	O
the	O
last	O
twenty	O
years	O
"	O
.	O
Harrison	PERSON
's	O
last	O
recording	O
session	O
with	O
The	O
Beatles	O
was	O
on	O
4	O
January	O
1970	O
.	O
Lennon	PERSON
,	O
who	O
had	O
left	O
the	O
group	O
the	O
previous	O
September	O
,	O
did	O
not	O
attend	O
the	O
session	O
.	O
Ringo	PERSON
Starr	PERSON
also	O
stated	O
,	O
"	O
We	O
really	O
looked	O
out	O
for	O
each	O
other	O
and	O
we	O
had	O
so	O
many	O
laughs	O
together	O
.	O
John	PERSON
Lennon	PERSON
stated	O
that	O
his	O
relationship	O
with	O
George	PERSON
was	O
"	O
one	O
of	O
young	O
follower	O
and	O
older	O
guy	O
.	O
"	O
and	O
admitted	O
that	O
"	O
[	O
George	PERSON
]	O
was	O
like	O
a	O
disciple	O
of	O
mine	O
when	O
we	O
started	O
.	O
"	O
Lennon	PERSON
felt	O
insulted	O
and	O
hurt	O
that	O
George	PERSON
mentioned	O
him	O
only	O
in	O
passing	O
.	O
As	O
a	O
result	O
,	O
George	PERSON
and	O
John	PERSON
were	O
not	O
on	O
good	O
terms	O
during	O
the	O
last	O
years	O
of	O
Lennon	PERSON
's	O
life	O
.	O
After	O
Lennon	PERSON
's	O
murder	O
,	O
George	PERSON
paid	O
tribute	O
to	O
Lennon	PERSON
with	O
his	O
song	O
"	O
All	O
Those	O
Years	O
Ago	O
"	O
which	O
was	O
released	O
in	O
1981	O
,	O
six	O
months	O
after	O
Lennon	PERSON
's	O
murder	O
.	O
Paul	PERSON
McCartney	PERSON
has	O
often	O
referred	O
to	O
Harrison	PERSON
as	O
his	O
"	O
baby	O
brother	O
"	O
,	O
and	O
he	O
did	O
the	O
honours	O
as	O
best	O
man	O
at	O
George	PERSON
's	O
wedding	O
in	O
1966	O
.	O
McCartney	PERSON
stated	O
that	O
he	O
and	O
George	PERSON
usually	O
shared	O
bedrooms	O
together	O
while	O
touring	O
.	O
Harrison	PERSON
's	O
guitar	O
work	O
with	O
the	O
Beatles	O
was	O
varied	O
,	O
flexible	O
and	O
innovative	O
;	O
although	O
not	O
fast	O
or	O
flashy	O
,	O
his	O
guitar	O
playing	O
was	O
solid	O
and	O
typified	O
the	O
more	O
subdued	O
lead	O
guitar	O
style	O
of	O
the	O
early	O
1960s	O
.	O
Harrison	PERSON
explored	O
several	O
guitar	O
instruments	O
,	O
the	O
twelve-string	O
,	O
the	O
sitar	O
and	O
the	O
slide	O
guitar	O
,	O
and	O
developed	O
his	O
playing	O
from	O
tight	O
eight-and	O
twelve-bar	O
solos	O
in	O
such	O
songs	O
as	O
"	O
A	O
Hard	O
Day	O
's	O
Night	O
"	O
and	O
"	O
Ca	O
n't	O
Buy	O
Me	O
Love	O
"	O
,	O
to	O
lyrical	O
slide	O
guitar	O
playing	O
,	O
first	O
recorded	O
during	O
an	O
early	O
session	O
of	O
"	O
If	O
Not	O
for	O
You	O
"	O
for	O
Dylan	PERSON
's	O
New	O
Morning	O
in	O
1970	O
.	O
The	O
earliest	O
example	O
of	O
notable	O
guitar	O
work	O
from	O
Harrison	PERSON
was	O
the	O
extended	O
acoustic	O
guitar	O
solo	O
of	O
"	O
Till	O
There	O
Was	O
You	O
"	O
,	O
for	O
which	O
Harrison	PERSON
purchased	O
a	O
José	PERSON
Ramírez	PERSON
nylon-stringed	O
classical	O
guitar	O
to	O
produce	O
the	O
sensitivity	O
needed	O
.	O
However	O
,	O
the	O
guitars	O
Harrison	PERSON
used	O
on	O
early	O
recordings	O
were	O
mainly	O
Gretsch	PERSON
played	O
through	O
a	O
Vox	O
amp	O
.	O
Harrison	PERSON
used	O
the	O
guitar	O
extensively	O
during	O
the	O
recording	O
of	O
A	O
Hard	O
Day	O
's	O
Night	O
,	O
and	O
the	O
jangly	O
sound	O
became	O
so	O
popular	O
that	O
the	O
Melody	O
Maker	O
termed	O
it	O
"	O
the	O
beat	O
boys	O
'	O
secret	O
weapon	O
"	O
.	O
Roger	PERSON
McGuinn	PERSON
liked	O
the	O
effect	O
Harrison	PERSON
achieved	O
so	O
much	O
that	O
it	O
became	O
his	O
signature	O
guitar	O
sound	O
with	O
the	O
Byrds	O
.	O
He	O
played	O
this	O
guitar	O
in	O
the	O
Magical	O
Mystery	O
Tour	O
film	O
and	O
throughout	O
his	O
solo	O
career	O
.	O
Harrison	PERSON
listed	O
his	O
early	O
influences	O
as	O
Carl	PERSON
Perkins	PERSON
,	O
Bo	PERSON
Diddley	PERSON
,	O
Chuck	PERSON
Berry	PERSON
and	O
the	O
Everly	O
Brothers	O
.	O
The	O
group	O
did	O
not	O
record	O
another	O
Harrison	PERSON
composition	O
until	O
1965	O
,	O
when	O
he	O
contributed	O
"	O
I	O
Need	O
You	O
"	O
and	O
"	O
You	O
Like	O
Me	O
Too	O
Much	O
"	O
to	O
the	O
album	O
Help	O
!	O
.	O
McCartney	PERSON
told	O
Lennon	PERSON
in	O
1969	O
:	O
"	O
Until	O
this	O
year	O
,	O
our	O
songs	O
have	O
been	O
better	O
than	O
George	PERSON
's	O
.	O
Harrison	PERSON
had	O
difficulty	O
getting	O
the	O
band	O
to	O
record	O
his	O
songs	O
.	O
The	O
group	O
's	O
incorporation	O
of	O
Harrison	PERSON
's	O
material	O
reached	O
a	O
peak	O
of	O
three	O
songs	O
on	O
the	O
1966	O
Revolver	O
album	O
and	O
four	O
songs	O
on	O
the	O
1968	O
double	O
The	O
Beatles	O
.	O
He	O
also	O
sang	O
lead	O
vocal	O
on	O
other	O
songs	O
,	O
including	O
"	O
Chains	O
"	O
and	O
"	O
Do	O
You	O
Want	O
to	O
Know	O
a	O
Secret	O
"	O
on	O
Please	O
Please	O
Me	O
,	O
"	O
Roll	O
Over	O
Beethoven	PERSON
"	O
and	O
"	O
Devil	O
in	O
Her	O
Heart	O
"	O
on	O
With	O
The	O
Beatles	O
,	O
"	O
I	O
'm	O
Happy	O
Just	O
to	O
Dance	O
with	O
You	O
"	O
on	O
A	O
Hard	O
Day	O
's	O
Night	O
,	O
and	O
"	O
Everybody	O
's	O
Trying	O
to	O
Be	O
My	O
Baby	O
"	O
on	O
Beatles	O
for	O
Sale	O
.	O
Harrison	PERSON
denied	O
deliberately	O
stealing	O
the	O
song	O
,	O
but	O
he	O
lost	O
the	O
resulting	O
court	O
case	O
in	O
1976	O
as	O
the	O
judge	O
accepted	O
that	O
Harrison	PERSON
had	O
"	O
subconsciously	O
"	O
plagiarised	O
"	O
He	O
's	O
So	O
Fine	O
"	O
.	O
The	O
aim	O
of	O
the	O
event	O
was	O
to	O
raise	O
money	O
to	O
aid	O
the	O
starving	O
refugees	O
during	O
the	O
Bangladesh	O
Liberation	O
War	O
.	O
Ravi	PERSON
Shankar	PERSON
opened	O
the	O
proceedings	O
,	O
which	O
included	O
other	O
popular	O
musicians	O
such	O
as	O
Bob	PERSON
Dylan	PERSON
(	O
who	O
rarely	O
appeared	O
live	O
in	O
the	O
early	O
1970s	O
)	O
,	O
Eric	PERSON
Clapton	PERSON
,	O
who	O
made	O
his	O
first	O
public	O
appearance	O
in	O
months	O
(	O
due	O
to	O
a	O
heroin	O
addiction	O
which	O
began	O
when	O
Derek	PERSON
and	O
the	O
Dominos	O
broke	O
up	O
)	O
,	O
Leon	PERSON
Russell	PERSON
,	O
Badfinger	O
,	O
Billy	PERSON
Preston	PERSON
and	O
fellow	O
Beatle	PERSON
Ringo	PERSON
Starr	PERSON
.	O
Harrison	PERSON
would	O
not	O
again	O
release	O
an	O
album	O
that	O
came	O
close	O
to	O
the	O
critical	O
and	O
commercial	O
achievements	O
of	O
All	O
Things	O
Must	O
Pass	O
.	O
His	O
final	O
studio	O
album	O
for	O
EMI	O
(	O
and	O
Apple	O
Records	O
)	O
was	O
Extra	O
Texture	O
(	O
Read	O
All	O
About	O
It	O
)	O
,	O
featuring	O
a	O
diecut	O
cover	O
.	O
Thirty	O
Three	O
&	O
1/3	O
his	O
first	O
Dark	O
Horse	O
release	O
,	O
was	O
his	O
most	O
successful	O
late-1970s	O
album	O
,	O
reaching	O
number	O
11	O
on	O
the	O
US	LOCATION
charts	O
in	O
1976	O
,	O
and	O
producing	O
the	O
singles	O
"	O
This	O
Song	O
"	O
and	O
"	O
Crackerbox	O
Palace	O
"	O
,	O
both	O
of	O
which	O
reached	O
the	O
top	O
25	O
in	O
the	O
US	LOCATION
.	O
With	O
an	O
emphasis	O
on	O
melody	O
,	O
musicianship	O
,	O
and	O
subtler	O
subject	O
matter	O
rather	O
than	O
the	O
heavy	O
orchestration	O
and	O
didactic	O
messaging	O
of	O
earlier	O
works	O
,	O
he	O
received	O
his	O
best	O
critical	O
notices	O
since	O
All	O
Things	O
Must	O
Pass	O
.	O
With	O
its	O
surreal	O
humour	O
,	O
"	O
Crackerbox	O
Palace	O
"	O
also	O
reflected	O
Harrison	PERSON
's	O
association	O
with	O
Monty	PERSON
Python	PERSON
's	PERSON
Eric	PERSON
Idle	PERSON
,	O
who	O
directed	O
a	O
comic	O
music	O
video	O
for	O
the	O
song	O
.	O
After	O
his	O
second	O
marriage	O
and	O
the	O
birth	O
of	O
son	O
Dhani	PERSON
Harrison	PERSON
,	O
Harrison	PERSON
's	O
next	O
released	O
a	O
self-titled	O
album	O
.	O
1979	O
's	O
George	PERSON
Harrison	PERSON
included	O
the	O
singles	O
"	O
Blow	O
Away	O
"	O
,	O
"	O
Love	O
Comes	O
to	O
Everyone	O
"	O
and	O
"	O
Faster	O
"	O
.	O
In	O
addition	O
to	O
his	O
own	O
works	O
during	O
this	O
time	O
,	O
between	O
1971	O
and	O
1973	O
Harrison	PERSON
co-wrote	O
or	O
produced	O
three	O
top	O
ten	O
US	LOCATION
and	O
UK	LOCATION
hits	O
for	O
Ringo	PERSON
Starr	PERSON
(	O
"	O
It	O
Do	O
n't	O
Come	O
Easy	O
"	O
,	O
"	O
Back	O
Off	O
Boogaloo	O
"	O
,	O
and	O
"	O
Photograph	O
"	O
)	O
.	O
Harrison	PERSON
played	O
electric	O
,	O
slide	O
and	O
dobro	PERSON
guitars	O
on	O
five	O
songs	O
on	O
John	PERSON
Lennon	PERSON
's	PERSON
1971	O
Imagine	O
album	O
(	O
"	O
How	O
Do	O
You	O
Sleep	O
?	O
"	O
,	O
"	O
Oh	O
My	O
Love	O
"	O
,	O
"	O
I	O
Do	O
n't	O
Want	O
to	O
Be	O
a	O
Soldier	O
"	O
,	O
"	O
Crippled	O
Inside	O
"	O
and	O
"	O
Gim	O
me	O
Some	O
Truth	O
"	O
)	O
,	O
with	O
his	O
stinging	O
slide	O
guitar	O
work	O
on	O
the	O
first	O
of	O
these	O
indicating	O
that	O
he	O
took	O
John	PERSON
's	O
side	O
of	O
the	O
intense	O
Lennon-McCartney	O
feud	O
of	O
the	O
time	O
.	O
Lennon	PERSON
later	O
said	O
of	O
Harrison	PERSON
's	O
work	O
on	O
the	O
album	O
,	O
"	O
That	O
's	O
the	O
best	O
he	O
's	O
ever	O
fucking	O
played	O
in	O
his	O
life	O
!	O
"	O
Harrison	PERSON
also	O
produced	O
and	O
played	O
slide	O
guitar	O
on	O
the	O
Apple	O
band	O
Badfinger	O
's	O
1971	O
top	O
ten	O
US	LOCATION
and	O
UK	LOCATION
hit	O
"	O
Day	O
After	O
Day	O
"	O
.	O
During	O
the	O
decade	O
,	O
Harrison	PERSON
also	O
worked	O
with	O
Harry	PERSON
Nilsson	PERSON
,	O
as	O
well	O
as	O
Billy	PERSON
Preston	PERSON
(	O
"	O
That	O
's	O
the	O
Way	O
God	O
Planned	O
It	O
"	O
,	O
1969	O
and	O
"	O
It	O
's	O
My	O
Pleasure	O
"	O
,	O
1975	O
)	O
and	O
Cheech	O
&	O
Chong	O
(	O
"	O
Basketball	O
Jones	O
"	O
,	O
1973	O
)	O
.	O
He	O
also	O
appeared	O
with	O
Paul	PERSON
Simon	PERSON
to	O
perform	O
two	O
acoustic	O
songs	O
on	O
Saturday	O
Night	O
Live	O
.	O
Harrison	PERSON
was	O
deeply	O
shocked	O
by	O
the	O
8	O
December	O
1980	O
murder	O
of	O
John	PERSON
Lennon	PERSON
.	O
It	O
was	O
also	O
a	O
deep	O
personal	O
loss	O
,	O
although	O
unlike	O
former	O
bandmates	O
McCartney	PERSON
and	O
Starr	PERSON
,	O
Harrison	PERSON
had	O
little	O
contact	O
with	O
Lennon	PERSON
in	O
the	O
years	O
before	O
the	O
murder	O
.	O
The	O
omission	O
had	O
upset	O
Lennon	PERSON
greatly	O
,	O
which	O
Harrison	PERSON
had	O
regretted	O
,	O
leading	O
him	O
to	O
leave	O
a	O
telephone	O
message	O
for	O
Lennon	PERSON
,	O
but	O
Lennon	PERSON
had	O
declined	O
to	O
return	O
the	O
call	O
and	O
they	O
had	O
not	O
spoken	O
again	O
.	O
Following	O
the	O
murder	O
,	O
Harrison	PERSON
said	O
,	O
"	O
After	O
all	O
we	O
went	O
through	O
together	O
I	O
had	O
and	O
still	O
have	O
great	O
love	O
and	O
respect	O
for	O
John	PERSON
Lennon	PERSON
.	O
Harrison	PERSON
modified	O
the	O
lyrics	O
of	O
a	O
song	O
he	O
had	O
written	O
for	O
Starr	PERSON
to	O
make	O
it	O
a	O
tribute	O
song	O
to	O
Lennon	PERSON
.	O
"	O
All	O
Those	O
Years	O
Ago	O
"	O
received	O
substantial	O
radio	O
airplay	O
,	O
reaching	O
number	O
two	O
on	O
the	O
US	LOCATION
charts	O
.	O
Both	O
singles	O
came	O
from	O
the	O
album	O
Somewhere	O
in	O
England	O
,	O
released	O
in	O
1981	O
.	O
Originally	O
slated	O
for	O
release	O
in	O
late	O
1980	O
,	O
Warner	O
Bros.	O
rejected	O
the	O
album	O
,	O
ordering	O
Harrison	PERSON
to	O
replace	O
several	O
tracks	O
,	O
and	O
to	O
change	O
the	O
album	O
cover	O
as	O
well	O
.	O
The	O
original	O
album	O
cover	O
that	O
Harrison	PERSON
wanted	O
was	O
used	O
in	O
the	O
2004	O
reissue	O
of	O
the	O
album	O
.	O
Aside	O
from	O
a	O
song	O
on	O
the	O
Porky	O
's	O
Revenge	O
soundtrack	O
in	O
1984	O
,	O
Harrison	PERSON
released	O
no	O
new	O
records	O
for	O
five	O
years	O
after	O
1982	O
's	O
Gone	O
Troppo	O
received	O
apparent	O
indifference	O
.	O
He	O
only	O
agreed	O
to	O
appear	O
because	O
he	O
was	O
a	O
close	O
admirer	O
of	O
Perkins	PERSON
.	O
George	PERSON
Harrison	PERSON
played	O
slide	O
guitar	O
in	O
this	O
band	O
as	O
a	O
favour	O
since	O
Wright	PERSON
had	O
played	O
piano	O
on	O
Harrison	PERSON
's	O
album	O
All	O
Things	O
Must	O
Pass	O
.	O
Harrison	PERSON
had	O
hired	O
filmmaker	O
David	PERSON
Acomba	PERSON
to	O
accompany	O
the	O
tour	O
and	O
gather	O
footage	O
for	O
a	O
documentary	O
.	O
In	O
1986	O
,	O
Harrison	PERSON
made	O
a	O
surprise	O
performance	O
at	O
the	O
Birmingham	O
Heart	O
Beat	O
Charity	O
Concert	O
1986	O
a	O
concert	O
event	O
to	O
raise	O
money	O
for	O
the	O
Birmingham	O
Children	O
's	O
Hospital	O
.	O
Harrison	PERSON
played	O
and	O
sang	O
the	O
finale	O
"	O
Johnny	PERSON
B.	PERSON
Goode	PERSON
"	O
along	O
with	O
Robert	PERSON
Plant	PERSON
,	O
The	O
Moody	O
Blues	O
,	O
and	O
Electric	O
Light	O
Orchestra	O
,	O
among	O
others	O
.	O
The	O
following	O
year	O
,	O
Harrison	PERSON
appeared	O
at	O
The	O
Prince	O
's	O
Trust	O
concert	O
in	O
Wembley	O
Arena	O
,	O
performing	O
"	O
While	O
My	O
Guitar	O
Gently	O
Weeps	O
"	O
and	O
"	O
Here	O
Comes	O
the	O
Sun	O
"	O
with	O
Ringo	PERSON
Starr	PERSON
,	O
Eric	PERSON
Clapton	PERSON
,	O
and	O
others	O
.	O
The	O
Live	O
in	O
Japan	LOCATION
recording	O
came	O
from	O
these	O
shows	O
.	O
This	O
was	O
released	O
on	O
the	O
album	O
The	O
30th	O
Anniversary	O
Concert	O
Celebration	O
in	O
August	O
1993	O
.	O
The	O
same	O
year	O
also	O
saw	O
the	O
release	O
of	O
Best	O
of	O
Dark	O
Horse	O
1976	O
--	O
1989	O
,	O
a	O
compilation	O
drawn	O
from	O
his	O
later	O
solo	O
work	O
.	O
Unlike	O
his	O
previous	O
greatest	O
hits	O
package	O
,	O
Harrison	PERSON
made	O
sure	O
to	O
oversee	O
this	O
compilation	O
.	O
In	O
1989	O
Harrison	PERSON
played	O
slide	O
guitar	O
on	O
the	O
"	O
Leave	O
a	O
Light	O
On	O
"	O
song	O
from	O
Belinda	PERSON
Carlisle	PERSON
's	PERSON
third	O
album	O
"	O
Runaway	O
Horses	O
"	O
.	O
John	PERSON
Fugelsang	PERSON
,	O
then	O
of	O
VH1	O
,	O
conducted	O
the	O
interview	O
,	O
and	O
at	O
one	O
point	O
an	O
acoustic	O
guitar	O
was	O
produced	O
and	O
handed	O
to	O
Harrison	PERSON
.	O
That	O
same	O
year	O
he	O
attended	O
the	O
public	O
memorial	O
service	O
for	O
Linda	PERSON
McCartney	PERSON
.	O
Also	O
that	O
same	O
year	O
,	O
he	O
appeared	O
on	O
Ringo	O
Starr	O
's	O
Vertical	O
Man	O
,	O
where	O
he	O
played	O
both	O
electric	O
and	O
slide	O
guitars	O
on	O
two	O
tracks	O
.	O
In	O
2001	O
,	O
Harrison	PERSON
performed	O
as	O
a	O
guest	O
musician	O
on	O
the	O
Electric	O
Light	O
Orchestra	O
album	O
Zoom	O
.	O
The	O
latter	O
song	O
ended	O
up	O
as	O
Harrison	PERSON
's	O
final	O
recording	O
session	O
,	O
on	O
2	O
October	O
.	O
Harrison	PERSON
's	O
final	O
album	O
,	O
Brainwashed	O
,	O
was	O
completed	O
by	O
Dhani	PERSON
Harrison	PERSON
and	O
Jeff	PERSON
Lynne	PERSON
and	O
released	O
on	O
18	O
November	O
2002	O
.	O
This	O
had	O
to	O
be	O
completed	O
within	O
two	O
weeks	O
,	O
as	O
Dylan	PERSON
was	O
scheduled	O
to	O
start	O
a	O
tour	O
.	O
The	O
album	O
,	O
Traveling	O
Wilburys	O
Vol.	O
1	O
,	O
was	O
released	O
in	O
October	O
1988	O
and	O
recorded	O
under	O
pseudonyms	O
as	O
half-brothers	O
.	O
Harrison	PERSON
's	O
pseudonym	O
on	O
the	O
first	O
album	O
was	O
"	O
Nelson	PERSON
Wilbury	PERSON
"	O
;	O
he	O
would	O
use	O
the	O
name	O
"	O
Spike	PERSON
Wilbury	PERSON
"	O
for	O
the	O
Traveling	O
Wilburys	O
'	O
second	O
album	O
.	O
After	O
the	O
death	O
of	O
Roy	PERSON
Orbison	PERSON
in	O
late	O
1988	O
the	O
group	O
recorded	O
as	O
a	O
four-piece	O
.	O
It	O
was	O
created	O
to	O
help	O
out	O
his	O
Monty	O
Python	O
friends	O
by	O
raising	O
£	O
2	O
million	O
to	O
finish	O
their	O
film	O
Life	O
of	O
Brian	O
after	O
EMI	O
Films	O
,	O
the	O
original	O
financiers	O
,	O
pulled	O
out	O
due	O
to	O
the	O
film	O
's	O
satirical	O
content	O
.	O
Harrison	PERSON
took	O
the	O
name	O
from	O
some	O
handmade	O
paper	O
he	O
had	O
been	O
given	O
on	O
a	O
mill	O
visit	O
.	O
Harrison	PERSON
was	O
involved	O
in	O
some	O
creative	O
decisions	O
,	O
approving	O
projects	O
such	O
as	O
Withnail	O
and	O
I	O
and	O
visiting	O
sets	O
as	O
executive	O
producer	O
to	O
sort	O
out	O
creative	O
problems	O
.	O
On	O
the	O
whole	O
,	O
though	O
,	O
Harrison	PERSON
preferred	O
to	O
stay	O
out	O
of	O
the	O
way	O
:	O
"	O
I	O
've	O
been	O
the	O
person	O
who	O
's	O
said	O
of	O
the	O
people	O
with	O
the	O
money	O
,	O
'	O
What	O
do	O
they	O
know	O
?	O
'	O
This	O
eventually	O
resulted	O
in	O
disagreements	O
and	O
lawsuits	O
between	O
the	O
pair	O
as	O
Handmade	O
Films	O
encountered	O
reversals	O
,	O
and	O
Harrison	PERSON
sold	O
the	O
company	O
in	O
1994	O
.	O
That	O
same	O
year	O
,	O
he	O
and	O
fellow	O
Beatle	PERSON
John	PERSON
Lennon	PERSON
met	O
A.C.	PERSON
Bhaktivedanta	PERSON
Swami	PERSON
Prabhupada	PERSON
,	O
founder	O
--	O
acharya	O
of	O
the	O
International	O
Society	O
for	O
Krishna	O
Consciousness	O
(	O
ISKCON	O
)	O
.	O
Soon	O
after	O
,	O
Harrison	PERSON
embraced	O
the	O
Hare	O
Krishna	O
tradition	O
(	O
particularly	O
japa-yoga	O
chanting	O
with	O
beads	O
)	O
,	O
became	O
a	O
lifelong	O
devotee	O
,	O
being	O
associated	O
with	O
it	O
until	O
his	O
death	O
.	O
Harrison	PERSON
was	O
a	O
vegetarian	O
from	O
1968	O
until	O
his	O
death	O
.	O
They	O
had	O
met	O
during	O
the	O
filming	O
for	O
A	O
Hard	O
Day	O
's	O
Night	O
,	O
in	O
which	O
the	O
19-year-old	O
Boyd	PERSON
was	O
cast	O
as	O
a	O
schoolgirl	O
fan	O
.	O
After	O
Harrison	PERSON
and	O
Boyd	PERSON
split	O
up	O
in	O
1974	O
,	O
she	O
moved	O
in	O
with	O
Eric	PERSON
Clapton	PERSON
and	O
they	O
subsequently	O
married	O
.	O
Harrison	PERSON
married	O
for	O
a	O
second	O
time	O
,	O
to	O
Dark	O
Horse	O
Records	O
secretary	O
Olivia	PERSON
Trinidad	PERSON
Arias	PERSON
on	O
2	O
September	O
1978	O
.	O
They	O
had	O
met	O
at	O
the	O
Dark	O
Horse	O
offices	O
in	O
Los	LOCATION
Angeles	LOCATION
in	O
1974	O
.	O
They	O
had	O
one	O
son	O
,	O
Dhani	PERSON
Harrison	PERSON
.	O
Harrison	PERSON
formed	O
a	O
close	O
friendship	O
with	O
Clapton	PERSON
in	O
the	O
late	O
1960s	O
,	O
and	O
they	O
co-wrote	O
the	O
song	O
"	O
Badge	O
"	O
,	O
which	O
was	O
released	O
on	O
Cream	O
's	O
Goodbye	O
album	O
in	O
1969	O
.	O
Harrison	PERSON
also	O
played	O
rhythm	O
guitar	O
on	O
the	O
song	O
.	O
Through	O
Clapton	O
,	O
Harrison	O
met	O
Delaney	PERSON
Bramlett	PERSON
,	O
who	O
introduced	O
Harrison	PERSON
to	O
slide	O
guitar	O
.	O
They	O
remained	O
close	O
friends	O
after	O
Pattie	PERSON
Boyd	PERSON
split	O
from	O
Harrison	PERSON
and	O
married	O
Clapton	PERSON
,	O
referring	O
to	O
each	O
other	O
as	O
"	O
husbands-in-law	O
"	O
.	O
Through	O
his	O
appreciation	O
of	O
Monty	PERSON
Python	PERSON
,	O
he	O
met	O
Eric	PERSON
Idle	PERSON
.	O
Idle	O
also	O
performed	O
at	O
the	O
Concert	O
for	O
George	PERSON
,	O
held	O
to	O
commemorate	O
Harrison	PERSON
.	O
Several	O
Harrison	PERSON
videos	O
were	O
also	O
filmed	O
on	O
the	O
grounds	O
,	O
including	O
"	O
Crackerbox	O
Palace	O
"	O
;	O
in	O
addition	O
,	O
the	O
grounds	O
served	O
as	O
the	O
background	O
for	O
the	O
cover	O
of	O
All	O
Things	O
Must	O
Pass	O
.	O
He	O
employed	O
a	O
staff	O
of	O
ten	O
workers	O
to	O
maintain	O
the	O
36-acre	O
garden	O
,	O
and	O
both	O
of	O
his	O
older	O
brothers	O
worked	O
on	O
Friar	O
Park	O
as	O
well	O
.	O
Harrison	PERSON
took	O
great	O
solace	O
working	O
in	O
the	O
garden	O
and	O
grew	O
to	O
consider	O
himself	O
more	O
a	O
gardener	O
than	O
a	O
musician	O
;	O
his	O
autobiography	O
is	O
dedicated	O
"	O
to	O
gardeners	O
everywhere	O
"	O
.	O
In	O
late	O
1999	O
,	O
Harrison	PERSON
survived	O
a	O
knife	O
attack	O
by	O
an	O
intruder	O
in	O
his	O
home	O
.	O
Abram	PERSON
attacked	O
Harrison	PERSON
with	O
a	O
seven-inch	O
kitchen	O
knife	O
,	O
inflicting	O
seven	O
stab	O
wounds	O
,	O
puncturing	O
a	O
lung	O
and	O
causing	O
head	O
injuries	O
before	O
Olivia	PERSON
Harrison	PERSON
incapacitated	O
the	O
assailant	O
by	O
striking	O
him	O
repeatedly	O
with	O
a	O
fireplace	O
poker	O
.	O
35-year-old	O
Abram	PERSON
,	O
who	O
believed	O
he	O
was	O
possessed	O
by	O
Harrison	PERSON
and	O
was	O
on	O
a	O
"	O
mission	O
from	O
God	O
"	O
to	O
kill	O
him	O
,	O
was	O
later	O
acquitted	O
of	O
attempted	O
murder	O
on	O
grounds	O
of	O
insanity	O
,	O
but	O
was	O
detained	O
for	O
treatment	O
in	O
a	O
secure	O
hospital	O
.	O
Since	O
John	PERSON
Lennon	PERSON
's	PERSON
assassination	O
in	O
1980	O
Harrison	PERSON
had	O
rarely	O
made	O
public	O
appearances	O
,	O
except	O
at	O
tightly	O
controlled	O
settings	O
such	O
as	O
Rock	O
and	O
Roll	O
Hall	O
of	O
Fame	O
inductions	O
.	O
Harrison	PERSON
developed	O
throat	O
cancer	O
,	O
which	O
was	O
discovered	O
in	O
1997	O
after	O
a	O
lump	O
on	O
his	O
neck	O
was	O
analysed	O
.	O
Early	O
in	O
May	O
2001	O
,	O
it	O
was	O
revealed	O
that	O
he	O
had	O
undergone	O
an	O
operation	O
at	O
the	O
Mayo	O
Clinic	O
to	O
remove	O
a	O
cancerous	O
growth	O
from	O
one	O
of	O
his	O
lungs	O
.	O
Despite	O
the	O
treatments	O
and	O
operations	O
,	O
Harrison	PERSON
died	O
on	O
29	O
November	O
2001	O
at	O
a	O
Hollywood	LOCATION
Hills	LOCATION
mansion	O
that	O
was	O
once	O
leased	O
by	O
McCartney	PERSON
and	O
was	O
previously	O
owned	O
by	O
Courtney	O
Love	O
.	O
In	O
2002	O
,	O
on	O
the	O
first	O
anniversary	O
of	O
Harrison	PERSON
's	O
death	O
,	O
the	O
Concert	O
for	O
George	PERSON
was	O
held	O
at	O
the	O
Royal	O
Albert	O
Hall	O
;	O
it	O
was	O
organised	O
by	O
Eric	PERSON
Clapton	PERSON
and	O
included	O
performances	O
by	O
many	O
of	O
Harrison	PERSON
's	O
musical	O
friends	O
,	O
including	O
Paul	PERSON
McCartney	PERSON
and	O
Ringo	PERSON
Starr	PERSON
.	O
He	O
was	O
inducted	O
into	O
the	O
Madison	O
Square	O
Garden	O
Walk	O
of	O
Fame	O
on	O
1	O
August	O
2006	O
for	O
the	O
Concert	O
for	O
Bangladesh	LOCATION
.	O
In	O
September	O
2007	O
,	O
Variety	O
announced	O
that	O
Martin	PERSON
Scorsese	PERSON
would	O
make	O
a	O
film	O
about	O
Harrison	PERSON
's	O
life	O
.	O
Musicians	O
Tom	PERSON
Petty	PERSON
,	O
Jeff	PERSON
Lynne	PERSON
and	O
Paul	PERSON
McCartney	PERSON
were	O
among	O
those	O
in	O
attendance	O
when	O
the	O
star	O
was	O
unveiled	O
.	O
