In	O
most	O
cases	O
,	O
the	O
discipline	O
is	O
self-governed	O
by	O
the	O
entities	O
which	O
require	O
the	O
programming	O
,	O
and	O
sometimes	O
very	O
strict	O
environments	O
are	O
defined	O
(	O
e.g.	O
United	O
States	O
Air	O
Force	O
use	O
of	O
AdaCore	O
and	O
security	O
clearance	O
)	O
.	O
Charles	PERSON
Babbage	PERSON
adopted	O
the	O
use	O
of	O
punched	O
cards	O
around	O
1830	O
to	O
control	O
his	O
Analytical	O
Engine	O
.	O
In	O
the	O
late	O
1880s	O
,	O
Herman	PERSON
Hollerith	PERSON
invented	O
the	O
recording	O
of	O
data	O
on	O
a	O
medium	O
that	O
could	O
then	O
be	O
read	O
by	O
a	O
machine	O
.	O
In	O
1896	O
he	O
founded	O
the	O
Tabulating	O
Machine	O
Company	O
(	O
which	O
later	O
became	O
the	O
core	O
of	O
IBM	O
)	O
.	O
In	O
1954	O
,	O
FORTRAN	O
was	O
invented	O
;	O
it	O
was	O
the	O
first	O
high	O
level	O
programming	O
language	O
to	O
have	O
a	O
functional	O
implementation	O
,	O
as	O
opposed	O
to	O
just	O
a	O
design	O
on	O
paper	O
.	O
The	O
program	O
text	O
,	O
or	O
source	O
,	O
is	O
converted	O
into	O
machine	O
instructions	O
using	O
a	O
special	O
program	O
called	O
a	O
compiler	O
,	O
which	O
translates	O
the	O
FORTRAN	O
program	O
into	O
machine	O
language	O
.	O
Many	O
other	O
languages	O
were	O
developed	O
,	O
including	O
some	O
for	O
commercial	O
programming	O
,	O
such	O
as	O
COBOL	O
.	O
Methods	O
of	O
measuring	O
programming	O
language	O
popularity	O
include	O
:	O
counting	O
the	O
number	O
of	O
job	O
advertisements	O
that	O
mention	O
the	O
language	O
,	O
the	O
number	O
of	O
books	O
teaching	O
the	O
language	O
that	O
are	O
sold	O
(	O
this	O
overestimates	O
the	O
importance	O
of	O
newer	O
languages	O
)	O
,	O
and	O
estimates	O
of	O
the	O
number	O
of	O
existing	O
lines	O
of	O
code	O
written	O
in	O
the	O
language	O
(	O
this	O
underestimates	O
the	O
number	O
of	O
users	O
of	O
business	O
languages	O
such	O
as	O
COBOL	O
)	O
.	O
