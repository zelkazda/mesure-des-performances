In	O
Europe	LOCATION
,	O
the	O
4-to-1	O
pattern	O
was	O
completely	O
dominant	O
.	O
Historically	O
,	O
in	O
Europe	LOCATION
,	O
from	O
the	O
pre-Roman	O
period	O
on	O
,	O
the	O
rings	O
composing	O
a	O
piece	O
of	O
mail	O
would	O
be	O
riveted	O
closed	O
to	O
reduce	O
the	O
chance	O
of	O
the	O
rings	O
splitting	O
open	O
when	O
subjected	O
to	O
a	O
thrusting	O
attack	O
or	O
a	O
hit	O
by	O
an	O
arrow	O
.	O
During	O
World	O
War	O
I	O
,	O
mail	O
was	O
evaluated	O
as	O
a	O
material	O
for	O
bullet	O
proof	O
vests	O
,	O
but	O
results	O
were	O
unsatisfactory	O
as	O
the	O
rings	O
would	O
fragment	O
and	O
further	O
aggravate	O
the	O
damage	O
.	O
In	O
some	O
films	O
,	O
knitted	O
string	O
spray-painted	O
with	O
a	O
metallic	O
paint	O
is	O
used	O
instead	O
of	O
actual	O
mail	O
in	O
order	O
to	O
cut	O
down	O
on	O
cost	O
(	O
a	O
notable	O
example	O
being	O
Monty	PERSON
Python	PERSON
and	O
the	O
Holy	O
Grail	O
,	O
which	O
was	O
filmed	O
on	O
a	O
very	O
small	O
budget	O
)	O
.	O
