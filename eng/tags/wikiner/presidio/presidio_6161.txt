It	O
used	O
the	O
same	O
basic	O
instruction	O
set	O
as	O
the	O
8008	O
(	O
developed	O
by	O
Computer	O
Terminal	O
Corporation	O
)	O
and	O
was	O
source	O
code	O
compatible	O
with	O
its	O
predecessor	O
,	O
but	O
added	O
some	O
handy	O
16-bit	O
operations	O
to	O
the	O
instruction	O
set	O
as	O
well	O
.	O
Space	O
Invaders	O
was	O
perhaps	O
the	O
most	O
popular	O
such	O
title	O
.	O
Shortly	O
after	O
the	O
launch	O
of	O
the	O
8080	O
,	O
the	O
Motorola	O
6800	O
competing	O
design	O
was	O
introduced	O
,	O
and	O
after	O
that	O
,	O
the	O
MOS	O
Technology	O
6502	O
variation	O
of	O
the	O
6800	O
.	O
Later	O
NEC	O
made	O
an	O
NEC	O
V20	O
processor	O
(	O
an	O
8088	O
clone	O
)	O
which	O
supported	O
8080	O
emulation	O
mode	O
.	O
According	O
to	O
some	O
sources	O
,	O
the	O
Soviet	O
analog	O
had	O
two	O
undocumented	O
instructions	O
,	O
specific	O
to	O
itself	O
;	O
however	O
,	O
these	O
were	O
not	O
widely	O
known	O
.	O
When	O
the	O
8080	O
was	O
introduced	O
,	O
computer	O
systems	O
were	O
usually	O
created	O
by	O
computer	O
manufacturers	O
such	O
as	O
Digital	O
Equipment	O
Corporation	O
,	O
Hewlett	O
Packard	O
,	O
or	O
IBM	O
.	O
Hewlett	O
Packard	O
developed	O
the	O
HP	O
2640	O
series	O
of	O
smart	O
terminals	O
around	O
the	O
8080	O
.	O
The	O
HP	O
2647	O
was	O
a	O
terminal	O
which	O
ran	O
BASIC	O
on	O
the	O
8080	O
.	O
Microsoft	O
would	O
create	O
the	O
first	O
popular	O
programming	O
language	O
for	O
the	O
8080	O
,	O
and	O
would	O
later	O
acquire	O
DOS	O
for	O
the	O
IBM-PC	O
.	O
The	O
size	O
of	O
chips	O
has	O
grown	O
so	O
that	O
the	O
size	O
and	O
power	O
of	O
large	O
x86	O
chips	O
is	O
not	O
much	O
different	O
from	O
high	O
end	O
architecture	O
chips	O
,	O
and	O
a	O
common	O
strategy	O
to	O
produce	O
a	O
very	O
large	O
computer	O
is	O
to	O
network	O
many	O
x86	O
processors	O
.	O
Though	O
x86	O
may	O
not	O
be	O
the	O
most	O
elegant	O
,	O
or	O
theoretically	O
most	O
efficient	O
design	O
,	O
the	O
sheer	O
market	O
force	O
of	O
so	O
many	O
dollars	O
going	O
into	O
refining	O
a	O
design	O
has	O
made	O
the	O
x86	O
family	O
today	O
,	O
and	O
will	O
remain	O
for	O
some	O
time	O
,	O
the	O
dominant	O
processor	O
architecture	O
,	O
even	O
bypassing	O
Intel	O
's	O
attempts	O
to	O
replace	O
it	O
with	O
incompatible	O
architectures	O
such	O
as	O
the	O
iAPX	O
432	O
and	O
Itanium	O
.	O
Federico	PERSON
Faggin	PERSON
,	O
the	O
originator	O
of	O
the	O
8080	O
architecture	O
in	O
early	O
1972	O
,	O
proposed	O
it	O
to	O
Intel	O
's	O
management	O
and	O
pushed	O
for	O
its	O
implementation	O
.	O
Stanley	PERSON
Mazor	PERSON
contributed	O
a	O
couple	O
of	O
instructions	O
to	O
the	O
instruction	O
set	O
.	O
