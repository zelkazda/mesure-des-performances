Lear	O
suffered	O
from	O
health	O
problems	O
.	O
Lear	O
felt	O
lifelong	O
guilt	O
and	O
shame	O
for	O
his	O
epileptic	O
condition	O
.	O
How	O
Lear	O
was	O
able	O
to	O
anticipate	O
them	O
is	O
not	O
known	O
,	O
but	O
many	O
people	O
with	O
epilepsy	O
report	O
a	O
ringing	O
in	O
their	O
ears	O
or	O
an	O
"	O
aura	O
"	O
before	O
the	O
onset	O
of	O
a	O
seizure	O
.	O
In	O
Lear	PERSON
's	O
time	O
epilepsy	O
was	O
believed	O
to	O
be	O
associated	O
with	O
demonic	O
possession	O
,	O
which	O
contributed	O
to	O
his	O
feelings	O
of	O
guilt	O
and	O
loneliness	O
.	O
When	O
Lear	PERSON
was	O
about	O
seven	O
he	O
began	O
to	O
show	O
signs	O
of	O
depression	O
,	O
possibly	O
due	O
to	O
the	O
constant	O
instability	O
of	O
his	O
childhood	O
.	O
After	O
a	O
long	O
decline	O
in	O
his	O
health	O
,	O
Lear	PERSON
died	O
at	O
his	O
villa	O
in	O
1888	O
,	O
of	O
the	O
heart	O
disease	O
from	O
which	O
he	O
had	O
suffered	O
since	O
at	O
least	O
1870	O
.	O
His	O
paintings	O
were	O
well	O
received	O
and	O
he	O
was	O
favourably	O
compared	O
with	O
Audubon	O
.	O
He	O
had	O
a	O
lifelong	O
ambition	O
to	O
illustrate	O
Tennyson	PERSON
's	O
poems	O
;	O
near	O
the	O
end	O
of	O
his	O
life	O
a	O
volume	O
with	O
a	O
small	O
number	O
of	O
illustrations	O
was	O
published	O
,	O
but	O
his	O
vision	O
for	O
the	O
work	O
was	O
never	O
realized	O
.	O
Lear	PERSON
's	O
nonsense	O
works	O
are	O
distinguished	O
by	O
a	O
facility	O
of	O
verbal	O
invention	O
and	O
a	O
poet	O
's	O
delight	O
in	O
the	O
sounds	O
of	O
words	O
,	O
both	O
real	O
and	O
imaginary	O
.	O
Though	O
famous	O
for	O
his	O
neologisms	O
,	O
Lear	PERSON
employed	O
a	O
number	O
of	O
other	O
devices	O
in	O
his	O
works	O
in	O
order	O
to	O
defy	O
reader	O
expectations	O
.	O
Limericks	O
are	O
invariably	O
typeset	O
as	O
four	O
plus	O
one	O
lines	O
today	O
,	O
but	O
Lear	PERSON
's	O
limericks	O
were	O
published	O
in	O
a	O
variety	O
of	O
formats	O
.	O
It	O
appears	O
that	O
Lear	PERSON
wrote	O
them	O
in	O
manuscript	O
in	O
as	O
many	O
lines	O
as	O
there	O
was	O
room	O
for	O
beneath	O
the	O
picture	O
.	O
In	O
Lear	PERSON
's	O
limericks	O
the	O
first	O
and	O
last	O
lines	O
usually	O
end	O
with	O
the	O
same	O
word	O
rather	O
than	O
rhyming	O
.	O
An	O
example	O
of	O
a	O
typical	O
Lear	PERSON
limerick	PERSON
:	O
