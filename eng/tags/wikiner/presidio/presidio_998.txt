The	O
early	O
life	O
of	O
the	O
sculptor	O
Andreas	PERSON
Schlüter	PERSON
is	O
obscured	O
as	O
at	O
least	O
three	O
different	O
persons	O
of	O
that	O
names	O
are	O
documented	O
.	O
He	O
later	O
created	O
statues	O
for	O
King	O
John	PERSON
III	PERSON
Sobieski	PERSON
's	PERSON
Wilanów	O
Palace	O
in	O
Warsaw	LOCATION
and	O
sepulchral	O
sculptures	O
in	O
Żółkiew	PERSON
(	O
Zhovkva	PERSON
)	O
.	O
In	O
1689	O
,	O
he	O
moved	O
to	O
Warsaw	LOCATION
and	O
made	O
the	O
pediment	O
reliefs	O
and	O
sculptural	O
work	O
of	O
Krasiński	O
Palace	O
.	O
While	O
the	O
more	O
visible	O
reliefs	O
on	O
the	O
outside	O
had	O
to	O
praise	O
fighting	O
,	O
the	O
statues	O
of	O
dying	O
warriors	O
in	O
the	O
interior	O
denounced	O
war	O
and	O
gave	O
an	O
indication	O
of	O
his	O
pacifist	O
religious	O
beliefs	O
(	O
he	O
is	O
said	O
to	O
have	O
been	O
a	O
Mennonite	O
)	O
.	O
The	O
Berlin	O
City	O
Palace	O
,	O
and	O
many	O
of	O
his	O
works	O
,	O
were	O
partially	O
destroyed	O
by	O
bombing	O
in	O
World	O
War	O
II	O
and	O
by	O
the	O
subsequent	O
Communist	O
regime	O
.	O
A	O
similar	O
fate	O
probably	O
befell	O
the	O
Amber	O
Room	O
,	O
made	O
between	O
1701	O
and	O
1709	O
,	O
Schlüter	PERSON
's	O
most	O
famous	O
work	O
of	O
architecture	O
.	O
In	O
1713	O
Schlüter	PERSON
's	O
fame	O
brought	O
him	O
to	O
work	O
for	O
Tsar	O
Peter	O
the	O
Great	O
in	O
Saint	LOCATION
Petersburg	LOCATION
,	O
where	O
he	O
died	O
of	O
an	O
illness	O
after	O
creating	O
several	O
designs	O
.	O
