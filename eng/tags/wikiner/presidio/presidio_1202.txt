Berlin	LOCATION
)	O
is	O
the	O
capital	O
city	O
and	O
one	O
of	O
16	O
states	O
of	O
Germany	LOCATION
.	O
With	O
a	O
population	O
of	O
3.4	O
million	O
people	O
,	O
Berlin	LOCATION
is	O
Germany	LOCATION
's	O
largest	O
city	O
.	O
It	O
is	O
the	O
second	O
most	O
populous	O
city	O
proper	O
and	O
the	O
eighth	O
most	O
populous	O
urban	O
area	O
in	O
the	O
European	O
Union	O
.	O
During	O
the	O
1920s	O
,	O
Berlin	LOCATION
was	O
the	O
third	O
largest	O
municipality	O
in	O
the	O
world	O
.	O
After	O
World	O
War	O
II	O
,	O
the	O
city	O
was	O
divided	O
;	O
East	LOCATION
Berlin	LOCATION
became	O
the	O
capital	O
of	O
East	LOCATION
Germany	LOCATION
while	O
West	LOCATION
Berlin	LOCATION
became	O
a	O
de	O
facto	O
West	O
German	O
exclave	O
,	O
surrounded	O
by	O
the	O
Berlin	O
Wall	O
(	O
1961	O
--	O
1989	O
)	O
.	O
Following	O
German	O
reunification	O
in	O
1990	O
,	O
the	O
city	O
regained	O
its	O
status	O
as	O
the	O
capital	O
of	O
all	O
Germany	LOCATION
hosting	O
147	O
foreign	O
embassies	O
.	O
Berlin	LOCATION
is	O
a	O
world	O
city	O
of	O
culture	O
,	O
politics	O
,	O
media	O
,	O
and	O
science	O
.	O
Berlin	LOCATION
serves	O
as	O
a	O
continental	O
hub	O
for	O
air	O
and	O
rail	O
transport	O
,	O
and	O
is	O
one	O
of	O
the	O
most	O
visited	O
tourist	O
destinations	O
in	O
the	O
EU	O
.	O
Berlin	LOCATION
has	O
evolved	O
into	O
a	O
global	O
focal	O
point	O
for	O
young	O
individuals	O
and	O
artists	O
attracted	O
by	O
a	O
liberal	O
lifestyle	O
and	O
modern	O
zeitgeist	O
.	O
The	O
earliest	O
evidence	O
of	O
settlements	O
in	O
today	O
's	O
Berlin	LOCATION
central	O
areas	O
is	O
a	O
wooden	O
beam	O
dated	O
from	O
approximately	O
1192	O
.	O
The	O
first	O
written	O
mention	O
of	O
towns	O
in	O
the	O
area	O
of	O
present-day	O
Berlin	LOCATION
dates	O
from	O
the	O
late	O
12th	O
century	O
.	O
The	O
settlement	O
of	O
Spandau	LOCATION
is	O
first	O
mentioned	O
in	O
1197	O
,	O
and	O
Köpenick	PERSON
in	O
1209	O
,	O
though	O
these	O
areas	O
did	O
not	O
join	O
Berlin	LOCATION
until	O
1920	O
.	O
The	O
central	O
part	O
of	O
Berlin	LOCATION
can	O
be	O
traced	O
back	O
to	O
two	O
towns	O
.	O
Cölln	PERSON
on	O
the	O
Fischerinsel	O
is	O
first	O
mentioned	O
in	O
a	O
1237	O
document	O
,	O
and	O
Berlin	LOCATION
,	O
across	O
the	O
Spree	LOCATION
in	O
what	O
is	O
now	O
called	O
the	O
Nikolaiviertel	O
,	O
is	O
referenced	O
in	O
a	O
document	O
from	O
1244	O
.	O
Over	O
time	O
,	O
the	O
twin	O
cities	O
came	O
to	O
be	O
known	O
simply	O
as	O
Berlin	LOCATION
.	O
In	O
1435	O
,	O
Frederick	PERSON
I	PERSON
became	O
the	O
elector	O
of	O
the	O
Margraviate	O
of	O
Brandenburg	LOCATION
,	O
which	O
he	O
ruled	O
until	O
1440	O
.	O
In	O
1539	O
,	O
the	O
electors	O
and	O
the	O
city	O
officially	O
became	O
Lutheran	O
.	O
The	O
Thirty	O
Years	O
'	O
War	O
between	O
1618	O
and	O
1648	O
had	O
devastating	O
consequences	O
for	O
Berlin	LOCATION
.	O
More	O
than	O
15,000	O
Huguenots	O
went	O
to	O
Brandenburg	LOCATION
,	O
of	O
whom	O
6,000	O
settled	O
in	O
Berlin	LOCATION
.	O
Many	O
other	O
immigrants	O
came	O
from	O
Bohemia	LOCATION
,	O
Poland	LOCATION
,	O
and	O
Salzburg	LOCATION
.	O
With	O
the	O
coronation	O
of	O
Frederick	PERSON
I	O
in	O
1701	O
as	O
king	O
(	O
in	O
Königsberg	LOCATION
)	O
,	O
Berlin	LOCATION
became	O
the	O
new	O
capital	O
of	O
the	LOCATION
Kingdom	LOCATION
of	LOCATION
Prussia	LOCATION
(	O
instead	O
of	O
Königsberg	LOCATION
)	O
;	O
this	O
was	O
a	O
successful	O
attempt	O
to	O
centralize	O
the	O
capital	O
in	O
the	O
very	O
outspread	O
Prussian	LOCATION
Kingdom	LOCATION
,	O
and	O
it	O
was	O
the	O
first	O
time	O
the	O
city	O
began	O
to	O
grow	O
.	O
In	O
1740	O
Frederick	PERSON
II	PERSON
,	O
known	O
as	O
Frederick	PERSON
the	PERSON
Great	PERSON
(	O
1740	O
--	O
1786	O
)	O
,	O
came	O
to	O
power	O
.	O
Following	O
France	LOCATION
's	O
victory	O
in	O
the	O
War	O
of	O
the	O
Fourth	O
Coalition	O
,	O
Napoleon	PERSON
Bonaparte	PERSON
marched	O
into	O
Berlin	LOCATION
in	O
1806	O
,	O
but	O
granted	O
self-government	O
to	O
the	O
city	O
.	O
In	O
1815	O
the	O
city	O
became	O
part	O
of	O
the	O
new	O
Province	O
of	O
Brandenburg	LOCATION
.	O
Additional	O
suburbs	O
soon	O
developed	O
and	O
increased	O
the	O
area	O
and	O
population	O
of	O
Berlin	LOCATION
.	O
In	O
1861	O
,	O
outlying	O
suburbs	O
including	O
Wedding	O
,	O
Moabit	PERSON
,	O
and	O
several	O
others	O
were	O
incorporated	O
into	O
Berlin	LOCATION
.	O
On	O
1	O
April	O
1881	O
it	O
became	O
a	O
city	O
district	O
separate	O
from	O
Brandenburg	LOCATION
.	O
At	O
the	O
end	O
of	O
World	O
War	O
I	O
in	O
1918	O
,	O
the	LOCATION
Weimar	LOCATION
Republic	LOCATION
was	O
proclaimed	O
in	O
Berlin	LOCATION
.	O
In	O
1920	O
,	O
the	O
Greater	O
Berlin	O
Act	O
incorporated	O
dozens	O
of	O
suburban	O
cities	O
,	O
villages	O
,	O
and	O
estates	O
around	O
Berlin	LOCATION
into	O
an	O
expanded	O
city	O
.	O
After	O
this	O
expansion	O
,	O
Berlin	LOCATION
had	O
a	O
population	O
of	O
around	O
four	O
million	O
.	O
On	O
30	O
January	O
1933	O
,	O
Adolf	PERSON
Hitler	PERSON
and	O
the	O
Nazi	O
Party	O
came	O
to	O
power	O
.	O
After	O
the	O
Kristallnacht	PERSON
pogrom	O
in	O
1938	O
,	O
thousands	O
of	O
the	O
city	O
's	O
German	O
Jews	O
were	O
imprisoned	O
in	O
the	O
nearby	O
Sachsenhausen	LOCATION
concentration	O
camp	O
or	O
,	O
in	O
early	O
1943	O
,	O
were	O
shipped	O
to	O
death	O
camps	O
,	O
such	O
as	O
Auschwitz	O
.	O
Among	O
the	O
hundreds	O
of	O
thousands	O
who	O
died	O
during	O
the	O
Battle	O
for	O
Berlin	LOCATION
,	O
an	O
estimated	O
125,000	O
were	O
civilians	O
.	O
All	O
four	O
allies	O
retained	O
shared	O
responsibility	O
for	O
Berlin	LOCATION
.	O
The	O
West	O
German	O
government	O
,	O
meanwhile	O
,	O
established	O
itself	O
provisionally	O
in	O
Bonn	LOCATION
.	O
Berlin	LOCATION
was	O
completely	O
separated	O
.	O
In	O
1989	O
,	O
pressure	O
from	O
the	O
East	O
German	O
population	O
broke	O
free	O
across	O
the	O
Berlin	O
Wall	O
on	O
9	O
November	O
1989	O
,	O
which	O
was	O
subsequently	O
mostly	O
demolished	O
.	O
In	O
1999	O
,	O
the	O
German	O
parliament	O
and	O
government	O
began	O
their	O
work	O
in	O
Berlin	LOCATION
.	O
Berlin	LOCATION
is	O
located	O
in	O
eastern	O
Germany	LOCATION
,	O
about	O
70	O
kilometers	O
(	O
43	O
mi	O
)	O
west	O
of	O
the	O
border	O
with	O
Poland	LOCATION
in	O
an	O
area	O
with	O
marshy	O
terrain	O
.	O
The	O
Spree	LOCATION
follows	O
this	O
valley	O
now	O
.	O
In	O
Spandau	LOCATION
,	O
Berlin	LOCATION
's	O
westernmost	O
borough	O
,	O
the	O
Spree	LOCATION
meets	O
the	O
river	O
Havel	LOCATION
,	O
which	O
flows	O
from	O
north	O
to	O
south	O
through	O
western	O
Berlin	LOCATION
.	O
A	O
series	O
of	O
lakes	O
also	O
feeds	O
into	O
the	O
upper	O
Spree	LOCATION
,	O
which	O
flows	O
through	O
the	O
Großer	O
Müggelsee	O
in	O
eastern	O
Berlin	LOCATION
.	O
The	O
highest	O
elevations	O
in	O
Berlin	LOCATION
are	O
the	O
Teufelsberg	O
and	O
the	O
Müggelberge	O
.	O
The	O
Teufelsberg	O
is	O
in	O
fact	O
an	O
artificial	O
pile	O
of	O
rubble	O
from	O
the	O
ruins	O
of	O
World	O
War	O
II	O
.	O
Berlin	LOCATION
's	O
built-up	O
area	O
creates	O
a	O
microclimate	O
,	O
with	O
heat	O
stored	O
by	O
the	O
city	O
's	O
buildings	O
.	O
The	O
city	O
's	O
appearance	O
today	O
is	O
predominantly	O
shaped	O
by	O
the	O
key	O
role	O
it	O
played	O
in	O
Germany	LOCATION
's	O
history	O
in	O
the	O
twentieth	O
century	O
.	O
Berlin	LOCATION
was	O
devastated	O
by	O
bombing	O
raids	O
during	O
World	O
War	O
II	O
and	O
many	O
of	O
the	O
old	O
buildings	O
that	O
escaped	O
the	O
bombs	O
were	O
eradicated	O
in	O
the	O
1950s	O
and	O
1960s	O
in	O
both	O
West	LOCATION
and	O
East	LOCATION
.	O
Berlin	LOCATION
's	O
unique	O
recent	O
history	O
has	O
left	O
the	O
city	O
with	O
a	O
highly	O
eclectic	O
array	O
of	O
architecture	O
and	O
buildings	O
.	O
The	O
Fernsehturm	O
at	O
Alexanderplatz	O
in	O
Mitte	LOCATION
is	O
among	O
the	O
tallest	O
structures	O
in	O
the	O
European	O
Union	O
at	O
368	O
meters	O
(	O
1207	O
ft	O
)	O
.	O
Built	O
in	O
1969	O
,	O
it	O
is	O
visible	O
throughout	O
most	O
of	O
the	O
central	O
districts	O
of	O
Berlin	LOCATION
.	O
Adjacent	O
to	O
this	O
area	O
is	O
the	O
Rotes	O
Rathaus	PERSON
,	O
with	O
its	O
distinctive	O
red-brick	O
architecture	O
.	O
The	O
previously	O
built-up	O
part	O
in	O
front	O
of	O
it	O
is	O
the	O
Neptunbrunnen	O
,	O
a	O
fountain	O
featuring	O
a	O
mythological	O
scene	O
.	O
The	O
East	O
Side	O
Gallery	O
is	O
an	O
open-air	O
exhibition	O
of	O
art	O
painted	O
directly	O
on	O
the	O
last	O
existing	O
portions	O
of	O
the	O
Berlin	O
Wall	O
.	O
The	O
Brandenburg	O
Gate	O
is	O
an	O
iconic	O
landmark	O
of	O
Berlin	LOCATION
and	O
Germany	LOCATION
.	O
The	O
Reichstag	LOCATION
building	O
is	O
the	O
traditional	O
seat	O
of	O
the	O
German	O
Parliament	O
,	O
renovated	O
in	O
the	O
1950s	O
after	O
severe	O
World	O
War	O
II	O
damage	O
.	O
Like	O
many	O
other	O
buildings	O
,	O
it	O
suffered	O
extensive	O
damage	O
during	O
the	O
Second	O
World	O
War	O
.	O
St.	O
Hedwig	O
's	O
Cathedral	O
is	O
Berlin	LOCATION
's	O
Roman	O
Catholic	O
cathedral	O
.	O
Unter	PERSON
den	O
Linden	O
is	O
a	O
tree	O
lined	O
east-west	O
avenue	O
from	O
the	O
Brandenburg	O
Gate	O
to	O
the	O
site	O
of	O
the	O
former	O
Berliner	PERSON
Stadtschloss	PERSON
,	O
and	O
was	O
once	O
Berlin	LOCATION
's	O
premier	O
promenade	O
.	O
It	O
combines	O
twentieth	O
century	O
traditions	O
with	O
the	O
modern	O
architecture	O
of	O
today	O
's	O
Berlin	LOCATION
.	O
Potsdamer	O
Platz	O
is	O
an	O
entire	O
quarter	O
built	O
from	O
scratch	O
after	O
1995	O
after	O
the	O
Wall	O
came	O
down	O
.	O
The	O
Memorial	O
to	O
the	O
Murdered	O
Jews	O
of	O
Europe	LOCATION
,	O
a	O
Holocaust	LOCATION
memorial	O
,	O
is	O
situated	O
to	O
the	O
north	O
.	O
Its	O
name	O
commemorates	O
the	O
uprisings	O
in	O
East	LOCATION
Berlin	LOCATION
of	O
17	O
June	O
1953	O
.	O
This	O
monument	O
,	O
built	O
to	O
commemorate	O
Prussia	LOCATION
's	O
victories	O
,	O
was	O
relocated	O
1938	O
--	O
39	O
from	O
its	O
previous	O
position	O
in	O
front	O
of	O
the	O
Reichstag	LOCATION
.	O
The	O
Kurfürstendamm	LOCATION
is	O
home	O
to	O
some	O
of	O
Berlin	LOCATION
's	O
luxurious	O
stores	O
with	O
the	O
Kaiser	O
Wilhelm	O
Memorial	O
Church	O
at	O
its	O
eastern	O
end	O
on	O
Breitscheidplatz	O
.	O
The	O
church	O
was	O
destroyed	O
in	O
the	O
Second	O
World	O
War	O
and	O
left	O
in	O
ruins	O
.	O
The	O
Rathaus	O
Schöneberg	O
,	O
where	O
John	PERSON
F.	PERSON
Kennedy	PERSON
made	O
his	O
famous	O
"	O
Ich	PERSON
bin	PERSON
ein	PERSON
Berliner	PERSON
!	O
"	O
speech	O
,	O
is	O
situated	O
in	O
Tempelhof-Schöneberg	LOCATION
.	O
Schloss	PERSON
Charlottenburg	PERSON
,	O
which	O
was	O
burnt	O
out	O
in	O
the	O
Second	O
World	O
War	O
and	O
largely	O
destroyed	O
,	O
has	O
been	O
rebuilt	O
and	O
is	O
the	O
largest	O
surviving	O
historical	O
palace	O
in	O
Berlin	LOCATION
.	O
The	LOCATION
Funkturm	LOCATION
Berlin	LOCATION
is	O
a	O
150	O
m	O
(	O
490	O
ft	O
)	O
tall	O
lattice	O
radio	O
tower	O
at	O
the	O
fair	O
area	O
,	O
built	O
between	O
1924	O
and	O
1926	O
.	O
Since	O
German	O
reunification	O
on	O
3	O
October	O
1990	O
,	O
it	O
has	O
been	O
one	O
of	O
the	O
three	O
city	O
states	O
,	O
together	O
with	O
Hamburg	LOCATION
and	O
Bremen	LOCATION
,	O
among	O
the	O
present	O
sixteen	O
states	O
of	O
Germany	LOCATION
.	O
Though	O
most	O
of	O
the	O
ministries	O
are	O
seated	O
in	O
Berlin	LOCATION
,	O
some	O
of	O
them	O
,	O
as	O
well	O
as	O
some	O
minor	O
departments	O
,	O
are	O
seated	O
in	O
Bonn	LOCATION
,	O
the	O
former	O
capital	O
of	O
West	LOCATION
Germany	LOCATION
.	O
The	O
European	O
Union	O
invests	O
in	O
several	O
projects	O
within	O
the	O
city	O
of	O
Berlin	LOCATION
.	O
The	O
city	O
and	O
state	O
parliament	O
is	O
the	O
House	O
of	O
Representatives	O
(	O
Abgeordnetenhaus	O
)	O
,	O
which	O
currently	O
has	O
141	O
seats	O
.	O
Since	O
2001	O
this	O
office	O
has	O
been	O
held	O
by	O
Klaus	PERSON
Wowereit	PERSON
of	O
the	O
SPD	O
.	O
The	O
city	O
's	O
government	O
is	O
based	O
on	O
a	O
coalition	O
between	O
the	O
Social	O
Democratic	O
Party	O
and	O
The	O
Left	O
.	O
The	O
total	O
annual	O
state	O
budget	O
of	O
Berlin	LOCATION
in	O
2007	O
exceeded	O
€	O
20.5	O
(	O
$	O
28.7	O
)	O
billion	O
including	O
a	O
budget	O
surplus	O
of	O
€	O
80	O
(	O
$	O
112	O
)	O
million	O
.	O
At	O
present	O
the	O
city	O
of	O
Berlin	LOCATION
consists	O
of	O
95	O
such	O
localities	O
.	O
The	O
localities	O
have	O
no	O
government	O
bodies	O
of	O
their	O
own	O
,	O
even	O
though	O
most	O
of	O
the	O
localities	O
have	O
historic	O
roots	O
in	O
older	O
municipalities	O
that	O
predate	O
the	O
formation	O
of	O
Greater	LOCATION
Berlin	LOCATION
on	O
1	O
October	O
1920	O
.	O
Berlin	LOCATION
maintains	O
official	O
partnerships	O
with	O
17	O
cities	O
.	O
Town	O
twinning	O
between	O
Berlin	LOCATION
and	O
other	O
cities	O
began	O
with	O
L.A.	LOCATION
in	O
1967	O
.	O
There	O
are	O
several	O
joint	O
projects	O
with	O
many	O
other	O
cities	O
,	O
such	O
as	O
Copenhagen	LOCATION
,	O
Helsinki	LOCATION
,	O
Johannesburg	LOCATION
,	O
Shanghai	LOCATION
,	O
Seoul	LOCATION
,	O
Sofia	LOCATION
,	O
Sydney	LOCATION
,	O
and	O
Vienna	LOCATION
.	O
Berlin	LOCATION
's	O
official	O
sister	O
cities	O
are	O
:	O
As	O
of	O
September	O
2009	O
,	O
the	O
city-state	O
of	O
Berlin	LOCATION
had	O
a	O
population	O
of	O
3,439,100	O
registered	O
inhabitants	O
in	O
an	O
area	O
of	O
891.82	O
square	O
kilometers	O
(	O
344.33	O
sq	O
mi	O
)	O
.	O
The	O
urban	O
area	O
of	O
Berlin	LOCATION
stretches	O
beyond	O
the	O
city	O
limits	O
and	O
comprises	O
about	O
3.7	O
million	O
people	O
while	O
the	O
metropolitan	O
area	O
of	O
the	O
Berlin-Brandenburg	O
region	O
is	O
home	O
to	O
about	O
4.3	O
million	O
in	O
an	O
area	O
of	O
5370	O
km	O
2	O
(	O
2070	O
sq	O
mi	O
)	O
.	O
The	O
Greater	O
Berlin	O
Act	O
in	O
1920	O
incorporated	O
many	O
suburbs	O
and	O
surrounding	O
cities	O
of	O
Berlin	LOCATION
.	O
It	O
formed	O
most	O
of	O
the	O
territory	O
that	O
comprises	O
modern	O
Berlin	LOCATION
.	O
The	O
act	O
increased	O
the	O
area	O
of	O
Berlin	LOCATION
from	O
66	O
km	O
2	O
(	O
25	O
sq	O
mi	O
)	O
to	O
883	O
km	O
2	O
(	O
341	O
sq	O
mi	O
)	O
and	O
the	O
population	O
from	O
1.9	O
million	O
to	O
4	O
million	O
.	O
More	O
than	O
sixty	O
percent	O
of	O
Berlin	LOCATION
residents	O
have	O
no	O
registered	O
religious	O
affiliation	O
and	O
Berlin	LOCATION
is	O
said	O
to	O
be	O
the	O
atheist	O
capital	O
of	O
Europe	LOCATION
.	O
The	O
largest	O
denominations	O
are	O
the	O
Evangelical	O
Church	O
of	O
Berlin-Brandenburg-Silesian	O
Upper	O
Lusatia	O
(	O
a	O
united	O
church	O
within	O
the	O
Evangelical	O
Church	O
in	O
Germany	LOCATION
)	O
with	O
19.4	O
%	O
of	O
the	O
population	O
as	O
of	O
2008	O
,	O
and	O
the	O
Roman	O
Catholic	O
Church	O
with	O
9.4	O
%	O
.	O
The	O
Independent	O
Evangelical	O
Lutheran	O
Church	O
has	O
eight	O
parishes	O
of	O
different	O
sizes	O
in	O
Berlin	LOCATION
.	O
In	O
2009	O
,	O
the	O
nominal	O
GDP	O
of	O
the	O
citystate	O
Berlin	LOCATION
experienced	O
a	O
growth	O
rate	O
of	O
1.7	O
%	O
(	O
-3.5	O
%	O
in	O
Germany	LOCATION
)	O
and	O
totaled	O
€	O
90.1	O
(	O
~	O
$	O
117	O
)	O
billion	O
.	O
Berlin	LOCATION
's	O
economy	O
is	O
today	O
dominated	O
by	O
the	O
service	O
sector	O
,	O
around	O
80	O
%	O
of	O
all	O
companies	O
are	O
operating	O
in	O
this	O
field	O
.	O
The	O
stateowned	O
firm	O
Deutsche	O
Bahn	O
has	O
its	O
headquarters	O
in	O
Berlin	LOCATION
as	O
well	O
.	O
Daimler	O
manufactures	O
cars	O
,	O
and	O
BMW	O
builds	O
motorcycles	O
in	O
Berlin	LOCATION
.	O
In	O
Germany	LOCATION
,	O
Universal	O
Music	O
and	O
Sony	O
Music	O
are	O
headquartered	O
in	O
Berlin	LOCATION
as	O
well	O
.	O
Research	O
and	O
development	O
have	O
established	O
economic	O
significance	O
,	O
and	O
the	O
Berlin	LOCATION
Brandenburg	LOCATION
region	O
ranks	O
among	O
the	O
top	O
three	O
innovative	O
regions	O
in	O
the	O
EU	O
.	O
Berlin	LOCATION
is	O
among	O
the	O
top	O
three	O
convention	O
cities	O
in	O
the	O
world	O
and	O
is	O
home	O
to	O
Europe	LOCATION
's	O
biggest	O
convention	O
center	O
in	O
the	O
form	O
of	O
the	O
Internationales	O
Congress	O
Centrum	O
(	O
ICC	O
)	O
.	O
Berlin	LOCATION
has	O
established	O
itself	O
as	O
the	O
third	O
most-visited	O
city	O
destination	O
in	O
the	O
European	O
Union	O
.	O
The	O
Berlin-Brandenburg	LOCATION
capital	O
region	O
is	O
one	O
of	O
the	O
most	O
prolific	O
centers	O
of	O
higher	O
education	O
and	O
research	O
in	O
the	O
European	O
Union	O
.	O
These	O
are	O
the	O
Humboldt	O
Universität	O
zu	O
Berlin	O
with	O
35,000	O
students	O
,	O
the	O
Freie	O
Universität	O
Berlin	O
(	O
Free	O
University	O
of	O
Berlin	O
)	O
with	O
around	O
35,000	O
students	O
,	O
and	O
the	O
Technische	O
Universität	O
Berlin	O
with	O
30,000	O
students	O
.	O
The	O
Universität	O
der	O
Künste	O
has	O
about	O
4,300	O
students	O
.	O
The	O
city	O
has	O
a	O
high	O
concentration	O
of	O
research	O
institutions	O
,	O
such	O
as	O
the	O
Fraunhofer	O
Society	O
,	O
Leibniz-Gemeinschaft	O
and	O
the	O
Max	O
Planck	O
Society	O
,	O
which	O
are	O
independent	O
of	O
,	O
or	O
only	O
loosely	O
connected	O
to	O
its	O
universities	O
.	O
In	O
addition	O
to	O
the	O
libraries	O
affiliated	O
with	O
the	O
various	O
universities	O
,	O
the	O
Staatsbibliothek	O
zu	O
Berlin	O
is	O
a	O
major	O
research	O
library	O
.	O
Berlin	LOCATION
has	O
878	O
schools	O
teaching	O
340,658	O
children	O
in	O
13,727	O
classes	O
and	O
56,787	O
trainees	O
in	O
businesses	O
and	O
elsewhere	O
.	O
The	O
Französisches	O
Gymnasium	O
Berlin	O
which	O
was	O
founded	O
in	O
1689	O
for	O
the	O
benefit	O
of	O
Huguenot	O
refugees	O
,	O
offers	O
instruction	O
.	O
Berlin	LOCATION
is	O
noted	O
for	O
its	O
numerous	O
cultural	O
institutions	O
,	O
many	O
of	O
which	O
enjoy	O
international	O
reputation	O
.	O
Berlin	LOCATION
is	O
the	O
home	O
of	O
many	O
television	O
and	O
radio	O
stations	O
;	O
international	O
,	O
national	O
as	O
well	O
as	O
regional	O
.	O
The	O
public	O
broadcaster	O
RBB	O
has	O
its	O
headquarters	O
there	O
as	O
well	O
as	O
the	O
commercial	O
broadcasters	O
MTV	O
Europe	O
,	O
VIVA	O
,	O
TVB	O
,	O
N24	PERSON
and	O
Sat.1	O
.	O
Berlin	LOCATION
has	O
Germany	LOCATION
's	O
largest	O
number	O
of	O
daily	O
newspapers	O
,	O
with	O
numerous	O
local	O
broadsheets	O
(	O
Berliner	O
Morgenpost	O
,	O
Berliner	O
Zeitung	O
,	O
Der	PERSON
Tagesspiegel	PERSON
)	O
,	O
and	O
three	O
major	O
tabloids	O
,	O
as	O
well	O
as	O
national	O
dailies	O
of	O
varying	O
sizes	O
,	O
each	O
with	O
a	O
different	O
political	O
affiliation	O
,	O
such	O
as	O
Die	O
Welt	O
,	O
Junge	PERSON
Welt	PERSON
,	O
Neues	O
Deutschland	O
,	O
and	O
Die	O
Tageszeitung	O
.	O
The	O
venerable	O
Babelsberg	O
Studios	O
and	O
the	O
production	O
company	O
UFA	O
are	O
located	O
outside	O
Berlin	LOCATION
in	O
Potsdam	LOCATION
.	O
Berlin	LOCATION
has	O
one	O
of	O
the	O
most	O
diverse	O
and	O
vibrant	O
nightlife	O
scenes	O
in	O
Europe	LOCATION
.	O
Many	O
had	O
not	O
been	O
rebuilt	O
since	O
the	O
Second	O
World	O
War	O
.	O
Several	O
technology	O
and	O
media	O
art	O
festivals	O
and	O
conferences	O
are	O
held	O
in	O
the	O
city	O
,	O
including	O
Transmediale	O
and	O
Chaos	O
Communication	O
Congress	O
.	O
Berlin	LOCATION
is	O
home	O
to	O
153	O
museums	O
.	O
Opposite	O
the	O
Museum	O
Island	O
there	O
is	O
the	O
DDR	O
Museum	O
about	O
the	O
life	O
in	O
the	O
GDR	LOCATION
.	O
Apart	O
from	O
the	LOCATION
Museum	LOCATION
Island	LOCATION
,	O
there	O
are	O
a	O
wide	O
variety	O
of	O
other	O
museums	O
.	O
The	O
Hamburger	O
Bahnhof	O
,	O
located	O
in	O
Moabit	LOCATION
,	O
exhibits	O
a	O
major	O
collection	O
of	O
modern	O
and	O
contemporary	O
art	O
.	O
The	O
Bauhaus	O
Archive	O
is	O
an	O
architecture	O
museum	O
.	O
The	O
Jewish	O
Museum	O
has	O
a	O
standing	O
exhibition	O
on	O
two	O
millennia	O
of	O
German-Jewish	O
history	O
.	O
The	O
German	O
Museum	O
of	O
Technology	O
in	O
Kreuzberg	LOCATION
has	O
a	O
large	O
collection	O
of	O
historical	O
technical	O
artifacts	O
.	O
The	O
Museum	O
für	O
Naturkunde	O
exhibits	O
natural	O
history	O
near	O
Berlin	LOCATION
Hauptbahnhof	LOCATION
.	O
In	O
Lichtenberg	LOCATION
,	O
on	O
the	O
grounds	O
of	O
the	O
former	O
East	O
German	O
Ministry	O
for	O
State	O
Security	O
(	O
Stasi	O
)	O
,	O
is	O
the	O
Stasi	O
Museum	O
.	O
The	O
site	O
of	O
Checkpoint	O
Charlie	O
,	O
one	O
of	O
the	O
renowned	O
crossing	O
points	O
of	O
the	O
Berlin	O
Wall	O
,	O
is	O
still	O
preserved	O
and	O
also	O
has	O
a	O
museum	O
.	O
The	O
museum	O
,	O
which	O
is	O
a	O
private	O
venture	O
,	O
exhibits	O
a	O
comprehensive	O
array	O
of	O
material	O
about	O
people	O
who	O
devised	O
ingenious	O
plans	O
to	O
flee	O
the	O
East	LOCATION
.	O
The	O
Beate	O
Uhse	O
Erotic	O
Museum	O
near	O
Zoo	O
Station	O
claims	O
to	O
be	O
the	O
world	O
's	O
largest	O
erotic	O
museum	O
.	O
Berlin	LOCATION
is	O
home	O
to	O
more	O
than	O
50	O
theaters	O
.	O
Berlin	LOCATION
has	O
three	O
major	O
opera	O
houses	O
:	O
the	O
Deutsche	O
Oper	O
,	O
the	O
Berlin	O
State	O
Opera	O
,	O
and	O
the	O
Komische	O
Oper	O
.	O
The	O
Berlin	O
State	O
Opera	O
on	O
Unter	O
den	O
Linden	O
is	O
the	O
oldest	O
;	O
it	O
opened	O
in	O
1742	O
.	O
Its	O
current	O
musical	O
director	O
is	O
Daniel	PERSON
Barenboim	PERSON
.	O
The	O
Komische	O
Oper	O
has	O
traditionally	O
specialized	O
in	O
operettas	O
and	O
is	O
located	O
at	O
Unter	O
den	O
Linden	O
as	O
well	O
.	O
The	O
Deutsche	O
Oper	O
opened	O
in	O
1912	O
in	O
Charlottenburg	LOCATION
.	O
There	O
are	O
seven	O
symphony	O
orchestras	O
in	O
Berlin	LOCATION
.	O
The	O
Berlin	O
Philharmonic	O
Orchestra	O
is	O
one	O
of	O
the	O
preeminent	O
orchestras	O
in	O
the	O
world	O
;	O
it	O
is	O
housed	O
in	O
the	O
Berliner	O
Philharmonie	O
near	O
Potsdamer	O
Platz	O
on	O
a	O
street	O
named	O
for	O
the	O
orchestra	O
's	O
longest-serving	O
conductor	O
,	O
Herbert	PERSON
von	PERSON
Karajan	PERSON
.	O
The	O
current	O
principal	O
conductor	O
is	O
Simon	PERSON
Rattle	PERSON
.	O
Its	O
current	O
principal	O
conductor	O
is	O
Lothar	PERSON
Zagrosek	PERSON
.	PERSON
The	O
Haus	O
der	O
Kulturen	O
der	O
Welt	O
presents	O
various	O
exhibitions	O
dealing	O
with	O
intercultural	O
issues	O
and	O
stages	O
world	O
music	O
and	O
conferences	O
.	O
They	O
remain	O
favorites	O
in	O
Berlin	LOCATION
along	O
with	O
rustic	O
and	O
hearty	O
dishes	O
using	O
pork	O
,	O
goose	O
,	O
fish	O
,	O
peas	O
,	O
beans	O
and	O
potatoes	O
.	O
One	O
renowned	O
dish	O
that	O
actually	O
bears	O
the	O
name	O
of	O
Berlin	LOCATION
is	O
Berlin	LOCATION
style	O
liver	O
;	O
floured	O
liver	O
with	O
apples	O
and	O
onions	O
.	O
Zoologischer	O
Garten	O
Berlin	O
,	O
the	O
older	O
of	O
two	O
zoos	O
in	O
the	O
city	O
,	O
was	O
founded	O
in	O
1844	O
,	O
and	O
presents	O
the	O
most	O
diverse	O
range	O
of	O
species	O
in	O
the	O
world	O
.	O
It	O
is	O
the	O
home	O
of	O
the	O
captive-born	O
celebrity	O
polar	O
bear	O
Knut	PERSON
,	O
born	O
in	O
December	O
2006	O
.	O
The	O
Tiergarten	LOCATION
is	O
Berlin	LOCATION
's	O
largest	O
park	O
located	O
in	O
Mitte	LOCATION
and	O
was	O
designed	O
by	O
Peter	PERSON
Joseph	PERSON
Lenné	PERSON
.	O
In	O
Kreuzberg	LOCATION
the	O
Viktoriapark	O
provides	O
a	O
good	O
viewing	O
point	O
over	O
the	O
southern	O
part	O
of	O
inner	O
city	O
Berlin	LOCATION
.	O
Berlin	LOCATION
is	O
known	O
for	O
its	O
numerous	O
beach	O
bars	O
along	O
the	O
river	O
Spree	LOCATION
.	O
Berlin	LOCATION
has	O
established	O
a	O
high	O
profile	O
reputation	O
as	O
a	O
host	O
city	O
of	O
international	O
sporting	O
events	O
.	O
The	O
IAAF	O
World	O
Championships	O
in	O
Athletics	O
were	O
held	O
in	O
the	O
Olympiastadion	O
in	O
August	O
2009	O
.	O
The	O
annual	O
Berlin	O
Marathon	O
and	O
the	O
annual	O
ÅF	O
Golden	O
League	O
event	O
ISTAF	O
for	O
athletics	O
are	O
also	O
held	O
here	O
.	O
The	O
WTA	O
Tour	O
holds	O
the	O
Qatar	LOCATION
Total	O
German	O
Open	O
annually	O
in	O
the	O
city	O
.	O
Several	O
major	O
clubs	O
representing	O
the	O
most	O
popular	O
spectator	O
sports	O
in	O
Germany	LOCATION
have	O
their	O
base	O
in	O
Berlin	LOCATION
.	O
Berlin	LOCATION
has	O
developed	O
a	O
highly	O
complex	O
transportation	O
infrastructure	O
providing	O
very	O
diverse	O
modes	O
of	O
urban	O
mobility	O
.	O
979	O
bridges	O
cross	O
197	O
kilometers	O
of	O
innercity	O
waterways	O
,	O
5334	O
kilometers	O
(	O
3314	O
mi	O
)	O
of	O
roads	O
run	O
through	O
Berlin	LOCATION
,	O
of	O
which	O
73	O
kilometers	O
(	O
45	O
mi	O
)	O
are	O
motorways	O
.	O
Regional	O
rail	O
lines	O
provide	O
access	O
to	O
the	O
surrounding	O
regions	O
of	O
Brandenburg	LOCATION
and	O
to	O
the	LOCATION
Baltic	LOCATION
Sea	LOCATION
.	O
The	O
Berlin	O
Hauptbahnhof	O
is	O
the	O
largest	O
crossing	O
station	O
in	O
Europe	LOCATION
.	O
Deutsche	O
Bahn	O
runs	O
trains	O
to	O
domestic	O
destinations	O
like	O
Nuremberg	LOCATION
,	O
Hamburg	LOCATION
,	O
Freiburg	LOCATION
and	O
more	O
.	O
Berlin	LOCATION
is	O
known	O
for	O
its	O
highly	O
developed	O
bike	O
lane	O
system	O
.	O
The	O
Berliner	O
Verkehrsbetriebe	O
and	O
the	O
Deutsche	O
Bahn	O
manage	O
several	O
dense	O
urban	O
public	O
transport	O
systems	O
.	O
Berlin	LOCATION
has	O
two	O
commercial	O
airports	O
.	O
Tegel	O
International	O
Airport	O
,	O
the	O
busier	O
,	O
and	O
Schönefeld	O
International	O
Airport	O
(	O
SXF	O
)	O
handled	O
more	O
than	O
21	O
million	O
passengers	O
combined	O
in	O
2009	O
.	O
Whereas	O
Schönefeld	PERSON
handles	O
mainly	O
low-cost-aviation	O
and	O
is	O
situated	O
just	O
outside	O
Berlin	LOCATION
's	O
south-eastern	O
border	O
in	O
the	O
state	O
of	O
Brandenburg	LOCATION
.	O
Berlin	LOCATION
's	O
airport	O
authority	O
aims	O
to	O
transfer	O
all	O
of	O
Berlin	LOCATION
's	O
air	O
traffic	O
in	O
June	O
2012	O
to	O
a	O
newly	O
built	O
airport	O
at	O
Schönefeld	LOCATION
,	O
to	O
be	O
renamed	O
Berlin	O
Brandenburg	O
Airport	O
.	O
Because	O
burning	O
lignite	O
produces	O
harmful	O
emissions	O
,	O
Vattenfall	O
has	O
announced	O
a	O
commitment	O
to	O
shift	O
towards	O
reliance	O
on	O
cleaner	O
,	O
renewable	O
energy	O
sources	O
.	O
Berlin	LOCATION
has	O
a	O
long	O
tradition	O
as	O
a	O
city	O
of	O
medicine	O
and	O
medical	O
technology	O
.	O
The	O
history	O
of	O
medicine	O
has	O
been	O
widely	O
influenced	O
by	O
scientists	O
from	O
Berlin	LOCATION
.	O
Rudolf	PERSON
Virchow	PERSON
was	O
the	O
founder	O
of	O
cellular	O
pathology	O
,	O
while	O
Robert	PERSON
Koch	PERSON
,	O
discovered	O
the	O
vaccinations	O
for	O
anthrax	O
,	O
cholera	O
,	O
and	O
tuberculosis	O
bacillus	O
.	O
The	O
Charité	O
hospital	O
complex	O
is	O
today	O
the	O
largest	O
university	O
hospital	O
in	O
Europe	LOCATION
tracing	O
back	O
its	O
origins	O
to	O
the	O
year	O
1710	O
.	O
The	O
Charité	O
is	O
spread	O
over	O
four	O
sites	O
and	O
comprises	O
3,300	O
beds	O
,	O
around	O
14,000	O
staff	O
,	O
8,000	O
students	O
,	O
over	O
60	O
operating	O
theatres	O
with	O
an	O
annual	O
turnover	O
of	O
over	O
one	O
billion	O
euros	O
.	O
It	O
is	O
a	O
joint	O
institution	O
of	O
the	O
Free	O
University	O
of	O
Berlin	O
and	O
the	O
Humboldt	O
University	O
of	O
Berlin	O
,	O
including	O
a	O
wide	O
range	O
of	O
institutes	O
and	O
medical	O
competence	O
centers	O
.	O
