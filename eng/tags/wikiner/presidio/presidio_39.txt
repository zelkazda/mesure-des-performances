Although	O
some	O
speculate	O
that	O
it	O
is	O
related	O
to	O
Latin	O
algēre	O
,	O
"	O
be	O
cold	O
"	O
,	O
there	O
is	O
no	O
known	O
reason	O
to	O
associate	O
seaweed	O
with	O
temperature	O
.	O
Algae	O
are	O
national	O
foods	O
of	O
many	O
nations	O
:	O
China	LOCATION
consumes	O
more	O
than	O
70	O
species	O
,	O
including	O
fat	O
choy	O
,	O
a	O
cyanobacterium	O
considered	O
a	O
vegetable	O
;	O
Japan	LOCATION
,	O
over	O
20	O
species	O
;	O
Ireland	LOCATION
,	O
dulse	O
;	O
Chile	LOCATION
,	O
cochayuyo	O
.	O
Laver	PERSON
is	O
used	O
to	O
make	O
"	O
laver	O
bread	O
"	O
in	O
Wales	LOCATION
where	O
it	O
is	O
known	O
as	O
bara	PERSON
lawr	PERSON
;	O
in	O
Korea	LOCATION
,	O
gim	O
;	O
in	O
Japan	LOCATION
,	O
nori	O
and	O
aonori	LOCATION
.	LOCATION
Sea	LOCATION
lettuce	O
and	O
badderlocks	O
are	O
a	O
salad	O
ingredient	O
in	O
Scotland	LOCATION
,	O
Ireland	LOCATION
,	O
Greenland	LOCATION
and	O
Iceland	LOCATION
.	O
