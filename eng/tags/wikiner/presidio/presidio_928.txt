Abd	PERSON
ar-Rahman	PERSON
II	PERSON
(	O
Arabic	O
:	O
عبد	O
الرحمن	O
الثاني	PERSON
‎	O
;	O
788	O
--	O
852	O
)	O
was	O
Umayyad	O
Emir	O
of	O
Córdoba	LOCATION
in	O
the	LOCATION
Al-Andalus	LOCATION
from	O
822	O
until	O
his	O
death	O
.	O
In	O
844	O
Abd	PERSON
ar-Rahman	PERSON
repulsed	O
an	O
assault	O
by	O
Vikings	O
who	O
had	O
disembarked	O
in	O
Cadiz	LOCATION
,	O
conquered	O
Seville	LOCATION
(	O
with	O
the	O
exception	O
of	O
its	O
citadel	O
)	O
and	O
attacked	O
Córdoba	LOCATION
itself	O
.	O
Thereafter	O
he	O
constructed	O
a	O
fleet	O
and	O
naval	O
arsenal	O
at	O
Seville	LOCATION
to	O
repel	O
future	O
raids	O
.	O
Abd	PERSON
ar-Rahman	PERSON
was	O
famous	O
for	O
his	O
public	O
building	O
program	O
in	O
Córdoba	LOCATION
where	O
he	O
died	O
in	O
852	O
.	O
He	O
was	O
also	O
involved	O
in	O
the	O
execution	O
of	O
the	O
"	O
Martyrs	O
of	O
Córdoba	O
"	O
.	O
