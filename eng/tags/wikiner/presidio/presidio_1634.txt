Charles	PERSON
Hardin	PERSON
Holley	PERSON
(	O
September	O
7	O
,	O
1936	O
--	O
February	O
3	O
,	O
1959	O
)	O
known	O
professionally	O
as	O
Buddy	O
Holly	O
,	O
was	O
an	O
American	O
singer-songwriter	O
and	O
a	O
pioneer	O
of	O
rock	O
and	O
roll	O
.	O
His	O
works	O
and	O
innovations	O
inspired	O
and	O
influenced	O
contemporary	O
and	O
later	O
musicians	O
,	O
notably	O
The	O
Beatles	O
,	O
The	O
Rolling	O
Stones	O
,	O
Don	PERSON
McLean	PERSON
,	O
and	O
Bob	PERSON
Dylan	PERSON
,	O
and	O
exerted	O
a	O
profound	O
influence	O
on	O
popular	O
music	O
.	O
More	O
recently	O
his	O
influence	O
reached	O
MGMT	O
and	O
Weezer	PERSON
.	O
Holly	O
was	O
amongst	O
the	O
first	O
group	O
of	O
inductees	O
to	O
the	O
Rock	O
and	O
Roll	O
Hall	O
of	O
Fame	O
in	O
1986	O
.	O
Holly	PERSON
was	O
always	O
called	O
"	O
Buddy	O
"	O
by	O
his	O
family	O
.	O
Holly	PERSON
saw	O
Elvis	PERSON
Presley	PERSON
sing	O
in	O
Lubbock	LOCATION
in	O
1955	O
and	O
began	O
to	O
incorporate	O
a	O
rockabilly	O
style	O
with	O
Chet	PERSON
Atkins	PERSON
style	O
lead	O
guitar	O
,	O
strong	O
rhythm	O
acoustic	O
and	O
slap	O
bass	O
into	O
his	O
music	O
.	O
On	O
October	O
15	O
he	O
opened	O
the	O
bill	O
for	O
Presley	PERSON
in	O
Lubbock	LOCATION
,	O
catching	O
the	O
eye	O
of	O
a	O
Nashville	LOCATION
talent	O
scout	O
.	O
Following	O
this	O
performance	O
Decca	O
Records	O
signed	O
him	O
to	O
a	O
contract	O
in	O
February	O
1956	O
,	O
misspelling	O
his	O
name	O
as	O
"	O
Holly	O
"	O
.	O
Holly	PERSON
formed	O
his	O
own	O
band	O
,	O
later	O
to	O
be	O
called	O
The	O
Crickets	O
and	O
consisting	O
of	O
Holly	PERSON
(	O
lead	O
guitar	O
and	O
vocalist	O
)	O
,	O
Niki	PERSON
Sullivan	PERSON
(	O
guitar	O
)	O
,	O
Joe	PERSON
B.	PERSON
Mauldin	PERSON
(	O
bass	O
)	O
,	O
and	O
Jerry	PERSON
Allison	PERSON
(	O
drums	O
)	O
.	O
They	O
went	O
to	O
Nashville	LOCATION
for	O
three	O
recording	O
sessions	O
with	O
producer	O
Owen	PERSON
Bradley	PERSON
.	O
Among	O
the	O
tracks	O
he	O
recorded	O
was	O
an	O
early	O
version	O
of	O
"	O
That	O
'll	O
Be	O
The	O
Day	O
"	O
,	O
which	O
took	O
its	O
title	O
from	O
a	O
line	O
that	O
John	PERSON
Wayne	PERSON
's	PERSON
character	O
says	O
repeatedly	O
in	O
the	O
1956	O
film	O
,	O
The	O
Searchers	O
.	O
On	O
January	O
22	O
,	O
1957	O
,	O
Decca	O
informed	O
Holly	PERSON
that	O
his	O
contract	O
would	O
not	O
be	O
renewed	O
,	O
insisting	O
however	O
that	O
he	O
could	O
not	O
record	O
the	O
same	O
songs	O
for	O
anyone	O
else	O
for	O
five	O
years	O
.	O
Petty	O
contacted	O
music	O
publishers	O
and	O
labels	O
,	O
and	O
Brunswick	O
Records	O
,	O
a	O
subsidiary	O
of	O
Decca	O
,	O
signed	O
the	O
Crickets	O
on	O
March	O
19	O
,	O
1957	O
.	O
Holly	PERSON
signed	O
as	O
a	O
solo	O
artist	O
with	O
another	O
Decca	O
subsidiary	O
,	O
Coral	O
Records	O
.	O
On	O
May	O
27	O
,	O
1957	O
,	O
"	O
That	O
'll	O
Be	O
The	O
Day	O
"	O
was	O
released	O
as	O
a	O
single	O
,	O
credited	O
to	O
the	O
Crickets	O
to	O
try	O
to	O
bypass	O
Decca	O
's	O
claimed	O
legal	O
rights	O
.	O
When	O
the	O
song	O
became	O
a	O
hit	O
Decca	O
decided	O
not	O
to	O
press	O
its	O
claim	O
.	O
Unlike	O
the	O
immediate	O
acceptance	O
shown	O
in	O
the	O
1978	O
movie	O
The	O
Buddy	O
Holly	O
Story	O
,	O
it	O
actually	O
took	O
several	O
performances	O
for	O
the	O
audience	O
to	O
warm	O
to	O
him	O
.	O
In	O
August	O
1957	O
,	O
the	O
Crickets	O
were	O
the	O
only	O
white	O
performers	O
on	O
a	O
national	O
tour	O
including	O
black	O
neighborhood	O
theaters	O
.	O
As	O
Holly	PERSON
was	O
signed	O
both	O
as	O
a	O
solo	O
artist	O
and	O
a	O
member	O
of	O
the	O
Crickets	O
two	O
debut	O
albums	O
were	O
released	O
:	O
The	O
"	O
Chirping	O
"	O
Crickets	O
on	O
November	O
27	O
,	O
1957	O
and	O
Buddy	O
Holly	O
on	O
February	O
20	O
,	O
1958	O
.	O
Their	O
third	O
and	O
final	O
album	O
,	O
That	O
'll	O
Be	O
the	O
Day	O
,	O
was	O
put	O
together	O
from	O
early	O
recordings	O
and	O
was	O
released	O
in	O
April	O
.	O
He	O
asked	O
her	O
to	O
have	O
dinner	O
with	O
him	O
that	O
night	O
at	O
P.	PERSON
J.	PERSON
Clarke	PERSON
's	PERSON
.	O
Holly	PERSON
proposed	O
marriage	O
to	O
her	O
on	O
their	O
very	O
first	O
date	O
.	O
But	O
when	O
I	O
saw	O
Buddy	O
,	O
it	O
was	O
like	O
magic	O
.	O
We	O
had	O
something	O
special	O
:	O
love	O
at	O
first	O
sight,	O
"	O
she	O
told	O
the	O
Lubbock	O
Avalanche-Journal	O
on	O
what	O
would	O
have	O
been	O
their	O
50th	O
wedding	O
anniversary	O
.	O
Holly	PERSON
wrote	O
the	O
song	O
"	O
True	O
Love	O
Ways	O
"	O
about	O
his	O
relationship	O
with	O
his	O
young	O
wife	O
.	O
It	O
was	O
not	O
until	O
Holly	PERSON
died	O
that	O
many	O
fans	O
became	O
aware	O
of	O
his	O
marriage	O
.	O
He	O
wanted	O
to	O
develop	O
collaborations	O
between	O
soul	O
singers	O
and	O
rock	O
'	O
n	O
'	O
roll	O
,	O
hoping	O
to	O
make	O
an	O
album	O
with	O
Ray	PERSON
Charles	PERSON
and	O
gospel	O
legend	O
Mahalia	PERSON
Jackson	PERSON
.	O
Yet	O
,	O
with	O
the	O
money	O
still	O
being	O
withheld	O
by	O
Petty	PERSON
and	O
with	O
rent	O
due	O
,	O
Buddy	O
was	O
forced	O
to	O
go	O
back	O
on	O
the	O
road	O
.	O
He	O
assembled	O
a	O
backing	O
band	O
consisting	O
of	O
Tommy	PERSON
Allsup	PERSON
(	O
guitar	O
)	O
,	O
Waylon	PERSON
Jennings	PERSON
(	O
bass	O
)	O
and	O
Carl	PERSON
Bunch	PERSON
(	O
drums	O
)	O
and	O
billed	O
as	O
The	O
Crickets	O
.	O
Following	O
a	O
performance	O
at	O
the	O
Surf	O
Ballroom	O
in	O
Clear	LOCATION
Lake	LOCATION
,	O
Iowa	LOCATION
on	O
February	O
2	O
,	O
1959	O
,	O
Holly	PERSON
chartered	O
a	O
small	O
airplane	O
to	O
take	O
him	O
to	O
the	O
next	O
stop	O
on	O
the	O
tour	O
.	O
Jennings	PERSON
shot	O
back	O
facetiously	O
,	O
"	O
Well	O
,	O
I	O
hope	O
your	O
ol	O
'	O
plane	O
crashes	O
!	O
"	O
It	O
was	O
a	O
statement	O
that	O
would	O
haunt	O
Jennings	PERSON
for	O
decades	O
.	O
Don	PERSON
McLean	PERSON
referred	O
to	O
it	O
as	O
"	O
The	O
Day	O
the	O
Music	O
Died	O
"	O
in	O
his	O
song	O
"	O
American	O
Pie	O
"	O
.	O
María	PERSON
Elena	PERSON
Holly	PERSON
did	O
not	O
attend	O
the	O
funeral	O
and	O
has	O
never	O
visited	O
the	O
grave	O
site	O
.	O
Holly	O
's	O
music	O
was	O
sophisticated	O
for	O
its	O
day	O
,	O
including	O
the	O
use	O
of	O
instruments	O
considered	O
novel	O
for	O
rock	O
and	O
roll	O
,	O
such	O
as	O
the	O
celesta	O
(	O
heard	O
on	O
"	O
Everyday	O
"	O
)	O
.	O
While	O
Holly	PERSON
could	O
pump	O
out	O
boy-loves-girl	O
songs	O
with	O
the	O
best	O
of	O
his	O
contemporaries	O
,	O
other	O
songs	O
featured	O
more	O
sophisticated	O
lyrics	O
and	O
more	O
complex	O
harmonies	O
and	O
melodies	O
than	O
had	O
previously	O
appeared	O
in	O
the	O
genre	O
.	O
Other	O
singers	O
(	O
such	O
as	O
Elvis	PERSON
)	O
have	O
used	O
a	O
similar	O
technique	O
,	O
though	O
less	O
obviously	O
and	O
consistently	O
.	O
Holly	PERSON
set	O
the	O
template	O
for	O
the	O
standard	O
rock	O
and	O
roll	O
band	O
:	O
two	O
guitars	O
,	O
bass	O
,	O
and	O
drums	O
.	O
Along	O
with	O
Elvis	PERSON
and	O
others	O
,	O
Holly	PERSON
made	O
rock	O
and	O
roll	O
,	O
with	O
its	O
roots	O
in	O
rockabilly	O
country	O
music	O
and	O
blues	O
inspired	O
rhythm	O
and	O
blues	O
music	O
,	O
more	O
popular	O
among	O
a	O
broad	O
white	O
audience	O
.	O
From	O
listening	O
to	O
their	O
recordings	O
,	O
one	O
had	O
difficulty	O
determining	O
if	O
the	O
Crickets	O
were	O
white	O
or	O
black	O
singers	O
.	O
Holly	O
indeed	O
sometimes	O
played	O
with	O
black	O
musicians	O
Little	PERSON
Richard	PERSON
and	O
Chuck	PERSON
Berry	PERSON
.	O
The	O
Crickets	O
were	O
only	O
the	O
second	O
white	O
rock	O
group	O
to	O
tour	O
Great	LOCATION
Britain	LOCATION
,	O
and	O
they	O
inspired	O
the	O
later	O
Beatles	O
,	O
a	O
name	O
somewhat	O
similar	O
to	O
the	O
Crickets	O
.	O
Holly	O
's	O
essential	O
eyeglasses	O
encouraged	O
other	O
musicians	O
,	O
such	O
as	O
John	PERSON
Lennon	PERSON
,	O
also	O
to	O
wear	O
their	O
glasses	O
during	O
performances	O
.	O
Lennon	PERSON
and	O
McCartney	PERSON
later	O
cited	O
Holly	PERSON
as	O
a	O
primary	O
influence	O
.	O
(	O
Their	O
band	O
's	O
name	O
,	O
The	O
Beatles	O
,	O
was	O
chosen	O
partly	O
in	O
homage	O
to	O
Holly	O
's	O
Crickets	O
.	O
)	O
McCartney	O
owns	O
the	O
publishing	O
rights	O
to	O
Holly	PERSON
's	O
song	O
catalogue	O
.	O
A	O
17-year-old	O
Bob	PERSON
Dylan	PERSON
attended	O
the	O
January	O
31	O
,	O
1959	O
,	O
show	O
,	O
two	O
nights	O
before	O
Holly	PERSON
's	O
death	O
.	O
Keith	PERSON
Richards	PERSON
attended	O
one	O
of	O
Holly	PERSON
's	O
performances	O
,	O
where	O
he	O
heard	O
"	O
Not	O
Fade	O
Away	O
"	O
for	O
the	O
first	O
time	O
.	O
The	O
Rolling	O
Stones	O
had	O
an	O
early	O
hit	O
covering	O
the	O
song	O
.	O
Holly	PERSON
influenced	O
many	O
other	O
singers	O
during	O
and	O
after	O
a	O
career	O
that	O
lasted	O
barely	O
two	O
years	O
.	O
Keith	PERSON
Richards	PERSON
once	O
said	O
that	O
Holly	PERSON
had	O
"	O
an	O
influence	O
on	O
everybody	O
.	O
"	O
The	O
Grateful	O
Dead	O
performed	O
"	O
Not	O
Fade	O
Away	O
"	O
530	O
times	O
over	O
the	O
course	O
of	O
their	O
career	O
,	O
making	O
it	O
their	O
seventh	O
most-performed	O
song	O
.	O
Various	O
rock	O
and	O
roll	O
histories	O
have	O
asserted	O
that	O
the	O
singing	O
group	O
The	O
Hollies	O
were	O
named	O
in	O
homage	O
to	O
Buddy	O
Holly	O
.	O
Buddy	O
Holly	O
released	O
only	O
three	O
albums	O
in	O
his	O
lifetime	O
.	O
Nonetheless	O
,	O
he	O
recorded	O
so	O
prolifically	O
that	O
Coral	O
Records	O
was	O
able	O
to	O
release	O
brand-new	O
albums	O
and	O
singles	O
for	O
10	O
years	O
after	O
his	O
death	O
,	O
although	O
the	O
technical	O
quality	O
was	O
very	O
mixed	O
,	O
some	O
being	O
studio	O
quality	O
and	O
others	O
home	O
recordings	O
.	O
Holly	O
's	O
simple	O
demonstration	O
recordings	O
were	O
overdubbed	O
by	O
studio	O
musicians	O
to	O
bring	O
them	O
up	O
to	O
then-commercial	O
standards	O
.	O
A	O
short	O
time	O
later	O
,	O
a	O
raid	O
produced	O
the	O
stolen	O
tapes	O
,	O
which	O
were	O
returned	O
to	O
MCA	O
.	O
Star	O
Gary	PERSON
Busey	PERSON
received	O
a	O
nomination	O
for	O
Academy	O
Award	O
for	O
Best	O
Actor	O
for	O
his	O
portrayal	O
of	O
Holly	PERSON
.	O
The	O
movie	O
was	O
widely	O
criticized	O
by	O
the	O
rock	O
community	O
and	O
Holly	PERSON
's	O
friends	O
and	O
family	O
for	O
its	O
inaccuracies	O
.	O
This	O
video	O
includes	O
interviews	O
with	O
Keith	PERSON
Richards	PERSON
,	O
Phil	PERSON
and	O
Don	PERSON
Everly	PERSON
,	O
Sonny	PERSON
Curtis	PERSON
,	O
Jerry	PERSON
Allison	PERSON
,	O
Holly	PERSON
's	O
family	O
,	O
and	O
McCartney	PERSON
himself	O
,	O
among	O
others	O
.	O
In	O
1987	O
,	O
Marshall	PERSON
Crenshaw	PERSON
portrayed	O
Buddy	O
Holly	O
in	O
the	O
movie	O
La	PERSON
Bamba	PERSON
.	O
He	O
is	O
featured	O
performing	O
at	O
the	O
Surf	O
Ballroom	O
and	O
boarding	O
the	O
doomed	O
airplane	O
with	O
Ritchie	PERSON
Valens	PERSON
and	O
The	O
Big	O
Bopper	O
.	O
Steve	PERSON
Buscemi	PERSON
played	O
a	O
Buddy	O
Holly	O
imitator/waiter	O
in	O
Pulp	O
Fiction	O
(	O
1994	O
)	O
.	O
There	O
were	O
also	O
successful	O
Broadway	O
and	O
West	O
End	O
musicals	O
documenting	O
his	O
career	O
.	O
Buddy	O
--	O
The	O
Buddy	O
Holly	O
Story	O
ran	O
in	O
the	LOCATION
West	LOCATION
End	LOCATION
for	O
13	O
years	O
.	O
This	O
was	O
followed	O
by	O
a	O
tour	O
and	O
return	O
to	O
the	LOCATION
West	LOCATION
End	LOCATION
on	O
August	O
3	O
,	O
2007	O
.	O
Buddy	O
has	O
appeared	O
as	O
a	O
fictional	O
character	O
in	O
several	O
novels	O
.	O
Holly	O
did	O
not	O
gain	O
widespread	O
acceptance	O
in	O
Lubbock	LOCATION
until	O
after	O
his	O
death	O
.	O
In	O
Lubbock	LOCATION
,	O
recognition	O
of	O
musicians	O
or	O
their	O
popularity	O
was	O
rare	O
,	O
even	O
as	O
numerous	O
musicians	O
and	O
performers	O
emerged	O
from	O
the	O
city	O
and	O
surrounding	O
region	O
at	O
the	O
same	O
time	O
as	O
Holly	PERSON
and	O
in	O
successive	O
years	O
.	O
It	O
is	O
located	O
on	O
private	O
farmland	O
approximately	O
five	O
miles	O
north	O
of	O
Clear	LOCATION
Lake	LOCATION
.	O
Buddy	O
Holly	O
's	O
previous	O
home	O
in	O
Lubbock	LOCATION
is	O
still	O
standing	O
.	O
