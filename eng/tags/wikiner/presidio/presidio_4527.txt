The	O
foreign	O
relations	O
of	O
Afghanistan	LOCATION
,	O
like	O
those	O
of	O
any	O
country	O
,	O
have	O
changed	O
along	O
with	O
the	O
political	O
,	O
sociological	O
,	O
and	O
economic	O
state	O
of	O
the	O
various	O
parts	O
of	O
Afghanistan	LOCATION
.	O
Before	O
the	O
Soviet	O
invasion	O
,	O
Afghanistan	LOCATION
pursued	O
a	O
policy	O
of	O
neutrality	O
and	O
nonalignment	O
in	O
its	O
foreign	O
relations	O
,	O
being	O
one	O
of	O
a	O
few	O
independent	O
nations	O
to	O
stay	O
neutral	O
in	O
both	O
World	O
War	O
I	O
and	O
World	O
War	O
II	O
.	O
In	O
international	O
forums	O
,	O
Afghanistan	LOCATION
generally	O
followed	O
the	O
voting	O
patterns	O
of	O
Asian	O
and	O
African	O
non-aligned	O
countries	O
.	O
During	O
the	O
1950s	O
and	O
60s	O
,	O
Afghanistan	LOCATION
was	O
able	O
to	O
use	O
the	O
Russian	O
and	O
American	O
need	O
for	O
allies	O
during	O
the	O
Cold	O
War	O
as	O
a	O
way	O
to	O
receive	O
economic	O
assistance	O
from	O
both	O
countries	O
.	O
However	O
,	O
given	O
that	O
unlike	O
Russia	LOCATION
,	O
America	LOCATION
refused	O
to	O
give	O
extensive	O
military	O
aid	O
to	O
the	O
country	O
,	O
the	O
government	O
of	O
Mohammed	PERSON
Daoud	PERSON
Khan	PERSON
developed	O
warmer	O
ties	O
with	O
the	O
USSR	LOCATION
while	O
officially	O
remaining	O
non-aligned	O
.	O
After	O
the	O
December	O
1979	O
invasion	O
,	O
Afghanistan	LOCATION
's	O
foreign	O
policy	O
mirrored	O
that	O
of	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
.	O
Afghan	O
foreign	O
policymakers	O
attempted	O
,	O
with	O
little	O
success	O
,	O
to	O
increase	O
their	O
regime	O
's	O
low	O
standing	O
in	O
the	O
noncommunist	O
world	O
.	O
With	O
the	O
signing	O
of	O
the	O
Geneva	O
Accords	O
,	O
President	O
Mohammad	PERSON
Najibullah	PERSON
unsuccessfully	O
sought	O
to	O
end	O
the	LOCATION
Democratic	LOCATION
Republic	LOCATION
of	LOCATION
Afghanistan	LOCATION
's	O
isolation	O
within	O
the	O
Islamic	O
world	O
and	O
in	O
the	O
Non-Aligned	O
Movement	O
.	O
Many	O
countries	O
subsequently	O
closed	O
their	O
missions	O
due	O
to	O
instability	O
and	O
heavy	O
fighting	O
in	O
Kabul	LOCATION
after	O
the	O
Soviet	O
withdrawal	O
in	O
1989	O
.	O
Many	O
countries	O
initially	O
welcomed	O
the	O
introduction	O
of	O
the	O
Taliban	O
,	O
who	O
they	O
saw	O
as	O
a	O
stabilizing	O
,	O
law-enforcing	O
alternative	O
to	O
the	O
warlords	O
who	O
had	O
ruled	O
the	O
country	O
since	O
the	O
fall	O
of	O
Najibullah	PERSON
's	O
government	O
in	O
1992	O
.	O
Repeated	O
Taliban	O
efforts	O
to	O
occupy	O
Afghanistan	LOCATION
's	O
seat	O
at	O
the	O
United	O
Nations	O
and	O
OIC	O
were	O
unsuccessful	O
.	O
The	O
government	O
of	O
President	O
Hamid	PERSON
Karzai	PERSON
is	O
currently	O
focused	O
on	O
securing	O
continued	O
assistance	O
for	O
rebuilding	O
the	O
economy	O
,	O
infrastructure	O
,	O
and	O
military	O
of	O
the	O
country	O
.	O
Many	O
Afghan	O
academics	O
studied	O
in	O
Germany	LOCATION
,	O
many	O
more	O
sought	O
refuge	O
in	O
Germany	LOCATION
during	O
the	O
years	O
of	O
civil	O
war	O
.	O
Germany	LOCATION
remains	O
one	O
of	O
the	O
most	O
significant	O
donors	O
of	O
foreign	O
aid	O
and	O
partners	O
in	O
the	O
rebuilding	O
of	O
Afghanistan	LOCATION
.	O
The	O
Bonn	LOCATION
agreement	O
for	O
the	O
post	O
Taliban	O
governance	O
of	O
Afghanistan	LOCATION
was	O
debated	O
and	O
signed	O
in	O
the	O
former	O
seat	O
of	O
government	O
of	O
Western	LOCATION
Germany	LOCATION
.	O
India	LOCATION
has	O
traditionally	O
enjoyed	O
good	O
relations	O
with	O
the	O
Afghan	O
government	O
.	O
President	O
Hamid	PERSON
Karzai	PERSON
graduated	O
from	O
a	O
university	O
in	O
India	LOCATION
.	O
It	O
also	O
supported	O
the	O
Afghan	O
Northern	O
Alliance	O
"	O
unofficially	O
"	O
against	O
the	O
Taliban	O
.	O
Relations	O
deteriorated	O
after	O
the	O
Taliban	O
took	O
power	O
.	O
During	O
the	O
course	O
of	O
the	O
hijack	O
of	O
Indian	O
Airlines	O
Flight	O
814	O
,	O
the	O
Taliban	O
requested	O
recognition	O
by	O
India	LOCATION
in	O
exchange	O
for	O
help	O
in	O
negotiations	O
.	O
The	O
request	O
was	O
not	O
acted	O
upon	O
by	O
the	O
Indian	O
government	O
.	O
After	O
the	O
fall	O
of	O
the	O
Taliban	O
,	O
India	LOCATION
resumed	O
previous	O
ties	O
.	O
India	LOCATION
has	O
donated	O
buses	O
,	O
aircraft	O
and	O
has	O
imparted	O
training	O
to	O
its	O
fledgling	O
police	O
force	O
.	O
The	O
project	O
is	O
being	O
carried	O
out	O
by	O
state-owned	O
Border	O
Roads	O
Organization	O
(	O
BRO	O
)	O
,	O
the	O
mission	O
statement	O
of	O
which	O
states	O
that	O
the	O
BRO	O
is	O
India	LOCATION
's	O
"	O
most	O
reputed	O
,	O
multifaceted	O
,	O
transnational	O
,	O
modern	O
construction	O
organization	O
committed	O
to	O
meeting	O
the	O
strategic	O
needs	O
of	O
the	O
armed	O
forces	O
.	O
"	O
The	O
other	O
major	O
language	O
is	O
Pashto	O
,	O
which	O
is	O
also	O
the	O
official	O
language	O
of	O
the	O
nation	O
and	O
shares	O
many	O
similar	O
words	O
.	O
Relations	O
between	O
the	O
two	O
nations	O
officially	O
began	O
after	O
Iran	LOCATION
was	O
created	O
as	O
a	O
state	O
in	O
1935	O
.	O
The	O
two	O
are	O
considered	O
by	O
some	O
to	O
be	O
part	O
of	O
"	O
Greater	O
Persia	O
"	O
,	O
which	O
refers	O
to	O
an	O
area	O
with	O
major	O
or	O
limited	O
Persian	O
influence	O
.	O
Despite	O
such	O
close	O
ties	O
,	O
Afghanistan	LOCATION
's	O
relations	O
with	O
Iran	LOCATION
have	O
fluctuated	O
over	O
the	O
years	O
,	O
with	O
periodic	O
disputes	O
over	O
the	O
water	O
rights	O
of	O
the	LOCATION
Helmand	LOCATION
River	LOCATION
as	O
the	O
main	O
issue	O
of	O
contention	O
.	O
Following	O
the	O
Soviet	O
invasion	O
of	O
Afghanistan	LOCATION
,	O
relations	O
deteriorated	O
.	O
Although	O
Iran	LOCATION
has	O
hosted	O
large	O
numbers	O
of	O
Afghan	O
refugees	O
since	O
the	O
early	O
1980s	O
,	O
it	O
is	O
seeking	O
to	O
repatriate	O
the	O
remaining	O
ones	O
back	O
to	O
Afghanistan	LOCATION
as	O
soon	O
as	O
possible	O
.	O
Following	O
this	O
incident	O
,	O
Iran	LOCATION
almost	O
went	O
to	O
war	O
with	O
the	O
Taliban	O
by	O
massing	O
up	O
troops	O
and	O
tanks	O
on	O
the	O
border	O
with	O
Afghanistan	LOCATION
.	O
As	O
a	O
response	O
,	O
the	O
Taliban	O
immediately	O
began	O
gathering	O
and	O
recruiting	O
large	O
number	O
of	O
men	O
along	O
the	O
border	O
with	O
Iran	LOCATION
.	O
On	O
the	O
other	O
hand	O
,	O
Iran	LOCATION
is	O
accused	O
by	O
the	LOCATION
United	LOCATION
States	LOCATION
of	O
helping	O
the	O
Taliban	O
insurgency	O
led	O
by	O
mullah	PERSON
Mohammad	PERSON
Omar	PERSON
.	O
This	O
means	O
that	O
Iran	LOCATION
is	O
playing	O
a	O
double	O
role	O
with	O
Afghanistan	LOCATION
,	O
as	O
a	O
helper	O
in	O
the	O
open	O
and	O
a	O
destroyer	O
secretly	O
.	O
Three	O
areas	O
(	O
NWFP	O
,	O
FATA	PERSON
and	O
Balochistan	LOCATION
)	O
have	O
long	O
complicated	O
Afghanistan	LOCATION
's	O
relations	O
with	O
Pakistan	LOCATION
.	O
From	O
those	O
living	O
in	O
what	O
later	O
became	O
Pakistan	LOCATION
in	O
1947	O
.	O
Pakistan	LOCATION
,	O
aided	O
by	O
UN	O
agencies	O
,	O
private	O
groups	O
,	O
and	O
many	O
friendly	O
countries	O
,	O
still	O
continues	O
to	O
provide	O
refuge	O
to	O
several	O
million	O
Afghans	O
by	O
nationality	O
.	O
Pakistan	LOCATION
developed	O
close	O
ties	O
to	O
the	O
Taliban	O
regime	O
since	O
1996	O
,	O
which	O
it	O
believed	O
would	O
offer	O
strategic	O
depth	O
in	O
any	O
future	O
conflict	O
with	O
India	LOCATION
,	O
and	O
extended	O
recognition	O
in	O
1997	O
.	O
Following	O
the	O
2001	O
invasion	O
and	O
overthrow	O
of	O
the	O
Taliban	O
,	O
Pakistan	LOCATION
recognized	O
the	O
transitional	O
administration	O
led	O
by	O
Hamid	PERSON
Karzai	PERSON
and	O
offered	O
significant	O
amounts	O
of	O
aid	O
for	O
reconstruction	O
.	O
It	O
also	O
continued	O
to	O
host	O
approximately	O
1.5	O
million	O
Afghan	O
refugees	O
and	O
facilitate	O
them	O
living	O
in	O
Pakistan	LOCATION
.	O
Relations	O
between	O
Afghanistan	LOCATION
and	O
Russia	LOCATION
were	O
contentious	O
after	O
the	O
Soviet	O
invasion	O
of	O
Afghanistan	LOCATION
in	O
1979	O
.	O
This	O
came	O
as	O
relations	O
between	O
Afghan	O
President	O
Karzai	PERSON
and	O
American	O
President	O
Obama	PERSON
reached	O
a	O
low	O
.	O
Relations	O
with	O
the	LOCATION
United	LOCATION
States	LOCATION
can	O
be	O
traced	O
back	O
to	O
over	O
150	O
years	O
,	O
to	O
the	O
early	O
1800s	O
,	O
when	O
the	O
first	O
recorded	O
person	O
from	O
America	LOCATION
was	O
visiting	O
Afghanistan	LOCATION
.	O
In	O
the	O
1940s	O
,	O
the	LOCATION
United	LOCATION
States	LOCATION
established	O
its	O
first	O
official	O
embassy	O
in	O
Kabul	LOCATION
.	O
The	O
first	O
official	O
Afghanistan	LOCATION
Ambassador	O
to	O
the	LOCATION
United	LOCATION
States	LOCATION
was	O
Habibullah	PERSON
Khan	PERSON
Tarzi	PERSON
who	O
served	O
from	O
1948	O
to	O
1953	O
.	O
Since	O
the	O
1950s	O
the	O
U.S.	LOCATION
extended	O
an	O
economic	O
assistance	O
program	O
focused	O
on	O
the	O
development	O
of	O
Afghanistan	LOCATION
's	O
physical	O
infrastructure	O
which	O
included	O
roads	O
,	O
dams	O
,	O
and	O
power	O
plants	O
.	O
Later	O
,	O
U.S.	LOCATION
aid	O
shifted	O
from	O
infrastructure	O
projects	O
to	O
technical	O
assistance	O
programs	O
to	O
help	O
develop	O
the	O
skills	O
needed	O
to	O
build	O
a	O
modern	O
economy	O
.	O
Dwight	PERSON
D.	PERSON
Eisenhower	PERSON
visited	O
Kabul	LOCATION
in	O
December	O
1959	O
,	O
becoming	O
the	O
first	O
U.S.	LOCATION
President	O
to	O
travel	O
to	O
Afghanistan	LOCATION
.	O
In	O
February	O
1979	O
,	O
U.S.	LOCATION
Ambassador	O
Adolph	PERSON
"	PERSON
Spike	PERSON
"	PERSON
Dubs	PERSON
was	O
murdered	O
in	O
Kabul	LOCATION
after	O
security	O
forces	O
burst	O
in	O
on	O
his	O
kidnappers	O
.	O
The	O
U.S.	LOCATION
then	O
reduced	O
bilateral	O
assistance	O
and	O
terminated	O
a	O
small	O
military	O
training	O
program	O
.	O
All	O
remaining	O
assistance	O
agreements	O
were	O
ended	O
after	O
the	O
Soviet	O
invasion	O
of	O
Afghanistan	LOCATION
.	O
Following	O
the	O
Soviet	O
invasion	O
,	O
the	LOCATION
United	LOCATION
States	LOCATION
supported	O
diplomatic	O
efforts	O
to	O
achieve	O
a	O
Soviet	O
withdrawal	O
.	O
In	O
addition	O
,	O
generous	O
U.S.	LOCATION
contributions	O
to	O
the	O
refugee	O
program	O
in	O
Pakistan	LOCATION
played	O
a	O
major	O
part	O
in	O
efforts	O
to	O
assist	O
Afghans	O
in	O
need	O
.	O
U.S.	LOCATION
efforts	O
also	O
included	O
helping	O
Afghans	O
living	O
inside	O
Afghanistan	LOCATION
.	O
This	O
cross-border	O
humanitarian	O
assistance	O
program	O
aimed	O
at	O
increasing	O
Afghan	O
self-sufficiency	O
and	O
helping	O
Afghans	O
resist	O
Soviet	O
attempts	O
to	O
drive	O
civilians	O
out	O
of	O
the	O
rebel-dominated	O
countryside	O
.	O
Following	O
the	O
September	O
11	O
attacks	O
,	O
the	LOCATION
United	LOCATION
States	LOCATION
launched	O
an	O
attack	O
on	O
the	O
Taliban	O
government	O
as	O
part	O
of	O
Operation	O
Enduring	O
Freedom	O
.	O
Following	O
the	O
overthrow	O
of	O
the	O
Taliban	O
,	O
the	O
U.S.	LOCATION
supported	O
the	O
new	O
government	O
of	O
Afghanistan	LOCATION
and	O
continues	O
to	O
station	O
thousands	O
of	O
U.S.	LOCATION
troops	O
in	O
the	O
country	O
.	O
The	LOCATION
United	LOCATION
States	LOCATION
is	O
also	O
the	O
leading	O
nation	O
in	O
the	O
rebuilding	O
or	O
reconstruction	O
of	O
Afghanistan	LOCATION
.	O
In	O
2005	O
,	O
the	O
U.S.	LOCATION
and	O
Afghanistan	LOCATION
signed	O
a	O
strategic	O
partnership	O
agreement	O
committing	O
both	O
nations	O
to	O
a	O
long-term	O
relationship	O
.	O
U.S.	LOCATION
President	O
George	PERSON
W.	PERSON
Bush	PERSON
and	O
First	O
Lady	O
Laura	PERSON
Bush	PERSON
made	O
a	O
surprise	O
visit	O
to	O
Afghanistan	LOCATION
on	O
March	O
1	O
,	O
2006	O
.	O
The	O
UNDP	O
and	O
associated	O
agencies	O
have	O
undertaken	O
a	O
limited	O
number	O
of	O
development	O
projects	O
.	O
Throughout	O
the	O
late	O
1990s	O
,	O
2000	O
,	O
and	O
2001	O
,	O
the	O
UN	O
unsuccessfully	O
strived	O
to	O
promote	O
a	O
peaceful	O
settlement	O
between	O
the	O
Afghan	O
factions	O
as	O
well	O
as	O
provide	O
humanitarian	O
aid	O
,	O
this	O
despite	O
increasing	O
Taliban	O
restrictions	O
upon	O
UN	O
personnel	O
and	O
agencies	O
.	O
