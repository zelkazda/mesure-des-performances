His	O
most	O
important	O
work	O
,	O
The	O
History	O
of	O
the	O
Decline	O
and	O
Fall	O
of	O
the	O
Roman	O
Empire	O
,	O
was	O
published	O
in	O
six	O
volumes	O
between	O
1776	O
and	O
1788	O
.	O
The	O
Decline	O
and	O
Fall	O
is	O
known	O
for	O
the	O
quality	O
and	O
irony	O
of	O
its	O
prose	O
,	O
its	O
use	O
of	O
primary	O
sources	O
,	O
and	O
its	O
open	O
denigration	O
of	O
organised	O
religion	O
.	O
He	O
was	O
further	O
"	O
corrupted	O
"	O
by	O
the	O
'	O
free	O
thinking	O
'	O
deism	O
of	O
the	O
playwright/poet	O
couple	O
David	PERSON
and	O
Lucy	PERSON
Mallet	PERSON
;	O
and	O
finally	O
Gibbon	PERSON
's	O
father	O
,	O
already	O
"	O
in	O
despair,	O
"	O
had	O
had	O
enough	O
.	O
It	O
was	O
here	O
that	O
he	O
made	O
one	O
of	O
his	O
life	O
's	O
two	O
great	O
friendships	O
,	O
that	O
of	O
Jacques	PERSON
Georges	PERSON
Deyverdun	PERSON
;	O
the	O
other	O
being	O
John	PERSON
Baker	PERSON
Holroyd	PERSON
(	O
later	O
Lord	O
Sheffield	PERSON
)	O
.	O
Gibbon	PERSON
returned	O
to	O
England	LOCATION
in	O
August	O
1758	O
to	O
face	O
his	O
father	O
.	O
Gibbon	PERSON
put	O
it	O
this	O
way	O
:	O
"	O
After	O
a	O
painful	O
struggle	O
I	O
yielded	O
to	O
my	O
fate	O
:	O
I	O
sighed	O
as	O
a	O
lover	O
,	O
I	O
obeyed	O
as	O
a	O
son	O
.	O
"	O
The	O
following	O
year	O
he	O
embarked	O
on	O
the	O
Grand	O
Tour	O
,	O
which	O
included	O
a	O
visit	O
to	O
Rome	LOCATION
.	O
He	O
became	O
the	O
archetypal	O
back-bencher	O
,	O
benignly	O
"	O
mute	O
"	O
and	O
"	O
indifferent,	O
"	O
his	O
support	O
of	O
the	O
Whig	O
ministry	O
routinely	O
automatic	O
.	O
Gibbon	PERSON
's	O
indolence	O
in	O
that	O
position	O
,	O
perhaps	O
fully	O
intentional	O
,	O
subtracted	O
little	O
from	O
the	O
progress	O
of	O
his	O
writing	O
.	O
After	O
several	O
rewrites	O
,	O
with	O
Gibbon	PERSON
"	O
often	O
tempted	O
to	O
throw	O
away	O
the	O
labours	O
of	O
seven	O
years,	O
"	O
the	O
first	O
volume	O
of	O
what	O
would	O
become	O
his	O
life	O
's	O
major	O
achievement	O
,	O
The	O
History	O
of	O
the	O
Decline	O
and	O
Fall	O
of	O
the	O
Roman	O
Empire	O
,	O
was	O
published	O
on	O
February	O
17	O
,	O
1776	O
.	O
Through	O
1777	O
,	O
the	O
reading	O
public	O
eagerly	O
consumed	O
three	O
editions	O
for	O
which	O
Gibbon	PERSON
was	O
rewarded	O
handsomely	O
:	O
two-thirds	O
of	O
the	O
profits	O
amounting	O
to	O
approx.	O
£	O
1000	O
.	O
Biographer	O
Leslie	PERSON
Stephen	PERSON
wrote	O
that	O
thereafter	O
,	O
"	O
His	O
fame	O
was	O
as	O
rapid	O
as	O
it	O
has	O
been	O
lasting	O
.	O
"	O
And	O
as	O
regards	O
this	O
first	O
volume	O
,	O
"	O
Some	O
warm	O
praise	O
from	O
David	PERSON
Hume	PERSON
overpaid	O
the	O
labour	O
of	O
ten	O
years	O
.	O
"	O
Smith	PERSON
remarked	O
that	O
Gibbon	PERSON
's	O
triumph	O
had	O
positioned	O
him	O
"	O
at	O
the	O
very	O
head	O
of	O
literary	O
tribe	O
.	O
"	O
The	O
years	O
following	O
Gibbon	PERSON
's	O
completion	O
of	O
The	O
History	O
were	O
filled	O
largely	O
with	O
sorrow	O
and	O
increasing	O
physical	O
discomfort	O
.	O
He	O
had	O
returned	O
to	O
London	LOCATION
in	O
late	O
1787	O
to	O
oversee	O
the	O
publication	O
process	O
alongside	O
Lord	PERSON
Sheffield	PERSON
,	O
John	PERSON
Baker-Holroyd	PERSON
.	O
Gibbon	PERSON
is	O
believed	O
to	O
have	O
suffered	O
from	O
hydrocele	O
testis	O
,	O
a	O
condition	O
which	O
causes	O
the	O
scrotum	O
to	O
swell	O
with	O
fluid	O
in	O
a	O
compartment	O
overlying	O
either	O
testicle	O
.	O
In	O
an	O
age	O
when	O
close-fitting	O
clothes	O
were	O
fashionable	O
,	O
his	O
condition	O
led	O
to	O
a	O
chronic	O
and	O
disfiguring	O
inflammation	O
that	O
left	O
Gibbon	PERSON
a	O
lonely	O
figure	O
.	O
More	O
specifically	O
,	O
Gibbon	PERSON
's	O
blasphemous	O
chapters	O
excoriated	O
the	O
church	O
for	O
"	O
supplanting	O
in	O
an	O
unnecessarily	O
destructive	O
way	O
the	O
great	O
culture	O
that	O
preceded	O
it	O
"	O
and	O
for	O
"	O
the	O
outrage	O
of	O
[	O
practicing	O
]	O
religious	O
intolerance	O
and	O
warfare	O
"	O
.	O
Gibbon	PERSON
,	O
though	O
assumed	O
to	O
be	O
entirely	O
anti-religion	O
,	O
was	O
actually	O
supportive	O
to	O
some	O
extent	O
,	O
insofar	O
as	O
it	O
did	O
not	O
obscure	O
his	O
true	O
endeavour	O
--	O
a	O
history	O
that	O
was	O
not	O
influenced	O
and	O
swayed	O
by	O
official	O
church	O
doctrine	O
.	O
Gibbon	PERSON
,	O
in	O
letters	O
to	O
Holroyd	PERSON
and	O
others	O
,	O
expected	O
some	O
type	O
of	O
church-inspired	O
backlash	O
,	O
but	O
the	O
utter	O
harshness	O
of	O
the	O
ensuing	O
torrents	O
far	O
exceeded	O
anything	O
he	O
or	O
his	O
friends	O
could	O
possibly	O
have	O
anticipated	O
.	O
However	O
,	O
politically	O
,	O
he	O
aligned	O
himself	O
with	O
the	O
conservative	O
Edmund	PERSON
Burke	PERSON
's	PERSON
rejection	O
of	O
the	O
democratic	O
movements	O
of	O
the	O
time	O
as	O
well	O
as	O
with	O
Burke	PERSON
's	O
dismissal	O
of	O
the	O
"	O
rights	O
of	O
man	O
.	O
"	O
Gibbon	PERSON
's	O
work	O
has	O
been	O
praised	O
for	O
its	O
style	O
,	O
his	O
piquant	O
epigrams	O
and	O
its	O
effective	O
irony	O
.	O
Winston	PERSON
Churchill	PERSON
memorably	O
noted	O
,	O
"	O
I	O
set	O
out	O
upon	O
...	O
Gibbon	PERSON
's	O
Decline	O
and	O
Fall	O
of	O
the	O
Roman	O
Empire	O
[	O
and	O
]	O
was	O
immediately	O
dominated	O
both	O
by	O
the	O
story	O
and	O
the	O
style	O
.	O
...	O
I	O
devoured	O
Gibbon	PERSON
.	O
Churchill	PERSON
modelled	O
much	O
of	O
his	O
own	O
literary	O
style	O
on	O
Gibbon	PERSON
's	O
.	O
Like	O
Gibbon	PERSON
,	O
he	O
dedicated	O
himself	O
to	O
producing	O
a	O
"	O
vivid	O
historical	O
narrative	O
,	O
ranging	O
widely	O
over	O
period	O
and	O
place	O
and	O
enriched	O
by	O
analysis	O
and	O
reflection	O
.	O
"	O
Unusually	O
for	O
the	O
18th	O
century	O
,	O
Gibbon	PERSON
was	O
never	O
content	O
with	O
secondhand	O
accounts	O
when	O
the	O
primary	O
sources	O
were	O
accessible	O
(	O
though	O
most	O
of	O
these	O
were	O
drawn	O
from	O
well-known	O
printed	O
editions	O
)	O
.	O
In	O
this	O
insistence	O
upon	O
the	O
importance	O
of	O
primary	O
sources	O
,	O
Gibbon	PERSON
is	O
considered	O
by	O
many	O
to	O
be	O
one	O
of	O
the	O
first	O
modern	O
historians	O
:	O
The	O
subject	O
of	O
Gibbon	PERSON
's	O
writing	O
as	O
well	O
as	O
his	O
ideas	O
and	O
style	O
have	O
influenced	O
other	O
writers	O
.	O
Besides	O
his	O
influence	O
on	O
Churchill	PERSON
,	O
Gibbon	PERSON
was	O
also	O
a	O
model	O
for	O
Isaac	PERSON
Asimov	PERSON
in	O
his	O
writing	O
of	O
The	O
Foundation	O
Trilogy	O
,	O
which	O
he	O
said	O
involved	O
"	O
a	O
little	O
bit	O
of	O
cribbin	O
'	O
from	O
the	O
works	O
of	O
Edward	PERSON
Gibbon	PERSON
"	O
.	O
Evelyn	PERSON
Waugh	PERSON
admired	O
Gibbon	PERSON
's	O
style	O
but	O
not	O
his	O
secular	O
viewpoint	O
.	O
