Douglas	PERSON
Engelbart	PERSON
introduced	O
the	O
chorded	O
keyset	O
as	O
a	O
computer	O
interface	O
in	O
1968	O
at	O
what	O
is	O
often	O
called	O
"	O
The	O
Mother	O
of	O
All	O
Demos	O
"	O
.	O
In	O
Engelbart	PERSON
's	O
original	O
mapping	O
,	O
he	O
used	O
five	O
keys	O
:	O
1,2,4,8,16	O
.	O
Since	O
Engelbart	PERSON
introduced	O
the	O
keyset	O
,	O
several	O
different	O
designs	O
have	O
been	O
developed	O
based	O
on	O
similar	O
concepts	O
.	O
Practical	O
devices	O
generally	O
use	O
simpler	O
chords	O
for	O
common	O
characters	O
,	O
or	O
may	O
have	O
ways	O
to	O
make	O
it	O
easier	O
to	O
remember	O
the	O
chords	O
(	O
e.g.	O
,	O
Microwriter	PERSON
)	O
,	O
but	O
the	O
same	O
principles	O
apply	O
.	O
The	O
earliest	O
known	O
chord	O
keyboard	O
was	O
part	O
of	O
the	O
"	O
five-needle	O
"	O
telegraph	O
operator	O
station	O
,	O
designed	O
by	O
Wheatstone	O
and	O
Cooke	O
in	O
1836	O
,	O
in	O
which	O
any	O
two	O
of	O
the	O
five	O
needles	O
could	O
point	O
left	O
or	O
right	O
to	O
indicate	O
letters	O
on	O
a	O
grid	O
.	O
Braille	O
(	O
a	O
writing	O
system	O
for	O
the	O
blind	O
)	O
uses	O
either	O
6	O
or	O
8	O
tactile	O
'	O
points	O
'	O
from	O
which	O
all	O
letters	O
and	O
numbers	O
are	O
formed	O
.	O
When	O
Louis	PERSON
Braille	PERSON
invented	O
it	O
,	O
it	O
was	O
produced	O
with	O
a	O
needle	O
holing	O
successively	O
all	O
needed	O
points	O
in	O
a	O
cardboard	O
sheet	O
.	O
The	O
Perkins	O
Brailler	O
,	O
first	O
manufactured	O
in	O
1951	O
,	O
uses	O
a	O
6-key	O
chord	O
keyboard	O
(	O
plus	O
a	O
spacebar	O
)	O
to	O
produce	O
braille	O
output	O
,	O
and	O
has	O
been	O
very	O
successful	O
as	O
a	O
mass	O
market	O
affordable	O
product	O
.	O
Researchers	O
at	O
IBM	O
investigated	O
chord	O
keyboards	O
for	O
both	O
typewriters	O
and	O
computer	O
data	O
entry	O
as	O
early	O
as	O
1959	O
,	O
with	O
the	O
idea	O
that	O
it	O
might	O
be	O
faster	O
than	O
touch-typing	O
if	O
some	O
chords	O
were	O
used	O
to	O
enter	O
whole	O
words	O
or	O
parts	O
of	O
words	O
.	O
(	O
"	O
keyset	O
"	O
)	O
Engelbart	PERSON
uses	O
the	O
keyset	O
and	O
the	O
mouse	O
with	O
his	O
right	O
.	O
To	O
type	O
a	O
command	O
Engelbart	PERSON
presses	O
on	O
of	O
the	O
three	O
buttons	O
of	O
the	O
mouse	O
.	O
Braille	O
comes	O
closest	O
,	O
as	O
it	O
has	O
been	O
extended	O
to	O
eight	O
bits	O
.	O
It	O
is	O
proposed	O
for	O
the	O
hand	O
which	O
does	O
not	O
hold	O
the	O
mouse	O
,	O
in	O
an	O
exact	O
continuation	O
of	O
Engelbart	PERSON
's	O
vision	O
.	O
