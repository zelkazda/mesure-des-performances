The	O
object	O
of	O
the	O
prophet	O
is	O
generally	O
urging	O
the	O
people	O
to	O
proceed	O
with	O
the	O
rebuilding	O
of	O
the	O
second	O
Jerusalem	LOCATION
temple	O
in	O
521	O
BCE	O
after	O
the	O
return	O
of	O
the	O
deportees	O
.	O
Haggai	PERSON
attributes	O
a	O
recent	O
drought	O
to	O
the	O
peoples	O
'	O
refusal	O
to	O
rebuild	O
the	O
temple	O
,	O
which	O
he	O
sees	O
as	O
key	O
to	O
Jerusalem	LOCATION
's	O
glory	O
.	O
The	O
book	O
ends	O
with	O
the	O
prediction	O
of	O
the	O
downfall	O
of	O
kingdoms	O
,	O
with	O
one	O
Zerubbabel	PERSON
,	O
governor	O
of	O
Judah	LOCATION
,	O
as	O
the	O
Lord	O
's	O
chosen	O
leader	O
.	O
These	O
discourses	O
are	O
referred	O
to	O
in	O
Ezra	PERSON
5:1	PERSON
and	O
6:14	PERSON
.	O
(	O
Compare	O
Haggai	O
2:7	O
,	O
8	O
and	O
22	O
)	O
(	O
Ezra	PERSON
6:15	PERSON
)	O
