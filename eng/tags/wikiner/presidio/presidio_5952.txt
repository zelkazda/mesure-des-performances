In	O
academic	O
literature	O
,	O
the	O
practice	O
was	O
first	O
documented	O
by	O
Judith	PERSON
Donath	PERSON
(	O
1999	O
)	O
.	O
Donath	PERSON
's	O
paper	O
outlines	O
the	O
ambiguity	O
of	O
identity	O
in	O
a	O
disembodied	O
"	O
virtual	O
community	O
"	O
such	O
as	O
Usenet	O
:	O
Donath	PERSON
provides	O
a	O
concise	O
overview	O
of	O
identity	O
deception	O
games	O
which	O
trade	O
on	O
the	O
confusion	O
between	O
physical	O
and	O
epistemic	O
community	O
:	O
For	O
example	O
,	O
a	O
New	O
York	O
Times	O
article	O
discussed	O
troll	O
activity	O
at	O
the	O
/b/	O
board	O
on	O
4chan	O
and	O
at	O
Encyclopedia	O
Dramatica	O
,	O
which	O
it	O
described	O
as	O
"	O
an	O
online	O
compendium	O
of	O
troll	O
humor	O
and	O
troll	O
lore	O
"	O
.	O
These	O
websites	O
include	O
but	O
are	O
not	O
limited	O
to	O
;	O
Myspace	O
,	O
Facebook	O
,	O
Youtube	O
,	O
Stickam	PERSON
,	O
and	O
Chatroulette	LOCATION
.	O
On	O
March	O
31	O
,	O
2010	O
,	O
the	O
Today	O
Show	O
ran	O
a	O
segment	O
detailing	O
the	O
deaths	O
of	O
three	O
separate	O
adolescent	O
girls	O
and	O
trolls	O
'	O
subsequent	O
reactions	O
to	O
their	O
deaths	O
.	O
In	O
the	O
wake	O
of	O
these	O
events	O
,	O
Facebook	O
responded	O
by	O
strongly	O
urging	O
administrators	O
to	O
be	O
aware	O
of	O
ways	O
to	O
ban	O
users	O
and	O
remove	O
inappropriate	O
content	O
from	O
Facebook	O
pages	O
.	O
The	O
case	O
of	O
Zeran	O
v.	O
America	O
Online	O
,	O
Inc.	O
resulted	O
primarily	O
from	O
trolling	O
.	O
