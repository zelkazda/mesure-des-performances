The	O
State	O
of	O
Israel	O
had	O
population	O
of	O
approximately	O
7,503,800	O
inhabitants	O
as	O
of	O
December	O
2009	O
.	O
According	O
to	O
Israel	LOCATION
's	O
Central	O
Bureau	O
of	O
Statistics	O
,	O
in	O
2008	O
,	O
of	O
Israel	LOCATION
's	O
7.3	O
million	O
people	O
,	O
75.6	O
%	O
were	O
Jews	O
of	O
any	O
background	O
.	O
In	O
2006	O
,	O
the	O
official	O
number	O
of	O
Arab	O
residents	O
in	O
Israel	LOCATION
was	O
1,413,500	O
people	O
,	O
about	O
20	O
%	O
of	O
Israel	LOCATION
's	O
population	O
.	O
As	O
of	O
2008	O
,	O
Arab	O
citizens	O
of	O
Israel	LOCATION
comprise	O
just	O
over	O
20	O
%	O
of	O
the	O
country	O
's	O
total	O
population	O
.	O
The	O
Arab	O
citizens	O
of	O
Israel	LOCATION
include	O
also	O
the	O
Druze	O
who	O
were	O
numbered	O
at	O
an	O
estimated	O
117,500	O
at	O
the	O
end	O
of	O
2006	O
.	O
The	O
African	O
Hebrew	O
Israelite	O
Nation	O
of	O
Jerusalem	O
is	O
a	O
small	O
spiritual	O
group	O
whose	O
members	O
believe	O
they	O
are	O
descended	O
from	O
the	O
Ten	O
Lost	O
Tribes	O
of	O
Israel	O
.	O
With	O
a	O
population	O
of	O
over	O
5,000	O
,	O
most	O
members	O
live	O
in	O
their	O
own	O
community	O
in	O
Dimona	LOCATION
,	O
Israel	LOCATION
,	O
with	O
additional	O
families	O
in	O
Arad	LOCATION
,	O
Mitzpe	PERSON
Ramon	PERSON
,	O
and	O
the	O
Tiberias	LOCATION
area	O
.	O
At	O
least	O
some	O
of	O
them	O
consider	O
themselves	O
to	O
be	O
Jewish	O
,	O
but	O
mainstream	O
Judaism	O
does	O
not	O
consider	O
them	O
to	O
be	O
Jewish	O
.	O
In	O
Israel	LOCATION
,	O
there	O
are	O
also	O
a	O
few	O
thousand	O
Circassians	O
,	O
living	O
mostly	O
in	O
Kfar	LOCATION
Kama	O
(	O
2,000	O
)	O
and	O
Reyhaniye	O
(	O
1,000	O
)	O
.	O
These	O
two	O
villages	O
were	O
a	O
part	O
of	O
a	O
greater	O
group	O
of	O
Circassian	O
villages	O
around	O
the	LOCATION
Golan	LOCATION
Heights	LOCATION
.	O
The	O
Circassians	O
in	O
Israel	LOCATION
enjoy	O
,	O
like	O
Druzes	O
,	O
a	O
status	O
aparte	O
.	O
Male	O
Circassians	O
(	O
at	O
their	O
leader	O
's	O
request	O
)	O
are	O
mandated	O
for	O
military	O
service	O
,	O
while	O
females	O
are	O
not	O
.	O
2007	O
population	O
estimates	O
show	O
that	O
712	O
Samaritans	O
live	O
half	O
in	O
Holon	LOCATION
,	O
Israel	LOCATION
and	O
half	O
at	O
Mount	O
Gerizim	O
in	O
the	LOCATION
West	LOCATION
Bank	LOCATION
.	O
The	O
growth	O
rate	O
of	O
the	O
Arab	O
population	O
in	O
Israel	LOCATION
is	O
2.6	O
%	O
,	O
while	O
the	O
growth	O
rate	O
of	O
the	O
Jewish	O
population	O
in	O
Israel	LOCATION
is	O
1.7	O
%	O
.	O
The	O
growth	O
rate	O
of	O
the	O
both	O
Jewish	O
and	O
Arab	O
population	O
has	O
slowed	O
from	O
3.8	O
%	O
in	O
1999	O
to	O
2.6	O
%	O
in	O
2008	O
for	O
Arab	O
and	O
2.7	O
%	O
to	O
1.7	O
%	O
for	O
the	O
Jewish	O
population	O
.	O
By	O
point	O
of	O
comparison	O
,	O
in	O
2008	O
there	O
was	O
a	O
slowly	O
rising	O
birthrate	O
of	O
2.88	O
children	O
among	O
the	O
Jewish	O
population	O
.	O
Of	O
this	O
33,568	O
were	O
Jews	O
(	O
34,031	O
in	O
2005	O
&	O
33,421	O
in	O
2000	O
)	O
.	O
360	O
were	O
Druze	O
(	O
363	O
in	O
2005	O
&	O
305	O
in	O
2000	O
)	O
.	O
There	O
were	O
a	O
total	O
of	O
19,269	O
immigrants	O
in	O
2006	O
:	O
7,472	O
from	O
the	LOCATION
Former	LOCATION
Soviet	LOCATION
Union	LOCATION
,	O
3,595	O
from	O
Ethiopia	LOCATION
,	O
2,411	O
from	O
France	LOCATION
,	O
2,159	O
from	O
the	LOCATION
United	LOCATION
States	LOCATION
,	O
594	O
from	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
,	O
304	O
from	O
India	LOCATION
,	O
293	O
from	O
Argentina	LOCATION
,	O
232	O
from	O
Brazil	LOCATION
,	O
228	O
from	O
Canada	LOCATION
,	O
142	O
from	O
Colombia	LOCATION
,	O
134	O
from	O
Venezuela	LOCATION
,	O
114	O
from	O
South	LOCATION
Africa	LOCATION
,	O
112	O
from	O
Germany	LOCATION
,	O
91	O
from	O
Belgium	LOCATION
,	O
91	O
from	O
Central	LOCATION
America	LOCATION
,	O
85	O
from	O
Switzerland	LOCATION
,	O
73	O
from	O
Uruguay	LOCATION
,	O
72	O
from	O
Mexico	LOCATION
,	O
66	O
from	O
Oceania	LOCATION
,	O
63	O
from	O
Hungary	LOCATION
,	O
61	O
from	O
Chile	LOCATION
,	O
50	O
from	O
Romania	LOCATION
and	O
50	O
from	O
the	O
Netherlands	LOCATION
.	O
In	O
Israel	LOCATION
,	O
the	O
total	O
fertility	O
rate	O
(	O
TFR	O
)	O
is	O
2.96	O
children	O
born	O
per	O
woman	O
.	O
Due	O
to	O
its	O
immigrant	O
nature	O
,	O
Israel	LOCATION
is	O
one	O
of	O
the	O
most	O
multicultural	O
and	O
multilingual	O
societies	O
in	O
the	O
world	O
.	O
Those	O
in	O
favor	O
of	O
the	O
law	O
say	O
the	O
law	O
not	O
only	O
limits	O
the	O
possibility	O
of	O
the	O
entrance	O
of	O
terrorists	O
into	O
Israel	LOCATION
,	O
but	O
,	O
as	O
Ze'ev	PERSON
Boim	PERSON
asserts	O
,	O
allows	O
Israel	LOCATION
"	O
to	O
maintain	O
the	O
state	O
's	O
democratic	O
nature	O
,	O
but	O
also	O
its	O
Jewish	O
nature	O
"	O
(	O
i.e.	O
its	O
Jewish	O
demographic	O
majority	O
)	O
.	O
