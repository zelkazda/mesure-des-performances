Franz	PERSON
Kafka	PERSON
(	O
German	O
pronunciation	O
:	O
[	O
ˈfʁants	PERSON
ˈkafka	O
]	O
;	O
3	O
July	O
1883	O
--	O
3	O
June	O
1924	O
)	O
was	O
one	O
of	O
the	O
most	O
influential	O
novelists	O
of	O
the	O
20th	O
century	O
,	O
whose	O
works	O
after	O
his	O
death	O
came	O
to	O
be	O
regarded	O
as	O
one	O
of	O
the	O
major	O
achievements	O
of	O
world	O
literature	O
.	O
Kafka	PERSON
was	O
born	O
to	O
middle	O
class	O
German-speaking	O
Jewish	O
parents	O
in	O
Prague	LOCATION
,	O
Bohemia	LOCATION
,	O
now	O
part	O
of	O
the	LOCATION
Czech	LOCATION
Republic	LOCATION
,	O
in	O
what	O
was	O
then	O
the	O
Austro-Hungarian	O
Empire	O
.	O
Kafka	PERSON
was	O
born	O
into	O
a	O
middle-class	O
Jewish	O
family	O
in	O
Prague	LOCATION
,	O
the	O
capital	O
of	O
Bohemia	LOCATION
.	O
After	O
working	O
as	O
a	O
traveling	O
sales	O
representative	O
,	O
he	O
established	O
himself	O
as	O
an	O
independent	O
retailer	O
of	O
men	O
's	O
and	O
women	O
's	O
fancy	O
goods	O
and	O
accessories	O
,	O
employing	O
up	O
to	O
15	O
people	O
and	O
using	O
a	O
jackdaw	O
(	O
kavka	O
in	O
Czech	O
)	O
as	O
his	O
business	O
logo	O
.	O
Franz	PERSON
was	O
the	O
eldest	O
of	O
six	O
children	O
.	O
Kafka	PERSON
's	O
first	O
language	O
was	O
German	O
,	O
but	O
he	O
was	O
also	O
fluent	O
in	O
Czech	O
.	O
Later	O
,	O
Kafka	PERSON
acquired	O
some	O
knowledge	O
of	O
French	O
language	O
and	O
culture	O
;	O
one	O
of	O
his	O
favorite	O
authors	O
was	O
Flaubert	PERSON
.	O
In	O
the	O
end	O
of	O
his	O
first	O
year	O
of	O
studies	O
,	O
he	O
met	O
Max	PERSON
Brod	PERSON
,	O
who	O
would	O
become	O
a	O
close	O
friend	O
of	O
his	O
throughout	O
his	O
life	O
,	O
together	O
with	O
the	O
journalist	O
Felix	PERSON
Weltsch	PERSON
,	O
who	O
also	O
studied	O
law	O
.	O
While	O
Kafka	PERSON
often	O
claimed	O
that	O
he	O
despised	O
the	O
job	O
,	O
he	O
was	O
a	O
diligent	O
and	O
capable	O
employee	O
.	O
In	O
parallel	O
,	O
Kafka	PERSON
was	O
also	O
committed	O
to	O
his	O
literary	O
work	O
.	O
During	O
that	O
period	O
,	O
he	O
also	O
found	O
interest	O
and	O
entertainment	O
in	O
the	O
performances	O
of	O
Yiddish	O
theatre	O
,	O
despite	O
the	O
misgivings	O
of	O
even	O
close	O
friends	O
such	O
as	O
Max	PERSON
Brod	PERSON
,	O
who	O
usually	O
supported	O
him	O
in	O
everything	O
else	O
.	O
Those	O
performances	O
also	O
served	O
as	O
a	O
starting	O
point	O
for	O
his	O
growing	O
relationship	O
with	O
Judaism	O
.	O
Kafka	PERSON
developed	O
an	O
intense	O
relationship	O
with	O
Czech	O
journalist	O
and	O
writer	O
Milena	PERSON
Jesenská	PERSON
.	O
She	O
became	O
his	O
lover	O
,	O
and	O
influenced	O
Kafka	PERSON
's	O
interest	O
in	O
the	O
Talmud	O
.	O
The	O
condition	O
of	O
Kafka	PERSON
's	O
throat	O
made	O
eating	O
too	O
painful	O
for	O
him	O
,	O
and	O
since	O
parenteral	O
nutrition	O
had	O
not	O
yet	O
been	O
developed	O
,	O
there	O
was	O
no	O
way	O
to	O
feed	O
him	O
.	O
"	O
Franz	PERSON
became	O
a	O
socialist	O
,	O
I	O
became	O
a	O
Zionist	O
in	O
1898	O
.	O
The	O
synthesis	O
of	O
Zionism	O
and	O
socialism	O
did	O
not	O
yet	O
exist	O
.	O
"	O
In	O
one	O
diary	O
entry	O
,	O
Kafka	PERSON
referenced	O
influential	O
anarchist	O
philosopher	O
Peter	PERSON
Kropotkin	PERSON
:	O
"	O
Do	O
n't	O
forget	O
Kropotkin	PERSON
!	O
"	O
Kafka	PERSON
was	O
not	O
formally	O
involved	O
in	O
Jewish	O
religious	O
life	O
,	O
but	O
he	O
showed	O
a	O
great	O
interest	O
in	O
Jewish	O
culture	O
and	O
spirituality	O
.	O
He	O
was	O
well	O
versed	O
in	O
Yiddish	O
literature	O
,	O
and	O
loved	O
Yiddish	O
theatre	O
.	O
"	O
It	O
seems	O
that	O
those	O
who	O
claim	O
that	O
there	O
was	O
such	O
a	O
connection	O
and	O
that	O
Zionism	O
played	O
a	O
central	O
role	O
in	O
his	O
life	O
and	O
literary	O
work	O
,	O
and	O
those	O
who	O
deny	O
the	O
connection	O
altogether	O
or	O
dismiss	O
its	O
importance	O
,	O
are	O
both	O
wrong	O
.	O
Pavel	PERSON
Eisner	PERSON
,	O
one	O
of	O
Kafka	PERSON
's	O
first	O
translators	O
,	O
interprets	O
the	O
classic	O
,	O
The	O
Trial	O
,	O
as	O
the	O
"	O
triple	O
dimension	O
of	O
Jewish	O
existence	O
in	O
Prague	LOCATION
is	O
embodied	O
in	O
Kafka	PERSON
's	O
The	O
Trial	O
:	O
his	O
protagonist	O
Josef	PERSON
K.	PERSON
is	O
(	O
symbolically	O
)	O
arrested	O
by	O
a	O
German	O
,	O
a	O
Czech	O
,	O
and	O
a	O
Jew	O
.	O
He	O
stands	O
for	O
the	O
"	O
guiltless	O
guilt	O
"	O
that	O
imbues	O
the	O
Jew	O
in	O
the	O
modern	O
world	O
,	O
although	O
there	O
is	O
no	O
evidence	O
that	O
he	O
himself	O
is	O
a	O
Jew	O
.	O
"	O
An	O
illustrious	O
example	O
is	O
Franz	PERSON
Kafka	PERSON
.	O
"	O
Kafka	PERSON
's	O
writing	O
attracted	O
little	O
attention	O
until	O
after	O
his	O
death	O
.	O
During	O
his	O
lifetime	O
,	O
he	O
published	O
only	O
a	O
few	O
short	O
stories	O
and	O
never	O
finished	O
any	O
of	O
his	O
novels	O
,	O
unless	O
The	O
Metamorphosis	O
is	O
considered	O
a	O
(	O
short	O
)	O
novel	O
.	O
(	O
His	O
lover	O
,	O
Dora	PERSON
Diamant	PERSON
,	O
also	O
ignored	O
his	O
wishes	O
,	O
secretly	O
keeping	O
up	O
to	O
20	O
notebooks	O
and	O
35	O
letters	O
until	O
they	O
were	O
confiscated	O
by	O
the	O
Gestapo	O
in	O
1933	O
.	O
An	O
ongoing	O
international	O
search	O
is	O
being	O
conducted	O
for	O
these	O
missing	O
Kafka	PERSON
papers	O
.	O
)	O
Kafka	PERSON
often	O
made	O
extensive	O
use	O
of	O
a	O
characteristic	O
peculiar	O
to	O
the	O
German	O
language	O
allowing	O
for	O
long	O
sentences	O
that	O
sometimes	O
can	O
span	O
an	O
entire	O
page	O
.	O
Kafka	PERSON
's	O
sentences	O
then	O
deliver	O
an	O
unexpected	O
impact	O
just	O
before	O
the	O
full	O
stop	O
--	O
that	O
being	O
the	O
finalizing	O
meaning	O
and	O
focus	O
.	O
Critics	O
have	O
interpreted	O
Kafka	PERSON
's	O
works	O
in	O
the	O
context	O
of	O
a	O
variety	O
of	O
literary	O
schools	O
,	O
such	O
as	O
modernism	O
,	O
magic	O
realism	O
,	O
and	O
so	O
on	O
.	O
Furthermore	O
,	O
an	O
isolated	O
reading	O
of	O
Kafka	PERSON
's	O
work	O
--	O
focusing	O
on	O
the	O
futility	O
of	O
his	O
characters	O
'	O
struggling	O
without	O
the	O
influence	O
of	O
any	O
studies	O
on	O
Kafka	PERSON
's	O
life	O
--	O
reveals	O
the	O
humor	O
of	O
Kafka	PERSON
.	O
Kafka	PERSON
's	O
work	O
,	O
in	O
this	O
sense	O
,	O
is	O
not	O
a	O
written	O
reflection	O
of	O
any	O
of	O
his	O
own	O
struggles	O
,	O
but	O
a	O
reflection	O
of	O
how	O
people	O
invent	O
struggles	O
.	O
Milan	PERSON
Kundera	PERSON
refers	O
to	O
the	O
essentially	O
surrealist	O
humour	O
of	O
Kafka	PERSON
as	O
a	O
main	O
predecessor	O
of	O
later	O
artists	O
such	O
as	O
Federico	PERSON
Fellini	PERSON
,	O
Gabriel	PERSON
García	PERSON
Márquez	PERSON
,	O
Carlos	PERSON
Fuentes	PERSON
and	O
Salman	PERSON
Rushdie	PERSON
.	O
For	O
García	PERSON
Márquez	PERSON
,	O
it	O
was	O
as	O
he	O
said	O
the	O
reading	O
of	O
Kafka	PERSON
's	O
The	O
Metamorphosis	O
that	O
showed	O
him	O
"	O
that	O
it	O
was	O
possible	O
to	O
write	O
in	O
a	O
different	O
way	O
.	O
"	O
Many	O
attempts	O
have	O
been	O
made	O
to	O
examine	O
Kafka	PERSON
's	O
legal	O
background	O
and	O
the	O
role	O
of	O
law	O
in	O
his	O
fiction	O
.	O
Mainstream	O
studies	O
of	O
Kafka	PERSON
's	O
works	O
normally	O
present	O
his	O
fiction	O
as	O
an	O
engagement	O
with	O
absurdity	O
,	O
a	O
critique	O
of	O
bureaucracy	O
or	O
a	O
search	O
for	O
redemption	O
,	O
failing	O
to	O
account	O
for	O
the	O
images	O
of	O
law	O
and	O
legality	O
which	O
constitute	O
an	O
important	O
part	O
of	O
"	O
the	O
horizon	O
of	O
meaning	O
"	O
in	O
his	O
fiction	O
.	O
Much	O
of	O
Kafka	PERSON
's	O
work	O
was	O
unfinished	O
,	O
or	O
prepared	O
for	O
publication	O
posthumously	O
by	O
Max	PERSON
Brod	PERSON
.	O
According	O
to	O
the	O
publisher	O
's	O
note	O
for	O
The	O
Castle	O
,	O
Malcolm	PERSON
Pasley	PERSON
was	O
able	O
to	O
get	O
most	O
of	O
Kafka	PERSON
's	O
original	O
handwritten	O
work	O
into	O
the	O
Oxford	O
Bodleian	O
Library	O
in	O
1961	O
.	O
There	O
is	O
another	O
Kafka	O
Project	O
based	O
at	O
San	O
Diego	O
State	O
University	O
,	O
which	O
began	O
in	O
1998	O
as	O
the	O
official	O
international	O
search	O
for	O
Kafka	PERSON
's	O
last	O
writings	O
.	O
In	O
2003	O
,	O
the	O
Kafka	O
Project	O
discovered	O
three	O
original	O
Kafka	PERSON
letters	O
,	O
written	O
in	O
1923	O
.	O
In	O
2008	O
,	O
academic	O
and	O
Kafka	PERSON
expert	O
James	PERSON
Hawes	PERSON
accused	O
scholars	O
of	O
suppressing	O
details	O
of	O
the	O
pornography	O
Kafka	PERSON
subscribed	O
to	O
(	O
published	O
by	O
the	O
same	O
man	O
who	O
was	O
Kafka	PERSON
's	O
own	O
first	O
publisher	O
)	O
in	O
order	O
to	O
preserve	O
his	O
image	O
as	O
a	O
quasi-saintly	O
"	O
outsider	O
"	O
.	O
Among	O
the	O
writings	O
was	O
a	O
short	O
story	O
by	O
Kafka	PERSON
.	O
There	O
are	O
two	O
primary	O
sources	O
for	O
the	O
translations	O
based	O
on	O
the	O
two	O
German	O
editions	O
.	O
Franz	PERSON
Kafka	PERSON
has	O
a	O
museum	O
dedicated	O
to	O
his	O
work	O
in	O
Prague	LOCATION
,	O
Czech	LOCATION
Republic	LOCATION
.	O
