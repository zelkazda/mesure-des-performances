Born	O
in	O
Florence	LOCATION
,	O
in	O
1540	O
,	O
after	O
the	O
death	O
of	O
his	O
father	O
,	O
he	O
was	O
brought	O
up	O
and	O
trained	O
in	O
art	O
by	O
a	O
close	O
friend	O
,	O
often	O
referred	O
to	O
as	O
his	O
'	O
uncle	O
'	O
,	O
the	O
mannerist	O
painter	O
Agnolo	PERSON
Bronzino	PERSON
,	O
whose	O
name	O
he	O
sometimes	O
assumed	O
in	O
his	O
pictures	O
.	O
It	O
can	O
be	O
said	O
of	O
late	O
phase	O
mannerist	O
painting	O
in	O
Florence	LOCATION
,	O
that	O
the	O
city	O
that	O
had	O
early	O
breathed	O
life	O
into	O
statuary	O
with	O
the	O
works	O
of	O
masters	O
like	O
Donatello	PERSON
and	O
Michelangelo	PERSON
,	O
was	O
still	O
so	O
awed	O
by	O
them	O
that	O
it	O
petrified	O
the	O
poses	O
of	O
figures	O
in	O
painting	O
.	O
Among	O
his	O
collaborators	O
was	O
Giovanni	PERSON
Maria	PERSON
Butteri	PERSON
and	O
his	O
main	O
pupil	O
was	O
Giovanni	PERSON
Bizzelli	PERSON
.	O
Allori	O
was	O
one	O
of	O
the	O
artists	O
,	O
working	O
under	O
Vasari	PERSON
,	O
included	O
in	O
the	O
decoration	O
of	O
the	O
Studiolo	O
of	O
Francesco	O
I	O
.	O
He	O
is	O
the	O
father	O
of	O
Cristofano	PERSON
Allori	PERSON
(	O
1577	O
--	O
1621	O
)	O
.	O
