Gibson	PERSON
's	O
first	O
production	O
electric	O
guitar	O
,	O
marketed	O
in	O
1936	O
,	O
was	O
the	O
ES-150	O
model	O
.	O
It	O
became	O
known	O
as	O
the	O
"	O
Charlie	PERSON
Christian	PERSON
"	O
pickup	O
(	O
named	O
for	O
the	O
great	O
jazz	O
guitarist	O
who	O
was	O
among	O
the	O
first	O
to	O
perform	O
with	O
the	O
ES-150	O
guitar	O
)	O
.	O
An	O
early	O
commercially	O
successful	O
solid-body	O
electric	O
guitar	O
was	O
the	O
Fender	O
Esquire	O
in	O
1950	O
.	O
Guitar	O
innovator	O
Les	PERSON
Paul	PERSON
experimented	O
with	O
microphones	O
attached	O
to	O
guitars	O
.	O
An	O
electrically	O
amplified	O
guitar	O
was	O
developed	O
by	O
George	PERSON
Beauchamp	PERSON
in	O
1931	O
.	O
The	O
earliest	O
documented	O
performance	O
with	O
an	O
electrically	O
amplified	O
guitar	O
was	O
in	O
1932	O
,	O
by	O
Gage	PERSON
Brewer	PERSON
.	O
The	O
company	O
Audiovox	O
built	O
and	O
may	O
have	O
offered	O
an	O
electric	O
solid-body	O
as	O
early	O
as	O
the	O
mid-1930s	O
.	O
An	O
example	O
of	O
this	O
model	O
,	O
featuring	O
a	O
guitar-shaped	O
body	O
of	O
a	O
single	O
sheet	O
of	O
plywood	O
affixed	O
to	O
a	O
wood	O
frame	O
,	O
can	O
be	O
seen	O
in	O
the	O
Experience	O
Music	O
Project	O
.	O
His	O
log	O
guitar	O
shares	O
nothing	O
in	O
design	O
or	O
hardware	O
with	O
the	O
solid	O
body	O
"	O
Les	PERSON
Paul	PERSON
"	O
model	O
sold	O
by	O
Gibson	PERSON
.	O
While	O
Gibson	PERSON
's	O
scale	O
has	O
often	O
claimed	O
to	O
be	O
24.75	O
"	O
,	O
it	O
has	O
varied	O
through	O
the	O
years	O
by	O
as	O
much	O
as	O
a	O
half	O
inch	O
.	O
Bolt-on	O
necks	O
were	O
pioneered	O
by	O
Leo	PERSON
Fender	PERSON
to	O
facilitate	O
easy	O
adjustment	O
and	O
replacement	O
of	O
the	O
guitar	O
neck	O
.	O
Some	O
instruments	O
,	O
notably	O
most	O
Gibson	PERSON
models	O
,	O
have	O
continued	O
to	O
use	O
set/glued	O
necks	O
.	O
Not	O
all	O
special	O
effects	O
are	O
electronic	O
;	O
in	O
1967	O
,	O
guitarist	O
Jimmy	PERSON
Page	PERSON
of	O
The	O
Yardbirds	O
and	O
Led	PERSON
Zeppelin	PERSON
created	O
unusual	O
,	O
psychedelic	O
sound	O
effects	O
by	O
playing	O
the	O
electric	O
guitar	O
using	O
a	O
violin	O
bow	O
with	O
a	O
delay	O
pedal	O
and	O
wah	O
pedal	O
In	O
2002	O
,	O
Gibson	PERSON
announced	O
the	O
first	O
digital	O
guitar	O
,	O
which	O
performs	O
analog-to-digital	O
conversion	O
internally	O
.	O
The	O
resulting	O
digital	O
signal	O
is	O
delivered	O
over	O
a	O
standard	O
Ethernet	O
cable	O
,	O
eliminating	O
cable-induced	O
line	O
noise	O
.	O
One	O
of	O
the	O
first	O
solid	O
body	O
guitars	O
was	O
invented	O
by	O
Les	PERSON
Paul	PERSON
.	O
Examples	O
of	O
string-through	O
bodies	O
on	O
guitars	O
include	O
the	O
Fender	O
Telecaster	PERSON
Thinline	PERSON
and	O
Telecaster	O
Deluxe	O
.	O
Grimes	O
'	O
guitar	O
omitted	O
the	O
bottom	O
two	O
strings	O
.	O
Deron	PERSON
Miller	PERSON
of	O
CKY	O
only	O
uses	O
four	O
strings	O
,	O
but	O
plays	O
a	O
six	O
string	O
guitar	O
with	O
the	O
two	O
highest	O
strings	O
removed	O
.	O
Jazz	O
guitarists	O
using	O
a	O
seven-string	O
include	O
veteran	O
jazz	O
guitarists	O
George	PERSON
Van	PERSON
Eps	PERSON
,	O
Bucky	PERSON
Pizzarelli	PERSON
and	O
his	O
son	O
John	PERSON
Pizzarelli	PERSON
.	O
Seven-string	O
electric	O
guitars	O
were	O
popularized	O
among	O
rock	O
players	O
in	O
the	O
1980s	O
by	O
Steve	PERSON
Vai	PERSON
.	O
These	O
models	O
were	O
based	O
on	O
Vai	PERSON
's	O
six	O
string	O
signature	O
series	O
,	O
the	O
Ibanez	O
Jem	O
.	O
Seven-string	O
guitars	O
experienced	O
a	O
resurgence	O
in	O
popularity	O
in	O
the	O
2000s	O
,	O
championed	O
by	O
Linkin	PERSON
Park	PERSON
,	O
Slayer	O
,	O
KoRn	O
,	O
Fear	O
Factory	O
,	O
Strapping	O
Young	O
Lad	O
,	O
Nevermore	O
,	O
and	O
other	O
hard	O
rock	O
/	O
metal	O
bands	O
.	O
One	O
is	O
played	O
by	O
Charlie	PERSON
Hunter	PERSON
.	O
Their	O
models	O
are	O
used	O
by	O
Trey	PERSON
Gunn	PERSON
(	O
ex	O
King	O
Crimson	O
)	O
who	O
has	O
his	O
own	O
signature	O
line	O
from	O
the	O
company	O
.	O
Jethro	PERSON
Tull	PERSON
's	PERSON
first	O
album	O
uses	O
a	O
nine-string	O
guitar	O
on	O
one	O
track	O
.	O
Bill	PERSON
Kelliher	PERSON
,	O
guitarist	O
for	O
the	O
heavy	O
metal	O
group	O
Mastodon	O
,	O
worked	O
with	O
First	O
Act	O
on	O
a	O
custom	O
mass-produced	O
nine-string	O
guitar	O
.	O
Lead	PERSON
Belly	PERSON
is	O
the	O
folk	O
artist	O
most	O
identified	O
with	O
the	O
twelve-string	O
guitar	O
,	O
usually	O
acoustic	O
with	O
a	O
pickup	O
.	O
George	PERSON
Harrison	PERSON
of	O
The	O
Beatles	O
and	O
Roger	PERSON
McGuinn	PERSON
of	O
The	O
Byrds	O
brought	O
the	O
electric	O
twelve-string	O
to	O
notability	O
in	O
rock	O
and	O
roll	O
.	O
Roger	PERSON
McGuinn	PERSON
began	O
using	O
electric	O
12-string	O
guitars	O
to	O
create	O
the	O
jangly	O
sound	O
of	O
The	O
Byrds	O
.	O
Another	O
notable	O
guitarist	O
to	O
utilize	O
electric	O
12-string	O
guitars	O
is	O
Jimmy	PERSON
Page	PERSON
,	O
the	O
guitarist	O
with	O
hard	O
rock-heavy	O
metal	O
and	O
rock	O
group	O
Led	PERSON
Zeppelin	PERSON
.	O
Lee	PERSON
Ranaldo	PERSON
of	O
Sonic	O
Youth	O
plays	O
with	O
a	O
3rd	O
bridge	O
.	O
