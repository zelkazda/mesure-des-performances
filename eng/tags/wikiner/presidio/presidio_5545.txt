In	O
1925	O
,	O
Hugo	O
founded	O
radio	O
station	O
WRNY	O
and	O
was	O
involved	O
in	O
the	O
first	O
television	O
broadcasts	O
.	O
Gernsback	PERSON
started	O
the	O
modern	O
genre	O
of	O
science	O
fiction	O
by	O
founding	O
the	O
first	O
magazine	O
dedicated	O
to	O
it	O
,	O
Amazing	O
Stories	O
,	O
in	O
1926	O
.	O
He	O
said	O
he	O
became	O
interested	O
in	O
the	O
concept	O
after	O
reading	O
a	O
translation	O
of	O
the	O
work	O
of	O
Percival	PERSON
Lowell	PERSON
as	O
a	O
child	O
.	O
There	O
is	O
some	O
debate	O
about	O
whether	O
this	O
process	O
was	O
genuine	O
,	O
manipulated	O
by	O
publisher	O
Bernarr	PERSON
Macfadden	PERSON
,	O
or	O
was	O
a	O
Gernsback	PERSON
scheme	O
to	O
begin	O
another	O
company	O
.	O
After	O
losing	O
control	O
of	O
Amazing	O
Stories	O
,	O
Gernsback	PERSON
founded	O
two	O
new	O
science	O
fiction	O
magazines	O
,	O
Science	O
Wonder	O
Stories	O
and	O
Air	O
Wonder	O
Stories	O
.	O
Gernsback	PERSON
returned	O
in	O
1952-53	O
with	O
Science-Fiction	O
Plus	O
.	O
Gernsback	PERSON
was	O
noted	O
for	O
sharp	O
(	O
and	O
sometimes	O
shady	O
)	O
business	O
practices	O
,	O
and	O
for	O
paying	O
his	O
writers	O
extremely	O
low	O
fees	O
.	O
Gernsback	PERSON
wrote	O
some	O
fiction	O
,	O
including	O
the	O
novel	O
Ralph	O
124C	O
41+	O
in	O
1911	O
(	O
the	O
title	O
was	O
a	O
pun	O
of	O
the	O
phrase	O
"	O
one	O
to	O
foresee	O
for	O
one	O
"	O
)	O
.	O
In	O
April	O
1908	O
he	O
founded	O
Modern	O
Electrics	O
,	O
the	O
world	O
's	O
first	O
magazine	O
about	O
electronics	O
.	O
In	O
1913	O
,	O
he	O
founded	O
a	O
similar	O
magazine	O
,	O
The	O
Electrical	O
Experimenter	O
,	O
which	O
became	O
Science	O
and	O
Invention	O
in	O
1920	O
.	O
Gernsback	PERSON
held	O
80	O
patents	O
by	O
the	O
time	O
of	O
his	O
death	O
in	O
New	LOCATION
York	LOCATION
City	LOCATION
on	O
August	O
19	O
,	O
1967	O
.	O
