The	O
hertz	O
is	O
named	O
after	O
the	O
German	O
physicist	O
Heinrich	PERSON
Hertz	PERSON
,	O
who	O
made	O
important	O
scientific	O
contributions	O
to	O
the	O
study	O
of	O
electromagnetism	O
.	O
The	O
name	O
was	O
established	O
by	O
the	O
International	O
Electrotechnical	O
Commission	O
(	O
IEC	O
)	O
in	O
1930	O
.	O
