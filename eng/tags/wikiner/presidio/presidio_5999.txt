According	O
to	O
Eurostat	O
data	O
,	O
Italian	O
PPS	O
GDP	O
per	O
capita	O
approximately	O
equaled	O
the	O
EU	O
average	O
in	O
2008	O
.	O
In	O
the	O
second	O
phase	O
of	O
emigration	O
(	O
1900	O
to	O
World	O
War	O
I	O
)	O
most	O
emigrants	O
were	O
from	O
the	O
south	O
and	O
most	O
of	O
them	O
were	O
from	O
rural	O
areas	O
,	O
driven	O
off	O
the	O
land	O
by	O
inefficient	O
land	O
management	O
policies	O
.	O
An	O
unpopular	O
and	O
costly	O
conflict	O
had	O
been	O
borne	O
by	O
an	O
underdeveloped	O
country	O
,	O
and	O
this	O
resulted	O
in	O
the	O
several	O
Italian	O
economic	O
battles	O
during	O
the	O
period	O
.	O
Post-World	O
War	O
I	O
there	O
was	O
inflation	O
,	O
massive	O
debts	O
and	O
an	O
extended	O
depression	O
.	O
Successive	O
Italian	O
governments	O
have	O
adopted	O
annual	O
austerity	O
budgets	O
with	O
cutbacks	O
in	O
spending	O
,	O
as	O
well	O
as	O
new	O
revenue	O
raising	O
measures	O
.	O
The	O
opposition	O
blamed	O
Silvio	PERSON
Berlusconi	PERSON
's	PERSON
government	O
for	O
incompetence	O
,	O
especially	O
the	O
minister	O
of	O
economy	O
Giulio	PERSON
Tremonti	PERSON
.	O
Industrial	O
output	O
in	O
Italy	LOCATION
has	O
slumped	O
6.6	O
percent	O
over	O
the	O
past	O
year	O
.	O
The	O
Italian	O
economy	O
is	O
one	O
of	O
the	O
world	O
's	O
major	O
economies	O
,	O
and	O
its	O
main	O
industries	O
are	O
tourism	O
,	O
commerce	O
,	O
communications	O
,	O
chemicals	O
,	O
machinery	O
,	O
car	O
manufacture	O
,	O
food	O
,	O
textiles	O
,	O
clothing	O
,	O
footwear	O
and	O
ceramics	O
.	O
During	O
the	O
last	O
decade	O
the	O
average	O
annual	O
growth	O
was	O
1.23	O
%	O
in	O
comparison	O
to	O
an	O
average	O
EU	O
annual	O
growth	O
rate	O
of	O
2.28	O
%	O
.	O
The	O
Italian	O
economy	O
is	O
weakened	O
by	O
the	O
lack	O
of	O
infrastructure	O
development	O
,	O
market	O
reforms	O
and	O
research	O
investment	O
.	O
In	O
2009	O
,	O
the	O
deficit	O
of	O
the	O
Italian	O
state	O
budget	O
made	O
up	O
5.3	O
percent	O
while	O
the	O
state	O
debt	O
of	O
the	O
country	O
reached	O
1.761	O
trillion	O
euro	O
(	O
115.8	O
%	O
of	O
GDP	O
)	O
.	O
The	O
national	O
railway	O
network	O
,	O
state-owned	O
and	O
operated	O
by	O
Ferrovie	O
dello	O
Stato	O
,	O
in	O
2003	O
totalled	O
16,287	O
km	O
(	O
10,122	O
mi	O
)	O
of	O
which	O
69	O
%	O
electrified	O
,	O
and	O
on	O
which	O
4,937	O
locomotives	O
and	O
railcars	O
circulated	O
.	O
As	O
part	O
of	O
the	O
agreement	O
,	O
ENEL	O
received	O
a	O
12.5	O
%	O
stake	O
in	O
the	O
project	O
and	O
direct	O
involvement	O
in	O
design	O
,	O
construction	O
,	O
and	O
operation	O
of	O
the	O
plants	O
.	O
Milan	LOCATION
and	O
Rome	LOCATION
are	O
also	O
the	O
world	O
's	O
11th	O
and	O
18th	O
(	O
respectively	O
)	O
most	O
expensive	O
cities	O
in	O
the	O
world	O
.	O
Milan	LOCATION
is	O
Europe	LOCATION
's	O
26th	O
richest	O
city	O
by	O
purchasing	O
power	O
in	O
2009	O
,	O
with	O
a	O
GDP	O
of	O
$	O
115	O
billion	O
.	O
Naples	LOCATION
,	O
in	O
southern	O
Italy	LOCATION
,	O
which	O
is	O
characterized	O
by	O
high	O
levels	O
of	O
unemployment	O
and	O
organized	O
crime	O
,	O
is	O
the	O
world	O
's	O
91st	O
richest	O
city	O
by	O
purchasing	O
power	O
,	O
with	O
a	O
GDP	O
of	O
$	O
43	O
billion	O
.	O
Northern	LOCATION
Italy	LOCATION
is	O
the	O
wealthiest	O
and	O
most	O
prosperous	O
of	O
Italian	O
areas	O
.	O
The	O
region	O
Lazio	PERSON
(	O
GDP	O
:	O
€	O
161	O
billion	O
(	O
2006	O
)	O
)	O
,	O
is	O
geographically	O
in	O
Central	LOCATION
Italy	LOCATION
and	O
includes	O
the	O
capital	O
Rome	LOCATION
.	O
Southern	LOCATION
Italy	LOCATION
is	O
the	O
country	O
's	O
less	O
affluent	O
and	O
less	O
prosperous	O
area	O
.	O
In	O
the	O
11th	O
and	O
12th	O
centuries	O
,	O
Sicily	LOCATION
and	O
the	LOCATION
Kingdom	LOCATION
of	LOCATION
Naples	LOCATION
played	O
a	O
major	O
role	O
in	O
European	O
affairs	O
and	O
exhibited	O
many	O
signs	O
of	O
prosperity	O
.	O
By	O
the	O
middle	O
of	O
the	O
13th	O
century	O
,	O
due	O
to	O
fiscal	O
policies	O
that	O
prevented	O
the	O
growth	O
of	O
a	O
strong	O
merchant	O
class	O
,	O
the	O
region	O
became	O
economically	O
backward	O
compared	O
to	O
the	O
other	O
Italian	O
states	O
.	O
Sicily	LOCATION
's	O
trade	O
fell	O
primarily	O
under	O
Catalan	O
control	O
and	O
by	O
the	O
14th	O
century	O
finances	O
of	O
the	O
kingdom	O
fell	O
primarily	O
into	O
Tuscan	O
hands	O
.	O
Following	O
unification	O
with	O
the	O
rest	O
of	O
the	O
Italian	O
peninsula	O
in	O
1861	O
,	O
factory	O
technology	O
was	O
taken	O
away	O
to	O
Piedmont	LOCATION
,	O
Lombardy	LOCATION
and	O
Liguria	LOCATION
.	O
After	O
unification	O
southern	O
Italy	LOCATION
experienced	O
a	O
huge	O
demographic	O
expansion	O
which	O
provoked	O
mass	O
emigration	O
,	O
especially	O
between	O
1892	O
and	O
1921	O
.	O
In	O
addition	O
,	O
corruption	O
was	O
such	O
a	O
large	O
problem	O
that	O
the	O
prime-minister	O
Giovanni	PERSON
Giolitti	PERSON
once	O
conceded	O
that	O
places	O
existed	O
"	O
where	O
the	O
law	O
does	O
not	O
operate	O
at	O
all	O
"	O
.	O
One	O
study	O
released	O
in	O
1910	O
examined	O
tax	O
rates	O
in	O
north	O
,	O
central	O
and	O
southern	O
Italy	LOCATION
indicated	O
that	O
northern	O
Italy	LOCATION
with	O
48	O
%	O
of	O
the	O
nation	O
's	O
wealth	O
paid	O
40	O
%	O
of	O
the	O
nation	O
's	O
taxes	O
,	O
while	O
the	O
south	O
with	O
27	O
%	O
of	O
the	O
nation	O
's	O
wealth	O
paid	O
32	O
%	O
of	O
the	O
nation	O
's	O
taxes	O
.	O
Naples	LOCATION
enjoyed	O
a	O
demographic	O
and	O
economic	O
rebirth	O
,	O
mainly	O
thanks	O
to	O
the	O
interest	O
of	O
the	O
King	O
Victor	O
Emmanuel	PERSON
III	PERSON
who	O
was	O
born	O
there	O
.	O
During	O
the	O
1940s	O
,	O
50s	O
,	O
60s	O
and	O
70s	O
,	O
the	O
economy	O
of	O
southern	O
Italy	LOCATION
has	O
had	O
a	O
remarkable	O
growth	O
.	O
Unemployment	O
has	O
been	O
decreasing	O
,	O
since	O
the	O
2003	O
contreversial	O
"	O
Biagi	O
law	O
"	O
,	O
as	O
unemployment	O
in	O
Campania	LOCATION
has	O
fallen	O
from	O
23.7	O
%	O
in	O
1999	O
to	O
11.2	O
%	O
in	O
2007	O
,	O
and	O
in	O
Sicily	LOCATION
from	O
24.5	O
%	O
to	O
13	O
%	O
.	O
The	O
area	O
's	O
richest	O
region	O
,	O
Campania	LOCATION
,	O
has	O
a	O
GDP	O
nominal	O
of	O
€	O
94.3	O
billion	O
in	O
2006	O
,	O
and	O
a	O
GDP	O
per	O
capita	O
of	O
€	O
16,294	O
.	O
A	O
chart	O
showing	O
the	O
different	O
GDP	O
per	O
capita	O
distribution	O
amongst	O
Italian	O
regions	O
:	O
This	O
is	O
a	O
list	O
of	O
the	O
top	O
10	O
Italian	O
banks	O
ranked	O
by	O
market	O
capitalization	O
.	O
The	O
Colosseum	O
(	O
4	O
million	O
tourists	O
)	O
and	O
the	O
Vatican	O
Museums	O
(	O
4.2	O
million	O
tourists	O
)	O
are	O
the	O
39th	O
and	O
37th	O
(	O
respectively	O
)	O
most	O
visited	O
places	O
in	O
the	O
world	O
,	O
according	O
to	O
a	O
recent	O
study	O
.	O
With	O
a	O
contribution	O
to	O
the	O
national	O
GDP	O
of	O
8.5	O
%	O
,	O
and	O
a	O
workforce	O
of	O
200,000	O
or	O
more	O
,	O
the	O
automotive	O
industry	O
forms	O
a	O
significant	O
component	O
of	O
the	O
Italian	O
economy	O
.	O
The	O
country	O
is	O
the	O
5th	O
largest	O
automobile	O
producer	O
in	O
Europe	LOCATION
(	O
2006	O
)	O
.	O
The	O
industry	O
is	O
almost	O
totally	O
dominated	O
today	O
by	O
Fiat	O
Group	O
which	O
produced	O
over	O
90	O
%	O
of	O
vehicles	O
manufactured	O
in	O
2001	O
and	O
whose	O
brands	O
include	O
Fiat	O
,	O
Lancia	O
,	O
Alfa	O
Romeo	O
,	O
Iveco	O
,	O
Bertone	O
,	O
Maserati	O
,	O
Ferrari	O
and	O
Abarth	PERSON
.	O
This	O
DOC	O
certificate	O
,	O
which	O
is	O
attributed	O
by	O
the	O
European	O
Union	O
,	O
ensures	O
that	O
the	O
origins	O
and	O
work	O
that	O
goes	O
into	O
a	O
product	O
are	O
recognised	O
.	O
Industrial	O
sectors	O
have	O
long	O
been	O
concentrated	O
in	O
northern	O
areas	O
of	O
Piemonte	LOCATION
,	O
Lombardia	LOCATION
,	O
and	O
Veneto	LOCATION
.	O
The	O
FIAT	O
factory	O
,	O
for	O
example	O
,	O
is	O
located	O
in	O
Turin	LOCATION
.	O
Most	O
Italian	O
industrial	O
companies	O
,	O
often	O
of	O
small	O
size	O
,	O
are	O
located	O
in	O
the	O
"	O
industrial	O
triangle	O
"	O
(	O
Milan	LOCATION
,	O
Turin	LOCATION
,	O
Genoa	LOCATION
)	O
and	O
in	O
some	O
centres	O
of	O
northeast	O
and	O
Emilia	PERSON
Romagna	PERSON
.	O
Following	O
the	O
2003	O
"	O
Biagi	O
law	O
"	O
,	O
a	O
controversial	O
labour	O
reform	O
,	O
unemployment	O
has	O
been	O
steadily	O
decreasing	O
,	O
reaching	O
6.2	O
%	O
in	O
2007	O
,	O
the	O
lowest	O
rate	O
since	O
the	O
1970s	O
.	O
Most	O
Italian	O
unions	O
are	O
grouped	O
in	O
three	O
major	O
confederations	O
:	O
the	O
CGIL	O
,	O
the	O
CISL	O
,	O
and	O
the	O
UIL	O
,	O
which	O
together	O
claim	O
35	O
%	O
of	O
the	O
work	O
force	O
.	O
Of	O
the	O
three	O
unions	O
,	O
CGIL	O
is	O
the	O
strongest	O
in	O
numbers	O
.	O
[	O
citation	O
needed	O
]	O
CGIL	O
once	O
single-handedly	O
organized	O
a	O
three-million	O
people	O
rally	O
in	O
Rome	LOCATION
.	O
