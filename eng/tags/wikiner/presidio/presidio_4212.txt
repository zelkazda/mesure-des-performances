ELO	O
was	O
formed	O
to	O
accommodate	O
Roy	PERSON
Wood	PERSON
and	O
Jeff	PERSON
Lynne	PERSON
's	PERSON
desire	O
to	O
create	O
modern	O
rock	O
and	O
pop	O
songs	O
with	O
classical	O
overtones	O
.	O
They	O
soon	O
gained	O
a	O
cult	O
following	O
despite	O
lukewarm	O
reviews	O
back	O
in	O
their	O
native	O
U.K.	LOCATION
.	O
ELO	O
collected	O
21	O
RIAA	O
awards	O
,	O
38	O
BPI	O
awards	O
,	O
and	O
sold	O
well	O
over	O
50	O
million	O
records	O
worldwide	O
,	O
not	O
including	O
singles	O
.	O
In	O
the	O
late	O
1960s	O
,	O
Roy	PERSON
Wood	PERSON
--	O
guitarist	O
,	O
vocalist	O
and	O
songwriter	O
of	O
The	O
Move	O
--	O
had	O
an	O
idea	O
to	O
form	O
a	O
new	O
band	O
that	O
would	O
use	O
violins	O
,	O
cellos	O
,	O
string	O
basses	O
,	O
horns	O
and	O
woodwinds	O
to	O
give	O
their	O
music	O
a	O
classical	O
sound	O
,	O
taking	O
rock	O
music	O
in	O
the	O
direction	O
"	O
that	O
The	O
Beatles	O
had	O
left	O
off	O
.	O
"	O
Jeff	PERSON
Lynne	PERSON
,	O
frontman	O
of	O
fellow	O
Birmingham	LOCATION
group	O
The	O
Idle	O
Race	O
,	O
was	O
excited	O
by	O
the	O
concept	O
.	O
To	O
help	O
finance	O
the	O
fledgling	O
band	O
,	O
two	O
more	O
Move	O
albums	O
were	O
released	O
during	O
the	O
lengthy	O
ELO	O
recordings	O
.	O
The	O
resulting	O
debut	O
album	O
The	O
Electric	O
Light	O
Orchestra	O
was	O
released	O
in	O
1971	O
(	O
1972	O
in	O
the	LOCATION
United	LOCATION
States	LOCATION
as	O
No	O
Answer	O
)	O
and	O
"	O
10538	O
Overture	O
"	O
became	O
a	O
UK	LOCATION
top	O
ten	O
hit	O
.	O
The	O
new	O
lineup	O
performed	O
at	O
the	O
1972	O
Reading	O
Festival	O
.	O
The	O
band	O
released	O
their	O
second	O
album	O
,	O
ELO	O
2	O
in	O
1973	O
,	O
which	O
produced	O
their	O
first	O
US	LOCATION
chart	O
single	O
,	O
a	O
hugely	O
elaborate	O
version	O
of	O
the	O
Chuck	PERSON
Berry	PERSON
classic	O
"	O
Roll	O
Over	O
Beethoven	PERSON
"	O
.	O
ELO	O
also	O
made	O
their	O
first	O
appearance	O
on	O
American	O
Bandstand	O
.	O
Louis	PERSON
Clark	PERSON
joined	O
the	O
band	O
as	O
string	O
arranger	O
.	O
ELO	O
had	O
become	O
successful	O
in	O
the	LOCATION
United	LOCATION
States	LOCATION
at	O
this	O
point	O
and	O
they	O
were	O
a	O
star	O
attraction	O
on	O
the	O
stadium	O
and	O
arena	O
circuit	O
,	O
as	O
well	O
as	O
regularly	O
appearing	O
on	O
The	O
Midnight	O
Special	O
(	O
1973	O
,	O
1975	O
,	O
1976	O
&	O
1977	O
)	O
more	O
than	O
any	O
other	O
band	O
in	O
that	O
show	O
's	O
history	O
with	O
four	O
appearances	O
.	O
Face	O
the	O
Music	O
was	O
released	O
in	O
1975	O
,	O
producing	O
the	O
hit	O
singles	O
"	O
Evil	O
Woman	O
"	O
and	O
"	O
Strange	O
Magic	O
"	O
.	O
The	O
opening	O
instrumental	O
"	O
Fire	O
On	O
High	O
"	O
,	O
with	O
its	O
mix	O
of	O
strings	O
and	O
blazing	O
acoustic	O
guitars	O
,	O
saw	O
heavy	O
exposure	O
as	O
background	O
music	O
on	O
CBS	O
Sports	O
Spectacular	O
montages	O
,	O
though	O
most	O
viewers	O
had	O
no	O
idea	O
of	O
the	O
song	O
's	O
origins	O
.	O
The	O
group	O
toured	O
extensively	O
from	O
3	O
February	O
till	O
13	O
April	O
1976	O
promoting	O
the	O
album	O
in	O
the	O
USA	LOCATION
,	O
playing	O
68	O
shows	O
in	O
76	O
days	O
.	O
It	O
was	O
on	O
the	O
American	O
tour	O
that	O
ELO	O
first	O
debuted	O
their	O
use	O
of	O
coloured	O
lasers	O
.	O
Despite	O
the	O
recognition	O
and	O
success	O
they	O
enjoyed	O
in	O
the	O
states	O
they	O
were	O
still	O
largely	O
ignored	O
in	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
until	O
their	O
sixth	O
album	O
,	O
A	O
New	O
World	O
Record	O
,	O
hit	O
the	O
top	O
ten	O
there	O
in	O
1976	O
.	O
It	O
contained	O
the	O
hit	O
singles	O
"	O
Livin	O
'	O
Thing	O
"	O
,	O
"	O
Telephone	O
Line	O
"	O
,	O
"	O
Rockaria	PERSON
!	O
"	O
and	O
"	O
Do	O
Ya	O
"	O
,	O
a	O
rerecording	O
of	O
a	O
Move	O
song	O
.	O
The	O
band	O
also	O
played	O
at	O
the	O
Wembley	O
Stadium	O
for	O
eight	O
straight	O
sold-out	O
nights	O
during	O
the	O
tour	O
as	O
well	O
,	O
another	O
record	O
at	O
that	O
time	O
.	O
The	O
first	O
of	O
these	O
shows	O
was	O
recorded	O
and	O
televised	O
,	O
and	O
later	O
released	O
as	O
a	O
CD	O
and	O
DVD	O
.	O
Although	O
the	O
biggest	O
hit	O
on	O
the	O
album	O
(	O
and	O
ELO	O
's	O
biggest	O
hit	O
overall	O
)	O
was	O
the	O
rock	O
song	O
"	O
Do	O
n't	O
Bring	O
Me	O
Down	O
"	O
,	O
the	O
album	O
was	O
noted	O
for	O
its	O
heavy	O
disco	O
influence	O
.	O
Discovery	O
also	O
produced	O
the	O
hits	O
"	O
Shine	O
a	O
Little	O
Love	O
"	O
,	O
"	O
Last	O
Train	O
to	O
London	LOCATION
"	O
,	O
"	O
Confusion	O
"	O
and	O
"	O
The	O
Diary	O
of	O
Horace	O
Wimp	O
"	O
.	O
Although	O
there	O
would	O
be	O
no	O
live	O
tour	O
associated	O
with	O
Discovery	O
,	O
the	O
band	O
recorded	O
the	O
entire	O
album	O
in	O
video	O
form	O
.	O
The	O
Discovery	O
music	O
videos	O
would	O
be	O
the	O
last	O
time	O
the	O
"	O
classic	O
"	O
late	O
1970s	O
lineup	O
would	O
be	O
seen	O
together	O
,	O
as	O
the	O
violinist	O
,	O
Mik	PERSON
Kaminski	PERSON
,	O
and	O
the	O
two	O
cellists	O
,	O
Hugh	PERSON
McDowell	PERSON
and	O
Melvyn	PERSON
Gale	PERSON
,	O
were	O
dismissed	O
shortly	O
thereafter	O
.	O
The	O
Electric	O
Light	O
Orchestra	O
finished	O
1979	O
as	O
the	O
biggest	O
selling	O
act	O
in	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
.	O
[	O
citation	O
needed	O
]	O
ELO	O
had	O
reached	O
the	O
peak	O
of	O
their	O
stardom	O
,	O
selling	O
millions	O
of	O
albums	O
and	O
singles	O
and	O
even	O
inspiring	O
a	O
parody	O
/	O
tribute	O
song	O
on	O
the	O
Randy	PERSON
Newman	PERSON
album	O
Born	O
Again	O
.	O
In	O
1980	O
Jeff	PERSON
Lynne	PERSON
was	O
asked	O
to	O
write	O
for	O
the	O
soundtrack	O
of	O
the	O
musical	O
film	O
Xanadu	PERSON
,	O
with	O
the	O
other	O
half	O
written	O
by	O
John	PERSON
Farrar	PERSON
and	O
performed	O
by	O
the	O
film	O
's	O
star	O
Olivia	PERSON
Newton-John	PERSON
.	O
The	O
album	O
spawned	O
hit	O
singles	O
from	O
both	O
Newton-John	O
(	O
"	O
Magic,	O
"	O
#	O
1	O
in	O
the	LOCATION
United	LOCATION
States	LOCATION
,	O
and	O
"	O
Suddenly	O
"	O
with	O
Cliff	PERSON
Richard	PERSON
)	O
and	O
ELO	O
(	O
"	O
I	O
'm	O
Alive	O
"	O
,	O
which	O
went	O
gold	O
,	O
"	O
All	O
Over	O
the	O
World	O
"	O
and	O
"	O
Do	O
n't	O
Walk	O
Away	O
"	O
)	O
.	O
The	O
title	O
track	O
,	O
performed	O
by	O
both	O
Newton-John	PERSON
and	O
ELO	O
,	O
is	O
ELO	O
's	O
only	O
song	O
to	O
top	O
the	O
singles	O
chart	O
in	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
.	O
In	O
1981	O
ELO	O
's	O
sound	O
changed	O
again	O
with	O
the	O
science	O
fiction	O
concept	O
album	O
Time	O
,	O
a	O
throwback	O
to	O
earlier	O
,	O
more	O
progressive	O
rock	O
albums	O
like	O
Eldorado	O
.	O
Time	O
topped	O
the	O
U.K.	LOCATION
charts	O
for	O
two	O
weeks	O
and	O
was	O
the	O
last	O
ELO	O
studio	O
album	O
to	O
date	O
to	O
be	O
certified	O
platinum	O
in	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
.	O
It	O
was	O
the	O
first	O
ELO	O
tour	O
without	O
cellists	O
,	O
although	O
Mik	PERSON
Kaminski	PERSON
returned	O
to	O
play	O
his	O
famous	O
"	O
blue	O
violin	O
.	O
"	O
Jeff	PERSON
Lynne	PERSON
wanted	O
to	O
follow	O
Time	O
with	O
a	O
double	O
album	O
.	O
The	O
new	O
album	O
was	O
edited	O
down	O
from	O
double	O
album	O
to	O
a	O
single	O
disc	O
and	O
released	O
as	O
Secret	O
Messages	O
in	O
1983	O
.	O
(	O
Many	O
of	O
the	O
outtakes	O
were	O
later	O
released	O
on	O
"	O
Afterglow	O
"	O
or	O
as	O
b-sides	O
of	O
singles	O
.	O
)	O
The	O
album	O
was	O
an	O
instant	O
hit	O
in	O
the	O
UK	LOCATION
reaching	O
the	O
top	O
5	O
.	O
Rumours	O
from	O
fans	O
about	O
the	O
group	O
disbanding	O
were	O
publicly	O
denied	O
by	O
Bevan	PERSON
.	O
Although	O
Secret	O
Messages	O
debuted	O
at	O
number	O
four	O
in	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
,	O
it	O
fell	O
off	O
the	O
charts	O
,	O
failing	O
to	O
catch	O
fire	O
with	O
a	O
lack	O
of	O
hit	O
singles	O
in	O
the	O
U.K.	LOCATION
and	O
a	O
lukewarm	O
media	O
response	O
.	O
The	O
album	O
was	O
absent	O
of	O
actual	O
strings	O
,	O
replaced	O
once	O
again	O
by	O
synthesisers	O
,	O
played	O
by	O
Tandy	O
.	O
The	O
album	O
also	O
shed	O
the	O
customary	O
ELO	O
logo	O
that	O
had	O
appeared	O
on	O
every	O
album	O
since	O
1976	O
.	O
The	O
Birmingham	O
Heart	O
Beat	O
Charity	O
Concert	O
1986	O
was	O
a	O
charity	O
concert	O
organised	O
by	O
Bevan	PERSON
in	O
ELO	O
's	O
hometown	O
of	O
Birmingham	LOCATION
on	O
15	O
March	O
1986	O
.	O
Bevan	PERSON
continued	O
on	O
in	O
1988	O
as	O
ELO	O
Part	O
II	O
,	O
initially	O
with	O
no	O
other	O
former	O
ELO	O
members	O
except	O
Clark	PERSON
.	O
ELO	O
Part	O
II	O
released	O
their	O
debut	O
album	O
Electric	O
Light	O
Orchestra	O
Part	O
Two	O
in	O
1990	O
.	O
Mik	PERSON
Kaminski	PERSON
,	O
Kelly	PERSON
Groucutt	PERSON
and	O
Hugh	PERSON
McDowell	PERSON
joined	O
the	O
band	O
for	O
the	O
first	O
tour	O
in	O
1991	O
.	O
Bevan	PERSON
retired	O
from	O
the	O
lineup	O
in	O
1999	O
and	O
sold	O
his	O
share	O
of	O
the	O
ELO	O
name	O
to	O
Jeff	PERSON
Lynne	PERSON
in	O
2000	O
.	O
In	O
2001	O
Zoom	O
,	O
ELO	O
's	O
first	O
album	O
since	O
1986	O
,	O
was	O
released	O
.	O
Though	O
billed	O
and	O
marketed	O
as	O
an	O
ELO	O
album	O
,	O
the	O
only	O
returning	O
member	O
other	O
than	O
Jeff	PERSON
Lynne	PERSON
was	O
Richard	PERSON
Tandy	PERSON
,	O
who	O
performed	O
on	O
one	O
track	O
.	O
Zoom	O
took	O
on	O
a	O
more	O
organic	O
sound	O
,	O
with	O
less	O
emphasis	O
on	O
strings	O
and	O
electronic	O
effects	O
.	O
Guest	O
musicians	O
included	O
former	O
Beatles	PERSON
Ringo	PERSON
Starr	PERSON
and	O
George	PERSON
Harrison	PERSON
.	O
Former	O
ELO	O
member	O
Richard	PERSON
Tandy	PERSON
rejoined	O
the	O
band	O
a	O
short	O
time	O
afterwards	O
for	O
two	O
television	O
live	O
performances	O
:	O
VH1	O
Storytellers	O
and	O
a	O
PBS	O
concert	O
shot	O
at	O
CBS	O
Television	O
City	O
,	O
later	O
titled	O
Zoom	O
Tour	O
Live	O
,	O
that	O
was	O
released	O
on	O
DVD	O
.	O
The	O
ELO	O
tour	O
was	O
not	O
rescheduled	O
.	O
Included	O
amongst	O
the	O
remastered	O
album	O
tracks	O
were	O
unreleased	O
songs	O
and	O
outtakes	O
,	O
including	O
2	O
new	O
singles	O
"	O
Surrender	O
"	O
which	O
registered	O
on	O
the	O
lower	O
end	O
of	O
the	O
UK	O
Singles	O
Chart	O
at	O
#	O
81	O
,	O
some	O
30	O
years	O
after	O
it	O
was	O
written	O
in	O
1976	O
.	O
Jeff	PERSON
Lynne	PERSON
returned	O
to	O
the	O
song	O
and	O
finished	O
it	O
in	O
preparation	O
for	O
the	O
remastered	O
version	O
of	O
Out	O
of	O
the	O
Blue	O
.	O
A	O
lost	O
demo	O
from	O
1977	O
was	O
finished	O
and	O
released	O
in	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
as	O
a	O
download	O
single	O
on	O
6	O
February	O
2007	O
,	O
titled	O
"	O
Latitude	O
88	O
North	O
"	O
.	O
Among	O
the	O
many	O
features	O
was	O
the	O
original	O
Jet	O
Records	O
label	O
on	O
the	O
disc	O
and	O
original	O
inner	O
sleeves	O
and	O
lyrics	O
.	O
It	O
's	O
the	O
follow-up	O
to	O
All	O
Over	O
the	O
World	O
:	O
The	O
Very	O
Best	O
of	O
Electric	O
Light	O
Orchestra	O
and	O
is	O
called	O
Ticket	O
to	O
the	O
Moon	PERSON
:	O
The	O
Very	O
Best	O
of	O
Electric	O
Light	O
Orchestra	O
Volume	O
2	O
.	O
The	O
US	LOCATION
will	O
see	O
a	O
slightly	O
edited	O
release	O
on	O
24	O
August	O
2010	O
.	O
The	O
group	O
's	O
name	O
is	O
an	O
intended	O
pun	O
based	O
not	O
only	O
on	O
electric	O
light	O
(	O
as	O
in	O
a	O
light	O
bulb	O
as	O
seen	O
on	O
early	O
album	O
covers	O
)	O
but	O
also	O
using	O
"	O
electric	O
"	O
rock	O
instruments	O
combined	O
with	O
a	O
"	O
light	O
orchestra	O
"	O
(	O
orchestras	O
with	O
only	O
a	O
few	O
cellos	O
and	O
violins	O
that	O
were	O
popular	O
in	O
Britain	LOCATION
during	O
the	O
1960s	O
)	O
.	O
The	O
official	O
band	O
logo	O
(	O
left	O
)	O
,	O
designed	O
in	O
1976	O
by	O
artist	O
Kosh	PERSON
,	O
was	O
first	O
seen	O
on	O
their	O
1976	O
album	O
A	O
New	O
World	O
Record	O
and	O
is	O
based	O
on	O
a	O
1946	O
Wurlitzer	O
jukebox	O
model	O
4008	O
speaker	O
.	O
The	O
4008	O
speaker	O
was	O
itself	O
based	O
upon	O
the	O
upper	O
cabinet	O
of	O
the	O
Wurlitzer	PERSON
model	O
1015	O
jukebox	O
.	O
The	O
band	O
's	O
previous	O
logo	O
(	O
right	O
)	O
was	O
similar	O
to	O
the	O
General	O
Electric	O
logo	O
.	O
For	O
instance	O
,	O
on	O
1977	O
's	O
Out	O
of	O
the	O
Blue	O
,	O
the	O
logo	O
was	O
turned	O
into	O
a	O
huge	O
flying	O
saucer	O
space	O
station	O
,	O
an	O
enduring	O
image	O
now	O
synonymous	O
with	O
the	O
band	O
.	O
On	O
the	O
follow	O
up	O
album	O
Discovery	O
,	O
the	O
logo	O
became	O
a	O
small	O
glowing	O
artifact	O
on	O
top	O
of	O
a	O
treasure	O
chest	O
.	O
Bev	PERSON
Bevan	PERSON
usually	O
displayed	O
the	O
logo	O
on	O
his	O
drum	O
kit	O
.	O
