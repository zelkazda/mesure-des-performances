DNA	O
has	O
also	O
been	O
used	O
to	O
look	O
at	O
modern	O
family	O
relationships	O
,	O
such	O
as	O
establishing	O
family	O
relationships	O
between	O
the	O
descendants	O
of	O
Sally	PERSON
Hemings	PERSON
and	O
Thomas	PERSON
Jefferson	PERSON
.	O
DNA	O
was	O
first	O
isolated	O
by	O
the	O
Swiss	O
physician	O
Friedrich	PERSON
Miescher	PERSON
who	O
,	O
in	O
1869	O
,	O
discovered	O
a	O
microscopic	O
substance	O
in	O
the	O
pus	O
of	O
discarded	O
surgical	O
bandages	O
.	O
In	O
1919	O
,	O
Phoebus	PERSON
Levene	PERSON
identified	O
the	O
base	O
,	O
sugar	O
and	O
phosphate	O
nucleotide	O
unit	O
.	O
Levene	PERSON
suggested	O
that	O
DNA	O
consisted	O
of	O
a	O
string	O
of	O
nucleotide	O
units	O
linked	O
together	O
through	O
the	O
phosphate	O
groups	O
.	O
However	O
,	O
Levene	PERSON
thought	O
the	O
chain	O
was	O
short	O
and	O
the	O
bases	O
repeated	O
in	O
a	O
fixed	O
order	O
.	O
Further	O
work	O
by	O
Crick	PERSON
and	O
coworkers	O
showed	O
that	O
the	O
genetic	O
code	O
was	O
based	O
on	O
non-overlapping	O
triplets	O
of	O
bases	O
,	O
called	O
codons	O
,	O
allowing	O
Har	PERSON
Gobind	PERSON
Khorana	PERSON
,	O
Robert	PERSON
W.	PERSON
Holley	PERSON
and	O
Marshall	PERSON
Warren	PERSON
Nirenberg	PERSON
to	O
decipher	O
the	O
genetic	O
code	O
.	O
