The	O
ColecoVision	O
offered	O
arcade-quality	O
graphics	O
and	O
gaming	O
style	O
,	O
and	O
the	O
means	O
to	O
expand	O
the	O
system	O
's	O
basic	O
hardware	O
.	O
Coleco	O
licensed	O
Nintendo	O
's	O
Donkey	LOCATION
Kong	LOCATION
as	O
the	O
official	O
pack-in	O
cartridge	O
for	O
all	O
ColecoVision	O
consoles	O
,	O
and	O
this	O
version	O
of	O
the	O
game	O
was	O
well	O
received	O
as	O
a	O
near-perfect	O
arcade	O
port	O
,	O
helping	O
to	O
boost	O
the	O
console	O
's	O
popularity	O
.	O
The	O
ColecoVision	O
's	O
main	O
competitor	O
was	O
the	O
arguably	O
more	O
advanced	O
but	O
less	O
commercially	O
successful	O
Atari	O
5200	O
.	O
By	O
the	O
beginning	O
of	O
1984	O
,	O
quarterly	O
sales	O
of	O
the	O
ColecoVision	O
had	O
dramatically	O
decreased	O
.	O
Over	O
the	O
next	O
18	O
months	O
,	O
the	O
Coleco	O
company	O
ramped	O
down	O
its	O
video	O
game	O
division	O
,	O
ultimately	O
withdrawing	O
from	O
the	O
video	O
game	O
market	O
by	O
the	O
end	O
of	O
the	O
summer	O
of	O
1985	O
.	O
The	O
ColecoVision	O
was	O
officially	O
discontinued	O
by	O
October	O
1985	O
.	O
Total	O
sales	O
of	O
the	O
ColecoVision	O
are	O
uncertain	O
but	O
were	O
ultimately	O
in	O
excess	O
of	O
2	O
million	O
units	O
,	O
as	O
sales	O
had	O
reached	O
that	O
number	O
by	O
the	O
spring	O
of	O
1984	O
,	O
while	O
the	O
console	O
continued	O
to	O
sell	O
modestly	O
up	O
until	O
its	O
discontinuation	O
the	O
following	O
year	O
.	O
The	O
design	O
of	O
the	O
controllers	O
is	O
similar	O
to	O
that	O
of	O
Mattel	O
's	O
Intellivision	O
--	O
the	O
controller	O
is	O
rectangular	O
and	O
consists	O
of	O
a	O
numeric	O
keypad	O
and	O
a	O
set	O
of	O
side	O
buttons	O
.	O
In	O
place	O
of	O
the	O
circular	O
control	O
disc	O
below	O
the	O
keypad	O
,	O
the	O
Coleco	O
controller	O
has	O
a	O
short	O
,	O
1.5-inch	O
joystick	O
.	O
Each	O
ColecoVision	O
console	O
shipped	O
with	O
two	O
controllers	O
.	O
This	O
delay	O
results	O
from	O
an	O
intentional	O
loop	O
in	O
the	O
console	O
's	O
BIOS	O
to	O
enable	O
on-screen	O
display	O
of	O
the	O
ColecoVision	O
brand	O
.	O
Coleco	O
was	O
also	O
able	O
to	O
design	O
and	O
market	O
the	O
Gemini	O
game	O
system	O
which	O
was	O
an	O
exact	O
clone	O
of	O
the	O
2600	O
,	O
but	O
with	O
combined	O
joystick/paddle	O
controllers	O
.	O
Coleco	O
prototyped	O
a	O
fourth	O
expansion	O
module	O
intended	O
to	O
provide	O
compatibility	O
with	O
Mattel	O
's	O
Intellivision	O
,	O
but	O
this	O
was	O
never	O
released	O
.	O
It	O
also	O
shares	O
a	O
sound	O
chip	O
with	O
Sega	O
consoles	O
(	O
including	O
the	O
Master	O
System	O
)	O
,	O
making	O
them	O
identical	O
in	O
hardware	O
capabilities	O
.	O
The	O
MSX	O
contains	O
a	O
different	O
sound	O
chip	O
that	O
is	O
very	O
similar	O
in	O
capabilities	O
,	O
the	O
General	O
Instruments	O
AY-3-8910	O
.	O
Given	O
that	O
the	O
ColecoVision	O
could	O
produce	O
near	O
arcade-quality	O
ports	O
,	O
industry	O
magazines	O
like	O
Electronic	O
Games	O
were	O
unanimous	O
in	O
their	O
enthusiasm	O
over	O
the	O
console	O
.	O
Some	O
of	O
the	O
more	O
popular	O
games	O
include	O
Donkey	LOCATION
Kong	LOCATION
(	O
the	O
pack-in	O
)	O
,	O
Donkey	O
Kong	O
Junior	O
,	O
Carnival	O
,	O
Lady	PERSON
Bug	PERSON
,	O
Mouse	O
Trap	O
,	O
and	O
Zaxxon	O
.	O
In	O
some	O
cases	O
,	O
the	O
console	O
versions	O
were	O
arguably	O
superior	O
to	O
the	O
arcade	O
versions	O
,	O
as	O
seen	O
in	O
Space	O
Panic	O
.	O
Later	O
Coleco	O
continued	O
adapting	O
newer	O
successful	O
arcade	O
games	O
like	O
Subroc	LOCATION
,	O
Time	O
Pilot	O
and	O
Frenzy	LOCATION
,	O
the	O
company	O
also	O
made	O
inferior	O
ports	O
of	O
many	O
of	O
these	O
games	O
for	O
the	O
Atari	O
2600	O
and	O
Intellivision	O
,	O
in	O
an	O
effort	O
to	O
broaden	O
its	O
market	O
[	O
citation	O
needed	O
]	O
.	O
Coleco	O
was	O
infamous	O
for	O
its	O
vaporware	O
offerings	O
.	O
Several	O
television	O
series	O
have	O
aired	O
episodes	O
that	O
reference	O
or	O
parody	O
the	O
console	O
:	O
South	LOCATION
Park	LOCATION
,	O
Family	PERSON
Guy	PERSON
,	O
Everybody	O
Hates	O
Chris	PERSON
and	O
The	O
Simpsons	O
.	O
