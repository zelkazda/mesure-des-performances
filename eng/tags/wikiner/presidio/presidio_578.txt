After	O
the	O
death	O
of	O
Amalric	PERSON
's	O
father	O
,	O
the	O
throne	O
passed	O
jointly	O
to	O
his	O
mother	O
Melisende	PERSON
and	O
his	O
older	O
brother	O
Baldwin	PERSON
III	PERSON
.	O
Melisende	O
did	O
not	O
step	O
down	O
when	O
Baldwin	PERSON
came	O
of	O
age	O
,	O
and	O
by	O
1150	O
the	O
two	O
were	O
becoming	O
increasingly	O
hostile	O
towards	O
each	O
other	O
.	O
In	O
1152	O
Baldwin	PERSON
had	O
himself	O
crowned	O
sole	O
king	O
,	O
and	O
civil	O
war	O
broke	O
out	O
,	O
with	O
Melisende	LOCATION
retaining	O
Jerusalem	LOCATION
while	O
Baldwin	PERSON
held	O
territory	O
further	O
north	LOCATION
.	LOCATION
Melisende	O
was	O
defeated	O
in	O
this	O
struggle	O
and	O
Baldwin	PERSON
ruled	O
alone	O
thereafter	O
.	O
As	O
a	O
Crusader	O
state	O
Jerusalem	LOCATION
was	O
constantly	O
in	O
a	O
state	O
of	O
war	O
.	O
The	O
main	O
theatre	O
of	O
conflict	O
of	O
Amalric	PERSON
's	O
reign	O
was	O
Fatimid	O
Egypt	LOCATION
,	O
which	O
was	O
suffering	O
from	O
a	O
series	O
of	O
young	O
caliphs	O
and	O
civil	O
wars	O
.	O
Amalric	PERSON
led	O
his	O
first	O
expedition	O
into	O
Egypt	LOCATION
in	O
1163	O
,	O
claiming	O
that	O
the	O
Fatimids	O
had	O
not	O
paid	O
the	O
yearly	O
tribute	O
that	O
had	O
begun	O
during	O
the	O
reign	O
of	O
Baldwin	PERSON
III	PERSON
.	O
Amalric	O
returned	O
home	O
but	O
Shawar	O
fled	O
to	O
the	O
court	O
of	O
Nur	PERSON
ad-Din	PERSON
,	O
who	O
sent	O
his	O
general	O
Shirkuh	PERSON
to	O
settle	O
the	O
dispute	O
in	O
1164	O
.	O
Shawar	O
,	O
however	O
,	O
feared	O
that	O
Shirkuh	PERSON
would	O
seize	O
power	O
for	O
himself	O
,	O
and	O
he	O
too	O
looked	O
to	O
Amalric	O
for	O
assistance	O
.	O
Amalric	PERSON
returned	O
to	O
Egypt	LOCATION
in	O
1164	O
and	O
besieged	O
Shirkuh	O
in	O
Bilbeis	LOCATION
until	O
Shirkuh	PERSON
retreated	O
to	O
Damascus	LOCATION
.	O
The	O
year	O
1166	O
was	O
relatively	O
quiet	O
,	O
but	O
Amalric	PERSON
sent	O
envoys	O
to	O
the	O
Byzantine	LOCATION
Empire	LOCATION
seeking	O
an	O
alliance	O
and	O
a	O
Byzantine	O
wife	O
,	O
and	O
throughout	O
the	O
year	O
had	O
to	O
deal	O
with	O
raids	O
by	O
Nur	PERSON
ad-Din	PERSON
,	O
who	O
captured	O
Banias	PERSON
.	O
In	O
1167	O
,	O
Nur	PERSON
ad-Din	PERSON
sent	O
Shirkuh	PERSON
back	O
to	O
Egypt	LOCATION
and	O
Amalric	PERSON
once	O
again	O
followed	O
him	O
,	O
establishing	O
a	O
camp	O
near	O
Cairo	LOCATION
;	O
Shawar	O
again	O
allied	O
with	O
Amalric	O
as	O
well	O
and	O
a	O
treaty	O
was	O
signed	O
with	O
the	PERSON
caliph	PERSON
al-Adid	PERSON
himself	O
.	O
Shirkuh	PERSON
encamped	O
on	O
the	O
opposite	O
side	O
of	O
the	O
Nile	LOCATION
.	O
After	O
an	O
indecisive	O
battle	O
,	O
Amalric	PERSON
retreated	O
to	O
Cairo	LOCATION
and	O
Shirkuh	PERSON
took	O
his	O
troops	O
to	O
capture	O
Alexandria	LOCATION
;	O
Amalric	PERSON
followed	O
and	O
besieged	O
Shirkuh	O
there	O
,	O
aided	O
by	O
a	O
fleet	O
from	O
Jerusalem	LOCATION
.	O
Shirkuh	PERSON
negotiated	O
for	O
peace	O
and	O
Alexandria	LOCATION
was	O
handed	O
over	O
to	O
Amalric	O
.	O
However	O
Amalric	O
could	O
not	O
remain	O
there	O
forever	O
,	O
and	O
after	O
exacting	O
an	O
enormous	O
tribute	O
,	O
returned	O
to	O
Jerusalem	LOCATION
.	O
During	O
this	O
time	O
the	O
queen	O
dowager	O
,	O
Baldwin	PERSON
III	PERSON
's	PERSON
widow	O
Theodora	PERSON
,	O
eloped	O
with	O
her	O
cousin	O
Andronicus	PERSON
to	O
Damascus	LOCATION
,	O
and	O
Acre	O
reverted	O
back	O
into	O
the	O
royal	O
domain	O
of	O
Jerusalem	LOCATION
.	O
Although	O
Amalric	O
still	O
had	O
a	O
peace	O
treaty	O
with	O
Shawar	O
,	O
Shawar	O
was	O
accused	O
of	O
attempting	O
to	O
ally	O
with	O
Nur	PERSON
ad-Din	PERSON
,	O
and	O
Amalric	PERSON
invaded	O
.	O
The	O
Knights	O
Hospitaller	O
eagerly	O
supported	O
this	O
invasion	O
and	O
may	O
have	O
even	O
been	O
responsible	O
for	O
convincing	O
the	O
king	O
to	O
do	O
it	O
,	O
while	O
the	O
Knights	O
Templar	O
refused	O
to	O
have	O
any	O
part	O
in	O
it	O
.	O
In	O
October	O
,	O
without	O
waiting	O
for	O
any	O
Byzantine	O
assistance	O
(	O
and	O
in	O
fact	O
without	O
even	O
waiting	O
for	O
the	O
ambassadors	O
to	O
return	O
)	O
,	O
Amalric	PERSON
invaded	O
and	O
seized	O
Bilbeis	PERSON
.	O
Amalric	PERSON
then	O
marched	O
to	O
Cairo	LOCATION
,	O
where	O
Shawar	O
offered	O
Amalric	O
two	O
million	O
pieces	O
of	O
gold	O
.	O
Meanwhile	O
Nur	PERSON
ad-Din	PERSON
sent	O
Shirkuh	PERSON
back	O
to	O
Egypt	LOCATION
as	O
well	O
,	O
and	O
upon	O
his	O
arrival	O
Amalric	PERSON
retreated	O
.	O
In	O
January	O
of	O
1169	O
Shirkuh	PERSON
had	O
Shawar	O
assassinated	O
.	O
Shirkuh	PERSON
became	O
vizier	O
,	O
although	O
he	O
himself	O
died	O
in	O
March	O
,	O
and	O
was	O
succeeded	O
by	O
his	O
nephew	O
Saladin	PERSON
.	O
Later	O
that	O
year	O
however	O
a	O
Byzantine	O
fleet	O
arrived	O
,	O
and	O
in	O
October	O
Amalric	O
launched	O
yet	O
another	O
invasion	O
and	O
besieged	O
Damietta	LOCATION
by	O
sea	O
and	O
by	O
land	O
.	O
Amalric	PERSON
returned	O
home	O
.	O
Now	O
Jerusalem	LOCATION
was	O
surrounded	O
by	O
hostile	O
enemies	O
.	O
Saladin	PERSON
's	O
rise	O
to	O
Sultan	PERSON
was	O
an	O
unexpected	O
reprieve	O
for	O
Jerusalem	LOCATION
,	O
as	O
Nur	PERSON
ad-Din	PERSON
was	O
now	O
preoccupied	O
with	O
reining	O
in	O
his	O
powerful	O
vassal	O
.	O
Nur	PERSON
ad-Din	PERSON
died	O
in	O
1174	O
,	O
upon	O
which	O
Amalric	O
immediately	O
besieged	O
Banias	LOCATION
.	O
On	O
the	O
way	O
back	O
after	O
giving	O
up	O
the	O
siege	O
he	O
fell	O
ill	O
from	O
dysentery	O
,	O
which	O
was	O
ameliorated	O
by	O
doctors	O
but	O
turned	O
into	O
a	O
fever	O
in	O
Jerusalem	LOCATION
.	O
Maria	PERSON
Comnena	PERSON
had	O
borne	O
Amalric	O
two	O
daughters	O
:	O
Isabella	PERSON
,	O
who	O
would	O
eventually	O
marry	O
four	O
husbands	O
in	O
turn	O
and	O
succeed	O
as	O
queen	O
,	O
was	O
born	O
in	O
1172	O
;	O
and	O
a	O
stillborn	O
child	O
some	O
time	O
later	O
.	O
William	PERSON
was	O
a	O
good	O
friend	O
of	O
Amalric	PERSON
and	O
described	O
him	O
in	O
great	O
detail	O
.	O
Like	O
his	O
brother	O
Baldwin	PERSON
III	PERSON
,	O
he	O
was	O
more	O
of	O
an	O
academic	O
than	O
a	O
warrior	O
,	O
who	O
studied	O
law	O
and	O
languages	O
in	O
his	O
leisure	O
time	O
:	O
"	O
He	O
was	O
well	O
skilled	O
in	O
the	O
customary	O
law	O
by	O
which	O
the	O
kingdom	O
was	O
governed	O
--	O
in	O
fact	O
,	O
he	O
was	O
second	O
to	O
no	O
one	O
in	O
this	O
respect	O
.	O
"	O
He	O
especially	O
enjoyed	O
reading	O
and	O
being	O
read	O
to	O
,	O
spending	O
long	O
hours	O
listening	O
to	O
William	PERSON
read	O
early	O
drafts	O
of	O
his	O
history	O
.	O
He	O
did	O
not	O
overeat	O
or	O
drink	O
to	O
excess	O
,	O
but	O
his	O
corpulence	O
grew	O
in	O
his	O
later	O
years	O
,	O
decreasing	O
his	O
interest	O
in	O
military	O
operations	O
;	O
according	O
to	O
William	PERSON
,	O
he	O
"	O
was	O
excessively	O
fat	O
,	O
with	O
breasts	O
like	O
those	O
of	O
a	O
woman	O
hanging	O
down	O
to	O
his	O
waist	O
.	O
"	O
As	O
William	PERSON
says	O
,	O
"	O
he	O
was	O
a	O
man	O
of	O
wisdom	O
and	O
discretion	O
,	O
fully	O
competent	O
to	O
hold	O
the	O
reins	O
of	O
government	O
in	O
the	O
kingdom	O
.	O
"	O
Within	O
a	O
few	O
years	O
,	O
Emperor	O
Manuel	PERSON
died	O
as	O
well	O
,	O
and	O
Saladin	PERSON
remained	O
the	O
only	O
strong	O
leader	O
in	O
the	O
east	O
.	O
