The	O
Habsburg	O
monarch	O
ruled	O
as	O
Emperor	O
of	O
Austria	LOCATION
over	O
the	O
western	O
and	O
northern	O
half	O
of	O
the	O
country	O
that	O
was	O
the	LOCATION
Empire	LOCATION
of	LOCATION
Austria	LOCATION
and	O
as	O
King	O
of	O
Hungary	LOCATION
over	O
the	LOCATION
Kingdom	LOCATION
of	LOCATION
Hungary	LOCATION
which	O
enjoyed	O
self-government	O
and	O
representation	O
in	O
joint	O
affairs	O
(	O
principally	O
foreign	O
relations	O
and	O
defence	O
)	O
.	O
In	O
the	O
effort	O
to	O
shore	O
up	O
support	O
for	O
the	O
monarchy	O
,	O
Emperor	O
Franz	PERSON
Joseph	PERSON
began	O
negotiations	O
for	O
a	O
compromise	O
with	O
the	O
Hungarian	O
nobility	O
to	O
ensure	O
their	O
support	O
.	O
From	O
1867	O
onwards	O
,	O
the	O
abbreviations	O
heading	O
the	O
names	O
of	O
official	O
institutions	O
in	O
Austria-Hungary	LOCATION
reflected	O
their	O
responsibility	O
:	O
Three	O
distinct	O
elements	O
ruled	O
the	O
Austro-Hungarian	O
Empire	O
:	O
All	O
laws	O
,	O
even	O
the	O
ones	O
with	O
identical	O
content	O
like	O
the	O
compromise	O
of	O
1867	O
,	O
had	O
to	O
pass	O
the	O
parliaments	O
both	O
in	O
Vienna	LOCATION
and	O
Budapest	LOCATION
and	O
were	O
published	O
in	O
the	O
respective	O
official	O
medium	O
.	O
A	O
road	O
was	O
thus	O
mapped	O
out	O
,	O
with	O
a	O
terminus	O
at	O
Sarajevo	LOCATION
in	O
1914	O
.	O
In	O
the	O
later	O
years	O
of	O
the	O
nineteenth	O
century	O
,	O
rapid	O
economic	O
growth	O
spread	O
to	O
the	O
central	O
Hungarian	O
plain	O
and	O
to	O
the	O
Carpathian	O
lands	O
.	O
Meanwhile	O
,	O
western	O
areas	O
,	O
concentrated	O
mainly	O
around	O
Prague	LOCATION
and	O
Vienna	LOCATION
,	O
excelled	O
in	O
various	O
manufacturing	O
industries	O
.	O
This	O
division	O
of	O
labour	O
between	O
the	O
east	O
and	O
west	O
,	O
besides	O
the	O
existing	O
economic	O
and	O
monetary	O
union	O
,	O
led	O
to	O
rapid	O
economic	O
growth	O
throughout	O
Austria-Hungary	LOCATION
by	O
the	O
early	O
20th	O
century	O
.	O
Rail	O
transport	O
expanded	O
rapidly	O
in	O
the	O
Austro-Hungarian	O
Empire	O
.	O
Preßburg	LOCATION
(	O
Bratislava	LOCATION
)	O
,	O
Budapest	LOCATION
,	O
Prague	LOCATION
,	O
Kraków	LOCATION
,	O
Graz	LOCATION
,	O
Laibach	LOCATION
(	O
Ljubljana	LOCATION
)	O
,	O
and	O
Venedig	PERSON
(	O
Venice	LOCATION
)	O
became	O
linked	O
to	O
the	O
main	O
network	O
.	O
This	O
period	O
marked	O
the	O
beginning	O
of	O
widespread	O
rail	O
transportation	O
in	O
Austria-Hungary	LOCATION
,	O
and	O
also	O
the	O
integration	O
of	O
transportation	O
systems	O
in	O
the	O
area	O
.	O
After	O
1879	O
,	O
the	O
Austro-Hungarian	O
government	O
slowly	O
began	O
to	O
renationalize	O
the	O
rail	O
network	O
,	O
largely	O
because	O
of	O
the	O
sluggish	O
pace	O
of	O
development	O
during	O
the	O
worldwide	O
depression	O
of	O
the	O
1870s	O
.	O
See	O
Imperial	O
Austrian	O
State	O
Railways	O
for	O
details	O
.	O
In	O
addition	O
,	O
the	O
emergence	O
of	O
national	O
identity	O
in	O
newly	O
independent	O
Romania	LOCATION
and	O
Serbia	LOCATION
also	O
contributed	O
to	O
the	O
ethnic	O
issues	O
of	O
the	O
empire	O
.	O
On	O
one	O
occasion	O
Count	O
A.	PERSON
Auersperg	PERSON
(	O
Anastasius	PERSON
Grün	PERSON
)	O
entered	O
the	O
diet	O
of	O
Carniola	O
carrying	O
what	O
he	O
claimed	O
to	O
be	O
the	O
whole	O
corpus	O
of	O
Slovene	O
literature	O
under	O
his	O
arm	O
to	O
provide	O
evidence	O
that	O
the	O
Slovene	O
language	O
could	O
in	O
his	O
view	O
not	O
be	O
substituted	O
for	O
German	O
as	O
a	O
medium	O
of	O
higher	O
education	O
.	O
From	O
1882	O
there	O
was	O
a	O
Slovene	O
majority	O
in	O
the	O
diet	O
of	O
Carniola	O
and	O
in	O
the	O
capital	O
Laibach	LOCATION
(	O
Ljubljana	LOCATION
)	O
,	O
thereby	O
replacing	O
German	O
as	O
the	O
dominant	O
official	O
language	O
.	O
Polish	O
was	O
introduced	O
instead	O
of	O
German	O
in	O
1869	O
in	O
Galicia	LOCATION
as	O
the	O
normal	O
language	O
of	O
government	O
.	O
The	O
Romanians	O
and	O
the	O
Serbs	O
also	O
looked	O
to	O
union	O
with	O
their	O
fellow	O
nationalists	O
in	O
the	O
newly-founded	O
states	O
of	O
Romania	LOCATION
(	O
1859	O
--	O
78	O
)	O
and	O
Serbia	LOCATION
.	O
Language	O
was	O
one	O
of	O
the	O
most	O
contentious	O
questions	O
in	O
Austro-Hungarian	O
politics	O
.	O
In	O
the	O
end	O
Badeni	PERSON
was	O
dismissed	O
.	O
The	O
situation	O
of	O
Jews	O
in	O
the	O
kingdom	O
,	O
who	O
numbered	O
about	O
2	O
million	O
in	O
1914	O
,	O
was	O
ambiguous	O
.	O
The	O
occupation	O
of	O
Bosnia-Herzegovina	LOCATION
was	O
a	O
step	O
taken	O
in	O
return	O
for	O
Russia	LOCATION
's	O
advances	O
into	O
Bessarabia	LOCATION
.	O
As	O
a	O
result	O
,	O
Great	LOCATION
Bulgaria	LOCATION
was	O
broken	O
up	O
and	O
Serbian	O
independence	O
was	O
guaranteed	O
.	O
The	O
deaths	O
of	O
Franz	PERSON
Joseph	PERSON
's	PERSON
brother	O
,	O
Maximilian	PERSON
(	O
1867	O
)	O
,	O
and	O
only	O
son	O
,	O
Rudolf	PERSON
,	O
made	O
the	O
Emperor	O
's	O
nephew	O
,	O
Franz	PERSON
Ferdinand	PERSON
,	O
heir	O
to	O
the	O
throne	O
.	O
The	O
archduke	O
was	O
said	O
to	O
have	O
been	O
an	O
advocate	O
for	O
this	O
trialism	O
,	O
by	O
which	O
he	O
wanted	O
to	O
limit	O
the	O
power	O
of	O
the	O
Magyar	O
aristocracy	O
.	O
There	O
were	O
a	O
few	O
members	O
of	O
the	O
Black	O
Hand	O
in	O
Sarajevo	LOCATION
that	O
day	O
.	O
Before	O
Franz	PERSON
was	O
shot	O
,	O
somebody	O
had	O
already	O
tried	O
to	O
kill	O
him	O
and	O
his	O
wife	O
.	O
A	O
member	O
of	O
the	O
Black	O
Hand	O
threw	O
a	O
grenade	O
at	O
the	O
car	O
,	O
but	O
missed	O
.	O
It	O
injured	O
some	O
people	O
nearby	O
and	O
Franz	PERSON
made	O
sure	O
they	O
were	O
given	O
medical	O
attention	O
before	O
the	O
convoy	O
could	O
carry	O
on	O
.	O
The	O
convoy	O
took	O
a	O
wrong	O
turn	O
into	O
a	O
street	O
where	O
Gavrilo	PERSON
Princip	PERSON
was	O
.	O
He	O
took	O
out	O
a	O
pistol	O
from	O
his	O
pocket	O
and	O
shot	O
Franz	PERSON
and	O
his	O
wife	O
.	O
Serbia	LOCATION
had	O
recently	O
gained	O
a	O
significant	O
amount	O
of	O
territory	O
in	O
the	O
Second	O
Balkan	O
War	O
of	O
1913	O
,	O
causing	O
much	O
distress	O
in	O
government	O
circles	O
in	O
Vienna	LOCATION
and	O
Budapest	LOCATION
.	O
Some	O
members	O
of	O
the	O
government	O
,	O
such	O
as	O
Conrad	PERSON
von	PERSON
Hötzendorf	PERSON
,	O
had	O
wanted	O
to	O
confront	O
the	O
resurgent	O
Serbian	O
nation	O
for	O
some	O
years	O
in	O
a	O
preventive	O
war	O
,	O
which	O
was	O
disliked	O
by	O
the	O
emperor	O
,	O
84	O
years	O
old	O
and	O
enemy	O
of	O
all	O
adventures	O
.	O
When	O
Serbia	LOCATION
accepted	O
nine	O
of	O
the	O
ten	O
demands	O
but	O
only	O
partially	O
accepted	O
the	O
remaining	O
one	O
,	O
Austria-Hungary	LOCATION
declared	O
war	O
.	O
Franz	PERSON
Joseph	PERSON
I	O
in	O
the	O
end	O
had	O
followed	O
the	O
urgent	O
suggestions	O
of	O
his	O
top	O
advisors	O
.	O
Over	O
the	O
course	O
of	O
July	O
and	O
August	O
1914	O
,	O
these	O
events	O
caused	O
the	O
start	O
of	O
World	O
War	O
I	O
,	O
as	O
Russia	LOCATION
mobilized	O
in	O
support	O
of	O
Serbia	LOCATION
,	O
setting	O
off	O
a	O
series	O
of	O
countermobilizations	O
.	O
Italy	LOCATION
initially	O
remained	O
neutral	O
,	O
although	O
it	O
had	O
an	O
alliance	O
with	O
Austria-Hungary	LOCATION
.	O
In	O
1915	O
,	O
it	O
switched	O
to	O
the	O
side	O
of	O
the	O
Entente	O
powers	O
,	O
hoping	O
to	O
gain	O
territory	O
from	O
its	O
former	O
ally	O
.	O
Under	O
Conrad	O
's	O
command	O
,	O
Austro-Hungarian	O
troops	O
were	O
involved	O
in	O
the	O
fighting	O
in	O
the	O
Great	O
War	O
until	O
emperor	O
Karl	PERSON
I	O
took	O
the	O
supreme	O
command	O
himself	O
in	O
late	O
1916	O
and	O
dismissed	O
Conrad	PERSON
in	O
1917	O
.	O
The	O
1914	O
invasion	O
of	O
Serbia	LOCATION
was	O
a	O
disaster	O
.	O
By	O
the	O
end	O
of	O
the	O
year	O
,	O
the	O
Austro-Hungarian	O
Army	O
had	O
taken	O
no	O
territory	O
and	O
had	O
lost	O
227,000	O
men	O
out	O
of	O
a	O
total	O
force	O
of	O
450,000	O
men	O
(	O
see	O
Serbian	O
Campaign	O
(	O
World	O
War	O
I	O
)	O
)	O
.	O
On	O
the	O
Eastern	O
front	O
,	O
things	O
started	O
out	O
equally	O
poorly	O
.	O
The	O
Austro-Hungarian	O
Army	O
was	O
defeated	O
at	O
the	O
Battle	O
of	O
Lemberg	O
and	O
the	O
mighty	O
fort	O
city	O
of	O
Przemyśl	LOCATION
was	O
besieged	O
and	O
fell	O
in	O
March	O
1915	O
.	O
The	O
bloody	O
but	O
indecisive	O
fighting	O
on	O
the	O
Italian	O
Front	O
would	O
last	O
for	O
the	O
next	O
three	O
and	O
a	O
half	O
years	O
.	O
The	O
Austro-Hungarian	O
war	O
effort	O
became	O
more	O
and	O
more	O
subordinate	O
to	O
the	O
direction	O
of	O
German	O
planners	O
.	O
This	O
unfortunate	O
maneuver	O
resulted	O
in	O
a	O
greater	O
loss	O
of	O
men	O
in	O
the	O
Serbian	O
invasion	O
than	O
expected	O
.	O
The	O
Austro-Hungarian	O
Empire	O
then	O
withdrew	O
from	O
all	O
defeated	O
countries	O
.	O
Austria-Hungary	LOCATION
signed	O
general	O
armistice	O
in	O
Padua	LOCATION
on	O
3	O
November	O
1918	O
.	O
In	O
1918	O
,	O
as	O
a	O
political	O
result	O
of	O
German	O
defeat	O
on	O
the	O
Western	O
front	O
in	O
World	O
War	O
I	O
,	O
the	O
Austro-Hungarian	O
Monarchy	O
collapsed	O
.	O
In	O
the	O
capital	O
cities	O
:	O
Vienna	LOCATION
and	O
Budapest	LOCATION
the	O
leftist	O
and	O
liberal	O
movements	O
and	O
politicians	O
strengthened	O
and	O
supported	O
the	O
separatism	O
of	O
ethnic	O
minorities	O
.	O
These	O
leftist	O
or	O
left-liberal	O
pro-Entente	O
maverick	O
parties	O
opposed	O
the	O
monarchy	O
as	O
form	O
of	O
government	O
and	O
they	O
considered	O
themselves	O
as	O
internationalist	O
rather	O
than	O
patriotic	O
.	O
As	O
one	O
of	O
his	O
Fourteen	O
Points	O
,	O
U.S.	LOCATION
President	O
Woodrow	PERSON
Wilson	PERSON
demanded	O
that	O
the	O
nationalities	O
of	O
the	O
empire	O
have	O
"	O
freest	O
opportunity	O
to	O
autonomous	O
development	O
.	O
"	O
In	O
response	O
,	O
Karl	PERSON
I	O
agreed	O
to	O
reconvene	O
the	O
Imperial	O
parliament	O
in	O
1917	O
and	O
allow	O
for	O
the	O
creation	O
of	O
a	O
confederation	O
with	O
each	O
national	O
group	O
exercising	O
self-governance	O
.	O
However	O
,	O
the	O
latter	O
no	O
longer	O
trusted	O
Vienna	LOCATION
,	O
and	O
were	O
now	O
dead	O
set	O
on	O
independence	O
.	O
The	O
Lansing	LOCATION
note	O
was	O
,	O
in	O
effect	O
,	O
the	O
death	O
certificate	O
for	O
Austria-Hungary	LOCATION
.	O
Károlyi	O
was	O
a	O
devotee	O
of	O
Entente	LOCATION
from	O
the	O
beginning	O
of	O
the	O
World	O
War	O
.	O
By	O
a	O
notion	O
of	O
Woodrow	PERSON
Wilson	PERSON
's	PERSON
pacifism	O
,	O
Károlyi	O
ordered	O
the	O
full	O
disarmament	O
of	O
Hungarian	O
Army	O
.	O
The	O
First	O
Republic	O
was	O
proclaimed	O
on	O
16	O
November	O
1918	O
with	O
Károlyi	O
being	O
named	O
as	O
president	O
.	O
The	O
Károlyi	O
government	O
also	O
dissolved	O
the	O
gendarme	O
and	O
police	O
,	O
the	O
lack	O
of	O
police	O
force	O
caused	O
big	O
problems	O
in	O
the	O
country	O
.	O
Károlyi	PERSON
(	O
with	O
a	O
new	O
Czechoslovakian	O
passport	O
and	O
Czechoslovak	O
diplomatic	O
help	O
)	O
moved	O
to	O
Paris	LOCATION
.	O
Most	O
soldiers	O
of	O
the	O
Red	O
Army	O
were	O
armed	O
factory	O
workers	O
from	O
Budapest	LOCATION
.	O
Despite	O
the	O
great	O
military	O
successes	O
against	O
Czechoslovakian	O
army	O
,	O
the	O
communist	O
leaders	O
gave	O
back	O
all	O
recaptured	O
lands	O
.	O
In	O
the	O
aftermath	O
of	O
a	O
coup	O
attempt	O
,	O
the	O
government	O
took	O
a	O
series	O
of	O
actions	O
called	O
the	O
Red	O
Terror	O
,	O
murdering	O
several	O
hundred	O
people	O
(	O
mostly	O
intellectuals	O
)	O
,	O
which	O
alienated	O
much	O
of	O
the	O
population	O
.	O
In	O
the	O
face	O
of	O
domestic	O
backlash	O
and	O
an	O
advancing	O
Romanian	O
force	O
,	O
Béla	PERSON
Kun	PERSON
and	O
most	O
of	O
his	O
comrades	O
fled	O
to	O
Austria	LOCATION
,	O
while	O
Budapest	LOCATION
was	O
occupied	O
on	O
August	O
6	O
.	O
Austria	LOCATION
,	O
with	O
its	O
modern	O
borders	O
,	O
was	O
created	O
out	O
of	O
the	O
main	O
German	O
speaking	O
areas	O
.	O
On	O
November	O
12	O
,	O
1918	O
,	O
Austria	LOCATION
became	O
a	O
republic	O
called	O
German	O
Austria	LOCATION
.	O
The	O
German-speaking	O
bordering	O
areas	O
of	O
Bohemia	LOCATION
and	O
Moravia	LOCATION
were	O
allocated	O
to	O
the	O
newly	O
founded	O
Czechoslovakia	LOCATION
.	O
Austria	LOCATION
never	O
did	O
have	O
to	O
pay	O
reparations	O
because	O
allied	O
commissions	O
determined	O
that	O
the	O
country	O
could	O
not	O
afford	O
to	O
pay	O
.	O
As	O
a	O
result	O
,	O
the	LOCATION
Republic	LOCATION
of	LOCATION
German	LOCATION
Austria	LOCATION
lost	O
roughly	O
60	O
percent	O
of	O
the	O
old	O
Austrian	LOCATION
Empire	LOCATION
's	LOCATION
territory	O
.	O
The	O
Hungarian	O
Democratic	O
Republic	O
lost	O
roughly	O
72	O
percent	O
of	O
the	O
pre-war	O
territory	O
included	O
as	O
part	O
of	O
the	LOCATION
Kingdom	LOCATION
of	LOCATION
Hungary	LOCATION
.	O
The	O
decisions	O
of	O
the	O
nations	O
of	O
former	O
Austria-Hungary	LOCATION
and	O
the	O
victors	O
of	O
the	O
Great	O
War	O
,	O
symbolized	O
in	O
the	O
heavily	O
one-sided	O
treaties	O
,	O
had	O
devastating	O
political	O
and	O
economic	O
effects	O
.	O
However	O
,	O
after	O
a	O
brief	O
period	O
of	O
upheaval	O
and	O
the	O
Allies	O
'	O
foreclosure	O
of	O
Anschluss	LOCATION
,	O
Austria	LOCATION
established	O
itself	O
as	O
a	O
federal	O
republic	O
.	O
Apart	O
from	O
Anschluss	O
with	O
Nazi	LOCATION
Germany	LOCATION
,	O
it	O
still	O
survives	O
today	O
.	O
The	O
Hungarian	O
Democratic	O
Republic	O
was	O
short-lived	O
and	O
was	O
replaced	O
by	O
the	O
communist	O
Hungarian	LOCATION
Soviet	LOCATION
Republic	LOCATION
.	O
Romanian	O
troops	O
ousted	O
Béla	PERSON
Kun	PERSON
and	O
his	O
communist	O
government	O
during	O
the	O
Hungarian-Romanian	O
War	O
.	O
In	O
March	O
and	O
again	O
in	O
October	O
1921	O
,	O
ill-prepared	O
attempts	O
by	O
Károly	PERSON
IV	PERSON
(	O
Karl	O
I	O
in	O
Austria	LOCATION
)	O
to	O
regain	O
the	O
throne	O
in	O
Budapest	LOCATION
collapsed	O
.	O
The	O
initially	O
wavering	O
Horthy	O
,	O
after	O
receiving	O
threats	O
of	O
intervention	O
from	O
the	O
Allied	O
powers	O
and	O
neighboring	O
countries	O
,	O
refused	O
his	O
cooperation	O
.	O
The	O
following	O
successor	O
states	O
were	O
formed	O
(	O
entirely	O
or	O
in	O
part	O
)	O
on	O
the	O
territory	O
of	O
the	O
former	O
Austria-Hungary	LOCATION
:	O
Austro-Hungarian	O
lands	O
were	O
also	O
ceded	O
to	O
the	LOCATION
Kingdom	LOCATION
of	LOCATION
Romania	LOCATION
and	O
the	LOCATION
Kingdom	LOCATION
of	LOCATION
Italy	LOCATION
.	O
The	O
Allies	O
generally	O
discarded	O
the	O
ideas	O
of	O
plebiscites	O
in	O
the	O
regions	O
of	O
Austria-Hungary	LOCATION
[	O
citation	O
needed	O
]	O
.	O
The	O
following	O
present-day	O
countries	O
and	O
parts	O
of	O
countries	O
were	O
located	O
within	O
the	O
boundaries	O
of	O
Austria-Hungary	LOCATION
when	O
the	O
empire	O
was	O
dissolved	O
:	O
Empire	LOCATION
of	O
Austria	LOCATION
:	O
Kingdom	LOCATION
of	O
Hungary	O
(	O
Transleithania	O
)	O
:	O
Despite	O
these	O
measures	O
,	O
Austria-Hungary	LOCATION
remained	O
resolutely	O
monarchist	O
and	O
authoritarian	O
.	O
Serbia	LOCATION
and	O
Montenegro	LOCATION
became	O
fully	O
independent	O
.	O
Although	O
Austria-Hungary	LOCATION
did	O
not	O
have	O
a	O
common	O
flag	O
,	O
a	O
common	O
civil	O
naval	O
ensign	O
(	O
introduced	O
in	O
1869	O
)	O
did	O
exist	O
.	O
Additionally	O
each	O
of	O
the	O
two	O
parts	O
of	O
Austria-Hungary	LOCATION
had	O
its	O
own	O
coat	O
of	O
arms	O
.	O
