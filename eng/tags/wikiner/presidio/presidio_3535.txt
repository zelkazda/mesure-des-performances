Modern	O
distance	O
education	O
has	O
been	O
practiced	O
at	O
least	O
since	O
Isaac	PERSON
Pitman	PERSON
taught	O
shorthand	O
in	O
Great	LOCATION
Britain	LOCATION
via	O
correspondence	O
in	O
the	O
1840s	O
.	O
The	O
University	O
of	O
London	O
was	O
the	O
first	O
university	O
to	O
offer	O
distance-learning	O
degrees	O
,	O
establishing	O
its	O
External	O
Programme	O
in	O
1858	O
.	O
The	O
Society	O
to	O
Encourage	O
Studies	O
at	O
Home	O
was	O
founded	O
in	O
1873	O
in	O
Boston	LOCATION
,	O
Massachusetts	LOCATION
.	O
The	O
largest	O
distance-education	O
university	O
in	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
is	O
the	O
Open	O
University	O
,	O
founded	O
1969	O
.	O
In	O
Germany	LOCATION
the	O
FernUniversität	O
in	O
Hagen	O
was	O
founded	O
1974	O
.	O
There	O
are	O
now	O
many	O
similar	O
institutions	O
around	O
the	O
world	O
,	O
often	O
with	O
the	O
name	O
Open	O
University	O
.	O
The	O
first	O
president	O
of	O
the	O
University	O
of	O
Chicago	O
,	O
William	PERSON
Rainey	PERSON
Harper	PERSON
developed	O
extended	O
education	O
and	O
was	O
considered	O
one	O
of	O
the	O
founders	O
of	O
"	O
learning	O
by	O
correspondence	O
programs	O
"	O
.	O
Levels	O
of	O
accreditation	O
vary	O
;	O
some	O
institutions	O
offering	O
distance	O
education	O
in	O
the	LOCATION
United	LOCATION
States	LOCATION
have	O
received	O
little	O
outside	O
oversight	O
,	O
and	O
some	O
may	O
be	O
fraudulent	O
diploma	O
mills	O
.	O
Online	O
education	O
is	O
rapidly	O
increasing	O
among	O
mainstream	O
universities	O
in	O
the	O
U.S.	LOCATION
,	O
where	O
online	O
doctoral	O
programs	O
have	O
even	O
developed	O
at	O
prestigious	O
research	O
institutions	O
.	O
In	O
the	O
twentieth	O
century	O
,	O
radio	O
,	O
television	O
,	O
and	O
the	O
Internet	O
have	O
all	O
been	O
used	O
to	O
further	O
distance	O
education	O
.	O
Computers	O
and	O
the	O
Internet	O
have	O
made	O
distance-learning	O
distribution	O
easier	O
and	O
faster	O
.	O
The	O
private	O
,	O
for-profit	O
University	O
of	O
Phoenix	O
,	O
which	O
is	O
primarily	O
an	O
online	O
university	O
,	O
now	O
has	O
200,000	O
students	O
and	O
expects	O
to	O
serve	O
500,000	O
by	O
2010	O
,	O
yet	O
little	O
is	O
known	O
about	O
student	O
success	O
or	O
lack	O
of	O
success	O
in	O
such	O
a	O
fast-growing	O
institution	O
.	O
Australian	O
children	O
in	O
extremely	O
remote	O
areas	O
have	O
been	O
participating	O
in	O
the	O
"	O
School	O
of	O
the	O
air	O
"	O
since	O
the	O
1940s	O
using	O
2	O
way	O
radio	O
.	O
Some	O
colleges	O
have	O
been	O
working	O
with	O
the	O
U.S.	LOCATION
military	O
to	O
distribute	O
entire	O
course	O
content	O
on	O
a	O
PDA	O
to	O
deployed	O
personnel	O
.	O
When	O
the	O
Internet	O
became	O
a	O
popular	O
medium	O
for	O
distance	O
education	O
many	O
websites	O
were	O
founded	O
offering	O
secure	O
exam	O
software	O
and	O
packages	O
to	O
help	O
professors	O
manage	O
their	O
students	O
more	O
effectively	O
.	O
