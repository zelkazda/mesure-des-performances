Hummer	PERSON
was	O
a	O
marque	O
of	O
trucks	O
sold	O
by	O
General	O
Motors	O
that	O
marketed	O
three	O
vehicles	O
:	O
the	O
original	O
Hummer	PERSON
H1	O
based	O
on	O
the	O
military	O
High	O
Mobility	O
Multipurpose	O
Wheeled	O
Vehicle	O
(	O
HMMWV	O
,	O
or	O
Humvee	O
)	O
,	O
along	O
with	O
the	O
Hummer	PERSON
H2	O
and	O
the	O
Hummer	PERSON
H3	O
--	O
based	O
on	O
other	O
,	O
smaller	O
civilian-market	O
vehicles	O
.	O
The	O
brand	O
was	O
not	O
transferred	O
to	O
Motors	O
Liquidation	O
Company	O
as	O
part	O
of	O
the	O
GM	O
bankruptcy	O
;	O
instead	O
,	O
it	O
was	O
retained	O
by	O
GM	O
in	O
order	O
to	O
investigate	O
selling	O
the	O
brand	O
.	O
At	O
the	O
end	O
of	O
February	O
2010	O
,	O
General	O
Motors	O
announced	O
it	O
would	O
begin	O
dismantling	O
the	O
Hummer	PERSON
brand	O
.	O
Two	O
days	O
later	O
,	O
the	O
automaker	O
announced	O
it	O
had	O
been	O
approached	O
with	O
new	O
offers	O
for	O
the	O
brand	O
after	O
the	O
deal	O
with	O
Sichuan	LOCATION
Tengzhong	LOCATION
could	O
not	O
be	O
completed	O
.	O
After	O
filling	O
a	O
rental	O
car	O
fleet	O
order	O
,	O
the	O
last	O
Hummer	PERSON
H3	O
rolled	O
off	O
line	O
at	O
Shreveport	LOCATION
on	O
24	O
May	O
2010	O
.	O
They	O
were	O
created	O
under	O
a	O
contract	O
for	O
the	LOCATION
United	LOCATION
States	LOCATION
armed	O
forces	O
.	O
The	O
first	O
model	O
,	O
the	O
Hum-Vee	O
,	O
was	O
built	O
in	O
a	O
variety	O
of	O
military-based	O
equipment	O
and	O
versions	O
.	O
The	O
U.S.	LOCATION
military	O
,	O
on	O
receiving	O
their	O
quota	O
,	O
have	O
adapted	O
some	O
of	O
the	O
vehicles	O
,	O
including	O
modifications	O
to	O
facilitate	O
a	O
directional	O
microwave	O
crowd	O
control	O
beam	O
.	O
AM	O
General	O
had	O
planned	O
to	O
sell	O
a	O
civilian	O
version	O
of	O
the	O
Hum-Vee	O
as	O
far	O
back	O
as	O
the	O
late	O
1980s	O
.	O
This	O
publicity	O
would	O
pale	O
in	O
comparison	O
to	O
the	O
attention	O
that	O
the	O
Hum-Vee	O
received	O
for	O
its	O
service	O
in	O
Operation	O
:	O
Desert	O
Storm	O
the	O
following	O
year	O
.	O
In	O
1992	O
,	O
AM	O
General	O
began	O
selling	O
a	O
civilian	O
version	O
of	O
the	O
M998	O
High	O
Mobility	O
Multipurpose	O
Wheeled	O
Vehicle	O
(	O
HMMWV	O
or	O
Hum-Vee	O
)	O
vehicle	O
to	O
the	O
public	O
under	O
the	O
brand	O
name	O
"	O
Hummer	PERSON
"	O
.	O
In	O
1998	O
,	O
AM	O
General	O
sold	O
the	O
brand	O
name	O
to	O
General	O
Motors	O
,	O
but	O
continued	O
to	O
manufacture	O
the	O
vehicles	O
.	O
Shortly	O
thereafter	O
,	O
GM	O
introduced	O
two	O
new	O
homegrown	O
models	O
,	O
the	O
H2	O
and	O
H3	O
,	O
and	O
renamed	O
the	O
original	O
vehicle	O
H1	O
.	O
AM	O
General	O
continued	O
to	O
build	O
the	O
H1	O
until	O
it	O
was	O
discontinued	O
in	O
2006	O
,	O
and	O
was	O
contracted	O
by	O
GM	O
to	O
produce	O
the	O
H2	O
.	O
By	O
2006	O
,	O
the	O
Hummer	PERSON
also	O
began	O
to	O
be	O
exported	O
and	O
sold	O
through	O
importers	O
and	O
distributors	O
in	O
33	O
countries	O
.	O
The	O
plant	O
produces	O
a	O
few	O
hundred	O
vehicles	O
annually	O
,	O
and	O
its	O
output	O
is	O
limited	O
to	O
local	O
consumption	O
(	O
five	O
dealers	O
in	O
Russia	LOCATION
)	O
.	O
On	O
June	O
1	O
,	O
2009	O
,	O
as	O
a	O
part	O
of	O
the	O
General	O
Motors	O
bankruptcy	O
announcement	O
,	O
the	O
company	O
revealed	O
that	O
the	O
Hummer	PERSON
brand	O
would	O
be	O
discontinued	O
.	O
However	O
,	O
the	O
following	O
day	O
GM	O
announced	O
that	O
instead	O
it	O
had	O
reached	O
a	O
deal	O
to	O
sell	O
the	O
brand	O
to	O
an	O
undisclosed	O
buyer	O
.	O
Later	O
that	O
day	O
,	O
Sichuan	LOCATION
Tengzhong	LOCATION
itself	O
announced	O
the	O
deal	O
on	O
their	O
own	O
website	O
.	O
On	O
January	O
6	O
,	O
2010	O
,	O
GM	O
CEO	O
Ed	PERSON
Whitacre	PERSON
said	O
he	O
hopes	O
to	O
close	O
the	O
deal	O
with	O
Tengzhong	O
by	O
the	O
end	O
of	O
January	O
2010	O
.	O
It	O
was	O
also	O
revealed	O
that	O
the	O
price	O
tag	O
of	O
the	O
Hummer	PERSON
brand	O
was	O
$	O
150	O
million	O
.	O
Later	O
,	O
on	O
February	O
24	O
,	O
2010	O
,	O
GM	O
announced	O
the	O
Tengzhong	O
deal	O
had	O
collapsed	O
and	O
the	O
Hummer	PERSON
brand	O
would	O
soon	O
shut	O
down	O
.	O
However	O
,	O
on	O
April	O
7	O
,	O
2010	O
,	O
this	O
attempt	O
failed	O
as	O
well	O
,	O
and	O
General	O
Motors	O
officially	O
said	O
it	O
is	O
shutting	O
down	O
the	O
Hummer	PERSON
SUV	O
brand	O
and	O
offering	O
rich	O
rebates	O
in	O
a	O
bid	O
to	O
move	O
the	O
remaining	O
2,200	O
vehicles	O
.	O
Originally	O
released	O
in	O
1992	O
,	O
this	O
vehicle	O
was	O
designed	O
by	O
American	O
Motors	O
'	O
AM	O
General	O
subsidiary	O
for	O
the	O
U.S.	LOCATION
Military	O
.	O
The	O
Hummer	PERSON
H2	O
was	O
the	O
second	O
vehicle	O
in	O
the	O
Hummer	PERSON
range	O
.	O
The	O
Hummer	PERSON
HX	PERSON
is	O
an	O
open-air	O
,	O
two-door	O
off-road	O
vehicle	O
,	O
smaller	O
than	O
any	O
other	O
Hummer	PERSON
.	O
Team	O
Hummer	O
has	O
tallied	O
one	O
of	O
the	O
most	O
impressive	O
records	O
in	O
production-class	O
racing	O
,	O
earning	O
11	O
class	O
wins	O
at	O
the	O
Baja	O
1000	O
.	O
A	O
highly	O
modified	O
,	O
two-wheel	O
drive	O
Hummer	PERSON
was	O
raced	O
by	O
Robby	PERSON
Gordon	PERSON
in	O
the	O
2006	O
,	O
2007	O
(	O
8th	O
place	O
)	O
,	O
2009	O
(	O
3rd	O
place	O
)	O
,	O
and	O
2010	O
(	O
8th	O
place	O
)	O
Dakar	O
Rally	O
.	O
GM	O
has	O
been	O
active	O
in	O
licensing	O
the	O
Hummer	PERSON
.	O
Together	O
,	O
the	O
two	O
organizations	O
train	O
Hummer	PERSON
owners	O
with	O
CPR	O
and	O
first	O
aid	O
skills	O
,	O
and	O
basic	O
off-highway	O
skills	O
so	O
that	O
they	O
may	O
assist	O
victims	O
during	O
a	O
disaster	O
situation	O
.	O
