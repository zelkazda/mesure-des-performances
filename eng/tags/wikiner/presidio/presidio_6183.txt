Space	O
probes	O
have	O
also	O
been	O
inserted	O
into	O
orbit	O
around	O
the	O
planets	O
Venus	LOCATION
,	O
Mars	LOCATION
,	O
Jupiter	LOCATION
,	O
and	O
Saturn	LOCATION
,	O
and	O
have	O
returned	O
data	O
about	O
these	O
bodies	O
and	O
their	O
natural	O
satellites	O
.	O
Further	O
probes	O
are	O
currently	O
en	O
route	O
to	O
orbit	O
Mercury	O
,	O
the	O
dwarf	O
planet	O
Ceres	PERSON
as	O
well	O
as	O
the	O
large	O
asteroid	O
Vesta	O
.	O
The	O
NEAR	O
Shoemaker	O
orbiter	O
successfully	O
landed	O
on	O
the	O
asteroid	O
433	O
Eros	O
,	O
even	O
though	O
it	O
was	O
not	O
designed	O
with	O
this	O
maneuver	O
in	O
mind	O
.	O
An	O
earlier	O
project	O
which	O
received	O
some	O
significant	O
planning	O
by	O
NASA	O
included	O
a	O
manned	O
fly-by	O
of	O
Venus	LOCATION
in	O
the	O
Manned	O
Venus	O
Flyby	O
mission	O
,	O
but	O
was	O
cancelled	O
when	O
the	O
Apollo	O
Applications	O
Program	O
was	O
terminated	O
due	O
to	O
NASA	O
budget	O
cuts	O
in	O
the	O
late	O
1960s	O
.	O
The	O
costs	O
and	O
risk	O
of	O
interplanetary	O
travel	O
receive	O
a	O
lot	O
of	O
publicity	O
--	O
spectacular	O
examples	O
include	O
the	O
malfunctions	O
or	O
complete	O
failures	O
of	O
unmanned	O
probes	O
such	O
as	O
Mars	LOCATION
96	O
,	O
Deep	O
Space	O
2	O
and	O
Beagle	O
2	O
.	O
So	O
far	O
the	O
only	O
benefits	O
of	O
this	O
type	O
have	O
been	O
"	O
spin-off	O
"	O
technologies	O
which	O
were	O
developed	O
for	O
space	O
missions	O
and	O
then	O
were	O
found	O
to	O
be	O
at	O
least	O
as	O
useful	O
in	O
other	O
activities	O
(	O
NASA	O
publicizes	O
spin-offs	O
from	O
its	O
activities	O
)	O
.	O
But	O
science	O
fiction	O
writers	O
have	O
a	O
fairly	O
good	O
track	O
record	O
in	O
predicting	O
future	O
technologies	O
--	O
for	O
example	O
geosynchronous	O
communications	O
satellites	O
(	O
Arthur	PERSON
C.	PERSON
Clarke	PERSON
)	O
and	O
many	O
aspects	O
of	O
computer	O
technology	O
(	O
Mack	O
Reynolds	O
)	O
.	O
Many	O
science	O
fiction	O
stories	O
(	O
notably	O
Ben	PERSON
Bova	PERSON
's	PERSON
Grand	O
Tour	O
stories	O
)	O
feature	O
detailed	O
descriptions	O
of	O
how	O
people	O
could	O
extract	O
minerals	O
from	O
asteroids	O
and	O
energy	O
from	O
sources	O
including	O
orbital	O
solar	O
panels	O
(	O
unhampered	O
by	O
clouds	O
)	O
and	O
the	O
very	O
strong	O
magnetic	O
field	O
of	O
Jupiter	LOCATION
.	O
Some	O
scientists	O
,	O
including	O
members	O
of	O
the	O
Space	O
Studies	O
Institute	O
,	O
argue	O
that	O
the	O
vast	O
majority	O
of	O
mankind	O
eventually	O
will	O
live	O
in	O
space	O
and	O
will	O
benefit	O
from	O
doing	O
this	O
.	O
If	O
the	O
manoeuver	O
is	O
timed	O
properly	O
,	O
Mars	LOCATION
will	O
be	O
"	O
arriving	O
"	O
under	O
the	O
spacecraft	O
when	O
this	O
happens	O
.	O
Most	O
are	O
still	O
just	O
theoretical	O
,	O
but	O
the	O
Deep	O
Space	O
One	O
mission	O
was	O
a	O
very	O
successful	O
test	O
of	O
an	O
ion	O
drive	O
.	O
The	O
NASA	O
designs	O
were	O
conceived	O
as	O
replacements	O
for	O
the	O
upper	O
stages	O
of	O
the	O
Saturn	O
V	O
launch	O
vehicle	O
,	O
but	O
the	O
tests	O
revealed	O
reliability	O
problems	O
,	O
mainly	O
caused	O
by	O
the	O
vibration	O
and	O
heating	O
involved	O
in	O
running	O
the	O
engines	O
at	O
such	O
high	O
thrust	O
levels	O
.	O
Dawn	O
,	O
the	O
first	O
NASA	O
operational	O
(	O
ie	O
,	O
non-technology	O
demonstration	O
)	O
mission	O
to	O
use	O
an	O
ion	O
drive	O
for	O
its	O
primary	O
propulsion	O
,	O
is	O
currently	O
on	O
track	O
to	O
explore	O
and	O
orbit	O
the	O
large	O
main-belt	O
asteroids	O
1	O
Ceres	O
and	O
4	O
Vesta	O
.	O
A	O
more	O
ambitious	O
,	O
nuclear-powered	O
version	O
was	O
intended	O
for	O
an	O
unmanned	O
Jupiter	LOCATION
mission	O
,	O
the	O
Jupiter	O
Icy	O
Moons	O
Orbiter	O
,	O
originally	O
planned	O
for	O
launch	O
sometime	O
in	O
the	O
next	O
decade	O
.	O
Due	O
to	O
a	O
shift	O
in	O
priorities	O
at	O
NASA	O
that	O
favored	O
manned	O
space	O
missions	O
,	O
the	O
project	O
lost	O
funding	O
in	O
2005	O
.	O
One	O
proposal	O
using	O
a	O
fusion	O
(	O
nuclear	O
bomb	O
)	O
rocket	O
was	O
Project	O
Daedalus	O
.	O
The	O
original	O
concept	O
relied	O
only	O
on	O
radiation	O
from	O
the	O
Sun	O
--	O
for	O
example	O
in	O
Arthur	O
C.	O
Clarke	O
's	O
1965	O
story	O
"	O
Sunjammer	PERSON
"	O
.	O
Currently	O
,	O
the	O
only	O
spacecraft	O
to	O
use	O
a	O
solar	O
sail	O
as	O
the	O
main	O
method	O
of	O
production	O
is	O
IKAROS	O
which	O
was	O
launched	O
by	O
JAXA	O
on	O
May	O
21	O
,	O
2010	O
.	O
At	O
least	O
two	O
fundamental	O
non-terrestrial	O
energy	O
sources	O
have	O
been	O
proposed	O
:	O
solar-powered	O
energy	O
generation	O
(	O
unhampered	O
by	O
clouds	O
)	O
,	O
either	O
directly	O
by	O
solar	O
cells	O
or	O
indirectly	O
by	O
focusing	O
solar	O
radiation	O
on	O
boilers	O
which	O
produce	O
steam	O
to	O
drive	O
generators	O
;	O
and	O
electrodynamic	O
tethers	O
which	O
generate	O
electricity	O
from	O
the	O
powerful	O
magnetic	O
fields	O
of	O
some	O
planets	O
(	O
Jupiter	LOCATION
has	O
a	O
very	O
powerful	O
magnetic	O
field	O
)	O
.	O
Water	O
ice	O
would	O
be	O
very	O
useful	O
and	O
is	O
widespread	O
on	O
the	O
moons	O
of	O
Jupiter	LOCATION
and	O
Saturn	O
:	O
Scientists	O
of	O
Russian	O
Academy	O
of	O
Sciences	O
are	O
searching	O
for	O
methods	O
of	O
reducing	O
the	O
risk	O
of	O
radiation-induced	O
cancer	O
in	O
preparation	O
for	O
the	O
mission	O
to	O
Mars	LOCATION
.	O
The	O
crew	O
of	O
the	O
Apollo	O
13	O
mission	O
survived	O
despite	O
an	O
explosion	O
caused	O
by	O
a	O
faulty	O
oxygen	O
tank	O
(	O
1970	O
)	O
;	O
the	O
crews	O
of	O
Soyuz	O
11	O
(	O
1971	O
)	O
,	O
the	O
Space	O
Shuttles	O
Challenger	O
(	O
1986	O
)	O
and	O
Columbia	O
(	O
2003	O
)	O
were	O
killed	O
by	O
malfunctions	O
of	O
their	O
vessels	O
'	O
components	O
.	O
