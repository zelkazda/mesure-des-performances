The	O
history	O
of	O
France	LOCATION
goes	O
back	O
to	O
the	O
arrival	O
of	O
the	O
earliest	O
human	O
being	O
in	O
what	O
is	O
now	O
France	LOCATION
.	O
Frankish	O
power	O
reached	O
its	O
fullest	O
extent	O
under	O
Charlemagne	PERSON
.	O
It	O
was	O
one	O
of	O
the	O
Allied	O
Powers	O
in	O
World	O
War	O
II	O
,	O
but	O
was	O
conquered	O
by	O
Nazi	LOCATION
Germany	LOCATION
within	O
two	O
months	O
.	O
Following	O
liberation	O
,	O
a	LOCATION
Fourth	LOCATION
Republic	LOCATION
was	O
established	O
;	O
this	O
was	O
succeeded	O
by	O
the	O
French	O
Fifth	O
Republic	O
in	O
1958	O
,	O
the	O
country	O
's	O
current	O
government	O
.	O
On	O
the	O
lower	O
Garonne	LOCATION
the	O
people	O
spoke	O
Aquitanian	O
,	O
an	O
archaic	O
language	O
related	O
to	O
Basque	O
.	O
The	O
Phoceans	O
were	O
great	O
navigators	O
such	O
as	O
Pytheas	O
who	O
was	O
born	O
in	O
Marseille	LOCATION
.	O
It	O
was	O
this	O
Gaulish	O
participation	O
that	O
caused	O
Provence	LOCATION
to	O
be	O
annexed	O
in	O
122	O
BC	O
by	O
the	LOCATION
Roman	LOCATION
Republic	LOCATION
.	O
It	O
has	O
been	O
argued	O
the	O
similarities	O
between	O
the	O
Gaulish	O
and	O
Latin	O
languages	O
favoured	O
the	O
transition	O
.	O
Antoninus	PERSON
Pius	PERSON
also	O
came	O
from	O
a	O
Gaulish	O
family	O
.	O
The	LOCATION
Gallic	LOCATION
Empire	LOCATION
ended	O
with	O
Emperor	O
Aurelian	PERSON
's	O
victory	O
at	O
Châlons	O
in	O
274	O
.	O
They	O
were	O
led	O
by	O
the	O
legendary	O
king	O
Conan	PERSON
Meriadoc	PERSON
and	O
came	O
from	O
Britain	LOCATION
.	O
They	O
spoke	O
the	O
now	O
extinct	O
British	O
language	O
,	O
which	O
evolved	O
into	O
the	O
Breton	O
,	O
Cornish	O
,	O
and	O
Welsh	O
languages	O
.	O
In	O
418	O
the	O
Aquitanian	O
province	O
was	O
given	O
to	O
the	O
Goths	O
in	O
exchange	O
for	O
their	O
support	O
against	O
the	O
Vandals	O
.	O
He	O
first	O
used	O
the	O
Huns	O
against	O
the	O
Burgundians	O
,	O
and	O
these	O
mercenaries	O
destroyed	O
Worms	O
,	O
killed	O
king	O
Gunther	PERSON
,	O
and	O
pushed	O
the	O
Burgundians	O
westward	O
.	O
The	O
Burgundians	O
were	O
resettled	O
by	O
Aëtius	O
near	O
Lugdunum	LOCATION
in	O
443	O
.	O
The	LOCATION
Roman	LOCATION
Empire	LOCATION
was	O
on	O
the	O
verge	O
of	O
collapsing	O
.	O
The	O
Franks	O
treated	O
land	O
purely	O
as	O
a	O
private	O
possession	O
and	O
divided	O
it	O
among	O
their	O
heirs	O
,	O
so	O
four	O
kingdoms	O
emerged	O
:	O
Paris	LOCATION
,	O
Orléans	LOCATION
,	O
Soissons	PERSON
,	O
and	O
Rheims	LOCATION
.	O
Duke	O
Odo	O
the	O
Great	O
defeated	O
a	O
major	O
invading	O
force	O
at	O
Toulouse	LOCATION
in	O
721	O
but	O
failed	O
to	O
repel	O
a	O
raiding	O
party	O
in	O
732	O
.	O
The	O
mayor	O
of	O
the	O
palace	O
,	O
Charles	PERSON
Martel	PERSON
,	O
defeated	O
that	O
raiding	O
party	O
at	O
the	O
Battle	O
of	O
Tours	O
and	O
earned	O
respect	O
and	O
power	O
within	O
the	LOCATION
Frankish	LOCATION
Kingdom	LOCATION
.	O
Carolingian	O
power	O
reached	O
its	O
fullest	O
extent	O
under	O
Pippin	PERSON
's	O
son	O
,	O
Charlemagne	PERSON
.	O
The	O
eastern	O
realm	O
,	O
which	O
would	O
become	O
Germany	LOCATION
,	O
elected	O
the	O
Saxon	O
dynasty	O
of	O
Henry	PERSON
the	PERSON
Fowler	PERSON
.	O
Some	O
of	O
the	O
king	O
's	O
vassals	O
would	O
grow	O
so	O
powerful	O
that	O
they	O
would	O
become	O
some	O
of	O
the	O
strongest	O
rulers	O
of	O
western	O
Europe	LOCATION
.	O
These	O
Norman	PERSON
nobles	O
then	O
commissioned	O
the	O
weaving	O
of	O
the	O
Bayeux	O
Tapestry	O
.	O
Hugh	PERSON
Capet	PERSON
was	O
elected	O
by	O
an	O
assembly	O
summoned	O
in	O
Reims	LOCATION
on	O
1	O
June	O
987	O
.	O
Hugh	PERSON
Capet	PERSON
decided	O
so	O
in	O
order	O
to	O
have	O
his	O
succession	O
secured	O
.	O
Although	O
a	O
king	O
weak	O
in	O
power	O
,	O
Robert	PERSON
II	PERSON
's	PERSON
efforts	O
were	O
considerable	O
.	O
It	O
is	O
from	O
Louis	PERSON
VI	PERSON
onward	O
that	O
royal	O
authority	O
became	O
more	O
accepted	O
.	O
Louis	PERSON
VI	PERSON
was	O
more	O
a	O
soldier	O
and	O
warmongering	O
king	O
than	O
a	O
scholar	O
.	O
From	O
1127	O
onward	O
the	O
royal	O
adviser	O
was	O
a	O
skilled	O
politician	O
--	O
Abbot	PERSON
Suger	PERSON
.	O
Louis	PERSON
VI	PERSON
successfully	O
defeated	O
,	O
both	O
military	O
and	O
politically	O
,	O
many	O
of	O
the	O
robber	O
barons	O
.	O
Louis	PERSON
VI	PERSON
frequently	O
summoned	O
his	O
vassals	O
to	O
the	O
court	O
,	O
and	O
those	O
who	O
did	O
not	O
show	O
up	O
often	O
had	O
their	O
land	O
possessions	O
confiscated	O
and	O
military	O
campaigns	O
mounted	O
against	O
them	O
.	O
This	O
drastic	O
policy	O
clearly	O
imposed	O
some	O
royal	O
authority	O
on	O
Paris	LOCATION
and	O
its	O
surrounding	O
areas	O
.	O
However	O
,	O
the	O
couple	O
disagreed	O
over	O
the	O
burning	O
of	O
more	O
than	O
a	O
thousand	O
people	O
in	O
Vitry	LOCATION
during	O
the	O
conflict	O
against	O
the	O
Count	O
of	O
Champagne	O
.	O
King	O
Louis	O
VII	O
was	O
deeply	O
horrified	O
by	O
the	O
event	O
and	O
sought	O
penitence	O
by	O
going	O
to	O
the	O
holy	O
land	O
.	O
The	O
late	O
direct	O
Capetian	O
kings	O
were	O
considerably	O
more	O
powerful	O
and	O
influential	O
than	O
the	O
earliest	O
ones	O
.	O
He	O
had	O
set	O
the	O
context	O
for	O
the	O
rise	O
of	O
power	O
to	O
much	O
more	O
powerful	O
monarchs	O
like	O
Saint	LOCATION
Louis	LOCATION
and	O
Philip	O
the	O
Fair	O
.	O
Saint	LOCATION
Louis	LOCATION
has	O
often	O
been	O
portrayed	O
as	O
a	O
one	O
dimensional	O
character	O
,	O
a	O
flawless	O
representant	O
of	O
the	O
faith	O
and	O
an	O
administrator	O
caring	O
for	O
the	O
governed	O
ones	O
.	O
It	O
appears	O
Louis	PERSON
had	O
a	O
strong	O
sense	O
of	O
justice	O
and	O
always	O
wanted	O
to	O
judge	O
people	O
himself	O
before	O
applying	O
any	O
sentence	O
.	O
He	O
landed	O
in	O
1230	O
at	O
Saint-Malo	O
with	O
a	O
massive	O
force	O
.	O
He	O
died	O
in	O
the	O
Eighth	O
Crusade	O
and	O
Philip	PERSON
III	PERSON
became	O
king	O
.	O
Philip	PERSON
III	PERSON
took	O
part	O
in	O
another	O
crusading	O
disaster	O
:	O
the	O
Aragonese	O
Crusade	O
,	O
which	O
cost	O
him	O
his	O
life	O
.	O
More	O
administrative	O
reforms	O
were	O
made	O
by	O
Philip	O
the	O
Fair	O
.	O
Francis	PERSON
I	O
faced	O
powerful	O
foes	O
,	O
and	O
he	O
was	O
captured	O
at	O
Pavia	LOCATION
.	O
Because	O
of	O
its	O
international	O
status	O
,	O
there	O
was	O
a	O
desire	O
to	O
regulate	O
the	O
French	O
language	O
.	O
Several	O
reforms	O
of	O
the	O
French	O
language	O
worked	O
to	O
uniformise	O
it	O
.	O
Jacques	PERSON
Peletier	PERSON
du	PERSON
Mans	PERSON
(	O
born	O
1517	O
)	O
was	O
one	O
of	O
the	O
scholars	O
who	O
reformed	O
the	O
French	O
language	O
.	O
He	O
improved	O
Nicolas	PERSON
Chuquet	PERSON
's	PERSON
long	O
scale	O
system	O
by	O
adding	O
names	O
for	O
intermediate	O
numbers	O
(	O
"	O
milliards	O
"	O
instead	O
of	O
"	O
thousand	O
million	O
"	O
,	O
etc.	O
)	O
.	O
The	O
largest	O
settlement	O
was	O
New	LOCATION
France	LOCATION
,	O
with	O
the	O
towns	O
of	O
Quebec	LOCATION
City	LOCATION
and	O
Montreal	LOCATION
and	O
long	O
stretches	O
of	O
riverfront	O
.	O
Richelieu	PERSON
died	O
in	O
1642	O
and	O
was	O
succeeded	O
by	O
Cardinal	PERSON
Mazarin	PERSON
,	O
while	O
Louis	PERSON
XIII	PERSON
died	O
one	O
year	O
later	O
and	O
was	O
succeeded	O
by	O
Louis	PERSON
XIV	PERSON
.	O
A	O
tumultuous	O
friendship	O
was	O
established	O
between	O
Lully	PERSON
and	O
Molière	O
.	O
His	O
military	O
architect	O
,	O
Vauban	PERSON
,	O
became	O
famous	O
for	O
his	O
pentagonal	O
fortresses	O
,	O
and	O
Jean-Baptiste	PERSON
Colbert	PERSON
supported	O
the	O
royal	O
spending	O
as	O
much	O
as	O
possible	O
.	O
Louis	O
II	O
de	O
Bourbon	O
had	O
captured	O
Franche-Comté	O
,	O
but	O
in	O
face	O
of	O
an	O
indefensible	O
position	O
,	O
Louis	PERSON
XIV	PERSON
agreed	O
to	O
a	O
peace	O
at	O
Aachen	LOCATION
.	O
Under	O
its	O
terms	O
,	O
Louis	PERSON
XIV	PERSON
did	O
not	O
annex	O
Franche-Comté	O
but	O
did	O
gain	O
Lille	LOCATION
.	O
Although	O
the	O
war	O
was	O
long	O
and	O
difficult	O
(	O
it	O
was	O
also	O
called	O
the	O
Nine	O
Years	O
War	O
)	O
,	O
its	O
results	O
were	O
inconclusive	O
.	O
Louis	PERSON
also	O
had	O
to	O
evacuate	O
Catalonia	LOCATION
and	O
the	O
Palatinate	O
.	O
In	O
1701	O
the	O
War	O
of	O
the	O
Spanish	O
Succession	O
began	O
.	O
Finally	O
,	O
a	O
compromise	O
was	O
achieved	O
with	O
the	O
Ultrecht	O
in	O
1713	O
.	O
Louis	PERSON
XIV	PERSON
died	O
in	O
1715	O
of	O
gangrene	O
.	O
Under	O
Cardinal	O
Fleury	PERSON
's	O
administration	O
,	O
peace	O
was	O
maintained	O
as	O
long	O
as	O
possible	O
.	O
Charles	PERSON
de	PERSON
Secondat	PERSON
,	O
baron	O
de	PERSON
Montesquieu	PERSON
described	O
the	O
separation	O
of	O
powers	O
.	O
In	O
February	O
1787	O
his	O
finance	O
minister	O
,	O
Charles	PERSON
Alexandre	PERSON
de	PERSON
Calonne	PERSON
,	O
convened	O
an	O
Assembly	O
of	O
Notables	O
,	O
a	O
group	O
of	O
nobles	O
,	O
clergy	O
,	O
bourgeoisie	O
,	O
and	O
bureaucrats	O
selected	O
in	O
order	O
to	O
bypass	O
the	O
local	O
parliaments	O
.	O
Paris	LOCATION
was	O
soon	O
consumed	O
with	O
riots	O
,	O
anarchy	O
,	O
and	O
widespread	O
looting	O
.	O
Because	O
of	O
this	O
new	O
period	O
of	O
instability	O
,	O
the	O
state	O
was	O
struck	O
for	O
several	O
weeks	O
in	O
July	O
and	O
August	O
1789	O
by	O
the	O
Great	O
Fear	O
,	O
a	O
period	O
of	O
violent	O
class	O
conflict	O
.	O
Considered	O
to	O
be	O
a	O
precursor	O
to	O
modern	O
international	O
rights	O
instruments	O
and	O
using	O
the	O
U.S.	O
Declaration	O
of	O
Independence	O
as	O
a	O
model	O
,	O
it	O
defined	O
a	O
set	O
of	O
individual	O
rights	O
and	O
collective	O
rights	O
of	O
all	O
of	O
the	O
estates	O
as	O
one	O
.	O
However	O
,	O
the	O
trial	O
progressed	O
and	O
Louis	PERSON
XVI	PERSON
was	O
executed	O
by	O
guillotine	O
on	O
January	O
21	O
,	O
1793	O
.	O
What	O
remained	O
of	O
a	O
national	O
government	O
depended	O
on	O
the	O
support	O
of	O
the	O
insurrectionary	O
Commune	O
.	O
As	O
a	O
consequence	O
of	O
these	O
actions	O
,	O
however	O
,	O
Robespierre	PERSON
's	O
own	O
popular	O
support	O
eroded	O
markedly	O
.	O
The	O
possibility	O
of	O
foreign	O
interference	O
had	O
vanished	O
with	O
the	O
failure	O
of	O
the	O
First	O
Coalition	O
.	O
Napoleon	PERSON
captured	O
Malta	LOCATION
from	O
the	O
Knights	O
of	O
Saint	O
John	O
on	O
the	O
way	O
to	O
Egypt	LOCATION
.	O
The	O
Rosetta	O
Stone	O
was	O
discovered	O
during	O
this	O
campaign	O
and	O
Champollion	O
translated	O
it	O
.	O
The	O
Second	O
Coalition	O
was	O
beaten	O
and	O
peace	O
was	O
settled	O
in	O
two	O
distinct	O
treaties	O
:	O
The	O
Treaty	O
of	O
Lunéville	LOCATION
and	O
the	O
Treaty	O
of	O
Amiens	LOCATION
.	O
There	O
the	LOCATION
Russian	LOCATION
Empire	LOCATION
was	O
defeated	O
at	O
the	O
Battle	O
of	O
Friedland	O
.	O
In	O
1812	O
war	O
broke	O
out	O
with	O
Russia	LOCATION
,	O
engaging	O
Napoleon	LOCATION
in	O
the	O
disastrous	O
Patriotic	O
War	O
.	O
Napoleon	PERSON
was	O
largely	O
defeated	O
in	O
the	O
Battle	O
of	O
the	O
Nations	O
and	O
was	O
overwhelmed	O
by	O
much	O
larger	O
armies	O
during	O
the	O
Six	O
Days	O
Campaign	O
,	O
although	O
,	O
because	O
of	O
the	O
much	O
larger	O
amount	O
of	O
casualties	O
suffered	O
by	O
the	O
allies	O
,	O
the	O
Six	O
Days	O
Campaign	O
is	O
often	O
considered	O
a	O
tactical	O
masterpiece	O
.	O
Napoleon	PERSON
abdicated	O
on	O
6	O
April	O
1814	O
,	O
and	O
was	O
exiled	O
to	O
Elba	PERSON
.	O
The	O
conservative	O
Congress	O
of	O
Vienna	O
reversed	O
the	O
political	O
changes	O
that	O
had	O
occurred	O
during	O
the	O
wars	O
.	O
Napoleon	O
's	O
attempted	O
restoration	O
,	O
a	O
period	O
known	O
as	O
the	O
Hundred	O
Days	O
,	O
ended	O
with	O
his	O
final	O
defeat	O
at	O
Waterloo	LOCATION
in	O
1815	O
.	O
The	O
monarchy	O
was	O
subsequently	O
restored	O
and	O
Louis	PERSON
XVIII	PERSON
became	O
king	O
.	O
Louis	PERSON
XVIII	PERSON
was	O
the	O
younger	O
brother	O
of	O
Louis	PERSON
XVI	PERSON
,	O
and	O
reigned	O
from	O
1814	O
to	O
1824	O
.	O
Louis	PERSON
was	O
succeeded	O
in	O
turn	O
by	O
a	O
younger	O
brother	O
,	O
Charles	PERSON
X	PERSON
,	O
who	O
reigned	O
from	O
1824	O
to	O
1830	O
.	O
However	O
,	O
the	O
news	O
of	O
the	O
fall	O
of	O
Algiers	LOCATION
had	O
barely	O
reached	O
Paris	LOCATION
when	O
a	O
new	O
revolution	O
broke	O
out	O
and	O
quickly	O
resulted	O
in	O
a	O
change	O
of	O
regime	O
.	O
Charles	PERSON
X	PERSON
was	O
deposed	O
and	O
replaced	O
by	O
King	PERSON
Louis-Philippe	PERSON
in	O
what	O
is	O
known	O
as	O
the	O
July	O
Revolution	O
.	O
The	O
July	O
Revolution	O
is	O
traditionally	O
regarded	O
as	O
a	O
rising	O
of	O
the	O
bourgeoisie	O
against	O
the	O
absolute	O
monarchy	O
of	O
the	O
Bourbons	O
.	O
Working	O
behind	O
the	O
scenes	O
on	O
behalf	O
of	O
the	O
bourgeois	O
propertied	O
interests	O
was	O
Louis	PERSON
Adolphe	PERSON
Thiers	PERSON
.	O
Thiers	O
was	O
perfectly	O
willing	O
to	O
see	O
changes	O
made	O
in	O
the	O
government	O
so	O
long	O
as	O
property	O
was	O
not	O
harmed	O
.	O
Thiers	O
wanted	O
the	O
"	O
middle	O
class	O
accommodated	O
"	O
with	O
the	O
vote	O
realizing	O
that	O
,	O
ironically	O
,	O
although	O
the	O
petty	O
bourgeoisie	O
(	O
the	O
inn	O
and	O
cafe	O
keepers	O
,	O
restaurant	O
owners	O
,	O
wine	O
merchants	O
,	O
small	O
traders	O
,	O
shop	O
keepers	O
,	O
handicraftsmen	O
,	O
etc.	O
)	O
were	O
all	O
being	O
ruined	O
by	O
the	O
rise	O
of	O
the	O
larger	O
bourgeoisie	O
that	O
the	O
petty	O
bourgeoisie	O
remained	O
strong	O
supporters	O
of	O
property	O
interests	O
.	O
Lafayette	LOCATION
arrived	O
in	O
Paris	LOCATION
on	O
July	O
29	O
.	O
Unlike	O
Thiers	LOCATION
,	O
Lafayette	LOCATION
knew	O
that	O
the	O
Bourbons	O
were	O
finished	O
.	O
At	O
72	O
years	O
of	O
age	O
,	O
Lafayette	LOCATION
felt	O
himself	O
to	O
be	O
too	O
old	O
for	O
the	O
task	O
of	O
forming	O
and	O
serving	O
as	O
the	O
President	O
of	O
a	O
new	O
republic	O
.	O
Consequently	O
,	O
Louis-Philippe	PERSON
became	O
"	O
king	O
by	O
the	O
grace	O
of	O
the	O
barricades	O
.	O
"	O
Louis-Philippe	PERSON
's	PERSON
"	O
July	O
Monarchy	O
"	O
(	O
1830	O
--	O
1848	O
)	O
is	O
generally	O
seen	O
as	O
a	O
period	O
during	O
which	O
the	O
haute	O
bourgeoisie	O
(	O
high	O
bourgeoisie	O
)	O
was	O
dominant	O
.	O
This	O
term	O
is	O
a	O
recognition	O
that	O
the	O
July	O
Monarchy	O
was	O
controlled	O
by	O
one	O
faction	O
of	O
the	O
bourgeoisie	O
class	O
--	O
finance	O
capitalists	O
.	O
This	O
faction	O
consisted	O
of	O
the	O
bankers	O
(	O
particularly	O
the	O
Rothchilds	O
,	O
the	O
stock	O
exchange	O
magnates	O
,	O
owners	O
of	O
railroad	O
,	O
iron	O
and	O
coal	O
mines	O
that	O
that	O
part	O
of	O
the	O
landed	O
proprietors	O
associated	O
with	O
finance	O
capital	O
.	O
Indeed	O
,	O
government	O
during	O
the	O
July	O
Monarchy	O
has	O
been	O
called	O
a	O
"	O
finance	O
aristocracy	O
.	O
"	O
Indeed	O
,	O
LaFayette	PERSON
's	O
good	O
friend	O
,	O
Jacques	PERSON
Laffitte	PERSON
a	O
liberal	O
banker	O
and	O
supporter	O
of	O
the	O
July	O
Revolution	O
celebrated	O
after	O
the	O
crowning	O
of	O
Louis-Philippe	PERSON
by	O
stating	O
that	O
"	O
From	O
now	O
on	O
bankers	O
will	O
rule	O
.	O
"	O
To	O
honour	O
the	O
victims	O
of	O
the	O
July	O
Revolution	O
,	O
Hector	O
Berlioz	PERSON
composed	O
a	O
Requiem	O
;	O
he	O
also	O
rearranged	O
La	PERSON
Marseillaise	PERSON
,	O
which	O
would	O
become	O
the	O
French	O
national	O
anthem	O
.	O
On	O
November	O
22	O
,	O
1831	O
in	O
Lyon	LOCATION
the	O
silk	O
workers	O
revolted	O
and	O
took	O
over	O
the	O
town	O
hall	O
in	O
protest	O
of	O
recent	O
salary	O
reductions	O
and	O
working	O
conditions	O
.	O
The	O
revolt	O
was	O
vigorously	O
put	O
down	O
by	O
Casimir	PERSON
Perier	PERSON
.	O
The	O
next	O
spring	O
,	O
on	O
June	O
5	O
--	O
6	O
,	O
1832	O
,	O
the	O
workers	O
of	O
Paris	LOCATION
flowed	O
out	O
into	O
the	O
streets	O
and	O
threw	O
up	O
barricades	O
again	O
.	O
This	O
was	O
on	O
the	O
occasion	O
of	O
the	O
funeral	O
of	O
General	O
Jean	PERSON
Maximilien	PERSON
Lamarque	PERSON
.	O
General	O
Lamarque	PERSON
as	O
a	O
well-known	O
opponent	O
of	O
Louis-Philippe	PERSON
.	O
Although	O
supported	O
by	O
workers	O
in	O
Paris	LOCATION
,	O
this	O
revolt	O
was	O
brutally	O
suppressed	O
.	O
Because	O
of	O
the	O
constant	O
threats	O
to	O
the	O
throne	O
,	O
the	O
July	O
Monarchy	O
began	O
to	O
rule	O
with	O
a	O
stronger	O
and	O
stronger	O
hand	O
.	O
The	O
climaxing	O
banquet	O
was	O
scheduled	O
for	O
February	O
22	O
,	O
1848	O
in	O
Paris	LOCATION
.	O
On	O
February	O
22	O
,	O
citizens	O
of	O
all	O
classes	O
poured	O
out	O
onto	O
the	O
streets	O
of	O
Paris	LOCATION
in	O
a	O
revolt	O
against	O
the	O
July	O
Monarchy	O
.	O
In	O
reality	O
Lamartine	PERSON
was	O
the	O
virtual	O
head	O
of	O
government	O
in	O
1848	O
.	O
The	O
era	O
saw	O
great	O
industrialization	O
,	O
urbanization	O
(	O
including	O
the	O
massive	O
rebuilding	O
of	O
Paris	LOCATION
by	O
Baron	O
Haussmann	PERSON
)	O
and	O
economic	O
growth	O
,	O
but	O
Napoleon	PERSON
III	PERSON
's	PERSON
foreign	O
policies	O
were	O
not	O
so	O
successful	O
.	O
This	O
led	O
to	O
the	O
French	O
intervention	O
in	O
Mexico	LOCATION
,	O
which	O
turned	O
out	O
to	O
be	O
a	O
failure	O
.	O
Prince	PERSON
Leopold	PERSON
was	O
a	O
part	O
of	O
the	O
Prussian	O
royal	O
family	O
.	O
Surrounded	O
Paris	LOCATION
and	O
was	O
forced	O
to	O
surrender	O
on	O
January	O
28	O
,	O
1871	O
.	O
The	O
last	O
straw	O
was	O
the	O
Siege	O
of	O
Paris	O
.	O
Railways	O
became	O
a	O
national	O
medium	O
for	O
the	O
modernization	O
of	O
backward	O
regions	O
,	O
and	O
a	O
leading	O
advocate	O
of	O
this	O
approach	O
was	O
the	O
poet-politician	O
Alphonse	PERSON
de	PERSON
Lamartine	PERSON
.	O
Much	O
of	O
the	O
equipment	O
was	O
imported	O
from	O
Britain	LOCATION
and	O
therefore	O
did	O
not	O
stimulate	O
machinery	O
makers	O
.	O
Although	O
starting	O
the	O
whole	O
system	O
at	O
once	O
was	O
politically	O
expedient	O
,	O
it	O
delayed	O
completion	O
,	O
and	O
forced	O
even	O
more	O
reliance	O
on	O
temporary	O
experts	O
brought	O
in	O
from	O
Britain	LOCATION
.	O
Critics	O
such	O
as	O
Emile	PERSON
Zola	PERSON
complained	O
that	O
it	O
never	O
overcame	O
the	O
corruption	O
of	O
the	O
political	O
system	O
,	O
but	O
rather	O
contributed	O
to	O
it	O
.	O
The	O
railways	O
probably	O
helped	O
the	O
industrial	O
revolution	O
in	O
France	LOCATION
by	O
facilitating	O
a	O
national	O
market	O
for	O
raw	O
materials	O
,	O
wines	O
,	O
cheeses	O
,	O
and	O
imported	O
manufactured	O
products	O
.	O
Because	O
of	O
the	O
revolutionary	O
unrest	O
in	O
Paris	LOCATION
,	O
the	O
center	O
of	O
the	O
Thiers	O
government	O
was	O
located	O
at	O
Versailles	LOCATION
.	O
Meanwhile	O
,	O
the	O
people	O
of	O
Paris	LOCATION
,	O
however	O
,	O
were	O
seething	O
at	O
the	O
thought	O
of	O
peace	O
with	O
Germany	LOCATION
under	O
the	O
humiliating	O
terms	O
proposed	O
by	O
Bismarck	O
.	O
On	O
October	O
31	O
,	O
1870	O
and	O
on	O
January	O
22	O
,	O
1871	O
,	O
the	O
people	O
of	O
Paris	LOCATION
rose	O
up	O
in	O
premature	O
and	O
unsuccessful	O
uprisings	O
.	O
Adolphe	PERSON
Thiers	PERSON
could	O
recognize	O
a	O
revolutionary	O
situation	O
when	O
he	O
saw	O
it	O
.	O
The	O
people	O
of	O
Paris	LOCATION
revolted	O
and	O
threw	O
up	O
the	O
barricades	O
just	O
as	O
they	O
had	O
in	O
1830	O
and	O
1848	O
.	O
The	O
Paris	O
Commune	O
was	O
born	O
.	O
This	O
time	O
the	O
Hotel	O
de	O
Ville	O
became	O
the	O
seat	O
of	O
an	O
actual	O
government	O
.	O
On	O
March	O
22	O
,	O
1871	O
,	O
the	O
people	O
of	O
Lyon	LOCATION
overtook	O
their	O
local	O
government	O
and	O
proclaimed	O
a	O
"	O
Commune	O
.	O
"	O
From	O
the	O
first	O
,	O
the	O
new	O
government	O
of	O
Paris	LOCATION
reflected	O
something	O
totally	O
new	O
.	O
The	O
industrial	O
Revolution	O
in	O
France	LOCATION
had	O
been	O
in	O
full	O
bloom	O
for	O
some	O
time	O
,	O
by	O
1871	O
.	O
Consequently	O
,	O
a	O
large	O
number	O
of	O
Paris	LOCATION
citizens	O
who	O
came	O
out	O
in	O
the	O
streets	O
in	O
revolt	O
and	O
who	O
subsequently	O
formed	O
the	O
new	O
government	O
of	O
the	O
Paris	O
Commune	O
were	O
working	O
class	O
citizens	O
who	O
worked	O
for	O
wages	O
in	O
the	O
factories	O
in	O
the	O
city	O
.	O
These	O
working	O
class	O
citizens	O
directed	O
the	O
tone	O
of	O
the	O
government	O
of	O
the	O
Paris	O
Commune	O
in	O
a	O
way	O
that	O
represented	O
working	O
class	O
interests	O
.	O
Thus	O
,	O
the	O
Paris	O
Commune	O
became	O
the	O
first	O
"	O
proletarian	O
revolution	O
"	O
in	O
history	O
.	O
The	O
government	O
of	O
the	O
Paris	O
Commune	O
was	O
divided	O
,	O
but	O
the	O
political	O
divisions	O
were	O
totally	O
new	O
to	O
the	O
political	O
scene	O
.	O
The	O
minority	O
opposition	O
within	O
the	O
communard	O
government	O
were	O
anarchists	O
and	O
followers	O
of	O
Pierre	PERSON
Joseph	PERSON
Proudhon	PERSON
(	O
1809	O
--	O
1855	O
)	O
.	O
Indeed	O
the	O
Commune	O
is	O
faulted	O
with	O
not	O
having	O
taken	O
over	O
control	O
of	O
the	O
Bank	O
of	O
France	O
which	O
continued	O
normal	O
operations	O
within	O
Paris	LOCATION
without	O
any	O
interruption	O
during	O
the	O
whole	O
life	O
of	O
the	O
Commune	O
.	O
Such	O
a	O
measure	O
might	O
have	O
made	O
the	O
Commune	O
successful	O
against	O
the	O
Thiers	O
government	O
.	O
and	O
that	O
the	O
only	O
way	O
to	O
accomplish	O
this	O
was	O
to	O
"	O
take	O
over	O
the	O
Bank	O
of	O
France	O
.	O
"	O
The	O
repression	O
of	O
the	O
Commune	O
was	O
bloody	O
.	O
Hundreds	O
were	O
executed	O
in	O
front	O
of	O
the	O
Communards	O
'	O
Wall	O
in	O
the	O
Père	O
Lachaise	O
cemetery	O
,	O
while	O
thousands	O
of	O
others	O
were	O
marched	O
to	O
Versailles	LOCATION
for	O
trials	O
.	O
Many	O
more	O
were	O
wounded	O
,	O
and	O
perhaps	O
as	O
many	O
as	O
50,000	O
later	O
executed	O
or	O
imprisoned	O
;	O
7,000	O
were	O
exiled	O
to	O
New	LOCATION
Caledonia	LOCATION
.	O
Under	O
the	O
constitution	O
,	O
President	O
MacMahon	PERSON
was	O
required	O
to	O
pick	O
a	O
"	O
premier	O
"	O
to	O
actually	O
lead	O
the	O
day-to-day	O
affairs	O
of	O
the	O
government	O
.	O
The	O
most	O
natural	O
leader	O
to	O
pick	O
following	O
this	O
republican	O
victory	O
at	O
the	O
polls	O
would	O
have	O
been	O
Leon	PERSON
Gambetta	PERSON
.	O
However	O
,	O
the	O
old	O
Marshal	O
was	O
dead	O
set	O
against	O
Gambetta	PERSON
and	O
chose	O
,	O
instead	O
,	O
moderate	O
Armand	PERSON
Dufaure	PERSON
.	O
Next	O
Marshall	PERSON
MacMahon	PERSON
chose	O
conservative	O
Jules	PERSON
Simon	PERSON
.	O
During	O
his	O
life	O
Henri	PERSON
,	O
comte	O
de	O
Chambord	O
,	O
who	O
,	O
as	O
the	O
grandson	O
of	O
Charles	PERSON
X	PERSON
,	O
had	O
refused	O
to	O
abandon	O
the	O
fleur-de-lys	O
and	O
the	O
white	O
flag	O
.	O
This	O
was	O
the	O
start	O
of	O
the	O
Boulanger	O
era	O
,	O
and	O
was	O
the	O
start	O
of	O
another	O
time	O
of	O
threats	O
.	O
Some	O
of	O
them	O
founded	O
Action	O
Française	O
in	O
1898	O
,	O
during	O
the	O
Dreyfus	O
Affair	O
,	O
which	O
became	O
an	O
influential	O
movement	O
throughout	O
the	O
1930s	O
,	O
in	O
particular	O
among	O
the	O
intellectuals	O
of	O
Paris	LOCATION
'	O
Quartier	O
Latin	O
.	O
The	O
initial	O
republic	O
was	O
in	O
effect	O
led	O
by	O
pro-royalists	O
,	O
but	O
republicans	O
(	O
the	O
"	O
Radicals	O
"	O
)	O
and	O
bonapartists	O
scrambled	O
for	O
power	O
.	O
The	O
writer	O
Emile	PERSON
Zola	PERSON
published	O
an	O
impassioned	O
editorial	O
on	O
the	O
injustice	O
,	O
and	O
was	O
himself	O
condemned	O
by	O
the	O
government	O
for	O
libel	O
.	O
In	O
1889	O
the	O
Exposition	O
Universelle	O
took	O
place	O
in	O
Paris	LOCATION
,	O
and	O
the	O
Eiffel	O
Tower	O
was	O
built	O
as	O
a	O
temporary	O
gate	O
to	O
the	O
fair	O
.	O
The	O
original	O
plan	O
was	O
to	O
continue	O
southwest	O
and	O
attack	O
Paris	LOCATION
from	O
the	O
west	O
.	O
On	O
the	O
Western	O
Front	O
the	O
small	O
improvised	O
trenches	O
of	O
the	O
first	O
few	O
months	O
rapidly	O
grew	O
deeper	O
and	O
more	O
complex	O
,	O
gradually	O
becoming	O
vast	O
areas	O
of	O
interlocking	O
defensive	O
works	O
.	O
On	O
the	O
Western	O
Front	O
it	O
was	O
typically	O
between	O
100	O
and	O
300	O
yards	O
(	O
90	O
--	O
275	O
m	O
)	O
,	O
though	O
sometimes	O
much	O
less	O
.	O
The	O
use	O
in	O
large	O
quantity	O
of	O
these	O
light	O
tanks	O
by	O
Jean-Baptiste	PERSON
Estienne	PERSON
can	O
be	O
considered	O
a	O
decisive	O
evolution	O
in	O
World	O
War	O
I	O
's	O
strategies	O
.	O
In	O
March	O
1918	O
Germany	LOCATION
launched	O
the	O
last	O
major	O
offensive	O
on	O
the	O
Western	O
Front	O
.	O
Other	O
Central	O
Power	O
strongholds	O
in	O
Europe	LOCATION
had	O
fallen	O
,	O
and	O
in	O
early	O
October	O
,	O
when	O
a	O
new	O
government	O
assumed	O
power	O
in	O
Germany	LOCATION
,	O
it	O
asked	O
for	O
an	O
armistice	O
.	O
This	O
regime	O
sought	O
to	O
collaborate	O
with	O
Germany	LOCATION
.	O
Isolated	O
opposers	O
eventually	O
formed	O
a	O
real	O
movement	O
:	O
the	O
Resistants	O
.	O
Increasing	O
repression	O
culminated	O
in	O
the	O
complete	O
destruction	O
and	O
extermination	O
of	O
the	O
village	O
of	O
Oradour-sur-Glane	O
,	O
at	O
the	O
height	O
of	O
the	O
Battle	O
of	O
Normandy	O
.	O
Vichy	PERSON
's	O
purpose	O
in	O
this	O
respect	O
was	O
to	O
preserve	O
its	O
sovereignty	O
.	O
The	O
Vichy	O
regime	O
fled	O
to	O
Germany	LOCATION
.	O
After	O
a	O
short	O
period	O
of	O
provisional	O
government	O
initially	O
led	O
by	O
General	O
Charles	PERSON
de	PERSON
Gaulle	PERSON
,	O
a	O
new	O
constitution	O
(	O
13	O
October	O
1946	O
)	O
established	O
the	LOCATION
Fourth	LOCATION
Republic	LOCATION
under	O
a	O
parliamentary	O
form	O
of	O
government	O
controlled	O
by	O
a	O
series	O
of	O
coalitions	O
.	O
During	O
the	O
following	O
16	O
years	O
the	O
French	O
Colonial	LOCATION
Empire	LOCATION
would	O
disintegrate	O
.	O
Vietnam	LOCATION
was	O
divided	O
in	O
two	O
states	O
while	O
Cambodia	LOCATION
and	O
Laos	LOCATION
were	O
made	O
independent	O
.	O
In	O
1956	O
another	O
crisis	O
struck	O
French	O
colonies	O
,	O
this	O
time	O
in	O
Egypt	LOCATION
.	O
The	O
new	O
constitution	O
of	O
the	O
French	O
Fifth	O
Republic	O
,	O
introduced	O
on	O
5	O
October	O
1958	O
,	O
gave	O
greater	O
powers	O
to	O
the	O
presidency	O
.	O
Algeria	LOCATION
became	O
independent	O
in	O
1962	O
.	O
The	O
hijacking	O
was	O
a	O
failure	O
for	O
the	O
terrorist	O
group	O
,	O
known	O
as	O
the	O
GIA	O
after	O
an	O
intervention	O
from	O
the	O
GIGN	PERSON
in	O
Marseille	LOCATION
,	O
where	O
the	O
plane	O
was	O
grounded	O
.	O
More	O
terrorist	O
attacks	O
would	O
happen	O
and	O
these	O
culminated	O
into	O
the	O
1995	O
Paris	O
Metro	O
bombing	O
.	O
French	O
troops	O
joined	O
the	O
1999	O
NATO	O
bombing	O
of	O
the	LOCATION
Federal	LOCATION
Republic	LOCATION
of	LOCATION
Yugoslavia	LOCATION
.	O
In	O
2002	O
Alliance	O
Base	O
,	O
an	O
international	O
Counterterrorist	O
Intelligence	O
Center	O
,	O
was	O
secretly	O
established	O
in	O
Paris	LOCATION
.	O
Jacques	PERSON
Chirac	PERSON
was	O
reelected	O
in	O
2002	O
,	O
mainly	O
because	O
his	O
socialist	O
rival	O
Lionel	PERSON
Jospin	PERSON
was	O
defeated	O
by	O
the	O
extreme	O
right	O
wing	O
candidate	O
Jean-Marie	PERSON
Le	PERSON
Pen	PERSON
.	O
At	O
the	O
end	O
of	O
his	O
second	O
term	O
Jacques	PERSON
Chirac	PERSON
chose	O
not	O
to	O
run	O
again	O
at	O
the	O
age	O
of	O
74	O
.	O
