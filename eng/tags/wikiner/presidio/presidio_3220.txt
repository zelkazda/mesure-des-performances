There	O
are	O
also	O
Danish	O
language	O
communities	O
in	O
Argentina	LOCATION
,	O
the	O
U.S.	LOCATION
and	O
Canada	LOCATION
.	O
Moreover	O
,	O
the	O
øy	O
diphthong	O
changed	O
into	O
ø	O
as	O
well	O
,	O
as	O
in	O
the	O
Old	O
Norse	O
word	O
for	O
"	O
island	O
"	O
.	O
Danish	O
is	O
the	O
national	O
language	O
of	O
Denmark	LOCATION
,	O
and	O
one	O
of	O
two	O
official	O
languages	O
of	O
the	O
Faroes	O
(	O
alongside	O
Faroese	O
)	O
.	O
Until	O
2009	O
,	O
it	O
had	O
also	O
been	O
one	O
of	O
two	O
official	O
languages	O
of	O
Greenland	LOCATION
(	O
alongside	O
Greenlandic	O
)	O
.	O
In	O
addition	O
,	O
there	O
is	O
a	O
small	O
community	O
of	O
Danish	O
speakers	O
in	O
Southern	LOCATION
Schleswig	LOCATION
,	O
the	O
portion	O
of	O
Germany	LOCATION
bordering	O
Denmark	LOCATION
,	O
where	O
it	O
is	O
an	O
officially	O
recognized	O
regional	O
language	O
,	O
just	O
as	O
German	O
is	O
north	O
of	O
the	O
border	O
.	O
Furthermore	O
,	O
Danish	O
is	O
one	O
of	O
the	O
official	O
languages	O
of	O
the	O
European	O
Union	O
and	O
one	O
of	O
the	O
working	O
languages	O
of	O
the	O
Nordic	O
Council	O
.	O
There	O
is	O
no	O
law	O
stipulating	O
an	O
official	O
language	O
for	O
Denmark	LOCATION
,	O
making	O
Danish	O
the	O
de	O
facto	O
language	O
only	O
.	O
Standard	O
Danish	O
(	O
rigsdansk	O
)	O
is	O
the	O
language	O
based	O
on	O
dialects	O
spoken	O
in	O
and	O
around	O
the	O
capital	O
,	O
Copenhagen	LOCATION
.	O
More	O
than	O
25	O
%	O
of	O
all	O
Danish	O
speakers	O
live	O
in	O
the	O
metropolitan	O
area	O
of	O
the	O
capital	O
and	O
most	O
government	O
agencies	O
,	O
institutions	O
and	O
major	O
businesses	O
keep	O
their	O
main	O
offices	O
in	O
Copenhagen	LOCATION
,	O
something	O
that	O
has	O
resulted	O
in	O
a	O
very	O
homogeneous	O
national	O
speech	O
norm	O
.	O
In	O
contrast	O
,	O
though	O
Oslo	LOCATION
(	O
Norway	LOCATION
)	O
and	O
Stockholm	LOCATION
(	O
Sweden	LOCATION
)	O
are	O
quite	O
dominant	O
in	O
terms	O
of	O
speech	O
standards	O
,	O
cities	O
like	O
Bergen	LOCATION
,	O
Göteborg	LOCATION
and	O
the	O
Malmö	LOCATION
--	O
Lund	O
region	O
are	O
large	O
and	O
influential	O
enough	O
to	O
create	O
secondary	O
regional	O
norms	O
,	O
making	O
the	O
standard	O
language	O
more	O
varied	O
than	O
is	O
the	O
case	O
with	O
Danish	O
.	O
Danish	O
is	O
divided	O
into	O
three	O
distinct	O
dialect	O
groups	O
:	O
The	O
background	O
for	O
this	O
lies	O
in	O
the	O
loss	O
of	O
the	O
originally	O
Danish	O
provinces	O
of	O
Blekinge	LOCATION
,	O
Halland	LOCATION
and	O
Scania	LOCATION
to	O
Sweden	LOCATION
in	O
1658	O
.	O
The	O
sound	O
system	O
of	O
Danish	O
is	O
in	O
many	O
ways	O
unique	O
among	O
the	O
world	O
's	O
languages	O
.	O
At	O
the	O
beginning	O
of	O
a	O
word	O
or	O
after	O
a	O
consonant	O
,	O
it	O
is	O
pronounced	O
as	O
a	O
uvular	O
fricative	O
,	O
[	O
ʁ	O
]	O
,	O
but	O
in	O
most	O
other	O
positions	O
it	O
is	O
either	O
realized	O
as	O
a	O
non-syllabic	O
low	O
central	O
vowel	O
,	O
[	O
ɐ̯	O
]	O
(	O
which	O
is	O
almost	O
identical	O
to	O
how	O
/r/	O
is	O
often	O
pronounced	O
in	O
German	O
)	O
or	O
simply	O
coalesces	O
with	O
the	O
preceding	O
vowel	O
.	O
For	O
example	O
the	O
present	O
tense	O
form	O
of	O
the	O
Danish	O
infinitive	O
verb	O
spise	O
(	O
"	O
to	O
eat	O
"	O
)	O
is	O
spiser	O
;	O
this	O
form	O
is	O
the	O
same	O
regardless	O
of	O
whether	O
the	O
subject	O
is	O
in	O
the	O
first	O
,	O
second	O
,	O
or	O
third	O
person	O
,	O
or	O
whether	O
it	O
is	O
singular	O
or	O
plural	O
.	O
Standard	O
Danish	O
nouns	O
fall	O
into	O
only	O
two	O
grammatical	O
genders	O
:	O
common	O
and	O
neuter	O
,	O
while	O
some	O
dialects	O
still	O
often	O
have	O
masculine	O
,	O
feminine	O
and	O
neuter	O
.	O
While	O
the	O
majority	O
of	O
Danish	O
nouns	O
(	O
ca.	O
75	O
%	O
)	O
have	O
the	O
common	O
gender	O
,	O
and	O
neuter	O
is	O
often	O
used	O
for	O
inanimate	O
objects	O
,	O
the	O
genders	O
of	O
nouns	O
are	O
not	O
generally	O
predictable	O
and	O
must	O
in	O
most	O
cases	O
be	O
memorized	O
.	O
Danish	O
words	O
are	O
largely	O
derived	O
from	O
the	O
Old	O
Norse	O
language	O
,	O
with	O
new	O
words	O
formed	O
by	O
compounding	O
.	O
A	O
large	O
percentage	O
of	O
Danish	O
words	O
,	O
however	O
,	O
hail	O
from	O
Middle	O
Low	O
German	O
(	O
for	O
example	O
,	O
betale	O
=	O
to	O
pay	O
)	O
.	O
One	O
peculiar	O
feature	O
of	O
the	O
Danish	O
language	O
is	O
the	O
fact	O
that	O
numerals	O
from	O
40	O
through	O
90	O
are	O
(	O
somewhat	O
like	O
the	O
French	O
numerals	O
80	O
and	O
90	O
)	O
based	O
on	O
a	O
vigesimal	O
system	O
,	O
formerly	O
also	O
used	O
in	O
Norwegian	O
and	O
Swedish	O
.	O
Thus	O
,	O
in	O
modern	O
Danish	O
forty	O
is	O
called	O
fyrre	O
derived	O
from	O
the	O
original	O
fyrretyve	O
(	O
i.e.	O
four	O
score	O
)	O
,	O
which	O
is	O
now	O
only	O
used	O
in	O
extremely	O
formal	O
settings	O
or	O
for	O
humorous	O
effect	O
.	O
For	O
large	O
numbers	O
(	O
one	O
billion	O
or	O
larger	O
)	O
,	O
Danish	O
uses	O
the	O
long	O
scale	O
,	O
so	O
that	O
e.g.	O
one	O
U.S.	O
billion	O
(	O
1,000,000,000	O
)	O
is	O
called	O
milliard	O
,	O
and	O
one	O
U.S.	O
trillion	O
(	O
1,000,000,000,000	O
)	O
is	O
billion	O
.	O
The	O
same	O
spelling	O
reform	O
changed	O
the	O
spelling	O
of	O
a	O
few	O
common	O
words	O
,	O
such	O
as	O
the	O
past	O
tense	O
vilde	O
(	O
would	O
)	O
,	O
kunde	PERSON
(	O
could	O
)	O
and	O
skulde	O
(	O
should	O
)	O
,	O
to	O
their	O
current	O
forms	O
of	O
ville	LOCATION
,	O
kunne	O
and	O
skulle	O
(	O
making	O
them	O
identical	O
to	O
the	O
infinitives	O
in	O
writing	O
,	O
as	O
they	O
are	O
in	O
speech	O
)	O
,	O
and	O
did	O
away	O
with	O
the	O
practice	O
of	O
capitalizing	O
all	O
nouns	O
,	O
which	O
is	O
still	O
done	O
in	O
German	O
.	O
