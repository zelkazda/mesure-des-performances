Banks	O
was	O
a	O
member	O
of	O
the	O
England	LOCATION
national	O
team	O
that	O
won	O
the	O
1966	O
World	O
Cup	O
.	O
Banks	O
,	O
born	O
in	O
Sheffield	LOCATION
was	O
a	O
careful	O
student	O
of	O
goalkeepers	O
during	O
childhood	O
.	O
Banks	O
played	O
in	O
local	O
colliery	O
football	O
as	O
a	O
boy	O
and	O
was	O
offered	O
an	O
apprenticeship	O
by	O
Chesterfield	LOCATION
after	O
initially	O
going	O
to	O
work	O
as	O
a	O
coal	O
bagger	O
and	O
then	O
as	O
a	O
bricklayer	O
on	O
leaving	O
school	O
.	O
On	O
his	O
return	O
he	O
was	O
offered	O
a	O
full-time	O
contract	O
by	O
the	O
Chesterfield	LOCATION
manager	O
,	O
Teddy	PERSON
Davison	PERSON
.	O
He	O
reached	O
the	O
two-legged	O
final	O
of	O
the	O
FA	O
Youth	O
Cup	O
with	O
Chesterfield	LOCATION
in	O
1956	O
,	O
losing	O
4	O
--	O
3	O
on	O
aggregate	O
to	O
the	O
Manchester	O
United	O
team	O
of	O
the	O
famous	O
Busby	O
Babes	O
.	O
He	O
played	O
just	O
23	O
games	O
for	O
the	O
club	O
before	O
First	O
Division	O
Leicester	O
City	O
offered	O
Chesterfield	LOCATION
£	O
7,000	O
in	O
the	O
summer	O
of	O
1959	O
.	O
Banks	O
'	O
career	O
started	O
to	O
rise	O
rapidly	O
from	O
this	O
point	O
.	O
In	O
1961	O
,	O
Leicester	LOCATION
City	LOCATION
beat	O
Sheffield	LOCATION
United	O
to	O
reach	O
the	O
FA	O
Cup	O
final	O
at	O
Wembley	O
,	O
the	O
first	O
of	O
three	O
they	O
would	O
manage	O
that	O
decade	O
.	O
Their	O
opponents	O
were	O
Tottenham	PERSON
Hotspur	PERSON
,	O
who	O
were	O
a	O
cut	O
above	O
everyone	O
else	O
having	O
won	O
the	O
First	O
Division	O
title	O
with	O
ease	O
and	O
style	O
.	O
At	O
the	O
time	O
,	O
Ron	PERSON
Springett	PERSON
was	O
the	O
goalkeeper	O
for	O
England	LOCATION
,	O
but	O
after	O
the	O
1962	O
World	O
Cup	O
in	O
Chile	LOCATION
,	O
a	O
new	O
coach	O
was	O
appointed	O
in	O
former	O
England	LOCATION
right	O
back	O
Alf	PERSON
Ramsey	PERSON
.	O
He	O
knew	O
that	O
he	O
just	O
needed	O
to	O
find	O
a	O
squad	O
for	O
the	O
final	O
stages	O
as	O
England	LOCATION
were	O
hosting	O
the	O
event	O
and	O
did	O
n't	O
need	O
to	O
undergo	O
a	O
qualifying	O
campaign	O
.	O
In	O
goal	O
,	O
Banks	PERSON
was	O
checked	O
out	O
by	O
Ramsey	PERSON
for	O
the	O
first	O
time	O
in	O
April	O
1963	O
against	O
Scotland	LOCATION
at	O
Wembley	LOCATION
.	O
Though	O
England	LOCATION
lost	O
2	O
--	O
1	O
,	O
Banks	O
gained	O
plaudits	O
and	O
Ramsey	PERSON
was	O
pleased	O
with	O
his	O
performance	O
.	O
He	O
played	O
in	O
13	O
of	O
the	O
next	O
15	O
internationals	O
,	O
including	O
a	O
1	O
--	O
1	O
draw	O
against	O
Brazil	LOCATION
.	O
Meanwhile	O
,	O
a	O
month	O
after	O
his	O
international	O
bow	O
,	O
Banks	PERSON
was	O
back	O
at	O
Wembley	LOCATION
with	O
Leicester	LOCATION
for	O
another	O
FA	O
Cup	O
final	O
,	O
this	O
time	O
against	O
Manchester	LOCATION
United	LOCATION
.	O
Banks	O
failed	O
to	O
hold	O
a	O
Bobby	PERSON
Charlton	PERSON
shot	O
from	O
distance	O
which	O
gave	O
a	O
chance	O
to	O
David	PERSON
Herd	PERSON
.	O
After	O
that	O
things	O
got	O
worse	O
for	O
England	LOCATION
's	O
newest	O
keeper	O
,	O
when	O
Denis	PERSON
Law	PERSON
wrong-footed	O
Banks	O
with	O
a	O
smart	O
shot	O
on	O
the	O
turn	O
to	O
put	O
United	O
2	O
--	O
0	O
ahead	O
.	O
After	O
Leicester	LOCATION
had	O
pulled	O
one	O
back	O
through	O
a	O
diving	O
header	O
from	O
Ken	PERSON
Keyworth	PERSON
,	O
Banks	O
leapt	O
high	O
in	O
the	O
air	O
to	O
claim	O
a	O
high	O
cross	O
from	O
Johnny	PERSON
Giles	PERSON
,	O
only	O
to	O
drop	O
the	O
ball	O
at	O
Herd	O
's	O
feet	O
.	O
Herd	O
scored	O
his	O
second	O
to	O
conclude	O
a	O
3	O
--	O
1	O
win	O
.	O
By	O
1965	O
,	O
Banks	PERSON
was	O
indisputably	O
the	O
first-choice	O
England	LOCATION
goalkeeper	O
.	O
Banks	O
was	O
not	O
greatly	O
tested	O
,	O
but	O
it	O
was	O
hugely	O
encouraging	O
that	O
he	O
emerged	O
from	O
the	O
group	O
with	O
three	O
clean	O
sheets	O
from	O
three	O
games	O
,	O
a	O
trend	O
that	O
continued	O
when	O
England	LOCATION
beat	O
a	O
physical	O
Argentina	LOCATION
side	O
1	O
--	O
0	O
in	O
the	O
last	O
eight	O
,	O
with	O
Geoff	PERSON
Hurst	PERSON
scoring	O
with	O
a	O
header	O
.	O
Bobby	PERSON
Charlton	PERSON
scored	O
twice	O
in	O
the	O
semi	O
final	O
against	O
Portugal	LOCATION
before	O
a	O
late	O
penalty	O
was	O
conceded	O
by	O
Jack	PERSON
Charlton	PERSON
handling	O
the	O
ball	O
.	O
Banks	O
was	O
finally	O
beaten	O
after	O
43	O
minutes	O
when	O
Eusébio	O
put	O
away	O
the	O
spot	O
kick	O
to	O
his	O
right	O
.	O
That	O
said	O
,	O
England	LOCATION
had	O
won	O
2	O
--	O
1	O
and	O
were	O
in	O
the	O
final	O
,	O
where	O
West	LOCATION
Germany	LOCATION
awaited	O
.	O
It	O
was	O
England	LOCATION
who	O
dominated	O
the	O
final	O
but	O
it	O
was	O
Banks	PERSON
who	O
was	O
beaten	O
first	O
.	O
A	O
weak	O
header	O
from	O
Ray	PERSON
Wilson	PERSON
handed	O
a	O
chance	O
to	O
Helmut	PERSON
Haller	PERSON
whose	O
shot	O
was	O
not	O
fierce	O
but	O
was	O
on	O
target	O
and	O
needed	O
dealing	O
with	O
.	O
Banks	O
thought	O
Jack	PERSON
Charlton	PERSON
was	O
going	O
to	O
clear	O
;	O
Charlton	PERSON
in	O
turn	O
thought	O
Banks	PERSON
had	O
it	O
covered	O
.	O
England	LOCATION
equalised	O
through	O
a	O
Geoff	PERSON
Hurst	PERSON
header	O
within	O
six	O
minutes	O
and	O
went	O
ahead	O
late	O
in	O
the	O
second	O
half	O
through	O
Martin	PERSON
Peters	PERSON
.	O
Banks	O
had	O
little	O
to	O
do	O
during	O
the	O
second	O
half	O
but	O
his	O
known	O
powers	O
of	O
concentration	O
were	O
required	O
when	O
Jack	PERSON
Charlton	PERSON
gave	O
away	O
a	O
dubious	O
free	O
kick	O
30	O
yards	O
from	O
goal	O
.	O
Banks	O
duly	O
organised	O
a	O
defensive	O
wall	O
and	O
got	O
into	O
position	O
.	O
Lothar	PERSON
Emmerich	PERSON
slammed	O
the	O
ball	O
into	O
the	O
wall	O
,	O
the	O
ball	O
ricocheted	O
across	O
goal	O
and	O
Banks	PERSON
struggled	O
to	O
follow	O
it	O
across	O
his	O
six	O
yard	O
box	O
,	O
such	O
was	O
the	O
speed	O
and	O
unpredictability	O
of	O
its	O
movement	O
as	O
it	O
took	O
deflections	O
and	O
swipes	O
.	O
England	LOCATION
took	O
the	O
lead	O
in	O
extra	O
time	O
with	O
that	O
hotly	O
debated	O
second	O
goal	O
from	O
Hurst	O
.	O
Hurst	O
then	O
scored	O
his	O
hat-trick	O
goal	O
and	O
the	O
game	O
was	O
over	O
.	O
Gordon	PERSON
Banks	PERSON
had	O
33	O
England	LOCATION
caps	O
and	O
was	O
a	O
world	O
champion	O
.	O
Coming	O
through	O
the	O
ranks	O
at	O
Leicester	LOCATION
City	LOCATION
was	O
a	O
young	O
local	O
goalkeeper	O
called	O
Peter	PERSON
Shilton	PERSON
,	O
who	O
was	O
given	O
his	O
debut	O
as	O
a	O
17-year-old	O
in	O
1966	O
.	O
It	O
was	O
clear	O
that	O
Shilton	O
was	O
something	O
special	O
,	O
yet	O
the	O
man	O
he	O
had	O
to	O
displace	O
was	O
now	O
regarded	O
as	O
the	O
world	O
's	O
number	O
one	O
goalkeeper	O
.	O
Banks	O
joined	O
Stoke	LOCATION
City	LOCATION
and	O
maintained	O
his	O
England	LOCATION
place	O
,	O
while	O
Shilton	O
lost	O
in	O
Leicester	LOCATION
's	O
third	O
FA	O
Cup	O
final	O
of	O
the	O
1960s	O
(	O
the	O
1969	O
game	O
against	O
Manchester	LOCATION
City	LOCATION
)	O
and	O
began	O
to	O
make	O
his	O
name	O
.	O
Ramsey	PERSON
gave	O
the	O
odd	O
chance	O
to	O
Chelsea	O
keeper	O
Peter	PERSON
Bonetti	PERSON
,	O
Everton	O
's	O
Gordon	PERSON
West	PERSON
and	O
Manchester	LOCATION
United	LOCATION
's	LOCATION
Alex	PERSON
Stepney	PERSON
,	O
but	O
when	O
the	O
big	O
games	O
came	O
along	O
,	O
it	O
was	O
only	O
Banks	O
.	O
England	LOCATION
reached	O
the	O
last	O
four	O
of	O
the	O
1968	O
European	O
Championships	O
where	O
they	O
lost	O
to	O
Yugoslavia	LOCATION
in	O
Florence	LOCATION
.	O
After	O
just	O
ten	O
minutes	O
,	O
Banks	PERSON
wrote	O
himself	O
into	O
football	O
folklore	O
.	O
Banks	O
,	O
like	O
all	O
goalkeepers	O
reliant	O
on	O
positional	O
sensibility	O
,	O
had	O
been	O
at	O
the	O
near	O
post	O
and	O
suddenly	O
had	O
to	O
turn	O
on	O
his	O
heels	O
and	O
follow	O
the	O
ball	O
to	O
its	O
back	O
post	O
destination	O
.	O
He	O
leapt	O
hard	O
at	O
the	O
ball	O
above	O
England	LOCATION
right	O
back	O
Tommy	PERSON
Wright	PERSON
and	O
thundered	O
a	O
harsh	O
,	O
pacy	O
downward	O
header	O
towards	O
Banks	O
'	O
near	O
post	O
corner	O
.	O
Banks	O
was	O
still	O
making	O
his	O
way	O
across	O
the	O
line	O
from	O
Jairzinho	O
's	O
cross	O
and	O
in	O
the	O
split-second	O
of	O
assessment	O
the	O
incident	O
allowed	O
,	O
it	O
seemed	O
impossible	O
for	O
him	O
to	O
get	O
to	O
the	O
ball	O
.	O
It	O
was	O
only	O
when	O
he	O
heard	O
the	O
applause	O
and	O
praise	O
of	O
captain	O
Bobby	PERSON
Moore	PERSON
and	O
then	O
looked	O
up	O
and	O
saw	O
the	O
ball	O
trundling	O
towards	O
the	O
advertising	O
hoardings	O
at	O
the	O
far	O
corner	O
,	O
that	O
he	O
realised	O
he	O
'd	O
managed	O
to	O
divert	O
the	O
ball	O
over	O
the	O
bar	O
--	O
he	O
'd	O
known	O
he	O
got	O
a	O
touch	O
but	O
still	O
assumed	O
the	O
ball	O
had	O
gone	O
in	O
.	O
Pelé	PERSON
,	O
who	O
'd	O
begun	O
to	O
celebrate	O
a	O
goal	O
when	O
he	O
headed	O
the	O
ball	O
,	O
would	O
later	O
describe	O
the	O
save	O
as	O
the	O
greatest	O
he	O
'd	O
ever	O
seen	O
.	O
Gordon	PERSON
Banks	PERSON
would	O
later	O
describe	O
the	O
save	O
as	O
the	O
thing	O
he	O
would	O
remain	O
best	O
known	O
for	O
.	O
Brazil	LOCATION
still	O
won	O
the	O
game	O
1	O
--	O
0	O
--	O
Jairzinho	PERSON
guided	O
a	O
shot	O
past	O
Banks	PERSON
in	O
the	O
second	O
half	O
--	O
but	O
England	LOCATION
missed	O
chances	O
to	O
get	O
something	O
,	O
with	O
Jeff	PERSON
Astle	PERSON
infamously	O
putting	O
the	O
ball	O
wide	O
of	O
an	O
open	O
goal	O
and	O
Alan	PERSON
Ball	PERSON
striking	O
the	O
crossbar	O
.	O
England	LOCATION
ultimately	O
joined	O
Brazil	LOCATION
in	O
the	O
last	O
eight	O
after	O
a	O
win	O
in	O
the	O
final	O
group	O
game	O
against	O
Czechoslovakia	LOCATION
.	O
But	O
on	O
the	O
day	O
of	O
the	O
game	O
,	O
he	O
offered	O
a	O
glimmer	O
of	O
hope	O
to	O
Ramsey	PERSON
when	O
he	O
said	O
he	O
felt	O
better	O
and	O
asked	O
for	O
a	O
fitness	O
test	O
.	O
He	O
caught	O
a	O
few	O
balls	O
and	O
did	O
some	O
short	O
sprints	O
but	O
something	O
was	O
not	O
right	O
and	O
Ramsey	PERSON
decided	O
he	O
could	O
n't	O
risk	O
him	O
.	O
Peter	PERSON
Bonetti	PERSON
was	O
summoned	O
to	O
take	O
his	O
place	O
.	O
Franz	PERSON
Beckenbauer	PERSON
then	O
hit	O
a	O
low	O
shot	O
under	O
the	O
body	O
of	O
Bonetti	PERSON
,	O
who	O
had	O
been	O
slow	O
to	O
react	O
.	O
Beckenbauer	PERSON
would	O
later	O
claim	O
that	O
he	O
would	O
not	O
have	O
scored	O
had	O
Banks	PERSON
been	O
the	O
goalkeeper	O
.	O
In	O
the	O
last	O
ten	O
minutes	O
,	O
veteran	O
striker	O
Uwe	PERSON
Seeler	PERSON
looped	O
a	O
back	O
header	O
over	O
Bonetti	PERSON
to	O
take	O
the	O
game	O
into	O
extra	O
time	O
;	O
then	O
Gerd	PERSON
Müller	PERSON
smashed	O
home	O
the	O
winner	O
in	O
the	O
added	O
period	O
.	O
Conspiracies	O
began	O
to	O
surface	O
that	O
Banks	PERSON
had	O
been	O
"	O
nobbled	O
"	O
by	O
someone	O
in	O
England	LOCATION
's	O
hotel	O
and	O
that	O
his	O
food	O
had	O
been	O
somehow	O
spiked	O
.	O
During	O
this	O
period	O
,	O
Banks	PERSON
was	O
also	O
involved	O
in	O
a	O
notorious	O
incident	O
with	O
Manchester	PERSON
United	PERSON
's	PERSON
George	PERSON
Best	PERSON
who	O
,	O
while	O
playing	O
against	O
England	LOCATION
for	O
Northern	LOCATION
Ireland	LOCATION
,	O
flicked	O
the	O
ball	O
out	O
of	O
Banks	O
'	O
hands	O
and	O
headed	O
it	O
into	O
the	O
net	O
as	O
the	O
protesting	O
goalkeeper	O
chased	O
him	O
.	O
The	O
goal	O
was	O
disallowed	O
for	O
ungentlemanly	O
conduct	O
and	O
England	LOCATION
won	O
1	O
--	O
0	O
,	O
but	O
Banks	PERSON
was	O
left	O
feeling	O
rather	O
embarrassed	O
.	O
Having	O
lost	O
two	O
FA	O
Cup	O
finals	O
,	O
Banks	O
'	O
attempts	O
to	O
be	O
luckier	O
with	O
Stoke	O
in	O
the	O
competition	O
fell	O
agonisingly	O
short	O
as	O
Arsenal	O
beat	O
them	O
in	O
the	O
semi	O
finals	O
of	O
both	O
the	O
1971	O
and	O
1972	O
competitions	O
.	O
He	O
began	O
the	O
next	O
season	O
with	O
Stoke	O
in	O
his	O
usual	O
unflappable	O
manner	O
,	O
but	O
then	O
his	O
top-flight	O
career	O
would	O
be	O
suddenly	O
and	O
violently	O
brought	O
to	O
an	O
end	O
.	O
On	O
22	O
October	O
1972	O
,	O
while	O
driving	O
home	O
from	O
a	O
session	O
with	O
the	O
Stoke	O
physiotherapist	O
,	O
Banks	PERSON
lost	O
control	O
of	O
his	O
car	O
which	O
ended	O
up	O
in	O
a	O
ditch	O
.	O
Shilton	O
became	O
England	LOCATION
's	O
number	O
one	O
and	O
it	O
was	O
he	O
whom	O
Stoke	O
bought	O
in	O
1974	O
as	O
Banks	O
'	O
long-term	O
replacement	O
.	O
His	O
final	O
appearance	O
was	O
a	O
2	O
--	O
3	O
defeat	O
away	O
to	O
local	O
rivals	O
Tampa	O
Bay	O
Rowdies	O
on	O
17	O
June	O
1978	O
,	O
in	O
which	O
Rodney	PERSON
Marsh	PERSON
scored	O
all	O
three	O
for	O
the	O
home	O
team.	O
Banks	O
later	O
began	O
a	O
business	O
which	O
distributed	O
tickets	O
for	O
big	O
events	O
to	O
corporate	O
clients	O
,	O
but	O
this	O
fell	O
into	O
a	O
mini-scandal	O
when	O
he	O
received	O
a	O
restricted	O
ban	O
on	O
getting	O
tickets	O
for	O
the	O
FA	O
Cup	O
final	O
after	O
some	O
attributed	O
to	O
his	O
company	O
fell	O
into	O
the	O
wrong	O
hands	O
.	O
In	O
December	O
1978	O
he	O
was	O
appointed	O
as	O
a	O
coach	O
at	O
Port	LOCATION
Vale	LOCATION
,	O
being	O
demoted	O
to	O
reserve	O
coach	O
in	O
October	O
1978	O
as	O
the	O
team	O
struggled	O
,	O
before	O
being	O
dismissed	O
in	O
1979	O
.	O
He	O
was	O
awarded	O
an	O
honorary	O
doctorate	O
from	O
Keele	O
University	O
in	O
February	O
2006	O
.	O
But	O
the	O
former	O
Leicester	LOCATION
and	O
Stoke	LOCATION
City	LOCATION
keeper	O
wanted	O
to	O
save	O
his	O
children	O
the	O
burden	O
of	O
deciding	O
what	O
to	O
do	O
with	O
the	O
medal	O
after	O
his	O
death	O
.	O
Banks	O
's	O
international	O
cap	O
from	O
the	O
same	O
match	O
was	O
also	O
sold	O
in	O
the	O
same	O
aution	O
.	O
Banks	O
made	O
a	O
speech	O
to	O
an	O
attendant	O
crowd	O
as	O
to	O
how	O
thrilled	O
he	O
was	O
to	O
be	O
given	O
this	O
honour	O
.	O
Originally	O
the	O
second	O
phase	O
was	O
to	O
feature	O
two	O
diving	O
figures	O
of	O
Gordon	PERSON
Banks	PERSON
.	O
