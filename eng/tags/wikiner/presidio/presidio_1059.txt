They	O
were	O
active	O
in	O
the	O
fort	O
of	O
Alamut	LOCATION
in	O
Iran	LOCATION
from	O
the	O
eighth	O
to	O
the	O
fourteenth	O
centuries	O
.	O
The	O
earliest	O
known	O
literary	O
use	O
of	O
the	O
word	O
assassination	O
is	O
in	O
Macbeth	PERSON
by	O
William	PERSON
Shakespeare	PERSON
(	O
1605	O
)	O
.	O
The	O
practice	O
was	O
also	O
well	O
known	O
in	O
ancient	O
China	LOCATION
.	O
An	O
example	O
of	O
this	O
is	O
Jing	PERSON
Ke	PERSON
's	PERSON
failed	O
assassination	O
of	O
Qin	PERSON
Shi	PERSON
Huang	PERSON
.	O
The	O
ancient	O
Indian	O
military	O
adviser	O
Chanakya	PERSON
wrote	O
about	O
assassinations	O
in	O
detail	O
in	O
his	O
political	O
treatise	O
Arthashastra	O
.	O
In	O
Russia	LOCATION
alone	O
,	O
four	O
emperors	O
were	O
assassinated	O
within	O
less	O
than	O
two	O
hundred	O
years	O
:	O
Ivan	PERSON
VI	PERSON
,	O
Peter	PERSON
III	PERSON
,	O
Paul	PERSON
I	O
,	O
and	O
Alexander	PERSON
II	PERSON
.	O
In	O
the	O
Philippines	LOCATION
,	O
the	O
assassination	O
of	O
Benigno	PERSON
Aquino	PERSON
,	O
Jr.	PERSON
triggered	O
the	O
eventual	O
downfall	O
of	O
the	O
20-year	O
autocratic	O
rule	O
of	O
President	O
Ferdinand	PERSON
Marcos	PERSON
.	O
His	O
death	O
thrust	O
his	O
widow	O
,	O
Corazon	PERSON
Aquino	PERSON
,	O
into	O
the	O
limelight	O
and	O
,	O
ultimately	O
,	O
the	O
presidency	O
following	O
the	O
peaceful	O
1986	O
EDSA	O
Revolution	O
.	O
At	O
least	O
162	O
killings	O
in	O
19	O
different	O
countries	O
have	O
been	O
linked	O
to	O
the	O
senior	O
leadership	O
of	O
the	LOCATION
Islamic	LOCATION
Republic	LOCATION
of	LOCATION
Iran	LOCATION
.	O
Evidence	O
indicates	O
that	O
Fallahian	O
's	O
personal	O
involvement	O
and	O
individual	O
responsibility	O
for	O
the	O
murders	O
were	O
far	O
more	O
pervasive	O
than	O
his	O
current	O
indictment	O
record	O
represents	O
.	O
Various	O
governments	O
around	O
the	O
world	O
,	O
such	O
as	O
Saddam	PERSON
Hussein	PERSON
,	O
have	O
also	O
used	O
assassination	O
to	O
remove	O
individual	O
opponents	O
,	O
or	O
to	O
terrorize	O
troublesome	O
population	O
groups	O
.	O
The	O
assassinations	O
were	O
linked	O
to	O
separatist	O
movements	O
in	O
Punjab	LOCATION
and	O
northern	O
Sri	LOCATION
Lanka	LOCATION
,	O
respectively	O
.	O
In	O
Israel	LOCATION
,	O
Prime	O
Minister	O
Yitzhak	PERSON
Rabin	PERSON
was	O
assassinated	O
on	O
November	O
4	O
,	O
1995	O
.	O
Yigal	PERSON
Amir	PERSON
confessed	O
and	O
was	O
convicted	O
of	O
the	O
crime	O
.	O
Israeli	O
tourists	O
minister	O
Rehavam	PERSON
Ze'evi	PERSON
was	O
assassinated	O
on	O
October	O
17	O
,	O
2001	O
by	O
Hamdi	PERSON
Quran	PERSON
and	O
three	O
other	O
members	O
of	O
the	O
Popular	O
Front	O
for	O
the	O
Liberation	O
of	O
Palestine	O
(	O
PFLP	O
)	O
.	O
The	O
PFLP	O
stated	O
that	O
the	O
assassination	O
was	O
in	O
retaliation	O
for	O
the	O
August	O
27	O
,	O
2001	O
assassination	O
of	O
Abu	PERSON
Ali	PERSON
Mustafa	PERSON
,	O
the	O
Secretary	O
General	O
of	O
the	O
PFLP	O
,	O
by	O
the	O
Israeli	O
Air	O
Force	O
under	O
its	O
policy	O
of	O
targeted	O
killings	O
.	O
In	O
Lebanon	LOCATION
,	O
the	O
assassination	O
of	O
former	O
Prime	O
Minister	O
Rafik	PERSON
Hariri	PERSON
on	O
February	O
14	O
,	O
2005	O
,	O
prompted	O
an	O
investigation	O
by	O
the	O
United	O
Nations	O
.	O
The	O
suggestions	O
in	O
the	O
resulting	O
Mehlis	O
report	O
,	O
that	O
there	O
was	O
Syrian	O
involvement	O
,	O
prompted	O
the	O
Cedar	O
Revolution	O
,	O
which	O
drove	O
Syrian	O
troops	O
out	O
of	O
Lebanon	LOCATION
.	O
In	O
Guinea	LOCATION
Bissau	O
,	O
President	O
João	PERSON
Bernardo	PERSON
Vieira	PERSON
was	O
assassinated	O
in	O
the	O
early	O
hours	O
of	O
Monday	O
March	O
2	O
,	O
2009	O
in	O
the	O
capital	O
,	O
Bissau	O
.	O
In	O
2002	O
,	O
the	O
George	O
W.	O
Bush	O
Administration	O
prepared	O
a	O
list	O
of	O
"	O
terrorist	O
leaders	O
"	O
the	O
CIA	O
is	O
authorized	O
to	O
assassinate	O
,	O
if	O
capture	O
is	O
impractical	O
and	O
civilian	O
casualties	O
can	O
be	O
kept	O
to	O
an	O
acceptable	O
number	O
.	O
The	O
list	O
includes	O
key	O
al-Qa'ida	O
leaders	O
like	O
Osama	PERSON
bin	PERSON
Laden	PERSON
and	O
his	O
chief	O
deputy	O
,	O
Ayman	PERSON
al-Zawahiri	PERSON
,	O
as	O
well	O
as	O
other	O
principal	O
figures	O
from	O
al-Qa'ida	O
and	O
affiliated	O
groups	O
.	O
The	O
program	O
consisted	O
of	O
teams	O
of	O
Special	O
Activities	O
Division	O
paramilitary	O
officers	O
organized	O
to	O
execute	O
targeted	O
assassination	O
operations	O
against	O
al-Qa'ida	O
operatives	O
around	O
the	O
world	O
in	O
any	O
country	O
.	O
Per	O
senior	O
intelligence	O
officers	O
,	O
this	O
program	O
was	O
an	O
attempt	O
to	O
avoid	O
the	O
civilian	O
casualties	O
that	O
can	O
occur	O
during	O
Predator	O
drone	O
strikes	O
using	O
Hellfire	O
missiles	O
.	O
Assassination	O
for	O
military	O
purposes	O
has	O
long	O
been	O
espoused	O
--	O
Sun	PERSON
Tzu	PERSON
,	O
writing	O
around	O
500	O
BC	LOCATION
,	O
argued	O
in	O
favor	O
of	O
using	O
assassination	O
in	O
his	O
book	O
The	O
Art	O
of	O
War	O
.	O
A	O
number	O
of	O
additional	O
examples	O
from	O
World	O
War	O
II	O
show	O
how	O
assassination	O
was	O
used	O
as	O
a	O
military	O
tool	O
at	O
both	O
tactical	O
and	O
strategic	O
levels	O
:	O
Michael	PERSON
Collins	PERSON
set	O
up	O
a	O
special	O
unit	O
--	O
the	O
Squad	O
--	O
for	O
this	O
purpose	O
,	O
which	O
had	O
the	O
effect	O
of	O
intimidating	O
many	O
policemen	O
into	O
resigning	O
from	O
the	O
force	O
.	O
This	O
tactic	O
was	O
used	O
again	O
by	O
the	O
Provisional	O
IRA	O
during	O
the	O
Troubles	O
in	O
Northern	LOCATION
Ireland	LOCATION
(	O
1969	O
--	O
present	O
)	O
.	O
In	O
the	O
Vietnam	O
War	O
,	O
assassinations	O
were	O
routinely	O
carried	O
out	O
by	O
communist	O
insurgents	O
against	O
government	O
officials	O
and	O
individual	O
civilians	O
deemed	O
to	O
offend	O
or	O
rival	O
the	O
revolutionary	O
movement	O
.	O
A	O
professional	O
hitman	O
is	O
called	O
"	O
cleaner	O
"	O
in	O
Russia	LOCATION
;	O
he	O
is	O
used	O
to	O
clean	O
away	O
the	O
target	O
.	O
Gunpowder	O
and	O
other	O
explosives	O
also	O
allowed	O
the	O
use	O
of	O
bombs	O
or	O
even	O
greater	O
concentrations	O
of	O
explosives	O
for	O
deeds	O
requiring	O
a	O
larger	O
touch	O
;	O
for	O
an	O
example	O
,	O
the	O
Gunpowder	O
Plot	O
could	O
have	O
'	O
assassinated	O
'	O
almost	O
a	O
thousand	O
people	O
had	O
it	O
not	O
been	O
foiled	O
.	O
With	O
heavy	O
weapons	O
,	O
the	O
rocket-propelled	O
grenade	O
(	O
RPG	O
)	O
has	O
become	O
a	O
useful	O
tool	O
given	O
the	O
popularity	O
of	O
armored	O
cars	O
(	O
discussed	O
below	O
)	O
,	O
while	O
Israeli	O
forces	O
have	O
pioneered	O
the	O
use	O
of	O
aircraft-mounted	O
missiles	O
for	O
assassination	O
,	O
as	O
well	O
as	O
the	O
innovative	O
use	O
of	O
explosive	O
devices	O
.	O
Georgi	PERSON
Markov	PERSON
,	O
a	O
Bulgarian	O
dissident	O
was	O
assassinated	O
by	O
ricin	O
poisoning	O
.	O
However	O
,	O
it	O
was	O
learned	O
that	O
after	O
fall	O
of	O
the	O
USSR	LOCATION
,	O
the	O
KGB	O
had	O
developed	O
an	O
umbrella	O
that	O
could	O
inject	O
ricin	O
pellets	O
into	O
a	O
victim	O
,	O
and	O
two	O
former	O
KGB	O
agents	O
who	O
defected	O
said	O
the	O
agency	O
assisted	O
in	O
the	O
murder	O
.	O
The	O
CIA	O
has	O
allegedly	O
made	O
several	O
attempts	O
to	O
assassinate	O
Fidel	PERSON
Castro	PERSON
,	O
many	O
of	O
the	O
schemes	O
involving	O
poisoning	O
his	O
milkshakes	O
.	O
Shortly	O
before	O
his	O
death	O
he	O
issued	O
a	O
statement	O
accusing	O
then-President	O
of	O
Russia	LOCATION
Vladimir	PERSON
Putin	PERSON
of	O
involvement	O
in	O
his	O
assassination	O
.	O
Other	O
potential	O
targets	O
go	O
into	O
seclusion	O
,	O
and	O
are	O
rarely	O
heard	O
from	O
or	O
seen	O
in	O
public	O
,	O
such	O
as	O
writer	O
Salman	PERSON
Rushdie	PERSON
.	O
United	O
States	O
Secret	O
Service	O
protective	O
agents	O
receive	O
training	O
in	O
the	O
psychology	O
of	O
assassins	O
.	O
