Originally	O
larger	O
countries	O
,	O
such	O
as	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
or	O
France	LOCATION
,	O
were	O
assigned	O
two-digit	O
codes	O
(	O
to	O
compensate	O
for	O
their	O
usually	O
longer	O
domestic	O
numbers	O
)	O
and	O
small	O
countries	O
,	O
such	O
as	O
Iceland	LOCATION
,	O
were	O
assigned	O
three-digit	O
codes	O
;	O
however	O
,	O
since	O
the	O
1980s	O
,	O
all	O
new	O
assignments	O
have	O
been	O
three-digit	O
regardless	O
of	O
countries	O
'	O
populations	O
.	O
Some	O
of	O
these	O
country	O
codes	O
were	O
retained	O
in	O
the	O
CCITT	O
country	O
code	O
assignments	O
and	O
remain	O
in	O
effect	O
(	O
e.g.	O
France	LOCATION
+33	O
,	O
U.K.	LOCATION
+44	O
)	O
.	O
