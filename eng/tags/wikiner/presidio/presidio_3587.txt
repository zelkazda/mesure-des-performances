Runyon	O
wrote	O
these	O
stories	O
in	O
a	O
distinctive	O
vernacular	O
style	O
:	O
a	O
mixture	O
of	O
formal	O
speech	O
and	O
colorful	O
slang	O
,	O
almost	O
always	O
in	O
present	O
tense	O
,	O
and	O
always	O
devoid	O
of	O
contractions	O
.	O
The	O
film	O
Little	O
Miss	O
Marker	PERSON
(	O
and	O
its	O
remake	O
,	O
Sorrowful	PERSON
Jones	PERSON
)	O
grew	O
from	O
his	O
short	O
story	O
of	O
the	O
same	O
name	O
.	O
Runyon	O
was	O
also	O
a	O
newspaperman	O
.	O
In	O
1882	O
Runyon	O
's	O
father	O
was	O
forced	O
to	O
sell	O
his	O
newspaper	O
,	O
and	O
the	O
family	O
moved	O
westward	O
.	O
The	O
family	O
eventually	O
settled	O
in	O
Pueblo	LOCATION
,	O
Colorado	LOCATION
,	O
in	O
1887	O
,	O
where	O
Runyon	PERSON
spent	O
the	O
rest	O
of	O
his	O
youth	O
.	O
He	O
began	O
to	O
work	O
in	O
the	O
newspaper	O
trade	O
under	O
his	O
father	O
in	O
Pueblo	LOCATION
.	O
For	O
the	O
next	O
ten	O
years	O
he	O
covered	O
the	O
New	O
York	O
Giants	O
and	O
professional	O
boxing	O
for	O
the	O
New	LOCATION
York	LOCATION
American	O
.	O
When	O
Berman	PERSON
was	O
killed	O
in	O
a	O
hit	O
on	O
Berman	PERSON
's	O
boss	O
,	O
Dutch	PERSON
Schultz	PERSON
,	O
Runyon	PERSON
quickly	O
assumed	O
the	O
role	O
of	O
damage	O
control	O
for	O
his	O
deceased	O
friend	O
,	O
correcting	O
erroneous	O
press	O
releases	O
He	O
was	O
the	O
Hearst	O
newspapers	O
'	O
baseball	O
columnist	O
for	O
many	O
years	O
,	O
beginning	O
in	O
1911	O
,	O
and	O
his	O
knack	O
for	O
spotting	O
the	O
eccentric	O
and	O
the	O
unusual	O
,	O
on	O
the	O
field	O
or	O
in	O
the	O
stands	O
,	O
is	O
credited	O
with	O
revolutionizing	O
the	O
way	O
baseball	O
was	O
covered	O
.	O
Perhaps	O
as	O
confirmation	O
,	O
Runyon	PERSON
was	O
inducted	O
into	O
the	O
writers	O
'	O
wing	O
of	O
the	O
Baseball	O
Hall	O
of	O
Fame	O
in	O
1967	O
.	O
He	O
is	O
also	O
a	O
member	O
of	O
the	O
International	O
Boxing	O
Hall	O
Of	O
Fame	O
and	O
is	O
known	O
for	O
dubbing	O
heavyweight	O
champion	O
James	PERSON
J.	PERSON
Braddock	PERSON
,	O
the	O
"	O
Cinderella	O
Man	O
"	O
.	O
Gambling	O
was	O
a	O
common	O
theme	O
of	O
Runyon	O
's	O
works	O
,	O
and	O
he	O
was	O
a	O
notorious	O
gambler	O
himself	O
.	O
He	O
died	O
in	O
New	LOCATION
York	LOCATION
City	LOCATION
from	O
throat	O
cancer	O
in	O
late	O
1946	O
,	O
at	O
age	O
66	O
.	O
The	O
family	O
plot	O
of	O
Damon	PERSON
Runyon	PERSON
is	O
located	O
at	O
Woodlawn	O
Cemetery	O
in	O
Bronx	LOCATION
,	O
NY	LOCATION
.	O
Numerous	O
Damon	PERSON
Runyon	PERSON
stories	O
were	O
adapted	O
for	O
the	O
stage	O
and	O
the	O
screen	O
.	O
Runyon	O
almost	O
totally	O
avoids	O
the	O
past	O
tense	O
,	O
and	O
makes	O
little	O
use	O
of	O
the	O
future	O
tense	O
,	O
using	O
the	O
present	O
for	O
both	O
.	O
There	O
is	O
an	O
homage	O
to	O
Runyon	O
that	O
makes	O
use	O
of	O
this	O
peculiarity	O
which	O
involves	O
a	O
time	O
machine	O
.	O
Runyon	O
's	O
short	O
stories	O
are	O
told	O
in	O
the	O
first	O
person	O
by	O
a	O
protagonist	O
who	O
is	O
never	O
named	O
,	O
and	O
whose	O
role	O
is	O
unclear	O
;	O
he	O
knows	O
many	O
gangsters	O
and	O
does	O
not	O
appear	O
to	O
have	O
a	O
job	O
,	O
but	O
he	O
does	O
not	O
admit	O
to	O
any	O
criminal	O
involvement	O
,	O
and	O
seems	O
to	O
be	O
largely	O
a	O
bystander	O
.	O
