The	O
Colorado	O
Front	O
Range	O
is	O
a	O
colloquial	O
geographic	O
term	O
for	O
the	O
most	O
populous	O
region	O
of	O
the	O
state	O
of	O
Colorado	LOCATION
in	O
the	LOCATION
United	LOCATION
States	LOCATION
.	O
The	O
region	O
contains	O
the	O
largest	O
cities	O
and	O
the	O
majority	O
of	O
the	O
population	O
of	O
Colo.	LOCATION
.	O
The	O
Colorado	O
Front	O
Range	O
communities	O
include	O
(	O
in	O
a	O
roughly	O
north-to-south	O
order	O
)	O
:	O
The	O
addition	O
of	O
the	O
adjacent	O
Wyoming	LOCATION
community	O
of	O
Cheyenne	LOCATION
to	O
the	O
Colorado	O
Front	O
Range	O
forms	O
the	O
full	O
Front	O
Range	O
Urban	O
Corridor	O
.	O
