That	O
approximates	O
the	O
modern	O
definition	O
of	O
Johannes	PERSON
Nicolaus	PERSON
Brønsted	PERSON
and	O
Martin	PERSON
Lowry	PERSON
,	O
who	O
independently	O
defined	O
an	O
acid	O
as	O
a	O
compound	O
which	O
donates	O
a	O
hydrogen	O
ion	O
to	O
another	O
compound	O
(	O
called	O
a	O
base	O
)	O
.	O
An	O
Arrhenius	O
base	O
is	O
a	O
molecule	O
which	O
increases	O
the	O
concentration	O
of	O
the	O
hydroxide	O
ion	O
when	O
dissolved	O
in	O
water	O
.	O
While	O
the	O
Arrhenius	O
concept	O
is	O
useful	O
for	O
describing	O
many	O
reactions	O
,	O
it	O
is	O
also	O
quite	O
limited	O
in	O
its	O
scope	O
.	O
In	O
1923	O
chemists	O
Johannes	PERSON
Nicolaus	PERSON
Brønsted	PERSON
and	O
Thomas	PERSON
Martin	PERSON
Lowry	PERSON
independently	O
recognized	O
that	O
acid-base	O
reactions	O
involve	O
the	O
transfer	O
of	O
a	O
proton	O
.	O
The	O
following	O
reactions	O
illustrate	O
the	O
limitations	O
of	O
Arrhenius	O
'	O
definition	O
:	O
As	O
with	O
the	O
acetic	O
acid	O
reactions	O
,	O
both	O
definitions	O
work	O
for	O
the	O
first	O
example	O
,	O
where	O
water	O
is	O
the	O
solvent	O
and	O
hydronium	O
ion	O
is	O
formed	O
.	O
A	O
third	O
concept	O
was	O
proposed	O
by	O
Gilbert	PERSON
N.	PERSON
Lewis	PERSON
which	O
includes	O
reactions	O
with	O
acid-base	O
characteristics	O
that	O
do	O
not	O
involve	O
a	O
proton	O
transfer	O
.	O
This	O
reaction	O
can	O
not	O
be	O
described	O
in	O
terms	O
of	O
Brønsted	O
theory	O
because	O
there	O
is	O
no	O
proton	O
transfer	O
.	O
In	O
the	O
IUPAC	O
naming	O
system	O
,	O
"	O
aqueous	O
"	O
is	O
simply	O
added	O
to	O
the	O
name	O
of	O
the	O
ionic	O
compound	O
.	O
Thus	O
,	O
for	O
hydrogen	O
chloride	O
,	O
the	O
IUPAC	O
name	O
would	O
be	O
aqueous	O
hydrogen	O
chloride	O
.	O
