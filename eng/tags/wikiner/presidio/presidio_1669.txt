Brian	PERSON
Wilson	PERSON
's	PERSON
growing	O
creative	O
ambitions	O
later	O
transformed	O
them	O
into	O
a	O
more	O
artistically	O
innovative	O
group	O
that	O
earned	O
critical	O
praise	O
and	O
influenced	O
many	O
later	O
musicians	O
.	O
The	O
group	O
was	O
initially	O
composed	O
of	O
singer-musician-composer	O
Brian	PERSON
Wilson	PERSON
,	O
his	O
brothers	O
,	O
Carl	PERSON
and	O
Dennis	PERSON
,	O
their	O
cousin	O
Mike	PERSON
Love	PERSON
,	O
and	O
friend	O
Al	PERSON
Jardine	PERSON
.	O
Many	O
changes	O
in	O
both	O
musical	O
styles	O
and	O
personnel	O
have	O
occurred	O
during	O
their	O
career	O
,	O
notably	O
because	O
of	O
Brian	PERSON
Wilson	PERSON
's	PERSON
mental	O
illness	O
and	O
drug	O
use	O
(	O
leading	O
to	O
his	O
eventual	O
withdrawal	O
from	O
the	O
group	O
)	O
and	O
the	O
deaths	O
of	O
Dennis	PERSON
and	O
Carl	PERSON
Wilson	PERSON
in	O
1983	O
and	O
1998	O
,	O
respectively	O
.	O
After	O
the	O
death	O
of	O
Carl	PERSON
Wilson	PERSON
,	O
founding	O
member	O
Al	PERSON
Jardine	PERSON
left	O
to	O
pursue	O
a	O
solo	O
career	O
.	O
Brian	PERSON
Wilson	PERSON
was	O
born	O
in	O
Inglewood	LOCATION
,	O
Calif.	LOCATION
,	O
in	O
1942	O
,	O
and	O
his	O
family	O
moved	O
to	O
nearby	O
Hawthorne	LOCATION
when	O
Brian	PERSON
was	O
two	O
years	O
old	O
.	O
At	O
the	O
age	O
of	O
16	O
,	O
Brian	PERSON
shared	O
a	O
bedroom	O
with	O
his	O
two	O
brothers	O
,	O
Dennis	PERSON
and	O
Carl	PERSON
.	O
He	O
watched	O
his	O
father	O
,	O
Murry	PERSON
Wilson	PERSON
,	O
play	O
piano	O
and	O
listened	O
intently	O
to	O
the	O
harmonies	O
of	O
vocal	O
groups	O
like	O
The	O
Four	O
Freshmen	O
.	O
For	O
his	O
16th	O
birthday	O
,	O
Brian	PERSON
had	O
received	O
a	O
reel-to-reel	O
tape	O
recorder	O
.	O
He	O
learned	O
how	O
to	O
overdub	O
,	O
using	O
his	O
vocals	O
and	O
those	O
of	O
Carl	PERSON
and	O
his	O
mother	O
.	O
Brian	PERSON
suggested	O
to	O
Jardine	PERSON
that	O
they	O
team	O
up	O
with	O
his	O
cousin	O
and	O
brother	O
Carl	PERSON
.	O
It	O
was	O
at	O
these	O
sessions	O
,	O
held	O
in	O
Brian	PERSON
's	O
bedroom	O
,	O
that	O
"	O
the	O
Beach	O
Boys	O
sound	O
"	O
began	O
to	O
form	O
.	O
Brian	PERSON
says	O
:	O
"	O
Everyone	O
contributed	O
something	O
.	O
It	O
was	O
Love	O
who	O
encouraged	O
Brian	PERSON
to	O
write	O
songs	O
and	O
he	O
also	O
gave	O
the	O
fledgling	O
band	O
its	O
first	O
name	O
:	O
The	O
Pendletones	O
.	O
The	O
Pendletones	O
name	O
was	O
derived	O
from	O
the	O
Pendleton	PERSON
woolen	O
shirts	O
popular	O
at	O
that	O
time	O
.	O
In	O
1962	O
,	O
the	O
Beach	O
Boys	O
began	O
wearing	O
blue/gray-striped	O
button-down	O
shirts	O
tucked	O
into	O
white	O
pants	O
as	O
their	O
touring	O
"	O
uniforms	O
.	O
"	O
Although	O
surfing	O
motifs	O
were	O
very	O
prominent	O
in	O
their	O
early	O
songs	O
,	O
Dennis	PERSON
was	O
the	O
only	O
member	O
of	O
the	O
group	O
who	O
surfed	O
.	O
He	O
suggested	O
that	O
his	O
brothers	O
compose	O
some	O
songs	O
celebrating	O
his	O
hobby	O
and	O
the	O
lifestyle	O
which	O
had	O
developed	O
around	O
it	O
in	O
Southern	LOCATION
California	LOCATION
.	O
When	O
the	O
boys	O
eagerly	O
unpacked	O
the	O
first	O
box	O
of	O
singles	O
,	O
on	O
the	O
Candix	O
Records	O
label	O
,	O
they	O
were	O
surprised	O
and	O
angered	O
to	O
see	O
their	O
band	O
name	O
had	O
been	O
changed	O
to	O
"	O
Beach	O
Boys	O
"	O
.	O
As	O
an	O
eight-year-old	O
,	O
Brian	PERSON
Wilson	PERSON
says	O
his	O
"	O
young	O
life	O
was	O
already	O
being	O
shaped	O
and	O
influenced	O
by	O
music	O
...	O
Carl	PERSON
found	O
comfort	O
in	O
food	O
and	O
Dennis	PERSON
rebelled	O
against	O
the	O
world	O
to	O
express	O
his	O
anger	O
.	O
Brian	PERSON
would	O
immerse	O
himself	O
in	O
music	O
to	O
cope	O
,	O
but	O
though	O
he	O
longed	O
to	O
learn	O
piano	O
as	O
a	O
child	O
,	O
he	O
was	O
too	O
frightened	O
to	O
ask	O
and	O
even	O
too	O
scared	O
to	O
press	O
the	O
keys	O
when	O
his	O
father	O
was	O
at	O
work	O
.	O
Eventually	O
Brian	PERSON
surprised	O
his	O
parents	O
by	O
showing	O
he	O
had	O
learned	O
how	O
to	O
play	O
the	O
piano	O
by	O
watching	O
his	O
father	O
.	O
In	O
1964	O
,	O
Brian	PERSON
ousted	O
his	O
father	O
after	O
a	O
violent	O
confrontation	O
in	O
the	O
studio	O
.	O
Brian	PERSON
recalls	O
how	O
he	O
wondered	O
what	O
they	O
were	O
doing	O
there	O
;	O
"	O
five	O
clean-cut	O
,	O
unworldly	O
white	O
boys	O
from	O
a	O
conservative	O
white	O
suburb	O
,	O
in	O
an	O
auditorium	O
full	O
of	O
black	O
kids	O
"	O
.	O
In	O
February	O
1962	O
,	O
Al	PERSON
Jardine	PERSON
left	O
the	O
band	O
to	O
continue	O
his	O
college	O
studies	O
.	O
David	PERSON
Marks	PERSON
,	O
a	O
thirteen-year-old	O
neighbor	O
and	O
friend	O
of	O
Carl	PERSON
and	O
Dennis	PERSON
who	O
had	O
been	O
playing	O
electric	O
guitar	O
for	O
years	O
with	O
Carl	PERSON
,	O
replaced	O
him	O
in	O
the	O
band	O
.	O
(	O
Jardine	O
,	O
at	O
Brian	PERSON
's	O
request	O
,	O
rejoined	O
the	O
group	O
in	O
July	O
1963	O
)	O
.	O
The	O
session	O
ended	O
on	O
a	O
bitter	O
note	O
,	O
however	O
:	O
Murry	PERSON
Wilson	PERSON
unsuccessfully	O
suggested	O
and	O
then	O
demanded	O
that	O
the	O
Beach	O
Boys	O
record	O
some	O
of	O
his	O
own	O
songs	O
,	O
saying	O
"	O
My	O
songs	O
are	O
better	O
than	O
yours	O
.	O
"	O
On	O
July	O
16	O
,	O
on	O
the	O
strength	O
of	O
the	O
June	O
demo	O
session	O
,	O
the	O
Beach	O
Boys	O
were	O
signed	O
to	O
Capitol	O
Records	O
.	O
By	O
November	O
,	O
their	O
first	O
album	O
was	O
ready	O
--	O
"	O
Surfin	O
'	O
Safari	PERSON
"	O
.	O
Their	O
song	O
output	O
continued	O
along	O
the	O
same	O
commercial	O
line	O
,	O
focusing	O
on	O
Calif.	LOCATION
youth	O
lifestyle	O
.	O
The	O
group	O
also	O
celebrated	O
the	O
Golden	O
State	O
's	O
obsession	O
with	O
hot-rod	O
racing	O
and	O
the	O
pursuit	O
of	O
happiness	O
by	O
carefree	O
teens	O
in	O
less	O
complicated	O
times	O
.	O
Musically	O
,	O
their	O
early	O
songs	O
are	O
often	O
based	O
on	O
those	O
of	O
others	O
;	O
for	O
instance	O
,	O
"	O
Surfer	O
Girl	O
"	O
shares	O
its	O
rhythmic	O
melody	O
with	O
"	O
When	O
You	O
Wish	O
Upon	O
a	O
Star	O
"	O
,	O
while	O
"	O
Surfin	O
'	O
USA	O
"	O
is	O
a	O
slight	O
variation	O
of	O
Berry	PERSON
's	O
"	O
Sweet	O
Little	O
Sixteen	O
"	O
.	O
However	O
,	O
Brian	PERSON
Wilson	PERSON
rapidly	O
progressed	O
as	O
a	O
composer	O
,	O
arranger	O
and	O
producer	O
;	O
the	O
Pet	O
Sounds	O
album	O
in	O
particular	O
is	O
recognized	O
for	O
the	O
quality	O
and	O
originality	O
of	O
its	O
melodies	O
,	O
harmonies	O
,	O
and	O
arrangements	O
.	O
In	O
his	O
autobiography	O
,	O
Brian	PERSON
states	O
that	O
the	O
melody	O
of	O
"	O
God	O
Only	O
Knows	O
"	O
was	O
inspired	O
by	O
a	O
John	PERSON
Sebastian	PERSON
record	O
.	O
The	O
stress	O
of	O
road	O
travel	O
,	O
composing	O
,	O
producing	O
and	O
maintaining	O
a	O
high	O
level	O
of	O
creativity	O
was	O
too	O
much	O
for	O
Brian	PERSON
Wilson	PERSON
to	O
bear	O
.	O
This	O
was	O
n't	O
the	O
first	O
time	O
Brian	PERSON
had	O
stopped	O
touring	O
.	O
In	O
1963	O
,	O
when	O
Al	PERSON
Jardine	PERSON
returned	O
,	O
Brian	PERSON
left	O
the	O
road	O
;	O
but	O
when	O
David	PERSON
Marks	PERSON
quit	O
,	O
Brian	PERSON
had	O
to	O
return	O
in	O
his	O
place	O
.	O
For	O
the	O
rest	O
of	O
1964	O
and	O
into	O
1965	O
,	O
Glen	PERSON
Campbell	PERSON
served	O
as	O
Wilson	O
's	O
replacement	O
in	O
concert	O
,	O
until	O
his	O
own	O
career	O
success	O
required	O
him	O
to	O
leave	O
the	O
group	O
.	O
Jan	O
&	O
Dean	O
,	O
close	O
friends	O
with	O
the	O
band	O
and	O
opening	O
act	O
for	O
them	O
in	O
concert	O
in	O
1963	O
and	O
1964	O
,	O
encouraged	O
Brian	PERSON
to	O
use	O
session	O
musicians	O
in	O
the	O
studio	O
.	O
This	O
,	O
along	O
with	O
Brian	PERSON
's	O
withdrawal	O
from	O
touring	O
,	O
permitted	O
him	O
to	O
expand	O
his	O
role	O
as	O
a	O
producer	O
.	O
Wilson	PERSON
also	O
wrote	O
"	O
Surf	LOCATION
City	LOCATION
"	O
for	O
the	O
Jan	O
&	O
Dean	O
opening	O
act	O
.	O
A	O
year	O
later	O
,	O
the	O
Beach	O
Boys	O
would	O
notch	O
their	O
first	O
#	O
1	O
single	O
with	O
"	O
I	O
Get	O
Around	O
.	O
"	O
As	O
Wilson	O
's	O
musical	O
efforts	O
became	O
more	O
ambitious	O
,	O
the	O
group	O
relied	O
more	O
on	O
nimble	O
session	O
players	O
,	O
on	O
tracks	O
such	O
as	O
"	O
I	O
Get	O
Around	O
"	O
and	O
"	O
When	O
I	O
Grow	O
Up	O
.	O
"	O
1965	O
led	O
to	O
greater	O
experimentation	O
behind	O
the	O
soundboard	O
with	O
Wilson	PERSON
.	O
The	O
album	O
Today	O
!	O
featured	O
less	O
focus	O
on	O
guitars	O
,	O
more	O
emphasis	O
on	O
keyboards	O
and	O
percussion	O
,	O
as	O
well	O
as	O
volume	O
experiments	O
and	O
increased	O
lyrical	O
maturity	O
.	O
It	O
is	O
considered	O
to	O
be	O
the	O
band	O
's	O
most	O
experimental	O
statements	O
prior	O
to	O
Pet	O
Sounds	O
,	O
using	O
silence	O
as	O
a	O
pre-chorus	O
,	O
clashing	O
keyboards	O
,	O
moody	O
brass	O
,	O
and	O
vocal	O
tics	O
.	O
In	O
December	O
they	O
would	O
score	O
an	O
unexpected	O
#	O
2	O
hit	O
with	O
the	O
single	O
"	O
Barbara	PERSON
Ann	PERSON
"	O
,	O
which	O
Capitol	O
Records	O
released	O
as	O
a	O
single	O
without	O
input	O
from	O
any	O
of	O
the	O
Beach	O
Boys	O
.	O
It	O
has	O
become	O
one	O
of	O
their	O
most	O
recognized	O
hits	O
over	O
the	O
years	O
and	O
was	O
a	O
cover	O
of	O
a	O
1961	O
song	O
by	O
The	O
Regents	O
.	O
Until	O
then	O
,	O
each	O
Beach	LOCATION
Boys	O
album	O
(	O
and	O
most	O
pop	O
albums	O
of	O
the	O
day	O
)	O
contained	O
a	O
few	O
"	O
filler	O
tracks	O
"	O
like	O
cover	O
songs	O
or	O
even	O
stitched-together	O
comedy	O
bits	O
.	O
Brian	PERSON
found	O
Rubber	O
Soul	O
filled	O
with	O
all-original	O
songs	O
and	O
,	O
more	O
importantly	O
,	O
all	O
good	O
ones	O
,	O
none	O
of	O
them	O
filler	O
.	O
Pet	O
Sounds	O
is	O
on	O
many	O
music	O
lists	O
as	O
one	O
of	O
the	O
greatest	O
albums	O
of	O
all	O
time	O
,	O
including	O
those	O
of	O
TIME	O
,	O
Rolling	O
Stone	O
,	O
New	O
Musical	O
Express	O
,	O
Mojo	O
,	O
and	O
The	O
Times	O
.	O
Among	O
other	O
accolades	O
,	O
Paul	PERSON
McCartney	PERSON
has	O
named	O
it	O
one	O
of	O
his	O
favorite	O
albums	O
of	O
all	O
time	O
(	O
with	O
"	O
God	O
Only	O
Knows	O
"	O
as	O
his	O
all-time	O
favorite	O
song	O
)	O
.	O
The	O
album	O
's	O
meticulously	O
layered	O
harmonies	O
and	O
inventive	O
instrumentation	O
(	O
performed	O
by	O
the	O
cream	O
of	O
Los	LOCATION
Angeles	LOCATION
session	O
musicians	O
known	O
among	O
themselves	O
as	O
The	O
Wrecking	O
Crew	O
)	O
set	O
a	O
new	O
standard	O
for	O
popular	O
music	O
.	O
The	O
tracks	O
"	O
Would	O
n't	O
It	O
Be	O
Nice	O
"	O
and	O
"	O
God	O
Only	O
Knows	O
"	O
,	O
showcased	O
Wilson	O
's	O
growing	O
mastery	O
as	O
a	O
composer	O
,	O
arranger	O
,	O
and	O
producer	O
.	O
Despite	O
the	O
critical	O
praise	O
it	O
received	O
,	O
the	O
album	O
was	O
indifferently	O
promoted	O
by	O
Capitol	O
Records	O
and	O
failed	O
to	O
become	O
the	O
major	O
hit	O
Brian	PERSON
had	O
hoped	O
it	O
would	O
be	O
(	O
only	O
reaching	O
#	O
10	O
)	O
.	O
Lead	O
singer	O
Mike	PERSON
Love	PERSON
is	O
reported	O
to	O
have	O
been	O
strongly	O
opposed	O
to	O
it	O
,	O
calling	O
it	O
"	O
Brian	PERSON
's	O
ego	O
music,	O
"	O
and	O
warning	O
the	O
composer	O
not	O
to	O
"	O
fuck	O
with	O
the	O
formula	O
.	O
"	O
Another	O
likely	O
factor	O
in	O
Love	O
's	O
antipathy	O
to	O
Pet	O
Sounds	O
was	O
that	O
Wilson	O
worked	O
extensively	O
on	O
it	O
with	O
outside	O
lyricist	O
Tony	PERSON
Asher	PERSON
rather	O
than	O
with	O
Love	O
,	O
even	O
though	O
Love	O
had	O
co-written	O
the	O
lyrics	O
for	O
many	O
of	O
their	O
earlier	O
songs	O
and	O
was	O
the	O
lead	O
vocalist	O
on	O
most	O
of	O
their	O
early	O
hits	O
.	O
Its	O
first	O
fruit	O
was	O
"	O
Good	O
Vibrations	O
"	O
,	O
which	O
Brian	PERSON
described	O
as	O
"	O
a	O
pocket	O
symphony	O
"	O
.	O
In	O
contrast	O
to	O
his	O
work	O
on	O
Pet	O
Sounds	O
,	O
Wilson	O
adopted	O
a	O
modular	O
approach	O
to	O
"	O
Good	O
Vibrations	O
"	O
--	O
he	O
broke	O
the	O
song	O
into	O
sections	O
and	O
taped	O
multiple	O
versions	O
of	O
each	O
at	O
different	O
studios	O
to	O
take	O
advantage	O
of	O
the	O
different	O
sound	O
and	O
ambience	O
of	O
each	O
facility	O
.	O
The	O
group	O
members	O
recall	O
the	O
"	O
Good	O
Vibrations	O
"	O
vocal	O
sessions	O
as	O
among	O
the	O
most	O
demanding	O
of	O
their	O
career	O
.	O
Even	O
as	O
his	O
personal	O
life	O
deteriorated	O
,	O
Wilson	O
's	O
musical	O
output	O
remained	O
remarkable	O
.	O
Several	O
biographies	O
have	O
suggested	O
that	O
his	O
father	O
may	O
have	O
had	O
bipolar	O
disorder	O
and	O
after	O
years	O
of	O
suffering	O
,	O
Wilson	PERSON
's	O
own	O
condition	O
was	O
eventually	O
diagnosed	O
as	O
schizoaffective	O
disorder	O
.	O
But	O
some	O
of	O
the	O
other	O
Beach	O
Boys	O
,	O
especially	O
Love	O
,	O
found	O
the	O
new	O
music	O
too	O
difficult	O
and	O
too	O
far	O
removed	O
from	O
their	O
established	O
style	O
.	O
Another	O
serious	O
concern	O
was	O
that	O
the	O
new	O
music	O
was	O
simply	O
not	O
feasible	O
for	O
live	O
performance	O
by	O
the	O
current	O
Beach	O
Boys	O
lineup	O
.	O
Many	O
factors	O
combined	O
to	O
put	O
intense	O
pressure	O
on	O
Brian	PERSON
Wilson	PERSON
as	O
Smile	O
neared	O
completion	O
:	O
Wilson	O
's	O
own	O
mental	O
instability	O
,	O
the	O
pressure	O
to	O
create	O
against	O
fierce	O
internal	O
opposition	O
to	O
his	O
new	O
music	O
,	O
the	O
relatively	O
unenthusiastic	O
response	O
to	O
Pet	O
Sounds	O
,	O
Carl	PERSON
Wilson	PERSON
's	PERSON
draft	O
resistance	O
,	O
and	O
a	O
major	O
dispute	O
with	O
Capitol	O
Records	O
.	O
Matters	O
were	O
complicated	O
by	O
Wilson	O
's	O
reliance	O
on	O
both	O
prescription	O
and	O
illegal	O
drugs	O
,	O
amphetamines	O
in	O
particular	O
,	O
which	O
only	O
exacerbated	O
his	O
underlying	O
mental	O
health	O
problems	O
.	O
Also	O
at	O
this	O
time	O
the	O
Beach	O
Boys	O
management	O
started	O
work	O
on	O
developing	O
and	O
implementing	O
the	O
band	O
's	O
own	O
record	O
label	O
,	O
Brother	O
.	O
The	O
Beach	O
Boys	O
became	O
one	O
of	O
the	O
first	O
rock	O
bands	O
to	O
create	O
their	O
own	O
label	O
.	O
In	O
May	O
1967	O
,	O
Smile	O
was	O
shelved	O
,	O
and	O
over	O
the	O
next	O
thirty	O
years	O
,	O
the	O
legends	O
surrounding	O
Smile	O
grew	O
until	O
it	O
became	O
the	O
most	O
famous	O
unreleased	O
album	O
in	O
the	O
history	O
of	O
popular	O
music	O
.	O
However	O
,	O
some	O
of	O
the	O
tracks	O
were	O
salvaged	O
and	O
re-recorded	O
at	O
Brian	PERSON
's	O
new	O
home	O
studio	O
,	O
albeit	O
in	O
drastically	O
scaled-down	O
versions	O
.	O
Many	O
were	O
assembled	O
by	O
Carl	PERSON
Wilson	PERSON
over	O
the	O
next	O
few	O
years	O
and	O
included	O
on	O
later	O
albums	O
.	O
The	O
band	O
was	O
still	O
expecting	O
to	O
complete	O
and	O
release	O
Smile	O
as	O
late	O
as	O
1972	O
,	O
before	O
it	O
became	O
clear	O
that	O
Brian	PERSON
had	O
been	O
the	O
only	O
one	O
who	O
could	O
have	O
made	O
sense	O
out	O
of	O
the	O
endless	O
fragments	O
that	O
were	O
recorded	O
.	O
After	O
the	O
popularity	O
of	O
the	O
song	O
"	O
Good	O
Vibrations	O
"	O
came	O
a	O
period	O
of	O
declining	O
commercial	O
success	O
.	O
Smiley	PERSON
Smile	PERSON
and	O
subsequent	O
albums	O
performed	O
poorly	O
on	O
the	O
U.S.	LOCATION
charts	O
.	O
The	O
group	O
's	O
image	O
problems	O
took	O
a	O
further	O
hit	O
following	O
their	O
withdrawal	O
from	O
the	O
bill	O
of	O
the	O
1967	O
Monterey	O
International	O
Pop	O
Festival	O
.	O
The	O
1967	O
album	O
Wild	O
Honey	O
,	O
regarded	O
by	O
some	O
as	O
another	O
classic	O
,	O
features	O
songs	O
written	O
by	O
Wilson	PERSON
and	O
Love	O
,	O
including	O
the	O
hit	O
"	O
Darlin	O
'	O
"	O
and	O
a	O
rendition	O
of	O
Stevie	PERSON
Wonder	PERSON
's	PERSON
"	O
I	O
Was	O
Made	O
to	O
Love	O
Her	O
"	O
.	O
This	O
was	O
followed	O
by	O
the	O
single	O
"	O
Do	O
It	O
Again	O
"	O
,	O
a	O
return	O
to	O
their	O
earlier-style	O
formula	O
.	O
As	O
Brian	PERSON
's	O
mental	O
and	O
physical	O
health	O
deteriorated	O
in	O
the	O
late	O
1960s	O
and	O
early	O
1970s	O
,	O
his	O
song	O
output	O
diminished	O
;	O
he	O
eventually	O
became	O
withdrawn	O
and	O
detached	O
from	O
the	O
band	O
.	O
Carl	PERSON
Wilson	PERSON
gradually	O
took	O
over	O
leadership	O
of	O
the	O
band	O
,	O
developing	O
into	O
an	O
accomplished	O
producer	O
.	O
One	O
of	O
those	O
songs	O
,	O
"	O
Never	O
Learn	O
Not	O
to	O
Love	O
"	O
,	O
featured	O
uncredited	O
lyrics	O
by	O
Charles	PERSON
Manson	PERSON
and	O
was	O
originally	O
titled	O
"	O
Cease	O
to	O
Exist	O
"	O
.	O
In	O
1969	O
,	O
the	O
Beach	O
Boys	O
reactivated	O
their	O
Brother	O
Records	O
label	O
and	O
signed	O
with	O
Reprise	O
Records	O
.	O
With	O
the	O
new	O
contract	O
,	O
the	O
band	O
appeared	O
rejuvenated	O
,	O
releasing	O
the	O
album	O
Sunflower	O
to	O
critical	O
acclaim	O
.	O
The	O
album	O
was	O
and	O
still	O
is	O
recognized	O
as	O
a	O
complete	O
group	O
effort	O
,	O
with	O
all	O
band	O
members	O
contributing	O
significant	O
material	O
,	O
such	O
as	O
"	O
Add	O
Some	O
Music	O
to	O
Your	O
Day	O
"	O
,	O
Brian	PERSON
's	O
"	O
This	O
Whole	O
World	O
"	O
,	O
Dennis	PERSON
'	O
"	O
Forever	O
"	O
and	O
Bruce	PERSON
Johnston	PERSON
's	PERSON
"	O
Tears	O
in	O
the	O
Morning	O
"	O
.	O
After	O
Sunflower	O
,	O
the	O
band	O
hired	O
Jack	PERSON
Rieley	PERSON
as	O
their	O
manager	O
.	O
The	O
result	O
was	O
1971	O
's	O
Surf	O
's	O
Up	O
,	O
featuring	O
Brian	PERSON
's	O
Smile	O
centerpiece	O
,	O
"	O
Surf	O
's	O
Up	O
"	O
.	O
Carl	PERSON
's	O
"	O
Long	O
Promised	O
Road	O
"	O
and	O
"	O
Feel	O
Flows	O
"	O
are	O
standouts	O
.	O
Brian	PERSON
contributed	O
one	O
of	O
his	O
best	O
songs	O
,	O
"	O
'	O
Til	O
I	O
Die	O
"	O
,	O
which	O
almost	O
did	O
not	O
make	O
the	O
album	O
sequencing	O
.	O
Bruce	PERSON
Johnston	PERSON
produced	O
the	O
classic	O
"	O
Disney	O
Girls	O
(	O
1957	O
)	O
"	O
,	O
a	O
throwback	O
to	O
the	O
easier	O
,	O
simpler	O
times	O
they	O
remembered	O
.	O
While	O
the	O
record	O
made	O
its	O
run	O
on	O
the	O
charts	O
,	O
the	O
Beach	O
Boys	O
added	O
to	O
their	O
refound	O
fame	O
by	O
performing	O
a	O
near-sellout	O
concert	O
at	O
Carnegie	O
Hall	O
,	O
and	O
following	O
that	O
with	O
the	O
famous	O
appearance	O
with	O
the	O
Grateful	O
Dead	O
at	O
Fillmore	O
East	O
on	O
April	O
27	O
,	O
1971	O
.	O
The	O
addition	O
of	O
Ricky	PERSON
Fataar	PERSON
and	O
Blondie	PERSON
Chaplin	PERSON
in	O
February	O
,	O
1972	O
,	O
led	O
to	O
a	O
dramatic	O
departure	O
in	O
sound	O
for	O
the	O
band	O
.	O
The	O
Beach	O
Boys	O
came	O
up	O
with	O
an	O
ambitious	O
(	O
and	O
expensive	O
)	O
plan	O
in	O
developing	O
their	O
next	O
project	O
,	O
Holland	LOCATION
.	O
The	O
band	O
,	O
their	O
families	O
,	O
assorted	O
associates	O
and	O
technicians	O
moved	O
to	O
the	LOCATION
Netherlands	LOCATION
for	O
the	O
summer	O
of	O
1972	O
,	O
renting	O
a	O
farmhouse	O
to	O
convert	O
into	O
a	O
makeshift	O
studio	O
.	O
Reprise	O
,	O
however	O
,	O
felt	O
that	O
the	O
album	O
was	O
weak	O
,	O
and	O
after	O
some	O
wrangling	O
between	O
the	O
camps	O
,	O
the	O
band	O
asked	O
Brian	PERSON
to	O
come	O
up	O
with	O
commercial	O
material	O
.	O
This	O
resulted	O
in	O
the	O
song	O
"	O
Sail	O
On	O
,	O
Sailor	O
"	O
,	O
a	O
collaboration	O
between	O
Brian	PERSON
Wilson	PERSON
and	O
Van	PERSON
Dyke	PERSON
Parks	PERSON
,	O
became	O
one	O
of	O
the	O
most	O
emblematic	O
Beach	O
Boys	O
songs	O
.	O
Reprise	O
approved	O
and	O
the	O
album	O
was	O
released	O
early	O
1973	O
,	O
peaking	O
at	O
#	O
37	O
on	O
the	O
Billboard	O
album	O
chart	O
.	O
Holland	LOCATION
proved	O
that	O
the	O
band	O
could	O
still	O
produce	O
contemporary	O
songs	O
with	O
wide	O
(	O
if	O
not	O
mass	O
)	O
appeal	O
.	O
The	O
Beach	O
Boys	O
in	O
Concert	O
,	O
a	O
double	O
album	O
documenting	O
the	O
1972	O
and	O
1973	O
US	LOCATION
tours	O
,	O
became	O
the	O
band	O
's	O
first	O
gold	O
record	O
for	O
Reprise	O
.	O
Endless	O
Summer	O
,	O
helped	O
by	O
a	O
sunny	O
,	O
colorful	O
graphic	O
cover	O
,	O
caught	O
the	O
mood	O
of	O
the	O
country	O
and	O
surged	O
to	O
#	O
1	O
on	O
the	O
Billboard	O
album	O
chart	O
.	O
It	O
was	O
the	O
group	O
's	O
first	O
multi-million	O
selling	O
record	O
since	O
"	O
Good	O
Vibrations	O
"	O
,	O
and	O
remained	O
on	O
the	O
album	O
chart	O
for	O
three	O
years	O
.	O
The	O
following	O
year	O
Capitol	O
released	O
another	O
compilation	O
,	O
Spirit	O
of	O
America	O
,	O
which	O
also	O
sold	O
well	O
.	O
Manager	O
Jack	PERSON
Rieley	PERSON
,	O
who	O
remained	O
in	O
the	O
Netherlands	LOCATION
after	O
Holland	LOCATION
's	O
release	O
,	O
was	O
relieved	O
of	O
his	O
managerial	O
duties	O
in	O
late	O
1973	O
.	O
Blondie	PERSON
Chaplin	PERSON
left	O
the	O
band	O
in	O
late	O
1973	O
after	O
an	O
argument	O
with	O
Steve	PERSON
Love	PERSON
,	O
the	O
band	O
's	O
business	O
manager	O
,	O
Ricky	PERSON
Fataar	PERSON
stayed	O
until	O
fall	O
1974	O
,	O
when	O
he	O
was	O
offered	O
a	O
chance	O
to	O
join	O
a	O
new	O
group	O
being	O
formed	O
by	O
Joe	PERSON
Walsh	PERSON
.	O
Chaplin	PERSON
's	O
replacement	O
,	O
James	PERSON
William	PERSON
Guercio	PERSON
,	O
started	O
offering	O
the	O
group	O
career	O
advice	O
that	O
turned	O
out	O
to	O
be	O
so	O
smart	O
and	O
sensible	O
that	O
eventually	O
he	O
became	O
the	O
band	O
's	O
new	O
manager	O
.	O
15	O
Big	O
Ones	O
marked	O
the	O
return	O
of	O
Brian	PERSON
Wilson	PERSON
as	O
a	O
major	O
force	O
in	O
the	O
group	O
in	O
that	O
it	O
was	O
the	O
first	O
album	O
he	O
produced	O
since	O
Pet	O
Sounds	O
.	O
This	O
album	O
included	O
several	O
new	O
songs	O
composed	O
by	O
Brian	PERSON
,	O
and	O
several	O
of	O
his	O
arrangements	O
of	O
favorite	O
old	O
songs	O
by	O
other	O
artists	O
,	O
including	O
"	O
Rock	O
and	O
Roll	O
Music	O
"	O
(	O
which	O
made	O
#	O
5	O
)	O
,	O
"	O
Blueberry	LOCATION
Hill	LOCATION
"	O
,	O
and	O
"	O
In	O
the	O
Still	O
of	O
the	O
Night	O
"	O
.	O
The	O
Beach	O
Boys	O
were	O
expected	O
to	O
finish	O
them	O
,	O
but	O
because	O
of	O
time	O
constraints	O
the	O
majority	O
of	O
the	O
material	O
was	O
released	O
as	O
Brian	PERSON
's	O
originally	O
recorded	O
demos	O
.	O
After	O
Love	O
You	O
,	O
Brian	PERSON
's	O
contributions	O
began	O
to	O
decline	O
over	O
the	O
next	O
several	O
albums	O
until	O
he	O
again	O
virtually	O
withdrew	O
from	O
the	O
group	O
.	O
Many	O
expected	O
that	O
at	O
some	O
point	O
Brian	PERSON
Wilson	PERSON
would	O
eventually	O
become	O
the	O
latest	O
in	O
a	O
long	O
line	O
of	O
celebrity	O
drug	O
casualties	O
.	O
During	O
this	O
period	O
the	O
band	O
put	O
out	O
two	O
further	O
studio	O
efforts	O
:	O
M.I.U.	O
Album	O
and	O
L.A.	O
Light	O
Album	O
.	O
Regardless	O
,	O
despite	O
a	O
handful	O
of	O
interesting	O
tracks	O
,	O
the	O
album	O
was	O
largely	O
a	O
contractual	O
obligation	O
to	O
finish	O
out	O
their	O
association	O
with	O
Reprise	O
Records	O
.	O
Reprise	O
likewise	O
did	O
not	O
promote	O
the	O
album	O
.	O
The	O
band	O
realized	O
at	O
this	O
point	O
that	O
Brian	PERSON
either	O
could	O
not	O
or	O
would	O
not	O
write	O
and	O
produce	O
the	O
required	O
material	O
.	O
As	O
a	O
stop-gap	O
measure	O
,	O
Bruce	PERSON
Johnston	PERSON
returned	O
to	O
the	O
group	O
as	O
both	O
a	O
member	O
and	O
this	O
time	O
as	O
a	O
producer	O
.	O
The	O
album	O
featured	O
outstanding	O
performances	O
by	O
both	O
Dennis	PERSON
and	O
Carl	PERSON
.	O
Again	O
,	O
Bruce	PERSON
Johnston	PERSON
was	O
in	O
the	O
producer	O
's	O
role	O
as	O
well	O
as	O
performing	O
on	O
the	O
album	O
.	O
Brian	PERSON
contributed	O
some	O
inspired	O
production	O
ideas	O
occasionally	O
as	O
seen	O
in	O
the	O
television	O
special	O
the	O
band	O
made	O
for	O
the	O
album	O
's	O
release	O
.	O
Even	O
though	O
Dennis	PERSON
Wilson	PERSON
was	O
credited,	O
this	O
was	O
the	O
first	O
Beach	LOCATION
Boys	O
album	O
not	O
to	O
feature	O
Dennis	PERSON
(	O
due	O
to	O
his	O
ongoing	O
personal	O
problems	O
)	O
.	O
During	O
the	O
ensuing	O
uproar	O
,	O
The	O
Beach	O
Boys	O
stated	O
that	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
,	O
which	O
had	O
invited	O
them	O
to	O
perform	O
in	O
Leningrad	LOCATION
in	O
1978	O
,	O
"	O
obviously	O
...	O
.	O
did	O
not	O
feel	O
that	O
the	O
group	O
attracted	O
the	O
wrong	O
element	O
"	O
.	O
Vice	O
President	O
George	PERSON
H.	PERSON
W.	PERSON
Bush	PERSON
said	O
of	O
The	O
Beach	O
Boys	O
,	O
"	O
They	O
're	O
my	O
friends	O
and	O
I	O
like	O
their	O
music	O
"	O
.	O
Watt	O
apologized	O
to	O
The	O
Beach	O
Boys	O
after	O
learning	O
that	O
President	O
Reagan	PERSON
and	O
First	O
Lady	O
Nancy	PERSON
Reagan	PERSON
were	O
fans	O
of	O
The	O
Beach	O
Boys	O
.	O
Nancy	PERSON
Reagan	PERSON
apologized	O
for	O
Watt	PERSON
.	O
Meanwhile	O
,	O
Dennis	PERSON
Wilson	PERSON
's	PERSON
personal	O
problems	O
continued	O
to	O
escalate	O
,	O
and	O
on	O
December	O
28	O
,	O
1983	O
,	O
he	O
drowned	O
while	O
diving	O
from	O
a	O
friend	O
's	O
boat	O
,	O
trying	O
to	O
recover	O
items	O
he	O
had	O
previously	O
thrown	O
overboard	O
in	O
fits	O
of	O
rage	O
.	O
Despite	O
Dennis	PERSON
's	O
death	O
,	O
the	O
Beach	O
Boys	O
soldiered	O
on	O
as	O
a	O
successful	O
touring	O
act	O
.	O
They	O
also	O
appeared	O
nine	O
days	O
later	O
at	O
the	O
Live	O
Aid	O
concert	O
.	O
In	O
1987	O
,	O
they	O
played	O
with	O
the	O
rap	O
group	O
The	O
Fat	O
Boys	O
,	O
performing	O
the	O
song	O
"	O
Wipe	O
Out	O
"	O
and	O
filming	O
a	O
video	O
for	O
it	O
.	O
In	O
1988	O
,	O
they	O
unexpectedly	O
scored	O
their	O
first	O
#	O
1	O
hit	O
in	O
22	O
years	O
with	O
the	O
song	O
"	O
Kokomo	LOCATION
"	O
which	O
was	O
written	O
for	O
the	O
movie	O
Cocktail	O
,	O
becoming	O
their	O
biggest-selling	O
hit	O
ever	O
.	O
It	O
was	O
written	O
by	O
John	PERSON
Phillips	PERSON
,	O
Scott	PERSON
McKenzie	PERSON
,	O
Mike	PERSON
Love	PERSON
,	O
and	O
Terry	PERSON
Melcher	PERSON
.	O
In	O
1990	O
,	O
the	O
band	O
,	O
featuring	O
John	PERSON
Stamos	PERSON
on	O
drums	O
,	O
recorded	O
the	O
title	O
track	O
of	O
the	O
comedy	O
Problem	O
Child	O
.	O
Stamos	PERSON
later	O
appeared	O
singing	O
lead	O
vocals	O
on	O
the	O
song	O
"	O
Forever	O
"	O
(	O
written	O
by	O
Dennis	PERSON
Wilson	PERSON
)	O
on	O
their	O
1992	O
album	O
Summer	O
in	O
Paradise	LOCATION
.	O
Members	O
of	O
the	O
band	O
appeared	O
on	O
television	O
shows	O
such	O
as	O
Full	O
House	O
,	O
Home	O
Improvement	O
,	O
and	O
Baywatch	O
in	O
the	O
late	O
1980s	O
and	O
1990s	O
,	O
as	O
well	O
as	O
touring	O
regularly	O
.	O
In	O
1995	O
,	O
Brian	PERSON
Wilson	PERSON
appeared	O
in	O
the	O
critically	O
acclaimed	O
documentary	O
I	O
Just	O
Was	O
n't	O
Made	O
for	O
These	O
Times	O
,	O
which	O
saw	O
him	O
performing	O
for	O
the	O
first	O
time	O
with	O
his	O
now-adult	O
daughters	O
,	O
Wendy	PERSON
and	O
Carnie	PERSON
of	O
the	O
group	O
Wilson	PERSON
Phillips	PERSON
.	O
After	O
years	O
of	O
heavy	O
smoking	O
,	O
Carl	PERSON
Wilson	PERSON
succumbed	O
to	O
lung	O
cancer	O
on	O
February	O
6	O
,	O
1998	O
after	O
a	O
long	O
battle	O
with	O
the	O
disease	O
.	O
Meanwhile	O
,	O
Brian	PERSON
Wilson	PERSON
and	O
Al	PERSON
Jardine	PERSON
(	O
both	O
still	O
legally	O
members	O
of	O
the	O
Beach	O
Boys	O
organization	O
)	O
each	O
pursued	O
solo	O
careers	O
with	O
their	O
new	O
bands	O
.	O
Wilson	PERSON
himself	O
implied	O
there	O
was	O
a	O
chance	O
that	O
all	O
the	O
living	O
members	O
(	O
not	O
having	O
performed	O
together	O
since	O
September	O
1996	O
)	O
would	O
reunite	O
again	O
.	O
Many	O
legal	O
difficulties	O
developed	O
from	O
Brian	PERSON
Wilson	PERSON
's	PERSON
psychological	O
problems	O
.	O
In	O
the	O
early	O
1980s	O
,	O
the	O
band	O
hired	O
controversial	O
therapist	O
Eugene	PERSON
Landy	PERSON
in	O
an	O
attempt	O
to	O
help	O
him	O
.	O
Landy	PERSON
did	O
achieve	O
some	O
significant	O
improvements	O
in	O
Wilson	O
's	O
overall	O
condition	O
;	O
from	O
his	O
own	O
admissions	O
about	O
his	O
massive	O
drug	O
intake	O
,	O
it	O
was	O
highly	O
likely	O
that	O
Wilson	PERSON
would	O
have	O
died	O
if	O
Landy	PERSON
had	O
not	O
intervened	O
.	O
After	O
accusations	O
that	O
Landy	PERSON
was	O
using	O
his	O
control	O
over	O
Wilson	PERSON
for	O
his	O
own	O
benefit	O
,	O
the	O
band	O
successfully	O
entreated	O
the	O
courts	O
to	O
separate	O
Landy	PERSON
from	O
Wilson	O
.	O
In	O
addition	O
to	O
the	O
challenges	O
over	O
the	O
use	O
of	O
the	O
band	O
's	O
name	O
and	O
over	O
the	O
best	O
way	O
to	O
care	O
for	O
Wilson	PERSON
,	O
there	O
have	O
been	O
three	O
significant	O
legal	O
cases	O
involving	O
the	O
Beach	O
Boys	O
in	O
recent	O
years	O
.	O
The	O
first	O
was	O
Wilson	O
's	O
suit	O
to	O
reclaim	O
the	O
rights	O
to	O
his	O
songs	O
and	O
the	O
group	O
's	O
publishing	O
company	O
,	O
Sea	O
of	O
Tunes	O
,	O
which	O
he	O
had	O
signed	O
away	O
to	O
his	O
father	O
in	O
1969	O
.	O
While	O
Wilson	O
failed	O
to	O
regain	O
his	O
copyrights	O
,	O
he	O
was	O
awarded	O
$	O
25	O
million	O
for	O
unpaid	O
royalties	O
.	O
The	O
second	O
lawsuit	O
stemmed	O
from	O
Wilson	O
's	O
reclamation	O
of	O
his	O
publishing	O
rights	O
.	O
Soon	O
after	O
Wilson	PERSON
won	O
his	O
Sea	O
of	O
Tunes	O
case	O
in	O
1989	O
,	O
Mike	PERSON
Love	PERSON
discovered	O
Murry	PERSON
Wilson	PERSON
did	O
not	O
properly	O
credit	O
him	O
as	O
co-writer	O
on	O
dozens	O
of	O
Beach	O
Boys	O
songs	O
,	O
including	O
"	O
California	LOCATION
Girls	O
"	O
,	O
"	O
Catch	O
a	O
Wave	O
"	O
,	O
"	O
I	O
Get	O
Around	O
"	O
,	O
"	O
When	O
I	O
Grow	O
Up	O
"	O
,	O
"	O
Be	O
True	O
to	O
Your	O
School	O
"	O
,	O
"	O
Help	O
Me	O
,	O
Rhonda	PERSON
"	O
,	O
"	O
I	O
Know	O
There	O
's	O
an	O
Answer	O
"	O
,	O
and	O
numerous	O
others	O
.	O
However	O
in	O
November	O
2005	O
,	O
Love	O
filed	O
yet	O
another	O
lawsuit	O
against	O
Brian	PERSON
Wilson	PERSON
and	O
his	O
management	O
.	O
Love	O
stated	O
at	O
the	O
time	O
:	O
"	O
Once	O
again	O
the	O
people	O
around	O
Brian	PERSON
,	O
my	O
cousin	O
and	O
collaborator	O
on	O
many	O
hits	O
,	O
who	O
I	O
love	O
and	O
care	O
about	O
,	O
have	O
used	O
him	O
for	O
their	O
own	O
financial	O
gain	O
without	O
regard	O
to	O
his	O
rights	O
,	O
or	O
my	O
rights	O
,	O
or	O
even	O
the	O
rights	O
of	O
the	O
estates	O
of	O
his	O
deceased	O
brothers	O
,	O
Carl	PERSON
and	O
Dennis	PERSON
,	O
and	O
their	O
children	O
...	O
Because	O
of	O
Brian	PERSON
's	O
mental	O
issues	O
he	O
has	O
always	O
been	O
vulnerable	O
to	O
manipulation	O
.	O
Wilson	O
's	O
website	O
listed	O
the	O
following	O
statement	O
in	O
response	O
:	O
"	O
The	O
lawsuit	O
against	O
Brian	PERSON
is	O
meritless	O
.	O
While	O
he	O
will	O
vigorously	O
defend	O
himself	O
he	O
is	O
deeply	O
saddened	O
that	O
his	O
cousin	O
Mike	PERSON
Love	PERSON
has	O
sunk	O
to	O
these	O
depths	O
for	O
his	O
own	O
financial	O
gain	O
.	O
"	O
Love	O
's	O
2005	O
lawsuit	O
was	O
dismissed	O
with	O
prejudice	O
on	O
May	O
10	O
,	O
2007	O
as	O
to	O
all	O
the	O
defendants	O
,	O
including	O
Wilson	O
.	O
In	O
a	O
series	O
of	O
rulings	O
,	O
the	O
court	O
rejected	O
all	O
of	O
Love	O
's	O
claims	O
,	O
including	O
the	O
claim	O
that	O
Smile	O
was	O
a	O
Beach	O
Boys	O
project	O
as	O
to	O
which	O
Love	O
deserved	O
compensation	O
from	O
Wilson	O
directly	O
.	O
The	O
court	O
subsequently	O
ruled	O
that	O
Love	O
had	O
to	O
pay	O
the	O
legal	O
fees	O
of	O
all	O
the	O
defendants	O
as	O
well	O
.	O
On	O
June	O
21	O
,	O
2010	O
the	O
Las	O
Vegas	O
Sun	O
reported	O
that	O
Brian	PERSON
Wilson	PERSON
would	O
join	O
The	O
Beach	O
Boys	O
for	O
their	O
50th	O
anniversary	O
.	O
In	O
a	O
story	O
dated	O
July	O
21	O
,	O
2010	O
,	O
Rolling	O
Stone	O
magazine	O
reported	O
that	O
Al	PERSON
Jardine	PERSON
stated	O
"	O
we	O
're	O
definitely	O
doing	O
at	O
least	O
one	O
show	O
"	O
in	O
2011	O
to	O
celebrate	O
the	O
50th	O
anniversary	O
of	O
the	O
formation	O
of	O
the	O
band	O
.	O
The	O
reunion	O
would	O
feature	O
all	O
the	O
surviving	O
sixties-era	O
Beach	O
Boys	O
,	O
including	O
himself	O
,	O
Brian	PERSON
Wilson	PERSON
,	O
Mike	PERSON
Love	PERSON
,	O
Bruce	PERSON
Johnston	PERSON
,	O
and	O
possibly	O
David	PERSON
Marks	PERSON
.	O
Jardine	PERSON
added	O
that	O
the	O
tension	O
between	O
various	O
ex-bandmates	O
has	O
been	O
resolved	O
.	O
The	O
group	O
was	O
inducted	O
into	O
the	O
Rock	O
and	O
Roll	O
Hall	O
of	O
Fame	O
in	O
1988	O
,	O
with	O
Mike	PERSON
Love	PERSON
delivering	O
a	O
speech	O
that	O
assailed	O
Mick	PERSON
Jagger	PERSON
,	O
Paul	PERSON
McCartney	PERSON
and	O
the	O
Beatles	O
,	O
Bruce	PERSON
Springsteen	PERSON
,	O
Billy	PERSON
Joel	PERSON
and	O
Diana	PERSON
Ross	PERSON
.	O
The	O
band	O
was	O
chosen	O
for	O
the	O
Vocal	O
Group	O
Hall	O
of	O
Fame	O
in	O
1998	O
.	O
A	O
Beach	O
Boys	O
Historic	O
Landmark	O
,	O
dedicated	O
on	O
May	O
20	O
,	O
2005	O
,	O
marks	O
the	O
location	O
.	O
The	O
Beach	O
Boys	O
continue	O
to	O
tour	O
,	O
with	O
a	O
backing	O
band	O
accompanying	O
original	O
members	O
Mike	PERSON
Love	PERSON
and	O
Bruce	PERSON
Johnston	PERSON
.	O
Other	O
"	O
honorary	O
Beach	PERSON
Boys	PERSON
"	O
,	O
such	O
as	O
John	PERSON
Stamos	PERSON
and	O
former	O
member	O
David	PERSON
Marks	PERSON
also	O
make	O
guest	O
appearances	O
on	O
their	O
tours	O
.	O
As	O
of	O
2010	O
,	O
the	O
remaining	O
Beach	O
Boys	O
continue	O
to	O
tour	O
.	O
The	O
Beach	O
Boys	O
logo	O
was	O
created	O
by	O
artist	O
Jim	PERSON
Evans	PERSON
,	O
and	O
was	O
originally	O
used	O
on	O
the	O
"	O
15	O
Big	O
Ones	O
"	O
record	O
sleeve	O
.	O
When	O
the	O
band	O
first	O
formed	O
in	O
1961	O
,	O
it	O
consisted	O
of	O
the	O
Wilson	PERSON
brothers	O
,	O
their	O
cousin	O
Mike	PERSON
Love	PERSON
,	O
and	O
their	O
friend	O
Al	PERSON
Jardine	PERSON
.	O
Jardine	PERSON
quickly	O
left	O
and	O
was	O
replaced	O
by	O
David	PERSON
Marks	PERSON
.	O
After	O
16	O
months	O
or	O
so	O
,	O
Jardine	PERSON
came	O
back	O
,	O
and	O
Marks	O
would	O
quit	O
soon	O
thereafter	O
.	O
Brian	PERSON
Wilson	PERSON
quit	O
touring	O
in	O
1965	O
,	O
and	O
at	O
first	O
,	O
Glen	PERSON
Campbell	PERSON
filled	O
in	O
for	O
him	O
.	O
In	O
the	O
early	O
1970s	O
,	O
Ricky	PERSON
Fataar	PERSON
and	O
Blondie	PERSON
Chaplin	PERSON
,	O
both	O
members	O
of	O
The	O
Flame	PERSON
,	O
joined	O
the	O
band	O
.	O
With	O
the	O
deaths	O
of	O
Dennis	PERSON
and	O
Carl	PERSON
Wilson	PERSON
,	O
surviving	O
Beach	O
Boys	O
include	O
Brian	PERSON
Wilson	PERSON
,	O
Mike	PERSON
Love	PERSON
,	O
Al	PERSON
Jardine	PERSON
,	O
David	PERSON
Marks	PERSON
and	O
Bruce	PERSON
Johnston	PERSON
.	O
