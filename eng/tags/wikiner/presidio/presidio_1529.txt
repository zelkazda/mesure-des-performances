Despite	O
its	O
proximity	O
,	O
Barnard	O
's	O
Star	O
at	O
a	O
dim	O
apparent	O
magnitude	O
of	O
about	O
nine	O
is	O
not	O
visible	O
with	O
the	O
unaided	O
eye	O
.	O
Historically	O
,	O
research	O
on	O
Barnard	O
's	O
Star	O
has	O
focused	O
on	O
measuring	O
its	O
stellar	O
characteristics	O
,	O
its	O
astrometry	O
,	O
and	O
also	O
refining	O
the	O
limits	O
of	O
possible	O
extrasolar	O
planets	O
.	O
Although	O
Barnard	O
's	O
Star	O
is	O
an	O
ancient	O
star	O
,	O
some	O
observations	O
suggest	O
that	O
it	O
still	O
experiences	O
star	O
flare	O
events	O
.	O
Barnard	O
's	O
Star	O
has	O
also	O
been	O
the	O
subject	O
of	O
some	O
controversy	O
.	O
For	O
a	O
decade	O
,	O
from	O
the	O
early	O
1960s	O
to	O
the	O
early	O
1970s	O
,	O
Peter	PERSON
van	PERSON
de	PERSON
Kamp	PERSON
claimed	O
that	O
there	O
was	O
a	O
gas	O
giant	O
planet	O
(	O
or	O
planets	O
)	O
in	O
orbit	O
around	O
Barnard	O
's	O
Star	O
.	O
Barnard	O
's	O
Star	O
is	O
also	O
notable	O
as	O
the	O
target	O
for	O
Project	O
Daedalus	O
,	O
a	O
study	O
on	O
the	O
possibility	O
of	O
fast	O
,	O
unmanned	O
travel	O
to	O
nearby	O
star	O
systems	O
.	O
This	O
compares	O
with	O
a	O
magnitude	O
of	O
−1.5	O
for	O
Sirius	O
--	O
the	O
brightest	O
star	O
in	O
the	O
night	O
sky	O
--	O
and	O
about	O
6.0	O
for	O
the	O
faintest	O
visible	O
objects	O
with	O
the	O
naked	O
eye	O
(	O
this	O
magnitude	O
scale	O
is	O
logarithmic	O
,	O
and	O
so	O
the	O
magnitude	O
of	O
9.57	O
is	O
only	O
about	O
1/27th	O
of	O
the	O
brightness	O
of	O
the	O
faintest	O
star	O
that	O
can	O
be	O
seen	O
with	O
the	O
naked	O
eye	O
under	O
good	O
viewing	O
conditions	O
)	O
.	O
Barnard	O
's	O
Star	O
has	O
lost	O
a	O
great	O
deal	O
of	O
rotational	O
energy	O
,	O
and	O
the	O
periodic	O
slight	O
changes	O
in	O
its	O
brightness	O
indicate	O
that	O
it	O
rotates	O
just	O
once	O
every	O
130	O
days	O
(	O
compared	O
with	O
just	O
over	O
25	O
days	O
for	O
the	O
Sun	O
)	O
.	O
Given	O
its	O
age	O
,	O
Barnard	O
's	O
Star	O
was	O
long	O
assumed	O
to	O
be	O
quiescent	O
in	O
terms	O
of	O
stellar	O
activity	O
.	O
However	O
in	O
1998	O
,	O
astronomers	O
observed	O
an	O
intense	O
stellar	O
flare	O
,	O
surprisingly	O
showing	O
that	O
Barnard	O
's	O
Star	O
is	O
a	O
flare	O
star	O
.	O
In	O
2003	O
,	O
Barnard	O
's	O
Star	O
presented	O
the	O
first	O
detectable	O
change	O
in	O
the	O
radial	O
velocity	O
of	O
a	O
star	O
caused	O
by	O
its	O
motion	O
.	O
Further	O
variability	O
in	O
the	O
radial	O
velocity	O
of	O
Barnard	O
's	O
Star	O
was	O
attributed	O
to	O
its	O
stellar	O
activity	O
.	O
The	O
proper	O
motion	O
of	O
Barnard	O
's	O
Star	O
corresponds	O
to	O
a	O
relative	O
lateral	O
speed	O
(	O
"	O
sideways	O
"	O
relative	O
to	O
the	O
Sun	O
)	O
of	O
90	O
kilometers	O
per	O
second	O
(	O
km/sec	O
)	O
.	O
The	O
radial	O
velocity	O
of	O
Barnard	O
's	O
Star	O
towards	O
the	O
Sun	O
can	O
be	O
measured	O
by	O
its	O
blue	O
shift	O
.	O
However	O
,	O
at	O
that	O
time	O
,	O
Barnard	O
's	O
Star	O
will	O
not	O
be	O
the	O
nearest	O
star	O
,	O
since	O
Proxima	O
Centauri	O
will	O
have	O
moved	O
even	O
closer	O
to	O
the	O
Sun	O
.	O
Barnard	O
's	O
Star	O
will	O
still	O
be	O
too	O
dim	O
to	O
be	O
seen	O
with	O
the	O
naked	O
eye	O
at	O
the	O
time	O
of	O
its	O
closest	O
approach	O
,	O
since	O
its	O
apparent	O
magnitude	O
will	O
be	O
about	O
8.5	O
then	O
.	O
Barnard	O
's	O
Star	O
has	O
approximately	O
17	O
%	O
of	O
a	O
solar	O
mass	O
,	O
and	O
it	O
has	O
a	O
radius	O
15	O
%	O
to	O
20	O
%	O
of	O
that	O
of	O
the	O
Sun	O
.	O
In	O
2003	O
,	O
its	O
radius	O
was	O
estimated	O
as	O
0.20±0.008	O
of	O
the	O
solar	O
radius	O
,	O
at	O
the	O
high	O
end	O
of	O
the	O
ranges	O
that	O
were	O
typically	O
calculated	O
in	O
the	O
past	O
,	O
indicating	O
that	O
previous	O
estimates	O
of	O
the	O
radius	O
of	O
Barnard	O
's	O
Star	O
probably	O
underestimated	O
the	O
actual	O
value	O
.	O
Thus	O
,	O
although	O
Barnard	O
's	O
Star	O
has	O
roughly	O
180	O
times	O
the	O
mass	O
of	O
Jupiter	LOCATION
,	O
its	O
radius	O
is	O
only	O
1.5	O
to	O
2.0	O
times	O
larger	O
,	O
reflecting	O
the	O
tendency	O
of	O
objects	O
in	O
the	O
brown	O
dwarf	O
range	O
to	O
be	O
about	O
the	O
same	O
size	O
.	O
For	O
a	O
decade	O
from	O
1963	O
to	O
about	O
1973	O
,	O
a	O
substantial	O
number	O
of	O
astronomers	O
accepted	O
a	O
claim	O
by	O
Peter	PERSON
van	PERSON
de	PERSON
Kamp	PERSON
that	O
he	O
had	O
detected	O
,	O
by	O
using	O
astrometry	O
,	O
a	O
perturbation	O
in	O
the	O
proper	O
motion	O
of	O
Barnard	O
's	O
Star	O
consistent	O
with	O
its	O
having	O
one	O
or	O
more	O
planets	O
comparable	O
in	O
mass	O
with	O
Jupiter	LOCATION
.	O
While	O
not	O
completely	O
ruling	O
out	O
the	O
possibility	O
of	O
planets	O
,	O
null	O
results	O
for	O
planetary	O
companions	O
continued	O
throughout	O
the	O
1980s	O
and	O
90s	O
,	O
the	O
latest	O
based	O
on	O
interferometric	O
work	O
with	O
the	O
Hubble	O
Space	O
Telescope	O
in	O
1999	O
.	O
M	O
dwarfs	O
such	O
as	O
Barnard	O
's	O
Star	O
are	O
more	O
easily	O
studied	O
than	O
larger	O
stars	O
in	O
this	O
regard	O
because	O
their	O
lower	O
masses	O
render	O
perturbations	O
more	O
obvious	O
.	O
Gatewood	PERSON
was	O
thus	O
able	O
to	O
show	O
in	O
1995	O
that	O
planets	O
with	O
10	O
times	O
the	O
mass	O
of	O
Jupiter	LOCATION
(	O
the	O
lower	O
limit	O
for	O
brown	O
dwarfs	O
)	O
were	O
impossible	O
around	O
Barnard	O
's	O
Star	O
,	O
in	O
a	O
paper	O
which	O
helped	O
refine	O
the	O
negative	O
certainty	O
regarding	O
planetary	O
objects	O
in	O
general	O
.	O
While	O
this	O
research	O
has	O
greatly	O
restricted	O
the	O
possible	O
properties	O
of	O
planets	O
around	O
Barnard	O
's	O
Star	O
,	O
it	O
has	O
not	O
ruled	O
them	O
out	O
completely	O
;	O
terrestrial	O
planets	O
would	O
be	O
difficult	O
to	O
detect	O
.	O
Barnard	O
's	O
Star	O
was	O
chosen	O
as	O
a	O
target	O
,	O
partly	O
because	O
it	O
was	O
believed	O
to	O
have	O
planets	O
.	O
The	O
initial	O
Project	O
Daedalus	O
model	O
sparked	O
further	O
theoretical	O
research	O
.	O
In	O
1980	O
,	O
Robert	PERSON
Freitas	PERSON
suggested	O
a	O
more	O
ambitious	O
plan	O
:	O
a	O
self-replicating	O
spacecraft	O
intended	O
to	O
search	O
for	O
and	O
make	O
contact	O
with	O
extraterrestrial	O
life	O
.	O
Built	O
and	O
launched	O
in	O
Jovian	O
orbit	O
,	O
it	O
would	O
reach	O
Barnard	O
's	O
Star	O
in	O
47	O
years	O
under	O
parameters	O
similar	O
to	O
those	O
of	O
the	O
original	O
Project	O
Daedalus	O
.	O
The	O
observation	O
of	O
a	O
stellar	O
flare	O
on	O
Barnard	O
's	O
Star	O
has	O
added	O
another	O
element	O
of	O
interest	O
to	O
its	O
study	O
.	O
An	O
event	O
of	O
such	O
magnitude	O
around	O
Barnard	O
's	O
Star	O
is	O
thus	O
presumed	O
to	O
be	O
a	O
rarity	O
.	O
Research	O
on	O
the	O
star	O
's	O
periodicity	O
,	O
or	O
changes	O
in	O
stellar	O
activity	O
over	O
a	O
given	O
timescale	O
,	O
also	O
suggest	O
it	O
ought	O
to	O
be	O
quiescent	O
;	O
1998	O
research	O
showed	O
weak	O
evidence	O
for	O
periodic	O
variation	O
in	O
Barnard	O
's	O
Star	O
's	O
brightness	O
,	O
noting	O
only	O
one	O
possible	O
starspot	O
over	O
130	O
days	O
.	O
Stellar	O
activity	O
of	O
this	O
sort	O
has	O
created	O
interest	O
in	O
using	O
Barnard	O
's	O
Star	O
as	O
a	O
proxy	O
to	O
understand	O
similar	O
stars	O
.	O
Barnard	O
's	O
Star	O
shares	O
much	O
the	O
same	O
neighbourhood	O
as	O
the	O
Sun	O
.	O
The	O
neighbours	O
of	O
Barnard	O
's	O
Star	O
are	O
generally	O
of	O
red	O
dwarf	O
size	O
,	O
the	O
smallest	O
and	O
most	O
common	O
star	O
type	O
.	O
Its	O
closest	O
neighbour	O
is	O
currently	O
the	O
red	O
dwarf	O
Ross	PERSON
154	O
,	O
at	O
1.66	O
parsecs	O
or	O
5.41	O
light	O
years	O
distance	O
.	O
The	O
Sun	O
and	O
Alpha	O
Centauri	O
are	O
,	O
respectively	O
,	O
the	O
next	O
closest	O
systems	O
.	O
