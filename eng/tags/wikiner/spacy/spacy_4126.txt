The	ORGANIZATION
European	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
(	O
ESA	ORGANIZATION
)	O
,	O
established	O
in	O
1975	O
,	O
is	O
an	O
intergovernmental	O
organisation	O
dedicated	O
to	O
the	O
exploration	O
of	O
space	O
,	O
currently	O
with	O
18	O
member	O
states	O
.	O
Headquartered	O
in	O
Paris	LOCATION
,	O
ESA	ORGANIZATION
has	O
a	O
staff	O
of	O
more	O
than	O
2,000	O
with	O
an	O
annual	O
budget	O
of	O
about	O
€	O
3.6	O
billion	O
in	O
2009	O
.	O
The	O
meeting	O
was	O
attended	O
by	O
scientific	O
representatives	O
from	O
eight	O
countries	O
,	O
including	O
Harrie	PERSON
Massey	PERSON
(	O
UK	LOCATION
)	O
.	O
Seven	O
research	O
satellites	O
were	O
brought	O
into	O
orbit	O
,	O
all	O
by	O
US	LOCATION
launch	O
systems	O
.	O
ESA	ORGANIZATION
had	O
10	O
founding	O
members	O
:	O
Belgium	LOCATION
,	O
Denmark	LOCATION
,	O
France	LOCATION
,	O
Germany	LOCATION
,	O
Italy	LOCATION
,	O
the	O
Netherlands	LOCATION
,	O
Spain	LOCATION
,	O
Sweden	LOCATION
,	O
Switzerland	LOCATION
and	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
.	O
ESA	ORGANIZATION
joined	O
NASA	ORGANIZATION
in	O
the	O
IUE	ORGANIZATION
,	O
the	O
world	O
's	O
first	O
high-orbit	O
telescope	O
,	O
which	O
was	O
launched	O
in	O
1978	O
and	O
operated	O
very	O
successfully	O
for	O
18	O
years	O
.	O
Ariane	PERSON
1	PERSON
,	O
launched	O
in	O
1979	O
,	O
brought	O
mostly	O
commercial	O
payloads	O
into	O
orbit	O
from	O
1984	O
onward	O
.	O
Although	O
the	O
succeeding	O
Ariane	PERSON
5	PERSON
experienced	O
a	O
failure	O
on	O
its	O
first	O
flight	O
,	O
it	O
has	O
since	O
firmly	O
established	O
itself	O
within	O
the	O
heavily	O
competitive	O
commercial	O
space	O
launch	O
market	O
with	O
40	O
successful	O
launches	O
as	O
of	O
2009	O
.	O
The	O
beginning	O
of	O
the	O
new	O
millennium	O
saw	O
ESA	ORGANIZATION
become	O
,	O
along	O
with	O
agencies	O
like	O
NASA	ORGANIZATION
,	O
JAXA	ORGANIZATION
,	O
and	O
Roscosmos	MISC
,	O
one	O
of	O
the	O
major	O
participants	O
in	O
scientific	O
space	O
research	O
.	O
While	O
ESA	ORGANIZATION
had	O
relied	O
on	O
cooperation	O
with	O
NASA	ORGANIZATION
in	O
previous	O
decades	O
,	O
especially	O
the	O
1990s	O
,	O
changed	O
circumstances	O
(	O
such	O
as	O
tough	O
legal	O
restrictions	O
on	O
information	O
sharing	O
by	O
the	LOCATION
United	LOCATION
States	LOCATION
military	O
)	O
led	O
to	O
decisions	O
to	O
rely	O
more	O
on	O
itself	O
and	O
on	O
cooperation	O
with	O
Russia	LOCATION
.	O
ESA	ORGANIZATION
maintains	O
its	O
scientific	O
and	O
research	O
projects	O
mainly	O
for	O
astronomy-space	O
missions	O
such	O
as	O
Corot	PERSON
,	O
launched	O
on	O
27	O
December	O
2006	O
,	O
a	O
milestone	O
in	O
the	O
search	O
for	O
extrasolar	O
planets	O
.	O
Since	O
the	MISC
Cold	MISC
War	MISC
ended	O
with	O
the	O
fall	O
of	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
's	LOCATION
"	O
iron	O
curtain	O
"	O
,	O
space	O
agencies	O
around	O
the	O
world	O
had	O
to	O
refocus	O
and	O
revise	O
their	O
visions	O
and	O
goals	O
.	O
ESA	ORGANIZATION
is	O
an	O
intergovernmental	O
organisation	O
of	O
18	O
member	O
states	O
.	O
The	O
following	O
table	O
gives	O
an	O
overview	O
of	O
all	O
member	O
states	O
and	O
adjunct	O
members	O
and	O
their	O
contributions	O
to	O
ESA	ORGANIZATION
in	O
2008	O
:	O
Currently	O
the	O
only	O
associated	O
member	O
of	O
ESA	ORGANIZATION
is	O
Canada	LOCATION
.	O
Previously	O
associated	O
members	O
were	O
Austria	LOCATION
,	O
Norway	LOCATION
and	O
Finland	LOCATION
,	O
all	O
of	O
which	O
later	O
joined	O
ESA	ORGANIZATION
as	O
full	O
members	O
.	O
By	O
virtue	O
of	O
this	O
accord	O
,	O
the	ORGANIZATION
Canadian	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
takes	O
part	O
in	O
ESA	ORGANIZATION
's	O
deliberative	O
bodies	O
and	O
decision-making	O
and	O
also	O
in	O
ESA	ORGANIZATION
's	O
programmes	O
and	O
activities	O
.	O
Canadian	MISC
firms	O
can	O
bid	O
for	O
and	O
receive	O
contracts	O
to	O
work	O
on	O
programmes	O
.	O
The	O
accord	O
has	O
a	O
provision	O
ensuring	O
a	O
fair	O
industrial	O
return	O
to	O
Canada	LOCATION
.	O
Nations	ORGANIZATION
who	O
want	O
to	O
become	O
a	O
full	O
member	O
of	O
ESA	ORGANIZATION
do	O
so	O
in	O
3	O
stages	O
.	O
ESA	ORGANIZATION
is	O
likely	O
to	O
expand	O
quite	O
rapidly	O
in	O
the	O
coming	O
years	O
.	O
Many	O
countries	O
,	O
most	O
of	O
which	O
joined	O
the	O
EU	ORGANIZATION
in	O
both	O
2004	O
and	O
2007	O
,	O
have	O
started	O
to	O
cooperate	O
with	O
ESA	ORGANIZATION
on	O
various	O
levels	O
:	O
The	O
budget	O
of	O
ESA	ORGANIZATION
was	O
€	O
2.977	O
billion	O
in	O
2005	O
,	O
€	O
2.904	O
billion	O
in	O
2006	O
and	O
grew	O
to	O
€	O
3.018	O
billion	O
in	O
2008	O
and	O
€	O
3.600	O
billion	O
in	O
2009	O
.	O
Every	O
3	O
--	O
4	O
years	O
,	O
ESA	ORGANIZATION
memberstates	O
agree	O
on	O
a	O
budget	O
plan	O
for	O
several	O
years	O
at	O
an	O
ESA	ORGANIZATION
memberstates	O
conference	O
.	O
This	O
plan	O
can	O
be	O
amended	O
in	O
future	O
years	O
,	O
however	O
provides	O
the	O
major	O
guideline	O
for	O
ESA	ORGANIZATION
for	O
several	O
years	O
.	O
The	O
rest	O
of	O
the	O
budget	O
consists	O
of	O
funds	O
for	O
microgravity	O
research	O
,	O
technology	O
development	O
,	O
robotic	O
exploration	O
(	O
e.g.	O
Mars	MISC
Express	MISC
)	O
and	O
general	O
budget	O
items	O
and	O
administration	O
.	O
Countries	O
typically	O
have	O
their	O
own	O
space	O
programmes	O
that	O
differ	O
in	O
how	O
they	O
operate	O
organisationally	O
and	O
financially	O
with	O
ESA	ORGANIZATION
.	O
For	O
example	O
,	O
the	O
French	MISC
space	O
agency	O
CNES	ORGANIZATION
has	O
a	O
budget	O
double	O
the	O
amount	O
it	O
contributes	O
to	O
ESA	ORGANIZATION
.	O
Several	O
space-related	O
projects	O
are	O
joint	O
projects	O
between	O
national	O
space	O
agencies	O
and	O
ESA	ORGANIZATION
(	O
e.g.	O
COROT	ORGANIZATION
)	O
.	O
ESA	ORGANIZATION
has	O
made	O
great	O
progress	O
towards	O
its	O
goal	O
of	O
having	O
a	O
complete	O
fleet	O
of	O
launch	O
vehicles	O
in	O
service	O
,	O
competing	O
in	O
all	O
sectors	O
of	O
the	O
launch	O
market	O
.	O
ESA	ORGANIZATION
's	O
fleet	O
will	O
soon	O
consist	O
of	O
three	O
major	O
rocket	O
designs	O
,	O
Ariane	PERSON
5	PERSON
,	O
Soyuz-2	O
and	O
Vega	MISC
.	O
Because	O
many	O
communication	O
satellites	O
have	O
equatorial	O
orbits	O
,	O
launches	O
from	O
French	MISC
Guiana	LOCATION
are	O
able	O
to	O
take	O
larger	O
payloads	O
into	O
space	O
than	O
from	O
more	O
northerly	O
spaceports	O
.	O
The	O
Ariane	ORGANIZATION
5	ORGANIZATION
rocket	O
is	O
the	O
primary	O
launcher	O
of	O
ESA	ORGANIZATION
.	O
The	O
launch	O
craft	O
has	O
been	O
in	O
service	O
since	O
1997	O
and	O
replaced	O
Ariane	PERSON
4	PERSON
.	O
Soyuz-2	O
(	O
also	O
called	O
the	O
Soyuz-ST	MISC
)	O
is	O
a	O
Russian	MISC
medium	O
payload	O
(	O
ca.	O
3	O
metric	O
tons	O
to	O
GTO	O
)	O
launcher	O
to	O
be	O
brought	O
into	O
ESA	ORGANIZATION
service	O
in	O
April	O
2010	O
.	O
ESA	ORGANIZATION
has	O
entered	O
into	O
a	O
€	O
340	O
million	O
joint	O
venture	O
with	O
the	ORGANIZATION
Russian	ORGANIZATION
Federal	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
over	O
the	O
use	O
of	O
the	O
Soyuz	MISC
launcher	O
.	O
Under	O
the	O
agreement	O
,	O
the	O
Russian	MISC
agency	O
manufactures	O
Soyuz	MISC
rocket	O
parts	O
for	O
ESA	ORGANIZATION
,	O
which	O
are	O
then	O
shipped	O
to	O
French	MISC
Guiana	LOCATION
for	O
assembly	O
.	O
ESA	ORGANIZATION
benefits	O
because	O
it	O
gains	O
a	O
medium	O
payloads	O
launcher	O
,	O
complementing	O
its	O
fleet	O
while	O
saving	O
on	O
development	O
costs	O
.	O
In	O
addition	O
,	O
the	O
Soyuz	MISC
rocket	O
--	O
which	O
has	O
been	O
the	O
Russian	MISC
's	O
space	O
launch	O
workhorse	O
for	O
some	O
40	O
years	O
--	O
is	O
proven	O
technology	O
with	O
a	O
good	O
safety	O
record	O
,	O
which	O
ESA	ORGANIZATION
might	O
use	O
for	O
launching	O
humans	O
into	O
space	O
.	O
Russia	LOCATION
also	O
benefits	O
in	O
that	O
it	O
gets	O
access	O
to	O
the	O
Kourou	ORGANIZATION
launch	O
site	O
.	O
Vega	MISC
is	O
ESA	ORGANIZATION
's	O
small	O
payload	O
(	O
ca.	O
1.5	O
metric	O
tons	O
to	O
700	O
km	O
orbit	O
)	O
launcher	O
;	O
its	O
first	O
launch	O
is	O
planned	O
for	O
2010	O
or	O
early	O
2011	O
.	O
Vega	MISC
itself	O
has	O
been	O
designed	O
to	O
be	O
a	O
body	O
launcher	O
with	O
three	O
solid	O
propulsion	O
stages	O
and	O
an	O
additional	O
liquid	O
propulsion	O
upper	O
module	O
to	O
place	O
the	O
cargo	O
into	O
the	O
exact	O
orbit	O
intended	O
.	O
For	O
a	O
small-cargo	O
rocket	O
it	O
is	O
remarkable	O
that	O
Vega	PERSON
will	O
be	O
able	O
to	O
place	O
multiple	O
payloads	O
into	O
orbit	O
.	O
At	O
the	O
time	O
ESA	ORGANIZATION
was	O
formed	O
,	O
its	O
main	O
goals	O
did	O
not	O
encompass	O
human	O
space	O
flight	O
,	O
rather	O
it	O
considered	O
itself	O
to	O
be	O
primarily	O
a	O
scientific	O
research	O
organisation	O
for	O
unmanned	O
space	O
exploration	O
in	O
contrast	O
to	O
its	O
American	MISC
and	O
Soviet	MISC
counterparts	O
.	O
Because	O
Chrétien	ORGANIZATION
did	O
not	O
officially	O
fly	O
into	O
space	O
as	O
an	O
ESA	ORGANIZATION
astronaut	O
,	O
but	O
rather	O
as	O
a	O
member	O
of	O
the	O
French	MISC
CNES	O
astronaut	O
corps	O
,	O
the	O
German	MISC
Ulf	PERSON
Merbold	PERSON
is	O
considered	O
the	O
first	O
ESA	ORGANIZATION
astronaut	O
to	O
fly	O
into	O
space	O
.	O
Beside	O
paying	O
for	O
Spacelab	ORGANIZATION
flights	O
and	O
seats	O
on	O
the	O
shuttles	O
,	O
ESA	ORGANIZATION
continued	O
its	O
human	O
space	O
flight	O
cooperation	O
with	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
and	O
later	O
Russia	LOCATION
,	O
including	O
numerous	O
visits	O
to	O
Mir	MISC
.	O
In	O
the	O
summer	O
of	O
2008	O
ESA	ORGANIZATION
started	O
to	O
recruit	O
new	O
astronauts	O
so	O
that	O
final	O
selection	O
would	O
be	O
due	O
spring	O
2009	O
.	O
After	O
two	O
stage	O
psychological	O
tests	O
and	O
medical	O
evaluation	O
in	O
early	O
2009	O
as	O
well	O
as	O
formal	O
interviews	O
,	O
six	O
new	O
members	O
of	O
the	ORGANIZATION
European	ORGANIZATION
Astronaut	ORGANIZATION
Corps	ORGANIZATION
were	O
selected	O
:	O
five	O
men	O
and	O
one	O
woman	O
.	O
The	O
astronauts	O
of	O
the	ORGANIZATION
European	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
are	O
:	O
Around	O
1978	O
it	O
was	O
decided	O
to	O
pursue	O
a	O
reusable	O
spacecraft	O
model	O
and	O
starting	O
in	O
November	O
1987	O
a	O
project	O
to	O
create	O
a	O
mini-shuttle	O
by	O
the	O
name	O
of	O
Hermes	ORGANIZATION
was	O
introduced	O
.	O
The	O
craft	O
itself	O
was	O
modelled	O
comparable	O
to	O
the	O
first	O
proposals	O
of	O
the	MISC
Space	MISC
Shuttle	MISC
and	O
consisted	O
of	O
a	O
small	O
reusable	O
spaceship	O
that	O
would	O
carry	O
3	O
to	O
5	O
astronauts	O
and	O
3	O
to	O
4	O
metric	O
tons	O
of	O
payload	O
for	O
scientific	O
experiments	O
.	O
With	O
a	O
total	O
maximum	O
weight	O
of	O
21	O
metric	O
tons	O
it	O
would	O
have	O
been	O
launched	O
on	O
the	O
Ariane	ORGANIZATION
5	ORGANIZATION
rocket	O
,	O
which	O
was	O
being	O
developed	O
at	O
that	O
time	O
.	O
With	O
the	O
fall	O
of	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
ESA	O
looked	O
forward	O
to	O
cooperation	O
with	O
Russia	LOCATION
to	O
build	O
a	O
next-generation	O
human	O
space	O
vehicle	O
.	O
Thus	O
the	O
Hermes	ORGANIZATION
program	O
was	O
cancelled	O
in	O
1995	O
after	O
about	O
3	O
billion	O
dollars	O
had	O
been	O
spent	O
.	O
In	O
the	O
21st	O
century	O
ESA	ORGANIZATION
started	O
new	O
programs	O
in	O
order	O
to	O
create	O
its	O
own	O
manned	O
spacecraft	O
,	O
most	O
notable	O
among	O
its	O
various	O
projects	O
and	O
proposals	O
is	O
Hopper	PERSON
,	O
whose	O
prototype	O
by	O
EADS	ORGANIZATION
,	O
called	O
Phoenix	LOCATION
,	O
has	O
already	O
been	O
tested	O
.	O
While	O
projects	O
such	O
as	O
Hopper	PERSON
are	O
neither	O
concrete	O
nor	O
to	O
be	O
realised	O
within	O
the	O
next	O
decade	O
,	O
other	O
possibilities	O
for	O
human	O
spaceflight	O
in	O
cooperation	O
with	O
the	ORGANIZATION
Russian	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
have	O
emerged	O
.	O
Following	O
talks	O
with	O
the	ORGANIZATION
Russian	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
in	O
2004	O
and	O
June	O
2005	O
,	O
a	O
cooperation	O
between	O
ESA	ORGANIZATION
and	O
the	ORGANIZATION
Russian	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
was	O
announced	O
to	O
jointly	O
work	O
on	O
the	O
Russian-designed	MISC
Kliper	LOCATION
,	O
a	O
reusable	O
spacecraft	O
that	O
would	O
be	O
available	O
for	O
space	O
travel	O
beyond	O
LEO	LOCATION
.	O
However	O
,	O
a	O
€	O
50	O
million	O
participation	O
study	O
for	O
Kliper	LOCATION
,	O
which	O
was	O
expected	O
to	O
be	O
approved	O
in	O
December	O
2005	O
,	O
was	O
finally	O
not	O
approved	O
by	O
the	O
ESA	ORGANIZATION
member	O
states	O
.	O
The	O
Russian	MISC
state	O
tender	O
for	O
the	O
Kliper	PERSON
project	O
was	O
subsequently	O
cancelled	O
in	O
the	O
summer	O
of	O
2006	O
.	O
This	O
project	O
is	O
pursued	O
with	O
Roskosmos	ORGANIZATION
instead	O
of	O
the	O
previously	O
cancelled	O
Kliper	ORGANIZATION
proposal	O
.	O
A	O
decision	O
on	O
the	O
actual	O
implementation	O
and	O
construction	O
of	O
the	O
CSTS	ORGANIZATION
spacecraft	O
is	O
contemplated	O
for	O
2008	O
,	O
with	O
the	O
major	O
design	O
decisions	O
being	O
made	O
before	O
the	O
summer	O
of	O
2007	O
.	O
ESA	ORGANIZATION
has	O
signed	O
cooperation	O
agreements	O
with	O
the	O
following	O
states	O
that	O
currently	O
neither	O
plan	O
to	O
integrate	O
as	O
tightly	O
with	O
ESA	ORGANIZATION
institutions	O
as	O
Canada	LOCATION
,	O
nor	O
envision	O
future	O
membership	O
of	O
ESA	ORGANIZATION
:	O
Argentina	LOCATION
,	O
Brazil	LOCATION
,	O
China	LOCATION
,	O
India	LOCATION
(	O
for	O
the	O
Chandrayan	O
mission	O
)	O
,	O
and	O
Russia	LOCATION
.	O
There	O
are	O
however	O
ties	O
between	O
the	O
two	O
,	O
with	O
various	O
agreements	O
in	O
place	O
and	O
being	O
worked	O
on	O
,	O
to	O
define	O
the	O
legal	O
status	O
of	O
ESA	ORGANIZATION
with	O
regard	O
to	O
the	O
EU	ORGANIZATION
.	O
On	O
certain	O
projects	O
,	O
the	O
EU	ORGANIZATION
and	O
ESA	ORGANIZATION
cooperate	O
,	O
such	O
as	O
the	O
upcoming	O
Galileo	MISC
satellite	O
navigation	O
system	O
.	O
Space	O
policy	O
has	O
since	O
December	O
2009	O
been	O
an	O
area	O
for	O
voting	O
in	O
the	ORGANIZATION
European	ORGANIZATION
Council	ORGANIZATION
.	O
An	O
independent	O
report	O
on	O
the	O
future	O
of	O
ESA	ORGANIZATION
,	O
requested	O
by	O
its	O
director-general	O
,	O
recommends	O
further	O
integration	O
of	O
ESA	ORGANIZATION
into	O
the	O
structures	O
of	O
the	O
EU	ORGANIZATION
.	O
Space	O
policy	O
would	O
be	O
decided	O
by	O
the	ORGANIZATION
European	ORGANIZATION
Council	ORGANIZATION
and	O
ESA	ORGANIZATION
would	O
be	O
the	O
de	O
facto	O
space	O
agency	O
of	O
the	ORGANIZATION
European	ORGANIZATION
Union	ORGANIZATION
,	O
not	O
excluding	O
the	O
possibility	O
of	O
making	O
it	O
a	O
formal	O
EU	ORGANIZATION
agency	O
.	O
ESA	ORGANIZATION
has	O
a	O
long	O
history	O
of	O
collaboration	O
with	O
NASA	ORGANIZATION
.	O
Since	O
ESA	ORGANIZATION
's	O
astronaut	O
corps	O
was	O
formed	O
,	O
the	MISC
Space	MISC
Shuttle	MISC
has	O
been	O
the	O
primary	O
launch	O
vehicle	O
used	O
by	O
ESA	ORGANIZATION
's	O
astronauts	O
to	O
get	O
into	O
space	O
through	O
partnership	O
programs	O
with	O
NASA	ORGANIZATION
.	O
In	O
the	O
1980s	O
and	O
1990s	O
,	O
the	O
Spacelab	ORGANIZATION
program	O
was	O
an	O
ESA-NASA	ORGANIZATION
joint	O
research	O
program	O
that	O
had	O
ESA	ORGANIZATION
develop	O
and	O
manufacture	O
orbital	O
labs	O
for	O
the	MISC
Space	MISC
Shuttle	MISC
for	O
several	O
flights	O
on	O
which	O
ESA	ORGANIZATION
participate	O
with	O
astronauts	O
in	O
experiments	O
.	O
In	O
robotic	O
science	O
mission	O
and	O
exploration	O
missions	O
,	O
NASA	ORGANIZATION
has	O
been	O
ESA	ORGANIZATION
's	O
main	O
partner	O
.	O
Also	O
,	O
the	O
Hubble	MISC
space	O
telescope	O
is	O
a	O
joint	O
project	O
of	O
NASA	ORGANIZATION
and	O
ESA	ORGANIZATION
.	O
NASA	ORGANIZATION
and	O
ESA	ORGANIZATION
will	O
also	O
likely	O
join	O
together	O
for	O
a	O
Mars	O
Sample	O
Return	O
Mission	O
.	O
Since	O
China	LOCATION
has	O
started	O
to	O
invest	O
more	O
money	O
into	O
space	O
activities	O
,	O
the	ORGANIZATION
Chinese	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
has	O
sought	O
international	O
partnerships	O
.	O
ESA	ORGANIZATION
is	O
,	O
beside	O
the	ORGANIZATION
Russian	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
,	O
one	O
of	O
its	O
most	O
important	O
partners	O
.	O
Recently	O
the	O
two	O
space	O
agencies	O
cooperated	O
in	O
the	O
development	O
of	O
the	ORGANIZATION
Double	ORGANIZATION
Star	ORGANIZATION
Mission	ORGANIZATION
.	O
ESA	ORGANIZATION
entered	O
into	O
a	O
major	O
joint	O
venture	O
with	O
Russia	LOCATION
in	O
the	O
form	O
of	O
the	O
CSTS	ORGANIZATION
,	O
the	O
preparation	O
of	O
French	MISC
Guyana	LOCATION
spaceport	O
for	O
launches	O
of	O
Soyuz	MISC
rockets	O
and	O
other	O
projects	O
.	O
With	O
India	LOCATION
ESA	ORGANIZATION
agreed	O
to	O
send	O
instruments	O
into	O
space	O
aboard	O
the	O
ISRO	ORGANIZATION
's	O
Chandrayaan	PERSON
in	O
2008	O
.	O
Each	O
ATV	ORGANIZATION
has	O
a	O
cargo	O
capacity	O
of	O
7667	O
kilograms	O
(	O
16900	O
lb	O
)	O
.	O
