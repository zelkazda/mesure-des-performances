Ariane	O
5	O
rockets	O
are	O
manufactured	O
under	O
the	O
authority	O
of	O
the	ORGANIZATION
European	ORGANIZATION
Space	ORGANIZATION
Agency	ORGANIZATION
(	O
ESA	ORGANIZATION
)	O
and	O
the	ORGANIZATION
Centre	ORGANIZATION
National	ORGANIZATION
d'Etudes	ORGANIZATION
Spatiales	ORGANIZATION
(	O
CNES	O
)	O
.	O
EADS	ORGANIZATION
Astrium	ORGANIZATION
is	O
the	O
prime	O
contractor	O
for	O
the	O
vehicles	O
,	O
leading	O
a	O
consortium	O
of	O
sub-contractors	O
.	O
EADS	ORGANIZATION
Astrium	ORGANIZATION
builds	O
the	O
rockets	O
in	O
Europe	LOCATION
and	O
Arianespace	MISC
launches	O
them	O
from	O
the	ORGANIZATION
Guiana	ORGANIZATION
Space	ORGANIZATION
Centre	ORGANIZATION
in	O
French	MISC
Guiana	LOCATION
.	O
Ariane	PERSON
5	PERSON
succeeded	O
Ariane	PERSON
4	PERSON
,	O
but	O
was	O
not	O
derived	O
from	O
it	O
directly	O
.	O
ESA	ORGANIZATION
originally	O
designed	O
Ariane	ORGANIZATION
5	ORGANIZATION
to	O
launch	O
the	O
manned	O
mini	O
shuttle	O
Hermes	MISC
,	O
and	O
thus	O
intended	O
it	O
to	O
be	O
"	O
human	O
rated	O
"	O
from	O
the	O
beginning	O
.	O
By	O
mid	O
2007	O
,	O
Arianespace	ORGANIZATION
has	O
ordered	O
a	O
total	O
of	O
99	O
Ariane	ORGANIZATION
5	ORGANIZATION
launchers	O
from	O
EADS	ORGANIZATION
Astrium	ORGANIZATION
.	O
Through	O
these	O
orders	O
,	O
the	O
Ariane	O
5	O
will	O
be	O
the	O
workhorse	O
of	O
Arianespace	ORGANIZATION
at	O
least	O
through	O
2015	O
.	O
It	O
consists	O
of	O
a	O
large	O
tank	O
30.5	O
metres	O
high	O
with	O
two	O
compartments	O
,	O
one	O
for	O
130	O
tonnes	O
of	O
liquid	O
oxygen	O
and	O
one	O
for	O
25	O
tonnes	O
of	O
liquid	O
hydrogen	O
,	O
and	O
a	O
Vulcain	MISC
engine	O
at	O
the	O
base	O
with	O
thrust	O
of	O
115	O
tonnes-force	O
(	O
1.13	O
meganewtons	O
)	O
.	O
The	O
most	O
recent	O
attempt	O
was	O
for	O
the	O
first	O
Ariane	ORGANIZATION
5	ORGANIZATION
ECA	ORGANIZATION
mission	O
.	O
One	O
of	O
the	O
two	O
boosters	O
was	O
successfully	O
recovered	O
and	O
returned	O
to	O
the	ORGANIZATION
Guiana	ORGANIZATION
Space	ORGANIZATION
Center	ORGANIZATION
for	O
analysis	O
.	O
In	O
March	O
2000	O
the	O
nose	O
cone	O
of	O
an	O
Ariane	ORGANIZATION
5	ORGANIZATION
booster	O
washed	O
ashore	O
on	O
the	O
South	LOCATION
Texas	LOCATION
coast	O
,	O
and	O
was	O
recovered	O
by	O
beachcombers	O
.	O
The	O
first	O
operational	O
use	O
of	O
restart	O
capability	O
as	O
part	O
of	O
a	O
mission	O
,	O
came	O
on	O
9	O
March	O
2008	O
,	O
when	O
two	O
burns	O
were	O
made	O
to	O
deploy	O
the	O
first	O
Automated	ORGANIZATION
Transfer	ORGANIZATION
Vehicle	ORGANIZATION
into	O
a	O
circular	O
parking	O
orbit	O
.	O
Ariane	PERSON
5	PERSON
's	PERSON
first	O
test	O
flight	O
(	O
Ariane	O
5	O
Flight	O
501	O
)	O
on	O
4	O
June	O
1996	O
failed	O
,	O
with	O
the	O
rocket	O
self-destructing	O
37	O
seconds	O
after	O
launch	O
because	O
of	O
a	O
malfunction	O
in	O
the	O
control	O
software	O
,	O
which	O
was	O
arguably	O
one	O
of	O
the	O
most	O
expensive	O
computer	O
bugs	O
in	O
history	O
.	O
The	O
software	O
was	O
originally	O
written	O
for	O
the	O
Ariane	O
4	O
where	O
efficiency	O
considerations	O
(	O
the	O
computer	O
running	O
the	O
software	O
had	O
an	O
80	O
%	O
maximum	O
workload	O
requirement	O
)	O
led	O
to	O
4	O
variables	O
being	O
protected	O
with	O
a	O
handler	O
while	O
3	O
others	O
,	O
including	O
the	O
horizontal	O
bias	O
variable	O
,	O
were	O
left	O
unprotected	O
because	O
it	O
was	O
thought	O
that	O
they	O
were	O
"	O
physically	O
limited	O
or	O
that	O
there	O
was	O
a	O
large	O
margin	O
of	O
error	O
"	O
.	O
The	O
software	O
,	O
written	O
in	O
Ada	PERSON
,	O
was	O
included	O
in	O
the	O
Ariane	O
5	O
through	O
the	O
reuse	O
of	O
an	O
entire	O
Ariane	ORGANIZATION
4	ORGANIZATION
subsystem	O
despite	O
the	O
fact	O
that	O
the	O
particular	O
software	O
containing	O
the	O
bug	O
,	O
which	O
was	O
just	O
a	O
part	O
of	O
the	O
subsystem	O
,	O
was	O
not	O
required	O
by	O
the	O
Ariane	O
5	O
because	O
it	O
has	O
a	O
different	O
preparation	O
sequence	O
than	O
the	O
Ariane	O
4	O
.	O
The	O
Vulcain	MISC
nozzle	O
caused	O
a	O
roll	O
problem	O
,	O
leading	O
to	O
premature	O
shutdown	O
of	O
the	O
core	O
stage	O
.	O
The	O
ESA	ORGANIZATION
Artemis	MISC
telecommunications	O
satellite	O
was	O
able	O
to	O
reach	O
its	O
intended	O
orbit	O
on	O
31	O
January	O
2003	O
,	O
through	O
the	O
use	O
of	O
its	O
experimental	O
ion	O
propulsion	O
system	O
.	O
At	O
8111	O
kg	O
,	O
it	O
was	O
the	O
heaviest	O
single	O
payload	O
until	O
the	O
launch	O
of	O
the	O
first	O
ATV	ORGANIZATION
on	O
March	O
9	O
,	O
2008	O
(	O
~9000	O
kg	O
)	O
.	O
After	O
this	O
failure	O
,	O
Arianespace	ORGANIZATION
SA	ORGANIZATION
delayed	O
the	O
expected	O
January	O
2003	O
launch	O
for	O
the	O
Rosetta	PERSON
mission	O
to	O
26	O
February	O
2004	O
,	O
but	O
this	O
was	O
again	O
delayed	O
to	O
early	O
March	O
2004	O
due	O
to	O
a	O
minor	O
fault	O
in	O
the	O
foam	O
that	O
protects	O
the	O
cryogenic	O
tanks	O
on	O
the	O
Ariane	O
5	O
.	O
The	O
first	O
successful	O
launch	O
of	O
the	O
Ariane	O
5	O
ECA	ORGANIZATION
took	O
place	O
on	O
12	O
February	O
2005	O
.	O
The	O
launch	O
had	O
been	O
originally	O
scheduled	O
for	O
October	O
2004	O
,	O
but	O
additional	O
testing	O
and	O
the	O
military	O
requiring	O
a	O
launch	O
at	O
that	O
time	O
(	O
of	O
an	O
Helios	MISC
2A	MISC
observation	O
satellite	O
)	O
delayed	O
the	O
attempt	O
.	O
On	O
16	O
November	O
2005	O
,	O
the	O
third	O
Ariane	ORGANIZATION
5	ORGANIZATION
ECA	ORGANIZATION
launch	O
took	O
place	O
.	O
On	O
27	O
May	O
2006	O
,	O
an	O
Ariane	ORGANIZATION
5	ORGANIZATION
ECA	ORGANIZATION
rocket	O
set	O
a	O
new	O
commercial	O
payload	O
lifting	O
record	O
of	O
8.2	O
tonnes	O
.	O
The	O
dual-payload	O
consisted	O
of	O
the	O
Thaicom	O
5	O
and	O
Satmex	ORGANIZATION
6	O
satellites	O
.	O
On	O
4	O
May	O
2007	O
the	O
Ariane	ORGANIZATION
5	ORGANIZATION
ECA	ORGANIZATION
set	O
another	O
new	O
commercial	O
record	O
,	O
lifting	O
into	O
transfer	O
orbit	O
the	O
Astra	ORGANIZATION
1L	O
and	O
Galaxy	MISC
17	MISC
communication	O
satellites	O
with	O
a	O
combined	O
weight	O
of	O
8.6	O
tonnes	O
,	O
and	O
a	O
total	O
payload	O
weight	O
of	O
9.4	O
tonnes	O
.	O
On	O
1	O
July	O
2009	O
,	O
an	O
ariane	O
5	O
ECA	ORGANIZATION
launched	O
TerreStar-1	LOCATION
,	O
the	O
largest	O
commercial	O
telecommunication	O
satellite	O
ever	O
built	O
.	O
