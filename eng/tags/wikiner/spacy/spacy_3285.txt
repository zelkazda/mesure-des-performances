David	PERSON
Hayes	PERSON
Agnew	PERSON
(	O
November	O
24	O
,	O
1818	O
--	O
March	O
22	O
,	O
1892	O
)	O
was	O
an	O
American	MISC
surgeon	O
.	O
He	O
was	O
born	O
on	O
November	O
24	O
,	O
1818	O
in	O
Lancaster	LOCATION
County	LOCATION
,	O
Pennsylvania	LOCATION
.	O
For	O
twenty-six	O
years	O
(	O
1863	O
--	O
1889	O
)	O
he	O
was	O
connected	O
with	O
the	O
medical	O
faculty	O
of	O
the	ORGANIZATION
University	ORGANIZATION
of	ORGANIZATION
Pennsylvania	ORGANIZATION
,	O
being	O
elected	O
professor	O
of	O
operative	O
surgery	O
in	O
1870	O
and	O
professor	O
of	O
the	O
principles	O
and	O
practice	O
of	O
surgery	O
in	O
the	O
following	O
year	O
.	O
From	O
1865	O
to	O
1884	O
--	O
except	O
for	O
a	O
brief	O
interval	O
--	O
he	O
was	O
a	O
surgeon	O
at	O
the	ORGANIZATION
Pennsylvania	ORGANIZATION
Hospital	ORGANIZATION
.	O
In	O
1889	O
he	O
became	O
the	O
subject	O
of	O
the	O
largest	O
painting	O
ever	O
made	O
by	O
the	O
Philadelphia	LOCATION
artist	O
Thomas	PERSON
Eakins	PERSON
,	O
called	O
The	ORGANIZATION
Agnew	ORGANIZATION
Clinic	ORGANIZATION
,	O
in	O
which	O
he	O
is	O
shown	O
conducting	O
a	O
mastectomy	O
operation	O
before	O
a	O
gallery	O
of	O
students	O
and	O
doctors	O
.	O
During	O
the	MISC
American	MISC
Civil	MISC
War	MISC
he	O
was	O
consulting	O
surgeon	O
in	O
the	ORGANIZATION
Mower	ORGANIZATION
Army	ORGANIZATION
Hospital	ORGANIZATION
,	O
near	O
Philadelphia	LOCATION
,	O
and	O
acquired	O
considerable	O
reputation	O
for	O
his	O
operations	O
in	O
cases	O
of	O
gun-shot	O
wounds	O
.	O
He	O
attended	O
as	O
operating	O
surgeon	O
when	O
President	O
Garfield	PERSON
was	O
fatally	O
wounded	O
by	O
the	O
bullet	O
of	O
an	O
assassin	O
in	O
1881	O
.	O
He	O
died	O
at	O
Philadelphia	LOCATION
on	O
March	O
22	O
,	O
1892	O
,	O
and	O
is	O
buried	O
in	O
West	ORGANIZATION
Laurel	ORGANIZATION
Hill	ORGANIZATION
Cemetery	ORGANIZATION
.	O
