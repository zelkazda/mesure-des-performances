Gygax	PERSON
is	O
generally	O
acknowledged	O
as	O
the	O
father	O
of	O
role-playing	O
games	O
.	O
In	O
the	O
1960s	O
,	O
Gygax	PERSON
created	O
an	O
organization	O
of	O
wargaming	O
clubs	O
and	O
founded	O
the	O
Gen	ORGANIZATION
Con	ORGANIZATION
gaming	O
convention	O
.	O
In	O
1971	O
,	O
he	O
helped	O
develop	O
Chainmail	O
,	O
a	O
miniatures	O
wargame	O
based	O
on	O
medieval	O
warfare	O
.	O
The	O
following	O
year	O
,	O
he	O
and	O
Dave	PERSON
Arneson	PERSON
created	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
,	O
which	O
expanded	O
on	O
his	O
work	O
on	O
Chainmail	O
and	O
included	O
elements	O
of	O
the	O
fantasy	O
stories	O
he	O
loved	O
as	O
a	O
child	O
.	O
In	O
the	O
same	O
year	O
,	O
he	O
founded	O
The	O
Dragon	O
,	O
a	O
magazine	O
based	O
around	O
the	O
new	O
game	O
.	O
In	O
1977	O
,	O
Gygax	PERSON
began	O
work	O
on	O
a	O
more	O
comprehensive	O
version	O
of	O
the	O
game	O
,	O
called	O
Advanced	ORGANIZATION
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
.	O
Gygax	PERSON
designed	O
numerous	O
manuals	O
for	O
the	O
game	O
system	O
,	O
as	O
well	O
as	O
several	O
pre-packaged	O
adventures	O
called	O
"	O
modules	O
"	O
that	O
gave	O
a	O
person	O
running	O
a	O
D	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
game	O
a	O
rough	O
script	O
and	O
ideas	O
on	O
how	O
to	O
run	O
a	O
particular	O
gaming	O
scenario	O
.	O
In	O
1983	O
,	O
he	O
worked	O
to	O
license	O
the	O
D	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
product	O
line	O
into	O
the	O
successful	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
cartoon	O
series	O
.	O
After	O
leaving	O
TSR	ORGANIZATION
in	O
1985	O
over	O
issues	O
with	O
its	O
new	O
majority	O
owner	O
,	O
Gygax	PERSON
continued	O
to	O
create	O
role-playing	O
game	O
titles	O
independently	O
,	O
beginning	O
with	O
the	O
multi-genre	O
Dangerous	MISC
Journeys	MISC
in	O
1992	O
.	O
He	O
designed	O
another	O
gaming	O
system	O
called	O
Lejendary	PERSON
Adventure	PERSON
,	O
released	O
in	O
1999	O
.	O
In	O
2005	O
,	O
Gygax	PERSON
was	O
involved	O
in	O
the	ORGANIZATION
Castles	ORGANIZATION
&	ORGANIZATION
Crusades	ORGANIZATION
role-playing	O
game	O
,	O
which	O
was	O
conceived	O
as	O
a	O
hybrid	O
between	O
D	O
&	O
D	O
'	O
s	O
third	O
edition	O
and	O
the	O
original	O
version	O
of	O
the	O
game	O
conceived	O
by	O
Gygax	PERSON
.	O
Gygax	PERSON
was	O
married	O
twice	O
and	O
had	O
six	O
children	O
.	O
His	O
interest	O
in	O
games	O
,	O
combined	O
with	O
an	O
appreciation	O
of	O
history	O
,	O
eventually	O
led	O
Gygax	PERSON
to	O
begin	O
playing	O
miniature	O
war	O
games	O
in	O
1953	O
,	O
with	O
his	O
best	O
friend	O
Don	PERSON
Kaye	PERSON
.	O
Gygax	PERSON
dropped	O
out	O
of	O
high	O
school	O
in	O
his	O
junior	O
year	O
,	O
and	O
worked	O
at	O
odd	O
jobs	O
for	O
a	O
while	O
,	O
but	O
moved	O
back	O
to	O
Chicago	LOCATION
at	O
age	O
19	O
to	O
attend	O
night	O
classes	O
in	O
junior	O
college	O
.	O
He	O
also	O
took	O
anthropology	O
classes	O
at	O
University	ORGANIZATION
of	ORGANIZATION
Chicago	ORGANIZATION
.	O
By	O
December	O
1958	O
,	O
the	O
game	O
Gettysburg	LOCATION
from	O
the	O
Avalon	ORGANIZATION
Hill	ORGANIZATION
company	O
had	O
particularly	O
captured	O
Gygax	PERSON
's	O
attention	O
.	O
It	O
was	O
also	O
from	O
Avalon	ORGANIZATION
Hill	ORGANIZATION
that	O
he	O
ordered	O
the	O
first	O
blank	O
hexagon	O
mapping	O
sheets	O
that	O
were	O
available	O
,	O
which	O
he	O
then	O
employed	O
to	O
design	O
his	O
own	O
games	O
.	O
Gygax	PERSON
became	O
active	O
in	O
fandom	O
and	O
became	O
involved	O
in	O
play-by-mail	O
Diplomacy	O
games	O
,	O
for	O
which	O
he	O
designed	O
his	O
own	O
variants	O
.	O
Then	O
Gygax	PERSON
looked	O
for	O
innovative	O
ways	O
to	O
generate	O
random	O
numbers	O
,	O
and	O
used	O
not	O
only	O
common	O
,	O
six-sided	O
dice	O
,	O
but	O
dice	O
of	O
all	O
five	O
platonic	O
solid	O
shapes	O
,	O
which	O
he	O
discovered	O
in	O
a	O
school	O
supply	O
catalog	O
.	O
In	O
1967	O
,	O
he	O
and	O
his	O
family	O
moved	O
back	O
to	O
Lake	LOCATION
Geneva	LOCATION
.	O
In	O
1967	O
,	O
Gygax	PERSON
organized	O
a	O
20-person	O
gaming	O
meet	O
in	O
the	O
basement	O
of	O
his	O
home	O
;	O
this	O
event	O
would	O
go	O
on	O
to	O
be	O
called	O
"	O
Gen	MISC
Con	MISC
0	MISC
.	O
"	O
Gen	ORGANIZATION
Con	ORGANIZATION
is	O
now	O
one	O
of	O
North	LOCATION
America	LOCATION
's	LOCATION
largest	O
annual	O
hobby-game	O
gatherings	O
.	O
Gygax	PERSON
met	O
Dave	PERSON
Arneson	PERSON
,	O
the	O
future	O
co-creator	O
of	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
,	O
at	O
the	O
second	O
Gen	O
Con	O
,	O
in	O
August	O
1969	O
.	O
In	O
1970	O
,	O
he	O
began	O
working	O
as	O
editor-in-chief	O
at	O
Guidon	ORGANIZATION
Games	ORGANIZATION
,	O
a	O
publisher	O
of	O
wargames	O
,	O
for	O
which	O
he	O
produced	O
the	O
board	O
games	O
Alexander	ORGANIZATION
the	ORGANIZATION
Great	ORGANIZATION
and	O
Dunkirk	LOCATION
in	O
1971	O
.	O
Gygax	PERSON
also	O
collaborated	O
with	O
Dave	PERSON
Arneson	PERSON
on	O
the	O
naval	O
wargame	O
Do	O
n't	O
Give	O
Up	O
the	O
Ship	O
!	O
.	O
These	O
included	O
warriors	O
who	O
were	O
monsters	O
of	O
non-human	O
races	O
,	O
drawn	O
from	O
the	O
works	O
of	O
Tolkien	ORGANIZATION
and	O
other	O
sources	O
.	O
Dave	PERSON
Arneson	PERSON
adopted	O
the	O
modified	O
rules	O
for	O
his	O
fantasy	O
Blackmoor	PERSON
campaign	O
.	O
While	O
visiting	O
Lake	LOCATION
Geneva	LOCATION
in	O
1972	O
,	O
Arneson	PERSON
ran	O
his	O
fantasy	O
game	O
using	O
the	O
new	O
rules	O
and	O
Gygax	PERSON
immediately	O
saw	O
the	O
potential	O
of	O
role-playing	O
games	O
.	O
The	O
rules	O
for	O
simulating	O
magic	O
were	O
inspired	O
by	O
the	O
works	O
of	O
fantasy	O
author	O
Jack	PERSON
Vance	PERSON
,	O
and	O
the	O
system	O
as	O
a	O
whole	O
drew	O
upon	O
the	O
work	O
of	O
authors	O
like	O
Robert	PERSON
E.	PERSON
Howard	PERSON
,	O
L.	PERSON
Sprague	PERSON
de	PERSON
Camp	PERSON
,	O
and	O
Fritz	PERSON
Leiber	PERSON
.	O
In	O
1973	O
,	O
Gygax	PERSON
quit	O
his	O
day	O
job	O
and	O
attempted	O
to	O
publish	O
the	O
game	O
through	O
Avalon	ORGANIZATION
Hill	ORGANIZATION
,	O
who	O
turned	O
down	O
his	O
offer	O
.	O
Gygax	PERSON
left	O
Guidon	ORGANIZATION
Games	ORGANIZATION
in	O
1973	O
and	O
,	O
with	O
Don	PERSON
Kaye	PERSON
as	O
a	O
partner	O
,	O
founded	O
the	O
publishing	O
company	O
Tactical	ORGANIZATION
Studies	ORGANIZATION
Rules	ORGANIZATION
in	O
October	O
.	O
However	O
,	O
this	O
did	O
not	O
give	O
them	O
enough	O
capital	O
to	O
publish	O
the	O
rules	O
for	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
and	O
,	O
worried	O
that	O
other	O
companies	O
would	O
be	O
able	O
to	O
publish	O
similar	O
projects	O
first	O
,	O
the	O
two	O
convinced	O
acquaintance	O
Brian	PERSON
Blume	PERSON
to	O
join	O
TSR	ORGANIZATION
in	O
1974	O
as	O
an	O
equal	O
one-third	O
partner	O
;	O
this	O
brought	O
the	O
financing	O
that	O
enabled	O
them	O
to	O
publish	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
.	O
Gygax	PERSON
wrote	O
the	O
supplements	O
Greyhawk	MISC
,	O
Eldritch	ORGANIZATION
Wizardry	ORGANIZATION
,	O
and	O
Swords	ORGANIZATION
&	ORGANIZATION
Spells	ORGANIZATION
for	O
the	O
original	O
D	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
game	O
.	O
With	O
Brian	PERSON
Blume	PERSON
he	O
also	O
designed	O
the	O
wild	O
west-oriented	O
role-playing	O
game	O
Boot	ORGANIZATION
Hill	ORGANIZATION
in	O
1975	O
.	O
The	O
Dungeons	O
&	O
Dragons	O
Basic	O
Set	O
,	O
a	O
variation	O
of	O
the	O
original	O
D	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
geared	O
towards	O
younger	O
players	O
and	O
edited	O
by	O
J.	PERSON
Eric	PERSON
Holmes	PERSON
,	O
was	O
released	O
in	O
1977	O
.	O
When	O
he	O
unexpectedly	O
died	O
of	O
a	O
heart	O
attack	O
in	O
January	O
1975	O
,	O
his	O
share	O
of	O
TSR	ORGANIZATION
passed	O
to	O
his	O
wife	O
,	O
a	O
woman	O
whom	O
Gygax	PERSON
characterized	O
as	O
"	O
less	O
than	O
personable	O
...	O
In	O
1977	O
,	O
a	O
new	O
version	O
of	O
D	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
,	O
Advanced	ORGANIZATION
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
(	O
AD	O
&	O
D	O
)	O
,	O
was	O
first	O
published	O
.	O
The	MISC
Monster	MISC
Manual	MISC
,	O
released	O
later	O
that	O
year	O
,	O
became	O
the	O
first	O
supplemental	O
rule	O
book	O
of	O
the	O
new	O
system	O
,	O
and	O
many	O
more	O
followed	O
.	O
The	O
AD	O
&	O
D	O
rules	O
were	O
not	O
compatible	O
with	O
those	O
of	O
D	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
,	O
and	O
as	O
a	O
result	O
,	O
D	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
and	O
AD	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
became	O
distinct	O
product	O
lines	O
.	O
Gygax	PERSON
wrote	O
the	ORGANIZATION
Advanced	ORGANIZATION
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
hardcovers	O
Players	MISC
Handbook	MISC
,	O
Dungeon	O
Masters	O
Guide	O
,	O
Monster	O
Manual	O
,	O
Monster	O
Manual	O
II	O
,	O
Unearthed	O
Arcana	MISC
,	O
and	O
Oriental	ORGANIZATION
Adventures	ORGANIZATION
.	O
In	O
1980	O
,	O
Gygax	PERSON
's	O
long-time	O
campaign	O
setting	O
of	O
Greyhawk	MISC
was	O
published	O
in	O
the	O
form	O
of	O
the	ORGANIZATION
World	ORGANIZATION
of	ORGANIZATION
Greyhawk	ORGANIZATION
Fantasy	ORGANIZATION
World	ORGANIZATION
Setting	ORGANIZATION
folio	O
,	O
which	O
was	O
expanded	O
in	O
1983	O
into	O
the	ORGANIZATION
World	ORGANIZATION
of	ORGANIZATION
Greyhawk	ORGANIZATION
Fantasy	ORGANIZATION
Game	ORGANIZATION
Setting	ORGANIZATION
boxed	O
set	O
.	O
Sales	O
of	O
the	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
game	O
reached	O
$	O
8.5	O
million	O
in	O
1980	O
.	O
In	O
1979	O
,	O
a	O
Michigan	ORGANIZATION
State	ORGANIZATION
University	ORGANIZATION
student	O
,	O
James	PERSON
Dallas	PERSON
Egbert	PERSON
III	PERSON
,	O
disappeared	O
into	O
the	O
school	O
's	O
steam	O
tunnels	O
,	O
allegedly	O
while	O
playing	O
a	O
live-action	O
version	O
of	O
D	ORGANIZATION
&	ORGANIZATION
D	ORGANIZATION
.	O
Negative	O
mainstream	O
media	O
attention	O
focused	O
on	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
as	O
a	O
result	O
.	O
(	O
Bothered	O
About	O
Dungeons	O
&	O
Dragons	O
)	O
to	O
attack	O
the	O
game	O
and	O
the	O
company	O
that	O
produced	O
it	O
.	O
Gygax	PERSON
defended	O
the	O
game	O
on	O
a	O
segment	O
of	O
60	O
Minutes	O
,	O
which	O
aired	O
in	O
1985	O
.	O
When	O
death	O
threats	O
started	O
arriving	O
at	O
the	O
TSR	O
office	O
,	O
Gygax	PERSON
hired	O
a	O
bodyguard	O
.	O
One	O
of	O
Gygax	PERSON
's	O
creations	O
during	O
this	O
time	O
was	O
Dragonchess	PERSON
,	O
a	O
three-dimensional	O
fantasy	O
chess	O
variant	O
,	O
published	O
in	O
Dragon	O
#	O
100	O
(	O
August	O
1985	O
)	O
.	O
In	O
1984	O
,	O
he	O
discovered	O
that	O
TSR	ORGANIZATION
had	O
run	O
into	O
serious	O
financial	O
difficulties	O
.	O
At	O
this	O
point	O
,	O
he	O
hired	O
Lorraine	PERSON
Williams	PERSON
to	O
manage	O
the	O
company	O
.	O
Sales	O
of	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
reached	O
$	O
29	O
million	O
by	O
1985	O
,	O
but	O
Gygax	PERSON
,	O
seeing	O
his	O
future	O
at	O
TSR	ORGANIZATION
as	O
untenable	O
,	O
left	O
the	O
company	O
on	O
December	O
31	O
,	O
1985	O
.	O
However	O
,	O
he	O
lost	O
the	O
rights	O
to	O
all	O
his	O
other	O
work	O
,	O
including	O
the	ORGANIZATION
World	ORGANIZATION
of	ORGANIZATION
Greyhawk	ORGANIZATION
and	O
the	O
names	O
of	O
all	O
the	O
characters	O
he	O
had	O
ever	O
used	O
in	O
TSR	O
material	O
,	O
such	O
as	O
Mordenkainen	PERSON
,	O
Robilar	O
,	O
and	O
Tenser	O
.	O
Gygax	PERSON
's	O
next	O
project	O
a	O
was	O
a	O
new	O
fantasy	O
role-playing	O
game	O
spanning	O
multiple	O
genres	O
called	O
Dangerous	MISC
Journeys	MISC
.	O
When	O
the	O
product	O
was	O
released	O
by	O
Game	ORGANIZATION
Designers	ORGANIZATION
'	ORGANIZATION
Workshop	ORGANIZATION
,	O
TSR	O
immediately	O
sued	O
for	O
copyright	O
infringement	O
.	O
However	O
,	O
by	O
1988	O
,	O
Gygax	PERSON
was	O
not	O
happy	O
with	O
the	O
new	O
direction	O
in	O
which	O
TSR	ORGANIZATION
was	O
taking	O
"	O
his	O
"	O
world	O
of	O
Greyhawk	LOCATION
.	O
Gygax	PERSON
also	O
wrote	O
a	O
number	O
of	O
published	O
short	O
stories	O
.	O
In	O
1995	O
,	O
he	O
began	O
work	O
on	O
a	O
new	O
computer	O
game	O
,	O
but	O
by	O
1999	O
it	O
had	O
morphed	O
into	O
book	O
form	O
,	O
and	O
was	O
published	O
as	O
a	O
new	O
role-playing	O
system	O
,	O
Lejendary	PERSON
Adventure	PERSON
.	O
Gygax	PERSON
also	O
worked	O
on	O
a	O
number	O
of	O
releases	O
with	O
the	O
d20	O
System	O
under	O
the	MISC
Open	MISC
Game	MISC
License	MISC
.	O
In	O
2003	O
,	O
Gygax	PERSON
announced	O
that	O
he	O
was	O
working	O
with	O
Rob	PERSON
Kuntz	PERSON
to	O
publish	O
the	O
original	O
and	O
previously	O
unpublished	O
details	O
of	O
Castle	MISC
Greyhawk	MISC
and	O
the	O
city	O
of	O
Greyhawk	MISC
in	O
6	O
volumes	O
,	O
although	O
the	O
project	O
would	O
use	O
the	O
rules	O
for	O
Castles	MISC
and	MISC
Crusades	MISC
rather	O
than	O
D	O
&	O
D	O
.	O
Since	O
Wizards	ORGANIZATION
of	ORGANIZATION
the	ORGANIZATION
Coast	ORGANIZATION
,	O
which	O
had	O
bought	O
TSR	ORGANIZATION
in	O
1997	O
,	O
still	O
owned	O
the	O
rights	O
to	O
the	O
name	O
"	O
Greyhawk	MISC
"	O
,	O
Gygax	PERSON
changed	O
the	O
name	O
of	O
Castle	ORGANIZATION
Greyhawk	ORGANIZATION
to	O
"	O
Castle	MISC
Zagyg	MISC
"	O
--	O
a	O
reverse	O
homophone	O
of	O
his	O
own	O
name	O
.	O
Gygax	PERSON
decided	O
he	O
would	O
compress	O
the	O
castle	O
dungeons	O
into	O
13	O
levels	O
,	O
the	O
size	O
of	O
his	O
original	O
Castle	ORGANIZATION
Greyhawk	ORGANIZATION
in	O
1973	O
,	O
by	O
amalgamating	O
the	O
best	O
of	O
what	O
could	O
be	O
gleaned	O
from	O
binders	O
and	O
boxes	O
of	O
old	O
notes	O
.	O
Even	O
this	O
slow	O
and	O
laborious	O
process	O
came	O
to	O
a	O
complete	O
halt	O
in	O
April	O
2004	O
when	O
Gygax	PERSON
suffered	O
a	O
serious	O
stroke	O
.	O
This	O
256-page	O
hardcover	O
book	O
contained	O
details	O
of	O
Gygax	PERSON
's	O
original	O
city	O
,	O
its	O
personalities	O
and	O
politics	O
,	O
as	O
well	O
as	O
over	O
30	O
encounters	O
outside	O
the	O
city	O
.	O
However	O
,	O
Gygax	PERSON
died	O
in	O
March	O
2008	O
before	O
any	O
further	O
books	O
were	O
published	O
.	O
From	O
an	O
early	O
age	O
,	O
Gygax	PERSON
hunted	O
and	O
was	O
a	O
target-shooter	O
with	O
both	O
bow	O
and	O
gun	O
.	O
By	O
1961	O
they	O
had	O
two	O
children	O
who	O
would	O
later	O
assist	O
with	O
play-testing	O
Dungeons	ORGANIZATION
&	ORGANIZATION
Dragons	ORGANIZATION
.	O
By	O
2005	O
,	O
Gygax	PERSON
had	O
seven	O
grandchildren	O
.	O
Gygax	PERSON
went	O
into	O
semi-retirement	O
after	O
suffering	O
strokes	O
on	O
April	O
1	O
and	O
May	O
4	O
,	O
2004	O
,	O
and	O
almost	O
suffered	O
a	O
heart	O
attack	O
after	O
receiving	O
incorrect	O
medication	O
to	O
prevent	O
further	O
strokes	O
.	O
Gygax	PERSON
died	O
the	O
morning	O
of	O
March	O
4	O
,	O
2008	O
,	O
at	O
his	O
home	O
in	O
Lake	LOCATION
Geneva	LOCATION
at	O
age	O
69	O
.	O
As	O
the	O
"	O
father	O
of	O
role-playing	O
games	O
"	O
,	O
Gygax	PERSON
received	O
several	O
awards	O
and	O
honors	O
related	O
to	O
gaming	O
Members	O
of	O
Gygax	PERSON
's	O
family	O
have	O
been	O
raising	O
funds	O
to	O
construct	O
a	O
memorial	O
in	O
his	O
honor	O
.	O
