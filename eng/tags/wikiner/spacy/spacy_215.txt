The	O
capital	O
of	O
Azerbaijan	LOCATION
is	O
the	O
ancient	O
city	O
of	O
Baku	LOCATION
,	O
which	O
has	O
the	O
largest	O
and	O
best	O
harbor	O
on	O
the	LOCATION
Caspian	LOCATION
Sea	LOCATION
and	O
has	O
long	O
been	O
the	O
center	O
of	O
the	O
republic	O
's	O
oil	O
industry	O
.	O
To	O
the	O
extreme	O
southeast	O
,	O
the	LOCATION
Talysh	LOCATION
Mountains	LOCATION
form	O
part	O
of	O
the	O
border	O
with	O
Iran	LOCATION
.	O
The	O
highest	O
elevations	O
occur	O
in	O
the	LOCATION
Greater	LOCATION
Caucasus	LOCATION
,	O
where	O
Mount	MISC
Bazar-dyuzi	MISC
rises	O
4,485	O
meters	O
above	O
sea	O
level	O
.	O
The	O
waters	O
of	O
the	O
reservoir	O
provide	O
hydroelectric	O
power	O
and	O
irrigation	O
of	O
the	O
Kura-Aras	ORGANIZATION
plain	O
.	O
The	O
Greater	LOCATION
Caucasus	LOCATION
range	O
,	O
with	O
the	O
country	O
's	O
highest	O
elevations	O
,	O
lies	O
in	O
the	O
north	O
along	O
the	O
border	O
with	O
Russia	LOCATION
and	O
run	O
southeast	O
to	O
the	LOCATION
Abseron	LOCATION
Peninsula	LOCATION
on	O
the	LOCATION
Caspian	LOCATION
Sea	LOCATION
.	O
The	O
Lesser	LOCATION
Caucasus	LOCATION
range	O
,	O
with	O
elevations	O
up	O
to	O
3,500	O
m	O
,	O
lies	O
to	O
the	O
west	O
along	O
the	O
border	O
with	O
Armenia	LOCATION
.	O
The	O
Talish	LOCATION
Mountains	LOCATION
form	O
part	O
of	O
the	O
border	O
with	O
Iran	LOCATION
at	O
the	O
southeast	O
tip	O
of	O
the	O
country	O
.	O
Along	O
the	O
shores	O
of	O
the	LOCATION
Caspian	LOCATION
Sea	LOCATION
it	O
is	O
temperate	O
,	O
while	O
the	O
higher	O
mountain	O
elevations	O
are	O
generally	O
cold	O
.	O
The	O
greatest	O
precipitation	O
falls	O
in	O
the	O
highest	O
elevations	O
of	O
the	O
Caucasus	LOCATION
but	O
also	O
in	O
the	LOCATION
Länkäran	LOCATION
Lowlands	LOCATION
of	O
the	O
extreme	O
southeast	O
.	O
Major	O
sources	O
of	O
pollution	O
include	O
oil	O
refineries	O
and	O
chemical	O
and	O
metallurgical	O
industries	O
,	O
which	O
in	O
the	O
early	O
1990s	O
continued	O
to	O
operate	O
as	O
inefficiently	O
as	O
they	O
had	O
in	O
the	O
Soviet	MISC
era	O
.	O
Air	O
quality	O
is	O
extremely	O
poor	O
in	O
Baku	LOCATION
,	O
the	O
center	O
of	O
oil	O
refining	O
.	O
The	O
continued	O
regular	O
use	O
of	O
the	O
pesticide	O
DDT	O
in	O
the	O
1970s	O
and	O
1980s	O
was	O
an	O
egregious	O
lapse	O
,	O
although	O
that	O
chemical	O
was	O
officially	O
banned	O
in	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
because	O
of	O
its	O
toxicity	O
to	O
humans	O
.	O
Excessive	O
application	O
of	O
pesticides	O
and	O
chemical	O
fertilizers	O
has	O
caused	O
extensive	O
groundwater	O
pollution	O
and	O
has	O
been	O
linked	O
by	O
Azerbaijani	MISC
scientists	O
to	O
birth	O
defects	O
and	O
illnesses	O
.	O
Rising	O
water	O
levels	O
in	O
the	LOCATION
Caspian	LOCATION
Sea	LOCATION
,	O
mainly	O
caused	O
by	O
natural	O
factors	O
exacerbated	O
by	O
man-made	O
structures	O
,	O
have	O
reversed	O
the	O
decades-long	O
drying	O
trend	O
and	O
now	O
threaten	O
coastal	O
areas	O
;	O
the	O
average	O
level	O
rose	O
1.5	O
meters	O
between	O
1978	O
and	O
1993	O
.	O
Because	O
of	O
the	O
Nagorno-Karabakh	LOCATION
conflict	O
,	O
large	O
numbers	O
of	O
trees	O
were	O
felled	O
,	O
roads	O
were	O
built	O
through	O
pristine	O
areas	O
,	O
and	O
large	O
expanses	O
of	O
agricultural	O
land	O
were	O
occupied	O
by	O
military	O
forces	O
.	O
