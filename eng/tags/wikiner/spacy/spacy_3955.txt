She	O
was	O
the	O
patroness	O
of	O
such	O
literary	O
figures	O
as	O
Wace	PERSON
,	O
Benoît	O
de	O
Sainte-More	O
,	O
and	O
Chrétien	PERSON
de	PERSON
Troyes	PERSON
.	O
Three	O
months	O
after	O
her	O
accession	O
she	O
married	O
Louis	PERSON
VII	PERSON
,	O
son	O
and	O
junior	O
co-ruler	O
of	O
her	O
guardian	O
,	O
King	O
Louis	PERSON
VI	PERSON
.	O
Soon	O
after	O
the	O
Crusade	O
was	O
over	O
,	O
Louis	PERSON
VII	PERSON
and	O
Eleanor	PERSON
agreed	O
to	O
dissolve	O
their	O
marriage	O
,	O
because	O
of	O
Eleanor	PERSON
's	O
own	O
desire	O
for	O
divorce	O
and	O
also	O
because	O
the	O
only	O
children	O
they	O
had	O
were	O
two	O
daughters	O
--	O
Marie	PERSON
and	O
Alix	PERSON
.	O
Over	O
the	O
next	O
thirteen	O
years	O
,	O
she	O
bore	O
Henry	PERSON
eight	PERSON
children	O
:	O
five	O
sons	O
,	O
two	O
of	O
whom	O
would	O
become	O
king	O
,	O
and	O
three	O
daughters	O
.	O
However	O
,	O
Henry	PERSON
and	O
Eleanor	PERSON
eventually	O
became	O
estranged	O
.	O
She	O
was	O
imprisoned	O
between	O
1173	O
and	O
1189	O
for	O
supporting	O
her	O
son	O
Henry	PERSON
's	O
revolt	O
against	O
her	O
husband	O
,	O
King	O
Henry	O
II	O
.	O
Eleanor	PERSON
was	O
widowed	O
on	O
6	O
July	O
1189	O
.	O
Her	O
husband	O
was	O
succeeded	O
by	O
their	O
son	O
,	O
Richard	PERSON
the	PERSON
Lionheart	PERSON
,	O
who	O
immediately	O
moved	O
to	O
release	O
his	O
mother	O
.	O
Now	O
queen	O
mother	O
,	O
Eleanor	PERSON
acted	O
as	O
a	O
regent	O
for	O
her	O
son	O
while	O
he	O
went	O
off	O
on	O
the	MISC
Third	MISC
Crusade	MISC
.	O
Eleanor	PERSON
survived	O
her	O
son	O
Richard	PERSON
and	O
lived	O
well	O
into	O
the	O
reign	O
of	O
her	O
youngest	O
son	O
King	PERSON
John	PERSON
.	O
By	O
all	O
accounts	O
,	O
Eleanor	PERSON
's	O
father	O
ensured	O
that	O
she	O
had	O
the	O
best	O
possible	O
education	O
.	O
Although	O
her	O
native	O
tongue	O
was	O
Poitevin	PERSON
,	O
she	O
was	O
taught	O
to	O
read	O
and	O
speak	O
Latin	LANGUAGE
,	O
was	O
well	O
versed	O
in	O
music	O
and	O
literature	O
,	O
and	O
schooled	O
in	O
riding	O
,	O
hawking	O
,	O
and	O
hunting	O
.	O
Eleanor	PERSON
was	O
extroverted	O
,	O
lively	O
,	O
intelligent	O
,	O
and	O
strong	O
willed	O
.	O
Eleanor	PERSON
became	O
the	O
heir	O
presumptive	O
to	O
her	O
father	O
's	O
domains	O
.	O
Later	O
,	O
during	O
the	O
first	O
four	O
years	O
of	O
Henry	PERSON
II	PERSON
's	PERSON
reign	O
,	O
all	O
three	O
siblings	O
joined	O
Eleanor	PERSON
's	O
royal	O
household	O
.	O
In	O
1137	O
,	O
Duke	O
William	PERSON
X	PERSON
set	O
out	O
from	O
Poitiers	ORGANIZATION
to	ORGANIZATION
Bordeaux	ORGANIZATION
,	O
taking	O
his	O
daughters	O
with	O
him	O
.	O
Upon	O
reaching	O
Bordeaux	LOCATION
,	O
he	O
left	O
Eleanor	PERSON
and	O
Petronilla	ORGANIZATION
in	O
the	O
charge	O
of	O
the	O
Archbishop	O
of	O
Bordeaux	LOCATION
,	O
one	O
of	O
the	O
Duke	ORGANIZATION
's	O
few	O
loyal	O
vassals	O
who	O
could	O
be	O
entrusted	O
with	O
the	O
safety	O
of	O
the	O
duke	O
's	O
daughters	O
.	O
William	PERSON
requested	O
the	O
King	O
to	O
take	O
care	O
of	O
both	O
the	O
lands	O
and	O
the	O
duchess	O
,	O
and	O
to	O
also	O
find	O
her	O
a	O
suitable	O
husband	O
.	O
However	O
,	O
until	O
a	O
husband	O
was	O
found	O
,	O
the	O
King	O
had	O
the	O
legal	O
right	O
to	O
Eleanor	PERSON
's	O
lands	O
.	O
Despite	O
his	O
immense	O
obesity	O
and	O
impending	O
mortality	O
,	O
however	O
,	O
Louis	PERSON
the	PERSON
Fat	PERSON
remained	O
clear-minded	O
.	O
On	O
25	O
July	O
1137	O
the	O
couple	O
were	O
married	O
in	O
the	MISC
Cathedral	MISC
of	MISC
Saint-André	MISC
in	O
Bordeaux	LOCATION
by	O
the	O
Archbishop	O
of	O
Bordeaux	LOCATION
.	O
On	O
1	O
August	O
,	O
Eleanor	PERSON
's	O
father-in-law	O
died	O
and	O
her	O
husband	O
became	O
sole	O
monarch	O
.	O
Louis	PERSON
was	O
personally	O
involved	O
in	O
the	O
assault	O
and	O
burning	O
of	O
the	O
town	O
of	O
Vitry	LOCATION
.	O
In	O
response	O
,	O
Eleanor	PERSON
broke	O
down	O
,	O
and	O
meekly	O
excused	O
her	O
behaviour	O
,	O
claiming	O
to	O
be	O
bitter	O
because	O
of	O
her	O
lack	O
of	O
children	O
.	O
In	O
response	O
to	O
this	O
,	O
Bernard	PERSON
became	O
more	O
kindly	O
towards	O
her	O
:	O
"	O
My	O
child	O
,	O
seek	O
those	O
things	O
which	O
make	O
for	O
peace	O
.	O
In	O
April	O
1145	O
,	O
Eleanor	PERSON
gave	O
birth	O
to	O
a	O
daughter	O
,	O
Marie	PERSON
.	O
She	O
insisted	O
on	O
taking	O
part	O
in	O
the	O
Crusades	O
as	O
the	O
feudal	O
leader	O
of	O
the	O
soldiers	O
from	O
her	O
duchy	O
.	O
The	O
story	O
that	O
she	O
and	O
her	O
ladies	O
dressed	O
as	O
Amazons	ORGANIZATION
is	O
disputed	O
by	O
serious	O
historians	O
,	O
sometime	O
confused	O
with	O
the	O
account	O
of	O
King	ORGANIZATION
Conrad	ORGANIZATION
's	ORGANIZATION
train	O
of	O
ladies	O
during	O
this	O
campaign	O
.	O
Her	O
testimonial	O
launch	O
of	O
the	MISC
Second	MISC
Crusade	MISC
from	O
Vézelay	O
,	O
the	O
rumored	O
location	O
of	O
Mary	PERSON
Magdalene	PERSON
´s	PERSON
burial	O
,	O
dramatically	O
emphasized	O
the	O
role	O
of	O
women	O
in	O
the	O
campaign	O
.	O
The	O
Crusade	O
itself	O
achieved	O
little	O
.	O
Louis	PERSON
and	O
Eleanor	PERSON
stayed	O
in	O
the	MISC
Philopation	MISC
palace	MISC
,	O
just	O
outside	O
the	O
city	O
walls	O
.	O
Since	O
he	O
was	O
Eleanor	PERSON
's	O
vassal	O
,	O
many	O
believed	O
that	O
it	O
was	O
she	O
who	O
had	O
been	O
ultimately	O
responsible	O
for	O
the	O
change	O
in	O
plan	O
,	O
and	O
thus	O
the	O
massacre	O
.	O
From	O
here	O
the	O
army	O
was	O
split	O
by	O
a	O
land	O
march	O
with	O
the	O
royalty	O
taking	O
the	O
sea	O
path	O
to	O
Antioch	LOCATION
.	O
She	O
introduced	O
those	O
conventions	O
in	O
her	O
own	O
lands	O
,	O
on	O
the	O
island	O
of	O
Oleron	ORGANIZATION
in	O
1160	O
and	O
later	O
in	O
England	LOCATION
as	O
well	O
.	O
Clearly	O
,	O
Eleanor	PERSON
supported	O
his	O
desire	O
to	O
re-capture	O
the	O
nearby	O
County	O
of	O
Edessa	LOCATION
,	O
the	O
cause	O
of	O
the	O
Crusade	O
;	O
in	O
addition	O
,	O
having	O
been	O
close	O
to	O
him	O
in	O
their	O
youth	O
,	O
she	O
now	O
showed	O
excessive	O
affection	O
towards	O
her	O
uncle	O
--	O
whilst	O
many	O
historians	O
today	O
dismiss	O
this	O
as	O
familial	O
affection	O
(	O
noting	O
their	O
early	O
friendship	O
,	O
and	O
his	O
similarity	O
to	O
her	O
father	O
and	O
grandfather	O
)	O
,	O
most	O
at	O
the	O
time	O
firmly	O
believed	O
the	O
two	O
to	O
be	O
involved	O
in	O
an	O
incestuous	O
and	O
adulterous	O
affair	O
.	O
Failing	O
in	O
this	O
attempt	O
,	O
they	O
retired	O
to	O
Jerusalem	LOCATION
,	O
and	O
then	O
home	O
.	O
Although	O
they	O
escaped	O
this	O
predicament	O
unharmed	O
,	O
stormy	O
weather	O
served	O
to	O
drive	O
Eleanor	PERSON
's	O
ship	O
far	O
to	O
the	O
south	O
,	O
and	O
to	O
similarly	O
lose	O
her	O
husband	O
.	O
On	O
21	O
March	O
,	O
the	O
four	O
archbishops	O
,	O
with	O
the	O
approval	O
of	O
Pope	PERSON
Eugenius	PERSON
,	O
granted	O
an	O
annulment	O
due	O
to	O
consanguinity	O
within	O
the	O
fourth	O
degree	O
.	O
On	O
18	O
May	O
1152	O
(	O
Whit	O
Sunday	O
)	O
,	O
six	O
weeks	O
after	O
her	O
annulment	O
,	O
Eleanor	PERSON
married	O
Henry	PERSON
'	O
without	O
the	O
pomp	O
and	O
ceremony	O
that	O
befitted	O
their	O
rank	O
'	O
.	O
A	O
marriage	O
between	O
Henry	PERSON
and	O
Eleanor	PERSON
's	O
daughter	O
,	O
Marie	PERSON
,	O
had	O
indeed	O
been	O
declared	O
impossible	O
for	O
this	O
very	O
reason	O
.	O
Over	O
the	O
next	O
thirteen	O
years	O
,	O
she	O
bore	O
Henry	PERSON
five	PERSON
sons	O
and	O
three	O
daughters	O
:	O
William	PERSON
,	O
Henry	PERSON
,	O
Richard	PERSON
,	O
Geoffrey	PERSON
,	O
John	PERSON
,	O
Matilda	ORGANIZATION
,	O
Eleanor	PERSON
,	O
and	O
Joan	PERSON
.	O
Eleanor	PERSON
's	O
marriage	O
to	O
Henry	PERSON
was	O
reputed	O
to	O
be	O
tumultuous	O
and	O
argumentative	O
,	O
although	O
sufficiently	O
cooperative	O
to	O
produce	O
at	O
least	O
eight	O
pregnancies	O
.	O
Henry	PERSON
was	O
by	O
no	O
means	O
faithful	O
to	O
his	O
wife	O
and	O
had	O
a	O
reputation	O
for	O
philandering	O
.	O
Their	O
son	O
,	O
William	PERSON
,	O
and	O
Henry	PERSON
's	O
illegitimate	O
son	O
,	O
Geoffrey	PERSON
,	O
were	O
born	O
just	O
months	O
apart	O
.	O
Henry	PERSON
fathered	O
other	O
illegitimate	O
children	O
throughout	O
the	O
marriage	O
.	O
Little	O
is	O
known	O
of	O
Eleanor	PERSON
's	O
involvement	O
in	O
these	O
events	O
.	O
By	O
late	O
1166	O
,	O
and	O
the	O
birth	O
of	O
her	O
final	O
child	O
,	O
however	O
,	O
Henry	PERSON
's	O
notorious	O
affair	O
with	O
Rosamund	PERSON
Clifford	PERSON
had	O
become	O
known	O
,	O
and	O
her	O
marriage	O
to	O
Henry	PERSON
appears	O
to	O
have	O
become	O
terminally	O
strained	O
.	O
Afterwards	O
,	O
Eleanor	PERSON
proceeded	O
to	O
gather	O
together	O
her	O
movable	O
possessions	O
in	O
England	LOCATION
and	O
transport	O
them	O
on	O
several	O
ships	O
in	O
December	O
to	O
Argentan	LOCATION
.	O
At	O
the	O
royal	O
court	O
,	O
celebrated	O
there	O
that	O
Christmas	O
,	O
she	O
appears	O
to	O
have	O
agreed	O
to	O
a	O
separation	O
from	O
Henry	PERSON
.	O
Of	O
all	O
her	O
influence	O
on	O
culture	O
,	O
Eleanor	PERSON
's	O
time	O
in	O
Poitiers	PERSON
was	O
perhaps	O
the	O
most	O
critical	O
and	O
yet	O
very	O
little	O
is	O
known	O
as	O
to	O
what	O
happened	O
.	O
At	O
the	O
time	O
Henry	PERSON
went	O
off	O
to	O
do	O
his	O
own	O
business	O
,	O
most	O
likely	O
deciding	O
to	O
become	O
invisible	O
for	O
a	O
short	O
while	O
as	O
the	O
drama	O
caused	O
by	O
Becket	PERSON
's	O
murder	O
cooled-off	O
.	O
We	O
know	O
that	O
the	O
court	O
culminated	O
in	O
1174	O
,	O
and	O
Eleanor	PERSON
was	O
about	O
52	O
(	O
a	O
woman	O
far	O
superior	O
in	O
age	O
and	O
status	O
to	O
those	O
around	O
her	O
)	O
.	O
Several	O
women	O
,	O
including	O
Eleanor	PERSON
and	O
her	O
daughter	O
Marie	PERSON
de	PERSON
Champagne	PERSON
,	O
would	O
sit	O
and	O
listen	O
to	O
the	O
quarrels	O
of	O
lovers	O
and	O
act	O
as	O
a	O
jury	O
to	O
the	O
questions	O
of	O
the	O
court	O
that	O
revolved	O
around	O
acts	O
of	O
courtly	O
love	O
.	O
In	O
March	O
1173	O
,	O
aggrieved	O
at	O
his	O
lack	O
of	O
power	O
and	O
egged	O
on	O
by	O
his	O
father	O
's	O
enemies	O
,	O
the	O
younger	O
Henry	PERSON
launched	O
the	O
Revolt	O
of	O
1173	O
--	O
1174	O
.	O
He	O
fled	O
to	O
Paris	LOCATION
.	O
Once	O
her	O
sons	O
had	O
left	O
for	O
Paris	LOCATION
,	O
Eleanor	PERSON
encouraged	O
the	O
lords	O
of	O
the	O
south	O
to	O
rise	O
up	O
and	O
support	O
them	O
.	O
On	O
8	O
July	O
1174	O
,	O
Henry	PERSON
took	O
ship	O
for	O
England	LOCATION
from	O
Barfleur	LOCATION
.	O
He	O
brought	O
Eleanor	PERSON
on	O
the	O
ship	O
.	O
Eleanor	PERSON
was	O
imprisoned	O
for	O
the	O
next	O
sixteen	O
years	O
,	O
much	O
of	O
the	O
time	O
in	O
various	O
locations	O
in	O
England	LOCATION
.	O
During	O
her	O
imprisonment	O
,	O
Eleanor	PERSON
had	O
become	O
more	O
and	O
more	O
distant	O
with	O
her	O
sons	O
,	O
especially	O
Richard	PERSON
(	O
who	O
had	O
always	O
been	O
her	O
favorite	O
)	O
.	O
She	O
did	O
not	O
have	O
the	O
opportunity	O
to	O
see	O
her	O
sons	O
very	O
often	O
during	O
her	O
imprisonment	O
,	O
though	O
she	O
was	O
released	O
for	O
special	O
occasions	O
such	O
as	O
Christmas	O
.	O
Henry	PERSON
lost	O
his	O
great	O
love	O
,	O
Rosamund	PERSON
Clifford	PERSON
,	O
in	O
1176	O
.	O
He	O
had	O
met	O
her	O
in	O
1166	O
and	O
began	O
the	O
liaison	O
in	O
1173	O
,	O
supposedly	O
contemplating	O
divorce	O
from	O
Eleanor	PERSON
.	O
Had	O
she	O
done	O
so	O
,	O
Henry	PERSON
might	O
have	O
appointed	O
Eleanor	PERSON
abbess	PERSON
of	O
Fontevrault	LOCATION
(	O
Fontevraud	O
)	O
,	O
requiring	O
her	O
to	O
take	O
a	O
vow	O
of	O
poverty	O
,	O
thereby	O
releasing	O
her	O
titles	O
and	O
nearly	O
half	O
their	O
empire	O
to	O
him	O
,	O
but	O
Eleanor	PERSON
was	O
much	O
too	O
wily	O
to	O
be	O
provoked	O
into	O
this	O
.	O
Nevertheless	O
,	O
rumours	O
persisted	O
,	O
perhaps	O
assisted	O
by	O
Henry	PERSON
's	O
camp	O
,	O
that	O
Eleanor	PERSON
had	O
poisoned	O
Rosamund	PERSON
.	O
No	O
one	O
knows	O
what	O
Henry	PERSON
believed	O
,	O
but	O
he	O
did	O
donate	O
much	O
money	O
to	O
the	ORGANIZATION
Godstow	ORGANIZATION
Nunnery	ORGANIZATION
in	O
which	O
Rosamund	PERSON
was	O
buried	O
.	O
In	O
debt	O
and	O
refused	O
control	O
of	O
Normandy	LOCATION
,	O
he	O
tried	O
to	O
ambush	O
his	O
father	O
at	O
Limoges	LOCATION
.	O
Henry	PERSON
's	O
troops	O
besieged	O
the	O
town	O
,	O
forcing	O
his	O
son	O
to	O
flee	O
.	O
When	O
his	O
father	O
's	O
ring	O
was	O
sent	O
to	O
him	O
,	O
he	O
begged	O
that	O
his	O
father	O
would	O
show	O
mercy	O
to	O
his	O
mother	O
,	O
and	O
that	O
all	O
his	O
companions	O
would	O
plead	O
with	O
Henry	PERSON
to	O
set	O
her	O
free	O
.	O
Eleanor	PERSON
had	O
had	O
a	O
dream	O
in	O
which	O
she	O
foresaw	O
her	O
son	O
Henry	PERSON
's	O
death	O
.	O
In	O
1193	O
she	O
would	O
tell	O
Pope	O
Celestine	PERSON
III	PERSON
that	O
she	O
was	O
tortured	O
by	O
his	O
memory	O
.	O
For	O
this	O
reason	O
Henry	PERSON
summoned	O
Eleanor	PERSON
to	O
Normandy	LOCATION
in	O
the	O
late	O
summer	O
of	O
1183	O
.	O
She	O
stayed	O
in	O
Normandy	LOCATION
for	O
six	O
months	O
.	O
This	O
was	O
the	O
beginning	O
of	O
a	O
period	O
of	O
greater	O
freedom	O
for	O
the	O
still	O
supervised	O
Eleanor	PERSON
.	O
Eleanor	PERSON
went	O
back	O
to	O
England	LOCATION
probably	O
early	O
in	O
1184	O
.	O
Over	O
the	O
next	O
few	O
years	O
Eleanor	PERSON
often	O
traveled	O
with	O
her	O
husband	O
and	O
was	O
sometimes	O
associated	O
with	O
him	O
in	O
the	O
government	O
of	O
the	O
realm	O
,	O
but	O
still	O
had	O
a	O
custodian	O
so	O
that	O
she	O
was	O
not	O
free	O
.	O
Upon	O
Henry	PERSON
's	O
death	O
on	O
6	O
July	O
1189	O
,	O
Richard	PERSON
was	O
his	O
undisputed	O
heir	O
.	O
She	O
ruled	O
England	LOCATION
as	O
regent	O
while	O
Richard	PERSON
went	O
off	O
on	O
the	MISC
Third	MISC
Crusade	MISC
.	O
Eleanor	PERSON
survived	O
Richard	PERSON
and	O
lived	O
well	O
into	O
the	O
reign	O
of	O
her	O
youngest	O
son	O
King	PERSON
John	PERSON
.	O
Now	O
77	O
,	O
Eleanor	PERSON
set	O
out	O
from	O
Poitiers	LOCATION
.	O
Eleanor	PERSON
selected	O
the	O
younger	O
daughter	O
,	O
Blanche	PERSON
.	O
Late	O
in	O
March	O
,	O
Eleanor	PERSON
and	O
her	O
granddaughter	O
Blanche	PERSON
journeyed	O
back	O
across	O
the	O
Pyrenees	LOCATION
.	O
This	O
tragedy	O
was	O
too	O
much	O
for	O
the	O
elderly	O
Queen	PERSON
,	O
who	O
was	O
fatigued	O
and	O
unable	O
to	O
continue	O
to	O
Normandy	LOCATION
.	O
The	O
exhausted	O
Eleanor	PERSON
went	O
to	O
Fontevrault	ORGANIZATION
,	O
where	O
she	O
remained	O
.	O
In	O
early	O
summer	O
,	O
Eleanor	PERSON
was	O
ill	O
and	O
John	PERSON
visited	O
her	O
at	O
Fontevrault	ORGANIZATION
.	O
Eleanor	PERSON
was	O
again	O
unwell	O
in	O
early	O
1201	O
.	O
When	O
war	O
broke	O
out	O
between	O
John	PERSON
and	O
Philip	PERSON
,	O
Eleanor	PERSON
declared	O
her	O
support	O
for	O
John	PERSON
,	O
and	O
set	O
out	O
from	O
Fontevrault	ORGANIZATION
for	O
her	O
capital	O
Poitiers	O
to	O
prevent	O
her	O
grandson	O
Arthur	PERSON
,	O
John	PERSON
's	O
enemy	O
,	O
from	O
taking	O
control	O
.	O
As	O
soon	O
as	O
John	PERSON
heard	O
of	O
this	O
he	O
marched	O
south	O
,	O
overcame	O
the	O
besiegers	O
and	O
captured	O
Arthur	ORGANIZATION
.	O
Eleanor	PERSON
then	O
returned	O
to	O
Fontevrault	LOCATION
where	O
she	O
took	O
the	O
veil	O
as	O
a	O
nun	O
.	O
Eleanor	PERSON
died	O
in	O
1204	O
and	O
was	O
entombed	O
in	O
Fontevraud	MISC
Abbey	MISC
next	O
to	O
her	O
husband	O
Henry	PERSON
and	O
her	O
son	O
Richard	PERSON
.	O
Her	O
tomb	O
effigy	O
shows	O
her	O
reading	O
a	O
Bible	MISC
and	O
is	O
decorated	O
with	O
magnificent	O
jewelry	O
.	O
By	O
the	O
time	O
of	O
her	O
death	O
she	O
had	O
outlived	O
all	O
of	O
her	O
children	O
except	O
for	O
King	PERSON
John	PERSON
and	O
Queen	PERSON
Eleanor	PERSON
.	O
Eleanor	PERSON
was	O
very	O
beautiful	O
:	O
all	O
contemporary	O
sources	O
agree	O
on	O
this	O
point	O
.	O
When	O
she	O
was	O
around	O
30	O
,	O
which	O
would	O
have	O
been	O
considered	O
middle	O
aged	O
by	O
medieval	O
standards	O
,	O
Bernard	PERSON
de	PERSON
Ventadour	PERSON
,	O
a	O
noted	O
troubadour	O
,	O
called	O
her	O
"	O
gracious	O
,	O
lovely	O
,	O
the	O
embodiment	O
of	O
charm,	O
"	O
extolling	O
her	O
"	O
lovely	O
eyes	O
and	O
noble	O
countenance	O
"	O
and	O
declaring	O
that	O
she	O
was	O
"	O
one	O
meet	O
to	O
crown	O
the	O
state	O
of	O
any	O
king	O
.	O
"	O
However	O
,	O
no	O
one	O
left	O
a	O
more	O
detailed	O
description	O
of	O
Eleanor	PERSON
.	O
The	O
12th-century	O
ideal	O
of	O
beauty	O
was	O
blonde	O
hair	O
and	O
blue	O
eyes	O
;	O
thus	O
many	O
have	O
suggested	O
that	O
the	O
chroniclers	O
would	O
not	O
have	O
been	O
so	O
exuberant	O
in	O
their	O
praises	O
if	O
Eleanor	PERSON
had	O
not	O
conformed	O
to	O
this	O
ideal	O
.	O
The	O
mural	O
,	O
which	O
was	O
painted	O
during	O
Eleanor	PERSON
's	O
lifetime	O
in	O
a	O
region	O
in	O
which	O
she	O
was	O
well	O
known	O
and	O
almost	O
certainly	O
depicts	O
her	O
,	O
shows	O
a	O
woman	O
with	O
reddish-brown	O
hair	O
.	O
What	O
is	O
certain	O
is	O
that	O
from	O
an	O
early	O
age	O
Eleanor	PERSON
attracted	O
the	O
attention	O
of	O
men	O
,	O
not	O
only	O
because	O
of	O
her	O
looks	O
but	O
also	O
because	O
of	O
her	O
"	O
welcoming	O
"	O
manner	O
and	O
inherent	O
flirtatiousness	O
and	O
wit	O
.	O
The	O
Abbot	PERSON
Suger	PERSON
stops	O
to	O
chat	O
with	O
Eleanor	PERSON
and	O
stays	O
to	O
wait	O
,	O
too	O
.	O
The	O
flashbacks	O
trace	O
the	O
highlights	O
of	O
Eleanor	PERSON
's	O
life	O
from	O
1137	O
to	O
her	O
death	O
in	O
1204	O
.	O
Eleanor	PERSON
has	O
also	O
featured	O
in	O
a	O
number	O
of	O
screen	O
versions	O
of	O
Ivanhoe	LOCATION
and	O
the	O
Robin	PERSON
Hood	PERSON
story	O
.	O
She	O
was	O
portrayed	O
by	O
Lynda	PERSON
Bellingham	PERSON
in	O
the	O
BBC	ORGANIZATION
series	O
Robin	PERSON
Hood	PERSON
.	O
