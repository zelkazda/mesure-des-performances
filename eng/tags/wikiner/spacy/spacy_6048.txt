The	MISC
International	MISC
Olympiad	MISC
in	MISC
Informatics	MISC
(	O
IOI	ORGANIZATION
)	O
is	O
an	O
annual	O
computer	O
science	O
competition	O
for	O
secondary	O
school	O
students	O
.	O
The	O
first	O
IOI	ORGANIZATION
was	O
held	O
in	O
1989	O
in	O
Pravetz	LOCATION
,	O
Bulgaria	LOCATION
.	O
UNESCO	ORGANIZATION
and	O
IFIP	O
are	O
patrons	O
of	O
the	MISC
International	MISC
Olympiad	MISC
in	MISC
Informatics	MISC
.	O
Usually	O
to	O
solve	O
a	O
task	O
the	O
contestant	O
has	O
to	O
write	O
a	O
computer	O
program	O
(	O
in	O
C	O
,	O
C++	O
or	O
Pascal	PERSON
)	O
and	O
submit	O
it	O
before	O
the	O
five	O
hour	O
competition	O
time	O
ends	O
.	O
For	O
IOI	O
2010	O
,	O
tasks	O
are	O
divided	O
into	O
subtasks	O
with	O
graduated	O
difficulty	O
,	O
and	O
points	O
are	O
awarded	O
only	O
when	O
all	O
tests	O
for	O
a	O
particular	O
subtask	O
yield	O
correct	O
results	O
,	O
within	O
specific	O
time	O
and	O
memory	O
limits	O
.	O
IOI	O
2010	O
will	O
for	O
the	O
first	O
time	O
have	O
a	O
live	O
web	O
scoreboard	O
with	O
real-time	O
provisional	O
results	O
.	O
Unlike	O
other	O
science	O
olympiads	O
,	O
the	O
IOI	ORGANIZATION
regulations	O
specifically	O
prohibit	O
ranking	O
by	O
countries	O
.	O
The	O
following	O
is	O
a	O
list	O
of	O
the	O
top	O
30	O
performers	O
in	O
the	O
history	O
of	O
the	O
IOI	ORGANIZATION
.	O
This	O
list	O
includes	O
only	O
those	O
countries	O
where	O
the	O
national	O
selection	O
contest	O
allows	O
the	O
same	O
participant	O
to	O
go	O
multiple	O
times	O
to	O
the	O
IOI	ORGANIZATION
.	O
