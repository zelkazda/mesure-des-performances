Charlize	PERSON
Theron	PERSON
is	O
a	O
South	MISC
African	MISC
actress	O
,	O
film	O
producer	O
,	O
and	O
former	O
fashion	O
model	O
.	O
She	O
rose	O
to	O
fame	O
in	O
the	O
late	O
1990s	O
following	O
her	O
roles	O
in	O
2	O
Days	O
in	O
the	O
Valley	LOCATION
,	O
Mighty	O
Joe	O
Young	O
,	O
The	O
Devil	O
's	O
Advocate	O
,	O
and	O
The	ORGANIZATION
Cider	ORGANIZATION
House	ORGANIZATION
Rules	ORGANIZATION
.	O
She	O
received	O
critical	O
acclaim	O
and	O
an	O
Academy	O
Award	O
for	O
her	O
portrayal	O
of	O
serial	O
killer	O
Aileen	PERSON
Wuornos	PERSON
in	O
the	O
film	O
Monster	O
,	O
for	O
which	O
she	O
became	O
the	O
first	O
African	MISC
to	O
win	O
an	O
Academy	O
Award	O
in	O
a	O
major	O
acting	O
category	O
.	O
She	O
received	O
another	O
Academy	ORGANIZATION
Award	ORGANIZATION
nomination	O
for	O
her	O
performance	O
in	O
North	LOCATION
Country	LOCATION
.	O
She	O
grew	O
up	O
on	O
her	O
parents	O
'	O
farm	O
in	O
Benoni	LOCATION
,	O
near	O
Johannesburg	LOCATION
.	O
At	O
the	O
age	O
of	O
15	O
,	O
Charlize	PERSON
Theron	PERSON
witnessed	O
the	O
death	O
of	O
her	O
father	O
,	O
reportedly	O
an	O
abusive	O
alcoholic	O
;	O
her	O
mother	O
shot	O
him	O
in	O
self-defence	O
when	O
he	O
attacked	O
her	O
.	O
At	O
the	O
age	O
of	O
16	O
,	O
Theron	PERSON
traveled	O
to	O
Milan	LOCATION
on	O
a	O
one-year	O
modeling	O
contract	O
,	O
after	O
winning	O
a	O
local	O
competition	O
.	O
A	O
knee	O
injury	O
closed	O
this	O
career	O
path	O
when	O
Theron	PERSON
was	O
19	O
.	O
Unable	O
to	O
dance	O
,	O
Theron	PERSON
flew	O
to	O
L.A.	LOCATION
on	O
a	O
one-way	O
ticket	O
her	O
mother	O
bought	O
her	O
.	O
When	O
the	O
teller	O
refused	O
to	O
cash	O
it	O
,	O
Theron	PERSON
immediately	O
started	O
a	O
shouting	O
match	O
with	O
her	O
.	O
She	O
later	O
fired	O
him	O
as	O
her	O
manager	O
after	O
he	O
kept	O
sending	O
her	O
scripts	O
for	O
films	O
similar	O
to	O
Showgirls	O
and	O
Species	O
.	O
After	O
eight	O
months	O
in	O
the	O
city	O
,	O
she	O
was	O
cast	O
in	O
her	O
first	O
film	O
part	O
,	O
a	O
non-speaking	O
role	O
in	O
the	O
direct-to-video	O
film	O
Children	MISC
of	MISC
the	MISC
Corn	MISC
III	MISC
(	MISC
1995	MISC
)	O
.	O
Larger	O
roles	O
in	O
widely	O
released	O
Hollywood	LOCATION
films	O
followed	O
,	O
and	O
her	O
career	O
skyrocketed	O
in	O
the	O
late	O
1990s	O
with	O
box	O
office	O
successes	O
like	O
The	O
Devil	O
's	O
Advocate	O
(	O
1997	O
)	O
,	O
Mighty	O
Joe	O
Young	O
(	O
1998	O
)	O
,	O
and	O
The	ORGANIZATION
Cider	ORGANIZATION
House	ORGANIZATION
Rules	ORGANIZATION
(	O
1999	O
)	O
.	O
After	O
appearing	O
in	O
a	O
few	O
notable	O
films	O
,	O
Theron	PERSON
starred	O
as	O
the	O
serial	O
killer	O
Aileen	PERSON
Wuornos	PERSON
in	O
Monster	O
(	O
2003	O
)	O
.	O
Film	O
critic	O
Roger	PERSON
Ebert	PERSON
called	O
it	O
"	O
one	O
of	O
the	O
greatest	O
performances	O
in	O
the	O
history	O
of	O
the	O
cinema	O
"	O
.	O
For	O
this	O
role	O
,	O
Theron	PERSON
won	O
the	MISC
Academy	MISC
Award	MISC
for	MISC
Best	MISC
Actress	MISC
at	O
the	O
76th	O
Academy	O
Awards	O
in	O
February	O
2004	O
,	O
as	O
well	O
as	O
the	O
SAG	O
Award	O
and	O
the	O
Golden	O
Globe	O
Award	O
.	O
She	O
is	O
the	O
first	O
African	MISC
to	O
win	O
an	O
Oscar	MISC
for	MISC
Best	MISC
Actress	MISC
.	O
The	O
Oscar	PERSON
win	O
pushed	O
her	O
to	O
The	ORGANIZATION
Hollywood	ORGANIZATION
Reporter	ORGANIZATION
's	ORGANIZATION
2006	O
list	O
of	O
highest-paid	O
actresses	O
in	O
Hollywood	LOCATION
;	O
earning	O
$	O
10,000,000	O
for	O
both	O
her	O
subsequent	O
films	O
,	O
North	LOCATION
Country	LOCATION
and	O
Æon	LOCATION
Flux	LOCATION
,	O
she	O
ranked	O
seventh	O
,	O
behind	O
Halle	PERSON
Berry	PERSON
,	O
Cameron	PERSON
Diaz	PERSON
,	O
Drew	PERSON
Barrymore	PERSON
,	O
Renée	PERSON
Zellweger	PERSON
,	O
Reese	PERSON
Witherspoon	PERSON
,	O
and	O
Nicole	PERSON
Kidman	PERSON
.	O
On	O
September	O
30	O
,	O
2005	O
,	O
Theron	PERSON
received	O
her	O
own	O
star	O
on	O
the	ORGANIZATION
Hollywood	ORGANIZATION
Walk	ORGANIZATION
of	ORGANIZATION
Fame	ORGANIZATION
.	O
In	O
the	O
same	O
year	O
,	O
she	O
starred	O
in	O
the	O
financially	O
unsuccessful	O
science	O
fiction	O
thriller	O
Æon	PERSON
Flux	PERSON
.	O
Theron	PERSON
received	O
Best	ORGANIZATION
Actress	ORGANIZATION
Academy	ORGANIZATION
Award	ORGANIZATION
and	O
Golden	O
Globe	O
nominations	O
for	O
her	O
lead	O
performance	O
in	O
the	O
drama	O
North	LOCATION
Country	LOCATION
.	O
She	O
also	O
received	O
Golden	ORGANIZATION
Globe	ORGANIZATION
and	O
Emmy	MISC
nominations	O
for	O
her	O
role	O
of	O
Britt	PERSON
Ekland	PERSON
in	O
the	O
2004	O
HBO	ORGANIZATION
movie	O
The	MISC
Life	MISC
and	MISC
Death	MISC
of	MISC
Peter	MISC
Sellers	MISC
.	O
In	O
2008	O
,	O
Theron	PERSON
was	O
named	O
the	ORGANIZATION
Hasty	ORGANIZATION
Pudding	ORGANIZATION
Theatricals	ORGANIZATION
Woman	ORGANIZATION
of	ORGANIZATION
the	ORGANIZATION
Year	ORGANIZATION
.	O
On	O
November	O
10	O
,	O
2008	O
,	O
TV	ORGANIZATION
Guide	ORGANIZATION
reported	O
that	O
Theron	PERSON
will	O
star	O
in	O
the	O
film	O
adaptation	O
of	O
The	O
Danish	MISC
Girl	O
alongside	O
Nicole	PERSON
Kidman	PERSON
.	O
Theron	PERSON
will	O
play	O
Gerda	PERSON
Wegener	PERSON
,	O
wife	O
of	O
Einar	PERSON
Wegener/Lili	PERSON
Elbe	PERSON
(	O
Kidman	PERSON
)	O
,	O
the	O
world	O
's	O
first	O
known	O
person	O
to	O
undergo	O
sex	O
reassignment	O
surgery	O
.	O
On	O
December	O
4	O
,	O
2009	O
,	O
Theron	ORGANIZATION
co-presented	O
the	O
draw	O
for	O
the	O
2010	O
FIFA	O
World	MISC
Cup	MISC
in	O
Cape	LOCATION
Town	LOCATION
,	O
South	LOCATION
Africa	LOCATION
,	O
accompanied	O
by	O
several	O
other	O
celebrities	O
of	O
South	MISC
African	MISC
origin	O
.	O
While	O
filming	O
Æon	MISC
Flux	MISC
in	O
Berlin	LOCATION
,	O
Germany	LOCATION
,	O
Theron	PERSON
suffered	O
a	O
herniated	O
disc	O
in	O
her	O
neck	O
,	O
which	O
occurred	O
as	O
a	O
result	O
of	O
her	O
suffering	O
a	O
fall	O
while	O
filming	O
a	O
series	O
of	O
back	O
handsprings	O
.	O
She	O
was	O
hospitalized	O
at	O
Cedars-Sinai	ORGANIZATION
Hospital	ORGANIZATION
and	O
she	O
finished	O
convalescing	O
in	O
her	O
own	O
home	O
.	O
From	O
October	O
2005	O
to	O
December	O
2006	O
,	O
Theron	ORGANIZATION
earned	O
$	O
3,000,000	O
for	O
the	O
use	O
of	O
her	O
image	O
in	O
a	O
worldwide	O
print	O
media	O
advertising	O
campaign	O
for	O
Raymond	PERSON
Weil	PERSON
watches	O
.	O
In	O
February	O
2006	O
,	O
she	O
and	O
her	O
loan-out	O
corporation	O
were	O
sued	O
by	O
Weil	PERSON
for	O
breach	O
of	O
contract	O
.	O
Theron	PERSON
is	O
involved	O
in	O
women	O
's	O
rights	O
organizations	O
,	O
and	O
marched	O
for	O
abortion	O
rights	O
.	O
Theron	PERSON
is	O
a	O
supporter	O
of	O
animal	O
rights	O
and	O
active	O
member	O
of	O
PETA	ORGANIZATION
.	O
She	O
appeared	O
in	O
a	O
PETA	O
ad	O
for	O
their	O
anti-fur	O
campaign	O
.	O
She	O
is	O
also	O
an	O
active	O
supporter	O
of	O
Democracy	ORGANIZATION
Now	ORGANIZATION
!	O
and	O
Link	ORGANIZATION
TV	ORGANIZATION
.	O
