Diego	PERSON
Armando	PERSON
Maradona	PERSON
is	O
an	O
Argentine	MISC
former	O
football	O
player	O
and	O
was	O
manager	O
of	O
the	O
Argentine	MISC
national	O
team	O
between	O
November	O
2008	O
and	O
July	O
2010	O
.	O
Over	O
the	O
course	O
of	O
his	O
professional	O
club	O
career	O
Maradona	PERSON
played	O
for	O
Argentinos	ORGANIZATION
Juniors	ORGANIZATION
,	O
Boca	ORGANIZATION
Juniors	ORGANIZATION
,	O
Barcelona	LOCATION
,	O
Napoli	LOCATION
,	O
Sevilla	ORGANIZATION
and	O
Newell	ORGANIZATION
's	ORGANIZATION
Old	ORGANIZATION
Boys	ORGANIZATION
,	O
setting	O
world-record	O
contract	O
fees	O
.	O
In	O
his	O
international	O
career	O
,	O
playing	O
for	O
Argentina	LOCATION
,	O
he	O
earned	O
91	O
caps	O
and	O
scored	O
34	O
goals	O
.	O
In	O
that	O
same	O
tournament	O
's	O
quarter-final	O
round	O
he	O
scored	O
two	O
goals	O
in	O
a	O
2	O
--	O
1	O
victory	O
over	O
England	LOCATION
that	O
entered	O
football	O
history	O
,	O
though	O
for	O
two	O
very	O
different	O
reasons	O
.	O
For	O
various	O
reasons	O
,	O
Maradona	PERSON
is	O
considered	O
one	O
of	O
the	O
sport	O
's	O
most	O
controversial	O
and	O
newsworthy	O
figures	O
.	O
Although	O
he	O
had	O
little	O
previous	O
managerial	O
experience	O
,	O
he	O
became	O
head	O
coach	O
of	O
the	O
Argentina	LOCATION
national	O
team	O
in	O
November	O
2008	O
.	O
Maradona	PERSON
was	O
born	O
in	O
Lanús	PERSON
,	O
but	O
raised	O
in	O
Villa	ORGANIZATION
Fiorito	ORGANIZATION
,	O
a	O
shantytown	O
on	O
the	O
southern	O
outskirts	O
of	O
Buenos	LOCATION
Aires	LOCATION
,	O
to	O
a	O
poor	O
family	O
that	O
had	O
moved	O
from	O
Corrientes	LOCATION
Province	LOCATION
.	O
On	O
20	O
October	O
1976	O
,	O
Maradona	PERSON
made	O
his	O
professional	O
debut	O
with	O
Argentinos	ORGANIZATION
Juniors	ORGANIZATION
,	O
ten	O
days	O
before	O
his	O
sixteenth	O
birthday	O
.	O
He	O
played	O
there	O
between	O
1976	O
and	O
1981	O
,	O
before	O
his	O
£	O
1m	O
transfer	O
to	O
Boca	ORGANIZATION
Juniors	ORGANIZATION
.	O
Joining	O
the	O
squad	O
midway	O
through	O
the	O
1981	O
season	O
,	O
Maradona	PERSON
played	O
through	O
1982	O
,	O
and	O
secured	O
his	O
first	O
league	O
winners	O
'	O
medal	O
.	O
After	O
the	MISC
1982	MISC
World	MISC
Cup	MISC
,	O
in	O
June	O
,	O
Maradona	PERSON
was	O
transferred	O
to	O
Barcelona	LOCATION
in	O
Spain	LOCATION
for	O
a	O
then	O
world	O
record	O
£	O
5m	O
.	O
In	O
1983	O
,	O
under	O
coach	O
César	PERSON
Luis	PERSON
Menotti	PERSON
,	O
Barcelona	LOCATION
and	O
Maradona	PERSON
won	O
the	MISC
Copa	MISC
del	MISC
Rey	MISC
(	O
Spain	LOCATION
's	O
annual	O
national	O
cup	O
competition	O
)	O
,	O
beating	O
Real	ORGANIZATION
Madrid	ORGANIZATION
,	O
and	O
the	ORGANIZATION
Spanish	ORGANIZATION
Super	ORGANIZATION
Cup	ORGANIZATION
,	O
beating	O
Athletic	ORGANIZATION
de	ORGANIZATION
Bilbao	ORGANIZATION
.	O
However	O
,	O
Maradona	PERSON
had	O
a	O
difficult	O
tenure	O
in	O
Barcelona	LOCATION
.	O
First	O
a	O
bout	O
with	O
hepatitis	O
,	O
then	O
a	O
broken	O
leg	O
caused	O
by	O
an	O
ill-timed	O
tackle	O
by	O
Athletic	ORGANIZATION
's	O
Andoni	PERSON
Goikoetxea	PERSON
jeopardized	O
his	O
career	O
,	O
but	O
Maradona	PERSON
's	O
physical	O
strength	O
and	O
willpower	O
made	O
it	O
possible	O
for	O
him	O
to	O
soon	O
be	O
back	O
on	O
the	O
pitch	O
.	O
At	O
Barcelona	LOCATION
,	O
Maradona	PERSON
got	O
into	O
frequent	O
disputes	O
with	O
the	O
team	O
's	O
directors	O
,	O
especially	O
club	O
president	O
Josep	PERSON
Lluís	PERSON
Núñez	PERSON
,	O
culminating	O
with	O
a	O
demand	O
to	O
be	O
transferred	O
out	O
of	O
Camp	MISC
Nou	MISC
in	O
1984	O
.	O
At	O
Napoli	LOCATION
,	O
Maradona	PERSON
reached	O
the	O
peak	O
of	O
his	O
professional	O
career	O
.	O
Other	O
honors	O
during	O
the	O
Maradona	PERSON
era	O
at	O
Napoli	LOCATION
included	O
the	ORGANIZATION
Coppa	ORGANIZATION
Italia	ORGANIZATION
in	O
1987	O
,	O
(	O
second	O
place	O
in	O
the	ORGANIZATION
Coppa	ORGANIZATION
Italia	ORGANIZATION
in	O
1989	O
)	O
,	O
the	MISC
UEFA	MISC
Cup	MISC
in	O
1989	O
and	O
the	ORGANIZATION
Italian	ORGANIZATION
Supercup	ORGANIZATION
in	O
1990	O
.	O
After	O
serving	O
a	O
15-month	O
ban	O
for	O
failing	O
a	O
drug	O
test	O
for	O
cocaine	O
,	O
Maradona	PERSON
left	O
Napoli	LOCATION
in	O
disgrace	O
in	O
1992	O
.	O
By	O
the	O
time	O
he	O
joined	O
his	O
next	O
team	O
,	O
Sevilla	ORGANIZATION
(	O
1992	O
--	O
93	O
)	O
,	O
he	O
had	O
not	O
played	O
professional	O
football	O
for	O
two	O
years	O
.	O
In	O
1993	O
he	O
played	O
for	O
Newell	ORGANIZATION
's	ORGANIZATION
Old	ORGANIZATION
Boys	ORGANIZATION
and	O
in	O
1995	O
he	O
returned	O
to	O
Boca	ORGANIZATION
Juniors	ORGANIZATION
for	O
2	O
years	O
.	O
Maradona	PERSON
also	O
appeared	O
for	O
Tottenham	PERSON
Hotspur	PERSON
in	O
a	O
friendly	O
match	O
against	O
Internazionale	ORGANIZATION
,	O
shortly	O
before	O
the	O
1986	O
world	O
cup	O
.	O
The	O
match	O
was	O
Osvaldo	PERSON
Ardiles	PERSON
'	PERSON
testimonial	O
,	O
who	O
insisted	O
his	O
friend	O
Maradona	PERSON
played	O
,	O
which	O
Tottenham	PERSON
won	O
2	O
--	O
1	O
.	O
He	O
played	O
alongside	O
Glenn	PERSON
Hoddle	PERSON
,	O
who	O
gave	O
up	O
his	O
number	O
ten	O
shirt	O
for	O
the	O
Argentine	MISC
.	O
Along	O
with	O
his	O
time	O
at	O
Napoli	LOCATION
,	O
international	O
football	O
is	O
where	O
Maradona	PERSON
found	O
his	O
fame	O
.	O
Playing	O
for	O
the	O
Albicelestes	ORGANIZATION
of	O
the	O
Argentina	LOCATION
national	O
football	O
team	O
,	O
he	O
participated	O
in	O
four	O
consecutive	O
FIFA	O
World	MISC
Cup	MISC
tournaments	O
,	O
leading	O
Argentina	LOCATION
to	O
victory	O
in	O
1986	O
and	O
to	O
second	O
place	O
in	O
1990	O
.	O
He	O
made	O
his	O
full	O
international	O
debut	O
at	O
age	O
16	O
,	O
against	O
Hungary	LOCATION
on	O
27	O
February	O
1977	O
.	O
At	O
age	O
18	O
,	O
he	O
played	O
the	MISC
World	MISC
Youth	MISC
Championship	MISC
for	O
Argentina	LOCATION
,	O
and	O
was	O
the	O
star	O
of	O
the	O
tournament	O
,	O
shining	O
in	O
their	O
3	O
--	O
1	O
final	O
win	O
over	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
.	O
On	O
2	O
June	O
1979	O
,	O
Maradona	PERSON
scored	O
his	O
first	O
senior	O
international	O
goal	O
in	O
a	O
3	O
--	O
1	O
win	O
against	O
Scotland	LOCATION
at	O
Hampden	MISC
Park	MISC
.	O
Maradona	PERSON
played	O
his	O
first	O
World	MISC
Cup	MISC
tournament	O
in	O
1982	O
.	O
In	O
the	O
first	O
round	O
,	O
Argentina	LOCATION
,	O
the	O
defending	O
champions	O
,	O
lost	O
to	O
Belgium	LOCATION
.	O
Although	O
the	O
team	O
convincingly	O
beat	O
Hungary	LOCATION
and	O
El	LOCATION
Salvador	LOCATION
to	O
progress	O
to	O
the	O
second	O
round	O
,	O
they	O
were	O
defeated	O
in	O
the	O
second	O
round	O
by	O
Brazil	LOCATION
and	O
by	O
eventual	O
winners	O
Italy	LOCATION
.	O
Maradona	PERSON
played	O
in	O
all	O
five	O
matches	O
without	O
being	O
substituted	O
,	O
scoring	O
twice	O
against	O
Hungary	LOCATION
,	O
but	O
was	O
sent	O
off	O
with	O
5	O
minutes	O
remaining	O
in	O
the	O
game	O
against	O
Brazil	LOCATION
for	O
serious	O
foul	O
play	O
.	O
Throughout	O
the	O
1986	O
World	MISC
Cup	MISC
Maradona	MISC
asserted	O
his	O
dominance	O
and	O
was	O
the	O
most	O
dynamic	O
player	O
of	O
the	O
tournament	O
.	O
He	O
played	O
every	O
minute	O
of	O
every	O
Argentina	LOCATION
game	O
,	O
scored	O
5	O
goals	O
and	O
made	O
5	O
assists	O
.	O
This	O
match	O
was	O
played	O
with	O
the	O
background	O
of	O
the	MISC
Falklands	MISC
War	MISC
between	O
Argentina	LOCATION
and	O
the	LOCATION
United	LOCATION
Kingdom	LOCATION
and	O
emotions	O
were	O
still	O
lingering	O
in	O
the	O
air	O
throughout	O
the	O
entire	O
match	O
.	O
Ultimately	O
,	O
on	O
22	O
August	O
2005	O
Maradona	PERSON
acknowledged	O
on	O
his	O
television	O
show	O
that	O
he	O
had	O
hit	O
the	O
ball	O
with	O
his	O
hand	O
purposely	O
,	O
and	O
that	O
he	O
immediately	O
knew	O
the	O
goal	O
was	O
illegitimate	O
.	O
Maradona	PERSON
's	O
second	O
goal	O
was	O
later	O
voted	O
by	O
FIFA	ORGANIZATION
as	O
the	O
greatest	O
goal	O
in	O
the	O
history	O
of	O
the	MISC
World	MISC
Cup	MISC
.	O
Maradona	PERSON
followed	O
this	O
with	O
two	O
more	O
goals	O
in	O
the	O
semi-final	O
against	O
Belgium	LOCATION
,	O
including	O
another	O
virtuoso	O
dribbling	O
display	O
for	O
the	O
second	O
goal	O
.	O
In	O
the	O
final	O
,	O
the	O
opposing	O
West	MISC
German	MISC
side	O
attempted	O
to	O
contain	O
him	O
by	O
double-marking	O
,	O
but	O
he	O
nevertheless	O
found	O
the	O
space	O
to	O
give	O
the	O
final	O
pass	O
to	O
Jorge	PERSON
Burruchaga	PERSON
for	O
the	O
winning	O
goal	O
.	O
Argentina	LOCATION
beat	O
West	LOCATION
Germany	LOCATION
3	O
--	O
2	O
in	O
front	O
of	O
115,000	O
spectators	O
at	O
the	MISC
Azteca	MISC
Stadium	MISC
and	O
Maradona	PERSON
lifted	O
the	MISC
World	MISC
Cup	MISC
trophy	O
,	O
ensuring	O
that	O
he	O
would	O
be	O
remembered	O
as	O
one	O
of	O
the	O
greatest	O
names	O
in	O
football	O
history	O
.	O
In	O
a	O
tribute	O
to	O
him	O
,	O
the	O
Azteca	MISC
Stadium	MISC
authorities	O
built	O
a	O
statue	O
of	O
him	O
scoring	O
the	O
"	O
goal	O
of	O
the	O
century	O
"	O
and	O
placed	O
it	O
at	O
the	O
entrance	O
of	O
the	O
stadium	O
.	O
Maradona	PERSON
captained	O
Argentina	LOCATION
again	O
in	O
the	O
1990	O
FIFA	O
World	MISC
Cup	MISC
.	O
Argentina	LOCATION
was	O
almost	O
eliminated	O
in	O
the	O
first	O
round	O
,	O
only	O
qualifying	O
in	O
third	O
position	O
from	O
their	O
group	O
.	O
In	O
the	O
round	O
of	O
16	O
match	O
against	O
Brazil	LOCATION
,	O
Claudio	PERSON
Caniggia	PERSON
scored	O
the	O
only	O
goal	O
after	O
being	O
set	O
up	O
by	O
Maradona	PERSON
.	O
In	O
the	O
quarter	O
final	O
,	O
Argentina	LOCATION
faced	O
Yugoslavia	LOCATION
,	O
the	O
match	O
ending	O
0	O
--	O
0	O
after	O
120	O
minutes	O
,	O
and	O
Argentina	LOCATION
advancing	O
on	O
penalty	O
kicks	O
,	O
despite	O
Maradona	PERSON
missing	O
one	O
of	O
the	O
penalties	O
in	O
the	O
shootout	O
with	O
a	O
weak	O
shot	O
at	O
the	O
centre	O
of	O
the	O
goal	O
.	O
At	O
the	O
1994	O
FIFA	O
World	MISC
Cup	MISC
Maradona	MISC
played	O
in	O
only	O
two	O
games	O
,	O
scoring	O
one	O
goal	O
against	O
Greece	LOCATION
,	O
before	O
being	O
sent	O
home	O
after	O
failing	O
a	O
drug	O
test	O
for	O
ephedrine	O
doping	O
.	O
Maradona	PERSON
has	O
also	O
separately	O
claimed	O
that	O
he	O
had	O
an	O
agreement	O
with	O
FIFA	ORGANIZATION
,	O
on	O
which	O
the	O
organization	O
reneged	O
,	O
to	O
allow	O
him	O
to	O
use	O
the	O
drug	O
for	O
weight	O
loss	O
before	O
the	O
competition	O
in	O
order	O
to	O
be	O
able	O
to	O
play	O
.	O
According	O
to	O
Maradona	PERSON
,	O
this	O
was	O
so	O
that	O
the	MISC
World	MISC
Cup	MISC
would	O
not	O
lose	O
prestige	O
because	O
of	O
his	O
absence	O
.	O
Maradona	PERSON
had	O
a	O
compact	O
physique	O
and	O
could	O
withstand	O
physical	O
pressure	O
well	O
.	O
Maradona	PERSON
was	O
a	O
strategist	O
and	O
a	O
team	O
player	O
,	O
as	O
well	O
as	O
highly	O
technical	O
with	O
the	O
ball	O
.	O
One	O
of	O
Maradona	PERSON
's	O
trademark	O
moves	O
was	O
dribbling	O
full-speed	O
on	O
the	O
left	O
wing	O
,	O
and	O
on	O
reaching	O
the	O
opponent	O
's	O
goal	O
line	O
,	O
delivering	O
accurate	O
passes	O
to	O
his	O
teammates	O
.	O
This	O
maneuver	O
led	O
to	O
several	O
assists	O
,	O
such	O
as	O
the	O
powerful	O
cross	O
for	O
Ramón	PERSON
Díaz	PERSON
's	PERSON
header	O
in	O
the	O
1980	O
friendly	O
against	O
Switzerland	LOCATION
.	O
Maradona	PERSON
was	O
dominantly	O
left-footed	O
,	O
often	O
using	O
his	O
left	O
foot	O
even	O
when	O
the	O
ball	O
was	O
positioned	O
more	O
suitably	O
for	O
a	O
right-footed	O
connection	O
.	O
This	O
quote	O
from	O
former	O
teammate	O
Jorge	PERSON
Valdano	PERSON
summarizes	O
the	O
feelings	O
of	O
many	O
:	O
Maradona	PERSON
finished	O
top	O
of	O
the	O
poll	O
with	O
53.6	O
%	O
of	O
the	O
vote	O
.	O
Maradona	PERSON
protested	O
at	O
the	O
change	O
in	O
procedure	O
,	O
and	O
declared	O
he	O
would	O
not	O
attend	O
the	O
ceremony	O
if	O
Pelé	PERSON
replaced	O
him	O
.	O
Maradona	PERSON
accepted	O
his	O
prize	O
,	O
but	O
left	O
the	O
ceremony	O
without	O
waiting	O
to	O
see	O
Pelé	PERSON
receive	O
his	O
accolade	O
.	O
It	O
should	O
be	O
mentioned	O
that	O
Pelé	PERSON
and	O
numerous	O
FIFA	ORGANIZATION
officials	O
criticised	O
the	O
poll	O
for	O
a	O
number	O
of	O
methodological	O
shortcomings	O
,	O
most	O
notably	O
,	O
for	O
the	O
'	O
recency	O
effect	O
'	O
.	O
FIFA	ORGANIZATION
did	O
not	O
grant	O
the	O
request	O
,	O
even	O
though	O
Argentine	MISC
officials	O
have	O
maintained	O
that	O
FIFA	ORGANIZATION
hinted	O
that	O
it	O
would	O
.	O
His	O
contract	O
began	O
1	O
August	O
2005	O
,	O
and	O
one	O
of	O
his	O
first	O
recommendations	O
proved	O
to	O
be	O
very	O
effective	O
:	O
he	O
was	O
the	O
one	O
who	O
decided	O
to	O
hire	O
Alfio	PERSON
Basile	PERSON
as	O
the	O
new	O
coach	O
.	O
His	O
main	O
guest	O
on	O
opening	O
night	O
was	O
Pelé	PERSON
;	O
the	O
two	O
had	O
a	O
friendly	O
chat	O
,	O
showing	O
no	O
signs	O
of	O
past	O
differences	O
.	O
However	O
,	O
the	O
show	O
also	O
included	O
a	O
cartoon	O
villain	O
with	O
a	O
clear	O
physical	O
resemblance	O
to	O
Pelé	PERSON
.	O
On	O
26	O
August	O
2006	O
,	O
it	O
was	O
announced	O
that	O
Maradona	PERSON
was	O
quitting	O
his	O
position	O
in	O
the	O
club	O
Boca	ORGANIZATION
Juniors	ORGANIZATION
because	O
of	O
disagreements	O
with	O
the	O
AFA	ORGANIZATION
,	O
who	O
selected	O
Basile	PERSON
to	O
be	O
the	O
new	O
coach	O
of	O
the	ORGANIZATION
Argentina	ORGANIZATION
National	ORGANIZATION
Football	ORGANIZATION
Team	ORGANIZATION
.	O
The	O
award-winning	O
Serbian	MISC
filmmaker	O
Emir	PERSON
Kusturica	PERSON
made	O
a	O
documentary	O
about	O
Maradona	PERSON
's	O
life	O
,	O
entitled	O
Maradona	PERSON
.	O
He	O
attempted	O
to	O
work	O
as	O
a	O
coach	O
alongside	O
former	O
Argentinos	ORGANIZATION
Juniors	ORGANIZATION
midfield	O
team	O
mate	O
Carlos	PERSON
Fren	PERSON
.	O
The	O
pair	O
led	O
Mandiyú	ORGANIZATION
of	O
Corrientes	LOCATION
(	O
1994	O
)	O
and	O
Racing	ORGANIZATION
Club	ORGANIZATION
(	O
1995	O
)	O
,	O
but	O
with	O
little	O
success	O
.	O
After	O
the	O
resignation	O
of	O
Argentina	LOCATION
national	O
football	O
team	O
coach	O
Alfio	PERSON
Basile	PERSON
in	O
2008	O
,	O
Diego	PERSON
Maradona	PERSON
immediately	O
proposed	O
his	O
candidacy	O
for	O
the	O
vacant	O
role	O
.	O
On	O
October	O
29	O
,	O
2008	O
,	O
AFA	ORGANIZATION
chairman	O
Julio	PERSON
Grondona	PERSON
confirmed	O
that	O
Maradona	PERSON
would	O
be	O
the	O
head	O
coach	O
of	O
the	O
national	O
side	O
from	O
December	O
2008	O
.	O
After	O
winning	O
his	O
first	O
three	O
matches	O
in	O
charge	O
of	O
the	O
national	O
team	O
,	O
he	O
oversaw	O
a	O
6	O
--	O
1	O
defeat	O
to	O
Bolivia	LOCATION
,	O
equalling	O
the	O
team	O
's	O
worst	O
ever	O
margin	O
of	O
defeat	O
.	O
After	O
Argentina	LOCATION
's	O
qualification	O
,	O
Maradona	PERSON
used	O
abusive	O
language	O
at	O
the	O
live	O
post-game	O
press	O
conference	O
,	O
telling	O
members	O
of	O
the	O
media	O
to	O
"	O
suck	O
it	O
and	O
keep	O
on	O
sucking	O
it	O
"	O
.	O
FIFA	ORGANIZATION
responded	O
with	O
a	O
two	O
month	O
ban	O
on	O
all	O
footballing	O
activity	O
,	O
which	O
expired	O
on	O
January	O
15	O
,	O
2010	O
,	O
and	O
a	O
CHF	O
25,000	O
fine	O
,	O
with	O
a	O
warning	O
as	O
to	O
his	O
future	O
conduct	O
.	O
Argentina	LOCATION
had	O
one	O
friendly	O
match	O
scheduled	O
during	O
the	O
period	O
of	O
the	O
ban	O
,	O
at	O
home	O
to	O
the	LOCATION
Czech	LOCATION
Republic	LOCATION
on	O
December	O
15	O
,	O
but	O
this	O
was	O
subsequently	O
cancelled	O
.	O
At	O
the	MISC
World	MISC
Cup	MISC
finals	O
in	O
June	O
2010	O
,	O
Argentina	LOCATION
started	O
by	O
winning	O
1	O
--	O
0	O
against	O
Nigeria	LOCATION
,	O
and	O
then	O
defeated	O
South	LOCATION
Korea	LOCATION
by	O
4	O
--	O
1	O
,	O
with	O
a	O
hat-trick	O
from	O
Gonzalo	PERSON
Higuain	PERSON
.	O
.	O
In	O
the	O
final	O
match	O
of	O
the	O
group	O
stage	O
Argentina	LOCATION
won	O
2	O
--	O
0	O
against	O
Greece	LOCATION
to	O
win	O
their	O
the	O
group	O
and	O
advance	O
to	O
a	O
second	O
round	O
meeting	O
with	O
Mexico	LOCATION
.	O
Argentina	LOCATION
was	O
ranked	O
5th	O
in	O
the	O
tournament	O
.	O
On	O
15	O
July	O
2010	O
,	O
the	ORGANIZATION
Argentine	ORGANIZATION
Football	ORGANIZATION
Association	ORGANIZATION
said	O
that	O
he	O
would	O
be	O
offered	O
a	O
new	O
4	O
year	O
deal	O
that	O
would	O
keep	O
him	O
in	O
charge	O
through	O
to	O
the	O
summer	O
of	O
2014	O
when	O
Brazil	LOCATION
stages	O
the	MISC
World	MISC
Cup	MISC
,	O
however	O
on	O
27th	O
July	O
the	O
AFA	ORGANIZATION
announced	O
that	O
its	O
board	O
had	O
unanimously	O
decided	O
not	O
to	O
renew	O
his	O
contract	O
.	O
Afterwards	O
on	O
29	O
July	O
2010	O
,	O
Maradona	PERSON
claimed	O
that	O
AFA	ORGANIZATION
president	O
Julio	PERSON
Grondona	PERSON
and	O
director	O
of	O
national	O
teams	O
Carlos	PERSON
Bilardo	PERSON
had	O
"	O
lied	O
to	O
"	O
and	O
"	O
betrayed	O
"	O
and	O
effectively	O
sacked	O
him	O
from	O
the	O
role	O
.	O
They	O
traveled	O
together	O
to	O
Napoli	LOCATION
for	O
a	O
series	O
of	O
homages	O
in	O
June	O
2005	O
and	O
were	O
seen	O
together	O
on	O
many	O
other	O
occasions	O
,	O
including	O
the	O
Argentina	LOCATION
matches	O
during	O
2006	MISC
FIFA	MISC
World	MISC
Cup	MISC
.	O
During	O
the	O
divorce	O
proceedings	O
,	O
Maradona	PERSON
admitted	O
he	O
was	O
the	O
father	O
of	O
Diego	PERSON
Sinagra	PERSON
(	O
born	O
in	O
Naples	LOCATION
on	O
September	O
20	O
,	O
1986	O
)	O
.	O
From	O
the	O
mid-1980s	O
until	O
2004	O
Diego	PERSON
Maradona	PERSON
was	O
addicted	O
to	O
cocaine	O
.	O
He	O
allegedly	O
began	O
using	O
the	O
drug	O
in	O
Barcelona	LOCATION
in	O
1983	O
.	O
By	O
the	O
time	O
he	O
was	O
playing	O
for	O
Napoli	PERSON
he	O
had	O
a	O
regular	O
addiction	O
,	O
which	O
began	O
to	O
interfere	O
with	O
his	O
ability	O
to	O
play	O
football	O
.	O
On	O
January	O
4	O
,	O
2000	O
,	O
while	O
vacationing	O
in	O
Punta	LOCATION
del	LOCATION
Este	LOCATION
,	O
Uruguay	LOCATION
,	O
Maradona	PERSON
had	O
to	O
be	O
rushed	O
to	O
the	O
emergency	O
room	O
of	O
a	O
local	O
clinic	O
.	O
It	O
was	O
later	O
known	O
that	O
traces	O
of	O
cocaine	O
were	O
found	O
in	O
his	O
blood	O
and	O
Maradona	PERSON
had	O
to	O
explain	O
the	O
circumstances	O
to	O
the	O
police	O
.	O
After	O
this	O
he	O
left	O
Argentina	LOCATION
and	O
went	O
to	O
Cuba	LOCATION
in	O
order	O
to	O
follow	O
a	O
drug	O
rehab	O
plan	O
.	O
Maradona	PERSON
had	O
a	O
tendency	O
to	O
put	O
on	O
weight	O
,	O
and	O
suffered	O
increasingly	O
from	O
obesity	O
from	O
the	O
end	O
of	O
his	O
playing	O
career	O
until	O
undergoing	O
gastric	O
bypass	O
surgery	O
in	O
a	O
clinic	O
in	O
Cartagena	ORGANIZATION
de	ORGANIZATION
Indias	ORGANIZATION
,	O
Colombia	LOCATION
on	O
6	O
March	O
2005	O
.	O
When	O
Maradona	PERSON
resumed	O
public	O
appearances	O
shortly	O
thereafter	O
,	O
he	O
displayed	O
a	O
notably	O
thinner	O
figure	O
.	O
On	O
18	O
April	O
2004	O
,	O
doctors	O
reported	O
that	O
Maradona	PERSON
had	O
suffered	O
a	O
major	O
myocardial	O
infarction	O
following	O
a	O
cocaine	O
overdose	O
;	O
he	O
was	O
admitted	O
to	O
intensive	O
care	O
in	O
a	O
Buenos	LOCATION
Aires	LOCATION
hospital	O
.	O
He	O
tried	O
to	O
return	O
to	O
Cuba	LOCATION
,	O
where	O
he	O
had	O
spent	O
most	O
of	O
his	O
time	O
in	O
the	O
years	O
leading	O
up	O
to	O
the	O
heart	O
attack	O
,	O
but	O
his	O
family	O
opposed	O
,	O
having	O
filed	O
a	O
judicial	O
petition	O
to	O
exercise	O
his	O
legal	O
guardianship	O
.	O
On	O
29	O
March	O
2007	O
,	O
Maradona	PERSON
was	O
readmitted	O
to	O
a	O
hospital	O
in	O
Buenos	LOCATION
Aires	LOCATION
.	O
On	O
8	O
May	O
2007	O
,	O
Maradona	PERSON
appeared	O
on	O
Argentine	MISC
television	O
and	O
stated	O
that	O
he	O
had	O
quit	O
drinking	O
and	O
had	O
not	O
used	O
drugs	O
in	O
two	O
and	O
a	O
half	O
years	O
.	O
In	O
recent	O
years	O
,	O
Maradona	PERSON
has	O
shown	O
sympathy	O
to	O
left-wing	O
ideologies	O
.	O
After	O
this	O
meeting	O
Maradona	PERSON
claimed	O
that	O
he	O
had	O
come	O
with	O
the	O
aim	O
of	O
meeting	O
a	O
"	O
great	O
man	O
"	O
but	O
he	O
had	O
met	O
instead	O
a	O
gigantic	O
man	O
.	O
He	O
has	O
declared	O
his	O
opposition	O
to	O
what	O
he	O
identifies	O
as	O
imperialism	O
,	O
notably	O
during	O
the	MISC
2005	MISC
Summit	MISC
of	MISC
the	MISC
Americas	MISC
in	O
Mar	LOCATION
del	LOCATION
Plata	LOCATION
,	O
Argentina	LOCATION
.	O
They	O
reported	O
that	O
thus	O
far	O
,	O
Maradona	PERSON
has	O
paid	O
only	O
42,000	O
euros	O
,	O
two	O
luxury	O
watches	O
and	O
a	O
set	O
of	O
earrings	O
.	O
In	O
Argentina	LOCATION
,	O
Maradona	PERSON
is	O
often	O
talked	O
about	O
in	O
terms	O
reserved	O
for	O
legends	O
.	O
In	O
the	O
Argentine	MISC
film	O
El	PERSON
Hijo	PERSON
de	PERSON
la	PERSON
Novia	PERSON
(	O
"	O
Son	MISC
of	MISC
the	MISC
Bride	MISC
"	O
)	O
,	O
somebody	O
who	O
impersonates	O
a	O
Catholic	MISC
priest	O
says	O
to	O
a	O
bar	O
patron	O
:	O
"	O
they	O
idolized	O
him	O
and	O
then	O
crucified	O
him	O
"	O
.	O
When	O
a	O
friend	O
scolds	O
him	O
for	O
taking	O
the	O
prank	O
too	O
far	O
,	O
the	O
fake	O
priest	O
retorts	O
:	O
"	O
But	O
I	O
was	O
talking	O
about	O
Maradona	PERSON
"	O
.	O
He	O
's	O
the	O
subject	O
of	O
the	O
film	O
El	MISC
Camino	MISC
de	MISC
San	MISC
Diego	MISC
,	O
though	O
he	O
himself	O
only	O
appears	O
in	O
archive	O
footage	O
.	O
Maradona	PERSON
was	O
included	O
in	O
many	O
cameos	O
in	O
the	O
Argentine	MISC
comic	O
book	O
El	MISC
Cazador	MISC
de	MISC
Aventuras	MISC
.	O
In	O
Rosario	LOCATION
,	O
Argentina	LOCATION
,	O
fans	O
organized	O
the	ORGANIZATION
"	ORGANIZATION
Church	ORGANIZATION
of	ORGANIZATION
Maradona	ORGANIZATION
"	O
.	O
This	O
generated	O
some	O
controversy	O
in	O
the	O
Argentine	MISC
media	O
after	O
its	O
release	O
(	O
although	O
the	O
commercial	O
was	O
not	O
supposed	O
to	O
air	O
on	O
the	O
Argentine	MISC
market	O
,	O
fans	O
could	O
see	O
it	O
via	O
internet	O
)	O
.	O
