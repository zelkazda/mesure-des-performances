The	O
RGC	ORGANIZATION
is	O
a	O
member	O
of	O
most	O
major	O
international	O
organizations	O
,	O
including	O
the	ORGANIZATION
United	ORGANIZATION
Nations	ORGANIZATION
and	O
its	O
specialized	O
agencies	O
such	O
as	O
the	ORGANIZATION
World	ORGANIZATION
Bank	ORGANIZATION
and	ORGANIZATION
International	ORGANIZATION
Monetary	ORGANIZATION
Fund	ORGANIZATION
.	O
The	O
RGC	ORGANIZATION
is	O
an	ORGANIZATION
Asian	ORGANIZATION
Development	ORGANIZATION
Bank	ORGANIZATION
(	O
ADB	ORGANIZATION
)	O
member	O
,	O
a	O
member	O
of	O
ASEAN	ORGANIZATION
,	O
also	O
a	O
member	O
of	O
the	O
WTO	ORGANIZATION
.	O
In	O
2005	O
Cambodia	LOCATION
attended	O
the	O
inaugural	O
East	LOCATION
Asia	LOCATION
Summit	LOCATION
.	O
Japan	LOCATION
has	O
an	O
embassy	O
in	O
Phnom	LOCATION
Penh	LOCATION
.	O
Russia	LOCATION
has	O
an	O
embassy	O
in	O
Phnom	LOCATION
Penh	LOCATION
.	O
Both	O
countries	O
are	O
full	O
members	O
of	O
the	LOCATION
East	LOCATION
Asia	LOCATION
Summit	LOCATION
.	O
Both	O
countries	O
established	O
diplomatic	O
relations	O
on	O
1970	O
whereas	O
it	O
was	O
ceased	O
during	O
Khmer	ORGANIZATION
Rouge	ORGANIZATION
.	O
In	O
the	O
past	O
two	O
years	O
,	O
bilateral	O
relations	O
between	O
the	O
U.S.	LOCATION
and	O
Cambodia	LOCATION
have	O
strengthened	O
.	O
Bilateral	O
relations	O
between	O
the	LOCATION
Kingdom	LOCATION
of	LOCATION
Cambodia	LOCATION
and	O
the	LOCATION
Socialist	LOCATION
Republic	LOCATION
of	LOCATION
Vietnam	LOCATION
were	O
for	O
long	O
strained	O
due	O
to	O
the	O
Cambodian-Vietnamese	MISC
War	O
(	O
1976	O
--	O
1990	O
)	O
.	O
The	O
maritime	O
boundary	O
with	O
Vietnam	LOCATION
is	O
hampered	O
by	O
unresolved	O
dispute	O
over	O
sovereignty	O
of	O
offshore	O
islands	O
.	O
