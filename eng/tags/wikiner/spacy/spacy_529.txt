Although	O
he	O
saved	O
his	O
life	O
by	O
turning	O
informer	O
,	O
he	O
was	O
condemned	O
to	O
partial	O
loss	O
of	O
civil	O
rights	O
and	O
forced	O
to	O
leave	O
Athens	LOCATION
.	O
He	O
engaged	O
in	O
commercial	O
pursuits	O
,	O
and	O
returned	O
to	O
Athens	LOCATION
under	O
the	O
general	O
amnesty	O
that	O
followed	O
the	O
restoration	O
of	O
the	O
democracy	O
(	O
403	O
BC	O
)	O
,	O
and	O
filled	O
some	O
important	O
offices	O
.	O
In	O
391	O
BC	O
he	O
was	O
one	O
of	O
the	O
ambassadors	O
sent	O
to	O
Sparta	LOCATION
to	O
discuss	O
peace	O
terms	O
,	O
but	O
the	O
negotiations	O
failed	O
.	O
Andocides	ORGANIZATION
was	O
no	O
professional	O
orator	O
;	O
his	O
style	O
is	O
simple	O
and	O
lively	O
,	O
natural	O
but	O
inartistic	MISC
.	O
