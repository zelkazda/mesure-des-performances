In	O
1993	O
,	O
it	O
established	O
formal	O
diplomatic	O
relations	O
with	O
the	LOCATION
United	LOCATION
States	LOCATION
.	O
It	O
also	O
has	O
intervened	O
in	O
the	LOCATION
Republic	LOCATION
of	LOCATION
the	LOCATION
Congo	LOCATION
to	O
support	O
the	O
existing	O
government	O
in	O
that	O
country	O
.	O
More	O
recently	O
,	O
it	O
has	O
extended	O
those	O
efforts	O
to	O
controls	O
on	O
conflict	O
diamonds	O
,	O
the	O
primary	O
source	O
of	O
revenue	O
for	O
UNITA	ORGANIZATION
.	O
Angola	LOCATION
sent	O
a	O
delegation	O
to	O
DR	O
Congo	LOCATION
's	O
capital	O
Kinshasa	LOCATION
and	O
succeeded	O
in	O
stopping	O
government-forced	O
expulsions	O
which	O
had	O
become	O
a	O
"	O
tit-for-tat	O
"	O
immigration	O
dispute	O
.	O
In	O
1999	O
Namibia	LOCATION
signed	O
a	O
mutual	O
defense	O
pact	O
with	O
its	O
northern	O
neighbor	O
Angola	LOCATION
.	O
Angolan-Nigerian	LOCATION
relations	O
are	O
primarily	O
based	O
on	O
their	O
roles	O
as	O
oil	O
exporting	O
nations	O
.	O
Both	O
are	O
members	O
of	O
the	ORGANIZATION
Organization	ORGANIZATION
of	ORGANIZATION
the	ORGANIZATION
Petroleum	ORGANIZATION
Exporting	ORGANIZATION
Countries	ORGANIZATION
,	O
the	ORGANIZATION
African	ORGANIZATION
Union	ORGANIZATION
and	O
other	O
multilateral	O
organizations	O
.	O
Russia	LOCATION
has	O
an	O
embassy	O
in	O
Luanda	LOCATION
.	O
Parts	O
of	O
both	O
countries	O
were	O
part	O
of	O
the	LOCATION
Portuguese	LOCATION
Empire	LOCATION
from	O
the	O
early	O
16th	O
century	O
until	O
Brazil	LOCATION
's	O
independence	O
in	O
1822	O
.	O
From	O
the	O
mid-1980s	O
through	O
at	O
least	O
1992	O
,	O
the	LOCATION
United	LOCATION
States	LOCATION
was	O
the	O
primary	O
source	O
of	O
military	O
and	O
other	O
support	O
for	O
the	O
UNITA	ORGANIZATION
rebel	O
movement	O
,	O
which	O
was	O
led	O
from	O
its	O
creation	O
through	O
2002	O
by	O
Jonas	PERSON
Savimbi	PERSON
.	O
This	O
article	O
incorporates	O
public	O
domain	O
material	O
from	O
websites	O
or	O
documents	O
of	O
the	ORGANIZATION
CIA	ORGANIZATION
World	ORGANIZATION
Factbook	ORGANIZATION
.	O
