Italy	LOCATION
has	O
the	O
fifth-highest	O
population	O
density	O
in	O
Europe	LOCATION
--	O
about	O
196	O
persons	O
per	O
square	O
kilometer	O
(	O
490	O
per	O
square	O
mile	O
)	O
.	O
The	O
nation	O
has	O
a	O
relatively	O
low	O
fertility	O
rate	O
,	O
of	O
1.41	O
children	O
per	O
family	O
,	O
while	O
having	O
the	O
world	O
's	O
19th	O
highest	O
life	O
expectancy	O
,	O
coming	O
after	O
New	LOCATION
Zealand	LOCATION
and	O
Bermuda	LOCATION
,	O
and	O
beating	O
Gibraltar	LOCATION
and	O
Monaco	LOCATION
.	O
According	O
to	O
the	O
OECD	ORGANIZATION
,	O
the	O
largest	O
metropolitan	O
areas	O
are	O
:	O
Roman	ORGANIZATION
Catholicism	ORGANIZATION
is	O
by	O
far	O
the	O
largest	O
religion	O
in	O
the	O
country	O
,	O
although	O
the	ORGANIZATION
Catholic	ORGANIZATION
Church	ORGANIZATION
is	O
no	O
longer	O
officially	O
the	O
state	O
religion	O
.	O
The	O
country	O
's	O
oldest	O
religious	O
minority	O
is	O
the	O
Jewish	MISC
community	O
,	O
comprising	O
roughly	O
45,000	O
people	O
(	O
0.06	O
%	O
)	O
.	O
The	O
following	O
demographic	O
statistics	O
are	O
from	O
the	ORGANIZATION
CIA	ORGANIZATION
World	ORGANIZATION
Factbook	ORGANIZATION
,	O
unless	O
otherwise	O
indicated	O
.	O
