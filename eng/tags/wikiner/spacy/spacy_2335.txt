The	O
Comoros	MISC
archipelago	O
consists	O
of	O
four	O
main	O
islands	O
aligned	O
along	O
a	O
northwest-southeast	O
axis	O
at	O
the	O
north	O
end	O
of	O
the	ORGANIZATION
Mozambique	ORGANIZATION
Channel	ORGANIZATION
,	O
between	O
Mozambique	LOCATION
and	O
the	O
island	O
of	O
Madagascar	LOCATION
.	O
They	O
are	O
Njazidja	LOCATION
(	O
Grande	O
Comore	O
)	O
,	O
Mwali	MISC
(	O
Mohéli	PERSON
)	O
,	O
Nzwani	PERSON
(	O
Anjouan	PERSON
)	O
,	O
and	O
Mahoré	PERSON
(	O
Mayotte	PERSON
)	O
.	O
The	O
islands	O
'	O
distance	O
from	O
each	O
other	O
--	O
Njazidja	ORGANIZATION
is	O
some	O
200	O
kilometers	O
from	O
Mahoré	MISC
,	O
forty	O
kilometers	O
from	O
Mwali	MISC
,	O
and	O
eighty	O
kilometers	O
from	O
Nzwani	PERSON
--	O
along	O
with	O
a	O
lack	O
of	O
good	O
harbor	O
facilities	O
,	O
make	O
transportation	O
and	O
communication	O
difficult	O
.	O
The	O
islands	O
have	O
a	O
total	O
land	O
area	O
of	O
2,236	O
square	O
kilometers	O
(	O
including	O
Mahoré	MISC
)	O
,	O
and	O
claim	O
territorial	O
waters	O
of	O
320	O
square	O
kilometers	O
.	O
Le	LOCATION
Karthala	LOCATION
(	O
2316	O
m	O
)	O
on	O
Grande	ORGANIZATION
Comore	ORGANIZATION
is	O
an	O
active	O
volcano	O
.	O
Njazidja	ORGANIZATION
is	O
the	O
largest	O
island	O
,	O
sixty-seven	O
kilometers	O
long	O
and	O
twenty-seven	O
kilometers	O
wide	O
,	O
with	O
a	O
total	O
area	O
of	O
1,146	O
square	O
kilometers	O
.	O
Because	O
Njazidja	ORGANIZATION
is	O
geologically	O
a	O
relatively	O
new	O
island	O
,	O
its	O
soil	O
is	O
thin	O
and	O
rocky	O
and	O
can	O
not	O
hold	O
water	O
.	O
Nzwani	PERSON
,	O
triangular	O
shaped	O
and	O
forty	O
kilometers	O
from	O
apex	O
to	O
base	O
,	O
has	O
an	O
area	O
of	O
424	O
square	O
kilometers	O
.	O
Older	O
than	O
Njazidja	LOCATION
,	O
Nzwani	PERSON
has	O
deeper	O
soil	O
cover	O
,	O
but	O
overcultivation	O
has	O
caused	O
serious	O
erosion	O
.	O
Mwali	ORGANIZATION
is	O
thirty	O
kilometers	O
long	O
and	O
twelve	O
kilometers	O
wide	O
,	O
with	O
an	O
area	O
of	O
290	O
square	O
kilometers	O
.	O
Like	O
Njazidja	ORGANIZATION
,	O
it	O
retains	O
stands	O
of	O
rain	O
forest	O
.	O
Mahoré	MISC
,	O
geologically	O
the	O
oldest	O
of	O
the	O
four	O
islands	O
,	O
is	O
thirty-nine	O
kilometers	O
long	O
and	O
twenty-two	O
kilometers	O
wide	O
,	O
totaling	O
375	O
square	O
kilometers	O
,	O
and	O
its	O
highest	O
points	O
are	O
between	O
500	O
and	O
600	O
meters	O
above	O
sea	O
level	O
.	O
A	O
live	O
specimen	O
was	O
caught	O
in	O
1938	O
off	O
southern	O
Africa	LOCATION
;	O
other	O
coelacanths	O
have	O
since	O
been	O
found	O
in	O
the	O
vicinity	O
of	O
the	LOCATION
Comoro	LOCATION
Islands	LOCATION
.	O
22	O
species	O
of	O
bird	O
are	O
unique	O
to	O
the	O
archipelago	O
and	O
17	O
of	O
these	O
are	O
restricted	O
to	O
the	ORGANIZATION
Union	ORGANIZATION
of	ORGANIZATION
the	ORGANIZATION
Comoros	ORGANIZATION
.	O
Mwali	MISC
and	O
Mahoré	MISC
possess	O
streams	O
and	O
other	O
natural	O
sources	O
of	O
water	O
,	O
but	O
Njazidja	ORGANIZATION
and	O
Nzwani	PERSON
,	O
whose	O
mountainous	O
landscapes	O
retain	O
water	O
poorly	O
,	O
are	O
almost	O
devoid	O
of	O
naturally	O
occurring	O
running	O
water	O
.	O
Natural	O
hazards	O
:	O
cyclones	O
possible	O
during	O
rainy	O
season	O
(	O
December	O
to	O
April	O
)	O
;	O
Le	LOCATION
Kartala	LOCATION
on	O
Grande	LOCATION
Comore	LOCATION
is	O
an	O
active	O
volcano	O
