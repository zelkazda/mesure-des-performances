Most	O
common	O
law	O
jurisdictions	O
(	O
except	O
for	O
much	O
of	O
the	LOCATION
United	LOCATION
States	LOCATION
)	O
have	O
abolished	O
grand	O
juries	O
.	O
In	O
Australia	LOCATION
,	O
an	O
indictment	O
is	O
issued	O
by	O
a	O
government	O
official	O
(	O
the	O
Attorney-General	O
,	O
the	ORGANIZATION
Director	ORGANIZATION
of	ORGANIZATION
Public	ORGANIZATION
Prosecutions	ORGANIZATION
,	O
or	O
one	O
of	O
their	O
subordinates	O
)	O
.	O
All	O
proceedings	O
on	O
indictment	O
must	O
be	O
brought	O
before	O
the	ORGANIZATION
Crown	ORGANIZATION
Court	ORGANIZATION
.	O
In	O
many	O
(	O
though	O
not	O
all	O
)	O
U.S.	LOCATION
jurisdictions	O
retaining	O
the	O
grand	O
jury	O
,	O
prosecutors	O
often	O
have	O
a	O
choice	O
between	O
seeking	O
an	O
indictment	O
from	O
a	O
grand	O
jury	O
,	O
or	O
filing	O
a	O
charging	O
document	O
directly	O
with	O
the	O
court	O
.	O
Notwithstanding	O
the	O
existence	O
of	O
the	O
right	O
to	O
jury	O
trial	O
,	O
the	O
vast	O
majority	O
of	O
criminal	O
cases	O
in	O
the	O
U.S.	LOCATION
are	O
resolved	O
by	O
the	O
plea	O
bargaining	O
process	O
.	O
