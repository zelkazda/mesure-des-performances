However	O
,	O
as	O
the	O
Morris	PERSON
worm	O
and	O
Mydoom	MISC
showed	O
,	O
the	O
network	O
traffic	O
and	O
other	O
unintended	O
effects	O
can	O
often	O
cause	O
major	O
disruption	O
.	O
A	O
"	O
payload	O
"	O
is	O
code	O
designed	O
to	O
do	O
more	O
than	O
spread	O
the	O
worm	O
--	O
it	O
might	O
delete	O
files	O
on	O
a	O
host	O
system	O
(	O
e.g.	O
,	O
the	O
ExploreZip	MISC
worm	O
)	O
,	O
encrypt	O
files	O
in	O
a	O
cryptoviral	O
extortion	O
attack	O
,	O
or	O
send	O
documents	O
via	O
e-mail	O
.	O
Beginning	O
with	O
the	O
very	O
first	O
research	O
into	O
worms	O
at	O
Xerox	ORGANIZATION
PARC	ORGANIZATION
,	O
there	O
have	O
been	O
attempts	O
to	O
create	O
useful	O
worms	O
.	O
However	O
,	O
as	O
with	O
the	O
ILOVEYOU	O
worm	O
,	O
and	O
with	O
the	O
increased	O
growth	O
and	O
efficiency	O
of	O
phishing	O
attacks	O
,	O
it	O
remains	O
possible	O
to	O
trick	O
the	O
end-user	O
into	O
running	O
a	O
malicious	O
code	O
.	O
The	O
actual	O
term	O
'	O
worm	O
'	O
was	O
first	O
used	O
in	O
John	PERSON
Brunner	PERSON
's	PERSON
1975	O
novel	O
,	O
The	MISC
Shockwave	MISC
Rider	MISC
.	O
