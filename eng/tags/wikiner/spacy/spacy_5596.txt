Henry	PERSON
Alfred	PERSON
Kissinger	PERSON
(	O
pronounced	O
/ˈkɪsɪndʒər/	O
;	O
born	O
May	O
27	O
,	O
1923	O
)	O
is	O
a	O
German-born	MISC
American	MISC
political	O
scientist	O
,	O
diplomat	O
,	O
and	O
recipient	O
of	O
the	MISC
Nobel	MISC
Peace	MISC
Prize	MISC
.	O
During	O
this	O
period	O
,	O
he	O
pioneered	O
the	O
policy	O
of	O
détente	PERSON
with	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
,	O
orchestrated	O
the	O
opening	O
of	O
relations	O
with	O
China	LOCATION
,	O
and	O
negotiated	O
the	LAW
Paris	LAW
Peace	LAW
Accords	LAW
,	O
ending	O
American	MISC
involvement	O
in	O
the	MISC
Vietnam	MISC
War	MISC
.	O
His	O
role	O
in	O
the	O
bombing	O
of	O
Cambodia	LOCATION
and	O
other	O
American	MISC
interventions	O
abroad	O
during	O
this	O
period	O
remains	O
controversial	O
.	O
Kissinger	PERSON
is	O
still	O
a	O
controversial	O
figure	O
today	O
.	O
Kissinger	PERSON
was	O
born	O
Heinz	PERSON
Alfred	PERSON
Kissinger	PERSON
in	O
Fürth	LOCATION
,	O
Bavaria	LOCATION
,	O
to	O
a	O
family	O
of	O
German	MISC
Jews	MISC
.	O
Kissinger	PERSON
spent	O
his	O
high	O
school	O
years	O
in	O
the	O
Washington	LOCATION
Heights	LOCATION
section	O
of	O
upper	O
Manhattan	LOCATION
as	O
part	O
of	O
the	O
German	MISC
Jewish	MISC
immigrant	O
community	O
there	O
.	O
Although	O
Kissinger	PERSON
assimilated	O
quickly	O
into	O
American	MISC
culture	O
,	O
he	O
never	O
lost	O
his	O
pronounced	O
Frankish	MISC
accent	O
,	O
due	O
to	O
childhood	O
shyness	O
that	O
made	O
him	O
hesitant	O
to	O
speak	O
.	O
Following	O
his	O
first	O
year	O
at	O
George	ORGANIZATION
Washington	ORGANIZATION
High	ORGANIZATION
School	ORGANIZATION
,	O
he	O
began	O
attending	O
school	O
at	O
night	O
and	O
worked	O
in	O
a	O
shave	O
brush	O
factory	O
during	O
the	O
day	O
.	O
Following	O
high	O
school	O
,	O
Kissinger	PERSON
enrolled	O
in	O
the	ORGANIZATION
City	ORGANIZATION
College	ORGANIZATION
of	ORGANIZATION
New	ORGANIZATION
York	ORGANIZATION
,	O
studying	O
accounting	O
.	O
His	O
studies	O
were	O
interrupted	O
in	O
early	O
1943	O
,	O
when	O
he	O
was	O
drafted	O
into	O
the	ORGANIZATION
U.S.	ORGANIZATION
Army	ORGANIZATION
.	O
Ten	O
years	O
later	O
,	O
he	O
married	O
Nancy	PERSON
Maginnes	PERSON
.	O
David	PERSON
was	O
an	O
executive	O
with	O
NBC	ORGANIZATION
Universal	ORGANIZATION
before	O
being	O
tapped	O
as	O
the	O
head	O
of	O
Conaco	LOCATION
,	O
Conan	PERSON
O'Brien	PERSON
's	PERSON
production	O
company	O
.	O
The	O
Army	ORGANIZATION
sent	O
him	O
to	O
study	O
engineering	O
at	O
Lafayette	ORGANIZATION
College	ORGANIZATION
,	O
Pa.	LOCATION
,	O
but	O
the	O
program	O
was	O
canceled	O
,	O
and	O
Kissinger	PERSON
was	O
reassigned	O
to	O
the	ORGANIZATION
84th	ORGANIZATION
Infantry	ORGANIZATION
Division	ORGANIZATION
.	O
Kissinger	PERSON
saw	O
combat	O
with	O
the	O
division	O
,	O
and	O
volunteered	O
for	O
hazardous	O
intelligence	O
duties	O
during	O
the	MISC
Battle	MISC
of	MISC
the	MISC
Bulge	MISC
.	MISC
In	O
June	O
1945	O
,	O
Kissinger	PERSON
was	O
made	O
commandant	O
of	O
a	O
CIC	ORGANIZATION
detachment	O
in	O
the	O
Bergstraße	LOCATION
district	O
of	O
Hesse	LOCATION
,	O
with	O
responsibility	O
for	O
de-Nazification	O
of	O
the	O
district	O
.	O
Although	O
he	O
possessed	O
absolute	O
authority	O
and	O
powers	O
of	O
arrest	O
,	O
Kissinger	PERSON
took	O
care	O
to	O
avoid	O
abuses	O
against	O
the	O
local	O
population	O
by	O
his	O
command	O
.	O
Henry	PERSON
Kissinger	PERSON
received	O
his	O
B.A.	ORGANIZATION
degree	ORGANIZATION
summa	ORGANIZATION
cum	ORGANIZATION
laude	ORGANIZATION
at	O
Harvard	ORGANIZATION
College	ORGANIZATION
in	O
1950	O
,	O
where	O
he	O
studied	O
under	O
William	PERSON
Yandell	PERSON
Elliott	PERSON
.	O
In	O
1952	O
,	O
while	O
still	O
at	O
Harvard	ORGANIZATION
,	O
he	O
served	O
as	O
a	O
consultant	O
to	O
the	O
Director	O
of	O
the	ORGANIZATION
Psychological	ORGANIZATION
Strategy	ORGANIZATION
Board	ORGANIZATION
.	O
Outside	O
of	O
academia	O
,	O
he	O
served	O
as	O
a	O
consultant	O
to	O
several	O
government	O
agencies	O
,	O
including	O
the	ORGANIZATION
Operations	ORGANIZATION
Research	ORGANIZATION
Office	ORGANIZATION
,	O
the	ORGANIZATION
Arms	ORGANIZATION
Control	ORGANIZATION
and	ORGANIZATION
Disarmament	ORGANIZATION
Agency	ORGANIZATION
,	O
and	O
the	ORGANIZATION
Department	ORGANIZATION
of	ORGANIZATION
State	ORGANIZATION
,	O
and	O
the	ORGANIZATION
Rand	ORGANIZATION
Corporation	ORGANIZATION
,	O
a	O
think-tank	O
.	O
After	O
Richard	PERSON
Nixon	PERSON
won	O
the	O
presidency	O
in	O
1968	O
,	O
he	O
made	O
Kissinger	ORGANIZATION
National	ORGANIZATION
Security	ORGANIZATION
Advisor	ORGANIZATION
.	O
Kissinger	PERSON
served	O
as	O
National	ORGANIZATION
Security	ORGANIZATION
Advisor	ORGANIZATION
and	O
Secretary	O
of	O
State	ORGANIZATION
under	O
President	O
Richard	PERSON
Nixon	PERSON
,	O
and	O
continued	O
as	O
Secretary	O
of	O
State	ORGANIZATION
under	O
Nixon	PERSON
's	O
successor	O
Gerald	PERSON
Ford	PERSON
.	O
He	O
was	O
awarded	O
the	O
1973	O
Nobel	MISC
Peace	MISC
Prize	MISC
for	O
helping	O
to	O
establish	O
a	O
ceasefire	O
and	O
US	LOCATION
withdrawal	O
from	O
Vietnam	LOCATION
.	O
As	O
National	ORGANIZATION
Security	ORGANIZATION
Advisor	O
under	O
Nixon	PERSON
,	O
Kissinger	PERSON
pioneered	O
the	O
policy	O
of	O
détente	PERSON
with	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
,	O
seeking	O
a	O
relaxation	O
in	O
tensions	O
between	O
the	O
two	O
superpowers	O
.	O
Negotiations	O
about	O
strategic	O
disarmament	O
were	O
originally	O
supposed	O
to	O
start	O
under	O
the	ORGANIZATION
Johnson	ORGANIZATION
Administration	ORGANIZATION
but	O
were	O
postponed	O
in	O
protest	O
to	O
the	O
invasion	O
by	O
Warsaw	ORGANIZATION
Pact	ORGANIZATION
troops	O
of	O
Czechoslovakia	LOCATION
in	O
August	O
1968	O
.	O
Kissinger	PERSON
sought	O
to	O
place	O
diplomatic	O
pressure	O
on	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
.	O
Kissinger	PERSON
's	O
involvement	O
in	O
Indochina	LOCATION
started	O
prior	O
to	O
his	O
appointment	O
as	O
National	ORGANIZATION
Security	ORGANIZATION
Adviser	O
to	O
Nixon	PERSON
.	O
While	O
still	O
at	O
Harvard	ORGANIZATION
,	O
he	O
had	O
worked	O
as	O
a	O
consultant	O
on	O
foreign	O
policy	O
to	O
both	O
the	ORGANIZATION
White	ORGANIZATION
House	ORGANIZATION
and	O
State	ORGANIZATION
Department	ORGANIZATION
.	O
He	O
became	O
convinced	O
of	O
the	O
meaninglessness	O
of	O
military	O
victories	O
in	O
Vietnam	LOCATION
,	O
"	O
...	O
unless	O
they	O
brought	O
about	O
a	O
political	O
reality	O
that	O
could	O
survive	O
our	O
ultimate	O
withdrawal	O
"	O
.	O
In	O
a	O
1967	O
peace	O
initiative	O
,	O
he	O
would	O
mediate	O
between	O
Wash.	LOCATION
and	O
Hanoi	LOCATION
.	O
Nixon	PERSON
had	O
been	O
elected	O
in	O
1968	O
on	O
the	O
promise	O
of	O
achieving	O
"	O
peace	O
with	O
honor	O
"	O
and	O
ending	O
the	MISC
Vietnam	MISC
War	MISC
.	O
The	O
bombing	O
campaign	O
contributed	O
to	O
the	O
chaos	O
of	O
the	MISC
Cambodian	MISC
Civil	MISC
War	MISC
,	O
which	O
saw	O
the	O
forces	O
of	O
dictator	O
Lon	PERSON
Nol	PERSON
unable	O
to	O
retain	O
foreign	O
support	O
to	O
combat	O
the	O
growing	O
Khmer	ORGANIZATION
Rouge	ORGANIZATION
insurgency	O
that	O
would	O
overthrow	O
him	O
in	O
1975	O
.	O
Under	O
Kissinger	PERSON
's	O
guidance	O
,	O
the	O
U.S.	LOCATION
government	O
supported	O
Pakistan	LOCATION
in	O
the	MISC
Liberation	MISC
War	MISC
of	MISC
Bangladesh	MISC
in	O
1971	O
.	O
Kissinger	PERSON
has	O
since	O
expressed	O
his	O
regret	O
over	O
the	O
comments	O
.	O
In	O
1973	O
,	O
Kissinger	PERSON
negotiated	O
the	O
end	O
to	O
the	MISC
Yom	MISC
Kippur	MISC
War	MISC
,	O
which	O
had	O
begun	O
on	O
October	O
6	O
,	O
1973	O
when	O
Egypt	LOCATION
and	O
Syria	LOCATION
attacked	O
Israel	LOCATION
in	O
retaliation	O
to	O
1967	O
's	O
Israeli	MISC
offensive	O
and	O
invasion	O
of	O
Sinai	LOCATION
.	O
Israel	LOCATION
regained	O
the	O
territory	O
it	O
lost	O
in	O
the	O
early	O
fighting	O
and	O
gained	O
new	O
territories	O
from	O
Syria	LOCATION
and	O
Egypt	LOCATION
,	O
including	O
land	O
in	O
Syria	LOCATION
east	O
of	O
the	O
previously	O
captured	O
Golan	LOCATION
Heights	LOCATION
,	O
and	O
additionally	O
on	O
the	ORGANIZATION
western	ORGANIZATION
bank	ORGANIZATION
of	O
the	MISC
Suez	MISC
Canal	MISC
,	O
although	O
they	O
did	O
lose	O
some	O
territory	O
on	O
the	O
eastern	O
side	O
of	O
the	MISC
Suez	MISC
Canal	MISC
that	O
had	O
been	O
in	O
Israeli	MISC
hands	O
since	O
the	O
end	O
of	O
the	MISC
Six	MISC
Day	MISC
War	MISC
.	O
The	LOCATION
United	LOCATION
States	LOCATION
continued	O
to	O
recognize	O
and	O
maintain	O
relationships	O
with	O
non-left-wing	O
governments	O
,	O
democratic	MISC
and	O
authoritarian	O
alike	O
.	O
John	PERSON
F.	PERSON
Kennedy	PERSON
's	PERSON
Alliance	ORGANIZATION
for	ORGANIZATION
Progress	ORGANIZATION
was	O
ended	O
in	O
1973	O
.	O
In	O
1974	O
,	O
negotiations	O
about	O
new	O
settlement	O
over	O
Panama	MISC
Canal	MISC
started	O
.	O
Kissinger	PERSON
initially	O
supported	O
the	O
normalization	O
of	O
United	LOCATION
States-Cuba	LOCATION
relations	O
,	O
broken	O
since	O
1961	O
(	O
all	O
U.S.	LOCATION
--	O
Cuban	MISC
trade	O
was	O
blocked	O
in	O
February	O
1962	O
,	O
a	O
few	O
weeks	O
after	O
the	O
exclusion	O
of	O
Cuba	LOCATION
from	O
the	ORGANIZATION
Organization	ORGANIZATION
of	ORGANIZATION
American	ORGANIZATION
States	ORGANIZATION
because	O
of	O
US	LOCATION
pressure	O
)	O
.	O
However	O
,	O
he	O
quickly	O
changed	O
his	O
mind	O
and	O
followed	O
Kennedy	PERSON
's	O
policy	O
.	O
After	O
the	O
involvement	O
of	O
the	ORGANIZATION
Cuban	ORGANIZATION
Revolutionary	ORGANIZATION
Armed	ORGANIZATION
Forces	ORGANIZATION
in	O
the	O
liberation	O
struggles	O
in	O
Angola	LOCATION
and	O
Mozambique	LOCATION
,	O
Kissinger	PERSON
said	O
that	O
unless	O
Cuba	LOCATION
withdrew	O
its	O
forces	O
relations	O
would	O
not	O
be	O
normalized	O
.	O
Cuba	LOCATION
refused	O
.	O
The	O
Nixon	PERSON
administration	O
authorized	O
the	ORGANIZATION
Central	ORGANIZATION
Intelligence	ORGANIZATION
Agency	ORGANIZATION
(	O
CIA	ORGANIZATION
)	O
to	O
instigate	O
a	O
military	O
coup	O
that	O
would	O
prevent	O
Allende	PERSON
's	O
inauguration	O
,	O
but	O
the	O
plan	O
was	O
not	O
successful	O
.	O
The	O
extent	O
of	O
Kissinger	PERSON
's	O
involvement	O
in	O
or	O
support	O
of	O
these	O
plans	O
is	O
a	O
subject	O
of	O
controversy	O
.	O
On	O
September	O
16	O
,	O
1973	O
,	O
five	O
days	O
after	O
Pinochet	PERSON
had	O
assumed	O
power	O
,	O
the	O
following	O
exchange	O
about	O
the	O
coup	O
took	O
place	O
between	O
Kissinger	PERSON
and	O
President	O
Nixon	PERSON
:	O
Kissinger	PERSON
canceled	O
a	O
letter	O
that	O
was	O
to	O
be	O
sent	O
to	O
Chile	LOCATION
warning	O
them	O
against	O
carrying	O
out	O
any	O
political	O
assassinations	O
.	O
Orlando	PERSON
Letelier	PERSON
was	O
then	O
assassinated	O
in	O
Wash.	LOCATION
,	O
DC	LOCATION
with	O
a	O
car	O
bomb	O
on	O
September	O
21	O
,	O
1976	O
.	O
The	ORGANIZATION
National	ORGANIZATION
Salvation	ORGANIZATION
Junta	ORGANIZATION
,	O
the	O
new	O
government	O
,	O
quickly	O
granted	O
Portugal	LOCATION
's	O
colonies	O
independence	O
.	O
Cuban	MISC
troops	O
in	O
Angola	LOCATION
supported	O
the	O
left-wing	O
Popular	ORGANIZATION
Movement	ORGANIZATION
for	ORGANIZATION
the	ORGANIZATION
Liberation	ORGANIZATION
of	ORGANIZATION
Angola	ORGANIZATION
(	O
MPLA	O
)	O
in	O
its	O
fight	O
against	O
right-wing	O
UNITA	ORGANIZATION
and	O
FNLA	ORGANIZATION
rebels	O
during	O
the	MISC
Angolan	MISC
Civil	MISC
War	MISC
(	O
1975	O
--	O
2002	O
)	O
.	O
Kissinger	PERSON
supported	O
FNLA	ORGANIZATION
,	O
led	O
by	O
Holden	PERSON
Roberto	PERSON
,	O
and	O
UNITA	ORGANIZATION
,	O
led	O
by	O
Jonas	PERSON
Savimbi	PERSON
,	O
the	ORGANIZATION
Mozambican	ORGANIZATION
National	ORGANIZATION
Resistance	ORGANIZATION
(	O
RENAMO	O
)	O
insurgencies	O
,	O
as	O
well	O
as	O
the	O
CIA-supported	ORGANIZATION
invasion	O
of	O
Angola	LOCATION
by	O
South	MISC
African	MISC
troops	O
.	O
Only	O
under	O
Reagan	PERSON
's	O
presidency	O
would	O
U.S.	LOCATION
support	O
for	O
UNITA	ORGANIZATION
return	O
.	O
In	O
September	O
1976	O
Kissinger	PERSON
was	O
actively	O
involved	O
in	O
negotiations	O
regarding	O
the	O
Rhodesian	MISC
Bush	O
War	O
.	O
Kissinger	PERSON
,	O
along	O
with	O
South	LOCATION
Africa	LOCATION
's	LOCATION
Prime	O
Minister	O
John	PERSON
Vorster	PERSON
,	O
pressured	O
Rhodesian	MISC
Prime	O
Minister	O
Ian	PERSON
Smith	PERSON
to	O
hasten	O
the	O
transition	O
to	O
black	O
majority	O
rule	O
in	O
Rhodesia	LOCATION
.	O
With	O
FRELIMO	ORGANIZATION
in	O
control	O
of	O
Mozambique	LOCATION
and	O
even	O
South	LOCATION
Africa	LOCATION
withdrawing	O
its	O
support	O
,	O
Rhodesia	LOCATION
's	O
isolation	O
was	O
nearly	O
complete	O
.	O
According	O
to	O
Smith	ORGANIZATION
's	O
autobiography	O
,	O
Kissinger	PERSON
told	O
Smith	PERSON
of	O
Mrs.	O
Kissinger	PERSON
's	O
admiration	O
for	O
him	O
,	O
but	O
Smith	PERSON
stated	O
that	O
he	O
thought	O
Kissinger	PERSON
was	O
asking	O
him	O
to	O
sign	O
Rhodesia	LOCATION
's	O
"	O
death	O
certificate	O
"	O
.	O
Both	O
Ford	ORGANIZATION
and	O
Kissinger	PERSON
made	O
clear	O
that	O
US	LOCATION
relations	O
with	O
Indonesia	LOCATION
would	O
remain	O
strong	O
and	O
that	O
it	O
would	O
not	O
object	O
to	O
the	O
proposed	O
annexation	O
.	O
US	LOCATION
arms	O
sales	O
to	O
Indonesia	LOCATION
continued	O
,	O
and	O
Suharto	PERSON
went	O
ahead	O
with	O
the	O
annexation	O
plan	O
.	O
Shortly	O
after	O
Kissinger	PERSON
left	O
office	O
in	O
1977	O
,	O
he	O
was	O
offered	O
an	O
endowed	O
chair	O
at	O
Columbia	ORGANIZATION
University	ORGANIZATION
.	O
There	O
was	O
significant	O
student	O
opposition	O
to	O
the	O
appointment	O
,	O
which	O
eventually	O
became	O
a	O
subject	O
of	O
significant	O
media	O
commentary	O
Columbia	ORGANIZATION
cancelled	O
the	O
appointment	O
as	O
a	O
result	O
.	O
Kissinger	PERSON
was	O
then	O
appointed	O
to	O
Georgetown	ORGANIZATION
University	ORGANIZATION
's	ORGANIZATION
Center	O
for	O
Strategic	O
and	O
International	O
Studies	O
.	O
He	O
taught	O
at	O
Georgetown	LOCATION
's	O
Edmund	ORGANIZATION
Walsh	ORGANIZATION
School	ORGANIZATION
of	ORGANIZATION
Foreign	ORGANIZATION
Service	ORGANIZATION
for	O
several	O
years	O
in	O
the	O
late	O
1970s	O
.	O
In	O
1982	O
,	O
Kissinger	PERSON
founded	O
a	O
consulting	O
firm	O
,	O
Kissinger	ORGANIZATION
Associates	ORGANIZATION
,	O
and	O
is	O
a	O
partner	O
in	O
affiliate	O
Kissinger	ORGANIZATION
McLarty	ORGANIZATION
Associates	ORGANIZATION
with	O
Mack	ORGANIZATION
McLarty	ORGANIZATION
,	O
former	O
chief	O
of	O
staff	O
to	O
President	O
Bill	PERSON
Clinton	PERSON
.	O
He	O
also	O
serves	O
on	O
board	O
of	O
directors	O
of	O
Hollinger	ORGANIZATION
International	ORGANIZATION
,	O
a	O
Chicago-based	LOCATION
newspaper	O
group	O
,	O
and	O
as	O
of	O
March	O
1999	O
,	O
he	O
also	O
serves	O
on	O
board	O
of	O
directors	O
of	O
Gulfstream	ORGANIZATION
Aerospace	ORGANIZATION
.	O
In	O
1978	O
,	O
Kissinger	PERSON
was	O
named	O
chairman	O
of	O
the	ORGANIZATION
North	ORGANIZATION
American	ORGANIZATION
Soccer	ORGANIZATION
League	ORGANIZATION
board	O
of	O
directors	O
.	O
From	O
1995	O
to	O
2001	O
,	O
he	O
served	O
on	O
the	O
board	O
of	O
directors	O
for	O
Freeport-McMoRan	ORGANIZATION
,	O
a	O
multinational	O
copper	O
and	O
gold	O
producer	O
with	O
significant	O
mining	O
and	O
milling	O
operations	O
in	O
Papua	LOCATION
,	O
Indonesia	LOCATION
.	O
In	O
February	O
2000	O
,	O
then-president	O
of	O
Indonesia	LOCATION
Abdurrahman	PERSON
Wahid	PERSON
appointed	O
Kissinger	PERSON
as	O
a	O
political	O
advisor	O
.	O
He	O
also	O
serves	O
as	O
an	O
honorary	O
advisor	O
to	O
the	LOCATION
United	LOCATION
States-Azerbaijan	LOCATION
Chamber	O
of	O
Commerce	O
.	O
Kissinger	PERSON
continued	O
to	O
participate	O
in	O
policy	O
groups	O
,	O
such	O
as	O
the	ORGANIZATION
Trilateral	ORGANIZATION
Commission	ORGANIZATION
,	O
and	O
to	O
maintain	O
political	O
consulting	O
,	O
speaking	O
,	O
and	O
writing	O
engagements	O
.	O
[	O
citation	O
needed	O
]	O
Kissinger	PERSON
stepped	O
down	O
as	O
chairman	O
on	O
December	O
13	O
,	O
2002	O
rather	O
than	O
reveal	O
his	O
client	O
list	O
,	O
when	O
queried	O
about	O
potential	O
conflicts	O
of	O
interest	O
.	O
Most	O
importantly	O
he	O
dismissed	O
the	O
notion	O
of	O
Serbs	MISC
,	O
and	O
Croats	MISC
for	O
that	O
part	O
,	O
being	O
aggressors	O
or	O
separatist	O
,	O
saying	O
that	O
"	O
they	O
ca	O
n't	O
be	O
separating	O
from	O
something	O
that	O
has	O
never	O
existed	O
"	O
.	O
In	O
2006	O
,	O
it	O
was	O
reported	O
in	O
the	O
book	O
State	ORGANIZATION
of	ORGANIZATION
Denial	ORGANIZATION
by	O
Bob	PERSON
Woodward	PERSON
that	O
Kissinger	PERSON
was	O
meeting	O
regularly	O
with	O
President	O
George	PERSON
W.	PERSON
Bush	PERSON
and	O
Vice	O
President	O
Dick	PERSON
Cheney	PERSON
to	O
offer	O
advice	O
on	O
the	MISC
Iraq	MISC
War	MISC
.	O
Kissinger	PERSON
confirmed	O
in	O
recorded	O
interviews	O
with	O
Woodward	PERSON
that	O
the	O
advice	O
was	O
the	O
same	O
as	O
he	O
had	O
given	O
in	O
an	O
August	O
12	O
,	O
2005	O
column	O
in	O
The	ORGANIZATION
Washington	ORGANIZATION
Post	ORGANIZATION
:	O
"	O
Victory	O
over	O
the	O
insurgency	O
is	O
the	O
only	O
meaningful	O
exit	O
strategy	O
.	O
"	O
After	O
apologizing	O
for	O
his	O
use	O
of	O
the	O
word	O
'	O
bitch	O
'	O
in	O
reference	O
to	O
Mrs.	O
Indira	PERSON
Gandhi	PERSON
,	O
Kissinger	PERSON
met	O
India	LOCATION
's	O
main	O
Opposition	ORGANIZATION
Leader	O
Lal	PERSON
Krishna	PERSON
Advani	PERSON
in	O
early	O
October	O
2007	O
and	O
lobbied	O
for	O
the	O
support	O
of	O
his	O
Bharatiya	ORGANIZATION
Janata	ORGANIZATION
Party	ORGANIZATION
for	O
the	O
Indo-US	O
civilian	O
nuclear	O
agreement	O
.	O
Kissinger	PERSON
was	O
present	O
at	O
the	O
opening	O
ceremony	O
of	O
the	MISC
Beijing	MISC
Summer	MISC
Olympics	MISC
.	O
Kissinger	PERSON
said	O
in	O
April	O
2008	O
that	O
"	O
India	LOCATION
has	O
parallel	O
objectives	O
to	O
the	LOCATION
United	LOCATION
States	LOCATION
"	O
and	O
he	O
called	O
it	O
an	O
ally	O
of	O
the	O
U.S	LOCATION
.	O
Kissinger	PERSON
,	O
like	O
the	O
rest	O
of	O
the	O
Nixon	PERSON
administration	O
,	O
was	O
unpopular	O
with	O
the	O
anti-war	O
political	O
left	O
,	O
especially	O
after	O
his	O
central	O
role	O
in	O
the	O
US	LOCATION
bombing	O
of	O
neutral	O
Cambodia	LOCATION
was	O
revealed	O
.	O
Kissinger	PERSON
was	O
also	O
opposed	O
by	O
more	O
ideological	O
elements	O
of	O
the	O
Republican	MISC
foreign	O
policy	O
establishment	O
for	O
his	O
policy	O
of	O
detente	O
and	O
accomodation	O
with	O
the	O
Soviets	MISC
.	O
However	O
,	O
few	O
doubted	O
his	O
intellect	O
and	O
diplomatic	O
skill	O
,	O
and	O
he	O
became	O
one	O
of	O
the	O
better-liked	O
members	O
of	O
the	O
Nixon	PERSON
administration	O
,	O
though	O
many	O
Americans	MISC
came	O
to	O
view	O
Kissinger	PERSON
's	O
talents	O
as	O
increasingly	O
cynical	O
and	O
self-serving	O
.	O
Kissinger	PERSON
was	O
not	O
connected	O
with	O
the	O
Watergate	MISC
scandal	O
that	O
would	O
eventually	O
ruin	O
Nixon	PERSON
and	O
many	O
of	O
his	O
closest	O
aides	O
,	O
and	O
this	O
greatly	O
improved	O
Kissinger	PERSON
's	O
reputation	O
as	O
he	O
became	O
known	O
as	O
the	O
"	O
clean	O
man	O
"	O
of	O
the	O
bunch	O
.	O
At	O
the	O
height	O
of	O
Kissinger	PERSON
's	O
prominence	O
,	O
he	O
was	O
even	O
regarded	O
as	O
something	O
of	O
a	O
sex	O
symbol	O
due	O
to	O
his	O
prominent	O
dating	O
life	O
.	O
Kissinger	PERSON
has	O
shied	O
away	O
from	O
mainstream	O
media	O
and	O
cable	O
talk	O
shows	O
.	O
Recently	O
,	O
he	O
granted	O
a	O
rare	O
interview	O
to	O
the	O
producers	O
of	O
a	O
documentary	O
examining	O
the	O
underpinnings	O
of	O
the	O
1979	O
peace	O
treaty	O
between	O
Israel	LOCATION
and	O
Egypt	LOCATION
entitled	O
"	O
Back	MISC
Door	MISC
Channels	MISC
:	MISC
The	MISC
Price	MISC
of	MISC
Peace	MISC
"	O
.	O
In	O
the	O
film	O
,	O
a	O
candid	O
Kissinger	PERSON
reveals	O
how	O
close	O
he	O
felt	O
the	O
world	O
was	O
to	O
nuclear	O
war	O
during	O
the	O
1973	O
Yom	MISC
Kippur	MISC
War	MISC
launched	O
by	O
Egypt	LOCATION
and	O
Syria	LOCATION
against	O
Israeli	MISC
forces	O
in	O
the	O
territories	O
Israel	LOCATION
occupied	O
from	O
them	O
as	O
a	O
result	O
of	O
the	MISC
1967	MISC
War	MISC
.	O
Despite	O
popular	O
belief,	O
[	O
citation	O
needed	O
]	O
Kissinger	PERSON
did	O
not	O
voice	O
himself	O
on	O
the	O
animated	O
shows	O
The	O
Simpsons	ORGANIZATION
or	O
Futurama	ORGANIZATION
.	O
In	O
1973	O
,	O
Kissinger	PERSON
and	O
Le	PERSON
Duc	PERSON
Tho	PERSON
were	O
awarded	O
the	MISC
Nobel	MISC
Peace	MISC
Prize	MISC
for	O
the	MISC
Paris	MISC
Peace	MISC
Accords	MISC
of	O
1973	O
,	O
"	O
intended	O
to	O
bring	O
about	O
a	O
cease-fire	O
in	O
the	MISC
Vietnam	MISC
war	MISC
and	O
a	O
withdrawal	O
of	O
the	O
American	MISC
forces,	O
"	O
while	O
serving	O
as	O
the	LOCATION
United	LOCATION
States	LOCATION
Secretary	O
of	O
State	ORGANIZATION
.	O
In	O
1998	O
,	O
Kissinger	PERSON
became	O
an	O
honorary	O
citizen	O
of	O
Fürth	LOCATION
,	O
Germany	LOCATION
,	O
his	O
hometown	O
.	O
He	O
has	O
been	O
a	O
life-long	O
supporter	O
of	O
the	ORGANIZATION
Spielvereinigung	ORGANIZATION
Greuther	ORGANIZATION
Fürth	ORGANIZATION
football	O
club	O
and	O
is	O
now	O
an	O
honorary	O
member	O
.	O
He	O
served	O
as	O
Chancellor	O
of	O
the	ORGANIZATION
College	ORGANIZATION
of	ORGANIZATION
William	ORGANIZATION
and	O
Mary	PERSON
from	O
February	O
10	O
,	O
2001	O
to	O
the	O
summer	O
of	O
2005	O
.	O
In	O
April	O
2006	O
,	O
Kissinger	PERSON
received	O
the	O
prestigious	O
Woodrow	PERSON
Wilson	PERSON
Award	PERSON
for	O
Public	O
Service	O
from	O
the	ORGANIZATION
Woodrow	ORGANIZATION
Wilson	ORGANIZATION
Center	ORGANIZATION
of	O
the	ORGANIZATION
Smithsonian	ORGANIZATION
Institution	ORGANIZATION
.	O
He	O
was	O
celebrated	O
by	O
tens	O
of	O
thousands	O
of	O
spectators	O
on	O
Fifth	MISC
Avenue	MISC
.	O
Kissinger	PERSON
is	O
known	O
to	O
be	O
a	O
member	O
of	O
the	O
following	O
groups	O
:	O
