He	O
was	O
,	O
with	O
Bertrand	PERSON
Russell	PERSON
,	O
Ludwig	PERSON
Wittgenstein	PERSON
,	O
and	O
(	O
before	O
them	O
)	O
Gottlob	PERSON
Frege	PERSON
,	O
one	O
of	O
the	O
founders	O
of	O
the	O
analytic	O
tradition	O
in	O
philosophy	O
.	O
He	O
was	O
the	O
brother	O
of	O
the	O
writer	O
and	O
engraver	O
Thomas	PERSON
Sturge	PERSON
Moore	PERSON
.	O
Moore	PERSON
is	O
best	O
known	O
today	O
for	O
his	O
defense	O
of	O
ethical	O
non-naturalism	O
,	O
his	O
emphasis	O
on	O
common	O
sense	O
in	O
philosophical	O
method	O
,	O
and	O
the	O
paradox	O
that	O
bears	O
his	O
name	O
.	O
He	O
was	O
admired	O
by	O
and	O
influential	O
among	O
other	O
philosophers	O
,	O
and	O
also	O
by	O
the	ORGANIZATION
Bloomsbury	ORGANIZATION
Group	ORGANIZATION
,	O
but	O
is	O
(	O
unlike	O
his	O
colleague	O
Russell	PERSON
)	O
mostly	O
unknown	O
today	O
outside	O
of	O
academic	O
philosophy	O
.	O
Moore	PERSON
's	O
essays	O
are	O
known	O
for	O
his	O
clear	O
,	O
circumspect	O
writing	O
style	O
,	O
and	O
for	O
his	O
methodical	O
and	O
patient	O
approach	O
to	O
philosophical	O
problems	O
.	O
Moore	PERSON
thought	O
Thales	ORGANIZATION
'	O
reasoning	O
was	O
one	O
of	O
the	O
few	O
historical	O
examples	O
of	O
philosophical	O
inquiry	O
resulting	O
in	O
practical	O
advances	O
.	O
He	O
was	O
president	O
of	O
the	ORGANIZATION
Aristotelian	ORGANIZATION
Society	ORGANIZATION
from	O
1918	O
to	O
1919	O
.	O
The	O
business	O
of	O
ethics	O
,	O
Moore	PERSON
agreed	O
,	O
is	O
to	O
discover	O
the	O
qualities	O
that	O
make	O
things	O
good	O
.	O
With	O
this	O
project	O
Moore	PERSON
has	O
no	O
quarrel	O
.	O
Moore	PERSON
regards	O
this	O
as	O
a	O
serious	O
confusion	O
.	O
But	O
this	O
does	O
not	O
mean	O
,	O
Moore	PERSON
wants	O
to	O
insist	O
,	O
that	O
we	O
can	O
define	O
value	O
in	O
terms	O
of	O
pleasure	O
.	O
According	O
to	O
Moore	PERSON
,	O
these	O
questions	O
are	O
open	O
and	O
these	O
statements	O
are	O
significant	O
;	O
and	O
they	O
will	O
remain	O
so	O
no	O
matter	O
what	O
is	O
substituted	O
for	O
"	O
pleasure	O
"	O
.	O
Moore	PERSON
concludes	O
from	O
this	O
that	O
any	O
analysis	O
of	O
value	O
is	O
bound	O
to	O
fail	O
.	O
Critics	O
of	O
Moore	PERSON
's	O
arguments	O
sometimes	O
claim	O
that	O
he	O
is	O
appealing	O
to	O
general	O
puzzles	O
concerning	O
analysis	O
(	O
cf.	O
the	O
paradox	O
of	O
analysis	O
)	O
,	O
rather	O
than	O
revealing	O
anything	O
special	O
about	O
value	O
.	O
Other	O
responses	O
appeal	O
to	O
the	O
Fregean	O
distinction	O
between	O
sense	O
and	O
reference	O
,	O
allowing	O
that	O
value	O
concepts	O
are	O
special	O
and	O
sui	O
generis	O
,	O
but	O
insisting	O
that	O
value	O
properties	O
are	O
nothing	O
but	O
natural	O
properties	O
(	O
this	O
strategy	O
is	O
similar	O
to	O
that	O
taken	O
by	O
non-reductive	O
materialists	O
in	O
philosophy	O
of	O
mind	O
)	O
.	O
Moore	PERSON
contended	O
that	O
goodness	O
can	O
not	O
be	O
analysed	O
in	O
terms	O
of	O
any	O
other	O
property	O
.	O
In	O
Principia	PERSON
Ethica	PERSON
,	O
he	O
writes	O
:	O
In	O
addition	O
to	O
categorising	O
"	O
good	O
"	O
as	O
indefinable	O
,	O
Moore	PERSON
also	O
emphasized	O
that	O
it	O
is	O
a	O
non-natural	O
property	O
.	O
Moore	PERSON
argued	O
that	O
once	O
arguments	O
based	O
on	O
the	O
naturalistic	O
fallacy	O
had	O
been	O
discarded	O
,	O
questions	O
of	O
intrinsic	O
goodness	O
could	O
only	O
be	O
settled	O
by	O
appeal	O
to	O
what	O
he	O
(	O
following	O
Sidgwick	PERSON
)	O
called	O
"	O
moral	O
intuitions:	O
"	O
self-evident	O
propositions	O
which	O
recommend	O
themselves	O
to	O
moral	O
reflection	O
,	O
but	O
which	O
are	O
not	O
susceptible	O
to	O
either	O
direct	O
proof	O
or	O
disproof	O
.	O
Moore	PERSON
distinguished	O
his	O
view	O
from	O
the	O
view	O
of	O
deontological	O
intuitionists	ORGANIZATION
,	O
who	O
held	O
that	O
"	O
intuitions	O
"	O
could	O
determine	O
questions	O
about	O
what	O
actions	O
are	O
right	O
or	O
required	O
by	O
duty	O
.	O
Moore	PERSON
,	O
as	O
a	O
consequentialist	O
,	O
argued	O
that	O
"	O
duties	O
"	O
and	O
moral	O
rules	O
could	O
be	O
determined	O
by	O
investigating	O
the	O
effects	O
of	O
particular	O
actions	O
or	O
kinds	O
of	O
actions	O
,	O
and	O
so	O
were	O
matters	O
for	O
empirical	O
investigation	O
rather	O
than	O
direct	O
objects	O
of	O
intuition	O
.	O
On	O
Moore	PERSON
's	O
view	O
,	O
"	O
intuitions	O
"	O
revealed	O
not	O
the	O
rightness	O
or	O
wrongness	O
of	O
specific	O
actions	O
,	O
but	O
only	O
what	O
things	O
were	O
good	O
in	O
themselves	O
,	O
as	O
ends	O
to	O
be	O
pursued	O
.	O
Not	O
surprisingly	O
,	O
not	O
everyone	O
inclined	O
to	O
sceptical	O
doubts	O
found	O
Moore	PERSON
's	O
method	O
of	O
argument	O
entirely	O
convincing	O
;	O
Moore	PERSON
,	O
however	O
,	O
defends	O
his	O
argument	O
on	O
the	O
grounds	O
that	O
sceptical	O
arguments	O
seem	O
invariably	O
to	O
require	O
an	O
appeal	O
to	O
"	O
philosophical	O
intuitions	O
"	O
that	O
we	O
have	O
considerably	O
less	O
reason	O
to	O
accept	O
than	O
we	O
have	O
for	O
the	O
common	O
sense	O
claims	O
that	O
they	O
supposedly	O
refute	O
.	O
In	O
addition	O
to	O
Moore	PERSON
's	O
own	O
work	O
on	O
the	O
paradox	O
,	O
the	O
puzzle	O
also	O
inspired	O
a	O
great	O
deal	O
of	O
work	O
by	O
Ludwig	PERSON
Wittgenstein	PERSON
,	O
who	O
described	O
the	O
paradox	O
as	O
the	O
most	O
impressive	O
philosophical	O
insight	O
that	O
Moore	PERSON
had	O
ever	O
introduced	O
.	O
It	O
is	O
said	O
that	O
when	O
Wittgenstein	PERSON
first	O
heard	O
this	O
paradox	O
one	O
evening	O
(	O
which	O
Moore	PERSON
had	O
earlier	O
stated	O
in	O
a	O
lecture	O
)	O
,	O
he	O
rushed	O
round	O
to	O
Moore	PERSON
's	O
lodgings	O
,	O
got	O
him	O
out	O
of	O
bed	O
and	O
insisted	O
that	O
Moore	PERSON
repeat	O
the	O
entire	O
lecture	O
to	O
him	O
.	O
Moore	PERSON
's	O
description	O
of	O
the	O
principle	O
of	O
organic	O
unity	O
is	O
extremely	O
straightforward	O
;	O
nonetheless	O
,	O
it	O
is	O
a	O
principle	O
that	O
seems	O
to	O
have	O
generally	O
escaped	O
ethical	O
philosophers	O
and	O
ontologists	O
before	O
his	O
time	O
:	O
According	O
to	O
Moore	PERSON
,	O
a	O
moral	O
actor	O
can	O
not	O
survey	O
the	O
"	O
goodness	O
"	O
inherent	O
in	O
the	O
various	O
parts	O
of	O
a	O
situation	O
,	O
assign	O
a	O
value	O
to	O
each	O
of	O
them	O
,	O
and	O
then	O
generate	O
a	O
sum	O
in	O
order	O
to	O
get	O
an	O
idea	O
of	O
its	O
total	O
value	O
.	O
To	O
understand	O
the	O
application	O
of	O
the	O
organic	O
principle	O
to	O
questions	O
of	O
value	O
,	O
it	O
is	O
perhaps	O
best	O
to	O
consider	O
Moore	PERSON
's	O
primary	O
example	O
,	O
that	O
of	O
a	O
consciousness	O
experiencing	O
a	O
beautiful	O
object	O
.	O
They	O
might	O
have	O
some	O
value	O
,	O
but	O
when	O
we	O
consider	O
the	O
total	O
value	O
of	O
a	O
consciousness	O
experiencing	O
a	O
beautiful	O
object	O
,	O
it	O
seems	O
to	O
exceed	O
the	O
simple	O
sum	O
of	O
these	O
values	O
(	O
Principia	O
18:2	O
)	O
.	O
