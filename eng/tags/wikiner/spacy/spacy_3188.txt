Asia	LOCATION
tops	O
the	O
list	O
of	O
casualties	O
due	O
to	O
natural	O
disaster	O
.	O
Rising	O
frequency	O
,	O
amplitude	O
and	O
number	O
of	O
natural	O
disasters	O
and	O
attendant	O
problem	O
coupled	O
with	O
loss	O
of	O
human	O
lives	O
prompted	O
the	ORGANIZATION
General	ORGANIZATION
Assembly	ORGANIZATION
of	O
the	ORGANIZATION
United	ORGANIZATION
Nations	ORGANIZATION
to	O
proclaim	O
1990s	O
as	O
the	MISC
International	MISC
Decade	MISC
for	MISC
Natural	MISC
Disaster	MISC
Reduction	MISC
(	O
IDNDR	O
)	O
through	O
a	O
resolution	O
44/236	O
of	O
December	O
22	O
,	O
1989	O
to	O
focus	O
on	O
all	O
issues	O
related	O
to	O
natural	O
disaster	O
reduction	O
.	O
In	O
spite	O
of	O
IDNDR	O
,	O
there	O
had	O
been	O
a	O
string	O
of	O
major	O
disaster	O
throughout	O
the	O
decade	O
.	O
Nevertheless	O
,	O
by	O
establishing	O
the	O
rich	O
disaster	O
management	O
related	O
traditions	O
and	O
by	O
spreading	O
public	O
awareness	O
the	O
IDNDR	O
provided	O
required	O
stimulus	O
for	O
disaster	O
reduction	O
.	O
