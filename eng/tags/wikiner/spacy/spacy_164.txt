Among	O
the	O
ancient	O
Greek	MISC
philosophers	O
an	O
axiom	O
was	O
a	O
claim	O
which	O
could	O
be	O
seen	O
to	O
be	O
true	O
without	O
any	O
need	O
for	O
proof	O
.	O
The	O
root	O
meaning	O
of	O
the	O
word	O
'	O
postulate	O
'	O
is	O
to	O
'	O
demand	O
'	O
;	O
for	O
instance	O
,	O
Euclid	ORGANIZATION
demands	O
of	O
us	O
that	O
we	O
agree	O
that	O
the	O
some	O
things	O
can	O
be	O
done	O
,	O
e.g.	O
any	O
two	O
points	O
can	O
be	O
joined	O
by	O
a	O
straight	O
line	O
,	O
etc	O
.	O
Ancient	O
geometers	O
maintained	O
some	O
distinction	O
between	O
axioms	O
and	O
postulates	O
.	O
Boethius	PERSON
translated	O
'	O
postulate	O
'	O
as	O
petitio	ORGANIZATION
and	O
called	O
the	O
axioms	O
notiones	O
communes	O
but	O
in	O
later	O
manuscripts	O
this	O
usage	O
was	O
not	O
always	O
strictly	O
kept	O
.	O
The	O
logico-deductive	O
method	O
whereby	O
conclusions	O
(	O
new	O
knowledge	O
)	O
follow	O
from	O
premises	O
(	O
old	O
knowledge	O
)	O
through	O
the	O
application	O
of	O
sound	O
arguments	O
(	O
syllogisms	O
,	O
rules	O
of	O
inference	O
)	O
,	O
was	O
developed	O
by	O
the	O
ancient	O
Greeks	MISC
,	O
and	O
has	O
become	O
the	O
core	O
principle	O
of	O
modern	O
mathematics	O
[	O
citation	O
needed	O
]	O
.	O
The	O
ancient	O
Greeks	MISC
considered	O
geometry	O
as	O
just	O
one	O
of	O
several	O
sciences	O
[	O
citation	O
needed	O
]	O
,	O
and	O
held	O
the	O
theorems	O
of	O
geometry	O
on	O
par	O
with	O
scientific	O
facts	O
.	O
The	O
classical	O
approach	O
is	O
well	O
illustrated	O
by	O
Euclid	ORGANIZATION
's	ORGANIZATION
Elements	ORGANIZATION
,	O
where	O
a	O
list	O
of	O
postulates	O
is	O
given	O
(	O
common-sensical	O
geometric	O
facts	O
drawn	O
from	O
our	O
experience	O
)	O
,	O
followed	O
by	O
a	O
list	O
of	O
"	O
common	O
notions	O
"	O
(	O
very	O
basic	O
,	O
self-evident	O
assertions	O
)	O
.	O
The	O
postulates	O
of	O
Euclid	ORGANIZATION
are	O
profitably	O
motivated	O
by	O
saying	O
that	O
they	O
lead	O
to	O
a	O
great	O
wealth	O
of	O
geometric	O
facts	O
.	O
Frege	O
,	O
Russell	PERSON
,	O
Poincaré	ORGANIZATION
,	O
Hilbert	PERSON
,	O
and	O
Gödel	PERSON
are	O
some	O
of	O
the	O
key	O
figures	O
in	O
this	O
development	O
.	O
In	O
a	O
wider	O
context	O
,	O
there	O
was	O
an	O
attempt	O
to	O
base	O
all	O
of	O
mathematics	O
on	O
Cantor	ORGANIZATION
's	O
set	O
theory	O
.	O
The	O
formalist	O
project	O
suffered	O
a	O
decisive	O
setback	O
,	O
when	O
in	O
1931	O
Gödel	O
showed	O
that	O
it	O
is	O
possible	O
,	O
for	O
any	O
sufficiently	O
large	O
set	O
of	O
axioms	O
to	O
construct	O
a	O
statement	O
whose	O
truth	O
is	O
independent	O
of	O
that	O
set	O
of	O
axioms	O
.	O
They	O
are	O
a	O
set	O
of	O
axioms	O
strong	O
enough	O
to	O
prove	O
many	O
important	O
facts	O
about	O
number	O
theory	O
and	O
they	O
allowed	O
Gödel	O
to	O
establish	O
his	O
famous	O
second	O
incompleteness	O
theorem	O
.	O
Galois	PERSON
showed	O
just	O
before	O
his	O
untimely	O
death	O
that	O
these	O
efforts	O
were	O
largely	O
wasted	O
.	O
