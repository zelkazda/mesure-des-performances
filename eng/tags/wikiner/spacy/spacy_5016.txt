In	O
psychology	O
,	O
the	O
inventor	O
of	O
the	O
first	O
IQ	O
tests	O
,	O
Alfred	PERSON
Binet	PERSON
,	O
described	O
persons	O
in	O
the	O
top	O
1‰	O
of	O
those	O
tested	O
as	O
genii	O
(	O
a	O
noun	O
)	O
.	O
The	O
assessing	O
of	O
intelligence	O
was	O
initiated	O
by	O
Francis	PERSON
Galton	PERSON
and	O
James	PERSON
McKeen	PERSON
Cattell	PERSON
.	O
Galton	PERSON
is	O
regarded	O
as	O
the	O
founder	O
of	O
psychometry	O
(	O
among	O
other	O
kinds	O
of	O
assessing	O
,	O
such	O
as	O
fingerprinting	O
)	O
.	O
He	O
studied	O
the	O
work	O
of	O
Charles	PERSON
Darwin	PERSON
.	O
Charles	PERSON
Darwin	PERSON
showed	O
that	O
traits	O
must	O
be	O
inherited	O
before	O
evolution	O
can	O
occur	O
.	O
Galton	PERSON
's	O
theories	O
were	O
elaborated	O
from	O
the	O
work	O
of	O
two	O
early	O
19th-century	O
pioneers	O
in	O
statistics	O
:	O
Karl	PERSON
Friedrich	PERSON
Gauss	PERSON
and	O
Adolphe	PERSON
Quetelet	PERSON
.	O
Gauss	O
discovered	O
the	O
normal	O
distribution	O
(	O
bell-shaped	O
curve	O
)	O
:	O
given	O
a	O
large	O
number	O
of	O
measurements	O
of	O
the	O
same	O
variable	O
under	O
the	O
same	O
conditions	O
,	O
they	O
vary	O
at	O
random	O
from	O
a	O
most	O
frequent	O
value	O
,	O
the	O
"	O
average	O
"	O
,	O
to	O
two	O
least	O
frequent	O
values	O
at	O
maximum	O
differences	O
greater	O
and	O
less	O
than	O
the	O
most	O
frequent	O
value	O
.	O
In	O
contrast	O
to	O
Quetelet	ORGANIZATION
,	ORGANIZATION
Galton	ORGANIZATION
's	ORGANIZATION
average	O
man	O
was	O
not	O
statistical	O
,	O
but	O
was	O
theoretical	O
only	O
.	O
Setting	O
out	O
to	O
discover	O
a	O
general	O
measure	O
of	O
the	O
average	O
,	O
Galton	PERSON
looked	O
at	O
educational	O
statistics	O
and	O
found	O
bell-curves	O
in	O
test	O
results	O
of	O
all	O
sorts	O
;	O
initially	O
in	O
mathematics	O
grades	O
for	O
the	O
final	O
honors	O
examination	O
and	O
in	O
entrance	O
examination	O
scores	O
for	O
Sandhurst	ORGANIZATION
.	O
Galton	PERSON
was	O
looking	O
for	O
a	O
combination	O
of	O
differences	O
that	O
would	O
reveal	O
"	O
the	O
existence	O
of	O
grand	O
human	O
animals	O
,	O
of	O
natures	O
preeminantly	O
noble	O
,	O
of	O
individuals	O
born	O
to	O
be	O
kings	O
of	O
men	O
.	O
"	O
Darwin	PERSON
read	O
and	O
espoused	O
Galton	PERSON
's	O
work	O
.	O
Galton	PERSON
went	O
on	O
to	O
develop	O
the	O
field	O
of	O
eugenics	O
.	O
In	O
Ancient	LOCATION
Rome	LOCATION
,	O
the	O
genius	O
was	O
the	O
guiding	O
or	O
"	O
tutelary	O
"	O
spirit	O
of	O
a	O
person	O
,	O
or	O
even	O
of	O
an	O
entire	O
gens	O
,	O
the	O
plural	O
of	O
which	O
was	O
'	O
genii	O
'	O
.	O
Two	O
among	O
the	O
most	O
influential	O
psychologists	O
studying	O
intelligence	O
,	O
Lewis	PERSON
M.	PERSON
Terman	PERSON
and	O
Leta	PERSON
Hollingworth	PERSON
,	O
suggested	O
two	O
different	O
numbers	O
when	O
considering	O
the	O
cut-off	O
for	O
genius	O
in	O
psychometric	O
terms	O
.	O
Dr.	O
Terman	PERSON
considered	O
it	O
to	O
be	O
an	O
IQ	O
of	O
140	O
,	O
while	O
Dr.	O
Hollingworth	PERSON
put	O
it	O
at	O
an	O
IQ	O
of	O
180	O
.	O
[	O
citation	O
needed	O
]	O
Stephen	PERSON
Hawking	PERSON
,	O
widely	O
considered	O
to	O
be	O
a	O
genius	O
as	O
a	O
result	O
of	O
his	O
contributions	O
in	O
the	O
field	O
of	O
theoretical	O
physics	O
,	O
claims	O
to	O
not	O
know	O
his	O
IQ	O
.	O
In	O
the	O
philosophy	O
of	O
Arthur	PERSON
Schopenhauer	PERSON
,	O
a	O
genius	O
is	O
someone	O
in	O
whom	O
intellect	O
predominates	O
over	O
"	O
will	O
"	O
much	O
more	O
than	O
within	O
the	O
average	O
person	O
.	O
Their	O
remoteness	O
from	O
mundane	O
concerns	O
means	O
that	O
Schopenhauer	PERSON
's	O
geniuses	O
often	O
display	O
maladaptive	O
traits	O
in	O
more	O
mundane	O
concerns	O
;	O
in	O
Schopenhauer	PERSON
's	O
words	O
,	O
they	O
fall	O
into	O
the	O
mire	O
while	O
gazing	O
at	O
the	O
stars	O
.	O
In	O
the	O
philosophy	O
of	O
Immanuel	PERSON
Kant	PERSON
,	O
genius	O
is	O
the	O
ability	O
to	O
independently	O
arrive	O
at	O
and	O
understand	O
concepts	O
that	O
would	O
normally	O
have	O
to	O
be	O
taught	O
by	O
another	O
person	O
.	O
For	O
Kant	PERSON
,	O
originality	O
was	O
the	O
essential	O
character	O
of	O
genius	O
.	O
In	O
the	O
philosophy	O
of	O
David	PERSON
Hume	PERSON
,	O
the	O
way	O
society	O
perceives	O
genius	O
is	O
similar	O
to	O
the	O
way	O
society	O
perceives	O
the	O
ignorant	O
.	O
Hume	PERSON
states	O
that	O
a	O
person	O
with	O
the	O
characteristics	O
of	O
a	O
genius	O
is	O
looked	O
at	O
as	O
a	O
person	O
disconnected	O
from	O
society	O
,	O
as	O
well	O
as	O
a	O
person	O
who	O
works	O
remotely	O
,	O
at	O
a	O
distance	O
,	O
away	O
from	O
the	O
rest	O
of	O
the	O
world	O
.	O
In	O
the	O
philosophy	O
of	O
Nietzsche	PERSON
,	O
genius	O
is	O
merely	O
the	O
context	O
which	O
leads	O
us	O
to	O
consider	O
someone	O
a	O
genius	O
.	O
