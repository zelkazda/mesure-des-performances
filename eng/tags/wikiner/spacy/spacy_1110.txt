Alexandria	LOCATION
,	O
with	O
a	O
population	O
of	O
4.1	O
million	O
,	O
is	O
the	O
second-largest	O
city	O
in	O
Egypt	LOCATION
,	O
and	O
is	O
the	O
country	O
's	O
largest	O
seaport	O
,	O
serving	O
approximately	O
80	O
%	O
of	O
Egypt	LOCATION
's	O
imports	O
and	O
exports	O
.	O
Alexandria	LOCATION
is	O
also	O
an	O
important	O
tourist	O
resort	O
.	O
Alexandria	LOCATION
extends	O
about	O
32	O
km	O
(	O
20	O
mi	O
)	O
along	O
the	O
coast	O
of	O
the	LOCATION
Mediterranean	LOCATION
Sea	LOCATION
in	O
north-central	O
Egypt	LOCATION
.	O
It	O
is	O
home	O
to	O
the	ORGANIZATION
Bibliotheca	ORGANIZATION
Alexandrina	ORGANIZATION
(	O
the	O
new	O
Library	ORGANIZATION
)	O
.	O
It	O
is	O
an	O
important	O
industrial	O
centre	O
because	O
of	O
its	O
natural	O
gas	O
and	O
oil	O
pipelines	O
from	O
Suez	LOCATION
,	O
another	O
city	O
in	O
Egypt	LOCATION
.	O
Alexander	ORGANIZATION
's	O
chief	O
architect	O
for	O
the	O
project	O
was	O
Dinocrates	ORGANIZATION
.	O
An	O
Egyptian	MISC
city	O
,	O
Rhakotis	ORGANIZATION
,	O
already	O
existed	O
on	O
the	O
shore	O
,	O
and	O
later	O
gave	O
its	O
name	O
to	O
Alexandria	LOCATION
in	O
the	O
Egyptian	MISC
language	O
(	O
Egypt	LOCATION
.	O
It	O
continued	O
to	O
exist	O
as	O
the	MISC
Egyptian	MISC
quarter	MISC
of	O
the	O
city	O
.	O
After	O
Alexander	PERSON
departed	O
,	O
his	O
viceroy	O
,	O
Cleomenes	PERSON
,	O
continued	O
the	O
expansion	O
.	O
Following	O
a	O
struggle	O
with	O
the	O
other	O
successors	O
of	O
Alexander	PERSON
,	O
his	O
general	O
Ptolemy	PERSON
succeeded	O
in	O
bringing	O
Alexander	PERSON
's	O
body	O
to	O
Alexandria	LOCATION
.	O
From	O
this	O
division	O
arose	O
much	O
of	O
the	O
later	O
turbulence	O
,	O
which	O
began	O
to	O
manifest	O
itself	O
under	O
Ptolemy	O
Philopater	O
who	O
reigned	O
from	O
221	O
--	O
204	O
BCE	O
.	O
The	O
reign	O
of	O
Ptolemy	O
VIII	O
Physcon	O
from	O
144	O
--	O
116	O
BCE	O
was	O
marked	O
by	O
purges	O
and	O
civil	O
warfare	O
.	O
It	O
was	O
finally	O
captured	O
by	O
Octavian	PERSON
,	O
future	O
emperor	O
Augustus	PERSON
on	O
1	O
August	O
30	O
BCE	O
,	O
with	O
the	O
name	O
of	O
the	O
month	O
later	O
being	O
changed	O
to	O
august	O
to	O
commemorate	O
his	O
victory	O
.	O
In	O
215	O
the	O
emperor	O
Caracalla	PERSON
visited	O
the	O
city	O
and	O
,	O
because	O
of	O
some	O
insulting	O
satires	O
that	O
the	O
inhabitants	O
had	O
directed	O
at	O
him	O
,	O
abruptly	O
commanded	O
his	O
troops	O
to	O
put	O
to	O
death	O
all	O
youths	O
capable	O
of	O
bearing	O
arms	O
.	O
On	O
21	O
July	O
365	O
,	O
Alexandria	LOCATION
was	O
devastated	O
by	O
a	O
tsunami	O
(	O
365	LOCATION
Crete	LOCATION
earthquake	O
)	O
,	O
an	O
event	O
two	O
hundred	O
years	O
later	O
still	O
annually	O
commemorated	O
as	O
"	O
day	O
of	O
horror	O
"	O
.	O
Alexandria	LOCATION
figured	O
prominently	O
in	O
the	O
military	O
operations	O
of	O
Napoleon	ORGANIZATION
's	O
expedition	O
to	O
Egypt	LOCATION
in	O
1798	O
.	O
In	O
July	O
1882	O
,	O
the	O
city	O
came	O
under	O
bombardment	O
from	O
British	MISC
naval	O
forces	O
and	O
was	O
occupied	O
.	O
In	O
July	O
1954	O
,	O
the	O
city	O
was	O
a	O
target	O
of	O
an	O
Israeli	MISC
bombing	O
campaign	O
that	O
later	O
became	O
known	O
as	O
the	ORGANIZATION
Lavon	ORGANIZATION
Affair	ORGANIZATION
.	O
Alexandria	LOCATION
experiences	O
violent	O
storms	O
,	O
rain	O
and	O
sometimes	O
hail	O
during	O
the	O
cooler	O
months	O
.	O
None	O
,	O
however	O
,	O
are	O
as	O
famous	O
as	O
the	O
building	O
that	O
stood	O
on	O
the	O
eastern	O
point	O
of	O
Pharos	LOCATION
island	O
.	O
The	O
first	O
Ptolemy	MISC
began	O
the	O
project	O
,	O
and	O
the	O
second	O
Ptolemy	MISC
completed	O
it	O
,	O
at	O
a	O
total	O
cost	O
of	O
800	O
talents	O
.	O
The	ORGANIZATION
Pharos	ORGANIZATION
lighthouse	ORGANIZATION
was	O
destroyed	O
by	O
an	O
earthquake	O
in	O
the	O
14th	O
century	O
,	O
making	O
it	O
the	O
second	O
longest	O
surviving	O
ancient	O
wonder	O
next	O
to	O
the	MISC
Great	MISC
Pyramid	MISC
of	MISC
Giza	MISC
.	O
A	O
temple	O
of	O
Hephaestus	ORGANIZATION
also	O
stood	O
on	O
Pharos	PERSON
at	O
the	O
head	O
of	O
the	O
mole	O
.	O
Due	O
to	O
the	O
constant	O
presence	O
of	O
war	O
in	O
Alexandria	LOCATION
in	O
ancient	O
times	O
,	O
very	O
little	O
of	O
the	O
ancient	O
city	O
has	O
survived	O
into	O
the	O
present	O
day	O
.	O
Pompey	PERSON
's	O
Pillar	O
may	O
have	O
been	O
erected	O
using	O
the	O
same	O
methods	O
that	O
were	O
used	O
to	O
erect	O
the	O
ancient	O
obelisks	O
.	O
The	O
Romans	MISC
had	O
cranes	O
but	O
they	O
were	O
n't	O
strong	O
enough	O
to	O
lift	O
something	O
this	O
heavy	O
.	O
"	O
Pompey	O
's	O
Pillar	O
"	O
is	O
a	O
misnomer	O
,	O
as	O
it	O
has	O
nothing	O
to	O
do	O
with	O
Pompey	PERSON
,	O
having	O
been	O
erected	O
in	O
293	O
for	O
Diocletian	ORGANIZATION
,	O
possibly	O
in	O
memory	O
of	O
the	O
rebellion	O
of	O
Domitius	LOCATION
Domitianus	LOCATION
.	O
But	O
two	O
difficulties	O
face	O
the	O
would-be	O
excavator	O
in	O
Alexandria	LOCATION
:	O
lack	O
of	O
space	O
for	O
excavation	O
and	O
the	O
underwater	O
location	O
of	O
some	O
areas	O
of	O
interest	O
.	O
It	O
raised	O
a	O
noted	O
head	O
of	O
Caesarion	ORGANIZATION
.	O
Here	O
substructures	O
of	O
a	O
large	O
building	O
or	O
group	O
of	O
buildings	O
have	O
been	O
exposed	O
,	O
which	O
are	O
perhaps	O
part	O
of	O
the	O
Serapeum	MISC
.	O
The	O
objects	O
found	O
in	O
these	O
researches	O
are	O
in	O
the	O
museum	O
,	O
the	O
most	O
notable	O
being	O
a	O
great	O
basalt	O
bull	O
,	O
probably	O
once	O
an	O
object	O
of	O
cult	O
in	O
the	O
Serapeum	MISC
.	O
The	O
wealth	O
underground	O
is	O
doubtlessly	O
immense	O
;	O
but	O
despite	O
all	O
efforts	O
,	O
there	O
is	O
not	O
much	O
for	O
antiquarians	O
to	O
see	O
in	O
Alexandria	LOCATION
outside	O
the	O
museum	O
and	O
the	O
neighbourhood	O
of	O
"	O
Pompey	MISC
's	MISC
Pillar	MISC
"	O
.	O
Modern	LOCATION
Alexandria	LOCATION
is	O
divided	O
into	O
six	O
districts	O
:	O
There	O
are	O
also	O
two	O
cities	O
under	O
the	O
jurisdiction	O
of	O
the	O
Alexandria	LOCATION
governorate	O
forming	O
metropolitan	O
Alexandria	LOCATION
:	O
The	O
Pope	O
of	O
Alexandria	LOCATION
was	O
the	O
second	O
among	O
equals	O
,	O
second	O
only	O
to	O
the	O
bishop	O
of	O
Rome	LOCATION
,	O
the	O
capital	O
of	O
the	LOCATION
Roman	LOCATION
Empire	LOCATION
until	O
430	O
.	O
The	O
most	O
famous	O
mosque	MISC
in	O
Alexandria	LOCATION
is	O
Abu	LOCATION
el-Abbas	LOCATION
el-Mursi	LOCATION
Mosque	LOCATION
in	O
Anfoushi	PERSON
.	O
The	O
most	O
important	O
synagogue	O
in	O
Alexandria	LOCATION
is	O
the	MISC
Eliyahu	MISC
Hanavi	MISC
Synagogue	MISC
.	O
Alexandria	LOCATION
comprises	O
a	O
number	O
of	O
higher	O
education	O
institutions	O
.	O
Alexandria	ORGANIZATION
University	ORGANIZATION
is	O
a	O
public	O
university	O
that	O
follows	O
the	O
Egyptian	MISC
system	O
of	O
higher	O
education	O
.	O
In	O
addition	O
,	O
the	ORGANIZATION
Arab	ORGANIZATION
Academy	ORGANIZATION
for	ORGANIZATION
Science	ORGANIZATION
and	ORGANIZATION
Technology	ORGANIZATION
and	O
Maritime	ORGANIZATION
Transport	ORGANIZATION
is	O
a	O
semi-private	O
educational	O
institution	O
that	O
offers	O
courses	O
for	O
both	O
high	O
school	O
and	O
undergraduate	O
level	O
students	O
.	O
Other	O
institutions	O
of	O
higher	O
education	O
in	O
Alexandria	LOCATION
include	O
Alexandria	ORGANIZATION
Institute	ORGANIZATION
of	ORGANIZATION
Technology	ORGANIZATION
and	O
Pharos	ORGANIZATION
University	ORGANIZATION
in	O
Alexandria	LOCATION
.	O
Alexandria	LOCATION
has	O
a	O
very	O
long	O
history	O
of	O
foreign	O
educational	O
institutions	O
.	O
Most	O
of	O
these	O
schools	O
have	O
been	O
nationalized	O
during	O
the	O
era	O
of	O
Nasser	PERSON
,	O
and	O
are	O
currently	O
Egyptian	MISC
public	O
schools	O
run	O
by	O
the	O
Egyptian	MISC
ministry	O
of	O
education	O
.	O
Alexandria	LOCATION
is	O
served	O
by	O
Borg	ORGANIZATION
al	ORGANIZATION
Arab	ORGANIZATION
Airport	ORGANIZATION
located	O
about	O
25	O
km	O
away	O
from	O
city	O
centre	O
.	O
In	O
March	O
2010	O
,	O
the	O
former	O
airport	O
,	O
Alexandria	MISC
International	MISC
Airport	MISC
was	O
closed	O
to	O
commercial	O
operations	O
with	O
all	O
airlines	O
operating	O
out	O
of	O
Borg	ORGANIZATION
al	ORGANIZATION
Arab	ORGANIZATION
Airport	ORGANIZATION
where	O
a	O
brand	O
new	O
terminal	O
was	O
completed	O
in	O
February	O
2010	O
.	O
The	ORGANIZATION
Royal	ORGANIZATION
Library	ORGANIZATION
of	ORGANIZATION
Alexandria	ORGANIZATION
in	O
Alexandria	LOCATION
,	O
Egypt	LOCATION
,	O
was	O
once	O
the	O
largest	O
library	O
in	O
the	O
world	O
.	O
Alexandria	MISC
Stadium	MISC
is	O
a	O
multi-purpose	O
stadium	O
in	O
Alexandria	LOCATION
,	O
Egypt	LOCATION
.	O
It	O
is	O
currently	O
used	O
mostly	O
for	O
football	O
matches	O
,	O
and	O
was	O
used	O
for	O
the	MISC
2006	MISC
African	MISC
Cup	MISC
of	MISC
Nations	MISC
.	O
Alexandria	LOCATION
was	O
one	O
of	O
three	O
cities	O
that	O
participated	O
in	O
hosting	O
the	MISC
African	MISC
Cup	MISC
of	MISC
Nations	MISC
in	O
January	O
2006	O
,	O
which	O
Egypt	LOCATION
won	O
.	O
Alexandria	LOCATION
has	O
four	O
stadiums	O
:	O
Other	O
less	O
popular	O
sports	O
like	O
tennis	O
and	O
squash	O
are	O
usually	O
played	O
in	O
private	O
social	O
and	O
sports	O
clubs	O
,	O
like	O
:	O
Durrell	PERSON
used	O
the	O
cosmopolitan	O
city	O
as	O
a	O
landscape	O
to	O
explore	O
human	O
desires	O
.	O
Alexandria	LOCATION
is	O
twinned	O
with	O
:	O
