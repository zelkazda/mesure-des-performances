Bruce	PERSON
Perens	PERSON
is	O
a	O
computer	O
programmer	O
and	O
advocate	O
in	O
the	O
open	O
source	O
community	O
.	O
He	O
co-founded	O
the	O
Open	O
Source	O
Initiative	O
with	O
Eric	PERSON
S.	PERSON
Raymond	PERSON
.	O
This	O
differs	O
from	O
Stallman	PERSON
and	O
Raymond	PERSON
.	O
This	O
differs	O
from	O
Raymond	ORGANIZATION
's	O
theory	O
in	O
The	O
Cathedral	O
and	O
the	O
Bazaar	ORGANIZATION
,	O
which	O
having	O
been	O
written	O
before	O
there	O
was	O
much	O
business	O
involvement	O
in	O
open	O
source	O
,	O
explains	O
open	O
source	O
as	O
a	O
consequence	O
of	O
programmer	O
motivation	O
and	O
leisure	O
.	O
Debian	O
developers	O
contributed	O
discussion	O
and	O
changes	O
for	O
the	O
rest	O
of	O
the	O
month	O
while	O
Perens	PERSON
edited	O
,	O
and	O
the	O
completed	O
document	O
was	O
then	O
announced	O
as	O
Debian	MISC
project	O
policy	O
.	O
He	O
is	O
credited	O
as	O
a	O
studio	O
tools	O
engineer	O
on	O
Toy	ORGANIZATION
Story	ORGANIZATION
2	ORGANIZATION
and	ORGANIZATION
A	ORGANIZATION
Bug	ORGANIZATION
's	ORGANIZATION
Life	ORGANIZATION
.	O
With	O
this	O
funding	O
,	O
he	O
spent	O
part	O
of	O
the	O
summer	O
as	O
a	O
visiting	O
lecturer	O
and	O
researcher	O
at	O
University	ORGANIZATION
of	ORGANIZATION
Agder	ORGANIZATION
in	O
2006	O
and	O
2007	O
,	O
and	O
does	O
other	O
work	O
remotely	O
.	O
