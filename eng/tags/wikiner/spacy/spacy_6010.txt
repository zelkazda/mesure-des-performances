BitchX	LAW
1.1	LAW
final	O
was	O
released	O
in	O
2004	O
.	O
It	O
is	O
written	O
in	O
C	O
,	O
and	O
is	O
a	O
console	O
application	O
.	O
A	O
graphical	O
interface	O
is	O
also	O
available	O
,	O
which	O
uses	O
the	O
GTK+	O
toolkit	O
.	O
It	O
supports	O
IPv6	O
,	O
multiple	O
servers	O
and	O
SSL	O
,	O
but	O
not	O
UTF-8	ORGANIZATION
.	O
The	O
current	O
version	O
of	O
BitchX	LOCATION
,	O
released	O
in	O
2004	O
,	O
has	O
security	O
problems	O
allowing	O
remote	O
IRC	ORGANIZATION
servers	O
to	O
execute	O
arbitrary	O
code	O
on	O
the	O
client	O
's	O
machine	O
(	O
CVE-2007-3360	O
,	O
CVE-2007-4584	O
)	O
.	O
On	O
April	O
26	O
,	O
2009	O
,	O
Slackware	ORGANIZATION
removed	O
BitchX	LOCATION
from	O
its	O
distribution	O
,	O
citing	O
the	O
numerous	O
unresolved	O
security	O
issues	O
.	O
