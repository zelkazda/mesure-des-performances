Beastie	MISC
Boys	MISC
is	O
an	O
American	MISC
hip-hop	O
group	O
from	O
Brooklyn	LOCATION
,	O
New	LOCATION
York	LOCATION
.	O
The	O
group	O
comprises	O
Michael	PERSON
"	O
Mike	O
D	O
"	O
Diamond	O
,	O
Adam	PERSON
"	PERSON
MCA	PERSON
"	O
Yauch	PERSON
,	O
and	O
Adam	PERSON
"	PERSON
Ad-Rock	PERSON
"	O
Horovitz	PERSON
.	O
Since	O
the	O
release	O
of	O
Hello	ORGANIZATION
Nasty	ORGANIZATION
,	O
the	O
DJ	O
for	O
the	O
group	O
has	O
been	O
Michael	PERSON
"	PERSON
Mix	PERSON
Master	PERSON
Mike	PERSON
"	PERSON
Schwartz	PERSON
,	O
who	O
was	O
first	O
featured	O
in	O
the	O
song	O
"	O
Three	O
MCs	O
and	O
One	O
DJ	O
"	O
.	O
.	O
After	O
achieving	O
moderate	O
local	O
success	O
with	O
the	O
1983	O
release	O
of	O
experimental	O
hip-hop	O
12	O
"	O
Cooky	PERSON
Puss	PERSON
,	O
they	O
made	O
the	O
transition	O
to	O
hip-hop	O
in	O
1984	O
and	O
a	O
string	O
of	O
successful	O
12	O
"	O
singles	O
followed	O
culminating	O
with	O
their	O
debut	O
album	O
Licensed	MISC
to	MISC
Ill	MISC
(	MISC
1986	MISC
)	O
which	O
received	O
international	O
critical	O
acclaim	O
and	O
commercial	O
success	O
.	O
On	O
September	O
27	O
,	O
2007	O
,	O
they	O
were	O
nominated	O
for	O
induction	O
into	O
the	ORGANIZATION
Rock	ORGANIZATION
and	ORGANIZATION
Roll	ORGANIZATION
Hall	ORGANIZATION
of	ORGANIZATION
Fame	ORGANIZATION
.	O
In	O
2009	O
,	O
the	O
group	O
released	O
digitally	O
remastered	O
deluxe	O
editions	O
of	O
their	O
albums	O
Paul	ORGANIZATION
's	ORGANIZATION
Boutique	ORGANIZATION
,	O
Check	O
Your	O
Head	O
'	O
,	O
Ill	ORGANIZATION
Communication	ORGANIZATION
and	O
Hello	O
Nasty	O
.	O
The	MISC
Beastie	MISC
Boys	MISC
came	O
together	O
in	O
1981	O
as	O
a	O
hardcore	O
punk	O
band	O
.	O
The	O
opening	O
band	O
for	O
that	O
performance	O
was	O
"	O
The	MISC
Young	MISC
and	MISC
the	MISC
Useless	MISC
"	O
,	O
which	O
featured	O
Adam	PERSON
Horovitz	PERSON
(	O
Ad-Rock	O
)	O
as	O
their	O
lead	O
singer	O
.	O
The	O
band	O
quickly	O
earned	O
support	O
slots	O
for	O
Bad	ORGANIZATION
Brains	ORGANIZATION
,	O
the	O
Dead	O
Kennedys	O
,	O
the	O
Misfits	O
and	O
Reagan	PERSON
Youth	O
at	O
venues	O
such	O
as	O
CBGB	ORGANIZATION
and	O
Max	PERSON
's	O
Kansas	LOCATION
City	LOCATION
,	O
playing	O
at	O
the	O
latter	O
venue	O
on	O
its	O
closing	O
night	O
.	O
The	O
band	O
also	O
performed	O
its	O
first	O
rap	O
track	O
,	O
"	O
Cooky	PERSON
Puss	PERSON
"	O
,	O
based	O
on	O
a	O
prank	O
call	O
by	O
the	O
group	O
to	O
Carvel	ORGANIZATION
Ice	ORGANIZATION
Cream	ORGANIZATION
.	O
It	O
became	O
a	O
hit	O
in	O
New	LOCATION
York	LOCATION
underground	O
dance	O
clubs	O
upon	O
its	O
release	O
.	O
It	O
was	O
during	O
this	O
period	O
that	O
Def	ORGANIZATION
Jam	ORGANIZATION
record	O
producer	O
Rick	PERSON
Rubin	PERSON
signed	O
on	O
and	O
the	MISC
Beastie	MISC
Boys	MISC
changed	O
from	O
a	O
punk	O
rock	O
outfit	O
to	O
a	O
three-man	O
rap	O
crew	O
.	O
The	O
band	O
recorded	O
Licensed	O
to	O
Ill	O
in	O
1986	O
and	O
released	O
the	O
album	O
at	O
the	O
end	O
of	O
the	O
year	O
.	O
It	O
was	O
Def	PERSON
Jam	PERSON
's	PERSON
fastest	O
selling	O
debut	O
record	O
to	O
date	O
and	O
sold	O
over	O
five	O
million	O
copies	O
.	O
The	O
band	O
took	O
the	O
Licensed	ORGANIZATION
to	O
Ill	O
tour	O
around	O
the	O
world	O
the	O
following	O
year	O
.	O
It	O
was	O
a	O
tour	O
clouded	O
in	O
controversy	O
featuring	O
female	O
members	O
of	O
the	O
crowd	O
dancing	O
in	O
cages	O
and	O
a	O
giant	O
motorized	O
inflatable	O
penis	O
similar	O
to	O
one	O
used	O
by	O
The	MISC
Rolling	MISC
Stones	MISC
in	O
the	O
1970s	O
.	O
Rolling	PERSON
Stone	PERSON
would	O
describe	O
the	O
album	O
as	O
"	O
the	MISC
Pet	MISC
Sounds	MISC
/	MISC
The	MISC
Dark	MISC
Side	MISC
of	MISC
the	MISC
Moon	MISC
of	O
hip	O
hop	O
.	O
"	O
Paul	ORGANIZATION
's	ORGANIZATION
Boutique	ORGANIZATION
would	O
eventually	O
sell	O
a	O
million	O
albums	O
,	O
despite	O
the	O
initially	O
weak	O
commercial	O
reception	O
.	O
Beastie	ORGANIZATION
Boys	ORGANIZATION
owned	O
Grand	ORGANIZATION
Royal	ORGANIZATION
Records	ORGANIZATION
until	O
2001	O
when	O
it	O
was	O
then	O
sold	O
for	O
financial	O
reasons	O
.	O
Grand	ORGANIZATION
Royal	ORGANIZATION
's	ORGANIZATION
first	O
independent	O
release	O
was	O
Luscious	PERSON
Jackson	PERSON
's	PERSON
album	O
In	O
Search	ORGANIZATION
of	ORGANIZATION
Manny	ORGANIZATION
in	O
1993	O
.	O
The	MISC
Beastie	MISC
Boys	MISC
also	O
published	O
Grand	ORGANIZATION
Royal	ORGANIZATION
Magazine	ORGANIZATION
,	O
with	O
the	O
first	O
edition	O
in	O
1993	O
featuring	O
a	O
cover	O
story	O
on	O
Bruce	PERSON
Lee	PERSON
,	O
artwork	O
by	O
George	PERSON
Clinton	PERSON
,	O
and	O
interviews	O
with	O
Kareem	PERSON
Abdul-Jabbar	PERSON
and	O
A	O
Tribe	O
Called	O
Quest	MISC
MC	MISC
Q-Tip	MISC
.	O
The	O
OED	PERSON
says	O
that	O
the	O
term	O
was	O
"	O
apparently	O
coined	O
,	O
and	O
certainly	O
popularized	O
,	O
by	O
U.S.	LOCATION
hip-hop	O
group	O
the	MISC
Beastie	MISC
Boys	MISC
.	O
"	O
Beastie	MISC
Boys	MISC
headlined	O
at	O
Lollapalooza	O
--	O
an	O
American	MISC
travelling	O
music	O
festival	O
--	O
in	O
1994	O
,	O
together	O
with	O
The	ORGANIZATION
Smashing	ORGANIZATION
Pumpkins	ORGANIZATION
.	O
In	O
1995	O
,	O
the	O
popularity	O
of	O
Beastie	ORGANIZATION
Boys	ORGANIZATION
was	O
underlined	O
when	O
tickets	O
for	O
an	O
arena	O
tour	O
went	O
on	O
sale	O
in	O
the	O
U.S.	LOCATION
and	O
sold	O
out	O
within	O
a	O
few	O
minutes	O
.	O
The	O
band	O
also	O
released	O
Aglio	ORGANIZATION
e	ORGANIZATION
Olio	ORGANIZATION
,	O
a	O
collection	O
of	O
eight	O
songs	O
lasting	O
just	O
eleven	O
minutes	O
harking	O
back	O
to	O
their	O
punk	O
roots	O
,	O
in	O
1995	O
.	O
Both	O
Michael	PERSON
Diamond	PERSON
and	O
Adam	PERSON
Yauch	PERSON
are	O
credited	O
with	O
interview	O
comments	O
that	O
piqued	O
interest	O
in	O
whether	O
or	O
not	O
an	O
album	O
would	O
be	O
released	O
.	O
Since	O
they	O
had	O
long	O
been	O
notorious	O
for	O
pranking	O
the	O
media	O
,	O
it	O
was	O
difficult	O
for	O
anyone	O
to	O
take	O
these	O
comments	O
seriously	O
until	O
tracks	O
became	O
available	O
,	O
most	O
notably	O
on	O
The	ORGANIZATION
Sounds	ORGANIZATION
of	ORGANIZATION
Science	ORGANIZATION
anthology	O
album	O
.	O
As	O
it	O
started	O
coming	O
back	O
he	O
believed	O
he	O
was	O
a	O
country	O
singer	O
named	O
Country	O
Mike	PERSON
.	O
These	O
comments	O
were	O
made	O
in	O
the	O
wake	O
of	O
the	O
U.S.	O
Embassy	O
bombings	O
that	O
had	O
occurred	O
in	O
both	O
Kenya	LOCATION
and	O
Tanzania	LOCATION
only	O
a	O
month	O
earlier	O
.	O
At	O
the	O
1999	O
ceremony	O
in	O
the	O
wake	O
of	O
the	O
horror	O
stories	O
that	O
were	O
coming	O
out	O
of	O
Woodstock	LOCATION
99	O
,	O
Adam	PERSON
Horovitz	PERSON
addressed	O
the	O
fact	O
that	O
there	O
had	O
been	O
so	O
many	O
cases	O
of	O
sexual	O
assaults	O
and	O
rapes	O
at	O
the	O
festival	O
and	O
the	O
need	O
for	O
bands	O
and	O
festivals	O
to	O
pay	O
much	O
more	O
attention	O
to	O
the	O
security	O
details	O
at	O
their	O
concerts	O
.	O
Beastie	MISC
Boys	MISC
started	O
an	O
arena	O
tour	O
in	O
1998	O
.	O
The	MISC
Beastie	MISC
Boys	MISC
was	O
one	O
of	O
the	O
first	O
bands	O
who	O
made	O
mp3	O
downloads	O
available	O
on	O
their	O
website	O
;	O
they	O
got	O
a	O
high	O
level	O
of	O
response	O
and	O
public	O
awareness	O
as	O
a	O
result	O
including	O
a	O
published	O
article	O
in	O
The	ORGANIZATION
Wall	ORGANIZATION
Street	ORGANIZATION
Journal	ORGANIZATION
on	O
the	O
band	O
's	O
efforts	O
.	O
On	O
September	O
28	O
,	O
1999	O
,	O
Beastie	ORGANIZATION
Boys	ORGANIZATION
joined	O
Elvis	PERSON
Costello	PERSON
to	O
play	O
"	O
Radio	MISC
Radio	MISC
"	O
on	O
the	O
25th	O
anniversary	O
season	O
of	O
Saturday	MISC
Night	MISC
Live	MISC
.	O
In	O
the	O
years	O
following	O
the	O
release	O
of	O
Hello	ORGANIZATION
Nasty	ORGANIZATION
the	O
group	O
launched	O
their	O
official	O
website	O
which	O
underwent	O
several	O
transformations	O
eventually	O
culminating	O
in	O
one	O
of	O
the	O
most	O
popular	O
recording	O
artist	O
related	O
websites	O
on	O
the	O
internet	O
.	O
By	O
the	O
time	O
he	O
recovered	O
,	O
Rage	O
Against	O
the	O
Machine	ORGANIZATION
had	O
disbanded	O
.	O
Hello	O
Nasty	PERSON
was	O
reissued	O
on	O
September	O
22	O
,	O
1998	O
.	O
Beastie	ORGANIZATION
Boys	ORGANIZATION
also	O
headlined	O
the	ORGANIZATION
Coachella	ORGANIZATION
Valley	ORGANIZATION
Music	ORGANIZATION
and	ORGANIZATION
Arts	ORGANIZATION
Festival	ORGANIZATION
.	O
To	O
the	O
5	O
Boroughs	O
was	O
released	O
worldwide	O
on	O
June	O
15	O
,	O
2004	O
.	O
The	O
band	O
subsequently	O
confirmed	O
this	O
in	O
public	O
,	O
playing	O
several	O
tracks	O
from	O
the	O
album	O
at	O
the	O
2007	O
Virgin	O
Festival	O
at	O
Pimlico	MISC
Race	MISC
Course	MISC
in	O
Baltimore	LOCATION
,	O
Md.	LOCATION
.	O
They	O
worked	O
with	O
Reverb	O
,	O
a	O
non-profit	O
environmental	O
organization	O
,	O
on	O
their	O
2007	O
summer	O
tour	O
.	O
Beastie	MISC
Boys	MISC
were	O
featured	O
on	O
the	O
cover	O
of	O
Beyond	ORGANIZATION
Race	ORGANIZATION
magazine	O
for	O
the	O
publication	O
's	O
summer	O
2007	O
issue	O
.	O
In	O
February	O
2009	O
,	O
Yauch	PERSON
revealed	O
their	O
forthcoming	O
new	O
album	O
has	O
taken	O
the	O
band	O
's	O
sound	O
in	O
a	O
"	O
bizarre	O
"	O
new	O
direction	O
,	O
saying	O
"	O
It	O
's	O
a	O
combination	O
of	O
playing	O
and	O
sampling	O
stuff	O
as	O
we	O
're	O
playing	O
,	O
and	O
also	O
sampling	O
pretty	O
obscure	O
records	O
.	O
"	O
The	O
tentative	O
title	O
for	O
the	O
record	O
was	O
Tadlock	ORGANIZATION
's	ORGANIZATION
Glasses	ORGANIZATION
,	O
of	O
which	O
Yauch	PERSON
explained	O
the	O
inspiration	O
behind	O
the	O
title	O
:	O
Mike	PERSON
D	PERSON
also	O
hinted	O
it	O
may	O
be	O
released	O
via	O
unusual	O
means	O
:	O
The	O
group	O
also	O
had	O
to	O
cancel	O
their	O
co-headlining	O
gig	O
at	O
the	MISC
Osheaga	MISC
Festival	MISC
in	O
Montreal	LOCATION
as	O
well	O
as	O
a	O
headlining	O
spot	O
at	O
2009	O
's	O
Lollapalooza	O
.	O
