However	O
,	O
it	O
continued	O
to	O
use	O
native	O
x86	O
execution	O
and	O
ordinary	O
microcode	O
only	O
,	O
like	O
Centaur	ORGANIZATION
's	O
Winchip	LOCATION
,	O
unlike	O
competitors	O
Intel	ORGANIZATION
and	O
AMD	ORGANIZATION
which	O
introduced	O
the	O
method	O
of	O
dynamic	O
translation	O
to	O
micro-operations	O
with	O
Pentium	ORGANIZATION
Pro	ORGANIZATION
and	O
K5	ORGANIZATION
.	O
The	O
lack	O
of	O
full	O
P5	ORGANIZATION
Pentium	ORGANIZATION
compatibility	O
caused	O
problems	O
with	O
some	O
applications	O
because	O
programmers	O
had	O
begun	O
to	O
use	O
P5	ORGANIZATION
Pentium-specific	ORGANIZATION
instructions	O
.	O
This	O
chip	O
was	O
later	O
renamed	O
MII	ORGANIZATION
,	O
to	O
better	O
compete	O
with	O
the	O
Pentium	ORGANIZATION
II	ORGANIZATION
processor	O
.	O
While	O
the	O
6x86	O
's	O
integer	O
performance	O
was	O
significantly	O
higher	O
than	O
P5	ORGANIZATION
Pentium	ORGANIZATION
's	ORGANIZATION
,	O
it	O
's	O
floating	O
point	O
performance	O
was	O
more	O
mediocre	O
--	O
around	O
twice	O
the	O
performance	O
of	O
the	O
486	O
FPU	O
per	O
clock	O
cycle	O
.	O
The	O
FPU	O
in	O
the	O
6x86	LAW
was	O
largely	O
the	O
same	O
circuitry	O
that	O
was	O
developed	O
for	O
Cyrix	ORGANIZATION
's	O
earlier	O
high	O
performance	O
8087/80287/80387-compatible	O
coprocessors	O
,	O
which	O
was	O
very	O
fast	O
for	O
its	O
time	O
--	O
the	O
Cyrix	PERSON
FPU	PERSON
was	O
much	O
faster	O
than	O
the	O
80387	O
,	O
and	O
even	O
the	O
80486	O
FPU	O
.	O
The	O
popularity	O
of	O
the	ORGANIZATION
P5	ORGANIZATION
Pentium	ORGANIZATION
caused	O
many	O
software	O
developers	O
to	O
hand-optimize	O
code	O
in	O
assembly	O
language	O
,	O
to	O
take	O
advantage	O
of	O
the	ORGANIZATION
P5	ORGANIZATION
Pentium	ORGANIZATION
's	ORGANIZATION
tightly	O
pipelined	O
and	O
lower	O
latency	O
FPU	O
.	O
For	O
example	O
,	O
the	O
highly	O
anticipated	O
first	O
person	O
shooter	O
Quake	O
used	O
highly-optimized	O
assembly	O
code	O
designed	O
almost	O
entirely	O
around	O
the	O
P5	O
Pentium	O
's	O
FPU	O
.	O
Fortunately	O
for	O
the	O
6x86	O
(	O
and	O
AMD	MISC
K6	MISC
)	O
,	O
many	O
games	O
continued	O
to	O
be	O
integer-based	O
throughout	O
the	O
chip	O
's	O
lifetime	O
.	O
The	O
6x86	O
successor	O
,	O
MII	ORGANIZATION
,	O
was	O
late	O
to	O
market	O
,	O
and	O
could	O
n't	O
scale	O
well	O
in	O
clock	O
speed	O
with	O
the	O
manufacturing	O
processes	O
used	O
at	O
the	O
time	O
.	O
Similar	O
to	O
the	O
AMD	ORGANIZATION
K5	MISC
,	O
the	O
Cyrix	MISC
6x86	O
was	O
a	O
design	O
far	O
more	O
focused	O
on	O
integer	O
per-clock	O
performance	O
than	O
clock	O
scalability	O
,	O
something	O
that	O
proved	O
to	O
be	O
a	O
strategic	O
mistake	O
.	O
Therefore	O
,	O
despite	O
being	O
very	O
fast	O
clock	O
by	O
clock	O
,	O
the	O
6x86	MISC
and	O
MII	ORGANIZATION
were	O
forced	O
to	O
compete	O
at	O
the	O
low-end	O
of	O
the	O
market	O
as	O
AMD	O
K6	MISC
and	O
Intel	ORGANIZATION
P6	MISC
Pentium	MISC
II	MISC
were	O
always	O
ahead	O
on	O
clock	O
speed	O
.	O
The	O
6x86	O
's	O
and	O
MII	ORGANIZATION
's	O
old	O
generation	O
"	O
486	O
class	O
"	O
floating	O
point	O
unit	O
combined	O
with	O
an	O
integer	O
section	O
that	O
was	O
at	O
best	O
on-par	O
with	O
the	O
newer	O
P6	MISC
and	O
K6	O
chips	O
meant	O
that	O
Cyrix	PERSON
could	O
no	O
longer	O
compete	O
in	O
performance	O
.	O
