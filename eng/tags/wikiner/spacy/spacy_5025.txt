He	O
was	O
dismissed	O
as	O
Prime	O
Minister	O
by	O
the	ORGANIZATION
Governor-General	ORGANIZATION
Sir	O
John	PERSON
Kerr	PERSON
at	O
the	O
climax	O
of	O
the	O
1975	O
Australian	MISC
constitutional	O
crisis	O
;	O
he	O
is	O
the	O
only	O
Prime	O
Minister	O
to	O
have	O
his	O
commission	O
terminated	O
in	O
that	O
manner	O
.	O
Whitlam	ORGANIZATION
entered	O
Parliament	ORGANIZATION
in	O
1952	O
,	O
representing	O
the	ORGANIZATION
Australian	ORGANIZATION
Labor	ORGANIZATION
Party	ORGANIZATION
(	O
ALP	ORGANIZATION
)	O
.	O
In	O
1960	O
he	O
was	O
elected	O
deputy	O
leader	O
of	O
the	O
ALP	ORGANIZATION
and	O
in	O
1967	O
,	O
after	O
party	O
leader	O
Arthur	PERSON
Calwell	PERSON
retired	O
,	O
he	O
assumed	O
the	O
leadership	O
and	O
became	O
Leader	O
of	O
the	O
Opposition	O
.	O
Over	O
a	O
third	O
of	O
a	O
century	O
after	O
he	O
left	O
office	O
,	O
Whitlam	ORGANIZATION
continues	O
to	O
comment	O
on	O
political	O
affairs	O
.	O
Edward	PERSON
Gough	PERSON
Whitlam	PERSON
was	O
born	O
on	O
11	O
July	O
1916	O
in	O
Kew	LOCATION
,	O
a	O
suburb	O
of	O
Melbourne	LOCATION
.	O
After	O
a	O
year	O
there	O
,	O
he	O
attended	O
Mowbray	ORGANIZATION
House	ORGANIZATION
School	ORGANIZATION
and	O
Knox	ORGANIZATION
Grammar	ORGANIZATION
School	ORGANIZATION
,	O
in	O
the	O
suburbs	O
of	O
Sydney	LOCATION
.	O
The	O
position	O
was	O
located	O
in	O
the	O
new	O
national	O
capital	O
of	O
Canberra	LOCATION
,	O
and	O
the	O
Whitlam	ORGANIZATION
family	O
moved	O
there	O
.	O
Gough	O
Whitlam	ORGANIZATION
remains	O
the	O
only	O
Prime	O
Minister	O
to	O
have	O
spent	O
his	O
formative	O
years	O
in	O
Canberra	LOCATION
.	O
Gough	O
,	O
who	O
had	O
always	O
attended	O
a	O
private	O
school	O
,	O
was	O
sent	O
to	O
the	O
government-run	O
Telopea	ORGANIZATION
Park	ORGANIZATION
School	ORGANIZATION
,	O
since	O
no	O
other	O
school	O
was	O
available	O
.	O
Whitlam	ORGANIZATION
enrolled	O
at	O
St.	ORGANIZATION
Paul	ORGANIZATION
's	ORGANIZATION
College	O
at	O
the	ORGANIZATION
University	ORGANIZATION
of	ORGANIZATION
Sydney	ORGANIZATION
at	O
the	O
age	O
of	O
18	O
.	O
Whitlam	ORGANIZATION
trained	O
as	O
a	O
navigator	O
and	O
bomb	O
aimer	O
,	O
before	O
serving	O
with	O
No.	O
13	O
Squadron	O
RAAF	O
,	O
based	O
mainly	O
on	O
the	LOCATION
Gove	LOCATION
Peninsula	LOCATION
,	O
Northern	LOCATION
Territory	LOCATION
,	O
flying	O
Lockheed	ORGANIZATION
Ventura	ORGANIZATION
bombers	O
.	O
While	O
still	O
in	O
uniform	O
,	O
Whitlam	ORGANIZATION
joined	O
the	O
ALP	ORGANIZATION
in	O
Sydney	LOCATION
in	O
1945	O
.	O
With	O
his	O
war	O
service	O
loan	O
,	O
Whitlam	ORGANIZATION
built	O
a	O
house	O
in	O
the	O
seaside	O
town	O
of	O
Cronulla	ORGANIZATION
.	O
He	O
ran	O
twice	O
--	O
unsuccessfully	O
--	O
for	O
the	O
local	O
council	O
,	O
once	O
(	O
also	O
unsuccessfully	O
)	O
for	O
the	ORGANIZATION
New	ORGANIZATION
South	ORGANIZATION
Wales	ORGANIZATION
Legislative	ORGANIZATION
Assembly	ORGANIZATION
,	O
and	O
campaigned	O
for	O
other	O
candidates	O
.	O
Whitlam	ORGANIZATION
won	O
the	O
preselection	O
as	O
ALP	ORGANIZATION
candidate	O
.	O
Whitlam	ORGANIZATION
responded	O
to	O
McEwen	ORGANIZATION
by	O
stating	O
that	O
Benjamin	PERSON
Disraeli	PERSON
had	O
been	O
heckled	O
in	O
his	O
maiden	O
speech	O
,	O
and	O
had	O
responded	O
,	O
"	O
The	O
time	O
will	O
come	O
when	O
you	O
shall	O
hear	O
me	O
"	O
.	O
He	O
told	O
McEwen	ORGANIZATION
,	O
"	O
The	O
time	O
will	O
come	O
when	O
you	O
may	O
interrupt	O
me	O
"	O
.	O
After	O
he	O
stated	O
that	O
future	O
Prime	O
Minister	O
William	PERSON
McMahon	PERSON
was	O
a	O
"	O
quean	O
"	O
,	O
he	O
apologised	O
.	O
In	O
1954	O
,	O
the	O
ALP	ORGANIZATION
seemed	O
likely	O
to	O
return	O
to	O
power	O
.	O
In	O
1955	O
,	O
a	O
redistribution	O
divided	O
Whitlam	ORGANIZATION
's	O
electorate	O
of	O
Werriwa	ORGANIZATION
in	O
two	O
,	O
with	O
his	O
Cronulla	ORGANIZATION
home	O
located	O
in	O
the	O
new	O
electorate	O
of	O
Hughes	ORGANIZATION
.	O
Although	O
Whitlam	ORGANIZATION
would	O
have	O
received	O
ALP	ORGANIZATION
support	O
in	O
either	O
division	O
,	O
he	O
chose	O
to	O
continue	O
standing	O
for	O
Werriwa	ORGANIZATION
,	O
and	O
moved	O
from	O
Cronulla	ORGANIZATION
to	O
Cabramatta	ORGANIZATION
.	O
This	O
meant	O
even	O
longer	O
journeys	O
for	O
his	O
older	O
children	O
to	O
attend	O
school	O
,	O
since	O
neither	O
electorate	O
had	O
a	O
high	O
school	O
at	O
the	O
time	O
,	O
and	O
they	O
attended	O
school	O
in	O
Sydney	LOCATION
.	O
In	O
1960	O
,	O
after	O
losing	O
three	O
elections	O
,	O
Evatt	PERSON
resigned	O
and	O
was	O
replaced	O
by	O
Calwell	ORGANIZATION
,	O
with	O
Whitlam	ORGANIZATION
defeating	O
Ward	ORGANIZATION
for	O
deputy	O
leader	O
.	O
Calwell	ORGANIZATION
's	O
statement	O
was	O
called	O
"	O
crazy	O
and	O
irresponsible	O
"	O
by	O
Prime	O
Minister	O
Menzies	PERSON
,	O
and	O
the	O
incident	O
reduced	O
public	O
support	O
for	O
the	O
ALP	ORGANIZATION
.	O
The	O
party	O
was	O
also	O
defeated	O
in	O
the	O
state	O
elections	O
in	O
the	O
most	O
populous	O
state	O
,	O
New	LOCATION
South	LOCATION
Wales	LOCATION
,	O
surrendering	O
control	O
of	O
the	O
state	O
government	O
for	O
the	O
first	O
time	O
since	O
1941	O
.	O
Whitlam	ORGANIZATION
's	O
relationship	O
with	O
Calwell	ORGANIZATION
,	O
never	O
good	O
,	O
deteriorated	O
further	O
after	O
a	O
1965	O
article	O
in	O
The	O
Australian	MISC
was	O
published	O
.	O
The	O
article	O
reported	O
off-the-record	O
comments	O
Whitlam	ORGANIZATION
had	O
made	O
that	O
his	O
leader	O
was	O
"	O
too	O
old	O
and	O
weak	O
"	O
to	O
win	O
office	O
,	O
and	O
that	O
the	O
party	O
might	O
be	O
gravely	O
damaged	O
by	O
an	O
"	O
old-fashioned	O
"	O
70-year-old	O
Calwell	ORGANIZATION
seeking	O
his	O
first	O
term	O
as	O
Prime	O
Minister	O
.	O
Sir	O
Robert	PERSON
Menzies	PERSON
retired	O
in	O
January	O
1966	O
,	O
and	O
was	O
succeeded	O
as	O
Prime	O
Minister	O
by	O
the	O
new	O
Liberal	ORGANIZATION
Party	ORGANIZATION
leader	O
,	O
Harold	PERSON
Holt	PERSON
.	O
After	O
years	O
of	O
politics	O
being	O
dominated	O
by	O
the	O
elderly	O
Menzies	PERSON
and	O
Calwell	ORGANIZATION
,	O
the	O
younger	O
Holt	PERSON
was	O
seen	O
as	O
a	O
breath	O
of	O
fresh	O
air	O
,	O
and	O
attracted	O
public	O
interest	O
and	O
support	O
in	O
the	O
run-up	O
to	O
the	O
November	O
election	O
.	O
In	O
early	O
1966	O
,	O
the	O
36-member	O
conference	O
,	O
with	O
Calwell	ORGANIZATION
's	O
assent	O
,	O
banned	O
any	O
ALP	ORGANIZATION
parliamentarian	O
from	O
supporting	O
federal	O
assistance	O
to	O
the	O
states	O
for	O
spending	O
on	O
both	O
government	O
and	O
private	O
schools	O
,	O
commonly	O
called	O
"	O
state	O
aid	O
"	O
.	O
Whitlam	ORGANIZATION
broke	O
with	O
the	O
party	O
on	O
the	O
issue	O
,	O
and	O
was	O
charged	O
with	O
gross	O
disloyalty	O
by	O
the	O
Executive	ORGANIZATION
,	O
an	O
offence	O
which	O
carried	O
the	O
penalty	O
of	O
expulsion	O
.	O
Before	O
the	O
matter	O
could	O
be	O
heard	O
,	O
Whitlam	ORGANIZATION
left	O
for	O
Queensland	LOCATION
,	O
where	O
he	O
campaigned	O
intensively	O
for	O
the	O
ALP	ORGANIZATION
candidate	O
in	O
the	O
Dawson	PERSON
by-election	O
.	O
The	O
ALP	ORGANIZATION
won	O
,	O
dealing	O
the	O
government	O
their	O
first	O
by-election	O
defeat	O
since	O
1952	O
.	O
Whitlam	ORGANIZATION
survived	O
the	O
expulsion	O
vote	O
by	O
a	O
margin	O
of	O
only	O
two	O
,	O
gaining	O
both	O
Queensland	ORGANIZATION
votes	O
.	O
At	O
the	O
end	O
of	O
April	O
,	O
Whitlam	ORGANIZATION
challenged	O
Calwell	PERSON
for	O
the	O
leadership	O
;	O
though	O
Calwell	ORGANIZATION
received	O
two-thirds	O
of	O
the	O
vote	O
,	O
the	O
ALP	ORGANIZATION
leader	O
announced	O
that	O
if	O
the	O
party	O
lost	O
the	O
upcoming	O
election	O
,	O
he	O
would	O
not	O
stand	O
again	O
for	O
the	O
leadership	O
.	O
Holt	PERSON
called	O
an	O
election	O
for	O
November	O
1966	O
,	O
in	O
which	O
the	O
Australian	MISC
involvement	O
in	O
the	MISC
Vietnam	MISC
War	MISC
was	O
a	O
major	O
issue	O
.	O
Calwell	ORGANIZATION
called	O
for	O
an	O
"	O
immediate	O
and	O
unconditional	O
withdrawal	O
"	O
of	O
Australian	MISC
troops	O
from	O
Vietnam	LOCATION
.	O
Calwell	ORGANIZATION
considered	O
Whitlam	ORGANIZATION
's	O
remark	O
disastrous	O
,	O
disputing	O
the	O
party	O
line	O
just	O
five	O
days	O
before	O
the	O
election	O
.	O
At	O
the	O
caucus	O
meeting	O
on	O
8	O
February	O
1967	O
,	O
Gough	O
Whitlam	ORGANIZATION
was	O
elected	O
leader	O
of	O
the	O
party	O
,	O
defeating	O
leading	O
left-wing	O
candidate	O
Jim	PERSON
Cairns	PERSON
.	O
Whitlam	ORGANIZATION
saw	O
that	O
the	O
party	O
had	O
little	O
chance	O
of	O
being	O
elected	O
unless	O
it	O
could	O
expand	O
its	O
appeal	O
from	O
the	O
traditional	O
working-class	O
base	O
to	O
include	O
the	O
suburban	O
middle	O
class	O
.	O
He	O
sought	O
to	O
shift	O
control	O
of	O
the	O
ALP	ORGANIZATION
from	O
union	O
officials	O
to	O
the	O
parliamentary	O
party	O
,	O
and	O
hoped	O
that	O
even	O
rank-and-file	O
party	O
members	O
could	O
be	O
given	O
a	O
voice	O
in	O
the	O
conference	O
.	O
Whitlam	ORGANIZATION
resigned	O
the	O
leadership	O
,	O
demanding	O
a	O
vote	O
of	O
confidence	O
from	O
caucus	O
.	O
He	O
defeated	O
Cairns	PERSON
for	O
the	O
leadership	O
in	O
an	O
unexpectedly	O
close	O
38	O
--	O
32	O
vote	O
.	O
With	O
the	O
ALP	ORGANIZATION
's	O
governing	O
bodies	O
unwilling	O
to	O
reform	O
themselves	O
,	O
Whitlam	ORGANIZATION
worked	O
to	O
build	O
support	O
for	O
change	O
among	O
ordinary	O
party	O
members	O
.	O
By	O
the	O
time	O
of	O
the	O
1969	O
party	O
conference	O
,	O
Whitlam	ORGANIZATION
had	O
gained	O
considerable	O
control	O
over	O
the	O
ALP	ORGANIZATION
.	O
Beginning	O
in	O
1965	O
,	O
Whitlam	ORGANIZATION
had	O
sought	O
to	O
change	O
this	O
goal	O
.	O
These	O
federal	O
victories	O
,	O
in	O
which	O
both	O
Whitlam	ORGANIZATION
and	O
Holt	PERSON
campaigned	O
,	O
helped	O
give	O
Whitlam	ORGANIZATION
the	O
leverage	O
he	O
needed	O
to	O
carry	O
out	O
party	O
reforms	O
.	O
At	O
the	O
end	O
of	O
1967	O
,	O
Prime	O
Minister	O
Holt	PERSON
vanished	O
while	O
swimming	O
in	O
rough	O
seas	O
near	O
Melbourne	LOCATION
;	O
his	O
body	O
was	O
never	O
recovered	O
.	O
Senator	O
John	PERSON
Gorton	PERSON
won	O
the	O
vote	O
and	O
became	O
Prime	O
Minister	O
.	O
The	O
leadership	O
campaign	O
was	O
conducted	O
mostly	O
by	O
television	O
,	O
and	O
Gorton	PERSON
appeared	O
to	O
have	O
the	O
visual	O
appeal	O
needed	O
to	O
keep	O
Whitlam	ORGANIZATION
out	O
of	O
office	O
.	O
Whitlam	ORGANIZATION
and	O
the	O
ALP	ORGANIZATION
,	O
with	O
little	O
internal	O
dissension	O
,	O
stood	O
on	O
a	O
platform	O
calling	O
for	O
domestic	O
reform	O
,	O
an	O
end	O
to	O
conscription	O
,	O
and	O
the	O
withdrawal	O
of	O
Australian	MISC
troops	O
from	O
Vietnam	LOCATION
by	O
1	O
July	O
1970	O
.	O
Whitlam	ORGANIZATION
knew	O
that	O
,	O
given	O
the	O
ALP	ORGANIZATION
's	O
poor	O
position	O
after	O
the	O
1966	O
election	O
,	O
victory	O
was	O
unlikely	O
.	O
The	O
Coalition	O
was	O
returned	O
to	O
office	O
with	O
a	O
slim	O
majority	O
.	O
In	O
March	O
1971	O
,	O
Gorton	PERSON
lost	O
a	O
vote	O
of	O
no	O
confidence	O
in	O
the	O
Liberal	ORGANIZATION
caucus	O
.	O
The	O
party	O
's	O
actions	O
,	O
such	O
as	O
its	O
abandonment	O
of	O
the	O
White	LOCATION
Australia	LOCATION
policy	O
,	O
gained	O
favourable	O
media	O
attention	O
.	O
According	O
to	O
Whitlam	ORGANIZATION
biographer	O
Jenny	PERSON
Hocking	PERSON
,	O
the	O
incident	O
transformed	O
Whitlam	ORGANIZATION
into	O
an	O
international	O
statesman	O
,	O
while	O
McMahon	PERSON
was	O
seen	O
as	O
reacting	O
defensively	O
to	O
Whitlam	ORGANIZATION
's	O
foreign	O
policy	O
ventures	O
.	O
Whitlam	ORGANIZATION
performed	O
far	O
better	O
on	O
television	O
than	O
did	O
McMahon	PERSON
,	O
further	O
strengthening	O
his	O
hand	O
.	O
Whitlam	ORGANIZATION
noted	O
that	O
the	O
polling	O
day	O
was	O
the	O
anniversary	O
of	O
the	O
Battle	O
of	O
Austerlitz	O
--	O
at	O
which	O
another	O
"	O
ramshackle	O
,	O
reactionary	O
coalition	O
"	O
had	O
been	O
given	O
a	O
"	O
crushing	O
defeat	O
"	O
.	O
Whitlam	ORGANIZATION
pledged	O
an	O
end	O
to	O
conscription	O
and	O
the	O
release	O
of	O
individuals	O
who	O
had	O
refused	O
the	O
draft	O
,	O
an	O
income	O
tax	O
surcharge	O
to	O
pay	O
for	O
universal	O
health	O
insurance	O
,	O
free	O
dental	O
care	O
for	O
students	O
,	O
and	O
renovation	O
of	O
aging	O
urban	O
infrastructure	O
.	O
The	O
party	O
benefited	O
from	O
the	O
support	O
of	O
the	O
proprietor	O
of	O
News	ORGANIZATION
Limited	ORGANIZATION
,	O
Rupert	PERSON
Murdoch	PERSON
,	O
who	O
preferred	O
Whitlam	ORGANIZATION
over	O
McMahon	PERSON
.	O
However	O
,	O
the	O
ALP	ORGANIZATION
gained	O
little	O
beyond	O
the	O
suburban	O
belts	O
,	O
losing	O
a	O
seat	O
in	O
South	LOCATION
Australia	LOCATION
and	O
two	O
in	O
Western	LOCATION
Australia	LOCATION
.	O
The	O
ALP	ORGANIZATION
parliamentary	O
caucus	O
chose	O
the	O
ministers	O
,	O
but	O
Whitlam	ORGANIZATION
was	O
allowed	O
to	O
assign	O
portfolios	O
.	O
In	O
the	O
meantime	O
,	O
McMahon	PERSON
would	O
remain	O
caretaker	O
Prime	O
Minister	O
.	O
Whitlam	ORGANIZATION
,	O
however	O
,	O
was	O
unwilling	O
to	O
wait	O
that	O
long	O
.	O
During	O
the	O
two	O
weeks	O
the	O
so-called	O
"	O
duumvirate	O
"	O
held	O
office	O
,	O
Whitlam	ORGANIZATION
sought	O
to	O
fulfill	O
those	O
campaign	O
promises	O
that	O
did	O
not	O
require	O
legislation	O
.	O
Barnard	ORGANIZATION
held	O
this	O
office	O
,	O
and	O
exempted	O
everyone	O
.	O
Seven	O
men	O
were	O
at	O
that	O
time	O
incarcerated	O
for	O
refusing	O
conscription	O
;	O
Whitlam	ORGANIZATION
arranged	O
for	O
their	O
freedom	O
.	O
Whitlam	ORGANIZATION
and	O
Barnard	ORGANIZATION
eliminated	O
sales	O
tax	O
on	O
contraceptive	O
pills	O
,	O
announced	O
major	O
grants	O
for	O
the	O
arts	O
,	O
and	O
appointed	O
an	O
interim	O
schools	O
commission	O
.	O
The	O
results	O
were	O
generally	O
acceptable	O
to	O
Whitlam	ORGANIZATION
,	O
and	O
within	O
three	O
hours	O
,	O
he	O
had	O
announced	O
the	O
portfolios	O
of	O
the	O
cabinet	O
members	O
.	O
The	O
Whitlam	ORGANIZATION
government	O
gave	O
grants	O
directly	O
to	O
local	O
government	O
units	O
for	O
urban	O
renewal	O
,	O
flood	O
prevention	O
,	O
and	O
the	O
promotion	O
of	O
tourism	O
.	O
This	O
required	O
Whitlam	ORGANIZATION
's	O
personal	O
permission	O
,	O
which	O
he	O
gave	O
on	O
the	O
condition	O
the	O
price	O
was	O
publicized	O
.	O
In	O
the	O
conservative	O
climate	O
of	O
the	O
time	O
,	O
the	O
purchase	O
created	O
a	O
political	O
and	O
media	O
scandal	O
,	O
and	O
was	O
said	O
to	O
symbolise	O
either	O
Whitlam	ORGANIZATION
's	O
foresight	O
and	O
vision	O
,	O
or	O
his	O
profligate	O
spending	O
.	O
In	O
February	O
1973	O
,	O
the	O
Attorney	O
General	O
,	O
Senator	O
Lionel	PERSON
Murphy	PERSON
,	O
led	O
a	O
police	O
raid	O
on	O
the	O
Melbourne	LOCATION
office	O
of	O
the	ORGANIZATION
Australian	ORGANIZATION
Security	ORGANIZATION
Intelligence	ORGANIZATION
Organisation	ORGANIZATION
,	O
which	O
was	O
under	O
his	O
ministerial	O
responsibility	O
.	O
The	O
Whitlam	ORGANIZATION
government	O
also	O
had	O
troubles	O
in	O
relations	O
with	O
the	O
states	O
.	O
The	O
Whitlam	ORGANIZATION
government	O
had	O
cut	O
tariffs	O
by	O
25	O
percent	O
in	O
1973	O
;	O
1974	O
saw	O
an	O
increase	O
in	O
imports	O
of	O
30	O
percent	O
and	O
a	O
$	O
1.5	O
billion	O
increase	O
in	O
the	O
trade	O
deficit	O
.	O
Whitlam	ORGANIZATION
gave	O
little	O
help	O
to	O
his	O
embattled	O
deputy	O
,	O
who	O
had	O
formed	O
the	O
other	O
half	O
of	O
the	O
duumvirate	O
.	O
The	O
budget	O
was	O
unsuccessful	O
in	O
dealing	O
with	O
the	O
inflation	O
and	O
unemployment	O
,	O
and	O
Whitlam	ORGANIZATION
introduced	O
large	O
tax	O
cuts	O
in	O
November	O
.	O
Beginning	O
in	O
October	O
1974	O
,	O
the	O
Whitlam	ORGANIZATION
government	O
sought	O
overseas	O
loans	O
to	O
finance	O
its	O
development	O
plans	O
,	O
with	O
the	O
newly-enriched	O
oil	O
nations	O
a	O
likely	O
target	O
.	O
Whitlam	ORGANIZATION
appointed	O
Murphy	PERSON
anyway	O
.	O
The	O
New	O
South	LOCATION
Wales	LOCATION
premier	O
,	O
Tom	PERSON
Lewis	PERSON
felt	O
that	O
this	O
convention	O
only	O
applied	O
to	O
vacancies	O
caused	O
by	O
deaths	O
or	O
ill-health	O
,	O
and	O
arranged	O
for	O
the	O
legislature	O
to	O
elect	O
Cleaver	ORGANIZATION
Bunton	ORGANIZATION
,	O
former	O
mayor	O
of	O
Albury	LOCATION
and	O
an	O
independent	O
.	O
Malcolm	PERSON
Fraser	PERSON
challenged	O
Snedden	PERSON
for	O
the	O
leadership	O
,	O
and	O
defeated	O
him	O
on	O
21	O
March	O
.	O
Laurie	PERSON
Brereton	PERSON
,	O
foreign	O
affairs	O
spokesman	O
under	O
Whitlam	ORGANIZATION
,	O
stated	O
in	O
1999	O
that	O
Whitlam	ORGANIZATION
's	O
policy	O
was	O
at	O
best	O
"	O
dangerously	O
ambiguous	O
"	O
;	O
she	O
was	O
subsequently	O
mocked	O
by	O
Whitlam	ORGANIZATION
.	O
The	O
state	O
Labor	O
party	O
nominated	O
Mal	PERSON
Colston	PERSON
,	O
resulting	O
in	O
a	O
deadlock	O
.	O
The	O
unicameral	O
Queensland	LOCATION
legislature	O
twice	O
voted	O
against	O
Colston	PERSON
,	O
and	O
the	O
party	O
refused	O
to	O
submit	O
any	O
alternative	O
candidates	O
.	O
The	O
Coalition	O
believed	O
that	O
if	O
Whitlam	ORGANIZATION
could	O
not	O
deliver	O
supply	O
,	O
and	O
would	O
not	O
advise	O
new	O
elections	O
,	O
Kerr	ORGANIZATION
would	O
have	O
to	O
dismiss	O
him	O
.	O
The	O
continuing	O
scandal	O
confirmed	O
the	O
Coalition	O
in	O
their	O
stance	O
that	O
they	O
would	O
not	O
concede	O
supply	O
.	O
The	O
Coalition	O
senators	O
tried	O
to	O
remain	O
united	O
,	O
as	O
several	O
became	O
increasingly	O
concerned	O
about	O
the	O
tactic	O
of	O
blocking	O
supply	O
.	O
As	O
the	O
crisis	O
dragged	O
into	O
November	O
,	O
Whitlam	ORGANIZATION
attempted	O
to	O
make	O
arrangements	O
for	O
public	O
servants	O
and	O
suppliers	O
to	O
be	O
able	O
to	O
cash	O
cheques	O
at	O
banks	O
.	O
Governor-General	O
Kerr	PERSON
was	O
following	O
the	O
crisis	O
closely	O
.	O
With	O
the	O
crisis	O
unresolved	O
,	O
on	O
6	O
November	O
,	O
Kerr	ORGANIZATION
decided	O
to	O
dismiss	O
Whitlam	ORGANIZATION
as	O
Prime	O
Minister	O
.	O
A	O
meeting	O
among	O
the	O
party	O
leaders	O
,	O
including	O
Whitlam	ORGANIZATION
and	O
Fraser	PERSON
,	O
to	O
resolve	O
the	O
crisis	O
on	O
the	O
morning	O
of	O
11	O
November	O
came	O
to	O
nothing	O
.	O
Kerr	PERSON
and	O
Whitlam	ORGANIZATION
met	O
at	O
the	ORGANIZATION
Governor-General	ORGANIZATION
's	ORGANIZATION
office	O
that	O
afternoon	O
at	O
1.00	O
pm	O
.	O
Unknown	O
to	O
Whitlam	ORGANIZATION
,	O
Fraser	PERSON
was	O
waiting	O
in	O
an	O
ante-room	O
;	O
Whitlam	ORGANIZATION
later	O
stated	O
that	O
he	O
would	O
not	O
have	O
set	O
foot	O
in	O
the	O
building	O
if	O
he	O
had	O
known	O
Fraser	PERSON
was	O
there	O
.	O
Kerr	PERSON
instead	O
told	O
Whitlam	ORGANIZATION
that	O
he	O
had	O
terminated	O
his	O
commission	O
as	O
Prime	O
Minister	O
,	O
and	O
handed	O
him	O
a	O
letter	O
to	O
that	O
effect	O
.	O
After	O
the	O
conversation	O
,	O
Whitlam	ORGANIZATION
returned	O
to	O
the	O
Prime	O
Minister	O
's	O
residence	O
,	O
The	O
Lodge	ORGANIZATION
,	O
had	O
lunch	O
and	O
conferred	O
with	O
his	O
advisers	O
.	O
Immediately	O
after	O
his	O
meeting	O
with	O
Whitlam	ORGANIZATION
,	O
Kerr	PERSON
commissioned	O
Fraser	PERSON
as	O
caretaker	O
Prime	O
Minister	O
,	O
on	O
the	O
assurance	O
he	O
could	O
obtain	O
supply	O
and	O
would	O
then	O
advise	O
Kerr	PERSON
to	O
dissolve	O
both	O
houses	O
for	O
election	O
.	O
As	O
the	O
ALP	ORGANIZATION
began	O
the	O
1975	O
race	O
,	O
it	O
seemed	O
that	O
its	O
supporters	O
would	O
maintain	O
their	O
rage	O
.	O
Early	O
rallies	O
saw	O
huge	O
crowds	O
,	O
with	O
attendees	O
handing	O
Whitlam	ORGANIZATION
money	O
to	O
pay	O
election	O
expenses	O
.	O
Fraser	PERSON
's	O
appearances	O
saw	O
protests	O
,	O
and	O
a	O
letter	O
bomb	O
sent	O
to	O
Kerr	PERSON
was	O
defused	O
by	O
authorities	O
.	O
Instead	O
of	O
making	O
a	O
policy	O
speech	O
to	O
keynote	O
his	O
campaign	O
,	O
Whitlam	ORGANIZATION
made	O
a	O
speech	O
attacking	O
his	O
opponents	O
and	O
calling	O
11	O
November	O
"	O
a	O
day	O
which	O
will	O
live	O
in	O
infamy	O
"	O
.	O
Whitlam	ORGANIZATION
's	O
campaign	O
disbelieved	O
the	O
results	O
at	O
first	O
,	O
but	O
additional	O
polling	O
were	O
clear	O
:	O
the	O
electorate	O
was	O
turning	O
against	O
the	O
ALP	ORGANIZATION
.	O
The	O
ALP	ORGANIZATION
campaign	O
,	O
which	O
had	O
concentrated	O
on	O
the	O
issue	O
of	O
Whitlam	ORGANIZATION
's	O
dismissal	O
,	O
did	O
not	O
address	O
the	O
economy	O
until	O
its	O
final	O
days	O
.	O
By	O
that	O
time	O
Fraser	PERSON
,	O
confident	O
of	O
victory	O
,	O
was	O
content	O
to	O
sit	O
back	O
,	O
avoid	O
specifics	O
and	O
make	O
no	O
mistakes	O
.	O
Whitlam	ORGANIZATION
stayed	O
on	O
as	O
Opposition	O
Leader	O
,	O
defeating	O
a	O
leadership	O
challenge	O
.	O
The	O
Whitlams	PERSON
were	O
visiting	O
China	LOCATION
at	O
the	O
time	O
of	O
the	O
Tangshan	LOCATION
earthquake	O
in	O
July	O
1976	O
,	O
though	O
they	O
were	O
staying	O
in	O
Tianjin	LOCATION
,	O
90	O
miles	O
away	O
from	O
the	O
epicentre	O
.	O
In	O
early	O
1977	O
,	O
Whitlam	ORGANIZATION
faced	O
a	O
leadership	O
challenge	O
from	O
his	O
final	O
Treasurer	PERSON
,	O
Bill	PERSON
Hayden	PERSON
,	O
and	O
won	O
by	O
a	O
two-vote	O
margin	O
.	O
According	O
to	O
Freudenberg	PERSON
,	O
"	O
The	O
meaning	O
and	O
the	O
message	O
were	O
unmistakable	O
.	O
It	O
was	O
the	O
Australian	MISC
people	O
's	O
rejection	O
of	O
Edward	PERSON
Gough	PERSON
Whitlam	PERSON
.	O
"	O
Shortly	O
after	O
the	O
election	O
,	O
Whitlam	ORGANIZATION
resigned	O
as	O
party	O
leader	O
and	O
was	O
succeeded	O
by	O
Hayden	PERSON
.	O
He	O
served	O
for	O
three	O
years	O
in	O
this	O
post	O
,	O
defending	O
UNESCO	ORGANIZATION
against	O
allegations	O
of	O
corruption	O
.	O
In	O
1995	O
,	O
Gough	PERSON
and	O
Margaret	PERSON
Whitlam	PERSON
were	O
part	O
of	O
the	O
bid	O
team	O
which	O
was	O
successful	O
in	O
getting	O
the	ORGANIZATION
International	ORGANIZATION
Olympic	ORGANIZATION
Committee	ORGANIZATION
to	O
host	O
the	O
2000	O
Summer	MISC
Olympics	MISC
in	O
Sydney	LOCATION
.	O
Kerr	PERSON
died	O
in	O
1991	O
;	O
he	O
and	O
Whitlam	ORGANIZATION
never	O
reconciled	O
.	O
In	O
March	O
2010	O
,	O
Fraser	PERSON
visited	O
Whitlam	ORGANIZATION
at	O
his	O
Sydney	LOCATION
office	O
while	O
on	O
a	O
book	O
tour	O
to	O
promote	O
his	O
memoirs	O
.	O
In	O
2003	O
,	O
Mark	PERSON
Latham	PERSON
became	O
the	O
leader	O
of	O
the	O
ALP	ORGANIZATION
.	O
Although	O
Latham	PERSON
was	O
more	O
conservative	O
than	O
Whitlam	ORGANIZATION
,	O
the	O
former	O
Prime	O
Minister	O
gave	O
Latham	PERSON
much	O
support	O
,	O
according	O
to	O
one	O
account	O
"	O
anointing	O
him	O
as	O
his	O
political	O
heir	O
"	O
.	O
In	O
2006	O
,	O
he	O
accused	O
the	O
ALP	ORGANIZATION
of	O
failing	O
to	O
press	O
for	O
this	O
change	O
.	O
In	O
April	O
2007	O
,	O
Gough	PERSON
and	O
Margaret	PERSON
Whitlam	PERSON
were	O
made	O
life	O
members	O
of	O
the	ORGANIZATION
Australian	ORGANIZATION
Labor	ORGANIZATION
Party	ORGANIZATION
.	O
The	O
former	O
Prime	O
Minister	O
also	O
alleged	O
that	O
Shackleton	PERSON
was	O
"	O
culpable	O
"	O
if	O
Shackleton	PERSON
had	O
not	O
passed	O
on	O
Whitlam	ORGANIZATION
's	O
warning	O
.	O
The	O
Whitlams	PERSON
have	O
now	O
been	O
married	O
for	O
more	O
than	O
two-thirds	O
of	O
a	O
century	O
.	O
On	O
the	O
60th	O
anniversary	O
of	O
their	O
marriage	O
,	O
Gough	O
Whitlam	O
called	O
it	O
"	O
very	O
satisfactory	O
"	O
and	O
claimed	O
a	O
record	O
for	O
"	O
matrimonial	O
endurance	O
"	O
.	O
In	O
2010	O
,	O
it	O
was	O
reported	O
that	O
Gough	O
Whitlam	ORGANIZATION
had	O
moved	O
into	O
an	O
aged	O
care	O
facility	O
in	O
Sydney	LOCATION
's	O
inner	O
east	O
in	O
2007	O
.	O
Margaret	PERSON
Whitlam	PERSON
remains	O
in	O
the	O
couple	O
's	O
nearby	O
apartment	O
.	O
Now	O
well	O
into	O
his	O
nineties	O
,	O
Whitlam	ORGANIZATION
remains	O
well	O
remembered	O
for	O
the	O
circumstances	O
of	O
his	O
dismissal	O
.	O
According	O
to	O
journalist	O
and	O
author	O
Paul	PERSON
Kelly	PERSON
,	O
who	O
penned	O
two	O
books	O
on	O
the	O
crisis	O
,	O
Whitlam	ORGANIZATION
has	O
"	O
achieved	O
a	O
paradoxical	O
triumph	O
:	O
the	O
shadow	O
of	O
the	O
dismissal	O
has	O
obscured	O
the	O
sins	O
of	O
his	O
government	O
"	O
.	O
According	O
to	O
Whitlam	ORGANIZATION
biographer	O
Jenny	PERSON
Hocking	PERSON
,	O
for	O
a	O
period	O
of	O
at	O
least	O
a	O
decade	O
,	O
the	O
Whitlam	ORGANIZATION
era	O
was	O
viewed	O
almost	O
entirely	O
in	O
negative	O
terms	O
,	O
but	O
that	O
has	O
changed	O
.	O
