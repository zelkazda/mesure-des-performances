The	ORGANIZATION
European	ORGANIZATION
Union	ORGANIZATION
calls	O
it	O
the	O
legal	O
age	O
for	O
sexual	O
activities	O
.	O
Further	O
still	O
,	O
some	O
jurisdictions	O
prohibit	O
any	O
sex	O
outside	O
of	O
marriage	O
,	O
which	O
can	O
take	O
place	O
at	O
any	O
age	O
,	O
as	O
in	O
the	O
case	O
of	O
Yemen	LOCATION
.	O
There	O
are	O
no	O
specific	O
age	O
of	O
consent	O
laws	O
in	O
the	O
Antarctic	LOCATION
.	O
In	O
the	O
unlikely	O
event	O
of	O
a	O
minor	O
engaging	O
in	O
sexual	O
activity	O
,	O
under	O
the	LAW
Antarctic	LAW
Treaty	LAW
,	O
scientists	O
and	O
support	O
staff	O
stationed	O
there	O
may	O
be	O
subject	O
to	O
the	O
laws	O
of	O
the	O
country	O
of	O
which	O
they	O
are	O
nationals	O
.	O
