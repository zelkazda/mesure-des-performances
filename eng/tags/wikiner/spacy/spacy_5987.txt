Ireland	LOCATION
has	O
,	O
throughout	O
most	O
of	O
its	O
history	O
,	O
had	O
a	O
relatively	O
small	O
population;	O
[	O
citation	O
needed	O
]	O
until	O
the	O
19th	O
century	O
this	O
was	O
comparable	O
to	O
other	O
regions	O
of	O
similar	O
area	O
in	O
Europe	LOCATION
.	O
Only	O
in	O
the	O
mid-20th	O
century	O
did	O
the	O
Republic	O
's	O
population	O
start	O
to	O
grow	O
once	O
more	O
,	O
but	O
emigration	O
was	O
still	O
common	O
until	O
the	O
1990s	O
.	O
Ireland	LOCATION
is	O
home	O
to	O
people	O
from	O
all	O
over	O
the	O
globe	O
,	O
especially	O
in	O
Dublin	LOCATION
.	O
These	O
countries	O
include	O
Poland	LOCATION
,	O
Great	LOCATION
Britain	LOCATION
,	O
China	LOCATION
,	O
India	LOCATION
,	O
Brazil	LOCATION
,	O
Nigeria	LOCATION
,	O
and	O
Russia	LOCATION
.	O
Figures	O
from	O
the	O
CSO	ORGANIZATION
.	O
Irish	MISC
is	O
the	O
main	O
language	O
of	O
the	O
Gaeltacht	ORGANIZATION
regions	O
,	O
where	O
91,862	O
people	O
live	O
.	O
The	O
main	O
sign	O
language	O
used	O
is	O
Irish	ORGANIZATION
Sign	ORGANIZATION
Language	ORGANIZATION
.	O
