A	O
Byzantine	MISC
nobleman	O
,	O
he	O
had	O
risen	O
to	O
the	O
court	O
position	O
of	O
protovestiarios	O
by	O
the	O
time	O
of	O
the	MISC
Fourth	MISC
Crusade	MISC
.	O
He	O
had	O
been	O
married	O
twice	O
but	O
was	O
now	O
allegedly	O
the	O
lover	O
of	O
Eudokia	PERSON
Angelina	PERSON
,	O
a	O
daughter	O
of	O
Emperor	O
Alexios	PERSON
III	PERSON
Angelos	PERSON
.	O
His	O
participation	O
in	O
the	O
attempted	O
usurpation	O
of	O
John	PERSON
Komnenos	PERSON
the	O
Fat	O
in	O
1200	O
had	O
caused	O
him	O
to	O
be	O
imprisoned	O
until	O
the	O
accession	O
of	O
Isaac	PERSON
II	PERSON
Angelos	PERSON
,	O
who	O
was	O
restored	O
to	O
the	O
throne	O
after	O
having	O
been	O
deposed	O
and	O
imprisoned	O
by	O
his	O
brother	O
Alexios	PERSON
III	PERSON
,	O
and	O
his	O
son	O
Alexios	PERSON
IV	PERSON
Angelos	PERSON
,	O
who	O
were	O
placed	O
on	O
the	O
throne	O
by	O
the	O
intervention	O
of	O
the	MISC
Fourth	MISC
Crusade	MISC
in	O
July	O
1203	O
.	O
When	O
the	O
populace	O
rebelled	O
in	O
late	O
January	O
1204	O
,	O
the	O
emperors	O
barricaded	O
themselves	O
in	O
the	O
palace	O
and	O
entrusted	O
Alexios	PERSON
Doukas	PERSON
with	O
a	O
mission	O
to	O
seek	O
help	O
from	O
the	O
crusaders	O
.	O
Instead	O
,	O
Alexios	PERSON
Doukas	PERSON
used	O
his	O
access	O
to	O
the	O
palace	O
to	O
arrest	O
the	O
emperors	O
.	O
Alexios	PERSON
V	PERSON
Doukas	PERSON
was	O
crowned	O
in	O
early	O
February	O
1204	O
.	O
The	O
crusaders	O
'	O
second	O
attack	O
proved	O
too	O
strong	O
to	O
repel	O
,	O
and	O
Alexios	PERSON
V	PERSON
fled	O
into	O
Thrace	LOCATION
on	O
the	O
night	O
of	O
12	O
April	O
1204	O
,	O
accompanied	O
by	O
Eudokia	PERSON
Angelina	PERSON
and	O
her	O
mother	O
Euphrosyne	PERSON
Doukaina	PERSON
Kamatera	LOCATION
.	O
The	O
refugees	O
reached	O
Mosynopolis	PERSON
,	O
the	O
base	O
of	O
the	O
deposed	O
emperor	O
Alexios	PERSON
III	PERSON
Angelos	PERSON
,	O
where	O
they	O
were	O
initially	O
well	O
received	O
,	O
and	O
Alexios	PERSON
V	PERSON
married	O
Eudokia	PERSON
Angelina	PERSON
.	O
Later	O
,	O
however	O
,	O
Alexios	PERSON
III	PERSON
arranged	O
for	O
his	O
new	O
son-in-law	O
to	O
be	O
ambushed	O
and	O
blinded	O
,	O
making	O
him	O
ineligible	O
for	O
the	O
imperial	O
throne	O
.	O
