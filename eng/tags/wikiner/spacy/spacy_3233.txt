The	O
basic	O
principle	O
of	O
operation	O
of	O
thermionic	O
diodes	O
was	O
discovered	O
by	O
Frederick	PERSON
Guthrie	PERSON
in	O
1873	O
.	O
Guthrie	PERSON
discovered	O
that	O
a	O
positively	O
charged	O
electroscope	O
could	O
be	O
discharged	O
by	O
bringing	O
a	O
grounded	O
piece	O
of	O
white-hot	O
metal	O
close	O
to	O
it	O
(	O
but	O
not	O
actually	O
touching	O
it	O
)	O
.	O
Thomas	PERSON
Edison	PERSON
independently	O
rediscovered	O
the	O
principle	O
on	O
February	O
13	O
,	O
1880	O
.	O
Braun	PERSON
patented	O
the	O
crystal	O
rectifier	O
in	O
1899	O
.	O
Braun	PERSON
's	O
discovery	O
was	O
further	O
developed	O
by	O
Jagadish	PERSON
Chandra	PERSON
Bose	PERSON
into	O
a	O
useful	O
device	O
for	O
radio	O
detection	O
.	O
The	O
first	O
actual	O
radio	O
receiver	O
using	O
a	O
crystal	O
diode	O
was	O
built	O
by	O
Greenleaf	ORGANIZATION
Whittier	ORGANIZATION
Pickard	ORGANIZATION
.	O
Pickard	ORGANIZATION
received	O
a	O
patent	O
for	O
a	O
silicon	O
crystal	O
detector	O
on	O
November	O
20	O
,	O
1906	O
.	O
The	O
reverse	O
breakdown	O
region	O
is	O
not	O
modeled	O
by	O
the	O
Shockley	ORGANIZATION
diode	O
equation	O
.	O
