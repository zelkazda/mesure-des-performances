The	O
project	O
was	O
founded	O
by	O
George	PERSON
Woltman	PERSON
,	O
who	O
also	O
wrote	O
the	O
software	O
Prime95	O
and	O
MPrime	MISC
for	O
the	O
project	O
.	O
This	O
prime	O
allowed	O
GIMPS	PERSON
to	O
win	O
the	O
$	O
100,000	O
prize	O
from	O
Electronic	ORGANIZATION
Frontier	ORGANIZATION
Foundation	ORGANIZATION
for	O
discovering	O
a	O
prime	O
with	O
more	O
than	O
10	O
million	O
decimal	O
digits	O
.	O
John	PERSON
Pollard	PERSON
's	PERSON
p	O
−	O
1	O
algorithm	O
is	O
also	O
used	O
to	O
search	O
for	O
larger	O
factors	O
.	O
Although	O
the	O
GIMPS	PERSON
software	O
's	O
source	O
code	O
is	O
publicly	O
available	O
,	O
technically	O
it	O
is	O
not	O
free	O
software	O
,	O
since	O
it	O
has	O
a	O
restriction	O
that	O
users	O
must	O
abide	O
by	O
the	O
project	O
's	O
distribution	O
terms	O
if	O
the	O
software	O
is	O
used	O
to	O
discover	O
a	O
prime	O
number	O
with	O
at	O
least	O
100	O
million	O
decimal	O
digits	O
and	O
wins	O
the	O
$	O
150	O
,000	O
bounty	O
offered	O
by	O
the	ORGANIZATION
Electronic	ORGANIZATION
Frontier	ORGANIZATION
Foundation	ORGANIZATION
.	O
