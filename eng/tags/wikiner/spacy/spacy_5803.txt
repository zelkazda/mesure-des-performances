Between	O
1500	O
and	O
2009	O
,	O
875	O
extinctions	O
have	O
been	O
documented	O
by	O
the	ORGANIZATION
International	ORGANIZATION
Union	ORGANIZATION
for	ORGANIZATION
Conservation	ORGANIZATION
of	ORGANIZATION
Nature	ORGANIZATION
and	ORGANIZATION
Natural	ORGANIZATION
Resources	ORGANIZATION
.	O
In	O
1998	O
the	ORGANIZATION
American	ORGANIZATION
Museum	ORGANIZATION
of	ORGANIZATION
Natural	ORGANIZATION
History	ORGANIZATION
conducted	O
a	O
poll	O
of	O
biologists	O
that	O
revealed	O
that	O
the	O
vast	O
majority	O
of	O
biologists	O
believe	O
that	O
we	O
are	O
in	O
the	O
midst	O
of	O
an	O
anthropogenic	O
extinction	O
.	O
