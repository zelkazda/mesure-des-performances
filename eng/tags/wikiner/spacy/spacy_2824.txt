In	O
the	O
9th	O
century	O
,	O
the	O
non-manual	O
crank	O
appears	O
in	O
several	O
of	O
the	O
hydraulic	O
machines	O
described	O
by	O
the	O
Banu	MISC
Musa	O
brothers	O
in	O
their	MISC
Book	MISC
of	MISC
Ingenious	MISC
Devices	MISC
.	O
In	O
reality	O
,	O
however	O
,	O
these	O
devices	O
made	O
only	O
partial	O
rotations	O
and	O
could	O
only	O
be	O
lightly	O
loaded	O
,	O
while	O
the	O
historian	O
of	O
technology	O
Lynn	PERSON
White	PERSON
did	O
not	O
classify	O
them	O
even	O
as	O
the	O
simplest	O
application	O
of	O
a	O
crank	O
.	O
The	O
first	O
known	O
use	O
of	O
a	O
crankshaft	O
in	O
a	O
chain	O
pump	O
was	O
in	O
one	O
of	O
Al-Jazari	ORGANIZATION
's	ORGANIZATION
(	O
1136	O
--	O
1206	O
)	O
saqiya	O
machines	O
.	O
Al-Jazari	PERSON
described	O
a	O
crank	O
and	O
connecting	O
rod	O
system	O
in	O
a	O
rotating	O
machine	O
in	O
two	O
of	O
his	O
water-raising	O
machines	O
.	O
Al-Jazari	ORGANIZATION
's	ORGANIZATION
suction	O
piston	O
pump	O
could	O
lift	O
13.6	O
m	O
(	O
45	O
ft	O
)	O
of	O
water	O
,	O
with	O
the	O
help	O
of	O
delivery	O
pipes	O
.	O
The	ORGANIZATION
Luttrell	ORGANIZATION
Psalter	ORGANIZATION
,	O
dating	O
to	O
around	O
1340	O
,	O
describes	O
a	O
grindstone	O
which	O
was	O
rotated	O
by	O
two	O
cranks	O
,	O
one	O
at	O
each	O
end	O
of	O
its	O
axle	O
;	O
the	O
geared	O
hand-mill	O
,	O
operated	O
either	O
with	O
one	O
or	O
two	O
cranks	O
,	O
appeared	O
later	O
in	O
the	O
15th	O
century	O
;	O
Taqi	PERSON
al-Din	PERSON
incorporated	O
a	O
crankshaft	O
in	O
a	O
six-cylinder	O
pump	O
in	O
1551	O
.	O
A	O
sound	O
grasp	O
of	O
the	O
crank	O
motion	O
involved	O
demonstrates	O
a	O
little	O
later	O
Pisanello	ORGANIZATION
who	O
painted	O
a	O
piston-pump	O
driven	O
by	O
a	O
water-wheel	O
and	O
operated	O
by	O
two	O
simple	O
cranks	O
and	O
two	O
connecting-rods	O
.	O
For	O
this	O
reason	O
,	O
even	O
such	O
high	O
speed	O
production	O
engines	O
as	O
current	O
Honda	ORGANIZATION
engines	O
are	O
classified	O
as	O
"	O
under	O
square	O
"	O
or	O
long-stroke	O
,	O
in	O
that	O
the	O
stroke	O
is	O
longer	O
than	O
the	O
diameter	O
of	O
the	O
cylinder	O
bore	O
.	O
The	O
same	O
engine	O
,	O
however	O
,	O
can	O
be	O
made	O
to	O
provide	O
evenly	O
spaced	O
power	O
pulses	O
by	O
using	O
a	O
crankshaft	O
with	O
an	O
individual	O
crank	O
throw	O
for	O
each	O
cylinder	O
,	O
spaced	O
so	O
that	O
the	O
pistons	O
are	O
actually	O
phased	O
120°	O
apart	O
,	O
as	O
in	O
the	O
GM	ORGANIZATION
3800	MISC
engine	O
.	O
The	O
difference	O
can	O
be	O
heard	O
as	O
the	O
flat-plane	O
crankshafts	O
result	O
in	O
the	O
engine	O
having	O
a	O
smoother	O
,	O
higher-pitched	O
sound	O
than	O
cross-plane	O
(	O
for	O
example	O
,	O
IRL	ORGANIZATION
IndyCar	O
Series	O
compared	O
to	O
NASCAR	ORGANIZATION
Nextel	ORGANIZATION
Cup	ORGANIZATION
,	O
or	O
a	O
Ferrari	ORGANIZATION
355	MISC
compared	O
to	O
a	O
Chevrolet	ORGANIZATION
Corvette	MISC
)	O
.	O
