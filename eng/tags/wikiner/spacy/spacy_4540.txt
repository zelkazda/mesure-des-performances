Foster	ORGANIZATION
's	ORGANIZATION
Lager	ORGANIZATION
is	O
an	O
internationally	O
distributed	O
Australian	MISC
brand	O
of	O
4.9	O
%	O
abv	O
pale	O
lager	O
(	O
4	O
%	O
in	O
the	O
UK	LOCATION
)	O
.	O
It	O
is	O
a	O
product	O
of	O
Foster	ORGANIZATION
's	ORGANIZATION
Group	ORGANIZATION
brewed	O
under	O
licence	O
in	O
several	O
countries	O
,	O
including	O
the	LOCATION
United	LOCATION
States	LOCATION
,	O
and	O
Russia	LOCATION
.	O
The	O
Foster	ORGANIZATION
's	O
brand	O
is	O
also	O
used	O
on	O
several	O
other	O
beers	O
.	O
While	O
international	O
marketing	O
of	O
the	O
beer	O
often	O
focuses	O
on	O
its	O
Australian	MISC
connections	O
,	O
Foster	PERSON
's	O
does	O
not	O
enjoy	O
widespread	O
popularity	O
in	O
Australia	LOCATION
.	O
Foster	ORGANIZATION
's	ORGANIZATION
Lager	ORGANIZATION
was	O
imported	O
into	O
the	O
UK	LOCATION
from	O
Australia	LOCATION
in	O
its	O
distinctly	O
blue	O
,	O
white	O
and	O
gold	O
cans	O
since	O
the	O
early	O
1970s	O
.	O
While	O
popular	O
in	O
many	O
countries	O
,	O
particularly	O
where	O
it	O
is	O
brewed	O
locally	O
,	O
Foster	ORGANIZATION
's	ORGANIZATION
Lager	ORGANIZATION
does	O
not	O
enjoy	O
widespread	O
success	O
in	O
Australia	LOCATION
.	O
As	O
a	O
bottled	O
beer	O
produced	O
by	O
the	O
Foster	ORGANIZATION
's	ORGANIZATION
Group	ORGANIZATION
)	O
it	O
has	O
rarely	O
been	O
promoted	O
in	O
Australia	LOCATION
since	O
the	O
early	O
2000s	O
.	O
In	O
Australia	LOCATION
until	O
the	O
end	O
of	O
the	O
1970s	O
,	O
Foster	ORGANIZATION
's	ORGANIZATION
Lager	ORGANIZATION
was	O
a	O
reasonably	O
popular	O
bottled	O
and	O
canned	O
beer	O
with	O
a	O
somewhat	O
premium	O
image	O
.	O
Arguably	O
,	O
at	O
the	O
end	O
of	O
this	O
failed	O
exercise	O
Foster	ORGANIZATION
's	ORGANIZATION
Lager	ORGANIZATION
was	O
no	O
longer	O
viewed	O
by	O
consumers	O
as	O
a	O
"	O
premium	O
"	O
brand	O
,	O
and	O
has	O
not	O
been	O
promoted	O
in	O
Australia	LOCATION
recently	O
.	O
The	O
Foster	ORGANIZATION
's	ORGANIZATION
Group	ORGANIZATION
has	O
tended	O
to	O
promote	O
the	O
brands	O
of	O
Carlton	ORGANIZATION
Draught	O
(	O
mainstream	O
market	O
)	O
and	O
Victoria	PERSON
Bitter	PERSON
(	O
working	O
class	O
male	O
market	O
)	O
.	O
As	O
an	O
"	O
also-ran	O
"	O
,	O
Foster	ORGANIZATION
's	ORGANIZATION
Lager	ORGANIZATION
will	O
no	O
doubt	O
be	O
brewed	O
as	O
a	O
bottled	O
and	O
canned	O
beer	O
in	O
Australia	LOCATION
for	O
the	O
foreseeable	O
future,	O
[	O
citation	O
needed	O
]	O
at	O
least	O
for	O
sentimental	O
reasons	O
.	O
In	O
2008	O
,	O
Foster	ORGANIZATION
's	ORGANIZATION
was	O
introduced	O
with	O
a	O
widget	O
called	O
a	O
"	O
scuba	O
"	O
placed	O
into	O
the	O
can	O
to	O
ensure	O
good	O
mixing	O
.	O
In	O
the	O
UK	LOCATION
,	O
customers	O
are	O
also	O
able	O
to	O
purchase	O
a	O
keg	O
of	O
Foster	PERSON
's	O
for	O
private	O
parties	O
,	O
collecting	O
and	O
returning	O
the	O
keg	O
at	O
a	O
participating	O
store	O
or	O
public	O
house	O
.	O
The	O
overseas	O
advertising	O
of	O
the	O
product	O
often	O
focuses	O
upon	O
the	O
Australian	MISC
connotations	O
of	O
the	O
beer	O
,	O
e.g.	O
with	O
reference	O
to	O
stereotypical	O
Australian	MISC
imagery	O
such	O
as	O
kangaroos	O
,	O
exaggerated	O
accents	O
,	O
and	O
cork	O
hats	O
.	O
The	O
Foster	ORGANIZATION
's	ORGANIZATION
Lager	ORGANIZATION
brand	O
was	O
used	O
as	O
an	O
advertising	O
sponsorship	O
deal	O
with	O
Norwich	ORGANIZATION
City	ORGANIZATION
Football	ORGANIZATION
Club	ORGANIZATION
from	O
1986	O
to	O
1989	O
.	O
At	O
its	O
commencement	O
,	O
the	O
sponsorship	O
by	O
Foster	ORGANIZATION
's	ORGANIZATION
was	O
the	O
most	O
lucrative	O
sponsorship	O
ever	O
given	O
to	O
an	O
English	MISC
football	O
club	O
.	O
The	O
brand	O
was	O
also	O
used	O
in	O
a	O
sponsorship	O
deal	O
with	O
the	ORGANIZATION
A1	ORGANIZATION
Team	ORGANIZATION
Australia	ORGANIZATION
from	O
2005	O
to	O
2007	O
.	O
The	O
brand	O
is	O
currently	O
used	O
in	O
a	O
major	O
sponsorship	O
deal	O
with	O
the	ORGANIZATION
ASP	ORGANIZATION
World	ORGANIZATION
Tour	ORGANIZATION
.	O
