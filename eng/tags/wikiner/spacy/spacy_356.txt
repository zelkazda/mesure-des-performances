Considered	O
one	O
of	O
the	O
most	O
important	O
archaeological	O
sites	O
of	O
Ancient	LOCATION
Egypt	LOCATION
,	O
the	O
sacred	O
city	O
of	O
Abydos	O
was	O
the	O
site	O
of	O
many	O
ancient	O
temples	O
,	O
including	O
a	O
Umm	LOCATION
el-Qa'ab	LOCATION
,	O
a	O
royal	O
necropolis	O
where	O
early	O
pharaohs	O
were	O
entombed	O
.	O
Its	O
equivalent	O
in	O
Arabic	LANGUAGE
is	O
أبيدوس	O
.	O
The	O
pharaohs	O
of	O
the	O
first	O
dynasty	O
were	O
buried	O
in	O
Abydos	O
,	O
including	O
Narmer	PERSON
,	O
who	O
is	O
regarded	O
as	O
founder	O
of	O
the	O
first	O
dynasty	O
,	O
and	O
his	O
successor	O
,	O
Aha	O
.	O
Some	O
pharaohs	O
of	O
the	O
second	O
dynasty	O
were	O
also	O
buried	O
in	O
Abydos	LOCATION
.	O
Abydos	O
became	O
the	O
centre	O
of	O
the	O
worship	O
of	O
the	O
Isis	PERSON
and	O
Osiris	ORGANIZATION
cult	O
.	O
In	O
the	O
twelfth	O
dynasty	O
a	O
gigantic	O
tomb	O
was	O
cut	O
into	O
the	O
rock	O
by	O
Senusret	PERSON
III	PERSON
.	PERSON
Merneptah	O
added	O
the	O
Osireion	MISC
just	O
to	O
the	O
north	O
of	O
the	O
temple	O
of	O
Seti	LOCATION
.	O
Ahmose	PERSON
II	PERSON
in	O
the	O
twenty-sixth	O
dynasty	O
rebuilt	O
the	O
temple	O
again	O
,	O
and	O
placed	O
in	O
it	O
a	O
large	O
monolith	O
shrine	O
of	O
red	O
granite	O
,	O
finely	O
wrought	O
.	O
The	O
latest	O
building	O
was	O
a	O
new	O
temple	O
of	O
Nectanebo	PERSON
I	O
,	O
built	O
in	O
the	O
thirtieth	O
dynasty	O
.	O
A	O
tradition	O
developed	O
that	O
the	O
Early	O
Dynastic	O
cemetery	O
was	O
the	O
burial	O
place	O
of	O
Osiris	PERSON
and	O
the	O
tomb	O
of	O
Djer	PERSON
was	O
reinterpreted	O
as	O
that	O
of	O
Osiris	ORGANIZATION
.	O
A	O
vase	O
of	O
Menes	O
with	O
purple	O
hieroglyphs	O
inlaid	O
into	O
a	O
green	O
glaze	O
and	O
tiles	O
with	O
relief	O
figures	O
are	O
the	O
most	O
important	O
pieces	O
found	O
.	O
The	O
noble	O
statuette	O
of	O
Cheops	PERSON
in	O
ivory	LOCATION
,	O
found	O
in	O
the	O
stone	O
chamber	O
of	O
the	O
temple	O
,	O
gives	O
the	O
only	O
portrait	O
of	O
this	O
great	O
pharaoh	O
.	O
The	O
temple	O
was	O
rebuilt	O
entirely	O
on	O
a	O
larger	O
scale	O
by	O
Pepi	O
I	O
in	O
the	O
sixth	O
dynasty	O
.	O
Soon	O
after	O
,	O
Mentuhotep	O
II	O
entirely	O
rebuilt	O
the	O
temple	O
,	O
laying	O
a	O
stone	O
pavement	O
over	O
the	O
area	O
,	O
about	O
45	O
ft	O
(	O
14	O
m	O
)	O
square	O
,	O
and	O
added	O
subsidiary	O
chambers	O
.	O
Soon	O
thereafter	O
in	O
the	O
twelfth	O
dynasty	O
,	O
Senusret	PERSON
I	O
laid	O
massive	O
foundations	O
of	O
stone	O
over	O
the	O
pavement	O
of	O
his	O
predecessor	O
.	O
The	O
temple	O
of	O
Seti	PERSON
I	O
was	O
built	O
on	O
entirely	O
new	O
ground	O
half	O
a	O
mile	O
to	O
the	O
south	O
of	O
the	O
long	O
series	O
of	O
temples	O
just	O
described	O
.	O
This	O
surviving	O
building	O
is	O
best	O
known	O
as	O
the	MISC
Great	MISC
Temple	MISC
of	MISC
Abydos	MISC
,	O
being	O
nearly	O
complete	O
and	O
an	O
impressive	O
sight	O
.	O
Except	O
for	O
the	O
list	O
of	O
pharaohs	O
and	O
a	O
panegyric	O
on	O
Ramesses	O
II	O
,	O
the	O
subjects	O
are	O
not	O
historical	O
,	O
but	O
mythological	O
.	O
The	O
sculptures	O
had	O
been	O
published	O
mostly	O
in	O
hand	O
copy	O
,	O
not	O
facsimile	O
,	O
by	O
Auguste	PERSON
Mariette	PERSON
in	O
his	O
Abydos	O
,	O
i	O
.	O
The	O
adjacent	O
temple	O
of	O
Ramesses	O
II	O
was	O
much	O
smaller	O
and	O
simpler	O
in	O
plan	O
;	O
but	O
it	O
had	O
a	O
fine	O
historical	O
series	O
of	O
scenes	O
around	O
the	O
outside	O
that	O
lauded	O
his	O
achievements	O
,	O
of	O
which	O
the	O
lower	O
parts	O
remain	O
.	O
The	O
outside	O
of	O
the	O
temple	O
was	O
decorated	O
with	O
scenes	O
of	O
the	MISC
Battle	MISC
of	MISC
Kadesh	MISC
.	O
Others	O
also	O
built	O
before	O
Menes	O
are	O
15×25	O
ft	O
(	O
4.6	O
x	O
7.6	O
m	O
)	O
.	O
The	O
probable	O
tomb	O
of	O
Menes	PERSON
is	O
of	O
the	O
latter	O
size	O
.	O
Some	O
of	O
the	O
offerings	O
included	O
sacrificed	O
animals	O
,	O
such	O
as	O
the	O
asses	O
found	O
in	O
the	O
tomb	O
of	O
Merneith	O
.	O
Many	O
hundreds	O
of	O
funeral	O
steles	O
were	O
removed	O
by	O
Mariette	PERSON
's	O
workmen	O
,	O
without	O
any	O
record	O
of	O
the	O
burials	O
being	O
made	O
.	O
Later	O
excavations	O
have	O
been	O
recorded	O
by	O
Edward	PERSON
R.	PERSON
Ayrton	PERSON
,	O
Abydos	O
,	O
iii	O
.	O
Known	O
as	O
Shunet	PERSON
ez	PERSON
Zebib	PERSON
,	O
it	O
is	O
about	O
450	O
×	O
250	O
ft	O
(	O
137	O
x	O
76	O
m	O
)	O
over	O
all	O
,	O
and	O
one	O
still	O
stands	O
30-ft	O
(	O
9	O
m	O
)	O
high	O
.	O
It	O
was	O
built	O
by	O
Khasekhemwy	PERSON
,	O
the	O
last	O
pharaoh	O
of	O
the	O
second	O
dynasty	O
.	O
Another	O
structure	O
nearly	O
as	O
large	O
adjoined	O
it	O
,	O
and	O
probably	O
is	O
older	O
than	O
that	O
of	O
Khasekhemwy	PERSON
.	O
This	O
concept	O
was	O
adopted	O
in	O
the	O
plot	O
of	O
the	O
Stargate	MISC
series	O
.	O
