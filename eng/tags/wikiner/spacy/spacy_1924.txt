This	O
doctrine	O
was	O
announced	O
to	O
retroactively	O
justify	O
the	O
Soviet	MISC
invasion	O
of	O
Czechoslovakia	LOCATION
in	O
August	O
1968	O
that	O
ended	O
the	MISC
Prague	MISC
Spring	MISC
,	O
along	O
with	O
earlier	O
Soviet	MISC
military	O
interventions	O
,	O
such	O
as	O
the	O
invasion	O
of	O
Hungary	LOCATION
in	O
1956	O
.	O
Following	O
the	O
announcement	O
of	O
the	MISC
Brezhnev	MISC
Doctrine	MISC
,	O
numerous	O
treaties	O
were	O
signed	O
between	O
the	LOCATION
Soviet	LOCATION
Union	LOCATION
and	O
its	O
satellite	O
states	O
to	O
reassert	O
these	O
points	O
and	O
to	O
further	O
ensure	O
inter-state	O
cooperation	O
.	O
The	O
principles	O
of	O
the	O
doctrine	O
were	O
so	O
broad	O
that	O
the	O
Soviets	MISC
even	O
used	O
it	O
to	O
justify	O
their	O
military	O
intervention	O
in	O
the	ORGANIZATION
non-Warsaw	ORGANIZATION
Pact	ORGANIZATION
nation	O
of	O
Afghanistan	LOCATION
in	O
1979	O
.	O
