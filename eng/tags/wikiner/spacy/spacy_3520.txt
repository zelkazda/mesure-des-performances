His	O
11th-century	O
biographer	O
,	O
Osbern	PERSON
,	O
himself	O
an	O
artist	O
and	O
scribe	O
,	O
states	O
that	O
Dunstan	LOCATION
was	O
skilled	O
in	O
"	O
making	O
a	O
picture	O
and	O
forming	O
letters	O
"	O
,	O
as	O
were	O
other	O
clergy	O
of	O
his	O
age	O
who	O
reached	O
senior	O
rank	O
.	O
Dunstan	LOCATION
was	O
born	O
in	O
Baltonsborough	LOCATION
.	O
This	O
date	O
,	O
however	O
,	O
can	O
not	O
be	O
reconciled	O
with	O
other	O
known	O
dates	O
of	O
Dunstan	LOCATION
's	O
life	O
and	O
creates	O
many	O
obvious	O
anachronisms	O
.	O
Historians	O
therefore	O
assume	O
that	O
Dunstan	LOCATION
was	O
born	O
c.	O
910	O
or	O
earlier	O
.	O
While	O
still	O
a	O
boy	O
,	O
Dunstan	LOCATION
was	O
stricken	O
with	O
a	O
near-fatal	O
illness	O
and	O
effected	O
a	O
seemingly	O
miraculous	O
recovery	O
.	O
He	O
became	O
so	O
well	O
known	O
for	O
his	O
devotion	O
to	O
learning	O
that	O
he	O
is	O
said	O
to	O
have	O
been	O
summoned	O
by	O
his	O
uncle	O
Athelm	PERSON
,	O
the	O
Archbishop	O
of	O
Canterbury	PERSON
,	O
to	O
enter	O
his	O
service	O
.	O
He	O
was	O
later	O
appointed	O
to	O
the	O
court	O
of	O
King	LOCATION
Athelstan	LOCATION
.	O
Dunstan	LOCATION
soon	O
became	O
a	O
favourite	O
of	O
the	O
king	O
and	O
was	O
the	O
envy	O
of	O
other	O
members	O
of	O
the	O
court	O
.	O
A	O
plot	O
was	O
hatched	O
to	O
disgrace	O
him	O
and	O
Dunstan	LOCATION
was	O
accused	O
of	O
being	O
involved	O
with	O
witchcraft	O
and	O
black	O
magic	O
.	O
The	O
king	O
ordered	O
him	O
to	O
leave	O
the	O
court	O
and	O
as	O
Dunstan	LOCATION
was	O
leaving	O
the	O
palace	O
his	O
enemies	O
physically	O
attacked	O
him	O
,	O
beat	O
him	O
severely	O
,	O
bound	O
him	O
,	O
and	O
threw	O
him	O
into	O
a	O
cesspool	O
.	O
The	O
bishop	O
tried	O
to	O
persuade	O
him	O
to	O
become	O
a	O
monk	O
,	O
but	O
Dunstan	LOCATION
was	O
doubtful	O
whether	O
he	O
had	O
a	O
vocation	O
to	O
a	O
celibate	O
life	O
.	O
The	O
answer	O
came	O
in	O
the	O
form	O
of	O
an	O
attack	O
of	O
swelling	O
tumours	O
all	O
over	O
Dunstan	LOCATION
's	O
body	O
.	O
Whatever	O
the	O
cause	O
,	O
it	O
changed	O
Dunstan	LOCATION
's	O
mind	O
.	O
It	O
was	O
there	O
that	O
Dunstan	ORGANIZATION
studied	O
,	O
worked	O
at	O
his	O
handicrafts	O
,	O
and	O
played	O
on	O
his	O
harp	O
.	O
It	O
is	O
at	O
this	O
time	O
,	O
according	O
to	O
a	O
late	O
11th-century	O
legend	O
,	O
that	O
the	O
Devil	PERSON
is	O
said	O
to	O
have	O
tempted	O
Dunstan	LOCATION
and	O
to	O
have	O
been	O
held	O
by	O
the	O
face	O
with	O
Dunstan	LOCATION
's	O
tongs	O
.	O
Dunstan	ORGANIZATION
worked	O
as	O
a	O
silversmith	O
and	O
in	O
the	O
scriptorium	O
while	O
he	O
was	O
living	O
at	O
Glastonbury	LOCATION
.	O
Dunstan	ORGANIZATION
became	O
famous	O
as	O
a	O
musician	O
,	O
illuminator	O
,	O
and	O
metalworker	O
.	O
He	O
became	O
a	O
person	O
of	O
great	O
influence	O
,	O
and	O
on	O
the	O
death	O
of	O
King	LOCATION
Æthelstan	LOCATION
in	O
940	O
,	O
the	O
new	O
King	O
,	O
Edmund	PERSON
,	O
summoned	O
him	O
to	O
his	O
court	O
at	O
Cheddar	ORGANIZATION
and	O
made	O
him	O
a	O
minister	O
.	O
Again	O
,	O
royal	O
favour	O
fostered	O
jealousy	O
among	O
other	O
courtiers	O
and	O
again	O
Dunstan	ORGANIZATION
's	O
enemies	O
succeeded	O
in	O
their	O
plots	O
.	O
The	O
king	O
was	O
prepared	O
to	O
send	O
Dunstan	LOCATION
away	O
.	O
Dunstan	ORGANIZATION
implored	O
the	O
envoys	O
to	O
take	O
him	O
with	O
them	O
when	O
they	O
returned	O
to	O
their	O
homes	O
.	O
He	O
began	O
by	O
establishing	O
Benedictine	ORGANIZATION
monasticism	O
at	O
Glastonbury	LOCATION
.	O
A	O
substantial	O
extension	O
of	O
the	O
irrigation	O
system	O
on	O
the	O
surrounding	O
Somerset	ORGANIZATION
Levels	ORGANIZATION
was	O
also	O
completed	O
.	O
Within	O
two	O
years	O
of	O
Dunstan	LOCATION
's	O
appointment	O
,	O
in	O
946	O
,	O
King	PERSON
Edmund	PERSON
was	O
assassinated	O
.	O
His	O
successor	O
was	O
Eadred	O
.	O
Against	O
all	O
these	O
reforms	O
were	O
the	O
nobles	O
of	O
Wessex	PERSON
,	O
who	O
included	O
most	O
of	O
Dunstan	LOCATION
's	O
own	O
relatives	O
,	O
and	O
who	O
had	O
an	O
interest	O
in	O
maintaining	O
established	O
customs	O
.	O
For	O
nine	O
years	O
Dunstan	ORGANIZATION
's	O
influence	O
was	O
dominant	O
,	O
during	O
which	O
time	O
he	O
twice	O
refused	O
the	O
office	O
of	O
bishop	O
,	O
affirming	O
that	O
he	O
would	O
not	O
leave	O
the	O
king	O
's	O
side	O
so	O
long	O
as	O
the	O
king	O
lived	O
and	O
needed	O
him	O
.	O
In	O
955	O
,	O
Eadred	ORGANIZATION
died	O
,	O
and	O
the	O
situation	O
was	O
at	O
once	O
changed	O
.	O
Eadwig	PERSON
,	O
the	O
elder	O
son	O
of	O
Edmund	PERSON
,	O
who	O
then	O
came	O
to	O
the	O
throne	O
,	O
was	O
a	O
headstrong	O
youth	O
wholly	O
devoted	O
to	O
the	O
reactionary	O
nobles	O
.	O
According	O
to	O
one	O
legend	O
,	O
the	O
feud	O
with	O
Dunstan	LOCATION
began	O
on	O
the	O
day	O
of	O
Eadwig	PERSON
's	O
coronation	O
,	O
when	O
he	O
failed	O
to	O
attend	O
a	O
meeting	O
of	O
nobles	O
.	O
Although	O
Dunstan	ORGANIZATION
managed	O
to	O
escape	O
,	O
he	O
saw	O
that	O
his	O
life	O
was	O
in	O
danger	O
.	O
This	O
was	O
one	O
of	O
the	O
centres	O
of	O
the	O
Benedictine	ORGANIZATION
revival	O
in	O
that	O
country	O
,	O
and	O
Dunstan	LOCATION
was	O
able	O
for	O
the	O
first	O
time	O
to	O
observe	O
the	O
strict	O
observance	O
that	O
had	O
seen	O
its	O
rebirth	O
at	O
Cluny	PERSON
at	O
the	O
beginning	O
of	O
the	O
century	O
.	O
The	O
south	O
remained	O
faithful	O
to	O
Eadwig	PERSON
.	O
At	O
once	O
Edgar	PERSON
's	O
advisers	O
recalled	O
Dunstan	LOCATION
.	O
On	O
his	O
return	O
,	O
the	O
archbishop	O
consecrated	O
Dunstan	LOCATION
a	O
bishop	O
and	O
,	O
on	O
the	O
death	O
of	O
Coenwald	O
of	O
Worcester	O
at	O
the	O
end	O
of	O
957	O
,	O
Oda	PERSON
appointed	O
Dunstan	LOCATION
to	O
that	O
see	O
.	O
In	O
October	O
959	O
,	O
Eadwig	PERSON
died	O
and	O
his	O
brother	O
Edgar	PERSON
was	O
readily	O
accepted	O
as	O
ruler	O
of	O
Wessex	PERSON
.	O
One	O
of	O
Eadwig	PERSON
's	O
final	O
acts	O
had	O
been	O
to	O
appoint	O
a	O
successor	O
to	O
Archbishop	O
Oda	PERSON
,	O
who	O
died	O
on	O
2	O
June	O
958	O
.	O
In	O
his	O
place	O
Eadwig	PERSON
nominated	O
Byrhthelm	PERSON
,	O
the	O
Bishop	O
of	O
Wells	ORGANIZATION
.	O
The	O
archbishopric	O
was	O
then	O
conferred	O
on	O
Dunstan	LOCATION
.	O
On	O
his	O
journey	O
there	O
,	O
Dunstan	LOCATION
's	O
charities	O
were	O
so	O
lavish	O
as	O
to	O
leave	O
nothing	O
for	O
himself	O
and	O
his	O
attendants	O
.	O
The	O
monks	O
in	O
his	O
communities	O
were	O
taught	O
to	O
live	O
in	O
a	O
spirit	O
of	O
self-sacrifice	O
,	O
and	O
Dunstan	LOCATION
actively	O
enforced	O
the	O
law	O
of	O
celibacy	O
whenever	O
possible	O
.	O
In	O
973	O
,	O
Dunstan	LOCATION
's	O
statesmanship	O
reached	O
its	O
zenith	O
when	O
he	O
officiated	O
at	O
the	O
coronation	O
of	O
King	PERSON
Edgar	PERSON
.	O
Edgar	PERSON
died	O
two	O
years	O
after	O
his	O
coronation	O
,	O
and	O
was	O
succeeded	O
by	O
his	O
eldest	O
son	O
Edward	PERSON
(	PERSON
II	PERSON
)	O
"	O
the	O
Martyr	O
"	O
.	O
His	O
accession	O
was	O
disputed	O
by	O
his	O
stepmother	O
,	O
Ælfthryth	PERSON
,	O
who	O
wished	O
her	O
own	O
son	O
Æthelred	O
to	O
reign	O
.	O
Edgar	PERSON
's	O
death	O
had	O
encouraged	O
the	O
reactionary	O
nobles	O
,	O
and	O
at	O
once	O
there	O
was	O
a	O
determined	O
attack	O
upon	O
the	O
monks	O
,	O
the	O
protagonists	O
of	O
reform	O
.	O
Throughout	O
Mercia	PERSON
they	O
were	O
persecuted	O
and	O
deprived	O
of	O
their	O
possessions	O
.	O
In	O
March	O
978	O
,	O
King	ORGANIZATION
Eadweard	ORGANIZATION
was	O
assassinated	O
at	O
Corfe	MISC
Castle	MISC
,	O
possibly	O
at	O
the	O
instigation	O
of	O
his	O
stepmother	O
,	O
and	O
Æthelred	O
the	O
Unready	O
became	O
king	O
.	O
When	O
the	O
young	O
king	O
took	O
the	O
usual	O
oath	O
to	O
govern	O
well	O
,	O
Dunstan	LOCATION
addressed	O
him	O
in	O
solemn	O
warning	O
.	O
He	O
criticised	O
the	O
violent	O
act	O
whereby	O
he	O
became	O
king	O
and	O
prophesied	O
the	O
misfortunes	O
that	O
were	O
shortly	O
to	O
fall	O
on	O
the	O
kingdom	O
,	O
but	O
Dunstan	ORGANIZATION
's	O
influence	O
at	O
court	O
was	O
ended	O
.	O
Dunstan	ORGANIZATION
retired	O
to	O
Canterbury	ORGANIZATION
,	O
to	O
teach	O
at	O
the	O
cathedral	O
school	O
.	O
He	O
visited	O
the	O
shrines	O
of	O
St	LOCATION
Augustine	LOCATION
and	O
St	LOCATION
Æthelberht	LOCATION
,	O
and	O
there	O
are	O
reports	O
of	O
a	O
vision	O
of	O
angels	O
who	O
sang	O
to	O
him	O
heavenly	O
canticles	O
.	O
Dunstan	LOCATION
had	O
been	O
buried	O
in	O
his	O
cathedral	O
;	O
and	O
when	O
that	O
building	O
was	O
destroyed	O
by	O
a	O
fire	O
in	O
1074	O
,	O
his	O
relics	O
were	O
translated	O
by	O
Archbishop	O
Lanfranc	PERSON
to	O
a	O
tomb	O
on	O
the	O
south	O
side	O
of	O
the	O
high	O
altar	O
in	O
the	O
rebuilt	O
Canterbury	PERSON
Cathedral	PERSON
.	O
This	O
story	O
was	O
disproved	O
by	O
Archbishop	O
William	PERSON
Warham	PERSON
,	O
who	O
opened	O
the	O
tomb	O
at	O
Canterbury	LOCATION
in	O
1508	O
.	O
They	O
found	O
Dunstan	LOCATION
's	O
relics	O
still	O
to	O
be	O
there	O
.	O
Within	O
a	O
century	O
,	O
his	O
shrine	O
was	O
destroyed	O
during	O
the	MISC
English	MISC
Reformation	MISC
.	MISC
St	LOCATION
Dunstan	LOCATION
's	LOCATION
--	O
the	O
charity	O
that	O
provides	O
support	O
,	O
rehabilitation	O
,	O
and	O
respite	O
care	O
to	O
blind	O
ex-service	O
personnel	O
of	O
the	ORGANIZATION
British	ORGANIZATION
Armed	ORGANIZATION
Forces	ORGANIZATION
--	O
is	O
named	O
after	O
him	O
,	O
as	O
are	O
many	O
churches	O
all	O
over	O
the	O
world	O
.	O
