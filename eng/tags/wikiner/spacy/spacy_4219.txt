Emperor	O
Yōzei	PERSON
was	O
the	O
57th	O
emperor	O
of	O
Japan	LOCATION
,	O
according	O
to	O
the	O
traditional	O
order	O
of	O
succession	O
.	O
Yōzei	PERSON
's	O
reign	O
spanned	O
the	O
years	O
from	O
876	O
through	O
884	O
.	O
Yōzei	PERSON
was	O
the	O
oldest	O
son	O
of	O
Emperor	O
Seiwa	PERSON
.	O
Yōzei	PERSON
's	O
mother	O
was	O
the	O
sister	O
of	O
Fujiwara	PERSON
no	O
Mototsune	LOCATION
,	O
who	O
would	O
figure	O
prominently	O
in	O
the	O
young	O
emperor	O
's	O
life	O
.	O
Yōzei	PERSON
had	O
nine	O
Imperial	ORGANIZATION
sons	O
,	O
born	O
after	O
he	O
had	O
abdicated	O
.	O
Yōzei	PERSON
was	O
made	O
emperor	O
when	O
he	O
was	O
an	O
immature	O
,	O
unformed	O
young	O
boy	O
.	O
Many	O
of	O
the	O
high	O
court	O
officials	O
construed	O
Emperor	O
Yōzei	PERSON
's	O
actions	O
as	O
exceeding	O
the	O
bounds	O
of	O
acceptable	O
behavior	O
,	O
and	O
as	O
justifiable	O
cause	O
for	O
the	O
emperor	O
to	O
be	O
forcibly	O
deposed	O
.	O
In	O
Kitabatake	PERSON
Chikafusa	PERSON
's	PERSON
14th	O
century	O
account	O
of	O
Emperor	O
Yōzei	PERSON
's	O
reign	O
,	O
the	O
emperor	O
is	O
described	O
as	O
possessing	O
a	O
"	O
violent	O
disposition	O
"	O
and	O
unfit	O
to	O
be	O
a	O
ruler	O
.	O
Yōzei	PERSON
was	O
succeeded	O
by	O
his	O
father	O
's	O
uncle	O
,	O
Emperor	O
Kōkō	O
;	O
and	O
in	O
the	O
reign	O
of	O
Kōkō	PERSON
's	O
son	O
,	O
Emperor	O
Uda	PERSON
,	O
the	O
madness	O
re-visited	O
the	O
tormented	O
former	O
emperor	O
:	O
Yōzei	PERSON
lived	O
in	O
retirement	O
until	O
the	O
age	O
of	O
81	O
.	O
The	O
actual	O
site	O
of	O
Yōzei	PERSON
's	O
grave	O
is	O
known	O
.	O
The	ORGANIZATION
Imperial	ORGANIZATION
Household	ORGANIZATION
Agency	ORGANIZATION
designates	O
this	O
location	O
as	O
Yōzei	PERSON
's	O
mausoleum	O
.	O
The	O
years	O
of	O
Yōzei	PERSON
's	O
reign	O
are	O
more	O
specifically	O
identified	O
by	O
more	O
than	O
one	O
era	O
name	O
or	O
nengō	O
.	O
