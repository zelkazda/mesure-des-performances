AsDB	MISC
,	O
BSEC	ORGANIZATION
,	O
CE	O
,	O
CIS	ORGANIZATION
,	O
EAPC	ORGANIZATION
,	O
EBRD	ORGANIZATION
,	O
ECE	ORGANIZATION
,	O
ECO	ORGANIZATION
,	O
ESCAP	ORGANIZATION
,	O
FAO	ORGANIZATION
,	O
GUAM	O
,	O
IAEA	ORGANIZATION
,	O
IBRD	ORGANIZATION
,	O
ICAO	ORGANIZATION
,	O
ICRM	ORGANIZATION
,	O
IDA	ORGANIZATION
,	O
IDB	ORGANIZATION
,	O
IFAD	ORGANIZATION
,	O
IFC	ORGANIZATION
,	O
IFRCS	ORGANIZATION
,	O
ILO	ORGANIZATION
,	O
IMF	ORGANIZATION
,	O
IMO	ORGANIZATION
,	O
Interpol	ORGANIZATION
,	O
IOC	ORGANIZATION
,	O
IOM	ORGANIZATION
,	O
ISO	ORGANIZATION
(	O
correspondent	O
)	O
,	O
ITU	ORGANIZATION
,	O
ITUC	ORGANIZATION
,	O
OAS	ORGANIZATION
(	O
observer	O
)	O
,	O
OIC	ORGANIZATION
,	O
OPCW	ORGANIZATION
,	O
OSCE	ORGANIZATION
,	O
PFP	ORGANIZATION
,	O
UN	ORGANIZATION
,	O
UNCTAD	ORGANIZATION
,	O
UNESCO	ORGANIZATION
,	O
UNIDO	ORGANIZATION
,	O
UPU	ORGANIZATION
,	O
WCO	LOCATION
,	O
WFTU	PERSON
,	O
WHO	ORGANIZATION
,	O
WIPO	O
,	O
WMO	ORGANIZATION
,	O
WToO	O
,	O
WTrO	ORGANIZATION
(	O
observer	O
)	O
Today	O
,	O
Israel	LOCATION
is	O
a	O
major	O
arms	O
supplier	O
to	O
the	O
country	O
.	O
(	O
See	O
Azerbaijan-Israel	O
relations	O
)	O
.	O
Moscow	LOCATION
also	O
deployed	O
troops	O
to	O
Yerevan	LOCATION
.	O
Military	O
action	O
was	O
heavily	O
influenced	O
by	O
the	O
Russian	MISC
military	O
,	O
which	O
inspired	O
and	O
manipulated	O
the	O
rivalry	O
between	O
the	O
two	O
neighbouring	O
nations	O
in	O
order	O
to	O
keep	O
both	O
under	O
control	O
.	O
Negotiations	O
to	O
resolve	O
the	O
conflict	O
peacefully	O
have	O
been	O
ongoing	O
since	O
1992	O
under	O
the	O
aegis	O
of	O
the	ORGANIZATION
Minsk	ORGANIZATION
Group	ORGANIZATION
of	O
the	O
OSCE	ORGANIZATION
.	O
U.S.	LOCATION
Secretary	O
of	O
State	ORGANIZATION
Colin	PERSON
Powell	PERSON
launched	O
the	O
talks	O
on	O
April	O
3	O
,	O
2001	O
,	O
and	O
the	O
negotiations	O
continued	O
with	O
mediation	O
by	O
the	O
U.S.	LOCATION
,	O
Russia	LOCATION
,	O
and	O
France	LOCATION
until	O
April	O
6	O
,	O
2001	O
.	O
Issues	O
with	O
Russia	LOCATION
and	O
Kazakhstan	LOCATION
were	O
settled	O
in	O
2003	O
.	O
