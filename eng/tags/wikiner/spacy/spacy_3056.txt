The	ORGANIZATION
Chaos	ORGANIZATION
Computer	ORGANIZATION
Club	ORGANIZATION
(	O
CCC	ORGANIZATION
)	O
is	O
an	O
organization	O
of	O
hackers	O
.	O
The	O
CCC	ORGANIZATION
is	O
based	O
in	O
Germany	LOCATION
and	O
other	O
German-speaking	MISC
countries	O
and	O
currently	O
has	O
over	O
4,000	O
members	O
.	O
The	O
CCC	ORGANIZATION
was	O
founded	O
in	O
Berlin	LOCATION
on	O
September	O
12	O
,	O
1981	O
at	O
the	ORGANIZATION
Kommune	ORGANIZATION
1	O
's	O
table	O
in	O
the	O
rooms	O
of	O
the	O
newspaper	O
Die	ORGANIZATION
Tageszeitung	ORGANIZATION
by	O
Wau	LOCATION
Holland	LOCATION
and	O
others	O
in	O
anticipation	O
of	O
the	O
prominent	O
role	O
that	O
information	O
technology	O
would	O
play	O
in	O
the	O
way	O
people	O
live	O
and	O
communicate	O
.	O
The	O
CCC	ORGANIZATION
became	O
world	O
famous	O
when	O
they	O
drew	O
public	O
attention	O
to	O
the	O
security	O
flaws	O
of	O
the	O
German	MISC
Bildschirmtext	ORGANIZATION
computer	O
network	O
by	O
causing	O
it	O
to	O
debit	O
a	O
bank	O
in	O
Hamburg	LOCATION
DM	O
134,000	O
in	O
favor	O
of	O
the	O
club	O
.	O
Prior	O
to	O
the	O
incident	O
,	O
the	O
system	O
provider	O
had	O
failed	O
to	O
react	O
to	O
proof	O
of	O
the	O
security	O
flaw	O
provided	O
by	O
the	O
CCC	ORGANIZATION
,	O
claiming	O
to	O
the	O
public	O
that	O
their	O
system	O
was	O
safe	O
.	O
Bildschirmtext	ORGANIZATION
was	O
the	O
biggest	O
commercially	O
available	O
online	O
system	O
targeted	O
at	O
the	O
general	O
public	O
in	O
its	O
region	O
at	O
that	O
time	O
,	O
run	O
and	O
heavily	O
advertised	O
by	O
the	O
German	MISC
telecommunications	O
agency	O
(	O
Deutsche	ORGANIZATION
Bundespost	ORGANIZATION
)	O
which	O
also	O
strove	O
to	O
keep	O
up-to-date	O
alternatives	O
out	O
of	O
the	O
market	O
.	O
The	O
CCC	ORGANIZATION
is	O
more	O
widely	O
known	O
for	O
its	O
public	O
demonstrations	O
of	O
security	O
risks	O
.	O
In	O
1996	O
,	O
CCC	ORGANIZATION
members	O
demonstrated	O
an	O
attack	O
against	O
Microsoft	ORGANIZATION
's	O
ActiveX	O
technology	O
,	O
changing	O
personal	O
data	O
in	O
a	O
Quicken	ORGANIZATION
database	O
.	O
A	O
follow	O
up	O
installation	O
at	O
the	ORGANIZATION
Bibliothèque	ORGANIZATION
nationale	ORGANIZATION
de	ORGANIZATION
France	ORGANIZATION
was	O
the	O
world	O
's	O
biggest	O
light	O
installation	O
ever	O
.	O
In	O
March	O
2008	O
,	O
the	O
CCC	ORGANIZATION
acquired	O
and	O
published	O
the	O
fingerprints	O
of	O
German	MISC
Minister	O
of	O
the	O
Interior	O
Wolfgang	PERSON
Schäuble	PERSON
.	O
Every	O
four	O
years	O
,	O
the	ORGANIZATION
Chaos	ORGANIZATION
Communication	ORGANIZATION
Camp	ORGANIZATION
is	O
the	O
outdoor	O
alternative	O
for	O
hackers	O
worldwide	O
.	O
Members	O
of	O
the	O
CCC	ORGANIZATION
also	O
participate	O
in	O
various	O
technological	O
and	O
political	O
conferences	O
around	O
the	O
planet	O
.	O
