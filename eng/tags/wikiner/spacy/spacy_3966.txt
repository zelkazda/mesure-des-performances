EastEnders	ORGANIZATION
is	O
a	O
long-running	O
British	MISC
television	O
soap	O
opera	O
,	O
first	O
broadcast	O
in	O
the	O
U.K.	LOCATION
on	O
BBC	ORGANIZATION
One	ORGANIZATION
on	O
19	O
February	O
1985	O
.	O
EastEnders	ORGANIZATION
storylines	O
examine	O
the	O
domestic	O
and	O
professional	O
lives	O
of	O
the	O
people	O
who	O
live	O
and	O
work	O
in	O
the	O
fictional	O
London	O
Borough	O
of	O
Walford	O
in	O
the	LOCATION
East	LOCATION
End	LOCATION
of	LOCATION
London	LOCATION
.	O
Since	O
August	O
2001	O
,	O
four	O
episodes	O
are	O
broadcast	O
each	O
week	O
on	O
BBC	ORGANIZATION
One	ORGANIZATION
(	O
each	O
episode	O
is	O
repeated	O
on	O
BBC	ORGANIZATION
Three	ORGANIZATION
at	O
22:00	O
)	O
and	O
an	O
omnibus	O
edition	O
screens	O
on	O
Sunday	O
afternoons	O
.	O
It	O
is	O
one	O
of	O
the	O
UK	LOCATION
's	O
highest-rated	O
programmes	O
,	O
often	O
appearing	O
near	O
or	O
at	O
the	O
top	O
of	O
the	O
week	O
's	O
BARB	O
ratings	O
.	O
Albert	PERSON
Square	PERSON
was	O
built	O
around	O
the	O
early	O
20th	O
century	O
,	O
named	O
after	O
Prince	PERSON
Albert	PERSON
(	O
1819	O
--	O
1861	O
)	O
,	O
the	O
husband	O
of	O
Queen	PERSON
Victoria	PERSON
(	O
1819	O
--	O
1901	O
,	O
reigned	O
1837	O
--	O
1901	O
)	O
.	O
Fans	O
have	O
tried	O
to	O
establish	O
the	O
actual	O
location	O
of	O
Walford	LOCATION
within	O
London	LOCATION
.	O
Walford	PERSON
has	O
the	O
postal	O
district	O
of	O
E20	MISC
.	O
In	O
1917	O
the	O
current	O
postal	O
districts	O
in	O
London	LOCATION
were	O
assigned	O
alphabetically	O
according	O
to	O
the	O
name	O
of	O
the	O
main	O
sorting	O
office	O
for	O
each	O
district	O
.	O
The	O
name	O
Walford	PERSON
is	O
both	O
a	O
street	O
in	O
Dalston	LOCATION
where	O
Tony	PERSON
Holland	PERSON
lived	O
and	O
a	O
blend	O
of	O
Walthamstow	LOCATION
and	O
Stratford	LOCATION
--	O
the	O
areas	O
of	O
London	LOCATION
where	O
the	O
creators	O
were	O
born	O
.	O
EastEnders	ORGANIZATION
is	O
built	O
around	O
the	O
ideas	O
of	O
relationships	O
and	O
strong	O
families	O
,	O
with	O
each	O
character	O
having	O
a	O
place	O
in	O
the	O
community	O
.	O
The	O
first	O
central	O
family	O
was	O
the	O
Beale	PERSON
and	O
Fowler	PERSON
clan	O
consisting	O
of	O
Pauline	PERSON
Fowler	PERSON
,	O
her	O
husband	O
Arthur	PERSON
,	O
and	O
teenage	O
children	O
Mark	PERSON
and	O
Michelle	PERSON
.	O
Living	O
nearby	O
was	O
Pauline	PERSON
's	O
twin	O
brother	O
Pete	PERSON
Beale	PERSON
,	O
his	O
wife	O
Kathy	PERSON
and	O
their	O
son	O
Ian	PERSON
.	O
The	O
early	O
2000s	O
saw	O
a	O
shift	O
in	O
attention	O
towards	O
the	O
newly-introduced	O
female	O
Slater	PERSON
clan	O
,	O
before	O
a	O
renewal	O
of	O
emphasis	O
upon	O
the	O
restored	O
Watts	ORGANIZATION
family	O
beginning	O
in	O
2003	O
.	O
The	O
Beales	ORGANIZATION
are	O
the	O
show	O
's	O
longest	O
running	O
family	O
,	O
having	O
been	O
in	O
EastEnders	ORGANIZATION
since	O
it	O
began	O
in	O
1985	O
.	O
Key	O
people	O
involved	O
in	O
the	O
production	O
of	O
EastEnders	ORGANIZATION
have	O
stressed	O
how	O
important	O
the	O
idea	O
of	O
strong	O
families	O
is	O
to	O
the	O
programme	O
.	O
Some	O
families	O
feature	O
a	O
stereotypical	O
East	LOCATION
End	LOCATION
matriarch	O
.	O
The	O
original	O
matriarch	O
was	O
Lou	PERSON
Beale	PERSON
,	O
though	O
later	O
examples	O
include	O
Pauline	PERSON
Fowler	PERSON
,	O
Mo	PERSON
Butcher	PERSON
,	O
Mo	PERSON
Harris	PERSON
,	O
Pat	PERSON
Evans	PERSON
,	O
Peggy	PERSON
Mitchell	PERSON
and	O
Zainab	PERSON
Masood	PERSON
.	O
Such	O
characters	O
include	O
Angie	PERSON
Watts	PERSON
,	O
Kathy	PERSON
Mitchell	PERSON
,	O
Sharon	PERSON
Rickman	PERSON
and	O
Pat	PERSON
Evans	PERSON
.	O
Conversely	O
there	O
are	O
female	O
characters	O
who	O
handle	O
tragedy	O
less	O
well	O
,	O
depicted	O
as	O
eternal	O
victims	O
and	O
endless	O
sufferers	O
,	O
who	O
include	O
Sue	PERSON
Osman	PERSON
,	O
Little	O
Mo	PERSON
Mitchell	PERSON
,	O
Laura	PERSON
Beale	PERSON
and	O
Lisa	PERSON
Fowler	PERSON
.	O
Such	O
characters	O
have	O
included	O
Pat	PERSON
,	O
Tiffany	PERSON
Mitchell	PERSON
,	O
Kat	PERSON
Moon	PERSON
and	O
Stacey	PERSON
Slater	PERSON
.	O
A	O
gender	O
balance	O
in	O
the	O
show	O
is	O
maintained	O
via	O
the	O
inclusion	O
of	O
various	O
'	O
macho	O
'	O
male	O
personalities	O
such	O
as	O
Phil	PERSON
and	O
Grant	PERSON
Mitchell	PERSON
,	O
'	O
bad	O
boys	O
'	O
such	O
as	O
Den	PERSON
Watts	PERSON
and	O
Dennis	PERSON
Rickman	PERSON
and	O
'	O
heartthrobs	O
'	O
such	O
as	O
Simon	PERSON
Wicks	PERSON
and	O
Jamie	PERSON
Mitchell	PERSON
.	O
Another	O
recurring	O
male	O
character	O
seen	O
in	O
EastEnders	ORGANIZATION
is	O
the	O
'	O
loser	O
'	O
or	O
'	O
soft	O
touch	O
'	O
,	O
males	O
often	O
comically	O
under	O
the	O
thumb	O
of	O
their	O
female	O
counterparts	O
,	O
which	O
have	O
included	O
Arthur	PERSON
Fowler	PERSON
,	O
Ricky	PERSON
Butcher	PERSON
and	O
Lofty	PERSON
Holloway	PERSON
.	O
Other	O
recurring	O
characters	O
that	O
have	O
appeared	O
throughout	O
the	O
serial	O
are	O
'	O
lost	O
girls	O
'	O
such	O
as	O
Mary	PERSON
Smith	PERSON
and	O
Donna	PERSON
Ludlow	PERSON
,	O
delinquents	O
such	O
as	O
Mandy	PERSON
Salter	PERSON
,	O
Stacey	PERSON
Slater	PERSON
and	O
Jay	PERSON
Brown	PERSON
,	O
villains	O
such	O
as	O
Nick	PERSON
Cotton	PERSON
and	O
Trevor	PERSON
Morgan	PERSON
,	O
bitches	O
such	O
as	O
Cindy	PERSON
Beale	PERSON
and	O
Janine	PERSON
Evans	PERSON
and	O
cockney	O
'	O
wide	O
boys	O
'	O
or	O
'	O
wheeler	O
dealers	O
'	O
such	O
as	O
Frank	PERSON
Butcher	PERSON
and	O
Alfie	PERSON
Moon	PERSON
.	O
Over	O
the	O
years	O
EastEnders	ORGANIZATION
has	O
typically	O
featured	O
a	O
number	O
of	O
elderly	O
residents	O
,	O
who	O
are	O
used	O
to	O
show	O
vulnerability	O
,	O
nostalgia	O
,	O
stalwart-like	O
attributes	O
and	O
are	O
sometimes	O
used	O
for	O
comedic	O
purposes	O
.	O
The	O
original	O
elderly	O
residents	O
included	O
Lou	PERSON
Beale	PERSON
,	O
Ethel	PERSON
Skinner	PERSON
and	O
Dot	PERSON
Cotton	PERSON
.	O
This	O
has	O
spurred	O
criticism	O
,	O
most	O
notably	O
from	O
the	O
actress	O
Anna	PERSON
Wing	PERSON
,	O
who	O
played	O
Lou	PERSON
Beale	PERSON
in	O
the	O
show	O
.	O
EastEnders	ORGANIZATION
has	O
been	O
known	O
to	O
feature	O
a	O
'	O
comedy	O
double-act	O
'	O
,	O
originally	O
demonstrated	O
with	O
the	O
characters	O
of	O
Dot	PERSON
and	O
Ethel	PERSON
,	O
whose	O
friendship	O
was	O
one	O
of	O
the	O
serial	O
's	O
most	O
enduring	O
.	O
The	O
majority	O
of	O
EastEnders	ORGANIZATION
'	O
characters	O
are	O
working-class	O
.	O
Despite	O
this	O
,	O
the	O
programme	O
has	O
been	O
criticised	O
by	O
the	ORGANIZATION
Commission	ORGANIZATION
for	ORGANIZATION
Racial	ORGANIZATION
Equality	ORGANIZATION
,	O
who	O
argued	O
in	O
2002	O
that	O
EastEnders	ORGANIZATION
was	O
not	O
giving	O
a	O
realistic	O
representation	O
of	O
the	LOCATION
East	LOCATION
End	LOCATION
's	LOCATION
"	O
ethnic	O
make-up	O
"	O
.	O
A	O
sari	O
shop	O
was	O
opened	O
and	O
various	O
characters	O
of	O
differing	O
ethnicities	O
were	O
introduced	O
throughout	O
2006	O
and	O
2007	O
,	O
including	O
the	O
Fox	ORGANIZATION
family	O
,	O
the	O
Masoods	MISC
,	O
and	O
various	O
background	O
artists	O
.	O
This	O
was	O
part	O
of	O
producer	O
Diederick	PERSON
Santer	PERSON
's	PERSON
plan	O
to	O
"	O
diversify	O
"	O
,	O
to	O
make	O
EastEnders	ORGANIZATION
"	O
feel	O
more	O
21st	O
century	O
"	O
.	O
EastEnders	ORGANIZATION
have	O
had	O
varying	O
success	O
with	O
ethnic	O
minority	O
characters	O
.	O
EastEnders	ORGANIZATION
has	O
a	O
high	O
cast	O
turnover	O
and	O
characters	O
are	O
regularly	O
changed	O
in	O
order	O
to	O
facilitate	O
storylines	O
or	O
refresh	O
the	O
format	O
.	O
Sharon	PERSON
Rickman	PERSON
has	O
so	O
far	O
completed	O
six	O
separate	O
stints	O
on	O
the	O
programme	O
,	O
as	O
did	O
Frank	PERSON
Butcher	PERSON
,	O
and	O
writers	O
stunned	O
viewers	O
by	O
bringing	O
back	O
Den	ORGANIZATION
Watts	ORGANIZATION
14	O
years	O
after	O
he	O
was	O
believed	O
to	O
have	O
died	O
.	O
The	O
character	O
of	O
Nick	PERSON
Cotton	PERSON
gained	O
a	O
reputation	O
for	O
making	O
constant	O
exits	O
and	O
returns	O
since	O
the	O
programme	O
's	O
first	O
episode	O
.	O
Pauline	PERSON
Fowler	PERSON
's	PERSON
death	O
in	O
December	O
2006	O
means	O
that	O
,	O
as	O
of	O
2010	O
,	O
Ian	PERSON
Beale	PERSON
is	O
the	O
only	O
character	O
to	O
have	O
been	O
in	O
EastEnders	ORGANIZATION
from	O
the	O
first	O
episode	O
without	O
officially	O
leaving	O
.	O
Other	O
long-running	O
characters	O
include	O
Dot	ORGANIZATION
Branning	ORGANIZATION
who	O
joined	O
in	O
July	O
1985	O
but	O
had	O
a	O
four-year	O
break	O
in	O
the	O
mid	O
1990s	O
,	O
Pat	PERSON
Evans	PERSON
who	O
first	O
appeared	O
in	O
1986	O
and	O
has	O
never	O
officially	O
left	O
,	O
Ricky	PERSON
Butcher	PERSON
,	O
who	O
first	O
appeared	O
in	O
1988	O
but	O
with	O
substantial	O
breaks	O
since	O
2000	O
,	O
and	O
Phil	PERSON
Mitchell	PERSON
who	O
first	O
appeared	O
in	O
1990	O
,	O
and	O
had	O
a	O
two-year	O
break	O
from	O
2003	O
to	O
2005	O
.	O
EastEnders	ORGANIZATION
is	O
filmed	O
at	O
the	MISC
BBC	MISC
Elstree	MISC
Centre	MISC
in	O
Borehamwood	LOCATION
,	O
Hertfordshire	LOCATION
.	O
When	O
EastEnders	ORGANIZATION
went	O
to	O
four	O
episodes	O
a	O
week	O
,	O
more	O
studio	O
space	O
was	O
needed	O
.	O
Although	O
episodes	O
are	O
predominantly	O
recorded	O
weeks	O
before	O
they	O
are	O
broadcast	O
,	O
occasionally	O
,	O
EastEnders	ORGANIZATION
includes	O
current	O
affairs	O
in	O
their	O
episodes	O
.	O
During	O
the	O
2006	O
FIFA	O
World	MISC
Cup	MISC
,	O
actors	O
filmed	O
short	O
scenes	O
following	O
the	O
tournament	O
's	O
events	O
,	O
that	O
were	O
edited	O
into	O
the	O
programme	O
in	O
the	O
following	O
episode	O
.	O
EastEnders	ORGANIZATION
programme	O
makers	O
took	O
the	O
decision	O
that	O
the	O
show	O
was	O
to	O
be	O
about	O
"	O
everyday	O
life	O
"	O
in	O
the	O
inner	O
city	O
"	O
today	O
"	O
and	O
regarded	O
it	O
as	O
a	O
"	O
slice	O
of	O
life	O
"	O
.	O
In	O
the	O
1980s	O
,	O
EastEnders	ORGANIZATION
featured	O
"	O
gritty	O
"	O
storylines	O
involving	O
drugs	O
and	O
crime	O
,	O
representing	O
the	O
issues	O
faced	O
by	O
working-class	O
Britain	LOCATION
.	O
Mental	O
health	O
issues	O
were	O
confronted	O
in	O
1996	O
when	O
16-year-old	O
Joe	PERSON
Wicks	PERSON
developed	O
schizophrenia	O
following	O
the	O
off-screen	O
death	O
of	O
his	O
sister	O
in	O
a	O
car	O
crash	O
.	O
The	O
issue	O
of	O
illiteracy	O
was	O
highlighted	O
by	O
the	O
characters	O
of	O
middle-aged	O
Keith	PERSON
and	O
his	O
young	O
son	O
Darren	PERSON
.	O
David	PERSON
Proud	PERSON
,	O
who	O
plays	O
the	O
character	O
of	O
Adam	PERSON
Best	PERSON
,	O
is	O
the	O
first	O
wheelchair-using	O
actor	O
in	O
the	O
soap	O
's	O
history	O
.	O
Whodunnits	ORGANIZATION
also	O
feature	O
regularly	O
,	O
including	O
the	O
"	O
Who	O
Shot	O
Phil	O
?	O
"	O
They	O
gave	O
the	O
job	O
of	O
creating	O
this	O
new	O
soap	O
to	O
script	O
writer	O
Tony	PERSON
Holland	PERSON
and	O
producer	O
Julia	PERSON
Smith	PERSON
,	O
famous	O
for	O
their	O
work	O
together	O
on	O
Z	ORGANIZATION
Cars	ORGANIZATION
.	O
They	O
cast	O
actors	O
for	O
their	O
characters	O
,	O
and	O
began	O
to	O
film	O
the	O
show	O
at	O
the	MISC
BBC	MISC
Elstree	MISC
Centre	MISC
in	O
Borehamwood	LOCATION
,	O
Hertfordshire	LOCATION
.	O
Julia	PERSON
Smith	PERSON
thought	O
"	O
Eastenders	O
"	O
"	O
looked	O
ugly	O
written	O
down	O
"	O
,	O
and	O
capitalised	O
the	O
second	O
'	O
e	O
'	O
,	O
and	O
thus	O
the	O
name	O
EastEnders	ORGANIZATION
was	O
born	O
.	O
Filming	O
commenced	O
in	O
late-1984	LOCATION
and	O
the	O
show	O
was	O
first	O
broadcast	O
on	O
19	O
February	O
1985	O
,	O
and	O
became	O
wildly	O
popular	O
,	O
often	O
displacing	O
Coronation	MISC
Street	MISC
from	O
the	O
top	O
of	O
the	O
ratings	O
for	O
the	O
rest	O
of	O
the	O
1980s	O
.	O
Since	O
1985	O
,	O
EastEnders	ORGANIZATION
has	O
remained	O
at	O
the	O
centre	O
of	O
BBC	ORGANIZATION
One	ORGANIZATION
's	ORGANIZATION
primetime	O
schedule	O
.	O
EastEnders	ORGANIZATION
output	O
then	O
increased	O
to	O
three	O
times	O
a	O
week	O
on	O
Mondays	O
,	O
Tuesday	O
&	O
Thursdays	O
from	O
11	O
April	O
1994	O
until	O
2	O
August	O
2001	O
.	O
From	O
6	O
August	O
2001	O
,	O
EastEnders	ORGANIZATION
then	O
added	O
its	O
fourth	O
episode	O
(	O
shown	O
on	O
Fridays	O
)	O
.	O
In	O
this	O
first	O
head-to-head	O
battle	O
,	O
EastEnders	ORGANIZATION
claimed	O
victory	O
over	O
its	O
rival	O
.	O
In	O
early	O
2003	O
,	O
viewers	O
could	O
watch	O
episodes	O
of	O
EastEnders	ORGANIZATION
on	O
digital	O
channel	O
BBC	ORGANIZATION
Three	ORGANIZATION
before	O
they	O
were	O
broadcast	O
on	O
BBC	ORGANIZATION
One	ORGANIZATION
.	O
This	O
was	O
to	O
coincide	O
with	O
the	O
relaunch	O
of	O
the	O
channel	O
and	O
helped	O
BBC	ORGANIZATION
Three	O
break	O
the	O
one	O
million	O
viewers	O
mark	O
for	O
the	O
first	O
time	O
with	O
1.03	O
million	O
who	O
watched	O
to	O
see	O
Mark	PERSON
Fowler	PERSON
's	PERSON
departure	O
.	O
In	O
February	O
2005	O
,	O
there	O
were	O
reports	O
that	O
the	O
EastEnders	ORGANIZATION
schedule	O
was	O
threatened	O
due	O
to	O
production	O
problems	O
.	O
In	O
March	O
of	O
the	O
same	O
year	O
,	O
as	O
Peter	PERSON
Fincham	PERSON
became	O
the	O
BBC	ORGANIZATION
One	ORGANIZATION
controller	O
,	O
rumours	O
were	O
sparked	O
that	O
EastEnders	ORGANIZATION
could	O
be	O
broadcast	O
in	O
a	O
new	O
time	O
slot	O
.	O
EastEnders	ORGANIZATION
is	O
usually	O
repeated	O
on	O
BBC	ORGANIZATION
Three	ORGANIZATION
at	O
22:00	O
and	O
there	O
is	O
an	O
omnibus	O
on	O
BBC	ORGANIZATION
One	ORGANIZATION
on	O
Sunday	O
afternoons	O
,	O
which	O
replays	O
the	O
previous	O
weeks	O
'	O
episodes	O
in	O
a	O
block	O
.	O
Repeats	O
are	O
also	O
available	O
on-demand	O
through	O
BBC	ORGANIZATION
iPlayer	O
for	O
seven	O
days	O
after	O
the	O
original	O
screening	O
.	O
Selected	O
episodes	O
from	O
1985	O
and	O
1986	O
were	O
also	O
repeated	O
on	O
BBC1	O
on	O
Friday	O
evenings	O
at	O
20:30	O
for	O
a	O
short	O
while	O
.	O
Old	O
reruns	O
used	O
to	O
be	O
shown	O
on	O
UKTV	ORGANIZATION
Gold	ORGANIZATION
.	O
However	O
,	O
when	O
the	O
channel	O
was	O
rebranded	O
as	O
the	O
dedicated	O
comedy	O
channel	O
G.O.L.D	O
.	O
in	O
October	O
2008	O
,	O
EastEnders	ORGANIZATION
was	O
removed	O
.	O
The	O
show	O
takes	O
a	O
look	O
behind	O
the	O
scenes	O
of	O
the	O
EastEnders	ORGANIZATION
and	O
investigates	O
particular	O
places	O
,	O
characters	O
or	O
families	O
within	O
EastEnders	ORGANIZATION
.	O
The	O
show	O
was	O
presented	O
by	O
Angellica	PERSON
Bell	PERSON
and	O
was	O
available	O
to	O
digital	O
viewers	O
at	O
20:30	O
on	O
Monday	O
nights	O
.	O
A	O
new	O
breed	O
of	O
behind-the-scenes	O
programmes	O
have	O
been	O
broadcast	O
on	O
BBC	ORGANIZATION
Three	ORGANIZATION
since	O
1	O
December	O
2006	O
.	O
The	O
series	O
was	O
conceived	O
by	O
executive	O
producer	O
Diederick	PERSON
Santer	PERSON
"	O
as	O
a	O
way	O
of	O
nurturing	O
new	O
,	O
young	O
talent	O
,	O
both	O
on-and	O
off-screen	O
,	O
and	O
exploring	O
the	O
stories	O
of	O
the	O
soaps	O
'	O
anonymous	O
bystanders	O
.	O
"	O
E20	ORGANIZATION
features	O
a	O
group	O
of	O
sixth-form	O
characters	O
and	O
targets	O
the	O
"	O
Hollyoaks	O
demographic	O
"	O
.	O
It	O
was	O
written	O
by	O
a	O
team	O
of	O
young	O
writers	O
and	O
is	O
shown	O
three	O
times	O
a	O
week	O
on	O
the	O
EastEnders	ORGANIZATION
website	O
from	O
8	O
January	O
2010	O
.	O
E20	ORGANIZATION
has	O
been	O
to	O
return	O
for	O
a	O
second	O
series	O
.	O
A	O
behind	O
the	O
scenes	O
video	O
of	O
EastEnders	ORGANIZATION
,	O
hosted	O
by	O
Matt	PERSON
Di	PERSON
Angelo	PERSON
,	O
formerly	O
Deano	PERSON
Wicks	PERSON
on	O
the	O
show	O
,	O
was	O
put	O
on	O
the	O
site	O
the	O
same	O
day	O
,	O
and	O
was	O
followed	O
by	O
another	O
on	O
6	O
March	O
2007	O
.	O
The	O
second	O
and	O
third	O
featured	O
Stacey	PERSON
Slater	PERSON
and	O
Dawn	PERSON
Swann	PERSON
,	O
respectively	O
.	O
It	O
is	O
also	O
shown	O
on	O
BBC	ORGANIZATION
Canada	ORGANIZATION
.	O
In	O
June	O
2004	O
,	O
the	ORGANIZATION
Dish	ORGANIZATION
Network	ORGANIZATION
picked	O
up	O
EastEnders	ORGANIZATION
,	O
broadcasting	O
episodes	O
starting	O
at	O
the	O
point	O
where	O
BBC	ORGANIZATION
America	ORGANIZATION
had	O
ceased	O
broadcasting	O
them	O
,	O
offering	O
the	O
serial	O
as	O
a	O
pay-per-view	O
item	O
.	O
Dish	O
first	O
broadcast	O
two	O
weeks	O
'	O
worth	O
of	O
shows	O
each	O
week	O
to	O
catch	O
up	O
.	O
At	O
that	O
point	O
,	O
Dish	O
stopped	O
its	O
double-helping	O
schedule	O
,	O
and	O
now	O
maintains	O
the	O
schedule	O
of	O
broadcasting	O
the	O
new	O
programmes	O
consistently	O
one	O
month	O
behind	O
the	O
UK	LOCATION
schedule	O
.	O
In	O
Ireland	LOCATION
,	O
EastEnders	ORGANIZATION
first	O
aired	O
on	O
TV3	ORGANIZATION
from	O
September	O
1998	O
until	O
March	O
2001	O
when	O
it	O
moved	O
over	O
to	O
RTÉ	ORGANIZATION
One	ORGANIZATION
,	O
after	O
RTÉ	ORGANIZATION
lost	O
the	O
rights	O
to	O
air	O
rival	O
soap	O
Coronation	MISC
Street	MISC
to	O
TV3	O
.	O
An	O
average	O
EastEnders	ORGANIZATION
episode	O
attracts	O
a	O
total	O
audience	O
share	O
between	O
35	O
and	O
40	O
%	O
.	O
Aside	O
from	O
that	O
,	O
the	O
22:00	O
repeat	O
showing	O
on	O
BBC	ORGANIZATION
Three	ORGANIZATION
attracts	O
an	O
average	O
of	O
500,000	O
viewers	O
,	O
whilst	O
the	O
Sunday	O
omnibus	O
generally	O
attracts	O
3	O
million	O
.	O
EastEnders	ORGANIZATION
generally	O
rates	O
lower	O
than	O
Coronation	MISC
Street	MISC
,	O
and	O
has	O
been	O
beaten	O
by	O
Emmerdale	PERSON
on	O
numerous	O
occasions	O
.	O
In	O
2001	O
,	O
EastEnders	ORGANIZATION
clashed	O
with	O
Coronation	MISC
Street	MISC
for	O
the	O
first	O
time	O
.	O
EastEnders	ORGANIZATION
won	O
the	O
battle	O
with	O
8.4	O
million	O
viewers	O
(	O
41	O
%	O
share	O
)	O
whilst	O
Coronation	MISC
Street	MISC
lagged	O
behind	O
with	O
7.3	O
million	O
viewers	O
(	O
34	O
%	O
share	O
)	O
.	O
EastEnders	ORGANIZATION
often	O
clashes	O
with	O
Emmerdale	PERSON
,	O
and	O
this	O
gives	O
the	O
show	O
some	O
of	O
its	O
lowest	O
viewing	O
figures	O
,	O
dropping	O
to	O
below	O
six	O
million	O
.	O
However	O
,	O
the	O
BBC	O
Three	O
repeats	O
often	O
top	O
one	O
million	O
viewers	O
because	O
of	O
this	O
.	O
The	O
live	O
25th	O
anniversary	O
show	O
on	O
19	O
February	O
2010	O
,	O
which	O
revealed	O
Stacey	PERSON
Branning	PERSON
as	O
Archie	PERSON
Mitchell	PERSON
's	PERSON
killer	O
,	O
received	O
16.41	O
million	O
viewers	O
,	O
the	O
show	O
's	O
highest	O
rating	O
since	O
14	O
November	O
2003	O
.	O
On	O
Christmas	O
Day	O
1986	O
,	O
EastEnders	ORGANIZATION
attracted	O
30.15	O
million	O
viewers	O
who	O
tuned	O
in	O
to	O
see	O
Den	PERSON
Watts	PERSON
hand	O
over	O
divorce	O
papers	O
to	O
wife	O
Angie	PERSON
.	O
This	O
remains	O
the	O
highest	O
rated	O
episode	O
of	O
a	O
soap	O
in	O
British	MISC
television	O
history	O
.	O
On	O
21	O
September	O
2004	O
,	O
Louise	PERSON
Berridge	PERSON
,	O
the	O
then	O
executive	O
producer	O
,	O
quit	O
following	O
criticism	O
of	O
the	O
show	O
.	O
Emmerdale	PERSON
was	O
watched	O
by	O
8.1	O
million	O
people	O
.	O
Since	O
then	O
,	O
Emmerdale	PERSON
has	O
managed	O
to	O
beat	O
EastEnders	ORGANIZATION
in	O
the	O
ratings	O
many	O
times	O
,	O
establishing	O
itself	O
as	O
a	O
serious	O
contender	O
for	O
the	O
second	O
most	O
popular	O
UK	LOCATION
soap	O
.	O
Kathleen	PERSON
Hutchison	PERSON
,	O
who	O
had	O
been	O
the	O
executive	O
producer	O
of	O
hospital	O
drama	O
Holby	LOCATION
City	LOCATION
,	O
was	O
announced	O
as	O
the	O
new	O
executive	O
producer	O
.	O
Within	O
a	O
few	O
weeks	O
,	O
she	O
announced	O
a	O
major	O
shake-up	O
of	O
the	O
cast	O
with	O
the	O
highly-criticised	O
Ferreira	PERSON
family	O
,	O
first	O
seen	O
in	O
June	O
2003	O
,	O
written	O
out	O
at	O
the	O
beginning	O
of	O
2005	O
.	O
It	O
indicated	O
a	O
fresh	O
start	O
for	O
EastEnders	ORGANIZATION
after	O
declining	O
ratings	O
in	O
2004	O
.	O
In	O
January	O
2005	O
,	O
after	O
just	O
four	O
months	O
,	O
Kathleen	PERSON
Hutchison	PERSON
left	O
EastEnders	ORGANIZATION
.	O
He	O
also	O
brought	O
back	O
long	O
serving	O
script	O
writer	O
Tony	PERSON
Jordan	PERSON
.	O
It	O
is	O
reported	O
that	O
the	O
cast	O
and	O
crew	O
did	O
not	O
get	O
on	O
well	O
with	O
Hutchison	PERSON
as	O
she	O
had	O
them	O
working	O
up	O
to	O
midnight	O
and	O
beyond	O
.	O
But	O
through	O
her	O
short	O
reign	O
she	O
led	O
EastEnders	ORGANIZATION
to	O
some	O
of	O
its	O
most	O
healthy	O
viewing	O
figures	O
in	O
months	O
.	O
John	PERSON
Yorke	PERSON
immediately	O
stepped	O
into	O
her	O
position	O
until	O
a	O
few	O
weeks	O
later	O
when	O
Kate	PERSON
Harwood	PERSON
was	O
announced	O
as	O
the	O
new	O
executive	O
producer	O
.	O
2005	O
saw	O
EastEnders	ORGANIZATION
go	O
through	O
some	O
of	O
its	O
worst	O
ratings	O
in	O
its	O
20	O
year	O
history	O
.	O
On	O
1	O
March	O
2005	O
EastEnders	ORGANIZATION
received	O
its	O
second	O
lowest	O
ratings	O
at	O
that	O
time	O
,	O
when	O
both	O
EastEnders	ORGANIZATION
and	O
Emmerdale	PERSON
aired	O
one-hour	O
episodes	O
starting	O
at	O
19:00	O
.	O
The	O
episode	O
of	O
Emmerdale	PERSON
,	O
which	O
saw	O
the	O
exit	O
of	O
one	O
of	O
its	O
most	O
popular	O
characters	O
Charity	PERSON
Tate	PERSON
,	O
attracted	O
9.06	O
million	O
viewers	O
,	O
leaving	O
EastEnders	ORGANIZATION
with	O
just	O
6.2	O
million	O
viewers	O
.	O
Emmerdale	PERSON
was	O
watched	O
by	O
8.8	O
million	O
viewers	O
,	O
whilst	O
EastEnders	ORGANIZATION
was	O
watched	O
by	O
6.2	O
million	O
viewers	O
.	O
In	O
the	O
autumn	O
of	O
2005	O
,	O
EastEnders	ORGANIZATION
saw	O
its	O
average	O
audience	O
share	O
increase	O
.	O
One	O
episode	O
of	O
Emmerdale	PERSON
,	O
which	O
saw	O
the	O
departure	O
of	O
long	O
serving	O
,	O
popular	O
character	O
,	O
Zoe	PERSON
Tate	PERSON
,	O
attracted	O
8.3	O
million	O
viewers	O
,	O
leaving	O
EastEnders	ORGANIZATION
with	O
6.5	O
million	O
for	O
the	O
funeral	O
of	O
Den	PERSON
Watts	PERSON
.	O
However	O
,	O
this	O
indirectly	O
helped	O
increase	O
the	O
audience	O
of	O
digital	O
channel	O
BBC	ORGANIZATION
Three	ORGANIZATION
as	ORGANIZATION
1	ORGANIZATION
million	ORGANIZATION
(	O
10	O
%	O
share	O
)	O
tuned	O
in	O
to	O
see	O
the	O
second	O
showing	O
.	O
However	O
,	O
the	O
battle	O
between	O
EastEnders	ORGANIZATION
and	O
Emmerdale	PERSON
saw	O
EastEnders	ORGANIZATION
come	O
out	O
on	O
top	O
with	O
200,000	O
more	O
viewers	O
on	O
1	O
December	O
2005	O
.	O
EastEnders	ORGANIZATION
was	O
the	O
top-rated	O
soap	O
on	O
Christmas	O
Day	O
2005	O
,	O
attracting	O
10.6	O
million	O
viewers	O
while	O
Coronation	MISC
Street	MISC
got	O
9.8	O
million	O
,	O
and	O
Emmerdale	ORGANIZATION
got	O
7.9	O
million	O
.	O
12.6	O
million	O
viewers	O
watched	O
as	O
Dennis	PERSON
Rickman	PERSON
was	O
stabbed	O
by	O
a	O
mystery	O
attacker	O
on	O
30	O
December	O
2005	O
,	O
and	O
the	O
aftermath	O
attracted	O
12.34	O
million	O
viewers	O
on	O
2	O
January	O
2006	O
.	O
Since	O
then	O
EastEnders	ORGANIZATION
has	O
beaten	O
Coronation	MISC
Street	MISC
in	O
the	O
ratings	O
several	O
times	O
,	O
although	O
Coronation	MISC
Street	MISC
continues	O
to	O
average	O
more	O
on	O
a	O
regular	O
basis	O
.	O
Ratings	O
reached	O
an	O
all	O
time	O
low	O
in	O
July	O
2006	O
with	O
5.2	O
million	O
viewers	O
,	O
followed	O
two	O
days	O
later	O
by	O
only	O
3.9	O
million	O
when	O
it	O
was	O
scheduled	O
against	O
an	O
action	O
packed	O
hour	O
long	O
episode	O
of	O
Emmerdale	PERSON
featuring	O
several	O
characters	O
trapped	O
in	O
an	O
exploding	O
building	O
.	O
Emmerdale	PERSON
was	O
watched	O
by	O
9.03	O
million	O
people	O
.	O
Christmas	O
Day	O
2006	O
saw	O
EastEnders	ORGANIZATION
as	O
the	O
top	O
rated	O
soap	O
;	O
10.7	O
million	O
viewers	O
watched	O
to	O
see	O
the	O
death	O
of	O
Pauline	PERSON
Fowler	PERSON
.	O
EastEnders	ORGANIZATION
was	O
consequently	O
snubbed	O
from	O
the	O
Royal	ORGANIZATION
Television	ORGANIZATION
Society	ORGANIZATION
awards	O
.	O
EastEnders	ORGANIZATION
received	O
its	O
second	O
lowest	O
ratings	O
on	O
17	O
May	O
2007	O
,	O
when	O
4.0	O
million	O
viewers	O
tuned	O
in	O
to	O
see	O
Ian	PERSON
Beale	PERSON
and	O
Phil	PERSON
Mitchell	PERSON
's	PERSON
car	O
crash	O
,	O
part	O
of	O
the	O
show	O
's	O
most	O
expensive	O
stunt	O
.	O
Emmerdale	ORGANIZATION
's	O
audience	O
peaked	O
at	O
9.1	O
million	O
.	O
Ratings	O
for	O
the	O
22:00	O
EastEnders	O
repeat	O
on	O
BBC	ORGANIZATION
Three	ORGANIZATION
reached	O
an	O
all	O
time	O
high	O
of	O
1.4	O
million	O
.	O
The	O
second	O
half	O
of	O
the	O
double	O
bill	O
was	O
the	O
most	O
watched	O
programme	O
on	O
Christmas	O
Day	O
2007	O
in	O
the	O
UK	LOCATION
,	O
while	O
the	O
first	O
half	O
was	O
third	O
most	O
watched	O
,	O
surpassed	O
only	O
by	O
the	O
Doctor	O
Who	O
Christmas	O
special	O
.	O
On	O
24	O
March	O
2008	O
,	O
EastEnders	ORGANIZATION
attracted	O
a	O
strong	O
audience	O
of	O
11.4	O
million	O
viewers	O
a	O
42.4	O
%	O
audience	O
share	O
,	O
which	O
saw	O
Max	PERSON
Branning	PERSON
buried	O
alive	O
by	O
his	O
wife	O
Tanya	PERSON
Branning	PERSON
.	O
This	O
episode	O
beat	O
the	O
double	O
bill	O
of	O
Coronation	MISC
Street	MISC
which	O
attracted	O
10.9	O
million	O
viewers	O
at	O
19.30	O
a	O
41	O
%	O
audience	O
share	O
and	O
9.9	O
million	O
viewers	O
a	O
36.5	O
%	O
audience	O
share	O
at	O
20:30	O
.	O
It	O
was	O
the	O
most	O
watched	O
soap	O
opera	O
on	O
Christmas	O
Day	O
.	O
This	O
episode	O
marked	O
the	O
highest	O
audience	O
attraction	O
since	O
Christmas	O
Day	O
2008	O
.	O
As	O
of	O
December	O
2009	O
,	O
it	O
is	O
the	O
second	O
most	O
watched	O
programme	O
on	O
BBC	ORGANIZATION
One	ORGANIZATION
,	O
behind	O
Strictly	MISC
Come	MISC
Dancing	MISC
.	O
EastEnders	ORGANIZATION
was	O
the	O
most	O
watched	O
television	O
programme	O
on	O
Christmas	O
Day	O
2009	O
,	O
drawing	O
in	O
an	O
average	O
of	O
10.9	O
million	O
viewers	O
,	O
with	O
an	O
audience	O
share	O
of	O
45.9	O
%	O
.	O
The	O
hour-long	O
episode	O
featured	O
the	O
murder	O
of	O
Archie	PERSON
Mitchell	PERSON
.	O
EastEnders	ORGANIZATION
'	O
average	O
viewing	O
figures	O
for	O
January	O
2010	O
were	O
10.8million	O
(	O
40.4	O
%	O
)	O
compared	O
to	O
Coronation	MISC
Street	MISC
'	MISC
s	MISC
10.4million	O
(	O
38.2	O
%	O
)	O
,	O
making	O
EastEnders	ORGANIZATION
the	O
most	O
watched	O
soap	O
opera	O
on	O
British	MISC
television	O
for	O
the	O
first	O
time	O
in	O
three	O
years	O
.	O
The	O
live-episode	O
of	O
EastEnders	ORGANIZATION
on	O
19	O
February	O
2010	O
averaged	O
15.6	O
million	O
viewers	O
,	O
peaking	O
at	O
16.6	O
million	O
in	O
the	O
final	O
five	O
minutes	O
of	O
broadcast	O
.	O
In	O
April	O
2010	O
following	O
a	O
head	O
to	O
head	O
with	O
an	O
hour	O
long	O
edition	O
of	O
Emmerdale	ORGANIZATION
,	O
EastEnders	ORGANIZATION
recorded	O
it	O
's	O
lowest	O
viewing	O
figures	O
of	O
2010	O
with	O
5.88	O
million	O
viewers	O
tuning	O
in	O
,	O
binging	O
an	O
end	O
to	O
a	O
10	O
week	O
reign	O
at	O
the	O
top	O
of	O
the	O
Thursday	O
night	O
viewing	O
figures	O
.	O
Later	O
in	O
April	O
Eastenders	O
got	O
higher	O
ratings	O
when	O
Amira	PERSON
Masood	PERSON
found	O
out	O
that	O
her	O
husband	O
was	O
having	O
a	O
gay	O
affair	O
.	O
This	O
episode	O
got	O
9.3	O
million	O
viewers	O
and	O
also	O
got	O
1.04	O
million	O
on	O
Eastenders	O
'	O
BBC3	O
repeat	O
.	O
It	O
was	O
the	O
2nd	O
most	O
popular	O
UK	LOCATION
search	O
term	O
in	O
2003	O
,	O
and	O
the	O
7th	O
in	O
2004	O
.	O
EastEnders	ORGANIZATION
holds	O
the	O
record	O
for	O
the	O
most	O
watched	O
soap	O
episode	O
in	O
Britain	LOCATION
.	O
EastEnders	ORGANIZATION
has	O
received	O
both	O
praise	O
and	O
criticism	O
for	O
most	O
of	O
its	O
storylines	O
,	O
which	O
have	O
dealt	O
with	O
difficult	O
themes	O
,	O
such	O
as	O
violence	O
,	O
rape	O
,	O
murder	O
and	O
child	O
abuse	O
.	O
Mary	PERSON
Whitehouse	PERSON
argued	O
at	O
the	O
time	O
that	O
EastEnders	ORGANIZATION
represented	O
a	O
violation	O
of	O
"	O
family	O
viewing	O
time	O
"	O
and	O
that	O
it	O
undermined	O
the	O
watershed	O
policy	O
.	O
She	O
regarded	O
EastEnders	ORGANIZATION
as	O
a	O
fundamental	O
assault	O
on	O
the	O
family	O
and	O
morality	O
itself	O
.	O
However	O
,	O
Whitehouse	ORGANIZATION
also	O
praised	O
the	O
programme	O
,	O
describing	O
Michelle	PERSON
Fowler	PERSON
's	O
decision	O
not	O
to	O
have	O
an	O
abortion	O
as	O
a	O
"	O
very	O
positive	O
storyline	O
"	O
.	O
She	O
also	O
felt	O
that	O
EastEnders	ORGANIZATION
had	O
been	O
cleaned	O
up	O
as	O
a	O
result	O
of	O
her	O
protests	O
,	O
though	O
she	O
later	O
commented	O
that	O
EastEnders	ORGANIZATION
had	O
returned	O
to	O
its	O
old	O
ways	O
.	O
In	O
1997	O
several	O
episodes	O
were	O
shot	O
and	O
set	O
in	O
Ireland	LOCATION
,	O
resulting	O
in	O
criticisms	O
for	O
portraying	O
the	O
Irish	MISC
in	O
a	O
negatively	O
stereotypical	O
way	O
.	O
The	O
chief	O
executive	O
of	O
the	O
NSPCC	ORGANIZATION
praised	O
the	O
storyline	O
for	O
covering	O
the	O
subject	O
in	O
a	O
direct	O
and	O
sensitive	O
way	O
,	O
coming	O
to	O
the	O
conclusion	O
that	O
people	O
were	O
more	O
likely	O
to	O
report	O
any	O
issues	O
relating	O
to	O
child	O
protection	O
because	O
of	O
it	O
.	O
As	O
EastEnders	ORGANIZATION
is	O
shown	O
pre-watershed	O
,	O
there	O
were	O
worries	O
that	O
some	O
scenes	O
in	O
this	O
storyline	O
were	O
too	O
graphic	O
for	O
its	O
audience	O
.	O
The	O
character	O
of	O
Phil	PERSON
Mitchell	PERSON
(	O
played	O
by	O
Steve	PERSON
McFadden	PERSON
since	O
early	O
1990	O
)	O
has	O
been	O
criticised	O
on	O
several	O
occasions	O
for	O
glorifying	O
violence	O
and	O
proving	O
a	O
bad	O
role	O
model	O
to	O
children	O
.	O
On	O
one	O
occasion	O
following	O
a	O
scene	O
in	O
an	O
episode	O
broadcast	O
in	O
October	O
2002	O
,	O
where	O
Phil	PERSON
brutally	O
beat	O
his	O
godson	O
,	O
Jamie	PERSON
Mitchell	PERSON
,	O
31	O
complaints	O
came	O
from	O
viewers	O
who	O
watched	O
the	O
scenes	O
.	O
Originally	O
there	O
was	O
a	O
storyline	O
written	O
that	O
the	O
whole	O
Ferreira	PERSON
family	O
killed	O
their	O
pushy	O
father	O
Dan	PERSON
,	O
but	O
after	O
actor	O
Dalip	PERSON
Tahil	PERSON
could	O
not	O
get	O
a	O
visa	O
for	O
working	O
in	O
the	O
UK	LOCATION
the	O
storyline	O
was	O
scrapped	O
and	O
instead	O
Ronny	PERSON
Ferreira	PERSON
got	O
stabbed	O
and	O
survived	O
.	O
In	O
2003	O
,	O
Shaun	PERSON
Williamson	PERSON
,	O
who	O
was	O
in	O
the	O
final	O
months	O
of	O
his	O
role	O
of	O
Barry	PERSON
Evans	PERSON
,	O
said	O
that	O
the	O
programme	O
had	O
become	O
much	O
grittier	O
over	O
the	O
past	O
ten	O
to	O
fifteen	O
years	O
,	O
and	O
found	O
it	O
"	O
frightening	O
"	O
that	O
parents	O
let	O
their	O
young	O
children	O
watch	O
.	O
She	O
quotes	O
endlessly	O
from	O
the	O
Bible	MISC
and	O
it	O
ridicules	O
religion	O
to	O
some	O
extent	O
.	O
"	O
The	O
actress	O
rejected	O
offers	O
to	O
return	O
again	O
for	O
Pauline	PERSON
's	O
funeral	O
,	O
and	O
Scarlett	PERSON
Johnson	PERSON
,	O
who	O
played	O
Vicki	PERSON
Fowler	PERSON
,	O
was	O
n't	O
asked	O
to	O
return	O
.	O
In	O
July	O
2006	O
,	O
former	O
cast	O
member	O
Tracy-Ann	PERSON
Oberman	PERSON
suggested	O
that	O
the	O
scriptwriters	O
had	O
been	O
"	O
on	O
crack	O
"	O
when	O
they	O
penned	O
the	O
storyline	O
about	O
Den	ORGANIZATION
's	ORGANIZATION
murder	O
and	O
described	O
her	O
18	O
months	O
on	O
the	O
show	O
as	O
being	O
"	O
four	O
years	O
of	O
acting	O
experience	O
"	O
.	O
Wendy	PERSON
Richard	PERSON
,	O
who	O
played	O
Pauline	PERSON
Fowler	PERSON
for	O
21	O
years	O
,	O
has	O
also	O
claimed	O
that	O
she	O
quit	O
the	O
show	O
because	O
of	O
the	O
producers	O
'	O
decision	O
to	O
remarry	O
her	O
character	O
to	O
Joe	PERSON
Macer	PERSON
(	O
played	O
by	O
Ray	PERSON
Brooks	PERSON
)	O
,	O
as	O
she	O
felt	O
this	O
was	O
out	O
of	O
character	O
for	O
Pauline	PERSON
.	O
Conversely	O
,	O
learning	O
disability	O
charity	O
Mencap	ORGANIZATION
have	O
praised	O
the	O
soap	O
,	O
saying	O
it	O
will	O
help	O
to	O
raise	O
awareness	O
.	O
EastEnders	ORGANIZATION
also	O
came	O
under	O
fierce	O
criticism	O
for	O
a	O
homosexual	O
storyline	O
involving	O
Syed	PERSON
Masood	PERSON
and	O
Christian	PERSON
Clarke	PERSON
.	O
In	O
2010	O
,	O
EastEnders	ORGANIZATION
came	O
under	O
criticism	O
from	O
the	O
police	O
for	O
the	O
way	O
that	O
they	O
were	O
portrayed	O
during	O
the	O
"	O
Who	O
Killed	O
Archie	O
?	O
"	O
In	O
response	O
to	O
the	O
criticism	O
,	O
EastEnders	ORGANIZATION
apologised	O
for	O
offending	O
real-life	O
detectives	O
and	O
confirmed	O
that	O
they	O
use	O
a	O
police	O
consultant	O
for	O
such	O
storylines	O
.	O
Many	O
books	O
have	O
been	O
written	O
about	O
EastEnders	ORGANIZATION
.	O
Notably	O
,	O
from	O
1985	O
to	O
1988	O
,	O
author	O
and	O
television	O
writer	O
Hugh	PERSON
Miller	PERSON
wrote	O
seventeen	O
novels	O
,	O
detailing	O
the	O
lives	O
of	O
many	O
of	O
the	O
show	O
's	O
original	O
characters	O
before	O
1985	O
,	O
when	O
events	O
on	O
screen	O
took	O
place	O
.	O
