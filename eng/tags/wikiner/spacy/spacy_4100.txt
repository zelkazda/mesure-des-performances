Vincenzo	PERSON
Galilei	PERSON
(	O
father	O
of	O
Galileo	MISC
Galilei	O
)	O
was	O
one	O
of	O
the	O
first	O
advocates	O
of	O
twelve-tone	O
equal	O
temperament	O
in	O
a	O
1581	O
treatise	O
,	O
along	O
two	O
sets	O
of	O
dance	O
suites	O
on	O
each	O
of	O
the	O
12	O
notes	O
of	O
the	O
chromatic	O
scale	O
,	O
and	O
24	O
ricercars	O
in	O
all	O
the	O
"	O
major/minor	O
keys	O
"	O
.	O
Among	O
the	O
17th	O
century	O
keyboard	O
composers	O
Girolamo	PERSON
Frescobaldi	PERSON
advocated	O
equal	O
temperament	O
.	O
Some	O
theorists	O
,	O
such	O
as	O
Giuseppe	PERSON
Tartini	PERSON
,	O
were	O
opposed	O
to	O
the	O
adoption	O
of	O
equal	O
temperament	O
;	O
they	O
felt	O
that	O
degrading	O
the	O
purity	O
of	O
each	O
chord	O
degraded	O
the	O
aesthetic	O
appeal	O
of	O
music	O
,	O
although	O
Andreas	PERSON
Werckmeister	PERSON
emphatically	O
advocated	O
equal	O
temperament	O
in	O
his	O
1707	O
treatise	O
published	O
posthumously	O
.	O
This	O
allowed	O
greater	O
expression	O
through	O
enharmonic	O
modulation	O
,	O
which	O
became	O
extremely	O
important	O
in	O
the	O
18th	O
century	O
in	O
music	O
of	O
such	O
composers	O
as	O
Francesco	PERSON
Geminiani	PERSON
,	O
Wilhelm	PERSON
Friedemann	PERSON
Bach	PERSON
,	O
Carl	PERSON
Philipp	PERSON
Emmanuel	PERSON
Bach	PERSON
and	O
Johann	PERSON
Gottfried	PERSON
Müthel	PERSON
.	O
Wendy	PERSON
Carlos	PERSON
discovered	O
three	O
unusual	O
equal	O
temperaments	O
after	O
a	O
thorough	O
study	O
of	O
the	O
properties	O
of	O
possible	O
temperaments	O
having	O
a	O
step	O
size	O
between	O
30	O
and	O
120	O
cents	O
.	O
