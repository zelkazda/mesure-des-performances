The	O
doctrine	O
of	O
the	ORGANIZATION
Protestant	ORGANIZATION
Church	ORGANIZATION
in	O
the	O
Netherlands	LOCATION
is	O
expressed	O
in	O
its	O
creeds	O
.	O
From	O
the	O
Lutheran	MISC
tradition	O
are	O
the	O
unaltered	O
Augsburg	ORGANIZATION
Confession	ORGANIZATION
and	O
Luther	PERSON
's	PERSON
Catechism	O
.	O
The	O
PKN	ORGANIZATION
contains	O
both	O
liberal	O
and	O
conservative	O
movements	O
.	O
The	O
polity	O
of	O
the	ORGANIZATION
Protestant	ORGANIZATION
Church	ORGANIZATION
in	O
the	O
Netherlands	LOCATION
is	O
a	O
hybrid	O
of	O
presbyterian	O
and	O
congregationalist	O
church	O
governance	O
.	O
The	O
PKN	ORGANIZATION
has	O
four	O
different	O
types	O
of	O
congregations	O
:	O
Lutherans	MISC
are	O
a	O
minority	O
(	O
about	O
1	O
percent	O
)	O
of	O
the	O
PKN	ORGANIZATION
's	O
membership	O
.	O
Only	O
those	O
congregations	O
belonging	O
to	O
the	O
former	O
Reformed	ORGANIZATION
Churches	ORGANIZATION
in	O
the	O
Netherlands	LOCATION
have	O
the	O
legal	O
right	O
to	O
secede	O
from	O
the	O
PKN	ORGANIZATION
without	O
losing	O
its	O
property	O
and	O
church	O
during	O
a	O
transition	O
period	O
of	O
10	O
years	O
.	O
Two	O
congregations	O
have	O
joined	O
one	O
of	O
the	O
other	O
smaller	O
Reformed	O
churches	O
in	O
the	O
Netherlands	LOCATION
.	O
Some	O
congregations	O
and	O
members	O
in	O
the	ORGANIZATION
Dutch	ORGANIZATION
Reformed	ORGANIZATION
Church	ORGANIZATION
did	O
not	O
agree	O
with	O
the	O
merger	O
and	O
have	O
separated	O
.	O
They	O
have	O
organized	O
themselves	O
in	O
the	ORGANIZATION
Restored	ORGANIZATION
Reformed	ORGANIZATION
Church	ORGANIZATION
.	O
