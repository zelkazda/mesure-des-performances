The	LOCATION
Republic	LOCATION
of	LOCATION
Haiti	LOCATION
comprises	O
the	O
western	O
third	O
of	O
the	O
island	O
of	O
Hispaniola	LOCATION
,	O
west	O
of	O
the	LOCATION
Dominican	LOCATION
Republic	LOCATION
.	O
It	O
is	O
positioned	O
between	O
the	LOCATION
Caribbean	LOCATION
Sea	LOCATION
and	O
the	LOCATION
North	LOCATION
Atlantic	LOCATION
Ocean	LOCATION
.	O
The	O
country	O
(	O
and	O
Hispaniola	MISC
)	O
is	O
separated	O
from	O
Cuba	LOCATION
by	O
way	O
of	O
the	LOCATION
Windward	LOCATION
Passage	LOCATION
,	O
a	O
45	O
nmi	O
(	O
83	O
km	O
;	O
52	O
mi	O
)	O
wide	O
strait	O
that	O
passes	O
between	O
the	O
two	O
countries	O
.	O
It	O
is	O
located	O
in	O
the	MISC
Cul-de-Sac	MISC
Depression	MISC
with	O
an	O
area	O
of	O
170	O
km²	O
.	O
Lake	MISC
Peligre	MISC
is	O
an	O
artificial	O
lake	O
created	O
by	O
the	O
construction	O
of	O
the	MISC
Peligre	MISC
Hydroelectric	MISC
Dam	MISC
.	O
Trou	PERSON
Caïman	PERSON
is	O
a	O
saltwater	O
lake	O
with	O
a	O
total	O
area	O
of	O
16.2	O
km²	O
.	O
Lake	LOCATION
Miragoâne	LOCATION
is	O
one	O
of	O
the	O
largest	O
natural	O
freshwater	O
lakes	O
in	O
the	O
Caribbean	LOCATION
,	O
with	O
an	O
area	O
of	O
25	O
km²	O
.	O
Port-au-Prince	MISC
receives	O
an	O
average	O
annual	O
rainfall	O
of	O
1370	O
mm	O
(	O
53.9	O
in	O
)	O
.	O
