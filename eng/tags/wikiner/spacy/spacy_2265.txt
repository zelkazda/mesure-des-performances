The	O
chief	O
western	O
mountain	O
range	O
,	O
the	ORGANIZATION
Cordillera	ORGANIZATION
Occidental	ORGANIZATION
,	O
is	O
a	O
moderately	O
high	O
range	O
with	O
peaks	O
reaching	O
up	O
to	O
about	O
13000	O
ft	O
(	O
3962	O
m	O
)	O
.	O
The	LOCATION
Guajira	LOCATION
Peninsula	LOCATION
in	O
the	O
east	O
is	O
semiarid	O
.	O
The	O
Sierra	O
Nevada	O
de	O
Santa	O
Marta	O
is	O
a	O
spectacular	O
triangular	O
snowcapped	O
block	O
of	O
rock	O
that	O
towers	O
over	O
the	O
eastern	O
part	O
of	O
this	O
lowland	O
.	O
Near	O
the	O
Ecuadorian	MISC
frontier	O
,	O
the	O
Andes	LOCATION
Mountains	LOCATION
divide	O
into	O
three	O
distinct	O
,	O
roughly	O
parallel	O
chains	O
,	O
called	O
cordilleras	O
,	O
that	O
extend	O
northeastward	O
almost	O
to	O
the	LOCATION
Caribbean	LOCATION
Sea	LOCATION
.	O
In	O
the	O
late	O
1980s	O
,	O
approximately	O
78	O
percent	O
of	O
the	O
country	O
's	O
population	O
lived	O
in	O
the	O
Andean	MISC
highlands	O
.	O
The	ORGANIZATION
Cordillera	ORGANIZATION
Occidental	ORGANIZATION
is	O
relatively	O
low	O
and	O
is	O
the	O
least	O
populated	O
of	O
the	O
three	O
cordilleras	O
.	O
Few	O
passes	O
exist	O
,	O
although	O
one	O
that	O
is	O
about	O
4985	O
ft	O
(	O
1519	O
m	O
)	O
above	O
sea	O
level	O
provides	O
the	O
major	O
city	O
of	O
Cali	LOCATION
with	O
an	O
outlet	O
to	O
the	LOCATION
Pacific	LOCATION
Ocean	LOCATION
.	O
The	LOCATION
Río	LOCATION
Cauca	LOCATION
rises	O
within	O
124	O
mi	O
(	O
200	O
km	O
)	O
of	O
the	O
border	O
with	O
Ecuador	LOCATION
and	O
flows	O
through	O
some	O
of	O
the	O
best	O
farmland	O
in	O
the	O
country	O
.	O
The	O
highest	O
peak	O
in	O
this	O
range	O
,	O
the	MISC
Nevado	MISC
del	MISC
Huila	MISC
,	O
reaches	O
17602	O
ft	O
(	O
5365	O
m	O
)	O
above	O
sea	O
level	O
.	O
The	O
second	O
highest	O
peak	O
is	O
a	O
volcano	O
,	O
Nevado	LOCATION
del	LOCATION
Ruiz	LOCATION
,	O
which	O
erupted	O
violently	O
on	O
November	O
13	O
,	O
1985	O
.	O
The	LOCATION
Magdalena	LOCATION
River	LOCATION
is	O
generally	O
navigable	O
from	O
the	LOCATION
Caribbean	LOCATION
Sea	LOCATION
as	O
far	O
as	O
the	O
town	O
of	O
Neiva	ORGANIZATION
,	O
deep	O
in	O
the	O
interior	O
,	O
but	O
is	O
interrupted	O
midway	O
by	O
rapids	O
.	O
To	O
the	O
north	O
of	O
Bogotá	LOCATION
,	O
in	O
the	O
densely	O
populated	O
basins	O
of	O
Chiquinquirá	LOCATION
and	O
Boyacá	LOCATION
,	O
are	O
fertile	O
fields	O
,	O
rich	O
mines	O
,	O
and	O
large	O
industrial	O
establishments	O
that	O
produce	O
much	O
of	O
the	O
national	O
wealth	O
.	O
In	O
the	O
department	O
of	O
Santander	ORGANIZATION
,	O
the	O
valleys	O
on	O
the	O
western	O
slopes	O
are	O
more	O
spacious	O
,	O
and	O
agriculture	O
is	O
intensive	O
in	O
the	O
area	O
around	O
Bucaramanga	LOCATION
.	O
In	O
the	O
southern	O
part	O
rises	O
the	O
Sierra	ORGANIZATION
Nevada	ORGANIZATION
de	ORGANIZATION
Santa	ORGANIZATION
Marta	ORGANIZATION
,	O
an	O
isolated	O
mountain	O
system	O
with	O
peaks	O
reaching	O
heights	O
over	O
5700	O
meters	O
(	O
18701	O
ft	O
)	O
and	O
slopes	O
generally	O
too	O
steep	O
for	O
cultivation	O
.	O
The	O
Caribbean	LOCATION
region	O
merges	O
into	O
and	O
is	O
connected	O
with	O
the	O
Andean	MISC
highlands	O
through	O
the	O
two	O
great	O
river	O
valleys	O
.	O
After	O
the	O
Andean	MISC
highlands	O
,	O
it	O
is	O
the	O
second	O
most	O
important	O
region	O
in	O
economic	O
activity	O
.	O
On	O
an	O
special	O
note	O
,	O
although	O
the	O
border	O
between	O
the	O
Caribbean	LOCATION
region	O
and	O
the	O
Andes	LOCATION
region	O
has	O
some	O
presence	O
of	O
guerrilla	O
left-wing	O
exremist	O
groups	O
and	O
some	O
paramilitary	O
right-wing	O
groups	O
,	O
their	O
presence	O
is	O
marginal	O
in	O
that	O
area	O
and	O
even	O
more	O
minoritary	O
in	O
the	O
rest	O
of	O
the	O
Caribbean	LOCATION
region	O
,	O
as	O
opposed	O
to	O
the	O
heavy	O
presence	O
and	O
social	O
infiltration	O
they	O
have	O
in	O
the	O
inner	O
side	O
of	O
the	O
country	O
where	O
they	O
are	O
infiltrated	O
in	O
most	O
levels	O
of	O
society	O
and	O
economic	O
sectors	O
or	O
establishments	O
and	O
institutions	O
while	O
the	O
government	O
fights	O
heavily	O
to	O
purge	O
such	O
insurgent	O
infiltrates	O
out	O
of	O
such	O
cities	O
.	O
Next	ORGANIZATION
is	O
the	O
broad	O
region	O
of	O
the	LOCATION
Río	LOCATION
Atrato	LOCATION
/	LOCATION
Río	LOCATION
San	LOCATION
Juan	LOCATION
lowland	O
,	O
which	O
has	O
been	O
proposed	O
as	O
a	O
possible	O
alternate	O
to	O
the	MISC
Panama	MISC
Canal	MISC
as	O
a	O
human-made	O
route	O
between	O
the	O
Atlantic	LOCATION
and	O
the	O
Pacific	LOCATION
oceans	O
.	O
On	O
the	O
east	O
,	O
the	O
Pacific	LOCATION
lowlands	O
are	O
bounded	O
by	O
the	ORGANIZATION
Cordillera	ORGANIZATION
Occidental	ORGANIZATION
,	O
from	O
which	O
numerous	O
streams	O
run	O
.	O
To	O
the	O
west	O
of	O
the	LOCATION
Río	LOCATION
Atrato	LOCATION
rises	O
the	O
Serranía	ORGANIZATION
de	ORGANIZATION
Baudó	ORGANIZATION
,	O
an	O
isolated	O
chain	O
of	O
low	O
mountains	O
that	O
occupies	O
a	O
large	O
part	O
of	O
the	O
region	O
.	O
A	O
second	O
interoceanic	O
canal	O
would	O
be	O
constructed	O
by	O
dredging	O
the	LOCATION
Río	LOCATION
Atrato	LOCATION
and	O
other	O
streams	O
and	O
digging	O
short	O
access	O
canals	O
.	O
The	O
region	O
is	O
unbroken	O
by	O
highlands	O
except	O
in	O
Meta	ORGANIZATION
Department	ORGANIZATION
,	O
where	O
the	MISC
Serranía	MISC
de	MISC
la	MISC
Macarena	MISC
,	O
an	O
out	O
lier	O
of	O
the	O
Andes	LOCATION
has	O
unique	O
vegetation	O
and	O
wildlife	O
believed	O
to	O
be	O
reminiscent	O
of	O
those	O
that	O
once	O
existed	O
throughout	O
the	O
Andes	LOCATION
.	O
The	MISC
Río	MISC
Guaviare	MISC
and	O
the	O
streams	O
to	O
its	O
north	O
flow	O
eastward	O
and	O
drain	O
into	O
the	O
basin	O
of	O
the	LOCATION
Río	LOCATION
Orinoco	LOCATION
,	O
river	O
that	O
crosses	O
into	O
Venezuela	LOCATION
and	O
flows	O
into	O
the	LOCATION
Atlantic	LOCATION
Ocean	LOCATION
.	O
Those	O
south	O
of	O
the	MISC
Río	MISC
Guaviare	MISC
flow	O
into	O
the	O
Amazon	O
Basin	LOCATION
.	O
Precipitation	O
exceeds	O
7600	O
millimeters	O
(	O
299.2	O
in	O
)	O
annually	O
in	O
most	O
of	O
the	O
Pacific	LOCATION
lowlands	O
,	O
making	O
this	O
one	O
of	O
the	O
wettest	O
regions	O
in	O
the	O
world	O
.	O
For	O
example	O
,	O
rainfall	O
in	O
parts	O
of	O
the	LOCATION
Guajira	LOCATION
Peninsula	LOCATION
seldom	O
exceeds	O
30	O
in	O
(	O
762	O
mm	O
)	O
per	O
year	O
.	O
The	O
northern	O
part	O
,	O
called	O
"	O
Los	O
Llanos	O
"	O
is	O
a	O
savanna	O
region	O
,	O
mostly	O
in	O
the	O
Orinoco	LOCATION
basin	O
.	O
The	O
southern	O
part	O
is	O
covered	O
by	O
the	O
Amazon	ORGANIZATION
rain	O
forest	O
and	O
belongs	O
mostly	O
to	O
the	O
Amazon	ORGANIZATION
basin	O
.	O
At	O
the	O
north	O
and	O
west	O
of	O
the	O
Andes	LOCATION
range	O
there	O
are	O
some	O
coastal	O
plains	O
.	O
The	O
collateral	O
damaged	O
produced	O
by	O
attacks	O
against	O
oil	O
pipeline	O
infrastructure	O
by	O
rebel	O
guerrillas	O
in	O
the	O
Colombian	MISC
armed	O
conflict	O
has	O
produced	O
long	O
term	O
damage	O
to	O
the	O
environment	O
.	O
Coastline	O
:	O
3,208	O
km	O
(	O
Caribbean	LOCATION
Sea	LOCATION
1,760	O
km	O
,	O
North	LOCATION
Pacific	LOCATION
Ocean	LOCATION
1,448	O
km	O
)	O
