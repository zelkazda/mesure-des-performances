In	O
1951	O
,	O
David	PERSON
A.	PERSON
Huffman	PERSON
and	O
his	O
MIT	ORGANIZATION
information	O
theory	O
classmates	O
were	O
given	O
the	O
choice	O
of	O
a	O
term	O
paper	O
or	O
a	O
final	O
exam	O
.	O
The	O
professor	O
,	O
Robert	PERSON
M.	PERSON
Fano	PERSON
,	O
assigned	O
a	O
term	O
paper	O
on	O
the	O
problem	O
of	O
finding	O
the	O
most	O
efficient	O
binary	O
code	O
.	O
In	O
doing	O
so	O
,	O
the	O
student	O
outdid	O
his	O
professor	O
,	O
who	O
had	O
worked	O
with	O
information	O
theory	O
inventor	O
Claude	PERSON
Shannon	PERSON
to	O
develop	O
a	O
similar	O
code	O
.	O
So	O
not	O
only	O
is	O
this	O
code	O
optimal	O
in	O
the	O
sense	O
that	O
no	O
other	O
feasible	O
code	O
performs	O
better	O
,	O
but	O
it	O
is	O
very	O
close	O
to	O
the	O
theoretical	O
limit	O
established	O
by	O
Shannon	MISC
.	O
