Sex	O
researcher	O
Alfred	PERSON
Kinsey	PERSON
,	O
working	O
in	O
the	O
1940s	O
,	O
had	O
found	O
that	O
number	O
to	O
be	O
closer	O
to	O
40	O
%	O
at	O
the	O
time	O
.	O
More	O
recently	O
,	O
a	O
researcher	O
from	O
the	ORGANIZATION
University	ORGANIZATION
of	ORGANIZATION
British	ORGANIZATION
Columbia	ORGANIZATION
in	O
2005	O
put	O
the	O
number	O
of	O
heterosexuals	O
who	O
have	O
practiced	O
anal	O
sex	O
at	O
between	O
30	O
%	O
and	O
50	O
%	O
.	O
According	O
to	O
Columbia	ORGANIZATION
University	ORGANIZATION
's	ORGANIZATION
health	O
website	O
,	O
Go	O
Ask	O
Alice	PERSON
!	O
:	O
"	O
Studies	O
indicate	O
that	O
about	O
25	O
percent	O
of	O
heterosexual	O
couples	O
have	O
had	O
anal	O
sex	O
at	O
least	O
once	O
,	O
and	O
10	O
percent	O
regularly	O
have	O
anal	O
penetration	O
.	O
"	O
A	O
1993	O
study	O
published	O
in	O
the	ORGANIZATION
Journal	ORGANIZATION
of	ORGANIZATION
the	ORGANIZATION
Royal	ORGANIZATION
Society	ORGANIZATION
of	ORGANIZATION
Medicine	ORGANIZATION
found	O
that	O
fourteen	O
out	O
of	O
a	O
sample	O
of	O
forty	O
men	O
receiving	O
anal	O
intercourse	O
experienced	O
episodes	O
of	O
frequent	O
anal	O
incontinence	O
.	O
The	O
term	O
"	O
Greek	MISC
love	O
"	O
has	O
long	O
been	O
used	O
to	O
refer	O
to	O
the	O
practice	O
,	O
and	O
in	O
modern	O
times	O
,	O
"	O
doing	O
it	O
the	O
Greek	MISC
way	O
"	O
is	O
sometimes	O
used	O
as	O
slang	O
for	O
anal	O
sex	O
.	O
However	O
,	O
homosexual	O
anal	O
sex	O
was	O
far	O
from	O
a	O
universally	O
accepted	O
practice	O
in	O
Ancient	LOCATION
Greece	LOCATION
.	O
It	O
was	O
the	O
target	O
of	O
jokes	O
in	O
surviving	O
comedies	O
;	O
Aristophanes	O
mockingly	O
alludes	O
to	O
the	O
practice	O
,	O
claiming	O
that	O
"	O
Most	O
citizens	O
are	O
europroktoi	O
(	O
wide-arsed	O
)	O
now	O
.	O
"	O
Greek	MISC
courtesans	O
,	O
or	O
hetaerae	ORGANIZATION
,	O
are	O
said	O
to	O
have	O
frequently	O
practiced	O
heterosexual	O
anal	O
intercourse	O
as	O
a	O
means	O
of	O
preventing	O
pregnancy	O
.	O
The	O
acceptability	O
of	O
anal	O
sex	O
thus	O
varied	O
with	O
the	O
time-period	O
and	O
the	O
location	O
,	O
as	O
Ancient	LOCATION
Greece	LOCATION
spanned	O
a	O
long	O
time	O
and	O
stretched	O
over	O
three	O
continents	O
and	O
two	O
major	O
seas	O
.	O
For	O
a	O
male	O
citizen	O
to	O
take	O
the	O
passive	O
(	O
or	O
receptive	O
)	O
role	O
in	O
anal	O
intercourse	O
was	O
condemned	O
in	O
Rome	LOCATION
as	O
an	O
act	O
of	O
impudicitia	PERSON
(	O
immodesty	O
or	O
unchastity	O
)	O
.	O
Another	O
term	O
for	O
the	O
practice	O
,	O
more	O
archaic	O
,	O
is	O
"	O
pedicate	O
"	O
from	O
the	O
Latin	MISC
pedicare	O
,	O
with	O
the	O
same	O
meaning	O
.	O
Among	O
these	O
,	O
in	O
recent	O
times	O
,	O
have	O
been	O
André	PERSON
Gide	PERSON
,	O
who	O
found	O
it	O
repulsive	O
;	O
and	O
Noel	PERSON
Coward	PERSON
,	O
who	O
had	O
a	O
horror	O
of	O
disease	O
,	O
and	O
asserted	O
when	O
young	O
that	O
"	O
I	O
'd	O
never	O
do	O
anything	O
--	O
well	O
the	O
disgusting	O
thing	O
they	O
do	O
--	O
because	O
I	O
know	O
I	O
could	O
get	O
something	O
wrong	O
with	O
me	O
.	O
"	O
This	O
prohibition	O
of	O
the	O
Abrahamic	PERSON
religions	O
against	O
anal	O
sex	O
has	O
been	O
promulgated	O
under	O
the	O
rubric	O
of	O
"	O
sodomy	O
"	O
,	O
which	O
includes	O
various	O
other	O
transgressions	O
of	O
a	O
sexual	O
nature	O
,	O
whether	O
with	O
men	O
,	O
women	O
or	O
animals	O
.	O
This	O
idea	O
is	O
vividly	O
brought	O
to	O
life	O
in	O
the	O
popular	O
interpretation	O
of	O
the	O
story	O
of	O
Sodom	LOCATION
and	O
Gomorrah	LOCATION
,	O
where	O
the	O
people	O
were	O
prone	O
to	O
sexual	O
immorality	O
,	O
and	O
as	O
a	O
result	O
were	O
destroyed	O
.	O
There	O
are	O
conflicting	O
views	O
as	O
to	O
why	O
Sodom	PERSON
was	O
destroyed	O
.	O
The	O
14th	O
Dalai	PERSON
Lama	PERSON
of	O
Tibet	LOCATION
has	O
said	O
that	O
he	O
interprets	O
sexual	O
misconduct	O
to	O
include	O
any	O
sex	O
other	O
than	O
penis-vagina	O
intercourse	O
,	O
including	O
oral	O
sex	O
,	O
anal	O
sex	O
,	O
and	O
masturbation	O
.	O
