The	O
concept	O
was	O
made	O
popular	O
by	O
Karl	PERSON
Popper	PERSON
,	O
who	O
,	O
in	O
his	O
philosophical	O
analysis	O
of	O
the	O
scientific	O
method	O
,	O
concluded	O
that	O
a	O
hypothesis	O
,	O
proposition	O
,	O
or	O
theory	O
is	O
"	O
scientific	O
"	O
only	O
if	O
it	O
is	O
falsifiable	O
.	O
Popper	PERSON
asserted	O
that	O
unfalsifiable	O
statements	O
are	O
non-scientific	O
,	O
but	O
not	O
of	O
zero	O
importance	O
.	O
A	O
falsifiable	O
theory	O
that	O
has	O
withstood	O
severe	O
scientific	O
testing	O
is	O
said	O
to	O
be	O
corroborated	O
by	O
past	O
experience	O
,	O
though	O
in	O
Popper	PERSON
's	O
view	O
this	O
is	O
not	O
equivalent	O
with	O
confirmation	O
and	O
does	O
not	O
lead	O
to	O
the	O
conclusion	O
that	O
the	O
theory	O
is	O
true	O
or	O
even	O
partially	O
true	O
.	O
Popper	PERSON
invented	O
the	O
notion	O
of	O
metaphysical	O
research	O
programs	O
to	O
name	O
such	O
ideas	O
.	O
In	O
contrast	O
to	O
positivism	O
,	O
which	O
held	O
that	O
statements	O
are	O
senseless	O
if	O
they	O
can	O
not	O
be	O
verified	O
or	O
falsified	O
,	O
Popper	PERSON
claimed	O
that	O
falsifiability	O
is	O
merely	O
a	O
special	O
case	O
of	O
the	O
more	O
general	O
notion	O
of	O
criticizability	ORGANIZATION
,	O
even	O
though	O
he	O
admitted	O
that	O
refutation	O
is	O
one	O
of	O
the	O
most	O
effective	O
methods	O
by	O
which	O
theories	O
can	O
be	O
criticized	O
.	O
In	O
work	O
beginning	O
in	O
the	O
1930s	O
,	O
Popper	PERSON
gave	O
falsifiability	O
a	O
renewed	O
emphasis	O
as	O
a	O
criterion	O
of	O
empirical	O
statements	O
in	O
science	O
.	O
Popper	PERSON
noticed	O
that	O
two	O
types	O
of	O
statements	O
are	O
of	O
particular	O
value	O
to	O
scientists	O
.	O
Popper	PERSON
held	O
that	O
science	O
could	O
not	O
be	O
grounded	O
on	O
such	O
an	O
invalid	O
inference	O
.	O
Popper	PERSON
noticed	O
that	O
although	O
a	O
singular	O
existential	O
statement	O
such	O
as	O
'	O
there	O
is	O
a	O
white	O
swan	O
'	O
can	O
not	O
be	O
used	O
to	O
affirm	O
a	O
universal	O
statement	O
,	O
it	O
can	O
be	O
used	O
to	O
show	O
that	O
one	O
is	O
false	O
:	O
the	O
singular	O
existential	O
observation	O
of	O
a	O
black	O
swan	O
serves	O
to	O
show	O
that	O
the	O
universal	O
statement	O
'	O
all	O
swans	O
are	O
white	O
'	O
is	O
false	O
--	O
in	O
logic	O
this	O
is	O
called	O
modus	O
tollens	O
.	O
Popper	PERSON
drew	O
attention	O
to	O
these	O
limitations	O
in	O
The	MISC
Logic	MISC
of	MISC
Scientific	MISC
Discovery	MISC
in	O
response	O
to	O
criticism	O
from	O
Pierre	PERSON
Duhem	PERSON
.	O
W.	PERSON
V.	PERSON
Quine	PERSON
expounded	O
this	O
argument	O
in	O
detail	O
,	O
calling	O
it	O
confirmation	O
holism	O
.	O
But	O
Popper	PERSON
pointed	O
out	O
that	O
it	O
is	O
always	O
possible	O
to	O
change	O
the	O
universal	O
statement	O
or	O
the	O
existential	O
statement	O
so	O
that	O
falsification	O
does	O
not	O
occur	O
.	O
As	O
Popper	PERSON
put	O
it	O
,	O
a	O
decision	O
is	O
required	O
on	O
the	O
part	O
of	O
the	O
scientist	O
to	O
accept	O
or	O
reject	O
the	O
statements	O
that	O
go	O
to	O
make	O
up	O
a	O
theory	O
or	O
that	O
might	O
falsify	O
it	O
.	O
In	O
place	O
of	O
naïve	O
falsification	O
,	O
Popper	PERSON
envisioned	O
science	O
as	O
evolving	O
by	O
the	O
successive	O
rejection	O
of	O
falsified	O
theories	O
,	O
rather	O
than	O
falsified	O
statements	O
.	O
Popper	PERSON
uses	O
falsification	O
as	O
a	O
criterion	O
of	O
demarcation	O
to	O
draw	O
a	O
sharp	O
line	O
between	O
those	O
theories	O
that	O
are	O
scientific	O
and	O
those	O
that	O
are	O
unscientific	O
.	O
Popper	PERSON
claimed	O
that	O
,	O
if	O
a	O
theory	O
is	O
falsifiable	O
,	O
then	O
it	O
is	O
scientific	O
.	O
Popper	PERSON
's	O
own	O
falsificationism	O
,	O
thus	O
,	O
is	O
not	O
only	O
an	O
alternative	O
to	O
verificationism	O
,	O
it	O
is	O
also	O
an	O
acknowledgement	O
of	O
the	O
conceptual	O
distinction	O
that	O
previous	O
theories	O
had	O
ignored	O
.	O
In	O
opposition	O
to	O
this	O
view	O
,	O
Popper	PERSON
emphasized	O
that	O
there	O
are	O
meaningful	O
theories	O
that	O
are	O
not	O
scientific	O
,	O
and	O
that	O
,	O
accordingly	O
,	O
a	O
criterion	O
of	O
meaningfulness	O
does	O
not	O
coincide	O
with	O
a	O
criterion	O
of	O
demarcation	O
.	O
Thus	O
,	O
Popper	PERSON
urged	O
that	O
verifiability	O
be	O
replaced	O
with	O
falsifiability	O
as	O
the	O
criterion	O
of	O
demarcation	O
.	O
Falsifiability	O
was	O
one	O
of	O
the	O
criteria	O
used	O
by	O
Judge	O
William	PERSON
Overton	PERSON
in	O
the	O
McLean	LOCATION
v.	O
Arkansas	LOCATION
ruling	O
to	O
determine	O
that	O
'	O
creation	O
science	O
'	O
was	O
not	O
scientific	O
and	O
should	O
not	O
be	O
taught	O
in	O
Arkansas	LOCATION
public	O
schools	O
.	O
Many	O
contemporary	O
philosophers	O
of	O
science	O
and	O
analytic	O
philosophers	O
are	O
strongly	O
critical	O
of	O
Popper	PERSON
's	O
philosophy	O
of	O
science	O
[	O
citation	O
needed	O
]	O
.	O
Popper	PERSON
's	O
mistrust	O
of	O
inductive	O
reasoning	O
has	O
led	O
to	O
claims	O
that	O
he	O
misrepresents	O
scientific	O
practice	O
.	O
Among	O
the	O
professional	O
philosophers	O
of	O
science	O
,	O
the	O
Popperian	PERSON
view	O
has	O
never	O
been	O
seriously	O
preferred	O
to	O
probabilistic	O
induction	O
,	O
which	O
is	O
the	O
mainstream	O
account	O
of	O
scientific	O
reasoning	O
.	O
Whereas	O
Popper	PERSON
was	O
concerned	O
in	O
the	O
main	O
with	O
the	O
logic	O
of	O
science	O
,	O
Thomas	PERSON
Kuhn	PERSON
's	PERSON
influential	O
book	O
The	MISC
Structure	MISC
of	MISC
Scientific	MISC
Revolutions	MISC
examined	O
in	O
detail	O
the	O
history	O
of	O
science	O
.	O
Kuhn	PERSON
argued	O
that	O
scientists	O
work	O
within	O
a	O
conceptual	O
paradigm	O
that	O
strongly	O
influences	O
the	O
way	O
in	O
which	O
they	O
see	O
data	O
.	O
Some	O
falsificationists	O
saw	O
Kuhn	ORGANIZATION
's	O
work	O
as	O
a	O
vindication	O
,	O
since	O
it	O
provided	O
historical	O
evidence	O
that	O
science	O
progressed	O
by	O
rejecting	O
inadequate	O
theories	O
,	O
and	O
that	O
it	O
is	O
the	O
decision	O
,	O
on	O
the	O
part	O
of	O
the	O
scientist	O
,	O
to	O
accept	O
or	O
reject	O
a	O
theory	O
that	O
is	O
the	O
crucial	O
element	O
of	O
falsificationism	O
.	O
Lakatos	PERSON
attempted	O
to	O
explain	O
Kuhn	PERSON
's	O
work	O
by	O
arguing	O
that	O
science	O
progresses	O
by	O
the	O
falsification	O
of	O
research	O
programs	O
rather	O
than	O
the	O
more	O
specific	O
universal	O
statements	O
of	O
naïve	O
falsification	O
.	O
In	O
Lakatos	PERSON
'	O
approach	O
,	O
a	O
scientist	O
works	O
within	O
a	O
research	O
program	O
that	O
corresponds	O
roughly	O
with	O
Kuhn	ORGANIZATION
's	O
'	O
paradigm	O
'	O
.	O
Whereas	O
Popper	PERSON
rejected	O
the	O
use	O
of	O
ad	O
hoc	O
hypotheses	O
as	O
unscientific	O
,	O
Lakatos	PERSON
accepted	O
their	O
place	O
in	O
the	O
development	O
of	O
new	O
theories	O
.	O
Some	O
philosophers	O
of	O
science	O
,	O
such	O
as	O
Paul	PERSON
Feyerabend	PERSON
,	O
take	O
Kuhn	PERSON
's	O
work	O
as	O
showing	O
that	O
social	O
factors	O
,	O
rather	O
than	O
adherence	O
to	O
a	O
purely	O
rational	O
method	O
,	O
decide	O
which	O
scientific	O
theories	O
gain	O
general	O
acceptance	O
.	O
Many	O
other	O
philosophers	O
of	O
science	O
dispute	O
such	O
a	O
view	O
,	O
such	O
as	O
Alan	PERSON
Sokal	PERSON
and	O
Kuhn	PERSON
himself	O
.	O
Paul	PERSON
Feyerabend	PERSON
examined	O
the	O
history	O
of	O
science	O
with	O
a	O
more	O
critical	O
eye	O
,	O
and	O
ultimately	O
rejected	O
any	O
prescriptive	O
methodology	O
at	O
all	O
.	O
He	O
rejected	O
Lakatos	PERSON
'	O
argument	O
for	O
ad	O
hoc	O
hypothesis	O
,	O
arguing	O
that	O
science	O
would	O
not	O
have	O
progressed	O
without	O
making	O
use	O
of	O
any	O
and	O
all	O
available	O
methods	O
to	O
support	O
new	O
theories	O
.	O
For	O
Feyerabend	O
,	O
any	O
special	O
status	O
that	O
science	O
might	O
have	O
derives	O
from	O
the	O
social	O
and	O
physical	O
value	O
of	O
the	O
results	O
of	O
science	O
rather	O
than	O
its	O
method	O
.	O
Sokal	ORGANIZATION
and	O
Bricmont	ORGANIZATION
write	O
,	O
"	O
When	O
a	O
theory	O
successfully	O
withstands	O
an	O
attempt	O
at	O
falsification	O
,	O
a	O
scientist	O
will	O
,	O
quite	O
naturally	O
,	O
consider	O
the	O
theory	O
to	O
be	O
partially	O
confirmed	O
and	O
will	O
accord	O
it	O
a	O
greater	O
likelihood	O
or	O
a	O
higher	O
subjective	O
probability	O
.	O
But	O
Popper	PERSON
will	O
have	O
none	O
of	O
this	O
:	O
throughout	O
his	O
life	O
he	O
was	O
a	O
stubborn	O
opponent	O
of	O
any	O
idea	O
of	O
'	O
confirmation	O
'	O
of	O
a	O
theory	O
,	O
or	O
even	O
of	O
its	O
'	O
probability	O
'	O
.	O
(	O
Sokal	ORGANIZATION
and	O
Bricmont	ORGANIZATION
1997	ORGANIZATION
,	O
62f	O
)	O
David	PERSON
Miller	PERSON
,	O
a	O
contemporary	O
philosopher	O
of	O
critical	O
rationalism	O
,	O
has	O
attempted	O
to	O
defend	O
Popper	PERSON
against	O
these	O
claims	O
.	O
Popper	PERSON
himself	O
drew	O
a	O
distinction	O
between	O
common	O
descent	O
and	O
the	O
process	O
of	O
natural	O
selection	O
.	O
Popper	O
later	O
recanted	O
,	O
"	O
I	O
have	O
changed	O
my	O
mind	O
about	O
the	O
testability	O
and	O
logical	O
status	O
of	O
the	O
theory	O
of	O
natural	O
selection	O
,	O
and	O
I	O
am	O
glad	O
to	O
have	O
the	O
opportunity	O
to	O
make	O
a	O
recantation	O
.	O
"	O
Popper	PERSON
considered	O
falsifiability	O
a	O
test	O
of	O
whether	O
theories	O
are	O
scientific	O
,	O
not	O
of	O
whether	O
propositions	O
that	O
they	O
contain	O
or	O
support	O
are	O
true	O
.	O
In	O
considering	O
this	O
question	O
,	O
it	O
is	O
helpful	O
to	O
introduce	O
a	O
classical	O
distinction	O
that	O
is	O
frequently	O
emphasized	O
in	O
this	O
connection	O
by	O
Charles	PERSON
Sanders	PERSON
Peirce	PERSON
.	O
That	O
is	O
,	O
since	O
truth	O
values	O
are	O
defined	O
in	O
relation	O
to	O
the	O
laws	O
of	O
logic	O
any	O
"	O
falsification	O
"	O
of	O
these	O
laws	O
would	O
represent	O
a	O
self-contradictory	O
situation	O
though	O
this	O
conclusion	O
has	O
been	O
argued	O
against	O
by	O
philosophers	O
such	O
as	O
W.V.	PERSON
Quine	PERSON
.	O
Many	O
working	O
mathematicians	O
,	O
from	O
Peirce	PERSON
in	O
his	O
day	O
to	O
Stephen	PERSON
Wolfram	PERSON
in	O
ours	O
,	O
have	O
remarked	O
on	O
the	O
active	O
,	O
observational	O
,	O
and	O
even	O
experimental	O
character	O
of	O
mathematical	O
work	O
.	O
Lakatos	PERSON
argues	O
that	O
often	O
axioms	O
,	O
definitions	O
,	O
and	O
proofs	O
evolve	O
through	O
criticism	O
and	O
counterexample	O
in	O
a	O
manner	O
not	O
unlike	O
the	O
way	O
that	O
a	O
scientific	O
theory	O
evolves	O
in	O
response	O
to	O
experiments	O
.	O
