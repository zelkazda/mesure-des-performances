The	O
AA	ORGANIZATION
compiler	O
generated	O
range-checking	O
for	O
array	O
accesses	O
,	O
and	O
allowed	O
an	O
array	O
to	O
have	O
dimensions	O
that	O
were	O
determined	O
at	O
run-time	O
,	O
where	O
i	O
and	O
j	O
were	O
calculated	O
values	O
)	O
.	O
Atlas	PERSON
Autocode	PERSON
included	O
a	O
complex	O
data	O
type	O
which	O
would	O
support	O
complex	O
numbers	O
(	O
for	O
example	O
,	O
the	O
square	O
root	O
of	O
-1	O
)	O
.	O
This	O
'	O
complex	O
'	O
feature	O
was	O
dropped	O
when	O
Atlas	PERSON
Autocode	PERSON
later	O
morphed	O
into	O
the	O
Edinburgh	LOCATION
IMP	O
programming	O
language	O
.	O
(	O
Imp	ORGANIZATION
was	O
an	O
extension	O
of	O
AA	ORGANIZATION
and	O
was	O
notable	O
for	O
being	O
used	O
to	O
write	O
the	O
EMAS	ORGANIZATION
operating	O
system	O
.	O
)	O
Because	O
of	O
this	O
keyword	O
stropping	O
,	O
it	O
was	O
possible	O
for	O
AA	ORGANIZATION
to	O
allow	O
spaces	O
in	O
variable	O
names	O
,	O
such	O
as	O
integer	O
previous	O
value	O
.	O
The	O
flexowriter	O
supported	O
overstriking	O
and	O
therefore	O
AA	ORGANIZATION
did	O
as	O
well	O
--	O
up	O
to	O
three	O
characters	O
could	O
be	O
overstruck	PERSON
as	O
a	O
single	O
symbol	O
.	O
A	O
variant	O
of	O
the	O
AA	ORGANIZATION
compiler	O
included	O
run-time	O
support	O
for	O
a	O
top-down	O
recursive	O
descent	O
parser	O
.	O
