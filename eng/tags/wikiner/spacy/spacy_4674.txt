He	O
also	O
was	O
the	O
founder	O
of	O
the	ORGANIZATION
Order	ORGANIZATION
of	ORGANIZATION
Friars	ORGANIZATION
Minor	ORGANIZATION
,	O
more	O
commonly	O
known	O
as	O
the	O
Franciscans	MISC
.	O
A	O
strange	O
vision	O
made	O
him	O
return	O
to	O
Assisi	PERSON
,	O
deepening	O
his	O
ecclesiastical	O
awakening	O
.	O
By	O
degrees	O
he	O
took	O
to	O
nursing	O
lepers	O
,	O
the	O
most	O
repulsive	O
victims	O
in	O
the	O
lazar	PERSON
houses	O
near	O
Assisi	LOCATION
.	O
For	O
the	O
next	O
couple	O
of	O
months	O
he	O
lived	O
as	O
a	O
beggar	O
in	O
the	O
region	O
of	O
Assisi	LOCATION
.	O
Returning	O
to	O
the	O
countryside	O
around	O
the	O
town	O
for	O
two	O
years	O
this	O
time	O
,	O
he	O
restored	O
several	O
ruined	O
churches	O
,	O
among	O
them	O
the	O
Porziuncola	ORGANIZATION
--	O
little	O
chapel	O
of	O
St	LOCATION
Mary	LOCATION
of	O
the	O
Angels	ORGANIZATION
--	O
just	O
outside	O
the	O
town	O
,	O
which	O
later	O
became	O
his	O
favorite	O
abode	O
.	O
The	O
sermon	O
was	O
about	O
Matthew	PERSON
10:9	PERSON
,	O
in	O
which	O
Christ	O
tells	O
his	O
followers	O
they	O
should	O
go	O
forth	O
and	O
proclaim	O
that	O
the	LOCATION
Kingdom	LOCATION
of	LOCATION
Heaven	LOCATION
was	O
upon	O
them	O
,	O
that	O
they	O
should	O
take	O
no	O
money	O
with	O
them	O
,	O
nor	O
even	O
a	O
walking	O
stick	O
or	O
shoes	O
for	O
the	O
road	O
.	O
He	O
was	O
soon	O
joined	O
by	O
his	O
first	O
follower	O
,	O
a	O
prominent	O
fellow	O
townsman	O
,	O
the	O
jurist	O
Bernardo	PERSON
di	PERSON
Quintavalle	PERSON
,	O
who	O
contributed	O
all	O
that	O
he	O
had	O
to	O
the	O
work	O
.	O
Back	O
in	O
Assisi	LOCATION
,	O
several	O
noblemen	O
(	O
among	O
them	O
Tommaso	ORGANIZATION
da	ORGANIZATION
Celano	ORGANIZATION
,	O
who	O
would	O
later	O
write	O
the	O
biography	O
of	O
St.	LOCATION
Francis	LOCATION
)	O
and	O
some	O
well-educated	O
men	O
joined	O
his	O
order	O
.	O
During	O
this	O
time	O
,	O
he	O
probably	O
met	O
Dominic	PERSON
de	PERSON
Guzman	PERSON
.	O
Crossing	O
the	O
lines	O
between	O
the	O
sultan	O
and	O
the	O
Crusaders	MISC
in	O
Damietta	LOCATION
,	O
he	O
was	O
received	O
by	O
the	O
sultan	O
Melek-el-Kamel	O
.	O
He	O
used	O
real	O
animals	O
to	O
create	O
a	O
living	O
scene	O
so	O
that	O
the	O
worshipers	O
could	O
contemplate	O
the	O
birth	O
of	O
the	O
child	O
Jesus	O
in	O
a	O
direct	O
way	O
,	O
making	O
use	O
of	O
the	O
senses	O
,	O
especially	O
sight	O
.	O
In	O
the	O
end	O
,	O
he	O
was	O
brought	O
back	O
to	O
a	O
hut	O
next	O
to	O
the	O
Porziuncola	ORGANIZATION
.	O
On	O
July	O
16	O
,	O
1228	O
,	O
he	O
was	O
pronounced	O
a	O
saint	O
by	O
Pope	O
Gregory	PERSON
IX	PERSON
.	O
The	O
next	O
day	O
,	O
the	O
Pope	O
laid	O
the	O
foundation	O
stone	O
for	O
the	MISC
Basilica	MISC
of	MISC
Saint	MISC
Francis	MISC
in	O
Assisi	O
.	O
In	O
1978	O
the	O
remains	O
of	O
St.	LOCATION
Francis	LOCATION
were	O
identified	O
by	O
a	O
commission	O
of	O
scholars	O
appointed	O
by	O
Pope	O
Paul	PERSON
VI	PERSON
,	O
and	O
put	O
in	O
a	O
glass	O
urn	O
in	O
the	O
ancient	O
stone	O
tomb	O
.	O
Saint	MISC
Francis	MISC
is	O
considered	O
the	O
first	O
Italian	MISC
poet	O
by	O
literary	O
critics	O
.	O
Saint	LOCATION
Francis	LOCATION
's	LOCATION
feast	O
day	O
is	O
observed	O
on	O
October	O
4	O
.	O
Pius	PERSON
XII	PERSON
also	O
mentioned	O
the	O
two	O
saints	O
in	O
the	O
laudative	O
discourse	O
he	O
pronounced	O
on	O
May	O
5	O
,	O
1949	O
in	O
the	O
Santa	O
Maria	O
sopra	ORGANIZATION
Minerva	ORGANIZATION
church	O
.	O
Many	O
of	O
the	O
stories	O
that	O
surround	O
the	O
life	O
of	O
St.	LOCATION
Francis	LOCATION
deal	O
with	O
his	O
love	O
for	O
animals	O
.	O
In	O
this	O
manner	O
Gubbio	PERSON
was	O
freed	O
from	O
the	O
menace	O
of	O
the	O
predator	O
.	O
Legend	O
has	O
it	O
that	O
St.	LOCATION
Francis	LOCATION
on	O
his	O
deathbed	O
thanked	O
his	O
donkey	O
for	O
carrying	O
and	O
helping	O
him	O
throughout	O
his	O
life	O
,	O
and	O
his	O
donkey	O
wept	O
.	O
