In	O
World	MISC
War	MISC
I	MISC
,	O
the	O
majority	O
of	O
all	O
deaths	O
were	O
caused	O
by	O
cannon	O
;	O
they	O
were	O
also	O
used	O
widely	O
in	O
World	MISC
War	MISC
II	MISC
.	O
Most	O
modern	O
cannon	O
are	O
similar	O
to	O
those	O
used	O
in	O
the	MISC
Second	MISC
World	MISC
War	MISC
,	O
except	O
for	O
heavy	O
naval	O
guns	O
,	O
which	O
have	O
been	O
replaced	O
by	O
missiles	O
.	O
In	O
particular	O
,	O
autocannon	O
have	O
remained	O
nearly	O
identical	O
to	O
their	O
World	MISC
War	MISC
II	MISC
counterparts	O
.	O
Autocannon	ORGANIZATION
have	O
been	O
used	O
extensively	O
in	O
fighter	O
aircraft	O
since	O
World	MISC
War	MISC
II	MISC
,	O
and	O
are	O
sometimes	O
used	O
on	O
land	O
vehicles	O
.	O
The	O
earliest	O
known	O
depiction	O
of	O
a	O
gun	O
is	O
a	O
sculpture	O
from	O
a	O
cave	O
in	O
Sichuan	LOCATION
,	O
dating	O
to	O
the	O
12th	O
century	O
,	O
that	O
portrays	O
a	O
figure	O
carrying	O
a	O
vase-shaped	O
bombard	O
,	O
firing	O
flames	O
and	O
a	O
ball	O
.	O
The	O
world	O
's	O
earliest	O
known	O
cannon	O
,	O
dated	O
1282	O
,	O
was	O
found	O
in	O
Mongol-held	MISC
Manchuria	LOCATION
.	O
Joseph	PERSON
Needham	PERSON
suggests	O
that	O
the	O
proto-shells	O
described	O
in	O
the	O
Huolongjing	LOCATION
may	O
be	O
among	O
the	O
first	O
of	O
their	O
kind	O
.	O
The	O
weapon	O
was	O
later	O
taken	O
up	O
by	O
both	O
the	O
Mongol	MISC
conquerors	O
and	O
the	O
Koreans	MISC
.	O
In	O
the	O
1593	O
Siege	O
of	O
Pyongyang	LOCATION
,	O
40,000	O
Ming	O
troops	O
deployed	O
a	O
variety	O
of	O
cannon	O
to	O
bombard	O
an	O
equally	O
large	O
Japanese	MISC
army	O
.	O
Similar	O
cannon	O
were	O
also	O
used	O
at	O
the	MISC
Siege	MISC
of	MISC
Calais	MISC
,	O
in	O
the	O
same	O
year	O
,	O
although	O
it	O
was	O
not	O
until	O
the	O
1380s	O
that	O
the	O
"	O
ribaudekin	O
"	O
clearly	O
became	O
mounted	O
on	O
wheels	O
.	O
The	O
first	O
cannon	O
appeared	O
in	O
Russia	LOCATION
around	O
1380	O
,	O
though	O
they	O
were	O
used	O
only	O
in	O
sieges	O
,	O
often	O
by	O
the	O
defenders	O
.	O
Around	O
the	O
same	O
period	O
,	O
the	LOCATION
Byzantine	LOCATION
Empire	LOCATION
began	O
to	O
accumulate	O
its	O
own	O
cannon	O
to	O
face	O
the	O
Ottoman	MISC
threat	O
,	O
starting	O
with	O
medium-sized	O
cannon	O
3	O
feet	O
(	O
0.91	O
m	O
)	O
long	O
and	O
of	O
10	O
in	O
caliber	O
.	O
The	O
first	O
definite	O
use	O
of	O
artillery	O
in	O
the	O
region	O
was	O
against	O
the	O
Ottoman	MISC
siege	O
of	O
Constantinople	LOCATION
,	O
in	O
1396	O
,	O
forcing	O
the	O
Ottomans	MISC
to	O
withdraw	O
.	O
They	O
acquired	O
their	O
own	O
cannon	O
,	O
and	O
laid	O
siege	O
to	O
the	O
Byzantine	MISC
capital	O
again	O
,	O
in	O
1422	O
,	O
using	O
"	O
falcons	O
"	O
,	O
which	O
were	O
short	O
but	O
wide	O
cannon	O
.	O
By	O
1453	O
,	O
the	O
Ottomans	MISC
used	O
68	O
Hungarian-made	MISC
cannon	O
for	O
the	O
55-day	O
bombardment	O
of	O
the	O
walls	O
of	O
Constantinople	LOCATION
,	O
"	O
hurling	O
the	O
pieces	O
everywhere	O
and	O
killing	O
those	O
who	O
happened	O
to	O
be	O
nearby	O
.	O
"	O
However	O
,	O
wooden	O
"	O
battery-towers	O
"	O
took	O
on	O
a	O
similar	O
role	O
as	O
siege	O
towers	O
in	O
the	O
gunpowder	O
age	O
--	O
such	O
as	O
that	O
used	O
at	O
siege	O
of	O
Kazan	LOCATION
in	O
1552	O
,	O
which	O
could	O
hold	O
ten	O
large-caliber	O
cannon	O
,	O
in	O
addition	O
to	O
50	O
lighter	O
pieces	O
.	O
Niccolò	PERSON
Machiavelli	PERSON
wrote	O
,	O
"	O
There	O
is	O
no	O
wall	O
,	O
whatever	O
its	O
thickness	O
that	O
artillery	O
will	O
not	O
destroy	O
in	O
only	O
a	O
few	O
days	O
.	O
"	O
In	O
The	MISC
Art	MISC
of	MISC
War	MISC
,	O
Niccolò	PERSON
Machiavelli	PERSON
observed	O
that	O
"	O
It	O
is	O
true	O
that	O
the	O
arquebuses	O
and	O
the	O
small	O
artillery	O
do	O
much	O
more	O
harm	O
than	O
the	O
heavy	O
artillery	O
.	O
"	O
Also	O
,	O
Adolphus	ORGANIZATION
's	O
army	O
was	O
the	O
first	O
to	O
use	O
a	O
special	O
cartridge	O
that	O
contained	O
both	O
powder	O
and	O
shot	O
,	O
which	O
sped	O
up	O
loading	O
,	O
and	O
therefore	O
increased	O
the	O
rate	O
of	O
fire	O
.	O
At	O
the	O
time	O
,	O
for	O
each	O
thousand	O
infantrymen	O
,	O
there	O
was	O
one	O
cannon	O
on	O
the	O
battlefield	O
;	O
Gustavus	PERSON
Adolphus	PERSON
increased	O
the	O
number	O
of	O
cannon	O
in	O
his	O
army	O
so	O
dramatically	O
,	O
that	O
there	O
were	O
six	O
cannon	O
for	O
each	O
one	O
thousand	O
infantry	O
.	O
Careful	O
sapping	O
forward	O
,	O
supported	O
by	O
enfilading	O
ricochet	O
fire	O
,	O
was	O
a	O
key	O
feature	O
of	O
this	O
system	O
,	O
and	O
it	O
even	O
allowed	O
Vauban	PERSON
to	O
calculate	O
the	O
length	O
of	O
time	O
a	O
siege	O
would	O
take	O
.	O
These	O
principles	O
were	O
followed	O
into	O
the	O
mid-19th	O
century	O
,	O
when	O
changes	O
in	O
armaments	O
necessitated	O
greater	O
depth	O
defense	O
than	O
Vauban	PERSON
had	O
provided	O
for	O
.	O
It	O
was	O
only	O
in	O
the	O
years	O
prior	O
to	O
World	MISC
War	MISC
I	MISC
that	O
new	O
works	O
began	O
to	O
break	O
radically	O
away	O
from	O
his	O
designs	O
.	O
The	ORGANIZATION
United	ORGANIZATION
States	ORGANIZATION
Navy	ORGANIZATION
tested	O
guns	O
by	O
measuring	O
them	O
,	O
firing	O
them	O
two	O
or	O
three	O
times	O
--	O
termed	O
"	O
proof	O
by	O
powder	O
"	O
--	O
and	O
using	O
pressurized	O
water	O
to	O
detect	O
leaks	O
.	O
The	O
carronade	O
was	O
adopted	O
by	O
the	ORGANIZATION
Royal	ORGANIZATION
Navy	ORGANIZATION
in	O
1779	O
;	O
the	O
lower	O
muzzle	O
velocity	O
of	O
the	O
round	O
shot	O
when	O
fired	O
from	O
this	O
cannon	O
was	O
intended	O
to	O
create	O
more	O
wooden	O
splinters	O
when	O
hitting	O
the	O
structure	O
of	O
an	O
enemy	O
vessel	O
,	O
as	O
they	O
were	O
believed	O
to	O
be	O
deadly	O
.	O
As	O
a	O
result	O
,	O
the	O
classification	O
of	O
Royal	ORGANIZATION
Navy	ORGANIZATION
vessels	O
in	O
this	O
period	O
can	O
be	O
misleading	O
,	O
as	O
they	O
often	O
carried	O
more	O
cannon	O
than	O
were	O
listed	O
.	O
The	O
carronade	O
,	O
although	O
initially	O
very	O
successful	O
and	O
widely	O
adopted	O
,	O
disappeared	O
from	O
the	ORGANIZATION
Royal	ORGANIZATION
Navy	ORGANIZATION
in	O
the	O
1850s	O
,	O
after	O
the	O
development	O
of	O
steel	O
,	O
jacketed	O
cannon	O
,	O
by	O
William	PERSON
George	PERSON
Armstrong	PERSON
and	O
Joseph	PERSON
Whitworth	PERSON
.	O
Another	O
is	O
the	O
smoothbore	O
12	O
pounder	O
Napoleon	MISC
,	O
which	O
was	O
renowned	O
for	O
its	O
sturdiness	O
,	O
reliability	O
,	O
firepower	O
,	O
flexibility	O
,	O
relatively	O
light	O
weight	O
,	O
and	O
range	O
of	O
1700	O
m	O
(	O
5600	O
ft	O
)	O
.	O
Cannon	O
were	O
crucial	O
in	O
Napoleon	PERSON
Bonaparte	PERSON
's	PERSON
rise	O
to	O
power	O
,	O
and	O
continued	O
to	O
play	O
an	O
important	O
role	O
in	O
his	O
army	O
in	O
later	O
years	O
.	O
When	O
Napoleon	ORGANIZATION
arrived	O
,	O
he	O
reorganized	O
the	O
defenses	O
,	O
while	O
realizing	O
that	O
without	O
cannon	O
,	O
the	O
city	O
could	O
not	O
be	O
held	O
.	O
The	O
slaughter	O
effectively	O
ended	O
the	O
threat	O
to	O
the	O
new	O
government	O
,	O
while	O
,	O
at	O
the	O
same	O
time	O
,	O
made	O
Bonaparte	PERSON
a	O
famous	O
--	O
and	O
popular	O
--	O
public	O
figure	O
.	O
According	O
to	O
NATO	ORGANIZATION
,	O
the	O
general	O
role	O
of	O
artillery	O
is	O
to	O
provide	O
fire	O
support	O
,	O
which	O
is	O
defined	O
as	O
"	O
the	O
application	O
of	O
fire	O
,	O
coordinated	O
with	O
the	O
maneuver	O
of	O
forces	O
to	O
destroy	O
,	O
neutralize	O
,	O
or	O
suppress	O
the	O
enemy	O
.	O
"	O
Despite	O
the	O
change	O
to	O
indirect	O
fire	O
,	O
cannon	O
still	O
proved	O
highly	O
effective	O
during	O
World	MISC
War	MISC
I	MISC
,	O
causing	O
over	O
75	O
%	O
of	O
casualties	O
.	O
The	O
onset	O
of	O
trench	O
warfare	O
after	O
the	O
first	O
few	O
months	O
of	O
World	O
War	O
I	O
greatly	O
increased	O
the	O
demand	O
for	O
howitzers	O
,	O
as	O
they	O
fired	O
at	O
a	O
steep	O
angle	O
,	O
and	O
were	O
thus	O
better	O
suited	O
than	O
guns	O
at	O
hitting	O
targets	O
in	O
trenches	O
.	O
World	MISC
War	MISC
I	MISC
also	O
marked	O
the	O
use	O
of	O
the	MISC
Paris	MISC
Gun	MISC
,	O
the	O
longest-ranged	O
gun	O
ever	O
fired	O
.	O
The	MISC
Second	MISC
World	MISC
War	MISC
sparked	O
new	O
developments	O
in	O
cannon	O
technology	O
.	O
For	O
example	O
,	O
the	MISC
Panzer	MISC
III	MISC
was	O
originally	O
designed	O
with	O
a	O
37	O
mm	O
gun	O
,	O
but	O
was	O
mass	O
produced	O
with	O
a	O
50	O
mm	O
cannon	O
.	O
In	O
1944	O
,	O
the	O
8.8	O
cm	O
KwK	O
43	O
--	O
and	O
its	O
multiple	O
variations	O
--	O
entered	O
service	O
,	O
used	O
by	O
the	O
Wehrmacht	ORGANIZATION
,	O
and	O
was	O
adapted	O
to	O
be	O
both	O
a	O
tank	O
's	O
main	O
gun	O
,	O
and	O
the	O
PaK	ORGANIZATION
43	O
anti-tank	O
gun	O
.	O
One	O
of	O
the	O
most	O
powerful	O
guns	O
to	O
see	O
service	O
in	O
World	MISC
War	MISC
II	MISC
,	O
it	O
was	O
capable	O
of	O
destroying	O
any	O
Allied	ORGANIZATION
tank	O
at	O
very	O
long	O
ranges	O
.	O
The	ORGANIZATION
United	ORGANIZATION
States	ORGANIZATION
Army	ORGANIZATION
,	O
for	O
example	O
,	O
sought	O
a	O
lighter	O
,	O
more	O
versatile	O
howitzer	O
,	O
to	O
replace	O
their	O
aging	O
pieces	O
.	O
As	O
it	O
could	O
be	O
towed	O
,	O
the	O
M198	ORGANIZATION
was	O
selected	O
to	O
be	O
the	O
successor	O
to	O
the	MISC
World	MISC
War	MISC
II	MISC
--	O
era	O
cannon	O
used	O
at	O
the	O
time	O
,	O
and	O
entered	O
service	O
in	O
1979	O
.	O
Although	O
land-based	O
artillery	O
such	O
as	O
the	O
M198	ORGANIZATION
are	O
powerful	O
,	O
long-ranged	O
,	O
and	O
accurate	O
,	O
naval	O
guns	O
have	O
not	O
been	O
neglected	O
,	O
despite	O
being	O
much	O
smaller	O
than	O
in	O
the	O
past	O
,	O
and	O
,	O
in	O
some	O
cases	O
,	O
having	O
been	O
replaced	O
by	O
cruise	O
missiles	O
.	O
The	O
warhead	O
,	O
which	O
weighs	O
24	O
pounds	O
(	O
11	O
kg	O
)	O
,	O
has	O
a	O
circular	O
error	O
of	O
probability	O
of	O
50	O
m	O
(	O
160	O
ft	O
)	O
,	O
and	O
will	O
be	O
mounted	O
on	O
a	O
rocket	O
,	O
to	O
increase	O
the	O
effective	O
range	O
to	O
100	O
nmi	O
(	O
190	O
km	O
)	O
--	O
a	O
longer	O
range	O
than	O
that	O
of	O
the	MISC
Paris	MISC
Gun	MISC
.	O
The	O
combined	O
firepower	O
from	O
both	O
turrets	O
will	O
give	O
Zumwalt	PERSON
-class	O
destroyers	O
the	O
firepower	O
equivalent	O
to	O
18	O
conventional	O
M-198	LAW
howitzers	O
.	O
The	O
reason	O
for	O
the	O
re-integration	O
of	O
cannon	O
as	O
a	O
main	O
armament	O
in	O
United	ORGANIZATION
States	ORGANIZATION
Navy	ORGANIZATION
ships	O
is	O
because	O
satellite-guided	O
munitions	O
fired	O
from	O
a	O
gun	O
are	O
far	O
less	O
expensive	O
than	O
a	O
cruise	O
missile	O
,	O
and	O
are	O
therefore	O
a	O
better	O
alternative	O
to	O
many	O
combat	O
situations	O
.	O
While	O
there	O
is	O
no	O
minimum	O
bore	O
for	O
autocannon	O
,	O
they	O
are	O
usually	O
larger	O
than	O
machine	O
guns	O
,	O
typically	O
20	O
mm	O
or	O
greater	O
since	O
World	MISC
War	MISC
II	MISC
.	O
A	O
typical	O
autocannon	O
is	O
the	O
25	O
mm	O
"	O
Bushmaster	PERSON
"	O
chain	O
gun	O
,	O
mounted	O
on	O
the	O
LAV-25	O
and	O
M2	PERSON
Bradley	PERSON
armored	O
vehicles	O
.	O
The	O
first	O
airborne	O
cannon	O
appeared	O
in	O
World	MISC
War	MISC
II	MISC
,	O
but	O
each	O
airplane	O
could	O
carry	O
only	O
one	O
or	O
two	O
,	O
as	O
large	O
bore	O
cannon	O
and	O
their	O
ammunition	O
are	O
generally	O
heavier	O
than	O
machine	O
guns	O
,	O
the	O
standard	O
armament	O
.	O
The	ORGANIZATION
Hispano-Suiza	ORGANIZATION
HS.404	ORGANIZATION
,	O
Oerlikon	ORGANIZATION
20	O
mm	O
cannon	O
,	O
MG	ORGANIZATION
FF	ORGANIZATION
,	O
and	O
their	O
numerous	O
variants	O
became	O
among	O
the	O
most	O
widely	O
used	O
autocannon	O
in	O
the	O
war	O
.	O
Nearly	O
all	O
modern	O
fighter	O
aircraft	O
are	O
armed	O
with	O
an	O
autocannon	O
,	O
and	O
most	O
are	O
derived	O
from	O
their	O
counterparts	O
from	O
the	MISC
Second	MISC
World	MISC
War	MISC
.	O
For	O
this	O
reason	O
,	O
both	O
the	O
25	O
mm	PERSON
Bushmaster	PERSON
and	O
the	O
30	O
mm	O
RARDEN	O
are	O
deliberately	O
designed	O
with	O
relatively	O
slow	O
rates	O
of	O
fire	O
,	O
so	O
that	O
they	O
can	O
operate	O
for	O
longer	O
with	O
a	O
limited	O
supply	O
of	O
ammunition	O
.	O
Systems	O
with	O
multiple	O
barrels	O
--	O
gatling	O
guns	O
--	O
can	O
have	O
rates	O
of	O
fire	O
of	O
several	O
thousand	O
rounds	O
per	O
minute	O
;	O
the	O
fastest	O
of	O
these	O
is	O
the	MISC
GSh-6-30K	MISC
,	O
which	O
has	O
a	O
rate	O
of	O
fire	O
of	O
over	O
6,000	O
rounds	O
per	O
minute	O
.	O
Giuseppe	PERSON
Sarti	PERSON
is	O
believed	O
to	O
be	O
the	O
first	O
composer	O
to	O
orchestrate	O
real	O
cannon	O
in	O
a	O
musical	O
work	O
.	O
However	O
,	O
the	O
overture	O
was	O
not	O
recorded	O
with	O
real	O
cannon	O
fire	O
until	O
Mercury	ORGANIZATION
Records	ORGANIZATION
and	O
conductor	O
Antal	PERSON
Doráti	PERSON
's	O
1958	O
recording	O
of	O
the	ORGANIZATION
Minnesota	ORGANIZATION
Orchestra	ORGANIZATION
.	O
The	O
hard	O
rock	O
band	O
AC/DC	ORGANIZATION
also	O
used	O
cannon	O
in	O
their	O
song	O
"	O
For	O
Those	O
About	O
to	O
Rock	O
"	O
.	O
