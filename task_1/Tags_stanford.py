import os
import wget
import zipfile
from tqdm import tqdm
from pathlib import Path
from nltk.tag import StanfordNERTagger
from concurrent.futures import ThreadPoolExecutor 

#create the models folder if it does not exists 
Path("./models").mkdir(parents=True, exist_ok=True)

# download and unzip the stanford model if needed
if(not os.path.exists('./models/stanford-ner-4.2.0')):
    if(not os.path.exists('./models/stanford-ner-4.2.0.zip')):
        print("Downloading model...")
        wget.download("https://nlp.stanford.edu/software/stanford-ner-4.2.0.zip", out="models/")
    with zipfile.ZipFile('./models/stanford-ner-4.2.0.zip', 'r') as zip_ref:
        zip_ref.extractall("./models")

stanford_ner_path = './models/stanford-ner-2020-11-17'
jar_file = os.path.join(stanford_ner_path, 'stanford-ner.jar')
model_file = os.path.join(stanford_ner_path, 'classifiers/english.all.3class.distsim.crf.ser.gz')

# InitialiSe  NER tagger
ner_tagger = StanfordNERTagger(model_file, jar_file)

input_folder = './eng/data/wikiner/articles'
output_folder = './eng/tags/wikiner/stanford'
Path(output_folder).mkdir(parents=True, exist_ok=True)

print("Tagging with Stanford...")
for filename in tqdm(os.listdir(input_folder)):
    if filename.startswith('article') and filename.endswith('.txt'):
        # les fichiers d'entrée porte le nom article_{i}.txt avec numero attribué à l'article lors de sa création

        output_filename = filename.replace('article', 'stanford')

        # lit l'article
        with open(os.path.join(input_folder, filename), 'r', encoding='utf-8') as file:
            article = file.read()

        article_encoded = article.replace('/', 'SLASHH')
        """
        la ligne juste avant est bien nécessaire, en effet la tokenization par .split() dans la ligne après fait la tâche
        mais en appelant le ner_tagger.tag() juste après , ce dernier supprime le caractère '/' des token ( transorme '9/11' en '911' par exemple)
        Pour cela en replace tout les '/' dans l'article par 'SLASHH' (pas SLASH car des articles contiennent déjà cette chaine de caractère) 
        et on refait l'opération inverse lors de l'écriture dans le fichier sortie
        """
        tokenized_article = article_encoded.split()

        #  NER
        ner_results = ner_tagger.tag(tokenized_article)

        # output filename
        output_file_path = os.path.join(output_folder, output_filename)

        # Write the results to an output file
        with open(output_file_path, 'w', encoding='utf-8') as output_file:
            for token, tag in ner_results: 
                #les tag générés par stanford sont {Person , Location , Oragnization , O} pas de MISC
                token = token.replace('SLASHH', '/')
                output_file.write(f'{token}\t{tag}\n')

    

        #print(f"NER results for {filename} saved to {output_file_path}")
