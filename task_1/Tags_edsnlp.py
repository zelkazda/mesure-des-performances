import edsnlp
import os
from tqdm import tqdm
from pathlib import Path
import huggingface_hub


# Replace YOUR_TOKEN with the token you generated and copied
token = "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"
# Log in to Hugging Face and register the token on your machine
huggingface_hub.login(token=token, new_session=False, add_to_git_credential=True)
nlp = edsnlp.load("AP-HP/eds-pseudo-public", auto_update=True)

input_folder = './eng/data/wikiner/articles'
output_folder = './eng/tags/wikiner/edsnlp'
Path(output_folder).mkdir(parents=True, exist_ok=True)

def replace (entity):
    """
    """
    if entity in  {'DATE','DATE_NAISSANCE','MAIL','TEL','SECU','IPP'}:
        return 'O'
    elif entity in {'NOM','PRENOM'}:
        return 'PERSON'
    elif entity in {'VILLE','ZIP','ADRESSE'}:
        return 'LOCATION'
    elif entity == 'HOPITAL':
        return 'ORGANIZATION'
    else :
        return 'O'




print("Tagging with Edsnlp...")    
for filename in tqdm(os.listdir(input_folder)):
    if filename.startswith('article') and filename.endswith('.txt'):
        # les fichiers d'entrée porte le nom article_{i}.txt avec numero attribué à l'article lors de sa création
        output_filename = filename.replace('article', 'edsnlp')
        
        existing = os.listdir(output_folder) 
        if output_filename in existing :
            print(output_filename , 'already exists')
            continue

        with open(os.path.join(input_folder, filename), 'r', encoding='utf-8') as file:
            article = file.read()

        doc = nlp(article)

        tokens = article.split()

        token_index = 0
        output_tokens = []
        for token in tokens:
            entity_tag = 'O' # par défaut l'entité est 'O'
            #pour chaque token , on parcours les entités reconnues :
            #si l'index du token est compris entre le debut et la fin d'une entité reconnue , on lui attribue son entity_type
            #On sort de la boucle si l'index de la fin du token est plus grand que celui de l'entité courante 
            # (pas besoin de parcourir toute les entités pour chaque token)
            for result in doc.ents:
                if result.start_char <= token_index < result.end_char:
                    entity_tag = replace(result.label_)
                    break
                elif token_index+len(token) < result.start_char:
                    #print(token , 'i stopped at ', article[result.start:result.end])
                    break
            output_tokens.append((token, entity_tag))
            token_index += len(token) + 1  # +1 pour l'espace après le token dans le text


        #on construit le chemin de sortie
        output_file_path = os.path.join(output_folder, output_filename)
        
        with open(output_file_path, 'w', encoding='utf-8') as output_file:
            for token, tag in output_tokens:
                if token.strip():
                    output_file.write(f'{token}\t{tag}\n')
        
        #print(f"NER results for {filename} saved to {output_file_path}")
