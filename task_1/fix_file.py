import os
from tqdm import tqdm

def fix_file( tag_path , annot_path):
    """
    fonction qui sert à fixer le décalage entre le fichier annotation et la sortie du modèle sur étagère (chaqu'un tokenize de sa manière)
    Params : tag_path : chemin du fichier sortie du benchmark
    annot_path : chemin du fichier annotation
    """

    #lecture des deux fichiers
    with open (tag_path , 'r', encoding='utf-8') as file:
        Tlines = file.readlines()
    with open (annot_path , 'r', encoding='utf-8') as otherfile:
        Alines = otherfile.readlines()

    TagCompteur,AnnotCompteur=0,0 
    #chaque ligne , dans les deux fichiers est de la forme  token\tentity
    while TagCompteur<len(Tlines) and AnnotCompteur<len(Alines):

        Ttoken  =Tlines[TagCompteur].strip().split()
        Tword , Ttag = ' '.join(Ttoken[:-1]) , Ttoken[-1]
         #il se peut que le token soit un ensemble de mot séparés par des espaces , pour cela Tword = Ttoken[0] n'est pas toujours valide

        Atoken = Alines[AnnotCompteur].strip().split()
        Aword , Atag = ' '.join(Atoken[:-1]) , Atoken[-1]

        if Tword == Aword :
            TagCompteur+=1 
            AnnotCompteur+=1
            continue

        elif Tword in Aword:
            #tant que le Tword est partie du Aword, on ajoute le mot suivant dans le fichier et on le supprime de la liste
            while ( TagCompteur+1 < len(Tlines) and Tword != Aword and Tword in Aword):
                Tword += Tlines[TagCompteur+1].strip().split()[:-1][0] 
                del Tlines[TagCompteur+1]
            Tlines[TagCompteur]= Tword + '\t' + Ttag + '\n' # l'entité associé est celui du premier mot 
            if (Tword==Aword):
                TagCompteur+=1 
                AnnotCompteur+=1
            else : continue

        elif Aword in Tword:
            #meme chose que précedemment mais dans le sens inverse
            while ( AnnotCompteur+1 < len(Alines) and Tword != Aword and Aword in Tword):
                Aword += Alines[AnnotCompteur+1].strip().split()[:-1][0]
                del Alines[AnnotCompteur+1]
            Alines[AnnotCompteur]= Aword +'\t' +Atag +'\n'  
            if (Tword==Aword):
                TagCompteur+=1 
                AnnotCompteur+=1
            else : continue
        else:
            #si aucun des cas vu précedemment , (relativement imposible)
            print('Quelque chose qui ne vas pas !!!!')
            print(tag_path)
            print(annot_path)
            print(Aword,Tword ,TagCompteur,AnnotCompteur)    
            break

    
    #à la sortie du while , les deux fichiers doivent avoir le même nombre de lignes !!
    assert(len(Alines)==len(Tlines))
    #On écrit dans les deux fichiers 
    with open (tag_path , 'w') as file:
        for token in Tlines:
            file.write(token)
    with open (annot_path , 'w') as otherfile:
        for token in Alines:
            otherfile.write(token)
    #print('Done' , tag_path)
    
tags_dir = './eng/data/wikiner/tags/'
benchmarks = ["stanford", "spacy", "presidio" , "edsnlp"]
#benchmarks = ["spacy" , "edsnlp"]

for b in benchmarks:
    bench_dir = './eng/tags/wikiner/'+b+'/'
    if(os.path.exists(bench_dir)):
        print("Fixing files in", bench_dir)
        bench_filenames = os.listdir(bench_dir)
        for bench_file in tqdm(bench_filenames):
            tags_file = bench_file.replace(b, 'tags')
                
            fix_file(os.path.join(tags_dir, tags_file), os.path.join(bench_dir,bench_file))

    
            