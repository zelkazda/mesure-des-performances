import os 
from pathlib import Path
from tqdm import tqdm
"""
Ce code sert à extraire les texte et annotations de chaque articles à partir des donnée brut dans le fichier aij-wikiner-en-wp2
"""



def replace ( entity ):
    # sur aij-wikiner-en-wp2 , les entittés sont de la forme I-ENTITY ou bien B-ENTITY ce qui n'est pas la forme de sortie des benchmarks
    if entity == 'I-MISC' or  entity == 'B-MISC' :
        return '\tMISC'
    elif  entity == 'I-LOC' or  entity == 'B-LOC' :
        return '\tLOCATION'
    elif  entity == 'I-PER' or  entity == 'B-PER' :
        return '\tPERSON'
    elif entity == 'I-ORG' or  entity == 'B-ORG' :
        return '\tORGANIZATION'
    else :
        return '\tO'


def convert_file (input_file):
    """
    Params : chemin du fichier qui contienet la data brute , dans notre cas ce sera le chemin de aij-wikiner-en-wp2
    Return : tableaux des articles et annotations de chaque article 
    """
    with open(input_file , 'r') as file:
        lines = file.readlines()

    articles = []
    annotations = []
    current_article = []
    current_annotations = []

    for line in lines:
        line = line.strip() # On se débarasse des espaces , tabulations , retour à la ligne du debut et la fin de la ligne
        if line :
            #rentre si la ligne est non vide  --> ajouté à l'article
            phrase = ''
            tokens = line.split() 
            for token in tokens :
                # chaque token est de la forme Haymarket|NNP|I-PER
                mot = token.split('|')[0] 
                entite = replace(token.split('|')[2])
               
                phrase += (mot + ' ')
                current_annotations.append(mot+entite)
            current_article.append(phrase)

        else:
            #ligne vide marquant la fin de l'article
            if current_article: 
                #vérifie article non vide et l'ajoute
                articles.append(current_article)
                annotations.append(current_annotations)
                current_article = []
                current_annotations= []

    if current_article:  # dernier article ne contiendera pas ligne vide à la fin
        articles.append(current_article)
        annotations.append(current_annotations)

    return articles , annotations


def preprocess_wikiner(source, articles_dir, annotations_dir):
    """
    appelle convert_file et ecrit les articles et leurs annotations dans des fichiers qui porte le même indice dans leur nom
    """

    Path(articles_dir).mkdir(parents=True, exist_ok=True)
    Path(annotations_dir).mkdir(parents=True, exist_ok=True)
    articles, annotations = convert_file(source)
    
    print("Preprocessing wikiner...")

    for i in tqdm(range(len(articles))):

        article_filename = os.path.join(articles_dir, f'article_{i+1}.txt')
        annotation_filename = os.path.join(annotations_dir, f'tags_{i+1}.txt')

        with open(article_filename, 'w') as article_file:
            for sentence in articles[i]:
                article_file.write(sentence.strip() + '\n')

        with open(annotation_filename, 'w') as annotation_file:
            for annot in annotations[i]:
                annotation_file.write(annot.strip() + '\n')
                

preprocess_wikiner('./fr/data/wikiner/aij-wikiner-fr-wp2' , './fr/data/wikiner/articles', './fr/data/wikiner/tags' )

#preprocess_wikiner('./eng/data/wikiner/aijwikinerenwp2' ,'./eng/data/wikiner/articles','./eng/data/wikiner/tags' )

