import os
from tqdm import tqdm
from sklearn.metrics import precision_score, recall_score, f1_score

def load_annotations(file_path):
    """
    lit les annotations d'un fichier qui correspond à un article 

    Params: file_path(str) : chemin du fichier à lire
    Return :tableau , chaque element est un tableau [mot , entité] 
    
    """
    with open(file_path, 'r') as file:
        annotations = file.readlines()
    return [line.strip().split('\t') for line in annotations]

def evaluate_entities(tagged_dir, annotations_dir , benchmark):
    entity_types = ['PERSON', 'LOCATION', 'ORGANIZATION', 'MISC' , 'O']
    
    # dictionnaire où les clés sont les entités et valeurs sont des tableaux qui contienderont la precision,recall,f1-score dans chaque fichier 
    precision_scores = {entity: [] for entity in entity_types}
    recall_scores = {entity: [] for entity in entity_types}
    f1_scores = {entity: [] for entity in entity_types}

    # Listes globales pour toutes les entités
    global_y_true = []
    global_y_pred = []

    counter = 0

    print("Evaluating", benchmark, "...")

    for filename in tqdm(os.listdir(tagged_dir)):
        #parcours les fichiers du répértoire
        if filename.endswith(".txt"):
            tagged_file = os.path.join(tagged_dir, filename) # construit chemin du fichier résultat
            #annotation_file = os.path.join(annotations_dir, filename.replace("p","annotationP")) 
            #annotation_file = os.path.join(annotations_dir, filename.replace("spacyTag","annotation")) 
            #annotation_file = os.path.join(annotations_dir, filename.replace("stanford","annotationStanford")) 
            #annotation_file = os.path.join(annotations_dir, filename.replace("edsnlp","annotation")) 
            annotation_file = os.path.join(annotations_dir, filename.replace(benchmark,"tags")) 


            if os.path.exists(annotation_file):
                tagged_data = load_annotations(tagged_file)
                annotation_data = load_annotations(annotation_file)
    
                if len(tagged_data) != len(annotation_data):
                    #on ignore le fichier (l'article) s'il y a au moins un token de +/- entre annotation et predit
                    # cela ne doit pas du tout être affiché si on a utilisé fix_file.py avant
                    print(f"File length mismatch!!: {filename}")
                    print(len(tagged_data))
                    print(len(annotation_data))
                    counter+=1
                    continue
                
                y_true = []
                y_pred = []
                
                for tag, ann in zip(tagged_data, annotation_data):
                    # le zip est utilisé pour créer un combinaison des éléments entre tagged_data et annotated_data
                    if len(tag) < 2 or len(ann) < 2:
                        continue
                    #on ignore les tokens et on s'intéresse qu'aux entités
                    y_true.append(ann[1])
                    y_pred.append(tag[1])

                #ajout à la fin de la liste
                global_y_true.extend(y_true)
                global_y_pred.extend(y_pred)

                for entity in entity_types:
                    # On vérifie si entity est présente dans au moins l'un des deux fichier (annotation / prédiction) avant de calculer les métriques ,
                    # sinon elle rentre pas dans le calcul de la moyenne 
                    if entity in y_true or entity in y_pred:

                        y_true_entity = [1 if label == entity else 0 for label in y_true]
                        y_pred_entity = [1 if label == entity else 0 for label in y_pred]
                        
                        #zero_division = 0 pour que la metrique prend 0 si le denominateur est nul (aucune prediction positive)
                        precision = precision_score(y_true_entity, y_pred_entity, zero_division=0)
                        recall = recall_score(y_true_entity, y_pred_entity, zero_division=0)
                        f1 = f1_score(y_true_entity, y_pred_entity, zero_division=0)
                        
                        precision_scores[entity].append(precision)
                        recall_scores[entity].append(recall)
                        f1_scores[entity].append(f1)

    for entity in entity_types:
        avg_precision = sum(precision_scores[entity]) / len(precision_scores[entity]) if precision_scores[entity] else 0
        avg_recall = sum(recall_scores[entity]) / len(recall_scores[entity]) if recall_scores[entity] else 0
        avg_f1 = sum(f1_scores[entity]) / len(f1_scores[entity]) if f1_scores[entity] else 0

        print(f"Entity: {entity}")
        print(f"  Precision: {avg_precision:.4f}")
        print(f"  Recall: {avg_recall:.4f}")
        print(f"  F1 Score: {avg_f1:.4f}")
    
    # Calcul des métriques globales pour toutes les entités combinées
    print("Computing global metrics...")
    global_precision_macro = precision_score(global_y_true, global_y_pred, average='macro', zero_division=0)
    global_recall_macro = recall_score(global_y_true, global_y_pred, average='macro', zero_division=0)
    global_f1_macro = f1_score(global_y_true, global_y_pred, average='macro', zero_division=0)

    global_precision_micro = precision_score(global_y_true, global_y_pred, average='micro', zero_division=0)
    global_recall_micro = recall_score(global_y_true, global_y_pred, average='micro', zero_division=0)
    global_f1_micro = f1_score(global_y_true, global_y_pred, average='micro', zero_division=0)

    global_precision_weight = precision_score(global_y_true, global_y_pred, average='weighted', zero_division=0)
    global_recall_weight = recall_score(global_y_true, global_y_pred, average='weighted', zero_division=0)
    global_f1_weight = f1_score(global_y_true, global_y_pred, average='weighted', zero_division=0)
    
    print("Global Metrics Macro  for all entities combined:")
    print(f"  Precision: {global_precision_macro:.8f}")
    print(f"  Recall: {global_recall_macro:.8f}")
    print(f"  F1 Score: {global_f1_macro:.8f}")

    print("Global Metrics Micro  for all entities combined:")
    print(f"  Precision: {global_precision_micro:.8f}")
    print(f"  Recall: {global_recall_micro:.8f}")
    print(f"  F1 Score: {global_f1_micro:.8f}")

    print("Global Metrics weighted  for all entities combined:")
    print(f"  Precision: {global_precision_weight:.8f}")
    print(f"  Recall: {global_recall_weight:.8f}")
    print(f"  F1 Score: {global_f1_weight:.8f}")
    
    print("number of files mismatched is " , counter)
    

# Example usage
benchmarks = ["stanford", "spacy", "presidio", "edsnlp"]
#benchmarks = [ "spacy" , "edsnlp"]

tags_dir = "./eng/data/wikiner/tags"
for b in benchmarks:
    bench_dir = './eng/tags/wikiner/'+b+'/'
    if(os.path.exists(bench_dir)):
        evaluate_entities(bench_dir, tags_dir, b)

    
    