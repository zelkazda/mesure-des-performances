import os 
from pathlib import Path
from tqdm import tqdm
import jsonlines

"""
Ce code sert à extraire les texte et annotations de chaque articles à partir des donnée brut dans le fichier aij-wikiner-en-wp2
"""
def replace ( entity ):
    entities = ['\tO']
    return entities[int(entity)]


def preprocess_hcl(source, articles_dir, tags_dir):
    """
    appelle convert_file et ecrit les articles et leurs annotations dans des fichiers qui porte le même indice dans leur nom
    """

    Path(tags_dir).mkdir(parents=True, exist_ok=True)

    print("Preprocessing hcl data...")

    with jsonlines.open(source, "r") as f:
        for i, line in tqdm(enumerate(f.iter())):
            text = line["tokens"]
            labels = list(map(str, line["ner_tags"]))

            tags_file = os.path.join(tags_dir, f'tags_{i+1}.txt')
            article_file = os.path.join(tags_dir, f'article_{i+1}.txt')

            with open(tags_file, 'w') as t_file:
                for j in range(len(text)):
                    tag = replace(labels[j])
                    t_file.write(text[j] + ' ' + tag + '\n')

            with open(article_file, 'w') as a_file:
                a_file.write(" ".join(text))
            
                

preprocess_hcl('./eng/data/hcl/data.jsonl',
            './eng/data/wikiner/articles',
            './eng/data/hcl/tags')
