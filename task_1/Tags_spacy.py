import spacy
import os 
from tqdm import tqdm
from pathlib import Path

# on charge le modèle linguistique (ici, le modèle anglais de base)
#nlp = spacy.load("en_core_web_sm")
#nlp = spacy.load("en_core_web_md")
nlp = spacy.load("en_core_web_lg")
#nlp = spacy.load("fr_core_news_lg")

input_folder = './eng/data/wikiner/articles'
output_folder = './eng/tags/wikiner/spacy'

Path(output_folder).mkdir(parents=True, exist_ok=True)


def replace (entity):
    """
    Spacy reconnaît plusieurs entités autre que Person, location et organization.
    On l'ajuste aux annotations de WikiNER
    """
    if entity in {'ORDINAL','DATE','CARDINAL','QUANTITY','PERCENT','TIME','MONEY'}:
        return 'O'
    elif entity in {'EVENT' ,'WORK_OF_ART', 'NORP', 'PRODUCT','FAC'}:
        return 'MISC'
    elif entity in {'GPE','LOC'}:
        return 'LOCATION'
    elif (entity == 'ORG'):
        return 'ORGANIZATION'
    elif (entity =='PER'):
        return 'PERSON'
    else :
        return entity

print("Tagging with Spacy...")
for filename in tqdm(os.listdir(input_folder)):
    if filename.startswith('article') and filename.endswith('.txt'):
        
        output_filename = filename.replace('article', 'spacy')

        #lit l'article
        with open(os.path.join(input_folder, filename), 'r', encoding='utf-8') as file:
            article = file.read()
        
        doc = nlp(article)    
        results = []

        #parcours de tous les tokens, si l'attribut ent_type_(str) est non nul , on appelle replace , sinon c'est un 'O'
        for token in doc :
            if token.ent_type_:
                results.append((token.text,replace(token.ent_type_)))
            else:
                results.append((token.text , 'O'))

        # chemin du fichier de sortie
        output_file_path = os.path.join(output_folder, output_filename)

        with open(output_file_path, 'w', encoding='utf-8') as output_file:
            for token , tag in results:
                if token.strip(): #verification que le token n'est pas un espace ou une tabulation (can be removed i guess :/)
                    output_file.write(f'{token}\t{tag}\n')
        
        #print(f"NER results for {filename} saved to {output_file_path}")

