import os
from tqdm import tqdm
from pathlib import Path
from presidio_analyzer import AnalyzerEngine
from presidio_analyzer.nlp_engine import NlpEngineProvider
"""
Ce code sert à passer chaque article à presidio et stocker la sortie ( entité reconnue   Label) dans des fichiers sortie qui porte le même numéro que dans le fichier d'entrée

"""
# Initialise engine
analyzer = AnalyzerEngine() 

#text = "Microsoft Corporation is one of the leading tech companies in the world."
# ceci exemple simple pour s'assurer que presidio ne reconnait pas les organizations

#répértoire des articles (6223 articles)
input_folder = './eng/data/wikiner/articles' 
#répértoire du output de Presidio
output_folder = './eng/tags/wikiner/presidio'
# les autres entités que Presidio peut reconnaitre sont adresseIp , IBAN , ... mais ne sont pas présent dans les annotations de WikiNER
Path(output_folder).mkdir(parents=True, exist_ok=True)
entities_to_recognize = ["PERSON", "LOCATION", "ORGANIZATION"]

print("Tagging with presidio...")
for filename in tqdm(os.listdir(input_folder)):
    if filename.startswith('article') and filename.endswith('.txt'):
        # les fichiers d'entrée porte le nom article_{i}.txt avec numero attribué à l'article lors de sa création
        output_filename = filename.replace('article', 'presidio')

        with open(os.path.join(input_folder, filename), 'r', encoding='utf-8') as file:
            article = file.read()
        
        #on appelle l'analyzeur
        results = analyzer.analyze(text=article, entities=entities_to_recognize, language='en')
        #tokenization par les espaces
        tokens = article.split()

        token_index = 0
        output_tokens = []

        for token in tokens:
            entity_tag = 'O' # par défaut l'entité est 'O'
            #pour chaque token , on parcours les entités reconnues :
            #si l'index du token est compris entre le debut et la fin d'une entité reconnue , on lui attribue son entity_type
            #On sort de la boucle si l'index de la fin du token est plus grand que celui de l'entité courante 
            # (pas besoin de parcourir toute les entités pour chaque token)
            for result in results:
                if result.start <= token_index < result.end:
                    entity_tag = result.entity_type
                    break
                elif token_index+len(token) < result.start:
                    #print(token , 'i stopped at ', article[result.start:result.end])
                    break
            output_tokens.append((token, entity_tag))
            token_index += len(token) + 1  # +1 pour l'espace après le token dans le texte

        #on construit le chemin de sortie
        output_file_path = os.path.join(output_folder, output_filename)
        
        with open(output_file_path, 'w', encoding='utf-8') as output_file:
            for token, tag in output_tokens:
                if token.strip():
                    output_file.write(f'{token}\t{tag}\n')
        
        #print(f"NER results for {filename} saved to {output_file_path}")


 

