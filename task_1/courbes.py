import matplotlib.pyplot as plt
import os

dir_path = './eng/RESULTS/'
model_names = ['stanford', 'spacy_SM', 'spacy_MD', 'spacy_LG', 'presidio', 'edsnlp']
entities = ['PERSON', 'LOCATION', 'ORGANIZATION', 'O', 'global_MACRO', 'global_MICRO', 'global_WEIGHTED']

def extract_metrics():
    metrics = {
        'PERSON': {'precision': [], 'recall': [], 'f1': []},
        'LOCATION': {'precision': [], 'recall': [], 'f1': []},
        'ORGANIZATION': {'precision': [], 'recall': [], 'f1': []},
        'O': {'precision': [], 'recall': [], 'f1': []},
        'global_MACRO': {'precision': [], 'recall': [], 'f1': []},
        'global_MICRO': {'precision': [], 'recall': [], 'f1': []},
        'global_WEIGHTED': {'precision': [], 'recall': [], 'f1': []}
    }
    
    indices = {
        'PERSON': (1, 2, 3),
        'LOCATION': (5, 6, 7),
        'ORGANIZATION': (9, 10, 11),
        'O': (17, 18, 19),
        'global_MACRO': (21, 22, 23),
        'global_MICRO': (25, 26, 27),
        'global_WEIGHTED': (29, 30, 31)
    }
    
    for x in model_names:
        file_path = os.path.join(dir_path,x+'.txt')
        with open(file_path, 'r') as file:
            lines = file.readlines()
        for entity, (p_idx, r_idx, f1_idx) in indices.items():
            metrics[entity]['precision'].append(float(lines[p_idx].strip().split(':')[1]))
            metrics[entity]['recall'].append(float(lines[r_idx].strip().split(':')[1]))
            metrics[entity]['f1'].append(float(lines[f1_idx].strip().split(':')[1]))
    return metrics

def plot_histograms(metrics):
    x = range(len(model_names))
    bar_width = 0.1 #largeur des barres de l'histogramme
    opacity = 0.8 #transparence
    font_size = 20 #taille des textes dans l'image
    
    for metric in ['precision', 'recall', 'f1']:
        fig, ax = plt.subplots(figsize=(20, 10))  # Crée la figure et les axes avec ue taille spécifié
        
        for i, entity in enumerate(entities):
            metric_values = metrics[entity][metric] #tableau qui contient les valeurs de la metrique pour l'entité pour tous les modèles
            ax.bar([p + bar_width * i for p in x], metric_values, bar_width, alpha=opacity, label=entity)
            #le premier argument de ax.bar est les coordonnées des bars , et le deuxiéme les valeurs

            # Ajout des annotations au-dessus des barres
            for j, value in enumerate(metric_values):
                ax.text(j + bar_width * i, value , f'{value:.3f}', ha='center', va='bottom', fontsize=font_size*0.7) 
                #les deux premiers arguments sont les coordonnées, du texte à ajouter
                

        ax.set_xlabel('Modèles', fontsize=font_size)
        ax.set_ylabel(f'{metric.capitalize()} Scores', fontsize=font_size)
        ax.set_title(f'{metric.capitalize()} Scores par Entité et Modèle', fontsize=font_size)
        #l'argument c'est Array of tick locations ( division par 2 pour le placer dans le centre)
        ax.set_xticks([p + bar_width * (len(entities) / 2) for p in x])
        #ajout des noms des modèles
        ax.set_xticklabels(model_names , fontsize = font_size)
        ax.set_ylim(0, 1)
        ax.legend(fontsize=font_size)
        
        ax.grid(True)
        plt.tight_layout()  #évitant les chevauchements et rendant le graphique plus propre et lisible en ajustant l'espacement entre les éléments.
        mng = plt.get_current_fig_manager()  #Récupère le gestionnaire de figure actuel.
        mng.window.maximize() #Maximise la fenêtre de la figure.
        plt.show()
        

metrics = extract_metrics()
plot_histograms(metrics)
