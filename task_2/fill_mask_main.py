import os
from pathlib import Path
from tqdm import tqdm
from datasets import Dataset
from transformers import Trainer, TrainingArguments
from sklearn.model_selection import KFold
from transformers import CamembertTokenizer, CamembertForMaskedLM
from torch.nn.parallel import DataParallel
import torch 
from sklearn.model_selection import train_test_split
import fill_mask_train 
import fill_mask_test
import matplotlib.pyplot as plt
import numpy as np

if torch.cuda.is_available():
    device = torch.device('cuda')
    print("GPU is available")
else:
    device = torch.device('cpu')
    print("GPU not available, using CPU instead !!!")
    
articles_dir = './fr/data/wikiner/articles'
tags_dir = './fr/data/wikiner/tags'
masked_dir = './fr/data/wikiner/masked'
train_dir = './fr/data/wikiner/masked/train'
eval_dir = './fr/data/wikiner/masked/eval'
test_dir = './fr/data/wikiner/masked/test'

Path(train_dir).mkdir(parents=True, exist_ok=True)
Path(eval_dir).mkdir(parents=True, exist_ok=True)
Path(test_dir).mkdir(parents=True, exist_ok=True)
articles_names = os.listdir(articles_dir)

tokenizer = CamembertTokenizer.from_pretrained('camembert-base')
mask_token_id = tokenizer.mask_token_id

def train_and_evaluate(train_articles, eval_articles, test_articles, train_size, entity, nb_entity=1, max_length=512, tokenise_chunk=True):
    fill_mask_train.clean_directory(train_dir)
    fill_mask_train.clean_directory(eval_dir)
    fill_mask_train.clean_directory(test_dir)
    
    train_articles_subset = train_articles[:int(len(train_articles) * train_size)]
    
    masked_words_dict = fill_mask_train.mask_dir(test_articles, test_dir, articles_dir, tags_dir, entity, nb_entity)
    _ = fill_mask_train.mask_dir(eval_articles, eval_dir, articles_dir, tags_dir, entity, nb_entity)
    _ = fill_mask_train.mask_dir(train_articles_subset, train_dir, articles_dir, tags_dir, entity, nb_entity)
    
    training_articles = fill_mask_train.segmentate_and_tokenize(train_dir, tokenizer, max_length) if tokenise_chunk else fill_mask_train.segmentate_and_tokenize_phrase(train_dir, tokenizer)
    training_articles = fill_mask_train.mask_labels(training_articles, mask_token_id)
    training_dataset = Dataset.from_dict({k: [dic[k].squeeze().tolist() for dic in training_articles] for k in training_articles[0].keys()})
    
    eval_articles = fill_mask_train.segmentate_and_tokenize(eval_dir, tokenizer, max_length) if tokenise_chunk else fill_mask_train.segmentate_and_tokenize_phrase(train_dir, tokenizer)
    eval_articles = fill_mask_train.mask_labels(eval_articles, mask_token_id)
    eval_dataset = Dataset.from_dict({k: [dic[k].squeeze().tolist() for dic in eval_articles] for k in eval_articles[0].keys()})

    model = CamembertForMaskedLM.from_pretrained('camembert-base')
    if torch.cuda.is_available():
        model = DataParallel(model)
        model.to('cuda')

    training_args = TrainingArguments(
        output_dir=f'./results_{train_size}',
        learning_rate=2e-5,
        per_device_train_batch_size=8,
        num_train_epochs=4,
        fp16=torch.cuda.is_available(),
        weight_decay=0.01,
        remove_unused_columns=False,
    )

    trainer = Trainer(
        model=model,
        args=training_args,
        train_dataset=training_dataset,
        eval_dataset=eval_dataset,
    )
    trainer.train()
    
    number_correct, total_masks, accuracy, perplexity, correct_predictions_per_article = fill_mask_test.evaluate(test_dir, masked_words_dict, model, tokenizer, max_length, tokenise_chunk)
    
    correct_counts = list(correct_predictions_per_article.values())
    unique_counts = sorted(set(correct_counts))
    count_frequencies = [correct_counts.count(count) for count in unique_counts]

    fig, ax = plt.subplots(figsize=(10, 6))
    ax.bar(unique_counts, count_frequencies)
    ax.set_xlabel('Nombre de mots correctement prédits')
    ax.set_ylabel("Nombre d'articles")
    ax.set_title(f'Distribution des prédictions correctes par article (train_size={train_size}, entity={entity})')

    for i, v in enumerate(count_frequencies):
        ax.text(unique_counts[i], v, str(v), ha='center', va='bottom')

    plt.tight_layout()
    plt.savefig(f'distribution_predictions_correctes_{train_size}_{entity}.png')
    plt.close()

    #plot_direct_reidentification(correct_predictions_per_article, train_size, entity)

    return accuracy, perplexity, correct_predictions_per_article


def cross_validate(entity, n_splits=5, eval_on_train=False, nb_entity=1, max_length=512, tokenise_chunk=True):
    all_accuracies = {size: [] for size in train_sizes}
    all_perplexities = {size: [] for size in train_sizes}
    
    for split in range(n_splits):
        print(f"Split {split+1}/{n_splits}")
        
        rest_articles, test_articles = train_test_split(articles_names, test_size=0.05, random_state=42+split)
        train_articles, eval_articles = train_test_split(rest_articles, test_size=1/9, random_state=42+split)
        if eval_on_train:
            _, test_articles = train_test_split(train_articles, test_size=0.1, random_state=42+split)
            
        for size in train_sizes:
            print(f"  Training with {size*100}% of training data")
            accuracy, perplexity, _ = train_and_evaluate(train_articles, eval_articles, test_articles, size, entity, nb_entity, max_length, tokenise_chunk)
            all_accuracies[size].append(accuracy)
            all_perplexities[size].append(perplexity)

            print(f"  Accuracy: {accuracy:.4f}")
            print(f"  Perplexity: {perplexity:.4f}")
            
    mean_accuracies = [np.mean(all_accuracies[size]) for size in train_sizes]
    std_accuracies = [np.std(all_accuracies[size]) for size in train_sizes]

    mean_perplexities = [np.mean(all_perplexities[size]) for size in train_sizes]
    std_perplexities = [np.std(all_perplexities[size]) for size in train_sizes]  
          
    return mean_accuracies, mean_perplexities, std_accuracies, std_perplexities

def test_context(entity, size, eval_on_train=False, nb_entity=1):
    all_accuracies = [] 
    all_perplexities = []
    
    rest_articles, test_articles = train_test_split(articles_names, test_size=0.05, random_state=42)
    train_articles, eval_articles = train_test_split(rest_articles, test_size=1/9, random_state=42)
    if eval_on_train:
        _, test_articles = train_test_split(train_articles, test_size=0.1, random_state=42)
                                              
    for length in length_sizes:
        print(f" training with a max_length={length}")
        accuracy, perplexity, _ = train_and_evaluate(train_articles, eval_articles, test_articles, size, entity, nb_entity, length, tokenise_chunk=True)
        all_accuracies.append(accuracy)
        all_perplexities.append(perplexity)
        
        print(f"  Accuracy: {accuracy:.4f}")
        print(f"  Perplexity: {perplexity:.4f}")
        
    return all_accuracies, all_perplexities



def test_entity_combinations(train_size=0.9, max_length=512, tokenise_chunk=True):
    entity_combinations = [
        ['PERSON'],
        ['PERSON', 'LOCATION'],
        ['PERSON', 'LOCATION', 'ORGANIZATION'],
        ['PERSON', 'LOCATION', 'ORGANIZATION', 'MISC']
    ]
    
    all_correct_predictions = []

    rest_articles, test_articles = train_test_split(articles_names, test_size=0.05, random_state=42)
    train_articles, eval_articles = train_test_split(rest_articles, test_size=1/9, random_state=42)

    for entities in entity_combinations:
        print(f"Testing with entities: {', '.join(entities)}")
        _, _, correct_predictions = train_and_evaluate(
            train_articles, eval_articles, test_articles, 
            train_size, entities, nb_entity=1, max_length=max_length, 
            tokenise_chunk=tokenise_chunk
        )
        all_correct_predictions.append(correct_predictions)

    # Créer un histogramme et une courbe de cumul relatif pour chaque combinaison d'entités
    for i, (entities, correct_predictions) in enumerate(zip(entity_combinations, all_correct_predictions)):
        # Créer l'histogramme de distribution des prédictions correctes
        correct_counts = list(correct_predictions.values())
        unique_counts = sorted(set(correct_counts))
        count_frequencies = [correct_counts.count(count) for count in unique_counts]

        # Histogramme
        fig, ax = plt.subplots(figsize=(10, 6))
        ax.bar(unique_counts, count_frequencies)
        ax.set_xlabel('Nombre de mots correctement prédits')
        ax.set_ylabel("Nombre d'articles")
        ax.set_title(f'Entités: {", ".join(entities)} - Distribution des prédictions correctes')

        for j, v in enumerate(count_frequencies):
            ax.text(unique_counts[j], v, str(v), ha='center', va='bottom')

        plt.tight_layout()
        plt.savefig(f'distribution_predictions_correctes_entites_{"_".join(entities)}.png')
        plt.close()

        # Créer les courbes de cumul relatif
        cumulative = np.cumsum(count_frequencies)
        cumulative_relative = cumulative / cumulative[-1]

        # Courbe de cumul relatif
        fig, ax = plt.subplots(figsize=(10, 6))
        ax.plot(unique_counts, cumulative_relative, marker='o')
        ax.set_xlabel('Nombre de mots correctement prédits')
        ax.set_ylabel('Cumul relatif')
        ax.set_title(f'Entités: {", ".join(entities)} - Cumul relatif des prédictions correctes')
        ax.set_ylim(0, 1)
        ax.grid(True)

        plt.tight_layout()
        plt.savefig(f'cumul_relatif_predictions_correctes_entites_{"_".join(entities)}.png')
        plt.close()

    print("Expérience sur les combinaisons d'entités terminée. Résultats sauvegardés dans des images séparées.")


# Appel de la fonction modifiée
test_entity_combinations()



train_sizes = [0.5, 0.75, 0.9]
length_sizes = [64, 128, 256, 512]
tokenizer = CamembertTokenizer.from_pretrained('camembert-base')
mask_token_id = tokenizer.mask_token_id
entity_ = 'PERSON'
"""
mean_accuracies, mean_perplexities, std_accuracies, std_perplexities = cross_validate(entity_, n_splits=1)

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 12))
ax1.errorbar(train_sizes, mean_accuracies, yerr=std_accuracies, fmt='-o', capsize=5)
ax1.set_xlabel('Proportion of training data')
ax1.set_ylabel('Accuracy')
ax1.set_title(f'Accuracy vs Train size (entity={entity_})')
ax1.grid(True)

ax2.errorbar(train_sizes, mean_perplexities, fmt='-o', capsize=5)
ax2.set_xlabel('Proportion of training data')
ax2.set_ylabel('Perplexity')
ax2.set_title(f'Perplexity vs Train size (entity={entity_})')
ax2.grid(True)

plt.tight_layout()
plt.savefig('accuracy_and_perplexity_vs_train_size.png')
plt.show()
"""
print("Experiment completed. Results saved in 'accuracy_and_perplexity_vs_train_size.png'")