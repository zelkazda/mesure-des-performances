import random
from collections import defaultdict
from tqdm import tqdm
import matplotlib.pyplot as plt

def clean_word(word):
    """ Fonction qui nettoie le mot après le split du texte """
    punctuation = {':', ',', ';', '-', '"', '.', '/' ,'<','>','*','_','?','!','%','=','^','~','$','&','+','@',"'"}
    brackets = {'(': ')', '[': ']', '{': '}'}
    word = word.strip()
    
    # Fonction pour vérifier si le mot est entouré de parenthèses, crochets ou accolades
    def is_surrounded(w, open_char, close_char):
        return w.startswith(open_char) and w.endswith(close_char) and w.count(open_char) == 1 and w.count(close_char) == 1
    
    changed = True
    while changed:
        changed = False
        
        # Enlever la ponctuation au début et à la fin
        if word and word[0] in punctuation:
            word = word[1:]
            changed = True
        if word and word[-1] in punctuation:
            word = word[:-1]
            changed = True
        
        # Traiter les parenthèses, crochets et accolades
        for open_char, close_char in brackets.items():
            if is_surrounded(word, open_char, close_char):
                word = word[1:-1]
                changed = True
            elif word.startswith(open_char) and close_char not in word:
                word = word[1:]
                changed = True
            elif word.endswith(close_char) and open_char not in word:
                word = word[:-1]
                changed = True
    
    return word.lower() 
    
def simulate_attacker_knowledge(exclusive_words_per_article, percentage=0.05):
    """
    params : exclusive_words_per_article = dictionnaire où les clés sont le noms des articles et les valeurs sont des sets de mots exclusifs
             percentage , par defaut 5% , c'est le pourcentage du total que l'attaquant saura
             
    return : attacker_knowledge = un dictionnaire de mots exclusifs par article que l'attaquant sait 
    """
    all_exclusive_words = [(word, article) for article, words in exclusive_words_per_article.items() for word in words]
    #l'attaquant sait bien à qui correspond chaque mot exclusifs 
    total_words = len(all_exclusive_words)
    num_words_to_know = max(1, int(total_words * percentage)) #nombre d'élément que l'attaquant sait
    random.seed(42)
    attacker_knowledge = defaultdict(set)
    for word, article in random.sample(all_exclusive_words, num_words_to_know):
        attacker_knowledge[article].add(word)
    
    return attacker_knowledge

def attempt_reidentification(attacker_knowledge, anonymized_articles):
    reidentified_articles = {}
    
    # Pré-traitement des articles anonymisés
    split_anonymized_articles = {
        anon_article: content.split()
        for anon_article, content in anonymized_articles.items()
    }
    
    for known_article, known_words in tqdm(attacker_knowledge.items()):
        #parcours de mots exclusifs par article (plutôt patient)
        potential_matches = []
        
        for anon_article, content_words in split_anonymized_articles.items():
            match_score = 0
            for known_word in known_words:
                match_score += sum(1 for uncleaned in content_words if known_word == clean_word(uncleaned))
            if match_score > 0:
                potential_matches.append((anon_article, match_score))
        
        potential_matches.sort(key=lambda x: x[1], reverse=True) # tri dans l'ordre décroissant
        
        if potential_matches:
            reidentified_articles[known_article] = [match[0] for match in potential_matches]
            print(known_article)
            print(reidentified_articles[known_article])
        else:
            reidentified_articles[known_article] = []

    return reidentified_articles

# Mesurer la précision
def measure_reidentification_accuracy(reidentified_articles):
    total_articles = len(reidentified_articles)
    correctly_identified = sum(1 for known, identified in reidentified_articles.items() 
                               if f"anon_{known}" in identified and identified.index(f"anon_{known}") == 0)
    print("correctly identified", correctly_identified)
    print("total is ", total_articles)
    accuracy = correctly_identified / total_articles
    return accuracy


def main():
    # Charger les mots exclusifs par article depuis le fichier
    exclusive_words_per_article = {}
    current_article = None
    with open('mots_exclusifs_par_article.txt', 'r', encoding='utf-8') as f:
        for line in f:
            if line.startswith("Article:"):
                current_article = line.split(":")[1].strip()
                exclusive_words_per_article[current_article] = set()
            elif line.strip():
                word = line.strip()
                exclusive_words_per_article[current_article].add(word)

    # Simuler les articles anonymisés
    anonymized_articles = {}
    for article in exclusive_words_per_article.keys():
        with open(f'./n2c2/{article}', 'r', encoding='utf-8') as file:
            anonymized_content = file.read().strip()
        anonymized_articles[f"anon_{article}"] = anonymized_content

    # Tester différents pourcentages
    percentages = [0.01, 0.02, 0.05, 0.1]
    accuracies = []

    for percentage in percentages:
        print(f"Testing with {percentage:.2%} of exclusive words...")
        attacker_knowledge = simulate_attacker_knowledge(exclusive_words_per_article, percentage)
        reidentified_articles = attempt_reidentification(attacker_knowledge, anonymized_articles)
        accuracy = measure_reidentification_accuracy(reidentified_articles)
        accuracies.append(accuracy)
        print(f"Accuracy: {accuracy:.2%}")

    # Tracer le graphique
    plt.figure(figsize=(10, 6))
    plt.plot(percentages, accuracies, marker='o')
    plt.xlabel("Percentage of Known Exclusive Words")
    plt.ylabel("Re-identification Accuracy")
    plt.title("Re-identification Accuracy vs. Attacker Knowledge")
    plt.grid(True)
    plt.savefig('reidentification_accuracy_plot(organe_cancer)_egalité_exact.png')
    plt.show()

if __name__ == "__main__":
    main()