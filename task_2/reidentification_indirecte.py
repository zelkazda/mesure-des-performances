import pandas as pd
import matplotlib.pyplot as plt
from collections import defaultdict
import os 


# Définir les listes de cancer, organes et produits chimiques
cancers = [
    'breast cancer', 'leukemia', 'lymphoma', 'lung cancer', 'anal cancer', 'bladder cancer',
    'kidney cancer', 'bone tumor', 'prostate cancer', 'colorectal cancer', 'endometrial cancer',
    'thyroid cancer', 'multiple myeloma', 'sarcoma', 'mesothelioma', 'brain cancer', 
    'esophageal cancer', 'gastric cancer', 'stomach cancer', 'liver cancer', 'pancreatic cancer',
    'ovarian cancer', 'cervical cancer', 'skin cancer', 'melanoma', 'testicular cancer', 
    'oral cancer', 'head and neck cancer', 'neuroblastoma', 'retinoblastoma', 'gallbladder cancer', 
    'uterine cancer', 'penile cancer', 'vaginal cancer', 'vulvar cancer', 'adrenal cancer',
    'small intestine cancer', 'soft tissue sarcoma', 'pleural mesothelioma', 'peritoneal mesothelioma',
    'cholangiocarcinoma', 'ampullary cancer', 'nasopharyngeal cancer', 'parathyroid cancer',
    'fallopian tube cancer', 'spinal cord tumor', 'thymoma', 'thymic carcinoma', 
    'merkel cell carcinoma', 'kaposi sarcoma', 'angiosarcoma', 'chondrosarcoma', 'ewing sarcoma',
    'fibrosarcoma', 'gastrointestinal stromal tumor', 'hemangiosarcoma', 'malignant fibrous histiocytoma',
    'mycosis fungoides', 'paget disease', 'rhabdomyosarcoma', 'waldenstrom macroglobulinemia'
]

organs = [
    'breast', 'lung', 'esophagus', 'heart', 'bladder', 'spleen', 'ovaries', 'thyroid', 'larynx', 
    'skin', 'pancreas', 'rectum', 'parotid gland', 'liver', 'stomach', 'kidney', 'prostate', 
    'brain', 'cervix', 'uterus', 'gallbladder', 'testes', 'vagina', 'vulva', 'colon', 
    'small intestine', 'large intestine', 'bile duct', 'thymus', 'adrenal gland', 'bone', 
    'cartilage', 'spinal cord', 'parathyroid', 'fallopian tube', 'salivary gland', 
    'tonsil', 'pleura', 'peritoneum', 'ampulla of vater', 'nasopharynx', 'sinus', 
    'oropharynx', 'laryngopharynx', 'hypopharynx', 'tongue', 'gums', 'palate', 'jaw',
    'mandible', 'maxilla', 'clavicle', 'scapula', 'pelvis', 'femur', 'tibia', 'fibula',
    'humerus', 'radius', 'ulna', 'sternum', 'ribs', 'vertebrae', 'muscle', 'tendons',
    'ligaments', 'eyeball', 'optic nerve', 'retina', 'cornea', 'lens', 'iris', 'ear', 
    'cochlea', 'semicircular canal', 'eustachian tube', 'nasal cavity', 'trachea', 
    'bronchi', 'alveoli', 'pericardium', 'aorta', 'arteries', 'veins', 'capillaries'
]
characteristic_counts = defaultdict(lambda: defaultdict(int))


# Parcourir les articles une seule fois
for article_index, article in enumerate(os.listdir('./n2c2')):
    with open (os.path.join('./n2c2', article) , 'r', encoding ='utf-8') as file :
        article_lower = file.read().strip().lower()
    
    for cancer in cancers:
        if cancer in article_lower:
            characteristic_counts['cancer'][cancer] += 1
            print ('cancer:', cancer)
    
    for organ in organs:
        if organ in article_lower:
            characteristic_counts['organ'][organ] += 1
            print( 'organ: ' , organ)

# Calcul de la proportion d'articles réidentifiables
def calculate_identifiable_proportion(characteristic_counts, total_articles):
    identifiable_count = 0
    for count in characteristic_counts.values():
        if count == 1:  # Un seul article mentionne cette caractéristique
            identifiable_count += 1
    print("identifiable count is ", identifiable_count)
    return identifiable_count / total_articles

total_articles = len(os.listdir('./n2c2'))
cancer_proportion = calculate_identifiable_proportion(characteristic_counts['cancer'], total_articles)
organ_proportion = calculate_identifiable_proportion(characteristic_counts['organ'], total_articles)

print("Proportion d'articles réidentifiables (cancers):", cancer_proportion)
print("Proportion d'articles réidentifiables (organes):", organ_proportion)

# Tracer la courbe
categories = ["Cancers", "Organes"]
proportions = [cancer_proportion, organ_proportion]

plt.figure(figsize=(10, 5))
plt.plot(categories, proportions, marker='o')
plt.title("Proportion d'articles réidentifiables par catégorie")
plt.xlabel("Catégorie")
plt.ylabel("Proportion réidentifiable")
plt.tight_layout()
plt.savefig(f'reidentification indirecte.png')
plt.show()