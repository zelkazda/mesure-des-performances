import torch
import os 
import torch.nn.functional as F
from tqdm import tqdm
from transformers import CamembertTokenizer, CamembertForMaskedLM
from torch.nn.parallel import DistributedDataParallel
from fill_mask_train import mask_dir   


def predict_masked_tokens(text_masked, model, tokenizer, top_k=3, max_length=512):
    """
    Params : 
    text(str) , le texte à compléter
    top_k (int) , le nombre de prédiction à considerer du vocabulaire
    
    return : liste des ensembles des mots prédits pour chaque mot masqué , dans l'ordre d'entrée
    
    """
    # Tokenize tout le texte
    tokens = tokenizer.tokenize(text_masked)
    all_predictions = []
    all_probabilities = []

    chunks = []
    current_chunk = []
    current_length = 0
    last_period_index = -1
    
    for token in tokens:
        current_chunk.append(token)
        current_length += 1
        
        # Vérifie si le token est exactement un point ou se termine par un point suivi d'un espace
        if token == '.':
            last_period_index = current_length-1
        
        if current_length >= max_length:
            if last_period_index != -1 and last_period_index > max_length * 0.75:
                chunk_to_add = current_chunk[:last_period_index+1]
                chunks.append(chunk_to_add)
                current_chunk = current_chunk[last_period_index+1:]
                current_length = len(current_chunk)
                last_period_index = -1
            else:
                chunks.append(current_chunk)
                current_chunk = []
                current_length = 0
                last_period_index = -1
        
    if current_chunk:
        chunks.append(current_chunk)
    
    # Traiter le texte par morceaux sans chevauchement !!!
    for chunk in chunks:
        chunk_text = tokenizer.convert_tokens_to_string(chunk)
        
        inputs = tokenizer(chunk_text, return_tensors="pt", padding=True, truncation=True, max_length=max_length)
        
        mask_token_index = torch.where(inputs["input_ids"] == tokenizer.mask_token_id)[1]
        " par exemple [7,9,14]"
        #torch.where rend un tensor de dim 2 ou les indices sont dans le second

        
        if len(mask_token_index) == 0:
            continue  # Aucun masque dans ce chunk, passer au suivant

        model.eval()
        with torch.no_grad():
            outputs = model(**inputs)  #MaskedLMOutput objet avec attributs logits , loss , ...

        # Les logits sont des prédictions brutes du modèle, non normalisées, pour chaque token de chaque position de la séquence.!!!
        #tensor 3d contenent batch_size , sequence_length , vocab_size
        logits = outputs.logits
         
        #texte est une seule str donc unique batch , d'où 0 . ON selectionne les logits pour les positions des tokens MASK dans la séquence.
        # ":" pour selectionner toutes les probabilités non normalisées pour chaque token dans le vocabulaire
        mask_token_logits = logits[0, mask_token_index, :]
        """
            mask_token_logits = tensor([
            [0.1, 0.2, 0.3, ..., 0.02, 0.15, 0.05],  # Logits pour le token masqué à la position 7
            [0.05, 0.1, 0.4, ..., 0.01, 0.2, 0.1],  # Logits pour le token masqué à la position 9
            [0.2, 0.05, 0.1, ..., 0.3, 0.05, 0.4]   # Logits pour le token masqué à la position 14
        ])
        """

        # Appliquer softmax pour obtenir les probabilités , dim = -1 la dernière dimension , donc sur chaque ligne 
        # tensor 2D contenant les probabilités pour chaque token du vocabulaire.
        probabilities = F.softmax(mask_token_logits, dim=-1)
        
        # Obtenir les top_k tokens les plus probables avec leurs probabilités (dim=1  signifie cela pour chaque ligne)
        top_k_tokens = torch.topk(probabilities, top_k, dim=1)
        "retour est un tensor 2d avec les values (les probas maximales ) classées et leur indices dans le vocabulaires"
        
        for j in range(len(mask_token_index)):
            top_tokens = top_k_tokens.indices[j]
            top_probs = top_k_tokens.values[j] #added
            top_words = tokenizer.convert_ids_to_tokens(top_tokens)
            top_words = [word.replace('▁', '') for word in top_words]
            all_predictions.append(top_words)
            all_probabilities.append(top_probs) 

    return all_predictions,all_probabilities

    
def predict_masked_tokens_phrase(text_masked, model, tokenizer, top_k=3, max_length=512):
    #memem principe qu'avant mais les prédiction se font phrase para phrase 
    #Hypothèse : les resultats seront moins bons vu que le modèle aura moins de context
    all_predictions = []
    all_probabilities = []
    model.eval()
    
    #text_masked est une liste de phrasee de l'article
    for sentence in text_masked:
        
        inputs = tokenizer(sentence.strip(), return_tensors="pt", padding=True, truncation=True, max_length=max_length)
        mask_token_index = torch.where(inputs["input_ids"] == tokenizer.mask_token_id)[1]
        
    
        if len(mask_token_index) == 0:
            continue  

        with torch.no_grad():
            outputs = model(**inputs)  

        logits = outputs.logits
         
    
        mask_token_logits = logits[0, mask_token_index, :]
        
        probabilities = F.softmax(mask_token_logits, dim=-1)
        
        top_k_tokens = torch.topk(probabilities, top_k, dim=1)
        
        for j in range(len(mask_token_index)):
            top_tokens = top_k_tokens.indices[j]
            top_probs = top_k_tokens.values[j] #added
            top_words = tokenizer.convert_ids_to_tokens(top_tokens)
            top_words = [word.replace('▁', '') for word in top_words]
            all_predictions.append(top_words)
            all_probabilities.append(top_probs) #added
            
    return all_predictions,all_probabilities
    

def evaluate(dir, masked_words_dict, model, tokenizer , max_length = 512 , tokenise_chunk = True):
    #fonction pour évaluer les prédictions du modèle
    #TODO ajouter le type d'evaluation (phrase ou chunks de 512) comme paramètre pour ne pas avoir à commenter des lignes à chaque fois 
    articles_names = os.listdir(dir)
    counter = 0
    total_masks = 0
    total_log_likelihood= 0
    correct_predictions_per_article = {}  # Nouveau dictionnaire
    
    for article_name in tqdm(articles_names):
        with open(os.path.join(dir, article_name), 'r', encoding='utf-8') as file:
            text_masked = file.read().strip() if tokenise_chunk  else file.readlines()

        predictions , probabilities = predict_masked_tokens(text_masked, model, tokenizer , max_length) if tokenise_chunk else predict_masked_tokens_phrase(text_masked, model, tokenizer, max_length)
        words = masked_words_dict[article_name]
        total_masks += len(words)
        correct_count = 0  # Compteur pour cet article

        for i in range(min(len(words), len(predictions))):
            if words[i] in predictions[i]:
                counter += 1
                correct_count += 1  # Compteur pour cet article
                word_index = predictions[i].index(words[i])
                prob = probabilities[i][word_index]
                total_log_likelihood += torch.log(prob).item()
            else : 
                 # Si le mot correct n'est pas dans le top_k, on utilise une probabilité très faible
                total_log_likelihood += torch.log(torch.tensor(1e-10)).item()
    
        correct_predictions_per_article[article_name] = correct_count
    
    accuracy = counter / total_masks if total_masks > 0 else 0
    perplexity = torch.exp(torch.tensor(-total_log_likelihood / total_masks)).item()
    return counter, total_masks, accuracy, perplexity , correct_predictions_per_article




