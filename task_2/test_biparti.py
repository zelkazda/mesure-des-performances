import os
from tqdm import tqdm
from collections import defaultdict
import matplotlib.pyplot as plt
import spacy
import os 
from tqdm import tqdm
from pathlib import Path
import re
import scispacy
import torch 
import numpy as np 


if torch.cuda.is_available():
    device = torch.device('cuda')
    print("GPU is available")
else:
    device = torch.device('cpu')
    print("GPU not available, using CPU instead !!!")
    
#model = "en_core_sci_md"
model = "en_ner_bionlp13cg_md"
nlp = spacy.load(model)

def clean_word(word):
    """ Fonction qui nettoie le mot après le split du texte """
    punctuation = {':', ',', ';', '-', '"', '.', '/' ,'<','>','*','_','?','!','%','=','^','~','$','&','+','@',"'"}
    brackets = {'(': ')', '[': ']', '{': '}'}
    word = word.strip()
    
    # Fonction pour vérifier si le mot est entouré de parenthèses, crochets ou accolades
    def is_surrounded(w, open_char, close_char):
        return w.startswith(open_char) and w.endswith(close_char) and w.count(open_char) == 1 and w.count(close_char) == 1
    
    changed = True
    while changed:
        changed = False
        
        # Enlever la ponctuation au début et à la fin
        if word and word[0] in punctuation:
            word = word[1:]
            changed = True
        if word and word[-1] in punctuation:
            word = word[:-1]
            changed = True
        
        # Traiter les parenthèses, crochets et accolades
        for open_char, close_char in brackets.items():
            if is_surrounded(word, open_char, close_char):
                word = word[1:-1]
                changed = True
            elif word.startswith(open_char) and close_char not in word:
                word = word[1:]
                changed = True
            elif word.endswith(close_char) and open_char not in word:
                word = word[:-1]
                changed = True
    
    return word.lower() 


def identify_entity(word):
    """ Cette fonction identifie si un mot appartient à une catégorie particulière en utilisant des expressions régulières """
    
    # Date patterns
    date_patterns = [
        r'\d{4}-\d{2}-\d{2}',  # Format YYYY-MM-DD
        r'\b(?:0?[1-9]|1[0-2])/(?:0?[1-9]|[12]\d|3[01])/(?:\d{2}|\d{4})\b',  # Format MM/DD/YY ou MM/DD/YYYY
        r'\b(?:dd|d|dv|dt|t):(?:0?[1-9]|1[0-2])[/-](?:0?[1-9]|[12]\d|3[01])[/-](?:\d{2}|\d{4})\b',  # Nouvelles formes avec préfixes
        r'\b(?:0?[1-9]|[12]\d|3[01])-[A-Za-z]{3}-\d{2,4}\b',  # Format DD-MMM-YY ou DD-MMM-YYYY
        r'\b[A-Za-z]{3}-\d{1,2}-\d{2,4}\b'  # Format MMM-DD-YY ou MMM-DD-YYYY
    ]
    
    if any(re.match(pattern, word) for pattern in date_patterns):
        return 'DATE'
    
    elif re.match(r'\b\d{7,8}(?:[A-Z]+)?\b', word):  # MRN pattern
        return 'MRN'
    
    elif re.match(r'\b(?:0?[0-9]|1[0-9]|2[0-3]):[0-5][0-9](?::[0-5][0-9])?(?:\s*[ap]m)?\b', word):  # Time pattern
        return 'TIME'
    
    elif re.match(r'\(?\d{3}\)?[-.\s]?\d{3}[-.\s]?\d{4}', word):  # Phone number pattern
        return 'PHONE_NUMBER'
    
    elif re.match(r'document:\d+-\d+', word):  # Document ID pattern
        return 'DOCUMENT_ID'
    
    elif re.match(r'#\d{5}', word):  # Pager number pattern
        return 'PAGER'
    
    elif re.match(r'edvisit\^\d{8}\^[A-Z]+',word):
        return 'EDVISIT'
    
    # Measure patterns
    measure_patterns = [
        r'\b\d+(?:\.\d+)?(?:\s*\(?[A-Za-z]+\)?)??\b',
        r'\b(?:\d+(?:\.\d+)?-\d+(?:\.\d+)?)(?:\s*\(?[A-Za-z]+\)?)?\b',
        r'\b\d+(?:\.\d+)?/\d+(?:\.\d+)?\b',  # Forme similaire à 168/78
        r'\b\d+(?:\.\d+)?/[a-zA-Z]{1,3}\b',  # 10/ml
        r'\b\d+/\d+\b'  # Pour capturer les motifs comme 102/70, 105/68, etc.
    ]
    
    if any(re.match(pattern, word) for pattern in measure_patterns):
        return 'MEASURE'
    
    return None

def align_spacy_with_split(doc, split_words):
    """ La méthode .split() des str utilise les espaces par défaut pour segmenter un texte, 
    tandis que spacy tokenise de manière différente. Cette fonction aligne les tokens spacy avec les mots splittés. """
    aligned_tokens = []
    current_token = ""
    spacy_index = 0
    first_entity = ""
    first_pos = ""

    for split_word in split_words:
        first_entity = ""
        first_pos = ""
        while spacy_index < len(doc) and len(current_token) < len(split_word):
            if doc[spacy_index].text.strip():
                if not first_entity:
                    first_entity = doc[spacy_index].ent_type_
                if not first_pos:
                    first_pos = doc[spacy_index].pos_
                current_token += doc[spacy_index].text
            spacy_index += 1

        if current_token == split_word:
            aligned_tokens.append((current_token, first_entity if first_entity else 'O', first_pos))
            current_token = ""

    return aligned_tokens

directory = "./n2c2"
articles = os.listdir(directory)

word_occurrences = defaultdict(set) # Dictionnaire qui pour chaque mot a comme valeur un ensemble des textes qui le contiennent 
entity_words = defaultdict(lambda: defaultdict(set)) # Dictionnaire qui pour chaque article contient un dictionnaire (les clés sont les entités et les valeurs sont les mots)
word_categories = defaultdict(lambda: defaultdict(int)) # Dictionnaire qui pour chaque article contient un dictionnaire (les clés sont les classes grammaticales et les valeurs sont l'effectif des mots exclusifs)

# Première passe : collecter tous les mots et entités nommées
for article in tqdm(articles, desc="Collecte des mots et entités"):
    with open(os.path.join(directory, article), 'r', encoding='utf-8') as file:
        text = file.read()
        words = text.split()
        
        # Analyse du texte complet avec spaCy
        doc = nlp(text)
        aligned_tokens = align_spacy_with_split(doc, words)
        
        for word, entity, pos in aligned_tokens:
            cleaned_word = clean_word(word)
            if cleaned_word:
                word_occurrences[cleaned_word].add(article)
                
                # Identifier l'entité basée sur des règles
                rule_based_entity = identify_entity(cleaned_word)
                if rule_based_entity:
                    entity_words[article][rule_based_entity].add(cleaned_word)
                elif entity != 'O':
                    entity_words[article][entity].add(cleaned_word)
                else:
                    # Utiliser la partie du discours seulement si aucune entité n'est disponible
                    if pos in ['NOUN', 'VERB', 'ADJ']:
                        entity_words[article][pos].add(cleaned_word)
                    else:
                        entity_words[article]['OTHER'].add(cleaned_word)

# Deuxième passe : classifier les mots exclusifs
exclusive_words = defaultdict(set)
for word, articles_containing in word_occurrences.items():
    if len(articles_containing) == 1:
        # Il s'agit d'un mot exclusif
        article = next(iter(articles_containing))
        for category, words in entity_words[article].items():
            if word in words:
                exclusive_words[category].add(word)
                break
        else:
            exclusive_words['OTHER'].add(word)
            
# Affichage des résultats et création du graphique
total_words = sum(len(words) for words in exclusive_words.values())
print(f"Total des mots exclusifs : {total_words}")

# Préparer les données pour le graphique
categories = list(exclusive_words.keys())
counts = [len(words) for words in exclusive_words.values()]

# Trier les catégories par nombre total décroissant
sorted_data = sorted(zip(categories, counts), key=lambda x: x[1], reverse=True)
categories, counts = zip(*sorted_data)

# Créer le graphique
plt.figure(figsize=(12, 6))
bars = plt.bar(categories, counts)
plt.title("Effectif des mots exclusifs par catégorie")
plt.xlabel("Catégories")
plt.ylabel("Nombre de mots")
plt.xticks(rotation=45, ha='right')

# Ajouter le nombre de mots sur chaque barre
for bar in bars:
    height = bar.get_height()
    plt.text(bar.get_x() + bar.get_width()/2., height,
             f'{height}',
             ha='center', va='bottom')

plt.tight_layout()
plt.savefig(f'effectif_mots_exclusifs_{model}.png')
plt.show()


# Définir les catégories que vous voulez inclure dans le graphique
categories_to_include = ['ORGAN', 'CANCER']  # Ajustez selon vos besoins

# Calculer le nombre de mots exclusifs par article, en ne considérant que les catégories spécifiées
exclusive_words_per_article = {}
for article in articles:
    exclusive_word_count=0
    for category in categories_to_include :
        if category in entity_words[article] :
            for word in entity_words[article][category]:
                if len(word_occurrences[word]) == 1:
                    exclusive_word_count+=1
        
    exclusive_words_per_article[article] = exclusive_word_count



# Compter combien d'articles ont chaque nombre de mots exclusifs
article_counts = defaultdict(int)
for count in exclusive_words_per_article.values():
    article_counts[count] += 1

# Préparer les données pour le graphique
x_values = sorted(article_counts.keys())
y_values = [article_counts[x] for x in x_values]

# Créer le nouveau graphique
plt.figure(figsize=(12, 6))
plt.bar(x_values, y_values)
plt.title(f"Distribution du nombre de mots exclusifs par article\n(Catégories: {', '.join(categories_to_include)})")
plt.xlabel("Nombre de mots exclusifs")
plt.ylabel("Nombre d'articles")

# Ajouter les valeurs sur chaque barre
for i, v in enumerate(y_values):
    plt.text(x_values[i], v, str(v), ha='center', va='bottom')

plt.tight_layout()
plt.savefig(f'distribution_mots_exclusifs_categories_specifiques_{model}.png')
plt.show()


# Calculer le nombre total d'articles
total_articles = len(articles)
print (f'total des articles est {total_articles}')
# Trier les données par nombre de mots exclusifs
sorted_data = sorted(article_counts.items())

# Calculer la distribution cumulative relative
cumulative_counts = np.cumsum([count for _, count in sorted_data])
print("les anciennes valeurs de l'histogramme")
print(sorted_data)
print('cumulative')
print(cumulative_counts)
cumulative_relative = cumulative_counts / total_articles

# Préparer les données pour le graphique
x_values = [x for x, _ in sorted_data]
y_values = cumulative_relative

# Créer le nouveau graphique
plt.figure(figsize=(12, 6))
plt.plot(x_values, y_values, marker='o')
plt.title(f"Distribution cumulative relative des articles par nombre de mots exclusifs\n(Catégories: {', '.join(categories_to_include)})")
plt.xlabel("Nombre de mots exclusifs")
plt.ylabel("Proportion cumulative d'articles")

# Ajouter les valeurs sur chaque point
for i, v in enumerate(y_values):
    plt.text(x_values[i], v, f'{v:.3f}', ha='center', va='bottom')

# Ajuster les limites de l'axe y pour qu'il aille de 0 à 1
plt.ylim(min(y_values)-0.1, max(y_values)+0.1)

plt.tight_layout()
plt.savefig(f'distribution_cumulative_relative_mots_exclusifs_{model}.png')
plt.show()

# Écrire les mots exclusifs par classe dans un fichier texte
with open('mots_exclusifs_par_classe.txt', 'w', encoding='utf-8') as f:
    for category, words in sorted(exclusive_words.items(), key=lambda x: len(x[1]), reverse=True):
        f.write(f"{category} ({len(words)}):\n")
        for word in sorted(words):
            f.write(f"  {word}\n")
        f.write("\n")
        
# Écrire les mots exclusifs par catégorie pour chaque article
with open('mots_exclusifs_par_article.txt', 'w', encoding='utf-8') as f:
    for article in articles:
        f.write(f"Article: {article}\n")
        for category in categories_to_include:
            if category in entity_words[article]:
                exclusive_words_in_category = [word for word in entity_words[article][category] if len(word_occurrences[word]) == 1]
                if exclusive_words_in_category:
                    #f.write(f"  {category} ({len(exclusive_words_in_category)}):\n")
                    for word in exclusive_words_in_category:
                        f.write(f"    {word}\n")
        f.write("\n")

print("Les mots exclusifs par catégorie pour chaque article ont été écrits dans 'mots_exclusifs_par_article.txt'")

print("Les mots exclusifs par classe ont été écrits dans 'mots_exclusifs_par_classe.txt'")
