import os
import random
from tqdm import tqdm
import random
import torch 
import shutil

#Cette fonction est utilisé pour le mask des articles , au niveau de chaque phrase une seule séquence d'entité est masqué, si plusieurs existent , cette séquence est choisie de manière alèatoire
# pour le moment on masque une unique sequence , mais on peut passer le nombre de sequence à masqué en paramètre de la fonction

def mask_article(article_path, tag_path,entities , nb_entity=1):
    
    masked_text = [] # liste des phrases masqué du texte

    # Lire le fichier des tags
    with open(tag_path, 'r', encoding='utf-8') as tag_file:
        tag_lines = tag_file.readlines()  # chaque ligne représente un mot de l'article avec son tag

    # Lire le fichier de l'article
    with open(article_path, 'r', encoding='utf-8') as article_file:
        article_lines = article_file.readlines()  # les vraies lignes de l'article

    tag_index = 0
    mask_words =[]
    for line in article_lines:
        if not line.strip():  # Vérifier les lignes vides pour les retours à la ligne
            masked_text.append('\n')
            continue

        words = line.strip().split()  # tableau des mots de la ligne courante
        masked_line = []
        person_sequences = []
        current_sequence = []
        

        # Identifier toutes les séquences de mots "PERSON"
        for word in words:
            if tag_index < len(tag_lines):
                tag_line = tag_lines[tag_index].strip()
                if not tag_line:
                    tag_index += 1
                    continue

                word_tag, tag = tag_line.split('\t')
                if tag in entities:
                    current_sequence.append(tag_index) # on s'interesse aux indices au liex des mots meme
                    masked_line.append(word_tag)
                else:
                    if current_sequence: #si l'entité n'est pas person et que current sequence est non vide  c'est que la sequence de person est finie
                        person_sequences.append(current_sequence)                        
                        current_sequence = []
                    masked_line.append(word_tag)
                tag_index += 1
            else:
                masked_line.append(word)  # on a atteint le dernier mot du texte
        
        # Ajouter la dernière séquence si elle existe
        if current_sequence:
            person_sequences.append(current_sequence)
        
        # Si des séquences "PERSON" sont trouvées, choisir une au hasard pour remplacer par "[MASK]"
        if person_sequences:
            num_sequences = min(nb_entity, len(person_sequences))
            sequences_to_mask = random.sample(person_sequences, num_sequences)
            for sequence in sequences_to_mask :
                mask_words.extend(tag_lines[word].split('\t')[0].strip() for word in sequence)
                for idx in sequence:
                    masked_line[idx - tag_index + len(words)] =  '<mask>'  # Ajuster l'index pour la ligne masquée
                    # en effet tag_index est l'index du dernier mot de la phrase courante +1 , dasn le texte en entier
                    # idx est l'index du mot à masquer dans le texte en entier.
                    # tag_index - idx = len(words) - indice_du_mot_dans_la_phrase_courante

        masked_text.append(' '.join(masked_line)) # ajout de la ligne au texte

    return '\n'.join(masked_text), mask_words   



def mask_dir (names , directory ,articles_dir,tags_dir, entities , nb_entity=1):
    print (f'masking for the {directory} files ...') 
    masked_words_dict = {}  # Dictionnaire pour stocker les mots masqués de chaque article
    
    for article_name in tqdm(names):
        article_path = os.path.join(articles_dir, article_name)
        tag_path = os.path.join(tags_dir,article_name).replace('article','tags')
        masked, masked_words =  mask_article(article_path, tag_path , entities, nb_entity)
        
        masked_words_dict[article_name] = masked_words

        output_path = os.path.join(directory, article_name)
        with open(output_path, 'w', encoding='utf-8') as file:
            file.write(masked)
            
    return masked_words_dict



"""
la ségmentation est intéressante et obligatoire dans notre cas , en effet si on se contente de mettre les articles dans un tableau seulement,
après la tokenization , le modèle rencontrera un problème  vu que les sequence n'ont pas tous la memem longueur
et meme si on ajoute la troncature seulement , on perdera trop d'information vu que les articles sont long et le modèle n'accepte 
des sequences de longueur supérieur à 512
"""

def segmentate_and_tokenize(dir, tokenizer, max_length=512):
    segmentated_articles = []
    print(f'Segmentation et tokenization des articles de {dir} en chunks de {max_length}')
    for article_name in tqdm(os.listdir(dir)):
        article_path = os.path.join(dir, article_name)
        with open(article_path, 'r', encoding='utf-8') as file:
            article_content = file.read().strip() 
        
        tokens = tokenizer.tokenize(article_content)
        
        chunks = []
        current_chunk = []
        current_length = 0
        last_period_index = -1
        
        for token in tokens:
            current_chunk.append(token)
            current_length += 1
            
            # Vérifie si le token est exactement un point ou se termine par un point suivi d'un espace
            if token == '.':
                last_period_index = current_length-1
            
            if current_length >= max_length:
                if last_period_index != -1 and last_period_index > max_length * 0.75:
                    chunk_to_add = current_chunk[:last_period_index+1]
                    chunks.append(chunk_to_add)
                    current_chunk = current_chunk[last_period_index+1:]
                    current_length = len(current_chunk)
                    last_period_index = -1
                else:
                    chunks.append(current_chunk)
                    current_chunk = []
                    current_length = 0
                    last_period_index = -1
        
        if current_chunk:
            chunks.append(current_chunk)
        
        for chunk in chunks:
        
            chunk_text = tokenizer.convert_tokens_to_string(chunk)
            tokenized_chunk = tokenizer(chunk_text, padding='max_length', truncation=True, max_length=max_length, return_tensors='pt')
            tokenized_chunk["labels"] = tokenized_chunk["input_ids"].clone()
            segmentated_articles.append(tokenized_chunk)
    
    return segmentated_articles

def segmentate_and_tokenize_phrase(dir, tokenizer,max_length=512):
    #meme principe , segmentation et tokenization mais en utilisant des phrases pour l'entrainement au lieu de chunks de 512
    #l'entrainement prendrera clairement beaucoup plus de temps vu que la taille des données sera extrême
    segmentated_phrases = []
    print(f'Segmentation et tokenization des articles de {dir} en phrases')
    for article_name in tqdm(os.listdir(dir)):
        article_path = os.path.join(dir, article_name)
        with open(article_path, 'r', encoding='utf-8') as file:
            lines = file.readlines()
        
        for line in lines:
            tokenized_chunk = tokenizer(line.strip(), padding='max_length', truncation=True, max_length=max_length, return_tensors='pt')
            tokenized_chunk["labels"] = tokenized_chunk["input_ids"].clone()
            
            segmentated_phrases.append(tokenized_chunk)
    
    return segmentated_phrases 


def mask_labels(tokenized_segments, mask_token_id):
    #tokenized_segment est une liste de dictionnaires , chaque dictionnaire est la sortie d'un tokenizer avec une clé de plus 'labels' qu'on a ajouté
    for segment in tokenized_segments:
        segment['labels'] = torch.where(segment['input_ids'] == mask_token_id, segment['input_ids'], torch.tensor(-100))
    return tokenized_segments

def clean_directory(directory):
    #supprime tout le contenu d'un dossier pour l'entrainement suivant 
    print(f'cleaning directory {directory}')
    for filename in tqdm(os.listdir(directory)):
        file_path = os.path.join(directory, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print(f'Failed to delete {file_path}. Reason: {e}')
            #exception  au cas ou un fichier ou sous dossier n'a pas pu être supprimé


