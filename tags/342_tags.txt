Record	O
date:	O
2093-01-13	O
EDVISIT^32612454^Barr,	O
Nicky^01/13/93^IP,	O
CARRIE	O
I	O
saw	O
this	O
patient	O
in	O
conjunction	O
with	O
Dr.	O
Xue.	O
I	O
confirm	O
that	O
I	O
have	O
interviewed	O
and	O
examined	O
the	O
patient,	O
reviewed	O
the	O
resident's	O
documentation	O
on	O
the	O
patient's	O
chart,	O
and	O
discussed	O
the	O
evaluation,	O
plan	O
of	O
care,	O
and	O
disposition	O
with	O
the	O
patient.	O
CHIEF	O
COMPLAINT:	O
A	O
57-year-old	O
with	O
back	O
pain.	O
HISTORY	O
OF	O
PRESENT	O
ILLNESS:	O
The	O
patient	O
is	O
a	O
young	O
woman	O
with	O
a	O
complicated	O
past	O
medical	O
history,	O
presents	O
with	O
a	O
chronic	O
history	O
of	O
low	O
back	O
pain,	O
worse	O
over	O
the	O
last	O
week.	O
She	O
states	O
that	O
3	O
years	O
ago,	O
she	O
had	O
a	O
motor	O
vehicle	O
accident.	O
Since	O
that	O
time,	O
has	O
had	O
chronic	O
low	O
back	O
pain.	O
She	O
has	O
been	O
able	O
to	O
ambulate	O
at	O
home.	O
Denies	O
any	O
fevers,	O
rashes,	O
history	O
of	O
urinary	O
incontinence,	O
weakness,	O
or	O
numbness.	O
She	O
went	O
to	O
EDUCARE-FARGO	O
earlier	O
this	O
week	O
and	O
was	O
prescribed	O
Percocet,	O
comes	O
here	O
now	O
because	O
the	O
patient's	O
family	O
and	O
the	O
patient	O
would	O
like	O
to	O
change	O
her	O
care	O
to	O
Lansing	LOCATION
Dover	O
Clinic.	O
Denies	O
any	O
numbness	O
or	O
weakness.	O
States	O
that	O
she	O
is	O
no	O
longer	O
taking	O
her	O
pills	O
at	O
home.	O
Denies	O
any	O
suicidal	O
or	O
homicidal	O
ideation,	O
has	O
a	O
flat	O
affect.	O
Denies	O
any	O
depression.	O
Her	O
son,	O
however,	O
with	O
whom	O
she	O
lives	O
disagrees	O
and	O
is	O
very	O
concerned	O
that	O
she	O
may	O
be	O
depressed.	O
He	O
is	O
also	O
very	O
concerned	O
that	O
she	O
is	O
not	O
taking	O
her	O
medicines	O
and	O
that	O
she	O
is	O
not	O
caring	O
for	O
herself	O
well	O
at	O
home.	O
PAST	O
MEDICAL	O
HISTORY:	O
Remarkable	O
for	O
hypertension,	O
diabetes,	O
and	O
a	O
prior	O
stroke.	O
PAST	O
SURGICAL	O
HISTORY:	O
She	O
has	O
also	O
had	O
knee	O
surgery.	O
SOCIAL	O
HISTORY,	O
FAMILY	O
HISTORY,	O
AND	O
REVIEW	O
OF	O
SYSTEMS:	O
As	O
per	O
the	O
written	O
note.	O
The	O
patient	O
was	O
counseled	O
regarding	O
smoking	O
cessation.	O
MEDICATIONS:	O
Hydrochlorothiazide	O
and	O
Coumadin.	O
ALLERGIES:	O
None.	O
PHYSICAL	O
EXAMINATION:	O
The	O
patient	O
is	O
awake	O
and	O
alert	O
in	O
no	O
acute	O
distress.	O
Vital	O
signs	O
are	O
within	O
normal	O
limits	O
as	O
documented.	O
For	O
full	O
physical	O
exam	O
findings,	O
please	O
see	O
the	O
resident's	O
written	O
note.	O
Specifically,	O
HEENT,	O
neck,	O
respiratory,	O
cardiac,	O
rectal,	O
and	O
abdominal	O
exam	O
are	O
within	O
normal	O
limits	O
as	O
documented.	O
Skin	O
exam	O
is	O
remarkable	O
for	O
erythematous	O
changes	O
in	O
the	O
groin	O
where	O
the	O
patient	O
has	O
significant	O
skin	O
fold	O
due	O
to	O
obesity.	O
Musculoskeletal	O
exam	O
is	O
otherwise	O
unremarkable.	O
Range	O
of	O
motion	O
is	O
full.	O
Strength	O
and	O
tone	O
is	O
normal.	O
The	O
patient	O
does	O
have	O
a	O
positive	O
straight	O
leg	O
raise	O
on	O
the	O
left,	O
reproducing	O
her	O
back	O
pain.	O
Neurologically,	O
the	O
patient	O
has	O
a	O
flat	O
affect	O
but	O
is	O
oriented	O
with	O
intact	O
cranial	O
nerves	O
and	O
no	O
focal	O
neurologic	O
deficit	O
in	O
either	O
motor	O
or	O
sensory	O
distribution.	O
She	O
does,	O
however,	O
walk	O
with	O
a	O
limp,	O
which	O
is	O
chronic	O
for	O
her.	O
LABORATORY	O
DATA:	O
White	O
blood	O
count	O
is	O
slightly	O
elevated	O
at	O
11.3.	O
BUN	O
and	O
creatinine	O
is	O
also	O
significantly	O
elevated	O
at	O
26	O
and	O
1.1,	O
respectively.	O
Potassium	O
is	O
also	O
quite	O
low	O
at	O
2.9.	O
Urinalysis	O
is	O
positive	O
with	O
35-40	O
white	O
blood	O
cells,	O
15-20	O
red	O
blood	O
cells,	O
and	O
2+	O
bacteria.	O
EMERGENCY	O
DEPARTMENT	O
COURSE:	O
My	O
chief	O
concern	O
for	O
this	O
patient	O
is	O
her	O
inability	O
to	O
care	O
for	O
self	O
at	O
home.	O
It	O
is	O
apparent	O
that	O
her	O
son	O
is	O
interested	O
in	O
helping	O
her	O
and	O
do	O
so,	O
but	O
the	O
patient	O
on	O
physical	O
exam	O
has	O
some	O
obvious	O
lack	O
of	O
hygiene	O
and	O
I	O
am	O
concerned	O
that	O
potentially	O
the	O
patient	O
has	O
either	O
an	O
underlying	O
neurologic	O
or	O
psychiatric	O
issue	O
that	O
is	O
preventing	O
her	O
from	O
thriving	O
at	O
home.	O
Psychiatry	O
initially	O
was	O
consulted.	O
Their	O
impression	O
was	O
that	O
the	O
patient	O
may	O
have	O
neuro	O
behavior	O
problem	O
that	O
needs	O
further	O
outpatient	O
workup,	O
and	O
she	O
was	O
referred	O
to	O
a	O
specific	O
providers	O
within	O
the	O
next	O
2	O
weeks.	O
The	O
patient	O
was	O
treated	O
with	O
potassium	O
chloride	O
for	O
her	O
hypokalemia.	O
Care	O
coordinator	O
and	O
social	O
services	O
were	O
consulted.	O
The	O
patient	O
will	O
have	O
a	O
home	O
safety	O
evaluation	O
and	O
will	O
be	O
referred	O
for	O
primary	O
care	O
services,	O
was	O
discharged	O
with	O
a	O
cane	O
for	O
ambulation,	O
and	O
will	O
likely	O
also	O
receive	O
VNA	ORGANIZATION
Services	ORGANIZATION
to	O
help	O
her	O
with	O
medication	O
compliance	O
and	O
other	O
ADLs.	O
********	O
Not	O
reviewed	O
by	O
Attending	O
Physician	O
********	O
DIAGNOSES:	O
1.	O
Chronic	O
low	O
back	O
pain.	O
2.	O
Hypokalemia.	O
3.	O
Urinary	O
tract	O
infection.	O
4.	O
Failure	O
to	O
thrive.	O
DISPOSITION:	O
Discharged.	O
CONDITION	O
ON	O
DISCHARGE:	O
Satisfactory.	O
______________________________	O
IP,	O
CARRIE	O
MD	O
D:	O
01/13/93	O
T:	O
01/13/93	O
Dictated	O
By:	O
IP,	O
CARRIE	O
eScription	O
document:1-1843613	O
BFFocus	O
****************************************************************************************************	O
Record	O
date:	O
2093-02-28	O
Personal	O
Data	O
and	O
Overall	O
Health	O
Mrs.	O
Barr	PERSON
is	O
a	O
57	O
years	O
old	O
lady	O
with	O
multiple	O
significant	O
medical	O
problems	O
new	O
to	O
KEKELA.	O
Chief	O
Complaint	O
She	O
is	O
here	O
today	O
for	O
a	O
Physical	O
Exam	O
History	O
of	O
Present	O
Illness	O
Patient	O
has	O
HO	O
HTN	O
for	O
years	O
and	O
CVA.	O
She	O
has	O
had	O
4	O
CVAs	O
in	O
the	O
past.	O
First	O
one	O
in	O
2087,	O
2088,	O
2089	O
and	O
2090.	O
She	O
was	O
not	O
taking	O
her	O
BP	O
meds	O
those	O
times	O
and	O
she	O
has	O
been	O
smoking	O
1	O
PPD	ORGANIZATION
since	O
then	O
despite	O
being	O
told	O
she	O
had	O
to	O
quit	O
smoking.	O
She	O
was	O
seen	O
at	O
the	O
LDC	O
ED	O
twice	O
in	O
Jan	O
13	O
and	O
21/05.	O
First	O
visit	O
she	O
was	O
taken	O
by	O
her	O
son	O
after	O
he	O
couldn't	O
take	O
care	O
of	O
her	O
anymore.	O
Not	O
doing	O
anything	O
at	O
home,	O
not	O
caring	O
about	O
anything.	O
Was	O
seen	O
by	O
psych	O
and	O
arranged	O
to	O
have	O
a	O
PCP,	O
spoke	O
about	O
therapy	O
as	O
well.	O
Then	O
1/21/93	O
seen	O
for	O
chest	O
pain.	O
EKG	O
negative	O
and	O
was	O
D/C	O
home.	O
Had	O
a	O
MIBI	O
done	O
1/23/93	O
1.	O
Clinical	O
Response:	O
Non-ischemic.	O
2.	O
ECG	O
Response:	O
No	O
ECG	O
changes	O
during	O
infusion.	O
3.	O
Myocardial	O
Perfusion:	O
Normal.	O
4.	O
Global	O
LV	O
Function:	O
Normal.	O
She	O
states	O
she	O
is	O
supposed	O
to	O
be	O
on	O
Coumadin	O
for	O
the	O
strokes	O
and	O
she	O
self	O
D/C	O
it	O
last	O
year.	O
Previous	O
PCP	O
at	O
EDUCARE-FARGO	O
Dr.	O
Nancy	PERSON
Odell.	O
Changing	O
to	O
KEKELA	ORGANIZATION
because	O
her	O
son	O
thinks	O
she	O
can	O
get	O
better	O
help	O
here.	O
She	O
looks	O
depressed	O
and	O
I	O
mentioned	O
that	O
to	O
her.	O
She	O
states	O
"I	O
always	O
have	O
looked	O
the	O
same".	O
After	O
speaking	O
for	O
her	O
for	O
20	O
minutes	O
she	O
tells	O
me	O
that	O
at	O
age	O
28	O
she	O
had	O
a	O
miscarriage	O
of	O
her	O
twins.	O
She	O
was	O
jumping	O
rope	O
and	O
after	O
coming	O
back	O
home	O
her	O
husband	O
told	O
her	O
if	O
she	O
was	O
trying	O
to	O
kill	O
the	O
twins.	O
That	O
night	O
she	O
had	O
a	O
miscarriage	O
and	O
one	O
baby	O
was	O
born	O
in	O
the	O
toilet	O
and	O
the	O
otherone	O
was	O
taken	O
by	O
the	O
EMTs.	O
She	O
was	O
4	O
months	O
pregnant.	O
She	O
had	O
therapy	O
then	O
and	O
meds	O
and	O
felt	O
a	O
little	O
better.	O
Her	O
husband	O
died	O
6	O
years	O
ago	O
and	O
before	O
he	O
died	O
he	O
reminded	O
her	O
that	O
she	O
had	O
killed	O
his	O
twins.	O
She	O
felt	O
bad	O
and	O
has	O
felt	O
bad	O
since.	O
She	O
cries	O
during	O
interview.	O
She	O
worked	O
for	O
many	O
years	O
for	O
mcilhenny	O
company,	O
Fire	O
Chief	O
and	O
retird	O
last	O
year.	O
She	O
denies	O
being	O
depressed,	O
denies	O
suicidal	O
ideation	O
or	O
plan.	O
States	O
has	O
trouble	O
sleeping	O
and	O
wants	O
AMbien	O
she	O
has	O
taken	O
in	O
the	O
past.	O
Also	O
has	O
taken	O
Zoloft	O
in	O
the	O
past.	O
Not	O
interested	O
on	O
meds	O
now.	O
Past	O
Medical	O
History	O
Hypertension	O
Obesity	O
Cerebrovascular	O
accident	O
2087,2088,2089,2090	O
Past	O
Surgical	O
History	O
Cesarean	O
section	O
x	O
1	O
Left	O
leg	O
ORIF	ORGANIZATION
Allergies	ORGANIZATION
NKA	ORGANIZATION
Medications	ORGANIZATION
HYDROCHLOROTHIAZIDE	ORGANIZATION
25	O
MG	O
PO	O
QD	O
Metoprolol	O
Tartrate	O
50MG	O
TABLET	O
take	O
1	O
Tablet(s)	O
PO	O
BID	O
Family	O
History	O
Negative	O
for	O
breast	O
cancer,	O
colon	O
cancer,	O
prostate	O
cancer,	O
skin	O
cancer,	O
leukemias,	O
CAD,	O
Myocardial	O
infarction,	O
Hypertension,	O
Diabetes	O
Mellitus,	O
Hypercholesterolemia,	O
Asthma.	O
Social	O
History	O
Lives	O
at	O
home	O
with	O
her	O
son	O
and	O
his	O
wife	O
Smoking	O
1	O
PPD	ORGANIZATION
cigarretes	O
ETOH	O
occasional.	O
Denies	O
IVDA.	O
Denies	O
other	O
recreational	O
drugs	O
Sexually	O
active	O
with	O
the	O
same	O
partner	O
for	O
the	O
past	O
1	O
year.	O
Doesn't	O
use	O
condoms.	O
No	O
history	O
of	O
blood	O
transfussions	O
Retired	O
MC	ORGANIZATION
employee	O
-	O
Fire	O
Chief	O
Routine	O
screening	O
questions	O
for	O
abuse	O
asked.	O
Patient	O
stated	O
that	O
abuse	O
is	O
not	O
an	O
issue	O
at	O
the	O
present	O
time.	O
Habits	O
Exercises	O
rarely	O
Uses	O
seat	O
belt	O
most	O
of	O
the	O
time	O
Preventive	O
Health	O
History	O
Last	O
Physical	O
exam	O
1/92	O
Last	O
mammogram	O
1/92	O
at	O
EDUCARE-FARGO	O
by	O
her	O
report	O
Last	O
Pap	O
smear	O
1/92	O
normal	O
at	O
EDUCARE-FARGO	O
by	O
her	O
report	O
COlonoscopy	O
-	O
never	O
Bone	O
density	O
test	O
-	O
never	O
Review	O
of	O
Systems	O
Cardiopulmonary	O
:	O
Occasional	O
chest	O
pain,	O
Denies	O
shortness	O
of	O
breath,	O
PND,	O
orthopnea,	O
leg	O
edema,	O
Palpitations	O
HEENT:	O
doesn't	O
wear	O
glasses,	O
normal	O
hearing	O
Genitourinary:	O
denies	O
dysuria,	O
polyuria,	O
vaginal	O
discharge.	O
Occasional	O
back	O
pain	O
LMP	O
:	O
4/92	O
General:	O
Denies	O
weight	O
changes,	O
fever,	O
chills,	O
night	O
sweats.	O
Doesn't	O
follow	O
a	O
diet,	O
eats	O
fast	O
foods,	O
regular	O
sodas	O
Endocrino:	O
Denies	O
dizzyness,	O
diaphoresis	O
Gastrointestinal:	O
Denies	O
diarrhea,	O
constipation,	O
abdominal	O
pain,	O
heartburn,	O
nausea,	O
vomiting,	O
no	O
rectal	O
bleeding	O
Skin	O
:	O
no	O
lesions,	O
no	O
hair	O
loss	O
Neurological:	O
no	O
weakness,	O
numbness,	O
tingling	O
after	O
the	O
strokes.	O
Recovered	O
completely	O
Musculoskeletal:	O
left	O
leg	O
pain	O
every	O
day	O
and	O
low	O
back	O
pain.	O
Wants	O
Celebrex.	O
Psychiatric:	O
no	O
anxiety,	O
Denies	O
depression	O
or	O
suicidal	O
thoughts	O
but	O
cries	O
many	O
times	O
when	O
confronted	O
about	O
depression	O
All	O
other	O
systems	O
are	O
negative	O
Vital	O
Signs	O
BLOOD	O
PRESSURE	O
150/90	O
PULSE	O
76	O
RESPIRATORY	O
RATE	O
14	O
TEMPERATURE	O
97	O
F	O
HEIGHT	O
64	O
in	O
WEIGHT	O
234	O
lb	O
BMI	O
40.2	O
Physical	O
Exam	O
Alert,	O
obese,	O
unkempt,	O
in	O
no	O
respiratory	O
distress,	O
tolerates	O
decubitus	O
Normocephalic,	O
no	O
scalp	O
lesions,	O
wearing	O
a	O
wig.	O
PEERL,	O
EOMI,	O
no	O
jaundice	O
in	O
sclerae,	O
no	O
conjunctival	O
hyperhemia.	O
Bilateral	O
fundoscopy:	O
sharp	O
disks,	O
no	O
papilledema,	O
no	O
AV	O
nicking	O
Bilateral	O
otoscopy:	O
normal,	O
no	O
exudates,	O
normal	O
timpanic	O
membranes.	O
No	O
sinus	O
tenderness	O
on	O
palpation	O
on	O
percussion.	O
Bilateral	O
rhinoscopy:	O
normal	O
Oral	O
mucosa	O
moist,	O
no	O
oral	O
thrush,	O
no	O
lesions,	O
no	O
masses	O
Throat:	O
normal,	O
no	O
exudates	O
or	O
plaques,	O
normal	O
tonsils.	O
Neck:	O
supple,	O
no	O
masses	O
or	O
adenopathies,	O
normal	O
thyroid	O
palpation,	O
no	O
JVD,	O
normal	O
carotid	O
pulses,	O
no	O
carotid	O
bruit.	O
Lungs:	O
N	O
use	O
of	O
accesory	O
muscles,	O
no	O
intercostal	O
retractions,	O
clear	O
anteriorly	O
and	O
posteriroly	O
to	O
auscultation,	O
no	O
rhonchi,	O
crackles	O
or	O
wheezes,	O
normal	O
percussion,	O
no	O
egophony.	O
Heart:	O
regular,	O
rhythmic,	O
S1S2	O
normal,	O
no	O
S3	O
or	O
murmurs.	O
Breast	O
examination	O
:	O
Large	O
sized	O
breasts,	O
no	O
masses,	O
no	O
skin	O
lesions,	O
no	O
tenderness,	O
no	O
nipple	O
discharges	O
or	O
axillary	O
adenopathies.	O
Abdomen:	O
lower	O
midline	O
surgical	O
scar,	O
no	O
aortic	O
bruit,	O
soft,	O
diffuse	O
tenderness,	O
no	O
masses	O
or	O
organomegalies,	O
BS(+),	O
no	O
rebound.	O
No	O
inguinal	O
adenopathies	O
or	O
hernias,	O
good	O
femoral	O
pulses.	O
Pelvic	O
exam:	O
athrophic	O
external	O
genitalia,	O
no	O
masses	O
or	O
lesions,	O
normal	O
hair	O
distribution,	O
no	O
cystocele	O
or	O
rectocele.	O
No	O
masses,	O
scaring	O
or	O
tenderness	O
on	O
urethra	O
examination.	O
Stool	O
seen	O
on	O
the	O
rectal	O
area	O
Speculum	O
exam:	O
Normal	O
looking	O
cervix,closed	O
oz,	O
no	O
vaginal	O
discharge	O
or	O
lesions.	O
Pap	O
smear	O
taken,	O
GC	O
and	O
Chlamydia	O
taken.	O
Bimanual	O
exam:	O
no	O
cervical	O
motion	O
tenderness,	O
no	O
masses,	O
no	O
adnexal	O
tenderness	O
or	O
masses,	O
unable	O
to	O
feel	O
uterine	O
size	O
due	O
to	O
obesity	O
Rectal	O
exam:	O
no	O
external	O
hemmorrhoids	O
Digital	O
exam:	O
no	O
lesions,	O
normal	O
sphincter	O
tone,	O
Guaiac	O
stool	O
Negative	O
Ext:	O
1++	O
edema	O
left	O
leg,	O
ORIF	O
midline	O
large	O
surgical	O
scar	O
seen,	O
good	O
pulses,	O
normal	O
skin,	O
no	O
varicosities	O
Skin:	O
no	O
lesions	O
Neurological:	O
Alert,	O
oriented	O
x	O
3,	O
Speech	O
normal.	O
Cranial	O
nerves	O
Normal,	O
Strenght	O
5/5/	O
bilateral	O
symmetrical	O
in	O
upper	O
and	O
lower	O
extremities.	O
DTR	O
2++	O
symmetrical,	O
sensory	O
intact,	O
no	O
pathological	O
reflexes	O
Assessment	O
Physical	O
exam	O
remarkable	O
for	O
:	O
1.	O
HYPERTENSION	O
Explained	O
the	O
pathophysiology	O
of	O
Hypertension	O
and	O
the	O
importance	O
of	O
goog	O
blood	O
pressure	O
control	O
Explained	O
the	O
increased	O
risk	O
of	O
CVA,	O
CAD,	O
eye	O
and	O
renal	O
problems	O
Spoke	O
about	O
the	O
need	O
to	O
quit	O
smoking,	O
loose	O
weight,	O
follow	O
a	O
low	O
fat	O
low	O
carb	O
diet.	O
Patient	O
undesrtood	O
Recommended	O
to	O
follow	O
a	O
low	O
salt	O
diet	O
and	O
exercise	O
regulalry	O
Will	O
check	O
a	O
SMA-7,	O
CBCm	O
TSH	O
and	O
U/A	ORGANIZATION
to	O
evaluate	O
renal	O
function	O
Continue	O
taking	O
HCTZ	O
QD	O
#	O
30	O
and	O
6	O
refills	O
and	O
follow	O
up	O
in	O
6	O
weeks	O
for	O
BP	O
check.	O
Increase	O
Metroprolol	O
to	O
50	O
mg	O
BID	O
#	O
60	O
and	O
6	O
refills	O
Increased	O
risk	O
for	O
CAD	O
and	O
needs	O
to	O
modify	O
life	O
style.	O
2.	O
HO	O
CEREBROVASCULAR	O
ACCIDENT	O
She	O
reports	O
4	O
strokes	O
and	O
no	O
residual	O
neurological	O
deficit.	O
Declines	O
Coumadin.	O
Is	O
on	O
ASA	O
sometimes	O
Start	O
Plavix	O
75mg	O
QD	O
#	O
30	O
and	O
6	O
refills	O
to	O
decrease	O
risk	O
of	O
CVA	ORGANIZATION
Again	O
needs	O
to	O
quit	O
smoking	O
ASAP	O
3.	O
OBESITY	O
Calculated	O
BMI	O
40	O
Obesity	O
Type	O
2	O
Spoke	O
about	O
diet	O
and	O
exercise	O
as	O
the	O
main	O
steps	O
to	O
loose	O
weigth.	O
Explained	O
that	O
Obesity	O
predisposes	O
patients	O
to	O
develop	O
Diabetes	O
Mellitus,	O
Hypertension,	O
CAD,	O
CVA,	O
Strokes,	O
Osteoarthritis	O
specially	O
of	O
the	O
knees,	O
Colon	O
cancer	O
and	O
breast	O
cancer	O
and	O
endometrial	O
cancer	O
in	O
woman	O
Encouraged	O
the	O
patient	O
to	O
increase	O
the	O
exercise	O
to	O
5-7	O
times	O
per	O
week	O
and	O
35-45	O
minutes	O
of	O
cardiovascular.	O
Change	O
to	O
diet	O
sodas,	O
and	O
avoid	O
fast	O
foods	O
completely	O
Increase	O
the	O
amount	O
of	O
vegetables	O
and	O
avoid	O
carbohydrates,	O
sugar	O
products	O
and	O
follow	O
a	O
low	O
fat	O
diet	O
Check	O
TSH,	O
Lipid	O
profile	O
and	O
Glucose	O
4.	O
SMOKER	O
Recommendations	O
to	O
quit	O
smoking	O
Spoke	O
about	O
increased	O
risk	O
for	O
lung	O
cancer,	O
bladder	O
cancer,	O
cervical	O
cancer,	O
CAD,	O
CVA	ORGANIZATION
on	O
smokers	O
Spoke	O
about	O
Nicotine	O
patch,	O
Nicotine	O
gum,	O
Zyban	O
etc	O
Recommended	O
the	O
patient	O
to	O
use	O
Nicorette	O
gum.	O
Referal	O
to	O
see	O
Georgia	ORGANIZATION
Upshaw	ORGANIZATION
-	O
smoking	O
cessation	O
counselour.	O
She	O
is	O
not	O
iterested	O
on	O
quitting	O
5.	O
DEPRESSION	O
Spent	O
25	O
minutes	O
with	O
her	O
talking	O
about	O
depression.	O
She	O
was	O
very	O
quit	O
at	O
the	O
beggining	O
of	O
the	O
visit	O
and	O
denied	O
depression	O
even	O
after	O
finishing	O
her	O
visit.	O
But	O
she	O
cried	O
a	O
lot	O
when	O
telling	O
me	O
about	O
her	O
twins.	O
Explained	O
that	O
is	O
never	O
late	O
to	O
get	O
therapy	O
and	O
feel	O
happier	O
on	O
meds	O
and	O
therapy.	O
She	O
has	O
had	O
4	O
CVA	ORGANIZATION
and	O
still	O
smokes	O
and	O
doesn't	O
care	O
of	O
herself,	O
which	O
is	O
remarkable	O
and	O
seems	O
to	O
me	O
she	O
is	O
passively	O
suicidal	O
by	O
following	O
those	O
behaviours.	O
Spoke	O
about	O
depression	O
and	O
different	O
modalities	O
of	O
therapy	O
Spoke	O
about	O
mental	O
therapy	O
and	O
pharmacological	O
therapy	O
for	O
depression.	O
Recommended	O
her	O
to	O
start	O
medications,	O
she	O
declines	O
I	O
agree	O
to	O
Rx	O
Ambien	O
5	O
mg	O
QHS	O
PRN	O
#	O
30	O
and	O
6	O
refills	O
She	O
will	O
call	O
the	O
Psychiatric	O
department	O
at	O
KEKELA	ORGANIZATION
to	O
start	O
therapy	O
5.	O
MENOPAUSE	O
Start	O
Calcium	O
500	O
mg	O
2	O
tb	O
QD	O
#	O
60	O
and	O
6	O
refills	O
MVI	O
#	O
30	O
and	O
6	O
refills	O
Schedule	O
BDT	O
and	O
tehrapy	O
if	O
indicated	O
6.	O
LEFT	O
LEG	O
PAIN	O
after	O
ORIF	O
Tylenol	O
XS	O
TID	O
for	O
now	O
No	O
Celebre	O
on	O
a	O
patient	O
with	O
high	O
risk	O
of	O
CVA	ORGANIZATION
and	O
CAD	O
Plan	O
Will	O
send	O
the	O
Pap	O
smear	O
results	O
in	O
2	O
weeks	O
Will	O
call	O
if	O
GC	O
or	O
Chlamydia	O
positive	O
in	O
48	O
hours	O
Will	O
check	O
a	O
lipid	O
profile	O
Schedule	O
BDT	O
Schedule	O
mammogram	O
Schedule	O
colonoscopy	O
Three	O
stool	O
cards	O
given	O
to	O
bring	O
soon	O
Counseling	O
Recommendations	O
to	O
exercise	O
2-3	O
times	O
per	O
week	O
Follow	O
a	O
low	O
fat	O
diet	O
****************************************************************************************************	O
Record	O
date:	O
2093-12-26	O
Patient	O
Name:	O
BARR,NICKY	O
[	O
MRN:	O
32612454LDC	O
]	O
Date	O
of	O
Visit:	O
12/26/93	O
Chief	O
Complaint	O
Mrs	PERSON
Barr	PERSON
is	O
here	O
to	O
follow	O
up	O
on	O
Hypertension,	O
diabetes	O
History	O
of	O
Present	O
Illness	O
Mrs	O
Barr	PERSON
has	O
been	O
doing	O
well.	O
Has	O
a	O
8	O
weeks	O
grandaughter	O
and	O
is	O
very	O
happy	O
with	O
her.	O
Interesetd	O
on	O
quiting	O
smoking.	O
Can	O
not	O
hold	O
the	O
baby	O
as	O
long	O
as	O
she	O
smokes	O
and	O
she	O
has	O
decided	O
to	O
quit	O
to	O
be	O
able	O
to	O
care	O
or	O
the	O
baby.	O
She	O
is	O
taking	O
all	O
her	O
meds	O
as	O
directed.	O
HbA1C	O
in	O
5/93	O
7.2	O
and	O
LDL	O
99	O
on	O
Lipitor	O
20	O
mg.	O
She	O
still	O
smokes	O
1/2	O
PPD	O
but	O
better	O
than	O
before	O
Declines	O
the	O
FLu	O
shot	O
and	O
Pneumovax	O
Not	O
exercising.	O
Walks	O
daily	O
1-2	O
blocks	O
for	O
chores.	O
Review	O
of	O
Systems	O
C/P:Denies	O
chest	O
pain,	O
SOB	O
Skin(-)	O
MSK(-)	O
HEENT	O
:	O
Denies	O
cold	O
sx	O
GU(-)	O
General:	O
Denies	O
fever,	O
chills,	O
weigth	O
loss	O
GI	O
:Denies	O
nausea,	O
vomiting,	O
diarrhea	O
Neuro:	O
denies	O
headaches	O
All	O
other	O
systems	O
negative	O
Problems	O
Cerebrovascular	O
accident	O
:	O
x	O
4	O
Obesity	O
:	O
BMI	O
40	O
-	O
Type	O
3	O
Hypertension	O
Diabetes	O
mellitus	O
type	O
2	O
Osteopenia	O
:	O
T	O
score	O
-1.8	O
L1-4	O
Allergies	O
NKA	O
Medications	O
Plavix	O
(CLOPIDOGREL)	O
75	O
MG	O
(75MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Hydrochlorothiazide	O
25	O
MG	O
PO	O
QD	O
Metoprolol	O
TARTRATE	O
100	O
MG	O
(100MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Multivitamins	O
1	O
CAPSULE	O
PO	O
QD	O
Ambien	O
(ZOLPIDEM	O
TARTRATE)	O
5	O
MG	O
PO	O
QHS	O
Fosamax	O
(ALENDRONATE)	O
35MG	O
TABLET	O
PO	O
QSunday	O
AM	O
,	O
Take	O
it	O
with	O
8	O
Oz	O
of	O
water	O
Lisinopril	O
5	O
MG	O
(5MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Oscal	O
500	O
+	O
D	O
(CALCIUM	O
CARBONATE	O
1250	O
MG	O
(500MG	O
ELEM	O
CA)/	O
VIT	O
D	O
200	O
IU))	O
2	O
TAB	O
PO	O
qd	O
Ibuprofen	O
800	O
MG	O
(800MG	O
TABLET	O
take	O
1)	O
PO	O
TID	O
PRN	O
leg	O
pain	O
Lipitor	O
(ATORVASTATIN)	O
20MG	O
TABLET	O
PO	O
QHS	O
Vital	O
Signs	O
BP	O
136/90,	O
P	O
68,	O
RR	O
14,	O
Wt	O
242	O
lb	O
Physical	O
Exam	O
Alert,	O
in	O
no	O
distress,	O
obese,	O
happy	O
looking,	O
smiling	O
HEENT	O
:	O
PEERL,	O
EOMI,	O
oral	O
mucosa	O
moist,	O
no	O
lesions	O
Neck	O
:	O
supple,	O
no	O
masses	O
or	O
nodes,	O
no	O
JVD.	O
Lungs:	O
clear	O
anteriorly	O
and	O
posteriroly	O
to	O
auscultation,	O
no	O
rhonchi,	O
crackles	O
or	O
wheezes	O
Heart:	O
regular,.	O
S1S2	O
normal,	O
no	O
S3	O
or	O
murmurs	O
Abdomen:	O
soft,	O
no	O
tenderness	O
BS(+),	O
no	O
rebound	O
Ext:	O
no	O
edema,	O
normal	O
skin	O
Feet	O
exam:	O
normal	O
ksin,.	O
good	O
DP,	O
good	O
capillary	O
filling	O
Assessment	O
1.	O
HYPERTENSION	O
Poorly	O
controlled	O
today	O
Continue	O
Lisinopril	O
5	O
mg	O
QD	O
#	O
30	O
and	O
3	O
refills	O
Increase	O
Metroprolol	O
to	O
50	O
mg	O
BID	O
from	O
QD	O
and	O
HCTZ	ORGANIZATION
25	O
mg	O
QD	O
See	O
me	O
in	O
8	O
weeks	O
to	O
recheck	O
BP	O
Check	O
SMA-7	O
2.	O
CEREBROVASCULAR	O
ACCIDENT	O
Uncontrolled	O
HTN	O
Smoking	O
-	O
active	O
Elevated	O
LDL	O
Obese	O
Sedentary	O
On	O
statins.	O
Goal	O
LDL	O
<100	O
Check	O
Lipid	O
profile	O
and	O
LFTs	O
Goal	O
LDL	O
<70	O
Quit	O
smoking	O
-	O
try	O
Nicorette	O
Gum.	O
Referal	O
to	O
see	O
Georgia	LOCATION
Upshaw-	O
smoking	O
cessation	O
counselour	O
Plavix	O
75	O
mg	O
QD	O
3.	O
DIABETES	O
MELLITUS	O
HbA1C	O
7.2	O
in	O
5/93	O
Goal	O
below	O
7	O
Repeat	O
HbA1C	O
and	O
microalbuminuria	O
Due	O
for	O
eye	O
exam.	O
Declines	O
Flu	O
shot	O
4.	O
OSTEOPENIA	O
Continue	O
Fosamax	O
35	O
mg	O
Q	O
week,	O
calcium	O
and	O
MVI	O
Plan	O
Follow	O
up	O
in	O
8	O
weeks	O
_____________________________________________	O
Victoria	O
Amy-Armstrong,	O
M.D.	O
****************************************************************************************************	O
Record	O
date:	O
2094-09-07	O
Patient	O
Name:	O
BARR,NICKY	O
[	O
32612454(LDC)	O
]	O
Date	O
of	O
Visit:	O
09/07/2094	O
Chief	O
Complaint	O
Mrs	PERSON
Barr	PERSON
is	O
here	O
to	O
follow	O
up	O
on	O
Hypertension,	O
diabetes,	O
cholesterol	O
History	O
of	O
Present	O
Illness	O
Mrs	O
Barr	PERSON
has	O
been	O
doing	O
well.	O
Didn't	O
take	O
her	O
blood	O
pressure	O
meds	O
today,	O
forgot	O
Seen	O
yesterday	O
by	O
RN	O
and	O
got	O
glucemeter.	O
Still	O
can	O
not	O
make	O
it	O
work	O
Has	O
been	O
on	O
Metformin	O
850	O
mg	O
TID	O
and	O
Avandia	ORGANIZATION
4	O
mg	O
QD	O
Not	O
checking	O
her	O
glucose	O
yet	O
Last	O
HbA1C	O
9.6	O
on	O
6/28	O
down	O
from	O
5/11/94	O
-	O
10.6	O
She	O
is	O
taking	O
Lipitor	O
80	O
mg	O
for	O
few	O
weeks.	O
Last	O
LDL	O
134	O
on	O
6/28	O
On	O
Lisinopril	O
to	O
40	O
mg	O
QD	O
and	O
Metoprolol	O
100	O
mg	O
BID	O
She	O
still	O
smokes	O
1/2	O
PPD	O
Review	O
of	O
Systems	O
C/P:	O
Occasional	O
chest	O
pain	O
on	O
and	O
off,	O
No	O
SOB	O
or	O
leg	O
edema	O
Skin(-)	O
MSK(-)	O
HEENT	O
:	O
Denies	O
cold	O
sx	O
General:	O
Denies	O
fever,	O
chills,	O
weigth	O
loss	O
GI	O
:Denies	O
nausea,	O
vomiting,	O
diarrhea	O
Neuro:	O
denies	O
headaches	O
Psych:	O
depressed.	O
All	O
other	O
systems	O
negative	O
Problems	O
Cerebrovascular	O
accident	O
:	O
x	O
4	O
Obesity	O
:	O
BMI	O
40	O
-	O
Type	O
3	O
Hypertension	O
Diabetes	O
mellitus	O
type	O
2	O
Osteopenia	O
:	O
T	O
score	O
-1.8	O
L1-4	O
Allergies	O
NKA	O
Medications	O
Plavix	O
(CLOPIDOGREL)	O
75	O
MG	O
(75MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Hydrochlorothiazide	O
25	O
MG	O
PO	O
QD	O
Multivitamins	O
1	O
CAPSULE	O
PO	O
QD	O
Fosamax	O
(ALENDRONATE)	O
35MG	O
TABLET	O
PO	O
QSundayAM	O
,	O
Take	O
it	O
with	O
8	O
Oz	O
of	O
water	O
Oscal	O
500	O
+	O
D	O
(CALCIUM	O
CARBONATE	O
1250	O
MG	O
(500MG	O
ELEM	O
CA)/	O
VIT	O
D	O
200	O
IU))	O
2	O
TAB	O
PO	O
qd	O
Ambien	O
(ZOLPIDEM	O
TARTRATE)	O
5	O
MG	O
PO	O
QHS	O
Ibuprofen	O
800	O
MG	O
(800MG	O
TABLET	O
take	O
1)	O
PO	O
TID	O
PRN	O
leg	O
pain	O
Lisinopril	O
40	O
MG	O
(40MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
,	O
dose	O
increased	O
from	O
10	O
to	O
20	O
mg	O
QD	O
on	O
5/11/94	O
and	O
from	O
20	O
to	O
40	O
mg	O
today	O
6/1/94	O
Metoprolol	O
TARTRATE	O
150	O
MG	O
(100MG	O
TABLET	O
take	O
1.5)	O
PO	O
BID	O
,	O
Change	O
of	O
dose	O
from	O
100	O
mg	O
BID	O
to	O
150	O
mg	O
BID	O
Avandia	O
(ROSIGLITAZONE)	O
4	O
MG	O
(4MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Lipitor	O
(ATORVASTATIN)	O
80MG	O
TABLET	O
PO	O
QHS	O
,	O
dose	O
increased	O
from	O
40	O
to	O
80	O
mg	O
today	O
Metformin	O
850MG	O
TABLET	O
PO	O
TID	O
,	O
dose	O
increased	O
today	O
from	O
500	O
mg	O
TID	O
to	O
850	O
mg	O
TID.	O
Thanks	O
Vital	O
Signs	O
BP	O
160/110,	O
P	O
78,	O
RR	O
14,	O
Temp	O
97	O
F,	O
Wt	O
249	O
lb	O
Physical	O
Exam	O
Alert,	O
in	O
no	O
distress,	O
obese,	O
flat	O
affect	O
HEENT	O
:	O
oral	O
mucosa	O
moist,	O
no	O
lesions.	O
Throat	O
clear	O
Neck	O
:	O
supple,	O
no	O
masses	O
or	O
nodes,	O
no	O
JVD.	O
Lungs:	O
clear	O
anteriorly	O
and	O
posteriroly	O
to	O
auscultation,	O
no	O
rhonchi,	O
crackles	O
or	O
wheezes	O
Heart:	O
regular,.	O
S1S2	O
normal,	O
no	O
S3	O
or	O
murmurs	O
Abdomen:	O
soft,	O
no	O
tenderness	O
BS(+),	O
no	O
rebound	O
Ext:	O
no	O
edema,	O
normal	O
skin	O
Feet	O
exam:	O
normal	O
skin,.	O
good	O
DP,	O
good	O
capillary	O
filling	O
Assessment	O
1.	O
HYPERTENSION	O
Poorly	O
controlled	O
today	O
hasn't	O
taken	O
her	O
meds	O
yet	O
Explained	O
the	O
importance	O
of	O
taking	O
BP	ORGANIZATION
meds	O
eraly	O
AM	O
Keep	O
same	O
meds	O
and	O
see	O
me	O
in	O
6	O
weeks	O
Continue	O
Metroprolol	O
to	O
150	O
mg	O
BID	O
Continue	O
HCTZ	O
25	O
mg	O
QD	O
Continue	O
Lisinopril	O
to	O
40	O
mg	O
QD	O
See	O
me	O
in	O
2	O
weeks	O
to	O
recheck	O
BP	ORGANIZATION
2.	O
CEREBROVASCULAR	O
ACCIDENT	O
Uncontrolled	O
HTN	O
Smoking	O
-	O
active	O
Elevated	O
LDL	O
Obese	O
Sedentary	O
On	O
statins.	O
Goal	O
LDL	O
<100	O
Goal	O
LDL	O
<70	O
Plavix	O
75	O
mg	O
QD	O
3.	O
DIABETES	O
MELLITUS	O
Start	O
checking	O
glucose	O
QD	ORGANIZATION
Continue	ORGANIZATION
Avandia	ORGANIZATION
4	O
mg	O
QD	O
and	O
Metformin	O
850	O
mg	O
TID	O
Goal	O
below	O
7	O
Repeat	O
HbA1C	O
now	O
and	O
glucose	O
Nutritionist	O
referal	O
Due	O
for	O
eye	O
exam.	O
LDL	O
134	O
on	O
Lipitor	O
40	O
Repeat	O
cholesterol	O
and	O
lipir	O
profile	O
today	O
4.	O
OSTEOPENIA	O
Continue	O
Fosamax	O
35	O
mg	O
Q	O
week,	O
calcium	O
and	O
MVI	O
Plan	O
Follow	O
up	O
in	O
2	O
weeks	O
_____________________________________________	O
Victoria	LOCATION
Amy-Armstrong,	O
M.D.	O
****************************************************************************************************	O
