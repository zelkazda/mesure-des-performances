Record	O
date:	O
2066-11-14	O
Chief	O
Complaint	O
Dyspnea	O
and	O
exertional	O
chest	O
discomfort	O
History	O
of	O
Present	O
Illness	O
Patient	O
is	O
a	O
59-year-old	O
Chadian	O
gentleman	O
with	O
multiple	O
medical	O
problems	O
including	O
DM	O
and	O
HTN	O
and	O
ESRF,	O
recently	O
started	O
on	O
hemodialysis	O
who	O
is	O
admitted	O
for	O
right	O
and	O
left	O
heart	O
cath	O
prior	O
to	O
MVR/CABG/MAZE.	O
Pt	O
presented	O
1	O
year	O
ago	O
with	O
complaints	O
of	O
increased	O
abd	O
girth,	O
lower	O
ext	O
edema,	O
and	O
worsening	O
DOE.	O
Cardiac	O
ultrasound	O
at	O
that	O
time	O
revealed	O
severe	O
mitral	O
regurgitation	O
secondary	O
to	O
diffuse	O
ventricular	O
dilation	O
and	O
papillary	O
muscle	O
displacement	O
with	O
ejection	O
fraction	O
36%.	O
He	O
denied	O
CP,	O
PND	O
and	O
orthopnea	O
at	O
the	O
time	O
and	O
adenoMibi	O
showed	O
inferior	O
thinning	O
which	O
might	O
be	O
consistent	O
with	O
a	O
prior	O
(silent)	O
IMI	O
without	O
evidence	O
of	O
ongoing	O
ischemia.	O
He	O
refused	O
further	O
work-up	O
or	O
intervention	O
until	O
recently.	O
He	O
presents	O
now	O
for	O
cath	O
and	O
surgery.	O
Review	O
of	O
Systems	O
As	O
in	O
HPI,	O
additionally;	O
-	O
General:	O
no	O
weight	O
loss/gain	O
-	O
ID:	O
no	O
fever	O
chills	O
-	O
Derm:	O
no	O
rash	O
pruritus	O
-	O
Gastrointestinal:	O
no	O
nausea	O
dysphagia	O
vomiting	O
dyspesia	O
-	O
Respiratory:no	O
dysnpnea	O
wheezing	O
-	O
Neuorologic:	O
no	O
weakness	O
numbness/tingling	O
seizures	O
syncope	O
-	O
Hematologic:	O
no	O
excessive	O
bruising,	O
bleeding	O
-	O
MSK	O
no	O
joint	O
pain	O
swelling	O
Problems	O
Neuropathy	O
Nephropathy	O
:	O
cr.	O
2.6.	O
3.3	O
gm	O
proteinuria	O
Hemodialysis	O
for	O
the	O
past	O
2	O
months.	O
Mature	O
L	O
fistula.	O
problems	O
with	O
fistula	O
last	O
week	O
Hyperlipidemia	ORGANIZATION
Hypertension	ORGANIZATION
Previously	ORGANIZATION
reasonably	O
controlled	O
on	O
meds.	O
Greatly	O
Improved	O
after	O
start	O
of	O
dialysis	O
Obesity	O
Diabetes	O
mellitus	O
:	O
type	O
2	O
Noncompliance	O
:	O
poor	O
compliance	O
repeated	O
noncompliance,	O
frequently,	O
but	O
not	O
always,	O
due	O
to	O
financial	O
constraints	O
and	O
language	O
barrier	O
Basal	O
cell	O
carcinoma	O
:	O
carcinoma	O
of	O
skin	O
left	O
cheek	O
Lower	O
extremity	O
ulcer	O
:	O
DIABETIC	O
FOOT	O
ULCER.	O
L	O
Foot	O
Abscess	O
:	O
Rt	O
shin	O
2062	O
Back:	O
11/64.	O
S/P	O
I&D	O
Mitral	O
valve	O
insufficiency	O
:	O
severe,	O
with	O
cardiomyopathy	O
and	O
EF	O
36%	O
Colonic	O
polyps	O
:	O
tubulovillious	O
adenoma	O
11/65.	O
Needs	O
repeat	O
scope	O
11/66	O
Social	O
History	O
Married	O
with	O
2	O
daughters.	O
Lives	O
in	O
De	PERSON
Soto	PERSON
and	O
assists	O
in	O
care	O
of	O
his	O
grandchildren.	O
He	O
has	O
been	O
unable	O
to	O
work	O
due	O
to	O
his	O
chronic	O
medical	O
conditions	O
and	O
is	O
exceptionally	O
limited	O
financially.	O
He	O
worked	O
(his	O
wife	O
works)	O
in	O
visual	O
art.	O
Medications	O
Aspirin	O
Buffered	O
325MG	O
TABLET	O
take	O
1	O
Tablet(s)	O
PO	O
QD	O
Toprol	O
Xl	O
(METOPROLOL	O
Succinate	O
Extended	O
Release)	O
100	O
MG	O
(100MG	O
TAB.SR	O
24H	O
take	O
1)	O
PO	O
QD	O
Ferrous	O
Sulfate	O
325	O
MG	O
(325(65)MG	O
TABLET	O
take	O
1)	O
PO	O
TID	O
Aranesp	O
(DARBEPOETIN	O
Alfa)	O
60MCG/.3ML	O
DISP	O
SYRIN	O
ML	O
SC	O
QWeek	O
Lipitor	O
(ATORVASTATIN)	O
80	O
MG	O
(80MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Zetia	O
(EZETIMIBE)	O
10	O
MG	O
(10MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Norvasc	O
(AMLODIPINE)	O
15	O
MG	O
PO	O
QD	O
Ca	O
Acetate	O
(CALCIUM	O
Acetate	O
(	O
1	O
Tablet=667	O
Mg))	O
1334	O
MG	O
(667MG	O
TABLET	O
take	O
2)	O
PO	O
TID	O
x	O
30	O
days,	O
take	O
with	O
meals;	O
if	O
snack	O
inbetween	O
meals	O
take	O
one	O
extra	O
pill	O
LISINOPRIL	O
10MG	O
TABLET	O
PO	O
QD	O
,	O
Evening	O
Qpm	O
Insulin	O
70/30	O
(HUMAN)	O
Variable	O
(70-30	O
U/ML	O
VIAL	O
)	O
SC	O
Physical	O
Exam	O
General:mildly	O
obese	O
adult	O
male,	O
dishevelled	O
Integument:	O
Warm	O
and	O
dry	O
HEENT:Moist	O
mucous	O
membranes,	O
edentulous	O
Neck:	O
Supple,	O
no	O
thryomegaly,	O
carotids	O
Chest:	O
Lungs	O
clear	O
to	O
auscultation	O
and	O
percussion	O
Cardiac:apical	O
impulse,	O
Normal	O
S1,	O
S2	O
III	O
HSM	O
murmer,	O
S3	O
gallop	O
Abdomen:	O
soft,	O
nondistended,	O
normal	O
bowel	O
sounds	O
Ext:	O
External	O
Labs	O
Lab	O
Results	O
For:	O
Ogrady,Ulysses	O
Test	O
IMC	O
11/08/2066	O
13:37	O
WBC	O
12.5(H)	O
RBC	O
4.36(L)	O
HGB	O
12.7(L)	O
HCT	O
38.2(L)	O
MCV	O
88	O
MCH	O
29.1	O
MCHC	O
33.2	O
PLT	O
329	O
RDW	O
15.6(H)	O
PT	O
13.8(H)	O
PT-INR	O
1.2(T)	O
PTT	ORGANIZATION
NA	O
137	O
K	O
4.8	O
CL	O
98(L)	O
CO2	O
26.6	O
BUN	O
42(H)	O
CRE	O
5.6(H)	O
GLU	O
130(H)	O
Nov	O
08	O
LEFT	O
FOREARM	O
RADIOCEPHALIC	O
AV	O
FISTULA	O
WITH	O
MARKED	O
STENOSIS	O
WITHIN	O
THE	O
VENOUS	O
OUTFLOW	O
JUST	O
BEYOND	O
THE	O
AV	O
ANASTOMOSIS	O
SUCCESSFULLY	O
TREATED	O
WITH	O
ANGIOPLASTY.	O
CXR	O
8/18/66	O
REPORT:	O
No	O
prior:	O
Findings:	O
The	O
lungs	O
are	O
clear	O
however	O
there	O
is	O
some	O
cephalization	O
of	O
blood	O
flow.	O
The	O
cardiac	O
shadow	O
is	O
at	O
the	O
upper	O
limits	O
of	O
normal.	O
The	O
bony	O
thorax	O
demonstrates	O
degenerative	O
changes.	O
Impression:	O
Cephalization	O
of	O
blood	O
flow	O
and	O
cardiomegaly	O
Assessment	O
58-year-old	O
Chadian	O
gentleman	O
with	O
multiple	O
medical	O
problems	O
including	O
DM,	O
HTN	O
and	O
ESRF	O
on	O
dialysis.	O
In	O
addition	O
he	O
has	O
ischemic	O
cardiomyopathy	O
complicated	O
by	O
severe	O
mitral	O
regurgitation.	O
He	O
has	O
been	O
reluctant	O
to	O
consider	O
surgery	O
in	O
the	O
past	O
but	O
in	O
the	O
face	O
of	O
worsening	O
symptoms	O
has	O
now	O
agreed	O
to	O
proceed.	O
Plan	O
1.	O
Admit	O
for	O
right	O
and	O
left	O
heart	O
cath	O
with	O
cors.	O
2.	O
Pt	O
schedulled	O
for	O
dialysis	O
on	O
Friday	O
please	O
notify	O
Dr	O
Mcdaniel	PERSON
of	O
his	O
arrival.	O
3.	O
CABG/MVR/MAZE	O
schedulled	O
for	O
Thursday	O
****************************************************************************************************	O
Record	O
date:	O
2067-08-02	O
60	O
year	O
old	O
gentleman	O
presents	O
for	O
follow-up	O
of	O
right	O
lower	O
extremity	O
foot	O
ulcer.	O
It	O
would	O
appear	O
that	O
two	O
weeks	O
prior,	O
the	O
patient	O
sustained	O
a	O
burn	O
to	O
the	O
right	O
lower	O
extremity	O
while	O
walking	O
on	O
a	O
hot	O
sandy	O
beach.	O
He	O
has	O
been	O
followed	O
regularly	O
by	O
a	O
podiatrist	O
Dr.	O
Sanchez,	PERSON
who	O
has	O
attempted	O
debridement	O
of	O
lesion.	O
He	O
also	O
asked	O
that	O
the	O
patient	O
receive	O
vancomycin	O
at	O
the	O
time	O
of	O
his	O
dialysis.	O
In	O
the	O
setting	O
of	O
four	O
compliance	O
and	O
poor	O
wound	O
healing,	O
the	O
patient	O
was	O
admitted	O
for	O
48	O
hours	O
to	O
the	O
Northport	ORGANIZATION
Veterans	ORGANIZATION
Hospital.	ORGANIZATION
Records	ORGANIZATION
are	O
not	O
immediately	O
available	O
but	O
it	O
would	O
appear	O
that	O
he	O
received	O
intravenous	O
antibiotics	O
at	O
that	O
time.	O
He	O
was	O
discharged	O
without	O
clear	O
follow-up.	O
The	O
patient	O
presents	O
to	O
my	O
office	O
with	O
his	O
foot	O
unwrapped	O
and	O
undressed.	O
He	O
has	O
been	O
ambulating	O
on	O
the	O
wound.	O
He	O
reports	O
that	O
it	O
appears	O
less	O
red	O
and	O
has	O
less	O
drainage	O
than	O
at	O
his	O
initial	O
presentation.	O
He	O
reports	O
no	O
pain.	O
Having	O
spoken	O
with	O
his	O
podiatrist,	O
it	O
would	O
appear	O
that	O
the	O
patient	O
has	O
been	O
noncompliant	O
with	O
leg	O
elevation	O
and	O
dressing	O
overall.	O
Review	O
of	O
systems	O
is	O
otherwise	O
negative.	O
Past	O
Medical	O
History:	O
Neuropathy	O
Nephropathy	O
:	O
cr.	O
2.6.	O
3.3	O
gm	O
proteinuria.	O
Currently	O
on	O
hemodialysis	O
Hyperlipidemia	O
Hypertension	O
Obesity	O
Diabetes	O
mellitus	O
:	O
type	O
2	O
Noncompliance	O
:	O
poor	O
compliance	O
Basal	O
cell	O
carcinoma	O
:	O
carcinoma	O
of	O
skin	O
left	O
cheek	O
Lower	O
extremity	O
ulcer	O
:	O
DIABETIC	O
FOOT	O
ULCER.	O
L	O
Foot	O
Abscess	O
:	O
Rt	O
shin	O
2062Back:	O
11/64.	O
S/P	O
I&D	O
Mitral	O
valve	O
insufficiency	O
:	O
severe,	O
with	O
cardiomyopathy	O
and	O
EF	O
36%	O
Colonic	O
polyps	O
:	O
tubulovillious	O
adenoma	O
11/65.	O
Needs	O
repeat	O
scope	O
11/66	O
FH	O
Uncoded	O
Information:	O
Comments:	O
Noncontributory	O
Allergies	O
NKA	O
Medications	O
Aspirin	O
BUFFERED	O
325MG	O
TABLET	O
take	O
1	O
Tablet(s)	O
PO	O
QD	O
Nephrocaps	O
1	O
CAPSULE	O
PO	O
QD	O
x	O
90	O
days	O
Ca	O
ACETATE	O
(CALCIUM	O
ACETATE	O
(	O
1	O
TABLET=667	O
MG))	O
1334	O
MG	O
(667MG	O
TABLET	O
take	O
2)	O
PO	O
TID	O
x	O
30	O
days,	O
take	O
with	O
meals;	O
if	O
snack	O
inbetween	O
meals	O
take	O
one	O
extra	O
pill	O
Lipitor	O
(ATORVASTATIN)	O
80	O
MG	O
(80MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Zetia	O
(EZETIMIBE)	O
10	O
MG	O
(10MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Toprol	O
XL	O
(METOPROLOL	O
SUCCINATE	O
EXTENDED	O
RELEASE)	O
100	O
MG	O
(100MG	O
TAB.SR	O
24H	O
take	O
1)	O
PO	O
QD	O
Lisinopril	O
40	O
MG	O
(40MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
,	O
Evening	O
Qpm	O
Insulin	O
70/30	O
(HUMAN)	O
Variable	O
(70-30	O
U/ML	O
VIAL	O
)	O
SC	O
,	O
45	O
am,	O
Pm	O
Physical	O
exam:	O
In	O
no	O
acute	O
distress.	O
Blood	O
pressure	O
is	O
100/58.	O
Weight	O
is	O
236.	O
Pulse	O
is	O
70	O
and	O
regular.HEENT:	O
Pupils	O
equal	O
round	O
reactive	O
to	O
light.	O
Extraocular	O
motions	O
intact.	O
Oropharynx	O
moist	O
without	O
erythema	O
or	O
exudate.	O
Neck:	O
Supple	O
without	O
JVD,	O
lymphadenopathy,	O
or	O
thyromegaly.	O
Chest:	O
Clear	O
to	O
auscultation	O
bilaterally	O
without	O
wheezes	O
or	O
rhonchi	O
or	O
rhales.	O
Cardiac:	O
Regular	O
rate	O
and	O
rhythm,	O
normal	O
S1	O
and	O
S2	O
without	O
murmurs	O
rubs	O
or	O
gallops.	O
Extremities:	O
Right	O
lower	O
extremity	O
with	O
a	O
1.6	O
cm	O
x	O
1.6	O
cm	O
black	O
eschar	O
to	O
be	O
great	O
toe.	O
Lesion	O
is	O
soft.	O
It	O
is	O
unclear	O
if	O
there	O
is	O
significant	O
tracking	O
beneath	O
the	O
surface.	O
A	O
3	O
cm	O
x	O
2	O
cm	O
flat	O
based	O
ulcer	O
with	O
surrounding	O
callus	O
formation	O
is	O
noted	O
to	O
be	O
ball	O
of	O
the	O
right	O
foot	O
with	O
some	O
granulation	O
and	O
no	O
drainage.	O
A	O
collar	O
is	O
noted.	O
No	O
surrounding	O
erythema	O
is	O
appreciated.	O
Dorsalis	O
pedis	O
and	O
posterior	O
tibial	O
pulses	O
are	O
palpable	O
but	O
clearly	O
diminished.	O
Assessment	O
and	O
plan:	O
This	O
is	O
a	O
60	O
year	O
old	O
gentleman	O
with	O
multiple	O
medical	O
issues	O
including	O
end-stage	O
renal	O
disease	O
my	O
for	O
regurgitation,	O
congestive	O
heart	O
failure,	O
severe	O
peripheral	O
neuropathy,	O
diabetes.	O
Present	O
in	O
with	O
two	O
large	O
altars	O
to	O
the	O
right	O
lower	O
extremity.	O
He	O
is	O
status	O
post	O
hospitalization	O
and	O
some	O
debridement	O
and	O
appears	O
to	O
have	O
had	O
little	O
overall	O
improvement	O
in	O
clinical	O
status.	O
I	O
will	O
proceed	O
with	O
an	O
aggressive	O
outpatient	O
treatment	O
plan	O
however	O
I	O
suspect	O
that	O
he	O
will	O
need	O
inpatient	O
admission	O
once	O
again	O
if	O
significant	O
improvement	O
is	O
not	O
rapidly	O
obtained.	O
A	O
referral	O
has	O
been	O
made	O
to	O
the	O
Island	LOCATION
Medical	LOCATION
Center	LOCATION
wound	O
clinic	O
for	O
this	O
Tuesday.	O
Visiting	O
nurse	O
has	O
been	O
arranged	O
for	O
his	O
home	O
for	O
wound	O
care	O
in	O
the	O
interim.	O
He	O
is	O
advised	O
to	O
avoid	O
all	O
weight-bearing	O
and	O
to	O
keep	O
his	O
foot	O
dressed	O
at	O
all	O
times.	O
I	O
have	O
explained	O
to	O
the	O
patient	O
that	O
he	O
is	O
at	O
risk	O
of	O
amputation	O
of	O
at	O
least	O
his	O
right	O
great	O
toe	O
should	O
his	O
healing	O
not	O
improve.	O
With	O
regards	O
to	O
his	O
other	O
medical	O
issues,	O
he	O
continues	O
on	O
dialysis.	O
I	O
have	O
asked	O
for	O
a	O
hemoglobin	O
A1c	O
and	O
fasting	O
cholesterol	O
to	O
be	O
drawn	O
at	O
the	O
time	O
of	O
his	O
dialysis	O
tomorrow.	O
I'll	O
see	O
the	O
patient	O
on	O
Tuesday.	O
His	O
blood	O
pressure	O
is	O
appropriately	O
controlled	O
and	O
he	O
is	O
no	O
stigmata	O
of	O
congestive	O
heart	O
failure	O
At	O
today's	O
visit,	O
the	O
patient's	O
wound	O
was	O
dressed	O
and	O
cleaned	O
the	O
our	O
nursing	O
staff	O
****************************************************************************************************	O
Record	O
date:	O
2069-04-16	O
Patient	O
Name:	O
OGRADY,ULYSSES	O
[	O
0937884(IMC)	O
]	O
Date	O
of	O
Visit:	O
04/16/2069	O
Renal	O
Staff	O
Patient	O
seen	O
and	O
examined	O
on	O
dialysis.	O
Agree	O
with	O
details	O
per	O
Fellow	O
Note.	O
62	O
year	O
old	O
man	O
referred	O
to	O
ED	O
after	O
missing	O
dialysis	O
yesterday	O
and	O
noted	O
to	O
be	O
mentally	O
confused.	O
On	O
presentation	O
he	O
was	O
hyperkalemic	O
to	O
7.4.	O
He	O
has	O
been	O
evaluated	O
in	O
ED	O
setting	O
with	O
negative	O
head	O
CT	O
and	O
LP.	ORGANIZATION
Family	ORGANIZATION
reports	O
h/o	O
poor	O
compliance	O
and	O
self	O
care	O
deficit.	O
PMH/PSH:ESRD,	O
presumed	O
from	O
HTN/DM,	O
TTS,	O
LUE	O
AVF;	O
initiated	O
dialysis	O
in	O
2067;	O
has	O
left	O
arm	O
AVF;	O
HD	O
at	O
Eureka	LOCATION
Springs	LOCATION
Hospital	LOCATION
-	O
Dr	O
Allan	PERSON
Jester	PERSON
is	O
attending	O
NephrologistHyperlipidemia	O
Hypertension	O
Obesity	O
Diabetes	O
mellitus	O
:	O
type	O
2,	O
with	O
tripothyNoncompliance	O
Basal	O
cell	O
carcinoma	O
:	O
carcinoma	O
of	O
skin	O
left	O
cheekLower	O
extremity	O
ulcer	O
:	O
DIABETIC	O
FOOT	O
ULCER.	O
L	O
FootAbscess	O
:	O
Rt	O
shin	O
2062Back:	O
11/64.	O
S/P	O
I&amp;DMitral	O
valve	O
insufficiency	O
:	O
severe,	O
with	O
cardiomyopathy	O
and	O
EF	O
36%Colonic	O
polyps	O
:	O
tubulovillious	O
adenoma	O
11/65.	O
Needs	O
repeat	O
scope	O
11/66	O
Medications	O
Insulin	O
variable	O
dosing	O
Reportedly	O
not	O
taking	O
other	O
meds	O
SOCIAL	O
Lives	O
in	O
De	PERSON
Soto	PERSON
with	O
Wife	O
and	O
Daughter	O
ROS	O
As	O
detailed.	O
He	O
also	O
has	O
been	O
c/o	O
dizziness	O
and	O
weakness	O
after	O
dialysis,	O
which	O
was	O
the	O
main	O
reason	O
he	O
refused	O
to	O
go	O
for	O
HD	O
yesterday.	O
No	O
additional	O
hx	O
is	O
available,	O
no	O
known	O
sick	O
contact	O
per	O
wife.	O
When	O
he	O
was	O
in	O
ED,	O
he	O
was	O
noted	O
to	O
have	O
LGF	O
100.4,	O
hypertensive	O
and	O
tachycardic.	O
He	O
is	O
waiting	O
for	O
head	O
CT	O
and	O
possible	O
LP.	O
EXAM	O
T	O
100.4,	O
P	O
110,	O
BP	O
150/80,	O
R	O
26,	O
SO2	O
94%(4L)	O
NAD,	O
limited	O
cooperation	O
with	O
exam,	O
A+O	O
x2	O
CV:	O
tachy/reg,	O
no	O
rubs	O
CHEST:	O
CTA	ORGANIZATION
ant	O
ABD:	O
soft,	O
NT/ND/NABS	O
EXT:	O
1+	O
edema	O
Neuro:	O
nonfocal-limited	O
exam	O
DATAEKG	O
is	O
not	O
available	O
for	O
review,	O
but	O
no	O
acute	O
ischemic	O
changes	O
and	O
no	O
widened	O
QRS/peaked	O
T	O
wave	O
per	O
ED.	O
PCXR:	O
poor	O
penetration,	O
but	O
no	O
acute	O
process	O
Head	O
CT	O
IMPRESSION:	O
1.	O
No	O
hemorrhage,	O
mass	O
lesion,	O
or	O
evidence	O
of	O
acute	O
territorial	O
infarction.	O
2.	O
Extensive	O
nonspecific	O
periventricular,	O
deep,	O
and	O
subcortical	O
white	O
matter	O
changes	O
are	O
likely	O
sequela	O
from	O
chronic	O
microangiopathic	O
disease.	O
3.	O
Old	O
lacunar	O
infarctions	O
in	O
the	O
left	O
caudate	O
head	O
and	O
right	O
caudate-anterior	O
corona	O
radiata.	O
Evaluation	O
for	O
acute	O
ischemia	O
is	O
limited	O
in	O
the	O
areas	O
of	O
extensive	O
white	O
matter	O
disease.	O
Brain	O
MRI	O
with	O
diffusion-weighted	O
imaging	O
is	O
more	O
sensitive	O
in	O
assessing	O
for	O
acute	O
ischemia	O
and	O
should	O
be	O
obtained	O
as	O
clinically	O
indicated.	O
UA-SED-RBC	O
20-50	O
(0-2)	O
/hpfUA-SED-WBC	O
5-10	O
(0-2)	O
/hpfUA-SED-Bacteria	O
Many	O
(NEG)	O
/hpf	O
WBC	O
20.7	O
H	O
(4.5-11.0)	O
th/cmmHCT	O
33.6	O
L	O
(41.0-53.0)	O
%HGB	O
11.3	O
L	O
(13.5-17.5)	O
gm/dlRBC	O
3.77	O
L	O
(4.50-5.90)	O
mil/cmmPLT	O
239	O
(150-400)	O
th/cumm	O
Plasma	O
Sodium	O
133	O
L	O
(135-145)	O
mmol/LPlasma	O
Potassium	O
7.4	O
H	O
(3.4-4.8)	O
mmol/L	O
Plasma	O
Chloride	O
94	O
L	O
(100-108)	O
mmol/LPlasma	O
Carbon	O
Dioxide	O
18.1	O
L	O
(23.0-31.9)	O
mmol/LPlasma	O
Anion	O
GAP	O
21	O
H	O
(3-15)	O
mmol/LCalcium	O
9.6	O
(8.5-10.5)	O
mg/dlPhosphorus	O
7.3	O
H	O
(2.6-4.5)	O
mg/dlMagnesium	O
2.2	O
H	O
(1.4-2.0)	O
meq/LPlasma	O
Urea	O
Nitrogen	O
68	O
H	O
(8-25)	O
mg/dlPlasma	O
Creatinine	O
9.18	O
H	O
(0.60-1.50)	O
mg/dlAlbumin	O
4.6	O
(3.3-5.0)	O
g/dlDirect	O
Bilirubin	O
0.5	O
H	O
(0-0.4)	O
mg/dlTotal	O
Bilirubin	O
1.1	O
H	O
(0.0-1.0)	O
mg/dlA+P	O
1.	O
ESRD,	O
with	O
hyperkalemia	O
2.	O
AMS,	O
found	O
down	O
s/p	O
LP	O
with	O
results	O
pending	O
and	O
extensive	O
CNS	O
white	O
matter	O
changes	O
3.	O
Medical	O
noncompliance	O
4.	O
Leukocytosis	PERSON
with	O
infected	O
leg	O
ulcer	O
and	O
?	O
cellulitis	O
5.	O
Pyuria	O
-	O
Dialysis	O
3	O
hrs	O
1K,	O
2.5Ca	O
-	O
UF	ORGANIZATION
2L	O
-	O
AVF	ORGANIZATION
access	O
-	O
Hold	O
Epogen	O
today	O
-	O
F/U	O
LP	O
results	O
-	O
Blood	O
Cultures	O
on	O
dialysis;	O
wound	O
culture,	O
urine	O
culture	O
-	O
Vancomycin	O
1g	O
post	O
dialysis;	O
Levofloxacin	O
500mg	O
qod	O
-	O
cycle	O
cardiac	O
enzymes	O
-	O
Review	O
outpatient	O
medication	O
regimen	O
____________________________________	O
Kenneth	PERSON
DUSTIN	PERSON
Mcdaniel,	PERSON
M.D.	O
****************************************************************************************************	O
Record	O
date:	O
2069-10-07	O
IMC	O
Odem	O
IM	O
Attending	O
Medicine	O
Consult	O
NotePt:	O
Ulysses	O
OgradyIMC	O
#	O
0937884Date:	O
10/07/69	O
Time:	O
12:15pmPCP:	O
Paul	O
Jaramillo,	O
MD	O
Pager	O
#	O
52666	O
HPI:	O
Asked	O
to	O
see	O
patient	O
by	O
surgical	O
team	O
in	O
consultation	O
for	O
is	O
multiple	O
medical	O
issues.	O
I	O
have	O
served	O
as	O
his	O
PCP	O
for	O
eight	O
years.	O
Pt	O
seen	O
and	O
examined.	O
Case	O
discussed	O
in	O
detail	O
.	O
This	O
is	O
a	O
62	O
y/o	O
male	O
with	O
multiple	O
medical	O
issues	O
last	O
admitted	O
in	O
September	O
for	O
increased	O
falls,	O
found	O
to	O
have	O
a	O
small	O
stroke	O
in	O
addition	O
to	O
his	O
likely	O
vascular	O
dementia.	O
Readmitted	O
last	O
week	O
in	O
the	O
setting	O
of	O
Purulent	O
L	O
foot	O
ulcer/	O
S/P	O
debridement	O
and	O
later	O
TMA.	O
He	O
has	O
a	O
long	O
history	O
well	O
documented	O
in	O
the	O
electronic	O
medical	O
record	O
which	O
is	O
notable	O
for	O
poor	O
compliance	O
with	O
medication	O
and	O
follow-up.	O
At	O
baseline,	O
patient	O
has	O
declined	O
further	O
cardiology	O
workup	O
for	O
his	O
severe	O
mitral	O
regurgitation	O
and	O
cardiomyopathy,	O
has	O
declined	O
treatment	O
for	O
his	O
hyperlipidemia	O
and	O
takes	O
his	O
blood	O
pressure	O
medications	O
with	O
less	O
than	O
25%	O
compliance.	O
At	O
the	O
time	O
of	O
his	O
last	O
admission,	O
he	O
was	O
actually	O
admitted	O
to	O
a	O
nursing	O
home	O
with	O
plans	O
for	O
long	O
term	O
placement.	O
The	O
patient's	O
wound	O
has	O
currently	O
not	O
been	O
closed.	O
He	O
has	O
a	O
vac	O
dressing	O
in	O
place.	O
He	O
has	O
been	O
attending	O
dialysis	O
per	O
schedule	O
of	O
nephrology.	O
Current	O
hospital	O
course	O
has	O
been	O
complicated	O
by	O
increased	O
agitation	O
and	O
last	O
evening,	O
self	O
limited	O
substernal	O
chest	O
pain	O
and	O
dyspnea.	O
This	O
resolved	O
spontaneously.	O
He	O
has	O
also	O
been	O
intermittently	O
refusing	O
his	O
medications	O
and	O
has	O
been	O
attempting	O
to	O
ambulate.	O
This	O
AM.	O
the	O
patient	O
actually	O
denies	O
ever	O
experiencing	O
chest	O
pain.	O
He	O
also	O
only	O
recalls	O
his	O
foot	O
pain	O
with	O
repeated	O
prompting.	O
He	O
denies	O
any	O
pain,	O
dyspnea	O
ROS	O
otherwise	O
negative.,	O
Past	O
Medical	O
History:	O
Neuropathy	O
Nephropathy	O
:	O
cr.	O
2.6.	O
3.3	O
gm	O
proteinuria.	O
Currently	O
on	O
hemodialysisHyperlipidemia	O
Hypertension	O
Obesity	O
Diabetes	O
mellitus	O
:	O
type	O
2Noncompliance	O
:	O
poor	O
complianceBasal	O
cell	O
carcinoma	O
:	O
carcinoma	O
of	O
skin	O
left	O
cheekLower	O
extremity	O
ulcer	O
:	O
DIABETIC	O
FOOT	O
ULCER.	O
L	O
FootAbscess	O
:	O
Rt	O
shin	O
2062	O
Back:	O
11/64.	O
S/P	O
I&amp;DMitral	O
valve	O
insufficiency	O
:	O
severe,	O
with	O
cardiomyopathy	O
and	O
EF	O
36%Colonic	O
polyps	O
:	O
tubulovillious	O
adenoma	O
11/65.	O
CAD:	O
cath	O
in	O
2066	O
showed	O
occluded	O
mid	O
LAD	O
with	O
collaterals.	O
Stress	O
test	O
showed	O
mid-distal	O
anterior	O
wall	O
and	O
apical	O
ischemia.	O
-	O
Cardiomyopathy	O
with	O
EF=36%,	O
severe	O
mitral	O
regurg	O
not	O
on	O
any	O
SBE	ORGANIZATION
ppx	O
Medications:	O
As	O
prescribed:Olanzapine	O
5mg	O
BID	O
SQ	O
Hepatin	O
Toprol	O
XL,	O
50mg	O
Lisinopril	O
40mg	O
po	O
qd	O
Vancomycin:	O
dosed	O
at	O
dialysis	O
RISS	ORGANIZATION
Oxycodone	ORGANIZATION
prn	ORGANIZATION
Simvastatin	ORGANIZATION
Levofloxacin	ORGANIZATION
Fluoxetine	ORGANIZATION
Nephocaps	ORGANIZATION
Calcium	ORGANIZATION
acetate.	O
Allergies:	O
NKDA	O
Social	O
History/FH	O
per	O
Surgical	O
notes.	O
Pt	O
is	O
a	O
nonsmoker.	O
No	O
ETOH	O
or	O
Drug	O
use	O
hx.	O
Formerly	O
lived	O
with	O
daughter.	O
Emigrated	O
to	O
tajikistan	O
many	O
years	O
prior	O
but	O
still	O
primarily	O
speaks	O
Chad.	O
Examination:	O
Modestly	O
confused	O
but	O
not	O
agitated.	O
Does	O
recognize	O
PCP.	O
Does	O
not	O
know	O
date	O
or	O
time.	O
Does	O
not	O
clearly	O
know	O
why	O
he	O
is	O
hospitalized.	O
VITALS:	O
99.0	O
hr	O
80-101	O
R	O
BP	O
150/76	O
.GEN&#8217;L	O
NAD,	O
AO	O
X	O
2	O
(&#8220;Superior	O
Memorial	O
Hospital)HEENT	O
MMM,	O
no	O
scleral	O
icterus,	O
no	O
pale	O
conjunctiva.	O
No	O
roth	O
spots.	O
NECK	O
supple,	O
no	O
thyromegaly,	O
no	O
LAD,	O
no	O
carotid	O
bruits.	O
+	O
10cm	O
JVOP	O
Heart:	O
RRR,	O
no	O
m/r/gLUNG:	O
Bibasilar	O
crackles.	O
No	O
wheezeABD:	O
Soft,	O
non-tender,	O
obese,	O
NABS.	O
No	O
HSM.	O
No	O
masses,	O
no	O
rebound.	O
EXT:	O
RLE	O
in	O
Vac	O
dressing.	O
Wound	O
open	O
without	O
surrounding	O
erythema.NEURO:	O
AO	O
x	O
3;	O
alert;	O
CN	O
II-XII	O
grossly	O
intact;	O
Motor:	O
5/5	O
B	O
UE/LE;	O
sensation	O
dinished	O
to	O
light	O
tough,	O
proprioception.	O
Full	O
neurologic	O
eval	O
per	O
neurology	O
assessment	O
Data:	O
Date/Time	O
NA	O
K	O
CL	O
CO2	O
10/07/2069	O
136	O
4.4	O
100	O
24.5	O
10/07/2069	O
138	O
4.7	O
100	O
26.4	O
10/06/2069	O
136	O
4.7	O
99	O
(L)	O
26.6	O
10/06/2069	O
136	O
4.2	O
98	O
(L)	O
Date/Time	O
BUN	O
CRE	O
EGFR	O
GLU	O
10/07/2069	O
35	O
(H)	O
6.64	O
(H)	O
9	O
[1]	O
124	O
(H)	O
10/07/2069	O
35	O
(H)	O
6.38	O
(H)	O
9	O
[2]	O
106	O
10/06/2069	O
34	O
(H)	O
6.33	O
(H)	O
10	O
[3]	O
141	O
(H)	O
Date/Time	O
ANION	O
10/07/2069	O
12	O
10/07/2069	O
12	O
10/06/2069	O
10	O
Date/Time	O
CA	O
PHOS	O
MG	O
ALB	O
10/07/2069	O
8.5	O
5.0	O
(H)	O
2.0	O
3.0	O
(L)	O
10/07/2069	O
8.5	O
4.8	O
(H)	O
2.0	O
10/06/2069	O
8.7	O
10/06/2069	O
8.4	O
(L)	O
4.0	O
2.0	O
3.4	O
Date/Time	O
25	O
OH-VITD	O
10/02/2069	O
23	O
(L)[13]	O
Date/Time	O
CK	O
CK-MB	O
CKMBRI	O
TROP-T	O
10/07/2069	O
25	O
(L)	O
2.7	O
SEE	O
DETAIL[14]	O
0.07	O
10/06/2069	O
32	O
(L)	O
2.8	O
SEE	O
DETAIL[15]	O
0.08	O
Date/Time	O
FE	O
TIBC	O
FER	O
10/02/2069	O
23	O
(L)	O
156	O
(L)	O
501	O
(H)	O
Date/Time	O
PTH	O
10/02/2069	O
66	O
(H)	O
Date/Time	O
WBC	O
RBC	O
HGB	O
HCT	O
10/07/2069	O
13.9	O
(H)	O
3.42	O
(L)	O
9.7	O
(L)	O
29.4	O
(L)	O
10/07/2069	O
16.4	O
(H)	O
3.34	O
(L)	O
9.6	O
(L)	O
29.2	O
(L)	O
10/06/2069	O
14.6	O
(H)	O
3.58	O
(L)	O
10.0	O
(L)	O
31.2	O
(L)	O
Date/Time	O
MCV	O
MCH	O
MCHC	O
PLT	O
10/07/2069	O
86	O
28.2	O
32.9	O
372	O
10/07/2069	O
88	O
28.8	O
32.9	O
372	O
10/06/2069	O
87	O
28.0	O
32.1	O
Date/Time	O
RETIC	O
10/02/2069	O
1.6	O
Date/Time	O
PT	O
PT-INR	O
PTT	O
10/07/2069	O
17.6	O
(H)	O
1.6	O
30.6	O
10/01/2069	O
17.3	O
(H)	O
1.6	O
29.2	O
[21]	O
Date/Time	O
HBV-SAB	O
HBV-SAG	O
HCVAB	O
10/02/2069	O
EQUIVOCAL	O
Non-Reactive	O
Non-Reactive	O
Date/Time	O
VANC	O
TR-VANC	O
10/07/2069	O
13.2	O
[22]	O
10/04/2069	O
38.6	O
[23]	O
Specimen	O
Type:	O
BLOOD	O
CULTURE	O
Few	O
POLYS,	O
Few	O
MONONUCLEAR	O
CELLS,	O
Abundant	O
GRAM	O
NEGATIVE	O
RODS,	O
Rare	O
LARGE	O
GRAM	O
POSITIVE	O
RODS	O
Wound	O
Culture	O
-	O
Final	O
Reported:	O
06-Oct-69	O
14:55	O
Specimen	O
received	O
from	O
OR	O
Abundant	O
KLEBSIELLA	O
PNEUMONIAE	O
RAPID	O
MIC	O
METHOD	O
Antibiotic	O
Interpretation	O
----------------------------------------------	O
Amikacin	O
Susceptible	O
Ampicillin	O
Resistant	O
Aztreonam	O
Susceptible	O
Cefazolin	O
Susceptible	O
Cefepime	O
Susceptible	O
Ceftriaxone	O
Susceptible	O
Ciprofloxacin	O
Susceptible	O
Gentamicin	O
Susceptible	O
Levofloxacin	O
Susceptible	O
Piperacillin	O
Resistant	O
Trimethoprim/Sulfamethoxazole	O
Susceptible	O
Abundant	O
PSEUDOMONAS	O
AERUGINOSA	O
DISK	O
METHOD	O
Antibiotic	O
Interpretation	O
----------------------------------------	O
Aztreonam	O
Susceptible	O
Cefepime	O
Susceptible	O
Gentamicin	O
Susceptible	O
Levofloxacin	O
Susceptible	O
Piperacillin/Tazobactam	O
Susceptible	O
Ticarcillin	O
Susceptible	O
Tobramycin	O
Susceptible	O
Abundant	O
MORGANELLA	O
MORGANII	O
SUBSPECIES	O
MORGANII	O
RAPID	O
MIC	O
METHOD	O
Antibiotic	O
Interpretation	O
----------------------------------------------	O
Amikacin	O
Susceptible	O
Ampicillin	O
Resistant	O
Aztreonam	O
Susceptible	O
Cefazolin	O
Resistant	O
Cefepime	O
Susceptible	O
Ceftriaxone	O
Susceptible	O
Ciprofloxacin	O
Susceptible	O
Gentamicin	O
Susceptible	O
Levofloxacin	O
Susceptible	O
Piperacillin	O
Susceptible	O
Trimethoprim/Sulfamethoxazole	O
Susceptible	O
Anaerobic	O
Culture	O
-	O
Preliminary	O
Reported:	O
06-Oct-69	O
11:48	O
Specimen	O
received	O
from	O
OR	O
NO	O
ANAEROBES	O
ISOLATED	O
SO	O
FAR	O
EKG:	O
NSR	ORGANIZATION
@	O
90,	O
incomplete	O
RBBB,	O
L	O
anterior	O
fascicular	O
block;	O
QTC	O
495	O
ms.	O
Assessment/Plan	O
In	O
summary,62	O
y/o	O
male	O
with	O
multiple	O
medical	O
issues	O
as	O
outlined	O
in	O
this	O
and	O
many	O
other	O
notes.	O
Presenting	O
with	O
necrotic	O
foot	O
infection,	O
polymicrobial.	O
S/P	O
Rt	O
TMA.	O
Mildly	O
delerious	O
and	O
certainly	O
off	O
from	O
a	O
baseline	O
that	O
was	O
tenuous	O
to	O
start.	O
By	O
issue:	O
1)	O
CAD:	O
The	O
patient	O
does	O
have	O
CAD,	O
MR	O
and	O
cardiomyopathy	O
that	O
are	O
really	O
not	O
amenable	O
to	O
intervention,	O
given	O
patient	O
noncompliance.	O
The	O
best	O
goal	O
at	O
present	O
is	O
going	O
to	O
be	O
continued	O
medical	O
management.	O
If	O
BP	O
allows,	O
could	O
increase	O
toprol	O
XL,	O
although	O
I	O
suspect	O
that	O
this	O
will	O
not	O
be	O
an	O
option.	O
In	O
addition,	O
would	O
aim	O
to	O
keep	O
even	O
at	O
dialysis.	O
Certainly,	O
cycle	O
CKs	O
and	O
troponin	O
and	O
follow	O
sx.	O
I	O
will	O
defer	O
to	O
Dr.	O
Shah,	O
but	O
I	O
don't	O
see	O
clear	O
benefit	O
for	O
cardiac	O
cath,	O
especially	O
as	O
we	O
are	O
not	O
going	O
to	O
proceed	O
to	O
valve	O
repair	O
at	O
this	O
juncture.	O
If	O
he	O
were	O
to	O
become	O
more	O
compliant,	O
this	O
might	O
be	O
another	O
consideration	O
and	O
as	O
he	O
is	O
now	O
a	O
CT	O
resident,	O
perhaps	O
this	O
will	O
be	O
a	O
future	O
consideration.	O
2)	O
Delirium:	O
He	O
does	O
have	O
a	O
very	O
tenuous	O
baseline	O
and	O
has	O
significant	O
vascular	O
dementia	O
to	O
start.	O
He	O
is	O
off	O
of	O
this	O
baseline,	O
likely	O
reflecting	O
sundowning,	O
medications	O
and	O
underlying	O
metabolic	O
processes.	O
Please	O
d/c	O
medications	O
that	O
might	O
be	O
contributing	O
to	O
sx.	O
Fluoxetine	O
can	O
be	O
held	O
for	O
now	O
as	O
it	O
was	O
only	O
started	O
3	O
weeks	O
prior.	O
Might	O
hold	O
ranitidine	O
as	O
well.	O
Haldol	O
is	O
d/cd	O
in	O
the	O
setting	O
of	O
his	O
prolonged	O
QTc.	O
Currently	O
on	O
olanzapine.	O
Would	O
continue	O
to	O
follow	O
QTc	O
closely	O
and	O
would	O
d/c	O
this	O
med	O
as	O
well	O
is	O
still	O
prolonged.	O
Please	O
send	O
urine	O
culture	O
and	O
surveillance	O
blood	O
cultures.	O
3)	O
Infection:	O
Appreciate	O
excellent	O
surgical	O
management	O
of	O
patient's	O
wound.	O
Levofloxacin	O
has	O
been	O
d/cd	O
2nd	O
to	O
concerns	O
for	O
altered	O
mental	O
status.	O
All	O
bacteria	O
are	O
sensitive	O
to	O
aztreonam	O
so	O
perhaps	O
this	O
would	O
be	O
a	O
reasonable	O
alternative.	O
Would	O
seek	O
ID	O
input	O
to	O
the	O
end.	O
Wound	O
care	O
per	O
vascular	O
with	O
plans	O
for	O
D/C	O
when	O
stable	O
and	O
return	O
for	O
closure	O
in	O
the	O
future.	O
4)	O
ESRD:	O
Patient	O
HAD	O
dialysis	O
today	O
.	O
Recs	ORGANIZATION
per	O
nephrology	O
3)	O
DM:	O
HgA1c	O
was	O
at	O
target.	O
Please	O
cover	O
as	O
needed	O
with	O
RISS	ORGANIZATION
for	O
now.	O
5)	O
Competency:	O
certainly	O
not	O
able	O
to	O
make	O
informed	O
decisions	O
at	O
this	O
time.	O
Please	O
confirm	O
that	O
his	O
daughter	O
is	O
his	O
HCP.	O
Would	O
try	O
to	O
use	O
a	O
Chadian	O
interpreter	O
and	O
not	O
daughters	O
if	O
at	O
all	O
possible.	O
Thanks,	O
____________________________________Paul	O
Anthony	O
Jaramillo,	O
M.D.Pager	O
#	O
52666.Sheila	O
Strong	O
to	O
cover	O
for	O
Thursday	O
and	O
Friday	O
****************************************************************************************************	O
