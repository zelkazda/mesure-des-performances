Record	O
date:	O
2067-12-09	O
Patient	O
Name:	O
LAGNSTON,	O
SHERMAN	O
I.;	O
MRN:	O
7223692	O
Dictated	O
at:	O
12/09/67	O
by	O
GARY	PERSON
J.	PERSON
HUGHES,	PERSON
MD	O
FOLLOW	O
UP	O
NOTE	O
DIAGNOSIS:	O
Renal	O
cell	O
carcinoma,	O
presumably	O
metastatic	O
to	O
T1.	O
Interval	O
since	O
conformal	O
radiation	O
therapy	O
to	O
vertebra	O
T1	O
to	O
a	O
dose	O
of	O
50.4	O
gray:	O
Three	O
months.	O
TUMOR	O
STATUS:	O
Not	O
available.	O
NORMAL	O
TISSUE	O
STATUS:	O
No	O
difficulty.	O
INTERVAL	O
HISTORY:	O
Mr.	O
Lagnston	PERSON
is	O
generally	O
doing	O
well	O
with	O
no	O
deterioration	O
and	O
some	O
improvement	O
in	O
his	O
symptomatology.	O
He	O
now	O
has	O
occasionally	O
tingling	O
in	O
his	O
feet	O
and	O
hands,	O
which	O
is	O
possibly	O
similar	O
to	O
before	O
or	O
less	O
so.	O
He	O
also	O
has	O
some	O
posterior	O
neck	O
tingling,	O
probably	O
related	O
to	O
some	O
position	O
or	O
activity	O
and	O
with	O
lifting	O
weight.	O
He	O
has	O
no	O
weakness	O
in	O
any	O
extremities.	O
His	O
nocturia	O
has	O
decreased	O
from	O
three	O
to	O
one.	O
His	O
hands	O
feel	O
a	O
little	O
"fuzzy."	O
The	O
trembles,	O
which	O
he	O
has	O
had	O
for	O
some	O
time	O
are	O
possibly	O
better	O
or	O
the	O
same,	O
still	O
a	O
little	O
worse	O
on	O
the	O
left	O
hand	O
than	O
the	O
right.	O
PHYSICAL	O
EXAMINATION:	O
On	O
physical	O
examination	O
he	O
looks	O
well.	O
His	O
weight	O
is	O
up	O
to	O
230	O
pounds.	O
He	O
has	O
no	O
percussible	O
bony	O
tenderness	O
throughout.	O
His	O
abdomen	O
is	O
entirely	O
negative,	O
so	O
is	O
his	O
incision.	O
IMPRESSION:	O
Clinically	O
possibly	O
slightly	O
improved,	O
certainly	O
no	O
evidence	O
of	O
progression.	O
PLAN:	O
I	O
will	O
obtain	O
a	O
repeat	O
chest	O
and	O
thoracic	O
spine	O
CT,	O
as	O
well	O
as	O
a	O
bone	O
scan.	O
Assuming	O
that	O
these	O
are	O
not	O
indicative	O
of	O
further	O
progression,	O
I	O
plan	O
to	O
see	O
him	O
in	O
routine	O
follow	O
up	O
in	O
four	O
to	O
six	O
months.	O
Dr.	O
Levine	PERSON
would	O
like	O
some	O
blood	O
tests	O
drawn	O
on	O
him,	O
which	O
I	O
will	O
happy	O
to	O
arrange	O
when	O
I	O
hear	O
exactly	O
what	O
they	O
are.	O
Dr.	O
Sweet	PERSON
is	O
still	O
seeing	O
him	O
frequently	O
and	O
giving	O
him	O
Norvasc	O
and	O
Atenolol	O
for	O
his	O
blood	O
pressure.	O
_______________________________	O
GARY	O
J.	O
HUGHES,	O
MD	O
cc:	O
IZEYAH	O
SWEET,	O
MD	O
HUNTUR	O
IVERSON,	O
MD	O
THOMAS	O
EARL,	O
MD	O
Isaac	PERSON
Ortega,	O
M.D.	O
KYLE	O
LEVINE,	O
MD	O
DD:12/09/67	O
DT:12/10/67	O
GW:1014	O
:04	O
****************************************************************************************************	O
Record	O
date:	O
2070-01-30	O
Medicine	O
Junior	O
Admit	O
Note	O
(Choi	O
00576)	O
Name	O
Sherman	PERSON
Langston	PERSON
MR#	O
722-36-92	O
Date	O
of	O
Admission:	O
January	O
29,	O
2070	O
Cardiologist:	O
Calvin	PERSON
Null	PERSON
PCP:	O
Kyle	O
Eads	O
CC:	O
61	O
yo	O
male	O
former	O
smoker,	O
hyperlipidemia	O
with	O
increasing	O
frequency	O
of	O
chest	O
pain	O
over	O
the	O
past	O
4-5	O
days.	O
HPI:	O
61	O
yo	O
male	O
(CRF:former	O
smoker,	O
HTN,	O
obesity)	O
present	O
with	O
4-5	O
days	O
of	O
increasing	O
chest	O
pain.	O
Patient	O
reports	O
that	O
he	O
began	O
having	O
pain	O
at	O
the	O
R	O
sternal	O
border,	O
no	O
radiation	O
4-5	O
days	O
ago.	O
Patient	O
unsure	O
when	O
pain	O
initially	O
began.	O
Patient	O
describes	O
suprasternal	O
pain,	O
sharp	O
pain	O
that	O
felt	O
like	O
gas	O
pain.	O
Initially	O
lasting	O
for	O
minutes.	O
Pain	O
came	O
and	O
went	O
without	O
any	O
correlation	O
with	O
activity.	O
Episodes	O
increased	O
in	O
frequency	O
and	O
intensity	O
over	O
the	O
days.	O
Exercise	O
did	O
not	O
worsen	O
his	O
pain	O
and	O
was	O
able	O
to	O
climb	O
2-3	O
flights	O
of	O
stairs	O
without	O
exacerbation	O
of	O
symptoms.	O
Pain	O
has	O
been	O
increasing	O
in	O
intensity	O
over	O
the	O
past	O
4-5	O
days.	O
Pain	O
became	O
unbearable	O
and	O
patient	O
finally	O
presented	O
to	O
BRA.	O
Pt	O
did	O
not	O
have	O
any	O
clear	O
STE's,	O
given	O
IV	O
nitro,	O
heparin,	O
and	O
ASA.	O
Evaluated	O
by	O
BRA	O
Cardiologist	O
and	O
transferred	O
to	O
CCH	ORGANIZATION
CCU	ORGANIZATION
for	O
further	O
workup.	O
Pt	O
had	O
similar	O
pain	O
past	O
summer	O
and	O
1	O
year	O
ago	O
while	O
walking	O
up	O
steep	O
hill	O
2061	O
ETT	O
12	O
METs,	O
37000	O
PDP,	O
no	O
perfusion	O
defects	O
PMH:	O
Metastatic	O
renal	O
cell	O
cancer,	O
s/p	O
L	O
nephrectomy,	O
T1	O
radiation	O
right	O
kidney	O
ca,	O
hypertension,	O
no	O
DM,	O
low	O
HDL,	O
s/p	O
L	O
ankle	O
tendon	O
surgery	O
Meds:	O
Norvasc	O
30mg	O
QD	O
Atenolol	O
12.5mg	O
QD	O
All:	O
NKDA	O
SH:	O
lives	O
on	O
his	O
own,	O
used	O
to	O
be	O
avid	O
bicyclist,	O
60	O
pack	O
year	O
history	O
quit	O
18	O
years	O
ago,	O
retired,	O
not	O
married,	O
no	O
drugs,	O
or	O
ETOH	O
FH:	O
mother	O
and	O
father	O
died	O
of	O
MI's	O
at	O
age	O
84	O
and	O
70	O
both	O
their	O
first	O
Physical	O
Exam:	O
Vitals:	O
T	O
99.2	O
BP	O
126/	O
60	O
HR	O
61	O
RR	O
18	O
Sat	O
95%	O
on	O
RA	O
Gen:	O
NAD	O
HEENT:	O
NC/AT,	O
PERRL,	O
nonicteric.	O
EOMI.	O
No	O
strabismus,	O
OP	O
w/	O
MMM	O
Neck:	O
supple,	O
NT,	O
full	O
ROM,	O
JVP	O
normal	O
Lungs:	O
CTA	ORGANIZATION
bilat	O
Cor:	O
reg,	O
S1S2,	O
no	O
m/r/g	O
Abd:	O
soft,	O
non-tender,	O
BS+	O
G/R:	O
normal	O
rectal	O
tone,	O
no	O
stool	O
in	O
vault	O
Extr:	O
no	O
c/c/e	O
2+	O
DP's	O
bilat	O
Neuro:	O
AAOX3,	O
MS	O
nl	O
Chemistry:	O
Plasma	O
Sodium	O
139	O
(135-145)	O
mmol/L	O
Plasma	O
Potassium	O
3.9	O
(3.4-4.8)	O
mmol/L	O
Plasma	O
Chloride	O
108	O
(100-108)	O
mmol/L	O
Plasma	O
Carbon	O
Dioxide	O
19.6	O
L	O
(23.0-31.9)	O
mmol/L	O
Calcium	O
8.8	O
(8.5-10.5)	O
mg/dl	O
Phosphorus	O
3.6	O
(2.6-4.5)	O
mg/dl	O
Magnesium	O
1.6	O
(1.4-2.0)	O
meq/L	O
Plasma	O
Urea	O
Nitrogen	O
35	O
H	O
(8-25)	O
mg/dl	O
Plasma	O
Creatinine	O
2.3	O
H	O
(0.6-1.5)	O
mg/dl	O
Plasma	O
Glucose	O
107	O
(70-110)	O
mg/dl	O
Amylase	O
106	O
H	O
(3-100)	O
units/L	O
Lipase	O
3.4	O
(1.3-6.0)	O
U/dl	O
Creatine	O
Kinase	O
Isoenz	O
219.1	O
H	O
(0.0-6.9)	O
ng/ml	O
CPK	O
Isoenzymes	O
Index	O
16.5	O
H	O
(0.0-3.5)	O
%	O
Troponin-T	O
4.32	O
H	O
(0.00-0.09)	O
ng/ml	O
Creatine	O
Kinase	O
1327	O
H	O
(60-400)	O
U/L	O
Heme:	O
WBC	O
10.3	O
(4.5-11.0)	O
th/cmm	O
HCT	O
36.5	O
L	O
(41.0-53.0)	O
%	O
HGB	O
12.8	O
L	O
(13.5-17.5)	O
gm/dl	O
RBC	O
4.28	O
L	O
(4.50-5.90)	O
mil/cmm	O
PLT	O
190	O
(150-350)	O
th/cumm	O
MCV	O
85	O
(80-100)	O
fl	O
MCH	O
29.8	O
(26.0-34.0)	O
pg/rbc	O
MCHC	O
35.0	O
(31.0-37.0)	O
g/dl	O
RDW	O
13.6	O
(11.5-14.5)	O
%	O
Superstat	O
PT	O
13.3	O
H	O
(11.1-13.1)	O
sec	O
Superstat	O
PT-INR	O
1.2	O
PT-INR	O
values	O
are	O
valid	O
only	O
for	O
WARFARIN	O
ANTI-COAG	O
THERAPY.	O
Superstat	O
APTT	O
29.0	O
(22.1-35.1)	O
sec	O
CXR:	O
wet	O
read	O
-normal	O
heart	O
size,	O
mild	O
increase	O
vascular	O
markings	O
otherwise	O
clear	O
xray	O
EKG:	O
Old	O
EKG	O
2067	O
NSR	O
84	O
BPM	O
normal	O
axis,	O
normal	O
intervals,	O
LAE,	O
Q	O
in	O
III,	O
biphasic	O
T	O
waves	O
V3-V6	O
old	O
in	O
V4-V6,	O
I	O
AVL,	O
PVC	O
EKG	O
at	O
BRA:	O
2/3	O
66BPM	O
NSR,	O
LAE,	O
normal	O
intervals,	O
2mm	O
STD	O
in	O
V5	O
V6,	O
Q's	O
in	O
V2,	O
V3	O
On	O
admission	O
to	O
CCU:	O
NSR	ORGANIZATION
66	O
BPM,	O
LAE,	O
normal	O
axis,	O
normal	O
intervals,	O
q's	O
in	O
V2	O
V3,	O
new	O
1mm	O
STD	O
in	O
I,	O
AVL,	O
II,	O
AVF,	O
V4;	O
new	O
2mm	O
STD	O
in	O
V5	O
V6;	O
new	O
1.5mm	O
STE	O
in	O
AVR,	O
R	O
sided	O
leads	O
no	O
elevations	O
in	O
V3	O
V4	O
Impr:	O
61	O
yo	O
male	O
with	O
(CRF:former	O
smoker,	O
HTN)	O
presents	O
with	O
NSTEMI	O
with	O
likely	O
diffuse	O
disease,	O
concerning	O
given	O
crescendo	O
nature	O
of	O
symptoms	O
Plan:	O
1.	O
Ischemia	O
-	O
patient	O
pain	O
free	O
right	O
now	O
a.	O
Heparin,	O
ASA,	O
titrate	O
betablocker,	O
statin,	O
holding	O
ACEI	O
given	O
CRI,	O
s/p	O
double	O
bolus	O
of	O
initegrillin	O
per	O
Dr.	O
Null	O
b.	O
Cycle	O
enzymes,	O
high	O
troponin	O
and	O
CK's,	O
troponin	O
combination	O
of	O
renal	O
failure	O
and	O
clear	O
cardiac	O
event	O
over	O
the	O
past	O
few	O
days	O
c.	O
Check	O
fasting	O
lipids,	O
HgA1C	O
d.	O
Plan	O
to	O
go	O
to	O
cath	O
lab	O
tomorrow,	O
NPO	O
past	O
MN	O
2.	O
Pump	O
a.	O
Attempt	O
to	O
get	O
AM	O
echo	O
before	O
cath	O
per	O
Dr.	O
Null	PERSON
b.	O
May	O
have	O
mild	O
pulm	O
edema	O
on	O
CXR	O
however,	O
clinically	O
appears	O
euvolemic	O
without	O
signs	O
of	O
failure	O
3.	O
CRI	O
--	O
S/p	O
L	O
nephrectomy	O
and	O
R	O
renal	O
radiation	O
therapy,	O
and	O
radiation	O
to	O
T1	O
a.	O
Mucomyst,	O
IV	O
hydration	O
_________________________________	O
Terry	PERSON
Choi	PERSON
M.D.	O
pg	O
00576	O
****************************************************************************************************	O
Record	O
date:	O
2070-10-27	O
CLERMONT	O
COUNTY	O
HOSPITAL	O
ERVING,	O
VERMONT	O
GI	O
CONSULT	O
NOTE	O
10/27/2070	O
Patient:	O
Langston,	O
Sherman	O
Unit	O
#:	O
7223692	O
Location:	O
Ol	O
11	O
Referring	O
Attending:	O
Null	O
Reason	O
for	O
consult:	O
BRBPR	O
HPI:	O
The	O
patient	O
is	O
a	O
62	O
year	O
old	O
male	O
with	O
history	O
of	O
CAD	O
s/p	O
stents	O
of	O
LAD	O
lesion	O
in	O
February	O
2070	O
readmit	O
with	O
chest	O
pain.	O
At	O
the	O
time	O
of	O
this	O
stenting,	O
he	O
had	O
total	O
occlusion	O
of	O
his	O
midRCA	O
and	O
mid	O
circumflex	O
with	O
collaterals.	O
He	O
presented	O
with	O
recurrent	O
CP	O
in	O
May,	O
and	O
he	O
underwent	O
a	O
second	O
cath	O
that	O
revealed	O
patent	O
LAD	O
with	O
stent.	O
This	O
admission	O
he	O
had	O
a	O
10-15	O
minute	O
episode	O
of	O
chest	O
and	O
jaw	O
pain	O
with	O
tachycardia	O
and	O
hypertension	O
by	O
home	O
monitoring.	O
The	O
pain	O
resolved	O
with	O
two	O
sublingual	O
nitroglycerins.	O
He	O
went	O
to	O
Butte	ORGANIZATION
Ridge	ORGANIZATION
Associates	ORGANIZATION
where	O
he	O
was	O
placed	O
on	O
heparin.	O
In	O
the	O
setting	O
of	O
heparin	O
bolus	O
with	O
PPT	O
>150,	O
he	O
had	O
one	O
episode,	O
moderate	O
volume	O
of	O
painless	O
rectal	O
bleeding.	O
Since	O
transfer,	O
still	O
on	O
heparin,	O
he	O
had	O
no	O
further	O
bleeding,	O
nor	O
guiac	O
positive	O
stool	O
until	O
today.	O
This	O
morning	O
he	O
passed	O
a	O
large	O
brown	O
bowel	O
movement	O
which	O
was	O
guiac	O
negative.	O
Following	O
this,	O
after	O
returning	O
to	O
bed,	O
he	O
had	O
a	O
small	O
amount	O
of	O
oozing	O
of	O
blood	O
from	O
his	O
rectum.	O
He	O
had	O
no	O
abdominal	O
pain.	O
No	O
further	O
bleeding	O
since	O
this	O
morning.	O
He	O
has	O
never	O
had	O
rectal	O
bleeding	O
previously	O
on	O
aspirin	O
and	O
plavix.	O
His	O
colonoscopy	O
in	O
2066	O
revealed	O
diverticulosis.	O
He	O
is	O
currently	O
on	O
aspirin,	O
plavix,	O
heparin	O
with	O
therapeutic	O
PTT.	O
PMH:	O
CAD	O
Hypertension	O
Hypercholesterolemia	O
CRI	O
Bilateral	O
RCC	O
-	O
s/p	O
L	O
nephrectomy,	O
R	O
RFA,	O
XRT.	O
Per	O
renal	O
note	O
also	O
has	O
pulmonary	O
nodules,	O
which	O
are	O
presumably	O
metastatic	O
RCC,	O
but	O
patient	O
states	O
prior	O
eval	O
not	O
consistent	O
with	O
met	O
disease.	O
MEDS:	O
ASA	O
325	O
Plavix	ORGANIZATION
Heparin	ORGANIZATION
gtt	ORGANIZATION
Toprol	ORGANIZATION
Protonix	ORGANIZATION
Lipitor	ORGANIZATION
HCTZ	ORGANIZATION
Diovan	ORGANIZATION
MVI	ORGANIZATION
ALL:	O
NKDA	O
SH:	O
works	O
as	O
an	O
archivist.	O
FH:	O
no	O
pertinent	O
FH.	O
PE:	O
VS:	O
97.7	O
65	O
120/75	O
99	O
%	O
RA	O
General:	O
appears	O
well,	O
NAD	O
Neck:	O
no	O
LN	O
Heent:	O
OP	O
dry,	O
conjunc	O
nl	O
Chest:	O
CTA	O
B	O
Cardiac:	O
RRR,	O
nl	O
s1,	O
s2,	O
no	O
murmurs,	O
rubs	O
or	O
gallops	O
Abdomen:	O
NT,	O
ND,	O
nl	O
BS.	O
No	O
hepatomegaly.	O
No	O
epigastric	O
discomfort	O
Rectal:	O
small	O
external	O
hemorrhoids,	O
nontender,	O
blood	O
around	O
anus	O
Extremities:	O
no	O
edema,	O
normal	O
pulses	O
Skin:	O
No	O
jaundice,	O
no	O
rashes	O
LABS:	O
Plasma	O
Sodium	O
137	O
(135-145)	O
mmo	O
Plasma	O
Potassium	O
4.0	O
(3.4-4.8)	O
mmol	O
Plasma	O
Chloride	O
102	O
(100-108)	O
mmol	O
Plasma	O
Carbon	O
Dioxide	O
26.9	O
(23.0-31.9)	O
mmol	O
Plasma	ORGANIZATION
Urea	ORGANIZATION
Nitrogen	ORGANIZATION
38	O
H	O
(8-25)	O
mg/	O
Plasma	O
Creatinine	O
2.6	O
H	O
(0.6-1.5)	O
mg/	O
WBC	O
6.2	O
(4.5-11.0)	O
th/	O
HCT	O
37.2	O
L	O
(41.0-53.0)	O
%	O
HGB	O
12.7	O
L	O
(13.5-17.5)	O
gm/	O
RBC	O
4.18	O
L	O
(4.50-5.90)	O
mil/	O
PLT	O
169	O
(150-350)	O
th/c	O
MCV	O
89	O
(80-100)	O
fl	O
MCH	O
30.3	O
(26.0-34.0)	O
pg/	O
MCHC	O
34.0	O
(31.0-37.0)	O
g/dl	O
RDW	O
13.9	O
(11.5-14.5)	O
%	O
PT	O
15.3	O
H	O
(11.3-13.3)	O
sec	O
PT-INR	O
1.5	O
C	O
Superstat	O
APTT	O
72.0	O
H	O
(22.1-35.1)	O
sec	O
10/27/70	O
10/26/70	O
10/25/70	O
10/24/70	O
10/24/70	O
10/23/70	O
10/23/70	O
<5	O
<5	O
HCT	O
37.2(L)	O
38.4(L)	O
35.5(L)	O
33.3(L)	O
31.8(L)	O
33.3(L)	O
37.0(L)	O
No	O
transfusions	O
while	O
in	O
the	O
hospital	O
09/22/70	O
FE	O
47	O
TIBC	O
278	O
FER	O
134	O
Colonoscopy	O
June	O
2066	O
-	O
large	O
mouthed	O
diverticulosis	O
in	O
the	O
sigmoid.	O
No	O
prior	O
EGD	O
-	O
had	O
an	O
ENT	O
exam	O
at	O
Ridge	O
Family	O
Practice	O
ASSESSMENT	O
AND	O
PLAN:	O
62	O
year	O
old	O
male	O
with	O
CAD	O
with	O
chest	O
pain	O
to	O
be	O
evaluated	O
by	O
cardiac	O
cath	O
with	O
painless	O
BRBPR	O
following	O
a	O
large	O
bowel	O
movement	O
today	O
in	O
the	O
setting	O
of	O
heparin,	O
aspirin	O
and	O
plavix.	O
This	O
bleeding	O
was	O
minimal	O
and	O
self-limited,	O
most	O
likely	O
of	O
hemorrhoidal	O
origin,	O
rather	O
than	O
diverticular.	O
He	O
does	O
have	O
some	O
risk	O
of	O
recurrent	O
rectal	O
bleeding	O
on	O
these	O
blood	O
thinners,	O
but	O
the	O
bleeding	O
is	O
likely	O
to	O
continue	O
to	O
be	O
limited.	O
There	O
is	O
no	O
contraindication	O
to	O
cardiac	O
catherization	O
or	O
continuing	O
these	O
important	O
medications.	O
There	O
is	O
also	O
no	O
intervention	O
we	O
could	O
do	O
now	O
to	O
adjust	O
the	O
risk.	O
Would	O
recommend	O
proceeding	O
to	O
cardiac	O
catherization.	O
No	O
alteration	O
in	O
medications	O
required.	O
Opal	O
Larson,	O
M.D.	O
GI	O
Fellow	O
-	O
Page	O
09744	O
****************************************************************************************************	O
Record	O
date:	O
2072-01-13	O
INTERNAL	O
MEDICINE	O
ASSOCIATES	O
CLERMONT	O
COUNTY	O
HOSPITAL	O
Personal	O
data	O
and	O
overall	O
health	O
This	O
is	O
a	O
63-year-old	O
man	O
status	O
post	O
renal	O
cell	O
CA	O
and	O
coronary	O
artery	O
disease.	O
Reason	O
for	O
visit	O
Followup	O
multiple	O
medical	O
problems.	O
Major	O
Problems	O
Coronary	O
artery	O
disease	O
The	O
patient	O
had	O
recatheterization	O
in	O
May	O
demonstrating	O
30%	O
stenosis	O
at	O
the	O
stent	O
on	O
the	O
LAD.	O
He	O
has	O
diffuse	O
disease,	O
otherwise	O
with	O
good	O
collateralization.	O
His	O
exercise	O
tolerance	O
test	O
in	O
May	O
showed	O
excellent	O
exercise	O
tolerance,	O
but	O
positive	O
images	O
and	O
positive	O
tracings	O
for	O
ischemia.	O
His	O
Duke	PERSON
ETT	PERSON
score	O
was	O
-10,	O
but	O
his	O
perfusion	O
images	O
showed	O
moderate	O
reversibility	O
in	O
the	O
inferior	O
myocardium	O
with	O
fixed	O
defect	O
anteriorly.	O
He	O
had	O
infero-apical	O
hypokinesis	O
with	O
an	O
EF	O
of	O
45%.	O
It	O
is	O
also	O
noted	O
that	O
his	O
blood	O
pressure	O
fell,	O
but	O
normalized	O
during	O
exercise.	O
The	O
patient	O
has	O
been	O
stable	O
until	O
last	O
several	O
weeks	O
when	O
he	O
has	O
noted	O
some	O
lower	O
jaw	O
symptoms	O
anteriorly.	O
He	O
has	O
no	O
chest	O
squeezing,	O
neck	O
squeezing,	O
or	O
jaw	O
pain,	O
but	O
there	O
is	O
an	O
odd	O
sensation	O
under	O
the	O
front	O
teeth.	O
He	O
correlates	O
this	O
with	O
exercise,	O
but	O
not	O
consistently.	O
He	O
also	O
occasionally	O
feels	O
this	O
at	O
rest.	O
He	O
is	O
able	O
to	O
walk	O
uphill	O
and	O
generate	O
a	O
pulse	O
of	O
90.	O
He	O
is	O
working	O
doing	O
heavy	O
labor	O
in	O
the	O
garden,	O
moving,	O
and	O
digging.	O
It	O
is	O
not	O
clear	O
that	O
this	O
represents	O
angina,	O
but	O
we	O
will	O
let	O
Dr.	O
Null	O
know.	O
He	O
is	O
seeing	O
Dr.	O
Null	PERSON
in	O
three	O
to	O
four	O
weeks.	O
01/13/2072:	O
.	O
The	O
patient	O
has	O
been	O
improving	O
steadily	O
since	O
his	O
last	O
admission	O
for	O
CABG	O
in	O
November	O
2070.	O
He	O
states	O
his	O
exercise	O
tolerance	O
is	O
steadily	O
improving	O
and	O
he	O
is	O
able	O
to	O
walk	O
2	O
miles	O
at	O
a	O
rate	O
of	O
17	O
minutes	O
per	O
mile	O
without	O
dyspnea	O
or	O
fatigue.	O
He	O
has	O
no	O
chest	O
pain	O
or	O
palpitations.	O
He	O
does	O
worry	O
about	O
bradycardia	O
and	O
states	O
his	O
metoprolol	O
of	O
50	O
mg	O
BID	O
may	O
be	O
too	O
much.	O
He	O
has	O
been	O
withholding	O
half	O
to	O
full	O
dose	O
in	O
the	O
evenings	O
when	O
he	O
finds	O
his	O
pulses	O
about	O
50.	O
He	O
has	O
no	O
symptoms	O
of	O
forgetfulness,	O
dizziness,	O
or	O
loss	O
of	O
consciousness.	O
He	O
does	O
have	O
cold	O
extremities.	O
There	O
is	O
no	O
weight	O
gain,	O
weight	O
loss,	O
or	O
orthopnea.	O
He	O
remains	O
on	O
beta	O
blocker,	O
lipitor,	O
Diovan,	O
hydrochlorothiazide,	O
digoxin,	O
and	O
warfarin.	O
He	O
is	O
no	O
longer	O
on	O
Plavix.	O
He	O
has	O
no	O
nitrate.	O
On	O
exam,	O
he	O
is	O
somewhat	O
hypertensive,	O
but	O
lungs	O
are	O
good	O
and	O
there	O
is	O
no	O
elevated	O
JVP.	O
His	O
EKG	O
shows	O
first-degree	O
AV	O
block	O
and	O
I	O
have	O
asked	O
him	O
to	O
bring	O
that	O
to	O
Dr.	O
Null's	O
office	O
today.	O
A:	O
Stable.	O
P:	O
Continue	O
medicines	O
as	O
they	O
are	O
because	O
of	O
borderline	O
blood	O
pressure	O
today.	O
We	O
would	O
not	O
escalate	O
Elevated	O
blood	O
pressure	O
The	O
patient	O
is	O
on	O
Diovan,	O
hydrochlorothiazide,	O
and	O
metoprolol.	O
He	O
states	O
his	O
blood	O
pressure	O
at	O
home	O
is	O
usually	O
quite	O
good	O
and	O
he	O
feels	O
it	O
is	O
responsive	O
to	O
external	O
stressors.	O
I	O
have	O
asked	O
him	O
to	O
think	O
hard	O
about	O
removing	O
those	O
stressors	O
from	O
his	O
life	O
in	O
order	O
to	O
maintain	O
health.	O
Hyperlipidemia	O
The	O
patient	O
remains	O
on	O
40	O
mg	O
of	O
lipitor.	O
It	O
has	O
been	O
a	O
year	O
since	O
we	O
have	O
checked	O
his	O
excellent	O
results	O
and	O
we	O
will	O
check	O
today.	O
He	O
has	O
been	O
fasting	O
for	O
12	O
hours.	O
Iron	O
deficiency	O
anemia	O
We	O
will	O
recheck	O
his	O
indices	O
today.	O
He	O
is	O
not	O
fatigued	O
or	O
pale.	O
Renal	O
carcinoma	O
The	O
patient	O
had	O
repeat	O
MRI	O
and	O
CT	O
in	O
January	O
at	O
the	O
Healthwin	O
Hospital.	O
These	O
were	O
read	O
as	O
stable	O
and	O
he	O
needs	O
no	O
intervention	O
other	O
than	O
continued	O
monitoring	O
at	O
six-month	O
intervals.	O
He	O
is	O
quite	O
reassured	O
by	O
this.	O
Seborrheic	O
keratosis	O
The	O
patient	O
is	O
ready	O
for	O
evaluation	O
by	O
Dermatology.	O
He	O
has	O
several	O
atypical-looking	O
keratoses	O
as	O
well	O
as	O
poorly	O
rounded	O
lesions	O
consistent	O
with	O
BCCs.	O
I	O
have	O
referred	O
him	O
to	O
Dermatology	O
today.	O
Sinus	O
congestion	O
/	O
5/10/64=&gt;/and	O
stuffiness,	O
no	O
ear	O
ache,	O
no	O
sore	O
throat,	O
no	O
shaking,	O
chills	O
(saw	O
Dr.	O
I	O
have	O
reinforced	O
the	O
wisdom	O
of	O
taking	O
some	O
nasal	O
steroid	O
at	O
HS.	O
Anticoagulation	O
The	O
patient	O
is	O
fully	O
anticoagulated	O
indefinitely.	O
Active	O
Medications	O
aspirin	O
81	O
mg	O
po	O
qd	O
digoxin	O
0.125	O
mg	O
po	O
qd	O
Diovan	O
HCT	O
160mg/25mg	O
po	O
qd	O
ferrous	O
sulfate	O
325	O
mg	O
PO	O
TID	O
with	O
ascorbic	O
acid	O
Foltx	O
2.2	O
mg	O
Lipitor	O
40	O
mg	O
metoprolol	O
50	O
mg	O
bid	O
Nasacort	O
aq	O
55mcg/spray	O
nasal	O
qd	O
1-2	O
sprays/nostril;	O
discontinue	O
after	O
3wks	O
if	O
no	O
improvement	O
nystatin	O
powder	O
100,000u/g	O
top	O
bid-tid	O
apply	O
to	O
affected	O
area(s)	O
warfarin	O
5	O
mg	O
5	O
mg	O
5	O
days	O
and	O
7.5	O
2	O
days	O
qwk	O
Change	O
in	O
therapies	O
and	O
Renewals	O
warfarin	O
5	O
mg	O
5	O
mg	O
5	O
days	O
and	O
7.5	O
2	O
days	O
qwk	O
Start:	O
2/13/2070	O
metoprolol	O
50	O
mg	O
bid	O
Start:	O
3/3/2070	O
Lipitor	O
40	O
mg	O
Start:	O
3/3/2070	O
Plavix	O
75	O
mg	O
1	O
tab	O
po	O
QD	O
Start:	O
3/3/2070	O
End:	O
01/13/2072	O
-	O
Inactivated	O
ranitidine	O
hcl	O
150	O
mg	O
1	O
tab	O
po	O
QD	O
Start:	O
3/3/2070	O
End:	O
01/13/2072	O
-	O
Inactivated	O
aspirin	O
81	O
mg	O
po	O
qd	O
Start:	O
3/3/2070	O
valsartan	O
80	O
mg	O
QD	O
Start:	O
4/24/2070	O
End:	O
01/13/2072	O
-	O
Inactivated	O
Foltx	O
2.2	O
mg	O
QTY:90	O
Refills:3	O
Start:	O
5/09/2070	O
hydrochlorothiazide	O
12.5	O
mg	O
1	O
tab	O
po	O
QD	O
Start:	O
6/12/2070	O
End:	O
01/13/2072	O
-	O
Inactivated	O
isosorbide	O
mononitrate	O
60	O
mg	O
QAM	O
and	O
1	O
half	O
qpm	O
Start:	O
6/12/2070	O
End:	O
01/13/2072	O
-	O
Inactivated	O
Nasacort	O
aq	O
55mcg/spray	O
nasal	O
qd	O
1-2	O
sprays/nostril;	O
discontinue	O
after	O
3wks	O
if	O
no	O
improvement	O
QTY:3	O
bottles	O
Refills:3	O
Start:	O
8/07/2070	O
digoxin	O
0.125	O
mg	O
po	O
qd	O
Start:	O
01/13/2072	O
Diovan	O
HCT	O
160mg/25mg	O
po	O
qd	O
Start:	O
01/13/2072	O
Social	O
history	O
The	O
patient	O
remains	O
at	O
home	O
and	O
has	O
continued	O
to	O
nurture	O
his	O
digital	O
archiving	O
business.	O
He	O
continues	O
to	O
interest	O
himself	O
in	O
equities	O
trading.	O
He	O
states	O
he	O
is	O
not	O
as	O
active	O
as	O
prior	O
to	O
his	O
CABG.	O
He	O
denies	O
depression.	O
Procedures	O
and	O
Immunizations	O
Influenza	O
vaccine	O
right	O
deltoid.	O
Lot	O
#82201,	O
expires	O
06/25/2072.	O
Review	O
of	O
systems	O
No	O
chest	O
pain,	O
palpitations,	O
shortness	O
of	O
breath	O
with	O
dyspnea	O
on	O
exertion,	O
or	O
orthopnea.	O
Stools	O
are	O
normal.	O
No	O
blood	O
or	O
black	O
stools.	O
Urination	O
is	O
fine	O
without	O
dysuria	O
or	O
frequency.	O
He	O
has	O
some	O
right	O
ankle	O
and	O
knee	O
pain	O
but	O
is	O
tolerable.	O
He	O
has	O
had	O
developed	O
some	O
strabismus	O
and	O
is	O
being	O
followed	O
by	O
Dr.	O
Benjamin	PERSON
Shepherd	PERSON
at	O
Ridge	O
Family	O
Practice	O
Center.	O
Physical	O
examination	O
Blood	O
pressure	O
140/84	O
,	O
repeated	O
by	O
me	O
140/90	O
Weight	O
206	O
pounds	O
Height	O
70.1	O
inches	O
General	O
appearance	O
Well	O
developed	O
and	O
well	O
nourished	O
in	O
no	O
apparent	O
distress	O
Skin	O
Multiple	O
seborrheic	O
keratoses	O
over	O
the	O
chest	O
and	O
back.	O
There	O
are	O
several	O
atypical	O
keratoses	O
as	O
well	O
as	O
some	O
rounded,	O
smooth	O
papules	O
over	O
the	O
chest.	O
He	O
has	O
skin	O
tags	O
on	O
the	O
neck	O
and	O
larger	O
pigmented	O
nevi	O
on	O
the	O
forehead	O
Heent	O
Oropharynx,	O
normal.	O
No	O
lesions.	O
Hearing	O
intact.	O
EOMI.	O
PERRL	O
Neck	O
Supple.	O
Carotids	O
2+.	O
No	O
bruits.	O
Thyroid	O
normal	O
Chest	O
Clear	O
to	O
PandA	O
Cor	O
S1	O
and	O
S2.	O
No	O
murmur,	O
gallops,	O
or	O
rubs	O
Abdomen	O
Bowel	O
sounds	O
positive.	O
Soft	O
and	O
nontender.	O
No	O
hepatosplenomegaly	O
Rectal	O
exam	O
Guaiac	O
negative	O
brown	O
stool.	O
Prostate	O
is	O
small,	O
flat,	O
and	O
only	O
partial	O
exam	O
is	O
possible	O
with	O
the	O
examining	O
finger	O
Genitalia	O
Genital,	O
normal	O
circumcised	O
phallus.	O
Normal	O
testes	O
without	O
discrete	O
nodule	O
or	O
mass.	O
No	O
inguinal	O
hernia	O
detected	O
Extrem	O
No	O
clubbing,	O
cyanosis,	O
or	O
edema.	O
No	O
DP	O
or	O
PT	O
pulses	O
detected.	O
Toes	O
well	O
perfused	O
but	O
cool.	O
DTRs	O
2+	O
at	O
the	O
patella,	O
0	O
at	O
the	O
calcaneus.	O
Toes	O
bilaterally	O
downgoing.	O
Gait	O
is	O
normal	O
Selected	O
recent	O
labs	O
EKG	O
reviewed.	O
There	O
is	O
first-degree	O
AV	O
block,	O
IMI,	O
and	O
lateral	O
T-wave	O
changes	O
that	O
are	O
chronic.	O
Rate	O
was	O
50.	O
Assessment	O
and	O
plan	O
Generally	O
stable	O
cardiac.	O
Renal	O
cell	O
cancer	O
without	O
progression.	O
Renal	O
function	O
to	O
be	O
evaluated	O
today.	O
Unilateral	O
kidney.	O
Anticoagulation	O
stable.	O
We	O
will	O
send	O
INR	O
today.	O
Blood	O
pressure	O
could	O
be	O
better	O
controlled.	O
Note	O
transcribed	O
by	O
outside	O
service	O
Transcription	O
errors	O
may	O
be	O
present.	O
Signed	O
electronically	O
by	O
Eads,	O
Kyle	PERSON
on	O
Jan	O
29,	O
2072	O
****************************************************************************************************	O
