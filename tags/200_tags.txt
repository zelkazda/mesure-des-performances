Record	O
date:	O
2170-02-17	O
Reason	O
for	O
Visit:	O
chemotherapy	O
Diagnosis/Treatment	O
Summary:	O
10/16/69	O
EGD	O
@	O
Griffin	PERSON
Hospital:	O
long	O
partially-obstructing	O
distal	O
esophageal	O
well	O
differentiated	O
invasive	O
adenoCa,	O
from	O
34-39	O
cm	O
from	O
Barrett's.	O
Rare	O
mucin	O
production.	O
10/17/69	O
CT	O
chest/abd:	O
prominent	O
thickening	O
distal	O
esophagus.	O
10/21/69	O
CT	O
chest:	O
LLL	O
8	O
mm	O
nodule,	O
R-sided	O
pleural	O
thickening.	O
10/21/69	O
Upper	O
EUS:	O
large	O
esophageal	O
mass	O
extending	O
35-40	O
cm,	O
9	O
mm	O
thickness,	O
partially	O
obstructing,	O
suggest	O
invasion	O
muscularis	O
propria.	O
Porta	O
hepatis	O
LAD	O
2	O
LN,	O
largest	O
12	O
mm.	O
10/22/69	O
FNA	O
LN	O
(porta	O
hepatis);	O
no	O
malignancy	O
12/01/69	O
esophagectomy	O
->	O
pT2N1	O
(three	O
positive	O
nodes)	O
2/08/2170	O
->	O
adjuvant	O
chemotherapy	O
with	O
5FU/LCV	O
Interval	O
History:	O
Received	O
week	O
1	O
adjuvant	O
chemo	O
last	O
week.	O
He	O
is	O
able	O
to	O
maintain	O
weight.	O
He	O
has	O
had	O
some	O
vomiting	O
associated	O
with	O
food	O
getting	O
stuck.	O
He	O
is	O
due	O
for	O
a	O
dilation	O
on	O
2/09.	O
He	O
had	O
diarrhea	O
X	O
2	O
last	O
night,	O
did	O
not	O
need	O
anything	O
to	O
resolve	O
it.	O
He	O
had	O
mild	O
mouth	O
sores	O
on	O
his	O
bottom	O
lip,	O
and	O
tongue.	O
Presents	O
today	O
for	O
chemotherapy	O
PMH/PSH:	O
GERD	O
past	O
20	O
yrs	O
HTN	O
DM	O
type	O
II	O
Partially	O
blocked	O
L	O
carotid	O
Hypercholesterolemia	O
Chronic	O
bronchitis	O
AAA	O
repair	O
x2	O
2165,	O
2167	O
Cardiac	O
stent	O
2164	O
Medications	O
were	O
reviewed	O
today:	O
Aspirin	O
(ACETYLSALICYLIC	O
ACID)	O
325	O
MG	O
(325MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Caltrate	O
600	O
+	O
D	O
1	O
TAB	O
PO	O
BID	O
Centrum	O
SILVER	O
(MULTIVITAMINS)	O
1	O
TAB	O
PO	O
QD	O
Isosorbide	O
30	O
MG	O
1/2	O
TAB	O
PO	O
bid	O
Lipitor	O
(ATORVASTATIN)	O
20	O
MG	O
(20MG	O
TABLET	O
take	O
1)	O
PO	O
QHS	O
Lisinopril	O
40	O
MG	O
(40MG	O
TABLET	O
take	O
1)	O
PO	ORGANIZATION
QD	ORGANIZATION
Metoprolol	ORGANIZATION
50	O
MG	O
PO	O
QD	O
Plavix	O
(CLOPIDOGREL)	O
75	O
MG	O
(75MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Prilosec	O
(OMEPRAZOLE)	O
40	O
MG	O
(40MG	O
CAPSULE	O
DR	O
take	O
1)	O
PO	O
QD	O
Tricor	O
(FENOFIBRATE	O
(TRICOR))	O
145	O
MG	O
(145MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Vitamin	O
C	O
(ASCORBIC	O
ACID)	O
500	O
MG	O
PO	O
qd	O
Vitamin	O
E	O
(TOCOPHEROL-DL-ALPHA)	O
400	O
UNITS	O
PO	O
QD	O
Glyburide	O
2.5	O
mg	O
qd	O
Allergies:	O
Adhesive	O
tape-rash	O
SH/FH:	O
Marital	O
status:	O
lives	O
w/	O
wife	O
Children:	O
3	O
grown	O
healthy	O
adult	O
children	O
Job:	O
retired	O
2158	O
Producer	O
for	O
National	ORGANIZATION
Radio	ORGANIZATION
and	ORGANIZATION
Access	ORGANIZATION
America	ORGANIZATION
Smoking	ORGANIZATION
hx:	O
1/2-1ppd	O
x	O
30	O
yrs,	O
quit	O
2153	O
ETOH:	O
rare	O
social	O
Illicit	O
drugs:	O
no	O
Environmental	O
exposure:	O
battery	O
chemicals	O
such	O
as	O
zinc	O
chloride,	O
manganese	O
dioxide,	O
carbon,	O
and	O
chemicals	O
for	O
dye	O
production	O
Exercise:	O
generally	O
walks	O
several	O
times	O
a	O
week	O
and	O
plays	O
golf	O
once	O
a	O
week.	O
Able	O
to	O
climb	O
1-2	O
flt	O
stairs	O
without	O
sob.	O
Mother:	O
died	O
age	O
65	O
MI	O
Father:	O
died	O
age	O
89	O
"multip	O
organ	O
failure"	O
Siblings:	O
older	O
brother	O
and	O
younger	O
sister	O
no	O
ca	O
Review	O
of	O
Systems:	O
General:	O
stable	O
weight	O
after	O
losing	O
weight	O
postoperatively,	O
fatigue	O
stable,	O
no	O
fevers,	O
no	O
chills,	O
no	O
change	O
in	O
appetite	O
Respiratory:	O
chronic	O
intermit	O
cough,	O
no	O
SOB,	O
no	O
DOE,	O
no	O
hemoptysis,	O
no	O
wheezing	O
HEENT:	O
No	O
neck	O
swelling,	O
no	O
neck	O
stiffness,	O
slight	O
hoarseness,	O
significant	O
hearing	O
loss,	O
primarily	O
on	O
the	O
left.	O
Cardiac:	O
No	O
chest	O
pain,	O
no	O
palpitations,	O
no	O
arrythmias,	O
no	O
valve	O
disease	O
Gastrointestinal:	O
No	O
nausea,	O
no	O
vomiting,	O
no	O
diarrhea,	O
no	O
constipation,	O
no	O
bleeding,	O
eating	O
by	O
mouth	O
with	O
dysphagia	O
Neurologic:	O
No	O
seizures,	O
occas	O
headache,	O
no	O
weakness,	O
no	O
numbness/	O
tingling	O
Lymph	O
nodes:	O
No	O
enlarged	O
lymph	O
nodes	O
Musculoskeletal:	O
chronic	O
intermit	O
back	O
pain,	O
no	O
neck	O
pain,	O
no	O
leg	O
pain,	O
no	O
arm	O
pain	O
Urologic:	O
No	O
hematuria,	O
no	O
dysuria,	O
or	O
no	O
obstruction	O
Endocrine:	O
No	O
adrenal,	O
no	O
thyroid	O
or	O
no	O
parathyroid	O
disease	O
Hematologic:	O
No	O
bruising,	O
no	O
clotting,	O
no	O
bleeding	O
Toxicities:	O
0	O
1	O
2	O
3	O
4	O
Shortness	O
of	O
Breath	O
Nausea	O
Vomiting	O
x	O
Diarrhea	O
Renal	O
Toxicity	O
Dysphagia	O
/	O
Esophagitis	O
x	O
Rash	O
Neutropenic	O
Fever	O
Non-Neutropenic	O
Infection	O
Sensory	O
Neuropathy	O
Motor	O
Neuropathy	O
PE	O
/	O
DVT	O
Physical	O
Examination:	O
Vital	O
Signs	O
BP	O
106/52,	O
P	O
56,	O
RR	O
16,	O
Temp	O
97.8	O
F,	O
Ht	O
64.75	O
in,	O
Wt	O
147.5	O
lb	O
PAIN	O
LEVEL	O
(0-10)	O
0	O
FATIGUE	O
(0-10)	O
0	O
PS=1	O
Neck:	O
no	O
nodes	O
palpable	O
HEENT:grade	O
1	O
mucositis	O
lower	O
lip;	O
EOMI;	O
PERRL;	O
anicteric	O
Skin:	O
no	O
rash	O
Ext:	O
without	O
edema,	O
clubbing,	O
cyanosis	O
Cor:	O
S1	O
S2,	O
no	O
mrg	O
Chest:	O
clear	O
lungs	O
without	O
wheezing	O
or	O
crackles,	O
no	O
dullness	O
to	O
percussion;	O
incisions	O
have	O
healed	O
well	O
Abdomen:	O
soft,	O
NT	O
no	O
HSM;	O
G-tube	O
site	O
is	O
clean	O
Nodes:	O
no	O
cervical	O
supraclavicular	O
axillary	O
or	O
inguinal	O
adenopathy	O
Neuro:	O
grossly	O
non-focal	O
Data	O
(laboratory	O
and	O
radiographs)	O
pathology	O
report	O
does	O
above	O
showed	O
a	O
T2N1	O
cancer	O
with	O
three	O
positive	O
nodes	O
CBC	ORGANIZATION
and	O
chemistries	O
were	O
reviewed	O
Assessment	O
and	O
Plan:	O
-	O
locally	O
advanced	O
esophageal	O
cancer	O
status	O
post	O
esophagectomy	O
with	O
negative	O
margins	O
-	O
he	O
has	O
node	O
positive	O
disease,	O
and	O
therefore	O
we	O
recommended	O
adjuvant	O
chemotherapy	O
and	O
radiation.	O
-	O
Week	O
2	O
5FU/	O
leukovorin	O
today	O
-	O
dilation	O
2/19	O
with	O
Dr.	O
Everson.	O
-	O
all	O
questions	O
were	O
answered.	O
_____________________________________________	O
Pauline	PERSON
Victoria	PERSON
Xenos,NP	O
****************************************************************************************************	O
Record	O
date:	O
2170-03-02	O
Reason	O
for	O
Visit:	O
Week	O
4	O
adjuvant	O
chemotherapy	O
with	O
5-FU/LCV	O
Diagnosis/Treatment	O
Summary:	O
10/16/69	O
EGD	O
@	O
Griffin	PERSON
Hospital:	O
long	O
partially-obstructing	O
distal	O
esophageal	O
well	O
differentiated	O
invasive	O
adenoCa,	O
from	O
34-39	O
cm	O
from	O
Barrett's.	O
Rare	O
mucin	O
production.	O
10/17/69	O
CT	O
chest/abd:	O
prominent	O
thickening	O
distal	O
esophagus.	O
10/21/69	O
CT	O
chest:	O
LLL	O
8	O
mm	O
nodule,	O
R-sided	O
pleural	O
thickening.	O
10/21/69	O
Upper	O
EUS:	O
large	O
esophageal	O
mass	O
extending	O
35-40	O
cm,	O
9	O
mm	O
thickness,	O
partially	O
obstructing,	O
suggest	O
invasion	O
muscularis	O
propria.	O
Porta	O
hepatis	O
LAD	O
2	O
LN,	O
largest	O
12	O
mm.	O
10/22/69	O
FNA	O
LN	O
(porta	O
hepatis);	O
no	O
malignancy	O
12/01/69	O
esophagectomy	O
->	O
pT2N1	O
(three	O
positive	O
nodes)	O
2/08/2170	O
->	O
adjuvant	O
chemotherapy	O
with	O
5FU/LCV	O
Interval	O
History:	O
One	O
episode	O
of	O
diarrhea;	O
took	O
imodium	O
with	O
effect.	O
Episode	O
of	O
meat	O
getting	O
stuck	O
over	O
the	O
w/e	O
due	O
to	O
eating	O
too	O
fast/not	O
chewing	O
well.	O
To	O
have	O
another	O
dilation	O
this	O
week.	O
To	O
meet	O
with	O
radiation	O
today.	O
Denies	O
numbness/tingling.	O
No	O
fevers.	O
Minimal	O
fatigue.	O
Lisinopril	O
was	O
reduced	O
to	O
20mg	O
last	O
week	O
secondary	O
to	O
hypotension.	O
PMH/PSH:	O
GERD	O
past	O
20	O
yrs	O
HTN	O
DM	O
type	O
II	O
Partially	O
blocked	O
L	O
carotid	O
Hypercholesterolemia	O
Chronic	O
bronchitis	O
AAA	O
repair	O
x2	O
2165,	O
2167	O
Cardiac	O
stent	O
2164	O
Medications	O
were	O
reviewed	O
today:	O
Aspirin	O
(ACETYLSALICYLIC	O
ACID)	O
325	O
MG	O
(325MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Ativan	O
(LORAZEPAM)	O
1MG	O
TABLET	O
take	O
1	O
Tablet(s)	O
PO	O
Q4H	O
PRN	O
nausea	O
Caltrate	O
600	O
+	O
D	O
(CALCIUM	O
CARBONATE	O
1500	O
MG	O
(600	O
MG	O
ELEM	O
CA)/	O
VIT	O
D	O
200	O
IU)	O
1	O
TAB	O
PO	O
BID	O
Centrum	O
SILVER	O
(MULTIVITAMINS)	O
1	O
TAB	O
PO	O
QD	O
Glyburide	O
2.5	O
MG	O
(2.5MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Isosorbide	O
30	O
MG	O
1/2	O
TAB	O
PO	O
bid	O
Lipitor	O
(ATORVASTATIN)	O
20	O
MG	O
(20MG	O
TABLET	O
take	O
1)	O
PO	O
QHS	O
Lisinopril	O
20MG	O
TABLET	O
PO	O
QD	O
Metoprolol	O
SUCCINATE	O
EXTENDED	O
RELEASE	O
50	O
MG	O
(50MG	O
TAB.SR	O
24H	O
take	O
1)	O
PO	O
QD	O
Plavix	O
(CLOPIDOGREL)	O
75	O
MG	O
(75MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Prilosec	O
(OMEPRAZOLE)	O
40	O
MG	O
(40MG	O
CAPSULE	O
DR	O
take	O
1)	O
PO	O
QD	O
Reglan	O
5	O
MG/5	O
ML	O
SYRUP	O
(METOCLOPRAMIDE	O
5	O
MG/5	O
ML	O
SYRUP)	O
10	O
MG	O
(5MG/5ML	O
SOLUTION	O
take	O
10	O
ML)	O
PO	O
QID	O
(AC	O
+	O
HS)	O
PRN	O
nausea	O
,	O
10ML=2tsp	O
Tricor	O
(FENOFIBRATE	O
(TRICOR))	O
145	O
MG	O
(145MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Vitamin	O
C	O
(ASCORBIC	O
ACID)	O
500	O
MG	O
PO	O
qd	O
Vitamin	O
E	O
(TOCOPHEROL-DL-ALPHA)	O
400	O
UNITS	O
(400	O
UNIT	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Zoloft	O
(SERTRALINE)	O
50	O
MG	O
(50MG	O
TABLET	O
take	O
1)	O
PO	O
QD	O
Allergies:	O
Adhesive	O
tape-rash	O
SH/FH:	O
Marital	O
status:	O
lives	O
w/	O
wife	O
Children:	O
3	O
grown	O
healthy	O
adult	O
children	O
Job:	O
retired	O
2158	O
Producer	O
for	O
National	ORGANIZATION
Radio	ORGANIZATION
and	O
Access	O
Americas	O
Smoking	O
hx:	O
1/2-1ppd	O
x	O
30	O
yrs,	O
quit	O
2153	O
ETOH:	O
rare	O
social	O
Illicit	O
drugs:	O
no	O
Environmental	O
exposure:	O
battery	O
chemicals	O
such	O
as	O
zinc	O
chloride,	O
manganese	O
dioxide,	O
carbon,	O
and	O
chemicals	O
for	O
dye	O
production	O
Exercise:	O
generally	O
walks	O
several	O
times	O
a	O
week	O
and	O
plays	O
golf	O
once	O
a	O
week.	O
Able	O
to	O
climb	O
1-2	O
flt	O
stairs	O
without	O
sob.	O
Mother:	O
died	O
age	O
65	O
MI	O
Father:	O
died	O
age	O
89	O
"multip	O
organ	O
failure"	O
Siblings:	O
older	O
brother	O
and	O
younger	O
sister	O
no	O
ca	O
Review	O
of	O
Systems:	O
General:	O
No	O
fevers,	O
no	O
chills,	O
no	O
change	O
in	O
appetite	O
Respiratory:	O
chronic	O
intermit	O
cough,	O
no	O
SOB,	O
no	O
DOE,	O
no	O
hemoptysis,	O
no	O
wheezing	O
HEENT:	O
No	O
neck	O
swelling,	O
no	O
neck	O
stiffness,	O
slight	O
hoarseness,	O
significant	O
hearing	O
loss,	O
primarily	O
on	O
the	O
left.	O
Cardiac:	O
No	O
chest	O
pain,	O
no	O
palpitations,	O
no	O
arrythmias,	O
no	O
valve	O
disease	O
Gastrointestinal:	O
No	O
nausea,	O
no	O
vomiting,	O
no	O
diarrhea,	O
no	O
constipation,	O
no	O
bleeding,	O
eating	O
by	O
mouth	O
with	O
intermittent	O
dysphagia	O
Neurologic:	O
No	O
seizures,	O
occas	O
headache,	O
no	O
weakness,	O
no	O
numbness/	O
tingling	O
Lymph	O
nodes:	O
No	O
enlarged	O
lymph	O
nodes	O
Musculoskeletal:	O
chronic	O
intermit	O
back	O
pain,	O
no	O
neck	O
pain,	O
no	O
leg	O
pain,	O
no	O
arm	O
pain	O
Urologic:	O
No	O
hematuria,	O
no	O
dysuria,	O
or	O
no	O
obstruction	O
Endocrine:	O
No	O
adrenal,	O
no	O
thyroid	O
or	O
no	O
parathyroid	O
disease	O
Hematologic:	O
No	O
bruising,	O
no	O
clotting,	O
no	O
bleeding	O
Toxicities:	O
0	O
1	O
2	O
3	O
4	O
Shortness	O
of	O
Breath	O
Nausea	O
Vomiting	O
Diarrhea	O
Renal	O
Toxicity	O
Dysphagia	O
/	O
Esophagitis	O
x	O
Rash	O
Neutropenic	O
Fever	O
Non-Neutropenic	O
Infection	O
Sensory	O
Neuropathy	O
Motor	O
Neuropathy	O
PE	O
/	O
DVT	O
Physical	O
Examination:	O
Vital	O
Signs	O
BP	O
88/52,	O
P	O
68,	O
RR	O
18,	O
Temp	O
97.8	O
F,	O
Ht	O
64.75	O
in,	O
Wt	O
147.25	O
lb	O
HT.	O
64.75	O
in	O
PULSE	O
SITTING	O
68	O
PAIN	O
LEVEL	O
(0-10)	O
0	O
FATIGUE	O
(0-10)	O
2	O
PS=1	O
Neck:	O
no	O
nodes	O
palpable	O
HEENT:no	O
mucositis;	O
EOMI;	O
PERRL;	O
anicteric	O
Skin:	O
no	O
rash	O
Ext:	O
without	O
edema,	O
clubbing,	O
cyanosis	O
Cor:	O
S1	O
S2,	O
no	O
mrg	O
Chest:	O
clear	O
lungs	O
without	O
wheezing	O
or	O
crackles,	O
no	O
dullness	O
to	O
percussion;	O
incisions	O
have	O
healed	O
well	O
Abdomen:	O
soft,	O
NT	O
no	O
HSM;	O
G-tube	O
site	O
is	O
clean	O
Nodes:	O
no	O
cervical	O
supraclavicular	O
axillary	O
or	O
inguinal	O
adenopathy	O
Neuro:	O
grossly	O
non-focal	O
Data	O
(laboratory	O
and	O
radiographs)	O
CBC	ORGANIZATION
and	O
chemistries	O
were	O
reviewed.	O
Assessment	O
and	O
Plan:	O
-	O
74	O
yr	O
old	O
gentleman	O
with	O
locally	O
advanced	O
esophageal	O
cancer	O
status	O
post	O
esophagectomy	O
with	O
negative	O
margins.	O
He	O
has	O
node	O
positive	O
disease,	O
and	O
therefore	O
we	O
initiated	O
adjuvant	O
chemotherapy	O
with	O
5-FU/LCV.	O
-	O
Labs	O
adequate	O
for	O
Week	O
4	O
5FU/	O
leukovorin	O
today.	O
-	O
To	O
meet	O
with	O
radiation	O
today.	O
-	O
Will	O
decrease	O
lisinopril	O
to	O
10mg	O
daily,	O
and	O
will	O
contact	O
pt's	O
cardiologist,	O
Dr.	O
Scott	PERSON
Ocasio	PERSON
regarding	O
hypotension.	O
-	O
Instructed	O
to	O
call	O
with	O
temp	O
100.5,	O
and	O
with	O
any	O
questions	O
or	O
concerns.	O
-	O
All	O
questions	O
answered	O
and	O
support	O
and	O
encouragement	O
provided.	O
-	O
RTC	ORGANIZATION
in	O
1	O
week	O
for	O
Week	O
5	O
5-FU/LCV.	O
__________________________________________	O
Paula	O
Xin,	O
NP	O
****************************************************************************************************	O
Record	O
date:	O
2171-09-20	O
09/20/2171	O
Dr.	O
Charles	PERSON
Calhoun	PERSON
479	O
Clairmoor	O
Drive	O
Athina,	O
GA	O
39341	O
RE:	O
HEATH,	O
QUINTEN	O
M	O
MRN:	O
8059475	O
DOB:	O
08/02/2095	O
Dear	O
Dr.	O
Calhoun;	O
We	O
had	O
the	O
pleasure	O
of	O
seeing	O
your	O
patient	O
Quinten	PERSON
Heath	PERSON
today	O
in	O
Internal	O
Medicine	O
in	O
followup	O
for	O
osteoporosis	O
and	O
hypogonadism.	O
As	O
you	O
know	O
Mr.	O
Heat	PERSON
is	O
a	O
pleasant	O
76-year-old	O
man	O
with	O
a	O
history	O
of	O
esophageal	O
cancer	O
status	O
post	O
surgery	O
in	O
2169,	O
chemotherapy	O
and	O
radiation	O
who	O
had	O
back	O
pain	O
prompting	O
imaging	O
that	O
revealed	O
several	O
vertebral	O
compression	O
fractures	O
and	O
his	O
BMD	ORGANIZATION
was	O
consistent	O
with	O
osteoporosis	O
with	O
a	O
T	O
score	O
of	O
minus	O
4.2	O
in	O
the	O
lumbar	O
spine	O
and	O
minus	O
2.3	O
in	O
the	O
femoral	O
neck.	O
We.	O
started	O
zoledronic	O
acid	O
IV	O
in	O
05/2171	O
that	O
he	O
tolerated	O
fine.	O
In	O
addition,	O
we	O
discovered	O
primary	O
hypogonadism	O
with	O
testosterone	O
levels	O
around	O
45	O
mg/dL	O
and	O
elevated	O
FSH	O
and	O
LH	O
levels.	O
We	O
started	O
AndroGel	O
therapy	O
for	O
testosterone	O
replacement	O
and	O
appeared	O
that	O
he	O
is	O
taking	O
every	O
day.	O
He	O
does	O
not	O
really	O
feel	O
any	O
better	O
or	O
stronger	O
with	O
this	O
medication.	O
PAST	O
MEDICAL	O
HISTORY:	O
1.	O
Osteoporosis.	O
2.	O
Hypogonadism.	O
3.	O
History	O
of	O
esophageal	O
cancer	O
status	O
post	O
esophagectomy	O
in	O
2169,	O
adjuvant	O
chemotherapy	O
and	O
radiation	O
therapy.	O
4.	O
Gastroesophageal	O
reflux	O
disease.	O
5.	O
Hypertension.	O
6.	O
Diabetes	O
type	O
2.	O
7.	O
Carotid	O
artery	O
disease,	O
left.	O
8.	O
Hypercholesterolemia.	O
9.	O
Status	O
post	O
AAA	O
repair	O
in	O
2165.	O
10.	O
Coronary	O
artery	O
disease	O
with	O
status	O
post	O
stent.	O
REVIEW	O
OF	O
SYSTEMS:	O
He	O
is	O
anxious	O
because	O
he	O
has	O
some	O
swallowing	O
problems	O
and	O
fears	O
that	O
his	O
esophageal	O
disease	O
is	O
acting	O
up	O
and	O
he	O
is	O
anticipating	O
a	O
CAT	O
scan	O
in	O
a	O
few	O
days.	O
MEDICATIONS:	O
Tricor,	O
metoprolol,	O
Prilosec,	O
Lipitor,	O
aspirin,	O
Plavix,	O
Isordil,	O
Zoloft,	O
vitamin	O
C,	O
Centrum	O
Caltrate	O
plus	O
D	O
600	O
mg	O
twice	O
a	O
day,	O
zoledronic	O
acid	O
4	O
mg	O
IV	O
once	O
a	O
year	O
and	O
AndroGel	O
5	O
grams	O
once	O
a	O
day.	O
ALLERGIES:	O
Tape.	O
SOCIAL	O
HISTORY:	O
He	O
is	O
married	O
and	O
has	O
3	O
children.	O
Vital	O
signs:	O
Blood	O
pressure	O
118/48,	O
pulse	O
68,	O
weight	O
136	O
pounds.	O
Height	O
5	O
feet	O
4	O
inches.	O
LABORATORY	O
EVALUATION:	O
His	O
testosterone	O
level	O
on	O
AndroGel	O
is	O
normal	O
at	O
292	O
mg/dL	O
(normal	O
195-1138)	O
on	O
09/13/2171.	O
His	O
hematocrit	O
is	O
39.	O
It	O
was	O
37	O
in	O
02/2171..	O
Platelets	O
are	O
179,000.	O
White	O
count	O
4.8.	O
In	O
summary,	O
Mr.	O
Heath	PERSON
is	O
a	O
76-year-old	O
man	O
with	O
osteoporosis	O
and	O
compression	O
fractures	O
probably	O
secondary	O
to	O
hypogonadism	O
and	O
therapy	O
for	O
esophageal	O
cancer.	O
The	O
cause	O
of	O
hypogonadism	O
is	O
probably	O
the	O
chemotherapy.	O
I	O
asked	O
him	O
again	O
to	O
see	O
a	O
urologist	O
to	O
rule	O
out	O
other	O
causes	O
of	O
primary	O
hypogonadism.	O
He	O
will	O
continue	O
the	O
current	O
treatment	O
with	O
calcium,	O
vitamin	O
D,	O
testosterone	O
and	O
antiresorptive	O
therapy	O
of	O
his	O
yearly	O
zoledronic	O
acid.	O
We	O
will	O
repeat	O
a	O
BMD	ORGANIZATION
in	O
2	O
years	O
from	O
now.	O
Please	O
let	O
me	O
know	O
if	O
you	O
have	O
any	O
questions.	O
Yours	O
sincerely,	O
___________________________	O
Vincent	O
Voorhees,	O
M.D.	O
cc:	O
Quinten	O
Heath,	O
976	O
Clinton	PERSON
Street,	O
Gardnerville	LOCATION
Ranchos,	O
GA	O
79269	O
Dr.	O
Quebedeaux	PERSON
Dr.	O
Hendricks	PERSON
DD:	O
09/20/2171	O
TD:	O
09/21/2171	O
16:37:30	O
TR:	O
8190465	O
BackJob	O
ID:	O
326372	O
VoiceJob	O
ID:	O
91587262	O
****************************************************************************************************	O
Record	O
date:	O
2173-02-04	O
RADIATION	O
ONCOLOGY	O
FOLLOW-UP	O
NOTE	O
HEATH,	O
QUINTEN	O
M.	O
805-94-75	O
REFERRING	O
PHYSICIAN:	O
Fred	O
Quebedeaux,	O
M.D.	O
DATE	O
OF	O
VISIT:	O
2/4/2173	O
REASON	O
FOR	O
VISIT:	O
Reassessment	O
of	O
progress,	O
s/p	O
postoperative	O
adjuvant	O
radiation	O
therapy	O
in	O
combination	O
with	O
concurrent	O
chemotherapy	O
for	O
esophageal	O
adenocarcinoma,	O
T2N1	O
stage	O
IIB,	O
s/p	O
left	O
thoracoabdominal	O
esophagectomy	O
(12/01/69)	O
with	O
pathologic	O
tumor	O
stage	O
T2N1M0	O
(3/23	O
involved	O
lymph	O
nodes).	O
POSTOPERATIVE	O
RADIATION	O
THERAPY:	O
For	O
the	O
presence	O
of	O
metastatic	O
disease	O
in	O
regional	O
lymph	O
nodes,	O
he	O
was	O
judged	O
a	O
candidate	O
for	O
postoperative	O
adjuvant	O
chemo-radiation	O
therapy.	O
He	O
was	O
treated	O
with	O
a	O
total	O
dose	O
of	O
45	O
Gy	O
in	O
25	O
fractions	O
to	O
the	O
tumor	O
bed	O
region	O
using	O
IMRT,	O
planned	O
with	O
4D	O
CT	O
simulation.	O
This	O
treatment	O
was	O
administered	O
in	O
combination	O
with	O
chemotherapy	O
between	O
March	O
29,	O
2170,	O
and	O
Apr	O
30,	O
2170.	O
HISTORY	O
OF	O
PRESENT	O
ILLNESS:	O
Mr.	O
Quinten	PERSON
Heath	PERSON
is	O
a	O
77	O
year-old	O
gentleman	O
with	O
a	O
history	O
of	O
smoking,	O
chronic	O
GERD,	O
HTN,	O
CAD	O
s/p	O
PCI,	O
partially	O
blocked	O
L	O
carotid	O
artery	O
and	O
AAA	O
repair	O
x	O
2.	O
He	O
was	O
in	O
his	O
USOH	O
until	O
8/69	O
when	O
he	O
began	O
to	O
have	O
difficulty	O
swallowing.	O
While	O
eating	O
a	O
hamburger	O
he	O
felt	O
it	O
lodge	O
in	O
his	O
chest	O
with	O
accompanying	O
retrosternal	O
discomfort	O
and	O
increased	O
salivation.	O
He	O
has	O
had	O
about	O
a	O
dozen	O
similar	O
episodes	O
since	O
then,	O
which	O
have	O
generally	O
resolved	O
within	O
10-15	O
minutes	O
without	O
regurgitation.	O
In	O
9/69	O
his	O
PCP	O
prescribed	O
a	O
PPI,	O
which	O
helped	O
somewhat,	O
but	O
he	O
continued	O
to	O
have	O
dysphagia.	O
On	O
10/16/69	O
EGD	O
at	O
Griffin	LOCATION
Hospital	LOCATION
demonstrated	O
a	O
partially-obstructing	O
distal	O
esophageal	O
mass	O
34-39	O
cm	O
from	O
the	O
incisors,	O
associated	O
with	O
Barrett&#8217;s	O
esophagus,	O
with	O
pathology	O
significant	O
for	O
well	O
differentiated	O
invasive	O
adenocarcinoma.	O
On	O
10/17/69	O
CT	O
of	O
the	O
chest	O
and	O
abdomen	O
showed	O
prominent	O
thickening	O
of	O
the	O
distal	O
esophagus.	O
On	O
10/21/69	O
repeat	O
CT	O
of	O
the	O
chest	O
showed	O
an	O
8	O
mm	O
LLL	O
nodule	O
and	O
R-sided	O
pleural	O
thickening.	O
Upper	O
EUS	ORGANIZATION
showed	O
a	O
partially	O
obstructing	O
esophageal	O
mass	O
extending	O
from	O
35-40	O
cm	O
with	O
9	O
mm	O
thickness,	O
suggestive	O
of	O
invasion	O
to	O
the	O
muscularis	O
propria,	O
one	O
suspicious	O
mediastinal	O
LN,	O
and	O
porta	O
hepatis	O
adenopathy.	O
On	O
10/22	O
FNA	O
of	O
a	O
porta	O
hepatis	O
LN	O
was	O
negative.	O
He	O
underwent	O
a	O
left	O
thoracoabdominal	O
esophagectomy	O
on	O
12/1/69.	O
Pathology	O
showed	O
an	O
ulcerated	O
tumor	O
measuring	O
4.0x3.5x0.8	O
cm,	O
invading	O
into	O
muscularis	O
propria	O
(pT2)	O
and	O
metastasis	O
to	O
3	O
of	O
27	O
LN	O
(N1).	O
INTERVAL	O
HISTORY/REVIEW	O
OF	O
SYSTEMS:	O
He	O
has	O
been	O
doing	O
well	O
without	O
significant	O
GI	O
or	O
respiratory	O
symptoms.	O
He	O
has	O
maintained	O
steady	O
energy	O
and	O
appetite.	O
He	O
has	O
had	O
mild	O
dysphagia	O
for	O
solid	O
and	O
meat.	O
Otherwise,	O
he	O
has	O
been	O
doing	O
well.	O
He	O
denies	O
significant	O
respiratory	O
symptoms.	O
No	O
focal	O
or	O
generalized	O
neurological	O
symptoms.	O
The	O
remainder	O
of	O
review	O
of	O
systems	O
was	O
unremarkable.	O
PAST	O
MEDICAL	O
HISTORY:	O
GERD	O
x	O
20	O
years	O
HTN	O
Hypercholesterolemia	O
DM	O
type	O
II	O
CAD	O
s/p	O
stent	O
2164	O
Partially	O
blocked	O
L	O
carotid	O
AAA	O
repair	O
2165,	O
2167	O
Chronic	O
bronchitis	O
CURRENT	O
MEDICATIONS:	O
Aspirin	O
(ACETYLSALICYLIC	O
ACID)	O
325	O
MG	O
(325MG	O
TABLET	O
Take	O
1)	O
PO	O
QD	O
Caltrate	O
600	O
+	O
D	O
(CALCIUM	O
CARBONATE	O
1500	O
MG	O
(600	O
MG	O
ELEM	O
CA)/	O
VIT	O
D	O
200	O
IU)	O
1	O
TAB	O
PO	O
BID	O
Centrum	O
SILVER	O
(MULTIVITAMINS)	O
1	O
TAB	O
PO	O
QD	O
Isosorbide	O
30	O
MG	O
1/2	O
TAB	O
PO	O
bid	O
Metformin	O
250	O
MG	O
(500MG	O
TABLET	O
Take	O
0.5)	O
PO	O
QD	O
Metoprolol	O
SUCCINATE	O
EXTENDED	O
RELEASE	O
50	O
MG	O
(50MG	O
TAB.SR	O
24H	O
Take	O
1)	O
PO	O
QD	O
Plavix	O
(CLOPIDOGREL)	O
75	O
MG	O
(75MG	O
TABLET	O
Take	O
1)	O
PO	O
QD	O
Prilosec	O
(OMEPRAZOLE)	O
20	O
MG	O
(20	O
MG	O
CAPSULE	O
DR	O
Take	O
1)	O
PO	O
QD	O
Reclast	O
(ZOLEDRONIC	O
ACID	O
)	O
4	O
MG	O
(4MG/5ML	O
VIAL	O
Take	O
5	O
ML)	O
IV	O
once	O
a	O
year	O
,	O
given	O
6/25/72	O
Simvastatin	O
20	O
MG	O
(20	O
MG	O
TABLET	O
Take	O
1)	O
PO	O
QPM	O
Tricor	O
(FENOFIBRATE	O
(TRICOR))	O
134	O
MG	O
(134MG	O
CAPSULE	O
Take	O
1)	O
PO	O
QD	O
Vitamin	O
C	O
(ASCORBIC	O
ACID)	O
500	O
MG	O
PO	O
qd	O
Zoloft	O
(SERTRALINE)	O
50	O
MG	O
(50MG	O
TABLET	O
Take	O
1)	O
PO	O
QD	O
#30	O
Tablet(s)	O
ALLERGIES:	O
Adhesive	O
tape	O
(rash)	O
SOCIAL	O
HISTORY:	O
Lives	O
with	O
wife	O
in	O
Gardnerville	LOCATION
Ranchos,	O
GA.	O
3	O
healthy	O
adult	O
children.	O
Retired	O
2158	O
as	O
a	O
Producer	O
for	O
National	O
Radi	PERSON
and	O
Access	O
Amer..	O
Quit	O
smoking	O
2153,	O
prior	O
&#189;-1	O
ppd	O
x	O
30	O
years.	O
Rare	O
social	O
alcohol.	O
FAMILY	O
HISTORY:	O
Mother,	O
MI,	O
died	O
age	O
65.	O
Father,	O
died	O
age	O
89.	O
Brother	O
and	O
sister,	O
no	O
cancer.	O
PHYSICAL	O
EXAMINATION:	O
General:	O
pleasant	O
elderly	O
gentleman,	O
NAD	O
ECOG	O
PS:	O
1	O
Vital	O
Signs:	O
BP	O
118/78,	O
P	O
72,	O
RR	O
16,	O
Temp	O
97.3	O
F,	O
Ht	O
64.75	O
in,	O
Wt	O
137.5	O
lb	O
O2	O
SAT	O
97	O
BMI	O
23.1	O
PAIN	O
LEVEL	O
(0-10)	O
0	O
HEENT:	O
PERRL,	O
EOMI,	O
MMM,	O
OP	O
clear	O
Neck:	O
supple,	O
no	O
JVD.	O
Nodes:	O
no	O
cervical	O
or	O
supraclavicular	O
LAD	O
Chest:	O
Clear	O
without	O
wheezes	O
or	O
rales.	O
Heart:	O
RRR,	O
no	O
m/r/g	O
Abdomen:	O
soft,	O
NT/ND,	O
no	O
HSM,	O
NABS,	O
well-healed	O
scar	O
from	O
AAA	ORGANIZATION
repair	O
Ext:	O
no	O
edema	O
or	O
tenderness.	O
Skin:	O
Unremarkable.	O
Neuro:	O
AOx3,	O
nonfocal	O
RADIOGRAPHS:	O
Restaging	O
CT	O
of	O
chest,	O
abdomen	O
and	O
pelvis	O
obtained	O
on	O
1/28/2173	O
showed	O
no	O
demonstrable	O
recurrence	O
or	O
metastatic	O
disease.	O
IMPRESSION	O
AND	O
PLAN:	O
This	O
77	O
year-old	O
man	O
with	O
adenocarcinoma	O
of	O
the	O
distal	O
esophagus,	O
s/p	O
left	O
thoracoabdominal	O
esophagectomy	O
(12/01/69)	O
with	O
pathologic	O
tumor	O
stage	O
T2N1M0	O
and	O
postoperative	O
adjuvant	O
chemo-radiation	O
therapy	O
has	O
been	O
doing	O
well	O
with	O
no	O
demonstrable	O
recurrence	O
or	O
metastatic	O
disease.	O
He	O
has	O
minimum	O
symptoms	O
of	O
dysphagia	O
with	O
meats.	O
An	O
arrangement	O
has	O
been	O
made	O
for	O
him	O
to	O
be	O
seen	O
at	O
the	O
clinic	O
in	O
6	O
months	O
with	O
restaging	O
CT	O
of	O
chest,	O
abdomen	O
and	O
pelvis.	O
.	O
_____________________	O
Benjamin	PERSON
H.	PERSON
Hendricks,	PERSON
M.D.	PERSON
Fernando	PERSON
Quebedeaux,	PERSON
M.D.	PERSON
Vincent	PERSON
Everson,	PERSON
M.D.	PERSON
Larry	PERSON
Quezada,	PERSON
M.D.	PERSON
Charles	PERSON
Chaney,	PERSON
M.D.	O
100	O
Clairmoor	O
Drive	O
Athina,	O
GA	O
39341	O
****************************************************************************************************	O
